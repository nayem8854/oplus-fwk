package android.media;

import android.app.ActivityThread;
import android.app.Application;
import android.app.OplusNotificationManager;
import android.common.IOplusCommonFeature;
import android.common.OplusFrameworkFactory;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.system.ErrnoException;
import android.system.Os;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.android.internal.util.Preconditions;
import com.oplus.media.IOplusZenModeFeature;
import com.oplus.multiapp.OplusMultiAppManager;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.net.CookieHandler;
import java.net.HttpCookie;
import java.net.InetSocketAddress;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;

public class MediaPlayer extends PlayerBase implements SubtitleController.Listener, VolumeAutomation, AudioRouting {
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private PowerManager.WakeLock mWakeLock = null;
  
  private int mStreamType = Integer.MIN_VALUE;
  
  private final Object mDrmLock = new Object();
  
  private static IOplusZenModeFeature mOplusZenModeFeature = (IOplusZenModeFeature)OplusFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusZenModeFeature.DEFAULT, new Object[0]);
  
  private OplusNotificationManager mOplusNotificationManager = new OplusNotificationManager();
  
  public static final boolean APPLY_METADATA_FILTER = true;
  
  public static final boolean BYPASS_METADATA_FILTER = false;
  
  private static final String IMEDIA_PLAYER = "android.media.IMediaPlayer";
  
  private static final int INVOKE_ID_ADD_EXTERNAL_SOURCE = 2;
  
  private static final int INVOKE_ID_ADD_EXTERNAL_SOURCE_FD = 3;
  
  private static final int INVOKE_ID_DESELECT_TRACK = 5;
  
  private static final int INVOKE_ID_GET_SELECTED_TRACK = 7;
  
  private static final int INVOKE_ID_GET_TRACK_INFO = 1;
  
  private static final int INVOKE_ID_SELECT_TRACK = 4;
  
  private static final int INVOKE_ID_SET_VIDEO_SCALE_MODE = 6;
  
  private static final int KEY_PARAMETER_AUDIO_ATTRIBUTES = 1400;
  
  private static final int MEDIA_AUDIO_ROUTING_CHANGED = 10000;
  
  private static final int MEDIA_BUFFERING_UPDATE = 3;
  
  private static final int MEDIA_DRM_INFO = 210;
  
  private static final int MEDIA_ERROR = 100;
  
  public static final int MEDIA_ERROR_IO = -1004;
  
  public static final int MEDIA_ERROR_MALFORMED = -1007;
  
  public static final int MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = 200;
  
  public static final int MEDIA_ERROR_SERVER_DIED = 100;
  
  public static final int MEDIA_ERROR_SYSTEM = -2147483648;
  
  public static final int MEDIA_ERROR_TIMED_OUT = -110;
  
  public static final int MEDIA_ERROR_UNKNOWN = 1;
  
  public static final int MEDIA_ERROR_UNSUPPORTED = -1010;
  
  private static final int MEDIA_INFO = 200;
  
  public static final int MEDIA_INFO_AUDIO_NOT_PLAYING = 804;
  
  public static final int MEDIA_INFO_BAD_INTERLEAVING = 800;
  
  public static final int MEDIA_INFO_BUFFERING_END = 702;
  
  public static final int MEDIA_INFO_BUFFERING_START = 701;
  
  public static final int MEDIA_INFO_EXTERNAL_METADATA_UPDATE = 803;
  
  public static final int MEDIA_INFO_METADATA_UPDATE = 802;
  
  public static final int MEDIA_INFO_NETWORK_BANDWIDTH = 703;
  
  public static final int MEDIA_INFO_NOT_SEEKABLE = 801;
  
  public static final int MEDIA_INFO_STARTED_AS_NEXT = 2;
  
  public static final int MEDIA_INFO_SUBTITLE_TIMED_OUT = 902;
  
  public static final int MEDIA_INFO_TIMED_TEXT_ERROR = 900;
  
  public static final int MEDIA_INFO_UNKNOWN = 1;
  
  public static final int MEDIA_INFO_UNSUPPORTED_SUBTITLE = 901;
  
  public static final int MEDIA_INFO_VIDEO_NOT_PLAYING = 805;
  
  public static final int MEDIA_INFO_VIDEO_RENDERING_START = 3;
  
  public static final int MEDIA_INFO_VIDEO_TRACK_LAGGING = 700;
  
  private static final int MEDIA_META_DATA = 202;
  
  public static final String MEDIA_MIMETYPE_TEXT_CEA_608 = "text/cea-608";
  
  public static final String MEDIA_MIMETYPE_TEXT_CEA_708 = "text/cea-708";
  
  public static final String MEDIA_MIMETYPE_TEXT_SUBRIP = "application/x-subrip";
  
  public static final String MEDIA_MIMETYPE_TEXT_VTT = "text/vtt";
  
  private static final int MEDIA_NOP = 0;
  
  private static final int MEDIA_NOTIFY_TIME = 98;
  
  private static final int MEDIA_PAUSED = 7;
  
  private static final int MEDIA_PLAYBACK_COMPLETE = 2;
  
  private static final int MEDIA_PREPARED = 1;
  
  private static final int MEDIA_SEEK_COMPLETE = 4;
  
  private static final int MEDIA_SET_VIDEO_SIZE = 5;
  
  private static final int MEDIA_SKIPPED = 9;
  
  private static final int MEDIA_STARTED = 6;
  
  private static final int MEDIA_STOPPED = 8;
  
  private static final int MEDIA_SUBTITLE_DATA = 201;
  
  private static final int MEDIA_TIMED_TEXT = 99;
  
  private static final int MEDIA_TIME_DISCONTINUITY = 211;
  
  public static final boolean METADATA_ALL = false;
  
  public static final boolean METADATA_UPDATE_ONLY = true;
  
  public static final int PLAYBACK_RATE_AUDIO_MODE_DEFAULT = 0;
  
  public static final int PLAYBACK_RATE_AUDIO_MODE_RESAMPLE = 2;
  
  public static final int PLAYBACK_RATE_AUDIO_MODE_STRETCH = 1;
  
  public static final int PREPARE_DRM_STATUS_PREPARATION_ERROR = 3;
  
  public static final int PREPARE_DRM_STATUS_PROVISIONING_NETWORK_ERROR = 1;
  
  public static final int PREPARE_DRM_STATUS_PROVISIONING_SERVER_ERROR = 2;
  
  public static final int PREPARE_DRM_STATUS_SUCCESS = 0;
  
  public static final int SEEK_CLOSEST = 3;
  
  public static final int SEEK_CLOSEST_SYNC = 2;
  
  public static final int SEEK_NEXT_SYNC = 1;
  
  public static final int SEEK_PREVIOUS_SYNC = 0;
  
  private static final String TAG = "MediaPlayer";
  
  public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT = 1;
  
  public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2;
  
  private boolean mActiveDrmScheme;
  
  private boolean mDrmConfigAllowed;
  
  private DrmInfo mDrmInfo;
  
  private boolean mDrmInfoResolved;
  
  private MediaDrm mDrmObj;
  
  private boolean mDrmProvisioningInProgress;
  
  private ProvisioningThread mDrmProvisioningThread;
  
  private byte[] mDrmSessionId;
  
  private UUID mDrmUUID;
  
  private EventHandler mEventHandler;
  
  private Handler mExtSubtitleDataHandler;
  
  private OnSubtitleDataListener mExtSubtitleDataListener;
  
  private BitSet mInbandTrackIndices;
  
  private Vector<Pair<Integer, SubtitleTrack>> mIndexTrackPairs;
  
  private final OnSubtitleDataListener mIntSubtitleDataListener;
  
  private int mListenerContext;
  
  private long mNativeContext;
  
  private long mNativeSurfaceTexture;
  
  private OnBufferingUpdateListener mOnBufferingUpdateListener;
  
  private final OnCompletionListener mOnCompletionInternalListener;
  
  private OnCompletionListener mOnCompletionListener;
  
  private OnDrmConfigHelper mOnDrmConfigHelper;
  
  private OnDrmInfoHandlerDelegate mOnDrmInfoHandlerDelegate;
  
  private OnDrmPreparedHandlerDelegate mOnDrmPreparedHandlerDelegate;
  
  private OnErrorListener mOnErrorListener;
  
  private OnInfoListener mOnInfoListener;
  
  private Handler mOnMediaTimeDiscontinuityHandler;
  
  private OnMediaTimeDiscontinuityListener mOnMediaTimeDiscontinuityListener;
  
  private OnPreparedListener mOnPreparedListener;
  
  private OnSeekCompleteListener mOnSeekCompleteListener;
  
  private OnTimedMetaDataAvailableListener mOnTimedMetaDataAvailableListener;
  
  private OnTimedTextListener mOnTimedTextListener;
  
  private OnVideoSizeChangedListener mOnVideoSizeChangedListener;
  
  private Vector<InputStream> mOpenSubtitleSources;
  
  private AudioDeviceInfo mPreferredDevice;
  
  private boolean mPrepareDrmInProgress;
  
  private ArrayMap<AudioRouting.OnRoutingChangedListener, NativeRoutingEventHandlerDelegate> mRoutingChangeListeners;
  
  private boolean mScreenOnWhilePlaying;
  
  private int mSelectedSubtitleTrackIndex;
  
  private boolean mStayAwake;
  
  private SubtitleController mSubtitleController;
  
  private boolean mSubtitleDataListenerDisabled;
  
  private SurfaceHolder mSurfaceHolder;
  
  private TimeProvider mTimeProvider;
  
  private final Object mTimeProviderLock;
  
  public MediaPlayer() {
    super((new AudioAttributes.Builder()).build(), 2);
    this.mPreferredDevice = null;
    this.mRoutingChangeListeners = new ArrayMap();
    this.mIndexTrackPairs = new Vector<>();
    this.mInbandTrackIndices = new BitSet();
    this.mSelectedSubtitleTrackIndex = -1;
    this.mIntSubtitleDataListener = new OnSubtitleDataListener() {
        final MediaPlayer this$0;
        
        public void onSubtitleData(MediaPlayer param1MediaPlayer, SubtitleData param1SubtitleData) {
          int i = param1SubtitleData.getTrackIndex();
          synchronized (MediaPlayer.this.mIndexTrackPairs) {
            for (Pair pair : MediaPlayer.this.mIndexTrackPairs) {
              if (pair.first != null && ((Integer)pair.first).intValue() == i && pair.second != null) {
                SubtitleTrack subtitleTrack = (SubtitleTrack)pair.second;
                subtitleTrack.onData(param1SubtitleData);
              } 
            } 
            return;
          } 
        }
      };
    this.mTimeProviderLock = new Object();
    this.mOnCompletionInternalListener = new OnCompletionListener() {
        final MediaPlayer this$0;
        
        public void onCompletion(MediaPlayer param1MediaPlayer) {
          MediaPlayer.this.baseStop();
        }
      };
    Looper looper = Looper.myLooper();
    if (looper != null) {
      this.mEventHandler = new EventHandler(this, looper);
    } else {
      looper = Looper.getMainLooper();
      if (looper != null) {
        this.mEventHandler = new EventHandler(this, looper);
      } else {
        this.mEventHandler = null;
      } 
    } 
    this.mTimeProvider = new TimeProvider(this);
    this.mOpenSubtitleSources = new Vector<>();
    native_setup(new WeakReference<>(this));
    baseRegisterPlayer();
  }
  
  public Parcel newRequest() {
    Parcel parcel = Parcel.obtain();
    parcel.writeInterfaceToken("android.media.IMediaPlayer");
    return parcel;
  }
  
  public void invoke(Parcel paramParcel1, Parcel paramParcel2) {
    int i = native_invoke(paramParcel1, paramParcel2);
    paramParcel2.setDataPosition(0);
    if (i == 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("failure code: ");
    stringBuilder.append(i);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public void setDisplay(SurfaceHolder paramSurfaceHolder) {
    this.mSurfaceHolder = paramSurfaceHolder;
    if (paramSurfaceHolder != null) {
      Surface surface = paramSurfaceHolder.getSurface();
    } else {
      paramSurfaceHolder = null;
    } 
    _setVideoSurface((Surface)paramSurfaceHolder);
    updateSurfaceScreenOn();
  }
  
  public void setSurface(Surface paramSurface) {
    if (this.mScreenOnWhilePlaying && paramSurface != null)
      Log.w("MediaPlayer", "setScreenOnWhilePlaying(true) is ineffective for Surface"); 
    this.mSurfaceHolder = null;
    _setVideoSurface(paramSurface);
    updateSurfaceScreenOn();
  }
  
  public void setVideoScalingMode(int paramInt) {
    if (isVideoScalingModeSupported(paramInt)) {
      Parcel parcel1 = Parcel.obtain();
      Parcel parcel2 = Parcel.obtain();
      try {
        parcel1.writeInterfaceToken("android.media.IMediaPlayer");
        parcel1.writeInt(6);
        parcel1.writeInt(paramInt);
        invoke(parcel1, parcel2);
        return;
      } finally {
        parcel1.recycle();
        parcel2.recycle();
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Scaling mode ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" is not supported");
    String str = stringBuilder.toString();
    throw new IllegalArgumentException(str);
  }
  
  public static MediaPlayer create(Context paramContext, Uri paramUri) {
    return create(paramContext, paramUri, (SurfaceHolder)null);
  }
  
  public static MediaPlayer create(Context paramContext, Uri paramUri, SurfaceHolder paramSurfaceHolder) {
    int i = AudioSystem.newAudioSessionId();
    if (i <= 0)
      i = 0; 
    return create(paramContext, paramUri, paramSurfaceHolder, (AudioAttributes)null, i);
  }
  
  public static MediaPlayer create(Context paramContext, Uri paramUri, SurfaceHolder paramSurfaceHolder, AudioAttributes paramAudioAttributes, int paramInt) {
    try {
      AudioAttributes audioAttributes;
      MediaPlayer mediaPlayer = new MediaPlayer();
      this();
      if (paramAudioAttributes == null) {
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        this();
        audioAttributes = builder.build();
      } 
      mediaPlayer.setAudioAttributes(audioAttributes);
      mediaPlayer.setAudioSessionId(paramInt);
      mediaPlayer.setDataSource(paramContext, paramUri);
      if (paramSurfaceHolder != null)
        mediaPlayer.setDisplay(paramSurfaceHolder); 
      mediaPlayer.prepare();
      return mediaPlayer;
    } catch (IOException iOException) {
      Log.d("MediaPlayer", "create failed:", iOException);
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.d("MediaPlayer", "create failed:", illegalArgumentException);
    } catch (SecurityException securityException) {
      Log.d("MediaPlayer", "create failed:", securityException);
    } 
    return null;
  }
  
  public static MediaPlayer create(Context paramContext, int paramInt) {
    int i = AudioSystem.newAudioSessionId();
    if (i <= 0)
      i = 0; 
    return create(paramContext, paramInt, (AudioAttributes)null, i);
  }
  
  public static MediaPlayer create(Context paramContext, int paramInt1, AudioAttributes paramAudioAttributes, int paramInt2) {
    try {
      AudioAttributes audioAttributes;
      AssetFileDescriptor assetFileDescriptor = paramContext.getResources().openRawResourceFd(paramInt1);
      if (assetFileDescriptor == null)
        return null; 
      MediaPlayer mediaPlayer = new MediaPlayer();
      this();
      if (paramAudioAttributes != null) {
        audioAttributes = paramAudioAttributes;
      } else {
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        this();
        audioAttributes = builder.build();
      } 
      mediaPlayer.setAudioAttributes(audioAttributes);
      mediaPlayer.setAudioSessionId(paramInt2);
      mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
      assetFileDescriptor.close();
      mediaPlayer.prepare();
      return mediaPlayer;
    } catch (IOException iOException) {
      Log.d("MediaPlayer", "create failed:", iOException);
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.d("MediaPlayer", "create failed:", illegalArgumentException);
    } catch (SecurityException securityException) {
      Log.d("MediaPlayer", "create failed:", securityException);
    } 
    return null;
  }
  
  public void setDataSource(Context paramContext, Uri paramUri) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
    setDataSource(paramContext, paramUri, (Map<String, String>)null, (List<HttpCookie>)null);
  }
  
  public void setDataSource(Context paramContext, Uri paramUri, Map<String, String> paramMap, List<HttpCookie> paramList) throws IOException {
    if (paramContext != null) {
      if (paramUri != null) {
        if (paramList != null) {
          CookieHandler cookieHandler = CookieHandler.getDefault();
          if (cookieHandler != null && !(cookieHandler instanceof java.net.CookieManager))
            throw new IllegalArgumentException("The cookie handler has to be of CookieManager type when cookies are provided."); 
        } 
        ContentResolver contentResolver = paramContext.getContentResolver();
        String str1 = paramUri.getScheme();
        String str2 = ContentProvider.getAuthorityWithoutUserId(paramUri.getAuthority());
        if ("file".equals(str1)) {
          setDataSource(paramUri.getPath());
          return;
        } 
        if ("content".equals(str1) && "settings".equals(str2)) {
          str2 = paramUri.toString();
          try {
            str2 = str1 = this.mOplusNotificationManager.getDynamicRingtone(paramUri.toString(), ActivityThread.currentPackageName());
          } catch (RemoteException remoteException) {
            Log.e("MediaPlayer", "setDataSource: exception");
          } 
          if (!TextUtils.equals(str2, paramUri.toString())) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("setDataSource: newUri = ");
            stringBuilder.append(str2);
            stringBuilder.append(" oldUri = ");
            stringBuilder.append(paramUri.toString());
            Log.d("MediaPlayer", stringBuilder.toString());
            paramUri = Uri.parse(str2);
            if (OplusMultiAppManager.shouldRedirectToMainUser(paramUri, paramContext)) {
              paramContext = paramContext.createContextAsUser(UserHandle.SYSTEM, 0);
              contentResolver = paramContext.getContentResolver();
            } 
            if (attemptDataSource(contentResolver, paramUri))
              return; 
            setDataSource(paramUri.toString(), paramMap, paramList);
            return;
          } 
          int i = RingtoneManager.getDefaultType(paramUri);
          Uri uri2 = RingtoneManager.getCacheForType(i, paramContext.getUserId());
          Uri uri1 = RingtoneManager.getActualDefaultRingtoneUri(paramContext, i);
          if (OplusMultiAppManager.shouldRedirectToMainUser(uri1, paramContext)) {
            paramContext = paramContext.createContextAsUser(UserHandle.SYSTEM, 0);
            contentResolver = paramContext.getContentResolver();
          } 
          if (attemptDataSource(contentResolver, uri2))
            return; 
          if (attemptDataSource(contentResolver, uri1))
            return; 
          setDataSource(paramUri.toString(), paramMap, paramList);
        } else {
          if (OplusMultiAppManager.shouldRedirectToMainUser(paramUri, paramContext)) {
            paramContext = paramContext.createContextAsUser(UserHandle.SYSTEM, 0);
            contentResolver = paramContext.getContentResolver();
          } 
          if (attemptDataSource(contentResolver, paramUri))
            return; 
          setDataSource(paramUri.toString(), paramMap, paramList);
        } 
        return;
      } 
      throw new NullPointerException("uri param can not be null.");
    } 
    throw new NullPointerException("context param can not be null.");
  }
  
  public void setDataSource(Context paramContext, Uri paramUri, Map<String, String> paramMap) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
    setDataSource(paramContext, paramUri, paramMap, (List<HttpCookie>)null);
  }
  
  private boolean attemptDataSource(ContentResolver paramContentResolver, Uri paramUri) {
    try {
      AssetFileDescriptor assetFileDescriptor = paramContentResolver.openAssetFileDescriptor(paramUri, "r");
      try {
        setDataSource(assetFileDescriptor);
        return true;
      } finally {
        if (assetFileDescriptor != null)
          try {
            assetFileDescriptor.close();
          } finally {
            assetFileDescriptor = null;
          }  
      } 
    } catch (NullPointerException|SecurityException|IOException nullPointerException) {
      return false;
    } 
  }
  
  public void setDataSource(String paramString) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
    setDataSource(paramString, (Map<String, String>)null, (List<HttpCookie>)null);
  }
  
  public void setDataSource(String paramString, Map<String, String> paramMap) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
    setDataSource(paramString, paramMap, (List<HttpCookie>)null);
  }
  
  private void setDataSource(String paramString, Map<String, String> paramMap, List<HttpCookie> paramList) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
    Map.Entry entry;
    String[] arrayOfString1 = null;
    String[] arrayOfString2 = null;
    if (paramMap != null) {
      String[] arrayOfString3 = new String[paramMap.size()];
      String[] arrayOfString4 = new String[paramMap.size()];
      byte b = 0;
      Iterator<Map.Entry> iterator = paramMap.entrySet().iterator();
      while (true) {
        arrayOfString1 = arrayOfString3;
        arrayOfString2 = arrayOfString4;
        if (iterator.hasNext()) {
          entry = iterator.next();
          arrayOfString3[b] = (String)entry.getKey();
          arrayOfString4[b] = (String)entry.getValue();
          b++;
          continue;
        } 
        break;
      } 
    } 
    setDataSource(paramString, (String[])entry, arrayOfString2, paramList);
  }
  
  private void setDataSource(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, List<HttpCookie> paramList) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
    String str1;
    Uri uri = Uri.parse(paramString);
    String str2 = uri.getScheme();
    if ("file".equals(str2)) {
      str1 = uri.getPath();
    } else {
      str1 = paramString;
      if (str2 != null)
        if (paramString.startsWith("/storage/") && !paramString.contains("://")) {
          str1 = paramString;
        } else {
          IBinder iBinder = MediaHTTPService.createHttpServiceBinderIfNecessary(paramString, paramList);
          nativeSetDataSource(iBinder, paramString, paramArrayOfString1, paramArrayOfString2);
          return;
        }  
    } 
    null = new File(str1);
    FileInputStream fileInputStream = new FileInputStream(null);
    try {
      setDataSource(fileInputStream.getFD());
      return;
    } finally {
      try {
        fileInputStream.close();
      } finally {
        fileInputStream = null;
      } 
    } 
  }
  
  public void setDataSource(AssetFileDescriptor paramAssetFileDescriptor) throws IOException, IllegalArgumentException, IllegalStateException {
    Preconditions.checkNotNull(paramAssetFileDescriptor);
    if (paramAssetFileDescriptor.getDeclaredLength() < 0L) {
      setDataSource(paramAssetFileDescriptor.getFileDescriptor());
    } else {
      setDataSource(paramAssetFileDescriptor.getFileDescriptor(), paramAssetFileDescriptor.getStartOffset(), paramAssetFileDescriptor.getDeclaredLength());
    } 
  }
  
  public void setDataSource(FileDescriptor paramFileDescriptor) throws IOException, IllegalArgumentException, IllegalStateException {
    setDataSource(paramFileDescriptor, 0L, 576460752303423487L);
  }
  
  public void setDataSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2) throws IOException, IllegalArgumentException, IllegalStateException {
    _setDataSource(paramFileDescriptor, paramLong1, paramLong2);
  }
  
  public void setDataSource(MediaDataSource paramMediaDataSource) throws IllegalArgumentException, IllegalStateException {
    _setDataSource(paramMediaDataSource);
  }
  
  public void prepare() throws IOException, IllegalStateException {
    _prepare();
    scanInternalSubtitleTracks();
    synchronized (this.mDrmLock) {
      this.mDrmInfoResolved = true;
      return;
    } 
  }
  
  public void start() throws IllegalStateException {
    int i = getStartDelayMs();
    Parcel parcel = mOplusZenModeFeature.checkZenMode();
    if (parcel != null) {
      setParameter(10011, parcel);
      parcel.recycle();
    } 
    if (i == 0) {
      startImpl();
    } else {
      Object object = new Object(this, i);
      object.start();
    } 
  }
  
  private void startImpl() {
    baseStart();
    stayAwake(true);
    _start();
  }
  
  private int getAudioStreamType() {
    if (this.mStreamType == Integer.MIN_VALUE)
      this.mStreamType = _getAudioStreamType(); 
    return this.mStreamType;
  }
  
  public void stop() throws IllegalStateException {
    stayAwake(false);
    _stop();
    baseStop();
  }
  
  public void pause() throws IllegalStateException {
    stayAwake(false);
    _pause();
    basePause();
  }
  
  void playerStart() {
    start();
  }
  
  void playerPause() {
    pause();
  }
  
  void playerStop() {
    stop();
  }
  
  int playerApplyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation) {
    return native_applyVolumeShaper(paramConfiguration, paramOperation);
  }
  
  VolumeShaper.State playerGetVolumeShaperState(int paramInt) {
    return native_getVolumeShaperState(paramInt);
  }
  
  public VolumeShaper createVolumeShaper(VolumeShaper.Configuration paramConfiguration) {
    return new VolumeShaper(paramConfiguration, this);
  }
  
  public boolean setPreferredDevice(AudioDeviceInfo paramAudioDeviceInfo) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_1
    //   3: ifnull -> 15
    //   6: aload_1
    //   7: invokevirtual isSink : ()Z
    //   10: ifne -> 15
    //   13: iconst_0
    //   14: ireturn
    //   15: aload_1
    //   16: ifnull -> 24
    //   19: aload_1
    //   20: invokevirtual getId : ()I
    //   23: istore_2
    //   24: aload_0
    //   25: iload_2
    //   26: invokespecial native_setOutputDevice : (I)Z
    //   29: istore_3
    //   30: iload_3
    //   31: iconst_1
    //   32: if_icmpne -> 52
    //   35: aload_0
    //   36: monitorenter
    //   37: aload_0
    //   38: aload_1
    //   39: putfield mPreferredDevice : Landroid/media/AudioDeviceInfo;
    //   42: aload_0
    //   43: monitorexit
    //   44: goto -> 52
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    //   52: iload_3
    //   53: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1527	-> 0
    //   #1528	-> 13
    //   #1530	-> 15
    //   #1531	-> 24
    //   #1532	-> 30
    //   #1533	-> 35
    //   #1534	-> 37
    //   #1535	-> 42
    //   #1537	-> 52
    // Exception table:
    //   from	to	target	type
    //   37	42	47	finally
    //   42	44	47	finally
    //   48	50	47	finally
  }
  
  public AudioDeviceInfo getPreferredDevice() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferredDevice : Landroid/media/AudioDeviceInfo;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1546	-> 0
    //   #1547	-> 2
    //   #1548	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  public AudioDeviceInfo getRoutedDevice() {
    int i = native_getRoutedDeviceId();
    if (i == 0)
      return null; 
    AudioDeviceInfo[] arrayOfAudioDeviceInfo = AudioManager.getDevicesStatic(2);
    for (byte b = 0; b < arrayOfAudioDeviceInfo.length; b++) {
      if (arrayOfAudioDeviceInfo[b].getId() == i)
        return arrayOfAudioDeviceInfo[b]; 
    } 
    return null;
  }
  
  private void enableNativeRoutingCallbacksLocked(boolean paramBoolean) {
    if (this.mRoutingChangeListeners.size() == 0)
      native_enableDeviceCallback(paramBoolean); 
  }
  
  public void addOnRoutingChangedListener(AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 67
    //   11: aload_0
    //   12: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   15: aload_1
    //   16: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   19: ifne -> 67
    //   22: aload_0
    //   23: iconst_1
    //   24: invokespecial enableNativeRoutingCallbacksLocked : (Z)V
    //   27: aload_0
    //   28: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   31: astore #4
    //   33: new android/media/NativeRoutingEventHandlerDelegate
    //   36: astore #5
    //   38: aload_2
    //   39: ifnull -> 45
    //   42: goto -> 50
    //   45: aload_0
    //   46: getfield mEventHandler : Landroid/media/MediaPlayer$EventHandler;
    //   49: astore_2
    //   50: aload #5
    //   52: aload_0
    //   53: aload_1
    //   54: aload_2
    //   55: invokespecial <init> : (Landroid/media/AudioRouting;Landroid/media/AudioRouting$OnRoutingChangedListener;Landroid/os/Handler;)V
    //   58: aload #4
    //   60: aload_1
    //   61: aload #5
    //   63: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   66: pop
    //   67: aload_3
    //   68: monitorexit
    //   69: return
    //   70: astore_1
    //   71: aload_3
    //   72: monitorexit
    //   73: aload_1
    //   74: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1603	-> 0
    //   #1604	-> 7
    //   #1605	-> 22
    //   #1606	-> 27
    //   #1608	-> 38
    //   #1606	-> 58
    //   #1610	-> 67
    //   #1611	-> 69
    //   #1610	-> 70
    // Exception table:
    //   from	to	target	type
    //   11	22	70	finally
    //   22	27	70	finally
    //   27	38	70	finally
    //   45	50	70	finally
    //   50	58	70	finally
    //   58	67	70	finally
    //   67	69	70	finally
    //   71	73	70	finally
  }
  
  public void removeOnRoutingChangedListener(AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener) {
    synchronized (this.mRoutingChangeListeners) {
      if (this.mRoutingChangeListeners.containsKey(paramOnRoutingChangedListener)) {
        this.mRoutingChangeListeners.remove(paramOnRoutingChangedListener);
        enableNativeRoutingCallbacksLocked(false);
      } 
      return;
    } 
  }
  
  public void setWakeMode(Context paramContext, int paramInt) {
    StringBuilder stringBuilder;
    boolean bool1 = false, bool2 = false;
    if (SystemProperties.getBoolean("audio.offload.ignore_setawake", false) == true) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("IGNORING setWakeMode ");
      stringBuilder.append(paramInt);
      Log.w("MediaPlayer", stringBuilder.toString());
      return;
    } 
    PowerManager.WakeLock wakeLock2 = this.mWakeLock;
    if (wakeLock2 != null) {
      bool1 = bool2;
      if (wakeLock2.isHeld()) {
        bool1 = true;
        this.mWakeLock.release();
      } 
      this.mWakeLock = null;
    } 
    PowerManager powerManager = (PowerManager)stringBuilder.getSystemService("power");
    PowerManager.WakeLock wakeLock1 = powerManager.newWakeLock(0x20000000 | paramInt, MediaPlayer.class.getName());
    wakeLock1.setReferenceCounted(false);
    if (bool1)
      this.mWakeLock.acquire(); 
  }
  
  public void setScreenOnWhilePlaying(boolean paramBoolean) {
    if (this.mScreenOnWhilePlaying != paramBoolean) {
      if (paramBoolean && this.mSurfaceHolder == null)
        Log.w("MediaPlayer", "setScreenOnWhilePlaying(true) is ineffective without a SurfaceHolder"); 
      this.mScreenOnWhilePlaying = paramBoolean;
      updateSurfaceScreenOn();
    } 
  }
  
  private void stayAwake(boolean paramBoolean) {
    PowerManager.WakeLock wakeLock = this.mWakeLock;
    if (wakeLock != null)
      if (paramBoolean && !wakeLock.isHeld()) {
        this.mWakeLock.acquire();
      } else if (!paramBoolean && this.mWakeLock.isHeld()) {
        this.mWakeLock.release();
      }  
    this.mStayAwake = paramBoolean;
    updateSurfaceScreenOn();
  }
  
  private void updateSurfaceScreenOn() {
    SurfaceHolder surfaceHolder = this.mSurfaceHolder;
    if (surfaceHolder != null) {
      boolean bool;
      if (this.mScreenOnWhilePlaying && this.mStayAwake) {
        bool = true;
      } else {
        bool = false;
      } 
      surfaceHolder.setKeepScreenOn(bool);
    } 
  }
  
  public PersistableBundle getMetrics() {
    return native_getMetrics();
  }
  
  public PlaybackParams easyPlaybackParams(float paramFloat, int paramInt) {
    String str;
    PlaybackParams playbackParams = new PlaybackParams();
    playbackParams.allowDefaults();
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2) {
          playbackParams.setSpeed(paramFloat).setPitch(paramFloat);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Audio playback mode ");
          stringBuilder.append(paramInt);
          stringBuilder.append(" is not supported");
          str = stringBuilder.toString();
          throw new IllegalArgumentException(str);
        } 
      } else {
        PlaybackParams playbackParams1 = str.setSpeed(paramFloat).setPitch(1.0F);
        playbackParams1.setAudioFallbackMode(2);
      } 
    } else {
      str.setSpeed(paramFloat).setPitch(1.0F);
    } 
    return (PlaybackParams)str;
  }
  
  public void seekTo(long paramLong, int paramInt) {
    if (paramInt >= 0 && paramInt <= 3) {
      long l;
      if (paramLong > 2147483647L) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("seekTo offset ");
        stringBuilder1.append(paramLong);
        stringBuilder1.append(" is too large, cap to ");
        stringBuilder1.append(2147483647);
        Log.w("MediaPlayer", stringBuilder1.toString());
        l = 2147483647L;
      } else {
        l = paramLong;
        if (paramLong < -2147483648L) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("seekTo offset ");
          stringBuilder1.append(paramLong);
          stringBuilder1.append(" is too small, cap to ");
          stringBuilder1.append(-2147483648);
          Log.w("MediaPlayer", stringBuilder1.toString());
          l = -2147483648L;
        } 
      } 
      _seekTo(l, paramInt);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal seek mode: ");
    stringBuilder.append(paramInt);
    String str = stringBuilder.toString();
    throw new IllegalArgumentException(str);
  }
  
  public void seekTo(int paramInt) throws IllegalStateException {
    seekTo(paramInt, 0);
  }
  
  public MediaTimestamp getTimestamp() {
    try {
      float f;
      long l1 = getCurrentPosition();
      long l2 = System.nanoTime();
      if (isPlaying()) {
        f = getPlaybackParams().getSpeed();
      } else {
        f = 0.0F;
      } 
      return new MediaTimestamp(l1 * 1000L, l2, f);
    } catch (IllegalStateException illegalStateException) {
      return null;
    } 
  }
  
  public Metadata getMetadata(boolean paramBoolean1, boolean paramBoolean2) {
    Parcel parcel = Parcel.obtain();
    Metadata metadata = new Metadata();
    if (!native_getMetadata(paramBoolean1, paramBoolean2, parcel)) {
      parcel.recycle();
      return null;
    } 
    if (!metadata.parse(parcel)) {
      parcel.recycle();
      return null;
    } 
    return metadata;
  }
  
  public int setMetadataFilter(Set<Integer> paramSet1, Set<Integer> paramSet2) {
    Parcel parcel = newRequest();
    int i = parcel.dataSize() + (paramSet1.size() + 1 + 1 + paramSet2.size()) * 4;
    if (parcel.dataCapacity() < i)
      parcel.setDataCapacity(i); 
    parcel.writeInt(paramSet1.size());
    for (Integer integer : paramSet1)
      parcel.writeInt(integer.intValue()); 
    parcel.writeInt(paramSet2.size());
    for (Integer integer : paramSet2)
      parcel.writeInt(integer.intValue()); 
    return native_setMetadataFilter(parcel);
  }
  
  public void release() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual baseRelease : ()V
    //   4: aload_0
    //   5: iconst_0
    //   6: invokespecial stayAwake : (Z)V
    //   9: aload_0
    //   10: invokespecial updateSurfaceScreenOn : ()V
    //   13: aload_0
    //   14: aconst_null
    //   15: putfield mOnPreparedListener : Landroid/media/MediaPlayer$OnPreparedListener;
    //   18: aload_0
    //   19: aconst_null
    //   20: putfield mOnBufferingUpdateListener : Landroid/media/MediaPlayer$OnBufferingUpdateListener;
    //   23: aload_0
    //   24: aconst_null
    //   25: putfield mOnCompletionListener : Landroid/media/MediaPlayer$OnCompletionListener;
    //   28: aload_0
    //   29: aconst_null
    //   30: putfield mOnSeekCompleteListener : Landroid/media/MediaPlayer$OnSeekCompleteListener;
    //   33: aload_0
    //   34: aconst_null
    //   35: putfield mOnErrorListener : Landroid/media/MediaPlayer$OnErrorListener;
    //   38: aload_0
    //   39: aconst_null
    //   40: putfield mOnInfoListener : Landroid/media/MediaPlayer$OnInfoListener;
    //   43: aload_0
    //   44: aconst_null
    //   45: putfield mOnVideoSizeChangedListener : Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
    //   48: aload_0
    //   49: aconst_null
    //   50: putfield mOnTimedTextListener : Landroid/media/MediaPlayer$OnTimedTextListener;
    //   53: aload_0
    //   54: getfield mTimeProviderLock : Ljava/lang/Object;
    //   57: astore_1
    //   58: aload_1
    //   59: monitorenter
    //   60: aload_0
    //   61: getfield mTimeProvider : Landroid/media/MediaPlayer$TimeProvider;
    //   64: ifnull -> 79
    //   67: aload_0
    //   68: getfield mTimeProvider : Landroid/media/MediaPlayer$TimeProvider;
    //   71: invokevirtual close : ()V
    //   74: aload_0
    //   75: aconst_null
    //   76: putfield mTimeProvider : Landroid/media/MediaPlayer$TimeProvider;
    //   79: aload_1
    //   80: monitorexit
    //   81: aload_0
    //   82: monitorenter
    //   83: aload_0
    //   84: iconst_0
    //   85: putfield mSubtitleDataListenerDisabled : Z
    //   88: aload_0
    //   89: aconst_null
    //   90: putfield mExtSubtitleDataListener : Landroid/media/MediaPlayer$OnSubtitleDataListener;
    //   93: aload_0
    //   94: aconst_null
    //   95: putfield mExtSubtitleDataHandler : Landroid/os/Handler;
    //   98: aload_0
    //   99: aconst_null
    //   100: putfield mOnMediaTimeDiscontinuityListener : Landroid/media/MediaPlayer$OnMediaTimeDiscontinuityListener;
    //   103: aload_0
    //   104: aconst_null
    //   105: putfield mOnMediaTimeDiscontinuityHandler : Landroid/os/Handler;
    //   108: aload_0
    //   109: monitorexit
    //   110: aload_0
    //   111: aconst_null
    //   112: putfield mOnDrmConfigHelper : Landroid/media/MediaPlayer$OnDrmConfigHelper;
    //   115: aload_0
    //   116: aconst_null
    //   117: putfield mOnDrmInfoHandlerDelegate : Landroid/media/MediaPlayer$OnDrmInfoHandlerDelegate;
    //   120: aload_0
    //   121: aconst_null
    //   122: putfield mOnDrmPreparedHandlerDelegate : Landroid/media/MediaPlayer$OnDrmPreparedHandlerDelegate;
    //   125: aload_0
    //   126: invokespecial resetDrmState : ()V
    //   129: aload_0
    //   130: invokespecial _release : ()V
    //   133: return
    //   134: astore_1
    //   135: aload_0
    //   136: monitorexit
    //   137: aload_1
    //   138: athrow
    //   139: astore_2
    //   140: aload_1
    //   141: monitorexit
    //   142: aload_2
    //   143: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2177	-> 0
    //   #2178	-> 4
    //   #2179	-> 9
    //   #2180	-> 13
    //   #2181	-> 18
    //   #2182	-> 23
    //   #2183	-> 28
    //   #2184	-> 33
    //   #2185	-> 38
    //   #2186	-> 43
    //   #2187	-> 48
    //   #2188	-> 53
    //   #2189	-> 60
    //   #2190	-> 67
    //   #2191	-> 74
    //   #2193	-> 79
    //   #2194	-> 81
    //   #2195	-> 83
    //   #2196	-> 88
    //   #2197	-> 93
    //   #2198	-> 98
    //   #2199	-> 103
    //   #2200	-> 108
    //   #2203	-> 110
    //   #2204	-> 115
    //   #2205	-> 120
    //   #2206	-> 125
    //   #2208	-> 129
    //   #2209	-> 133
    //   #2200	-> 134
    //   #2193	-> 139
    // Exception table:
    //   from	to	target	type
    //   60	67	139	finally
    //   67	74	139	finally
    //   74	79	139	finally
    //   79	81	139	finally
    //   83	88	134	finally
    //   88	93	134	finally
    //   93	98	134	finally
    //   98	103	134	finally
    //   103	108	134	finally
    //   108	110	134	finally
    //   135	137	134	finally
    //   140	142	139	finally
  }
  
  public void reset() {
    this.mSelectedSubtitleTrackIndex = -1;
    synchronized (this.mOpenSubtitleSources) {
      for (InputStream inputStream : this.mOpenSubtitleSources) {
        try {
          inputStream.close();
        } catch (IOException iOException) {}
      } 
      this.mOpenSubtitleSources.clear();
      null = this.mSubtitleController;
      if (null != null)
        null.reset(); 
      synchronized (this.mTimeProviderLock) {
        if (this.mTimeProvider != null) {
          this.mTimeProvider.close();
          this.mTimeProvider = null;
        } 
        stayAwake(false);
        _reset();
        EventHandler eventHandler = this.mEventHandler;
        if (eventHandler != null)
          eventHandler.removeCallbacksAndMessages(null); 
        synchronized (this.mIndexTrackPairs) {
          this.mIndexTrackPairs.clear();
          this.mInbandTrackIndices.clear();
          mOplusZenModeFeature.resetZenModeFlag();
          resetDrmState();
          return;
        } 
      } 
    } 
  }
  
  public void notifyAt(long paramLong) {
    _notifyAt(paramLong);
  }
  
  public void setAudioStreamType(int paramInt) {
    deprecateStreamTypeForPlayback(paramInt, "MediaPlayer", "setAudioStreamType()");
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    AudioAttributes audioAttributes = builder.setInternalLegacyStreamType(paramInt).build();
    baseUpdateAudioAttributes(audioAttributes);
    _setAudioStreamType(paramInt);
    this.mStreamType = paramInt;
    mOplusZenModeFeature.setAudioStreamType(paramInt);
    Parcel parcel = mOplusZenModeFeature.checkWechatMute();
    if (parcel != null) {
      setParameter(10011, parcel);
      parcel.recycle();
    } 
  }
  
  public void setAudioAttributes(AudioAttributes paramAudioAttributes) throws IllegalArgumentException {
    if (paramAudioAttributes != null) {
      baseUpdateAudioAttributes(paramAudioAttributes);
      Parcel parcel = Parcel.obtain();
      paramAudioAttributes.writeToParcel(parcel, 1);
      setParameter(1400, parcel);
      parcel.recycle();
      return;
    } 
    throw new IllegalArgumentException("Cannot set AudioAttributes to null");
  }
  
  public void setVolume(float paramFloat1, float paramFloat2) {
    baseSetVolume(paramFloat1, paramFloat2);
  }
  
  void playerSetVolume(boolean paramBoolean, float paramFloat1, float paramFloat2) {
    float f = 0.0F;
    if (paramBoolean)
      paramFloat1 = 0.0F; 
    if (paramBoolean)
      paramFloat2 = f; 
    _setVolume(paramFloat1, paramFloat2);
  }
  
  public void setVolume(float paramFloat) {
    setVolume(paramFloat, paramFloat);
  }
  
  public void setAuxEffectSendLevel(float paramFloat) {
    baseSetAuxEffectSendLevel(paramFloat);
  }
  
  int playerSetAuxEffectSendLevel(boolean paramBoolean, float paramFloat) {
    if (paramBoolean)
      paramFloat = 0.0F; 
    _setAuxEffectSendLevel(paramFloat);
    return 0;
  }
  
  public static class TrackInfo implements Parcelable {
    public int getTrackType() {
      return this.mTrackType;
    }
    
    public String getLanguage() {
      String str = this.mFormat.getString("language");
      if (str == null)
        str = "und"; 
      return str;
    }
    
    public MediaFormat getFormat() {
      int i = this.mTrackType;
      if (i == 3 || i == 4)
        return this.mFormat; 
      return null;
    }
    
    TrackInfo(Parcel param1Parcel) {
      this.mTrackType = param1Parcel.readInt();
      String str1 = param1Parcel.readString();
      String str2 = param1Parcel.readString();
      MediaFormat mediaFormat = MediaFormat.createSubtitleFormat(str1, str2);
      if (this.mTrackType == 4) {
        mediaFormat.setInteger("is-autoselect", param1Parcel.readInt());
        this.mFormat.setInteger("is-default", param1Parcel.readInt());
        this.mFormat.setInteger("is-forced-subtitle", param1Parcel.readInt());
      } 
    }
    
    TrackInfo(int param1Int, MediaFormat param1MediaFormat) {
      this.mTrackType = param1Int;
      this.mFormat = param1MediaFormat;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mTrackType);
      param1Parcel.writeString(this.mFormat.getString("mime"));
      param1Parcel.writeString(getLanguage());
      if (this.mTrackType == 4) {
        param1Parcel.writeInt(this.mFormat.getInteger("is-autoselect"));
        param1Parcel.writeInt(this.mFormat.getInteger("is-default"));
        param1Parcel.writeInt(this.mFormat.getInteger("is-forced-subtitle"));
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder1 = new StringBuilder(128);
      stringBuilder1.append(getClass().getName());
      stringBuilder1.append('{');
      int i = this.mTrackType;
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 4) {
              stringBuilder1.append("UNKNOWN");
            } else {
              stringBuilder1.append("SUBTITLE");
            } 
          } else {
            stringBuilder1.append("TIMEDTEXT");
          } 
        } else {
          stringBuilder1.append("AUDIO");
        } 
      } else {
        stringBuilder1.append("VIDEO");
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", ");
      stringBuilder2.append(this.mFormat.toString());
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append("}");
      return stringBuilder1.toString();
    }
    
    static final Parcelable.Creator<TrackInfo> CREATOR = new Parcelable.Creator<TrackInfo>() {
        public MediaPlayer.TrackInfo createFromParcel(Parcel param2Parcel) {
          return new MediaPlayer.TrackInfo(param2Parcel);
        }
        
        public MediaPlayer.TrackInfo[] newArray(int param2Int) {
          return new MediaPlayer.TrackInfo[param2Int];
        }
      };
    
    public static final int MEDIA_TRACK_TYPE_AUDIO = 2;
    
    public static final int MEDIA_TRACK_TYPE_METADATA = 5;
    
    public static final int MEDIA_TRACK_TYPE_SUBTITLE = 4;
    
    public static final int MEDIA_TRACK_TYPE_TIMEDTEXT = 3;
    
    public static final int MEDIA_TRACK_TYPE_UNKNOWN = 0;
    
    public static final int MEDIA_TRACK_TYPE_VIDEO = 1;
    
    final MediaFormat mFormat;
    
    final int mTrackType;
    
    @Retention(RetentionPolicy.SOURCE)
    class TrackType implements Annotation {}
  }
  
  class null implements Parcelable.Creator<TrackInfo> {
    public MediaPlayer.TrackInfo createFromParcel(Parcel param1Parcel) {
      return new MediaPlayer.TrackInfo(param1Parcel);
    }
    
    public MediaPlayer.TrackInfo[] newArray(int param1Int) {
      return new MediaPlayer.TrackInfo[param1Int];
    }
  }
  
  public TrackInfo[] getTrackInfo() throws IllegalStateException {
    TrackInfo[] arrayOfTrackInfo = getInbandTrackInfo();
    synchronized (this.mIndexTrackPairs) {
      TrackInfo[] arrayOfTrackInfo1 = new TrackInfo[this.mIndexTrackPairs.size()];
      for (byte b = 0; b < arrayOfTrackInfo1.length; b++) {
        Pair pair = this.mIndexTrackPairs.get(b);
        if (pair.first != null) {
          arrayOfTrackInfo1[b] = arrayOfTrackInfo[((Integer)pair.first).intValue()];
        } else {
          SubtitleTrack subtitleTrack = (SubtitleTrack)pair.second;
          arrayOfTrackInfo1[b] = new TrackInfo(subtitleTrack.getTrackType(), subtitleTrack.getFormat());
        } 
      } 
      return arrayOfTrackInfo1;
    } 
  }
  
  private TrackInfo[] getInbandTrackInfo() throws IllegalStateException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.media.IMediaPlayer");
      parcel1.writeInt(1);
      invoke(parcel1, parcel2);
      return parcel2.<TrackInfo>createTypedArray(TrackInfo.CREATOR);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  private static boolean availableMimeTypeForExternalSource(String paramString) {
    if ("application/x-subrip".equals(paramString))
      return true; 
    return false;
  }
  
  public void setSubtitleAnchor(SubtitleController paramSubtitleController, SubtitleController.Anchor paramAnchor) {
    this.mSubtitleController = paramSubtitleController;
    paramSubtitleController.setAnchor(paramAnchor);
  }
  
  private void setSubtitleAnchor() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSubtitleController : Landroid/media/SubtitleController;
    //   6: ifnonnull -> 93
    //   9: invokestatic currentApplication : ()Landroid/app/Application;
    //   12: ifnull -> 93
    //   15: aload_0
    //   16: invokevirtual getMediaTimeProvider : ()Landroid/media/MediaTimeProvider;
    //   19: checkcast android/media/MediaPlayer$TimeProvider
    //   22: astore_1
    //   23: new android/os/HandlerThread
    //   26: astore_2
    //   27: aload_2
    //   28: ldc_w 'SetSubtitleAnchorThread'
    //   31: invokespecial <init> : (Ljava/lang/String;)V
    //   34: aload_2
    //   35: invokevirtual start : ()V
    //   38: new android/os/Handler
    //   41: astore_3
    //   42: aload_3
    //   43: aload_2
    //   44: invokevirtual getLooper : ()Landroid/os/Looper;
    //   47: invokespecial <init> : (Landroid/os/Looper;)V
    //   50: new android/media/MediaPlayer$2
    //   53: astore #4
    //   55: aload #4
    //   57: aload_0
    //   58: aload_1
    //   59: aload_2
    //   60: invokespecial <init> : (Landroid/media/MediaPlayer;Landroid/media/MediaPlayer$TimeProvider;Landroid/os/HandlerThread;)V
    //   63: aload_3
    //   64: aload #4
    //   66: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   69: pop
    //   70: aload_2
    //   71: invokevirtual join : ()V
    //   74: goto -> 93
    //   77: astore_1
    //   78: invokestatic currentThread : ()Ljava/lang/Thread;
    //   81: invokevirtual interrupt : ()V
    //   84: ldc 'MediaPlayer'
    //   86: ldc_w 'failed to join SetSubtitleAnchorThread'
    //   89: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   92: pop
    //   93: aload_0
    //   94: monitorexit
    //   95: return
    //   96: astore_1
    //   97: aload_0
    //   98: monitorexit
    //   99: aload_1
    //   100: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2755	-> 2
    //   #2756	-> 15
    //   #2757	-> 23
    //   #2758	-> 34
    //   #2759	-> 38
    //   #2760	-> 50
    //   #2780	-> 70
    //   #2784	-> 74
    //   #2781	-> 77
    //   #2782	-> 78
    //   #2783	-> 84
    //   #2786	-> 93
    //   #2754	-> 96
    // Exception table:
    //   from	to	target	type
    //   2	15	96	finally
    //   15	23	96	finally
    //   23	34	96	finally
    //   34	38	96	finally
    //   38	50	96	finally
    //   50	70	96	finally
    //   70	74	77	java/lang/InterruptedException
    //   70	74	96	finally
    //   78	84	96	finally
    //   84	93	96	finally
  }
  
  public void onSubtitleTrackSelected(SubtitleTrack paramSubtitleTrack) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSelectedSubtitleTrackIndex : I
    //   4: istore_2
    //   5: iload_2
    //   6: iflt -> 24
    //   9: aload_0
    //   10: iload_2
    //   11: iconst_0
    //   12: invokespecial selectOrDeselectInbandTrack : (IZ)V
    //   15: goto -> 19
    //   18: astore_3
    //   19: aload_0
    //   20: iconst_m1
    //   21: putfield mSelectedSubtitleTrackIndex : I
    //   24: aload_0
    //   25: monitorenter
    //   26: aload_0
    //   27: iconst_1
    //   28: putfield mSubtitleDataListenerDisabled : Z
    //   31: aload_0
    //   32: monitorexit
    //   33: aload_1
    //   34: ifnonnull -> 38
    //   37: return
    //   38: aload_0
    //   39: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   42: astore_3
    //   43: aload_3
    //   44: monitorenter
    //   45: aload_0
    //   46: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   49: invokevirtual iterator : ()Ljava/util/Iterator;
    //   52: astore #4
    //   54: aload #4
    //   56: invokeinterface hasNext : ()Z
    //   61: ifeq -> 114
    //   64: aload #4
    //   66: invokeinterface next : ()Ljava/lang/Object;
    //   71: checkcast android/util/Pair
    //   74: astore #5
    //   76: aload #5
    //   78: getfield first : Ljava/lang/Object;
    //   81: ifnull -> 111
    //   84: aload #5
    //   86: getfield second : Ljava/lang/Object;
    //   89: aload_1
    //   90: if_acmpne -> 111
    //   93: aload_0
    //   94: aload #5
    //   96: getfield first : Ljava/lang/Object;
    //   99: checkcast java/lang/Integer
    //   102: invokevirtual intValue : ()I
    //   105: putfield mSelectedSubtitleTrackIndex : I
    //   108: goto -> 114
    //   111: goto -> 54
    //   114: aload_3
    //   115: monitorexit
    //   116: aload_0
    //   117: getfield mSelectedSubtitleTrackIndex : I
    //   120: istore_2
    //   121: iload_2
    //   122: iflt -> 152
    //   125: aload_0
    //   126: iload_2
    //   127: iconst_1
    //   128: invokespecial selectOrDeselectInbandTrack : (IZ)V
    //   131: goto -> 135
    //   134: astore_1
    //   135: aload_0
    //   136: monitorenter
    //   137: aload_0
    //   138: iconst_0
    //   139: putfield mSubtitleDataListenerDisabled : Z
    //   142: aload_0
    //   143: monitorexit
    //   144: goto -> 152
    //   147: astore_1
    //   148: aload_0
    //   149: monitorexit
    //   150: aload_1
    //   151: athrow
    //   152: return
    //   153: astore_1
    //   154: aload_3
    //   155: monitorexit
    //   156: aload_1
    //   157: athrow
    //   158: astore_1
    //   159: aload_0
    //   160: monitorexit
    //   161: aload_1
    //   162: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2810	-> 0
    //   #2812	-> 9
    //   #2814	-> 15
    //   #2813	-> 18
    //   #2815	-> 19
    //   #2817	-> 24
    //   #2818	-> 26
    //   #2819	-> 31
    //   #2820	-> 33
    //   #2821	-> 37
    //   #2824	-> 38
    //   #2825	-> 45
    //   #2826	-> 76
    //   #2828	-> 93
    //   #2829	-> 108
    //   #2831	-> 111
    //   #2832	-> 114
    //   #2834	-> 116
    //   #2836	-> 125
    //   #2838	-> 131
    //   #2837	-> 134
    //   #2839	-> 135
    //   #2840	-> 137
    //   #2841	-> 142
    //   #2844	-> 152
    //   #2832	-> 153
    //   #2819	-> 158
    // Exception table:
    //   from	to	target	type
    //   9	15	18	java/lang/IllegalStateException
    //   26	31	158	finally
    //   31	33	158	finally
    //   45	54	153	finally
    //   54	76	153	finally
    //   76	93	153	finally
    //   93	108	153	finally
    //   114	116	153	finally
    //   125	131	134	java/lang/IllegalStateException
    //   137	142	147	finally
    //   142	144	147	finally
    //   148	150	147	finally
    //   154	156	153	finally
    //   159	161	158	finally
  }
  
  public void addSubtitleSource(InputStream paramInputStream, MediaFormat paramMediaFormat) throws IllegalStateException {
    if (paramInputStream != null) {
      synchronized (this.mOpenSubtitleSources) {
        this.mOpenSubtitleSources.add(paramInputStream);
      } 
    } else {
      Log.w("MediaPlayer", "addSubtitleSource called with null InputStream");
    } 
    getMediaTimeProvider();
    HandlerThread handlerThread = new HandlerThread("SubtitleReadThread", 9);
    handlerThread.start();
    Handler handler = new Handler(handlerThread.getLooper());
    handler.post((Runnable)new Object(this, paramInputStream, paramMediaFormat, handlerThread));
  }
  
  private void scanInternalSubtitleTracks() {
    setSubtitleAnchor();
    populateInbandTracks();
    SubtitleController subtitleController = this.mSubtitleController;
    if (subtitleController != null)
      subtitleController.selectDefaultTrack(); 
  }
  
  private void populateInbandTracks() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial getInbandTrackInfo : ()[Landroid/media/MediaPlayer$TrackInfo;
    //   4: astore_1
    //   5: aload_0
    //   6: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   9: astore_2
    //   10: aload_2
    //   11: monitorenter
    //   12: iconst_0
    //   13: istore_3
    //   14: iload_3
    //   15: aload_1
    //   16: arraylength
    //   17: if_icmpge -> 174
    //   20: aload_0
    //   21: getfield mInbandTrackIndices : Ljava/util/BitSet;
    //   24: iload_3
    //   25: invokevirtual get : (I)Z
    //   28: ifeq -> 34
    //   31: goto -> 168
    //   34: aload_0
    //   35: getfield mInbandTrackIndices : Ljava/util/BitSet;
    //   38: iload_3
    //   39: invokevirtual set : (I)V
    //   42: aload_1
    //   43: iload_3
    //   44: aaload
    //   45: ifnonnull -> 85
    //   48: new java/lang/StringBuilder
    //   51: astore #4
    //   53: aload #4
    //   55: invokespecial <init> : ()V
    //   58: aload #4
    //   60: ldc_w 'unexpected NULL track at index '
    //   63: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload #4
    //   69: iload_3
    //   70: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: ldc 'MediaPlayer'
    //   76: aload #4
    //   78: invokevirtual toString : ()Ljava/lang/String;
    //   81: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   84: pop
    //   85: aload_1
    //   86: iload_3
    //   87: aaload
    //   88: ifnull -> 152
    //   91: aload_1
    //   92: iload_3
    //   93: aaload
    //   94: astore #4
    //   96: aload #4
    //   98: invokevirtual getTrackType : ()I
    //   101: iconst_4
    //   102: if_icmpne -> 152
    //   105: aload_0
    //   106: getfield mSubtitleController : Landroid/media/SubtitleController;
    //   109: astore #4
    //   111: aload_1
    //   112: iload_3
    //   113: aaload
    //   114: astore #5
    //   116: aload #5
    //   118: invokevirtual getFormat : ()Landroid/media/MediaFormat;
    //   121: astore #5
    //   123: aload #4
    //   125: aload #5
    //   127: invokevirtual addTrack : (Landroid/media/MediaFormat;)Landroid/media/SubtitleTrack;
    //   130: astore #4
    //   132: aload_0
    //   133: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   136: iload_3
    //   137: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   140: aload #4
    //   142: invokestatic create : (Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    //   145: invokevirtual add : (Ljava/lang/Object;)Z
    //   148: pop
    //   149: goto -> 168
    //   152: aload_0
    //   153: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   156: iload_3
    //   157: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   160: aconst_null
    //   161: invokestatic create : (Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    //   164: invokevirtual add : (Ljava/lang/Object;)Z
    //   167: pop
    //   168: iinc #3, 1
    //   171: goto -> 14
    //   174: aload_2
    //   175: monitorexit
    //   176: return
    //   177: astore_1
    //   178: aload_2
    //   179: monitorexit
    //   180: aload_1
    //   181: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2928	-> 0
    //   #2929	-> 5
    //   #2930	-> 12
    //   #2931	-> 20
    //   #2932	-> 31
    //   #2934	-> 34
    //   #2937	-> 42
    //   #2938	-> 48
    //   #2941	-> 85
    //   #2942	-> 96
    //   #2943	-> 105
    //   #2944	-> 116
    //   #2943	-> 123
    //   #2945	-> 132
    //   #2946	-> 149
    //   #2947	-> 152
    //   #2930	-> 168
    //   #2950	-> 174
    //   #2951	-> 176
    //   #2950	-> 177
    // Exception table:
    //   from	to	target	type
    //   14	20	177	finally
    //   20	31	177	finally
    //   34	42	177	finally
    //   48	85	177	finally
    //   96	105	177	finally
    //   105	111	177	finally
    //   116	123	177	finally
    //   123	132	177	finally
    //   132	149	177	finally
    //   152	168	177	finally
    //   174	176	177	finally
    //   178	180	177	finally
  }
  
  public void addTimedTextSource(String paramString1, String paramString2) throws IOException, IllegalArgumentException, IllegalStateException {
    if (availableMimeTypeForExternalSource(paramString2)) {
      null = new File(paramString1);
      FileInputStream fileInputStream = new FileInputStream(null);
      try {
        addTimedTextSource(fileInputStream.getFD(), paramString2);
        return;
      } finally {
        try {
          fileInputStream.close();
        } finally {
          paramString2 = null;
        } 
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal mimeType for timed text source: ");
    stringBuilder.append(paramString2);
    String str = stringBuilder.toString();
    throw new IllegalArgumentException(str);
  }
  
  public void addTimedTextSource(Context paramContext, Uri paramUri, String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    AssetFileDescriptor assetFileDescriptor1;
    String str1 = paramUri.getScheme();
    if (str1 == null || str1.equals("file")) {
      addTimedTextSource(paramUri.getPath(), paramString);
      return;
    } 
    String str2 = null;
    AssetFileDescriptor assetFileDescriptor2 = null, assetFileDescriptor3 = null;
    AssetFileDescriptor assetFileDescriptor4 = assetFileDescriptor3;
    str1 = str2;
    AssetFileDescriptor assetFileDescriptor5 = assetFileDescriptor2;
    try {
      ContentResolver contentResolver = paramContext.getContentResolver();
      assetFileDescriptor4 = assetFileDescriptor3;
      str1 = str2;
      assetFileDescriptor5 = assetFileDescriptor2;
      AssetFileDescriptor assetFileDescriptor = contentResolver.openAssetFileDescriptor(paramUri, "r");
      if (assetFileDescriptor == null)
        return; 
      assetFileDescriptor4 = assetFileDescriptor;
      assetFileDescriptor1 = assetFileDescriptor;
      assetFileDescriptor5 = assetFileDescriptor;
      addTimedTextSource(assetFileDescriptor.getFileDescriptor(), paramString);
      return;
    } catch (SecurityException securityException) {
    
    } catch (IOException iOException) {
    
    } finally {
      if (assetFileDescriptor4 != null)
        assetFileDescriptor4.close(); 
    } 
    assetFileDescriptor1.close();
  }
  
  public void addTimedTextSource(FileDescriptor paramFileDescriptor, String paramString) throws IllegalArgumentException, IllegalStateException {
    addTimedTextSource(paramFileDescriptor, 0L, 576460752303423487L, paramString);
  }
  
  public void addTimedTextSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2, String paramString) throws IllegalArgumentException, IllegalStateException {
    SubtitleTrack subtitleTrack;
    if (availableMimeTypeForExternalSource(paramString))
      try {
        Vector<Pair<Integer, SubtitleTrack>> vector;
        Handler handler;
        paramFileDescriptor = Os.dup(paramFileDescriptor);
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", paramString);
        mediaFormat.setInteger("is-timed-text", 1);
        if (this.mSubtitleController == null)
          setSubtitleAnchor(); 
        if (!this.mSubtitleController.hasRendererFor(mediaFormat)) {
          Application application = ActivityThread.currentApplication();
          this.mSubtitleController.registerRenderer(new SRTRenderer((Context)application, this.mEventHandler));
        } 
        subtitleTrack = this.mSubtitleController.addTrack(mediaFormat);
        synchronized (this.mIndexTrackPairs) {
          this.mIndexTrackPairs.add(Pair.create(null, subtitleTrack));
          getMediaTimeProvider();
          HandlerThread handlerThread = new HandlerThread("TimedTextReadThread", 9);
          handlerThread.start();
          handler = new Handler(handlerThread.getLooper());
          handler.post((Runnable)new Object(this, paramFileDescriptor, paramLong1, paramLong2, subtitleTrack, handlerThread));
          return;
        } 
      } catch (ErrnoException errnoException) {
        Log.e("MediaPlayer", errnoException.getMessage(), (Throwable)errnoException);
        throw new RuntimeException(errnoException);
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal mimeType for timed text source: ");
    stringBuilder.append((String)subtitleTrack);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getSelectedTrack(int paramInt) throws IllegalStateException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSubtitleController : Landroid/media/SubtitleController;
    //   4: ifnull -> 103
    //   7: iload_1
    //   8: iconst_4
    //   9: if_icmpeq -> 17
    //   12: iload_1
    //   13: iconst_3
    //   14: if_icmpne -> 103
    //   17: aload_0
    //   18: getfield mSubtitleController : Landroid/media/SubtitleController;
    //   21: invokevirtual getSelectedTrack : ()Landroid/media/SubtitleTrack;
    //   24: astore_2
    //   25: aload_2
    //   26: ifnull -> 103
    //   29: aload_0
    //   30: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   33: astore_3
    //   34: aload_3
    //   35: monitorenter
    //   36: iconst_0
    //   37: istore #4
    //   39: iload #4
    //   41: aload_0
    //   42: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   45: invokevirtual size : ()I
    //   48: if_icmpge -> 93
    //   51: aload_0
    //   52: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   55: iload #4
    //   57: invokevirtual get : (I)Ljava/lang/Object;
    //   60: checkcast android/util/Pair
    //   63: astore #5
    //   65: aload #5
    //   67: getfield second : Ljava/lang/Object;
    //   70: aload_2
    //   71: if_acmpne -> 87
    //   74: aload_2
    //   75: invokevirtual getTrackType : ()I
    //   78: iload_1
    //   79: if_icmpne -> 87
    //   82: aload_3
    //   83: monitorexit
    //   84: iload #4
    //   86: ireturn
    //   87: iinc #4, 1
    //   90: goto -> 39
    //   93: aload_3
    //   94: monitorexit
    //   95: goto -> 103
    //   98: astore_2
    //   99: aload_3
    //   100: monitorexit
    //   101: aload_2
    //   102: athrow
    //   103: invokestatic obtain : ()Landroid/os/Parcel;
    //   106: astore_2
    //   107: invokestatic obtain : ()Landroid/os/Parcel;
    //   110: astore_3
    //   111: aload_2
    //   112: ldc 'android.media.IMediaPlayer'
    //   114: invokevirtual writeInterfaceToken : (Ljava/lang/String;)V
    //   117: aload_2
    //   118: bipush #7
    //   120: invokevirtual writeInt : (I)V
    //   123: aload_2
    //   124: iload_1
    //   125: invokevirtual writeInt : (I)V
    //   128: aload_0
    //   129: aload_2
    //   130: aload_3
    //   131: invokevirtual invoke : (Landroid/os/Parcel;Landroid/os/Parcel;)V
    //   134: aload_3
    //   135: invokevirtual readInt : ()I
    //   138: istore #4
    //   140: aload_0
    //   141: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   144: astore #5
    //   146: aload #5
    //   148: monitorenter
    //   149: iconst_0
    //   150: istore_1
    //   151: iload_1
    //   152: aload_0
    //   153: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   156: invokevirtual size : ()I
    //   159: if_icmpge -> 218
    //   162: aload_0
    //   163: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   166: iload_1
    //   167: invokevirtual get : (I)Ljava/lang/Object;
    //   170: checkcast android/util/Pair
    //   173: astore #6
    //   175: aload #6
    //   177: getfield first : Ljava/lang/Object;
    //   180: ifnull -> 212
    //   183: aload #6
    //   185: getfield first : Ljava/lang/Object;
    //   188: checkcast java/lang/Integer
    //   191: invokevirtual intValue : ()I
    //   194: iload #4
    //   196: if_icmpne -> 212
    //   199: aload #5
    //   201: monitorexit
    //   202: aload_2
    //   203: invokevirtual recycle : ()V
    //   206: aload_3
    //   207: invokevirtual recycle : ()V
    //   210: iload_1
    //   211: ireturn
    //   212: iinc #1, 1
    //   215: goto -> 151
    //   218: aload #5
    //   220: monitorexit
    //   221: aload_2
    //   222: invokevirtual recycle : ()V
    //   225: aload_3
    //   226: invokevirtual recycle : ()V
    //   229: iconst_m1
    //   230: ireturn
    //   231: astore #6
    //   233: aload #5
    //   235: monitorexit
    //   236: aload #6
    //   238: athrow
    //   239: astore #5
    //   241: aload_2
    //   242: invokevirtual recycle : ()V
    //   245: aload_3
    //   246: invokevirtual recycle : ()V
    //   249: aload #5
    //   251: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3174	-> 0
    //   #3177	-> 17
    //   #3178	-> 25
    //   #3179	-> 29
    //   #3180	-> 36
    //   #3181	-> 51
    //   #3182	-> 65
    //   #3183	-> 82
    //   #3180	-> 87
    //   #3186	-> 93
    //   #3190	-> 103
    //   #3191	-> 107
    //   #3193	-> 111
    //   #3194	-> 117
    //   #3195	-> 123
    //   #3196	-> 128
    //   #3197	-> 134
    //   #3198	-> 140
    //   #3199	-> 149
    //   #3200	-> 162
    //   #3201	-> 175
    //   #3202	-> 199
    //   #3208	-> 202
    //   #3209	-> 206
    //   #3202	-> 210
    //   #3199	-> 212
    //   #3205	-> 218
    //   #3206	-> 221
    //   #3208	-> 221
    //   #3209	-> 225
    //   #3206	-> 229
    //   #3205	-> 231
    //   #3208	-> 239
    //   #3209	-> 245
    //   #3210	-> 249
    // Exception table:
    //   from	to	target	type
    //   39	51	98	finally
    //   51	65	98	finally
    //   65	82	98	finally
    //   82	84	98	finally
    //   93	95	98	finally
    //   99	101	98	finally
    //   111	117	239	finally
    //   117	123	239	finally
    //   123	128	239	finally
    //   128	134	239	finally
    //   134	140	239	finally
    //   140	149	239	finally
    //   151	162	231	finally
    //   162	175	231	finally
    //   175	199	231	finally
    //   199	202	231	finally
    //   218	221	231	finally
    //   233	236	231	finally
    //   236	239	239	finally
  }
  
  public void selectTrack(int paramInt) throws IllegalStateException {
    selectOrDeselectTrack(paramInt, true);
  }
  
  public void deselectTrack(int paramInt) throws IllegalStateException {
    selectOrDeselectTrack(paramInt, false);
  }
  
  private void selectOrDeselectTrack(int paramInt, boolean paramBoolean) throws IllegalStateException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial populateInbandTracks : ()V
    //   4: aload_0
    //   5: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   8: iload_1
    //   9: invokevirtual get : (I)Ljava/lang/Object;
    //   12: checkcast android/util/Pair
    //   15: astore_3
    //   16: aload_3
    //   17: getfield second : Ljava/lang/Object;
    //   20: checkcast android/media/SubtitleTrack
    //   23: astore #4
    //   25: aload #4
    //   27: ifnonnull -> 46
    //   30: aload_0
    //   31: aload_3
    //   32: getfield first : Ljava/lang/Object;
    //   35: checkcast java/lang/Integer
    //   38: invokevirtual intValue : ()I
    //   41: iload_2
    //   42: invokespecial selectOrDeselectInbandTrack : (IZ)V
    //   45: return
    //   46: aload_0
    //   47: getfield mSubtitleController : Landroid/media/SubtitleController;
    //   50: astore_3
    //   51: aload_3
    //   52: ifnonnull -> 56
    //   55: return
    //   56: iload_2
    //   57: ifne -> 91
    //   60: aload_3
    //   61: invokevirtual getSelectedTrack : ()Landroid/media/SubtitleTrack;
    //   64: aload #4
    //   66: if_acmpne -> 81
    //   69: aload_0
    //   70: getfield mSubtitleController : Landroid/media/SubtitleController;
    //   73: aconst_null
    //   74: invokevirtual selectTrack : (Landroid/media/SubtitleTrack;)Z
    //   77: pop
    //   78: goto -> 90
    //   81: ldc 'MediaPlayer'
    //   83: ldc_w 'trying to deselect track that was not selected'
    //   86: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   89: pop
    //   90: return
    //   91: aload #4
    //   93: invokevirtual getTrackType : ()I
    //   96: iconst_3
    //   97: if_icmpne -> 185
    //   100: aload_0
    //   101: iconst_3
    //   102: invokevirtual getSelectedTrack : (I)I
    //   105: istore_1
    //   106: aload_0
    //   107: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   110: astore_3
    //   111: aload_3
    //   112: monitorenter
    //   113: iload_1
    //   114: iflt -> 173
    //   117: iload_1
    //   118: aload_0
    //   119: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   122: invokevirtual size : ()I
    //   125: if_icmpge -> 173
    //   128: aload_0
    //   129: getfield mIndexTrackPairs : Ljava/util/Vector;
    //   132: iload_1
    //   133: invokevirtual get : (I)Ljava/lang/Object;
    //   136: checkcast android/util/Pair
    //   139: astore #5
    //   141: aload #5
    //   143: getfield first : Ljava/lang/Object;
    //   146: ifnull -> 173
    //   149: aload #5
    //   151: getfield second : Ljava/lang/Object;
    //   154: ifnonnull -> 173
    //   157: aload_0
    //   158: aload #5
    //   160: getfield first : Ljava/lang/Object;
    //   163: checkcast java/lang/Integer
    //   166: invokevirtual intValue : ()I
    //   169: iconst_0
    //   170: invokespecial selectOrDeselectInbandTrack : (IZ)V
    //   173: aload_3
    //   174: monitorexit
    //   175: goto -> 185
    //   178: astore #4
    //   180: aload_3
    //   181: monitorexit
    //   182: aload #4
    //   184: athrow
    //   185: aload_0
    //   186: getfield mSubtitleController : Landroid/media/SubtitleController;
    //   189: aload #4
    //   191: invokevirtual selectTrack : (Landroid/media/SubtitleTrack;)Z
    //   194: pop
    //   195: return
    //   196: astore #4
    //   198: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3266	-> 0
    //   #3268	-> 4
    //   #3270	-> 4
    //   #3274	-> 16
    //   #3276	-> 16
    //   #3277	-> 25
    //   #3279	-> 30
    //   #3280	-> 45
    //   #3283	-> 46
    //   #3284	-> 55
    //   #3287	-> 56
    //   #3289	-> 60
    //   #3290	-> 69
    //   #3292	-> 81
    //   #3294	-> 90
    //   #3298	-> 91
    //   #3299	-> 100
    //   #3300	-> 106
    //   #3301	-> 113
    //   #3302	-> 128
    //   #3303	-> 141
    //   #3305	-> 157
    //   #3308	-> 173
    //   #3310	-> 185
    //   #3311	-> 195
    //   #3271	-> 196
    //   #3273	-> 198
    // Exception table:
    //   from	to	target	type
    //   4	16	196	java/lang/ArrayIndexOutOfBoundsException
    //   117	128	178	finally
    //   128	141	178	finally
    //   141	157	178	finally
    //   157	173	178	finally
    //   173	175	178	finally
    //   180	182	178	finally
  }
  
  private void selectOrDeselectInbandTrack(int paramInt, boolean paramBoolean) throws IllegalStateException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      byte b;
      parcel1.writeInterfaceToken("android.media.IMediaPlayer");
      if (paramBoolean) {
        b = 4;
      } else {
        b = 5;
      } 
      parcel1.writeInt(b);
      parcel1.writeInt(paramInt);
      invoke(parcel1, parcel2);
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setRetransmitEndpoint(InetSocketAddress paramInetSocketAddress) throws IllegalStateException, IllegalArgumentException {
    String str = null;
    int i = 0;
    if (paramInetSocketAddress != null) {
      str = paramInetSocketAddress.getAddress().getHostAddress();
      i = paramInetSocketAddress.getPort();
    } 
    i = native_setRetransmitEndpoint(str, i);
    if (i == 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal re-transmit endpoint; native ret ");
    stringBuilder.append(i);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  protected void finalize() {
    baseRelease();
    native_finalize();
  }
  
  public MediaTimeProvider getMediaTimeProvider() {
    synchronized (this.mTimeProviderLock) {
      if (this.mTimeProvider == null) {
        TimeProvider timeProvider = new TimeProvider();
        this(this);
        this.mTimeProvider = timeProvider;
      } 
      return this.mTimeProvider;
    } 
  }
  
  private class EventHandler extends Handler {
    private MediaPlayer mMediaPlayer;
    
    final MediaPlayer this$0;
    
    public EventHandler(MediaPlayer param1MediaPlayer1, Looper param1Looper) {
      super(param1Looper);
      this.mMediaPlayer = param1MediaPlayer1;
    }
    
    public void handleMessage(Message param1Message) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   4: invokestatic access$800 : (Landroid/media/MediaPlayer;)J
      //   7: lconst_0
      //   8: lcmp
      //   9: ifne -> 21
      //   12: ldc 'MediaPlayer'
      //   14: ldc 'mediaplayer went away with unhandled events'
      //   16: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   19: pop
      //   20: return
      //   21: aload_1
      //   22: getfield what : I
      //   25: istore_2
      //   26: iload_2
      //   27: sipush #210
      //   30: if_icmpeq -> 1388
      //   33: iconst_0
      //   34: istore_3
      //   35: iconst_0
      //   36: istore #4
      //   38: iload_2
      //   39: sipush #211
      //   42: if_icmpeq -> 1229
      //   45: iload_2
      //   46: sipush #10000
      //   49: if_icmpeq -> 1158
      //   52: iload_2
      //   53: tableswitch default -> 108, 0 -> 1157, 1 -> 1105, 2 -> 1058, 3 -> 1028, 4 -> 985, 5 -> 951, 6 -> 917, 7 -> 917, 8 -> 898, 9 -> 1007
      //   108: iload_2
      //   109: tableswitch default -> 136, 98 -> 881, 99 -> 797, 100 -> 646
      //   136: iload_2
      //   137: tableswitch default -> 164, 200 -> 399, 201 -> 264, 202 -> 203
      //   164: new java/lang/StringBuilder
      //   167: dup
      //   168: invokespecial <init> : ()V
      //   171: astore #5
      //   173: aload #5
      //   175: ldc 'Unknown message type '
      //   177: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   180: pop
      //   181: aload #5
      //   183: aload_1
      //   184: getfield what : I
      //   187: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   190: pop
      //   191: ldc 'MediaPlayer'
      //   193: aload #5
      //   195: invokevirtual toString : ()Ljava/lang/String;
      //   198: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   201: pop
      //   202: return
      //   203: aload_0
      //   204: getfield this$0 : Landroid/media/MediaPlayer;
      //   207: astore #5
      //   209: aload #5
      //   211: invokestatic access$2900 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnTimedMetaDataAvailableListener;
      //   214: astore #5
      //   216: aload #5
      //   218: ifnonnull -> 222
      //   221: return
      //   222: aload_1
      //   223: getfield obj : Ljava/lang/Object;
      //   226: instanceof android/os/Parcel
      //   229: ifeq -> 263
      //   232: aload_1
      //   233: getfield obj : Ljava/lang/Object;
      //   236: checkcast android/os/Parcel
      //   239: astore_1
      //   240: aload_1
      //   241: invokestatic createTimedMetaDataFromParcel : (Landroid/os/Parcel;)Landroid/media/TimedMetaData;
      //   244: astore #6
      //   246: aload_1
      //   247: invokevirtual recycle : ()V
      //   250: aload #5
      //   252: aload_0
      //   253: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   256: aload #6
      //   258: invokeinterface onTimedMetaDataAvailable : (Landroid/media/MediaPlayer;Landroid/media/TimedMetaData;)V
      //   263: return
      //   264: aload_0
      //   265: monitorenter
      //   266: aload_0
      //   267: getfield this$0 : Landroid/media/MediaPlayer;
      //   270: invokestatic access$2400 : (Landroid/media/MediaPlayer;)Z
      //   273: ifeq -> 279
      //   276: aload_0
      //   277: monitorexit
      //   278: return
      //   279: aload_0
      //   280: getfield this$0 : Landroid/media/MediaPlayer;
      //   283: invokestatic access$2500 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnSubtitleDataListener;
      //   286: astore #6
      //   288: aload_0
      //   289: getfield this$0 : Landroid/media/MediaPlayer;
      //   292: invokestatic access$2600 : (Landroid/media/MediaPlayer;)Landroid/os/Handler;
      //   295: astore #5
      //   297: aload_0
      //   298: monitorexit
      //   299: aload_1
      //   300: getfield obj : Ljava/lang/Object;
      //   303: instanceof android/os/Parcel
      //   306: ifeq -> 393
      //   309: aload_1
      //   310: getfield obj : Ljava/lang/Object;
      //   313: checkcast android/os/Parcel
      //   316: astore_1
      //   317: new android/media/SubtitleData
      //   320: dup
      //   321: aload_1
      //   322: invokespecial <init> : (Landroid/os/Parcel;)V
      //   325: astore #7
      //   327: aload_1
      //   328: invokevirtual recycle : ()V
      //   331: aload_0
      //   332: getfield this$0 : Landroid/media/MediaPlayer;
      //   335: invokestatic access$2700 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnSubtitleDataListener;
      //   338: aload_0
      //   339: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   342: aload #7
      //   344: invokeinterface onSubtitleData : (Landroid/media/MediaPlayer;Landroid/media/SubtitleData;)V
      //   349: aload #6
      //   351: ifnull -> 393
      //   354: aload #5
      //   356: ifnonnull -> 375
      //   359: aload #6
      //   361: aload_0
      //   362: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   365: aload #7
      //   367: invokeinterface onSubtitleData : (Landroid/media/MediaPlayer;Landroid/media/SubtitleData;)V
      //   372: goto -> 393
      //   375: aload #5
      //   377: new android/media/MediaPlayer$EventHandler$1
      //   380: dup
      //   381: aload_0
      //   382: aload #6
      //   384: aload #7
      //   386: invokespecial <init> : (Landroid/media/MediaPlayer$EventHandler;Landroid/media/MediaPlayer$OnSubtitleDataListener;Landroid/media/SubtitleData;)V
      //   389: invokevirtual post : (Ljava/lang/Runnable;)Z
      //   392: pop
      //   393: return
      //   394: astore_1
      //   395: aload_0
      //   396: monitorexit
      //   397: aload_1
      //   398: athrow
      //   399: aload_1
      //   400: getfield arg1 : I
      //   403: istore_2
      //   404: iload_2
      //   405: sipush #802
      //   408: if_icmpeq -> 552
      //   411: iload_2
      //   412: sipush #803
      //   415: if_icmpeq -> 584
      //   418: iload_2
      //   419: tableswitch default -> 444, 700 -> 485, 701 -> 447, 702 -> 447
      //   444: goto -> 611
      //   447: aload_0
      //   448: getfield this$0 : Landroid/media/MediaPlayer;
      //   451: invokestatic access$600 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$TimeProvider;
      //   454: astore #5
      //   456: aload #5
      //   458: ifnull -> 611
      //   461: iload #4
      //   463: istore_3
      //   464: aload_1
      //   465: getfield arg1 : I
      //   468: sipush #701
      //   471: if_icmpne -> 476
      //   474: iconst_1
      //   475: istore_3
      //   476: aload #5
      //   478: iload_3
      //   479: invokevirtual onBuffering : (Z)V
      //   482: goto -> 611
      //   485: new java/lang/StringBuilder
      //   488: dup
      //   489: invokespecial <init> : ()V
      //   492: astore #5
      //   494: aload #5
      //   496: ldc 'Info ('
      //   498: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   501: pop
      //   502: aload #5
      //   504: aload_1
      //   505: getfield arg1 : I
      //   508: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   511: pop
      //   512: aload #5
      //   514: ldc ','
      //   516: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   519: pop
      //   520: aload #5
      //   522: aload_1
      //   523: getfield arg2 : I
      //   526: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   529: pop
      //   530: aload #5
      //   532: ldc ')'
      //   534: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   537: pop
      //   538: ldc 'MediaPlayer'
      //   540: aload #5
      //   542: invokevirtual toString : ()Ljava/lang/String;
      //   545: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   548: pop
      //   549: goto -> 611
      //   552: aload_0
      //   553: getfield this$0 : Landroid/media/MediaPlayer;
      //   556: invokestatic access$900 : (Landroid/media/MediaPlayer;)V
      //   559: goto -> 584
      //   562: astore #5
      //   564: aload_0
      //   565: bipush #100
      //   567: iconst_1
      //   568: sipush #-1010
      //   571: aconst_null
      //   572: invokevirtual obtainMessage : (IIILjava/lang/Object;)Landroid/os/Message;
      //   575: astore #5
      //   577: aload_0
      //   578: aload #5
      //   580: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   583: pop
      //   584: aload_1
      //   585: sipush #802
      //   588: putfield arg1 : I
      //   591: aload_0
      //   592: getfield this$0 : Landroid/media/MediaPlayer;
      //   595: invokestatic access$100 : (Landroid/media/MediaPlayer;)Landroid/media/SubtitleController;
      //   598: ifnull -> 611
      //   601: aload_0
      //   602: getfield this$0 : Landroid/media/MediaPlayer;
      //   605: invokestatic access$100 : (Landroid/media/MediaPlayer;)Landroid/media/SubtitleController;
      //   608: invokevirtual selectDefaultTrack : ()V
      //   611: aload_0
      //   612: getfield this$0 : Landroid/media/MediaPlayer;
      //   615: invokestatic access$2200 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnInfoListener;
      //   618: astore #5
      //   620: aload #5
      //   622: ifnull -> 645
      //   625: aload #5
      //   627: aload_0
      //   628: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   631: aload_1
      //   632: getfield arg1 : I
      //   635: aload_1
      //   636: getfield arg2 : I
      //   639: invokeinterface onInfo : (Landroid/media/MediaPlayer;II)Z
      //   644: pop
      //   645: return
      //   646: new java/lang/StringBuilder
      //   649: dup
      //   650: invokespecial <init> : ()V
      //   653: astore #5
      //   655: aload #5
      //   657: ldc 'Error ('
      //   659: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   662: pop
      //   663: aload #5
      //   665: aload_1
      //   666: getfield arg1 : I
      //   669: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   672: pop
      //   673: aload #5
      //   675: ldc ','
      //   677: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   680: pop
      //   681: aload #5
      //   683: aload_1
      //   684: getfield arg2 : I
      //   687: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   690: pop
      //   691: aload #5
      //   693: ldc ')'
      //   695: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   698: pop
      //   699: ldc 'MediaPlayer'
      //   701: aload #5
      //   703: invokevirtual toString : ()Ljava/lang/String;
      //   706: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   709: pop
      //   710: iconst_0
      //   711: istore_3
      //   712: aload_0
      //   713: getfield this$0 : Landroid/media/MediaPlayer;
      //   716: invokestatic access$2100 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnErrorListener;
      //   719: astore #5
      //   721: aload #5
      //   723: ifnull -> 746
      //   726: aload #5
      //   728: aload_0
      //   729: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   732: aload_1
      //   733: getfield arg1 : I
      //   736: aload_1
      //   737: getfield arg2 : I
      //   740: invokeinterface onError : (Landroid/media/MediaPlayer;II)Z
      //   745: istore_3
      //   746: aload_0
      //   747: getfield this$0 : Landroid/media/MediaPlayer;
      //   750: invokestatic access$1500 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;
      //   753: aload_0
      //   754: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   757: invokeinterface onCompletion : (Landroid/media/MediaPlayer;)V
      //   762: aload_0
      //   763: getfield this$0 : Landroid/media/MediaPlayer;
      //   766: invokestatic access$1600 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;
      //   769: astore_1
      //   770: aload_1
      //   771: ifnull -> 788
      //   774: iload_3
      //   775: ifne -> 788
      //   778: aload_1
      //   779: aload_0
      //   780: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   783: invokeinterface onCompletion : (Landroid/media/MediaPlayer;)V
      //   788: aload_0
      //   789: getfield this$0 : Landroid/media/MediaPlayer;
      //   792: iconst_0
      //   793: invokestatic access$1700 : (Landroid/media/MediaPlayer;Z)V
      //   796: return
      //   797: aload_0
      //   798: getfield this$0 : Landroid/media/MediaPlayer;
      //   801: invokestatic access$2300 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnTimedTextListener;
      //   804: astore #5
      //   806: aload #5
      //   808: ifnonnull -> 812
      //   811: return
      //   812: aload_1
      //   813: getfield obj : Ljava/lang/Object;
      //   816: ifnonnull -> 834
      //   819: aload #5
      //   821: aload_0
      //   822: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   825: aconst_null
      //   826: invokeinterface onTimedText : (Landroid/media/MediaPlayer;Landroid/media/TimedText;)V
      //   831: goto -> 880
      //   834: aload_1
      //   835: getfield obj : Ljava/lang/Object;
      //   838: instanceof android/os/Parcel
      //   841: ifeq -> 880
      //   844: aload_1
      //   845: getfield obj : Ljava/lang/Object;
      //   848: checkcast android/os/Parcel
      //   851: astore #6
      //   853: new android/media/TimedText
      //   856: dup
      //   857: aload #6
      //   859: invokespecial <init> : (Landroid/os/Parcel;)V
      //   862: astore_1
      //   863: aload #6
      //   865: invokevirtual recycle : ()V
      //   868: aload #5
      //   870: aload_0
      //   871: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   874: aload_1
      //   875: invokeinterface onTimedText : (Landroid/media/MediaPlayer;Landroid/media/TimedText;)V
      //   880: return
      //   881: aload_0
      //   882: getfield this$0 : Landroid/media/MediaPlayer;
      //   885: invokestatic access$600 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$TimeProvider;
      //   888: astore_1
      //   889: aload_1
      //   890: ifnull -> 897
      //   893: aload_1
      //   894: invokevirtual onNotifyTime : ()V
      //   897: return
      //   898: aload_0
      //   899: getfield this$0 : Landroid/media/MediaPlayer;
      //   902: invokestatic access$600 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$TimeProvider;
      //   905: astore_1
      //   906: aload_1
      //   907: ifnull -> 914
      //   910: aload_1
      //   911: invokevirtual onStopped : ()V
      //   914: goto -> 1157
      //   917: aload_0
      //   918: getfield this$0 : Landroid/media/MediaPlayer;
      //   921: invokestatic access$600 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$TimeProvider;
      //   924: astore #5
      //   926: aload #5
      //   928: ifnull -> 948
      //   931: aload_1
      //   932: getfield what : I
      //   935: bipush #7
      //   937: if_icmpne -> 942
      //   940: iconst_1
      //   941: istore_3
      //   942: aload #5
      //   944: iload_3
      //   945: invokevirtual onPaused : (Z)V
      //   948: goto -> 1157
      //   951: aload_0
      //   952: getfield this$0 : Landroid/media/MediaPlayer;
      //   955: invokestatic access$2000 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
      //   958: astore #5
      //   960: aload #5
      //   962: ifnull -> 984
      //   965: aload #5
      //   967: aload_0
      //   968: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   971: aload_1
      //   972: getfield arg1 : I
      //   975: aload_1
      //   976: getfield arg2 : I
      //   979: invokeinterface onVideoSizeChanged : (Landroid/media/MediaPlayer;II)V
      //   984: return
      //   985: aload_0
      //   986: getfield this$0 : Landroid/media/MediaPlayer;
      //   989: invokestatic access$1900 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnSeekCompleteListener;
      //   992: astore_1
      //   993: aload_1
      //   994: ifnull -> 1007
      //   997: aload_1
      //   998: aload_0
      //   999: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   1002: invokeinterface onSeekComplete : (Landroid/media/MediaPlayer;)V
      //   1007: aload_0
      //   1008: getfield this$0 : Landroid/media/MediaPlayer;
      //   1011: invokestatic access$600 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$TimeProvider;
      //   1014: astore_1
      //   1015: aload_1
      //   1016: ifnull -> 1027
      //   1019: aload_1
      //   1020: aload_0
      //   1021: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   1024: invokevirtual onSeekComplete : (Landroid/media/MediaPlayer;)V
      //   1027: return
      //   1028: aload_0
      //   1029: getfield this$0 : Landroid/media/MediaPlayer;
      //   1032: invokestatic access$1800 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnBufferingUpdateListener;
      //   1035: astore #5
      //   1037: aload #5
      //   1039: ifnull -> 1057
      //   1042: aload #5
      //   1044: aload_0
      //   1045: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   1048: aload_1
      //   1049: getfield arg1 : I
      //   1052: invokeinterface onBufferingUpdate : (Landroid/media/MediaPlayer;I)V
      //   1057: return
      //   1058: aload_0
      //   1059: getfield this$0 : Landroid/media/MediaPlayer;
      //   1062: invokestatic access$1500 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;
      //   1065: aload_0
      //   1066: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   1069: invokeinterface onCompletion : (Landroid/media/MediaPlayer;)V
      //   1074: aload_0
      //   1075: getfield this$0 : Landroid/media/MediaPlayer;
      //   1078: invokestatic access$1600 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;
      //   1081: astore_1
      //   1082: aload_1
      //   1083: ifnull -> 1096
      //   1086: aload_1
      //   1087: aload_0
      //   1088: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   1091: invokeinterface onCompletion : (Landroid/media/MediaPlayer;)V
      //   1096: aload_0
      //   1097: getfield this$0 : Landroid/media/MediaPlayer;
      //   1100: iconst_0
      //   1101: invokestatic access$1700 : (Landroid/media/MediaPlayer;Z)V
      //   1104: return
      //   1105: aload_0
      //   1106: getfield this$0 : Landroid/media/MediaPlayer;
      //   1109: invokestatic access$900 : (Landroid/media/MediaPlayer;)V
      //   1112: goto -> 1134
      //   1115: astore_1
      //   1116: aload_0
      //   1117: bipush #100
      //   1119: iconst_1
      //   1120: sipush #-1010
      //   1123: aconst_null
      //   1124: invokevirtual obtainMessage : (IIILjava/lang/Object;)Landroid/os/Message;
      //   1127: astore_1
      //   1128: aload_0
      //   1129: aload_1
      //   1130: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   1133: pop
      //   1134: aload_0
      //   1135: getfield this$0 : Landroid/media/MediaPlayer;
      //   1138: invokestatic access$1000 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnPreparedListener;
      //   1141: astore_1
      //   1142: aload_1
      //   1143: ifnull -> 1156
      //   1146: aload_1
      //   1147: aload_0
      //   1148: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   1151: invokeinterface onPrepared : (Landroid/media/MediaPlayer;)V
      //   1156: return
      //   1157: return
      //   1158: invokestatic resetAudioPortGeneration : ()I
      //   1161: pop
      //   1162: aload_0
      //   1163: getfield this$0 : Landroid/media/MediaPlayer;
      //   1166: invokestatic access$3000 : (Landroid/media/MediaPlayer;)Landroid/util/ArrayMap;
      //   1169: astore_1
      //   1170: aload_1
      //   1171: monitorenter
      //   1172: aload_0
      //   1173: getfield this$0 : Landroid/media/MediaPlayer;
      //   1176: invokestatic access$3000 : (Landroid/media/MediaPlayer;)Landroid/util/ArrayMap;
      //   1179: invokevirtual values : ()Ljava/util/Collection;
      //   1182: invokeinterface iterator : ()Ljava/util/Iterator;
      //   1187: astore #5
      //   1189: aload #5
      //   1191: invokeinterface hasNext : ()Z
      //   1196: ifeq -> 1219
      //   1199: aload #5
      //   1201: invokeinterface next : ()Ljava/lang/Object;
      //   1206: checkcast android/media/NativeRoutingEventHandlerDelegate
      //   1209: astore #6
      //   1211: aload #6
      //   1213: invokevirtual notifyClient : ()V
      //   1216: goto -> 1189
      //   1219: aload_1
      //   1220: monitorexit
      //   1221: return
      //   1222: astore #5
      //   1224: aload_1
      //   1225: monitorexit
      //   1226: aload #5
      //   1228: athrow
      //   1229: aload_0
      //   1230: monitorenter
      //   1231: aload_0
      //   1232: getfield this$0 : Landroid/media/MediaPlayer;
      //   1235: invokestatic access$3100 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnMediaTimeDiscontinuityListener;
      //   1238: astore #6
      //   1240: aload_0
      //   1241: getfield this$0 : Landroid/media/MediaPlayer;
      //   1244: invokestatic access$3200 : (Landroid/media/MediaPlayer;)Landroid/os/Handler;
      //   1247: astore #5
      //   1249: aload_0
      //   1250: monitorexit
      //   1251: aload #6
      //   1253: ifnonnull -> 1257
      //   1256: return
      //   1257: aload_1
      //   1258: getfield obj : Ljava/lang/Object;
      //   1261: instanceof android/os/Parcel
      //   1264: ifeq -> 1382
      //   1267: aload_1
      //   1268: getfield obj : Ljava/lang/Object;
      //   1271: checkcast android/os/Parcel
      //   1274: astore_1
      //   1275: aload_1
      //   1276: iconst_0
      //   1277: invokevirtual setDataPosition : (I)V
      //   1280: aload_1
      //   1281: invokevirtual readLong : ()J
      //   1284: lstore #8
      //   1286: aload_1
      //   1287: invokevirtual readLong : ()J
      //   1290: lstore #10
      //   1292: aload_1
      //   1293: invokevirtual readFloat : ()F
      //   1296: fstore #12
      //   1298: aload_1
      //   1299: invokevirtual recycle : ()V
      //   1302: lload #8
      //   1304: ldc2_w -1
      //   1307: lcmp
      //   1308: ifeq -> 1341
      //   1311: lload #10
      //   1313: ldc2_w -1
      //   1316: lcmp
      //   1317: ifeq -> 1341
      //   1320: new android/media/MediaTimestamp
      //   1323: dup
      //   1324: lload #8
      //   1326: lload #10
      //   1328: ldc2_w 1000
      //   1331: lmul
      //   1332: fload #12
      //   1334: invokespecial <init> : (JJF)V
      //   1337: astore_1
      //   1338: goto -> 1345
      //   1341: getstatic android/media/MediaTimestamp.TIMESTAMP_UNKNOWN : Landroid/media/MediaTimestamp;
      //   1344: astore_1
      //   1345: aload #5
      //   1347: ifnonnull -> 1365
      //   1350: aload #6
      //   1352: aload_0
      //   1353: getfield mMediaPlayer : Landroid/media/MediaPlayer;
      //   1356: aload_1
      //   1357: invokeinterface onMediaTimeDiscontinuity : (Landroid/media/MediaPlayer;Landroid/media/MediaTimestamp;)V
      //   1362: goto -> 1382
      //   1365: aload #5
      //   1367: new android/media/MediaPlayer$EventHandler$2
      //   1370: dup
      //   1371: aload_0
      //   1372: aload #6
      //   1374: aload_1
      //   1375: invokespecial <init> : (Landroid/media/MediaPlayer$EventHandler;Landroid/media/MediaPlayer$OnMediaTimeDiscontinuityListener;Landroid/media/MediaTimestamp;)V
      //   1378: invokevirtual post : (Ljava/lang/Runnable;)Z
      //   1381: pop
      //   1382: return
      //   1383: astore_1
      //   1384: aload_0
      //   1385: monitorexit
      //   1386: aload_1
      //   1387: athrow
      //   1388: new java/lang/StringBuilder
      //   1391: dup
      //   1392: invokespecial <init> : ()V
      //   1395: astore #5
      //   1397: aload #5
      //   1399: ldc_w 'MEDIA_DRM_INFO '
      //   1402: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1405: pop
      //   1406: aload #5
      //   1408: aload_0
      //   1409: getfield this$0 : Landroid/media/MediaPlayer;
      //   1412: invokestatic access$1100 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnDrmInfoHandlerDelegate;
      //   1415: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1418: pop
      //   1419: ldc 'MediaPlayer'
      //   1421: aload #5
      //   1423: invokevirtual toString : ()Ljava/lang/String;
      //   1426: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   1429: pop
      //   1430: aload_1
      //   1431: getfield obj : Ljava/lang/Object;
      //   1434: ifnonnull -> 1449
      //   1437: ldc 'MediaPlayer'
      //   1439: ldc_w 'MEDIA_DRM_INFO msg.obj=NULL'
      //   1442: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   1445: pop
      //   1446: goto -> 1582
      //   1449: aload_1
      //   1450: getfield obj : Ljava/lang/Object;
      //   1453: instanceof android/os/Parcel
      //   1456: ifeq -> 1543
      //   1459: aconst_null
      //   1460: astore #5
      //   1462: aload_0
      //   1463: getfield this$0 : Landroid/media/MediaPlayer;
      //   1466: invokestatic access$1200 : (Landroid/media/MediaPlayer;)Ljava/lang/Object;
      //   1469: astore #6
      //   1471: aload #6
      //   1473: monitorenter
      //   1474: aload #5
      //   1476: astore_1
      //   1477: aload_0
      //   1478: getfield this$0 : Landroid/media/MediaPlayer;
      //   1481: invokestatic access$1100 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnDrmInfoHandlerDelegate;
      //   1484: ifnull -> 1511
      //   1487: aload #5
      //   1489: astore_1
      //   1490: aload_0
      //   1491: getfield this$0 : Landroid/media/MediaPlayer;
      //   1494: invokestatic access$1300 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$DrmInfo;
      //   1497: ifnull -> 1511
      //   1500: aload_0
      //   1501: getfield this$0 : Landroid/media/MediaPlayer;
      //   1504: invokestatic access$1300 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$DrmInfo;
      //   1507: invokestatic access$1400 : (Landroid/media/MediaPlayer$DrmInfo;)Landroid/media/MediaPlayer$DrmInfo;
      //   1510: astore_1
      //   1511: aload_0
      //   1512: getfield this$0 : Landroid/media/MediaPlayer;
      //   1515: invokestatic access$1100 : (Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnDrmInfoHandlerDelegate;
      //   1518: astore #5
      //   1520: aload #6
      //   1522: monitorexit
      //   1523: aload #5
      //   1525: ifnull -> 1534
      //   1528: aload #5
      //   1530: aload_1
      //   1531: invokevirtual notifyClient : (Landroid/media/MediaPlayer$DrmInfo;)V
      //   1534: goto -> 1582
      //   1537: astore_1
      //   1538: aload #6
      //   1540: monitorexit
      //   1541: aload_1
      //   1542: athrow
      //   1543: new java/lang/StringBuilder
      //   1546: dup
      //   1547: invokespecial <init> : ()V
      //   1550: astore #5
      //   1552: aload #5
      //   1554: ldc_w 'MEDIA_DRM_INFO msg.obj of unexpected type '
      //   1557: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1560: pop
      //   1561: aload #5
      //   1563: aload_1
      //   1564: getfield obj : Ljava/lang/Object;
      //   1567: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1570: pop
      //   1571: ldc 'MediaPlayer'
      //   1573: aload #5
      //   1575: invokevirtual toString : ()Ljava/lang/String;
      //   1578: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   1581: pop
      //   1582: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3438	-> 0
      //   #3439	-> 12
      //   #3440	-> 20
      //   #3442	-> 21
      //   #3721	-> 164
      //   #3722	-> 202
      //   #3657	-> 203
      //   #3658	-> 209
      //   #3659	-> 216
      //   #3660	-> 221
      //   #3662	-> 222
      //   #3663	-> 232
      //   #3664	-> 240
      //   #3665	-> 246
      //   #3666	-> 250
      //   #3668	-> 263
      //   #3627	-> 264
      //   #3628	-> 266
      //   #3629	-> 276
      //   #3631	-> 279
      //   #3632	-> 288
      //   #3633	-> 297
      //   #3634	-> 299
      //   #3635	-> 309
      //   #3636	-> 317
      //   #3637	-> 327
      //   #3639	-> 331
      //   #3641	-> 349
      //   #3642	-> 354
      //   #3643	-> 359
      //   #3645	-> 375
      //   #3654	-> 393
      //   #3633	-> 394
      //   #3564	-> 399
      //   #3587	-> 447
      //   #3588	-> 456
      //   #3589	-> 461
      //   #3566	-> 485
      //   #3567	-> 549
      //   #3570	-> 552
      //   #3575	-> 559
      //   #3571	-> 562
      //   #3572	-> 564
      //   #3574	-> 577
      //   #3579	-> 584
      //   #3581	-> 591
      //   #3582	-> 601
      //   #3594	-> 611
      //   #3595	-> 620
      //   #3596	-> 625
      //   #3599	-> 645
      //   #3547	-> 646
      //   #3548	-> 710
      //   #3549	-> 712
      //   #3550	-> 721
      //   #3551	-> 726
      //   #3554	-> 746
      //   #3555	-> 762
      //   #3556	-> 770
      //   #3557	-> 778
      //   #3560	-> 788
      //   #3561	-> 796
      //   #3609	-> 797
      //   #3610	-> 806
      //   #3611	-> 811
      //   #3612	-> 812
      //   #3613	-> 819
      //   #3615	-> 834
      //   #3616	-> 844
      //   #3617	-> 853
      //   #3618	-> 863
      //   #3619	-> 868
      //   #3622	-> 880
      //   #3602	-> 881
      //   #3603	-> 889
      //   #3604	-> 893
      //   #3606	-> 897
      //   #3499	-> 898
      //   #3500	-> 906
      //   #3501	-> 910
      //   #3504	-> 914
      //   #3509	-> 917
      //   #3510	-> 926
      //   #3511	-> 931
      //   #3514	-> 948
      //   #3539	-> 951
      //   #3540	-> 960
      //   #3541	-> 965
      //   #3544	-> 984
      //   #3523	-> 985
      //   #3524	-> 993
      //   #3525	-> 997
      //   #3531	-> 1007
      //   #3532	-> 1015
      //   #3533	-> 1019
      //   #3536	-> 1027
      //   #3517	-> 1028
      //   #3518	-> 1037
      //   #3519	-> 1042
      //   #3520	-> 1057
      //   #3489	-> 1058
      //   #3490	-> 1074
      //   #3491	-> 1082
      //   #3492	-> 1086
      //   #3494	-> 1096
      //   #3495	-> 1104
      //   #3445	-> 1105
      //   #3453	-> 1112
      //   #3446	-> 1115
      //   #3450	-> 1116
      //   #3452	-> 1128
      //   #3455	-> 1134
      //   #3456	-> 1142
      //   #3457	-> 1146
      //   #3458	-> 1156
      //   #3671	-> 1157
      //   #3724	-> 1157
      //   #3674	-> 1158
      //   #3675	-> 1162
      //   #3677	-> 1172
      //   #3678	-> 1211
      //   #3679	-> 1216
      //   #3680	-> 1219
      //   #3681	-> 1221
      //   #3680	-> 1222
      //   #3686	-> 1229
      //   #3687	-> 1231
      //   #3688	-> 1240
      //   #3689	-> 1249
      //   #3690	-> 1251
      //   #3691	-> 1256
      //   #3693	-> 1257
      //   #3694	-> 1267
      //   #3695	-> 1275
      //   #3696	-> 1280
      //   #3697	-> 1286
      //   #3698	-> 1292
      //   #3699	-> 1298
      //   #3701	-> 1302
      //   #3702	-> 1320
      //   #3705	-> 1341
      //   #3707	-> 1345
      //   #3708	-> 1350
      //   #3710	-> 1365
      //   #3718	-> 1382
      //   #3689	-> 1383
      //   #3461	-> 1388
      //   #3463	-> 1430
      //   #3464	-> 1437
      //   #3465	-> 1449
      //   #3467	-> 1459
      //   #3470	-> 1462
      //   #3471	-> 1474
      //   #3472	-> 1500
      //   #3475	-> 1511
      //   #3476	-> 1520
      //   #3479	-> 1523
      //   #3480	-> 1528
      //   #3482	-> 1534
      //   #3476	-> 1537
      //   #3483	-> 1543
      //   #3485	-> 1582
      // Exception table:
      //   from	to	target	type
      //   266	276	394	finally
      //   276	278	394	finally
      //   279	288	394	finally
      //   288	297	394	finally
      //   297	299	394	finally
      //   395	397	394	finally
      //   552	559	562	java/lang/RuntimeException
      //   1105	1112	1115	java/lang/RuntimeException
      //   1172	1189	1222	finally
      //   1189	1211	1222	finally
      //   1211	1216	1222	finally
      //   1219	1221	1222	finally
      //   1224	1226	1222	finally
      //   1231	1240	1383	finally
      //   1240	1249	1383	finally
      //   1249	1251	1383	finally
      //   1384	1386	1383	finally
      //   1477	1487	1537	finally
      //   1490	1500	1537	finally
      //   1500	1511	1537	finally
      //   1511	1520	1537	finally
      //   1520	1523	1537	finally
      //   1538	1541	1537	finally
    }
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    // Byte code:
    //   0: aload_0
    //   1: checkcast java/lang/ref/WeakReference
    //   4: invokevirtual get : ()Ljava/lang/Object;
    //   7: checkcast android/media/MediaPlayer
    //   10: astore_0
    //   11: aload_0
    //   12: ifnonnull -> 16
    //   15: return
    //   16: iload_1
    //   17: iconst_1
    //   18: if_icmpeq -> 174
    //   21: iload_1
    //   22: sipush #200
    //   25: if_icmpeq -> 141
    //   28: iload_1
    //   29: sipush #210
    //   32: if_icmpeq -> 38
    //   35: goto -> 191
    //   38: ldc 'MediaPlayer'
    //   40: ldc_w 'postEventFromNative MEDIA_DRM_INFO'
    //   43: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   46: pop
    //   47: aload #4
    //   49: instanceof android/os/Parcel
    //   52: ifeq -> 101
    //   55: aload #4
    //   57: checkcast android/os/Parcel
    //   60: astore #5
    //   62: new android/media/MediaPlayer$DrmInfo
    //   65: dup
    //   66: aload #5
    //   68: aconst_null
    //   69: invokespecial <init> : (Landroid/os/Parcel;Landroid/media/MediaPlayer$1;)V
    //   72: astore #6
    //   74: aload_0
    //   75: getfield mDrmLock : Ljava/lang/Object;
    //   78: astore #5
    //   80: aload #5
    //   82: monitorenter
    //   83: aload_0
    //   84: aload #6
    //   86: putfield mDrmInfo : Landroid/media/MediaPlayer$DrmInfo;
    //   89: aload #5
    //   91: monitorexit
    //   92: goto -> 191
    //   95: astore_0
    //   96: aload #5
    //   98: monitorexit
    //   99: aload_0
    //   100: athrow
    //   101: new java/lang/StringBuilder
    //   104: dup
    //   105: invokespecial <init> : ()V
    //   108: astore #5
    //   110: aload #5
    //   112: ldc_w 'MEDIA_DRM_INFO msg.obj of unexpected type '
    //   115: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   118: pop
    //   119: aload #5
    //   121: aload #4
    //   123: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: ldc 'MediaPlayer'
    //   129: aload #5
    //   131: invokevirtual toString : ()Ljava/lang/String;
    //   134: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   137: pop
    //   138: goto -> 191
    //   141: iload_2
    //   142: iconst_2
    //   143: if_icmpne -> 191
    //   146: new java/lang/Thread
    //   149: dup
    //   150: new android/media/MediaPlayer$6
    //   153: dup
    //   154: aload_0
    //   155: invokespecial <init> : (Landroid/media/MediaPlayer;)V
    //   158: invokespecial <init> : (Ljava/lang/Runnable;)V
    //   161: astore #5
    //   163: aload #5
    //   165: invokevirtual start : ()V
    //   168: invokestatic yield : ()V
    //   171: goto -> 191
    //   174: aload_0
    //   175: getfield mDrmLock : Ljava/lang/Object;
    //   178: astore #5
    //   180: aload #5
    //   182: monitorenter
    //   183: aload_0
    //   184: iconst_1
    //   185: putfield mDrmInfoResolved : Z
    //   188: aload #5
    //   190: monitorexit
    //   191: aload_0
    //   192: getfield mEventHandler : Landroid/media/MediaPlayer$EventHandler;
    //   195: astore #5
    //   197: aload #5
    //   199: ifnull -> 224
    //   202: aload #5
    //   204: iload_1
    //   205: iload_2
    //   206: iload_3
    //   207: aload #4
    //   209: invokevirtual obtainMessage : (IIILjava/lang/Object;)Landroid/os/Message;
    //   212: astore #4
    //   214: aload_0
    //   215: getfield mEventHandler : Landroid/media/MediaPlayer$EventHandler;
    //   218: aload #4
    //   220: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   223: pop
    //   224: return
    //   225: astore_0
    //   226: aload #5
    //   228: monitorexit
    //   229: aload_0
    //   230: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3737	-> 0
    //   #3738	-> 11
    //   #3739	-> 15
    //   #3742	-> 16
    //   #3761	-> 38
    //   #3762	-> 47
    //   #3763	-> 55
    //   #3764	-> 62
    //   #3765	-> 74
    //   #3766	-> 83
    //   #3767	-> 89
    //   #3768	-> 92
    //   #3767	-> 95
    //   #3769	-> 101
    //   #3771	-> 138
    //   #3744	-> 141
    //   #3745	-> 146
    //   #3751	-> 163
    //   #3752	-> 168
    //   #3778	-> 174
    //   #3779	-> 183
    //   #3780	-> 188
    //   #3785	-> 191
    //   #3786	-> 202
    //   #3787	-> 214
    //   #3789	-> 224
    //   #3780	-> 225
    // Exception table:
    //   from	to	target	type
    //   83	89	95	finally
    //   89	92	95	finally
    //   96	99	95	finally
    //   183	188	225	finally
    //   188	191	225	finally
    //   226	229	225	finally
  }
  
  public void setOnPreparedListener(OnPreparedListener paramOnPreparedListener) {
    this.mOnPreparedListener = paramOnPreparedListener;
  }
  
  public void setOnCompletionListener(OnCompletionListener paramOnCompletionListener) {
    this.mOnCompletionListener = paramOnCompletionListener;
  }
  
  public void setOnBufferingUpdateListener(OnBufferingUpdateListener paramOnBufferingUpdateListener) {
    this.mOnBufferingUpdateListener = paramOnBufferingUpdateListener;
  }
  
  public void setOnSeekCompleteListener(OnSeekCompleteListener paramOnSeekCompleteListener) {
    this.mOnSeekCompleteListener = paramOnSeekCompleteListener;
  }
  
  public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener paramOnVideoSizeChangedListener) {
    this.mOnVideoSizeChangedListener = paramOnVideoSizeChangedListener;
  }
  
  public void setOnTimedTextListener(OnTimedTextListener paramOnTimedTextListener) {
    this.mOnTimedTextListener = paramOnTimedTextListener;
  }
  
  public void setOnSubtitleDataListener(OnSubtitleDataListener paramOnSubtitleDataListener, Handler paramHandler) {
    if (paramOnSubtitleDataListener != null) {
      if (paramHandler != null) {
        setOnSubtitleDataListenerInt(paramOnSubtitleDataListener, paramHandler);
        return;
      } 
      throw new IllegalArgumentException("Illegal null handler");
    } 
    throw new IllegalArgumentException("Illegal null listener");
  }
  
  public void setOnSubtitleDataListener(OnSubtitleDataListener paramOnSubtitleDataListener) {
    if (paramOnSubtitleDataListener != null) {
      setOnSubtitleDataListenerInt(paramOnSubtitleDataListener, (Handler)null);
      return;
    } 
    throw new IllegalArgumentException("Illegal null listener");
  }
  
  public void clearOnSubtitleDataListener() {
    setOnSubtitleDataListenerInt((OnSubtitleDataListener)null, (Handler)null);
  }
  
  private void setOnSubtitleDataListenerInt(OnSubtitleDataListener paramOnSubtitleDataListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mExtSubtitleDataListener : Landroid/media/MediaPlayer$OnSubtitleDataListener;
    //   7: aload_0
    //   8: aload_2
    //   9: putfield mExtSubtitleDataHandler : Landroid/os/Handler;
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4047	-> 0
    //   #4048	-> 2
    //   #4049	-> 7
    //   #4050	-> 12
    //   #4051	-> 14
    //   #4050	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
    //   12	14	15	finally
    //   16	18	15	finally
  }
  
  public void setOnMediaTimeDiscontinuityListener(OnMediaTimeDiscontinuityListener paramOnMediaTimeDiscontinuityListener, Handler paramHandler) {
    if (paramOnMediaTimeDiscontinuityListener != null) {
      if (paramHandler != null) {
        setOnMediaTimeDiscontinuityListenerInt(paramOnMediaTimeDiscontinuityListener, paramHandler);
        return;
      } 
      throw new IllegalArgumentException("Illegal null handler");
    } 
    throw new IllegalArgumentException("Illegal null listener");
  }
  
  public void setOnMediaTimeDiscontinuityListener(OnMediaTimeDiscontinuityListener paramOnMediaTimeDiscontinuityListener) {
    if (paramOnMediaTimeDiscontinuityListener != null) {
      setOnMediaTimeDiscontinuityListenerInt(paramOnMediaTimeDiscontinuityListener, (Handler)null);
      return;
    } 
    throw new IllegalArgumentException("Illegal null listener");
  }
  
  public void clearOnMediaTimeDiscontinuityListener() {
    setOnMediaTimeDiscontinuityListenerInt((OnMediaTimeDiscontinuityListener)null, (Handler)null);
  }
  
  private void setOnMediaTimeDiscontinuityListenerInt(OnMediaTimeDiscontinuityListener paramOnMediaTimeDiscontinuityListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mOnMediaTimeDiscontinuityListener : Landroid/media/MediaPlayer$OnMediaTimeDiscontinuityListener;
    //   7: aload_0
    //   8: aload_2
    //   9: putfield mOnMediaTimeDiscontinuityHandler : Landroid/os/Handler;
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4129	-> 0
    //   #4130	-> 2
    //   #4131	-> 7
    //   #4132	-> 12
    //   #4133	-> 14
    //   #4132	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
    //   12	14	15	finally
    //   16	18	15	finally
  }
  
  public void setOnTimedMetaDataAvailableListener(OnTimedMetaDataAvailableListener paramOnTimedMetaDataAvailableListener) {
    this.mOnTimedMetaDataAvailableListener = paramOnTimedMetaDataAvailableListener;
  }
  
  public void setOnErrorListener(OnErrorListener paramOnErrorListener) {
    this.mOnErrorListener = paramOnErrorListener;
  }
  
  public void setOnInfoListener(OnInfoListener paramOnInfoListener) {
    this.mOnInfoListener = paramOnInfoListener;
  }
  
  public void setOnDrmConfigHelper(OnDrmConfigHelper paramOnDrmConfigHelper) {
    synchronized (this.mDrmLock) {
      this.mOnDrmConfigHelper = paramOnDrmConfigHelper;
      return;
    } 
  }
  
  public void setOnDrmInfoListener(OnDrmInfoListener paramOnDrmInfoListener) {
    setOnDrmInfoListener(paramOnDrmInfoListener, (Handler)null);
  }
  
  public void setOnDrmInfoListener(OnDrmInfoListener paramOnDrmInfoListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDrmLock : Ljava/lang/Object;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 34
    //   11: new android/media/MediaPlayer$OnDrmInfoHandlerDelegate
    //   14: astore #4
    //   16: aload #4
    //   18: aload_0
    //   19: aload_0
    //   20: aload_1
    //   21: aload_2
    //   22: invokespecial <init> : (Landroid/media/MediaPlayer;Landroid/media/MediaPlayer;Landroid/media/MediaPlayer$OnDrmInfoListener;Landroid/os/Handler;)V
    //   25: aload_0
    //   26: aload #4
    //   28: putfield mOnDrmInfoHandlerDelegate : Landroid/media/MediaPlayer$OnDrmInfoHandlerDelegate;
    //   31: goto -> 39
    //   34: aload_0
    //   35: aconst_null
    //   36: putfield mOnDrmInfoHandlerDelegate : Landroid/media/MediaPlayer$OnDrmInfoHandlerDelegate;
    //   39: aload_3
    //   40: monitorexit
    //   41: return
    //   42: astore_1
    //   43: aload_3
    //   44: monitorexit
    //   45: aload_1
    //   46: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4484	-> 0
    //   #4485	-> 7
    //   #4486	-> 11
    //   #4488	-> 34
    //   #4490	-> 39
    //   #4491	-> 41
    //   #4490	-> 42
    // Exception table:
    //   from	to	target	type
    //   11	31	42	finally
    //   34	39	42	finally
    //   39	41	42	finally
    //   43	45	42	finally
  }
  
  public void setOnDrmPreparedListener(OnDrmPreparedListener paramOnDrmPreparedListener) {
    setOnDrmPreparedListener(paramOnDrmPreparedListener, (Handler)null);
  }
  
  public void setOnDrmPreparedListener(OnDrmPreparedListener paramOnDrmPreparedListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDrmLock : Ljava/lang/Object;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 34
    //   11: new android/media/MediaPlayer$OnDrmPreparedHandlerDelegate
    //   14: astore #4
    //   16: aload #4
    //   18: aload_0
    //   19: aload_0
    //   20: aload_1
    //   21: aload_2
    //   22: invokespecial <init> : (Landroid/media/MediaPlayer;Landroid/media/MediaPlayer;Landroid/media/MediaPlayer$OnDrmPreparedListener;Landroid/os/Handler;)V
    //   25: aload_0
    //   26: aload #4
    //   28: putfield mOnDrmPreparedHandlerDelegate : Landroid/media/MediaPlayer$OnDrmPreparedHandlerDelegate;
    //   31: goto -> 39
    //   34: aload_0
    //   35: aconst_null
    //   36: putfield mOnDrmPreparedHandlerDelegate : Landroid/media/MediaPlayer$OnDrmPreparedHandlerDelegate;
    //   39: aload_3
    //   40: monitorexit
    //   41: return
    //   42: astore_1
    //   43: aload_3
    //   44: monitorexit
    //   45: aload_1
    //   46: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4567	-> 0
    //   #4568	-> 7
    //   #4569	-> 11
    //   #4572	-> 34
    //   #4574	-> 39
    //   #4575	-> 41
    //   #4574	-> 42
    // Exception table:
    //   from	to	target	type
    //   11	31	42	finally
    //   34	39	42	finally
    //   39	41	42	finally
    //   43	45	42	finally
  }
  
  class OnDrmInfoHandlerDelegate {
    private Handler mHandler;
    
    private MediaPlayer mMediaPlayer;
    
    private MediaPlayer.OnDrmInfoListener mOnDrmInfoListener;
    
    final MediaPlayer this$0;
    
    OnDrmInfoHandlerDelegate(MediaPlayer param1MediaPlayer1, MediaPlayer.OnDrmInfoListener param1OnDrmInfoListener, Handler param1Handler) {
      this.mMediaPlayer = param1MediaPlayer1;
      this.mOnDrmInfoListener = param1OnDrmInfoListener;
      if (param1Handler != null)
        this.mHandler = param1Handler; 
    }
    
    void notifyClient(final MediaPlayer.DrmInfo drmInfo) {
      Handler handler = this.mHandler;
      if (handler != null) {
        handler.post(new Runnable() {
              final MediaPlayer.OnDrmInfoHandlerDelegate this$1;
              
              final MediaPlayer.DrmInfo val$drmInfo;
              
              public void run() {
                MediaPlayer.OnDrmInfoHandlerDelegate.this.mOnDrmInfoListener.onDrmInfo(MediaPlayer.OnDrmInfoHandlerDelegate.this.mMediaPlayer, drmInfo);
              }
            });
      } else {
        this.mOnDrmInfoListener.onDrmInfo(this.mMediaPlayer, drmInfo);
      } 
    }
  }
  
  class OnDrmPreparedHandlerDelegate {
    private Handler mHandler;
    
    private MediaPlayer mMediaPlayer;
    
    private MediaPlayer.OnDrmPreparedListener mOnDrmPreparedListener;
    
    final MediaPlayer this$0;
    
    OnDrmPreparedHandlerDelegate(MediaPlayer param1MediaPlayer1, MediaPlayer.OnDrmPreparedListener param1OnDrmPreparedListener, Handler param1Handler) {
      this.mMediaPlayer = param1MediaPlayer1;
      this.mOnDrmPreparedListener = param1OnDrmPreparedListener;
      if (param1Handler != null) {
        this.mHandler = param1Handler;
      } else if (MediaPlayer.this.mEventHandler != null) {
        this.mHandler = MediaPlayer.this.mEventHandler;
      } else {
        Log.e("MediaPlayer", "OnDrmPreparedHandlerDelegate: Unexpected null mEventHandler");
      } 
    }
    
    void notifyClient(final int status) {
      Handler handler = this.mHandler;
      if (handler != null) {
        handler.post(new Runnable() {
              final MediaPlayer.OnDrmPreparedHandlerDelegate this$1;
              
              final int val$status;
              
              public void run() {
                MediaPlayer.OnDrmPreparedHandlerDelegate.this.mOnDrmPreparedListener.onDrmPrepared(MediaPlayer.OnDrmPreparedHandlerDelegate.this.mMediaPlayer, status);
              }
            });
      } else {
        Log.e("MediaPlayer", "OnDrmPreparedHandlerDelegate:notifyClient: Unexpected null mHandler");
      } 
    }
  }
  
  public DrmInfo getDrmInfo() {
    null = null;
    synchronized (this.mDrmLock) {
      if (this.mDrmInfoResolved || this.mDrmInfo != null) {
        if (this.mDrmInfo != null)
          null = this.mDrmInfo.makeCopy(); 
        return null;
      } 
      Log.v("MediaPlayer", "The Player has not been prepared yet");
      IllegalStateException illegalStateException = new IllegalStateException();
      this("The Player has not been prepared yet");
      throw illegalStateException;
    } 
  }
  
  public void prepareDrm(UUID paramUUID) throws UnsupportedSchemeException, ResourceBusyException, ProvisioningNetworkErrorException, ProvisioningServerErrorException {
    // Byte code:
    //   0: new java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore_2
    //   8: aload_2
    //   9: ldc_w 'prepareDrm: uuid: '
    //   12: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   15: pop
    //   16: aload_2
    //   17: aload_1
    //   18: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   21: pop
    //   22: aload_2
    //   23: ldc_w ' mOnDrmConfigHelper: '
    //   26: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: pop
    //   30: aload_2
    //   31: aload_0
    //   32: getfield mOnDrmConfigHelper : Landroid/media/MediaPlayer$OnDrmConfigHelper;
    //   35: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: ldc 'MediaPlayer'
    //   41: aload_2
    //   42: invokevirtual toString : ()Ljava/lang/String;
    //   45: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   48: pop
    //   49: iconst_0
    //   50: istore_3
    //   51: iconst_0
    //   52: istore #4
    //   54: aload_0
    //   55: getfield mDrmLock : Ljava/lang/Object;
    //   58: astore_2
    //   59: aload_2
    //   60: monitorenter
    //   61: aload_0
    //   62: getfield mDrmInfo : Landroid/media/MediaPlayer$DrmInfo;
    //   65: ifnull -> 662
    //   68: aload_0
    //   69: getfield mActiveDrmScheme : Z
    //   72: ifne -> 611
    //   75: aload_0
    //   76: getfield mPrepareDrmInProgress : Z
    //   79: ifne -> 589
    //   82: aload_0
    //   83: getfield mDrmProvisioningInProgress : Z
    //   86: ifne -> 567
    //   89: aload_0
    //   90: invokespecial cleanDrmObj : ()V
    //   93: aload_0
    //   94: iconst_1
    //   95: putfield mPrepareDrmInProgress : Z
    //   98: aload_0
    //   99: getfield mOnDrmPreparedHandlerDelegate : Landroid/media/MediaPlayer$OnDrmPreparedHandlerDelegate;
    //   102: astore #5
    //   104: aload_0
    //   105: aload_1
    //   106: invokespecial prepareDrm_createDrmStep : (Ljava/util/UUID;)V
    //   109: aload_0
    //   110: iconst_1
    //   111: putfield mDrmConfigAllowed : Z
    //   114: aload_2
    //   115: monitorexit
    //   116: aload_0
    //   117: getfield mOnDrmConfigHelper : Landroid/media/MediaPlayer$OnDrmConfigHelper;
    //   120: astore_2
    //   121: aload_2
    //   122: ifnull -> 132
    //   125: aload_2
    //   126: aload_0
    //   127: invokeinterface onDrmConfig : (Landroid/media/MediaPlayer;)V
    //   132: aload_0
    //   133: getfield mDrmLock : Ljava/lang/Object;
    //   136: astore_2
    //   137: aload_2
    //   138: monitorenter
    //   139: aload_0
    //   140: iconst_0
    //   141: putfield mDrmConfigAllowed : Z
    //   144: iconst_0
    //   145: istore #6
    //   147: iload #6
    //   149: istore #7
    //   151: aload_0
    //   152: aload_1
    //   153: invokespecial prepareDrm_openSessionStep : (Ljava/util/UUID;)V
    //   156: iload #6
    //   158: istore #7
    //   160: aload_0
    //   161: aload_1
    //   162: putfield mDrmUUID : Ljava/util/UUID;
    //   165: iload #6
    //   167: istore #7
    //   169: aload_0
    //   170: iconst_1
    //   171: putfield mActiveDrmScheme : Z
    //   174: iconst_1
    //   175: istore #7
    //   177: iconst_1
    //   178: istore #4
    //   180: aload_0
    //   181: getfield mDrmProvisioningInProgress : Z
    //   184: ifne -> 192
    //   187: aload_0
    //   188: iconst_0
    //   189: putfield mPrepareDrmInProgress : Z
    //   192: iconst_0
    //   193: ifeq -> 460
    //   196: iload #4
    //   198: istore #7
    //   200: aload_0
    //   201: invokespecial cleanDrmObj : ()V
    //   204: goto -> 460
    //   207: astore_1
    //   208: goto -> 521
    //   211: astore_1
    //   212: iload #6
    //   214: istore #7
    //   216: new java/lang/StringBuilder
    //   219: astore #5
    //   221: iload #6
    //   223: istore #7
    //   225: aload #5
    //   227: invokespecial <init> : ()V
    //   230: iload #6
    //   232: istore #7
    //   234: aload #5
    //   236: ldc_w 'prepareDrm: Exception '
    //   239: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: iload #6
    //   245: istore #7
    //   247: aload #5
    //   249: aload_1
    //   250: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   253: pop
    //   254: iload #6
    //   256: istore #7
    //   258: ldc 'MediaPlayer'
    //   260: aload #5
    //   262: invokevirtual toString : ()Ljava/lang/String;
    //   265: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   268: pop
    //   269: iconst_1
    //   270: istore #7
    //   272: aload_1
    //   273: athrow
    //   274: astore #8
    //   276: iload #6
    //   278: istore #7
    //   280: ldc 'MediaPlayer'
    //   282: ldc_w 'prepareDrm: NotProvisionedException'
    //   285: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   288: pop
    //   289: iload #6
    //   291: istore #7
    //   293: aload_0
    //   294: aload_1
    //   295: invokespecial HandleProvisioninig : (Ljava/util/UUID;)I
    //   298: istore #6
    //   300: iload #6
    //   302: ifeq -> 434
    //   305: iconst_1
    //   306: istore #4
    //   308: iload #6
    //   310: iconst_1
    //   311: if_icmpeq -> 396
    //   314: iload #6
    //   316: iconst_2
    //   317: if_icmpeq -> 358
    //   320: iload #4
    //   322: istore #7
    //   324: ldc 'MediaPlayer'
    //   326: ldc_w 'prepareDrm: Post-provisioning preparation failed.'
    //   329: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   332: pop
    //   333: iload #4
    //   335: istore #7
    //   337: new java/lang/IllegalStateException
    //   340: astore_1
    //   341: iload #4
    //   343: istore #7
    //   345: aload_1
    //   346: ldc_w 'prepareDrm: Post-provisioning preparation failed.'
    //   349: invokespecial <init> : (Ljava/lang/String;)V
    //   352: iload #4
    //   354: istore #7
    //   356: aload_1
    //   357: athrow
    //   358: iload #4
    //   360: istore #7
    //   362: ldc 'MediaPlayer'
    //   364: ldc_w 'prepareDrm: Provisioning was required but the request was denied by the server.'
    //   367: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   370: pop
    //   371: iload #4
    //   373: istore #7
    //   375: new android/media/MediaPlayer$ProvisioningServerErrorException
    //   378: astore_1
    //   379: iload #4
    //   381: istore #7
    //   383: aload_1
    //   384: ldc_w 'prepareDrm: Provisioning was required but the request was denied by the server.'
    //   387: invokespecial <init> : (Ljava/lang/String;)V
    //   390: iload #4
    //   392: istore #7
    //   394: aload_1
    //   395: athrow
    //   396: iload #4
    //   398: istore #7
    //   400: ldc 'MediaPlayer'
    //   402: ldc_w 'prepareDrm: Provisioning was required but failed due to a network error.'
    //   405: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   408: pop
    //   409: iload #4
    //   411: istore #7
    //   413: new android/media/MediaPlayer$ProvisioningNetworkErrorException
    //   416: astore_1
    //   417: iload #4
    //   419: istore #7
    //   421: aload_1
    //   422: ldc_w 'prepareDrm: Provisioning was required but failed due to a network error.'
    //   425: invokespecial <init> : (Ljava/lang/String;)V
    //   428: iload #4
    //   430: istore #7
    //   432: aload_1
    //   433: athrow
    //   434: aload_0
    //   435: getfield mDrmProvisioningInProgress : Z
    //   438: ifne -> 446
    //   441: aload_0
    //   442: iconst_0
    //   443: putfield mPrepareDrmInProgress : Z
    //   446: iload_3
    //   447: istore #7
    //   449: iconst_0
    //   450: ifeq -> 460
    //   453: iload #4
    //   455: istore #7
    //   457: goto -> 200
    //   460: aload_2
    //   461: monitorexit
    //   462: iload #7
    //   464: ifeq -> 478
    //   467: aload #5
    //   469: ifnull -> 478
    //   472: aload #5
    //   474: iconst_0
    //   475: invokevirtual notifyClient : (I)V
    //   478: return
    //   479: astore_1
    //   480: iload #6
    //   482: istore #7
    //   484: ldc 'MediaPlayer'
    //   486: ldc_w 'prepareDrm(): Wrong usage: The player must be in the prepared state to call prepareDrm().'
    //   489: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   492: pop
    //   493: iconst_1
    //   494: istore #4
    //   496: iload #4
    //   498: istore #7
    //   500: new java/lang/IllegalStateException
    //   503: astore_1
    //   504: iload #4
    //   506: istore #7
    //   508: aload_1
    //   509: ldc_w 'prepareDrm(): Wrong usage: The player must be in the prepared state to call prepareDrm().'
    //   512: invokespecial <init> : (Ljava/lang/String;)V
    //   515: iload #4
    //   517: istore #7
    //   519: aload_1
    //   520: athrow
    //   521: aload_0
    //   522: getfield mDrmProvisioningInProgress : Z
    //   525: ifne -> 533
    //   528: aload_0
    //   529: iconst_0
    //   530: putfield mPrepareDrmInProgress : Z
    //   533: iload #7
    //   535: ifeq -> 542
    //   538: aload_0
    //   539: invokespecial cleanDrmObj : ()V
    //   542: aload_1
    //   543: athrow
    //   544: astore_1
    //   545: aload_2
    //   546: monitorexit
    //   547: aload_1
    //   548: athrow
    //   549: astore_1
    //   550: ldc 'MediaPlayer'
    //   552: ldc_w 'prepareDrm(): Exception '
    //   555: aload_1
    //   556: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   559: pop
    //   560: aload_0
    //   561: iconst_0
    //   562: putfield mPrepareDrmInProgress : Z
    //   565: aload_1
    //   566: athrow
    //   567: ldc 'MediaPlayer'
    //   569: ldc_w 'prepareDrm(): Unexpectd: Provisioning is already in progress.'
    //   572: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   575: pop
    //   576: new java/lang/IllegalStateException
    //   579: astore_1
    //   580: aload_1
    //   581: ldc_w 'prepareDrm(): Unexpectd: Provisioning is already in progress.'
    //   584: invokespecial <init> : (Ljava/lang/String;)V
    //   587: aload_1
    //   588: athrow
    //   589: ldc 'MediaPlayer'
    //   591: ldc_w 'prepareDrm(): Wrong usage: There is already a pending prepareDrm call.'
    //   594: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   597: pop
    //   598: new java/lang/IllegalStateException
    //   601: astore_1
    //   602: aload_1
    //   603: ldc_w 'prepareDrm(): Wrong usage: There is already a pending prepareDrm call.'
    //   606: invokespecial <init> : (Ljava/lang/String;)V
    //   609: aload_1
    //   610: athrow
    //   611: new java/lang/StringBuilder
    //   614: astore_1
    //   615: aload_1
    //   616: invokespecial <init> : ()V
    //   619: aload_1
    //   620: ldc_w 'prepareDrm(): Wrong usage: There is already an active DRM scheme with '
    //   623: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   626: pop
    //   627: aload_1
    //   628: aload_0
    //   629: getfield mDrmUUID : Ljava/util/UUID;
    //   632: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   635: pop
    //   636: aload_1
    //   637: invokevirtual toString : ()Ljava/lang/String;
    //   640: astore_1
    //   641: ldc 'MediaPlayer'
    //   643: aload_1
    //   644: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   647: pop
    //   648: new java/lang/IllegalStateException
    //   651: astore #5
    //   653: aload #5
    //   655: aload_1
    //   656: invokespecial <init> : (Ljava/lang/String;)V
    //   659: aload #5
    //   661: athrow
    //   662: ldc 'MediaPlayer'
    //   664: ldc_w 'prepareDrm(): Wrong usage: The player must be prepared and DRM info be retrieved before this call.'
    //   667: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   670: pop
    //   671: new java/lang/IllegalStateException
    //   674: astore_1
    //   675: aload_1
    //   676: ldc_w 'prepareDrm(): Wrong usage: The player must be prepared and DRM info be retrieved before this call.'
    //   679: invokespecial <init> : (Ljava/lang/String;)V
    //   682: aload_1
    //   683: athrow
    //   684: astore_1
    //   685: aload_2
    //   686: monitorexit
    //   687: aload_1
    //   688: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4719	-> 0
    //   #4721	-> 49
    //   #4723	-> 54
    //   #4725	-> 54
    //   #4728	-> 61
    //   #4735	-> 68
    //   #4742	-> 75
    //   #4749	-> 82
    //   #4756	-> 89
    //   #4758	-> 93
    //   #4760	-> 98
    //   #4764	-> 104
    //   #4769	-> 109
    //   #4771	-> 109
    //   #4772	-> 114
    //   #4776	-> 116
    //   #4777	-> 125
    //   #4780	-> 132
    //   #4781	-> 139
    //   #4782	-> 144
    //   #4785	-> 147
    //   #4787	-> 156
    //   #4788	-> 165
    //   #4790	-> 174
    //   #4836	-> 180
    //   #4837	-> 187
    //   #4839	-> 192
    //   #4840	-> 200
    //   #4836	-> 207
    //   #4831	-> 211
    //   #4832	-> 212
    //   #4833	-> 269
    //   #4834	-> 272
    //   #4797	-> 274
    //   #4798	-> 276
    //   #4801	-> 289
    //   #4805	-> 300
    //   #4806	-> 305
    //   #4809	-> 308
    //   #4824	-> 320
    //   #4825	-> 320
    //   #4826	-> 333
    //   #4817	-> 358
    //   #4819	-> 358
    //   #4820	-> 371
    //   #4811	-> 396
    //   #4813	-> 396
    //   #4814	-> 409
    //   #4836	-> 434
    //   #4837	-> 441
    //   #4839	-> 446
    //   #4840	-> 453
    //   #4843	-> 460
    //   #4847	-> 462
    //   #4848	-> 467
    //   #4849	-> 472
    //   #4852	-> 478
    //   #4791	-> 479
    //   #4792	-> 480
    //   #4794	-> 480
    //   #4795	-> 493
    //   #4796	-> 496
    //   #4836	-> 521
    //   #4837	-> 528
    //   #4839	-> 533
    //   #4840	-> 538
    //   #4842	-> 542
    //   #4843	-> 544
    //   #4765	-> 549
    //   #4766	-> 550
    //   #4767	-> 560
    //   #4768	-> 565
    //   #4750	-> 567
    //   #4751	-> 567
    //   #4752	-> 576
    //   #4743	-> 589
    //   #4745	-> 589
    //   #4746	-> 598
    //   #4736	-> 611
    //   #4738	-> 641
    //   #4739	-> 648
    //   #4729	-> 662
    //   #4731	-> 662
    //   #4732	-> 671
    //   #4772	-> 684
    // Exception table:
    //   from	to	target	type
    //   61	68	684	finally
    //   68	75	684	finally
    //   75	82	684	finally
    //   82	89	684	finally
    //   89	93	684	finally
    //   93	98	684	finally
    //   98	104	684	finally
    //   104	109	549	java/lang/Exception
    //   104	109	684	finally
    //   109	114	684	finally
    //   114	116	684	finally
    //   139	144	544	finally
    //   151	156	479	java/lang/IllegalStateException
    //   151	156	274	android/media/NotProvisionedException
    //   151	156	211	java/lang/Exception
    //   151	156	207	finally
    //   160	165	479	java/lang/IllegalStateException
    //   160	165	274	android/media/NotProvisionedException
    //   160	165	211	java/lang/Exception
    //   160	165	207	finally
    //   169	174	479	java/lang/IllegalStateException
    //   169	174	274	android/media/NotProvisionedException
    //   169	174	211	java/lang/Exception
    //   169	174	207	finally
    //   180	187	544	finally
    //   187	192	544	finally
    //   200	204	544	finally
    //   216	221	207	finally
    //   225	230	207	finally
    //   234	243	207	finally
    //   247	254	207	finally
    //   258	269	207	finally
    //   272	274	207	finally
    //   280	289	207	finally
    //   293	300	207	finally
    //   324	333	207	finally
    //   337	341	207	finally
    //   345	352	207	finally
    //   356	358	207	finally
    //   362	371	207	finally
    //   375	379	207	finally
    //   383	390	207	finally
    //   394	396	207	finally
    //   400	409	207	finally
    //   413	417	207	finally
    //   421	428	207	finally
    //   432	434	207	finally
    //   434	441	544	finally
    //   441	446	544	finally
    //   460	462	544	finally
    //   484	493	207	finally
    //   500	504	207	finally
    //   508	515	207	finally
    //   519	521	207	finally
    //   521	528	544	finally
    //   528	533	544	finally
    //   538	542	544	finally
    //   542	544	544	finally
    //   545	547	544	finally
    //   550	560	684	finally
    //   560	565	684	finally
    //   565	567	684	finally
    //   567	576	684	finally
    //   576	589	684	finally
    //   589	598	684	finally
    //   598	611	684	finally
    //   611	641	684	finally
    //   641	648	684	finally
    //   648	662	684	finally
    //   662	671	684	finally
    //   671	684	684	finally
    //   685	687	684	finally
  }
  
  public void releaseDrm() throws NoDrmSchemeException {
    Log.v("MediaPlayer", "releaseDrm:");
    synchronized (this.mDrmLock) {
      boolean bool = this.mActiveDrmScheme;
      if (bool)
        try {
          _releaseDrm();
          cleanDrmObj();
          this.mActiveDrmScheme = false;
          return;
        } catch (IllegalStateException illegalStateException) {
          Log.w("MediaPlayer", "releaseDrm: Exception ", illegalStateException);
          illegalStateException = new IllegalStateException();
          this("releaseDrm: The player is not in a valid state.");
          throw illegalStateException;
        } catch (Exception exception) {
          Log.e("MediaPlayer", "releaseDrm: Exception ", exception);
          return;
        }  
      Log.e("MediaPlayer", "releaseDrm(): No active DRM scheme to release.");
      NoDrmSchemeException noDrmSchemeException = new NoDrmSchemeException();
      this("releaseDrm: No active DRM scheme to release.");
      throw noDrmSchemeException;
    } 
  }
  
  public MediaDrm.KeyRequest getKeyRequest(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, String paramString, int paramInt, Map<String, String> paramMap) throws NoDrmSchemeException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getKeyRequest:  keySetId: ");
    stringBuilder.append(paramArrayOfbyte1);
    stringBuilder.append(" initData:");
    stringBuilder.append(paramArrayOfbyte2);
    stringBuilder.append(" mimeType: ");
    stringBuilder.append(paramString);
    stringBuilder.append(" keyType: ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" optionalParameters: ");
    stringBuilder.append(paramMap);
    Log.v("MediaPlayer", stringBuilder.toString());
    synchronized (this.mDrmLock) {
      boolean bool = this.mActiveDrmScheme;
      if (bool) {
        if (paramInt != 3)
          try {
            paramArrayOfbyte1 = this.mDrmSessionId;
          } catch (NotProvisionedException notProvisionedException) {
          
          } catch (Exception exception) {} 
        if (paramMap != null) {
          HashMap<Object, Object> hashMap2 = new HashMap<>();
          this((Map)paramMap);
          HashMap<Object, Object> hashMap1 = hashMap2;
        } else {
          paramMap = null;
        } 
        MediaDrm.KeyRequest keyRequest = this.mDrmObj.getKeyRequest((byte[])notProvisionedException, (byte[])exception, paramString, paramInt, (HashMap<String, String>)paramMap);
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("getKeyRequest:   --> request: ");
        stringBuilder1.append(keyRequest);
        Log.v("MediaPlayer", stringBuilder1.toString());
        return keyRequest;
      } 
      Log.e("MediaPlayer", "getKeyRequest NoDrmSchemeException");
      NoDrmSchemeException noDrmSchemeException = new NoDrmSchemeException();
      this("getKeyRequest: Has to set a DRM scheme first.");
      throw noDrmSchemeException;
    } 
  }
  
  public byte[] provideKeyResponse(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws NoDrmSchemeException, DeniedByServerException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("provideKeyResponse: keySetId: ");
    stringBuilder.append(paramArrayOfbyte1);
    stringBuilder.append(" response: ");
    stringBuilder.append(paramArrayOfbyte2);
    Log.v("MediaPlayer", stringBuilder.toString());
    synchronized (this.mDrmLock) {
      boolean bool = this.mActiveDrmScheme;
      if (bool) {
        NotProvisionedException notProvisionedException2;
        if (paramArrayOfbyte1 == null) {
          try {
            byte[] arrayOfByte1 = this.mDrmSessionId;
          } catch (NotProvisionedException notProvisionedException1) {
          
          } catch (Exception exception) {}
        } else {
          notProvisionedException2 = notProvisionedException1;
        } 
        byte[] arrayOfByte = this.mDrmObj.provideKeyResponse((byte[])notProvisionedException2, (byte[])exception);
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("provideKeyResponse: keySetId: ");
        stringBuilder1.append(notProvisionedException1);
        stringBuilder1.append(" response: ");
        stringBuilder1.append(exception);
        stringBuilder1.append(" --> ");
        stringBuilder1.append(arrayOfByte);
        Log.v("MediaPlayer", stringBuilder1.toString());
        return arrayOfByte;
      } 
      Log.e("MediaPlayer", "getKeyRequest NoDrmSchemeException");
      NoDrmSchemeException noDrmSchemeException = new NoDrmSchemeException();
      this("getKeyRequest: Has to set a DRM scheme first.");
      throw noDrmSchemeException;
    } 
  }
  
  public void restoreKeys(byte[] paramArrayOfbyte) throws NoDrmSchemeException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("restoreKeys: keySetId: ");
    stringBuilder.append(paramArrayOfbyte);
    Log.v("MediaPlayer", stringBuilder.toString());
    synchronized (this.mDrmLock) {
      boolean bool = this.mActiveDrmScheme;
      if (bool)
        try {
          this.mDrmObj.restoreKeys(this.mDrmSessionId, paramArrayOfbyte);
          return;
        } catch (Exception exception) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("restoreKeys Exception ");
          stringBuilder1.append(exception);
          Log.w("MediaPlayer", stringBuilder1.toString());
          throw exception;
        }  
      Log.w("MediaPlayer", "restoreKeys NoDrmSchemeException");
      NoDrmSchemeException noDrmSchemeException = new NoDrmSchemeException();
      this("restoreKeys: Has to set a DRM scheme first.");
      throw noDrmSchemeException;
    } 
  }
  
  public String getDrmPropertyString(String paramString) throws NoDrmSchemeException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getDrmPropertyString: propertyName: ");
    stringBuilder.append(paramString);
    Log.v("MediaPlayer", stringBuilder.toString());
    synchronized (this.mDrmLock) {
      if (this.mActiveDrmScheme || this.mDrmConfigAllowed)
        try {
          String str = this.mDrmObj.getPropertyString(paramString);
          null = new StringBuilder();
          null.append("getDrmPropertyString: propertyName: ");
          null.append(paramString);
          null.append(" --> value: ");
          null.append(str);
          Log.v("MediaPlayer", null.toString());
          return str;
        } catch (Exception exception) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("getDrmPropertyString Exception ");
          stringBuilder1.append(exception);
          Log.w("MediaPlayer", stringBuilder1.toString());
          throw exception;
        }  
      Log.w("MediaPlayer", "getDrmPropertyString NoDrmSchemeException");
      NoDrmSchemeException noDrmSchemeException = new NoDrmSchemeException();
      this("getDrmPropertyString: Has to prepareDrm() first.");
      throw noDrmSchemeException;
    } 
  }
  
  public void setDrmPropertyString(String paramString1, String paramString2) throws NoDrmSchemeException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setDrmPropertyString: propertyName: ");
    stringBuilder.append(paramString1);
    stringBuilder.append(" value: ");
    stringBuilder.append(paramString2);
    Log.v("MediaPlayer", stringBuilder.toString());
    synchronized (this.mDrmLock) {
      if (this.mActiveDrmScheme || this.mDrmConfigAllowed)
        try {
          this.mDrmObj.setPropertyString(paramString1, paramString2);
          return;
        } catch (Exception exception) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("setDrmPropertyString Exception ");
          stringBuilder1.append(exception);
          Log.w("MediaPlayer", stringBuilder1.toString());
          throw exception;
        }  
      Log.w("MediaPlayer", "setDrmPropertyString NoDrmSchemeException");
      NoDrmSchemeException noDrmSchemeException = new NoDrmSchemeException();
      this("setDrmPropertyString: Has to prepareDrm() first.");
      throw noDrmSchemeException;
    } 
  }
  
  class DrmInfo {
    private Map<UUID, byte[]> mapPssh;
    
    private UUID[] supportedSchemes;
    
    public Map<UUID, byte[]> getPssh() {
      return this.mapPssh;
    }
    
    public UUID[] getSupportedSchemes() {
      return this.supportedSchemes;
    }
    
    private DrmInfo(UUID[] param1ArrayOfUUID) {
      this.mapPssh = (Map<UUID, byte[]>)this$0;
      this.supportedSchemes = param1ArrayOfUUID;
    }
    
    private DrmInfo(MediaPlayer this$0) {
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append("DrmInfo(");
      stringBuilder3.append(this$0);
      stringBuilder3.append(") size ");
      stringBuilder3.append(this$0.dataSize());
      Log.v("MediaPlayer", stringBuilder3.toString());
      int i = this$0.readInt();
      byte[] arrayOfByte = new byte[i];
      this$0.readByteArray(arrayOfByte);
      StringBuilder stringBuilder4 = new StringBuilder();
      stringBuilder4.append("DrmInfo() PSSH: ");
      stringBuilder4.append(arrToHex(arrayOfByte));
      Log.v("MediaPlayer", stringBuilder4.toString());
      this.mapPssh = parsePSSH(arrayOfByte, i);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("DrmInfo() PSSH: ");
      stringBuilder2.append(this.mapPssh);
      Log.v("MediaPlayer", stringBuilder2.toString());
      int j = this$0.readInt();
      this.supportedSchemes = new UUID[j];
      for (byte b = 0; b < j; b++) {
        byte[] arrayOfByte1 = new byte[16];
        this$0.readByteArray(arrayOfByte1);
        this.supportedSchemes[b] = bytesToUUID(arrayOfByte1);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DrmInfo() supportedScheme[");
        stringBuilder.append(b);
        stringBuilder.append("]: ");
        stringBuilder.append(this.supportedSchemes[b]);
        Log.v("MediaPlayer", stringBuilder.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("DrmInfo() Parcel psshsize: ");
      stringBuilder1.append(i);
      stringBuilder1.append(" supportedDRMsCount: ");
      stringBuilder1.append(j);
      Log.v("MediaPlayer", stringBuilder1.toString());
    }
    
    private DrmInfo makeCopy() {
      return new DrmInfo(this.supportedSchemes);
    }
    
    private String arrToHex(byte[] param1ArrayOfbyte) {
      String str = "0x";
      for (byte b = 0; b < param1ArrayOfbyte.length; b++) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(String.format("%02x", new Object[] { Byte.valueOf(param1ArrayOfbyte[b]) }));
        str = stringBuilder.toString();
      } 
      return str;
    }
    
    private UUID bytesToUUID(byte[] param1ArrayOfbyte) {
      long l1 = 0L, l2 = 0L;
      for (byte b = 0; b < 8; b++) {
        l1 |= (param1ArrayOfbyte[b] & 0xFFL) << (7 - b) * 8;
        l2 |= (param1ArrayOfbyte[b + 8] & 0xFFL) << (7 - b) * 8;
      } 
      return new UUID(l1, l2);
    }
    
    private Map<UUID, byte[]> parsePSSH(byte[] param1ArrayOfbyte, int param1Int) {
      HashMap<Object, Object> hashMap = new HashMap<>();
      int i = param1Int;
      byte b = 0;
      int j = 0;
      while (i > 0) {
        if (i < 16) {
          Log.w("MediaPlayer", String.format("parsePSSH: len is too short to parse UUID: (%d < 16) pssh: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(param1Int) }));
          return null;
        } 
        byte[] arrayOfByte1 = Arrays.copyOfRange(param1ArrayOfbyte, j, j + 16);
        UUID uUID = bytesToUUID(arrayOfByte1);
        int k = j + 16;
        j = i - 16;
        if (j < 4) {
          Log.w("MediaPlayer", String.format("parsePSSH: len is too short to parse datalen: (%d < 4) pssh: %d", new Object[] { Integer.valueOf(j), Integer.valueOf(param1Int) }));
          return null;
        } 
        byte[] arrayOfByte2 = Arrays.copyOfRange(param1ArrayOfbyte, k, k + 4);
        if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN) {
          i = (arrayOfByte2[3] & 0xFF) << 24 | (arrayOfByte2[2] & 0xFF) << 16 | (arrayOfByte2[1] & 0xFF) << 8 | arrayOfByte2[0] & 0xFF;
        } else {
          i = (arrayOfByte2[0] & 0xFF) << 24 | (arrayOfByte2[1] & 0xFF) << 16 | (arrayOfByte2[2] & 0xFF) << 8 | arrayOfByte2[3] & 0xFF;
        } 
        k += 4;
        int m = j - 4;
        if (m < i) {
          Log.w("MediaPlayer", String.format("parsePSSH: len is too short to parse data: (%d < %d) pssh: %d", new Object[] { Integer.valueOf(m), Integer.valueOf(i), Integer.valueOf(param1Int) }));
          return null;
        } 
        byte[] arrayOfByte3 = Arrays.copyOfRange(param1ArrayOfbyte, k, k + i);
        j = k + i;
        i = m - i;
        String str = arrToHex(arrayOfByte3);
        Log.v("MediaPlayer", String.format("parsePSSH[%d]: <%s, %s> pssh: %d", new Object[] { Integer.valueOf(b), uUID, str, Integer.valueOf(param1Int) }));
        b++;
        hashMap.put(uUID, arrayOfByte3);
      } 
      return (Map)hashMap;
    }
  }
  
  public static final class NoDrmSchemeException extends MediaDrmException {
    public NoDrmSchemeException(String param1String) {
      super(param1String);
    }
  }
  
  public static final class ProvisioningNetworkErrorException extends MediaDrmException {
    public ProvisioningNetworkErrorException(String param1String) {
      super(param1String);
    }
  }
  
  public static final class ProvisioningServerErrorException extends MediaDrmException {
    public ProvisioningServerErrorException(String param1String) {
      super(param1String);
    }
  }
  
  private void prepareDrm_createDrmStep(UUID paramUUID) throws UnsupportedSchemeException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("prepareDrm_createDrmStep: UUID: ");
    stringBuilder.append(paramUUID);
    Log.v("MediaPlayer", stringBuilder.toString());
    try {
      MediaDrm mediaDrm = new MediaDrm();
      this(paramUUID);
      this.mDrmObj = mediaDrm;
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("prepareDrm_createDrmStep: Created mDrmObj=");
      stringBuilder1.append(this.mDrmObj);
      Log.v("MediaPlayer", stringBuilder1.toString());
      return;
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("prepareDrm_createDrmStep: MediaDrm failed with ");
      stringBuilder1.append(exception);
      Log.e("MediaPlayer", stringBuilder1.toString());
      throw exception;
    } 
  }
  
  private void prepareDrm_openSessionStep(UUID paramUUID) throws NotProvisionedException, ResourceBusyException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("prepareDrm_openSessionStep: uuid: ");
    stringBuilder.append(paramUUID);
    Log.v("MediaPlayer", stringBuilder.toString());
    try {
      this.mDrmSessionId = this.mDrmObj.openSession();
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("prepareDrm_openSessionStep: mDrmSessionId=");
      stringBuilder.append(this.mDrmSessionId);
      Log.v("MediaPlayer", stringBuilder.toString());
      _prepareDrm(getByteArrayFromUUID(paramUUID), this.mDrmSessionId);
      Log.v("MediaPlayer", "prepareDrm_openSessionStep: _prepareDrm/Crypto succeeded");
      return;
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("prepareDrm_openSessionStep: open/crypto failed with ");
      stringBuilder1.append(exception);
      Log.e("MediaPlayer", stringBuilder1.toString());
      throw exception;
    } 
  }
  
  class ProvisioningThread extends Thread {
    public static final int TIMEOUT_MS = 60000;
    
    private Object drmLock;
    
    private boolean finished;
    
    private MediaPlayer mediaPlayer;
    
    private MediaPlayer.OnDrmPreparedHandlerDelegate onDrmPreparedHandlerDelegate;
    
    private int status;
    
    final MediaPlayer this$0;
    
    private String urlStr;
    
    private UUID uuid;
    
    private ProvisioningThread() {}
    
    public int status() {
      return this.status;
    }
    
    public ProvisioningThread initialize(MediaDrm.ProvisionRequest param1ProvisionRequest, UUID param1UUID, MediaPlayer param1MediaPlayer) {
      this.drmLock = param1MediaPlayer.mDrmLock;
      this.onDrmPreparedHandlerDelegate = param1MediaPlayer.mOnDrmPreparedHandlerDelegate;
      this.mediaPlayer = param1MediaPlayer;
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(param1ProvisionRequest.getDefaultUrl());
      stringBuilder2.append("&signedRequest=");
      stringBuilder2.append(new String(param1ProvisionRequest.getData()));
      this.urlStr = stringBuilder2.toString();
      this.uuid = param1UUID;
      this.status = 3;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("HandleProvisioninig: Thread is initialised url: ");
      stringBuilder1.append(this.urlStr);
      Log.v("MediaPlayer", stringBuilder1.toString());
      return this;
    }
    
    public void run() {
      // Byte code:
      //   0: aconst_null
      //   1: astore_1
      //   2: aconst_null
      //   3: astore_2
      //   4: aconst_null
      //   5: astore_3
      //   6: iconst_0
      //   7: istore #4
      //   9: aload_2
      //   10: astore #5
      //   12: new java/net/URL
      //   15: astore #6
      //   17: aload_2
      //   18: astore #5
      //   20: aload #6
      //   22: aload_0
      //   23: getfield urlStr : Ljava/lang/String;
      //   26: invokespecial <init> : (Ljava/lang/String;)V
      //   29: aload_2
      //   30: astore #5
      //   32: aload #6
      //   34: invokevirtual openConnection : ()Ljava/net/URLConnection;
      //   37: checkcast java/net/HttpURLConnection
      //   40: astore #7
      //   42: aload_3
      //   43: astore_2
      //   44: aload_1
      //   45: astore #5
      //   47: aload #7
      //   49: ldc 'POST'
      //   51: invokevirtual setRequestMethod : (Ljava/lang/String;)V
      //   54: aload_3
      //   55: astore_2
      //   56: aload_1
      //   57: astore #5
      //   59: aload #7
      //   61: iconst_0
      //   62: invokevirtual setDoOutput : (Z)V
      //   65: aload_3
      //   66: astore_2
      //   67: aload_1
      //   68: astore #5
      //   70: aload #7
      //   72: iconst_1
      //   73: invokevirtual setDoInput : (Z)V
      //   76: aload_3
      //   77: astore_2
      //   78: aload_1
      //   79: astore #5
      //   81: aload #7
      //   83: ldc 60000
      //   85: invokevirtual setConnectTimeout : (I)V
      //   88: aload_3
      //   89: astore_2
      //   90: aload_1
      //   91: astore #5
      //   93: aload #7
      //   95: ldc 60000
      //   97: invokevirtual setReadTimeout : (I)V
      //   100: aload_3
      //   101: astore_2
      //   102: aload_1
      //   103: astore #5
      //   105: aload #7
      //   107: invokevirtual connect : ()V
      //   110: aload_3
      //   111: astore_2
      //   112: aload_1
      //   113: astore #5
      //   115: aload #7
      //   117: invokevirtual getInputStream : ()Ljava/io/InputStream;
      //   120: invokestatic readFully : (Ljava/io/InputStream;)[B
      //   123: astore_3
      //   124: aload_3
      //   125: astore_2
      //   126: aload_3
      //   127: astore #5
      //   129: new java/lang/StringBuilder
      //   132: astore_1
      //   133: aload_3
      //   134: astore_2
      //   135: aload_3
      //   136: astore #5
      //   138: aload_1
      //   139: invokespecial <init> : ()V
      //   142: aload_3
      //   143: astore_2
      //   144: aload_3
      //   145: astore #5
      //   147: aload_1
      //   148: ldc 'HandleProvisioninig: Thread run: response '
      //   150: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   153: pop
      //   154: aload_3
      //   155: astore_2
      //   156: aload_3
      //   157: astore #5
      //   159: aload_1
      //   160: aload_3
      //   161: arraylength
      //   162: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   165: pop
      //   166: aload_3
      //   167: astore_2
      //   168: aload_3
      //   169: astore #5
      //   171: aload_1
      //   172: ldc ' '
      //   174: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   177: pop
      //   178: aload_3
      //   179: astore_2
      //   180: aload_3
      //   181: astore #5
      //   183: aload_1
      //   184: aload_3
      //   185: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   188: pop
      //   189: aload_3
      //   190: astore_2
      //   191: aload_3
      //   192: astore #5
      //   194: ldc 'MediaPlayer'
      //   196: aload_1
      //   197: invokevirtual toString : ()Ljava/lang/String;
      //   200: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   203: pop
      //   204: aload_3
      //   205: astore_2
      //   206: goto -> 291
      //   209: astore_3
      //   210: goto -> 302
      //   213: astore_1
      //   214: aload #5
      //   216: astore_2
      //   217: aload_0
      //   218: iconst_1
      //   219: putfield status : I
      //   222: aload #5
      //   224: astore_2
      //   225: new java/lang/StringBuilder
      //   228: astore_3
      //   229: aload #5
      //   231: astore_2
      //   232: aload_3
      //   233: invokespecial <init> : ()V
      //   236: aload #5
      //   238: astore_2
      //   239: aload_3
      //   240: ldc 'HandleProvisioninig: Thread run: connect '
      //   242: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   245: pop
      //   246: aload #5
      //   248: astore_2
      //   249: aload_3
      //   250: aload_1
      //   251: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   254: pop
      //   255: aload #5
      //   257: astore_2
      //   258: aload_3
      //   259: ldc ' url: '
      //   261: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   264: pop
      //   265: aload #5
      //   267: astore_2
      //   268: aload_3
      //   269: aload #6
      //   271: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   274: pop
      //   275: aload #5
      //   277: astore_2
      //   278: ldc 'MediaPlayer'
      //   280: aload_3
      //   281: invokevirtual toString : ()Ljava/lang/String;
      //   284: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   287: pop
      //   288: aload #5
      //   290: astore_2
      //   291: aload_2
      //   292: astore #5
      //   294: aload #7
      //   296: invokevirtual disconnect : ()V
      //   299: goto -> 355
      //   302: aload_2
      //   303: astore #5
      //   305: aload #7
      //   307: invokevirtual disconnect : ()V
      //   310: aload_2
      //   311: astore #5
      //   313: aload_3
      //   314: athrow
      //   315: astore_2
      //   316: aload_0
      //   317: iconst_1
      //   318: putfield status : I
      //   321: new java/lang/StringBuilder
      //   324: dup
      //   325: invokespecial <init> : ()V
      //   328: astore_3
      //   329: aload_3
      //   330: ldc 'HandleProvisioninig: Thread run: openConnection '
      //   332: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   335: pop
      //   336: aload_3
      //   337: aload_2
      //   338: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   341: pop
      //   342: ldc 'MediaPlayer'
      //   344: aload_3
      //   345: invokevirtual toString : ()Ljava/lang/String;
      //   348: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   351: pop
      //   352: aload #5
      //   354: astore_2
      //   355: iload #4
      //   357: istore #8
      //   359: aload_2
      //   360: ifnull -> 431
      //   363: aload_0
      //   364: getfield this$0 : Landroid/media/MediaPlayer;
      //   367: invokestatic access$3900 : (Landroid/media/MediaPlayer;)Landroid/media/MediaDrm;
      //   370: aload_2
      //   371: invokevirtual provideProvisionResponse : ([B)V
      //   374: ldc 'MediaPlayer'
      //   376: ldc 'HandleProvisioninig: Thread run: provideProvisionResponse SUCCEEDED!'
      //   378: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   381: pop
      //   382: iconst_1
      //   383: istore #8
      //   385: goto -> 431
      //   388: astore #5
      //   390: aload_0
      //   391: iconst_2
      //   392: putfield status : I
      //   395: new java/lang/StringBuilder
      //   398: dup
      //   399: invokespecial <init> : ()V
      //   402: astore_2
      //   403: aload_2
      //   404: ldc 'HandleProvisioninig: Thread run: provideProvisionResponse '
      //   406: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   409: pop
      //   410: aload_2
      //   411: aload #5
      //   413: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   416: pop
      //   417: ldc 'MediaPlayer'
      //   419: aload_2
      //   420: invokevirtual toString : ()Ljava/lang/String;
      //   423: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   426: pop
      //   427: iload #4
      //   429: istore #8
      //   431: iconst_0
      //   432: istore #9
      //   434: iconst_0
      //   435: istore #10
      //   437: aload_0
      //   438: getfield onDrmPreparedHandlerDelegate : Landroid/media/MediaPlayer$OnDrmPreparedHandlerDelegate;
      //   441: astore_2
      //   442: iconst_3
      //   443: istore #4
      //   445: aload_2
      //   446: ifnull -> 548
      //   449: aload_0
      //   450: getfield drmLock : Ljava/lang/Object;
      //   453: astore_2
      //   454: aload_2
      //   455: monitorenter
      //   456: iload #10
      //   458: istore #9
      //   460: iload #8
      //   462: ifeq -> 495
      //   465: aload_0
      //   466: getfield mediaPlayer : Landroid/media/MediaPlayer;
      //   469: aload_0
      //   470: getfield uuid : Ljava/util/UUID;
      //   473: invokestatic access$4000 : (Landroid/media/MediaPlayer;Ljava/util/UUID;)Z
      //   476: istore #9
      //   478: iload #9
      //   480: ifeq -> 489
      //   483: iconst_0
      //   484: istore #4
      //   486: goto -> 489
      //   489: aload_0
      //   490: iload #4
      //   492: putfield status : I
      //   495: aload_0
      //   496: getfield mediaPlayer : Landroid/media/MediaPlayer;
      //   499: iconst_0
      //   500: invokestatic access$4102 : (Landroid/media/MediaPlayer;Z)Z
      //   503: pop
      //   504: aload_0
      //   505: getfield mediaPlayer : Landroid/media/MediaPlayer;
      //   508: iconst_0
      //   509: invokestatic access$4202 : (Landroid/media/MediaPlayer;Z)Z
      //   512: pop
      //   513: iload #9
      //   515: ifne -> 525
      //   518: aload_0
      //   519: getfield this$0 : Landroid/media/MediaPlayer;
      //   522: invokestatic access$4300 : (Landroid/media/MediaPlayer;)V
      //   525: aload_2
      //   526: monitorexit
      //   527: aload_0
      //   528: getfield onDrmPreparedHandlerDelegate : Landroid/media/MediaPlayer$OnDrmPreparedHandlerDelegate;
      //   531: aload_0
      //   532: getfield status : I
      //   535: invokevirtual notifyClient : (I)V
      //   538: goto -> 613
      //   541: astore #5
      //   543: aload_2
      //   544: monitorexit
      //   545: aload #5
      //   547: athrow
      //   548: iload #8
      //   550: ifeq -> 583
      //   553: aload_0
      //   554: getfield mediaPlayer : Landroid/media/MediaPlayer;
      //   557: aload_0
      //   558: getfield uuid : Ljava/util/UUID;
      //   561: invokestatic access$4000 : (Landroid/media/MediaPlayer;Ljava/util/UUID;)Z
      //   564: istore #9
      //   566: iload #9
      //   568: ifeq -> 577
      //   571: iconst_0
      //   572: istore #4
      //   574: goto -> 577
      //   577: aload_0
      //   578: iload #4
      //   580: putfield status : I
      //   583: aload_0
      //   584: getfield mediaPlayer : Landroid/media/MediaPlayer;
      //   587: iconst_0
      //   588: invokestatic access$4102 : (Landroid/media/MediaPlayer;Z)Z
      //   591: pop
      //   592: aload_0
      //   593: getfield mediaPlayer : Landroid/media/MediaPlayer;
      //   596: iconst_0
      //   597: invokestatic access$4202 : (Landroid/media/MediaPlayer;Z)Z
      //   600: pop
      //   601: iload #9
      //   603: ifne -> 613
      //   606: aload_0
      //   607: getfield this$0 : Landroid/media/MediaPlayer;
      //   610: invokestatic access$4300 : (Landroid/media/MediaPlayer;)V
      //   613: aload_0
      //   614: iconst_1
      //   615: putfield finished : Z
      //   618: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5380	-> 0
      //   #5381	-> 6
      //   #5383	-> 9
      //   #5384	-> 29
      //   #5386	-> 42
      //   #5387	-> 54
      //   #5388	-> 65
      //   #5389	-> 76
      //   #5390	-> 88
      //   #5392	-> 100
      //   #5393	-> 110
      //   #5395	-> 124
      //   #5401	-> 209
      //   #5397	-> 213
      //   #5398	-> 214
      //   #5399	-> 222
      //   #5401	-> 291
      //   #5402	-> 299
      //   #5406	-> 299
      //   #5401	-> 302
      //   #5402	-> 310
      //   #5403	-> 315
      //   #5404	-> 316
      //   #5405	-> 321
      //   #5408	-> 355
      //   #5410	-> 363
      //   #5411	-> 374
      //   #5414	-> 382
      //   #5419	-> 385
      //   #5415	-> 388
      //   #5416	-> 390
      //   #5417	-> 395
      //   #5422	-> 431
      //   #5425	-> 437
      //   #5427	-> 449
      //   #5429	-> 456
      //   #5430	-> 465
      //   #5431	-> 478
      //   #5432	-> 483
      //   #5433	-> 489
      //   #5435	-> 495
      //   #5436	-> 504
      //   #5437	-> 513
      //   #5438	-> 518
      //   #5440	-> 525
      //   #5443	-> 527
      //   #5440	-> 541
      //   #5447	-> 548
      //   #5448	-> 553
      //   #5449	-> 566
      //   #5450	-> 571
      //   #5451	-> 577
      //   #5453	-> 583
      //   #5454	-> 592
      //   #5455	-> 601
      //   #5456	-> 606
      //   #5460	-> 613
      //   #5461	-> 618
      // Exception table:
      //   from	to	target	type
      //   12	17	315	java/lang/Exception
      //   20	29	315	java/lang/Exception
      //   32	42	315	java/lang/Exception
      //   47	54	213	java/lang/Exception
      //   47	54	209	finally
      //   59	65	213	java/lang/Exception
      //   59	65	209	finally
      //   70	76	213	java/lang/Exception
      //   70	76	209	finally
      //   81	88	213	java/lang/Exception
      //   81	88	209	finally
      //   93	100	213	java/lang/Exception
      //   93	100	209	finally
      //   105	110	213	java/lang/Exception
      //   105	110	209	finally
      //   115	124	213	java/lang/Exception
      //   115	124	209	finally
      //   129	133	213	java/lang/Exception
      //   129	133	209	finally
      //   138	142	213	java/lang/Exception
      //   138	142	209	finally
      //   147	154	213	java/lang/Exception
      //   147	154	209	finally
      //   159	166	213	java/lang/Exception
      //   159	166	209	finally
      //   171	178	213	java/lang/Exception
      //   171	178	209	finally
      //   183	189	213	java/lang/Exception
      //   183	189	209	finally
      //   194	204	213	java/lang/Exception
      //   194	204	209	finally
      //   217	222	209	finally
      //   225	229	209	finally
      //   232	236	209	finally
      //   239	246	209	finally
      //   249	255	209	finally
      //   258	265	209	finally
      //   268	275	209	finally
      //   278	288	209	finally
      //   294	299	315	java/lang/Exception
      //   305	310	315	java/lang/Exception
      //   313	315	315	java/lang/Exception
      //   363	374	388	java/lang/Exception
      //   374	382	388	java/lang/Exception
      //   465	478	541	finally
      //   489	495	541	finally
      //   495	504	541	finally
      //   504	513	541	finally
      //   518	525	541	finally
      //   525	527	541	finally
      //   543	545	541	finally
    }
  }
  
  private int HandleProvisioninig(UUID paramUUID) {
    int i;
    if (this.mDrmProvisioningInProgress) {
      Log.e("MediaPlayer", "HandleProvisioninig: Unexpected mDrmProvisioningInProgress");
      return 3;
    } 
    MediaDrm.ProvisionRequest provisionRequest = this.mDrmObj.getProvisionRequest();
    if (provisionRequest == null) {
      Log.e("MediaPlayer", "HandleProvisioninig: getProvisionRequest returned null.");
      return 3;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("HandleProvisioninig provReq  data: ");
    stringBuilder.append(provisionRequest.getData());
    stringBuilder.append(" url: ");
    stringBuilder.append(provisionRequest.getDefaultUrl());
    String str = stringBuilder.toString();
    Log.v("MediaPlayer", str);
    this.mDrmProvisioningInProgress = true;
    ProvisioningThread provisioningThread = (new ProvisioningThread()).initialize(provisionRequest, paramUUID, this);
    provisioningThread.start();
    if (this.mOnDrmPreparedHandlerDelegate != null) {
      i = 0;
    } else {
      try {
        this.mDrmProvisioningThread.join();
      } catch (Exception exception) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("HandleProvisioninig: Thread.join Exception ");
        stringBuilder1.append(exception);
        Log.w("MediaPlayer", stringBuilder1.toString());
      } 
      i = this.mDrmProvisioningThread.status();
      this.mDrmProvisioningThread = null;
    } 
    return i;
  }
  
  private boolean resumePrepareDrm(UUID paramUUID) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("resumePrepareDrm: uuid: ");
    stringBuilder.append(paramUUID);
    Log.v("MediaPlayer", stringBuilder.toString());
    boolean bool = false;
    try {
      prepareDrm_openSessionStep(paramUUID);
      this.mDrmUUID = paramUUID;
      this.mActiveDrmScheme = true;
      bool = true;
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("HandleProvisioninig: Thread run _prepareDrm resume failed with ");
      stringBuilder1.append(exception);
      Log.w("MediaPlayer", stringBuilder1.toString());
    } 
    return bool;
  }
  
  private void resetDrmState() {
    synchronized (this.mDrmLock) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("resetDrmState:  mDrmInfo=");
      stringBuilder.append(this.mDrmInfo);
      stringBuilder.append(" mDrmProvisioningThread=");
      stringBuilder.append(this.mDrmProvisioningThread);
      stringBuilder.append(" mPrepareDrmInProgress=");
      stringBuilder.append(this.mPrepareDrmInProgress);
      stringBuilder.append(" mActiveDrmScheme=");
      stringBuilder.append(this.mActiveDrmScheme);
      Log.v("MediaPlayer", stringBuilder.toString());
      this.mDrmInfoResolved = false;
      this.mDrmInfo = null;
      ProvisioningThread provisioningThread = this.mDrmProvisioningThread;
      if (provisioningThread != null) {
        try {
          this.mDrmProvisioningThread.join();
        } catch (InterruptedException interruptedException) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("resetDrmState: ProvThread.join Exception ");
          stringBuilder1.append(interruptedException);
          Log.w("MediaPlayer", stringBuilder1.toString());
        } 
        this.mDrmProvisioningThread = null;
      } 
      this.mPrepareDrmInProgress = false;
      this.mActiveDrmScheme = false;
      cleanDrmObj();
      return;
    } 
  }
  
  private void cleanDrmObj() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("cleanDrmObj: mDrmObj=");
    stringBuilder.append(this.mDrmObj);
    stringBuilder.append(" mDrmSessionId=");
    stringBuilder.append(this.mDrmSessionId);
    Log.v("MediaPlayer", stringBuilder.toString());
    byte[] arrayOfByte = this.mDrmSessionId;
    if (arrayOfByte != null) {
      this.mDrmObj.closeSession(arrayOfByte);
      this.mDrmSessionId = null;
    } 
    MediaDrm mediaDrm = this.mDrmObj;
    if (mediaDrm != null) {
      mediaDrm.release();
      this.mDrmObj = null;
    } 
  }
  
  private static final byte[] getByteArrayFromUUID(UUID paramUUID) {
    long l1 = paramUUID.getMostSignificantBits();
    long l2 = paramUUID.getLeastSignificantBits();
    byte[] arrayOfByte = new byte[16];
    for (byte b = 0; b < 8; b++) {
      arrayOfByte[b] = (byte)(int)(l1 >>> (7 - b) * 8);
      arrayOfByte[b + 8] = (byte)(int)(l2 >>> (7 - b) * 8);
    } 
    return arrayOfByte;
  }
  
  private boolean isVideoScalingModeSupported(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 1)
      if (paramInt == 2) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  private native int _getAudioStreamType() throws IllegalStateException;
  
  private native void _notifyAt(long paramLong);
  
  private native void _pause() throws IllegalStateException;
  
  private native void _prepare() throws IOException, IllegalStateException;
  
  private native void _prepareDrm(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2);
  
  private native void _release();
  
  private native void _releaseDrm();
  
  private native void _reset();
  
  private final native void _seekTo(long paramLong, int paramInt);
  
  private native void _setAudioStreamType(int paramInt);
  
  private native void _setAuxEffectSendLevel(float paramFloat);
  
  private native void _setDataSource(MediaDataSource paramMediaDataSource) throws IllegalArgumentException, IllegalStateException;
  
  private native void _setDataSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2) throws IOException, IllegalArgumentException, IllegalStateException;
  
  private native void _setVideoSurface(Surface paramSurface);
  
  private native void _setVolume(float paramFloat1, float paramFloat2);
  
  private native void _start() throws IllegalStateException;
  
  private native void _stop() throws IllegalStateException;
  
  private native void nativeSetDataSource(IBinder paramIBinder, String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
  
  private native int native_applyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation);
  
  private final native void native_enableDeviceCallback(boolean paramBoolean);
  
  private final native void native_finalize();
  
  private final native boolean native_getMetadata(boolean paramBoolean1, boolean paramBoolean2, Parcel paramParcel);
  
  private native PersistableBundle native_getMetrics();
  
  private final native int native_getRoutedDeviceId();
  
  private native VolumeShaper.State native_getVolumeShaperState(int paramInt);
  
  private static final native void native_init();
  
  private final native int native_invoke(Parcel paramParcel1, Parcel paramParcel2);
  
  public static native int native_pullBatteryData(Parcel paramParcel);
  
  private final native int native_setMetadataFilter(Parcel paramParcel);
  
  private final native boolean native_setOutputDevice(int paramInt);
  
  private final native int native_setRetransmitEndpoint(String paramString, int paramInt);
  
  private final native void native_setup(Object paramObject);
  
  private native boolean setParameter(int paramInt, Parcel paramParcel);
  
  public native void attachAuxEffect(int paramInt);
  
  public native int getAudioSessionId();
  
  public native int getCurrentPosition();
  
  public native int getDuration();
  
  public native PlaybackParams getPlaybackParams();
  
  public native SyncParams getSyncParams();
  
  public native int getVideoHeight();
  
  public native int getVideoWidth();
  
  public native boolean isLooping();
  
  public native boolean isPlaying();
  
  public native void prepareAsync() throws IllegalStateException;
  
  public native void setAudioSessionId(int paramInt) throws IllegalArgumentException, IllegalStateException;
  
  public native void setLooping(boolean paramBoolean);
  
  public native void setNextMediaPlayer(MediaPlayer paramMediaPlayer);
  
  public native void setPlaybackParams(PlaybackParams paramPlaybackParams);
  
  public native void setSyncParams(SyncParams paramSyncParams);
  
  static class TimeProvider implements OnSeekCompleteListener, MediaTimeProvider {
    private long mLastTimeUs = 0L;
    
    private boolean mPaused = true;
    
    private boolean mStopped = true;
    
    private boolean mRefresh = false;
    
    private boolean mPausing = false;
    
    private boolean mSeeking = false;
    
    public boolean DEBUG = false;
    
    private static final long MAX_EARLY_CALLBACK_US = 1000L;
    
    private static final long MAX_NS_WITHOUT_POSITION_CHECK = 5000000000L;
    
    private static final int NOTIFY = 1;
    
    private static final int NOTIFY_SEEK = 3;
    
    private static final int NOTIFY_STOP = 2;
    
    private static final int NOTIFY_TIME = 0;
    
    private static final int NOTIFY_TRACK_DATA = 4;
    
    private static final String TAG = "MTP";
    
    private static final long TIME_ADJUSTMENT_RATE = 2L;
    
    private boolean mBuffering;
    
    private Handler mEventHandler;
    
    private HandlerThread mHandlerThread;
    
    private long mLastReportedTime;
    
    private MediaTimeProvider.OnMediaTimeListener[] mListeners;
    
    private MediaPlayer mPlayer;
    
    private long[] mTimes;
    
    public TimeProvider(MediaPlayer param1MediaPlayer) {
      this.mPlayer = param1MediaPlayer;
      try {
        getCurrentTimeUs(true, false);
      } catch (IllegalStateException illegalStateException) {
        this.mRefresh = true;
      } 
      Looper looper2 = Looper.myLooper(), looper1 = looper2;
      if (looper2 == null) {
        looper1 = looper2 = Looper.getMainLooper();
        if (looper2 == null) {
          HandlerThread handlerThread = new HandlerThread("MediaPlayerMTPEventThread", -2);
          handlerThread.start();
          looper1 = this.mHandlerThread.getLooper();
        } 
      } 
      this.mEventHandler = new EventHandler(looper1);
      this.mListeners = new MediaTimeProvider.OnMediaTimeListener[0];
      this.mTimes = new long[0];
      this.mLastTimeUs = 0L;
    }
    
    private void scheduleNotification(int param1Int, long param1Long) {
      if (this.mSeeking && param1Int == 0)
        return; 
      if (this.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("scheduleNotification ");
        stringBuilder.append(param1Int);
        stringBuilder.append(" in ");
        stringBuilder.append(param1Long);
        Log.v("MTP", stringBuilder.toString());
      } 
      this.mEventHandler.removeMessages(1);
      Message message = this.mEventHandler.obtainMessage(1, param1Int, 0);
      this.mEventHandler.sendMessageDelayed(message, (int)(param1Long / 1000L));
    }
    
    public void close() {
      this.mEventHandler.removeMessages(1);
      HandlerThread handlerThread = this.mHandlerThread;
      if (handlerThread != null) {
        handlerThread.quitSafely();
        this.mHandlerThread = null;
      } 
    }
    
    protected void finalize() {
      HandlerThread handlerThread = this.mHandlerThread;
      if (handlerThread != null)
        handlerThread.quitSafely(); 
    }
    
    public void onNotifyTime() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield DEBUG : Z
      //   6: ifeq -> 18
      //   9: ldc 'MTP'
      //   11: ldc_w 'onNotifyTime: '
      //   14: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   17: pop
      //   18: aload_0
      //   19: iconst_0
      //   20: lconst_0
      //   21: invokespecial scheduleNotification : (IJ)V
      //   24: aload_0
      //   25: monitorexit
      //   26: return
      //   27: astore_1
      //   28: aload_0
      //   29: monitorexit
      //   30: aload_1
      //   31: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5685	-> 0
      //   #5686	-> 2
      //   #5687	-> 18
      //   #5688	-> 24
      //   #5689	-> 26
      //   #5688	-> 27
      // Exception table:
      //   from	to	target	type
      //   2	18	27	finally
      //   18	24	27	finally
      //   24	26	27	finally
      //   28	30	27	finally
    }
    
    public void onPaused(boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield DEBUG : Z
      //   6: ifeq -> 41
      //   9: new java/lang/StringBuilder
      //   12: astore_2
      //   13: aload_2
      //   14: invokespecial <init> : ()V
      //   17: aload_2
      //   18: ldc_w 'onPaused: '
      //   21: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   24: pop
      //   25: aload_2
      //   26: iload_1
      //   27: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   30: pop
      //   31: ldc 'MTP'
      //   33: aload_2
      //   34: invokevirtual toString : ()Ljava/lang/String;
      //   37: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   40: pop
      //   41: aload_0
      //   42: getfield mStopped : Z
      //   45: ifeq -> 67
      //   48: aload_0
      //   49: iconst_0
      //   50: putfield mStopped : Z
      //   53: aload_0
      //   54: iconst_1
      //   55: putfield mSeeking : Z
      //   58: aload_0
      //   59: iconst_3
      //   60: lconst_0
      //   61: invokespecial scheduleNotification : (IJ)V
      //   64: goto -> 83
      //   67: aload_0
      //   68: iload_1
      //   69: putfield mPausing : Z
      //   72: aload_0
      //   73: iconst_0
      //   74: putfield mSeeking : Z
      //   77: aload_0
      //   78: iconst_0
      //   79: lconst_0
      //   80: invokespecial scheduleNotification : (IJ)V
      //   83: aload_0
      //   84: monitorexit
      //   85: return
      //   86: astore_2
      //   87: aload_0
      //   88: monitorexit
      //   89: aload_2
      //   90: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5693	-> 0
      //   #5694	-> 2
      //   #5695	-> 41
      //   #5696	-> 48
      //   #5697	-> 53
      //   #5698	-> 58
      //   #5700	-> 67
      //   #5701	-> 72
      //   #5702	-> 77
      //   #5704	-> 83
      //   #5705	-> 85
      //   #5704	-> 86
      // Exception table:
      //   from	to	target	type
      //   2	41	86	finally
      //   41	48	86	finally
      //   48	53	86	finally
      //   53	58	86	finally
      //   58	64	86	finally
      //   67	72	86	finally
      //   72	77	86	finally
      //   77	83	86	finally
      //   83	85	86	finally
      //   87	89	86	finally
    }
    
    public void onBuffering(boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield DEBUG : Z
      //   6: ifeq -> 41
      //   9: new java/lang/StringBuilder
      //   12: astore_2
      //   13: aload_2
      //   14: invokespecial <init> : ()V
      //   17: aload_2
      //   18: ldc_w 'onBuffering: '
      //   21: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   24: pop
      //   25: aload_2
      //   26: iload_1
      //   27: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   30: pop
      //   31: ldc 'MTP'
      //   33: aload_2
      //   34: invokevirtual toString : ()Ljava/lang/String;
      //   37: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   40: pop
      //   41: aload_0
      //   42: iload_1
      //   43: putfield mBuffering : Z
      //   46: aload_0
      //   47: iconst_0
      //   48: lconst_0
      //   49: invokespecial scheduleNotification : (IJ)V
      //   52: aload_0
      //   53: monitorexit
      //   54: return
      //   55: astore_2
      //   56: aload_0
      //   57: monitorexit
      //   58: aload_2
      //   59: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5709	-> 0
      //   #5710	-> 2
      //   #5711	-> 41
      //   #5712	-> 46
      //   #5713	-> 52
      //   #5714	-> 54
      //   #5713	-> 55
      // Exception table:
      //   from	to	target	type
      //   2	41	55	finally
      //   41	46	55	finally
      //   46	52	55	finally
      //   52	54	55	finally
      //   56	58	55	finally
    }
    
    public void onStopped() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield DEBUG : Z
      //   6: ifeq -> 18
      //   9: ldc 'MTP'
      //   11: ldc_w 'onStopped'
      //   14: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   17: pop
      //   18: aload_0
      //   19: iconst_1
      //   20: putfield mPaused : Z
      //   23: aload_0
      //   24: iconst_1
      //   25: putfield mStopped : Z
      //   28: aload_0
      //   29: iconst_0
      //   30: putfield mSeeking : Z
      //   33: aload_0
      //   34: iconst_0
      //   35: putfield mBuffering : Z
      //   38: aload_0
      //   39: iconst_2
      //   40: lconst_0
      //   41: invokespecial scheduleNotification : (IJ)V
      //   44: aload_0
      //   45: monitorexit
      //   46: return
      //   47: astore_1
      //   48: aload_0
      //   49: monitorexit
      //   50: aload_1
      //   51: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5718	-> 0
      //   #5719	-> 2
      //   #5720	-> 18
      //   #5721	-> 23
      //   #5722	-> 28
      //   #5723	-> 33
      //   #5724	-> 38
      //   #5725	-> 44
      //   #5726	-> 46
      //   #5725	-> 47
      // Exception table:
      //   from	to	target	type
      //   2	18	47	finally
      //   18	23	47	finally
      //   23	28	47	finally
      //   28	33	47	finally
      //   33	38	47	finally
      //   38	44	47	finally
      //   44	46	47	finally
      //   48	50	47	finally
    }
    
    public void onSeekComplete(MediaPlayer param1MediaPlayer) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iconst_0
      //   4: putfield mStopped : Z
      //   7: aload_0
      //   8: iconst_1
      //   9: putfield mSeeking : Z
      //   12: aload_0
      //   13: iconst_3
      //   14: lconst_0
      //   15: invokespecial scheduleNotification : (IJ)V
      //   18: aload_0
      //   19: monitorexit
      //   20: return
      //   21: astore_1
      //   22: aload_0
      //   23: monitorexit
      //   24: aload_1
      //   25: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5731	-> 0
      //   #5732	-> 2
      //   #5733	-> 7
      //   #5734	-> 12
      //   #5735	-> 18
      //   #5736	-> 20
      //   #5735	-> 21
      // Exception table:
      //   from	to	target	type
      //   2	7	21	finally
      //   7	12	21	finally
      //   12	18	21	finally
      //   18	20	21	finally
      //   22	24	21	finally
    }
    
    public void onNewPlayer() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mRefresh : Z
      //   4: ifeq -> 40
      //   7: aload_0
      //   8: monitorenter
      //   9: aload_0
      //   10: iconst_0
      //   11: putfield mStopped : Z
      //   14: aload_0
      //   15: iconst_1
      //   16: putfield mSeeking : Z
      //   19: aload_0
      //   20: iconst_0
      //   21: putfield mBuffering : Z
      //   24: aload_0
      //   25: iconst_3
      //   26: lconst_0
      //   27: invokespecial scheduleNotification : (IJ)V
      //   30: aload_0
      //   31: monitorexit
      //   32: goto -> 40
      //   35: astore_1
      //   36: aload_0
      //   37: monitorexit
      //   38: aload_1
      //   39: athrow
      //   40: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5740	-> 0
      //   #5741	-> 7
      //   #5742	-> 9
      //   #5743	-> 14
      //   #5744	-> 19
      //   #5745	-> 24
      //   #5746	-> 30
      //   #5748	-> 40
      // Exception table:
      //   from	to	target	type
      //   9	14	35	finally
      //   14	19	35	finally
      //   19	24	35	finally
      //   24	30	35	finally
      //   30	32	35	finally
      //   36	38	35	finally
    }
    
    private void notifySeek() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iconst_0
      //   4: putfield mSeeking : Z
      //   7: aload_0
      //   8: iconst_1
      //   9: iconst_0
      //   10: invokevirtual getCurrentTimeUs : (ZZ)J
      //   13: lstore_1
      //   14: aload_0
      //   15: getfield DEBUG : Z
      //   18: ifeq -> 52
      //   21: new java/lang/StringBuilder
      //   24: astore_3
      //   25: aload_3
      //   26: invokespecial <init> : ()V
      //   29: aload_3
      //   30: ldc 'onSeekComplete at '
      //   32: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   35: pop
      //   36: aload_3
      //   37: lload_1
      //   38: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   41: pop
      //   42: ldc 'MTP'
      //   44: aload_3
      //   45: invokevirtual toString : ()Ljava/lang/String;
      //   48: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   51: pop
      //   52: aload_0
      //   53: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   56: astore #4
      //   58: aload #4
      //   60: arraylength
      //   61: istore #5
      //   63: iconst_0
      //   64: istore #6
      //   66: iload #6
      //   68: iload #5
      //   70: if_icmpge -> 99
      //   73: aload #4
      //   75: iload #6
      //   77: aaload
      //   78: astore_3
      //   79: aload_3
      //   80: ifnonnull -> 86
      //   83: goto -> 99
      //   86: aload_3
      //   87: lload_1
      //   88: invokeinterface onSeek : (J)V
      //   93: iinc #6, 1
      //   96: goto -> 66
      //   99: goto -> 128
      //   102: astore_3
      //   103: aload_0
      //   104: getfield DEBUG : Z
      //   107: ifeq -> 118
      //   110: ldc 'MTP'
      //   112: ldc 'onSeekComplete but no player'
      //   114: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   117: pop
      //   118: aload_0
      //   119: iconst_1
      //   120: putfield mPausing : Z
      //   123: aload_0
      //   124: iconst_0
      //   125: invokespecial notifyTimedEvent : (Z)V
      //   128: aload_0
      //   129: monitorexit
      //   130: return
      //   131: astore_3
      //   132: aload_0
      //   133: monitorexit
      //   134: aload_3
      //   135: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5751	-> 2
      //   #5753	-> 7
      //   #5754	-> 14
      //   #5756	-> 52
      //   #5757	-> 79
      //   #5758	-> 83
      //   #5760	-> 86
      //   #5756	-> 93
      //   #5767	-> 99
      //   #5762	-> 102
      //   #5764	-> 103
      //   #5765	-> 118
      //   #5766	-> 123
      //   #5768	-> 128
      //   #5750	-> 131
      // Exception table:
      //   from	to	target	type
      //   2	7	131	finally
      //   7	14	102	java/lang/IllegalStateException
      //   7	14	131	finally
      //   14	52	102	java/lang/IllegalStateException
      //   14	52	131	finally
      //   52	63	102	java/lang/IllegalStateException
      //   52	63	131	finally
      //   86	93	102	java/lang/IllegalStateException
      //   86	93	131	finally
      //   103	118	131	finally
      //   118	123	131	finally
      //   123	128	131	finally
    }
    
    private void notifyTrackData(Pair<SubtitleTrack, byte[]> param1Pair) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_1
      //   3: getfield first : Ljava/lang/Object;
      //   6: checkcast android/media/SubtitleTrack
      //   9: astore_2
      //   10: aload_1
      //   11: getfield second : Ljava/lang/Object;
      //   14: checkcast [B
      //   17: astore_1
      //   18: aload_2
      //   19: aload_1
      //   20: iconst_1
      //   21: ldc2_w -1
      //   24: invokevirtual onData : ([BZJ)V
      //   27: aload_0
      //   28: monitorexit
      //   29: return
      //   30: astore_1
      //   31: aload_0
      //   32: monitorexit
      //   33: aload_1
      //   34: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5771	-> 2
      //   #5772	-> 10
      //   #5773	-> 18
      //   #5774	-> 27
      //   #5770	-> 30
      // Exception table:
      //   from	to	target	type
      //   2	10	30	finally
      //   10	18	30	finally
      //   18	27	30	finally
    }
    
    private void notifyStop() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   6: astore_1
      //   7: aload_1
      //   8: arraylength
      //   9: istore_2
      //   10: iconst_0
      //   11: istore_3
      //   12: iload_3
      //   13: iload_2
      //   14: if_icmpge -> 43
      //   17: aload_1
      //   18: iload_3
      //   19: aaload
      //   20: astore #4
      //   22: aload #4
      //   24: ifnonnull -> 30
      //   27: goto -> 43
      //   30: aload #4
      //   32: invokeinterface onStop : ()V
      //   37: iinc #3, 1
      //   40: goto -> 12
      //   43: aload_0
      //   44: monitorexit
      //   45: return
      //   46: astore_1
      //   47: aload_0
      //   48: monitorexit
      //   49: aload_1
      //   50: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5777	-> 2
      //   #5778	-> 22
      //   #5779	-> 27
      //   #5781	-> 30
      //   #5777	-> 37
      //   #5783	-> 43
      //   #5776	-> 46
      // Exception table:
      //   from	to	target	type
      //   2	10	46	finally
      //   30	37	46	finally
    }
    
    private int registerListener(MediaTimeProvider.OnMediaTimeListener param1OnMediaTimeListener) {
      byte b = 0;
      while (true) {
        MediaTimeProvider.OnMediaTimeListener[] arrayOfOnMediaTimeListener = this.mListeners;
        if (b >= arrayOfOnMediaTimeListener.length || 
          arrayOfOnMediaTimeListener[b] == param1OnMediaTimeListener || arrayOfOnMediaTimeListener[b] == null)
          break; 
        b++;
      } 
      MediaTimeProvider.OnMediaTimeListener[] arrayOfOnMediaTimeListener2 = this.mListeners;
      if (b >= arrayOfOnMediaTimeListener2.length) {
        MediaTimeProvider.OnMediaTimeListener[] arrayOfOnMediaTimeListener = new MediaTimeProvider.OnMediaTimeListener[b + 1];
        long[] arrayOfLong2 = new long[b + 1];
        System.arraycopy(arrayOfOnMediaTimeListener2, 0, arrayOfOnMediaTimeListener, 0, arrayOfOnMediaTimeListener2.length);
        long[] arrayOfLong1 = this.mTimes;
        System.arraycopy(arrayOfLong1, 0, arrayOfLong2, 0, arrayOfLong1.length);
        this.mListeners = arrayOfOnMediaTimeListener;
        this.mTimes = arrayOfLong2;
      } 
      MediaTimeProvider.OnMediaTimeListener[] arrayOfOnMediaTimeListener1 = this.mListeners;
      if (arrayOfOnMediaTimeListener1[b] == null) {
        arrayOfOnMediaTimeListener1[b] = param1OnMediaTimeListener;
        this.mTimes[b] = -1L;
      } 
      return b;
    }
    
    public void notifyAt(long param1Long, MediaTimeProvider.OnMediaTimeListener param1OnMediaTimeListener) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield DEBUG : Z
      //   6: ifeq -> 46
      //   9: new java/lang/StringBuilder
      //   12: astore #4
      //   14: aload #4
      //   16: invokespecial <init> : ()V
      //   19: aload #4
      //   21: ldc_w 'notifyAt '
      //   24: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   27: pop
      //   28: aload #4
      //   30: lload_1
      //   31: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   34: pop
      //   35: ldc 'MTP'
      //   37: aload #4
      //   39: invokevirtual toString : ()Ljava/lang/String;
      //   42: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   45: pop
      //   46: aload_0
      //   47: getfield mTimes : [J
      //   50: aload_0
      //   51: aload_3
      //   52: invokespecial registerListener : (Landroid/media/MediaTimeProvider$OnMediaTimeListener;)I
      //   55: lload_1
      //   56: lastore
      //   57: aload_0
      //   58: iconst_0
      //   59: lconst_0
      //   60: invokespecial scheduleNotification : (IJ)V
      //   63: aload_0
      //   64: monitorexit
      //   65: return
      //   66: astore_3
      //   67: aload_0
      //   68: monitorexit
      //   69: aload_3
      //   70: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5813	-> 0
      //   #5814	-> 2
      //   #5815	-> 46
      //   #5816	-> 57
      //   #5817	-> 63
      //   #5818	-> 65
      //   #5817	-> 66
      // Exception table:
      //   from	to	target	type
      //   2	46	66	finally
      //   46	57	66	finally
      //   57	63	66	finally
      //   63	65	66	finally
      //   67	69	66	finally
    }
    
    public void scheduleUpdate(MediaTimeProvider.OnMediaTimeListener param1OnMediaTimeListener) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield DEBUG : Z
      //   6: ifeq -> 18
      //   9: ldc 'MTP'
      //   11: ldc_w 'scheduleUpdate'
      //   14: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   17: pop
      //   18: aload_0
      //   19: aload_1
      //   20: invokespecial registerListener : (Landroid/media/MediaTimeProvider$OnMediaTimeListener;)I
      //   23: istore_2
      //   24: aload_0
      //   25: getfield mStopped : Z
      //   28: ifne -> 44
      //   31: aload_0
      //   32: getfield mTimes : [J
      //   35: iload_2
      //   36: lconst_0
      //   37: lastore
      //   38: aload_0
      //   39: iconst_0
      //   40: lconst_0
      //   41: invokespecial scheduleNotification : (IJ)V
      //   44: aload_0
      //   45: monitorexit
      //   46: return
      //   47: astore_1
      //   48: aload_0
      //   49: monitorexit
      //   50: aload_1
      //   51: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5821	-> 0
      //   #5822	-> 2
      //   #5823	-> 18
      //   #5825	-> 24
      //   #5826	-> 31
      //   #5827	-> 38
      //   #5829	-> 44
      //   #5830	-> 46
      //   #5829	-> 47
      // Exception table:
      //   from	to	target	type
      //   2	18	47	finally
      //   18	24	47	finally
      //   24	31	47	finally
      //   31	38	47	finally
      //   38	44	47	finally
      //   44	46	47	finally
      //   48	50	47	finally
    }
    
    public void cancelNotifications(MediaTimeProvider.OnMediaTimeListener param1OnMediaTimeListener) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: iconst_0
      //   3: istore_2
      //   4: iload_2
      //   5: aload_0
      //   6: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   9: arraylength
      //   10: if_icmpge -> 120
      //   13: aload_0
      //   14: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   17: iload_2
      //   18: aaload
      //   19: aload_1
      //   20: if_acmpne -> 102
      //   23: aload_0
      //   24: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   27: iload_2
      //   28: iconst_1
      //   29: iadd
      //   30: aload_0
      //   31: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   34: iload_2
      //   35: aload_0
      //   36: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   39: arraylength
      //   40: iload_2
      //   41: isub
      //   42: iconst_1
      //   43: isub
      //   44: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
      //   47: aload_0
      //   48: getfield mTimes : [J
      //   51: iload_2
      //   52: iconst_1
      //   53: iadd
      //   54: aload_0
      //   55: getfield mTimes : [J
      //   58: iload_2
      //   59: aload_0
      //   60: getfield mTimes : [J
      //   63: arraylength
      //   64: iload_2
      //   65: isub
      //   66: iconst_1
      //   67: isub
      //   68: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
      //   71: aload_0
      //   72: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   75: aload_0
      //   76: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   79: arraylength
      //   80: iconst_1
      //   81: isub
      //   82: aconst_null
      //   83: aastore
      //   84: aload_0
      //   85: getfield mTimes : [J
      //   88: aload_0
      //   89: getfield mTimes : [J
      //   92: arraylength
      //   93: iconst_1
      //   94: isub
      //   95: ldc2_w -1
      //   98: lastore
      //   99: goto -> 120
      //   102: aload_0
      //   103: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   106: iload_2
      //   107: aaload
      //   108: ifnonnull -> 114
      //   111: goto -> 120
      //   114: iinc #2, 1
      //   117: goto -> 4
      //   120: aload_0
      //   121: iconst_0
      //   122: lconst_0
      //   123: invokespecial scheduleNotification : (IJ)V
      //   126: aload_0
      //   127: monitorexit
      //   128: return
      //   129: astore_1
      //   130: aload_0
      //   131: monitorexit
      //   132: aload_1
      //   133: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5834	-> 0
      //   #5835	-> 2
      //   #5836	-> 4
      //   #5837	-> 13
      //   #5838	-> 23
      //   #5840	-> 47
      //   #5842	-> 71
      //   #5843	-> 84
      //   #5844	-> 99
      //   #5845	-> 102
      //   #5846	-> 111
      //   #5836	-> 114
      //   #5850	-> 120
      //   #5851	-> 126
      //   #5852	-> 128
      //   #5851	-> 129
      // Exception table:
      //   from	to	target	type
      //   4	13	129	finally
      //   13	23	129	finally
      //   23	47	129	finally
      //   47	71	129	finally
      //   71	84	129	finally
      //   84	99	129	finally
      //   102	111	129	finally
      //   120	126	129	finally
      //   126	128	129	finally
      //   130	132	129	finally
    }
    
    private void notifyTimedEvent(boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iload_1
      //   4: iconst_1
      //   5: invokevirtual getCurrentTimeUs : (ZZ)J
      //   8: lstore_2
      //   9: goto -> 36
      //   12: astore #4
      //   14: goto -> 498
      //   17: astore #4
      //   19: aload_0
      //   20: iconst_1
      //   21: putfield mRefresh : Z
      //   24: aload_0
      //   25: iconst_1
      //   26: putfield mPausing : Z
      //   29: aload_0
      //   30: iload_1
      //   31: iconst_1
      //   32: invokevirtual getCurrentTimeUs : (ZZ)J
      //   35: lstore_2
      //   36: lload_2
      //   37: lstore #5
      //   39: aload_0
      //   40: getfield mSeeking : Z
      //   43: istore_1
      //   44: iload_1
      //   45: ifeq -> 51
      //   48: aload_0
      //   49: monitorexit
      //   50: return
      //   51: aload_0
      //   52: getfield DEBUG : Z
      //   55: ifeq -> 201
      //   58: new java/lang/StringBuilder
      //   61: astore #7
      //   63: aload #7
      //   65: invokespecial <init> : ()V
      //   68: aload #7
      //   70: ldc 'notifyTimedEvent('
      //   72: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   75: pop
      //   76: aload #7
      //   78: aload_0
      //   79: getfield mLastTimeUs : J
      //   82: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   85: pop
      //   86: aload #7
      //   88: ldc ' -> '
      //   90: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   93: pop
      //   94: aload #7
      //   96: lload_2
      //   97: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   100: pop
      //   101: aload #7
      //   103: ldc ') from {'
      //   105: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   108: pop
      //   109: iconst_1
      //   110: istore #8
      //   112: aload_0
      //   113: getfield mTimes : [J
      //   116: astore #4
      //   118: aload #4
      //   120: arraylength
      //   121: istore #9
      //   123: iconst_0
      //   124: istore #10
      //   126: iload #10
      //   128: iload #9
      //   130: if_icmpge -> 182
      //   133: aload #4
      //   135: iload #10
      //   137: laload
      //   138: lstore #11
      //   140: lload #11
      //   142: ldc2_w -1
      //   145: lcmp
      //   146: ifne -> 152
      //   149: goto -> 176
      //   152: iload #8
      //   154: ifne -> 165
      //   157: aload #7
      //   159: ldc ', '
      //   161: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   164: pop
      //   165: aload #7
      //   167: lload #11
      //   169: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   172: pop
      //   173: iconst_0
      //   174: istore #8
      //   176: iinc #10, 1
      //   179: goto -> 126
      //   182: aload #7
      //   184: ldc '}'
      //   186: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   189: pop
      //   190: ldc 'MTP'
      //   192: aload #7
      //   194: invokevirtual toString : ()Ljava/lang/String;
      //   197: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   200: pop
      //   201: new java/util/Vector
      //   204: astore #4
      //   206: aload #4
      //   208: invokespecial <init> : ()V
      //   211: iconst_0
      //   212: istore #10
      //   214: iload #10
      //   216: aload_0
      //   217: getfield mTimes : [J
      //   220: arraylength
      //   221: if_icmpge -> 362
      //   224: aload_0
      //   225: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   228: iload #10
      //   230: aaload
      //   231: ifnonnull -> 237
      //   234: goto -> 362
      //   237: aload_0
      //   238: getfield mTimes : [J
      //   241: iload #10
      //   243: laload
      //   244: ldc2_w -1
      //   247: lcmp
      //   248: ifgt -> 258
      //   251: lload #5
      //   253: lstore #11
      //   255: goto -> 352
      //   258: aload_0
      //   259: getfield mTimes : [J
      //   262: iload #10
      //   264: laload
      //   265: ldc2_w 1000
      //   268: lload_2
      //   269: ladd
      //   270: lcmp
      //   271: ifgt -> 319
      //   274: aload #4
      //   276: aload_0
      //   277: getfield mListeners : [Landroid/media/MediaTimeProvider$OnMediaTimeListener;
      //   280: iload #10
      //   282: aaload
      //   283: invokevirtual add : (Ljava/lang/Object;)Z
      //   286: pop
      //   287: aload_0
      //   288: getfield DEBUG : Z
      //   291: ifeq -> 302
      //   294: ldc 'MTP'
      //   296: ldc 'removed'
      //   298: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   301: pop
      //   302: aload_0
      //   303: getfield mTimes : [J
      //   306: iload #10
      //   308: ldc2_w -1
      //   311: lastore
      //   312: lload #5
      //   314: lstore #11
      //   316: goto -> 352
      //   319: lload #5
      //   321: lload_2
      //   322: lcmp
      //   323: ifeq -> 343
      //   326: lload #5
      //   328: lstore #11
      //   330: aload_0
      //   331: getfield mTimes : [J
      //   334: iload #10
      //   336: laload
      //   337: lload #5
      //   339: lcmp
      //   340: ifge -> 352
      //   343: aload_0
      //   344: getfield mTimes : [J
      //   347: iload #10
      //   349: laload
      //   350: lstore #11
      //   352: iinc #10, 1
      //   355: lload #11
      //   357: lstore #5
      //   359: goto -> 214
      //   362: lload #5
      //   364: lload_2
      //   365: lcmp
      //   366: ifle -> 447
      //   369: aload_0
      //   370: getfield mPaused : Z
      //   373: ifne -> 447
      //   376: aload_0
      //   377: getfield DEBUG : Z
      //   380: ifeq -> 435
      //   383: new java/lang/StringBuilder
      //   386: astore #7
      //   388: aload #7
      //   390: invokespecial <init> : ()V
      //   393: aload #7
      //   395: ldc 'scheduling for '
      //   397: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   400: pop
      //   401: aload #7
      //   403: lload #5
      //   405: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   408: pop
      //   409: aload #7
      //   411: ldc ' and '
      //   413: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   416: pop
      //   417: aload #7
      //   419: lload_2
      //   420: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   423: pop
      //   424: ldc 'MTP'
      //   426: aload #7
      //   428: invokevirtual toString : ()Ljava/lang/String;
      //   431: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   434: pop
      //   435: aload_0
      //   436: getfield mPlayer : Landroid/media/MediaPlayer;
      //   439: lload #5
      //   441: invokevirtual notifyAt : (J)V
      //   444: goto -> 455
      //   447: aload_0
      //   448: getfield mEventHandler : Landroid/os/Handler;
      //   451: iconst_1
      //   452: invokevirtual removeMessages : (I)V
      //   455: aload #4
      //   457: invokevirtual iterator : ()Ljava/util/Iterator;
      //   460: astore #7
      //   462: aload #7
      //   464: invokeinterface hasNext : ()Z
      //   469: ifeq -> 495
      //   472: aload #7
      //   474: invokeinterface next : ()Ljava/lang/Object;
      //   479: checkcast android/media/MediaTimeProvider$OnMediaTimeListener
      //   482: astore #4
      //   484: aload #4
      //   486: lload_2
      //   487: invokeinterface onTimedEvent : (J)V
      //   492: goto -> 462
      //   495: aload_0
      //   496: monitorexit
      //   497: return
      //   498: aload_0
      //   499: monitorexit
      //   500: aload #4
      //   502: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5858	-> 2
      //   #5864	-> 9
      //   #5857	-> 12
      //   #5859	-> 17
      //   #5861	-> 19
      //   #5862	-> 24
      //   #5863	-> 29
      //   #5865	-> 36
      //   #5867	-> 39
      //   #5869	-> 48
      //   #5872	-> 51
      //   #5873	-> 58
      //   #5874	-> 68
      //   #5875	-> 94
      //   #5876	-> 109
      //   #5877	-> 112
      //   #5878	-> 140
      //   #5879	-> 149
      //   #5881	-> 152
      //   #5882	-> 165
      //   #5883	-> 173
      //   #5877	-> 176
      //   #5885	-> 182
      //   #5886	-> 190
      //   #5889	-> 201
      //   #5891	-> 211
      //   #5892	-> 224
      //   #5893	-> 234
      //   #5895	-> 237
      //   #5897	-> 258
      //   #5898	-> 274
      //   #5899	-> 287
      //   #5900	-> 302
      //   #5901	-> 319
      //   #5902	-> 343
      //   #5891	-> 352
      //   #5906	-> 362
      //   #5908	-> 376
      //   #5909	-> 435
      //   #5911	-> 447
      //   #5915	-> 455
      //   #5916	-> 484
      //   #5917	-> 492
      //   #5918	-> 495
      //   #5857	-> 498
      // Exception table:
      //   from	to	target	type
      //   2	9	17	java/lang/IllegalStateException
      //   2	9	12	finally
      //   19	24	12	finally
      //   24	29	12	finally
      //   29	36	12	finally
      //   39	44	12	finally
      //   51	58	12	finally
      //   58	68	12	finally
      //   68	94	12	finally
      //   94	109	12	finally
      //   112	123	12	finally
      //   157	165	12	finally
      //   165	173	12	finally
      //   182	190	12	finally
      //   190	201	12	finally
      //   201	211	12	finally
      //   214	224	12	finally
      //   224	234	12	finally
      //   237	251	12	finally
      //   258	274	12	finally
      //   274	287	12	finally
      //   287	302	12	finally
      //   302	312	12	finally
      //   330	343	12	finally
      //   343	352	12	finally
      //   369	376	12	finally
      //   376	435	12	finally
      //   435	444	12	finally
      //   447	455	12	finally
      //   455	462	12	finally
      //   462	484	12	finally
      //   484	492	12	finally
    }
    
    public long getCurrentTimeUs(boolean param1Boolean1, boolean param1Boolean2) throws IllegalStateException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mPaused : Z
      //   6: ifeq -> 22
      //   9: iload_1
      //   10: ifne -> 22
      //   13: aload_0
      //   14: getfield mLastReportedTime : J
      //   17: lstore_3
      //   18: aload_0
      //   19: monitorexit
      //   20: lload_3
      //   21: lreturn
      //   22: aload_0
      //   23: aload_0
      //   24: getfield mPlayer : Landroid/media/MediaPlayer;
      //   27: invokevirtual getCurrentPosition : ()I
      //   30: i2l
      //   31: ldc2_w 1000
      //   34: lmul
      //   35: putfield mLastTimeUs : J
      //   38: aload_0
      //   39: getfield mPlayer : Landroid/media/MediaPlayer;
      //   42: invokevirtual isPlaying : ()Z
      //   45: ifeq -> 63
      //   48: aload_0
      //   49: getfield mBuffering : Z
      //   52: ifeq -> 58
      //   55: goto -> 63
      //   58: iconst_0
      //   59: istore_1
      //   60: goto -> 65
      //   63: iconst_1
      //   64: istore_1
      //   65: aload_0
      //   66: iload_1
      //   67: putfield mPaused : Z
      //   70: aload_0
      //   71: getfield DEBUG : Z
      //   74: ifeq -> 145
      //   77: new java/lang/StringBuilder
      //   80: astore #5
      //   82: aload #5
      //   84: invokespecial <init> : ()V
      //   87: aload_0
      //   88: getfield mPaused : Z
      //   91: ifeq -> 102
      //   94: ldc_w 'paused'
      //   97: astore #6
      //   99: goto -> 107
      //   102: ldc_w 'playing'
      //   105: astore #6
      //   107: aload #5
      //   109: aload #6
      //   111: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   114: pop
      //   115: aload #5
      //   117: ldc_w ' at '
      //   120: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   123: pop
      //   124: aload #5
      //   126: aload_0
      //   127: getfield mLastTimeUs : J
      //   130: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   133: pop
      //   134: ldc 'MTP'
      //   136: aload #5
      //   138: invokevirtual toString : ()Ljava/lang/String;
      //   141: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   144: pop
      //   145: iload_2
      //   146: ifeq -> 196
      //   149: aload_0
      //   150: getfield mLastTimeUs : J
      //   153: aload_0
      //   154: getfield mLastReportedTime : J
      //   157: lcmp
      //   158: ifge -> 196
      //   161: aload_0
      //   162: getfield mLastReportedTime : J
      //   165: aload_0
      //   166: getfield mLastTimeUs : J
      //   169: lsub
      //   170: ldc2_w 1000000
      //   173: lcmp
      //   174: ifle -> 204
      //   177: aload_0
      //   178: iconst_0
      //   179: putfield mStopped : Z
      //   182: aload_0
      //   183: iconst_1
      //   184: putfield mSeeking : Z
      //   187: aload_0
      //   188: iconst_3
      //   189: lconst_0
      //   190: invokespecial scheduleNotification : (IJ)V
      //   193: goto -> 204
      //   196: aload_0
      //   197: aload_0
      //   198: getfield mLastTimeUs : J
      //   201: putfield mLastReportedTime : J
      //   204: aload_0
      //   205: getfield mLastReportedTime : J
      //   208: lstore_3
      //   209: aload_0
      //   210: monitorexit
      //   211: lload_3
      //   212: lreturn
      //   213: astore #6
      //   215: aload_0
      //   216: getfield mPausing : Z
      //   219: ifeq -> 312
      //   222: aload_0
      //   223: iconst_0
      //   224: putfield mPausing : Z
      //   227: iload_2
      //   228: ifeq -> 243
      //   231: aload_0
      //   232: getfield mLastReportedTime : J
      //   235: aload_0
      //   236: getfield mLastTimeUs : J
      //   239: lcmp
      //   240: ifge -> 251
      //   243: aload_0
      //   244: aload_0
      //   245: getfield mLastTimeUs : J
      //   248: putfield mLastReportedTime : J
      //   251: aload_0
      //   252: iconst_1
      //   253: putfield mPaused : Z
      //   256: aload_0
      //   257: getfield DEBUG : Z
      //   260: ifeq -> 303
      //   263: new java/lang/StringBuilder
      //   266: astore #6
      //   268: aload #6
      //   270: invokespecial <init> : ()V
      //   273: aload #6
      //   275: ldc_w 'illegal state, but pausing: estimating at '
      //   278: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   281: pop
      //   282: aload #6
      //   284: aload_0
      //   285: getfield mLastReportedTime : J
      //   288: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   291: pop
      //   292: ldc 'MTP'
      //   294: aload #6
      //   296: invokevirtual toString : ()Ljava/lang/String;
      //   299: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   302: pop
      //   303: aload_0
      //   304: getfield mLastReportedTime : J
      //   307: lstore_3
      //   308: aload_0
      //   309: monitorexit
      //   310: lload_3
      //   311: lreturn
      //   312: aload #6
      //   314: athrow
      //   315: astore #6
      //   317: aload_0
      //   318: monitorexit
      //   319: aload #6
      //   321: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5922	-> 0
      //   #5925	-> 2
      //   #5926	-> 13
      //   #5930	-> 22
      //   #5931	-> 38
      //   #5932	-> 70
      //   #5946	-> 145
      //   #5947	-> 145
      //   #5949	-> 161
      //   #5952	-> 177
      //   #5953	-> 182
      //   #5954	-> 187
      //   #5957	-> 196
      //   #5960	-> 204
      //   #5933	-> 213
      //   #5934	-> 215
      //   #5936	-> 222
      //   #5937	-> 227
      //   #5938	-> 243
      //   #5940	-> 251
      //   #5941	-> 256
      //   #5942	-> 303
      //   #5945	-> 312
      //   #5961	-> 315
      // Exception table:
      //   from	to	target	type
      //   2	9	315	finally
      //   13	20	315	finally
      //   22	38	213	java/lang/IllegalStateException
      //   22	38	315	finally
      //   38	55	213	java/lang/IllegalStateException
      //   38	55	315	finally
      //   65	70	213	java/lang/IllegalStateException
      //   65	70	315	finally
      //   70	94	213	java/lang/IllegalStateException
      //   70	94	315	finally
      //   107	145	213	java/lang/IllegalStateException
      //   107	145	315	finally
      //   149	161	315	finally
      //   161	177	315	finally
      //   177	182	315	finally
      //   182	187	315	finally
      //   187	193	315	finally
      //   196	204	315	finally
      //   204	211	315	finally
      //   215	222	315	finally
      //   222	227	315	finally
      //   231	243	315	finally
      //   243	251	315	finally
      //   251	256	315	finally
      //   256	303	315	finally
      //   303	310	315	finally
      //   312	315	315	finally
      //   317	319	315	finally
    }
    
    private class EventHandler extends Handler {
      final MediaPlayer.TimeProvider this$0;
      
      public EventHandler(Looper param2Looper) {
        super(param2Looper);
      }
      
      public void handleMessage(Message param2Message) {
        if (param2Message.what == 1) {
          int i = param2Message.arg1;
          if (i != 0) {
            if (i != 2) {
              if (i != 3) {
                if (i == 4)
                  MediaPlayer.TimeProvider.this.notifyTrackData((Pair)param2Message.obj); 
              } else {
                MediaPlayer.TimeProvider.this.notifySeek();
              } 
            } else {
              MediaPlayer.TimeProvider.this.notifyStop();
            } 
          } else {
            MediaPlayer.TimeProvider.this.notifyTimedEvent(true);
          } 
        } 
      }
    }
  }
  
  private class EventHandler extends Handler {
    final MediaPlayer.TimeProvider this$0;
    
    public EventHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      if (param1Message.what == 1) {
        int i = param1Message.arg1;
        if (i != 0) {
          if (i != 2) {
            if (i != 3) {
              if (i == 4)
                this.this$0.notifyTrackData((Pair)param1Message.obj); 
            } else {
              this.this$0.notifySeek();
            } 
          } else {
            this.this$0.notifyStop();
          } 
        } else {
          this.this$0.notifyTimedEvent(true);
        } 
      } 
    }
  }
  
  class MetricsConstants {
    public static final String CODEC_AUDIO = "android.media.mediaplayer.audio.codec";
    
    public static final String CODEC_VIDEO = "android.media.mediaplayer.video.codec";
    
    public static final String DURATION = "android.media.mediaplayer.durationMs";
    
    public static final String ERRORS = "android.media.mediaplayer.err";
    
    public static final String ERROR_CODE = "android.media.mediaplayer.errcode";
    
    public static final String FRAMES = "android.media.mediaplayer.frames";
    
    public static final String FRAMES_DROPPED = "android.media.mediaplayer.dropped";
    
    public static final String HEIGHT = "android.media.mediaplayer.height";
    
    public static final String MIME_TYPE_AUDIO = "android.media.mediaplayer.audio.mime";
    
    public static final String MIME_TYPE_VIDEO = "android.media.mediaplayer.video.mime";
    
    public static final String PLAYING = "android.media.mediaplayer.playingMs";
    
    public static final String WIDTH = "android.media.mediaplayer.width";
  }
  
  class OnBufferingUpdateListener {
    public abstract void onBufferingUpdate(MediaPlayer param1MediaPlayer, int param1Int);
  }
  
  class OnCompletionListener {
    public abstract void onCompletion(MediaPlayer param1MediaPlayer);
  }
  
  class OnDrmConfigHelper {
    public abstract void onDrmConfig(MediaPlayer param1MediaPlayer);
  }
  
  class OnDrmInfoListener {
    public abstract void onDrmInfo(MediaPlayer param1MediaPlayer, MediaPlayer.DrmInfo param1DrmInfo);
  }
  
  class OnDrmPreparedListener {
    public abstract void onDrmPrepared(MediaPlayer param1MediaPlayer, int param1Int);
  }
  
  class OnErrorListener {
    public abstract boolean onError(MediaPlayer param1MediaPlayer, int param1Int1, int param1Int2);
  }
  
  class OnInfoListener {
    public abstract boolean onInfo(MediaPlayer param1MediaPlayer, int param1Int1, int param1Int2);
  }
  
  class OnMediaTimeDiscontinuityListener {
    public abstract void onMediaTimeDiscontinuity(MediaPlayer param1MediaPlayer, MediaTimestamp param1MediaTimestamp);
  }
  
  class OnPreparedListener {
    public abstract void onPrepared(MediaPlayer param1MediaPlayer);
  }
  
  class OnSeekCompleteListener {
    public abstract void onSeekComplete(MediaPlayer param1MediaPlayer);
  }
  
  class OnSubtitleDataListener {
    public abstract void onSubtitleData(MediaPlayer param1MediaPlayer, SubtitleData param1SubtitleData);
  }
  
  class OnTimedMetaDataAvailableListener {
    public abstract void onTimedMetaDataAvailable(MediaPlayer param1MediaPlayer, TimedMetaData param1TimedMetaData);
  }
  
  class OnTimedTextListener {
    public abstract void onTimedText(MediaPlayer param1MediaPlayer, TimedText param1TimedText);
  }
  
  class OnVideoSizeChangedListener {
    public abstract void onVideoSizeChanged(MediaPlayer param1MediaPlayer, int param1Int1, int param1Int2);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class PlaybackRateAudioMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PrepareDrmStatusCode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class SeekMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TrackType implements Annotation {}
}
