package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class Rating implements Parcelable {
  private Rating(int paramInt, float paramFloat) {
    this.mRatingStyle = paramInt;
    this.mRatingValue = paramFloat;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Rating:style=");
    stringBuilder.append(this.mRatingStyle);
    stringBuilder.append(" rating=");
    float f = this.mRatingValue;
    if (f < 0.0F) {
      null = "unrated";
    } else {
      null = String.valueOf(f);
    } 
    stringBuilder.append(null);
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return this.mRatingStyle;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRatingStyle);
    paramParcel.writeFloat(this.mRatingValue);
  }
  
  public static final Parcelable.Creator<Rating> CREATOR = new Parcelable.Creator<Rating>() {
      public Rating createFromParcel(Parcel param1Parcel) {
        return new Rating(param1Parcel.readInt(), param1Parcel.readFloat());
      }
      
      public Rating[] newArray(int param1Int) {
        return new Rating[param1Int];
      }
    };
  
  public static final int RATING_3_STARS = 3;
  
  public static final int RATING_4_STARS = 4;
  
  public static final int RATING_5_STARS = 5;
  
  public static final int RATING_HEART = 1;
  
  public static final int RATING_NONE = 0;
  
  private static final float RATING_NOT_RATED = -1.0F;
  
  public static final int RATING_PERCENTAGE = 6;
  
  public static final int RATING_THUMB_UP_DOWN = 2;
  
  private static final String TAG = "Rating";
  
  private final int mRatingStyle;
  
  private final float mRatingValue;
  
  public static Rating newUnratedRating(int paramInt) {
    switch (paramInt) {
      default:
        return null;
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
        break;
    } 
    return new Rating(paramInt, -1.0F);
  }
  
  public static Rating newHeartRating(boolean paramBoolean) {
    float f;
    if (paramBoolean) {
      f = 1.0F;
    } else {
      f = 0.0F;
    } 
    return new Rating(1, f);
  }
  
  public static Rating newThumbRating(boolean paramBoolean) {
    float f;
    if (paramBoolean) {
      f = 1.0F;
    } else {
      f = 0.0F;
    } 
    return new Rating(2, f);
  }
  
  public static Rating newStarRating(int paramInt, float paramFloat) {
    float f;
    if (paramInt != 3) {
      if (paramInt != 4) {
        if (paramInt != 5) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid rating style (");
          stringBuilder.append(paramInt);
          stringBuilder.append(") for a star rating");
          Log.e("Rating", stringBuilder.toString());
          return null;
        } 
        f = 5.0F;
      } else {
        f = 4.0F;
      } 
    } else {
      f = 3.0F;
    } 
    if (paramFloat < 0.0F || paramFloat > f) {
      Log.e("Rating", "Trying to set out of range star-based rating");
      return null;
    } 
    return new Rating(paramInt, paramFloat);
  }
  
  public static Rating newPercentageRating(float paramFloat) {
    if (paramFloat < 0.0F || paramFloat > 100.0F) {
      Log.e("Rating", "Invalid percentage-based rating value");
      return null;
    } 
    return new Rating(6, paramFloat);
  }
  
  public boolean isRated() {
    boolean bool;
    if (this.mRatingValue >= 0.0F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getRatingStyle() {
    return this.mRatingStyle;
  }
  
  public boolean hasHeart() {
    int i = this.mRatingStyle;
    boolean bool = false;
    if (i != 1)
      return false; 
    if (this.mRatingValue == 1.0F)
      bool = true; 
    return bool;
  }
  
  public boolean isThumbUp() {
    int i = this.mRatingStyle;
    boolean bool = false;
    if (i != 2)
      return false; 
    if (this.mRatingValue == 1.0F)
      bool = true; 
    return bool;
  }
  
  public float getStarRating() {
    float f = -1.0F;
    int i = this.mRatingStyle;
    if (i == 3 || i == 4 || i == 5)
      if (isRated())
        f = this.mRatingValue;  
    return f;
  }
  
  public float getPercentRating() {
    if (this.mRatingStyle != 6 || !isRated())
      return -1.0F; 
    return this.mRatingValue;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class StarStyle implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Style implements Annotation {}
}
