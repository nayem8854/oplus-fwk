package android.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

public class OifaceBindUtils {
  public static final int BIND_TASK = 2;
  
  private static final String OIFACE_DESCRIPTOR = "com.oppo.oiface.IOIfaceService";
  
  private static final String TAG = "OifaceBindUtils";
  
  private static final int TRANSACTION_BIND_TASK = 7;
  
  public static final int UNBIND_TASK = 0;
  
  private static OifaceBindUtils sInstance;
  
  private static boolean sOifaceProp = SystemProperties.getBoolean("persist.sys.oiface.enable", true);
  
  private IBinder.DeathRecipient mDeathRecipient;
  
  private IBinder mRemote;
  
  public static OifaceBindUtils getInstance() {
    // Byte code:
    //   0: ldc android/media/OifaceBindUtils
    //   2: monitorenter
    //   3: getstatic android/media/OifaceBindUtils.sInstance : Landroid/media/OifaceBindUtils;
    //   6: ifnonnull -> 21
    //   9: new android/media/OifaceBindUtils
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/media/OifaceBindUtils.sInstance : Landroid/media/OifaceBindUtils;
    //   21: getstatic android/media/OifaceBindUtils.sInstance : Landroid/media/OifaceBindUtils;
    //   24: astore_0
    //   25: ldc android/media/OifaceBindUtils
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/media/OifaceBindUtils
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #38	-> 3
    //   #39	-> 9
    //   #40	-> 21
    //   #37	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  private OifaceBindUtils() {
    this.mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
    connectOifaceService();
  }
  
  private IBinder connectOifaceService() {
    this.mRemote = ServiceManager.checkService("oiface");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("connectOifaceService mRemote= ");
    stringBuilder.append(this.mRemote);
    Log.d("OifaceBindUtils", stringBuilder.toString());
    IBinder iBinder = this.mRemote;
    if (iBinder != null)
      try {
        iBinder.linkToDeath(this.mDeathRecipient, 0);
      } catch (RemoteException remoteException) {
        this.mRemote = null;
      }  
    return this.mRemote;
  }
  
  public void bindTaskWithOiface(int paramInt) {
    bindTaskWithOiface(paramInt, Process.myTid());
  }
  
  public void bindTaskWithOiface(int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("bindTaskWithOiface mRemote= ");
    stringBuilder.append(this.mRemote);
    stringBuilder.append(",type =");
    stringBuilder.append(paramInt1);
    stringBuilder.append(",threadId =");
    stringBuilder.append(paramInt2);
    Log.d("OifaceBindUtils", stringBuilder.toString());
    if (this.mRemote == null && connectOifaceService() == null) {
      this.mRemote = null;
      return;
    } 
    Parcel parcel2 = Parcel.obtain();
    Parcel parcel1 = Parcel.obtain();
    try {
      parcel2.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
      parcel2.writeInt(paramInt1);
      parcel2.writeInt(paramInt2);
      this.mRemote.transact(7, parcel2, parcel1, 1);
      Log.d("OifaceBindUtils", "bindTaskWithOiface transact");
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("bindTaskWithOiface = ");
      stringBuilder1.append(exception.getMessage());
      Log.e("OifaceBindUtils", stringBuilder1.toString());
      parcel2.recycle();
      parcel1.recycle();
    } finally {
      Exception exception;
    } 
  }
  
  public static void bindTask() {
    try {
    
    } finally {
      Exception exception = null;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("bindTask, t=");
      stringBuilder.append(exception);
    } 
  }
}
