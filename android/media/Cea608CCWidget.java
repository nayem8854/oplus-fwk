package android.media;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.CaptioningManager;
import android.widget.LinearLayout;
import android.widget.TextView;

class Cea608CCWidget extends ClosedCaptionWidget implements Cea608CCParser.DisplayListener {
  private static final String mDummyText = "1234567890123456789012345678901234";
  
  private static final Rect mTextBounds = new Rect();
  
  public Cea608CCWidget(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public Cea608CCWidget(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public Cea608CCWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public Cea608CCWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public ClosedCaptionWidget.ClosedCaptionLayout createCaptionLayout(Context paramContext) {
    return new CCLayout(paramContext);
  }
  
  public void onDisplayChanged(SpannableStringBuilder[] paramArrayOfSpannableStringBuilder) {
    ((CCLayout)this.mClosedCaptionLayout).update(paramArrayOfSpannableStringBuilder);
    if (this.mListener != null)
      this.mListener.onChanged(this); 
  }
  
  public CaptioningManager.CaptionStyle getCaptionStyle() {
    return this.mCaptionStyle;
  }
  
  class CCLineBox extends TextView {
    private int mTextColor = -1;
    
    private int mBgColor = -16777216;
    
    private int mEdgeType = 0;
    
    private int mEdgeColor = 0;
    
    private static final float EDGE_OUTLINE_RATIO = 0.1F;
    
    private static final float EDGE_SHADOW_RATIO = 0.05F;
    
    private static final float FONT_PADDING_RATIO = 0.75F;
    
    private float mOutlineWidth;
    
    private float mShadowOffset;
    
    private float mShadowRadius;
    
    CCLineBox(Cea608CCWidget this$0) {
      super((Context)this$0);
      setGravity(17);
      setBackgroundColor(0);
      setTextColor(-1);
      setTypeface(Typeface.MONOSPACE);
      setVisibility(4);
      Resources resources = getContext().getResources();
      this.mOutlineWidth = resources.getDimensionPixelSize(17105487);
      this.mShadowRadius = resources.getDimensionPixelSize(17105489);
      this.mShadowOffset = resources.getDimensionPixelSize(17105488);
    }
    
    void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle) {
      this.mTextColor = param1CaptionStyle.foregroundColor;
      this.mBgColor = param1CaptionStyle.backgroundColor;
      this.mEdgeType = param1CaptionStyle.edgeType;
      this.mEdgeColor = param1CaptionStyle.edgeColor;
      setTextColor(this.mTextColor);
      if (this.mEdgeType == 2) {
        float f1 = this.mShadowRadius, f2 = this.mShadowOffset;
        setShadowLayer(f1, f2, f2, this.mEdgeColor);
      } else {
        setShadowLayer(0.0F, 0.0F, 0.0F, 0);
      } 
      invalidate();
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      float f1 = View.MeasureSpec.getSize(param1Int2) * 0.75F;
      setTextSize(0, f1);
      this.mOutlineWidth = 0.1F * f1 + 1.0F;
      this.mShadowRadius = f1 = 0.05F * f1 + 1.0F;
      this.mShadowOffset = f1;
      setScaleX(1.0F);
      getPaint().getTextBounds("1234567890123456789012345678901234", 0, "1234567890123456789012345678901234".length(), Cea608CCWidget.mTextBounds);
      float f2 = Cea608CCWidget.mTextBounds.width();
      f1 = View.MeasureSpec.getSize(param1Int1);
      setScaleX(f1 / f2);
      super.onMeasure(param1Int1, param1Int2);
    }
    
    protected void onDraw(Canvas param1Canvas) {
      int i = this.mEdgeType;
      if (i == -1 || i == 0 || i == 2) {
        super.onDraw(param1Canvas);
        return;
      } 
      if (i == 1) {
        drawEdgeOutline(param1Canvas);
      } else {
        drawEdgeRaisedOrDepressed(param1Canvas);
      } 
    }
    
    private void drawEdgeOutline(Canvas param1Canvas) {
      TextPaint textPaint = getPaint();
      Paint.Style style = textPaint.getStyle();
      Paint.Join join = textPaint.getStrokeJoin();
      float f = textPaint.getStrokeWidth();
      setTextColor(this.mEdgeColor);
      textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
      textPaint.setStrokeJoin(Paint.Join.ROUND);
      textPaint.setStrokeWidth(this.mOutlineWidth);
      super.onDraw(param1Canvas);
      setTextColor(this.mTextColor);
      textPaint.setStyle(style);
      textPaint.setStrokeJoin(join);
      textPaint.setStrokeWidth(f);
      setBackgroundSpans(0);
      super.onDraw(param1Canvas);
      setBackgroundSpans(this.mBgColor);
    }
    
    private void drawEdgeRaisedOrDepressed(Canvas param1Canvas) {
      boolean bool;
      int j;
      TextPaint textPaint = getPaint();
      Paint.Style style = textPaint.getStyle();
      textPaint.setStyle(Paint.Style.FILL);
      if (this.mEdgeType == 3) {
        bool = true;
      } else {
        bool = false;
      } 
      int i = -1;
      if (bool) {
        j = -1;
      } else {
        j = this.mEdgeColor;
      } 
      if (bool)
        i = this.mEdgeColor; 
      float f1 = this.mShadowRadius, f2 = f1 / 2.0F;
      setShadowLayer(f1, -f2, -f2, j);
      super.onDraw(param1Canvas);
      setBackgroundSpans(0);
      setShadowLayer(this.mShadowRadius, f2, f2, i);
      super.onDraw(param1Canvas);
      textPaint.setStyle(style);
      setBackgroundSpans(this.mBgColor);
    }
    
    private void setBackgroundSpans(int param1Int) {
      CharSequence charSequence = getText();
      if (charSequence instanceof Spannable) {
        Spannable spannable = (Spannable)charSequence;
        int i = spannable.length();
        Cea608CCParser.MutableBackgroundColorSpan[] arrayOfMutableBackgroundColorSpan = (Cea608CCParser.MutableBackgroundColorSpan[])spannable.getSpans(0, i, Cea608CCParser.MutableBackgroundColorSpan.class);
        for (i = 0; i < arrayOfMutableBackgroundColorSpan.length; i++)
          arrayOfMutableBackgroundColorSpan[i].setBackgroundColor(param1Int); 
      } 
    }
  }
  
  private static class CCLayout extends LinearLayout implements ClosedCaptionWidget.ClosedCaptionLayout {
    private static final int MAX_ROWS = 15;
    
    private static final float SAFE_AREA_RATIO = 0.9F;
    
    private final Cea608CCWidget.CCLineBox[] mLineBoxes = new Cea608CCWidget.CCLineBox[15];
    
    CCLayout(Context param1Context) {
      super(param1Context);
      setGravity(8388611);
      setOrientation(1);
      for (byte b = 0; b < 15; b++) {
        this.mLineBoxes[b] = new Cea608CCWidget.CCLineBox(getContext());
        addView((View)this.mLineBoxes[b], -2, -2);
      } 
    }
    
    public void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle) {
      for (byte b = 0; b < 15; b++)
        this.mLineBoxes[b].setCaptionStyle(param1CaptionStyle); 
    }
    
    public void setFontScale(float param1Float) {}
    
    void update(SpannableStringBuilder[] param1ArrayOfSpannableStringBuilder) {
      for (byte b = 0; b < 15; b++) {
        if (param1ArrayOfSpannableStringBuilder[b] != null) {
          this.mLineBoxes[b].setText((CharSequence)param1ArrayOfSpannableStringBuilder[b], TextView.BufferType.SPANNABLE);
          this.mLineBoxes[b].setVisibility(0);
        } else {
          this.mLineBoxes[b].setVisibility(4);
        } 
      } 
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      super.onMeasure(param1Int1, param1Int2);
      param1Int2 = getMeasuredWidth();
      param1Int1 = getMeasuredHeight();
      if (param1Int2 * 3 >= param1Int1 * 4) {
        param1Int2 = param1Int1 * 4 / 3;
      } else {
        param1Int1 = param1Int2 * 3 / 4;
      } 
      int i = (int)(param1Int2 * 0.9F);
      param1Int1 = (int)(param1Int1 * 0.9F);
      param1Int1 /= 15;
      param1Int2 = View.MeasureSpec.makeMeasureSpec(param1Int1, 1073741824);
      i = View.MeasureSpec.makeMeasureSpec(i, 1073741824);
      for (param1Int1 = 0; param1Int1 < 15; param1Int1++)
        this.mLineBoxes[param1Int1].measure(i, param1Int2); 
    }
    
    protected void onLayout(boolean param1Boolean, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      param1Int3 -= param1Int1;
      param1Int4 -= param1Int2;
      if (param1Int3 * 3 >= param1Int4 * 4) {
        param1Int2 = param1Int4 * 4 / 3;
        param1Int1 = param1Int4;
      } else {
        param1Int2 = param1Int3;
        param1Int1 = param1Int3 * 3 / 4;
      } 
      param1Int2 = (int)(param1Int2 * 0.9F);
      int i = (int)(param1Int1 * 0.9F);
      param1Int3 = (param1Int3 - param1Int2) / 2;
      param1Int4 = (param1Int4 - i) / 2;
      for (param1Int1 = 0; param1Int1 < 15; param1Int1++)
        this.mLineBoxes[param1Int1].layout(param1Int3, i * param1Int1 / 15 + param1Int4, param1Int3 + param1Int2, (param1Int1 + 1) * i / 15 + param1Int4); 
    }
  }
}
