package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class PlaybackParams implements Parcelable {
  private int mSet = 0;
  
  private int mAudioFallbackMode = 0;
  
  private int mAudioStretchMode = 0;
  
  private float mPitch = 1.0F;
  
  private float mSpeed = 1.0F;
  
  private PlaybackParams(Parcel paramParcel) {
    this.mSet = paramParcel.readInt();
    this.mAudioFallbackMode = paramParcel.readInt();
    this.mAudioStretchMode = paramParcel.readInt();
    float f = paramParcel.readFloat();
    if (f < 0.0F)
      this.mPitch = 0.0F; 
    this.mSpeed = paramParcel.readFloat();
  }
  
  public PlaybackParams allowDefaults() {
    this.mSet |= 0xF;
    return this;
  }
  
  public PlaybackParams setAudioFallbackMode(int paramInt) {
    this.mAudioFallbackMode = paramInt;
    this.mSet |= 0x4;
    return this;
  }
  
  public int getAudioFallbackMode() {
    if ((this.mSet & 0x4) != 0)
      return this.mAudioFallbackMode; 
    throw new IllegalStateException("audio fallback mode not set");
  }
  
  public PlaybackParams setAudioStretchMode(int paramInt) {
    this.mAudioStretchMode = paramInt;
    this.mSet |= 0x8;
    return this;
  }
  
  public int getAudioStretchMode() {
    if ((this.mSet & 0x8) != 0)
      return this.mAudioStretchMode; 
    throw new IllegalStateException("audio stretch mode not set");
  }
  
  public PlaybackParams setPitch(float paramFloat) {
    if (paramFloat >= 0.0F) {
      this.mPitch = paramFloat;
      this.mSet |= 0x2;
      return this;
    } 
    throw new IllegalArgumentException("pitch must not be negative");
  }
  
  public float getPitch() {
    if ((this.mSet & 0x2) != 0)
      return this.mPitch; 
    throw new IllegalStateException("pitch not set");
  }
  
  public PlaybackParams setSpeed(float paramFloat) {
    this.mSpeed = paramFloat;
    this.mSet |= 0x1;
    return this;
  }
  
  public float getSpeed() {
    if ((this.mSet & 0x1) != 0)
      return this.mSpeed; 
    throw new IllegalStateException("speed not set");
  }
  
  public static final Parcelable.Creator<PlaybackParams> CREATOR = new Parcelable.Creator<PlaybackParams>() {
      public PlaybackParams createFromParcel(Parcel param1Parcel) {
        return new PlaybackParams(param1Parcel);
      }
      
      public PlaybackParams[] newArray(int param1Int) {
        return new PlaybackParams[param1Int];
      }
    };
  
  public static final int AUDIO_FALLBACK_MODE_DEFAULT = 0;
  
  public static final int AUDIO_FALLBACK_MODE_FAIL = 2;
  
  public static final int AUDIO_FALLBACK_MODE_MUTE = 1;
  
  public static final int AUDIO_STRETCH_MODE_DEFAULT = 0;
  
  public static final int AUDIO_STRETCH_MODE_VOICE = 1;
  
  private static final int SET_AUDIO_FALLBACK_MODE = 4;
  
  private static final int SET_AUDIO_STRETCH_MODE = 8;
  
  private static final int SET_PITCH = 2;
  
  private static final int SET_SPEED = 1;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSet);
    paramParcel.writeInt(this.mAudioFallbackMode);
    paramParcel.writeInt(this.mAudioStretchMode);
    paramParcel.writeFloat(this.mPitch);
    paramParcel.writeFloat(this.mSpeed);
  }
  
  public PlaybackParams() {}
  
  @Retention(RetentionPolicy.SOURCE)
  class AudioFallbackMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class AudioStretchMode implements Annotation {}
}
