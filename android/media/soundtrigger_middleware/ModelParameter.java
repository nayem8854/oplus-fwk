package android.media.soundtrigger_middleware;

public @interface ModelParameter {
  public static final int INVALID = -1;
  
  public static final int THRESHOLD_FACTOR = 0;
}
