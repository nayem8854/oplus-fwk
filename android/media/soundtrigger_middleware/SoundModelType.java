package android.media.soundtrigger_middleware;

public @interface SoundModelType {
  public static final int GENERIC = 1;
  
  public static final int KEYPHRASE = 0;
  
  public static final int UNKNOWN = -1;
}
