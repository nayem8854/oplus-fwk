package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class RecognitionConfig implements Parcelable {
  public static final Parcelable.Creator<RecognitionConfig> CREATOR = new Parcelable.Creator<RecognitionConfig>() {
      public RecognitionConfig createFromParcel(Parcel param1Parcel) {
        RecognitionConfig recognitionConfig = new RecognitionConfig();
        recognitionConfig.readFromParcel(param1Parcel);
        return recognitionConfig;
      }
      
      public RecognitionConfig[] newArray(int param1Int) {
        return new RecognitionConfig[param1Int];
      }
    };
  
  public int audioCapabilities;
  
  public boolean captureRequested;
  
  public byte[] data;
  
  public PhraseRecognitionExtra[] phraseRecognitionExtras;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.captureRequested);
    paramParcel.writeTypedArray(this.phraseRecognitionExtras, 0);
    paramParcel.writeInt(this.audioCapabilities);
    paramParcel.writeByteArray(this.data);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      boolean bool;
      if (paramParcel.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.captureRequested = bool;
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.phraseRecognitionExtras = paramParcel.<PhraseRecognitionExtra>createTypedArray(PhraseRecognitionExtra.CREATOR);
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.audioCapabilities = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.data = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
