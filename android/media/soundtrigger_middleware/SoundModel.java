package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.FileDescriptor;

public class SoundModel implements Parcelable {
  public static final Parcelable.Creator<SoundModel> CREATOR = new Parcelable.Creator<SoundModel>() {
      public SoundModel createFromParcel(Parcel param1Parcel) {
        SoundModel soundModel = new SoundModel();
        soundModel.readFromParcel(param1Parcel);
        return soundModel;
      }
      
      public SoundModel[] newArray(int param1Int) {
        return new SoundModel[param1Int];
      }
    };
  
  public FileDescriptor data;
  
  public int dataSize;
  
  public int type;
  
  public String uuid;
  
  public String vendorUuid;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.type);
    paramParcel.writeString(this.uuid);
    paramParcel.writeString(this.vendorUuid);
    paramParcel.writeRawFileDescriptor(this.data);
    paramParcel.writeInt(this.dataSize);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.type = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.uuid = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.vendorUuid = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.data = paramParcel.readRawFileDescriptor();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.dataSize = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
