package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class SoundTriggerModuleDescriptor implements Parcelable {
  public static final Parcelable.Creator<SoundTriggerModuleDescriptor> CREATOR = (Parcelable.Creator<SoundTriggerModuleDescriptor>)new Object();
  
  public int handle;
  
  public SoundTriggerModuleProperties properties;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.handle);
    if (this.properties != null) {
      paramParcel.writeInt(1);
      this.properties.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.handle = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        this.properties = SoundTriggerModuleProperties.CREATOR.createFromParcel(paramParcel);
      } else {
        this.properties = null;
      } 
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
