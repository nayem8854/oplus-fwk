package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class PhraseRecognitionExtra implements Parcelable {
  public static final Parcelable.Creator<PhraseRecognitionExtra> CREATOR = new Parcelable.Creator<PhraseRecognitionExtra>() {
      public PhraseRecognitionExtra createFromParcel(Parcel param1Parcel) {
        PhraseRecognitionExtra phraseRecognitionExtra = new PhraseRecognitionExtra();
        phraseRecognitionExtra.readFromParcel(param1Parcel);
        return phraseRecognitionExtra;
      }
      
      public PhraseRecognitionExtra[] newArray(int param1Int) {
        return new PhraseRecognitionExtra[param1Int];
      }
    };
  
  public int confidenceLevel;
  
  public int id;
  
  public ConfidenceLevel[] levels;
  
  public int recognitionModes;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.id);
    paramParcel.writeInt(this.recognitionModes);
    paramParcel.writeInt(this.confidenceLevel);
    paramParcel.writeTypedArray(this.levels, 0);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.id = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.recognitionModes = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.confidenceLevel = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.levels = paramParcel.<ConfidenceLevel>createTypedArray(ConfidenceLevel.CREATOR);
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
