package android.media.soundtrigger_middleware;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISoundTriggerCallback extends IInterface {
  void onModuleDied() throws RemoteException;
  
  void onPhraseRecognition(int paramInt, PhraseRecognitionEvent paramPhraseRecognitionEvent) throws RemoteException;
  
  void onRecognition(int paramInt, RecognitionEvent paramRecognitionEvent) throws RemoteException;
  
  void onRecognitionAvailabilityChange(boolean paramBoolean) throws RemoteException;
  
  class Default implements ISoundTriggerCallback {
    public void onRecognition(int param1Int, RecognitionEvent param1RecognitionEvent) throws RemoteException {}
    
    public void onPhraseRecognition(int param1Int, PhraseRecognitionEvent param1PhraseRecognitionEvent) throws RemoteException {}
    
    public void onRecognitionAvailabilityChange(boolean param1Boolean) throws RemoteException {}
    
    public void onModuleDied() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISoundTriggerCallback {
    private static final String DESCRIPTOR = "android.media.soundtrigger_middleware.ISoundTriggerCallback";
    
    static final int TRANSACTION_onModuleDied = 4;
    
    static final int TRANSACTION_onPhraseRecognition = 2;
    
    static final int TRANSACTION_onRecognition = 1;
    
    static final int TRANSACTION_onRecognitionAvailabilityChange = 3;
    
    public Stub() {
      attachInterface(this, "android.media.soundtrigger_middleware.ISoundTriggerCallback");
    }
    
    public static ISoundTriggerCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.soundtrigger_middleware.ISoundTriggerCallback");
      if (iInterface != null && iInterface instanceof ISoundTriggerCallback)
        return (ISoundTriggerCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onModuleDied";
          } 
          return "onRecognitionAvailabilityChange";
        } 
        return "onPhraseRecognition";
      } 
      return "onRecognition";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          boolean bool;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.media.soundtrigger_middleware.ISoundTriggerCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerCallback");
            onModuleDied();
            return true;
          } 
          param1Parcel1.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerCallback");
          if (param1Parcel1.readInt() != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          onRecognitionAvailabilityChange(bool);
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerCallback");
        param1Int1 = param1Parcel1.readInt();
        if (param1Parcel1.readInt() != 0) {
          PhraseRecognitionEvent phraseRecognitionEvent = PhraseRecognitionEvent.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onPhraseRecognition(param1Int1, (PhraseRecognitionEvent)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerCallback");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        RecognitionEvent recognitionEvent = RecognitionEvent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onRecognition(param1Int1, (RecognitionEvent)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ISoundTriggerCallback {
      public static ISoundTriggerCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.soundtrigger_middleware.ISoundTriggerCallback";
      }
      
      public void onRecognition(int param2Int, RecognitionEvent param2RecognitionEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerCallback");
          parcel.writeInt(param2Int);
          if (param2RecognitionEvent != null) {
            parcel.writeInt(1);
            param2RecognitionEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ISoundTriggerCallback.Stub.getDefaultImpl() != null) {
            ISoundTriggerCallback.Stub.getDefaultImpl().onRecognition(param2Int, param2RecognitionEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPhraseRecognition(int param2Int, PhraseRecognitionEvent param2PhraseRecognitionEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerCallback");
          parcel.writeInt(param2Int);
          if (param2PhraseRecognitionEvent != null) {
            parcel.writeInt(1);
            param2PhraseRecognitionEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ISoundTriggerCallback.Stub.getDefaultImpl() != null) {
            ISoundTriggerCallback.Stub.getDefaultImpl().onPhraseRecognition(param2Int, param2PhraseRecognitionEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRecognitionAvailabilityChange(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool1 && ISoundTriggerCallback.Stub.getDefaultImpl() != null) {
            ISoundTriggerCallback.Stub.getDefaultImpl().onRecognitionAvailabilityChange(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onModuleDied() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerCallback");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ISoundTriggerCallback.Stub.getDefaultImpl() != null) {
            ISoundTriggerCallback.Stub.getDefaultImpl().onModuleDied();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISoundTriggerCallback param1ISoundTriggerCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISoundTriggerCallback != null) {
          Proxy.sDefaultImpl = param1ISoundTriggerCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISoundTriggerCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
