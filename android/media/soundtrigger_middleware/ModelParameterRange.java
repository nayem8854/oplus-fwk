package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelParameterRange implements Parcelable {
  public static final Parcelable.Creator<ModelParameterRange> CREATOR = new Parcelable.Creator<ModelParameterRange>() {
      public ModelParameterRange createFromParcel(Parcel param1Parcel) {
        ModelParameterRange modelParameterRange = new ModelParameterRange();
        modelParameterRange.readFromParcel(param1Parcel);
        return modelParameterRange;
      }
      
      public ModelParameterRange[] newArray(int param1Int) {
        return new ModelParameterRange[param1Int];
      }
    };
  
  public int maxInclusive;
  
  public int minInclusive;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.minInclusive);
    paramParcel.writeInt(this.maxInclusive);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.minInclusive = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.maxInclusive = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
