package android.media.soundtrigger_middleware;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISoundTriggerModule extends IInterface {
  void detach() throws RemoteException;
  
  void forceRecognitionEvent(int paramInt) throws RemoteException;
  
  int getModelParameter(int paramInt1, int paramInt2) throws RemoteException;
  
  int loadModel(SoundModel paramSoundModel) throws RemoteException;
  
  int loadPhraseModel(PhraseSoundModel paramPhraseSoundModel) throws RemoteException;
  
  ModelParameterRange queryModelParameterSupport(int paramInt1, int paramInt2) throws RemoteException;
  
  void setModelParameter(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void startRecognition(int paramInt, RecognitionConfig paramRecognitionConfig) throws RemoteException;
  
  void stopRecognition(int paramInt) throws RemoteException;
  
  void unloadModel(int paramInt) throws RemoteException;
  
  class Default implements ISoundTriggerModule {
    public int loadModel(SoundModel param1SoundModel) throws RemoteException {
      return 0;
    }
    
    public int loadPhraseModel(PhraseSoundModel param1PhraseSoundModel) throws RemoteException {
      return 0;
    }
    
    public void unloadModel(int param1Int) throws RemoteException {}
    
    public void startRecognition(int param1Int, RecognitionConfig param1RecognitionConfig) throws RemoteException {}
    
    public void stopRecognition(int param1Int) throws RemoteException {}
    
    public void forceRecognitionEvent(int param1Int) throws RemoteException {}
    
    public void setModelParameter(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public int getModelParameter(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public ModelParameterRange queryModelParameterSupport(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void detach() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISoundTriggerModule {
    private static final String DESCRIPTOR = "android.media.soundtrigger_middleware.ISoundTriggerModule";
    
    static final int TRANSACTION_detach = 10;
    
    static final int TRANSACTION_forceRecognitionEvent = 6;
    
    static final int TRANSACTION_getModelParameter = 8;
    
    static final int TRANSACTION_loadModel = 1;
    
    static final int TRANSACTION_loadPhraseModel = 2;
    
    static final int TRANSACTION_queryModelParameterSupport = 9;
    
    static final int TRANSACTION_setModelParameter = 7;
    
    static final int TRANSACTION_startRecognition = 4;
    
    static final int TRANSACTION_stopRecognition = 5;
    
    static final int TRANSACTION_unloadModel = 3;
    
    public Stub() {
      attachInterface(this, "android.media.soundtrigger_middleware.ISoundTriggerModule");
    }
    
    public static ISoundTriggerModule asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
      if (iInterface != null && iInterface instanceof ISoundTriggerModule)
        return (ISoundTriggerModule)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "detach";
        case 9:
          return "queryModelParameterSupport";
        case 8:
          return "getModelParameter";
        case 7:
          return "setModelParameter";
        case 6:
          return "forceRecognitionEvent";
        case 5:
          return "stopRecognition";
        case 4:
          return "startRecognition";
        case 3:
          return "unloadModel";
        case 2:
          return "loadPhraseModel";
        case 1:
          break;
      } 
      return "loadModel";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        ModelParameterRange modelParameterRange;
        int i;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            detach();
            param1Parcel2.writeNoException();
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            modelParameterRange = queryModelParameterSupport(param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            if (modelParameterRange != null) {
              param1Parcel2.writeInt(1);
              modelParameterRange.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            modelParameterRange.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            param1Int2 = modelParameterRange.readInt();
            param1Int1 = modelParameterRange.readInt();
            param1Int1 = getModelParameter(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 7:
            modelParameterRange.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            param1Int2 = modelParameterRange.readInt();
            param1Int1 = modelParameterRange.readInt();
            i = modelParameterRange.readInt();
            setModelParameter(param1Int2, param1Int1, i);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            modelParameterRange.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            param1Int1 = modelParameterRange.readInt();
            forceRecognitionEvent(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            modelParameterRange.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            param1Int1 = modelParameterRange.readInt();
            stopRecognition(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            modelParameterRange.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            param1Int1 = modelParameterRange.readInt();
            if (modelParameterRange.readInt() != 0) {
              RecognitionConfig recognitionConfig = RecognitionConfig.CREATOR.createFromParcel((Parcel)modelParameterRange);
            } else {
              modelParameterRange = null;
            } 
            startRecognition(param1Int1, (RecognitionConfig)modelParameterRange);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            modelParameterRange.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            param1Int1 = modelParameterRange.readInt();
            unloadModel(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            modelParameterRange.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
            if (modelParameterRange.readInt() != 0) {
              PhraseSoundModel phraseSoundModel = PhraseSoundModel.CREATOR.createFromParcel((Parcel)modelParameterRange);
            } else {
              modelParameterRange = null;
            } 
            param1Int1 = loadPhraseModel((PhraseSoundModel)modelParameterRange);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 1:
            break;
        } 
        modelParameterRange.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerModule");
        if (modelParameterRange.readInt() != 0) {
          SoundModel soundModel = SoundModel.CREATOR.createFromParcel((Parcel)modelParameterRange);
        } else {
          modelParameterRange = null;
        } 
        param1Int1 = loadModel((SoundModel)modelParameterRange);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      param1Parcel2.writeString("android.media.soundtrigger_middleware.ISoundTriggerModule");
      return true;
    }
    
    private static class Proxy implements ISoundTriggerModule {
      public static ISoundTriggerModule sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.soundtrigger_middleware.ISoundTriggerModule";
      }
      
      public int loadModel(SoundModel param2SoundModel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          if (param2SoundModel != null) {
            parcel1.writeInt(1);
            param2SoundModel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null)
            return ISoundTriggerModule.Stub.getDefaultImpl().loadModel(param2SoundModel); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int loadPhraseModel(PhraseSoundModel param2PhraseSoundModel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          if (param2PhraseSoundModel != null) {
            parcel1.writeInt(1);
            param2PhraseSoundModel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null)
            return ISoundTriggerModule.Stub.getDefaultImpl().loadPhraseModel(param2PhraseSoundModel); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unloadModel(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null) {
            ISoundTriggerModule.Stub.getDefaultImpl().unloadModel(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startRecognition(int param2Int, RecognitionConfig param2RecognitionConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          parcel1.writeInt(param2Int);
          if (param2RecognitionConfig != null) {
            parcel1.writeInt(1);
            param2RecognitionConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null) {
            ISoundTriggerModule.Stub.getDefaultImpl().startRecognition(param2Int, param2RecognitionConfig);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopRecognition(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null) {
            ISoundTriggerModule.Stub.getDefaultImpl().stopRecognition(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceRecognitionEvent(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null) {
            ISoundTriggerModule.Stub.getDefaultImpl().forceRecognitionEvent(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setModelParameter(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null) {
            ISoundTriggerModule.Stub.getDefaultImpl().setModelParameter(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getModelParameter(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null) {
            param2Int1 = ISoundTriggerModule.Stub.getDefaultImpl().getModelParameter(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ModelParameterRange queryModelParameterSupport(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ModelParameterRange modelParameterRange;
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null) {
            modelParameterRange = ISoundTriggerModule.Stub.getDefaultImpl().queryModelParameterSupport(param2Int1, param2Int2);
            return modelParameterRange;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            modelParameterRange = ModelParameterRange.CREATOR.createFromParcel(parcel2);
          } else {
            modelParameterRange = null;
          } 
          return modelParameterRange;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void detach() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerModule");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerModule.Stub.getDefaultImpl() != null) {
            ISoundTriggerModule.Stub.getDefaultImpl().detach();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISoundTriggerModule param1ISoundTriggerModule) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISoundTriggerModule != null) {
          Proxy.sDefaultImpl = param1ISoundTriggerModule;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISoundTriggerModule getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
