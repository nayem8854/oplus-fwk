package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class PhraseSoundModel implements Parcelable {
  public static final Parcelable.Creator<PhraseSoundModel> CREATOR = new Parcelable.Creator<PhraseSoundModel>() {
      public PhraseSoundModel createFromParcel(Parcel param1Parcel) {
        PhraseSoundModel phraseSoundModel = new PhraseSoundModel();
        phraseSoundModel.readFromParcel(param1Parcel);
        return phraseSoundModel;
      }
      
      public PhraseSoundModel[] newArray(int param1Int) {
        return new PhraseSoundModel[param1Int];
      }
    };
  
  public SoundModel common;
  
  public Phrase[] phrases;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    if (this.common != null) {
      paramParcel.writeInt(1);
      this.common.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeTypedArray(this.phrases, 0);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      if (paramParcel.readInt() != 0) {
        this.common = SoundModel.CREATOR.createFromParcel(paramParcel);
      } else {
        this.common = null;
      } 
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.phrases = paramParcel.<Phrase>createTypedArray(Phrase.CREATOR);
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
