package android.media.soundtrigger_middleware;

public @interface AudioCapabilities {
  public static final int ECHO_CANCELLATION = 1;
  
  public static final int NOISE_SUPPRESSION = 2;
}
