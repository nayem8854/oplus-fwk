package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class PhraseRecognitionEvent implements Parcelable {
  public static final Parcelable.Creator<PhraseRecognitionEvent> CREATOR = new Parcelable.Creator<PhraseRecognitionEvent>() {
      public PhraseRecognitionEvent createFromParcel(Parcel param1Parcel) {
        PhraseRecognitionEvent phraseRecognitionEvent = new PhraseRecognitionEvent();
        phraseRecognitionEvent.readFromParcel(param1Parcel);
        return phraseRecognitionEvent;
      }
      
      public PhraseRecognitionEvent[] newArray(int param1Int) {
        return new PhraseRecognitionEvent[param1Int];
      }
    };
  
  public RecognitionEvent common;
  
  public PhraseRecognitionExtra[] phraseExtras;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    if (this.common != null) {
      paramParcel.writeInt(1);
      this.common.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeTypedArray(this.phraseExtras, 0);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      if (paramParcel.readInt() != 0) {
        this.common = RecognitionEvent.CREATOR.createFromParcel(paramParcel);
      } else {
        this.common = null;
      } 
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.phraseExtras = paramParcel.<PhraseRecognitionExtra>createTypedArray(PhraseRecognitionExtra.CREATOR);
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
