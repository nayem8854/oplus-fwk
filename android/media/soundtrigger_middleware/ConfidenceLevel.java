package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class ConfidenceLevel implements Parcelable {
  public static final Parcelable.Creator<ConfidenceLevel> CREATOR = new Parcelable.Creator<ConfidenceLevel>() {
      public ConfidenceLevel createFromParcel(Parcel param1Parcel) {
        ConfidenceLevel confidenceLevel = new ConfidenceLevel();
        confidenceLevel.readFromParcel(param1Parcel);
        return confidenceLevel;
      }
      
      public ConfidenceLevel[] newArray(int param1Int) {
        return new ConfidenceLevel[param1Int];
      }
    };
  
  public int levelPercent;
  
  public int userId;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.userId);
    paramParcel.writeInt(this.levelPercent);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.userId = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.levelPercent = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
