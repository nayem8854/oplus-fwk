package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class SoundTriggerModuleProperties implements Parcelable {
  public static final Parcelable.Creator<SoundTriggerModuleProperties> CREATOR = new Parcelable.Creator<SoundTriggerModuleProperties>() {
      public SoundTriggerModuleProperties createFromParcel(Parcel param1Parcel) {
        SoundTriggerModuleProperties soundTriggerModuleProperties = new SoundTriggerModuleProperties();
        soundTriggerModuleProperties.readFromParcel(param1Parcel);
        return soundTriggerModuleProperties;
      }
      
      public SoundTriggerModuleProperties[] newArray(int param1Int) {
        return new SoundTriggerModuleProperties[param1Int];
      }
    };
  
  public int audioCapabilities;
  
  public boolean captureTransition;
  
  public boolean concurrentCapture;
  
  public String description;
  
  public String implementor;
  
  public int maxBufferMs;
  
  public int maxKeyPhrases;
  
  public int maxSoundModels;
  
  public int maxUsers;
  
  public int powerConsumptionMw;
  
  public int recognitionModes;
  
  public String supportedModelArch;
  
  public boolean triggerInEvent;
  
  public String uuid;
  
  public int version;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeString(this.implementor);
    paramParcel.writeString(this.description);
    paramParcel.writeInt(this.version);
    paramParcel.writeString(this.uuid);
    paramParcel.writeString(this.supportedModelArch);
    paramParcel.writeInt(this.maxSoundModels);
    paramParcel.writeInt(this.maxKeyPhrases);
    paramParcel.writeInt(this.maxUsers);
    paramParcel.writeInt(this.recognitionModes);
    paramParcel.writeInt(this.captureTransition);
    paramParcel.writeInt(this.maxBufferMs);
    paramParcel.writeInt(this.concurrentCapture);
    paramParcel.writeInt(this.triggerInEvent);
    paramParcel.writeInt(this.powerConsumptionMw);
    paramParcel.writeInt(this.audioCapabilities);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      boolean bool2;
      this.implementor = paramParcel.readString();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.description = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.version = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.uuid = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.supportedModelArch = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.maxSoundModels = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.maxKeyPhrases = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.maxUsers = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.recognitionModes = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      k = paramParcel.readInt();
      boolean bool1 = true;
      if (k != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.captureTransition = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.maxBufferMs = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.concurrentCapture = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      this.triggerInEvent = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.powerConsumptionMw = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.audioCapabilities = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
