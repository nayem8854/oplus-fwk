package android.media.soundtrigger_middleware;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISoundTriggerMiddlewareService extends IInterface {
  ISoundTriggerModule attach(int paramInt, ISoundTriggerCallback paramISoundTriggerCallback) throws RemoteException;
  
  SoundTriggerModuleDescriptor[] listModules() throws RemoteException;
  
  class Default implements ISoundTriggerMiddlewareService {
    public SoundTriggerModuleDescriptor[] listModules() throws RemoteException {
      return null;
    }
    
    public ISoundTriggerModule attach(int param1Int, ISoundTriggerCallback param1ISoundTriggerCallback) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISoundTriggerMiddlewareService {
    private static final String DESCRIPTOR = "android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService";
    
    static final int TRANSACTION_attach = 2;
    
    static final int TRANSACTION_listModules = 1;
    
    public Stub() {
      attachInterface(this, "android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService");
    }
    
    public static ISoundTriggerMiddlewareService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService");
      if (iInterface != null && iInterface instanceof ISoundTriggerMiddlewareService)
        return (ISoundTriggerMiddlewareService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "attach";
      } 
      return "listModules";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ISoundTriggerModule iSoundTriggerModule;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService");
        param1Int1 = param1Parcel1.readInt();
        ISoundTriggerCallback iSoundTriggerCallback = ISoundTriggerCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
        iSoundTriggerModule = attach(param1Int1, iSoundTriggerCallback);
        param1Parcel2.writeNoException();
        if (iSoundTriggerModule != null) {
          IBinder iBinder = iSoundTriggerModule.asBinder();
        } else {
          iSoundTriggerModule = null;
        } 
        param1Parcel2.writeStrongBinder((IBinder)iSoundTriggerModule);
        return true;
      } 
      iSoundTriggerModule.enforceInterface("android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService");
      SoundTriggerModuleDescriptor[] arrayOfSoundTriggerModuleDescriptor = listModules();
      param1Parcel2.writeNoException();
      param1Parcel2.writeTypedArray(arrayOfSoundTriggerModuleDescriptor, 1);
      return true;
    }
    
    private static class Proxy implements ISoundTriggerMiddlewareService {
      public static ISoundTriggerMiddlewareService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService";
      }
      
      public SoundTriggerModuleDescriptor[] listModules() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerMiddlewareService.Stub.getDefaultImpl() != null)
            return ISoundTriggerMiddlewareService.Stub.getDefaultImpl().listModules(); 
          parcel2.readException();
          return parcel2.<SoundTriggerModuleDescriptor>createTypedArray(SoundTriggerModuleDescriptor.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ISoundTriggerModule attach(int param2Int, ISoundTriggerCallback param2ISoundTriggerCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService");
          parcel1.writeInt(param2Int);
          if (param2ISoundTriggerCallback != null) {
            iBinder = param2ISoundTriggerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerMiddlewareService.Stub.getDefaultImpl() != null)
            return ISoundTriggerMiddlewareService.Stub.getDefaultImpl().attach(param2Int, param2ISoundTriggerCallback); 
          parcel2.readException();
          return ISoundTriggerModule.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISoundTriggerMiddlewareService param1ISoundTriggerMiddlewareService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISoundTriggerMiddlewareService != null) {
          Proxy.sDefaultImpl = param1ISoundTriggerMiddlewareService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISoundTriggerMiddlewareService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
