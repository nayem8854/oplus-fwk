package android.media.soundtrigger_middleware;

import android.media.audio.common.AudioConfig;
import android.os.Parcel;
import android.os.Parcelable;

public class RecognitionEvent implements Parcelable {
  public static final Parcelable.Creator<RecognitionEvent> CREATOR = new Parcelable.Creator<RecognitionEvent>() {
      public RecognitionEvent createFromParcel(Parcel param1Parcel) {
        RecognitionEvent recognitionEvent = new RecognitionEvent();
        recognitionEvent.readFromParcel(param1Parcel);
        return recognitionEvent;
      }
      
      public RecognitionEvent[] newArray(int param1Int) {
        return new RecognitionEvent[param1Int];
      }
    };
  
  public AudioConfig audioConfig;
  
  public boolean captureAvailable;
  
  public int captureDelayMs;
  
  public int capturePreambleMs;
  
  public int captureSession;
  
  public byte[] data;
  
  public int status;
  
  public boolean triggerInData;
  
  public int type;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.status);
    paramParcel.writeInt(this.type);
    paramParcel.writeInt(this.captureAvailable);
    paramParcel.writeInt(this.captureSession);
    paramParcel.writeInt(this.captureDelayMs);
    paramParcel.writeInt(this.capturePreambleMs);
    paramParcel.writeInt(this.triggerInData);
    if (this.audioConfig != null) {
      paramParcel.writeInt(1);
      this.audioConfig.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeByteArray(this.data);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      boolean bool2;
      this.status = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.type = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      k = paramParcel.readInt();
      boolean bool1 = true;
      if (k != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.captureAvailable = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.captureSession = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.captureDelayMs = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.capturePreambleMs = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      this.triggerInData = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        this.audioConfig = AudioConfig.CREATOR.createFromParcel(paramParcel);
      } else {
        this.audioConfig = null;
      } 
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.data = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
