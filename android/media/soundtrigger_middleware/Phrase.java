package android.media.soundtrigger_middleware;

import android.os.Parcel;
import android.os.Parcelable;

public class Phrase implements Parcelable {
  public static final Parcelable.Creator<Phrase> CREATOR = new Parcelable.Creator<Phrase>() {
      public Phrase createFromParcel(Parcel param1Parcel) {
        Phrase phrase = new Phrase();
        phrase.readFromParcel(param1Parcel);
        return phrase;
      }
      
      public Phrase[] newArray(int param1Int) {
        return new Phrase[param1Int];
      }
    };
  
  public int id;
  
  public String locale;
  
  public int recognitionModes;
  
  public String text;
  
  public int[] users;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.id);
    paramParcel.writeInt(this.recognitionModes);
    paramParcel.writeIntArray(this.users);
    paramParcel.writeString(this.locale);
    paramParcel.writeString(this.text);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.id = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.recognitionModes = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.users = paramParcel.createIntArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.locale = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.text = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
