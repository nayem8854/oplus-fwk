package android.media;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.accessibility.CaptioningManager;
import java.util.Vector;

public class SubtitleController {
  static final boolean $assertionsDisabled = false;
  
  private static final int WHAT_HIDE = 2;
  
  private static final int WHAT_SELECT_DEFAULT_TRACK = 4;
  
  private static final int WHAT_SELECT_TRACK = 3;
  
  private static final int WHAT_SHOW = 1;
  
  private Anchor mAnchor;
  
  private final Handler.Callback mCallback = (Handler.Callback)new Object(this);
  
  private CaptioningManager.CaptioningChangeListener mCaptioningChangeListener = (CaptioningManager.CaptioningChangeListener)new Object(this);
  
  private CaptioningManager mCaptioningManager;
  
  private Handler mHandler;
  
  private Listener mListener;
  
  private Vector<Renderer> mRenderers;
  
  private SubtitleTrack mSelectedTrack;
  
  private boolean mShowing;
  
  private MediaTimeProvider mTimeProvider;
  
  private boolean mTrackIsExplicit;
  
  private Vector<SubtitleTrack> mTracks;
  
  private boolean mVisibilityIsExplicit;
  
  protected void finalize() throws Throwable {
    this.mCaptioningManager.removeCaptioningChangeListener(this.mCaptioningChangeListener);
    super.finalize();
  }
  
  public SubtitleTrack[] getTracks() {
    synchronized (this.mTracks) {
      SubtitleTrack[] arrayOfSubtitleTrack = new SubtitleTrack[this.mTracks.size()];
      this.mTracks.toArray(arrayOfSubtitleTrack);
      return arrayOfSubtitleTrack;
    } 
  }
  
  public SubtitleTrack getSelectedTrack() {
    return this.mSelectedTrack;
  }
  
  private SubtitleTrack.RenderingWidget getRenderingWidget() {
    SubtitleTrack subtitleTrack = this.mSelectedTrack;
    if (subtitleTrack == null)
      return null; 
    return subtitleTrack.getRenderingWidget();
  }
  
  public boolean selectTrack(SubtitleTrack paramSubtitleTrack) {
    if (paramSubtitleTrack != null && !this.mTracks.contains(paramSubtitleTrack))
      return false; 
    Handler handler = this.mHandler;
    if (handler != null)
      processOnAnchor(handler.obtainMessage(3, paramSubtitleTrack)); 
    return true;
  }
  
  private void doSelectTrack(SubtitleTrack paramSubtitleTrack) {
    this.mTrackIsExplicit = true;
    SubtitleTrack subtitleTrack2 = this.mSelectedTrack;
    if (subtitleTrack2 == paramSubtitleTrack)
      return; 
    if (subtitleTrack2 != null) {
      subtitleTrack2.hide();
      this.mSelectedTrack.setTimeProvider(null);
    } 
    this.mSelectedTrack = paramSubtitleTrack;
    Anchor anchor = this.mAnchor;
    if (anchor != null)
      anchor.setSubtitleWidget(getRenderingWidget()); 
    SubtitleTrack subtitleTrack1 = this.mSelectedTrack;
    if (subtitleTrack1 != null) {
      subtitleTrack1.setTimeProvider(this.mTimeProvider);
      this.mSelectedTrack.show();
    } 
    Listener listener = this.mListener;
    if (listener != null)
      listener.onSubtitleTrackSelected(paramSubtitleTrack); 
  }
  
  public SubtitleTrack getDefaultTrack() {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: iconst_m1
    //   3: istore_2
    //   4: aload_0
    //   5: getfield mCaptioningManager : Landroid/view/accessibility/CaptioningManager;
    //   8: invokevirtual getLocale : ()Ljava/util/Locale;
    //   11: astore_3
    //   12: aload_3
    //   13: ifnonnull -> 24
    //   16: invokestatic getDefault : ()Ljava/util/Locale;
    //   19: astore #4
    //   21: goto -> 27
    //   24: aload_3
    //   25: astore #4
    //   27: aload_0
    //   28: getfield mCaptioningManager : Landroid/view/accessibility/CaptioningManager;
    //   31: invokevirtual isEnabled : ()Z
    //   34: istore #5
    //   36: aload_0
    //   37: getfield mTracks : Ljava/util/Vector;
    //   40: astore #6
    //   42: aload #6
    //   44: monitorenter
    //   45: aload_0
    //   46: getfield mTracks : Ljava/util/Vector;
    //   49: invokevirtual iterator : ()Ljava/util/Iterator;
    //   52: astore #7
    //   54: aload #7
    //   56: invokeinterface hasNext : ()Z
    //   61: ifeq -> 369
    //   64: aload #7
    //   66: invokeinterface next : ()Ljava/lang/Object;
    //   71: checkcast android/media/SubtitleTrack
    //   74: astore #8
    //   76: aload #8
    //   78: invokevirtual getFormat : ()Landroid/media/MediaFormat;
    //   81: astore #9
    //   83: aload #9
    //   85: ldc 'language'
    //   87: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
    //   90: astore #10
    //   92: aload #9
    //   94: ldc 'is-forced-subtitle'
    //   96: iconst_0
    //   97: invokevirtual getInteger : (Ljava/lang/String;I)I
    //   100: ifeq -> 109
    //   103: iconst_1
    //   104: istore #11
    //   106: goto -> 112
    //   109: iconst_0
    //   110: istore #11
    //   112: aload #9
    //   114: ldc_w 'is-autoselect'
    //   117: iconst_1
    //   118: invokevirtual getInteger : (Ljava/lang/String;I)I
    //   121: ifeq -> 130
    //   124: iconst_1
    //   125: istore #12
    //   127: goto -> 133
    //   130: iconst_0
    //   131: istore #12
    //   133: aload #9
    //   135: ldc_w 'is-default'
    //   138: iconst_0
    //   139: invokevirtual getInteger : (Ljava/lang/String;I)I
    //   142: ifeq -> 151
    //   145: iconst_1
    //   146: istore #13
    //   148: goto -> 154
    //   151: iconst_0
    //   152: istore #13
    //   154: aload #4
    //   156: ifnull -> 208
    //   159: aload #4
    //   161: invokevirtual getLanguage : ()Ljava/lang/String;
    //   164: ldc_w ''
    //   167: invokevirtual equals : (Ljava/lang/Object;)Z
    //   170: ifne -> 208
    //   173: aload #4
    //   175: invokevirtual getISO3Language : ()Ljava/lang/String;
    //   178: aload #10
    //   180: invokevirtual equals : (Ljava/lang/Object;)Z
    //   183: ifne -> 208
    //   186: aload #4
    //   188: invokevirtual getLanguage : ()Ljava/lang/String;
    //   191: aload #10
    //   193: invokevirtual equals : (Ljava/lang/Object;)Z
    //   196: ifeq -> 202
    //   199: goto -> 208
    //   202: iconst_0
    //   203: istore #14
    //   205: goto -> 211
    //   208: iconst_1
    //   209: istore #14
    //   211: iload #11
    //   213: ifeq -> 222
    //   216: iconst_0
    //   217: istore #15
    //   219: goto -> 226
    //   222: bipush #8
    //   224: istore #15
    //   226: aload_3
    //   227: ifnonnull -> 241
    //   230: iload #13
    //   232: ifeq -> 241
    //   235: iconst_4
    //   236: istore #16
    //   238: goto -> 244
    //   241: iconst_0
    //   242: istore #16
    //   244: iload #12
    //   246: ifeq -> 255
    //   249: iconst_0
    //   250: istore #17
    //   252: goto -> 258
    //   255: iconst_2
    //   256: istore #17
    //   258: iload #14
    //   260: ifeq -> 269
    //   263: iconst_1
    //   264: istore #18
    //   266: goto -> 272
    //   269: iconst_0
    //   270: istore #18
    //   272: iload #15
    //   274: iload #16
    //   276: iadd
    //   277: iload #17
    //   279: iadd
    //   280: iload #18
    //   282: iadd
    //   283: istore #15
    //   285: iload #5
    //   287: iconst_1
    //   288: ixor
    //   289: ifeq -> 300
    //   292: iload #11
    //   294: ifne -> 300
    //   297: goto -> 54
    //   300: aload_3
    //   301: ifnonnull -> 309
    //   304: iload #13
    //   306: ifne -> 340
    //   309: aload_1
    //   310: astore #9
    //   312: iload_2
    //   313: istore #13
    //   315: iload #14
    //   317: ifeq -> 360
    //   320: iload #12
    //   322: ifne -> 340
    //   325: iload #11
    //   327: ifne -> 340
    //   330: aload_1
    //   331: astore #9
    //   333: iload_2
    //   334: istore #13
    //   336: aload_3
    //   337: ifnull -> 360
    //   340: aload_1
    //   341: astore #9
    //   343: iload_2
    //   344: istore #13
    //   346: iload #15
    //   348: iload_2
    //   349: if_icmple -> 360
    //   352: iload #15
    //   354: istore #13
    //   356: aload #8
    //   358: astore #9
    //   360: aload #9
    //   362: astore_1
    //   363: iload #13
    //   365: istore_2
    //   366: goto -> 54
    //   369: aload #6
    //   371: monitorexit
    //   372: aload_1
    //   373: areturn
    //   374: astore #4
    //   376: aload #6
    //   378: monitorexit
    //   379: aload #4
    //   381: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #223	-> 0
    //   #224	-> 2
    //   #226	-> 4
    //   #227	-> 12
    //   #228	-> 12
    //   #229	-> 16
    //   #228	-> 24
    //   #231	-> 27
    //   #233	-> 36
    //   #234	-> 45
    //   #235	-> 76
    //   #236	-> 83
    //   #237	-> 92
    //   #238	-> 92
    //   #239	-> 112
    //   #240	-> 112
    //   #241	-> 133
    //   #242	-> 133
    //   #244	-> 154
    //   #246	-> 159
    //   #247	-> 173
    //   #248	-> 186
    //   #250	-> 211
    //   #251	-> 226
    //   #252	-> 244
    //   #254	-> 285
    //   #255	-> 297
    //   #259	-> 300
    //   #262	-> 340
    //   #263	-> 352
    //   #264	-> 356
    //   #267	-> 360
    //   #268	-> 369
    //   #269	-> 372
    //   #268	-> 374
    // Exception table:
    //   from	to	target	type
    //   45	54	374	finally
    //   54	76	374	finally
    //   76	83	374	finally
    //   83	92	374	finally
    //   92	103	374	finally
    //   112	124	374	finally
    //   133	145	374	finally
    //   159	173	374	finally
    //   173	186	374	finally
    //   186	199	374	finally
    //   369	372	374	finally
    //   376	379	374	finally
  }
  
  public SubtitleController(Context paramContext, MediaTimeProvider paramMediaTimeProvider, Listener paramListener) {
    this.mTrackIsExplicit = false;
    this.mVisibilityIsExplicit = false;
    this.mTimeProvider = paramMediaTimeProvider;
    this.mListener = paramListener;
    this.mRenderers = new Vector<>();
    this.mShowing = false;
    this.mTracks = new Vector<>();
    this.mCaptioningManager = (CaptioningManager)paramContext.getSystemService("captioning");
  }
  
  public void selectDefaultTrack() {
    Handler handler = this.mHandler;
    if (handler != null)
      processOnAnchor(handler.obtainMessage(4)); 
  }
  
  private void doSelectDefaultTrack() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTrackIsExplicit : Z
    //   4: ifeq -> 83
    //   7: aload_0
    //   8: getfield mVisibilityIsExplicit : Z
    //   11: ifne -> 82
    //   14: aload_0
    //   15: getfield mCaptioningManager : Landroid/view/accessibility/CaptioningManager;
    //   18: invokevirtual isEnabled : ()Z
    //   21: ifne -> 73
    //   24: aload_0
    //   25: getfield mSelectedTrack : Landroid/media/SubtitleTrack;
    //   28: astore_1
    //   29: aload_1
    //   30: ifnull -> 49
    //   33: aload_1
    //   34: invokevirtual getFormat : ()Landroid/media/MediaFormat;
    //   37: ldc 'is-forced-subtitle'
    //   39: iconst_0
    //   40: invokevirtual getInteger : (Ljava/lang/String;I)I
    //   43: ifeq -> 49
    //   46: goto -> 73
    //   49: aload_0
    //   50: getfield mSelectedTrack : Landroid/media/SubtitleTrack;
    //   53: astore_1
    //   54: aload_1
    //   55: ifnull -> 77
    //   58: aload_1
    //   59: invokevirtual getTrackType : ()I
    //   62: iconst_4
    //   63: if_icmpne -> 77
    //   66: aload_0
    //   67: invokevirtual hide : ()V
    //   70: goto -> 77
    //   73: aload_0
    //   74: invokevirtual show : ()V
    //   77: aload_0
    //   78: iconst_0
    //   79: putfield mVisibilityIsExplicit : Z
    //   82: return
    //   83: aload_0
    //   84: invokevirtual getDefaultTrack : ()Landroid/media/SubtitleTrack;
    //   87: astore_1
    //   88: aload_1
    //   89: ifnull -> 119
    //   92: aload_0
    //   93: aload_1
    //   94: invokevirtual selectTrack : (Landroid/media/SubtitleTrack;)Z
    //   97: pop
    //   98: aload_0
    //   99: iconst_0
    //   100: putfield mTrackIsExplicit : Z
    //   103: aload_0
    //   104: getfield mVisibilityIsExplicit : Z
    //   107: ifne -> 119
    //   110: aload_0
    //   111: invokevirtual show : ()V
    //   114: aload_0
    //   115: iconst_0
    //   116: putfield mVisibilityIsExplicit : Z
    //   119: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #289	-> 0
    //   #292	-> 7
    //   #293	-> 14
    //   #295	-> 33
    //   #298	-> 49
    //   #299	-> 58
    //   #300	-> 66
    //   #297	-> 73
    //   #302	-> 77
    //   #304	-> 82
    //   #310	-> 83
    //   #311	-> 88
    //   #312	-> 92
    //   #313	-> 98
    //   #314	-> 103
    //   #315	-> 110
    //   #316	-> 114
    //   #319	-> 119
  }
  
  public void reset() {
    checkAnchorLooper();
    hide();
    selectTrack(null);
    this.mTracks.clear();
    this.mTrackIsExplicit = false;
    this.mVisibilityIsExplicit = false;
    this.mCaptioningManager.removeCaptioningChangeListener(this.mCaptioningChangeListener);
  }
  
  public SubtitleTrack addTrack(MediaFormat paramMediaFormat) {
    synchronized (this.mRenderers) {
      for (Renderer renderer : this.mRenderers) {
        if (renderer.supports(paramMediaFormat)) {
          SubtitleTrack subtitleTrack = renderer.createTrack(paramMediaFormat);
          if (subtitleTrack != null)
            synchronized (this.mTracks) {
              if (this.mTracks.size() == 0)
                this.mCaptioningManager.addCaptioningChangeListener(this.mCaptioningChangeListener); 
              this.mTracks.add(subtitleTrack);
              return subtitleTrack;
            }  
        } 
      } 
      return null;
    } 
  }
  
  public void show() {
    Handler handler = this.mHandler;
    if (handler != null)
      processOnAnchor(handler.obtainMessage(1)); 
  }
  
  private void doShow() {
    this.mShowing = true;
    this.mVisibilityIsExplicit = true;
    SubtitleTrack subtitleTrack = this.mSelectedTrack;
    if (subtitleTrack != null)
      subtitleTrack.show(); 
  }
  
  public void hide() {
    Handler handler = this.mHandler;
    if (handler != null)
      processOnAnchor(handler.obtainMessage(2)); 
  }
  
  private void doHide() {
    this.mVisibilityIsExplicit = true;
    SubtitleTrack subtitleTrack = this.mSelectedTrack;
    if (subtitleTrack != null)
      subtitleTrack.hide(); 
    this.mShowing = false;
  }
  
  public static interface Anchor {
    Looper getSubtitleLooper();
    
    void setSubtitleWidget(SubtitleTrack.RenderingWidget param1RenderingWidget);
  }
  
  public static interface Listener {
    void onSubtitleTrackSelected(SubtitleTrack param1SubtitleTrack);
  }
  
  public static abstract class Renderer {
    public abstract SubtitleTrack createTrack(MediaFormat param1MediaFormat);
    
    public abstract boolean supports(MediaFormat param1MediaFormat);
  }
  
  public void registerRenderer(Renderer paramRenderer) {
    synchronized (this.mRenderers) {
      if (!this.mRenderers.contains(paramRenderer))
        this.mRenderers.add(paramRenderer); 
      return;
    } 
  }
  
  public boolean hasRendererFor(MediaFormat paramMediaFormat) {
    synchronized (this.mRenderers) {
      for (Renderer renderer : this.mRenderers) {
        if (renderer.supports(paramMediaFormat))
          return true; 
      } 
      return false;
    } 
  }
  
  public void setAnchor(Anchor paramAnchor) {
    Anchor anchor = this.mAnchor;
    if (anchor == paramAnchor)
      return; 
    if (anchor != null) {
      checkAnchorLooper();
      this.mAnchor.setSubtitleWidget(null);
    } 
    this.mAnchor = paramAnchor;
    this.mHandler = null;
    if (paramAnchor != null) {
      this.mHandler = new Handler(this.mAnchor.getSubtitleLooper(), this.mCallback);
      checkAnchorLooper();
      this.mAnchor.setSubtitleWidget(getRenderingWidget());
    } 
  }
  
  private void checkAnchorLooper() {}
  
  private void processOnAnchor(Message paramMessage) {
    if (Looper.myLooper() == this.mHandler.getLooper()) {
      this.mHandler.dispatchMessage(paramMessage);
    } else {
      this.mHandler.sendMessage(paramMessage);
    } 
  }
}
