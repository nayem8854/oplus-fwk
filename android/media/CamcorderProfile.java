package android.media;

import android.hardware.Camera;

public class CamcorderProfile {
  public static final int QUALITY_1080P = 6;
  
  public static final int QUALITY_2160P = 8;
  
  public static final int QUALITY_2K = 12;
  
  public static final int QUALITY_480P = 4;
  
  public static final int QUALITY_4KDCI = 10;
  
  public static final int QUALITY_720P = 5;
  
  public static final int QUALITY_8KUHD = 3001;
  
  public static final int QUALITY_CIF = 3;
  
  public static final int QUALITY_HIGH = 1;
  
  public static final int QUALITY_HIGH_SPEED_1080P = 2004;
  
  public static final int QUALITY_HIGH_SPEED_2160P = 2005;
  
  public static final int QUALITY_HIGH_SPEED_480P = 2002;
  
  public static final int QUALITY_HIGH_SPEED_4KDCI = 2008;
  
  public static final int QUALITY_HIGH_SPEED_720P = 2003;
  
  public static final int QUALITY_HIGH_SPEED_CIF = 2006;
  
  public static final int QUALITY_HIGH_SPEED_HIGH = 2001;
  
  private static final int QUALITY_HIGH_SPEED_LIST_END = 2008;
  
  private static final int QUALITY_HIGH_SPEED_LIST_START = 2000;
  
  public static final int QUALITY_HIGH_SPEED_LOW = 2000;
  
  public static final int QUALITY_HIGH_SPEED_VGA = 2007;
  
  private static final int QUALITY_LIST_END = 12;
  
  private static final int QUALITY_LIST_START = 0;
  
  public static final int QUALITY_LOW = 0;
  
  public static final int QUALITY_QCIF = 2;
  
  public static final int QUALITY_QHD = 11;
  
  public static final int QUALITY_QVGA = 7;
  
  public static final int QUALITY_TIME_LAPSE_1080P = 1006;
  
  public static final int QUALITY_TIME_LAPSE_2160P = 1008;
  
  public static final int QUALITY_TIME_LAPSE_2K = 1012;
  
  public static final int QUALITY_TIME_LAPSE_480P = 1004;
  
  public static final int QUALITY_TIME_LAPSE_4KDCI = 1010;
  
  public static final int QUALITY_TIME_LAPSE_720P = 1005;
  
  public static final int QUALITY_TIME_LAPSE_8KUHD = 3002;
  
  public static final int QUALITY_TIME_LAPSE_CIF = 1003;
  
  public static final int QUALITY_TIME_LAPSE_HIGH = 1001;
  
  private static final int QUALITY_TIME_LAPSE_LIST_END = 1012;
  
  private static final int QUALITY_TIME_LAPSE_LIST_START = 1000;
  
  public static final int QUALITY_TIME_LAPSE_LOW = 1000;
  
  public static final int QUALITY_TIME_LAPSE_QCIF = 1002;
  
  public static final int QUALITY_TIME_LAPSE_QHD = 1011;
  
  public static final int QUALITY_TIME_LAPSE_QVGA = 1007;
  
  public static final int QUALITY_TIME_LAPSE_VGA = 1009;
  
  private static final int QUALITY_VENDOR_LIST_END = 3002;
  
  private static final int QUALITY_VENDOR_LIST_START = 3001;
  
  public static final int QUALITY_VGA = 9;
  
  public int audioBitRate;
  
  public int audioChannels;
  
  public int audioCodec;
  
  public int audioSampleRate;
  
  public int duration;
  
  public int fileFormat;
  
  public int quality;
  
  public int videoBitRate;
  
  public int videoCodec;
  
  public int videoFrameHeight;
  
  public int videoFrameRate;
  
  public int videoFrameWidth;
  
  public static CamcorderProfile get(int paramInt) {
    int i = Camera.getNumberOfCameras();
    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
    for (byte b = 0; b < i; b++) {
      Camera.getCameraInfo(b, cameraInfo);
      if (cameraInfo.facing == 0)
        return get(b, paramInt); 
    } 
    return null;
  }
  
  public static CamcorderProfile get(int paramInt1, int paramInt2) {
    if ((paramInt2 >= 0 && paramInt2 <= 12) || (paramInt2 >= 1000 && paramInt2 <= 1012) || (paramInt2 >= 2000 && paramInt2 <= 2008) || (paramInt2 >= 3001 && paramInt2 <= 3002))
      return native_get_camcorder_profile(paramInt1, paramInt2); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported quality level: ");
    stringBuilder.append(paramInt2);
    String str = stringBuilder.toString();
    throw new IllegalArgumentException(str);
  }
  
  public static boolean hasProfile(int paramInt) {
    int i = Camera.getNumberOfCameras();
    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
    for (byte b = 0; b < i; b++) {
      Camera.getCameraInfo(b, cameraInfo);
      if (cameraInfo.facing == 0)
        return hasProfile(b, paramInt); 
    } 
    return false;
  }
  
  public static boolean hasProfile(int paramInt1, int paramInt2) {
    return native_has_camcorder_profile(paramInt1, paramInt2);
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private CamcorderProfile(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12) {
    this.duration = paramInt1;
    this.quality = paramInt2;
    this.fileFormat = paramInt3;
    this.videoCodec = paramInt4;
    this.videoBitRate = paramInt5;
    this.videoFrameRate = paramInt6;
    this.videoFrameWidth = paramInt7;
    this.videoFrameHeight = paramInt8;
    this.audioCodec = paramInt9;
    this.audioBitRate = paramInt10;
    this.audioSampleRate = paramInt11;
    this.audioChannels = paramInt12;
  }
  
  private static final native CamcorderProfile native_get_camcorder_profile(int paramInt1, int paramInt2);
  
  private static final native boolean native_has_camcorder_profile(int paramInt1, int paramInt2);
  
  private static final native void native_init();
}
