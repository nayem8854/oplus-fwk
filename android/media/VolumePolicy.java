package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class VolumePolicy implements Parcelable {
  public static final int A11Y_MODE_INDEPENDENT_A11Y_VOLUME = 1;
  
  public static final int A11Y_MODE_MEDIA_A11Y_VOLUME = 0;
  
  public static final Parcelable.Creator<VolumePolicy> CREATOR;
  
  public static final VolumePolicy DEFAULT = new VolumePolicy(false, false, false, 400);
  
  public final boolean doNotDisturbWhenSilent;
  
  public final int vibrateToSilentDebounce;
  
  public final boolean volumeDownToEnterSilent;
  
  public final boolean volumeUpToExitSilent;
  
  public VolumePolicy(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, int paramInt) {
    this.volumeDownToEnterSilent = paramBoolean1;
    this.volumeUpToExitSilent = paramBoolean2;
    this.doNotDisturbWhenSilent = paramBoolean3;
    this.vibrateToSilentDebounce = paramInt;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VolumePolicy[volumeDownToEnterSilent=");
    stringBuilder.append(this.volumeDownToEnterSilent);
    stringBuilder.append(",volumeUpToExitSilent=");
    stringBuilder.append(this.volumeUpToExitSilent);
    stringBuilder.append(",doNotDisturbWhenSilent=");
    stringBuilder.append(this.doNotDisturbWhenSilent);
    stringBuilder.append(",vibrateToSilentDebounce=");
    stringBuilder.append(this.vibrateToSilentDebounce);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    boolean bool1 = this.volumeDownToEnterSilent, bool2 = this.volumeUpToExitSilent, bool3 = this.doNotDisturbWhenSilent;
    int i = this.vibrateToSilentDebounce;
    return Objects.hash(new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(bool2), Boolean.valueOf(bool3), Integer.valueOf(i) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof VolumePolicy;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramObject == this)
      return true; 
    paramObject = paramObject;
    bool = bool1;
    if (((VolumePolicy)paramObject).volumeDownToEnterSilent == this.volumeDownToEnterSilent) {
      bool = bool1;
      if (((VolumePolicy)paramObject).volumeUpToExitSilent == this.volumeUpToExitSilent) {
        bool = bool1;
        if (((VolumePolicy)paramObject).doNotDisturbWhenSilent == this.doNotDisturbWhenSilent) {
          bool = bool1;
          if (((VolumePolicy)paramObject).vibrateToSilentDebounce == this.vibrateToSilentDebounce)
            bool = true; 
        } 
      } 
    } 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.volumeDownToEnterSilent);
    paramParcel.writeInt(this.volumeUpToExitSilent);
    paramParcel.writeInt(this.doNotDisturbWhenSilent);
    paramParcel.writeInt(this.vibrateToSilentDebounce);
  }
  
  static {
    CREATOR = new Parcelable.Creator<VolumePolicy>() {
        public VolumePolicy createFromParcel(Parcel param1Parcel) {
          boolean bool2, bool3;
          int i = param1Parcel.readInt();
          boolean bool1 = true;
          if (i != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          if (param1Parcel.readInt() != 0) {
            bool3 = true;
          } else {
            bool3 = false;
          } 
          if (param1Parcel.readInt() == 0)
            bool1 = false; 
          return new VolumePolicy(bool2, bool3, bool1, param1Parcel.readInt());
        }
        
        public VolumePolicy[] newArray(int param1Int) {
          return new VolumePolicy[param1Int];
        }
      };
  }
}
