package android.media;

import android.os.Parcel;
import android.os.Parcelable;

public class MediaResourceParcel implements Parcelable {
  public long value = 0L;
  
  public int type;
  
  public int subType;
  
  public byte[] id;
  
  public static final Parcelable.Creator<MediaResourceParcel> CREATOR = new Parcelable.Creator<MediaResourceParcel>() {
      public MediaResourceParcel createFromParcel(Parcel param1Parcel) {
        MediaResourceParcel mediaResourceParcel = new MediaResourceParcel();
        mediaResourceParcel.readFromParcel(param1Parcel);
        return mediaResourceParcel;
      }
      
      public MediaResourceParcel[] newArray(int param1Int) {
        return new MediaResourceParcel[param1Int];
      }
    };
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.type);
    paramParcel.writeInt(this.subType);
    paramParcel.writeByteArray(this.id);
    paramParcel.writeLong(this.value);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.type = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.subType = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.id = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.value = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
