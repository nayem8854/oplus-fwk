package android.media;

import android.annotation.SystemApi;
import android.app.Activity;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.FileUtils;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import com.android.internal.database.SortCursor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class RingtoneManager extends OplusBaseRingtoneManager {
  private static final String[] INTERNAL_COLUMNS = new String[] { "_id", "title", "title", "title_key" };
  
  private static final String[] MEDIA_COLUMNS = new String[] { "_id", "title", "title", "title_key" };
  
  private int mType = 1;
  
  private final List<String> mFilterColumns = new ArrayList<>();
  
  private boolean mStopPreviousRingtone = true;
  
  public static final String ACTION_RINGTONE_PICKER = "android.intent.action.RINGTONE_PICKER";
  
  public static final String EXTRA_RINGTONE_AUDIO_ATTRIBUTES_FLAGS = "android.intent.extra.ringtone.AUDIO_ATTRIBUTES_FLAGS";
  
  public static final String EXTRA_RINGTONE_DEFAULT_URI = "android.intent.extra.ringtone.DEFAULT_URI";
  
  public static final String EXTRA_RINGTONE_EXISTING_URI = "android.intent.extra.ringtone.EXISTING_URI";
  
  @Deprecated
  public static final String EXTRA_RINGTONE_INCLUDE_DRM = "android.intent.extra.ringtone.INCLUDE_DRM";
  
  public static final String EXTRA_RINGTONE_PICKED_URI = "android.intent.extra.ringtone.PICKED_URI";
  
  public static final String EXTRA_RINGTONE_SHOW_DEFAULT = "android.intent.extra.ringtone.SHOW_DEFAULT";
  
  public static final String EXTRA_RINGTONE_SHOW_SILENT = "android.intent.extra.ringtone.SHOW_SILENT";
  
  public static final String EXTRA_RINGTONE_TITLE = "android.intent.extra.ringtone.TITLE";
  
  public static final String EXTRA_RINGTONE_TYPE = "android.intent.extra.ringtone.TYPE";
  
  public static final int ID_COLUMN_INDEX = 0;
  
  private static final String TAG = "RingtoneManager";
  
  public static final int TITLE_COLUMN_INDEX = 1;
  
  public static final int TYPE_ALARM = 4;
  
  public static final int TYPE_ALL = 7;
  
  public static final int TYPE_NOTIFICATION = 2;
  
  public static final int TYPE_RINGTONE = 1;
  
  public static final int URI_COLUMN_INDEX = 2;
  
  private final Activity mActivity;
  
  private final Context mContext;
  
  private Cursor mCursor;
  
  private boolean mIncludeParentRingtones;
  
  private Ringtone mPreviousRingtone;
  
  public RingtoneManager(Activity paramActivity) {
    this(paramActivity, false);
  }
  
  public RingtoneManager(Activity paramActivity, boolean paramBoolean) {
    this.mActivity = paramActivity;
    this.mContext = (Context)paramActivity;
    setType(this.mType);
    this.mIncludeParentRingtones = paramBoolean;
  }
  
  public RingtoneManager(Context paramContext) {
    this(paramContext, false);
  }
  
  public RingtoneManager(Context paramContext, boolean paramBoolean) {
    this.mActivity = null;
    this.mContext = paramContext;
    setType(this.mType);
    this.mIncludeParentRingtones = paramBoolean;
  }
  
  public void setType(int paramInt) {
    if (this.mCursor == null) {
      this.mType = paramInt;
      setFilterColumnsList(paramInt);
      return;
    } 
    throw new IllegalStateException("Setting filter columns should be done before querying for ringtones.");
  }
  
  public int inferStreamType() {
    int i = this.mType;
    if (i != 2)
      if (i != 4) {
        if (i != 8 && i != 16 && i != 32)
          return 2; 
      } else {
        return 4;
      }  
    return 5;
  }
  
  public void setStopPreviousRingtone(boolean paramBoolean) {
    this.mStopPreviousRingtone = paramBoolean;
  }
  
  public boolean getStopPreviousRingtone() {
    return this.mStopPreviousRingtone;
  }
  
  public void stopPreviousRingtone() {
    Ringtone ringtone = this.mPreviousRingtone;
    if (ringtone != null)
      ringtone.stop(); 
  }
  
  @Deprecated
  public boolean getIncludeDrm() {
    return false;
  }
  
  @Deprecated
  public void setIncludeDrm(boolean paramBoolean) {
    if (paramBoolean)
      Log.w("RingtoneManager", "setIncludeDrm no longer supported"); 
  }
  
  public Cursor getCursor() {
    Cursor cursor = this.mCursor;
    if (cursor != null && cursor.requery())
      return this.mCursor; 
    ArrayList<Cursor> arrayList = new ArrayList();
    arrayList.add(getInternalRingtones());
    arrayList.add(getMediaRingtones());
    if (this.mIncludeParentRingtones) {
      Cursor cursor1 = getParentProfileRingtones();
      if (cursor1 != null)
        arrayList.add(cursor1); 
    } 
    SortCursor sortCursor = new SortCursor(arrayList.<Cursor>toArray(new Cursor[arrayList.size()]), "title_key");
    return (Cursor)sortCursor;
  }
  
  private Cursor getParentProfileRingtones() {
    UserManager userManager = UserManager.get(this.mContext);
    UserInfo userInfo = userManager.getProfileParent(this.mContext.getUserId());
    if (userInfo != null && userInfo.id != this.mContext.getUserId()) {
      Context context = createPackageContextAsUser(this.mContext, userInfo.id);
      if (context != null) {
        Cursor cursor = getMediaRingtones(context);
        return (Cursor)new ExternalRingtonesCursorWrapper(cursor, ContentProvider.maybeAddUserId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, userInfo.id));
      } 
    } 
    return null;
  }
  
  public Ringtone getRingtone(int paramInt) {
    if (this.mStopPreviousRingtone) {
      Ringtone ringtone1 = this.mPreviousRingtone;
      if (ringtone1 != null)
        ringtone1.stop(); 
    } 
    Ringtone ringtone = getRingtone(this.mContext, getRingtoneUri(paramInt), inferStreamType());
    return ringtone;
  }
  
  public Uri getRingtoneUri(int paramInt) {
    Cursor cursor = this.mCursor;
    if (cursor == null || !cursor.moveToPosition(paramInt))
      return null; 
    return getUriFromCursor(this.mContext, this.mCursor);
  }
  
  private static Uri getUriFromCursor(Context paramContext, Cursor paramCursor) {
    Uri uri2 = Uri.parse(paramCursor.getString(2));
    long l = paramCursor.getLong(0);
    Uri uri1 = ContentUris.withAppendedId(uri2, l);
    return paramContext.getContentResolver().canonicalizeOrElse(uri1);
  }
  
  public int getRingtonePosition(Uri paramUri) {
    if (paramUri == null)
      return -1; 
    try {
      long l = ContentUris.parseId(paramUri);
      Cursor cursor = getCursor();
      cursor.moveToPosition(-1);
      while (cursor.moveToNext()) {
        if (l == cursor.getLong(0))
          return cursor.getPosition(); 
      } 
    } catch (NumberFormatException numberFormatException) {
      Log.e("RingtoneManager", "NumberFormatException while getting ringtone position, returning -1", numberFormatException);
    } 
    return -1;
  }
  
  public static Uri getValidRingtoneUri(Context paramContext) {
    RingtoneManager ringtoneManager = new RingtoneManager(paramContext);
    Uri uri1 = getValidRingtoneUriFromCursorAndClose(paramContext, ringtoneManager.getInternalRingtones());
    Uri uri2 = uri1;
    if (uri1 == null)
      uri2 = getValidRingtoneUriFromCursorAndClose(paramContext, ringtoneManager.getMediaRingtones()); 
    return uri2;
  }
  
  private static Uri getValidRingtoneUriFromCursorAndClose(Context paramContext, Cursor paramCursor) {
    if (paramCursor != null) {
      Uri uri = null;
      if (paramCursor.moveToFirst())
        uri = getUriFromCursor(paramContext, paramCursor); 
      paramCursor.close();
      return uri;
    } 
    return null;
  }
  
  private Cursor getInternalRingtones() {
    Uri uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
    String[] arrayOfString = INTERNAL_COLUMNS;
    List<String> list = this.mFilterColumns;
    String str = constructBooleanTrueWhereClause(list);
    Cursor cursor = query(uri, arrayOfString, str, null, "title_key");
    return (Cursor)new ExternalRingtonesCursorWrapper(cursor, MediaStore.Audio.Media.INTERNAL_CONTENT_URI);
  }
  
  private Cursor getMediaRingtones() {
    Cursor cursor = getMediaRingtones(this.mContext);
    return (Cursor)new ExternalRingtonesCursorWrapper(cursor, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
  }
  
  private Cursor getMediaRingtones(Context paramContext) {
    Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    String[] arrayOfString = MEDIA_COLUMNS;
    List<String> list = this.mFilterColumns;
    String str = constructBooleanTrueWhereClause(list);
    return query(uri, arrayOfString, str, null, "title_key", paramContext);
  }
  
  private void setFilterColumnsList(int paramInt) {
    List<String> list = this.mFilterColumns;
    list.clear();
    if ((paramInt & 0x1) != 0)
      list.add("is_ringtone"); 
    if ((paramInt & 0x2) != 0)
      list.add("is_notification"); 
    if ((paramInt & 0x4) != 0)
      list.add("is_alarm"); 
  }
  
  private static String constructBooleanTrueWhereClause(List<String> paramList) {
    if (paramList == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("(");
    for (int i = paramList.size() - 1; i >= 0; i--) {
      stringBuilder.append(paramList.get(i));
      stringBuilder.append("=1 or ");
    } 
    if (paramList.size() > 0)
      stringBuilder.setLength(stringBuilder.length() - 4); 
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  private Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2) {
    return query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, this.mContext);
  }
  
  private Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, Context paramContext) {
    Activity activity = this.mActivity;
    if (activity != null)
      return activity.managedQuery(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2); 
    return paramContext.getContentResolver().query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2);
  }
  
  public static Ringtone getRingtone(Context paramContext, Uri paramUri) {
    return getRingtone(paramContext, paramUri, -1);
  }
  
  public static Ringtone getRingtone(Context paramContext, Uri paramUri, VolumeShaper.Configuration paramConfiguration) {
    return getRingtone(paramContext, paramUri, -1, paramConfiguration);
  }
  
  private static Ringtone getRingtone(Context paramContext, Uri paramUri, int paramInt) {
    return getRingtone(paramContext, paramUri, paramInt, null);
  }
  
  private static Ringtone getRingtone(Context paramContext, Uri paramUri, int paramInt, VolumeShaper.Configuration paramConfiguration) {
    try {
      Ringtone ringtone = new Ringtone();
      this(paramContext, true);
      if (paramInt >= 0)
        ringtone.setStreamType(paramInt); 
      ringtone.setUri(paramUri, paramConfiguration);
      return ringtone;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to open ringtone ");
      stringBuilder.append(paramUri);
      stringBuilder.append(": ");
      stringBuilder.append(exception);
      Log.e("RingtoneManager", stringBuilder.toString());
      return null;
    } 
  }
  
  public static void disableSyncFromParent(Context paramContext) {
    IBinder iBinder = ServiceManager.getService("audio");
    IAudioService iAudioService = IAudioService.Stub.asInterface(iBinder);
    try {
      iAudioService.disableRingtoneSync(paramContext.getUserId());
    } catch (RemoteException remoteException) {
      Log.e("RingtoneManager", "Unable to disable ringtone sync.");
    } 
  }
  
  public static void enableSyncFromParent(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    int i = paramContext.getUserId();
    Settings.Secure.putIntForUser(contentResolver, "sync_parent_sounds", 1, i);
  }
  
  public static Uri getActualDefaultRingtoneUri(Context paramContext, int paramInt) {
    String str = getSettingForType(paramInt);
    Uri uri2 = null;
    if (str == null)
      return null; 
    ContentResolver contentResolver = paramContext.getContentResolver();
    paramInt = paramContext.getUserId();
    str = Settings.System.getStringForUser(contentResolver, str, paramInt);
    if (str != null)
      uri2 = Uri.parse(str); 
    Uri uri1 = uri2;
    if (uri2 != null) {
      uri1 = uri2;
      if (ContentProvider.getUserIdFromUri(uri2) == paramContext.getUserId())
        uri1 = ContentProvider.getUriWithoutUserId(uri2); 
    } 
    return uri1;
  }
  
  public static void setActualDefaultRingtoneUri(Context paramContext, int paramInt, Uri paramUri) {
    String str = getSettingForType(paramInt);
    if (str == null)
      return; 
    ContentResolver contentResolver = paramContext.getContentResolver();
    int i = paramContext.getUserId();
    if (Settings.Secure.getIntForUser(contentResolver, "sync_parent_sounds", 0, i) == 1)
      disableSyncFromParent(paramContext); 
    Uri uri = paramUri;
    if (!isInternalRingtoneUri(paramUri))
      uri = ContentProvider.maybeAddUserId(paramUri, paramContext.getUserId()); 
    if (uri != null) {
      String str1 = uri.toString();
    } else {
      paramUri = null;
    } 
    i = paramContext.getUserId();
    Settings.System.putStringForUser(contentResolver, str, (String)paramUri, i);
    if (uri != null) {
      paramUri = getCacheForType(paramInt, paramContext.getUserId());
      try {
        InputStream inputStream = openRingtone(paramContext, uri);
        try {
          OutputStream outputStream = contentResolver.openOutputStream(paramUri);
        } finally {
          if (inputStream != null)
            try {
              inputStream.close();
            } finally {
              inputStream = null;
            }  
        } 
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to cache ringtone: ");
        stringBuilder.append(iOException);
        Log.w("RingtoneManager", stringBuilder.toString());
      } 
    } 
  }
  
  private static boolean isInternalRingtoneUri(Uri paramUri) {
    return isRingtoneUriInStorage(paramUri, MediaStore.Audio.Media.INTERNAL_CONTENT_URI);
  }
  
  private static boolean isExternalRingtoneUri(Uri paramUri) {
    return isRingtoneUriInStorage(paramUri, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
  }
  
  private static boolean isRingtoneUriInStorage(Uri paramUri1, Uri paramUri2) {
    boolean bool;
    paramUri1 = ContentProvider.getUriWithoutUserId(paramUri1);
    if (paramUri1 == null) {
      bool = false;
    } else {
      bool = paramUri1.toString().startsWith(paramUri2.toString());
    } 
    return bool;
  }
  
  public Uri addCustomExternalRingtone(Uri paramUri, int paramInt) throws FileNotFoundException, IllegalArgumentException, IOException {
    if (Environment.getExternalStorageState().equals("mounted")) {
      String str = this.mContext.getContentResolver().getType(paramUri);
      if (str != null && (
        str.startsWith("audio/") || str.equals("application/ogg"))) {
        String str1 = getExternalDirectoryForType(paramInt);
        Context context = this.mContext;
        String str2 = FileUtils.buildValidFatFilename(Utils.getFileDisplayNameFromUri(context, paramUri));
        null = Utils.getUniqueExternalFile(context, str1, str2, str);
        InputStream inputStream = this.mContext.getContentResolver().openInputStream(paramUri);
        try {
          FileOutputStream fileOutputStream = new FileOutputStream();
          this(null);
        } finally {
          if (inputStream != null)
            try {
              inputStream.close();
            } finally {
              inputStream = null;
            }  
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ringtone file must have MIME type \"audio/*\". Given file has MIME type \"");
      stringBuilder.append(str);
      stringBuilder.append("\"");
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IOException("External storage is not mounted. Unable to install ringtones.");
  }
  
  private static final String getExternalDirectoryForType(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt == 4)
          return Environment.DIRECTORY_ALARMS; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported ringtone type: ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return Environment.DIRECTORY_NOTIFICATIONS;
    } 
    return Environment.DIRECTORY_RINGTONES;
  }
  
  private static InputStream openRingtone(Context paramContext, Uri paramUri) throws IOException {
    ContentResolver contentResolver = paramContext.getContentResolver();
    try {
      return contentResolver.openInputStream(paramUri);
    } catch (SecurityException|IOException securityException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to open directly; attempting failover: ");
      stringBuilder.append(securityException);
      Log.w("RingtoneManager", stringBuilder.toString());
      AudioManager audioManager = (AudioManager)paramContext.getSystemService(AudioManager.class);
      IRingtonePlayer iRingtonePlayer = audioManager.getRingtonePlayer();
      try {
        return new ParcelFileDescriptor.AutoCloseInputStream(iRingtonePlayer.openRingtone(paramUri));
      } catch (Exception exception) {
        throw new IOException(exception);
      } 
    } 
  }
  
  private static String getSettingForType(int paramInt) {
    if ((paramInt & 0x1) != 0)
      return "ringtone"; 
    if ((paramInt & 0x2) != 0)
      return "notification_sound"; 
    if ((paramInt & 0x4) != 0)
      return "alarm_alert"; 
    if ((paramInt & 0x8) != 0)
      return "oppo_sms_notification_sound"; 
    if ((paramInt & 0x10) != 0)
      return "notification_sim2"; 
    if ((paramInt & 0x20) != 0)
      return "calendar_sound"; 
    if ((paramInt & 0x40) != 0)
      return "ringtone_sim2"; 
    return null;
  }
  
  public static Uri getCacheForType(int paramInt) {
    return getCacheForType(paramInt, UserHandle.getCallingUserId());
  }
  
  public static Uri getCacheForType(int paramInt1, int paramInt2) {
    if ((paramInt1 & 0x1) != 0)
      return ContentProvider.maybeAddUserId(Settings.System.RINGTONE_CACHE_URI, paramInt2); 
    if ((paramInt1 & 0x2) != 0)
      return ContentProvider.maybeAddUserId(Settings.System.NOTIFICATION_SOUND_CACHE_URI, paramInt2); 
    if ((paramInt1 & 0x4) != 0)
      return ContentProvider.maybeAddUserId(Settings.System.ALARM_ALERT_CACHE_URI, paramInt2); 
    return null;
  }
  
  public static boolean isDefault(Uri paramUri) {
    boolean bool;
    if (getDefaultType(paramUri) != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int getDefaultType(Uri paramUri) {
    paramUri = ContentProvider.getUriWithoutUserId(paramUri);
    if (paramUri == null)
      return -1; 
    if (paramUri.equals(Settings.System.DEFAULT_RINGTONE_URI))
      return 1; 
    if (paramUri.equals(Settings.System.DEFAULT_NOTIFICATION_URI))
      return 2; 
    if (paramUri.equals(Settings.System.DEFAULT_ALARM_ALERT_URI))
      return 4; 
    return -1;
  }
  
  public static Uri getDefaultUri(int paramInt) {
    if ((paramInt & 0x1) != 0)
      return Settings.System.DEFAULT_RINGTONE_URI; 
    if ((paramInt & 0x2) != 0)
      return Settings.System.DEFAULT_NOTIFICATION_URI; 
    if ((paramInt & 0x4) != 0)
      return Settings.System.DEFAULT_ALARM_ALERT_URI; 
    return null;
  }
  
  public static AssetFileDescriptor openDefaultRingtoneUri(Context paramContext, Uri paramUri) throws FileNotFoundException {
    AssetFileDescriptor assetFileDescriptor;
    int i = getDefaultType(paramUri);
    paramUri = getCacheForType(i, paramContext.getUserId());
    Uri uri = getActualDefaultRingtoneUri(paramContext, i);
    ContentResolver contentResolver = paramContext.getContentResolver();
    paramContext = null;
    if (paramUri != null) {
      AssetFileDescriptor assetFileDescriptor1 = contentResolver.openAssetFileDescriptor(paramUri, "r");
      assetFileDescriptor = assetFileDescriptor1;
      if (assetFileDescriptor1 != null)
        return assetFileDescriptor1; 
    } 
    if (uri != null)
      assetFileDescriptor = contentResolver.openAssetFileDescriptor(uri, "r"); 
    return assetFileDescriptor;
  }
  
  public boolean hasHapticChannels(int paramInt) {
    return hasHapticChannels(getRingtoneUri(paramInt));
  }
  
  public static boolean hasHapticChannels(Uri paramUri) {
    return AudioManager.hasHapticChannels(paramUri);
  }
  
  private static Context createPackageContextAsUser(Context paramContext, int paramInt) {
    try {
      String str = paramContext.getPackageName();
      UserHandle userHandle = UserHandle.of(paramInt);
      return paramContext.createPackageContextAsUser(str, 0, userHandle);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      Log.e("RingtoneManager", "Unable to create package context", (Throwable)nameNotFoundException);
      return null;
    } 
  }
  
  @SystemApi
  public static void ensureDefaultRingtones(Context paramContext) {
    hookforMediaProviderCustomized(paramContext);
    if (OplusRingtoneManager.isComponentVersionChange(paramContext) && 
      OplusRingtoneManager.isCustomDefaultRingtoneNeeded(paramContext))
      OplusRingtoneManager.clearDefaultRingtonesHistory(paramContext); 
    byte b = 0;
    while (true) {
      if (b < 7) {
        (new int[7])[0] = 1;
        (new int[7])[1] = 2;
        (new int[7])[2] = 4;
        (new int[7])[3] = 8;
        (new int[7])[4] = 16;
        (new int[7])[5] = 32;
        (new int[7])[6] = 64;
        int i = (new int[7])[b];
        String str = getDefaultRingtoneSetting(i);
        if (Settings.System.getInt(paramContext.getContentResolver(), str, 0) == 0) {
          str = getDefaultRingtoneFilename(i);
          Uri uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
          Cursor cursor = paramContext.getContentResolver().query(uri, new String[] { "_id" }, "_display_name=?", new String[] { str }, null);
          try {
            if (cursor.moveToFirst()) {
              ContentResolver contentResolver = paramContext.getContentResolver();
              uri = ContentUris.withAppendedId(uri, cursor.getLong(0));
              Uri uri1 = contentResolver.canonicalizeOrElse(uri);
              OplusRingtoneManager.setRingtonesUri(paramContext, i, uri1);
            } 
            if (cursor != null)
              cursor.close(); 
          } finally {
            if (cursor != null)
              try {
                cursor.close();
              } finally {
                cursor = null;
              }  
          } 
          continue;
        } 
      } else {
        break;
      } 
      b++;
    } 
  }
  
  private static String getDefaultRingtoneSetting(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt == 64)
                  return "ringtone_sim2_set"; 
                throw new IllegalArgumentException();
              } 
              return "notification_sound_calendar_set";
            } 
            return "notification_sound_sim2_set";
          } 
          return "notification_sound_sms_set";
        } 
        return "alarm_alert_set";
      } 
      return "notification_sound_set";
    } 
    return "ringtone_set";
  }
  
  private static String getDefaultRingtoneFilename(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt == 64)
                  return SystemProperties.get("ro.config.ringtone_sim2"); 
                throw new IllegalArgumentException();
              } 
              return SystemProperties.get("ro.config.calendar_sound");
            } 
            return SystemProperties.get("ro.config.notification_sim2");
          } 
          return SystemProperties.get("ro.config.notification_sms");
        } 
        return SystemProperties.get("ro.config.alarm_alert");
      } 
      return SystemProperties.get("ro.config.notification_sound");
    } 
    return SystemProperties.get("ro.config.ringtone");
  }
  
  private static void hookforMediaProviderCustomized(Context paramContext) {
    if (paramContext == null) {
      Log.d("RingtoneManager", "hookforMediaProviderCustomized context is Null");
      return;
    } 
    String str = paramContext.getPackageName();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("hookforMediaProviderCustomized pkgName");
    stringBuilder.append(str);
    Log.d("RingtoneManager", stringBuilder.toString());
    if ("com.google.android.providers.media.module".equals(str)) {
      OifaceBindUtils.bindTask();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("hookforMediaProviderCustomized curThread");
      stringBuilder1.append(Thread.currentThread());
      Log.d("RingtoneManager", stringBuilder1.toString());
      Utils.filterBlacklistDirectory();
    } 
  }
}
