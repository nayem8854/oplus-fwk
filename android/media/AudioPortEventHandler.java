package android.media;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

class AudioPortEventHandler {
  private final Object mLock = new Object();
  
  private final ArrayList<AudioManager.OnAudioPortUpdateListener> mListeners = new ArrayList<>();
  
  private long mJniCallback;
  
  private HandlerThread mHandlerThread;
  
  private Handler mHandler;
  
  private static final String TAG = "AudioPortEventHandler";
  
  private static final long RESCHEDULE_MESSAGE_DELAY_MS = 100L;
  
  private static final int AUDIOPORT_EVENT_SERVICE_DIED = 3;
  
  private static final int AUDIOPORT_EVENT_PORT_LIST_UPDATED = 1;
  
  private static final int AUDIOPORT_EVENT_PATCH_LIST_UPDATED = 2;
  
  private static final int AUDIOPORT_EVENT_NEW_LISTENER = 4;
  
  void init() {
    synchronized (this.mLock) {
      if (this.mHandler != null)
        return; 
      HandlerThread handlerThread = new HandlerThread();
      this("AudioPortEventHandler");
      this.mHandlerThread = handlerThread;
      handlerThread.start();
      if (this.mHandlerThread.getLooper() != null) {
        Object object = new Object();
        super(this, this.mHandlerThread.getLooper());
        this.mHandler = (Handler)object;
        object = new WeakReference();
        super((T)this);
        native_setup(object);
      } else {
        this.mHandler = null;
      } 
      return;
    } 
  }
  
  protected void finalize() {
    native_finalize();
    if (this.mHandlerThread.isAlive())
      this.mHandlerThread.quit(); 
  }
  
  void registerListener(AudioManager.OnAudioPortUpdateListener paramOnAudioPortUpdateListener) {
    synchronized (this.mLock) {
      this.mListeners.add(paramOnAudioPortUpdateListener);
      null = this.mHandler;
      if (null != null) {
        Message message = null.obtainMessage(4, 0, 0, paramOnAudioPortUpdateListener);
        this.mHandler.sendMessage(message);
      } 
      return;
    } 
  }
  
  void unregisterListener(AudioManager.OnAudioPortUpdateListener paramOnAudioPortUpdateListener) {
    synchronized (this.mLock) {
      this.mListeners.remove(paramOnAudioPortUpdateListener);
      return;
    } 
  }
  
  Handler handler() {
    return this.mHandler;
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = paramObject1;
    paramObject1 = paramObject1.get();
    if (paramObject1 == null)
      return; 
    if (paramObject1 != null) {
      paramObject1 = paramObject1.handler();
      if (paramObject1 != null) {
        paramObject2 = paramObject1.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
        if (paramInt1 != 4)
          paramObject1.removeMessages(paramInt1); 
        paramObject1.sendMessage((Message)paramObject2);
      } 
    } 
  }
  
  private native void native_finalize();
  
  private native void native_setup(Object paramObject);
}
