package android.media.effect;

public interface EffectUpdateListener {
  void onEffectUpdated(Effect paramEffect, Object paramObject);
}
