package android.media.effect;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterGraph;
import android.filterfw.core.GraphRunner;
import android.filterfw.core.SyncRunner;
import android.filterfw.io.GraphIOException;
import android.filterfw.io.TextGraphReader;

public class FilterGraphEffect extends FilterEffect {
  private static final String TAG = "FilterGraphEffect";
  
  protected FilterGraph mGraph;
  
  protected String mInputName;
  
  protected String mOutputName;
  
  protected GraphRunner mRunner;
  
  protected Class mSchedulerClass;
  
  public FilterGraphEffect(EffectContext paramEffectContext, String paramString1, String paramString2, String paramString3, String paramString4, Class paramClass) {
    super(paramEffectContext, paramString1);
    this.mInputName = paramString3;
    this.mOutputName = paramString4;
    this.mSchedulerClass = paramClass;
    createGraph(paramString2);
  }
  
  private void createGraph(String paramString) {
    TextGraphReader textGraphReader = new TextGraphReader();
    try {
      FilterGraph filterGraph = textGraphReader.readGraphString(paramString);
      if (filterGraph != null) {
        this.mRunner = (GraphRunner)new SyncRunner(getFilterContext(), this.mGraph, this.mSchedulerClass);
        return;
      } 
      throw new RuntimeException("Could not setup effect");
    } catch (GraphIOException graphIOException) {
      throw new RuntimeException("Could not setup effect", graphIOException);
    } 
  }
  
  public void apply(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    beginGLEffect();
    Filter filter = this.mGraph.getFilter(this.mInputName);
    if (filter != null) {
      filter.setInputValue("texId", Integer.valueOf(paramInt1));
      filter.setInputValue("width", Integer.valueOf(paramInt2));
      filter.setInputValue("height", Integer.valueOf(paramInt3));
      filter = this.mGraph.getFilter(this.mOutputName);
      if (filter != null) {
        filter.setInputValue("texId", Integer.valueOf(paramInt4));
        try {
          this.mRunner.run();
          endGLEffect();
          return;
        } catch (RuntimeException runtimeException) {
          throw new RuntimeException("Internal error applying effect: ", runtimeException);
        } 
      } 
      throw new RuntimeException("Internal error applying effect");
    } 
    throw new RuntimeException("Internal error applying effect");
  }
  
  public void setParameter(String paramString, Object paramObject) {}
  
  public void release() {
    this.mGraph.tearDown(getFilterContext());
    this.mGraph = null;
  }
}
