package android.media.effect;

import android.filterfw.core.Frame;

public class SizeChangeEffect extends SingleFilterEffect {
  public SizeChangeEffect(EffectContext paramEffectContext, String paramString1, Class paramClass, String paramString2, String paramString3, Object... paramVarArgs) {
    super(paramEffectContext, paramString1, paramClass, paramString2, paramString3, paramVarArgs);
  }
  
  public void apply(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    beginGLEffect();
    Frame frame1 = frameFromTexture(paramInt1, paramInt2, paramInt3);
    Frame frame2 = this.mFunction.executeWithArgList(new Object[] { this.mInputName, frame1 });
    paramInt1 = frame2.getFormat().getWidth();
    paramInt2 = frame2.getFormat().getHeight();
    Frame frame3 = frameFromTexture(paramInt4, paramInt1, paramInt2);
    frame3.setDataFromFrame(frame2);
    frame1.release();
    frame3.release();
    frame2.release();
    endGLEffect();
  }
}
