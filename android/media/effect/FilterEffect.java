package android.media.effect;

import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.format.ImageFormat;

public abstract class FilterEffect extends Effect {
  protected EffectContext mEffectContext;
  
  private String mName;
  
  protected FilterEffect(EffectContext paramEffectContext, String paramString) {
    this.mEffectContext = paramEffectContext;
    this.mName = paramString;
  }
  
  public String getName() {
    return this.mName;
  }
  
  protected void beginGLEffect() {
    this.mEffectContext.assertValidGLState();
    this.mEffectContext.saveGLState();
  }
  
  protected void endGLEffect() {
    this.mEffectContext.restoreGLState();
  }
  
  protected FilterContext getFilterContext() {
    return this.mEffectContext.mFilterContext;
  }
  
  protected Frame frameFromTexture(int paramInt1, int paramInt2, int paramInt3) {
    FrameManager frameManager = getFilterContext().getFrameManager();
    MutableFrameFormat mutableFrameFormat = ImageFormat.create(paramInt2, paramInt3, 3, 3);
    Frame frame = frameManager.newBoundFrame((FrameFormat)mutableFrameFormat, 100, paramInt1);
    frame.setTimestamp(-1L);
    return frame;
  }
}
