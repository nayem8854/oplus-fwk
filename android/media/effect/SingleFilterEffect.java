package android.media.effect;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterFactory;
import android.filterfw.core.FilterFunction;
import android.filterfw.core.Frame;

public class SingleFilterEffect extends FilterEffect {
  protected FilterFunction mFunction;
  
  protected String mInputName;
  
  protected String mOutputName;
  
  public SingleFilterEffect(EffectContext paramEffectContext, String paramString1, Class paramClass, String paramString2, String paramString3, Object... paramVarArgs) {
    super(paramEffectContext, paramString1);
    this.mInputName = paramString2;
    this.mOutputName = paramString3;
    String str = paramClass.getSimpleName();
    FilterFactory filterFactory = FilterFactory.sharedFactory();
    Filter filter = filterFactory.createFilterByClass(paramClass, str);
    filter.initWithAssignmentList(paramVarArgs);
    this.mFunction = new FilterFunction(getFilterContext(), filter);
  }
  
  public void apply(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    beginGLEffect();
    Frame frame1 = frameFromTexture(paramInt1, paramInt2, paramInt3);
    Frame frame2 = frameFromTexture(paramInt4, paramInt2, paramInt3);
    Frame frame3 = this.mFunction.executeWithArgList(new Object[] { this.mInputName, frame1 });
    frame2.setDataFromFrame(frame3);
    frame1.release();
    frame2.release();
    frame3.release();
    endGLEffect();
  }
  
  public void setParameter(String paramString, Object paramObject) {
    this.mFunction.setInputValue(paramString, paramObject);
  }
  
  public void release() {
    this.mFunction.tearDown();
    this.mFunction = null;
  }
}
