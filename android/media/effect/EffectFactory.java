package android.media.effect;

import java.lang.reflect.Constructor;

public class EffectFactory {
  public static final String EFFECT_AUTOFIX = "android.media.effect.effects.AutoFixEffect";
  
  public static final String EFFECT_BACKDROPPER = "android.media.effect.effects.BackDropperEffect";
  
  public static final String EFFECT_BITMAPOVERLAY = "android.media.effect.effects.BitmapOverlayEffect";
  
  public static final String EFFECT_BLACKWHITE = "android.media.effect.effects.BlackWhiteEffect";
  
  public static final String EFFECT_BRIGHTNESS = "android.media.effect.effects.BrightnessEffect";
  
  public static final String EFFECT_CONTRAST = "android.media.effect.effects.ContrastEffect";
  
  public static final String EFFECT_CROP = "android.media.effect.effects.CropEffect";
  
  public static final String EFFECT_CROSSPROCESS = "android.media.effect.effects.CrossProcessEffect";
  
  public static final String EFFECT_DOCUMENTARY = "android.media.effect.effects.DocumentaryEffect";
  
  public static final String EFFECT_DUOTONE = "android.media.effect.effects.DuotoneEffect";
  
  public static final String EFFECT_FILLLIGHT = "android.media.effect.effects.FillLightEffect";
  
  public static final String EFFECT_FISHEYE = "android.media.effect.effects.FisheyeEffect";
  
  public static final String EFFECT_FLIP = "android.media.effect.effects.FlipEffect";
  
  public static final String EFFECT_GRAIN = "android.media.effect.effects.GrainEffect";
  
  public static final String EFFECT_GRAYSCALE = "android.media.effect.effects.GrayscaleEffect";
  
  public static final String EFFECT_IDENTITY = "IdentityEffect";
  
  public static final String EFFECT_LOMOISH = "android.media.effect.effects.LomoishEffect";
  
  public static final String EFFECT_NEGATIVE = "android.media.effect.effects.NegativeEffect";
  
  private static final String[] EFFECT_PACKAGES = new String[] { "android.media.effect.effects.", "" };
  
  public static final String EFFECT_POSTERIZE = "android.media.effect.effects.PosterizeEffect";
  
  public static final String EFFECT_REDEYE = "android.media.effect.effects.RedEyeEffect";
  
  public static final String EFFECT_ROTATE = "android.media.effect.effects.RotateEffect";
  
  public static final String EFFECT_SATURATE = "android.media.effect.effects.SaturateEffect";
  
  public static final String EFFECT_SEPIA = "android.media.effect.effects.SepiaEffect";
  
  public static final String EFFECT_SHARPEN = "android.media.effect.effects.SharpenEffect";
  
  public static final String EFFECT_STRAIGHTEN = "android.media.effect.effects.StraightenEffect";
  
  public static final String EFFECT_TEMPERATURE = "android.media.effect.effects.ColorTemperatureEffect";
  
  public static final String EFFECT_TINT = "android.media.effect.effects.TintEffect";
  
  public static final String EFFECT_VIGNETTE = "android.media.effect.effects.VignetteEffect";
  
  private EffectContext mEffectContext;
  
  EffectFactory(EffectContext paramEffectContext) {
    this.mEffectContext = paramEffectContext;
  }
  
  public Effect createEffect(String paramString) {
    Class clazz = getEffectClassByName(paramString);
    if (clazz != null)
      return instantiateEffect(clazz, paramString); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cannot instantiate unknown effect '");
    stringBuilder.append(paramString);
    stringBuilder.append("'!");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static boolean isEffectSupported(String paramString) {
    boolean bool;
    if (getEffectClassByName(paramString) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static Class getEffectClassByName(String paramString) {
    Class<?> clazz;
    String str = null;
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    String[] arrayOfString = EFFECT_PACKAGES;
    int i = arrayOfString.length;
    byte b = 0;
    while (true) {
      String str1 = str;
      if (b < i) {
        str1 = arrayOfString[b];
        try {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(str1);
          stringBuilder.append(paramString);
          clazz1 = clazz = classLoader.loadClass(stringBuilder.toString());
          clazz = clazz1;
          if (clazz1 != null) {
            clazz = clazz1;
            break;
          } 
        } catch (ClassNotFoundException classNotFoundException) {
          clazz = clazz1;
        } 
        b++;
        Class<?> clazz1 = clazz;
        continue;
      } 
      break;
    } 
    return clazz;
  }
  
  private Effect instantiateEffect(Class paramClass, String paramString) {
    try {
      paramClass.asSubclass(Effect.class);
      try {
        Constructor<Effect> constructor = paramClass.getConstructor(new Class[] { EffectContext.class, String.class });
        try {
          return effect;
        } finally {
          constructor = null;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("There was an error constructing the effect '");
          stringBuilder.append(paramClass);
          stringBuilder.append("'!");
        } 
      } catch (NoSuchMethodException noSuchMethodException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("The effect class '");
        stringBuilder.append(paramClass);
        stringBuilder.append("' does not have the required constructor.");
        throw new RuntimeException(stringBuilder.toString(), noSuchMethodException);
      } 
    } catch (ClassCastException classCastException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Attempting to allocate effect '");
      stringBuilder.append(paramClass);
      stringBuilder.append("' which is not a subclass of Effect!");
      throw new IllegalArgumentException(stringBuilder.toString(), classCastException);
    } 
  }
}
