package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaScannerService extends IInterface {
  void requestScanFile(String paramString1, String paramString2, IMediaScannerListener paramIMediaScannerListener) throws RemoteException;
  
  void scanFile(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IMediaScannerService {
    public void requestScanFile(String param1String1, String param1String2, IMediaScannerListener param1IMediaScannerListener) throws RemoteException {}
    
    public void scanFile(String param1String1, String param1String2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaScannerService {
    private static final String DESCRIPTOR = "android.media.IMediaScannerService";
    
    static final int TRANSACTION_requestScanFile = 1;
    
    static final int TRANSACTION_scanFile = 2;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaScannerService");
    }
    
    public static IMediaScannerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaScannerService");
      if (iInterface != null && iInterface instanceof IMediaScannerService)
        return (IMediaScannerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "scanFile";
      } 
      return "requestScanFile";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str1;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.media.IMediaScannerService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.IMediaScannerService");
        String str = param1Parcel1.readString();
        str1 = param1Parcel1.readString();
        scanFile(str, str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      str1.enforceInterface("android.media.IMediaScannerService");
      String str2 = str1.readString();
      String str3 = str1.readString();
      IMediaScannerListener iMediaScannerListener = IMediaScannerListener.Stub.asInterface(str1.readStrongBinder());
      requestScanFile(str2, str3, iMediaScannerListener);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IMediaScannerService {
      public static IMediaScannerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaScannerService";
      }
      
      public void requestScanFile(String param2String1, String param2String2, IMediaScannerListener param2IMediaScannerListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaScannerService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2IMediaScannerListener != null) {
            iBinder = param2IMediaScannerListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMediaScannerService.Stub.getDefaultImpl() != null) {
            IMediaScannerService.Stub.getDefaultImpl().requestScanFile(param2String1, param2String2, param2IMediaScannerListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void scanFile(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaScannerService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMediaScannerService.Stub.getDefaultImpl() != null) {
            IMediaScannerService.Stub.getDefaultImpl().scanFile(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaScannerService param1IMediaScannerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaScannerService != null) {
          Proxy.sDefaultImpl = param1IMediaScannerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaScannerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
