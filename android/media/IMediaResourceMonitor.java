package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaResourceMonitor extends IInterface {
  void notifyResourceGranted(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IMediaResourceMonitor {
    public void notifyResourceGranted(int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaResourceMonitor {
    private static final String DESCRIPTOR = "android.media.IMediaResourceMonitor";
    
    static final int TRANSACTION_notifyResourceGranted = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaResourceMonitor");
    }
    
    public static IMediaResourceMonitor asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaResourceMonitor");
      if (iInterface != null && iInterface instanceof IMediaResourceMonitor)
        return (IMediaResourceMonitor)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyResourceGranted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IMediaResourceMonitor");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IMediaResourceMonitor");
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      notifyResourceGranted(param1Int1, param1Int2);
      return true;
    }
    
    private static class Proxy implements IMediaResourceMonitor {
      public static IMediaResourceMonitor sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaResourceMonitor";
      }
      
      public void notifyResourceGranted(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaResourceMonitor");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaResourceMonitor.Stub.getDefaultImpl() != null) {
            IMediaResourceMonitor.Stub.getDefaultImpl().notifyResourceGranted(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaResourceMonitor param1IMediaResourceMonitor) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaResourceMonitor != null) {
          Proxy.sDefaultImpl = param1IMediaResourceMonitor;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaResourceMonitor getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
