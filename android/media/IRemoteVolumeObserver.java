package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteVolumeObserver extends IInterface {
  void dispatchRemoteVolumeUpdate(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IRemoteVolumeObserver {
    public void dispatchRemoteVolumeUpdate(int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteVolumeObserver {
    private static final String DESCRIPTOR = "android.media.IRemoteVolumeObserver";
    
    static final int TRANSACTION_dispatchRemoteVolumeUpdate = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IRemoteVolumeObserver");
    }
    
    public static IRemoteVolumeObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IRemoteVolumeObserver");
      if (iInterface != null && iInterface instanceof IRemoteVolumeObserver)
        return (IRemoteVolumeObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "dispatchRemoteVolumeUpdate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IRemoteVolumeObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IRemoteVolumeObserver");
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      dispatchRemoteVolumeUpdate(param1Int2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IRemoteVolumeObserver {
      public static IRemoteVolumeObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IRemoteVolumeObserver";
      }
      
      public void dispatchRemoteVolumeUpdate(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteVolumeObserver");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRemoteVolumeObserver.Stub.getDefaultImpl() != null) {
            IRemoteVolumeObserver.Stub.getDefaultImpl().dispatchRemoteVolumeUpdate(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteVolumeObserver param1IRemoteVolumeObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteVolumeObserver != null) {
          Proxy.sDefaultImpl = param1IRemoteVolumeObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteVolumeObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
