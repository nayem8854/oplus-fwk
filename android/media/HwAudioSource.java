package android.media;

import android.annotation.SystemApi;
import com.android.internal.util.Preconditions;

@SystemApi
public class HwAudioSource extends PlayerBase {
  private final AudioAttributes mAudioAttributes;
  
  private final AudioDeviceInfo mAudioDeviceInfo;
  
  private int mNativeHandle;
  
  private HwAudioSource(AudioDeviceInfo paramAudioDeviceInfo, AudioAttributes paramAudioAttributes) {
    super(paramAudioAttributes, 14);
    Preconditions.checkNotNull(paramAudioDeviceInfo);
    Preconditions.checkNotNull(paramAudioAttributes);
    Preconditions.checkArgument(paramAudioDeviceInfo.isSource(), "Requires a source device");
    this.mAudioDeviceInfo = paramAudioDeviceInfo;
    this.mAudioAttributes = paramAudioAttributes;
    baseRegisterPlayer();
  }
  
  void playerSetVolume(boolean paramBoolean, float paramFloat1, float paramFloat2) {}
  
  int playerApplyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation) {
    return 0;
  }
  
  VolumeShaper.State playerGetVolumeShaperState(int paramInt) {
    return new VolumeShaper.State(1.0F, 1.0F);
  }
  
  int playerSetAuxEffectSendLevel(boolean paramBoolean, float paramFloat) {
    return 0;
  }
  
  void playerStart() {
    start();
  }
  
  void playerPause() {
    stop();
  }
  
  void playerStop() {
    stop();
  }
  
  public void start() {
    Preconditions.checkState(isPlaying() ^ true, "HwAudioSource is currently playing");
    baseStart();
    AudioDeviceInfo audioDeviceInfo = this.mAudioDeviceInfo;
    AudioPortConfig audioPortConfig = audioDeviceInfo.getPort().activeConfig();
    AudioAttributes audioAttributes = this.mAudioAttributes;
    this.mNativeHandle = AudioSystem.startAudioSource(audioPortConfig, audioAttributes);
  }
  
  public boolean isPlaying() {
    boolean bool;
    if (this.mNativeHandle != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void stop() {
    baseStop();
    int i = this.mNativeHandle;
    if (i > 0) {
      AudioSystem.stopAudioSource(i);
      this.mNativeHandle = 0;
    } 
  }
  
  class Builder {
    private AudioAttributes mAudioAttributes;
    
    private AudioDeviceInfo mAudioDeviceInfo;
    
    public Builder setAudioAttributes(AudioAttributes param1AudioAttributes) {
      Preconditions.checkNotNull(param1AudioAttributes);
      this.mAudioAttributes = param1AudioAttributes;
      return this;
    }
    
    public Builder setAudioDeviceInfo(AudioDeviceInfo param1AudioDeviceInfo) {
      Preconditions.checkNotNull(param1AudioDeviceInfo);
      Preconditions.checkArgument(param1AudioDeviceInfo.isSource());
      this.mAudioDeviceInfo = param1AudioDeviceInfo;
      return this;
    }
    
    public HwAudioSource build() {
      Preconditions.checkNotNull(this.mAudioDeviceInfo);
      if (this.mAudioAttributes == null) {
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        builder = builder.setUsage(1);
        this.mAudioAttributes = builder.build();
      } 
      return new HwAudioSource(this.mAudioDeviceInfo, this.mAudioAttributes);
    }
  }
}
