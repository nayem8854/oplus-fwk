package android.media;

public final class MediaTimestamp {
  public static final MediaTimestamp TIMESTAMP_UNKNOWN = new MediaTimestamp(-1L, -1L, 0.0F);
  
  public final float clockRate;
  
  public final long mediaTimeUs;
  
  public final long nanoTime;
  
  public long getAnchorMediaTimeUs() {
    return this.mediaTimeUs;
  }
  
  @Deprecated
  public long getAnchorSytemNanoTime() {
    return getAnchorSystemNanoTime();
  }
  
  public long getAnchorSystemNanoTime() {
    return this.nanoTime;
  }
  
  public float getMediaClockRate() {
    return this.clockRate;
  }
  
  public MediaTimestamp(long paramLong1, long paramLong2, float paramFloat) {
    this.mediaTimeUs = paramLong1;
    this.nanoTime = paramLong2;
    this.clockRate = paramFloat;
  }
  
  MediaTimestamp() {
    this.mediaTimeUs = 0L;
    this.nanoTime = 0L;
    this.clockRate = 1.0F;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mediaTimeUs != ((MediaTimestamp)paramObject).mediaTimeUs || this.nanoTime != ((MediaTimestamp)paramObject).nanoTime || this.clockRate != ((MediaTimestamp)paramObject).clockRate)
      bool = false; 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getName());
    stringBuilder.append("{AnchorMediaTimeUs=");
    stringBuilder.append(this.mediaTimeUs);
    stringBuilder.append(" AnchorSystemNanoTime=");
    stringBuilder.append(this.nanoTime);
    stringBuilder.append(" clockRate=");
    stringBuilder.append(this.clockRate);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
