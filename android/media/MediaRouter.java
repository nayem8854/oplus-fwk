package android.media;

import android.app.ActivityThread;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.hardware.display.WifiDisplay;
import android.hardware.display.WifiDisplayStatus;
import android.media.session.MediaSession;
import android.os.Handler;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

public class MediaRouter {
  static final boolean $assertionsDisabled = false;
  
  public static final int AVAILABILITY_FLAG_IGNORE_DEFAULT_ROUTE = 1;
  
  public static final int CALLBACK_FLAG_PASSIVE_DISCOVERY = 8;
  
  public static final int CALLBACK_FLAG_PERFORM_ACTIVE_SCAN = 1;
  
  public static final int CALLBACK_FLAG_REQUEST_DISCOVERY = 4;
  
  public static final int CALLBACK_FLAG_UNFILTERED_EVENTS = 2;
  
  private static final boolean DEBUG = Log.isLoggable("MediaRouter", 3);
  
  public static final String MIRRORING_GROUP_ID = "android.media.mirroring_group";
  
  static final int ROUTE_TYPE_ANY = 8388615;
  
  public static final int ROUTE_TYPE_LIVE_AUDIO = 1;
  
  public static final int ROUTE_TYPE_LIVE_VIDEO = 2;
  
  public static final int ROUTE_TYPE_REMOTE_DISPLAY = 4;
  
  public static final int ROUTE_TYPE_USER = 8388608;
  
  private static final String TAG = "MediaRouter";
  
  class Static implements DisplayManager.DisplayListener {
    final CopyOnWriteArrayList<MediaRouter.CallbackInfo> mCallbacks = new CopyOnWriteArrayList<>();
    
    final ArrayList<MediaRouter.RouteInfo> mRoutes = new ArrayList<>();
    
    final ArrayList<MediaRouter.RouteCategory> mCategories = new ArrayList<>();
    
    final AudioRoutesInfo mCurAudioRoutesInfo = new AudioRoutesInfo();
    
    int mCurrentUserId = -1;
    
    final IAudioRoutesObserver.Stub mAudioRoutesObserver = (IAudioRoutesObserver.Stub)new Object(this);
    
    boolean mActivelyScanningWifiDisplays;
    
    final IAudioService mAudioService;
    
    MediaRouter.RouteInfo mBluetoothA2dpRoute;
    
    final boolean mCanConfigureWifiDisplays;
    
    IMediaRouterClient mClient;
    
    MediaRouterClientState mClientState;
    
    MediaRouter.RouteInfo mDefaultAudioVideo;
    
    boolean mDiscoverRequestActiveScan;
    
    int mDiscoveryRequestRouteTypes;
    
    final DisplayManager mDisplayService;
    
    final Handler mHandler;
    
    final IMediaRouterService mMediaRouterService;
    
    final String mPackageName;
    
    String mPreviousActiveWifiDisplayAddress;
    
    final Resources mResources;
    
    MediaRouter.RouteInfo mSelectedRoute;
    
    final MediaRouter.RouteCategory mSystemCategory;
    
    Static(MediaRouter this$0) {
      this.mPackageName = this$0.getPackageName();
      this.mResources = this$0.getResources();
      this.mHandler = new Handler(this$0.getMainLooper());
      IBinder iBinder = ServiceManager.getService("audio");
      this.mAudioService = IAudioService.Stub.asInterface(iBinder);
      this.mDisplayService = (DisplayManager)this$0.getSystemService("display");
      iBinder = ServiceManager.getService("media_router");
      this.mMediaRouterService = IMediaRouterService.Stub.asInterface(iBinder);
      boolean bool = false;
      MediaRouter.RouteCategory routeCategory = new MediaRouter.RouteCategory(17040049, 3, false);
      routeCategory.mIsSystem = true;
      int i = Process.myPid(), j = Process.myUid();
      if (this$0.checkPermission("android.permission.CONFIGURE_WIFI_DISPLAY", i, j) == 0)
        bool = true; 
      this.mCanConfigureWifiDisplays = bool;
    }
    
    void startMonitoringRoutes(Context param1Context) {
      AudioRoutesInfo audioRoutesInfo;
      MediaRouter.RouteInfo routeInfo = new MediaRouter.RouteInfo(this.mSystemCategory);
      routeInfo.mNameResId = 17040050;
      this.mDefaultAudioVideo.mSupportedTypes = 3;
      this.mDefaultAudioVideo.updatePresentationDisplay();
      AudioManager audioManager = (AudioManager)param1Context.getSystemService("audio");
      if (audioManager.isVolumeFixed())
        this.mDefaultAudioVideo.mVolumeHandling = 0; 
      MediaRouter.addRouteStatic(this.mDefaultAudioVideo);
      MediaRouter.updateWifiDisplayStatus(this.mDisplayService.getWifiDisplayStatus());
      param1Context.registerReceiver(new MediaRouter.WifiDisplayStatusChangedReceiver(), new IntentFilter("android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"));
      param1Context.registerReceiver(new MediaRouter.VolumeChangeReceiver(), new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
      this.mDisplayService.registerDisplayListener(this, this.mHandler);
      param1Context = null;
      try {
        AudioRoutesInfo audioRoutesInfo1 = this.mAudioService.startWatchingRoutes(this.mAudioRoutesObserver);
      } catch (RemoteException remoteException) {}
      if (audioRoutesInfo != null)
        updateAudioRoutes(audioRoutesInfo); 
      rebindAsUser(UserHandle.myUserId());
      if (this.mSelectedRoute == null)
        MediaRouter.selectDefaultRouteStatic(); 
    }
    
    void updateAudioRoutes(AudioRoutesInfo param1AudioRoutesInfo) {
      // Byte code:
      //   0: iconst_0
      //   1: istore_2
      //   2: iconst_0
      //   3: istore_3
      //   4: iconst_0
      //   5: istore #4
      //   7: aload_1
      //   8: getfield mainType : I
      //   11: aload_0
      //   12: getfield mCurAudioRoutesInfo : Landroid/media/AudioRoutesInfo;
      //   15: getfield mainType : I
      //   18: if_icmpeq -> 146
      //   21: aload_0
      //   22: getfield mCurAudioRoutesInfo : Landroid/media/AudioRoutesInfo;
      //   25: aload_1
      //   26: getfield mainType : I
      //   29: putfield mainType : I
      //   32: aload_1
      //   33: getfield mainType : I
      //   36: iconst_2
      //   37: iand
      //   38: ifne -> 110
      //   41: aload_1
      //   42: getfield mainType : I
      //   45: iconst_1
      //   46: iand
      //   47: ifeq -> 53
      //   50: goto -> 110
      //   53: aload_1
      //   54: getfield mainType : I
      //   57: iconst_4
      //   58: iand
      //   59: ifeq -> 69
      //   62: ldc_w 17040051
      //   65: istore_3
      //   66: goto -> 114
      //   69: aload_1
      //   70: getfield mainType : I
      //   73: bipush #8
      //   75: iand
      //   76: ifeq -> 86
      //   79: ldc_w 17040052
      //   82: istore_3
      //   83: goto -> 114
      //   86: aload_1
      //   87: getfield mainType : I
      //   90: bipush #16
      //   92: iand
      //   93: ifeq -> 103
      //   96: ldc_w 17040054
      //   99: istore_3
      //   100: goto -> 114
      //   103: ldc_w 17040050
      //   106: istore_3
      //   107: goto -> 114
      //   110: ldc_w 17040053
      //   113: istore_3
      //   114: aload_0
      //   115: getfield mDefaultAudioVideo : Landroid/media/MediaRouter$RouteInfo;
      //   118: iload_3
      //   119: putfield mNameResId : I
      //   122: aload_0
      //   123: getfield mDefaultAudioVideo : Landroid/media/MediaRouter$RouteInfo;
      //   126: invokestatic dispatchRouteChanged : (Landroid/media/MediaRouter$RouteInfo;)V
      //   129: iload #4
      //   131: istore_3
      //   132: aload_1
      //   133: getfield mainType : I
      //   136: bipush #19
      //   138: iand
      //   139: ifeq -> 144
      //   142: iconst_1
      //   143: istore_3
      //   144: iconst_1
      //   145: istore_2
      //   146: aload_1
      //   147: getfield bluetoothName : Ljava/lang/CharSequence;
      //   150: aload_0
      //   151: getfield mCurAudioRoutesInfo : Landroid/media/AudioRoutesInfo;
      //   154: getfield bluetoothName : Ljava/lang/CharSequence;
      //   157: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
      //   160: ifne -> 290
      //   163: iconst_0
      //   164: istore_3
      //   165: aload_1
      //   166: getfield bluetoothName : Ljava/lang/CharSequence;
      //   169: ifnull -> 265
      //   172: aload_0
      //   173: getfield mBluetoothA2dpRoute : Landroid/media/MediaRouter$RouteInfo;
      //   176: astore #5
      //   178: aload #5
      //   180: ifnonnull -> 246
      //   183: new android/media/MediaRouter$RouteInfo
      //   186: dup
      //   187: aload_0
      //   188: getfield mSystemCategory : Landroid/media/MediaRouter$RouteCategory;
      //   191: invokespecial <init> : (Landroid/media/MediaRouter$RouteCategory;)V
      //   194: astore #5
      //   196: aload #5
      //   198: aload_1
      //   199: getfield bluetoothName : Ljava/lang/CharSequence;
      //   202: putfield mName : Ljava/lang/CharSequence;
      //   205: aload #5
      //   207: aload_0
      //   208: getfield mResources : Landroid/content/res/Resources;
      //   211: ldc_w 17039773
      //   214: invokevirtual getText : (I)Ljava/lang/CharSequence;
      //   217: putfield mDescription : Ljava/lang/CharSequence;
      //   220: aload #5
      //   222: iconst_1
      //   223: putfield mSupportedTypes : I
      //   226: aload #5
      //   228: iconst_3
      //   229: putfield mDeviceType : I
      //   232: aload_0
      //   233: aload #5
      //   235: putfield mBluetoothA2dpRoute : Landroid/media/MediaRouter$RouteInfo;
      //   238: aload #5
      //   240: invokestatic addRouteStatic : (Landroid/media/MediaRouter$RouteInfo;)V
      //   243: goto -> 288
      //   246: aload #5
      //   248: aload_1
      //   249: getfield bluetoothName : Ljava/lang/CharSequence;
      //   252: putfield mName : Ljava/lang/CharSequence;
      //   255: aload_0
      //   256: getfield mBluetoothA2dpRoute : Landroid/media/MediaRouter$RouteInfo;
      //   259: invokestatic dispatchRouteChanged : (Landroid/media/MediaRouter$RouteInfo;)V
      //   262: goto -> 288
      //   265: aload_0
      //   266: getfield mBluetoothA2dpRoute : Landroid/media/MediaRouter$RouteInfo;
      //   269: ifnull -> 288
      //   272: aload_0
      //   273: getfield mBluetoothA2dpRoute : Landroid/media/MediaRouter$RouteInfo;
      //   276: astore #5
      //   278: aload_0
      //   279: aconst_null
      //   280: putfield mBluetoothA2dpRoute : Landroid/media/MediaRouter$RouteInfo;
      //   283: aload #5
      //   285: invokestatic removeRouteStatic : (Landroid/media/MediaRouter$RouteInfo;)V
      //   288: iconst_1
      //   289: istore_2
      //   290: iload_2
      //   291: ifeq -> 415
      //   294: new java/lang/StringBuilder
      //   297: dup
      //   298: invokespecial <init> : ()V
      //   301: astore #5
      //   303: aload #5
      //   305: ldc_w 'Audio routes updated: '
      //   308: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   311: pop
      //   312: aload #5
      //   314: aload_1
      //   315: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   318: pop
      //   319: aload #5
      //   321: ldc_w ', a2dp='
      //   324: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   327: pop
      //   328: aload #5
      //   330: aload_0
      //   331: invokevirtual isBluetoothA2dpOn : ()Z
      //   334: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   337: pop
      //   338: ldc 'MediaRouter'
      //   340: aload #5
      //   342: invokevirtual toString : ()Ljava/lang/String;
      //   345: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   348: pop
      //   349: aload_0
      //   350: getfield mSelectedRoute : Landroid/media/MediaRouter$RouteInfo;
      //   353: astore #5
      //   355: aload #5
      //   357: ifnull -> 378
      //   360: aload #5
      //   362: aload_0
      //   363: getfield mDefaultAudioVideo : Landroid/media/MediaRouter$RouteInfo;
      //   366: if_acmpeq -> 378
      //   369: aload #5
      //   371: aload_0
      //   372: getfield mBluetoothA2dpRoute : Landroid/media/MediaRouter$RouteInfo;
      //   375: if_acmpne -> 415
      //   378: iload_3
      //   379: ifne -> 406
      //   382: aload_0
      //   383: getfield mBluetoothA2dpRoute : Landroid/media/MediaRouter$RouteInfo;
      //   386: astore #5
      //   388: aload #5
      //   390: ifnonnull -> 396
      //   393: goto -> 406
      //   396: iconst_1
      //   397: aload #5
      //   399: iconst_0
      //   400: invokestatic selectRouteStatic : (ILandroid/media/MediaRouter$RouteInfo;Z)V
      //   403: goto -> 415
      //   406: iconst_1
      //   407: aload_0
      //   408: getfield mDefaultAudioVideo : Landroid/media/MediaRouter$RouteInfo;
      //   411: iconst_0
      //   412: invokestatic selectRouteStatic : (ILandroid/media/MediaRouter$RouteInfo;Z)V
      //   415: aload_0
      //   416: getfield mCurAudioRoutesInfo : Landroid/media/AudioRoutesInfo;
      //   419: aload_1
      //   420: getfield bluetoothName : Ljava/lang/CharSequence;
      //   423: putfield bluetoothName : Ljava/lang/CharSequence;
      //   426: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #194	-> 0
      //   #195	-> 2
      //   #197	-> 7
      //   #198	-> 21
      //   #200	-> 32
      //   #203	-> 53
      //   #204	-> 62
      //   #205	-> 69
      //   #206	-> 79
      //   #207	-> 86
      //   #208	-> 96
      //   #210	-> 103
      //   #202	-> 110
      //   #212	-> 114
      //   #213	-> 122
      //   #215	-> 129
      //   #217	-> 142
      //   #219	-> 144
      //   #222	-> 146
      //   #223	-> 163
      //   #224	-> 165
      //   #225	-> 172
      //   #227	-> 183
      //   #228	-> 196
      //   #229	-> 205
      //   #231	-> 220
      //   #232	-> 226
      //   #233	-> 232
      //   #234	-> 238
      //   #235	-> 243
      //   #236	-> 246
      //   #237	-> 255
      //   #239	-> 265
      //   #241	-> 272
      //   #242	-> 278
      //   #243	-> 283
      //   #245	-> 288
      //   #248	-> 290
      //   #249	-> 294
      //   #250	-> 349
      //   #252	-> 378
      //   #255	-> 396
      //   #253	-> 406
      //   #259	-> 415
      //   #260	-> 426
    }
    
    boolean isBluetoothA2dpOn() {
      boolean bool1 = false, bool2 = bool1;
      try {
        if (this.mBluetoothA2dpRoute != null) {
          boolean bool = this.mAudioService.isBluetoothA2dpOn();
          bool2 = bool1;
          if (bool)
            bool2 = true; 
        } 
        return bool2;
      } catch (RemoteException remoteException) {
        Log.e("MediaRouter", "Error querying Bluetooth A2DP state", (Throwable)remoteException);
        return false;
      } 
    }
    
    void updateDiscoveryRequest() {
      // Byte code:
      //   0: iconst_0
      //   1: istore_1
      //   2: iconst_0
      //   3: istore_2
      //   4: iconst_0
      //   5: istore_3
      //   6: iconst_0
      //   7: istore #4
      //   9: aload_0
      //   10: getfield mCallbacks : Ljava/util/concurrent/CopyOnWriteArrayList;
      //   13: invokevirtual size : ()I
      //   16: istore #5
      //   18: iconst_0
      //   19: istore #6
      //   21: iload #6
      //   23: iload #5
      //   25: if_icmpge -> 143
      //   28: aload_0
      //   29: getfield mCallbacks : Ljava/util/concurrent/CopyOnWriteArrayList;
      //   32: iload #6
      //   34: invokevirtual get : (I)Ljava/lang/Object;
      //   37: checkcast android/media/MediaRouter$CallbackInfo
      //   40: astore #7
      //   42: aload #7
      //   44: getfield flags : I
      //   47: iconst_5
      //   48: iand
      //   49: ifeq -> 63
      //   52: iload_1
      //   53: aload #7
      //   55: getfield type : I
      //   58: ior
      //   59: istore_1
      //   60: goto -> 93
      //   63: aload #7
      //   65: getfield flags : I
      //   68: bipush #8
      //   70: iand
      //   71: ifeq -> 85
      //   74: iload_2
      //   75: aload #7
      //   77: getfield type : I
      //   80: ior
      //   81: istore_2
      //   82: goto -> 93
      //   85: iload_1
      //   86: aload #7
      //   88: getfield type : I
      //   91: ior
      //   92: istore_1
      //   93: iload #4
      //   95: istore #8
      //   97: iconst_1
      //   98: aload #7
      //   100: getfield flags : I
      //   103: iand
      //   104: ifeq -> 133
      //   107: iconst_1
      //   108: istore #9
      //   110: iload #9
      //   112: istore_3
      //   113: iload #4
      //   115: istore #8
      //   117: iconst_4
      //   118: aload #7
      //   120: getfield type : I
      //   123: iand
      //   124: ifeq -> 133
      //   127: iconst_1
      //   128: istore #8
      //   130: iload #9
      //   132: istore_3
      //   133: iinc #6, 1
      //   136: iload #8
      //   138: istore #4
      //   140: goto -> 21
      //   143: iload_1
      //   144: ifne -> 154
      //   147: iload_1
      //   148: istore #6
      //   150: iload_3
      //   151: ifeq -> 159
      //   154: iload_1
      //   155: iload_2
      //   156: ior
      //   157: istore #6
      //   159: aload_0
      //   160: getfield mCanConfigureWifiDisplays : Z
      //   163: ifeq -> 239
      //   166: aload_0
      //   167: getfield mSelectedRoute : Landroid/media/MediaRouter$RouteInfo;
      //   170: astore #7
      //   172: iload #4
      //   174: istore_1
      //   175: aload #7
      //   177: ifnull -> 194
      //   180: iload #4
      //   182: istore_1
      //   183: aload #7
      //   185: iconst_4
      //   186: invokevirtual matchesTypes : (I)Z
      //   189: ifeq -> 194
      //   192: iconst_0
      //   193: istore_1
      //   194: iload_1
      //   195: ifeq -> 220
      //   198: aload_0
      //   199: getfield mActivelyScanningWifiDisplays : Z
      //   202: ifne -> 239
      //   205: aload_0
      //   206: iconst_1
      //   207: putfield mActivelyScanningWifiDisplays : Z
      //   210: aload_0
      //   211: getfield mDisplayService : Landroid/hardware/display/DisplayManager;
      //   214: invokevirtual startWifiDisplayScan : ()V
      //   217: goto -> 239
      //   220: aload_0
      //   221: getfield mActivelyScanningWifiDisplays : Z
      //   224: ifeq -> 239
      //   227: aload_0
      //   228: iconst_0
      //   229: putfield mActivelyScanningWifiDisplays : Z
      //   232: aload_0
      //   233: getfield mDisplayService : Landroid/hardware/display/DisplayManager;
      //   236: invokevirtual stopWifiDisplayScan : ()V
      //   239: iload #6
      //   241: aload_0
      //   242: getfield mDiscoveryRequestRouteTypes : I
      //   245: if_icmpne -> 256
      //   248: iload_3
      //   249: aload_0
      //   250: getfield mDiscoverRequestActiveScan : Z
      //   253: if_icmpeq -> 271
      //   256: aload_0
      //   257: iload #6
      //   259: putfield mDiscoveryRequestRouteTypes : I
      //   262: aload_0
      //   263: iload_3
      //   264: putfield mDiscoverRequestActiveScan : Z
      //   267: aload_0
      //   268: invokevirtual publishClientDiscoveryRequest : ()V
      //   271: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #273	-> 0
      //   #274	-> 2
      //   #275	-> 4
      //   #276	-> 6
      //   #277	-> 9
      //   #278	-> 18
      //   #279	-> 28
      //   #280	-> 42
      //   #283	-> 52
      //   #284	-> 63
      //   #286	-> 74
      //   #291	-> 85
      //   #293	-> 93
      //   #294	-> 107
      //   #295	-> 110
      //   #296	-> 127
      //   #278	-> 133
      //   #300	-> 143
      //   #304	-> 154
      //   #309	-> 159
      //   #310	-> 166
      //   #311	-> 180
      //   #314	-> 192
      //   #316	-> 194
      //   #317	-> 198
      //   #318	-> 205
      //   #319	-> 210
      //   #322	-> 220
      //   #323	-> 227
      //   #324	-> 232
      //   #330	-> 239
      //   #332	-> 256
      //   #333	-> 262
      //   #334	-> 267
      //   #336	-> 271
    }
    
    public void onDisplayAdded(int param1Int) {
      updatePresentationDisplays(param1Int);
    }
    
    public void onDisplayChanged(int param1Int) {
      updatePresentationDisplays(param1Int);
    }
    
    public void onDisplayRemoved(int param1Int) {
      updatePresentationDisplays(param1Int);
    }
    
    public void setRouterGroupId(String param1String) {
      IMediaRouterClient iMediaRouterClient = this.mClient;
      if (iMediaRouterClient != null)
        try {
          this.mMediaRouterService.registerClientGroupId(iMediaRouterClient, param1String);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Unable to register group ID of the client.", (Throwable)remoteException);
        }  
    }
    
    public Display[] getAllPresentationDisplays() {
      return this.mDisplayService.getDisplays("android.hardware.display.category.PRESENTATION");
    }
    
    private void updatePresentationDisplays(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mRoutes : Ljava/util/ArrayList;
      //   4: invokevirtual size : ()I
      //   7: istore_2
      //   8: iconst_0
      //   9: istore_3
      //   10: iload_3
      //   11: iload_2
      //   12: if_icmpge -> 71
      //   15: aload_0
      //   16: getfield mRoutes : Ljava/util/ArrayList;
      //   19: iload_3
      //   20: invokevirtual get : (I)Ljava/lang/Object;
      //   23: checkcast android/media/MediaRouter$RouteInfo
      //   26: astore #4
      //   28: aload #4
      //   30: invokevirtual updatePresentationDisplay : ()Z
      //   33: ifne -> 60
      //   36: aload #4
      //   38: getfield mPresentationDisplay : Landroid/view/Display;
      //   41: ifnull -> 65
      //   44: aload #4
      //   46: getfield mPresentationDisplay : Landroid/view/Display;
      //   49: astore #5
      //   51: aload #5
      //   53: invokevirtual getDisplayId : ()I
      //   56: iload_1
      //   57: if_icmpne -> 65
      //   60: aload #4
      //   62: invokestatic dispatchRoutePresentationDisplayChanged : (Landroid/media/MediaRouter$RouteInfo;)V
      //   65: iinc #3, 1
      //   68: goto -> 10
      //   71: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #368	-> 0
      //   #369	-> 8
      //   #370	-> 15
      //   #371	-> 28
      //   #372	-> 51
      //   #373	-> 60
      //   #369	-> 65
      //   #376	-> 71
    }
    
    void updateSelectedRouteForId(String param1String) {
      MediaRouter.RouteInfo routeInfo;
      if (isBluetoothA2dpOn()) {
        routeInfo = this.mBluetoothA2dpRoute;
      } else {
        routeInfo = this.mDefaultAudioVideo;
      } 
      int i = this.mRoutes.size();
      for (byte b = 0; b < i; b++) {
        MediaRouter.RouteInfo routeInfo1 = this.mRoutes.get(b);
        if (TextUtils.equals(routeInfo1.mGlobalRouteId, param1String))
          routeInfo = routeInfo1; 
      } 
      if (routeInfo != this.mSelectedRoute)
        MediaRouter.selectRouteStatic(routeInfo.mSupportedTypes, routeInfo, false); 
    }
    
    void setSelectedRoute(MediaRouter.RouteInfo param1RouteInfo, boolean param1Boolean) {
      this.mSelectedRoute = param1RouteInfo;
      publishClientSelectedRoute(param1Boolean);
    }
    
    void rebindAsUser(int param1Int) {
      if (this.mCurrentUserId != param1Int || param1Int < 0 || this.mClient == null) {
        IMediaRouterClient iMediaRouterClient = this.mClient;
        if (iMediaRouterClient != null) {
          try {
            this.mMediaRouterService.unregisterClient(iMediaRouterClient);
          } catch (RemoteException remoteException) {
            Log.e("MediaRouter", "Unable to unregister media router client.", (Throwable)remoteException);
          } 
          this.mClient = null;
        } 
        this.mCurrentUserId = param1Int;
        try {
          iMediaRouterClient = new Client();
          super(this);
          this.mMediaRouterService.registerClientAsUser(iMediaRouterClient, this.mPackageName, param1Int);
          this.mClient = iMediaRouterClient;
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Unable to register media router client.", (Throwable)remoteException);
        } 
        publishClientDiscoveryRequest();
        publishClientSelectedRoute(false);
        updateClientState();
      } 
    }
    
    void publishClientDiscoveryRequest() {
      IMediaRouterClient iMediaRouterClient = this.mClient;
      if (iMediaRouterClient != null)
        try {
          this.mMediaRouterService.setDiscoveryRequest(iMediaRouterClient, this.mDiscoveryRequestRouteTypes, this.mDiscoverRequestActiveScan);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Unable to publish media router client discovery request.", (Throwable)remoteException);
        }  
    }
    
    void publishClientSelectedRoute(boolean param1Boolean) {
      IMediaRouterClient iMediaRouterClient = this.mClient;
      if (iMediaRouterClient != null)
        try {
          String str;
          IMediaRouterService iMediaRouterService = this.mMediaRouterService;
          if (this.mSelectedRoute != null) {
            str = this.mSelectedRoute.mGlobalRouteId;
          } else {
            str = null;
          } 
          iMediaRouterService.setSelectedRoute(iMediaRouterClient, str, param1Boolean);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Unable to publish media router client selected route.", (Throwable)remoteException);
        }  
    }
    
    void updateClientState() {
      byte b;
      ArrayList<MediaRouterClientState.RouteInfo> arrayList = null;
      this.mClientState = null;
      IMediaRouterClient iMediaRouterClient = this.mClient;
      if (iMediaRouterClient != null)
        try {
          this.mClientState = this.mMediaRouterService.getState(iMediaRouterClient);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Unable to retrieve media router client state.", (Throwable)remoteException);
        }  
      MediaRouterClientState mediaRouterClientState = this.mClientState;
      if (mediaRouterClientState != null)
        arrayList = mediaRouterClientState.routes; 
      if (arrayList != null) {
        b = arrayList.size();
      } else {
        b = 0;
      } 
      int i;
      for (i = 0; i < b; i++) {
        MediaRouterClientState.RouteInfo routeInfo1 = arrayList.get(i);
        MediaRouter.RouteInfo routeInfo = findGlobalRoute(routeInfo1.id);
        if (routeInfo == null) {
          routeInfo = makeGlobalRoute(routeInfo1);
          MediaRouter.addRouteStatic(routeInfo);
        } else {
          updateGlobalRoute(routeInfo, routeInfo1);
        } 
      } 
      i = this.mRoutes.size();
      while (true) {
        int j = i - 1;
        if (i > 0) {
          MediaRouter.RouteInfo routeInfo = this.mRoutes.get(j);
          String str = routeInfo.mGlobalRouteId;
          if (str != null) {
            i = 0;
            while (true) {
              if (i < b) {
                MediaRouterClientState.RouteInfo routeInfo1 = arrayList.get(i);
                if (str.equals(routeInfo1.id))
                  break; 
                i++;
                continue;
              } 
              MediaRouter.removeRouteStatic(routeInfo);
              break;
            } 
          } 
          i = j;
          continue;
        } 
        break;
      } 
    }
    
    void requestSetVolume(MediaRouter.RouteInfo param1RouteInfo, int param1Int) {
      if (param1RouteInfo.mGlobalRouteId != null) {
        IMediaRouterClient iMediaRouterClient = this.mClient;
        if (iMediaRouterClient != null)
          try {
            this.mMediaRouterService.requestSetVolume(iMediaRouterClient, param1RouteInfo.mGlobalRouteId, param1Int);
          } catch (RemoteException remoteException) {
            Log.w("MediaRouter", "Unable to request volume change.", (Throwable)remoteException);
          }  
      } 
    }
    
    void requestUpdateVolume(MediaRouter.RouteInfo param1RouteInfo, int param1Int) {
      if (param1RouteInfo.mGlobalRouteId != null) {
        IMediaRouterClient iMediaRouterClient = this.mClient;
        if (iMediaRouterClient != null)
          try {
            this.mMediaRouterService.requestUpdateVolume(iMediaRouterClient, param1RouteInfo.mGlobalRouteId, param1Int);
          } catch (RemoteException remoteException) {
            Log.w("MediaRouter", "Unable to request volume change.", (Throwable)remoteException);
          }  
      } 
    }
    
    MediaRouter.RouteInfo makeGlobalRoute(MediaRouterClientState.RouteInfo param1RouteInfo) {
      MediaRouter.RouteInfo routeInfo = new MediaRouter.RouteInfo(this.mSystemCategory);
      routeInfo.mGlobalRouteId = param1RouteInfo.id;
      routeInfo.mName = param1RouteInfo.name;
      routeInfo.mDescription = param1RouteInfo.description;
      routeInfo.mSupportedTypes = param1RouteInfo.supportedTypes;
      routeInfo.mDeviceType = param1RouteInfo.deviceType;
      routeInfo.mEnabled = param1RouteInfo.enabled;
      routeInfo.setRealStatusCode(param1RouteInfo.statusCode);
      routeInfo.mPlaybackType = param1RouteInfo.playbackType;
      routeInfo.mPlaybackStream = param1RouteInfo.playbackStream;
      routeInfo.mVolume = param1RouteInfo.volume;
      routeInfo.mVolumeMax = param1RouteInfo.volumeMax;
      routeInfo.mVolumeHandling = param1RouteInfo.volumeHandling;
      routeInfo.mPresentationDisplayId = param1RouteInfo.presentationDisplayId;
      routeInfo.updatePresentationDisplay();
      return routeInfo;
    }
    
    void updateGlobalRoute(MediaRouter.RouteInfo param1RouteInfo, MediaRouterClientState.RouteInfo param1RouteInfo1) {
      boolean bool1 = false;
      boolean bool2 = false;
      boolean bool3 = false;
      if (!Objects.equals(param1RouteInfo.mName, param1RouteInfo1.name)) {
        param1RouteInfo.mName = param1RouteInfo1.name;
        bool1 = true;
      } 
      if (!Objects.equals(param1RouteInfo.mDescription, param1RouteInfo1.description)) {
        param1RouteInfo.mDescription = param1RouteInfo1.description;
        bool1 = true;
      } 
      int i = param1RouteInfo.mSupportedTypes;
      if (i != param1RouteInfo1.supportedTypes) {
        param1RouteInfo.mSupportedTypes = param1RouteInfo1.supportedTypes;
        bool1 = true;
      } 
      if (param1RouteInfo.mEnabled != param1RouteInfo1.enabled) {
        param1RouteInfo.mEnabled = param1RouteInfo1.enabled;
        bool1 = true;
      } 
      if (param1RouteInfo.mRealStatusCode != param1RouteInfo1.statusCode) {
        param1RouteInfo.setRealStatusCode(param1RouteInfo1.statusCode);
        bool1 = true;
      } 
      if (param1RouteInfo.mPlaybackType != param1RouteInfo1.playbackType) {
        param1RouteInfo.mPlaybackType = param1RouteInfo1.playbackType;
        bool1 = true;
      } 
      if (param1RouteInfo.mPlaybackStream != param1RouteInfo1.playbackStream) {
        param1RouteInfo.mPlaybackStream = param1RouteInfo1.playbackStream;
        bool1 = true;
      } 
      boolean bool4 = bool1;
      bool1 = bool2;
      if (param1RouteInfo.mVolume != param1RouteInfo1.volume) {
        param1RouteInfo.mVolume = param1RouteInfo1.volume;
        bool4 = true;
        bool1 = true;
      } 
      if (param1RouteInfo.mVolumeMax != param1RouteInfo1.volumeMax) {
        param1RouteInfo.mVolumeMax = param1RouteInfo1.volumeMax;
        bool4 = true;
        bool1 = true;
      } 
      if (param1RouteInfo.mVolumeHandling != param1RouteInfo1.volumeHandling) {
        param1RouteInfo.mVolumeHandling = param1RouteInfo1.volumeHandling;
        bool4 = true;
        bool1 = true;
      } 
      if (param1RouteInfo.mPresentationDisplayId != param1RouteInfo1.presentationDisplayId) {
        param1RouteInfo.mPresentationDisplayId = param1RouteInfo1.presentationDisplayId;
        param1RouteInfo.updatePresentationDisplay();
        bool4 = true;
        bool3 = true;
      } 
      if (bool4)
        MediaRouter.dispatchRouteChanged(param1RouteInfo, i); 
      if (bool1)
        MediaRouter.dispatchRouteVolumeChanged(param1RouteInfo); 
      if (bool3)
        MediaRouter.dispatchRoutePresentationDisplayChanged(param1RouteInfo); 
    }
    
    MediaRouter.RouteInfo findGlobalRoute(String param1String) {
      int i = this.mRoutes.size();
      for (byte b = 0; b < i; b++) {
        MediaRouter.RouteInfo routeInfo = this.mRoutes.get(b);
        if (param1String.equals(routeInfo.mGlobalRouteId))
          return routeInfo; 
      } 
      return null;
    }
    
    boolean isPlaybackActive() {
      IMediaRouterClient iMediaRouterClient = this.mClient;
      if (iMediaRouterClient != null)
        try {
          return this.mMediaRouterService.isPlaybackActive(iMediaRouterClient);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Unable to retrieve playback active state.", (Throwable)remoteException);
        }  
      return false;
    }
    
    class Client extends IMediaRouterClient.Stub {
      final MediaRouter.Static this$0;
      
      public void onStateChanged() {
        MediaRouter.Static.this.mHandler.post(new _$$Lambda$MediaRouter$Static$Client$XF0KN_1sVIeOFknsjg3lpuzXcTA(this));
      }
      
      public void onRestoreRoute() {
        MediaRouter.Static.this.mHandler.post(new _$$Lambda$MediaRouter$Static$Client$v_XMhGBiQAd3JDWrYnJwEnhJ7uo(this));
      }
      
      public void onSelectedRouteChanged(String param2String) {
        MediaRouter.Static.this.mHandler.post(new _$$Lambda$MediaRouter$Static$Client$dRtmHNVGzO9WYhZljZDXfu3CN0M(this, param2String));
      }
      
      public void onGlobalA2dpChanged(boolean param2Boolean) {
        MediaRouter.Static.this.mHandler.post(new _$$Lambda$MediaRouter$Static$Client$QYhpJCCUIsqhdBorG19DlY1wt5w(this, param2Boolean));
      }
    }
  }
  
  static final HashMap<Context, MediaRouter> sRouters = new HashMap<>();
  
  static Static sStatic;
  
  static String typesToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    if ((paramInt & 0x1) != 0)
      stringBuilder.append("ROUTE_TYPE_LIVE_AUDIO "); 
    if ((paramInt & 0x2) != 0)
      stringBuilder.append("ROUTE_TYPE_LIVE_VIDEO "); 
    if ((paramInt & 0x4) != 0)
      stringBuilder.append("ROUTE_TYPE_REMOTE_DISPLAY "); 
    if ((0x800000 & paramInt) != 0)
      stringBuilder.append("ROUTE_TYPE_USER "); 
    return stringBuilder.toString();
  }
  
  public MediaRouter(Context paramContext) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: ldc android/media/MediaRouter$Static
    //   6: monitorenter
    //   7: getstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   10: ifnonnull -> 36
    //   13: aload_1
    //   14: invokevirtual getApplicationContext : ()Landroid/content/Context;
    //   17: astore_2
    //   18: new android/media/MediaRouter$Static
    //   21: astore_1
    //   22: aload_1
    //   23: aload_2
    //   24: invokespecial <init> : (Landroid/content/Context;)V
    //   27: aload_1
    //   28: putstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   31: aload_1
    //   32: aload_2
    //   33: invokevirtual startMonitoringRoutes : (Landroid/content/Context;)V
    //   36: ldc android/media/MediaRouter$Static
    //   38: monitorexit
    //   39: return
    //   40: astore_1
    //   41: ldc android/media/MediaRouter$Static
    //   43: monitorexit
    //   44: aload_1
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #807	-> 0
    //   #808	-> 4
    //   #809	-> 7
    //   #810	-> 13
    //   #811	-> 18
    //   #812	-> 31
    //   #814	-> 36
    //   #815	-> 39
    //   #814	-> 40
    // Exception table:
    //   from	to	target	type
    //   7	13	40	finally
    //   13	18	40	finally
    //   18	31	40	finally
    //   31	36	40	finally
    //   36	39	40	finally
    //   41	44	40	finally
  }
  
  public RouteInfo getDefaultRoute() {
    return sStatic.mDefaultAudioVideo;
  }
  
  public RouteInfo getFallbackRoute() {
    RouteInfo routeInfo;
    if (sStatic.mBluetoothA2dpRoute != null) {
      routeInfo = sStatic.mBluetoothA2dpRoute;
    } else {
      routeInfo = sStatic.mDefaultAudioVideo;
    } 
    return routeInfo;
  }
  
  public RouteCategory getSystemCategory() {
    return sStatic.mSystemCategory;
  }
  
  public RouteInfo getSelectedRoute() {
    return getSelectedRoute(8388615);
  }
  
  public RouteInfo getSelectedRoute(int paramInt) {
    if (sStatic.mSelectedRoute != null && (sStatic.mSelectedRoute.mSupportedTypes & paramInt) != 0)
      return sStatic.mSelectedRoute; 
    if (paramInt == 8388608)
      return null; 
    return sStatic.mDefaultAudioVideo;
  }
  
  public boolean isRouteAvailable(int paramInt1, int paramInt2) {
    int i = sStatic.mRoutes.size();
    for (byte b = 0; b < i; b++) {
      RouteInfo routeInfo = sStatic.mRoutes.get(b);
      if (routeInfo.matchesTypes(paramInt1) && ((
        paramInt2 & 0x1) == 0 || routeInfo != sStatic.mDefaultAudioVideo))
        return true; 
    } 
    return false;
  }
  
  public void setRouterGroupId(String paramString) {
    sStatic.setRouterGroupId(paramString);
  }
  
  public void addCallback(int paramInt, Callback paramCallback) {
    addCallback(paramInt, paramCallback, 0);
  }
  
  public void addCallback(int paramInt1, Callback paramCallback, int paramInt2) {
    CallbackInfo callbackInfo;
    int i = findCallbackInfo(paramCallback);
    if (i >= 0) {
      callbackInfo = sStatic.mCallbacks.get(i);
      callbackInfo.type |= paramInt1;
      callbackInfo.flags |= paramInt2;
    } else {
      callbackInfo = new CallbackInfo((Callback)callbackInfo, paramInt1, paramInt2, this);
      sStatic.mCallbacks.add(callbackInfo);
    } 
    sStatic.updateDiscoveryRequest();
  }
  
  public void removeCallback(Callback paramCallback) {
    int i = findCallbackInfo(paramCallback);
    if (i >= 0) {
      sStatic.mCallbacks.remove(i);
      sStatic.updateDiscoveryRequest();
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeCallback(");
      stringBuilder.append(paramCallback);
      stringBuilder.append("): callback not registered");
      Log.w("MediaRouter", stringBuilder.toString());
    } 
  }
  
  private int findCallbackInfo(Callback paramCallback) {
    int i = sStatic.mCallbacks.size();
    for (byte b = 0; b < i; b++) {
      CallbackInfo callbackInfo = sStatic.mCallbacks.get(b);
      if (callbackInfo.cb == paramCallback)
        return b; 
    } 
    return -1;
  }
  
  public void selectRoute(int paramInt, RouteInfo paramRouteInfo) {
    if (paramRouteInfo != null) {
      selectRouteStatic(paramInt, paramRouteInfo, true);
      return;
    } 
    throw new IllegalArgumentException("Route cannot be null.");
  }
  
  public void selectRouteInt(int paramInt, RouteInfo paramRouteInfo, boolean paramBoolean) {
    selectRouteStatic(paramInt, paramRouteInfo, paramBoolean);
  }
  
  static void selectRouteStatic(int paramInt, RouteInfo paramRouteInfo, boolean paramBoolean) {
    String str;
    RouteInfo routeInfo1;
    boolean bool2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Selecting route: ");
    stringBuilder.append(paramRouteInfo);
    Log.v("MediaRouter", stringBuilder.toString());
    RouteInfo routeInfo2 = sStatic.mSelectedRoute;
    if (sStatic.isBluetoothA2dpOn()) {
      routeInfo1 = sStatic.mBluetoothA2dpRoute;
    } else {
      routeInfo1 = sStatic.mDefaultAudioVideo;
    } 
    RouteInfo routeInfo3 = sStatic.mDefaultAudioVideo;
    boolean bool1 = false;
    if (routeInfo2 == routeInfo3 || routeInfo2 == sStatic.mBluetoothA2dpRoute) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (routeInfo2 == paramRouteInfo && (!bool2 || paramRouteInfo == routeInfo1))
      return; 
    if (!paramRouteInfo.matchesTypes(paramInt)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("selectRoute ignored; cannot select route with supported types ");
      stringBuilder1.append(typesToString(paramRouteInfo.getSupportedTypes()));
      stringBuilder1.append(" into route types ");
      stringBuilder1.append(typesToString(paramInt));
      str = stringBuilder1.toString();
      Log.w("MediaRouter", str);
      return;
    } 
    routeInfo3 = sStatic.mBluetoothA2dpRoute;
    if (sStatic.isPlaybackActive() && routeInfo3 != null && (paramInt & 0x1) != 0 && (str == routeInfo3 || str == sStatic.mDefaultAudioVideo))
      try {
        boolean bool;
        IAudioService iAudioService = sStatic.mAudioService;
        if (str == routeInfo3) {
          bool = true;
        } else {
          bool = false;
        } 
        iAudioService.setBluetoothA2dpOn(bool);
      } catch (RemoteException remoteException) {
        Log.e("MediaRouter", "Error changing Bluetooth A2DP state", (Throwable)remoteException);
      }  
    DisplayManager displayManager = sStatic.mDisplayService;
    WifiDisplay wifiDisplay = displayManager.getWifiDisplayStatus().getActiveDisplay();
    if (routeInfo2 != null && routeInfo2.mDeviceAddress != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (((RouteInfo)str).mDeviceAddress != null)
      bool1 = true; 
    if (wifiDisplay != null || bool2 || bool1)
      if (bool1 && !matchesDeviceAddress(wifiDisplay, (RouteInfo)str)) {
        if (sStatic.mCanConfigureWifiDisplays) {
          sStatic.mDisplayService.connectWifiDisplay(((RouteInfo)str).mDeviceAddress);
        } else {
          Log.e("MediaRouter", "Cannot connect to wifi displays because this process is not allowed to do so.");
        } 
      } else if (wifiDisplay != null && !bool1) {
        sStatic.mDisplayService.disconnectWifiDisplay();
      }  
    sStatic.setSelectedRoute((RouteInfo)str, paramBoolean);
    if (routeInfo2 != null) {
      dispatchRouteUnselected(routeInfo2.getSupportedTypes() & paramInt, routeInfo2);
      if (routeInfo2.resolveStatusCode())
        dispatchRouteChanged(routeInfo2); 
    } 
    if (str != null) {
      if (str.resolveStatusCode())
        dispatchRouteChanged((RouteInfo)str); 
      dispatchRouteSelected(str.getSupportedTypes() & paramInt, (RouteInfo)str);
    } 
    sStatic.updateDiscoveryRequest();
  }
  
  static void selectDefaultRouteStatic() {
    if (sStatic.mSelectedRoute != sStatic.mBluetoothA2dpRoute && sStatic.isBluetoothA2dpOn()) {
      selectRouteStatic(8388615, sStatic.mBluetoothA2dpRoute, false);
    } else {
      selectRouteStatic(8388615, sStatic.mDefaultAudioVideo, false);
    } 
  }
  
  static boolean matchesDeviceAddress(WifiDisplay paramWifiDisplay, RouteInfo paramRouteInfo) {
    boolean bool;
    if (paramRouteInfo != null && paramRouteInfo.mDeviceAddress != null) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramWifiDisplay == null && !bool)
      return true; 
    if (paramWifiDisplay != null && bool)
      return paramWifiDisplay.getDeviceAddress().equals(paramRouteInfo.mDeviceAddress); 
    return false;
  }
  
  public void addUserRoute(UserRouteInfo paramUserRouteInfo) {
    addRouteStatic(paramUserRouteInfo);
  }
  
  public void addRouteInt(RouteInfo paramRouteInfo) {
    addRouteStatic(paramRouteInfo);
  }
  
  static void addRouteStatic(RouteInfo paramRouteInfo) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Adding route: ");
      stringBuilder.append(paramRouteInfo);
      Log.d("MediaRouter", stringBuilder.toString());
    } 
    RouteCategory routeCategory = paramRouteInfo.getCategory();
    if (!sStatic.mCategories.contains(routeCategory))
      sStatic.mCategories.add(routeCategory); 
    if (routeCategory.isGroupable() && !(paramRouteInfo instanceof RouteGroup)) {
      RouteGroup routeGroup = new RouteGroup(paramRouteInfo.getCategory());
      routeGroup.mSupportedTypes = paramRouteInfo.mSupportedTypes;
      sStatic.mRoutes.add(routeGroup);
      dispatchRouteAdded(routeGroup);
      routeGroup.addRoute(paramRouteInfo);
    } else {
      sStatic.mRoutes.add(paramRouteInfo);
      dispatchRouteAdded(paramRouteInfo);
    } 
  }
  
  public void removeUserRoute(UserRouteInfo paramUserRouteInfo) {
    removeRouteStatic(paramUserRouteInfo);
  }
  
  public void clearUserRoutes() {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: iload_1
    //   3: getstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   6: getfield mRoutes : Ljava/util/ArrayList;
    //   9: invokevirtual size : ()I
    //   12: if_icmpge -> 60
    //   15: getstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   18: getfield mRoutes : Ljava/util/ArrayList;
    //   21: iload_1
    //   22: invokevirtual get : (I)Ljava/lang/Object;
    //   25: checkcast android/media/MediaRouter$RouteInfo
    //   28: astore_2
    //   29: aload_2
    //   30: instanceof android/media/MediaRouter$UserRouteInfo
    //   33: ifne -> 45
    //   36: iload_1
    //   37: istore_3
    //   38: aload_2
    //   39: instanceof android/media/MediaRouter$RouteGroup
    //   42: ifeq -> 53
    //   45: aload_2
    //   46: invokestatic removeRouteStatic : (Landroid/media/MediaRouter$RouteInfo;)V
    //   49: iload_1
    //   50: iconst_1
    //   51: isub
    //   52: istore_3
    //   53: iload_3
    //   54: iconst_1
    //   55: iadd
    //   56: istore_1
    //   57: goto -> 2
    //   60: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1172	-> 0
    //   #1173	-> 15
    //   #1176	-> 29
    //   #1177	-> 45
    //   #1178	-> 49
    //   #1172	-> 53
    //   #1181	-> 60
  }
  
  public void removeRouteInt(RouteInfo paramRouteInfo) {
    removeRouteStatic(paramRouteInfo);
  }
  
  static void removeRouteStatic(RouteInfo paramRouteInfo) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Removing route: ");
      stringBuilder.append(paramRouteInfo);
      Log.d("MediaRouter", stringBuilder.toString());
    } 
    if (sStatic.mRoutes.remove(paramRouteInfo)) {
      boolean bool2;
      RouteCategory routeCategory = paramRouteInfo.getCategory();
      int i = sStatic.mRoutes.size();
      boolean bool1 = false;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < i) {
          RouteCategory routeCategory1 = ((RouteInfo)sStatic.mRoutes.get(b)).getCategory();
          if (routeCategory == routeCategory1) {
            bool2 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (paramRouteInfo.isSelected())
        selectDefaultRouteStatic(); 
      if (!bool2)
        sStatic.mCategories.remove(routeCategory); 
      dispatchRouteRemoved(paramRouteInfo);
    } 
  }
  
  public int getCategoryCount() {
    return sStatic.mCategories.size();
  }
  
  public RouteCategory getCategoryAt(int paramInt) {
    return sStatic.mCategories.get(paramInt);
  }
  
  public int getRouteCount() {
    return sStatic.mRoutes.size();
  }
  
  public RouteInfo getRouteAt(int paramInt) {
    return sStatic.mRoutes.get(paramInt);
  }
  
  static int getRouteCountStatic() {
    return sStatic.mRoutes.size();
  }
  
  static RouteInfo getRouteAtStatic(int paramInt) {
    return sStatic.mRoutes.get(paramInt);
  }
  
  public UserRouteInfo createUserRoute(RouteCategory paramRouteCategory) {
    return new UserRouteInfo(paramRouteCategory);
  }
  
  public RouteCategory createRouteCategory(CharSequence paramCharSequence, boolean paramBoolean) {
    return new RouteCategory(paramCharSequence, 8388608, paramBoolean);
  }
  
  public RouteCategory createRouteCategory(int paramInt, boolean paramBoolean) {
    return new RouteCategory(paramInt, 8388608, paramBoolean);
  }
  
  public void rebindAsUser(int paramInt) {
    sStatic.rebindAsUser(paramInt);
  }
  
  static void updateRoute(RouteInfo paramRouteInfo) {
    dispatchRouteChanged(paramRouteInfo);
  }
  
  static void dispatchRouteSelected(int paramInt, RouteInfo paramRouteInfo) {
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      if (callbackInfo.filterRouteEvent(paramRouteInfo))
        callbackInfo.cb.onRouteSelected(callbackInfo.router, paramInt, paramRouteInfo); 
    } 
  }
  
  static void dispatchRouteUnselected(int paramInt, RouteInfo paramRouteInfo) {
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      if (callbackInfo.filterRouteEvent(paramRouteInfo))
        callbackInfo.cb.onRouteUnselected(callbackInfo.router, paramInt, paramRouteInfo); 
    } 
  }
  
  static void dispatchRouteChanged(RouteInfo paramRouteInfo) {
    dispatchRouteChanged(paramRouteInfo, paramRouteInfo.mSupportedTypes);
  }
  
  static void dispatchRouteChanged(RouteInfo paramRouteInfo, int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Dispatching route change: ");
      stringBuilder.append(paramRouteInfo);
      Log.d("MediaRouter", stringBuilder.toString());
    } 
    int i = paramRouteInfo.mSupportedTypes;
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      boolean bool1 = callbackInfo.filterRouteEvent(paramInt);
      boolean bool2 = callbackInfo.filterRouteEvent(i);
      if (!bool1 && bool2) {
        callbackInfo.cb.onRouteAdded(callbackInfo.router, paramRouteInfo);
        if (paramRouteInfo.isSelected())
          callbackInfo.cb.onRouteSelected(callbackInfo.router, i, paramRouteInfo); 
      } 
      if (bool1 || bool2)
        callbackInfo.cb.onRouteChanged(callbackInfo.router, paramRouteInfo); 
      if (bool1 && !bool2) {
        if (paramRouteInfo.isSelected())
          callbackInfo.cb.onRouteUnselected(callbackInfo.router, paramInt, paramRouteInfo); 
        callbackInfo.cb.onRouteRemoved(callbackInfo.router, paramRouteInfo);
      } 
    } 
  }
  
  static void dispatchRouteAdded(RouteInfo paramRouteInfo) {
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      if (callbackInfo.filterRouteEvent(paramRouteInfo))
        callbackInfo.cb.onRouteAdded(callbackInfo.router, paramRouteInfo); 
    } 
  }
  
  static void dispatchRouteRemoved(RouteInfo paramRouteInfo) {
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      if (callbackInfo.filterRouteEvent(paramRouteInfo))
        callbackInfo.cb.onRouteRemoved(callbackInfo.router, paramRouteInfo); 
    } 
  }
  
  static void dispatchRouteGrouped(RouteInfo paramRouteInfo, RouteGroup paramRouteGroup, int paramInt) {
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      if (callbackInfo.filterRouteEvent(paramRouteGroup))
        callbackInfo.cb.onRouteGrouped(callbackInfo.router, paramRouteInfo, paramRouteGroup, paramInt); 
    } 
  }
  
  static void dispatchRouteUngrouped(RouteInfo paramRouteInfo, RouteGroup paramRouteGroup) {
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      if (callbackInfo.filterRouteEvent(paramRouteGroup))
        callbackInfo.cb.onRouteUngrouped(callbackInfo.router, paramRouteInfo, paramRouteGroup); 
    } 
  }
  
  static void dispatchRouteVolumeChanged(RouteInfo paramRouteInfo) {
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      if (callbackInfo.filterRouteEvent(paramRouteInfo))
        callbackInfo.cb.onRouteVolumeChanged(callbackInfo.router, paramRouteInfo); 
    } 
  }
  
  static void dispatchRoutePresentationDisplayChanged(RouteInfo paramRouteInfo) {
    for (CallbackInfo callbackInfo : sStatic.mCallbacks) {
      if (callbackInfo.filterRouteEvent(paramRouteInfo))
        callbackInfo.cb.onRoutePresentationDisplayChanged(callbackInfo.router, paramRouteInfo); 
    } 
  }
  
  static void systemVolumeChanged(int paramInt) {
    RouteInfo routeInfo = sStatic.mSelectedRoute;
    if (routeInfo == null)
      return; 
    if (routeInfo == sStatic.mBluetoothA2dpRoute || routeInfo == sStatic.mDefaultAudioVideo) {
      dispatchRouteVolumeChanged(routeInfo);
      return;
    } 
    if (sStatic.mBluetoothA2dpRoute != null) {
      try {
        if (sStatic.mAudioService.isBluetoothA2dpOn()) {
          routeInfo = sStatic.mBluetoothA2dpRoute;
        } else {
          routeInfo = sStatic.mDefaultAudioVideo;
        } 
        dispatchRouteVolumeChanged(routeInfo);
      } catch (RemoteException remoteException) {
        Log.e("MediaRouter", "Error checking Bluetooth A2DP state to report volume change", (Throwable)remoteException);
      } 
    } else {
      dispatchRouteVolumeChanged(sStatic.mDefaultAudioVideo);
    } 
  }
  
  static void updateWifiDisplayStatus(WifiDisplayStatus paramWifiDisplayStatus) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getFeatureState : ()I
    //   4: iconst_3
    //   5: if_icmpne -> 56
    //   8: aload_0
    //   9: invokevirtual getDisplays : ()[Landroid/hardware/display/WifiDisplay;
    //   12: astore_1
    //   13: aload_0
    //   14: invokevirtual getActiveDisplay : ()Landroid/hardware/display/WifiDisplay;
    //   17: astore_2
    //   18: aload_2
    //   19: astore_3
    //   20: getstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   23: getfield mCanConfigureWifiDisplays : Z
    //   26: ifne -> 62
    //   29: aload_2
    //   30: ifnull -> 47
    //   33: iconst_1
    //   34: anewarray android/hardware/display/WifiDisplay
    //   37: dup
    //   38: iconst_0
    //   39: aload_2
    //   40: aastore
    //   41: astore_1
    //   42: aload_2
    //   43: astore_3
    //   44: goto -> 62
    //   47: getstatic android/hardware/display/WifiDisplay.EMPTY_ARRAY : [Landroid/hardware/display/WifiDisplay;
    //   50: astore_1
    //   51: aload_2
    //   52: astore_3
    //   53: goto -> 62
    //   56: getstatic android/hardware/display/WifiDisplay.EMPTY_ARRAY : [Landroid/hardware/display/WifiDisplay;
    //   59: astore_1
    //   60: aconst_null
    //   61: astore_3
    //   62: aload_3
    //   63: ifnull -> 74
    //   66: aload_3
    //   67: invokevirtual getDeviceAddress : ()Ljava/lang/String;
    //   70: astore_2
    //   71: goto -> 76
    //   74: aconst_null
    //   75: astore_2
    //   76: iconst_0
    //   77: istore #4
    //   79: iload #4
    //   81: aload_1
    //   82: arraylength
    //   83: if_icmpge -> 208
    //   86: aload_1
    //   87: iload #4
    //   89: aaload
    //   90: astore #5
    //   92: aload #5
    //   94: aload_3
    //   95: invokestatic shouldShowWifiDisplay : (Landroid/hardware/display/WifiDisplay;Landroid/hardware/display/WifiDisplay;)Z
    //   98: ifeq -> 202
    //   101: aload #5
    //   103: invokestatic findWifiDisplayRoute : (Landroid/hardware/display/WifiDisplay;)Landroid/media/MediaRouter$RouteInfo;
    //   106: astore #6
    //   108: aload #6
    //   110: ifnonnull -> 129
    //   113: aload #5
    //   115: aload_0
    //   116: invokestatic makeWifiDisplayRoute : (Landroid/hardware/display/WifiDisplay;Landroid/hardware/display/WifiDisplayStatus;)Landroid/media/MediaRouter$RouteInfo;
    //   119: astore #6
    //   121: aload #6
    //   123: invokestatic addRouteStatic : (Landroid/media/MediaRouter$RouteInfo;)V
    //   126: goto -> 182
    //   129: aload #5
    //   131: invokevirtual getDeviceAddress : ()Ljava/lang/String;
    //   134: astore #7
    //   136: aload #7
    //   138: aload_2
    //   139: invokevirtual equals : (Ljava/lang/Object;)Z
    //   142: ifne -> 169
    //   145: getstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   148: getfield mPreviousActiveWifiDisplayAddress : Ljava/lang/String;
    //   151: astore #8
    //   153: aload #7
    //   155: aload #8
    //   157: invokevirtual equals : (Ljava/lang/Object;)Z
    //   160: ifeq -> 169
    //   163: iconst_1
    //   164: istore #9
    //   166: goto -> 172
    //   169: iconst_0
    //   170: istore #9
    //   172: aload #6
    //   174: aload #5
    //   176: aload_0
    //   177: iload #9
    //   179: invokestatic updateWifiDisplayRoute : (Landroid/media/MediaRouter$RouteInfo;Landroid/hardware/display/WifiDisplay;Landroid/hardware/display/WifiDisplayStatus;Z)V
    //   182: aload #5
    //   184: aload_3
    //   185: invokevirtual equals : (Landroid/hardware/display/WifiDisplay;)Z
    //   188: ifeq -> 202
    //   191: aload #6
    //   193: invokevirtual getSupportedTypes : ()I
    //   196: aload #6
    //   198: iconst_0
    //   199: invokestatic selectRouteStatic : (ILandroid/media/MediaRouter$RouteInfo;Z)V
    //   202: iinc #4, 1
    //   205: goto -> 79
    //   208: getstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   211: getfield mRoutes : Ljava/util/ArrayList;
    //   214: invokevirtual size : ()I
    //   217: istore #4
    //   219: iload #4
    //   221: iconst_1
    //   222: isub
    //   223: istore #10
    //   225: iload #4
    //   227: ifle -> 288
    //   230: getstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   233: getfield mRoutes : Ljava/util/ArrayList;
    //   236: iload #10
    //   238: invokevirtual get : (I)Ljava/lang/Object;
    //   241: checkcast android/media/MediaRouter$RouteInfo
    //   244: astore #6
    //   246: aload #6
    //   248: getfield mDeviceAddress : Ljava/lang/String;
    //   251: ifnull -> 281
    //   254: aload_1
    //   255: aload #6
    //   257: getfield mDeviceAddress : Ljava/lang/String;
    //   260: invokestatic findWifiDisplay : ([Landroid/hardware/display/WifiDisplay;Ljava/lang/String;)Landroid/hardware/display/WifiDisplay;
    //   263: astore_0
    //   264: aload_0
    //   265: ifnull -> 276
    //   268: aload_0
    //   269: aload_3
    //   270: invokestatic shouldShowWifiDisplay : (Landroid/hardware/display/WifiDisplay;Landroid/hardware/display/WifiDisplay;)Z
    //   273: ifne -> 281
    //   276: aload #6
    //   278: invokestatic removeRouteStatic : (Landroid/media/MediaRouter$RouteInfo;)V
    //   281: iload #10
    //   283: istore #4
    //   285: goto -> 219
    //   288: getstatic android/media/MediaRouter.sStatic : Landroid/media/MediaRouter$Static;
    //   291: aload_2
    //   292: putfield mPreviousActiveWifiDisplayAddress : Ljava/lang/String;
    //   295: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1443	-> 0
    //   #1444	-> 8
    //   #1445	-> 13
    //   #1451	-> 18
    //   #1452	-> 29
    //   #1453	-> 33
    //   #1455	-> 47
    //   #1459	-> 56
    //   #1460	-> 60
    //   #1462	-> 62
    //   #1463	-> 66
    //   #1466	-> 76
    //   #1467	-> 86
    //   #1468	-> 92
    //   #1469	-> 101
    //   #1470	-> 108
    //   #1471	-> 113
    //   #1472	-> 121
    //   #1474	-> 129
    //   #1475	-> 136
    //   #1476	-> 153
    //   #1477	-> 172
    //   #1479	-> 182
    //   #1480	-> 191
    //   #1466	-> 202
    //   #1486	-> 208
    //   #1487	-> 230
    //   #1488	-> 246
    //   #1489	-> 254
    //   #1490	-> 264
    //   #1491	-> 276
    //   #1494	-> 281
    //   #1498	-> 288
    //   #1499	-> 295
  }
  
  private static boolean shouldShowWifiDisplay(WifiDisplay paramWifiDisplay1, WifiDisplay paramWifiDisplay2) {
    return (paramWifiDisplay1.isRemembered() || paramWifiDisplay1.equals(paramWifiDisplay2));
  }
  
  static int getWifiDisplayStatusCode(WifiDisplay paramWifiDisplay, WifiDisplayStatus paramWifiDisplayStatus) {
    byte b;
    if (paramWifiDisplayStatus.getScanState() == 1) {
      b = 1;
    } else if (paramWifiDisplay.isAvailable()) {
      if (paramWifiDisplay.canConnect()) {
        b = 3;
      } else {
        b = 5;
      } 
    } else {
      b = 4;
    } 
    int i = b;
    if (paramWifiDisplay.equals(paramWifiDisplayStatus.getActiveDisplay())) {
      i = paramWifiDisplayStatus.getActiveDisplayState();
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            i = b;
          } else {
            i = 6;
          } 
        } else {
          i = 2;
        } 
      } else {
        Log.e("MediaRouter", "Active display is not connected!");
        i = b;
      } 
    } 
    return i;
  }
  
  static boolean isWifiDisplayEnabled(WifiDisplay paramWifiDisplay, WifiDisplayStatus paramWifiDisplayStatus) {
    boolean bool;
    if (paramWifiDisplay.isAvailable() && (paramWifiDisplay.canConnect() || paramWifiDisplay.equals(paramWifiDisplayStatus.getActiveDisplay()))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  static RouteInfo makeWifiDisplayRoute(WifiDisplay paramWifiDisplay, WifiDisplayStatus paramWifiDisplayStatus) {
    RouteInfo routeInfo = new RouteInfo(sStatic.mSystemCategory);
    routeInfo.mDeviceAddress = paramWifiDisplay.getDeviceAddress();
    routeInfo.mSupportedTypes = 7;
    routeInfo.mVolumeHandling = 0;
    routeInfo.mPlaybackType = 1;
    routeInfo.setRealStatusCode(getWifiDisplayStatusCode(paramWifiDisplay, paramWifiDisplayStatus));
    routeInfo.mEnabled = isWifiDisplayEnabled(paramWifiDisplay, paramWifiDisplayStatus);
    routeInfo.mName = paramWifiDisplay.getFriendlyDisplayName();
    routeInfo.mDescription = sStatic.mResources.getText(17041585);
    routeInfo.updatePresentationDisplay();
    routeInfo.mDeviceType = 1;
    return routeInfo;
  }
  
  private static void updateWifiDisplayRoute(RouteInfo paramRouteInfo, WifiDisplay paramWifiDisplay, WifiDisplayStatus paramWifiDisplayStatus, boolean paramBoolean) {
    boolean bool3, bool1 = false;
    String str = paramWifiDisplay.getFriendlyDisplayName();
    if (!paramRouteInfo.getName().equals(str)) {
      paramRouteInfo.mName = str;
      bool1 = true;
    } 
    boolean bool2 = isWifiDisplayEnabled(paramWifiDisplay, paramWifiDisplayStatus);
    if (paramRouteInfo.mEnabled != bool2) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    paramRouteInfo.mEnabled = bool2;
    boolean bool4 = paramRouteInfo.setRealStatusCode(getWifiDisplayStatusCode(paramWifiDisplay, paramWifiDisplayStatus));
    if ((bool1 | bool3 | bool4) != 0)
      dispatchRouteChanged(paramRouteInfo); 
    if ((!bool2 || paramBoolean) && paramRouteInfo.isSelected())
      selectDefaultRouteStatic(); 
  }
  
  private static WifiDisplay findWifiDisplay(WifiDisplay[] paramArrayOfWifiDisplay, String paramString) {
    for (byte b = 0; b < paramArrayOfWifiDisplay.length; b++) {
      WifiDisplay wifiDisplay = paramArrayOfWifiDisplay[b];
      if (wifiDisplay.getDeviceAddress().equals(paramString))
        return wifiDisplay; 
    } 
    return null;
  }
  
  private static RouteInfo findWifiDisplayRoute(WifiDisplay paramWifiDisplay) {
    int i = sStatic.mRoutes.size();
    for (byte b = 0; b < i; b++) {
      RouteInfo routeInfo = sStatic.mRoutes.get(b);
      if (paramWifiDisplay.getDeviceAddress().equals(routeInfo.mDeviceAddress))
        return routeInfo; 
    } 
    return null;
  }
  
  public static class RouteInfo {
    int mPlaybackType = 0;
    
    int mVolumeMax = 15;
    
    int mVolume = 15;
    
    int mVolumeHandling = 1;
    
    int mPlaybackStream = 3;
    
    int mPresentationDisplayId = -1;
    
    boolean mEnabled = true;
    
    public static final int DEVICE_TYPE_BLUETOOTH = 3;
    
    public static final int DEVICE_TYPE_SPEAKER = 2;
    
    public static final int DEVICE_TYPE_TV = 1;
    
    public static final int DEVICE_TYPE_UNKNOWN = 0;
    
    public static final int PLAYBACK_TYPE_LOCAL = 0;
    
    public static final int PLAYBACK_TYPE_REMOTE = 1;
    
    public static final int PLAYBACK_VOLUME_FIXED = 0;
    
    public static final int PLAYBACK_VOLUME_VARIABLE = 1;
    
    public static final int STATUS_AVAILABLE = 3;
    
    public static final int STATUS_CONNECTED = 6;
    
    public static final int STATUS_CONNECTING = 2;
    
    public static final int STATUS_IN_USE = 5;
    
    public static final int STATUS_NONE = 0;
    
    public static final int STATUS_NOT_AVAILABLE = 4;
    
    public static final int STATUS_SCANNING = 1;
    
    final MediaRouter.RouteCategory mCategory;
    
    CharSequence mDescription;
    
    String mDeviceAddress;
    
    int mDeviceType;
    
    String mGlobalRouteId;
    
    MediaRouter.RouteGroup mGroup;
    
    Drawable mIcon;
    
    CharSequence mName;
    
    int mNameResId;
    
    Display mPresentationDisplay;
    
    private int mRealStatusCode;
    
    final IRemoteVolumeObserver.Stub mRemoteVolObserver;
    
    private int mResolvedStatusCode;
    
    private CharSequence mStatus;
    
    int mSupportedTypes;
    
    private Object mTag;
    
    MediaRouter.VolumeCallbackInfo mVcb;
    
    public CharSequence getName() {
      return getName(MediaRouter.sStatic.mResources);
    }
    
    public CharSequence getName(Context param1Context) {
      return getName(param1Context.getResources());
    }
    
    CharSequence getName(Resources param1Resources) {
      int i = this.mNameResId;
      if (i != 0)
        return param1Resources.getText(i); 
      return this.mName;
    }
    
    public CharSequence getDescription() {
      return this.mDescription;
    }
    
    public CharSequence getStatus() {
      return this.mStatus;
    }
    
    boolean setRealStatusCode(int param1Int) {
      if (this.mRealStatusCode != param1Int) {
        this.mRealStatusCode = param1Int;
        return resolveStatusCode();
      } 
      return false;
    }
    
    boolean resolveStatusCode() {
      CharSequence charSequence;
      int i = this.mRealStatusCode;
      int j = i;
      if (isSelected())
        if (i != 1 && i != 3) {
          j = i;
        } else {
          j = 2;
        }  
      if (this.mResolvedStatusCode == j)
        return false; 
      this.mResolvedStatusCode = j;
      if (j != 1) {
        if (j != 2) {
          if (j != 3) {
            if (j != 4) {
              if (j != 5) {
                j = 0;
              } else {
                j = 17040538;
              } 
            } else {
              j = 17040539;
            } 
          } else {
            j = 17040536;
          } 
        } else {
          j = 17040537;
        } 
      } else {
        j = 17040540;
      } 
      if (j != 0) {
        charSequence = MediaRouter.sStatic.mResources.getText(j);
      } else {
        charSequence = null;
      } 
      this.mStatus = charSequence;
      return true;
    }
    
    public int getStatusCode() {
      return this.mResolvedStatusCode;
    }
    
    public int getSupportedTypes() {
      return this.mSupportedTypes;
    }
    
    public int getDeviceType() {
      return this.mDeviceType;
    }
    
    public boolean matchesTypes(int param1Int) {
      boolean bool;
      if ((this.mSupportedTypes & param1Int) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public MediaRouter.RouteGroup getGroup() {
      return this.mGroup;
    }
    
    public MediaRouter.RouteCategory getCategory() {
      return this.mCategory;
    }
    
    public Drawable getIconDrawable() {
      return this.mIcon;
    }
    
    public void setTag(Object param1Object) {
      this.mTag = param1Object;
      routeUpdated();
    }
    
    public Object getTag() {
      return this.mTag;
    }
    
    public int getPlaybackType() {
      return this.mPlaybackType;
    }
    
    public int getPlaybackStream() {
      return this.mPlaybackStream;
    }
    
    public int getVolume() {
      if (this.mPlaybackType == 0) {
        int i = 0;
        try {
          int j = MediaRouter.sStatic.mAudioService.getStreamVolume(this.mPlaybackStream);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Error getting local stream volume", (Throwable)remoteException);
        } 
        return i;
      } 
      return this.mVolume;
    }
    
    public void requestSetVolume(int param1Int) {
      if (this.mPlaybackType == 0) {
        try {
          IAudioService iAudioService = MediaRouter.sStatic.mAudioService;
          int i = this.mPlaybackStream;
          String str = ActivityThread.currentPackageName();
          iAudioService.setStreamVolume(i, param1Int, 0, str);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Error setting local stream volume", (Throwable)remoteException);
        } 
      } else {
        MediaRouter.sStatic.requestSetVolume(this, param1Int);
      } 
    }
    
    public void requestUpdateVolume(int param1Int) {
      if (this.mPlaybackType == 0) {
        try {
          param1Int = Math.max(0, Math.min(getVolume() + param1Int, getVolumeMax()));
          IAudioService iAudioService = MediaRouter.sStatic.mAudioService;
          int i = this.mPlaybackStream;
          String str = ActivityThread.currentPackageName();
          iAudioService.setStreamVolume(i, param1Int, 0, str);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Error setting local stream volume", (Throwable)remoteException);
        } 
      } else {
        MediaRouter.sStatic.requestUpdateVolume(this, param1Int);
      } 
    }
    
    public int getVolumeMax() {
      if (this.mPlaybackType == 0) {
        int i = 0;
        try {
          int j = MediaRouter.sStatic.mAudioService.getStreamMaxVolume(this.mPlaybackStream);
        } catch (RemoteException remoteException) {
          Log.e("MediaRouter", "Error getting local stream volume", (Throwable)remoteException);
        } 
        return i;
      } 
      return this.mVolumeMax;
    }
    
    public int getVolumeHandling() {
      return this.mVolumeHandling;
    }
    
    public Display getPresentationDisplay() {
      return this.mPresentationDisplay;
    }
    
    boolean updatePresentationDisplay() {
      Display display = choosePresentationDisplay();
      if (this.mPresentationDisplay != display) {
        this.mPresentationDisplay = display;
        return true;
      } 
      return false;
    }
    
    private Display choosePresentationDisplay() {
      if ((this.mSupportedTypes & 0x2) != 0) {
        Display[] arrayOfDisplay = MediaRouter.sStatic.getAllPresentationDisplays();
        int i = this.mPresentationDisplayId, j = 0, k = 0;
        if (i >= 0) {
          for (i = arrayOfDisplay.length, j = k; j < i; ) {
            Display display = arrayOfDisplay[j];
            if (display.getDisplayId() == this.mPresentationDisplayId)
              return display; 
            j++;
          } 
          return null;
        } 
        if (this.mDeviceAddress != null) {
          for (k = arrayOfDisplay.length; j < k; ) {
            Display display = arrayOfDisplay[j];
            if (display.getType() == 3) {
              String str = this.mDeviceAddress;
              if (str.equals(display.getAddress()))
                return display; 
            } 
            j++;
          } 
          return null;
        } 
        if (this == MediaRouter.sStatic.mDefaultAudioVideo && arrayOfDisplay.length > 0)
          return arrayOfDisplay[0]; 
      } 
      return null;
    }
    
    public String getDeviceAddress() {
      return this.mDeviceAddress;
    }
    
    public boolean isEnabled() {
      return this.mEnabled;
    }
    
    public boolean isConnecting() {
      boolean bool;
      if (this.mResolvedStatusCode == 2) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isSelected() {
      boolean bool;
      if (this == MediaRouter.sStatic.mSelectedRoute) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isDefault() {
      boolean bool;
      if (this == MediaRouter.sStatic.mDefaultAudioVideo) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isBluetooth() {
      boolean bool;
      if (this == MediaRouter.sStatic.mBluetoothA2dpRoute) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void select() {
      MediaRouter.selectRouteStatic(this.mSupportedTypes, this, true);
    }
    
    void setStatusInt(CharSequence param1CharSequence) {
      if (!param1CharSequence.equals(this.mStatus)) {
        this.mStatus = param1CharSequence;
        MediaRouter.RouteGroup routeGroup = this.mGroup;
        if (routeGroup != null)
          routeGroup.memberStatusChanged(this, param1CharSequence); 
        routeUpdated();
      } 
    }
    
    RouteInfo(MediaRouter.RouteCategory param1RouteCategory) {
      this.mRemoteVolObserver = (IRemoteVolumeObserver.Stub)new Object(this);
      this.mCategory = param1RouteCategory;
      this.mDeviceType = 0;
    }
    
    void routeUpdated() {
      MediaRouter.updateRoute(this);
    }
    
    public String toString() {
      null = MediaRouter.typesToString(getSupportedTypes());
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getClass().getSimpleName());
      stringBuilder.append("{ name=");
      stringBuilder.append(getName());
      stringBuilder.append(", description=");
      stringBuilder.append(getDescription());
      stringBuilder.append(", status=");
      stringBuilder.append(getStatus());
      stringBuilder.append(", category=");
      stringBuilder.append(getCategory());
      stringBuilder.append(", supportedTypes=");
      stringBuilder.append(null);
      stringBuilder.append(", presentationDisplay=");
      stringBuilder.append(this.mPresentationDisplay);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface DeviceType {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface PlaybackType {}
    
    @Retention(RetentionPolicy.SOURCE)
    private static @interface PlaybackVolume {}
  }
  
  class UserRouteInfo extends RouteInfo {
    RemoteControlClient mRcc;
    
    SessionVolumeProvider mSvp;
    
    UserRouteInfo(MediaRouter this$0) {
      super((MediaRouter.RouteCategory)this$0);
      this.mSupportedTypes = 8388608;
      this.mPlaybackType = 1;
      this.mVolumeHandling = 0;
    }
    
    public void setName(CharSequence param1CharSequence) {
      this.mNameResId = 0;
      this.mName = param1CharSequence;
      routeUpdated();
    }
    
    public void setName(int param1Int) {
      this.mNameResId = param1Int;
      this.mName = null;
      routeUpdated();
    }
    
    public void setDescription(CharSequence param1CharSequence) {
      this.mDescription = param1CharSequence;
      routeUpdated();
    }
    
    public void setStatus(CharSequence param1CharSequence) {
      setStatusInt(param1CharSequence);
    }
    
    public void setRemoteControlClient(RemoteControlClient param1RemoteControlClient) {
      this.mRcc = param1RemoteControlClient;
      updatePlaybackInfoOnRcc();
    }
    
    public RemoteControlClient getRemoteControlClient() {
      return this.mRcc;
    }
    
    public void setIconDrawable(Drawable param1Drawable) {
      this.mIcon = param1Drawable;
    }
    
    public void setIconResource(int param1Int) {
      setIconDrawable(MediaRouter.sStatic.mResources.getDrawable(param1Int));
    }
    
    public void setVolumeCallback(MediaRouter.VolumeCallback param1VolumeCallback) {
      this.mVcb = new MediaRouter.VolumeCallbackInfo(param1VolumeCallback, this);
    }
    
    public void setPlaybackType(int param1Int) {
      if (this.mPlaybackType != param1Int) {
        this.mPlaybackType = param1Int;
        configureSessionVolume();
      } 
    }
    
    public void setVolumeHandling(int param1Int) {
      if (this.mVolumeHandling != param1Int) {
        this.mVolumeHandling = param1Int;
        configureSessionVolume();
      } 
    }
    
    public void setVolume(int param1Int) {
      param1Int = Math.max(0, Math.min(param1Int, getVolumeMax()));
      if (this.mVolume != param1Int) {
        this.mVolume = param1Int;
        SessionVolumeProvider sessionVolumeProvider = this.mSvp;
        if (sessionVolumeProvider != null)
          sessionVolumeProvider.setCurrentVolume(this.mVolume); 
        MediaRouter.dispatchRouteVolumeChanged(this);
        if (this.mGroup != null)
          this.mGroup.memberVolumeChanged(this); 
      } 
    }
    
    public void requestSetVolume(int param1Int) {
      if (this.mVolumeHandling == 1) {
        if (this.mVcb == null) {
          Log.e("MediaRouter", "Cannot requestSetVolume on user route - no volume callback set");
          return;
        } 
        this.mVcb.vcb.onVolumeSetRequest(this, param1Int);
      } 
    }
    
    public void requestUpdateVolume(int param1Int) {
      if (this.mVolumeHandling == 1) {
        if (this.mVcb == null) {
          Log.e("MediaRouter", "Cannot requestChangeVolume on user route - no volumec callback set");
          return;
        } 
        this.mVcb.vcb.onVolumeUpdateRequest(this, param1Int);
      } 
    }
    
    public void setVolumeMax(int param1Int) {
      if (this.mVolumeMax != param1Int) {
        this.mVolumeMax = param1Int;
        configureSessionVolume();
      } 
    }
    
    public void setPlaybackStream(int param1Int) {
      if (this.mPlaybackStream != param1Int) {
        this.mPlaybackStream = param1Int;
        configureSessionVolume();
      } 
    }
    
    private void updatePlaybackInfoOnRcc() {
      configureSessionVolume();
    }
    
    private void configureSessionVolume() {
      StringBuilder stringBuilder;
      byte b;
      RemoteControlClient remoteControlClient = this.mRcc;
      if (remoteControlClient == null) {
        if (MediaRouter.DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("No Rcc to configure volume for route ");
          stringBuilder.append(getName());
          Log.d("MediaRouter", stringBuilder.toString());
        } 
        return;
      } 
      MediaSession mediaSession = stringBuilder.getMediaSession();
      if (mediaSession == null) {
        if (MediaRouter.DEBUG)
          Log.d("MediaRouter", "Rcc has no session to configure volume"); 
        return;
      } 
      if (this.mPlaybackType == 1) {
        b = 0;
        if (this.mVolumeHandling == 1)
          b = 2; 
        SessionVolumeProvider sessionVolumeProvider1 = this.mSvp;
        if (sessionVolumeProvider1 != null && sessionVolumeProvider1.getVolumeControl() == b) {
          sessionVolumeProvider1 = this.mSvp;
          if (sessionVolumeProvider1.getMaxVolume() != this.mVolumeMax) {
            this.mSvp = sessionVolumeProvider1 = new SessionVolumeProvider(b, this.mVolumeMax, this.mVolume);
            mediaSession.setPlaybackToRemote(sessionVolumeProvider1);
            return;
          } 
          return;
        } 
      } else {
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        builder.setLegacyStreamType(this.mPlaybackStream);
        mediaSession.setPlaybackToLocal(builder.build());
        this.mSvp = null;
        return;
      } 
      SessionVolumeProvider sessionVolumeProvider = new SessionVolumeProvider(b, this.mVolumeMax, this.mVolume);
      mediaSession.setPlaybackToRemote(sessionVolumeProvider);
    }
    
    class SessionVolumeProvider extends VolumeProvider {
      final MediaRouter.UserRouteInfo this$0;
      
      SessionVolumeProvider(int param2Int1, int param2Int2, int param2Int3) {
        super(param2Int1, param2Int2, param2Int3);
      }
      
      public void onSetVolumeTo(int param2Int) {
        MediaRouter.sStatic.mHandler.post((Runnable)new Object(this, param2Int));
      }
      
      public void onAdjustVolume(int param2Int) {
        MediaRouter.sStatic.mHandler.post((Runnable)new Object(this, param2Int));
      }
    }
  }
  
  class RouteGroup extends RouteInfo {
    final ArrayList<MediaRouter.RouteInfo> mRoutes = new ArrayList<>();
    
    private boolean mUpdateName;
    
    RouteGroup(MediaRouter this$0) {
      super((MediaRouter.RouteCategory)this$0);
      this.mGroup = this;
      this.mVolumeHandling = 0;
    }
    
    CharSequence getName(Resources param1Resources) {
      if (this.mUpdateName)
        updateName(); 
      return super.getName(param1Resources);
    }
    
    public void addRoute(MediaRouter.RouteInfo param1RouteInfo) {
      if (param1RouteInfo.getGroup() == null) {
        if (param1RouteInfo.getCategory() == this.mCategory) {
          int i = this.mRoutes.size();
          this.mRoutes.add(param1RouteInfo);
          param1RouteInfo.mGroup = this;
          this.mUpdateName = true;
          updateVolume();
          routeUpdated();
          MediaRouter.dispatchRouteGrouped(param1RouteInfo, this, i);
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Route cannot be added to a group with a different category. (Route category=");
        stringBuilder1.append(param1RouteInfo.getCategory());
        stringBuilder1.append(" group category=");
        stringBuilder1.append(this.mCategory);
        stringBuilder1.append(")");
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Route ");
      stringBuilder.append(param1RouteInfo);
      stringBuilder.append(" is already part of a group.");
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public void addRoute(MediaRouter.RouteInfo param1RouteInfo, int param1Int) {
      if (param1RouteInfo.getGroup() == null) {
        if (param1RouteInfo.getCategory() == this.mCategory) {
          this.mRoutes.add(param1Int, param1RouteInfo);
          param1RouteInfo.mGroup = this;
          this.mUpdateName = true;
          updateVolume();
          routeUpdated();
          MediaRouter.dispatchRouteGrouped(param1RouteInfo, this, param1Int);
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Route cannot be added to a group with a different category. (Route category=");
        stringBuilder1.append(param1RouteInfo.getCategory());
        stringBuilder1.append(" group category=");
        stringBuilder1.append(this.mCategory);
        stringBuilder1.append(")");
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Route ");
      stringBuilder.append(param1RouteInfo);
      stringBuilder.append(" is already part of a group.");
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public void removeRoute(MediaRouter.RouteInfo param1RouteInfo) {
      if (param1RouteInfo.getGroup() == this) {
        this.mRoutes.remove(param1RouteInfo);
        param1RouteInfo.mGroup = null;
        this.mUpdateName = true;
        updateVolume();
        MediaRouter.dispatchRouteUngrouped(param1RouteInfo, this);
        routeUpdated();
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Route ");
      stringBuilder.append(param1RouteInfo);
      stringBuilder.append(" is not a member of this group.");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void removeRoute(int param1Int) {
      MediaRouter.RouteInfo routeInfo = this.mRoutes.remove(param1Int);
      routeInfo.mGroup = null;
      this.mUpdateName = true;
      updateVolume();
      MediaRouter.dispatchRouteUngrouped(routeInfo, this);
      routeUpdated();
    }
    
    public int getRouteCount() {
      return this.mRoutes.size();
    }
    
    public MediaRouter.RouteInfo getRouteAt(int param1Int) {
      return this.mRoutes.get(param1Int);
    }
    
    public void setIconDrawable(Drawable param1Drawable) {
      this.mIcon = param1Drawable;
    }
    
    public void setIconResource(int param1Int) {
      setIconDrawable(MediaRouter.sStatic.mResources.getDrawable(param1Int));
    }
    
    public void requestSetVolume(int param1Int) {
      int i = getVolumeMax();
      if (i == 0)
        return; 
      float f = param1Int / i;
      int j = getRouteCount();
      for (i = 0; i < j; i++) {
        MediaRouter.RouteInfo routeInfo = getRouteAt(i);
        int k = (int)(routeInfo.getVolumeMax() * f);
        routeInfo.requestSetVolume(k);
      } 
      if (param1Int != this.mVolume) {
        this.mVolume = param1Int;
        MediaRouter.dispatchRouteVolumeChanged(this);
      } 
    }
    
    public void requestUpdateVolume(int param1Int) {
      int i = getVolumeMax();
      if (i == 0)
        return; 
      int j = getRouteCount();
      int k = 0;
      for (i = 0; i < j; i++, k = n) {
        MediaRouter.RouteInfo routeInfo = getRouteAt(i);
        routeInfo.requestUpdateVolume(param1Int);
        int m = routeInfo.getVolume();
        int n = k;
        if (m > k)
          n = m; 
      } 
      if (k != this.mVolume) {
        this.mVolume = k;
        MediaRouter.dispatchRouteVolumeChanged(this);
      } 
    }
    
    void memberNameChanged(MediaRouter.RouteInfo param1RouteInfo, CharSequence param1CharSequence) {
      this.mUpdateName = true;
      routeUpdated();
    }
    
    void memberStatusChanged(MediaRouter.RouteInfo param1RouteInfo, CharSequence param1CharSequence) {
      setStatusInt(param1CharSequence);
    }
    
    void memberVolumeChanged(MediaRouter.RouteInfo param1RouteInfo) {
      updateVolume();
    }
    
    void updateVolume() {
      int i = getRouteCount();
      int j = 0;
      for (byte b = 0; b < i; b++, j = m) {
        int k = getRouteAt(b).getVolume();
        int m = j;
        if (k > j)
          m = k; 
      } 
      if (j != this.mVolume) {
        this.mVolume = j;
        MediaRouter.dispatchRouteVolumeChanged(this);
      } 
    }
    
    void routeUpdated() {
      Drawable drawable;
      int i = 0;
      int j = this.mRoutes.size();
      if (j == 0) {
        MediaRouter.removeRouteStatic(this);
        return;
      } 
      int k = 0;
      int m = 1;
      int n = 1;
      byte b = 0;
      while (true) {
        byte b1 = 0;
        if (b < j) {
          drawable = (Drawable)this.mRoutes.get(b);
          i |= ((MediaRouter.RouteInfo)drawable).mSupportedTypes;
          int i1 = drawable.getVolumeMax();
          int i2 = k;
          if (i1 > k)
            i2 = i1; 
          if (drawable.getPlaybackType() == 0) {
            k = 1;
          } else {
            k = 0;
          } 
          i1 = m & k;
          m = b1;
          if (drawable.getVolumeHandling() == 0)
            m = 1; 
          n &= m;
          b++;
          k = i2;
          m = i1;
          continue;
        } 
        break;
      } 
      this.mPlaybackType = m ^ 0x1;
      this.mVolumeHandling = n ^ 0x1;
      this.mSupportedTypes = i;
      this.mVolumeMax = k;
      if (j == 1) {
        drawable = ((MediaRouter.RouteInfo)this.mRoutes.get(0)).getIconDrawable();
      } else {
        drawable = null;
      } 
      this.mIcon = drawable;
      super.routeUpdated();
    }
    
    void updateName() {
      StringBuilder stringBuilder = new StringBuilder();
      int i = this.mRoutes.size();
      for (byte b = 0; b < i; b++) {
        MediaRouter.RouteInfo routeInfo = this.mRoutes.get(b);
        if (b > 0)
          stringBuilder.append(", "); 
        stringBuilder.append(routeInfo.getName());
      } 
      this.mName = stringBuilder.toString();
      this.mUpdateName = false;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(super.toString());
      stringBuilder.append('[');
      int i = this.mRoutes.size();
      for (byte b = 0; b < i; b++) {
        if (b > 0)
          stringBuilder.append(", "); 
        stringBuilder.append(this.mRoutes.get(b));
      } 
      stringBuilder.append(']');
      return stringBuilder.toString();
    }
  }
  
  public static class RouteCategory {
    final boolean mGroupable;
    
    boolean mIsSystem;
    
    CharSequence mName;
    
    int mNameResId;
    
    int mTypes;
    
    RouteCategory(CharSequence param1CharSequence, int param1Int, boolean param1Boolean) {
      this.mName = param1CharSequence;
      this.mTypes = param1Int;
      this.mGroupable = param1Boolean;
    }
    
    RouteCategory(int param1Int1, int param1Int2, boolean param1Boolean) {
      this.mNameResId = param1Int1;
      this.mTypes = param1Int2;
      this.mGroupable = param1Boolean;
    }
    
    public CharSequence getName() {
      return getName(MediaRouter.sStatic.mResources);
    }
    
    public CharSequence getName(Context param1Context) {
      return getName(param1Context.getResources());
    }
    
    CharSequence getName(Resources param1Resources) {
      int i = this.mNameResId;
      if (i != 0)
        return param1Resources.getText(i); 
      return this.mName;
    }
    
    public List<MediaRouter.RouteInfo> getRoutes(List<MediaRouter.RouteInfo> param1List) {
      if (param1List == null) {
        param1List = new ArrayList<>();
      } else {
        param1List.clear();
      } 
      int i = MediaRouter.getRouteCountStatic();
      for (byte b = 0; b < i; b++) {
        MediaRouter.RouteInfo routeInfo = MediaRouter.getRouteAtStatic(b);
        if (routeInfo.mCategory == this)
          param1List.add(routeInfo); 
      } 
      return param1List;
    }
    
    public int getSupportedTypes() {
      return this.mTypes;
    }
    
    public boolean isGroupable() {
      return this.mGroupable;
    }
    
    public boolean isSystem() {
      return this.mIsSystem;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RouteCategory{ name=");
      stringBuilder.append(getName());
      stringBuilder.append(" types=");
      stringBuilder.append(MediaRouter.typesToString(this.mTypes));
      stringBuilder.append(" groupable=");
      stringBuilder.append(this.mGroupable);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
  }
  
  static class CallbackInfo {
    public final MediaRouter.Callback cb;
    
    public int flags;
    
    public final MediaRouter router;
    
    public int type;
    
    public CallbackInfo(MediaRouter.Callback param1Callback, int param1Int1, int param1Int2, MediaRouter param1MediaRouter) {
      this.cb = param1Callback;
      this.type = param1Int1;
      this.flags = param1Int2;
      this.router = param1MediaRouter;
    }
    
    public boolean filterRouteEvent(MediaRouter.RouteInfo param1RouteInfo) {
      return filterRouteEvent(param1RouteInfo.mSupportedTypes);
    }
    
    public boolean filterRouteEvent(int param1Int) {
      return ((this.flags & 0x2) != 0 || (this.type & param1Int) != 0);
    }
  }
  
  public static abstract class Callback {
    public abstract void onRouteAdded(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo);
    
    public abstract void onRouteChanged(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo);
    
    public abstract void onRouteGrouped(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo, MediaRouter.RouteGroup param1RouteGroup, int param1Int);
    
    public void onRoutePresentationDisplayChanged(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {}
    
    public abstract void onRouteRemoved(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo);
    
    public abstract void onRouteSelected(MediaRouter param1MediaRouter, int param1Int, MediaRouter.RouteInfo param1RouteInfo);
    
    public abstract void onRouteUngrouped(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo, MediaRouter.RouteGroup param1RouteGroup);
    
    public abstract void onRouteUnselected(MediaRouter param1MediaRouter, int param1Int, MediaRouter.RouteInfo param1RouteInfo);
    
    public abstract void onRouteVolumeChanged(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo);
  }
  
  class SimpleCallback extends Callback {
    public void onRouteSelected(MediaRouter param1MediaRouter, int param1Int, MediaRouter.RouteInfo param1RouteInfo) {}
    
    public void onRouteUnselected(MediaRouter param1MediaRouter, int param1Int, MediaRouter.RouteInfo param1RouteInfo) {}
    
    public void onRouteAdded(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {}
    
    public void onRouteRemoved(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {}
    
    public void onRouteChanged(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {}
    
    public void onRouteGrouped(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo, MediaRouter.RouteGroup param1RouteGroup, int param1Int) {}
    
    public void onRouteUngrouped(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo, MediaRouter.RouteGroup param1RouteGroup) {}
    
    public void onRouteVolumeChanged(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {}
  }
  
  static class VolumeCallbackInfo {
    public final MediaRouter.RouteInfo route;
    
    public final MediaRouter.VolumeCallback vcb;
    
    public VolumeCallbackInfo(MediaRouter.VolumeCallback param1VolumeCallback, MediaRouter.RouteInfo param1RouteInfo) {
      this.vcb = param1VolumeCallback;
      this.route = param1RouteInfo;
    }
  }
  
  public static abstract class VolumeCallback {
    public abstract void onVolumeSetRequest(MediaRouter.RouteInfo param1RouteInfo, int param1Int);
    
    public abstract void onVolumeUpdateRequest(MediaRouter.RouteInfo param1RouteInfo, int param1Int);
  }
  
  class VolumeChangeReceiver extends BroadcastReceiver {
    public void onReceive(Context param1Context, Intent param1Intent) {
      if (param1Intent.getAction().equals("android.media.VOLUME_CHANGED_ACTION")) {
        int i = param1Intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1);
        if (i != 3)
          return; 
        int j = param1Intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", 0);
        i = param1Intent.getIntExtra("android.media.EXTRA_PREV_VOLUME_STREAM_VALUE", 0);
        if (j != i)
          MediaRouter.systemVolumeChanged(j); 
      } 
    }
  }
  
  class WifiDisplayStatusChangedReceiver extends BroadcastReceiver {
    public void onReceive(Context param1Context, Intent param1Intent) {
      if (param1Intent.getAction().equals("android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"))
        MediaRouter.updateWifiDisplayStatus((WifiDisplayStatus)param1Intent.getParcelableExtra("android.hardware.display.extra.WIFI_DISPLAY_STATUS")); 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DeviceType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PlaybackType {}
  
  @Retention(RetentionPolicy.SOURCE)
  private static @interface PlaybackVolume {}
}
