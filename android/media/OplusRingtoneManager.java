package android.media;

import android.content.Context;
import android.net.Uri;
import android.os.SystemProperties;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

public class OplusRingtoneManager {
  public static final String CALENDAR_REMINDER_SOUND = "calendar_sound";
  
  private static final String CARRIER_CUSTOM_DEFAULT_RINGTONE_VERSION_PROP = "ro.oplus.carrier.ringtone.version";
  
  private static final String CARRIER_CUSTOM_DEFAULT_RINGTONE_VERSION_SETTINGS = "carrier_custom_default_ringtone_version";
  
  private static final String CARRIER_OTA_VERSION_BACKUP_NAME = "carrier_ota_version_backup";
  
  private static final String CARRIER_OTA_VERSION_NAME = "ro.oplus.image.my_carrier.date";
  
  private static final String COMPANY_CUSTOM_DEFAULT_RINGTONE_VERSION_PROP = "ro.oplus.company.ringtone.version";
  
  private static final String COMPANY_CUSTOM_DEFAULT_RINGTONE_VERSION_SETTINGS = "company_custom_default_ringtone_version";
  
  private static final String COMPANY_OTA_VERSION_BACKUP_NAME = "company_ota_version_backup";
  
  private static final String COMPANY_OTA_VERSION_NAME = "ro.oplus.image.my_company.date";
  
  public static final String DEFAULT_CALENDAR_REMINDER_SOUND = "calendar_default_sound";
  
  public static final String NOTIFICATION_SOUND_SIM2 = "notification_sim2";
  
  public static final String OPLUS_DEFAULT_ALARM = "oppo_default_alarm";
  
  public static final String OPLUS_DEFAULT_NOTIFICATION = "oppo_default_notification";
  
  public static final String OPLUS_DEFAULT_NOTIFICATION_SIM2 = "oppo_default_notification_sim2";
  
  public static final String OPLUS_DEFAULT_RINGTONE = "oppo_default_ringtone";
  
  public static final String OPLUS_DEFAULT_RINGTONE_SIM2 = "oppo_default_ringtone_sim2";
  
  public static final String OPLUS_DEFAULT_SMS_NOTIFICATION = "oppo_default_sms_notification_sound";
  
  public static final String OPLUS_SMS_NOTIFICATION_SOUND = "oppo_sms_notification_sound";
  
  public static final String RINGTONE_SIM2 = "ringtone_sim2";
  
  private static final String TAG = "OplusRingtoneManager";
  
  public static final int TYPE_NOTIFICATION_CALENDAR = 32;
  
  public static final int TYPE_NOTIFICATION_SIM2 = 16;
  
  public static final int TYPE_NOTIFICATION_SMS = 8;
  
  public static final int TYPE_RINGTONE_SIM2 = 64;
  
  public static void setRingtoneIfNotSet(Context paramContext, String paramString, Uri paramUri) {
    if (isNeedSet(paramContext, paramString))
      Settings.System.putString(paramContext.getContentResolver(), paramString, paramUri.toString()); 
  }
  
  private static boolean isNeedSet(Context paramContext, String paramString) {
    String str = Settings.System.getString(paramContext.getContentResolver(), paramString);
    return TextUtils.isEmpty(str);
  }
  
  public static void setRingtonesUri(Context paramContext, int paramInt, Uri paramUri) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setRingtonesUri type:");
    stringBuilder.append(paramInt);
    stringBuilder.append("ringtoneUri");
    stringBuilder.append(paramUri);
    Log.d("OplusRingtoneManager", stringBuilder.toString());
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt == 64) {
                  setRingtoneIfNotSet(paramContext, "ringtone_sim2", paramUri);
                  setRingtoneIfNotSet(paramContext, "oppo_default_ringtone_sim2", paramUri);
                } else {
                  throw new IllegalArgumentException();
                } 
              } else {
                setRingtoneIfNotSet(paramContext, "calendar_sound", paramUri);
                setRingtoneIfNotSet(paramContext, "calendar_default_sound", paramUri);
              } 
            } else {
              setRingtoneIfNotSet(paramContext, "notification_sim2", paramUri);
              setRingtoneIfNotSet(paramContext, "oppo_default_notification_sim2", paramUri);
            } 
          } else {
            setRingtoneIfNotSet(paramContext, "oppo_sms_notification_sound", paramUri);
            setRingtoneIfNotSet(paramContext, "oppo_default_sms_notification_sound", paramUri);
          } 
        } else {
          if (isNeedSet(paramContext, "alarm_alert"))
            RingtoneManager.setActualDefaultRingtoneUri(paramContext, paramInt, paramUri); 
          setRingtoneIfNotSet(paramContext, "oppo_default_alarm", paramUri);
        } 
      } else {
        if (isNeedSet(paramContext, "notification_sound"))
          RingtoneManager.setActualDefaultRingtoneUri(paramContext, paramInt, paramUri); 
        setRingtoneIfNotSet(paramContext, "oppo_default_notification", paramUri);
      } 
    } else {
      if (isNeedSet(paramContext, "ringtone"))
        RingtoneManager.setActualDefaultRingtoneUri(paramContext, paramInt, paramUri); 
      setRingtoneIfNotSet(paramContext, "oppo_default_ringtone", paramUri);
    } 
  }
  
  public static boolean isComponentVersionChange(Context paramContext) {
    String str1 = Settings.System.getString(paramContext.getContentResolver(), "company_ota_version_backup");
    String str2 = Settings.System.getString(paramContext.getContentResolver(), "carrier_ota_version_backup");
    String str3 = SystemProperties.get("ro.oplus.image.my_company.date", "");
    String str4 = SystemProperties.get("ro.oplus.image.my_carrier.date", "");
    if (isVersionStringChanged(str3, str1)) {
      Settings.System.putString(paramContext.getContentResolver(), "company_ota_version_backup", str3);
      Log.d("OplusRingtoneManager", "my_company changed");
      return true;
    } 
    if (isVersionStringChanged(str4, str2)) {
      Settings.System.putString(paramContext.getContentResolver(), "carrier_ota_version_backup", str4);
      Log.d("OplusRingtoneManager", "my_carrier changed");
      return true;
    } 
    return false;
  }
  
  public static boolean isCustomDefaultRingtoneNeeded(Context paramContext) {
    String str1 = Settings.System.getString(paramContext.getContentResolver(), "company_custom_default_ringtone_version");
    String str2 = Settings.System.getString(paramContext.getContentResolver(), "carrier_custom_default_ringtone_version");
    String str3 = SystemProperties.get("ro.oplus.company.ringtone.version", "");
    String str4 = SystemProperties.get("ro.oplus.carrier.ringtone.version", "");
    if (isCustRingtoneVersionUpdate(str3, str1)) {
      Settings.System.putString(paramContext.getContentResolver(), "company_custom_default_ringtone_version", str3);
      Log.d("OplusRingtoneManager", "company custom ringtone version change, need update config");
      return true;
    } 
    if (isCustRingtoneVersionUpdate(str4, str2)) {
      Settings.System.putString(paramContext.getContentResolver(), "carrier_custom_default_ringtone_version", str4);
      Log.d("OplusRingtoneManager", "carrier custom ringtone version change, need update config");
      return true;
    } 
    return false;
  }
  
  private static boolean isVersionStringChanged(String paramString1, String paramString2) {
    if (!paramString1.equals("default")) {
      String str = "";
      if (!paramString1.equals("") && !paramString1.equals("nconf")) {
        if (paramString2 == null)
          paramString2 = str; 
        return paramString2.equals(paramString1) ^ true;
      } 
    } 
    return false;
  }
  
  private static boolean isCustRingtoneVersionUpdate(String paramString1, String paramString2) {
    boolean bool = TextUtils.isEmpty(paramString1);
    boolean bool1 = false;
    if (bool)
      return false; 
    if (TextUtils.isEmpty(paramString2))
      paramString2 = ""; 
    if (paramString1.compareTo(paramString2) > 0)
      bool1 = true; 
    return bool1;
  }
  
  public static void clearDefaultRingtonesHistory(Context paramContext) {
    Log.d("OplusRingtoneManager", "component update, clearDefaultRingtonesHistory");
    Settings.System.putInt(paramContext.getContentResolver(), "ringtone_set", 0);
    Settings.System.putInt(paramContext.getContentResolver(), "ringtone_sim2_set", 0);
    Settings.System.putInt(paramContext.getContentResolver(), "notification_sound_set", 0);
    Settings.System.putInt(paramContext.getContentResolver(), "notification_sound_sms_set", 0);
    Settings.System.putInt(paramContext.getContentResolver(), "notification_sound_sim2_set", 0);
    Settings.System.putString(paramContext.getContentResolver(), "ringtone", "");
    Settings.System.putString(paramContext.getContentResolver(), "oppo_default_ringtone", "");
    Settings.System.putString(paramContext.getContentResolver(), "notification_sound", "");
    Settings.System.putString(paramContext.getContentResolver(), "oppo_default_notification", "");
    Settings.System.putString(paramContext.getContentResolver(), "oppo_sms_notification_sound", "");
    Settings.System.putString(paramContext.getContentResolver(), "oppo_default_sms_notification_sound", "");
    Settings.System.putString(paramContext.getContentResolver(), "notification_sim2", "");
    Settings.System.putString(paramContext.getContentResolver(), "oppo_default_notification_sim2", "");
    Settings.System.putString(paramContext.getContentResolver(), "ringtone_sim2", "");
    Settings.System.putString(paramContext.getContentResolver(), "oppo_default_ringtone_sim2", "");
  }
}
