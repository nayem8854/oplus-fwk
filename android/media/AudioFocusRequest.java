package android.media;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Handler;

public final class AudioFocusRequest {
  private static final AudioAttributes FOCUS_DEFAULT_ATTR;
  
  public static final String KEY_ACCESSIBILITY_FORCE_FOCUS_DUCKING = "a11y_force_ducking";
  
  private final AudioAttributes mAttr;
  
  private final int mFlags;
  
  private final int mFocusGain;
  
  private final AudioManager.OnAudioFocusChangeListener mFocusListener;
  
  private final Handler mListenerHandler;
  
  static {
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    FOCUS_DEFAULT_ATTR = builder.setUsage(1).build();
  }
  
  private AudioFocusRequest(AudioManager.OnAudioFocusChangeListener paramOnAudioFocusChangeListener, Handler paramHandler, AudioAttributes paramAudioAttributes, int paramInt1, int paramInt2) {
    this.mFocusListener = paramOnAudioFocusChangeListener;
    this.mListenerHandler = paramHandler;
    this.mFocusGain = paramInt1;
    this.mAttr = paramAudioAttributes;
    this.mFlags = paramInt2;
  }
  
  static final boolean isValidFocusGain(int paramInt) {
    if (paramInt != 1 && paramInt != 2 && paramInt != 3 && paramInt != 4)
      return false; 
    return true;
  }
  
  public AudioManager.OnAudioFocusChangeListener getOnAudioFocusChangeListener() {
    return this.mFocusListener;
  }
  
  public Handler getOnAudioFocusChangeListenerHandler() {
    return this.mListenerHandler;
  }
  
  public AudioAttributes getAudioAttributes() {
    return this.mAttr;
  }
  
  public int getFocusGain() {
    return this.mFocusGain;
  }
  
  public boolean willPauseWhenDucked() {
    boolean bool;
    if ((this.mFlags & 0x2) == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean acceptsDelayedFocusGain() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) != 1)
      bool = false; 
    return bool;
  }
  
  @SystemApi
  public boolean locksFocus() {
    boolean bool;
    if ((this.mFlags & 0x4) == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  int getFlags() {
    return this.mFlags;
  }
  
  public static final class Builder {
    private AudioAttributes mAttr = AudioFocusRequest.FOCUS_DEFAULT_ATTR;
    
    private boolean mPausesOnDuck = false;
    
    private boolean mDelayedFocus = false;
    
    private boolean mFocusLocked = false;
    
    private boolean mA11yForceDucking = false;
    
    private int mFocusGain;
    
    private AudioManager.OnAudioFocusChangeListener mFocusListener;
    
    private Handler mListenerHandler;
    
    public Builder(int param1Int) {
      setFocusGain(param1Int);
    }
    
    public Builder(AudioFocusRequest param1AudioFocusRequest) {
      if (param1AudioFocusRequest != null) {
        this.mAttr = param1AudioFocusRequest.mAttr;
        this.mFocusListener = param1AudioFocusRequest.mFocusListener;
        this.mListenerHandler = param1AudioFocusRequest.mListenerHandler;
        this.mFocusGain = param1AudioFocusRequest.mFocusGain;
        this.mPausesOnDuck = param1AudioFocusRequest.willPauseWhenDucked();
        this.mDelayedFocus = param1AudioFocusRequest.acceptsDelayedFocusGain();
        return;
      } 
      throw new IllegalArgumentException("Illegal null AudioFocusRequest");
    }
    
    public Builder setFocusGain(int param1Int) {
      if (AudioFocusRequest.isValidFocusGain(param1Int)) {
        this.mFocusGain = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Illegal audio focus gain type ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setOnAudioFocusChangeListener(AudioManager.OnAudioFocusChangeListener param1OnAudioFocusChangeListener) {
      if (param1OnAudioFocusChangeListener != null) {
        this.mFocusListener = param1OnAudioFocusChangeListener;
        this.mListenerHandler = null;
        return this;
      } 
      throw new NullPointerException("Illegal null focus listener");
    }
    
    Builder setOnAudioFocusChangeListenerInt(AudioManager.OnAudioFocusChangeListener param1OnAudioFocusChangeListener, Handler param1Handler) {
      this.mFocusListener = param1OnAudioFocusChangeListener;
      this.mListenerHandler = param1Handler;
      return this;
    }
    
    public Builder setOnAudioFocusChangeListener(AudioManager.OnAudioFocusChangeListener param1OnAudioFocusChangeListener, Handler param1Handler) {
      if (param1OnAudioFocusChangeListener != null && param1Handler != null) {
        this.mFocusListener = param1OnAudioFocusChangeListener;
        this.mListenerHandler = param1Handler;
        return this;
      } 
      throw new NullPointerException("Illegal null focus listener or handler");
    }
    
    public Builder setAudioAttributes(AudioAttributes param1AudioAttributes) {
      if (param1AudioAttributes != null) {
        this.mAttr = param1AudioAttributes;
        return this;
      } 
      throw new NullPointerException("Illegal null AudioAttributes");
    }
    
    public Builder setWillPauseWhenDucked(boolean param1Boolean) {
      this.mPausesOnDuck = param1Boolean;
      return this;
    }
    
    public Builder setAcceptsDelayedFocusGain(boolean param1Boolean) {
      this.mDelayedFocus = param1Boolean;
      return this;
    }
    
    @SystemApi
    public Builder setLocksFocus(boolean param1Boolean) {
      this.mFocusLocked = param1Boolean;
      return this;
    }
    
    public Builder setForceDucking(boolean param1Boolean) {
      this.mA11yForceDucking = param1Boolean;
      return this;
    }
    
    public AudioFocusRequest build() {
      if ((!this.mDelayedFocus && !this.mPausesOnDuck) || this.mFocusListener != null) {
        boolean bool1;
        if (this.mA11yForceDucking) {
          Bundle bundle;
          if (this.mAttr.getBundle() == null) {
            bundle = new Bundle();
          } else {
            bundle = this.mAttr.getBundle();
          } 
          bundle.putBoolean("a11y_force_ducking", true);
          this.mAttr = (new AudioAttributes.Builder(this.mAttr)).addBundle(bundle).build();
        } 
        boolean bool = this.mDelayedFocus;
        byte b = 0;
        if (this.mPausesOnDuck) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (this.mFocusLocked)
          b = 4; 
        return new AudioFocusRequest(this.mFocusListener, this.mListenerHandler, this.mAttr, this.mFocusGain, bool | false | bool1 | b);
      } 
      throw new IllegalStateException("Can't use delayed focus or pause on duck without a listener");
    }
  }
}
