package android.media.voice;

import android.annotation.SystemApi;
import android.hardware.soundtrigger.SoundTrigger;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import com.android.internal.app.IVoiceInteractionManagerService;
import java.util.Locale;
import java.util.Objects;

@SystemApi
public final class KeyphraseModelManager {
  private static final boolean DBG = false;
  
  private static final String TAG = "KeyphraseModelManager";
  
  private final IVoiceInteractionManagerService mVoiceInteractionManagerService;
  
  public KeyphraseModelManager(IVoiceInteractionManagerService paramIVoiceInteractionManagerService) {
    this.mVoiceInteractionManagerService = paramIVoiceInteractionManagerService;
  }
  
  public SoundTrigger.KeyphraseSoundModel getKeyphraseSoundModel(int paramInt, Locale paramLocale) {
    Objects.requireNonNull(paramLocale);
    try {
      IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mVoiceInteractionManagerService;
      String str = paramLocale.toLanguageTag();
      return iVoiceInteractionManagerService.getKeyphraseSoundModel(paramInt, str);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void updateKeyphraseSoundModel(SoundTrigger.KeyphraseSoundModel paramKeyphraseSoundModel) {
    Objects.requireNonNull(paramKeyphraseSoundModel);
    try {
      int i = this.mVoiceInteractionManagerService.updateKeyphraseSoundModel(paramKeyphraseSoundModel);
      if (i == 0)
        return; 
      ServiceSpecificException serviceSpecificException = new ServiceSpecificException();
      this(i);
      throw serviceSpecificException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void deleteKeyphraseSoundModel(int paramInt, Locale paramLocale) {
    Objects.requireNonNull(paramLocale);
    try {
      IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mVoiceInteractionManagerService;
      String str = paramLocale.toLanguageTag();
      paramInt = iVoiceInteractionManagerService.deleteKeyphraseSoundModel(paramInt, str);
      if (paramInt == 0)
        return; 
      ServiceSpecificException serviceSpecificException = new ServiceSpecificException();
      this(paramInt);
      throw serviceSpecificException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
