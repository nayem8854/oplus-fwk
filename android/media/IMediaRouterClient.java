package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaRouterClient extends IInterface {
  void onGlobalA2dpChanged(boolean paramBoolean) throws RemoteException;
  
  void onRestoreRoute() throws RemoteException;
  
  void onSelectedRouteChanged(String paramString) throws RemoteException;
  
  void onStateChanged() throws RemoteException;
  
  class Default implements IMediaRouterClient {
    public void onStateChanged() throws RemoteException {}
    
    public void onRestoreRoute() throws RemoteException {}
    
    public void onSelectedRouteChanged(String param1String) throws RemoteException {}
    
    public void onGlobalA2dpChanged(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaRouterClient {
    private static final String DESCRIPTOR = "android.media.IMediaRouterClient";
    
    static final int TRANSACTION_onGlobalA2dpChanged = 4;
    
    static final int TRANSACTION_onRestoreRoute = 2;
    
    static final int TRANSACTION_onSelectedRouteChanged = 3;
    
    static final int TRANSACTION_onStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaRouterClient");
    }
    
    public static IMediaRouterClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaRouterClient");
      if (iInterface != null && iInterface instanceof IMediaRouterClient)
        return (IMediaRouterClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onGlobalA2dpChanged";
          } 
          return "onSelectedRouteChanged";
        } 
        return "onRestoreRoute";
      } 
      return "onStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            boolean bool;
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.media.IMediaRouterClient");
              return true;
            } 
            param1Parcel1.enforceInterface("android.media.IMediaRouterClient");
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            onGlobalA2dpChanged(bool);
            return true;
          } 
          param1Parcel1.enforceInterface("android.media.IMediaRouterClient");
          str = param1Parcel1.readString();
          onSelectedRouteChanged(str);
          return true;
        } 
        str.enforceInterface("android.media.IMediaRouterClient");
        onRestoreRoute();
        return true;
      } 
      str.enforceInterface("android.media.IMediaRouterClient");
      onStateChanged();
      return true;
    }
    
    private static class Proxy implements IMediaRouterClient {
      public static IMediaRouterClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaRouterClient";
      }
      
      public void onStateChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouterClient");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouterClient.Stub.getDefaultImpl() != null) {
            IMediaRouterClient.Stub.getDefaultImpl().onStateChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRestoreRoute() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouterClient");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouterClient.Stub.getDefaultImpl() != null) {
            IMediaRouterClient.Stub.getDefaultImpl().onRestoreRoute();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSelectedRouteChanged(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouterClient");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouterClient.Stub.getDefaultImpl() != null) {
            IMediaRouterClient.Stub.getDefaultImpl().onSelectedRouteChanged(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGlobalA2dpChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IMediaRouterClient");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool1 && IMediaRouterClient.Stub.getDefaultImpl() != null) {
            IMediaRouterClient.Stub.getDefaultImpl().onGlobalA2dpChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaRouterClient param1IMediaRouterClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaRouterClient != null) {
          Proxy.sDefaultImpl = param1IMediaRouterClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaRouterClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
