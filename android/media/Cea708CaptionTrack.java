package android.media;

import java.util.Vector;

class Cea708CaptionTrack extends SubtitleTrack {
  private final Cea708CCParser mCCParser;
  
  private final Cea708CCWidget mRenderingWidget;
  
  Cea708CaptionTrack(Cea708CCWidget paramCea708CCWidget, MediaFormat paramMediaFormat) {
    super(paramMediaFormat);
    this.mRenderingWidget = paramCea708CCWidget;
    this.mCCParser = new Cea708CCParser(this.mRenderingWidget);
  }
  
  public void onData(byte[] paramArrayOfbyte, boolean paramBoolean, long paramLong) {
    this.mCCParser.parse(paramArrayOfbyte);
  }
  
  public SubtitleTrack.RenderingWidget getRenderingWidget() {
    return this.mRenderingWidget;
  }
  
  public void updateView(Vector<SubtitleTrack.Cue> paramVector) {}
}
