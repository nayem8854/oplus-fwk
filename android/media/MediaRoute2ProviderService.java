package android.media;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;

public abstract class MediaRoute2ProviderService extends Service {
  private static final boolean DEBUG = Log.isLoggable("MR2ProviderService", 3);
  
  private final Object mSessionLock = new Object();
  
  private final Object mRequestIdsLock = new Object();
  
  private final AtomicBoolean mStatePublishScheduled = new AtomicBoolean(false);
  
  private final Deque<Long> mRequestIds = new ArrayDeque<>(500);
  
  private final ArrayMap<String, RoutingSessionInfo> mSessionInfo = new ArrayMap();
  
  private static final int MAX_REQUEST_IDS_SIZE = 500;
  
  public static final int REASON_INVALID_COMMAND = 4;
  
  public static final int REASON_NETWORK_ERROR = 2;
  
  public static final int REASON_REJECTED = 1;
  
  public static final int REASON_ROUTE_NOT_AVAILABLE = 3;
  
  public static final int REASON_UNKNOWN_ERROR = 0;
  
  public static final long REQUEST_ID_NONE = 0L;
  
  public static final String SERVICE_INTERFACE = "android.media.MediaRoute2ProviderService";
  
  private static final String TAG = "MR2ProviderService";
  
  private final Handler mHandler;
  
  private volatile MediaRoute2ProviderInfo mProviderInfo;
  
  private IMediaRoute2ProviderServiceCallback mRemoteCallback;
  
  private MediaRoute2ProviderServiceStub mStub;
  
  public MediaRoute2ProviderService() {
    this.mHandler = new Handler(Looper.getMainLooper());
  }
  
  public IBinder onBind(Intent paramIntent) {
    if ("android.media.MediaRoute2ProviderService".equals(paramIntent.getAction())) {
      if (this.mStub == null)
        this.mStub = new MediaRoute2ProviderServiceStub(); 
      return this.mStub;
    } 
    return null;
  }
  
  public final RoutingSessionInfo getSessionInfo(String paramString) {
    if (!TextUtils.isEmpty(paramString))
      synchronized (this.mSessionLock) {
        return (RoutingSessionInfo)this.mSessionInfo.get(paramString);
      }  
    throw new IllegalArgumentException("sessionId must not be empty");
  }
  
  public final List<RoutingSessionInfo> getAllSessionInfo() {
    synchronized (this.mSessionLock) {
      ArrayList<RoutingSessionInfo> arrayList = new ArrayList();
      this(this.mSessionInfo.values());
      return arrayList;
    } 
  }
  
  public final void notifySessionCreated(long paramLong, RoutingSessionInfo paramRoutingSessionInfo) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifySessionCreated: Creating a session. requestId=");
      stringBuilder.append(paramLong);
      stringBuilder.append(", sessionInfo=");
      stringBuilder.append(paramRoutingSessionInfo);
      Log.d("MR2ProviderService", stringBuilder.toString());
    } 
    if (paramLong != 0L && !removeRequestId(paramLong)) {
      null = new StringBuilder();
      null.append("notifySessionCreated: The requestId doesn't exist. requestId=");
      null.append(paramLong);
      Log.w("MR2ProviderService", null.toString());
      return;
    } 
    String str = null.getId();
    synchronized (this.mSessionLock) {
      if (this.mSessionInfo.containsKey(str)) {
        Log.w("MR2ProviderService", "notifySessionCreated: Ignoring duplicate session id.");
        return;
      } 
      this.mSessionInfo.put(null.getId(), null);
      if (this.mRemoteCallback == null)
        return; 
      try {
        this.mRemoteCallback.notifySessionCreated(paramLong, (RoutingSessionInfo)null);
      } catch (RemoteException remoteException) {
        Log.w("MR2ProviderService", "Failed to notify session created.");
      } 
      return;
    } 
  }
  
  public final void notifySessionUpdated(RoutingSessionInfo paramRoutingSessionInfo) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifySessionUpdated: Updating session id=");
      stringBuilder.append(paramRoutingSessionInfo);
      Log.d("MR2ProviderService", stringBuilder.toString());
    } 
    String str = paramRoutingSessionInfo.getId();
    synchronized (this.mSessionLock) {
      if (this.mSessionInfo.containsKey(str)) {
        this.mSessionInfo.put(str, paramRoutingSessionInfo);
        if (this.mRemoteCallback == null)
          return; 
        try {
          this.mRemoteCallback.notifySessionUpdated(paramRoutingSessionInfo);
        } catch (RemoteException remoteException) {
          Log.w("MR2ProviderService", "Failed to notify session info changed.");
        } 
        return;
      } 
      Log.w("MR2ProviderService", "notifySessionUpdated: Ignoring unknown session info.");
      return;
    } 
  }
  
  public final void notifySessionReleased(String paramString) {
    if (!TextUtils.isEmpty(paramString)) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("notifySessionReleased: Releasing session id=");
        stringBuilder.append(paramString);
        Log.d("MR2ProviderService", stringBuilder.toString());
      } 
      synchronized (this.mSessionLock) {
        RoutingSessionInfo routingSessionInfo = (RoutingSessionInfo)this.mSessionInfo.remove(paramString);
        if (routingSessionInfo == null) {
          Log.w("MR2ProviderService", "notifySessionReleased: Ignoring unknown session info.");
          return;
        } 
        if (this.mRemoteCallback == null)
          return; 
        try {
          this.mRemoteCallback.notifySessionReleased(routingSessionInfo);
        } catch (RemoteException remoteException) {
          Log.w("MR2ProviderService", "Failed to notify session released.", (Throwable)remoteException);
        } 
        return;
      } 
    } 
    throw new IllegalArgumentException("sessionId must not be empty");
  }
  
  public final void notifyRequestFailed(long paramLong, int paramInt) {
    if (this.mRemoteCallback == null)
      return; 
    if (!removeRequestId(paramLong)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifyRequestFailed: The requestId doesn't exist. requestId=");
      stringBuilder.append(paramLong);
      Log.w("MR2ProviderService", stringBuilder.toString());
      return;
    } 
    try {
      this.mRemoteCallback.notifyRequestFailed(paramLong, paramInt);
    } catch (RemoteException remoteException) {
      Log.w("MR2ProviderService", "Failed to notify that the request has failed.");
    } 
  }
  
  public void onDiscoveryPreferenceChanged(RouteDiscoveryPreference paramRouteDiscoveryPreference) {}
  
  public final void notifyRoutes(Collection<MediaRoute2Info> paramCollection) {
    Objects.requireNonNull(paramCollection, "routes must not be null");
    MediaRoute2ProviderInfo.Builder builder2 = new MediaRoute2ProviderInfo.Builder();
    MediaRoute2ProviderInfo.Builder builder1 = builder2.addRoutes(paramCollection);
    this.mProviderInfo = builder1.build();
    schedulePublishState();
  }
  
  void setCallback(IMediaRoute2ProviderServiceCallback paramIMediaRoute2ProviderServiceCallback) {
    this.mRemoteCallback = paramIMediaRoute2ProviderServiceCallback;
    schedulePublishState();
  }
  
  void schedulePublishState() {
    if (this.mStatePublishScheduled.compareAndSet(false, true))
      this.mHandler.post(new _$$Lambda$MediaRoute2ProviderService$MC0o_sBd6s0cxW9vHmSYdgeA6FM(this)); 
  }
  
  private void publishState() {
    if (!this.mStatePublishScheduled.compareAndSet(true, false))
      return; 
    IMediaRoute2ProviderServiceCallback iMediaRoute2ProviderServiceCallback = this.mRemoteCallback;
    if (iMediaRoute2ProviderServiceCallback == null)
      return; 
    try {
      iMediaRoute2ProviderServiceCallback.updateState(this.mProviderInfo);
    } catch (RemoteException remoteException) {
      Log.w("MR2ProviderService", "Failed to publish provider state.", (Throwable)remoteException);
    } 
  }
  
  private void addRequestId(long paramLong) {
    synchronized (this.mRequestIdsLock) {
      if (this.mRequestIds.size() >= 500)
        this.mRequestIds.removeFirst(); 
      this.mRequestIds.addLast(Long.valueOf(paramLong));
      return;
    } 
  }
  
  private boolean removeRequestId(long paramLong) {
    synchronized (this.mRequestIdsLock) {
      return this.mRequestIds.removeFirstOccurrence(Long.valueOf(paramLong));
    } 
  }
  
  public abstract void onCreateSession(long paramLong, String paramString1, String paramString2, Bundle paramBundle);
  
  public abstract void onDeselectRoute(long paramLong, String paramString1, String paramString2);
  
  public abstract void onReleaseSession(long paramLong, String paramString);
  
  public abstract void onSelectRoute(long paramLong, String paramString1, String paramString2);
  
  public abstract void onSetRouteVolume(long paramLong, String paramString, int paramInt);
  
  public abstract void onSetSessionVolume(long paramLong, String paramString, int paramInt);
  
  public abstract void onTransferToRoute(long paramLong, String paramString1, String paramString2);
  
  final class MediaRoute2ProviderServiceStub extends IMediaRoute2ProviderService.Stub {
    final MediaRoute2ProviderService this$0;
    
    private boolean checkCallerIsSystem() {
      boolean bool;
      if (Binder.getCallingUid() == 1000) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean checkSessionIdIsValid(String param1String1, String param1String2) {
      StringBuilder stringBuilder;
      if (TextUtils.isEmpty(param1String1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(param1String2);
        stringBuilder.append(": Ignoring empty sessionId from system service.");
        Log.w("MR2ProviderService", stringBuilder.toString());
        return false;
      } 
      if (MediaRoute2ProviderService.this.getSessionInfo((String)stringBuilder) == null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(param1String2);
        stringBuilder1.append(": Ignoring unknown session from system service. sessionId=");
        stringBuilder1.append((String)stringBuilder);
        Log.w("MR2ProviderService", stringBuilder1.toString());
        return false;
      } 
      return true;
    }
    
    private boolean checkRouteIdIsValid(String param1String1, String param1String2) {
      StringBuilder stringBuilder;
      if (TextUtils.isEmpty(param1String1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(param1String2);
        stringBuilder.append(": Ignoring empty routeId from system service.");
        Log.w("MR2ProviderService", stringBuilder.toString());
        return false;
      } 
      if (MediaRoute2ProviderService.this.mProviderInfo == null || MediaRoute2ProviderService.this.mProviderInfo.getRoute((String)stringBuilder) == null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(param1String2);
        stringBuilder1.append(": Ignoring unknown route from system service. routeId=");
        stringBuilder1.append((String)stringBuilder);
        Log.w("MR2ProviderService", stringBuilder1.toString());
        return false;
      } 
      return true;
    }
    
    public void setCallback(IMediaRoute2ProviderServiceCallback param1IMediaRoute2ProviderServiceCallback) {
      if (!checkCallerIsSystem())
        return; 
      MediaRoute2ProviderService.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$io02Kf1ict0DH83WLluLtzQSGQo.INSTANCE, MediaRoute2ProviderService.this, param1IMediaRoute2ProviderServiceCallback));
    }
    
    public void updateDiscoveryPreference(RouteDiscoveryPreference param1RouteDiscoveryPreference) {
      if (!checkCallerIsSystem())
        return; 
      MediaRoute2ProviderService.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$zs_1ohlviAzvACJcfYn_NI8YNFc.INSTANCE, MediaRoute2ProviderService.this, param1RouteDiscoveryPreference));
    }
    
    public void setRouteVolume(long param1Long, String param1String, int param1Int) {
      if (!checkCallerIsSystem())
        return; 
      if (!checkRouteIdIsValid(param1String, "setRouteVolume"))
        return; 
      MediaRoute2ProviderService.this.addRequestId(param1Long);
      Handler handler = MediaRoute2ProviderService.this.mHandler;
      -$.Lambda.Z9rxhzVNP08ZyBKTAu2QaNgeJBo z9rxhzVNP08ZyBKTAu2QaNgeJBo = _$$Lambda$Z9rxhzVNP08ZyBKTAu2QaNgeJBo.INSTANCE;
      MediaRoute2ProviderService mediaRoute2ProviderService = MediaRoute2ProviderService.this;
      handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)z9rxhzVNP08ZyBKTAu2QaNgeJBo, mediaRoute2ProviderService, Long.valueOf(param1Long), param1String, Integer.valueOf(param1Int)));
    }
    
    public void requestCreateSession(long param1Long, String param1String1, String param1String2, Bundle param1Bundle) {
      if (!checkCallerIsSystem())
        return; 
      if (!checkRouteIdIsValid(param1String2, "requestCreateSession"))
        return; 
      MediaRoute2ProviderService.this.addRequestId(param1Long);
      Handler handler = MediaRoute2ProviderService.this.mHandler;
      -$.Lambda.kJ3hCZUGWrnpcHbziUcLGUyS8YA kJ3hCZUGWrnpcHbziUcLGUyS8YA = _$$Lambda$kJ3hCZUGWrnpcHbziUcLGUyS8YA.INSTANCE;
      MediaRoute2ProviderService mediaRoute2ProviderService = MediaRoute2ProviderService.this;
      handler.sendMessage(PooledLambda.obtainMessage((QuintConsumer)kJ3hCZUGWrnpcHbziUcLGUyS8YA, mediaRoute2ProviderService, Long.valueOf(param1Long), param1String1, param1String2, param1Bundle));
    }
    
    public void selectRoute(long param1Long, String param1String1, String param1String2) {
      if (!checkCallerIsSystem())
        return; 
      if (!checkSessionIdIsValid(param1String1, "selectRoute") || 
        !checkRouteIdIsValid(param1String2, "selectRoute"))
        return; 
      MediaRoute2ProviderService.this.addRequestId(param1Long);
      Handler handler = MediaRoute2ProviderService.this.mHandler;
      -$.Lambda.vBRPMdg_QrRz6M5Jf1vFUX7p348 vBRPMdg_QrRz6M5Jf1vFUX7p348 = _$$Lambda$vBRPMdg_QrRz6M5Jf1vFUX7p348.INSTANCE;
      MediaRoute2ProviderService mediaRoute2ProviderService = MediaRoute2ProviderService.this;
      handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)vBRPMdg_QrRz6M5Jf1vFUX7p348, mediaRoute2ProviderService, Long.valueOf(param1Long), param1String1, param1String2));
    }
    
    public void deselectRoute(long param1Long, String param1String1, String param1String2) {
      if (!checkCallerIsSystem())
        return; 
      if (!checkSessionIdIsValid(param1String1, "deselectRoute") || 
        !checkRouteIdIsValid(param1String2, "deselectRoute"))
        return; 
      MediaRoute2ProviderService.this.addRequestId(param1Long);
      Handler handler = MediaRoute2ProviderService.this.mHandler;
      -$.Lambda.oIYR_uFXIzjOYgcaHDJvXF7vaTo oIYR_uFXIzjOYgcaHDJvXF7vaTo = _$$Lambda$oIYR_uFXIzjOYgcaHDJvXF7vaTo.INSTANCE;
      MediaRoute2ProviderService mediaRoute2ProviderService = MediaRoute2ProviderService.this;
      handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)oIYR_uFXIzjOYgcaHDJvXF7vaTo, mediaRoute2ProviderService, Long.valueOf(param1Long), param1String1, param1String2));
    }
    
    public void transferToRoute(long param1Long, String param1String1, String param1String2) {
      if (!checkCallerIsSystem())
        return; 
      if (!checkSessionIdIsValid(param1String1, "transferToRoute") || 
        !checkRouteIdIsValid(param1String2, "transferToRoute"))
        return; 
      MediaRoute2ProviderService.this.addRequestId(param1Long);
      Handler handler = MediaRoute2ProviderService.this.mHandler;
      -$.Lambda.oNqqU7q9NNLGaqLJ6CkSYk9Pwt0 oNqqU7q9NNLGaqLJ6CkSYk9Pwt0 = _$$Lambda$oNqqU7q9NNLGaqLJ6CkSYk9Pwt0.INSTANCE;
      MediaRoute2ProviderService mediaRoute2ProviderService = MediaRoute2ProviderService.this;
      handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)oNqqU7q9NNLGaqLJ6CkSYk9Pwt0, mediaRoute2ProviderService, Long.valueOf(param1Long), param1String1, param1String2));
    }
    
    public void setSessionVolume(long param1Long, String param1String, int param1Int) {
      if (!checkCallerIsSystem())
        return; 
      if (!checkSessionIdIsValid(param1String, "setSessionVolume"))
        return; 
      MediaRoute2ProviderService.this.addRequestId(param1Long);
      Handler handler = MediaRoute2ProviderService.this.mHandler;
      -$.Lambda.NaAryPBSI2JqN5mM_RWDdyz8eFE naAryPBSI2JqN5mM_RWDdyz8eFE = _$$Lambda$NaAryPBSI2JqN5mM_RWDdyz8eFE.INSTANCE;
      MediaRoute2ProviderService mediaRoute2ProviderService = MediaRoute2ProviderService.this;
      handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)naAryPBSI2JqN5mM_RWDdyz8eFE, mediaRoute2ProviderService, Long.valueOf(param1Long), param1String, Integer.valueOf(param1Int)));
    }
    
    public void releaseSession(long param1Long, String param1String) {
      if (!checkCallerIsSystem())
        return; 
      if (!checkSessionIdIsValid(param1String, "releaseSession"))
        return; 
      MediaRoute2ProviderService.this.addRequestId(param1Long);
      Handler handler = MediaRoute2ProviderService.this.mHandler;
      -$.Lambda.vzDjZebXFgg25eChK64A6RbOKWM vzDjZebXFgg25eChK64A6RbOKWM = _$$Lambda$vzDjZebXFgg25eChK64A6RbOKWM.INSTANCE;
      MediaRoute2ProviderService mediaRoute2ProviderService = MediaRoute2ProviderService.this;
      handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)vzDjZebXFgg25eChK64A6RbOKWM, mediaRoute2ProviderService, Long.valueOf(param1Long), param1String));
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Reason implements Annotation {}
}
