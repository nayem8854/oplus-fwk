package android.media;

import android.hardware.cas.V1_0.IDescramblerBase;
import android.os.IHwBinder;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.util.Log;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public final class MediaDescrambler implements AutoCloseable {
  public static final byte SCRAMBLE_CONTROL_EVEN_KEY = 2;
  
  public static final byte SCRAMBLE_CONTROL_ODD_KEY = 3;
  
  public static final byte SCRAMBLE_CONTROL_RESERVED = 1;
  
  public static final byte SCRAMBLE_CONTROL_UNSCRAMBLED = 0;
  
  public static final byte SCRAMBLE_FLAG_PES_HEADER = 1;
  
  private static final String TAG = "MediaDescrambler";
  
  private IDescramblerBase mIDescrambler;
  
  private long mNativeContext;
  
  private final void validateInternalStates() {
    if (this.mIDescrambler != null)
      return; 
    throw new IllegalStateException();
  }
  
  private final void cleanupAndRethrowIllegalState() {
    this.mIDescrambler = null;
    throw new IllegalStateException();
  }
  
  public MediaDescrambler(int paramInt) throws MediaCasException.UnsupportedCasException {
    try {
      IDescramblerBase iDescramblerBase = MediaCas.getService().createDescrambler(paramInt);
      if (iDescramblerBase == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported CA_system_id ");
        stringBuilder.append(paramInt);
        throw new MediaCasException.UnsupportedCasException(stringBuilder.toString());
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to create descrambler: ");
      stringBuilder.append(exception);
      Log.e("MediaDescrambler", stringBuilder.toString());
      this.mIDescrambler = null;
      if (!false) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unsupported CA_system_id ");
        stringBuilder1.append(paramInt);
        throw new MediaCasException.UnsupportedCasException(stringBuilder1.toString());
      } 
    } finally {
      Exception exception;
    } 
    native_setup(this.mIDescrambler.asBinder());
  }
  
  IHwBinder getBinder() {
    validateInternalStates();
    return this.mIDescrambler.asBinder();
  }
  
  public final boolean requiresSecureDecoderComponent(String paramString) {
    validateInternalStates();
    try {
      return this.mIDescrambler.requiresSecureDecoderComponent(paramString);
    } catch (RemoteException remoteException) {
      cleanupAndRethrowIllegalState();
      return true;
    } 
  }
  
  public final void setMediaCasSession(MediaCas.Session paramSession) {
    validateInternalStates();
    try {
      IDescramblerBase iDescramblerBase = this.mIDescrambler;
      ArrayList<Byte> arrayList = paramSession.mSessionId;
      int i = iDescramblerBase.setMediaCasSession(arrayList);
      MediaCasStateException.throwExceptionIfNeeded(i);
    } catch (RemoteException remoteException) {
      cleanupAndRethrowIllegalState();
    } 
  }
  
  public final int descramble(ByteBuffer paramByteBuffer1, ByteBuffer paramByteBuffer2, MediaCodec.CryptoInfo paramCryptoInfo) {
    int[] arrayOfInt;
    validateInternalStates();
    if (paramCryptoInfo.numSubSamples > 0) {
      if (paramCryptoInfo.numBytesOfClearData != null || paramCryptoInfo.numBytesOfEncryptedData != null) {
        if (paramCryptoInfo.numBytesOfClearData == null || paramCryptoInfo.numBytesOfClearData.length >= paramCryptoInfo.numSubSamples) {
          if (paramCryptoInfo.numBytesOfEncryptedData == null || paramCryptoInfo.numBytesOfEncryptedData.length >= paramCryptoInfo.numSubSamples) {
            if (paramCryptoInfo.key != null && paramCryptoInfo.key.length == 16) {
              try {
                byte b1 = paramCryptoInfo.key[0], b2 = paramCryptoInfo.key[1];
                int i = paramCryptoInfo.numSubSamples, arrayOfInt1[] = paramCryptoInfo.numBytesOfClearData;
                arrayOfInt = paramCryptoInfo.numBytesOfEncryptedData;
                null = paramByteBuffer1.position();
                int j = paramByteBuffer1.limit();
                int k = paramByteBuffer2.position(), m = paramByteBuffer2.limit();
                return native_descramble(b1, b2, i, arrayOfInt1, arrayOfInt, paramByteBuffer1, null, j, paramByteBuffer2, k, m);
              } catch (ServiceSpecificException serviceSpecificException) {
                MediaCasStateException.throwExceptionIfNeeded(serviceSpecificException.errorCode, serviceSpecificException.getMessage());
              } catch (RemoteException remoteException) {
                cleanupAndRethrowIllegalState();
              } 
              return -1;
            } 
            throw new IllegalArgumentException("Invalid CryptoInfo: key array is invalid!");
          } 
          throw new IllegalArgumentException("Invalid CryptoInfo: numBytesOfEncryptedData is too small!");
        } 
        throw new IllegalArgumentException("Invalid CryptoInfo: numBytesOfClearData is too small!");
      } 
      throw new IllegalArgumentException("Invalid CryptoInfo: clearData and encryptedData size arrays are both null!");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid CryptoInfo: invalid numSubSamples=");
    stringBuilder.append(((MediaCodec.CryptoInfo)arrayOfInt).numSubSamples);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void close() {
    IDescramblerBase iDescramblerBase = this.mIDescrambler;
    if (iDescramblerBase != null)
      try {
        iDescramblerBase.release();
      } catch (RemoteException remoteException) {
      
      } finally {
        this.mIDescrambler = null;
      }  
    native_release();
  }
  
  protected void finalize() {
    close();
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private final native int native_descramble(byte paramByte1, byte paramByte2, int paramInt1, int[] paramArrayOfint1, int[] paramArrayOfint2, ByteBuffer paramByteBuffer1, int paramInt2, int paramInt3, ByteBuffer paramByteBuffer2, int paramInt4, int paramInt5) throws RemoteException;
  
  private static final native void native_init();
  
  private final native void native_release();
  
  private final native void native_setup(IHwBinder paramIHwBinder);
}
