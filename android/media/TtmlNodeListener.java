package android.media;

interface TtmlNodeListener {
  void onRootNodeParsed(TtmlNode paramTtmlNode);
  
  void onTtmlNodeParsed(TtmlNode paramTtmlNode);
}
