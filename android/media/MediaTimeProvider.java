package android.media;

public interface MediaTimeProvider {
  public static final long NO_TIME = -1L;
  
  void cancelNotifications(OnMediaTimeListener paramOnMediaTimeListener);
  
  long getCurrentTimeUs(boolean paramBoolean1, boolean paramBoolean2) throws IllegalStateException;
  
  void notifyAt(long paramLong, OnMediaTimeListener paramOnMediaTimeListener);
  
  void scheduleUpdate(OnMediaTimeListener paramOnMediaTimeListener);
  
  public static interface OnMediaTimeListener {
    void onSeek(long param1Long);
    
    void onStop();
    
    void onTimedEvent(long param1Long);
  }
}
