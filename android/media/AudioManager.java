package android.media;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.bluetooth.BluetoothCodecConfig;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.audiopolicy.AudioPolicy;
import android.media.audiopolicy.AudioPolicyConfig;
import android.media.audiopolicy.AudioProductStrategy;
import android.media.audiopolicy.AudioVolumeGroup;
import android.media.audiopolicy.AudioVolumeGroupChangeHandler;
import android.media.audiopolicy.IAudioPolicyCallback;
import android.media.projection.IMediaProjection;
import android.media.projection.MediaProjection;
import android.media.session.MediaSessionLegacyHelper;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import com.android.internal.util.Preconditions;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

public class AudioManager extends OppoBaseAudioManager {
  private static final AudioPortEventHandler sAudioPortEventHandler = new AudioPortEventHandler();
  
  static {
    sAudioAudioVolumeGroupChangedHandler = new AudioVolumeGroupChangeHandler();
    TreeMap<Object, Object> treeMap = new TreeMap<>();
    treeMap.put(Integer.valueOf(1), "FLAG_SHOW_UI");
    FLAG_NAMES.put(Integer.valueOf(2), "FLAG_ALLOW_RINGER_MODES");
    FLAG_NAMES.put(Integer.valueOf(4), "FLAG_PLAY_SOUND");
    FLAG_NAMES.put(Integer.valueOf(8), "FLAG_REMOVE_SOUND_AND_VIBRATE");
    FLAG_NAMES.put(Integer.valueOf(16), "FLAG_VIBRATE");
    FLAG_NAMES.put(Integer.valueOf(32), "FLAG_FIXED_VOLUME");
    FLAG_NAMES.put(Integer.valueOf(64), "FLAG_BLUETOOTH_ABS_VOLUME");
    FLAG_NAMES.put(Integer.valueOf(128), "FLAG_SHOW_SILENT_HINT");
    FLAG_NAMES.put(Integer.valueOf(256), "FLAG_HDMI_SYSTEM_AUDIO_VOLUME");
    FLAG_NAMES.put(Integer.valueOf(512), "FLAG_ACTIVE_MEDIA_ONLY");
    FLAG_NAMES.put(Integer.valueOf(1024), "FLAG_SHOW_UI_WARNINGS");
    FLAG_NAMES.put(Integer.valueOf(2048), "FLAG_SHOW_VIBRATE_HINT");
    FLAG_NAMES.put(Integer.valueOf(4096), "FLAG_FROM_KEY");
    sAudioPortGeneration = new Integer(0);
    sAudioPortsCached = new ArrayList<>();
    sPreviousAudioPortsCached = new ArrayList<>();
    sAudioPatchesCached = new ArrayList<>();
  }
  
  public static final String adjustToString(int paramInt) {
    if (paramInt != -100) {
      if (paramInt != -1) {
        if (paramInt != 0) {
          if (paramInt != 1) {
            if (paramInt != 100) {
              if (paramInt != 101) {
                StringBuilder stringBuilder = new StringBuilder("unknown adjust mode ");
                stringBuilder.append(paramInt);
                return stringBuilder.toString();
              } 
              return "ADJUST_TOGGLE_MUTE";
            } 
            return "ADJUST_UNMUTE";
          } 
          return "ADJUST_RAISE";
        } 
        return "ADJUST_SAME";
      } 
      return "ADJUST_LOWER";
    } 
    return "ADJUST_MUTE";
  }
  
  public static String flagsToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    int i;
    for (Iterator<Map.Entry> iterator = FLAG_NAMES.entrySet().iterator(); iterator.hasNext(); ) {
      Map.Entry entry = iterator.next();
      int j = ((Integer)entry.getKey()).intValue();
      paramInt = i;
      if ((i & j) != 0) {
        if (stringBuilder.length() > 0)
          stringBuilder.append(','); 
        stringBuilder.append((String)entry.getValue());
        paramInt = i & (j ^ 0xFFFFFFFF);
      } 
      i = paramInt;
    } 
    if (i != 0) {
      if (stringBuilder.length() > 0)
        stringBuilder.append(','); 
      stringBuilder.append(i);
    } 
    return stringBuilder.toString();
  }
  
  public AudioManager() {
    this.mUseVolumeKeySounds = true;
    this.mUseFixedVolume = false;
  }
  
  public AudioManager(Context paramContext) {
    super(paramContext);
    setContext(paramContext);
    this.mUseVolumeKeySounds = getContext().getResources().getBoolean(17891578);
    this.mUseFixedVolume = getContext().getResources().getBoolean(17891573);
  }
  
  private Context getContext() {
    if (this.mApplicationContext == null)
      setContext(this.mOriginalContext); 
    Context context = this.mApplicationContext;
    if (context != null)
      return context; 
    return this.mOriginalContext;
  }
  
  private void setContext(Context paramContext) {
    Context context = paramContext.getApplicationContext();
    if (context != null) {
      this.mOriginalContext = null;
    } else {
      this.mOriginalContext = paramContext;
    } 
  }
  
  private static IAudioService getService() {
    IAudioService iAudioService2 = sService;
    if (iAudioService2 != null)
      return iAudioService2; 
    IBinder iBinder = ServiceManager.getService("audio");
    IAudioService iAudioService1 = IAudioService.Stub.asInterface(iBinder);
    return iAudioService1;
  }
  
  public void dispatchMediaKeyEvent(KeyEvent paramKeyEvent) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("dispatchMediaKeyEvent keyEvent=");
    stringBuilder.append(paramKeyEvent);
    Log.d("AudioManager", stringBuilder.toString());
    MediaSessionLegacyHelper mediaSessionLegacyHelper = MediaSessionLegacyHelper.getHelper(getContext());
    mediaSessionLegacyHelper.sendMediaButtonEvent(paramKeyEvent, false);
  }
  
  public void preDispatchKeyEvent(KeyEvent paramKeyEvent, int paramInt) {
    int i = paramKeyEvent.getKeyCode();
    if (i != 25 && i != 24 && i != 164) {
      long l = this.mVolumeKeyUpTime;
      if (l + 300L > SystemClock.uptimeMillis())
        adjustSuggestedStreamVolume(0, paramInt, 8); 
    } 
  }
  
  public boolean isVolumeFixed() {
    return this.mUseFixedVolume;
  }
  
  public void adjustStreamVolume(int paramInt1, int paramInt2, int paramInt3) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("adjustStreamVolume streamType=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" direction=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" flags=");
      stringBuilder.append(paramInt3);
      stringBuilder.append(" from ");
      stringBuilder.append(getContext().getOpPackageName());
      String str = stringBuilder.toString();
      Log.d("AudioManager", str);
      if (!adjustStreamVolumePermission(paramInt1, paramInt2))
        return; 
      str = getContext().getOpPackageName();
      iAudioService.adjustStreamVolume(paramInt1, paramInt2, paramInt3, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void adjustVolume(int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("adjustVolume direction=");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" flags=");
    stringBuilder.append(paramInt2);
    Log.d("AudioManager", stringBuilder.toString());
    MediaSessionLegacyHelper mediaSessionLegacyHelper = MediaSessionLegacyHelper.getHelper(getContext());
    mediaSessionLegacyHelper.sendAdjustVolumeBy(-2147483648, paramInt1, paramInt2);
  }
  
  public void adjustSuggestedStreamVolume(int paramInt1, int paramInt2, int paramInt3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("adjustSuggestedStreamVolume direction=");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" suggestedStream=");
    stringBuilder.append(paramInt2);
    stringBuilder.append(" flags=");
    stringBuilder.append(paramInt3);
    stringBuilder.append(" from ");
    stringBuilder.append(getContext().getOpPackageName());
    String str = stringBuilder.toString();
    Log.d("AudioManager", str);
    MediaSessionLegacyHelper mediaSessionLegacyHelper = MediaSessionLegacyHelper.getHelper(getContext());
    mediaSessionLegacyHelper.sendAdjustVolumeBy(paramInt2, paramInt1, paramInt3);
  }
  
  public void setMasterMute(boolean paramBoolean, int paramInt) {
    IAudioService iAudioService = getService();
    try {
      String str = getContext().getOpPackageName();
      int i = UserHandle.getCallingUserId();
      iAudioService.setMasterMute(paramBoolean, paramInt, str, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getRingerMode() {
    IAudioService iAudioService = getService();
    try {
      int i = iAudioService.getRingerModeExternal();
      if (mDebugLog) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("getRingerMode mode=");
        stringBuilder.append(i);
        Log.d("AudioManager", stringBuilder.toString());
      } 
      return i;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean isValidRingerMode(int paramInt) {
    if (paramInt < 0 || paramInt > 2)
      return false; 
    IAudioService iAudioService = getService();
    try {
      return iAudioService.isValidRingerMode(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getStreamMaxVolume(int paramInt) {
    return oppoGetStreamMaxVolume(paramInt);
  }
  
  public int getStreamMinVolume(int paramInt) {
    if (isPublicStreamType(paramInt))
      return getStreamMinVolumeInt(paramInt); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid stream type ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getStreamMinVolumeInt(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getStreamMinVolume(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getStreamVolume(int paramInt) {
    return oppoGetStreamVolume(paramInt);
  }
  
  public float getStreamVolumeDb(int paramInt1, int paramInt2, int paramInt3) {
    if (isPublicStreamType(paramInt1)) {
      if (paramInt2 <= getStreamMaxVolume(paramInt1) && paramInt2 >= getStreamMinVolume(paramInt1)) {
        if (AudioDeviceInfo.isValidAudioDeviceTypeOut(paramInt3)) {
          paramInt3 = AudioDeviceInfo.convertDeviceTypeToInternalDevice(paramInt3);
          float f = AudioSystem.getStreamVolumeDB(paramInt1, paramInt2, paramInt3);
          if (f <= -758.0F)
            return Float.NEGATIVE_INFINITY; 
          return f;
        } 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Invalid audio output device type ");
        stringBuilder2.append(paramInt3);
        throw new IllegalArgumentException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid stream volume index ");
      stringBuilder1.append(paramInt2);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid stream type ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static boolean isPublicStreamType(int paramInt) {
    if (paramInt != 0 && paramInt != 1 && paramInt != 2 && paramInt != 3 && paramInt != 4 && paramInt != 5 && paramInt != 8 && paramInt != 10)
      return false; 
    return true;
  }
  
  public int getLastAudibleStreamVolume(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getLastAudibleStreamVolume(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUiSoundsStreamType() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getUiSoundsStreamType();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setRingerMode(int paramInt) {
    if (!isValidRingerMode(paramInt))
      return; 
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setRingerMode ringerMode=");
      stringBuilder.append(paramInt);
      stringBuilder.append(" from ");
      stringBuilder.append(getContext().getOpPackageName());
      String str = stringBuilder.toString();
      Log.d("AudioManager", str);
      if (!setRingerModePermission(paramInt))
        return; 
      iAudioService.setRingerModeExternal(paramInt, getContext().getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setStreamVolume(int paramInt1, int paramInt2, int paramInt3) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setStreamVolume streamType=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" index=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" flags=");
      stringBuilder.append(paramInt3);
      stringBuilder.append(" from ");
      stringBuilder.append(getContext().getOpPackageName());
      String str = stringBuilder.toString();
      Log.d("AudioManager", str);
      if (!setStreamVolumePermission(paramInt1))
        return; 
      iAudioService.setStreamVolume(paramInt1, paramInt2, paramInt3, getContext().getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setVolumeIndexForAttributes(AudioAttributes paramAudioAttributes, int paramInt1, int paramInt2) {
    Preconditions.checkNotNull(paramAudioAttributes, "attr must not be null");
    IAudioService iAudioService = getService();
    try {
      String str = getContext().getOpPackageName();
      iAudioService.setVolumeIndexForAttributes(paramAudioAttributes, paramInt1, paramInt2, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public int getVolumeIndexForAttributes(AudioAttributes paramAudioAttributes) {
    Preconditions.checkNotNull(paramAudioAttributes, "attr must not be null");
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getVolumeIndexForAttributes(paramAudioAttributes);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public int getMaxVolumeIndexForAttributes(AudioAttributes paramAudioAttributes) {
    Preconditions.checkNotNull(paramAudioAttributes, "attr must not be null");
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getMaxVolumeIndexForAttributes(paramAudioAttributes);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public int getMinVolumeIndexForAttributes(AudioAttributes paramAudioAttributes) {
    Preconditions.checkNotNull(paramAudioAttributes, "attr must not be null");
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getMinVolumeIndexForAttributes(paramAudioAttributes);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setSupportedSystemUsages(int[] paramArrayOfint) {
    Objects.requireNonNull(paramArrayOfint, "systemUsages must not be null");
    IAudioService iAudioService = getService();
    try {
      iAudioService.setSupportedSystemUsages(paramArrayOfint);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public int[] getSupportedSystemUsages() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getSupportedSystemUsages();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void setStreamSolo(int paramInt, boolean paramBoolean) {
    Log.w("AudioManager", "setStreamSolo has been deprecated. Do not use.");
  }
  
  @Deprecated
  public void setStreamMute(int paramInt, boolean paramBoolean) {
    byte b;
    Log.w("AudioManager", "setStreamMute is deprecated. adjustStreamVolume should be used instead.");
    if (paramBoolean) {
      b = -100;
    } else {
      b = 100;
    } 
    if (paramInt == Integer.MIN_VALUE) {
      adjustSuggestedStreamVolume(b, paramInt, 0);
    } else {
      adjustStreamVolume(paramInt, b, 0);
    } 
  }
  
  public boolean isStreamMute(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.isStreamMute(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isMasterMute() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.isMasterMute();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void forceVolumeControlStream(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("forceVolumeControlStream streamType=");
      stringBuilder.append(paramInt);
      stringBuilder.append(" from ");
      stringBuilder.append(getContext().getOpPackageName());
      String str = stringBuilder.toString();
      Log.d("AudioManager", str);
      iAudioService.forceVolumeControlStream(paramInt, this.mICallBack);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean shouldVibrate(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.shouldVibrate(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getVibrateSetting(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getVibrateSetting(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setVibrateSetting(int paramInt1, int paramInt2) {
    IAudioService iAudioService = getService();
    try {
      if (mDebugLog) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setVibrateSetting vibrateType=");
        stringBuilder.append(paramInt1);
        stringBuilder.append(" vibrateSetting=");
        stringBuilder.append(paramInt2);
        Log.d("AudioManager", stringBuilder.toString());
      } 
      iAudioService.setVibrateSetting(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setSpeakerphoneOn(boolean paramBoolean) {
    IAudioService iAudioService = getService();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("In setSpeakerphoneOn(), on: ");
    stringBuilder.append(paramBoolean);
    stringBuilder.append(", calling application: ");
    Context context = this.mApplicationContext;
    stringBuilder.append(context.getOpPackageName());
    String str = stringBuilder.toString();
    Log.i("AudioManager", str);
    try {
      if (!setSpeakerphoneOnPermission())
        return; 
      iAudioService.setSpeakerphoneOn(this.mICallBack, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isSpeakerphoneOn() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("In isSpeakerphoneOn(), calling application: ");
    Context context = this.mApplicationContext;
    stringBuilder.append(context.getOpPackageName());
    String str = stringBuilder.toString();
    Log.i("AudioManager", str);
    IAudioService iAudioService = getService();
    try {
      return iAudioService.isSpeakerphoneOn();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setAllowedCapturePolicy(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      paramInt = iAudioService.setAllowedCapturePolicy(paramInt);
      if (paramInt != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Could not setAllowedCapturePolicy: ");
        stringBuilder.append(paramInt);
        Log.e("AudioManager", stringBuilder.toString());
        return;
      } 
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getAllowedCapturePolicy() {
    boolean bool2, bool1 = true;
    try {
      bool2 = getService().getAllowedCapturePolicy();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to query allowed capture policy: ");
      stringBuilder.append(remoteException);
      Log.e("AudioManager", stringBuilder.toString());
      bool2 = bool1;
    } 
    return bool2;
  }
  
  @SystemApi
  public boolean setPreferredDeviceForStrategy(AudioProductStrategy paramAudioProductStrategy, AudioDeviceAttributes paramAudioDeviceAttributes) {
    Objects.requireNonNull(paramAudioProductStrategy);
    Objects.requireNonNull(paramAudioDeviceAttributes);
    try {
      boolean bool;
      int i = getService().setPreferredDeviceForStrategy(paramAudioProductStrategy.getId(), paramAudioDeviceAttributes);
      if (i == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean removePreferredDeviceForStrategy(AudioProductStrategy paramAudioProductStrategy) {
    Objects.requireNonNull(paramAudioProductStrategy);
    try {
      boolean bool;
      int i = getService().removePreferredDeviceForStrategy(paramAudioProductStrategy.getId());
      if (i == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public AudioDeviceAttributes getPreferredDeviceForStrategy(AudioProductStrategy paramAudioProductStrategy) {
    Objects.requireNonNull(paramAudioProductStrategy);
    try {
      return getService().getPreferredDeviceForStrategy(paramAudioProductStrategy.getId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void addOnPreferredDeviceForStrategyChangedListener(Executor paramExecutor, OnPreferredDeviceForStrategyChangedListener paramOnPreferredDeviceForStrategyChangedListener) throws SecurityException {
    Objects.requireNonNull(paramExecutor);
    Objects.requireNonNull(paramOnPreferredDeviceForStrategyChangedListener);
    synchronized (this.mPrefDevListenerLock) {
      if (!hasPrefDevListener(paramOnPreferredDeviceForStrategyChangedListener)) {
        if (this.mPrefDevListeners == null) {
          ArrayList<PrefDevListenerInfo> arrayList1 = new ArrayList();
          this();
          this.mPrefDevListeners = arrayList1;
        } 
        int i = this.mPrefDevListeners.size();
        ArrayList<PrefDevListenerInfo> arrayList = this.mPrefDevListeners;
        PrefDevListenerInfo prefDevListenerInfo = new PrefDevListenerInfo();
        this(paramOnPreferredDeviceForStrategyChangedListener, paramExecutor);
        arrayList.add(prefDevListenerInfo);
        if (i == 0 && this.mPrefDevListeners.size() > 0) {
          if (this.mPrefDevDispatcherStub == null) {
            StrategyPreferredDeviceDispatcherStub strategyPreferredDeviceDispatcherStub = new StrategyPreferredDeviceDispatcherStub();
            this(this);
            this.mPrefDevDispatcherStub = strategyPreferredDeviceDispatcherStub;
          } 
          try {
            getService().registerStrategyPreferredDeviceDispatcher(this.mPrefDevDispatcherStub);
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          } 
        } 
        return;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("attempt to call addOnPreferredDeviceForStrategyChangedListener() on a previously registered listener");
      throw illegalArgumentException;
    } 
  }
  
  @SystemApi
  public void removeOnPreferredDeviceForStrategyChangedListener(OnPreferredDeviceForStrategyChangedListener paramOnPreferredDeviceForStrategyChangedListener) {
    Objects.requireNonNull(paramOnPreferredDeviceForStrategyChangedListener);
    synchronized (this.mPrefDevListenerLock) {
      if (removePrefDevListener(paramOnPreferredDeviceForStrategyChangedListener)) {
        int i = this.mPrefDevListeners.size();
        if (i == 0)
          try {
            getService().unregisterStrategyPreferredDeviceDispatcher(this.mPrefDevDispatcherStub);
            this.mPrefDevDispatcherStub = null;
            this.mPrefDevListeners = null;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          } finally {} 
        return;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("attempt to call removeOnPreferredDeviceForStrategyChangedListener() on an unregistered listener");
      throw illegalArgumentException;
    } 
  }
  
  private final Object mPrefDevListenerLock = new Object();
  
  class PrefDevListenerInfo {
    final Executor mExecutor;
    
    final AudioManager.OnPreferredDeviceForStrategyChangedListener mListener;
    
    PrefDevListenerInfo(AudioManager this$0, Executor param1Executor) {
      this.mListener = (AudioManager.OnPreferredDeviceForStrategyChangedListener)this$0;
      this.mExecutor = param1Executor;
    }
  }
  
  class StrategyPreferredDeviceDispatcherStub extends IStrategyPreferredDeviceDispatcher.Stub {
    final AudioManager this$0;
    
    private StrategyPreferredDeviceDispatcherStub() {}
    
    public void dispatchPrefDeviceChanged(int param1Int, AudioDeviceAttributes param1AudioDeviceAttributes) {
      synchronized (AudioManager.this.mPrefDevListenerLock) {
        if (AudioManager.this.mPrefDevListeners == null || AudioManager.this.mPrefDevListeners.size() == 0)
          return; 
        ArrayList arrayList = (ArrayList)AudioManager.this.mPrefDevListeners.clone();
        null = AudioProductStrategy.getAudioProductStrategyWithId(param1Int);
        long l = Binder.clearCallingIdentity();
        try {
          for (AudioManager.PrefDevListenerInfo prefDevListenerInfo : arrayList) {
            Executor executor = prefDevListenerInfo.mExecutor;
            _$$Lambda$AudioManager$StrategyPreferredDeviceDispatcherStub$7E5sSMD41PYjkWfIKdg4SkBnq7c _$$Lambda$AudioManager$StrategyPreferredDeviceDispatcherStub$7E5sSMD41PYjkWfIKdg4SkBnq7c = new _$$Lambda$AudioManager$StrategyPreferredDeviceDispatcherStub$7E5sSMD41PYjkWfIKdg4SkBnq7c();
            this(prefDevListenerInfo, (AudioProductStrategy)null, param1AudioDeviceAttributes);
            executor.execute(_$$Lambda$AudioManager$StrategyPreferredDeviceDispatcherStub$7E5sSMD41PYjkWfIKdg4SkBnq7c);
          } 
          return;
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
    }
  }
  
  private PrefDevListenerInfo getPrefDevListenerInfo(OnPreferredDeviceForStrategyChangedListener paramOnPreferredDeviceForStrategyChangedListener) {
    ArrayList<PrefDevListenerInfo> arrayList = this.mPrefDevListeners;
    if (arrayList == null)
      return null; 
    for (PrefDevListenerInfo prefDevListenerInfo : arrayList) {
      if (prefDevListenerInfo.mListener == paramOnPreferredDeviceForStrategyChangedListener)
        return prefDevListenerInfo; 
    } 
    return null;
  }
  
  private boolean hasPrefDevListener(OnPreferredDeviceForStrategyChangedListener paramOnPreferredDeviceForStrategyChangedListener) {
    boolean bool;
    if (getPrefDevListenerInfo(paramOnPreferredDeviceForStrategyChangedListener) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean removePrefDevListener(OnPreferredDeviceForStrategyChangedListener paramOnPreferredDeviceForStrategyChangedListener) {
    PrefDevListenerInfo prefDevListenerInfo = getPrefDevListenerInfo(paramOnPreferredDeviceForStrategyChangedListener);
    if (prefDevListenerInfo != null) {
      this.mPrefDevListeners.remove(prefDevListenerInfo);
      return true;
    } 
    return false;
  }
  
  public static boolean isOffloadedPlaybackSupported(AudioFormat paramAudioFormat, AudioAttributes paramAudioAttributes) {
    if (paramAudioFormat != null) {
      if (paramAudioAttributes != null)
        return AudioSystem.isOffloadSupported(paramAudioFormat, paramAudioAttributes); 
      throw new NullPointerException("Illegal null AudioAttributes");
    } 
    throw new NullPointerException("Illegal null AudioFormat");
  }
  
  public boolean isBluetoothScoAvailableOffCall() {
    IAudioService iAudioService = getService();
    if (iAudioService != null)
      try {
        boolean bool = iAudioService.isBluetoothScoAvailableOffCall();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("In isBluetoothScoAvailableOffCall(), calling appilication: ");
        Context context = this.mApplicationContext;
        stringBuilder.append(context.getOpPackageName());
        stringBuilder.append(", return value: ");
        stringBuilder.append(bool);
        String str = stringBuilder.toString();
        Log.i("AudioManager", str);
        return bool;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return true;
  }
  
  public void startBluetoothSco() {
    IAudioService iAudioService = getService();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("In startbluetoothSco(), calling application: ");
    Context context = this.mApplicationContext;
    stringBuilder.append(context.getOpPackageName());
    String str = stringBuilder.toString();
    Log.i("AudioManager", str);
    try {
      IBinder iBinder = this.mICallBack;
      int i = (getContext().getApplicationInfo()).targetSdkVersion;
      iAudioService.startBluetoothSco(iBinder, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void startBluetoothScoVirtualCall() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("In startBluetoothScoVirtualCall(), calling application: ");
    Context context = this.mApplicationContext;
    stringBuilder.append(context.getOpPackageName());
    String str = stringBuilder.toString();
    Log.i("AudioManager", str);
    IAudioService iAudioService = getService();
    try {
      iAudioService.startBluetoothScoVirtualCall(this.mICallBack);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void stopBluetoothSco() {
    IAudioService iAudioService = getService();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("In stopBluetoothSco(), calling application: ");
    Context context = this.mApplicationContext;
    stringBuilder.append(context.getOpPackageName());
    String str = stringBuilder.toString();
    Log.i("AudioManager", str);
    try {
      iAudioService.stopBluetoothSco(this.mICallBack);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setBluetoothScoOn(boolean paramBoolean) {
    IAudioService iAudioService = getService();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("In setBluetoothScoOn(), on: ");
    stringBuilder.append(paramBoolean);
    stringBuilder.append(", calling application: ");
    Context context = this.mApplicationContext;
    stringBuilder.append(context.getOpPackageName());
    String str = stringBuilder.toString();
    Log.i("AudioManager", str);
    try {
      if (!setBluetoothScoOnPermission())
        return; 
      iAudioService.setBluetoothScoOn(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isBluetoothScoOn() {
    IAudioService iAudioService = getService();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("In isBluetoothScoOn(), calling application: ");
    Context context = this.mApplicationContext;
    stringBuilder.append(context.getOpPackageName());
    String str = stringBuilder.toString();
    Log.i("AudioManager", str);
    try {
      return iAudioService.isBluetoothScoOn();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void setBluetoothA2dpOn(boolean paramBoolean) {}
  
  public boolean isBluetoothA2dpOn() {
    if (AudioSystem.getDeviceConnectionState(128, "") == 1)
      return true; 
    if (AudioSystem.getDeviceConnectionState(256, "") == 1)
      return true; 
    if (AudioSystem.getDeviceConnectionState(512, "") == 1)
      return true; 
    return false;
  }
  
  @Deprecated
  public void setWiredHeadsetOn(boolean paramBoolean) {}
  
  public boolean isWiredHeadsetOn() {
    if (AudioSystem.getDeviceConnectionState(4, "") == 0)
      if (AudioSystem.getDeviceConnectionState(8, "") == 0)
        if (AudioSystem.getDeviceConnectionState(67108864, "") == 0)
          return false;   
    return true;
  }
  
  public void setMicrophoneMute(boolean paramBoolean) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setMicrophoneMute on=");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(" from ");
      stringBuilder.append(getContext().getOpPackageName());
      String str = stringBuilder.toString();
      Log.d("AudioManager", str);
      setMicrophoneMutePermission();
      str = getContext().getOpPackageName();
      int i = UserHandle.getCallingUserId();
      iAudioService.setMicrophoneMute(paramBoolean, str, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setMicrophoneMuteFromSwitch(boolean paramBoolean) {
    IAudioService iAudioService = getService();
    try {
      iAudioService.setMicrophoneMuteFromSwitch(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isMicrophoneMute() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.isMicrophoneMuted();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setMode(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setMode mode=");
      stringBuilder.append(paramInt);
      stringBuilder.append(" from ");
      stringBuilder.append(getContext().getOpPackageName());
      String str = stringBuilder.toString();
      Log.d("AudioManager", str);
      iAudioService.setMode(paramInt, this.mICallBack, this.mApplicationContext.getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getMode() {
    IAudioService iAudioService = getService();
    try {
      int j, i = iAudioService.getMode();
      try {
        j = (getContext().getApplicationInfo()).targetSdkVersion;
      } catch (NullPointerException nullPointerException) {
        j = Build.VERSION.SDK_INT;
      } 
      int k = i;
      if (i == 4) {
        k = i;
        if (j <= 29)
          k = 2; 
      } 
      if (mDebugLog) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("getMode mode=");
        stringBuilder.append(k);
        Log.d("AudioManager", stringBuilder.toString());
      } 
      return k;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isCallScreeningModeSupported() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.isCallScreeningModeSupported();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void setRouting(int paramInt1, int paramInt2, int paramInt3) {}
  
  @Deprecated
  public int getRouting(int paramInt) {
    return -1;
  }
  
  public boolean isMusicActive() {
    return AudioSystem.isStreamActive(3, 0);
  }
  
  public boolean isMusicActiveRemotely() {
    return AudioSystem.isStreamActiveRemotely(3, 0);
  }
  
  public boolean isAudioFocusExclusive() {
    IAudioService iAudioService = getService();
    try {
      boolean bool;
      int i = iAudioService.getCurrentAudioFocus();
      if (i == 4) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int generateAudioSessionId() {
    int i = AudioSystem.newAudioSessionId();
    if (i > 0)
      return i; 
    Log.e("AudioManager", "Failure to generate a new audio session ID");
    return -1;
  }
  
  @Deprecated
  public void setParameter(String paramString1, String paramString2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append("=");
    stringBuilder.append(paramString2);
    setParameters(stringBuilder.toString());
  }
  
  public void setParameters(String paramString) {
    if (!setParametersPermission(paramString))
      return; 
    if (paramString.startsWith("OPLUS_AUDIO_SET_") || paramString.startsWith("DualHeadPh")) {
      IAudioService iAudioService = getService();
      try {
        iAudioService.setParameters(paramString);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    AudioSystem.setParameters((String)remoteException);
  }
  
  public String getParameters(String paramString) {
    if (paramString.startsWith("OPLUS_AUDIO_GET_")) {
      IAudioService iAudioService = getService();
      try {
        return iAudioService.getParameters(paramString);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    return AudioSystem.getParameters((String)remoteException);
  }
  
  public void playSoundEffect(int paramInt) {
    if (paramInt < 0 || paramInt >= 10)
      return; 
    if (!querySoundEffectsEnabled(Process.myUserHandle().getIdentifier()))
      return; 
    IAudioService iAudioService = getService();
    try {
      iAudioService.playSoundEffect(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void playSoundEffect(int paramInt1, int paramInt2) {
    if (paramInt1 < 0 || paramInt1 >= 10)
      return; 
    if (!querySoundEffectsEnabled(paramInt2))
      return; 
    IAudioService iAudioService = getService();
    try {
      if (mDebugLog) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("playSoundEffect effectType=");
        stringBuilder.append(paramInt1);
        stringBuilder.append(" userId=");
        stringBuilder.append(paramInt2);
        Log.d("AudioManager", stringBuilder.toString());
      } 
      iAudioService.playSoundEffect(paramInt1);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void playSoundEffect(int paramInt, float paramFloat) {
    if (paramInt < 0 || paramInt >= 10)
      return; 
    IAudioService iAudioService = getService();
    try {
      if (mDebugLog) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("playSoundEffect effectType=");
        stringBuilder.append(paramInt);
        stringBuilder.append(" volume=");
        stringBuilder.append(paramFloat);
        Log.d("AudioManager", stringBuilder.toString());
      } 
      iAudioService.playSoundEffectVolume(paramInt, paramFloat);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private boolean querySoundEffectsEnabled(int paramInt) {
    ContentResolver contentResolver = getContext().getContentResolver();
    boolean bool = false;
    if (Settings.System.getIntForUser(contentResolver, "sound_effects_enabled", 0, paramInt) != 0)
      bool = true; 
    return bool;
  }
  
  public void loadSoundEffects() {
    IAudioService iAudioService = getService();
    try {
      iAudioService.loadSoundEffects();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unloadSoundEffects() {
    IAudioService iAudioService = getService();
    try {
      iAudioService.unloadSoundEffects();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static String audioFocusToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("AUDIO_FOCUS_UNKNOWN(");
        stringBuilder.append(paramInt);
        stringBuilder.append(")");
        return stringBuilder.toString();
      case 4:
        return "AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE";
      case 3:
        return "AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK";
      case 2:
        return "AUDIOFOCUS_GAIN_TRANSIENT";
      case 1:
        return "AUDIOFOCUS_GAIN";
      case 0:
        return "AUDIOFOCUS_NONE";
      case -1:
        return "AUDIOFOCUS_LOSS";
      case -2:
        return "AUDIOFOCUS_LOSS_TRANSIENT";
      case -3:
        break;
    } 
    return "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK";
  }
  
  class FocusRequestInfo {
    final Handler mHandler;
    
    final AudioFocusRequest mRequest;
    
    FocusRequestInfo(AudioManager this$0, Handler param1Handler) {
      this.mRequest = (AudioFocusRequest)this$0;
      this.mHandler = param1Handler;
    }
  }
  
  private final ConcurrentHashMap<String, FocusRequestInfo> mAudioFocusIdListenerMap = new ConcurrentHashMap<>();
  
  private FocusRequestInfo findFocusRequestInfo(String paramString) {
    return this.mAudioFocusIdListenerMap.get(paramString);
  }
  
  private final ServiceEventHandlerDelegate mServiceEventHandlerDelegate = new ServiceEventHandlerDelegate(null);
  
  class ServiceEventHandlerDelegate {
    private final Handler mHandler;
    
    final AudioManager this$0;
    
    ServiceEventHandlerDelegate(Handler param1Handler) {
      Looper looper;
      if (param1Handler == null) {
        Looper looper1 = Looper.myLooper();
        if (looper1 == null)
          looper = Looper.getMainLooper(); 
      } else {
        looper = looper.getLooper();
      } 
      if (looper != null) {
        this.mHandler = new Handler(looper) {
            final AudioManager.ServiceEventHandlerDelegate this$1;
            
            final AudioManager val$this$0;
            
            Handler(Looper param1Looper) {
              super(param1Looper);
            }
            
            public void handleMessage(Message param1Message) {
              AudioManager.RecordConfigChangeCallbackData recordConfigChangeCallbackData;
              int i = param1Message.what;
              if (i != 0) {
                AudioManager.PlaybackConfigChangeCallbackData playbackConfigChangeCallbackData;
                if (i != 1) {
                  if (i != 2) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Unknown event ");
                    stringBuilder.append(param1Message.what);
                    Log.e("AudioManager", stringBuilder.toString());
                  } else {
                    playbackConfigChangeCallbackData = (AudioManager.PlaybackConfigChangeCallbackData)param1Message.obj;
                    if (playbackConfigChangeCallbackData.mCb != null)
                      playbackConfigChangeCallbackData.mCb.onPlaybackConfigChanged(playbackConfigChangeCallbackData.mConfigs); 
                  } 
                } else {
                  recordConfigChangeCallbackData = (AudioManager.RecordConfigChangeCallbackData)((Message)playbackConfigChangeCallbackData).obj;
                  if (recordConfigChangeCallbackData.mCb != null)
                    recordConfigChangeCallbackData.mCb.onRecordingConfigChanged(recordConfigChangeCallbackData.mConfigs); 
                } 
              } else {
                AudioManager.FocusRequestInfo focusRequestInfo = AudioManager.this.findFocusRequestInfo((String)((Message)recordConfigChangeCallbackData).obj);
                if (focusRequestInfo != null) {
                  AudioFocusRequest audioFocusRequest = focusRequestInfo.mRequest;
                  AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = audioFocusRequest.getOnAudioFocusChangeListener();
                  if (onAudioFocusChangeListener != null) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("dispatching onAudioFocusChange(");
                    stringBuilder.append(((Message)recordConfigChangeCallbackData).arg1);
                    stringBuilder.append(") to ");
                    stringBuilder.append(((Message)recordConfigChangeCallbackData).obj);
                    Log.d("AudioManager", stringBuilder.toString());
                    onAudioFocusChangeListener.onAudioFocusChange(((Message)recordConfigChangeCallbackData).arg1);
                  } 
                } 
              } 
            }
          };
      } else {
        this.mHandler = null;
      } 
    }
    
    Handler getHandler() {
      return this.mHandler;
    }
  }
  
  class null extends Handler {
    final AudioManager.ServiceEventHandlerDelegate this$1;
    
    final AudioManager val$this$0;
    
    null(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      AudioManager.RecordConfigChangeCallbackData recordConfigChangeCallbackData;
      int i = param1Message.what;
      if (i != 0) {
        AudioManager.PlaybackConfigChangeCallbackData playbackConfigChangeCallbackData;
        if (i != 1) {
          if (i != 2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown event ");
            stringBuilder.append(param1Message.what);
            Log.e("AudioManager", stringBuilder.toString());
          } else {
            playbackConfigChangeCallbackData = (AudioManager.PlaybackConfigChangeCallbackData)param1Message.obj;
            if (playbackConfigChangeCallbackData.mCb != null)
              playbackConfigChangeCallbackData.mCb.onPlaybackConfigChanged(playbackConfigChangeCallbackData.mConfigs); 
          } 
        } else {
          recordConfigChangeCallbackData = (AudioManager.RecordConfigChangeCallbackData)((Message)playbackConfigChangeCallbackData).obj;
          if (recordConfigChangeCallbackData.mCb != null)
            recordConfigChangeCallbackData.mCb.onRecordingConfigChanged(recordConfigChangeCallbackData.mConfigs); 
        } 
      } else {
        AudioManager.FocusRequestInfo focusRequestInfo = AudioManager.this.findFocusRequestInfo((String)((Message)recordConfigChangeCallbackData).obj);
        if (focusRequestInfo != null) {
          AudioFocusRequest audioFocusRequest = focusRequestInfo.mRequest;
          AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = audioFocusRequest.getOnAudioFocusChangeListener();
          if (onAudioFocusChangeListener != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("dispatching onAudioFocusChange(");
            stringBuilder.append(((Message)recordConfigChangeCallbackData).arg1);
            stringBuilder.append(") to ");
            stringBuilder.append(((Message)recordConfigChangeCallbackData).obj);
            Log.d("AudioManager", stringBuilder.toString());
            onAudioFocusChangeListener.onAudioFocusChange(((Message)recordConfigChangeCallbackData).arg1);
          } 
        } 
      } 
    }
  }
  
  private final IAudioFocusDispatcher mAudioFocusDispatcher = (IAudioFocusDispatcher)new Object(this);
  
  private String getIdForAudioFocusListener(OnAudioFocusChangeListener paramOnAudioFocusChangeListener) {
    if (paramOnAudioFocusChangeListener == null)
      return new String(toString()); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(toString());
    stringBuilder.append(paramOnAudioFocusChangeListener.toString());
    return new String(stringBuilder.toString());
  }
  
  public void registerAudioFocusRequest(AudioFocusRequest paramAudioFocusRequest) {
    Handler handler = paramAudioFocusRequest.getOnAudioFocusChangeListenerHandler();
    if (handler == null) {
      handler = null;
    } else {
      handler = (new ServiceEventHandlerDelegate(handler)).getHandler();
    } 
    FocusRequestInfo focusRequestInfo = new FocusRequestInfo(paramAudioFocusRequest, handler);
    String str = getIdForAudioFocusListener(paramAudioFocusRequest.getOnAudioFocusChangeListener());
    this.mAudioFocusIdListenerMap.put(str, focusRequestInfo);
  }
  
  public void unregisterAudioFocusRequest(OnAudioFocusChangeListener paramOnAudioFocusChangeListener) {
    this.mAudioFocusIdListenerMap.remove(getIdForAudioFocusListener(paramOnAudioFocusChangeListener));
  }
  
  private final Object mFocusRequestsLock = new Object();
  
  public int requestAudioFocus(OnAudioFocusChangeListener paramOnAudioFocusChangeListener, int paramInt1, int paramInt2) {
    PlayerBase.deprecateStreamTypeForPlayback(paramInt1, "AudioManager", "requestAudioFocus()");
    boolean bool = false;
    try {
      AudioAttributes.Builder builder = new AudioAttributes.Builder();
      this();
      AudioAttributes audioAttributes = builder.setInternalLegacyStreamType(paramInt1).build();
      paramInt1 = requestAudioFocus(paramOnAudioFocusChangeListener, audioAttributes, paramInt2, 0);
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.e("AudioManager", "Audio focus request denied due to ", illegalArgumentException);
      paramInt1 = bool;
    } 
    return paramInt1;
  }
  
  public int requestAudioFocus(AudioFocusRequest paramAudioFocusRequest) {
    return requestAudioFocus(paramAudioFocusRequest, (AudioPolicy)null);
  }
  
  public int abandonAudioFocusRequest(AudioFocusRequest paramAudioFocusRequest) {
    if (paramAudioFocusRequest != null) {
      OnAudioFocusChangeListener onAudioFocusChangeListener = paramAudioFocusRequest.getOnAudioFocusChangeListener();
      AudioAttributes audioAttributes = paramAudioFocusRequest.getAudioAttributes();
      return abandonAudioFocus(onAudioFocusChangeListener, audioAttributes);
    } 
    throw new IllegalArgumentException("Illegal null AudioFocusRequest");
  }
  
  @SystemApi
  public int requestAudioFocus(OnAudioFocusChangeListener paramOnAudioFocusChangeListener, AudioAttributes paramAudioAttributes, int paramInt1, int paramInt2) throws IllegalArgumentException {
    if (paramInt2 == (paramInt2 & 0x3))
      return requestAudioFocus(paramOnAudioFocusChangeListener, paramAudioAttributes, paramInt1, paramInt2 & 0x3, (AudioPolicy)null); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid flags 0x");
    stringBuilder.append(Integer.toHexString(paramInt2).toUpperCase());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  @SystemApi
  public int requestAudioFocus(OnAudioFocusChangeListener paramOnAudioFocusChangeListener, AudioAttributes paramAudioAttributes, int paramInt1, int paramInt2, AudioPolicy paramAudioPolicy) throws IllegalArgumentException {
    if (paramAudioAttributes != null) {
      if (AudioFocusRequest.isValidFocusGain(paramInt1)) {
        if (paramInt2 == (paramInt2 & 0x7)) {
          boolean bool = true;
          if ((paramInt2 & 0x1) != 1 || paramOnAudioFocusChangeListener != null) {
            if ((paramInt2 & 0x2) != 2 || paramOnAudioFocusChangeListener != null) {
              if ((paramInt2 & 0x4) != 4 || paramAudioPolicy != null) {
                boolean bool1;
                AudioFocusRequest.Builder builder2 = new AudioFocusRequest.Builder(paramInt1);
                AudioFocusRequest.Builder builder1 = builder2.setOnAudioFocusChangeListenerInt(paramOnAudioFocusChangeListener, null);
                builder1 = builder1.setAudioAttributes(paramAudioAttributes);
                if ((paramInt2 & 0x1) == 1) {
                  bool1 = true;
                } else {
                  bool1 = false;
                } 
                builder1 = builder1.setAcceptsDelayedFocusGain(bool1);
                if ((paramInt2 & 0x2) == 2) {
                  bool1 = true;
                } else {
                  bool1 = false;
                } 
                builder1 = builder1.setWillPauseWhenDucked(bool1);
                if ((paramInt2 & 0x4) == 4) {
                  bool1 = bool;
                } else {
                  bool1 = false;
                } 
                builder1 = builder1.setLocksFocus(bool1);
                AudioFocusRequest audioFocusRequest = builder1.build();
                return requestAudioFocus(audioFocusRequest, paramAudioPolicy);
              } 
              throw new IllegalArgumentException("Illegal null audio policy when locking audio focus");
            } 
            throw new IllegalArgumentException("Illegal null focus listener when flagged as pausing instead of ducking");
          } 
          throw new IllegalArgumentException("Illegal null focus listener when flagged as accepting delayed focus grant");
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Illegal flags 0x");
        stringBuilder.append(Integer.toHexString(paramInt2).toUpperCase());
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new IllegalArgumentException("Invalid duration hint");
    } 
    throw new IllegalArgumentException("Illegal null AudioAttributes argument");
  }
  
  @SystemApi
  public int requestAudioFocus(AudioFocusRequest paramAudioFocusRequest, AudioPolicy paramAudioPolicy) {
    if (paramAudioFocusRequest != null) {
      if (!paramAudioFocusRequest.locksFocus() || paramAudioPolicy != null) {
        int i;
        registerAudioFocusRequest(paramAudioFocusRequest);
        IAudioService iAudioService = getService();
        try {
          i = (getContext().getApplicationInfo()).targetSdkVersion;
        } catch (NullPointerException nullPointerException) {
          i = Build.VERSION.SDK_INT;
        } 
        String str = getIdForAudioFocusListener(paramAudioFocusRequest.getOnAudioFocusChangeListener());
        Object object = this.mFocusRequestsLock;
        /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        try {
          AudioAttributes audioAttributes = paramAudioFocusRequest.getAudioAttributes();
          int j = paramAudioFocusRequest.getFocusGain();
          IBinder iBinder = this.mICallBack;
          IAudioFocusDispatcher iAudioFocusDispatcher = this.mAudioFocusDispatcher;
          String str1 = getContext().getOpPackageName();
          int k = paramAudioFocusRequest.getFlags();
          if (paramAudioPolicy != null) {
            IAudioPolicyCallback iAudioPolicyCallback = paramAudioPolicy.cb();
          } else {
            paramAudioFocusRequest = null;
          } 
          i = iAudioService.requestAudioFocus(audioAttributes, j, iBinder, iAudioFocusDispatcher, str, str1, k, (IAudioPolicyCallback)paramAudioFocusRequest, i);
          if (i != 100) {
            /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
            return i;
          } 
          if (this.mFocusRequestsAwaitingResult == null) {
            HashMap<Object, Object> hashMap = new HashMap<>();
            this(1);
            this.mFocusRequestsAwaitingResult = (HashMap)hashMap;
          } 
          null = new BlockingFocusResultReceiver();
          this(str);
          this.mFocusRequestsAwaitingResult.put(str, null);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          null.waitForResult(200L);
          synchronized (this.mFocusRequestsLock) {
            this.mFocusRequestsAwaitingResult.remove(str);
            return null.requestResult();
          } 
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } finally {}
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        throw paramAudioFocusRequest;
      } 
      throw new IllegalArgumentException("Illegal null audio policy when locking audio focus");
    } 
    throw new NullPointerException("Illegal null AudioFocusRequest");
  }
  
  class SafeWaitObject {
    private SafeWaitObject() {}
    
    private boolean mQuit = false;
    
    public void safeNotify() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iconst_1
      //   4: putfield mQuit : Z
      //   7: aload_0
      //   8: invokevirtual notify : ()V
      //   11: aload_0
      //   12: monitorexit
      //   13: return
      //   14: astore_1
      //   15: aload_0
      //   16: monitorexit
      //   17: aload_1
      //   18: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3583	-> 0
      //   #3584	-> 2
      //   #3585	-> 7
      //   #3586	-> 11
      //   #3587	-> 13
      //   #3586	-> 14
      // Exception table:
      //   from	to	target	type
      //   2	7	14	finally
      //   7	11	14	finally
      //   11	13	14	finally
      //   15	17	14	finally
    }
    
    public void safeWait(long param1Long) throws InterruptedException {
      // Byte code:
      //   0: invokestatic currentTimeMillis : ()J
      //   3: lstore_3
      //   4: aload_0
      //   5: monitorenter
      //   6: aload_0
      //   7: getfield mQuit : Z
      //   10: ifne -> 41
      //   13: lload_3
      //   14: lload_1
      //   15: ladd
      //   16: invokestatic currentTimeMillis : ()J
      //   19: lsub
      //   20: lstore #5
      //   22: lload #5
      //   24: lconst_0
      //   25: lcmp
      //   26: ifge -> 32
      //   29: goto -> 41
      //   32: aload_0
      //   33: lload #5
      //   35: invokevirtual wait : (J)V
      //   38: goto -> 6
      //   41: aload_0
      //   42: monitorexit
      //   43: return
      //   44: astore #7
      //   46: aload_0
      //   47: monitorexit
      //   48: aload #7
      //   50: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3590	-> 0
      //   #3591	-> 4
      //   #3592	-> 6
      //   #3593	-> 13
      //   #3594	-> 22
      //   #3595	-> 32
      //   #3596	-> 38
      //   #3597	-> 41
      //   #3598	-> 43
      //   #3597	-> 44
      // Exception table:
      //   from	to	target	type
      //   6	13	44	finally
      //   13	22	44	finally
      //   32	38	44	finally
      //   41	43	44	finally
      //   46	48	44	finally
    }
  }
  
  class BlockingFocusResultReceiver {
    private final AudioManager.SafeWaitObject mLock = new AudioManager.SafeWaitObject();
    
    private boolean mResultReceived = false;
    
    private int mFocusRequestResult = 0;
    
    private final String mFocusClientId;
    
    BlockingFocusResultReceiver(AudioManager this$0) {
      this.mFocusClientId = (String)this$0;
    }
    
    boolean receivedResult() {
      return this.mResultReceived;
    }
    
    int requestResult() {
      return this.mFocusRequestResult;
    }
    
    void notifyResult(int param1Int) {
      synchronized (this.mLock) {
        this.mResultReceived = true;
        this.mFocusRequestResult = param1Int;
        this.mLock.safeNotify();
        return;
      } 
    }
    
    public void waitForResult(long param1Long) {
      synchronized (this.mLock) {
        if (this.mResultReceived)
          return; 
        try {
          this.mLock.safeWait(param1Long);
        } catch (InterruptedException interruptedException) {}
        return;
      } 
    }
  }
  
  public void requestAudioFocusForCall(int paramInt1, int paramInt2) {
    IAudioService iAudioService = getService();
    try {
      AudioAttributes.Builder builder = new AudioAttributes.Builder();
      this();
      AudioAttributes audioAttributes = builder.setInternalLegacyStreamType(paramInt1).build();
      IBinder iBinder = this.mICallBack;
      String str = getContext().getOpPackageName();
      iAudioService.requestAudioFocus(audioAttributes, paramInt2, iBinder, null, "AudioFocus_For_Phone_Ring_And_Calls", str, 4, null, 0);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getFocusRampTimeMs(int paramInt, AudioAttributes paramAudioAttributes) {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getFocusRampTimeMs(paramInt, paramAudioAttributes);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setFocusRequestResult(AudioFocusInfo paramAudioFocusInfo, int paramInt, AudioPolicy paramAudioPolicy) {
    if (paramAudioFocusInfo != null) {
      if (paramAudioPolicy != null) {
        IAudioService iAudioService = getService();
        try {
          iAudioService.setFocusRequestResultFromExtPolicy(paramAudioFocusInfo, paramInt, paramAudioPolicy.cb());
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
      throw new IllegalArgumentException("Illegal null AudioPolicy");
    } 
    throw new IllegalArgumentException("Illegal null AudioFocusInfo");
  }
  
  @SystemApi
  public int dispatchAudioFocusChange(AudioFocusInfo paramAudioFocusInfo, int paramInt, AudioPolicy paramAudioPolicy) {
    if (paramAudioFocusInfo != null) {
      if (paramAudioPolicy != null) {
        IAudioService iAudioService = getService();
        try {
          return iAudioService.dispatchFocusChange(paramAudioFocusInfo, paramInt, paramAudioPolicy.cb());
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
      throw new NullPointerException("Illegal null AudioPolicy");
    } 
    throw new NullPointerException("Illegal null AudioFocusInfo");
  }
  
  public void abandonAudioFocusForCall() {
    IAudioService iAudioService = getService();
    try {
      String str = getContext().getOpPackageName();
      iAudioService.abandonAudioFocus(null, "AudioFocus_For_Phone_Ring_And_Calls", null, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int abandonAudioFocus(OnAudioFocusChangeListener paramOnAudioFocusChangeListener) {
    return abandonAudioFocus(paramOnAudioFocusChangeListener, (AudioAttributes)null);
  }
  
  @SystemApi
  public int abandonAudioFocus(OnAudioFocusChangeListener paramOnAudioFocusChangeListener, AudioAttributes paramAudioAttributes) {
    unregisterAudioFocusRequest(paramOnAudioFocusChangeListener);
    IAudioService iAudioService = getService();
    try {
      IAudioFocusDispatcher iAudioFocusDispatcher = this.mAudioFocusDispatcher;
      String str1 = getIdForAudioFocusListener(paramOnAudioFocusChangeListener), str2 = getContext().getOpPackageName();
      return iAudioService.abandonAudioFocus(iAudioFocusDispatcher, str1, paramAudioAttributes, str2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void registerMediaButtonEventReceiver(ComponentName paramComponentName) {
    if (paramComponentName == null)
      return; 
    if (!paramComponentName.getPackageName().equals(getContext().getPackageName())) {
      Log.e("AudioManager", "registerMediaButtonEventReceiver() error: receiver and context package names don't match");
      return;
    } 
    Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
    intent.setComponent(paramComponentName);
    PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 0, intent, 0);
    registerMediaButtonIntent(pendingIntent, paramComponentName);
  }
  
  @Deprecated
  public void registerMediaButtonEventReceiver(PendingIntent paramPendingIntent) {
    if (paramPendingIntent == null)
      return; 
    registerMediaButtonIntent(paramPendingIntent, (ComponentName)null);
  }
  
  public void registerMediaButtonIntent(PendingIntent paramPendingIntent, ComponentName paramComponentName) {
    if (paramPendingIntent == null) {
      Log.e("AudioManager", "Cannot call registerMediaButtonIntent() with a null parameter");
      return;
    } 
    MediaSessionLegacyHelper mediaSessionLegacyHelper = MediaSessionLegacyHelper.getHelper(getContext());
    mediaSessionLegacyHelper.addMediaButtonListener(paramPendingIntent, paramComponentName, getContext());
  }
  
  @Deprecated
  public void unregisterMediaButtonEventReceiver(ComponentName paramComponentName) {
    if (paramComponentName == null)
      return; 
    Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
    intent.setComponent(paramComponentName);
    PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 0, intent, 0);
    unregisterMediaButtonIntent(pendingIntent);
  }
  
  @Deprecated
  public void unregisterMediaButtonEventReceiver(PendingIntent paramPendingIntent) {
    if (paramPendingIntent == null)
      return; 
    unregisterMediaButtonIntent(paramPendingIntent);
  }
  
  public void unregisterMediaButtonIntent(PendingIntent paramPendingIntent) {
    MediaSessionLegacyHelper mediaSessionLegacyHelper = MediaSessionLegacyHelper.getHelper(getContext());
    mediaSessionLegacyHelper.removeMediaButtonListener(paramPendingIntent);
  }
  
  @Deprecated
  public void registerRemoteControlClient(RemoteControlClient paramRemoteControlClient) {
    if (paramRemoteControlClient == null || paramRemoteControlClient.getRcMediaIntent() == null)
      return; 
    paramRemoteControlClient.registerWithSession(MediaSessionLegacyHelper.getHelper(getContext()));
  }
  
  @Deprecated
  public void unregisterRemoteControlClient(RemoteControlClient paramRemoteControlClient) {
    if (paramRemoteControlClient == null || paramRemoteControlClient.getRcMediaIntent() == null)
      return; 
    paramRemoteControlClient.unregisterWithSession(MediaSessionLegacyHelper.getHelper(getContext()));
  }
  
  @Deprecated
  public boolean registerRemoteController(RemoteController paramRemoteController) {
    if (paramRemoteController == null)
      return false; 
    paramRemoteController.startListeningToSessions();
    return true;
  }
  
  @Deprecated
  public void unregisterRemoteController(RemoteController paramRemoteController) {
    if (paramRemoteController == null)
      return; 
    paramRemoteController.stopListeningToSessions();
  }
  
  @SystemApi
  public int registerAudioPolicy(AudioPolicy paramAudioPolicy) {
    return registerAudioPolicyStatic(paramAudioPolicy);
  }
  
  static int registerAudioPolicyStatic(AudioPolicy paramAudioPolicy) {
    if (paramAudioPolicy != null) {
      IAudioService iAudioService = getService();
      try {
        IMediaProjection iMediaProjection;
        MediaProjection mediaProjection = paramAudioPolicy.getMediaProjection();
        AudioPolicyConfig audioPolicyConfig = paramAudioPolicy.getConfig();
        IAudioPolicyCallback iAudioPolicyCallback = paramAudioPolicy.cb();
        boolean bool1 = paramAudioPolicy.hasFocusListener(), bool2 = paramAudioPolicy.isFocusPolicy(), bool3 = paramAudioPolicy.isTestFocusPolicy();
        boolean bool4 = paramAudioPolicy.isVolumeController();
        if (mediaProjection == null) {
          mediaProjection = null;
        } else {
          iMediaProjection = mediaProjection.getProjection();
        } 
        String str = iAudioService.registerAudioPolicy(audioPolicyConfig, iAudioPolicyCallback, bool1, bool2, bool3, bool4, iMediaProjection);
        if (str == null)
          return -1; 
        paramAudioPolicy.setRegistration(str);
        return 0;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    throw new IllegalArgumentException("Illegal null AudioPolicy argument");
  }
  
  @SystemApi
  public void unregisterAudioPolicyAsync(AudioPolicy paramAudioPolicy) {
    unregisterAudioPolicyAsyncStatic(paramAudioPolicy);
  }
  
  static void unregisterAudioPolicyAsyncStatic(AudioPolicy paramAudioPolicy) {
    if (paramAudioPolicy != null) {
      IAudioService iAudioService = getService();
      try {
        iAudioService.unregisterAudioPolicyAsync(paramAudioPolicy.cb());
        paramAudioPolicy.setRegistration(null);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    throw new IllegalArgumentException("Illegal null AudioPolicy argument");
  }
  
  @SystemApi
  public void unregisterAudioPolicy(AudioPolicy paramAudioPolicy) {
    Preconditions.checkNotNull(paramAudioPolicy, "Illegal null AudioPolicy argument");
    IAudioService iAudioService = getService();
    try {
      paramAudioPolicy.invalidateCaptorsAndInjectors();
      iAudioService.unregisterAudioPolicy(paramAudioPolicy.cb());
      paramAudioPolicy.setRegistration(null);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasRegisteredDynamicPolicy() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.hasRegisteredDynamicPolicy();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class AudioPlaybackCallback {
    public void onPlaybackConfigChanged(List<AudioPlaybackConfiguration> param1List) {}
  }
  
  class AudioPlaybackCallbackInfo {
    final AudioManager.AudioPlaybackCallback mCb;
    
    final Handler mHandler;
    
    AudioPlaybackCallbackInfo(AudioManager this$0, Handler param1Handler) {
      this.mCb = (AudioManager.AudioPlaybackCallback)this$0;
      this.mHandler = param1Handler;
    }
  }
  
  class PlaybackConfigChangeCallbackData {
    final AudioManager.AudioPlaybackCallback mCb;
    
    final List<AudioPlaybackConfiguration> mConfigs;
    
    PlaybackConfigChangeCallbackData(AudioManager this$0, List<AudioPlaybackConfiguration> param1List) {
      this.mCb = (AudioManager.AudioPlaybackCallback)this$0;
      this.mConfigs = param1List;
    }
  }
  
  public void registerAudioPlaybackCallback(AudioPlaybackCallback paramAudioPlaybackCallback, Handler paramHandler) {
    if (paramAudioPlaybackCallback != null)
      synchronized (this.mPlaybackCallbackLock) {
        if (this.mPlaybackCallbackList == null) {
          ArrayList<AudioPlaybackCallbackInfo> arrayList = new ArrayList();
          this();
          this.mPlaybackCallbackList = arrayList;
        } 
        int i = this.mPlaybackCallbackList.size();
        if (!hasPlaybackCallback_sync(paramAudioPlaybackCallback)) {
          List<AudioPlaybackCallbackInfo> list = this.mPlaybackCallbackList;
          AudioPlaybackCallbackInfo audioPlaybackCallbackInfo = new AudioPlaybackCallbackInfo();
          ServiceEventHandlerDelegate serviceEventHandlerDelegate = new ServiceEventHandlerDelegate();
          this(this, paramHandler);
          this(paramAudioPlaybackCallback, serviceEventHandlerDelegate.getHandler());
          list.add(audioPlaybackCallbackInfo);
          int j = this.mPlaybackCallbackList.size();
          if (i == 0 && j > 0)
            try {
              getService().registerPlaybackCallback(this.mPlayCb);
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            }  
        } else {
          Log.w("AudioManager", "attempt to call registerAudioPlaybackCallback() on a previouslyregistered callback");
        } 
        return;
      }  
    throw new IllegalArgumentException("Illegal null AudioPlaybackCallback argument");
  }
  
  public void unregisterAudioPlaybackCallback(AudioPlaybackCallback paramAudioPlaybackCallback) {
    if (paramAudioPlaybackCallback != null)
      synchronized (this.mPlaybackCallbackLock) {
        if (this.mPlaybackCallbackList == null) {
          Log.w("AudioManager", "attempt to call unregisterAudioPlaybackCallback() on a callback that was never registered");
          return;
        } 
        int i = this.mPlaybackCallbackList.size();
        if (removePlaybackCallback_sync(paramAudioPlaybackCallback)) {
          int j = this.mPlaybackCallbackList.size();
          if (i > 0 && j == 0)
            try {
              getService().unregisterPlaybackCallback(this.mPlayCb);
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            }  
        } else {
          Log.w("AudioManager", "attempt to call unregisterAudioPlaybackCallback() on a callback already unregistered or never registered");
        } 
        return;
      }  
    throw new IllegalArgumentException("Illegal null AudioPlaybackCallback argument");
  }
  
  public List<AudioPlaybackConfiguration> getActivePlaybackConfigurations() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getActivePlaybackConfigurations();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private final Object mPlaybackCallbackLock = new Object();
  
  private boolean hasPlaybackCallback_sync(AudioPlaybackCallback paramAudioPlaybackCallback) {
    if (this.mPlaybackCallbackList != null)
      for (byte b = 0; b < this.mPlaybackCallbackList.size(); b++) {
        if (paramAudioPlaybackCallback.equals(((AudioPlaybackCallbackInfo)this.mPlaybackCallbackList.get(b)).mCb))
          return true; 
      }  
    return false;
  }
  
  private boolean removePlaybackCallback_sync(AudioPlaybackCallback paramAudioPlaybackCallback) {
    if (this.mPlaybackCallbackList != null)
      for (byte b = 0; b < this.mPlaybackCallbackList.size(); b++) {
        if (paramAudioPlaybackCallback.equals(((AudioPlaybackCallbackInfo)this.mPlaybackCallbackList.get(b)).mCb)) {
          this.mPlaybackCallbackList.remove(b);
          return true;
        } 
      }  
    return false;
  }
  
  private final IPlaybackConfigDispatcher mPlayCb = (IPlaybackConfigDispatcher)new Object(this);
  
  class AudioRecordingCallback {
    public void onRecordingConfigChanged(List<AudioRecordingConfiguration> param1List) {}
  }
  
  class AudioRecordingCallbackInfo {
    final AudioManager.AudioRecordingCallback mCb;
    
    final Handler mHandler;
    
    AudioRecordingCallbackInfo(AudioManager this$0, Handler param1Handler) {
      this.mCb = (AudioManager.AudioRecordingCallback)this$0;
      this.mHandler = param1Handler;
    }
  }
  
  class RecordConfigChangeCallbackData {
    final AudioManager.AudioRecordingCallback mCb;
    
    final List<AudioRecordingConfiguration> mConfigs;
    
    RecordConfigChangeCallbackData(AudioManager this$0, List<AudioRecordingConfiguration> param1List) {
      this.mCb = (AudioManager.AudioRecordingCallback)this$0;
      this.mConfigs = param1List;
    }
  }
  
  public void registerAudioRecordingCallback(AudioRecordingCallback paramAudioRecordingCallback, Handler paramHandler) {
    if (paramAudioRecordingCallback != null)
      synchronized (this.mRecordCallbackLock) {
        if (this.mRecordCallbackList == null) {
          ArrayList<AudioRecordingCallbackInfo> arrayList = new ArrayList();
          this();
          this.mRecordCallbackList = arrayList;
        } 
        int i = this.mRecordCallbackList.size();
        if (!hasRecordCallback_sync(paramAudioRecordingCallback)) {
          List<AudioRecordingCallbackInfo> list = this.mRecordCallbackList;
          AudioRecordingCallbackInfo audioRecordingCallbackInfo = new AudioRecordingCallbackInfo();
          ServiceEventHandlerDelegate serviceEventHandlerDelegate = new ServiceEventHandlerDelegate();
          this(this, paramHandler);
          this(paramAudioRecordingCallback, serviceEventHandlerDelegate.getHandler());
          list.add(audioRecordingCallbackInfo);
          int j = this.mRecordCallbackList.size();
          if (i == 0 && j > 0) {
            IAudioService iAudioService = getService();
            try {
              iAudioService.registerRecordingCallback(this.mRecCb);
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            } 
          } 
        } else {
          Log.w("AudioManager", "attempt to call registerAudioRecordingCallback() on a previouslyregistered callback");
        } 
        return;
      }  
    throw new IllegalArgumentException("Illegal null AudioRecordingCallback argument");
  }
  
  public void unregisterAudioRecordingCallback(AudioRecordingCallback paramAudioRecordingCallback) {
    if (paramAudioRecordingCallback != null)
      synchronized (this.mRecordCallbackLock) {
        if (this.mRecordCallbackList == null)
          return; 
        int i = this.mRecordCallbackList.size();
        if (removeRecordCallback_sync(paramAudioRecordingCallback)) {
          int j = this.mRecordCallbackList.size();
          if (i > 0 && j == 0) {
            IAudioService iAudioService = getService();
            try {
              iAudioService.unregisterRecordingCallback(this.mRecCb);
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            } 
          } 
        } else {
          Log.w("AudioManager", "attempt to call unregisterAudioRecordingCallback() on a callback already unregistered or never registered");
        } 
        return;
      }  
    throw new IllegalArgumentException("Illegal null AudioRecordingCallback argument");
  }
  
  public List<AudioRecordingConfiguration> getActiveRecordingConfigurations() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getActiveRecordingConfigurations();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private final Object mRecordCallbackLock = new Object();
  
  private boolean hasRecordCallback_sync(AudioRecordingCallback paramAudioRecordingCallback) {
    if (this.mRecordCallbackList != null)
      for (byte b = 0; b < this.mRecordCallbackList.size(); b++) {
        if (paramAudioRecordingCallback.equals(((AudioRecordingCallbackInfo)this.mRecordCallbackList.get(b)).mCb))
          return true; 
      }  
    return false;
  }
  
  private boolean removeRecordCallback_sync(AudioRecordingCallback paramAudioRecordingCallback) {
    if (this.mRecordCallbackList != null)
      for (byte b = 0; b < this.mRecordCallbackList.size(); b++) {
        if (paramAudioRecordingCallback.equals(((AudioRecordingCallbackInfo)this.mRecordCallbackList.get(b)).mCb)) {
          this.mRecordCallbackList.remove(b);
          return true;
        } 
      }  
    return false;
  }
  
  private final IRecordingConfigDispatcher mRecCb = (IRecordingConfigDispatcher)new Object(this);
  
  public void reloadAudioSettings() {
    IAudioService iAudioService = getService();
    try {
      iAudioService.reloadAudioSettings();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void avrcpSupportsAbsoluteVolume(String paramString, boolean paramBoolean) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("avrcpSupportsAbsoluteVolume support=");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(" from ");
      stringBuilder.append(getContext().getOpPackageName());
      String str = stringBuilder.toString();
      Log.d("AudioManager", str);
      iAudioService.avrcpSupportsAbsoluteVolume(paramString, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private final IBinder mICallBack = new Binder();
  
  public boolean isSilentMode() {
    int i = getRingerMode();
    boolean bool1 = true, bool2 = bool1;
    if (i != 0)
      if (i == 1) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public static boolean isOutputDevice(int paramInt) {
    boolean bool;
    if ((Integer.MIN_VALUE & paramInt) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isInputDevice(int paramInt) {
    boolean bool;
    if ((paramInt & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getDevicesForStream(int paramInt) {
    if (paramInt != 0 && paramInt != 1 && paramInt != 2 && paramInt != 3 && paramInt != 4 && paramInt != 5 && paramInt != 8 && paramInt != 10)
      return 0; 
    return AudioSystem.getDevicesForStream(paramInt);
  }
  
  @SystemApi
  public List<AudioDeviceAttributes> getDevicesForAttributes(AudioAttributes paramAudioAttributes) {
    Objects.requireNonNull(paramAudioAttributes);
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getDevicesForAttributes(paramAudioAttributes);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void enforceValidVolumeBehavior(int paramInt) {
    if (paramInt == 0 || paramInt == 1 || paramInt == 2 || paramInt == 3 || paramInt == 4)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal volume behavior ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setDeviceVolumeBehavior(int paramInt1, String paramString, int paramInt2) {
    setDeviceVolumeBehavior(new AudioDeviceAttributes(2, paramInt1, paramString), paramInt2);
  }
  
  public void setDeviceVolumeBehavior(AudioDeviceAttributes paramAudioDeviceAttributes, int paramInt) {
    Objects.requireNonNull(paramAudioDeviceAttributes);
    enforceValidVolumeBehavior(paramInt);
    IAudioService iAudioService = getService();
    try {
      Context context = this.mApplicationContext;
      String str = context.getOpPackageName();
      iAudioService.setDeviceVolumeBehavior(paramAudioDeviceAttributes, paramInt, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getDeviceVolumeBehavior(int paramInt, String paramString) {
    AudioDeviceInfo.enforceValidAudioDeviceTypeOut(paramInt);
    return getDeviceVolumeBehavior(new AudioDeviceAttributes(2, paramInt, paramString));
  }
  
  public int getDeviceVolumeBehavior(AudioDeviceAttributes paramAudioDeviceAttributes) {
    Objects.requireNonNull(paramAudioDeviceAttributes);
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getDeviceVolumeBehavior(paramAudioDeviceAttributes);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setWiredDeviceConnectionState(int paramInt1, int paramInt2, String paramString1, String paramString2) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setWiredDeviceConnectionState type=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" state=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" address=");
      stringBuilder.append(paramString1);
      stringBuilder.append(" name=");
      stringBuilder.append(paramString2);
      Log.d("AudioManager", stringBuilder.toString());
      Context context = this.mApplicationContext;
      String str = context.getOpPackageName();
      iAudioService.setWiredDeviceConnectionState(paramInt1, paramInt2, paramString1, paramString2, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setBluetoothHearingAidDeviceConnectionState(BluetoothDevice paramBluetoothDevice, int paramInt1, boolean paramBoolean, int paramInt2) {
    IAudioService iAudioService = getService();
    try {
      iAudioService.setBluetoothHearingAidDeviceConnectionState(paramBluetoothDevice, paramInt1, paramBoolean, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setBluetoothA2dpDeviceConnectionState device=");
      stringBuilder.append(paramBluetoothDevice);
      stringBuilder.append(" state=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" profile=");
      stringBuilder.append(paramInt2);
      Log.d("AudioManager", stringBuilder.toString());
      iAudioService.setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent(paramBluetoothDevice, paramInt1, paramInt2, paramBoolean, paramInt3);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void handleBluetoothA2dpDeviceConfigChange(BluetoothDevice paramBluetoothDevice) {
    IAudioService iAudioService = getService();
    try {
      iAudioService.handleBluetoothA2dpDeviceConfigChange(paramBluetoothDevice);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void handleBluetoothA2dpActiveDeviceChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("handleBluetoothA2dpActiveDeviceChange device=");
      stringBuilder.append(paramBluetoothDevice);
      stringBuilder.append(" state=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" profile=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" suppressNoisyIntent=");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(" a2dpVolume=");
      stringBuilder.append(paramInt3);
      stringBuilder.append(" from ");
      stringBuilder.append(getContext().getOpPackageName());
      String str = stringBuilder.toString();
      Log.d("AudioManager", str);
      iAudioService.handleBluetoothA2dpActiveDeviceChange(paramBluetoothDevice, paramInt1, paramInt2, paramBoolean, paramInt3);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public IRingtonePlayer getRingtonePlayer() {
    try {
      return getService().getRingtonePlayer();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getProperty(String paramString) {
    boolean bool = "android.media.property.OUTPUT_SAMPLE_RATE".equals(paramString);
    String str1 = null, str2 = null;
    if (bool) {
      int i = AudioSystem.getPrimaryOutputSamplingRate();
      paramString = str2;
      if (i > 0)
        paramString = Integer.toString(i); 
      return paramString;
    } 
    if ("android.media.property.OUTPUT_FRAMES_PER_BUFFER".equals(paramString)) {
      int i = AudioSystem.getPrimaryOutputFrameCount();
      paramString = str1;
      if (i > 0)
        paramString = Integer.toString(i); 
      return paramString;
    } 
    if ("android.media.property.SUPPORT_MIC_NEAR_ULTRASOUND".equals(paramString))
      return String.valueOf(getContext().getResources().getBoolean(17891547)); 
    if ("android.media.property.SUPPORT_SPEAKER_NEAR_ULTRASOUND".equals(paramString))
      return String.valueOf(getContext().getResources().getBoolean(17891549)); 
    if ("android.media.property.SUPPORT_AUDIO_SOURCE_UNPROCESSED".equals(paramString))
      return String.valueOf(getContext().getResources().getBoolean(17891542)); 
    return null;
  }
  
  @SystemApi
  public boolean setAdditionalOutputDeviceDelay(AudioDeviceInfo paramAudioDeviceInfo, long paramLong) {
    Objects.requireNonNull(paramAudioDeviceInfo);
    return false;
  }
  
  @SystemApi
  public long getAdditionalOutputDeviceDelay(AudioDeviceInfo paramAudioDeviceInfo) {
    Objects.requireNonNull(paramAudioDeviceInfo);
    return 0L;
  }
  
  @SystemApi
  public long getMaxAdditionalOutputDeviceDelay(AudioDeviceInfo paramAudioDeviceInfo) {
    Objects.requireNonNull(paramAudioDeviceInfo);
    return 0L;
  }
  
  public int getOutputLatency(int paramInt) {
    return AudioSystem.getOutputLatency(paramInt);
  }
  
  public void setVolumeController(IVolumeController paramIVolumeController) {
    try {
      getService().setVolumeController(paramIVolumeController);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void notifyVolumeControllerVisible(IVolumeController paramIVolumeController, boolean paramBoolean) {
    try {
      getService().notifyVolumeControllerVisible(paramIVolumeController, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isStreamAffectedByRingerMode(int paramInt) {
    try {
      return getService().isStreamAffectedByRingerMode(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isStreamAffectedByMute(int paramInt) {
    try {
      return getService().isStreamAffectedByMute(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void disableSafeMediaVolume() {
    try {
      getService().disableSafeMediaVolume(this.mApplicationContext.getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setRingerModeInternal(int paramInt) {
    try {
      getService().setRingerModeInternal(paramInt, getContext().getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getRingerModeInternal() {
    try {
      return getService().getRingerModeInternal();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setVolumePolicy(VolumePolicy paramVolumePolicy) {
    try {
      getService().setVolumePolicy(paramVolumePolicy);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int setHdmiSystemAudioSupported(boolean paramBoolean) {
    try {
      return getService().setHdmiSystemAudioSupported(paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isHdmiSystemAudioSupported() {
    try {
      return getService().isHdmiSystemAudioSupported();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static int listAudioPorts(ArrayList<AudioPort> paramArrayList) {
    return updateAudioPortCache(paramArrayList, (ArrayList<AudioPatch>)null, (ArrayList<AudioPort>)null);
  }
  
  public static int listPreviousAudioPorts(ArrayList<AudioPort> paramArrayList) {
    return updateAudioPortCache((ArrayList<AudioPort>)null, (ArrayList<AudioPatch>)null, paramArrayList);
  }
  
  public static int listAudioDevicePorts(ArrayList<AudioDevicePort> paramArrayList) {
    if (paramArrayList == null)
      return -2; 
    ArrayList<AudioPort> arrayList = new ArrayList();
    int i = updateAudioPortCache(arrayList, (ArrayList<AudioPatch>)null, (ArrayList<AudioPort>)null);
    if (i == 0)
      filterDevicePorts(arrayList, paramArrayList); 
    return i;
  }
  
  public static int listPreviousAudioDevicePorts(ArrayList<AudioDevicePort> paramArrayList) {
    if (paramArrayList == null)
      return -2; 
    ArrayList<AudioPort> arrayList = new ArrayList();
    int i = updateAudioPortCache((ArrayList<AudioPort>)null, (ArrayList<AudioPatch>)null, arrayList);
    if (i == 0)
      filterDevicePorts(arrayList, paramArrayList); 
    return i;
  }
  
  private static void filterDevicePorts(ArrayList<AudioPort> paramArrayList, ArrayList<AudioDevicePort> paramArrayList1) {
    paramArrayList1.clear();
    for (byte b = 0; b < paramArrayList.size(); b++) {
      if (paramArrayList.get(b) instanceof AudioDevicePort)
        paramArrayList1.add((AudioDevicePort)paramArrayList.get(b)); 
    } 
  }
  
  public static int createAudioPatch(AudioPatch[] paramArrayOfAudioPatch, AudioPortConfig[] paramArrayOfAudioPortConfig1, AudioPortConfig[] paramArrayOfAudioPortConfig2) {
    return AudioSystem.createAudioPatch(paramArrayOfAudioPatch, paramArrayOfAudioPortConfig1, paramArrayOfAudioPortConfig2);
  }
  
  public static int releaseAudioPatch(AudioPatch paramAudioPatch) {
    return AudioSystem.releaseAudioPatch(paramAudioPatch);
  }
  
  public static int listAudioPatches(ArrayList<AudioPatch> paramArrayList) {
    return updateAudioPortCache((ArrayList<AudioPort>)null, paramArrayList, (ArrayList<AudioPort>)null);
  }
  
  public static int setAudioPortGain(AudioPort paramAudioPort, AudioGainConfig paramAudioGainConfig) {
    if (paramAudioPort == null || paramAudioGainConfig == null)
      return -2; 
    AudioPortConfig audioPortConfig2 = paramAudioPort.activeConfig();
    int i = audioPortConfig2.samplingRate();
    AudioPortConfig audioPortConfig1 = new AudioPortConfig(paramAudioPort, i, audioPortConfig2.channelMask(), audioPortConfig2.format(), paramAudioGainConfig);
    audioPortConfig1.mConfigMask = 8;
    return AudioSystem.setAudioPortConfig(audioPortConfig1);
  }
  
  public void registerAudioPortUpdateListener(OnAudioPortUpdateListener paramOnAudioPortUpdateListener) {
    sAudioPortEventHandler.init();
    sAudioPortEventHandler.registerListener(paramOnAudioPortUpdateListener);
  }
  
  public void unregisterAudioPortUpdateListener(OnAudioPortUpdateListener paramOnAudioPortUpdateListener) {
    sAudioPortEventHandler.unregisterListener(paramOnAudioPortUpdateListener);
  }
  
  static int resetAudioPortGeneration() {
    synchronized (sAudioPortGeneration) {
      int i = sAudioPortGeneration.intValue();
      sAudioPortGeneration = Integer.valueOf(0);
      return i;
    } 
  }
  
  static int updateAudioPortCache(ArrayList<AudioPort> paramArrayList1, ArrayList<AudioPatch> paramArrayList, ArrayList<AudioPort> paramArrayList2) {
    sAudioPortEventHandler.init();
    synchronized (sAudioPortGeneration) {
      if (sAudioPortGeneration.intValue() == 0) {
        int[] arrayOfInt1 = new int[1];
        int[] arrayOfInt2 = new int[1];
        ArrayList<AudioPort> arrayList = new ArrayList();
        this();
        ArrayList<AudioPatch> arrayList1 = new ArrayList();
        this();
        do {
          arrayList.clear();
          int i = AudioSystem.listAudioPorts(arrayList, arrayOfInt2);
          if (i != 0) {
            Log.w("AudioManager", "updateAudioPortCache: listAudioPorts failed");
            return i;
          } 
          arrayList1.clear();
          i = AudioSystem.listAudioPatches(arrayList1, arrayOfInt1);
          if (i != 0) {
            Log.w("AudioManager", "updateAudioPortCache: listAudioPatches failed");
            return i;
          } 
        } while (arrayOfInt1[0] != arrayOfInt2[0] && (paramArrayList1 == null || paramArrayList == null));
        if (arrayOfInt1[0] != arrayOfInt2[0])
          return -1; 
        byte b;
        for (b = 0; b < arrayList1.size(); b++) {
          byte b1;
          for (b1 = 0; b1 < (((AudioPatch)arrayList1.get(b)).sources()).length; b1++) {
            AudioPortConfig audioPortConfig = updatePortConfig(((AudioPatch)arrayList1.get(b)).sources()[b1], arrayList);
            ((AudioPatch)arrayList1.get(b)).sources()[b1] = audioPortConfig;
          } 
          for (b1 = 0; b1 < (((AudioPatch)arrayList1.get(b)).sinks()).length; b1++) {
            AudioPortConfig audioPortConfig = updatePortConfig(((AudioPatch)arrayList1.get(b)).sinks()[b1], arrayList);
            ((AudioPatch)arrayList1.get(b)).sinks()[b1] = audioPortConfig;
          } 
        } 
        for (Iterator<AudioPatch> iterator = arrayList1.iterator(); iterator.hasNext(); ) {
          AudioPatch audioPatch = iterator.next();
          byte b2 = 0;
          AudioPortConfig[] arrayOfAudioPortConfig = audioPatch.sources();
          int i = arrayOfAudioPortConfig.length;
          byte b1 = 0;
          while (true) {
            b = b2;
            if (b1 < i) {
              AudioPortConfig audioPortConfig = arrayOfAudioPortConfig[b1];
              if (audioPortConfig == null) {
                b = 1;
                break;
              } 
              b1++;
              continue;
            } 
            break;
          } 
          arrayOfAudioPortConfig = audioPatch.sinks();
          i = arrayOfAudioPortConfig.length;
          b1 = 0;
          while (true) {
            b2 = b;
            if (b1 < i) {
              AudioPortConfig audioPortConfig = arrayOfAudioPortConfig[b1];
              if (audioPortConfig == null) {
                b2 = 1;
                break;
              } 
              b1++;
              continue;
            } 
            break;
          } 
          if (b2 != 0)
            iterator.remove(); 
        } 
        sPreviousAudioPortsCached = sAudioPortsCached;
        sAudioPortsCached = arrayList;
        sAudioPatchesCached = arrayList1;
        sAudioPortGeneration = Integer.valueOf(arrayOfInt2[0]);
      } 
      if (paramArrayList1 != null) {
        paramArrayList1.clear();
        paramArrayList1.addAll(sAudioPortsCached);
      } 
      if (paramArrayList != null) {
        paramArrayList.clear();
        paramArrayList.addAll(sAudioPatchesCached);
      } 
      if (paramArrayList2 != null) {
        paramArrayList2.clear();
        paramArrayList2.addAll(sPreviousAudioPortsCached);
      } 
      return 0;
    } 
  }
  
  static AudioPortConfig updatePortConfig(AudioPortConfig paramAudioPortConfig, ArrayList<AudioPort> paramArrayList) {
    StringBuilder stringBuilder;
    AudioPort audioPort2, audioPort1 = paramAudioPortConfig.port();
    int i = 0;
    while (true) {
      audioPort2 = audioPort1;
      if (i < paramArrayList.size()) {
        if (((AudioPort)paramArrayList.get(i)).handle().equals(audioPort1.handle())) {
          audioPort2 = paramArrayList.get(i);
          break;
        } 
        i++;
        continue;
      } 
      break;
    } 
    if (i == paramArrayList.size()) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("updatePortConfig port not found for handle: ");
      stringBuilder.append(audioPort2.handle().id());
      Log.e("AudioManager", stringBuilder.toString());
      return null;
    } 
    AudioGainConfig audioGainConfig2 = stringBuilder.gain();
    AudioGainConfig audioGainConfig1 = audioGainConfig2;
    if (audioGainConfig2 != null) {
      AudioGain audioGain = audioPort2.gain(audioGainConfig2.index());
      int m = audioGainConfig2.mode();
      int n = audioGainConfig2.channelMask();
      int[] arrayOfInt = audioGainConfig2.values();
      i = audioGainConfig2.rampDurationMs();
      audioGainConfig1 = audioGain.buildConfig(m, n, arrayOfInt, i);
    } 
    i = stringBuilder.samplingRate();
    int k = stringBuilder.channelMask();
    int j = stringBuilder.format();
    return audioPort2.buildConfig(i, k, j, audioGainConfig1);
  }
  
  private OnAmPortUpdateListener mPortListener = null;
  
  private final ArrayMap<AudioDeviceCallback, NativeEventHandlerDelegate> mDeviceCallbacks = new ArrayMap();
  
  private static boolean checkFlags(AudioDevicePort paramAudioDevicePort, int paramInt) {
    int i = paramAudioDevicePort.role();
    boolean bool = true;
    if ((i != 2 || (paramInt & 0x2) == 0) && (
      paramAudioDevicePort.role() != 1 || (paramInt & 0x1) == 0))
      bool = false; 
    return bool;
  }
  
  private static boolean checkTypes(AudioDevicePort paramAudioDevicePort) {
    boolean bool;
    if (AudioDeviceInfo.convertInternalDeviceToDeviceType(paramAudioDevicePort.type()) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public AudioDeviceInfo[] getDevices(int paramInt) {
    return getDevicesStatic(paramInt);
  }
  
  private static AudioDeviceInfo[] infoListFromPortList(ArrayList<AudioDevicePort> paramArrayList, int paramInt) {
    int i = 0;
    for (AudioDevicePort audioDevicePort : paramArrayList) {
      int j = i;
      if (checkTypes(audioDevicePort)) {
        j = i;
        if (checkFlags(audioDevicePort, paramInt))
          j = i + 1; 
      } 
      i = j;
    } 
    AudioDeviceInfo[] arrayOfAudioDeviceInfo = new AudioDeviceInfo[i];
    i = 0;
    for (AudioDevicePort audioDevicePort : paramArrayList) {
      int j = i;
      if (checkTypes(audioDevicePort)) {
        j = i;
        if (checkFlags(audioDevicePort, paramInt)) {
          arrayOfAudioDeviceInfo[i] = new AudioDeviceInfo(audioDevicePort);
          j = i + 1;
        } 
      } 
      i = j;
    } 
    return arrayOfAudioDeviceInfo;
  }
  
  private static AudioDeviceInfo[] calcListDeltas(ArrayList<AudioDevicePort> paramArrayList1, ArrayList<AudioDevicePort> paramArrayList2, int paramInt) {
    ArrayList<AudioDevicePort> arrayList = new ArrayList();
    for (byte b = 0; b < paramArrayList2.size(); b++) {
      boolean bool = false;
      AudioDevicePort audioDevicePort = paramArrayList2.get(b);
      byte b1 = 0;
      for (; b1 < paramArrayList1.size() && !bool; 
        b1++) {
        if (audioDevicePort.id() == ((AudioDevicePort)paramArrayList1.get(b1)).id()) {
          bool = true;
        } else {
          bool = false;
        } 
      } 
      if (!bool)
        arrayList.add(audioDevicePort); 
    } 
    return infoListFromPortList(arrayList, paramInt);
  }
  
  public static AudioDeviceInfo[] getDevicesStatic(int paramInt) {
    ArrayList<AudioDevicePort> arrayList = new ArrayList();
    int i = listAudioDevicePorts(arrayList);
    if (i != 0)
      return new AudioDeviceInfo[0]; 
    return infoListFromPortList(arrayList, paramInt);
  }
  
  public void registerAudioDeviceCallback(AudioDeviceCallback paramAudioDeviceCallback, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDeviceCallbacks : Landroid/util/ArrayMap;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 98
    //   11: aload_0
    //   12: getfield mDeviceCallbacks : Landroid/util/ArrayMap;
    //   15: aload_1
    //   16: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   19: ifne -> 98
    //   22: aload_0
    //   23: getfield mDeviceCallbacks : Landroid/util/ArrayMap;
    //   26: invokevirtual size : ()I
    //   29: ifne -> 65
    //   32: aload_0
    //   33: getfield mPortListener : Landroid/media/AudioManager$OnAmPortUpdateListener;
    //   36: ifnonnull -> 57
    //   39: new android/media/AudioManager$OnAmPortUpdateListener
    //   42: astore #4
    //   44: aload #4
    //   46: aload_0
    //   47: aconst_null
    //   48: invokespecial <init> : (Landroid/media/AudioManager;Landroid/media/AudioManager$1;)V
    //   51: aload_0
    //   52: aload #4
    //   54: putfield mPortListener : Landroid/media/AudioManager$OnAmPortUpdateListener;
    //   57: aload_0
    //   58: aload_0
    //   59: getfield mPortListener : Landroid/media/AudioManager$OnAmPortUpdateListener;
    //   62: invokevirtual registerAudioPortUpdateListener : (Landroid/media/AudioManager$OnAudioPortUpdateListener;)V
    //   65: new android/media/AudioManager$NativeEventHandlerDelegate
    //   68: astore #4
    //   70: aload #4
    //   72: aload_0
    //   73: aload_1
    //   74: aload_2
    //   75: invokespecial <init> : (Landroid/media/AudioManager;Landroid/media/AudioDeviceCallback;Landroid/os/Handler;)V
    //   78: aload_0
    //   79: getfield mDeviceCallbacks : Landroid/util/ArrayMap;
    //   82: aload_1
    //   83: aload #4
    //   85: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   88: pop
    //   89: aload_0
    //   90: aload #4
    //   92: invokevirtual getHandler : ()Landroid/os/Handler;
    //   95: invokespecial broadcastDeviceListChange_sync : (Landroid/os/Handler;)V
    //   98: aload_3
    //   99: monitorexit
    //   100: return
    //   101: astore_1
    //   102: aload_3
    //   103: monitorexit
    //   104: aload_1
    //   105: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5968	-> 0
    //   #5969	-> 7
    //   #5970	-> 22
    //   #5971	-> 32
    //   #5972	-> 39
    //   #5974	-> 57
    //   #5976	-> 65
    //   #5978	-> 78
    //   #5979	-> 89
    //   #5981	-> 98
    //   #5982	-> 100
    //   #5981	-> 101
    // Exception table:
    //   from	to	target	type
    //   11	22	101	finally
    //   22	32	101	finally
    //   32	39	101	finally
    //   39	57	101	finally
    //   57	65	101	finally
    //   65	78	101	finally
    //   78	89	101	finally
    //   89	98	101	finally
    //   98	100	101	finally
    //   102	104	101	finally
  }
  
  public void unregisterAudioDeviceCallback(AudioDeviceCallback paramAudioDeviceCallback) {
    synchronized (this.mDeviceCallbacks) {
      if (this.mDeviceCallbacks.containsKey(paramAudioDeviceCallback)) {
        this.mDeviceCallbacks.remove(paramAudioDeviceCallback);
        if (this.mDeviceCallbacks.size() == 0)
          unregisterAudioPortUpdateListener(this.mPortListener); 
      } 
      return;
    } 
  }
  
  public static void setPortIdForMicrophones(ArrayList<MicrophoneInfo> paramArrayList) {
    AudioDeviceInfo[] arrayOfAudioDeviceInfo = getDevicesStatic(1);
    for (int i = paramArrayList.size() - 1; i >= 0; i--) {
      boolean bool2, bool1 = false;
      int j = arrayOfAudioDeviceInfo.length;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < j) {
          AudioDeviceInfo audioDeviceInfo = arrayOfAudioDeviceInfo[b];
          if (audioDeviceInfo.getPort().type() == ((MicrophoneInfo)paramArrayList.get(i)).getInternalDeviceType() && 
            TextUtils.equals(audioDeviceInfo.getAddress(), ((MicrophoneInfo)paramArrayList.get(i)).getAddress())) {
            ((MicrophoneInfo)paramArrayList.get(i)).setId(audioDeviceInfo.getId());
            bool2 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (!bool2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to find port id for device with type:");
        stringBuilder.append(((MicrophoneInfo)paramArrayList.get(i)).getType());
        stringBuilder.append(" address:");
        stringBuilder.append(((MicrophoneInfo)paramArrayList.get(i)).getAddress());
        String str = stringBuilder.toString();
        Log.i("AudioManager", str);
        paramArrayList.remove(i);
      } 
    } 
  }
  
  public static MicrophoneInfo microphoneInfoFromAudioDeviceInfo(AudioDeviceInfo paramAudioDeviceInfo) {
    int i = paramAudioDeviceInfo.getType();
    if (i == 15 || i == 18) {
      i = 1;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramAudioDeviceInfo.getPort().name());
      stringBuilder1.append(paramAudioDeviceInfo.getId());
      String str1 = stringBuilder1.toString();
      MicrophoneInfo microphoneInfo1 = new MicrophoneInfo(str1, paramAudioDeviceInfo.getPort().type(), paramAudioDeviceInfo.getAddress(), i, -1, -1, MicrophoneInfo.POSITION_UNKNOWN, MicrophoneInfo.ORIENTATION_UNKNOWN, new ArrayList<>(), new ArrayList<>(), -3.4028235E38F, -3.4028235E38F, -3.4028235E38F, 0);
      microphoneInfo1.setId(paramAudioDeviceInfo.getId());
      return microphoneInfo1;
    } 
    if (i == 0) {
      i = 0;
    } else {
      i = 3;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramAudioDeviceInfo.getPort().name());
    stringBuilder.append(paramAudioDeviceInfo.getId());
    String str = stringBuilder.toString();
    MicrophoneInfo microphoneInfo = new MicrophoneInfo(str, paramAudioDeviceInfo.getPort().type(), paramAudioDeviceInfo.getAddress(), i, -1, -1, MicrophoneInfo.POSITION_UNKNOWN, MicrophoneInfo.ORIENTATION_UNKNOWN, new ArrayList<>(), new ArrayList<>(), -3.4028235E38F, -3.4028235E38F, -3.4028235E38F, 0);
    microphoneInfo.setId(paramAudioDeviceInfo.getId());
    return microphoneInfo;
  }
  
  private void addMicrophonesFromAudioDeviceInfo(ArrayList<MicrophoneInfo> paramArrayList, HashSet<Integer> paramHashSet) {
    for (AudioDeviceInfo audioDeviceInfo : getDevicesStatic(1)) {
      if (!paramHashSet.contains(Integer.valueOf(audioDeviceInfo.getType()))) {
        MicrophoneInfo microphoneInfo = microphoneInfoFromAudioDeviceInfo(audioDeviceInfo);
        paramArrayList.add(microphoneInfo);
      } 
    } 
  }
  
  public List<MicrophoneInfo> getMicrophones() throws IOException {
    ArrayList<MicrophoneInfo> arrayList = new ArrayList();
    int i = AudioSystem.getMicrophones(arrayList);
    HashSet<Integer> hashSet = new HashSet();
    hashSet.add(Integer.valueOf(18));
    if (i != 0) {
      if (i != -3) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getMicrophones failed:");
        stringBuilder.append(i);
        Log.e("AudioManager", stringBuilder.toString());
      } 
      Log.i("AudioManager", "fallback on device info");
      addMicrophonesFromAudioDeviceInfo(arrayList, hashSet);
      return arrayList;
    } 
    setPortIdForMicrophones(arrayList);
    hashSet.add(Integer.valueOf(15));
    addMicrophonesFromAudioDeviceInfo(arrayList, hashSet);
    return arrayList;
  }
  
  public List<BluetoothCodecConfig> getHwOffloadEncodingFormatsSupportedForA2DP() {
    StringBuilder stringBuilder;
    ArrayList<Integer> arrayList = new ArrayList();
    ArrayList<BluetoothCodecConfig> arrayList1 = new ArrayList();
    int i = AudioSystem.getHwOffloadEncodingFormatsSupportedForA2DP(arrayList);
    if (i != 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("getHwOffloadEncodingFormatsSupportedForA2DP failed:");
      stringBuilder.append(i);
      Log.e("AudioManager", stringBuilder.toString());
      return arrayList1;
    } 
    for (Integer integer : stringBuilder) {
      i = AudioSystem.audioFormatToBluetoothSourceCodec(integer.intValue());
      if (i != 1000000)
        arrayList1.add(new BluetoothCodecConfig(i)); 
    } 
    return arrayList1;
  }
  
  private ArrayList<AudioDevicePort> mPreviousPorts = new ArrayList<>();
  
  private void broadcastDeviceListChange_sync(Handler paramHandler) {
    ArrayList<AudioDevicePort> arrayList = new ArrayList();
    int i = listAudioDevicePorts(arrayList);
    if (i != 0)
      return; 
    if (paramHandler != null) {
      AudioDeviceInfo[] arrayOfAudioDeviceInfo = infoListFromPortList(arrayList, 3);
      Message message = Message.obtain(paramHandler, 0, arrayOfAudioDeviceInfo);
      paramHandler.sendMessage(message);
    } else {
      ArrayList<AudioDevicePort> arrayList1 = this.mPreviousPorts;
      AudioDeviceInfo[] arrayOfAudioDeviceInfo1 = calcListDeltas(arrayList1, arrayList, 3);
      ArrayList<AudioDevicePort> arrayList2 = this.mPreviousPorts;
      AudioDeviceInfo[] arrayOfAudioDeviceInfo2 = calcListDeltas(arrayList, arrayList2, 3);
      if (arrayOfAudioDeviceInfo1.length != 0 || arrayOfAudioDeviceInfo2.length != 0)
        for (i = 0; i < this.mDeviceCallbacks.size(); i++) {
          Handler handler = ((NativeEventHandlerDelegate)this.mDeviceCallbacks.valueAt(i)).getHandler();
          if (handler != null) {
            if (arrayOfAudioDeviceInfo2.length != 0)
              handler.sendMessage(Message.obtain(handler, 2, arrayOfAudioDeviceInfo2)); 
            if (arrayOfAudioDeviceInfo1.length != 0)
              handler.sendMessage(Message.obtain(handler, 1, arrayOfAudioDeviceInfo1)); 
          } 
        }  
    } 
    this.mPreviousPorts = arrayList;
  }
  
  private class OnAmPortUpdateListener implements OnAudioPortUpdateListener {
    static final String TAG = "OnAmPortUpdateListener";
    
    final AudioManager this$0;
    
    private OnAmPortUpdateListener() {}
    
    public void onAudioPortListUpdate(AudioPort[] param1ArrayOfAudioPort) {
      synchronized (AudioManager.this.mDeviceCallbacks) {
        AudioManager.this.broadcastDeviceListChange_sync((Handler)null);
        return;
      } 
    }
    
    public void onAudioPatchListUpdate(AudioPatch[] param1ArrayOfAudioPatch) {}
    
    public void onServiceDied() {
      synchronized (AudioManager.this.mDeviceCallbacks) {
        AudioManager.this.broadcastDeviceListChange_sync((Handler)null);
        return;
      } 
    }
  }
  
  @SystemApi
  class AudioServerStateCallback {
    public void onAudioServerDown() {}
    
    public void onAudioServerUp() {}
  }
  
  private final Object mAudioServerStateCbLock = new Object();
  
  private final IAudioServerStateDispatcher mAudioServerStateDispatcher = (IAudioServerStateDispatcher)new Object(this);
  
  public static final String ACTION_AUDIO_BECOMING_NOISY = "android.media.AUDIO_BECOMING_NOISY";
  
  public static final String ACTION_HDMI_AUDIO_PLUG = "android.media.action.HDMI_AUDIO_PLUG";
  
  public static final String ACTION_HEADSET_PLUG = "android.intent.action.HEADSET_PLUG";
  
  public static final String ACTION_MICROPHONE_MUTE_CHANGED = "android.media.action.MICROPHONE_MUTE_CHANGED";
  
  @Deprecated
  public static final String ACTION_SCO_AUDIO_STATE_CHANGED = "android.media.SCO_AUDIO_STATE_CHANGED";
  
  public static final String ACTION_SCO_AUDIO_STATE_UPDATED = "android.media.ACTION_SCO_AUDIO_STATE_UPDATED";
  
  public static final String ACTION_SET_SAFE_VOLUME = "android.media.action.SET_SAFE_VOLUME";
  
  public static final String ACTION_SPEAKERPHONE_STATE_CHANGED = "android.media.action.SPEAKERPHONE_STATE_CHANGED";
  
  public static final int ADJUST_LOWER = -1;
  
  public static final int ADJUST_MUTE = -100;
  
  public static final int ADJUST_RAISE = 1;
  
  public static final int ADJUST_SAME = 0;
  
  public static final int ADJUST_TOGGLE_MUTE = 101;
  
  public static final int ADJUST_UNMUTE = 100;
  
  public static final int AUDIOFOCUS_FLAGS_APPS = 3;
  
  public static final int AUDIOFOCUS_FLAGS_SYSTEM = 7;
  
  @SystemApi
  public static final int AUDIOFOCUS_FLAG_DELAY_OK = 1;
  
  @SystemApi
  public static final int AUDIOFOCUS_FLAG_LOCK = 4;
  
  @SystemApi
  public static final int AUDIOFOCUS_FLAG_PAUSES_ON_DUCKABLE_LOSS = 2;
  
  public static final int AUDIOFOCUS_GAIN = 1;
  
  public static final int AUDIOFOCUS_GAIN_TRANSIENT = 2;
  
  public static final int AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE = 4;
  
  public static final int AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK = 3;
  
  public static final int AUDIOFOCUS_LOSS = -1;
  
  public static final int AUDIOFOCUS_LOSS_TRANSIENT = -2;
  
  public static final int AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK = -3;
  
  public static final int AUDIOFOCUS_NONE = 0;
  
  public static final int AUDIOFOCUS_REQUEST_DELAYED = 2;
  
  public static final int AUDIOFOCUS_REQUEST_FAILED = 0;
  
  public static final int AUDIOFOCUS_REQUEST_GRANTED = 1;
  
  public static final int AUDIOFOCUS_REQUEST_WAITING_FOR_EXT_POLICY = 100;
  
  static final int AUDIOPORT_GENERATION_INIT = 0;
  
  public static final int AUDIO_SESSION_ID_GENERATE = 0;
  
  private static final boolean DEBUG = false;
  
  public static final int DEVICE_IN_ANLG_DOCK_HEADSET = -2147483136;
  
  public static final int DEVICE_IN_BACK_MIC = -2147483520;
  
  public static final int DEVICE_IN_BLUETOOTH_SCO_HEADSET = -2147483640;
  
  public static final int DEVICE_IN_BUILTIN_MIC = -2147483644;
  
  public static final int DEVICE_IN_DGTL_DOCK_HEADSET = -2147482624;
  
  public static final int DEVICE_IN_FM_TUNER = -2147475456;
  
  public static final int DEVICE_IN_HDMI = -2147483616;
  
  public static final int DEVICE_IN_HDMI_ARC = -2013265920;
  
  public static final int DEVICE_IN_LINE = -2147450880;
  
  public static final int DEVICE_IN_LOOPBACK = -2147221504;
  
  public static final int DEVICE_IN_SPDIF = -2147418112;
  
  public static final int DEVICE_IN_TELEPHONY_RX = -2147483584;
  
  public static final int DEVICE_IN_TV_TUNER = -2147467264;
  
  public static final int DEVICE_IN_USB_ACCESSORY = -2147481600;
  
  public static final int DEVICE_IN_USB_DEVICE = -2147479552;
  
  public static final int DEVICE_IN_WIRED_HEADSET = -2147483632;
  
  public static final int DEVICE_NONE = 0;
  
  public static final int DEVICE_OUT_ANLG_DOCK_HEADSET = 2048;
  
  public static final int DEVICE_OUT_AUX_DIGITAL = 1024;
  
  public static final int DEVICE_OUT_BLUETOOTH_A2DP = 128;
  
  public static final int DEVICE_OUT_BLUETOOTH_A2DP_HEADPHONES = 256;
  
  public static final int DEVICE_OUT_BLUETOOTH_A2DP_SPEAKER = 512;
  
  public static final int DEVICE_OUT_BLUETOOTH_SCO = 16;
  
  public static final int DEVICE_OUT_BLUETOOTH_SCO_CARKIT = 64;
  
  public static final int DEVICE_OUT_BLUETOOTH_SCO_HEADSET = 32;
  
  public static final int DEVICE_OUT_DEFAULT = 1073741824;
  
  public static final int DEVICE_OUT_DGTL_DOCK_HEADSET = 4096;
  
  public static final int DEVICE_OUT_EARPIECE = 1;
  
  public static final int DEVICE_OUT_FM = 1048576;
  
  public static final int DEVICE_OUT_HDMI = 1024;
  
  public static final int DEVICE_OUT_HDMI_ARC = 262144;
  
  public static final int DEVICE_OUT_LINE = 131072;
  
  public static final int DEVICE_OUT_REMOTE_SUBMIX = 32768;
  
  public static final int DEVICE_OUT_SPDIF = 524288;
  
  public static final int DEVICE_OUT_SPEAKER = 2;
  
  public static final int DEVICE_OUT_TELEPHONY_TX = 65536;
  
  public static final int DEVICE_OUT_USB_ACCESSORY = 8192;
  
  public static final int DEVICE_OUT_USB_DEVICE = 16384;
  
  public static final int DEVICE_OUT_USB_HEADSET = 67108864;
  
  public static final int DEVICE_OUT_WIRED_HEADPHONE = 8;
  
  public static final int DEVICE_OUT_WIRED_HEADSET = 4;
  
  public static final int DEVICE_VOLUME_BEHAVIOR_ABSOLUTE = 3;
  
  public static final int DEVICE_VOLUME_BEHAVIOR_ABSOLUTE_MULTI_MODE = 4;
  
  public static final int DEVICE_VOLUME_BEHAVIOR_FIXED = 2;
  
  public static final int DEVICE_VOLUME_BEHAVIOR_FULL = 1;
  
  public static final int DEVICE_VOLUME_BEHAVIOR_UNSET = -1;
  
  public static final int DEVICE_VOLUME_BEHAVIOR_VARIABLE = 0;
  
  public static final int ERROR = -1;
  
  public static final int ERROR_BAD_VALUE = -2;
  
  public static final int ERROR_DEAD_OBJECT = -6;
  
  public static final int ERROR_INVALID_OPERATION = -3;
  
  public static final int ERROR_NO_INIT = -5;
  
  public static final int ERROR_PERMISSION_DENIED = -4;
  
  public static final String EXTRA_AUDIO_PLUG_STATE = "android.media.extra.AUDIO_PLUG_STATE";
  
  public static final String EXTRA_ENCODINGS = "android.media.extra.ENCODINGS";
  
  public static final String EXTRA_MASTER_VOLUME_MUTED = "android.media.EXTRA_MASTER_VOLUME_MUTED";
  
  public static final String EXTRA_MAX_CHANNEL_COUNT = "android.media.extra.MAX_CHANNEL_COUNT";
  
  public static final String EXTRA_PREV_VOLUME_STREAM_DEVICES = "android.media.EXTRA_PREV_VOLUME_STREAM_DEVICES";
  
  public static final String EXTRA_PREV_VOLUME_STREAM_VALUE = "android.media.EXTRA_PREV_VOLUME_STREAM_VALUE";
  
  public static final String EXTRA_RINGER_MODE = "android.media.EXTRA_RINGER_MODE";
  
  public static final String EXTRA_SCO_AUDIO_PREVIOUS_STATE = "android.media.extra.SCO_AUDIO_PREVIOUS_STATE";
  
  public static final String EXTRA_SCO_AUDIO_STATE = "android.media.extra.SCO_AUDIO_STATE";
  
  public static final String EXTRA_STREAM_VOLUME_MUTED = "android.media.EXTRA_STREAM_VOLUME_MUTED";
  
  public static final String EXTRA_VIBRATE_SETTING = "android.media.EXTRA_VIBRATE_SETTING";
  
  public static final String EXTRA_VIBRATE_TYPE = "android.media.EXTRA_VIBRATE_TYPE";
  
  public static final String EXTRA_VOLUME_STREAM_DEVICES = "android.media.EXTRA_VOLUME_STREAM_DEVICES";
  
  public static final String EXTRA_VOLUME_STREAM_TYPE = "android.media.EXTRA_VOLUME_STREAM_TYPE";
  
  public static final String EXTRA_VOLUME_STREAM_TYPE_ALIAS = "android.media.EXTRA_VOLUME_STREAM_TYPE_ALIAS";
  
  public static final String EXTRA_VOLUME_STREAM_VALUE = "android.media.EXTRA_VOLUME_STREAM_VALUE";
  
  private static final int EXT_FOCUS_POLICY_TIMEOUT_MS = 200;
  
  public static final int FLAG_ACTIVE_MEDIA_ONLY = 512;
  
  public static final int FLAG_ALLOW_RINGER_MODES = 2;
  
  public static final int FLAG_BLUETOOTH_ABS_VOLUME = 64;
  
  public static final int FLAG_FIXED_VOLUME = 32;
  
  public static final int FLAG_FROM_KEY = 4096;
  
  public static final int FLAG_HDMI_SYSTEM_AUDIO_VOLUME = 256;
  
  private static final TreeMap<Integer, String> FLAG_NAMES;
  
  public static final int FLAG_PLAY_SOUND = 4;
  
  public static final int FLAG_REMOVE_SOUND_AND_VIBRATE = 8;
  
  public static final int FLAG_SHOW_SILENT_HINT = 128;
  
  public static final int FLAG_SHOW_UI = 1;
  
  public static final int FLAG_SHOW_UI_WARNINGS = 1024;
  
  public static final int FLAG_SHOW_VIBRATE_HINT = 2048;
  
  public static final int FLAG_VIBRATE = 16;
  
  private static final String FOCUS_CLIENT_ID_STRING = "android_audio_focus_client_id";
  
  public static final int FX_FOCUS_NAVIGATION_DOWN = 2;
  
  public static final int FX_FOCUS_NAVIGATION_LEFT = 3;
  
  public static final int FX_FOCUS_NAVIGATION_RIGHT = 4;
  
  public static final int FX_FOCUS_NAVIGATION_UP = 1;
  
  public static final int FX_KEYPRESS_DELETE = 7;
  
  public static final int FX_KEYPRESS_INVALID = 9;
  
  public static final int FX_KEYPRESS_RETURN = 8;
  
  public static final int FX_KEYPRESS_SPACEBAR = 6;
  
  public static final int FX_KEYPRESS_STANDARD = 5;
  
  public static final int FX_KEY_CLICK = 0;
  
  public static final int GET_DEVICES_ALL = 3;
  
  public static final int GET_DEVICES_INPUTS = 1;
  
  public static final int GET_DEVICES_OUTPUTS = 2;
  
  public static final String INTERNAL_RINGER_MODE_CHANGED_ACTION = "android.media.INTERNAL_RINGER_MODE_CHANGED_ACTION";
  
  public static final String MASTER_MUTE_CHANGED_ACTION = "android.media.MASTER_MUTE_CHANGED_ACTION";
  
  public static final int MODE_CALL_SCREENING = 4;
  
  public static final int MODE_CURRENT = -1;
  
  public static final int MODE_INVALID = -2;
  
  public static final int MODE_IN_CALL = 2;
  
  public static final int MODE_IN_COMMUNICATION = 3;
  
  public static final int MODE_NORMAL = 0;
  
  public static final int MODE_RINGTONE = 1;
  
  private static final int MSG_DEVICES_CALLBACK_REGISTERED = 0;
  
  private static final int MSG_DEVICES_DEVICES_ADDED = 1;
  
  private static final int MSG_DEVICES_DEVICES_REMOVED = 2;
  
  private static final int MSSG_FOCUS_CHANGE = 0;
  
  private static final int MSSG_PLAYBACK_CONFIG_CHANGE = 2;
  
  private static final int MSSG_RECORDING_CONFIG_CHANGE = 1;
  
  public static final int NUM_SOUND_EFFECTS = 10;
  
  @Deprecated
  public static final int NUM_STREAMS = 5;
  
  public static final String PROPERTY_OUTPUT_FRAMES_PER_BUFFER = "android.media.property.OUTPUT_FRAMES_PER_BUFFER";
  
  public static final String PROPERTY_OUTPUT_SAMPLE_RATE = "android.media.property.OUTPUT_SAMPLE_RATE";
  
  public static final String PROPERTY_SUPPORT_AUDIO_SOURCE_UNPROCESSED = "android.media.property.SUPPORT_AUDIO_SOURCE_UNPROCESSED";
  
  public static final String PROPERTY_SUPPORT_MIC_NEAR_ULTRASOUND = "android.media.property.SUPPORT_MIC_NEAR_ULTRASOUND";
  
  public static final String PROPERTY_SUPPORT_SPEAKER_NEAR_ULTRASOUND = "android.media.property.SUPPORT_SPEAKER_NEAR_ULTRASOUND";
  
  public static final int RECORDER_STATE_STARTED = 0;
  
  public static final int RECORDER_STATE_STOPPED = 1;
  
  public static final int RECORD_CONFIG_EVENT_NONE = -1;
  
  public static final int RECORD_CONFIG_EVENT_RELEASE = 3;
  
  public static final int RECORD_CONFIG_EVENT_START = 0;
  
  public static final int RECORD_CONFIG_EVENT_STOP = 1;
  
  public static final int RECORD_CONFIG_EVENT_UPDATE = 2;
  
  public static final int RECORD_RIID_INVALID = -1;
  
  public static final String RINGER_MODE_CHANGED_ACTION = "android.media.RINGER_MODE_CHANGED";
  
  public static final int RINGER_MODE_MAX = 2;
  
  public static final int RINGER_MODE_NORMAL = 2;
  
  public static final int RINGER_MODE_SILENT = 0;
  
  public static final int RINGER_MODE_VIBRATE = 1;
  
  @Deprecated
  public static final int ROUTE_ALL = -1;
  
  @Deprecated
  public static final int ROUTE_BLUETOOTH = 4;
  
  @Deprecated
  public static final int ROUTE_BLUETOOTH_A2DP = 16;
  
  @Deprecated
  public static final int ROUTE_BLUETOOTH_SCO = 4;
  
  @Deprecated
  public static final int ROUTE_EARPIECE = 1;
  
  @Deprecated
  public static final int ROUTE_HEADSET = 8;
  
  @Deprecated
  public static final int ROUTE_SPEAKER = 2;
  
  public static final int SCO_AUDIO_STATE_CONNECTED = 1;
  
  public static final int SCO_AUDIO_STATE_CONNECTING = 2;
  
  public static final int SCO_AUDIO_STATE_DISCONNECTED = 0;
  
  public static final int SCO_AUDIO_STATE_ERROR = -1;
  
  public static final int STREAM_ACCESSIBILITY = 10;
  
  public static final int STREAM_ALARM = 4;
  
  @SystemApi
  public static final int STREAM_ASSISTANT = 11;
  
  public static final int STREAM_BLUETOOTH_SCO = 6;
  
  public static final String STREAM_DEVICES_CHANGED_ACTION = "android.media.STREAM_DEVICES_CHANGED_ACTION";
  
  public static final int STREAM_DTMF = 8;
  
  public static final int STREAM_MUSIC = 3;
  
  public static final String STREAM_MUTE_CHANGED_ACTION = "android.media.STREAM_MUTE_CHANGED_ACTION";
  
  public static final int STREAM_NOTIFICATION = 5;
  
  public static final int STREAM_RING = 2;
  
  public static final int STREAM_SYSTEM = 1;
  
  public static final int STREAM_SYSTEM_ENFORCED = 7;
  
  public static final int STREAM_TTS = 9;
  
  public static final int STREAM_VOICE_CALL = 0;
  
  @SystemApi
  public static final int SUCCESS = 0;
  
  private static final String TAG = "AudioManager";
  
  public static final int USE_DEFAULT_STREAM_TYPE = -2147483648;
  
  public static final String VIBRATE_SETTING_CHANGED_ACTION = "android.media.VIBRATE_SETTING_CHANGED";
  
  public static final int VIBRATE_SETTING_OFF = 0;
  
  public static final int VIBRATE_SETTING_ON = 1;
  
  public static final int VIBRATE_SETTING_ONLY_SILENT = 2;
  
  public static final int VIBRATE_TYPE_NOTIFICATION = 1;
  
  public static final int VIBRATE_TYPE_RINGER = 0;
  
  public static final String VOLUME_CHANGED_ACTION = "android.media.VOLUME_CHANGED_ACTION";
  
  private static final float VOLUME_MIN_DB = -758.0F;
  
  private static final AudioVolumeGroupChangeHandler sAudioAudioVolumeGroupChangedHandler;
  
  static ArrayList<AudioPatch> sAudioPatchesCached;
  
  static Integer sAudioPortGeneration;
  
  static ArrayList<AudioPort> sAudioPortsCached;
  
  static ArrayList<AudioPort> sPreviousAudioPortsCached;
  
  private static IAudioService sService;
  
  private Context mApplicationContext;
  
  private AudioServerStateCallback mAudioServerStateCb;
  
  private Executor mAudioServerStateExec;
  
  private HashMap<String, BlockingFocusResultReceiver> mFocusRequestsAwaitingResult;
  
  private Context mOriginalContext;
  
  private List<AudioPlaybackCallbackInfo> mPlaybackCallbackList;
  
  private StrategyPreferredDeviceDispatcherStub mPrefDevDispatcherStub;
  
  private ArrayList<PrefDevListenerInfo> mPrefDevListeners;
  
  private List<AudioRecordingCallbackInfo> mRecordCallbackList;
  
  private final boolean mUseFixedVolume;
  
  private final boolean mUseVolumeKeySounds;
  
  private long mVolumeKeyUpTime;
  
  @SystemApi
  public void setAudioServerStateCallback(Executor paramExecutor, AudioServerStateCallback paramAudioServerStateCallback) {
    if (paramAudioServerStateCallback != null) {
      if (paramExecutor != null)
        synchronized (this.mAudioServerStateCbLock) {
          if (this.mAudioServerStateCb == null) {
            IAudioService iAudioService = getService();
            try {
              iAudioService.registerAudioServerStateDispatcher(this.mAudioServerStateDispatcher);
              this.mAudioServerStateExec = paramExecutor;
              this.mAudioServerStateCb = paramAudioServerStateCallback;
              return;
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            } 
          } 
          IllegalStateException illegalStateException = new IllegalStateException();
          this("setAudioServerStateCallback called with already registered callabck");
          throw illegalStateException;
        }  
      throw new IllegalArgumentException("Illegal null Executor for the AudioServerStateCallback");
    } 
    throw new IllegalArgumentException("Illegal null AudioServerStateCallback");
  }
  
  @SystemApi
  public void clearAudioServerStateCallback() {
    synchronized (this.mAudioServerStateCbLock) {
      if (this.mAudioServerStateCb != null) {
        IAudioService iAudioService = getService();
        try {
          iAudioService.unregisterAudioServerStateDispatcher(this.mAudioServerStateDispatcher);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
      this.mAudioServerStateExec = null;
      this.mAudioServerStateCb = null;
      return;
    } 
  }
  
  @SystemApi
  public boolean isAudioServerRunning() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.isAudioServerRunning();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Map<Integer, Boolean> getSurroundFormats() {
    StringBuilder stringBuilder;
    HashMap<Object, Object> hashMap = new HashMap<>();
    int i = AudioSystem.getSurroundFormats((Map)hashMap, false);
    if (i != 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("getSurroundFormats failed:");
      stringBuilder.append(i);
      Log.e("AudioManager", stringBuilder.toString());
      return new HashMap<>();
    } 
    return (Map<Integer, Boolean>)stringBuilder;
  }
  
  public boolean setSurroundFormatEnabled(int paramInt, boolean paramBoolean) {
    paramInt = AudioSystem.setSurroundFormatEnabled(paramInt, paramBoolean);
    if (paramInt == 0) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  public Map<Integer, Boolean> getReportedSurroundFormats() {
    StringBuilder stringBuilder;
    HashMap<Object, Object> hashMap = new HashMap<>();
    int i = AudioSystem.getSurroundFormats((Map)hashMap, true);
    if (i != 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("getReportedSurroundFormats failed:");
      stringBuilder.append(i);
      Log.e("AudioManager", stringBuilder.toString());
      return new HashMap<>();
    } 
    return (Map<Integer, Boolean>)stringBuilder;
  }
  
  public static boolean isHapticPlaybackSupported() {
    return AudioSystem.isHapticPlaybackSupported();
  }
  
  @SystemApi
  public static List<AudioProductStrategy> getAudioProductStrategies() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getAudioProductStrategies();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public static List<AudioVolumeGroup> getAudioVolumeGroups() {
    IAudioService iAudioService = getService();
    try {
      return iAudioService.getAudioVolumeGroups();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  class VolumeGroupCallback {
    public void onAudioVolumeGroupChanged(int param1Int1, int param1Int2) {}
  }
  
  @SystemApi
  public void registerVolumeGroupCallback(Executor paramExecutor, VolumeGroupCallback paramVolumeGroupCallback) {
    Preconditions.checkNotNull(paramExecutor, "executor must not be null");
    Preconditions.checkNotNull(paramVolumeGroupCallback, "volume group change cb must not be null");
    sAudioAudioVolumeGroupChangedHandler.init();
    sAudioAudioVolumeGroupChangedHandler.registerListener(paramVolumeGroupCallback);
  }
  
  @SystemApi
  public void unregisterVolumeGroupCallback(VolumeGroupCallback paramVolumeGroupCallback) {
    Preconditions.checkNotNull(paramVolumeGroupCallback, "volume group change cb must not be null");
    sAudioAudioVolumeGroupChangedHandler.unregisterListener(paramVolumeGroupCallback);
  }
  
  public static boolean hasHapticChannels(Uri paramUri) {
    try {
      return getService().hasHapticChannels(paramUri);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void setRttEnabled(boolean paramBoolean) {
    try {
      getService().setRttEnabled(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setMultiAudioFocusEnabled(boolean paramBoolean) {
    try {
      getService().setMultiAudioFocusEnabled(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class NativeEventHandlerDelegate {
    private final Handler mHandler;
    
    final AudioManager this$0;
    
    NativeEventHandlerDelegate(final AudioDeviceCallback callback, Handler param1Handler) {
      Looper looper;
      if (param1Handler != null) {
        looper = param1Handler.getLooper();
      } else {
        looper = Looper.getMainLooper();
      } 
      if (looper != null) {
        this.mHandler = new Handler(looper) {
            final AudioManager.NativeEventHandlerDelegate this$1;
            
            final AudioDeviceCallback val$callback;
            
            final AudioManager val$this$0;
            
            Handler(Looper param1Looper) {
              super(param1Looper);
            }
            
            public void handleMessage(Message param1Message) {
              int i = param1Message.what;
              if (i != 0 && i != 1) {
                if (i != 2) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("Unknown native event type: ");
                  stringBuilder.append(param1Message.what);
                  Log.e("AudioManager", stringBuilder.toString());
                } else {
                  AudioDeviceCallback audioDeviceCallback = callback;
                  if (audioDeviceCallback != null)
                    audioDeviceCallback.onAudioDevicesRemoved((AudioDeviceInfo[])param1Message.obj); 
                } 
              } else {
                AudioDeviceCallback audioDeviceCallback = callback;
                if (audioDeviceCallback != null)
                  audioDeviceCallback.onAudioDevicesAdded((AudioDeviceInfo[])param1Message.obj); 
              } 
            }
          };
      } else {
        this.mHandler = null;
      } 
    }
    
    Handler getHandler() {
      return this.mHandler;
    }
  }
  
  class null extends Handler {
    final AudioManager.NativeEventHandlerDelegate this$1;
    
    final AudioDeviceCallback val$callback;
    
    final AudioManager val$this$0;
    
    null(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 0 && i != 1) {
        if (i != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown native event type: ");
          stringBuilder.append(param1Message.what);
          Log.e("AudioManager", stringBuilder.toString());
        } else {
          AudioDeviceCallback audioDeviceCallback = callback;
          if (audioDeviceCallback != null)
            audioDeviceCallback.onAudioDevicesRemoved((AudioDeviceInfo[])param1Message.obj); 
        } 
      } else {
        AudioDeviceCallback audioDeviceCallback = callback;
        if (audioDeviceCallback != null)
          audioDeviceCallback.onAudioDevicesAdded((AudioDeviceInfo[])param1Message.obj); 
      } 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AudioDeviceRole implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class AudioMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class DeviceVolumeBehavior implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class DeviceVolumeBehaviorState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class FocusRequestResult implements Annotation {}
  
  class OnAudioFocusChangeListener {
    public abstract void onAudioFocusChange(int param1Int);
  }
  
  class OnAudioPortUpdateListener {
    public abstract void onAudioPatchListUpdate(AudioPatch[] param1ArrayOfAudioPatch);
    
    public abstract void onAudioPortListUpdate(AudioPort[] param1ArrayOfAudioPort);
    
    public abstract void onServiceDied();
  }
  
  @SystemApi
  class OnPreferredDeviceForStrategyChangedListener {
    public abstract void onPreferredDeviceForStrategyChanged(AudioProductStrategy param1AudioProductStrategy, AudioDeviceAttributes param1AudioDeviceAttributes);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class PublicStreamTypes implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class VolumeAdjustment implements Annotation {}
}
