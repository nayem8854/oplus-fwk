package android.media;

import java.util.ArrayList;
import java.util.List;

public class EncoderCapabilities {
  private static final String TAG = "EncoderCapabilities";
  
  public static class VideoEncoderCap {
    public final int mCodec;
    
    public final int mMaxBitRate;
    
    public final int mMaxFrameHeight;
    
    public final int mMaxFrameRate;
    
    public final int mMaxFrameWidth;
    
    public final int mMinBitRate;
    
    public final int mMinFrameHeight;
    
    public final int mMinFrameRate;
    
    public final int mMinFrameWidth;
    
    private VideoEncoderCap(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, int param1Int8, int param1Int9) {
      this.mCodec = param1Int1;
      this.mMinBitRate = param1Int2;
      this.mMaxBitRate = param1Int3;
      this.mMinFrameRate = param1Int4;
      this.mMaxFrameRate = param1Int5;
      this.mMinFrameWidth = param1Int6;
      this.mMaxFrameWidth = param1Int7;
      this.mMinFrameHeight = param1Int8;
      this.mMaxFrameHeight = param1Int9;
    }
  }
  
  public static class AudioEncoderCap {
    public final int mCodec;
    
    public final int mMaxBitRate;
    
    public final int mMaxChannels;
    
    public final int mMaxSampleRate;
    
    public final int mMinBitRate;
    
    public final int mMinChannels;
    
    public final int mMinSampleRate;
    
    private AudioEncoderCap(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7) {
      this.mCodec = param1Int1;
      this.mMinBitRate = param1Int2;
      this.mMaxBitRate = param1Int3;
      this.mMinSampleRate = param1Int4;
      this.mMaxSampleRate = param1Int5;
      this.mMinChannels = param1Int6;
      this.mMaxChannels = param1Int7;
    }
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  public static int[] getOutputFileFormats() {
    int i = native_get_num_file_formats();
    if (i == 0)
      return null; 
    int[] arrayOfInt = new int[i];
    for (byte b = 0; b < i; b++)
      arrayOfInt[b] = native_get_file_format(b); 
    return arrayOfInt;
  }
  
  public static List<VideoEncoderCap> getVideoEncoders() {
    int i = native_get_num_video_encoders();
    if (i == 0)
      return null; 
    ArrayList<VideoEncoderCap> arrayList = new ArrayList();
    for (byte b = 0; b < i; b++)
      arrayList.add(native_get_video_encoder_cap(b)); 
    return arrayList;
  }
  
  public static List<AudioEncoderCap> getAudioEncoders() {
    int i = native_get_num_audio_encoders();
    if (i == 0)
      return null; 
    ArrayList<AudioEncoderCap> arrayList = new ArrayList();
    for (byte b = 0; b < i; b++)
      arrayList.add(native_get_audio_encoder_cap(b)); 
    return arrayList;
  }
  
  private static final native AudioEncoderCap native_get_audio_encoder_cap(int paramInt);
  
  private static final native int native_get_file_format(int paramInt);
  
  private static final native int native_get_num_audio_encoders();
  
  private static final native int native_get_num_file_formats();
  
  private static final native int native_get_num_video_encoders();
  
  private static final native VideoEncoderCap native_get_video_encoder_cap(int paramInt);
  
  private static final native void native_init();
}
