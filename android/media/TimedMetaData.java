package android.media;

import android.os.Parcel;

public final class TimedMetaData {
  private static final String TAG = "TimedMetaData";
  
  private byte[] mMetaData;
  
  private long mTimestampUs;
  
  static TimedMetaData createTimedMetaDataFromParcel(Parcel paramParcel) {
    return new TimedMetaData(paramParcel);
  }
  
  private TimedMetaData(Parcel paramParcel) {
    if (parseParcel(paramParcel))
      return; 
    throw new IllegalArgumentException("parseParcel() fails");
  }
  
  public TimedMetaData(long paramLong, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mTimestampUs = paramLong;
      this.mMetaData = paramArrayOfbyte;
      return;
    } 
    throw new IllegalArgumentException("null metaData is not allowed");
  }
  
  public long getTimestamp() {
    return this.mTimestampUs;
  }
  
  public byte[] getMetaData() {
    return this.mMetaData;
  }
  
  private boolean parseParcel(Parcel paramParcel) {
    paramParcel.setDataPosition(0);
    if (paramParcel.dataAvail() == 0)
      return false; 
    this.mTimestampUs = paramParcel.readLong();
    byte[] arrayOfByte = new byte[paramParcel.readInt()];
    paramParcel.readByteArray(arrayOfByte);
    return true;
  }
}
