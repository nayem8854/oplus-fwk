package android.media;

import java.util.ArrayList;
import java.util.List;

public class DecoderCapabilities {
  public enum VideoDecoder {
    VIDEO_DECODER_WMV;
    
    private static final VideoDecoder[] $VALUES;
    
    static {
      VideoDecoder videoDecoder = new VideoDecoder("VIDEO_DECODER_WMV", 0);
      $VALUES = new VideoDecoder[] { videoDecoder };
    }
  }
  
  public enum AudioDecoder {
    AUDIO_DECODER_WMA;
    
    private static final AudioDecoder[] $VALUES;
    
    static {
      AudioDecoder audioDecoder = new AudioDecoder("AUDIO_DECODER_WMA", 0);
      $VALUES = new AudioDecoder[] { audioDecoder };
    }
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  public static List<VideoDecoder> getVideoDecoders() {
    ArrayList<VideoDecoder> arrayList = new ArrayList();
    int i = native_get_num_video_decoders();
    for (byte b = 0; b < i; b++)
      arrayList.add(VideoDecoder.values()[native_get_video_decoder_type(b)]); 
    return arrayList;
  }
  
  public static List<AudioDecoder> getAudioDecoders() {
    ArrayList<AudioDecoder> arrayList = new ArrayList();
    int i = native_get_num_audio_decoders();
    for (byte b = 0; b < i; b++)
      arrayList.add(AudioDecoder.values()[native_get_audio_decoder_type(b)]); 
    return arrayList;
  }
  
  private static final native int native_get_audio_decoder_type(int paramInt);
  
  private static final native int native_get_num_audio_decoders();
  
  private static final native int native_get_num_video_decoders();
  
  private static final native int native_get_video_decoder_type(int paramInt);
  
  private static final native void native_init();
}
