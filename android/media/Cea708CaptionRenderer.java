package android.media;

import android.content.Context;

public class Cea708CaptionRenderer extends SubtitleController.Renderer {
  private Cea708CCWidget mCCWidget;
  
  private final Context mContext;
  
  public Cea708CaptionRenderer(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public boolean supports(MediaFormat paramMediaFormat) {
    if (paramMediaFormat.containsKey("mime")) {
      String str = paramMediaFormat.getString("mime");
      return "text/cea-708".equals(str);
    } 
    return false;
  }
  
  public SubtitleTrack createTrack(MediaFormat paramMediaFormat) {
    String str = paramMediaFormat.getString("mime");
    if ("text/cea-708".equals(str)) {
      if (this.mCCWidget == null)
        this.mCCWidget = new Cea708CCWidget(this.mContext); 
      return new Cea708CaptionTrack(this.mCCWidget, paramMediaFormat);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No matching format: ");
    stringBuilder.append(paramMediaFormat.toString());
    throw new RuntimeException(stringBuilder.toString());
  }
}
