package android.media;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import android.util.Pair;
import com.android.internal.util.ArrayUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import libcore.io.IoUtils;
import libcore.io.Streams;

public class ExifInterface {
  private static final boolean DEBUG = Log.isLoggable("ExifInterface", 3);
  
  private static final byte[] JPEG_SIGNATURE = new byte[] { -1, -40, -1 };
  
  static {
    HEIF_TYPE_FTYP = new byte[] { 102, 116, 121, 112 };
    HEIF_BRAND_MIF1 = new byte[] { 109, 105, 102, 49 };
    HEIF_BRAND_HEIC = new byte[] { 104, 101, 105, 99 };
    ORF_MAKER_NOTE_HEADER_1 = new byte[] { 79, 76, 89, 77, 80, 0 };
    ORF_MAKER_NOTE_HEADER_2 = new byte[] { 79, 76, 89, 77, 80, 85, 83, 0, 73, 73 };
    PNG_SIGNATURE = new byte[] { -119, 80, 78, 71, 13, 10, 26, 10 };
    PNG_CHUNK_TYPE_EXIF = new byte[] { 101, 88, 73, 102 };
    PNG_CHUNK_TYPE_IHDR = new byte[] { 73, 72, 68, 82 };
    PNG_CHUNK_TYPE_IEND = new byte[] { 73, 69, 78, 68 };
    WEBP_SIGNATURE_1 = new byte[] { 82, 73, 70, 70 };
    WEBP_SIGNATURE_2 = new byte[] { 87, 69, 66, 80 };
    WEBP_CHUNK_TYPE_EXIF = new byte[] { 69, 88, 73, 70 };
    IFD_FORMAT_NAMES = new String[] { 
        "", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", 
        "SRATIONAL", "SINGLE", "DOUBLE", "IFD" };
    IFD_FORMAT_BYTES_PER_FORMAT = new int[] { 
        0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 
        8, 4, 8, 1 };
    EXIF_ASCII_PREFIX = new byte[] { 65, 83, 67, 73, 73, 0, 0, 0 };
    BITS_PER_SAMPLE_RGB = new int[] { 8, 8, 8 };
    BITS_PER_SAMPLE_GREYSCALE_1 = new int[] { 4 };
    BITS_PER_SAMPLE_GREYSCALE_2 = new int[] { 8 };
    IFD_TIFF_TAGS = new ExifTag[] { 
        new ExifTag("NewSubfileType", 254, 4), new ExifTag("SubfileType", 255, 4), new ExifTag("ImageWidth", 256, 3, 4), new ExifTag("ImageLength", 257, 3, 4), new ExifTag("BitsPerSample", 258, 3), new ExifTag("Compression", 259, 3), new ExifTag("PhotometricInterpretation", 262, 3), new ExifTag("ImageDescription", 270, 2), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), 
        new ExifTag("StripOffsets", 273, 3, 4), new ExifTag("Orientation", 274, 3), new ExifTag("SamplesPerPixel", 277, 3), new ExifTag("RowsPerStrip", 278, 3, 4), new ExifTag("StripByteCounts", 279, 3, 4), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("PlanarConfiguration", 284, 3), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("TransferFunction", 301, 3), 
        new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("Artist", 315, 2), new ExifTag("WhitePoint", 318, 5), new ExifTag("PrimaryChromaticities", 319, 5), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("JPEGInterchangeFormat", 513, 4), new ExifTag("JPEGInterchangeFormatLength", 514, 4), new ExifTag("YCbCrCoefficients", 529, 5), new ExifTag("YCbCrSubSampling", 530, 3), 
        new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("ReferenceBlackWhite", 532, 5), new ExifTag("Copyright", 33432, 2), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("SensorTopBorder", 4, 4), new ExifTag("SensorLeftBorder", 5, 4), new ExifTag("SensorBottomBorder", 6, 4), new ExifTag("SensorRightBorder", 7, 4), new ExifTag("ISO", 23, 3), 
        new ExifTag("JpgFromRaw", 46, 7), new ExifTag("Xmp", 700, 1) };
    IFD_EXIF_TAGS = new ExifTag[] { 
        new ExifTag("ExposureTime", 33434, 5), new ExifTag("FNumber", 33437, 5), new ExifTag("ExposureProgram", 34850, 3), new ExifTag("SpectralSensitivity", 34852, 2), new ExifTag("ISOSpeedRatings", 34855, 3), new ExifTag("OECF", 34856, 7), new ExifTag("ExifVersion", 36864, 2), new ExifTag("DateTimeOriginal", 36867, 2), new ExifTag("DateTimeDigitized", 36868, 2), new ExifTag("OffsetTime", 36880, 2), 
        new ExifTag("OffsetTimeOriginal", 36881, 2), new ExifTag("OffsetTimeDigitized", 36882, 2), new ExifTag("ComponentsConfiguration", 37121, 7), new ExifTag("CompressedBitsPerPixel", 37122, 5), new ExifTag("ShutterSpeedValue", 37377, 10), new ExifTag("ApertureValue", 37378, 5), new ExifTag("BrightnessValue", 37379, 10), new ExifTag("ExposureBiasValue", 37380, 10), new ExifTag("MaxApertureValue", 37381, 5), new ExifTag("SubjectDistance", 37382, 5), 
        new ExifTag("MeteringMode", 37383, 3), new ExifTag("LightSource", 37384, 3), new ExifTag("Flash", 37385, 3), new ExifTag("FocalLength", 37386, 5), new ExifTag("SubjectArea", 37396, 3), new ExifTag("MakerNote", 37500, 7), new ExifTag("UserComment", 37510, 7), new ExifTag("SubSecTime", 37520, 2), new ExifTag("SubSecTimeOriginal", 37521, 2), new ExifTag("SubSecTimeDigitized", 37522, 2), 
        new ExifTag("FlashpixVersion", 40960, 7), new ExifTag("ColorSpace", 40961, 3), new ExifTag("PixelXDimension", 40962, 3, 4), new ExifTag("PixelYDimension", 40963, 3, 4), new ExifTag("RelatedSoundFile", 40964, 2), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("FlashEnergy", 41483, 5), new ExifTag("SpatialFrequencyResponse", 41484, 7), new ExifTag("FocalPlaneXResolution", 41486, 5), new ExifTag("FocalPlaneYResolution", 41487, 5), 
        new ExifTag("FocalPlaneResolutionUnit", 41488, 3), new ExifTag("SubjectLocation", 41492, 3), new ExifTag("ExposureIndex", 41493, 5), new ExifTag("SensingMethod", 41495, 3), new ExifTag("FileSource", 41728, 7), new ExifTag("SceneType", 41729, 7), new ExifTag("CFAPattern", 41730, 7), new ExifTag("CustomRendered", 41985, 3), new ExifTag("ExposureMode", 41986, 3), new ExifTag("WhiteBalance", 41987, 3), 
        new ExifTag("DigitalZoomRatio", 41988, 5), new ExifTag("FocalLengthIn35mmFilm", 41989, 3), new ExifTag("SceneCaptureType", 41990, 3), new ExifTag("GainControl", 41991, 3), new ExifTag("Contrast", 41992, 3), new ExifTag("Saturation", 41993, 3), new ExifTag("Sharpness", 41994, 3), new ExifTag("DeviceSettingDescription", 41995, 7), new ExifTag("SubjectDistanceRange", 41996, 3), new ExifTag("ImageUniqueID", 42016, 2), 
        new ExifTag("DNGVersion", 50706, 1), new ExifTag("DefaultCropSize", 50720, 3, 4) };
    IFD_GPS_TAGS = new ExifTag[] { 
        new ExifTag("GPSVersionID", 0, 1), new ExifTag("GPSLatitudeRef", 1, 2), new ExifTag("GPSLatitude", 2, 5), new ExifTag("GPSLongitudeRef", 3, 2), new ExifTag("GPSLongitude", 4, 5), new ExifTag("GPSAltitudeRef", 5, 1), new ExifTag("GPSAltitude", 6, 5), new ExifTag("GPSTimeStamp", 7, 5), new ExifTag("GPSSatellites", 8, 2), new ExifTag("GPSStatus", 9, 2), 
        new ExifTag("GPSMeasureMode", 10, 2), new ExifTag("GPSDOP", 11, 5), new ExifTag("GPSSpeedRef", 12, 2), new ExifTag("GPSSpeed", 13, 5), new ExifTag("GPSTrackRef", 14, 2), new ExifTag("GPSTrack", 15, 5), new ExifTag("GPSImgDirectionRef", 16, 2), new ExifTag("GPSImgDirection", 17, 5), new ExifTag("GPSMapDatum", 18, 2), new ExifTag("GPSDestLatitudeRef", 19, 2), 
        new ExifTag("GPSDestLatitude", 20, 5), new ExifTag("GPSDestLongitudeRef", 21, 2), new ExifTag("GPSDestLongitude", 22, 5), new ExifTag("GPSDestBearingRef", 23, 2), new ExifTag("GPSDestBearing", 24, 5), new ExifTag("GPSDestDistanceRef", 25, 2), new ExifTag("GPSDestDistance", 26, 5), new ExifTag("GPSProcessingMethod", 27, 7), new ExifTag("GPSAreaInformation", 28, 7), new ExifTag("GPSDateStamp", 29, 2), 
        new ExifTag("GPSDifferential", 30, 3) };
    IFD_INTEROPERABILITY_TAGS = new ExifTag[] { new ExifTag("InteroperabilityIndex", 1, 2) };
    IFD_THUMBNAIL_TAGS = new ExifTag[] { 
        new ExifTag("NewSubfileType", 254, 4), new ExifTag("SubfileType", 255, 4), new ExifTag("ThumbnailImageWidth", 256, 3, 4), new ExifTag("ThumbnailImageLength", 257, 3, 4), new ExifTag("BitsPerSample", 258, 3), new ExifTag("Compression", 259, 3), new ExifTag("PhotometricInterpretation", 262, 3), new ExifTag("ImageDescription", 270, 2), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), 
        new ExifTag("StripOffsets", 273, 3, 4), new ExifTag("ThumbnailOrientation", 274, 3), new ExifTag("SamplesPerPixel", 277, 3), new ExifTag("RowsPerStrip", 278, 3, 4), new ExifTag("StripByteCounts", 279, 3, 4), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("PlanarConfiguration", 284, 3), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("TransferFunction", 301, 3), 
        new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("Artist", 315, 2), new ExifTag("WhitePoint", 318, 5), new ExifTag("PrimaryChromaticities", 319, 5), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("JPEGInterchangeFormat", 513, 4), new ExifTag("JPEGInterchangeFormatLength", 514, 4), new ExifTag("YCbCrCoefficients", 529, 5), new ExifTag("YCbCrSubSampling", 530, 3), 
        new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("ReferenceBlackWhite", 532, 5), new ExifTag("Copyright", 33432, 2), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("DNGVersion", 50706, 1), new ExifTag("DefaultCropSize", 50720, 3, 4) };
    TAG_RAF_IMAGE_SIZE = new ExifTag("StripOffsets", 273, 3);
    ORF_MAKER_NOTE_TAGS = new ExifTag[] { new ExifTag("ThumbnailImage", 256, 7), new ExifTag("CameraSettingsIFDPointer", 8224, 4), new ExifTag("ImageProcessingIFDPointer", 8256, 4) };
    ORF_CAMERA_SETTINGS_TAGS = new ExifTag[] { new ExifTag("PreviewImageStart", 257, 4), new ExifTag("PreviewImageLength", 258, 4) };
    ORF_IMAGE_PROCESSING_TAGS = new ExifTag[] { new ExifTag("AspectFrame", 4371, 3) };
    ExifTag[] arrayOfExifTag1 = new ExifTag[1];
    arrayOfExifTag1[0] = new ExifTag("ColorSpace", 55, 3);
    PEF_TAGS = arrayOfExifTag1;
    ExifTag[] arrayOfExifTag2 = IFD_TIFF_TAGS;
    EXIF_TAGS = new ExifTag[][] { arrayOfExifTag2, IFD_EXIF_TAGS, IFD_GPS_TAGS, IFD_INTEROPERABILITY_TAGS, IFD_THUMBNAIL_TAGS, arrayOfExifTag2, ORF_MAKER_NOTE_TAGS, ORF_CAMERA_SETTINGS_TAGS, ORF_IMAGE_PROCESSING_TAGS, arrayOfExifTag1 };
    EXIF_POINTER_TAGS = new ExifTag[] { new ExifTag("SubIFDPointer", 330, 4), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("CameraSettingsIFDPointer", 8224, 1), new ExifTag("ImageProcessingIFDPointer", 8256, 1) };
    JPEG_INTERCHANGE_FORMAT_TAG = new ExifTag("JPEGInterchangeFormat", 513, 4);
    JPEG_INTERCHANGE_FORMAT_LENGTH_TAG = new ExifTag("JPEGInterchangeFormatLength", 514, 4);
    ExifTag[][] arrayOfExifTag = EXIF_TAGS;
    sExifTagMapsForReading = new HashMap[arrayOfExifTag.length];
    sExifTagMapsForWriting = new HashMap[arrayOfExifTag.length];
    sTagSetForCompatibility = new HashSet<>(Arrays.asList(new String[] { "FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp" }));
    sExifPointerTagMap = new HashMap<>();
    Charset charset = Charset.forName("US-ASCII");
    IDENTIFIER_EXIF_APP1 = "Exif\000\000".getBytes(charset);
    IDENTIFIER_XMP_APP1 = "http://ns.adobe.com/xap/1.0/\000".getBytes(ASCII);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    sFormatterTz = simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss XXX");
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    for (byte b = 0; b < EXIF_TAGS.length; b++) {
      sExifTagMapsForReading[b] = new HashMap<>();
      sExifTagMapsForWriting[b] = new HashMap<>();
      for (ExifTag exifTag : EXIF_TAGS[b]) {
        sExifTagMapsForReading[b].put(Integer.valueOf(exifTag.number), exifTag);
        sExifTagMapsForWriting[b].put(exifTag.name, exifTag);
      } 
    } 
    sExifPointerTagMap.put(Integer.valueOf((EXIF_POINTER_TAGS[0]).number), Integer.valueOf(5));
    sExifPointerTagMap.put(Integer.valueOf((EXIF_POINTER_TAGS[1]).number), Integer.valueOf(1));
    sExifPointerTagMap.put(Integer.valueOf((EXIF_POINTER_TAGS[2]).number), Integer.valueOf(2));
    sExifPointerTagMap.put(Integer.valueOf((EXIF_POINTER_TAGS[3]).number), Integer.valueOf(3));
    sExifPointerTagMap.put(Integer.valueOf((EXIF_POINTER_TAGS[4]).number), Integer.valueOf(7));
    sExifPointerTagMap.put(Integer.valueOf((EXIF_POINTER_TAGS[5]).number), Integer.valueOf(8));
    sNonZeroTimePattern = Pattern.compile(".*[1-9].*");
    sGpsTimestampPattern = Pattern.compile("^([0-9][0-9]):([0-9][0-9]):([0-9][0-9])$");
  }
  
  private int mReadImageFileDirectoryCount = 0;
  
  private boolean mIsProgressiveModePic = false;
  
  private static class Rational {
    public final long denominator;
    
    public final long numerator;
    
    private Rational(long param1Long1, long param1Long2) {
      if (param1Long2 == 0L) {
        this.numerator = 0L;
        this.denominator = 1L;
        return;
      } 
      this.numerator = param1Long1;
      this.denominator = param1Long2;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.numerator);
      stringBuilder.append("/");
      stringBuilder.append(this.denominator);
      return stringBuilder.toString();
    }
    
    public double calculate() {
      return this.numerator / this.denominator;
    }
  }
  
  private static class ExifAttribute {
    public static final long BYTES_OFFSET_UNKNOWN = -1L;
    
    public final byte[] bytes;
    
    public final long bytesOffset;
    
    public final int format;
    
    public final int numberOfComponents;
    
    private ExifAttribute(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte) {
      this(param1Int1, param1Int2, -1L, param1ArrayOfbyte);
    }
    
    private ExifAttribute(int param1Int1, int param1Int2, long param1Long, byte[] param1ArrayOfbyte) {
      this.format = param1Int1;
      this.numberOfComponents = param1Int2;
      this.bytesOffset = param1Long;
      this.bytes = param1ArrayOfbyte;
    }
    
    public static ExifAttribute createUShort(int[] param1ArrayOfint, ByteOrder param1ByteOrder) {
      byte[] arrayOfByte = new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[3] * param1ArrayOfint.length];
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
      byteBuffer.order(param1ByteOrder);
      int i;
      byte b;
      for (i = param1ArrayOfint.length, b = 0; b < i; ) {
        int j = param1ArrayOfint[b];
        byteBuffer.putShort((short)j);
        b++;
      } 
      return new ExifAttribute(3, param1ArrayOfint.length, byteBuffer.array());
    }
    
    public static ExifAttribute createUShort(int param1Int, ByteOrder param1ByteOrder) {
      return createUShort(new int[] { param1Int }, param1ByteOrder);
    }
    
    public static ExifAttribute createULong(long[] param1ArrayOflong, ByteOrder param1ByteOrder) {
      byte[] arrayOfByte = new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[4] * param1ArrayOflong.length];
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
      byteBuffer.order(param1ByteOrder);
      int i;
      byte b;
      for (i = param1ArrayOflong.length, b = 0; b < i; ) {
        long l = param1ArrayOflong[b];
        byteBuffer.putInt((int)l);
        b++;
      } 
      return new ExifAttribute(4, param1ArrayOflong.length, byteBuffer.array());
    }
    
    public static ExifAttribute createULong(long param1Long, ByteOrder param1ByteOrder) {
      return createULong(new long[] { param1Long }, param1ByteOrder);
    }
    
    public static ExifAttribute createSLong(int[] param1ArrayOfint, ByteOrder param1ByteOrder) {
      byte[] arrayOfByte = new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[9] * param1ArrayOfint.length];
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
      byteBuffer.order(param1ByteOrder);
      int i;
      byte b;
      for (i = param1ArrayOfint.length, b = 0; b < i; ) {
        int j = param1ArrayOfint[b];
        byteBuffer.putInt(j);
        b++;
      } 
      return new ExifAttribute(9, param1ArrayOfint.length, byteBuffer.array());
    }
    
    public static ExifAttribute createSLong(int param1Int, ByteOrder param1ByteOrder) {
      return createSLong(new int[] { param1Int }, param1ByteOrder);
    }
    
    public static ExifAttribute createByte(String param1String) {
      if (param1String.length() == 1 && param1String.charAt(0) >= '0' && param1String.charAt(0) <= '1') {
        byte[] arrayOfByte1 = new byte[1];
        arrayOfByte1[0] = (byte)(param1String.charAt(0) - 48);
        return new ExifAttribute(1, arrayOfByte1.length, arrayOfByte1);
      } 
      byte[] arrayOfByte = param1String.getBytes(ExifInterface.ASCII);
      return new ExifAttribute(1, arrayOfByte.length, arrayOfByte);
    }
    
    public static ExifAttribute createString(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append(false);
      byte[] arrayOfByte = stringBuilder.toString().getBytes(ExifInterface.ASCII);
      return new ExifAttribute(2, arrayOfByte.length, arrayOfByte);
    }
    
    public static ExifAttribute createURational(ExifInterface.Rational[] param1ArrayOfRational, ByteOrder param1ByteOrder) {
      byte[] arrayOfByte = new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[5] * param1ArrayOfRational.length];
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
      byteBuffer.order(param1ByteOrder);
      int i;
      byte b;
      for (i = param1ArrayOfRational.length, b = 0; b < i; ) {
        ExifInterface.Rational rational = param1ArrayOfRational[b];
        byteBuffer.putInt((int)rational.numerator);
        byteBuffer.putInt((int)rational.denominator);
        b++;
      } 
      return new ExifAttribute(5, param1ArrayOfRational.length, byteBuffer.array());
    }
    
    public static ExifAttribute createURational(ExifInterface.Rational param1Rational, ByteOrder param1ByteOrder) {
      return createURational(new ExifInterface.Rational[] { param1Rational }, param1ByteOrder);
    }
    
    public static ExifAttribute createSRational(ExifInterface.Rational[] param1ArrayOfRational, ByteOrder param1ByteOrder) {
      byte[] arrayOfByte = new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[10] * param1ArrayOfRational.length];
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
      byteBuffer.order(param1ByteOrder);
      int i;
      byte b;
      for (i = param1ArrayOfRational.length, b = 0; b < i; ) {
        ExifInterface.Rational rational = param1ArrayOfRational[b];
        byteBuffer.putInt((int)rational.numerator);
        byteBuffer.putInt((int)rational.denominator);
        b++;
      } 
      return new ExifAttribute(10, param1ArrayOfRational.length, byteBuffer.array());
    }
    
    public static ExifAttribute createSRational(ExifInterface.Rational param1Rational, ByteOrder param1ByteOrder) {
      return createSRational(new ExifInterface.Rational[] { param1Rational }, param1ByteOrder);
    }
    
    public static ExifAttribute createDouble(double[] param1ArrayOfdouble, ByteOrder param1ByteOrder) {
      byte[] arrayOfByte = new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[12] * param1ArrayOfdouble.length];
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
      byteBuffer.order(param1ByteOrder);
      int i;
      byte b;
      for (i = param1ArrayOfdouble.length, b = 0; b < i; ) {
        double d = param1ArrayOfdouble[b];
        byteBuffer.putDouble(d);
        b++;
      } 
      return new ExifAttribute(12, param1ArrayOfdouble.length, byteBuffer.array());
    }
    
    public static ExifAttribute createDouble(double param1Double, ByteOrder param1ByteOrder) {
      return createDouble(new double[] { param1Double }, param1ByteOrder);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("(");
      stringBuilder.append(ExifInterface.IFD_FORMAT_NAMES[this.format]);
      stringBuilder.append(", data length:");
      stringBuilder.append(this.bytes.length);
      stringBuilder.append(")");
      return stringBuilder.toString();
    }
    
    private Object getValue(ByteOrder param1ByteOrder) {
      try {
        ExifInterface.ByteOrderedDataInputStream byteOrderedDataInputStream = new ExifInterface.ByteOrderedDataInputStream();
        this(this.bytes);
        try {
          double[] arrayOfDouble;
          ExifInterface.Rational[] arrayOfRational2;
          int[] arrayOfInt2;
          ExifInterface.Rational[] arrayOfRational1;
          long[] arrayOfLong;
          int[] arrayOfInt1;
          StringBuilder stringBuilder;
          int i;
          byte b;
          byteOrderedDataInputStream.setByteOrder(param1ByteOrder);
          switch (this.format) {
            default:
              return null;
            case 12:
              arrayOfDouble = new double[this.numberOfComponents];
              for (i = 0; i < this.numberOfComponents; i++)
                arrayOfDouble[i] = byteOrderedDataInputStream.readDouble(); 
              return arrayOfDouble;
            case 11:
              arrayOfDouble = new double[this.numberOfComponents];
              for (i = 0; i < this.numberOfComponents; i++)
                arrayOfDouble[i] = byteOrderedDataInputStream.readFloat(); 
              return arrayOfDouble;
            case 10:
              arrayOfRational2 = new ExifInterface.Rational[this.numberOfComponents];
              for (i = 0; i < this.numberOfComponents; i++) {
                long l1 = byteOrderedDataInputStream.readInt();
                long l2 = byteOrderedDataInputStream.readInt();
                arrayOfRational2[i] = new ExifInterface.Rational(l1, l2);
              } 
              return arrayOfRational2;
            case 9:
              arrayOfInt2 = new int[this.numberOfComponents];
              for (i = 0; i < this.numberOfComponents; i++)
                arrayOfInt2[i] = byteOrderedDataInputStream.readInt(); 
              return arrayOfInt2;
            case 8:
              arrayOfInt2 = new int[this.numberOfComponents];
              for (i = 0; i < this.numberOfComponents; i++)
                arrayOfInt2[i] = byteOrderedDataInputStream.readShort(); 
              return arrayOfInt2;
            case 5:
              arrayOfRational1 = new ExifInterface.Rational[this.numberOfComponents];
              for (i = 0; i < this.numberOfComponents; i++) {
                long l1 = byteOrderedDataInputStream.readUnsignedInt();
                long l2 = byteOrderedDataInputStream.readUnsignedInt();
                arrayOfRational1[i] = new ExifInterface.Rational(l1, l2);
              } 
              return arrayOfRational1;
            case 4:
              arrayOfLong = new long[this.numberOfComponents];
              for (i = 0; i < this.numberOfComponents; i++)
                arrayOfLong[i] = byteOrderedDataInputStream.readUnsignedInt(); 
              return arrayOfLong;
            case 3:
              arrayOfInt1 = new int[this.numberOfComponents];
              for (i = 0; i < this.numberOfComponents; i++)
                arrayOfInt1[i] = byteOrderedDataInputStream.readUnsignedShort(); 
              return arrayOfInt1;
            case 2:
            case 7:
              b = 0;
              i = b;
              if (this.numberOfComponents >= ExifInterface.EXIF_ASCII_PREFIX.length) {
                boolean bool2, bool1 = true;
                i = 0;
                while (true) {
                  bool2 = bool1;
                  if (i < ExifInterface.EXIF_ASCII_PREFIX.length) {
                    if (this.bytes[i] != ExifInterface.EXIF_ASCII_PREFIX[i]) {
                      bool2 = false;
                      break;
                    } 
                    i++;
                    continue;
                  } 
                  break;
                } 
                i = b;
                if (bool2)
                  i = ExifInterface.EXIF_ASCII_PREFIX.length; 
              } 
              stringBuilder = new StringBuilder();
              this();
              while (i < this.numberOfComponents) {
                byte b1 = this.bytes[i];
                if (b1 == 0)
                  break; 
                if (b1 >= 32) {
                  stringBuilder.append((char)b1);
                } else {
                  stringBuilder.append('?');
                } 
                i++;
              } 
              return stringBuilder.toString();
            case 1:
            case 6:
              break;
          } 
          if (this.bytes.length == 1 && this.bytes[0] >= 0 && this.bytes[0] <= 1)
            return new String(new char[] { (char)(this.bytes[0] + 48) }); 
          return new String(this.bytes, ExifInterface.ASCII);
        } catch (IOException null) {}
      } catch (IOException iOException) {}
      Log.w("ExifInterface", "IOException occurred during reading a value", iOException);
      return null;
    }
    
    public double getDoubleValue(ByteOrder param1ByteOrder) {
      Object object = getValue(param1ByteOrder);
      if (object != null) {
        if (object instanceof String)
          return Double.parseDouble((String)object); 
        if (object instanceof long[]) {
          object = object;
          if (object.length == 1)
            return object[0]; 
          throw new NumberFormatException("There are more than one component");
        } 
        if (object instanceof int[]) {
          object = object;
          if (object.length == 1)
            return object[0]; 
          throw new NumberFormatException("There are more than one component");
        } 
        if (object instanceof double[]) {
          object = object;
          if (object.length == 1)
            return object[0]; 
          throw new NumberFormatException("There are more than one component");
        } 
        if (object instanceof ExifInterface.Rational[]) {
          object = object;
          if (object.length == 1)
            return object[0].calculate(); 
          throw new NumberFormatException("There are more than one component");
        } 
        throw new NumberFormatException("Couldn't find a double value");
      } 
      throw new NumberFormatException("NULL can't be converted to a double value");
    }
    
    public int getIntValue(ByteOrder param1ByteOrder) {
      Object object = getValue(param1ByteOrder);
      if (object != null) {
        if (object instanceof String)
          return Integer.parseInt((String)object); 
        if (object instanceof long[]) {
          object = object;
          if (object.length == 1)
            return (int)object[0]; 
          throw new NumberFormatException("There are more than one component");
        } 
        if (object instanceof int[]) {
          object = object;
          if (object.length == 1)
            return object[0]; 
          throw new NumberFormatException("There are more than one component");
        } 
        throw new NumberFormatException("Couldn't find a integer value");
      } 
      throw new NumberFormatException("NULL can't be converted to a integer value");
    }
    
    public String getStringValue(ByteOrder param1ByteOrder) {
      Object object = getValue(param1ByteOrder);
      if (object == null)
        return null; 
      if (object instanceof String)
        return (String)object; 
      StringBuilder stringBuilder = new StringBuilder();
      if (object instanceof long[]) {
        object = object;
        for (byte b = 0; b < object.length; b++) {
          stringBuilder.append(object[b]);
          if (b + 1 != object.length)
            stringBuilder.append(","); 
        } 
        return stringBuilder.toString();
      } 
      if (object instanceof int[]) {
        object = object;
        for (byte b = 0; b < object.length; b++) {
          stringBuilder.append(object[b]);
          if (b + 1 != object.length)
            stringBuilder.append(","); 
        } 
        return stringBuilder.toString();
      } 
      if (object instanceof double[]) {
        object = object;
        for (byte b = 0; b < object.length; b++) {
          stringBuilder.append(object[b]);
          if (b + 1 != object.length)
            stringBuilder.append(","); 
        } 
        return stringBuilder.toString();
      } 
      if (object instanceof ExifInterface.Rational[]) {
        object = object;
        for (byte b = 0; b < object.length; b++) {
          stringBuilder.append(((ExifInterface.Rational)object[b]).numerator);
          stringBuilder.append('/');
          stringBuilder.append(((ExifInterface.Rational)object[b]).denominator);
          if (b + 1 != object.length)
            stringBuilder.append(","); 
        } 
        return stringBuilder.toString();
      } 
      return null;
    }
    
    public int size() {
      return ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[this.format] * this.numberOfComponents;
    }
  }
  
  private static class ExifTag {
    public final String name;
    
    public final int number;
    
    public final int primaryFormat;
    
    public final int secondaryFormat;
    
    private ExifTag(String param1String, int param1Int1, int param1Int2) {
      this.name = param1String;
      this.number = param1Int1;
      this.primaryFormat = param1Int2;
      this.secondaryFormat = -1;
    }
    
    private ExifTag(String param1String, int param1Int1, int param1Int2, int param1Int3) {
      this.name = param1String;
      this.number = param1Int1;
      this.primaryFormat = param1Int2;
      this.secondaryFormat = param1Int3;
    }
  }
  
  private final HashMap[] mAttributes = new HashMap[EXIF_TAGS.length];
  
  private Set<Integer> mHandledIfdOffsets = new HashSet<>(EXIF_TAGS.length);
  
  private ByteOrder mExifByteOrder = ByteOrder.BIG_ENDIAN;
  
  private static final Charset ASCII;
  
  private static final int[] BITS_PER_SAMPLE_GREYSCALE_1;
  
  private static final int[] BITS_PER_SAMPLE_GREYSCALE_2;
  
  private static final int[] BITS_PER_SAMPLE_RGB;
  
  private static final short BYTE_ALIGN_II = 18761;
  
  private static final short BYTE_ALIGN_MM = 19789;
  
  private static final int DATA_DEFLATE_ZIP = 8;
  
  private static final int DATA_HUFFMAN_COMPRESSED = 2;
  
  private static final int DATA_JPEG = 6;
  
  private static final int DATA_JPEG_COMPRESSED = 7;
  
  private static final int DATA_LOSSY_JPEG = 34892;
  
  private static final int DATA_PACK_BITS_COMPRESSED = 32773;
  
  private static final int DATA_UNCOMPRESSED = 1;
  
  private static final byte[] EXIF_ASCII_PREFIX;
  
  private static final ExifTag[] EXIF_POINTER_TAGS;
  
  private static final ExifTag[][] EXIF_TAGS;
  
  private static final byte[] HEIF_BRAND_HEIC;
  
  private static final byte[] HEIF_BRAND_MIF1;
  
  private static final byte[] HEIF_TYPE_FTYP;
  
  private static final byte[] IDENTIFIER_EXIF_APP1;
  
  private static final byte[] IDENTIFIER_XMP_APP1;
  
  private static final ExifTag[] IFD_EXIF_TAGS;
  
  private static final int IFD_FORMAT_BYTE = 1;
  
  private static final int[] IFD_FORMAT_BYTES_PER_FORMAT;
  
  private static final int IFD_FORMAT_DOUBLE = 12;
  
  private static final int IFD_FORMAT_IFD = 13;
  
  private static final String[] IFD_FORMAT_NAMES;
  
  private static final int IFD_FORMAT_SBYTE = 6;
  
  private static final int IFD_FORMAT_SINGLE = 11;
  
  private static final int IFD_FORMAT_SLONG = 9;
  
  private static final int IFD_FORMAT_SRATIONAL = 10;
  
  private static final int IFD_FORMAT_SSHORT = 8;
  
  private static final int IFD_FORMAT_STRING = 2;
  
  private static final int IFD_FORMAT_ULONG = 4;
  
  private static final int IFD_FORMAT_UNDEFINED = 7;
  
  private static final int IFD_FORMAT_URATIONAL = 5;
  
  private static final int IFD_FORMAT_USHORT = 3;
  
  private static final ExifTag[] IFD_GPS_TAGS;
  
  private static final ExifTag[] IFD_INTEROPERABILITY_TAGS;
  
  private static final int IFD_OFFSET = 8;
  
  private static final ExifTag[] IFD_THUMBNAIL_TAGS;
  
  private static final ExifTag[] IFD_TIFF_TAGS;
  
  private static final int IFD_TYPE_EXIF = 1;
  
  private static final int IFD_TYPE_GPS = 2;
  
  private static final int IFD_TYPE_INTEROPERABILITY = 3;
  
  private static final int IFD_TYPE_ORF_CAMERA_SETTINGS = 7;
  
  private static final int IFD_TYPE_ORF_IMAGE_PROCESSING = 8;
  
  private static final int IFD_TYPE_ORF_MAKER_NOTE = 6;
  
  private static final int IFD_TYPE_PEF = 9;
  
  private static final int IFD_TYPE_PREVIEW = 5;
  
  private static final int IFD_TYPE_PRIMARY = 0;
  
  private static final int IFD_TYPE_THUMBNAIL = 4;
  
  private static final int IMAGE_TYPE_ARW = 1;
  
  private static final int IMAGE_TYPE_CR2 = 2;
  
  private static final int IMAGE_TYPE_DNG = 3;
  
  private static final int IMAGE_TYPE_HEIF = 12;
  
  private static final int IMAGE_TYPE_JPEG = 4;
  
  private static final int IMAGE_TYPE_NEF = 5;
  
  private static final int IMAGE_TYPE_NRW = 6;
  
  private static final int IMAGE_TYPE_ORF = 7;
  
  private static final int IMAGE_TYPE_PEF = 8;
  
  private static final int IMAGE_TYPE_PNG = 13;
  
  private static final int IMAGE_TYPE_RAF = 9;
  
  private static final int IMAGE_TYPE_RW2 = 10;
  
  private static final int IMAGE_TYPE_SRW = 11;
  
  private static final int IMAGE_TYPE_UNKNOWN = 0;
  
  private static final int IMAGE_TYPE_WEBP = 14;
  
  private static final ExifTag JPEG_INTERCHANGE_FORMAT_LENGTH_TAG;
  
  private static final ExifTag JPEG_INTERCHANGE_FORMAT_TAG;
  
  private static final byte MARKER = -1;
  
  private static final byte MARKER_APP1 = -31;
  
  private static final byte MARKER_COM = -2;
  
  private static final byte MARKER_EOI = -39;
  
  private static final byte MARKER_SOF0 = -64;
  
  private static final byte MARKER_SOF1 = -63;
  
  private static final byte MARKER_SOF10 = -54;
  
  private static final byte MARKER_SOF11 = -53;
  
  private static final byte MARKER_SOF13 = -51;
  
  private static final byte MARKER_SOF14 = -50;
  
  private static final byte MARKER_SOF15 = -49;
  
  private static final byte MARKER_SOF2 = -62;
  
  private static final byte MARKER_SOF3 = -61;
  
  private static final byte MARKER_SOF5 = -59;
  
  private static final byte MARKER_SOF6 = -58;
  
  private static final byte MARKER_SOF7 = -57;
  
  private static final byte MARKER_SOF9 = -55;
  
  private static final byte MARKER_SOI = -40;
  
  private static final byte MARKER_SOS = -38;
  
  private static final int MAX_THUMBNAIL_SIZE = 512;
  
  private static final ExifTag[] ORF_CAMERA_SETTINGS_TAGS;
  
  private static final ExifTag[] ORF_IMAGE_PROCESSING_TAGS;
  
  private static final byte[] ORF_MAKER_NOTE_HEADER_1;
  
  private static final int ORF_MAKER_NOTE_HEADER_1_SIZE = 8;
  
  private static final byte[] ORF_MAKER_NOTE_HEADER_2;
  
  private static final int ORF_MAKER_NOTE_HEADER_2_SIZE = 12;
  
  private static final ExifTag[] ORF_MAKER_NOTE_TAGS;
  
  private static final short ORF_SIGNATURE_1 = 20306;
  
  private static final short ORF_SIGNATURE_2 = 21330;
  
  public static final int ORIENTATION_FLIP_HORIZONTAL = 2;
  
  public static final int ORIENTATION_FLIP_VERTICAL = 4;
  
  public static final int ORIENTATION_NORMAL = 1;
  
  public static final int ORIENTATION_ROTATE_180 = 3;
  
  public static final int ORIENTATION_ROTATE_270 = 8;
  
  public static final int ORIENTATION_ROTATE_90 = 6;
  
  public static final int ORIENTATION_TRANSPOSE = 5;
  
  public static final int ORIENTATION_TRANSVERSE = 7;
  
  public static final int ORIENTATION_UNDEFINED = 0;
  
  private static final int ORIGINAL_RESOLUTION_IMAGE = 0;
  
  private static final int PEF_MAKER_NOTE_SKIP_SIZE = 6;
  
  private static final String PEF_SIGNATURE = "PENTAX";
  
  private static final ExifTag[] PEF_TAGS;
  
  private static final int PHOTOMETRIC_INTERPRETATION_BLACK_IS_ZERO = 1;
  
  private static final int PHOTOMETRIC_INTERPRETATION_RGB = 2;
  
  private static final int PHOTOMETRIC_INTERPRETATION_WHITE_IS_ZERO = 0;
  
  private static final int PHOTOMETRIC_INTERPRETATION_YCBCR = 6;
  
  private static final int PNG_CHUNK_CRC_BYTE_LENGTH = 4;
  
  private static final int PNG_CHUNK_TYPE_BYTE_LENGTH = 4;
  
  private static final byte[] PNG_CHUNK_TYPE_EXIF;
  
  private static final byte[] PNG_CHUNK_TYPE_IEND;
  
  private static final byte[] PNG_CHUNK_TYPE_IHDR;
  
  private static final byte[] PNG_SIGNATURE;
  
  private static final int RAF_INFO_SIZE = 160;
  
  private static final int RAF_JPEG_LENGTH_VALUE_SIZE = 4;
  
  private static final int RAF_OFFSET_TO_JPEG_IMAGE_OFFSET = 84;
  
  private static final String RAF_SIGNATURE = "FUJIFILMCCD-RAW";
  
  private static final int READ_IMAGE_DIRECTORY_COUNT = 512;
  
  private static final int REDUCED_RESOLUTION_IMAGE = 1;
  
  private static final short RW2_SIGNATURE = 85;
  
  private static final int SIGNATURE_CHECK_SIZE = 5000;
  
  private static final byte START_CODE = 42;
  
  public static final int STREAM_TYPE_EXIF_DATA_ONLY = 1;
  
  public static final int STREAM_TYPE_FULL_IMAGE_DATA = 0;
  
  private static final String TAG = "ExifInterface";
  
  @Deprecated
  public static final String TAG_APERTURE = "FNumber";
  
  public static final String TAG_APERTURE_VALUE = "ApertureValue";
  
  public static final String TAG_ARTIST = "Artist";
  
  public static final String TAG_BITS_PER_SAMPLE = "BitsPerSample";
  
  public static final String TAG_BRIGHTNESS_VALUE = "BrightnessValue";
  
  public static final String TAG_CFA_PATTERN = "CFAPattern";
  
  public static final String TAG_COLOR_SPACE = "ColorSpace";
  
  public static final String TAG_COMPONENTS_CONFIGURATION = "ComponentsConfiguration";
  
  public static final String TAG_COMPRESSED_BITS_PER_PIXEL = "CompressedBitsPerPixel";
  
  public static final String TAG_COMPRESSION = "Compression";
  
  public static final String TAG_CONTRAST = "Contrast";
  
  public static final String TAG_COPYRIGHT = "Copyright";
  
  public static final String TAG_CUSTOM_RENDERED = "CustomRendered";
  
  public static final String TAG_DATETIME = "DateTime";
  
  public static final String TAG_DATETIME_DIGITIZED = "DateTimeDigitized";
  
  public static final String TAG_DATETIME_ORIGINAL = "DateTimeOriginal";
  
  public static final String TAG_DEFAULT_CROP_SIZE = "DefaultCropSize";
  
  public static final String TAG_DEVICE_SETTING_DESCRIPTION = "DeviceSettingDescription";
  
  public static final String TAG_DIGITAL_ZOOM_RATIO = "DigitalZoomRatio";
  
  public static final String TAG_DNG_VERSION = "DNGVersion";
  
  private static final String TAG_EXIF_IFD_POINTER = "ExifIFDPointer";
  
  public static final String TAG_EXIF_VERSION = "ExifVersion";
  
  public static final String TAG_EXPOSURE_BIAS_VALUE = "ExposureBiasValue";
  
  public static final String TAG_EXPOSURE_INDEX = "ExposureIndex";
  
  public static final String TAG_EXPOSURE_MODE = "ExposureMode";
  
  public static final String TAG_EXPOSURE_PROGRAM = "ExposureProgram";
  
  public static final String TAG_EXPOSURE_TIME = "ExposureTime";
  
  public static final String TAG_FILE_SOURCE = "FileSource";
  
  public static final String TAG_FLASH = "Flash";
  
  public static final String TAG_FLASHPIX_VERSION = "FlashpixVersion";
  
  public static final String TAG_FLASH_ENERGY = "FlashEnergy";
  
  public static final String TAG_FOCAL_LENGTH = "FocalLength";
  
  public static final String TAG_FOCAL_LENGTH_IN_35MM_FILM = "FocalLengthIn35mmFilm";
  
  public static final String TAG_FOCAL_PLANE_RESOLUTION_UNIT = "FocalPlaneResolutionUnit";
  
  public static final String TAG_FOCAL_PLANE_X_RESOLUTION = "FocalPlaneXResolution";
  
  public static final String TAG_FOCAL_PLANE_Y_RESOLUTION = "FocalPlaneYResolution";
  
  public static final String TAG_F_NUMBER = "FNumber";
  
  public static final String TAG_GAIN_CONTROL = "GainControl";
  
  public static final String TAG_GPS_ALTITUDE = "GPSAltitude";
  
  public static final String TAG_GPS_ALTITUDE_REF = "GPSAltitudeRef";
  
  public static final String TAG_GPS_AREA_INFORMATION = "GPSAreaInformation";
  
  public static final String TAG_GPS_DATESTAMP = "GPSDateStamp";
  
  public static final String TAG_GPS_DEST_BEARING = "GPSDestBearing";
  
  public static final String TAG_GPS_DEST_BEARING_REF = "GPSDestBearingRef";
  
  public static final String TAG_GPS_DEST_DISTANCE = "GPSDestDistance";
  
  public static final String TAG_GPS_DEST_DISTANCE_REF = "GPSDestDistanceRef";
  
  public static final String TAG_GPS_DEST_LATITUDE = "GPSDestLatitude";
  
  public static final String TAG_GPS_DEST_LATITUDE_REF = "GPSDestLatitudeRef";
  
  public static final String TAG_GPS_DEST_LONGITUDE = "GPSDestLongitude";
  
  public static final String TAG_GPS_DEST_LONGITUDE_REF = "GPSDestLongitudeRef";
  
  public static final String TAG_GPS_DIFFERENTIAL = "GPSDifferential";
  
  public static final String TAG_GPS_DOP = "GPSDOP";
  
  public static final String TAG_GPS_IMG_DIRECTION = "GPSImgDirection";
  
  public static final String TAG_GPS_IMG_DIRECTION_REF = "GPSImgDirectionRef";
  
  private static final String TAG_GPS_INFO_IFD_POINTER = "GPSInfoIFDPointer";
  
  public static final String TAG_GPS_LATITUDE = "GPSLatitude";
  
  public static final String TAG_GPS_LATITUDE_REF = "GPSLatitudeRef";
  
  public static final String TAG_GPS_LONGITUDE = "GPSLongitude";
  
  public static final String TAG_GPS_LONGITUDE_REF = "GPSLongitudeRef";
  
  public static final String TAG_GPS_MAP_DATUM = "GPSMapDatum";
  
  public static final String TAG_GPS_MEASURE_MODE = "GPSMeasureMode";
  
  public static final String TAG_GPS_PROCESSING_METHOD = "GPSProcessingMethod";
  
  public static final String TAG_GPS_SATELLITES = "GPSSatellites";
  
  public static final String TAG_GPS_SPEED = "GPSSpeed";
  
  public static final String TAG_GPS_SPEED_REF = "GPSSpeedRef";
  
  public static final String TAG_GPS_STATUS = "GPSStatus";
  
  public static final String TAG_GPS_TIMESTAMP = "GPSTimeStamp";
  
  public static final String TAG_GPS_TRACK = "GPSTrack";
  
  public static final String TAG_GPS_TRACK_REF = "GPSTrackRef";
  
  public static final String TAG_GPS_VERSION_ID = "GPSVersionID";
  
  private static final String TAG_HAS_THUMBNAIL = "HasThumbnail";
  
  public static final String TAG_IMAGE_DESCRIPTION = "ImageDescription";
  
  public static final String TAG_IMAGE_LENGTH = "ImageLength";
  
  public static final String TAG_IMAGE_UNIQUE_ID = "ImageUniqueID";
  
  public static final String TAG_IMAGE_WIDTH = "ImageWidth";
  
  private static final String TAG_INTEROPERABILITY_IFD_POINTER = "InteroperabilityIFDPointer";
  
  public static final String TAG_INTEROPERABILITY_INDEX = "InteroperabilityIndex";
  
  @Deprecated
  public static final String TAG_ISO = "ISOSpeedRatings";
  
  public static final String TAG_ISO_SPEED_RATINGS = "ISOSpeedRatings";
  
  public static final String TAG_JPEG_INTERCHANGE_FORMAT = "JPEGInterchangeFormat";
  
  public static final String TAG_JPEG_INTERCHANGE_FORMAT_LENGTH = "JPEGInterchangeFormatLength";
  
  public static final String TAG_LIGHT_SOURCE = "LightSource";
  
  public static final String TAG_MAKE = "Make";
  
  public static final String TAG_MAKER_NOTE = "MakerNote";
  
  public static final String TAG_MAX_APERTURE_VALUE = "MaxApertureValue";
  
  public static final String TAG_METERING_MODE = "MeteringMode";
  
  public static final String TAG_MODEL = "Model";
  
  public static final String TAG_NEW_SUBFILE_TYPE = "NewSubfileType";
  
  public static final String TAG_OECF = "OECF";
  
  public static final String TAG_OFFSET_TIME = "OffsetTime";
  
  public static final String TAG_OFFSET_TIME_DIGITIZED = "OffsetTimeDigitized";
  
  public static final String TAG_OFFSET_TIME_ORIGINAL = "OffsetTimeOriginal";
  
  public static final String TAG_ORF_ASPECT_FRAME = "AspectFrame";
  
  private static final String TAG_ORF_CAMERA_SETTINGS_IFD_POINTER = "CameraSettingsIFDPointer";
  
  private static final String TAG_ORF_IMAGE_PROCESSING_IFD_POINTER = "ImageProcessingIFDPointer";
  
  public static final String TAG_ORF_PREVIEW_IMAGE_LENGTH = "PreviewImageLength";
  
  public static final String TAG_ORF_PREVIEW_IMAGE_START = "PreviewImageStart";
  
  public static final String TAG_ORF_THUMBNAIL_IMAGE = "ThumbnailImage";
  
  public static final String TAG_ORIENTATION = "Orientation";
  
  public static final String TAG_PHOTOMETRIC_INTERPRETATION = "PhotometricInterpretation";
  
  public static final String TAG_PIXEL_X_DIMENSION = "PixelXDimension";
  
  public static final String TAG_PIXEL_Y_DIMENSION = "PixelYDimension";
  
  public static final String TAG_PLANAR_CONFIGURATION = "PlanarConfiguration";
  
  public static final String TAG_PRIMARY_CHROMATICITIES = "PrimaryChromaticities";
  
  private static final ExifTag TAG_RAF_IMAGE_SIZE;
  
  public static final String TAG_REFERENCE_BLACK_WHITE = "ReferenceBlackWhite";
  
  public static final String TAG_RELATED_SOUND_FILE = "RelatedSoundFile";
  
  public static final String TAG_RESOLUTION_UNIT = "ResolutionUnit";
  
  public static final String TAG_ROWS_PER_STRIP = "RowsPerStrip";
  
  public static final String TAG_RW2_ISO = "ISO";
  
  public static final String TAG_RW2_JPG_FROM_RAW = "JpgFromRaw";
  
  public static final String TAG_RW2_SENSOR_BOTTOM_BORDER = "SensorBottomBorder";
  
  public static final String TAG_RW2_SENSOR_LEFT_BORDER = "SensorLeftBorder";
  
  public static final String TAG_RW2_SENSOR_RIGHT_BORDER = "SensorRightBorder";
  
  public static final String TAG_RW2_SENSOR_TOP_BORDER = "SensorTopBorder";
  
  public static final String TAG_SAMPLES_PER_PIXEL = "SamplesPerPixel";
  
  public static final String TAG_SATURATION = "Saturation";
  
  public static final String TAG_SCENE_CAPTURE_TYPE = "SceneCaptureType";
  
  public static final String TAG_SCENE_TYPE = "SceneType";
  
  public static final String TAG_SENSING_METHOD = "SensingMethod";
  
  public static final String TAG_SHARPNESS = "Sharpness";
  
  public static final String TAG_SHUTTER_SPEED_VALUE = "ShutterSpeedValue";
  
  public static final String TAG_SOFTWARE = "Software";
  
  public static final String TAG_SPATIAL_FREQUENCY_RESPONSE = "SpatialFrequencyResponse";
  
  public static final String TAG_SPECTRAL_SENSITIVITY = "SpectralSensitivity";
  
  public static final String TAG_STRIP_BYTE_COUNTS = "StripByteCounts";
  
  public static final String TAG_STRIP_OFFSETS = "StripOffsets";
  
  public static final String TAG_SUBFILE_TYPE = "SubfileType";
  
  public static final String TAG_SUBJECT_AREA = "SubjectArea";
  
  public static final String TAG_SUBJECT_DISTANCE = "SubjectDistance";
  
  public static final String TAG_SUBJECT_DISTANCE_RANGE = "SubjectDistanceRange";
  
  public static final String TAG_SUBJECT_LOCATION = "SubjectLocation";
  
  public static final String TAG_SUBSEC_TIME = "SubSecTime";
  
  public static final String TAG_SUBSEC_TIME_DIG = "SubSecTimeDigitized";
  
  public static final String TAG_SUBSEC_TIME_DIGITIZED = "SubSecTimeDigitized";
  
  public static final String TAG_SUBSEC_TIME_ORIG = "SubSecTimeOriginal";
  
  public static final String TAG_SUBSEC_TIME_ORIGINAL = "SubSecTimeOriginal";
  
  private static final String TAG_SUB_IFD_POINTER = "SubIFDPointer";
  
  private static final String TAG_THUMBNAIL_DATA = "ThumbnailData";
  
  public static final String TAG_THUMBNAIL_IMAGE_LENGTH = "ThumbnailImageLength";
  
  public static final String TAG_THUMBNAIL_IMAGE_WIDTH = "ThumbnailImageWidth";
  
  private static final String TAG_THUMBNAIL_LENGTH = "ThumbnailLength";
  
  private static final String TAG_THUMBNAIL_OFFSET = "ThumbnailOffset";
  
  public static final String TAG_THUMBNAIL_ORIENTATION = "ThumbnailOrientation";
  
  public static final String TAG_TRANSFER_FUNCTION = "TransferFunction";
  
  public static final String TAG_USER_COMMENT = "UserComment";
  
  public static final String TAG_WHITE_BALANCE = "WhiteBalance";
  
  public static final String TAG_WHITE_POINT = "WhitePoint";
  
  public static final String TAG_XMP = "Xmp";
  
  public static final String TAG_X_RESOLUTION = "XResolution";
  
  public static final String TAG_Y_CB_CR_COEFFICIENTS = "YCbCrCoefficients";
  
  public static final String TAG_Y_CB_CR_POSITIONING = "YCbCrPositioning";
  
  public static final String TAG_Y_CB_CR_SUB_SAMPLING = "YCbCrSubSampling";
  
  public static final String TAG_Y_RESOLUTION = "YResolution";
  
  private static final int WEBP_CHUNK_SIZE_BYTE_LENGTH = 4;
  
  private static final int WEBP_CHUNK_TYPE_BYTE_LENGTH = 4;
  
  private static final byte[] WEBP_CHUNK_TYPE_EXIF;
  
  private static final int WEBP_FILE_SIZE_BYTE_LENGTH = 4;
  
  private static final byte[] WEBP_SIGNATURE_1;
  
  private static final byte[] WEBP_SIGNATURE_2;
  
  public static final int WHITEBALANCE_AUTO = 0;
  
  public static final int WHITEBALANCE_MANUAL = 1;
  
  private static final HashMap<Integer, Integer> sExifPointerTagMap;
  
  private static final HashMap[] sExifTagMapsForReading;
  
  private static final HashMap[] sExifTagMapsForWriting;
  
  private static SimpleDateFormat sFormatter;
  
  private static SimpleDateFormat sFormatterTz;
  
  private static final Pattern sGpsTimestampPattern;
  
  private static final Pattern sNonZeroTimePattern;
  
  private static final HashSet<String> sTagSetForCompatibility;
  
  private boolean mAreThumbnailStripsConsecutive;
  
  private AssetManager.AssetInputStream mAssetInputStream;
  
  private int mExifOffset;
  
  private String mFilename;
  
  private boolean mHasThumbnail;
  
  private boolean mHasThumbnailStrips;
  
  private boolean mIsExifDataOnly;
  
  private boolean mIsInputStream;
  
  private boolean mIsSupportedFile;
  
  private int mMimeType;
  
  private boolean mModified;
  
  private int mOrfMakerNoteOffset;
  
  private int mOrfThumbnailLength;
  
  private int mOrfThumbnailOffset;
  
  private int mRw2JpgFromRawOffset;
  
  private FileDescriptor mSeekableFileDescriptor;
  
  private byte[] mThumbnailBytes;
  
  private int mThumbnailCompression;
  
  private int mThumbnailLength;
  
  private int mThumbnailOffset;
  
  private boolean mXmpIsFromSeparateMarker;
  
  public ExifInterface(File paramFile) throws IOException {
    if (paramFile != null) {
      initForFilename(paramFile.getAbsolutePath());
      return;
    } 
    throw new NullPointerException("file cannot be null");
  }
  
  public ExifInterface(String paramString) throws IOException {
    if (paramString != null) {
      initForFilename(paramString);
      return;
    } 
    throw new NullPointerException("filename cannot be null");
  }
  
  public ExifInterface(FileDescriptor paramFileDescriptor) throws IOException {
    if (paramFileDescriptor != null) {
      FileInputStream fileInputStream;
      this.mAssetInputStream = null;
      this.mFilename = null;
      boolean bool = false;
      if (isSeekableFD(paramFileDescriptor)) {
        this.mSeekableFileDescriptor = paramFileDescriptor;
        try {
          FileDescriptor fileDescriptor = Os.dup(paramFileDescriptor);
          bool = true;
        } catch (ErrnoException errnoException1) {
          throw errnoException1.rethrowAsIOException();
        } 
      } else {
        this.mSeekableFileDescriptor = null;
        null = errnoException1;
      } 
      this.mIsInputStream = false;
      ErrnoException errnoException2 = null;
      this.mReadImageFileDirectoryCount = 0;
      errnoException1 = errnoException2;
      try {
        FileInputStream fileInputStream2 = new FileInputStream();
        errnoException1 = errnoException2;
        this((FileDescriptor)null, bool);
        FileInputStream fileInputStream1 = fileInputStream2;
        fileInputStream = fileInputStream1;
        loadAttributes(fileInputStream1);
        return;
      } finally {
        IoUtils.closeQuietly(fileInputStream);
      } 
    } 
    throw new NullPointerException("fileDescriptor cannot be null");
  }
  
  public ExifInterface(InputStream paramInputStream) throws IOException {
    this(paramInputStream, false);
  }
  
  public ExifInterface(InputStream paramInputStream, int paramInt) throws IOException {
    this(paramInputStream, bool);
  }
  
  private ExifInterface(InputStream paramInputStream, boolean paramBoolean) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_0
    //   5: iconst_0
    //   6: putfield mReadImageFileDirectoryCount : I
    //   9: aload_0
    //   10: iconst_0
    //   11: putfield mIsProgressiveModePic : Z
    //   14: aload_0
    //   15: getstatic android/media/ExifInterface.EXIF_TAGS : [[Landroid/media/ExifInterface$ExifTag;
    //   18: arraylength
    //   19: anewarray java/util/HashMap
    //   22: putfield mAttributes : [Ljava/util/HashMap;
    //   25: aload_0
    //   26: new java/util/HashSet
    //   29: dup
    //   30: getstatic android/media/ExifInterface.EXIF_TAGS : [[Landroid/media/ExifInterface$ExifTag;
    //   33: arraylength
    //   34: invokespecial <init> : (I)V
    //   37: putfield mHandledIfdOffsets : Ljava/util/Set;
    //   40: aload_0
    //   41: getstatic java/nio/ByteOrder.BIG_ENDIAN : Ljava/nio/ByteOrder;
    //   44: putfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   47: aload_1
    //   48: ifnull -> 200
    //   51: aload_0
    //   52: aconst_null
    //   53: putfield mFilename : Ljava/lang/String;
    //   56: iload_2
    //   57: ifeq -> 110
    //   60: new java/io/BufferedInputStream
    //   63: dup
    //   64: aload_1
    //   65: sipush #5000
    //   68: invokespecial <init> : (Ljava/io/InputStream;I)V
    //   71: astore_1
    //   72: aload_1
    //   73: checkcast java/io/BufferedInputStream
    //   76: invokestatic isExifDataOnly : (Ljava/io/BufferedInputStream;)Z
    //   79: ifne -> 92
    //   82: ldc 'ExifInterface'
    //   84: ldc_w 'Given data does not follow the structure of an Exif-only data.'
    //   87: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   90: pop
    //   91: return
    //   92: aload_0
    //   93: iconst_1
    //   94: putfield mIsExifDataOnly : Z
    //   97: aload_0
    //   98: aconst_null
    //   99: putfield mAssetInputStream : Landroid/content/res/AssetManager$AssetInputStream;
    //   102: aload_0
    //   103: aconst_null
    //   104: putfield mSeekableFileDescriptor : Ljava/io/FileDescriptor;
    //   107: goto -> 184
    //   110: aload_1
    //   111: instanceof android/content/res/AssetManager$AssetInputStream
    //   114: ifeq -> 133
    //   117: aload_0
    //   118: aload_1
    //   119: checkcast android/content/res/AssetManager$AssetInputStream
    //   122: putfield mAssetInputStream : Landroid/content/res/AssetManager$AssetInputStream;
    //   125: aload_0
    //   126: aconst_null
    //   127: putfield mSeekableFileDescriptor : Ljava/io/FileDescriptor;
    //   130: goto -> 184
    //   133: aload_1
    //   134: instanceof java/io/FileInputStream
    //   137: ifeq -> 174
    //   140: aload_1
    //   141: checkcast java/io/FileInputStream
    //   144: astore_3
    //   145: aload_3
    //   146: invokevirtual getFD : ()Ljava/io/FileDescriptor;
    //   149: invokestatic isSeekableFD : (Ljava/io/FileDescriptor;)Z
    //   152: ifeq -> 174
    //   155: aload_0
    //   156: aconst_null
    //   157: putfield mAssetInputStream : Landroid/content/res/AssetManager$AssetInputStream;
    //   160: aload_0
    //   161: aload_1
    //   162: checkcast java/io/FileInputStream
    //   165: invokevirtual getFD : ()Ljava/io/FileDescriptor;
    //   168: putfield mSeekableFileDescriptor : Ljava/io/FileDescriptor;
    //   171: goto -> 184
    //   174: aload_0
    //   175: aconst_null
    //   176: putfield mAssetInputStream : Landroid/content/res/AssetManager$AssetInputStream;
    //   179: aload_0
    //   180: aconst_null
    //   181: putfield mSeekableFileDescriptor : Ljava/io/FileDescriptor;
    //   184: aload_0
    //   185: iconst_1
    //   186: putfield mIsInputStream : Z
    //   189: aload_0
    //   190: iconst_0
    //   191: putfield mReadImageFileDirectoryCount : I
    //   194: aload_0
    //   195: aload_1
    //   196: invokespecial loadAttributes : (Ljava/io/InputStream;)V
    //   199: return
    //   200: new java/lang/NullPointerException
    //   203: dup
    //   204: ldc_w 'inputStream cannot be null'
    //   207: invokespecial <init> : (Ljava/lang/String;)V
    //   210: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1592	-> 0
    //   #639	-> 4
    //   #683	-> 9
    //   #1452	-> 14
    //   #1454	-> 25
    //   #1455	-> 40
    //   #1593	-> 47
    //   #1596	-> 51
    //   #1598	-> 56
    //   #1599	-> 60
    //   #1600	-> 72
    //   #1601	-> 82
    //   #1602	-> 91
    //   #1604	-> 92
    //   #1605	-> 97
    //   #1606	-> 102
    //   #1608	-> 110
    //   #1609	-> 117
    //   #1610	-> 125
    //   #1611	-> 133
    //   #1612	-> 145
    //   #1613	-> 155
    //   #1614	-> 160
    //   #1616	-> 174
    //   #1617	-> 179
    //   #1620	-> 184
    //   #1624	-> 189
    //   #1626	-> 194
    //   #1627	-> 199
    //   #1594	-> 200
  }
  
  public static boolean isSupportedMimeType(String paramString) {
    if (paramString != null) {
      switch (paramString.toLowerCase(Locale.ROOT)) {
        default:
          return false;
        case "image/jpeg":
        case "image/x-adobe-dng":
        case "image/x-canon-cr2":
        case "image/x-nikon-nef":
        case "image/x-nikon-nrw":
        case "image/x-sony-arw":
        case "image/x-panasonic-rw2":
        case "image/x-olympus-orf":
        case "image/x-pentax-pef":
        case "image/x-samsung-srw":
        case "image/x-fuji-raf":
        case "image/heic":
        case "image/heif":
          break;
      } 
      return true;
    } 
    throw new NullPointerException("mimeType shouldn't be null");
  }
  
  private ExifAttribute getExifAttribute(String paramString) {
    if (paramString != null) {
      for (byte b = 0; b < EXIF_TAGS.length; b++) {
        ExifAttribute exifAttribute = (ExifAttribute)this.mAttributes[b].get(paramString);
        if (exifAttribute != null)
          return exifAttribute; 
      } 
      return null;
    } 
    throw new NullPointerException("tag shouldn't be null");
  }
  
  public String getAttribute(String paramString) {
    if (paramString != null) {
      ExifAttribute exifAttribute = getExifAttribute(paramString);
      if (exifAttribute != null) {
        if (!sTagSetForCompatibility.contains(paramString))
          return exifAttribute.getStringValue(this.mExifByteOrder); 
        if (paramString.equals("GPSTimeStamp")) {
          if (exifAttribute.format != 5 && exifAttribute.format != 10)
            return null; 
          Rational[] arrayOfRational = (Rational[])exifAttribute.getValue(this.mExifByteOrder);
          if (arrayOfRational.length != 3)
            return null; 
          int i = (int)((float)(arrayOfRational[0]).numerator / (float)(arrayOfRational[0]).denominator);
          int j = (int)((float)(arrayOfRational[1]).numerator / (float)(arrayOfRational[1]).denominator);
          int k = (int)((float)(arrayOfRational[2]).numerator / (float)(arrayOfRational[2]).denominator);
          return String.format("%02d:%02d:%02d", new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
        } 
        try {
          return Double.toString(exifAttribute.getDoubleValue(this.mExifByteOrder));
        } catch (NumberFormatException numberFormatException) {
          return null;
        } 
      } 
      return null;
    } 
    throw new NullPointerException("tag shouldn't be null");
  }
  
  public int getAttributeInt(String paramString, int paramInt) {
    if (paramString != null) {
      ExifAttribute exifAttribute = getExifAttribute(paramString);
      if (exifAttribute == null)
        return paramInt; 
      try {
        return exifAttribute.getIntValue(this.mExifByteOrder);
      } catch (NumberFormatException numberFormatException) {
        return paramInt;
      } 
    } 
    throw new NullPointerException("tag shouldn't be null");
  }
  
  public double getAttributeDouble(String paramString, double paramDouble) {
    if (paramString != null) {
      ExifAttribute exifAttribute = getExifAttribute(paramString);
      if (exifAttribute == null)
        return paramDouble; 
      try {
        return exifAttribute.getDoubleValue(this.mExifByteOrder);
      } catch (NumberFormatException numberFormatException) {
        return paramDouble;
      } 
    } 
    throw new NullPointerException("tag shouldn't be null");
  }
  
  public void setAttribute(String paramString1, String paramString2) {
    // Byte code:
    //   0: aload_0
    //   1: astore_3
    //   2: aload_2
    //   3: astore #4
    //   5: aload_1
    //   6: ifnull -> 1675
    //   9: iconst_1
    //   10: istore #5
    //   12: aload #4
    //   14: astore #6
    //   16: aload #4
    //   18: ifnull -> 289
    //   21: aload #4
    //   23: astore #6
    //   25: getstatic android/media/ExifInterface.sTagSetForCompatibility : Ljava/util/HashSet;
    //   28: aload_1
    //   29: invokevirtual contains : (Ljava/lang/Object;)Z
    //   32: ifeq -> 289
    //   35: aload_1
    //   36: ldc_w 'GPSTimeStamp'
    //   39: invokevirtual equals : (Ljava/lang/Object;)Z
    //   42: ifeq -> 197
    //   45: getstatic android/media/ExifInterface.sGpsTimestampPattern : Ljava/util/regex/Pattern;
    //   48: aload #4
    //   50: invokevirtual matcher : (Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   53: astore_2
    //   54: aload_2
    //   55: invokevirtual find : ()Z
    //   58: ifne -> 109
    //   61: new java/lang/StringBuilder
    //   64: dup
    //   65: invokespecial <init> : ()V
    //   68: astore_2
    //   69: aload_2
    //   70: ldc_w 'Invalid value for '
    //   73: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: pop
    //   77: aload_2
    //   78: aload_1
    //   79: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: aload_2
    //   84: ldc_w ' : '
    //   87: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   90: pop
    //   91: aload_2
    //   92: aload #4
    //   94: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: ldc 'ExifInterface'
    //   100: aload_2
    //   101: invokevirtual toString : ()Ljava/lang/String;
    //   104: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   107: pop
    //   108: return
    //   109: new java/lang/StringBuilder
    //   112: dup
    //   113: invokespecial <init> : ()V
    //   116: astore #6
    //   118: aload #6
    //   120: aload_2
    //   121: iconst_1
    //   122: invokevirtual group : (I)Ljava/lang/String;
    //   125: invokestatic parseInt : (Ljava/lang/String;)I
    //   128: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: aload #6
    //   134: ldc_w '/1,'
    //   137: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   140: pop
    //   141: aload #6
    //   143: aload_2
    //   144: iconst_2
    //   145: invokevirtual group : (I)Ljava/lang/String;
    //   148: invokestatic parseInt : (Ljava/lang/String;)I
    //   151: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   154: pop
    //   155: aload #6
    //   157: ldc_w '/1,'
    //   160: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload #6
    //   166: aload_2
    //   167: iconst_3
    //   168: invokevirtual group : (I)Ljava/lang/String;
    //   171: invokestatic parseInt : (Ljava/lang/String;)I
    //   174: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   177: pop
    //   178: aload #6
    //   180: ldc_w '/1'
    //   183: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   186: pop
    //   187: aload #6
    //   189: invokevirtual toString : ()Ljava/lang/String;
    //   192: astore #6
    //   194: goto -> 289
    //   197: aload_2
    //   198: invokestatic parseDouble : (Ljava/lang/String;)D
    //   201: dstore #7
    //   203: new java/lang/StringBuilder
    //   206: astore_2
    //   207: aload_2
    //   208: invokespecial <init> : ()V
    //   211: aload_2
    //   212: ldc2_w 10000.0
    //   215: dload #7
    //   217: dmul
    //   218: d2l
    //   219: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   222: pop
    //   223: aload_2
    //   224: ldc_w '/10000'
    //   227: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   230: pop
    //   231: aload_2
    //   232: invokevirtual toString : ()Ljava/lang/String;
    //   235: astore #6
    //   237: goto -> 289
    //   240: astore_2
    //   241: new java/lang/StringBuilder
    //   244: dup
    //   245: invokespecial <init> : ()V
    //   248: astore_2
    //   249: aload_2
    //   250: ldc_w 'Invalid value for '
    //   253: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   256: pop
    //   257: aload_2
    //   258: aload_1
    //   259: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: pop
    //   263: aload_2
    //   264: ldc_w ' : '
    //   267: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   270: pop
    //   271: aload_2
    //   272: aload #4
    //   274: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   277: pop
    //   278: ldc 'ExifInterface'
    //   280: aload_2
    //   281: invokevirtual toString : ()Ljava/lang/String;
    //   284: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   287: pop
    //   288: return
    //   289: iconst_0
    //   290: istore #9
    //   292: aload_3
    //   293: astore_2
    //   294: iload #9
    //   296: getstatic android/media/ExifInterface.EXIF_TAGS : [[Landroid/media/ExifInterface$ExifTag;
    //   299: arraylength
    //   300: if_icmpge -> 1674
    //   303: iload #9
    //   305: iconst_4
    //   306: if_icmpne -> 333
    //   309: aload_2
    //   310: getfield mHasThumbnail : Z
    //   313: ifne -> 333
    //   316: iload #5
    //   318: istore #10
    //   320: iload #9
    //   322: istore #5
    //   324: aload_2
    //   325: astore_3
    //   326: iload #10
    //   328: istore #9
    //   330: goto -> 1655
    //   333: getstatic android/media/ExifInterface.sExifTagMapsForWriting : [Ljava/util/HashMap;
    //   336: iload #9
    //   338: aaload
    //   339: aload_1
    //   340: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   343: astore #4
    //   345: aload #4
    //   347: ifnull -> 1641
    //   350: aload #6
    //   352: ifnonnull -> 384
    //   355: aload_2
    //   356: getfield mAttributes : [Ljava/util/HashMap;
    //   359: iload #9
    //   361: aaload
    //   362: aload_1
    //   363: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   366: pop
    //   367: iload #5
    //   369: istore #10
    //   371: iload #9
    //   373: istore #5
    //   375: aload_2
    //   376: astore_3
    //   377: iload #10
    //   379: istore #9
    //   381: goto -> 1655
    //   384: aload #4
    //   386: checkcast android/media/ExifInterface$ExifTag
    //   389: astore_3
    //   390: aload #6
    //   392: invokestatic guessDataFormat : (Ljava/lang/String;)Landroid/util/Pair;
    //   395: astore #11
    //   397: aload_3
    //   398: getfield primaryFormat : I
    //   401: aload #11
    //   403: getfield first : Ljava/lang/Object;
    //   406: checkcast java/lang/Integer
    //   409: invokevirtual intValue : ()I
    //   412: if_icmpeq -> 819
    //   415: aload_3
    //   416: getfield primaryFormat : I
    //   419: aload #11
    //   421: getfield second : Ljava/lang/Object;
    //   424: checkcast java/lang/Integer
    //   427: invokevirtual intValue : ()I
    //   430: if_icmpne -> 436
    //   433: goto -> 819
    //   436: aload_3
    //   437: getfield secondaryFormat : I
    //   440: iconst_m1
    //   441: if_icmpeq -> 497
    //   444: aload_3
    //   445: getfield secondaryFormat : I
    //   448: aload #11
    //   450: getfield first : Ljava/lang/Object;
    //   453: checkcast java/lang/Integer
    //   456: invokevirtual intValue : ()I
    //   459: if_icmpeq -> 488
    //   462: aload_3
    //   463: getfield secondaryFormat : I
    //   466: istore #10
    //   468: aload #11
    //   470: getfield second : Ljava/lang/Object;
    //   473: checkcast java/lang/Integer
    //   476: astore #12
    //   478: iload #10
    //   480: aload #12
    //   482: invokevirtual intValue : ()I
    //   485: if_icmpne -> 497
    //   488: aload_3
    //   489: getfield secondaryFormat : I
    //   492: istore #10
    //   494: goto -> 825
    //   497: aload_3
    //   498: getfield primaryFormat : I
    //   501: iload #5
    //   503: if_icmpeq -> 810
    //   506: aload_3
    //   507: getfield primaryFormat : I
    //   510: bipush #7
    //   512: if_icmpeq -> 810
    //   515: aload_3
    //   516: getfield primaryFormat : I
    //   519: iconst_2
    //   520: if_icmpne -> 526
    //   523: goto -> 810
    //   526: getstatic android/media/ExifInterface.DEBUG : Z
    //   529: ifeq -> 793
    //   532: new java/lang/StringBuilder
    //   535: dup
    //   536: invokespecial <init> : ()V
    //   539: astore #12
    //   541: aload #12
    //   543: ldc_w 'Given tag ('
    //   546: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   549: pop
    //   550: aload #12
    //   552: aload_1
    //   553: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   556: pop
    //   557: aload #12
    //   559: ldc_w ') value didn't match with one of expected formats: '
    //   562: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   565: pop
    //   566: aload #12
    //   568: getstatic android/media/ExifInterface.IFD_FORMAT_NAMES : [Ljava/lang/String;
    //   571: aload_3
    //   572: getfield primaryFormat : I
    //   575: aaload
    //   576: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   579: pop
    //   580: aload_3
    //   581: getfield secondaryFormat : I
    //   584: istore #10
    //   586: ldc_w ''
    //   589: astore #4
    //   591: iload #10
    //   593: iconst_m1
    //   594: if_icmpne -> 604
    //   597: ldc_w ''
    //   600: astore_3
    //   601: goto -> 642
    //   604: new java/lang/StringBuilder
    //   607: dup
    //   608: invokespecial <init> : ()V
    //   611: astore #13
    //   613: aload #13
    //   615: ldc_w ', '
    //   618: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   621: pop
    //   622: aload #13
    //   624: getstatic android/media/ExifInterface.IFD_FORMAT_NAMES : [Ljava/lang/String;
    //   627: aload_3
    //   628: getfield secondaryFormat : I
    //   631: aaload
    //   632: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   635: pop
    //   636: aload #13
    //   638: invokevirtual toString : ()Ljava/lang/String;
    //   641: astore_3
    //   642: aload #12
    //   644: aload_3
    //   645: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   648: pop
    //   649: aload #12
    //   651: ldc_w ' (guess: '
    //   654: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   657: pop
    //   658: getstatic android/media/ExifInterface.IFD_FORMAT_NAMES : [Ljava/lang/String;
    //   661: astore_3
    //   662: aload #11
    //   664: getfield first : Ljava/lang/Object;
    //   667: checkcast java/lang/Integer
    //   670: astore #13
    //   672: aload #12
    //   674: aload_3
    //   675: aload #13
    //   677: invokevirtual intValue : ()I
    //   680: aaload
    //   681: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   684: pop
    //   685: aload #11
    //   687: getfield second : Ljava/lang/Object;
    //   690: checkcast java/lang/Integer
    //   693: invokevirtual intValue : ()I
    //   696: iconst_m1
    //   697: if_icmpne -> 706
    //   700: aload #4
    //   702: astore_3
    //   703: goto -> 747
    //   706: new java/lang/StringBuilder
    //   709: dup
    //   710: invokespecial <init> : ()V
    //   713: astore_3
    //   714: aload_3
    //   715: ldc_w ', '
    //   718: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   721: pop
    //   722: aload_3
    //   723: getstatic android/media/ExifInterface.IFD_FORMAT_NAMES : [Ljava/lang/String;
    //   726: aload #11
    //   728: getfield second : Ljava/lang/Object;
    //   731: checkcast java/lang/Integer
    //   734: invokevirtual intValue : ()I
    //   737: aaload
    //   738: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   741: pop
    //   742: aload_3
    //   743: invokevirtual toString : ()Ljava/lang/String;
    //   746: astore_3
    //   747: aload #12
    //   749: aload_3
    //   750: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   753: pop
    //   754: aload #12
    //   756: ldc_w ')'
    //   759: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   762: pop
    //   763: aload #12
    //   765: invokevirtual toString : ()Ljava/lang/String;
    //   768: astore_3
    //   769: ldc 'ExifInterface'
    //   771: aload_3
    //   772: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   775: pop
    //   776: iload #9
    //   778: istore #10
    //   780: aload_2
    //   781: astore_3
    //   782: iload #5
    //   784: istore #9
    //   786: iload #10
    //   788: istore #5
    //   790: goto -> 1655
    //   793: iload #9
    //   795: istore #10
    //   797: aload_2
    //   798: astore_3
    //   799: iload #5
    //   801: istore #9
    //   803: iload #10
    //   805: istore #5
    //   807: goto -> 1655
    //   810: aload_3
    //   811: getfield primaryFormat : I
    //   814: istore #10
    //   816: goto -> 825
    //   819: aload_3
    //   820: getfield primaryFormat : I
    //   823: istore #10
    //   825: iload #10
    //   827: tableswitch default -> 888, 1 -> 1607, 2 -> 1573, 3 -> 1482, 4 -> 1390, 5 -> 1262, 6 -> 888, 7 -> 1573, 8 -> 888, 9 -> 1176, 10 -> 1049, 11 -> 888, 12 -> 958
    //   888: iload #5
    //   890: istore #14
    //   892: iload #9
    //   894: istore #15
    //   896: aload_2
    //   897: astore_3
    //   898: iload #14
    //   900: istore #9
    //   902: iload #15
    //   904: istore #5
    //   906: getstatic android/media/ExifInterface.DEBUG : Z
    //   909: ifeq -> 1655
    //   912: new java/lang/StringBuilder
    //   915: dup
    //   916: invokespecial <init> : ()V
    //   919: astore_3
    //   920: aload_3
    //   921: ldc_w 'Data format isn't one of expected formats: '
    //   924: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   927: pop
    //   928: aload_3
    //   929: iload #10
    //   931: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   934: pop
    //   935: ldc 'ExifInterface'
    //   937: aload_3
    //   938: invokevirtual toString : ()Ljava/lang/String;
    //   941: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   944: pop
    //   945: aload_2
    //   946: astore_3
    //   947: iload #14
    //   949: istore #9
    //   951: iload #15
    //   953: istore #5
    //   955: goto -> 1655
    //   958: aload #6
    //   960: ldc_w ','
    //   963: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   966: astore_3
    //   967: aload_3
    //   968: arraylength
    //   969: newarray double
    //   971: astore #4
    //   973: iconst_0
    //   974: istore #10
    //   976: iload #10
    //   978: aload_3
    //   979: arraylength
    //   980: if_icmpge -> 1001
    //   983: aload #4
    //   985: iload #10
    //   987: aload_3
    //   988: iload #10
    //   990: aaload
    //   991: invokestatic parseDouble : (Ljava/lang/String;)D
    //   994: dastore
    //   995: iinc #10, 1
    //   998: goto -> 976
    //   1001: aload_2
    //   1002: getfield mAttributes : [Ljava/util/HashMap;
    //   1005: iload #9
    //   1007: aaload
    //   1008: astore_3
    //   1009: aload_2
    //   1010: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   1013: astore #11
    //   1015: aload #4
    //   1017: aload #11
    //   1019: invokestatic createDouble : ([DLjava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   1022: astore #4
    //   1024: aload_3
    //   1025: aload_1
    //   1026: aload #4
    //   1028: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1031: pop
    //   1032: iload #9
    //   1034: istore #10
    //   1036: aload_2
    //   1037: astore_3
    //   1038: iload #5
    //   1040: istore #9
    //   1042: iload #10
    //   1044: istore #5
    //   1046: goto -> 1655
    //   1049: aload #6
    //   1051: ldc_w ','
    //   1054: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1057: astore_2
    //   1058: aload_2
    //   1059: arraylength
    //   1060: anewarray android/media/ExifInterface$Rational
    //   1063: astore_3
    //   1064: iconst_0
    //   1065: istore #10
    //   1067: iload #10
    //   1069: aload_2
    //   1070: arraylength
    //   1071: if_icmpge -> 1132
    //   1074: aload_2
    //   1075: iload #10
    //   1077: aaload
    //   1078: ldc_w '/'
    //   1081: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1084: astore #4
    //   1086: aload #4
    //   1088: iconst_0
    //   1089: aaload
    //   1090: invokestatic parseDouble : (Ljava/lang/String;)D
    //   1093: d2l
    //   1094: lstore #16
    //   1096: aload #4
    //   1098: iload #5
    //   1100: aaload
    //   1101: astore #4
    //   1103: aload_3
    //   1104: iload #10
    //   1106: new android/media/ExifInterface$Rational
    //   1109: dup
    //   1110: lload #16
    //   1112: aload #4
    //   1114: invokestatic parseDouble : (Ljava/lang/String;)D
    //   1117: d2l
    //   1118: aconst_null
    //   1119: invokespecial <init> : (JJLandroid/media/ExifInterface$1;)V
    //   1122: aastore
    //   1123: iinc #10, 1
    //   1126: iconst_1
    //   1127: istore #5
    //   1129: goto -> 1067
    //   1132: iload #9
    //   1134: istore #5
    //   1136: aload_0
    //   1137: astore_2
    //   1138: aload_2
    //   1139: getfield mAttributes : [Ljava/util/HashMap;
    //   1142: iload #5
    //   1144: aaload
    //   1145: astore #4
    //   1147: aload_2
    //   1148: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   1151: astore #11
    //   1153: aload_3
    //   1154: aload #11
    //   1156: invokestatic createSRational : ([Landroid/media/ExifInterface$Rational;Ljava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   1159: astore_3
    //   1160: aload #4
    //   1162: aload_1
    //   1163: aload_3
    //   1164: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1167: pop
    //   1168: iconst_1
    //   1169: istore #9
    //   1171: aload_2
    //   1172: astore_3
    //   1173: goto -> 1655
    //   1176: iload #9
    //   1178: istore #5
    //   1180: aload #6
    //   1182: ldc_w ','
    //   1185: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1188: astore_3
    //   1189: aload_3
    //   1190: arraylength
    //   1191: newarray int
    //   1193: astore #4
    //   1195: iconst_0
    //   1196: istore #9
    //   1198: iload #9
    //   1200: aload_3
    //   1201: arraylength
    //   1202: if_icmpge -> 1223
    //   1205: aload #4
    //   1207: iload #9
    //   1209: aload_3
    //   1210: iload #9
    //   1212: aaload
    //   1213: invokestatic parseInt : (Ljava/lang/String;)I
    //   1216: iastore
    //   1217: iinc #9, 1
    //   1220: goto -> 1198
    //   1223: aload_2
    //   1224: getfield mAttributes : [Ljava/util/HashMap;
    //   1227: iload #5
    //   1229: aaload
    //   1230: astore_3
    //   1231: aload_2
    //   1232: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   1235: astore #11
    //   1237: aload #4
    //   1239: aload #11
    //   1241: invokestatic createSLong : ([ILjava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   1244: astore #4
    //   1246: aload_3
    //   1247: aload_1
    //   1248: aload #4
    //   1250: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1253: pop
    //   1254: iconst_1
    //   1255: istore #9
    //   1257: aload_2
    //   1258: astore_3
    //   1259: goto -> 1655
    //   1262: iload #9
    //   1264: istore #5
    //   1266: aload #6
    //   1268: ldc_w ','
    //   1271: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1274: astore #12
    //   1276: aload #12
    //   1278: arraylength
    //   1279: anewarray android/media/ExifInterface$Rational
    //   1282: astore #11
    //   1284: iconst_0
    //   1285: istore #9
    //   1287: iload #9
    //   1289: aload #12
    //   1291: arraylength
    //   1292: if_icmpge -> 1351
    //   1295: aload #12
    //   1297: iload #9
    //   1299: aaload
    //   1300: ldc_w '/'
    //   1303: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1306: astore #13
    //   1308: aload #13
    //   1310: iconst_0
    //   1311: aaload
    //   1312: invokestatic parseDouble : (Ljava/lang/String;)D
    //   1315: d2l
    //   1316: lstore #16
    //   1318: aload #13
    //   1320: iconst_1
    //   1321: aaload
    //   1322: astore #13
    //   1324: aload #11
    //   1326: iload #9
    //   1328: new android/media/ExifInterface$Rational
    //   1331: dup
    //   1332: lload #16
    //   1334: aload #13
    //   1336: invokestatic parseDouble : (Ljava/lang/String;)D
    //   1339: d2l
    //   1340: aconst_null
    //   1341: invokespecial <init> : (JJLandroid/media/ExifInterface$1;)V
    //   1344: aastore
    //   1345: iinc #9, 1
    //   1348: goto -> 1287
    //   1351: iconst_1
    //   1352: istore #9
    //   1354: aload_2
    //   1355: getfield mAttributes : [Ljava/util/HashMap;
    //   1358: iload #5
    //   1360: aaload
    //   1361: astore_3
    //   1362: aload_2
    //   1363: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   1366: astore #4
    //   1368: aload #11
    //   1370: aload #4
    //   1372: invokestatic createURational : ([Landroid/media/ExifInterface$Rational;Ljava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   1375: astore #4
    //   1377: aload_3
    //   1378: aload_1
    //   1379: aload #4
    //   1381: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1384: pop
    //   1385: aload_2
    //   1386: astore_3
    //   1387: goto -> 1655
    //   1390: iload #5
    //   1392: istore #10
    //   1394: iload #9
    //   1396: istore #5
    //   1398: aload #6
    //   1400: ldc_w ','
    //   1403: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1406: astore #4
    //   1408: aload #4
    //   1410: arraylength
    //   1411: newarray long
    //   1413: astore_3
    //   1414: iconst_0
    //   1415: istore #9
    //   1417: iload #9
    //   1419: aload #4
    //   1421: arraylength
    //   1422: if_icmpge -> 1443
    //   1425: aload_3
    //   1426: iload #9
    //   1428: aload #4
    //   1430: iload #9
    //   1432: aaload
    //   1433: invokestatic parseLong : (Ljava/lang/String;)J
    //   1436: lastore
    //   1437: iinc #9, 1
    //   1440: goto -> 1417
    //   1443: aload_2
    //   1444: getfield mAttributes : [Ljava/util/HashMap;
    //   1447: iload #5
    //   1449: aaload
    //   1450: astore #4
    //   1452: aload_2
    //   1453: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   1456: astore #11
    //   1458: aload_3
    //   1459: aload #11
    //   1461: invokestatic createULong : ([JLjava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   1464: astore_3
    //   1465: aload #4
    //   1467: aload_1
    //   1468: aload_3
    //   1469: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1472: pop
    //   1473: aload_2
    //   1474: astore_3
    //   1475: iload #10
    //   1477: istore #9
    //   1479: goto -> 1655
    //   1482: iload #5
    //   1484: istore #10
    //   1486: iload #9
    //   1488: istore #5
    //   1490: aload #6
    //   1492: ldc_w ','
    //   1495: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1498: astore_3
    //   1499: aload_3
    //   1500: arraylength
    //   1501: newarray int
    //   1503: astore #4
    //   1505: iconst_0
    //   1506: istore #9
    //   1508: iload #9
    //   1510: aload_3
    //   1511: arraylength
    //   1512: if_icmpge -> 1533
    //   1515: aload #4
    //   1517: iload #9
    //   1519: aload_3
    //   1520: iload #9
    //   1522: aaload
    //   1523: invokestatic parseInt : (Ljava/lang/String;)I
    //   1526: iastore
    //   1527: iinc #9, 1
    //   1530: goto -> 1508
    //   1533: aload_2
    //   1534: getfield mAttributes : [Ljava/util/HashMap;
    //   1537: iload #5
    //   1539: aaload
    //   1540: astore_3
    //   1541: aload_2
    //   1542: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   1545: astore #11
    //   1547: aload #4
    //   1549: aload #11
    //   1551: invokestatic createUShort : ([ILjava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   1554: astore #4
    //   1556: aload_3
    //   1557: aload_1
    //   1558: aload #4
    //   1560: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1563: pop
    //   1564: aload_2
    //   1565: astore_3
    //   1566: iload #10
    //   1568: istore #9
    //   1570: goto -> 1655
    //   1573: iload #5
    //   1575: istore #10
    //   1577: iload #9
    //   1579: istore #5
    //   1581: aload_2
    //   1582: getfield mAttributes : [Ljava/util/HashMap;
    //   1585: iload #5
    //   1587: aaload
    //   1588: aload_1
    //   1589: aload #6
    //   1591: invokestatic createString : (Ljava/lang/String;)Landroid/media/ExifInterface$ExifAttribute;
    //   1594: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1597: pop
    //   1598: aload_2
    //   1599: astore_3
    //   1600: iload #10
    //   1602: istore #9
    //   1604: goto -> 1655
    //   1607: iload #5
    //   1609: istore #10
    //   1611: iload #9
    //   1613: istore #5
    //   1615: aload_2
    //   1616: getfield mAttributes : [Ljava/util/HashMap;
    //   1619: iload #5
    //   1621: aaload
    //   1622: aload_1
    //   1623: aload #6
    //   1625: invokestatic createByte : (Ljava/lang/String;)Landroid/media/ExifInterface$ExifAttribute;
    //   1628: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1631: pop
    //   1632: aload_2
    //   1633: astore_3
    //   1634: iload #10
    //   1636: istore #9
    //   1638: goto -> 1655
    //   1641: iload #5
    //   1643: istore #10
    //   1645: iload #9
    //   1647: istore #5
    //   1649: iload #10
    //   1651: istore #9
    //   1653: aload_2
    //   1654: astore_3
    //   1655: iload #5
    //   1657: iconst_1
    //   1658: iadd
    //   1659: istore #10
    //   1661: aload_3
    //   1662: astore_2
    //   1663: iload #9
    //   1665: istore #5
    //   1667: iload #10
    //   1669: istore #9
    //   1671: goto -> 294
    //   1674: return
    //   1675: new java/lang/NullPointerException
    //   1678: dup
    //   1679: ldc_w 'tag shouldn't be null'
    //   1682: invokespecial <init> : (Ljava/lang/String;)V
    //   1685: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1775	-> 0
    //   #1779	-> 9
    //   #1780	-> 35
    //   #1781	-> 45
    //   #1782	-> 54
    //   #1783	-> 61
    //   #1784	-> 108
    //   #1786	-> 109
    //   #1787	-> 164
    //   #1788	-> 194
    //   #1790	-> 197
    //   #1791	-> 203
    //   #1795	-> 237
    //   #1792	-> 240
    //   #1793	-> 241
    //   #1794	-> 288
    //   #1799	-> 289
    //   #1800	-> 303
    //   #1801	-> 316
    //   #1803	-> 333
    //   #1804	-> 345
    //   #1805	-> 350
    //   #1806	-> 355
    //   #1807	-> 367
    //   #1809	-> 384
    //   #1810	-> 390
    //   #1812	-> 397
    //   #1814	-> 436
    //   #1815	-> 478
    //   #1816	-> 488
    //   #1817	-> 497
    //   #1822	-> 526
    //   #1823	-> 532
    //   #1826	-> 580
    //   #1827	-> 604
    //   #1828	-> 672
    //   #1829	-> 706
    //   #1823	-> 769
    //   #1822	-> 793
    //   #1820	-> 810
    //   #1813	-> 819
    //   #1833	-> 825
    //   #1908	-> 888
    //   #1909	-> 912
    //   #1898	-> 958
    //   #1899	-> 967
    //   #1900	-> 973
    //   #1901	-> 983
    //   #1900	-> 995
    //   #1903	-> 1001
    //   #1904	-> 1015
    //   #1903	-> 1024
    //   #1905	-> 1032
    //   #1886	-> 1049
    //   #1887	-> 1058
    //   #1888	-> 1064
    //   #1889	-> 1074
    //   #1890	-> 1086
    //   #1891	-> 1103
    //   #1888	-> 1123
    //   #1893	-> 1136
    //   #1894	-> 1153
    //   #1893	-> 1160
    //   #1895	-> 1168
    //   #1854	-> 1176
    //   #1855	-> 1189
    //   #1856	-> 1195
    //   #1857	-> 1205
    //   #1856	-> 1217
    //   #1859	-> 1223
    //   #1860	-> 1237
    //   #1859	-> 1246
    //   #1861	-> 1254
    //   #1874	-> 1262
    //   #1875	-> 1276
    //   #1876	-> 1284
    //   #1877	-> 1295
    //   #1878	-> 1308
    //   #1879	-> 1324
    //   #1876	-> 1345
    //   #1881	-> 1354
    //   #1882	-> 1368
    //   #1881	-> 1377
    //   #1883	-> 1385
    //   #1864	-> 1390
    //   #1865	-> 1408
    //   #1866	-> 1414
    //   #1867	-> 1425
    //   #1866	-> 1437
    //   #1869	-> 1443
    //   #1870	-> 1458
    //   #1869	-> 1465
    //   #1871	-> 1473
    //   #1844	-> 1482
    //   #1845	-> 1499
    //   #1846	-> 1505
    //   #1847	-> 1515
    //   #1846	-> 1527
    //   #1849	-> 1533
    //   #1850	-> 1547
    //   #1849	-> 1556
    //   #1851	-> 1564
    //   #1840	-> 1573
    //   #1841	-> 1598
    //   #1835	-> 1607
    //   #1836	-> 1632
    //   #1804	-> 1641
    //   #1799	-> 1655
    //   #1915	-> 1674
    //   #1776	-> 1675
    // Exception table:
    //   from	to	target	type
    //   197	203	240	java/lang/NumberFormatException
    //   203	237	240	java/lang/NumberFormatException
  }
  
  private boolean updateAttribute(String paramString, ExifAttribute paramExifAttribute) {
    boolean bool = false;
    for (byte b = 0; b < EXIF_TAGS.length; b++) {
      if (this.mAttributes[b].containsKey(paramString)) {
        this.mAttributes[b].put(paramString, paramExifAttribute);
        bool = true;
      } 
    } 
    return bool;
  }
  
  private void removeAttribute(String paramString) {
    for (byte b = 0; b < EXIF_TAGS.length; b++)
      this.mAttributes[b].remove(paramString); 
  }
  
  private void loadAttributes(InputStream paramInputStream) {
    if (paramInputStream != null) {
      byte b = 0;
      try {
        for (; b < EXIF_TAGS.length; b++)
          this.mAttributes[b] = new HashMap<>(); 
        InputStream inputStream = paramInputStream;
        if (!this.mIsExifDataOnly) {
          inputStream = new BufferedInputStream();
          super(paramInputStream, 5000);
          this.mMimeType = getMimeType((BufferedInputStream)inputStream);
        } 
        paramInputStream = new ByteOrderedDataInputStream();
        super(inputStream);
        if (!this.mIsExifDataOnly) {
          switch (this.mMimeType) {
            case 14:
              getWebpAttributes((ByteOrderedDataInputStream)paramInputStream);
              break;
            case 13:
              getPngAttributes((ByteOrderedDataInputStream)paramInputStream);
              break;
            case 12:
              getHeifAttributes((ByteOrderedDataInputStream)paramInputStream);
              break;
            case 10:
              getRw2Attributes((ByteOrderedDataInputStream)paramInputStream);
              break;
            case 9:
              getRafAttributes((ByteOrderedDataInputStream)paramInputStream);
              break;
            case 7:
              getOrfAttributes((ByteOrderedDataInputStream)paramInputStream);
              break;
            case 4:
              getJpegAttributes((ByteOrderedDataInputStream)paramInputStream, 0, 0);
              break;
            case 0:
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 8:
            case 11:
              getRawAttributes((ByteOrderedDataInputStream)paramInputStream);
              break;
          } 
        } else {
          getStandaloneAttributes((ByteOrderedDataInputStream)paramInputStream);
        } 
        setThumbnailData((ByteOrderedDataInputStream)paramInputStream);
        this.mIsSupportedFile = true;
        addDefaultValuesForCompatibility();
        if (DEBUG) {
          printAttributes();
          return;
        } 
      } catch (IOException|OutOfMemoryError iOException) {
        this.mIsSupportedFile = false;
        Log.w("ExifInterface", "Invalid image: ExifInterface got an unsupported image format file(ExifInterface supports JPEG and some RAW image formats only) or a corrupted JPEG file to ExifInterface.", iOException);
        addDefaultValuesForCompatibility();
        if (DEBUG) {
          printAttributes();
          return;
        } 
      } finally {}
      return;
    } 
    throw new NullPointerException("inputstream shouldn't be null");
  }
  
  private static boolean isSeekableFD(FileDescriptor paramFileDescriptor) {
    try {
      Os.lseek(paramFileDescriptor, 0L, OsConstants.SEEK_CUR);
      return true;
    } catch (ErrnoException errnoException) {
      if (DEBUG)
        Log.d("ExifInterface", "The file descriptor for the given input is not seekable"); 
      return false;
    } 
  }
  
  private void printAttributes() {
    for (byte b = 0; b < this.mAttributes.length; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The size of tag group[");
      stringBuilder.append(b);
      stringBuilder.append("]: ");
      stringBuilder.append(this.mAttributes[b].size());
      Log.d("ExifInterface", stringBuilder.toString());
      for (Map.Entry<?, ?> entry : this.mAttributes[b].entrySet()) {
        ExifAttribute exifAttribute = (ExifAttribute)entry.getValue();
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("tagName: ");
        stringBuilder1.append(entry.getKey());
        stringBuilder1.append(", tagType: ");
        stringBuilder1.append(exifAttribute.toString());
        stringBuilder1.append(", tagValue: '");
        ByteOrder byteOrder = this.mExifByteOrder;
        stringBuilder1.append(exifAttribute.getStringValue(byteOrder));
        stringBuilder1.append("'");
        String str = stringBuilder1.toString();
        Log.d("ExifInterface", str);
      } 
    } 
  }
  
  public void saveAttributes() throws IOException {
    if (this.mIsSupportedFile) {
      int i = this.mMimeType;
      if (i == 4 || i == 13) {
        if (!this.mIsInputStream && (this.mSeekableFileDescriptor != null || this.mFilename != null)) {
          BufferedInputStream bufferedInputStream1;
          IOException iOException2;
          BufferedInputStream bufferedInputStream2;
          IOException iOException3;
          this.mModified = true;
          this.mThumbnailBytes = getThumbnail();
          FileInputStream fileInputStream1 = null, fileInputStream2 = null;
          IOException iOException1 = null;
          FileOutputStream fileOutputStream1 = null, fileOutputStream2 = null, fileOutputStream3 = null;
          File file1 = null;
          if (this.mFilename != null)
            file1 = new File(this.mFilename); 
          File file2 = null;
          StringBuilder stringBuilder = null;
          FileInputStream fileInputStream3 = fileInputStream1;
          FileOutputStream fileOutputStream4 = fileOutputStream1;
          FileInputStream fileInputStream4 = fileInputStream2;
          FileOutputStream fileOutputStream5 = fileOutputStream2;
          try {
            FileInputStream fileInputStream6, fileInputStream5;
            FileOutputStream fileOutputStream7;
            FileInputStream fileInputStream7;
            if (this.mFilename != null) {
              fileInputStream3 = fileInputStream1;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream4 = fileInputStream2;
              fileOutputStream5 = fileOutputStream2;
              file2 = new File();
              fileInputStream3 = fileInputStream1;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream4 = fileInputStream2;
              fileOutputStream5 = fileOutputStream2;
              stringBuilder = new StringBuilder();
              fileInputStream3 = fileInputStream1;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream4 = fileInputStream2;
              fileOutputStream5 = fileOutputStream2;
              this();
              fileInputStream3 = fileInputStream1;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream4 = fileInputStream2;
              fileOutputStream5 = fileOutputStream2;
              stringBuilder.append(this.mFilename);
              fileInputStream3 = fileInputStream1;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream4 = fileInputStream2;
              fileOutputStream5 = fileOutputStream2;
              stringBuilder.append(".tmp");
              fileInputStream3 = fileInputStream1;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream4 = fileInputStream2;
              fileOutputStream5 = fileOutputStream2;
              this(stringBuilder.toString());
              fileInputStream3 = fileInputStream1;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream4 = fileInputStream2;
              fileOutputStream5 = fileOutputStream2;
              if (!file1.renameTo(file2)) {
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                iOException1 = new IOException();
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                stringBuilder = new StringBuilder();
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                this();
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                stringBuilder.append("Couldn't rename to ");
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                stringBuilder.append(file2.getAbsolutePath());
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                this(stringBuilder.toString());
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                throw iOException1;
              } 
            } else {
              fileInputStream3 = fileInputStream1;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream4 = fileInputStream2;
              fileOutputStream5 = fileOutputStream2;
              if (this.mSeekableFileDescriptor != null) {
                File file;
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                i = this.mMimeType;
                if (i == 4) {
                  fileInputStream3 = fileInputStream1;
                  fileOutputStream4 = fileOutputStream1;
                  fileInputStream4 = fileInputStream2;
                  fileOutputStream5 = fileOutputStream2;
                  file = File.createTempFile("temp", "jpg");
                } else {
                  fileInputStream3 = fileInputStream1;
                  fileOutputStream4 = fileOutputStream1;
                  fileInputStream4 = fileInputStream2;
                  fileOutputStream5 = fileOutputStream2;
                  if (this.mMimeType == 13) {
                    fileInputStream3 = fileInputStream1;
                    fileOutputStream4 = fileOutputStream1;
                    fileInputStream4 = fileInputStream2;
                    fileOutputStream5 = fileOutputStream2;
                    file = File.createTempFile("temp", "png");
                  } 
                } 
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                Os.lseek(this.mSeekableFileDescriptor, 0L, OsConstants.SEEK_SET);
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                fileInputStream6 = new FileInputStream();
                fileInputStream3 = fileInputStream1;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream2;
                fileOutputStream5 = fileOutputStream2;
                this(this.mSeekableFileDescriptor);
                fileInputStream3 = fileInputStream6;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream6;
                fileOutputStream5 = fileOutputStream2;
                fileOutputStream3 = new FileOutputStream();
                fileInputStream3 = fileInputStream6;
                fileOutputStream4 = fileOutputStream1;
                fileInputStream4 = fileInputStream6;
                fileOutputStream5 = fileOutputStream2;
                this(file);
                fileInputStream3 = fileInputStream6;
                fileOutputStream4 = fileOutputStream3;
                fileInputStream4 = fileInputStream6;
                fileOutputStream5 = fileOutputStream3;
                Streams.copy(fileInputStream6, fileOutputStream3);
                file2 = file;
              } 
            } 
            IoUtils.closeQuietly(fileInputStream6);
            IoUtils.closeQuietly(fileOutputStream3);
            fileOutputStream1 = null;
            fileOutputStream5 = null;
            fileOutputStream2 = null;
            fileInputStream4 = null;
            stringBuilder = null;
            FileOutputStream fileOutputStream6 = fileOutputStream5;
            fileOutputStream3 = fileOutputStream2;
            fileOutputStream4 = fileOutputStream1;
            fileInputStream3 = fileInputStream4;
            try {
              FileOutputStream fileOutputStream;
              fileInputStream1 = new FileInputStream();
              fileOutputStream6 = fileOutputStream5;
              fileOutputStream3 = fileOutputStream2;
              fileOutputStream4 = fileOutputStream1;
              fileInputStream3 = fileInputStream4;
              this(file2);
              FileInputStream fileInputStream = fileInputStream1;
              fileInputStream5 = fileInputStream;
              fileOutputStream3 = fileOutputStream2;
              fileInputStream7 = fileInputStream;
              fileInputStream3 = fileInputStream4;
              if (this.mFilename != null) {
                fileInputStream5 = fileInputStream;
                fileOutputStream3 = fileOutputStream2;
                fileInputStream7 = fileInputStream;
                fileInputStream3 = fileInputStream4;
                fileOutputStream = new FileOutputStream();
                fileInputStream5 = fileInputStream;
                fileOutputStream3 = fileOutputStream2;
                fileInputStream7 = fileInputStream;
                fileInputStream3 = fileInputStream4;
                this(this.mFilename);
              } else {
                fileInputStream5 = fileInputStream;
                fileOutputStream3 = fileOutputStream2;
                fileInputStream7 = fileInputStream;
                fileInputStream3 = fileInputStream4;
                if (this.mSeekableFileDescriptor != null) {
                  fileInputStream5 = fileInputStream;
                  fileOutputStream3 = fileOutputStream2;
                  fileInputStream7 = fileInputStream;
                  fileInputStream3 = fileInputStream4;
                  Os.lseek(this.mSeekableFileDescriptor, 0L, OsConstants.SEEK_SET);
                  fileInputStream5 = fileInputStream;
                  fileOutputStream3 = fileOutputStream2;
                  fileInputStream7 = fileInputStream;
                  fileInputStream3 = fileInputStream4;
                  fileOutputStream = new FileOutputStream(this.mSeekableFileDescriptor);
                } 
              } 
              fileInputStream5 = fileInputStream;
              fileOutputStream3 = fileOutputStream;
              fileInputStream7 = fileInputStream;
              fileOutputStream7 = fileOutputStream;
              bufferedInputStream2 = new BufferedInputStream();
              fileInputStream5 = fileInputStream;
              fileOutputStream3 = fileOutputStream;
              fileInputStream7 = fileInputStream;
              fileOutputStream7 = fileOutputStream;
              this(fileInputStream);
              try {
                BufferedOutputStream bufferedOutputStream;
                FileOutputStream fileOutputStream8;
              } finally {
                try {
                  bufferedInputStream2.close();
                } finally {
                  bufferedInputStream2 = null;
                  fileInputStream5 = fileInputStream;
                  fileOutputStream3 = fileOutputStream;
                  fileInputStream7 = fileInputStream;
                  fileOutputStream7 = fileOutputStream;
                } 
                fileInputStream5 = fileInputStream;
                fileOutputStream3 = fileOutputStream;
                fileInputStream7 = fileInputStream;
                fileOutputStream7 = fileOutputStream;
              } 
            } catch (Exception exception) {
              fileInputStream5 = fileInputStream7;
              fileOutputStream3 = fileOutputStream7;
              if (this.mFilename != null) {
                fileInputStream5 = fileInputStream7;
                fileOutputStream3 = fileOutputStream7;
                if (!file2.renameTo(file1)) {
                  fileInputStream5 = fileInputStream7;
                  fileOutputStream3 = fileOutputStream7;
                  exception = new IOException();
                  fileInputStream5 = fileInputStream7;
                  fileOutputStream3 = fileOutputStream7;
                  StringBuilder stringBuilder1 = new StringBuilder();
                  fileInputStream5 = fileInputStream7;
                  fileOutputStream3 = fileOutputStream7;
                  this();
                  fileInputStream5 = fileInputStream7;
                  fileOutputStream3 = fileOutputStream7;
                  stringBuilder1.append("Couldn't restore original file: ");
                  fileInputStream5 = fileInputStream7;
                  fileOutputStream3 = fileOutputStream7;
                  stringBuilder1.append(file1.getAbsolutePath());
                  fileInputStream5 = fileInputStream7;
                  fileOutputStream3 = fileOutputStream7;
                  super(stringBuilder1.toString());
                  fileInputStream5 = fileInputStream7;
                  fileOutputStream3 = fileOutputStream7;
                  throw exception;
                } 
              } 
              fileInputStream5 = fileInputStream7;
              fileOutputStream3 = fileOutputStream7;
              iOException3 = new IOException();
              fileInputStream5 = fileInputStream7;
              fileOutputStream3 = fileOutputStream7;
              this("Failed to save new file", exception);
              fileInputStream5 = fileInputStream7;
              fileOutputStream3 = fileOutputStream7;
              throw iOException3;
            } finally {}
            IoUtils.closeQuietly(fileInputStream5);
            IoUtils.closeQuietly(fileOutputStream3);
            file2.delete();
            throw stringBuilder;
          } catch (Exception exception) {
            bufferedInputStream1 = bufferedInputStream2;
            iOException2 = iOException3;
            IOException iOException = new IOException();
            bufferedInputStream1 = bufferedInputStream2;
            iOException2 = iOException3;
            this("Failed to copy original file to temp file", exception);
            bufferedInputStream1 = bufferedInputStream2;
            iOException2 = iOException3;
            throw iOException;
          } finally {}
          IoUtils.closeQuietly(bufferedInputStream1);
          IoUtils.closeQuietly((AutoCloseable)iOException2);
          throw stringBuilder;
        } 
        throw new IOException("ExifInterface does not support saving attributes for the current input.");
      } 
    } 
    throw new IOException("ExifInterface only supports saving attributes on JPEG or PNG formats.");
  }
  
  public boolean hasThumbnail() {
    return this.mHasThumbnail;
  }
  
  public boolean hasAttribute(String paramString) {
    boolean bool;
    if (getExifAttribute(paramString) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public byte[] getThumbnail() {
    int i = this.mThumbnailCompression;
    if (i == 6 || i == 7)
      return getThumbnailBytes(); 
    return null;
  }
  
  public byte[] getThumbnailBytes() {
    FileInputStream fileInputStream1, fileInputStream2;
    if (!this.mHasThumbnail)
      return null; 
    byte[] arrayOfByte = this.mThumbnailBytes;
    if (arrayOfByte != null)
      return arrayOfByte; 
    AssetManager.AssetInputStream assetInputStream1 = null, assetInputStream2 = null;
    arrayOfByte = null;
    AssetManager.AssetInputStream assetInputStream3 = assetInputStream1, assetInputStream4 = assetInputStream2;
    try {
      FileInputStream fileInputStream;
      if (this.mAssetInputStream != null) {
        assetInputStream3 = assetInputStream1;
        assetInputStream4 = assetInputStream2;
        AssetManager.AssetInputStream assetInputStream = this.mAssetInputStream;
        assetInputStream3 = assetInputStream;
        assetInputStream4 = assetInputStream;
        if (assetInputStream.markSupported()) {
          assetInputStream3 = assetInputStream;
          assetInputStream4 = assetInputStream;
          assetInputStream.reset();
        } else {
          assetInputStream3 = assetInputStream;
          assetInputStream4 = assetInputStream;
          Log.d("ExifInterface", "Cannot read thumbnail from inputstream without mark/reset support");
          IoUtils.closeQuietly((AutoCloseable)assetInputStream);
          return null;
        } 
      } else {
        assetInputStream3 = assetInputStream1;
        assetInputStream4 = assetInputStream2;
        if (this.mFilename != null) {
          assetInputStream3 = assetInputStream1;
          assetInputStream4 = assetInputStream2;
          fileInputStream = new FileInputStream(this.mFilename);
        } else {
          assetInputStream3 = assetInputStream1;
          assetInputStream4 = assetInputStream2;
          if (this.mSeekableFileDescriptor != null) {
            assetInputStream3 = assetInputStream1;
            assetInputStream4 = assetInputStream2;
            FileDescriptor fileDescriptor = Os.dup(this.mSeekableFileDescriptor);
            assetInputStream3 = assetInputStream1;
            assetInputStream4 = assetInputStream2;
            Os.lseek(fileDescriptor, 0L, OsConstants.SEEK_SET);
            assetInputStream3 = assetInputStream1;
            assetInputStream4 = assetInputStream2;
            fileInputStream = new FileInputStream(fileDescriptor, true);
          } 
        } 
      } 
      if (fileInputStream != null) {
        FileInputStream fileInputStream3 = fileInputStream, fileInputStream4 = fileInputStream;
        long l = fileInputStream.skip(this.mThumbnailOffset);
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        int i = this.mThumbnailOffset;
        if (l == i) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          byte[] arrayOfByte1 = new byte[this.mThumbnailLength];
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (fileInputStream.read(arrayOfByte1) == this.mThumbnailLength) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this.mThumbnailBytes = arrayOfByte1;
            IoUtils.closeQuietly(fileInputStream);
            return arrayOfByte1;
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          IOException iOException1 = new IOException();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this("Corrupted image");
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          throw iOException1;
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        IOException iOException = new IOException();
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        this("Corrupted image");
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        throw iOException;
      } 
      fileInputStream1 = fileInputStream;
      fileInputStream2 = fileInputStream;
      FileNotFoundException fileNotFoundException = new FileNotFoundException();
      fileInputStream1 = fileInputStream;
      fileInputStream2 = fileInputStream;
      this();
      fileInputStream1 = fileInputStream;
      fileInputStream2 = fileInputStream;
      throw fileNotFoundException;
    } catch (IOException|ErrnoException iOException) {
      fileInputStream1 = fileInputStream2;
      Log.d("ExifInterface", "Encountered exception while getting thumbnail", iOException);
      IoUtils.closeQuietly(fileInputStream2);
      return null;
    } finally {}
    IoUtils.closeQuietly(fileInputStream1);
    throw arrayOfByte;
  }
  
  public Bitmap getThumbnailBitmap() {
    if (!this.mHasThumbnail)
      return null; 
    if (this.mThumbnailBytes == null)
      this.mThumbnailBytes = getThumbnailBytes(); 
    int i = this.mThumbnailCompression;
    if (i == 6 || i == 7)
      return BitmapFactory.decodeByteArray(this.mThumbnailBytes, 0, this.mThumbnailLength); 
    if (i == 1) {
      int[] arrayOfInt = new int[this.mThumbnailBytes.length / 3];
      for (i = 0; i < arrayOfInt.length; i++) {
        byte[] arrayOfByte = this.mThumbnailBytes;
        arrayOfInt[i] = (arrayOfByte[i * 3] << 16) + 0 + (arrayOfByte[i * 3 + 1] << 8) + arrayOfByte[i * 3 + 2];
      } 
      HashMap hashMap1 = this.mAttributes[4];
      ExifAttribute exifAttribute1 = (ExifAttribute)hashMap1.get("ImageLength");
      HashMap hashMap2 = this.mAttributes[4];
      ExifAttribute exifAttribute2 = (ExifAttribute)hashMap2.get("ImageWidth");
      if (exifAttribute1 != null && exifAttribute2 != null) {
        int j = exifAttribute1.getIntValue(this.mExifByteOrder);
        i = exifAttribute2.getIntValue(this.mExifByteOrder);
        return Bitmap.createBitmap(arrayOfInt, i, j, Bitmap.Config.ARGB_8888);
      } 
    } 
    return null;
  }
  
  public boolean isThumbnailCompressed() {
    if (!this.mHasThumbnail)
      return false; 
    int i = this.mThumbnailCompression;
    if (i == 6 || i == 7)
      return true; 
    return false;
  }
  
  public long[] getThumbnailRange() {
    if (!this.mModified) {
      if (this.mHasThumbnail) {
        if (this.mHasThumbnailStrips && !this.mAreThumbnailStripsConsecutive)
          return null; 
        return new long[] { this.mThumbnailOffset, this.mThumbnailLength };
      } 
      return null;
    } 
    throw new IllegalStateException("The underlying file has been modified since being parsed");
  }
  
  public long[] getAttributeRange(String paramString) {
    if (paramString != null) {
      if (!this.mModified) {
        ExifAttribute exifAttribute = getExifAttribute(paramString);
        if (exifAttribute != null)
          return new long[] { exifAttribute.bytesOffset, exifAttribute.bytes.length }; 
        return null;
      } 
      throw new IllegalStateException("The underlying file has been modified since being parsed");
    } 
    throw new NullPointerException("tag shouldn't be null");
  }
  
  public byte[] getAttributeBytes(String paramString) {
    if (paramString != null) {
      ExifAttribute exifAttribute = getExifAttribute(paramString);
      if (exifAttribute != null)
        return exifAttribute.bytes; 
      return null;
    } 
    throw new NullPointerException("tag shouldn't be null");
  }
  
  public boolean getLatLong(float[] paramArrayOffloat) {
    String str1 = getAttribute("GPSLatitude");
    String str2 = getAttribute("GPSLatitudeRef");
    String str3 = getAttribute("GPSLongitude");
    String str4 = getAttribute("GPSLongitudeRef");
    if (str1 != null && str2 != null && str3 != null && str4 != null)
      try {
        paramArrayOffloat[0] = convertRationalLatLonToFloat(str1, str2);
        paramArrayOffloat[1] = convertRationalLatLonToFloat(str3, str4);
        return true;
      } catch (IllegalArgumentException illegalArgumentException) {} 
    return false;
  }
  
  public double getAltitude(double paramDouble) {
    double d = getAttributeDouble("GPSAltitude", -1.0D);
    byte b = -1;
    int i = getAttributeInt("GPSAltitudeRef", -1);
    if (d >= 0.0D && i >= 0) {
      if (i != 1)
        b = 1; 
      return b * d;
    } 
    return paramDouble;
  }
  
  public long getDateTime() {
    String str1 = getAttribute("DateTime");
    String str2 = getAttribute("SubSecTime");
    String str3 = getAttribute("OffsetTime");
    return parseDateTime(str1, str2, str3);
  }
  
  public long getDateTimeDigitized() {
    String str1 = getAttribute("DateTimeDigitized");
    String str2 = getAttribute("SubSecTimeDigitized");
    String str3 = getAttribute("OffsetTimeDigitized");
    return parseDateTime(str1, str2, str3);
  }
  
  public long getDateTimeOriginal() {
    String str1 = getAttribute("DateTimeOriginal");
    String str2 = getAttribute("SubSecTimeOriginal");
    String str3 = getAttribute("OffsetTimeOriginal");
    return parseDateTime(str1, str2, str3);
  }
  
  private static long parseDateTime(String paramString1, String paramString2, String paramString3) {
    if (paramString1 != null) {
      Pattern pattern = sNonZeroTimePattern;
      if (pattern.matcher(paramString1).matches()) {
        ParsePosition parsePosition = new ParsePosition(0);
        try {
          synchronized (sFormatter) {
            Date date = sFormatter.parse(paramString1, parsePosition);
            if (paramString3 != null) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append(paramString1);
              stringBuilder.append(" ");
              stringBuilder.append(paramString3);
              String str = stringBuilder.toString();
              ParsePosition parsePosition1 = new ParsePosition();
              this(0);
              synchronized (sFormatterTz) {
                date = sFormatterTz.parse(str, parsePosition1);
              } 
            } 
            if (date == null)
              return -1L; 
            long l1 = date.getTime();
            long l2 = l1;
            if (paramString2 != null)
              try {
                l2 = Long.parseLong(paramString2);
                while (l2 > 1000L)
                  l2 /= 10L; 
                l2 = l1 + l2;
              } catch (NumberFormatException numberFormatException) {
                l2 = l1;
              }  
            return l2;
          } 
        } catch (IllegalArgumentException illegalArgumentException) {
          return -1L;
        } 
      } 
    } 
    return -1L;
  }
  
  public long getGpsDateTime() {
    String str1 = getAttribute("GPSDateStamp");
    String str2 = getAttribute("GPSTimeStamp");
    if (str1 != null && str2 != null) {
      Pattern pattern = sNonZeroTimePattern;
      if (!pattern.matcher(str1).matches()) {
        pattern = sNonZeroTimePattern;
        if (!pattern.matcher(str2).matches())
          return -1L; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str1);
      stringBuilder.append(' ');
      stringBuilder.append(str2);
      String str = stringBuilder.toString();
      ParsePosition parsePosition = new ParsePosition(0);
      try {
        synchronized (sFormatter) {
          Date date = sFormatter.parse(str, parsePosition);
          if (date == null)
            return -1L; 
          return date.getTime();
        } 
      } catch (IllegalArgumentException illegalArgumentException) {
        return -1L;
      } 
    } 
    return -1L;
  }
  
  public static float convertRationalLatLonToFloat(String paramString1, String paramString2) {
    try {
      String[] arrayOfString1 = paramString1.split(",");
      String[] arrayOfString3 = arrayOfString1[0].split("/");
      double d1 = Double.parseDouble(arrayOfString3[0].trim());
      String str3 = arrayOfString3[1];
      d1 /= Double.parseDouble(str3.trim());
      String[] arrayOfString2 = arrayOfString1[1].split("/");
      double d2 = Double.parseDouble(arrayOfString2[0].trim());
      String str2 = arrayOfString2[1];
      d2 /= Double.parseDouble(str2.trim());
      arrayOfString1 = arrayOfString1[2].split("/");
      double d3 = Double.parseDouble(arrayOfString1[0].trim());
      String str1 = arrayOfString1[1];
      d3 /= Double.parseDouble(str1.trim());
      d1 = d2 / 60.0D + d1 + d3 / 3600.0D;
      if (!paramString2.equals("S")) {
        boolean bool = paramString2.equals("W");
        if (!bool)
          return (float)d1; 
      } 
      return (float)-d1;
    } catch (NumberFormatException|ArrayIndexOutOfBoundsException numberFormatException) {
      throw new IllegalArgumentException();
    } 
  }
  
  private void initForFilename(String paramString) throws IOException {
    FileInputStream fileInputStream1 = null;
    this.mAssetInputStream = null;
    this.mFilename = paramString;
    this.mIsInputStream = false;
    this.mReadImageFileDirectoryCount = 0;
    FileInputStream fileInputStream2 = fileInputStream1;
    try {
      FileInputStream fileInputStream4 = new FileInputStream();
      fileInputStream2 = fileInputStream1;
      this(paramString);
      FileInputStream fileInputStream3 = fileInputStream4;
      fileInputStream2 = fileInputStream3;
      if (isSeekableFD(fileInputStream3.getFD())) {
        fileInputStream2 = fileInputStream3;
        this.mSeekableFileDescriptor = fileInputStream3.getFD();
      } else {
        fileInputStream2 = fileInputStream3;
        this.mSeekableFileDescriptor = null;
      } 
      fileInputStream2 = fileInputStream3;
      loadAttributes(fileInputStream3);
      return;
    } finally {
      IoUtils.closeQuietly(fileInputStream2);
    } 
  }
  
  private int getMimeType(BufferedInputStream paramBufferedInputStream) throws IOException {
    paramBufferedInputStream.mark(5000);
    byte[] arrayOfByte = new byte[5000];
    paramBufferedInputStream.read(arrayOfByte);
    paramBufferedInputStream.reset();
    if (isJpegFormat(arrayOfByte))
      return 4; 
    if (isRafFormat(arrayOfByte))
      return 9; 
    if (isHeifFormat(arrayOfByte))
      return 12; 
    if (isOrfFormat(arrayOfByte))
      return 7; 
    if (isRw2Format(arrayOfByte))
      return 10; 
    if (isPngFormat(arrayOfByte))
      return 13; 
    if (isWebpFormat(arrayOfByte))
      return 14; 
    return 0;
  }
  
  private static boolean isJpegFormat(byte[] paramArrayOfbyte) throws IOException {
    byte b = 0;
    while (true) {
      byte[] arrayOfByte = JPEG_SIGNATURE;
      if (b < arrayOfByte.length) {
        if (paramArrayOfbyte[b] != arrayOfByte[b])
          return false; 
        b++;
        continue;
      } 
      break;
    } 
    return true;
  }
  
  private boolean isRafFormat(byte[] paramArrayOfbyte) throws IOException {
    byte[] arrayOfByte = "FUJIFILMCCD-RAW".getBytes();
    for (byte b = 0; b < arrayOfByte.length; b++) {
      if (paramArrayOfbyte[b] != arrayOfByte[b])
        return false; 
    } 
    return true;
  }
  
  private boolean isHeifFormat(byte[] paramArrayOfbyte) throws IOException {
    ByteOrderedDataInputStream byteOrderedDataInputStream1 = null, byteOrderedDataInputStream2 = null;
    ByteOrderedDataInputStream byteOrderedDataInputStream3 = byteOrderedDataInputStream2, byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
    try {
      ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream();
      byteOrderedDataInputStream3 = byteOrderedDataInputStream2;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      this(paramArrayOfbyte);
      byteOrderedDataInputStream1 = byteOrderedDataInputStream;
      byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream1.setByteOrder(ByteOrder.BIG_ENDIAN);
      byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      long l1 = byteOrderedDataInputStream1.readInt();
      byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      byte[] arrayOfByte = new byte[4];
      byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream1.read(arrayOfByte);
      byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      boolean bool = Arrays.equals(arrayOfByte, HEIF_TYPE_FTYP);
      if (!bool) {
        byteOrderedDataInputStream1.close();
        return false;
      } 
      long l2 = 8L;
      long l3 = l1;
      if (l1 == 1L) {
        byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
        byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
        l3 = byteOrderedDataInputStream1.readLong();
        if (l3 < 16L) {
          byteOrderedDataInputStream1.close();
          return false;
        } 
        l2 = 8L + 8L;
      } 
      l1 = l3;
      byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      if (l3 > paramArrayOfbyte.length) {
        byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
        byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
        int k = paramArrayOfbyte.length;
        l1 = k;
      } 
      l2 = l1 - l2;
      if (l2 < 8L) {
        byteOrderedDataInputStream1.close();
        return false;
      } 
      byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      paramArrayOfbyte = new byte[4];
      int j = 0;
      int i = 0;
      l3 = 0L;
      while (true) {
        byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
        byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
        if (l3 < l2 / 4L) {
          byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
          byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
          int k = byteOrderedDataInputStream1.read(paramArrayOfbyte);
          byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
          byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
          int m = paramArrayOfbyte.length;
          if (k != m) {
            byteOrderedDataInputStream1.close();
            return false;
          } 
          if (l3 == 1L) {
            m = i;
          } else {
            byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
            byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
            if (Arrays.equals(paramArrayOfbyte, HEIF_BRAND_MIF1)) {
              k = 1;
            } else {
              byteOrderedDataInputStream3 = byteOrderedDataInputStream1;
              byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
              bool = Arrays.equals(paramArrayOfbyte, HEIF_BRAND_HEIC);
              k = j;
              if (bool) {
                i = 1;
                k = j;
              } 
            } 
            j = k;
            m = i;
            if (k != 0) {
              j = k;
              m = i;
              if (i) {
                byteOrderedDataInputStream1.close();
                return true;
              } 
            } 
          } 
          l3++;
          i = m;
          continue;
        } 
        break;
      } 
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
    } catch (Exception exception) {
      byteOrderedDataInputStream3 = byteOrderedDataInputStream4;
      if (DEBUG) {
        byteOrderedDataInputStream3 = byteOrderedDataInputStream4;
        Log.d("ExifInterface", "Exception parsing HEIF file type box.", exception);
      } 
      if (byteOrderedDataInputStream4 != null) {
        byteOrderedDataInputStream4.close();
        return false;
      } 
    } finally {}
    byteOrderedDataInputStream4.close();
    return false;
  }
  
  private boolean isOrfFormat(byte[] paramArrayOfbyte) throws IOException {
    ByteOrderedDataInputStream byteOrderedDataInputStream1 = null, byteOrderedDataInputStream2 = null;
    boolean bool = false;
    ByteOrderedDataInputStream byteOrderedDataInputStream3 = byteOrderedDataInputStream2, byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
    try {
      ByteOrderedDataInputStream byteOrderedDataInputStream6 = new ByteOrderedDataInputStream();
      byteOrderedDataInputStream3 = byteOrderedDataInputStream2;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      this(paramArrayOfbyte);
      ByteOrderedDataInputStream byteOrderedDataInputStream5 = byteOrderedDataInputStream6;
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      ByteOrder byteOrder = readByteOrder(byteOrderedDataInputStream5);
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      this.mExifByteOrder = byteOrder;
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream5.setByteOrder(byteOrder);
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      short s = byteOrderedDataInputStream5.readShort();
      if (s == 20306 || s == 21330)
        bool = true; 
      return bool;
    } catch (Exception exception) {
      return false;
    } finally {
      if (byteOrderedDataInputStream3 != null)
        byteOrderedDataInputStream3.close(); 
    } 
  }
  
  private boolean isRw2Format(byte[] paramArrayOfbyte) throws IOException {
    ByteOrderedDataInputStream byteOrderedDataInputStream1 = null, byteOrderedDataInputStream2 = null;
    boolean bool = false;
    ByteOrderedDataInputStream byteOrderedDataInputStream3 = byteOrderedDataInputStream2, byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
    try {
      ByteOrderedDataInputStream byteOrderedDataInputStream6 = new ByteOrderedDataInputStream();
      byteOrderedDataInputStream3 = byteOrderedDataInputStream2;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream1;
      this(paramArrayOfbyte);
      ByteOrderedDataInputStream byteOrderedDataInputStream5 = byteOrderedDataInputStream6;
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      ByteOrder byteOrder = readByteOrder(byteOrderedDataInputStream5);
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      this.mExifByteOrder = byteOrder;
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream5.setByteOrder(byteOrder);
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      short s = byteOrderedDataInputStream5.readShort();
      byteOrderedDataInputStream3 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream4 = byteOrderedDataInputStream5;
      byteOrderedDataInputStream5.close();
      if (s == 85)
        bool = true; 
      return bool;
    } catch (Exception exception) {
      return false;
    } finally {
      if (byteOrderedDataInputStream3 != null)
        byteOrderedDataInputStream3.close(); 
    } 
  }
  
  private boolean isPngFormat(byte[] paramArrayOfbyte) throws IOException {
    byte b = 0;
    while (true) {
      byte[] arrayOfByte = PNG_SIGNATURE;
      if (b < arrayOfByte.length) {
        if (paramArrayOfbyte[b] != arrayOfByte[b])
          return false; 
        b++;
        continue;
      } 
      break;
    } 
    return true;
  }
  
  private boolean isWebpFormat(byte[] paramArrayOfbyte) throws IOException {
    byte b = 0;
    while (true) {
      byte[] arrayOfByte = WEBP_SIGNATURE_1;
      if (b < arrayOfByte.length) {
        if (paramArrayOfbyte[b] != arrayOfByte[b])
          return false; 
        b++;
        continue;
      } 
      break;
    } 
    b = 0;
    while (true) {
      byte[] arrayOfByte = WEBP_SIGNATURE_2;
      if (b < arrayOfByte.length) {
        if (paramArrayOfbyte[WEBP_SIGNATURE_1.length + b + 4] != arrayOfByte[b])
          return false; 
        b++;
        continue;
      } 
      break;
    } 
    return true;
  }
  
  private static boolean isExifDataOnly(BufferedInputStream paramBufferedInputStream) throws IOException {
    paramBufferedInputStream.mark(IDENTIFIER_EXIF_APP1.length);
    byte[] arrayOfByte = new byte[IDENTIFIER_EXIF_APP1.length];
    paramBufferedInputStream.read(arrayOfByte);
    paramBufferedInputStream.reset();
    byte b = 0;
    while (true) {
      byte[] arrayOfByte1 = IDENTIFIER_EXIF_APP1;
      if (b < arrayOfByte1.length) {
        if (arrayOfByte[b] != arrayOfByte1[b])
          return false; 
        b++;
        continue;
      } 
      break;
    } 
    return true;
  }
  
  private void getJpegAttributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream, int paramInt1, int paramInt2) throws IOException {
    if (DEBUG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("getJpegAttributes starting with: ");
      stringBuilder1.append(paramByteOrderedDataInputStream);
      Log.d("ExifInterface", stringBuilder1.toString());
    } 
    paramByteOrderedDataInputStream.setByteOrder(ByteOrder.BIG_ENDIAN);
    paramByteOrderedDataInputStream.seek(paramInt1);
    int i = paramByteOrderedDataInputStream.readByte();
    if (i == -1) {
      int j = 1;
      if (paramByteOrderedDataInputStream.readByte() == -40) {
        i = paramInt1 + 1 + 1;
        paramInt1 = j;
        j = i;
        while (true) {
          i = paramByteOrderedDataInputStream.readByte();
          if (i == -1) {
            byte b = paramByteOrderedDataInputStream.readByte();
            if (DEBUG) {
              StringBuilder stringBuilder3 = new StringBuilder();
              stringBuilder3.append("Found JPEG segment indicator: ");
              stringBuilder3.append(Integer.toHexString(b & 0xFF));
              Log.d("ExifInterface", stringBuilder3.toString());
            } 
            if (b != -39) {
              if (b == -38)
                continue; 
              int k = paramByteOrderedDataInputStream.readUnsignedShort() - 2;
              i = j + 1 + paramInt1 + 2;
              if (DEBUG) {
                StringBuilder stringBuilder3 = new StringBuilder();
                stringBuilder3.append("JPEG segment: ");
                stringBuilder3.append(Integer.toHexString(b & 0xFF));
                stringBuilder3.append(" (length: ");
                stringBuilder3.append(k + 2);
                stringBuilder3.append(")");
                Log.d("ExifInterface", stringBuilder3.toString());
              } 
              if (k >= 0) {
                if (b != -31) {
                  if (b != -2) {
                    switch (b) {
                      default:
                        switch (b) {
                          default:
                            switch (b) {
                              default:
                                switch (b) {
                                  default:
                                    j = k;
                                    break;
                                  case -51:
                                  case -50:
                                  case -49:
                                    break;
                                } 
                                break;
                              case -55:
                              case -54:
                              case -53:
                                break;
                            } 
                            break;
                          case -59:
                          case -58:
                          case -57:
                            break;
                        } 
                      case -64:
                      case -63:
                      case -62:
                      case -61:
                        if (b == -62) {
                          this.mIsProgressiveModePic = paramInt1;
                          Log.i("ExifInterface", "the picture is progressive jpeg");
                        } 
                        if (paramByteOrderedDataInputStream.skipBytes(paramInt1) == paramInt1) {
                          HashMap<String, ExifAttribute> hashMap1 = this.mAttributes[paramInt2];
                          long l = paramByteOrderedDataInputStream.readUnsignedShort();
                          ByteOrder byteOrder2 = this.mExifByteOrder;
                          hashMap1.put("ImageLength", ExifAttribute.createULong(l, byteOrder2));
                          HashMap<String, ExifAttribute> hashMap2 = this.mAttributes[paramInt2];
                          l = paramByteOrderedDataInputStream.readUnsignedShort();
                          ByteOrder byteOrder1 = this.mExifByteOrder;
                          hashMap2.put("ImageWidth", ExifAttribute.createULong(l, byteOrder1));
                          j = k - 5;
                          break;
                        } 
                        throw new IOException("Invalid SOFx");
                    } 
                  } else {
                    byte[] arrayOfByte = new byte[k];
                    if (paramByteOrderedDataInputStream.read(arrayOfByte) == k) {
                      j = 0;
                      if (getAttribute("UserComment") == null)
                        this.mAttributes[paramInt1].put("UserComment", ExifAttribute.createString(new String(arrayOfByte, ASCII))); 
                    } else {
                      throw new IOException("Invalid exif");
                    } 
                  } 
                } else {
                  byte[] arrayOfByte = new byte[k];
                  paramByteOrderedDataInputStream.readFully(arrayOfByte);
                  k = i + k;
                  j = 0;
                  if (ArrayUtils.startsWith(arrayOfByte, IDENTIFIER_EXIF_APP1)) {
                    byte[] arrayOfByte1 = IDENTIFIER_EXIF_APP1;
                    long l = (arrayOfByte1.length + i);
                    arrayOfByte = Arrays.copyOfRange(arrayOfByte, arrayOfByte1.length, arrayOfByte.length);
                    this.mExifOffset = (int)l;
                    readExifSegment(arrayOfByte, paramInt2);
                    i = k;
                  } else if (ArrayUtils.startsWith(arrayOfByte, IDENTIFIER_XMP_APP1)) {
                    byte[] arrayOfByte1 = IDENTIFIER_XMP_APP1;
                    long l = (arrayOfByte1.length + i);
                    arrayOfByte = Arrays.copyOfRange(arrayOfByte, arrayOfByte1.length, arrayOfByte.length);
                    if (getAttribute("Xmp") == null) {
                      this.mAttributes[0].put("Xmp", new ExifAttribute(1, arrayOfByte.length, l, arrayOfByte));
                      paramInt1 = 1;
                      this.mXmpIsFromSeparateMarker = true;
                    } 
                    i = k;
                  } else {
                    i = k;
                  } 
                } 
                if (j >= 0) {
                  if (paramByteOrderedDataInputStream.skipBytes(j) == j) {
                    j = i + j;
                    continue;
                  } 
                  throw new IOException("Invalid JPEG segment");
                } 
                throw new IOException("Invalid length");
              } 
              throw new IOException("Invalid length");
            } 
            paramByteOrderedDataInputStream.setByteOrder(this.mExifByteOrder);
            return;
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Invalid marker:");
          stringBuilder2.append(Integer.toHexString(i & 0xFF));
          throw new IOException(stringBuilder2.toString());
        } 
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid marker: ");
      stringBuilder1.append(Integer.toHexString(i & 0xFF));
      throw new IOException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid marker: ");
    stringBuilder.append(Integer.toHexString(i & 0xFF));
    throw new IOException(stringBuilder.toString());
  }
  
  private void getRawAttributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    parseTiffHeaders(paramByteOrderedDataInputStream, paramByteOrderedDataInputStream.available());
    readImageFileDirectory(paramByteOrderedDataInputStream, 0);
    updateImageSizeValues(paramByteOrderedDataInputStream, 0);
    updateImageSizeValues(paramByteOrderedDataInputStream, 5);
    updateImageSizeValues(paramByteOrderedDataInputStream, 4);
    validateImages();
    if (this.mMimeType == 8) {
      HashMap hashMap = this.mAttributes[1];
      ExifAttribute exifAttribute = (ExifAttribute)hashMap.get("MakerNote");
      if (exifAttribute != null) {
        ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(exifAttribute.bytes);
        byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder);
        byteOrderedDataInputStream.seek(6L);
        readImageFileDirectory(byteOrderedDataInputStream, 9);
        HashMap hashMap1 = this.mAttributes[9];
        ExifAttribute exifAttribute1 = (ExifAttribute)hashMap1.get("ColorSpace");
        if (exifAttribute1 != null)
          this.mAttributes[1].put("ColorSpace", exifAttribute1); 
      } 
    } 
  }
  
  private void getRafAttributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    paramByteOrderedDataInputStream.skipBytes(84);
    byte[] arrayOfByte1 = new byte[4];
    byte[] arrayOfByte2 = new byte[4];
    paramByteOrderedDataInputStream.read(arrayOfByte1);
    paramByteOrderedDataInputStream.skipBytes(4);
    paramByteOrderedDataInputStream.read(arrayOfByte2);
    int i = ByteBuffer.wrap(arrayOfByte1).getInt();
    int j = ByteBuffer.wrap(arrayOfByte2).getInt();
    getJpegAttributes(paramByteOrderedDataInputStream, i, 5);
    paramByteOrderedDataInputStream.seek(j);
    paramByteOrderedDataInputStream.setByteOrder(ByteOrder.BIG_ENDIAN);
    j = paramByteOrderedDataInputStream.readInt();
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("numberOfDirectoryEntry: ");
      stringBuilder.append(j);
      Log.d("ExifInterface", stringBuilder.toString());
    } 
    for (i = 0; i < j; i++) {
      StringBuilder stringBuilder;
      int k = paramByteOrderedDataInputStream.readUnsignedShort();
      int m = paramByteOrderedDataInputStream.readUnsignedShort();
      if (k == TAG_RAF_IMAGE_SIZE.number) {
        j = paramByteOrderedDataInputStream.readShort();
        i = paramByteOrderedDataInputStream.readShort();
        ByteOrder byteOrder1 = this.mExifByteOrder;
        ExifAttribute exifAttribute1 = ExifAttribute.createUShort(j, byteOrder1);
        ByteOrder byteOrder2 = this.mExifByteOrder;
        ExifAttribute exifAttribute2 = ExifAttribute.createUShort(i, byteOrder2);
        this.mAttributes[0].put("ImageLength", exifAttribute1);
        this.mAttributes[0].put("ImageWidth", exifAttribute2);
        if (DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Updated to length: ");
          stringBuilder.append(j);
          stringBuilder.append(", width: ");
          stringBuilder.append(i);
          Log.d("ExifInterface", stringBuilder.toString());
        } 
        return;
      } 
      stringBuilder.skipBytes(m);
    } 
  }
  
  private void getHeifAttributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
    try {
      Object object = new Object();
      super(this, paramByteOrderedDataInputStream);
      mediaMetadataRetriever.setDataSource((MediaDataSource)object);
      String str1 = mediaMetadataRetriever.extractMetadata(33);
      String str2 = mediaMetadataRetriever.extractMetadata(34);
      String str3 = mediaMetadataRetriever.extractMetadata(26);
      String str4 = mediaMetadataRetriever.extractMetadata(17);
      object = null;
      String str5 = null;
      String str6 = null;
      if ("yes".equals(str3)) {
        object = mediaMetadataRetriever.extractMetadata(29);
        str5 = mediaMetadataRetriever.extractMetadata(30);
        str6 = mediaMetadataRetriever.extractMetadata(31);
      } else if ("yes".equals(str4)) {
        object = mediaMetadataRetriever.extractMetadata(18);
        str5 = mediaMetadataRetriever.extractMetadata(19);
        str6 = mediaMetadataRetriever.extractMetadata(24);
      } 
      if (object != null) {
        HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
        ExifAttribute exifAttribute = ExifAttribute.createUShort(Integer.parseInt((String)object), this.mExifByteOrder);
        hashMap.put("ImageWidth", exifAttribute);
      } 
      if (str5 != null) {
        HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
        ExifAttribute exifAttribute = ExifAttribute.createUShort(Integer.parseInt(str5), this.mExifByteOrder);
        hashMap.put("ImageLength", exifAttribute);
      } 
      if (str6 != null) {
        byte b = 1;
        int i = Integer.parseInt(str6);
        if (i != 90) {
          if (i != 180) {
            if (i == 270)
              b = 8; 
          } else {
            b = 3;
          } 
        } else {
          b = 6;
        } 
        HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
        ByteOrder byteOrder = this.mExifByteOrder;
        ExifAttribute exifAttribute = ExifAttribute.createUShort(b, byteOrder);
        hashMap.put("Orientation", exifAttribute);
      } 
      if (str1 != null && str2 != null) {
        IOException iOException;
        int i = Integer.parseInt(str1);
        int j = Integer.parseInt(str2);
        if (j > 6) {
          long l = i;
          try {
            paramByteOrderedDataInputStream.seek(l);
            byte[] arrayOfByte = new byte[6];
            if (paramByteOrderedDataInputStream.read(arrayOfByte) == 6) {
              int k = j - 6;
              if (Arrays.equals(arrayOfByte, IDENTIFIER_EXIF_APP1)) {
                arrayOfByte = new byte[k];
                j = paramByteOrderedDataInputStream.read(arrayOfByte);
                if (j == k) {
                  try {
                    this.mExifOffset = i + 6;
                    readExifSegment(arrayOfByte, 0);
                    if (DEBUG) {
                      StringBuilder stringBuilder = new StringBuilder();
                      this();
                      stringBuilder.append("Heif meta: ");
                      stringBuilder.append((String)object);
                      stringBuilder.append("x");
                      stringBuilder.append(str5);
                      stringBuilder.append(", rotation ");
                      stringBuilder.append(str6);
                      Log.d("ExifInterface", stringBuilder.toString());
                    } 
                    mediaMetadataRetriever.release();
                    return;
                  } finally {}
                } else {
                  iOException = new IOException();
                  this("Can't read exif");
                  throw iOException;
                } 
              } else {
                iOException = new IOException();
                this("Invalid identifier");
                throw iOException;
              } 
            } else {
              iOException = new IOException();
              this("Can't read identifier");
              throw iOException;
            } 
          } finally {}
        } else {
          iOException = new IOException();
          this("Invalid exif length");
          throw iOException;
        } 
        mediaMetadataRetriever.release();
        throw iOException;
      } 
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Heif meta: ");
        stringBuilder.append((String)object);
        stringBuilder.append("x");
        stringBuilder.append(str5);
        stringBuilder.append(", rotation ");
        stringBuilder.append(str6);
        Log.d("ExifInterface", stringBuilder.toString());
      } 
      mediaMetadataRetriever.release();
      return;
    } finally {}
    mediaMetadataRetriever.release();
    throw paramByteOrderedDataInputStream;
  }
  
  private void getStandaloneAttributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    paramByteOrderedDataInputStream.skipBytes(IDENTIFIER_EXIF_APP1.length);
    byte[] arrayOfByte = new byte[paramByteOrderedDataInputStream.available()];
    paramByteOrderedDataInputStream.readFully(arrayOfByte);
    this.mExifOffset = IDENTIFIER_EXIF_APP1.length;
    readExifSegment(arrayOfByte, 0);
  }
  
  private void getOrfAttributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    getRawAttributes(paramByteOrderedDataInputStream);
    HashMap hashMap = this.mAttributes[1];
    ExifAttribute exifAttribute = (ExifAttribute)hashMap.get("MakerNote");
    if (exifAttribute != null) {
      ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(exifAttribute.bytes);
      byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder);
      byte[] arrayOfByte1 = new byte[ORF_MAKER_NOTE_HEADER_1.length];
      byteOrderedDataInputStream.readFully(arrayOfByte1);
      byteOrderedDataInputStream.seek(0L);
      byte[] arrayOfByte2 = new byte[ORF_MAKER_NOTE_HEADER_2.length];
      byteOrderedDataInputStream.readFully(arrayOfByte2);
      if (Arrays.equals(arrayOfByte1, ORF_MAKER_NOTE_HEADER_1)) {
        byteOrderedDataInputStream.seek(8L);
      } else if (Arrays.equals(arrayOfByte2, ORF_MAKER_NOTE_HEADER_2)) {
        byteOrderedDataInputStream.seek(12L);
      } 
      readImageFileDirectory(byteOrderedDataInputStream, 6);
      HashMap hashMap2 = this.mAttributes[7];
      ExifAttribute exifAttribute2 = (ExifAttribute)hashMap2.get("PreviewImageStart");
      HashMap hashMap3 = this.mAttributes[7];
      ExifAttribute exifAttribute3 = (ExifAttribute)hashMap3.get("PreviewImageLength");
      if (exifAttribute2 != null && exifAttribute3 != null) {
        this.mAttributes[5].put("JPEGInterchangeFormat", exifAttribute2);
        this.mAttributes[5].put("JPEGInterchangeFormatLength", exifAttribute3);
      } 
      HashMap hashMap1 = this.mAttributes[8];
      ExifAttribute exifAttribute1 = (ExifAttribute)hashMap1.get("AspectFrame");
      if (exifAttribute1 != null) {
        int[] arrayOfInt2 = new int[4];
        int[] arrayOfInt1 = (int[])exifAttribute1.getValue(this.mExifByteOrder);
        if (arrayOfInt1[2] > arrayOfInt1[0] && arrayOfInt1[3] > arrayOfInt1[1]) {
          int i = arrayOfInt1[2] - arrayOfInt1[0] + 1;
          int j = arrayOfInt1[3] - arrayOfInt1[1] + 1;
          int k = i, m = j;
          if (i < j) {
            k = i + j;
            m = k - j;
            k -= m;
          } 
          ByteOrder byteOrder1 = this.mExifByteOrder;
          ExifAttribute exifAttribute4 = ExifAttribute.createUShort(k, byteOrder1);
          ByteOrder byteOrder2 = this.mExifByteOrder;
          ExifAttribute exifAttribute5 = ExifAttribute.createUShort(m, byteOrder2);
          this.mAttributes[0].put("ImageWidth", exifAttribute4);
          this.mAttributes[0].put("ImageLength", exifAttribute5);
        } 
      } 
    } 
  }
  
  private void getRw2Attributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    getRawAttributes(paramByteOrderedDataInputStream);
    HashMap hashMap3 = this.mAttributes[0];
    ExifAttribute exifAttribute3 = (ExifAttribute)hashMap3.get("JpgFromRaw");
    if (exifAttribute3 != null)
      getJpegAttributes(paramByteOrderedDataInputStream, this.mRw2JpgFromRawOffset, 5); 
    HashMap hashMap1 = this.mAttributes[0];
    ExifAttribute exifAttribute1 = (ExifAttribute)hashMap1.get("ISO");
    HashMap hashMap2 = this.mAttributes[1];
    ExifAttribute exifAttribute2 = (ExifAttribute)hashMap2.get("ISOSpeedRatings");
    if (exifAttribute1 != null && exifAttribute2 == null)
      this.mAttributes[1].put("ISOSpeedRatings", exifAttribute1); 
  }
  
  private void getPngAttributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPngAttributes starting with: ");
      stringBuilder.append(paramByteOrderedDataInputStream);
      Log.d("ExifInterface", stringBuilder.toString());
    } 
    paramByteOrderedDataInputStream.setByteOrder(ByteOrder.BIG_ENDIAN);
    paramByteOrderedDataInputStream.skipBytes(PNG_SIGNATURE.length);
    int i = 0 + PNG_SIGNATURE.length;
    try {
      while (true) {
        int j = paramByteOrderedDataInputStream.readInt();
        byte[] arrayOfByte = new byte[4];
        if (paramByteOrderedDataInputStream.read(arrayOfByte) == arrayOfByte.length) {
          i = i + 4 + 4;
          if (i != 16 || Arrays.equals(arrayOfByte, PNG_CHUNK_TYPE_IHDR)) {
            if (!Arrays.equals(arrayOfByte, PNG_CHUNK_TYPE_IEND)) {
              IOException iOException2;
              if (Arrays.equals(arrayOfByte, PNG_CHUNK_TYPE_EXIF)) {
                StringBuilder stringBuilder1;
                byte[] arrayOfByte1 = new byte[j];
                if (paramByteOrderedDataInputStream.read(arrayOfByte1) == j) {
                  j = paramByteOrderedDataInputStream.readInt();
                  CRC32 cRC32 = new CRC32();
                  this();
                  cRC32.update(arrayOfByte);
                  cRC32.update(arrayOfByte1);
                  if ((int)cRC32.getValue() == j) {
                    this.mExifOffset = i;
                    readExifSegment(arrayOfByte1, 0);
                    validateImages();
                    return;
                  } 
                  IOException iOException3 = new IOException();
                  stringBuilder1 = new StringBuilder();
                  this();
                  stringBuilder1.append("Encountered invalid CRC value for PNG-EXIF chunk.\n recorded CRC value: ");
                  stringBuilder1.append(j);
                  stringBuilder1.append(", calculated CRC value: ");
                  stringBuilder1.append(cRC32.getValue());
                  this(stringBuilder1.toString());
                  throw iOException3;
                } 
                iOException2 = new IOException();
                StringBuilder stringBuilder2 = new StringBuilder();
                this();
                stringBuilder2.append("Failed to read given length for given PNG chunk type: ");
                stringBuilder2.append(byteArrayToHexString((byte[])stringBuilder1));
                this(stringBuilder2.toString());
                throw iOException2;
              } 
              iOException2.skipBytes(j + 4);
              i += j + 4;
              continue;
            } 
            return;
          } 
          IOException iOException1 = new IOException();
          this("Encountered invalid PNG file--IHDR chunk should appearas the first chunk");
          throw iOException1;
        } 
        IOException iOException = new IOException();
        this("Encountered invalid length while parsing PNG chunktype");
        throw iOException;
      } 
    } catch (EOFException eOFException) {
      throw new IOException("Encountered corrupt PNG file.");
    } 
  }
  
  private void getWebpAttributes(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getWebpAttributes starting with: ");
      stringBuilder.append(paramByteOrderedDataInputStream);
      Log.d("ExifInterface", stringBuilder.toString());
    } 
    paramByteOrderedDataInputStream.setByteOrder(ByteOrder.LITTLE_ENDIAN);
    paramByteOrderedDataInputStream.skipBytes(WEBP_SIGNATURE_1.length);
    int i = paramByteOrderedDataInputStream.readInt() + 8;
    int j = 8 + paramByteOrderedDataInputStream.skipBytes(WEBP_SIGNATURE_2.length);
    try {
      while (true) {
        byte[] arrayOfByte = new byte[4];
        if (paramByteOrderedDataInputStream.read(arrayOfByte) == arrayOfByte.length) {
          IOException iOException1;
          int k = paramByteOrderedDataInputStream.readInt();
          int m = j + 4 + 4;
          if (Arrays.equals(WEBP_CHUNK_TYPE_EXIF, arrayOfByte)) {
            byte[] arrayOfByte1 = new byte[k];
            if (paramByteOrderedDataInputStream.read(arrayOfByte1) == k) {
              this.mExifOffset = m;
              readExifSegment(arrayOfByte1, 0);
            } else {
              iOException1 = new IOException();
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Failed to read given length for given PNG chunk type: ");
              stringBuilder.append(byteArrayToHexString(arrayOfByte));
              this(stringBuilder.toString());
              throw iOException1;
            } 
          } else {
            if (k % 2 == 1) {
              j = k + 1;
            } else {
              j = k;
            } 
            if (m + j != i) {
              if (m + j <= i) {
                k = iOException1.skipBytes(j);
                if (k == j) {
                  j = m + k;
                  continue;
                } 
                iOException1 = new IOException();
                this("Encountered WebP file with invalid chunk size");
                throw iOException1;
              } 
              iOException1 = new IOException();
              this("Encountered WebP file with invalid chunk size");
              throw iOException1;
            } 
          } 
          return;
        } 
        IOException iOException = new IOException();
        this("Encountered invalid length while parsing WebP chunktype");
        throw iOException;
      } 
    } catch (EOFException eOFException) {
      throw new IOException("Encountered corrupt WebP file.");
    } 
  }
  
  private void saveJpegAttributes(InputStream paramInputStream, OutputStream paramOutputStream) throws IOException {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("saveJpegAttributes starting with (inputStream: ");
      stringBuilder.append(paramInputStream);
      stringBuilder.append(", outputStream: ");
      stringBuilder.append(paramOutputStream);
      stringBuilder.append(")");
      Log.d("ExifInterface", stringBuilder.toString());
    } 
    DataInputStream dataInputStream = new DataInputStream(paramInputStream);
    ByteOrderedDataOutputStream byteOrderedDataOutputStream = new ByteOrderedDataOutputStream(paramOutputStream, ByteOrder.BIG_ENDIAN);
    if (dataInputStream.readByte() == -1) {
      byteOrderedDataOutputStream.writeByte(-1);
      if (dataInputStream.readByte() == -40) {
        ExifAttribute exifAttribute;
        byteOrderedDataOutputStream.writeByte(-40);
        paramOutputStream = null;
        OutputStream outputStream = paramOutputStream;
        if (getAttribute("Xmp") != null) {
          outputStream = paramOutputStream;
          if (this.mXmpIsFromSeparateMarker)
            exifAttribute = this.mAttributes[0].remove("Xmp"); 
        } 
        byteOrderedDataOutputStream.writeByte(-1);
        byteOrderedDataOutputStream.writeByte(-31);
        writeExifSegment(byteOrderedDataOutputStream);
        if (exifAttribute != null)
          this.mAttributes[0].put("Xmp", exifAttribute); 
        byte[] arrayOfByte = new byte[4096];
        while (true) {
          byte b = dataInputStream.readByte();
          if (b == -1) {
            b = dataInputStream.readByte();
            if (b != -39) {
              int i;
              if (b != -38) {
                if (b != -31) {
                  byteOrderedDataOutputStream.writeByte(-1);
                  byteOrderedDataOutputStream.writeByte(b);
                  i = dataInputStream.readUnsignedShort();
                  byteOrderedDataOutputStream.writeUnsignedShort(i);
                  i -= 2;
                  if (i >= 0) {
                    while (i > 0) {
                      int k = arrayOfByte.length;
                      k = Math.min(i, k);
                      k = dataInputStream.read(arrayOfByte, 0, k);
                      if (k >= 0) {
                        byteOrderedDataOutputStream.write(arrayOfByte, 0, k);
                        i -= k;
                      } 
                    } 
                    continue;
                  } 
                  throw new IOException("Invalid length");
                } 
                int j = dataInputStream.readUnsignedShort() - 2;
                if (j >= 0) {
                  byte[] arrayOfByte1 = new byte[6];
                  if (j >= 6)
                    if (dataInputStream.read(arrayOfByte1) == 6) {
                      if (Arrays.equals(arrayOfByte1, IDENTIFIER_EXIF_APP1)) {
                        if (dataInputStream.skipBytes(j - 6) == j - 6)
                          continue; 
                        throw new IOException("Invalid length");
                      } 
                    } else {
                      throw new IOException("Invalid exif");
                    }  
                  byteOrderedDataOutputStream.writeByte(-1);
                  byteOrderedDataOutputStream.writeByte(i);
                  byteOrderedDataOutputStream.writeUnsignedShort(j + 2);
                  i = j;
                  if (j >= 6) {
                    i = j - 6;
                    byteOrderedDataOutputStream.write(arrayOfByte1);
                  } 
                  while (i > 0) {
                    j = arrayOfByte.length;
                    j = Math.min(i, j);
                    j = dataInputStream.read(arrayOfByte, 0, j);
                    if (j >= 0) {
                      byteOrderedDataOutputStream.write(arrayOfByte, 0, j);
                      i -= j;
                    } 
                  } 
                  continue;
                } 
                throw new IOException("Invalid length");
              } 
              byteOrderedDataOutputStream.writeByte(-1);
              byteOrderedDataOutputStream.writeByte(i);
              Streams.copy(dataInputStream, byteOrderedDataOutputStream);
              return;
            } 
            continue;
          } 
          throw new IOException("Invalid marker");
        } 
      } 
      throw new IOException("Invalid marker");
    } 
    throw new IOException("Invalid marker");
  }
  
  private void savePngAttributes(InputStream paramInputStream, OutputStream paramOutputStream) throws IOException {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("savePngAttributes starting with (inputStream: ");
      stringBuilder.append(paramInputStream);
      stringBuilder.append(", outputStream: ");
      stringBuilder.append(paramOutputStream);
      stringBuilder.append(")");
      Log.d("ExifInterface", stringBuilder.toString());
    } 
    paramInputStream = new DataInputStream(paramInputStream);
    ByteOrderedDataOutputStream byteOrderedDataOutputStream = new ByteOrderedDataOutputStream(paramOutputStream, ByteOrder.BIG_ENDIAN);
    copy(paramInputStream, byteOrderedDataOutputStream, PNG_SIGNATURE.length);
    int i = this.mExifOffset;
    if (i == 0) {
      i = paramInputStream.readInt();
      byteOrderedDataOutputStream.writeInt(i);
      copy(paramInputStream, byteOrderedDataOutputStream, i + 4 + 4);
    } else {
      int j = PNG_SIGNATURE.length;
      copy(paramInputStream, byteOrderedDataOutputStream, i - j - 4 - 4);
      i = paramInputStream.readInt();
      paramInputStream.skipBytes(i + 4 + 4);
    } 
    paramOutputStream = new ByteArrayOutputStream();
    try {
      ByteOrderedDataOutputStream byteOrderedDataOutputStream1 = new ByteOrderedDataOutputStream();
      this(paramOutputStream, ByteOrder.BIG_ENDIAN);
      writeExifSegment(byteOrderedDataOutputStream1);
      ByteArrayOutputStream byteArrayOutputStream = (ByteArrayOutputStream)byteOrderedDataOutputStream1.mOutputStream;
      byte[] arrayOfByte = byteArrayOutputStream.toByteArray();
      byteOrderedDataOutputStream.write(arrayOfByte);
      CRC32 cRC32 = new CRC32();
      this();
      cRC32.update(arrayOfByte, 4, arrayOfByte.length - 4);
      byteOrderedDataOutputStream.writeInt((int)cRC32.getValue());
      paramOutputStream.close();
      return;
    } finally {
      try {
        paramOutputStream.close();
      } finally {
        paramOutputStream = null;
      } 
    } 
  }
  
  private void readExifSegment(byte[] paramArrayOfbyte, int paramInt) throws IOException {
    ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(paramArrayOfbyte);
    parseTiffHeaders(byteOrderedDataInputStream, paramArrayOfbyte.length);
    readImageFileDirectory(byteOrderedDataInputStream, paramInt);
  }
  
  private void addDefaultValuesForCompatibility() {
    String str = getAttribute("DateTimeOriginal");
    if (str != null && getAttribute("DateTime") == null) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
      ExifAttribute exifAttribute = ExifAttribute.createString(str);
      hashMap.put("DateTime", exifAttribute);
    } 
    if (getAttribute("ImageWidth") == null) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(0L, byteOrder);
      hashMap.put("ImageWidth", exifAttribute);
    } 
    if (getAttribute("ImageLength") == null) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(0L, byteOrder);
      hashMap.put("ImageLength", exifAttribute);
    } 
    if (getAttribute("Orientation") == null) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createUShort(0, byteOrder);
      hashMap.put("Orientation", exifAttribute);
    } 
    if (getAttribute("LightSource") == null) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[1];
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(0L, byteOrder);
      hashMap.put("LightSource", exifAttribute);
    } 
  }
  
  private ByteOrder readByteOrder(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    short s = paramByteOrderedDataInputStream.readShort();
    if (s != 18761) {
      if (s == 19789) {
        if (DEBUG)
          Log.d("ExifInterface", "readExifSegment: Byte Align MM"); 
        return ByteOrder.BIG_ENDIAN;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid byte order: ");
      stringBuilder.append(Integer.toHexString(s));
      throw new IOException(stringBuilder.toString());
    } 
    if (DEBUG)
      Log.d("ExifInterface", "readExifSegment: Byte Align II"); 
    return ByteOrder.LITTLE_ENDIAN;
  }
  
  private void parseTiffHeaders(ByteOrderedDataInputStream paramByteOrderedDataInputStream, int paramInt) throws IOException {
    ByteOrder byteOrder = readByteOrder(paramByteOrderedDataInputStream);
    paramByteOrderedDataInputStream.setByteOrder(byteOrder);
    int i = paramByteOrderedDataInputStream.readUnsignedShort();
    int j = this.mMimeType;
    if (j == 7 || j == 10 || i == 42) {
      i = paramByteOrderedDataInputStream.readInt();
      if (i >= 8 && i < paramInt) {
        paramInt = i - 8;
        if (paramInt <= 0 || 
          paramByteOrderedDataInputStream.skipBytes(paramInt) == paramInt) {
          this.mReadImageFileDirectoryCount = 0;
          return;
        } 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Couldn't jump to first Ifd: ");
        stringBuilder2.append(paramInt);
        throw new IOException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid first Ifd offset: ");
      stringBuilder1.append(i);
      throw new IOException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid start code: ");
    stringBuilder.append(Integer.toHexString(i));
    throw new IOException(stringBuilder.toString());
  }
  
  private void readImageFileDirectory(ByteOrderedDataInputStream paramByteOrderedDataInputStream, int paramInt) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mHandledIfdOffsets : Ljava/util/Set;
    //   4: aload_1
    //   5: invokestatic access$900 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   8: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   11: invokeinterface add : (Ljava/lang/Object;)Z
    //   16: pop
    //   17: aload_1
    //   18: invokestatic access$900 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   21: iconst_2
    //   22: iadd
    //   23: aload_1
    //   24: invokestatic access$1000 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   27: if_icmple -> 31
    //   30: return
    //   31: aload_1
    //   32: invokevirtual readShort : ()S
    //   35: istore_3
    //   36: aload_1
    //   37: invokestatic access$900 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   40: iload_3
    //   41: bipush #12
    //   43: imul
    //   44: iadd
    //   45: aload_1
    //   46: invokestatic access$1000 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   49: if_icmpgt -> 1645
    //   52: iload_3
    //   53: ifgt -> 59
    //   56: goto -> 1645
    //   59: getstatic android/media/ExifInterface.DEBUG : Z
    //   62: ifeq -> 101
    //   65: new java/lang/StringBuilder
    //   68: dup
    //   69: invokespecial <init> : ()V
    //   72: astore #4
    //   74: aload #4
    //   76: ldc_w 'numberOfDirectoryEntry: '
    //   79: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: aload #4
    //   85: iload_3
    //   86: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: ldc 'ExifInterface'
    //   92: aload #4
    //   94: invokevirtual toString : ()Ljava/lang/String;
    //   97: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   100: pop
    //   101: aload_0
    //   102: getfield mReadImageFileDirectoryCount : I
    //   105: iconst_1
    //   106: iadd
    //   107: istore #5
    //   109: aload_0
    //   110: iload #5
    //   112: putfield mReadImageFileDirectoryCount : I
    //   115: iload #5
    //   117: sipush #512
    //   120: if_icmple -> 133
    //   123: ldc 'ExifInterface'
    //   125: ldc_w 'readImageFileDirectory maybe can't breakout, so return.'
    //   128: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   131: pop
    //   132: return
    //   133: iconst_0
    //   134: istore #6
    //   136: iload #6
    //   138: iload_3
    //   139: if_icmpge -> 1439
    //   142: aload_1
    //   143: invokevirtual readUnsignedShort : ()I
    //   146: istore #7
    //   148: aload_1
    //   149: invokevirtual readUnsignedShort : ()I
    //   152: istore #8
    //   154: aload_1
    //   155: invokevirtual readInt : ()I
    //   158: istore #9
    //   160: aload_1
    //   161: invokevirtual peek : ()I
    //   164: iconst_4
    //   165: iadd
    //   166: i2l
    //   167: lstore #10
    //   169: getstatic android/media/ExifInterface.sExifTagMapsForReading : [Ljava/util/HashMap;
    //   172: iload_2
    //   173: aaload
    //   174: iload #7
    //   176: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   179: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   182: checkcast android/media/ExifInterface$ExifTag
    //   185: astore #4
    //   187: ldc_w 36867
    //   190: iload #7
    //   192: if_icmpeq -> 211
    //   195: ldc_w 37521
    //   198: iload #7
    //   200: if_icmpeq -> 211
    //   203: ldc_w 36881
    //   206: iload #7
    //   208: if_icmpne -> 229
    //   211: getstatic android/media/ExifInterface.sExifTagMapsForReading : [Ljava/util/HashMap;
    //   214: iconst_1
    //   215: aaload
    //   216: iload #7
    //   218: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   221: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   224: checkcast android/media/ExifInterface$ExifTag
    //   227: astore #4
    //   229: getstatic android/media/ExifInterface.DEBUG : Z
    //   232: ifeq -> 305
    //   235: aload #4
    //   237: ifnull -> 250
    //   240: aload #4
    //   242: getfield name : Ljava/lang/String;
    //   245: astore #12
    //   247: goto -> 253
    //   250: aconst_null
    //   251: astore #12
    //   253: ldc 'ExifInterface'
    //   255: ldc_w 'ifdType: %d, tagNumber: %d, tagName: %s, dataFormat: %d, numberOfComponents: %d'
    //   258: iconst_5
    //   259: anewarray java/lang/Object
    //   262: dup
    //   263: iconst_0
    //   264: iload_2
    //   265: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   268: aastore
    //   269: dup
    //   270: iconst_1
    //   271: iload #7
    //   273: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   276: aastore
    //   277: dup
    //   278: iconst_2
    //   279: aload #12
    //   281: aastore
    //   282: dup
    //   283: iconst_3
    //   284: iload #8
    //   286: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   289: aastore
    //   290: dup
    //   291: iconst_4
    //   292: iload #9
    //   294: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   297: aastore
    //   298: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   301: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   304: pop
    //   305: aload #4
    //   307: ifnonnull -> 359
    //   310: getstatic android/media/ExifInterface.DEBUG : Z
    //   313: ifeq -> 356
    //   316: new java/lang/StringBuilder
    //   319: dup
    //   320: invokespecial <init> : ()V
    //   323: astore #12
    //   325: aload #12
    //   327: ldc_w 'Skip the tag entry since tag number is not defined: '
    //   330: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   333: pop
    //   334: aload #12
    //   336: iload #7
    //   338: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   341: pop
    //   342: ldc 'ExifInterface'
    //   344: aload #12
    //   346: invokevirtual toString : ()Ljava/lang/String;
    //   349: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   352: pop
    //   353: goto -> 509
    //   356: goto -> 509
    //   359: iload #8
    //   361: ifle -> 466
    //   364: getstatic android/media/ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT : [I
    //   367: astore #12
    //   369: iload #8
    //   371: aload #12
    //   373: arraylength
    //   374: if_icmplt -> 380
    //   377: goto -> 466
    //   380: iload #9
    //   382: i2l
    //   383: aload #12
    //   385: iload #8
    //   387: iaload
    //   388: i2l
    //   389: lmul
    //   390: lstore #13
    //   392: lload #13
    //   394: lconst_0
    //   395: lcmp
    //   396: iflt -> 417
    //   399: lload #13
    //   401: ldc2_w 2147483647
    //   404: lcmp
    //   405: ifle -> 411
    //   408: goto -> 417
    //   411: iconst_1
    //   412: istore #5
    //   414: goto -> 515
    //   417: getstatic android/media/ExifInterface.DEBUG : Z
    //   420: ifeq -> 460
    //   423: new java/lang/StringBuilder
    //   426: dup
    //   427: invokespecial <init> : ()V
    //   430: astore #12
    //   432: aload #12
    //   434: ldc_w 'Skip the tag entry since the number of components is invalid: '
    //   437: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   440: pop
    //   441: aload #12
    //   443: iload #9
    //   445: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   448: pop
    //   449: ldc 'ExifInterface'
    //   451: aload #12
    //   453: invokevirtual toString : ()Ljava/lang/String;
    //   456: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   459: pop
    //   460: iconst_0
    //   461: istore #5
    //   463: goto -> 515
    //   466: getstatic android/media/ExifInterface.DEBUG : Z
    //   469: ifeq -> 509
    //   472: new java/lang/StringBuilder
    //   475: dup
    //   476: invokespecial <init> : ()V
    //   479: astore #12
    //   481: aload #12
    //   483: ldc_w 'Skip the tag entry since data format is invalid: '
    //   486: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   489: pop
    //   490: aload #12
    //   492: iload #8
    //   494: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   497: pop
    //   498: ldc 'ExifInterface'
    //   500: aload #12
    //   502: invokevirtual toString : ()Ljava/lang/String;
    //   505: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   508: pop
    //   509: iconst_0
    //   510: istore #5
    //   512: lconst_0
    //   513: lstore #13
    //   515: iload #5
    //   517: ifne -> 529
    //   520: aload_1
    //   521: lload #10
    //   523: invokevirtual seek : (J)V
    //   526: goto -> 1429
    //   529: lload #13
    //   531: ldc2_w 4
    //   534: lcmp
    //   535: ifle -> 863
    //   538: aload_1
    //   539: invokevirtual readInt : ()I
    //   542: istore #15
    //   544: getstatic android/media/ExifInterface.DEBUG : Z
    //   547: ifeq -> 590
    //   550: new java/lang/StringBuilder
    //   553: dup
    //   554: invokespecial <init> : ()V
    //   557: astore #12
    //   559: aload #12
    //   561: ldc_w 'seek to data offset: '
    //   564: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   567: pop
    //   568: aload #12
    //   570: iload #15
    //   572: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   575: pop
    //   576: ldc 'ExifInterface'
    //   578: aload #12
    //   580: invokevirtual toString : ()Ljava/lang/String;
    //   583: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   586: pop
    //   587: goto -> 590
    //   590: aload_0
    //   591: getfield mMimeType : I
    //   594: istore #5
    //   596: iload #5
    //   598: bipush #7
    //   600: if_icmpne -> 762
    //   603: aload #4
    //   605: getfield name : Ljava/lang/String;
    //   608: ldc_w 'MakerNote'
    //   611: if_acmpne -> 623
    //   614: aload_0
    //   615: iload #15
    //   617: putfield mOrfMakerNoteOffset : I
    //   620: goto -> 786
    //   623: iload_2
    //   624: bipush #6
    //   626: if_icmpne -> 759
    //   629: aload #4
    //   631: getfield name : Ljava/lang/String;
    //   634: ldc_w 'ThumbnailImage'
    //   637: if_acmpne -> 759
    //   640: aload_0
    //   641: iload #15
    //   643: putfield mOrfThumbnailOffset : I
    //   646: aload_0
    //   647: iload #9
    //   649: putfield mOrfThumbnailLength : I
    //   652: aload_0
    //   653: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   656: astore #12
    //   658: bipush #6
    //   660: aload #12
    //   662: invokestatic createUShort : (ILjava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   665: astore #12
    //   667: aload_0
    //   668: getfield mOrfThumbnailOffset : I
    //   671: i2l
    //   672: lstore #16
    //   674: aload_0
    //   675: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   678: astore #18
    //   680: lload #16
    //   682: aload #18
    //   684: invokestatic createULong : (JLjava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   687: astore #18
    //   689: aload_0
    //   690: getfield mOrfThumbnailLength : I
    //   693: i2l
    //   694: lstore #16
    //   696: aload_0
    //   697: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   700: astore #19
    //   702: lload #16
    //   704: aload #19
    //   706: invokestatic createULong : (JLjava/nio/ByteOrder;)Landroid/media/ExifInterface$ExifAttribute;
    //   709: astore #19
    //   711: aload_0
    //   712: getfield mAttributes : [Ljava/util/HashMap;
    //   715: iconst_4
    //   716: aaload
    //   717: ldc_w 'Compression'
    //   720: aload #12
    //   722: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   725: pop
    //   726: aload_0
    //   727: getfield mAttributes : [Ljava/util/HashMap;
    //   730: iconst_4
    //   731: aaload
    //   732: ldc_w 'JPEGInterchangeFormat'
    //   735: aload #18
    //   737: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   740: pop
    //   741: aload_0
    //   742: getfield mAttributes : [Ljava/util/HashMap;
    //   745: iconst_4
    //   746: aaload
    //   747: ldc_w 'JPEGInterchangeFormatLength'
    //   750: aload #19
    //   752: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   755: pop
    //   756: goto -> 786
    //   759: goto -> 786
    //   762: iload #5
    //   764: bipush #10
    //   766: if_icmpne -> 786
    //   769: aload #4
    //   771: getfield name : Ljava/lang/String;
    //   774: ldc_w 'JpgFromRaw'
    //   777: if_acmpne -> 786
    //   780: aload_0
    //   781: iload #15
    //   783: putfield mRw2JpgFromRawOffset : I
    //   786: iload #15
    //   788: i2l
    //   789: lload #13
    //   791: ladd
    //   792: aload_1
    //   793: invokestatic access$1000 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   796: i2l
    //   797: lcmp
    //   798: ifgt -> 811
    //   801: aload_1
    //   802: iload #15
    //   804: i2l
    //   805: invokevirtual seek : (J)V
    //   808: goto -> 863
    //   811: getstatic android/media/ExifInterface.DEBUG : Z
    //   814: ifeq -> 854
    //   817: new java/lang/StringBuilder
    //   820: dup
    //   821: invokespecial <init> : ()V
    //   824: astore #4
    //   826: aload #4
    //   828: ldc_w 'Skip the tag entry since data offset is invalid: '
    //   831: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   834: pop
    //   835: aload #4
    //   837: iload #15
    //   839: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   842: pop
    //   843: ldc 'ExifInterface'
    //   845: aload #4
    //   847: invokevirtual toString : ()Ljava/lang/String;
    //   850: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   853: pop
    //   854: aload_1
    //   855: lload #10
    //   857: invokevirtual seek : (J)V
    //   860: goto -> 1429
    //   863: getstatic android/media/ExifInterface.sExifPointerTagMap : Ljava/util/HashMap;
    //   866: iload #7
    //   868: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   871: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   874: checkcast java/lang/Integer
    //   877: astore #12
    //   879: getstatic android/media/ExifInterface.DEBUG : Z
    //   882: ifeq -> 939
    //   885: new java/lang/StringBuilder
    //   888: dup
    //   889: invokespecial <init> : ()V
    //   892: astore #18
    //   894: aload #18
    //   896: ldc_w 'nextIfdType: '
    //   899: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   902: pop
    //   903: aload #18
    //   905: aload #12
    //   907: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   910: pop
    //   911: aload #18
    //   913: ldc_w ' byteCount: '
    //   916: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   919: pop
    //   920: aload #18
    //   922: lload #13
    //   924: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   927: pop
    //   928: ldc 'ExifInterface'
    //   930: aload #18
    //   932: invokevirtual toString : ()Ljava/lang/String;
    //   935: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   938: pop
    //   939: aload #12
    //   941: ifnull -> 1241
    //   944: ldc2_w -1
    //   947: lstore #13
    //   949: iload #8
    //   951: iconst_3
    //   952: if_icmpeq -> 1014
    //   955: iload #8
    //   957: iconst_4
    //   958: if_icmpeq -> 1005
    //   961: iload #8
    //   963: bipush #8
    //   965: if_icmpeq -> 995
    //   968: iload #8
    //   970: bipush #9
    //   972: if_icmpeq -> 985
    //   975: iload #8
    //   977: bipush #13
    //   979: if_icmpeq -> 985
    //   982: goto -> 1021
    //   985: aload_1
    //   986: invokevirtual readInt : ()I
    //   989: i2l
    //   990: lstore #13
    //   992: goto -> 1021
    //   995: aload_1
    //   996: invokevirtual readShort : ()S
    //   999: i2l
    //   1000: lstore #13
    //   1002: goto -> 1021
    //   1005: aload_1
    //   1006: invokevirtual readUnsignedInt : ()J
    //   1009: lstore #13
    //   1011: goto -> 1021
    //   1014: aload_1
    //   1015: invokevirtual readUnsignedShort : ()I
    //   1018: i2l
    //   1019: lstore #13
    //   1021: getstatic android/media/ExifInterface.DEBUG : Z
    //   1024: ifeq -> 1062
    //   1027: ldc 'ExifInterface'
    //   1029: ldc_w 'Offset: %d, tagName: %s'
    //   1032: iconst_2
    //   1033: anewarray java/lang/Object
    //   1036: dup
    //   1037: iconst_0
    //   1038: lload #13
    //   1040: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1043: aastore
    //   1044: dup
    //   1045: iconst_1
    //   1046: aload #4
    //   1048: getfield name : Ljava/lang/String;
    //   1051: aastore
    //   1052: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   1055: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1058: pop
    //   1059: goto -> 1062
    //   1062: lload #13
    //   1064: lconst_0
    //   1065: lcmp
    //   1066: ifle -> 1189
    //   1069: lload #13
    //   1071: aload_1
    //   1072: invokestatic access$1000 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   1075: i2l
    //   1076: lcmp
    //   1077: ifge -> 1189
    //   1080: aload_0
    //   1081: getfield mHandledIfdOffsets : Ljava/util/Set;
    //   1084: lload #13
    //   1086: l2i
    //   1087: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1090: invokeinterface contains : (Ljava/lang/Object;)Z
    //   1095: ifne -> 1117
    //   1098: aload_1
    //   1099: lload #13
    //   1101: invokevirtual seek : (J)V
    //   1104: aload_0
    //   1105: aload_1
    //   1106: aload #12
    //   1108: invokevirtual intValue : ()I
    //   1111: invokespecial readImageFileDirectory : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;I)V
    //   1114: goto -> 1232
    //   1117: getstatic android/media/ExifInterface.DEBUG : Z
    //   1120: ifeq -> 1232
    //   1123: new java/lang/StringBuilder
    //   1126: dup
    //   1127: invokespecial <init> : ()V
    //   1130: astore #4
    //   1132: aload #4
    //   1134: ldc_w 'Skip jump into the IFD since it has already been read: IfdType '
    //   1137: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1140: pop
    //   1141: aload #4
    //   1143: aload #12
    //   1145: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1148: pop
    //   1149: aload #4
    //   1151: ldc_w ' (at '
    //   1154: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1157: pop
    //   1158: aload #4
    //   1160: lload #13
    //   1162: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   1165: pop
    //   1166: aload #4
    //   1168: ldc_w ')'
    //   1171: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1174: pop
    //   1175: ldc 'ExifInterface'
    //   1177: aload #4
    //   1179: invokevirtual toString : ()Ljava/lang/String;
    //   1182: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1185: pop
    //   1186: goto -> 1232
    //   1189: getstatic android/media/ExifInterface.DEBUG : Z
    //   1192: ifeq -> 1232
    //   1195: new java/lang/StringBuilder
    //   1198: dup
    //   1199: invokespecial <init> : ()V
    //   1202: astore #4
    //   1204: aload #4
    //   1206: ldc_w 'Skip jump into the IFD since its offset is invalid: '
    //   1209: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1212: pop
    //   1213: aload #4
    //   1215: lload #13
    //   1217: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   1220: pop
    //   1221: ldc 'ExifInterface'
    //   1223: aload #4
    //   1225: invokevirtual toString : ()Ljava/lang/String;
    //   1228: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1231: pop
    //   1232: aload_1
    //   1233: lload #10
    //   1235: invokevirtual seek : (J)V
    //   1238: goto -> 1429
    //   1241: aload_1
    //   1242: invokevirtual peek : ()I
    //   1245: istore #5
    //   1247: aload_0
    //   1248: getfield mExifOffset : I
    //   1251: istore #7
    //   1253: lload #13
    //   1255: l2i
    //   1256: iflt -> 1409
    //   1259: lload #13
    //   1261: l2i
    //   1262: newarray byte
    //   1264: astore #12
    //   1266: aload_1
    //   1267: aload #12
    //   1269: invokevirtual readFully : ([B)V
    //   1272: new android/media/ExifInterface$ExifAttribute
    //   1275: dup
    //   1276: iload #8
    //   1278: iload #9
    //   1280: iload #5
    //   1282: iload #7
    //   1284: iadd
    //   1285: i2l
    //   1286: aload #12
    //   1288: aconst_null
    //   1289: invokespecial <init> : (IIJ[BLandroid/media/ExifInterface$1;)V
    //   1292: astore #12
    //   1294: aload_0
    //   1295: getfield mAttributes : [Ljava/util/HashMap;
    //   1298: iload_2
    //   1299: aaload
    //   1300: aload #4
    //   1302: getfield name : Ljava/lang/String;
    //   1305: aload #12
    //   1307: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1310: pop
    //   1311: aload #4
    //   1313: getfield name : Ljava/lang/String;
    //   1316: ldc_w 'DNGVersion'
    //   1319: if_acmpne -> 1327
    //   1322: aload_0
    //   1323: iconst_3
    //   1324: putfield mMimeType : I
    //   1327: aload #4
    //   1329: getfield name : Ljava/lang/String;
    //   1332: ldc_w 'Make'
    //   1335: if_acmpeq -> 1349
    //   1338: aload #4
    //   1340: getfield name : Ljava/lang/String;
    //   1343: ldc_w 'Model'
    //   1346: if_acmpne -> 1370
    //   1349: aload_0
    //   1350: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   1353: astore #18
    //   1355: aload #12
    //   1357: aload #18
    //   1359: invokevirtual getStringValue : (Ljava/nio/ByteOrder;)Ljava/lang/String;
    //   1362: ldc 'PENTAX'
    //   1364: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   1367: ifne -> 1400
    //   1370: aload #4
    //   1372: getfield name : Ljava/lang/String;
    //   1375: ldc_w 'Compression'
    //   1378: if_acmpne -> 1409
    //   1381: aload_0
    //   1382: getfield mExifByteOrder : Ljava/nio/ByteOrder;
    //   1385: astore #4
    //   1387: aload #12
    //   1389: aload #4
    //   1391: invokevirtual getIntValue : (Ljava/nio/ByteOrder;)I
    //   1394: ldc_w 65535
    //   1397: if_icmpne -> 1409
    //   1400: aload_0
    //   1401: bipush #8
    //   1403: putfield mMimeType : I
    //   1406: goto -> 1409
    //   1409: aload_1
    //   1410: invokevirtual peek : ()I
    //   1413: i2l
    //   1414: lload #10
    //   1416: lcmp
    //   1417: ifeq -> 1429
    //   1420: aload_1
    //   1421: lload #10
    //   1423: invokevirtual seek : (J)V
    //   1426: goto -> 1429
    //   1429: iload #6
    //   1431: iconst_1
    //   1432: iadd
    //   1433: i2s
    //   1434: istore #6
    //   1436: goto -> 136
    //   1439: aload_1
    //   1440: invokevirtual peek : ()I
    //   1443: iconst_4
    //   1444: iadd
    //   1445: aload_1
    //   1446: invokestatic access$1000 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   1449: if_icmpgt -> 1644
    //   1452: aload_1
    //   1453: invokevirtual readInt : ()I
    //   1456: istore_2
    //   1457: getstatic android/media/ExifInterface.DEBUG : Z
    //   1460: ifeq -> 1486
    //   1463: ldc 'ExifInterface'
    //   1465: ldc_w 'nextIfdOffset: %d'
    //   1468: iconst_1
    //   1469: anewarray java/lang/Object
    //   1472: dup
    //   1473: iconst_0
    //   1474: iload_2
    //   1475: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1478: aastore
    //   1479: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   1482: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1485: pop
    //   1486: iload_2
    //   1487: i2l
    //   1488: lconst_0
    //   1489: lcmp
    //   1490: ifle -> 1606
    //   1493: iload_2
    //   1494: aload_1
    //   1495: invokestatic access$1000 : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;)I
    //   1498: if_icmpge -> 1606
    //   1501: aload_0
    //   1502: getfield mHandledIfdOffsets : Ljava/util/Set;
    //   1505: iload_2
    //   1506: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1509: invokeinterface contains : (Ljava/lang/Object;)Z
    //   1514: ifne -> 1565
    //   1517: aload_1
    //   1518: iload_2
    //   1519: i2l
    //   1520: invokevirtual seek : (J)V
    //   1523: aload_0
    //   1524: getfield mAttributes : [Ljava/util/HashMap;
    //   1527: iconst_4
    //   1528: aaload
    //   1529: invokevirtual isEmpty : ()Z
    //   1532: ifeq -> 1544
    //   1535: aload_0
    //   1536: aload_1
    //   1537: iconst_4
    //   1538: invokespecial readImageFileDirectory : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;I)V
    //   1541: goto -> 1644
    //   1544: aload_0
    //   1545: getfield mAttributes : [Ljava/util/HashMap;
    //   1548: iconst_5
    //   1549: aaload
    //   1550: invokevirtual isEmpty : ()Z
    //   1553: ifeq -> 1644
    //   1556: aload_0
    //   1557: aload_1
    //   1558: iconst_5
    //   1559: invokespecial readImageFileDirectory : (Landroid/media/ExifInterface$ByteOrderedDataInputStream;I)V
    //   1562: goto -> 1644
    //   1565: getstatic android/media/ExifInterface.DEBUG : Z
    //   1568: ifeq -> 1644
    //   1571: new java/lang/StringBuilder
    //   1574: dup
    //   1575: invokespecial <init> : ()V
    //   1578: astore_1
    //   1579: aload_1
    //   1580: ldc_w 'Stop reading file since re-reading an IFD may cause an infinite loop: '
    //   1583: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1586: pop
    //   1587: aload_1
    //   1588: iload_2
    //   1589: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1592: pop
    //   1593: ldc 'ExifInterface'
    //   1595: aload_1
    //   1596: invokevirtual toString : ()Ljava/lang/String;
    //   1599: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1602: pop
    //   1603: goto -> 1644
    //   1606: getstatic android/media/ExifInterface.DEBUG : Z
    //   1609: ifeq -> 1644
    //   1612: new java/lang/StringBuilder
    //   1615: dup
    //   1616: invokespecial <init> : ()V
    //   1619: astore_1
    //   1620: aload_1
    //   1621: ldc_w 'Stop reading file since a wrong offset may cause an infinite loop: '
    //   1624: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1627: pop
    //   1628: aload_1
    //   1629: iload_2
    //   1630: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1633: pop
    //   1634: ldc 'ExifInterface'
    //   1636: aload_1
    //   1637: invokevirtual toString : ()Ljava/lang/String;
    //   1640: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1643: pop
    //   1644: return
    //   1645: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3738	-> 0
    //   #3740	-> 17
    //   #3742	-> 30
    //   #3745	-> 31
    //   #3746	-> 36
    //   #3752	-> 59
    //   #3753	-> 65
    //   #3758	-> 101
    //   #3759	-> 115
    //   #3760	-> 123
    //   #3761	-> 132
    //   #3765	-> 133
    //   #3766	-> 142
    //   #3767	-> 148
    //   #3768	-> 154
    //   #3770	-> 160
    //   #3773	-> 169
    //   #3778	-> 187
    //   #3781	-> 211
    //   #3785	-> 229
    //   #3786	-> 235
    //   #3787	-> 235
    //   #3788	-> 235
    //   #3786	-> 253
    //   #3791	-> 305
    //   #3792	-> 305
    //   #3793	-> 305
    //   #3794	-> 310
    //   #3795	-> 316
    //   #3794	-> 356
    //   #3797	-> 359
    //   #3802	-> 380
    //   #3803	-> 392
    //   #3809	-> 411
    //   #3804	-> 417
    //   #3805	-> 423
    //   #3812	-> 460
    //   #3797	-> 466
    //   #3798	-> 466
    //   #3799	-> 472
    //   #3812	-> 509
    //   #3813	-> 520
    //   #3814	-> 526
    //   #3819	-> 529
    //   #3820	-> 538
    //   #3821	-> 544
    //   #3822	-> 550
    //   #3821	-> 590
    //   #3824	-> 590
    //   #3825	-> 603
    //   #3827	-> 614
    //   #3828	-> 623
    //   #3831	-> 640
    //   #3832	-> 646
    //   #3834	-> 652
    //   #3835	-> 658
    //   #3836	-> 667
    //   #3837	-> 680
    //   #3838	-> 689
    //   #3839	-> 702
    //   #3841	-> 711
    //   #3842	-> 726
    //   #3844	-> 741
    //   #3846	-> 756
    //   #3828	-> 759
    //   #3847	-> 762
    //   #3848	-> 769
    //   #3849	-> 780
    //   #3852	-> 786
    //   #3853	-> 801
    //   #3856	-> 811
    //   #3857	-> 817
    //   #3859	-> 854
    //   #3860	-> 860
    //   #3819	-> 863
    //   #3865	-> 863
    //   #3866	-> 879
    //   #3867	-> 885
    //   #3870	-> 939
    //   #3871	-> 944
    //   #3873	-> 949
    //   #3888	-> 985
    //   #3889	-> 992
    //   #3879	-> 995
    //   #3880	-> 1002
    //   #3883	-> 1005
    //   #3884	-> 1011
    //   #3875	-> 1014
    //   #3876	-> 1021
    //   #3896	-> 1021
    //   #3897	-> 1027
    //   #3896	-> 1062
    //   #3903	-> 1062
    //   #3904	-> 1080
    //   #3905	-> 1098
    //   #3906	-> 1104
    //   #3908	-> 1117
    //   #3909	-> 1123
    //   #3914	-> 1189
    //   #3915	-> 1195
    //   #3919	-> 1232
    //   #3920	-> 1238
    //   #3923	-> 1241
    //   #3927	-> 1253
    //   #3929	-> 1259
    //   #3930	-> 1266
    //   #3931	-> 1272
    //   #3933	-> 1294
    //   #3938	-> 1311
    //   #3939	-> 1322
    //   #3945	-> 1327
    //   #3946	-> 1355
    //   #3948	-> 1387
    //   #3949	-> 1400
    //   #3927	-> 1409
    //   #3957	-> 1409
    //   #3958	-> 1420
    //   #3957	-> 1429
    //   #3765	-> 1429
    //   #3962	-> 1439
    //   #3963	-> 1452
    //   #3964	-> 1457
    //   #3965	-> 1463
    //   #3970	-> 1486
    //   #3971	-> 1501
    //   #3972	-> 1517
    //   #3974	-> 1523
    //   #3975	-> 1535
    //   #3976	-> 1544
    //   #3977	-> 1556
    //   #3980	-> 1565
    //   #3981	-> 1571
    //   #3986	-> 1606
    //   #3987	-> 1612
    //   #3992	-> 1644
    //   #3746	-> 1645
    //   #3749	-> 1645
  }
  
  private void retrieveJpegImageSize(ByteOrderedDataInputStream paramByteOrderedDataInputStream, int paramInt) throws IOException {
    HashMap hashMap1 = this.mAttributes[paramInt];
    ExifAttribute exifAttribute1 = (ExifAttribute)hashMap1.get("ImageLength");
    HashMap hashMap2 = this.mAttributes[paramInt];
    ExifAttribute exifAttribute2 = (ExifAttribute)hashMap2.get("ImageWidth");
    if (exifAttribute1 == null || exifAttribute2 == null) {
      HashMap hashMap = this.mAttributes[paramInt];
      exifAttribute2 = (ExifAttribute)hashMap.get("JPEGInterchangeFormat");
      if (exifAttribute2 != null) {
        ByteOrder byteOrder = this.mExifByteOrder;
        int i = exifAttribute2.getIntValue(byteOrder);
        getJpegAttributes(paramByteOrderedDataInputStream, i, paramInt);
      } 
    } 
  }
  
  private void setThumbnailData(ByteOrderedDataInputStream paramByteOrderedDataInputStream) throws IOException {
    HashMap hashMap = this.mAttributes[4];
    ExifAttribute exifAttribute = (ExifAttribute)hashMap.get("Compression");
    if (exifAttribute != null) {
      int i = exifAttribute.getIntValue(this.mExifByteOrder);
      if (i != 1)
        if (i != 6) {
          if (i != 7)
            return; 
        } else {
          handleThumbnailFromJfif(paramByteOrderedDataInputStream, hashMap);
          return;
        }  
      if (isSupportedDataType(hashMap))
        handleThumbnailFromStrips(paramByteOrderedDataInputStream, hashMap); 
    } else {
      handleThumbnailFromJfif(paramByteOrderedDataInputStream, hashMap);
    } 
  }
  
  private void handleThumbnailFromJfif(ByteOrderedDataInputStream paramByteOrderedDataInputStream, HashMap paramHashMap) throws IOException {
    ExifAttribute exifAttribute2 = (ExifAttribute)paramHashMap.get("JPEGInterchangeFormat");
    ExifAttribute exifAttribute1 = (ExifAttribute)paramHashMap.get("JPEGInterchangeFormatLength");
    if (exifAttribute2 != null && exifAttribute1 != null) {
      int i = exifAttribute2.getIntValue(this.mExifByteOrder);
      int j = exifAttribute1.getIntValue(this.mExifByteOrder);
      int k = i;
      if (this.mMimeType == 7)
        k = i + this.mOrfMakerNoteOffset; 
      j = Math.min(j, paramByteOrderedDataInputStream.getLength() - k);
      if (k > 0 && j > 0) {
        this.mHasThumbnail = true;
        this.mThumbnailOffset = i = this.mExifOffset + k;
        this.mThumbnailLength = j;
        this.mThumbnailCompression = 6;
        if (this.mFilename == null && this.mAssetInputStream == null && this.mSeekableFileDescriptor == null) {
          byte[] arrayOfByte = new byte[j];
          paramByteOrderedDataInputStream.seek(i);
          paramByteOrderedDataInputStream.readFully(arrayOfByte);
          this.mThumbnailBytes = arrayOfByte;
        } 
      } 
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting thumbnail attributes with offset: ");
        stringBuilder.append(k);
        stringBuilder.append(", length: ");
        stringBuilder.append(j);
        Log.d("ExifInterface", stringBuilder.toString());
      } 
    } 
  }
  
  private void handleThumbnailFromStrips(ByteOrderedDataInputStream paramByteOrderedDataInputStream, HashMap paramHashMap) throws IOException {
    ExifAttribute exifAttribute2 = (ExifAttribute)paramHashMap.get("StripOffsets");
    ExifAttribute exifAttribute1 = (ExifAttribute)paramHashMap.get("StripByteCounts");
    if (exifAttribute2 != null && exifAttribute1 != null) {
      ByteOrder byteOrder1 = this.mExifByteOrder;
      long[] arrayOfLong1 = convertToLongArray(exifAttribute2.getValue(byteOrder1));
      ByteOrder byteOrder2 = this.mExifByteOrder;
      long[] arrayOfLong2 = convertToLongArray(exifAttribute1.getValue(byteOrder2));
      if (arrayOfLong1 == null || arrayOfLong1.length == 0) {
        Log.w("ExifInterface", "stripOffsets should not be null or have zero length.");
        return;
      } 
      if (arrayOfLong2 == null || arrayOfLong2.length == 0) {
        Log.w("ExifInterface", "stripByteCounts should not be null or have zero length.");
        return;
      } 
      if (arrayOfLong1.length != arrayOfLong2.length) {
        Log.w("ExifInterface", "stripOffsets and stripByteCounts should have same length.");
        return;
      } 
      byte[] arrayOfByte = new byte[(int)Arrays.stream(arrayOfLong2).sum()];
      int i = 0;
      int j = 0;
      this.mAreThumbnailStripsConsecutive = true;
      this.mHasThumbnailStrips = true;
      this.mHasThumbnail = true;
      for (byte b = 0; b < arrayOfLong1.length; b++) {
        int k = (int)arrayOfLong1[b];
        int m = (int)arrayOfLong2[b];
        if (b < arrayOfLong1.length - 1 && (k + m) != arrayOfLong1[b + 1])
          this.mAreThumbnailStripsConsecutive = false; 
        k -= i;
        if (k < 0)
          Log.d("ExifInterface", "Invalid strip offset value"); 
        paramByteOrderedDataInputStream.seek(k);
        byte[] arrayOfByte1 = new byte[m];
        paramByteOrderedDataInputStream.read(arrayOfByte1);
        i = i + k + m;
        System.arraycopy(arrayOfByte1, 0, arrayOfByte, j, arrayOfByte1.length);
        j += arrayOfByte1.length;
      } 
      this.mThumbnailBytes = arrayOfByte;
      if (this.mAreThumbnailStripsConsecutive) {
        this.mThumbnailOffset = (int)arrayOfLong1[0] + this.mExifOffset;
        this.mThumbnailLength = arrayOfByte.length;
      } 
    } 
  }
  
  private boolean isSupportedDataType(HashMap paramHashMap) throws IOException {
    ExifAttribute exifAttribute = (ExifAttribute)paramHashMap.get("BitsPerSample");
    if (exifAttribute != null) {
      int[] arrayOfInt = (int[])exifAttribute.getValue(this.mExifByteOrder);
      if (Arrays.equals(BITS_PER_SAMPLE_RGB, arrayOfInt))
        return true; 
      if (this.mMimeType == 3) {
        ExifAttribute exifAttribute1 = (ExifAttribute)paramHashMap.get("PhotometricInterpretation");
        if (exifAttribute1 != null) {
          ByteOrder byteOrder = this.mExifByteOrder;
          int i = exifAttribute1.getIntValue(byteOrder);
          if (i == 1) {
            int[] arrayOfInt1 = BITS_PER_SAMPLE_GREYSCALE_2;
            if (Arrays.equals(arrayOfInt, arrayOfInt1))
              return true; 
          } 
          if (i == 6) {
            int[] arrayOfInt1 = BITS_PER_SAMPLE_RGB;
            if (Arrays.equals(arrayOfInt, arrayOfInt1))
              return true; 
          } 
        } 
      } 
    } 
    if (DEBUG)
      Log.d("ExifInterface", "Unsupported data type value"); 
    return false;
  }
  
  private boolean isThumbnail(HashMap paramHashMap) throws IOException {
    ExifAttribute exifAttribute2 = (ExifAttribute)paramHashMap.get("ImageLength");
    ExifAttribute exifAttribute1 = (ExifAttribute)paramHashMap.get("ImageWidth");
    if (exifAttribute2 != null && exifAttribute1 != null) {
      int i = exifAttribute2.getIntValue(this.mExifByteOrder);
      int j = exifAttribute1.getIntValue(this.mExifByteOrder);
      if (i <= 512 && j <= 512)
        return true; 
    } 
    return false;
  }
  
  private void validateImages() throws IOException {
    swapBasedOnImageSize(0, 5);
    swapBasedOnImageSize(0, 4);
    swapBasedOnImageSize(5, 4);
    HashMap hashMap1 = this.mAttributes[1];
    ExifAttribute exifAttribute1 = (ExifAttribute)hashMap1.get("PixelXDimension");
    HashMap hashMap2 = this.mAttributes[1];
    ExifAttribute exifAttribute2 = (ExifAttribute)hashMap2.get("PixelYDimension");
    if (exifAttribute1 != null && exifAttribute2 != null) {
      this.mAttributes[0].put("ImageWidth", exifAttribute1);
      this.mAttributes[0].put("ImageLength", exifAttribute2);
    } 
    if (this.mAttributes[4].isEmpty() && 
      isThumbnail(this.mAttributes[5])) {
      HashMap[] arrayOfHashMap = this.mAttributes;
      arrayOfHashMap[4] = arrayOfHashMap[5];
      arrayOfHashMap[5] = new HashMap<>();
    } 
    if (!isThumbnail(this.mAttributes[4]))
      Log.d("ExifInterface", "No image meets the size requirements of a thumbnail image."); 
  }
  
  private void updateImageSizeValues(ByteOrderedDataInputStream paramByteOrderedDataInputStream, int paramInt) throws IOException {
    ByteOrder byteOrder;
    HashMap hashMap1 = this.mAttributes[paramInt];
    ExifAttribute exifAttribute1 = (ExifAttribute)hashMap1.get("DefaultCropSize");
    HashMap hashMap2 = this.mAttributes[paramInt];
    ExifAttribute exifAttribute2 = (ExifAttribute)hashMap2.get("SensorTopBorder");
    HashMap hashMap3 = this.mAttributes[paramInt];
    ExifAttribute exifAttribute3 = (ExifAttribute)hashMap3.get("SensorLeftBorder");
    HashMap hashMap4 = this.mAttributes[paramInt];
    ExifAttribute exifAttribute4 = (ExifAttribute)hashMap4.get("SensorBottomBorder");
    HashMap hashMap5 = this.mAttributes[paramInt];
    ExifAttribute exifAttribute5 = (ExifAttribute)hashMap5.get("SensorRightBorder");
    if (exifAttribute1 != null) {
      ExifAttribute exifAttribute6, exifAttribute7;
      if (exifAttribute1.format == 5) {
        ByteOrder byteOrder1 = this.mExifByteOrder;
        Rational[] arrayOfRational = (Rational[])exifAttribute1.getValue(byteOrder1);
        Rational rational2 = arrayOfRational[0];
        byteOrder = this.mExifByteOrder;
        exifAttribute7 = ExifAttribute.createURational(rational2, byteOrder);
        Rational rational1 = arrayOfRational[1];
        byteOrder = this.mExifByteOrder;
        exifAttribute6 = ExifAttribute.createURational(rational1, byteOrder);
      } else {
        ByteOrder byteOrder2 = this.mExifByteOrder;
        int[] arrayOfInt = (int[])exifAttribute7.getValue(byteOrder2);
        int i = arrayOfInt[0];
        ByteOrder byteOrder3 = this.mExifByteOrder;
        exifAttribute7 = ExifAttribute.createUShort(i, byteOrder3);
        i = arrayOfInt[1];
        ByteOrder byteOrder1 = this.mExifByteOrder;
        exifAttribute6 = ExifAttribute.createUShort(i, byteOrder1);
      } 
      this.mAttributes[paramInt].put("ImageWidth", exifAttribute7);
      this.mAttributes[paramInt].put("ImageLength", exifAttribute6);
    } else {
      ExifAttribute exifAttribute;
      if (byteOrder != null && exifAttribute3 != null && exifAttribute4 != null && exifAttribute5 != null) {
        int i = byteOrder.getIntValue(this.mExifByteOrder);
        int j = exifAttribute4.getIntValue(this.mExifByteOrder);
        int k = exifAttribute5.getIntValue(this.mExifByteOrder);
        int m = exifAttribute3.getIntValue(this.mExifByteOrder);
        if (j > i && k > m) {
          ByteOrder byteOrder1 = this.mExifByteOrder;
          exifAttribute = ExifAttribute.createUShort(j - i, byteOrder1);
          ByteOrder byteOrder2 = this.mExifByteOrder;
          ExifAttribute exifAttribute6 = ExifAttribute.createUShort(k - m, byteOrder2);
          this.mAttributes[paramInt].put("ImageLength", exifAttribute);
          this.mAttributes[paramInt].put("ImageWidth", exifAttribute6);
        } 
      } else {
        retrieveJpegImageSize((ByteOrderedDataInputStream)exifAttribute, paramInt);
      } 
    } 
  }
  
  private int writeExifSegment(ByteOrderedDataOutputStream paramByteOrderedDataOutputStream) throws IOException {
    char c;
    ExifTag[][] arrayOfExifTag = EXIF_TAGS;
    int[] arrayOfInt2 = new int[arrayOfExifTag.length];
    int[] arrayOfInt1 = new int[arrayOfExifTag.length];
    for (ExifTag exifTag : EXIF_POINTER_TAGS)
      removeAttribute(exifTag.name); 
    removeAttribute(JPEG_INTERCHANGE_FORMAT_TAG.name);
    removeAttribute(JPEG_INTERCHANGE_FORMAT_LENGTH_TAG.name);
    int j;
    for (j = 0; j < EXIF_TAGS.length; j++) {
      for (Object object : this.mAttributes[j].entrySet().toArray()) {
        object = object;
        if (object.getValue() == null)
          this.mAttributes[j].remove(object.getKey()); 
      } 
    } 
    if (!this.mAttributes[1].isEmpty()) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
      String str = (EXIF_POINTER_TAGS[1]).name;
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(0L, byteOrder);
      hashMap.put(str, exifAttribute);
    } 
    if (!this.mAttributes[2].isEmpty()) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
      String str = (EXIF_POINTER_TAGS[2]).name;
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(0L, byteOrder);
      hashMap.put(str, exifAttribute);
    } 
    if (!this.mAttributes[3].isEmpty()) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[1];
      String str = (EXIF_POINTER_TAGS[3]).name;
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(0L, byteOrder);
      hashMap.put(str, exifAttribute);
    } 
    if (this.mHasThumbnail) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[4];
      String str = JPEG_INTERCHANGE_FORMAT_TAG.name;
      ByteOrder byteOrder2 = this.mExifByteOrder;
      ExifAttribute exifAttribute2 = ExifAttribute.createULong(0L, byteOrder2);
      hashMap.put(str, exifAttribute2);
      hashMap = this.mAttributes[4];
      str = JPEG_INTERCHANGE_FORMAT_LENGTH_TAG.name;
      long l = this.mThumbnailLength;
      ByteOrder byteOrder1 = this.mExifByteOrder;
      ExifAttribute exifAttribute1 = ExifAttribute.createULong(l, byteOrder1);
      hashMap.put(str, exifAttribute1);
    } 
    for (j = 0; j < EXIF_TAGS.length; j++) {
      int k = 0;
      for (Map.Entry<?, ?> entry : this.mAttributes[j].entrySet()) {
        ExifAttribute exifAttribute = (ExifAttribute)entry.getValue();
        int n = exifAttribute.size();
        int m = k;
        if (n > 4)
          m = k + n; 
        k = m;
      } 
      arrayOfInt1[j] = arrayOfInt1[j] + k;
    } 
    j = 8;
    int i;
    for (i = 0; i < EXIF_TAGS.length; i++, j = k) {
      int k = j;
      if (!this.mAttributes[i].isEmpty()) {
        arrayOfInt2[i] = j;
        k = j + this.mAttributes[i].size() * 12 + 2 + 4 + arrayOfInt1[i];
      } 
    } 
    i = j;
    if (this.mHasThumbnail) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[4];
      String str = JPEG_INTERCHANGE_FORMAT_TAG.name;
      long l = j;
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(l, byteOrder);
      hashMap.put(str, exifAttribute);
      this.mThumbnailOffset = this.mExifOffset + j;
      i = j + this.mThumbnailLength;
    } 
    j = i;
    if (this.mMimeType == 4)
      j = i + 8; 
    if (DEBUG)
      for (i = 0; i < EXIF_TAGS.length; 
        Log.d("ExifInterface", String.format("index: %d, offsets: %d, tag count: %d, data sizes: %d, total size: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(n), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(j) })), i++)
        int n = arrayOfInt2[i], k = this.mAttributes[i].size(), m = arrayOfInt1[i];  
    if (!this.mAttributes[1].isEmpty()) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
      String str = (EXIF_POINTER_TAGS[1]).name;
      long l = arrayOfInt2[1];
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(l, byteOrder);
      hashMap.put(str, exifAttribute);
    } 
    if (!this.mAttributes[2].isEmpty()) {
      HashMap<String, ExifAttribute> hashMap = this.mAttributes[0];
      String str = (EXIF_POINTER_TAGS[2]).name;
      long l = arrayOfInt2[2];
      ByteOrder byteOrder = this.mExifByteOrder;
      ExifAttribute exifAttribute = ExifAttribute.createULong(l, byteOrder);
      hashMap.put(str, exifAttribute);
    } 
    if (!this.mAttributes[3].isEmpty())
      this.mAttributes[1].put((EXIF_POINTER_TAGS[3]).name, ExifAttribute.createULong(arrayOfInt2[3], this.mExifByteOrder)); 
    i = this.mMimeType;
    if (i == 4) {
      paramByteOrderedDataOutputStream.writeUnsignedShort(j);
      paramByteOrderedDataOutputStream.write(IDENTIFIER_EXIF_APP1);
    } else if (i == 13) {
      paramByteOrderedDataOutputStream.writeInt(j);
      paramByteOrderedDataOutputStream.write(PNG_CHUNK_TYPE_EXIF);
    } 
    if (this.mExifByteOrder == ByteOrder.BIG_ENDIAN) {
      c = '䵍';
    } else {
      c = '䥉';
    } 
    paramByteOrderedDataOutputStream.writeShort(c);
    paramByteOrderedDataOutputStream.setByteOrder(this.mExifByteOrder);
    paramByteOrderedDataOutputStream.writeUnsignedShort(42);
    paramByteOrderedDataOutputStream.writeUnsignedInt(8L);
    for (i = 0; i < EXIF_TAGS.length; i++) {
      if (!this.mAttributes[i].isEmpty()) {
        paramByteOrderedDataOutputStream.writeUnsignedShort(this.mAttributes[i].size());
        int k = arrayOfInt2[i] + 2 + this.mAttributes[i].size() * 12 + 4;
        for (Map.Entry<?, ?> entry : this.mAttributes[i].entrySet()) {
          HashMap hashMap = sExifTagMapsForWriting[i];
          ExifTag exifTag = (ExifTag)hashMap.get(entry.getKey());
          int m = exifTag.number;
          ExifAttribute exifAttribute = (ExifAttribute)entry.getValue();
          int n = exifAttribute.size();
          paramByteOrderedDataOutputStream.writeUnsignedShort(m);
          paramByteOrderedDataOutputStream.writeUnsignedShort(exifAttribute.format);
          paramByteOrderedDataOutputStream.writeInt(exifAttribute.numberOfComponents);
          if (n > 4) {
            paramByteOrderedDataOutputStream.writeUnsignedInt(k);
            m = k + n;
          } else {
            paramByteOrderedDataOutputStream.write(exifAttribute.bytes);
            m = k;
            if (n < 4)
              while (true) {
                m = k;
                if (n < 4) {
                  paramByteOrderedDataOutputStream.writeByte(0);
                  n++;
                  continue;
                } 
                break;
              }  
          } 
          k = m;
        } 
        if (i == 0 && !this.mAttributes[4].isEmpty()) {
          paramByteOrderedDataOutputStream.writeUnsignedInt(arrayOfInt2[4]);
        } else {
          paramByteOrderedDataOutputStream.writeUnsignedInt(0L);
        } 
        for (Map.Entry<?, ?> entry : this.mAttributes[i].entrySet()) {
          ExifAttribute exifAttribute = (ExifAttribute)entry.getValue();
          if (exifAttribute.bytes.length > 4)
            paramByteOrderedDataOutputStream.write(exifAttribute.bytes, 0, exifAttribute.bytes.length); 
        } 
      } 
    } 
    if (this.mHasThumbnail)
      paramByteOrderedDataOutputStream.write(getThumbnailBytes()); 
    paramByteOrderedDataOutputStream.setByteOrder(ByteOrder.BIG_ENDIAN);
    return j;
  }
  
  private static Pair<Integer, Integer> guessDataFormat(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: ldc_w ','
    //   4: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   7: istore_1
    //   8: iconst_2
    //   9: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   12: astore_2
    //   13: iconst_m1
    //   14: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   17: astore_3
    //   18: iload_1
    //   19: ifeq -> 249
    //   22: aload_0
    //   23: ldc_w ','
    //   26: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   29: astore #4
    //   31: aload #4
    //   33: iconst_0
    //   34: aaload
    //   35: invokestatic guessDataFormat : (Ljava/lang/String;)Landroid/util/Pair;
    //   38: astore_0
    //   39: aload_0
    //   40: getfield first : Ljava/lang/Object;
    //   43: checkcast java/lang/Integer
    //   46: invokevirtual intValue : ()I
    //   49: iconst_2
    //   50: if_icmpne -> 55
    //   53: aload_0
    //   54: areturn
    //   55: iconst_1
    //   56: istore #5
    //   58: iload #5
    //   60: aload #4
    //   62: arraylength
    //   63: if_icmpge -> 247
    //   66: aload #4
    //   68: iload #5
    //   70: aaload
    //   71: invokestatic guessDataFormat : (Ljava/lang/String;)Landroid/util/Pair;
    //   74: astore #6
    //   76: iconst_m1
    //   77: istore #7
    //   79: iconst_m1
    //   80: istore #8
    //   82: aload #6
    //   84: getfield first : Ljava/lang/Object;
    //   87: aload_0
    //   88: getfield first : Ljava/lang/Object;
    //   91: if_acmpeq -> 106
    //   94: aload #6
    //   96: getfield second : Ljava/lang/Object;
    //   99: aload_0
    //   100: getfield first : Ljava/lang/Object;
    //   103: if_acmpne -> 118
    //   106: aload_0
    //   107: getfield first : Ljava/lang/Object;
    //   110: checkcast java/lang/Integer
    //   113: invokevirtual intValue : ()I
    //   116: istore #7
    //   118: iload #8
    //   120: istore #9
    //   122: aload_0
    //   123: getfield second : Ljava/lang/Object;
    //   126: checkcast java/lang/Integer
    //   129: invokevirtual intValue : ()I
    //   132: iconst_m1
    //   133: if_icmpeq -> 176
    //   136: aload #6
    //   138: getfield first : Ljava/lang/Object;
    //   141: aload_0
    //   142: getfield second : Ljava/lang/Object;
    //   145: if_acmpeq -> 164
    //   148: iload #8
    //   150: istore #9
    //   152: aload #6
    //   154: getfield second : Ljava/lang/Object;
    //   157: aload_0
    //   158: getfield second : Ljava/lang/Object;
    //   161: if_acmpne -> 176
    //   164: aload_0
    //   165: getfield second : Ljava/lang/Object;
    //   168: checkcast java/lang/Integer
    //   171: invokevirtual intValue : ()I
    //   174: istore #9
    //   176: iload #7
    //   178: iconst_m1
    //   179: if_icmpne -> 198
    //   182: iload #9
    //   184: iconst_m1
    //   185: if_icmpne -> 198
    //   188: new android/util/Pair
    //   191: dup
    //   192: aload_2
    //   193: aload_3
    //   194: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   197: areturn
    //   198: iload #7
    //   200: iconst_m1
    //   201: if_icmpne -> 221
    //   204: new android/util/Pair
    //   207: dup
    //   208: iload #9
    //   210: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   213: aload_3
    //   214: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   217: astore_0
    //   218: goto -> 241
    //   221: iload #9
    //   223: iconst_m1
    //   224: if_icmpne -> 241
    //   227: new android/util/Pair
    //   230: dup
    //   231: iload #7
    //   233: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   236: aload_3
    //   237: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   240: astore_0
    //   241: iinc #5, 1
    //   244: goto -> 58
    //   247: aload_0
    //   248: areturn
    //   249: aload_0
    //   250: ldc_w '/'
    //   253: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   256: ifeq -> 386
    //   259: aload_0
    //   260: ldc_w '/'
    //   263: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   266: astore_0
    //   267: aload_0
    //   268: arraylength
    //   269: iconst_2
    //   270: if_icmpne -> 376
    //   273: aload_0
    //   274: iconst_0
    //   275: aaload
    //   276: invokestatic parseDouble : (Ljava/lang/String;)D
    //   279: d2l
    //   280: lstore #10
    //   282: aload_0
    //   283: iconst_1
    //   284: aaload
    //   285: invokestatic parseDouble : (Ljava/lang/String;)D
    //   288: d2l
    //   289: lstore #12
    //   291: lload #10
    //   293: lconst_0
    //   294: lcmp
    //   295: iflt -> 359
    //   298: lload #12
    //   300: lconst_0
    //   301: lcmp
    //   302: ifge -> 308
    //   305: goto -> 359
    //   308: lload #10
    //   310: ldc2_w 2147483647
    //   313: lcmp
    //   314: ifgt -> 346
    //   317: lload #12
    //   319: ldc2_w 2147483647
    //   322: lcmp
    //   323: ifle -> 329
    //   326: goto -> 346
    //   329: new android/util/Pair
    //   332: dup
    //   333: bipush #10
    //   335: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   338: iconst_5
    //   339: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   342: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   345: areturn
    //   346: new android/util/Pair
    //   349: dup
    //   350: iconst_5
    //   351: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   354: aload_3
    //   355: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   358: areturn
    //   359: new android/util/Pair
    //   362: dup
    //   363: bipush #10
    //   365: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   368: aload_3
    //   369: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   372: astore_0
    //   373: aload_0
    //   374: areturn
    //   375: astore_0
    //   376: new android/util/Pair
    //   379: dup
    //   380: aload_2
    //   381: aload_3
    //   382: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   385: areturn
    //   386: aload_0
    //   387: invokestatic parseLong : (Ljava/lang/String;)J
    //   390: invokestatic valueOf : (J)Ljava/lang/Long;
    //   393: astore #4
    //   395: aload #4
    //   397: invokevirtual longValue : ()J
    //   400: lconst_0
    //   401: lcmp
    //   402: iflt -> 433
    //   405: aload #4
    //   407: invokevirtual longValue : ()J
    //   410: ldc2_w 65535
    //   413: lcmp
    //   414: ifgt -> 433
    //   417: new android/util/Pair
    //   420: dup
    //   421: iconst_3
    //   422: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   425: iconst_4
    //   426: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   429: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   432: areturn
    //   433: aload #4
    //   435: invokevirtual longValue : ()J
    //   438: lconst_0
    //   439: lcmp
    //   440: ifge -> 457
    //   443: new android/util/Pair
    //   446: dup
    //   447: bipush #9
    //   449: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   452: aload_3
    //   453: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   456: areturn
    //   457: new android/util/Pair
    //   460: dup
    //   461: iconst_4
    //   462: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   465: aload_3
    //   466: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   469: astore #4
    //   471: aload #4
    //   473: areturn
    //   474: astore #4
    //   476: aload_0
    //   477: invokestatic parseDouble : (Ljava/lang/String;)D
    //   480: pop2
    //   481: new android/util/Pair
    //   484: dup
    //   485: bipush #12
    //   487: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   490: aload_3
    //   491: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   494: astore_0
    //   495: aload_0
    //   496: areturn
    //   497: astore_0
    //   498: new android/util/Pair
    //   501: dup
    //   502: aload_2
    //   503: aload_3
    //   504: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   507: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4518	-> 0
    //   #4567	-> 8
    //   #4518	-> 13
    //   #4567	-> 13
    //   #4518	-> 18
    //   #4519	-> 22
    //   #4520	-> 31
    //   #4521	-> 39
    //   #4522	-> 53
    //   #4524	-> 55
    //   #4525	-> 66
    //   #4526	-> 76
    //   #4527	-> 82
    //   #4529	-> 106
    //   #4531	-> 118
    //   #4533	-> 164
    //   #4535	-> 176
    //   #4536	-> 188
    //   #4538	-> 198
    //   #4539	-> 204
    //   #4540	-> 218
    //   #4542	-> 221
    //   #4543	-> 227
    //   #4524	-> 241
    //   #4547	-> 247
    //   #4550	-> 249
    //   #4551	-> 259
    //   #4552	-> 267
    //   #4554	-> 273
    //   #4555	-> 282
    //   #4556	-> 291
    //   #4559	-> 308
    //   #4562	-> 329
    //   #4560	-> 346
    //   #4557	-> 359
    //   #4563	-> 375
    //   #4567	-> 376
    //   #4570	-> 386
    //   #4571	-> 395
    //   #4572	-> 417
    //   #4574	-> 433
    //   #4575	-> 443
    //   #4577	-> 457
    //   #4578	-> 474
    //   #4582	-> 476
    //   #4583	-> 481
    //   #4584	-> 497
    //   #4587	-> 498
    // Exception table:
    //   from	to	target	type
    //   273	282	375	java/lang/NumberFormatException
    //   282	291	375	java/lang/NumberFormatException
    //   329	346	375	java/lang/NumberFormatException
    //   346	359	375	java/lang/NumberFormatException
    //   359	373	375	java/lang/NumberFormatException
    //   386	395	474	java/lang/NumberFormatException
    //   395	417	474	java/lang/NumberFormatException
    //   417	433	474	java/lang/NumberFormatException
    //   433	443	474	java/lang/NumberFormatException
    //   443	457	474	java/lang/NumberFormatException
    //   457	471	474	java/lang/NumberFormatException
    //   476	481	497	java/lang/NumberFormatException
    //   481	495	497	java/lang/NumberFormatException
  }
  
  private static class ByteOrderedDataInputStream extends InputStream implements DataInput {
    private static final ByteOrder BIG_ENDIAN = ByteOrder.BIG_ENDIAN;
    
    private static final ByteOrder LITTLE_ENDIAN = ByteOrder.LITTLE_ENDIAN;
    
    static {
    
    }
    
    private ByteOrder mByteOrder = ByteOrder.BIG_ENDIAN;
    
    private DataInputStream mDataInputStream;
    
    private InputStream mInputStream;
    
    private final int mLength;
    
    private int mPosition;
    
    public ByteOrderedDataInputStream(InputStream param1InputStream) throws IOException {
      this.mInputStream = param1InputStream;
      this.mDataInputStream = (DataInputStream)(param1InputStream = new DataInputStream(param1InputStream));
      int i = param1InputStream.available();
      this.mPosition = 0;
      this.mDataInputStream.mark(i);
    }
    
    public ByteOrderedDataInputStream(byte[] param1ArrayOfbyte) throws IOException {
      this(new ByteArrayInputStream(param1ArrayOfbyte));
    }
    
    public void setByteOrder(ByteOrder param1ByteOrder) {
      this.mByteOrder = param1ByteOrder;
    }
    
    public void seek(long param1Long) throws IOException {
      int i = this.mPosition;
      if (i > param1Long) {
        this.mPosition = 0;
        this.mDataInputStream.reset();
        this.mDataInputStream.mark(this.mLength);
      } else {
        param1Long -= i;
      } 
      if (skipBytes((int)param1Long) == (int)param1Long)
        return; 
      throw new IOException("Couldn't seek up to the byteCount");
    }
    
    public int peek() {
      return this.mPosition;
    }
    
    public int available() throws IOException {
      return this.mDataInputStream.available();
    }
    
    public int read() throws IOException {
      this.mPosition++;
      return this.mDataInputStream.read();
    }
    
    public int readUnsignedByte() throws IOException {
      this.mPosition++;
      return this.mDataInputStream.readUnsignedByte();
    }
    
    public String readLine() throws IOException {
      Log.d("ExifInterface", "Currently unsupported");
      return null;
    }
    
    public boolean readBoolean() throws IOException {
      this.mPosition++;
      return this.mDataInputStream.readBoolean();
    }
    
    public char readChar() throws IOException {
      this.mPosition += 2;
      return this.mDataInputStream.readChar();
    }
    
    public String readUTF() throws IOException {
      this.mPosition += 2;
      return this.mDataInputStream.readUTF();
    }
    
    public void readFully(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      int i = this.mPosition + param1Int2;
      if (i <= this.mLength) {
        if (this.mDataInputStream.read(param1ArrayOfbyte, param1Int1, param1Int2) == param1Int2)
          return; 
        throw new IOException("Couldn't read up to the length of buffer");
      } 
      throw new EOFException();
    }
    
    public void readFully(byte[] param1ArrayOfbyte) throws IOException {
      int i = this.mPosition + param1ArrayOfbyte.length;
      if (i <= this.mLength) {
        if (this.mDataInputStream.read(param1ArrayOfbyte, 0, param1ArrayOfbyte.length) == param1ArrayOfbyte.length)
          return; 
        throw new IOException("Couldn't read up to the length of buffer");
      } 
      throw new EOFException();
    }
    
    public byte readByte() throws IOException {
      int i = this.mPosition + 1;
      if (i <= this.mLength) {
        i = this.mDataInputStream.read();
        if (i >= 0)
          return (byte)i; 
        throw new EOFException();
      } 
      throw new EOFException();
    }
    
    public short readShort() throws IOException {
      int i = this.mPosition + 2;
      if (i <= this.mLength) {
        int j = this.mDataInputStream.read();
        i = this.mDataInputStream.read();
        if ((j | i) >= 0) {
          ByteOrder byteOrder = this.mByteOrder;
          if (byteOrder == LITTLE_ENDIAN)
            return (short)((i << 8) + j); 
          if (byteOrder == BIG_ENDIAN)
            return (short)((j << 8) + i); 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid byte order: ");
          stringBuilder.append(this.mByteOrder);
          throw new IOException(stringBuilder.toString());
        } 
        throw new EOFException();
      } 
      throw new EOFException();
    }
    
    public int readInt() throws IOException {
      int i = this.mPosition + 4;
      if (i <= this.mLength) {
        i = this.mDataInputStream.read();
        int j = this.mDataInputStream.read();
        int k = this.mDataInputStream.read();
        int m = this.mDataInputStream.read();
        if ((i | j | k | m) >= 0) {
          ByteOrder byteOrder = this.mByteOrder;
          if (byteOrder == LITTLE_ENDIAN)
            return (m << 24) + (k << 16) + (j << 8) + i; 
          if (byteOrder == BIG_ENDIAN)
            return (i << 24) + (j << 16) + (k << 8) + m; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid byte order: ");
          stringBuilder.append(this.mByteOrder);
          throw new IOException(stringBuilder.toString());
        } 
        throw new EOFException();
      } 
      throw new EOFException();
    }
    
    public int skipBytes(int param1Int) throws IOException {
      int i = Math.min(param1Int, this.mLength - this.mPosition);
      param1Int = 0;
      while (param1Int < i)
        param1Int += this.mDataInputStream.skipBytes(i - param1Int); 
      this.mPosition += param1Int;
      return param1Int;
    }
    
    public int readUnsignedShort() throws IOException {
      int i = this.mPosition + 2;
      if (i <= this.mLength) {
        i = this.mDataInputStream.read();
        int j = this.mDataInputStream.read();
        if ((i | j) >= 0) {
          ByteOrder byteOrder = this.mByteOrder;
          if (byteOrder == LITTLE_ENDIAN)
            return (j << 8) + i; 
          if (byteOrder == BIG_ENDIAN)
            return (i << 8) + j; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid byte order: ");
          stringBuilder.append(this.mByteOrder);
          throw new IOException(stringBuilder.toString());
        } 
        throw new EOFException();
      } 
      throw new EOFException();
    }
    
    public long readUnsignedInt() throws IOException {
      return readInt() & 0xFFFFFFFFL;
    }
    
    public long readLong() throws IOException {
      int i = this.mPosition + 8;
      if (i <= this.mLength) {
        int j = this.mDataInputStream.read();
        int k = this.mDataInputStream.read();
        int m = this.mDataInputStream.read();
        int n = this.mDataInputStream.read();
        i = this.mDataInputStream.read();
        int i1 = this.mDataInputStream.read();
        int i2 = this.mDataInputStream.read();
        int i3 = this.mDataInputStream.read();
        if ((j | k | m | n | i | i1 | i2 | i3) >= 0) {
          ByteOrder byteOrder = this.mByteOrder;
          if (byteOrder == LITTLE_ENDIAN)
            return (i3 << 56L) + (i2 << 48L) + (i1 << 40L) + (i << 32L) + (n << 24L) + (m << 16L) + (k << 8L) + j; 
          if (byteOrder == BIG_ENDIAN)
            return (j << 56L) + (k << 48L) + (m << 40L) + (n << 32L) + (i << 24L) + (i1 << 16L) + (i2 << 8L) + i3; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid byte order: ");
          stringBuilder.append(this.mByteOrder);
          throw new IOException(stringBuilder.toString());
        } 
        throw new EOFException();
      } 
      throw new EOFException();
    }
    
    public float readFloat() throws IOException {
      return Float.intBitsToFloat(readInt());
    }
    
    public double readDouble() throws IOException {
      return Double.longBitsToDouble(readLong());
    }
    
    public int getLength() {
      return this.mLength;
    }
  }
  
  private static class ByteOrderedDataOutputStream extends FilterOutputStream {
    private ByteOrder mByteOrder;
    
    final OutputStream mOutputStream;
    
    public ByteOrderedDataOutputStream(OutputStream param1OutputStream, ByteOrder param1ByteOrder) {
      super(param1OutputStream);
      this.mOutputStream = param1OutputStream;
      this.mByteOrder = param1ByteOrder;
    }
    
    public void setByteOrder(ByteOrder param1ByteOrder) {
      this.mByteOrder = param1ByteOrder;
    }
    
    public void write(byte[] param1ArrayOfbyte) throws IOException {
      this.mOutputStream.write(param1ArrayOfbyte);
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      this.mOutputStream.write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void writeByte(int param1Int) throws IOException {
      this.mOutputStream.write(param1Int);
    }
    
    public void writeShort(short param1Short) throws IOException {
      if (this.mByteOrder == ByteOrder.LITTLE_ENDIAN) {
        this.mOutputStream.write(param1Short >>> 0 & 0xFF);
        this.mOutputStream.write(param1Short >>> 8 & 0xFF);
      } else if (this.mByteOrder == ByteOrder.BIG_ENDIAN) {
        this.mOutputStream.write(param1Short >>> 8 & 0xFF);
        this.mOutputStream.write(param1Short >>> 0 & 0xFF);
      } 
    }
    
    public void writeInt(int param1Int) throws IOException {
      if (this.mByteOrder == ByteOrder.LITTLE_ENDIAN) {
        this.mOutputStream.write(param1Int >>> 0 & 0xFF);
        this.mOutputStream.write(param1Int >>> 8 & 0xFF);
        this.mOutputStream.write(param1Int >>> 16 & 0xFF);
        this.mOutputStream.write(param1Int >>> 24 & 0xFF);
      } else if (this.mByteOrder == ByteOrder.BIG_ENDIAN) {
        this.mOutputStream.write(param1Int >>> 24 & 0xFF);
        this.mOutputStream.write(param1Int >>> 16 & 0xFF);
        this.mOutputStream.write(param1Int >>> 8 & 0xFF);
        this.mOutputStream.write(param1Int >>> 0 & 0xFF);
      } 
    }
    
    public void writeUnsignedShort(int param1Int) throws IOException {
      writeShort((short)param1Int);
    }
    
    public void writeUnsignedInt(long param1Long) throws IOException {
      writeInt((int)param1Long);
    }
  }
  
  private void swapBasedOnImageSize(int paramInt1, int paramInt2) throws IOException {
    if (this.mAttributes[paramInt1].isEmpty() || this.mAttributes[paramInt2].isEmpty()) {
      if (DEBUG)
        Log.d("ExifInterface", "Cannot perform swap since only one image data exists"); 
      return;
    } 
    HashMap hashMap1 = this.mAttributes[paramInt1];
    ExifAttribute exifAttribute1 = (ExifAttribute)hashMap1.get("ImageLength");
    HashMap hashMap2 = this.mAttributes[paramInt1];
    ExifAttribute exifAttribute2 = (ExifAttribute)hashMap2.get("ImageWidth");
    HashMap hashMap3 = this.mAttributes[paramInt2];
    ExifAttribute exifAttribute3 = (ExifAttribute)hashMap3.get("ImageLength");
    HashMap hashMap4 = this.mAttributes[paramInt2];
    ExifAttribute exifAttribute4 = (ExifAttribute)hashMap4.get("ImageWidth");
    if (exifAttribute1 == null || exifAttribute2 == null) {
      if (DEBUG)
        Log.d("ExifInterface", "First image does not contain valid size information"); 
      return;
    } 
    if (exifAttribute3 == null || exifAttribute4 == null) {
      if (DEBUG)
        Log.d("ExifInterface", "Second image does not contain valid size information"); 
      return;
    } 
    int i = exifAttribute1.getIntValue(this.mExifByteOrder);
    int j = exifAttribute2.getIntValue(this.mExifByteOrder);
    int k = exifAttribute3.getIntValue(this.mExifByteOrder);
    int m = exifAttribute4.getIntValue(this.mExifByteOrder);
    if (i < k && j < m) {
      HashMap arrayOfHashMap[] = this.mAttributes, hashMap = arrayOfHashMap[paramInt1];
      arrayOfHashMap[paramInt1] = arrayOfHashMap[paramInt2];
      arrayOfHashMap[paramInt2] = hashMap;
    } 
  }
  
  private boolean containsMatch(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    for (byte b = 0; b < paramArrayOfbyte1.length - paramArrayOfbyte2.length; b++) {
      for (byte b1 = 0; b1 < paramArrayOfbyte2.length && 
        paramArrayOfbyte1[b + b1] == paramArrayOfbyte2[b1]; b1++) {
        if (b1 == paramArrayOfbyte2.length - 1)
          return true; 
      } 
    } 
    return false;
  }
  
  private static void copy(InputStream paramInputStream, OutputStream paramOutputStream, int paramInt) throws IOException {
    byte[] arrayOfByte = new byte[8192];
    while (paramInt > 0) {
      int i = Math.min(paramInt, 8192);
      int j = paramInputStream.read(arrayOfByte, 0, i);
      if (j == i) {
        paramInt -= j;
        paramOutputStream.write(arrayOfByte, 0, j);
        continue;
      } 
      throw new IOException("Failed to copy the given amount of bytes from the inputstream to the output stream.");
    } 
  }
  
  private static long[] convertToLongArray(Object paramObject) {
    if (paramObject instanceof int[]) {
      int[] arrayOfInt = (int[])paramObject;
      paramObject = new long[arrayOfInt.length];
      for (byte b = 0; b < arrayOfInt.length; b++)
        paramObject[b] = arrayOfInt[b]; 
      return (long[])paramObject;
    } 
    if (paramObject instanceof long[])
      return (long[])paramObject; 
    return null;
  }
  
  private static String byteArrayToHexString(byte[] paramArrayOfbyte) {
    StringBuilder stringBuilder = new StringBuilder(paramArrayOfbyte.length * 2);
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      stringBuilder.append(String.format("%02x", new Object[] { Byte.valueOf(paramArrayOfbyte[b]) }));
    } 
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ExifStreamType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface IfdType {}
}
