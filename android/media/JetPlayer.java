package android.media;

import android.content.res.AssetFileDescriptor;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AndroidRuntimeException;
import android.util.Log;
import java.io.FileDescriptor;
import java.lang.ref.WeakReference;

public class JetPlayer {
  private static int MAXTRACKS = 32;
  
  private NativeEventHandler mEventHandler = null;
  
  private Looper mInitializationLooper = null;
  
  private final Object mEventListenerLock = new Object();
  
  private OnJetEventListener mJetEventListener = null;
  
  private static final int JET_EVENT = 1;
  
  private static final int JET_EVENT_CHAN_MASK = 245760;
  
  private static final int JET_EVENT_CHAN_SHIFT = 14;
  
  private static final int JET_EVENT_CTRL_MASK = 16256;
  
  private static final int JET_EVENT_CTRL_SHIFT = 7;
  
  private static final int JET_EVENT_SEG_MASK = -16777216;
  
  private static final int JET_EVENT_SEG_SHIFT = 24;
  
  private static final int JET_EVENT_TRACK_MASK = 16515072;
  
  private static final int JET_EVENT_TRACK_SHIFT = 18;
  
  private static final int JET_EVENT_VAL_MASK = 127;
  
  private static final int JET_NUMQUEUEDSEGMENT_UPDATE = 3;
  
  private static final int JET_OUTPUT_CHANNEL_CONFIG = 12;
  
  private static final int JET_OUTPUT_RATE = 22050;
  
  private static final int JET_PAUSE_UPDATE = 4;
  
  private static final int JET_USERID_UPDATE = 2;
  
  private static final String TAG = "JetPlayer-J";
  
  private static JetPlayer singletonRef;
  
  private long mNativePlayerInJavaObj;
  
  static {
    System.loadLibrary("media_jni");
  }
  
  public static JetPlayer getJetPlayer() {
    if (singletonRef == null)
      singletonRef = new JetPlayer(); 
    return singletonRef;
  }
  
  public Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException();
  }
  
  private JetPlayer() {
    Looper looper = Looper.myLooper();
    if (looper == null)
      this.mInitializationLooper = Looper.getMainLooper(); 
    int i = AudioTrack.getMinBufferSize(22050, 12, 2);
    if (i != -1 && i != -2) {
      WeakReference<JetPlayer> weakReference = new WeakReference<>(this);
      int j = getMaxTracks();
      i /= AudioFormat.getBytesPerSample(2) * 2;
      i = Math.max(1200, i);
      native_setup(weakReference, j, i);
    } 
  }
  
  protected void finalize() {
    native_finalize();
  }
  
  public void release() {
    native_release();
    singletonRef = null;
  }
  
  public static int getMaxTracks() {
    return MAXTRACKS;
  }
  
  public boolean loadJetFile(String paramString) {
    return native_loadJetFromFile(paramString);
  }
  
  public boolean loadJetFile(AssetFileDescriptor paramAssetFileDescriptor) {
    long l = paramAssetFileDescriptor.getLength();
    if (l >= 0L) {
      FileDescriptor fileDescriptor = paramAssetFileDescriptor.getFileDescriptor();
      long l1 = paramAssetFileDescriptor.getStartOffset();
      return native_loadJetFromFileD(fileDescriptor, l1, l);
    } 
    throw new AndroidRuntimeException("no length for fd");
  }
  
  public boolean closeJetFile() {
    return native_closeJetFile();
  }
  
  public boolean play() {
    return native_playJet();
  }
  
  public boolean pause() {
    return native_pauseJet();
  }
  
  public boolean queueJetSegment(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte paramByte) {
    return native_queueJetSegment(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramByte);
  }
  
  public boolean queueJetSegmentMuteArray(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean[] paramArrayOfboolean, byte paramByte) {
    if (paramArrayOfboolean.length != getMaxTracks())
      return false; 
    return native_queueJetSegmentMuteArray(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfboolean, paramByte);
  }
  
  public boolean setMuteFlags(int paramInt, boolean paramBoolean) {
    return native_setMuteFlags(paramInt, paramBoolean);
  }
  
  public boolean setMuteArray(boolean[] paramArrayOfboolean, boolean paramBoolean) {
    if (paramArrayOfboolean.length != getMaxTracks())
      return false; 
    return native_setMuteArray(paramArrayOfboolean, paramBoolean);
  }
  
  public boolean setMuteFlag(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    return native_setMuteFlag(paramInt, paramBoolean1, paramBoolean2);
  }
  
  public boolean triggerClip(int paramInt) {
    return native_triggerClip(paramInt);
  }
  
  public boolean clearQueue() {
    return native_clearQueue();
  }
  
  class NativeEventHandler extends Handler {
    private JetPlayer mJet;
    
    final JetPlayer this$0;
    
    public NativeEventHandler(JetPlayer param1JetPlayer1, Looper param1Looper) {
      super(param1Looper);
      this.mJet = param1JetPlayer1;
    }
    
    public void handleMessage(Message param1Message) {
      synchronized (JetPlayer.this.mEventListenerLock) {
        JetPlayer.OnJetEventListener onJetEventListener = this.mJet.mJetEventListener;
        int i = param1Message.what;
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                null = new StringBuilder();
                null.append("Unknown message type ");
                null.append(param1Message.what);
                JetPlayer.loge(null.toString());
                return;
              } 
              if (onJetEventListener != null)
                onJetEventListener.onJetPauseUpdate(this.mJet, param1Message.arg1); 
              return;
            } 
            if (onJetEventListener != null)
              onJetEventListener.onJetNumQueuedSegmentUpdate(this.mJet, param1Message.arg1); 
            return;
          } 
          if (onJetEventListener != null)
            onJetEventListener.onJetUserIdUpdate(this.mJet, param1Message.arg1, param1Message.arg2); 
          return;
        } 
        if (onJetEventListener != null)
          JetPlayer.this.mJetEventListener.onJetEvent(this.mJet, (short)((param1Message.arg1 & 0xFF000000) >> 24), (byte)((param1Message.arg1 & 0xFC0000) >> 18), (byte)(((param1Message.arg1 & 0x3C000) >> 14) + 1), (byte)((param1Message.arg1 & 0x3F80) >> 7), (byte)(param1Message.arg1 & 0x7F)); 
        return;
      } 
    }
  }
  
  public void setEventListener(OnJetEventListener paramOnJetEventListener) {
    setEventListener(paramOnJetEventListener, null);
  }
  
  public void setEventListener(OnJetEventListener paramOnJetEventListener, Handler paramHandler) {
    synchronized (this.mEventListenerLock) {
      this.mJetEventListener = paramOnJetEventListener;
      if (paramOnJetEventListener != null) {
        if (paramHandler != null) {
          NativeEventHandler nativeEventHandler = new NativeEventHandler();
          this(this, this, paramHandler.getLooper());
          this.mEventHandler = nativeEventHandler;
        } else {
          NativeEventHandler nativeEventHandler = new NativeEventHandler();
          this(this, this, this.mInitializationLooper);
          this.mEventHandler = nativeEventHandler;
        } 
      } else {
        this.mEventHandler = null;
      } 
      return;
    } 
  }
  
  private static void postEventFromNative(Object paramObject, int paramInt1, int paramInt2, int paramInt3) {
    paramObject = ((WeakReference<JetPlayer>)paramObject).get();
    if (paramObject != null) {
      NativeEventHandler nativeEventHandler = ((JetPlayer)paramObject).mEventHandler;
      if (nativeEventHandler != null) {
        Message message = nativeEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, null);
        ((JetPlayer)paramObject).mEventHandler.sendMessage(message);
      } 
    } 
  }
  
  private static void logd(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[ android.media.JetPlayer ] ");
    stringBuilder.append(paramString);
    Log.d("JetPlayer-J", stringBuilder.toString());
  }
  
  private static void loge(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[ android.media.JetPlayer ] ");
    stringBuilder.append(paramString);
    Log.e("JetPlayer-J", stringBuilder.toString());
  }
  
  private final native boolean native_clearQueue();
  
  private final native boolean native_closeJetFile();
  
  private final native void native_finalize();
  
  private final native boolean native_loadJetFromFile(String paramString);
  
  private final native boolean native_loadJetFromFileD(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2);
  
  private final native boolean native_pauseJet();
  
  private final native boolean native_playJet();
  
  private final native boolean native_queueJetSegment(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte paramByte);
  
  private final native boolean native_queueJetSegmentMuteArray(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean[] paramArrayOfboolean, byte paramByte);
  
  private final native void native_release();
  
  private final native boolean native_setMuteArray(boolean[] paramArrayOfboolean, boolean paramBoolean);
  
  private final native boolean native_setMuteFlag(int paramInt, boolean paramBoolean1, boolean paramBoolean2);
  
  private final native boolean native_setMuteFlags(int paramInt, boolean paramBoolean);
  
  private final native boolean native_setup(Object paramObject, int paramInt1, int paramInt2);
  
  private final native boolean native_triggerClip(int paramInt);
  
  public static interface OnJetEventListener {
    void onJetEvent(JetPlayer param1JetPlayer, short param1Short, byte param1Byte1, byte param1Byte2, byte param1Byte3, byte param1Byte4);
    
    void onJetNumQueuedSegmentUpdate(JetPlayer param1JetPlayer, int param1Int);
    
    void onJetPauseUpdate(JetPlayer param1JetPlayer, int param1Int);
    
    void onJetUserIdUpdate(JetPlayer param1JetPlayer, int param1Int1, int param1Int2);
  }
}
