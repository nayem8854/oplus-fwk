package android.media;

import android.bluetooth.BluetoothDevice;
import android.media.audiopolicy.AudioPolicyConfig;
import android.media.audiopolicy.AudioProductStrategy;
import android.media.audiopolicy.AudioVolumeGroup;
import android.media.audiopolicy.IAudioPolicyCallback;
import android.media.projection.IMediaProjection;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.KeyEvent;
import java.util.List;

public interface IAudioService extends IInterface {
  int abandonAudioFocus(IAudioFocusDispatcher paramIAudioFocusDispatcher, String paramString1, AudioAttributes paramAudioAttributes, String paramString2) throws RemoteException;
  
  int addMixForPolicy(AudioPolicyConfig paramAudioPolicyConfig, IAudioPolicyCallback paramIAudioPolicyCallback) throws RemoteException;
  
  void adjustStreamVolume(int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException;
  
  void adjustSuggestedStreamVolume(int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2) throws RemoteException;
  
  void avrcpSupportsAbsoluteVolume(String paramString, boolean paramBoolean) throws RemoteException;
  
  void disableRingtoneSync(int paramInt) throws RemoteException;
  
  void disableSafeMediaVolume(String paramString) throws RemoteException;
  
  int dispatchFocusChange(AudioFocusInfo paramAudioFocusInfo, int paramInt, IAudioPolicyCallback paramIAudioPolicyCallback) throws RemoteException;
  
  void forceRemoteSubmixFullVolume(boolean paramBoolean, IBinder paramIBinder) throws RemoteException;
  
  void forceVolumeControlStream(int paramInt, IBinder paramIBinder) throws RemoteException;
  
  List<AudioPlaybackConfiguration> getActivePlaybackConfigurations() throws RemoteException;
  
  List<AudioRecordingConfiguration> getActiveRecordingConfigurations() throws RemoteException;
  
  int getAllowedCapturePolicy() throws RemoteException;
  
  List<AudioProductStrategy> getAudioProductStrategies() throws RemoteException;
  
  List<AudioVolumeGroup> getAudioVolumeGroups() throws RemoteException;
  
  int getCurrentAudioFocus() throws RemoteException;
  
  int getDeviceVolumeBehavior(AudioDeviceAttributes paramAudioDeviceAttributes) throws RemoteException;
  
  List<AudioDeviceAttributes> getDevicesForAttributes(AudioAttributes paramAudioAttributes) throws RemoteException;
  
  int getFocusRampTimeMs(int paramInt, AudioAttributes paramAudioAttributes) throws RemoteException;
  
  int getLastAudibleStreamVolume(int paramInt) throws RemoteException;
  
  int getMaxVolumeIndexForAttributes(AudioAttributes paramAudioAttributes) throws RemoteException;
  
  int getMinVolumeIndexForAttributes(AudioAttributes paramAudioAttributes) throws RemoteException;
  
  int getMode() throws RemoteException;
  
  String getParameters(String paramString) throws RemoteException;
  
  AudioDeviceAttributes getPreferredDeviceForStrategy(int paramInt) throws RemoteException;
  
  int getRingerModeExternal() throws RemoteException;
  
  int getRingerModeInternal() throws RemoteException;
  
  IRingtonePlayer getRingtonePlayer() throws RemoteException;
  
  String getScoClientInfo() throws RemoteException;
  
  int getStreamMaxVolume(int paramInt) throws RemoteException;
  
  int getStreamMinVolume(int paramInt) throws RemoteException;
  
  int getStreamVolume(int paramInt) throws RemoteException;
  
  int[] getSupportedSystemUsages() throws RemoteException;
  
  int getUiSoundsStreamType() throws RemoteException;
  
  int getVibrateSetting(int paramInt) throws RemoteException;
  
  int getVolumeIndexForAttributes(AudioAttributes paramAudioAttributes) throws RemoteException;
  
  void handleBluetoothA2dpActiveDeviceChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) throws RemoteException;
  
  void handleBluetoothA2dpDeviceConfigChange(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  void handleVolumeKey(KeyEvent paramKeyEvent, boolean paramBoolean, String paramString1, String paramString2) throws RemoteException;
  
  boolean hasHapticChannels(Uri paramUri) throws RemoteException;
  
  boolean hasRegisteredDynamicPolicy() throws RemoteException;
  
  boolean isAudioServerRunning() throws RemoteException;
  
  boolean isBluetoothA2dpOn() throws RemoteException;
  
  boolean isBluetoothScoAvailableOffCall() throws RemoteException;
  
  boolean isBluetoothScoOn() throws RemoteException;
  
  boolean isCallScreeningModeSupported() throws RemoteException;
  
  boolean isCameraSoundForced() throws RemoteException;
  
  boolean isHdmiSystemAudioSupported() throws RemoteException;
  
  boolean isMasterMute() throws RemoteException;
  
  boolean isMicrophoneMuted() throws RemoteException;
  
  boolean isSpeakerphoneOn() throws RemoteException;
  
  boolean isStreamAffectedByMute(int paramInt) throws RemoteException;
  
  boolean isStreamAffectedByRingerMode(int paramInt) throws RemoteException;
  
  boolean isStreamMute(int paramInt) throws RemoteException;
  
  boolean isValidRingerMode(int paramInt) throws RemoteException;
  
  boolean loadSoundEffects() throws RemoteException;
  
  void notifyVolumeControllerVisible(IVolumeController paramIVolumeController, boolean paramBoolean) throws RemoteException;
  
  int oppoGetMode() throws RemoteException;
  
  void playSoundEffect(int paramInt) throws RemoteException;
  
  void playSoundEffectVolume(int paramInt, float paramFloat) throws RemoteException;
  
  void playerAttributes(int paramInt, AudioAttributes paramAudioAttributes) throws RemoteException;
  
  void playerEvent(int paramInt1, int paramInt2) throws RemoteException;
  
  void playerHasOpPlayAudio(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void recorderEvent(int paramInt1, int paramInt2) throws RemoteException;
  
  String registerAudioPolicy(AudioPolicyConfig paramAudioPolicyConfig, IAudioPolicyCallback paramIAudioPolicyCallback, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, IMediaProjection paramIMediaProjection) throws RemoteException;
  
  void registerAudioServerStateDispatcher(IAudioServerStateDispatcher paramIAudioServerStateDispatcher) throws RemoteException;
  
  void registerPlaybackCallback(IPlaybackConfigDispatcher paramIPlaybackConfigDispatcher) throws RemoteException;
  
  void registerRecordingCallback(IRecordingConfigDispatcher paramIRecordingConfigDispatcher) throws RemoteException;
  
  void registerStrategyPreferredDeviceDispatcher(IStrategyPreferredDeviceDispatcher paramIStrategyPreferredDeviceDispatcher) throws RemoteException;
  
  void releasePlayer(int paramInt) throws RemoteException;
  
  void releaseRecorder(int paramInt) throws RemoteException;
  
  void reloadAudioSettings() throws RemoteException;
  
  int removeMixForPolicy(AudioPolicyConfig paramAudioPolicyConfig, IAudioPolicyCallback paramIAudioPolicyCallback) throws RemoteException;
  
  void removeMode(int paramInt, String paramString) throws RemoteException;
  
  int removePreferredDeviceForStrategy(int paramInt) throws RemoteException;
  
  int removeUidDeviceAffinity(IAudioPolicyCallback paramIAudioPolicyCallback, int paramInt) throws RemoteException;
  
  int removeUserIdDeviceAffinity(IAudioPolicyCallback paramIAudioPolicyCallback, int paramInt) throws RemoteException;
  
  int requestAudioFocus(AudioAttributes paramAudioAttributes, int paramInt1, IBinder paramIBinder, IAudioFocusDispatcher paramIAudioFocusDispatcher, String paramString1, String paramString2, int paramInt2, IAudioPolicyCallback paramIAudioPolicyCallback, int paramInt3) throws RemoteException;
  
  int setAllowedCapturePolicy(int paramInt) throws RemoteException;
  
  void setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) throws RemoteException;
  
  void setBluetoothA2dpOn(boolean paramBoolean) throws RemoteException;
  
  void setBluetoothHearingAidDeviceConnectionState(BluetoothDevice paramBluetoothDevice, int paramInt1, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  void setBluetoothScoOn(boolean paramBoolean) throws RemoteException;
  
  void setDeviceVolumeBehavior(AudioDeviceAttributes paramAudioDeviceAttributes, int paramInt, String paramString) throws RemoteException;
  
  int setFocusPropertiesForPolicy(int paramInt, IAudioPolicyCallback paramIAudioPolicyCallback) throws RemoteException;
  
  void setFocusRequestResultFromExtPolicy(AudioFocusInfo paramAudioFocusInfo, int paramInt, IAudioPolicyCallback paramIAudioPolicyCallback) throws RemoteException;
  
  int setHdmiSystemAudioSupported(boolean paramBoolean) throws RemoteException;
  
  void setMasterMute(boolean paramBoolean, int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void setMicrophoneMute(boolean paramBoolean, String paramString, int paramInt) throws RemoteException;
  
  void setMicrophoneMuteFromSwitch(boolean paramBoolean) throws RemoteException;
  
  void setMode(int paramInt, IBinder paramIBinder, String paramString) throws RemoteException;
  
  void setMultiAudioFocusEnabled(boolean paramBoolean) throws RemoteException;
  
  void setParameters(String paramString) throws RemoteException;
  
  int setPreferredDeviceForStrategy(int paramInt, AudioDeviceAttributes paramAudioDeviceAttributes) throws RemoteException;
  
  void setRingerModeExternal(int paramInt, String paramString) throws RemoteException;
  
  void setRingerModeInternal(int paramInt, String paramString) throws RemoteException;
  
  void setRingtonePlayer(IRingtonePlayer paramIRingtonePlayer) throws RemoteException;
  
  void setRttEnabled(boolean paramBoolean) throws RemoteException;
  
  void setSpeakerphoneOn(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setStreamVolume(int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException;
  
  void setSupportedSystemUsages(int[] paramArrayOfint) throws RemoteException;
  
  int setUidDeviceAffinity(IAudioPolicyCallback paramIAudioPolicyCallback, int paramInt, int[] paramArrayOfint, String[] paramArrayOfString) throws RemoteException;
  
  int setUserIdDeviceAffinity(IAudioPolicyCallback paramIAudioPolicyCallback, int paramInt, int[] paramArrayOfint, String[] paramArrayOfString) throws RemoteException;
  
  void setVibrateSetting(int paramInt1, int paramInt2) throws RemoteException;
  
  void setVolumeController(IVolumeController paramIVolumeController) throws RemoteException;
  
  void setVolumeIndexForAttributes(AudioAttributes paramAudioAttributes, int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  void setVolumePolicy(VolumePolicy paramVolumePolicy) throws RemoteException;
  
  void setWiredDeviceConnectionState(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  boolean shouldVibrate(int paramInt) throws RemoteException;
  
  void startBluetoothSco(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void startBluetoothScoVirtualCall(IBinder paramIBinder) throws RemoteException;
  
  AudioRoutesInfo startWatchingRoutes(IAudioRoutesObserver paramIAudioRoutesObserver) throws RemoteException;
  
  void stopBluetoothSco(IBinder paramIBinder) throws RemoteException;
  
  int trackPlayer(PlayerBase.PlayerIdCard paramPlayerIdCard) throws RemoteException;
  
  int trackRecorder(IBinder paramIBinder) throws RemoteException;
  
  void unloadSoundEffects() throws RemoteException;
  
  void unregisterAudioFocusClient(String paramString) throws RemoteException;
  
  void unregisterAudioPolicy(IAudioPolicyCallback paramIAudioPolicyCallback) throws RemoteException;
  
  void unregisterAudioPolicyAsync(IAudioPolicyCallback paramIAudioPolicyCallback) throws RemoteException;
  
  void unregisterAudioServerStateDispatcher(IAudioServerStateDispatcher paramIAudioServerStateDispatcher) throws RemoteException;
  
  void unregisterPlaybackCallback(IPlaybackConfigDispatcher paramIPlaybackConfigDispatcher) throws RemoteException;
  
  void unregisterRecordingCallback(IRecordingConfigDispatcher paramIRecordingConfigDispatcher) throws RemoteException;
  
  void unregisterStrategyPreferredDeviceDispatcher(IStrategyPreferredDeviceDispatcher paramIStrategyPreferredDeviceDispatcher) throws RemoteException;
  
  class Default implements IAudioService {
    public int trackPlayer(PlayerBase.PlayerIdCard param1PlayerIdCard) throws RemoteException {
      return 0;
    }
    
    public void playerAttributes(int param1Int, AudioAttributes param1AudioAttributes) throws RemoteException {}
    
    public void playerEvent(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void releasePlayer(int param1Int) throws RemoteException {}
    
    public int trackRecorder(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public void recorderEvent(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void releaseRecorder(int param1Int) throws RemoteException {}
    
    public void adjustSuggestedStreamVolume(int param1Int1, int param1Int2, int param1Int3, String param1String1, String param1String2) throws RemoteException {}
    
    public void adjustStreamVolume(int param1Int1, int param1Int2, int param1Int3, String param1String) throws RemoteException {}
    
    public void setStreamVolume(int param1Int1, int param1Int2, int param1Int3, String param1String) throws RemoteException {}
    
    public void handleVolumeKey(KeyEvent param1KeyEvent, boolean param1Boolean, String param1String1, String param1String2) throws RemoteException {}
    
    public boolean isStreamMute(int param1Int) throws RemoteException {
      return false;
    }
    
    public void forceRemoteSubmixFullVolume(boolean param1Boolean, IBinder param1IBinder) throws RemoteException {}
    
    public boolean isMasterMute() throws RemoteException {
      return false;
    }
    
    public void setMasterMute(boolean param1Boolean, int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public int getStreamVolume(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getStreamMinVolume(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getStreamMaxVolume(int param1Int) throws RemoteException {
      return 0;
    }
    
    public List<AudioVolumeGroup> getAudioVolumeGroups() throws RemoteException {
      return null;
    }
    
    public void setVolumeIndexForAttributes(AudioAttributes param1AudioAttributes, int param1Int1, int param1Int2, String param1String) throws RemoteException {}
    
    public int getVolumeIndexForAttributes(AudioAttributes param1AudioAttributes) throws RemoteException {
      return 0;
    }
    
    public int getMaxVolumeIndexForAttributes(AudioAttributes param1AudioAttributes) throws RemoteException {
      return 0;
    }
    
    public int getMinVolumeIndexForAttributes(AudioAttributes param1AudioAttributes) throws RemoteException {
      return 0;
    }
    
    public int getLastAudibleStreamVolume(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setSupportedSystemUsages(int[] param1ArrayOfint) throws RemoteException {}
    
    public int[] getSupportedSystemUsages() throws RemoteException {
      return null;
    }
    
    public List<AudioProductStrategy> getAudioProductStrategies() throws RemoteException {
      return null;
    }
    
    public boolean isMicrophoneMuted() throws RemoteException {
      return false;
    }
    
    public void setMicrophoneMute(boolean param1Boolean, String param1String, int param1Int) throws RemoteException {}
    
    public void setMicrophoneMuteFromSwitch(boolean param1Boolean) throws RemoteException {}
    
    public void setRingerModeExternal(int param1Int, String param1String) throws RemoteException {}
    
    public void setRingerModeInternal(int param1Int, String param1String) throws RemoteException {}
    
    public int getRingerModeExternal() throws RemoteException {
      return 0;
    }
    
    public int getRingerModeInternal() throws RemoteException {
      return 0;
    }
    
    public boolean isValidRingerMode(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setVibrateSetting(int param1Int1, int param1Int2) throws RemoteException {}
    
    public int getVibrateSetting(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean shouldVibrate(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setMode(int param1Int, IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public int getMode() throws RemoteException {
      return 0;
    }
    
    public void playSoundEffect(int param1Int) throws RemoteException {}
    
    public void playSoundEffectVolume(int param1Int, float param1Float) throws RemoteException {}
    
    public boolean loadSoundEffects() throws RemoteException {
      return false;
    }
    
    public void unloadSoundEffects() throws RemoteException {}
    
    public void reloadAudioSettings() throws RemoteException {}
    
    public void avrcpSupportsAbsoluteVolume(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void setSpeakerphoneOn(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public boolean isSpeakerphoneOn() throws RemoteException {
      return false;
    }
    
    public void setBluetoothScoOn(boolean param1Boolean) throws RemoteException {}
    
    public boolean isBluetoothScoOn() throws RemoteException {
      return false;
    }
    
    public void setBluetoothA2dpOn(boolean param1Boolean) throws RemoteException {}
    
    public boolean isBluetoothA2dpOn() throws RemoteException {
      return false;
    }
    
    public int requestAudioFocus(AudioAttributes param1AudioAttributes, int param1Int1, IBinder param1IBinder, IAudioFocusDispatcher param1IAudioFocusDispatcher, String param1String1, String param1String2, int param1Int2, IAudioPolicyCallback param1IAudioPolicyCallback, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int abandonAudioFocus(IAudioFocusDispatcher param1IAudioFocusDispatcher, String param1String1, AudioAttributes param1AudioAttributes, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void unregisterAudioFocusClient(String param1String) throws RemoteException {}
    
    public int getCurrentAudioFocus() throws RemoteException {
      return 0;
    }
    
    public void startBluetoothSco(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void startBluetoothScoVirtualCall(IBinder param1IBinder) throws RemoteException {}
    
    public void stopBluetoothSco(IBinder param1IBinder) throws RemoteException {}
    
    public void forceVolumeControlStream(int param1Int, IBinder param1IBinder) throws RemoteException {}
    
    public void setRingtonePlayer(IRingtonePlayer param1IRingtonePlayer) throws RemoteException {}
    
    public IRingtonePlayer getRingtonePlayer() throws RemoteException {
      return null;
    }
    
    public int getUiSoundsStreamType() throws RemoteException {
      return 0;
    }
    
    public void setWiredDeviceConnectionState(int param1Int1, int param1Int2, String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void handleBluetoothA2dpDeviceConfigChange(BluetoothDevice param1BluetoothDevice) throws RemoteException {}
    
    public void handleBluetoothA2dpActiveDeviceChange(BluetoothDevice param1BluetoothDevice, int param1Int1, int param1Int2, boolean param1Boolean, int param1Int3) throws RemoteException {}
    
    public AudioRoutesInfo startWatchingRoutes(IAudioRoutesObserver param1IAudioRoutesObserver) throws RemoteException {
      return null;
    }
    
    public boolean isCameraSoundForced() throws RemoteException {
      return false;
    }
    
    public void setVolumeController(IVolumeController param1IVolumeController) throws RemoteException {}
    
    public void notifyVolumeControllerVisible(IVolumeController param1IVolumeController, boolean param1Boolean) throws RemoteException {}
    
    public boolean isStreamAffectedByRingerMode(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isStreamAffectedByMute(int param1Int) throws RemoteException {
      return false;
    }
    
    public void disableSafeMediaVolume(String param1String) throws RemoteException {}
    
    public int setHdmiSystemAudioSupported(boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public boolean isHdmiSystemAudioSupported() throws RemoteException {
      return false;
    }
    
    public String registerAudioPolicy(AudioPolicyConfig param1AudioPolicyConfig, IAudioPolicyCallback param1IAudioPolicyCallback, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, IMediaProjection param1IMediaProjection) throws RemoteException {
      return null;
    }
    
    public void unregisterAudioPolicyAsync(IAudioPolicyCallback param1IAudioPolicyCallback) throws RemoteException {}
    
    public void unregisterAudioPolicy(IAudioPolicyCallback param1IAudioPolicyCallback) throws RemoteException {}
    
    public int addMixForPolicy(AudioPolicyConfig param1AudioPolicyConfig, IAudioPolicyCallback param1IAudioPolicyCallback) throws RemoteException {
      return 0;
    }
    
    public int removeMixForPolicy(AudioPolicyConfig param1AudioPolicyConfig, IAudioPolicyCallback param1IAudioPolicyCallback) throws RemoteException {
      return 0;
    }
    
    public int setFocusPropertiesForPolicy(int param1Int, IAudioPolicyCallback param1IAudioPolicyCallback) throws RemoteException {
      return 0;
    }
    
    public void setVolumePolicy(VolumePolicy param1VolumePolicy) throws RemoteException {}
    
    public boolean hasRegisteredDynamicPolicy() throws RemoteException {
      return false;
    }
    
    public void registerRecordingCallback(IRecordingConfigDispatcher param1IRecordingConfigDispatcher) throws RemoteException {}
    
    public void unregisterRecordingCallback(IRecordingConfigDispatcher param1IRecordingConfigDispatcher) throws RemoteException {}
    
    public List<AudioRecordingConfiguration> getActiveRecordingConfigurations() throws RemoteException {
      return null;
    }
    
    public void registerPlaybackCallback(IPlaybackConfigDispatcher param1IPlaybackConfigDispatcher) throws RemoteException {}
    
    public void unregisterPlaybackCallback(IPlaybackConfigDispatcher param1IPlaybackConfigDispatcher) throws RemoteException {}
    
    public List<AudioPlaybackConfiguration> getActivePlaybackConfigurations() throws RemoteException {
      return null;
    }
    
    public void disableRingtoneSync(int param1Int) throws RemoteException {}
    
    public int getFocusRampTimeMs(int param1Int, AudioAttributes param1AudioAttributes) throws RemoteException {
      return 0;
    }
    
    public int dispatchFocusChange(AudioFocusInfo param1AudioFocusInfo, int param1Int, IAudioPolicyCallback param1IAudioPolicyCallback) throws RemoteException {
      return 0;
    }
    
    public void playerHasOpPlayAudio(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setBluetoothHearingAidDeviceConnectionState(BluetoothDevice param1BluetoothDevice, int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {}
    
    public void setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent(BluetoothDevice param1BluetoothDevice, int param1Int1, int param1Int2, boolean param1Boolean, int param1Int3) throws RemoteException {}
    
    public void setFocusRequestResultFromExtPolicy(AudioFocusInfo param1AudioFocusInfo, int param1Int, IAudioPolicyCallback param1IAudioPolicyCallback) throws RemoteException {}
    
    public void registerAudioServerStateDispatcher(IAudioServerStateDispatcher param1IAudioServerStateDispatcher) throws RemoteException {}
    
    public void unregisterAudioServerStateDispatcher(IAudioServerStateDispatcher param1IAudioServerStateDispatcher) throws RemoteException {}
    
    public boolean isAudioServerRunning() throws RemoteException {
      return false;
    }
    
    public int setUidDeviceAffinity(IAudioPolicyCallback param1IAudioPolicyCallback, int param1Int, int[] param1ArrayOfint, String[] param1ArrayOfString) throws RemoteException {
      return 0;
    }
    
    public int removeUidDeviceAffinity(IAudioPolicyCallback param1IAudioPolicyCallback, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int setUserIdDeviceAffinity(IAudioPolicyCallback param1IAudioPolicyCallback, int param1Int, int[] param1ArrayOfint, String[] param1ArrayOfString) throws RemoteException {
      return 0;
    }
    
    public int removeUserIdDeviceAffinity(IAudioPolicyCallback param1IAudioPolicyCallback, int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean hasHapticChannels(Uri param1Uri) throws RemoteException {
      return false;
    }
    
    public boolean isCallScreeningModeSupported() throws RemoteException {
      return false;
    }
    
    public int setPreferredDeviceForStrategy(int param1Int, AudioDeviceAttributes param1AudioDeviceAttributes) throws RemoteException {
      return 0;
    }
    
    public int removePreferredDeviceForStrategy(int param1Int) throws RemoteException {
      return 0;
    }
    
    public AudioDeviceAttributes getPreferredDeviceForStrategy(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<AudioDeviceAttributes> getDevicesForAttributes(AudioAttributes param1AudioAttributes) throws RemoteException {
      return null;
    }
    
    public int setAllowedCapturePolicy(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getAllowedCapturePolicy() throws RemoteException {
      return 0;
    }
    
    public void registerStrategyPreferredDeviceDispatcher(IStrategyPreferredDeviceDispatcher param1IStrategyPreferredDeviceDispatcher) throws RemoteException {}
    
    public void unregisterStrategyPreferredDeviceDispatcher(IStrategyPreferredDeviceDispatcher param1IStrategyPreferredDeviceDispatcher) throws RemoteException {}
    
    public void setRttEnabled(boolean param1Boolean) throws RemoteException {}
    
    public void setDeviceVolumeBehavior(AudioDeviceAttributes param1AudioDeviceAttributes, int param1Int, String param1String) throws RemoteException {}
    
    public int getDeviceVolumeBehavior(AudioDeviceAttributes param1AudioDeviceAttributes) throws RemoteException {
      return 0;
    }
    
    public void setParameters(String param1String) throws RemoteException {}
    
    public String getParameters(String param1String) throws RemoteException {
      return null;
    }
    
    public int oppoGetMode() throws RemoteException {
      return 0;
    }
    
    public void removeMode(int param1Int, String param1String) throws RemoteException {}
    
    public String getScoClientInfo() throws RemoteException {
      return null;
    }
    
    public void setMultiAudioFocusEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isBluetoothScoAvailableOffCall() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAudioService {
    private static final String DESCRIPTOR = "android.media.IAudioService";
    
    static final int TRANSACTION_abandonAudioFocus = 54;
    
    static final int TRANSACTION_addMixForPolicy = 79;
    
    static final int TRANSACTION_adjustStreamVolume = 9;
    
    static final int TRANSACTION_adjustSuggestedStreamVolume = 8;
    
    static final int TRANSACTION_avrcpSupportsAbsoluteVolume = 46;
    
    static final int TRANSACTION_disableRingtoneSync = 90;
    
    static final int TRANSACTION_disableSafeMediaVolume = 73;
    
    static final int TRANSACTION_dispatchFocusChange = 92;
    
    static final int TRANSACTION_forceRemoteSubmixFullVolume = 13;
    
    static final int TRANSACTION_forceVolumeControlStream = 60;
    
    static final int TRANSACTION_getActivePlaybackConfigurations = 89;
    
    static final int TRANSACTION_getActiveRecordingConfigurations = 86;
    
    static final int TRANSACTION_getAllowedCapturePolicy = 111;
    
    static final int TRANSACTION_getAudioProductStrategies = 27;
    
    static final int TRANSACTION_getAudioVolumeGroups = 19;
    
    static final int TRANSACTION_getCurrentAudioFocus = 56;
    
    static final int TRANSACTION_getDeviceVolumeBehavior = 116;
    
    static final int TRANSACTION_getDevicesForAttributes = 109;
    
    static final int TRANSACTION_getFocusRampTimeMs = 91;
    
    static final int TRANSACTION_getLastAudibleStreamVolume = 24;
    
    static final int TRANSACTION_getMaxVolumeIndexForAttributes = 22;
    
    static final int TRANSACTION_getMinVolumeIndexForAttributes = 23;
    
    static final int TRANSACTION_getMode = 40;
    
    static final int TRANSACTION_getParameters = 118;
    
    static final int TRANSACTION_getPreferredDeviceForStrategy = 108;
    
    static final int TRANSACTION_getRingerModeExternal = 33;
    
    static final int TRANSACTION_getRingerModeInternal = 34;
    
    static final int TRANSACTION_getRingtonePlayer = 62;
    
    static final int TRANSACTION_getScoClientInfo = 121;
    
    static final int TRANSACTION_getStreamMaxVolume = 18;
    
    static final int TRANSACTION_getStreamMinVolume = 17;
    
    static final int TRANSACTION_getStreamVolume = 16;
    
    static final int TRANSACTION_getSupportedSystemUsages = 26;
    
    static final int TRANSACTION_getUiSoundsStreamType = 63;
    
    static final int TRANSACTION_getVibrateSetting = 37;
    
    static final int TRANSACTION_getVolumeIndexForAttributes = 21;
    
    static final int TRANSACTION_handleBluetoothA2dpActiveDeviceChange = 66;
    
    static final int TRANSACTION_handleBluetoothA2dpDeviceConfigChange = 65;
    
    static final int TRANSACTION_handleVolumeKey = 11;
    
    static final int TRANSACTION_hasHapticChannels = 104;
    
    static final int TRANSACTION_hasRegisteredDynamicPolicy = 83;
    
    static final int TRANSACTION_isAudioServerRunning = 99;
    
    static final int TRANSACTION_isBluetoothA2dpOn = 52;
    
    static final int TRANSACTION_isBluetoothScoAvailableOffCall = 123;
    
    static final int TRANSACTION_isBluetoothScoOn = 50;
    
    static final int TRANSACTION_isCallScreeningModeSupported = 105;
    
    static final int TRANSACTION_isCameraSoundForced = 68;
    
    static final int TRANSACTION_isHdmiSystemAudioSupported = 75;
    
    static final int TRANSACTION_isMasterMute = 14;
    
    static final int TRANSACTION_isMicrophoneMuted = 28;
    
    static final int TRANSACTION_isSpeakerphoneOn = 48;
    
    static final int TRANSACTION_isStreamAffectedByMute = 72;
    
    static final int TRANSACTION_isStreamAffectedByRingerMode = 71;
    
    static final int TRANSACTION_isStreamMute = 12;
    
    static final int TRANSACTION_isValidRingerMode = 35;
    
    static final int TRANSACTION_loadSoundEffects = 43;
    
    static final int TRANSACTION_notifyVolumeControllerVisible = 70;
    
    static final int TRANSACTION_oppoGetMode = 119;
    
    static final int TRANSACTION_playSoundEffect = 41;
    
    static final int TRANSACTION_playSoundEffectVolume = 42;
    
    static final int TRANSACTION_playerAttributes = 2;
    
    static final int TRANSACTION_playerEvent = 3;
    
    static final int TRANSACTION_playerHasOpPlayAudio = 93;
    
    static final int TRANSACTION_recorderEvent = 6;
    
    static final int TRANSACTION_registerAudioPolicy = 76;
    
    static final int TRANSACTION_registerAudioServerStateDispatcher = 97;
    
    static final int TRANSACTION_registerPlaybackCallback = 87;
    
    static final int TRANSACTION_registerRecordingCallback = 84;
    
    static final int TRANSACTION_registerStrategyPreferredDeviceDispatcher = 112;
    
    static final int TRANSACTION_releasePlayer = 4;
    
    static final int TRANSACTION_releaseRecorder = 7;
    
    static final int TRANSACTION_reloadAudioSettings = 45;
    
    static final int TRANSACTION_removeMixForPolicy = 80;
    
    static final int TRANSACTION_removeMode = 120;
    
    static final int TRANSACTION_removePreferredDeviceForStrategy = 107;
    
    static final int TRANSACTION_removeUidDeviceAffinity = 101;
    
    static final int TRANSACTION_removeUserIdDeviceAffinity = 103;
    
    static final int TRANSACTION_requestAudioFocus = 53;
    
    static final int TRANSACTION_setAllowedCapturePolicy = 110;
    
    static final int TRANSACTION_setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent = 95;
    
    static final int TRANSACTION_setBluetoothA2dpOn = 51;
    
    static final int TRANSACTION_setBluetoothHearingAidDeviceConnectionState = 94;
    
    static final int TRANSACTION_setBluetoothScoOn = 49;
    
    static final int TRANSACTION_setDeviceVolumeBehavior = 115;
    
    static final int TRANSACTION_setFocusPropertiesForPolicy = 81;
    
    static final int TRANSACTION_setFocusRequestResultFromExtPolicy = 96;
    
    static final int TRANSACTION_setHdmiSystemAudioSupported = 74;
    
    static final int TRANSACTION_setMasterMute = 15;
    
    static final int TRANSACTION_setMicrophoneMute = 29;
    
    static final int TRANSACTION_setMicrophoneMuteFromSwitch = 30;
    
    static final int TRANSACTION_setMode = 39;
    
    static final int TRANSACTION_setMultiAudioFocusEnabled = 122;
    
    static final int TRANSACTION_setParameters = 117;
    
    static final int TRANSACTION_setPreferredDeviceForStrategy = 106;
    
    static final int TRANSACTION_setRingerModeExternal = 31;
    
    static final int TRANSACTION_setRingerModeInternal = 32;
    
    static final int TRANSACTION_setRingtonePlayer = 61;
    
    static final int TRANSACTION_setRttEnabled = 114;
    
    static final int TRANSACTION_setSpeakerphoneOn = 47;
    
    static final int TRANSACTION_setStreamVolume = 10;
    
    static final int TRANSACTION_setSupportedSystemUsages = 25;
    
    static final int TRANSACTION_setUidDeviceAffinity = 100;
    
    static final int TRANSACTION_setUserIdDeviceAffinity = 102;
    
    static final int TRANSACTION_setVibrateSetting = 36;
    
    static final int TRANSACTION_setVolumeController = 69;
    
    static final int TRANSACTION_setVolumeIndexForAttributes = 20;
    
    static final int TRANSACTION_setVolumePolicy = 82;
    
    static final int TRANSACTION_setWiredDeviceConnectionState = 64;
    
    static final int TRANSACTION_shouldVibrate = 38;
    
    static final int TRANSACTION_startBluetoothSco = 57;
    
    static final int TRANSACTION_startBluetoothScoVirtualCall = 58;
    
    static final int TRANSACTION_startWatchingRoutes = 67;
    
    static final int TRANSACTION_stopBluetoothSco = 59;
    
    static final int TRANSACTION_trackPlayer = 1;
    
    static final int TRANSACTION_trackRecorder = 5;
    
    static final int TRANSACTION_unloadSoundEffects = 44;
    
    static final int TRANSACTION_unregisterAudioFocusClient = 55;
    
    static final int TRANSACTION_unregisterAudioPolicy = 78;
    
    static final int TRANSACTION_unregisterAudioPolicyAsync = 77;
    
    static final int TRANSACTION_unregisterAudioServerStateDispatcher = 98;
    
    static final int TRANSACTION_unregisterPlaybackCallback = 88;
    
    static final int TRANSACTION_unregisterRecordingCallback = 85;
    
    static final int TRANSACTION_unregisterStrategyPreferredDeviceDispatcher = 113;
    
    public Stub() {
      attachInterface(this, "android.media.IAudioService");
    }
    
    public static IAudioService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IAudioService");
      if (iInterface != null && iInterface instanceof IAudioService)
        return (IAudioService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 123:
          return "isBluetoothScoAvailableOffCall";
        case 122:
          return "setMultiAudioFocusEnabled";
        case 121:
          return "getScoClientInfo";
        case 120:
          return "removeMode";
        case 119:
          return "oppoGetMode";
        case 118:
          return "getParameters";
        case 117:
          return "setParameters";
        case 116:
          return "getDeviceVolumeBehavior";
        case 115:
          return "setDeviceVolumeBehavior";
        case 114:
          return "setRttEnabled";
        case 113:
          return "unregisterStrategyPreferredDeviceDispatcher";
        case 112:
          return "registerStrategyPreferredDeviceDispatcher";
        case 111:
          return "getAllowedCapturePolicy";
        case 110:
          return "setAllowedCapturePolicy";
        case 109:
          return "getDevicesForAttributes";
        case 108:
          return "getPreferredDeviceForStrategy";
        case 107:
          return "removePreferredDeviceForStrategy";
        case 106:
          return "setPreferredDeviceForStrategy";
        case 105:
          return "isCallScreeningModeSupported";
        case 104:
          return "hasHapticChannels";
        case 103:
          return "removeUserIdDeviceAffinity";
        case 102:
          return "setUserIdDeviceAffinity";
        case 101:
          return "removeUidDeviceAffinity";
        case 100:
          return "setUidDeviceAffinity";
        case 99:
          return "isAudioServerRunning";
        case 98:
          return "unregisterAudioServerStateDispatcher";
        case 97:
          return "registerAudioServerStateDispatcher";
        case 96:
          return "setFocusRequestResultFromExtPolicy";
        case 95:
          return "setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent";
        case 94:
          return "setBluetoothHearingAidDeviceConnectionState";
        case 93:
          return "playerHasOpPlayAudio";
        case 92:
          return "dispatchFocusChange";
        case 91:
          return "getFocusRampTimeMs";
        case 90:
          return "disableRingtoneSync";
        case 89:
          return "getActivePlaybackConfigurations";
        case 88:
          return "unregisterPlaybackCallback";
        case 87:
          return "registerPlaybackCallback";
        case 86:
          return "getActiveRecordingConfigurations";
        case 85:
          return "unregisterRecordingCallback";
        case 84:
          return "registerRecordingCallback";
        case 83:
          return "hasRegisteredDynamicPolicy";
        case 82:
          return "setVolumePolicy";
        case 81:
          return "setFocusPropertiesForPolicy";
        case 80:
          return "removeMixForPolicy";
        case 79:
          return "addMixForPolicy";
        case 78:
          return "unregisterAudioPolicy";
        case 77:
          return "unregisterAudioPolicyAsync";
        case 76:
          return "registerAudioPolicy";
        case 75:
          return "isHdmiSystemAudioSupported";
        case 74:
          return "setHdmiSystemAudioSupported";
        case 73:
          return "disableSafeMediaVolume";
        case 72:
          return "isStreamAffectedByMute";
        case 71:
          return "isStreamAffectedByRingerMode";
        case 70:
          return "notifyVolumeControllerVisible";
        case 69:
          return "setVolumeController";
        case 68:
          return "isCameraSoundForced";
        case 67:
          return "startWatchingRoutes";
        case 66:
          return "handleBluetoothA2dpActiveDeviceChange";
        case 65:
          return "handleBluetoothA2dpDeviceConfigChange";
        case 64:
          return "setWiredDeviceConnectionState";
        case 63:
          return "getUiSoundsStreamType";
        case 62:
          return "getRingtonePlayer";
        case 61:
          return "setRingtonePlayer";
        case 60:
          return "forceVolumeControlStream";
        case 59:
          return "stopBluetoothSco";
        case 58:
          return "startBluetoothScoVirtualCall";
        case 57:
          return "startBluetoothSco";
        case 56:
          return "getCurrentAudioFocus";
        case 55:
          return "unregisterAudioFocusClient";
        case 54:
          return "abandonAudioFocus";
        case 53:
          return "requestAudioFocus";
        case 52:
          return "isBluetoothA2dpOn";
        case 51:
          return "setBluetoothA2dpOn";
        case 50:
          return "isBluetoothScoOn";
        case 49:
          return "setBluetoothScoOn";
        case 48:
          return "isSpeakerphoneOn";
        case 47:
          return "setSpeakerphoneOn";
        case 46:
          return "avrcpSupportsAbsoluteVolume";
        case 45:
          return "reloadAudioSettings";
        case 44:
          return "unloadSoundEffects";
        case 43:
          return "loadSoundEffects";
        case 42:
          return "playSoundEffectVolume";
        case 41:
          return "playSoundEffect";
        case 40:
          return "getMode";
        case 39:
          return "setMode";
        case 38:
          return "shouldVibrate";
        case 37:
          return "getVibrateSetting";
        case 36:
          return "setVibrateSetting";
        case 35:
          return "isValidRingerMode";
        case 34:
          return "getRingerModeInternal";
        case 33:
          return "getRingerModeExternal";
        case 32:
          return "setRingerModeInternal";
        case 31:
          return "setRingerModeExternal";
        case 30:
          return "setMicrophoneMuteFromSwitch";
        case 29:
          return "setMicrophoneMute";
        case 28:
          return "isMicrophoneMuted";
        case 27:
          return "getAudioProductStrategies";
        case 26:
          return "getSupportedSystemUsages";
        case 25:
          return "setSupportedSystemUsages";
        case 24:
          return "getLastAudibleStreamVolume";
        case 23:
          return "getMinVolumeIndexForAttributes";
        case 22:
          return "getMaxVolumeIndexForAttributes";
        case 21:
          return "getVolumeIndexForAttributes";
        case 20:
          return "setVolumeIndexForAttributes";
        case 19:
          return "getAudioVolumeGroups";
        case 18:
          return "getStreamMaxVolume";
        case 17:
          return "getStreamMinVolume";
        case 16:
          return "getStreamVolume";
        case 15:
          return "setMasterMute";
        case 14:
          return "isMasterMute";
        case 13:
          return "forceRemoteSubmixFullVolume";
        case 12:
          return "isStreamMute";
        case 11:
          return "handleVolumeKey";
        case 10:
          return "setStreamVolume";
        case 9:
          return "adjustStreamVolume";
        case 8:
          return "adjustSuggestedStreamVolume";
        case 7:
          return "releaseRecorder";
        case 6:
          return "recorderEvent";
        case 5:
          return "trackRecorder";
        case 4:
          return "releasePlayer";
        case 3:
          return "playerEvent";
        case 2:
          return "playerAttributes";
        case 1:
          break;
      } 
      return "trackPlayer";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str6;
        IStrategyPreferredDeviceDispatcher iStrategyPreferredDeviceDispatcher;
        List<AudioDeviceAttributes> list4;
        AudioDeviceAttributes audioDeviceAttributes1;
        String[] arrayOfString;
        IAudioServerStateDispatcher iAudioServerStateDispatcher;
        IAudioPolicyCallback iAudioPolicyCallback2;
        List<AudioPlaybackConfiguration> list3;
        IPlaybackConfigDispatcher iPlaybackConfigDispatcher;
        List<AudioRecordingConfiguration> list2;
        IRecordingConfigDispatcher iRecordingConfigDispatcher;
        IAudioPolicyCallback iAudioPolicyCallback1;
        IMediaProjection iMediaProjection;
        String str5;
        IVolumeController iVolumeController1;
        IAudioRoutesObserver iAudioRoutesObserver;
        AudioRoutesInfo audioRoutesInfo;
        String str4;
        IRingtonePlayer iRingtonePlayer;
        IBinder iBinder3;
        String str3;
        List<AudioProductStrategy> list1;
        int[] arrayOfInt1;
        String str2;
        List<AudioVolumeGroup> list;
        IBinder iBinder2;
        String str1;
        IBinder iBinder1;
        AudioDeviceAttributes audioDeviceAttributes2;
        IAudioPolicyCallback iAudioPolicyCallback3;
        int[] arrayOfInt2;
        IVolumeController iVolumeController2;
        String str8;
        IBinder iBinder4;
        String str7;
        int[] arrayOfInt3;
        IAudioPolicyCallback iAudioPolicyCallback5;
        String str9;
        IAudioFocusDispatcher iAudioFocusDispatcher1;
        IAudioPolicyCallback iAudioPolicyCallback4;
        int i9;
        String str10;
        IBinder iBinder5;
        IAudioFocusDispatcher iAudioFocusDispatcher2;
        String str11;
        float f;
        boolean bool14 = false, bool15 = false, bool16 = false, bool17 = false, bool18 = false, bool19 = false, bool20 = false, bool21 = false, bool22 = false, bool23 = false, bool24 = false, bool25 = false, bool26 = false, bool27 = false, bool28 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 123:
            param1Parcel1.enforceInterface("android.media.IAudioService");
            bool13 = isBluetoothScoAvailableOffCall();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 122:
            param1Parcel1.enforceInterface("android.media.IAudioService");
            bool14 = bool28;
            if (param1Parcel1.readInt() != 0)
              bool14 = true; 
            setMultiAudioFocusEnabled(bool14);
            return true;
          case 121:
            param1Parcel1.enforceInterface("android.media.IAudioService");
            str6 = getScoClientInfo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str6);
            return true;
          case 120:
            str6.enforceInterface("android.media.IAudioService");
            i8 = str6.readInt();
            str6 = str6.readString();
            removeMode(i8, str6);
            param1Parcel2.writeNoException();
            return true;
          case 119:
            str6.enforceInterface("android.media.IAudioService");
            i8 = oppoGetMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 118:
            str6.enforceInterface("android.media.IAudioService");
            str6 = str6.readString();
            str6 = getParameters(str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str6);
            return true;
          case 117:
            str6.enforceInterface("android.media.IAudioService");
            str6 = str6.readString();
            setParameters(str6);
            param1Parcel2.writeNoException();
            return true;
          case 116:
            str6.enforceInterface("android.media.IAudioService");
            if (str6.readInt() != 0) {
              AudioDeviceAttributes audioDeviceAttributes = AudioDeviceAttributes.CREATOR.createFromParcel((Parcel)str6);
            } else {
              str6 = null;
            } 
            i8 = getDeviceVolumeBehavior((AudioDeviceAttributes)str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 115:
            str6.enforceInterface("android.media.IAudioService");
            if (str6.readInt() != 0) {
              audioDeviceAttributes2 = AudioDeviceAttributes.CREATOR.createFromParcel((Parcel)str6);
            } else {
              audioDeviceAttributes2 = null;
            } 
            i8 = str6.readInt();
            str6 = str6.readString();
            setDeviceVolumeBehavior(audioDeviceAttributes2, i8, str6);
            param1Parcel2.writeNoException();
            return true;
          case 114:
            str6.enforceInterface("android.media.IAudioService");
            if (str6.readInt() != 0)
              bool14 = true; 
            setRttEnabled(bool14);
            return true;
          case 113:
            str6.enforceInterface("android.media.IAudioService");
            iStrategyPreferredDeviceDispatcher = IStrategyPreferredDeviceDispatcher.Stub.asInterface(str6.readStrongBinder());
            unregisterStrategyPreferredDeviceDispatcher(iStrategyPreferredDeviceDispatcher);
            return true;
          case 112:
            iStrategyPreferredDeviceDispatcher.enforceInterface("android.media.IAudioService");
            iStrategyPreferredDeviceDispatcher = IStrategyPreferredDeviceDispatcher.Stub.asInterface(iStrategyPreferredDeviceDispatcher.readStrongBinder());
            registerStrategyPreferredDeviceDispatcher(iStrategyPreferredDeviceDispatcher);
            param1Parcel2.writeNoException();
            return true;
          case 111:
            iStrategyPreferredDeviceDispatcher.enforceInterface("android.media.IAudioService");
            i8 = getAllowedCapturePolicy();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 110:
            iStrategyPreferredDeviceDispatcher.enforceInterface("android.media.IAudioService");
            i8 = iStrategyPreferredDeviceDispatcher.readInt();
            i8 = setAllowedCapturePolicy(i8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 109:
            iStrategyPreferredDeviceDispatcher.enforceInterface("android.media.IAudioService");
            if (iStrategyPreferredDeviceDispatcher.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)iStrategyPreferredDeviceDispatcher);
            } else {
              iStrategyPreferredDeviceDispatcher = null;
            } 
            list4 = getDevicesForAttributes((AudioAttributes)iStrategyPreferredDeviceDispatcher);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list4);
            return true;
          case 108:
            list4.enforceInterface("android.media.IAudioService");
            i8 = list4.readInt();
            audioDeviceAttributes1 = getPreferredDeviceForStrategy(i8);
            param1Parcel2.writeNoException();
            if (audioDeviceAttributes1 != null) {
              param1Parcel2.writeInt(1);
              audioDeviceAttributes1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 107:
            audioDeviceAttributes1.enforceInterface("android.media.IAudioService");
            i8 = audioDeviceAttributes1.readInt();
            i8 = removePreferredDeviceForStrategy(i8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 106:
            audioDeviceAttributes1.enforceInterface("android.media.IAudioService");
            i8 = audioDeviceAttributes1.readInt();
            if (audioDeviceAttributes1.readInt() != 0) {
              audioDeviceAttributes1 = AudioDeviceAttributes.CREATOR.createFromParcel((Parcel)audioDeviceAttributes1);
            } else {
              audioDeviceAttributes1 = null;
            } 
            i8 = setPreferredDeviceForStrategy(i8, audioDeviceAttributes1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 105:
            audioDeviceAttributes1.enforceInterface("android.media.IAudioService");
            bool12 = isCallScreeningModeSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 104:
            audioDeviceAttributes1.enforceInterface("android.media.IAudioService");
            if (audioDeviceAttributes1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel((Parcel)audioDeviceAttributes1);
            } else {
              audioDeviceAttributes1 = null;
            } 
            bool12 = hasHapticChannels((Uri)audioDeviceAttributes1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 103:
            audioDeviceAttributes1.enforceInterface("android.media.IAudioService");
            iAudioPolicyCallback3 = IAudioPolicyCallback.Stub.asInterface(audioDeviceAttributes1.readStrongBinder());
            i7 = audioDeviceAttributes1.readInt();
            i7 = removeUserIdDeviceAffinity(iAudioPolicyCallback3, i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 102:
            audioDeviceAttributes1.enforceInterface("android.media.IAudioService");
            iAudioPolicyCallback3 = IAudioPolicyCallback.Stub.asInterface(audioDeviceAttributes1.readStrongBinder());
            i7 = audioDeviceAttributes1.readInt();
            arrayOfInt3 = audioDeviceAttributes1.createIntArray();
            arrayOfString = audioDeviceAttributes1.createStringArray();
            i7 = setUserIdDeviceAffinity(iAudioPolicyCallback3, i7, arrayOfInt3, arrayOfString);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 101:
            arrayOfString.enforceInterface("android.media.IAudioService");
            iAudioPolicyCallback3 = IAudioPolicyCallback.Stub.asInterface(arrayOfString.readStrongBinder());
            i7 = arrayOfString.readInt();
            i7 = removeUidDeviceAffinity(iAudioPolicyCallback3, i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 100:
            arrayOfString.enforceInterface("android.media.IAudioService");
            iAudioPolicyCallback5 = IAudioPolicyCallback.Stub.asInterface(arrayOfString.readStrongBinder());
            i7 = arrayOfString.readInt();
            arrayOfInt2 = arrayOfString.createIntArray();
            arrayOfString = arrayOfString.createStringArray();
            i7 = setUidDeviceAffinity(iAudioPolicyCallback5, i7, arrayOfInt2, arrayOfString);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 99:
            arrayOfString.enforceInterface("android.media.IAudioService");
            bool11 = isAudioServerRunning();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 98:
            arrayOfString.enforceInterface("android.media.IAudioService");
            iAudioServerStateDispatcher = IAudioServerStateDispatcher.Stub.asInterface(arrayOfString.readStrongBinder());
            unregisterAudioServerStateDispatcher(iAudioServerStateDispatcher);
            return true;
          case 97:
            iAudioServerStateDispatcher.enforceInterface("android.media.IAudioService");
            iAudioServerStateDispatcher = IAudioServerStateDispatcher.Stub.asInterface(iAudioServerStateDispatcher.readStrongBinder());
            registerAudioServerStateDispatcher(iAudioServerStateDispatcher);
            param1Parcel2.writeNoException();
            return true;
          case 96:
            iAudioServerStateDispatcher.enforceInterface("android.media.IAudioService");
            if (iAudioServerStateDispatcher.readInt() != 0) {
              AudioFocusInfo audioFocusInfo = AudioFocusInfo.CREATOR.createFromParcel((Parcel)iAudioServerStateDispatcher);
            } else {
              param1Parcel2 = null;
            } 
            i6 = iAudioServerStateDispatcher.readInt();
            iAudioPolicyCallback2 = IAudioPolicyCallback.Stub.asInterface(iAudioServerStateDispatcher.readStrongBinder());
            setFocusRequestResultFromExtPolicy((AudioFocusInfo)param1Parcel2, i6, iAudioPolicyCallback2);
            return true;
          case 95:
            iAudioPolicyCallback2.enforceInterface("android.media.IAudioService");
            if (iAudioPolicyCallback2.readInt() != 0) {
              BluetoothDevice bluetoothDevice = BluetoothDevice.CREATOR.createFromParcel((Parcel)iAudioPolicyCallback2);
            } else {
              arrayOfInt2 = null;
            } 
            i6 = iAudioPolicyCallback2.readInt();
            param1Int2 = iAudioPolicyCallback2.readInt();
            if (iAudioPolicyCallback2.readInt() != 0) {
              bool14 = true;
            } else {
              bool14 = false;
            } 
            i9 = iAudioPolicyCallback2.readInt();
            setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent((BluetoothDevice)arrayOfInt2, i6, param1Int2, bool14, i9);
            param1Parcel2.writeNoException();
            return true;
          case 94:
            iAudioPolicyCallback2.enforceInterface("android.media.IAudioService");
            if (iAudioPolicyCallback2.readInt() != 0) {
              BluetoothDevice bluetoothDevice = BluetoothDevice.CREATOR.createFromParcel((Parcel)iAudioPolicyCallback2);
            } else {
              arrayOfInt2 = null;
            } 
            param1Int2 = iAudioPolicyCallback2.readInt();
            bool14 = bool15;
            if (iAudioPolicyCallback2.readInt() != 0)
              bool14 = true; 
            i6 = iAudioPolicyCallback2.readInt();
            setBluetoothHearingAidDeviceConnectionState((BluetoothDevice)arrayOfInt2, param1Int2, bool14, i6);
            param1Parcel2.writeNoException();
            return true;
          case 93:
            iAudioPolicyCallback2.enforceInterface("android.media.IAudioService");
            i6 = iAudioPolicyCallback2.readInt();
            bool14 = bool16;
            if (iAudioPolicyCallback2.readInt() != 0)
              bool14 = true; 
            playerHasOpPlayAudio(i6, bool14);
            return true;
          case 92:
            iAudioPolicyCallback2.enforceInterface("android.media.IAudioService");
            if (iAudioPolicyCallback2.readInt() != 0) {
              AudioFocusInfo audioFocusInfo = AudioFocusInfo.CREATOR.createFromParcel((Parcel)iAudioPolicyCallback2);
            } else {
              arrayOfInt2 = null;
            } 
            i6 = iAudioPolicyCallback2.readInt();
            iAudioPolicyCallback2 = IAudioPolicyCallback.Stub.asInterface(iAudioPolicyCallback2.readStrongBinder());
            i6 = dispatchFocusChange((AudioFocusInfo)arrayOfInt2, i6, iAudioPolicyCallback2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 91:
            iAudioPolicyCallback2.enforceInterface("android.media.IAudioService");
            i6 = iAudioPolicyCallback2.readInt();
            if (iAudioPolicyCallback2.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)iAudioPolicyCallback2);
            } else {
              iAudioPolicyCallback2 = null;
            } 
            i6 = getFocusRampTimeMs(i6, (AudioAttributes)iAudioPolicyCallback2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 90:
            iAudioPolicyCallback2.enforceInterface("android.media.IAudioService");
            i6 = iAudioPolicyCallback2.readInt();
            disableRingtoneSync(i6);
            param1Parcel2.writeNoException();
            return true;
          case 89:
            iAudioPolicyCallback2.enforceInterface("android.media.IAudioService");
            list3 = getActivePlaybackConfigurations();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list3);
            return true;
          case 88:
            list3.enforceInterface("android.media.IAudioService");
            iPlaybackConfigDispatcher = IPlaybackConfigDispatcher.Stub.asInterface(list3.readStrongBinder());
            unregisterPlaybackCallback(iPlaybackConfigDispatcher);
            return true;
          case 87:
            iPlaybackConfigDispatcher.enforceInterface("android.media.IAudioService");
            iPlaybackConfigDispatcher = IPlaybackConfigDispatcher.Stub.asInterface(iPlaybackConfigDispatcher.readStrongBinder());
            registerPlaybackCallback(iPlaybackConfigDispatcher);
            param1Parcel2.writeNoException();
            return true;
          case 86:
            iPlaybackConfigDispatcher.enforceInterface("android.media.IAudioService");
            list2 = getActiveRecordingConfigurations();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 85:
            list2.enforceInterface("android.media.IAudioService");
            iRecordingConfigDispatcher = IRecordingConfigDispatcher.Stub.asInterface(list2.readStrongBinder());
            unregisterRecordingCallback(iRecordingConfigDispatcher);
            return true;
          case 84:
            iRecordingConfigDispatcher.enforceInterface("android.media.IAudioService");
            iRecordingConfigDispatcher = IRecordingConfigDispatcher.Stub.asInterface(iRecordingConfigDispatcher.readStrongBinder());
            registerRecordingCallback(iRecordingConfigDispatcher);
            param1Parcel2.writeNoException();
            return true;
          case 83:
            iRecordingConfigDispatcher.enforceInterface("android.media.IAudioService");
            bool10 = hasRegisteredDynamicPolicy();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 82:
            iRecordingConfigDispatcher.enforceInterface("android.media.IAudioService");
            if (iRecordingConfigDispatcher.readInt() != 0) {
              VolumePolicy volumePolicy = VolumePolicy.CREATOR.createFromParcel((Parcel)iRecordingConfigDispatcher);
            } else {
              iRecordingConfigDispatcher = null;
            } 
            setVolumePolicy((VolumePolicy)iRecordingConfigDispatcher);
            param1Parcel2.writeNoException();
            return true;
          case 81:
            iRecordingConfigDispatcher.enforceInterface("android.media.IAudioService");
            i5 = iRecordingConfigDispatcher.readInt();
            iAudioPolicyCallback1 = IAudioPolicyCallback.Stub.asInterface(iRecordingConfigDispatcher.readStrongBinder());
            i5 = setFocusPropertiesForPolicy(i5, iAudioPolicyCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 80:
            iAudioPolicyCallback1.enforceInterface("android.media.IAudioService");
            if (iAudioPolicyCallback1.readInt() != 0) {
              AudioPolicyConfig audioPolicyConfig = AudioPolicyConfig.CREATOR.createFromParcel((Parcel)iAudioPolicyCallback1);
            } else {
              arrayOfInt2 = null;
            } 
            iAudioPolicyCallback1 = IAudioPolicyCallback.Stub.asInterface(iAudioPolicyCallback1.readStrongBinder());
            i5 = removeMixForPolicy((AudioPolicyConfig)arrayOfInt2, iAudioPolicyCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 79:
            iAudioPolicyCallback1.enforceInterface("android.media.IAudioService");
            if (iAudioPolicyCallback1.readInt() != 0) {
              AudioPolicyConfig audioPolicyConfig = AudioPolicyConfig.CREATOR.createFromParcel((Parcel)iAudioPolicyCallback1);
            } else {
              arrayOfInt2 = null;
            } 
            iAudioPolicyCallback1 = IAudioPolicyCallback.Stub.asInterface(iAudioPolicyCallback1.readStrongBinder());
            i5 = addMixForPolicy((AudioPolicyConfig)arrayOfInt2, iAudioPolicyCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 78:
            iAudioPolicyCallback1.enforceInterface("android.media.IAudioService");
            iAudioPolicyCallback1 = IAudioPolicyCallback.Stub.asInterface(iAudioPolicyCallback1.readStrongBinder());
            unregisterAudioPolicy(iAudioPolicyCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 77:
            iAudioPolicyCallback1.enforceInterface("android.media.IAudioService");
            iAudioPolicyCallback1 = IAudioPolicyCallback.Stub.asInterface(iAudioPolicyCallback1.readStrongBinder());
            unregisterAudioPolicyAsync(iAudioPolicyCallback1);
            return true;
          case 76:
            iAudioPolicyCallback1.enforceInterface("android.media.IAudioService");
            if (iAudioPolicyCallback1.readInt() != 0) {
              AudioPolicyConfig audioPolicyConfig = AudioPolicyConfig.CREATOR.createFromParcel((Parcel)iAudioPolicyCallback1);
            } else {
              arrayOfInt2 = null;
            } 
            iAudioPolicyCallback5 = IAudioPolicyCallback.Stub.asInterface(iAudioPolicyCallback1.readStrongBinder());
            if (iAudioPolicyCallback1.readInt() != 0) {
              bool14 = true;
            } else {
              bool14 = false;
            } 
            if (iAudioPolicyCallback1.readInt() != 0) {
              bool16 = true;
            } else {
              bool16 = false;
            } 
            if (iAudioPolicyCallback1.readInt() != 0) {
              bool24 = true;
            } else {
              bool24 = false;
            } 
            if (iAudioPolicyCallback1.readInt() != 0) {
              bool20 = true;
            } else {
              bool20 = false;
            } 
            iMediaProjection = IMediaProjection.Stub.asInterface(iAudioPolicyCallback1.readStrongBinder());
            str5 = registerAudioPolicy((AudioPolicyConfig)arrayOfInt2, iAudioPolicyCallback5, bool14, bool16, bool24, bool20, iMediaProjection);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 75:
            str5.enforceInterface("android.media.IAudioService");
            bool9 = isHdmiSystemAudioSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 74:
            str5.enforceInterface("android.media.IAudioService");
            bool14 = bool17;
            if (str5.readInt() != 0)
              bool14 = true; 
            i4 = setHdmiSystemAudioSupported(bool14);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 73:
            str5.enforceInterface("android.media.IAudioService");
            str5 = str5.readString();
            disableSafeMediaVolume(str5);
            param1Parcel2.writeNoException();
            return true;
          case 72:
            str5.enforceInterface("android.media.IAudioService");
            i4 = str5.readInt();
            bool8 = isStreamAffectedByMute(i4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 71:
            str5.enforceInterface("android.media.IAudioService");
            i3 = str5.readInt();
            bool7 = isStreamAffectedByRingerMode(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 70:
            str5.enforceInterface("android.media.IAudioService");
            iVolumeController2 = IVolumeController.Stub.asInterface(str5.readStrongBinder());
            bool14 = bool18;
            if (str5.readInt() != 0)
              bool14 = true; 
            notifyVolumeControllerVisible(iVolumeController2, bool14);
            param1Parcel2.writeNoException();
            return true;
          case 69:
            str5.enforceInterface("android.media.IAudioService");
            iVolumeController1 = IVolumeController.Stub.asInterface(str5.readStrongBinder());
            setVolumeController(iVolumeController1);
            param1Parcel2.writeNoException();
            return true;
          case 68:
            iVolumeController1.enforceInterface("android.media.IAudioService");
            bool7 = isCameraSoundForced();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 67:
            iVolumeController1.enforceInterface("android.media.IAudioService");
            iAudioRoutesObserver = IAudioRoutesObserver.Stub.asInterface(iVolumeController1.readStrongBinder());
            audioRoutesInfo = startWatchingRoutes(iAudioRoutesObserver);
            param1Parcel2.writeNoException();
            if (audioRoutesInfo != null) {
              param1Parcel2.writeInt(1);
              audioRoutesInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 66:
            audioRoutesInfo.enforceInterface("android.media.IAudioService");
            if (audioRoutesInfo.readInt() != 0) {
              BluetoothDevice bluetoothDevice = BluetoothDevice.CREATOR.createFromParcel((Parcel)audioRoutesInfo);
            } else {
              iVolumeController2 = null;
            } 
            i2 = audioRoutesInfo.readInt();
            param1Int2 = audioRoutesInfo.readInt();
            if (audioRoutesInfo.readInt() != 0) {
              bool14 = true;
            } else {
              bool14 = false;
            } 
            i9 = audioRoutesInfo.readInt();
            handleBluetoothA2dpActiveDeviceChange((BluetoothDevice)iVolumeController2, i2, param1Int2, bool14, i9);
            param1Parcel2.writeNoException();
            return true;
          case 65:
            audioRoutesInfo.enforceInterface("android.media.IAudioService");
            if (audioRoutesInfo.readInt() != 0) {
              BluetoothDevice bluetoothDevice = BluetoothDevice.CREATOR.createFromParcel((Parcel)audioRoutesInfo);
            } else {
              audioRoutesInfo = null;
            } 
            handleBluetoothA2dpDeviceConfigChange((BluetoothDevice)audioRoutesInfo);
            param1Parcel2.writeNoException();
            return true;
          case 64:
            audioRoutesInfo.enforceInterface("android.media.IAudioService");
            param1Int2 = audioRoutesInfo.readInt();
            i2 = audioRoutesInfo.readInt();
            str8 = audioRoutesInfo.readString();
            str9 = audioRoutesInfo.readString();
            str4 = audioRoutesInfo.readString();
            setWiredDeviceConnectionState(param1Int2, i2, str8, str9, str4);
            param1Parcel2.writeNoException();
            return true;
          case 63:
            str4.enforceInterface("android.media.IAudioService");
            i2 = getUiSoundsStreamType();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 62:
            str4.enforceInterface("android.media.IAudioService");
            iRingtonePlayer = getRingtonePlayer();
            param1Parcel2.writeNoException();
            if (iRingtonePlayer != null) {
              IBinder iBinder = iRingtonePlayer.asBinder();
            } else {
              iRingtonePlayer = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iRingtonePlayer);
            return true;
          case 61:
            iRingtonePlayer.enforceInterface("android.media.IAudioService");
            iRingtonePlayer = IRingtonePlayer.Stub.asInterface(iRingtonePlayer.readStrongBinder());
            setRingtonePlayer(iRingtonePlayer);
            param1Parcel2.writeNoException();
            return true;
          case 60:
            iRingtonePlayer.enforceInterface("android.media.IAudioService");
            i2 = iRingtonePlayer.readInt();
            iBinder3 = iRingtonePlayer.readStrongBinder();
            forceVolumeControlStream(i2, iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 59:
            iBinder3.enforceInterface("android.media.IAudioService");
            iBinder3 = iBinder3.readStrongBinder();
            stopBluetoothSco(iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 58:
            iBinder3.enforceInterface("android.media.IAudioService");
            iBinder3 = iBinder3.readStrongBinder();
            startBluetoothScoVirtualCall(iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 57:
            iBinder3.enforceInterface("android.media.IAudioService");
            iBinder4 = iBinder3.readStrongBinder();
            i2 = iBinder3.readInt();
            startBluetoothSco(iBinder4, i2);
            param1Parcel2.writeNoException();
            return true;
          case 56:
            iBinder3.enforceInterface("android.media.IAudioService");
            i2 = getCurrentAudioFocus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 55:
            iBinder3.enforceInterface("android.media.IAudioService");
            str3 = iBinder3.readString();
            unregisterAudioFocusClient(str3);
            param1Parcel2.writeNoException();
            return true;
          case 54:
            str3.enforceInterface("android.media.IAudioService");
            iAudioFocusDispatcher1 = IAudioFocusDispatcher.Stub.asInterface(str3.readStrongBinder());
            str10 = str3.readString();
            if (str3.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)str3);
            } else {
              iBinder4 = null;
            } 
            str3 = str3.readString();
            i2 = abandonAudioFocus(iAudioFocusDispatcher1, str10, (AudioAttributes)iBinder4, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 53:
            str3.enforceInterface("android.media.IAudioService");
            if (str3.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)str3);
            } else {
              iBinder4 = null;
            } 
            i9 = str3.readInt();
            iBinder5 = str3.readStrongBinder();
            iAudioFocusDispatcher2 = IAudioFocusDispatcher.Stub.asInterface(str3.readStrongBinder());
            str11 = str3.readString();
            str10 = str3.readString();
            param1Int2 = str3.readInt();
            iAudioPolicyCallback4 = IAudioPolicyCallback.Stub.asInterface(str3.readStrongBinder());
            i2 = str3.readInt();
            i2 = requestAudioFocus((AudioAttributes)iBinder4, i9, iBinder5, iAudioFocusDispatcher2, str11, str10, param1Int2, iAudioPolicyCallback4, i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 52:
            str3.enforceInterface("android.media.IAudioService");
            bool6 = isBluetoothA2dpOn();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 51:
            str3.enforceInterface("android.media.IAudioService");
            bool14 = bool19;
            if (str3.readInt() != 0)
              bool14 = true; 
            setBluetoothA2dpOn(bool14);
            param1Parcel2.writeNoException();
            return true;
          case 50:
            str3.enforceInterface("android.media.IAudioService");
            bool6 = isBluetoothScoOn();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 49:
            str3.enforceInterface("android.media.IAudioService");
            bool14 = bool20;
            if (str3.readInt() != 0)
              bool14 = true; 
            setBluetoothScoOn(bool14);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            str3.enforceInterface("android.media.IAudioService");
            bool6 = isSpeakerphoneOn();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 47:
            str3.enforceInterface("android.media.IAudioService");
            iBinder4 = str3.readStrongBinder();
            bool14 = bool21;
            if (str3.readInt() != 0)
              bool14 = true; 
            setSpeakerphoneOn(iBinder4, bool14);
            param1Parcel2.writeNoException();
            return true;
          case 46:
            str3.enforceInterface("android.media.IAudioService");
            str = str3.readString();
            bool14 = bool22;
            if (str3.readInt() != 0)
              bool14 = true; 
            avrcpSupportsAbsoluteVolume(str, bool14);
            return true;
          case 45:
            str3.enforceInterface("android.media.IAudioService");
            reloadAudioSettings();
            return true;
          case 44:
            str3.enforceInterface("android.media.IAudioService");
            unloadSoundEffects();
            return true;
          case 43:
            str3.enforceInterface("android.media.IAudioService");
            bool6 = loadSoundEffects();
            str.writeNoException();
            str.writeInt(bool6);
            return true;
          case 42:
            str3.enforceInterface("android.media.IAudioService");
            i1 = str3.readInt();
            f = str3.readFloat();
            playSoundEffectVolume(i1, f);
            return true;
          case 41:
            str3.enforceInterface("android.media.IAudioService");
            i1 = str3.readInt();
            playSoundEffect(i1);
            return true;
          case 40:
            str3.enforceInterface("android.media.IAudioService");
            i1 = getMode();
            str.writeNoException();
            str.writeInt(i1);
            return true;
          case 39:
            str3.enforceInterface("android.media.IAudioService");
            i1 = str3.readInt();
            iBinder4 = str3.readStrongBinder();
            str3 = str3.readString();
            setMode(i1, iBinder4, str3);
            str.writeNoException();
            return true;
          case 38:
            str3.enforceInterface("android.media.IAudioService");
            i1 = str3.readInt();
            bool5 = shouldVibrate(i1);
            str.writeNoException();
            str.writeInt(bool5);
            return true;
          case 37:
            str3.enforceInterface("android.media.IAudioService");
            n = str3.readInt();
            n = getVibrateSetting(n);
            str.writeNoException();
            str.writeInt(n);
            return true;
          case 36:
            str3.enforceInterface("android.media.IAudioService");
            n = str3.readInt();
            param1Int2 = str3.readInt();
            setVibrateSetting(n, param1Int2);
            str.writeNoException();
            return true;
          case 35:
            str3.enforceInterface("android.media.IAudioService");
            n = str3.readInt();
            bool4 = isValidRingerMode(n);
            str.writeNoException();
            str.writeInt(bool4);
            return true;
          case 34:
            str3.enforceInterface("android.media.IAudioService");
            m = getRingerModeInternal();
            str.writeNoException();
            str.writeInt(m);
            return true;
          case 33:
            str3.enforceInterface("android.media.IAudioService");
            m = getRingerModeExternal();
            str.writeNoException();
            str.writeInt(m);
            return true;
          case 32:
            str3.enforceInterface("android.media.IAudioService");
            m = str3.readInt();
            str3 = str3.readString();
            setRingerModeInternal(m, str3);
            str.writeNoException();
            return true;
          case 31:
            str3.enforceInterface("android.media.IAudioService");
            m = str3.readInt();
            str3 = str3.readString();
            setRingerModeExternal(m, str3);
            str.writeNoException();
            return true;
          case 30:
            str3.enforceInterface("android.media.IAudioService");
            bool14 = bool23;
            if (str3.readInt() != 0)
              bool14 = true; 
            setMicrophoneMuteFromSwitch(bool14);
            return true;
          case 29:
            str3.enforceInterface("android.media.IAudioService");
            bool14 = bool24;
            if (str3.readInt() != 0)
              bool14 = true; 
            str7 = str3.readString();
            m = str3.readInt();
            setMicrophoneMute(bool14, str7, m);
            str.writeNoException();
            return true;
          case 28:
            str3.enforceInterface("android.media.IAudioService");
            bool3 = isMicrophoneMuted();
            str.writeNoException();
            str.writeInt(bool3);
            return true;
          case 27:
            str3.enforceInterface("android.media.IAudioService");
            list1 = getAudioProductStrategies();
            str.writeNoException();
            str.writeTypedList(list1);
            return true;
          case 26:
            list1.enforceInterface("android.media.IAudioService");
            arrayOfInt1 = getSupportedSystemUsages();
            str.writeNoException();
            str.writeIntArray(arrayOfInt1);
            return true;
          case 25:
            arrayOfInt1.enforceInterface("android.media.IAudioService");
            arrayOfInt1 = arrayOfInt1.createIntArray();
            setSupportedSystemUsages(arrayOfInt1);
            str.writeNoException();
            return true;
          case 24:
            arrayOfInt1.enforceInterface("android.media.IAudioService");
            k = arrayOfInt1.readInt();
            k = getLastAudibleStreamVolume(k);
            str.writeNoException();
            str.writeInt(k);
            return true;
          case 23:
            arrayOfInt1.enforceInterface("android.media.IAudioService");
            if (arrayOfInt1.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)arrayOfInt1);
            } else {
              arrayOfInt1 = null;
            } 
            k = getMinVolumeIndexForAttributes((AudioAttributes)arrayOfInt1);
            str.writeNoException();
            str.writeInt(k);
            return true;
          case 22:
            arrayOfInt1.enforceInterface("android.media.IAudioService");
            if (arrayOfInt1.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)arrayOfInt1);
            } else {
              arrayOfInt1 = null;
            } 
            k = getMaxVolumeIndexForAttributes((AudioAttributes)arrayOfInt1);
            str.writeNoException();
            str.writeInt(k);
            return true;
          case 21:
            arrayOfInt1.enforceInterface("android.media.IAudioService");
            if (arrayOfInt1.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)arrayOfInt1);
            } else {
              arrayOfInt1 = null;
            } 
            k = getVolumeIndexForAttributes((AudioAttributes)arrayOfInt1);
            str.writeNoException();
            str.writeInt(k);
            return true;
          case 20:
            arrayOfInt1.enforceInterface("android.media.IAudioService");
            if (arrayOfInt1.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)arrayOfInt1);
            } else {
              str7 = null;
            } 
            k = arrayOfInt1.readInt();
            param1Int2 = arrayOfInt1.readInt();
            str2 = arrayOfInt1.readString();
            setVolumeIndexForAttributes((AudioAttributes)str7, k, param1Int2, str2);
            str.writeNoException();
            return true;
          case 19:
            str2.enforceInterface("android.media.IAudioService");
            list = getAudioVolumeGroups();
            str.writeNoException();
            str.writeTypedList(list);
            return true;
          case 18:
            list.enforceInterface("android.media.IAudioService");
            k = list.readInt();
            k = getStreamMaxVolume(k);
            str.writeNoException();
            str.writeInt(k);
            return true;
          case 17:
            list.enforceInterface("android.media.IAudioService");
            k = list.readInt();
            k = getStreamMinVolume(k);
            str.writeNoException();
            str.writeInt(k);
            return true;
          case 16:
            list.enforceInterface("android.media.IAudioService");
            k = list.readInt();
            k = getStreamVolume(k);
            str.writeNoException();
            str.writeInt(k);
            return true;
          case 15:
            list.enforceInterface("android.media.IAudioService");
            bool14 = bool25;
            if (list.readInt() != 0)
              bool14 = true; 
            k = list.readInt();
            str7 = list.readString();
            param1Int2 = list.readInt();
            setMasterMute(bool14, k, str7, param1Int2);
            str.writeNoException();
            return true;
          case 14:
            list.enforceInterface("android.media.IAudioService");
            bool2 = isMasterMute();
            str.writeNoException();
            str.writeInt(bool2);
            return true;
          case 13:
            list.enforceInterface("android.media.IAudioService");
            bool14 = bool26;
            if (list.readInt() != 0)
              bool14 = true; 
            iBinder2 = list.readStrongBinder();
            forceRemoteSubmixFullVolume(bool14, iBinder2);
            str.writeNoException();
            return true;
          case 12:
            iBinder2.enforceInterface("android.media.IAudioService");
            j = iBinder2.readInt();
            bool1 = isStreamMute(j);
            str.writeNoException();
            str.writeInt(bool1);
            return true;
          case 11:
            iBinder2.enforceInterface("android.media.IAudioService");
            if (iBinder2.readInt() != 0) {
              KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              str = null;
            } 
            bool14 = bool27;
            if (iBinder2.readInt() != 0)
              bool14 = true; 
            str7 = iBinder2.readString();
            str1 = iBinder2.readString();
            handleVolumeKey((KeyEvent)str, bool14, str7, str1);
            return true;
          case 10:
            str1.enforceInterface("android.media.IAudioService");
            param1Int2 = str1.readInt();
            i9 = str1.readInt();
            i = str1.readInt();
            str1 = str1.readString();
            setStreamVolume(param1Int2, i9, i, str1);
            str.writeNoException();
            return true;
          case 9:
            str1.enforceInterface("android.media.IAudioService");
            i9 = str1.readInt();
            param1Int2 = str1.readInt();
            i = str1.readInt();
            str1 = str1.readString();
            adjustStreamVolume(i9, param1Int2, i, str1);
            str.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("android.media.IAudioService");
            param1Int2 = str1.readInt();
            i9 = str1.readInt();
            i = str1.readInt();
            str = str1.readString();
            str1 = str1.readString();
            adjustSuggestedStreamVolume(param1Int2, i9, i, str, str1);
            return true;
          case 7:
            str1.enforceInterface("android.media.IAudioService");
            i = str1.readInt();
            releaseRecorder(i);
            return true;
          case 6:
            str1.enforceInterface("android.media.IAudioService");
            param1Int2 = str1.readInt();
            i = str1.readInt();
            recorderEvent(param1Int2, i);
            return true;
          case 5:
            str1.enforceInterface("android.media.IAudioService");
            iBinder1 = str1.readStrongBinder();
            i = trackRecorder(iBinder1);
            str.writeNoException();
            str.writeInt(i);
            return true;
          case 4:
            iBinder1.enforceInterface("android.media.IAudioService");
            i = iBinder1.readInt();
            releasePlayer(i);
            return true;
          case 3:
            iBinder1.enforceInterface("android.media.IAudioService");
            param1Int2 = iBinder1.readInt();
            i = iBinder1.readInt();
            playerEvent(param1Int2, i);
            return true;
          case 2:
            iBinder1.enforceInterface("android.media.IAudioService");
            i = iBinder1.readInt();
            if (iBinder1.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder1 = null;
            } 
            playerAttributes(i, (AudioAttributes)iBinder1);
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("android.media.IAudioService");
        if (iBinder1.readInt() != 0) {
          PlayerBase.PlayerIdCard playerIdCard = PlayerBase.PlayerIdCard.CREATOR.createFromParcel((Parcel)iBinder1);
        } else {
          iBinder1 = null;
        } 
        int i = trackPlayer((PlayerBase.PlayerIdCard)iBinder1);
        str.writeNoException();
        str.writeInt(i);
        return true;
      } 
      str.writeString("android.media.IAudioService");
      return true;
    }
    
    private static class Proxy implements IAudioService {
      public static IAudioService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IAudioService";
      }
      
      public int trackPlayer(PlayerBase.PlayerIdCard param2PlayerIdCard) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2PlayerIdCard != null) {
            parcel1.writeInt(1);
            param2PlayerIdCard.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().trackPlayer(param2PlayerIdCard); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void playerAttributes(int param2Int, AudioAttributes param2AudioAttributes) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int);
          if (param2AudioAttributes != null) {
            parcel.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().playerAttributes(param2Int, param2AudioAttributes);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void playerEvent(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().playerEvent(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void releasePlayer(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().releasePlayer(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public int trackRecorder(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().trackRecorder(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void recorderEvent(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().recorderEvent(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void releaseRecorder(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().releaseRecorder(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void adjustSuggestedStreamVolume(int param2Int1, int param2Int2, int param2Int3, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().adjustSuggestedStreamVolume(param2Int1, param2Int2, param2Int3, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void adjustStreamVolume(int param2Int1, int param2Int2, int param2Int3, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().adjustStreamVolume(param2Int1, param2Int2, param2Int3, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setStreamVolume(int param2Int1, int param2Int2, int param2Int3, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setStreamVolume(param2Int1, param2Int2, param2Int3, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void handleVolumeKey(KeyEvent param2KeyEvent, boolean param2Boolean, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          boolean bool = false;
          if (param2KeyEvent != null) {
            parcel.writeInt(1);
            param2KeyEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool1 = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().handleVolumeKey(param2KeyEvent, param2Boolean, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isStreamMute(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isStreamMute(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceRemoteSubmixFullVolume(boolean param2Boolean, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().forceRemoteSubmixFullVolume(param2Boolean, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMasterMute() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isMasterMute();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMasterMute(boolean param2Boolean, int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setMasterMute(param2Boolean, param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getStreamVolume(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().getStreamVolume(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getStreamMinVolume(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().getStreamMinVolume(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getStreamMaxVolume(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().getStreamMaxVolume(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AudioVolumeGroup> getAudioVolumeGroups() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getAudioVolumeGroups(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AudioVolumeGroup.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVolumeIndexForAttributes(AudioAttributes param2AudioAttributes, int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setVolumeIndexForAttributes(param2AudioAttributes, param2Int1, param2Int2, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getVolumeIndexForAttributes(AudioAttributes param2AudioAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getVolumeIndexForAttributes(param2AudioAttributes); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaxVolumeIndexForAttributes(AudioAttributes param2AudioAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getMaxVolumeIndexForAttributes(param2AudioAttributes); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMinVolumeIndexForAttributes(AudioAttributes param2AudioAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getMinVolumeIndexForAttributes(param2AudioAttributes); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLastAudibleStreamVolume(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().getLastAudibleStreamVolume(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSupportedSystemUsages(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setSupportedSystemUsages(param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getSupportedSystemUsages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getSupportedSystemUsages(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AudioProductStrategy> getAudioProductStrategies() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getAudioProductStrategies(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AudioProductStrategy.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMicrophoneMuted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(28, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isMicrophoneMuted();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMicrophoneMute(boolean param2Boolean, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setMicrophoneMute(param2Boolean, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMicrophoneMuteFromSwitch(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel, (Parcel)null, 1);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setMicrophoneMuteFromSwitch(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRingerModeExternal(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setRingerModeExternal(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRingerModeInternal(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setRingerModeInternal(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRingerModeExternal() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getRingerModeExternal(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRingerModeInternal() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getRingerModeInternal(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isValidRingerMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(35, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isValidRingerMode(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVibrateSetting(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setVibrateSetting(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getVibrateSetting(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().getVibrateSetting(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldVibrate(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(38, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().shouldVibrate(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMode(int param2Int, IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setMode(param2Int, param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void playSoundEffect(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(41, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().playSoundEffect(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void playSoundEffectVolume(int param2Int, float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int);
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(42, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().playSoundEffectVolume(param2Int, param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean loadSoundEffects() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(43, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().loadSoundEffects();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unloadSoundEffects() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(44, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().unloadSoundEffects();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reloadAudioSettings() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(45, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().reloadAudioSettings();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void avrcpSupportsAbsoluteVolume(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(46, parcel, (Parcel)null, 1);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().avrcpSupportsAbsoluteVolume(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSpeakerphoneOn(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setSpeakerphoneOn(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSpeakerphoneOn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(48, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isSpeakerphoneOn();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBluetoothScoOn(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setBluetoothScoOn(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBluetoothScoOn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(50, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isBluetoothScoOn();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBluetoothA2dpOn(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setBluetoothA2dpOn(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBluetoothA2dpOn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(52, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isBluetoothA2dpOn();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int requestAudioFocus(AudioAttributes param2AudioAttributes, int param2Int1, IBinder param2IBinder, IAudioFocusDispatcher param2IAudioFocusDispatcher, String param2String1, String param2String2, int param2Int2, IAudioPolicyCallback param2IAudioPolicyCallback, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeStrongBinder(param2IBinder);
              IBinder iBinder1 = null;
              if (param2IAudioFocusDispatcher != null) {
                iBinder2 = param2IAudioFocusDispatcher.asBinder();
              } else {
                iBinder2 = null;
              } 
              parcel1.writeStrongBinder(iBinder2);
              parcel1.writeString(param2String1);
              parcel1.writeString(param2String2);
              parcel1.writeInt(param2Int2);
              IBinder iBinder2 = iBinder1;
              if (param2IAudioPolicyCallback != null)
                iBinder2 = param2IAudioPolicyCallback.asBinder(); 
              parcel1.writeStrongBinder(iBinder2);
              parcel1.writeInt(param2Int3);
              boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
              if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
                param2Int1 = IAudioService.Stub.getDefaultImpl().requestAudioFocus(param2AudioAttributes, param2Int1, param2IBinder, param2IAudioFocusDispatcher, param2String1, param2String2, param2Int2, param2IAudioPolicyCallback, param2Int3);
                parcel2.recycle();
                parcel1.recycle();
                return param2Int1;
              } 
              parcel2.readException();
              param2Int1 = parcel2.readInt();
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2AudioAttributes;
      }
      
      public int abandonAudioFocus(IAudioFocusDispatcher param2IAudioFocusDispatcher, String param2String1, AudioAttributes param2AudioAttributes, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioFocusDispatcher != null) {
            iBinder = param2IAudioFocusDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().abandonAudioFocus(param2IAudioFocusDispatcher, param2String1, param2AudioAttributes, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterAudioFocusClient(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().unregisterAudioFocusClient(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCurrentAudioFocus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getCurrentAudioFocus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startBluetoothSco(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().startBluetoothSco(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startBluetoothScoVirtualCall(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().startBluetoothScoVirtualCall(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopBluetoothSco(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().stopBluetoothSco(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceVolumeControlStream(int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().forceVolumeControlStream(param2Int, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRingtonePlayer(IRingtonePlayer param2IRingtonePlayer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IRingtonePlayer != null) {
            iBinder = param2IRingtonePlayer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setRingtonePlayer(param2IRingtonePlayer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IRingtonePlayer getRingtonePlayer() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getRingtonePlayer(); 
          parcel2.readException();
          return IRingtonePlayer.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUiSoundsStreamType() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getUiSoundsStreamType(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setWiredDeviceConnectionState(int param2Int1, int param2Int2, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setWiredDeviceConnectionState(param2Int1, param2Int2, param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void handleBluetoothA2dpDeviceConfigChange(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().handleBluetoothA2dpDeviceConfigChange(param2BluetoothDevice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void handleBluetoothA2dpActiveDeviceChange(BluetoothDevice param2BluetoothDevice, int param2Int1, int param2Int2, boolean param2Boolean, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int3);
          boolean bool1 = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().handleBluetoothA2dpActiveDeviceChange(param2BluetoothDevice, param2Int1, param2Int2, param2Boolean, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AudioRoutesInfo startWatchingRoutes(IAudioRoutesObserver param2IAudioRoutesObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioRoutesObserver != null) {
            iBinder = param2IAudioRoutesObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().startWatchingRoutes(param2IAudioRoutesObserver); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            AudioRoutesInfo audioRoutesInfo = AudioRoutesInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2IAudioRoutesObserver = null;
          } 
          return (AudioRoutesInfo)param2IAudioRoutesObserver;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCameraSoundForced() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(68, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isCameraSoundForced();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVolumeController(IVolumeController param2IVolumeController) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IVolumeController != null) {
            iBinder = param2IVolumeController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setVolumeController(param2IVolumeController);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyVolumeControllerVisible(IVolumeController param2IVolumeController, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IVolumeController != null) {
            iBinder = param2IVolumeController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().notifyVolumeControllerVisible(param2IVolumeController, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isStreamAffectedByRingerMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(71, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isStreamAffectedByRingerMode(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isStreamAffectedByMute(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(72, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isStreamAffectedByMute(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableSafeMediaVolume(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().disableSafeMediaVolume(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setHdmiSystemAudioSupported(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            i = IAudioService.Stub.getDefaultImpl().setHdmiSystemAudioSupported(param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isHdmiSystemAudioSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(75, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isHdmiSystemAudioSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String registerAudioPolicy(AudioPolicyConfig param2AudioPolicyConfig, IAudioPolicyCallback param2IAudioPolicyCallback, boolean param2Boolean1, boolean param2Boolean2, boolean param2Boolean3, boolean param2Boolean4, IMediaProjection param2IMediaProjection) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool1 = true;
          if (param2AudioPolicyConfig != null) {
            parcel1.writeInt(1);
            param2AudioPolicyConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          IBinder iBinder1 = null;
          if (param2IAudioPolicyCallback != null) {
            iBinder2 = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean3) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean4) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          IBinder iBinder2 = iBinder1;
          if (param2IMediaProjection != null)
            iBinder2 = param2IMediaProjection.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          try {
            boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
            if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
              String str1 = IAudioService.Stub.getDefaultImpl().registerAudioPolicy(param2AudioPolicyConfig, param2IAudioPolicyCallback, param2Boolean1, param2Boolean2, param2Boolean3, param2Boolean4, param2IMediaProjection);
              parcel2.recycle();
              parcel1.recycle();
              return str1;
            } 
            parcel2.readException();
            String str = parcel2.readString();
            parcel2.recycle();
            parcel1.recycle();
            return str;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2AudioPolicyConfig;
      }
      
      public void unregisterAudioPolicyAsync(IAudioPolicyCallback param2IAudioPolicyCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(77, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().unregisterAudioPolicyAsync(param2IAudioPolicyCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregisterAudioPolicy(IAudioPolicyCallback param2IAudioPolicyCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().unregisterAudioPolicy(param2IAudioPolicyCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addMixForPolicy(AudioPolicyConfig param2AudioPolicyConfig, IAudioPolicyCallback param2IAudioPolicyCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioPolicyConfig != null) {
            parcel1.writeInt(1);
            param2AudioPolicyConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().addMixForPolicy(param2AudioPolicyConfig, param2IAudioPolicyCallback); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int removeMixForPolicy(AudioPolicyConfig param2AudioPolicyConfig, IAudioPolicyCallback param2IAudioPolicyCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioPolicyConfig != null) {
            parcel1.writeInt(1);
            param2AudioPolicyConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().removeMixForPolicy(param2AudioPolicyConfig, param2IAudioPolicyCallback); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setFocusPropertiesForPolicy(int param2Int, IAudioPolicyCallback param2IAudioPolicyCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().setFocusPropertiesForPolicy(param2Int, param2IAudioPolicyCallback);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVolumePolicy(VolumePolicy param2VolumePolicy) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2VolumePolicy != null) {
            parcel1.writeInt(1);
            param2VolumePolicy.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setVolumePolicy(param2VolumePolicy);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasRegisteredDynamicPolicy() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(83, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().hasRegisteredDynamicPolicy();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerRecordingCallback(IRecordingConfigDispatcher param2IRecordingConfigDispatcher) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IRecordingConfigDispatcher != null) {
            iBinder = param2IRecordingConfigDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().registerRecordingCallback(param2IRecordingConfigDispatcher);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterRecordingCallback(IRecordingConfigDispatcher param2IRecordingConfigDispatcher) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2IRecordingConfigDispatcher != null) {
            iBinder = param2IRecordingConfigDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(85, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().unregisterRecordingCallback(param2IRecordingConfigDispatcher);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public List<AudioRecordingConfiguration> getActiveRecordingConfigurations() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(86, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getActiveRecordingConfigurations(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AudioRecordingConfiguration.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerPlaybackCallback(IPlaybackConfigDispatcher param2IPlaybackConfigDispatcher) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IPlaybackConfigDispatcher != null) {
            iBinder = param2IPlaybackConfigDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(87, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().registerPlaybackCallback(param2IPlaybackConfigDispatcher);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterPlaybackCallback(IPlaybackConfigDispatcher param2IPlaybackConfigDispatcher) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2IPlaybackConfigDispatcher != null) {
            iBinder = param2IPlaybackConfigDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(88, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().unregisterPlaybackCallback(param2IPlaybackConfigDispatcher);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public List<AudioPlaybackConfiguration> getActivePlaybackConfigurations() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getActivePlaybackConfigurations(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AudioPlaybackConfiguration.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableRingtoneSync(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(90, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().disableRingtoneSync(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFocusRampTimeMs(int param2Int, AudioAttributes param2AudioAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().getFocusRampTimeMs(param2Int, param2AudioAttributes);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int dispatchFocusChange(AudioFocusInfo param2AudioFocusInfo, int param2Int, IAudioPolicyCallback param2IAudioPolicyCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioFocusInfo != null) {
            parcel1.writeInt(1);
            param2AudioFocusInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(92, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().dispatchFocusChange(param2AudioFocusInfo, param2Int, param2IAudioPolicyCallback);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void playerHasOpPlayAudio(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IAudioService");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(93, parcel, (Parcel)null, 1);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().playerHasOpPlayAudio(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setBluetoothHearingAidDeviceConnectionState(BluetoothDevice param2BluetoothDevice, int param2Int1, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(94, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setBluetoothHearingAidDeviceConnectionState(param2BluetoothDevice, param2Int1, param2Boolean, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent(BluetoothDevice param2BluetoothDevice, int param2Int1, int param2Int2, boolean param2Boolean, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int3);
          boolean bool1 = this.mRemote.transact(95, parcel1, parcel2, 0);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setBluetoothA2dpDeviceConnectionStateSuppressNoisyIntent(param2BluetoothDevice, param2Int1, param2Int2, param2Boolean, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFocusRequestResultFromExtPolicy(AudioFocusInfo param2AudioFocusInfo, int param2Int, IAudioPolicyCallback param2IAudioPolicyCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioFocusInfo != null) {
            parcel.writeInt(1);
            param2AudioFocusInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(96, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setFocusRequestResultFromExtPolicy(param2AudioFocusInfo, param2Int, param2IAudioPolicyCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registerAudioServerStateDispatcher(IAudioServerStateDispatcher param2IAudioServerStateDispatcher) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioServerStateDispatcher != null) {
            iBinder = param2IAudioServerStateDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().registerAudioServerStateDispatcher(param2IAudioServerStateDispatcher);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterAudioServerStateDispatcher(IAudioServerStateDispatcher param2IAudioServerStateDispatcher) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioServerStateDispatcher != null) {
            iBinder = param2IAudioServerStateDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(98, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().unregisterAudioServerStateDispatcher(param2IAudioServerStateDispatcher);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isAudioServerRunning() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(99, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isAudioServerRunning();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setUidDeviceAffinity(IAudioPolicyCallback param2IAudioPolicyCallback, int param2Int, int[] param2ArrayOfint, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(100, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().setUidDeviceAffinity(param2IAudioPolicyCallback, param2Int, param2ArrayOfint, param2ArrayOfString);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int removeUidDeviceAffinity(IAudioPolicyCallback param2IAudioPolicyCallback, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(101, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().removeUidDeviceAffinity(param2IAudioPolicyCallback, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setUserIdDeviceAffinity(IAudioPolicyCallback param2IAudioPolicyCallback, int param2Int, int[] param2ArrayOfint, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(102, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().setUserIdDeviceAffinity(param2IAudioPolicyCallback, param2Int, param2ArrayOfint, param2ArrayOfString);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int removeUserIdDeviceAffinity(IAudioPolicyCallback param2IAudioPolicyCallback, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IAudioPolicyCallback != null) {
            iBinder = param2IAudioPolicyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(103, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().removeUserIdDeviceAffinity(param2IAudioPolicyCallback, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasHapticChannels(Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool1 = true;
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(104, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().hasHapticChannels(param2Uri);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCallScreeningModeSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(105, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isCallScreeningModeSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setPreferredDeviceForStrategy(int param2Int, AudioDeviceAttributes param2AudioDeviceAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          if (param2AudioDeviceAttributes != null) {
            parcel1.writeInt(1);
            param2AudioDeviceAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(106, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().setPreferredDeviceForStrategy(param2Int, param2AudioDeviceAttributes);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int removePreferredDeviceForStrategy(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(107, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().removePreferredDeviceForStrategy(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AudioDeviceAttributes getPreferredDeviceForStrategy(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          AudioDeviceAttributes audioDeviceAttributes;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(108, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            audioDeviceAttributes = IAudioService.Stub.getDefaultImpl().getPreferredDeviceForStrategy(param2Int);
            return audioDeviceAttributes;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            audioDeviceAttributes = AudioDeviceAttributes.CREATOR.createFromParcel(parcel2);
          } else {
            audioDeviceAttributes = null;
          } 
          return audioDeviceAttributes;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AudioDeviceAttributes> getDevicesForAttributes(AudioAttributes param2AudioAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(109, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getDevicesForAttributes(param2AudioAttributes); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AudioDeviceAttributes.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setAllowedCapturePolicy(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(110, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2Int = IAudioService.Stub.getDefaultImpl().setAllowedCapturePolicy(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAllowedCapturePolicy() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(111, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getAllowedCapturePolicy(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerStrategyPreferredDeviceDispatcher(IStrategyPreferredDeviceDispatcher param2IStrategyPreferredDeviceDispatcher) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2IStrategyPreferredDeviceDispatcher != null) {
            iBinder = param2IStrategyPreferredDeviceDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(112, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().registerStrategyPreferredDeviceDispatcher(param2IStrategyPreferredDeviceDispatcher);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterStrategyPreferredDeviceDispatcher(IStrategyPreferredDeviceDispatcher param2IStrategyPreferredDeviceDispatcher) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2IStrategyPreferredDeviceDispatcher != null) {
            iBinder = param2IStrategyPreferredDeviceDispatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(113, parcel, (Parcel)null, 1);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().unregisterStrategyPreferredDeviceDispatcher(param2IStrategyPreferredDeviceDispatcher);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRttEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(114, parcel, (Parcel)null, 1);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setRttEnabled(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDeviceVolumeBehavior(AudioDeviceAttributes param2AudioDeviceAttributes, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioDeviceAttributes != null) {
            parcel1.writeInt(1);
            param2AudioDeviceAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(115, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setDeviceVolumeBehavior(param2AudioDeviceAttributes, param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDeviceVolumeBehavior(AudioDeviceAttributes param2AudioDeviceAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          if (param2AudioDeviceAttributes != null) {
            parcel1.writeInt(1);
            param2AudioDeviceAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(116, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getDeviceVolumeBehavior(param2AudioDeviceAttributes); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setParameters(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(117, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setParameters(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getParameters(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(118, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            param2String = IAudioService.Stub.getDefaultImpl().getParameters(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int oppoGetMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(119, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().oppoGetMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeMode(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(120, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().removeMode(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getScoClientInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          boolean bool = this.mRemote.transact(121, parcel1, parcel2, 0);
          if (!bool && IAudioService.Stub.getDefaultImpl() != null)
            return IAudioService.Stub.getDefaultImpl().getScoClientInfo(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMultiAudioFocusEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IAudioService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(122, parcel, (Parcel)null, 1);
          if (!bool1 && IAudioService.Stub.getDefaultImpl() != null) {
            IAudioService.Stub.getDefaultImpl().setMultiAudioFocusEnabled(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isBluetoothScoAvailableOffCall() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IAudioService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(123, parcel1, parcel2, 0);
          if (!bool2 && IAudioService.Stub.getDefaultImpl() != null) {
            bool1 = IAudioService.Stub.getDefaultImpl().isBluetoothScoAvailableOffCall();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAudioService param1IAudioService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAudioService != null) {
          Proxy.sDefaultImpl = param1IAudioService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAudioService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
