package android.media;

import android.os.Parcel;
import android.os.Parcelable;

public class MediaResourcePolicyParcel implements Parcelable {
  public static final Parcelable.Creator<MediaResourcePolicyParcel> CREATOR = new Parcelable.Creator<MediaResourcePolicyParcel>() {
      public MediaResourcePolicyParcel createFromParcel(Parcel param1Parcel) {
        MediaResourcePolicyParcel mediaResourcePolicyParcel = new MediaResourcePolicyParcel();
        mediaResourcePolicyParcel.readFromParcel(param1Parcel);
        return mediaResourcePolicyParcel;
      }
      
      public MediaResourcePolicyParcel[] newArray(int param1Int) {
        return new MediaResourcePolicyParcel[param1Int];
      }
    };
  
  public String type;
  
  public String value;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeString(this.type);
    paramParcel.writeString(this.value);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.type = paramParcel.readString();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.value = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
