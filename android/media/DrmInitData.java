package android.media;

import java.util.Arrays;
import java.util.UUID;

public abstract class DrmInitData {
  public int getSchemeInitDataCount() {
    return 0;
  }
  
  public SchemeInitData getSchemeInitDataAt(int paramInt) {
    throw new IndexOutOfBoundsException();
  }
  
  @Deprecated
  public abstract SchemeInitData get(UUID paramUUID);
  
  public static final class SchemeInitData {
    public static final UUID UUID_NIL = new UUID(0L, 0L);
    
    public final byte[] data;
    
    public final String mimeType;
    
    public final UUID uuid;
    
    public SchemeInitData(UUID param1UUID, String param1String, byte[] param1ArrayOfbyte) {
      this.uuid = param1UUID;
      this.mimeType = param1String;
      this.data = param1ArrayOfbyte;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof SchemeInitData;
      boolean bool1 = false;
      if (!bool)
        return false; 
      if (param1Object == this)
        return true; 
      param1Object = param1Object;
      if (this.uuid.equals(((SchemeInitData)param1Object).uuid)) {
        String str1 = this.mimeType, str2 = ((SchemeInitData)param1Object).mimeType;
        if (str1.equals(str2)) {
          byte[] arrayOfByte = this.data;
          param1Object = ((SchemeInitData)param1Object).data;
          if (Arrays.equals(arrayOfByte, (byte[])param1Object))
            bool1 = true; 
        } 
      } 
      return bool1;
    }
    
    public int hashCode() {
      return this.uuid.hashCode() + (this.mimeType.hashCode() + Arrays.hashCode(this.data) * 31) * 31;
    }
  }
}
