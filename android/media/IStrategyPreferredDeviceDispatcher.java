package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IStrategyPreferredDeviceDispatcher extends IInterface {
  void dispatchPrefDeviceChanged(int paramInt, AudioDeviceAttributes paramAudioDeviceAttributes) throws RemoteException;
  
  class Default implements IStrategyPreferredDeviceDispatcher {
    public void dispatchPrefDeviceChanged(int param1Int, AudioDeviceAttributes param1AudioDeviceAttributes) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStrategyPreferredDeviceDispatcher {
    private static final String DESCRIPTOR = "android.media.IStrategyPreferredDeviceDispatcher";
    
    static final int TRANSACTION_dispatchPrefDeviceChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IStrategyPreferredDeviceDispatcher");
    }
    
    public static IStrategyPreferredDeviceDispatcher asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IStrategyPreferredDeviceDispatcher");
      if (iInterface != null && iInterface instanceof IStrategyPreferredDeviceDispatcher)
        return (IStrategyPreferredDeviceDispatcher)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "dispatchPrefDeviceChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IStrategyPreferredDeviceDispatcher");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IStrategyPreferredDeviceDispatcher");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        AudioDeviceAttributes audioDeviceAttributes = AudioDeviceAttributes.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      dispatchPrefDeviceChanged(param1Int1, (AudioDeviceAttributes)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IStrategyPreferredDeviceDispatcher {
      public static IStrategyPreferredDeviceDispatcher sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IStrategyPreferredDeviceDispatcher";
      }
      
      public void dispatchPrefDeviceChanged(int param2Int, AudioDeviceAttributes param2AudioDeviceAttributes) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IStrategyPreferredDeviceDispatcher");
          parcel.writeInt(param2Int);
          if (param2AudioDeviceAttributes != null) {
            parcel.writeInt(1);
            param2AudioDeviceAttributes.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IStrategyPreferredDeviceDispatcher.Stub.getDefaultImpl() != null) {
            IStrategyPreferredDeviceDispatcher.Stub.getDefaultImpl().dispatchPrefDeviceChanged(param2Int, param2AudioDeviceAttributes);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStrategyPreferredDeviceDispatcher param1IStrategyPreferredDeviceDispatcher) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStrategyPreferredDeviceDispatcher != null) {
          Proxy.sDefaultImpl = param1IStrategyPreferredDeviceDispatcher;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStrategyPreferredDeviceDispatcher getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
