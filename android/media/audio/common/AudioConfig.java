package android.media.audio.common;

import android.os.Parcel;
import android.os.Parcelable;

public class AudioConfig implements Parcelable {
  public static final Parcelable.Creator<AudioConfig> CREATOR = new Parcelable.Creator<AudioConfig>() {
      public AudioConfig createFromParcel(Parcel param1Parcel) {
        AudioConfig audioConfig = new AudioConfig();
        audioConfig.readFromParcel(param1Parcel);
        return audioConfig;
      }
      
      public AudioConfig[] newArray(int param1Int) {
        return new AudioConfig[param1Int];
      }
    };
  
  public int channelMask;
  
  public int format;
  
  public long frameCount;
  
  public AudioOffloadInfo offloadInfo;
  
  public int sampleRateHz;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.sampleRateHz);
    paramParcel.writeInt(this.channelMask);
    paramParcel.writeInt(this.format);
    if (this.offloadInfo != null) {
      paramParcel.writeInt(1);
      this.offloadInfo.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeLong(this.frameCount);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.sampleRateHz = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.channelMask = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.format = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        this.offloadInfo = AudioOffloadInfo.CREATOR.createFromParcel(paramParcel);
      } else {
        this.offloadInfo = null;
      } 
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.frameCount = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
