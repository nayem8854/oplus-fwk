package android.media.audio.common;

import android.os.Parcel;
import android.os.Parcelable;

public class AudioOffloadInfo implements Parcelable {
  public static final Parcelable.Creator<AudioOffloadInfo> CREATOR = new Parcelable.Creator<AudioOffloadInfo>() {
      public AudioOffloadInfo createFromParcel(Parcel param1Parcel) {
        AudioOffloadInfo audioOffloadInfo = new AudioOffloadInfo();
        audioOffloadInfo.readFromParcel(param1Parcel);
        return audioOffloadInfo;
      }
      
      public AudioOffloadInfo[] newArray(int param1Int) {
        return new AudioOffloadInfo[param1Int];
      }
    };
  
  public int bitRatePerSecond;
  
  public int bitWidth;
  
  public int bufferSize;
  
  public int channelMask;
  
  public long durationMicroseconds;
  
  public int format;
  
  public boolean hasVideo;
  
  public boolean isStreaming;
  
  public int sampleRateHz;
  
  public int streamType;
  
  public int usage;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.sampleRateHz);
    paramParcel.writeInt(this.channelMask);
    paramParcel.writeInt(this.format);
    paramParcel.writeInt(this.streamType);
    paramParcel.writeInt(this.bitRatePerSecond);
    paramParcel.writeLong(this.durationMicroseconds);
    paramParcel.writeInt(this.hasVideo);
    paramParcel.writeInt(this.isStreaming);
    paramParcel.writeInt(this.bitWidth);
    paramParcel.writeInt(this.bufferSize);
    paramParcel.writeInt(this.usage);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      boolean bool2;
      this.sampleRateHz = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.channelMask = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.format = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.streamType = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.bitRatePerSecond = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.durationMicroseconds = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      k = paramParcel.readInt();
      boolean bool1 = true;
      if (k != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.hasVideo = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      this.isStreaming = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.bitWidth = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.bufferSize = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.usage = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
