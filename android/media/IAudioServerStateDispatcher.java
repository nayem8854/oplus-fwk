package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAudioServerStateDispatcher extends IInterface {
  void dispatchAudioServerStateChange(boolean paramBoolean) throws RemoteException;
  
  class Default implements IAudioServerStateDispatcher {
    public void dispatchAudioServerStateChange(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAudioServerStateDispatcher {
    private static final String DESCRIPTOR = "android.media.IAudioServerStateDispatcher";
    
    static final int TRANSACTION_dispatchAudioServerStateChange = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IAudioServerStateDispatcher");
    }
    
    public static IAudioServerStateDispatcher asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IAudioServerStateDispatcher");
      if (iInterface != null && iInterface instanceof IAudioServerStateDispatcher)
        return (IAudioServerStateDispatcher)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "dispatchAudioServerStateChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IAudioServerStateDispatcher");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IAudioServerStateDispatcher");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      dispatchAudioServerStateChange(bool);
      return true;
    }
    
    private static class Proxy implements IAudioServerStateDispatcher {
      public static IAudioServerStateDispatcher sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IAudioServerStateDispatcher";
      }
      
      public void dispatchAudioServerStateChange(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IAudioServerStateDispatcher");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IAudioServerStateDispatcher.Stub.getDefaultImpl() != null) {
            IAudioServerStateDispatcher.Stub.getDefaultImpl().dispatchAudioServerStateChange(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAudioServerStateDispatcher param1IAudioServerStateDispatcher) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAudioServerStateDispatcher != null) {
          Proxy.sDefaultImpl = param1IAudioServerStateDispatcher;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAudioServerStateDispatcher getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
