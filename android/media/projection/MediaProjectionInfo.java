package android.media.projection;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import java.util.Objects;

public final class MediaProjectionInfo implements Parcelable {
  public MediaProjectionInfo(String paramString, UserHandle paramUserHandle) {
    this.mPackageName = paramString;
    this.mUserHandle = paramUserHandle;
  }
  
  public MediaProjectionInfo(Parcel paramParcel) {
    this.mPackageName = paramParcel.readString();
    this.mUserHandle = UserHandle.readFromParcel(paramParcel);
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public UserHandle getUserHandle() {
    return this.mUserHandle;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof MediaProjectionInfo;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (Objects.equals(((MediaProjectionInfo)paramObject).mPackageName, this.mPackageName)) {
        paramObject = ((MediaProjectionInfo)paramObject).mUserHandle;
        UserHandle userHandle = this.mUserHandle;
        if (Objects.equals(paramObject, userHandle))
          bool1 = true; 
      } 
      return bool1;
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mPackageName, this.mUserHandle });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MediaProjectionInfo{mPackageName=");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append(", mUserHandle=");
    stringBuilder.append(this.mUserHandle);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPackageName);
    UserHandle.writeToParcel(this.mUserHandle, paramParcel);
  }
  
  public static final Parcelable.Creator<MediaProjectionInfo> CREATOR = new Parcelable.Creator<MediaProjectionInfo>() {
      public MediaProjectionInfo createFromParcel(Parcel param1Parcel) {
        return new MediaProjectionInfo(param1Parcel);
      }
      
      public MediaProjectionInfo[] newArray(int param1Int) {
        return new MediaProjectionInfo[param1Int];
      }
    };
  
  private final String mPackageName;
  
  private final UserHandle mUserHandle;
}
