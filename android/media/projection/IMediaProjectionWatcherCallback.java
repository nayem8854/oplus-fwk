package android.media.projection;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaProjectionWatcherCallback extends IInterface {
  void onStart(MediaProjectionInfo paramMediaProjectionInfo) throws RemoteException;
  
  void onStop(MediaProjectionInfo paramMediaProjectionInfo) throws RemoteException;
  
  class Default implements IMediaProjectionWatcherCallback {
    public void onStart(MediaProjectionInfo param1MediaProjectionInfo) throws RemoteException {}
    
    public void onStop(MediaProjectionInfo param1MediaProjectionInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaProjectionWatcherCallback {
    private static final String DESCRIPTOR = "android.media.projection.IMediaProjectionWatcherCallback";
    
    static final int TRANSACTION_onStart = 1;
    
    static final int TRANSACTION_onStop = 2;
    
    public Stub() {
      attachInterface(this, "android.media.projection.IMediaProjectionWatcherCallback");
    }
    
    public static IMediaProjectionWatcherCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.projection.IMediaProjectionWatcherCallback");
      if (iInterface != null && iInterface instanceof IMediaProjectionWatcherCallback)
        return (IMediaProjectionWatcherCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onStop";
      } 
      return "onStart";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.media.projection.IMediaProjectionWatcherCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.projection.IMediaProjectionWatcherCallback");
        if (param1Parcel1.readInt() != 0) {
          MediaProjectionInfo mediaProjectionInfo = MediaProjectionInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onStop((MediaProjectionInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.projection.IMediaProjectionWatcherCallback");
      if (param1Parcel1.readInt() != 0) {
        MediaProjectionInfo mediaProjectionInfo = MediaProjectionInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onStart((MediaProjectionInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IMediaProjectionWatcherCallback {
      public static IMediaProjectionWatcherCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.projection.IMediaProjectionWatcherCallback";
      }
      
      public void onStart(MediaProjectionInfo param2MediaProjectionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.projection.IMediaProjectionWatcherCallback");
          if (param2MediaProjectionInfo != null) {
            parcel.writeInt(1);
            param2MediaProjectionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaProjectionWatcherCallback.Stub.getDefaultImpl() != null) {
            IMediaProjectionWatcherCallback.Stub.getDefaultImpl().onStart(param2MediaProjectionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStop(MediaProjectionInfo param2MediaProjectionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.projection.IMediaProjectionWatcherCallback");
          if (param2MediaProjectionInfo != null) {
            parcel.writeInt(1);
            param2MediaProjectionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMediaProjectionWatcherCallback.Stub.getDefaultImpl() != null) {
            IMediaProjectionWatcherCallback.Stub.getDefaultImpl().onStop(param2MediaProjectionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaProjectionWatcherCallback param1IMediaProjectionWatcherCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaProjectionWatcherCallback != null) {
          Proxy.sDefaultImpl = param1IMediaProjectionWatcherCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaProjectionWatcherCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
