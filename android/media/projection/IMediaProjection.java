package android.media.projection;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaProjection extends IInterface {
  int applyVirtualDisplayFlags(int paramInt) throws RemoteException;
  
  boolean canProjectAudio() throws RemoteException;
  
  boolean canProjectSecureVideo() throws RemoteException;
  
  boolean canProjectVideo() throws RemoteException;
  
  void registerCallback(IMediaProjectionCallback paramIMediaProjectionCallback) throws RemoteException;
  
  void start(IMediaProjectionCallback paramIMediaProjectionCallback) throws RemoteException;
  
  void stop() throws RemoteException;
  
  void unregisterCallback(IMediaProjectionCallback paramIMediaProjectionCallback) throws RemoteException;
  
  class Default implements IMediaProjection {
    public void start(IMediaProjectionCallback param1IMediaProjectionCallback) throws RemoteException {}
    
    public void stop() throws RemoteException {}
    
    public boolean canProjectAudio() throws RemoteException {
      return false;
    }
    
    public boolean canProjectVideo() throws RemoteException {
      return false;
    }
    
    public boolean canProjectSecureVideo() throws RemoteException {
      return false;
    }
    
    public int applyVirtualDisplayFlags(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void registerCallback(IMediaProjectionCallback param1IMediaProjectionCallback) throws RemoteException {}
    
    public void unregisterCallback(IMediaProjectionCallback param1IMediaProjectionCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaProjection {
    private static final String DESCRIPTOR = "android.media.projection.IMediaProjection";
    
    static final int TRANSACTION_applyVirtualDisplayFlags = 6;
    
    static final int TRANSACTION_canProjectAudio = 3;
    
    static final int TRANSACTION_canProjectSecureVideo = 5;
    
    static final int TRANSACTION_canProjectVideo = 4;
    
    static final int TRANSACTION_registerCallback = 7;
    
    static final int TRANSACTION_start = 1;
    
    static final int TRANSACTION_stop = 2;
    
    static final int TRANSACTION_unregisterCallback = 8;
    
    public Stub() {
      attachInterface(this, "android.media.projection.IMediaProjection");
    }
    
    public static IMediaProjection asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.projection.IMediaProjection");
      if (iInterface != null && iInterface instanceof IMediaProjection)
        return (IMediaProjection)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "unregisterCallback";
        case 7:
          return "registerCallback";
        case 6:
          return "applyVirtualDisplayFlags";
        case 5:
          return "canProjectSecureVideo";
        case 4:
          return "canProjectVideo";
        case 3:
          return "canProjectAudio";
        case 2:
          return "stop";
        case 1:
          break;
      } 
      return "start";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.media.projection.IMediaProjection");
            iMediaProjectionCallback = IMediaProjectionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            unregisterCallback(iMediaProjectionCallback);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iMediaProjectionCallback.enforceInterface("android.media.projection.IMediaProjection");
            iMediaProjectionCallback = IMediaProjectionCallback.Stub.asInterface(iMediaProjectionCallback.readStrongBinder());
            registerCallback(iMediaProjectionCallback);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iMediaProjectionCallback.enforceInterface("android.media.projection.IMediaProjection");
            param1Int1 = iMediaProjectionCallback.readInt();
            param1Int1 = applyVirtualDisplayFlags(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 5:
            iMediaProjectionCallback.enforceInterface("android.media.projection.IMediaProjection");
            bool = canProjectSecureVideo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            iMediaProjectionCallback.enforceInterface("android.media.projection.IMediaProjection");
            bool = canProjectVideo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            iMediaProjectionCallback.enforceInterface("android.media.projection.IMediaProjection");
            bool = canProjectAudio();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            iMediaProjectionCallback.enforceInterface("android.media.projection.IMediaProjection");
            stop();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iMediaProjectionCallback.enforceInterface("android.media.projection.IMediaProjection");
        IMediaProjectionCallback iMediaProjectionCallback = IMediaProjectionCallback.Stub.asInterface(iMediaProjectionCallback.readStrongBinder());
        start(iMediaProjectionCallback);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.media.projection.IMediaProjection");
      return true;
    }
    
    private static class Proxy implements IMediaProjection {
      public static IMediaProjection sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.projection.IMediaProjection";
      }
      
      public void start(IMediaProjectionCallback param2IMediaProjectionCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjection");
          if (param2IMediaProjectionCallback != null) {
            iBinder = param2IMediaProjectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMediaProjection.Stub.getDefaultImpl() != null) {
            IMediaProjection.Stub.getDefaultImpl().start(param2IMediaProjectionCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stop() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjection");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMediaProjection.Stub.getDefaultImpl() != null) {
            IMediaProjection.Stub.getDefaultImpl().stop();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canProjectAudio() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjection");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IMediaProjection.Stub.getDefaultImpl() != null) {
            bool1 = IMediaProjection.Stub.getDefaultImpl().canProjectAudio();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canProjectVideo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjection");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IMediaProjection.Stub.getDefaultImpl() != null) {
            bool1 = IMediaProjection.Stub.getDefaultImpl().canProjectVideo();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canProjectSecureVideo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjection");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IMediaProjection.Stub.getDefaultImpl() != null) {
            bool1 = IMediaProjection.Stub.getDefaultImpl().canProjectSecureVideo();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int applyVirtualDisplayFlags(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjection");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IMediaProjection.Stub.getDefaultImpl() != null) {
            param2Int = IMediaProjection.Stub.getDefaultImpl().applyVirtualDisplayFlags(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerCallback(IMediaProjectionCallback param2IMediaProjectionCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjection");
          if (param2IMediaProjectionCallback != null) {
            iBinder = param2IMediaProjectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IMediaProjection.Stub.getDefaultImpl() != null) {
            IMediaProjection.Stub.getDefaultImpl().registerCallback(param2IMediaProjectionCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterCallback(IMediaProjectionCallback param2IMediaProjectionCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjection");
          if (param2IMediaProjectionCallback != null) {
            iBinder = param2IMediaProjectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IMediaProjection.Stub.getDefaultImpl() != null) {
            IMediaProjection.Stub.getDefaultImpl().unregisterCallback(param2IMediaProjectionCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaProjection param1IMediaProjection) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaProjection != null) {
          Proxy.sDefaultImpl = param1IMediaProjection;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaProjection getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
