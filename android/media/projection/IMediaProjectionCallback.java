package android.media.projection;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaProjectionCallback extends IInterface {
  void onStop() throws RemoteException;
  
  class Default implements IMediaProjectionCallback {
    public void onStop() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaProjectionCallback {
    private static final String DESCRIPTOR = "android.media.projection.IMediaProjectionCallback";
    
    static final int TRANSACTION_onStop = 1;
    
    public Stub() {
      attachInterface(this, "android.media.projection.IMediaProjectionCallback");
    }
    
    public static IMediaProjectionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.projection.IMediaProjectionCallback");
      if (iInterface != null && iInterface instanceof IMediaProjectionCallback)
        return (IMediaProjectionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onStop";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.projection.IMediaProjectionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.projection.IMediaProjectionCallback");
      onStop();
      return true;
    }
    
    class Proxy implements IMediaProjectionCallback {
      public static IMediaProjectionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IMediaProjectionCallback.Stub this$0) {
        this.mRemote = this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.projection.IMediaProjectionCallback";
      }
      
      public void onStop() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.projection.IMediaProjectionCallback");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaProjectionCallback.Stub.getDefaultImpl() != null) {
            IMediaProjectionCallback.Stub.getDefaultImpl().onStop();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaProjectionCallback param1IMediaProjectionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaProjectionCallback != null) {
          Proxy.sDefaultImpl = param1IMediaProjectionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaProjectionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
