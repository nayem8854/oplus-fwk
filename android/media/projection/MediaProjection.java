package android.media.projection;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.hardware.display.VirtualDisplayConfig;
import android.os.Handler;
import android.os.RemoteException;
import android.os.customize.OplusCustomizeRestrictionManager;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Surface;
import java.util.Map;

public final class MediaProjection {
  private static final String TAG = "MediaProjection";
  
  private final Map<Callback, CallbackRecord> mCallbacks;
  
  private final Context mContext;
  
  private final IMediaProjection mImpl;
  
  public MediaProjection(Context paramContext, IMediaProjection paramIMediaProjection) {
    this.mCallbacks = (Map<Callback, CallbackRecord>)new ArrayMap();
    this.mContext = paramContext;
    this.mImpl = paramIMediaProjection;
    try {
      MediaProjectionCallback mediaProjectionCallback = new MediaProjectionCallback();
      this(this);
      paramIMediaProjection.start(mediaProjectionCallback);
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Failed to start media projection", remoteException);
    } 
  }
  
  public void registerCallback(Callback paramCallback, Handler paramHandler) {
    if (paramCallback != null) {
      Handler handler = paramHandler;
      if (paramHandler == null)
        handler = new Handler(); 
      this.mCallbacks.put(paramCallback, new CallbackRecord(paramCallback, handler));
      return;
    } 
    throw new IllegalArgumentException("callback should not be null");
  }
  
  public void unregisterCallback(Callback paramCallback) {
    if (paramCallback != null) {
      this.mCallbacks.remove(paramCallback);
      return;
    } 
    throw new IllegalArgumentException("callback should not be null");
  }
  
  public VirtualDisplay createVirtualDisplay(String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, Surface paramSurface, VirtualDisplay.Callback paramCallback, Handler paramHandler) {
    DisplayManager displayManager = (DisplayManager)this.mContext.getSystemService("display");
    int i = 18;
    if (paramBoolean)
      i = 0x12 | 0x4; 
    VirtualDisplayConfig.Builder builder = new VirtualDisplayConfig.Builder(paramString, paramInt1, paramInt2, paramInt3);
    builder.setFlags(i);
    if (paramSurface != null)
      builder.setSurface(paramSurface); 
    return displayManager.createVirtualDisplay(this, builder.build(), paramCallback, paramHandler);
  }
  
  public VirtualDisplay createVirtualDisplay(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Surface paramSurface, VirtualDisplay.Callback paramCallback, Handler paramHandler) {
    try {
      OplusCustomizeRestrictionManager oplusCustomizeRestrictionManager = OplusCustomizeRestrictionManager.getInstance(this.mContext);
      if (oplusCustomizeRestrictionManager.getForbidRecordScreenState()) {
        Log.w("MediaProjection", "createVirtualDisplay but RecordScreen forbid!");
        return null;
      } 
    } catch (Exception exception) {
      Log.e("MediaProjection", "Unable to connect to OplusCustomizeRestrictionManager", exception);
    } 
    VirtualDisplayConfig.Builder builder = new VirtualDisplayConfig.Builder(paramString, paramInt1, paramInt2, paramInt3);
    builder.setFlags(paramInt4);
    if (paramSurface != null)
      builder.setSurface(paramSurface); 
    return createVirtualDisplay(builder.build(), paramCallback, paramHandler);
  }
  
  public VirtualDisplay createVirtualDisplay(VirtualDisplayConfig paramVirtualDisplayConfig, VirtualDisplay.Callback paramCallback, Handler paramHandler) {
    DisplayManager displayManager = (DisplayManager)this.mContext.getSystemService(DisplayManager.class);
    return displayManager.createVirtualDisplay(this, paramVirtualDisplayConfig, paramCallback, paramHandler);
  }
  
  public void stop() {
    try {
      this.mImpl.stop();
    } catch (RemoteException remoteException) {
      Log.e("MediaProjection", "Unable to stop projection", (Throwable)remoteException);
    } 
  }
  
  public IMediaProjection getProjection() {
    return this.mImpl;
  }
  
  public static abstract class Callback {
    public void onStop() {}
  }
  
  class MediaProjectionCallback extends IMediaProjectionCallback.Stub {
    final MediaProjection this$0;
    
    private MediaProjectionCallback() {}
    
    public void onStop() {
      for (MediaProjection.CallbackRecord callbackRecord : MediaProjection.this.mCallbacks.values())
        callbackRecord.onStop(); 
    }
  }
  
  private static final class CallbackRecord {
    private final MediaProjection.Callback mCallback;
    
    private final Handler mHandler;
    
    public CallbackRecord(MediaProjection.Callback param1Callback, Handler param1Handler) {
      this.mCallback = param1Callback;
      this.mHandler = param1Handler;
    }
    
    public void onStop() {
      this.mHandler.post(new Runnable() {
            final MediaProjection.CallbackRecord this$0;
            
            public void run() {
              MediaProjection.CallbackRecord.this.mCallback.onStop();
            }
          });
    }
  }
  
  class null implements Runnable {
    final MediaProjection.CallbackRecord this$0;
    
    public void run() {
      this.this$0.mCallback.onStop();
    }
  }
}
