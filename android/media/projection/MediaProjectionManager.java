package android.media.projection;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.ArrayMap;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public final class MediaProjectionManager {
  public static final String EXTRA_APP_TOKEN = "android.media.projection.extra.EXTRA_APP_TOKEN";
  
  public static final String EXTRA_MEDIA_PROJECTION = "android.media.projection.extra.EXTRA_MEDIA_PROJECTION";
  
  private static final String TAG = "MediaProjectionManager";
  
  public static final int TYPE_MIRRORING = 1;
  
  public static final int TYPE_PRESENTATION = 2;
  
  public static final int TYPE_SCREEN_CAPTURE = 0;
  
  private Map<Callback, CallbackDelegate> mCallbacks;
  
  private Context mContext;
  
  private IMediaProjectionManager mService;
  
  public MediaProjectionManager(Context paramContext) {
    this.mContext = paramContext;
    IBinder iBinder = ServiceManager.getService("media_projection");
    this.mService = IMediaProjectionManager.Stub.asInterface(iBinder);
    this.mCallbacks = (Map<Callback, CallbackDelegate>)new ArrayMap();
  }
  
  public Intent createScreenCaptureIntent() {
    Intent intent = new Intent();
    Context context = this.mContext;
    ComponentName componentName = ComponentName.unflattenFromString(context.getResources().getString(17039928));
    intent.setComponent(componentName);
    return intent;
  }
  
  public MediaProjection getMediaProjection(int paramInt, Intent paramIntent) {
    String str2;
    if (paramInt != -1 || paramIntent == null)
      return null; 
    IBinder iBinder = paramIntent.getIBinderExtra("android.media.projection.extra.EXTRA_MEDIA_PROJECTION");
    if (iBinder == null)
      return null; 
    long l = System.currentTimeMillis();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date(l);
    String str3 = simpleDateFormat.format(date);
    paramInt = (this.mContext.getApplicationInfo()).labelRes;
    if (paramInt > 0) {
      str2 = this.mContext.getResources().getString(paramInt);
    } else {
      str2 = "Unkown-labelRes";
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str3);
    stringBuilder.append("<");
    stringBuilder.append(str2);
    stringBuilder.append(">[picture][");
    Context context = this.mContext;
    stringBuilder.append((context.getApplicationInfo()).processName);
    stringBuilder.append("]:[new MediaProjection(..)]");
    String str1 = stringBuilder.toString();
    Log.w("MediaProjectionManager", str1);
    return new MediaProjection(this.mContext, IMediaProjection.Stub.asInterface(iBinder));
  }
  
  public MediaProjectionInfo getActiveProjectionInfo() {
    try {
      return this.mService.getActiveProjectionInfo();
    } catch (RemoteException remoteException) {
      Log.e("MediaProjectionManager", "Unable to get the active projection info", (Throwable)remoteException);
      return null;
    } 
  }
  
  public void stopActiveProjection() {
    try {
      this.mService.stopActiveProjection();
    } catch (RemoteException remoteException) {
      Log.e("MediaProjectionManager", "Unable to stop the currently active media projection", (Throwable)remoteException);
    } 
  }
  
  public void addCallback(Callback paramCallback, Handler paramHandler) {
    if (paramCallback != null) {
      CallbackDelegate callbackDelegate = new CallbackDelegate(paramCallback, paramHandler);
      this.mCallbacks.put(paramCallback, callbackDelegate);
      try {
        this.mService.addCallback(callbackDelegate);
      } catch (RemoteException remoteException) {
        Log.e("MediaProjectionManager", "Unable to add callbacks to MediaProjection service", (Throwable)remoteException);
      } 
      return;
    } 
    throw new IllegalArgumentException("callback must not be null");
  }
  
  public void removeCallback(Callback paramCallback) {
    if (paramCallback != null) {
      CallbackDelegate callbackDelegate = this.mCallbacks.remove(paramCallback);
      if (callbackDelegate != null)
        try {
          this.mService.removeCallback(callbackDelegate);
        } catch (RemoteException remoteException) {
          Log.e("MediaProjectionManager", "Unable to add callbacks to MediaProjection service", (Throwable)remoteException);
        }  
      return;
    } 
    throw new IllegalArgumentException("callback must not be null");
  }
  
  public static abstract class Callback {
    public abstract void onStart(MediaProjectionInfo param1MediaProjectionInfo);
    
    public abstract void onStop(MediaProjectionInfo param1MediaProjectionInfo);
  }
  
  class CallbackDelegate extends IMediaProjectionWatcherCallback.Stub {
    private MediaProjectionManager.Callback mCallback;
    
    private Handler mHandler;
    
    public CallbackDelegate(MediaProjectionManager this$0, Handler param1Handler) {
      this.mCallback = (MediaProjectionManager.Callback)this$0;
      Handler handler = param1Handler;
      if (param1Handler == null)
        handler = new Handler(); 
      this.mHandler = handler;
    }
    
    public void onStart(final MediaProjectionInfo info) {
      this.mHandler.post(new Runnable() {
            final MediaProjectionManager.CallbackDelegate this$0;
            
            final MediaProjectionInfo val$info;
            
            public void run() {
              this.this$0.mCallback.onStart(info);
            }
          });
    }
    
    public void onStop(final MediaProjectionInfo info) {
      this.mHandler.post(new Runnable() {
            final MediaProjectionManager.CallbackDelegate this$0;
            
            final MediaProjectionInfo val$info;
            
            public void run() {
              this.this$0.mCallback.onStop(info);
            }
          });
    }
  }
  
  class null implements Runnable {
    final MediaProjectionManager.CallbackDelegate this$0;
    
    final MediaProjectionInfo val$info;
    
    public void run() {
      this.this$0.mCallback.onStart(info);
    }
  }
  
  class null implements Runnable {
    final MediaProjectionManager.CallbackDelegate this$0;
    
    final MediaProjectionInfo val$info;
    
    public void run() {
      this.this$0.mCallback.onStop(info);
    }
  }
}
