package android.media.projection;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaProjectionManager extends IInterface {
  void addCallback(IMediaProjectionWatcherCallback paramIMediaProjectionWatcherCallback) throws RemoteException;
  
  IMediaProjection createProjection(int paramInt1, String paramString, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  MediaProjectionInfo getActiveProjectionInfo() throws RemoteException;
  
  boolean hasProjectionPermission(int paramInt, String paramString) throws RemoteException;
  
  boolean isValidMediaProjection(IMediaProjection paramIMediaProjection) throws RemoteException;
  
  void removeCallback(IMediaProjectionWatcherCallback paramIMediaProjectionWatcherCallback) throws RemoteException;
  
  void stopActiveProjection() throws RemoteException;
  
  class Default implements IMediaProjectionManager {
    public boolean hasProjectionPermission(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public IMediaProjection createProjection(int param1Int1, String param1String, int param1Int2, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public boolean isValidMediaProjection(IMediaProjection param1IMediaProjection) throws RemoteException {
      return false;
    }
    
    public MediaProjectionInfo getActiveProjectionInfo() throws RemoteException {
      return null;
    }
    
    public void stopActiveProjection() throws RemoteException {}
    
    public void addCallback(IMediaProjectionWatcherCallback param1IMediaProjectionWatcherCallback) throws RemoteException {}
    
    public void removeCallback(IMediaProjectionWatcherCallback param1IMediaProjectionWatcherCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaProjectionManager {
    private static final String DESCRIPTOR = "android.media.projection.IMediaProjectionManager";
    
    static final int TRANSACTION_addCallback = 6;
    
    static final int TRANSACTION_createProjection = 2;
    
    static final int TRANSACTION_getActiveProjectionInfo = 4;
    
    static final int TRANSACTION_hasProjectionPermission = 1;
    
    static final int TRANSACTION_isValidMediaProjection = 3;
    
    static final int TRANSACTION_removeCallback = 7;
    
    static final int TRANSACTION_stopActiveProjection = 5;
    
    public Stub() {
      attachInterface(this, "android.media.projection.IMediaProjectionManager");
    }
    
    public static IMediaProjectionManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.projection.IMediaProjectionManager");
      if (iInterface != null && iInterface instanceof IMediaProjectionManager)
        return (IMediaProjectionManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "removeCallback";
        case 6:
          return "addCallback";
        case 5:
          return "stopActiveProjection";
        case 4:
          return "getActiveProjectionInfo";
        case 3:
          return "isValidMediaProjection";
        case 2:
          return "createProjection";
        case 1:
          break;
      } 
      return "hasProjectionPermission";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        IMediaProjectionWatcherCallback iMediaProjectionWatcherCallback;
        MediaProjectionInfo mediaProjectionInfo;
        IMediaProjection iMediaProjection;
        String str2;
        boolean bool = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.media.projection.IMediaProjectionManager");
            iMediaProjectionWatcherCallback = IMediaProjectionWatcherCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            removeCallback(iMediaProjectionWatcherCallback);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iMediaProjectionWatcherCallback.enforceInterface("android.media.projection.IMediaProjectionManager");
            iMediaProjectionWatcherCallback = IMediaProjectionWatcherCallback.Stub.asInterface(iMediaProjectionWatcherCallback.readStrongBinder());
            addCallback(iMediaProjectionWatcherCallback);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iMediaProjectionWatcherCallback.enforceInterface("android.media.projection.IMediaProjectionManager");
            stopActiveProjection();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iMediaProjectionWatcherCallback.enforceInterface("android.media.projection.IMediaProjectionManager");
            mediaProjectionInfo = getActiveProjectionInfo();
            param1Parcel2.writeNoException();
            if (mediaProjectionInfo != null) {
              param1Parcel2.writeInt(1);
              mediaProjectionInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            mediaProjectionInfo.enforceInterface("android.media.projection.IMediaProjectionManager");
            iMediaProjection = IMediaProjection.Stub.asInterface(mediaProjectionInfo.readStrongBinder());
            bool2 = isValidMediaProjection(iMediaProjection);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            iMediaProjection.enforceInterface("android.media.projection.IMediaProjectionManager");
            param1Int2 = iMediaProjection.readInt();
            str2 = iMediaProjection.readString();
            i = iMediaProjection.readInt();
            if (iMediaProjection.readInt() != 0)
              bool = true; 
            iMediaProjection = createProjection(param1Int2, str2, i, bool);
            param1Parcel2.writeNoException();
            if (iMediaProjection != null) {
              IBinder iBinder = iMediaProjection.asBinder();
            } else {
              iMediaProjection = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iMediaProjection);
            return true;
          case 1:
            break;
        } 
        iMediaProjection.enforceInterface("android.media.projection.IMediaProjectionManager");
        int i = iMediaProjection.readInt();
        String str1 = iMediaProjection.readString();
        boolean bool1 = hasProjectionPermission(i, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.media.projection.IMediaProjectionManager");
      return true;
    }
    
    private static class Proxy implements IMediaProjectionManager {
      public static IMediaProjectionManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.projection.IMediaProjectionManager";
      }
      
      public boolean hasProjectionPermission(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IMediaProjectionManager.Stub.getDefaultImpl() != null) {
            bool1 = IMediaProjectionManager.Stub.getDefaultImpl().hasProjectionPermission(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IMediaProjection createProjection(int param2Int1, String param2String, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IMediaProjectionManager.Stub.getDefaultImpl() != null)
            return IMediaProjectionManager.Stub.getDefaultImpl().createProjection(param2Int1, param2String, param2Int2, param2Boolean); 
          parcel2.readException();
          return IMediaProjection.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isValidMediaProjection(IMediaProjection param2IMediaProjection) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
          if (param2IMediaProjection != null) {
            iBinder = param2IMediaProjection.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IMediaProjectionManager.Stub.getDefaultImpl() != null) {
            bool1 = IMediaProjectionManager.Stub.getDefaultImpl().isValidMediaProjection(param2IMediaProjection);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MediaProjectionInfo getActiveProjectionInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          MediaProjectionInfo mediaProjectionInfo;
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMediaProjectionManager.Stub.getDefaultImpl() != null) {
            mediaProjectionInfo = IMediaProjectionManager.Stub.getDefaultImpl().getActiveProjectionInfo();
            return mediaProjectionInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            mediaProjectionInfo = MediaProjectionInfo.CREATOR.createFromParcel(parcel2);
          } else {
            mediaProjectionInfo = null;
          } 
          return mediaProjectionInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopActiveProjection() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IMediaProjectionManager.Stub.getDefaultImpl() != null) {
            IMediaProjectionManager.Stub.getDefaultImpl().stopActiveProjection();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addCallback(IMediaProjectionWatcherCallback param2IMediaProjectionWatcherCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
          if (param2IMediaProjectionWatcherCallback != null) {
            iBinder = param2IMediaProjectionWatcherCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IMediaProjectionManager.Stub.getDefaultImpl() != null) {
            IMediaProjectionManager.Stub.getDefaultImpl().addCallback(param2IMediaProjectionWatcherCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeCallback(IMediaProjectionWatcherCallback param2IMediaProjectionWatcherCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
          if (param2IMediaProjectionWatcherCallback != null) {
            iBinder = param2IMediaProjectionWatcherCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IMediaProjectionManager.Stub.getDefaultImpl() != null) {
            IMediaProjectionManager.Stub.getDefaultImpl().removeCallback(param2IMediaProjectionWatcherCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaProjectionManager param1IMediaProjectionManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaProjectionManager != null) {
          Proxy.sDefaultImpl = param1IMediaProjectionManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaProjectionManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
