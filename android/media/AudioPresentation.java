package android.media;

import android.icu.util.ULocale;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public final class AudioPresentation {
  public static final int MASTERED_FOR_3D = 3;
  
  public static final int MASTERED_FOR_HEADPHONE = 4;
  
  public static final int MASTERED_FOR_STEREO = 1;
  
  public static final int MASTERED_FOR_SURROUND = 2;
  
  public static final int MASTERING_NOT_INDICATED = 0;
  
  private static final int UNKNOWN_ID = -1;
  
  private final boolean mAudioDescriptionAvailable;
  
  private final boolean mDialogueEnhancementAvailable;
  
  private final Map<ULocale, CharSequence> mLabels;
  
  private final ULocale mLanguage;
  
  private final int mMasteringIndication;
  
  private final int mPresentationId;
  
  private final int mProgramId;
  
  private final boolean mSpokenSubtitlesAvailable;
  
  private AudioPresentation(int paramInt1, int paramInt2, ULocale paramULocale, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, Map<ULocale, CharSequence> paramMap) {
    this.mPresentationId = paramInt1;
    this.mProgramId = paramInt2;
    this.mLanguage = paramULocale;
    this.mMasteringIndication = paramInt3;
    this.mAudioDescriptionAvailable = paramBoolean1;
    this.mSpokenSubtitlesAvailable = paramBoolean2;
    this.mDialogueEnhancementAvailable = paramBoolean3;
    this.mLabels = new HashMap<>(paramMap);
  }
  
  public int getPresentationId() {
    return this.mPresentationId;
  }
  
  public int getProgramId() {
    return this.mProgramId;
  }
  
  public Map<Locale, String> getLabels() {
    HashMap<Object, Object> hashMap = new HashMap<>(this.mLabels.size());
    for (Map.Entry<ULocale, CharSequence> entry : this.mLabels.entrySet())
      hashMap.put(((ULocale)entry.getKey()).toLocale(), ((CharSequence)entry.getValue()).toString()); 
    return (Map)hashMap;
  }
  
  private Map<ULocale, CharSequence> getULabels() {
    return this.mLabels;
  }
  
  public Locale getLocale() {
    return this.mLanguage.toLocale();
  }
  
  private ULocale getULocale() {
    return this.mLanguage;
  }
  
  public int getMasteringIndication() {
    return this.mMasteringIndication;
  }
  
  public boolean hasAudioDescription() {
    return this.mAudioDescriptionAvailable;
  }
  
  public boolean hasSpokenSubtitles() {
    return this.mSpokenSubtitlesAvailable;
  }
  
  public boolean hasDialogueEnhancement() {
    return this.mDialogueEnhancementAvailable;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof AudioPresentation))
      return false; 
    paramObject = paramObject;
    if (this.mPresentationId == paramObject.getPresentationId()) {
      int i = this.mProgramId;
      if (i == paramObject.getProgramId()) {
        ULocale uLocale = this.mLanguage;
        if (uLocale.equals(paramObject.getULocale())) {
          i = this.mMasteringIndication;
          if (i == paramObject.getMasteringIndication()) {
            boolean bool = this.mAudioDescriptionAvailable;
            if (bool == paramObject.hasAudioDescription()) {
              bool = this.mSpokenSubtitlesAvailable;
              if (bool == paramObject.hasSpokenSubtitles()) {
                bool = this.mDialogueEnhancementAvailable;
                if (bool == paramObject.hasDialogueEnhancement()) {
                  Map<ULocale, CharSequence> map = this.mLabels;
                  if (map.equals(paramObject.getULabels()))
                    return null; 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mPresentationId, j = this.mProgramId;
    ULocale uLocale = this.mLanguage;
    int k = uLocale.hashCode(), m = this.mMasteringIndication;
    boolean bool1 = this.mAudioDescriptionAvailable;
    boolean bool2 = this.mSpokenSubtitlesAvailable;
    boolean bool3 = this.mDialogueEnhancementAvailable;
    Map<ULocale, CharSequence> map = this.mLabels;
    int n = map.hashCode();
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Boolean.valueOf(bool1), Boolean.valueOf(bool2), Boolean.valueOf(bool3), Integer.valueOf(n) });
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(getClass().getSimpleName());
    stringBuilder2.append(" ");
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("{ presentation id=");
    stringBuilder2.append(this.mPresentationId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", program id=");
    stringBuilder2.append(this.mProgramId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", language=");
    stringBuilder2.append(this.mLanguage);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", labels=");
    stringBuilder2.append(this.mLabels);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", mastering indication=");
    stringBuilder2.append(this.mMasteringIndication);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", audio description=");
    stringBuilder2.append(this.mAudioDescriptionAvailable);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", spoken subtitles=");
    stringBuilder2.append(this.mSpokenSubtitlesAvailable);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", dialogue enhancement=");
    stringBuilder2.append(this.mDialogueEnhancementAvailable);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
  
  public static final class Builder {
    private int mProgramId = -1;
    
    private ULocale mLanguage = new ULocale("");
    
    private int mMasteringIndication = 0;
    
    private boolean mAudioDescriptionAvailable = false;
    
    private boolean mSpokenSubtitlesAvailable = false;
    
    private boolean mDialogueEnhancementAvailable = false;
    
    private Map<ULocale, CharSequence> mLabels = new HashMap<>();
    
    private final int mPresentationId;
    
    public Builder(int param1Int) {
      this.mPresentationId = param1Int;
    }
    
    public Builder setProgramId(int param1Int) {
      this.mProgramId = param1Int;
      return this;
    }
    
    public Builder setLocale(ULocale param1ULocale) {
      this.mLanguage = param1ULocale;
      return this;
    }
    
    public Builder setMasteringIndication(int param1Int) {
      if (param1Int == 0 || param1Int == 1 || param1Int == 2 || param1Int == 3 || param1Int == 4) {
        this.mMasteringIndication = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown mastering indication: ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setLabels(Map<ULocale, CharSequence> param1Map) {
      this.mLabels = new HashMap<>(param1Map);
      return this;
    }
    
    public Builder setHasAudioDescription(boolean param1Boolean) {
      this.mAudioDescriptionAvailable = param1Boolean;
      return this;
    }
    
    public Builder setHasSpokenSubtitles(boolean param1Boolean) {
      this.mSpokenSubtitlesAvailable = param1Boolean;
      return this;
    }
    
    public Builder setHasDialogueEnhancement(boolean param1Boolean) {
      this.mDialogueEnhancementAvailable = param1Boolean;
      return this;
    }
    
    public AudioPresentation build() {
      return new AudioPresentation(this.mPresentationId, this.mProgramId, this.mLanguage, this.mMasteringIndication, this.mAudioDescriptionAvailable, this.mSpokenSubtitlesAvailable, this.mDialogueEnhancementAvailable, this.mLabels);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface MasteringIndicationType {}
}
