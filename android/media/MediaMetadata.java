package android.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseArray;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;
import java.util.Set;

public final class MediaMetadata implements Parcelable {
  public static final Parcelable.Creator<MediaMetadata> CREATOR;
  
  private static final SparseArray<String> EDITOR_KEY_MAPPING;
  
  private static final ArrayMap<String, Integer> METADATA_KEYS_TYPE;
  
  public static final String METADATA_KEY_ALBUM = "android.media.metadata.ALBUM";
  
  public static final String METADATA_KEY_ALBUM_ART = "android.media.metadata.ALBUM_ART";
  
  public static final String METADATA_KEY_ALBUM_ARTIST = "android.media.metadata.ALBUM_ARTIST";
  
  public static final String METADATA_KEY_ALBUM_ART_URI = "android.media.metadata.ALBUM_ART_URI";
  
  public static final String METADATA_KEY_ART = "android.media.metadata.ART";
  
  public static final String METADATA_KEY_ARTIST = "android.media.metadata.ARTIST";
  
  public static final String METADATA_KEY_ART_URI = "android.media.metadata.ART_URI";
  
  public static final String METADATA_KEY_AUTHOR = "android.media.metadata.AUTHOR";
  
  public static final String METADATA_KEY_BT_FOLDER_TYPE = "android.media.metadata.BT_FOLDER_TYPE";
  
  public static final String METADATA_KEY_COMPILATION = "android.media.metadata.COMPILATION";
  
  public static final String METADATA_KEY_COMPOSER = "android.media.metadata.COMPOSER";
  
  public static final String METADATA_KEY_DATE = "android.media.metadata.DATE";
  
  public static final String METADATA_KEY_DISC_NUMBER = "android.media.metadata.DISC_NUMBER";
  
  public static final String METADATA_KEY_DISPLAY_DESCRIPTION = "android.media.metadata.DISPLAY_DESCRIPTION";
  
  public static final String METADATA_KEY_DISPLAY_ICON = "android.media.metadata.DISPLAY_ICON";
  
  public static final String METADATA_KEY_DISPLAY_ICON_URI = "android.media.metadata.DISPLAY_ICON_URI";
  
  public static final String METADATA_KEY_DISPLAY_SUBTITLE = "android.media.metadata.DISPLAY_SUBTITLE";
  
  public static final String METADATA_KEY_DISPLAY_TITLE = "android.media.metadata.DISPLAY_TITLE";
  
  public static final String METADATA_KEY_DURATION = "android.media.metadata.DURATION";
  
  public static final String METADATA_KEY_GENRE = "android.media.metadata.GENRE";
  
  public static final String METADATA_KEY_MEDIA_ID = "android.media.metadata.MEDIA_ID";
  
  public static final String METADATA_KEY_MEDIA_URI = "android.media.metadata.MEDIA_URI";
  
  public static final String METADATA_KEY_NUM_TRACKS = "android.media.metadata.NUM_TRACKS";
  
  public static final String METADATA_KEY_RATING = "android.media.metadata.RATING";
  
  public static final String METADATA_KEY_TITLE = "android.media.metadata.TITLE";
  
  public static final String METADATA_KEY_TRACK_NUMBER = "android.media.metadata.TRACK_NUMBER";
  
  public static final String METADATA_KEY_USER_RATING = "android.media.metadata.USER_RATING";
  
  public static final String METADATA_KEY_WRITER = "android.media.metadata.WRITER";
  
  public static final String METADATA_KEY_YEAR = "android.media.metadata.YEAR";
  
  private static final int METADATA_TYPE_BITMAP = 2;
  
  private static final int METADATA_TYPE_INVALID = -1;
  
  private static final int METADATA_TYPE_LONG = 0;
  
  private static final int METADATA_TYPE_RATING = 3;
  
  private static final int METADATA_TYPE_TEXT = 1;
  
  private static final String[] PREFERRED_BITMAP_ORDER;
  
  private static final String[] PREFERRED_DESCRIPTION_ORDER = new String[] { "android.media.metadata.TITLE", "android.media.metadata.ARTIST", "android.media.metadata.ALBUM", "android.media.metadata.ALBUM_ARTIST", "android.media.metadata.WRITER", "android.media.metadata.AUTHOR", "android.media.metadata.COMPOSER" };
  
  private static final String[] PREFERRED_URI_ORDER;
  
  private static final String TAG = "MediaMetadata";
  
  private final Bundle mBundle;
  
  private MediaDescription mDescription;
  
  static {
    PREFERRED_BITMAP_ORDER = new String[] { "android.media.metadata.DISPLAY_ICON", "android.media.metadata.ART", "android.media.metadata.ALBUM_ART" };
    PREFERRED_URI_ORDER = new String[] { "android.media.metadata.DISPLAY_ICON_URI", "android.media.metadata.ART_URI", "android.media.metadata.ALBUM_ART_URI" };
    ArrayMap<String, Integer> arrayMap1 = new ArrayMap();
    Integer integer2 = Integer.valueOf(1);
    arrayMap1.put("android.media.metadata.TITLE", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.ARTIST", integer2);
    ArrayMap<String, Integer> arrayMap2 = METADATA_KEYS_TYPE;
    Integer integer1 = Integer.valueOf(0);
    arrayMap2.put("android.media.metadata.DURATION", integer1);
    METADATA_KEYS_TYPE.put("android.media.metadata.ALBUM", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.AUTHOR", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.WRITER", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.COMPOSER", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.COMPILATION", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.DATE", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.YEAR", integer1);
    METADATA_KEYS_TYPE.put("android.media.metadata.GENRE", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.TRACK_NUMBER", integer1);
    METADATA_KEYS_TYPE.put("android.media.metadata.NUM_TRACKS", integer1);
    METADATA_KEYS_TYPE.put("android.media.metadata.DISC_NUMBER", integer1);
    METADATA_KEYS_TYPE.put("android.media.metadata.ALBUM_ARTIST", integer2);
    ArrayMap<String, Integer> arrayMap3 = METADATA_KEYS_TYPE;
    Integer integer3 = Integer.valueOf(2);
    arrayMap3.put("android.media.metadata.ART", integer3);
    METADATA_KEYS_TYPE.put("android.media.metadata.ART_URI", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.ALBUM_ART", integer3);
    METADATA_KEYS_TYPE.put("android.media.metadata.ALBUM_ART_URI", integer2);
    ArrayMap<String, Integer> arrayMap4 = METADATA_KEYS_TYPE;
    Integer integer4 = Integer.valueOf(3);
    arrayMap4.put("android.media.metadata.USER_RATING", integer4);
    METADATA_KEYS_TYPE.put("android.media.metadata.RATING", integer4);
    METADATA_KEYS_TYPE.put("android.media.metadata.DISPLAY_TITLE", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.DISPLAY_SUBTITLE", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.DISPLAY_DESCRIPTION", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.DISPLAY_ICON", integer3);
    METADATA_KEYS_TYPE.put("android.media.metadata.DISPLAY_ICON_URI", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.BT_FOLDER_TYPE", integer1);
    METADATA_KEYS_TYPE.put("android.media.metadata.MEDIA_ID", integer2);
    METADATA_KEYS_TYPE.put("android.media.metadata.MEDIA_URI", integer2);
    SparseArray<String> sparseArray = new SparseArray();
    sparseArray.put(100, "android.media.metadata.ART");
    EDITOR_KEY_MAPPING.put(101, "android.media.metadata.RATING");
    EDITOR_KEY_MAPPING.put(268435457, "android.media.metadata.USER_RATING");
    EDITOR_KEY_MAPPING.put(1, "android.media.metadata.ALBUM");
    EDITOR_KEY_MAPPING.put(13, "android.media.metadata.ALBUM_ARTIST");
    EDITOR_KEY_MAPPING.put(2, "android.media.metadata.ARTIST");
    EDITOR_KEY_MAPPING.put(3, "android.media.metadata.AUTHOR");
    EDITOR_KEY_MAPPING.put(0, "android.media.metadata.TRACK_NUMBER");
    EDITOR_KEY_MAPPING.put(4, "android.media.metadata.COMPOSER");
    EDITOR_KEY_MAPPING.put(15, "android.media.metadata.COMPILATION");
    EDITOR_KEY_MAPPING.put(5, "android.media.metadata.DATE");
    EDITOR_KEY_MAPPING.put(14, "android.media.metadata.DISC_NUMBER");
    EDITOR_KEY_MAPPING.put(9, "android.media.metadata.DURATION");
    EDITOR_KEY_MAPPING.put(6, "android.media.metadata.GENRE");
    EDITOR_KEY_MAPPING.put(10, "android.media.metadata.NUM_TRACKS");
    EDITOR_KEY_MAPPING.put(7, "android.media.metadata.TITLE");
    EDITOR_KEY_MAPPING.put(11, "android.media.metadata.WRITER");
    EDITOR_KEY_MAPPING.put(8, "android.media.metadata.YEAR");
    CREATOR = new Parcelable.Creator<MediaMetadata>() {
        public MediaMetadata createFromParcel(Parcel param1Parcel) {
          return new MediaMetadata(param1Parcel);
        }
        
        public MediaMetadata[] newArray(int param1Int) {
          return new MediaMetadata[param1Int];
        }
      };
  }
  
  private MediaMetadata(Bundle paramBundle) {
    this.mBundle = new Bundle(paramBundle);
  }
  
  private MediaMetadata(Parcel paramParcel) {
    this.mBundle = paramParcel.readBundle();
  }
  
  public boolean containsKey(String paramString) {
    return this.mBundle.containsKey(paramString);
  }
  
  public CharSequence getText(String paramString) {
    return this.mBundle.getCharSequence(paramString);
  }
  
  public String getString(String paramString) {
    CharSequence charSequence = getText(paramString);
    if (charSequence != null)
      return charSequence.toString(); 
    return null;
  }
  
  public long getLong(String paramString) {
    return this.mBundle.getLong(paramString, 0L);
  }
  
  public Rating getRating(String paramString) {
    Exception exception2 = null;
    try {
      Rating rating = this.mBundle.<Rating>getParcelable(paramString);
    } catch (Exception exception1) {
      Log.w("MediaMetadata", "Failed to retrieve a key as Rating.", exception1);
      exception1 = exception2;
    } 
    return (Rating)exception1;
  }
  
  public Bitmap getBitmap(String paramString) {
    Exception exception2 = null;
    try {
      Bitmap bitmap = this.mBundle.<Bitmap>getParcelable(paramString);
    } catch (Exception exception1) {
      Log.w("MediaMetadata", "Failed to retrieve a key as Bitmap.", exception1);
      exception1 = exception2;
    } 
    return (Bitmap)exception1;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBundle(this.mBundle);
  }
  
  public int size() {
    return this.mBundle.size();
  }
  
  public Set<String> keySet() {
    return this.mBundle.keySet();
  }
  
  public MediaDescription getDescription() {
    Bitmap bitmap;
    Uri uri1, uri2;
    MediaDescription mediaDescription2 = this.mDescription;
    if (mediaDescription2 != null)
      return mediaDescription2; 
    String str1 = getString("android.media.metadata.MEDIA_ID");
    CharSequence[] arrayOfCharSequence = new CharSequence[3];
    CharSequence charSequence2 = null;
    CharSequence charSequence3 = null;
    CharSequence charSequence1 = getText("android.media.metadata.DISPLAY_TITLE");
    if (!TextUtils.isEmpty(charSequence1)) {
      arrayOfCharSequence[0] = charSequence1;
      arrayOfCharSequence[1] = getText("android.media.metadata.DISPLAY_SUBTITLE");
      arrayOfCharSequence[2] = getText("android.media.metadata.DISPLAY_DESCRIPTION");
    } else {
      int i = 0;
      byte b1 = 0;
      while (i < arrayOfCharSequence.length) {
        String[] arrayOfString = PREFERRED_DESCRIPTION_ORDER;
        if (b1 < arrayOfString.length) {
          CharSequence charSequence = getText(arrayOfString[b1]);
          int j = i;
          if (!TextUtils.isEmpty(charSequence)) {
            arrayOfCharSequence[i] = charSequence;
            j = i + 1;
          } 
          b1++;
          i = j;
        } 
      } 
    } 
    byte b = 0;
    while (true) {
      String[] arrayOfString = PREFERRED_BITMAP_ORDER;
      charSequence1 = charSequence2;
      if (b < arrayOfString.length) {
        bitmap = getBitmap(arrayOfString[b]);
        if (bitmap != null)
          break; 
        b++;
        continue;
      } 
      break;
    } 
    b = 0;
    while (true) {
      String[] arrayOfString = PREFERRED_URI_ORDER;
      charSequence2 = charSequence3;
      if (b < arrayOfString.length) {
        charSequence2 = getString(arrayOfString[b]);
        if (!TextUtils.isEmpty(charSequence2)) {
          uri1 = Uri.parse((String)charSequence2);
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    charSequence3 = null;
    String str2 = getString("android.media.metadata.MEDIA_URI");
    if (!TextUtils.isEmpty(str2))
      uri2 = Uri.parse(str2); 
    MediaDescription.Builder builder = new MediaDescription.Builder();
    builder.setMediaId(str1);
    builder.setTitle(arrayOfCharSequence[0]);
    builder.setSubtitle(arrayOfCharSequence[1]);
    builder.setDescription(arrayOfCharSequence[2]);
    builder.setIconBitmap(bitmap);
    builder.setIconUri(uri1);
    builder.setMediaUri(uri2);
    if (this.mBundle.containsKey("android.media.metadata.BT_FOLDER_TYPE")) {
      Bundle bundle = new Bundle();
      long l = getLong("android.media.metadata.BT_FOLDER_TYPE");
      bundle.putLong("android.media.extra.BT_FOLDER_TYPE", l);
      builder.setExtras(bundle);
    } 
    MediaDescription mediaDescription1 = builder.build();
    return mediaDescription1;
  }
  
  public static String getKeyFromMetadataEditorKey(int paramInt) {
    return (String)EDITOR_KEY_MAPPING.get(paramInt, null);
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof MediaMetadata))
      return false; 
    MediaMetadata mediaMetadata = (MediaMetadata)paramObject;
    for (byte b = 0; b < METADATA_KEYS_TYPE.size(); b++) {
      paramObject = METADATA_KEYS_TYPE.keyAt(b);
      int i = ((Integer)METADATA_KEYS_TYPE.valueAt(b)).intValue();
      if (i != 0) {
        if (i == 1)
          if (!Objects.equals(getString((String)paramObject), mediaMetadata.getString((String)paramObject)))
            return false;  
      } else if (getLong((String)paramObject) != mediaMetadata.getLong((String)paramObject)) {
        return false;
      } 
    } 
    return true;
  }
  
  public int hashCode() {
    int i = 17;
    for (byte b = 0; b < METADATA_KEYS_TYPE.size(); b++) {
      String str = (String)METADATA_KEYS_TYPE.keyAt(b);
      int j = ((Integer)METADATA_KEYS_TYPE.valueAt(b)).intValue();
      if (j != 0) {
        if (j == 1) {
          j = Objects.hash(new Object[] { getString(str) });
          i = i * 31 + j;
        } 
      } else {
        j = Long.hashCode(getLong(str));
        i = i * 31 + j;
      } 
    } 
    return i;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class BitmapKey implements Annotation {}
  
  class Builder {
    private final Bundle mBundle;
    
    public Builder() {
      this.mBundle = new Bundle();
    }
    
    public Builder(MediaMetadata this$0) {
      this.mBundle = new Bundle(MediaMetadata.this.mBundle);
    }
    
    public Builder(int param1Int) {
      this();
      for (String str : this.mBundle.keySet()) {
        Object object = this.mBundle.get(str);
        if (object != null && object instanceof Bitmap) {
          object = object;
          if (object.getHeight() > param1Int || object.getWidth() > param1Int)
            putBitmap(str, scaleBitmap((Bitmap)object, param1Int)); 
        } 
      } 
    }
    
    public Builder putText(String param1String, CharSequence param1CharSequence) {
      if (!MediaMetadata.METADATA_KEYS_TYPE.containsKey(param1String) || (
        (Integer)MediaMetadata.METADATA_KEYS_TYPE.get(param1String)).intValue() == 1) {
        this.mBundle.putCharSequence(param1String, param1CharSequence);
        return this;
      } 
      param1CharSequence = new StringBuilder();
      param1CharSequence.append("The ");
      param1CharSequence.append(param1String);
      param1CharSequence.append(" key cannot be used to put a CharSequence");
      throw new IllegalArgumentException(param1CharSequence.toString());
    }
    
    public Builder putString(String param1String1, String param1String2) {
      if (!MediaMetadata.METADATA_KEYS_TYPE.containsKey(param1String1) || (
        (Integer)MediaMetadata.METADATA_KEYS_TYPE.get(param1String1)).intValue() == 1) {
        this.mBundle.putCharSequence(param1String1, param1String2);
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The ");
      stringBuilder.append(param1String1);
      stringBuilder.append(" key cannot be used to put a String");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder putLong(String param1String, long param1Long) {
      if (!MediaMetadata.METADATA_KEYS_TYPE.containsKey(param1String) || (
        (Integer)MediaMetadata.METADATA_KEYS_TYPE.get(param1String)).intValue() == 0) {
        this.mBundle.putLong(param1String, param1Long);
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The ");
      stringBuilder.append(param1String);
      stringBuilder.append(" key cannot be used to put a long");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder putRating(String param1String, Rating param1Rating) {
      if (!MediaMetadata.METADATA_KEYS_TYPE.containsKey(param1String) || (
        (Integer)MediaMetadata.METADATA_KEYS_TYPE.get(param1String)).intValue() == 3) {
        this.mBundle.putParcelable(param1String, param1Rating);
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The ");
      stringBuilder.append(param1String);
      stringBuilder.append(" key cannot be used to put a Rating");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder putBitmap(String param1String, Bitmap param1Bitmap) {
      if (!MediaMetadata.METADATA_KEYS_TYPE.containsKey(param1String) || (
        (Integer)MediaMetadata.METADATA_KEYS_TYPE.get(param1String)).intValue() == 2) {
        this.mBundle.putParcelable(param1String, (Parcelable)param1Bitmap);
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The ");
      stringBuilder.append(param1String);
      stringBuilder.append(" key cannot be used to put a Bitmap");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public MediaMetadata build() {
      return new MediaMetadata(this.mBundle);
    }
    
    private Bitmap scaleBitmap(Bitmap param1Bitmap, int param1Int) {
      float f1 = param1Int;
      float f2 = f1 / param1Bitmap.getWidth();
      f1 /= param1Bitmap.getHeight();
      f2 = Math.min(f2, f1);
      int i = (int)(param1Bitmap.getHeight() * f2);
      param1Int = (int)(param1Bitmap.getWidth() * f2);
      return Bitmap.createScaledBitmap(param1Bitmap, param1Int, i, true);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class LongKey implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RatingKey implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TextKey implements Annotation {}
}
