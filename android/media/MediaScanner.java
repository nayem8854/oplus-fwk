package android.media;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.RemoteException;

@Deprecated
public class MediaScanner implements AutoCloseable {
  @Deprecated
  private static final String[] FILES_PRESCAN_PROJECTION = new String[0];
  
  @Deprecated
  private final Uri mAudioUri;
  
  private static class FileEntry {
    @Deprecated
    boolean mLastModifiedChanged;
    
    @Deprecated
    long mRowId;
    
    @Deprecated
    FileEntry(long param1Long1, String param1String, long param1Long2, int param1Int) {
      throw new UnsupportedOperationException();
    }
  }
  
  @Deprecated
  public MediaScanner(Context paramContext, String paramString) {
    throw new UnsupportedOperationException();
  }
  
  @Deprecated
  private final MyMediaScannerClient mClient = new MyMediaScannerClient();
  
  @Deprecated
  private final Context mContext;
  
  @Deprecated
  private String mDefaultAlarmAlertFilename;
  
  @Deprecated
  private String mDefaultNotificationFilename;
  
  @Deprecated
  private String mDefaultRingtoneFilename;
  
  @Deprecated
  private final Uri mFilesUri;
  
  @Deprecated
  private MediaInserter mMediaInserter;
  
  @Deprecated
  private final String mPackageName;
  
  @Deprecated
  private boolean isDrmEnabled() {
    throw new UnsupportedOperationException();
  }
  
  class MyMediaScannerClient implements MediaScannerClient {
    @Deprecated
    private int mFileType;
    
    @Deprecated
    private boolean mIsDrm;
    
    @Deprecated
    private String mMimeType;
    
    @Deprecated
    private boolean mNoMedia;
    
    @Deprecated
    private String mPath;
    
    final MediaScanner this$0;
    
    public MyMediaScannerClient() {
      throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public MediaScanner.FileEntry beginFile(String param1String1, String param1String2, long param1Long1, long param1Long2, boolean param1Boolean1, boolean param1Boolean2) {
      throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public void scanFile(String param1String, long param1Long1, long param1Long2, boolean param1Boolean1, boolean param1Boolean2) {
      throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public Uri doScanFile(String param1String1, String param1String2, long param1Long1, long param1Long2, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) {
      throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public void handleStringTag(String param1String1, String param1String2) {
      throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public void setMimeType(String param1String) {
      throw new UnsupportedOperationException();
    }
    
    @Deprecated
    private ContentValues toValues() {
      throw new UnsupportedOperationException();
    }
    
    @Deprecated
    private Uri endFile(MediaScanner.FileEntry param1FileEntry, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, boolean param1Boolean5, boolean param1Boolean6) throws RemoteException {
      throw new UnsupportedOperationException();
    }
    
    @Deprecated
    private int getFileTypeFromDrm(String param1String) {
      throw new UnsupportedOperationException();
    }
  }
  
  @Deprecated
  private void prescan(String paramString, boolean paramBoolean) throws RemoteException {
    throw new UnsupportedOperationException();
  }
  
  @Deprecated
  private void postscan(String[] paramArrayOfString) throws RemoteException {
    throw new UnsupportedOperationException();
  }
  
  @Deprecated
  public Uri scanSingleFile(String paramString1, String paramString2) {
    throw new UnsupportedOperationException();
  }
  
  @Deprecated
  public static boolean isNoMediaPath(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  @Deprecated
  FileEntry makeEntryFor(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  @Deprecated
  private void setLocale(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public void close() {
    throw new UnsupportedOperationException();
  }
}
