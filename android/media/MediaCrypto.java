package android.media;

import java.util.UUID;

public final class MediaCrypto {
  private long mNativeContext;
  
  public static final boolean isCryptoSchemeSupported(UUID paramUUID) {
    return isCryptoSchemeSupportedNative(getByteArrayFromUUID(paramUUID));
  }
  
  private static final byte[] getByteArrayFromUUID(UUID paramUUID) {
    long l1 = paramUUID.getMostSignificantBits();
    long l2 = paramUUID.getLeastSignificantBits();
    byte[] arrayOfByte = new byte[16];
    for (byte b = 0; b < 8; b++) {
      arrayOfByte[b] = (byte)(int)(l1 >>> (7 - b) * 8);
      arrayOfByte[b + 8] = (byte)(int)(l2 >>> (7 - b) * 8);
    } 
    return arrayOfByte;
  }
  
  public MediaCrypto(UUID paramUUID, byte[] paramArrayOfbyte) throws MediaCryptoException {
    native_setup(getByteArrayFromUUID(paramUUID), paramArrayOfbyte);
  }
  
  protected void finalize() {
    native_finalize();
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private static final native boolean isCryptoSchemeSupportedNative(byte[] paramArrayOfbyte);
  
  private final native void native_finalize();
  
  private static final native void native_init();
  
  private final native void native_setup(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws MediaCryptoException;
  
  public final native void release();
  
  public final native boolean requiresSecureDecoderComponent(String paramString);
  
  public final native void setMediaDrmSession(byte[] paramArrayOfbyte) throws MediaCryptoException;
}
