package android.media;

import android.content.Context;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.CaptioningManager;
import android.widget.LinearLayout;
import com.android.internal.widget.SubtitleView;
import java.util.ArrayList;
import java.util.Vector;

class WebVttRenderingWidget extends ViewGroup implements SubtitleTrack.RenderingWidget {
  private static final CaptioningManager.CaptionStyle DEFAULT_CAPTION_STYLE = CaptioningManager.CaptionStyle.DEFAULT;
  
  private final ArrayMap<TextTrackRegion, RegionLayout> mRegionBoxes = new ArrayMap();
  
  private final ArrayMap<TextTrackCue, CueLayout> mCueBoxes = new ArrayMap();
  
  private static final boolean DEBUG = false;
  
  private static final int DEBUG_CUE_BACKGROUND = -2130771968;
  
  private static final int DEBUG_REGION_BACKGROUND = -2147483393;
  
  private static final float LINE_HEIGHT_RATIO = 0.0533F;
  
  private CaptioningManager.CaptionStyle mCaptionStyle;
  
  private final CaptioningManager.CaptioningChangeListener mCaptioningListener;
  
  private float mFontSize;
  
  private boolean mHasChangeListener;
  
  private SubtitleTrack.RenderingWidget.OnChangedListener mListener;
  
  private final CaptioningManager mManager;
  
  public WebVttRenderingWidget(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public WebVttRenderingWidget(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public WebVttRenderingWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public WebVttRenderingWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mCaptioningListener = (CaptioningManager.CaptioningChangeListener)new Object(this);
    setLayerType(1, null);
    CaptioningManager captioningManager = (CaptioningManager)paramContext.getSystemService("captioning");
    this.mCaptionStyle = captioningManager.getUserStyle();
    this.mFontSize = this.mManager.getFontScale() * getHeight() * 0.0533F;
  }
  
  public void setSize(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    int j = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    measure(i, j);
    layout(0, 0, paramInt1, paramInt2);
  }
  
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    manageChangeListener();
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    manageChangeListener();
  }
  
  public void setOnChangedListener(SubtitleTrack.RenderingWidget.OnChangedListener paramOnChangedListener) {
    this.mListener = paramOnChangedListener;
  }
  
  public void setVisible(boolean paramBoolean) {
    if (paramBoolean) {
      setVisibility(0);
    } else {
      setVisibility(8);
    } 
    manageChangeListener();
  }
  
  private void manageChangeListener() {
    boolean bool;
    if (isAttachedToWindow() && getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mHasChangeListener != bool) {
      this.mHasChangeListener = bool;
      if (bool) {
        this.mManager.addCaptioningChangeListener(this.mCaptioningListener);
        CaptioningManager.CaptionStyle captionStyle = this.mManager.getUserStyle();
        float f1 = this.mManager.getFontScale(), f2 = getHeight();
        setCaptionStyle(captionStyle, f1 * f2 * 0.0533F);
      } else {
        this.mManager.removeCaptioningChangeListener(this.mCaptioningListener);
      } 
    } 
  }
  
  public void setActiveCues(Vector<SubtitleTrack.Cue> paramVector) {
    Context context = getContext();
    CaptioningManager.CaptionStyle captionStyle = this.mCaptionStyle;
    float f = this.mFontSize;
    prepForPrune();
    int i = paramVector.size();
    int j;
    for (j = 0; j < i; j++) {
      TextTrackCue textTrackCue = (TextTrackCue)paramVector.get(j);
      TextTrackRegion textTrackRegion = textTrackCue.mRegion;
      if (textTrackRegion != null) {
        RegionLayout regionLayout1 = (RegionLayout)this.mRegionBoxes.get(textTrackRegion);
        RegionLayout regionLayout2 = regionLayout1;
        if (regionLayout1 == null) {
          regionLayout2 = new RegionLayout(context, textTrackRegion, captionStyle, f);
          this.mRegionBoxes.put(textTrackRegion, regionLayout2);
          addView((View)regionLayout2, -2, -2);
        } 
        regionLayout2.put(textTrackCue);
      } else {
        CueLayout cueLayout1 = (CueLayout)this.mCueBoxes.get(textTrackCue);
        CueLayout cueLayout2 = cueLayout1;
        if (cueLayout1 == null) {
          cueLayout2 = new CueLayout(context, textTrackCue, captionStyle, f);
          this.mCueBoxes.put(textTrackCue, cueLayout2);
          addView((View)cueLayout2, -2, -2);
        } 
        cueLayout2.update();
        cueLayout2.setOrder(j);
      } 
    } 
    prune();
    j = getWidth();
    i = getHeight();
    setSize(j, i);
    SubtitleTrack.RenderingWidget.OnChangedListener onChangedListener = this.mListener;
    if (onChangedListener != null)
      onChangedListener.onChanged(this); 
  }
  
  private void setCaptionStyle(CaptioningManager.CaptionStyle paramCaptionStyle, float paramFloat) {
    paramCaptionStyle = DEFAULT_CAPTION_STYLE.applyStyle(paramCaptionStyle);
    this.mCaptionStyle = paramCaptionStyle;
    this.mFontSize = paramFloat;
    int i = this.mCueBoxes.size();
    byte b;
    for (b = 0; b < i; b++) {
      CueLayout cueLayout = (CueLayout)this.mCueBoxes.valueAt(b);
      cueLayout.setCaptionStyle(paramCaptionStyle, paramFloat);
    } 
    i = this.mRegionBoxes.size();
    for (b = 0; b < i; b++) {
      RegionLayout regionLayout = (RegionLayout)this.mRegionBoxes.valueAt(b);
      regionLayout.setCaptionStyle(paramCaptionStyle, paramFloat);
    } 
  }
  
  private void prune() {
    int i = this.mRegionBoxes.size();
    int j;
    for (j = 0; j < i; j = n + 1, i = m) {
      RegionLayout regionLayout = (RegionLayout)this.mRegionBoxes.valueAt(j);
      int m = i, n = j;
      if (regionLayout.prune()) {
        removeView((View)regionLayout);
        this.mRegionBoxes.removeAt(j);
        m = i - 1;
        n = j - 1;
      } 
    } 
    int k = this.mCueBoxes.size();
    for (j = 0; j < k; j = i + 1, k = m) {
      CueLayout cueLayout = (CueLayout)this.mCueBoxes.valueAt(j);
      int m = k;
      i = j;
      if (!cueLayout.isActive()) {
        removeView((View)cueLayout);
        this.mCueBoxes.removeAt(j);
        m = k - 1;
        i = j - 1;
      } 
    } 
  }
  
  private void prepForPrune() {
    int i = this.mRegionBoxes.size();
    byte b;
    for (b = 0; b < i; b++) {
      RegionLayout regionLayout = (RegionLayout)this.mRegionBoxes.valueAt(b);
      regionLayout.prepForPrune();
    } 
    i = this.mCueBoxes.size();
    for (b = 0; b < i; b++) {
      CueLayout cueLayout = (CueLayout)this.mCueBoxes.valueAt(b);
      cueLayout.prepForPrune();
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    int i = this.mRegionBoxes.size();
    byte b;
    for (b = 0; b < i; b++) {
      RegionLayout regionLayout = (RegionLayout)this.mRegionBoxes.valueAt(b);
      regionLayout.measureForParent(paramInt1, paramInt2);
    } 
    i = this.mCueBoxes.size();
    for (b = 0; b < i; b++) {
      CueLayout cueLayout = (CueLayout)this.mCueBoxes.valueAt(b);
      cueLayout.measureForParent(paramInt1, paramInt2);
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt3 -= paramInt1;
    paramInt2 = paramInt4 - paramInt2;
    CaptioningManager.CaptionStyle captionStyle = this.mCaptionStyle;
    CaptioningManager captioningManager = this.mManager;
    float f1 = captioningManager.getFontScale(), f2 = paramInt2;
    setCaptionStyle(captionStyle, f1 * 0.0533F * f2);
    paramInt4 = this.mRegionBoxes.size();
    for (paramInt1 = 0; paramInt1 < paramInt4; paramInt1++) {
      RegionLayout regionLayout = (RegionLayout)this.mRegionBoxes.valueAt(paramInt1);
      layoutRegion(paramInt3, paramInt2, regionLayout);
    } 
    paramInt4 = this.mCueBoxes.size();
    for (paramInt1 = 0; paramInt1 < paramInt4; paramInt1++) {
      CueLayout cueLayout = (CueLayout)this.mCueBoxes.valueAt(paramInt1);
      layoutCue(paramInt3, paramInt2, cueLayout);
    } 
  }
  
  private void layoutRegion(int paramInt1, int paramInt2, RegionLayout paramRegionLayout) {
    TextTrackRegion textTrackRegion = paramRegionLayout.getRegion();
    int i = paramRegionLayout.getMeasuredHeight();
    int j = paramRegionLayout.getMeasuredWidth();
    float f1 = textTrackRegion.mViewportAnchorPointX;
    float f2 = textTrackRegion.mViewportAnchorPointY;
    paramInt1 = (int)((paramInt1 - j) * f1 / 100.0F);
    paramInt2 = (int)((paramInt2 - i) * f2 / 100.0F);
    paramRegionLayout.layout(paramInt1, paramInt2, paramInt1 + j, paramInt2 + i);
  }
  
  private void layoutCue(int paramInt1, int paramInt2, CueLayout paramCueLayout) {
    TextTrackCue textTrackCue = paramCueLayout.getCue();
    int i = getLayoutDirection();
    int j = resolveCueAlignment(i, textTrackCue.mAlignment);
    boolean bool = textTrackCue.mSnapToLines;
    int k = paramCueLayout.getMeasuredWidth() * 100 / paramInt1;
    if (j != 203) {
      if (j != 204) {
        m = textTrackCue.mTextPosition - k / 2;
      } else {
        m = textTrackCue.mTextPosition - k;
      } 
    } else {
      m = textTrackCue.mTextPosition;
    } 
    j = m;
    if (i == 1)
      j = 100 - m; 
    int n = k, i1 = j;
    if (bool) {
      n = getPaddingLeft() * 100 / paramInt1;
      int i2 = getPaddingRight() * 100 / paramInt1;
      i = k;
      m = j;
      if (j < n) {
        i = k;
        m = j;
        if (j + k > n) {
          m = j + n;
          i = k - n;
        } 
      } 
      float f = (100 - i2);
      n = i;
      i1 = m;
      if (m < f) {
        n = i;
        i1 = m;
        if ((m + i) > f) {
          n = i - i2;
          i1 = m;
        } 
      } 
    } 
    j = i1 * paramInt1 / 100;
    i = n * paramInt1 / 100;
    paramInt1 = calculateLinePosition(paramCueLayout);
    int m = paramCueLayout.getMeasuredHeight();
    if (paramInt1 < 0) {
      paramInt1 = paramInt2 + paramInt1 * m;
    } else {
      paramInt1 = (paramInt2 - m) * paramInt1 / 100;
    } 
    paramCueLayout.layout(j, paramInt1, j + i, paramInt1 + m);
  }
  
  private int calculateLinePosition(CueLayout paramCueLayout) {
    boolean bool1;
    TextTrackCue textTrackCue = paramCueLayout.getCue();
    Integer integer = textTrackCue.mLinePosition;
    boolean bool = textTrackCue.mSnapToLines;
    if (integer == null) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (!bool && !bool1 && (integer.intValue() < 0 || integer.intValue() > 100))
      return 100; 
    if (!bool1)
      return integer.intValue(); 
    if (!bool)
      return 100; 
    return -(paramCueLayout.mOrder + 1);
  }
  
  private static int resolveCueAlignment(int paramInt1, int paramInt2) {
    char c = 'Ë';
    if (paramInt2 != 201) {
      if (paramInt2 != 202)
        return paramInt2; 
      if (paramInt1 == 0)
        c = 'Ì'; 
      return c;
    } 
    if (paramInt1 != 0)
      c = 'Ì'; 
    return c;
  }
  
  class RegionLayout extends LinearLayout {
    private CaptioningManager.CaptionStyle mCaptionStyle;
    
    private float mFontSize;
    
    private final TextTrackRegion mRegion;
    
    private final ArrayList<WebVttRenderingWidget.CueLayout> mRegionCueBoxes = new ArrayList<>();
    
    public RegionLayout(WebVttRenderingWidget this$0, TextTrackRegion param1TextTrackRegion, CaptioningManager.CaptionStyle param1CaptionStyle, float param1Float) {
      super((Context)this$0);
      this.mRegion = param1TextTrackRegion;
      this.mCaptionStyle = param1CaptionStyle;
      this.mFontSize = param1Float;
      setOrientation(1);
      setBackgroundColor(param1CaptionStyle.windowColor);
    }
    
    public void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle, float param1Float) {
      this.mCaptionStyle = param1CaptionStyle;
      this.mFontSize = param1Float;
      int i = this.mRegionCueBoxes.size();
      for (byte b = 0; b < i; b++) {
        WebVttRenderingWidget.CueLayout cueLayout = this.mRegionCueBoxes.get(b);
        cueLayout.setCaptionStyle(param1CaptionStyle, param1Float);
      } 
      setBackgroundColor(param1CaptionStyle.windowColor);
    }
    
    public void measureForParent(int param1Int1, int param1Int2) {
      TextTrackRegion textTrackRegion = this.mRegion;
      int i = View.MeasureSpec.getSize(param1Int1);
      param1Int1 = View.MeasureSpec.getSize(param1Int2);
      param1Int2 = (int)textTrackRegion.mWidth;
      param1Int2 = param1Int2 * i / 100;
      param1Int2 = View.MeasureSpec.makeMeasureSpec(param1Int2, -2147483648);
      param1Int1 = View.MeasureSpec.makeMeasureSpec(param1Int1, -2147483648);
      measure(param1Int2, param1Int1);
    }
    
    public void prepForPrune() {
      int i = this.mRegionCueBoxes.size();
      for (byte b = 0; b < i; b++) {
        WebVttRenderingWidget.CueLayout cueLayout = this.mRegionCueBoxes.get(b);
        cueLayout.prepForPrune();
      } 
    }
    
    public void put(TextTrackCue param1TextTrackCue) {
      int i = this.mRegionCueBoxes.size();
      for (byte b = 0; b < i; b++) {
        WebVttRenderingWidget.CueLayout cueLayout1 = this.mRegionCueBoxes.get(b);
        if (cueLayout1.getCue() == param1TextTrackCue) {
          cueLayout1.update();
          return;
        } 
      } 
      WebVttRenderingWidget.CueLayout cueLayout = new WebVttRenderingWidget.CueLayout(getContext(), param1TextTrackCue, this.mCaptionStyle, this.mFontSize);
      this.mRegionCueBoxes.add(cueLayout);
      addView((View)cueLayout, -2, -2);
      if (getChildCount() > this.mRegion.mLines)
        removeViewAt(0); 
    }
    
    public boolean prune() {
      int i = this.mRegionCueBoxes.size();
      for (int j = 0; j < i; j = m + 1, i = k) {
        WebVttRenderingWidget.CueLayout cueLayout = this.mRegionCueBoxes.get(j);
        int k = i, m = j;
        if (!cueLayout.isActive()) {
          this.mRegionCueBoxes.remove(j);
          removeView((View)cueLayout);
          k = i - 1;
          m = j - 1;
        } 
      } 
      return this.mRegionCueBoxes.isEmpty();
    }
    
    public TextTrackRegion getRegion() {
      return this.mRegion;
    }
  }
  
  class CueLayout extends LinearLayout {
    private boolean mActive;
    
    private CaptioningManager.CaptionStyle mCaptionStyle;
    
    public final TextTrackCue mCue;
    
    private float mFontSize;
    
    private int mOrder;
    
    public CueLayout(WebVttRenderingWidget this$0, TextTrackCue param1TextTrackCue, CaptioningManager.CaptionStyle param1CaptionStyle, float param1Float) {
      super((Context)this$0);
      this.mCue = param1TextTrackCue;
      this.mCaptionStyle = param1CaptionStyle;
      this.mFontSize = param1Float;
      int i = param1TextTrackCue.mWritingDirection;
      boolean bool1 = false, bool2 = true;
      if (i == 100) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i != 0)
        bool1 = true; 
      setOrientation(bool1);
      switch (param1TextTrackCue.mAlignment) {
        case 204:
          setGravity(5);
          break;
        case 203:
          setGravity(3);
          break;
        case 202:
          setGravity(8388613);
          break;
        case 201:
          setGravity(8388611);
          break;
        case 200:
          if (i != 0) {
            i = bool2;
          } else {
            i = 16;
          } 
          setGravity(i);
          break;
      } 
      update();
    }
    
    public void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle, float param1Float) {
      this.mCaptionStyle = param1CaptionStyle;
      this.mFontSize = param1Float;
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        if (view instanceof WebVttRenderingWidget.SpanLayout)
          ((WebVttRenderingWidget.SpanLayout)view).setCaptionStyle(param1CaptionStyle, param1Float); 
      } 
    }
    
    public void prepForPrune() {
      this.mActive = false;
    }
    
    public void update() {
      Layout.Alignment alignment;
      this.mActive = true;
      removeAllViews();
      int i = WebVttRenderingWidget.resolveCueAlignment(getLayoutDirection(), this.mCue.mAlignment);
      if (i != 203) {
        if (i != 204) {
          alignment = Layout.Alignment.ALIGN_CENTER;
        } else {
          alignment = Layout.Alignment.ALIGN_RIGHT;
        } 
      } else {
        alignment = Layout.Alignment.ALIGN_LEFT;
      } 
      CaptioningManager.CaptionStyle captionStyle = this.mCaptionStyle;
      float f = this.mFontSize;
      TextTrackCueSpan[][] arrayOfTextTrackCueSpan = this.mCue.mLines;
      int j = arrayOfTextTrackCueSpan.length;
      for (i = 0; i < j; i++) {
        WebVttRenderingWidget.SpanLayout spanLayout = new WebVttRenderingWidget.SpanLayout(getContext(), arrayOfTextTrackCueSpan[i]);
        spanLayout.setAlignment(alignment);
        spanLayout.setCaptionStyle(captionStyle, f);
        addView((View)spanLayout, -2, -2);
      } 
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      super.onMeasure(param1Int1, param1Int2);
    }
    
    public void measureForParent(int param1Int1, int param1Int2) {
      TextTrackCue textTrackCue = this.mCue;
      int i = View.MeasureSpec.getSize(param1Int1);
      param1Int2 = View.MeasureSpec.getSize(param1Int2);
      param1Int1 = getLayoutDirection();
      param1Int1 = WebVttRenderingWidget.resolveCueAlignment(param1Int1, textTrackCue.mAlignment);
      if (param1Int1 != 200) {
        if (param1Int1 != 203) {
          if (param1Int1 != 204) {
            param1Int1 = 0;
          } else {
            param1Int1 = textTrackCue.mTextPosition;
          } 
        } else {
          param1Int1 = 100 - textTrackCue.mTextPosition;
        } 
      } else if (textTrackCue.mTextPosition <= 50) {
        param1Int1 = textTrackCue.mTextPosition * 2;
      } else {
        param1Int1 = (100 - textTrackCue.mTextPosition) * 2;
      } 
      param1Int1 = Math.min(textTrackCue.mSize, param1Int1) * i / 100;
      param1Int1 = View.MeasureSpec.makeMeasureSpec(param1Int1, -2147483648);
      param1Int2 = View.MeasureSpec.makeMeasureSpec(param1Int2, -2147483648);
      measure(param1Int1, param1Int2);
    }
    
    public void setOrder(int param1Int) {
      this.mOrder = param1Int;
    }
    
    public boolean isActive() {
      return this.mActive;
    }
    
    public TextTrackCue getCue() {
      return this.mCue;
    }
  }
  
  private static class SpanLayout extends SubtitleView {
    private final SpannableStringBuilder mBuilder = new SpannableStringBuilder();
    
    private final TextTrackCueSpan[] mSpans;
    
    public SpanLayout(Context param1Context, TextTrackCueSpan[] param1ArrayOfTextTrackCueSpan) {
      super(param1Context);
      this.mSpans = param1ArrayOfTextTrackCueSpan;
      update();
    }
    
    public void update() {
      SpannableStringBuilder spannableStringBuilder = this.mBuilder;
      TextTrackCueSpan[] arrayOfTextTrackCueSpan = this.mSpans;
      spannableStringBuilder.clear();
      spannableStringBuilder.clearSpans();
      int i = arrayOfTextTrackCueSpan.length;
      for (byte b = 0; b < i; b++) {
        TextTrackCueSpan textTrackCueSpan = arrayOfTextTrackCueSpan[b];
        if (textTrackCueSpan.mEnabled)
          spannableStringBuilder.append((arrayOfTextTrackCueSpan[b]).mText); 
      } 
      setText((CharSequence)spannableStringBuilder);
    }
    
    public void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle, float param1Float) {
      setBackgroundColor(param1CaptionStyle.backgroundColor);
      setForegroundColor(param1CaptionStyle.foregroundColor);
      setEdgeColor(param1CaptionStyle.edgeColor);
      setEdgeType(param1CaptionStyle.edgeType);
      setTypeface(param1CaptionStyle.getTypeface());
      setTextSize(param1Float);
    }
  }
}
