package android.media;

import dalvik.system.CloseGuard;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.ByteBuffer;
import java.util.Map;

public final class MediaMuxer {
  static {
    System.loadLibrary("media_jni");
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Format {}
  
  public static final class OutputFormat {
    public static final int MUXER_OUTPUT_3GPP = 2;
    
    public static final int MUXER_OUTPUT_FIRST = 0;
    
    public static final int MUXER_OUTPUT_HEIF = 3;
    
    public static final int MUXER_OUTPUT_LAST = 4;
    
    public static final int MUXER_OUTPUT_MPEG_4 = 0;
    
    public static final int MUXER_OUTPUT_OGG = 4;
    
    public static final int MUXER_OUTPUT_WEBM = 1;
  }
  
  private int mState = -1;
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private int mLastTrackIndex = -1;
  
  private static final int MUXER_STATE_INITIALIZED = 0;
  
  private static final int MUXER_STATE_STARTED = 1;
  
  private static final int MUXER_STATE_STOPPED = 2;
  
  private static final int MUXER_STATE_UNINITIALIZED = -1;
  
  private long mNativeObject;
  
  private String convertMuxerStateCodeToString(int paramInt) {
    if (paramInt != -1) {
      if (paramInt != 0) {
        if (paramInt != 1) {
          if (paramInt != 2)
            return "UNKNOWN"; 
          return "STOPPED";
        } 
        return "STARTED";
      } 
      return "INITIALIZED";
    } 
    return "UNINITIALIZED";
  }
  
  public MediaMuxer(String paramString, int paramInt) throws IOException {
    if (paramString != null) {
      RandomAccessFile randomAccessFile1 = null;
      RandomAccessFile randomAccessFile2 = randomAccessFile1;
      try {
        RandomAccessFile randomAccessFile4 = new RandomAccessFile();
        randomAccessFile2 = randomAccessFile1;
        this(paramString, "rws");
        RandomAccessFile randomAccessFile3 = randomAccessFile4;
        randomAccessFile2 = randomAccessFile3;
        randomAccessFile3.setLength(0L);
        randomAccessFile2 = randomAccessFile3;
        FileDescriptor fileDescriptor = randomAccessFile3.getFD();
        randomAccessFile2 = randomAccessFile3;
        setUpMediaMuxer(fileDescriptor, paramInt);
        return;
      } finally {
        if (randomAccessFile2 != null)
          randomAccessFile2.close(); 
      } 
    } 
    throw new IllegalArgumentException("path must not be null");
  }
  
  public MediaMuxer(FileDescriptor paramFileDescriptor, int paramInt) throws IOException {
    setUpMediaMuxer(paramFileDescriptor, paramInt);
  }
  
  private void setUpMediaMuxer(FileDescriptor paramFileDescriptor, int paramInt) throws IOException {
    if (paramInt >= 0 && paramInt <= 4) {
      this.mNativeObject = nativeSetup(paramFileDescriptor, paramInt);
      this.mState = 0;
      this.mCloseGuard.open("release");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("format: ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" is invalid");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setOrientationHint(int paramInt) {
    if (paramInt == 0 || paramInt == 90 || paramInt == 180 || paramInt == 270) {
      if (this.mState == 0) {
        nativeSetOrientationHint(this.mNativeObject, paramInt);
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Can't set rotation degrees due to wrong state(");
      paramInt = this.mState;
      stringBuilder1.append(convertMuxerStateCodeToString(paramInt));
      stringBuilder1.append(")");
      throw new IllegalStateException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported angle: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setLocation(float paramFloat1, float paramFloat2) {
    int i = (int)((paramFloat1 * 10000.0F) + 0.5D);
    int j = (int)((10000.0F * paramFloat2) + 0.5D);
    if (i <= 900000 && i >= -900000) {
      if (j <= 1800000 && j >= -1800000) {
        if (this.mState == 0) {
          long l = this.mNativeObject;
          if (l != 0L) {
            nativeSetLocation(l, i, j);
            return;
          } 
        } 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Can't set location due to wrong state(");
        i = this.mState;
        stringBuilder2.append(convertMuxerStateCodeToString(i));
        stringBuilder2.append(")");
        throw new IllegalStateException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Longitude: ");
      stringBuilder1.append(paramFloat2);
      stringBuilder1.append(" out of range");
      String str1 = stringBuilder1.toString();
      throw new IllegalArgumentException(str1);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Latitude: ");
    stringBuilder.append(paramFloat1);
    stringBuilder.append(" out of range.");
    String str = stringBuilder.toString();
    throw new IllegalArgumentException(str);
  }
  
  public void start() {
    long l = this.mNativeObject;
    if (l != 0L) {
      if (this.mState == 0) {
        nativeStart(l);
        this.mState = 1;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Can't start due to wrong state(");
      int i = this.mState;
      stringBuilder.append(convertMuxerStateCodeToString(i));
      stringBuilder.append(")");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    throw new IllegalStateException("Muxer has been released!");
  }
  
  public void stop() {
    if (this.mState == 1) {
      Exception exception;
      try {
        nativeStop(this.mNativeObject);
        this.mState = 2;
        return;
      } catch (Exception null) {
        throw exception;
      } finally {}
      this.mState = 2;
      throw exception;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Can't stop due to wrong state(");
    int i = this.mState;
    stringBuilder.append(convertMuxerStateCodeToString(i));
    stringBuilder.append(")");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      if (this.mNativeObject != 0L) {
        nativeRelease(this.mNativeObject);
        this.mNativeObject = 0L;
      } 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public int addTrack(MediaFormat paramMediaFormat) {
    if (paramMediaFormat != null) {
      if (this.mState == 0) {
        if (this.mNativeObject != 0L) {
          Map<String, Object> map = paramMediaFormat.getMap();
          int i = map.size();
          if (i > 0) {
            String[] arrayOfString = new String[i];
            Object[] arrayOfObject = new Object[i];
            i = 0;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
              arrayOfString[i] = (String)entry.getKey();
              arrayOfObject[i] = entry.getValue();
              i++;
            } 
            i = nativeAddTrack(this.mNativeObject, arrayOfString, arrayOfObject);
            if (this.mLastTrackIndex < i) {
              this.mLastTrackIndex = i;
              return i;
            } 
            throw new IllegalArgumentException("Invalid format.");
          } 
          throw new IllegalArgumentException("format must not be empty.");
        } 
        throw new IllegalStateException("Muxer has been released!");
      } 
      throw new IllegalStateException("Muxer is not initialized.");
    } 
    throw new IllegalArgumentException("format must not be null.");
  }
  
  public void writeSampleData(int paramInt, ByteBuffer paramByteBuffer, MediaCodec.BufferInfo paramBufferInfo) {
    if (paramInt >= 0 && paramInt <= this.mLastTrackIndex) {
      if (paramByteBuffer != null) {
        if (paramBufferInfo != null) {
          if (paramBufferInfo.size >= 0 && paramBufferInfo.offset >= 0) {
            int i = paramBufferInfo.offset, j = paramBufferInfo.size;
            if (i + j <= paramByteBuffer.capacity()) {
              long l = this.mNativeObject;
              if (l != 0L) {
                if (this.mState == 1) {
                  nativeWriteSampleData(l, paramInt, paramByteBuffer, paramBufferInfo.offset, paramBufferInfo.size, paramBufferInfo.presentationTimeUs, paramBufferInfo.flags);
                  return;
                } 
                throw new IllegalStateException("Can't write, muxer is not started");
              } 
              throw new IllegalStateException("Muxer has been released!");
            } 
          } 
          throw new IllegalArgumentException("bufferInfo must specify a valid buffer offset and size");
        } 
        throw new IllegalArgumentException("bufferInfo must not be null");
      } 
      throw new IllegalArgumentException("byteBuffer must not be null");
    } 
    throw new IllegalArgumentException("trackIndex is invalid");
  }
  
  public void release() {
    if (this.mState == 1)
      stop(); 
    long l = this.mNativeObject;
    if (l != 0L) {
      nativeRelease(l);
      this.mNativeObject = 0L;
      this.mCloseGuard.close();
    } 
    this.mState = -1;
  }
  
  private static native int nativeAddTrack(long paramLong, String[] paramArrayOfString, Object[] paramArrayOfObject);
  
  private static native void nativeRelease(long paramLong);
  
  private static native void nativeSetLocation(long paramLong, int paramInt1, int paramInt2);
  
  private static native void nativeSetOrientationHint(long paramLong, int paramInt);
  
  private static native long nativeSetup(FileDescriptor paramFileDescriptor, int paramInt) throws IllegalArgumentException, IOException;
  
  private static native void nativeStart(long paramLong);
  
  private static native void nativeStop(long paramLong);
  
  private static native void nativeWriteSampleData(long paramLong1, int paramInt1, ByteBuffer paramByteBuffer, int paramInt2, int paramInt3, long paramLong2, int paramInt4);
}
