package android.media;

import android.annotation.SystemApi;
import android.media.audiofx.AudioEffect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class AudioRecordingConfiguration implements Parcelable {
  public static final Parcelable.Creator<AudioRecordingConfiguration> CREATOR;
  
  private static final String TAG = new String("AudioRecordingConfiguration");
  
  private final AudioEffect.Descriptor[] mClientEffects;
  
  private final AudioFormat mClientFormat;
  
  private final String mClientPackageName;
  
  private final int mClientPortId;
  
  private final int mClientSessionId;
  
  private boolean mClientSilenced;
  
  private final int mClientSource;
  
  private final int mClientUid;
  
  private final AudioEffect.Descriptor[] mDeviceEffects;
  
  private final AudioFormat mDeviceFormat;
  
  private final int mDeviceSource;
  
  private final int mPatchHandle;
  
  public AudioRecordingConfiguration(int paramInt1, int paramInt2, int paramInt3, AudioFormat paramAudioFormat1, AudioFormat paramAudioFormat2, int paramInt4, String paramString, int paramInt5, boolean paramBoolean, int paramInt6, AudioEffect.Descriptor[] paramArrayOfDescriptor1, AudioEffect.Descriptor[] paramArrayOfDescriptor2) {
    this.mClientUid = paramInt1;
    this.mClientSessionId = paramInt2;
    this.mClientSource = paramInt3;
    this.mClientFormat = paramAudioFormat1;
    this.mDeviceFormat = paramAudioFormat2;
    this.mPatchHandle = paramInt4;
    this.mClientPackageName = paramString;
    this.mClientPortId = paramInt5;
    this.mClientSilenced = paramBoolean;
    this.mDeviceSource = paramInt6;
    this.mClientEffects = paramArrayOfDescriptor1;
    this.mDeviceEffects = paramArrayOfDescriptor2;
  }
  
  public AudioRecordingConfiguration(int paramInt1, int paramInt2, int paramInt3, AudioFormat paramAudioFormat1, AudioFormat paramAudioFormat2, int paramInt4, String paramString) {
    this(paramInt1, paramInt2, paramInt3, paramAudioFormat1, paramAudioFormat2, paramInt4, paramString, 0, false, 0, new AudioEffect.Descriptor[0], new AudioEffect.Descriptor[0]);
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("  ");
    stringBuilder.append(toLogFriendlyString(this));
    paramPrintWriter.println(stringBuilder.toString());
  }
  
  public static String toLogFriendlyString(AudioRecordingConfiguration paramAudioRecordingConfiguration) {
    String str1 = new String();
    int i;
    byte b;
    int j;
    for (AudioEffect.Descriptor[] arrayOfDescriptor1 = paramAudioRecordingConfiguration.mClientEffects; j < i; ) {
      AudioEffect.Descriptor descriptor = arrayOfDescriptor1[j];
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str1);
      stringBuilder1.append("'");
      stringBuilder1.append(descriptor.name);
      stringBuilder1.append("' ");
      str1 = stringBuilder1.toString();
      j++;
    } 
    String str2 = new String();
    for (AudioEffect.Descriptor[] arrayOfDescriptor2 = paramAudioRecordingConfiguration.mDeviceEffects; j < i; ) {
      AudioEffect.Descriptor descriptor = arrayOfDescriptor2[j];
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str2);
      stringBuilder1.append("'");
      stringBuilder1.append(descriptor.name);
      stringBuilder1.append("' ");
      str2 = stringBuilder1.toString();
      j++;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("session:");
    stringBuilder.append(paramAudioRecordingConfiguration.mClientSessionId);
    stringBuilder.append(" -- source client=");
    j = paramAudioRecordingConfiguration.mClientSource;
    stringBuilder.append(MediaRecorder.toLogFriendlyAudioSource(j));
    stringBuilder.append(", dev=");
    AudioFormat audioFormat = paramAudioRecordingConfiguration.mDeviceFormat;
    stringBuilder.append(audioFormat.toLogFriendlyString());
    stringBuilder.append(" -- uid:");
    stringBuilder.append(paramAudioRecordingConfiguration.mClientUid);
    stringBuilder.append(" -- patch:");
    stringBuilder.append(paramAudioRecordingConfiguration.mPatchHandle);
    stringBuilder.append(" -- pack:");
    stringBuilder.append(paramAudioRecordingConfiguration.mClientPackageName);
    stringBuilder.append(" -- format client=");
    audioFormat = paramAudioRecordingConfiguration.mClientFormat;
    stringBuilder.append(audioFormat.toLogFriendlyString());
    stringBuilder.append(", dev=");
    audioFormat = paramAudioRecordingConfiguration.mDeviceFormat;
    stringBuilder.append(audioFormat.toLogFriendlyString());
    stringBuilder.append(" -- silenced:");
    stringBuilder.append(paramAudioRecordingConfiguration.mClientSilenced);
    stringBuilder.append(" -- effects client=");
    stringBuilder.append(str1);
    stringBuilder.append(", dev=");
    stringBuilder.append(str2);
    return new String(stringBuilder.toString());
  }
  
  public static AudioRecordingConfiguration anonymizedCopy(AudioRecordingConfiguration paramAudioRecordingConfiguration) {
    return new AudioRecordingConfiguration(-1, paramAudioRecordingConfiguration.mClientSessionId, paramAudioRecordingConfiguration.mClientSource, paramAudioRecordingConfiguration.mClientFormat, paramAudioRecordingConfiguration.mDeviceFormat, paramAudioRecordingConfiguration.mPatchHandle, "", paramAudioRecordingConfiguration.mClientPortId, paramAudioRecordingConfiguration.mClientSilenced, paramAudioRecordingConfiguration.mDeviceSource, paramAudioRecordingConfiguration.mClientEffects, paramAudioRecordingConfiguration.mDeviceEffects);
  }
  
  public int getClientAudioSource() {
    return this.mClientSource;
  }
  
  public int getClientAudioSessionId() {
    return this.mClientSessionId;
  }
  
  public AudioFormat getFormat() {
    return this.mDeviceFormat;
  }
  
  public AudioFormat getClientFormat() {
    return this.mClientFormat;
  }
  
  public String getClientPackageName() {
    return this.mClientPackageName;
  }
  
  @SystemApi
  public int getClientUid() {
    int i = this.mClientUid;
    if (i != -1)
      return i; 
    throw new SecurityException("MODIFY_AUDIO_ROUTING permission is missing");
  }
  
  public AudioDeviceInfo getAudioDevice() {
    ArrayList<AudioPatch> arrayList = new ArrayList();
    if (AudioManager.listAudioPatches(arrayList) != 0) {
      Log.e(TAG, "Error retrieving list of audio patches");
      return null;
    } 
    for (byte b = 0; b < arrayList.size(); b++) {
      AudioPatch audioPatch = arrayList.get(b);
      if (audioPatch.id() == this.mPatchHandle) {
        AudioPortConfig[] arrayOfAudioPortConfig = audioPatch.sources();
        if (arrayOfAudioPortConfig != null && arrayOfAudioPortConfig.length > 0) {
          int i = arrayOfAudioPortConfig[0].port().id();
          AudioDeviceInfo[] arrayOfAudioDeviceInfo = AudioManager.getDevicesStatic(1);
          for (b = 0; b < arrayOfAudioDeviceInfo.length; b++) {
            if (arrayOfAudioDeviceInfo[b].getId() == i)
              return arrayOfAudioDeviceInfo[b]; 
          } 
        } 
        break;
      } 
    } 
    Log.e(TAG, "Couldn't find device for recording, did recording end already?");
    return null;
  }
  
  public int getClientPortId() {
    return this.mClientPortId;
  }
  
  public boolean isClientSilenced() {
    return this.mClientSilenced;
  }
  
  public int getAudioSource() {
    return this.mDeviceSource;
  }
  
  public List<AudioEffect.Descriptor> getClientEffects() {
    return new ArrayList<>(Arrays.asList(this.mClientEffects));
  }
  
  public List<AudioEffect.Descriptor> getEffects() {
    return new ArrayList<>(Arrays.asList(this.mDeviceEffects));
  }
  
  static {
    CREATOR = new Parcelable.Creator<AudioRecordingConfiguration>() {
        public AudioRecordingConfiguration createFromParcel(Parcel param1Parcel) {
          return new AudioRecordingConfiguration(param1Parcel);
        }
        
        public AudioRecordingConfiguration[] newArray(int param1Int) {
          return new AudioRecordingConfiguration[param1Int];
        }
      };
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mClientSessionId), Integer.valueOf(this.mClientSource) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mClientSessionId);
    paramParcel.writeInt(this.mClientSource);
    this.mClientFormat.writeToParcel(paramParcel, 0);
    this.mDeviceFormat.writeToParcel(paramParcel, 0);
    paramParcel.writeInt(this.mPatchHandle);
    paramParcel.writeString(this.mClientPackageName);
    paramParcel.writeInt(this.mClientUid);
    paramParcel.writeInt(this.mClientPortId);
    paramParcel.writeBoolean(this.mClientSilenced);
    paramParcel.writeInt(this.mDeviceSource);
    paramParcel.writeInt(this.mClientEffects.length);
    paramInt = 0;
    while (true) {
      AudioEffect.Descriptor[] arrayOfDescriptor = this.mClientEffects;
      if (paramInt < arrayOfDescriptor.length) {
        arrayOfDescriptor[paramInt].writeToParcel(paramParcel);
        paramInt++;
        continue;
      } 
      break;
    } 
    paramParcel.writeInt(this.mDeviceEffects.length);
    paramInt = 0;
    while (true) {
      AudioEffect.Descriptor[] arrayOfDescriptor = this.mDeviceEffects;
      if (paramInt < arrayOfDescriptor.length) {
        arrayOfDescriptor[paramInt].writeToParcel(paramParcel);
        paramInt++;
        continue;
      } 
      break;
    } 
  }
  
  private AudioRecordingConfiguration(Parcel paramParcel) {
    this.mClientSessionId = paramParcel.readInt();
    this.mClientSource = paramParcel.readInt();
    this.mClientFormat = AudioFormat.CREATOR.createFromParcel(paramParcel);
    this.mDeviceFormat = AudioFormat.CREATOR.createFromParcel(paramParcel);
    this.mPatchHandle = paramParcel.readInt();
    this.mClientPackageName = paramParcel.readString();
    this.mClientUid = paramParcel.readInt();
    this.mClientPortId = paramParcel.readInt();
    this.mClientSilenced = paramParcel.readBoolean();
    this.mDeviceSource = paramParcel.readInt();
    this.mClientEffects = new AudioEffect.Descriptor[paramParcel.readInt()];
    byte b = 0;
    while (true) {
      AudioEffect.Descriptor[] arrayOfDescriptor = this.mClientEffects;
      if (b < arrayOfDescriptor.length) {
        arrayOfDescriptor[b] = new AudioEffect.Descriptor(paramParcel);
        b++;
        continue;
      } 
      break;
    } 
    this.mDeviceEffects = new AudioEffect.Descriptor[paramParcel.readInt()];
    b = 0;
    while (true) {
      AudioEffect.Descriptor[] arrayOfDescriptor = this.mDeviceEffects;
      if (b < arrayOfDescriptor.length) {
        arrayOfDescriptor[b] = new AudioEffect.Descriptor(paramParcel);
        b++;
        continue;
      } 
      break;
    } 
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || !(paramObject instanceof AudioRecordingConfiguration))
      return false; 
    paramObject = paramObject;
    if (this.mClientUid == ((AudioRecordingConfiguration)paramObject).mClientUid && this.mClientSessionId == ((AudioRecordingConfiguration)paramObject).mClientSessionId && this.mClientSource == ((AudioRecordingConfiguration)paramObject).mClientSource && this.mPatchHandle == ((AudioRecordingConfiguration)paramObject).mPatchHandle) {
      AudioFormat audioFormat1 = this.mClientFormat, audioFormat2 = ((AudioRecordingConfiguration)paramObject).mClientFormat;
      if (audioFormat1.equals(audioFormat2)) {
        audioFormat1 = this.mDeviceFormat;
        audioFormat2 = ((AudioRecordingConfiguration)paramObject).mDeviceFormat;
        if (audioFormat1.equals(audioFormat2)) {
          String str2 = this.mClientPackageName, str1 = ((AudioRecordingConfiguration)paramObject).mClientPackageName;
          if (str2.equals(str1) && this.mClientPortId == ((AudioRecordingConfiguration)paramObject).mClientPortId && this.mClientSilenced == ((AudioRecordingConfiguration)paramObject).mClientSilenced && this.mDeviceSource == ((AudioRecordingConfiguration)paramObject).mDeviceSource) {
            AudioEffect.Descriptor[] arrayOfDescriptor1 = this.mClientEffects, arrayOfDescriptor2 = ((AudioRecordingConfiguration)paramObject).mClientEffects;
            if (Arrays.equals((Object[])arrayOfDescriptor1, (Object[])arrayOfDescriptor2)) {
              arrayOfDescriptor1 = this.mDeviceEffects;
              paramObject = ((AudioRecordingConfiguration)paramObject).mDeviceEffects;
              if (Arrays.equals((Object[])arrayOfDescriptor1, (Object[])paramObject))
                return null; 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AudioSource implements Annotation {}
}
