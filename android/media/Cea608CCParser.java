package android.media;

import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.text.style.UpdateAppearance;
import android.util.Log;
import android.view.accessibility.CaptioningManager;
import java.util.ArrayList;
import java.util.Arrays;

class Cea608CCParser {
  private static final boolean DEBUG = Log.isLoggable("Cea608CCParser", 3);
  
  private int mMode = 1;
  
  private int mRollUpSize = 4;
  
  private int mPrevCtrlCode = -1;
  
  private CCMemory mDisplay = new CCMemory();
  
  private CCMemory mNonDisplay = new CCMemory();
  
  private CCMemory mTextMem = new CCMemory();
  
  private static final int AOF = 34;
  
  private static final int AON = 35;
  
  private static final int BS = 33;
  
  private static final int CR = 45;
  
  private static final int DER = 36;
  
  private static final int EDM = 44;
  
  private static final int ENM = 46;
  
  private static final int EOC = 47;
  
  private static final int FON = 40;
  
  private static final int INVALID = -1;
  
  public static final int MAX_COLS = 32;
  
  public static final int MAX_ROWS = 15;
  
  private static final int MODE_PAINT_ON = 1;
  
  private static final int MODE_POP_ON = 3;
  
  private static final int MODE_ROLL_UP = 2;
  
  private static final int MODE_TEXT = 4;
  
  private static final int MODE_UNKNOWN = 0;
  
  private static final int RCL = 32;
  
  private static final int RDC = 41;
  
  private static final int RTD = 43;
  
  private static final int RU2 = 37;
  
  private static final int RU3 = 38;
  
  private static final int RU4 = 39;
  
  private static final String TAG = "Cea608CCParser";
  
  private static final int TR = 42;
  
  private static final char TS = ' ';
  
  private final DisplayListener mListener;
  
  Cea608CCParser(DisplayListener paramDisplayListener) {
    this.mListener = paramDisplayListener;
  }
  
  public void parse(byte[] paramArrayOfbyte) {
    CCData[] arrayOfCCData = CCData.fromByteArray(paramArrayOfbyte);
    for (byte b = 0; b < arrayOfCCData.length; b++) {
      if (DEBUG)
        Log.d("Cea608CCParser", arrayOfCCData[b].toString()); 
      if (!handleCtrlCode(arrayOfCCData[b])) {
        CCData cCData = arrayOfCCData[b];
        if (!handleTabOffsets(cCData)) {
          cCData = arrayOfCCData[b];
          if (!handlePACCode(cCData)) {
            cCData = arrayOfCCData[b];
            if (!handleMidRowCode(cCData))
              handleDisplayableChars(arrayOfCCData[b]); 
          } 
        } 
      } 
    } 
  }
  
  private CCMemory getMemory() {
    int i = this.mMode;
    if (i != 1 && i != 2) {
      if (i != 3) {
        if (i != 4) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("unrecoginized mode: ");
          stringBuilder.append(this.mMode);
          Log.w("Cea608CCParser", stringBuilder.toString());
          return this.mDisplay;
        } 
        return this.mTextMem;
      } 
      return this.mNonDisplay;
    } 
    return this.mDisplay;
  }
  
  private boolean handleDisplayableChars(CCData paramCCData) {
    if (!paramCCData.isDisplayableChar())
      return false; 
    if (paramCCData.isExtendedChar())
      getMemory().bs(); 
    getMemory().writeText(paramCCData.getDisplayText());
    int i = this.mMode;
    if (i == 1 || i == 2)
      updateDisplay(); 
    return true;
  }
  
  private boolean handleMidRowCode(CCData paramCCData) {
    StyleCode styleCode = paramCCData.getMidRow();
    if (styleCode != null) {
      getMemory().writeMidRowCode(styleCode);
      return true;
    } 
    return false;
  }
  
  private boolean handlePACCode(CCData paramCCData) {
    PAC pAC = paramCCData.getPAC();
    if (pAC != null) {
      if (this.mMode == 2)
        getMemory().moveBaselineTo(pAC.getRow(), this.mRollUpSize); 
      getMemory().writePAC(pAC);
      return true;
    } 
    return false;
  }
  
  private boolean handleTabOffsets(CCData paramCCData) {
    int i = paramCCData.getTabOffset();
    if (i > 0) {
      getMemory().tab(i);
      return true;
    } 
    return false;
  }
  
  private boolean handleCtrlCode(CCData paramCCData) {
    int i = paramCCData.getCtrlCode();
    int j = this.mPrevCtrlCode;
    if (j != -1 && j == i) {
      this.mPrevCtrlCode = -1;
      return true;
    } 
    switch (i) {
      default:
        this.mPrevCtrlCode = -1;
        return false;
      case 47:
        swapMemory();
        this.mMode = 3;
        updateDisplay();
        this.mPrevCtrlCode = i;
        return true;
      case 46:
        this.mNonDisplay.erase();
        this.mPrevCtrlCode = i;
        return true;
      case 45:
        if (this.mMode == 2) {
          getMemory().rollUp(this.mRollUpSize);
        } else {
          getMemory().cr();
        } 
        if (this.mMode == 2)
          updateDisplay(); 
        this.mPrevCtrlCode = i;
        return true;
      case 44:
        this.mDisplay.erase();
        updateDisplay();
        this.mPrevCtrlCode = i;
        return true;
      case 43:
        this.mMode = 4;
        this.mPrevCtrlCode = i;
        return true;
      case 42:
        this.mMode = 4;
        this.mTextMem.erase();
        this.mPrevCtrlCode = i;
        return true;
      case 41:
        this.mMode = 1;
        this.mPrevCtrlCode = i;
        return true;
      case 40:
        Log.i("Cea608CCParser", "Flash On");
        this.mPrevCtrlCode = i;
        return true;
      case 37:
      case 38:
      case 39:
        this.mRollUpSize = i - 35;
        if (this.mMode != 2) {
          this.mDisplay.erase();
          this.mNonDisplay.erase();
        } 
        this.mMode = 2;
        this.mPrevCtrlCode = i;
        return true;
      case 36:
        getMemory().der();
        this.mPrevCtrlCode = i;
        return true;
      case 33:
        getMemory().bs();
        this.mPrevCtrlCode = i;
        return true;
      case 32:
        break;
    } 
    this.mMode = 3;
    this.mPrevCtrlCode = i;
    return true;
  }
  
  private void updateDisplay() {
    DisplayListener displayListener = this.mListener;
    if (displayListener != null) {
      CaptioningManager.CaptionStyle captionStyle = displayListener.getCaptionStyle();
      this.mListener.onDisplayChanged(this.mDisplay.getStyledText(captionStyle));
    } 
  }
  
  private void swapMemory() {
    CCMemory cCMemory = this.mDisplay;
    this.mDisplay = this.mNonDisplay;
    this.mNonDisplay = cCMemory;
  }
  
  private static class StyleCode {
    static final int COLOR_BLUE = 2;
    
    static final int COLOR_CYAN = 3;
    
    static final int COLOR_GREEN = 1;
    
    static final int COLOR_INVALID = 7;
    
    static final int COLOR_MAGENTA = 6;
    
    static final int COLOR_RED = 4;
    
    static final int COLOR_WHITE = 0;
    
    static final int COLOR_YELLOW = 5;
    
    static final int STYLE_ITALICS = 1;
    
    static final int STYLE_UNDERLINE = 2;
    
    static final String[] mColorMap = new String[] { "WHITE", "GREEN", "BLUE", "CYAN", "RED", "YELLOW", "MAGENTA", "INVALID" };
    
    final int mColor;
    
    final int mStyle;
    
    static StyleCode fromByte(byte param1Byte) {
      int j = 0;
      int k = param1Byte >> 1 & 0x7;
      if ((param1Byte & 0x1) != 0)
        j = 0x0 | 0x2; 
      int m = j, i = k;
      if (k == 7) {
        i = 0;
        m = j | 0x1;
      } 
      return new StyleCode(m, i);
    }
    
    StyleCode(int param1Int1, int param1Int2) {
      this.mStyle = param1Int1;
      this.mColor = param1Int2;
    }
    
    boolean isItalics() {
      int i = this.mStyle;
      boolean bool = true;
      if ((i & 0x1) == 0)
        bool = false; 
      return bool;
    }
    
    boolean isUnderline() {
      boolean bool;
      if ((this.mStyle & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    int getColor() {
      return this.mColor;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(mColorMap[this.mColor]);
      if ((this.mStyle & 0x1) != 0)
        stringBuilder.append(", ITALICS"); 
      if ((this.mStyle & 0x2) != 0)
        stringBuilder.append(", UNDERLINE"); 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class PAC extends StyleCode {
    final int mCol;
    
    final int mRow;
    
    static PAC fromBytes(byte param1Byte1, byte param1Byte2) {
      int i;
      (new int[8])[0] = 11;
      (new int[8])[1] = 1;
      (new int[8])[2] = 3;
      (new int[8])[3] = 12;
      (new int[8])[4] = 14;
      (new int[8])[5] = 5;
      (new int[8])[6] = 7;
      (new int[8])[7] = 9;
      int k = (new int[8])[param1Byte1 & 0x7] + ((param1Byte2 & 0x20) >> 5);
      param1Byte1 = 0;
      if ((param1Byte2 & 0x1) != 0)
        i = 0x0 | 0x2; 
      if ((param1Byte2 & 0x10) != 0)
        return new PAC(k, (param1Byte2 >> 1 & 0x7) * 4, i, 0); 
      int m = param1Byte2 >> 1 & 0x7;
      int n = i, j = m;
      if (m == 7) {
        j = 0;
        n = i | 0x1;
      } 
      return new PAC(k, -1, n, j);
    }
    
    PAC(Cea608CCParser this$0, int param1Int1, int param1Int2, int param1Int3) {
      super(param1Int2, param1Int3);
      this.mRow = this$0;
      this.mCol = param1Int1;
    }
    
    boolean isIndentPAC() {
      boolean bool;
      if (this.mCol >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    int getRow() {
      return this.mRow;
    }
    
    int getCol() {
      return this.mCol;
    }
    
    public String toString() {
      int i = this.mRow;
      int j = this.mCol;
      String str = super.toString();
      return String.format("{%d, %d}, %s", new Object[] { Integer.valueOf(i), Integer.valueOf(j), str });
    }
  }
  
  class MutableBackgroundColorSpan extends CharacterStyle implements UpdateAppearance {
    private int mColor;
    
    public MutableBackgroundColorSpan(Cea608CCParser this$0) {
      this.mColor = this$0;
    }
    
    public void setBackgroundColor(int param1Int) {
      this.mColor = param1Int;
    }
    
    public int getBackgroundColor() {
      return this.mColor;
    }
    
    public void updateDrawState(TextPaint param1TextPaint) {
      param1TextPaint.bgColor = this.mColor;
    }
  }
  
  private static class CCLineBuilder {
    private final StringBuilder mDisplayChars;
    
    private final Cea608CCParser.StyleCode[] mMidRowStyles;
    
    private final Cea608CCParser.StyleCode[] mPACStyles;
    
    CCLineBuilder(String param1String) {
      StringBuilder stringBuilder = new StringBuilder(param1String);
      this.mMidRowStyles = new Cea608CCParser.StyleCode[stringBuilder.length()];
      this.mPACStyles = new Cea608CCParser.StyleCode[this.mDisplayChars.length()];
    }
    
    void setCharAt(int param1Int, char param1Char) {
      this.mDisplayChars.setCharAt(param1Int, param1Char);
      this.mMidRowStyles[param1Int] = null;
    }
    
    void setMidRowAt(int param1Int, Cea608CCParser.StyleCode param1StyleCode) {
      this.mDisplayChars.setCharAt(param1Int, ' ');
      this.mMidRowStyles[param1Int] = param1StyleCode;
    }
    
    void setPACAt(int param1Int, Cea608CCParser.PAC param1PAC) {
      this.mPACStyles[param1Int] = param1PAC;
    }
    
    char charAt(int param1Int) {
      return this.mDisplayChars.charAt(param1Int);
    }
    
    int length() {
      return this.mDisplayChars.length();
    }
    
    void applyStyleSpan(SpannableStringBuilder param1SpannableStringBuilder, Cea608CCParser.StyleCode param1StyleCode, int param1Int1, int param1Int2) {
      if (param1StyleCode.isItalics())
        param1SpannableStringBuilder.setSpan(new StyleSpan(2), param1Int1, param1Int2, 33); 
      if (param1StyleCode.isUnderline())
        param1SpannableStringBuilder.setSpan(new UnderlineSpan(), param1Int1, param1Int2, 33); 
    }
    
    SpannableStringBuilder getStyledText(CaptioningManager.CaptionStyle param1CaptionStyle) {
      // Byte code:
      //   0: new android/text/SpannableStringBuilder
      //   3: dup
      //   4: aload_0
      //   5: getfield mDisplayChars : Ljava/lang/StringBuilder;
      //   8: invokespecial <init> : (Ljava/lang/CharSequence;)V
      //   11: astore_2
      //   12: iconst_m1
      //   13: istore_3
      //   14: iconst_0
      //   15: istore #4
      //   17: iconst_m1
      //   18: istore #5
      //   20: aconst_null
      //   21: astore #6
      //   23: iload #4
      //   25: aload_0
      //   26: getfield mDisplayChars : Ljava/lang/StringBuilder;
      //   29: invokevirtual length : ()I
      //   32: if_icmpge -> 271
      //   35: aconst_null
      //   36: astore #7
      //   38: aload_0
      //   39: getfield mMidRowStyles : [Landroid/media/Cea608CCParser$StyleCode;
      //   42: astore #8
      //   44: aload #8
      //   46: iload #4
      //   48: aaload
      //   49: ifnull -> 62
      //   52: aload #8
      //   54: iload #4
      //   56: aaload
      //   57: astore #8
      //   59: goto -> 98
      //   62: aload #7
      //   64: astore #8
      //   66: aload_0
      //   67: getfield mPACStyles : [Landroid/media/Cea608CCParser$StyleCode;
      //   70: iload #4
      //   72: aaload
      //   73: ifnull -> 98
      //   76: iload #5
      //   78: iflt -> 89
      //   81: aload #7
      //   83: astore #8
      //   85: iload_3
      //   86: ifge -> 98
      //   89: aload_0
      //   90: getfield mPACStyles : [Landroid/media/Cea608CCParser$StyleCode;
      //   93: iload #4
      //   95: aaload
      //   96: astore #8
      //   98: iload #5
      //   100: istore #9
      //   102: aload #8
      //   104: ifnull -> 135
      //   107: aload #8
      //   109: astore #6
      //   111: iload #5
      //   113: iflt -> 131
      //   116: iload_3
      //   117: iflt -> 131
      //   120: aload_0
      //   121: aload_2
      //   122: aload #8
      //   124: iload #5
      //   126: iload #4
      //   128: invokevirtual applyStyleSpan : (Landroid/text/SpannableStringBuilder;Landroid/media/Cea608CCParser$StyleCode;II)V
      //   131: iload #4
      //   133: istore #9
      //   135: aload_0
      //   136: getfield mDisplayChars : Ljava/lang/StringBuilder;
      //   139: iload #4
      //   141: invokevirtual charAt : (I)C
      //   144: sipush #160
      //   147: if_icmpeq -> 164
      //   150: iload_3
      //   151: istore #5
      //   153: iload_3
      //   154: ifge -> 258
      //   157: iload #4
      //   159: istore #5
      //   161: goto -> 258
      //   164: iload_3
      //   165: istore #5
      //   167: iload_3
      //   168: iflt -> 258
      //   171: aload_0
      //   172: getfield mDisplayChars : Ljava/lang/StringBuilder;
      //   175: iload_3
      //   176: invokevirtual charAt : (I)C
      //   179: bipush #32
      //   181: if_icmpne -> 187
      //   184: goto -> 190
      //   187: iinc #3, -1
      //   190: aload_0
      //   191: getfield mDisplayChars : Ljava/lang/StringBuilder;
      //   194: iload #4
      //   196: iconst_1
      //   197: isub
      //   198: invokevirtual charAt : (I)C
      //   201: bipush #32
      //   203: if_icmpne -> 213
      //   206: iload #4
      //   208: istore #5
      //   210: goto -> 219
      //   213: iload #4
      //   215: iconst_1
      //   216: iadd
      //   217: istore #5
      //   219: aload_2
      //   220: new android/media/Cea608CCParser$MutableBackgroundColorSpan
      //   223: dup
      //   224: aload_1
      //   225: getfield backgroundColor : I
      //   228: invokespecial <init> : (I)V
      //   231: iload_3
      //   232: iload #5
      //   234: bipush #33
      //   236: invokevirtual setSpan : (Ljava/lang/Object;III)V
      //   239: iload #9
      //   241: iflt -> 255
      //   244: aload_0
      //   245: aload_2
      //   246: aload #6
      //   248: iload #9
      //   250: iload #5
      //   252: invokevirtual applyStyleSpan : (Landroid/text/SpannableStringBuilder;Landroid/media/Cea608CCParser$StyleCode;II)V
      //   255: iconst_m1
      //   256: istore #5
      //   258: iinc #4, 1
      //   261: iload #5
      //   263: istore_3
      //   264: iload #9
      //   266: istore #5
      //   268: goto -> 23
      //   271: aload_2
      //   272: areturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #708	-> 0
      //   #709	-> 12
      //   #710	-> 17
      //   #711	-> 20
      //   #712	-> 23
      //   #713	-> 35
      //   #714	-> 38
      //   #716	-> 52
      //   #717	-> 62
      //   #722	-> 89
      //   #724	-> 98
      //   #725	-> 107
      //   #726	-> 111
      //   #727	-> 120
      //   #729	-> 131
      //   #732	-> 135
      //   #733	-> 150
      //   #734	-> 157
      //   #736	-> 164
      //   #737	-> 171
      //   #738	-> 190
      //   #739	-> 219
      //   #743	-> 239
      //   #744	-> 244
      //   #746	-> 255
      //   #748	-> 258
      //   #749	-> 261
      //   #751	-> 271
    }
  }
  
  private static class CCMemory {
    private final String mBlankLine;
    
    private int mCol;
    
    private final Cea608CCParser.CCLineBuilder[] mLines = new Cea608CCParser.CCLineBuilder[17];
    
    private int mRow;
    
    CCMemory() {
      char[] arrayOfChar = new char[34];
      Arrays.fill(arrayOfChar, ' ');
      this.mBlankLine = new String(arrayOfChar);
    }
    
    void erase() {
      byte b = 0;
      while (true) {
        Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
        if (b < arrayOfCCLineBuilder.length) {
          arrayOfCCLineBuilder[b] = null;
          b++;
          continue;
        } 
        break;
      } 
      this.mRow = 15;
      this.mCol = 1;
    }
    
    void der() {
      if (this.mLines[this.mRow] != null) {
        for (int i = 0; i < this.mCol; i++) {
          if (this.mLines[this.mRow].charAt(i) != ' ') {
            for (i = this.mCol; i < this.mLines[this.mRow].length(); i++)
              this.mLines[i].setCharAt(i, ' '); 
            return;
          } 
        } 
        this.mLines[this.mRow] = null;
      } 
    }
    
    void tab(int param1Int) {
      moveCursorByCol(param1Int);
    }
    
    void bs() {
      moveCursorByCol(-1);
      Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
      int i = this.mRow;
      if (arrayOfCCLineBuilder[i] != null) {
        arrayOfCCLineBuilder[i].setCharAt(this.mCol, ' ');
        if (this.mCol == 31)
          this.mLines[this.mRow].setCharAt(32, ' '); 
      } 
    }
    
    void cr() {
      moveCursorTo(this.mRow + 1, 1);
    }
    
    void rollUp(int param1Int) {
      int j, i = 0;
      while (true) {
        j = this.mRow;
        if (i <= j - param1Int) {
          this.mLines[i] = null;
          i++;
          continue;
        } 
        break;
      } 
      i = j - param1Int + 1;
      param1Int = i;
      if (i < 1)
        param1Int = 1; 
      for (; param1Int < this.mRow; param1Int++) {
        Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
        arrayOfCCLineBuilder[param1Int] = arrayOfCCLineBuilder[param1Int + 1];
      } 
      param1Int = this.mRow;
      while (true) {
        Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
        if (param1Int < arrayOfCCLineBuilder.length) {
          arrayOfCCLineBuilder[param1Int] = null;
          param1Int++;
          continue;
        } 
        break;
      } 
      this.mCol = 1;
    }
    
    void writeText(String param1String) {
      for (byte b = 0; b < param1String.length(); b++) {
        getLineBuffer(this.mRow).setCharAt(this.mCol, param1String.charAt(b));
        moveCursorByCol(1);
      } 
    }
    
    void writeMidRowCode(Cea608CCParser.StyleCode param1StyleCode) {
      getLineBuffer(this.mRow).setMidRowAt(this.mCol, param1StyleCode);
      moveCursorByCol(1);
    }
    
    void writePAC(Cea608CCParser.PAC param1PAC) {
      if (param1PAC.isIndentPAC()) {
        moveCursorTo(param1PAC.getRow(), param1PAC.getCol());
      } else {
        moveCursorTo(param1PAC.getRow(), 1);
      } 
      getLineBuffer(this.mRow).setPACAt(this.mCol, param1PAC);
    }
    
    SpannableStringBuilder[] getStyledText(CaptioningManager.CaptionStyle param1CaptionStyle) {
      ArrayList<Cea608CCParser.CCLineBuilder[]> arrayList = new ArrayList(15);
      for (byte b = 1; b <= 15; b++) {
        Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
        if (arrayOfCCLineBuilder[b] != null) {
          SpannableStringBuilder spannableStringBuilder = arrayOfCCLineBuilder[b].getStyledText(param1CaptionStyle);
        } else {
          arrayOfCCLineBuilder = null;
        } 
        arrayList.add(arrayOfCCLineBuilder);
      } 
      return arrayList.<SpannableStringBuilder>toArray(new SpannableStringBuilder[15]);
    }
    
    private static int clamp(int param1Int1, int param1Int2, int param1Int3) {
      if (param1Int1 < param1Int2) {
        param1Int1 = param1Int2;
      } else if (param1Int1 > param1Int3) {
        param1Int1 = param1Int3;
      } 
      return param1Int1;
    }
    
    private void moveCursorTo(int param1Int1, int param1Int2) {
      this.mRow = clamp(param1Int1, 1, 15);
      this.mCol = clamp(param1Int2, 1, 32);
    }
    
    private void moveCursorToRow(int param1Int) {
      this.mRow = clamp(param1Int, 1, 15);
    }
    
    private void moveCursorByCol(int param1Int) {
      this.mCol = clamp(this.mCol + param1Int, 1, 32);
    }
    
    private void moveBaselineTo(int param1Int1, int param1Int2) {
      if (this.mRow == param1Int1)
        return; 
      int i = param1Int2;
      int j = i;
      if (param1Int1 < i)
        j = param1Int1; 
      i = j;
      if (this.mRow < j)
        i = this.mRow; 
      if (param1Int1 < this.mRow) {
        for (j = i - 1; j >= 0; j--) {
          Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
          arrayOfCCLineBuilder[param1Int1 - j] = arrayOfCCLineBuilder[this.mRow - j];
        } 
      } else {
        for (j = 0; j < i; j++) {
          Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
          arrayOfCCLineBuilder[param1Int1 - j] = arrayOfCCLineBuilder[this.mRow - j];
        } 
      } 
      for (j = 0; j <= param1Int1 - param1Int2; j++)
        this.mLines[j] = null; 
      param1Int1++;
      while (true) {
        Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
        if (param1Int1 < arrayOfCCLineBuilder.length) {
          arrayOfCCLineBuilder[param1Int1] = null;
          param1Int1++;
          continue;
        } 
        break;
      } 
    }
    
    private Cea608CCParser.CCLineBuilder getLineBuffer(int param1Int) {
      Cea608CCParser.CCLineBuilder[] arrayOfCCLineBuilder = this.mLines;
      if (arrayOfCCLineBuilder[param1Int] == null)
        arrayOfCCLineBuilder[param1Int] = new Cea608CCParser.CCLineBuilder(this.mBlankLine); 
      return this.mLines[param1Int];
    }
  }
  
  private static class CCData {
    private static final String[] mCtrlCodeMap = new String[] { 
        "RCL", "BS", "AOF", "AON", "DER", "RU2", "RU3", "RU4", "FON", "RDC", 
        "TR", "RTD", "EDM", "CR", "ENM", "EOC" };
    
    private static final String[] mProtugueseCharMap;
    
    private static final String[] mSpanishCharMap = new String[] { 
        "Á", "É", "Ó", "Ú", "Ü", "ü", "‘", "¡", "*", "'", 
        "—", "©", "℠", "•", "“", "”", "À", "Â", "Ç", "È", 
        "Ê", "Ë", "ë", "Î", "Ï", "ï", "Ô", "Ù", "ù", "Û", 
        "«", "»" };
    
    private static final String[] mSpecialCharMap = new String[] { 
        "®", "°", "½", "¿", "™", "¢", "£", "♪", "à", " ", 
        "è", "â", "ê", "î", "ô", "û" };
    
    private final byte mData1;
    
    private final byte mData2;
    
    private final byte mType;
    
    static {
      mProtugueseCharMap = new String[] { 
          "Ã", "ã", "Í", "Ì", "ì", "Ò", "ò", "Õ", "õ", "{", 
          "}", "\\", "^", "_", "|", "~", "Ä", "ä", "Ö", "ö", 
          "ß", "¥", "¤", "│", "Å", "å", "Ø", "ø", "┌", "┐", 
          "└", "┘" };
    }
    
    static CCData[] fromByteArray(byte[] param1ArrayOfbyte) {
      CCData[] arrayOfCCData = new CCData[param1ArrayOfbyte.length / 3];
      for (byte b = 0; b < arrayOfCCData.length; b++)
        arrayOfCCData[b] = new CCData(param1ArrayOfbyte[b * 3], param1ArrayOfbyte[b * 3 + 1], param1ArrayOfbyte[b * 3 + 2]); 
      return arrayOfCCData;
    }
    
    CCData(byte param1Byte1, byte param1Byte2, byte param1Byte3) {
      this.mType = param1Byte1;
      this.mData1 = param1Byte2;
      this.mData2 = param1Byte3;
    }
    
    int getCtrlCode() {
      byte b = this.mData1;
      if (b == 20 || b == 28) {
        b = this.mData2;
        if (b >= 32 && b <= 47)
          return b; 
      } 
      return -1;
    }
    
    Cea608CCParser.StyleCode getMidRow() {
      byte b = this.mData1;
      if (b == 17 || b == 25) {
        byte b1 = this.mData2;
        if (b1 >= 32 && b1 <= 47)
          return Cea608CCParser.StyleCode.fromByte(b1); 
      } 
      return null;
    }
    
    Cea608CCParser.PAC getPAC() {
      byte b = this.mData1;
      if ((b & 0x70) == 16) {
        byte b1 = this.mData2;
        if ((b1 & 0x40) == 64 && ((b & 0x7) != 0 || (b1 & 0x20) == 0))
          return Cea608CCParser.PAC.fromBytes(this.mData1, this.mData2); 
      } 
      return null;
    }
    
    int getTabOffset() {
      byte b = this.mData1;
      if (b == 23 || b == 31) {
        b = this.mData2;
        if (b >= 33 && b <= 35)
          return b & 0x3; 
      } 
      return 0;
    }
    
    boolean isDisplayableChar() {
      return (isBasicChar() || isSpecialChar() || isExtendedChar());
    }
    
    String getDisplayText() {
      String str1 = getBasicChars();
      String str2 = str1;
      if (str1 == null) {
        str1 = getSpecialChar();
        str2 = str1;
        if (str1 == null)
          str2 = getExtendedChar(); 
      } 
      return str2;
    }
    
    private String ctrlCodeToString(int param1Int) {
      return mCtrlCodeMap[param1Int - 32];
    }
    
    private boolean isBasicChar() {
      boolean bool;
      byte b = this.mData1;
      if (b >= 32 && b <= Byte.MAX_VALUE) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean isSpecialChar() {
      byte b = this.mData1;
      if (b == 17 || b == 25) {
        b = this.mData2;
        if (b >= 48 && b <= 63)
          return true; 
      } 
      return false;
    }
    
    private boolean isExtendedChar() {
      byte b = this.mData1;
      if (b == 18 || b == 26 || b == 19 || b == 27) {
        b = this.mData2;
        if (b >= 32 && b <= 63)
          return true; 
      } 
      return false;
    }
    
    private char getBasicChar(byte param1Byte) {
      char c;
      if (param1Byte != 42) {
        if (param1Byte != 92) {
          switch (param1Byte) {
            default:
              switch (param1Byte) {
                default:
                  c = (char)param1Byte;
                  return c;
                case 127:
                  c = '█';
                  return c;
                case 126:
                  c = 'ñ';
                  return c;
                case 125:
                  c = 'Ñ';
                  return c;
                case 124:
                  c = '÷';
                  return c;
                case 123:
                  break;
              } 
              c = 'ç';
              return c;
            case 96:
              c = 'ú';
              return c;
            case 95:
              c = 'ó';
              return c;
            case 94:
              break;
          } 
          c = 'í';
        } else {
          c = 'é';
        } 
      } else {
        c = 'á';
      } 
      return c;
    }
    
    private String getBasicChars() {
      byte b = this.mData1;
      if (b >= 32 && b <= Byte.MAX_VALUE) {
        StringBuilder stringBuilder = new StringBuilder(2);
        stringBuilder.append(getBasicChar(this.mData1));
        byte b1 = this.mData2;
        if (b1 >= 32 && b1 <= Byte.MAX_VALUE)
          stringBuilder.append(getBasicChar(b1)); 
        return stringBuilder.toString();
      } 
      return null;
    }
    
    private String getSpecialChar() {
      byte b = this.mData1;
      if (b == 17 || b == 25) {
        b = this.mData2;
        if (b >= 48 && b <= 63)
          return mSpecialCharMap[b - 48]; 
      } 
      return null;
    }
    
    private String getExtendedChar() {
      byte b = this.mData1;
      if (b == 18 || b == 26) {
        b = this.mData2;
        if (b >= 32 && b <= 63)
          return mSpanishCharMap[b - 32]; 
      } 
      b = this.mData1;
      if (b == 19 || b == 27) {
        b = this.mData2;
        if (b >= 32 && b <= 63)
          return mProtugueseCharMap[b - 32]; 
      } 
      return null;
    }
    
    public String toString() {
      if (this.mData1 < 16 && this.mData2 < 16)
        return String.format("[%d]Null: %02x %02x", new Object[] { Byte.valueOf(this.mType), Byte.valueOf(this.mData1), Byte.valueOf(this.mData2) }); 
      int i = getCtrlCode();
      if (i != -1)
        return String.format("[%d]%s", new Object[] { Byte.valueOf(this.mType), ctrlCodeToString(i) }); 
      i = getTabOffset();
      if (i > 0)
        return String.format("[%d]Tab%d", new Object[] { Byte.valueOf(this.mType), Integer.valueOf(i) }); 
      Cea608CCParser.PAC pAC = getPAC();
      if (pAC != null)
        return String.format("[%d]PAC: %s", new Object[] { Byte.valueOf(this.mType), pAC.toString() }); 
      Cea608CCParser.StyleCode styleCode = getMidRow();
      if (styleCode != null)
        return String.format("[%d]Mid-row: %s", new Object[] { Byte.valueOf(this.mType), styleCode.toString() }); 
      if (isDisplayableChar()) {
        byte b1 = this.mType;
        String str = getDisplayText();
        byte b2 = this.mData1, b3 = this.mData2;
        return String.format("[%d]Displayable: %s (%02x %02x)", new Object[] { Byte.valueOf(b1), str, Byte.valueOf(b2), Byte.valueOf(b3) });
      } 
      return String.format("[%d]Invalid: %02x %02x", new Object[] { Byte.valueOf(this.mType), Byte.valueOf(this.mData1), Byte.valueOf(this.mData2) });
    }
  }
  
  static interface DisplayListener {
    CaptioningManager.CaptionStyle getCaptionStyle();
    
    void onDisplayChanged(SpannableStringBuilder[] param1ArrayOfSpannableStringBuilder);
  }
}
