package android.media;

class AudioHandle {
  private final int mId;
  
  AudioHandle(int paramInt) {
    this.mId = paramInt;
  }
  
  int id() {
    return this.mId;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null || !(paramObject instanceof AudioHandle))
      return false; 
    paramObject = paramObject;
    if (this.mId == paramObject.id())
      bool = true; 
    return bool;
  }
  
  public int hashCode() {
    return this.mId;
  }
  
  public String toString() {
    return Integer.toString(this.mId);
  }
}
