package android.media;

interface WebVttCueListener {
  void onCueParsed(TextTrackCue paramTextTrackCue);
  
  void onRegionParsed(TextTrackRegion paramTextTrackRegion);
}
