package android.media;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;
import com.oppo.atlas.OppoAtlasManager;
import java.util.Arrays;

public class OppoBaseAudioManager {
  private static final int ADJUST_LOWER = -1;
  
  private static final int ADJUST_MUTE = -100;
  
  private static final int ADJUST_RAISE = 1;
  
  private static final int ADJUST_SAME = 0;
  
  private static final int ADJUST_UNMUTE = 100;
  
  private static final String[] LimitGetStreamVolumePackageName;
  
  private static final int RINGER_MODE_SILENT = 0;
  
  private static final String TAG = "AudioManager";
  
  public static boolean mDebugLog = false;
  
  private static IAudioService sService;
  
  private Context mApplicationContext;
  
  private Context mOriginalContext;
  
  static {
    LimitGetStreamVolumePackageName = new String[] { "com.tencent.mobileqq", "com.tencent.mtt", "com.tencent.mm" };
  }
  
  public OppoBaseAudioManager(Context paramContext) {
    setContext(paramContext);
    if (SystemProperties.getBoolean("persist.sys.assert.panic", false))
      mDebugLog = true; 
  }
  
  public OppoBaseAudioManager() {}
  
  private Context getContext() {
    if (this.mApplicationContext == null)
      setContext(this.mOriginalContext); 
    Context context = this.mApplicationContext;
    if (context != null)
      return context; 
    return this.mOriginalContext;
  }
  
  private void setContext(Context paramContext) {
    Context context = paramContext.getApplicationContext();
    if (context != null) {
      this.mOriginalContext = null;
    } else {
      this.mOriginalContext = paramContext;
    } 
  }
  
  private static IAudioService getService() {
    IAudioService iAudioService2 = sService;
    if (iAudioService2 != null)
      return iAudioService2; 
    IBinder iBinder = ServiceManager.getService("audio");
    IAudioService iAudioService1 = IAudioService.Stub.asInterface(iBinder);
    return iAudioService1;
  }
  
  public boolean adjustStreamVolumePermission(int paramInt1, int paramInt2) {
    if (paramInt1 == 3 && paramInt2 == -100) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("get_adjustStreamVolume_control_status=");
      stringBuilder1.append(getContext().getOpPackageName());
      String str1 = stringBuilder1.toString();
      String str2 = OppoAtlasManager.getInstance(getContext()).getParameters(str1);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("adjustStreamVolume keys: ");
      stringBuilder2.append(str1);
      stringBuilder2.append(", lState = ");
      stringBuilder2.append(str2);
      Log.d("AudioManager", stringBuilder2.toString());
      if (str2 != null && str2.equalsIgnoreCase("false")) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getContext().getOpPackageName());
        stringBuilder.append("do not adjustStreamVolume music to mute when in calling");
        Log.d("AudioManager", stringBuilder.toString());
        return false;
      } 
    } 
    return true;
  }
  
  public int oppoGetStreamVolume(int paramInt) {
    IAudioService iAudioService = getService();
    try {
      StringBuilder stringBuilder2;
      String str = getContext().getOpPackageName();
      boolean bool = Arrays.<String>asList(LimitGetStreamVolumePackageName).contains(str);
      if (bool) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("get_stream_volume=getvolume-check-delay=");
        stringBuilder.append(str);
        stringBuilder.append("=");
        stringBuilder.append(paramInt);
        String str1 = stringBuilder.toString();
        str1 = OppoAtlasManager.getInstance(getContext()).getParameters(str1);
        if (str1 != null && !str1.equals("-1")) {
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("getStreamVolume too fast, volume index=");
          stringBuilder1.append(str1);
          stringBuilder1.append(", package=");
          stringBuilder1.append(str);
          Log.d("AudioManager", stringBuilder1.toString());
          return Integer.parseInt(str1);
        } 
        paramInt = stringBuilder1.getStreamVolume(paramInt);
        if (str1 != null) {
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("set_stream_volume=getvolume-check-delay=");
          stringBuilder1.append(str);
          stringBuilder1.append("=");
          stringBuilder1.append(paramInt);
          str = stringBuilder1.toString();
          OppoAtlasManager.getInstance(getContext()).getParameters(str);
        } 
        stringBuilder2 = new StringBuilder();
        this();
        stringBuilder2.append("getStreamVolume normal, volume index=");
        stringBuilder2.append(paramInt);
        Log.d("AudioManager", stringBuilder2.toString());
        return paramInt;
      } 
      int i = stringBuilder1.getStreamVolume(paramInt);
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("getStreamVolume packageName=");
      stringBuilder1.append((String)stringBuilder2);
      stringBuilder1.append(", index=");
      stringBuilder1.append(i);
      stringBuilder1.append(", streamType=");
      stringBuilder1.append(paramInt);
      Log.d("AudioManager", stringBuilder1.toString());
      return i;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int oppoGetStreamMaxVolume(int paramInt) {
    IAudioService iAudioService = getService();
    if (paramInt == 3)
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("get_listinfo_byname=oppo-getStreamMaxVolume=");
        stringBuilder.append(getContext().getOpPackageName());
        String str = stringBuilder.toString();
        str = OppoAtlasManager.getInstance(getContext()).getParameters(str);
        if (str != null) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("getStreamMaxVolume, value = ");
          stringBuilder1.append(str);
          stringBuilder1.append(" streamType ");
          stringBuilder1.append(paramInt);
          stringBuilder1.append(" from ");
          stringBuilder1.append(getContext().getOpPackageName());
          String str1 = stringBuilder1.toString();
          Log.d("AudioManager", str1);
          try {
            return Integer.parseInt(str);
          } catch (NumberFormatException numberFormatException) {
            return iAudioService.getStreamMaxVolume(paramInt);
          } 
        } 
        return iAudioService.getStreamMaxVolume(paramInt);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return remoteException.getStreamMaxVolume(paramInt);
  }
  
  public boolean setRingerModePermission(int paramInt) {
    if (paramInt == 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("call_interface_invalid=");
      stringBuilder.append(getContext().getOpPackageName());
      stringBuilder.append("=setRingerMode");
      String str = stringBuilder.toString();
      str = OppoAtlasManager.getInstance(getContext()).getParameters(str);
      if (str != null && str.equalsIgnoreCase("true"))
        return false; 
    } 
    return true;
  }
  
  private boolean isAppTopActivity(String paramString) {
    if (paramString != null) {
      IAudioService iAudioService = getService();
      try {
        String str = iAudioService.getParameters("OPLUS_AUDIO_GET_TOPAPPNAME");
        if (str != null) {
          boolean bool = str.equals(paramString);
          if (bool)
            return true; 
        } else {
          return true;
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    } 
    return false;
  }
  
  public boolean setStreamVolumePermission(int paramInt) {
    if (paramInt == 3) {
      String str1 = getContext().getOpPackageName();
      if (isAppTopActivity(str1))
        return true; 
      String str2 = OppoAtlasManager.getInstance(getContext()).getParameters("check_listinfo_byname=control=setStreamVolume");
      if (str2 != null && str2.equalsIgnoreCase("true")) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("get_volume_control_status=");
        stringBuilder1.append(str1);
        String str3 = stringBuilder1.toString();
        String str4 = OppoAtlasManager.getInstance(getContext()).getParameters(str3);
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("setStreamVolume keys: ");
        stringBuilder2.append(str3);
        stringBuilder2.append(", lState = ");
        stringBuilder2.append(str4);
        Log.d("AudioManager", stringBuilder2.toString());
        if (str4 != null && str4.equalsIgnoreCase("false")) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(str1);
          stringBuilder.append("do not setStreamVolume in playing");
          Log.d("AudioManager", stringBuilder.toString());
          return false;
        } 
      } 
    } 
    return true;
  }
  
  public boolean setSpeakerphoneOnPermission() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("get_speaker_authority=");
    stringBuilder1.append(getContext().getOpPackageName());
    String str2 = stringBuilder1.toString();
    String str1 = OppoAtlasManager.getInstance(getContext()).getParameters(str2);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("setSpeakerphoneOn keys: ");
    stringBuilder2.append(str2);
    stringBuilder2.append(", mAuthoriyStr = ");
    stringBuilder2.append(str1);
    Log.d("AudioManager", stringBuilder2.toString());
    if (str1 != null && str1.equalsIgnoreCase("false")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getContext().getOpPackageName());
      stringBuilder.append("do not have using speaker authority in call");
      Log.d("AudioManager", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  public boolean setBluetoothScoOnPermission() {
    String str = OppoAtlasManager.getInstance(getContext()).getParameters("check_listinfo_byname=control=setBluetoothScoOn");
    if (str != null && str.equalsIgnoreCase("true")) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("get_device_change_authority=");
      stringBuilder1.append(getContext().getOpPackageName());
      String str2 = stringBuilder1.toString();
      String str1 = OppoAtlasManager.getInstance(getContext()).getParameters(str2);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("setBluetoothScoOn keys: ");
      stringBuilder2.append(str2);
      stringBuilder2.append(", mAuthoriyStr = ");
      stringBuilder2.append(str1);
      Log.d("AudioManager", stringBuilder2.toString());
      if (str1 != null && str1.equalsIgnoreCase("false")) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getContext().getOpPackageName());
        stringBuilder.append("do not setBluetoothScoOn in call");
        Log.d("AudioManager", stringBuilder.toString());
        return false;
      } 
    } 
    return true;
  }
  
  public boolean setMicrophoneMutePermission() {
    String str = OppoAtlasManager.getInstance(getContext()).getParameters("check_listinfo_byname=control=setMicrophoneMute");
    if (str != null && str.equalsIgnoreCase("true")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("get_device_change_authority=");
      stringBuilder.append(getContext().getOpPackageName());
      String str1 = stringBuilder.toString();
      String str2 = OppoAtlasManager.getInstance(getContext()).getParameters(str1);
      stringBuilder = new StringBuilder();
      stringBuilder.append("setMicrophoneMute keys: ");
      stringBuilder.append(str1);
      stringBuilder.append(", mAuthoriyStr = ");
      stringBuilder.append(str2);
      Log.d("AudioManager", stringBuilder.toString());
      if (str2 != null && str2.equalsIgnoreCase("false")) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(getContext().getOpPackageName());
        stringBuilder.append(" do not setMicrophoneMute in call");
        Log.d("AudioManager", stringBuilder.toString());
        return false;
      } 
    } 
    return true;
  }
  
  public boolean setMuteGameVolumePermission(String paramString) {
    String str = OppoAtlasManager.getInstance(getContext()).getParameters("get_listinfo_byname=control=muteGameVolume");
    if (str != null && str.equalsIgnoreCase("true")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("check_listinfo_byname=oppo-muteStreamVolume=");
      stringBuilder.append(getContext().getOpPackageName());
      String str1 = stringBuilder.toString();
      str1 = OppoAtlasManager.getInstance(getContext()).getParameters(str1);
      if (str1 != null && str1.equals("true")) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("setParameters: ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" PackageName: ");
        stringBuilder1.append(getContext().getOpPackageName());
        paramString = stringBuilder1.toString();
        Log.d("AudioManager", paramString);
        return true;
      } 
      return false;
    } 
    return false;
  }
  
  public boolean setCustomApiParametersPermission(String paramString) {
    String str2 = OppoAtlasManager.getInstance(getContext()).getParameters("get_listinfo_byname=control=customApiParamSet");
    if (str2 == null || !str2.equalsIgnoreCase("true"))
      return false; 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("check_listinfo_byname=customApi-setParameters=");
    stringBuilder2.append(getContext().getOpPackageName());
    String str1 = stringBuilder2.toString();
    str1 = OppoAtlasManager.getInstance(getContext()).getParameters(str1);
    if (str1 == null || !str1.equals("true"))
      return false; 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("setParameters: ");
    stringBuilder1.append(paramString);
    stringBuilder1.append(" PackageName: ");
    stringBuilder1.append(getContext().getOpPackageName());
    paramString = stringBuilder1.toString();
    Log.d("AudioManager", paramString);
    return true;
  }
  
  public boolean setParametersPermission(String paramString) {
    if (paramString.startsWith("oppoMuteStream"))
      return setMuteGameVolumePermission(paramString); 
    boolean bool = isCustomApiParameters(paramString);
    boolean bool1 = true;
    if (bool) {
      if (!"com.oplus.customize.coreapp".equals(getContext().getOpPackageName()) && 
        !setCustomApiParametersPermission(paramString))
        bool1 = false; 
      return bool1;
    } 
    return true;
  }
  
  private boolean isCustomApiParameters(String paramString) {
    if (paramString.startsWith("OPLUS_AUDIO_SET_MUTE_PHONE") || 
      paramString.startsWith("OPLUS_AUDIO_SET_ADJUSTVOLUME_FORBID") || 
      paramString.startsWith("OPLUS_AUDIO_SET_MICROPHONE_FORBID") || 
      paramString.startsWith("record_forbid"))
      return true; 
    return false;
  }
}
