package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IRecordingConfigDispatcher extends IInterface {
  void dispatchRecordingConfigChange(List<AudioRecordingConfiguration> paramList) throws RemoteException;
  
  class Default implements IRecordingConfigDispatcher {
    public void dispatchRecordingConfigChange(List<AudioRecordingConfiguration> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecordingConfigDispatcher {
    private static final String DESCRIPTOR = "android.media.IRecordingConfigDispatcher";
    
    static final int TRANSACTION_dispatchRecordingConfigChange = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IRecordingConfigDispatcher");
    }
    
    public static IRecordingConfigDispatcher asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IRecordingConfigDispatcher");
      if (iInterface != null && iInterface instanceof IRecordingConfigDispatcher)
        return (IRecordingConfigDispatcher)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "dispatchRecordingConfigChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IRecordingConfigDispatcher");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IRecordingConfigDispatcher");
      ArrayList<AudioRecordingConfiguration> arrayList = param1Parcel1.createTypedArrayList(AudioRecordingConfiguration.CREATOR);
      dispatchRecordingConfigChange(arrayList);
      return true;
    }
    
    private static class Proxy implements IRecordingConfigDispatcher {
      public static IRecordingConfigDispatcher sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IRecordingConfigDispatcher";
      }
      
      public void dispatchRecordingConfigChange(List<AudioRecordingConfiguration> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRecordingConfigDispatcher");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRecordingConfigDispatcher.Stub.getDefaultImpl() != null) {
            IRecordingConfigDispatcher.Stub.getDefaultImpl().dispatchRecordingConfigChange(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRecordingConfigDispatcher param1IRecordingConfigDispatcher) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecordingConfigDispatcher != null) {
          Proxy.sDefaultImpl = param1IRecordingConfigDispatcher;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecordingConfigDispatcher getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
