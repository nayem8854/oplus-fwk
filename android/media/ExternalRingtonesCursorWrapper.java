package android.media;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.net.Uri;

public class ExternalRingtonesCursorWrapper extends CursorWrapper {
  private Uri mUri;
  
  public ExternalRingtonesCursorWrapper(Cursor paramCursor, Uri paramUri) {
    super(paramCursor);
    this.mUri = paramUri;
  }
  
  public String getString(int paramInt) {
    if (paramInt == 2)
      return this.mUri.toString(); 
    return super.getString(paramInt);
  }
}
