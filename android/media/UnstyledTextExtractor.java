package android.media;

import java.util.Vector;

class UnstyledTextExtractor implements Tokenizer.OnTokenListener {
  StringBuilder mLine = new StringBuilder();
  
  Vector<TextTrackCueSpan[]> mLines = (Vector)new Vector<>();
  
  Vector<TextTrackCueSpan> mCurrentLine = new Vector<>();
  
  long mLastTimestamp;
  
  UnstyledTextExtractor() {
    init();
  }
  
  private void init() {
    StringBuilder stringBuilder = this.mLine;
    stringBuilder.delete(0, stringBuilder.length());
    this.mLines.clear();
    this.mCurrentLine.clear();
    this.mLastTimestamp = -1L;
  }
  
  public void onData(String paramString) {
    this.mLine.append(paramString);
  }
  
  public void onStart(String paramString1, String[] paramArrayOfString, String paramString2) {}
  
  public void onEnd(String paramString) {}
  
  public void onTimeStamp(long paramLong) {
    if (this.mLine.length() > 0 && paramLong != this.mLastTimestamp) {
      Vector<TextTrackCueSpan> vector = this.mCurrentLine;
      StringBuilder stringBuilder2 = this.mLine;
      TextTrackCueSpan textTrackCueSpan = new TextTrackCueSpan(stringBuilder2.toString(), this.mLastTimestamp);
      vector.add(textTrackCueSpan);
      StringBuilder stringBuilder1 = this.mLine;
      stringBuilder1.delete(0, stringBuilder1.length());
    } 
    this.mLastTimestamp = paramLong;
  }
  
  public void onLineEnd() {
    if (this.mLine.length() > 0) {
      Vector<TextTrackCueSpan> vector = this.mCurrentLine;
      StringBuilder stringBuilder2 = this.mLine;
      TextTrackCueSpan textTrackCueSpan = new TextTrackCueSpan(stringBuilder2.toString(), this.mLastTimestamp);
      vector.add(textTrackCueSpan);
      StringBuilder stringBuilder1 = this.mLine;
      stringBuilder1.delete(0, stringBuilder1.length());
    } 
    TextTrackCueSpan[] arrayOfTextTrackCueSpan = new TextTrackCueSpan[this.mCurrentLine.size()];
    this.mCurrentLine.toArray(arrayOfTextTrackCueSpan);
    this.mCurrentLine.clear();
    this.mLines.add(arrayOfTextTrackCueSpan);
  }
  
  public TextTrackCueSpan[][] getText() {
    if (this.mLine.length() > 0 || this.mCurrentLine.size() > 0)
      onLineEnd(); 
    TextTrackCueSpan[][] arrayOfTextTrackCueSpan = new TextTrackCueSpan[this.mLines.size()][];
    this.mLines.toArray(arrayOfTextTrackCueSpan);
    init();
    return arrayOfTextTrackCueSpan;
  }
}
