package android.media;

import android.hardware.HardwareBuffer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Surface;
import dalvik.system.VMRuntime;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.NioUtils;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ImageReader implements AutoCloseable {
  private static final int ACQUIRE_MAX_IMAGES = 2;
  
  private static final int ACQUIRE_NO_BUFS = 1;
  
  private static final int ACQUIRE_SUCCESS = 0;
  
  private List<Image> mAcquiredImages;
  
  private final Object mCloseLock;
  
  private int mEstimatedNativeAllocBytes;
  
  private final int mFormat;
  
  private final int mHeight;
  
  private boolean mIsReaderValid;
  
  private OnImageAvailableListener mListener;
  
  private ListenerHandler mListenerHandler;
  
  private final Object mListenerLock;
  
  private final int mMaxImages;
  
  private long mNativeContext;
  
  private final int mNumPlanes;
  
  private final Surface mSurface;
  
  private final int mWidth;
  
  public static ImageReader newInstance(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    long l;
    if (paramInt3 == 34) {
      l = 0L;
    } else {
      l = 3L;
    } 
    return new ImageReader(paramInt1, paramInt2, paramInt3, paramInt4, l);
  }
  
  public static ImageReader newInstance(int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong) {
    return new ImageReader(paramInt1, paramInt2, paramInt3, paramInt4, paramLong);
  }
  
  protected ImageReader(int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong) {
    this.mListenerLock = new Object();
    this.mCloseLock = new Object();
    this.mIsReaderValid = false;
    this.mAcquiredImages = new CopyOnWriteArrayList<>();
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    this.mFormat = paramInt3;
    this.mMaxImages = paramInt4;
    if (paramInt1 >= 1 && paramInt2 >= 1) {
      if (paramInt4 >= 1) {
        if (paramInt3 != 17) {
          this.mNumPlanes = ImageUtils.getNumPlanesForFormat(paramInt3);
          nativeInit(new WeakReference<>(this), paramInt1, paramInt2, paramInt3, paramInt4, paramLong);
          this.mSurface = nativeGetSurface();
          this.mIsReaderValid = true;
          this.mEstimatedNativeAllocBytes = ImageUtils.getEstimatedNativeAllocBytes(paramInt1, paramInt2, paramInt3, 1);
          VMRuntime.getRuntime().registerNativeAllocation(this.mEstimatedNativeAllocBytes);
          return;
        } 
        throw new IllegalArgumentException("NV21 format is not supported");
      } 
      throw new IllegalArgumentException("Maximum outstanding image count must be at least 1");
    } 
    throw new IllegalArgumentException("The image dimensions must be positive");
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  public int getImageFormat() {
    return this.mFormat;
  }
  
  public int getMaxImages() {
    return this.mMaxImages;
  }
  
  public Surface getSurface() {
    return this.mSurface;
  }
  
  public Image acquireLatestImage() {
    null = acquireNextImage();
    Image image = null;
    if (null == null)
      return null; 
    try {
      while (true) {
        null = acquireNextImageNoThrowISE();
        if (null == null)
          return image; 
        image.close();
        image = null;
      } 
    } finally {
      if (image != null)
        image.close(); 
    } 
  }
  
  public Image acquireNextImageNoThrowISE() {
    SurfaceImage surfaceImage = new SurfaceImage(this.mFormat);
    if (acquireNextSurfaceImage(surfaceImage) != 0)
      surfaceImage = null; 
    return surfaceImage;
  }
  
  private int acquireNextSurfaceImage(SurfaceImage paramSurfaceImage) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mCloseLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: iconst_1
    //   8: istore_3
    //   9: aload_0
    //   10: getfield mIsReaderValid : Z
    //   13: ifeq -> 22
    //   16: aload_0
    //   17: aload_1
    //   18: invokespecial nativeImageSetup : (Landroid/media/Image;)I
    //   21: istore_3
    //   22: iload_3
    //   23: ifeq -> 77
    //   26: iload_3
    //   27: iconst_1
    //   28: if_icmpeq -> 82
    //   31: iload_3
    //   32: iconst_2
    //   33: if_icmpne -> 39
    //   36: goto -> 82
    //   39: new java/lang/AssertionError
    //   42: astore #4
    //   44: new java/lang/StringBuilder
    //   47: astore_1
    //   48: aload_1
    //   49: invokespecial <init> : ()V
    //   52: aload_1
    //   53: ldc 'Unknown nativeImageSetup return code '
    //   55: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   58: pop
    //   59: aload_1
    //   60: iload_3
    //   61: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: aload #4
    //   67: aload_1
    //   68: invokevirtual toString : ()Ljava/lang/String;
    //   71: invokespecial <init> : (Ljava/lang/Object;)V
    //   74: aload #4
    //   76: athrow
    //   77: aload_1
    //   78: iconst_1
    //   79: putfield mIsImageValid : Z
    //   82: iload_3
    //   83: ifne -> 97
    //   86: aload_0
    //   87: getfield mAcquiredImages : Ljava/util/List;
    //   90: aload_1
    //   91: invokeinterface add : (Ljava/lang/Object;)Z
    //   96: pop
    //   97: aload_2
    //   98: monitorexit
    //   99: iload_3
    //   100: ireturn
    //   101: astore_1
    //   102: aload_2
    //   103: monitorexit
    //   104: aload_1
    //   105: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #460	-> 0
    //   #462	-> 7
    //   #463	-> 9
    //   #464	-> 16
    //   #467	-> 22
    //   #474	-> 39
    //   #469	-> 77
    //   #472	-> 82
    //   #479	-> 82
    //   #480	-> 86
    //   #482	-> 97
    //   #483	-> 101
    // Exception table:
    //   from	to	target	type
    //   9	16	101	finally
    //   16	22	101	finally
    //   39	77	101	finally
    //   77	82	101	finally
    //   86	97	101	finally
    //   97	99	101	finally
    //   102	104	101	finally
  }
  
  public Image acquireNextImage() {
    StringBuilder stringBuilder;
    SurfaceImage surfaceImage = new SurfaceImage(this.mFormat);
    int i = acquireNextSurfaceImage(surfaceImage);
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown nativeImageSetup return code ");
          stringBuilder.append(i);
          throw new AssertionError(stringBuilder.toString());
        } 
        i = this.mMaxImages;
        throw new IllegalStateException(String.format("maxImages (%d) has already been acquired, call #close before acquiring more.", new Object[] { Integer.valueOf(i) }));
      } 
      return null;
    } 
    return (Image)stringBuilder;
  }
  
  private void releaseImage(Image paramImage) {
    if (paramImage instanceof SurfaceImage) {
      SurfaceImage surfaceImage = (SurfaceImage)paramImage;
      if (!surfaceImage.mIsImageValid)
        return; 
      if (surfaceImage.getReader() == this && this.mAcquiredImages.contains(paramImage)) {
        surfaceImage.clearSurfacePlanes();
        nativeReleaseImage(paramImage);
        surfaceImage.mIsImageValid = false;
        this.mAcquiredImages.remove(paramImage);
        return;
      } 
      throw new IllegalArgumentException("This image was not produced by this ImageReader");
    } 
    throw new IllegalArgumentException("This image was not produced by an ImageReader");
  }
  
  public void setOnImageAvailableListener(OnImageAvailableListener paramOnImageAvailableListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mListenerLock : Ljava/lang/Object;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 88
    //   11: aload_2
    //   12: ifnull -> 23
    //   15: aload_2
    //   16: invokevirtual getLooper : ()Landroid/os/Looper;
    //   19: astore_2
    //   20: goto -> 27
    //   23: invokestatic myLooper : ()Landroid/os/Looper;
    //   26: astore_2
    //   27: aload_2
    //   28: ifnull -> 75
    //   31: aload_0
    //   32: getfield mListenerHandler : Landroid/media/ImageReader$ListenerHandler;
    //   35: ifnull -> 49
    //   38: aload_0
    //   39: getfield mListenerHandler : Landroid/media/ImageReader$ListenerHandler;
    //   42: invokevirtual getLooper : ()Landroid/os/Looper;
    //   45: aload_2
    //   46: if_acmpeq -> 67
    //   49: new android/media/ImageReader$ListenerHandler
    //   52: astore #4
    //   54: aload #4
    //   56: aload_0
    //   57: aload_2
    //   58: invokespecial <init> : (Landroid/media/ImageReader;Landroid/os/Looper;)V
    //   61: aload_0
    //   62: aload #4
    //   64: putfield mListenerHandler : Landroid/media/ImageReader$ListenerHandler;
    //   67: aload_0
    //   68: aload_1
    //   69: putfield mListener : Landroid/media/ImageReader$OnImageAvailableListener;
    //   72: goto -> 98
    //   75: new java/lang/IllegalArgumentException
    //   78: astore_1
    //   79: aload_1
    //   80: ldc_w 'handler is null but the current thread is not a looper'
    //   83: invokespecial <init> : (Ljava/lang/String;)V
    //   86: aload_1
    //   87: athrow
    //   88: aload_0
    //   89: aconst_null
    //   90: putfield mListener : Landroid/media/ImageReader$OnImageAvailableListener;
    //   93: aload_0
    //   94: aconst_null
    //   95: putfield mListenerHandler : Landroid/media/ImageReader$ListenerHandler;
    //   98: aload_3
    //   99: monitorexit
    //   100: return
    //   101: astore_1
    //   102: aload_3
    //   103: monitorexit
    //   104: aload_1
    //   105: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #572	-> 0
    //   #573	-> 7
    //   #574	-> 11
    //   #575	-> 27
    //   #579	-> 31
    //   #580	-> 49
    //   #582	-> 67
    //   #583	-> 72
    //   #576	-> 75
    //   #584	-> 88
    //   #585	-> 93
    //   #587	-> 98
    //   #588	-> 100
    //   #587	-> 101
    // Exception table:
    //   from	to	target	type
    //   15	20	101	finally
    //   23	27	101	finally
    //   31	49	101	finally
    //   49	67	101	finally
    //   67	72	101	finally
    //   75	88	101	finally
    //   88	93	101	finally
    //   93	98	101	finally
    //   98	100	101	finally
    //   102	104	101	finally
  }
  
  public void close() {
    setOnImageAvailableListener(null, null);
    Surface surface = this.mSurface;
    if (surface != null)
      surface.release(); 
    synchronized (this.mCloseLock) {
      this.mIsReaderValid = false;
      for (Image image : this.mAcquiredImages)
        image.close(); 
      this.mAcquiredImages.clear();
      nativeClose();
      if (this.mEstimatedNativeAllocBytes > 0) {
        VMRuntime.getRuntime().registerNativeFree(this.mEstimatedNativeAllocBytes);
        this.mEstimatedNativeAllocBytes = 0;
      } 
      return;
    } 
  }
  
  public void discardFreeBuffers() {
    synchronized (this.mCloseLock) {
      nativeDiscardFreeBuffers();
      return;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  void detachImage(Image paramImage) {
    if (paramImage != null) {
      if (isImageOwnedbyMe(paramImage)) {
        SurfaceImage surfaceImage = (SurfaceImage)paramImage;
        surfaceImage.throwISEIfImageIsInvalid();
        if (!surfaceImage.isAttachable()) {
          nativeDetachImage(paramImage);
          surfaceImage.clearSurfacePlanes();
          SurfaceImage.access$102(surfaceImage, (SurfaceImage.SurfacePlane[])null);
          surfaceImage.setDetached(true);
          return;
        } 
        throw new IllegalStateException("Image was already detached from this ImageReader");
      } 
      throw new IllegalArgumentException("Trying to detach an image that is not owned by this ImageReader");
    } 
    throw new IllegalArgumentException("input image must not be null");
  }
  
  void oplusDetachImage(Image paramImage) {
    detachImage(paramImage);
  }
  
  private boolean isImageOwnedbyMe(Image paramImage) {
    boolean bool = paramImage instanceof SurfaceImage;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramImage = paramImage;
    if (paramImage.getReader() == this)
      bool1 = true; 
    return bool1;
  }
  
  private static void postEventFromNative(Object paramObject) {
    paramObject = paramObject;
    null = paramObject.get();
    if (null == null)
      return; 
    synchronized (null.mListenerLock) {
      ListenerHandler listenerHandler = null.mListenerHandler;
      if (listenerHandler != null)
        listenerHandler.sendEmptyMessage(0); 
      return;
    } 
  }
  
  class ListenerHandler extends Handler {
    final ImageReader this$0;
    
    public ListenerHandler(Looper param1Looper) {
      super(param1Looper, null, true);
    }
    
    public void handleMessage(Message param1Message) {
      synchronized (ImageReader.this.mListenerLock) {
        null = ImageReader.this.mListener;
        synchronized (ImageReader.this.mCloseLock) {
          boolean bool = ImageReader.this.mIsReaderValid;
          if (null != null && bool)
            null.onImageAvailable(ImageReader.this); 
          return;
        } 
      } 
    }
  }
  
  public static interface OnImageAvailableListener {
    void onImageAvailable(ImageReader param1ImageReader);
  }
  
  class SurfaceImage extends Image {
    private int mFormat;
    
    private AtomicBoolean mIsDetached;
    
    private long mNativeBuffer;
    
    private SurfacePlane[] mPlanes;
    
    private int mScalingMode;
    
    private long mTimestamp;
    
    private int mTransform;
    
    final ImageReader this$0;
    
    public SurfaceImage(int param1Int) {
      this.mFormat = 0;
      this.mIsDetached = new AtomicBoolean(false);
      this.mFormat = param1Int;
    }
    
    public void close() {
      ImageReader.this.releaseImage(this);
    }
    
    public ImageReader getReader() {
      return ImageReader.this;
    }
    
    public int getFormat() {
      throwISEIfImageIsInvalid();
      int i = ImageReader.this.getImageFormat();
      if (i != 34)
        i = nativeGetFormat(i); 
      this.mFormat = i;
      return i;
    }
    
    public int getWidth() {
      throwISEIfImageIsInvalid();
      int i = getFormat();
      if (i != 36 && i != 1212500294 && i != 1768253795 && i != 256 && i != 257) {
        i = nativeGetWidth();
      } else {
        i = ImageReader.this.getWidth();
      } 
      return i;
    }
    
    public int getHeight() {
      throwISEIfImageIsInvalid();
      int i = getFormat();
      if (i != 36 && i != 1212500294 && i != 1768253795 && i != 256 && i != 257) {
        i = nativeGetHeight();
      } else {
        i = ImageReader.this.getHeight();
      } 
      return i;
    }
    
    public long getTimestamp() {
      throwISEIfImageIsInvalid();
      return this.mTimestamp;
    }
    
    public int getTransform() {
      throwISEIfImageIsInvalid();
      return this.mTransform;
    }
    
    public int getScalingMode() {
      throwISEIfImageIsInvalid();
      return this.mScalingMode;
    }
    
    public HardwareBuffer getHardwareBuffer() {
      throwISEIfImageIsInvalid();
      return nativeGetHardwareBuffer();
    }
    
    public void setTimestamp(long param1Long) {
      throwISEIfImageIsInvalid();
      this.mTimestamp = param1Long;
    }
    
    public Image.Plane[] getPlanes() {
      throwISEIfImageIsInvalid();
      if (this.mPlanes == null)
        this.mPlanes = nativeCreatePlanes(ImageReader.this.mNumPlanes, ImageReader.this.mFormat); 
      return (Image.Plane[])this.mPlanes.clone();
    }
    
    protected final void finalize() throws Throwable {
      try {
        close();
        return;
      } finally {
        super.finalize();
      } 
    }
    
    boolean isAttachable() {
      throwISEIfImageIsInvalid();
      return this.mIsDetached.get();
    }
    
    ImageReader getOwner() {
      throwISEIfImageIsInvalid();
      return ImageReader.this;
    }
    
    long getNativeContext() {
      throwISEIfImageIsInvalid();
      return this.mNativeBuffer;
    }
    
    private void setDetached(boolean param1Boolean) {
      throwISEIfImageIsInvalid();
      this.mIsDetached.getAndSet(param1Boolean);
    }
    
    private void clearSurfacePlanes() {
      if (this.mIsImageValid && this.mPlanes != null) {
        byte b = 0;
        while (true) {
          SurfacePlane[] arrayOfSurfacePlane = this.mPlanes;
          if (b < arrayOfSurfacePlane.length) {
            if (arrayOfSurfacePlane[b] != null) {
              arrayOfSurfacePlane[b].clearBuffer();
              this.mPlanes[b] = null;
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
    }
    
    private synchronized native SurfacePlane[] nativeCreatePlanes(int param1Int1, int param1Int2);
    
    private synchronized native int nativeGetFormat(int param1Int);
    
    private synchronized native HardwareBuffer nativeGetHardwareBuffer();
    
    private synchronized native int nativeGetHeight();
    
    private synchronized native int nativeGetWidth();
    
    private class SurfacePlane extends Image.Plane {
      private ByteBuffer mBuffer;
      
      private final int mPixelStride;
      
      private final int mRowStride;
      
      final ImageReader.SurfaceImage this$1;
      
      private SurfacePlane(int param2Int1, int param2Int2, ByteBuffer param2ByteBuffer) {
        this.mRowStride = param2Int1;
        this.mPixelStride = param2Int2;
        this.mBuffer = param2ByteBuffer;
        param2ByteBuffer.order(ByteOrder.nativeOrder());
      }
      
      public ByteBuffer getBuffer() {
        ImageReader.SurfaceImage.this.throwISEIfImageIsInvalid();
        return this.mBuffer;
      }
      
      public int getPixelStride() {
        ImageReader.SurfaceImage.this.throwISEIfImageIsInvalid();
        if (ImageReader.this.mFormat != 36)
          return this.mPixelStride; 
        throw new UnsupportedOperationException("getPixelStride is not supported for RAW_PRIVATE plane");
      }
      
      public int getRowStride() {
        ImageReader.SurfaceImage.this.throwISEIfImageIsInvalid();
        if (ImageReader.this.mFormat != 36)
          return this.mRowStride; 
        throw new UnsupportedOperationException("getRowStride is not supported for RAW_PRIVATE plane");
      }
      
      private void clearBuffer() {
        ByteBuffer byteBuffer = this.mBuffer;
        if (byteBuffer == null)
          return; 
        if (byteBuffer.isDirect())
          NioUtils.freeDirectBuffer(this.mBuffer); 
        this.mBuffer = null;
      }
    }
  }
  
  static {
    System.loadLibrary("media_jni");
    nativeClassInit();
  }
  
  private static native void nativeClassInit();
  
  private synchronized native void nativeClose();
  
  private synchronized native int nativeDetachImage(Image paramImage);
  
  private synchronized native void nativeDiscardFreeBuffers();
  
  private synchronized native long nativeGetConsumer();
  
  private synchronized native Surface nativeGetSurface();
  
  private synchronized native int nativeImageSetup(Image paramImage);
  
  private synchronized native void nativeInit(Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong);
  
  private synchronized native void nativeReleaseImage(Image paramImage);
}
