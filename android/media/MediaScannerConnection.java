package android.media;

import android.content.ComponentName;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import com.android.internal.os.BackgroundThread;
import com.oplus.multiapp.OplusMultiAppManager;
import java.io.File;

public class MediaScannerConnection implements ServiceConnection {
  private static final String TAG = "MediaScannerConnection";
  
  private final MediaScannerConnectionClient mClient;
  
  @Deprecated
  private boolean mConnected;
  
  private final Context mContext;
  
  @Deprecated
  private final IMediaScannerListener.Stub mListener = (IMediaScannerListener.Stub)new Object(this);
  
  private ContentProviderClient mProvider;
  
  @Deprecated
  private IMediaScannerService mService;
  
  public MediaScannerConnection(Context paramContext, MediaScannerConnectionClient paramMediaScannerConnectionClient) {
    this.mContext = paramContext;
    this.mClient = paramMediaScannerConnectionClient;
  }
  
  public void connect() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mProvider : Landroid/content/ContentProviderClient;
    //   6: ifnonnull -> 43
    //   9: aload_0
    //   10: getfield mContext : Landroid/content/Context;
    //   13: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   16: astore_1
    //   17: aload_0
    //   18: aload_1
    //   19: ldc 'media'
    //   21: invokevirtual acquireContentProviderClient : (Ljava/lang/String;)Landroid/content/ContentProviderClient;
    //   24: putfield mProvider : Landroid/content/ContentProviderClient;
    //   27: aload_0
    //   28: getfield mClient : Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;
    //   31: ifnull -> 43
    //   34: aload_0
    //   35: getfield mClient : Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;
    //   38: invokeinterface onMediaScannerConnected : ()V
    //   43: aload_0
    //   44: monitorexit
    //   45: return
    //   46: astore_1
    //   47: aload_0
    //   48: monitorexit
    //   49: aload_1
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #117	-> 0
    //   #118	-> 2
    //   #119	-> 9
    //   #120	-> 17
    //   #121	-> 27
    //   #122	-> 34
    //   #125	-> 43
    //   #126	-> 45
    //   #125	-> 46
    // Exception table:
    //   from	to	target	type
    //   2	9	46	finally
    //   9	17	46	finally
    //   17	27	46	finally
    //   27	34	46	finally
    //   34	43	46	finally
    //   43	45	46	finally
    //   47	49	46	finally
  }
  
  public void disconnect() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mProvider : Landroid/content/ContentProviderClient;
    //   6: ifnull -> 21
    //   9: aload_0
    //   10: getfield mProvider : Landroid/content/ContentProviderClient;
    //   13: invokevirtual close : ()V
    //   16: aload_0
    //   17: aconst_null
    //   18: putfield mProvider : Landroid/content/ContentProviderClient;
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #132	-> 0
    //   #133	-> 2
    //   #134	-> 9
    //   #135	-> 16
    //   #137	-> 21
    //   #138	-> 23
    //   #137	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	9	24	finally
    //   9	16	24	finally
    //   16	21	24	finally
    //   21	23	24	finally
    //   25	27	24	finally
  }
  
  public boolean isConnected() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mProvider : Landroid/content/ContentProviderClient;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull -> 16
    //   11: iconst_1
    //   12: istore_2
    //   13: goto -> 18
    //   16: iconst_0
    //   17: istore_2
    //   18: aload_0
    //   19: monitorexit
    //   20: iload_2
    //   21: ireturn
    //   22: astore_1
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_1
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #145	-> 2
    //   #145	-> 22
    // Exception table:
    //   from	to	target	type
    //   2	7	22	finally
  }
  
  public void scanFile(String paramString1, String paramString2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mProvider : Landroid/content/ContentProviderClient;
    //   6: ifnull -> 33
    //   9: invokestatic getExecutor : ()Ljava/util/concurrent/Executor;
    //   12: astore_3
    //   13: new android/media/_$$Lambda$MediaScannerConnection$C5t4jecvX7xM6RyDB22R5_79npM
    //   16: astore_2
    //   17: aload_2
    //   18: aload_0
    //   19: aload_1
    //   20: invokespecial <init> : (Landroid/media/MediaScannerConnection;Ljava/lang/String;)V
    //   23: aload_3
    //   24: aload_2
    //   25: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   30: aload_0
    //   31: monitorexit
    //   32: return
    //   33: new java/lang/IllegalStateException
    //   36: astore_1
    //   37: aload_1
    //   38: ldc 'not connected to MediaScannerService'
    //   40: invokespecial <init> : (Ljava/lang/String;)V
    //   43: aload_1
    //   44: athrow
    //   45: astore_1
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_1
    //   49: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #158	-> 0
    //   #159	-> 2
    //   #162	-> 9
    //   #166	-> 30
    //   #167	-> 32
    //   #160	-> 33
    //   #166	-> 45
    // Exception table:
    //   from	to	target	type
    //   2	9	45	finally
    //   9	30	45	finally
    //   30	32	45	finally
    //   33	45	45	finally
    //   46	48	45	finally
  }
  
  public static void scanFile(Context paramContext, String[] paramArrayOfString1, String[] paramArrayOfString2, OnScanCompletedListener paramOnScanCompletedListener) {
    BackgroundThread.getExecutor().execute(new _$$Lambda$MediaScannerConnection$X7K_c7l7bfQR6Mg9eCJVPmLMn6I(paramContext, paramArrayOfString1, paramOnScanCompletedListener));
  }
  
  private static Uri scanFileQuietly(ContentProviderClient paramContentProviderClient, File paramFile) {
    Uri uri1;
    try {
      OplusMultiAppManager oplusMultiAppManager = OplusMultiAppManager.getInstance();
      int i = ContentResolver.wrap(paramContentProviderClient).getUserId();
      String str = paramFile.getAbsolutePath();
      oplusMultiAppManager.scanFileIfNeed(i, str);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("scanFileIfNeed Failed to scan ");
      stringBuilder.append(paramFile);
      stringBuilder.append(": ");
      stringBuilder.append(exception);
      Log.w("MediaScannerConnection", stringBuilder.toString());
    } 
    Uri uri2 = null;
    try {
      uri1 = MediaStore.scanFile(ContentResolver.wrap(paramContentProviderClient), paramFile.getCanonicalFile());
      uri2 = uri1;
      StringBuilder stringBuilder = new StringBuilder();
      uri2 = uri1;
      this();
      uri2 = uri1;
      stringBuilder.append("Scanned ");
      uri2 = uri1;
      stringBuilder.append(paramFile);
      uri2 = uri1;
      stringBuilder.append(" to ");
      uri2 = uri1;
      stringBuilder.append(uri1);
      uri2 = uri1;
      Log.d("MediaScannerConnection", stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to scan ");
      stringBuilder.append(paramFile);
      stringBuilder.append(": ");
      stringBuilder.append(exception);
      Log.w("MediaScannerConnection", stringBuilder.toString());
      uri1 = uri2;
    } 
    return uri1;
  }
  
  private static void runCallBack(Context paramContext, OnScanCompletedListener paramOnScanCompletedListener, String paramString, Uri paramUri) {
    if (paramOnScanCompletedListener != null)
      try {
        paramOnScanCompletedListener.onScanCompleted(paramString, paramUri);
      } finally {
        paramOnScanCompletedListener = null;
      }  
  }
  
  @Deprecated
  class ClientProxy implements MediaScannerConnectionClient {
    final MediaScannerConnection.OnScanCompletedListener mClient;
    
    MediaScannerConnection mConnection;
    
    final String[] mMimeTypes;
    
    int mNextPath;
    
    final String[] mPaths;
    
    ClientProxy(MediaScannerConnection this$0, String[] param1ArrayOfString1, MediaScannerConnection.OnScanCompletedListener param1OnScanCompletedListener) {
      this.mPaths = (String[])this$0;
      this.mMimeTypes = param1ArrayOfString1;
      this.mClient = param1OnScanCompletedListener;
    }
    
    public void onMediaScannerConnected() {}
    
    public void onScanCompleted(String param1String, Uri param1Uri) {}
    
    void scanNextPath() {}
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder) {}
  
  public void onServiceDisconnected(ComponentName paramComponentName) {}
  
  public static interface MediaScannerConnectionClient extends OnScanCompletedListener {
    void onMediaScannerConnected();
  }
  
  class OnScanCompletedListener {
    public abstract void onScanCompleted(String param1String, Uri param1Uri);
  }
}
