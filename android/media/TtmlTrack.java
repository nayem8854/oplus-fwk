package android.media;

import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;
import org.xmlpull.v1.XmlPullParserException;

class TtmlTrack extends SubtitleTrack implements TtmlNodeListener {
  private static final String TAG = "TtmlTrack";
  
  private Long mCurrentRunID;
  
  private final TtmlParser mParser = new TtmlParser(this);
  
  private String mParsingData;
  
  private final TtmlRenderingWidget mRenderingWidget;
  
  private TtmlNode mRootNode;
  
  private final TreeSet<Long> mTimeEvents;
  
  private final LinkedList<TtmlNode> mTtmlNodes;
  
  TtmlTrack(TtmlRenderingWidget paramTtmlRenderingWidget, MediaFormat paramMediaFormat) {
    super(paramMediaFormat);
    this.mTtmlNodes = new LinkedList<>();
    this.mTimeEvents = new TreeSet<>();
    this.mRenderingWidget = paramTtmlRenderingWidget;
    this.mParsingData = "";
  }
  
  public TtmlRenderingWidget getRenderingWidget() {
    return this.mRenderingWidget;
  }
  
  public void onData(byte[] paramArrayOfbyte, boolean paramBoolean, long paramLong) {
    try {
      null = new String();
      this(paramArrayOfbyte, "UTF-8");
      synchronized (this.mParser) {
        if (this.mCurrentRunID == null || paramLong == this.mCurrentRunID.longValue()) {
          this.mCurrentRunID = Long.valueOf(paramLong);
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(this.mParsingData);
          stringBuilder1.append(null);
          this.mParsingData = null = stringBuilder1.toString();
          if (paramBoolean) {
            try {
              this.mParser.parse(null, this.mCurrentRunID.longValue());
            } catch (XmlPullParserException xmlPullParserException) {
              xmlPullParserException.printStackTrace();
            } catch (IOException iOException) {
              iOException.printStackTrace();
            } 
            finishedRun(paramLong);
            this.mParsingData = "";
            this.mCurrentRunID = null;
          } 
          return;
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Run #");
        stringBuilder.append(this.mCurrentRunID);
        stringBuilder.append(" in progress.  Cannot process run #");
        stringBuilder.append(paramLong);
        this(stringBuilder.toString());
        throw illegalStateException;
      } 
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("subtitle data is not UTF-8 encoded: ");
      stringBuilder.append(unsupportedEncodingException);
      Log.w("TtmlTrack", stringBuilder.toString());
    } 
  }
  
  public void onTtmlNodeParsed(TtmlNode paramTtmlNode) {
    this.mTtmlNodes.addLast(paramTtmlNode);
    addTimeEvents(paramTtmlNode);
  }
  
  public void onRootNodeParsed(TtmlNode paramTtmlNode) {
    this.mRootNode = paramTtmlNode;
    while (true) {
      TtmlCue ttmlCue = getNextResult();
      if (ttmlCue != null) {
        addCue(ttmlCue);
        continue;
      } 
      break;
    } 
    this.mRootNode = null;
    this.mTtmlNodes.clear();
    this.mTimeEvents.clear();
  }
  
  public void updateView(Vector<SubtitleTrack.Cue> paramVector) {
    if (!this.mVisible)
      return; 
    if (this.DEBUG && this.mTimeProvider != null)
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("at ");
        MediaTimeProvider mediaTimeProvider = this.mTimeProvider;
        stringBuilder.append(mediaTimeProvider.getCurrentTimeUs(false, true) / 1000L);
        stringBuilder.append(" ms the active cues are:");
        String str = stringBuilder.toString();
        Log.d("TtmlTrack", str);
      } catch (IllegalStateException illegalStateException) {
        Log.d("TtmlTrack", "at (illegal state) the active cues are:");
      }  
    this.mRenderingWidget.setActiveCues(paramVector);
  }
  
  public TtmlCue getNextResult() {
    while (this.mTimeEvents.size() >= 2) {
      long l1 = ((Long)this.mTimeEvents.pollFirst()).longValue();
      long l2 = ((Long)this.mTimeEvents.first()).longValue();
      List<TtmlNode> list = getActiveNodes(l1, l2);
      if (!list.isEmpty()) {
        TtmlNode ttmlNode = this.mRootNode;
        String str = TtmlUtils.applySpacePolicy(TtmlUtils.extractText(ttmlNode, l1, l2), false);
        ttmlNode = this.mRootNode;
        return new TtmlCue(l1, l2, str, TtmlUtils.extractTtmlFragment(ttmlNode, l1, l2));
      } 
    } 
    return null;
  }
  
  private void addTimeEvents(TtmlNode paramTtmlNode) {
    this.mTimeEvents.add(Long.valueOf(paramTtmlNode.mStartTimeMs));
    this.mTimeEvents.add(Long.valueOf(paramTtmlNode.mEndTimeMs));
    for (byte b = 0; b < paramTtmlNode.mChildren.size(); b++)
      addTimeEvents(paramTtmlNode.mChildren.get(b)); 
  }
  
  private List<TtmlNode> getActiveNodes(long paramLong1, long paramLong2) {
    ArrayList<TtmlNode> arrayList = new ArrayList();
    for (byte b = 0; b < this.mTtmlNodes.size(); b++) {
      TtmlNode ttmlNode = this.mTtmlNodes.get(b);
      if (ttmlNode.isActive(paramLong1, paramLong2))
        arrayList.add(ttmlNode); 
    } 
    return arrayList;
  }
}
