package android.media;

import java.io.IOException;
import java.io.InputStream;

public final class ResampleInputStream extends InputStream {
  private static final String TAG = "ResampleInputStream";
  
  private static final int mFirLength = 29;
  
  private byte[] mBuf;
  
  private int mBufCount;
  
  private InputStream mInputStream;
  
  static {
    System.loadLibrary("media_jni");
  }
  
  private final byte[] mOneByte = new byte[1];
  
  private final int mRateIn;
  
  private final int mRateOut;
  
  public ResampleInputStream(InputStream paramInputStream, int paramInt1, int paramInt2) {
    if (paramInt1 == paramInt2 * 2) {
      this.mInputStream = paramInputStream;
      this.mRateIn = 2;
      this.mRateOut = 1;
      return;
    } 
    throw new IllegalArgumentException("only support 2:1 at the moment");
  }
  
  public int read() throws IOException {
    int i = read(this.mOneByte, 0, 1);
    if (i == 1) {
      i = this.mOneByte[0] & 0xFF;
    } else {
      i = -1;
    } 
    return i;
  }
  
  public int read(byte[] paramArrayOfbyte) throws IOException {
    return read(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public int read(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    if (this.mInputStream != null) {
      int i = (paramInt2 / 2 * this.mRateIn / this.mRateOut + 29) * 2;
      byte[] arrayOfByte = this.mBuf;
      if (arrayOfByte == null) {
        this.mBuf = new byte[i];
      } else if (i > arrayOfByte.length) {
        byte[] arrayOfByte1 = new byte[i];
        System.arraycopy(arrayOfByte, 0, arrayOfByte1, 0, this.mBufCount);
        this.mBuf = arrayOfByte1;
      } 
      while (true) {
        int j = this.mBufCount;
        i = (j / 2 - 29) * this.mRateOut / this.mRateIn * 2;
        if (i > 0) {
          if (i < paramInt2) {
            paramInt2 = i;
          } else {
            paramInt2 = paramInt2 / 2 * 2;
          } 
          fir21(this.mBuf, 0, paramArrayOfbyte, paramInt1, paramInt2 / 2);
          i = this.mRateIn * paramInt2 / this.mRateOut;
          this.mBufCount = paramInt1 = this.mBufCount - i;
          if (paramInt1 > 0) {
            paramArrayOfbyte = this.mBuf;
            System.arraycopy(paramArrayOfbyte, i, paramArrayOfbyte, 0, paramInt1);
          } 
          return paramInt2;
        } 
        InputStream inputStream = this.mInputStream;
        arrayOfByte = this.mBuf;
        i = inputStream.read(arrayOfByte, j, arrayOfByte.length - j);
        if (i == -1)
          return -1; 
        this.mBufCount += i;
      } 
    } 
    throw new IllegalStateException("not open");
  }
  
  public void close() throws IOException {
    try {
      if (this.mInputStream != null)
        this.mInputStream.close(); 
      return;
    } finally {
      this.mInputStream = null;
    } 
  }
  
  protected void finalize() throws Throwable {
    if (this.mInputStream == null)
      return; 
    close();
    throw new IllegalStateException("someone forgot to close ResampleInputStream");
  }
  
  private static native void fir21(byte[] paramArrayOfbyte1, int paramInt1, byte[] paramArrayOfbyte2, int paramInt2, int paramInt3);
}
