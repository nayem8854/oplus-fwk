package android.media;

import android.annotation.SystemApi;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class AudioPlaybackConfiguration implements Parcelable {
  public static final Parcelable.Creator<AudioPlaybackConfiguration> CREATOR;
  
  private static final boolean DEBUG = false;
  
  public static final int PLAYER_PIID_INVALID = -1;
  
  @SystemApi
  public static final int PLAYER_STATE_IDLE = 1;
  
  @SystemApi
  public static final int PLAYER_STATE_PAUSED = 3;
  
  @SystemApi
  public static final int PLAYER_STATE_RELEASED = 0;
  
  @SystemApi
  public static final int PLAYER_STATE_STARTED = 2;
  
  @SystemApi
  public static final int PLAYER_STATE_STOPPED = 4;
  
  @SystemApi
  public static final int PLAYER_STATE_UNKNOWN = -1;
  
  public static final int PLAYER_TYPE_AAUDIO = 13;
  
  public static final int PLAYER_TYPE_EXTERNAL_PROXY = 15;
  
  public static final int PLAYER_TYPE_HW_SOURCE = 14;
  
  @SystemApi
  public static final int PLAYER_TYPE_JAM_AUDIOTRACK = 1;
  
  @SystemApi
  public static final int PLAYER_TYPE_JAM_MEDIAPLAYER = 2;
  
  @SystemApi
  public static final int PLAYER_TYPE_JAM_SOUNDPOOL = 3;
  
  @SystemApi
  public static final int PLAYER_TYPE_SLES_AUDIOPLAYER_BUFFERQUEUE = 11;
  
  @SystemApi
  public static final int PLAYER_TYPE_SLES_AUDIOPLAYER_URI_FD = 12;
  
  @SystemApi
  public static final int PLAYER_TYPE_UNKNOWN = -1;
  
  public static final int PLAYER_UPID_INVALID = -1;
  
  private static final String TAG = new String("AudioPlaybackConfiguration");
  
  public static PlayerDeathMonitor sPlayerDeathMonitor;
  
  private int mClientPid;
  
  private int mClientUid;
  
  private IPlayerShell mIPlayerShell;
  
  private AudioAttributes mPlayerAttr;
  
  private final int mPlayerIId;
  
  private int mPlayerState;
  
  private int mPlayerType;
  
  private AudioPlaybackConfiguration(int paramInt) {
    this.mPlayerIId = paramInt;
    this.mIPlayerShell = null;
  }
  
  public AudioPlaybackConfiguration(PlayerBase.PlayerIdCard paramPlayerIdCard, int paramInt1, int paramInt2, int paramInt3) {
    this.mPlayerIId = paramInt1;
    this.mPlayerType = paramPlayerIdCard.mPlayerType;
    this.mClientUid = paramInt2;
    this.mClientPid = paramInt3;
    this.mPlayerState = 1;
    this.mPlayerAttr = paramPlayerIdCard.mAttributes;
    if (sPlayerDeathMonitor != null && paramPlayerIdCard.mIPlayer != null) {
      this.mIPlayerShell = new IPlayerShell(this, paramPlayerIdCard.mIPlayer);
    } else {
      this.mIPlayerShell = null;
    } 
  }
  
  public void init() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIPlayerShell : Landroid/media/AudioPlaybackConfiguration$IPlayerShell;
    //   6: ifnull -> 16
    //   9: aload_0
    //   10: getfield mIPlayerShell : Landroid/media/AudioPlaybackConfiguration$IPlayerShell;
    //   13: invokevirtual monitorDeath : ()V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #218	-> 0
    //   #219	-> 2
    //   #220	-> 9
    //   #222	-> 16
    //   #223	-> 18
    //   #222	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	9	19	finally
    //   9	16	19	finally
    //   16	18	19	finally
    //   20	22	19	finally
  }
  
  public static AudioPlaybackConfiguration anonymizedCopy(AudioPlaybackConfiguration paramAudioPlaybackConfiguration) {
    AudioPlaybackConfiguration audioPlaybackConfiguration = new AudioPlaybackConfiguration(paramAudioPlaybackConfiguration.mPlayerIId);
    audioPlaybackConfiguration.mPlayerState = paramAudioPlaybackConfiguration.mPlayerState;
    AudioAttributes.Builder builder3 = new AudioAttributes.Builder();
    AudioAttributes audioAttributes2 = paramAudioPlaybackConfiguration.mPlayerAttr;
    AudioAttributes.Builder builder4 = builder3.setUsage(audioAttributes2.getUsage());
    AudioAttributes audioAttributes1 = paramAudioPlaybackConfiguration.mPlayerAttr;
    builder4 = builder4.setContentType(audioAttributes1.getContentType());
    audioAttributes1 = paramAudioPlaybackConfiguration.mPlayerAttr;
    AudioAttributes.Builder builder2 = builder4.setFlags(audioAttributes1.getFlags());
    int i = paramAudioPlaybackConfiguration.mPlayerAttr.getAllowedCapturePolicy();
    byte b = 1;
    if (i != 1)
      b = 3; 
    AudioAttributes.Builder builder1 = builder2.setAllowedCapturePolicy(b);
    audioPlaybackConfiguration.mPlayerAttr = builder1.build();
    audioPlaybackConfiguration.mPlayerType = -1;
    audioPlaybackConfiguration.mClientUid = -1;
    audioPlaybackConfiguration.mClientPid = -1;
    audioPlaybackConfiguration.mIPlayerShell = null;
    return audioPlaybackConfiguration;
  }
  
  public AudioAttributes getAudioAttributes() {
    return this.mPlayerAttr;
  }
  
  @SystemApi
  public int getClientUid() {
    return this.mClientUid;
  }
  
  @SystemApi
  public int getClientPid() {
    return this.mClientPid;
  }
  
  @SystemApi
  public int getPlayerType() {
    int i = this.mPlayerType;
    switch (i) {
      default:
        return i;
      case 13:
      case 14:
      case 15:
        break;
    } 
    return -1;
  }
  
  @SystemApi
  public int getPlayerState() {
    return this.mPlayerState;
  }
  
  @SystemApi
  public int getPlayerInterfaceId() {
    return this.mPlayerIId;
  }
  
  @SystemApi
  public PlayerProxy getPlayerProxy() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIPlayerShell : Landroid/media/AudioPlaybackConfiguration$IPlayerShell;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: ifnonnull -> 18
    //   13: aconst_null
    //   14: astore_1
    //   15: goto -> 27
    //   18: new android/media/PlayerProxy
    //   21: dup
    //   22: aload_0
    //   23: invokespecial <init> : (Landroid/media/AudioPlaybackConfiguration;)V
    //   26: astore_1
    //   27: aload_1
    //   28: areturn
    //   29: astore_1
    //   30: aload_0
    //   31: monitorexit
    //   32: aload_1
    //   33: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #334	-> 0
    //   #335	-> 2
    //   #336	-> 7
    //   #337	-> 9
    //   #336	-> 29
    // Exception table:
    //   from	to	target	type
    //   2	7	29	finally
    //   7	9	29	finally
    //   30	32	29	finally
  }
  
  IPlayer getIPlayer() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIPlayerShell : Landroid/media/AudioPlaybackConfiguration$IPlayerShell;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: ifnonnull -> 18
    //   13: aconst_null
    //   14: astore_1
    //   15: goto -> 23
    //   18: aload_1
    //   19: invokevirtual getIPlayer : ()Landroid/media/IPlayer;
    //   22: astore_1
    //   23: aload_1
    //   24: areturn
    //   25: astore_1
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_1
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #346	-> 0
    //   #347	-> 2
    //   #348	-> 7
    //   #349	-> 9
    //   #348	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	7	25	finally
    //   7	9	25	finally
    //   26	28	25	finally
  }
  
  public boolean handleAudioAttributesEvent(AudioAttributes paramAudioAttributes) {
    boolean bool = paramAudioAttributes.equals(this.mPlayerAttr);
    this.mPlayerAttr = paramAudioAttributes;
    return bool ^ true;
  }
  
  public boolean handleStateEvent(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPlayerState : I
    //   6: iload_1
    //   7: if_icmpeq -> 15
    //   10: iconst_1
    //   11: istore_2
    //   12: goto -> 17
    //   15: iconst_0
    //   16: istore_2
    //   17: aload_0
    //   18: iload_1
    //   19: putfield mPlayerState : I
    //   22: iload_2
    //   23: ifeq -> 49
    //   26: iload_1
    //   27: ifne -> 49
    //   30: aload_0
    //   31: getfield mIPlayerShell : Landroid/media/AudioPlaybackConfiguration$IPlayerShell;
    //   34: ifnull -> 49
    //   37: aload_0
    //   38: getfield mIPlayerShell : Landroid/media/AudioPlaybackConfiguration$IPlayerShell;
    //   41: invokevirtual release : ()V
    //   44: aload_0
    //   45: aconst_null
    //   46: putfield mIPlayerShell : Landroid/media/AudioPlaybackConfiguration$IPlayerShell;
    //   49: aload_0
    //   50: monitorexit
    //   51: iload_2
    //   52: ireturn
    //   53: astore_3
    //   54: aload_0
    //   55: monitorexit
    //   56: aload_3
    //   57: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #371	-> 0
    //   #372	-> 2
    //   #373	-> 17
    //   #374	-> 22
    //   #375	-> 37
    //   #376	-> 44
    //   #378	-> 49
    //   #379	-> 51
    //   #378	-> 53
    // Exception table:
    //   from	to	target	type
    //   2	10	53	finally
    //   17	22	53	finally
    //   30	37	53	finally
    //   37	44	53	finally
    //   44	49	53	finally
    //   49	51	53	finally
    //   54	56	53	finally
  }
  
  private void playerDied() {
    PlayerDeathMonitor playerDeathMonitor = sPlayerDeathMonitor;
    if (playerDeathMonitor != null)
      playerDeathMonitor.playerDeath(this.mPlayerIId); 
  }
  
  @SystemApi
  public boolean isActive() {
    if (this.mPlayerState != 2)
      return false; 
    return true;
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("  ");
    stringBuilder.append(this);
    paramPrintWriter.println(stringBuilder.toString());
  }
  
  static {
    CREATOR = new Parcelable.Creator<AudioPlaybackConfiguration>() {
        public AudioPlaybackConfiguration createFromParcel(Parcel param1Parcel) {
          return new AudioPlaybackConfiguration(param1Parcel);
        }
        
        public AudioPlaybackConfiguration[] newArray(int param1Int) {
          return new AudioPlaybackConfiguration[param1Int];
        }
      };
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mPlayerIId), Integer.valueOf(this.mPlayerType), Integer.valueOf(this.mClientUid), Integer.valueOf(this.mClientPid) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    // Byte code:
    //   0: aload_1
    //   1: aload_0
    //   2: getfield mPlayerIId : I
    //   5: invokevirtual writeInt : (I)V
    //   8: aload_1
    //   9: aload_0
    //   10: getfield mPlayerType : I
    //   13: invokevirtual writeInt : (I)V
    //   16: aload_1
    //   17: aload_0
    //   18: getfield mClientUid : I
    //   21: invokevirtual writeInt : (I)V
    //   24: aload_1
    //   25: aload_0
    //   26: getfield mClientPid : I
    //   29: invokevirtual writeInt : (I)V
    //   32: aload_1
    //   33: aload_0
    //   34: getfield mPlayerState : I
    //   37: invokevirtual writeInt : (I)V
    //   40: aload_0
    //   41: getfield mPlayerAttr : Landroid/media/AudioAttributes;
    //   44: aload_1
    //   45: iconst_0
    //   46: invokevirtual writeToParcel : (Landroid/os/Parcel;I)V
    //   49: aload_0
    //   50: monitorenter
    //   51: aload_0
    //   52: getfield mIPlayerShell : Landroid/media/AudioPlaybackConfiguration$IPlayerShell;
    //   55: astore_3
    //   56: aload_0
    //   57: monitorexit
    //   58: aload_3
    //   59: ifnonnull -> 67
    //   62: aconst_null
    //   63: astore_3
    //   64: goto -> 72
    //   67: aload_3
    //   68: invokevirtual getIPlayer : ()Landroid/media/IPlayer;
    //   71: astore_3
    //   72: aload_1
    //   73: aload_3
    //   74: invokevirtual writeStrongInterface : (Landroid/os/IInterface;)V
    //   77: return
    //   78: astore_1
    //   79: aload_0
    //   80: monitorexit
    //   81: aload_1
    //   82: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #454	-> 0
    //   #455	-> 8
    //   #456	-> 16
    //   #457	-> 24
    //   #458	-> 32
    //   #459	-> 40
    //   #461	-> 49
    //   #462	-> 51
    //   #463	-> 56
    //   #464	-> 58
    //   #465	-> 77
    //   #463	-> 78
    // Exception table:
    //   from	to	target	type
    //   51	56	78	finally
    //   56	58	78	finally
    //   79	81	78	finally
  }
  
  private AudioPlaybackConfiguration(Parcel paramParcel) {
    IPlayerShell iPlayerShell;
    this.mPlayerIId = paramParcel.readInt();
    this.mPlayerType = paramParcel.readInt();
    this.mClientUid = paramParcel.readInt();
    this.mClientPid = paramParcel.readInt();
    this.mPlayerState = paramParcel.readInt();
    this.mPlayerAttr = AudioAttributes.CREATOR.createFromParcel(paramParcel);
    IPlayer iPlayer = IPlayer.Stub.asInterface(paramParcel.readStrongBinder());
    paramParcel = null;
    if (iPlayer != null)
      iPlayerShell = new IPlayerShell(null, iPlayer); 
    this.mIPlayerShell = iPlayerShell;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || !(paramObject instanceof AudioPlaybackConfiguration))
      return false; 
    paramObject = paramObject;
    if (this.mPlayerIId != ((AudioPlaybackConfiguration)paramObject).mPlayerIId || this.mPlayerType != ((AudioPlaybackConfiguration)paramObject).mPlayerType || this.mClientUid != ((AudioPlaybackConfiguration)paramObject).mClientUid || this.mClientPid != ((AudioPlaybackConfiguration)paramObject).mClientPid)
      bool = false; 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AudioPlaybackConfiguration piid:");
    stringBuilder.append(this.mPlayerIId);
    stringBuilder.append(" type:");
    int i = this.mPlayerType;
    stringBuilder.append(toLogFriendlyPlayerType(i));
    stringBuilder.append(" u/pid:");
    stringBuilder.append(this.mClientUid);
    stringBuilder.append("/");
    stringBuilder.append(this.mClientPid);
    stringBuilder.append(" state:");
    i = this.mPlayerState;
    stringBuilder.append(toLogFriendlyPlayerState(i));
    stringBuilder.append(" attr:");
    stringBuilder.append(this.mPlayerAttr);
    return stringBuilder.toString();
  }
  
  static final class IPlayerShell implements IBinder.DeathRecipient {
    private volatile IPlayer mIPlayer;
    
    final AudioPlaybackConfiguration mMonitor;
    
    IPlayerShell(AudioPlaybackConfiguration param1AudioPlaybackConfiguration, IPlayer param1IPlayer) {
      this.mMonitor = param1AudioPlaybackConfiguration;
      this.mIPlayer = param1IPlayer;
    }
    
    void monitorDeath() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mIPlayer : Landroid/media/IPlayer;
      //   6: astore_1
      //   7: aload_1
      //   8: ifnonnull -> 14
      //   11: aload_0
      //   12: monitorexit
      //   13: return
      //   14: aload_0
      //   15: getfield mIPlayer : Landroid/media/IPlayer;
      //   18: invokeinterface asBinder : ()Landroid/os/IBinder;
      //   23: aload_0
      //   24: iconst_0
      //   25: invokeinterface linkToDeath : (Landroid/os/IBinder$DeathRecipient;I)V
      //   30: goto -> 95
      //   33: astore_2
      //   34: aload_0
      //   35: getfield mMonitor : Landroid/media/AudioPlaybackConfiguration;
      //   38: ifnull -> 85
      //   41: invokestatic access$100 : ()Ljava/lang/String;
      //   44: astore_1
      //   45: new java/lang/StringBuilder
      //   48: astore_3
      //   49: aload_3
      //   50: invokespecial <init> : ()V
      //   53: aload_3
      //   54: ldc 'Could not link to client death for piid='
      //   56: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   59: pop
      //   60: aload_3
      //   61: aload_0
      //   62: getfield mMonitor : Landroid/media/AudioPlaybackConfiguration;
      //   65: invokestatic access$200 : (Landroid/media/AudioPlaybackConfiguration;)I
      //   68: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   71: pop
      //   72: aload_1
      //   73: aload_3
      //   74: invokevirtual toString : ()Ljava/lang/String;
      //   77: aload_2
      //   78: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   81: pop
      //   82: goto -> 95
      //   85: invokestatic access$100 : ()Ljava/lang/String;
      //   88: ldc 'Could not link to client death'
      //   90: aload_2
      //   91: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   94: pop
      //   95: aload_0
      //   96: monitorexit
      //   97: return
      //   98: astore_1
      //   99: aload_0
      //   100: monitorexit
      //   101: aload_1
      //   102: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #513	-> 2
      //   #514	-> 11
      //   #517	-> 14
      //   #524	-> 30
      //   #518	-> 33
      //   #519	-> 34
      //   #520	-> 41
      //   #522	-> 85
      //   #525	-> 95
      //   #512	-> 98
      // Exception table:
      //   from	to	target	type
      //   2	7	98	finally
      //   14	30	33	android/os/RemoteException
      //   14	30	98	finally
      //   34	41	98	finally
      //   41	82	98	finally
      //   85	95	98	finally
    }
    
    IPlayer getIPlayer() {
      return this.mIPlayer;
    }
    
    public void binderDied() {
      AudioPlaybackConfiguration audioPlaybackConfiguration = this.mMonitor;
      if (audioPlaybackConfiguration != null)
        audioPlaybackConfiguration.playerDied(); 
    }
    
    void release() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mIPlayer : Landroid/media/IPlayer;
      //   6: astore_1
      //   7: aload_1
      //   8: ifnonnull -> 14
      //   11: aload_0
      //   12: monitorexit
      //   13: return
      //   14: aload_0
      //   15: getfield mIPlayer : Landroid/media/IPlayer;
      //   18: invokeinterface asBinder : ()Landroid/os/IBinder;
      //   23: aload_0
      //   24: iconst_0
      //   25: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
      //   30: pop
      //   31: aload_0
      //   32: aconst_null
      //   33: putfield mIPlayer : Landroid/media/IPlayer;
      //   36: invokestatic flushPendingCommands : ()V
      //   39: aload_0
      //   40: monitorexit
      //   41: return
      //   42: astore_1
      //   43: aload_0
      //   44: monitorexit
      //   45: aload_1
      //   46: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #539	-> 2
      //   #540	-> 11
      //   #542	-> 14
      //   #543	-> 31
      //   #544	-> 36
      //   #545	-> 39
      //   #538	-> 42
      // Exception table:
      //   from	to	target	type
      //   2	7	42	finally
      //   14	31	42	finally
      //   31	36	42	finally
      //   36	39	42	finally
    }
  }
  
  public static String toLogFriendlyPlayerType(int paramInt) {
    if (paramInt != -1) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            StringBuilder stringBuilder;
            switch (paramInt) {
              default:
                stringBuilder = new StringBuilder();
                stringBuilder.append("unknown player type ");
                stringBuilder.append(paramInt);
                stringBuilder.append(" - FIXME");
                return stringBuilder.toString();
              case 15:
                return "external proxy";
              case 14:
                return "hardware source";
              case 13:
                return "AAudio";
              case 12:
                return "OpenSL ES AudioPlayer (URI/FD)";
              case 11:
                break;
            } 
            return "OpenSL ES AudioPlayer (Buffer Queue)";
          } 
          return "android.media.SoundPool";
        } 
        return "android.media.MediaPlayer";
      } 
      return "android.media.AudioTrack";
    } 
    return "unknown";
  }
  
  public static String toLogFriendlyPlayerState(int paramInt) {
    if (paramInt != -1) {
      if (paramInt != 0) {
        if (paramInt != 1) {
          if (paramInt != 2) {
            if (paramInt != 3) {
              if (paramInt != 4)
                return "unknown player state - FIXME"; 
              return "stopped";
            } 
            return "paused";
          } 
          return "started";
        } 
        return "idle";
      } 
      return "released";
    } 
    return "unknown";
  }
  
  class PlayerDeathMonitor {
    public abstract void playerDeath(int param1Int);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class PlayerState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PlayerType implements Annotation {}
}
