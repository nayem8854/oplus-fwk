package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IPlaybackConfigDispatcher extends IInterface {
  void dispatchPlaybackConfigChange(List<AudioPlaybackConfiguration> paramList, boolean paramBoolean) throws RemoteException;
  
  class Default implements IPlaybackConfigDispatcher {
    public void dispatchPlaybackConfigChange(List<AudioPlaybackConfiguration> param1List, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPlaybackConfigDispatcher {
    private static final String DESCRIPTOR = "android.media.IPlaybackConfigDispatcher";
    
    static final int TRANSACTION_dispatchPlaybackConfigChange = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IPlaybackConfigDispatcher");
    }
    
    public static IPlaybackConfigDispatcher asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IPlaybackConfigDispatcher");
      if (iInterface != null && iInterface instanceof IPlaybackConfigDispatcher)
        return (IPlaybackConfigDispatcher)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "dispatchPlaybackConfigChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IPlaybackConfigDispatcher");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IPlaybackConfigDispatcher");
      ArrayList<AudioPlaybackConfiguration> arrayList = param1Parcel1.createTypedArrayList(AudioPlaybackConfiguration.CREATOR);
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      dispatchPlaybackConfigChange(arrayList, bool);
      return true;
    }
    
    private static class Proxy implements IPlaybackConfigDispatcher {
      public static IPlaybackConfigDispatcher sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IPlaybackConfigDispatcher";
      }
      
      public void dispatchPlaybackConfigChange(List<AudioPlaybackConfiguration> param2List, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IPlaybackConfigDispatcher");
          parcel.writeTypedList(param2List);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IPlaybackConfigDispatcher.Stub.getDefaultImpl() != null) {
            IPlaybackConfigDispatcher.Stub.getDefaultImpl().dispatchPlaybackConfigChange(param2List, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPlaybackConfigDispatcher param1IPlaybackConfigDispatcher) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPlaybackConfigDispatcher != null) {
          Proxy.sDefaultImpl = param1IPlaybackConfigDispatcher;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPlaybackConfigDispatcher getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
