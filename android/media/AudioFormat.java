package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;

public final class AudioFormat implements Parcelable {
  public static final int AUDIO_FORMAT_HAS_PROPERTY_CHANNEL_INDEX_MASK = 8;
  
  public static final int AUDIO_FORMAT_HAS_PROPERTY_CHANNEL_MASK = 4;
  
  public static final int AUDIO_FORMAT_HAS_PROPERTY_ENCODING = 1;
  
  public static final int AUDIO_FORMAT_HAS_PROPERTY_NONE = 0;
  
  public static final int AUDIO_FORMAT_HAS_PROPERTY_SAMPLE_RATE = 2;
  
  @Deprecated
  public static final int CHANNEL_CONFIGURATION_DEFAULT = 1;
  
  @Deprecated
  public static final int CHANNEL_CONFIGURATION_INVALID = 0;
  
  @Deprecated
  public static final int CHANNEL_CONFIGURATION_MONO = 2;
  
  @Deprecated
  public static final int CHANNEL_CONFIGURATION_STEREO = 3;
  
  public static final int CHANNEL_INVALID = 0;
  
  public static final int CHANNEL_IN_2POINT0POINT2 = 6291468;
  
  public static final int CHANNEL_IN_5POINT1 = 252;
  
  public static final int CHANNEL_IN_BACK = 32;
  
  public static final int CHANNEL_IN_BACK_PROCESSED = 512;
  
  public static final int CHANNEL_IN_DEFAULT = 1;
  
  public static final int CHANNEL_IN_FRONT = 16;
  
  public static final int CHANNEL_IN_FRONT_BACK = 48;
  
  public static final int CHANNEL_IN_FRONT_PROCESSED = 256;
  
  public static final int CHANNEL_IN_LEFT = 4;
  
  public static final int CHANNEL_IN_LEFT_PROCESSED = 64;
  
  public static final int CHANNEL_IN_MONO = 16;
  
  public static final int CHANNEL_IN_PRESSURE = 1024;
  
  public static final int CHANNEL_IN_RIGHT = 8;
  
  public static final int CHANNEL_IN_RIGHT_PROCESSED = 128;
  
  public static final int CHANNEL_IN_STEREO = 12;
  
  public static final int CHANNEL_IN_TOP_LEFT = 2097152;
  
  public static final int CHANNEL_IN_TOP_RIGHT = 4194304;
  
  public static final int CHANNEL_IN_VOICE_DNLINK = 32768;
  
  public static final int CHANNEL_IN_VOICE_UPLINK = 16384;
  
  public static final int CHANNEL_IN_X_AXIS = 2048;
  
  public static final int CHANNEL_IN_Y_AXIS = 4096;
  
  public static final int CHANNEL_IN_Z_AXIS = 8192;
  
  public static final int CHANNEL_OUT_5POINT1 = 252;
  
  public static final int CHANNEL_OUT_5POINT1_SIDE = 6204;
  
  @Deprecated
  public static final int CHANNEL_OUT_7POINT1 = 1020;
  
  public static final int CHANNEL_OUT_7POINT1_SURROUND = 6396;
  
  public static final int CHANNEL_OUT_BACK_CENTER = 1024;
  
  public static final int CHANNEL_OUT_BACK_LEFT = 64;
  
  public static final int CHANNEL_OUT_BACK_RIGHT = 128;
  
  public static final int CHANNEL_OUT_DEFAULT = 1;
  
  public static final int CHANNEL_OUT_FRONT_CENTER = 16;
  
  public static final int CHANNEL_OUT_FRONT_LEFT = 4;
  
  public static final int CHANNEL_OUT_FRONT_LEFT_OF_CENTER = 256;
  
  public static final int CHANNEL_OUT_FRONT_RIGHT = 8;
  
  public static final int CHANNEL_OUT_FRONT_RIGHT_OF_CENTER = 512;
  
  public static final int CHANNEL_OUT_LOW_FREQUENCY = 32;
  
  public static final int CHANNEL_OUT_MONO = 4;
  
  public static final int CHANNEL_OUT_QUAD = 204;
  
  public static final int CHANNEL_OUT_QUAD_SIDE = 6156;
  
  public static final int CHANNEL_OUT_SIDE_LEFT = 2048;
  
  public static final int CHANNEL_OUT_SIDE_RIGHT = 4096;
  
  public static final int CHANNEL_OUT_STEREO = 12;
  
  public static final int CHANNEL_OUT_SURROUND = 1052;
  
  public static final int CHANNEL_OUT_TOP_BACK_CENTER = 262144;
  
  public static final int CHANNEL_OUT_TOP_BACK_LEFT = 131072;
  
  public static final int CHANNEL_OUT_TOP_BACK_RIGHT = 524288;
  
  public static final int CHANNEL_OUT_TOP_CENTER = 8192;
  
  public static final int CHANNEL_OUT_TOP_FRONT_CENTER = 32768;
  
  public static final int CHANNEL_OUT_TOP_FRONT_LEFT = 16384;
  
  public static final int CHANNEL_OUT_TOP_FRONT_RIGHT = 65536;
  
  public static String toLogFriendlyEncoding(int paramInt) {
    if (paramInt != 0) {
      StringBuilder stringBuilder;
      switch (paramInt) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("invalid encoding ");
          stringBuilder.append(paramInt);
          return stringBuilder.toString();
        case 20:
          return "ENCODING_OPUS";
        case 19:
          return "ENCODING_DOLBY_MAT";
        case 18:
          return "ENCODING_E_AC3_JOC";
        case 17:
          return "ENCODING_AC4";
        case 16:
          return "ENCODING_AAC_XHE";
        case 15:
          return "ENCODING_AAC_ELD";
        case 14:
          return "ENCODING_DOLBY_TRUEHD";
        case 13:
          return "ENCODING_IEC61937";
        case 12:
          return "ENCODING_AAC_HE_V2";
        case 11:
          return "ENCODING_AAC_HE_V1";
        case 10:
          return "ENCODING_AAC_LC";
        case 9:
          return "ENCODING_MP3";
        case 8:
          return "ENCODING_DTS_HD";
        case 7:
          return "ENCODING_DTS";
        case 6:
          return "ENCODING_E_AC3";
        case 5:
          return "ENCODING_AC3";
        case 4:
          return "ENCODING_PCM_FLOAT";
        case 3:
          return "ENCODING_PCM_8BIT";
        case 2:
          break;
      } 
      return "ENCODING_PCM_16BIT";
    } 
    return "ENCODING_INVALID";
  }
  
  public static int inChannelMaskFromOutChannelMask(int paramInt) throws IllegalArgumentException {
    if (paramInt != 1) {
      paramInt = channelCountFromOutChannelMask(paramInt);
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            return 28; 
          throw new IllegalArgumentException("Unsupported channel configuration for input.");
        } 
        return 12;
      } 
      return 16;
    } 
    throw new IllegalArgumentException("Illegal CHANNEL_OUT_DEFAULT channel mask for input.");
  }
  
  public static int channelCountFromInChannelMask(int paramInt) {
    return Integer.bitCount(paramInt);
  }
  
  public static int channelCountFromOutChannelMask(int paramInt) {
    return Integer.bitCount(paramInt);
  }
  
  public static int convertChannelOutMaskToNativeMask(int paramInt) {
    return paramInt >> 2;
  }
  
  public static int convertNativeChannelMaskToOutMask(int paramInt) {
    return paramInt << 2;
  }
  
  public static int getBytesPerSample(int paramInt) {
    if (paramInt != 1 && paramInt != 2)
      if (paramInt != 3) {
        if (paramInt != 4) {
          if (paramInt != 13) {
            StringBuilder stringBuilder;
            switch (paramInt) {
              default:
                stringBuilder = new StringBuilder();
                stringBuilder.append("Bad audio format ");
                stringBuilder.append(paramInt);
                throw new IllegalArgumentException(stringBuilder.toString());
              case 102:
              case 103:
              case 104:
              case 105:
                return 23;
              case 101:
                return 61;
              case 100:
                break;
            } 
            return 32;
          } 
        } else {
          return 4;
        } 
      } else {
        return 1;
      }  
    return 2;
  }
  
  public static boolean isValidEncoding(int paramInt) {
    switch (paramInt) {
      default:
        switch (paramInt) {
          default:
            return false;
          case 100:
          case 101:
          case 102:
          case 103:
          case 104:
          case 105:
            break;
        } 
        break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
        break;
    } 
    return true;
  }
  
  public static boolean isPublicEncoding(int paramInt) {
    switch (paramInt) {
      default:
        return false;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
        break;
    } 
    return true;
  }
  
  public static boolean isEncodingLinearPcm(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        switch (paramInt) {
          default:
            stringBuilder = new StringBuilder();
            stringBuilder.append("Bad audio format ");
            stringBuilder.append(paramInt);
            throw new IllegalArgumentException(stringBuilder.toString());
          case 100:
          case 101:
          case 102:
          case 103:
          case 104:
          case 105:
            break;
        } 
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
        return false;
      case 1:
      case 2:
      case 3:
      case 4:
        break;
    } 
    return true;
  }
  
  public static boolean isEncodingLinearFrames(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Bad audio format ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
        return false;
      case 1:
      case 2:
      case 3:
      case 4:
      case 13:
        break;
    } 
    return true;
  }
  
  public static int[] filterPublicFormats(int[] paramArrayOfint) {
    if (paramArrayOfint == null)
      return null; 
    paramArrayOfint = Arrays.copyOf(paramArrayOfint, paramArrayOfint.length);
    int i = 0;
    for (byte b = 0; b < paramArrayOfint.length; b++, i = j) {
      int j = i;
      if (isPublicEncoding(paramArrayOfint[b])) {
        if (i != b)
          paramArrayOfint[i] = paramArrayOfint[b]; 
        j = i + 1;
      } 
    } 
    return Arrays.copyOf(paramArrayOfint, i);
  }
  
  public AudioFormat() {
    throw new UnsupportedOperationException("There is no valid usage of this constructor");
  }
  
  private AudioFormat(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this(15, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  private AudioFormat(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mPropertySetMask = paramInt1;
    boolean bool = false;
    if ((paramInt1 & 0x1) == 0)
      paramInt2 = 0; 
    this.mEncoding = paramInt2;
    if ((paramInt1 & 0x2) == 0)
      paramInt3 = 0; 
    this.mSampleRate = paramInt3;
    if ((paramInt1 & 0x4) == 0)
      paramInt4 = 0; 
    this.mChannelMask = paramInt4;
    if ((paramInt1 & 0x8) == 0)
      paramInt5 = bool; 
    this.mChannelIndexMask = paramInt5;
    paramInt2 = Integer.bitCount(getChannelIndexMask());
    paramInt3 = channelCountFromOutChannelMask(getChannelMask());
    if (paramInt3 == 0) {
      paramInt1 = paramInt2;
    } else {
      paramInt1 = paramInt3;
      if (paramInt3 != paramInt2) {
        paramInt1 = paramInt3;
        if (paramInt2 != 0)
          paramInt1 = 0; 
      } 
    } 
    this.mChannelCount = paramInt1;
    paramInt2 = 1;
    try {
      paramInt3 = getBytesPerSample(this.mEncoding);
      paramInt1 = paramInt3 * paramInt1;
    } catch (IllegalArgumentException illegalArgumentException) {
      paramInt1 = paramInt2;
    } 
    if (paramInt1 == 0)
      paramInt1 = 1; 
    this.mFrameSizeInBytes = paramInt1;
  }
  
  public int getEncoding() {
    return this.mEncoding;
  }
  
  public int getSampleRate() {
    return this.mSampleRate;
  }
  
  public int getChannelMask() {
    return this.mChannelMask;
  }
  
  public int getChannelIndexMask() {
    return this.mChannelIndexMask;
  }
  
  public int getChannelCount() {
    return this.mChannelCount;
  }
  
  public int getFrameSizeInBytes() {
    return this.mFrameSizeInBytes;
  }
  
  public int getPropertySetMask() {
    return this.mPropertySetMask;
  }
  
  public String toLogFriendlyString() {
    int i = this.mChannelCount;
    int j = this.mSampleRate;
    String str = toLogFriendlyEncoding(this.mEncoding);
    return String.format("%dch %dHz %s", new Object[] { Integer.valueOf(i), Integer.valueOf(j), str });
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SurroundSoundEncoding implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Encoding implements Annotation {}
  
  class Builder {
    private int mEncoding = 0;
    
    private int mSampleRate = 0;
    
    private int mChannelMask = 0;
    
    private int mChannelIndexMask = 0;
    
    private int mPropertySetMask = 0;
    
    public Builder(AudioFormat this$0) {
      this.mEncoding = this$0.mEncoding;
      this.mSampleRate = this$0.mSampleRate;
      this.mChannelMask = this$0.mChannelMask;
      this.mChannelIndexMask = this$0.mChannelIndexMask;
      this.mPropertySetMask = this$0.mPropertySetMask;
    }
    
    public AudioFormat build() {
      return new AudioFormat(this.mPropertySetMask, this.mEncoding, this.mSampleRate, this.mChannelMask, this.mChannelIndexMask);
    }
    
    public Builder setEncoding(int param1Int) throws IllegalArgumentException {
      StringBuilder stringBuilder;
      switch (param1Int) {
        default:
          switch (param1Int) {
            default:
              stringBuilder = new StringBuilder();
              stringBuilder.append("Invalid encoding ");
              stringBuilder.append(param1Int);
              throw new IllegalArgumentException(stringBuilder.toString());
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
              break;
          } 
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
          this.mEncoding = param1Int;
          this.mPropertySetMask |= 0x1;
          return this;
        case 1:
          break;
      } 
      this.mEncoding = 2;
      this.mPropertySetMask |= 0x1;
      return this;
    }
    
    public Builder setChannelMask(int param1Int) {
      if (param1Int != 0) {
        if (this.mChannelIndexMask == 0 || 
          Integer.bitCount(param1Int) == Integer.bitCount(this.mChannelIndexMask)) {
          this.mChannelMask = param1Int;
          this.mPropertySetMask |= 0x4;
          return this;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Mismatched channel count for mask ");
        stringBuilder.append(Integer.toHexString(param1Int).toUpperCase());
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new IllegalArgumentException("Invalid zero channel mask");
    }
    
    public Builder setChannelIndexMask(int param1Int) {
      if (param1Int != 0) {
        if (this.mChannelMask == 0 || 
          Integer.bitCount(param1Int) == Integer.bitCount(this.mChannelMask)) {
          this.mChannelIndexMask = param1Int;
          this.mPropertySetMask |= 0x8;
          return this;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Mismatched channel count for index mask ");
        stringBuilder.append(Integer.toHexString(param1Int).toUpperCase());
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new IllegalArgumentException("Invalid zero channel index mask");
    }
    
    public Builder setSampleRate(int param1Int) throws IllegalArgumentException {
      if ((param1Int >= 4000 && param1Int <= 192000) || param1Int == 0) {
        this.mSampleRate = param1Int;
        this.mPropertySetMask |= 0x2;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid sample rate ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder() {}
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    int i = this.mPropertySetMask;
    if (i != ((AudioFormat)paramObject).mPropertySetMask)
      return false; 
    if (((i & 0x1) == 0 || this.mEncoding == ((AudioFormat)paramObject).mEncoding) && ((this.mPropertySetMask & 0x2) == 0 || this.mSampleRate == ((AudioFormat)paramObject).mSampleRate) && ((this.mPropertySetMask & 0x4) == 0 || this.mChannelMask == ((AudioFormat)paramObject).mChannelMask)) {
      boolean bool1 = bool;
      if ((this.mPropertySetMask & 0x8) != 0) {
        if (this.mChannelIndexMask == ((AudioFormat)paramObject).mChannelIndexMask)
          return bool; 
      } else {
        return bool1;
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mPropertySetMask, j = this.mSampleRate, k = this.mEncoding, m = this.mChannelMask, n = this.mChannelIndexMask;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPropertySetMask);
    paramParcel.writeInt(this.mEncoding);
    paramParcel.writeInt(this.mSampleRate);
    paramParcel.writeInt(this.mChannelMask);
    paramParcel.writeInt(this.mChannelIndexMask);
  }
  
  private AudioFormat(Parcel paramParcel) {
    this(i, j, k, m, n);
  }
  
  public static final Parcelable.Creator<AudioFormat> CREATOR = new Parcelable.Creator<AudioFormat>() {
      public AudioFormat createFromParcel(Parcel param1Parcel) {
        return new AudioFormat(param1Parcel);
      }
      
      public AudioFormat[] newArray(int param1Int) {
        return new AudioFormat[param1Int];
      }
    };
  
  public static final int ENCODING_AAC_ELD = 15;
  
  public static final int ENCODING_AAC_HE_V1 = 11;
  
  public static final int ENCODING_AAC_HE_V2 = 12;
  
  public static final int ENCODING_AAC_LC = 10;
  
  public static final int ENCODING_AAC_XHE = 16;
  
  public static final int ENCODING_AC3 = 5;
  
  public static final int ENCODING_AC4 = 17;
  
  public static final int ENCODING_AMRNB = 100;
  
  public static final int ENCODING_AMRWB = 101;
  
  public static final int ENCODING_DEFAULT = 1;
  
  public static final int ENCODING_DOLBY_MAT = 19;
  
  public static final int ENCODING_DOLBY_TRUEHD = 14;
  
  public static final int ENCODING_DTS = 7;
  
  public static final int ENCODING_DTS_HD = 8;
  
  public static final int ENCODING_EVRC = 102;
  
  public static final int ENCODING_EVRCB = 103;
  
  public static final int ENCODING_EVRCNW = 105;
  
  public static final int ENCODING_EVRCWB = 104;
  
  public static final int ENCODING_E_AC3 = 6;
  
  public static final int ENCODING_E_AC3_JOC = 18;
  
  public static final int ENCODING_IEC61937 = 13;
  
  public static final int ENCODING_INVALID = 0;
  
  public static final int ENCODING_MP3 = 9;
  
  public static final int ENCODING_OPUS = 20;
  
  public static final int ENCODING_PCM_16BIT = 2;
  
  public static final int ENCODING_PCM_8BIT = 3;
  
  public static final int ENCODING_PCM_FLOAT = 4;
  
  public static final int SAMPLE_RATE_HZ_MAX = 192000;
  
  public static final int SAMPLE_RATE_HZ_MIN = 4000;
  
  public static final int SAMPLE_RATE_UNSPECIFIED = 0;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AudioFormat: props=");
    stringBuilder.append(this.mPropertySetMask);
    stringBuilder.append(" enc=");
    stringBuilder.append(this.mEncoding);
    stringBuilder.append(" chan=0x");
    int i = this.mChannelMask;
    stringBuilder.append(Integer.toHexString(i).toUpperCase());
    stringBuilder.append(" chan_index=0x");
    i = this.mChannelIndexMask;
    stringBuilder.append(Integer.toHexString(i).toUpperCase());
    stringBuilder.append(" rate=");
    stringBuilder.append(this.mSampleRate);
    return new String(stringBuilder.toString());
  }
  
  public static final int[] SURROUND_SOUND_ENCODING = new int[] { 5, 6, 7, 8, 10, 14, 17, 18, 19 };
  
  private final int mChannelCount;
  
  private final int mChannelIndexMask;
  
  private final int mChannelMask;
  
  private final int mEncoding;
  
  private final int mFrameSizeInBytes;
  
  private final int mPropertySetMask;
  
  private final int mSampleRate;
  
  public static String toDisplayName(int paramInt) {
    if (paramInt != 5) {
      if (paramInt != 6) {
        if (paramInt != 7) {
          if (paramInt != 8) {
            if (paramInt != 10) {
              if (paramInt != 14) {
                switch (paramInt) {
                  default:
                    return "Unknown surround sound format";
                  case 19:
                    return "Dolby MAT";
                  case 18:
                    return "Dolby Atmos in Dolby Digital Plus";
                  case 17:
                    break;
                } 
                return "Dolby AC-4";
              } 
              return "Dolby TrueHD";
            } 
            return "AAC";
          } 
          return "DTS HD";
        } 
        return "DTS";
      } 
      return "Dolby Digital Plus";
    } 
    return "Dolby Digital";
  }
}
