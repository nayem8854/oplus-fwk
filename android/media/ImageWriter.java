package android.media;

import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.HardwareBuffer;
import android.hardware.camera2.utils.SurfaceUtils;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Size;
import android.view.Surface;
import dalvik.system.VMRuntime;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.NioUtils;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ImageWriter implements AutoCloseable {
  private List<Image> mDequeuedImages;
  
  private int mEstimatedNativeAllocBytes;
  
  private OnImageReleasedListener mListener;
  
  private ListenerHandler mListenerHandler;
  
  private final Object mListenerLock;
  
  private final int mMaxImages;
  
  private long mNativeContext;
  
  private int mWriterFormat;
  
  public static ImageWriter newInstance(Surface paramSurface, int paramInt) {
    return new ImageWriter(paramSurface, paramInt, 0);
  }
  
  public static ImageWriter newInstance(Surface paramSurface, int paramInt1, int paramInt2) {
    if (ImageFormat.isPublicFormat(paramInt2) || PixelFormat.isPublicFormat(paramInt2))
      return new ImageWriter(paramSurface, paramInt1, paramInt2); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid format is specified: ");
    stringBuilder.append(paramInt2);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  protected ImageWriter(Surface paramSurface, int paramInt1, int paramInt2) {
    Size size;
    this.mListenerLock = new Object();
    this.mDequeuedImages = new CopyOnWriteArrayList<>();
    if (paramSurface != null && paramInt1 >= 1) {
      this.mMaxImages = paramInt1;
      this.mNativeContext = nativeInit(new WeakReference<>(this), paramSurface, paramInt1, paramInt2);
      paramInt1 = paramInt2;
      if (paramInt2 == 0)
        paramInt1 = SurfaceUtils.getSurfaceFormat(paramSurface); 
      size = SurfaceUtils.getSurfaceSize(paramSurface);
      this.mEstimatedNativeAllocBytes = ImageUtils.getEstimatedNativeAllocBytes(size.getWidth(), size.getHeight(), paramInt1, 1);
      VMRuntime.getRuntime().registerNativeAllocation(this.mEstimatedNativeAllocBytes);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal input argument: surface ");
    stringBuilder.append(size);
    stringBuilder.append(", maxImages: ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getMaxImages() {
    return this.mMaxImages;
  }
  
  public Image dequeueInputImage() {
    if (this.mDequeuedImages.size() < this.mMaxImages) {
      WriterSurfaceImage writerSurfaceImage = new WriterSurfaceImage(this);
      nativeDequeueInputImage(this.mNativeContext, writerSurfaceImage);
      this.mDequeuedImages.add(writerSurfaceImage);
      writerSurfaceImage.mIsImageValid = true;
      return writerSurfaceImage;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Already dequeued max number of Images ");
    stringBuilder.append(this.mMaxImages);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void queueInputImage(Image paramImage) {
    if (paramImage != null) {
      boolean bool = isImageOwnedByMe(paramImage);
      if (!bool || ((WriterSurfaceImage)paramImage).mIsImageValid) {
        if (!bool) {
          if (paramImage.getOwner() instanceof ImageReader) {
            ImageReader imageReader = (ImageReader)paramImage.getOwner();
            imageReader.detachImage(paramImage);
            attachAndQueueInputImage(paramImage);
            paramImage.close();
            return;
          } 
          throw new IllegalArgumentException("Only images from ImageReader can be queued to ImageWriter, other image source is not supported yet!");
        } 
        Rect rect = paramImage.getCropRect();
        long l1 = this.mNativeContext, l2 = paramImage.getTimestamp();
        int i = rect.left, j = rect.top, k = rect.right, m = rect.bottom;
        int n = paramImage.getTransform(), i1 = paramImage.getScalingMode();
        nativeQueueInputImage(l1, paramImage, l2, i, j, k, m, n, i1);
        if (bool) {
          this.mDequeuedImages.remove(paramImage);
          paramImage = paramImage;
          paramImage.clearSurfacePlanes();
          ((WriterSurfaceImage)paramImage).mIsImageValid = false;
        } 
        return;
      } 
      throw new IllegalStateException("Image from ImageWriter is invalid");
    } 
    throw new IllegalArgumentException("image shouldn't be null");
  }
  
  public int getFormat() {
    return this.mWriterFormat;
  }
  
  public void setOnImageReleasedListener(OnImageReleasedListener paramOnImageReleasedListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mListenerLock : Ljava/lang/Object;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 88
    //   11: aload_2
    //   12: ifnull -> 23
    //   15: aload_2
    //   16: invokevirtual getLooper : ()Landroid/os/Looper;
    //   19: astore_2
    //   20: goto -> 27
    //   23: invokestatic myLooper : ()Landroid/os/Looper;
    //   26: astore_2
    //   27: aload_2
    //   28: ifnull -> 75
    //   31: aload_0
    //   32: getfield mListenerHandler : Landroid/media/ImageWriter$ListenerHandler;
    //   35: ifnull -> 49
    //   38: aload_0
    //   39: getfield mListenerHandler : Landroid/media/ImageWriter$ListenerHandler;
    //   42: invokevirtual getLooper : ()Landroid/os/Looper;
    //   45: aload_2
    //   46: if_acmpeq -> 67
    //   49: new android/media/ImageWriter$ListenerHandler
    //   52: astore #4
    //   54: aload #4
    //   56: aload_0
    //   57: aload_2
    //   58: invokespecial <init> : (Landroid/media/ImageWriter;Landroid/os/Looper;)V
    //   61: aload_0
    //   62: aload #4
    //   64: putfield mListenerHandler : Landroid/media/ImageWriter$ListenerHandler;
    //   67: aload_0
    //   68: aload_1
    //   69: putfield mListener : Landroid/media/ImageWriter$OnImageReleasedListener;
    //   72: goto -> 98
    //   75: new java/lang/IllegalArgumentException
    //   78: astore_1
    //   79: aload_1
    //   80: ldc_w 'handler is null but the current thread is not a looper'
    //   83: invokespecial <init> : (Ljava/lang/String;)V
    //   86: aload_1
    //   87: athrow
    //   88: aload_0
    //   89: aconst_null
    //   90: putfield mListener : Landroid/media/ImageWriter$OnImageReleasedListener;
    //   93: aload_0
    //   94: aconst_null
    //   95: putfield mListenerHandler : Landroid/media/ImageWriter$ListenerHandler;
    //   98: aload_3
    //   99: monitorexit
    //   100: return
    //   101: astore_1
    //   102: aload_3
    //   103: monitorexit
    //   104: aload_1
    //   105: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #463	-> 0
    //   #464	-> 7
    //   #465	-> 11
    //   #466	-> 27
    //   #470	-> 31
    //   #471	-> 49
    //   #473	-> 67
    //   #474	-> 72
    //   #467	-> 75
    //   #475	-> 88
    //   #476	-> 93
    //   #478	-> 98
    //   #479	-> 100
    //   #478	-> 101
    // Exception table:
    //   from	to	target	type
    //   15	20	101	finally
    //   23	27	101	finally
    //   31	49	101	finally
    //   49	67	101	finally
    //   67	72	101	finally
    //   75	88	101	finally
    //   88	93	101	finally
    //   93	98	101	finally
    //   98	100	101	finally
    //   102	104	101	finally
  }
  
  public void close() {
    setOnImageReleasedListener(null, null);
    for (Image image : this.mDequeuedImages)
      image.close(); 
    this.mDequeuedImages.clear();
    nativeClose(this.mNativeContext);
    this.mNativeContext = 0L;
    if (this.mEstimatedNativeAllocBytes > 0) {
      VMRuntime.getRuntime().registerNativeFree(this.mEstimatedNativeAllocBytes);
      this.mEstimatedNativeAllocBytes = 0;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private void attachAndQueueInputImage(Image paramImage) {
    if (paramImage != null) {
      if (!isImageOwnedByMe(paramImage)) {
        if (paramImage.isAttachable()) {
          Rect rect = paramImage.getCropRect();
          long l1 = this.mNativeContext, l2 = paramImage.getNativeContext();
          int i = paramImage.getFormat();
          long l3 = paramImage.getTimestamp();
          int j = rect.left, k = rect.top, m = rect.right, n = rect.bottom;
          int i1 = paramImage.getTransform(), i2 = paramImage.getScalingMode();
          nativeAttachAndQueueImage(l1, l2, i, l3, j, k, m, n, i1, i2);
          return;
        } 
        throw new IllegalStateException("Image was not detached from last owner, or image  is not detachable");
      } 
      throw new IllegalArgumentException("Can not attach an image that is owned ImageWriter already");
    } 
    throw new IllegalArgumentException("image shouldn't be null");
  }
  
  class ListenerHandler extends Handler {
    final ImageWriter this$0;
    
    public ListenerHandler(Looper param1Looper) {
      super(param1Looper, null, true);
    }
    
    public void handleMessage(Message param1Message) {
      synchronized (ImageWriter.this.mListenerLock) {
        ImageWriter.OnImageReleasedListener onImageReleasedListener = ImageWriter.this.mListener;
        if (onImageReleasedListener != null)
          onImageReleasedListener.onImageReleased(ImageWriter.this); 
        return;
      } 
    }
  }
  
  private static void postEventFromNative(Object paramObject) {
    paramObject = paramObject;
    null = paramObject.get();
    if (null == null)
      return; 
    synchronized (null.mListenerLock) {
      ListenerHandler listenerHandler = null.mListenerHandler;
      if (listenerHandler != null)
        listenerHandler.sendEmptyMessage(0); 
      return;
    } 
  }
  
  private void abortImage(Image paramImage) {
    if (paramImage != null) {
      if (this.mDequeuedImages.contains(paramImage)) {
        WriterSurfaceImage writerSurfaceImage = (WriterSurfaceImage)paramImage;
        if (!writerSurfaceImage.mIsImageValid)
          return; 
        cancelImage(this.mNativeContext, paramImage);
        this.mDequeuedImages.remove(paramImage);
        writerSurfaceImage.clearSurfacePlanes();
        writerSurfaceImage.mIsImageValid = false;
        return;
      } 
      throw new IllegalStateException("It is illegal to abort some image that is not dequeued yet");
    } 
    throw new IllegalArgumentException("image shouldn't be null");
  }
  
  private boolean isImageOwnedByMe(Image paramImage) {
    if (!(paramImage instanceof WriterSurfaceImage))
      return false; 
    paramImage = paramImage;
    if (paramImage.getOwner() != this)
      return false; 
    return true;
  }
  
  public static interface OnImageReleasedListener {
    void onImageReleased(ImageWriter param1ImageWriter);
  }
  
  class WriterSurfaceImage extends Image {
    private int mNativeFenceFd = -1;
    
    private int mHeight = -1;
    
    private int mWidth = -1;
    
    private int mFormat = -1;
    
    private final long DEFAULT_TIMESTAMP = Long.MIN_VALUE;
    
    private long mTimestamp = Long.MIN_VALUE;
    
    private int mTransform = 0;
    
    private int mScalingMode = 0;
    
    private long mNativeBuffer;
    
    private ImageWriter mOwner;
    
    private SurfacePlane[] mPlanes;
    
    public WriterSurfaceImage(ImageWriter this$0) {
      this.mOwner = this$0;
    }
    
    public int getFormat() {
      throwISEIfImageIsInvalid();
      if (this.mFormat == -1)
        this.mFormat = nativeGetFormat(); 
      return this.mFormat;
    }
    
    public int getWidth() {
      throwISEIfImageIsInvalid();
      if (this.mWidth == -1)
        this.mWidth = nativeGetWidth(); 
      return this.mWidth;
    }
    
    public int getHeight() {
      throwISEIfImageIsInvalid();
      if (this.mHeight == -1)
        this.mHeight = nativeGetHeight(); 
      return this.mHeight;
    }
    
    public int getTransform() {
      throwISEIfImageIsInvalid();
      return this.mTransform;
    }
    
    public int getScalingMode() {
      throwISEIfImageIsInvalid();
      return this.mScalingMode;
    }
    
    public long getTimestamp() {
      throwISEIfImageIsInvalid();
      return this.mTimestamp;
    }
    
    public void setTimestamp(long param1Long) {
      throwISEIfImageIsInvalid();
      this.mTimestamp = param1Long;
    }
    
    public HardwareBuffer getHardwareBuffer() {
      throwISEIfImageIsInvalid();
      return nativeGetHardwareBuffer();
    }
    
    public Image.Plane[] getPlanes() {
      throwISEIfImageIsInvalid();
      if (this.mPlanes == null) {
        int i = ImageUtils.getNumPlanesForFormat(getFormat());
        this.mPlanes = nativeCreatePlanes(i, getOwner().getFormat());
      } 
      return (Image.Plane[])this.mPlanes.clone();
    }
    
    boolean isAttachable() {
      throwISEIfImageIsInvalid();
      return false;
    }
    
    ImageWriter getOwner() {
      throwISEIfImageIsInvalid();
      return this.mOwner;
    }
    
    long getNativeContext() {
      throwISEIfImageIsInvalid();
      return this.mNativeBuffer;
    }
    
    public void close() {
      if (this.mIsImageValid)
        getOwner().abortImage(this); 
    }
    
    protected final void finalize() throws Throwable {
      try {
        close();
        return;
      } finally {
        super.finalize();
      } 
    }
    
    private void clearSurfacePlanes() {
      if (this.mIsImageValid && this.mPlanes != null) {
        byte b = 0;
        while (true) {
          SurfacePlane[] arrayOfSurfacePlane = this.mPlanes;
          if (b < arrayOfSurfacePlane.length) {
            if (arrayOfSurfacePlane[b] != null) {
              arrayOfSurfacePlane[b].clearBuffer();
              this.mPlanes[b] = null;
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
    }
    
    private synchronized native SurfacePlane[] nativeCreatePlanes(int param1Int1, int param1Int2);
    
    private synchronized native int nativeGetFormat();
    
    private synchronized native HardwareBuffer nativeGetHardwareBuffer();
    
    private synchronized native int nativeGetHeight();
    
    private synchronized native int nativeGetWidth();
    
    private class SurfacePlane extends Image.Plane {
      private ByteBuffer mBuffer;
      
      private final int mPixelStride;
      
      private final int mRowStride;
      
      final ImageWriter.WriterSurfaceImage this$0;
      
      private SurfacePlane(int param2Int1, int param2Int2, ByteBuffer param2ByteBuffer) {
        this.mRowStride = param2Int1;
        this.mPixelStride = param2Int2;
        this.mBuffer = param2ByteBuffer;
        param2ByteBuffer.order(ByteOrder.nativeOrder());
      }
      
      public int getRowStride() {
        ImageWriter.WriterSurfaceImage.this.throwISEIfImageIsInvalid();
        return this.mRowStride;
      }
      
      public int getPixelStride() {
        ImageWriter.WriterSurfaceImage.this.throwISEIfImageIsInvalid();
        return this.mPixelStride;
      }
      
      public ByteBuffer getBuffer() {
        ImageWriter.WriterSurfaceImage.this.throwISEIfImageIsInvalid();
        return this.mBuffer;
      }
      
      private void clearBuffer() {
        ByteBuffer byteBuffer = this.mBuffer;
        if (byteBuffer == null)
          return; 
        if (byteBuffer.isDirect())
          NioUtils.freeDirectBuffer(this.mBuffer); 
        this.mBuffer = null;
      }
    }
  }
  
  static {
    System.loadLibrary("media_jni");
    nativeClassInit();
  }
  
  private synchronized native void cancelImage(long paramLong, Image paramImage);
  
  private synchronized native int nativeAttachAndQueueImage(long paramLong1, long paramLong2, int paramInt1, long paramLong3, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7);
  
  private static native void nativeClassInit();
  
  private synchronized native void nativeClose(long paramLong);
  
  private synchronized native void nativeDequeueInputImage(long paramLong, Image paramImage);
  
  private synchronized native long nativeInit(Object paramObject, Surface paramSurface, int paramInt1, int paramInt2);
  
  private synchronized native void nativeQueueInputImage(long paramLong1, Image paramImage, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);
}
