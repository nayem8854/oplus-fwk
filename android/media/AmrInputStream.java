package android.media;

import android.util.Log;
import android.view.Surface;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class AmrInputStream extends InputStream {
  private static final int SAMPLES_PER_FRAME = 160;
  
  private static final String TAG = "AmrInputStream";
  
  private final byte[] mBuf = new byte[320];
  
  private int mBufIn = 0;
  
  private int mBufOut = 0;
  
  MediaCodec mCodec;
  
  MediaCodec.BufferInfo mInfo;
  
  private InputStream mInputStream;
  
  private byte[] mOneByte = new byte[1];
  
  boolean mSawInputEOS;
  
  boolean mSawOutputEOS;
  
  public AmrInputStream(InputStream paramInputStream) {
    Log.w("AmrInputStream", "@@@@ AmrInputStream is not a public API @@@@");
    this.mInputStream = paramInputStream;
    MediaFormat mediaFormat = new MediaFormat();
    mediaFormat.setString("mime", "audio/3gpp");
    mediaFormat.setInteger("sample-rate", 8000);
    mediaFormat.setInteger("channel-count", 1);
    mediaFormat.setInteger("bitrate", 12200);
    MediaCodecList mediaCodecList = new MediaCodecList(0);
    String str = mediaCodecList.findEncoderForFormat(mediaFormat);
    if (str != null)
      try {
        MediaCodec mediaCodec = MediaCodec.createByCodecName(str);
        mediaCodec.configure(mediaFormat, (Surface)null, (MediaCrypto)null, 1);
        this.mCodec.start();
      } catch (IOException iOException) {
        MediaCodec mediaCodec = this.mCodec;
        if (mediaCodec != null)
          mediaCodec.release(); 
        this.mCodec = null;
      }  
    this.mInfo = new MediaCodec.BufferInfo();
  }
  
  public int read() throws IOException {
    int i = read(this.mOneByte, 0, 1);
    if (i == 1) {
      i = this.mOneByte[0] & 0xFF;
    } else {
      i = -1;
    } 
    return i;
  }
  
  public int read(byte[] paramArrayOfbyte) throws IOException {
    return read(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public int read(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    if (this.mCodec != null) {
      if (this.mBufOut >= this.mBufIn && !this.mSawOutputEOS) {
        this.mBufOut = 0;
        this.mBufIn = 0;
        while (!this.mSawInputEOS) {
          boolean bool;
          int m = this.mCodec.dequeueInputBuffer(0L);
          if (m < 0)
            break; 
          int n;
          for (n = 0; n < 320; ) {
            bool = this.mInputStream.read(this.mBuf, n, 320 - n);
            if (bool == -1) {
              this.mSawInputEOS = true;
              break;
            } 
            n += bool;
          } 
          ByteBuffer byteBuffer = this.mCodec.getInputBuffer(m);
          byteBuffer.put(this.mBuf, 0, n);
          MediaCodec mediaCodec = this.mCodec;
          if (this.mSawInputEOS) {
            bool = true;
          } else {
            bool = false;
          } 
          mediaCodec.queueInputBuffer(m, 0, n, 0L, bool);
        } 
        int k = this.mCodec.dequeueOutputBuffer(this.mInfo, 0L);
        if (k >= 0) {
          this.mBufIn = this.mInfo.size;
          ByteBuffer byteBuffer = this.mCodec.getOutputBuffer(k);
          byteBuffer.get(this.mBuf, 0, this.mBufIn);
          this.mCodec.releaseOutputBuffer(k, false);
          if ((0x4 & this.mInfo.flags) != 0)
            this.mSawOutputEOS = true; 
        } 
      } 
      int i = this.mBufOut, j = this.mBufIn;
      if (i < j) {
        if (paramInt2 > j - i)
          paramInt2 = j - i; 
        System.arraycopy(this.mBuf, this.mBufOut, paramArrayOfbyte, paramInt1, paramInt2);
        this.mBufOut += paramInt2;
        return paramInt2;
      } 
      if (this.mSawInputEOS && this.mSawOutputEOS)
        return -1; 
      return 0;
    } 
    throw new IllegalStateException("not open");
  }
  
  public void close() throws IOException {
    try {
      if (this.mInputStream != null)
        this.mInputStream.close(); 
    } finally {
      null = null;
      this.mInputStream = null;
    } 
  }
  
  protected void finalize() throws Throwable {
    if (this.mCodec != null) {
      Log.w("AmrInputStream", "AmrInputStream wasn't closed");
      this.mCodec.release();
    } 
  }
}
