package android.media;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaRoute2ProviderService extends IInterface {
  void deselectRoute(long paramLong, String paramString1, String paramString2) throws RemoteException;
  
  void releaseSession(long paramLong, String paramString) throws RemoteException;
  
  void requestCreateSession(long paramLong, String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void selectRoute(long paramLong, String paramString1, String paramString2) throws RemoteException;
  
  void setCallback(IMediaRoute2ProviderServiceCallback paramIMediaRoute2ProviderServiceCallback) throws RemoteException;
  
  void setRouteVolume(long paramLong, String paramString, int paramInt) throws RemoteException;
  
  void setSessionVolume(long paramLong, String paramString, int paramInt) throws RemoteException;
  
  void transferToRoute(long paramLong, String paramString1, String paramString2) throws RemoteException;
  
  void updateDiscoveryPreference(RouteDiscoveryPreference paramRouteDiscoveryPreference) throws RemoteException;
  
  class Default implements IMediaRoute2ProviderService {
    public void setCallback(IMediaRoute2ProviderServiceCallback param1IMediaRoute2ProviderServiceCallback) throws RemoteException {}
    
    public void updateDiscoveryPreference(RouteDiscoveryPreference param1RouteDiscoveryPreference) throws RemoteException {}
    
    public void setRouteVolume(long param1Long, String param1String, int param1Int) throws RemoteException {}
    
    public void requestCreateSession(long param1Long, String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void selectRoute(long param1Long, String param1String1, String param1String2) throws RemoteException {}
    
    public void deselectRoute(long param1Long, String param1String1, String param1String2) throws RemoteException {}
    
    public void transferToRoute(long param1Long, String param1String1, String param1String2) throws RemoteException {}
    
    public void setSessionVolume(long param1Long, String param1String, int param1Int) throws RemoteException {}
    
    public void releaseSession(long param1Long, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaRoute2ProviderService {
    private static final String DESCRIPTOR = "android.media.IMediaRoute2ProviderService";
    
    static final int TRANSACTION_deselectRoute = 6;
    
    static final int TRANSACTION_releaseSession = 9;
    
    static final int TRANSACTION_requestCreateSession = 4;
    
    static final int TRANSACTION_selectRoute = 5;
    
    static final int TRANSACTION_setCallback = 1;
    
    static final int TRANSACTION_setRouteVolume = 3;
    
    static final int TRANSACTION_setSessionVolume = 8;
    
    static final int TRANSACTION_transferToRoute = 7;
    
    static final int TRANSACTION_updateDiscoveryPreference = 2;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaRoute2ProviderService");
    }
    
    public static IMediaRoute2ProviderService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaRoute2ProviderService");
      if (iInterface != null && iInterface instanceof IMediaRoute2ProviderService)
        return (IMediaRoute2ProviderService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "releaseSession";
        case 8:
          return "setSessionVolume";
        case 7:
          return "transferToRoute";
        case 6:
          return "deselectRoute";
        case 5:
          return "selectRoute";
        case 4:
          return "requestCreateSession";
        case 3:
          return "setRouteVolume";
        case 2:
          return "updateDiscoveryPreference";
        case 1:
          break;
      } 
      return "setCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1;
        long l;
        String str2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.media.IMediaRoute2ProviderService");
            l = param1Parcel1.readLong();
            str1 = param1Parcel1.readString();
            releaseSession(l, str1);
            return true;
          case 8:
            str1.enforceInterface("android.media.IMediaRoute2ProviderService");
            l = str1.readLong();
            str = str1.readString();
            param1Int1 = str1.readInt();
            setSessionVolume(l, str, param1Int1);
            return true;
          case 7:
            str1.enforceInterface("android.media.IMediaRoute2ProviderService");
            l = str1.readLong();
            str = str1.readString();
            str1 = str1.readString();
            transferToRoute(l, str, str1);
            return true;
          case 6:
            str1.enforceInterface("android.media.IMediaRoute2ProviderService");
            l = str1.readLong();
            str = str1.readString();
            str1 = str1.readString();
            deselectRoute(l, str, str1);
            return true;
          case 5:
            str1.enforceInterface("android.media.IMediaRoute2ProviderService");
            l = str1.readLong();
            str = str1.readString();
            str1 = str1.readString();
            selectRoute(l, str, str1);
            return true;
          case 4:
            str1.enforceInterface("android.media.IMediaRoute2ProviderService");
            l = str1.readLong();
            str2 = str1.readString();
            str = str1.readString();
            if (str1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            requestCreateSession(l, str2, str, (Bundle)str1);
            return true;
          case 3:
            str1.enforceInterface("android.media.IMediaRoute2ProviderService");
            l = str1.readLong();
            str = str1.readString();
            param1Int1 = str1.readInt();
            setRouteVolume(l, str, param1Int1);
            return true;
          case 2:
            str1.enforceInterface("android.media.IMediaRoute2ProviderService");
            if (str1.readInt() != 0) {
              RouteDiscoveryPreference routeDiscoveryPreference = RouteDiscoveryPreference.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            updateDiscoveryPreference((RouteDiscoveryPreference)str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.media.IMediaRoute2ProviderService");
        IMediaRoute2ProviderServiceCallback iMediaRoute2ProviderServiceCallback = IMediaRoute2ProviderServiceCallback.Stub.asInterface(str1.readStrongBinder());
        setCallback(iMediaRoute2ProviderServiceCallback);
        return true;
      } 
      str.writeString("android.media.IMediaRoute2ProviderService");
      return true;
    }
    
    private static class Proxy implements IMediaRoute2ProviderService {
      public static IMediaRoute2ProviderService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaRoute2ProviderService";
      }
      
      public void setCallback(IMediaRoute2ProviderServiceCallback param2IMediaRoute2ProviderServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          if (param2IMediaRoute2ProviderServiceCallback != null) {
            iBinder = param2IMediaRoute2ProviderServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().setCallback(param2IMediaRoute2ProviderServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateDiscoveryPreference(RouteDiscoveryPreference param2RouteDiscoveryPreference) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          if (param2RouteDiscoveryPreference != null) {
            parcel.writeInt(1);
            param2RouteDiscoveryPreference.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().updateDiscoveryPreference(param2RouteDiscoveryPreference);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRouteVolume(long param2Long, String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          parcel.writeLong(param2Long);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().setRouteVolume(param2Long, param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCreateSession(long param2Long, String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          parcel.writeLong(param2Long);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().requestCreateSession(param2Long, param2String1, param2String2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void selectRoute(long param2Long, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          parcel.writeLong(param2Long);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().selectRoute(param2Long, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deselectRoute(long param2Long, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          parcel.writeLong(param2Long);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().deselectRoute(param2Long, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void transferToRoute(long param2Long, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          parcel.writeLong(param2Long);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().transferToRoute(param2Long, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSessionVolume(long param2Long, String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          parcel.writeLong(param2Long);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().setSessionVolume(param2Long, param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void releaseSession(long param2Long, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderService");
          parcel.writeLong(param2Long);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderService.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderService.Stub.getDefaultImpl().releaseSession(param2Long, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaRoute2ProviderService param1IMediaRoute2ProviderService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaRoute2ProviderService != null) {
          Proxy.sDefaultImpl = param1IMediaRoute2ProviderService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaRoute2ProviderService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
