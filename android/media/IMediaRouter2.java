package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IMediaRouter2 extends IInterface {
  void notifyRouterRegistered(List<MediaRoute2Info> paramList, RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void notifyRoutesAdded(List<MediaRoute2Info> paramList) throws RemoteException;
  
  void notifyRoutesChanged(List<MediaRoute2Info> paramList) throws RemoteException;
  
  void notifyRoutesRemoved(List<MediaRoute2Info> paramList) throws RemoteException;
  
  void notifySessionCreated(int paramInt, RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void notifySessionInfoChanged(RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void notifySessionReleased(RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void requestCreateSessionByManager(long paramLong, RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) throws RemoteException;
  
  class Default implements IMediaRouter2 {
    public void notifyRouterRegistered(List<MediaRoute2Info> param1List, RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifyRoutesAdded(List<MediaRoute2Info> param1List) throws RemoteException {}
    
    public void notifyRoutesRemoved(List<MediaRoute2Info> param1List) throws RemoteException {}
    
    public void notifyRoutesChanged(List<MediaRoute2Info> param1List) throws RemoteException {}
    
    public void notifySessionCreated(int param1Int, RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifySessionInfoChanged(RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifySessionReleased(RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void requestCreateSessionByManager(long param1Long, RoutingSessionInfo param1RoutingSessionInfo, MediaRoute2Info param1MediaRoute2Info) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaRouter2 {
    private static final String DESCRIPTOR = "android.media.IMediaRouter2";
    
    static final int TRANSACTION_notifyRouterRegistered = 1;
    
    static final int TRANSACTION_notifyRoutesAdded = 2;
    
    static final int TRANSACTION_notifyRoutesChanged = 4;
    
    static final int TRANSACTION_notifyRoutesRemoved = 3;
    
    static final int TRANSACTION_notifySessionCreated = 5;
    
    static final int TRANSACTION_notifySessionInfoChanged = 6;
    
    static final int TRANSACTION_notifySessionReleased = 7;
    
    static final int TRANSACTION_requestCreateSessionByManager = 8;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaRouter2");
    }
    
    public static IMediaRouter2 asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaRouter2");
      if (iInterface != null && iInterface instanceof IMediaRouter2)
        return (IMediaRouter2)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "requestCreateSessionByManager";
        case 7:
          return "notifySessionReleased";
        case 6:
          return "notifySessionInfoChanged";
        case 5:
          return "notifySessionCreated";
        case 4:
          return "notifyRoutesChanged";
        case 3:
          return "notifyRoutesRemoved";
        case 2:
          return "notifyRoutesAdded";
        case 1:
          break;
      } 
      return "notifyRouterRegistered";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<MediaRoute2Info> arrayList;
      if (param1Int1 != 1598968902) {
        ArrayList<MediaRoute2Info> arrayList1;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.media.IMediaRouter2");
            l = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            requestCreateSessionByManager(l, (RoutingSessionInfo)param1Parcel2, (MediaRoute2Info)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.media.IMediaRouter2");
            if (param1Parcel1.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifySessionReleased((RoutingSessionInfo)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.IMediaRouter2");
            if (param1Parcel1.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifySessionInfoChanged((RoutingSessionInfo)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.IMediaRouter2");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifySessionCreated(param1Int1, (RoutingSessionInfo)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.IMediaRouter2");
            arrayList1 = param1Parcel1.createTypedArrayList(MediaRoute2Info.CREATOR);
            notifyRoutesChanged(arrayList1);
            return true;
          case 3:
            arrayList1.enforceInterface("android.media.IMediaRouter2");
            arrayList1 = arrayList1.createTypedArrayList(MediaRoute2Info.CREATOR);
            notifyRoutesRemoved(arrayList1);
            return true;
          case 2:
            arrayList1.enforceInterface("android.media.IMediaRouter2");
            arrayList1 = arrayList1.createTypedArrayList(MediaRoute2Info.CREATOR);
            notifyRoutesAdded(arrayList1);
            return true;
          case 1:
            break;
        } 
        arrayList1.enforceInterface("android.media.IMediaRouter2");
        arrayList = arrayList1.createTypedArrayList(MediaRoute2Info.CREATOR);
        if (arrayList1.readInt() != 0) {
          RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel((Parcel)arrayList1);
        } else {
          arrayList1 = null;
        } 
        notifyRouterRegistered(arrayList, (RoutingSessionInfo)arrayList1);
        return true;
      } 
      arrayList.writeString("android.media.IMediaRouter2");
      return true;
    }
    
    private static class Proxy implements IMediaRouter2 {
      public static IMediaRouter2 sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaRouter2";
      }
      
      public void notifyRouterRegistered(List<MediaRoute2Info> param2List, RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2");
          parcel.writeTypedList(param2List);
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2.Stub.getDefaultImpl() != null) {
            IMediaRouter2.Stub.getDefaultImpl().notifyRouterRegistered(param2List, param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRoutesAdded(List<MediaRoute2Info> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2.Stub.getDefaultImpl() != null) {
            IMediaRouter2.Stub.getDefaultImpl().notifyRoutesAdded(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRoutesRemoved(List<MediaRoute2Info> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2.Stub.getDefaultImpl() != null) {
            IMediaRouter2.Stub.getDefaultImpl().notifyRoutesRemoved(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRoutesChanged(List<MediaRoute2Info> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2.Stub.getDefaultImpl() != null) {
            IMediaRouter2.Stub.getDefaultImpl().notifyRoutesChanged(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySessionCreated(int param2Int, RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2");
          parcel.writeInt(param2Int);
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2.Stub.getDefaultImpl() != null) {
            IMediaRouter2.Stub.getDefaultImpl().notifySessionCreated(param2Int, param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySessionInfoChanged(RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2");
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2.Stub.getDefaultImpl() != null) {
            IMediaRouter2.Stub.getDefaultImpl().notifySessionInfoChanged(param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySessionReleased(RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2");
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2.Stub.getDefaultImpl() != null) {
            IMediaRouter2.Stub.getDefaultImpl().notifySessionReleased(param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCreateSessionByManager(long param2Long, RoutingSessionInfo param2RoutingSessionInfo, MediaRoute2Info param2MediaRoute2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2");
          parcel.writeLong(param2Long);
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2MediaRoute2Info != null) {
            parcel.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2.Stub.getDefaultImpl() != null) {
            IMediaRouter2.Stub.getDefaultImpl().requestCreateSessionByManager(param2Long, param2RoutingSessionInfo, param2MediaRoute2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaRouter2 param1IMediaRouter2) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaRouter2 != null) {
          Proxy.sDefaultImpl = param1IMediaRouter2;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaRouter2 getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
