package android.media;

public class AudioDevicePortConfig extends AudioPortConfig {
  AudioDevicePortConfig(AudioDevicePort paramAudioDevicePort, int paramInt1, int paramInt2, int paramInt3, AudioGainConfig paramAudioGainConfig) {
    super(paramAudioDevicePort, paramInt1, paramInt2, paramInt3, paramAudioGainConfig);
  }
  
  AudioDevicePortConfig(AudioDevicePortConfig paramAudioDevicePortConfig) {
    this(audioDevicePort, i, j, k, audioGainConfig);
  }
  
  public AudioDevicePort port() {
    return (AudioDevicePort)this.mPort;
  }
}
