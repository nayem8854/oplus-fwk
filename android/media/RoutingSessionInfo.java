package android.media;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.UnaryOperator;

public final class RoutingSessionInfo implements Parcelable {
  public static final Parcelable.Creator<RoutingSessionInfo> CREATOR = new Parcelable.Creator<RoutingSessionInfo>() {
      public RoutingSessionInfo createFromParcel(Parcel param1Parcel) {
        return new RoutingSessionInfo(param1Parcel);
      }
      
      public RoutingSessionInfo[] newArray(int param1Int) {
        return new RoutingSessionInfo[param1Int];
      }
    };
  
  private static final String TAG = "RoutingSessionInfo";
  
  final String mClientPackageName;
  
  final Bundle mControlHints;
  
  final List<String> mDeselectableRoutes;
  
  final String mId;
  
  final boolean mIsSystemSession;
  
  final CharSequence mName;
  
  final String mOwnerPackageName;
  
  final String mProviderId;
  
  final List<String> mSelectableRoutes;
  
  final List<String> mSelectedRoutes;
  
  final List<String> mTransferableRoutes;
  
  final int mVolume;
  
  final int mVolumeHandling;
  
  final int mVolumeMax;
  
  RoutingSessionInfo(Builder paramBuilder) {
    Objects.requireNonNull(paramBuilder, "builder must not be null.");
    this.mId = paramBuilder.mId;
    this.mName = paramBuilder.mName;
    this.mOwnerPackageName = paramBuilder.mOwnerPackageName;
    this.mClientPackageName = paramBuilder.mClientPackageName;
    this.mProviderId = paramBuilder.mProviderId;
    List<String> list = paramBuilder.mSelectedRoutes;
    list = convertToUniqueRouteIds(list);
    this.mSelectedRoutes = Collections.unmodifiableList(list);
    list = paramBuilder.mSelectableRoutes;
    list = convertToUniqueRouteIds(list);
    this.mSelectableRoutes = Collections.unmodifiableList(list);
    list = paramBuilder.mDeselectableRoutes;
    list = convertToUniqueRouteIds(list);
    this.mDeselectableRoutes = Collections.unmodifiableList(list);
    list = paramBuilder.mTransferableRoutes;
    list = convertToUniqueRouteIds(list);
    this.mTransferableRoutes = Collections.unmodifiableList(list);
    this.mVolumeHandling = paramBuilder.mVolumeHandling;
    this.mVolumeMax = paramBuilder.mVolumeMax;
    this.mVolume = paramBuilder.mVolume;
    this.mControlHints = paramBuilder.mControlHints;
    this.mIsSystemSession = paramBuilder.mIsSystemSession;
  }
  
  RoutingSessionInfo(Parcel paramParcel) {
    Objects.requireNonNull(paramParcel, "src must not be null.");
    this.mId = ensureString(paramParcel.readString());
    this.mName = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mOwnerPackageName = paramParcel.readString();
    this.mClientPackageName = ensureString(paramParcel.readString());
    this.mProviderId = paramParcel.readString();
    this.mSelectedRoutes = ensureList(paramParcel.createStringArrayList());
    this.mSelectableRoutes = ensureList(paramParcel.createStringArrayList());
    this.mDeselectableRoutes = ensureList(paramParcel.createStringArrayList());
    this.mTransferableRoutes = ensureList(paramParcel.createStringArrayList());
    this.mVolumeHandling = paramParcel.readInt();
    this.mVolumeMax = paramParcel.readInt();
    this.mVolume = paramParcel.readInt();
    this.mControlHints = paramParcel.readBundle();
    this.mIsSystemSession = paramParcel.readBoolean();
  }
  
  private static String ensureString(String paramString) {
    if (paramString == null)
      paramString = ""; 
    return paramString;
  }
  
  private static <T> List<T> ensureList(List<? extends T> paramList) {
    if (paramList != null)
      return Collections.unmodifiableList(paramList); 
    return Collections.emptyList();
  }
  
  public String getId() {
    String str = this.mProviderId;
    if (str != null)
      return MediaRouter2Utils.toUniqueId(str, this.mId); 
    return this.mId;
  }
  
  public CharSequence getName() {
    return this.mName;
  }
  
  public String getOriginalId() {
    return this.mId;
  }
  
  public String getOwnerPackageName() {
    return this.mOwnerPackageName;
  }
  
  public String getClientPackageName() {
    return this.mClientPackageName;
  }
  
  public String getProviderId() {
    return this.mProviderId;
  }
  
  public List<String> getSelectedRoutes() {
    return this.mSelectedRoutes;
  }
  
  public List<String> getSelectableRoutes() {
    return this.mSelectableRoutes;
  }
  
  public List<String> getDeselectableRoutes() {
    return this.mDeselectableRoutes;
  }
  
  public List<String> getTransferableRoutes() {
    return this.mTransferableRoutes;
  }
  
  public int getVolumeHandling() {
    return this.mVolumeHandling;
  }
  
  public int getVolumeMax() {
    return this.mVolumeMax;
  }
  
  public int getVolume() {
    return this.mVolume;
  }
  
  public Bundle getControlHints() {
    return this.mControlHints;
  }
  
  public boolean isSystemSession() {
    return this.mIsSystemSession;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    paramParcel.writeCharSequence(this.mName);
    paramParcel.writeString(this.mOwnerPackageName);
    paramParcel.writeString(this.mClientPackageName);
    paramParcel.writeString(this.mProviderId);
    paramParcel.writeStringList(this.mSelectedRoutes);
    paramParcel.writeStringList(this.mSelectableRoutes);
    paramParcel.writeStringList(this.mDeselectableRoutes);
    paramParcel.writeStringList(this.mTransferableRoutes);
    paramParcel.writeInt(this.mVolumeHandling);
    paramParcel.writeInt(this.mVolumeMax);
    paramParcel.writeInt(this.mVolume);
    paramParcel.writeBundle(this.mControlHints);
    paramParcel.writeBoolean(this.mIsSystemSession);
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof RoutingSessionInfo))
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mId, ((RoutingSessionInfo)paramObject).mId)) {
      CharSequence charSequence1 = this.mName, charSequence2 = ((RoutingSessionInfo)paramObject).mName;
      if (Objects.equals(charSequence1, charSequence2)) {
        charSequence2 = this.mOwnerPackageName;
        charSequence1 = ((RoutingSessionInfo)paramObject).mOwnerPackageName;
        if (Objects.equals(charSequence2, charSequence1)) {
          charSequence1 = this.mClientPackageName;
          charSequence2 = ((RoutingSessionInfo)paramObject).mClientPackageName;
          if (Objects.equals(charSequence1, charSequence2)) {
            charSequence1 = this.mProviderId;
            charSequence2 = ((RoutingSessionInfo)paramObject).mProviderId;
            if (Objects.equals(charSequence1, charSequence2)) {
              List<String> list1 = this.mSelectedRoutes, list2 = ((RoutingSessionInfo)paramObject).mSelectedRoutes;
              if (Objects.equals(list1, list2)) {
                list2 = this.mSelectableRoutes;
                list1 = ((RoutingSessionInfo)paramObject).mSelectableRoutes;
                if (Objects.equals(list2, list1)) {
                  list1 = this.mDeselectableRoutes;
                  list2 = ((RoutingSessionInfo)paramObject).mDeselectableRoutes;
                  if (Objects.equals(list1, list2)) {
                    list1 = this.mTransferableRoutes;
                    list2 = ((RoutingSessionInfo)paramObject).mTransferableRoutes;
                    if (Objects.equals(list1, list2) && this.mVolumeHandling == ((RoutingSessionInfo)paramObject).mVolumeHandling && this.mVolumeMax == ((RoutingSessionInfo)paramObject).mVolumeMax && this.mVolume == ((RoutingSessionInfo)paramObject).mVolume)
                      return null; 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    String str1 = this.mId;
    CharSequence charSequence = this.mName;
    String str2 = this.mOwnerPackageName, str3 = this.mClientPackageName, str4 = this.mProviderId;
    List<String> list1 = this.mSelectedRoutes, list2 = this.mSelectableRoutes, list3 = this.mDeselectableRoutes, list4 = this.mTransferableRoutes;
    int i = this.mVolumeMax;
    int j = this.mVolumeHandling, k = this.mVolume;
    return Objects.hash(new Object[] { 
          str1, charSequence, str2, str3, str4, list1, list2, list3, list4, Integer.valueOf(i), 
          Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("RoutingSessionInfo{ ");
    stringBuilder.append("sessionId=");
    stringBuilder.append(getId());
    stringBuilder.append(", name=");
    stringBuilder.append(getName());
    stringBuilder.append(", selectedRoutes={");
    stringBuilder.append(String.join(",", (Iterable)getSelectedRoutes()));
    stringBuilder.append("}");
    stringBuilder.append(", selectableRoutes={");
    stringBuilder.append(String.join(",", (Iterable)getSelectableRoutes()));
    stringBuilder.append("}");
    stringBuilder.append(", deselectableRoutes={");
    stringBuilder.append(String.join(",", (Iterable)getDeselectableRoutes()));
    stringBuilder.append("}");
    stringBuilder.append(", transferableRoutes={");
    stringBuilder.append(String.join(",", (Iterable)getTransferableRoutes()));
    stringBuilder.append("}");
    stringBuilder.append(", volumeHandling=");
    stringBuilder.append(getVolumeHandling());
    stringBuilder.append(", volumeMax=");
    stringBuilder.append(getVolumeMax());
    stringBuilder.append(", volume=");
    stringBuilder.append(getVolume());
    stringBuilder = stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  private List<String> convertToUniqueRouteIds(List<String> paramList) {
    if (paramList == null) {
      Log.w("RoutingSessionInfo", "routeIds is null. Returning an empty list");
      return Collections.emptyList();
    } 
    if (this.mProviderId == null)
      return paramList; 
    ArrayList<String> arrayList = new ArrayList();
    for (String str : paramList)
      arrayList.add(MediaRouter2Utils.toUniqueId(this.mProviderId, str)); 
    return arrayList;
  }
  
  class Builder {
    String mClientPackageName;
    
    Bundle mControlHints;
    
    final List<String> mDeselectableRoutes;
    
    final String mId;
    
    boolean mIsSystemSession;
    
    CharSequence mName;
    
    String mOwnerPackageName;
    
    String mProviderId;
    
    final List<String> mSelectableRoutes;
    
    final List<String> mSelectedRoutes;
    
    final List<String> mTransferableRoutes;
    
    int mVolume;
    
    int mVolumeHandling = 0;
    
    int mVolumeMax;
    
    public Builder(RoutingSessionInfo this$0, String param1String1) {
      if (!TextUtils.isEmpty((CharSequence)this$0)) {
        this.mId = (String)this$0;
        Objects.requireNonNull(param1String1, "clientPackageName must not be null");
        this.mClientPackageName = param1String1;
        this.mSelectedRoutes = new ArrayList<>();
        this.mSelectableRoutes = new ArrayList<>();
        this.mDeselectableRoutes = new ArrayList<>();
        this.mTransferableRoutes = new ArrayList<>();
        return;
      } 
      throw new IllegalArgumentException("id must not be empty");
    }
    
    public Builder(RoutingSessionInfo this$0) {
      Objects.requireNonNull(this$0, "sessionInfo must not be null");
      this.mId = this$0.mId;
      this.mName = this$0.mName;
      this.mClientPackageName = this$0.mClientPackageName;
      this.mProviderId = this$0.mProviderId;
      this.mSelectedRoutes = new ArrayList<>(this$0.mSelectedRoutes);
      this.mSelectableRoutes = new ArrayList<>(this$0.mSelectableRoutes);
      this.mDeselectableRoutes = new ArrayList<>(this$0.mDeselectableRoutes);
      this.mTransferableRoutes = new ArrayList<>(this$0.mTransferableRoutes);
      if (this.mProviderId != null) {
        this.mSelectedRoutes.replaceAll((UnaryOperator<String>)_$$Lambda$ibOGYFjCUH2v25L_zBCFVaU3qjg.INSTANCE);
        this.mSelectableRoutes.replaceAll((UnaryOperator<String>)_$$Lambda$ibOGYFjCUH2v25L_zBCFVaU3qjg.INSTANCE);
        this.mDeselectableRoutes.replaceAll((UnaryOperator<String>)_$$Lambda$ibOGYFjCUH2v25L_zBCFVaU3qjg.INSTANCE);
        this.mTransferableRoutes.replaceAll((UnaryOperator<String>)_$$Lambda$ibOGYFjCUH2v25L_zBCFVaU3qjg.INSTANCE);
      } 
      this.mVolumeHandling = this$0.mVolumeHandling;
      this.mVolumeMax = this$0.mVolumeMax;
      this.mVolume = this$0.mVolume;
      this.mControlHints = this$0.mControlHints;
      this.mIsSystemSession = this$0.mIsSystemSession;
    }
    
    public Builder setName(CharSequence param1CharSequence) {
      this.mName = param1CharSequence;
      return this;
    }
    
    public Builder setOwnerPackageName(String param1String) {
      this.mOwnerPackageName = param1String;
      return this;
    }
    
    public Builder setClientPackageName(String param1String) {
      this.mClientPackageName = param1String;
      return this;
    }
    
    public Builder setProviderId(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mProviderId = param1String;
        return this;
      } 
      throw new IllegalArgumentException("providerId must not be empty");
    }
    
    public Builder clearSelectedRoutes() {
      this.mSelectedRoutes.clear();
      return this;
    }
    
    public Builder addSelectedRoute(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mSelectedRoutes.add(param1String);
        return this;
      } 
      throw new IllegalArgumentException("routeId must not be empty");
    }
    
    public Builder removeSelectedRoute(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mSelectedRoutes.remove(param1String);
        return this;
      } 
      throw new IllegalArgumentException("routeId must not be empty");
    }
    
    public Builder clearSelectableRoutes() {
      this.mSelectableRoutes.clear();
      return this;
    }
    
    public Builder addSelectableRoute(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mSelectableRoutes.add(param1String);
        return this;
      } 
      throw new IllegalArgumentException("routeId must not be empty");
    }
    
    public Builder removeSelectableRoute(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mSelectableRoutes.remove(param1String);
        return this;
      } 
      throw new IllegalArgumentException("routeId must not be empty");
    }
    
    public Builder clearDeselectableRoutes() {
      this.mDeselectableRoutes.clear();
      return this;
    }
    
    public Builder addDeselectableRoute(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mDeselectableRoutes.add(param1String);
        return this;
      } 
      throw new IllegalArgumentException("routeId must not be empty");
    }
    
    public Builder removeDeselectableRoute(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mDeselectableRoutes.remove(param1String);
        return this;
      } 
      throw new IllegalArgumentException("routeId must not be empty");
    }
    
    public Builder clearTransferableRoutes() {
      this.mTransferableRoutes.clear();
      return this;
    }
    
    public Builder addTransferableRoute(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mTransferableRoutes.add(param1String);
        return this;
      } 
      throw new IllegalArgumentException("routeId must not be empty");
    }
    
    public Builder removeTransferableRoute(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mTransferableRoutes.remove(param1String);
        return this;
      } 
      throw new IllegalArgumentException("routeId must not be empty");
    }
    
    public Builder setVolumeHandling(int param1Int) {
      this.mVolumeHandling = param1Int;
      return this;
    }
    
    public Builder setVolumeMax(int param1Int) {
      this.mVolumeMax = param1Int;
      return this;
    }
    
    public Builder setVolume(int param1Int) {
      this.mVolume = param1Int;
      return this;
    }
    
    public Builder setControlHints(Bundle param1Bundle) {
      this.mControlHints = param1Bundle;
      return this;
    }
    
    public Builder setSystemSession(boolean param1Boolean) {
      this.mIsSystemSession = param1Boolean;
      return this;
    }
    
    public RoutingSessionInfo build() {
      if (!this.mSelectedRoutes.isEmpty())
        return new RoutingSessionInfo(this); 
      throw new IllegalArgumentException("selectedRoutes must not be empty");
    }
  }
}
