package android.media;

import android.graphics.Rect;
import android.os.Parcel;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public final class TimedText {
  private final HashMap<Integer, Object> mKeyObjectMap = new HashMap<>();
  
  private int mDisplayFlags = -1;
  
  private int mBackgroundColorRGBA = -1;
  
  private int mHighlightColorRGBA = -1;
  
  private int mScrollDelay = -1;
  
  private int mWrapText = -1;
  
  private List<CharPos> mBlinkingPosList = null;
  
  private List<CharPos> mHighlightPosList = null;
  
  private List<Karaoke> mKaraokeList = null;
  
  private List<Font> mFontList = null;
  
  private List<Style> mStyleList = null;
  
  private List<HyperText> mHyperTextList = null;
  
  private Rect mTextBounds = null;
  
  private String mTextChars = null;
  
  private static final int FIRST_PRIVATE_KEY = 101;
  
  private static final int FIRST_PUBLIC_KEY = 1;
  
  private static final int KEY_BACKGROUND_COLOR_RGBA = 3;
  
  private static final int KEY_DISPLAY_FLAGS = 1;
  
  private static final int KEY_END_CHAR = 104;
  
  private static final int KEY_FONT_ID = 105;
  
  private static final int KEY_FONT_SIZE = 106;
  
  private static final int KEY_GLOBAL_SETTING = 101;
  
  private static final int KEY_HIGHLIGHT_COLOR_RGBA = 4;
  
  private static final int KEY_LOCAL_SETTING = 102;
  
  private static final int KEY_SCROLL_DELAY = 5;
  
  private static final int KEY_START_CHAR = 103;
  
  private static final int KEY_START_TIME = 7;
  
  private static final int KEY_STRUCT_BLINKING_TEXT_LIST = 8;
  
  private static final int KEY_STRUCT_FONT_LIST = 9;
  
  private static final int KEY_STRUCT_HIGHLIGHT_LIST = 10;
  
  private static final int KEY_STRUCT_HYPER_TEXT_LIST = 11;
  
  private static final int KEY_STRUCT_JUSTIFICATION = 15;
  
  private static final int KEY_STRUCT_KARAOKE_LIST = 12;
  
  private static final int KEY_STRUCT_STYLE_LIST = 13;
  
  private static final int KEY_STRUCT_TEXT = 16;
  
  private static final int KEY_STRUCT_TEXT_POS = 14;
  
  private static final int KEY_STYLE_FLAGS = 2;
  
  private static final int KEY_TEXT_COLOR_RGBA = 107;
  
  private static final int KEY_WRAP_TEXT = 6;
  
  private static final int LAST_PRIVATE_KEY = 107;
  
  private static final int LAST_PUBLIC_KEY = 16;
  
  private static final String TAG = "TimedText";
  
  private Justification mJustification;
  
  public static final class CharPos {
    public final int endChar;
    
    public final int startChar;
    
    public CharPos(int param1Int1, int param1Int2) {
      this.startChar = param1Int1;
      this.endChar = param1Int2;
    }
  }
  
  public static final class Justification {
    public final int horizontalJustification;
    
    public final int verticalJustification;
    
    public Justification(int param1Int1, int param1Int2) {
      this.horizontalJustification = param1Int1;
      this.verticalJustification = param1Int2;
    }
  }
  
  public static final class Style {
    public final int colorRGBA;
    
    public final int endChar;
    
    public final int fontID;
    
    public final int fontSize;
    
    public final boolean isBold;
    
    public final boolean isItalic;
    
    public final boolean isUnderlined;
    
    public final int startChar;
    
    public Style(int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, int param1Int4, int param1Int5) {
      this.startChar = param1Int1;
      this.endChar = param1Int2;
      this.fontID = param1Int3;
      this.isBold = param1Boolean1;
      this.isItalic = param1Boolean2;
      this.isUnderlined = param1Boolean3;
      this.fontSize = param1Int4;
      this.colorRGBA = param1Int5;
    }
  }
  
  public static final class Font {
    public final int ID;
    
    public final String name;
    
    public Font(int param1Int, String param1String) {
      this.ID = param1Int;
      this.name = param1String;
    }
  }
  
  public static final class Karaoke {
    public final int endChar;
    
    public final int endTimeMs;
    
    public final int startChar;
    
    public final int startTimeMs;
    
    public Karaoke(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.startTimeMs = param1Int1;
      this.endTimeMs = param1Int2;
      this.startChar = param1Int3;
      this.endChar = param1Int4;
    }
  }
  
  public static final class HyperText {
    public final String URL;
    
    public final String altString;
    
    public final int endChar;
    
    public final int startChar;
    
    public HyperText(int param1Int1, int param1Int2, String param1String1, String param1String2) {
      this.startChar = param1Int1;
      this.endChar = param1Int2;
      this.URL = param1String1;
      this.altString = param1String2;
    }
  }
  
  public TimedText(Parcel paramParcel) {
    if (parseParcel(paramParcel))
      return; 
    this.mKeyObjectMap.clear();
    throw new IllegalArgumentException("parseParcel() fails");
  }
  
  public TimedText(String paramString, Rect paramRect) {
    this.mTextChars = paramString;
    this.mTextBounds = paramRect;
  }
  
  public String getText() {
    return this.mTextChars;
  }
  
  public Rect getBounds() {
    return this.mTextBounds;
  }
  
  private boolean parseParcel(Parcel paramParcel) {
    StringBuilder stringBuilder;
    paramParcel.setDataPosition(0);
    if (paramParcel.dataAvail() == 0)
      return false; 
    int i = paramParcel.readInt();
    if (i == 102) {
      i = paramParcel.readInt();
      if (i != 7)
        return false; 
      int j = paramParcel.readInt();
      this.mKeyObjectMap.put(Integer.valueOf(i), Integer.valueOf(j));
      i = paramParcel.readInt();
      if (i != 16)
        return false; 
      paramParcel.readInt();
      byte[] arrayOfByte = paramParcel.createByteArray();
      if (arrayOfByte == null || arrayOfByte.length == 0) {
        this.mTextChars = null;
      } else {
        this.mTextChars = new String(arrayOfByte);
      } 
    } else if (i != 101) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid timed text key found: ");
      stringBuilder.append(i);
      Log.w("TimedText", stringBuilder.toString());
      return false;
    } 
    while (stringBuilder.dataAvail() > 0) {
      int j;
      List<Style> list5;
      List<Karaoke> list4;
      List<HyperText> list3;
      List<CharPos> list2;
      List<Font> list;
      List<CharPos> list1;
      Integer integer;
      int k, m, n;
      i = stringBuilder.readInt();
      if (!isValidKey(i)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid timed text key found: ");
        stringBuilder.append(i);
        Log.w("TimedText", stringBuilder.toString());
        return false;
      } 
      Justification justification = null;
      switch (i) {
        case 15:
          j = stringBuilder.readInt();
          k = stringBuilder.readInt();
          this.mJustification = new Justification(j, k);
          justification = this.mJustification;
          break;
        case 14:
          m = stringBuilder.readInt();
          n = stringBuilder.readInt();
          k = stringBuilder.readInt();
          j = stringBuilder.readInt();
          this.mTextBounds = new Rect(n, m, j, k);
          break;
        case 13:
          readStyle((Parcel)stringBuilder);
          list5 = this.mStyleList;
          break;
        case 12:
          readKaraoke((Parcel)stringBuilder);
          list4 = this.mKaraokeList;
          break;
        case 11:
          readHyperText((Parcel)stringBuilder);
          list3 = this.mHyperTextList;
          break;
        case 10:
          readHighlight((Parcel)stringBuilder);
          list2 = this.mHighlightPosList;
          break;
        case 9:
          readFont((Parcel)stringBuilder);
          list = this.mFontList;
          break;
        case 8:
          readBlinkingText((Parcel)stringBuilder);
          list1 = this.mBlinkingPosList;
          break;
        case 6:
          this.mWrapText = j = stringBuilder.readInt();
          integer = Integer.valueOf(j);
          break;
        case 5:
          this.mScrollDelay = j = stringBuilder.readInt();
          integer = Integer.valueOf(j);
          break;
        case 4:
          this.mHighlightColorRGBA = j = stringBuilder.readInt();
          integer = Integer.valueOf(j);
          break;
        case 3:
          this.mBackgroundColorRGBA = j = stringBuilder.readInt();
          integer = Integer.valueOf(j);
          break;
        case 1:
          this.mDisplayFlags = j = stringBuilder.readInt();
          integer = Integer.valueOf(j);
          break;
      } 
      if (integer != null) {
        if (this.mKeyObjectMap.containsKey(Integer.valueOf(i)))
          this.mKeyObjectMap.remove(Integer.valueOf(i)); 
        this.mKeyObjectMap.put(Integer.valueOf(i), integer);
      } 
    } 
    return true;
  }
  
  private void readStyle(Parcel paramParcel) {
    boolean bool1 = false;
    int i = -1, j = -1, k = -1;
    boolean bool2 = false, bool3 = false, bool4 = false;
    int m = -1, n = -1;
    while (!bool1 && paramParcel.dataAvail() > 0) {
      int i1 = paramParcel.readInt();
      if (i1 != 2) {
        switch (i1) {
          default:
            paramParcel.setDataPosition(paramParcel.dataPosition() - 4);
            bool1 = true;
            continue;
          case 107:
            n = paramParcel.readInt();
            continue;
          case 106:
            m = paramParcel.readInt();
            continue;
          case 105:
            k = paramParcel.readInt();
            continue;
          case 104:
            j = paramParcel.readInt();
            continue;
          case 103:
            break;
        } 
        i = paramParcel.readInt();
        continue;
      } 
      i1 = paramParcel.readInt();
      bool3 = false;
      if (i1 % 2 == 1) {
        bool4 = true;
      } else {
        bool4 = false;
      } 
      if (i1 % 4 >= 2) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (i1 / 4 == 1)
        bool3 = true; 
      boolean bool = bool4;
      bool4 = bool3;
      bool3 = bool2;
      bool2 = bool;
    } 
    Style style = new Style(i, j, k, bool2, bool3, bool4, m, n);
    if (this.mStyleList == null)
      this.mStyleList = new ArrayList<>(); 
    this.mStyleList.add(style);
  }
  
  private void readFont(Parcel paramParcel) {
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      int j = paramParcel.readInt();
      int k = paramParcel.readInt();
      byte[] arrayOfByte = paramParcel.createByteArray();
      String str = new String(arrayOfByte, 0, k);
      Font font = new Font(j, str);
      if (this.mFontList == null)
        this.mFontList = new ArrayList<>(); 
      this.mFontList.add(font);
    } 
  }
  
  private void readHighlight(Parcel paramParcel) {
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    CharPos charPos = new CharPos(i, j);
    if (this.mHighlightPosList == null)
      this.mHighlightPosList = new ArrayList<>(); 
    this.mHighlightPosList.add(charPos);
  }
  
  private void readKaraoke(Parcel paramParcel) {
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      int j = paramParcel.readInt();
      int k = paramParcel.readInt();
      int m = paramParcel.readInt();
      int n = paramParcel.readInt();
      Karaoke karaoke = new Karaoke(j, k, m, n);
      if (this.mKaraokeList == null)
        this.mKaraokeList = new ArrayList<>(); 
      this.mKaraokeList.add(karaoke);
    } 
  }
  
  private void readHyperText(Parcel paramParcel) {
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    byte[] arrayOfByte2 = paramParcel.createByteArray();
    String str2 = new String(arrayOfByte2, 0, k);
    k = paramParcel.readInt();
    byte[] arrayOfByte1 = paramParcel.createByteArray();
    String str1 = new String(arrayOfByte1, 0, k);
    HyperText hyperText = new HyperText(i, j, str2, str1);
    if (this.mHyperTextList == null)
      this.mHyperTextList = new ArrayList<>(); 
    this.mHyperTextList.add(hyperText);
  }
  
  private void readBlinkingText(Parcel paramParcel) {
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    CharPos charPos = new CharPos(i, j);
    if (this.mBlinkingPosList == null)
      this.mBlinkingPosList = new ArrayList<>(); 
    this.mBlinkingPosList.add(charPos);
  }
  
  private boolean isValidKey(int paramInt) {
    if ((paramInt < 1 || paramInt > 16) && (paramInt < 101 || paramInt > 107))
      return false; 
    return true;
  }
  
  private boolean containsKey(int paramInt) {
    if (isValidKey(paramInt) && this.mKeyObjectMap.containsKey(Integer.valueOf(paramInt)))
      return true; 
    return false;
  }
  
  private Set keySet() {
    return this.mKeyObjectMap.keySet();
  }
  
  private Object getObject(int paramInt) {
    if (containsKey(paramInt))
      return this.mKeyObjectMap.get(Integer.valueOf(paramInt)); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid key: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
