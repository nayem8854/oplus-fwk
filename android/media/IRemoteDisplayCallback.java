package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteDisplayCallback extends IInterface {
  void onStateChanged(RemoteDisplayState paramRemoteDisplayState) throws RemoteException;
  
  class Default implements IRemoteDisplayCallback {
    public void onStateChanged(RemoteDisplayState param1RemoteDisplayState) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteDisplayCallback {
    private static final String DESCRIPTOR = "android.media.IRemoteDisplayCallback";
    
    static final int TRANSACTION_onStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IRemoteDisplayCallback");
    }
    
    public static IRemoteDisplayCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IRemoteDisplayCallback");
      if (iInterface != null && iInterface instanceof IRemoteDisplayCallback)
        return (IRemoteDisplayCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IRemoteDisplayCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IRemoteDisplayCallback");
      if (param1Parcel1.readInt() != 0) {
        RemoteDisplayState remoteDisplayState = RemoteDisplayState.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onStateChanged((RemoteDisplayState)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IRemoteDisplayCallback {
      public static IRemoteDisplayCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IRemoteDisplayCallback";
      }
      
      public void onStateChanged(RemoteDisplayState param2RemoteDisplayState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteDisplayCallback");
          if (param2RemoteDisplayState != null) {
            parcel.writeInt(1);
            param2RemoteDisplayState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRemoteDisplayCallback.Stub.getDefaultImpl() != null) {
            IRemoteDisplayCallback.Stub.getDefaultImpl().onStateChanged(param2RemoteDisplayState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteDisplayCallback param1IRemoteDisplayCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteDisplayCallback != null) {
          Proxy.sDefaultImpl = param1IRemoteDisplayCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteDisplayCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
