package android.media;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.SparseIntArray;

@Deprecated
public abstract class MediaMetadataEditor {
  protected boolean mMetadataChanged = false;
  
  protected boolean mApplied = false;
  
  protected boolean mArtworkChanged = false;
  
  public static final int BITMAP_KEY_ARTWORK = 100;
  
  public static final int KEY_EDITABLE_MASK = 536870911;
  
  protected static final SparseIntArray METADATA_KEYS_TYPE;
  
  protected static final int METADATA_TYPE_BITMAP = 2;
  
  protected static final int METADATA_TYPE_INVALID = -1;
  
  protected static final int METADATA_TYPE_LONG = 0;
  
  protected static final int METADATA_TYPE_RATING = 3;
  
  protected static final int METADATA_TYPE_STRING = 1;
  
  public static final int RATING_KEY_BY_OTHERS = 101;
  
  public static final int RATING_KEY_BY_USER = 268435457;
  
  private static final String TAG = "MediaMetadataEditor";
  
  protected long mEditableKeys;
  
  protected Bitmap mEditorArtwork;
  
  protected Bundle mEditorMetadata;
  
  protected MediaMetadata.Builder mMetadataBuilder;
  
  public void clear() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mApplied : Z
    //   6: ifeq -> 20
    //   9: ldc 'MediaMetadataEditor'
    //   11: ldc 'Can't clear a previously applied MediaMetadataEditor'
    //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: return
    //   20: aload_0
    //   21: getfield mEditorMetadata : Landroid/os/Bundle;
    //   24: invokevirtual clear : ()V
    //   27: aload_0
    //   28: aconst_null
    //   29: putfield mEditorArtwork : Landroid/graphics/Bitmap;
    //   32: new android/media/MediaMetadata$Builder
    //   35: astore_1
    //   36: aload_1
    //   37: invokespecial <init> : ()V
    //   40: aload_0
    //   41: aload_1
    //   42: putfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
    //   45: aload_0
    //   46: monitorexit
    //   47: return
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #125	-> 2
    //   #126	-> 9
    //   #127	-> 17
    //   #129	-> 20
    //   #130	-> 27
    //   #131	-> 32
    //   #132	-> 45
    //   #124	-> 48
    // Exception table:
    //   from	to	target	type
    //   2	9	48	finally
    //   9	17	48	finally
    //   20	27	48	finally
    //   27	32	48	finally
    //   32	45	48	finally
  }
  
  public void addEditableKey(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mApplied : Z
    //   6: ifeq -> 20
    //   9: ldc 'MediaMetadataEditor'
    //   11: ldc 'Can't change editable keys of a previously applied MetadataEditor'
    //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: return
    //   20: iload_1
    //   21: ldc 268435457
    //   23: if_icmpne -> 48
    //   26: aload_0
    //   27: aload_0
    //   28: getfield mEditableKeys : J
    //   31: ldc 536870911
    //   33: iload_1
    //   34: iand
    //   35: i2l
    //   36: lor
    //   37: putfield mEditableKeys : J
    //   40: aload_0
    //   41: iconst_1
    //   42: putfield mMetadataChanged : Z
    //   45: goto -> 86
    //   48: new java/lang/StringBuilder
    //   51: astore_2
    //   52: aload_2
    //   53: invokespecial <init> : ()V
    //   56: aload_2
    //   57: ldc 'Metadata key '
    //   59: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   62: pop
    //   63: aload_2
    //   64: iload_1
    //   65: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload_2
    //   70: ldc ' cannot be edited'
    //   72: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   75: pop
    //   76: ldc 'MediaMetadataEditor'
    //   78: aload_2
    //   79: invokevirtual toString : ()Ljava/lang/String;
    //   82: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   85: pop
    //   86: aload_0
    //   87: monitorexit
    //   88: return
    //   89: astore_2
    //   90: aload_0
    //   91: monitorexit
    //   92: aload_2
    //   93: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #143	-> 2
    //   #144	-> 9
    //   #145	-> 17
    //   #149	-> 20
    //   #150	-> 26
    //   #151	-> 40
    //   #153	-> 48
    //   #155	-> 86
    //   #142	-> 89
    // Exception table:
    //   from	to	target	type
    //   2	9	89	finally
    //   9	17	89	finally
    //   26	40	89	finally
    //   40	45	89	finally
    //   48	86	89	finally
  }
  
  public void removeEditableKeys() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mApplied : Z
    //   6: ifeq -> 20
    //   9: ldc 'MediaMetadataEditor'
    //   11: ldc 'Can't remove all editable keys of a previously applied MetadataEditor'
    //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: return
    //   20: aload_0
    //   21: getfield mEditableKeys : J
    //   24: lconst_0
    //   25: lcmp
    //   26: ifeq -> 39
    //   29: aload_0
    //   30: lconst_0
    //   31: putfield mEditableKeys : J
    //   34: aload_0
    //   35: iconst_1
    //   36: putfield mMetadataChanged : Z
    //   39: aload_0
    //   40: monitorexit
    //   41: return
    //   42: astore_1
    //   43: aload_0
    //   44: monitorexit
    //   45: aload_1
    //   46: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #161	-> 2
    //   #162	-> 9
    //   #163	-> 17
    //   #165	-> 20
    //   #166	-> 29
    //   #167	-> 34
    //   #169	-> 39
    //   #160	-> 42
    // Exception table:
    //   from	to	target	type
    //   2	9	42	finally
    //   9	17	42	finally
    //   20	29	42	finally
    //   29	34	42	finally
    //   34	39	42	finally
  }
  
  public int[] getEditableKeys() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mEditableKeys : J
    //   6: ldc2_w 268435457
    //   9: lcmp
    //   10: ifne -> 24
    //   13: aload_0
    //   14: monitorexit
    //   15: iconst_1
    //   16: newarray int
    //   18: dup
    //   19: iconst_0
    //   20: ldc 268435457
    //   22: iastore
    //   23: areturn
    //   24: aload_0
    //   25: monitorexit
    //   26: aconst_null
    //   27: areturn
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #177	-> 2
    //   #178	-> 13
    //   #179	-> 13
    //   #181	-> 24
    //   #176	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	13	28	finally
  }
  
  public MediaMetadataEditor putString(int paramInt, String paramString) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mApplied : Z
    //   6: ifeq -> 21
    //   9: ldc 'MediaMetadataEditor'
    //   11: ldc 'Can't edit a previously applied MediaMetadataEditor'
    //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: aload_0
    //   20: areturn
    //   21: getstatic android/media/MediaMetadataEditor.METADATA_KEYS_TYPE : Landroid/util/SparseIntArray;
    //   24: iload_1
    //   25: iconst_m1
    //   26: invokevirtual get : (II)I
    //   29: iconst_1
    //   30: if_icmpne -> 54
    //   33: aload_0
    //   34: getfield mEditorMetadata : Landroid/os/Bundle;
    //   37: iload_1
    //   38: invokestatic valueOf : (I)Ljava/lang/String;
    //   41: aload_2
    //   42: invokevirtual putString : (Ljava/lang/String;Ljava/lang/String;)V
    //   45: aload_0
    //   46: iconst_1
    //   47: putfield mMetadataChanged : Z
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_0
    //   53: areturn
    //   54: new java/lang/IllegalArgumentException
    //   57: astore_2
    //   58: new java/lang/StringBuilder
    //   61: astore_3
    //   62: aload_3
    //   63: invokespecial <init> : ()V
    //   66: aload_3
    //   67: ldc 'Invalid type 'String' for key '
    //   69: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload_3
    //   74: iload_1
    //   75: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload_2
    //   80: aload_3
    //   81: invokevirtual toString : ()Ljava/lang/String;
    //   84: invokespecial <init> : (Ljava/lang/String;)V
    //   87: aload_2
    //   88: athrow
    //   89: astore_2
    //   90: aload_0
    //   91: monitorexit
    //   92: aload_2
    //   93: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #207	-> 2
    //   #208	-> 9
    //   #209	-> 17
    //   #211	-> 21
    //   #214	-> 33
    //   #215	-> 45
    //   #216	-> 50
    //   #212	-> 54
    //   #206	-> 89
    // Exception table:
    //   from	to	target	type
    //   2	9	89	finally
    //   9	17	89	finally
    //   21	33	89	finally
    //   33	45	89	finally
    //   45	50	89	finally
    //   54	89	89	finally
  }
  
  public MediaMetadataEditor putLong(int paramInt, long paramLong) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mApplied : Z
    //   6: ifeq -> 21
    //   9: ldc 'MediaMetadataEditor'
    //   11: ldc 'Can't edit a previously applied MediaMetadataEditor'
    //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: aload_0
    //   20: areturn
    //   21: getstatic android/media/MediaMetadataEditor.METADATA_KEYS_TYPE : Landroid/util/SparseIntArray;
    //   24: iload_1
    //   25: iconst_m1
    //   26: invokevirtual get : (II)I
    //   29: ifne -> 53
    //   32: aload_0
    //   33: getfield mEditorMetadata : Landroid/os/Bundle;
    //   36: iload_1
    //   37: invokestatic valueOf : (I)Ljava/lang/String;
    //   40: lload_2
    //   41: invokevirtual putLong : (Ljava/lang/String;J)V
    //   44: aload_0
    //   45: iconst_1
    //   46: putfield mMetadataChanged : Z
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_0
    //   52: areturn
    //   53: new java/lang/IllegalArgumentException
    //   56: astore #4
    //   58: new java/lang/StringBuilder
    //   61: astore #5
    //   63: aload #5
    //   65: invokespecial <init> : ()V
    //   68: aload #5
    //   70: ldc 'Invalid type 'long' for key '
    //   72: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   75: pop
    //   76: aload #5
    //   78: iload_1
    //   79: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: aload #4
    //   85: aload #5
    //   87: invokevirtual toString : ()Ljava/lang/String;
    //   90: invokespecial <init> : (Ljava/lang/String;)V
    //   93: aload #4
    //   95: athrow
    //   96: astore #4
    //   98: aload_0
    //   99: monitorexit
    //   100: aload #4
    //   102: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #236	-> 2
    //   #237	-> 9
    //   #238	-> 17
    //   #240	-> 21
    //   #243	-> 32
    //   #244	-> 44
    //   #245	-> 49
    //   #241	-> 53
    //   #235	-> 96
    // Exception table:
    //   from	to	target	type
    //   2	9	96	finally
    //   9	17	96	finally
    //   21	32	96	finally
    //   32	44	96	finally
    //   44	49	96	finally
    //   53	96	96	finally
  }
  
  public MediaMetadataEditor putBitmap(int paramInt, Bitmap paramBitmap) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mApplied : Z
    //   6: ifeq -> 21
    //   9: ldc 'MediaMetadataEditor'
    //   11: ldc 'Can't edit a previously applied MediaMetadataEditor'
    //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: aload_0
    //   20: areturn
    //   21: iload_1
    //   22: bipush #100
    //   24: if_icmpne -> 41
    //   27: aload_0
    //   28: aload_2
    //   29: putfield mEditorArtwork : Landroid/graphics/Bitmap;
    //   32: aload_0
    //   33: iconst_1
    //   34: putfield mArtworkChanged : Z
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_0
    //   40: areturn
    //   41: new java/lang/IllegalArgumentException
    //   44: astore_3
    //   45: new java/lang/StringBuilder
    //   48: astore_2
    //   49: aload_2
    //   50: invokespecial <init> : ()V
    //   53: aload_2
    //   54: ldc 'Invalid type 'Bitmap' for key '
    //   56: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload_2
    //   61: iload_1
    //   62: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   65: pop
    //   66: aload_3
    //   67: aload_2
    //   68: invokevirtual toString : ()Ljava/lang/String;
    //   71: invokespecial <init> : (Ljava/lang/String;)V
    //   74: aload_3
    //   75: athrow
    //   76: astore_2
    //   77: aload_0
    //   78: monitorexit
    //   79: aload_2
    //   80: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #260	-> 2
    //   #261	-> 9
    //   #262	-> 17
    //   #264	-> 21
    //   #267	-> 27
    //   #268	-> 32
    //   #269	-> 37
    //   #265	-> 41
    //   #259	-> 76
    // Exception table:
    //   from	to	target	type
    //   2	9	76	finally
    //   9	17	76	finally
    //   27	32	76	finally
    //   32	37	76	finally
    //   41	76	76	finally
  }
  
  public MediaMetadataEditor putObject(int paramInt, Object paramObject) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mApplied : Z
    //   6: ifeq -> 21
    //   9: ldc 'MediaMetadataEditor'
    //   11: ldc 'Can't edit a previously applied MediaMetadataEditor'
    //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: aload_0
    //   20: areturn
    //   21: getstatic android/media/MediaMetadataEditor.METADATA_KEYS_TYPE : Landroid/util/SparseIntArray;
    //   24: iload_1
    //   25: iconst_m1
    //   26: invokevirtual get : (II)I
    //   29: istore_3
    //   30: iload_3
    //   31: ifeq -> 247
    //   34: iload_3
    //   35: iconst_1
    //   36: if_icmpeq -> 181
    //   39: iload_3
    //   40: iconst_2
    //   41: if_icmpeq -> 113
    //   44: iload_3
    //   45: iconst_3
    //   46: if_icmpne -> 73
    //   49: aload_0
    //   50: getfield mEditorMetadata : Landroid/os/Bundle;
    //   53: iload_1
    //   54: invokestatic valueOf : (I)Ljava/lang/String;
    //   57: aload_2
    //   58: checkcast android/os/Parcelable
    //   61: invokevirtual putParcelable : (Ljava/lang/String;Landroid/os/Parcelable;)V
    //   64: aload_0
    //   65: iconst_1
    //   66: putfield mMetadataChanged : Z
    //   69: aload_0
    //   70: monitorexit
    //   71: aload_0
    //   72: areturn
    //   73: new java/lang/IllegalArgumentException
    //   76: astore_2
    //   77: new java/lang/StringBuilder
    //   80: astore #4
    //   82: aload #4
    //   84: invokespecial <init> : ()V
    //   87: aload #4
    //   89: ldc 'Invalid key '
    //   91: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   94: pop
    //   95: aload #4
    //   97: iload_1
    //   98: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   101: pop
    //   102: aload_2
    //   103: aload #4
    //   105: invokevirtual toString : ()Ljava/lang/String;
    //   108: invokespecial <init> : (Ljava/lang/String;)V
    //   111: aload_2
    //   112: athrow
    //   113: aload_2
    //   114: ifnull -> 167
    //   117: aload_2
    //   118: instanceof android/graphics/Bitmap
    //   121: ifeq -> 127
    //   124: goto -> 167
    //   127: new java/lang/IllegalArgumentException
    //   130: astore_2
    //   131: new java/lang/StringBuilder
    //   134: astore #4
    //   136: aload #4
    //   138: invokespecial <init> : ()V
    //   141: aload #4
    //   143: ldc 'Not a Bitmap for key '
    //   145: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   148: pop
    //   149: aload #4
    //   151: iload_1
    //   152: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload_2
    //   157: aload #4
    //   159: invokevirtual toString : ()Ljava/lang/String;
    //   162: invokespecial <init> : (Ljava/lang/String;)V
    //   165: aload_2
    //   166: athrow
    //   167: aload_0
    //   168: iload_1
    //   169: aload_2
    //   170: checkcast android/graphics/Bitmap
    //   173: invokevirtual putBitmap : (ILandroid/graphics/Bitmap;)Landroid/media/MediaMetadataEditor;
    //   176: astore_2
    //   177: aload_0
    //   178: monitorexit
    //   179: aload_2
    //   180: areturn
    //   181: aload_2
    //   182: ifnull -> 233
    //   185: aload_2
    //   186: instanceof java/lang/String
    //   189: ifeq -> 195
    //   192: goto -> 233
    //   195: new java/lang/IllegalArgumentException
    //   198: astore #4
    //   200: new java/lang/StringBuilder
    //   203: astore_2
    //   204: aload_2
    //   205: invokespecial <init> : ()V
    //   208: aload_2
    //   209: ldc 'Not a String for key '
    //   211: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: pop
    //   215: aload_2
    //   216: iload_1
    //   217: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   220: pop
    //   221: aload #4
    //   223: aload_2
    //   224: invokevirtual toString : ()Ljava/lang/String;
    //   227: invokespecial <init> : (Ljava/lang/String;)V
    //   230: aload #4
    //   232: athrow
    //   233: aload_0
    //   234: iload_1
    //   235: aload_2
    //   236: checkcast java/lang/String
    //   239: invokevirtual putString : (ILjava/lang/String;)Landroid/media/MediaMetadataEditor;
    //   242: astore_2
    //   243: aload_0
    //   244: monitorexit
    //   245: aload_2
    //   246: areturn
    //   247: aload_2
    //   248: instanceof java/lang/Long
    //   251: ifeq -> 271
    //   254: aload_0
    //   255: iload_1
    //   256: aload_2
    //   257: checkcast java/lang/Long
    //   260: invokevirtual longValue : ()J
    //   263: invokevirtual putLong : (IJ)Landroid/media/MediaMetadataEditor;
    //   266: astore_2
    //   267: aload_0
    //   268: monitorexit
    //   269: aload_2
    //   270: areturn
    //   271: new java/lang/IllegalArgumentException
    //   274: astore_2
    //   275: new java/lang/StringBuilder
    //   278: astore #4
    //   280: aload #4
    //   282: invokespecial <init> : ()V
    //   285: aload #4
    //   287: ldc 'Not a non-null Long for key '
    //   289: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   292: pop
    //   293: aload #4
    //   295: iload_1
    //   296: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   299: pop
    //   300: aload_2
    //   301: aload #4
    //   303: invokevirtual toString : ()Ljava/lang/String;
    //   306: invokespecial <init> : (Ljava/lang/String;)V
    //   309: aload_2
    //   310: athrow
    //   311: astore_2
    //   312: aload_0
    //   313: monitorexit
    //   314: aload_2
    //   315: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #291	-> 2
    //   #292	-> 9
    //   #293	-> 17
    //   #295	-> 21
    //   #309	-> 49
    //   #310	-> 64
    //   #311	-> 69
    //   #321	-> 69
    //   #319	-> 73
    //   #313	-> 113
    //   #316	-> 127
    //   #314	-> 167
    //   #303	-> 181
    //   #306	-> 195
    //   #304	-> 233
    //   #297	-> 247
    //   #298	-> 254
    //   #300	-> 271
    //   #290	-> 311
    // Exception table:
    //   from	to	target	type
    //   2	9	311	finally
    //   9	17	311	finally
    //   21	30	311	finally
    //   49	64	311	finally
    //   64	69	311	finally
    //   73	113	311	finally
    //   117	124	311	finally
    //   127	167	311	finally
    //   167	177	311	finally
    //   185	192	311	finally
    //   195	233	311	finally
    //   233	243	311	finally
    //   247	254	311	finally
    //   254	267	311	finally
    //   271	311	311	finally
  }
  
  public long getLong(int paramInt, long paramLong) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/media/MediaMetadataEditor.METADATA_KEYS_TYPE : Landroid/util/SparseIntArray;
    //   5: iload_1
    //   6: iconst_m1
    //   7: invokevirtual get : (II)I
    //   10: ifne -> 30
    //   13: aload_0
    //   14: getfield mEditorMetadata : Landroid/os/Bundle;
    //   17: iload_1
    //   18: invokestatic valueOf : (I)Ljava/lang/String;
    //   21: lload_2
    //   22: invokevirtual getLong : (Ljava/lang/String;J)J
    //   25: lstore_2
    //   26: aload_0
    //   27: monitorexit
    //   28: lload_2
    //   29: lreturn
    //   30: new java/lang/IllegalArgumentException
    //   33: astore #4
    //   35: new java/lang/StringBuilder
    //   38: astore #5
    //   40: aload #5
    //   42: invokespecial <init> : ()V
    //   45: aload #5
    //   47: ldc 'Invalid type 'long' for key '
    //   49: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   52: pop
    //   53: aload #5
    //   55: iload_1
    //   56: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload #4
    //   62: aload #5
    //   64: invokevirtual toString : ()Ljava/lang/String;
    //   67: invokespecial <init> : (Ljava/lang/String;)V
    //   70: aload #4
    //   72: athrow
    //   73: astore #4
    //   75: aload_0
    //   76: monitorexit
    //   77: aload #4
    //   79: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #334	-> 2
    //   #337	-> 13
    //   #335	-> 30
    //   #333	-> 73
    // Exception table:
    //   from	to	target	type
    //   2	13	73	finally
    //   13	26	73	finally
    //   30	73	73	finally
  }
  
  public String getString(int paramInt, String paramString) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/media/MediaMetadataEditor.METADATA_KEYS_TYPE : Landroid/util/SparseIntArray;
    //   5: iload_1
    //   6: iconst_m1
    //   7: invokevirtual get : (II)I
    //   10: iconst_1
    //   11: if_icmpne -> 31
    //   14: aload_0
    //   15: getfield mEditorMetadata : Landroid/os/Bundle;
    //   18: iload_1
    //   19: invokestatic valueOf : (I)Ljava/lang/String;
    //   22: aload_2
    //   23: invokevirtual getString : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   26: astore_2
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_2
    //   30: areturn
    //   31: new java/lang/IllegalArgumentException
    //   34: astore_2
    //   35: new java/lang/StringBuilder
    //   38: astore_3
    //   39: aload_3
    //   40: invokespecial <init> : ()V
    //   43: aload_3
    //   44: ldc 'Invalid type 'String' for key '
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_3
    //   51: iload_1
    //   52: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload_2
    //   57: aload_3
    //   58: invokevirtual toString : ()Ljava/lang/String;
    //   61: invokespecial <init> : (Ljava/lang/String;)V
    //   64: aload_2
    //   65: athrow
    //   66: astore_2
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_2
    //   70: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #350	-> 2
    //   #353	-> 14
    //   #351	-> 31
    //   #349	-> 66
    // Exception table:
    //   from	to	target	type
    //   2	14	66	finally
    //   14	27	66	finally
    //   31	66	66	finally
  }
  
  public Bitmap getBitmap(int paramInt, Bitmap paramBitmap) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: bipush #100
    //   5: if_icmpne -> 31
    //   8: aload_0
    //   9: getfield mEditorArtwork : Landroid/graphics/Bitmap;
    //   12: ifnull -> 23
    //   15: aload_0
    //   16: getfield mEditorArtwork : Landroid/graphics/Bitmap;
    //   19: astore_2
    //   20: goto -> 23
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_2
    //   26: areturn
    //   27: astore_2
    //   28: goto -> 66
    //   31: new java/lang/IllegalArgumentException
    //   34: astore_3
    //   35: new java/lang/StringBuilder
    //   38: astore_2
    //   39: aload_2
    //   40: invokespecial <init> : ()V
    //   43: aload_2
    //   44: ldc 'Invalid type 'Bitmap' for key '
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_2
    //   51: iload_1
    //   52: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload_3
    //   57: aload_2
    //   58: invokevirtual toString : ()Ljava/lang/String;
    //   61: invokespecial <init> : (Ljava/lang/String;)V
    //   64: aload_3
    //   65: athrow
    //   66: aload_0
    //   67: monitorexit
    //   68: aload_2
    //   69: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #366	-> 2
    //   #369	-> 8
    //   #365	-> 27
    //   #367	-> 31
    //   #365	-> 66
    // Exception table:
    //   from	to	target	type
    //   8	20	27	finally
    //   31	66	27	finally
  }
  
  public Object getObject(int paramInt, Object paramObject) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/media/MediaMetadataEditor.METADATA_KEYS_TYPE : Landroid/util/SparseIntArray;
    //   5: iload_1
    //   6: iconst_m1
    //   7: invokevirtual get : (II)I
    //   10: istore_3
    //   11: iload_3
    //   12: ifeq -> 161
    //   15: iload_3
    //   16: iconst_1
    //   17: if_icmpeq -> 127
    //   20: iload_3
    //   21: iconst_2
    //   22: if_icmpeq -> 64
    //   25: iload_3
    //   26: iconst_3
    //   27: if_icmpne -> 89
    //   30: aload_0
    //   31: getfield mEditorMetadata : Landroid/os/Bundle;
    //   34: iload_1
    //   35: invokestatic valueOf : (I)Ljava/lang/String;
    //   38: invokevirtual containsKey : (Ljava/lang/String;)Z
    //   41: ifeq -> 60
    //   44: aload_0
    //   45: getfield mEditorMetadata : Landroid/os/Bundle;
    //   48: iload_1
    //   49: invokestatic valueOf : (I)Ljava/lang/String;
    //   52: invokevirtual getParcelable : (Ljava/lang/String;)Landroid/os/Parcelable;
    //   55: astore_2
    //   56: aload_0
    //   57: monitorexit
    //   58: aload_2
    //   59: areturn
    //   60: aload_0
    //   61: monitorexit
    //   62: aload_2
    //   63: areturn
    //   64: iload_1
    //   65: bipush #100
    //   67: if_icmpne -> 89
    //   70: aload_0
    //   71: getfield mEditorArtwork : Landroid/graphics/Bitmap;
    //   74: ifnull -> 85
    //   77: aload_0
    //   78: getfield mEditorArtwork : Landroid/graphics/Bitmap;
    //   81: astore_2
    //   82: goto -> 85
    //   85: aload_0
    //   86: monitorexit
    //   87: aload_2
    //   88: areturn
    //   89: new java/lang/IllegalArgumentException
    //   92: astore #4
    //   94: new java/lang/StringBuilder
    //   97: astore_2
    //   98: aload_2
    //   99: invokespecial <init> : ()V
    //   102: aload_2
    //   103: ldc 'Invalid key '
    //   105: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: aload_2
    //   110: iload_1
    //   111: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: aload #4
    //   117: aload_2
    //   118: invokevirtual toString : ()Ljava/lang/String;
    //   121: invokespecial <init> : (Ljava/lang/String;)V
    //   124: aload #4
    //   126: athrow
    //   127: aload_0
    //   128: getfield mEditorMetadata : Landroid/os/Bundle;
    //   131: iload_1
    //   132: invokestatic valueOf : (I)Ljava/lang/String;
    //   135: invokevirtual containsKey : (Ljava/lang/String;)Z
    //   138: ifeq -> 157
    //   141: aload_0
    //   142: getfield mEditorMetadata : Landroid/os/Bundle;
    //   145: iload_1
    //   146: invokestatic valueOf : (I)Ljava/lang/String;
    //   149: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
    //   152: astore_2
    //   153: aload_0
    //   154: monitorexit
    //   155: aload_2
    //   156: areturn
    //   157: aload_0
    //   158: monitorexit
    //   159: aload_2
    //   160: areturn
    //   161: aload_0
    //   162: getfield mEditorMetadata : Landroid/os/Bundle;
    //   165: iload_1
    //   166: invokestatic valueOf : (I)Ljava/lang/String;
    //   169: invokevirtual containsKey : (Ljava/lang/String;)Z
    //   172: ifeq -> 196
    //   175: aload_0
    //   176: getfield mEditorMetadata : Landroid/os/Bundle;
    //   179: iload_1
    //   180: invokestatic valueOf : (I)Ljava/lang/String;
    //   183: invokevirtual getLong : (Ljava/lang/String;)J
    //   186: lstore #5
    //   188: aload_0
    //   189: monitorexit
    //   190: lload #5
    //   192: invokestatic valueOf : (J)Ljava/lang/Long;
    //   195: areturn
    //   196: aload_0
    //   197: monitorexit
    //   198: aload_2
    //   199: areturn
    //   200: astore_2
    //   201: aload_0
    //   202: monitorexit
    //   203: aload_2
    //   204: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #383	-> 2
    //   #397	-> 30
    //   #398	-> 44
    //   #400	-> 60
    //   #404	-> 64
    //   #405	-> 70
    //   #408	-> 89
    //   #391	-> 127
    //   #392	-> 141
    //   #394	-> 157
    //   #385	-> 161
    //   #386	-> 175
    //   #388	-> 196
    //   #382	-> 200
    // Exception table:
    //   from	to	target	type
    //   2	11	200	finally
    //   30	44	200	finally
    //   44	56	200	finally
    //   70	82	200	finally
    //   89	127	200	finally
    //   127	141	200	finally
    //   141	153	200	finally
    //   161	175	200	finally
    //   175	188	200	finally
  }
  
  static {
    SparseIntArray sparseIntArray = new SparseIntArray(17);
    sparseIntArray.put(0, 0);
    METADATA_KEYS_TYPE.put(14, 0);
    METADATA_KEYS_TYPE.put(9, 0);
    METADATA_KEYS_TYPE.put(8, 0);
    METADATA_KEYS_TYPE.put(1, 1);
    METADATA_KEYS_TYPE.put(13, 1);
    METADATA_KEYS_TYPE.put(7, 1);
    METADATA_KEYS_TYPE.put(2, 1);
    METADATA_KEYS_TYPE.put(3, 1);
    METADATA_KEYS_TYPE.put(15, 1);
    METADATA_KEYS_TYPE.put(4, 1);
    METADATA_KEYS_TYPE.put(5, 1);
    METADATA_KEYS_TYPE.put(6, 1);
    METADATA_KEYS_TYPE.put(11, 1);
    METADATA_KEYS_TYPE.put(100, 2);
    METADATA_KEYS_TYPE.put(101, 3);
    METADATA_KEYS_TYPE.put(268435457, 3);
  }
  
  public abstract void apply();
}
