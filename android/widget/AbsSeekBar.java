package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.Insets;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbsSeekBar extends ProgressBar {
  private final Rect mTempRect = new Rect();
  
  private ColorStateList mThumbTintList = null;
  
  private BlendMode mThumbBlendMode = null;
  
  private boolean mHasThumbTint = false;
  
  private boolean mHasThumbBlendMode = false;
  
  private ColorStateList mTickMarkTintList = null;
  
  private BlendMode mTickMarkBlendMode = null;
  
  private boolean mHasTickMarkTint = false;
  
  private boolean mHasTickMarkBlendMode = false;
  
  boolean mIsUserSeekable = true;
  
  private int mKeyProgressIncrement = 1;
  
  private float mTouchThumbOffset = 0.0F;
  
  private List<Rect> mUserGestureExclusionRects = Collections.emptyList();
  
  private final List<Rect> mGestureExclusionRects = new ArrayList<>();
  
  private final Rect mThumbRect = new Rect();
  
  private static final int NO_ALPHA = 255;
  
  private float mDisabledAlpha;
  
  private boolean mIsDragging;
  
  private int mScaledTouchSlop;
  
  private boolean mSplitTrack;
  
  private Drawable mThumb;
  
  private int mThumbExclusionMaxSize;
  
  private int mThumbOffset;
  
  private Drawable mTickMark;
  
  private float mTouchDownX;
  
  float mTouchProgressOffset;
  
  public AbsSeekBar(Context paramContext) {
    super(paramContext);
  }
  
  public AbsSeekBar(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public AbsSeekBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AbsSeekBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SeekBar, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.SeekBar, paramAttributeSet, typedArray, paramInt1, paramInt2);
    Drawable drawable = typedArray.getDrawable(0);
    setThumb(drawable);
    if (typedArray.hasValue(4)) {
      this.mThumbBlendMode = Drawable.parseBlendMode(typedArray.getInt(4, -1), this.mThumbBlendMode);
      this.mHasThumbBlendMode = true;
    } 
    if (typedArray.hasValue(3)) {
      this.mThumbTintList = typedArray.getColorStateList(3);
      this.mHasThumbTint = true;
    } 
    drawable = typedArray.getDrawable(5);
    setTickMark(drawable);
    if (typedArray.hasValue(7)) {
      this.mTickMarkBlendMode = Drawable.parseBlendMode(typedArray.getInt(7, -1), this.mTickMarkBlendMode);
      this.mHasTickMarkBlendMode = true;
    } 
    if (typedArray.hasValue(6)) {
      this.mTickMarkTintList = typedArray.getColorStateList(6);
      this.mHasTickMarkTint = true;
    } 
    this.mSplitTrack = typedArray.getBoolean(2, false);
    paramInt1 = getThumbOffset();
    paramInt1 = typedArray.getDimensionPixelOffset(1, paramInt1);
    setThumbOffset(paramInt1);
    boolean bool = typedArray.getBoolean(8, true);
    typedArray.recycle();
    if (bool) {
      TypedArray typedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Theme, 0, 0);
      this.mDisabledAlpha = typedArray1.getFloat(3, 0.5F);
      typedArray1.recycle();
    } else {
      this.mDisabledAlpha = 1.0F;
    } 
    applyThumbTint();
    applyTickMarkTint();
    this.mScaledTouchSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.mThumbExclusionMaxSize = getResources().getDimensionPixelSize(17105469);
  }
  
  public void setThumb(Drawable paramDrawable) {
    boolean bool;
    Drawable drawable = this.mThumb;
    if (drawable != null && paramDrawable != drawable) {
      drawable.setCallback(null);
      bool = true;
    } else {
      bool = false;
    } 
    if (paramDrawable != null) {
      paramDrawable.setCallback(this);
      if (canResolveLayoutDirection())
        paramDrawable.setLayoutDirection(getLayoutDirection()); 
      this.mThumbOffset = paramDrawable.getIntrinsicWidth() / 2;
      if (bool && (
        paramDrawable.getIntrinsicWidth() != this.mThumb.getIntrinsicWidth() || 
        paramDrawable.getIntrinsicHeight() != this.mThumb.getIntrinsicHeight()))
        requestLayout(); 
    } 
    this.mThumb = paramDrawable;
    applyThumbTint();
    invalidate();
    if (bool) {
      updateThumbAndTrackPos(getWidth(), getHeight());
      if (paramDrawable != null && paramDrawable.isStateful()) {
        int[] arrayOfInt = getDrawableState();
        paramDrawable.setState(arrayOfInt);
      } 
    } 
  }
  
  public Drawable getThumb() {
    return this.mThumb;
  }
  
  public void setThumbTintList(ColorStateList paramColorStateList) {
    this.mThumbTintList = paramColorStateList;
    this.mHasThumbTint = true;
    applyThumbTint();
  }
  
  public ColorStateList getThumbTintList() {
    return this.mThumbTintList;
  }
  
  public void setThumbTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setThumbTintBlendMode((BlendMode)paramMode);
  }
  
  public void setThumbTintBlendMode(BlendMode paramBlendMode) {
    this.mThumbBlendMode = paramBlendMode;
    this.mHasThumbBlendMode = true;
    applyThumbTint();
  }
  
  public PorterDuff.Mode getThumbTintMode() {
    BlendMode blendMode = this.mThumbBlendMode;
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getThumbTintBlendMode() {
    return this.mThumbBlendMode;
  }
  
  private void applyThumbTint() {
    if (this.mThumb != null && (this.mHasThumbTint || this.mHasThumbBlendMode)) {
      Drawable drawable = this.mThumb.mutate();
      if (this.mHasThumbTint)
        drawable.setTintList(this.mThumbTintList); 
      if (this.mHasThumbBlendMode)
        this.mThumb.setTintBlendMode(this.mThumbBlendMode); 
      if (this.mThumb.isStateful())
        this.mThumb.setState(getDrawableState()); 
    } 
  }
  
  public int getThumbOffset() {
    return this.mThumbOffset;
  }
  
  public void setThumbOffset(int paramInt) {
    this.mThumbOffset = paramInt;
    invalidate();
  }
  
  public void setSplitTrack(boolean paramBoolean) {
    this.mSplitTrack = paramBoolean;
    invalidate();
  }
  
  public boolean getSplitTrack() {
    return this.mSplitTrack;
  }
  
  public void setTickMark(Drawable paramDrawable) {
    Drawable drawable = this.mTickMark;
    if (drawable != null)
      drawable.setCallback(null); 
    this.mTickMark = paramDrawable;
    if (paramDrawable != null) {
      paramDrawable.setCallback(this);
      paramDrawable.setLayoutDirection(getLayoutDirection());
      if (paramDrawable.isStateful())
        paramDrawable.setState(getDrawableState()); 
      applyTickMarkTint();
    } 
    invalidate();
  }
  
  public Drawable getTickMark() {
    return this.mTickMark;
  }
  
  public void setTickMarkTintList(ColorStateList paramColorStateList) {
    this.mTickMarkTintList = paramColorStateList;
    this.mHasTickMarkTint = true;
    applyTickMarkTint();
  }
  
  public ColorStateList getTickMarkTintList() {
    return this.mTickMarkTintList;
  }
  
  public void setTickMarkTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setTickMarkTintBlendMode((BlendMode)paramMode);
  }
  
  public void setTickMarkTintBlendMode(BlendMode paramBlendMode) {
    this.mTickMarkBlendMode = paramBlendMode;
    this.mHasTickMarkBlendMode = true;
    applyTickMarkTint();
  }
  
  public PorterDuff.Mode getTickMarkTintMode() {
    BlendMode blendMode = this.mTickMarkBlendMode;
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getTickMarkTintBlendMode() {
    return this.mTickMarkBlendMode;
  }
  
  private void applyTickMarkTint() {
    if (this.mTickMark != null && (this.mHasTickMarkTint || this.mHasTickMarkBlendMode)) {
      Drawable drawable = this.mTickMark.mutate();
      if (this.mHasTickMarkTint)
        drawable.setTintList(this.mTickMarkTintList); 
      if (this.mHasTickMarkBlendMode)
        this.mTickMark.setTintBlendMode(this.mTickMarkBlendMode); 
      if (this.mTickMark.isStateful())
        this.mTickMark.setState(getDrawableState()); 
    } 
  }
  
  public void setKeyProgressIncrement(int paramInt) {
    if (paramInt < 0)
      paramInt = -paramInt; 
    this.mKeyProgressIncrement = paramInt;
  }
  
  public int getKeyProgressIncrement() {
    return this.mKeyProgressIncrement;
  }
  
  public void setMin(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: invokespecial setMin : (I)V
    //   7: aload_0
    //   8: invokevirtual getMax : ()I
    //   11: aload_0
    //   12: invokevirtual getMin : ()I
    //   15: isub
    //   16: istore_1
    //   17: aload_0
    //   18: getfield mKeyProgressIncrement : I
    //   21: ifeq -> 35
    //   24: iload_1
    //   25: aload_0
    //   26: getfield mKeyProgressIncrement : I
    //   29: idiv
    //   30: bipush #20
    //   32: if_icmple -> 52
    //   35: aload_0
    //   36: iconst_1
    //   37: iload_1
    //   38: i2f
    //   39: ldc_w 20.0
    //   42: fdiv
    //   43: invokestatic round : (F)I
    //   46: invokestatic max : (II)I
    //   49: invokevirtual setKeyProgressIncrement : (I)V
    //   52: aload_0
    //   53: monitorexit
    //   54: return
    //   55: astore_2
    //   56: aload_0
    //   57: monitorexit
    //   58: aload_2
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #572	-> 2
    //   #573	-> 7
    //   #575	-> 17
    //   #579	-> 35
    //   #581	-> 52
    //   #571	-> 55
    // Exception table:
    //   from	to	target	type
    //   2	7	55	finally
    //   7	17	55	finally
    //   17	35	55	finally
    //   35	52	55	finally
  }
  
  public void setMax(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: invokespecial setMax : (I)V
    //   7: aload_0
    //   8: invokevirtual getMax : ()I
    //   11: aload_0
    //   12: invokevirtual getMin : ()I
    //   15: isub
    //   16: istore_1
    //   17: aload_0
    //   18: getfield mKeyProgressIncrement : I
    //   21: ifeq -> 35
    //   24: iload_1
    //   25: aload_0
    //   26: getfield mKeyProgressIncrement : I
    //   29: idiv
    //   30: bipush #20
    //   32: if_icmple -> 52
    //   35: aload_0
    //   36: iconst_1
    //   37: iload_1
    //   38: i2f
    //   39: ldc_w 20.0
    //   42: fdiv
    //   43: invokestatic round : (F)I
    //   46: invokestatic max : (II)I
    //   49: invokevirtual setKeyProgressIncrement : (I)V
    //   52: aload_0
    //   53: monitorexit
    //   54: return
    //   55: astore_2
    //   56: aload_0
    //   57: monitorexit
    //   58: aload_2
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #585	-> 2
    //   #586	-> 7
    //   #588	-> 17
    //   #591	-> 35
    //   #593	-> 52
    //   #584	-> 55
    // Exception table:
    //   from	to	target	type
    //   2	7	55	finally
    //   7	17	55	finally
    //   17	35	55	finally
    //   35	52	55	finally
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (paramDrawable == this.mThumb || paramDrawable == this.mTickMark || super.verifyDrawable(paramDrawable));
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mThumb;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
    drawable = this.mTickMark;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = getProgressDrawable();
    if (drawable != null && this.mDisabledAlpha < 1.0F) {
      int i;
      if (isEnabled()) {
        i = 255;
      } else {
        i = (int)(this.mDisabledAlpha * 255.0F);
      } 
      drawable.setAlpha(i);
    } 
    drawable = this.mThumb;
    if (drawable != null && drawable.isStateful() && 
      drawable.setState(getDrawableState()))
      invalidateDrawable(drawable); 
    drawable = this.mTickMark;
    if (drawable != null && drawable.isStateful() && 
      drawable.setState(getDrawableState()))
      invalidateDrawable(drawable); 
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Drawable drawable = this.mThumb;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  void onVisualProgressChanged(int paramInt, float paramFloat) {
    super.onVisualProgressChanged(paramInt, paramFloat);
    if (paramInt == 16908301) {
      Drawable drawable = this.mThumb;
      if (drawable != null) {
        setThumbPos(getWidth(), drawable, paramFloat, -2147483648);
        invalidate();
      } 
    } 
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    updateThumbAndTrackPos(paramInt1, paramInt2);
  }
  
  private void updateThumbAndTrackPos(int paramInt1, int paramInt2) {
    int i = paramInt2 - this.mPaddingTop - this.mPaddingBottom;
    Drawable drawable1 = getCurrentDrawable();
    Drawable drawable2 = this.mThumb;
    int j = Math.min(this.mMaxHeight, i);
    if (drawable2 == null) {
      paramInt2 = 0;
    } else {
      paramInt2 = drawable2.getIntrinsicHeight();
    } 
    if (paramInt2 > j) {
      int k = (i - paramInt2) / 2;
      i = (paramInt2 - j) / 2 + k;
      paramInt2 = k;
    } else {
      int k = (i - j) / 2;
      i = k;
      paramInt2 = (j - paramInt2) / 2 + k;
    } 
    if (drawable1 != null) {
      int k = this.mPaddingRight, m = this.mPaddingLeft;
      drawable1.setBounds(0, i, paramInt1 - k - m, i + j);
    } 
    if (drawable2 != null)
      setThumbPos(paramInt1, drawable2, getScale(), paramInt2); 
  }
  
  private float getScale() {
    float f;
    int i = getMin();
    int j = getMax();
    j -= i;
    if (j > 0) {
      f = (getProgress() - i) / j;
    } else {
      f = 0.0F;
    } 
    return f;
  }
  
  private void setThumbPos(int paramInt1, Drawable paramDrawable, float paramFloat, int paramInt2) {
    int i = this.mPaddingLeft, j = this.mPaddingRight;
    int k = paramDrawable.getIntrinsicWidth();
    int m = paramDrawable.getIntrinsicHeight();
    j = paramInt1 - i - j - k + this.mThumbOffset * 2;
    i = (int)(j * paramFloat + 0.5F);
    if (paramInt2 == Integer.MIN_VALUE) {
      Rect rect = paramDrawable.getBounds();
      paramInt2 = rect.top;
      paramInt1 = rect.bottom;
    } else {
      paramInt1 = paramInt2;
      m = paramInt2 + m;
      paramInt2 = paramInt1;
      paramInt1 = m;
    } 
    if (isLayoutRtl() && this.mMirrorForRtl)
      i = j - i; 
    m = i + k;
    Drawable drawable = getBackground();
    if (drawable != null) {
      k = this.mPaddingLeft - this.mThumbOffset;
      j = this.mPaddingTop;
      drawable.setHotspotBounds(i + k, paramInt2 + j, m + k, paramInt1 + j);
    } 
    paramDrawable.setBounds(i, paramInt2, m, paramInt1);
    updateGestureExclusionRects();
  }
  
  public void setSystemGestureExclusionRects(List<Rect> paramList) {
    Preconditions.checkNotNull(paramList, "rects must not be null");
    this.mUserGestureExclusionRects = paramList;
    updateGestureExclusionRects();
  }
  
  private void updateGestureExclusionRects() {
    Drawable drawable = this.mThumb;
    if (drawable == null) {
      super.setSystemGestureExclusionRects(this.mUserGestureExclusionRects);
      return;
    } 
    this.mGestureExclusionRects.clear();
    drawable.copyBounds(this.mThumbRect);
    this.mThumbRect.offset(this.mPaddingLeft - this.mThumbOffset, this.mPaddingTop);
    growRectTo(this.mThumbRect, Math.min(getHeight(), this.mThumbExclusionMaxSize));
    this.mGestureExclusionRects.add(this.mThumbRect);
    this.mGestureExclusionRects.addAll(this.mUserGestureExclusionRects);
    super.setSystemGestureExclusionRects(this.mGestureExclusionRects);
  }
  
  private void growRectTo(Rect paramRect, int paramInt) {
    int i = (paramInt - paramRect.height()) / 2;
    if (i > 0) {
      paramRect.top -= i;
      paramRect.bottom += i;
    } 
    paramInt = (paramInt - paramRect.width()) / 2;
    if (paramInt > 0) {
      paramRect.left -= paramInt;
      paramRect.right += paramInt;
    } 
  }
  
  public void onResolveDrawables(int paramInt) {
    super.onResolveDrawables(paramInt);
    Drawable drawable = this.mThumb;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
  }
  
  protected void onDraw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial onDraw : (Landroid/graphics/Canvas;)V
    //   7: aload_0
    //   8: aload_1
    //   9: invokevirtual drawThumb : (Landroid/graphics/Canvas;)V
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #806	-> 2
    //   #807	-> 7
    //   #808	-> 12
    //   #805	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
  }
  
  void drawTrack(Canvas paramCanvas) {
    Drawable drawable = this.mThumb;
    if (drawable != null && this.mSplitTrack) {
      Insets insets = drawable.getOpticalInsets();
      Rect rect = this.mTempRect;
      drawable.copyBounds(rect);
      rect.offset(this.mPaddingLeft - this.mThumbOffset, this.mPaddingTop);
      rect.left += insets.left;
      rect.right -= insets.right;
      int i = paramCanvas.save();
      paramCanvas.clipRect(rect, Region.Op.DIFFERENCE);
      super.drawTrack(paramCanvas);
      drawTickMarks(paramCanvas);
      paramCanvas.restoreToCount(i);
    } else {
      super.drawTrack(paramCanvas);
      drawTickMarks(paramCanvas);
    } 
  }
  
  protected void drawTickMarks(Canvas paramCanvas) {
    if (this.mTickMark != null) {
      int i = getMax() - getMin();
      int j = 1;
      if (i > 1) {
        int k = this.mTickMark.getIntrinsicWidth();
        int m = this.mTickMark.getIntrinsicHeight();
        if (k >= 0) {
          k /= 2;
        } else {
          k = 1;
        } 
        if (m >= 0)
          j = m / 2; 
        this.mTickMark.setBounds(-k, -j, k, j);
        float f = (getWidth() - this.mPaddingLeft - this.mPaddingRight) / i;
        j = paramCanvas.save();
        paramCanvas.translate(this.mPaddingLeft, (getHeight() / 2));
        for (k = 0; k <= i; k++) {
          this.mTickMark.draw(paramCanvas);
          paramCanvas.translate(f, 0.0F);
        } 
        paramCanvas.restoreToCount(j);
      } 
    } 
  }
  
  void drawThumb(Canvas paramCanvas) {
    if (this.mThumb != null) {
      int i = paramCanvas.save();
      paramCanvas.translate((this.mPaddingLeft - this.mThumbOffset), this.mPaddingTop);
      this.mThumb.draw(paramCanvas);
      paramCanvas.restoreToCount(i);
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual getCurrentDrawable : ()Landroid/graphics/drawable/Drawable;
    //   6: astore_3
    //   7: aload_0
    //   8: getfield mThumb : Landroid/graphics/drawable/Drawable;
    //   11: ifnonnull -> 20
    //   14: iconst_0
    //   15: istore #4
    //   17: goto -> 29
    //   20: aload_0
    //   21: getfield mThumb : Landroid/graphics/drawable/Drawable;
    //   24: invokevirtual getIntrinsicHeight : ()I
    //   27: istore #4
    //   29: iconst_0
    //   30: istore #5
    //   32: iconst_0
    //   33: istore #6
    //   35: aload_3
    //   36: ifnull -> 88
    //   39: aload_0
    //   40: getfield mMinWidth : I
    //   43: aload_0
    //   44: getfield mMaxWidth : I
    //   47: aload_3
    //   48: invokevirtual getIntrinsicWidth : ()I
    //   51: invokestatic min : (II)I
    //   54: invokestatic max : (II)I
    //   57: istore #5
    //   59: aload_0
    //   60: getfield mMinHeight : I
    //   63: aload_0
    //   64: getfield mMaxHeight : I
    //   67: aload_3
    //   68: invokevirtual getIntrinsicHeight : ()I
    //   71: invokestatic min : (II)I
    //   74: invokestatic max : (II)I
    //   77: istore #6
    //   79: iload #4
    //   81: iload #6
    //   83: invokestatic max : (II)I
    //   86: istore #6
    //   88: aload_0
    //   89: getfield mPaddingLeft : I
    //   92: istore #7
    //   94: aload_0
    //   95: getfield mPaddingRight : I
    //   98: istore #8
    //   100: aload_0
    //   101: getfield mPaddingTop : I
    //   104: istore #4
    //   106: aload_0
    //   107: getfield mPaddingBottom : I
    //   110: istore #9
    //   112: iload #5
    //   114: iload #7
    //   116: iload #8
    //   118: iadd
    //   119: iadd
    //   120: iload_1
    //   121: iconst_0
    //   122: invokestatic resolveSizeAndState : (III)I
    //   125: istore_1
    //   126: iload #6
    //   128: iload #4
    //   130: iload #9
    //   132: iadd
    //   133: iadd
    //   134: iload_2
    //   135: iconst_0
    //   136: invokestatic resolveSizeAndState : (III)I
    //   139: istore_2
    //   140: aload_0
    //   141: iload_1
    //   142: iload_2
    //   143: invokevirtual setMeasuredDimension : (II)V
    //   146: aload_0
    //   147: monitorexit
    //   148: return
    //   149: astore_3
    //   150: aload_0
    //   151: monitorexit
    //   152: aload_3
    //   153: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #874	-> 2
    //   #876	-> 7
    //   #877	-> 29
    //   #878	-> 32
    //   #879	-> 35
    //   #880	-> 39
    //   #881	-> 59
    //   #882	-> 79
    //   #884	-> 88
    //   #885	-> 100
    //   #887	-> 112
    //   #888	-> 126
    //   #887	-> 140
    //   #889	-> 146
    //   #873	-> 149
    // Exception table:
    //   from	to	target	type
    //   2	7	149	finally
    //   7	14	149	finally
    //   20	29	149	finally
    //   39	59	149	finally
    //   59	79	149	finally
    //   79	88	149	finally
    //   88	100	149	finally
    //   100	112	149	finally
    //   112	126	149	finally
    //   126	140	149	finally
    //   140	146	149	finally
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (!this.mIsUserSeekable || !isEnabled())
      return false; 
    int i = paramMotionEvent.getAction();
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i == 3) {
            if (this.mIsDragging) {
              onStopTrackingTouch();
              setPressed(false);
            } 
            invalidate();
          } 
        } else if (this.mIsDragging) {
          trackTouchEvent(paramMotionEvent);
        } else {
          float f = paramMotionEvent.getX();
          if (Math.abs(f - this.mTouchDownX) > this.mScaledTouchSlop)
            startDrag(paramMotionEvent); 
        } 
      } else {
        if (this.mIsDragging) {
          trackTouchEvent(paramMotionEvent);
          onStopTrackingTouch();
          setPressed(false);
        } else {
          onStartTrackingTouch();
          trackTouchEvent(paramMotionEvent);
          onStopTrackingTouch();
        } 
        invalidate();
      } 
    } else {
      if (this.mThumb != null) {
        i = getWidth() - this.mPaddingLeft - this.mPaddingRight;
        float f = (getProgress() - getMin());
        int j = getMax();
        this.mTouchThumbOffset = f = f / (j - getMin()) - (paramMotionEvent.getX() - this.mPaddingLeft) / i;
        if (Math.abs(f * i) > getThumbOffset())
          this.mTouchThumbOffset = 0.0F; 
      } 
      if (isInScrollingContainer()) {
        this.mTouchDownX = paramMotionEvent.getX();
      } else {
        startDrag(paramMotionEvent);
      } 
    } 
    return true;
  }
  
  private void startDrag(MotionEvent paramMotionEvent) {
    setPressed(true);
    Drawable drawable = this.mThumb;
    if (drawable != null)
      invalidate(drawable.getBounds()); 
    onStartTrackingTouch();
    trackTouchEvent(paramMotionEvent);
    attemptClaimDrag();
  }
  
  private void setHotspot(float paramFloat1, float paramFloat2) {
    Drawable drawable = getBackground();
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  private void trackTouchEvent(MotionEvent paramMotionEvent) {
    float f2;
    int i = Math.round(paramMotionEvent.getX());
    int j = Math.round(paramMotionEvent.getY());
    int k = getWidth();
    int m = k - this.mPaddingLeft - this.mPaddingRight;
    float f1 = 0.0F;
    if (isLayoutRtl() && this.mMirrorForRtl) {
      if (i > k - this.mPaddingRight) {
        f2 = 0.0F;
      } else if (i < this.mPaddingLeft) {
        f2 = 1.0F;
      } else {
        f2 = (m - i + this.mPaddingLeft) / m + this.mTouchThumbOffset;
        f1 = this.mTouchProgressOffset;
      } 
    } else if (i < this.mPaddingLeft) {
      f2 = 0.0F;
    } else if (i > k - this.mPaddingRight) {
      f2 = 1.0F;
    } else {
      f2 = (i - this.mPaddingLeft) / m + this.mTouchThumbOffset;
      f1 = this.mTouchProgressOffset;
    } 
    m = getMax();
    k = getMin();
    float f3 = (m - k), f4 = getMin();
    setHotspot(i, j);
    setProgressInternal(Math.round(f1 + f3 * f2 + f4), true, false);
  }
  
  private void attemptClaimDrag() {
    if (this.mParent != null)
      this.mParent.requestDisallowInterceptTouchEvent(true); 
  }
  
  void onStartTrackingTouch() {
    this.mIsDragging = true;
  }
  
  void onStopTrackingTouch() {
    this.mIsDragging = false;
  }
  
  void onKeyChange() {}
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isEnabled : ()Z
    //   4: ifeq -> 95
    //   7: aload_0
    //   8: getfield mKeyProgressIncrement : I
    //   11: istore_3
    //   12: iload_1
    //   13: bipush #21
    //   15: if_icmpeq -> 54
    //   18: iload_3
    //   19: istore #4
    //   21: iload_1
    //   22: bipush #22
    //   24: if_icmpeq -> 58
    //   27: iload_1
    //   28: bipush #69
    //   30: if_icmpeq -> 54
    //   33: iload_3
    //   34: istore #4
    //   36: iload_1
    //   37: bipush #70
    //   39: if_icmpeq -> 58
    //   42: iload_3
    //   43: istore #4
    //   45: iload_1
    //   46: bipush #81
    //   48: if_icmpeq -> 58
    //   51: goto -> 95
    //   54: iload_3
    //   55: ineg
    //   56: istore #4
    //   58: aload_0
    //   59: invokevirtual isLayoutRtl : ()Z
    //   62: ifeq -> 73
    //   65: iload #4
    //   67: ineg
    //   68: istore #4
    //   70: goto -> 73
    //   73: aload_0
    //   74: aload_0
    //   75: invokevirtual getProgress : ()I
    //   78: iload #4
    //   80: iadd
    //   81: iconst_1
    //   82: iconst_1
    //   83: invokevirtual setProgressInternal : (IZZ)Z
    //   86: ifeq -> 95
    //   89: aload_0
    //   90: invokevirtual onKeyChange : ()V
    //   93: iconst_1
    //   94: ireturn
    //   95: aload_0
    //   96: iload_1
    //   97: aload_2
    //   98: invokespecial onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   101: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1044	-> 0
    //   #1045	-> 7
    //   #1046	-> 12
    //   #1049	-> 54
    //   #1054	-> 58
    //   #1056	-> 73
    //   #1057	-> 89
    //   #1058	-> 93
    //   #1064	-> 95
  }
  
  public CharSequence getAccessibilityClassName() {
    return AbsSeekBar.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (isEnabled()) {
      int i = getProgress();
      if (i > getMin())
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD); 
      if (i < getMax())
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD); 
    } 
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    if (!isEnabled())
      return false; 
    if (paramInt != 4096 && paramInt != 8192) {
      if (paramInt != 16908349)
        return false; 
      if (!canUserSetProgress())
        return false; 
      if (paramBundle == null || !paramBundle.containsKey("android.view.accessibility.action.ARGUMENT_PROGRESS_VALUE"))
        return false; 
      float f = paramBundle.getFloat("android.view.accessibility.action.ARGUMENT_PROGRESS_VALUE");
      return setProgressInternal((int)f, true, true);
    } 
    if (!canUserSetProgress())
      return false; 
    int i = getMax(), j = getMin();
    j = Math.max(1, Math.round((i - j) / 20.0F));
    i = j;
    if (paramInt == 8192)
      i = -j; 
    if (setProgressInternal(getProgress() + i, true, true)) {
      onKeyChange();
      return true;
    } 
    return false;
  }
  
  boolean canUserSetProgress() {
    boolean bool;
    if (!isIndeterminate() && isEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    Drawable drawable = this.mThumb;
    if (drawable != null) {
      setThumbPos(getWidth(), drawable, getScale(), -2147483648);
      invalidate();
    } 
  }
}
