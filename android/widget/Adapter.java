package android.widget;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;

public interface Adapter {
  public static final int IGNORE_ITEM_VIEW_TYPE = -1;
  
  public static final int NO_SELECTION = -2147483648;
  
  default CharSequence[] getAutofillOptions() {
    return null;
  }
  
  int getCount();
  
  Object getItem(int paramInt);
  
  long getItemId(int paramInt);
  
  int getItemViewType(int paramInt);
  
  View getView(int paramInt, View paramView, ViewGroup paramViewGroup);
  
  int getViewTypeCount();
  
  boolean hasStableIds();
  
  boolean isEmpty();
  
  void registerDataSetObserver(DataSetObserver paramDataSetObserver);
  
  void unregisterDataSetObserver(DataSetObserver paramDataSetObserver);
}
