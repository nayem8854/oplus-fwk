package android.widget;

import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collections;

class ExpandableListConnector extends BaseAdapter implements Filterable {
  private int mTotalExpChildrenCount;
  
  private int mMaxExpGroupCount = Integer.MAX_VALUE;
  
  private ExpandableListAdapter mExpandableListAdapter;
  
  private ArrayList<GroupMetadata> mExpGroupMetadataList;
  
  private final DataSetObserver mDataSetObserver = new MyDataSetObserver();
  
  public ExpandableListConnector(ExpandableListAdapter paramExpandableListAdapter) {
    this.mExpGroupMetadataList = new ArrayList<>();
    setExpandableListAdapter(paramExpandableListAdapter);
  }
  
  public void setExpandableListAdapter(ExpandableListAdapter paramExpandableListAdapter) {
    ExpandableListAdapter expandableListAdapter = this.mExpandableListAdapter;
    if (expandableListAdapter != null)
      expandableListAdapter.unregisterDataSetObserver(this.mDataSetObserver); 
    this.mExpandableListAdapter = paramExpandableListAdapter;
    paramExpandableListAdapter.registerDataSetObserver(this.mDataSetObserver);
  }
  
  PositionMetadata getUnflattenedPos(int paramInt) {
    ArrayList<GroupMetadata> arrayList = this.mExpGroupMetadataList;
    int i = arrayList.size();
    int j = 0;
    int k = i - 1;
    int m = 0;
    if (i == 0)
      return PositionMetadata.obtain(paramInt, 2, paramInt, -1, null, 0); 
    while (j <= k) {
      i = (k - j) / 2 + j;
      GroupMetadata groupMetadata = arrayList.get(i);
      if (paramInt > groupMetadata.lastChildFlPos) {
        j = i + 1;
        m = i;
        continue;
      } 
      if (paramInt < groupMetadata.flPos) {
        k = i - 1;
        m = i;
        continue;
      } 
      if (paramInt == groupMetadata.flPos)
        return PositionMetadata.obtain(paramInt, 2, groupMetadata.gPos, -1, groupMetadata, i); 
      m = i;
      if (paramInt <= groupMetadata.lastChildFlPos) {
        j = groupMetadata.flPos;
        return PositionMetadata.obtain(paramInt, 1, groupMetadata.gPos, paramInt - j + 1, groupMetadata, i);
      } 
    } 
    if (j > m) {
      GroupMetadata groupMetadata = arrayList.get(j - 1);
      k = groupMetadata.lastChildFlPos;
      m = groupMetadata.gPos;
      k = paramInt - k + m;
    } else {
      if (k < m) {
        j = k + 1;
        GroupMetadata groupMetadata = arrayList.get(j);
        k = groupMetadata.gPos;
        m = groupMetadata.flPos;
        k -= m - paramInt;
        return PositionMetadata.obtain(paramInt, 2, k, -1, null, j);
      } 
      throw new RuntimeException("Unknown state");
    } 
    return PositionMetadata.obtain(paramInt, 2, k, -1, null, j);
  }
  
  PositionMetadata getFlattenedPos(ExpandableListPosition paramExpandableListPosition) {
    GroupMetadata groupMetadata;
    ArrayList<GroupMetadata> arrayList = this.mExpGroupMetadataList;
    int i = arrayList.size();
    int j = 0;
    int k = i - 1;
    int m = 0;
    if (i == 0)
      return PositionMetadata.obtain(paramExpandableListPosition.groupPos, paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, null, 0); 
    while (j <= k) {
      i = (k - j) / 2 + j;
      GroupMetadata groupMetadata1 = arrayList.get(i);
      if (paramExpandableListPosition.groupPos > groupMetadata1.gPos) {
        j = i + 1;
        m = i;
        continue;
      } 
      if (paramExpandableListPosition.groupPos < groupMetadata1.gPos) {
        k = i - 1;
        m = i;
        continue;
      } 
      m = i;
      if (paramExpandableListPosition.groupPos == groupMetadata1.gPos) {
        if (paramExpandableListPosition.type == 2)
          return PositionMetadata.obtain(groupMetadata1.flPos, paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, groupMetadata1, i); 
        if (paramExpandableListPosition.type == 1)
          return PositionMetadata.obtain(groupMetadata1.flPos + paramExpandableListPosition.childPos + 1, paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, groupMetadata1, i); 
        return null;
      } 
    } 
    if (paramExpandableListPosition.type != 2)
      return null; 
    if (j > m) {
      groupMetadata = arrayList.get(j - 1);
      m = groupMetadata.lastChildFlPos;
      i = paramExpandableListPosition.groupPos;
      k = groupMetadata.gPos;
      return PositionMetadata.obtain(m + i - k, paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, null, j);
    } 
    if (k < m) {
      groupMetadata = groupMetadata.get(++k);
      j = groupMetadata.flPos;
      i = groupMetadata.gPos;
      m = paramExpandableListPosition.groupPos;
      return PositionMetadata.obtain(j - i - m, paramExpandableListPosition.type, paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos, null, k);
    } 
    return null;
  }
  
  public boolean areAllItemsEnabled() {
    return this.mExpandableListAdapter.areAllItemsEnabled();
  }
  
  public boolean isEnabled(int paramInt) {
    boolean bool;
    PositionMetadata positionMetadata = getUnflattenedPos(paramInt);
    ExpandableListPosition expandableListPosition = positionMetadata.position;
    if (expandableListPosition.type == 1) {
      bool = this.mExpandableListAdapter.isChildSelectable(expandableListPosition.groupPos, expandableListPosition.childPos);
    } else {
      bool = true;
    } 
    positionMetadata.recycle();
    return bool;
  }
  
  public int getCount() {
    return this.mExpandableListAdapter.getGroupCount() + this.mTotalExpChildrenCount;
  }
  
  public Object getItem(int paramInt) {
    Object object;
    PositionMetadata positionMetadata = getUnflattenedPos(paramInt);
    if (positionMetadata.position.type == 2) {
      ExpandableListAdapter expandableListAdapter = this.mExpandableListAdapter;
      paramInt = positionMetadata.position.groupPos;
      object = expandableListAdapter.getGroup(paramInt);
    } else {
      if (positionMetadata.position.type == 1) {
        object = this.mExpandableListAdapter.getChild(positionMetadata.position.groupPos, positionMetadata.position.childPos);
        positionMetadata.recycle();
        return object;
      } 
      throw new RuntimeException("Flat list position is of unknown type");
    } 
    positionMetadata.recycle();
    return object;
  }
  
  public long getItemId(int paramInt) {
    PositionMetadata positionMetadata = getUnflattenedPos(paramInt);
    long l = this.mExpandableListAdapter.getGroupId(positionMetadata.position.groupPos);
    if (positionMetadata.position.type == 2) {
      l = this.mExpandableListAdapter.getCombinedGroupId(l);
    } else {
      if (positionMetadata.position.type == 1) {
        long l1 = this.mExpandableListAdapter.getChildId(positionMetadata.position.groupPos, positionMetadata.position.childPos);
        l = this.mExpandableListAdapter.getCombinedChildId(l, l1);
        positionMetadata.recycle();
        return l;
      } 
      throw new RuntimeException("Flat list position is of unknown type");
    } 
    positionMetadata.recycle();
    return l;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    PositionMetadata positionMetadata = getUnflattenedPos(paramInt);
    if (positionMetadata.position.type == 2) {
      ExpandableListAdapter expandableListAdapter = this.mExpandableListAdapter;
      paramInt = positionMetadata.position.groupPos;
      boolean bool = positionMetadata.isExpanded();
      paramView = expandableListAdapter.getGroupView(paramInt, bool, paramView, paramViewGroup);
    } else {
      int i = positionMetadata.position.type;
      boolean bool = true;
      if (i == 1) {
        if (positionMetadata.groupMetadata.lastChildFlPos != paramInt)
          bool = false; 
        paramView = this.mExpandableListAdapter.getChildView(positionMetadata.position.groupPos, positionMetadata.position.childPos, bool, paramView, paramViewGroup);
        positionMetadata.recycle();
        return paramView;
      } 
      throw new RuntimeException("Flat list position is of unknown type");
    } 
    positionMetadata.recycle();
    return paramView;
  }
  
  public int getItemViewType(int paramInt) {
    PositionMetadata positionMetadata = getUnflattenedPos(paramInt);
    ExpandableListPosition expandableListPosition = positionMetadata.position;
    ExpandableListAdapter expandableListAdapter = this.mExpandableListAdapter;
    if (expandableListAdapter instanceof HeterogeneousExpandableList) {
      HeterogeneousExpandableList heterogeneousExpandableList = (HeterogeneousExpandableList)expandableListAdapter;
      if (expandableListPosition.type == 2) {
        paramInt = heterogeneousExpandableList.getGroupType(expandableListPosition.groupPos);
      } else {
        paramInt = heterogeneousExpandableList.getChildType(expandableListPosition.groupPos, expandableListPosition.childPos);
        paramInt = heterogeneousExpandableList.getGroupTypeCount() + paramInt;
      } 
    } else if (expandableListPosition.type == 2) {
      paramInt = 0;
    } else {
      paramInt = 1;
    } 
    positionMetadata.recycle();
    return paramInt;
  }
  
  public int getViewTypeCount() {
    ExpandableListAdapter expandableListAdapter = this.mExpandableListAdapter;
    if (expandableListAdapter instanceof HeterogeneousExpandableList) {
      HeterogeneousExpandableList heterogeneousExpandableList = (HeterogeneousExpandableList)expandableListAdapter;
      return heterogeneousExpandableList.getGroupTypeCount() + heterogeneousExpandableList.getChildTypeCount();
    } 
    return 2;
  }
  
  public boolean hasStableIds() {
    return this.mExpandableListAdapter.hasStableIds();
  }
  
  private void refreshExpGroupMetadataList(boolean paramBoolean1, boolean paramBoolean2) {
    ArrayList<GroupMetadata> arrayList = this.mExpGroupMetadataList;
    int i = arrayList.size();
    byte b = 0;
    this.mTotalExpChildrenCount = 0;
    int j = i;
    if (paramBoolean2) {
      boolean bool = false;
      for (int n = i - 1; n >= 0; n--, i = i2, bool = bool1) {
        GroupMetadata groupMetadata = arrayList.get(n);
        int i1 = findGroupPosition(groupMetadata.gId, groupMetadata.gPos);
        int i2 = i;
        boolean bool1 = bool;
        if (i1 != groupMetadata.gPos) {
          j = i;
          if (i1 == -1) {
            arrayList.remove(n);
            j = i - 1;
          } 
          groupMetadata.gPos = i1;
          i2 = j;
          bool1 = bool;
          if (!bool) {
            bool1 = true;
            i2 = j;
          } 
        } 
      } 
      j = i;
      if (bool) {
        Collections.sort(arrayList);
        j = i;
      } 
    } 
    int k = 0;
    int m;
    for (i = 0, m = b; i < j; i++) {
      int n;
      GroupMetadata groupMetadata = arrayList.get(i);
      if (groupMetadata.lastChildFlPos == -1 || paramBoolean1) {
        n = this.mExpandableListAdapter.getChildrenCount(groupMetadata.gPos);
      } else {
        n = groupMetadata.lastChildFlPos - groupMetadata.flPos;
      } 
      this.mTotalExpChildrenCount += n;
      m += groupMetadata.gPos - k;
      k = groupMetadata.gPos;
      groupMetadata.flPos = m;
      m += n;
      groupMetadata.lastChildFlPos = m;
    } 
  }
  
  boolean collapseGroup(int paramInt) {
    ExpandableListPosition expandableListPosition = ExpandableListPosition.obtain(2, paramInt, -1, -1);
    PositionMetadata positionMetadata = getFlattenedPos(expandableListPosition);
    expandableListPosition.recycle();
    if (positionMetadata == null)
      return false; 
    boolean bool = collapseGroup(positionMetadata);
    positionMetadata.recycle();
    return bool;
  }
  
  boolean collapseGroup(PositionMetadata paramPositionMetadata) {
    if (paramPositionMetadata.groupMetadata == null)
      return false; 
    this.mExpGroupMetadataList.remove(paramPositionMetadata.groupMetadata);
    refreshExpGroupMetadataList(false, false);
    notifyDataSetChanged();
    this.mExpandableListAdapter.onGroupCollapsed(paramPositionMetadata.groupMetadata.gPos);
    return true;
  }
  
  boolean expandGroup(int paramInt) {
    ExpandableListPosition expandableListPosition = ExpandableListPosition.obtain(2, paramInt, -1, -1);
    PositionMetadata positionMetadata = getFlattenedPos(expandableListPosition);
    expandableListPosition.recycle();
    boolean bool = expandGroup(positionMetadata);
    positionMetadata.recycle();
    return bool;
  }
  
  boolean expandGroup(PositionMetadata paramPositionMetadata) {
    if (paramPositionMetadata.position.groupPos >= 0) {
      if (this.mMaxExpGroupCount == 0)
        return false; 
      if (paramPositionMetadata.groupMetadata != null)
        return false; 
      if (this.mExpGroupMetadataList.size() >= this.mMaxExpGroupCount) {
        GroupMetadata groupMetadata1 = this.mExpGroupMetadataList.get(0);
        int k = this.mExpGroupMetadataList.indexOf(groupMetadata1);
        collapseGroup(groupMetadata1.gPos);
        if (paramPositionMetadata.groupInsertIndex > k)
          paramPositionMetadata.groupInsertIndex--; 
      } 
      int i = paramPositionMetadata.position.groupPos;
      ExpandableListAdapter expandableListAdapter = this.mExpandableListAdapter;
      int j = paramPositionMetadata.position.groupPos;
      long l = expandableListAdapter.getGroupId(j);
      GroupMetadata groupMetadata = GroupMetadata.obtain(-1, -1, i, l);
      this.mExpGroupMetadataList.add(paramPositionMetadata.groupInsertIndex, groupMetadata);
      refreshExpGroupMetadataList(false, false);
      notifyDataSetChanged();
      this.mExpandableListAdapter.onGroupExpanded(groupMetadata.gPos);
      return true;
    } 
    throw new RuntimeException("Need group");
  }
  
  public boolean isGroupExpanded(int paramInt) {
    for (int i = this.mExpGroupMetadataList.size() - 1; i >= 0; i--) {
      GroupMetadata groupMetadata = this.mExpGroupMetadataList.get(i);
      if (groupMetadata.gPos == paramInt)
        return true; 
    } 
    return false;
  }
  
  public void setMaxExpGroupCount(int paramInt) {
    this.mMaxExpGroupCount = paramInt;
  }
  
  ExpandableListAdapter getAdapter() {
    return this.mExpandableListAdapter;
  }
  
  public Filter getFilter() {
    ExpandableListAdapter expandableListAdapter = getAdapter();
    if (expandableListAdapter instanceof Filterable)
      return ((Filterable)expandableListAdapter).getFilter(); 
    return null;
  }
  
  ArrayList<GroupMetadata> getExpandedGroupMetadataList() {
    return this.mExpGroupMetadataList;
  }
  
  void setExpandedGroupMetadataList(ArrayList<GroupMetadata> paramArrayList) {
    if (paramArrayList != null) {
      ExpandableListAdapter expandableListAdapter = this.mExpandableListAdapter;
      if (expandableListAdapter != null) {
        int i = expandableListAdapter.getGroupCount();
        for (int j = paramArrayList.size() - 1; j >= 0; j--) {
          if (((GroupMetadata)paramArrayList.get(j)).gPos >= i)
            return; 
        } 
        this.mExpGroupMetadataList = paramArrayList;
        refreshExpGroupMetadataList(true, false);
        return;
      } 
    } 
  }
  
  public boolean isEmpty() {
    boolean bool;
    ExpandableListAdapter expandableListAdapter = getAdapter();
    if (expandableListAdapter != null) {
      bool = expandableListAdapter.isEmpty();
    } else {
      bool = true;
    } 
    return bool;
  }
  
  int findGroupPosition(long paramLong, int paramInt) {
    int i = this.mExpandableListAdapter.getGroupCount();
    if (i == 0)
      return -1; 
    if (paramLong == Long.MIN_VALUE)
      return -1; 
    paramInt = Math.max(0, paramInt);
    paramInt = Math.min(i - 1, paramInt);
    long l = SystemClock.uptimeMillis();
    int j = paramInt;
    int k = paramInt;
    boolean bool = false;
    ExpandableListAdapter expandableListAdapter = getAdapter();
    if (expandableListAdapter == null)
      return -1; 
    while (SystemClock.uptimeMillis() <= l + 100L) {
      boolean bool2;
      long l1 = expandableListAdapter.getGroupId(paramInt);
      if (l1 == paramLong)
        return paramInt; 
      boolean bool1 = true;
      if (k == i - 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (j != 0)
        bool1 = false; 
      if (bool2 && bool1)
        break; 
      if (bool1 || (bool && !bool2)) {
        paramInt = ++k;
        bool = false;
        continue;
      } 
      if (bool2 || (!bool && !bool1)) {
        paramInt = --j;
        bool = true;
      } 
    } 
    return -1;
  }
  
  class MyDataSetObserver extends DataSetObserver {
    final ExpandableListConnector this$0;
    
    public void onChanged() {
      ExpandableListConnector.this.refreshExpGroupMetadataList(true, true);
      ExpandableListConnector.this.notifyDataSetChanged();
    }
    
    public void onInvalidated() {
      ExpandableListConnector.this.refreshExpGroupMetadataList(true, true);
      ExpandableListConnector.this.notifyDataSetInvalidated();
    }
  }
  
  class GroupMetadata implements Parcelable, Comparable<GroupMetadata> {
    static GroupMetadata obtain(int param1Int1, int param1Int2, int param1Int3, long param1Long) {
      GroupMetadata groupMetadata = new GroupMetadata();
      groupMetadata.flPos = param1Int1;
      groupMetadata.lastChildFlPos = param1Int2;
      groupMetadata.gPos = param1Int3;
      groupMetadata.gId = param1Long;
      return groupMetadata;
    }
    
    public int compareTo(GroupMetadata param1GroupMetadata) {
      if (param1GroupMetadata != null)
        return this.gPos - param1GroupMetadata.gPos; 
      throw new IllegalArgumentException();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.flPos);
      param1Parcel.writeInt(this.lastChildFlPos);
      param1Parcel.writeInt(this.gPos);
      param1Parcel.writeLong(this.gId);
    }
    
    public static final Parcelable.Creator<GroupMetadata> CREATOR = new Parcelable.Creator<GroupMetadata>() {
        public ExpandableListConnector.GroupMetadata createFromParcel(Parcel param2Parcel) {
          int i = param2Parcel.readInt();
          int j = param2Parcel.readInt();
          int k = param2Parcel.readInt();
          long l = param2Parcel.readLong();
          return ExpandableListConnector.GroupMetadata.obtain(i, j, k, l);
        }
        
        public ExpandableListConnector.GroupMetadata[] newArray(int param2Int) {
          return new ExpandableListConnector.GroupMetadata[param2Int];
        }
      };
    
    static final int REFRESH = -1;
    
    int flPos;
    
    long gId;
    
    int gPos;
    
    int lastChildFlPos;
  }
  
  class PositionMetadata {
    private static final int MAX_POOL_SIZE = 5;
    
    private static ArrayList<PositionMetadata> sPool = new ArrayList<>(5);
    
    public int groupInsertIndex;
    
    public ExpandableListConnector.GroupMetadata groupMetadata;
    
    public ExpandableListPosition position;
    
    private void resetState() {
      ExpandableListPosition expandableListPosition = this.position;
      if (expandableListPosition != null) {
        expandableListPosition.recycle();
        this.position = null;
      } 
      this.groupMetadata = null;
      this.groupInsertIndex = 0;
    }
    
    static PositionMetadata obtain(int param1Int1, int param1Int2, int param1Int3, int param1Int4, ExpandableListConnector.GroupMetadata param1GroupMetadata, int param1Int5) {
      PositionMetadata positionMetadata = getRecycledOrCreate();
      positionMetadata.position = ExpandableListPosition.obtain(param1Int2, param1Int3, param1Int4, param1Int1);
      positionMetadata.groupMetadata = param1GroupMetadata;
      positionMetadata.groupInsertIndex = param1Int5;
      return positionMetadata;
    }
    
    private static PositionMetadata getRecycledOrCreate() {
      synchronized (sPool) {
        if (sPool.size() > 0) {
          PositionMetadata positionMetadata1 = sPool.remove(0);
          positionMetadata1.resetState();
          return positionMetadata1;
        } 
        PositionMetadata positionMetadata = new PositionMetadata();
        this();
        return positionMetadata;
      } 
    }
    
    public void recycle() {
      resetState();
      synchronized (sPool) {
        if (sPool.size() < 5)
          sPool.add(this); 
        return;
      } 
    }
    
    public boolean isExpanded() {
      boolean bool;
      if (this.groupMetadata != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
}
