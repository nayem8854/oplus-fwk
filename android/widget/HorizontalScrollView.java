package android.widget;

import android.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AnimationUtils;
import java.util.ArrayList;

public class HorizontalScrollView extends FrameLayout {
  private final Rect mTempRect = new Rect();
  
  private EdgeEffect mEdgeGlowLeft = new EdgeEffect(getContext());
  
  private EdgeEffect mEdgeGlowRight = new EdgeEffect(getContext());
  
  private boolean mIsLayoutDirty = true;
  
  private View mChildToScrollTo = null;
  
  private boolean mIsBeingDragged = false;
  
  private boolean mSmoothScrollingEnabled = true;
  
  private int mActivePointerId = -1;
  
  private static final int ANIMATED_SCROLL_GAP = 250;
  
  private static final int INVALID_POINTER = -1;
  
  private static final float MAX_SCROLL_FACTOR = 0.5F;
  
  private static final String TAG = "HorizontalScrollView";
  
  @ExportedProperty(category = "layout")
  private boolean mFillViewport;
  
  private float mHorizontalScrollFactor;
  
  private int mLastMotionX;
  
  private long mLastScroll;
  
  private int mMaximumVelocity;
  
  private int mMinimumVelocity;
  
  private int mOverflingDistance;
  
  private int mOverscrollDistance;
  
  private SavedState mSavedState;
  
  private OverScroller mScroller;
  
  private int mTouchSlop;
  
  private VelocityTracker mVelocityTracker;
  
  public HorizontalScrollView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public HorizontalScrollView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843603);
  }
  
  public HorizontalScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public HorizontalScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    initScrollView();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.HorizontalScrollView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.HorizontalScrollView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    setFillViewport(typedArray.getBoolean(0, false));
    typedArray.recycle();
    if ((paramContext.getResources().getConfiguration()).uiMode == 6)
      setRevealOnFocusHint(false); 
  }
  
  protected float getLeftFadingEdgeStrength() {
    if (getChildCount() == 0)
      return 0.0F; 
    int i = getHorizontalFadingEdgeLength();
    if (this.mScrollX < i)
      return this.mScrollX / i; 
    return 1.0F;
  }
  
  protected float getRightFadingEdgeStrength() {
    if (getChildCount() == 0)
      return 0.0F; 
    int i = getHorizontalFadingEdgeLength();
    int j = getWidth(), k = this.mPaddingRight;
    k = getChildAt(0).getRight() - this.mScrollX - j - k;
    if (k < i)
      return k / i; 
    return 1.0F;
  }
  
  public void setEdgeEffectColor(int paramInt) {
    setLeftEdgeEffectColor(paramInt);
    setRightEdgeEffectColor(paramInt);
  }
  
  public void setRightEdgeEffectColor(int paramInt) {
    this.mEdgeGlowRight.setColor(paramInt);
  }
  
  public void setLeftEdgeEffectColor(int paramInt) {
    this.mEdgeGlowLeft.setColor(paramInt);
  }
  
  public int getLeftEdgeEffectColor() {
    return this.mEdgeGlowLeft.getColor();
  }
  
  public int getRightEdgeEffectColor() {
    return this.mEdgeGlowRight.getColor();
  }
  
  public int getMaxScrollAmount() {
    return (int)((this.mRight - this.mLeft) * 0.5F);
  }
  
  private void initScrollView() {
    this.mScroller = new OverScroller(getContext());
    setFocusable(true);
    setDescendantFocusability(262144);
    setWillNotDraw(false);
    ViewConfiguration viewConfiguration = ViewConfiguration.get(this.mContext);
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    this.mMinimumVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
    this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    this.mOverscrollDistance = viewConfiguration.getScaledOverscrollDistance();
    this.mOverflingDistance = viewConfiguration.getScaledOverflingDistance();
    this.mHorizontalScrollFactor = viewConfiguration.getScaledHorizontalScrollFactor();
  }
  
  public void addView(View paramView) {
    if (getChildCount() <= 0) {
      super.addView(paramView);
      return;
    } 
    throw new IllegalStateException("HorizontalScrollView can host only one direct child");
  }
  
  public void addView(View paramView, int paramInt) {
    if (getChildCount() <= 0) {
      super.addView(paramView, paramInt);
      return;
    } 
    throw new IllegalStateException("HorizontalScrollView can host only one direct child");
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    if (getChildCount() <= 0) {
      super.addView(paramView, paramLayoutParams);
      return;
    } 
    throw new IllegalStateException("HorizontalScrollView can host only one direct child");
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    if (getChildCount() <= 0) {
      super.addView(paramView, paramInt, paramLayoutParams);
      return;
    } 
    throw new IllegalStateException("HorizontalScrollView can host only one direct child");
  }
  
  private boolean canScroll() {
    boolean bool = false;
    View view = getChildAt(0);
    if (view != null) {
      int i = view.getWidth();
      if (getWidth() < this.mPaddingLeft + i + this.mPaddingRight)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public boolean isFillViewport() {
    return this.mFillViewport;
  }
  
  public void setFillViewport(boolean paramBoolean) {
    if (paramBoolean != this.mFillViewport) {
      this.mFillViewport = paramBoolean;
      requestLayout();
    } 
  }
  
  public boolean isSmoothScrollingEnabled() {
    return this.mSmoothScrollingEnabled;
  }
  
  public void setSmoothScrollingEnabled(boolean paramBoolean) {
    this.mSmoothScrollingEnabled = paramBoolean;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    if (!this.mFillViewport)
      return; 
    paramInt1 = View.MeasureSpec.getMode(paramInt1);
    if (paramInt1 == 0)
      return; 
    if (getChildCount() > 0) {
      View view = getChildAt(0);
      FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)view.getLayoutParams();
      paramInt1 = (getContext().getApplicationInfo()).targetSdkVersion;
      if (paramInt1 >= 23) {
        i = this.mPaddingLeft + this.mPaddingRight + layoutParams.leftMargin + layoutParams.rightMargin;
        paramInt1 = this.mPaddingTop + this.mPaddingBottom + layoutParams.topMargin + layoutParams.bottomMargin;
      } else {
        i = this.mPaddingLeft + this.mPaddingRight;
        paramInt1 = this.mPaddingTop + this.mPaddingBottom;
      } 
      int i = getMeasuredWidth() - i;
      if (view.getMeasuredWidth() < i) {
        i = View.MeasureSpec.makeMeasureSpec(i, 1073741824);
        paramInt1 = getChildMeasureSpec(paramInt2, paramInt1, layoutParams.height);
        view.measure(i, paramInt1);
      } 
    } 
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    return (super.dispatchKeyEvent(paramKeyEvent) || executeKeyEvent(paramKeyEvent));
  }
  
  public boolean executeKeyEvent(KeyEvent paramKeyEvent) {
    View view;
    this.mTempRect.setEmpty();
    boolean bool = canScroll();
    byte b = 66;
    if (!bool) {
      boolean bool2 = isFocused();
      bool = false;
      if (bool2) {
        View view1 = findFocus();
        view = view1;
        if (view1 == this)
          view = null; 
        view = FocusFinder.getInstance().findNextFocus(this, view, 66);
        if (view != null && view != this && 
          view.requestFocus(66))
          bool = true; 
        return bool;
      } 
      return false;
    } 
    boolean bool1 = false;
    bool = bool1;
    if (view.getAction() == 0) {
      int i = view.getKeyCode();
      if (i != 21) {
        if (i != 22) {
          if (i != 62) {
            bool = bool1;
          } else {
            if (view.isShiftPressed())
              b = 17; 
            pageScroll(b);
            bool = bool1;
          } 
        } else if (!view.isAltPressed()) {
          bool = arrowScroll(66);
        } else {
          bool = fullScroll(66);
        } 
      } else if (!view.isAltPressed()) {
        bool = arrowScroll(17);
      } else {
        bool = fullScroll(17);
      } 
    } 
    return bool;
  }
  
  private boolean inChild(int paramInt1, int paramInt2) {
    int i = getChildCount();
    boolean bool = false;
    if (i > 0) {
      i = this.mScrollX;
      View view = getChildAt(0);
      if (paramInt2 >= view.getTop() && 
        paramInt2 < view.getBottom() && 
        paramInt1 >= view.getLeft() - i && 
        paramInt1 < view.getRight() - i)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  private void initOrResetVelocityTracker() {
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker == null) {
      this.mVelocityTracker = VelocityTracker.obtain();
    } else {
      velocityTracker.clear();
    } 
  }
  
  private void initVelocityTrackerIfNotExists() {
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain(); 
  }
  
  private void recycleVelocityTracker() {
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null) {
      velocityTracker.recycle();
      this.mVelocityTracker = null;
    } 
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean) {
    if (paramBoolean)
      recycleVelocityTracker(); 
    super.requestDisallowInterceptTouchEvent(paramBoolean);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    StringBuilder stringBuilder;
    int i = paramMotionEvent.getAction();
    if (i == 2 && this.mIsBeingDragged)
      return true; 
    if (super.onInterceptTouchEvent(paramMotionEvent))
      return true; 
    i &= 0xFF;
    if (i != 0) {
      if (i != 1)
        if (i != 2) {
          if (i != 3) {
            if (i != 5) {
              if (i == 6) {
                onSecondaryPointerUp(paramMotionEvent);
                this.mLastMotionX = (int)paramMotionEvent.getX(paramMotionEvent.findPointerIndex(this.mActivePointerId));
              } 
            } else {
              i = paramMotionEvent.getActionIndex();
              this.mLastMotionX = (int)paramMotionEvent.getX(i);
              this.mActivePointerId = paramMotionEvent.getPointerId(i);
            } 
            return this.mIsBeingDragged;
          } 
        } else {
          i = this.mActivePointerId;
          if (i != -1) {
            int j = paramMotionEvent.findPointerIndex(i);
            if (j == -1) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Invalid pointerId=");
              stringBuilder.append(i);
              stringBuilder.append(" in onInterceptTouchEvent");
              Log.e("HorizontalScrollView", stringBuilder.toString());
            } else {
              j = (int)stringBuilder.getX(j);
              i = Math.abs(j - this.mLastMotionX);
              if (i > this.mTouchSlop) {
                this.mIsBeingDragged = true;
                this.mLastMotionX = j;
                initVelocityTrackerIfNotExists();
                this.mVelocityTracker.addMovement((MotionEvent)stringBuilder);
                if (this.mParent != null)
                  this.mParent.requestDisallowInterceptTouchEvent(true); 
              } 
            } 
          } 
          return this.mIsBeingDragged;
        }  
      this.mIsBeingDragged = false;
      this.mActivePointerId = -1;
      if (this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, getScrollRange(), 0, 0))
        postInvalidateOnAnimation(); 
    } else {
      i = (int)stringBuilder.getX();
      if (!inChild(i, (int)stringBuilder.getY())) {
        this.mIsBeingDragged = false;
        recycleVelocityTracker();
      } else {
        this.mLastMotionX = i;
        this.mActivePointerId = stringBuilder.getPointerId(0);
        initOrResetVelocityTracker();
        this.mVelocityTracker.addMovement((MotionEvent)stringBuilder);
        this.mIsBeingDragged = true ^ this.mScroller.isFinished();
      } 
    } 
    return this.mIsBeingDragged;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial initVelocityTrackerIfNotExists : ()V
    //   4: aload_0
    //   5: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   8: aload_1
    //   9: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   12: aload_1
    //   13: invokevirtual getAction : ()I
    //   16: istore_2
    //   17: iload_2
    //   18: sipush #255
    //   21: iand
    //   22: istore_2
    //   23: iconst_0
    //   24: istore_3
    //   25: iload_2
    //   26: ifeq -> 702
    //   29: iload_2
    //   30: iconst_1
    //   31: if_icmpeq -> 564
    //   34: iload_2
    //   35: iconst_2
    //   36: if_icmpeq -> 148
    //   39: iload_2
    //   40: iconst_3
    //   41: if_icmpeq -> 61
    //   44: iload_2
    //   45: bipush #6
    //   47: if_icmpeq -> 53
    //   50: goto -> 787
    //   53: aload_0
    //   54: aload_1
    //   55: invokespecial onSecondaryPointerUp : (Landroid/view/MotionEvent;)V
    //   58: goto -> 787
    //   61: aload_0
    //   62: getfield mIsBeingDragged : Z
    //   65: ifeq -> 145
    //   68: aload_0
    //   69: invokevirtual getChildCount : ()I
    //   72: ifle -> 145
    //   75: aload_0
    //   76: getfield mScroller : Landroid/widget/OverScroller;
    //   79: aload_0
    //   80: getfield mScrollX : I
    //   83: aload_0
    //   84: getfield mScrollY : I
    //   87: iconst_0
    //   88: aload_0
    //   89: invokespecial getScrollRange : ()I
    //   92: iconst_0
    //   93: iconst_0
    //   94: invokevirtual springBack : (IIIIII)Z
    //   97: ifeq -> 104
    //   100: aload_0
    //   101: invokevirtual postInvalidateOnAnimation : ()V
    //   104: aload_0
    //   105: iconst_m1
    //   106: putfield mActivePointerId : I
    //   109: aload_0
    //   110: iconst_0
    //   111: putfield mIsBeingDragged : Z
    //   114: aload_0
    //   115: invokespecial recycleVelocityTracker : ()V
    //   118: aload_0
    //   119: invokespecial shouldDisplayEdgeEffects : ()Z
    //   122: ifeq -> 142
    //   125: aload_0
    //   126: getfield mEdgeGlowLeft : Landroid/widget/EdgeEffect;
    //   129: invokevirtual onRelease : ()V
    //   132: aload_0
    //   133: getfield mEdgeGlowRight : Landroid/widget/EdgeEffect;
    //   136: invokevirtual onRelease : ()V
    //   139: goto -> 787
    //   142: goto -> 787
    //   145: goto -> 787
    //   148: aload_1
    //   149: aload_0
    //   150: getfield mActivePointerId : I
    //   153: invokevirtual findPointerIndex : (I)I
    //   156: istore #4
    //   158: iload #4
    //   160: iconst_m1
    //   161: if_icmpne -> 210
    //   164: new java/lang/StringBuilder
    //   167: dup
    //   168: invokespecial <init> : ()V
    //   171: astore_1
    //   172: aload_1
    //   173: ldc_w 'Invalid pointerId='
    //   176: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: aload_1
    //   181: aload_0
    //   182: getfield mActivePointerId : I
    //   185: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload_1
    //   190: ldc_w ' in onTouchEvent'
    //   193: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: ldc 'HorizontalScrollView'
    //   199: aload_1
    //   200: invokevirtual toString : ()Ljava/lang/String;
    //   203: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   206: pop
    //   207: goto -> 787
    //   210: aload_1
    //   211: iload #4
    //   213: invokevirtual getX : (I)F
    //   216: f2i
    //   217: istore #5
    //   219: aload_0
    //   220: getfield mLastMotionX : I
    //   223: iload #5
    //   225: isub
    //   226: istore_2
    //   227: aload_0
    //   228: getfield mIsBeingDragged : Z
    //   231: ifne -> 293
    //   234: iload_2
    //   235: invokestatic abs : (I)I
    //   238: aload_0
    //   239: getfield mTouchSlop : I
    //   242: if_icmple -> 293
    //   245: aload_0
    //   246: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   249: astore #6
    //   251: aload #6
    //   253: ifnull -> 264
    //   256: aload #6
    //   258: iconst_1
    //   259: invokeinterface requestDisallowInterceptTouchEvent : (Z)V
    //   264: aload_0
    //   265: iconst_1
    //   266: putfield mIsBeingDragged : Z
    //   269: iload_2
    //   270: ifle -> 283
    //   273: iload_2
    //   274: aload_0
    //   275: getfield mTouchSlop : I
    //   278: isub
    //   279: istore_2
    //   280: goto -> 293
    //   283: iload_2
    //   284: aload_0
    //   285: getfield mTouchSlop : I
    //   288: iadd
    //   289: istore_2
    //   290: goto -> 293
    //   293: aload_0
    //   294: getfield mIsBeingDragged : Z
    //   297: ifeq -> 561
    //   300: aload_0
    //   301: iload #5
    //   303: putfield mLastMotionX : I
    //   306: aload_0
    //   307: getfield mScrollX : I
    //   310: istore #7
    //   312: aload_0
    //   313: getfield mScrollY : I
    //   316: istore #5
    //   318: aload_0
    //   319: invokespecial getScrollRange : ()I
    //   322: istore #8
    //   324: aload_0
    //   325: invokevirtual getOverScrollMode : ()I
    //   328: istore #9
    //   330: iload #9
    //   332: ifeq -> 352
    //   335: iload_3
    //   336: istore #5
    //   338: iload #9
    //   340: iconst_1
    //   341: if_icmpne -> 355
    //   344: iload_3
    //   345: istore #5
    //   347: iload #8
    //   349: ifle -> 355
    //   352: iconst_1
    //   353: istore #5
    //   355: aload_0
    //   356: iload_2
    //   357: iconst_0
    //   358: aload_0
    //   359: getfield mScrollX : I
    //   362: iconst_0
    //   363: iload #8
    //   365: iconst_0
    //   366: aload_0
    //   367: getfield mOverscrollDistance : I
    //   370: iconst_0
    //   371: iconst_1
    //   372: invokevirtual overScrollBy : (IIIIIIIIZ)Z
    //   375: ifeq -> 385
    //   378: aload_0
    //   379: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   382: invokevirtual clear : ()V
    //   385: iload #5
    //   387: ifeq -> 558
    //   390: iload #7
    //   392: iload_2
    //   393: iadd
    //   394: istore #5
    //   396: iload #5
    //   398: ifge -> 462
    //   401: aload_0
    //   402: getfield mEdgeGlowLeft : Landroid/widget/EdgeEffect;
    //   405: astore #6
    //   407: iload_2
    //   408: i2f
    //   409: aload_0
    //   410: invokevirtual getWidth : ()I
    //   413: i2f
    //   414: fdiv
    //   415: fstore #10
    //   417: aload_1
    //   418: iload #4
    //   420: invokevirtual getY : (I)F
    //   423: aload_0
    //   424: invokevirtual getHeight : ()I
    //   427: i2f
    //   428: fdiv
    //   429: fstore #11
    //   431: aload #6
    //   433: fload #10
    //   435: fconst_1
    //   436: fload #11
    //   438: fsub
    //   439: invokevirtual onPull : (FF)V
    //   442: aload_0
    //   443: getfield mEdgeGlowRight : Landroid/widget/EdgeEffect;
    //   446: invokevirtual isFinished : ()Z
    //   449: ifne -> 525
    //   452: aload_0
    //   453: getfield mEdgeGlowRight : Landroid/widget/EdgeEffect;
    //   456: invokevirtual onRelease : ()V
    //   459: goto -> 525
    //   462: iload #5
    //   464: iload #8
    //   466: if_icmple -> 525
    //   469: aload_0
    //   470: getfield mEdgeGlowRight : Landroid/widget/EdgeEffect;
    //   473: astore #6
    //   475: iload_2
    //   476: i2f
    //   477: aload_0
    //   478: invokevirtual getWidth : ()I
    //   481: i2f
    //   482: fdiv
    //   483: fstore #11
    //   485: aload_1
    //   486: iload #4
    //   488: invokevirtual getY : (I)F
    //   491: aload_0
    //   492: invokevirtual getHeight : ()I
    //   495: i2f
    //   496: fdiv
    //   497: fstore #10
    //   499: aload #6
    //   501: fload #11
    //   503: fload #10
    //   505: invokevirtual onPull : (FF)V
    //   508: aload_0
    //   509: getfield mEdgeGlowLeft : Landroid/widget/EdgeEffect;
    //   512: invokevirtual isFinished : ()Z
    //   515: ifne -> 525
    //   518: aload_0
    //   519: getfield mEdgeGlowLeft : Landroid/widget/EdgeEffect;
    //   522: invokevirtual onRelease : ()V
    //   525: aload_0
    //   526: invokespecial shouldDisplayEdgeEffects : ()Z
    //   529: ifeq -> 558
    //   532: aload_0
    //   533: getfield mEdgeGlowLeft : Landroid/widget/EdgeEffect;
    //   536: astore_1
    //   537: aload_1
    //   538: invokevirtual isFinished : ()Z
    //   541: ifeq -> 554
    //   544: aload_0
    //   545: getfield mEdgeGlowRight : Landroid/widget/EdgeEffect;
    //   548: invokevirtual isFinished : ()Z
    //   551: ifne -> 558
    //   554: aload_0
    //   555: invokevirtual postInvalidateOnAnimation : ()V
    //   558: goto -> 787
    //   561: goto -> 787
    //   564: aload_0
    //   565: getfield mIsBeingDragged : Z
    //   568: ifeq -> 787
    //   571: aload_0
    //   572: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   575: astore_1
    //   576: aload_1
    //   577: sipush #1000
    //   580: aload_0
    //   581: getfield mMaximumVelocity : I
    //   584: i2f
    //   585: invokevirtual computeCurrentVelocity : (IF)V
    //   588: aload_1
    //   589: aload_0
    //   590: getfield mActivePointerId : I
    //   593: invokevirtual getXVelocity : (I)F
    //   596: f2i
    //   597: istore_2
    //   598: aload_0
    //   599: invokevirtual getChildCount : ()I
    //   602: ifle -> 664
    //   605: iload_2
    //   606: invokestatic abs : (I)I
    //   609: aload_0
    //   610: getfield mMinimumVelocity : I
    //   613: if_icmple -> 625
    //   616: aload_0
    //   617: iload_2
    //   618: ineg
    //   619: invokevirtual fling : (I)V
    //   622: goto -> 664
    //   625: aload_0
    //   626: getfield mScroller : Landroid/widget/OverScroller;
    //   629: astore_1
    //   630: aload_0
    //   631: getfield mScrollX : I
    //   634: istore_2
    //   635: aload_0
    //   636: getfield mScrollY : I
    //   639: istore #5
    //   641: aload_0
    //   642: invokespecial getScrollRange : ()I
    //   645: istore_3
    //   646: aload_1
    //   647: iload_2
    //   648: iload #5
    //   650: iconst_0
    //   651: iload_3
    //   652: iconst_0
    //   653: iconst_0
    //   654: invokevirtual springBack : (IIIIII)Z
    //   657: ifeq -> 664
    //   660: aload_0
    //   661: invokevirtual postInvalidateOnAnimation : ()V
    //   664: aload_0
    //   665: iconst_m1
    //   666: putfield mActivePointerId : I
    //   669: aload_0
    //   670: iconst_0
    //   671: putfield mIsBeingDragged : Z
    //   674: aload_0
    //   675: invokespecial recycleVelocityTracker : ()V
    //   678: aload_0
    //   679: invokespecial shouldDisplayEdgeEffects : ()Z
    //   682: ifeq -> 699
    //   685: aload_0
    //   686: getfield mEdgeGlowLeft : Landroid/widget/EdgeEffect;
    //   689: invokevirtual onRelease : ()V
    //   692: aload_0
    //   693: getfield mEdgeGlowRight : Landroid/widget/EdgeEffect;
    //   696: invokevirtual onRelease : ()V
    //   699: goto -> 787
    //   702: aload_0
    //   703: invokevirtual getChildCount : ()I
    //   706: ifne -> 711
    //   709: iconst_0
    //   710: ireturn
    //   711: aload_0
    //   712: getfield mScroller : Landroid/widget/OverScroller;
    //   715: invokevirtual isFinished : ()Z
    //   718: iconst_1
    //   719: ixor
    //   720: istore #12
    //   722: aload_0
    //   723: iload #12
    //   725: putfield mIsBeingDragged : Z
    //   728: iload #12
    //   730: ifeq -> 752
    //   733: aload_0
    //   734: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   737: astore #6
    //   739: aload #6
    //   741: ifnull -> 752
    //   744: aload #6
    //   746: iconst_1
    //   747: invokeinterface requestDisallowInterceptTouchEvent : (Z)V
    //   752: aload_0
    //   753: getfield mScroller : Landroid/widget/OverScroller;
    //   756: invokevirtual isFinished : ()Z
    //   759: ifne -> 769
    //   762: aload_0
    //   763: getfield mScroller : Landroid/widget/OverScroller;
    //   766: invokevirtual abortAnimation : ()V
    //   769: aload_0
    //   770: aload_1
    //   771: invokevirtual getX : ()F
    //   774: f2i
    //   775: putfield mLastMotionX : I
    //   778: aload_0
    //   779: aload_1
    //   780: iconst_0
    //   781: invokevirtual getPointerId : (I)I
    //   784: putfield mActivePointerId : I
    //   787: iconst_1
    //   788: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #668	-> 0
    //   #669	-> 4
    //   #671	-> 12
    //   #673	-> 17
    //   #803	-> 53
    //   #788	-> 61
    //   #789	-> 75
    //   #790	-> 100
    //   #792	-> 104
    //   #793	-> 109
    //   #794	-> 114
    //   #796	-> 118
    //   #797	-> 125
    //   #798	-> 132
    //   #796	-> 142
    //   #788	-> 145
    //   #699	-> 148
    //   #700	-> 158
    //   #701	-> 164
    //   #702	-> 207
    //   #705	-> 210
    //   #706	-> 219
    //   #707	-> 227
    //   #708	-> 245
    //   #709	-> 251
    //   #710	-> 256
    //   #712	-> 264
    //   #713	-> 269
    //   #714	-> 273
    //   #716	-> 283
    //   #719	-> 293
    //   #721	-> 300
    //   #723	-> 306
    //   #724	-> 312
    //   #725	-> 318
    //   #726	-> 324
    //   #727	-> 330
    //   #732	-> 355
    //   #735	-> 378
    //   #738	-> 385
    //   #739	-> 390
    //   #740	-> 396
    //   #741	-> 401
    //   #742	-> 417
    //   #741	-> 431
    //   #743	-> 442
    //   #744	-> 452
    //   #746	-> 462
    //   #747	-> 469
    //   #748	-> 485
    //   #747	-> 499
    //   #749	-> 508
    //   #750	-> 518
    //   #753	-> 525
    //   #754	-> 537
    //   #755	-> 554
    //   #758	-> 558
    //   #719	-> 561
    //   #761	-> 564
    //   #762	-> 571
    //   #763	-> 576
    //   #764	-> 588
    //   #766	-> 598
    //   #767	-> 605
    //   #768	-> 616
    //   #770	-> 625
    //   #771	-> 641
    //   #770	-> 646
    //   #772	-> 660
    //   #777	-> 664
    //   #778	-> 669
    //   #779	-> 674
    //   #781	-> 678
    //   #782	-> 685
    //   #783	-> 692
    //   #785	-> 699
    //   #675	-> 702
    //   #676	-> 709
    //   #678	-> 711
    //   #679	-> 733
    //   #680	-> 739
    //   #681	-> 744
    //   #689	-> 752
    //   #690	-> 762
    //   #694	-> 769
    //   #695	-> 778
    //   #696	-> 787
    //   #806	-> 787
  }
  
  private void onSecondaryPointerUp(MotionEvent paramMotionEvent) {
    int i = (paramMotionEvent.getAction() & 0xFF00) >> 8;
    int j = paramMotionEvent.getPointerId(i);
    if (j == this.mActivePointerId) {
      if (i == 0) {
        j = 1;
      } else {
        j = 0;
      } 
      this.mLastMotionX = (int)paramMotionEvent.getX(j);
      this.mActivePointerId = paramMotionEvent.getPointerId(j);
      VelocityTracker velocityTracker = this.mVelocityTracker;
      if (velocityTracker != null)
        velocityTracker.clear(); 
    } 
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.getAction() == 8)
      if (!this.mIsBeingDragged) {
        float f;
        if (paramMotionEvent.isFromSource(2)) {
          if ((paramMotionEvent.getMetaState() & 0x1) != 0) {
            f = -paramMotionEvent.getAxisValue(9);
          } else {
            f = paramMotionEvent.getAxisValue(10);
          } 
        } else if (paramMotionEvent.isFromSource(4194304)) {
          f = paramMotionEvent.getAxisValue(26);
        } else {
          f = 0.0F;
        } 
        int i = Math.round(this.mHorizontalScrollFactor * f);
        if (i != 0) {
          int j = getScrollRange();
          int k = this.mScrollX;
          int m = k + i;
          if (m < 0) {
            i = 0;
          } else {
            i = m;
            if (m > j)
              i = j; 
          } 
          if (i != k) {
            super.scrollTo(i, this.mScrollY);
            return true;
          } 
        } 
      }  
    return super.onGenericMotionEvent(paramMotionEvent);
  }
  
  public boolean shouldDelayChildPressedState() {
    return true;
  }
  
  protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    if (!this.mScroller.isFinished()) {
      int i = this.mScrollX;
      int j = this.mScrollY;
      this.mScrollX = paramInt1;
      this.mScrollY = paramInt2;
      invalidateParentIfNeeded();
      onScrollChanged(this.mScrollX, this.mScrollY, i, j);
      if (paramBoolean1)
        this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, getScrollRange(), 0, 0); 
    } else {
      super.scrollTo(paramInt1, paramInt2);
    } 
    awakenScrollBars();
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    if (paramInt != 4096)
      if (paramInt != 8192 && paramInt != 16908345) {
        if (paramInt != 16908347)
          return false; 
      } else {
        if (!isEnabled())
          return false; 
        int k = getWidth();
        paramInt = this.mPaddingLeft;
        int m = this.mPaddingRight;
        paramInt = Math.max(0, this.mScrollX - k - paramInt - m);
        if (paramInt != this.mScrollX) {
          smoothScrollTo(paramInt, 0);
          return true;
        } 
        return false;
      }  
    if (!isEnabled())
      return false; 
    paramInt = getWidth();
    int j = this.mPaddingLeft, i = this.mPaddingRight;
    paramInt = Math.min(this.mScrollX + paramInt - j - i, getScrollRange());
    if (paramInt != this.mScrollX) {
      smoothScrollTo(paramInt, 0);
      return true;
    } 
    return false;
  }
  
  public CharSequence getAccessibilityClassName() {
    return HorizontalScrollView.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    int i = getScrollRange();
    if (i > 0) {
      paramAccessibilityNodeInfo.setScrollable(true);
      if (isEnabled() && this.mScrollX > 0) {
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT);
      } 
      if (isEnabled() && this.mScrollX < i) {
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT);
      } 
    } 
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    boolean bool;
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    if (getScrollRange() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    paramAccessibilityEvent.setScrollable(bool);
    paramAccessibilityEvent.setMaxScrollX(getScrollRange());
    paramAccessibilityEvent.setMaxScrollY(this.mScrollY);
  }
  
  private int getScrollRange() {
    int i = 0;
    if (getChildCount() > 0) {
      View view = getChildAt(0);
      int j = view.getWidth(), k = getWidth(), m = this.mPaddingLeft;
      i = this.mPaddingRight;
      i = Math.max(0, j - k - m - i);
    } 
    return i;
  }
  
  private View findFocusableViewInMyBounds(boolean paramBoolean, int paramInt, View paramView) {
    int i = getHorizontalFadingEdgeLength() / 2;
    int j = paramInt + i;
    paramInt = getWidth() + paramInt - i;
    if (paramView != null && 
      paramView.getLeft() < paramInt && 
      paramView.getRight() > j)
      return paramView; 
    return findFocusableViewInBounds(paramBoolean, j, paramInt);
  }
  
  private View findFocusableViewInBounds(boolean paramBoolean, int paramInt1, int paramInt2) {
    ArrayList<View> arrayList = getFocusables(2);
    View view = null;
    boolean bool = false;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++, view = view2, bool = bool1) {
      View view1 = arrayList.get(b);
      int j = view1.getLeft();
      int k = view1.getRight();
      View view2 = view;
      boolean bool1 = bool;
      if (paramInt1 < k) {
        view2 = view;
        bool1 = bool;
        if (j < paramInt2) {
          boolean bool3, bool2 = false;
          if (paramInt1 < j && k < paramInt2) {
            bool3 = true;
          } else {
            bool3 = false;
          } 
          if (view == null) {
            view2 = view1;
            bool1 = bool3;
          } else {
            if ((paramBoolean && 
              j < view.getLeft()) || (!paramBoolean && 
              k > view.getRight()))
              bool2 = true; 
            if (bool) {
              view2 = view;
              bool1 = bool;
              if (bool3) {
                view2 = view;
                bool1 = bool;
                if (bool2) {
                  view2 = view1;
                  bool1 = bool;
                } 
              } 
            } else if (bool3) {
              view2 = view1;
              bool1 = true;
            } else {
              view2 = view;
              bool1 = bool;
              if (bool2) {
                view2 = view1;
                bool1 = bool;
              } 
            } 
          } 
        } 
      } 
    } 
    return view;
  }
  
  public boolean pageScroll(int paramInt) {
    int i;
    if (paramInt == 66) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = getWidth();
    if (i) {
      this.mTempRect.left = getScrollX() + j;
      i = getChildCount();
      if (i > 0) {
        View view = getChildAt(0);
        if (this.mTempRect.left + j > view.getRight())
          this.mTempRect.left = view.getRight() - j; 
      } 
    } else {
      this.mTempRect.left = getScrollX() - j;
      if (this.mTempRect.left < 0)
        this.mTempRect.left = 0; 
    } 
    Rect rect = this.mTempRect;
    rect.right = rect.left + j;
    return scrollAndFocus(paramInt, this.mTempRect.left, this.mTempRect.right);
  }
  
  public boolean fullScroll(int paramInt) {
    int i;
    if (paramInt == 66) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = getWidth();
    this.mTempRect.left = 0;
    this.mTempRect.right = j;
    if (i) {
      i = getChildCount();
      if (i > 0) {
        View view = getChildAt(0);
        this.mTempRect.right = view.getRight();
        Rect rect = this.mTempRect;
        rect.left = rect.right - j;
      } 
    } 
    return scrollAndFocus(paramInt, this.mTempRect.left, this.mTempRect.right);
  }
  
  private boolean scrollAndFocus(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool2, bool1 = true;
    int i = getWidth();
    int j = getScrollX();
    i = j + i;
    if (paramInt1 == 17) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    View view1 = findFocusableViewInBounds(bool2, paramInt2, paramInt3);
    View view2 = view1;
    if (view1 == null)
      view2 = this; 
    if (paramInt2 >= j && paramInt3 <= i) {
      bool2 = false;
    } else {
      if (bool2) {
        paramInt2 -= j;
      } else {
        paramInt2 = paramInt3 - i;
      } 
      doScrollX(paramInt2);
      bool2 = bool1;
    } 
    if (view2 != findFocus())
      view2.requestFocus(paramInt1); 
    return bool2;
  }
  
  public boolean arrowScroll(int paramInt) {
    View view1 = findFocus();
    View view2 = view1;
    if (view1 == this)
      view2 = null; 
    view1 = FocusFinder.getInstance().findNextFocus(this, view2, paramInt);
    int i = getMaxScrollAmount();
    if (view1 != null && isWithinDeltaOfScreen(view1, i)) {
      view1.getDrawingRect(this.mTempRect);
      offsetDescendantRectToMyCoords(view1, this.mTempRect);
      int j = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
      doScrollX(j);
      view1.requestFocus(paramInt);
    } else {
      int j, k = i;
      if (paramInt == 17 && getScrollX() < k) {
        j = getScrollX();
      } else {
        j = k;
        if (paramInt == 66) {
          j = k;
          if (getChildCount() > 0) {
            int m = getChildAt(0).getRight();
            int n = getScrollX() + getWidth();
            j = k;
            if (m - n < i)
              j = m - n; 
          } 
        } 
      } 
      if (j == 0)
        return false; 
      if (paramInt == 66) {
        paramInt = j;
      } else {
        paramInt = -j;
      } 
      doScrollX(paramInt);
    } 
    if (view2 != null && view2.isFocused() && 
      isOffScreen(view2)) {
      paramInt = getDescendantFocusability();
      setDescendantFocusability(131072);
      requestFocus();
      setDescendantFocusability(paramInt);
    } 
    return true;
  }
  
  private boolean isOffScreen(View paramView) {
    return isWithinDeltaOfScreen(paramView, 0) ^ true;
  }
  
  private boolean isWithinDeltaOfScreen(View paramView, int paramInt) {
    paramView.getDrawingRect(this.mTempRect);
    offsetDescendantRectToMyCoords(paramView, this.mTempRect);
    if (this.mTempRect.right + paramInt >= getScrollX()) {
      int i = this.mTempRect.left;
      if (i - paramInt <= getScrollX() + getWidth())
        return true; 
    } 
    return false;
  }
  
  private void doScrollX(int paramInt) {
    if (paramInt != 0)
      if (this.mSmoothScrollingEnabled) {
        smoothScrollBy(paramInt, 0);
      } else {
        scrollBy(paramInt, 0);
      }  
  }
  
  public final void smoothScrollBy(int paramInt1, int paramInt2) {
    if (getChildCount() == 0)
      return; 
    long l1 = AnimationUtils.currentAnimationTimeMillis(), l2 = this.mLastScroll;
    if (l1 - l2 > 250L) {
      int i = getWidth(), j = this.mPaddingRight;
      paramInt2 = this.mPaddingLeft;
      int k = getChildAt(0).getWidth();
      k = Math.max(0, k - i - j - paramInt2);
      paramInt2 = this.mScrollX;
      paramInt1 = Math.max(0, Math.min(paramInt2 + paramInt1, k));
      this.mScroller.startScroll(paramInt2, this.mScrollY, paramInt1 - paramInt2, 0);
      postInvalidateOnAnimation();
    } else {
      if (!this.mScroller.isFinished())
        this.mScroller.abortAnimation(); 
      scrollBy(paramInt1, paramInt2);
    } 
    this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
  }
  
  public final void smoothScrollTo(int paramInt1, int paramInt2) {
    smoothScrollBy(paramInt1 - this.mScrollX, paramInt2 - this.mScrollY);
  }
  
  protected int computeHorizontalScrollRange() {
    int i = getChildCount();
    int j = getWidth() - this.mPaddingLeft - this.mPaddingRight;
    if (i == 0)
      return j; 
    i = getChildAt(0).getRight();
    int k = this.mScrollX;
    int m = Math.max(0, i - j);
    if (k < 0) {
      j = i - k;
    } else {
      j = i;
      if (k > m)
        j = i + k - m; 
    } 
    return j;
  }
  
  protected int computeHorizontalScrollOffset() {
    return Math.max(0, super.computeHorizontalScrollOffset());
  }
  
  protected void measureChild(View paramView, int paramInt1, int paramInt2) {
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    int i = this.mPaddingLeft, j = this.mPaddingRight;
    paramInt1 = Math.max(0, View.MeasureSpec.getSize(paramInt1) - i + j);
    paramInt1 = View.MeasureSpec.makeSafeMeasureSpec(paramInt1, 0);
    paramInt2 = getChildMeasureSpec(paramInt2, this.mPaddingTop + this.mPaddingBottom, layoutParams.height);
    paramView.measure(paramInt1, paramInt2);
  }
  
  protected void measureChildWithMargins(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    paramInt3 = getChildMeasureSpec(paramInt3, this.mPaddingTop + this.mPaddingBottom + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + paramInt4, marginLayoutParams.height);
    paramInt4 = this.mPaddingLeft;
    int i = this.mPaddingRight, j = marginLayoutParams.leftMargin, k = marginLayoutParams.rightMargin;
    paramInt1 = Math.max(0, View.MeasureSpec.getSize(paramInt1) - paramInt4 + i + j + k + paramInt2);
    paramInt1 = View.MeasureSpec.makeSafeMeasureSpec(paramInt1, 0);
    paramView.measure(paramInt1, paramInt3);
  }
  
  public void computeScroll() {
    if (this.mScroller.computeScrollOffset()) {
      int i = this.mScrollX;
      int j = this.mScrollY;
      int k = this.mScroller.getCurrX();
      int m = this.mScroller.getCurrY();
      if (i != k || j != m) {
        int n = getScrollRange();
        int i1 = getOverScrollMode();
        boolean bool1 = true, bool2 = bool1;
        if (i1 != 0)
          if (i1 == 1 && n > 0) {
            bool2 = bool1;
          } else {
            bool2 = false;
          }  
        overScrollBy(k - i, m - j, i, j, n, 0, this.mOverflingDistance, 0, false);
        onScrollChanged(this.mScrollX, this.mScrollY, i, j);
        if (bool2)
          if (k < 0 && i >= 0) {
            this.mEdgeGlowLeft.onAbsorb((int)this.mScroller.getCurrVelocity());
          } else if (k > n && i <= n) {
            this.mEdgeGlowRight.onAbsorb((int)this.mScroller.getCurrVelocity());
          }  
      } 
      if (!awakenScrollBars())
        postInvalidateOnAnimation(); 
    } 
  }
  
  private void scrollToChild(View paramView) {
    paramView.getDrawingRect(this.mTempRect);
    offsetDescendantRectToMyCoords(paramView, this.mTempRect);
    int i = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
    if (i != 0)
      scrollBy(i, 0); 
  }
  
  private boolean scrollToChildRect(Rect paramRect, boolean paramBoolean) {
    boolean bool;
    int i = computeScrollDeltaToGetChildRectOnScreen(paramRect);
    if (i != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool)
      if (paramBoolean) {
        scrollBy(i, 0);
      } else {
        smoothScrollBy(i, 0);
      }  
    return bool;
  }
  
  protected int computeScrollDeltaToGetChildRectOnScreen(Rect paramRect) {
    if (getChildCount() == 0)
      return 0; 
    int i = getWidth();
    int j = getScrollX();
    int k = j + i;
    int m = getHorizontalFadingEdgeLength();
    int n = j;
    if (paramRect.left > 0)
      n = j + m; 
    j = k;
    if (paramRect.right < getChildAt(0).getWidth())
      j = k - m; 
    m = 0;
    if (paramRect.right > j && paramRect.left > n) {
      if (paramRect.width() > i) {
        k = 0 + paramRect.left - n;
      } else {
        k = 0 + paramRect.right - j;
      } 
      n = getChildAt(0).getRight();
      k = Math.min(k, n - j);
    } else {
      k = m;
      if (paramRect.left < n) {
        k = m;
        if (paramRect.right < j) {
          if (paramRect.width() > i) {
            k = 0 - j - paramRect.right;
          } else {
            k = 0 - n - paramRect.left;
          } 
          k = Math.max(k, -getScrollX());
        } 
      } 
    } 
    return k;
  }
  
  public void requestChildFocus(View paramView1, View paramView2) {
    if (paramView2 != null && paramView2.getRevealOnFocusHint())
      if (!this.mIsLayoutDirty) {
        scrollToChild(paramView2);
      } else {
        this.mChildToScrollTo = paramView2;
      }  
    super.requestChildFocus(paramView1, paramView2);
  }
  
  protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect) {
    int i;
    View view;
    if (paramInt == 2) {
      i = 66;
    } else {
      i = paramInt;
      if (paramInt == 1)
        i = 17; 
    } 
    if (paramRect == null) {
      view = FocusFinder.getInstance().findNextFocus(this, null, i);
    } else {
      view = FocusFinder.getInstance().findNextFocusFromRect(this, paramRect, i);
    } 
    if (view == null)
      return false; 
    if (isOffScreen(view))
      return false; 
    return view.requestFocus(i, paramRect);
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean) {
    int i = paramView.getLeft(), j = paramView.getScrollX();
    int k = paramView.getTop(), m = paramView.getScrollY();
    paramRect.offset(i - j, k - m);
    return scrollToChildRect(paramRect, paramBoolean);
  }
  
  public void requestLayout() {
    this.mIsLayoutDirty = true;
    super.requestLayout();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    byte b1, b2;
    if (getChildCount() > 0) {
      b1 = getChildAt(0).getMeasuredWidth();
      FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)getChildAt(0).getLayoutParams();
      b2 = layoutParams.leftMargin;
      int k = layoutParams.rightMargin;
      b2 += k;
    } else {
      b1 = 0;
      b2 = 0;
    } 
    int j = getPaddingLeftWithForeground();
    int i = getPaddingRightWithForeground();
    if (b1 > paramInt3 - paramInt1 - j - i - b2) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    layoutChildren(paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean);
    this.mIsLayoutDirty = false;
    View view = this.mChildToScrollTo;
    if (view != null && isViewDescendantOf(view, this))
      scrollToChild(this.mChildToScrollTo); 
    this.mChildToScrollTo = null;
    if (!isLaidOut()) {
      paramInt2 = Math.max(0, b1 - paramInt3 - paramInt1 - this.mPaddingLeft - this.mPaddingRight);
      if (this.mSavedState != null) {
        if (isLayoutRtl()) {
          paramInt1 = paramInt2 - this.mSavedState.scrollOffsetFromStart;
        } else {
          paramInt1 = this.mSavedState.scrollOffsetFromStart;
        } 
        this.mScrollX = paramInt1;
        this.mSavedState = null;
      } else if (isLayoutRtl()) {
        this.mScrollX = paramInt2 - this.mScrollX;
      } 
      if (this.mScrollX > paramInt2) {
        this.mScrollX = paramInt2;
      } else if (this.mScrollX < 0) {
        this.mScrollX = 0;
      } 
    } 
    scrollTo(this.mScrollX, this.mScrollY);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    View view = findFocus();
    if (view == null || this == view)
      return; 
    paramInt1 = this.mRight;
    paramInt2 = this.mLeft;
    if (isWithinDeltaOfScreen(view, paramInt1 - paramInt2)) {
      view.getDrawingRect(this.mTempRect);
      offsetDescendantRectToMyCoords(view, this.mTempRect);
      paramInt1 = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
      doScrollX(paramInt1);
    } 
  }
  
  private static boolean isViewDescendantOf(View paramView1, View paramView2) {
    boolean bool = true;
    if (paramView1 == paramView2)
      return true; 
    ViewParent viewParent = paramView1.getParent();
    if (!(viewParent instanceof ViewGroup) || !isViewDescendantOf((View)viewParent, paramView2))
      bool = false; 
    return bool;
  }
  
  public void fling(int paramInt) {
    if (getChildCount() > 0) {
      int i = getWidth() - this.mPaddingRight - this.mPaddingLeft;
      boolean bool = false;
      int j = getChildAt(0).getWidth();
      OverScroller overScroller = this.mScroller;
      int k = this.mScrollX, m = this.mScrollY;
      j = Math.max(0, j - i);
      i /= 2;
      overScroller.fling(k, m, paramInt, 0, 0, j, 0, 0, i, 0);
      if (paramInt > 0)
        bool = true; 
      View view2 = findFocus();
      overScroller = this.mScroller;
      paramInt = overScroller.getFinalX();
      View view3 = findFocusableViewInMyBounds(bool, paramInt, view2);
      View view1 = view3;
      if (view3 == null)
        view1 = this; 
      if (view1 != view2) {
        if (bool) {
          paramInt = 66;
        } else {
          paramInt = 17;
        } 
        view1.requestFocus(paramInt);
      } 
      postInvalidateOnAnimation();
    } 
  }
  
  public void scrollTo(int paramInt1, int paramInt2) {
    if (getChildCount() > 0) {
      View view = getChildAt(0);
      paramInt1 = clamp(paramInt1, getWidth() - this.mPaddingRight - this.mPaddingLeft, view.getWidth());
      paramInt2 = clamp(paramInt2, getHeight() - this.mPaddingBottom - this.mPaddingTop, view.getHeight());
      if (paramInt1 != this.mScrollX || paramInt2 != this.mScrollY)
        super.scrollTo(paramInt1, paramInt2); 
    } 
  }
  
  private boolean shouldDisplayEdgeEffects() {
    boolean bool;
    if (getOverScrollMode() != 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void draw(Canvas paramCanvas) {
    super.draw(paramCanvas);
    if (shouldDisplayEdgeEffects()) {
      int i = this.mScrollX;
      if (!this.mEdgeGlowLeft.isFinished()) {
        int j = paramCanvas.save();
        int k = getHeight() - this.mPaddingTop - this.mPaddingBottom;
        paramCanvas.rotate(270.0F);
        paramCanvas.translate((-k + this.mPaddingTop), Math.min(0, i));
        this.mEdgeGlowLeft.setSize(k, getWidth());
        if (this.mEdgeGlowLeft.draw(paramCanvas))
          postInvalidateOnAnimation(); 
        paramCanvas.restoreToCount(j);
      } 
      if (!this.mEdgeGlowRight.isFinished()) {
        int m = paramCanvas.save();
        int k = getWidth();
        int n = getHeight(), j = this.mPaddingTop, i1 = this.mPaddingBottom;
        paramCanvas.rotate(90.0F);
        float f1 = -this.mPaddingTop;
        float f2 = -(Math.max(getScrollRange(), i) + k);
        paramCanvas.translate(f1, f2);
        this.mEdgeGlowRight.setSize(n - j - i1, k);
        if (this.mEdgeGlowRight.draw(paramCanvas))
          postInvalidateOnAnimation(); 
        paramCanvas.restoreToCount(m);
      } 
    } 
  }
  
  private static int clamp(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt2 >= paramInt3 || paramInt1 < 0)
      return 0; 
    if (paramInt2 + paramInt1 > paramInt3)
      return paramInt3 - paramInt2; 
    return paramInt1;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if ((this.mContext.getApplicationInfo()).targetSdkVersion <= 18) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    this.mSavedState = (SavedState)paramParcelable;
    requestLayout();
  }
  
  protected Parcelable onSaveInstanceState() {
    int i;
    if ((this.mContext.getApplicationInfo()).targetSdkVersion <= 18)
      return super.onSaveInstanceState(); 
    Parcelable parcelable = super.onSaveInstanceState();
    parcelable = new SavedState(parcelable);
    if (isLayoutRtl()) {
      i = -this.mScrollX;
    } else {
      i = this.mScrollX;
    } 
    ((SavedState)parcelable).scrollOffsetFromStart = i;
    return parcelable;
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("layout:fillViewPort", this.mFillViewport);
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState(HorizontalScrollView this$0) {
      super((Parcelable)this$0);
    }
    
    public SavedState(HorizontalScrollView this$0) {
      super((Parcel)this$0);
      this.scrollOffsetFromStart = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.scrollOffsetFromStart);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("HorizontalScrollView.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" scrollPosition=");
      stringBuilder.append(this.scrollOffsetFromStart);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    public int scrollOffsetFromStart;
  }
}
