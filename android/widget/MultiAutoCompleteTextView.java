package android.widget;

import android.content.Context;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.QwertyKeyListener;
import android.util.AttributeSet;

public class MultiAutoCompleteTextView extends AutoCompleteTextView {
  private Tokenizer mTokenizer;
  
  public MultiAutoCompleteTextView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public MultiAutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842859);
  }
  
  public MultiAutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public MultiAutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  void finishInit() {}
  
  public void setTokenizer(Tokenizer paramTokenizer) {
    this.mTokenizer = paramTokenizer;
  }
  
  protected void performFiltering(CharSequence paramCharSequence, int paramInt) {
    if (enoughToFilter()) {
      int i = getSelectionEnd();
      int j = this.mTokenizer.findTokenStart(paramCharSequence, i);
      performFiltering(paramCharSequence, j, i, paramInt);
    } else {
      dismissDropDown();
      Filter filter = getFilter();
      if (filter != null)
        filter.filter(null); 
    } 
  }
  
  public boolean enoughToFilter() {
    Editable editable = getText();
    int i = getSelectionEnd();
    if (i >= 0) {
      Tokenizer tokenizer = this.mTokenizer;
      if (tokenizer != null) {
        int j = tokenizer.findTokenStart(editable, i);
        if (i - j >= getThreshold())
          return true; 
        return false;
      } 
    } 
    return false;
  }
  
  public void performValidation() {
    AutoCompleteTextView.Validator validator = getValidator();
    if (validator == null || this.mTokenizer == null)
      return; 
    Editable editable = getText();
    int i = getText().length();
    while (i > 0) {
      int j = this.mTokenizer.findTokenStart(editable, i);
      int k = this.mTokenizer.findTokenEnd(editable, j);
      CharSequence charSequence = editable.subSequence(j, k);
      if (TextUtils.isEmpty(charSequence)) {
        editable.replace(j, i, "");
      } else if (!validator.isValid(charSequence)) {
        Tokenizer tokenizer = this.mTokenizer;
        CharSequence charSequence1 = tokenizer.terminateToken(validator.fixText(charSequence));
        editable.replace(j, i, charSequence1);
      } 
      i = j;
    } 
  }
  
  protected void performFiltering(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    getFilter().filter(paramCharSequence.subSequence(paramInt1, paramInt2), this);
  }
  
  protected void replaceText(CharSequence paramCharSequence) {
    clearComposingText();
    int i = getSelectionEnd();
    int j = this.mTokenizer.findTokenStart(getText(), i);
    Editable editable = getText();
    String str = TextUtils.substring(editable, j, i);
    QwertyKeyListener.markAsReplaced(editable, j, i, str);
    editable.replace(j, i, this.mTokenizer.terminateToken(paramCharSequence));
  }
  
  public CharSequence getAccessibilityClassName() {
    return MultiAutoCompleteTextView.class.getName();
  }
  
  class CommaTokenizer implements Tokenizer {
    public int findTokenStart(CharSequence param1CharSequence, int param1Int) {
      int j, i = param1Int;
      while (true) {
        j = i;
        if (i > 0) {
          j = i;
          if (param1CharSequence.charAt(i - 1) != ',') {
            i--;
            continue;
          } 
        } 
        break;
      } 
      while (j < param1Int && param1CharSequence.charAt(j) == ' ')
        j++; 
      return j;
    }
    
    public int findTokenEnd(CharSequence param1CharSequence, int param1Int) {
      int i = param1CharSequence.length();
      while (param1Int < i) {
        if (param1CharSequence.charAt(param1Int) == ',')
          return param1Int; 
        param1Int++;
      } 
      return i;
    }
    
    public CharSequence terminateToken(CharSequence param1CharSequence) {
      int i = param1CharSequence.length();
      while (i > 0 && param1CharSequence.charAt(i - 1) == ' ')
        i--; 
      if (i > 0 && param1CharSequence.charAt(i - 1) == ',')
        return param1CharSequence; 
      if (param1CharSequence instanceof Spanned) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(param1CharSequence);
        stringBuilder1.append(", ");
        SpannableString spannableString = new SpannableString(stringBuilder1.toString());
        TextUtils.copySpansFrom((Spanned)param1CharSequence, 0, param1CharSequence.length(), Object.class, spannableString, 0);
        return spannableString;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1CharSequence);
      stringBuilder.append(", ");
      return stringBuilder.toString();
    }
  }
  
  class Tokenizer {
    public abstract int findTokenEnd(CharSequence param1CharSequence, int param1Int);
    
    public abstract int findTokenStart(CharSequence param1CharSequence, int param1Int);
    
    public abstract CharSequence terminateToken(CharSequence param1CharSequence);
  }
}
