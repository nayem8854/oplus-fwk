package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R;

@Deprecated
public class TwoLineListItem extends RelativeLayout {
  private TextView mText1;
  
  private TextView mText2;
  
  public TwoLineListItem(Context paramContext) {
    this(paramContext, (AttributeSet)null, 0);
  }
  
  public TwoLineListItem(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public TwoLineListItem(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TwoLineListItem(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TwoLineListItem, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.TwoLineListItem, paramAttributeSet, typedArray, paramInt1, paramInt2);
    typedArray.recycle();
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mText1 = findViewById(16908308);
    this.mText2 = findViewById(16908309);
  }
  
  public TextView getText1() {
    return this.mText1;
  }
  
  public TextView getText2() {
    return this.mText2;
  }
  
  public CharSequence getAccessibilityClassName() {
    return TwoLineListItem.class.getName();
  }
}
