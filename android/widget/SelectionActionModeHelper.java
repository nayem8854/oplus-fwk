package android.widget;

import android.app.RemoteAction;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.LocaleList;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.textclassifier.ExtrasUtils;
import android.view.textclassifier.SelectionEvent;
import android.view.textclassifier.TextClassification;
import android.view.textclassifier.TextClassificationConstants;
import android.view.textclassifier.TextClassificationContext;
import android.view.textclassifier.TextClassificationManager;
import android.view.textclassifier.TextClassifier;
import android.view.textclassifier.TextClassifierEvent;
import android.view.textclassifier.TextSelection;
import com.android.internal.util.Preconditions;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;

public final class SelectionActionModeHelper {
  private static final String LOG_TAG = "SelectActionModeHelper";
  
  private final Editor mEditor;
  
  private final SelectionTracker mSelectionTracker;
  
  private final SmartSelectSprite mSmartSelectSprite;
  
  private TextClassification mTextClassification;
  
  private AsyncTask mTextClassificationAsyncTask;
  
  private final TextClassificationHelper mTextClassificationHelper;
  
  private final TextView mTextView;
  
  SelectionActionModeHelper(Editor paramEditor) {
    Objects.requireNonNull(paramEditor);
    Editor editor = paramEditor;
    TextView textView1 = editor.getTextView();
    Context context = textView1.getContext();
    TextView textView2 = this.mTextView;
    Objects.requireNonNull(textView2);
    _$$Lambda$GgAIoNUUH8pNRbtcqGeR1oLuEXw _$$Lambda$GgAIoNUUH8pNRbtcqGeR1oLuEXw = new _$$Lambda$GgAIoNUUH8pNRbtcqGeR1oLuEXw(textView2);
    TextView textView3 = this.mTextView;
    CharSequence charSequence = getText(textView3);
    textView3 = this.mTextView;
    this.mTextClassificationHelper = new TextClassificationHelper(context, _$$Lambda$GgAIoNUUH8pNRbtcqGeR1oLuEXw, charSequence, 0, 1, textView3.getTextLocales());
    this.mSelectionTracker = new SelectionTracker(this.mTextView);
    if (getTextClassificationSettings().isSmartSelectionAnimationEnabled()) {
      context = this.mTextView.getContext();
      int i = (paramEditor.getTextView()).mHighlightColor;
      TextView textView = this.mTextView;
      Objects.requireNonNull(textView);
      this.mSmartSelectSprite = new SmartSelectSprite(context, i, new _$$Lambda$IfzAW5fP9thoftErKAjo9SLZufw(textView));
    } else {
      this.mSmartSelectSprite = null;
    } 
  }
  
  private static int[] sortSelctionIndices(int paramInt1, int paramInt2) {
    if (paramInt1 < paramInt2)
      return new int[] { paramInt1, paramInt2 }; 
    return new int[] { paramInt2, paramInt1 };
  }
  
  private static int[] sortSelctionIndicesFromTextView(TextView paramTextView) {
    int i = paramTextView.getSelectionStart();
    int j = paramTextView.getSelectionEnd();
    return sortSelctionIndices(i, j);
  }
  
  public void startSelectionActionModeAsync(boolean paramBoolean) {
    boolean bool = getTextClassificationSettings().isSmartSelectionEnabled();
    int[] arrayOfInt = sortSelctionIndicesFromTextView(this.mTextView);
    SelectionTracker selectionTracker = this.mSelectionTracker;
    TextView textView = this.mTextView;
    CharSequence charSequence = getText(textView);
    int i = arrayOfInt[0], j = arrayOfInt[1];
    selectionTracker.onOriginalSelection(charSequence, i, j, false);
    cancelAsyncTask();
    if (skipTextClassification()) {
      startSelectionActionMode(null);
    } else {
      _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI;
      _$$Lambda$SelectionActionModeHelper$CcJ0IF8nDFsmkuaqvOxFqYGazzY _$$Lambda$SelectionActionModeHelper$CcJ0IF8nDFsmkuaqvOxFqYGazzY;
      resetTextClassificationHelper();
      TextView textView1 = this.mTextView;
      TextClassificationHelper textClassificationHelper1 = this.mTextClassificationHelper;
      j = textClassificationHelper1.getTimeoutDuration();
      if (paramBoolean & bool) {
        textClassificationHelper1 = this.mTextClassificationHelper;
        Objects.requireNonNull(textClassificationHelper1);
        _$$Lambda$E_XesXLNXm7BCuVAnjZcIGfnQJQ _$$Lambda$E_XesXLNXm7BCuVAnjZcIGfnQJQ = new _$$Lambda$E_XesXLNXm7BCuVAnjZcIGfnQJQ(textClassificationHelper1);
      } else {
        textClassificationHelper1 = this.mTextClassificationHelper;
        Objects.requireNonNull(textClassificationHelper1);
        _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI = new _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI(textClassificationHelper1);
      } 
      if (this.mSmartSelectSprite != null) {
        _$$Lambda$SelectionActionModeHelper$l1f1_V5lw6noQxI_3u11qF753Iw _$$Lambda$SelectionActionModeHelper$l1f1_V5lw6noQxI_3u11qF753Iw = new _$$Lambda$SelectionActionModeHelper$l1f1_V5lw6noQxI_3u11qF753Iw(this);
      } else {
        _$$Lambda$SelectionActionModeHelper$CcJ0IF8nDFsmkuaqvOxFqYGazzY = new _$$Lambda$SelectionActionModeHelper$CcJ0IF8nDFsmkuaqvOxFqYGazzY(this);
      } 
      TextClassificationHelper textClassificationHelper2 = this.mTextClassificationHelper;
      Objects.requireNonNull(textClassificationHelper2);
      TextClassificationAsyncTask textClassificationAsyncTask = new TextClassificationAsyncTask(textView1, j, _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI, _$$Lambda$SelectionActionModeHelper$CcJ0IF8nDFsmkuaqvOxFqYGazzY, new _$$Lambda$etfJkiCJnT2dqM2O4M2TCm9i_oA(textClassificationHelper2));
      this.mTextClassificationAsyncTask = textClassificationAsyncTask.execute((Object[])new Void[0]);
    } 
  }
  
  public void startLinkActionModeAsync(int paramInt1, int paramInt2) {
    int[] arrayOfInt = sortSelctionIndices(paramInt1, paramInt2);
    this.mSelectionTracker.onOriginalSelection(getText(this.mTextView), arrayOfInt[0], arrayOfInt[1], true);
    cancelAsyncTask();
    if (skipTextClassification()) {
      startLinkActionMode(null);
    } else {
      resetTextClassificationHelper(arrayOfInt[0], arrayOfInt[1]);
      TextView textView = this.mTextView;
      TextClassificationHelper textClassificationHelper1 = this.mTextClassificationHelper;
      paramInt1 = textClassificationHelper1.getTimeoutDuration();
      textClassificationHelper1 = this.mTextClassificationHelper;
      Objects.requireNonNull(textClassificationHelper1);
      _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI = new _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI(textClassificationHelper1);
      _$$Lambda$SelectionActionModeHelper$WnFw1_gP20c3ltvTN6OPqQ5XUns _$$Lambda$SelectionActionModeHelper$WnFw1_gP20c3ltvTN6OPqQ5XUns = new _$$Lambda$SelectionActionModeHelper$WnFw1_gP20c3ltvTN6OPqQ5XUns(this);
      TextClassificationHelper textClassificationHelper2 = this.mTextClassificationHelper;
      Objects.requireNonNull(textClassificationHelper2);
      TextClassificationAsyncTask textClassificationAsyncTask = new TextClassificationAsyncTask(textView, paramInt1, _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI, _$$Lambda$SelectionActionModeHelper$WnFw1_gP20c3ltvTN6OPqQ5XUns, new _$$Lambda$etfJkiCJnT2dqM2O4M2TCm9i_oA(textClassificationHelper2));
      this.mTextClassificationAsyncTask = textClassificationAsyncTask.execute((Object[])new Void[0]);
    } 
  }
  
  public void invalidateActionModeAsync() {
    cancelAsyncTask();
    if (skipTextClassification()) {
      invalidateActionMode(null);
    } else {
      resetTextClassificationHelper();
      TextView textView = this.mTextView;
      TextClassificationHelper textClassificationHelper1 = this.mTextClassificationHelper;
      int i = textClassificationHelper1.getTimeoutDuration();
      textClassificationHelper1 = this.mTextClassificationHelper;
      Objects.requireNonNull(textClassificationHelper1);
      _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI = new _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI(textClassificationHelper1);
      _$$Lambda$SelectionActionModeHelper$Lwzg10CkEpNBaAXBpjnWEpIlTzQ _$$Lambda$SelectionActionModeHelper$Lwzg10CkEpNBaAXBpjnWEpIlTzQ = new _$$Lambda$SelectionActionModeHelper$Lwzg10CkEpNBaAXBpjnWEpIlTzQ(this);
      TextClassificationHelper textClassificationHelper2 = this.mTextClassificationHelper;
      Objects.requireNonNull(textClassificationHelper2);
      TextClassificationAsyncTask textClassificationAsyncTask = new TextClassificationAsyncTask(textView, i, _$$Lambda$aOGBsMC_jnvTDjezYLRtz35nAPI, _$$Lambda$SelectionActionModeHelper$Lwzg10CkEpNBaAXBpjnWEpIlTzQ, new _$$Lambda$etfJkiCJnT2dqM2O4M2TCm9i_oA(textClassificationHelper2));
      this.mTextClassificationAsyncTask = textClassificationAsyncTask.execute((Object[])new Void[0]);
    } 
  }
  
  public void onSelectionAction(int paramInt, String paramString) {
    int[] arrayOfInt = sortSelctionIndicesFromTextView(this.mTextView);
    SelectionTracker selectionTracker = this.mSelectionTracker;
    int i = arrayOfInt[0], j = arrayOfInt[1];
    paramInt = getActionType(paramInt);
    TextClassification textClassification = this.mTextClassification;
    selectionTracker.onSelectionAction(i, j, paramInt, paramString, textClassification);
  }
  
  public void onSelectionDrag() {
    int[] arrayOfInt = sortSelctionIndicesFromTextView(this.mTextView);
    this.mSelectionTracker.onSelectionAction(arrayOfInt[0], arrayOfInt[1], 106, null, this.mTextClassification);
  }
  
  public void onTextChanged(int paramInt1, int paramInt2) {
    int[] arrayOfInt = sortSelctionIndices(paramInt1, paramInt2);
    this.mSelectionTracker.onTextChanged(arrayOfInt[0], arrayOfInt[1], this.mTextClassification);
  }
  
  public boolean resetSelection(int paramInt) {
    if (this.mSelectionTracker.resetSelection(paramInt, this.mEditor)) {
      invalidateActionModeAsync();
      return true;
    } 
    return false;
  }
  
  public TextClassification getTextClassification() {
    return this.mTextClassification;
  }
  
  public void onDestroyActionMode() {
    cancelSmartSelectAnimation();
    this.mSelectionTracker.onSelectionDestroyed();
    cancelAsyncTask();
  }
  
  public void onDraw(Canvas paramCanvas) {
    if (isDrawingHighlight()) {
      SmartSelectSprite smartSelectSprite = this.mSmartSelectSprite;
      if (smartSelectSprite != null)
        smartSelectSprite.draw(paramCanvas); 
    } 
  }
  
  public boolean isDrawingHighlight() {
    boolean bool;
    SmartSelectSprite smartSelectSprite = this.mSmartSelectSprite;
    if (smartSelectSprite != null && smartSelectSprite.isAnimationActive()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private TextClassificationConstants getTextClassificationSettings() {
    return TextClassificationManager.getSettings(this.mTextView.getContext());
  }
  
  private void cancelAsyncTask() {
    AsyncTask asyncTask = this.mTextClassificationAsyncTask;
    if (asyncTask != null) {
      asyncTask.cancel(true);
      this.mTextClassificationAsyncTask = null;
    } 
    this.mTextClassification = null;
  }
  
  private boolean skipTextClassification() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTextView : Landroid/widget/TextView;
    //   4: invokevirtual usesNoOpTextClassifier : ()Z
    //   7: istore_1
    //   8: aload_0
    //   9: getfield mTextView : Landroid/widget/TextView;
    //   12: invokevirtual getSelectionEnd : ()I
    //   15: istore_2
    //   16: aload_0
    //   17: getfield mTextView : Landroid/widget/TextView;
    //   20: invokevirtual getSelectionStart : ()I
    //   23: istore_3
    //   24: iconst_1
    //   25: istore #4
    //   27: iload_2
    //   28: iload_3
    //   29: if_icmpne -> 37
    //   32: iconst_1
    //   33: istore_3
    //   34: goto -> 39
    //   37: iconst_0
    //   38: istore_3
    //   39: aload_0
    //   40: getfield mTextView : Landroid/widget/TextView;
    //   43: invokevirtual hasPasswordTransformationMethod : ()Z
    //   46: ifne -> 74
    //   49: aload_0
    //   50: getfield mTextView : Landroid/widget/TextView;
    //   53: astore #5
    //   55: aload #5
    //   57: invokevirtual getInputType : ()I
    //   60: invokestatic isPasswordInputType : (I)Z
    //   63: ifeq -> 69
    //   66: goto -> 74
    //   69: iconst_0
    //   70: istore_2
    //   71: goto -> 76
    //   74: iconst_1
    //   75: istore_2
    //   76: iload #4
    //   78: istore #6
    //   80: iload_1
    //   81: ifne -> 106
    //   84: iload #4
    //   86: istore #6
    //   88: iload_3
    //   89: ifne -> 106
    //   92: iload_2
    //   93: ifeq -> 103
    //   96: iload #4
    //   98: istore #6
    //   100: goto -> 106
    //   103: iconst_0
    //   104: istore #6
    //   106: iload #6
    //   108: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #266	-> 0
    //   #268	-> 8
    //   #270	-> 39
    //   #271	-> 55
    //   #272	-> 76
  }
  
  private void startLinkActionMode(SelectionResult paramSelectionResult) {
    startActionMode(2, paramSelectionResult);
  }
  
  private void startSelectionActionMode(SelectionResult paramSelectionResult) {
    startActionMode(0, paramSelectionResult);
  }
  
  private void startActionMode(int paramInt, SelectionResult paramSelectionResult) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTextView : Landroid/widget/TextView;
    //   4: invokestatic getText : (Landroid/widget/TextView;)Ljava/lang/CharSequence;
    //   7: astore_3
    //   8: aload_2
    //   9: ifnull -> 86
    //   12: aload_3
    //   13: instanceof android/text/Spannable
    //   16: ifeq -> 86
    //   19: aload_0
    //   20: getfield mTextView : Landroid/widget/TextView;
    //   23: astore #4
    //   25: aload #4
    //   27: invokevirtual isTextSelectable : ()Z
    //   30: ifne -> 43
    //   33: aload_0
    //   34: getfield mTextView : Landroid/widget/TextView;
    //   37: invokevirtual isTextEditable : ()Z
    //   40: ifeq -> 86
    //   43: aload_0
    //   44: invokespecial getTextClassificationSettings : ()Landroid/view/textclassifier/TextClassificationConstants;
    //   47: invokevirtual isModelDarkLaunchEnabled : ()Z
    //   50: ifne -> 75
    //   53: aload_3
    //   54: checkcast android/text/Spannable
    //   57: aload_2
    //   58: invokestatic access$000 : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)I
    //   61: aload_2
    //   62: invokestatic access$100 : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)I
    //   65: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   68: aload_0
    //   69: getfield mTextView : Landroid/widget/TextView;
    //   72: invokevirtual invalidate : ()V
    //   75: aload_0
    //   76: aload_2
    //   77: invokestatic access$200 : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)Landroid/view/textclassifier/TextClassification;
    //   80: putfield mTextClassification : Landroid/view/textclassifier/TextClassification;
    //   83: goto -> 111
    //   86: aload_2
    //   87: ifnull -> 106
    //   90: iload_1
    //   91: iconst_2
    //   92: if_icmpne -> 106
    //   95: aload_0
    //   96: aload_2
    //   97: invokestatic access$200 : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)Landroid/view/textclassifier/TextClassification;
    //   100: putfield mTextClassification : Landroid/view/textclassifier/TextClassification;
    //   103: goto -> 111
    //   106: aload_0
    //   107: aconst_null
    //   108: putfield mTextClassification : Landroid/view/textclassifier/TextClassification;
    //   111: aload_0
    //   112: getfield mEditor : Landroid/widget/Editor;
    //   115: iload_1
    //   116: invokevirtual startActionModeInternal : (I)Z
    //   119: ifeq -> 197
    //   122: aload_0
    //   123: getfield mEditor : Landroid/widget/Editor;
    //   126: invokevirtual getSelectionController : ()Landroid/widget/Editor$SelectionModifierCursorController;
    //   129: astore_3
    //   130: aload_3
    //   131: ifnull -> 162
    //   134: aload_0
    //   135: getfield mTextView : Landroid/widget/TextView;
    //   138: astore #4
    //   140: aload #4
    //   142: invokevirtual isTextSelectable : ()Z
    //   145: ifne -> 158
    //   148: aload_0
    //   149: getfield mTextView : Landroid/widget/TextView;
    //   152: invokevirtual isTextEditable : ()Z
    //   155: ifeq -> 162
    //   158: aload_3
    //   159: invokevirtual show : ()V
    //   162: aload_2
    //   163: ifnull -> 197
    //   166: iload_1
    //   167: ifeq -> 189
    //   170: iload_1
    //   171: iconst_2
    //   172: if_icmpeq -> 178
    //   175: goto -> 197
    //   178: aload_0
    //   179: getfield mSelectionTracker : Landroid/widget/SelectionActionModeHelper$SelectionTracker;
    //   182: aload_2
    //   183: invokevirtual onLinkSelected : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)V
    //   186: goto -> 197
    //   189: aload_0
    //   190: getfield mSelectionTracker : Landroid/widget/SelectionActionModeHelper$SelectionTracker;
    //   193: aload_2
    //   194: invokevirtual onSmartSelection : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)V
    //   197: aload_0
    //   198: getfield mEditor : Landroid/widget/Editor;
    //   201: iconst_0
    //   202: invokevirtual setRestartActionModeOnNextRefresh : (Z)V
    //   205: aload_0
    //   206: aconst_null
    //   207: putfield mTextClassificationAsyncTask : Landroid/os/AsyncTask;
    //   210: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #285	-> 0
    //   #286	-> 8
    //   #287	-> 25
    //   #289	-> 43
    //   #290	-> 53
    //   #291	-> 68
    //   #293	-> 75
    //   #294	-> 86
    //   #295	-> 95
    //   #297	-> 106
    //   #299	-> 111
    //   #300	-> 122
    //   #301	-> 130
    //   #302	-> 140
    //   #303	-> 158
    //   #305	-> 162
    //   #306	-> 166
    //   #311	-> 178
    //   #312	-> 186
    //   #308	-> 189
    //   #318	-> 197
    //   #319	-> 205
    //   #320	-> 210
  }
  
  private void startSelectionActionModeWithSmartSelectAnimation(SelectionResult paramSelectionResult) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTextView : Landroid/widget/TextView;
    //   4: invokevirtual getLayout : ()Landroid/text/Layout;
    //   7: astore_2
    //   8: new android/widget/_$$Lambda$SelectionActionModeHelper$xdBRwQcbRdz8duQr0RBo4YKAnOA
    //   11: dup
    //   12: aload_0
    //   13: aload_1
    //   14: invokespecial <init> : (Landroid/widget/SelectionActionModeHelper;Landroid/widget/SelectionActionModeHelper$SelectionResult;)V
    //   17: astore_3
    //   18: aload_0
    //   19: getfield mTextView : Landroid/widget/TextView;
    //   22: invokestatic sortSelctionIndicesFromTextView : (Landroid/widget/TextView;)[I
    //   25: astore #4
    //   27: iconst_1
    //   28: istore #5
    //   30: aload_1
    //   31: ifnull -> 67
    //   34: aload #4
    //   36: iconst_0
    //   37: iaload
    //   38: istore #6
    //   40: iload #6
    //   42: aload_1
    //   43: invokestatic access$000 : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)I
    //   46: if_icmpne -> 64
    //   49: aload #4
    //   51: iconst_1
    //   52: iaload
    //   53: istore #6
    //   55: iload #6
    //   57: aload_1
    //   58: invokestatic access$100 : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)I
    //   61: if_icmpeq -> 67
    //   64: goto -> 70
    //   67: iconst_0
    //   68: istore #5
    //   70: iload #5
    //   72: ifne -> 82
    //   75: aload_3
    //   76: invokeinterface run : ()V
    //   81: return
    //   82: aload_0
    //   83: aload_2
    //   84: aload_1
    //   85: invokestatic access$000 : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)I
    //   88: aload_1
    //   89: invokestatic access$100 : (Landroid/widget/SelectionActionModeHelper$SelectionResult;)I
    //   92: invokespecial convertSelectionToRectangles : (Landroid/text/Layout;II)Ljava/util/List;
    //   95: astore_1
    //   96: aload_0
    //   97: getfield mEditor : Landroid/widget/Editor;
    //   100: astore #4
    //   102: aload #4
    //   104: invokevirtual getLastUpPositionX : ()F
    //   107: fstore #7
    //   109: aload_0
    //   110: getfield mEditor : Landroid/widget/Editor;
    //   113: astore #4
    //   115: new android/graphics/PointF
    //   118: dup
    //   119: fload #7
    //   121: aload #4
    //   123: invokevirtual getLastUpPositionY : ()F
    //   126: invokespecial <init> : (FF)V
    //   129: astore_2
    //   130: getstatic android/widget/_$$Lambda$ChL7kntlZCrPaPVdRfaSzGdk1JU.INSTANCE : Landroid/widget/-$$Lambda$ChL7kntlZCrPaPVdRfaSzGdk1JU;
    //   133: astore #4
    //   135: aload_2
    //   136: aload_1
    //   137: aload #4
    //   139: invokestatic movePointInsideNearestRectangle : (Landroid/graphics/PointF;Ljava/util/List;Ljava/util/function/Function;)Landroid/graphics/PointF;
    //   142: astore #4
    //   144: aload_0
    //   145: getfield mSmartSelectSprite : Landroid/widget/SmartSelectSprite;
    //   148: aload #4
    //   150: aload_1
    //   151: aload_3
    //   152: invokevirtual startAnimation : (Landroid/graphics/PointF;Ljava/util/List;Ljava/lang/Runnable;)V
    //   155: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #324	-> 0
    //   #326	-> 8
    //   #337	-> 18
    //   #338	-> 27
    //   #339	-> 40
    //   #340	-> 55
    //   #341	-> 70
    //   #342	-> 75
    //   #343	-> 81
    //   #346	-> 82
    //   #347	-> 82
    //   #349	-> 96
    //   #350	-> 102
    //   #351	-> 115
    //   #353	-> 130
    //   #354	-> 135
    //   #357	-> 144
    //   #361	-> 155
  }
  
  private List<SmartSelectSprite.RectangleWithTextSelectionLayout> convertSelectionToRectangles(Layout paramLayout, int paramInt1, int paramInt2) {
    ArrayList<SmartSelectSprite.RectangleWithTextSelectionLayout> arrayList = new ArrayList();
    _$$Lambda$SelectionActionModeHelper$cMbIRcH_yFkksR3CQmROa0_hmgM _$$Lambda$SelectionActionModeHelper$cMbIRcH_yFkksR3CQmROa0_hmgM = new _$$Lambda$SelectionActionModeHelper$cMbIRcH_yFkksR3CQmROa0_hmgM(arrayList);
    if (paramLayout != null)
      paramLayout.getSelection(paramInt1, paramInt2, _$$Lambda$SelectionActionModeHelper$cMbIRcH_yFkksR3CQmROa0_hmgM); 
    arrayList.sort(Comparator.comparing((Function<?, ? extends RectF>)_$$Lambda$ChL7kntlZCrPaPVdRfaSzGdk1JU.INSTANCE, SmartSelectSprite.RECTANGLE_COMPARATOR));
    return arrayList;
  }
  
  public static <T> void mergeRectangleIntoList(List<T> paramList, RectF paramRectF, Function<T, RectF> paramFunction, Function<RectF, T> paramFunction1) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual isEmpty : ()Z
    //   4: ifeq -> 8
    //   7: return
    //   8: aload_0
    //   9: invokeinterface size : ()I
    //   14: istore #4
    //   16: iconst_0
    //   17: istore #5
    //   19: iload #5
    //   21: iload #4
    //   23: if_icmpge -> 193
    //   26: aload_2
    //   27: aload_0
    //   28: iload #5
    //   30: invokeinterface get : (I)Ljava/lang/Object;
    //   35: invokeinterface apply : (Ljava/lang/Object;)Ljava/lang/Object;
    //   40: checkcast android/graphics/RectF
    //   43: astore #6
    //   45: aload #6
    //   47: aload_1
    //   48: invokevirtual contains : (Landroid/graphics/RectF;)Z
    //   51: ifeq -> 55
    //   54: return
    //   55: aload_1
    //   56: aload #6
    //   58: invokevirtual contains : (Landroid/graphics/RectF;)Z
    //   61: ifeq -> 72
    //   64: aload #6
    //   66: invokevirtual setEmpty : ()V
    //   69: goto -> 187
    //   72: aload_1
    //   73: getfield left : F
    //   76: fstore #7
    //   78: aload #6
    //   80: getfield right : F
    //   83: fstore #8
    //   85: iconst_0
    //   86: istore #9
    //   88: fload #7
    //   90: fload #8
    //   92: fcmpl
    //   93: ifeq -> 118
    //   96: aload_1
    //   97: getfield right : F
    //   100: aload #6
    //   102: getfield left : F
    //   105: fcmpl
    //   106: ifne -> 112
    //   109: goto -> 118
    //   112: iconst_0
    //   113: istore #10
    //   115: goto -> 121
    //   118: iconst_1
    //   119: istore #10
    //   121: aload_1
    //   122: getfield top : F
    //   125: aload #6
    //   127: getfield top : F
    //   130: fcmpl
    //   131: ifne -> 167
    //   134: aload_1
    //   135: getfield bottom : F
    //   138: aload #6
    //   140: getfield bottom : F
    //   143: fcmpl
    //   144: ifne -> 167
    //   147: aload_1
    //   148: aload #6
    //   150: invokestatic intersects : (Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    //   153: ifne -> 161
    //   156: iload #10
    //   158: ifeq -> 167
    //   161: iconst_1
    //   162: istore #10
    //   164: goto -> 171
    //   167: iload #9
    //   169: istore #10
    //   171: iload #10
    //   173: ifeq -> 187
    //   176: aload_1
    //   177: aload #6
    //   179: invokevirtual union : (Landroid/graphics/RectF;)V
    //   182: aload #6
    //   184: invokevirtual setEmpty : ()V
    //   187: iinc #5, 1
    //   190: goto -> 19
    //   193: iload #4
    //   195: iconst_1
    //   196: isub
    //   197: istore #5
    //   199: iload #5
    //   201: iflt -> 246
    //   204: aload_2
    //   205: aload_0
    //   206: iload #5
    //   208: invokeinterface get : (I)Ljava/lang/Object;
    //   213: invokeinterface apply : (Ljava/lang/Object;)Ljava/lang/Object;
    //   218: checkcast android/graphics/RectF
    //   221: astore #6
    //   223: aload #6
    //   225: invokevirtual isEmpty : ()Z
    //   228: ifeq -> 240
    //   231: aload_0
    //   232: iload #5
    //   234: invokeinterface remove : (I)Ljava/lang/Object;
    //   239: pop
    //   240: iinc #5, -1
    //   243: goto -> 199
    //   246: aload_0
    //   247: aload_3
    //   248: aload_1
    //   249: invokeinterface apply : (Ljava/lang/Object;)Ljava/lang/Object;
    //   254: invokeinterface add : (Ljava/lang/Object;)Z
    //   259: pop
    //   260: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #413	-> 0
    //   #414	-> 7
    //   #417	-> 8
    //   #418	-> 16
    //   #419	-> 26
    //   #420	-> 45
    //   #421	-> 54
    //   #423	-> 55
    //   #424	-> 64
    //   #425	-> 69
    //   #428	-> 72
    //   #430	-> 121
    //   #432	-> 147
    //   #435	-> 171
    //   #436	-> 176
    //   #437	-> 182
    //   #418	-> 187
    //   #441	-> 193
    //   #442	-> 204
    //   #443	-> 223
    //   #444	-> 231
    //   #441	-> 240
    //   #448	-> 246
    //   #449	-> 260
  }
  
  public static <T> PointF movePointInsideNearestRectangle(PointF paramPointF, List<T> paramList, Function<T, RectF> paramFunction) {
    float f1 = -1.0F;
    float f2 = -1.0F;
    double d = Double.MAX_VALUE;
    int i = paramList.size();
    for (byte b = 0; b < i; b++, d = d1) {
      float f4;
      RectF rectF = paramFunction.apply(paramList.get(b));
      float f3 = rectF.centerY();
      if (paramPointF.x > rectF.right) {
        f4 = rectF.right;
      } else if (paramPointF.x < rectF.left) {
        f4 = rectF.left;
      } else {
        f4 = paramPointF.x;
      } 
      double d1 = Math.pow((paramPointF.x - f4), 2.0D), d2 = (paramPointF.y - f3);
      d2 = d1 + Math.pow(d2, 2.0D);
      d1 = d;
      if (d2 < d) {
        f2 = f3;
        d1 = d2;
        f1 = f4;
      } 
    } 
    return new PointF(f1, f2);
  }
  
  private void invalidateActionMode(SelectionResult paramSelectionResult) {
    cancelSmartSelectAnimation();
    if (paramSelectionResult != null) {
      TextClassification textClassification = paramSelectionResult.mClassification;
    } else {
      paramSelectionResult = null;
    } 
    this.mTextClassification = (TextClassification)paramSelectionResult;
    ActionMode actionMode = this.mEditor.getTextActionMode();
    if (actionMode != null)
      actionMode.invalidate(); 
    int[] arrayOfInt = sortSelctionIndicesFromTextView(this.mTextView);
    this.mSelectionTracker.onSelectionUpdated(arrayOfInt[0], arrayOfInt[1], this.mTextClassification);
    this.mTextClassificationAsyncTask = null;
  }
  
  private void resetTextClassificationHelper(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: iload_1
    //   1: iflt -> 12
    //   4: iload_1
    //   5: istore_3
    //   6: iload_2
    //   7: istore_1
    //   8: iload_2
    //   9: ifge -> 31
    //   12: aload_0
    //   13: getfield mTextView : Landroid/widget/TextView;
    //   16: invokestatic sortSelctionIndicesFromTextView : (Landroid/widget/TextView;)[I
    //   19: astore #4
    //   21: aload #4
    //   23: iconst_0
    //   24: iaload
    //   25: istore_3
    //   26: aload #4
    //   28: iconst_1
    //   29: iaload
    //   30: istore_1
    //   31: aload_0
    //   32: getfield mTextClassificationHelper : Landroid/widget/SelectionActionModeHelper$TextClassificationHelper;
    //   35: astore #4
    //   37: aload_0
    //   38: getfield mTextView : Landroid/widget/TextView;
    //   41: astore #5
    //   43: aload #5
    //   45: invokestatic requireNonNull : (Ljava/lang/Object;)Ljava/lang/Object;
    //   48: pop
    //   49: new android/widget/_$$Lambda$GgAIoNUUH8pNRbtcqGeR1oLuEXw
    //   52: dup
    //   53: aload #5
    //   55: invokespecial <init> : (Landroid/widget/TextView;)V
    //   58: astore #5
    //   60: aload_0
    //   61: getfield mTextView : Landroid/widget/TextView;
    //   64: astore #6
    //   66: aload #6
    //   68: invokestatic getText : (Landroid/widget/TextView;)Ljava/lang/CharSequence;
    //   71: astore #6
    //   73: aload_0
    //   74: getfield mTextView : Landroid/widget/TextView;
    //   77: astore #7
    //   79: aload #7
    //   81: invokevirtual getTextLocales : ()Landroid/os/LocaleList;
    //   84: astore #7
    //   86: aload #4
    //   88: aload #5
    //   90: aload #6
    //   92: iload_3
    //   93: iload_1
    //   94: aload #7
    //   96: invokevirtual init : (Ljava/util/function/Supplier;Ljava/lang/CharSequence;IILandroid/os/LocaleList;)V
    //   99: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #501	-> 0
    //   #503	-> 12
    //   #504	-> 21
    //   #505	-> 26
    //   #507	-> 31
    //   #508	-> 43
    //   #509	-> 66
    //   #511	-> 79
    //   #507	-> 86
    //   #512	-> 99
  }
  
  private void resetTextClassificationHelper() {
    resetTextClassificationHelper(-1, -1);
  }
  
  private void cancelSmartSelectAnimation() {
    SmartSelectSprite smartSelectSprite = this.mSmartSelectSprite;
    if (smartSelectSprite != null)
      smartSelectSprite.cancelAnimation(); 
  }
  
  private static final class SelectionTracker {
    private boolean mAllowReset;
    
    private final LogAbandonRunnable mDelayedLogAbandon = new LogAbandonRunnable();
    
    private SelectionActionModeHelper.SelectionMetricsLogger mLogger;
    
    private int mOriginalEnd;
    
    private int mOriginalStart;
    
    private int mSelectionEnd;
    
    private int mSelectionStart;
    
    private final TextView mTextView;
    
    SelectionTracker(TextView param1TextView) {
      Objects.requireNonNull(param1TextView);
      this.mTextView = param1TextView;
      this.mLogger = new SelectionActionModeHelper.SelectionMetricsLogger(param1TextView);
    }
    
    public void onOriginalSelection(CharSequence param1CharSequence, int param1Int1, int param1Int2, boolean param1Boolean) {
      this.mDelayedLogAbandon.flush();
      this.mSelectionStart = param1Int1;
      this.mOriginalStart = param1Int1;
      this.mSelectionEnd = param1Int2;
      this.mOriginalEnd = param1Int2;
      this.mAllowReset = false;
      maybeInvalidateLogger();
      SelectionActionModeHelper.SelectionMetricsLogger selectionMetricsLogger = this.mLogger;
      TextView textView1 = this.mTextView;
      TextClassifier textClassifier = textView1.getTextClassificationSession();
      TextView textView2 = this.mTextView;
      TextClassificationContext textClassificationContext = textView2.getTextClassificationContext();
      if (param1Boolean) {
        param1Int2 = 2;
      } else {
        param1Int2 = 1;
      } 
      selectionMetricsLogger.logSelectionStarted(textClassifier, textClassificationContext, param1CharSequence, param1Int1, param1Int2);
    }
    
    public void onSmartSelection(SelectionActionModeHelper.SelectionResult param1SelectionResult) {
      onClassifiedSelection(param1SelectionResult);
      SelectionActionModeHelper.SelectionMetricsLogger selectionMetricsLogger = this.mLogger;
      int i = param1SelectionResult.mStart, j = param1SelectionResult.mEnd;
      TextClassification textClassification = param1SelectionResult.mClassification;
      TextSelection textSelection = param1SelectionResult.mSelection;
      selectionMetricsLogger.logSelectionModified(i, j, textClassification, textSelection);
    }
    
    public void onLinkSelected(SelectionActionModeHelper.SelectionResult param1SelectionResult) {
      onClassifiedSelection(param1SelectionResult);
    }
    
    private void onClassifiedSelection(SelectionActionModeHelper.SelectionResult param1SelectionResult) {
      if (isSelectionStarted()) {
        boolean bool;
        this.mSelectionStart = param1SelectionResult.mStart;
        int i = param1SelectionResult.mEnd;
        if (this.mSelectionStart != this.mOriginalStart || i != this.mOriginalEnd) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mAllowReset = bool;
      } 
    }
    
    public void onSelectionUpdated(int param1Int1, int param1Int2, TextClassification param1TextClassification) {
      if (isSelectionStarted()) {
        this.mSelectionStart = param1Int1;
        this.mSelectionEnd = param1Int2;
        this.mAllowReset = false;
        this.mLogger.logSelectionModified(param1Int1, param1Int2, param1TextClassification, null);
      } 
    }
    
    public void onSelectionDestroyed() {
      this.mAllowReset = false;
      this.mDelayedLogAbandon.schedule(100);
    }
    
    public void onSelectionAction(int param1Int1, int param1Int2, int param1Int3, String param1String, TextClassification param1TextClassification) {
      if (isSelectionStarted()) {
        this.mAllowReset = false;
        this.mLogger.logSelectionAction(param1Int1, param1Int2, param1Int3, param1String, param1TextClassification);
      } 
    }
    
    public boolean resetSelection(int param1Int, Editor param1Editor) {
      TextView textView = param1Editor.getTextView();
      if (isSelectionStarted() && this.mAllowReset && param1Int >= this.mSelectionStart && param1Int <= this.mSelectionEnd)
        if (SelectionActionModeHelper.getText(textView) instanceof android.text.Spannable) {
          this.mAllowReset = false;
          boolean bool = param1Editor.selectCurrentWord();
          if (bool) {
            int[] arrayOfInt = SelectionActionModeHelper.sortSelctionIndicesFromTextView(textView);
            this.mSelectionStart = arrayOfInt[0];
            this.mSelectionEnd = arrayOfInt[1];
            this.mLogger.logSelectionAction(arrayOfInt[0], arrayOfInt[1], 201, null, null);
          } 
          return bool;
        }  
      return false;
    }
    
    public void onTextChanged(int param1Int1, int param1Int2, TextClassification param1TextClassification) {
      if (isSelectionStarted() && param1Int1 == this.mSelectionStart && param1Int2 == this.mSelectionEnd)
        onSelectionAction(param1Int1, param1Int2, 100, null, param1TextClassification); 
    }
    
    private void maybeInvalidateLogger() {
      if (this.mLogger.isEditTextLogger() != this.mTextView.isTextEditable())
        this.mLogger = new SelectionActionModeHelper.SelectionMetricsLogger(this.mTextView); 
    }
    
    private boolean isSelectionStarted() {
      int i = this.mSelectionStart;
      if (i >= 0) {
        int j = this.mSelectionEnd;
        if (j >= 0 && i != j)
          return true; 
      } 
      return false;
    }
    
    private final class LogAbandonRunnable implements Runnable {
      private boolean mIsPending;
      
      final SelectionActionModeHelper.SelectionTracker this$0;
      
      private LogAbandonRunnable() {}
      
      void schedule(int param2Int) {
        if (this.mIsPending) {
          Log.e("SelectActionModeHelper", "Force flushing abandon due to new scheduling request");
          flush();
        } 
        this.mIsPending = true;
        SelectionActionModeHelper.SelectionTracker.this.mTextView.postDelayed(this, param2Int);
      }
      
      void flush() {
        SelectionActionModeHelper.SelectionTracker.this.mTextView.removeCallbacks(this);
        run();
      }
      
      public void run() {
        if (this.mIsPending) {
          SelectionActionModeHelper.SelectionMetricsLogger selectionMetricsLogger = SelectionActionModeHelper.SelectionTracker.this.mLogger;
          SelectionActionModeHelper.SelectionTracker selectionTracker = SelectionActionModeHelper.SelectionTracker.this;
          int i = selectionTracker.mSelectionStart, j = SelectionActionModeHelper.SelectionTracker.this.mSelectionEnd;
          selectionMetricsLogger.logSelectionAction(i, j, 107, null, null);
          selectionTracker = SelectionActionModeHelper.SelectionTracker.this;
          SelectionActionModeHelper.SelectionTracker.access$802(selectionTracker, SelectionActionModeHelper.SelectionTracker.access$902(selectionTracker, -1));
          SelectionActionModeHelper.SelectionTracker.this.mLogger.endTextClassificationSession();
          this.mIsPending = false;
        } 
      }
    }
  }
  
  private final class LogAbandonRunnable implements Runnable {
    private boolean mIsPending;
    
    final SelectionActionModeHelper.SelectionTracker this$0;
    
    private LogAbandonRunnable() {}
    
    void schedule(int param1Int) {
      if (this.mIsPending) {
        Log.e("SelectActionModeHelper", "Force flushing abandon due to new scheduling request");
        flush();
      } 
      this.mIsPending = true;
      SelectionActionModeHelper.SelectionTracker.this.mTextView.postDelayed(this, param1Int);
    }
    
    void flush() {
      SelectionActionModeHelper.SelectionTracker.this.mTextView.removeCallbacks(this);
      run();
    }
    
    public void run() {
      if (this.mIsPending) {
        SelectionActionModeHelper.SelectionMetricsLogger selectionMetricsLogger = SelectionActionModeHelper.SelectionTracker.this.mLogger;
        SelectionActionModeHelper.SelectionTracker selectionTracker = SelectionActionModeHelper.SelectionTracker.this;
        int i = selectionTracker.mSelectionStart, j = SelectionActionModeHelper.SelectionTracker.this.mSelectionEnd;
        selectionMetricsLogger.logSelectionAction(i, j, 107, null, null);
        selectionTracker = SelectionActionModeHelper.SelectionTracker.this;
        SelectionActionModeHelper.SelectionTracker.access$802(selectionTracker, SelectionActionModeHelper.SelectionTracker.access$902(selectionTracker, -1));
        SelectionActionModeHelper.SelectionTracker.this.mLogger.endTextClassificationSession();
        this.mIsPending = false;
      } 
    }
  }
  
  private static final class SelectionMetricsLogger {
    private static final String LOG_TAG = "SelectionMetricsLogger";
    
    private static final Pattern PATTERN_WHITESPACE = Pattern.compile("\\s+");
    
    private TextClassificationContext mClassificationContext;
    
    private TextClassifier mClassificationSession;
    
    private final boolean mEditTextLogger;
    
    private int mStartIndex;
    
    private String mText;
    
    private final BreakIterator mTokenIterator;
    
    private TextClassifierEvent mTranslateClickEvent;
    
    private TextClassifierEvent mTranslateViewEvent;
    
    SelectionMetricsLogger(TextView param1TextView) {
      Objects.requireNonNull(param1TextView);
      this.mEditTextLogger = param1TextView.isTextEditable();
      this.mTokenIterator = BreakIterator.getWordInstance(param1TextView.getTextLocale());
    }
    
    public void logSelectionStarted(TextClassifier param1TextClassifier, TextClassificationContext param1TextClassificationContext, CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      try {
        Objects.requireNonNull(param1CharSequence);
        Preconditions.checkArgumentInRange(param1Int1, 0, param1CharSequence.length(), "index");
        if (this.mText == null || !this.mText.contentEquals(param1CharSequence))
          this.mText = param1CharSequence.toString(); 
        this.mTokenIterator.setText(this.mText);
        this.mStartIndex = param1Int1;
        this.mClassificationSession = param1TextClassifier;
        this.mClassificationContext = param1TextClassificationContext;
        if (hasActiveClassificationSession()) {
          TextClassifier textClassifier = this.mClassificationSession;
          SelectionEvent selectionEvent = SelectionEvent.createSelectionStartedEvent(param1Int2, 0);
          textClassifier.onSelectionEvent(selectionEvent);
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("");
        stringBuilder.append(exception.getMessage());
        Log.e("SelectionMetricsLogger", stringBuilder.toString(), exception);
      } 
    }
    
    public void logSelectionModified(int param1Int1, int param1Int2, TextClassification param1TextClassification, TextSelection param1TextSelection) {
      try {
        if (hasActiveClassificationSession()) {
          Preconditions.checkArgumentInRange(param1Int1, 0, this.mText.length(), "start");
          Preconditions.checkArgumentInRange(param1Int2, param1Int1, this.mText.length(), "end");
          int[] arrayOfInt = getWordDelta(param1Int1, param1Int2);
          if (param1TextSelection != null) {
            TextClassifier textClassifier = this.mClassificationSession;
            param1Int2 = arrayOfInt[0];
            param1Int1 = arrayOfInt[1];
            SelectionEvent selectionEvent = SelectionEvent.createSelectionModifiedEvent(param1Int2, param1Int1, param1TextSelection);
            textClassifier.onSelectionEvent(selectionEvent);
          } else {
            SelectionEvent selectionEvent;
            if (param1TextClassification != null) {
              TextClassifier textClassifier = this.mClassificationSession;
              param1Int1 = arrayOfInt[0];
              param1Int2 = arrayOfInt[1];
              selectionEvent = SelectionEvent.createSelectionModifiedEvent(param1Int1, param1Int2, param1TextClassification);
              textClassifier.onSelectionEvent(selectionEvent);
            } else {
              TextClassifier textClassifier = this.mClassificationSession;
              SelectionEvent selectionEvent2 = selectionEvent[0], selectionEvent1 = selectionEvent[1];
              selectionEvent = SelectionEvent.createSelectionModifiedEvent(selectionEvent2, selectionEvent1);
              textClassifier.onSelectionEvent(selectionEvent);
            } 
          } 
          maybeGenerateTranslateViewEvent(param1TextClassification);
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("");
        stringBuilder.append(exception.getMessage());
        Log.e("SelectionMetricsLogger", stringBuilder.toString(), exception);
      } 
    }
    
    public void logSelectionAction(int param1Int1, int param1Int2, int param1Int3, String param1String, TextClassification param1TextClassification) {
      try {
        if (hasActiveClassificationSession()) {
          SelectionEvent selectionEvent;
          Preconditions.checkArgumentInRange(param1Int1, 0, this.mText.length(), "start");
          Preconditions.checkArgumentInRange(param1Int2, param1Int1, this.mText.length(), "end");
          int[] arrayOfInt = getWordDelta(param1Int1, param1Int2);
          if (param1TextClassification != null) {
            TextClassifier textClassifier = this.mClassificationSession;
            param1Int1 = arrayOfInt[0];
            param1Int2 = arrayOfInt[1];
            selectionEvent = SelectionEvent.createSelectionActionEvent(param1Int1, param1Int2, param1Int3, param1TextClassification);
            textClassifier.onSelectionEvent(selectionEvent);
          } else {
            TextClassifier textClassifier = this.mClassificationSession;
            SelectionEvent selectionEvent1 = selectionEvent[0], selectionEvent2 = selectionEvent[1];
            selectionEvent = SelectionEvent.createSelectionActionEvent(selectionEvent1, selectionEvent2, param1Int3);
            textClassifier.onSelectionEvent(selectionEvent);
          } 
          maybeGenerateTranslateClickEvent(param1TextClassification, param1String);
          if (SelectionEvent.isTerminal(param1Int3))
            endTextClassificationSession(); 
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("");
        stringBuilder.append(exception.getMessage());
        Log.e("SelectionMetricsLogger", stringBuilder.toString(), exception);
      } 
    }
    
    public boolean isEditTextLogger() {
      return this.mEditTextLogger;
    }
    
    public void endTextClassificationSession() {
      if (hasActiveClassificationSession()) {
        maybeReportTranslateEvents();
        this.mClassificationSession.destroy();
      } 
    }
    
    private boolean hasActiveClassificationSession() {
      boolean bool;
      TextClassifier textClassifier = this.mClassificationSession;
      if (textClassifier != null && !textClassifier.isDestroyed()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private int[] getWordDelta(int param1Int1, int param1Int2) {
      int[] arrayOfInt = new int[2];
      int i = this.mStartIndex;
      if (param1Int1 == i) {
        arrayOfInt[0] = 0;
      } else if (param1Int1 < i) {
        arrayOfInt[0] = -countWordsForward(param1Int1);
      } else {
        arrayOfInt[0] = countWordsBackward(param1Int1);
        if (!this.mTokenIterator.isBoundary(param1Int1)) {
          BreakIterator breakIterator = this.mTokenIterator;
          i = breakIterator.preceding(param1Int1);
          breakIterator = this.mTokenIterator;
          param1Int1 = breakIterator.following(param1Int1);
          if (!isWhitespace(i, param1Int1))
            arrayOfInt[0] = arrayOfInt[0] - 1; 
        } 
      } 
      param1Int1 = this.mStartIndex;
      if (param1Int2 == param1Int1) {
        arrayOfInt[1] = 0;
      } else if (param1Int2 < param1Int1) {
        arrayOfInt[1] = -countWordsForward(param1Int2);
      } else {
        arrayOfInt[1] = countWordsBackward(param1Int2);
      } 
      return arrayOfInt;
    }
    
    private int countWordsBackward(int param1Int) {
      boolean bool;
      if (param1Int >= this.mStartIndex) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      int i = 0;
      int j = param1Int;
      param1Int = i;
      while (j > this.mStartIndex) {
        int k = this.mTokenIterator.preceding(j);
        i = param1Int;
        if (!isWhitespace(k, j))
          i = param1Int + 1; 
        j = k;
        param1Int = i;
      } 
      return param1Int;
    }
    
    private int countWordsForward(int param1Int) {
      boolean bool;
      if (param1Int <= this.mStartIndex) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      int i = 0;
      int j = param1Int;
      while (j < this.mStartIndex) {
        int k = this.mTokenIterator.following(j);
        param1Int = i;
        if (!isWhitespace(j, k))
          param1Int = i + 1; 
        j = k;
        i = param1Int;
      } 
      return i;
    }
    
    private boolean isWhitespace(int param1Int1, int param1Int2) {
      return PATTERN_WHITESPACE.matcher(this.mText.substring(param1Int1, param1Int2)).matches();
    }
    
    private void maybeGenerateTranslateViewEvent(TextClassification param1TextClassification) {
      if (param1TextClassification != null) {
        TextClassifierEvent textClassifierEvent = generateTranslateEvent(6, param1TextClassification, this.mClassificationContext, null);
        if (textClassifierEvent == null)
          textClassifierEvent = this.mTranslateViewEvent; 
        this.mTranslateViewEvent = textClassifierEvent;
      } 
    }
    
    private void maybeGenerateTranslateClickEvent(TextClassification param1TextClassification, String param1String) {
      if (param1TextClassification != null)
        this.mTranslateClickEvent = generateTranslateEvent(13, param1TextClassification, this.mClassificationContext, param1String); 
    }
    
    private void maybeReportTranslateEvents() {
      TextClassifierEvent textClassifierEvent = this.mTranslateViewEvent;
      if (textClassifierEvent != null) {
        this.mClassificationSession.onTextClassifierEvent(textClassifierEvent);
        this.mTranslateViewEvent = null;
      } 
      textClassifierEvent = this.mTranslateClickEvent;
      if (textClassifierEvent != null) {
        this.mClassificationSession.onTextClassifierEvent(textClassifierEvent);
        this.mTranslateClickEvent = null;
      } 
    }
    
    private static TextClassifierEvent generateTranslateEvent(int param1Int, TextClassification param1TextClassification, TextClassificationContext param1TextClassificationContext, String param1String) {
      RemoteAction remoteAction = ExtrasUtils.findTranslateAction(param1TextClassification);
      if (remoteAction == null)
        return null; 
      if (param1Int == 13 && 
        !remoteAction.getTitle().toString().equals(param1String))
        return null; 
      Bundle bundle = ExtrasUtils.getForeignLanguageExtra(param1TextClassification);
      ExtrasUtils.getEntityType(bundle);
      float f = ExtrasUtils.getScore(bundle);
      String str = ExtrasUtils.getModelName(bundle);
      TextClassifierEvent.LanguageDetectionEvent.Builder builder3 = new TextClassifierEvent.LanguageDetectionEvent.Builder(param1Int);
      TextClassifierEvent.LanguageDetectionEvent.Builder builder2 = builder3.setEventContext(param1TextClassificationContext);
      builder2 = builder2.setResultId(param1TextClassification.getId());
      builder2 = builder2.setScores(new float[] { f });
      TextClassifierEvent.LanguageDetectionEvent.Builder builder1 = builder2.setActionIndices(new int[] { param1TextClassification.getActions().indexOf(remoteAction) });
      builder1 = builder1.setModelName(str);
      return builder1.build();
    }
  }
  
  class TextClassificationAsyncTask extends AsyncTask<Void, Void, SelectionResult> {
    private final String mOriginalText;
    
    private final Consumer<SelectionActionModeHelper.SelectionResult> mSelectionResultCallback;
    
    private final Supplier<SelectionActionModeHelper.SelectionResult> mSelectionResultSupplier;
    
    private final TextView mTextView;
    
    private final int mTimeOutDuration;
    
    private final Supplier<SelectionActionModeHelper.SelectionResult> mTimeOutResultSupplier;
    
    TextClassificationAsyncTask(SelectionActionModeHelper this$0, int param1Int, Supplier<SelectionActionModeHelper.SelectionResult> param1Supplier1, Consumer<SelectionActionModeHelper.SelectionResult> param1Consumer, Supplier<SelectionActionModeHelper.SelectionResult> param1Supplier2) {
      super(handler);
      Handler handler;
      Objects.requireNonNull(this$0);
      this.mTextView = (TextView)this$0;
      this.mTimeOutDuration = param1Int;
      Objects.requireNonNull(param1Supplier1);
      this.mSelectionResultSupplier = param1Supplier1;
      Objects.requireNonNull(param1Consumer);
      this.mSelectionResultCallback = param1Consumer;
      Objects.requireNonNull(param1Supplier2);
      this.mTimeOutResultSupplier = param1Supplier2;
      this.mOriginalText = SelectionActionModeHelper.getText(this.mTextView).toString();
    }
    
    protected SelectionActionModeHelper.SelectionResult doInBackground(Void... param1VarArgs) {
      _$$Lambda$SelectionActionModeHelper$TextClassificationAsyncTask$D5tkmK_caFBtl9ux2L0aUfUee4E _$$Lambda$SelectionActionModeHelper$TextClassificationAsyncTask$D5tkmK_caFBtl9ux2L0aUfUee4E = new _$$Lambda$SelectionActionModeHelper$TextClassificationAsyncTask$D5tkmK_caFBtl9ux2L0aUfUee4E(this);
      this.mTextView.postDelayed(_$$Lambda$SelectionActionModeHelper$TextClassificationAsyncTask$D5tkmK_caFBtl9ux2L0aUfUee4E, this.mTimeOutDuration);
      SelectionActionModeHelper.SelectionResult selectionResult = this.mSelectionResultSupplier.get();
      this.mTextView.removeCallbacks(_$$Lambda$SelectionActionModeHelper$TextClassificationAsyncTask$D5tkmK_caFBtl9ux2L0aUfUee4E);
      return selectionResult;
    }
    
    protected void onPostExecute(SelectionActionModeHelper.SelectionResult param1SelectionResult) {
      if (!TextUtils.equals(this.mOriginalText, SelectionActionModeHelper.getText(this.mTextView)))
        param1SelectionResult = null; 
      this.mSelectionResultCallback.accept(param1SelectionResult);
    }
    
    private void onTimeOut() {
      Log.d("SelectActionModeHelper", "Timeout in TextClassificationAsyncTask");
      if (getStatus() == AsyncTask.Status.RUNNING)
        onPostExecute(this.mTimeOutResultSupplier.get()); 
      cancel(true);
    }
  }
  
  private static final class TextClassificationHelper {
    private static final int TRIM_DELTA = 120;
    
    private final Context mContext;
    
    private LocaleList mDefaultLocales;
    
    private boolean mHot;
    
    private LocaleList mLastClassificationLocales;
    
    private SelectionActionModeHelper.SelectionResult mLastClassificationResult;
    
    private int mLastClassificationSelectionEnd;
    
    private int mLastClassificationSelectionStart;
    
    private CharSequence mLastClassificationText;
    
    private int mRelativeEnd;
    
    private int mRelativeStart;
    
    private int mSelectionEnd;
    
    private int mSelectionStart;
    
    private String mText;
    
    private Supplier<TextClassifier> mTextClassifier;
    
    private int mTrimStart;
    
    private CharSequence mTrimmedText;
    
    TextClassificationHelper(Context param1Context, Supplier<TextClassifier> param1Supplier, CharSequence param1CharSequence, int param1Int1, int param1Int2, LocaleList param1LocaleList) {
      init(param1Supplier, param1CharSequence, param1Int1, param1Int2, param1LocaleList);
      Objects.requireNonNull(param1Context);
      this.mContext = param1Context;
    }
    
    public void init(Supplier<TextClassifier> param1Supplier, CharSequence param1CharSequence, int param1Int1, int param1Int2, LocaleList param1LocaleList) {
      boolean bool;
      Objects.requireNonNull(param1Supplier);
      this.mTextClassifier = param1Supplier;
      Objects.requireNonNull(param1CharSequence);
      this.mText = param1CharSequence.toString();
      this.mLastClassificationText = null;
      if (param1Int2 > param1Int1) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      this.mSelectionStart = param1Int1;
      this.mSelectionEnd = param1Int2;
      this.mDefaultLocales = param1LocaleList;
    }
    
    public SelectionActionModeHelper.SelectionResult classifyText() {
      this.mHot = true;
      return performClassification(null);
    }
    
    public SelectionActionModeHelper.SelectionResult suggestSelection() {
      TextSelection textSelection;
      this.mHot = true;
      trimText();
      if ((this.mContext.getApplicationInfo()).targetSdkVersion >= 28) {
        TextSelection.Request.Builder builder1 = new TextSelection.Request.Builder(this.mTrimmedText, this.mRelativeStart, this.mRelativeEnd);
        LocaleList localeList = this.mDefaultLocales;
        TextSelection.Request.Builder builder2 = builder1.setDefaultLocales(localeList);
        builder2 = builder2.setDarkLaunchAllowed(true);
        TextSelection.Request request = builder2.build();
        textSelection = ((TextClassifier)this.mTextClassifier.get()).suggestSelection(request);
      } else {
        textSelection = ((TextClassifier)this.mTextClassifier.get()).suggestSelection(this.mTrimmedText, this.mRelativeStart, this.mRelativeEnd, this.mDefaultLocales);
      } 
      if (!isDarkLaunchEnabled()) {
        this.mSelectionStart = Math.max(0, textSelection.getSelectionStartIndex() + this.mTrimStart);
        String str = this.mText;
        int i = str.length(), j = textSelection.getSelectionEndIndex(), k = this.mTrimStart;
        this.mSelectionEnd = Math.min(i, j + k);
      } 
      return performClassification(textSelection);
    }
    
    public SelectionActionModeHelper.SelectionResult getOriginalSelection() {
      return new SelectionActionModeHelper.SelectionResult(this.mSelectionStart, this.mSelectionEnd, null, null);
    }
    
    public int getTimeoutDuration() {
      if (this.mHot)
        return 200; 
      return 500;
    }
    
    private boolean isDarkLaunchEnabled() {
      return TextClassificationManager.getSettings(this.mContext).isModelDarkLaunchEnabled();
    }
    
    private SelectionActionModeHelper.SelectionResult performClassification(TextSelection param1TextSelection) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mText : Ljava/lang/String;
      //   4: aload_0
      //   5: getfield mLastClassificationText : Ljava/lang/CharSequence;
      //   8: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
      //   11: ifeq -> 54
      //   14: aload_0
      //   15: getfield mSelectionStart : I
      //   18: aload_0
      //   19: getfield mLastClassificationSelectionStart : I
      //   22: if_icmpne -> 54
      //   25: aload_0
      //   26: getfield mSelectionEnd : I
      //   29: aload_0
      //   30: getfield mLastClassificationSelectionEnd : I
      //   33: if_icmpne -> 54
      //   36: aload_0
      //   37: getfield mDefaultLocales : Landroid/os/LocaleList;
      //   40: astore_2
      //   41: aload_0
      //   42: getfield mLastClassificationLocales : Landroid/os/LocaleList;
      //   45: astore_3
      //   46: aload_2
      //   47: aload_3
      //   48: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
      //   51: ifne -> 262
      //   54: aload_0
      //   55: aload_0
      //   56: getfield mText : Ljava/lang/String;
      //   59: putfield mLastClassificationText : Ljava/lang/CharSequence;
      //   62: aload_0
      //   63: aload_0
      //   64: getfield mSelectionStart : I
      //   67: putfield mLastClassificationSelectionStart : I
      //   70: aload_0
      //   71: aload_0
      //   72: getfield mSelectionEnd : I
      //   75: putfield mLastClassificationSelectionEnd : I
      //   78: aload_0
      //   79: aload_0
      //   80: getfield mDefaultLocales : Landroid/os/LocaleList;
      //   83: putfield mLastClassificationLocales : Landroid/os/LocaleList;
      //   86: aload_0
      //   87: invokespecial trimText : ()V
      //   90: aload_0
      //   91: getfield mText : Ljava/lang/String;
      //   94: invokestatic containsUnsupportedCharacters : (Ljava/lang/String;)Z
      //   97: ifeq -> 134
      //   100: ldc 1397638484
      //   102: iconst_3
      //   103: anewarray java/lang/Object
      //   106: dup
      //   107: iconst_0
      //   108: ldc '116321860'
      //   110: aastore
      //   111: dup
      //   112: iconst_1
      //   113: iconst_m1
      //   114: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   117: aastore
      //   118: dup
      //   119: iconst_2
      //   120: ldc ''
      //   122: aastore
      //   123: invokestatic writeEvent : (I[Ljava/lang/Object;)I
      //   126: pop
      //   127: getstatic android/view/textclassifier/TextClassification.EMPTY : Landroid/view/textclassifier/TextClassification;
      //   130: astore_3
      //   131: goto -> 241
      //   134: aload_0
      //   135: getfield mContext : Landroid/content/Context;
      //   138: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
      //   141: getfield targetSdkVersion : I
      //   144: bipush #28
      //   146: if_icmplt -> 207
      //   149: new android/view/textclassifier/TextClassification$Request$Builder
      //   152: dup
      //   153: aload_0
      //   154: getfield mTrimmedText : Ljava/lang/CharSequence;
      //   157: aload_0
      //   158: getfield mRelativeStart : I
      //   161: aload_0
      //   162: getfield mRelativeEnd : I
      //   165: invokespecial <init> : (Ljava/lang/CharSequence;II)V
      //   168: astore_3
      //   169: aload_0
      //   170: getfield mDefaultLocales : Landroid/os/LocaleList;
      //   173: astore_2
      //   174: aload_3
      //   175: aload_2
      //   176: invokevirtual setDefaultLocales : (Landroid/os/LocaleList;)Landroid/view/textclassifier/TextClassification$Request$Builder;
      //   179: astore_3
      //   180: aload_3
      //   181: invokevirtual build : ()Landroid/view/textclassifier/TextClassification$Request;
      //   184: astore_3
      //   185: aload_0
      //   186: getfield mTextClassifier : Ljava/util/function/Supplier;
      //   189: invokeinterface get : ()Ljava/lang/Object;
      //   194: checkcast android/view/textclassifier/TextClassifier
      //   197: aload_3
      //   198: invokeinterface classifyText : (Landroid/view/textclassifier/TextClassification$Request;)Landroid/view/textclassifier/TextClassification;
      //   203: astore_3
      //   204: goto -> 241
      //   207: aload_0
      //   208: getfield mTextClassifier : Ljava/util/function/Supplier;
      //   211: invokeinterface get : ()Ljava/lang/Object;
      //   216: checkcast android/view/textclassifier/TextClassifier
      //   219: aload_0
      //   220: getfield mTrimmedText : Ljava/lang/CharSequence;
      //   223: aload_0
      //   224: getfield mRelativeStart : I
      //   227: aload_0
      //   228: getfield mRelativeEnd : I
      //   231: aload_0
      //   232: getfield mDefaultLocales : Landroid/os/LocaleList;
      //   235: invokeinterface classifyText : (Ljava/lang/CharSequence;IILandroid/os/LocaleList;)Landroid/view/textclassifier/TextClassification;
      //   240: astore_3
      //   241: aload_0
      //   242: new android/widget/SelectionActionModeHelper$SelectionResult
      //   245: dup
      //   246: aload_0
      //   247: getfield mSelectionStart : I
      //   250: aload_0
      //   251: getfield mSelectionEnd : I
      //   254: aload_3
      //   255: aload_1
      //   256: invokespecial <init> : (IILandroid/view/textclassifier/TextClassification;Landroid/view/textclassifier/TextSelection;)V
      //   259: putfield mLastClassificationResult : Landroid/widget/SelectionActionModeHelper$SelectionResult;
      //   262: aload_0
      //   263: getfield mLastClassificationResult : Landroid/widget/SelectionActionModeHelper$SelectionResult;
      //   266: areturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1167	-> 0
      //   #1170	-> 46
      //   #1172	-> 54
      //   #1173	-> 62
      //   #1174	-> 70
      //   #1175	-> 78
      //   #1177	-> 86
      //   #1179	-> 90
      //   #1181	-> 100
      //   #1182	-> 127
      //   #1183	-> 134
      //   #1185	-> 149
      //   #1188	-> 174
      //   #1189	-> 180
      //   #1190	-> 185
      //   #1191	-> 204
      //   #1193	-> 207
      //   #1196	-> 241
      //   #1200	-> 262
    }
    
    private void trimText() {
      this.mTrimStart = Math.max(0, this.mSelectionStart - 120);
      int i = Math.min(this.mText.length(), this.mSelectionEnd + 120);
      this.mTrimmedText = this.mText.subSequence(this.mTrimStart, i);
      int j = this.mSelectionStart;
      i = this.mTrimStart;
      this.mRelativeStart = j - i;
      this.mRelativeEnd = this.mSelectionEnd - i;
    }
  }
  
  private static final class SelectionResult {
    private final TextClassification mClassification;
    
    private final int mEnd;
    
    private final TextSelection mSelection;
    
    private final int mStart;
    
    SelectionResult(int param1Int1, int param1Int2, TextClassification param1TextClassification, TextSelection param1TextSelection) {
      int[] arrayOfInt = SelectionActionModeHelper.sortSelctionIndices(param1Int1, param1Int2);
      this.mStart = arrayOfInt[0];
      this.mEnd = arrayOfInt[1];
      this.mClassification = param1TextClassification;
      this.mSelection = param1TextSelection;
    }
  }
  
  private static int getActionType(int paramInt) {
    if (paramInt != 16908337)
      if (paramInt != 16908341) {
        if (paramInt != 16908353) {
          switch (paramInt) {
            default:
              return 108;
            case 16908321:
              return 101;
            case 16908320:
              return 103;
            case 16908319:
              return 200;
            case 16908322:
              break;
          } 
        } else {
          return 105;
        } 
      } else {
        return 104;
      }  
    return 102;
  }
  
  private static CharSequence getText(TextView paramTextView) {
    CharSequence charSequence = paramTextView.getText();
    if (charSequence != null)
      return charSequence; 
    return "";
  }
}
