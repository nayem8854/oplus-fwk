package android.widget;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

public abstract class CursorTreeAdapter extends BaseExpandableListAdapter implements Filterable, CursorFilter.CursorFilterClient {
  private boolean mAutoRequery;
  
  SparseArray<MyCursorHelper> mChildrenCursorHelpers;
  
  private Context mContext;
  
  CursorFilter mCursorFilter;
  
  FilterQueryProvider mFilterQueryProvider;
  
  MyCursorHelper mGroupCursorHelper;
  
  private Handler mHandler;
  
  public CursorTreeAdapter(Cursor paramCursor, Context paramContext) {
    init(paramCursor, paramContext, true);
  }
  
  public CursorTreeAdapter(Cursor paramCursor, Context paramContext, boolean paramBoolean) {
    init(paramCursor, paramContext, paramBoolean);
  }
  
  private void init(Cursor paramCursor, Context paramContext, boolean paramBoolean) {
    this.mContext = paramContext;
    this.mHandler = new Handler();
    this.mAutoRequery = paramBoolean;
    this.mGroupCursorHelper = new MyCursorHelper(paramCursor);
    this.mChildrenCursorHelpers = new SparseArray<>();
  }
  
  MyCursorHelper getChildrenCursorHelper(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mChildrenCursorHelpers : Landroid/util/SparseArray;
    //   6: iload_1
    //   7: invokevirtual get : (I)Ljava/lang/Object;
    //   10: checkcast android/widget/CursorTreeAdapter$MyCursorHelper
    //   13: astore_3
    //   14: aload_3
    //   15: astore #4
    //   17: aload_3
    //   18: ifnonnull -> 74
    //   21: aload_0
    //   22: getfield mGroupCursorHelper : Landroid/widget/CursorTreeAdapter$MyCursorHelper;
    //   25: iload_1
    //   26: invokevirtual moveTo : (I)Landroid/database/Cursor;
    //   29: astore #4
    //   31: aload #4
    //   33: ifnonnull -> 40
    //   36: aload_0
    //   37: monitorexit
    //   38: aconst_null
    //   39: areturn
    //   40: aload_0
    //   41: aload_0
    //   42: getfield mGroupCursorHelper : Landroid/widget/CursorTreeAdapter$MyCursorHelper;
    //   45: invokevirtual getCursor : ()Landroid/database/Cursor;
    //   48: invokevirtual getChildrenCursor : (Landroid/database/Cursor;)Landroid/database/Cursor;
    //   51: astore_3
    //   52: new android/widget/CursorTreeAdapter$MyCursorHelper
    //   55: astore #4
    //   57: aload #4
    //   59: aload_0
    //   60: aload_3
    //   61: invokespecial <init> : (Landroid/widget/CursorTreeAdapter;Landroid/database/Cursor;)V
    //   64: aload_0
    //   65: getfield mChildrenCursorHelpers : Landroid/util/SparseArray;
    //   68: iload_1
    //   69: aload #4
    //   71: invokevirtual put : (ILjava/lang/Object;)V
    //   74: aload_0
    //   75: monitorexit
    //   76: aload #4
    //   78: areturn
    //   79: astore #4
    //   81: aload_0
    //   82: monitorexit
    //   83: aload #4
    //   85: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #100	-> 2
    //   #102	-> 14
    //   #103	-> 21
    //   #105	-> 40
    //   #106	-> 52
    //   #107	-> 64
    //   #110	-> 74
    //   #99	-> 79
    // Exception table:
    //   from	to	target	type
    //   2	14	79	finally
    //   21	31	79	finally
    //   40	52	79	finally
    //   52	64	79	finally
    //   64	74	79	finally
  }
  
  public void setGroupCursor(Cursor paramCursor) {
    this.mGroupCursorHelper.changeCursor(paramCursor, false);
  }
  
  public void setChildrenCursor(int paramInt, Cursor paramCursor) {
    MyCursorHelper myCursorHelper = getChildrenCursorHelper(paramInt, false);
    myCursorHelper.changeCursor(paramCursor, false);
  }
  
  public Cursor getChild(int paramInt1, int paramInt2) {
    return getChildrenCursorHelper(paramInt1, true).moveTo(paramInt2);
  }
  
  public long getChildId(int paramInt1, int paramInt2) {
    return getChildrenCursorHelper(paramInt1, true).getId(paramInt2);
  }
  
  public int getChildrenCount(int paramInt) {
    MyCursorHelper myCursorHelper = getChildrenCursorHelper(paramInt, true);
    if (this.mGroupCursorHelper.isValid() && myCursorHelper != null) {
      paramInt = myCursorHelper.getCount();
    } else {
      paramInt = 0;
    } 
    return paramInt;
  }
  
  public Cursor getGroup(int paramInt) {
    return this.mGroupCursorHelper.moveTo(paramInt);
  }
  
  public int getGroupCount() {
    return this.mGroupCursorHelper.getCount();
  }
  
  public long getGroupId(int paramInt) {
    return this.mGroupCursorHelper.getId(paramInt);
  }
  
  public View getGroupView(int paramInt, boolean paramBoolean, View paramView, ViewGroup paramViewGroup) {
    Cursor cursor = this.mGroupCursorHelper.moveTo(paramInt);
    if (cursor != null) {
      if (paramView == null)
        paramView = newGroupView(this.mContext, cursor, paramBoolean, paramViewGroup); 
      bindGroupView(paramView, this.mContext, cursor, paramBoolean);
      return paramView;
    } 
    throw new IllegalStateException("this should only be called when the cursor is valid");
  }
  
  public View getChildView(int paramInt1, int paramInt2, boolean paramBoolean, View paramView, ViewGroup paramViewGroup) {
    MyCursorHelper myCursorHelper = getChildrenCursorHelper(paramInt1, true);
    Cursor cursor = myCursorHelper.moveTo(paramInt2);
    if (cursor != null) {
      if (paramView == null)
        paramView = newChildView(this.mContext, cursor, paramBoolean, paramViewGroup); 
      bindChildView(paramView, this.mContext, cursor, paramBoolean);
      return paramView;
    } 
    throw new IllegalStateException("this should only be called when the cursor is valid");
  }
  
  public boolean isChildSelectable(int paramInt1, int paramInt2) {
    return true;
  }
  
  public boolean hasStableIds() {
    return true;
  }
  
  private void releaseCursorHelpers() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mChildrenCursorHelpers : Landroid/util/SparseArray;
    //   6: invokevirtual size : ()I
    //   9: iconst_1
    //   10: isub
    //   11: istore_1
    //   12: iload_1
    //   13: iflt -> 36
    //   16: aload_0
    //   17: getfield mChildrenCursorHelpers : Landroid/util/SparseArray;
    //   20: iload_1
    //   21: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   24: checkcast android/widget/CursorTreeAdapter$MyCursorHelper
    //   27: invokevirtual deactivate : ()V
    //   30: iinc #1, -1
    //   33: goto -> 12
    //   36: aload_0
    //   37: getfield mChildrenCursorHelpers : Landroid/util/SparseArray;
    //   40: invokevirtual clear : ()V
    //   43: aload_0
    //   44: monitorexit
    //   45: return
    //   46: astore_2
    //   47: aload_0
    //   48: monitorexit
    //   49: aload_2
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #289	-> 2
    //   #290	-> 16
    //   #289	-> 30
    //   #293	-> 36
    //   #294	-> 43
    //   #288	-> 46
    // Exception table:
    //   from	to	target	type
    //   2	12	46	finally
    //   16	30	46	finally
    //   36	43	46	finally
  }
  
  public void notifyDataSetChanged() {
    notifyDataSetChanged(true);
  }
  
  public void notifyDataSetChanged(boolean paramBoolean) {
    if (paramBoolean)
      releaseCursorHelpers(); 
    super.notifyDataSetChanged();
  }
  
  public void notifyDataSetInvalidated() {
    releaseCursorHelpers();
    super.notifyDataSetInvalidated();
  }
  
  public void onGroupCollapsed(int paramInt) {
    deactivateChildrenCursorHelper(paramInt);
  }
  
  void deactivateChildrenCursorHelper(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: iconst_1
    //   5: invokevirtual getChildrenCursorHelper : (IZ)Landroid/widget/CursorTreeAdapter$MyCursorHelper;
    //   8: astore_2
    //   9: aload_0
    //   10: getfield mChildrenCursorHelpers : Landroid/util/SparseArray;
    //   13: iload_1
    //   14: invokevirtual remove : (I)V
    //   17: aload_2
    //   18: invokevirtual deactivate : ()V
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: astore_2
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_2
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #335	-> 2
    //   #336	-> 9
    //   #337	-> 17
    //   #338	-> 21
    //   #334	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	9	24	finally
    //   9	17	24	finally
    //   17	21	24	finally
  }
  
  public String convertToString(Cursor paramCursor) {
    String str;
    if (paramCursor == null) {
      str = "";
    } else {
      str = str.toString();
    } 
    return str;
  }
  
  public Cursor runQueryOnBackgroundThread(CharSequence paramCharSequence) {
    FilterQueryProvider filterQueryProvider = this.mFilterQueryProvider;
    if (filterQueryProvider != null)
      return filterQueryProvider.runQuery(paramCharSequence); 
    return this.mGroupCursorHelper.getCursor();
  }
  
  public Filter getFilter() {
    if (this.mCursorFilter == null)
      this.mCursorFilter = new CursorFilter(this); 
    return this.mCursorFilter;
  }
  
  public FilterQueryProvider getFilterQueryProvider() {
    return this.mFilterQueryProvider;
  }
  
  public void setFilterQueryProvider(FilterQueryProvider paramFilterQueryProvider) {
    this.mFilterQueryProvider = paramFilterQueryProvider;
  }
  
  public void changeCursor(Cursor paramCursor) {
    this.mGroupCursorHelper.changeCursor(paramCursor, true);
  }
  
  public Cursor getCursor() {
    return this.mGroupCursorHelper.getCursor();
  }
  
  protected abstract void bindChildView(View paramView, Context paramContext, Cursor paramCursor, boolean paramBoolean);
  
  protected abstract void bindGroupView(View paramView, Context paramContext, Cursor paramCursor, boolean paramBoolean);
  
  protected abstract Cursor getChildrenCursor(Cursor paramCursor);
  
  protected abstract View newChildView(Context paramContext, Cursor paramCursor, boolean paramBoolean, ViewGroup paramViewGroup);
  
  protected abstract View newGroupView(Context paramContext, Cursor paramCursor, boolean paramBoolean, ViewGroup paramViewGroup);
  
  class MyCursorHelper {
    private MyContentObserver mContentObserver;
    
    private Cursor mCursor;
    
    private MyDataSetObserver mDataSetObserver;
    
    private boolean mDataValid;
    
    private int mRowIDColumn;
    
    final CursorTreeAdapter this$0;
    
    MyCursorHelper(Cursor param1Cursor) {
      boolean bool;
      byte b;
      if (param1Cursor != null) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mCursor = param1Cursor;
      this.mDataValid = bool;
      if (bool) {
        b = param1Cursor.getColumnIndex("_id");
      } else {
        b = -1;
      } 
      this.mRowIDColumn = b;
      this.mContentObserver = new MyContentObserver();
      this.mDataSetObserver = new MyDataSetObserver();
      if (bool) {
        param1Cursor.registerContentObserver(this.mContentObserver);
        param1Cursor.registerDataSetObserver(this.mDataSetObserver);
      } 
    }
    
    Cursor getCursor() {
      return this.mCursor;
    }
    
    int getCount() {
      if (this.mDataValid) {
        Cursor cursor = this.mCursor;
        if (cursor != null)
          return cursor.getCount(); 
      } 
      return 0;
    }
    
    long getId(int param1Int) {
      if (this.mDataValid) {
        Cursor cursor = this.mCursor;
        if (cursor != null) {
          if (cursor.moveToPosition(param1Int))
            return this.mCursor.getLong(this.mRowIDColumn); 
          return 0L;
        } 
      } 
      return 0L;
    }
    
    Cursor moveTo(int param1Int) {
      if (this.mDataValid) {
        Cursor cursor = this.mCursor;
        if (cursor != null && cursor.moveToPosition(param1Int))
          return this.mCursor; 
      } 
      return null;
    }
    
    void changeCursor(Cursor param1Cursor, boolean param1Boolean) {
      if (param1Cursor == this.mCursor)
        return; 
      deactivate();
      this.mCursor = param1Cursor;
      if (param1Cursor != null) {
        param1Cursor.registerContentObserver(this.mContentObserver);
        param1Cursor.registerDataSetObserver(this.mDataSetObserver);
        this.mRowIDColumn = param1Cursor.getColumnIndex("_id");
        this.mDataValid = true;
        CursorTreeAdapter.this.notifyDataSetChanged(param1Boolean);
      } else {
        this.mRowIDColumn = -1;
        this.mDataValid = false;
        CursorTreeAdapter.this.notifyDataSetInvalidated();
      } 
    }
    
    void deactivate() {
      Cursor cursor = this.mCursor;
      if (cursor == null)
        return; 
      cursor.unregisterContentObserver(this.mContentObserver);
      this.mCursor.unregisterDataSetObserver(this.mDataSetObserver);
      this.mCursor.close();
      this.mCursor = null;
    }
    
    boolean isValid() {
      boolean bool;
      if (this.mDataValid && this.mCursor != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    class MyContentObserver extends ContentObserver {
      final CursorTreeAdapter.MyCursorHelper this$1;
      
      public MyContentObserver() {
        super(CursorTreeAdapter.this.mHandler);
      }
      
      public boolean deliverSelfNotifications() {
        return true;
      }
      
      public void onChange(boolean param2Boolean) {
        if (CursorTreeAdapter.this.mAutoRequery && CursorTreeAdapter.MyCursorHelper.this.mCursor != null && !CursorTreeAdapter.MyCursorHelper.this.mCursor.isClosed()) {
          CursorTreeAdapter.MyCursorHelper myCursorHelper = CursorTreeAdapter.MyCursorHelper.this;
          CursorTreeAdapter.MyCursorHelper.access$402(myCursorHelper, myCursorHelper.mCursor.requery());
        } 
      }
    }
    
    class MyDataSetObserver extends DataSetObserver {
      final CursorTreeAdapter.MyCursorHelper this$1;
      
      private MyDataSetObserver() {}
      
      public void onChanged() {
        CursorTreeAdapter.MyCursorHelper.access$402(CursorTreeAdapter.MyCursorHelper.this, true);
        CursorTreeAdapter.this.notifyDataSetChanged();
      }
      
      public void onInvalidated() {
        CursorTreeAdapter.MyCursorHelper.access$402(CursorTreeAdapter.MyCursorHelper.this, false);
        CursorTreeAdapter.this.notifyDataSetInvalidated();
      }
    }
  }
}
