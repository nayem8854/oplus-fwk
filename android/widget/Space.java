package android.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

public final class Space extends View {
  public Space(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    if (getVisibility() == 0)
      setVisibility(4); 
  }
  
  public Space(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public Space(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public Space(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void draw(Canvas paramCanvas) {}
  
  private static int getDefaultSize2(int paramInt1, int paramInt2) {
    int i = paramInt1;
    int j = View.MeasureSpec.getMode(paramInt2);
    paramInt2 = View.MeasureSpec.getSize(paramInt2);
    if (j != Integer.MIN_VALUE) {
      if (j != 0)
        if (j != 1073741824) {
          paramInt1 = i;
        } else {
          paramInt1 = paramInt2;
        }  
    } else {
      paramInt1 = Math.min(paramInt1, paramInt2);
    } 
    return paramInt1;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    paramInt1 = getDefaultSize2(getSuggestedMinimumWidth(), paramInt1);
    paramInt2 = getDefaultSize2(getSuggestedMinimumHeight(), paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
  }
}
