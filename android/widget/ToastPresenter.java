package android.widget;

import android.app.INotificationManager;
import android.app.ITransientNotificationCallback;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.IAccessibilityManager;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;

public class ToastPresenter {
  private static final long LONG_DURATION_TIMEOUT = 7000L;
  
  private static final long SHORT_DURATION_TIMEOUT = 4000L;
  
  private static final String TAG = "ToastPresenter";
  
  public static final int TEXT_TOAST_LAYOUT = 17367342;
  
  private static final String WINDOW_TITLE = "Toast";
  
  private final AccessibilityManager mAccessibilityManager;
  
  private final Context mContext;
  
  private final INotificationManager mNotificationManager;
  
  private final String mPackageName;
  
  private final WindowManager.LayoutParams mParams;
  
  private final Resources mResources;
  
  private IBinder mToken;
  
  private View mView;
  
  private final WindowManager mWindowManager;
  
  public static View getTextToastView(Context paramContext, CharSequence paramCharSequence) {
    View view = LayoutInflater.from(paramContext).inflate(17367342, (ViewGroup)null);
    TextView textView = view.<TextView>findViewById(16908299);
    textView.setText(paramCharSequence);
    return view;
  }
  
  public ToastPresenter(Context paramContext, IAccessibilityManager paramIAccessibilityManager, INotificationManager paramINotificationManager, String paramString) {
    this.mContext = paramContext;
    this.mResources = paramContext.getResources();
    this.mWindowManager = (WindowManager)paramContext.getSystemService(WindowManager.class);
    this.mNotificationManager = paramINotificationManager;
    this.mPackageName = paramString;
    this.mAccessibilityManager = new AccessibilityManager(paramContext, paramIAccessibilityManager, paramContext.getUserId());
    this.mParams = createLayoutParams();
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public WindowManager.LayoutParams getLayoutParams() {
    return this.mParams;
  }
  
  public View getView() {
    return this.mView;
  }
  
  public IBinder getToken() {
    return this.mToken;
  }
  
  private WindowManager.LayoutParams createLayoutParams() {
    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
    layoutParams.height = -2;
    layoutParams.width = -2;
    layoutParams.format = -3;
    layoutParams.windowAnimations = 16973828;
    layoutParams.type = 2005;
    layoutParams.setFitInsetsIgnoringVisibility(true);
    layoutParams.setTitle("Toast");
    layoutParams.flags = 152;
    setShowForAllUsersIfApplicable(layoutParams, this.mPackageName);
    return layoutParams;
  }
  
  private void adjustLayoutParams(WindowManager.LayoutParams paramLayoutParams, IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2) {
    long l;
    Configuration configuration = this.mResources.getConfiguration();
    paramInt2 = Gravity.getAbsoluteGravity(paramInt2, configuration.getLayoutDirection());
    paramLayoutParams.gravity = paramInt2;
    if ((paramInt2 & 0x7) == 7)
      paramLayoutParams.horizontalWeight = 1.0F; 
    if ((paramInt2 & 0x70) == 112)
      paramLayoutParams.verticalWeight = 1.0F; 
    paramLayoutParams.x = paramInt3;
    paramLayoutParams.y = paramInt4;
    paramLayoutParams.horizontalMargin = paramFloat1;
    paramLayoutParams.verticalMargin = paramFloat2;
    paramLayoutParams.packageName = this.mContext.getPackageName();
    if (paramInt1 == 1) {
      l = 7000L;
    } else {
      l = 4000L;
    } 
    paramLayoutParams.hideTimeoutMilliseconds = l;
    paramLayoutParams.token = paramIBinder;
  }
  
  private void setShowForAllUsersIfApplicable(WindowManager.LayoutParams paramLayoutParams, String paramString) {
    if (isCrossUserPackage(paramString))
      paramLayoutParams.privateFlags = 16; 
  }
  
  private boolean isCrossUserPackage(String paramString) {
    String[] arrayOfString = this.mResources.getStringArray(17236089);
    return ArrayUtils.contains((Object[])arrayOfString, paramString);
  }
  
  public void show(View paramView, IBinder paramIBinder1, IBinder paramIBinder2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, ITransientNotificationCallback paramITransientNotificationCallback) {
    boolean bool;
    if (this.mView == null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool, "Only one toast at a time is allowed, call hide() first.");
    this.mView = paramView;
    this.mToken = paramIBinder1;
    adjustLayoutParams(this.mParams, paramIBinder2, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2);
    if (this.mView.getParent() != null)
      this.mWindowManager.removeView(this.mView); 
    try {
      this.mWindowManager.addView(this.mView, this.mParams);
      trySendAccessibilityEvent(this.mView, this.mPackageName);
      if (paramITransientNotificationCallback != null)
        try {
          paramITransientNotificationCallback.onToastShown();
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error calling back ");
          stringBuilder.append(this.mPackageName);
          stringBuilder.append(" to notify onToastShow()");
          Log.w("ToastPresenter", stringBuilder.toString(), (Throwable)remoteException);
        }  
      return;
    } catch (android.view.WindowManager.BadTokenException badTokenException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error while attempting to show toast from ");
      stringBuilder.append(this.mPackageName);
      Log.w("ToastPresenter", stringBuilder.toString(), badTokenException);
      return;
    } 
  }
  
  public void hide(ITransientNotificationCallback paramITransientNotificationCallback) {
    boolean bool;
    if (this.mView != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool, "No toast to hide.");
    if (this.mView.getParent() != null)
      this.mWindowManager.removeViewImmediate(this.mView); 
    try {
      this.mNotificationManager.finishToken(this.mPackageName, this.mToken);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error finishing toast window token from package ");
      stringBuilder.append(this.mPackageName);
      Log.w("ToastPresenter", stringBuilder.toString(), (Throwable)remoteException);
    } 
    if (paramITransientNotificationCallback != null)
      try {
        paramITransientNotificationCallback.onToastHidden();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error calling back ");
        stringBuilder.append(this.mPackageName);
        stringBuilder.append(" to notify onToastHide()");
        Log.w("ToastPresenter", stringBuilder.toString(), (Throwable)remoteException);
      }  
    this.mView = null;
    this.mToken = null;
  }
  
  public void trySendAccessibilityEvent(View paramView, String paramString) {
    if (!this.mAccessibilityManager.isEnabled())
      return; 
    AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(64);
    accessibilityEvent.setClassName(Toast.class.getName());
    accessibilityEvent.setPackageName(paramString);
    paramView.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    this.mAccessibilityManager.sendAccessibilityEvent(accessibilityEvent);
  }
}
