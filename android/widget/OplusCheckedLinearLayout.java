package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class OplusCheckedLinearLayout extends LinearLayout implements Checkable {
  public OplusCheckedLinearLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusCheckedLinearLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusCheckedLinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public OplusCheckedLinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public boolean isChecked() {
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view instanceof Checkable)
        return ((Checkable)view).isChecked(); 
    } 
    return false;
  }
  
  public void setChecked(boolean paramBoolean) {
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view instanceof Checkable)
        ((Checkable)view).setChecked(paramBoolean); 
    } 
  }
  
  public void toggle() {
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view instanceof Checkable)
        ((Checkable)view).toggle(); 
    } 
  }
}
