package android.widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class RemoteViewsListAdapter extends BaseAdapter {
  private Context mContext;
  
  private ArrayList<RemoteViews> mRemoteViewsList;
  
  private int mViewTypeCount;
  
  private ArrayList<Integer> mViewTypes = new ArrayList<>();
  
  public RemoteViewsListAdapter(Context paramContext, ArrayList<RemoteViews> paramArrayList, int paramInt) {
    this.mContext = paramContext;
    this.mRemoteViewsList = paramArrayList;
    this.mViewTypeCount = paramInt;
    init();
  }
  
  public void setViewsList(ArrayList<RemoteViews> paramArrayList) {
    this.mRemoteViewsList = paramArrayList;
    init();
    notifyDataSetChanged();
  }
  
  private void init() {
    if (this.mRemoteViewsList == null)
      return; 
    this.mViewTypes.clear();
    for (RemoteViews remoteViews : this.mRemoteViewsList) {
      if (!this.mViewTypes.contains(Integer.valueOf(remoteViews.getLayoutId())))
        this.mViewTypes.add(Integer.valueOf(remoteViews.getLayoutId())); 
    } 
    int i = this.mViewTypes.size(), j = this.mViewTypeCount;
    if (i <= j && j >= 1)
      return; 
    throw new RuntimeException("Invalid view type count -- view type count must be >= 1and must be as large as the total number of distinct view types");
  }
  
  public int getCount() {
    ArrayList<RemoteViews> arrayList = this.mRemoteViewsList;
    if (arrayList != null)
      return arrayList.size(); 
    return 0;
  }
  
  public Object getItem(int paramInt) {
    return null;
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    if (paramInt < getCount()) {
      RemoteViews remoteViews = this.mRemoteViewsList.get(paramInt);
      remoteViews.addFlags(2);
      if (paramView != null && remoteViews != null && 
        paramView.getId() == remoteViews.getLayoutId()) {
        remoteViews.reapply(this.mContext, paramView);
      } else {
        paramView = remoteViews.apply(this.mContext, paramViewGroup);
      } 
      return paramView;
    } 
    return null;
  }
  
  public int getItemViewType(int paramInt) {
    if (paramInt < getCount()) {
      paramInt = ((RemoteViews)this.mRemoteViewsList.get(paramInt)).getLayoutId();
      return this.mViewTypes.indexOf(Integer.valueOf(paramInt));
    } 
    return 0;
  }
  
  public int getViewTypeCount() {
    return this.mViewTypeCount;
  }
  
  public boolean hasStableIds() {
    return false;
  }
}
