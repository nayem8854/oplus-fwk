package android.widget;

import android.os.Handler;

class DoubleDigitManager {
  private Integer intermediateDigit;
  
  private final CallBack mCallBack;
  
  private final long timeoutInMillis;
  
  public DoubleDigitManager(long paramLong, CallBack paramCallBack) {
    this.timeoutInMillis = paramLong;
    this.mCallBack = paramCallBack;
  }
  
  public void reportDigit(int paramInt) {
    Integer integer = this.intermediateDigit;
    if (integer == null) {
      this.intermediateDigit = Integer.valueOf(paramInt);
      (new Handler()).postDelayed(new Runnable() {
            final DoubleDigitManager this$0;
            
            public void run() {
              if (DoubleDigitManager.this.intermediateDigit != null) {
                DoubleDigitManager.this.mCallBack.singleDigitFinal(DoubleDigitManager.this.intermediateDigit.intValue());
                DoubleDigitManager.access$002(DoubleDigitManager.this, null);
              } 
            }
          }this.timeoutInMillis);
      if (!this.mCallBack.singleDigitIntermediate(paramInt)) {
        this.intermediateDigit = null;
        this.mCallBack.singleDigitFinal(paramInt);
      } 
    } else if (this.mCallBack.twoDigitsFinal(integer.intValue(), paramInt)) {
      this.intermediateDigit = null;
    } 
  }
  
  static interface CallBack {
    void singleDigitFinal(int param1Int);
    
    boolean singleDigitIntermediate(int param1Int);
    
    boolean twoDigitsFinal(int param1Int1, int param1Int2);
  }
}
