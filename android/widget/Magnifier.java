package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.HardwareRenderer;
import android.graphics.Insets;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RenderNode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.PixelCopy;
import android.view.Surface;
import android.view.SurfaceControl;
import android.view.SurfaceHolder;
import android.view.SurfaceSession;
import android.view.SurfaceView;
import android.view.ThreadedRenderer;
import android.view.View;
import android.view.ViewRootImpl;
import com.android.internal.R;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class Magnifier {
  static {
    HandlerThread handlerThread = new HandlerThread("magnifier pixel copy result handler");
    handlerThread.start();
  }
  
  private final Point mWindowCoords = new Point();
  
  private final Point mClampedCenterZoomCoords = new Point();
  
  private final Point mPrevStartCoordsInSurface = new Point(-1, -1);
  
  private final PointF mPrevShowSourceCoords = new PointF(-1.0F, -1.0F);
  
  private final PointF mPrevShowWindowCoords = new PointF(-1.0F, -1.0F);
  
  private final Rect mPixelCopyRequestRect = new Rect();
  
  private final Object mLock = new Object();
  
  private final Object mDestroyLock = new Object();
  
  private int mLeftCutWidth = 0;
  
  private int mRightCutWidth = 0;
  
  private int mLeftBound = Integer.MIN_VALUE;
  
  private int mRightBound = Integer.MAX_VALUE;
  
  private static final float FISHEYE_RAMP_WIDTH = 12.0F;
  
  private static final int NONEXISTENT_PREVIOUS_CONFIG_VALUE = -1;
  
  public static final int SOURCE_BOUND_MAX_IN_SURFACE = 0;
  
  public static final int SOURCE_BOUND_MAX_VISIBLE = 1;
  
  private static final String TAG = "Magnifier";
  
  private static final HandlerThread sPixelCopyHandlerThread;
  
  private int mBottomContentBound;
  
  private Callback mCallback;
  
  private final boolean mClippingEnabled;
  
  private SurfaceInfo mContentCopySurface;
  
  private final int mDefaultHorizontalSourceToMagnifierOffset;
  
  private final int mDefaultVerticalSourceToMagnifierOffset;
  
  private boolean mDirtyState;
  
  private boolean mIsFishEyeStyle;
  
  private int mLeftContentBound;
  
  private final Drawable mOverlay;
  
  private SurfaceInfo mParentSurface;
  
  private final int mRamp;
  
  private int mRightContentBound;
  
  private int mSourceHeight;
  
  private int mSourceWidth;
  
  private int mTopContentBound;
  
  private final View mView;
  
  private final int[] mViewCoordinatesInSurface;
  
  private InternalPopupWindow mWindow;
  
  private final float mWindowCornerRadius;
  
  private final float mWindowElevation;
  
  private int mWindowHeight;
  
  private final int mWindowWidth;
  
  private float mZoom;
  
  @Deprecated
  public Magnifier(View paramView) {
    this(createBuilderWithOldMagnifierDefaults(paramView));
  }
  
  static Builder createBuilderWithOldMagnifierDefaults(View paramView) {
    Builder builder = new Builder(paramView);
    Context context = paramView.getContext();
    TypedArray typedArray = context.obtainStyledAttributes(null, R.styleable.Magnifier, 17956984, 0);
    Builder.access$002(builder, typedArray.getDimensionPixelSize(5, 0));
    Builder.access$102(builder, typedArray.getDimensionPixelSize(2, 0));
    Builder.access$202(builder, typedArray.getDimension(1, 0.0F));
    Builder.access$302(builder, getDeviceDefaultDialogCornerRadius(context));
    Builder.access$402(builder, typedArray.getFloat(6, 0.0F));
    int i = typedArray.getDimensionPixelSize(3, 0);
    Builder.access$502(builder, i);
    i = typedArray.getDimensionPixelSize(4, 0);
    Builder.access$602(builder, i);
    Builder.access$702(builder, (Drawable)new ColorDrawable(typedArray.getColor(0, 0)));
    typedArray.recycle();
    Builder.access$802(builder, true);
    Builder.access$902(builder, 1);
    Builder.access$1002(builder, 0);
    Builder.access$1102(builder, 1);
    Builder.access$1202(builder, 0);
    return builder;
  }
  
  private static float getDeviceDefaultDialogCornerRadius(Context paramContext) {
    ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(paramContext, 16974120);
    TypedArray typedArray = contextThemeWrapper.obtainStyledAttributes(new int[] { 16844145 });
    float f = typedArray.getDimension(0, 0.0F);
    typedArray.recycle();
    return f;
  }
  
  private Magnifier(Builder paramBuilder) {
    this.mView = paramBuilder.mView;
    this.mWindowWidth = paramBuilder.mWidth;
    this.mWindowHeight = paramBuilder.mHeight;
    this.mZoom = paramBuilder.mZoom;
    this.mIsFishEyeStyle = paramBuilder.mIsFishEyeStyle;
    if (paramBuilder.mSourceWidth > 0 && paramBuilder.mSourceHeight > 0) {
      this.mSourceWidth = paramBuilder.mSourceWidth;
      this.mSourceHeight = paramBuilder.mSourceHeight;
    } else {
      this.mSourceWidth = Math.round(this.mWindowWidth / this.mZoom);
      this.mSourceHeight = Math.round(this.mWindowHeight / this.mZoom);
    } 
    this.mWindowElevation = paramBuilder.mElevation;
    this.mWindowCornerRadius = paramBuilder.mCornerRadius;
    this.mOverlay = paramBuilder.mOverlay;
    this.mDefaultHorizontalSourceToMagnifierOffset = paramBuilder.mHorizontalDefaultSourceToMagnifierOffset;
    this.mDefaultVerticalSourceToMagnifierOffset = paramBuilder.mVerticalDefaultSourceToMagnifierOffset;
    this.mClippingEnabled = paramBuilder.mClippingEnabled;
    this.mLeftContentBound = paramBuilder.mLeftContentBound;
    this.mTopContentBound = paramBuilder.mTopContentBound;
    this.mRightContentBound = paramBuilder.mRightContentBound;
    this.mBottomContentBound = paramBuilder.mBottomContentBound;
    this.mViewCoordinatesInSurface = new int[2];
    View view = this.mView;
    DisplayMetrics displayMetrics = view.getContext().getResources().getDisplayMetrics();
    this.mRamp = (int)TypedValue.applyDimension(1, 12.0F, displayMetrics);
  }
  
  void setSourceHorizontalBounds(int paramInt1, int paramInt2) {
    this.mLeftBound = paramInt1;
    this.mRightBound = paramInt2;
  }
  
  public void show(float paramFloat1, float paramFloat2) {
    show(paramFloat1, paramFloat2, this.mDefaultHorizontalSourceToMagnifierOffset + paramFloat1, this.mDefaultVerticalSourceToMagnifierOffset + paramFloat2);
  }
  
  public void show(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial obtainSurfaces : ()V
    //   4: aload_0
    //   5: fload_1
    //   6: fload_2
    //   7: invokespecial obtainContentCoordinates : (FF)V
    //   10: aload_0
    //   11: getfield mClampedCenterZoomCoords : Landroid/graphics/Point;
    //   14: getfield x : I
    //   17: aload_0
    //   18: getfield mSourceWidth : I
    //   21: iconst_2
    //   22: idiv
    //   23: isub
    //   24: istore #5
    //   26: aload_0
    //   27: getfield mClampedCenterZoomCoords : Landroid/graphics/Point;
    //   30: getfield y : I
    //   33: istore #6
    //   35: aload_0
    //   36: getfield mSourceHeight : I
    //   39: iconst_2
    //   40: idiv
    //   41: istore #7
    //   43: aload_0
    //   44: getfield mIsFishEyeStyle : Z
    //   47: ifeq -> 399
    //   50: aload_0
    //   51: getfield mClampedCenterZoomCoords : Landroid/graphics/Point;
    //   54: getfield x : I
    //   57: aload_0
    //   58: getfield mViewCoordinatesInSurface : [I
    //   61: iconst_0
    //   62: iaload
    //   63: isub
    //   64: i2f
    //   65: fstore #4
    //   67: aload_0
    //   68: getfield mClampedCenterZoomCoords : Landroid/graphics/Point;
    //   71: getfield y : I
    //   74: aload_0
    //   75: getfield mViewCoordinatesInSurface : [I
    //   78: iconst_1
    //   79: iaload
    //   80: isub
    //   81: i2f
    //   82: fstore #8
    //   84: aload_0
    //   85: getfield mSourceWidth : I
    //   88: istore #9
    //   90: iload #9
    //   92: i2f
    //   93: fstore #10
    //   95: aload_0
    //   96: getfield mRamp : I
    //   99: istore #11
    //   101: iload #9
    //   103: iload #11
    //   105: iconst_2
    //   106: imul
    //   107: isub
    //   108: i2f
    //   109: fstore_3
    //   110: aload_0
    //   111: getfield mZoom : F
    //   114: fstore #12
    //   116: fload #10
    //   118: fload_3
    //   119: fload #12
    //   121: fdiv
    //   122: fsub
    //   123: fconst_2
    //   124: fdiv
    //   125: fstore #10
    //   127: fload_1
    //   128: iload #9
    //   130: iconst_2
    //   131: idiv
    //   132: i2f
    //   133: fsub
    //   134: fstore #13
    //   136: iload #11
    //   138: i2f
    //   139: fload #13
    //   141: fadd
    //   142: fstore #14
    //   144: fconst_0
    //   145: fstore_3
    //   146: fconst_0
    //   147: fload #14
    //   149: fcmpl
    //   150: ifle -> 165
    //   153: fload_1
    //   154: fload_1
    //   155: fconst_0
    //   156: fsub
    //   157: fload #12
    //   159: fdiv
    //   160: fsub
    //   161: fstore_3
    //   162: goto -> 190
    //   165: fconst_0
    //   166: fload #13
    //   168: fcmpl
    //   169: ifle -> 190
    //   172: fload #13
    //   174: fload #10
    //   176: fadd
    //   177: fload #14
    //   179: fconst_0
    //   180: fsub
    //   181: fload #10
    //   183: fmul
    //   184: iload #11
    //   186: i2f
    //   187: fdiv
    //   188: fsub
    //   189: fstore_3
    //   190: fload_3
    //   191: f2i
    //   192: aload_0
    //   193: getfield mLeftBound : I
    //   196: invokestatic max : (II)I
    //   199: aload_0
    //   200: getfield mRightBound : I
    //   203: invokestatic min : (II)I
    //   206: istore #9
    //   208: aload_0
    //   209: getfield mSourceWidth : I
    //   212: iconst_2
    //   213: idiv
    //   214: i2f
    //   215: fload_1
    //   216: fadd
    //   217: fstore #14
    //   219: fload #14
    //   221: aload_0
    //   222: getfield mRamp : I
    //   225: i2f
    //   226: fsub
    //   227: fstore #12
    //   229: aload_0
    //   230: getfield mView : Landroid/view/View;
    //   233: invokevirtual getWidth : ()I
    //   236: i2f
    //   237: fstore_3
    //   238: fload_3
    //   239: fload #12
    //   241: fcmpg
    //   242: ifge -> 259
    //   245: fload_1
    //   246: fload_3
    //   247: fload_1
    //   248: fsub
    //   249: aload_0
    //   250: getfield mZoom : F
    //   253: fdiv
    //   254: fadd
    //   255: fstore_3
    //   256: goto -> 289
    //   259: fload_3
    //   260: fload #14
    //   262: fcmpg
    //   263: ifge -> 289
    //   266: fload #14
    //   268: fload #10
    //   270: fsub
    //   271: fload_3
    //   272: fload #12
    //   274: fsub
    //   275: fload #10
    //   277: fmul
    //   278: aload_0
    //   279: getfield mRamp : I
    //   282: i2f
    //   283: fdiv
    //   284: fadd
    //   285: fstore_3
    //   286: goto -> 289
    //   289: iload #9
    //   291: fload_3
    //   292: f2i
    //   293: aload_0
    //   294: getfield mRightBound : I
    //   297: invokestatic min : (II)I
    //   300: invokestatic max : (II)I
    //   303: istore #11
    //   305: aload_0
    //   306: getfield mViewCoordinatesInSurface : [I
    //   309: iconst_0
    //   310: iaload
    //   311: iload #9
    //   313: iadd
    //   314: iconst_0
    //   315: invokestatic max : (II)I
    //   318: istore #9
    //   320: aload_0
    //   321: getfield mViewCoordinatesInSurface : [I
    //   324: iconst_0
    //   325: iaload
    //   326: istore #15
    //   328: aload_0
    //   329: getfield mContentCopySurface : Landroid/widget/Magnifier$SurfaceInfo;
    //   332: astore #16
    //   334: aload #16
    //   336: invokestatic access$1700 : (Landroid/widget/Magnifier$SurfaceInfo;)I
    //   339: istore #17
    //   341: iload #15
    //   343: iload #11
    //   345: iadd
    //   346: iload #17
    //   348: invokestatic min : (II)I
    //   351: istore #11
    //   353: aload_0
    //   354: iconst_0
    //   355: iload #9
    //   357: iload #5
    //   359: isub
    //   360: invokestatic max : (II)I
    //   363: putfield mLeftCutWidth : I
    //   366: aload_0
    //   367: iconst_0
    //   368: aload_0
    //   369: getfield mSourceWidth : I
    //   372: iload #5
    //   374: iadd
    //   375: iload #11
    //   377: isub
    //   378: invokestatic max : (II)I
    //   381: putfield mRightCutWidth : I
    //   384: iload #5
    //   386: iload #9
    //   388: invokestatic max : (II)I
    //   391: istore #5
    //   393: fload #8
    //   395: fstore_3
    //   396: goto -> 409
    //   399: fload_3
    //   400: fstore #8
    //   402: fload #4
    //   404: fstore_3
    //   405: fload #8
    //   407: fstore #4
    //   409: aload_0
    //   410: fload #4
    //   412: fload_3
    //   413: invokespecial obtainWindowCoordinates : (FF)V
    //   416: fload_1
    //   417: aload_0
    //   418: getfield mPrevShowSourceCoords : Landroid/graphics/PointF;
    //   421: getfield x : F
    //   424: fcmpl
    //   425: ifne -> 518
    //   428: fload_2
    //   429: aload_0
    //   430: getfield mPrevShowSourceCoords : Landroid/graphics/PointF;
    //   433: getfield y : F
    //   436: fcmpl
    //   437: ifne -> 518
    //   440: aload_0
    //   441: getfield mDirtyState : Z
    //   444: ifeq -> 450
    //   447: goto -> 518
    //   450: fload #4
    //   452: aload_0
    //   453: getfield mPrevShowWindowCoords : Landroid/graphics/PointF;
    //   456: getfield x : F
    //   459: fcmpl
    //   460: ifne -> 481
    //   463: fload_3
    //   464: aload_0
    //   465: getfield mPrevShowWindowCoords : Landroid/graphics/PointF;
    //   468: getfield y : F
    //   471: fcmpl
    //   472: ifeq -> 478
    //   475: goto -> 481
    //   478: goto -> 735
    //   481: aload_0
    //   482: invokespecial getCurrentClampedWindowCoordinates : ()Landroid/graphics/Point;
    //   485: astore #16
    //   487: aload_0
    //   488: getfield mWindow : Landroid/widget/Magnifier$InternalPopupWindow;
    //   491: astore #18
    //   493: getstatic android/widget/Magnifier.sPixelCopyHandlerThread : Landroid/os/HandlerThread;
    //   496: invokevirtual getThreadHandler : ()Landroid/os/Handler;
    //   499: new android/widget/_$$Lambda$Magnifier$sEUKNU2_gseoDMBt_HOs_JGAfZ8
    //   502: dup
    //   503: aload_0
    //   504: aload #18
    //   506: aload #16
    //   508: invokespecial <init> : (Landroid/widget/Magnifier;Landroid/widget/Magnifier$InternalPopupWindow;Landroid/graphics/Point;)V
    //   511: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   514: pop
    //   515: goto -> 735
    //   518: aload_0
    //   519: getfield mWindow : Landroid/widget/Magnifier$InternalPopupWindow;
    //   522: ifnonnull -> 723
    //   525: aload_0
    //   526: getfield mLock : Ljava/lang/Object;
    //   529: astore #18
    //   531: aload #18
    //   533: monitorenter
    //   534: new android/widget/Magnifier$InternalPopupWindow
    //   537: astore #19
    //   539: aload_0
    //   540: getfield mView : Landroid/view/View;
    //   543: invokevirtual getContext : ()Landroid/content/Context;
    //   546: astore #20
    //   548: aload_0
    //   549: getfield mView : Landroid/view/View;
    //   552: invokevirtual getDisplay : ()Landroid/view/Display;
    //   555: astore #21
    //   557: aload_0
    //   558: getfield mParentSurface : Landroid/widget/Magnifier$SurfaceInfo;
    //   561: astore #16
    //   563: aload #16
    //   565: invokestatic access$1800 : (Landroid/widget/Magnifier$SurfaceInfo;)Landroid/view/SurfaceControl;
    //   568: astore #22
    //   570: aload_0
    //   571: getfield mWindowWidth : I
    //   574: istore #11
    //   576: aload_0
    //   577: getfield mWindowHeight : I
    //   580: istore #15
    //   582: aload_0
    //   583: getfield mZoom : F
    //   586: fstore #10
    //   588: aload_0
    //   589: getfield mRamp : I
    //   592: istore #9
    //   594: aload_0
    //   595: getfield mWindowElevation : F
    //   598: fstore #8
    //   600: aload_0
    //   601: getfield mWindowCornerRadius : F
    //   604: fstore #12
    //   606: aload_0
    //   607: getfield mOverlay : Landroid/graphics/drawable/Drawable;
    //   610: astore #16
    //   612: aload #16
    //   614: ifnull -> 631
    //   617: aload_0
    //   618: getfield mOverlay : Landroid/graphics/drawable/Drawable;
    //   621: astore #16
    //   623: goto -> 641
    //   626: astore #16
    //   628: goto -> 712
    //   631: new android/graphics/drawable/ColorDrawable
    //   634: dup
    //   635: iconst_0
    //   636: invokespecial <init> : (I)V
    //   639: astore #16
    //   641: invokestatic getMain : ()Landroid/os/Handler;
    //   644: astore #23
    //   646: aload_0
    //   647: getfield mLock : Ljava/lang/Object;
    //   650: astore #24
    //   652: aload_0
    //   653: getfield mCallback : Landroid/widget/Magnifier$Callback;
    //   656: astore #25
    //   658: aload #19
    //   660: aload #20
    //   662: aload #21
    //   664: aload #22
    //   666: iload #11
    //   668: iload #15
    //   670: fload #10
    //   672: iload #9
    //   674: fload #8
    //   676: fload #12
    //   678: aload #16
    //   680: aload #23
    //   682: aload #24
    //   684: aload #25
    //   686: aload_0
    //   687: getfield mIsFishEyeStyle : Z
    //   690: invokespecial <init> : (Landroid/content/Context;Landroid/view/Display;Landroid/view/SurfaceControl;IIFIFFLandroid/graphics/drawable/Drawable;Landroid/os/Handler;Ljava/lang/Object;Landroid/widget/Magnifier$Callback;Z)V
    //   693: aload_0
    //   694: aload #19
    //   696: putfield mWindow : Landroid/widget/Magnifier$InternalPopupWindow;
    //   699: aload #18
    //   701: monitorexit
    //   702: goto -> 723
    //   705: astore #16
    //   707: goto -> 712
    //   710: astore #16
    //   712: aload #18
    //   714: monitorexit
    //   715: aload #16
    //   717: athrow
    //   718: astore #16
    //   720: goto -> 712
    //   723: aload_0
    //   724: iload #5
    //   726: iload #6
    //   728: iload #7
    //   730: isub
    //   731: iconst_1
    //   732: invokespecial performPixelCopy : (IIZ)V
    //   735: aload_0
    //   736: getfield mPrevShowSourceCoords : Landroid/graphics/PointF;
    //   739: fload_1
    //   740: putfield x : F
    //   743: aload_0
    //   744: getfield mPrevShowSourceCoords : Landroid/graphics/PointF;
    //   747: fload_2
    //   748: putfield y : F
    //   751: aload_0
    //   752: getfield mPrevShowWindowCoords : Landroid/graphics/PointF;
    //   755: fload #4
    //   757: putfield x : F
    //   760: aload_0
    //   761: getfield mPrevShowWindowCoords : Landroid/graphics/PointF;
    //   764: fload_3
    //   765: putfield y : F
    //   768: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #303	-> 0
    //   #304	-> 4
    //   #306	-> 10
    //   #307	-> 26
    //   #309	-> 43
    //   #311	-> 50
    //   #312	-> 67
    //   #317	-> 84
    //   #323	-> 127
    //   #324	-> 136
    //   #325	-> 144
    //   #326	-> 146
    //   #329	-> 153
    //   #330	-> 165
    //   #333	-> 172
    //   #335	-> 190
    //   #340	-> 208
    //   #341	-> 219
    //   #342	-> 229
    //   #343	-> 238
    //   #346	-> 245
    //   #347	-> 259
    //   #350	-> 266
    //   #347	-> 289
    //   #352	-> 289
    //   #356	-> 305
    //   #357	-> 320
    //   #358	-> 334
    //   #357	-> 341
    //   #359	-> 353
    //   #360	-> 366
    //   #361	-> 384
    //   #309	-> 399
    //   #363	-> 409
    //   #365	-> 416
    //   #378	-> 450
    //   #380	-> 481
    //   #381	-> 487
    //   #382	-> 493
    //   #367	-> 518
    //   #368	-> 525
    //   #369	-> 534
    //   #370	-> 563
    //   #372	-> 606
    //   #375	-> 626
    //   #372	-> 631
    //   #373	-> 641
    //   #375	-> 699
    //   #367	-> 723
    //   #377	-> 723
    //   #392	-> 735
    //   #393	-> 743
    //   #394	-> 751
    //   #395	-> 760
    //   #396	-> 768
    // Exception table:
    //   from	to	target	type
    //   534	563	710	finally
    //   563	606	710	finally
    //   606	612	710	finally
    //   617	623	626	finally
    //   631	641	705	finally
    //   641	658	705	finally
    //   658	699	718	finally
    //   699	702	718	finally
    //   712	715	718	finally
  }
  
  public void dismiss() {
    if (this.mWindow != null)
      synchronized (this.mLock) {
        this.mWindow.destroy();
        this.mWindow = null;
        this.mPrevShowSourceCoords.x = -1.0F;
        this.mPrevShowSourceCoords.y = -1.0F;
        this.mPrevShowWindowCoords.x = -1.0F;
        this.mPrevShowWindowCoords.y = -1.0F;
        this.mPrevStartCoordsInSurface.x = -1;
        this.mPrevStartCoordsInSurface.y = -1;
      }  
  }
  
  public void update() {
    if (this.mWindow != null) {
      obtainSurfaces();
      if (!this.mDirtyState) {
        performPixelCopy(this.mPrevStartCoordsInSurface.x, this.mPrevStartCoordsInSurface.y, false);
      } else {
        show(this.mPrevShowSourceCoords.x, this.mPrevShowSourceCoords.y, this.mPrevShowWindowCoords.x, this.mPrevShowWindowCoords.y);
      } 
    } 
  }
  
  public int getWidth() {
    return this.mWindowWidth;
  }
  
  public int getHeight() {
    return this.mWindowHeight;
  }
  
  public int getSourceWidth() {
    return this.mSourceWidth;
  }
  
  public int getSourceHeight() {
    return this.mSourceHeight;
  }
  
  public void setZoom(float paramFloat) {
    int i;
    Preconditions.checkArgumentPositive(paramFloat, "Zoom should be positive");
    this.mZoom = paramFloat;
    if (this.mIsFishEyeStyle) {
      i = this.mWindowWidth;
    } else {
      i = Math.round(this.mWindowWidth / paramFloat);
    } 
    this.mSourceWidth = i;
    this.mSourceHeight = Math.round(this.mWindowHeight / this.mZoom);
    this.mDirtyState = true;
  }
  
  void updateSourceFactors(int paramInt, float paramFloat) {
    this.mZoom = paramFloat;
    this.mSourceHeight = paramInt;
    this.mWindowHeight = paramInt = (int)(paramInt * paramFloat);
    InternalPopupWindow internalPopupWindow = this.mWindow;
    if (internalPopupWindow != null)
      internalPopupWindow.updateContentFactors(paramInt, paramFloat); 
  }
  
  public float getZoom() {
    return this.mZoom;
  }
  
  public float getElevation() {
    return this.mWindowElevation;
  }
  
  public float getCornerRadius() {
    return this.mWindowCornerRadius;
  }
  
  public int getDefaultHorizontalSourceToMagnifierOffset() {
    return this.mDefaultHorizontalSourceToMagnifierOffset;
  }
  
  public int getDefaultVerticalSourceToMagnifierOffset() {
    return this.mDefaultVerticalSourceToMagnifierOffset;
  }
  
  public Drawable getOverlay() {
    return this.mOverlay;
  }
  
  public boolean isClippingEnabled() {
    return this.mClippingEnabled;
  }
  
  public Point getPosition() {
    if (this.mWindow == null)
      return null; 
    Point point = getCurrentClampedWindowCoordinates();
    point.offset(-this.mParentSurface.mInsets.left, -this.mParentSurface.mInsets.top);
    return new Point(point);
  }
  
  public Point getSourcePosition() {
    if (this.mWindow == null)
      return null; 
    Point point = new Point(this.mPixelCopyRequestRect.left, this.mPixelCopyRequestRect.top);
    point.offset(-this.mContentCopySurface.mInsets.left, -this.mContentCopySurface.mInsets.top);
    return new Point(point);
  }
  
  private void obtainSurfaces() {
    SurfaceInfo surfaceInfo1 = SurfaceInfo.NULL;
    SurfaceInfo surfaceInfo2 = surfaceInfo1;
    if (this.mView.getViewRootImpl() != null) {
      ViewRootImpl viewRootImpl = this.mView.getViewRootImpl();
      Surface surface = viewRootImpl.mSurface;
      surfaceInfo2 = surfaceInfo1;
      if (surface != null) {
        surfaceInfo2 = surfaceInfo1;
        if (surface.isValid()) {
          Rect rect = viewRootImpl.mWindowAttributes.surfaceInsets;
          int i = viewRootImpl.getWidth(), j = rect.left, k = rect.right;
          int m = viewRootImpl.getHeight(), n = rect.top, i1 = rect.bottom;
          surfaceInfo2 = new SurfaceInfo(viewRootImpl.getSurfaceControl(), surface, i + j + k, m + n + i1, rect, true);
        } 
      } 
    } 
    SurfaceInfo surfaceInfo3 = SurfaceInfo.NULL;
    View view = this.mView;
    surfaceInfo1 = surfaceInfo3;
    if (view instanceof SurfaceView) {
      SurfaceControl surfaceControl = ((SurfaceView)view).getSurfaceControl();
      SurfaceHolder surfaceHolder = ((SurfaceView)this.mView).getHolder();
      Surface surface = surfaceHolder.getSurface();
      surfaceInfo1 = surfaceInfo3;
      if (surfaceControl != null) {
        surfaceInfo1 = surfaceInfo3;
        if (surfaceControl.isValid()) {
          Rect rect = surfaceHolder.getSurfaceFrame();
          surfaceInfo1 = new SurfaceInfo(surfaceControl, surface, rect.right, rect.bottom, new Rect(), false);
        } 
      } 
    } 
    if (surfaceInfo2 != SurfaceInfo.NULL) {
      surfaceInfo3 = surfaceInfo2;
    } else {
      surfaceInfo3 = surfaceInfo1;
    } 
    this.mParentSurface = surfaceInfo3;
    if (!(this.mView instanceof SurfaceView))
      surfaceInfo1 = surfaceInfo2; 
    this.mContentCopySurface = surfaceInfo1;
  }
  
  private void obtainContentCoordinates(float paramFloat1, float paramFloat2) {
    int arrayOfInt[] = this.mViewCoordinatesInSurface, i = arrayOfInt[0];
    int j = arrayOfInt[1];
    this.mView.getLocationInSurface(arrayOfInt);
    arrayOfInt = this.mViewCoordinatesInSurface;
    if (arrayOfInt[0] != i || arrayOfInt[1] != j)
      this.mDirtyState = true; 
    if (this.mView instanceof SurfaceView) {
      i = Math.round(paramFloat1);
      j = Math.round(paramFloat2);
    } else {
      i = Math.round(paramFloat1 + this.mViewCoordinatesInSurface[0]);
      j = Math.round(paramFloat2 + this.mViewCoordinatesInSurface[1]);
    } 
    Rect[] arrayOfRect = new Rect[2];
    SurfaceInfo surfaceInfo = this.mContentCopySurface;
    Rect rect = new Rect(0, 0, surfaceInfo.mWidth, this.mContentCopySurface.mHeight);
    arrayOfRect[0] = rect;
    rect = new Rect();
    this.mView.getGlobalVisibleRect(rect);
    if (this.mView.getViewRootImpl() != null) {
      Rect rect1 = (this.mView.getViewRootImpl()).mWindowAttributes.surfaceInsets;
      rect.offset(rect1.left, rect1.top);
    } 
    if (this.mView instanceof SurfaceView) {
      int[] arrayOfInt1 = this.mViewCoordinatesInSurface;
      rect.offset(-arrayOfInt1[0], -arrayOfInt1[1]);
    } 
    arrayOfRect[1] = rect;
    int k = Integer.MIN_VALUE;
    int m;
    for (m = this.mLeftContentBound; m >= 0; m--)
      k = Math.max(k, (arrayOfRect[m]).left); 
    m = Integer.MIN_VALUE;
    int n;
    for (n = this.mTopContentBound; n >= 0; n--)
      m = Math.max(m, (arrayOfRect[n]).top); 
    n = Integer.MAX_VALUE;
    int i1;
    for (i1 = this.mRightContentBound; i1 >= 0; i1--)
      n = Math.min(n, (arrayOfRect[i1]).right); 
    i1 = Integer.MAX_VALUE;
    int i2;
    for (i2 = this.mBottomContentBound; i2 >= 0; i2--)
      i1 = Math.min(i1, (arrayOfRect[i2]).bottom); 
    i2 = Math.min(k, this.mContentCopySurface.mWidth - this.mSourceWidth);
    k = Math.min(m, this.mContentCopySurface.mHeight - this.mSourceHeight);
    if (i2 < 0 || k < 0)
      Log.e("Magnifier", "Magnifier's content is copied from a surface smaller thanthe content requested size. The magnifier will be dismissed."); 
    n = Math.max(n, this.mSourceWidth + i2);
    m = Math.max(i1, this.mSourceHeight + k);
    Point point = this.mClampedCenterZoomCoords;
    if (this.mIsFishEyeStyle) {
      i = Math.max(i2, Math.min(i, n));
    } else {
      i1 = this.mSourceWidth;
      i = Math.max(i1 / 2 + i2, Math.min(i, n - i1 / 2));
    } 
    point.x = i;
    point = this.mClampedCenterZoomCoords;
    i = this.mSourceHeight;
    point.y = Math.max(i / 2 + k, Math.min(j, m - i / 2));
  }
  
  private void obtainWindowCoordinates(float paramFloat1, float paramFloat2) {
    int i, j;
    if (this.mView instanceof SurfaceView) {
      i = Math.round(paramFloat1);
      j = Math.round(paramFloat2);
    } else {
      i = Math.round(this.mViewCoordinatesInSurface[0] + paramFloat1);
      j = Math.round(this.mViewCoordinatesInSurface[1] + paramFloat2);
    } 
    this.mWindowCoords.x = i - this.mWindowWidth / 2;
    this.mWindowCoords.y = j - this.mWindowHeight / 2;
    if (this.mParentSurface != this.mContentCopySurface) {
      Point point = this.mWindowCoords;
      point.x += this.mViewCoordinatesInSurface[0];
      point = this.mWindowCoords;
      point.y += this.mViewCoordinatesInSurface[1];
    } 
  }
  
  private void performPixelCopy(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (this.mContentCopySurface.mSurface == null || !this.mContentCopySurface.mSurface.isValid()) {
      onPixelCopyFailed();
      return;
    } 
    Point point = getCurrentClampedWindowCoordinates();
    this.mPixelCopyRequestRect.set(paramInt1, paramInt2, this.mSourceWidth + paramInt1 - this.mLeftCutWidth - this.mRightCutWidth, this.mSourceHeight + paramInt2);
    this.mPrevStartCoordsInSurface.x = paramInt1;
    this.mPrevStartCoordsInSurface.y = paramInt2;
    this.mDirtyState = false;
    InternalPopupWindow internalPopupWindow = this.mWindow;
    if (this.mPixelCopyRequestRect.width() == 0) {
      InternalPopupWindow internalPopupWindow1 = this.mWindow;
      paramInt2 = this.mSourceWidth;
      paramInt1 = this.mSourceHeight;
      Bitmap.Config config1 = Bitmap.Config.ALPHA_8;
      Bitmap bitmap1 = Bitmap.createBitmap(paramInt2, paramInt1, config1);
      internalPopupWindow1.updateContent(bitmap1);
      return;
    } 
    paramInt1 = this.mSourceWidth;
    int i = this.mLeftCutWidth;
    paramInt2 = this.mRightCutWidth;
    int j = this.mSourceHeight;
    Bitmap.Config config = Bitmap.Config.ARGB_8888;
    Bitmap bitmap = Bitmap.createBitmap(paramInt1 - i - paramInt2, j, config);
    Surface surface = this.mContentCopySurface.mSurface;
    Rect rect = this.mPixelCopyRequestRect;
    _$$Lambda$Magnifier$K0um0QSTAb4wXwua60CgJIIwGaI _$$Lambda$Magnifier$K0um0QSTAb4wXwua60CgJIIwGaI = new _$$Lambda$Magnifier$K0um0QSTAb4wXwua60CgJIIwGaI(this, internalPopupWindow, paramBoolean, point, bitmap);
    HandlerThread handlerThread = sPixelCopyHandlerThread;
    Handler handler = handlerThread.getThreadHandler();
    PixelCopy.request(surface, rect, bitmap, _$$Lambda$Magnifier$K0um0QSTAb4wXwua60CgJIIwGaI, handler);
  }
  
  private void onPixelCopyFailed() {
    Log.e("Magnifier", "Magnifier failed to copy content from the view Surface. It will be dismissed.");
    Handler.getMain().postAtFrontOfQueue(new _$$Lambda$Magnifier$esRj9C7NyDvOX8eqqqLKuB6jpTw(this));
  }
  
  private Point getCurrentClampedWindowCoordinates() {
    Rect rect;
    if (!this.mClippingEnabled)
      return new Point(this.mWindowCoords); 
    if (this.mParentSurface.mIsMainWindowSurface) {
      Insets insets = this.mView.getRootWindowInsets().getSystemWindowInsets();
      int i1 = insets.left;
      SurfaceInfo surfaceInfo2 = this.mParentSurface;
      int i2 = surfaceInfo2.mInsets.left, i3 = insets.top;
      surfaceInfo2 = this.mParentSurface;
      int i4 = surfaceInfo2.mInsets.top;
      surfaceInfo2 = this.mParentSurface;
      int i5 = surfaceInfo2.mWidth, i6 = insets.right, i7 = this.mParentSurface.mInsets.right;
      surfaceInfo2 = this.mParentSurface;
      int i8 = surfaceInfo2.mHeight, i9 = insets.bottom;
      SurfaceInfo surfaceInfo1 = this.mParentSurface;
      rect = new Rect(i1 + i2, i3 + i4, i5 - i6 - i7, i8 - i9 - surfaceInfo1.mInsets.bottom);
    } else {
      rect = new Rect(0, 0, this.mParentSurface.mWidth, this.mParentSurface.mHeight);
    } 
    int m = rect.left, j = rect.right, k = this.mWindowWidth, n = this.mWindowCoords.x;
    n = Math.min(j - k, n);
    m = Math.max(m, n);
    n = rect.top;
    k = rect.bottom;
    int i = this.mWindowHeight;
    j = this.mWindowCoords.y;
    k = Math.min(k - i, j);
    n = Math.max(n, k);
    return new Point(m, n);
  }
  
  private static class SurfaceInfo {
    public static final SurfaceInfo NULL = new SurfaceInfo(null, null, 0, 0, null, false);
    
    private int mHeight;
    
    private Rect mInsets;
    
    private boolean mIsMainWindowSurface;
    
    private Surface mSurface;
    
    private SurfaceControl mSurfaceControl;
    
    private int mWidth;
    
    SurfaceInfo(SurfaceControl param1SurfaceControl, Surface param1Surface, int param1Int1, int param1Int2, Rect param1Rect, boolean param1Boolean) {
      this.mSurfaceControl = param1SurfaceControl;
      this.mSurface = param1Surface;
      this.mWidth = param1Int1;
      this.mHeight = param1Int2;
      this.mInsets = param1Rect;
      this.mIsMainWindowSurface = param1Boolean;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SourceBound {}
  
  private static class InternalPopupWindow {
    private float mZoom;
    
    private int mWindowPositionY;
    
    private int mWindowPositionX;
    
    private final SurfaceControl.Transaction mTransaction = new SurfaceControl.Transaction();
    
    private final SurfaceSession mSurfaceSession;
    
    private final SurfaceControl mSurfaceControl;
    
    private final Surface mSurface;
    
    private final ThreadedRenderer.SimpleRenderer mRenderer;
    
    private final int mRamp;
    
    private boolean mPendingWindowPositionUpdate;
    
    private final RenderNode mOverlayRenderNode;
    
    private final Drawable mOverlay;
    
    private final int mOffsetY;
    
    private final int mOffsetX;
    
    private int mMeshWidth;
    
    private float[] mMeshRight;
    
    private float[] mMeshLeft;
    
    private int mMeshHeight;
    
    private final Runnable mMagnifierUpdater;
    
    private final Object mLock;
    
    private boolean mIsFishEyeStyle;
    
    private final Handler mHandler;
    
    private boolean mFrameDrawScheduled;
    
    private boolean mFirstDraw = true;
    
    private final Display mDisplay;
    
    private Bitmap mCurrentContent;
    
    private final int mContentWidth;
    
    private int mContentHeight;
    
    private Magnifier.Callback mCallback;
    
    private final RenderNode mBitmapRenderNode;
    
    private Bitmap mBitmap;
    
    private static final int SURFACE_Z = 5;
    
    InternalPopupWindow(Context param1Context, Display param1Display, SurfaceControl param1SurfaceControl, int param1Int1, int param1Int2, float param1Float1, int param1Int3, float param1Float2, float param1Float3, Drawable param1Drawable, Handler param1Handler, Object param1Object, Magnifier.Callback param1Callback, boolean param1Boolean) {
      this.mDisplay = param1Display;
      this.mOverlay = param1Drawable;
      this.mLock = param1Object;
      this.mCallback = param1Callback;
      this.mContentWidth = param1Int1;
      this.mContentHeight = param1Int2;
      this.mZoom = param1Float1;
      this.mRamp = param1Int3;
      this.mOffsetX = param1Int3 = (int)(param1Float2 * 1.05F);
      int i = (int)(1.05F * param1Float2);
      SurfaceSession surfaceSession = new SurfaceSession();
      SurfaceControl.Builder builder = new SurfaceControl.Builder(surfaceSession);
      builder = builder.setFormat(-3);
      builder = builder.setBufferSize(param1Int3 * 2 + param1Int1, param1Int2 + i * 2);
      builder = builder.setName("magnifier surface");
      builder = builder.setFlags(4);
      builder = builder.setParent(param1SurfaceControl);
      builder = builder.setCallsite("InternalPopupWindow");
      this.mSurfaceControl = builder.build();
      Surface surface = new Surface();
      surface.copyFrom(this.mSurfaceControl);
      this.mRenderer = new ThreadedRenderer.SimpleRenderer(param1Context, "magnifier renderer", this.mSurface);
      this.mBitmapRenderNode = createRenderNodeForBitmap("magnifier content", param1Float2, param1Float3);
      this.mOverlayRenderNode = createRenderNodeForOverlay("magnifier overlay", param1Float3);
      setupOverlay();
      null = this.mRenderer.getRootNode().beginRecording(param1Int1, param1Int2);
      try {
        null.enableZ();
        null.drawRenderNode(this.mBitmapRenderNode);
        null.disableZ();
        null.drawRenderNode(this.mOverlayRenderNode);
        null.disableZ();
        this.mRenderer.getRootNode().endRecording();
        if (this.mCallback != null) {
          param1Int1 = this.mContentWidth;
          param1Int2 = this.mContentHeight;
          Bitmap.Config config = Bitmap.Config.ARGB_8888;
          this.mCurrentContent = Bitmap.createBitmap(param1Int1, param1Int2, config);
          updateCurrentContentForTesting();
        } 
        this.mHandler = param1Handler;
        this.mMagnifierUpdater = new _$$Lambda$Magnifier$InternalPopupWindow$t9Cn2sIi2LBUhAVikvRPKKoAwIU(this);
        this.mFrameDrawScheduled = false;
        this.mIsFishEyeStyle = param1Boolean;
        return;
      } finally {
        this.mRenderer.getRootNode().endRecording();
      } 
    }
    
    private void updateContentFactors(int param1Int, float param1Float) {
      if (this.mContentHeight == param1Int && this.mZoom == param1Float)
        return; 
      if (this.mContentHeight < param1Int) {
        SurfaceControl.Transaction transaction = (new SurfaceControl.Transaction()).setBufferSize(this.mSurfaceControl, this.mContentWidth, param1Int);
        transaction.apply();
        this.mSurface.copyFrom(this.mSurfaceControl);
        this.mRenderer.setSurface(this.mSurface);
        Outline outline = new Outline();
        outline.setRoundRect(0, 0, this.mContentWidth, param1Int, 0.0F);
        outline.setAlpha(1.0F);
        RenderNode renderNode = this.mBitmapRenderNode;
        int i = this.mOffsetX, j = this.mOffsetY;
        renderNode.setLeftTopRightBottom(i, j, this.mContentWidth + i, j + param1Int);
        this.mBitmapRenderNode.setOutline(outline);
        renderNode = this.mOverlayRenderNode;
        j = this.mOffsetX;
        i = this.mOffsetY;
        renderNode.setLeftTopRightBottom(j, i, this.mContentWidth + j, i + param1Int);
        this.mOverlayRenderNode.setOutline(outline);
        ThreadedRenderer.SimpleRenderer simpleRenderer = this.mRenderer;
        null = simpleRenderer.getRootNode().beginRecording(this.mContentWidth, param1Int);
        try {
          null.enableZ();
          null.drawRenderNode(this.mBitmapRenderNode);
          null.disableZ();
          null.drawRenderNode(this.mOverlayRenderNode);
          null.disableZ();
        } finally {
          this.mRenderer.getRootNode().endRecording();
        } 
      } 
      this.mContentHeight = param1Int;
      this.mZoom = param1Float;
      fillMeshMatrix();
    }
    
    private void createMeshMatrixForFishEyeEffect() {
      this.mMeshWidth = 1;
      this.mMeshHeight = 6;
      this.mMeshLeft = new float[(1 + 1) * 2 * (6 + 1)];
      this.mMeshRight = new float[(1 + 1) * 2 * (6 + 1)];
      fillMeshMatrix();
    }
    
    private void fillMeshMatrix() {
      this.mMeshWidth = 1;
      this.mMeshHeight = 6;
      float f1 = this.mContentWidth;
      float f2 = this.mContentHeight;
      float f3 = f2 / this.mZoom;
      float f4 = f2 - f3;
      byte b = 0;
      while (true) {
        int i = this.mMeshWidth, j = this.mMeshHeight;
        if (b < (i + 1) * 2 * (j + 1)) {
          int k = b % (i + 1) * 2 / 2;
          float arrayOfFloat1[] = this.mMeshLeft, f5 = k;
          int m = this.mRamp;
          arrayOfFloat1[b] = f5 * m / i;
          float[] arrayOfFloat2 = this.mMeshRight;
          arrayOfFloat2[b] = f1 - m + (m * k / i);
          m = b / 2 / (i + 1);
          float f6 = k * f4 / i + f3;
          f5 = (f2 - f6) / 2.0F;
          arrayOfFloat1[b + 1] = m * f6 / j + f5;
          f5 = f2 - k * f4 / i;
          f6 = (f2 - f5) / 2.0F;
          arrayOfFloat2[b + 1] = m * f5 / j + f6;
          b += 2;
          continue;
        } 
        break;
      } 
    }
    
    private RenderNode createRenderNodeForBitmap(String param1String, float param1Float1, float param1Float2) {
      RenderNode renderNode = RenderNode.create(param1String, null);
      int i = this.mOffsetX, j = this.mOffsetY;
      renderNode.setLeftTopRightBottom(i, j, this.mContentWidth + i, this.mContentHeight + j);
      renderNode.setElevation(param1Float1);
      Outline outline = new Outline();
      outline.setRoundRect(0, 0, this.mContentWidth, this.mContentHeight, param1Float2);
      outline.setAlpha(1.0F);
      renderNode.setOutline(outline);
      renderNode.setClipToOutline(true);
      null = renderNode.beginRecording(this.mContentWidth, this.mContentHeight);
      try {
        null.drawColor(-16711936);
        return renderNode;
      } finally {
        renderNode.endRecording();
      } 
    }
    
    private RenderNode createRenderNodeForOverlay(String param1String, float param1Float) {
      RenderNode renderNode = RenderNode.create(param1String, null);
      int i = this.mOffsetX, j = this.mOffsetY;
      renderNode.setLeftTopRightBottom(i, j, this.mContentWidth + i, this.mContentHeight + j);
      Outline outline = new Outline();
      outline.setRoundRect(0, 0, this.mContentWidth, this.mContentHeight, param1Float);
      outline.setAlpha(1.0F);
      renderNode.setOutline(outline);
      renderNode.setClipToOutline(true);
      return renderNode;
    }
    
    private void setupOverlay() {
      drawOverlay();
      this.mOverlay.setCallback((Drawable.Callback)new Object(this));
    }
    
    private void drawOverlay() {
      RenderNode renderNode = this.mOverlayRenderNode;
      int i = this.mContentWidth, j = this.mContentHeight;
      null = renderNode.beginRecording(i, j);
      try {
        this.mOverlay.setBounds(0, 0, this.mContentWidth, this.mContentHeight);
        this.mOverlay.draw((Canvas)null);
        return;
      } finally {
        this.mOverlayRenderNode.endRecording();
      } 
    }
    
    public void setContentPositionForNextDraw(int param1Int1, int param1Int2) {
      this.mWindowPositionX = param1Int1 - this.mOffsetX;
      this.mWindowPositionY = param1Int2 - this.mOffsetY;
      this.mPendingWindowPositionUpdate = true;
      requestUpdate();
    }
    
    public void updateContent(Bitmap param1Bitmap) {
      Bitmap bitmap = this.mBitmap;
      if (bitmap != null)
        bitmap.recycle(); 
      this.mBitmap = param1Bitmap;
      requestUpdate();
    }
    
    private void requestUpdate() {
      if (this.mFrameDrawScheduled)
        return; 
      Message message = Message.obtain(this.mHandler, this.mMagnifierUpdater);
      message.setAsynchronous(true);
      message.sendToTarget();
      this.mFrameDrawScheduled = true;
    }
    
    public void destroy() {
      this.mRenderer.destroy();
      this.mSurface.destroy();
      (new SurfaceControl.Transaction()).remove(this.mSurfaceControl).apply();
      this.mSurfaceSession.kill();
      this.mHandler.removeCallbacks(this.mMagnifierUpdater);
      Bitmap bitmap = this.mBitmap;
      if (bitmap != null)
        bitmap.recycle(); 
      this.mOverlay.setCallback(null);
    }
    
    private void doDraw() {
      synchronized (this.mLock) {
        if (!this.mSurface.isValid())
          return; 
        null = this.mBitmapRenderNode;
        int i = this.mContentWidth, j = this.mContentHeight;
        RecordingCanvas recordingCanvas = null.beginRecording(i, j);
        try {
          i = this.mBitmap.getWidth();
          int k = this.mBitmap.getHeight();
          Paint paint = new Paint();
          this();
          paint.setFilterBitmap(true);
          boolean bool = this.mIsFishEyeStyle;
          if (bool) {
            try {
              j = (int)((this.mContentWidth - (this.mContentWidth - this.mRamp * 2) / this.mZoom) / 2.0F);
              Rect rect1 = new Rect();
              this(j, 0, i - j, k);
              Rect rect2 = new Rect();
              this(this.mRamp, 0, this.mContentWidth - this.mRamp, this.mContentHeight);
              recordingCanvas.drawBitmap(this.mBitmap, rect1, rect2, paint);
              Bitmap bitmap = this.mBitmap;
              bitmap = Bitmap.createBitmap(bitmap, 0, 0, j, k);
              int m = this.mMeshWidth, n = this.mMeshHeight;
              float[] arrayOfFloat = this.mMeshLeft;
              recordingCanvas.drawBitmapMesh(bitmap, m, n, arrayOfFloat, 0, null, 0, paint);
              bitmap = this.mBitmap;
              bitmap = Bitmap.createBitmap(bitmap, i - j, 0, j, k);
              j = this.mMeshWidth;
              i = this.mMeshHeight;
              arrayOfFloat = this.mMeshRight;
              recordingCanvas.drawBitmapMesh(bitmap, j, i, arrayOfFloat, 0, null, 0, paint);
            } finally {}
          } else {
            Rect rect1 = new Rect();
            this(0, 0, i, k);
            Rect rect2 = new Rect();
            this(0, 0, this.mContentWidth, this.mContentHeight);
            recordingCanvas.drawBitmap(this.mBitmap, rect1, rect2, paint);
          } 
          this.mBitmapRenderNode.endRecording();
          if (this.mPendingWindowPositionUpdate || this.mFirstDraw) {
            bool = this.mFirstDraw;
            this.mFirstDraw = false;
            boolean bool1 = this.mPendingWindowPositionUpdate;
            this.mPendingWindowPositionUpdate = false;
            j = this.mWindowPositionX;
            i = this.mWindowPositionY;
            _$$Lambda$Magnifier$InternalPopupWindow$qfjMrDJVvOQUv9_kKVdpLzbaJ_A _$$Lambda$Magnifier$InternalPopupWindow$qfjMrDJVvOQUv9_kKVdpLzbaJ_A = new _$$Lambda$Magnifier$InternalPopupWindow$qfjMrDJVvOQUv9_kKVdpLzbaJ_A();
            this(this, bool1, j, i, bool);
            if (!this.mIsFishEyeStyle)
              this.mRenderer.setLightCenter(this.mDisplay, j, i); 
          } else {
            paint = null;
          } 
          this.mFrameDrawScheduled = false;
          this.mRenderer.draw((HardwareRenderer.FrameDrawingCallback)paint);
          return;
        } finally {
          this.mBitmapRenderNode.endRecording();
        } 
      } 
    }
    
    private void updateCurrentContentForTesting() {
      Canvas canvas = new Canvas(this.mCurrentContent);
      Rect rect = new Rect(0, 0, this.mContentWidth, this.mContentHeight);
      Bitmap bitmap = this.mBitmap;
      if (bitmap != null && !bitmap.isRecycled()) {
        Rect rect1 = new Rect(0, 0, this.mBitmap.getWidth(), this.mBitmap.getHeight());
        canvas.drawBitmap(this.mBitmap, rect1, rect, null);
      } 
      this.mOverlay.setBounds(rect);
      this.mOverlay.draw(canvas);
    }
  }
  
  public static interface Callback {
    void onOperationComplete();
  }
  
  public static final class Builder {
    private int mBottomContentBound;
    
    private boolean mClippingEnabled;
    
    private float mCornerRadius;
    
    private float mElevation;
    
    private int mHeight;
    
    private int mHorizontalDefaultSourceToMagnifierOffset;
    
    private boolean mIsFishEyeStyle;
    
    private int mLeftContentBound;
    
    private Drawable mOverlay;
    
    private int mRightContentBound;
    
    private int mSourceHeight;
    
    private int mSourceWidth;
    
    private int mTopContentBound;
    
    private int mVerticalDefaultSourceToMagnifierOffset;
    
    private View mView;
    
    private int mWidth;
    
    private float mZoom;
    
    public Builder(View param1View) {
      Objects.requireNonNull(param1View);
      this.mView = param1View;
      applyDefaults();
    }
    
    private void applyDefaults() {
      Resources resources = this.mView.getContext().getResources();
      this.mWidth = resources.getDimensionPixelSize(17105152);
      this.mHeight = resources.getDimensionPixelSize(17105149);
      this.mElevation = resources.getDimension(17105148);
      this.mCornerRadius = resources.getDimension(17105147);
      this.mZoom = resources.getFloat(17105153);
      this.mHorizontalDefaultSourceToMagnifierOffset = resources.getDimensionPixelSize(17105150);
      this.mVerticalDefaultSourceToMagnifierOffset = resources.getDimensionPixelSize(17105151);
      this.mOverlay = (Drawable)new ColorDrawable(resources.getColor(17170752, null));
      this.mClippingEnabled = true;
      this.mLeftContentBound = 1;
      this.mTopContentBound = 1;
      this.mRightContentBound = 1;
      this.mBottomContentBound = 1;
      this.mIsFishEyeStyle = false;
    }
    
    public Builder setSize(int param1Int1, int param1Int2) {
      Preconditions.checkArgumentPositive(param1Int1, "Width should be positive");
      Preconditions.checkArgumentPositive(param1Int2, "Height should be positive");
      this.mWidth = param1Int1;
      this.mHeight = param1Int2;
      return this;
    }
    
    public Builder setInitialZoom(float param1Float) {
      Preconditions.checkArgumentPositive(param1Float, "Zoom should be positive");
      this.mZoom = param1Float;
      return this;
    }
    
    public Builder setElevation(float param1Float) {
      Preconditions.checkArgumentNonNegative(param1Float, "Elevation should be non-negative");
      this.mElevation = param1Float;
      return this;
    }
    
    public Builder setCornerRadius(float param1Float) {
      Preconditions.checkArgumentNonNegative(param1Float, "Corner radius should be non-negative");
      this.mCornerRadius = param1Float;
      return this;
    }
    
    public Builder setOverlay(Drawable param1Drawable) {
      this.mOverlay = param1Drawable;
      return this;
    }
    
    public Builder setDefaultSourceToMagnifierOffset(int param1Int1, int param1Int2) {
      this.mHorizontalDefaultSourceToMagnifierOffset = param1Int1;
      this.mVerticalDefaultSourceToMagnifierOffset = param1Int2;
      return this;
    }
    
    public Builder setClippingEnabled(boolean param1Boolean) {
      this.mClippingEnabled = param1Boolean;
      return this;
    }
    
    public Builder setSourceBounds(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.mLeftContentBound = param1Int1;
      this.mTopContentBound = param1Int2;
      this.mRightContentBound = param1Int3;
      this.mBottomContentBound = param1Int4;
      return this;
    }
    
    Builder setSourceSize(int param1Int1, int param1Int2) {
      this.mSourceWidth = param1Int1;
      this.mSourceHeight = param1Int2;
      return this;
    }
    
    Builder setFishEyeStyle() {
      this.mIsFishEyeStyle = true;
      return this;
    }
    
    public Magnifier build() {
      return new Magnifier(this);
    }
  }
  
  public void setOnOperationCompleteCallback(Callback paramCallback) {
    this.mCallback = paramCallback;
    InternalPopupWindow internalPopupWindow = this.mWindow;
    if (internalPopupWindow != null)
      InternalPopupWindow.access$2502(internalPopupWindow, paramCallback); 
  }
  
  public Bitmap getContent() {
    InternalPopupWindow internalPopupWindow = this.mWindow;
    if (internalPopupWindow == null)
      return null; 
    synchronized (internalPopupWindow.mLock) {
      return this.mWindow.mCurrentContent;
    } 
  }
  
  public Bitmap getOriginalContent() {
    InternalPopupWindow internalPopupWindow = this.mWindow;
    if (internalPopupWindow == null)
      return null; 
    synchronized (internalPopupWindow.mLock) {
      return Bitmap.createBitmap(this.mWindow.mBitmap);
    } 
  }
  
  public static PointF getMagnifierDefaultSize() {
    Resources resources = Resources.getSystem();
    float f = (resources.getDisplayMetrics()).density;
    PointF pointF = new PointF();
    pointF.x = resources.getDimension(17105152) / f;
    pointF.y = resources.getDimension(17105149) / f;
    return pointF;
  }
}
