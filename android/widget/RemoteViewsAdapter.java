package android.widget;

import android.app.IServiceConnection;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.widget.IRemoteViewsFactory;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Executor;

public class RemoteViewsAdapter extends BaseAdapter implements Handler.Callback {
  private static final int CACHE_RESET_CONFIG_FLAGS = -1073737216;
  
  private static final int DEFAULT_CACHE_SIZE = 40;
  
  private static final int DEFAULT_LOADING_VIEW_HEIGHT = 50;
  
  static final int MSG_LOAD_NEXT_ITEM = 3;
  
  private static final int MSG_MAIN_HANDLER_COMMIT_METADATA = 1;
  
  private static final int MSG_MAIN_HANDLER_REMOTE_ADAPTER_CONNECTED = 3;
  
  private static final int MSG_MAIN_HANDLER_REMOTE_ADAPTER_DISCONNECTED = 4;
  
  private static final int MSG_MAIN_HANDLER_REMOTE_VIEWS_LOADED = 5;
  
  private static final int MSG_MAIN_HANDLER_SUPER_NOTIFY_DATA_SET_CHANGED = 2;
  
  static final int MSG_NOTIFY_DATA_SET_CHANGED = 2;
  
  static final int MSG_REQUEST_BIND = 1;
  
  static final int MSG_UNBIND_SERVICE = 4;
  
  private static final int REMOTE_VIEWS_CACHE_DURATION = 5000;
  
  private static final String TAG = "RemoteViewsAdapter";
  
  private static final int UNBIND_SERVICE_DELAY = 5000;
  
  private static Handler sCacheRemovalQueue;
  
  private static HandlerThread sCacheRemovalThread;
  
  private static final HashMap<RemoteViewsCacheKey, FixedSizeRemoteViewsCache> sCachedRemoteViewsCaches = new HashMap<>();
  
  private static final HashMap<RemoteViewsCacheKey, Runnable> sRemoteViewsCacheRemoveRunnables = new HashMap<>();
  
  private final int mAppWidgetId;
  
  private final Executor mAsyncViewLoadExecutor;
  
  private final FixedSizeRemoteViewsCache mCache;
  
  private final RemoteAdapterConnectionCallback mCallback;
  
  private final Context mContext;
  
  private boolean mDataReady = false;
  
  private final Intent mIntent;
  
  private ApplicationInfo mLastRemoteViewAppInfo;
  
  private final Handler mMainHandler;
  
  private final boolean mOnLightBackground;
  
  private RemoteViews.OnClickHandler mRemoteViewsOnClickHandler;
  
  private RemoteViewsFrameLayoutRefSet mRequestedViews;
  
  private final RemoteServiceHandler mServiceHandler;
  
  private int mVisibleWindowLowerBound;
  
  private int mVisibleWindowUpperBound;
  
  private final HandlerThread mWorkerThread;
  
  class AsyncRemoteAdapterAction implements Runnable {
    private final RemoteViewsAdapter.RemoteAdapterConnectionCallback mCallback;
    
    private final Intent mIntent;
    
    public AsyncRemoteAdapterAction(RemoteViewsAdapter this$0, Intent param1Intent) {
      this.mCallback = (RemoteViewsAdapter.RemoteAdapterConnectionCallback)this$0;
      this.mIntent = param1Intent;
    }
    
    public void run() {
      this.mCallback.setRemoteViewsAdapter(this.mIntent, true);
    }
  }
  
  class RemoteServiceHandler extends Handler implements ServiceConnection {
    private IRemoteViewsFactory mRemoteViewsFactory;
    
    private boolean mNotifyDataSetChangedPending = false;
    
    private final Context mContext;
    
    private boolean mBindRequested = false;
    
    private final WeakReference<RemoteViewsAdapter> mAdapter;
    
    RemoteServiceHandler(RemoteViewsAdapter this$0, RemoteViewsAdapter param1RemoteViewsAdapter, Context param1Context) {
      super((Looper)this$0);
      this.mAdapter = new WeakReference<>(param1RemoteViewsAdapter);
      this.mContext = param1Context;
    }
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      Message message;
      this.mRemoteViewsFactory = IRemoteViewsFactory.Stub.asInterface(param1IBinder);
      enqueueDeferredUnbindServiceMessage();
      RemoteViewsAdapter remoteViewsAdapter = this.mAdapter.get();
      if (remoteViewsAdapter == null)
        return; 
      if (this.mNotifyDataSetChangedPending) {
        this.mNotifyDataSetChangedPending = false;
        message = Message.obtain(this, 2);
        handleMessage(message);
        message.recycle();
      } else {
        if (!sendNotifyDataSetChange(false))
          return; 
        message.updateTemporaryMetaData(this.mRemoteViewsFactory);
        ((RemoteViewsAdapter)message).mMainHandler.sendEmptyMessage(1);
        ((RemoteViewsAdapter)message).mMainHandler.sendEmptyMessage(3);
      } 
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      this.mRemoteViewsFactory = null;
      RemoteViewsAdapter remoteViewsAdapter = this.mAdapter.get();
      if (remoteViewsAdapter != null)
        remoteViewsAdapter.mMainHandler.sendEmptyMessage(4); 
    }
    
    public void handleMessage(Message param1Message) {
      RemoteViewsAdapter remoteViewsAdapter = this.mAdapter.get();
      int i = param1Message.what;
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 4)
              return; 
            unbindNow();
            return;
          } 
          if (remoteViewsAdapter == null || this.mRemoteViewsFactory == null)
            return; 
          removeMessages(4);
          i = remoteViewsAdapter.mCache.getNextIndexToLoad();
          if (i > -1) {
            remoteViewsAdapter.updateRemoteViews(this.mRemoteViewsFactory, i, true);
            sendEmptyMessage(3);
          } else {
            enqueueDeferredUnbindServiceMessage();
          } 
          return;
        } 
        enqueueDeferredUnbindServiceMessage();
        if (remoteViewsAdapter == null)
          return; 
        if (this.mRemoteViewsFactory == null) {
          this.mNotifyDataSetChangedPending = true;
          remoteViewsAdapter.requestBindService();
          return;
        } 
        if (!sendNotifyDataSetChange(true))
          return; 
        synchronized (remoteViewsAdapter.mCache) {
          remoteViewsAdapter.mCache.reset();
          remoteViewsAdapter.updateTemporaryMetaData(this.mRemoteViewsFactory);
          synchronized (remoteViewsAdapter.mCache.getTemporaryMetaData()) {
            int j = (remoteViewsAdapter.mCache.getTemporaryMetaData()).count;
            int[] arrayOfInt = remoteViewsAdapter.getVisibleWindow(j);
            for (int k = arrayOfInt.length; i < k; ) {
              int m = arrayOfInt[i];
              if (m < j)
                remoteViewsAdapter.updateRemoteViews(this.mRemoteViewsFactory, m, false); 
              i++;
            } 
            remoteViewsAdapter.mMainHandler.sendEmptyMessage(1);
            remoteViewsAdapter.mMainHandler.sendEmptyMessage(2);
            return;
          } 
        } 
      } 
      if (remoteViewsAdapter == null || this.mRemoteViewsFactory != null)
        enqueueDeferredUnbindServiceMessage(); 
      if (this.mBindRequested)
        return; 
      IServiceConnection iServiceConnection = this.mContext.getServiceDispatcher(this, this, 33554433);
      Intent intent = (Intent)param1Message.obj;
      i = param1Message.arg1;
      try {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.mContext);
        Context context = this.mContext;
        this.mBindRequested = appWidgetManager.bindRemoteViewsService(context, i, intent, iServiceConnection, 33554433);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to bind remoteViewsService: ");
        stringBuilder.append(exception.getMessage());
        Log.e("RemoteViewsAdapter", stringBuilder.toString());
      } 
    }
    
    protected void unbindNow() {
      if (this.mBindRequested) {
        this.mBindRequested = false;
        this.mContext.unbindService(this);
      } 
      this.mRemoteViewsFactory = null;
    }
    
    private boolean sendNotifyDataSetChange(boolean param1Boolean) {
      if (!param1Boolean)
        try {
          if (!this.mRemoteViewsFactory.isCreated()) {
            this.mRemoteViewsFactory.onDataSetChanged();
            return true;
          } 
          return true;
        } catch (RemoteException|RuntimeException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error in updateNotifyDataSetChanged(): ");
          stringBuilder.append(remoteException.getMessage());
          Log.e("RemoteViewsAdapter", stringBuilder.toString());
          return false;
        }  
      this.mRemoteViewsFactory.onDataSetChanged();
      return true;
    }
    
    private void enqueueDeferredUnbindServiceMessage() {
      removeMessages(4);
      sendEmptyMessageDelayed(4, 5000L);
    }
  }
  
  class RemoteViewsFrameLayout extends AppWidgetHostView {
    public int cacheIndex = -1;
    
    private final RemoteViewsAdapter.FixedSizeRemoteViewsCache mCache;
    
    public RemoteViewsFrameLayout(RemoteViewsAdapter this$0, RemoteViewsAdapter.FixedSizeRemoteViewsCache param1FixedSizeRemoteViewsCache) {
      super((Context)this$0);
      this.mCache = param1FixedSizeRemoteViewsCache;
    }
    
    public void onRemoteViewsLoaded(RemoteViews param1RemoteViews, RemoteViews.OnClickHandler param1OnClickHandler, boolean param1Boolean) {
      setOnClickHandler(param1OnClickHandler);
      if (param1Boolean || (param1RemoteViews != null && param1RemoteViews.prefersAsyncApply())) {
        param1Boolean = true;
      } else {
        param1Boolean = false;
      } 
      applyRemoteViews(param1RemoteViews, param1Boolean);
    }
    
    protected View getDefaultView() {
      int i = (this.mCache.getMetaData().getLoadingTemplate(getContext())).defaultHeight;
      TextView textView = (TextView)LayoutInflater.from(getContext()).inflate(17367269, (ViewGroup)this, false);
      textView.setHeight(i);
      return textView;
    }
    
    protected Context getRemoteContext() {
      return null;
    }
    
    protected View getErrorView() {
      return getDefaultView();
    }
  }
  
  class RemoteViewsFrameLayoutRefSet extends SparseArray<LinkedList<RemoteViewsFrameLayout>> {
    final RemoteViewsAdapter this$0;
    
    private RemoteViewsFrameLayoutRefSet() {}
    
    public void add(int param1Int, RemoteViewsAdapter.RemoteViewsFrameLayout param1RemoteViewsFrameLayout) {
      LinkedList<RemoteViewsAdapter.RemoteViewsFrameLayout> linkedList1 = get(param1Int);
      LinkedList<RemoteViewsAdapter.RemoteViewsFrameLayout> linkedList2 = linkedList1;
      if (linkedList1 == null) {
        linkedList2 = new LinkedList<>();
        put(param1Int, linkedList2);
      } 
      param1RemoteViewsFrameLayout.cacheIndex = param1Int;
      linkedList2.add(param1RemoteViewsFrameLayout);
    }
    
    public void notifyOnRemoteViewsLoaded(int param1Int, RemoteViews param1RemoteViews) {
      if (param1RemoteViews == null)
        return; 
      LinkedList<RemoteViewsAdapter.RemoteViewsFrameLayout> linkedList = removeReturnOld(param1Int);
      if (linkedList != null)
        for (RemoteViewsAdapter.RemoteViewsFrameLayout remoteViewsFrameLayout : linkedList)
          remoteViewsFrameLayout.onRemoteViewsLoaded(param1RemoteViews, RemoteViewsAdapter.this.mRemoteViewsOnClickHandler, true);  
    }
    
    public void removeView(RemoteViewsAdapter.RemoteViewsFrameLayout param1RemoteViewsFrameLayout) {
      if (param1RemoteViewsFrameLayout.cacheIndex < 0)
        return; 
      LinkedList<RemoteViewsAdapter.RemoteViewsFrameLayout> linkedList = get(param1RemoteViewsFrameLayout.cacheIndex);
      if (linkedList != null)
        linkedList.remove(param1RemoteViewsFrameLayout); 
      param1RemoteViewsFrameLayout.cacheIndex = -1;
    }
  }
  
  class RemoteViewsMetaData {
    int count;
    
    boolean hasStableIds;
    
    RemoteViewsAdapter.LoadingViewTemplate loadingTemplate;
    
    private final SparseIntArray mTypeIdIndexMap = new SparseIntArray();
    
    int viewTypeCount;
    
    public RemoteViewsMetaData() {
      reset();
    }
    
    public void set(RemoteViewsMetaData param1RemoteViewsMetaData) {
      // Byte code:
      //   0: aload_1
      //   1: monitorenter
      //   2: aload_0
      //   3: aload_1
      //   4: getfield count : I
      //   7: putfield count : I
      //   10: aload_0
      //   11: aload_1
      //   12: getfield viewTypeCount : I
      //   15: putfield viewTypeCount : I
      //   18: aload_0
      //   19: aload_1
      //   20: getfield hasStableIds : Z
      //   23: putfield hasStableIds : Z
      //   26: aload_0
      //   27: aload_1
      //   28: getfield loadingTemplate : Landroid/widget/RemoteViewsAdapter$LoadingViewTemplate;
      //   31: putfield loadingTemplate : Landroid/widget/RemoteViewsAdapter$LoadingViewTemplate;
      //   34: aload_1
      //   35: monitorexit
      //   36: return
      //   37: astore_2
      //   38: aload_1
      //   39: monitorexit
      //   40: aload_2
      //   41: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #497	-> 0
      //   #498	-> 2
      //   #499	-> 10
      //   #500	-> 18
      //   #501	-> 26
      //   #502	-> 34
      //   #503	-> 36
      //   #502	-> 37
      // Exception table:
      //   from	to	target	type
      //   2	10	37	finally
      //   10	18	37	finally
      //   18	26	37	finally
      //   26	34	37	finally
      //   34	36	37	finally
      //   38	40	37	finally
    }
    
    public void reset() {
      this.count = 0;
      this.viewTypeCount = 1;
      this.hasStableIds = true;
      this.loadingTemplate = null;
      this.mTypeIdIndexMap.clear();
    }
    
    public int getMappedViewType(int param1Int) {
      int i = this.mTypeIdIndexMap.get(param1Int, -1);
      int j = i;
      if (i == -1) {
        j = this.mTypeIdIndexMap.size() + 1;
        this.mTypeIdIndexMap.put(param1Int, j);
      } 
      return j;
    }
    
    public boolean isViewTypeInRange(int param1Int) {
      boolean bool;
      param1Int = getMappedViewType(param1Int);
      if (param1Int < this.viewTypeCount) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public RemoteViewsAdapter.LoadingViewTemplate getLoadingTemplate(Context param1Context) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield loadingTemplate : Landroid/widget/RemoteViewsAdapter$LoadingViewTemplate;
      //   6: ifnonnull -> 24
      //   9: new android/widget/RemoteViewsAdapter$LoadingViewTemplate
      //   12: astore_2
      //   13: aload_2
      //   14: aconst_null
      //   15: aload_1
      //   16: invokespecial <init> : (Landroid/widget/RemoteViews;Landroid/content/Context;)V
      //   19: aload_0
      //   20: aload_2
      //   21: putfield loadingTemplate : Landroid/widget/RemoteViewsAdapter$LoadingViewTemplate;
      //   24: aload_0
      //   25: getfield loadingTemplate : Landroid/widget/RemoteViewsAdapter$LoadingViewTemplate;
      //   28: astore_1
      //   29: aload_0
      //   30: monitorexit
      //   31: aload_1
      //   32: areturn
      //   33: astore_1
      //   34: aload_0
      //   35: monitorexit
      //   36: aload_1
      //   37: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #531	-> 2
      //   #532	-> 9
      //   #534	-> 24
      //   #530	-> 33
      // Exception table:
      //   from	to	target	type
      //   2	9	33	finally
      //   9	24	33	finally
      //   24	29	33	finally
    }
  }
  
  class RemoteViewsIndexMetaData {
    long itemId;
    
    int typeId;
    
    public RemoteViewsIndexMetaData(RemoteViewsAdapter this$0, long param1Long) {
      set((RemoteViews)this$0, param1Long);
    }
    
    public void set(RemoteViews param1RemoteViews, long param1Long) {
      this.itemId = param1Long;
      if (param1RemoteViews != null) {
        this.typeId = param1RemoteViews.getLayoutId();
      } else {
        this.typeId = 0;
      } 
    }
  }
  
  class FixedSizeRemoteViewsCache {
    private final RemoteViewsAdapter.RemoteViewsMetaData mMetaData = new RemoteViewsAdapter.RemoteViewsMetaData();
    
    private final RemoteViewsAdapter.RemoteViewsMetaData mTemporaryMetaData = new RemoteViewsAdapter.RemoteViewsMetaData();
    
    private final SparseArray<RemoteViewsAdapter.RemoteViewsIndexMetaData> mIndexMetaData = new SparseArray<>();
    
    private final SparseArray<RemoteViews> mIndexRemoteViews = new SparseArray<>();
    
    private final SparseBooleanArray mIndicesToLoad = new SparseBooleanArray();
    
    private static final float sMaxCountSlackPercent = 0.75F;
    
    private static final int sMaxMemoryLimitInBytes = 2097152;
    
    private final Configuration mConfiguration;
    
    private int mLastRequestedIndex;
    
    private final int mMaxCount;
    
    private final int mMaxCountSlack;
    
    private int mPreloadLowerBound;
    
    private int mPreloadUpperBound;
    
    FixedSizeRemoteViewsCache(RemoteViewsAdapter this$0, Configuration param1Configuration) {
      this.mMaxCount = this$0;
      this.mMaxCountSlack = Math.round((this$0 / 2) * 0.75F);
      this.mPreloadLowerBound = 0;
      this.mPreloadUpperBound = -1;
      this.mLastRequestedIndex = -1;
      this.mConfiguration = new Configuration(param1Configuration);
    }
    
    public void insert(int param1Int, RemoteViews param1RemoteViews, long param1Long, int[] param1ArrayOfint) {
      if (this.mIndexRemoteViews.size() >= this.mMaxCount)
        this.mIndexRemoteViews.remove(getFarthestPositionFrom(param1Int, param1ArrayOfint)); 
      int i = this.mLastRequestedIndex;
      if (i <= -1)
        i = param1Int; 
      while (getRemoteViewsBitmapMemoryUsage() >= 2097152) {
        int j = getFarthestPositionFrom(i, param1ArrayOfint);
        if (j < 0)
          break; 
        this.mIndexRemoteViews.remove(j);
      } 
      RemoteViewsAdapter.RemoteViewsIndexMetaData remoteViewsIndexMetaData = this.mIndexMetaData.get(param1Int);
      if (remoteViewsIndexMetaData != null) {
        remoteViewsIndexMetaData.set(param1RemoteViews, param1Long);
      } else {
        this.mIndexMetaData.put(param1Int, new RemoteViewsAdapter.RemoteViewsIndexMetaData(param1RemoteViews, param1Long));
      } 
      this.mIndexRemoteViews.put(param1Int, param1RemoteViews);
    }
    
    public RemoteViewsAdapter.RemoteViewsMetaData getMetaData() {
      return this.mMetaData;
    }
    
    public RemoteViewsAdapter.RemoteViewsMetaData getTemporaryMetaData() {
      return this.mTemporaryMetaData;
    }
    
    public RemoteViews getRemoteViewsAt(int param1Int) {
      return this.mIndexRemoteViews.get(param1Int);
    }
    
    public RemoteViewsAdapter.RemoteViewsIndexMetaData getMetaDataAt(int param1Int) {
      return this.mIndexMetaData.get(param1Int);
    }
    
    public void commitTemporaryMetaData() {
      synchronized (this.mTemporaryMetaData) {
        synchronized (this.mMetaData) {
          this.mMetaData.set(this.mTemporaryMetaData);
          return;
        } 
      } 
    }
    
    private int getRemoteViewsBitmapMemoryUsage() {
      int i = 0;
      for (int j = this.mIndexRemoteViews.size() - 1; j >= 0; j--, i = k) {
        RemoteViews remoteViews = this.mIndexRemoteViews.valueAt(j);
        int k = i;
        if (remoteViews != null)
          k = i + remoteViews.estimateMemoryUsage(); 
      } 
      return i;
    }
    
    private int getFarthestPositionFrom(int param1Int, int[] param1ArrayOfint) {
      int i = 0;
      int j = -1;
      int k = 0;
      int m = -1;
      for (int n = this.mIndexRemoteViews.size() - 1; n >= 0; n--, i = m, k = i3, m = i4) {
        int i1 = this.mIndexRemoteViews.keyAt(n);
        int i2 = Math.abs(i1 - param1Int);
        int i3 = k, i4 = m;
        if (i2 > k) {
          i3 = k;
          i4 = m;
          if (Arrays.binarySearch(param1ArrayOfint, i1) < 0) {
            i4 = i1;
            i3 = i2;
          } 
        } 
        m = i;
        if (i2 >= i) {
          j = i1;
          m = i2;
        } 
      } 
      if (m > -1)
        return m; 
      return j;
    }
    
    public void queueRequestedPositionToLoad(int param1Int) {
      this.mLastRequestedIndex = param1Int;
      synchronized (this.mIndicesToLoad) {
        this.mIndicesToLoad.put(param1Int, true);
        return;
      } 
    }
    
    public boolean queuePositionsToBePreloadedFromRequestedPosition(int param1Int) {
      int i = this.mPreloadLowerBound;
      if (i <= param1Int) {
        int j = this.mPreloadUpperBound;
        if (param1Int <= j) {
          i = (j + i) / 2;
          if (Math.abs(param1Int - i) < this.mMaxCountSlack)
            return false; 
        } 
      } 
      synchronized (this.mMetaData) {
        int j = this.mMetaData.count;
        synchronized (this.mIndicesToLoad) {
          for (i = this.mIndicesToLoad.size() - 1; i >= 0; i--) {
            if (!this.mIndicesToLoad.valueAt(i))
              this.mIndicesToLoad.removeAt(i); 
          } 
          i = this.mMaxCount / 2;
          int k = param1Int - i;
          this.mPreloadUpperBound = param1Int + i;
          param1Int = Math.max(0, k);
          i = Math.min(this.mPreloadUpperBound, j - 1);
          for (; param1Int <= i; param1Int++) {
            if (this.mIndexRemoteViews.indexOfKey(param1Int) < 0 && !this.mIndicesToLoad.get(param1Int))
              this.mIndicesToLoad.put(param1Int, false); 
          } 
          return true;
        } 
      } 
    }
    
    public int getNextIndexToLoad() {
      synchronized (this.mIndicesToLoad) {
        int i = this.mIndicesToLoad.indexOfValue(true);
        int j = i;
        if (i < 0)
          j = this.mIndicesToLoad.indexOfValue(false); 
        if (j < 0)
          return -1; 
        i = this.mIndicesToLoad.keyAt(j);
        this.mIndicesToLoad.removeAt(j);
        return i;
      } 
    }
    
    public boolean containsRemoteViewAt(int param1Int) {
      boolean bool;
      if (this.mIndexRemoteViews.indexOfKey(param1Int) >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean containsMetaDataAt(int param1Int) {
      boolean bool;
      if (this.mIndexMetaData.indexOfKey(param1Int) >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void reset() {
      this.mPreloadLowerBound = 0;
      this.mPreloadUpperBound = -1;
      this.mLastRequestedIndex = -1;
      this.mIndexRemoteViews.clear();
      this.mIndexMetaData.clear();
      synchronized (this.mIndicesToLoad) {
        this.mIndicesToLoad.clear();
        return;
      } 
    }
  }
  
  class RemoteViewsCacheKey {
    final Intent.FilterComparison filter;
    
    final int widgetId;
    
    RemoteViewsCacheKey(RemoteViewsAdapter this$0, int param1Int) {
      this.filter = (Intent.FilterComparison)this$0;
      this.widgetId = param1Int;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof RemoteViewsCacheKey;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (((RemoteViewsCacheKey)param1Object).filter.equals(this.filter)) {
        bool = bool1;
        if (((RemoteViewsCacheKey)param1Object).widgetId == this.widgetId)
          bool = true; 
      } 
      return bool;
    }
    
    public int hashCode() {
      int i;
      Intent.FilterComparison filterComparison = this.filter;
      if (filterComparison == null) {
        i = 0;
      } else {
        i = filterComparison.hashCode();
      } 
      return i ^ this.widgetId << 2;
    }
  }
  
  public RemoteViewsAdapter(Context paramContext, Intent paramIntent, RemoteAdapterConnectionCallback paramRemoteAdapterConnectionCallback, boolean paramBoolean) {
    this.mContext = paramContext;
    this.mIntent = paramIntent;
    if (paramIntent != null) {
      HandlerThreadExecutor handlerThreadExecutor;
      this.mAppWidgetId = paramIntent.getIntExtra("remoteAdapterAppWidgetId", -1);
      Looper looper2 = null;
      this.mRequestedViews = new RemoteViewsFrameLayoutRefSet();
      this.mOnLightBackground = paramIntent.getBooleanExtra("remoteAdapterOnLightBackground", false);
      paramIntent.removeExtra("remoteAdapterAppWidgetId");
      paramIntent.removeExtra("remoteAdapterOnLightBackground");
      HandlerThread handlerThread = new HandlerThread("RemoteViewsCache-loader");
      handlerThread.start();
      this.mMainHandler = new Handler(Looper.myLooper(), this);
      Looper looper1 = this.mWorkerThread.getLooper();
      this.mServiceHandler = new RemoteServiceHandler(looper1, this, paramContext.getApplicationContext());
      looper1 = looper2;
      if (paramBoolean)
        handlerThreadExecutor = new HandlerThreadExecutor(this.mWorkerThread); 
      this.mAsyncViewLoadExecutor = handlerThreadExecutor;
      this.mCallback = paramRemoteAdapterConnectionCallback;
      if (sCacheRemovalThread == null) {
        HandlerThread handlerThread1 = new HandlerThread("RemoteViewsAdapter-cachePruner");
        handlerThread1.start();
        sCacheRemovalQueue = new Handler(sCacheRemovalThread.getLooper());
      } 
      RemoteViewsCacheKey remoteViewsCacheKey = new RemoteViewsCacheKey(new Intent.FilterComparison(this.mIntent), this.mAppWidgetId);
      synchronized (sCachedRemoteViewsCaches) {
        FixedSizeRemoteViewsCache fixedSizeRemoteViewsCache1, fixedSizeRemoteViewsCache2 = sCachedRemoteViewsCaches.get(remoteViewsCacheKey);
        Configuration configuration = paramContext.getResources().getConfiguration();
        if (fixedSizeRemoteViewsCache2 == null || (
          fixedSizeRemoteViewsCache2.mConfiguration.diff(configuration) & 0xC0001200) != 0) {
          fixedSizeRemoteViewsCache1 = new FixedSizeRemoteViewsCache();
          this(40, configuration);
          this.mCache = fixedSizeRemoteViewsCache1;
        } else {
          this.mCache = null = sCachedRemoteViewsCaches.get(fixedSizeRemoteViewsCache1);
          synchronized (null.mMetaData) {
            if (this.mCache.mMetaData.count > 0)
              this.mDataReady = true; 
          } 
        } 
        if (!this.mDataReady)
          requestBindService(); 
        return;
      } 
    } 
    throw new IllegalArgumentException("Non-null Intent must be specified.");
  }
  
  protected void finalize() throws Throwable {
    try {
      this.mServiceHandler.unbindNow();
      this.mWorkerThread.quit();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public boolean isDataReady() {
    return this.mDataReady;
  }
  
  public void setRemoteViewsOnClickHandler(RemoteViews.OnClickHandler paramOnClickHandler) {
    this.mRemoteViewsOnClickHandler = paramOnClickHandler;
  }
  
  public void saveRemoteViewsCache() {
    null = new RemoteViewsCacheKey(new Intent.FilterComparison(this.mIntent), this.mAppWidgetId);
    synchronized (sCachedRemoteViewsCaches) {
      if (sRemoteViewsCacheRemoveRunnables.containsKey(null)) {
        sCacheRemovalQueue.removeCallbacks(sRemoteViewsCacheRemoveRunnables.get(null));
        sRemoteViewsCacheRemoveRunnables.remove(null);
      } 
      synchronized (this.mCache.mMetaData) {
        FixedSizeRemoteViewsCache fixedSizeRemoteViewsCache;
        _$$Lambda$RemoteViewsAdapter$_xHEGE7CkOWJ8u7GAjsH_hc_iiA _$$Lambda$RemoteViewsAdapter$_xHEGE7CkOWJ8u7GAjsH_hc_iiA;
        int i = this.mCache.mMetaData.count;
        synchronized (this.mCache) {
          int j = this.mCache.mIndexRemoteViews.size();
          if (i > 0 && j > 0)
            sCachedRemoteViewsCaches.put(null, this.mCache); 
          _$$Lambda$RemoteViewsAdapter$_xHEGE7CkOWJ8u7GAjsH_hc_iiA = new _$$Lambda$RemoteViewsAdapter$_xHEGE7CkOWJ8u7GAjsH_hc_iiA();
          this(null);
          sRemoteViewsCacheRemoveRunnables.put(null, _$$Lambda$RemoteViewsAdapter$_xHEGE7CkOWJ8u7GAjsH_hc_iiA);
          sCacheRemovalQueue.postDelayed(_$$Lambda$RemoteViewsAdapter$_xHEGE7CkOWJ8u7GAjsH_hc_iiA, 5000L);
          return;
        } 
      } 
    } 
  }
  
  private void updateTemporaryMetaData(IRemoteViewsFactory paramIRemoteViewsFactory) {
    try {
      boolean bool = paramIRemoteViewsFactory.hasStableIds();
      int i = paramIRemoteViewsFactory.getViewTypeCount();
      int j = paramIRemoteViewsFactory.getCount();
      null = new LoadingViewTemplate();
      this(paramIRemoteViewsFactory.getLoadingView(), this.mContext);
      if (j > 0 && null.remoteViews == null) {
        RemoteViews remoteViews = paramIRemoteViewsFactory.getViewAt(0);
        if (remoteViews != null) {
          Context context = this.mContext;
          HandlerThreadExecutor handlerThreadExecutor = new HandlerThreadExecutor();
          this(this.mWorkerThread);
          null.loadFirstViewHeight(remoteViews, context, handlerThreadExecutor);
        } 
      } 
      synchronized (this.mCache.getTemporaryMetaData()) {
        null.hasStableIds = bool;
        null.viewTypeCount = i + 1;
        null.count = j;
        null.loadingTemplate = null;
      } 
    } catch (RemoteException|RuntimeException null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error in updateMetaData: ");
      stringBuilder.append(null.getMessage());
      Log.e("RemoteViewsAdapter", stringBuilder.toString());
      synchronized (this.mCache.getMetaData()) {
        this.mCache.getMetaData().reset();
        synchronized (this.mCache) {
          this.mCache.reset();
          this.mMainHandler.sendEmptyMessage(2);
          return;
        } 
      } 
    } 
  }
  
  private void updateRemoteViews(IRemoteViewsFactory paramIRemoteViewsFactory, int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: iload_2
    //   2: invokeinterface getViewAt : (I)Landroid/widget/RemoteViews;
    //   7: astore #4
    //   9: aload_1
    //   10: iload_2
    //   11: invokeinterface getItemId : (I)J
    //   16: lstore #5
    //   18: aload #4
    //   20: ifnull -> 195
    //   23: aload #4
    //   25: getfield mApplication : Landroid/content/pm/ApplicationInfo;
    //   28: ifnull -> 70
    //   31: aload_0
    //   32: getfield mLastRemoteViewAppInfo : Landroid/content/pm/ApplicationInfo;
    //   35: astore_1
    //   36: aload_1
    //   37: ifnull -> 61
    //   40: aload #4
    //   42: aload_1
    //   43: invokevirtual hasSameAppInfo : (Landroid/content/pm/ApplicationInfo;)Z
    //   46: ifeq -> 61
    //   49: aload #4
    //   51: aload_0
    //   52: getfield mLastRemoteViewAppInfo : Landroid/content/pm/ApplicationInfo;
    //   55: putfield mApplication : Landroid/content/pm/ApplicationInfo;
    //   58: goto -> 70
    //   61: aload_0
    //   62: aload #4
    //   64: getfield mApplication : Landroid/content/pm/ApplicationInfo;
    //   67: putfield mLastRemoteViewAppInfo : Landroid/content/pm/ApplicationInfo;
    //   70: aload #4
    //   72: invokevirtual getLayoutId : ()I
    //   75: istore #7
    //   77: aload_0
    //   78: getfield mCache : Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
    //   81: invokevirtual getMetaData : ()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    //   84: astore_1
    //   85: aload_1
    //   86: monitorenter
    //   87: aload_1
    //   88: iload #7
    //   90: invokevirtual isViewTypeInRange : (I)Z
    //   93: istore #8
    //   95: aload_0
    //   96: getfield mCache : Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
    //   99: invokestatic access$900 : (Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    //   102: getfield count : I
    //   105: istore #7
    //   107: aload_1
    //   108: monitorexit
    //   109: aload_0
    //   110: getfield mCache : Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
    //   113: astore_1
    //   114: aload_1
    //   115: monitorenter
    //   116: iload #8
    //   118: ifeq -> 169
    //   121: aload_0
    //   122: iload #7
    //   124: invokespecial getVisibleWindow : (I)[I
    //   127: astore #9
    //   129: aload_0
    //   130: getfield mCache : Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
    //   133: iload_2
    //   134: aload #4
    //   136: lload #5
    //   138: aload #9
    //   140: invokevirtual insert : (ILandroid/widget/RemoteViews;J[I)V
    //   143: iload_3
    //   144: ifeq -> 166
    //   147: aload_0
    //   148: getfield mMainHandler : Landroid/os/Handler;
    //   151: iconst_5
    //   152: iload_2
    //   153: iconst_0
    //   154: aload #4
    //   156: invokestatic obtain : (Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;
    //   159: astore #4
    //   161: aload #4
    //   163: invokevirtual sendToTarget : ()V
    //   166: goto -> 178
    //   169: ldc 'RemoteViewsAdapter'
    //   171: ldc_w 'Error: widget's RemoteViewsFactory returns more view types than  indicated by getViewTypeCount() '
    //   174: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   177: pop
    //   178: aload_1
    //   179: monitorexit
    //   180: return
    //   181: astore #4
    //   183: aload_1
    //   184: monitorexit
    //   185: aload #4
    //   187: athrow
    //   188: astore #4
    //   190: aload_1
    //   191: monitorexit
    //   192: aload #4
    //   194: athrow
    //   195: new java/lang/RuntimeException
    //   198: astore_1
    //   199: aload_1
    //   200: ldc_w 'Null remoteViews'
    //   203: invokespecial <init> : (Ljava/lang/String;)V
    //   206: aload_1
    //   207: athrow
    //   208: astore_1
    //   209: new java/lang/StringBuilder
    //   212: dup
    //   213: invokespecial <init> : ()V
    //   216: astore #4
    //   218: aload #4
    //   220: ldc_w 'Error in updateRemoteViews('
    //   223: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload #4
    //   229: iload_2
    //   230: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   233: pop
    //   234: aload #4
    //   236: ldc_w '): '
    //   239: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: aload #4
    //   245: aload_1
    //   246: invokevirtual getMessage : ()Ljava/lang/String;
    //   249: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   252: pop
    //   253: ldc 'RemoteViewsAdapter'
    //   255: aload #4
    //   257: invokevirtual toString : ()Ljava/lang/String;
    //   260: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   263: pop
    //   264: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #994	-> 0
    //   #995	-> 9
    //   #997	-> 18
    //   #1006	-> 23
    //   #1008	-> 23
    //   #1012	-> 31
    //   #1013	-> 40
    //   #1016	-> 49
    //   #1018	-> 61
    //   #1022	-> 70
    //   #1023	-> 77
    //   #1026	-> 85
    //   #1027	-> 87
    //   #1028	-> 95
    //   #1029	-> 107
    //   #1030	-> 109
    //   #1031	-> 116
    //   #1032	-> 121
    //   #1034	-> 129
    //   #1036	-> 143
    //   #1039	-> 147
    //   #1040	-> 161
    //   #1042	-> 166
    //   #1047	-> 169
    //   #1050	-> 178
    //   #1051	-> 180
    //   #1050	-> 181
    //   #1029	-> 188
    //   #998	-> 195
    //   #1000	-> 208
    //   #1001	-> 209
    //   #1005	-> 264
    // Exception table:
    //   from	to	target	type
    //   0	9	208	android/os/RemoteException
    //   0	9	208	java/lang/RuntimeException
    //   9	18	208	android/os/RemoteException
    //   9	18	208	java/lang/RuntimeException
    //   87	95	188	finally
    //   95	107	188	finally
    //   107	109	188	finally
    //   121	129	181	finally
    //   129	143	181	finally
    //   147	161	181	finally
    //   161	166	181	finally
    //   169	178	181	finally
    //   178	180	181	finally
    //   183	185	181	finally
    //   190	192	188	finally
    //   195	208	208	android/os/RemoteException
    //   195	208	208	java/lang/RuntimeException
  }
  
  public Intent getRemoteViewsServiceIntent() {
    return this.mIntent;
  }
  
  public int getCount() {
    synchronized (this.mCache.getMetaData()) {
      return null.count;
    } 
  }
  
  public Object getItem(int paramInt) {
    return null;
  }
  
  public long getItemId(int paramInt) {
    synchronized (this.mCache) {
      if (this.mCache.containsMetaDataAt(paramInt))
        return (this.mCache.getMetaDataAt(paramInt)).itemId; 
      return 0L;
    } 
  }
  
  public int getItemViewType(int paramInt) {
    synchronized (this.mCache) {
      if (this.mCache.containsMetaDataAt(paramInt)) {
        paramInt = (this.mCache.getMetaDataAt(paramInt)).typeId;
        synchronized (this.mCache.getMetaData()) {
          paramInt = null.getMappedViewType(paramInt);
          return paramInt;
        } 
      } 
      return 0;
    } 
  }
  
  public void setVisibleRangeHint(int paramInt1, int paramInt2) {
    this.mVisibleWindowLowerBound = paramInt1;
    this.mVisibleWindowUpperBound = paramInt2;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    synchronized (this.mCache) {
      RemoteViewsFrameLayout remoteViewsFrameLayout;
      boolean bool;
      RemoteViews remoteViews = this.mCache.getRemoteViewsAt(paramInt);
      if (remoteViews != null) {
        bool = true;
      } else {
        bool = false;
      } 
      boolean bool1 = false;
      if (paramView != null && paramView instanceof RemoteViewsFrameLayout)
        this.mRequestedViews.removeView((RemoteViewsFrameLayout)paramView); 
      if (!bool) {
        requestBindService();
      } else {
        bool1 = this.mCache.queuePositionsToBePreloadedFromRequestedPosition(paramInt);
      } 
      if (paramView instanceof RemoteViewsFrameLayout) {
        remoteViewsFrameLayout = (RemoteViewsFrameLayout)paramView;
      } else {
        remoteViewsFrameLayout = new RemoteViewsFrameLayout();
        this(paramViewGroup.getContext(), this.mCache);
        remoteViewsFrameLayout.setExecutor(this.mAsyncViewLoadExecutor);
        remoteViewsFrameLayout.setOnLightBackground(this.mOnLightBackground);
      } 
      if (bool) {
        remoteViewsFrameLayout.onRemoteViewsLoaded(remoteViews, this.mRemoteViewsOnClickHandler, false);
        if (bool1)
          this.mServiceHandler.sendEmptyMessage(3); 
      } else {
        FixedSizeRemoteViewsCache fixedSizeRemoteViewsCache = this.mCache;
        remoteViews = (fixedSizeRemoteViewsCache.getMetaData().getLoadingTemplate(this.mContext)).remoteViews;
        RemoteViews.OnClickHandler onClickHandler = this.mRemoteViewsOnClickHandler;
        remoteViewsFrameLayout.onRemoteViewsLoaded(remoteViews, onClickHandler, false);
        this.mRequestedViews.add(paramInt, remoteViewsFrameLayout);
        this.mCache.queueRequestedPositionToLoad(paramInt);
        this.mServiceHandler.sendEmptyMessage(3);
      } 
      return (View)remoteViewsFrameLayout;
    } 
  }
  
  public int getViewTypeCount() {
    synchronized (this.mCache.getMetaData()) {
      return null.viewTypeCount;
    } 
  }
  
  public boolean hasStableIds() {
    synchronized (this.mCache.getMetaData()) {
      return null.hasStableIds;
    } 
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (getCount() <= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int[] getVisibleWindow(int paramInt) {
    int arrayOfInt[], i = this.mVisibleWindowLowerBound;
    int j = this.mVisibleWindowUpperBound;
    if ((i == 0 && j == 0) || i < 0 || j < 0)
      return new int[0]; 
    if (i <= j) {
      arrayOfInt = new int[j + 1 - i];
      for (paramInt = 0; i <= j; i++, paramInt++)
        arrayOfInt[paramInt] = i; 
    } else {
      int k = Math.max(paramInt, i);
      int[] arrayOfInt1 = new int[k - i + j + 1];
      paramInt = 0;
      for (byte b = 0; b <= j; b++, paramInt++)
        arrayOfInt1[paramInt] = b; 
      while (true) {
        arrayOfInt = arrayOfInt1;
        if (i < k) {
          arrayOfInt1[paramInt] = i;
          i++;
          paramInt++;
          continue;
        } 
        break;
      } 
    } 
    return arrayOfInt;
  }
  
  public void notifyDataSetChanged() {
    this.mServiceHandler.removeMessages(4);
    this.mServiceHandler.sendEmptyMessage(2);
  }
  
  void superNotifyDataSetChanged() {
    super.notifyDataSetChanged();
  }
  
  public boolean handleMessage(Message paramMessage) {
    int i = paramMessage.what;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 5)
              return false; 
            this.mRequestedViews.notifyOnRemoteViewsLoaded(paramMessage.arg1, (RemoteViews)paramMessage.obj);
            return true;
          } 
          RemoteAdapterConnectionCallback remoteAdapterConnectionCallback1 = this.mCallback;
          if (remoteAdapterConnectionCallback1 != null)
            remoteAdapterConnectionCallback1.onRemoteAdapterDisconnected(); 
          return true;
        } 
        RemoteAdapterConnectionCallback remoteAdapterConnectionCallback = this.mCallback;
        if (remoteAdapterConnectionCallback != null)
          remoteAdapterConnectionCallback.onRemoteAdapterConnected(); 
        return true;
      } 
      superNotifyDataSetChanged();
      return true;
    } 
    this.mCache.commitTemporaryMetaData();
    return true;
  }
  
  private void requestBindService() {
    this.mServiceHandler.removeMessages(4);
    Message.obtain(this.mServiceHandler, 1, this.mAppWidgetId, 0, this.mIntent).sendToTarget();
  }
  
  class HandlerThreadExecutor implements Executor {
    private final HandlerThread mThread;
    
    HandlerThreadExecutor(RemoteViewsAdapter this$0) {
      this.mThread = (HandlerThread)this$0;
    }
    
    public void execute(Runnable param1Runnable) {
      if (Thread.currentThread().getId() == this.mThread.getId()) {
        param1Runnable.run();
      } else {
        (new Handler(this.mThread.getLooper())).post(param1Runnable);
      } 
    }
  }
  
  class LoadingViewTemplate {
    public int defaultHeight;
    
    public final RemoteViews remoteViews;
    
    LoadingViewTemplate(RemoteViewsAdapter this$0, Context param1Context) {
      this.remoteViews = (RemoteViews)this$0;
      float f = (param1Context.getResources().getDisplayMetrics()).density;
      this.defaultHeight = Math.round(50.0F * f);
    }
    
    public void loadFirstViewHeight(RemoteViews param1RemoteViews, Context param1Context, Executor param1Executor) {
      param1RemoteViews.applyAsync(param1Context, (ViewGroup)new RemoteViewsAdapter.RemoteViewsFrameLayout(param1Context, null), param1Executor, (RemoteViews.OnViewAppliedListener)new Object(this));
    }
  }
  
  class RemoteAdapterConnectionCallback {
    public abstract void deferNotifyDataSetChanged();
    
    public abstract boolean onRemoteAdapterConnected();
    
    public abstract void onRemoteAdapterDisconnected();
    
    public abstract void setRemoteViewsAdapter(Intent param1Intent, boolean param1Boolean);
  }
}
