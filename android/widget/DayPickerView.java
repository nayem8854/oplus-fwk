package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.icu.util.Calendar;
import android.util.AttributeSet;
import android.util.MathUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityManager;
import com.android.internal.R;
import com.android.internal.widget.ViewPager;
import java.util.Locale;
import libcore.icu.LocaleData;

class DayPickerView extends ViewGroup {
  private static final int[] ATTRS_TEXT_COLOR = new int[] { 16842904 };
  
  private final Calendar mSelectedDay = Calendar.getInstance();
  
  private final Calendar mMinDate = Calendar.getInstance();
  
  private final Calendar mMaxDate = Calendar.getInstance();
  
  private static final int DEFAULT_END_YEAR = 2100;
  
  private static final int DEFAULT_LAYOUT = 17367145;
  
  private static final int DEFAULT_START_YEAR = 1900;
  
  private final AccessibilityManager mAccessibilityManager;
  
  private final DayPickerPagerAdapter mAdapter;
  
  private final ImageButton mNextButton;
  
  private final View.OnClickListener mOnClickListener;
  
  private OnDaySelectedListener mOnDaySelectedListener;
  
  private final ViewPager.OnPageChangeListener mOnPageChangedListener;
  
  private final ImageButton mPrevButton;
  
  private Calendar mTempCalendar;
  
  private final ViewPager mViewPager;
  
  public DayPickerView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public DayPickerView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843613);
  }
  
  public DayPickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public DayPickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mOnPageChangedListener = (ViewPager.OnPageChangeListener)new Object(this);
    this.mOnClickListener = (View.OnClickListener)new Object(this);
    this.mAccessibilityManager = (AccessibilityManager)paramContext.getSystemService("accessibility");
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CalendarView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.CalendarView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    paramInt1 = (LocaleData.get(Locale.getDefault())).firstDayOfWeek.intValue();
    paramInt1 = typedArray.getInt(0, paramInt1);
    String str2 = typedArray.getString(2);
    String str1 = typedArray.getString(3);
    int i = typedArray.getResourceId(16, 16974791);
    paramInt2 = typedArray.getResourceId(11, 16974790);
    int j = typedArray.getResourceId(12, 16974789);
    ColorStateList colorStateList = typedArray.getColorStateList(15);
    typedArray.recycle();
    DayPickerPagerAdapter dayPickerPagerAdapter = new DayPickerPagerAdapter(paramContext, 17367143, 16909192);
    dayPickerPagerAdapter.setMonthTextAppearance(i);
    this.mAdapter.setDayOfWeekTextAppearance(paramInt2);
    this.mAdapter.setDayTextAppearance(j);
    this.mAdapter.setDaySelectorColor(colorStateList);
    LayoutInflater layoutInflater = LayoutInflater.from(paramContext);
    ViewGroup viewGroup = (ViewGroup)layoutInflater.inflate(17367145, this, false);
    while (viewGroup.getChildCount() > 0) {
      View view = viewGroup.getChildAt(0);
      viewGroup.removeViewAt(0);
      addView(view);
    } 
    ImageButton imageButton = findViewById(16909317);
    imageButton.setOnClickListener(this.mOnClickListener);
    this.mNextButton = imageButton = findViewById(16909207);
    imageButton.setOnClickListener(this.mOnClickListener);
    ViewPager viewPager = findViewById(16908915);
    viewPager.setAdapter(this.mAdapter);
    this.mViewPager.setOnPageChangeListener(this.mOnPageChangedListener);
    if (i != 0) {
      TypedArray typedArray1 = this.mContext.obtainStyledAttributes(null, ATTRS_TEXT_COLOR, 0, i);
      ColorStateList colorStateList1 = typedArray1.getColorStateList(0);
      if (colorStateList1 != null) {
        this.mPrevButton.setImageTintList(colorStateList1);
        this.mNextButton.setImageTintList(colorStateList1);
      } 
      typedArray1.recycle();
    } 
    Calendar calendar = Calendar.getInstance();
    if (!CalendarView.parseDate(str2, calendar))
      calendar.set(1900, 0, 1); 
    long l1 = calendar.getTimeInMillis();
    if (!CalendarView.parseDate(str1, calendar))
      calendar.set(2100, 11, 31); 
    long l2 = calendar.getTimeInMillis();
    if (l2 >= l1) {
      long l = System.currentTimeMillis();
      l = MathUtils.constrain(l, l1, l2);
      setFirstDayOfWeek(paramInt1);
      setMinDate(l1);
      setMaxDate(l2);
      setDate(l, false);
      this.mAdapter.setOnDaySelectedListener((DayPickerPagerAdapter.OnDaySelectedListener)new Object(this));
      return;
    } 
    throw new IllegalArgumentException("maxDate must be >= minDate");
  }
  
  private void updateButtonVisibility(int paramInt) {
    byte b;
    boolean bool1 = true, bool2 = false;
    if (paramInt > 0) {
      b = 1;
    } else {
      b = 0;
    } 
    if (paramInt < this.mAdapter.getCount() - 1) {
      paramInt = bool1;
    } else {
      paramInt = 0;
    } 
    ImageButton imageButton = this.mPrevButton;
    if (b) {
      b = 0;
    } else {
      b = 4;
    } 
    imageButton.setVisibility(b);
    imageButton = this.mNextButton;
    if (paramInt != 0) {
      paramInt = bool2;
    } else {
      paramInt = 4;
    } 
    imageButton.setVisibility(paramInt);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    ViewPager viewPager = this.mViewPager;
    measureChild((View)viewPager, paramInt1, paramInt2);
    paramInt2 = viewPager.getMeasuredWidthAndState();
    paramInt1 = viewPager.getMeasuredHeightAndState();
    setMeasuredDimension(paramInt2, paramInt1);
    paramInt2 = viewPager.getMeasuredWidth();
    paramInt1 = viewPager.getMeasuredHeight();
    paramInt2 = View.MeasureSpec.makeMeasureSpec(paramInt2, -2147483648);
    paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, -2147483648);
    this.mPrevButton.measure(paramInt2, paramInt1);
    this.mNextButton.measure(paramInt2, paramInt1);
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    requestLayout();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    ImageButton imageButton1, imageButton2;
    if (isLayoutRtl()) {
      imageButton1 = this.mNextButton;
      imageButton2 = this.mPrevButton;
    } else {
      imageButton1 = this.mPrevButton;
      imageButton2 = this.mNextButton;
    } 
    paramInt1 = paramInt3 - paramInt1;
    this.mViewPager.layout(0, 0, paramInt1, paramInt4 - paramInt2);
    SimpleMonthView simpleMonthView = (SimpleMonthView)this.mViewPager.getChildAt(0);
    paramInt3 = simpleMonthView.getMonthHeight();
    paramInt2 = simpleMonthView.getCellWidth();
    paramInt4 = imageButton1.getMeasuredWidth();
    int i = imageButton1.getMeasuredHeight();
    int j = simpleMonthView.getPaddingTop() + (paramInt3 - i) / 2;
    int k = simpleMonthView.getPaddingLeft() + (paramInt2 - paramInt4) / 2;
    imageButton1.layout(k, j, k + paramInt4, j + i);
    paramInt4 = imageButton2.getMeasuredWidth();
    k = imageButton2.getMeasuredHeight();
    paramInt3 = simpleMonthView.getPaddingTop() + (paramInt3 - k) / 2;
    paramInt1 = paramInt1 - simpleMonthView.getPaddingRight() - (paramInt2 - paramInt4) / 2;
    imageButton2.layout(paramInt1 - paramInt4, paramInt3, paramInt1, paramInt3 + k);
  }
  
  public void setDayOfWeekTextAppearance(int paramInt) {
    this.mAdapter.setDayOfWeekTextAppearance(paramInt);
  }
  
  public int getDayOfWeekTextAppearance() {
    return this.mAdapter.getDayOfWeekTextAppearance();
  }
  
  public void setDayTextAppearance(int paramInt) {
    this.mAdapter.setDayTextAppearance(paramInt);
  }
  
  public int getDayTextAppearance() {
    return this.mAdapter.getDayTextAppearance();
  }
  
  public void setDate(long paramLong) {
    setDate(paramLong, false);
  }
  
  public void setDate(long paramLong, boolean paramBoolean) {
    setDate(paramLong, paramBoolean, true);
  }
  
  private void setDate(long paramLong, boolean paramBoolean1, boolean paramBoolean2) {
    long l;
    int i = 0;
    if (paramLong < this.mMinDate.getTimeInMillis()) {
      l = this.mMinDate.getTimeInMillis();
      i = 1;
    } else {
      l = paramLong;
      if (paramLong > this.mMaxDate.getTimeInMillis()) {
        l = this.mMaxDate.getTimeInMillis();
        i = 1;
      } 
    } 
    getTempCalendarForTime(l);
    if (paramBoolean2 || i)
      this.mSelectedDay.setTimeInMillis(l); 
    i = getPositionFromDay(l);
    if (i != this.mViewPager.getCurrentItem())
      this.mViewPager.setCurrentItem(i, paramBoolean1); 
    this.mAdapter.setSelectedDay(this.mTempCalendar);
  }
  
  public long getDate() {
    return this.mSelectedDay.getTimeInMillis();
  }
  
  public boolean getBoundsForDate(long paramLong, Rect paramRect) {
    int i = getPositionFromDay(paramLong);
    if (i != this.mViewPager.getCurrentItem())
      return false; 
    this.mTempCalendar.setTimeInMillis(paramLong);
    return this.mAdapter.getBoundsForDate(this.mTempCalendar, paramRect);
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    this.mAdapter.setFirstDayOfWeek(paramInt);
  }
  
  public int getFirstDayOfWeek() {
    return this.mAdapter.getFirstDayOfWeek();
  }
  
  public void setMinDate(long paramLong) {
    this.mMinDate.setTimeInMillis(paramLong);
    onRangeChanged();
  }
  
  public long getMinDate() {
    return this.mMinDate.getTimeInMillis();
  }
  
  public void setMaxDate(long paramLong) {
    this.mMaxDate.setTimeInMillis(paramLong);
    onRangeChanged();
  }
  
  public long getMaxDate() {
    return this.mMaxDate.getTimeInMillis();
  }
  
  public void onRangeChanged() {
    this.mAdapter.setRange(this.mMinDate, this.mMaxDate);
    setDate(this.mSelectedDay.getTimeInMillis(), false, false);
    updateButtonVisibility(this.mViewPager.getCurrentItem());
  }
  
  public void setOnDaySelectedListener(OnDaySelectedListener paramOnDaySelectedListener) {
    this.mOnDaySelectedListener = paramOnDaySelectedListener;
  }
  
  private int getDiffMonths(Calendar paramCalendar1, Calendar paramCalendar2) {
    int i = paramCalendar2.get(1), j = paramCalendar1.get(1);
    return paramCalendar2.get(2) - paramCalendar1.get(2) + (i - j) * 12;
  }
  
  private int getPositionFromDay(long paramLong) {
    int i = getDiffMonths(this.mMinDate, this.mMaxDate);
    int j = getDiffMonths(this.mMinDate, getTempCalendarForTime(paramLong));
    return MathUtils.constrain(j, 0, i);
  }
  
  private Calendar getTempCalendarForTime(long paramLong) {
    if (this.mTempCalendar == null)
      this.mTempCalendar = Calendar.getInstance(); 
    this.mTempCalendar.setTimeInMillis(paramLong);
    return this.mTempCalendar;
  }
  
  public int getMostVisiblePosition() {
    return this.mViewPager.getCurrentItem();
  }
  
  public void setPosition(int paramInt) {
    this.mViewPager.setCurrentItem(paramInt, false);
  }
  
  class OnDaySelectedListener {
    public abstract void onDaySelected(DayPickerView param1DayPickerView, Calendar param1Calendar);
  }
}
