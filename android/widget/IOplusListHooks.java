package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusListHooks extends IOplusCommonFeature {
  public static final IOplusListHooks DEFAULT = (IOplusListHooks)new Object();
  
  default IOplusListHooks getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusListHooks;
  }
  
  default FastScroller getFastScroller(AbsListView paramAbsListView, int paramInt) {
    return new FastScroller(paramAbsListView, paramInt);
  }
}
