package android.widget;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseAdapter implements ListAdapter, SpinnerAdapter {
  private CharSequence[] mAutofillOptions;
  
  private final DataSetObservable mDataSetObservable = new DataSetObservable();
  
  public boolean hasStableIds() {
    return false;
  }
  
  public void registerDataSetObserver(DataSetObserver paramDataSetObserver) {
    this.mDataSetObservable.registerObserver(paramDataSetObserver);
  }
  
  public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver) {
    this.mDataSetObservable.unregisterObserver(paramDataSetObserver);
  }
  
  public void notifyDataSetChanged() {
    this.mDataSetObservable.notifyChanged();
  }
  
  public void notifyDataSetInvalidated() {
    this.mDataSetObservable.notifyInvalidated();
  }
  
  public boolean areAllItemsEnabled() {
    return true;
  }
  
  public boolean isEnabled(int paramInt) {
    return true;
  }
  
  public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    return getView(paramInt, paramView, paramViewGroup);
  }
  
  public int getItemViewType(int paramInt) {
    return 0;
  }
  
  public int getViewTypeCount() {
    return 1;
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (getCount() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public CharSequence[] getAutofillOptions() {
    return this.mAutofillOptions;
  }
  
  public void setAutofillOptions(CharSequence... paramVarArgs) {
    this.mAutofillOptions = paramVarArgs;
  }
}
