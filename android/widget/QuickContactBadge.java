package android.widget;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.view.View;
import com.android.internal.R;

public class QuickContactBadge extends ImageView implements View.OnClickListener {
  private Bundle mExtras = null;
  
  protected String[] mExcludeMimes = null;
  
  static final String[] EMAIL_LOOKUP_PROJECTION = new String[] { "contact_id", "lookup" };
  
  static final String[] PHONE_LOOKUP_PROJECTION = new String[] { "_id", "lookup" };
  
  static final int EMAIL_ID_COLUMN_INDEX = 0;
  
  static final int EMAIL_LOOKUP_STRING_COLUMN_INDEX = 1;
  
  private static final String EXTRA_URI_CONTENT = "uri_content";
  
  static final int PHONE_ID_COLUMN_INDEX = 0;
  
  static final int PHONE_LOOKUP_STRING_COLUMN_INDEX = 1;
  
  private static final int TOKEN_EMAIL_LOOKUP = 0;
  
  private static final int TOKEN_EMAIL_LOOKUP_AND_TRIGGER = 2;
  
  private static final int TOKEN_PHONE_LOOKUP = 1;
  
  private static final int TOKEN_PHONE_LOOKUP_AND_TRIGGER = 3;
  
  private String mContactEmail;
  
  private String mContactPhone;
  
  private Uri mContactUri;
  
  private Drawable mDefaultAvatar;
  
  private Drawable mOverlay;
  
  private String mPrioritizedMimeType;
  
  private QueryHandler mQueryHandler;
  
  public QuickContactBadge(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public QuickContactBadge(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public QuickContactBadge(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public QuickContactBadge(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = this.mContext.obtainStyledAttributes(R.styleable.Theme);
    this.mOverlay = typedArray.getDrawable(325);
    typedArray.recycle();
    setOnClickListener(this);
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (!isInEditMode())
      this.mQueryHandler = new QueryHandler(this.mContext.getContentResolver()); 
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = this.mOverlay;
    if (drawable != null && drawable.isStateful() && 
      drawable.setState(getDrawableState()))
      invalidateDrawable(drawable); 
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Drawable drawable = this.mOverlay;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  public void setMode(int paramInt) {}
  
  public void setPrioritizedMimeType(String paramString) {
    this.mPrioritizedMimeType = paramString;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    if (!isEnabled())
      return; 
    Drawable drawable = this.mOverlay;
    if (drawable != null && drawable.getIntrinsicWidth() != 0) {
      drawable = this.mOverlay;
      if (drawable.getIntrinsicHeight() != 0) {
        this.mOverlay.setBounds(0, 0, getWidth(), getHeight());
        if (this.mPaddingTop == 0 && this.mPaddingLeft == 0) {
          this.mOverlay.draw(paramCanvas);
        } else {
          int i = paramCanvas.getSaveCount();
          paramCanvas.save();
          paramCanvas.translate(this.mPaddingLeft, this.mPaddingTop);
          this.mOverlay.draw(paramCanvas);
          paramCanvas.restoreToCount(i);
        } 
        return;
      } 
    } 
  }
  
  private boolean isAssigned() {
    return (this.mContactUri != null || this.mContactEmail != null || this.mContactPhone != null);
  }
  
  public void setImageToDefault() {
    if (this.mDefaultAvatar == null)
      this.mDefaultAvatar = this.mContext.getDrawable(17302375); 
    setImageDrawable(this.mDefaultAvatar);
  }
  
  public void assignContactUri(Uri paramUri) {
    this.mContactUri = paramUri;
    this.mContactEmail = null;
    this.mContactPhone = null;
    onContactUriChanged();
  }
  
  public void assignContactFromEmail(String paramString, boolean paramBoolean) {
    assignContactFromEmail(paramString, paramBoolean, (Bundle)null);
  }
  
  public void assignContactFromEmail(String paramString, boolean paramBoolean, Bundle paramBundle) {
    this.mContactEmail = paramString;
    this.mExtras = paramBundle;
    if (!paramBoolean) {
      QueryHandler queryHandler = this.mQueryHandler;
      if (queryHandler != null) {
        Uri uri2 = ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI;
        String str = this.mContactEmail;
        Uri uri1 = Uri.withAppendedPath(uri2, Uri.encode(str));
        String[] arrayOfString = EMAIL_LOOKUP_PROJECTION;
        queryHandler.startQuery(0, null, uri1, arrayOfString, null, null, null);
        return;
      } 
    } 
    this.mContactUri = null;
    onContactUriChanged();
  }
  
  public void assignContactFromPhone(String paramString, boolean paramBoolean) {
    assignContactFromPhone(paramString, paramBoolean, new Bundle());
  }
  
  public void assignContactFromPhone(String paramString, boolean paramBoolean, Bundle paramBundle) {
    this.mContactPhone = paramString;
    this.mExtras = paramBundle;
    if (!paramBoolean) {
      QueryHandler queryHandler = this.mQueryHandler;
      if (queryHandler != null) {
        Uri uri2 = ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
        String str = this.mContactPhone;
        Uri uri1 = Uri.withAppendedPath(uri2, str);
        String[] arrayOfString = PHONE_LOOKUP_PROJECTION;
        queryHandler.startQuery(1, null, uri1, arrayOfString, null, null, null);
        return;
      } 
    } 
    this.mContactUri = null;
    onContactUriChanged();
  }
  
  public void setOverlay(Drawable paramDrawable) {
    this.mOverlay = paramDrawable;
  }
  
  private void onContactUriChanged() {
    setEnabled(isAssigned());
  }
  
  public void onClick(View paramView) {
    Bundle bundle2 = this.mExtras, bundle1 = bundle2;
    if (bundle2 == null)
      bundle1 = new Bundle(); 
    if (this.mContactUri != null) {
      ContactsContract.QuickContact.showQuickContact(getContext(), this, this.mContactUri, this.mExcludeMimes, this.mPrioritizedMimeType);
    } else {
      String str = this.mContactEmail;
      if (str != null && this.mQueryHandler != null) {
        bundle1.putString("uri_content", str);
        QueryHandler queryHandler = this.mQueryHandler;
        Uri uri = ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI;
        String str1 = this.mContactEmail;
        uri = Uri.withAppendedPath(uri, Uri.encode(str1));
        String[] arrayOfString = EMAIL_LOOKUP_PROJECTION;
        queryHandler.startQuery(2, bundle1, uri, arrayOfString, null, null, null);
      } else {
        str = this.mContactPhone;
        if (str != null && this.mQueryHandler != null) {
          bundle1.putString("uri_content", str);
          QueryHandler queryHandler = this.mQueryHandler;
          Uri uri = ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
          String str1 = this.mContactPhone;
          uri = Uri.withAppendedPath(uri, str1);
          String[] arrayOfString = PHONE_LOOKUP_PROJECTION;
          queryHandler.startQuery(3, bundle1, uri, arrayOfString, null, null, null);
          return;
        } 
        return;
      } 
    } 
  }
  
  public CharSequence getAccessibilityClassName() {
    return QuickContactBadge.class.getName();
  }
  
  public void setExcludeMimes(String[] paramArrayOfString) {
    this.mExcludeMimes = paramArrayOfString;
  }
  
  class QueryHandler extends AsyncQueryHandler {
    final QuickContactBadge this$0;
    
    public QueryHandler(ContentResolver param1ContentResolver) {
      super(param1ContentResolver);
    }
    
    protected void onQueryComplete(int param1Int, Object param1Object, Cursor param1Cursor) {
      // Byte code:
      //   0: aconst_null
      //   1: astore #4
      //   3: aconst_null
      //   4: astore #5
      //   6: aconst_null
      //   7: astore #6
      //   9: aconst_null
      //   10: astore #7
      //   12: iconst_0
      //   13: istore #8
      //   15: iconst_0
      //   16: istore #9
      //   18: iconst_0
      //   19: istore #10
      //   21: aload_2
      //   22: ifnull -> 34
      //   25: aload_2
      //   26: checkcast android/os/Bundle
      //   29: astore #11
      //   31: goto -> 43
      //   34: new android/os/Bundle
      //   37: dup
      //   38: invokespecial <init> : ()V
      //   41: astore #11
      //   43: aload #5
      //   45: astore_2
      //   46: iload_1
      //   47: ifeq -> 197
      //   50: aload #7
      //   52: astore_2
      //   53: iload #10
      //   55: istore #8
      //   57: iload_1
      //   58: iconst_1
      //   59: if_icmpeq -> 128
      //   62: iload_1
      //   63: iconst_2
      //   64: if_icmpeq -> 102
      //   67: iload_1
      //   68: iconst_3
      //   69: if_icmpeq -> 82
      //   72: aload #4
      //   74: astore #7
      //   76: iload #9
      //   78: istore_1
      //   79: goto -> 278
      //   82: iconst_1
      //   83: istore #8
      //   85: ldc 'tel'
      //   87: aload #11
      //   89: ldc 'uri_content'
      //   91: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
      //   94: aconst_null
      //   95: invokestatic fromParts : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
      //   98: astore_2
      //   99: goto -> 128
      //   102: iconst_1
      //   103: istore #8
      //   105: aload #11
      //   107: ldc 'uri_content'
      //   109: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
      //   112: astore_2
      //   113: ldc 'mailto'
      //   115: aload_2
      //   116: aconst_null
      //   117: invokestatic fromParts : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
      //   120: astore_2
      //   121: goto -> 197
      //   124: astore_2
      //   125: goto -> 266
      //   128: aload #4
      //   130: astore #7
      //   132: aload_2
      //   133: astore #6
      //   135: iload #8
      //   137: istore_1
      //   138: aload_3
      //   139: ifnull -> 278
      //   142: aload #4
      //   144: astore #7
      //   146: aload_2
      //   147: astore #6
      //   149: iload #8
      //   151: istore_1
      //   152: aload_3
      //   153: invokeinterface moveToFirst : ()Z
      //   158: ifeq -> 278
      //   161: aload_3
      //   162: iconst_0
      //   163: invokeinterface getLong : (I)J
      //   168: lstore #12
      //   170: aload_3
      //   171: iconst_1
      //   172: invokeinterface getString : (I)Ljava/lang/String;
      //   177: astore #7
      //   179: lload #12
      //   181: aload #7
      //   183: invokestatic getLookupUri : (JLjava/lang/String;)Landroid/net/Uri;
      //   186: astore #7
      //   188: aload_2
      //   189: astore #6
      //   191: iload #8
      //   193: istore_1
      //   194: goto -> 278
      //   197: aload #4
      //   199: astore #7
      //   201: aload_2
      //   202: astore #6
      //   204: iload #8
      //   206: istore_1
      //   207: aload_3
      //   208: ifnull -> 278
      //   211: aload #4
      //   213: astore #7
      //   215: aload_2
      //   216: astore #6
      //   218: iload #8
      //   220: istore_1
      //   221: aload_3
      //   222: invokeinterface moveToFirst : ()Z
      //   227: ifeq -> 278
      //   230: aload_3
      //   231: iconst_0
      //   232: invokeinterface getLong : (I)J
      //   237: lstore #12
      //   239: aload_3
      //   240: iconst_1
      //   241: invokeinterface getString : (I)Ljava/lang/String;
      //   246: astore #7
      //   248: lload #12
      //   250: aload #7
      //   252: invokestatic getLookupUri : (JLjava/lang/String;)Landroid/net/Uri;
      //   255: astore #7
      //   257: aload_2
      //   258: astore #6
      //   260: iload #8
      //   262: istore_1
      //   263: goto -> 278
      //   266: aload_3
      //   267: ifnull -> 276
      //   270: aload_3
      //   271: invokeinterface close : ()V
      //   276: aload_2
      //   277: athrow
      //   278: aload_3
      //   279: ifnull -> 288
      //   282: aload_3
      //   283: invokeinterface close : ()V
      //   288: aload_0
      //   289: getfield this$0 : Landroid/widget/QuickContactBadge;
      //   292: aload #7
      //   294: invokestatic access$002 : (Landroid/widget/QuickContactBadge;Landroid/net/Uri;)Landroid/net/Uri;
      //   297: pop
      //   298: aload_0
      //   299: getfield this$0 : Landroid/widget/QuickContactBadge;
      //   302: invokestatic access$100 : (Landroid/widget/QuickContactBadge;)V
      //   305: iload_1
      //   306: ifeq -> 374
      //   309: aload_0
      //   310: getfield this$0 : Landroid/widget/QuickContactBadge;
      //   313: invokestatic access$000 : (Landroid/widget/QuickContactBadge;)Landroid/net/Uri;
      //   316: ifnull -> 374
      //   319: aload_0
      //   320: getfield this$0 : Landroid/widget/QuickContactBadge;
      //   323: invokevirtual getContext : ()Landroid/content/Context;
      //   326: astore_3
      //   327: aload_0
      //   328: getfield this$0 : Landroid/widget/QuickContactBadge;
      //   331: astore_2
      //   332: aload_2
      //   333: invokestatic access$000 : (Landroid/widget/QuickContactBadge;)Landroid/net/Uri;
      //   336: astore #7
      //   338: aload_0
      //   339: getfield this$0 : Landroid/widget/QuickContactBadge;
      //   342: getfield mExcludeMimes : [Ljava/lang/String;
      //   345: astore #6
      //   347: aload_0
      //   348: getfield this$0 : Landroid/widget/QuickContactBadge;
      //   351: astore #11
      //   353: aload #11
      //   355: invokestatic access$200 : (Landroid/widget/QuickContactBadge;)Ljava/lang/String;
      //   358: astore #11
      //   360: aload_3
      //   361: aload_2
      //   362: aload #7
      //   364: aload #6
      //   366: aload #11
      //   368: invokestatic showQuickContact : (Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)V
      //   371: goto -> 421
      //   374: aload #6
      //   376: ifnull -> 421
      //   379: new android/content/Intent
      //   382: dup
      //   383: ldc 'com.android.contacts.action.SHOW_OR_CREATE_CONTACT'
      //   385: aload #6
      //   387: invokespecial <init> : (Ljava/lang/String;Landroid/net/Uri;)V
      //   390: astore_2
      //   391: aload #11
      //   393: ifnull -> 410
      //   396: aload #11
      //   398: ldc 'uri_content'
      //   400: invokevirtual remove : (Ljava/lang/String;)V
      //   403: aload_2
      //   404: aload #11
      //   406: invokevirtual putExtras : (Landroid/os/Bundle;)Landroid/content/Intent;
      //   409: pop
      //   410: aload_0
      //   411: getfield this$0 : Landroid/widget/QuickContactBadge;
      //   414: invokevirtual getContext : ()Landroid/content/Context;
      //   417: aload_2
      //   418: invokevirtual startActivity : (Landroid/content/Intent;)V
      //   421: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #347	-> 0
      //   #348	-> 3
      //   #349	-> 12
      //   #350	-> 21
      //   #352	-> 43
      //   #354	-> 82
      //   #355	-> 85
      //   #368	-> 102
      //   #369	-> 105
      //   #370	-> 105
      //   #369	-> 113
      //   #383	-> 124
      //   #359	-> 128
      //   #360	-> 161
      //   #361	-> 170
      //   #362	-> 179
      //   #363	-> 188
      //   #374	-> 197
      //   #375	-> 230
      //   #376	-> 239
      //   #377	-> 248
      //   #383	-> 266
      //   #384	-> 270
      //   #386	-> 276
      //   #383	-> 278
      //   #384	-> 282
      //   #388	-> 288
      //   #389	-> 298
      //   #391	-> 305
      //   #393	-> 319
      //   #394	-> 353
      //   #393	-> 360
      //   #395	-> 374
      //   #397	-> 379
      //   #398	-> 391
      //   #399	-> 396
      //   #400	-> 403
      //   #402	-> 410
      //   #404	-> 421
      // Exception table:
      //   from	to	target	type
      //   85	99	124	finally
      //   105	113	124	finally
      //   113	121	124	finally
      //   152	161	124	finally
      //   161	170	124	finally
      //   170	179	124	finally
      //   179	188	124	finally
      //   221	230	124	finally
      //   230	239	124	finally
      //   239	248	124	finally
      //   248	257	124	finally
    }
  }
}
