package android.widget;

import android.view.MenuItem;
import com.android.internal.view.menu.MenuBuilder;

public interface MenuItemHoverListener {
  void onItemHoverEnter(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem);
  
  void onItemHoverExit(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem);
}
