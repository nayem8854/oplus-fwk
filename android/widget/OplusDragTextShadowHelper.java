package android.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Slog;
import android.view.View;
import android.view.ViewGroup;
import com.oplus.util.OplusDarkModeUtil;

public class OplusDragTextShadowHelper implements IOplusDragTextShadowHelper {
  private static final int MAX_DRAG_TEXT_SHADOW_WIDTH = 216;
  
  public static final String TAG = "OplusDragTextShadowHelper";
  
  private static OplusDragTextShadowHelper sInstance = null;
  
  public static OplusDragTextShadowHelper getInstance() {
    // Byte code:
    //   0: getstatic android/widget/OplusDragTextShadowHelper.sInstance : Landroid/widget/OplusDragTextShadowHelper;
    //   3: ifnonnull -> 39
    //   6: ldc android/widget/OplusDragTextShadowHelper
    //   8: monitorenter
    //   9: getstatic android/widget/OplusDragTextShadowHelper.sInstance : Landroid/widget/OplusDragTextShadowHelper;
    //   12: ifnonnull -> 27
    //   15: new android/widget/OplusDragTextShadowHelper
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/widget/OplusDragTextShadowHelper.sInstance : Landroid/widget/OplusDragTextShadowHelper;
    //   27: ldc android/widget/OplusDragTextShadowHelper
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/widget/OplusDragTextShadowHelper
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/widget/OplusDragTextShadowHelper.sInstance : Landroid/widget/OplusDragTextShadowHelper;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #41	-> 0
    //   #42	-> 6
    //   #43	-> 9
    //   #44	-> 15
    //   #46	-> 27
    //   #48	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public View.DragShadowBuilder getColorTextThumbnailBuilder(View paramView, String paramString) {
    if (paramView == null) {
      Slog.e("OplusDragTextShadowHelper", "getColorTextThumbnaiBuilder textview is null!");
      return null;
    } 
    Context context = paramView.getContext();
    ViewGroup viewGroup = (ViewGroup)View.inflate(context, 202440714, null);
    TextView textView = (TextView)viewGroup.findViewById(201457872);
    if (viewGroup != null && textView != null) {
      if (OplusDarkModeUtil.isNightMode(context)) {
        GradientDrawable gradientDrawable = (GradientDrawable)context.getResources().getDrawable(201851263);
        gradientDrawable.setColor(Color.parseColor("#404040"));
        textView.setBackground((Drawable)gradientDrawable);
        textView.setTextColor(Color.parseColor("#FFFFFF"));
      } 
      textView.setText(paramString);
      float f = (context.getResources().getDisplayMetrics()).density;
      int i = (int)(216.0F * f + 0.5F);
      i = View.MeasureSpec.makeMeasureSpec(i, -2147483648);
      int j = View.MeasureSpec.makeMeasureSpec(0, 0);
      viewGroup.measure(i, j);
      int k = viewGroup.getMeasuredWidth();
      int m = viewGroup.getMeasuredHeight();
      j = k;
      i = m;
      if (k == 0) {
        j = k;
        i = m;
        if (paramString != null) {
          j = k;
          i = m;
          if (paramString.length() > 0) {
            j = 1;
            i = 1;
          } 
        } 
      } 
      viewGroup.layout(0, 0, j, i);
      viewGroup.invalidate();
      return new View.DragShadowBuilder((View)viewGroup);
    } 
    throw new IllegalArgumentException("Unable to inflate text drag thumbnail");
  }
}
