package android.widget;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import com.android.internal.content.PackageMonitor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ActivityChooserModel extends DataSetObservable {
  private static final String LOG_TAG = ActivityChooserModel.class.getSimpleName();
  
  private static final Object sRegistryLock = new Object();
  
  static {
    sDataModelRegistry = new HashMap<>();
  }
  
  private final Object mInstanceLock = new Object();
  
  private final List<ActivityResolveInfo> mActivities = new ArrayList<>();
  
  private final List<HistoricalRecord> mHistoricalRecords = new ArrayList<>();
  
  private final PackageMonitor mPackageMonitor = new DataModelPackageMonitor();
  
  private ActivitySorter mActivitySorter = new DefaultSorter();
  
  private int mHistoryMaxSize = 50;
  
  private boolean mCanReadHistoricalData = true;
  
  private boolean mReadShareHistoryCalled = false;
  
  private boolean mHistoricalRecordsChanged = true;
  
  private boolean mReloadActivities = false;
  
  private static final String ATTRIBUTE_ACTIVITY = "activity";
  
  private static final String ATTRIBUTE_TIME = "time";
  
  private static final String ATTRIBUTE_WEIGHT = "weight";
  
  private static final boolean DEBUG = false;
  
  private static final int DEFAULT_ACTIVITY_INFLATION = 5;
  
  private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0F;
  
  public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
  
  public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
  
  private static final String HISTORY_FILE_EXTENSION = ".xml";
  
  private static final int INVALID_INDEX = -1;
  
  private static final String TAG_HISTORICAL_RECORD = "historical-record";
  
  private static final String TAG_HISTORICAL_RECORDS = "historical-records";
  
  private static final Map<String, ActivityChooserModel> sDataModelRegistry;
  
  private OnChooseActivityListener mActivityChoserModelPolicy;
  
  private final Context mContext;
  
  private final String mHistoryFileName;
  
  private Intent mIntent;
  
  public static ActivityChooserModel get(Context paramContext, String paramString) {
    synchronized (sRegistryLock) {
      ActivityChooserModel activityChooserModel1 = sDataModelRegistry.get(paramString);
      ActivityChooserModel activityChooserModel2 = activityChooserModel1;
      if (activityChooserModel1 == null) {
        activityChooserModel2 = new ActivityChooserModel();
        this(paramContext, paramString);
        sDataModelRegistry.put(paramString, activityChooserModel2);
      } 
      return activityChooserModel2;
    } 
  }
  
  private ActivityChooserModel(Context paramContext, String paramString) {
    this.mContext = paramContext.getApplicationContext();
    if (!TextUtils.isEmpty(paramString) && 
      !paramString.endsWith(".xml")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(".xml");
      this.mHistoryFileName = stringBuilder.toString();
    } else {
      this.mHistoryFileName = paramString;
    } 
    this.mPackageMonitor.register(this.mContext, null, true);
  }
  
  public void setIntent(Intent paramIntent) {
    synchronized (this.mInstanceLock) {
      if (this.mIntent == paramIntent)
        return; 
      this.mIntent = paramIntent;
      this.mReloadActivities = true;
      ensureConsistentState();
      return;
    } 
  }
  
  public Intent getIntent() {
    synchronized (this.mInstanceLock) {
      return this.mIntent;
    } 
  }
  
  public int getActivityCount() {
    synchronized (this.mInstanceLock) {
      ensureConsistentState();
      return this.mActivities.size();
    } 
  }
  
  public ResolveInfo getActivity(int paramInt) {
    synchronized (this.mInstanceLock) {
      ensureConsistentState();
      return ((ActivityResolveInfo)this.mActivities.get(paramInt)).resolveInfo;
    } 
  }
  
  public int getActivityIndex(ResolveInfo paramResolveInfo) {
    synchronized (this.mInstanceLock) {
      ensureConsistentState();
      List<ActivityResolveInfo> list = this.mActivities;
      int i = list.size();
      for (byte b = 0; b < i; b++) {
        ActivityResolveInfo activityResolveInfo = list.get(b);
        if (activityResolveInfo.resolveInfo == paramResolveInfo)
          return b; 
      } 
      return -1;
    } 
  }
  
  public Intent chooseActivity(int paramInt) {
    synchronized (this.mInstanceLock) {
      if (this.mIntent == null)
        return null; 
      ensureConsistentState();
      ActivityResolveInfo activityResolveInfo = this.mActivities.get(paramInt);
      ComponentName componentName = new ComponentName();
      this(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
      Intent intent = new Intent();
      this(this.mIntent);
      intent.setComponent(componentName);
      if (this.mActivityChoserModelPolicy != null) {
        Intent intent1 = new Intent();
        this(intent);
        boolean bool = this.mActivityChoserModelPolicy.onChooseActivity(this, intent1);
        if (bool)
          return null; 
      } 
      HistoricalRecord historicalRecord = new HistoricalRecord();
      this(componentName, System.currentTimeMillis(), 1.0F);
      addHisoricalRecord(historicalRecord);
      return intent;
    } 
  }
  
  public void setOnChooseActivityListener(OnChooseActivityListener paramOnChooseActivityListener) {
    synchronized (this.mInstanceLock) {
      this.mActivityChoserModelPolicy = paramOnChooseActivityListener;
      return;
    } 
  }
  
  public ResolveInfo getDefaultActivity() {
    synchronized (this.mInstanceLock) {
      ensureConsistentState();
      if (!this.mActivities.isEmpty())
        return ((ActivityResolveInfo)this.mActivities.get(0)).resolveInfo; 
      return null;
    } 
  }
  
  public void setDefaultActivity(int paramInt) {
    synchronized (this.mInstanceLock) {
      float f;
      ensureConsistentState();
      ActivityResolveInfo activityResolveInfo1 = this.mActivities.get(paramInt);
      ActivityResolveInfo activityResolveInfo2 = this.mActivities.get(0);
      if (activityResolveInfo2 != null) {
        f = activityResolveInfo2.weight - activityResolveInfo1.weight + 5.0F;
      } else {
        f = 1.0F;
      } 
      ComponentName componentName = new ComponentName();
      this(activityResolveInfo1.resolveInfo.activityInfo.packageName, activityResolveInfo1.resolveInfo.activityInfo.name);
      HistoricalRecord historicalRecord = new HistoricalRecord();
      this(componentName, System.currentTimeMillis(), f);
      addHisoricalRecord(historicalRecord);
      return;
    } 
  }
  
  private void persistHistoricalDataIfNeeded() {
    if (this.mReadShareHistoryCalled) {
      if (!this.mHistoricalRecordsChanged)
        return; 
      this.mHistoricalRecordsChanged = false;
      if (!TextUtils.isEmpty(this.mHistoryFileName))
        (new PersistHistoryAsyncTask()).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, new Object[] { new ArrayList<>(this.mHistoricalRecords), this.mHistoryFileName }); 
      return;
    } 
    throw new IllegalStateException("No preceding call to #readHistoricalData");
  }
  
  public void setActivitySorter(ActivitySorter paramActivitySorter) {
    synchronized (this.mInstanceLock) {
      if (this.mActivitySorter == paramActivitySorter)
        return; 
      this.mActivitySorter = paramActivitySorter;
      if (sortActivitiesIfNeeded())
        notifyChanged(); 
      return;
    } 
  }
  
  public void setHistoryMaxSize(int paramInt) {
    synchronized (this.mInstanceLock) {
      if (this.mHistoryMaxSize == paramInt)
        return; 
      this.mHistoryMaxSize = paramInt;
      pruneExcessiveHistoricalRecordsIfNeeded();
      if (sortActivitiesIfNeeded())
        notifyChanged(); 
      return;
    } 
  }
  
  public int getHistoryMaxSize() {
    synchronized (this.mInstanceLock) {
      return this.mHistoryMaxSize;
    } 
  }
  
  public int getHistorySize() {
    synchronized (this.mInstanceLock) {
      ensureConsistentState();
      return this.mHistoricalRecords.size();
    } 
  }
  
  protected void finalize() throws Throwable {
    super.finalize();
    this.mPackageMonitor.unregister();
  }
  
  private void ensureConsistentState() {
    boolean bool1 = loadActivitiesIfNeeded();
    boolean bool2 = readHistoricalDataIfNeeded();
    pruneExcessiveHistoricalRecordsIfNeeded();
    if (bool1 | bool2) {
      sortActivitiesIfNeeded();
      notifyChanged();
    } 
  }
  
  private boolean sortActivitiesIfNeeded() {
    if (this.mActivitySorter != null && this.mIntent != null) {
      List<ActivityResolveInfo> list = this.mActivities;
      if (!list.isEmpty() && !this.mHistoricalRecords.isEmpty()) {
        ActivitySorter activitySorter = this.mActivitySorter;
        Intent intent = this.mIntent;
        List<ActivityResolveInfo> list1 = this.mActivities;
        List<HistoricalRecord> list2 = this.mHistoricalRecords;
        list2 = Collections.unmodifiableList(list2);
        activitySorter.sort(intent, list1, list2);
        return true;
      } 
    } 
    return false;
  }
  
  private boolean loadActivitiesIfNeeded() {
    if (this.mReloadActivities && this.mIntent != null) {
      this.mReloadActivities = false;
      this.mActivities.clear();
      PackageManager packageManager = this.mContext.getPackageManager();
      Intent intent = this.mIntent;
      List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);
      int i = list.size();
      for (byte b = 0; b < i; b++) {
        ResolveInfo resolveInfo = list.get(b);
        ActivityInfo activityInfo = resolveInfo.activityInfo;
        String str = activityInfo.permission;
        int j = Process.myUid(), k = activityInfo.applicationInfo.uid;
        boolean bool = activityInfo.exported;
        if (ActivityManager.checkComponentPermission(str, j, k, bool) == 0)
          this.mActivities.add(new ActivityResolveInfo(resolveInfo)); 
      } 
      return true;
    } 
    return false;
  }
  
  private boolean readHistoricalDataIfNeeded() {
    if (this.mCanReadHistoricalData && this.mHistoricalRecordsChanged) {
      String str = this.mHistoryFileName;
      if (!TextUtils.isEmpty(str)) {
        this.mCanReadHistoricalData = false;
        this.mReadShareHistoryCalled = true;
        readHistoricalDataImpl();
        return true;
      } 
    } 
    return false;
  }
  
  private boolean addHisoricalRecord(HistoricalRecord paramHistoricalRecord) {
    boolean bool = this.mHistoricalRecords.add(paramHistoricalRecord);
    if (bool) {
      this.mHistoricalRecordsChanged = true;
      pruneExcessiveHistoricalRecordsIfNeeded();
      persistHistoricalDataIfNeeded();
      sortActivitiesIfNeeded();
      notifyChanged();
    } 
    return bool;
  }
  
  private void pruneExcessiveHistoricalRecordsIfNeeded() {
    int i = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
    if (i <= 0)
      return; 
    this.mHistoricalRecordsChanged = true;
    for (byte b = 0; b < i; b++)
      HistoricalRecord historicalRecord = this.mHistoricalRecords.remove(0); 
  }
  
  class HistoricalRecord {
    public final ComponentName activity;
    
    public final long time;
    
    public final float weight;
    
    public HistoricalRecord(long param1Long, float param1Float) {
      this(ComponentName.unflattenFromString((String)this$0), param1Long, param1Float);
    }
    
    public HistoricalRecord(ActivityChooserModel this$0, long param1Long, float param1Float) {
      this.activity = (ComponentName)this$0;
      this.time = param1Long;
      this.weight = param1Float;
    }
    
    public int hashCode() {
      int i;
      ComponentName componentName = this.activity;
      if (componentName == null) {
        i = 0;
      } else {
        i = componentName.hashCode();
      } 
      long l = this.time;
      int j = (int)(l ^ l >>> 32L);
      int k = Float.floatToIntBits(this.weight);
      return ((1 * 31 + i) * 31 + j) * 31 + k;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (getClass() != param1Object.getClass())
        return false; 
      HistoricalRecord historicalRecord = (HistoricalRecord)param1Object;
      param1Object = this.activity;
      if (param1Object == null) {
        if (historicalRecord.activity != null)
          return false; 
      } else if (!param1Object.equals(historicalRecord.activity)) {
        return false;
      } 
      if (this.time != historicalRecord.time)
        return false; 
      if (Float.floatToIntBits(this.weight) != Float.floatToIntBits(historicalRecord.weight))
        return false; 
      return true;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append("; activity:");
      stringBuilder.append(this.activity);
      stringBuilder.append("; time:");
      stringBuilder.append(this.time);
      stringBuilder.append("; weight:");
      stringBuilder.append(new BigDecimal(this.weight));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
  }
  
  class ActivityResolveInfo implements Comparable<ActivityResolveInfo> {
    public final ResolveInfo resolveInfo;
    
    final ActivityChooserModel this$0;
    
    public float weight;
    
    public ActivityResolveInfo(ResolveInfo param1ResolveInfo) {
      this.resolveInfo = param1ResolveInfo;
    }
    
    public int hashCode() {
      return Float.floatToIntBits(this.weight) + 31;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (Float.floatToIntBits(this.weight) != Float.floatToIntBits(((ActivityResolveInfo)param1Object).weight))
        return false; 
      return true;
    }
    
    public int compareTo(ActivityResolveInfo param1ActivityResolveInfo) {
      return Float.floatToIntBits(param1ActivityResolveInfo.weight) - Float.floatToIntBits(this.weight);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append("resolveInfo:");
      stringBuilder.append(this.resolveInfo.toString());
      stringBuilder.append("; weight:");
      stringBuilder.append(new BigDecimal(this.weight));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
  }
  
  class DefaultSorter implements ActivitySorter {
    private static final float WEIGHT_DECAY_COEFFICIENT = 0.95F;
    
    private DefaultSorter() {}
    
    private final Map<ComponentName, ActivityChooserModel.ActivityResolveInfo> mPackageNameToActivityMap = new HashMap<>();
    
    final ActivityChooserModel this$0;
    
    public void sort(Intent param1Intent, List<ActivityChooserModel.ActivityResolveInfo> param1List, List<ActivityChooserModel.HistoricalRecord> param1List1) {
      Map<ComponentName, ActivityChooserModel.ActivityResolveInfo> map = this.mPackageNameToActivityMap;
      map.clear();
      int i = param1List.size();
      int j;
      for (j = 0; j < i; j++) {
        ActivityChooserModel.ActivityResolveInfo activityResolveInfo = param1List.get(j);
        activityResolveInfo.weight = 0.0F;
        ComponentName componentName = new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
        map.put(componentName, activityResolveInfo);
      } 
      j = param1List1.size();
      float f = 1.0F;
      for (; --j >= 0; j--, f = f1) {
        ActivityChooserModel.HistoricalRecord historicalRecord = param1List1.get(j);
        ComponentName componentName = historicalRecord.activity;
        ActivityChooserModel.ActivityResolveInfo activityResolveInfo = map.get(componentName);
        float f1 = f;
        if (activityResolveInfo != null) {
          activityResolveInfo.weight += historicalRecord.weight * f;
          f1 = f * 0.95F;
        } 
      } 
      Collections.sort(param1List);
    }
  }
  
  private void readHistoricalDataImpl() {
    try {
      FileInputStream fileInputStream = this.mContext.openFileInput(this.mHistoryFileName);
      try {
        XmlPullParser xmlPullParser = Xml.newPullParser();
        xmlPullParser.setInput(fileInputStream, StandardCharsets.UTF_8.name());
        int i = 0;
        while (i != 1 && i != 2)
          i = xmlPullParser.next(); 
        if ("historical-records".equals(xmlPullParser.getName())) {
          List<HistoricalRecord> list = this.mHistoricalRecords;
          list.clear();
          while (true) {
            i = xmlPullParser.next();
            if (i == 1) {
              if (fileInputStream != null) {
                try {
                  fileInputStream.close();
                  break;
                } catch (IOException iOException) {}
                return;
              } 
              break;
            } 
            if (i == 3 || i == 4)
              continue; 
            String str = xmlPullParser.getName();
            if ("historical-record".equals(str)) {
              String str1 = xmlPullParser.getAttributeValue(null, "activity");
              long l = Long.parseLong(xmlPullParser.getAttributeValue(null, "time"));
              float f = Float.parseFloat(xmlPullParser.getAttributeValue(null, "weight"));
              HistoricalRecord historicalRecord = new HistoricalRecord();
              this(str1, l, f);
              list.add(historicalRecord);
              continue;
            } 
            XmlPullParserException xmlPullParserException = new XmlPullParserException();
            this("Share records file not well-formed.");
            throw xmlPullParserException;
          } 
        } else {
          XmlPullParserException xmlPullParserException = new XmlPullParserException();
          this("Share records file does not start with historical-records tag.");
          throw xmlPullParserException;
        } 
      } catch (XmlPullParserException xmlPullParserException) {
        String str = LOG_TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Error reading historical recrod file: ");
        stringBuilder.append(this.mHistoryFileName);
        Log.e(str, stringBuilder.toString(), (Throwable)xmlPullParserException);
        if (iOException != null)
          iOException.close(); 
      } catch (IOException iOException1) {
        String str = LOG_TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Error reading historical recrod file: ");
        stringBuilder.append(this.mHistoryFileName);
        Log.e(str, stringBuilder.toString(), iOException1);
        if (iOException != null)
          iOException.close(); 
      } finally {
        Exception exception;
      } 
      return;
    } catch (FileNotFoundException fileNotFoundException) {
      return;
    } 
  }
  
  class PersistHistoryAsyncTask extends AsyncTask<Object, Void, Void> {
    final ActivityChooserModel this$0;
    
    private PersistHistoryAsyncTask() {}
    
    public Void doInBackground(Object... param1VarArgs) {
      // Byte code:
      //   0: aload_1
      //   1: iconst_0
      //   2: aaload
      //   3: checkcast java/util/List
      //   6: astore_2
      //   7: aload_1
      //   8: iconst_1
      //   9: aaload
      //   10: checkcast java/lang/String
      //   13: astore_1
      //   14: aload_0
      //   15: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   18: invokestatic access$300 : (Landroid/widget/ActivityChooserModel;)Landroid/content/Context;
      //   21: aload_1
      //   22: iconst_0
      //   23: invokevirtual openFileOutput : (Ljava/lang/String;I)Ljava/io/FileOutputStream;
      //   26: astore_3
      //   27: invokestatic newSerializer : ()Lorg/xmlpull/v1/XmlSerializer;
      //   30: astore #4
      //   32: aload_2
      //   33: astore #5
      //   35: aload_2
      //   36: astore #5
      //   38: aload_2
      //   39: astore #5
      //   41: aload_2
      //   42: astore #5
      //   44: aload #4
      //   46: aload_3
      //   47: aconst_null
      //   48: invokeinterface setOutput : (Ljava/io/OutputStream;Ljava/lang/String;)V
      //   53: aload_2
      //   54: astore #5
      //   56: aload_2
      //   57: astore #5
      //   59: aload_2
      //   60: astore #5
      //   62: aload_2
      //   63: astore #5
      //   65: aload #4
      //   67: getstatic java/nio/charset/StandardCharsets.UTF_8 : Ljava/nio/charset/Charset;
      //   70: invokevirtual name : ()Ljava/lang/String;
      //   73: iconst_1
      //   74: invokestatic valueOf : (Z)Ljava/lang/Boolean;
      //   77: invokeinterface startDocument : (Ljava/lang/String;Ljava/lang/Boolean;)V
      //   82: aload_2
      //   83: astore #5
      //   85: aload_2
      //   86: astore #5
      //   88: aload_2
      //   89: astore #5
      //   91: aload_2
      //   92: astore #5
      //   94: aload #4
      //   96: aconst_null
      //   97: ldc 'historical-records'
      //   99: invokeinterface startTag : (Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
      //   104: pop
      //   105: aload_2
      //   106: astore #5
      //   108: aload_2
      //   109: astore #5
      //   111: aload_2
      //   112: astore #5
      //   114: aload_2
      //   115: astore #5
      //   117: aload_2
      //   118: invokeinterface size : ()I
      //   123: istore #6
      //   125: iconst_0
      //   126: istore #7
      //   128: aload_2
      //   129: astore_1
      //   130: iload #7
      //   132: iload #6
      //   134: if_icmpge -> 298
      //   137: aload_1
      //   138: astore #5
      //   140: aload_1
      //   141: astore #5
      //   143: aload_1
      //   144: astore #5
      //   146: aload_1
      //   147: astore #5
      //   149: aload_1
      //   150: iconst_0
      //   151: invokeinterface remove : (I)Ljava/lang/Object;
      //   156: checkcast android/widget/ActivityChooserModel$HistoricalRecord
      //   159: astore_2
      //   160: aload_1
      //   161: astore #5
      //   163: aload_1
      //   164: astore #5
      //   166: aload_1
      //   167: astore #5
      //   169: aload_1
      //   170: astore #5
      //   172: aload #4
      //   174: aconst_null
      //   175: ldc 'historical-record'
      //   177: invokeinterface startTag : (Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
      //   182: pop
      //   183: aload_1
      //   184: astore #5
      //   186: aload_1
      //   187: astore #5
      //   189: aload_1
      //   190: astore #5
      //   192: aload_1
      //   193: astore #5
      //   195: aload_2
      //   196: getfield activity : Landroid/content/ComponentName;
      //   199: astore #8
      //   201: aload_1
      //   202: astore #5
      //   204: aload_1
      //   205: astore #5
      //   207: aload_1
      //   208: astore #5
      //   210: aload_1
      //   211: astore #5
      //   213: aload #8
      //   215: invokevirtual flattenToString : ()Ljava/lang/String;
      //   218: astore #8
      //   220: aload_1
      //   221: astore #5
      //   223: aload_1
      //   224: astore #5
      //   226: aload_1
      //   227: astore #5
      //   229: aload_1
      //   230: astore #5
      //   232: aload #4
      //   234: aconst_null
      //   235: ldc 'activity'
      //   237: aload #8
      //   239: invokeinterface attribute : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
      //   244: pop
      //   245: aload #4
      //   247: aconst_null
      //   248: ldc 'time'
      //   250: aload_2
      //   251: getfield time : J
      //   254: invokestatic valueOf : (J)Ljava/lang/String;
      //   257: invokeinterface attribute : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
      //   262: pop
      //   263: aload #4
      //   265: aconst_null
      //   266: ldc 'weight'
      //   268: aload_2
      //   269: getfield weight : F
      //   272: invokestatic valueOf : (F)Ljava/lang/String;
      //   275: invokeinterface attribute : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
      //   280: pop
      //   281: aload #4
      //   283: aconst_null
      //   284: ldc 'historical-record'
      //   286: invokeinterface endTag : (Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
      //   291: pop
      //   292: iinc #7, 1
      //   295: goto -> 130
      //   298: aload #4
      //   300: aconst_null
      //   301: ldc 'historical-records'
      //   303: invokeinterface endTag : (Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
      //   308: pop
      //   309: aload #4
      //   311: invokeinterface endDocument : ()V
      //   316: aload_0
      //   317: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   320: iconst_1
      //   321: invokestatic access$602 : (Landroid/widget/ActivityChooserModel;Z)Z
      //   324: pop
      //   325: aload_3
      //   326: ifnull -> 554
      //   329: aload_3
      //   330: invokevirtual close : ()V
      //   333: goto -> 547
      //   336: astore_1
      //   337: goto -> 353
      //   340: astore_1
      //   341: goto -> 420
      //   344: astore_1
      //   345: goto -> 487
      //   348: astore_1
      //   349: goto -> 557
      //   352: astore_1
      //   353: invokestatic access$400 : ()Ljava/lang/String;
      //   356: astore_2
      //   357: new java/lang/StringBuilder
      //   360: astore #5
      //   362: aload #5
      //   364: invokespecial <init> : ()V
      //   367: aload #5
      //   369: ldc 'Error writing historical recrod file: '
      //   371: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   374: pop
      //   375: aload #5
      //   377: aload_0
      //   378: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   381: invokestatic access$500 : (Landroid/widget/ActivityChooserModel;)Ljava/lang/String;
      //   384: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   387: pop
      //   388: aload_2
      //   389: aload #5
      //   391: invokevirtual toString : ()Ljava/lang/String;
      //   394: aload_1
      //   395: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   398: pop
      //   399: aload_0
      //   400: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   403: iconst_1
      //   404: invokestatic access$602 : (Landroid/widget/ActivityChooserModel;Z)Z
      //   407: pop
      //   408: aload_3
      //   409: ifnull -> 554
      //   412: aload_3
      //   413: invokevirtual close : ()V
      //   416: goto -> 547
      //   419: astore_1
      //   420: invokestatic access$400 : ()Ljava/lang/String;
      //   423: astore_2
      //   424: new java/lang/StringBuilder
      //   427: astore #5
      //   429: aload #5
      //   431: invokespecial <init> : ()V
      //   434: aload #5
      //   436: ldc 'Error writing historical recrod file: '
      //   438: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   441: pop
      //   442: aload #5
      //   444: aload_0
      //   445: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   448: invokestatic access$500 : (Landroid/widget/ActivityChooserModel;)Ljava/lang/String;
      //   451: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   454: pop
      //   455: aload_2
      //   456: aload #5
      //   458: invokevirtual toString : ()Ljava/lang/String;
      //   461: aload_1
      //   462: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   465: pop
      //   466: aload_0
      //   467: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   470: iconst_1
      //   471: invokestatic access$602 : (Landroid/widget/ActivityChooserModel;Z)Z
      //   474: pop
      //   475: aload_3
      //   476: ifnull -> 554
      //   479: aload_3
      //   480: invokevirtual close : ()V
      //   483: goto -> 547
      //   486: astore_1
      //   487: invokestatic access$400 : ()Ljava/lang/String;
      //   490: astore #5
      //   492: new java/lang/StringBuilder
      //   495: astore_2
      //   496: aload_2
      //   497: invokespecial <init> : ()V
      //   500: aload_2
      //   501: ldc 'Error writing historical recrod file: '
      //   503: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   506: pop
      //   507: aload_2
      //   508: aload_0
      //   509: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   512: invokestatic access$500 : (Landroid/widget/ActivityChooserModel;)Ljava/lang/String;
      //   515: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   518: pop
      //   519: aload #5
      //   521: aload_2
      //   522: invokevirtual toString : ()Ljava/lang/String;
      //   525: aload_1
      //   526: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   529: pop
      //   530: aload_0
      //   531: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   534: iconst_1
      //   535: invokestatic access$602 : (Landroid/widget/ActivityChooserModel;Z)Z
      //   538: pop
      //   539: aload_3
      //   540: ifnull -> 554
      //   543: aload_3
      //   544: invokevirtual close : ()V
      //   547: goto -> 554
      //   550: astore_1
      //   551: goto -> 547
      //   554: aconst_null
      //   555: areturn
      //   556: astore_1
      //   557: aload_0
      //   558: getfield this$0 : Landroid/widget/ActivityChooserModel;
      //   561: iconst_1
      //   562: invokestatic access$602 : (Landroid/widget/ActivityChooserModel;Z)Z
      //   565: pop
      //   566: aload_3
      //   567: ifnull -> 578
      //   570: aload_3
      //   571: invokevirtual close : ()V
      //   574: goto -> 578
      //   577: astore_2
      //   578: aload_1
      //   579: athrow
      //   580: astore_3
      //   581: invokestatic access$400 : ()Ljava/lang/String;
      //   584: astore_2
      //   585: new java/lang/StringBuilder
      //   588: dup
      //   589: invokespecial <init> : ()V
      //   592: astore #5
      //   594: aload #5
      //   596: ldc 'Error writing historical recrod file: '
      //   598: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   601: pop
      //   602: aload #5
      //   604: aload_1
      //   605: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   608: pop
      //   609: aload_2
      //   610: aload #5
      //   612: invokevirtual toString : ()Ljava/lang/String;
      //   615: aload_3
      //   616: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   619: pop
      //   620: aconst_null
      //   621: areturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1069	-> 0
      //   #1070	-> 7
      //   #1072	-> 14
      //   #1075	-> 14
      //   #1079	-> 27
      //   #1081	-> 27
      //   #1084	-> 32
      //   #1085	-> 53
      //   #1086	-> 82
      //   #1088	-> 105
      //   #1089	-> 125
      //   #1090	-> 137
      //   #1091	-> 160
      //   #1092	-> 183
      //   #1093	-> 201
      //   #1092	-> 220
      //   #1094	-> 245
      //   #1095	-> 263
      //   #1096	-> 281
      //   #1089	-> 292
      //   #1102	-> 298
      //   #1103	-> 309
      //   #1115	-> 316
      //   #1116	-> 325
      //   #1118	-> 329
      //   #1112	-> 336
      //   #1110	-> 340
      //   #1108	-> 344
      //   #1115	-> 348
      //   #1112	-> 352
      //   #1113	-> 353
      //   #1115	-> 399
      //   #1116	-> 408
      //   #1118	-> 412
      //   #1110	-> 419
      //   #1111	-> 420
      //   #1115	-> 466
      //   #1116	-> 475
      //   #1118	-> 479
      //   #1108	-> 486
      //   #1109	-> 487
      //   #1115	-> 530
      //   #1116	-> 539
      //   #1118	-> 543
      //   #1121	-> 547
      //   #1119	-> 550
      //   #1124	-> 554
      //   #1115	-> 556
      //   #1116	-> 566
      //   #1118	-> 570
      //   #1121	-> 574
      //   #1119	-> 577
      //   #1123	-> 578
      //   #1076	-> 580
      //   #1077	-> 581
      //   #1078	-> 620
      // Exception table:
      //   from	to	target	type
      //   14	27	580	java/io/FileNotFoundException
      //   44	53	486	java/lang/IllegalArgumentException
      //   44	53	419	java/lang/IllegalStateException
      //   44	53	352	java/io/IOException
      //   44	53	348	finally
      //   65	82	486	java/lang/IllegalArgumentException
      //   65	82	419	java/lang/IllegalStateException
      //   65	82	352	java/io/IOException
      //   65	82	348	finally
      //   94	105	486	java/lang/IllegalArgumentException
      //   94	105	419	java/lang/IllegalStateException
      //   94	105	352	java/io/IOException
      //   94	105	348	finally
      //   117	125	486	java/lang/IllegalArgumentException
      //   117	125	419	java/lang/IllegalStateException
      //   117	125	352	java/io/IOException
      //   117	125	348	finally
      //   149	160	486	java/lang/IllegalArgumentException
      //   149	160	419	java/lang/IllegalStateException
      //   149	160	352	java/io/IOException
      //   149	160	348	finally
      //   172	183	486	java/lang/IllegalArgumentException
      //   172	183	419	java/lang/IllegalStateException
      //   172	183	352	java/io/IOException
      //   172	183	348	finally
      //   195	201	486	java/lang/IllegalArgumentException
      //   195	201	419	java/lang/IllegalStateException
      //   195	201	352	java/io/IOException
      //   195	201	348	finally
      //   213	220	486	java/lang/IllegalArgumentException
      //   213	220	419	java/lang/IllegalStateException
      //   213	220	352	java/io/IOException
      //   213	220	348	finally
      //   232	245	486	java/lang/IllegalArgumentException
      //   232	245	419	java/lang/IllegalStateException
      //   232	245	352	java/io/IOException
      //   232	245	348	finally
      //   245	263	344	java/lang/IllegalArgumentException
      //   245	263	340	java/lang/IllegalStateException
      //   245	263	336	java/io/IOException
      //   245	263	556	finally
      //   263	281	344	java/lang/IllegalArgumentException
      //   263	281	340	java/lang/IllegalStateException
      //   263	281	336	java/io/IOException
      //   263	281	556	finally
      //   281	292	344	java/lang/IllegalArgumentException
      //   281	292	340	java/lang/IllegalStateException
      //   281	292	336	java/io/IOException
      //   281	292	556	finally
      //   298	309	344	java/lang/IllegalArgumentException
      //   298	309	340	java/lang/IllegalStateException
      //   298	309	336	java/io/IOException
      //   298	309	556	finally
      //   309	316	344	java/lang/IllegalArgumentException
      //   309	316	340	java/lang/IllegalStateException
      //   309	316	336	java/io/IOException
      //   309	316	556	finally
      //   329	333	550	java/io/IOException
      //   353	399	556	finally
      //   412	416	550	java/io/IOException
      //   420	466	556	finally
      //   479	483	550	java/io/IOException
      //   487	530	556	finally
      //   543	547	550	java/io/IOException
      //   570	574	577	java/io/IOException
    }
  }
  
  private final class DataModelPackageMonitor extends PackageMonitor {
    final ActivityChooserModel this$0;
    
    private DataModelPackageMonitor() {}
    
    public void onSomePackagesChanged() {
      ActivityChooserModel.access$702(ActivityChooserModel.this, true);
    }
  }
  
  class ActivityChooserModelClient {
    public abstract void setActivityChooserModel(ActivityChooserModel param1ActivityChooserModel);
  }
  
  class ActivitySorter {
    public abstract void sort(Intent param1Intent, List<ActivityChooserModel.ActivityResolveInfo> param1List, List<ActivityChooserModel.HistoricalRecord> param1List1);
  }
  
  class OnChooseActivityListener {
    public abstract boolean onChooseActivity(ActivityChooserModel param1ActivityChooserModel, Intent param1Intent);
  }
}
