package android.widget;

public interface SectionIndexer {
  int getPositionForSection(int paramInt);
  
  int getSectionForPosition(int paramInt);
  
  Object[] getSections();
}
