package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.icu.text.DateFormat;
import android.icu.text.DisplayContext;
import android.icu.util.Calendar;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.StateSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.R;
import java.util.Locale;

class DatePickerCalendarDelegate extends DatePicker.AbstractDatePickerDelegate {
  private static final int ANIMATION_DURATION = 300;
  
  private static final int[] ATTRS_DISABLED_ALPHA;
  
  private static final int[] ATTRS_TEXT_COLOR = new int[] { 16842904 };
  
  private static final int DEFAULT_END_YEAR = 2100;
  
  private static final int DEFAULT_START_YEAR = 1900;
  
  private static final int UNINITIALIZED = -1;
  
  private static final int USE_LOCALE = 0;
  
  private static final int VIEW_MONTH_DAY = 0;
  
  private static final int VIEW_YEAR = 1;
  
  private ViewAnimator mAnimator;
  
  private ViewGroup mContainer;
  
  private int mCurrentView;
  
  private DayPickerView mDayPickerView;
  
  private int mFirstDayOfWeek;
  
  private TextView mHeaderMonthDay;
  
  private TextView mHeaderYear;
  
  private final Calendar mMaxDate;
  
  private final Calendar mMinDate;
  
  private DateFormat mMonthDayFormat;
  
  private final DayPickerView.OnDaySelectedListener mOnDaySelectedListener;
  
  private final View.OnClickListener mOnHeaderClickListener;
  
  private final YearPickerView.OnYearSelectedListener mOnYearSelectedListener;
  
  private String mSelectDay;
  
  private String mSelectYear;
  
  private final Calendar mTempDate;
  
  private DateFormat mYearFormat;
  
  private YearPickerView mYearPickerView;
  
  static {
    ATTRS_DISABLED_ALPHA = new int[] { 16842803 };
  }
  
  public DatePickerCalendarDelegate(DatePicker paramDatePicker, Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramDatePicker, paramContext);
    ColorStateList colorStateList1;
    this.mCurrentView = -1;
    this.mFirstDayOfWeek = 0;
    this.mOnDaySelectedListener = (DayPickerView.OnDaySelectedListener)new Object(this);
    this.mOnYearSelectedListener = (YearPickerView.OnYearSelectedListener)new Object(this);
    this.mOnHeaderClickListener = new _$$Lambda$DatePickerCalendarDelegate$GuCiuXPsIV2EU6oKGRXrsGY_DHM(this);
    Locale locale = this.mCurrentLocale;
    this.mCurrentDate = Calendar.getInstance(locale);
    this.mTempDate = Calendar.getInstance(locale);
    this.mMinDate = Calendar.getInstance(locale);
    this.mMaxDate = Calendar.getInstance(locale);
    this.mMinDate.set(1900, 0, 1);
    this.mMaxDate.set(2100, 11, 31);
    Resources resources = this.mDelegator.getResources();
    TypedArray typedArray = this.mContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DatePicker, paramInt1, paramInt2);
    LayoutInflater layoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
    paramInt1 = typedArray.getResourceId(19, 17367142);
    ViewGroup viewGroup1 = (ViewGroup)layoutInflater.inflate(paramInt1, this.mDelegator, false);
    viewGroup1.setSaveFromParentEnabled(false);
    this.mDelegator.addView(this.mContainer);
    ViewGroup viewGroup2 = this.mContainer.<ViewGroup>findViewById(16908908);
    TextView textView = viewGroup2.<TextView>findViewById(16908910);
    textView.setOnClickListener(this.mOnHeaderClickListener);
    this.mHeaderMonthDay = textView = viewGroup2.<TextView>findViewById(16908909);
    textView.setOnClickListener(this.mOnHeaderClickListener);
    textView = null;
    paramInt1 = typedArray.getResourceId(10, 0);
    if (paramInt1 != 0) {
      TypedArray typedArray1 = this.mContext.obtainStyledAttributes(null, ATTRS_TEXT_COLOR, 0, paramInt1);
      colorStateList1 = typedArray1.getColorStateList(0);
      colorStateList1 = applyLegacyColorFixes(colorStateList1);
      typedArray1.recycle();
    } 
    ColorStateList colorStateList2 = colorStateList1;
    if (colorStateList1 == null)
      colorStateList2 = typedArray.getColorStateList(18); 
    if (colorStateList2 != null) {
      this.mHeaderYear.setTextColor(colorStateList2);
      this.mHeaderMonthDay.setTextColor(colorStateList2);
    } 
    if (typedArray.hasValueOrEmpty(0))
      viewGroup2.setBackground(typedArray.getDrawable(0)); 
    typedArray.recycle();
    ViewAnimator viewAnimator = this.mContainer.<ViewAnimator>findViewById(16908757);
    DayPickerView dayPickerView = viewAnimator.<DayPickerView>findViewById(16908907);
    dayPickerView.setFirstDayOfWeek(this.mFirstDayOfWeek);
    this.mDayPickerView.setMinDate(this.mMinDate.getTimeInMillis());
    this.mDayPickerView.setMaxDate(this.mMaxDate.getTimeInMillis());
    this.mDayPickerView.setDate(this.mCurrentDate.getTimeInMillis());
    this.mDayPickerView.setOnDaySelectedListener(this.mOnDaySelectedListener);
    YearPickerView yearPickerView = this.mAnimator.<YearPickerView>findViewById(16908911);
    yearPickerView.setRange(this.mMinDate, this.mMaxDate);
    this.mYearPickerView.setYear(this.mCurrentDate.get(1));
    this.mYearPickerView.setOnYearSelectedListener(this.mOnYearSelectedListener);
    this.mSelectDay = resources.getString(17041245);
    this.mSelectYear = resources.getString(17041251);
    onLocaleChanged(this.mCurrentLocale);
    setCurrentView(0);
  }
  
  private ColorStateList applyLegacyColorFixes(ColorStateList paramColorStateList) {
    int i;
    int j;
    if (paramColorStateList == null || paramColorStateList.hasState(16843518))
      return paramColorStateList; 
    if (paramColorStateList.hasState(16842913)) {
      i = paramColorStateList.getColorForState(StateSet.get(10), 0);
      j = paramColorStateList.getColorForState(StateSet.get(8), 0);
    } else {
      i = paramColorStateList.getDefaultColor();
      TypedArray typedArray = this.mContext.obtainStyledAttributes(ATTRS_DISABLED_ALPHA);
      float f = typedArray.getFloat(0, 0.3F);
      j = multiplyAlphaComponent(i, f);
    } 
    if (i == 0 || j == 0)
      return null; 
    return new ColorStateList(new int[][] { { 16843518 }, , {} }, new int[] { i, j });
  }
  
  private int multiplyAlphaComponent(int paramInt, float paramFloat) {
    int i = (int)((paramInt >> 24 & 0xFF) * paramFloat + 0.5F);
    return i << 24 | 0xFFFFFF & paramInt;
  }
  
  protected void onLocaleChanged(Locale paramLocale) {
    TextView textView = this.mHeaderYear;
    if (textView == null)
      return; 
    DateFormat dateFormat = DateFormat.getInstanceForSkeleton("EMMMd", paramLocale);
    dateFormat.setContext(DisplayContext.CAPITALIZATION_FOR_STANDALONE);
    this.mYearFormat = DateFormat.getInstanceForSkeleton("y", paramLocale);
    onCurrentDateChanged(false);
  }
  
  private void onCurrentDateChanged(boolean paramBoolean) {
    if (this.mHeaderYear == null)
      return; 
    String str = this.mYearFormat.format(this.mCurrentDate.getTime());
    this.mHeaderYear.setText(str);
    str = this.mMonthDayFormat.format(this.mCurrentDate.getTime());
    this.mHeaderMonthDay.setText(str);
    if (paramBoolean)
      this.mAnimator.announceForAccessibility(getFormattedCurrentDate()); 
  }
  
  private void setCurrentView(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        int i = this.mCurrentDate.get(1);
        this.mYearPickerView.setYear(i);
        this.mYearPickerView.post(new _$$Lambda$DatePickerCalendarDelegate$_6rynvAYPe1gU9xVgvSm4VMsr2M(this));
        if (this.mCurrentView != paramInt) {
          this.mHeaderMonthDay.setActivated(false);
          this.mHeaderYear.setActivated(true);
          this.mAnimator.setDisplayedChild(1);
          this.mCurrentView = paramInt;
        } 
        this.mAnimator.announceForAccessibility(this.mSelectYear);
      } 
    } else {
      this.mDayPickerView.setDate(this.mCurrentDate.getTimeInMillis());
      if (this.mCurrentView != paramInt) {
        this.mHeaderMonthDay.setActivated(true);
        this.mHeaderYear.setActivated(false);
        this.mAnimator.setDisplayedChild(0);
        this.mCurrentView = paramInt;
      } 
      this.mAnimator.announceForAccessibility(this.mSelectDay);
    } 
  }
  
  public void init(int paramInt1, int paramInt2, int paramInt3, DatePicker.OnDateChangedListener paramOnDateChangedListener) {
    setDate(paramInt1, paramInt2, paramInt3);
    onDateChanged(false, false);
    this.mOnDateChangedListener = paramOnDateChangedListener;
  }
  
  public void updateDate(int paramInt1, int paramInt2, int paramInt3) {
    setDate(paramInt1, paramInt2, paramInt3);
    onDateChanged(false, true);
  }
  
  private void setDate(int paramInt1, int paramInt2, int paramInt3) {
    this.mCurrentDate.set(1, paramInt1);
    this.mCurrentDate.set(2, paramInt2);
    this.mCurrentDate.set(5, paramInt3);
    resetAutofilledValue();
  }
  
  private void onDateChanged(boolean paramBoolean1, boolean paramBoolean2) {
    int i = this.mCurrentDate.get(1);
    if (paramBoolean2 && (this.mOnDateChangedListener != null || this.mAutoFillChangeListener != null)) {
      int j = this.mCurrentDate.get(2);
      int k = this.mCurrentDate.get(5);
      if (this.mOnDateChangedListener != null)
        this.mOnDateChangedListener.onDateChanged(this.mDelegator, i, j, k); 
      if (this.mAutoFillChangeListener != null)
        this.mAutoFillChangeListener.onDateChanged(this.mDelegator, i, j, k); 
    } 
    this.mDayPickerView.setDate(this.mCurrentDate.getTimeInMillis());
    this.mYearPickerView.setYear(i);
    onCurrentDateChanged(paramBoolean1);
    if (paramBoolean1)
      tryVibrate(); 
  }
  
  public int getYear() {
    return this.mCurrentDate.get(1);
  }
  
  public int getMonth() {
    return this.mCurrentDate.get(2);
  }
  
  public int getDayOfMonth() {
    return this.mCurrentDate.get(5);
  }
  
  public void setMinDate(long paramLong) {
    this.mTempDate.setTimeInMillis(paramLong);
    if (this.mTempDate.get(1) == this.mMinDate.get(1)) {
      Calendar calendar = this.mTempDate;
      if (calendar.get(6) == this.mMinDate.get(6))
        return; 
    } 
    if (this.mCurrentDate.before(this.mTempDate)) {
      this.mCurrentDate.setTimeInMillis(paramLong);
      onDateChanged(false, true);
    } 
    this.mMinDate.setTimeInMillis(paramLong);
    this.mDayPickerView.setMinDate(paramLong);
    this.mYearPickerView.setRange(this.mMinDate, this.mMaxDate);
  }
  
  public Calendar getMinDate() {
    return this.mMinDate;
  }
  
  public void setMaxDate(long paramLong) {
    this.mTempDate.setTimeInMillis(paramLong);
    if (this.mTempDate.get(1) == this.mMaxDate.get(1)) {
      Calendar calendar = this.mTempDate;
      if (calendar.get(6) == this.mMaxDate.get(6))
        return; 
    } 
    if (this.mCurrentDate.after(this.mTempDate)) {
      this.mCurrentDate.setTimeInMillis(paramLong);
      onDateChanged(false, true);
    } 
    this.mMaxDate.setTimeInMillis(paramLong);
    this.mDayPickerView.setMaxDate(paramLong);
    this.mYearPickerView.setRange(this.mMinDate, this.mMaxDate);
  }
  
  public Calendar getMaxDate() {
    return this.mMaxDate;
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    this.mFirstDayOfWeek = paramInt;
    this.mDayPickerView.setFirstDayOfWeek(paramInt);
  }
  
  public int getFirstDayOfWeek() {
    int i = this.mFirstDayOfWeek;
    if (i != 0)
      return i; 
    return this.mCurrentDate.getFirstDayOfWeek();
  }
  
  public void setEnabled(boolean paramBoolean) {
    this.mContainer.setEnabled(paramBoolean);
    this.mDayPickerView.setEnabled(paramBoolean);
    this.mYearPickerView.setEnabled(paramBoolean);
    this.mHeaderYear.setEnabled(paramBoolean);
    this.mHeaderMonthDay.setEnabled(paramBoolean);
  }
  
  public boolean isEnabled() {
    return this.mContainer.isEnabled();
  }
  
  public CalendarView getCalendarView() {
    throw new UnsupportedOperationException("Not supported by calendar-mode DatePicker");
  }
  
  public void setCalendarViewShown(boolean paramBoolean) {}
  
  public boolean getCalendarViewShown() {
    return false;
  }
  
  public void setSpinnersShown(boolean paramBoolean) {}
  
  public boolean getSpinnersShown() {
    return false;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    setCurrentLocale(paramConfiguration.locale);
  }
  
  public Parcelable onSaveInstanceState(Parcelable paramParcelable) {
    byte b;
    int i = this.mCurrentDate.get(1);
    int j = this.mCurrentDate.get(2);
    int k = this.mCurrentDate.get(5);
    int m = this.mCurrentView;
    if (m == 0) {
      m = this.mDayPickerView.getMostVisiblePosition();
      b = -1;
    } else if (m == 1) {
      m = this.mYearPickerView.getFirstVisiblePosition();
      b = this.mYearPickerView.getFirstPositionOffset();
    } else {
      m = -1;
      b = -1;
    } 
    long l = this.mMinDate.getTimeInMillis();
    Calendar calendar = this.mMaxDate;
    return 
      new DatePicker.AbstractDatePickerDelegate.SavedState(paramParcelable, i, j, k, l, calendar.getTimeInMillis(), this.mCurrentView, m, b);
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable instanceof DatePicker.AbstractDatePickerDelegate.SavedState) {
      paramParcelable = paramParcelable;
      this.mCurrentDate.set(paramParcelable.getSelectedYear(), paramParcelable.getSelectedMonth(), paramParcelable.getSelectedDay());
      this.mMinDate.setTimeInMillis(paramParcelable.getMinDate());
      this.mMaxDate.setTimeInMillis(paramParcelable.getMaxDate());
      onCurrentDateChanged(false);
      int i = paramParcelable.getCurrentView();
      setCurrentView(i);
      int j = paramParcelable.getListPosition();
      if (j != -1)
        if (i == 0) {
          this.mDayPickerView.setPosition(j);
        } else if (i == 1) {
          i = paramParcelable.getListPositionOffset();
          this.mYearPickerView.setSelectionFromTop(j, i);
        }  
    } 
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    onPopulateAccessibilityEvent(paramAccessibilityEvent);
    return true;
  }
  
  public CharSequence getAccessibilityClassName() {
    return DatePicker.class.getName();
  }
  
  private static int getDaysInMonth(int paramInt1, int paramInt2) {
    switch (paramInt1) {
      default:
        throw new IllegalArgumentException("Invalid Month");
      case 3:
      case 5:
      case 8:
      case 10:
        return 30;
      case 1:
        if (paramInt2 % 4 == 0 && (paramInt2 % 100 != 0 || paramInt2 % 400 == 0)) {
          paramInt1 = 29;
        } else {
          paramInt1 = 28;
        } 
        return paramInt1;
      case 0:
      case 2:
      case 4:
      case 6:
      case 7:
      case 9:
      case 11:
        break;
    } 
    return 31;
  }
  
  private void tryVibrate() {
    this.mDelegator.performHapticFeedback(5);
  }
}
