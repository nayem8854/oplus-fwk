package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class ViewSwitcher extends ViewAnimator {
  ViewFactory mFactory;
  
  public ViewSwitcher(Context paramContext) {
    super(paramContext);
  }
  
  public ViewSwitcher(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    if (getChildCount() < 2) {
      super.addView(paramView, paramInt, paramLayoutParams);
      return;
    } 
    throw new IllegalStateException("Can't add more than 2 views to a ViewSwitcher");
  }
  
  public CharSequence getAccessibilityClassName() {
    return ViewSwitcher.class.getName();
  }
  
  public View getNextView() {
    boolean bool;
    if (this.mWhichChild == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return getChildAt(bool);
  }
  
  private View obtainView() {
    View view = this.mFactory.makeView();
    FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams)view.getLayoutParams();
    FrameLayout.LayoutParams layoutParams2 = layoutParams1;
    if (layoutParams1 == null)
      layoutParams2 = new FrameLayout.LayoutParams(-1, -2); 
    addView(view, layoutParams2);
    return view;
  }
  
  public void setFactory(ViewFactory paramViewFactory) {
    this.mFactory = paramViewFactory;
    obtainView();
    obtainView();
  }
  
  public void reset() {
    this.mFirstTime = true;
    View view = getChildAt(0);
    if (view != null)
      view.setVisibility(8); 
    view = getChildAt(1);
    if (view != null)
      view.setVisibility(8); 
  }
  
  class ViewFactory {
    public abstract View makeView();
  }
}
