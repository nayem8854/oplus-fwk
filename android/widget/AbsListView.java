package android.widget;

import android.common.ColorFrameworkFactory;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;
import android.os.Trace;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.LongSparseArray;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.StateSet;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.IOplusViewConfigHelper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewDebug.IntToString;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputContentInfo;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.List;

public abstract class AbsListView extends OplusBaseAbsListView implements TextWatcher, ViewTreeObserver.OnGlobalLayoutListener, Filter.FilterListener, ViewTreeObserver.OnTouchModeChangeListener, RemoteViewsAdapter.RemoteAdapterConnectionCallback {
  int mChoiceMode = 0;
  
  int mLayoutMode = 0;
  
  private boolean mDeferNotifyDataSetChanged = false;
  
  boolean mDrawSelectorOnTop = false;
  
  int mSelectorPosition = -1;
  
  Rect mSelectorRect = new Rect();
  
  final RecycleBin mRecycler = new RecycleBin();
  
  int mSelectionLeftPadding = 0;
  
  int mSelectionTopPadding = 0;
  
  int mSelectionRightPadding = 0;
  
  int mSelectionBottomPadding = 0;
  
  Rect mListPadding = new Rect();
  
  int mWidthMeasureSpec = 0;
  
  int mTouchMode = -1;
  
  int mSelectedTop = 0;
  
  private boolean mSmoothScrollbarEnabled = true;
  
  int mResurrectToPosition = -1;
  
  private ContextMenu.ContextMenuInfo mContextMenuInfo = null;
  
  private int mLastTouchMode = -1;
  
  private boolean mScrollProfilingStarted = false;
  
  private boolean mFlingProfilingStarted = false;
  
  private StrictMode.Span mScrollStrictSpan = null;
  
  private StrictMode.Span mFlingStrictSpan = null;
  
  private int mLastScrollState = 0;
  
  private float mVelocityScale = 1.0F;
  
  final boolean[] mIsScrap = new boolean[1];
  
  private final int[] mScrollOffset = new int[2];
  
  private final int[] mScrollConsumed = new int[2];
  
  private final float[] mTmpPoint = new float[2];
  
  private int mNestedYOffset = 0;
  
  private int mActivePointerId = -1;
  
  private EdgeEffect mEdgeGlowTop = new EdgeEffect(this.mContext);
  
  private EdgeEffect mEdgeGlowBottom = new EdgeEffect(this.mContext);
  
  private int mDirection = 0;
  
  static final Interpolator sLinearInterpolator = new LinearInterpolator();
  
  private boolean mIsFirstTouchMoveEvent = false;
  
  private int mNumTouchMoveEvent = 0;
  
  private static final int CHECK_POSITION_SEARCH_DISTANCE = 20;
  
  public static final int CHOICE_MODE_MULTIPLE = 2;
  
  public static final int CHOICE_MODE_MULTIPLE_MODAL = 3;
  
  public static final int CHOICE_MODE_NONE = 0;
  
  public static final int CHOICE_MODE_SINGLE = 1;
  
  private static final int INVALID_POINTER = -1;
  
  static final int LAYOUT_FORCE_BOTTOM = 3;
  
  static final int LAYOUT_FORCE_TOP = 1;
  
  static final int LAYOUT_MOVE_SELECTION = 6;
  
  static final int LAYOUT_NORMAL = 0;
  
  static final int LAYOUT_SET_SELECTION = 2;
  
  static final int LAYOUT_SPECIFIC = 4;
  
  static final int LAYOUT_SYNC = 5;
  
  private static final double MOVE_TOUCH_SLOP = 0.6D;
  
  private static final boolean OPTS_INPUT = true;
  
  static final int OVERSCROLL_LIMIT_DIVISOR = 3;
  
  private static final boolean PROFILE_FLINGING = false;
  
  private static final boolean PROFILE_SCROLLING = false;
  
  private static final String TAG = "AbsListView";
  
  static final int TOUCH_MODE_DONE_WAITING = 2;
  
  static final int TOUCH_MODE_DOWN = 0;
  
  static final int TOUCH_MODE_FLING = 4;
  
  private static final int TOUCH_MODE_OFF = 1;
  
  private static final int TOUCH_MODE_ON = 0;
  
  static final int TOUCH_MODE_OVERFLING = 6;
  
  static final int TOUCH_MODE_OVERSCROLL = 5;
  
  static final int TOUCH_MODE_REST = -1;
  
  static final int TOUCH_MODE_SCROLL = 3;
  
  static final int TOUCH_MODE_TAP = 1;
  
  private static final int TOUCH_MODE_UNKNOWN = -1;
  
  private static final double TOUCH_SLOP_MAX = 1.0D;
  
  private static final double TOUCH_SLOP_MIN = 0.6D;
  
  public static final int TRANSCRIPT_MODE_ALWAYS_SCROLL = 2;
  
  public static final int TRANSCRIPT_MODE_DISABLED = 0;
  
  public static final int TRANSCRIPT_MODE_NORMAL = 1;
  
  private ListItemAccessibilityDelegate mAccessibilityDelegate;
  
  ListAdapter mAdapter;
  
  boolean mAdapterHasStableIds;
  
  private int mCacheColorHint;
  
  boolean mCachingActive;
  
  boolean mCachingStarted;
  
  SparseBooleanArray mCheckStates;
  
  LongSparseArray<Integer> mCheckedIdStates;
  
  int mCheckedItemCount;
  
  ActionMode mChoiceActionMode;
  
  private Runnable mClearScrollingCache;
  
  private IOplusViewConfigHelper mColorViewConfigHelper;
  
  AdapterDataSetObserver mDataSetObserver;
  
  private InputConnection mDefInputConnection;
  
  private float mDensityScale;
  
  private FastScroller mFastScroll;
  
  boolean mFastScrollAlwaysVisible;
  
  boolean mFastScrollEnabled;
  
  private int mFastScrollStyle;
  
  private boolean mFiltered;
  
  private int mFirstPositionDistanceGuess;
  
  private FlingRunnable mFlingRunnable;
  
  private boolean mForceTranscriptScroll;
  
  private boolean mGlobalLayoutListenerAddedFilter;
  
  private boolean mHasPerformedLongPress;
  
  private boolean mIsChildViewEnabled;
  
  private boolean mIsDetaching;
  
  private int mLastHandledItemCount;
  
  private int mLastPositionDistanceGuess;
  
  int mLastY;
  
  private int mMaximumVelocity;
  
  private int mMinimumVelocity;
  
  int mMotionCorrection;
  
  int mMotionPosition;
  
  int mMotionViewNewTop;
  
  int mMotionViewOriginalTop;
  
  int mMotionX;
  
  int mMotionY;
  
  private int mMoveAcceleration;
  
  MultiChoiceModeWrapper mMultiChoiceModeCallback;
  
  private OnScrollListener mOnScrollListener;
  
  int mOverflingDistance;
  
  int mOverscrollDistance;
  
  int mOverscrollMax;
  
  private final Thread mOwnerThread;
  
  private CheckForKeyLongPress mPendingCheckForKeyLongPress;
  
  private CheckForLongPress mPendingCheckForLongPress;
  
  private CheckForTap mPendingCheckForTap;
  
  private SavedState mPendingSync;
  
  private PerformClick mPerformClick;
  
  PopupWindow mPopup;
  
  private boolean mPopupHidden;
  
  Runnable mPositionScrollAfterLayout;
  
  AbsPositionScroller mPositionScroller;
  
  private InputConnectionWrapper mPublicInputConnection;
  
  private RemoteViewsAdapter mRemoteAdapter;
  
  View mScrollDown;
  
  View mScrollUp;
  
  boolean mScrollingCacheEnabled;
  
  Drawable mSelector;
  
  private int[] mSelectorState;
  
  boolean mStackFromBottom;
  
  EditText mTextFilter;
  
  private boolean mTextFilterEnabled;
  
  private boolean mTopThirdPartApp;
  
  private Rect mTouchFrame;
  
  private Runnable mTouchModeReset;
  
  private int mTouchSlop;
  
  private int mTranscriptMode;
  
  private VelocityTracker mVelocityTracker;
  
  private float mVerticalScrollFactor;
  
  public AbsListView(Context paramContext) {
    super(paramContext);
    this.mTopThirdPartApp = false;
    initAbsListView();
    this.mOwnerThread = Thread.currentThread();
    setVerticalScrollBarEnabled(true);
    TypedArray typedArray = paramContext.obtainStyledAttributes(R.styleable.View);
    initializeScrollbarsInternal(typedArray);
    typedArray.recycle();
  }
  
  public AbsListView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842858);
  }
  
  public AbsListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AbsListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mTopThirdPartApp = false;
    initAbsListView();
    this.mOwnerThread = Thread.currentThread();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AbsListView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.AbsListView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    Drawable drawable = typedArray.getDrawable(0);
    if (drawable != null)
      setSelector(drawable); 
    this.mDrawSelectorOnTop = typedArray.getBoolean(1, false);
    setStackFromBottom(typedArray.getBoolean(2, false));
    setScrollingCacheEnabled(typedArray.getBoolean(3, true));
    setTextFilterEnabled(typedArray.getBoolean(4, false));
    setTranscriptMode(typedArray.getInt(5, 0));
    setCacheColorHint(typedArray.getColor(6, 0));
    setSmoothScrollbarEnabled(typedArray.getBoolean(9, true));
    setChoiceMode(typedArray.getInt(7, 0));
    setFastScrollEnabled(typedArray.getBoolean(8, false));
    setFastScrollStyle(typedArray.getResourceId(11, 0));
    setFastScrollAlwaysVisible(typedArray.getBoolean(10, false));
    typedArray.recycle();
    if ((paramContext.getResources().getConfiguration()).uiMode == 6)
      setRevealOnFocusHint(false); 
  }
  
  private void initAbsListView() {
    setClickable(true);
    setFocusableInTouchMode(true);
    setWillNotDraw(false);
    setAlwaysDrawnWithCacheEnabled(false);
    setScrollingCacheEnabled(true);
    ViewConfiguration viewConfiguration = ViewConfiguration.get(this.mContext);
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    this.mVerticalScrollFactor = viewConfiguration.getScaledVerticalScrollFactor();
    if (0.6D > 0.0D) {
      if (0.6D < 0.6D) {
        this.mMoveAcceleration = (int)(this.mTouchSlop * 0.6D);
      } else if (0.6D >= 0.6D && 0.6D < 1.0D) {
        this.mMoveAcceleration = (int)(this.mTouchSlop * 0.6D);
      } else {
        this.mMoveAcceleration = this.mTouchSlop;
      } 
    } else {
      this.mMoveAcceleration = this.mTouchSlop;
    } 
    this.mMinimumVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
    this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    this.mOverscrollDistance = viewConfiguration.getScaledOverscrollDistance();
    this.mOverflingDistance = viewConfiguration.getScaledOverflingDistance();
    IOplusViewConfigHelper iOplusViewConfigHelper = ColorFrameworkFactory.getInstance().getOplusViewHooks(null, null).getOplusViewConfigHelper(this.mContext);
    this.mOverscrollDistance = iOplusViewConfigHelper.getScaledOverscrollDistance(this.mOverscrollDistance);
    this.mOverflingDistance = this.mColorViewConfigHelper.getScaledOverflingDistance(this.mOverflingDistance);
    this.mDensityScale = (getContext().getResources().getDisplayMetrics()).density;
  }
  
  public void setAdapter(ListAdapter paramListAdapter) {
    if (paramListAdapter != null) {
      boolean bool = this.mAdapter.hasStableIds();
      if (this.mChoiceMode != 0 && bool && this.mCheckedIdStates == null)
        this.mCheckedIdStates = new LongSparseArray<>(); 
    } 
    clearChoices();
  }
  
  public int getCheckedItemCount() {
    return this.mCheckedItemCount;
  }
  
  public boolean isItemChecked(int paramInt) {
    if (this.mChoiceMode != 0) {
      SparseBooleanArray sparseBooleanArray = this.mCheckStates;
      if (sparseBooleanArray != null)
        return sparseBooleanArray.get(paramInt); 
    } 
    return false;
  }
  
  public int getCheckedItemPosition() {
    if (this.mChoiceMode == 1) {
      SparseBooleanArray sparseBooleanArray = this.mCheckStates;
      if (sparseBooleanArray != null && sparseBooleanArray.size() == 1)
        return this.mCheckStates.keyAt(0); 
    } 
    return -1;
  }
  
  public SparseBooleanArray getCheckedItemPositions() {
    if (this.mChoiceMode != 0)
      return this.mCheckStates; 
    return null;
  }
  
  public long[] getCheckedItemIds() {
    if (this.mChoiceMode == 0 || this.mCheckedIdStates == null || this.mAdapter == null)
      return new long[0]; 
    LongSparseArray<Integer> longSparseArray = this.mCheckedIdStates;
    int i = longSparseArray.size();
    long[] arrayOfLong = new long[i];
    for (byte b = 0; b < i; b++)
      arrayOfLong[b] = longSparseArray.keyAt(b); 
    return arrayOfLong;
  }
  
  public void clearChoices() {
    SparseBooleanArray sparseBooleanArray = this.mCheckStates;
    if (sparseBooleanArray != null)
      sparseBooleanArray.clear(); 
    LongSparseArray<Integer> longSparseArray = this.mCheckedIdStates;
    if (longSparseArray != null)
      longSparseArray.clear(); 
    this.mCheckedItemCount = 0;
  }
  
  public void setItemChecked(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mChoiceMode : I
    //   4: istore_3
    //   5: iload_3
    //   6: ifne -> 10
    //   9: return
    //   10: iload_2
    //   11: ifeq -> 71
    //   14: iload_3
    //   15: iconst_3
    //   16: if_icmpne -> 71
    //   19: aload_0
    //   20: getfield mChoiceActionMode : Landroid/view/ActionMode;
    //   23: ifnonnull -> 71
    //   26: aload_0
    //   27: getfield mMultiChoiceModeCallback : Landroid/widget/AbsListView$MultiChoiceModeWrapper;
    //   30: astore #4
    //   32: aload #4
    //   34: ifnull -> 60
    //   37: aload #4
    //   39: invokevirtual hasWrappedCallback : ()Z
    //   42: ifeq -> 60
    //   45: aload_0
    //   46: aload_0
    //   47: aload_0
    //   48: getfield mMultiChoiceModeCallback : Landroid/widget/AbsListView$MultiChoiceModeWrapper;
    //   51: invokevirtual startActionMode : (Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    //   54: putfield mChoiceActionMode : Landroid/view/ActionMode;
    //   57: goto -> 71
    //   60: new java/lang/IllegalStateException
    //   63: dup
    //   64: ldc_w 'AbsListView: attempted to start selection mode for CHOICE_MODE_MULTIPLE_MODAL but no choice mode callback was supplied. Call setMultiChoiceModeListener to set a callback.'
    //   67: invokespecial <init> : (Ljava/lang/String;)V
    //   70: athrow
    //   71: aload_0
    //   72: getfield mChoiceMode : I
    //   75: istore #5
    //   77: iconst_0
    //   78: istore_3
    //   79: iload #5
    //   81: iconst_2
    //   82: if_icmpeq -> 251
    //   85: iload #5
    //   87: iconst_3
    //   88: if_icmpne -> 94
    //   91: goto -> 251
    //   94: aload_0
    //   95: getfield mCheckedIdStates : Landroid/util/LongSparseArray;
    //   98: ifnull -> 119
    //   101: aload_0
    //   102: getfield mAdapter : Landroid/widget/ListAdapter;
    //   105: invokeinterface hasStableIds : ()Z
    //   110: ifeq -> 119
    //   113: iconst_1
    //   114: istore #5
    //   116: goto -> 122
    //   119: iconst_0
    //   120: istore #5
    //   122: aload_0
    //   123: iload_1
    //   124: invokevirtual isItemChecked : (I)Z
    //   127: iload_2
    //   128: if_icmpeq -> 136
    //   131: iconst_1
    //   132: istore_3
    //   133: goto -> 138
    //   136: iconst_0
    //   137: istore_3
    //   138: iload_2
    //   139: ifne -> 150
    //   142: aload_0
    //   143: iload_1
    //   144: invokevirtual isItemChecked : (I)Z
    //   147: ifeq -> 169
    //   150: aload_0
    //   151: getfield mCheckStates : Landroid/util/SparseBooleanArray;
    //   154: invokevirtual clear : ()V
    //   157: iload #5
    //   159: ifeq -> 169
    //   162: aload_0
    //   163: getfield mCheckedIdStates : Landroid/util/LongSparseArray;
    //   166: invokevirtual clear : ()V
    //   169: iload_2
    //   170: ifeq -> 218
    //   173: aload_0
    //   174: getfield mCheckStates : Landroid/util/SparseBooleanArray;
    //   177: iload_1
    //   178: iconst_1
    //   179: invokevirtual put : (IZ)V
    //   182: iload #5
    //   184: ifeq -> 208
    //   187: aload_0
    //   188: getfield mCheckedIdStates : Landroid/util/LongSparseArray;
    //   191: aload_0
    //   192: getfield mAdapter : Landroid/widget/ListAdapter;
    //   195: iload_1
    //   196: invokeinterface getItemId : (I)J
    //   201: iload_1
    //   202: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   205: invokevirtual put : (JLjava/lang/Object;)V
    //   208: aload_0
    //   209: iconst_1
    //   210: putfield mCheckedItemCount : I
    //   213: iload_3
    //   214: istore_1
    //   215: goto -> 409
    //   218: aload_0
    //   219: getfield mCheckStates : Landroid/util/SparseBooleanArray;
    //   222: invokevirtual size : ()I
    //   225: ifeq -> 241
    //   228: iload_3
    //   229: istore_1
    //   230: aload_0
    //   231: getfield mCheckStates : Landroid/util/SparseBooleanArray;
    //   234: iconst_0
    //   235: invokevirtual valueAt : (I)Z
    //   238: ifne -> 409
    //   241: aload_0
    //   242: iconst_0
    //   243: putfield mCheckedItemCount : I
    //   246: iload_3
    //   247: istore_1
    //   248: goto -> 409
    //   251: aload_0
    //   252: getfield mCheckStates : Landroid/util/SparseBooleanArray;
    //   255: iload_1
    //   256: invokevirtual get : (I)Z
    //   259: istore #6
    //   261: aload_0
    //   262: getfield mCheckStates : Landroid/util/SparseBooleanArray;
    //   265: iload_1
    //   266: iload_2
    //   267: invokevirtual put : (IZ)V
    //   270: aload_0
    //   271: getfield mCheckedIdStates : Landroid/util/LongSparseArray;
    //   274: ifnull -> 334
    //   277: aload_0
    //   278: getfield mAdapter : Landroid/widget/ListAdapter;
    //   281: invokeinterface hasStableIds : ()Z
    //   286: ifeq -> 334
    //   289: iload_2
    //   290: ifeq -> 317
    //   293: aload_0
    //   294: getfield mCheckedIdStates : Landroid/util/LongSparseArray;
    //   297: aload_0
    //   298: getfield mAdapter : Landroid/widget/ListAdapter;
    //   301: iload_1
    //   302: invokeinterface getItemId : (I)J
    //   307: iload_1
    //   308: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   311: invokevirtual put : (JLjava/lang/Object;)V
    //   314: goto -> 334
    //   317: aload_0
    //   318: getfield mCheckedIdStates : Landroid/util/LongSparseArray;
    //   321: aload_0
    //   322: getfield mAdapter : Landroid/widget/ListAdapter;
    //   325: iload_1
    //   326: invokeinterface getItemId : (I)J
    //   331: invokevirtual delete : (J)V
    //   334: iload #6
    //   336: iload_2
    //   337: if_icmpeq -> 342
    //   340: iconst_1
    //   341: istore_3
    //   342: iload_3
    //   343: ifeq -> 373
    //   346: iload_2
    //   347: ifeq -> 363
    //   350: aload_0
    //   351: aload_0
    //   352: getfield mCheckedItemCount : I
    //   355: iconst_1
    //   356: iadd
    //   357: putfield mCheckedItemCount : I
    //   360: goto -> 373
    //   363: aload_0
    //   364: aload_0
    //   365: getfield mCheckedItemCount : I
    //   368: iconst_1
    //   369: isub
    //   370: putfield mCheckedItemCount : I
    //   373: aload_0
    //   374: getfield mChoiceActionMode : Landroid/view/ActionMode;
    //   377: ifnull -> 407
    //   380: aload_0
    //   381: getfield mAdapter : Landroid/widget/ListAdapter;
    //   384: iload_1
    //   385: invokeinterface getItemId : (I)J
    //   390: lstore #7
    //   392: aload_0
    //   393: getfield mMultiChoiceModeCallback : Landroid/widget/AbsListView$MultiChoiceModeWrapper;
    //   396: aload_0
    //   397: getfield mChoiceActionMode : Landroid/view/ActionMode;
    //   400: iload_1
    //   401: lload #7
    //   403: iload_2
    //   404: invokevirtual onItemCheckedStateChanged : (Landroid/view/ActionMode;IJZ)V
    //   407: iload_3
    //   408: istore_1
    //   409: aload_0
    //   410: getfield mInLayout : Z
    //   413: ifne -> 440
    //   416: aload_0
    //   417: getfield mBlockLayoutRequests : Z
    //   420: ifne -> 440
    //   423: iload_1
    //   424: ifeq -> 440
    //   427: aload_0
    //   428: iconst_1
    //   429: putfield mDataChanged : Z
    //   432: aload_0
    //   433: invokevirtual rememberSyncState : ()V
    //   436: aload_0
    //   437: invokevirtual requestLayout : ()V
    //   440: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1105	-> 0
    //   #1106	-> 9
    //   #1110	-> 10
    //   #1111	-> 26
    //   #1112	-> 37
    //   #1117	-> 45
    //   #1113	-> 60
    //   #1121	-> 71
    //   #1145	-> 94
    //   #1148	-> 122
    //   #1149	-> 138
    //   #1150	-> 150
    //   #1151	-> 157
    //   #1152	-> 162
    //   #1157	-> 169
    //   #1158	-> 173
    //   #1159	-> 182
    //   #1160	-> 187
    //   #1162	-> 208
    //   #1163	-> 218
    //   #1164	-> 241
    //   #1122	-> 251
    //   #1123	-> 261
    //   #1124	-> 270
    //   #1125	-> 289
    //   #1126	-> 293
    //   #1128	-> 317
    //   #1131	-> 334
    //   #1132	-> 342
    //   #1133	-> 346
    //   #1134	-> 350
    //   #1136	-> 363
    //   #1139	-> 373
    //   #1140	-> 380
    //   #1141	-> 392
    //   #1144	-> 407
    //   #1169	-> 409
    //   #1170	-> 427
    //   #1171	-> 432
    //   #1172	-> 436
    //   #1174	-> 440
  }
  
  public boolean performItemClick(View paramView, int paramInt, long paramLong) {
    boolean bool;
    int i = 0;
    boolean bool1 = true, bool2 = true, bool3 = true, bool4 = true;
    int j = this.mChoiceMode;
    if (j != 0) {
      boolean bool5 = false;
      if (j == 2 || (j == 3 && this.mChoiceActionMode != null)) {
        i = this.mCheckStates.get(paramInt, false) ^ true;
        this.mCheckStates.put(paramInt, i);
        if (this.mCheckedIdStates != null && this.mAdapter.hasStableIds())
          if (i != 0) {
            this.mCheckedIdStates.put(this.mAdapter.getItemId(paramInt), Integer.valueOf(paramInt));
          } else {
            this.mCheckedIdStates.delete(this.mAdapter.getItemId(paramInt));
          }  
        if (i != 0) {
          this.mCheckedItemCount++;
        } else {
          this.mCheckedItemCount--;
        } 
        ActionMode actionMode = this.mChoiceActionMode;
        bool3 = bool1;
        if (actionMode != null) {
          this.mMultiChoiceModeCallback.onItemCheckedStateChanged(actionMode, paramInt, paramLong, i);
          bool3 = false;
        } 
        bool4 = true;
      } else {
        bool3 = bool4;
        bool4 = bool5;
        if (this.mChoiceMode == 1) {
          boolean bool6 = this.mCheckStates.get(paramInt, false);
          if ((bool6 ^ true) != 0) {
            this.mCheckStates.clear();
            this.mCheckStates.put(paramInt, true);
            if (this.mCheckedIdStates != null && this.mAdapter.hasStableIds()) {
              this.mCheckedIdStates.clear();
              this.mCheckedIdStates.put(this.mAdapter.getItemId(paramInt), Integer.valueOf(paramInt));
            } 
            this.mCheckedItemCount = 1;
          } else if (this.mCheckStates.size() == 0 || !this.mCheckStates.valueAt(0)) {
            this.mCheckedItemCount = 0;
          } 
          bool4 = true;
          bool3 = bool2;
        } 
      } 
      if (bool4)
        updateOnScreenCheckedViews(); 
      i = 1;
    } 
    int k = i;
    if (bool3)
      bool = i | super.performItemClick(paramView, paramInt, paramLong); 
    return bool;
  }
  
  private void updateOnScreenCheckedViews() {
    boolean bool;
    int i = this.mFirstPosition;
    int j = getChildCount();
    if ((getContext().getApplicationInfo()).targetSdkVersion >= 11) {
      bool = true;
    } else {
      bool = false;
    } 
    for (byte b = 0; b < j; b++) {
      View view = getChildAt(b);
      int k = i + b;
      if (view instanceof Checkable) {
        ((Checkable)view).setChecked(this.mCheckStates.get(k));
      } else if (bool) {
        view.setActivated(this.mCheckStates.get(k));
      } 
    } 
  }
  
  public int getChoiceMode() {
    return this.mChoiceMode;
  }
  
  public void setChoiceMode(int paramInt) {
    this.mChoiceMode = paramInt;
    ActionMode actionMode = this.mChoiceActionMode;
    if (actionMode != null) {
      actionMode.finish();
      this.mChoiceActionMode = null;
    } 
    if (this.mChoiceMode != 0) {
      if (this.mCheckStates == null)
        this.mCheckStates = new SparseBooleanArray(0); 
      if (this.mCheckedIdStates == null) {
        ListAdapter listAdapter = this.mAdapter;
        if (listAdapter != null && listAdapter.hasStableIds())
          this.mCheckedIdStates = new LongSparseArray<>(0); 
      } 
      if (this.mChoiceMode == 3) {
        clearChoices();
        setLongClickable(true);
      } 
    } 
  }
  
  public void setMultiChoiceModeListener(MultiChoiceModeListener paramMultiChoiceModeListener) {
    if (this.mMultiChoiceModeCallback == null)
      this.mMultiChoiceModeCallback = new MultiChoiceModeWrapper(); 
    this.mMultiChoiceModeCallback.setWrapped(paramMultiChoiceModeListener);
  }
  
  private boolean contentFits() {
    int i = getChildCount();
    boolean bool = true;
    if (i == 0)
      return true; 
    if (i != this.mItemCount)
      return false; 
    if (getChildAt(0).getTop() < this.mListPadding.top || getChildAt(i - 1).getBottom() > getHeight() - this.mListPadding.bottom)
      bool = false; 
    return bool;
  }
  
  public void setFastScrollEnabled(boolean paramBoolean) {
    if (this.mFastScrollEnabled != paramBoolean) {
      this.mFastScrollEnabled = paramBoolean;
      if (isOwnerThread()) {
        setFastScrollerEnabledUiThread(paramBoolean);
      } else {
        post((Runnable)new Object(this, paramBoolean));
      } 
    } 
  }
  
  private void setFastScrollerEnabledUiThread(boolean paramBoolean) {
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null) {
      fastScroller.setEnabled(paramBoolean);
    } else if (paramBoolean) {
      this.mFastScroll = fastScroller = ((IOplusListHooks)ColorFrameworkFactory.getInstance().getFeature(IOplusListHooks.DEFAULT, new Object[0])).getFastScroller(this, this.mFastScrollStyle);
      fastScroller.setEnabled(true);
    } 
    resolvePadding();
    fastScroller = this.mFastScroll;
    if (fastScroller != null)
      fastScroller.updateLayout(); 
  }
  
  public void setFastScrollStyle(int paramInt) {
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller == null) {
      this.mFastScrollStyle = paramInt;
    } else {
      fastScroller.setStyle(paramInt);
    } 
  }
  
  public void setFastScrollAlwaysVisible(boolean paramBoolean) {
    if (this.mFastScrollAlwaysVisible != paramBoolean) {
      if (paramBoolean && !this.mFastScrollEnabled)
        setFastScrollEnabled(true); 
      this.mFastScrollAlwaysVisible = paramBoolean;
      if (isOwnerThread()) {
        setFastScrollerAlwaysVisibleUiThread(paramBoolean);
      } else {
        post((Runnable)new Object(this, paramBoolean));
      } 
    } 
  }
  
  private void setFastScrollerAlwaysVisibleUiThread(boolean paramBoolean) {
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null)
      fastScroller.setAlwaysShow(paramBoolean); 
  }
  
  private boolean isOwnerThread() {
    boolean bool;
    if (this.mOwnerThread == Thread.currentThread()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isFastScrollAlwaysVisible() {
    FastScroller fastScroller = this.mFastScroll;
    boolean bool1 = true, bool2 = true;
    if (fastScroller == null) {
      if (!this.mFastScrollEnabled || !this.mFastScrollAlwaysVisible)
        bool2 = false; 
      return bool2;
    } 
    if (fastScroller.isEnabled() && this.mFastScroll.isAlwaysShowEnabled()) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  public int getVerticalScrollbarWidth() {
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null && fastScroller.isEnabled())
      return Math.max(super.getVerticalScrollbarWidth(), this.mFastScroll.getWidth()); 
    return super.getVerticalScrollbarWidth();
  }
  
  @ExportedProperty
  public boolean isFastScrollEnabled() {
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller == null)
      return this.mFastScrollEnabled; 
    return fastScroller.isEnabled();
  }
  
  public void setVerticalScrollbarPosition(int paramInt) {
    super.setVerticalScrollbarPosition(paramInt);
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null)
      fastScroller.setScrollbarPosition(paramInt); 
  }
  
  public void setScrollBarStyle(int paramInt) {
    super.setScrollBarStyle(paramInt);
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null)
      fastScroller.setScrollBarStyle(paramInt); 
  }
  
  protected boolean isVerticalScrollBarHidden() {
    return isFastScrollEnabled();
  }
  
  public void setSmoothScrollbarEnabled(boolean paramBoolean) {
    this.mSmoothScrollbarEnabled = paramBoolean;
  }
  
  @ExportedProperty
  public boolean isSmoothScrollbarEnabled() {
    return this.mSmoothScrollbarEnabled;
  }
  
  public void setOnScrollListener(OnScrollListener paramOnScrollListener) {
    this.mOnScrollListener = paramOnScrollListener;
    invokeOnItemScrollListener();
  }
  
  void invokeOnItemScrollListener() {
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null)
      fastScroller.onScroll(this.mFirstPosition, getChildCount(), this.mItemCount); 
    OnScrollListener onScrollListener = this.mOnScrollListener;
    if (onScrollListener != null)
      onScrollListener.onScroll(this, this.mFirstPosition, getChildCount(), this.mItemCount); 
    onScrollChanged(0, 0, 0, 0);
  }
  
  public CharSequence getAccessibilityClassName() {
    return AbsListView.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (isEnabled()) {
      if (canScrollUp()) {
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP);
        paramAccessibilityNodeInfo.setScrollable(true);
      } 
      if (canScrollDown()) {
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN);
        paramAccessibilityNodeInfo.setScrollable(true);
      } 
    } 
    paramAccessibilityNodeInfo.removeAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_CLICK);
    paramAccessibilityNodeInfo.setClickable(false);
  }
  
  int getSelectionModeForAccessibility() {
    int i = getChoiceMode();
    if (i != 1) {
      if (i != 2 && i != 3)
        return 0; 
      return 2;
    } 
    return 1;
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    if (paramInt != 4096)
      if (paramInt != 8192 && paramInt != 16908344) {
        if (paramInt != 16908346)
          return false; 
      } else {
        if (isEnabled() && canScrollUp()) {
          int i = getHeight();
          paramInt = this.mListPadding.top;
          int j = this.mListPadding.bottom;
          smoothScrollBy(-(i - paramInt - j), 200);
          return true;
        } 
        return false;
      }  
    if (isEnabled() && canScrollDown()) {
      paramInt = getHeight();
      int j = this.mListPadding.top, i = this.mListPadding.bottom;
      smoothScrollBy(paramInt - j - i, 200);
      return true;
    } 
    return false;
  }
  
  @ExportedProperty
  public boolean isScrollingCacheEnabled() {
    return this.mScrollingCacheEnabled;
  }
  
  public void setScrollingCacheEnabled(boolean paramBoolean) {
    if (this.mScrollingCacheEnabled && !paramBoolean)
      clearScrollingCache(); 
    this.mScrollingCacheEnabled = paramBoolean;
  }
  
  public void setTextFilterEnabled(boolean paramBoolean) {
    this.mTextFilterEnabled = paramBoolean;
  }
  
  @ExportedProperty
  public boolean isTextFilterEnabled() {
    return this.mTextFilterEnabled;
  }
  
  public void getFocusedRect(Rect paramRect) {
    View view = getSelectedView();
    if (view != null && view.getParent() == this) {
      view.getFocusedRect(paramRect);
      offsetDescendantRectToMyCoords(view, paramRect);
    } else {
      super.getFocusedRect(paramRect);
    } 
  }
  
  private void useDefaultSelector() {
    setSelector(getContext().getDrawable(17301602));
  }
  
  @ExportedProperty
  public boolean isStackFromBottom() {
    return this.mStackFromBottom;
  }
  
  public void setStackFromBottom(boolean paramBoolean) {
    if (this.mStackFromBottom != paramBoolean) {
      this.mStackFromBottom = paramBoolean;
      requestLayoutIfNecessary();
    } 
  }
  
  void requestLayoutIfNecessary() {
    if (getChildCount() > 0) {
      resetList();
      requestLayout();
      invalidate();
    } 
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState() {}
    
    private SavedState(AbsListView this$0) {
      boolean bool;
      this.selectedId = this$0.readLong();
      this.firstId = this$0.readLong();
      this.viewTop = this$0.readInt();
      this.position = this$0.readInt();
      this.height = this$0.readInt();
      this.filter = this$0.readString();
      if (this$0.readByte() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.inActionMode = bool;
      this.checkedItemCount = this$0.readInt();
      this.checkState = this$0.readSparseBooleanArray();
      int i = this$0.readInt();
      if (i > 0) {
        this.checkIdState = new LongSparseArray<>();
        for (byte b = 0; b < i; b++) {
          long l = this$0.readLong();
          int j = this$0.readInt();
          this.checkIdState.put(l, Integer.valueOf(j));
        } 
      } 
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeLong(this.selectedId);
      param1Parcel.writeLong(this.firstId);
      param1Parcel.writeInt(this.viewTop);
      param1Parcel.writeInt(this.position);
      param1Parcel.writeInt(this.height);
      param1Parcel.writeString(this.filter);
      param1Parcel.writeByte((byte)this.inActionMode);
      param1Parcel.writeInt(this.checkedItemCount);
      param1Parcel.writeSparseBooleanArray(this.checkState);
      LongSparseArray<Integer> longSparseArray = this.checkIdState;
      if (longSparseArray != null) {
        param1Int = longSparseArray.size();
      } else {
        param1Int = 0;
      } 
      param1Parcel.writeInt(param1Int);
      for (byte b = 0; b < param1Int; b++) {
        param1Parcel.writeLong(this.checkIdState.keyAt(b));
        param1Parcel.writeInt(((Integer)this.checkIdState.valueAt(b)).intValue());
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AbsListView.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" selectedId=");
      stringBuilder.append(this.selectedId);
      stringBuilder.append(" firstId=");
      stringBuilder.append(this.firstId);
      stringBuilder.append(" viewTop=");
      stringBuilder.append(this.viewTop);
      stringBuilder.append(" position=");
      stringBuilder.append(this.position);
      stringBuilder.append(" height=");
      stringBuilder.append(this.height);
      stringBuilder.append(" filter=");
      stringBuilder.append(this.filter);
      stringBuilder.append(" checkState=");
      stringBuilder.append(this.checkState);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    LongSparseArray<Integer> checkIdState;
    
    SparseBooleanArray checkState;
    
    int checkedItemCount;
    
    String filter;
    
    long firstId;
    
    int height;
    
    boolean inActionMode;
    
    int position;
    
    long selectedId;
    
    int viewTop;
  }
  
  public Parcelable onSaveInstanceState() {
    dismissPopup();
    Parcelable parcelable = super.onSaveInstanceState();
    parcelable = new SavedState();
    SavedState savedState = this.mPendingSync;
    if (savedState != null) {
      ((SavedState)parcelable).selectedId = savedState.selectedId;
      ((SavedState)parcelable).firstId = this.mPendingSync.firstId;
      ((SavedState)parcelable).viewTop = this.mPendingSync.viewTop;
      ((SavedState)parcelable).position = this.mPendingSync.position;
      ((SavedState)parcelable).height = this.mPendingSync.height;
      ((SavedState)parcelable).filter = this.mPendingSync.filter;
      ((SavedState)parcelable).inActionMode = this.mPendingSync.inActionMode;
      ((SavedState)parcelable).checkedItemCount = this.mPendingSync.checkedItemCount;
      ((SavedState)parcelable).checkState = this.mPendingSync.checkState;
      ((SavedState)parcelable).checkIdState = this.mPendingSync.checkIdState;
      return parcelable;
    } 
    int i = getChildCount();
    boolean bool = true;
    if (i > 0 && this.mItemCount > 0) {
      i = 1;
    } else {
      i = 0;
    } 
    long l = getSelectedItemId();
    ((SavedState)parcelable).selectedId = l;
    ((SavedState)parcelable).height = getHeight();
    if (l >= 0L) {
      ((SavedState)parcelable).viewTop = this.mSelectedTop;
      ((SavedState)parcelable).position = getSelectedItemPosition();
      ((SavedState)parcelable).firstId = -1L;
    } else if (i != 0 && this.mFirstPosition > 0) {
      View view = getChildAt(0);
      ((SavedState)parcelable).viewTop = view.getTop();
      int j = this.mFirstPosition;
      i = j;
      if (j >= this.mItemCount)
        i = this.mItemCount - 1; 
      ((SavedState)parcelable).position = i;
      ((SavedState)parcelable).firstId = this.mAdapter.getItemId(i);
    } else {
      ((SavedState)parcelable).viewTop = 0;
      ((SavedState)parcelable).firstId = -1L;
      ((SavedState)parcelable).position = 0;
    } 
    ((SavedState)parcelable).filter = null;
    if (this.mFiltered) {
      EditText editText = this.mTextFilter;
      if (editText != null) {
        Editable editable = editText.getText();
        if (editable != null)
          ((SavedState)parcelable).filter = editable.toString(); 
      } 
    } 
    if (this.mChoiceMode != 3 || this.mChoiceActionMode == null)
      bool = false; 
    ((SavedState)parcelable).inActionMode = bool;
    SparseBooleanArray sparseBooleanArray = this.mCheckStates;
    if (sparseBooleanArray != null)
      ((SavedState)parcelable).checkState = sparseBooleanArray.clone(); 
    if (this.mCheckedIdStates != null) {
      LongSparseArray<Integer> longSparseArray = new LongSparseArray();
      int j = this.mCheckedIdStates.size();
      for (i = 0; i < j; i++)
        longSparseArray.put(this.mCheckedIdStates.keyAt(i), this.mCheckedIdStates.valueAt(i)); 
      ((SavedState)parcelable).checkIdState = longSparseArray;
    } 
    ((SavedState)parcelable).checkedItemCount = this.mCheckedItemCount;
    RemoteViewsAdapter remoteViewsAdapter = this.mRemoteAdapter;
    if (remoteViewsAdapter != null)
      remoteViewsAdapter.saveRemoteViewsCache(); 
    return parcelable;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    this.mDataChanged = true;
    this.mSyncHeight = ((SavedState)paramParcelable).height;
    if (((SavedState)paramParcelable).selectedId >= 0L) {
      this.mNeedSync = true;
      this.mPendingSync = (SavedState)paramParcelable;
      this.mSyncRowId = ((SavedState)paramParcelable).selectedId;
      this.mSyncPosition = ((SavedState)paramParcelable).position;
      this.mSpecificTop = ((SavedState)paramParcelable).viewTop;
      this.mSyncMode = 0;
    } else if (((SavedState)paramParcelable).firstId >= 0L) {
      setSelectedPositionInt(-1);
      setNextSelectedPositionInt(-1);
      this.mSelectorPosition = -1;
      this.mNeedSync = true;
      this.mPendingSync = (SavedState)paramParcelable;
      this.mSyncRowId = ((SavedState)paramParcelable).firstId;
      this.mSyncPosition = ((SavedState)paramParcelable).position;
      this.mSpecificTop = ((SavedState)paramParcelable).viewTop;
      this.mSyncMode = 1;
    } 
    setFilterText(((SavedState)paramParcelable).filter);
    if (((SavedState)paramParcelable).checkState != null)
      this.mCheckStates = ((SavedState)paramParcelable).checkState; 
    if (((SavedState)paramParcelable).checkIdState != null)
      this.mCheckedIdStates = ((SavedState)paramParcelable).checkIdState; 
    this.mCheckedItemCount = ((SavedState)paramParcelable).checkedItemCount;
    if (((SavedState)paramParcelable).inActionMode && this.mChoiceMode == 3) {
      MultiChoiceModeWrapper multiChoiceModeWrapper = this.mMultiChoiceModeCallback;
      if (multiChoiceModeWrapper != null)
        this.mChoiceActionMode = startActionMode(multiChoiceModeWrapper); 
    } 
    requestLayout();
  }
  
  private boolean acceptFilter() {
    boolean bool;
    if (this.mTextFilterEnabled && getAdapter() instanceof Filterable && ((Filterable)getAdapter()).getFilter() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setFilterText(String paramString) {
    if (this.mTextFilterEnabled && !TextUtils.isEmpty(paramString)) {
      createTextFilter(false);
      this.mTextFilter.setText(paramString);
      this.mTextFilter.setSelection(paramString.length());
      ListAdapter listAdapter = this.mAdapter;
      if (listAdapter instanceof Filterable) {
        if (this.mPopup == null) {
          Filter filter = ((Filterable)listAdapter).getFilter();
          filter.filter(paramString);
        } 
        this.mFiltered = true;
        this.mDataSetObserver.clearSavedState();
      } 
    } 
  }
  
  public CharSequence getTextFilter() {
    if (this.mTextFilterEnabled) {
      EditText editText = this.mTextFilter;
      if (editText != null)
        return editText.getText(); 
    } 
    return null;
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    if (paramBoolean && this.mSelectedPosition < 0 && !isInTouchMode()) {
      if (!isAttachedToWindow() && this.mAdapter != null) {
        this.mDataChanged = true;
        this.mOldItemCount = this.mItemCount;
        this.mItemCount = this.mAdapter.getCount();
      } 
      resurrectSelection();
    } 
  }
  
  public void requestLayout() {
    if (!this.mBlockLayoutRequests && !this.mInLayout)
      super.requestLayout(); 
  }
  
  void resetList() {
    removeAllViewsInLayout();
    this.mFirstPosition = 0;
    this.mDataChanged = false;
    this.mPositionScrollAfterLayout = null;
    this.mNeedSync = false;
    this.mPendingSync = null;
    this.mOldSelectedPosition = -1;
    this.mOldSelectedRowId = Long.MIN_VALUE;
    setSelectedPositionInt(-1);
    setNextSelectedPositionInt(-1);
    this.mSelectedTop = 0;
    this.mSelectorPosition = -1;
    this.mSelectorRect.setEmpty();
    invalidate();
  }
  
  protected int computeVerticalScrollExtent() {
    int i = getChildCount();
    if (i > 0) {
      if (this.mSmoothScrollbarEnabled) {
        int j = i * 100;
        View view = getChildAt(0);
        int k = view.getTop();
        int m = view.getHeight();
        int n = j;
        if (m > 0)
          n = j + k * 100 / m; 
        view = getChildAt(i - 1);
        i = view.getBottom();
        m = view.getHeight();
        j = n;
        if (m > 0)
          j = n - (i - getHeight()) * 100 / m; 
        return j;
      } 
      return 1;
    } 
    return 0;
  }
  
  protected int computeVerticalScrollOffset() {
    int i = this.mFirstPosition;
    int j = getChildCount();
    if (i >= 0 && j > 0)
      if (this.mSmoothScrollbarEnabled) {
        View view = getChildAt(0);
        int k = view.getTop();
        int m = view.getHeight();
        if (m > 0) {
          m = k * 100 / m;
          float f = this.mScrollY;
          k = (int)(f / getHeight() * this.mItemCount * 100.0F);
          return Math.max(i * 100 - m + k, 0);
        } 
      } else {
        int k, m = this.mItemCount;
        if (i == 0) {
          k = 0;
        } else if (i + j == m) {
          k = m;
        } else {
          k = j / 2 + i;
        } 
        return (int)(i + j * k / m);
      }  
    return 0;
  }
  
  protected int computeVerticalScrollRange() {
    int i;
    if (this.mSmoothScrollbarEnabled) {
      int j = Math.max(this.mItemCount * 100, 0);
      i = j;
      if (this.mScrollY != 0)
        i = j + Math.abs((int)(this.mScrollY / getHeight() * this.mItemCount * 100.0F)); 
    } else {
      i = this.mItemCount;
    } 
    return i;
  }
  
  protected float getTopFadingEdgeStrength() {
    int i = getChildCount();
    float f1 = super.getTopFadingEdgeStrength();
    if (i == 0)
      return f1; 
    if (this.mFirstPosition > 0)
      return 1.0F; 
    i = getChildAt(0).getTop();
    float f2 = getVerticalFadingEdgeLength();
    if (i < this.mPaddingTop)
      f1 = -(i - this.mPaddingTop) / f2; 
    return f1;
  }
  
  protected float getBottomFadingEdgeStrength() {
    int i = getChildCount();
    float f1 = super.getBottomFadingEdgeStrength();
    if (i == 0)
      return f1; 
    if (this.mFirstPosition + i - 1 < this.mItemCount - 1)
      return 1.0F; 
    i = getChildAt(i - 1).getBottom();
    int j = getHeight();
    float f2 = getVerticalFadingEdgeLength();
    if (i > j - this.mPaddingBottom)
      f1 = (i - j + this.mPaddingBottom) / f2; 
    return f1;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    if (this.mSelector == null)
      useDefaultSelector(); 
    Rect rect = this.mListPadding;
    rect.left = this.mSelectionLeftPadding + this.mPaddingLeft;
    rect.top = this.mSelectionTopPadding + this.mPaddingTop;
    rect.right = this.mSelectionRightPadding + this.mPaddingRight;
    rect.bottom = this.mSelectionBottomPadding + this.mPaddingBottom;
    paramInt1 = this.mTranscriptMode;
    boolean bool = true;
    if (paramInt1 == 1) {
      int i = getChildCount();
      paramInt2 = getHeight() - getPaddingBottom();
      View view = getChildAt(i - 1);
      if (view != null) {
        paramInt1 = view.getBottom();
      } else {
        paramInt1 = paramInt2;
      } 
      if (this.mFirstPosition + i < this.mLastHandledItemCount || paramInt1 > paramInt2)
        bool = false; 
      this.mForceTranscriptScroll = bool;
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    this.mInLayout = true;
    paramInt3 = getChildCount();
    if (paramBoolean) {
      for (paramInt1 = 0; paramInt1 < paramInt3; paramInt1++)
        getChildAt(paramInt1).forceLayout(); 
      this.mRecycler.markChildrenDirty();
    } 
    layoutChildren();
    this.mOverscrollMax = (paramInt4 - paramInt2) / 3;
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null)
      fastScroller.onItemCountChanged(getChildCount(), this.mItemCount); 
    this.mInLayout = false;
  }
  
  protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
    if (bool) {
      if (getWindowVisibility() == 0) {
        paramInt1 = 1;
      } else {
        paramInt1 = 0;
      } 
      if (this.mFiltered && paramInt1 != 0) {
        PopupWindow popupWindow = this.mPopup;
        if (popupWindow != null && popupWindow.isShowing())
          positionPopup(); 
      } 
    } 
    return bool;
  }
  
  protected void layoutChildren() {}
  
  View getAccessibilityFocusedChild(View paramView) {
    ViewParent viewParent = paramView.getParent();
    while (viewParent instanceof View && viewParent != this) {
      paramView = (View)viewParent;
      viewParent = viewParent.getParent();
    } 
    if (!(viewParent instanceof View))
      return null; 
    return paramView;
  }
  
  void updateScrollIndicators() {
    View view = this.mScrollUp;
    boolean bool = false;
    if (view != null) {
      byte b;
      if (canScrollUp()) {
        b = 0;
      } else {
        b = 4;
      } 
      view.setVisibility(b);
    } 
    view = this.mScrollDown;
    if (view != null) {
      byte b;
      if (canScrollDown()) {
        b = bool;
      } else {
        b = 4;
      } 
      view.setVisibility(b);
    } 
  }
  
  private boolean canScrollUp() {
    boolean bool2;
    int i = this.mFirstPosition;
    boolean bool1 = true;
    if (i > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    boolean bool3 = bool2;
    if (!bool2) {
      bool3 = bool2;
      if (getChildCount() > 0) {
        View view = getChildAt(0);
        if (view.getTop() < this.mListPadding.top) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        bool3 = bool2;
      } 
    } 
    return bool3;
  }
  
  private boolean canScrollDown() {
    boolean bool2;
    int i = getChildCount();
    int j = this.mFirstPosition, k = this.mItemCount;
    boolean bool1 = false;
    if (j + i < k) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    boolean bool3 = bool2;
    if (!bool2) {
      bool3 = bool2;
      if (i > 0) {
        View view = getChildAt(i - 1);
        bool2 = bool1;
        if (view.getBottom() > this.mBottom - this.mListPadding.bottom)
          bool2 = true; 
        bool3 = bool2;
      } 
    } 
    return bool3;
  }
  
  @ExportedProperty
  public View getSelectedView() {
    if (this.mItemCount > 0 && this.mSelectedPosition >= 0)
      return getChildAt(this.mSelectedPosition - this.mFirstPosition); 
    return null;
  }
  
  public int getListPaddingTop() {
    return this.mListPadding.top;
  }
  
  public int getListPaddingBottom() {
    return this.mListPadding.bottom;
  }
  
  public int getListPaddingLeft() {
    return this.mListPadding.left;
  }
  
  public int getListPaddingRight() {
    return this.mListPadding.right;
  }
  
  View obtainView(int paramInt, boolean[] paramArrayOfboolean) {
    Trace.traceBegin(8L, "obtainView");
    paramArrayOfboolean[0] = false;
    View view1 = this.mRecycler.getTransientStateView(paramInt);
    if (view1 != null) {
      LayoutParams layoutParams = (LayoutParams)view1.getLayoutParams();
      if (layoutParams.viewType == this.mAdapter.getItemViewType(paramInt)) {
        View view = this.mAdapter.getView(paramInt, view1, this);
        if (view != view1) {
          setItemViewLayoutParams(view, paramInt);
          this.mRecycler.addScrapView(view, paramInt);
        } 
      } 
      paramArrayOfboolean[0] = true;
      view1.dispatchFinishTemporaryDetach();
      return view1;
    } 
    view1 = this.mRecycler.getScrapView(paramInt);
    View view2 = this.mAdapter.getView(paramInt, view1, this);
    if (view1 != null)
      if (view2 != view1) {
        this.mRecycler.addScrapView(view1, paramInt);
      } else if (view2.isTemporarilyDetached()) {
        paramArrayOfboolean[0] = true;
        view2.dispatchFinishTemporaryDetach();
      }  
    int i = this.mCacheColorHint;
    if (i != 0)
      view2.setDrawingCacheBackgroundColor(i); 
    if (view2.getImportantForAccessibility() == 0)
      view2.setImportantForAccessibility(1); 
    setItemViewLayoutParams(view2, paramInt);
    if (AccessibilityManager.getInstance(this.mContext).isEnabled()) {
      if (this.mAccessibilityDelegate == null)
        this.mAccessibilityDelegate = new ListItemAccessibilityDelegate(); 
      if (view2.getAccessibilityDelegate() == null)
        view2.setAccessibilityDelegate(this.mAccessibilityDelegate); 
    } 
    Trace.traceEnd(8L);
    return view2;
  }
  
  private void setItemViewLayoutParams(View paramView, int paramInt) {
    LayoutParams layoutParams1;
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    if (layoutParams == null) {
      layoutParams1 = (LayoutParams)generateDefaultLayoutParams();
    } else if (!checkLayoutParams(layoutParams)) {
      layoutParams1 = (LayoutParams)generateLayoutParams(layoutParams);
    } else {
      layoutParams1 = (LayoutParams)layoutParams;
    } 
    if (this.mAdapterHasStableIds)
      layoutParams1.itemId = this.mAdapter.getItemId(paramInt); 
    layoutParams1.viewType = this.mAdapter.getItemViewType(paramInt);
    layoutParams1.isEnabled = this.mAdapter.isEnabled(paramInt);
    if (layoutParams1 != layoutParams)
      paramView.setLayoutParams(layoutParams1); 
  }
  
  class ListItemAccessibilityDelegate extends View.AccessibilityDelegate {
    final AbsListView this$0;
    
    public void onInitializeAccessibilityNodeInfo(View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      super.onInitializeAccessibilityNodeInfo(param1View, param1AccessibilityNodeInfo);
      int i = AbsListView.this.getPositionForView(param1View);
      AbsListView.this.onInitializeAccessibilityNodeInfoForItem(param1View, i, param1AccessibilityNodeInfo);
    }
    
    public boolean performAccessibilityAction(View param1View, int param1Int, Bundle param1Bundle) {
      boolean bool;
      if (super.performAccessibilityAction(param1View, param1Int, param1Bundle))
        return true; 
      int i = AbsListView.this.getPositionForView(param1View);
      if (i == -1 || AbsListView.this.mAdapter == null)
        return false; 
      if (i >= AbsListView.this.mAdapter.getCount())
        return false; 
      ViewGroup.LayoutParams layoutParams = param1View.getLayoutParams();
      if (layoutParams instanceof AbsListView.LayoutParams) {
        bool = ((AbsListView.LayoutParams)layoutParams).isEnabled;
      } else {
        bool = false;
      } 
      if (!AbsListView.this.isEnabled() || !bool)
        return false; 
      if (param1Int != 4) {
        if (param1Int != 8) {
          if (param1Int != 16) {
            if (param1Int != 32)
              return false; 
            if (AbsListView.this.isLongClickable()) {
              long l = AbsListView.this.getItemIdAtPosition(i);
              return AbsListView.this.performLongPress(param1View, i, l);
            } 
            return false;
          } 
          if (AbsListView.this.isItemClickable(param1View)) {
            long l = AbsListView.this.getItemIdAtPosition(i);
            return AbsListView.this.performItemClick(param1View, i, l);
          } 
          return false;
        } 
        if (AbsListView.this.getSelectedItemPosition() == i) {
          AbsListView.this.setSelection(-1);
          return true;
        } 
        return false;
      } 
      if (AbsListView.this.getSelectedItemPosition() != i) {
        AbsListView.this.setSelection(i);
        return true;
      } 
      return false;
    }
  }
  
  public void onInitializeAccessibilityNodeInfoForItem(View paramView, int paramInt, AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    boolean bool1;
    if (paramInt == -1)
      return; 
    if (paramView.isEnabled() && isEnabled()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    boolean bool2 = bool1;
    if (layoutParams instanceof LayoutParams)
      bool2 = bool1 & ((LayoutParams)layoutParams).isEnabled; 
    paramAccessibilityNodeInfo.setEnabled(bool2);
    if (paramInt == getSelectedItemPosition()) {
      paramAccessibilityNodeInfo.setSelected(true);
      addAccessibilityActionIfEnabled(paramAccessibilityNodeInfo, bool2, AccessibilityNodeInfo.AccessibilityAction.ACTION_CLEAR_SELECTION);
    } else {
      addAccessibilityActionIfEnabled(paramAccessibilityNodeInfo, bool2, AccessibilityNodeInfo.AccessibilityAction.ACTION_SELECT);
    } 
    if (isItemClickable(paramView)) {
      addAccessibilityActionIfEnabled(paramAccessibilityNodeInfo, bool2, AccessibilityNodeInfo.AccessibilityAction.ACTION_CLICK);
      paramAccessibilityNodeInfo.setClickable(bool2);
    } 
    if (isLongClickable()) {
      addAccessibilityActionIfEnabled(paramAccessibilityNodeInfo, bool2, AccessibilityNodeInfo.AccessibilityAction.ACTION_LONG_CLICK);
      paramAccessibilityNodeInfo.setLongClickable(true);
    } 
  }
  
  private void addAccessibilityActionIfEnabled(AccessibilityNodeInfo paramAccessibilityNodeInfo, boolean paramBoolean, AccessibilityNodeInfo.AccessibilityAction paramAccessibilityAction) {
    if (paramBoolean)
      paramAccessibilityNodeInfo.addAction(paramAccessibilityAction); 
  }
  
  private boolean isItemClickable(View paramView) {
    return paramView.hasExplicitFocusable() ^ true;
  }
  
  void positionSelectorLikeTouch(int paramInt, View paramView, float paramFloat1, float paramFloat2) {
    positionSelector(paramInt, paramView, true, paramFloat1, paramFloat2);
  }
  
  void positionSelectorLikeFocus(int paramInt, View paramView) {
    if (this.mSelector != null && this.mSelectorPosition != paramInt && paramInt != -1) {
      Rect rect = this.mSelectorRect;
      float f1 = rect.exactCenterX();
      float f2 = rect.exactCenterY();
      positionSelector(paramInt, paramView, true, f1, f2);
    } else {
      positionSelector(paramInt, paramView);
    } 
  }
  
  void positionSelector(int paramInt, View paramView) {
    positionSelector(paramInt, paramView, false, -1.0F, -1.0F);
  }
  
  private void positionSelector(int paramInt, View paramView, boolean paramBoolean, float paramFloat1, float paramFloat2) {
    boolean bool;
    if (paramInt != this.mSelectorPosition) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramInt != -1)
      this.mSelectorPosition = paramInt; 
    Rect rect = this.mSelectorRect;
    rect.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
    if (paramView instanceof SelectionBoundsAdjuster)
      ((SelectionBoundsAdjuster)paramView).adjustListItemSelectionBounds(rect); 
    rect.left -= this.mSelectionLeftPadding;
    rect.top -= this.mSelectionTopPadding;
    rect.right += this.mSelectionRightPadding;
    rect.bottom += this.mSelectionBottomPadding;
    boolean bool1 = paramView.isEnabled();
    if (this.mIsChildViewEnabled != bool1)
      this.mIsChildViewEnabled = bool1; 
    Drawable drawable = this.mSelector;
    if (drawable != null) {
      if (bool) {
        drawable.setVisible(false, false);
        drawable.setState(StateSet.NOTHING);
      } 
      drawable.setBounds(rect);
      if (bool) {
        if (getVisibility() == 0)
          drawable.setVisible(true, false); 
        updateSelectorState();
      } 
      if (paramBoolean)
        drawable.setHotspot(paramFloat1, paramFloat2); 
    } 
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    boolean bool;
    int i = 0;
    if ((this.mGroupFlags & 0x22) == 34) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      i = paramCanvas.save();
      int j = this.mScrollX;
      int k = this.mScrollY;
      paramCanvas.clipRect(this.mPaddingLeft + j, this.mPaddingTop + k, this.mRight + j - this.mLeft - this.mPaddingRight, this.mBottom + k - this.mTop - this.mPaddingBottom);
      this.mGroupFlags &= 0xFFFFFFDD;
    } 
    boolean bool1 = this.mDrawSelectorOnTop;
    if (!bool1)
      drawSelector(paramCanvas); 
    super.dispatchDraw(paramCanvas);
    if (bool1)
      drawSelector(paramCanvas); 
    if (bool) {
      paramCanvas.restoreToCount(i);
      this.mGroupFlags = 0x22 | this.mGroupFlags;
    } 
  }
  
  protected boolean isPaddingOffsetRequired() {
    boolean bool;
    if ((this.mGroupFlags & 0x22) != 34) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected int getLeftPaddingOffset() {
    int i;
    if ((this.mGroupFlags & 0x22) == 34) {
      i = 0;
    } else {
      i = -this.mPaddingLeft;
    } 
    return i;
  }
  
  protected int getTopPaddingOffset() {
    int i;
    if ((this.mGroupFlags & 0x22) == 34) {
      i = 0;
    } else {
      i = -this.mPaddingTop;
    } 
    return i;
  }
  
  protected int getRightPaddingOffset() {
    int i;
    if ((this.mGroupFlags & 0x22) == 34) {
      i = 0;
    } else {
      i = this.mPaddingRight;
    } 
    return i;
  }
  
  protected int getBottomPaddingOffset() {
    int i;
    if ((this.mGroupFlags & 0x22) == 34) {
      i = 0;
    } else {
      i = this.mPaddingBottom;
    } 
    return i;
  }
  
  protected void internalSetPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.internalSetPadding(paramInt1, paramInt2, paramInt3, paramInt4);
    if (isLayoutRequested())
      handleBoundsChange(); 
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    handleBoundsChange();
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null)
      fastScroller.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4); 
  }
  
  void handleBoundsChange() {
    if (this.mInLayout)
      return; 
    int i = getChildCount();
    if (i > 0) {
      this.mDataChanged = true;
      rememberSyncState();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null || layoutParams.width < 1 || layoutParams.height < 1)
          view.forceLayout(); 
      } 
    } 
  }
  
  boolean touchModeDrawsInPressedState() {
    int i = this.mTouchMode;
    if (i != 1 && i != 2)
      return false; 
    return true;
  }
  
  boolean shouldShowSelector() {
    boolean bool;
    if ((isFocused() && !isInTouchMode()) || (touchModeDrawsInPressedState() && isPressed())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void drawSelector(Canvas paramCanvas) {
    if (shouldDrawSelector()) {
      Drawable drawable = this.mSelector;
      drawable.setBounds(this.mSelectorRect);
      drawable.draw(paramCanvas);
    } 
  }
  
  public final boolean shouldDrawSelector() {
    return this.mSelectorRect.isEmpty() ^ true;
  }
  
  public void setDrawSelectorOnTop(boolean paramBoolean) {
    this.mDrawSelectorOnTop = paramBoolean;
  }
  
  public boolean isDrawSelectorOnTop() {
    return this.mDrawSelectorOnTop;
  }
  
  public void setSelector(int paramInt) {
    setSelector(getContext().getDrawable(paramInt));
  }
  
  public void setSelector(Drawable paramDrawable) {
    Drawable drawable = this.mSelector;
    if (drawable != null) {
      drawable.setCallback(null);
      unscheduleDrawable(this.mSelector);
    } 
    this.mSelector = paramDrawable;
    Rect rect = new Rect();
    paramDrawable.getPadding(rect);
    this.mSelectionLeftPadding = rect.left;
    this.mSelectionTopPadding = rect.top;
    this.mSelectionRightPadding = rect.right;
    this.mSelectionBottomPadding = rect.bottom;
    paramDrawable.setCallback(this);
    updateSelectorState();
  }
  
  public Drawable getSelector() {
    return this.mSelector;
  }
  
  void keyPressed() {
    if (!isEnabled() || !isClickable())
      return; 
    Drawable drawable = this.mSelector;
    Rect rect = this.mSelectorRect;
    if (drawable != null && (isFocused() || touchModeDrawsInPressedState()) && !rect.isEmpty()) {
      View view = getChildAt(this.mSelectedPosition - this.mFirstPosition);
      if (view != null) {
        if (view.hasExplicitFocusable())
          return; 
        view.setPressed(true);
      } 
      setPressed(true);
      boolean bool = isLongClickable();
      drawable = drawable.getCurrent();
      if (drawable != null && drawable instanceof TransitionDrawable) {
        TransitionDrawable transitionDrawable;
        if (bool) {
          transitionDrawable = (TransitionDrawable)drawable;
          int i = ViewConfiguration.getLongPressTimeout();
          transitionDrawable.startTransition(i);
        } else {
          transitionDrawable.resetTransition();
        } 
      } 
      if (bool && !this.mDataChanged) {
        if (this.mPendingCheckForKeyLongPress == null)
          this.mPendingCheckForKeyLongPress = new CheckForKeyLongPress(); 
        this.mPendingCheckForKeyLongPress.rememberWindowAttachCount();
        postDelayed(this.mPendingCheckForKeyLongPress, ViewConfiguration.getLongPressTimeout());
      } 
    } 
  }
  
  public void setScrollIndicators(View paramView1, View paramView2) {
    this.mScrollUp = paramView1;
    this.mScrollDown = paramView2;
  }
  
  void updateSelectorState() {
    Drawable drawable = this.mSelector;
    if (drawable != null && drawable.isStateful())
      if (shouldShowSelector()) {
        if (drawable.setState(getDrawableStateForSelector()))
          invalidateDrawable(drawable); 
      } else {
        drawable.setState(StateSet.NOTHING);
      }  
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    updateSelectorState();
  }
  
  private int[] getDrawableStateForSelector() {
    int k;
    if (this.mIsChildViewEnabled)
      return getDrawableState(); 
    int i = ENABLED_STATE_SET[0];
    int[] arrayOfInt = onCreateDrawableState(1);
    byte b = -1;
    int j = arrayOfInt.length - 1;
    while (true) {
      k = b;
      if (j >= 0) {
        if (arrayOfInt[j] == i) {
          k = j;
          break;
        } 
        j--;
        continue;
      } 
      break;
    } 
    if (k >= 0)
      System.arraycopy(arrayOfInt, k + 1, arrayOfInt, k, arrayOfInt.length - k - 1); 
    return arrayOfInt;
  }
  
  public boolean verifyDrawable(Drawable paramDrawable) {
    return (this.mSelector == paramDrawable || super.verifyDrawable(paramDrawable));
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mSelector;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    ViewTreeObserver viewTreeObserver = getViewTreeObserver();
    viewTreeObserver.addOnTouchModeChangeListener(this);
    if (this.mTextFilterEnabled && this.mPopup != null && !this.mGlobalLayoutListenerAddedFilter)
      viewTreeObserver.addOnGlobalLayoutListener(this); 
    if (this.mAdapter != null && this.mDataSetObserver == null) {
      AdapterDataSetObserver adapterDataSetObserver = new AdapterDataSetObserver();
      this.mAdapter.registerDataSetObserver(adapterDataSetObserver);
      this.mDataChanged = true;
      this.mOldItemCount = this.mItemCount;
      this.mItemCount = this.mAdapter.getCount();
    } 
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    this.mIsDetaching = true;
    dismissPopup();
    this.mRecycler.clear();
    ViewTreeObserver viewTreeObserver = getViewTreeObserver();
    viewTreeObserver.removeOnTouchModeChangeListener(this);
    if (this.mTextFilterEnabled && this.mPopup != null) {
      viewTreeObserver.removeOnGlobalLayoutListener(this);
      this.mGlobalLayoutListenerAddedFilter = false;
    } 
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null) {
      AdapterDataSetObserver adapterDataSetObserver = this.mDataSetObserver;
      if (adapterDataSetObserver != null) {
        listAdapter.unregisterDataSetObserver(adapterDataSetObserver);
        this.mDataSetObserver = null;
      } 
    } 
    StrictMode.Span span = this.mScrollStrictSpan;
    if (span != null) {
      span.finish();
      this.mScrollStrictSpan = null;
    } 
    span = this.mFlingStrictSpan;
    if (span != null) {
      span.finish();
      this.mFlingStrictSpan = null;
    } 
    FlingRunnable flingRunnable = this.mFlingRunnable;
    if (flingRunnable != null)
      removeCallbacks(flingRunnable); 
    AbsPositionScroller absPositionScroller = this.mPositionScroller;
    if (absPositionScroller != null)
      absPositionScroller.stop(); 
    Runnable runnable = this.mClearScrollingCache;
    if (runnable != null)
      removeCallbacks(runnable); 
    runnable = this.mPerformClick;
    if (runnable != null)
      removeCallbacks(runnable); 
    runnable = this.mTouchModeReset;
    if (runnable != null) {
      removeCallbacks(runnable);
      this.mTouchModeReset.run();
    } 
    this.mIsDetaching = false;
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    super.onWindowFocusChanged(paramBoolean);
    int i = isInTouchMode() ^ true;
    if (!paramBoolean) {
      setChildrenDrawingCacheEnabled(false);
      FlingRunnable flingRunnable = this.mFlingRunnable;
      if (flingRunnable != null) {
        removeCallbacks(flingRunnable);
        FlingRunnable.access$502(this.mFlingRunnable, false);
        this.mFlingRunnable.endFling();
        AbsPositionScroller absPositionScroller = this.mPositionScroller;
        if (absPositionScroller != null)
          absPositionScroller.stop(); 
        if (this.mScrollY != 0) {
          this.mScrollY = 0;
          invalidateParentCaches();
          finishGlows();
          invalidate();
        } 
      } 
      dismissPopup();
      if (i == 1)
        this.mResurrectToPosition = this.mSelectedPosition; 
    } else {
      if (this.mFiltered && !this.mPopupHidden)
        showPopup(); 
      int j = this.mLastTouchMode;
      if (i != j && j != -1)
        if (i == 1) {
          resurrectSelection();
        } else {
          hideSelector();
          this.mLayoutMode = 0;
          layoutChildren();
        }  
    } 
    this.mLastTouchMode = i;
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null)
      fastScroller.setScrollbarPosition(getVerticalScrollbarPosition()); 
  }
  
  ContextMenu.ContextMenuInfo createContextMenuInfo(View paramView, int paramInt, long paramLong) {
    return new AdapterView.AdapterContextMenuInfo(paramView, paramInt, paramLong);
  }
  
  public void onCancelPendingInputEvents() {
    super.onCancelPendingInputEvents();
    PerformClick performClick = this.mPerformClick;
    if (performClick != null)
      removeCallbacks(performClick); 
    CheckForTap checkForTap = this.mPendingCheckForTap;
    if (checkForTap != null)
      removeCallbacks(checkForTap); 
    CheckForLongPress checkForLongPress = this.mPendingCheckForLongPress;
    if (checkForLongPress != null)
      removeCallbacks(checkForLongPress); 
    CheckForKeyLongPress checkForKeyLongPress = this.mPendingCheckForKeyLongPress;
    if (checkForKeyLongPress != null)
      removeCallbacks(checkForKeyLongPress); 
  }
  
  class WindowRunnnable {
    private int mOriginalAttachCount;
    
    final AbsListView this$0;
    
    private WindowRunnnable() {}
    
    public void rememberWindowAttachCount() {
      this.mOriginalAttachCount = AbsListView.this.getWindowAttachCount();
    }
    
    public boolean sameWindow() {
      boolean bool;
      if (AbsListView.this.getWindowAttachCount() == this.mOriginalAttachCount) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  class PerformClick extends WindowRunnnable implements Runnable {
    int mClickMotionPosition;
    
    final AbsListView this$0;
    
    private PerformClick() {}
    
    public void run() {
      if (AbsListView.this.mDataChanged)
        return; 
      ListAdapter listAdapter = AbsListView.this.mAdapter;
      int i = this.mClickMotionPosition;
      if (listAdapter != null && AbsListView.this.mItemCount > 0 && i != -1)
        if (i < listAdapter.getCount() && sameWindow() && listAdapter.isEnabled(i)) {
          AbsListView absListView = AbsListView.this;
          View view = absListView.getChildAt(i - absListView.mFirstPosition);
          if (view != null)
            AbsListView.this.performItemClick(view, i, listAdapter.getItemId(i)); 
        }  
    }
  }
  
  class CheckForLongPress extends WindowRunnnable implements Runnable {
    private static final int INVALID_COORD = -1;
    
    private float mX;
    
    private float mY;
    
    final AbsListView this$0;
    
    private CheckForLongPress() {
      this.mX = -1.0F;
      this.mY = -1.0F;
    }
    
    private void setCoords(float param1Float1, float param1Float2) {
      this.mX = param1Float1;
      this.mY = param1Float2;
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/widget/AbsListView;
      //   4: getfield mMotionPosition : I
      //   7: istore_1
      //   8: aload_0
      //   9: getfield this$0 : Landroid/widget/AbsListView;
      //   12: astore_2
      //   13: aload_2
      //   14: iload_1
      //   15: aload_2
      //   16: getfield mFirstPosition : I
      //   19: isub
      //   20: invokevirtual getChildAt : (I)Landroid/view/View;
      //   23: astore_2
      //   24: aload_2
      //   25: ifnull -> 189
      //   28: aload_0
      //   29: getfield this$0 : Landroid/widget/AbsListView;
      //   32: getfield mMotionPosition : I
      //   35: istore_1
      //   36: aload_0
      //   37: getfield this$0 : Landroid/widget/AbsListView;
      //   40: getfield mAdapter : Landroid/widget/ListAdapter;
      //   43: aload_0
      //   44: getfield this$0 : Landroid/widget/AbsListView;
      //   47: getfield mMotionPosition : I
      //   50: invokeinterface getItemId : (I)J
      //   55: lstore_3
      //   56: iconst_0
      //   57: istore #5
      //   59: iload #5
      //   61: istore #6
      //   63: aload_0
      //   64: invokevirtual sameWindow : ()Z
      //   67: ifeq -> 143
      //   70: iload #5
      //   72: istore #6
      //   74: aload_0
      //   75: getfield this$0 : Landroid/widget/AbsListView;
      //   78: getfield mDataChanged : Z
      //   81: ifne -> 143
      //   84: aload_0
      //   85: getfield mX : F
      //   88: fstore #7
      //   90: fload #7
      //   92: ldc -1.0
      //   94: fcmpl
      //   95: ifeq -> 131
      //   98: aload_0
      //   99: getfield mY : F
      //   102: fstore #8
      //   104: fload #8
      //   106: ldc -1.0
      //   108: fcmpl
      //   109: ifeq -> 131
      //   112: aload_0
      //   113: getfield this$0 : Landroid/widget/AbsListView;
      //   116: aload_2
      //   117: iload_1
      //   118: lload_3
      //   119: fload #7
      //   121: fload #8
      //   123: invokevirtual performLongPress : (Landroid/view/View;IJFF)Z
      //   126: istore #6
      //   128: goto -> 143
      //   131: aload_0
      //   132: getfield this$0 : Landroid/widget/AbsListView;
      //   135: aload_2
      //   136: iload_1
      //   137: lload_3
      //   138: invokevirtual performLongPress : (Landroid/view/View;IJ)Z
      //   141: istore #6
      //   143: iload #6
      //   145: ifeq -> 181
      //   148: aload_0
      //   149: getfield this$0 : Landroid/widget/AbsListView;
      //   152: iconst_1
      //   153: invokestatic access$902 : (Landroid/widget/AbsListView;Z)Z
      //   156: pop
      //   157: aload_0
      //   158: getfield this$0 : Landroid/widget/AbsListView;
      //   161: iconst_m1
      //   162: putfield mTouchMode : I
      //   165: aload_0
      //   166: getfield this$0 : Landroid/widget/AbsListView;
      //   169: iconst_0
      //   170: invokevirtual setPressed : (Z)V
      //   173: aload_2
      //   174: iconst_0
      //   175: invokevirtual setPressed : (Z)V
      //   178: goto -> 189
      //   181: aload_0
      //   182: getfield this$0 : Landroid/widget/AbsListView;
      //   185: iconst_2
      //   186: putfield mTouchMode : I
      //   189: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3245	-> 0
      //   #3246	-> 8
      //   #3247	-> 24
      //   #3248	-> 28
      //   #3249	-> 36
      //   #3251	-> 56
      //   #3252	-> 59
      //   #3253	-> 84
      //   #3254	-> 112
      //   #3256	-> 131
      //   #3260	-> 143
      //   #3261	-> 148
      //   #3262	-> 157
      //   #3263	-> 165
      //   #3264	-> 173
      //   #3266	-> 181
      //   #3269	-> 189
    }
  }
  
  class CheckForKeyLongPress extends WindowRunnnable implements Runnable {
    final AbsListView this$0;
    
    private CheckForKeyLongPress() {}
    
    public void run() {
      if (AbsListView.this.isPressed() && AbsListView.this.mSelectedPosition >= 0) {
        int i = AbsListView.this.mSelectedPosition, j = AbsListView.this.mFirstPosition;
        View view = AbsListView.this.getChildAt(i - j);
        if (!AbsListView.this.mDataChanged) {
          boolean bool = false;
          if (sameWindow()) {
            AbsListView absListView = AbsListView.this;
            bool = absListView.performLongPress(view, absListView.mSelectedPosition, AbsListView.this.mSelectedRowId);
          } 
          if (bool) {
            AbsListView.this.setPressed(false);
            view.setPressed(false);
          } 
        } else {
          AbsListView.this.setPressed(false);
          if (view != null)
            view.setPressed(false); 
        } 
      } 
    }
  }
  
  private boolean performStylusButtonPressAction(MotionEvent paramMotionEvent) {
    if (this.mChoiceMode == 3 && this.mChoiceActionMode == null) {
      View view = getChildAt(this.mMotionPosition - this.mFirstPosition);
      if (view != null) {
        int i = this.mMotionPosition;
        long l = this.mAdapter.getItemId(this.mMotionPosition);
        if (performLongPress(view, i, l)) {
          this.mTouchMode = -1;
          setPressed(false);
          view.setPressed(false);
          return true;
        } 
      } 
    } 
    return false;
  }
  
  boolean performLongPress(View paramView, int paramInt, long paramLong) {
    return performLongPress(paramView, paramInt, paramLong, -1.0F, -1.0F);
  }
  
  boolean performLongPress(View paramView, int paramInt, long paramLong, float paramFloat1, float paramFloat2) {
    ActionMode actionMode;
    if (this.mChoiceMode == 3) {
      if (this.mChoiceActionMode == null) {
        MultiChoiceModeWrapper multiChoiceModeWrapper = this.mMultiChoiceModeCallback;
        this.mChoiceActionMode = actionMode = startActionMode(multiChoiceModeWrapper);
        if (actionMode != null) {
          setItemChecked(paramInt, true);
          performHapticFeedback(0);
        } 
      } 
      return true;
    } 
    boolean bool1 = false;
    if (this.mOnItemLongClickListener != null)
      bool1 = this.mOnItemLongClickListener.onItemLongClick(this, (View)actionMode, paramInt, paramLong); 
    boolean bool2 = bool1;
    if (!bool1) {
      this.mContextMenuInfo = createContextMenuInfo((View)actionMode, paramInt, paramLong);
      if (paramFloat1 != -1.0F && paramFloat2 != -1.0F) {
        bool2 = super.showContextMenuForChild(this, paramFloat1, paramFloat2);
      } else {
        bool2 = super.showContextMenuForChild(this);
      } 
    } 
    if (bool2)
      performHapticFeedback(0); 
    return bool2;
  }
  
  protected ContextMenu.ContextMenuInfo getContextMenuInfo() {
    return this.mContextMenuInfo;
  }
  
  public boolean showContextMenu() {
    return showContextMenuInternal(0.0F, 0.0F, false);
  }
  
  public boolean showContextMenu(float paramFloat1, float paramFloat2) {
    return showContextMenuInternal(paramFloat1, paramFloat2, true);
  }
  
  private boolean showContextMenuInternal(float paramFloat1, float paramFloat2, boolean paramBoolean) {
    int i = pointToPosition((int)paramFloat1, (int)paramFloat2);
    if (i != -1) {
      long l = this.mAdapter.getItemId(i);
      View view = getChildAt(i - this.mFirstPosition);
      if (view != null) {
        this.mContextMenuInfo = createContextMenuInfo(view, i, l);
        if (paramBoolean)
          return super.showContextMenuForChild(this, paramFloat1, paramFloat2); 
        return super.showContextMenuForChild(this);
      } 
    } 
    if (paramBoolean)
      return super.showContextMenu(paramFloat1, paramFloat2); 
    return super.showContextMenu();
  }
  
  public boolean showContextMenuForChild(View paramView) {
    if (isShowingContextMenuWithCoords())
      return false; 
    return showContextMenuForChildInternal(paramView, 0.0F, 0.0F, false);
  }
  
  public boolean showContextMenuForChild(View paramView, float paramFloat1, float paramFloat2) {
    return showContextMenuForChildInternal(paramView, paramFloat1, paramFloat2, true);
  }
  
  private boolean showContextMenuForChildInternal(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    int i = getPositionForView(paramView);
    if (i < 0)
      return false; 
    long l = this.mAdapter.getItemId(i);
    boolean bool1 = false;
    if (this.mOnItemLongClickListener != null)
      bool1 = this.mOnItemLongClickListener.onItemLongClick(this, paramView, i, l); 
    boolean bool2 = bool1;
    if (!bool1) {
      View view = getChildAt(i - this.mFirstPosition);
      this.mContextMenuInfo = createContextMenuInfo(view, i, l);
      if (paramBoolean) {
        bool2 = super.showContextMenuForChild(paramView, paramFloat1, paramFloat2);
      } else {
        bool2 = super.showContextMenuForChild(paramView);
      } 
    } 
    return bool2;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    View view;
    if (KeyEvent.isConfirmKey(paramInt)) {
      if (!isEnabled())
        return true; 
      if (isClickable() && isPressed() && this.mSelectedPosition >= 0 && this.mAdapter != null) {
        int i = this.mSelectedPosition;
        ListAdapter listAdapter = this.mAdapter;
        if (i < listAdapter.getCount()) {
          view = getChildAt(this.mSelectedPosition - this.mFirstPosition);
          if (view != null) {
            performItemClick(view, this.mSelectedPosition, this.mSelectedRowId);
            view.setPressed(false);
          } 
          setPressed(false);
          return true;
        } 
      } 
    } 
    return super.onKeyUp(paramInt, (KeyEvent)view);
  }
  
  protected void dispatchSetPressed(boolean paramBoolean) {}
  
  public void dispatchDrawableHotspotChanged(float paramFloat1, float paramFloat2) {}
  
  public int pointToPosition(int paramInt1, int paramInt2) {
    Rect rect1 = this.mTouchFrame;
    Rect rect2 = rect1;
    if (rect1 == null) {
      this.mTouchFrame = new Rect();
      rect2 = this.mTouchFrame;
    } 
    int i = getChildCount();
    for (; --i >= 0; i--) {
      View view = getChildAt(i);
      if (view.getVisibility() == 0) {
        view.getHitRect(rect2);
        if (rect2.contains(paramInt1, paramInt2))
          return this.mFirstPosition + i; 
      } 
    } 
    return -1;
  }
  
  public long pointToRowId(int paramInt1, int paramInt2) {
    paramInt1 = pointToPosition(paramInt1, paramInt2);
    if (paramInt1 >= 0)
      return this.mAdapter.getItemId(paramInt1); 
    return Long.MIN_VALUE;
  }
  
  class CheckForTap implements Runnable {
    final AbsListView this$0;
    
    float x;
    
    float y;
    
    private CheckForTap() {}
    
    public void run() {
      if (AbsListView.this.mTouchMode == 0) {
        AbsListView.this.mTouchMode = 1;
        AbsListView absListView = AbsListView.this;
        View view = absListView.getChildAt(absListView.mMotionPosition - AbsListView.this.mFirstPosition);
        if (view != null && !view.hasExplicitFocusable()) {
          AbsListView.this.mLayoutMode = 0;
          if (!AbsListView.this.mDataChanged) {
            float[] arrayOfFloat = AbsListView.this.mTmpPoint;
            arrayOfFloat[0] = this.x;
            arrayOfFloat[1] = this.y;
            AbsListView.this.transformPointToViewLocal(arrayOfFloat, view);
            view.drawableHotspotChanged(arrayOfFloat[0], arrayOfFloat[1]);
            view.setPressed(true);
            AbsListView.this.setPressed(true);
            AbsListView.this.layoutChildren();
            AbsListView absListView1 = AbsListView.this;
            absListView1.positionSelector(absListView1.mMotionPosition, view);
            AbsListView.this.refreshDrawableState();
            int i = ViewConfiguration.getLongPressTimeout();
            boolean bool = AbsListView.this.isLongClickable();
            if (AbsListView.this.mSelector != null) {
              Drawable drawable = AbsListView.this.mSelector.getCurrent();
              if (drawable != null && drawable instanceof TransitionDrawable)
                if (bool) {
                  ((TransitionDrawable)drawable).startTransition(i);
                } else {
                  ((TransitionDrawable)drawable).resetTransition();
                }  
              AbsListView.this.mSelector.setHotspot(this.x, this.y);
            } 
            if (bool) {
              if (AbsListView.this.mPendingCheckForLongPress == null) {
                view = AbsListView.this;
                AbsListView.access$1102((AbsListView)view, new AbsListView.CheckForLongPress());
              } 
              AbsListView.this.mPendingCheckForLongPress.setCoords(this.x, this.y);
              AbsListView.this.mPendingCheckForLongPress.rememberWindowAttachCount();
              view = AbsListView.this;
              view.postDelayed(((AbsListView)view).mPendingCheckForLongPress, i);
            } else {
              AbsListView.this.mTouchMode = 2;
            } 
          } else {
            AbsListView.this.mTouchMode = 2;
          } 
        } 
      } 
    }
  }
  
  private boolean startScrollIfNeeded(int paramInt1, int paramInt2, MotionEvent paramMotionEvent) {
    boolean bool;
    int i = paramInt2 - this.mMotionY;
    int j = Math.abs(i);
    if (this.mScrollY != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mIsFirstTouchMoveEvent) {
      if (j > this.mMoveAcceleration) {
        j = 1;
      } else {
        j = 0;
      } 
    } else if (j > this.mTouchSlop) {
      j = 1;
    } else {
      j = 0;
    } 
    if ((bool || j != 0) && (getNestedScrollAxes() & 0x2) == 0) {
      createScrollingCache();
      if (bool) {
        this.mTouchMode = 5;
        this.mMotionCorrection = 0;
      } else {
        this.mTouchMode = 3;
        if (this.mIsFirstTouchMoveEvent) {
          j = this.mMoveAcceleration;
          if (i <= 0)
            j = -j; 
          this.mMotionCorrection = j;
        } else {
          j = this.mTouchSlop;
          if (i <= 0)
            j = -j; 
          this.mMotionCorrection = j;
        } 
      } 
      removeCallbacks(this.mPendingCheckForLongPress);
      setPressed(false);
      View view = getChildAt(this.mMotionPosition - this.mFirstPosition);
      if (view != null)
        view.setPressed(false); 
      reportScrollStateChange(1);
      ViewParent viewParent = getParent();
      if (viewParent != null)
        viewParent.requestDisallowInterceptTouchEvent(true); 
      scrollIfNeeded(paramInt1, paramInt2, paramMotionEvent);
      return true;
    } 
    return false;
  }
  
  private void scrollIfNeeded(int paramInt1, int paramInt2, MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: iload_2
    //   1: aload_0
    //   2: getfield mMotionY : I
    //   5: isub
    //   6: istore #4
    //   8: iload #4
    //   10: istore #5
    //   12: aload_0
    //   13: getfield mLastY : I
    //   16: ldc_w -2147483648
    //   19: if_icmpne -> 31
    //   22: iload #4
    //   24: aload_0
    //   25: getfield mMotionCorrection : I
    //   28: isub
    //   29: istore #5
    //   31: aload_0
    //   32: getfield mLastY : I
    //   35: istore #4
    //   37: iload #4
    //   39: ldc_w -2147483648
    //   42: if_icmpeq -> 54
    //   45: iload #4
    //   47: iload_2
    //   48: isub
    //   49: istore #4
    //   51: goto -> 59
    //   54: iload #5
    //   56: ineg
    //   57: istore #4
    //   59: aload_0
    //   60: iconst_0
    //   61: iload #4
    //   63: aload_0
    //   64: getfield mScrollConsumed : [I
    //   67: aload_0
    //   68: getfield mScrollOffset : [I
    //   71: invokevirtual dispatchNestedPreScroll : (II[I[I)Z
    //   74: ifeq -> 147
    //   77: aload_0
    //   78: getfield mScrollConsumed : [I
    //   81: astore #6
    //   83: aload #6
    //   85: iconst_1
    //   86: iaload
    //   87: istore #7
    //   89: aload_0
    //   90: getfield mScrollOffset : [I
    //   93: astore #8
    //   95: aload #8
    //   97: iconst_1
    //   98: iaload
    //   99: ineg
    //   100: istore #4
    //   102: aload #6
    //   104: iconst_1
    //   105: iaload
    //   106: istore #9
    //   108: aload_3
    //   109: ifnull -> 137
    //   112: aload_3
    //   113: fconst_0
    //   114: aload #8
    //   116: iconst_1
    //   117: iaload
    //   118: i2f
    //   119: invokevirtual offsetLocation : (FF)V
    //   122: aload_0
    //   123: aload_0
    //   124: getfield mNestedYOffset : I
    //   127: aload_0
    //   128: getfield mScrollOffset : [I
    //   131: iconst_1
    //   132: iaload
    //   133: iadd
    //   134: putfield mNestedYOffset : I
    //   137: iload #5
    //   139: iload #7
    //   141: iadd
    //   142: istore #5
    //   144: goto -> 153
    //   147: iconst_0
    //   148: istore #4
    //   150: iconst_0
    //   151: istore #9
    //   153: aload_0
    //   154: getfield mLastY : I
    //   157: istore #7
    //   159: iload #7
    //   161: ldc_w -2147483648
    //   164: if_icmpeq -> 179
    //   167: iload_2
    //   168: iload #7
    //   170: isub
    //   171: iload #9
    //   173: iadd
    //   174: istore #9
    //   176: goto -> 183
    //   179: iload #5
    //   181: istore #9
    //   183: iconst_0
    //   184: istore #10
    //   186: iconst_0
    //   187: istore #11
    //   189: aload_0
    //   190: getfield mTouchMode : I
    //   193: istore #7
    //   195: iload #7
    //   197: iconst_3
    //   198: if_icmpne -> 732
    //   201: aload_0
    //   202: getfield mScrollStrictSpan : Landroid/os/StrictMode$Span;
    //   205: ifnonnull -> 218
    //   208: aload_0
    //   209: ldc_w 'AbsListView-scroll'
    //   212: invokestatic enterCriticalSpan : (Ljava/lang/String;)Landroid/os/StrictMode$Span;
    //   215: putfield mScrollStrictSpan : Landroid/os/StrictMode$Span;
    //   218: iload_2
    //   219: aload_0
    //   220: getfield mLastY : I
    //   223: if_icmpeq -> 729
    //   226: aload_0
    //   227: getfield mGroupFlags : I
    //   230: ldc_w 524288
    //   233: iand
    //   234: ifne -> 268
    //   237: iload #5
    //   239: invokestatic abs : (I)I
    //   242: aload_0
    //   243: getfield mTouchSlop : I
    //   246: if_icmple -> 268
    //   249: aload_0
    //   250: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   253: astore #8
    //   255: aload #8
    //   257: ifnull -> 268
    //   260: aload #8
    //   262: iconst_1
    //   263: invokeinterface requestDisallowInterceptTouchEvent : (Z)V
    //   268: aload_0
    //   269: getfield mMotionPosition : I
    //   272: istore #7
    //   274: iload #7
    //   276: iflt -> 291
    //   279: iload #7
    //   281: aload_0
    //   282: getfield mFirstPosition : I
    //   285: isub
    //   286: istore #7
    //   288: goto -> 299
    //   291: aload_0
    //   292: invokevirtual getChildCount : ()I
    //   295: iconst_2
    //   296: idiv
    //   297: istore #7
    //   299: aload_0
    //   300: iload #7
    //   302: invokevirtual getChildAt : (I)Landroid/view/View;
    //   305: astore #8
    //   307: aload #8
    //   309: ifnull -> 322
    //   312: aload #8
    //   314: invokevirtual getTop : ()I
    //   317: istore #12
    //   319: goto -> 325
    //   322: iconst_0
    //   323: istore #12
    //   325: iload #9
    //   327: ifeq -> 343
    //   330: aload_0
    //   331: iload #5
    //   333: iload #9
    //   335: invokevirtual trackMotionScroll : (II)Z
    //   338: istore #13
    //   340: goto -> 346
    //   343: iconst_0
    //   344: istore #13
    //   346: aload_0
    //   347: iload #7
    //   349: invokevirtual getChildAt : (I)Landroid/view/View;
    //   352: astore #8
    //   354: aload #8
    //   356: ifnull -> 711
    //   359: aload #8
    //   361: invokevirtual getTop : ()I
    //   364: istore #5
    //   366: iload #13
    //   368: ifeq -> 693
    //   371: iload #9
    //   373: ineg
    //   374: iload #5
    //   376: iload #12
    //   378: isub
    //   379: isub
    //   380: istore #7
    //   382: aload_0
    //   383: iconst_0
    //   384: iload #7
    //   386: iload #9
    //   388: isub
    //   389: iconst_0
    //   390: iload #7
    //   392: aload_0
    //   393: getfield mScrollOffset : [I
    //   396: invokevirtual dispatchNestedScroll : (IIII[I)Z
    //   399: ifeq -> 448
    //   402: aload_0
    //   403: getfield mScrollOffset : [I
    //   406: astore #8
    //   408: iconst_0
    //   409: aload #8
    //   411: iconst_1
    //   412: iaload
    //   413: isub
    //   414: istore #5
    //   416: aload_3
    //   417: ifnull -> 445
    //   420: aload_3
    //   421: fconst_0
    //   422: aload #8
    //   424: iconst_1
    //   425: iaload
    //   426: i2f
    //   427: invokevirtual offsetLocation : (FF)V
    //   430: aload_0
    //   431: aload_0
    //   432: getfield mNestedYOffset : I
    //   435: aload_0
    //   436: getfield mScrollOffset : [I
    //   439: iconst_1
    //   440: iaload
    //   441: iadd
    //   442: putfield mNestedYOffset : I
    //   445: goto -> 697
    //   448: aload_0
    //   449: iconst_0
    //   450: iload #7
    //   452: iconst_0
    //   453: aload_0
    //   454: getfield mScrollY : I
    //   457: iconst_0
    //   458: iconst_0
    //   459: iconst_0
    //   460: aload_0
    //   461: getfield mOverscrollDistance : I
    //   464: iconst_1
    //   465: invokevirtual overScrollBy : (IIIIIIIIZ)Z
    //   468: istore #13
    //   470: iload #13
    //   472: ifeq -> 488
    //   475: aload_0
    //   476: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   479: astore_3
    //   480: aload_3
    //   481: ifnull -> 488
    //   484: aload_3
    //   485: invokevirtual clear : ()V
    //   488: aload_0
    //   489: invokevirtual getOverScrollMode : ()I
    //   492: istore #5
    //   494: iload #5
    //   496: ifeq -> 529
    //   499: iload #5
    //   501: iconst_1
    //   502: if_icmpne -> 522
    //   505: aload_0
    //   506: invokespecial contentFits : ()Z
    //   509: ifne -> 515
    //   512: goto -> 529
    //   515: iload #11
    //   517: istore #5
    //   519: goto -> 697
    //   522: iload #11
    //   524: istore #5
    //   526: goto -> 697
    //   529: iload #13
    //   531: ifne -> 544
    //   534: aload_0
    //   535: iconst_0
    //   536: putfield mDirection : I
    //   539: aload_0
    //   540: iconst_5
    //   541: putfield mTouchMode : I
    //   544: iload #9
    //   546: ifle -> 616
    //   549: aload_0
    //   550: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   553: astore_3
    //   554: iload #7
    //   556: ineg
    //   557: i2f
    //   558: aload_0
    //   559: invokevirtual getHeight : ()I
    //   562: i2f
    //   563: fdiv
    //   564: fstore #14
    //   566: iload_1
    //   567: i2f
    //   568: fstore #15
    //   570: fload #15
    //   572: aload_0
    //   573: invokevirtual getWidth : ()I
    //   576: i2f
    //   577: fdiv
    //   578: fstore #15
    //   580: aload_3
    //   581: fload #14
    //   583: fload #15
    //   585: invokevirtual onPull : (FF)V
    //   588: aload_0
    //   589: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   592: invokevirtual isFinished : ()Z
    //   595: ifne -> 605
    //   598: aload_0
    //   599: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   602: invokevirtual onRelease : ()V
    //   605: aload_0
    //   606: invokespecial invalidateTopGlow : ()V
    //   609: iload #11
    //   611: istore #5
    //   613: goto -> 697
    //   616: iload #11
    //   618: istore #5
    //   620: iload #9
    //   622: ifge -> 697
    //   625: aload_0
    //   626: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   629: astore_3
    //   630: iload #7
    //   632: i2f
    //   633: aload_0
    //   634: invokevirtual getHeight : ()I
    //   637: i2f
    //   638: fdiv
    //   639: fstore #14
    //   641: iload_1
    //   642: i2f
    //   643: fstore #15
    //   645: fload #15
    //   647: aload_0
    //   648: invokevirtual getWidth : ()I
    //   651: i2f
    //   652: fdiv
    //   653: fstore #15
    //   655: aload_3
    //   656: fload #14
    //   658: fconst_1
    //   659: fload #15
    //   661: fsub
    //   662: invokevirtual onPull : (FF)V
    //   665: aload_0
    //   666: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   669: invokevirtual isFinished : ()Z
    //   672: ifne -> 682
    //   675: aload_0
    //   676: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   679: invokevirtual onRelease : ()V
    //   682: aload_0
    //   683: invokespecial invalidateBottomGlow : ()V
    //   686: iload #11
    //   688: istore #5
    //   690: goto -> 697
    //   693: iload #11
    //   695: istore #5
    //   697: aload_0
    //   698: iload_2
    //   699: iload #5
    //   701: iadd
    //   702: iload #4
    //   704: iadd
    //   705: putfield mMotionY : I
    //   708: goto -> 715
    //   711: iload #10
    //   713: istore #5
    //   715: aload_0
    //   716: iload_2
    //   717: iload #5
    //   719: iadd
    //   720: iload #4
    //   722: iadd
    //   723: putfield mLastY : I
    //   726: goto -> 1157
    //   729: goto -> 1157
    //   732: iload #7
    //   734: iconst_5
    //   735: if_icmpne -> 1157
    //   738: iload_2
    //   739: aload_0
    //   740: getfield mLastY : I
    //   743: if_icmpeq -> 1157
    //   746: aload_0
    //   747: getfield mScrollY : I
    //   750: istore #10
    //   752: iload #10
    //   754: iload #9
    //   756: isub
    //   757: istore #11
    //   759: iload_2
    //   760: aload_0
    //   761: getfield mLastY : I
    //   764: if_icmple -> 773
    //   767: iconst_1
    //   768: istore #7
    //   770: goto -> 776
    //   773: iconst_m1
    //   774: istore #7
    //   776: aload_0
    //   777: getfield mDirection : I
    //   780: ifne -> 789
    //   783: aload_0
    //   784: iload #7
    //   786: putfield mDirection : I
    //   789: iload #9
    //   791: ineg
    //   792: istore #12
    //   794: aload_0
    //   795: getfield mColorViewConfigHelper : Landroid/view/IOplusViewConfigHelper;
    //   798: iload #9
    //   800: ineg
    //   801: aload_0
    //   802: getfield mScrollY : I
    //   805: invokeinterface calcRealOverScrollDist : (II)I
    //   810: istore #12
    //   812: iload #11
    //   814: ifge -> 822
    //   817: iload #10
    //   819: ifge -> 832
    //   822: iload #11
    //   824: ifle -> 851
    //   827: iload #10
    //   829: ifgt -> 851
    //   832: iload #10
    //   834: ineg
    //   835: istore #11
    //   837: iload #11
    //   839: istore #12
    //   841: iload #9
    //   843: iload #11
    //   845: iadd
    //   846: istore #9
    //   848: goto -> 854
    //   851: iconst_0
    //   852: istore #9
    //   854: iload #12
    //   856: ifeq -> 1049
    //   859: aload_0
    //   860: iconst_0
    //   861: iload #12
    //   863: iconst_0
    //   864: aload_0
    //   865: getfield mScrollY : I
    //   868: iconst_0
    //   869: iconst_0
    //   870: iconst_0
    //   871: aload_0
    //   872: getfield mOverscrollDistance : I
    //   875: iconst_1
    //   876: invokevirtual overScrollBy : (IIIIIIIIZ)Z
    //   879: pop
    //   880: aload_0
    //   881: invokevirtual getOverScrollMode : ()I
    //   884: istore #11
    //   886: iload #11
    //   888: ifeq -> 913
    //   891: iload #11
    //   893: iconst_1
    //   894: if_icmpne -> 910
    //   897: aload_0
    //   898: invokespecial contentFits : ()Z
    //   901: ifne -> 907
    //   904: goto -> 913
    //   907: goto -> 1049
    //   910: goto -> 1049
    //   913: iload #5
    //   915: ifle -> 980
    //   918: aload_0
    //   919: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   922: astore_3
    //   923: iload #12
    //   925: i2f
    //   926: aload_0
    //   927: invokevirtual getHeight : ()I
    //   930: i2f
    //   931: fdiv
    //   932: fstore #14
    //   934: iload_1
    //   935: i2f
    //   936: fstore #15
    //   938: fload #15
    //   940: aload_0
    //   941: invokevirtual getWidth : ()I
    //   944: i2f
    //   945: fdiv
    //   946: fstore #15
    //   948: aload_3
    //   949: fload #14
    //   951: fload #15
    //   953: invokevirtual onPull : (FF)V
    //   956: aload_0
    //   957: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   960: invokevirtual isFinished : ()Z
    //   963: ifne -> 973
    //   966: aload_0
    //   967: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   970: invokevirtual onRelease : ()V
    //   973: aload_0
    //   974: invokespecial invalidateTopGlow : ()V
    //   977: goto -> 1049
    //   980: iload #5
    //   982: ifge -> 1049
    //   985: aload_0
    //   986: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   989: astore_3
    //   990: iload #12
    //   992: i2f
    //   993: aload_0
    //   994: invokevirtual getHeight : ()I
    //   997: i2f
    //   998: fdiv
    //   999: fstore #14
    //   1001: iload_1
    //   1002: i2f
    //   1003: fstore #15
    //   1005: fload #15
    //   1007: aload_0
    //   1008: invokevirtual getWidth : ()I
    //   1011: i2f
    //   1012: fdiv
    //   1013: fstore #15
    //   1015: aload_3
    //   1016: fload #14
    //   1018: fconst_1
    //   1019: fload #15
    //   1021: fsub
    //   1022: invokevirtual onPull : (FF)V
    //   1025: aload_0
    //   1026: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   1029: invokevirtual isFinished : ()Z
    //   1032: ifne -> 1042
    //   1035: aload_0
    //   1036: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   1039: invokevirtual onRelease : ()V
    //   1042: aload_0
    //   1043: invokespecial invalidateBottomGlow : ()V
    //   1046: goto -> 1049
    //   1049: iload #9
    //   1051: ifeq -> 1138
    //   1054: aload_0
    //   1055: getfield mScrollY : I
    //   1058: ifeq -> 1070
    //   1061: aload_0
    //   1062: iconst_0
    //   1063: putfield mScrollY : I
    //   1066: aload_0
    //   1067: invokevirtual invalidateParentIfNeeded : ()V
    //   1070: aload_0
    //   1071: iload #9
    //   1073: iload #9
    //   1075: invokevirtual trackMotionScroll : (II)Z
    //   1078: pop
    //   1079: aload_0
    //   1080: iconst_3
    //   1081: putfield mTouchMode : I
    //   1084: aload_0
    //   1085: iload_2
    //   1086: invokevirtual findClosestMotionRow : (I)I
    //   1089: istore #5
    //   1091: iconst_0
    //   1092: istore_1
    //   1093: aload_0
    //   1094: iconst_0
    //   1095: putfield mMotionCorrection : I
    //   1098: aload_0
    //   1099: iload #5
    //   1101: aload_0
    //   1102: getfield mFirstPosition : I
    //   1105: isub
    //   1106: invokevirtual getChildAt : (I)Landroid/view/View;
    //   1109: astore_3
    //   1110: aload_3
    //   1111: ifnull -> 1119
    //   1114: aload_3
    //   1115: invokevirtual getTop : ()I
    //   1118: istore_1
    //   1119: aload_0
    //   1120: iload_1
    //   1121: putfield mMotionViewOriginalTop : I
    //   1124: aload_0
    //   1125: iload_2
    //   1126: iload #4
    //   1128: iadd
    //   1129: putfield mMotionY : I
    //   1132: aload_0
    //   1133: iload #5
    //   1135: putfield mMotionPosition : I
    //   1138: aload_0
    //   1139: iload_2
    //   1140: iconst_0
    //   1141: iadd
    //   1142: iload #4
    //   1144: iadd
    //   1145: putfield mLastY : I
    //   1148: aload_0
    //   1149: iload #7
    //   1151: putfield mDirection : I
    //   1154: goto -> 1157
    //   1157: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3627	-> 0
    //   #3628	-> 8
    //   #3629	-> 8
    //   #3630	-> 8
    //   #3631	-> 22
    //   #3633	-> 31
    //   #3635	-> 77
    //   #3636	-> 89
    //   #3637	-> 102
    //   #3638	-> 108
    //   #3639	-> 112
    //   #3640	-> 122
    //   #3643	-> 137
    //   #3633	-> 147
    //   #3643	-> 153
    //   #3645	-> 153
    //   #3646	-> 183
    //   #3648	-> 189
    //   #3656	-> 201
    //   #3658	-> 208
    //   #3661	-> 218
    //   #3665	-> 226
    //   #3666	-> 237
    //   #3667	-> 249
    //   #3668	-> 255
    //   #3669	-> 260
    //   #3674	-> 268
    //   #3675	-> 279
    //   #3679	-> 291
    //   #3682	-> 299
    //   #3683	-> 299
    //   #3684	-> 307
    //   #3685	-> 312
    //   #3684	-> 322
    //   #3689	-> 325
    //   #3690	-> 325
    //   #3691	-> 330
    //   #3690	-> 343
    //   #3695	-> 346
    //   #3696	-> 354
    //   #3699	-> 359
    //   #3700	-> 366
    //   #3703	-> 371
    //   #3705	-> 382
    //   #3707	-> 402
    //   #3708	-> 416
    //   #3709	-> 420
    //   #3710	-> 430
    //   #3747	-> 445
    //   #3713	-> 448
    //   #3716	-> 470
    //   #3718	-> 484
    //   #3721	-> 488
    //   #3722	-> 494
    //   #3724	-> 505
    //   #3722	-> 522
    //   #3725	-> 529
    //   #3726	-> 534
    //   #3727	-> 539
    //   #3729	-> 544
    //   #3730	-> 549
    //   #3731	-> 570
    //   #3730	-> 580
    //   #3732	-> 588
    //   #3733	-> 598
    //   #3735	-> 605
    //   #3736	-> 616
    //   #3737	-> 625
    //   #3738	-> 645
    //   #3737	-> 655
    //   #3739	-> 665
    //   #3740	-> 675
    //   #3742	-> 682
    //   #3700	-> 693
    //   #3747	-> 697
    //   #3696	-> 711
    //   #3749	-> 715
    //   #3750	-> 726
    //   #3661	-> 729
    //   #3751	-> 732
    //   #3752	-> 738
    //   #3753	-> 746
    //   #3754	-> 752
    //   #3755	-> 759
    //   #3757	-> 776
    //   #3758	-> 783
    //   #3761	-> 789
    //   #3764	-> 794
    //   #3766	-> 812
    //   #3767	-> 832
    //   #3768	-> 837
    //   #3770	-> 851
    //   #3773	-> 854
    //   #3774	-> 859
    //   #3776	-> 880
    //   #3777	-> 886
    //   #3779	-> 897
    //   #3777	-> 910
    //   #3780	-> 913
    //   #3781	-> 918
    //   #3782	-> 938
    //   #3781	-> 948
    //   #3783	-> 956
    //   #3784	-> 966
    //   #3786	-> 973
    //   #3787	-> 980
    //   #3788	-> 985
    //   #3789	-> 1005
    //   #3788	-> 1015
    //   #3790	-> 1025
    //   #3791	-> 1035
    //   #3793	-> 1042
    //   #3773	-> 1049
    //   #3798	-> 1049
    //   #3800	-> 1054
    //   #3801	-> 1061
    //   #3802	-> 1066
    //   #3805	-> 1070
    //   #3807	-> 1079
    //   #3811	-> 1084
    //   #3813	-> 1091
    //   #3814	-> 1098
    //   #3815	-> 1110
    //   #3816	-> 1124
    //   #3817	-> 1132
    //   #3819	-> 1138
    //   #3820	-> 1148
    //   #3823	-> 1157
  }
  
  private void invalidateTopGlow() {
    byte b;
    if (!shouldDisplayEdgeEffects())
      return; 
    boolean bool = getClipToPadding();
    int i = 0;
    if (bool) {
      b = this.mPaddingTop;
    } else {
      b = 0;
    } 
    if (bool)
      i = this.mPaddingLeft; 
    int j = getWidth(), k = j;
    if (bool)
      k = j - this.mPaddingRight; 
    invalidate(i, b, k, this.mEdgeGlowTop.getMaxHeight() + b);
  }
  
  private void invalidateBottomGlow() {
    if (!shouldDisplayEdgeEffects())
      return; 
    boolean bool = getClipToPadding();
    int i = getHeight(), j = i;
    if (bool)
      j = i - this.mPaddingBottom; 
    if (bool) {
      i = this.mPaddingLeft;
    } else {
      i = 0;
    } 
    int k = getWidth(), m = k;
    if (bool)
      m = k - this.mPaddingRight; 
    invalidate(i, j - this.mEdgeGlowBottom.getMaxHeight(), m, j);
  }
  
  public void onTouchModeChanged(boolean paramBoolean) {
    if (paramBoolean) {
      hideSelector();
      if (getHeight() > 0 && getChildCount() > 0)
        layoutChildren(); 
      updateSelectorState();
    } else {
      int i = this.mTouchMode;
      if (i == 5 || i == 6) {
        FlingRunnable flingRunnable = this.mFlingRunnable;
        if (flingRunnable != null)
          flingRunnable.endFling(); 
        AbsPositionScroller absPositionScroller = this.mPositionScroller;
        if (absPositionScroller != null)
          absPositionScroller.stop(); 
        if (this.mScrollY != 0) {
          this.mScrollY = 0;
          invalidateParentCaches();
          finishGlows();
          invalidate();
        } 
      } 
    } 
  }
  
  protected boolean handleScrollBarDragging(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    View view;
    boolean bool = isEnabled();
    boolean bool1 = true;
    if (!bool) {
      bool = bool1;
      if (!isClickable())
        if (isLongClickable()) {
          bool = bool1;
        } else {
          bool = false;
        }  
      return bool;
    } 
    AbsPositionScroller absPositionScroller = this.mPositionScroller;
    if (absPositionScroller != null)
      absPositionScroller.stop(); 
    if (this.mIsDetaching || !isAttachedToWindow())
      return false; 
    startNestedScroll(2);
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null && fastScroller.onTouchEvent(paramMotionEvent))
      return true; 
    initVelocityTrackerIfNotExists();
    MotionEvent motionEvent = MotionEvent.obtain(paramMotionEvent);
    int i = paramMotionEvent.getActionMasked();
    if (i == 0)
      this.mNestedYOffset = 0; 
    motionEvent.offsetLocation(0.0F, this.mNestedYOffset);
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 5) {
              if (i == 6) {
                onSecondaryPointerUp(paramMotionEvent);
                int j = this.mMotionX;
                i = this.mMotionY;
                j = pointToPosition(j, i);
                if (j >= 0) {
                  view = getChildAt(j - this.mFirstPosition);
                  this.mMotionViewOriginalTop = view.getTop();
                  this.mMotionPosition = j;
                } 
                this.mLastY = i;
                this.mNumTouchMoveEvent = 0;
              } 
            } else {
              int k = view.getActionIndex();
              int j = view.getPointerId(k);
              i = (int)view.getX(k);
              k = (int)view.getY(k);
              this.mMotionCorrection = 0;
              this.mActivePointerId = j;
              this.mMotionX = i;
              this.mMotionY = k;
              i = pointToPosition(i, k);
              if (i >= 0) {
                view = getChildAt(i - this.mFirstPosition);
                this.mMotionViewOriginalTop = view.getTop();
                this.mMotionPosition = i;
              } 
              this.mLastY = k;
              this.mNumTouchMoveEvent = 0;
            } 
          } else {
            onTouchCancel();
            this.mNumTouchMoveEvent = 0;
          } 
        } else {
          this.mNumTouchMoveEvent = i = this.mNumTouchMoveEvent + 1;
          if (i == 1) {
            this.mIsFirstTouchMoveEvent = true;
          } else {
            this.mIsFirstTouchMoveEvent = false;
          } 
          onTouchMove((MotionEvent)view, motionEvent);
        } 
      } else {
        onTouchUp((MotionEvent)view);
        this.mNumTouchMoveEvent = 0;
      } 
    } else {
      onTouchDown((MotionEvent)view);
      this.mNumTouchMoveEvent = 0;
    } 
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null)
      velocityTracker.addMovement(motionEvent); 
    motionEvent.recycle();
    return true;
  }
  
  private void onTouchDown(MotionEvent paramMotionEvent) {
    this.mHasPerformedLongPress = false;
    this.mActivePointerId = paramMotionEvent.getPointerId(0);
    hideSelector();
    if (this.mTouchMode == 6) {
      this.mFlingRunnable.endFling();
      AbsPositionScroller absPositionScroller = this.mPositionScroller;
      if (absPositionScroller != null)
        absPositionScroller.stop(); 
      this.mTouchMode = 5;
      this.mMotionX = (int)paramMotionEvent.getX();
      int i = (int)paramMotionEvent.getY();
      this.mLastY = i;
      this.mMotionCorrection = 0;
      this.mDirection = 0;
    } else {
      int j = (int)paramMotionEvent.getX();
      int k = (int)paramMotionEvent.getY();
      int m = pointToPosition(j, k);
      int i = m;
      if (!this.mDataChanged)
        if (this.mTouchMode == 4) {
          createScrollingCache();
          this.mTouchMode = 3;
          this.mMotionCorrection = 0;
          i = findMotionRow(k);
          this.mFlingRunnable.flywheelTouch();
        } else {
          i = m;
          if (m >= 0) {
            i = m;
            if (getAdapter().isEnabled(m)) {
              this.mTouchMode = 0;
              if (this.mPendingCheckForTap == null)
                this.mPendingCheckForTap = new CheckForTap(); 
              this.mPendingCheckForTap.x = paramMotionEvent.getX();
              this.mPendingCheckForTap.y = paramMotionEvent.getY();
              postDelayed(this.mPendingCheckForTap, ViewConfiguration.getTapTimeout());
              i = m;
            } 
          } 
        }  
      if (i >= 0) {
        View view = getChildAt(i - this.mFirstPosition);
        this.mMotionViewOriginalTop = view.getTop();
      } 
      this.mMotionX = j;
      this.mMotionY = k;
      this.mMotionPosition = i;
      this.mLastY = Integer.MIN_VALUE;
    } 
    if (this.mTouchMode == 0 && this.mMotionPosition != -1 && performButtonActionOnTouchDown(paramMotionEvent))
      removeCallbacks(this.mPendingCheckForTap); 
  }
  
  private void onTouchMove(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2) {
    if (this.mHasPerformedLongPress)
      return; 
    int i = paramMotionEvent1.findPointerIndex(this.mActivePointerId);
    int j = i;
    if (i == -1) {
      j = 0;
      this.mActivePointerId = paramMotionEvent1.getPointerId(0);
    } 
    if (this.mDataChanged)
      layoutChildren(); 
    i = (int)paramMotionEvent1.getY(j);
    int k = this.mTouchMode;
    if (k != 0 && k != 1 && k != 2) {
      if (k == 3 || k == 5)
        scrollIfNeeded((int)paramMotionEvent1.getX(j), i, paramMotionEvent2); 
    } else if (!startScrollIfNeeded((int)paramMotionEvent1.getX(j), i, paramMotionEvent2)) {
      View view = getChildAt(this.mMotionPosition - this.mFirstPosition);
      float f = paramMotionEvent1.getX(j);
      if (!pointInView(f, i, this.mTouchSlop)) {
        CheckForLongPress checkForLongPress;
        setPressed(false);
        if (view != null)
          view.setPressed(false); 
        if (this.mTouchMode == 0) {
          CheckForTap checkForTap = this.mPendingCheckForTap;
        } else {
          checkForLongPress = this.mPendingCheckForLongPress;
        } 
        removeCallbacks(checkForLongPress);
        this.mTouchMode = 2;
        updateSelectorState();
      } else if (view != null) {
        float[] arrayOfFloat = this.mTmpPoint;
        arrayOfFloat[0] = f;
        arrayOfFloat[1] = i;
        transformPointToViewLocal(arrayOfFloat, view);
        view.drawableHotspotChanged(arrayOfFloat[0], arrayOfFloat[1]);
      } 
    } 
  }
  
  private void onTouchUp(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTouchMode : I
    //   4: istore_2
    //   5: iload_2
    //   6: ifeq -> 493
    //   9: iload_2
    //   10: iconst_1
    //   11: if_icmpeq -> 493
    //   14: iload_2
    //   15: iconst_2
    //   16: if_icmpeq -> 493
    //   19: iload_2
    //   20: iconst_3
    //   21: if_icmpeq -> 133
    //   24: iload_2
    //   25: iconst_5
    //   26: if_icmpeq -> 32
    //   29: goto -> 925
    //   32: aload_0
    //   33: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   36: ifnonnull -> 51
    //   39: aload_0
    //   40: new android/widget/AbsListView$FlingRunnable
    //   43: dup
    //   44: aload_0
    //   45: invokespecial <init> : (Landroid/widget/AbsListView;)V
    //   48: putfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   51: aload_0
    //   52: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   55: astore_1
    //   56: aload_1
    //   57: sipush #1000
    //   60: aload_0
    //   61: getfield mMaximumVelocity : I
    //   64: i2f
    //   65: invokevirtual computeCurrentVelocity : (IF)V
    //   68: aload_1
    //   69: aload_0
    //   70: getfield mActivePointerId : I
    //   73: invokevirtual getYVelocity : (I)F
    //   76: f2i
    //   77: istore_2
    //   78: aload_0
    //   79: iconst_2
    //   80: invokevirtual reportScrollStateChange : (I)V
    //   83: iload_2
    //   84: invokestatic abs : (I)I
    //   87: aload_0
    //   88: getfield mMinimumVelocity : I
    //   91: if_icmple -> 123
    //   94: aload_0
    //   95: invokevirtual isOplusOSStyle : ()Z
    //   98: ifeq -> 111
    //   101: aload_0
    //   102: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   105: invokevirtual startSpringback : ()V
    //   108: goto -> 925
    //   111: aload_0
    //   112: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   115: iload_2
    //   116: ineg
    //   117: invokevirtual startOverfling : (I)V
    //   120: goto -> 925
    //   123: aload_0
    //   124: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   127: invokevirtual startSpringback : ()V
    //   130: goto -> 925
    //   133: aload_0
    //   134: invokevirtual getChildCount : ()I
    //   137: istore_3
    //   138: iload_3
    //   139: ifle -> 480
    //   142: aload_0
    //   143: iconst_0
    //   144: invokevirtual getChildAt : (I)Landroid/view/View;
    //   147: invokevirtual getTop : ()I
    //   150: istore #4
    //   152: aload_0
    //   153: iload_3
    //   154: iconst_1
    //   155: isub
    //   156: invokevirtual getChildAt : (I)Landroid/view/View;
    //   159: invokevirtual getBottom : ()I
    //   162: istore #5
    //   164: aload_0
    //   165: getfield mListPadding : Landroid/graphics/Rect;
    //   168: getfield top : I
    //   171: istore #6
    //   173: aload_0
    //   174: invokevirtual getHeight : ()I
    //   177: aload_0
    //   178: getfield mListPadding : Landroid/graphics/Rect;
    //   181: getfield bottom : I
    //   184: isub
    //   185: istore #7
    //   187: aload_0
    //   188: getfield mFirstPosition : I
    //   191: ifne -> 239
    //   194: iload #4
    //   196: iload #6
    //   198: if_icmplt -> 239
    //   201: aload_0
    //   202: getfield mFirstPosition : I
    //   205: iload_3
    //   206: iadd
    //   207: aload_0
    //   208: getfield mItemCount : I
    //   211: if_icmpge -> 239
    //   214: iload #5
    //   216: aload_0
    //   217: invokevirtual getHeight : ()I
    //   220: iload #7
    //   222: isub
    //   223: if_icmpgt -> 239
    //   226: aload_0
    //   227: iconst_m1
    //   228: putfield mTouchMode : I
    //   231: aload_0
    //   232: iconst_0
    //   233: invokevirtual reportScrollStateChange : (I)V
    //   236: goto -> 477
    //   239: aload_0
    //   240: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   243: astore_1
    //   244: aload_1
    //   245: sipush #1000
    //   248: aload_0
    //   249: getfield mMaximumVelocity : I
    //   252: i2f
    //   253: invokevirtual computeCurrentVelocity : (IF)V
    //   256: aload_0
    //   257: getfield mActivePointerId : I
    //   260: istore_2
    //   261: aload_1
    //   262: iload_2
    //   263: invokevirtual getYVelocity : (I)F
    //   266: aload_0
    //   267: getfield mVelocityScale : F
    //   270: fmul
    //   271: f2i
    //   272: istore #8
    //   274: iload #8
    //   276: invokestatic abs : (I)I
    //   279: aload_0
    //   280: getfield mMinimumVelocity : I
    //   283: if_icmple -> 291
    //   286: iconst_1
    //   287: istore_2
    //   288: goto -> 293
    //   291: iconst_0
    //   292: istore_2
    //   293: iload_2
    //   294: ifeq -> 414
    //   297: aload_0
    //   298: getfield mFirstPosition : I
    //   301: ifne -> 316
    //   304: iload #4
    //   306: iload #6
    //   308: aload_0
    //   309: getfield mOverscrollDistance : I
    //   312: isub
    //   313: if_icmpeq -> 414
    //   316: aload_0
    //   317: getfield mFirstPosition : I
    //   320: iload_3
    //   321: iadd
    //   322: aload_0
    //   323: getfield mItemCount : I
    //   326: if_icmpne -> 341
    //   329: iload #5
    //   331: aload_0
    //   332: getfield mOverscrollDistance : I
    //   335: iload #7
    //   337: iadd
    //   338: if_icmpeq -> 414
    //   341: aload_0
    //   342: fconst_0
    //   343: iload #8
    //   345: ineg
    //   346: i2f
    //   347: invokevirtual dispatchNestedPreFling : (FF)Z
    //   350: ifne -> 401
    //   353: aload_0
    //   354: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   357: ifnonnull -> 372
    //   360: aload_0
    //   361: new android/widget/AbsListView$FlingRunnable
    //   364: dup
    //   365: aload_0
    //   366: invokespecial <init> : (Landroid/widget/AbsListView;)V
    //   369: putfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   372: aload_0
    //   373: iconst_2
    //   374: invokevirtual reportScrollStateChange : (I)V
    //   377: aload_0
    //   378: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   381: iload #8
    //   383: ineg
    //   384: invokevirtual start : (I)V
    //   387: aload_0
    //   388: fconst_0
    //   389: iload #8
    //   391: ineg
    //   392: i2f
    //   393: iconst_1
    //   394: invokevirtual dispatchNestedFling : (FFZ)Z
    //   397: pop
    //   398: goto -> 477
    //   401: aload_0
    //   402: iconst_m1
    //   403: putfield mTouchMode : I
    //   406: aload_0
    //   407: iconst_0
    //   408: invokevirtual reportScrollStateChange : (I)V
    //   411: goto -> 477
    //   414: aload_0
    //   415: iconst_m1
    //   416: putfield mTouchMode : I
    //   419: aload_0
    //   420: iconst_0
    //   421: invokevirtual reportScrollStateChange : (I)V
    //   424: aload_0
    //   425: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   428: astore_1
    //   429: aload_1
    //   430: ifnull -> 437
    //   433: aload_1
    //   434: invokevirtual endFling : ()V
    //   437: aload_0
    //   438: getfield mPositionScroller : Landroid/widget/AbsListView$AbsPositionScroller;
    //   441: astore_1
    //   442: aload_1
    //   443: ifnull -> 450
    //   446: aload_1
    //   447: invokevirtual stop : ()V
    //   450: iload_2
    //   451: ifeq -> 477
    //   454: aload_0
    //   455: fconst_0
    //   456: iload #8
    //   458: ineg
    //   459: i2f
    //   460: invokevirtual dispatchNestedPreFling : (FF)Z
    //   463: ifne -> 477
    //   466: aload_0
    //   467: fconst_0
    //   468: iload #8
    //   470: ineg
    //   471: i2f
    //   472: iconst_0
    //   473: invokevirtual dispatchNestedFling : (FFZ)Z
    //   476: pop
    //   477: goto -> 925
    //   480: aload_0
    //   481: iconst_m1
    //   482: putfield mTouchMode : I
    //   485: aload_0
    //   486: iconst_0
    //   487: invokevirtual reportScrollStateChange : (I)V
    //   490: goto -> 925
    //   493: aload_0
    //   494: getfield mMotionPosition : I
    //   497: istore #7
    //   499: aload_0
    //   500: iload #7
    //   502: aload_0
    //   503: getfield mFirstPosition : I
    //   506: isub
    //   507: invokevirtual getChildAt : (I)Landroid/view/View;
    //   510: astore #9
    //   512: aload #9
    //   514: ifnull -> 916
    //   517: aload_0
    //   518: getfield mTouchMode : I
    //   521: ifeq -> 530
    //   524: aload #9
    //   526: iconst_0
    //   527: invokevirtual setPressed : (Z)V
    //   530: aload_1
    //   531: invokevirtual getX : ()F
    //   534: fstore #10
    //   536: fload #10
    //   538: aload_0
    //   539: getfield mListPadding : Landroid/graphics/Rect;
    //   542: getfield left : I
    //   545: i2f
    //   546: fcmpl
    //   547: ifle -> 574
    //   550: fload #10
    //   552: aload_0
    //   553: invokevirtual getWidth : ()I
    //   556: aload_0
    //   557: getfield mListPadding : Landroid/graphics/Rect;
    //   560: getfield right : I
    //   563: isub
    //   564: i2f
    //   565: fcmpg
    //   566: ifge -> 574
    //   569: iconst_1
    //   570: istore_2
    //   571: goto -> 576
    //   574: iconst_0
    //   575: istore_2
    //   576: iload_2
    //   577: ifeq -> 916
    //   580: aload #9
    //   582: invokevirtual hasExplicitFocusable : ()Z
    //   585: ifne -> 916
    //   588: aload_0
    //   589: getfield mPerformClick : Landroid/widget/AbsListView$PerformClick;
    //   592: ifnonnull -> 608
    //   595: aload_0
    //   596: new android/widget/AbsListView$PerformClick
    //   599: dup
    //   600: aload_0
    //   601: aconst_null
    //   602: invokespecial <init> : (Landroid/widget/AbsListView;Landroid/widget/AbsListView$1;)V
    //   605: putfield mPerformClick : Landroid/widget/AbsListView$PerformClick;
    //   608: aload_0
    //   609: getfield mPerformClick : Landroid/widget/AbsListView$PerformClick;
    //   612: astore #11
    //   614: aload #11
    //   616: iload #7
    //   618: putfield mClickMotionPosition : I
    //   621: aload #11
    //   623: invokevirtual rememberWindowAttachCount : ()V
    //   626: aload_0
    //   627: iload #7
    //   629: putfield mResurrectToPosition : I
    //   632: aload_0
    //   633: getfield mTouchMode : I
    //   636: istore_2
    //   637: iload_2
    //   638: ifeq -> 678
    //   641: iload_2
    //   642: iconst_1
    //   643: if_icmpne -> 649
    //   646: goto -> 678
    //   649: aload_0
    //   650: getfield mDataChanged : Z
    //   653: ifne -> 916
    //   656: aload_0
    //   657: getfield mAdapter : Landroid/widget/ListAdapter;
    //   660: iload #7
    //   662: invokeinterface isEnabled : (I)Z
    //   667: ifeq -> 916
    //   670: aload #11
    //   672: invokevirtual run : ()V
    //   675: goto -> 916
    //   678: aload_0
    //   679: getfield mTouchMode : I
    //   682: ifne -> 694
    //   685: aload_0
    //   686: getfield mPendingCheckForTap : Landroid/widget/AbsListView$CheckForTap;
    //   689: astore #12
    //   691: goto -> 700
    //   694: aload_0
    //   695: getfield mPendingCheckForLongPress : Landroid/widget/AbsListView$CheckForLongPress;
    //   698: astore #12
    //   700: aload_0
    //   701: aload #12
    //   703: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)Z
    //   706: pop
    //   707: aload_0
    //   708: iconst_0
    //   709: putfield mLayoutMode : I
    //   712: aload_0
    //   713: getfield mDataChanged : Z
    //   716: ifne -> 906
    //   719: aload_0
    //   720: getfield mAdapter : Landroid/widget/ListAdapter;
    //   723: iload #7
    //   725: invokeinterface isEnabled : (I)Z
    //   730: ifeq -> 906
    //   733: aload_0
    //   734: iconst_1
    //   735: putfield mTouchMode : I
    //   738: aload_0
    //   739: aload_0
    //   740: getfield mMotionPosition : I
    //   743: invokevirtual setSelectedPositionInt : (I)V
    //   746: aload_0
    //   747: invokevirtual layoutChildren : ()V
    //   750: aload #9
    //   752: iconst_1
    //   753: invokevirtual setPressed : (Z)V
    //   756: aload_0
    //   757: aload_0
    //   758: getfield mMotionPosition : I
    //   761: aload #9
    //   763: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   766: aload_0
    //   767: iconst_1
    //   768: invokevirtual setPressed : (Z)V
    //   771: aload_0
    //   772: getfield mSelector : Landroid/graphics/drawable/Drawable;
    //   775: astore #12
    //   777: aload #12
    //   779: ifnull -> 823
    //   782: aload #12
    //   784: invokevirtual getCurrent : ()Landroid/graphics/drawable/Drawable;
    //   787: astore #12
    //   789: aload #12
    //   791: ifnull -> 810
    //   794: aload #12
    //   796: instanceof android/graphics/drawable/TransitionDrawable
    //   799: ifeq -> 810
    //   802: aload #12
    //   804: checkcast android/graphics/drawable/TransitionDrawable
    //   807: invokevirtual resetTransition : ()V
    //   810: aload_0
    //   811: getfield mSelector : Landroid/graphics/drawable/Drawable;
    //   814: fload #10
    //   816: aload_1
    //   817: invokevirtual getY : ()F
    //   820: invokevirtual setHotspot : (FF)V
    //   823: aload_0
    //   824: getfield mDataChanged : Z
    //   827: ifne -> 858
    //   830: aload_0
    //   831: getfield mIsDetaching : Z
    //   834: ifne -> 858
    //   837: aload_0
    //   838: invokevirtual isAttachedToWindow : ()Z
    //   841: ifeq -> 858
    //   844: aload_0
    //   845: aload #11
    //   847: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   850: ifne -> 858
    //   853: aload #11
    //   855: invokevirtual run : ()V
    //   858: aload_0
    //   859: getfield mTouchModeReset : Ljava/lang/Runnable;
    //   862: astore_1
    //   863: aload_1
    //   864: ifnull -> 873
    //   867: aload_0
    //   868: aload_1
    //   869: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)Z
    //   872: pop
    //   873: new android/widget/AbsListView$3
    //   876: dup
    //   877: aload_0
    //   878: aload #9
    //   880: invokespecial <init> : (Landroid/widget/AbsListView;Landroid/view/View;)V
    //   883: astore_1
    //   884: aload_0
    //   885: aload_1
    //   886: putfield mTouchModeReset : Ljava/lang/Runnable;
    //   889: invokestatic getPressedStateDuration : ()I
    //   892: i2l
    //   893: lstore #13
    //   895: aload_0
    //   896: aload_1
    //   897: lload #13
    //   899: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   902: pop
    //   903: goto -> 915
    //   906: aload_0
    //   907: iconst_m1
    //   908: putfield mTouchMode : I
    //   911: aload_0
    //   912: invokevirtual updateSelectorState : ()V
    //   915: return
    //   916: aload_0
    //   917: iconst_m1
    //   918: putfield mTouchMode : I
    //   921: aload_0
    //   922: invokevirtual updateSelectorState : ()V
    //   925: aload_0
    //   926: iconst_0
    //   927: invokevirtual setPressed : (Z)V
    //   930: aload_0
    //   931: invokespecial shouldDisplayEdgeEffects : ()Z
    //   934: ifeq -> 963
    //   937: aload_0
    //   938: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   941: astore_1
    //   942: aload_1
    //   943: ifnull -> 950
    //   946: aload_1
    //   947: invokevirtual onRelease : ()V
    //   950: aload_0
    //   951: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   954: astore_1
    //   955: aload_1
    //   956: ifnull -> 963
    //   959: aload_1
    //   960: invokevirtual onRelease : ()V
    //   963: aload_0
    //   964: invokevirtual invalidate : ()V
    //   967: aload_0
    //   968: aload_0
    //   969: getfield mPendingCheckForLongPress : Landroid/widget/AbsListView$CheckForLongPress;
    //   972: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)Z
    //   975: pop
    //   976: aload_0
    //   977: invokespecial recycleVelocityTracker : ()V
    //   980: aload_0
    //   981: iconst_m1
    //   982: putfield mActivePointerId : I
    //   985: aload_0
    //   986: getfield mScrollStrictSpan : Landroid/os/StrictMode$Span;
    //   989: astore_1
    //   990: aload_1
    //   991: ifnull -> 1003
    //   994: aload_1
    //   995: invokevirtual finish : ()V
    //   998: aload_0
    //   999: aconst_null
    //   1000: putfield mScrollStrictSpan : Landroid/os/StrictMode$Span;
    //   1003: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4134	-> 0
    //   #4268	-> 32
    //   #4269	-> 39
    //   #4271	-> 51
    //   #4272	-> 56
    //   #4273	-> 68
    //   #4275	-> 78
    //   #4276	-> 83
    //   #4282	-> 94
    //   #4283	-> 101
    //   #4285	-> 111
    //   #4289	-> 123
    //   #4209	-> 133
    //   #4210	-> 138
    //   #4211	-> 142
    //   #4212	-> 152
    //   #4213	-> 164
    //   #4214	-> 173
    //   #4215	-> 187
    //   #4217	-> 214
    //   #4218	-> 226
    //   #4219	-> 231
    //   #4221	-> 239
    //   #4222	-> 244
    //   #4224	-> 256
    //   #4225	-> 261
    //   #4230	-> 274
    //   #4231	-> 293
    //   #4236	-> 341
    //   #4237	-> 353
    //   #4238	-> 360
    //   #4240	-> 372
    //   #4241	-> 377
    //   #4242	-> 387
    //   #4244	-> 401
    //   #4245	-> 406
    //   #4248	-> 414
    //   #4249	-> 419
    //   #4250	-> 424
    //   #4251	-> 433
    //   #4253	-> 437
    //   #4254	-> 446
    //   #4256	-> 450
    //   #4257	-> 466
    //   #4261	-> 477
    //   #4262	-> 480
    //   #4263	-> 485
    //   #4265	-> 490
    //   #4138	-> 493
    //   #4139	-> 499
    //   #4140	-> 512
    //   #4141	-> 517
    //   #4142	-> 524
    //   #4145	-> 530
    //   #4146	-> 536
    //   #4147	-> 576
    //   #4148	-> 588
    //   #4149	-> 595
    //   #4152	-> 608
    //   #4153	-> 614
    //   #4154	-> 621
    //   #4156	-> 626
    //   #4158	-> 632
    //   #4200	-> 649
    //   #4201	-> 670
    //   #4159	-> 678
    //   #4160	-> 685
    //   #4159	-> 700
    //   #4161	-> 707
    //   #4162	-> 712
    //   #4163	-> 733
    //   #4164	-> 738
    //   #4165	-> 746
    //   #4166	-> 750
    //   #4167	-> 756
    //   #4168	-> 766
    //   #4169	-> 771
    //   #4170	-> 782
    //   #4171	-> 789
    //   #4172	-> 802
    //   #4174	-> 810
    //   #4176	-> 823
    //   #4177	-> 844
    //   #4178	-> 853
    //   #4181	-> 858
    //   #4182	-> 867
    //   #4184	-> 873
    //   #4193	-> 889
    //   #4194	-> 889
    //   #4193	-> 895
    //   #4196	-> 906
    //   #4197	-> 911
    //   #4199	-> 915
    //   #4205	-> 916
    //   #4206	-> 921
    //   #4207	-> 925
    //   #4295	-> 925
    //   #4305	-> 930
    //   #4306	-> 937
    //   #4307	-> 946
    //   #4309	-> 950
    //   #4310	-> 959
    //   #4317	-> 963
    //   #4318	-> 967
    //   #4319	-> 976
    //   #4321	-> 980
    //   #4330	-> 985
    //   #4331	-> 994
    //   #4332	-> 998
    //   #4334	-> 1003
  }
  
  private boolean shouldDisplayEdgeEffects() {
    boolean bool;
    if (getOverScrollMode() != 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void onTouchCancel() {
    int i = this.mTouchMode;
    if (i != 5) {
      if (i != 6) {
        this.mTouchMode = -1;
        setPressed(false);
        View view = getChildAt(this.mMotionPosition - this.mFirstPosition);
        if (view != null)
          view.setPressed(false); 
        clearScrollingCache();
        removeCallbacks(this.mPendingCheckForLongPress);
        recycleVelocityTracker();
      } 
    } else {
      if (this.mFlingRunnable == null)
        this.mFlingRunnable = new FlingRunnable(); 
      this.mFlingRunnable.startSpringback();
    } 
    if (shouldDisplayEdgeEffects()) {
      this.mEdgeGlowTop.onRelease();
      this.mEdgeGlowBottom.onRelease();
    } 
    this.mActivePointerId = -1;
  }
  
  protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mScrollY != paramInt2) {
      onScrollChanged(this.mScrollX, paramInt2, this.mScrollX, this.mScrollY);
      this.mScrollY = paramInt2;
      invalidateParentIfNeeded();
      awakenScrollBars();
    } 
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    if (i != 8) {
      if (i == 11)
        if (paramMotionEvent.isFromSource(2)) {
          i = paramMotionEvent.getActionButton();
          if (i == 32 || i == 2) {
            i = this.mTouchMode;
            if (i == 0 || i == 1)
              if (performStylusButtonPressAction(paramMotionEvent)) {
                removeCallbacks(this.mPendingCheckForLongPress);
                removeCallbacks(this.mPendingCheckForTap);
              }  
          } 
        }  
    } else {
      float f;
      if (paramMotionEvent.isFromSource(2)) {
        f = paramMotionEvent.getAxisValue(9);
      } else if (paramMotionEvent.isFromSource(4194304)) {
        f = paramMotionEvent.getAxisValue(26);
      } else {
        f = 0.0F;
      } 
      i = Math.round(this.mVerticalScrollFactor * f);
      if (i != 0 && !trackMotionScroll(i, i))
        return true; 
    } 
    return super.onGenericMotionEvent(paramMotionEvent);
  }
  
  public void fling(int paramInt) {
    if (this.mFlingRunnable == null)
      this.mFlingRunnable = new FlingRunnable(); 
    reportScrollStateChange(2);
    this.mFlingRunnable.start(paramInt);
  }
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt) {
    boolean bool;
    if ((paramInt & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt) {
    super.onNestedScrollAccepted(paramView1, paramView2, paramInt);
    startNestedScroll(2);
  }
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt1 = getChildCount() / 2;
    paramView = getChildAt(paramInt1);
    if (paramView != null) {
      paramInt1 = paramView.getTop();
    } else {
      paramInt1 = 0;
    } 
    if (paramView == null || trackMotionScroll(-paramInt4, -paramInt4)) {
      if (paramView != null) {
        paramInt1 = paramView.getTop() - paramInt1;
        paramInt4 -= paramInt1;
      } else {
        paramInt1 = 0;
      } 
      dispatchNestedScroll(0, paramInt1, 0, paramInt4, (int[])null);
    } 
  }
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    int i = getChildCount();
    if (!paramBoolean && i > 0 && canScrollList((int)paramFloat2) && Math.abs(paramFloat2) > this.mMinimumVelocity) {
      reportScrollStateChange(2);
      if (this.mFlingRunnable == null)
        this.mFlingRunnable = new FlingRunnable(); 
      if (!dispatchNestedPreFling(0.0F, paramFloat2))
        this.mFlingRunnable.start((int)paramFloat2); 
      return true;
    } 
    return dispatchNestedFling(paramFloat1, paramFloat2, paramBoolean);
  }
  
  public void draw(Canvas paramCanvas) {
    super.draw(paramCanvas);
    if (shouldDisplayEdgeEffects()) {
      int j, k;
      byte b1;
      int m, i = this.mScrollY;
      boolean bool = getClipToPadding();
      if (bool) {
        j = getWidth() - this.mPaddingLeft - this.mPaddingRight;
        k = getHeight() - this.mPaddingTop - this.mPaddingBottom;
        b1 = this.mPaddingLeft;
        m = this.mPaddingTop;
      } else {
        j = getWidth();
        k = getHeight();
        b1 = 0;
        m = 0;
      } 
      this.mEdgeGlowTop.setSize(j, k);
      this.mEdgeGlowBottom.setSize(j, k);
      EdgeEffect edgeEffect = this.mEdgeGlowTop;
      byte b2 = 0;
      if (edgeEffect != null && !edgeEffect.isFinished()) {
        int n = paramCanvas.save();
        edgeEffect = this.mEdgeGlowTop;
        int i1 = edgeEffect.getMaxHeight();
        paramCanvas.clipRect(b1, m, b1 + j, i1 + m);
        i1 = Math.min(0, this.mFirstPositionDistanceGuess + i);
        paramCanvas.translate(b1, (i1 + m));
        if (this.mEdgeGlowTop.draw(paramCanvas))
          invalidateTopGlow(); 
        paramCanvas.restoreToCount(n);
      } 
      edgeEffect = this.mEdgeGlowBottom;
      if (edgeEffect != null && !edgeEffect.isFinished()) {
        int n = paramCanvas.save();
        paramCanvas.clipRect(b1, m + k - this.mEdgeGlowBottom.getMaxHeight(), b1 + j, m + k);
        k = -j;
        i = Math.max(getHeight(), this.mLastPositionDistanceGuess + i);
        m = b2;
        if (bool)
          m = this.mPaddingBottom; 
        paramCanvas.translate((k + b1), (i - m));
        paramCanvas.rotate(180.0F, j, 0.0F);
        if (this.mEdgeGlowBottom.draw(paramCanvas))
          invalidateBottomGlow(); 
        paramCanvas.restoreToCount(n);
      } 
    } 
  }
  
  private void initOrResetVelocityTracker() {
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker == null) {
      this.mVelocityTracker = VelocityTracker.obtain();
    } else {
      velocityTracker.clear();
    } 
  }
  
  private void initVelocityTrackerIfNotExists() {
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain(); 
  }
  
  private void recycleVelocityTracker() {
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null) {
      velocityTracker.recycle();
      this.mVelocityTracker = null;
    } 
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean) {
    if (paramBoolean)
      recycleVelocityTracker(); 
    super.requestDisallowInterceptTouchEvent(paramBoolean);
  }
  
  public boolean onInterceptHoverEvent(MotionEvent paramMotionEvent) {
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null && fastScroller.onInterceptHoverEvent(paramMotionEvent))
      return true; 
    return super.onInterceptHoverEvent(paramMotionEvent);
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null) {
      PointerIcon pointerIcon = fastScroller.onResolvePointerIcon(paramMotionEvent, paramInt);
      if (pointerIcon != null)
        return pointerIcon; 
    } 
    return super.onResolvePointerIcon(paramMotionEvent, paramInt);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    AbsPositionScroller absPositionScroller = this.mPositionScroller;
    if (absPositionScroller != null)
      absPositionScroller.stop(); 
    if (this.mIsDetaching || !isAttachedToWindow())
      return false; 
    FastScroller fastScroller = this.mFastScroll;
    if (fastScroller != null && fastScroller.onInterceptTouchEvent(paramMotionEvent))
      return true; 
    if (i != 0) {
      if (i != 1)
        if (i != 2) {
          if (i != 3) {
            if (i == 6) {
              this.mNumTouchMoveEvent = 0;
              onSecondaryPointerUp(paramMotionEvent);
            } 
            return false;
          } 
        } else {
          this.mNumTouchMoveEvent = i = this.mNumTouchMoveEvent + 1;
          if (i == 1) {
            this.mIsFirstTouchMoveEvent = true;
          } else {
            this.mIsFirstTouchMoveEvent = false;
          } 
          if (this.mTouchMode == 0) {
            int j = paramMotionEvent.findPointerIndex(this.mActivePointerId);
            i = j;
            if (j == -1) {
              i = 0;
              this.mActivePointerId = paramMotionEvent.getPointerId(0);
            } 
            j = (int)paramMotionEvent.getY(i);
            initVelocityTrackerIfNotExists();
            this.mVelocityTracker.addMovement(paramMotionEvent);
            if (startScrollIfNeeded((int)paramMotionEvent.getX(i), j, (MotionEvent)null))
              return true; 
          } 
          return false;
        }  
      this.mNumTouchMoveEvent = 0;
      this.mTouchMode = -1;
      this.mActivePointerId = -1;
      recycleVelocityTracker();
      reportScrollStateChange(0);
      stopNestedScroll();
    } else {
      this.mNumTouchMoveEvent = 0;
      int k = this.mTouchMode;
      if (k == 6 || k == 5) {
        this.mMotionCorrection = 0;
        return true;
      } 
      int m = (int)paramMotionEvent.getX();
      int j = (int)paramMotionEvent.getY();
      this.mActivePointerId = paramMotionEvent.getPointerId(0);
      i = findMotionRow(j);
      if (k != 4 && i >= 0) {
        View view = getChildAt(i - this.mFirstPosition);
        this.mMotionViewOriginalTop = view.getTop();
        this.mMotionX = m;
        this.mMotionY = j;
        this.mMotionPosition = i;
        this.mTouchMode = 0;
        clearScrollingCache();
      } 
      this.mLastY = Integer.MIN_VALUE;
      initOrResetVelocityTracker();
      this.mVelocityTracker.addMovement(paramMotionEvent);
      this.mNestedYOffset = 0;
      startNestedScroll(2);
      if (k == 4)
        return true; 
    } 
    return false;
  }
  
  private void onSecondaryPointerUp(MotionEvent paramMotionEvent) {
    int i = (paramMotionEvent.getAction() & 0xFF00) >> 8;
    int j = paramMotionEvent.getPointerId(i);
    if (j == this.mActivePointerId) {
      if (i == 0) {
        j = 1;
      } else {
        j = 0;
      } 
      this.mMotionX = (int)paramMotionEvent.getX(j);
      this.mMotionY = (int)paramMotionEvent.getY(j);
      this.mMotionCorrection = 0;
      this.mActivePointerId = paramMotionEvent.getPointerId(j);
    } 
  }
  
  public void addTouchables(ArrayList<View> paramArrayList) {
    int i = getChildCount();
    int j = this.mFirstPosition;
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter == null)
      return; 
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (listAdapter.isEnabled(j + b))
        paramArrayList.add(view); 
      view.addTouchables(paramArrayList);
    } 
  }
  
  void reportScrollStateChange(int paramInt) {
    if (paramInt != this.mLastScrollState) {
      OnScrollListener onScrollListener = this.mOnScrollListener;
      if (onScrollListener != null) {
        this.mLastScrollState = paramInt;
        onScrollListener.onScrollStateChanged(this, paramInt);
      } 
    } 
  }
  
  class FlingRunnable implements Runnable {
    private static final int FLYWHEEL_TIMEOUT = 40;
    
    private final Runnable mCheckFlywheel = new Runnable() {
        final AbsListView.FlingRunnable this$1;
        
        public void run() {
          int i = AbsListView.this.mActivePointerId;
          VelocityTracker velocityTracker = AbsListView.this.mVelocityTracker;
          OverScroller overScroller = AbsListView.FlingRunnable.this.mScroller;
          if (velocityTracker == null || i == -1)
            return; 
          velocityTracker.computeCurrentVelocity(1000, AbsListView.this.mMaximumVelocity);
          float f = -velocityTracker.getYVelocity(i);
          if (Math.abs(f) >= AbsListView.this.mMinimumVelocity && overScroller.isScrollingInDirection(0.0F, f)) {
            long l;
            AbsListView absListView = AbsListView.this;
            if (AbsListView.this.isOplusOSStyle()) {
              l = 0L;
            } else {
              l = 40L;
            } 
            absListView.postDelayed(this, l);
          } else {
            AbsListView.FlingRunnable.this.endFling();
            AbsListView.this.mTouchMode = 3;
            AbsListView.this.reportScrollStateChange(1);
          } 
        }
      };
    
    private int mLastFlingY;
    
    private final OverScroller mScroller;
    
    private boolean mSmoothScroll;
    
    private boolean mSuppressIdleStateChangeCall;
    
    final AbsListView this$0;
    
    void start(int param1Int) {
      boolean bool;
      if (param1Int < 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mLastFlingY = bool;
      this.mScroller.setInterpolator(null);
      this.mScroller.fling(0, bool, 0, param1Int, 0, 2147483647, 0, 2147483647);
      AbsListView.this.mTouchMode = 4;
      this.mSuppressIdleStateChangeCall = false;
      AbsListView.this.postOnAnimation(this);
      if (AbsListView.this.mFlingStrictSpan == null)
        AbsListView.access$2202(AbsListView.this, StrictMode.enterCriticalSpan("AbsListView-fling")); 
    }
    
    void startSpringback() {
      this.mSuppressIdleStateChangeCall = false;
      if (this.mScroller.springBack(0, AbsListView.this.mScrollY, 0, 0, 0, 0)) {
        AbsListView.this.mTouchMode = 6;
        AbsListView.this.invalidate();
        AbsListView.this.postOnAnimation(this);
      } else {
        AbsListView.this.mTouchMode = -1;
        AbsListView.this.reportScrollStateChange(0);
      } 
    }
    
    void startOverfling(int param1Int) {
      this.mScroller.setInterpolator(null);
      OverScroller overScroller = this.mScroller;
      int i = AbsListView.this.mScrollY;
      AbsListView absListView = AbsListView.this;
      int j = absListView.getHeight();
      overScroller.fling(0, i, 0, param1Int, 0, 0, -2147483648, 2147483647, 0, j);
      AbsListView.this.mTouchMode = 6;
      this.mSuppressIdleStateChangeCall = false;
      AbsListView.this.invalidate();
      AbsListView.this.postOnAnimation(this);
    }
    
    void edgeReached(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mScroller : Landroid/widget/OverScroller;
      //   4: aload_0
      //   5: getfield this$0 : Landroid/widget/AbsListView;
      //   8: invokestatic access$2500 : (Landroid/widget/AbsListView;)I
      //   11: iconst_0
      //   12: aload_0
      //   13: getfield this$0 : Landroid/widget/AbsListView;
      //   16: getfield mOverflingDistance : I
      //   19: invokevirtual notifyVerticalEdgeReached : (III)V
      //   22: aload_0
      //   23: getfield this$0 : Landroid/widget/AbsListView;
      //   26: invokevirtual getOverScrollMode : ()I
      //   29: istore_2
      //   30: iload_2
      //   31: ifeq -> 85
      //   34: iload_2
      //   35: iconst_1
      //   36: if_icmpne -> 54
      //   39: aload_0
      //   40: getfield this$0 : Landroid/widget/AbsListView;
      //   43: astore_3
      //   44: aload_3
      //   45: invokestatic access$2600 : (Landroid/widget/AbsListView;)Z
      //   48: ifne -> 54
      //   51: goto -> 85
      //   54: aload_0
      //   55: getfield this$0 : Landroid/widget/AbsListView;
      //   58: iconst_m1
      //   59: putfield mTouchMode : I
      //   62: aload_0
      //   63: getfield this$0 : Landroid/widget/AbsListView;
      //   66: getfield mPositionScroller : Landroid/widget/AbsListView$AbsPositionScroller;
      //   69: ifnull -> 132
      //   72: aload_0
      //   73: getfield this$0 : Landroid/widget/AbsListView;
      //   76: getfield mPositionScroller : Landroid/widget/AbsListView$AbsPositionScroller;
      //   79: invokevirtual stop : ()V
      //   82: goto -> 132
      //   85: aload_0
      //   86: getfield this$0 : Landroid/widget/AbsListView;
      //   89: bipush #6
      //   91: putfield mTouchMode : I
      //   94: aload_0
      //   95: getfield mScroller : Landroid/widget/OverScroller;
      //   98: invokevirtual getCurrVelocity : ()F
      //   101: f2i
      //   102: istore_2
      //   103: iload_1
      //   104: ifle -> 121
      //   107: aload_0
      //   108: getfield this$0 : Landroid/widget/AbsListView;
      //   111: invokestatic access$2700 : (Landroid/widget/AbsListView;)Landroid/widget/EdgeEffect;
      //   114: iload_2
      //   115: invokevirtual onAbsorb : (I)V
      //   118: goto -> 132
      //   121: aload_0
      //   122: getfield this$0 : Landroid/widget/AbsListView;
      //   125: invokestatic access$2800 : (Landroid/widget/AbsListView;)Landroid/widget/EdgeEffect;
      //   128: iload_2
      //   129: invokevirtual onAbsorb : (I)V
      //   132: aload_0
      //   133: getfield this$0 : Landroid/widget/AbsListView;
      //   136: invokevirtual invalidate : ()V
      //   139: aload_0
      //   140: getfield this$0 : Landroid/widget/AbsListView;
      //   143: aload_0
      //   144: invokevirtual postOnAnimation : (Ljava/lang/Runnable;)V
      //   147: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #4876	-> 0
      //   #4877	-> 22
      //   #4878	-> 30
      //   #4879	-> 44
      //   #4888	-> 54
      //   #4889	-> 62
      //   #4890	-> 72
      //   #4880	-> 85
      //   #4881	-> 94
      //   #4882	-> 103
      //   #4883	-> 107
      //   #4885	-> 121
      //   #4887	-> 132
      //   #4893	-> 132
      //   #4894	-> 139
      //   #4895	-> 147
    }
    
    void startScroll(int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2) {
      startScroll(param1Int1, param1Int2, param1Boolean1, param1Boolean2, null);
    }
    
    void startScroll(int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2, Interpolator param1Interpolator) {
      boolean bool2, bool1 = false;
      if (param1Int1 < 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mLastFlingY = bool2;
      if (param1Interpolator != null)
        bool1 = true; 
      this.mSmoothScroll = bool1;
      if (bool1) {
        this.mScroller.setInterpolator(param1Interpolator);
      } else {
        OverScroller overScroller = this.mScroller;
        if (param1Boolean1) {
          param1Interpolator = AbsListView.sLinearInterpolator;
        } else {
          param1Interpolator = null;
        } 
        overScroller.setInterpolator(param1Interpolator);
      } 
      this.mScroller.startScroll(0, bool2, 0, param1Int1, param1Int2);
      AbsListView.this.mTouchMode = 4;
      this.mSuppressIdleStateChangeCall = param1Boolean2;
      AbsListView.this.postOnAnimation(this);
    }
    
    void endFling() {
      AbsListView.this.mTouchMode = -1;
      this.mSmoothScroll = false;
      AbsListView.this.removeCallbacks(this);
      AbsListView.this.removeCallbacks(this.mCheckFlywheel);
      if (!this.mSuppressIdleStateChangeCall)
        AbsListView.this.reportScrollStateChange(0); 
      AbsListView.this.clearScrollingCache();
      this.mScroller.abortAnimation();
      if (AbsListView.this.mFlingStrictSpan != null) {
        AbsListView.this.mFlingStrictSpan.finish();
        AbsListView.access$2202(AbsListView.this, (StrictMode.Span)null);
      } 
    }
    
    void flywheelTouch() {
      long l;
      AbsListView absListView = AbsListView.this;
      Runnable runnable = this.mCheckFlywheel;
      if (absListView.isOplusOSStyle()) {
        l = 0L;
      } else {
        l = 40L;
      } 
      absListView.postDelayed(runnable, l);
    }
    
    public void run() {
      int i = AbsListView.this.mTouchMode, j = 0, k = 0;
      if (i != 3) {
        if (i != 4) {
          if (i != 6) {
            endFling();
            return;
          } 
          OverScroller overScroller1 = this.mScroller;
          if (overScroller1.computeScrollOffset()) {
            int i1 = AbsListView.this.mScrollY;
            j = overScroller1.getCurrY();
            AbsListView absListView1 = AbsListView.this;
            if (absListView1.overScrollBy(0, j - i1, 0, i1, 0, 0, 0, absListView1.mOverflingDistance, false)) {
              int i2;
              if (i1 <= 0 && j > 0) {
                i2 = 1;
              } else {
                i2 = 0;
              } 
              i = k;
              if (i1 >= 0) {
                i = k;
                if (j < 0)
                  i = 1; 
              } 
              if (i2 || i != 0) {
                k = (int)overScroller1.getCurrVelocity();
                i2 = k;
                if (i != 0)
                  i2 = -k; 
                overScroller1.abortAnimation();
                start(i2);
                return;
              } 
              startSpringback();
            } else {
              AbsListView.this.invalidate();
              AbsListView.this.postOnAnimation(this);
            } 
          } else {
            endFling();
          } 
          return;
        } 
      } else if (this.mScroller.isFinished()) {
        return;
      } 
      if (AbsListView.this.mDataChanged)
        AbsListView.this.layoutChildren(); 
      if (AbsListView.this.mItemCount == 0 || AbsListView.this.getChildCount() == 0) {
        endFling();
        return;
      } 
      OverScroller overScroller = this.mScroller;
      boolean bool1 = overScroller.computeScrollOffset();
      int m = overScroller.getCurrY();
      int n = this.mLastFlingY - m;
      if (n > 0) {
        AbsListView absListView1 = AbsListView.this;
        absListView1.mMotionPosition = absListView1.mFirstPosition;
        View view1 = AbsListView.this.getChildAt(0);
        AbsListView.this.mMotionViewOriginalTop = view1.getTop();
        i = Math.min(AbsListView.this.getHeight() - AbsListView.this.mPaddingBottom - AbsListView.this.mPaddingTop - 1, n);
      } else {
        i = AbsListView.this.getChildCount() - 1;
        AbsListView absListView1 = AbsListView.this;
        absListView1.mMotionPosition = absListView1.mFirstPosition + i;
        View view1 = AbsListView.this.getChildAt(i);
        AbsListView.this.mMotionViewOriginalTop = view1.getTop();
        i = Math.max(-(AbsListView.this.getHeight() - AbsListView.this.mPaddingBottom - AbsListView.this.mPaddingTop - 1), n);
      } 
      AbsListView absListView = AbsListView.this;
      View view = absListView.getChildAt(absListView.mMotionPosition - AbsListView.this.mFirstPosition);
      n = 0;
      if (view != null)
        n = view.getTop(); 
      boolean bool2 = AbsListView.this.trackMotionScroll(i, i);
      k = j;
      if (bool2) {
        k = j;
        if (i != 0)
          k = 1; 
      } 
      if (k != 0) {
        if (view != null) {
          n = -(i - view.getTop() - n);
          view = AbsListView.this;
          view.overScrollBy(0, n, 0, ((AbsListView)view).mScrollY, 0, 0, 0, AbsListView.this.mOverflingDistance, false);
        } 
        if (bool1)
          edgeReached(i); 
      } else if (bool1 && k == 0) {
        if (bool2)
          AbsListView.this.invalidate(); 
        this.mLastFlingY = m;
        AbsListView.this.postOnAnimation(this);
      } else {
        endFling();
      } 
    }
    
    FlingRunnable() {
      this.mSmoothScroll = false;
      this.mScroller = AbsListView.this.getOverScroller();
    }
  }
  
  public void setFriction(float paramFloat) {
    if (this.mFlingRunnable == null)
      this.mFlingRunnable = new FlingRunnable(); 
    this.mFlingRunnable.mScroller.setFriction(paramFloat);
  }
  
  public void setVelocityScale(float paramFloat) {
    this.mVelocityScale = paramFloat;
  }
  
  AbsPositionScroller createPositionScroller() {
    return new PositionScroller();
  }
  
  public void smoothScrollToPosition(int paramInt) {
    if (this.mPositionScroller == null)
      this.mPositionScroller = createPositionScroller(); 
    this.mPositionScroller.start(paramInt);
  }
  
  public void smoothScrollToPositionFromTop(int paramInt1, int paramInt2, int paramInt3) {
    if (this.mPositionScroller == null)
      this.mPositionScroller = createPositionScroller(); 
    this.mPositionScroller.startWithOffset(paramInt1, paramInt2, paramInt3);
  }
  
  public void smoothScrollToPositionFromTop(int paramInt1, int paramInt2) {
    if (this.mPositionScroller == null)
      this.mPositionScroller = createPositionScroller(); 
    this.mPositionScroller.startWithOffset(paramInt1, paramInt2);
  }
  
  public void smoothScrollToPosition(int paramInt1, int paramInt2) {
    if (this.mPositionScroller == null)
      this.mPositionScroller = createPositionScroller(); 
    this.mPositionScroller.start(paramInt1, paramInt2);
  }
  
  public void smoothScrollBy(int paramInt1, int paramInt2) {
    smoothScrollBy(paramInt1, paramInt2, false, false);
  }
  
  void smoothScrollBy(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    smoothScrollBy(paramInt1, paramInt2, paramBoolean1, paramBoolean2, (Interpolator)null);
  }
  
  void smoothScrollBy(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, Interpolator paramInterpolator) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   4: ifnonnull -> 19
    //   7: aload_0
    //   8: new android/widget/AbsListView$FlingRunnable
    //   11: dup
    //   12: aload_0
    //   13: invokespecial <init> : (Landroid/widget/AbsListView;)V
    //   16: putfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   19: aload_0
    //   20: getfield mFirstPosition : I
    //   23: istore #6
    //   25: aload_0
    //   26: invokevirtual getChildCount : ()I
    //   29: istore #7
    //   31: aload_0
    //   32: invokevirtual getPaddingTop : ()I
    //   35: istore #8
    //   37: aload_0
    //   38: invokevirtual getHeight : ()I
    //   41: istore #9
    //   43: aload_0
    //   44: invokevirtual getPaddingBottom : ()I
    //   47: istore #10
    //   49: iload_1
    //   50: ifeq -> 147
    //   53: aload_0
    //   54: getfield mItemCount : I
    //   57: ifeq -> 147
    //   60: iload #7
    //   62: ifeq -> 147
    //   65: iload #6
    //   67: ifne -> 87
    //   70: aload_0
    //   71: iconst_0
    //   72: invokevirtual getChildAt : (I)Landroid/view/View;
    //   75: invokevirtual getTop : ()I
    //   78: iload #8
    //   80: if_icmpne -> 87
    //   83: iload_1
    //   84: iflt -> 147
    //   87: iload #6
    //   89: iload #7
    //   91: iadd
    //   92: aload_0
    //   93: getfield mItemCount : I
    //   96: if_icmpne -> 125
    //   99: aload_0
    //   100: iload #7
    //   102: iconst_1
    //   103: isub
    //   104: invokevirtual getChildAt : (I)Landroid/view/View;
    //   107: invokevirtual getBottom : ()I
    //   110: iload #9
    //   112: iload #10
    //   114: isub
    //   115: if_icmpne -> 125
    //   118: iload_1
    //   119: ifle -> 125
    //   122: goto -> 147
    //   125: aload_0
    //   126: iconst_2
    //   127: invokevirtual reportScrollStateChange : (I)V
    //   130: aload_0
    //   131: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   134: iload_1
    //   135: iload_2
    //   136: iload_3
    //   137: iload #4
    //   139: aload #5
    //   141: invokevirtual startScroll : (IIZZLandroid/view/animation/Interpolator;)V
    //   144: goto -> 170
    //   147: aload_0
    //   148: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   151: invokevirtual endFling : ()V
    //   154: aload_0
    //   155: getfield mPositionScroller : Landroid/widget/AbsListView$AbsPositionScroller;
    //   158: astore #5
    //   160: aload #5
    //   162: ifnull -> 170
    //   165: aload #5
    //   167: invokevirtual stop : ()V
    //   170: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5209	-> 0
    //   #5210	-> 7
    //   #5214	-> 19
    //   #5215	-> 25
    //   #5216	-> 31
    //   #5217	-> 31
    //   #5218	-> 37
    //   #5220	-> 49
    //   #5221	-> 70
    //   #5223	-> 99
    //   #5229	-> 125
    //   #5235	-> 130
    //   #5224	-> 147
    //   #5225	-> 154
    //   #5226	-> 165
    //   #5238	-> 170
  }
  
  void smoothScrollByOffset(int paramInt) {
    int i = -1;
    if (paramInt < 0) {
      i = getFirstVisiblePosition();
    } else if (paramInt > 0) {
      i = getLastVisiblePosition();
    } 
    if (i > -1) {
      View view = getChildAt(i - getFirstVisiblePosition());
      if (view != null) {
        Rect rect = new Rect();
        int j = i;
        if (view.getGlobalVisibleRect(rect)) {
          j = view.getWidth();
          int k = view.getHeight();
          int m = rect.width(), n = rect.height();
          float f = (m * n) / (j * k);
          if (paramInt < 0 && f < 0.75F) {
            j = i + 1;
          } else {
            j = i;
            if (paramInt > 0) {
              j = i;
              if (f < 0.75F)
                j = i - 1; 
            } 
          } 
        } 
        smoothScrollToPosition(Math.max(0, Math.min(getCount(), j + paramInt)));
      } 
    } 
  }
  
  private void createScrollingCache() {
    if (this.mScrollingCacheEnabled && !this.mCachingStarted && !isHardwareAccelerated()) {
      setChildrenDrawnWithCacheEnabled(true);
      setChildrenDrawingCacheEnabled(true);
      this.mCachingActive = true;
      this.mCachingStarted = true;
    } 
  }
  
  private void clearScrollingCache() {
    if (!isHardwareAccelerated()) {
      if (this.mClearScrollingCache == null)
        this.mClearScrollingCache = (Runnable)new Object(this); 
      post(this.mClearScrollingCache);
    } 
  }
  
  public void scrollListBy(int paramInt) {
    trackMotionScroll(-paramInt, -paramInt);
  }
  
  public boolean canScrollList(int paramInt) {
    int i = getChildCount();
    null = false;
    boolean bool = false;
    if (i == 0)
      return false; 
    int j = this.mFirstPosition;
    Rect rect = this.mListPadding;
    if (paramInt > 0) {
      paramInt = getChildAt(i - 1).getBottom();
      if (j + i >= this.mItemCount) {
        null = bool;
        return (paramInt > getHeight() - rect.bottom) ? true : null;
      } 
    } else {
      paramInt = getChildAt(0).getTop();
      if (j > 0 || paramInt < rect.top)
        null = true; 
      return null;
    } 
    return true;
  }
  
  boolean trackMotionScroll(int paramInt1, int paramInt2) {
    View view;
    int i2, i3;
    boolean bool;
    int i = getChildCount();
    if (i == 0)
      return true; 
    int j = getChildAt(0).getTop();
    int k = getChildAt(i - 1).getBottom();
    Rect rect = this.mListPadding;
    int m = 0;
    int n = 0;
    if ((this.mGroupFlags & 0x22) == 34) {
      m = rect.top;
      n = rect.bottom;
    } 
    int i1 = getHeight() - n;
    n = getHeight() - this.mPaddingBottom - this.mPaddingTop;
    if (paramInt1 < 0) {
      i2 = Math.max(-(n - 1), paramInt1);
    } else {
      i2 = Math.min(n - 1, paramInt1);
    } 
    if (paramInt2 < 0) {
      i3 = Math.max(-(n - 1), paramInt2);
    } else {
      i3 = Math.min(n - 1, paramInt2);
    } 
    int i4 = this.mFirstPosition;
    if (i4 == 0) {
      this.mFirstPositionDistanceGuess = j - rect.top;
    } else {
      this.mFirstPositionDistanceGuess += i3;
    } 
    if (i4 + i == this.mItemCount) {
      this.mLastPositionDistanceGuess = rect.bottom + k;
    } else {
      this.mLastPositionDistanceGuess += i3;
    } 
    if (i4 == 0 && j >= rect.top && i3 >= 0) {
      paramInt1 = 1;
    } else {
      paramInt1 = 0;
    } 
    if (i4 + i == this.mItemCount && k <= getHeight() - rect.bottom && i3 <= 0) {
      paramInt2 = 1;
    } else {
      paramInt2 = 0;
    } 
    if (paramInt1 != 0 || paramInt2 != 0) {
      if (i3 != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    if (i3 < 0) {
      bool = true;
    } else {
      bool = false;
    } 
    boolean bool1 = isInTouchMode();
    if (bool1)
      hideSelector(); 
    int i5 = getHeaderViewsCount();
    int i6 = this.mItemCount - getFooterViewsCount();
    n = 0;
    paramInt1 = 0;
    int i7 = 0;
    if (bool) {
      paramInt1 = -i3;
      paramInt2 = paramInt1;
      if ((this.mGroupFlags & 0x22) == 34)
        paramInt2 = paramInt1 + rect.top; 
      for (int i8 = 0; i7 < i; i7++) {
        view = getChildAt(i7);
        if (view.getBottom() >= paramInt2)
          break; 
        paramInt1++;
        i8 = i4 + i7;
        if (i8 >= i5 && i8 < i6) {
          view.clearAccessibilityFocus();
          this.mRecycler.addScrapView(view, i8);
        } 
      } 
      n = 0;
    } else {
      paramInt2 = getHeight() - i3;
      i7 = paramInt2;
      if ((this.mGroupFlags & 0x22) == 34)
        i7 = paramInt2 - ((Rect)view).bottom; 
      for (paramInt2 = i - 1; paramInt2 >= 0; paramInt2--) {
        view = getChildAt(paramInt2);
        if (view.getTop() <= i7)
          break; 
        n = paramInt2;
        paramInt1++;
        int i8 = i4 + paramInt2;
        if (i8 >= i5 && i8 < i6) {
          view.clearAccessibilityFocus();
          this.mRecycler.addScrapView(view, i8);
        } 
      } 
    } 
    this.mMotionViewNewTop = this.mMotionViewOriginalTop + i2;
    this.mBlockLayoutRequests = true;
    if (paramInt1 > 0) {
      detachViewsFromParent(n, paramInt1);
      this.mRecycler.removeSkippedScrap();
    } 
    if (!awakenScrollBars())
      invalidate(); 
    offsetChildrenTopAndBottom(i3);
    if (bool)
      this.mFirstPosition += paramInt1; 
    paramInt1 = Math.abs(i3);
    if (m - j < paramInt1 || k - i1 < paramInt1)
      fillGap(bool); 
    this.mRecycler.fullyDetachScrapViews();
    paramInt2 = 0;
    paramInt1 = 0;
    if (!bool1 && this.mSelectedPosition != -1) {
      paramInt2 = this.mSelectedPosition - this.mFirstPosition;
      if (paramInt2 >= 0 && paramInt2 < getChildCount()) {
        positionSelector(this.mSelectedPosition, getChildAt(paramInt2));
        paramInt1 = 1;
      } 
    } else {
      n = this.mSelectorPosition;
      if (n != -1) {
        n -= this.mFirstPosition;
        paramInt1 = paramInt2;
        if (n >= 0) {
          paramInt1 = paramInt2;
          if (n < getChildCount()) {
            positionSelector(this.mSelectorPosition, getChildAt(n));
            paramInt1 = 1;
          } 
        } 
      } 
    } 
    if (paramInt1 == 0)
      this.mSelectorRect.setEmpty(); 
    this.mBlockLayoutRequests = false;
    invokeOnItemScrollListener();
    return false;
  }
  
  int getHeaderViewsCount() {
    return 0;
  }
  
  int getFooterViewsCount() {
    return 0;
  }
  
  void hideSelector() {
    if (this.mSelectedPosition != -1) {
      if (this.mLayoutMode != 4)
        this.mResurrectToPosition = this.mSelectedPosition; 
      if (this.mNextSelectedPosition >= 0 && this.mNextSelectedPosition != this.mSelectedPosition)
        this.mResurrectToPosition = this.mNextSelectedPosition; 
      setSelectedPositionInt(-1);
      setNextSelectedPositionInt(-1);
      this.mSelectedTop = 0;
    } 
  }
  
  int reconcileSelectedPosition() {
    int i = this.mSelectedPosition;
    int j = i;
    if (i < 0)
      j = this.mResurrectToPosition; 
    j = Math.max(0, j);
    j = Math.min(j, this.mItemCount - 1);
    return j;
  }
  
  int findClosestMotionRow(int paramInt) {
    int i = getChildCount();
    if (i == 0)
      return -1; 
    paramInt = findMotionRow(paramInt);
    if (paramInt == -1)
      paramInt = this.mFirstPosition + i - 1; 
    return paramInt;
  }
  
  public void invalidateViews() {
    this.mDataChanged = true;
    rememberSyncState();
    requestLayout();
    invalidate();
  }
  
  boolean resurrectSelectionIfNeeded() {
    if (this.mSelectedPosition < 0 && resurrectSelection()) {
      updateSelectorState();
      return true;
    } 
    return false;
  }
  
  boolean resurrectSelection() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getChildCount : ()I
    //   4: istore_1
    //   5: iconst_0
    //   6: istore_2
    //   7: iload_1
    //   8: ifgt -> 13
    //   11: iconst_0
    //   12: ireturn
    //   13: iconst_0
    //   14: istore_3
    //   15: iconst_0
    //   16: istore #4
    //   18: aload_0
    //   19: getfield mListPadding : Landroid/graphics/Rect;
    //   22: getfield top : I
    //   25: istore #5
    //   27: aload_0
    //   28: getfield mBottom : I
    //   31: aload_0
    //   32: getfield mTop : I
    //   35: isub
    //   36: aload_0
    //   37: getfield mListPadding : Landroid/graphics/Rect;
    //   40: getfield bottom : I
    //   43: isub
    //   44: istore #6
    //   46: aload_0
    //   47: getfield mFirstPosition : I
    //   50: istore #7
    //   52: aload_0
    //   53: getfield mResurrectToPosition : I
    //   56: istore #8
    //   58: iconst_1
    //   59: istore #9
    //   61: iload #8
    //   63: iload #7
    //   65: if_icmplt -> 150
    //   68: iload #8
    //   70: iload #7
    //   72: iload_1
    //   73: iadd
    //   74: if_icmpge -> 150
    //   77: aload_0
    //   78: iload #8
    //   80: aload_0
    //   81: getfield mFirstPosition : I
    //   84: isub
    //   85: invokevirtual getChildAt : (I)Landroid/view/View;
    //   88: astore #10
    //   90: aload #10
    //   92: invokevirtual getTop : ()I
    //   95: istore #4
    //   97: aload #10
    //   99: invokevirtual getBottom : ()I
    //   102: istore_3
    //   103: iload #4
    //   105: iload #5
    //   107: if_icmpge -> 122
    //   110: iload #5
    //   112: aload_0
    //   113: invokevirtual getVerticalFadingEdgeLength : ()I
    //   116: iadd
    //   117: istore #4
    //   119: goto -> 147
    //   122: iload_3
    //   123: iload #6
    //   125: if_icmple -> 147
    //   128: aload #10
    //   130: invokevirtual getMeasuredHeight : ()I
    //   133: istore #4
    //   135: iload #6
    //   137: iload #4
    //   139: isub
    //   140: aload_0
    //   141: invokevirtual getVerticalFadingEdgeLength : ()I
    //   144: isub
    //   145: istore #4
    //   147: goto -> 414
    //   150: iload #8
    //   152: iload #7
    //   154: if_icmpge -> 275
    //   157: iload #7
    //   159: istore_3
    //   160: iconst_0
    //   161: istore #11
    //   163: iload #4
    //   165: istore #8
    //   167: iload_3
    //   168: istore #6
    //   170: iload #11
    //   172: iload_1
    //   173: if_icmpge -> 264
    //   176: aload_0
    //   177: iload #11
    //   179: invokevirtual getChildAt : (I)Landroid/view/View;
    //   182: astore #10
    //   184: aload #10
    //   186: invokevirtual getTop : ()I
    //   189: istore #8
    //   191: iload #5
    //   193: istore #6
    //   195: iload #11
    //   197: ifne -> 237
    //   200: iload #8
    //   202: istore #12
    //   204: iload #7
    //   206: ifgt -> 224
    //   209: iload #12
    //   211: istore #4
    //   213: iload #5
    //   215: istore #6
    //   217: iload #8
    //   219: iload #5
    //   221: if_icmpge -> 237
    //   224: iload #5
    //   226: aload_0
    //   227: invokevirtual getVerticalFadingEdgeLength : ()I
    //   230: iadd
    //   231: istore #6
    //   233: iload #12
    //   235: istore #4
    //   237: iload #8
    //   239: iload #6
    //   241: if_icmplt -> 254
    //   244: iload #7
    //   246: iload #11
    //   248: iadd
    //   249: istore #6
    //   251: goto -> 264
    //   254: iinc #11, 1
    //   257: iload #6
    //   259: istore #5
    //   261: goto -> 163
    //   264: iload #8
    //   266: istore #4
    //   268: iload #6
    //   270: istore #8
    //   272: goto -> 414
    //   275: aload_0
    //   276: getfield mItemCount : I
    //   279: istore #12
    //   281: iconst_0
    //   282: istore #9
    //   284: iload_1
    //   285: iconst_1
    //   286: isub
    //   287: istore #8
    //   289: iload_3
    //   290: istore #4
    //   292: iload #8
    //   294: iflt -> 406
    //   297: aload_0
    //   298: iload #8
    //   300: invokevirtual getChildAt : (I)Landroid/view/View;
    //   303: astore #10
    //   305: aload #10
    //   307: invokevirtual getTop : ()I
    //   310: istore #11
    //   312: aload #10
    //   314: invokevirtual getBottom : ()I
    //   317: istore #13
    //   319: iload #4
    //   321: istore #5
    //   323: iload #6
    //   325: istore_3
    //   326: iload #8
    //   328: iload_1
    //   329: iconst_1
    //   330: isub
    //   331: if_icmpne -> 373
    //   334: iload #11
    //   336: istore #4
    //   338: iload #7
    //   340: iload_1
    //   341: iadd
    //   342: iload #12
    //   344: if_icmplt -> 361
    //   347: iload #4
    //   349: istore #5
    //   351: iload #6
    //   353: istore_3
    //   354: iload #13
    //   356: iload #6
    //   358: if_icmple -> 373
    //   361: iload #6
    //   363: aload_0
    //   364: invokevirtual getVerticalFadingEdgeLength : ()I
    //   367: isub
    //   368: istore_3
    //   369: iload #4
    //   371: istore #5
    //   373: iload #13
    //   375: iload_3
    //   376: if_icmpgt -> 393
    //   379: iload #11
    //   381: istore #4
    //   383: iload #7
    //   385: iload #8
    //   387: iadd
    //   388: istore #8
    //   390: goto -> 414
    //   393: iinc #8, -1
    //   396: iload #5
    //   398: istore #4
    //   400: iload_3
    //   401: istore #6
    //   403: goto -> 292
    //   406: iload #7
    //   408: iload_1
    //   409: iadd
    //   410: iconst_1
    //   411: isub
    //   412: istore #8
    //   414: aload_0
    //   415: iconst_m1
    //   416: putfield mResurrectToPosition : I
    //   419: aload_0
    //   420: aload_0
    //   421: getfield mFlingRunnable : Landroid/widget/AbsListView$FlingRunnable;
    //   424: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)Z
    //   427: pop
    //   428: aload_0
    //   429: getfield mPositionScroller : Landroid/widget/AbsListView$AbsPositionScroller;
    //   432: astore #10
    //   434: aload #10
    //   436: ifnull -> 444
    //   439: aload #10
    //   441: invokevirtual stop : ()V
    //   444: aload_0
    //   445: iconst_m1
    //   446: putfield mTouchMode : I
    //   449: aload_0
    //   450: invokespecial clearScrollingCache : ()V
    //   453: aload_0
    //   454: iload #4
    //   456: putfield mSpecificTop : I
    //   459: aload_0
    //   460: iload #8
    //   462: iload #9
    //   464: invokevirtual lookForSelectablePosition : (IZ)I
    //   467: istore #4
    //   469: iload #4
    //   471: iload #7
    //   473: if_icmplt -> 507
    //   476: iload #4
    //   478: aload_0
    //   479: invokevirtual getLastVisiblePosition : ()I
    //   482: if_icmpgt -> 507
    //   485: aload_0
    //   486: iconst_4
    //   487: putfield mLayoutMode : I
    //   490: aload_0
    //   491: invokevirtual updateSelectorState : ()V
    //   494: aload_0
    //   495: iload #4
    //   497: invokevirtual setSelectionInt : (I)V
    //   500: aload_0
    //   501: invokevirtual invokeOnItemScrollListener : ()V
    //   504: goto -> 510
    //   507: iconst_m1
    //   508: istore #4
    //   510: aload_0
    //   511: iconst_0
    //   512: invokevirtual reportScrollStateChange : (I)V
    //   515: iload_2
    //   516: istore #9
    //   518: iload #4
    //   520: iflt -> 526
    //   523: iconst_1
    //   524: istore #9
    //   526: iload #9
    //   528: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5646	-> 0
    //   #5648	-> 5
    //   #5649	-> 11
    //   #5652	-> 13
    //   #5654	-> 18
    //   #5655	-> 27
    //   #5656	-> 46
    //   #5657	-> 52
    //   #5658	-> 58
    //   #5660	-> 61
    //   #5661	-> 77
    //   #5663	-> 77
    //   #5664	-> 90
    //   #5665	-> 97
    //   #5668	-> 103
    //   #5669	-> 110
    //   #5670	-> 122
    //   #5671	-> 128
    //   #5672	-> 135
    //   #5674	-> 147
    //   #5675	-> 150
    //   #5677	-> 157
    //   #5678	-> 160
    //   #5679	-> 176
    //   #5680	-> 184
    //   #5682	-> 191
    //   #5684	-> 200
    //   #5686	-> 204
    //   #5689	-> 224
    //   #5692	-> 237
    //   #5694	-> 244
    //   #5695	-> 251
    //   #5696	-> 251
    //   #5678	-> 254
    //   #5700	-> 275
    //   #5701	-> 281
    //   #5702	-> 284
    //   #5704	-> 284
    //   #5705	-> 297
    //   #5706	-> 305
    //   #5707	-> 312
    //   #5709	-> 319
    //   #5710	-> 334
    //   #5711	-> 338
    //   #5712	-> 361
    //   #5716	-> 373
    //   #5717	-> 379
    //   #5718	-> 379
    //   #5719	-> 383
    //   #5704	-> 393
    //   #5725	-> 414
    //   #5726	-> 419
    //   #5727	-> 428
    //   #5728	-> 439
    //   #5730	-> 444
    //   #5731	-> 449
    //   #5732	-> 453
    //   #5733	-> 459
    //   #5734	-> 469
    //   #5735	-> 485
    //   #5736	-> 490
    //   #5737	-> 494
    //   #5738	-> 500
    //   #5740	-> 507
    //   #5742	-> 510
    //   #5744	-> 515
  }
  
  void confirmCheckedPositionsById() {
    this.mCheckStates.clear();
    boolean bool = false;
    for (byte b = 0; b < this.mCheckedIdStates.size(); b++) {
      long l1 = this.mCheckedIdStates.keyAt(b);
      int i = ((Integer)this.mCheckedIdStates.valueAt(b)).intValue();
      long l2 = this.mAdapter.getItemId(i);
      if (l1 != l2) {
        int j = Math.max(0, i - 20);
        int k = Math.min(i + 20, this.mItemCount);
        while (true) {
          if (j < k) {
            l2 = this.mAdapter.getItemId(j);
            if (l1 == l2) {
              this.mCheckStates.put(j, true);
              this.mCheckedIdStates.setValueAt(b, Integer.valueOf(j));
              j = 1;
              break;
            } 
            j++;
            continue;
          } 
          j = 0;
          break;
        } 
        if (j == 0) {
          this.mCheckedIdStates.delete(l1);
          b--;
          this.mCheckedItemCount--;
          bool = true;
          ActionMode actionMode = this.mChoiceActionMode;
          if (actionMode != null) {
            MultiChoiceModeWrapper multiChoiceModeWrapper = this.mMultiChoiceModeCallback;
            if (multiChoiceModeWrapper != null)
              multiChoiceModeWrapper.onItemCheckedStateChanged(actionMode, i, l1, false); 
          } 
        } 
      } else {
        this.mCheckStates.put(i, true);
      } 
    } 
    if (bool) {
      ActionMode actionMode = this.mChoiceActionMode;
      if (actionMode != null)
        actionMode.invalidate(); 
    } 
  }
  
  protected void handleDataChanged() {
    boolean bool;
    int i = this.mItemCount;
    int j = this.mLastHandledItemCount;
    this.mLastHandledItemCount = this.mItemCount;
    if (this.mChoiceMode != 0) {
      ListAdapter listAdapter = this.mAdapter;
      if (listAdapter != null && listAdapter.hasStableIds())
        confirmCheckedPositionsById(); 
    } 
    this.mRecycler.clearTransientStateViews();
    byte b = 3;
    if (i > 0) {
      if (this.mNeedSync) {
        this.mNeedSync = false;
        this.mPendingSync = null;
        bool = this.mTranscriptMode;
        if (bool == 2) {
          this.mLayoutMode = 3;
          return;
        } 
        if (bool == true) {
          if (this.mForceTranscriptScroll) {
            this.mForceTranscriptScroll = false;
            this.mLayoutMode = 3;
            return;
          } 
          int k = getChildCount();
          int m = getHeight() - getPaddingBottom();
          View view = getChildAt(k - 1);
          if (view != null) {
            bool = view.getBottom();
          } else {
            bool = m;
          } 
          if (this.mFirstPosition + k >= j && bool <= m) {
            this.mLayoutMode = 3;
            return;
          } 
          awakenScrollBars();
        } 
        bool = this.mSyncMode;
        if (bool != 0) {
          if (bool == true) {
            this.mLayoutMode = 5;
            this.mSyncPosition = Math.min(Math.max(0, this.mSyncPosition), i - 1);
            return;
          } 
        } else {
          if (isInTouchMode()) {
            this.mLayoutMode = 5;
            this.mSyncPosition = Math.min(Math.max(0, this.mSyncPosition), i - 1);
            return;
          } 
          int k = findSyncPosition();
          if (k >= 0) {
            bool = lookForSelectablePosition(k, true);
            if (bool == k) {
              this.mSyncPosition = k;
              if (this.mSyncHeight == getHeight()) {
                this.mLayoutMode = 5;
              } else {
                this.mLayoutMode = 2;
              } 
              setNextSelectedPositionInt(k);
              return;
            } 
          } 
        } 
      } 
      if (!isInTouchMode()) {
        int k = getSelectedItemPosition();
        bool = k;
        if (k >= i)
          bool = i - 1; 
        k = bool;
        if (bool < 0)
          k = 0; 
        bool = lookForSelectablePosition(k, true);
        if (bool >= 0) {
          setNextSelectedPositionInt(bool);
          return;
        } 
        bool = lookForSelectablePosition(k, false);
        if (bool >= 0) {
          setNextSelectedPositionInt(bool);
          return;
        } 
      } else if (this.mResurrectToPosition >= 0) {
        return;
      } 
    } 
    if (this.mStackFromBottom) {
      bool = b;
    } else {
      bool = true;
    } 
    this.mLayoutMode = bool;
    this.mSelectedPosition = -1;
    this.mSelectedRowId = Long.MIN_VALUE;
    this.mNextSelectedPosition = -1;
    this.mNextSelectedRowId = Long.MIN_VALUE;
    this.mNeedSync = false;
    this.mPendingSync = null;
    this.mSelectorPosition = -1;
    checkSelectionChanged();
  }
  
  protected void onDisplayHint(int paramInt) {
    boolean bool;
    super.onDisplayHint(paramInt);
    if (paramInt != 0) {
      if (paramInt == 4) {
        PopupWindow popupWindow = this.mPopup;
        if (popupWindow != null && popupWindow.isShowing())
          dismissPopup(); 
      } 
    } else if (this.mFiltered) {
      PopupWindow popupWindow = this.mPopup;
      if (popupWindow != null && !popupWindow.isShowing())
        showPopup(); 
    } 
    if (paramInt == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mPopupHidden = bool;
  }
  
  private void dismissPopup() {
    PopupWindow popupWindow = this.mPopup;
    if (popupWindow != null)
      popupWindow.dismiss(); 
  }
  
  private void showPopup() {
    if (getWindowVisibility() == 0) {
      createTextFilter(true);
      positionPopup();
      checkFocus();
    } 
  }
  
  private void positionPopup() {
    int i = (getResources().getDisplayMetrics()).heightPixels;
    int[] arrayOfInt = new int[2];
    getLocationOnScreen(arrayOfInt);
    i = i - arrayOfInt[1] - getHeight() + (int)(this.mDensityScale * 20.0F);
    if (!this.mPopup.isShowing()) {
      this.mPopup.showAtLocation(this, 81, arrayOfInt[0], i);
    } else {
      this.mPopup.update(arrayOfInt[0], i, -1, -1);
    } 
  }
  
  static int getDistance(Rect paramRect1, Rect paramRect2, int paramInt) {
    int i, j, k;
    if (paramInt != 1 && paramInt != 2) {
      if (paramInt != 17) {
        if (paramInt != 33) {
          if (paramInt != 66) {
            if (paramInt == 130) {
              paramInt = paramRect1.left + paramRect1.width() / 2;
              i = paramRect1.bottom;
              j = paramRect2.left + paramRect2.width() / 2;
              k = paramRect2.top;
            } else {
              throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}.");
            } 
          } else {
            paramInt = paramRect1.right;
            i = paramRect1.top + paramRect1.height() / 2;
            j = paramRect2.left;
            k = paramRect2.top + paramRect2.height() / 2;
          } 
        } else {
          paramInt = paramRect1.left + paramRect1.width() / 2;
          i = paramRect1.top;
          j = paramRect2.left + paramRect2.width() / 2;
          k = paramRect2.bottom;
        } 
      } else {
        paramInt = paramRect1.left;
        i = paramRect1.top + paramRect1.height() / 2;
        j = paramRect2.right;
        k = paramRect2.top + paramRect2.height() / 2;
      } 
    } else {
      paramInt = paramRect1.right + paramRect1.width() / 2;
      i = paramRect1.top + paramRect1.height() / 2;
      j = paramRect2.left + paramRect2.width() / 2;
      k = paramRect2.top + paramRect2.height() / 2;
    } 
    paramInt = j - paramInt;
    k -= i;
    return k * k + paramInt * paramInt;
  }
  
  protected boolean isInFilterMode() {
    return this.mFiltered;
  }
  
  boolean sendToTextFilter(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    if (!acceptFilter())
      return false; 
    boolean bool1 = false, bool2 = false;
    boolean bool = false;
    boolean bool3 = true;
    if (paramInt1 != 4) {
      if (paramInt1 != 62) {
        if (paramInt1 != 66 && paramInt1 != 160) {
          switch (paramInt1) {
            default:
              bool1 = bool3;
              break;
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
              bool1 = false;
              break;
          } 
        } else {
        
        } 
      } else {
        bool1 = this.mFiltered;
      } 
    } else {
      bool3 = bool1;
      if (this.mFiltered) {
        PopupWindow popupWindow = this.mPopup;
        bool3 = bool1;
        if (popupWindow != null) {
          bool3 = bool1;
          if (popupWindow.isShowing())
            if (paramKeyEvent.getAction() == 0 && paramKeyEvent.getRepeatCount() == 0) {
              KeyEvent.DispatcherState dispatcherState = getKeyDispatcherState();
              if (dispatcherState != null)
                dispatcherState.startTracking(paramKeyEvent, this); 
              bool3 = true;
            } else {
              bool3 = bool;
              if (paramKeyEvent.getAction() == 1) {
                bool3 = bool1;
                if (paramKeyEvent.isTracking()) {
                  bool3 = bool1;
                  if (!paramKeyEvent.isCanceled()) {
                    bool3 = true;
                    this.mTextFilter.setText("");
                  } 
                } 
              } 
            }  
        } 
      } 
      bool1 = false;
      bool2 = bool3;
    } 
    bool3 = bool2;
    if (bool1) {
      createTextFilter(true);
      KeyEvent keyEvent2 = paramKeyEvent;
      KeyEvent keyEvent1 = keyEvent2;
      if (keyEvent2.getRepeatCount() > 0)
        keyEvent1 = KeyEvent.changeTimeRepeat(paramKeyEvent, paramKeyEvent.getEventTime(), 0); 
      int i = paramKeyEvent.getAction();
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            bool3 = bool2;
          } else {
            bool3 = this.mTextFilter.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent);
          } 
        } else {
          bool3 = this.mTextFilter.onKeyUp(paramInt1, keyEvent1);
        } 
      } else {
        bool3 = this.mTextFilter.onKeyDown(paramInt1, keyEvent1);
      } 
    } 
    return bool3;
  }
  
  public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo) {
    if (isTextFilterEnabled()) {
      if (this.mPublicInputConnection == null) {
        this.mDefInputConnection = new BaseInputConnection(this, false);
        this.mPublicInputConnection = new InputConnectionWrapper(paramEditorInfo);
      } 
      paramEditorInfo.inputType = 177;
      paramEditorInfo.imeOptions = 6;
      return this.mPublicInputConnection;
    } 
    return null;
  }
  
  class InputConnectionWrapper implements InputConnection {
    private final EditorInfo mOutAttrs;
    
    private InputConnection mTarget;
    
    final AbsListView this$0;
    
    public InputConnectionWrapper(EditorInfo param1EditorInfo) {
      this.mOutAttrs = param1EditorInfo;
    }
    
    private InputConnection getTarget() {
      if (this.mTarget == null)
        this.mTarget = AbsListView.this.getTextFilterInput().onCreateInputConnection(this.mOutAttrs); 
      return this.mTarget;
    }
    
    public boolean reportFullscreenMode(boolean param1Boolean) {
      return AbsListView.this.mDefInputConnection.reportFullscreenMode(param1Boolean);
    }
    
    public boolean performEditorAction(int param1Int) {
      if (param1Int == 6) {
        AbsListView absListView = AbsListView.this;
        InputMethodManager inputMethodManager = (InputMethodManager)absListView.getContext().getSystemService(InputMethodManager.class);
        if (inputMethodManager != null)
          inputMethodManager.hideSoftInputFromWindow(AbsListView.this.getWindowToken(), 0); 
        return true;
      } 
      return false;
    }
    
    public boolean sendKeyEvent(KeyEvent param1KeyEvent) {
      return AbsListView.this.mDefInputConnection.sendKeyEvent(param1KeyEvent);
    }
    
    public CharSequence getTextBeforeCursor(int param1Int1, int param1Int2) {
      InputConnection inputConnection = this.mTarget;
      if (inputConnection == null)
        return ""; 
      return inputConnection.getTextBeforeCursor(param1Int1, param1Int2);
    }
    
    public CharSequence getTextAfterCursor(int param1Int1, int param1Int2) {
      InputConnection inputConnection = this.mTarget;
      if (inputConnection == null)
        return ""; 
      return inputConnection.getTextAfterCursor(param1Int1, param1Int2);
    }
    
    public CharSequence getSelectedText(int param1Int) {
      InputConnection inputConnection = this.mTarget;
      if (inputConnection == null)
        return ""; 
      return inputConnection.getSelectedText(param1Int);
    }
    
    public int getCursorCapsMode(int param1Int) {
      InputConnection inputConnection = this.mTarget;
      if (inputConnection == null)
        return 16384; 
      return inputConnection.getCursorCapsMode(param1Int);
    }
    
    public ExtractedText getExtractedText(ExtractedTextRequest param1ExtractedTextRequest, int param1Int) {
      return getTarget().getExtractedText(param1ExtractedTextRequest, param1Int);
    }
    
    public boolean deleteSurroundingText(int param1Int1, int param1Int2) {
      return getTarget().deleteSurroundingText(param1Int1, param1Int2);
    }
    
    public boolean deleteSurroundingTextInCodePoints(int param1Int1, int param1Int2) {
      return getTarget().deleteSurroundingTextInCodePoints(param1Int1, param1Int2);
    }
    
    public boolean setComposingText(CharSequence param1CharSequence, int param1Int) {
      return getTarget().setComposingText(param1CharSequence, param1Int);
    }
    
    public boolean setComposingRegion(int param1Int1, int param1Int2) {
      return getTarget().setComposingRegion(param1Int1, param1Int2);
    }
    
    public boolean finishComposingText() {
      InputConnection inputConnection = this.mTarget;
      return (inputConnection == null || inputConnection.finishComposingText());
    }
    
    public boolean commitText(CharSequence param1CharSequence, int param1Int) {
      return getTarget().commitText(param1CharSequence, param1Int);
    }
    
    public boolean commitCompletion(CompletionInfo param1CompletionInfo) {
      return getTarget().commitCompletion(param1CompletionInfo);
    }
    
    public boolean commitCorrection(CorrectionInfo param1CorrectionInfo) {
      return getTarget().commitCorrection(param1CorrectionInfo);
    }
    
    public boolean setSelection(int param1Int1, int param1Int2) {
      return getTarget().setSelection(param1Int1, param1Int2);
    }
    
    public boolean performContextMenuAction(int param1Int) {
      return getTarget().performContextMenuAction(param1Int);
    }
    
    public boolean beginBatchEdit() {
      return getTarget().beginBatchEdit();
    }
    
    public boolean endBatchEdit() {
      return getTarget().endBatchEdit();
    }
    
    public boolean clearMetaKeyStates(int param1Int) {
      return getTarget().clearMetaKeyStates(param1Int);
    }
    
    public boolean performPrivateCommand(String param1String, Bundle param1Bundle) {
      return getTarget().performPrivateCommand(param1String, param1Bundle);
    }
    
    public boolean requestCursorUpdates(int param1Int) {
      return getTarget().requestCursorUpdates(param1Int);
    }
    
    public Handler getHandler() {
      return getTarget().getHandler();
    }
    
    public void closeConnection() {
      getTarget().closeConnection();
    }
    
    public boolean commitContent(InputContentInfo param1InputContentInfo, int param1Int, Bundle param1Bundle) {
      return getTarget().commitContent(param1InputContentInfo, param1Int, param1Bundle);
    }
  }
  
  public boolean checkInputConnectionProxy(View paramView) {
    boolean bool;
    if (paramView == this.mTextFilter) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void createTextFilter(boolean paramBoolean) {
    if (this.mPopup == null) {
      PopupWindow popupWindow = new PopupWindow(getContext());
      popupWindow.setFocusable(false);
      popupWindow.setTouchable(false);
      popupWindow.setInputMethodMode(2);
      popupWindow.setContentView(getTextFilterInput());
      popupWindow.setWidth(-2);
      popupWindow.setHeight(-2);
      popupWindow.setBackgroundDrawable(null);
      this.mPopup = popupWindow;
      getViewTreeObserver().addOnGlobalLayoutListener(this);
      this.mGlobalLayoutListenerAddedFilter = true;
    } 
    if (paramBoolean) {
      this.mPopup.setAnimationStyle(16974601);
    } else {
      this.mPopup.setAnimationStyle(16974602);
    } 
  }
  
  private EditText getTextFilterInput() {
    if (this.mTextFilter == null) {
      LayoutInflater layoutInflater = LayoutInflater.from(getContext());
      EditText editText = (EditText)layoutInflater.inflate(17367344, (ViewGroup)null);
      editText.setRawInputType(177);
      this.mTextFilter.setImeOptions(268435456);
      this.mTextFilter.addTextChangedListener(this);
    } 
    return this.mTextFilter;
  }
  
  public void clearTextFilter() {
    if (this.mFiltered) {
      getTextFilterInput().setText("");
      this.mFiltered = false;
      PopupWindow popupWindow = this.mPopup;
      if (popupWindow != null && popupWindow.isShowing())
        dismissPopup(); 
    } 
  }
  
  public boolean hasTextFilter() {
    return this.mFiltered;
  }
  
  public void onGlobalLayout() {
    if (isShown()) {
      if (this.mFiltered) {
        PopupWindow popupWindow = this.mPopup;
        if (popupWindow != null && !popupWindow.isShowing() && !this.mPopupHidden)
          showPopup(); 
      } 
    } else {
      PopupWindow popupWindow = this.mPopup;
      if (popupWindow != null && popupWindow.isShowing())
        dismissPopup(); 
    } 
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    if (isTextFilterEnabled()) {
      createTextFilter(true);
      paramInt1 = paramCharSequence.length();
      boolean bool = this.mPopup.isShowing();
      if (!bool && paramInt1 > 0) {
        showPopup();
        this.mFiltered = true;
      } else if (bool && paramInt1 == 0) {
        dismissPopup();
        this.mFiltered = false;
      } 
      ListAdapter listAdapter = this.mAdapter;
      if (listAdapter instanceof Filterable) {
        Filter filter = ((Filterable)listAdapter).getFilter();
        if (filter != null) {
          filter.filter(paramCharSequence, this);
        } else {
          throw new IllegalStateException("You cannot call onTextChanged with a non filterable adapter");
        } 
      } 
    } 
  }
  
  public void afterTextChanged(Editable paramEditable) {}
  
  public void onFilterComplete(int paramInt) {
    if (this.mSelectedPosition < 0 && paramInt > 0) {
      this.mResurrectToPosition = -1;
      resurrectSelection();
    } 
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-1, -2, 0);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return new LayoutParams(paramLayoutParams);
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public void setTranscriptMode(int paramInt) {
    this.mTranscriptMode = paramInt;
  }
  
  public int getTranscriptMode() {
    return this.mTranscriptMode;
  }
  
  public int getSolidColor() {
    return this.mCacheColorHint;
  }
  
  public void setCacheColorHint(int paramInt) {
    if (paramInt != this.mCacheColorHint) {
      this.mCacheColorHint = paramInt;
      int i = getChildCount();
      for (byte b = 0; b < i; b++)
        getChildAt(b).setDrawingCacheBackgroundColor(paramInt); 
      this.mRecycler.setCacheColorHint(paramInt);
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public int getCacheColorHint() {
    return this.mCacheColorHint;
  }
  
  public void reclaimViews(List<View> paramList) {
    int i = getChildCount();
    RecyclerListener recyclerListener = this.mRecycler.mRecyclerListener;
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      if (layoutParams != null && this.mRecycler.shouldRecycleViewType(layoutParams.viewType)) {
        paramList.add(view);
        view.setAccessibilityDelegate((View.AccessibilityDelegate)null);
        if (recyclerListener != null)
          recyclerListener.onMovedToScrapHeap(view); 
      } 
    } 
    this.mRecycler.reclaimScrapViews(paramList);
    removeAllViewsInLayout();
  }
  
  private void finishGlows() {
    if (shouldDisplayEdgeEffects()) {
      this.mEdgeGlowTop.finish();
      this.mEdgeGlowBottom.finish();
    } 
  }
  
  public void setRemoteViewsAdapter(Intent paramIntent) {
    setRemoteViewsAdapter(paramIntent, false);
  }
  
  public Runnable setRemoteViewsAdapterAsync(Intent paramIntent) {
    return new RemoteViewsAdapter.AsyncRemoteAdapterAction(this, paramIntent);
  }
  
  public void setRemoteViewsAdapter(Intent paramIntent, boolean paramBoolean) {
    if (this.mRemoteAdapter != null) {
      Intent.FilterComparison filterComparison1 = new Intent.FilterComparison(paramIntent);
      RemoteViewsAdapter remoteViewsAdapter1 = this.mRemoteAdapter;
      Intent.FilterComparison filterComparison2 = new Intent.FilterComparison(remoteViewsAdapter1.getRemoteViewsServiceIntent());
      if (filterComparison1.equals(filterComparison2))
        return; 
    } 
    this.mDeferNotifyDataSetChanged = false;
    RemoteViewsAdapter remoteViewsAdapter = new RemoteViewsAdapter(getContext(), paramIntent, this, paramBoolean);
    if (remoteViewsAdapter.isDataReady())
      setAdapter(this.mRemoteAdapter); 
  }
  
  public void setRemoteViewsOnClickHandler(RemoteViews.OnClickHandler paramOnClickHandler) {
    RemoteViewsAdapter remoteViewsAdapter = this.mRemoteAdapter;
    if (remoteViewsAdapter != null)
      remoteViewsAdapter.setRemoteViewsOnClickHandler(paramOnClickHandler); 
  }
  
  public void deferNotifyDataSetChanged() {
    this.mDeferNotifyDataSetChanged = true;
  }
  
  public boolean onRemoteAdapterConnected() {
    RemoteViewsAdapter remoteViewsAdapter = this.mRemoteAdapter;
    if (remoteViewsAdapter != this.mAdapter) {
      setAdapter(remoteViewsAdapter);
      if (this.mDeferNotifyDataSetChanged) {
        this.mRemoteAdapter.notifyDataSetChanged();
        this.mDeferNotifyDataSetChanged = false;
      } 
      return false;
    } 
    if (remoteViewsAdapter != null) {
      remoteViewsAdapter.superNotifyDataSetChanged();
      return true;
    } 
    return false;
  }
  
  public void onRemoteAdapterDisconnected() {}
  
  void setVisibleRangeHint(int paramInt1, int paramInt2) {
    RemoteViewsAdapter remoteViewsAdapter = this.mRemoteAdapter;
    if (remoteViewsAdapter != null)
      remoteViewsAdapter.setVisibleRangeHint(paramInt1, paramInt2); 
  }
  
  public void setEdgeEffectColor(int paramInt) {
    setTopEdgeEffectColor(paramInt);
    setBottomEdgeEffectColor(paramInt);
  }
  
  public void setBottomEdgeEffectColor(int paramInt) {
    this.mEdgeGlowBottom.setColor(paramInt);
    invalidateBottomGlow();
  }
  
  public void setTopEdgeEffectColor(int paramInt) {
    this.mEdgeGlowTop.setColor(paramInt);
    invalidateTopGlow();
  }
  
  public int getTopEdgeEffectColor() {
    return this.mEdgeGlowTop.getColor();
  }
  
  public int getBottomEdgeEffectColor() {
    return this.mEdgeGlowBottom.getColor();
  }
  
  public void setRecyclerListener(RecyclerListener paramRecyclerListener) {
    RecycleBin.access$4302(this.mRecycler, paramRecyclerListener);
  }
  
  class AdapterDataSetObserver extends AdapterView<ListAdapter>.AdapterDataSetObserver {
    final AbsListView this$0;
    
    public void onChanged() {
      super.onChanged();
      if (AbsListView.this.mFastScroll != null)
        AbsListView.this.mFastScroll.onSectionsChanged(); 
    }
    
    public void onInvalidated() {
      super.onInvalidated();
      if (AbsListView.this.mFastScroll != null)
        AbsListView.this.mFastScroll.onSectionsChanged(); 
    }
  }
  
  class MultiChoiceModeWrapper implements MultiChoiceModeListener {
    private AbsListView.MultiChoiceModeListener mWrapped;
    
    final AbsListView this$0;
    
    public void setWrapped(AbsListView.MultiChoiceModeListener param1MultiChoiceModeListener) {
      this.mWrapped = param1MultiChoiceModeListener;
    }
    
    public boolean hasWrappedCallback() {
      boolean bool;
      if (this.mWrapped != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean onCreateActionMode(ActionMode param1ActionMode, Menu param1Menu) {
      if (this.mWrapped.onCreateActionMode(param1ActionMode, param1Menu)) {
        AbsListView.this.setLongClickable(false);
        return true;
      } 
      return false;
    }
    
    public boolean onPrepareActionMode(ActionMode param1ActionMode, Menu param1Menu) {
      return this.mWrapped.onPrepareActionMode(param1ActionMode, param1Menu);
    }
    
    public boolean onActionItemClicked(ActionMode param1ActionMode, MenuItem param1MenuItem) {
      return this.mWrapped.onActionItemClicked(param1ActionMode, param1MenuItem);
    }
    
    public void onDestroyActionMode(ActionMode param1ActionMode) {
      this.mWrapped.onDestroyActionMode(param1ActionMode);
      AbsListView.this.mChoiceActionMode = null;
      AbsListView.this.clearChoices();
      AbsListView.this.mDataChanged = true;
      AbsListView.this.rememberSyncState();
      AbsListView.this.requestLayout();
      AbsListView.this.setLongClickable(true);
    }
    
    public void onItemCheckedStateChanged(ActionMode param1ActionMode, int param1Int, long param1Long, boolean param1Boolean) {
      this.mWrapped.onItemCheckedStateChanged(param1ActionMode, param1Int, param1Long, param1Boolean);
      if (AbsListView.this.getCheckedItemCount() == 0)
        param1ActionMode.finish(); 
    }
  }
  
  class LayoutParams extends ViewGroup.LayoutParams {
    @ExportedProperty(category = "list")
    boolean forceAdd;
    
    boolean isEnabled;
    
    long itemId = -1L;
    
    @ExportedProperty(category = "list")
    boolean recycledHeaderFooter;
    
    int scrappedFromPosition;
    
    @ExportedProperty(category = "list", mapping = {@IntToString(from = -1, to = "ITEM_VIEW_TYPE_IGNORE"), @IntToString(from = -2, to = "ITEM_VIEW_TYPE_HEADER_OR_FOOTER")})
    int viewType;
    
    public LayoutParams(AbsListView this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(AbsListView this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(AbsListView this$0, int param1Int1, int param1Int2) {
      super(this$0, param1Int1);
      this.viewType = param1Int2;
    }
    
    public LayoutParams(AbsListView this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    protected void encodeProperties(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      super.encodeProperties(param1ViewHierarchyEncoder);
      param1ViewHierarchyEncoder.addProperty("list:viewType", this.viewType);
      param1ViewHierarchyEncoder.addProperty("list:recycledHeaderFooter", this.recycledHeaderFooter);
      param1ViewHierarchyEncoder.addProperty("list:forceAdd", this.forceAdd);
      param1ViewHierarchyEncoder.addProperty("list:isEnabled", this.isEnabled);
    }
  }
  
  class RecycleBin {
    private View[] mActiveViews = new View[0];
    
    private ArrayList<View> mCurrentScrap;
    
    private int mFirstActivePosition;
    
    private AbsListView.RecyclerListener mRecyclerListener;
    
    private ArrayList<View>[] mScrapViews;
    
    private ArrayList<View> mSkippedScrap;
    
    private SparseArray<View> mTransientStateViews;
    
    private LongSparseArray<View> mTransientStateViewsById;
    
    private int mViewTypeCount;
    
    final AbsListView this$0;
    
    public void setViewTypeCount(int param1Int) {
      if (param1Int >= 1) {
        ArrayList[] arrayOfArrayList = new ArrayList[param1Int];
        for (byte b = 0; b < param1Int; b++)
          arrayOfArrayList[b] = new ArrayList(); 
        this.mViewTypeCount = param1Int;
        this.mCurrentScrap = arrayOfArrayList[0];
        this.mScrapViews = (ArrayList<View>[])arrayOfArrayList;
        return;
      } 
      throw new IllegalArgumentException("Can't have a viewTypeCount < 1");
    }
    
    public void markChildrenDirty() {
      if (this.mViewTypeCount == 1) {
        ArrayList<View> arrayList = this.mCurrentScrap;
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((View)arrayList.get(b)).forceLayout(); 
      } else {
        int i = this.mViewTypeCount;
        for (byte b = 0; b < i; b++) {
          ArrayList<View> arrayList = this.mScrapViews[b];
          int j = arrayList.size();
          for (byte b1 = 0; b1 < j; b1++)
            ((View)arrayList.get(b1)).forceLayout(); 
        } 
      } 
      SparseArray<View> sparseArray = this.mTransientStateViews;
      if (sparseArray != null) {
        int i = sparseArray.size();
        for (byte b = 0; b < i; b++)
          ((View)this.mTransientStateViews.valueAt(b)).forceLayout(); 
      } 
      LongSparseArray<View> longSparseArray = this.mTransientStateViewsById;
      if (longSparseArray != null) {
        int i = longSparseArray.size();
        for (byte b = 0; b < i; b++)
          ((View)this.mTransientStateViewsById.valueAt(b)).forceLayout(); 
      } 
    }
    
    public boolean shouldRecycleViewType(int param1Int) {
      boolean bool;
      if (param1Int >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void clear() {
      if (this.mViewTypeCount == 1) {
        ArrayList<View> arrayList = this.mCurrentScrap;
        clearScrap(arrayList);
      } else {
        int i = this.mViewTypeCount;
        for (byte b = 0; b < i; b++) {
          ArrayList<View> arrayList = this.mScrapViews[b];
          clearScrap(arrayList);
        } 
      } 
      clearTransientStateViews();
    }
    
    void fillActiveViews(int param1Int1, int param1Int2) {
      if (this.mActiveViews.length < param1Int1)
        this.mActiveViews = new View[param1Int1]; 
      this.mFirstActivePosition = param1Int2;
      View[] arrayOfView = this.mActiveViews;
      for (byte b = 0; b < param1Int1; b++) {
        View view = AbsListView.this.getChildAt(b);
        AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)view.getLayoutParams();
        if (layoutParams != null && layoutParams.viewType != -2) {
          arrayOfView[b] = view;
          layoutParams.scrappedFromPosition = param1Int2 + b;
        } 
      } 
    }
    
    View getActiveView(int param1Int) {
      param1Int -= this.mFirstActivePosition;
      View[] arrayOfView = this.mActiveViews;
      if (param1Int >= 0 && param1Int < arrayOfView.length) {
        View view = arrayOfView[param1Int];
        arrayOfView[param1Int] = null;
        return view;
      } 
      return null;
    }
    
    View getTransientStateView(int param1Int) {
      if (AbsListView.this.mAdapter != null && AbsListView.this.mAdapterHasStableIds && this.mTransientStateViewsById != null) {
        long l = AbsListView.this.mAdapter.getItemId(param1Int);
        View view = this.mTransientStateViewsById.get(l);
        this.mTransientStateViewsById.remove(l);
        return view;
      } 
      SparseArray<View> sparseArray = this.mTransientStateViews;
      if (sparseArray != null) {
        param1Int = sparseArray.indexOfKey(param1Int);
        if (param1Int >= 0) {
          View view = this.mTransientStateViews.valueAt(param1Int);
          this.mTransientStateViews.removeAt(param1Int);
          return view;
        } 
      } 
      return null;
    }
    
    void clearTransientStateViews() {
      SparseArray<View> sparseArray = this.mTransientStateViews;
      if (sparseArray != null) {
        int i = sparseArray.size();
        for (byte b = 0; b < i; b++)
          removeDetachedView(sparseArray.valueAt(b), false); 
        sparseArray.clear();
      } 
      LongSparseArray<View> longSparseArray = this.mTransientStateViewsById;
      if (longSparseArray != null) {
        int i = longSparseArray.size();
        for (byte b = 0; b < i; b++)
          removeDetachedView(longSparseArray.valueAt(b), false); 
        longSparseArray.clear();
      } 
    }
    
    View getScrapView(int param1Int) {
      int i = AbsListView.this.mAdapter.getItemViewType(param1Int);
      if (i < 0)
        return null; 
      if (this.mViewTypeCount == 1)
        return retrieveFromScrap(this.mCurrentScrap, param1Int); 
      ArrayList<View>[] arrayOfArrayList = this.mScrapViews;
      if (i < arrayOfArrayList.length)
        return retrieveFromScrap(arrayOfArrayList[i], param1Int); 
      return null;
    }
    
    void addScrapView(View param1View, int param1Int) {
      AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)param1View.getLayoutParams();
      if (layoutParams == null)
        return; 
      layoutParams.scrappedFromPosition = param1Int;
      int i = layoutParams.viewType;
      if (!shouldRecycleViewType(i)) {
        if (i != -2)
          getSkippedScrap().add(param1View); 
        return;
      } 
      param1View.dispatchStartTemporaryDetach();
      AbsListView.this.notifyViewAccessibilityStateChangedIfNeeded(1);
      boolean bool = param1View.hasTransientState();
      if (bool) {
        if (AbsListView.this.mAdapter != null && AbsListView.this.mAdapterHasStableIds) {
          if (this.mTransientStateViewsById == null)
            this.mTransientStateViewsById = new LongSparseArray<>(); 
          this.mTransientStateViewsById.put(layoutParams.itemId, param1View);
        } else if (!AbsListView.this.mDataChanged) {
          if (this.mTransientStateViews == null)
            this.mTransientStateViews = new SparseArray<>(); 
          this.mTransientStateViews.put(param1Int, param1View);
        } else {
          clearScrapForRebind(param1View);
          getSkippedScrap().add(param1View);
        } 
      } else {
        clearScrapForRebind(param1View);
        if (this.mViewTypeCount == 1) {
          this.mCurrentScrap.add(param1View);
        } else {
          this.mScrapViews[i].add(param1View);
        } 
        AbsListView.RecyclerListener recyclerListener = this.mRecyclerListener;
        if (recyclerListener != null)
          recyclerListener.onMovedToScrapHeap(param1View); 
      } 
    }
    
    private ArrayList<View> getSkippedScrap() {
      if (this.mSkippedScrap == null)
        this.mSkippedScrap = new ArrayList<>(); 
      return this.mSkippedScrap;
    }
    
    void removeSkippedScrap() {
      ArrayList<View> arrayList = this.mSkippedScrap;
      if (arrayList == null)
        return; 
      int i = arrayList.size();
      for (byte b = 0; b < i; b++)
        removeDetachedView(this.mSkippedScrap.get(b), false); 
      this.mSkippedScrap.clear();
    }
    
    void scrapActiveViews() {
      boolean bool2;
      View[] arrayOfView = this.mActiveViews;
      AbsListView.RecyclerListener recyclerListener = this.mRecyclerListener;
      boolean bool1 = true;
      if (recyclerListener != null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (this.mViewTypeCount <= 1)
        bool1 = false; 
      ArrayList<View> arrayList = this.mCurrentScrap;
      int i = arrayOfView.length;
      for (; --i >= 0; i--, arrayList = arrayList1) {
        View view = arrayOfView[i];
        ArrayList<View> arrayList1 = arrayList;
        if (view != null) {
          AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)view.getLayoutParams();
          int j = layoutParams.viewType;
          arrayOfView[i] = null;
          if (view.hasTransientState()) {
            view.dispatchStartTemporaryDetach();
            if (AbsListView.this.mAdapter != null && AbsListView.this.mAdapterHasStableIds) {
              if (this.mTransientStateViewsById == null)
                this.mTransientStateViewsById = new LongSparseArray<>(); 
              long l = AbsListView.this.mAdapter.getItemId(this.mFirstActivePosition + i);
              this.mTransientStateViewsById.put(l, view);
              ArrayList<View> arrayList2 = arrayList;
            } else if (!AbsListView.this.mDataChanged) {
              if (this.mTransientStateViews == null)
                this.mTransientStateViews = new SparseArray<>(); 
              this.mTransientStateViews.put(this.mFirstActivePosition + i, view);
              ArrayList<View> arrayList2 = arrayList;
            } else {
              ArrayList<View> arrayList2 = arrayList;
              if (j != -2) {
                removeDetachedView(view, false);
                arrayList2 = arrayList;
              } 
            } 
          } else if (!shouldRecycleViewType(j)) {
            arrayList1 = arrayList;
            if (j != -2) {
              removeDetachedView(view, false);
              arrayList1 = arrayList;
            } 
          } else {
            if (bool1)
              arrayList = this.mScrapViews[j]; 
            ((AbsListView.LayoutParams)arrayList1).scrappedFromPosition = this.mFirstActivePosition + i;
            removeDetachedView(view, false);
            arrayList.add(view);
            arrayList1 = arrayList;
            if (bool2) {
              this.mRecyclerListener.onMovedToScrapHeap(view);
              arrayList1 = arrayList;
            } 
          } 
        } 
      } 
      pruneScrapViews();
    }
    
    void fullyDetachScrapViews() {
      int i = this.mViewTypeCount;
      ArrayList<View>[] arrayOfArrayList = this.mScrapViews;
      for (byte b = 0; b < i; b++) {
        ArrayList<View> arrayList = arrayOfArrayList[b];
        for (int j = arrayList.size() - 1; j >= 0; j--) {
          View view = arrayList.get(j);
          if (view.isTemporarilyDetached())
            removeDetachedView(view, false); 
        } 
      } 
    }
    
    private void pruneScrapViews() {
      int i = this.mActiveViews.length;
      int j = this.mViewTypeCount;
      ArrayList<View>[] arrayOfArrayList = this.mScrapViews;
      int k;
      for (k = 0; k < j; k++) {
        ArrayList<View> arrayList = arrayOfArrayList[k];
        int m = arrayList.size();
        while (m > i)
          arrayList.remove(--m); 
      } 
      SparseArray<View> sparseArray = this.mTransientStateViews;
      if (sparseArray != null)
        for (k = 0; k < sparseArray.size(); k = m + 1) {
          View view = sparseArray.valueAt(k);
          int m = k;
          if (!view.hasTransientState()) {
            removeDetachedView(view, false);
            sparseArray.removeAt(k);
            m = k - 1;
          } 
        }  
      LongSparseArray<View> longSparseArray = this.mTransientStateViewsById;
      if (longSparseArray != null)
        for (k = 0; k < longSparseArray.size(); k = m + 1) {
          View view = longSparseArray.valueAt(k);
          int m = k;
          if (!view.hasTransientState()) {
            removeDetachedView(view, false);
            longSparseArray.removeAt(k);
            m = k - 1;
          } 
        }  
    }
    
    void reclaimScrapViews(List<View> param1List) {
      if (this.mViewTypeCount == 1) {
        param1List.addAll(this.mCurrentScrap);
      } else {
        int i = this.mViewTypeCount;
        ArrayList<View>[] arrayOfArrayList = this.mScrapViews;
        for (byte b = 0; b < i; b++) {
          ArrayList<View> arrayList = arrayOfArrayList[b];
          param1List.addAll(arrayList);
        } 
      } 
    }
    
    void setCacheColorHint(int param1Int) {
      if (this.mViewTypeCount == 1) {
        ArrayList<View> arrayList = this.mCurrentScrap;
        int j = arrayList.size();
        for (byte b1 = 0; b1 < j; b1++)
          ((View)arrayList.get(b1)).setDrawingCacheBackgroundColor(param1Int); 
      } else {
        int j = this.mViewTypeCount;
        for (byte b1 = 0; b1 < j; b1++) {
          ArrayList<View> arrayList = this.mScrapViews[b1];
          int k = arrayList.size();
          for (byte b2 = 0; b2 < k; b2++)
            ((View)arrayList.get(b2)).setDrawingCacheBackgroundColor(param1Int); 
        } 
      } 
      View[] arrayOfView = this.mActiveViews;
      int i = arrayOfView.length;
      for (byte b = 0; b < i; b++) {
        View view = arrayOfView[b];
        if (view != null)
          view.setDrawingCacheBackgroundColor(param1Int); 
      } 
    }
    
    private View retrieveFromScrap(ArrayList<View> param1ArrayList, int param1Int) {
      int i = param1ArrayList.size();
      if (i > 0) {
        for (int j = i - 1; j >= 0; j--) {
          View view1 = param1ArrayList.get(j);
          AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)view1.getLayoutParams();
          if (AbsListView.this.mAdapterHasStableIds) {
            long l = AbsListView.this.mAdapter.getItemId(param1Int);
            if (l == layoutParams.itemId)
              return param1ArrayList.remove(j); 
          } else if (layoutParams.scrappedFromPosition == param1Int) {
            view = param1ArrayList.remove(j);
            clearScrapForRebind(view);
            return view;
          } 
        } 
        View view = view.remove(i - 1);
        clearScrapForRebind(view);
        return view;
      } 
      return null;
    }
    
    private void clearScrap(ArrayList<View> param1ArrayList) {
      int i = param1ArrayList.size();
      for (byte b = 0; b < i; b++)
        removeDetachedView(param1ArrayList.remove(i - 1 - b), false); 
    }
    
    private void clearScrapForRebind(View param1View) {
      param1View.clearAccessibilityFocus();
      param1View.setAccessibilityDelegate((View.AccessibilityDelegate)null);
    }
    
    private void removeDetachedView(View param1View, boolean param1Boolean) {
      param1View.setAccessibilityDelegate((View.AccessibilityDelegate)null);
      AbsListView.this.removeDetachedView(param1View, param1Boolean);
    }
  }
  
  int getHeightForPosition(int paramInt) {
    int i = getFirstVisiblePosition();
    int j = getChildCount();
    i = paramInt - i;
    if (i >= 0 && i < j) {
      View view1 = getChildAt(i);
      return view1.getHeight();
    } 
    View view = obtainView(paramInt, this.mIsScrap);
    view.measure(this.mWidthMeasureSpec, 0);
    j = view.getMeasuredHeight();
    this.mRecycler.addScrapView(view, paramInt);
    return j;
  }
  
  public void setSelectionFromTop(int paramInt1, int paramInt2) {
    if (this.mAdapter == null)
      return; 
    if (!isInTouchMode()) {
      int i = lookForSelectablePosition(paramInt1, true);
      paramInt1 = i;
      if (i >= 0) {
        setNextSelectedPositionInt(i);
        paramInt1 = i;
      } 
    } else {
      this.mResurrectToPosition = paramInt1;
    } 
    if (paramInt1 >= 0) {
      this.mLayoutMode = 4;
      this.mSpecificTop = this.mListPadding.top + paramInt2;
      if (this.mNeedSync) {
        this.mSyncPosition = paramInt1;
        this.mSyncRowId = this.mAdapter.getItemId(paramInt1);
      } 
      AbsPositionScroller absPositionScroller = this.mPositionScroller;
      if (absPositionScroller != null)
        absPositionScroller.stop(); 
      requestLayout();
    } 
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("drawing:cacheColorHint", getCacheColorHint());
    paramViewHierarchyEncoder.addProperty("list:fastScrollEnabled", isFastScrollEnabled());
    paramViewHierarchyEncoder.addProperty("list:scrollingCacheEnabled", isScrollingCacheEnabled());
    paramViewHierarchyEncoder.addProperty("list:smoothScrollbarEnabled", isSmoothScrollbarEnabled());
    paramViewHierarchyEncoder.addProperty("list:stackFromBottom", isStackFromBottom());
    paramViewHierarchyEncoder.addProperty("list:textFilterEnabled", isTextFilterEnabled());
    View view = getSelectedView();
    if (view != null) {
      paramViewHierarchyEncoder.addPropertyKey("selectedView");
      view.encode(paramViewHierarchyEncoder);
    } 
  }
  
  class AbsPositionScroller {
    public abstract void start(int param1Int);
    
    public abstract void start(int param1Int1, int param1Int2);
    
    public abstract void startWithOffset(int param1Int1, int param1Int2);
    
    public abstract void startWithOffset(int param1Int1, int param1Int2, int param1Int3);
    
    public abstract void stop();
  }
  
  class PositionScroller extends AbsPositionScroller implements Runnable {
    private static final int MOVE_DOWN_BOUND = 3;
    
    private static final int MOVE_DOWN_POS = 1;
    
    private static final int MOVE_OFFSET = 5;
    
    private static final int MOVE_UP_BOUND = 4;
    
    private static final int MOVE_UP_POS = 2;
    
    private static final int SCROLL_DURATION = 200;
    
    private int mBoundPos;
    
    private final int mExtraScroll;
    
    private int mLastSeenPos;
    
    private int mMode;
    
    private int mOffsetFromTop;
    
    private int mScrollDuration;
    
    private int mTargetPos;
    
    final AbsListView this$0;
    
    PositionScroller() {
      this.mExtraScroll = ViewConfiguration.get(AbsListView.this.mContext).getScaledFadingEdgeLength();
    }
    
    public void start(int param1Int) {
      stop();
      if (AbsListView.this.mDataChanged) {
        AbsListView.this.mPositionScrollAfterLayout = (Runnable)new Object(this, param1Int);
        return;
      } 
      int i = AbsListView.this.getChildCount();
      if (i == 0)
        return; 
      int j = AbsListView.this.mFirstPosition;
      int k = j + i - 1;
      i = Math.max(0, Math.min(AbsListView.this.getCount() - 1, param1Int));
      if (i < j) {
        param1Int = j - i + 1;
        this.mMode = 2;
      } else if (i > k) {
        param1Int = i - k + 1;
        this.mMode = 1;
      } else {
        scrollToVisible(i, -1, 200);
        return;
      } 
      if (param1Int > 0) {
        this.mScrollDuration = 200 / param1Int;
      } else {
        this.mScrollDuration = 200;
      } 
      this.mTargetPos = i;
      this.mBoundPos = -1;
      this.mLastSeenPos = -1;
      AbsListView.this.postOnAnimation(this);
    }
    
    public void start(int param1Int1, int param1Int2) {
      stop();
      if (param1Int2 == -1) {
        start(param1Int1);
        return;
      } 
      if (AbsListView.this.mDataChanged) {
        AbsListView.this.mPositionScrollAfterLayout = (Runnable)new Object(this, param1Int1, param1Int2);
        return;
      } 
      int i = AbsListView.this.getChildCount();
      if (i == 0)
        return; 
      int j = AbsListView.this.mFirstPosition;
      int k = j + i - 1;
      i = Math.max(0, Math.min(AbsListView.this.getCount() - 1, param1Int1));
      if (i < j) {
        k -= param1Int2;
        if (k < 1)
          return; 
        param1Int1 = j - i + 1;
        j = k - 1;
        if (j < param1Int1) {
          param1Int1 = j;
          this.mMode = 4;
        } else {
          this.mMode = 2;
        } 
      } else if (i > k) {
        j = param1Int2 - j;
        if (j < 1)
          return; 
        param1Int1 = i - k + 1;
        j--;
        if (j < param1Int1) {
          this.mMode = 3;
          param1Int1 = j;
        } else {
          this.mMode = 1;
        } 
      } else {
        scrollToVisible(i, param1Int2, 200);
        return;
      } 
      if (param1Int1 > 0) {
        this.mScrollDuration = 200 / param1Int1;
      } else {
        this.mScrollDuration = 200;
      } 
      this.mTargetPos = i;
      this.mBoundPos = param1Int2;
      this.mLastSeenPos = -1;
      AbsListView.this.postOnAnimation(this);
    }
    
    public void startWithOffset(int param1Int1, int param1Int2) {
      startWithOffset(param1Int1, param1Int2, 200);
    }
    
    public void startWithOffset(int param1Int1, int param1Int2, int param1Int3) {
      stop();
      if (AbsListView.this.mDataChanged) {
        AbsListView.this.mPositionScrollAfterLayout = (Runnable)new Object(this, param1Int1, param1Int2, param1Int3);
        return;
      } 
      int i = AbsListView.this.getChildCount();
      if (i == 0)
        return; 
      param1Int2 += AbsListView.this.getPaddingTop();
      this.mTargetPos = Math.max(0, Math.min(AbsListView.this.getCount() - 1, param1Int1));
      this.mOffsetFromTop = param1Int2;
      this.mBoundPos = -1;
      this.mLastSeenPos = -1;
      this.mMode = 5;
      int j = AbsListView.this.mFirstPosition;
      param1Int1 = j + i - 1;
      int k = this.mTargetPos;
      if (k < j) {
        param1Int1 = j - k;
      } else if (k > param1Int1) {
        param1Int1 = k - param1Int1;
      } else {
        param1Int1 = AbsListView.this.getChildAt(k - j).getTop();
        AbsListView.this.smoothScrollBy(param1Int1 - param1Int2, param1Int3, true, false);
        return;
      } 
      float f = param1Int1 / i;
      if (f < 1.0F) {
        param1Int1 = param1Int3;
      } else {
        param1Int1 = (int)(param1Int3 / f);
      } 
      this.mScrollDuration = param1Int1;
      this.mLastSeenPos = -1;
      AbsListView.this.postOnAnimation(this);
    }
    
    private void scrollToVisible(int param1Int1, int param1Int2, int param1Int3) {
      // Byte code:
      //   0: iload_2
      //   1: istore #4
      //   3: aload_0
      //   4: getfield this$0 : Landroid/widget/AbsListView;
      //   7: getfield mFirstPosition : I
      //   10: istore #5
      //   12: aload_0
      //   13: getfield this$0 : Landroid/widget/AbsListView;
      //   16: invokevirtual getChildCount : ()I
      //   19: istore_2
      //   20: iload #5
      //   22: iload_2
      //   23: iadd
      //   24: iconst_1
      //   25: isub
      //   26: istore #6
      //   28: aload_0
      //   29: getfield this$0 : Landroid/widget/AbsListView;
      //   32: getfield mListPadding : Landroid/graphics/Rect;
      //   35: getfield top : I
      //   38: istore #7
      //   40: aload_0
      //   41: getfield this$0 : Landroid/widget/AbsListView;
      //   44: invokevirtual getHeight : ()I
      //   47: aload_0
      //   48: getfield this$0 : Landroid/widget/AbsListView;
      //   51: getfield mListPadding : Landroid/graphics/Rect;
      //   54: getfield bottom : I
      //   57: isub
      //   58: istore #8
      //   60: iload_1
      //   61: iload #5
      //   63: if_icmplt -> 72
      //   66: iload_1
      //   67: iload #6
      //   69: if_icmple -> 147
      //   72: new java/lang/StringBuilder
      //   75: dup
      //   76: invokespecial <init> : ()V
      //   79: astore #9
      //   81: aload #9
      //   83: ldc 'scrollToVisible called with targetPos '
      //   85: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   88: pop
      //   89: aload #9
      //   91: iload_1
      //   92: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   95: pop
      //   96: aload #9
      //   98: ldc ' not visible ['
      //   100: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   103: pop
      //   104: aload #9
      //   106: iload #5
      //   108: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   111: pop
      //   112: aload #9
      //   114: ldc ', '
      //   116: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   119: pop
      //   120: aload #9
      //   122: iload #6
      //   124: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   127: pop
      //   128: aload #9
      //   130: ldc ']'
      //   132: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   135: pop
      //   136: ldc 'AbsListView'
      //   138: aload #9
      //   140: invokevirtual toString : ()Ljava/lang/String;
      //   143: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   146: pop
      //   147: iload #4
      //   149: iload #5
      //   151: if_icmplt -> 164
      //   154: iload #4
      //   156: istore_2
      //   157: iload #4
      //   159: iload #6
      //   161: if_icmple -> 166
      //   164: iconst_m1
      //   165: istore_2
      //   166: aload_0
      //   167: getfield this$0 : Landroid/widget/AbsListView;
      //   170: iload_1
      //   171: iload #5
      //   173: isub
      //   174: invokevirtual getChildAt : (I)Landroid/view/View;
      //   177: astore #9
      //   179: aload #9
      //   181: invokevirtual getTop : ()I
      //   184: istore #4
      //   186: aload #9
      //   188: invokevirtual getBottom : ()I
      //   191: istore #6
      //   193: iconst_0
      //   194: istore_1
      //   195: iload #6
      //   197: iload #8
      //   199: if_icmple -> 208
      //   202: iload #6
      //   204: iload #8
      //   206: isub
      //   207: istore_1
      //   208: iload #4
      //   210: iload #7
      //   212: if_icmpge -> 221
      //   215: iload #4
      //   217: iload #7
      //   219: isub
      //   220: istore_1
      //   221: iload_1
      //   222: ifne -> 226
      //   225: return
      //   226: iload_2
      //   227: iflt -> 318
      //   230: aload_0
      //   231: getfield this$0 : Landroid/widget/AbsListView;
      //   234: iload_2
      //   235: iload #5
      //   237: isub
      //   238: invokevirtual getChildAt : (I)Landroid/view/View;
      //   241: astore #9
      //   243: aload #9
      //   245: invokevirtual getTop : ()I
      //   248: istore #5
      //   250: aload #9
      //   252: invokevirtual getBottom : ()I
      //   255: istore_2
      //   256: iload_1
      //   257: invokestatic abs : (I)I
      //   260: istore #4
      //   262: iload_1
      //   263: ifge -> 287
      //   266: iload_2
      //   267: iload #4
      //   269: iadd
      //   270: iload #8
      //   272: if_icmple -> 287
      //   275: iconst_0
      //   276: iload_2
      //   277: iload #8
      //   279: isub
      //   280: invokestatic max : (II)I
      //   283: istore_2
      //   284: goto -> 320
      //   287: iload_1
      //   288: istore_2
      //   289: iload_1
      //   290: ifle -> 320
      //   293: iload_1
      //   294: istore_2
      //   295: iload #5
      //   297: iload #4
      //   299: isub
      //   300: iload #7
      //   302: if_icmpge -> 320
      //   305: iconst_0
      //   306: iload #5
      //   308: iload #7
      //   310: isub
      //   311: invokestatic min : (II)I
      //   314: istore_2
      //   315: goto -> 320
      //   318: iload_1
      //   319: istore_2
      //   320: aload_0
      //   321: getfield this$0 : Landroid/widget/AbsListView;
      //   324: iload_2
      //   325: iload_3
      //   326: invokevirtual smoothScrollBy : (II)V
      //   329: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #7792	-> 0
      //   #7793	-> 12
      //   #7794	-> 20
      //   #7795	-> 28
      //   #7796	-> 40
      //   #7798	-> 60
      //   #7799	-> 72
      //   #7802	-> 147
      //   #7804	-> 164
      //   #7807	-> 166
      //   #7808	-> 179
      //   #7809	-> 186
      //   #7810	-> 193
      //   #7812	-> 195
      //   #7813	-> 202
      //   #7815	-> 208
      //   #7816	-> 215
      //   #7819	-> 221
      //   #7820	-> 225
      //   #7823	-> 226
      //   #7824	-> 230
      //   #7825	-> 243
      //   #7826	-> 250
      //   #7827	-> 256
      //   #7829	-> 262
      //   #7831	-> 275
      //   #7829	-> 287
      //   #7832	-> 287
      //   #7834	-> 305
      //   #7823	-> 318
      //   #7838	-> 320
      //   #7839	-> 329
    }
    
    public void stop() {
      AbsListView.this.removeCallbacks(this);
    }
    
    public void run() {
      int i = AbsListView.this.getHeight();
      int j = AbsListView.this.mFirstPosition;
      int k = this.mMode;
      boolean bool = false;
      if (k != 1) {
        if (k != 2) {
          if (k != 3) {
            if (k != 4) {
              if (k == 5) {
                float f2;
                if (this.mLastSeenPos == j) {
                  AbsListView.this.postOnAnimation(this);
                  return;
                } 
                this.mLastSeenPos = j;
                int m = AbsListView.this.getChildCount();
                if (m <= 0)
                  return; 
                int n = this.mTargetPos;
                int i1 = j + m - 1;
                View view1 = AbsListView.this.getChildAt(0);
                i = view1.getHeight();
                View view2 = AbsListView.this.getChildAt(m - 1);
                k = view2.getHeight();
                if (i == 0.0F) {
                  f1 = 1.0F;
                } else {
                  f1 = (view1.getTop() + i) / i;
                } 
                if (k == 0.0F) {
                  f2 = 1.0F;
                } else {
                  view1 = AbsListView.this;
                  f2 = (view1.getHeight() + k - view2.getBottom()) / k;
                } 
                float f3 = 0.0F;
                if (n < j) {
                  f1 = (j - n) + 1.0F - f1 + 1.0F;
                } else {
                  f1 = f3;
                  if (n > i1)
                    f1 = (n - i1) + 1.0F - f2; 
                } 
                f1 /= m;
                float f1 = Math.min(Math.abs(f1), 1.0F);
                if (n < j) {
                  k = (int)(-AbsListView.this.getHeight() * f1);
                  j = (int)(this.mScrollDuration * f1);
                  AbsListView.this.smoothScrollBy(k, j, true, true);
                  AbsListView.this.postOnAnimation(this);
                } else if (n > i1) {
                  k = (int)(AbsListView.this.getHeight() * f1);
                  j = (int)(this.mScrollDuration * f1);
                  AbsListView.this.smoothScrollBy(k, j, true, true);
                  AbsListView.this.postOnAnimation(this);
                } else {
                  k = AbsListView.this.getChildAt(n - j).getTop();
                  j = k - this.mOffsetFromTop;
                  f1 = this.mScrollDuration;
                  k = (int)(f1 * Math.abs(j) / AbsListView.this.getHeight());
                  AbsListView.this.smoothScrollBy(j, k, true, false);
                } 
              } 
            } else {
              k = AbsListView.this.getChildCount() - 2;
              if (k < 0)
                return; 
              int n = j + k;
              if (n == this.mLastSeenPos) {
                AbsListView.this.postOnAnimation(this);
                return;
              } 
              View view = AbsListView.this.getChildAt(k);
              j = view.getHeight();
              k = view.getTop();
              int m = Math.max(AbsListView.this.mListPadding.top, this.mExtraScroll);
              this.mLastSeenPos = n;
              if (n > this.mBoundPos) {
                AbsListView.this.smoothScrollBy(-(i - k - m), this.mScrollDuration, true, true);
                AbsListView.this.postOnAnimation(this);
              } else {
                i -= m;
                k += j;
                if (i > k) {
                  AbsListView.this.smoothScrollBy(-(i - k), this.mScrollDuration, true, false);
                } else {
                  AbsListView.this.reportScrollStateChange(0);
                } 
              } 
            } 
          } else {
            k = AbsListView.this.getChildCount();
            if (j == this.mBoundPos || k <= 1 || j + k >= AbsListView.this.mItemCount) {
              AbsListView.this.reportScrollStateChange(0);
              return;
            } 
            j++;
            if (j == this.mLastSeenPos) {
              AbsListView.this.postOnAnimation(this);
              return;
            } 
            View view = AbsListView.this.getChildAt(1);
            i = view.getHeight();
            k = view.getTop();
            int m = Math.max(AbsListView.this.mListPadding.bottom, this.mExtraScroll);
            if (j < this.mBoundPos) {
              AbsListView.this.smoothScrollBy(Math.max(0, i + k - m), this.mScrollDuration, true, true);
              this.mLastSeenPos = j;
              AbsListView.this.postOnAnimation(this);
            } else if (k > m) {
              AbsListView.this.smoothScrollBy(k - m, this.mScrollDuration, true, false);
            } else {
              AbsListView.this.reportScrollStateChange(0);
            } 
          } 
        } else {
          if (j == this.mLastSeenPos) {
            AbsListView.this.postOnAnimation(this);
            return;
          } 
          AbsListView absListView = AbsListView.this;
          bool = false;
          View view = absListView.getChildAt(0);
          if (view == null)
            return; 
          i = view.getTop();
          if (j > 0) {
            k = Math.max(this.mExtraScroll, AbsListView.this.mListPadding.top);
          } else {
            k = AbsListView.this.mListPadding.top;
          } 
          view = AbsListView.this;
          int m = this.mScrollDuration;
          if (j > this.mTargetPos)
            bool = true; 
          view.smoothScrollBy(i - k, m, true, bool);
          this.mLastSeenPos = j;
          if (j > this.mTargetPos)
            AbsListView.this.postOnAnimation(this); 
        } 
      } else {
        k = AbsListView.this.getChildCount() - 1;
        j += k;
        if (k < 0)
          return; 
        if (j == this.mLastSeenPos) {
          AbsListView.this.postOnAnimation(this);
          return;
        } 
        View view = AbsListView.this.getChildAt(k);
        int m = view.getHeight();
        int i1 = view.getTop();
        if (j < AbsListView.this.mItemCount - 1) {
          k = Math.max(AbsListView.this.mListPadding.bottom, this.mExtraScroll);
        } else {
          k = AbsListView.this.mListPadding.bottom;
        } 
        view = AbsListView.this;
        int n = this.mScrollDuration;
        if (j < this.mTargetPos)
          bool = true; 
        view.smoothScrollBy(m - i - i1 + k, n, true, bool);
        this.mLastSeenPos = j;
        if (j < this.mTargetPos)
          AbsListView.this.postOnAnimation(this); 
      } 
    }
  }
  
  public int getTouchMode() {
    return this.mTouchMode;
  }
  
  public void setTouchMode(int paramInt) {
    this.mTouchMode = paramInt;
  }
  
  public void oplusStartSpringback() {
    if (this.mFlingRunnable == null)
      this.mFlingRunnable = new FlingRunnable(); 
    this.mFlingRunnable.startSpringback();
  }
  
  public boolean isOplusOSStyle() {
    if (this.mTopThirdPartApp)
      return false; 
    return super.isOplusOSStyle();
  }
  
  public boolean isOplusStyle() {
    if (this.mTopThirdPartApp)
      return false; 
    return super.isOplusStyle();
  }
  
  abstract void fillGap(boolean paramBoolean);
  
  abstract int findMotionRow(int paramInt);
  
  abstract void setSelectionInt(int paramInt);
  
  class MultiChoiceModeListener implements ActionMode.Callback {
    public abstract void onItemCheckedStateChanged(ActionMode param1ActionMode, int param1Int, long param1Long, boolean param1Boolean);
  }
  
  class OnScrollListener {
    public static final int SCROLL_STATE_FLING = 2;
    
    public static final int SCROLL_STATE_IDLE = 0;
    
    public static final int SCROLL_STATE_TOUCH_SCROLL = 1;
    
    public abstract void onScroll(AbsListView param1AbsListView, int param1Int1, int param1Int2, int param1Int3);
    
    public abstract void onScrollStateChanged(AbsListView param1AbsListView, int param1Int);
  }
  
  class RecyclerListener {
    public abstract void onMovedToScrapHeap(View param1View);
  }
  
  class SelectionBoundsAdjuster {
    public abstract void adjustListItemSelectionBounds(Rect param1Rect);
  }
}
