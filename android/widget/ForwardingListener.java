package android.widget;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import com.android.internal.view.menu.ShowableListMenu;

public abstract class ForwardingListener implements View.OnTouchListener, View.OnAttachStateChangeListener {
  private int mActivePointerId;
  
  private Runnable mDisallowIntercept;
  
  private boolean mForwarding;
  
  private final int mLongPressTimeout;
  
  private final float mScaledTouchSlop;
  
  private final View mSrc;
  
  private final int mTapTimeout;
  
  private Runnable mTriggerLongPress;
  
  public ForwardingListener(View paramView) {
    this.mSrc = paramView;
    paramView.setLongClickable(true);
    paramView.addOnAttachStateChangeListener(this);
    this.mScaledTouchSlop = ViewConfiguration.get(paramView.getContext()).getScaledTouchSlop();
    int i = ViewConfiguration.getTapTimeout();
    this.mLongPressTimeout = (i + ViewConfiguration.getLongPressTimeout()) / 2;
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {
    boolean bool3;
    boolean bool = this.mForwarding;
    boolean bool1 = true;
    if (bool) {
      boolean bool4;
      if (onTouchForwarded(paramMotionEvent) || !onForwardingStopped()) {
        bool4 = true;
      } else {
        bool4 = false;
      } 
      bool3 = bool4;
    } else {
      boolean bool4;
      if (onTouchObserved(paramMotionEvent) && onForwardingStarted()) {
        bool4 = true;
      } else {
        bool4 = false;
      } 
      bool3 = bool4;
      if (bool4) {
        long l = SystemClock.uptimeMillis();
        MotionEvent motionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
        this.mSrc.onTouchEvent(motionEvent);
        motionEvent.recycle();
        bool3 = bool4;
      } 
    } 
    this.mForwarding = bool3;
    boolean bool2 = bool1;
    if (!bool3)
      if (bool) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public void onViewAttachedToWindow(View paramView) {}
  
  public void onViewDetachedFromWindow(View paramView) {
    this.mForwarding = false;
    this.mActivePointerId = -1;
    Runnable runnable = this.mDisallowIntercept;
    if (runnable != null)
      this.mSrc.removeCallbacks(runnable); 
  }
  
  protected boolean onForwardingStarted() {
    ShowableListMenu showableListMenu = getPopup();
    if (showableListMenu != null && !showableListMenu.isShowing())
      showableListMenu.show(); 
    return true;
  }
  
  protected boolean onForwardingStopped() {
    ShowableListMenu showableListMenu = getPopup();
    if (showableListMenu != null && showableListMenu.isShowing())
      showableListMenu.dismiss(); 
    return true;
  }
  
  private boolean onTouchObserved(MotionEvent paramMotionEvent) {
    View view = this.mSrc;
    if (!view.isEnabled())
      return false; 
    int i = paramMotionEvent.getActionMasked();
    if (i != 0) {
      if (i != 1)
        if (i != 2) {
          if (i != 3)
            return false; 
        } else {
          i = paramMotionEvent.findPointerIndex(this.mActivePointerId);
          if (i >= 0) {
            float f1 = paramMotionEvent.getX(i);
            float f2 = paramMotionEvent.getY(i);
            if (!view.pointInView(f1, f2, this.mScaledTouchSlop)) {
              clearCallbacks();
              view.getParent().requestDisallowInterceptTouchEvent(true);
              return true;
            } 
          } 
          return false;
        }  
      clearCallbacks();
    } else {
      this.mActivePointerId = paramMotionEvent.getPointerId(0);
      if (this.mDisallowIntercept == null)
        this.mDisallowIntercept = new DisallowIntercept(); 
      view.postDelayed(this.mDisallowIntercept, this.mTapTimeout);
      if (this.mTriggerLongPress == null)
        this.mTriggerLongPress = new TriggerLongPress(); 
      view.postDelayed(this.mTriggerLongPress, this.mLongPressTimeout);
    } 
    return false;
  }
  
  private void clearCallbacks() {
    Runnable runnable = this.mTriggerLongPress;
    if (runnable != null)
      this.mSrc.removeCallbacks(runnable); 
    runnable = this.mDisallowIntercept;
    if (runnable != null)
      this.mSrc.removeCallbacks(runnable); 
  }
  
  private void onLongPress() {
    clearCallbacks();
    View view = this.mSrc;
    if (!view.isEnabled() || view.isLongClickable())
      return; 
    if (!onForwardingStarted())
      return; 
    view.getParent().requestDisallowInterceptTouchEvent(true);
    long l = SystemClock.uptimeMillis();
    MotionEvent motionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
    view.onTouchEvent(motionEvent);
    motionEvent.recycle();
    this.mForwarding = true;
  }
  
  private boolean onTouchForwarded(MotionEvent paramMotionEvent) {
    View view = this.mSrc;
    ShowableListMenu showableListMenu = getPopup();
    boolean bool1 = false;
    if (showableListMenu == null || !showableListMenu.isShowing())
      return false; 
    DropDownListView dropDownListView = (DropDownListView)showableListMenu.getListView();
    if (dropDownListView == null || !dropDownListView.isShown())
      return false; 
    MotionEvent motionEvent = MotionEvent.obtainNoHistory(paramMotionEvent);
    view.toGlobalMotionEvent(motionEvent);
    dropDownListView.toLocalMotionEvent(motionEvent);
    boolean bool = dropDownListView.onForwardedEvent(motionEvent, this.mActivePointerId);
    motionEvent.recycle();
    int i = paramMotionEvent.getActionMasked();
    if (i != 1 && i != 3) {
      i = 1;
    } else {
      i = 0;
    } 
    boolean bool2 = bool1;
    if (bool) {
      bool2 = bool1;
      if (i != 0)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public abstract ShowableListMenu getPopup();
  
  class DisallowIntercept implements Runnable {
    final ForwardingListener this$0;
    
    private DisallowIntercept() {}
    
    public void run() {
      ViewParent viewParent = ForwardingListener.this.mSrc.getParent();
      if (viewParent != null)
        viewParent.requestDisallowInterceptTouchEvent(true); 
    }
  }
  
  class TriggerLongPress implements Runnable {
    final ForwardingListener this$0;
    
    private TriggerLongPress() {}
    
    public void run() {
      ForwardingListener.this.onLongPress();
    }
  }
}
