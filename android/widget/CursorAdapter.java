package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;

public abstract class CursorAdapter extends BaseAdapter implements Filterable, CursorFilter.CursorFilterClient, ThemedSpinnerAdapter {
  @Deprecated
  public static final int FLAG_AUTO_REQUERY = 1;
  
  public static final int FLAG_REGISTER_CONTENT_OBSERVER = 2;
  
  protected boolean mAutoRequery;
  
  protected ChangeObserver mChangeObserver;
  
  protected Context mContext;
  
  protected Cursor mCursor;
  
  protected CursorFilter mCursorFilter;
  
  protected DataSetObserver mDataSetObserver;
  
  protected boolean mDataValid;
  
  protected Context mDropDownContext;
  
  protected FilterQueryProvider mFilterQueryProvider;
  
  protected int mRowIDColumn;
  
  @Deprecated
  public CursorAdapter(Context paramContext, Cursor paramCursor) {
    init(paramContext, paramCursor, 1);
  }
  
  public CursorAdapter(Context paramContext, Cursor paramCursor, boolean paramBoolean) {
    byte b;
    if (paramBoolean) {
      b = 1;
    } else {
      b = 2;
    } 
    init(paramContext, paramCursor, b);
  }
  
  public CursorAdapter(Context paramContext, Cursor paramCursor, int paramInt) {
    init(paramContext, paramCursor, paramInt);
  }
  
  @Deprecated
  protected void init(Context paramContext, Cursor paramCursor, boolean paramBoolean) {
    byte b;
    if (paramBoolean) {
      b = 1;
    } else {
      b = 2;
    } 
    init(paramContext, paramCursor, b);
  }
  
  void init(Context paramContext, Cursor paramCursor, int paramInt) {
    byte b;
    boolean bool = false;
    if ((paramInt & 0x1) == 1) {
      paramInt |= 0x2;
      this.mAutoRequery = true;
    } else {
      this.mAutoRequery = false;
    } 
    if (paramCursor != null)
      bool = true; 
    this.mCursor = paramCursor;
    this.mDataValid = bool;
    this.mContext = paramContext;
    if (bool) {
      b = paramCursor.getColumnIndexOrThrow("_id");
    } else {
      b = -1;
    } 
    this.mRowIDColumn = b;
    if ((paramInt & 0x2) == 2) {
      this.mChangeObserver = new ChangeObserver();
      this.mDataSetObserver = new MyDataSetObserver();
    } else {
      this.mChangeObserver = null;
      this.mDataSetObserver = null;
    } 
    if (bool) {
      ChangeObserver changeObserver = this.mChangeObserver;
      if (changeObserver != null)
        paramCursor.registerContentObserver(changeObserver); 
      DataSetObserver dataSetObserver = this.mDataSetObserver;
      if (dataSetObserver != null)
        paramCursor.registerDataSetObserver(dataSetObserver); 
    } 
  }
  
  public void setDropDownViewTheme(Resources.Theme paramTheme) {
    if (paramTheme == null) {
      this.mDropDownContext = null;
    } else if (paramTheme == this.mContext.getTheme()) {
      this.mDropDownContext = this.mContext;
    } else {
      this.mDropDownContext = (Context)new ContextThemeWrapper(this.mContext, paramTheme);
    } 
  }
  
  public Resources.Theme getDropDownViewTheme() {
    Resources.Theme theme;
    Context context = this.mDropDownContext;
    if (context == null) {
      context = null;
    } else {
      theme = context.getTheme();
    } 
    return theme;
  }
  
  public Cursor getCursor() {
    return this.mCursor;
  }
  
  public int getCount() {
    if (this.mDataValid) {
      Cursor cursor = this.mCursor;
      if (cursor != null)
        return cursor.getCount(); 
    } 
    return 0;
  }
  
  public Object getItem(int paramInt) {
    if (this.mDataValid) {
      Cursor cursor = this.mCursor;
      if (cursor != null) {
        cursor.moveToPosition(paramInt);
        return this.mCursor;
      } 
    } 
    return null;
  }
  
  public long getItemId(int paramInt) {
    if (this.mDataValid) {
      Cursor cursor = this.mCursor;
      if (cursor != null) {
        if (cursor.moveToPosition(paramInt))
          return this.mCursor.getLong(this.mRowIDColumn); 
        return 0L;
      } 
    } 
    return 0L;
  }
  
  public boolean hasStableIds() {
    return true;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    if (this.mDataValid) {
      if (this.mCursor.moveToPosition(paramInt)) {
        if (paramView == null)
          paramView = newView(this.mContext, this.mCursor, paramViewGroup); 
        bindView(paramView, this.mContext, this.mCursor);
        return paramView;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("couldn't move cursor to position ");
      stringBuilder.append(paramInt);
      throw new IllegalStateException(stringBuilder.toString());
    } 
    throw new IllegalStateException("this should only be called when the cursor is valid");
  }
  
  public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    if (this.mDataValid) {
      Context context1 = this.mDropDownContext, context2 = context1;
      if (context1 == null)
        context2 = this.mContext; 
      this.mCursor.moveToPosition(paramInt);
      if (paramView == null)
        paramView = newDropDownView(context2, this.mCursor, paramViewGroup); 
      bindView(paramView, context2, this.mCursor);
      return paramView;
    } 
    return null;
  }
  
  public View newDropDownView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup) {
    return newView(paramContext, paramCursor, paramViewGroup);
  }
  
  public void changeCursor(Cursor paramCursor) {
    paramCursor = swapCursor(paramCursor);
    if (paramCursor != null)
      paramCursor.close(); 
  }
  
  public Cursor swapCursor(Cursor paramCursor) {
    if (paramCursor == this.mCursor)
      return null; 
    Cursor cursor = this.mCursor;
    if (cursor != null) {
      ChangeObserver changeObserver = this.mChangeObserver;
      if (changeObserver != null)
        cursor.unregisterContentObserver(changeObserver); 
      DataSetObserver dataSetObserver = this.mDataSetObserver;
      if (dataSetObserver != null)
        cursor.unregisterDataSetObserver(dataSetObserver); 
    } 
    this.mCursor = paramCursor;
    if (paramCursor != null) {
      ChangeObserver changeObserver = this.mChangeObserver;
      if (changeObserver != null)
        paramCursor.registerContentObserver(changeObserver); 
      DataSetObserver dataSetObserver = this.mDataSetObserver;
      if (dataSetObserver != null)
        paramCursor.registerDataSetObserver(dataSetObserver); 
      this.mRowIDColumn = paramCursor.getColumnIndexOrThrow("_id");
      this.mDataValid = true;
      notifyDataSetChanged();
    } else {
      this.mRowIDColumn = -1;
      this.mDataValid = false;
      notifyDataSetInvalidated();
    } 
    return cursor;
  }
  
  public CharSequence convertToString(Cursor paramCursor) {
    String str;
    if (paramCursor == null) {
      str = "";
    } else {
      str = str.toString();
    } 
    return str;
  }
  
  public Cursor runQueryOnBackgroundThread(CharSequence paramCharSequence) {
    FilterQueryProvider filterQueryProvider = this.mFilterQueryProvider;
    if (filterQueryProvider != null)
      return filterQueryProvider.runQuery(paramCharSequence); 
    return this.mCursor;
  }
  
  public Filter getFilter() {
    if (this.mCursorFilter == null)
      this.mCursorFilter = new CursorFilter(this); 
    return this.mCursorFilter;
  }
  
  public FilterQueryProvider getFilterQueryProvider() {
    return this.mFilterQueryProvider;
  }
  
  public void setFilterQueryProvider(FilterQueryProvider paramFilterQueryProvider) {
    this.mFilterQueryProvider = paramFilterQueryProvider;
  }
  
  protected void onContentChanged() {
    if (this.mAutoRequery) {
      Cursor cursor = this.mCursor;
      if (cursor != null && !cursor.isClosed())
        this.mDataValid = this.mCursor.requery(); 
    } 
  }
  
  public abstract void bindView(View paramView, Context paramContext, Cursor paramCursor);
  
  public abstract View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup);
  
  class ChangeObserver extends ContentObserver {
    final CursorAdapter this$0;
    
    public ChangeObserver() {
      super(new Handler());
    }
    
    public boolean deliverSelfNotifications() {
      return true;
    }
    
    public void onChange(boolean param1Boolean) {
      CursorAdapter.this.onContentChanged();
    }
  }
  
  class MyDataSetObserver extends DataSetObserver {
    final CursorAdapter this$0;
    
    private MyDataSetObserver() {}
    
    public void onChanged() {
      CursorAdapter.this.mDataValid = true;
      CursorAdapter.this.notifyDataSetChanged();
    }
    
    public void onInvalidated() {
      CursorAdapter.this.mDataValid = false;
      CursorAdapter.this.notifyDataSetInvalidated();
    }
  }
}
