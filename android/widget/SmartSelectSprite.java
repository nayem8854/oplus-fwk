package android.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.shapes.Shape;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.ToDoubleFunction;

final class SmartSelectSprite {
  private static final int CORNER_DURATION = 50;
  
  private static final int EXPAND_DURATION = 300;
  
  static final Comparator<RectF> RECTANGLE_COMPARATOR;
  
  private Animator mActiveAnimator = null;
  
  private final Interpolator mCornerInterpolator;
  
  static {
    -$.Lambda.SmartSelectSprite.c8eqlh2kO_X0luLU2BexwK921WA c8eqlh2kO_X0luLU2BexwK921WA = _$$Lambda$SmartSelectSprite$c8eqlh2kO_X0luLU2BexwK921WA.INSTANCE;
    Comparator<?> comparator = Comparator.comparingDouble((ToDoubleFunction<?>)c8eqlh2kO_X0luLU2BexwK921WA);
    -$.Lambda.SmartSelectSprite.mdkXIT1_UNlJQMaziE_E815aIKE mdkXIT1_UNlJQMaziE_E815aIKE = _$$Lambda$SmartSelectSprite$mdkXIT1_UNlJQMaziE_E815aIKE.INSTANCE;
    RECTANGLE_COMPARATOR = (Comparator)comparator.thenComparingDouble((ToDoubleFunction<?>)mdkXIT1_UNlJQMaziE_E815aIKE);
  }
  
  private Drawable mExistingDrawable = null;
  
  private RectangleList mExistingRectangleList = null;
  
  private final Interpolator mExpandInterpolator;
  
  private final int mFillColor;
  
  private final Runnable mInvalidator;
  
  static final class RectangleWithTextSelectionLayout {
    private final RectF mRectangle;
    
    private final int mTextSelectionLayout;
    
    RectangleWithTextSelectionLayout(RectF param1RectF, int param1Int) {
      Objects.requireNonNull(param1RectF);
      this.mRectangle = param1RectF;
      this.mTextSelectionLayout = param1Int;
    }
    
    public RectF getRectangle() {
      return this.mRectangle;
    }
    
    public int getTextSelectionLayout() {
      return this.mTextSelectionLayout;
    }
  }
  
  class RoundedRectangleShape extends Shape {
    private static final String PROPERTY_ROUND_RATIO = "roundRatio";
    
    private final RectF mBoundingRectangle;
    
    private final float mBoundingWidth;
    
    private final Path mClipPath;
    
    private final RectF mDrawRect;
    
    private final int mExpansionDirection;
    
    private final boolean mInverted;
    
    private float mLeftBoundary;
    
    private float mRightBoundary;
    
    private float mRoundRatio;
    
    private static int invert(int param1Int) {
      return param1Int * -1;
    }
    
    private RoundedRectangleShape(SmartSelectSprite this$0, int param1Int, boolean param1Boolean) {
      boolean bool;
      this.mRoundRatio = 1.0F;
      this.mDrawRect = new RectF();
      this.mClipPath = new Path();
      this.mLeftBoundary = 0.0F;
      this.mRightBoundary = 0.0F;
      this.mBoundingRectangle = new RectF((RectF)this$0);
      this.mBoundingWidth = this$0.width();
      if (param1Boolean && param1Int != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mInverted = bool;
      if (param1Boolean) {
        this.mExpansionDirection = invert(param1Int);
      } else {
        this.mExpansionDirection = param1Int;
      } 
      if (this$0.height() > this$0.width()) {
        setRoundRatio(0.0F);
      } else {
        setRoundRatio(1.0F);
      } 
    }
    
    public void draw(Canvas param1Canvas, Paint param1Paint) {
      if (this.mLeftBoundary == this.mRightBoundary)
        return; 
      float f1 = getCornerRadius();
      float f2 = getAdjustedCornerRadius();
      this.mDrawRect.set(this.mBoundingRectangle);
      this.mDrawRect.left = this.mBoundingRectangle.left + this.mLeftBoundary - f1 / 2.0F;
      this.mDrawRect.right = this.mBoundingRectangle.left + this.mRightBoundary + f1 / 2.0F;
      param1Canvas.save();
      this.mClipPath.reset();
      this.mClipPath.addRoundRect(this.mDrawRect, f2, f2, Path.Direction.CW);
      param1Canvas.clipPath(this.mClipPath);
      param1Canvas.drawRect(this.mBoundingRectangle, param1Paint);
      param1Canvas.restore();
    }
    
    void setRoundRatio(float param1Float) {
      this.mRoundRatio = param1Float;
    }
    
    float getRoundRatio() {
      return this.mRoundRatio;
    }
    
    private void setStartBoundary(float param1Float) {
      if (this.mInverted) {
        this.mRightBoundary = this.mBoundingWidth - param1Float;
      } else {
        this.mLeftBoundary = param1Float;
      } 
    }
    
    private void setEndBoundary(float param1Float) {
      if (this.mInverted) {
        this.mLeftBoundary = this.mBoundingWidth - param1Float;
      } else {
        this.mRightBoundary = param1Float;
      } 
    }
    
    private float getCornerRadius() {
      return Math.min(this.mBoundingRectangle.width(), this.mBoundingRectangle.height());
    }
    
    private float getAdjustedCornerRadius() {
      return getCornerRadius() * this.mRoundRatio;
    }
    
    private float getBoundingWidth() {
      return (int)(this.mBoundingRectangle.width() + getCornerRadius());
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class ExpansionDirection implements Annotation {
      public static final int CENTER = 0;
      
      public static final int LEFT = -1;
      
      public static final int RIGHT = 1;
    }
  }
  
  class RectangleList extends Shape {
    private static final String PROPERTY_LEFT_BOUNDARY = "leftBoundary";
    
    private static final String PROPERTY_RIGHT_BOUNDARY = "rightBoundary";
    
    private int mDisplayType = 0;
    
    private final Path mOutlinePolygonPath;
    
    private final List<SmartSelectSprite.RoundedRectangleShape> mRectangles;
    
    private final List<SmartSelectSprite.RoundedRectangleShape> mReversedRectangles;
    
    private RectangleList(SmartSelectSprite this$0) {
      this.mRectangles = new ArrayList<>((Collection<? extends SmartSelectSprite.RoundedRectangleShape>)this$0);
      ArrayList<SmartSelectSprite.RoundedRectangleShape> arrayList = new ArrayList((Collection<?>)this$0);
      Collections.reverse(arrayList);
      this.mOutlinePolygonPath = generateOutlinePolygonPath((List<SmartSelectSprite.RoundedRectangleShape>)this$0);
    }
    
    private void setLeftBoundary(float param1Float) {
      float f = getTotalWidth();
      for (SmartSelectSprite.RoundedRectangleShape roundedRectangleShape : this.mReversedRectangles) {
        float f1 = f - roundedRectangleShape.getBoundingWidth();
        if (param1Float < f1) {
          roundedRectangleShape.setStartBoundary(0.0F);
        } else if (param1Float > f) {
          roundedRectangleShape.setStartBoundary(roundedRectangleShape.getBoundingWidth());
        } else {
          float f2 = roundedRectangleShape.getBoundingWidth();
          roundedRectangleShape.setStartBoundary(f2 - f + param1Float);
        } 
        f = f1;
      } 
    }
    
    private void setRightBoundary(float param1Float) {
      float f = 0.0F;
      for (SmartSelectSprite.RoundedRectangleShape roundedRectangleShape : this.mRectangles) {
        float f1 = roundedRectangleShape.getBoundingWidth() + f;
        if (f1 < param1Float) {
          roundedRectangleShape.setEndBoundary(roundedRectangleShape.getBoundingWidth());
        } else if (f > param1Float) {
          roundedRectangleShape.setEndBoundary(0.0F);
        } else {
          roundedRectangleShape.setEndBoundary(param1Float - f);
        } 
        f = f1;
      } 
    }
    
    void setDisplayType(int param1Int) {
      this.mDisplayType = param1Int;
    }
    
    private int getTotalWidth() {
      int i = 0;
      for (SmartSelectSprite.RoundedRectangleShape roundedRectangleShape : this.mRectangles)
        i = (int)(i + roundedRectangleShape.getBoundingWidth()); 
      return i;
    }
    
    public void draw(Canvas param1Canvas, Paint param1Paint) {
      if (this.mDisplayType == 1) {
        drawPolygon(param1Canvas, param1Paint);
      } else {
        drawRectangles(param1Canvas, param1Paint);
      } 
    }
    
    private void drawRectangles(Canvas param1Canvas, Paint param1Paint) {
      for (SmartSelectSprite.RoundedRectangleShape roundedRectangleShape : this.mRectangles)
        roundedRectangleShape.draw(param1Canvas, param1Paint); 
    }
    
    private void drawPolygon(Canvas param1Canvas, Paint param1Paint) {
      param1Canvas.drawPath(this.mOutlinePolygonPath, param1Paint);
    }
    
    private static Path generateOutlinePolygonPath(List<SmartSelectSprite.RoundedRectangleShape> param1List) {
      Path path = new Path();
      for (SmartSelectSprite.RoundedRectangleShape roundedRectangleShape : param1List) {
        Path path1 = new Path();
        path1.addRect(roundedRectangleShape.mBoundingRectangle, Path.Direction.CW);
        path.op(path1, Path.Op.UNION);
      } 
      return path;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class DisplayType implements Annotation {
      public static final int POLYGON = 1;
      
      public static final int RECTANGLES = 0;
    }
  }
  
  SmartSelectSprite(Context paramContext, int paramInt, Runnable paramRunnable) {
    this.mExpandInterpolator = AnimationUtils.loadInterpolator(paramContext, 17563661);
    this.mCornerInterpolator = AnimationUtils.loadInterpolator(paramContext, 17563663);
    this.mFillColor = paramInt;
    Objects.requireNonNull(paramRunnable);
    this.mInvalidator = paramRunnable;
  }
  
  public void startAnimation(PointF paramPointF, List<RectangleWithTextSelectionLayout> paramList, Runnable paramRunnable) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual cancelAnimation : ()V
    //   4: new android/widget/_$$Lambda$SmartSelectSprite$2pck5xTffRWoiD4l_tkO_IIf5iM
    //   7: dup
    //   8: aload_0
    //   9: invokespecial <init> : (Landroid/widget/SmartSelectSprite;)V
    //   12: astore #4
    //   14: aload_2
    //   15: invokeinterface size : ()I
    //   20: istore #5
    //   22: new java/util/ArrayList
    //   25: dup
    //   26: iload #5
    //   28: invokespecial <init> : (I)V
    //   31: astore #6
    //   33: new java/util/ArrayList
    //   36: dup
    //   37: iload #5
    //   39: invokespecial <init> : (I)V
    //   42: astore #7
    //   44: iconst_0
    //   45: istore #8
    //   47: aload_2
    //   48: invokeinterface iterator : ()Ljava/util/Iterator;
    //   53: astore #9
    //   55: aload #9
    //   57: invokeinterface hasNext : ()Z
    //   62: ifeq -> 111
    //   65: aload #9
    //   67: invokeinterface next : ()Ljava/lang/Object;
    //   72: checkcast android/widget/SmartSelectSprite$RectangleWithTextSelectionLayout
    //   75: astore #10
    //   77: aload #10
    //   79: invokevirtual getRectangle : ()Landroid/graphics/RectF;
    //   82: astore #11
    //   84: aload #11
    //   86: aload_1
    //   87: invokestatic contains : (Landroid/graphics/RectF;Landroid/graphics/PointF;)Z
    //   90: ifeq -> 96
    //   93: goto -> 114
    //   96: iload #8
    //   98: i2f
    //   99: aload #11
    //   101: invokevirtual width : ()F
    //   104: fadd
    //   105: f2i
    //   106: istore #8
    //   108: goto -> 55
    //   111: aconst_null
    //   112: astore #10
    //   114: aload #10
    //   116: ifnull -> 327
    //   119: iload #8
    //   121: i2f
    //   122: aload_1
    //   123: getfield x : F
    //   126: aload #10
    //   128: invokevirtual getRectangle : ()Landroid/graphics/RectF;
    //   131: getfield left : F
    //   134: fsub
    //   135: fadd
    //   136: f2i
    //   137: istore #12
    //   139: aload #10
    //   141: aload_2
    //   142: invokestatic generateDirections : (Landroid/widget/SmartSelectSprite$RectangleWithTextSelectionLayout;Ljava/util/List;)[I
    //   145: astore_1
    //   146: iconst_0
    //   147: istore #8
    //   149: iload #8
    //   151: iload #5
    //   153: if_icmpge -> 247
    //   156: aload_2
    //   157: iload #8
    //   159: invokeinterface get : (I)Ljava/lang/Object;
    //   164: checkcast android/widget/SmartSelectSprite$RectangleWithTextSelectionLayout
    //   167: astore #10
    //   169: aload #10
    //   171: invokevirtual getRectangle : ()Landroid/graphics/RectF;
    //   174: astore #9
    //   176: aload_1
    //   177: iload #8
    //   179: iaload
    //   180: istore #13
    //   182: aload #10
    //   184: invokevirtual getTextSelectionLayout : ()I
    //   187: ifne -> 196
    //   190: iconst_1
    //   191: istore #14
    //   193: goto -> 199
    //   196: iconst_0
    //   197: istore #14
    //   199: new android/widget/SmartSelectSprite$RoundedRectangleShape
    //   202: dup
    //   203: aload #9
    //   205: iload #13
    //   207: iload #14
    //   209: aconst_null
    //   210: invokespecial <init> : (Landroid/graphics/RectF;IZLandroid/widget/SmartSelectSprite$1;)V
    //   213: astore #10
    //   215: aload #7
    //   217: aload_0
    //   218: aload #10
    //   220: aload #4
    //   222: invokespecial createCornerAnimator : (Landroid/widget/SmartSelectSprite$RoundedRectangleShape;Landroid/animation/ValueAnimator$AnimatorUpdateListener;)Landroid/animation/ObjectAnimator;
    //   225: invokeinterface add : (Ljava/lang/Object;)Z
    //   230: pop
    //   231: aload #6
    //   233: aload #10
    //   235: invokeinterface add : (Ljava/lang/Object;)Z
    //   240: pop
    //   241: iinc #8, 1
    //   244: goto -> 149
    //   247: new android/widget/SmartSelectSprite$RectangleList
    //   250: dup
    //   251: aload #6
    //   253: aconst_null
    //   254: invokespecial <init> : (Ljava/util/List;Landroid/widget/SmartSelectSprite$1;)V
    //   257: astore_1
    //   258: new android/graphics/drawable/ShapeDrawable
    //   261: dup
    //   262: aload_1
    //   263: invokespecial <init> : (Landroid/graphics/drawable/shapes/Shape;)V
    //   266: astore_2
    //   267: aload_2
    //   268: invokevirtual getPaint : ()Landroid/graphics/Paint;
    //   271: astore #10
    //   273: aload #10
    //   275: aload_0
    //   276: getfield mFillColor : I
    //   279: invokevirtual setColor : (I)V
    //   282: aload #10
    //   284: getstatic android/graphics/Paint$Style.FILL : Landroid/graphics/Paint$Style;
    //   287: invokevirtual setStyle : (Landroid/graphics/Paint$Style;)V
    //   290: aload_0
    //   291: aload_1
    //   292: putfield mExistingRectangleList : Landroid/widget/SmartSelectSprite$RectangleList;
    //   295: aload_0
    //   296: aload_2
    //   297: putfield mExistingDrawable : Landroid/graphics/drawable/Drawable;
    //   300: aload_0
    //   301: aload_1
    //   302: iload #12
    //   304: i2f
    //   305: iload #12
    //   307: i2f
    //   308: aload #7
    //   310: aload #4
    //   312: aload_3
    //   313: invokespecial createAnimator : (Landroid/widget/SmartSelectSprite$RectangleList;FFLjava/util/List;Landroid/animation/ValueAnimator$AnimatorUpdateListener;Ljava/lang/Runnable;)Landroid/animation/Animator;
    //   316: astore_1
    //   317: aload_0
    //   318: aload_1
    //   319: putfield mActiveAnimator : Landroid/animation/Animator;
    //   322: aload_1
    //   323: invokevirtual start : ()V
    //   326: return
    //   327: new java/lang/IllegalArgumentException
    //   330: dup
    //   331: ldc_w 'Center point is not inside any of the rectangles!'
    //   334: invokespecial <init> : (Ljava/lang/String;)V
    //   337: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #367	-> 0
    //   #369	-> 4
    //   #372	-> 14
    //   #374	-> 22
    //   #375	-> 33
    //   #377	-> 44
    //   #379	-> 44
    //   #381	-> 47
    //   #382	-> 77
    //   #383	-> 84
    //   #384	-> 93
    //   #385	-> 93
    //   #387	-> 96
    //   #388	-> 108
    //   #381	-> 111
    //   #390	-> 114
    //   #394	-> 119
    //   #396	-> 139
    //   #397	-> 139
    //   #399	-> 146
    //   #400	-> 156
    //   #401	-> 156
    //   #402	-> 169
    //   #403	-> 176
    //   #406	-> 182
    //   #408	-> 215
    //   #409	-> 231
    //   #399	-> 241
    //   #412	-> 247
    //   #413	-> 258
    //   #415	-> 267
    //   #416	-> 273
    //   #417	-> 282
    //   #419	-> 290
    //   #420	-> 295
    //   #422	-> 300
    //   #424	-> 322
    //   #425	-> 326
    //   #391	-> 327
  }
  
  public boolean isAnimationActive() {
    boolean bool;
    Animator animator = this.mActiveAnimator;
    if (animator != null && animator.isRunning()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private Animator createAnimator(RectangleList paramRectangleList, float paramFloat1, float paramFloat2, List<Animator> paramList, ValueAnimator.AnimatorUpdateListener paramAnimatorUpdateListener, Runnable paramRunnable) {
    float f = paramRectangleList.getTotalWidth();
    ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(paramRectangleList, "rightBoundary", new float[] { paramFloat2, f });
    ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(paramRectangleList, "leftBoundary", new float[] { paramFloat1, 0.0F });
    objectAnimator2.setDuration(300L);
    objectAnimator1.setDuration(300L);
    objectAnimator2.addUpdateListener(paramAnimatorUpdateListener);
    objectAnimator1.addUpdateListener(paramAnimatorUpdateListener);
    objectAnimator2.setInterpolator(this.mExpandInterpolator);
    objectAnimator1.setInterpolator(this.mExpandInterpolator);
    AnimatorSet animatorSet3 = new AnimatorSet();
    animatorSet3.playTogether(paramList);
    AnimatorSet animatorSet2 = new AnimatorSet();
    animatorSet2.playTogether(new Animator[] { (Animator)objectAnimator1, (Animator)objectAnimator2 });
    AnimatorSet animatorSet1 = new AnimatorSet();
    animatorSet1.playSequentially(new Animator[] { (Animator)animatorSet2, (Animator)animatorSet3 });
    setUpAnimatorListener((Animator)animatorSet1, paramRunnable);
    return (Animator)animatorSet1;
  }
  
  private void setUpAnimatorListener(Animator paramAnimator, Runnable paramRunnable) {
    paramAnimator.addListener((Animator.AnimatorListener)new Object(this, paramRunnable));
  }
  
  private ObjectAnimator createCornerAnimator(RoundedRectangleShape paramRoundedRectangleShape, ValueAnimator.AnimatorUpdateListener paramAnimatorUpdateListener) {
    float f = paramRoundedRectangleShape.getRoundRatio();
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(paramRoundedRectangleShape, "roundRatio", new float[] { f, 0.0F });
    objectAnimator.setDuration(50L);
    objectAnimator.addUpdateListener(paramAnimatorUpdateListener);
    objectAnimator.setInterpolator(this.mCornerInterpolator);
    return objectAnimator;
  }
  
  private static int[] generateDirections(RectangleWithTextSelectionLayout paramRectangleWithTextSelectionLayout, List<RectangleWithTextSelectionLayout> paramList) {
    int[] arrayOfInt = new int[paramList.size()];
    int i = paramList.indexOf(paramRectangleWithTextSelectionLayout);
    int j;
    for (j = 0; j < i - 1; j++)
      arrayOfInt[j] = -1; 
    if (paramList.size() == 1) {
      arrayOfInt[i] = 0;
    } else if (i == 0) {
      arrayOfInt[i] = -1;
    } else if (i == paramList.size() - 1) {
      arrayOfInt[i] = 1;
    } else {
      arrayOfInt[i] = 0;
    } 
    for (j = i + 1; j < arrayOfInt.length; j++)
      arrayOfInt[j] = 1; 
    return arrayOfInt;
  }
  
  private static boolean contains(RectF paramRectF, PointF paramPointF) {
    boolean bool;
    float f1 = paramPointF.x;
    float f2 = paramPointF.y;
    if (f1 >= paramRectF.left && f1 <= paramRectF.right && f2 >= paramRectF.top && f2 <= paramRectF.bottom) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void removeExistingDrawables() {
    this.mExistingDrawable = null;
    this.mExistingRectangleList = null;
    this.mInvalidator.run();
  }
  
  public void cancelAnimation() {
    Animator animator = this.mActiveAnimator;
    if (animator != null) {
      animator.cancel();
      this.mActiveAnimator = null;
      removeExistingDrawables();
    } 
  }
  
  public void draw(Canvas paramCanvas) {
    Drawable drawable = this.mExistingDrawable;
    if (drawable != null)
      drawable.draw(paramCanvas); 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  private static @interface DisplayType {
    public static final int POLYGON = 1;
    
    public static final int RECTANGLES = 0;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  private static @interface ExpansionDirection {
    public static final int CENTER = 0;
    
    public static final int LEFT = -1;
    
    public static final int RIGHT = 1;
  }
}
