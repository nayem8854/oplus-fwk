package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.FloatProperty;
import android.util.Pools;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewHierarchyEncoder;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import com.android.internal.R;
import java.text.NumberFormat;
import java.util.ArrayList;

@RemoteView
public class ProgressBar extends View {
  private static final int MAX_LEVEL = 10000;
  
  private static final int PROGRESS_ANIM_DURATION = 80;
  
  private static final DecelerateInterpolator PROGRESS_ANIM_INTERPOLATOR = new DecelerateInterpolator();
  
  private final FloatProperty<ProgressBar> VISUAL_PROGRESS;
  
  private boolean mAggregatedIsVisible;
  
  private AlphaAnimation mAnimation;
  
  private boolean mAttached;
  
  private int mBehavior;
  
  private Drawable mCurrentDrawable;
  
  private CharSequence mCustomStateDescription;
  
  private int mDuration;
  
  private boolean mHasAnimation;
  
  private boolean mInDrawing;
  
  private boolean mIndeterminate;
  
  private Drawable mIndeterminateDrawable;
  
  private Interpolator mInterpolator;
  
  private int mMax;
  
  int mMaxHeight;
  
  private boolean mMaxInitialized;
  
  int mMaxWidth;
  
  private int mMin;
  
  int mMinHeight;
  
  private boolean mMinInitialized;
  
  int mMinWidth;
  
  boolean mMirrorForRtl;
  
  private boolean mNoInvalidate;
  
  private boolean mOnlyIndeterminate;
  
  private int mProgress;
  
  private Drawable mProgressDrawable;
  
  private ProgressTintInfo mProgressTintInfo;
  
  private final ArrayList<RefreshData> mRefreshData;
  
  private boolean mRefreshIsPosted;
  
  private RefreshProgressRunnable mRefreshProgressRunnable;
  
  int mSampleWidth;
  
  private int mSecondaryProgress;
  
  private boolean mShouldStartAnimationDrawable;
  
  private Transformation mTransformation;
  
  private long mUiThreadId;
  
  private float mVisualProgress;
  
  public ProgressBar(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ProgressBar(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842871);
  }
  
  public ProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    boolean bool = false;
    this.mSampleWidth = 0;
    this.mMirrorForRtl = false;
    this.mCustomStateDescription = null;
    this.mRefreshData = new ArrayList<>();
    this.VISUAL_PROGRESS = (FloatProperty<ProgressBar>)new Object(this, "visual_progress");
    this.mUiThreadId = Thread.currentThread().getId();
    initProgressBar();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ProgressBar, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ProgressBar, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mNoInvalidate = true;
    Drawable drawable2 = typedArray.getDrawable(8);
    if (drawable2 != null)
      if (needsTileify(drawable2)) {
        setProgressDrawableTiled(drawable2);
      } else {
        setProgressDrawable(drawable2);
      }  
    this.mDuration = typedArray.getInt(9, this.mDuration);
    this.mMinWidth = typedArray.getDimensionPixelSize(11, this.mMinWidth);
    this.mMaxWidth = typedArray.getDimensionPixelSize(0, this.mMaxWidth);
    this.mMinHeight = typedArray.getDimensionPixelSize(12, this.mMinHeight);
    this.mMaxHeight = typedArray.getDimensionPixelSize(1, this.mMaxHeight);
    this.mBehavior = typedArray.getInt(10, this.mBehavior);
    paramInt1 = typedArray.getResourceId(13, 17432587);
    if (paramInt1 > 0)
      setInterpolator(paramContext, paramInt1); 
    setMin(typedArray.getInt(26, this.mMin));
    setMax(typedArray.getInt(2, this.mMax));
    setProgress(typedArray.getInt(3, this.mProgress));
    setSecondaryProgress(typedArray.getInt(4, this.mSecondaryProgress));
    Drawable drawable1 = typedArray.getDrawable(7);
    if (drawable1 != null)
      if (needsTileify(drawable1)) {
        setIndeterminateDrawableTiled(drawable1);
      } else {
        setIndeterminateDrawable(drawable1);
      }  
    boolean bool1 = typedArray.getBoolean(6, this.mOnlyIndeterminate);
    this.mNoInvalidate = false;
    if (bool1 || typedArray.getBoolean(5, this.mIndeterminate))
      bool = true; 
    setIndeterminate(bool);
    this.mMirrorForRtl = typedArray.getBoolean(15, this.mMirrorForRtl);
    if (typedArray.hasValue(17)) {
      if (this.mProgressTintInfo == null)
        this.mProgressTintInfo = new ProgressTintInfo(); 
      this.mProgressTintInfo.mProgressBlendMode = Drawable.parseBlendMode(typedArray.getInt(17, -1), null);
      this.mProgressTintInfo.mHasProgressTintMode = true;
    } 
    if (typedArray.hasValue(16)) {
      if (this.mProgressTintInfo == null)
        this.mProgressTintInfo = new ProgressTintInfo(); 
      this.mProgressTintInfo.mProgressTintList = typedArray.getColorStateList(16);
      this.mProgressTintInfo.mHasProgressTint = true;
    } 
    if (typedArray.hasValue(19)) {
      if (this.mProgressTintInfo == null)
        this.mProgressTintInfo = new ProgressTintInfo(); 
      this.mProgressTintInfo.mProgressBackgroundBlendMode = Drawable.parseBlendMode(typedArray.getInt(19, -1), null);
      this.mProgressTintInfo.mHasProgressBackgroundTintMode = true;
    } 
    if (typedArray.hasValue(18)) {
      if (this.mProgressTintInfo == null)
        this.mProgressTintInfo = new ProgressTintInfo(); 
      this.mProgressTintInfo.mProgressBackgroundTintList = typedArray.getColorStateList(18);
      this.mProgressTintInfo.mHasProgressBackgroundTint = true;
    } 
    if (typedArray.hasValue(21)) {
      if (this.mProgressTintInfo == null)
        this.mProgressTintInfo = new ProgressTintInfo(); 
      ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
      paramInt1 = typedArray.getInt(21, -1);
      progressTintInfo.mSecondaryProgressBlendMode = Drawable.parseBlendMode(paramInt1, null);
      this.mProgressTintInfo.mHasSecondaryProgressTintMode = true;
    } 
    if (typedArray.hasValue(20)) {
      if (this.mProgressTintInfo == null)
        this.mProgressTintInfo = new ProgressTintInfo(); 
      this.mProgressTintInfo.mSecondaryProgressTintList = typedArray.getColorStateList(20);
      this.mProgressTintInfo.mHasSecondaryProgressTint = true;
    } 
    if (typedArray.hasValue(23)) {
      if (this.mProgressTintInfo == null)
        this.mProgressTintInfo = new ProgressTintInfo(); 
      this.mProgressTintInfo.mIndeterminateBlendMode = Drawable.parseBlendMode(typedArray.getInt(23, -1), null);
      this.mProgressTintInfo.mHasIndeterminateTintMode = true;
    } 
    if (typedArray.hasValue(22)) {
      if (this.mProgressTintInfo == null)
        this.mProgressTintInfo = new ProgressTintInfo(); 
      this.mProgressTintInfo.mIndeterminateTintList = typedArray.getColorStateList(22);
      this.mProgressTintInfo.mHasIndeterminateTint = true;
    } 
    typedArray.recycle();
    applyProgressTints();
    applyIndeterminateTint();
    if (getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
  }
  
  public void setMinWidth(int paramInt) {
    this.mMinWidth = paramInt;
    requestLayout();
  }
  
  public int getMinWidth() {
    return this.mMinWidth;
  }
  
  public void setMaxWidth(int paramInt) {
    this.mMaxWidth = paramInt;
    requestLayout();
  }
  
  public int getMaxWidth() {
    return this.mMaxWidth;
  }
  
  public void setMinHeight(int paramInt) {
    this.mMinHeight = paramInt;
    requestLayout();
  }
  
  public int getMinHeight() {
    return this.mMinHeight;
  }
  
  public void setMaxHeight(int paramInt) {
    this.mMaxHeight = paramInt;
    requestLayout();
  }
  
  public int getMaxHeight() {
    return this.mMaxHeight;
  }
  
  private static boolean needsTileify(Drawable paramDrawable) {
    LayerDrawable layerDrawable;
    StateListDrawable stateListDrawable;
    if (paramDrawable instanceof LayerDrawable) {
      layerDrawable = (LayerDrawable)paramDrawable;
      int i = layerDrawable.getNumberOfLayers();
      for (byte b = 0; b < i; b++) {
        if (needsTileify(layerDrawable.getDrawable(b)))
          return true; 
      } 
      return false;
    } 
    if (layerDrawable instanceof StateListDrawable) {
      stateListDrawable = (StateListDrawable)layerDrawable;
      int i = stateListDrawable.getStateCount();
      for (byte b = 0; b < i; b++) {
        if (needsTileify(stateListDrawable.getStateDrawable(b)))
          return true; 
      } 
      return false;
    } 
    if (stateListDrawable instanceof BitmapDrawable)
      return true; 
    return false;
  }
  
  private Drawable tileify(Drawable paramDrawable, boolean paramBoolean) {
    LayerDrawable layerDrawable;
    StateListDrawable stateListDrawable;
    BitmapDrawable bitmapDrawable;
    if (paramDrawable instanceof LayerDrawable) {
      layerDrawable = (LayerDrawable)paramDrawable;
      int i = layerDrawable.getNumberOfLayers();
      Drawable[] arrayOfDrawable = new Drawable[i];
      byte b;
      for (b = 0; b < i; b++) {
        int j = layerDrawable.getId(b);
        Drawable drawable = layerDrawable.getDrawable(b);
        if (j == 16908301 || j == 16908303) {
          paramBoolean = true;
        } else {
          paramBoolean = false;
        } 
        arrayOfDrawable[b] = tileify(drawable, paramBoolean);
      } 
      LayerDrawable layerDrawable1 = new LayerDrawable(arrayOfDrawable);
      for (b = 0; b < i; b++) {
        layerDrawable1.setId(b, layerDrawable.getId(b));
        layerDrawable1.setLayerGravity(b, layerDrawable.getLayerGravity(b));
        layerDrawable1.setLayerWidth(b, layerDrawable.getLayerWidth(b));
        layerDrawable1.setLayerHeight(b, layerDrawable.getLayerHeight(b));
        layerDrawable1.setLayerInsetLeft(b, layerDrawable.getLayerInsetLeft(b));
        layerDrawable1.setLayerInsetRight(b, layerDrawable.getLayerInsetRight(b));
        layerDrawable1.setLayerInsetTop(b, layerDrawable.getLayerInsetTop(b));
        layerDrawable1.setLayerInsetBottom(b, layerDrawable.getLayerInsetBottom(b));
        layerDrawable1.setLayerInsetStart(b, layerDrawable.getLayerInsetStart(b));
        layerDrawable1.setLayerInsetEnd(b, layerDrawable.getLayerInsetEnd(b));
      } 
      return (Drawable)layerDrawable1;
    } 
    if (layerDrawable instanceof StateListDrawable) {
      StateListDrawable stateListDrawable1 = (StateListDrawable)layerDrawable;
      stateListDrawable = new StateListDrawable();
      int i = stateListDrawable1.getStateCount();
      for (byte b = 0; b < i; b++)
        stateListDrawable.addState(stateListDrawable1.getStateSet(b), tileify(stateListDrawable1.getStateDrawable(b), paramBoolean)); 
      return (Drawable)stateListDrawable;
    } 
    if (stateListDrawable instanceof BitmapDrawable) {
      Drawable.ConstantState constantState = stateListDrawable.getConstantState();
      bitmapDrawable = (BitmapDrawable)constantState.newDrawable(getResources());
      bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.CLAMP);
      if (this.mSampleWidth <= 0)
        this.mSampleWidth = bitmapDrawable.getIntrinsicWidth(); 
      if (paramBoolean)
        return (Drawable)new ClipDrawable((Drawable)bitmapDrawable, 3, 1); 
      return (Drawable)bitmapDrawable;
    } 
    return (Drawable)bitmapDrawable;
  }
  
  Shape getDrawableShape() {
    return (Shape)new RoundRectShape(new float[] { 5.0F, 5.0F, 5.0F, 5.0F, 5.0F, 5.0F, 5.0F, 5.0F }, null, null);
  }
  
  private Drawable tileifyIndeterminate(Drawable paramDrawable) {
    AnimationDrawable animationDrawable;
    Drawable drawable = paramDrawable;
    if (paramDrawable instanceof AnimationDrawable) {
      AnimationDrawable animationDrawable1 = (AnimationDrawable)paramDrawable;
      int i = animationDrawable1.getNumberOfFrames();
      animationDrawable = new AnimationDrawable();
      animationDrawable.setOneShot(animationDrawable1.isOneShot());
      for (byte b = 0; b < i; b++) {
        Drawable drawable1 = tileify(animationDrawable1.getFrame(b), true);
        drawable1.setLevel(10000);
        animationDrawable.addFrame(drawable1, animationDrawable1.getDuration(b));
      } 
      animationDrawable.setLevel(10000);
    } 
    return (Drawable)animationDrawable;
  }
  
  private void initProgressBar() {
    this.mMin = 0;
    this.mMax = 100;
    this.mProgress = 0;
    this.mSecondaryProgress = 0;
    this.mIndeterminate = false;
    this.mOnlyIndeterminate = false;
    this.mDuration = 4000;
    this.mBehavior = 1;
    this.mMinWidth = 24;
    this.mMaxWidth = 48;
    this.mMinHeight = 24;
    this.mMaxHeight = 48;
  }
  
  @ExportedProperty(category = "progress")
  public boolean isIndeterminate() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIndeterminate : Z
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #654	-> 2
    //   #654	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  @RemotableViewMethod
  public void setIndeterminate(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mOnlyIndeterminate : Z
    //   6: ifeq -> 16
    //   9: aload_0
    //   10: getfield mIndeterminate : Z
    //   13: ifne -> 60
    //   16: iload_1
    //   17: aload_0
    //   18: getfield mIndeterminate : Z
    //   21: if_icmpeq -> 60
    //   24: aload_0
    //   25: iload_1
    //   26: putfield mIndeterminate : Z
    //   29: iload_1
    //   30: ifeq -> 48
    //   33: aload_0
    //   34: aload_0
    //   35: getfield mIndeterminateDrawable : Landroid/graphics/drawable/Drawable;
    //   38: invokespecial swapCurrentDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   41: aload_0
    //   42: invokevirtual startAnimation : ()V
    //   45: goto -> 60
    //   48: aload_0
    //   49: aload_0
    //   50: getfield mProgressDrawable : Landroid/graphics/drawable/Drawable;
    //   53: invokespecial swapCurrentDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   56: aload_0
    //   57: invokevirtual stopAnimation : ()V
    //   60: aload_0
    //   61: monitorexit
    //   62: return
    //   63: astore_2
    //   64: aload_0
    //   65: monitorexit
    //   66: aload_2
    //   67: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #669	-> 2
    //   #670	-> 24
    //   #672	-> 29
    //   #674	-> 33
    //   #675	-> 41
    //   #677	-> 48
    //   #678	-> 56
    //   #681	-> 60
    //   #668	-> 63
    // Exception table:
    //   from	to	target	type
    //   2	16	63	finally
    //   16	24	63	finally
    //   24	29	63	finally
    //   33	41	63	finally
    //   41	45	63	finally
    //   48	56	63	finally
    //   56	60	63	finally
  }
  
  private void swapCurrentDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mCurrentDrawable;
    this.mCurrentDrawable = paramDrawable;
    if (drawable != paramDrawable) {
      if (drawable != null)
        drawable.setVisible(false, false); 
      paramDrawable = this.mCurrentDrawable;
      if (paramDrawable != null) {
        boolean bool;
        if (getWindowVisibility() == 0 && isShown()) {
          bool = true;
        } else {
          bool = false;
        } 
        paramDrawable.setVisible(bool, false);
      } 
    } 
  }
  
  public Drawable getIndeterminateDrawable() {
    return this.mIndeterminateDrawable;
  }
  
  public void setIndeterminateDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mIndeterminateDrawable;
    if (drawable != paramDrawable) {
      if (drawable != null) {
        drawable.setCallback(null);
        unscheduleDrawable(this.mIndeterminateDrawable);
      } 
      this.mIndeterminateDrawable = paramDrawable;
      if (paramDrawable != null) {
        paramDrawable.setCallback(this);
        paramDrawable.setLayoutDirection(getLayoutDirection());
        if (paramDrawable.isStateful())
          paramDrawable.setState(getDrawableState()); 
        applyIndeterminateTint();
      } 
      if (this.mIndeterminate) {
        swapCurrentDrawable(paramDrawable);
        postInvalidate();
      } 
    } 
  }
  
  @RemotableViewMethod
  public void setIndeterminateTintList(ColorStateList paramColorStateList) {
    if (this.mProgressTintInfo == null)
      this.mProgressTintInfo = new ProgressTintInfo(); 
    this.mProgressTintInfo.mIndeterminateTintList = paramColorStateList;
    this.mProgressTintInfo.mHasIndeterminateTint = true;
    applyIndeterminateTint();
  }
  
  public ColorStateList getIndeterminateTintList() {
    ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
    if (progressTintInfo != null) {
      ColorStateList colorStateList = progressTintInfo.mIndeterminateTintList;
    } else {
      progressTintInfo = null;
    } 
    return (ColorStateList)progressTintInfo;
  }
  
  public void setIndeterminateTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setIndeterminateTintBlendMode((BlendMode)paramMode);
  }
  
  public void setIndeterminateTintBlendMode(BlendMode paramBlendMode) {
    if (this.mProgressTintInfo == null)
      this.mProgressTintInfo = new ProgressTintInfo(); 
    this.mProgressTintInfo.mIndeterminateBlendMode = paramBlendMode;
    this.mProgressTintInfo.mHasIndeterminateTintMode = true;
    applyIndeterminateTint();
  }
  
  public PorterDuff.Mode getIndeterminateTintMode() {
    BlendMode blendMode = getIndeterminateTintBlendMode();
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getIndeterminateTintBlendMode() {
    ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
    if (progressTintInfo != null) {
      BlendMode blendMode = progressTintInfo.mIndeterminateBlendMode;
    } else {
      progressTintInfo = null;
    } 
    return (BlendMode)progressTintInfo;
  }
  
  private void applyIndeterminateTint() {
    if (this.mIndeterminateDrawable != null && this.mProgressTintInfo != null) {
      ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
      if (progressTintInfo.mHasIndeterminateTint || progressTintInfo.mHasIndeterminateTintMode) {
        this.mIndeterminateDrawable = this.mIndeterminateDrawable.mutate();
        if (progressTintInfo.mHasIndeterminateTint)
          this.mIndeterminateDrawable.setTintList(progressTintInfo.mIndeterminateTintList); 
        if (progressTintInfo.mHasIndeterminateTintMode)
          this.mIndeterminateDrawable.setTintBlendMode(progressTintInfo.mIndeterminateBlendMode); 
        if (this.mIndeterminateDrawable.isStateful())
          this.mIndeterminateDrawable.setState(getDrawableState()); 
      } 
    } 
  }
  
  public void setIndeterminateDrawableTiled(Drawable paramDrawable) {
    Drawable drawable = paramDrawable;
    if (paramDrawable != null)
      drawable = tileifyIndeterminate(paramDrawable); 
    setIndeterminateDrawable(drawable);
  }
  
  public Drawable getProgressDrawable() {
    return this.mProgressDrawable;
  }
  
  public void setProgressDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mProgressDrawable;
    if (drawable != paramDrawable) {
      if (drawable != null) {
        drawable.setCallback(null);
        unscheduleDrawable(this.mProgressDrawable);
      } 
      this.mProgressDrawable = paramDrawable;
      if (paramDrawable != null) {
        paramDrawable.setCallback(this);
        paramDrawable.setLayoutDirection(getLayoutDirection());
        if (paramDrawable.isStateful())
          paramDrawable.setState(getDrawableState()); 
        int i = paramDrawable.getMinimumHeight();
        if (this.mMaxHeight < i) {
          this.mMaxHeight = i;
          requestLayout();
        } 
        applyProgressTints();
      } 
      if (!this.mIndeterminate) {
        swapCurrentDrawable(paramDrawable);
        postInvalidate();
      } 
      updateDrawableBounds(getWidth(), getHeight());
      updateDrawableState();
      doRefreshProgress(16908301, this.mProgress, false, false, false);
      doRefreshProgress(16908303, this.mSecondaryProgress, false, false, false);
    } 
  }
  
  public boolean getMirrorForRtl() {
    return this.mMirrorForRtl;
  }
  
  private void applyProgressTints() {
    if (this.mProgressDrawable != null && this.mProgressTintInfo != null) {
      applyPrimaryProgressTint();
      applyProgressBackgroundTint();
      applySecondaryProgressTint();
    } 
  }
  
  private void applyPrimaryProgressTint() {
    if (this.mProgressTintInfo.mHasProgressTint || this.mProgressTintInfo.mHasProgressTintMode) {
      Drawable drawable = getTintTarget(16908301, true);
      if (drawable != null) {
        if (this.mProgressTintInfo.mHasProgressTint)
          drawable.setTintList(this.mProgressTintInfo.mProgressTintList); 
        if (this.mProgressTintInfo.mHasProgressTintMode)
          drawable.setTintBlendMode(this.mProgressTintInfo.mProgressBlendMode); 
        if (drawable.isStateful())
          drawable.setState(getDrawableState()); 
      } 
    } 
  }
  
  private void applyProgressBackgroundTint() {
    if (this.mProgressTintInfo.mHasProgressBackgroundTint || this.mProgressTintInfo.mHasProgressBackgroundTintMode) {
      Drawable drawable = getTintTarget(16908288, false);
      if (drawable != null) {
        if (this.mProgressTintInfo.mHasProgressBackgroundTint)
          drawable.setTintList(this.mProgressTintInfo.mProgressBackgroundTintList); 
        if (this.mProgressTintInfo.mHasProgressBackgroundTintMode)
          drawable.setTintBlendMode(this.mProgressTintInfo.mProgressBackgroundBlendMode); 
        if (drawable.isStateful())
          drawable.setState(getDrawableState()); 
      } 
    } 
  }
  
  private void applySecondaryProgressTint() {
    if (this.mProgressTintInfo.mHasSecondaryProgressTint || this.mProgressTintInfo.mHasSecondaryProgressTintMode) {
      Drawable drawable = getTintTarget(16908303, false);
      if (drawable != null) {
        if (this.mProgressTintInfo.mHasSecondaryProgressTint)
          drawable.setTintList(this.mProgressTintInfo.mSecondaryProgressTintList); 
        if (this.mProgressTintInfo.mHasSecondaryProgressTintMode)
          drawable.setTintBlendMode(this.mProgressTintInfo.mSecondaryProgressBlendMode); 
        if (drawable.isStateful())
          drawable.setState(getDrawableState()); 
      } 
    } 
  }
  
  @RemotableViewMethod
  public void setProgressTintList(ColorStateList paramColorStateList) {
    if (this.mProgressTintInfo == null)
      this.mProgressTintInfo = new ProgressTintInfo(); 
    this.mProgressTintInfo.mProgressTintList = paramColorStateList;
    this.mProgressTintInfo.mHasProgressTint = true;
    if (this.mProgressDrawable != null)
      applyPrimaryProgressTint(); 
  }
  
  public ColorStateList getProgressTintList() {
    ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
    if (progressTintInfo != null) {
      ColorStateList colorStateList = progressTintInfo.mProgressTintList;
    } else {
      progressTintInfo = null;
    } 
    return (ColorStateList)progressTintInfo;
  }
  
  public void setProgressTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setProgressTintBlendMode((BlendMode)paramMode);
  }
  
  public void setProgressTintBlendMode(BlendMode paramBlendMode) {
    if (this.mProgressTintInfo == null)
      this.mProgressTintInfo = new ProgressTintInfo(); 
    this.mProgressTintInfo.mProgressBlendMode = paramBlendMode;
    this.mProgressTintInfo.mHasProgressTintMode = true;
    if (this.mProgressDrawable != null)
      applyPrimaryProgressTint(); 
  }
  
  public PorterDuff.Mode getProgressTintMode() {
    BlendMode blendMode = getProgressTintBlendMode();
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getProgressTintBlendMode() {
    ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
    if (progressTintInfo != null) {
      BlendMode blendMode = progressTintInfo.mProgressBlendMode;
    } else {
      progressTintInfo = null;
    } 
    return (BlendMode)progressTintInfo;
  }
  
  @RemotableViewMethod
  public void setProgressBackgroundTintList(ColorStateList paramColorStateList) {
    if (this.mProgressTintInfo == null)
      this.mProgressTintInfo = new ProgressTintInfo(); 
    this.mProgressTintInfo.mProgressBackgroundTintList = paramColorStateList;
    this.mProgressTintInfo.mHasProgressBackgroundTint = true;
    if (this.mProgressDrawable != null)
      applyProgressBackgroundTint(); 
  }
  
  public ColorStateList getProgressBackgroundTintList() {
    ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
    if (progressTintInfo != null) {
      ColorStateList colorStateList = progressTintInfo.mProgressBackgroundTintList;
    } else {
      progressTintInfo = null;
    } 
    return (ColorStateList)progressTintInfo;
  }
  
  public void setProgressBackgroundTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setProgressBackgroundTintBlendMode((BlendMode)paramMode);
  }
  
  public void setProgressBackgroundTintBlendMode(BlendMode paramBlendMode) {
    if (this.mProgressTintInfo == null)
      this.mProgressTintInfo = new ProgressTintInfo(); 
    this.mProgressTintInfo.mProgressBackgroundBlendMode = paramBlendMode;
    this.mProgressTintInfo.mHasProgressBackgroundTintMode = true;
    if (this.mProgressDrawable != null)
      applyProgressBackgroundTint(); 
  }
  
  public PorterDuff.Mode getProgressBackgroundTintMode() {
    BlendMode blendMode = getProgressBackgroundTintBlendMode();
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getProgressBackgroundTintBlendMode() {
    ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
    if (progressTintInfo != null) {
      BlendMode blendMode = progressTintInfo.mProgressBackgroundBlendMode;
    } else {
      progressTintInfo = null;
    } 
    return (BlendMode)progressTintInfo;
  }
  
  public void setSecondaryProgressTintList(ColorStateList paramColorStateList) {
    if (this.mProgressTintInfo == null)
      this.mProgressTintInfo = new ProgressTintInfo(); 
    this.mProgressTintInfo.mSecondaryProgressTintList = paramColorStateList;
    this.mProgressTintInfo.mHasSecondaryProgressTint = true;
    if (this.mProgressDrawable != null)
      applySecondaryProgressTint(); 
  }
  
  public ColorStateList getSecondaryProgressTintList() {
    ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
    if (progressTintInfo != null) {
      ColorStateList colorStateList = progressTintInfo.mSecondaryProgressTintList;
    } else {
      progressTintInfo = null;
    } 
    return (ColorStateList)progressTintInfo;
  }
  
  public void setSecondaryProgressTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setSecondaryProgressTintBlendMode((BlendMode)paramMode);
  }
  
  public void setSecondaryProgressTintBlendMode(BlendMode paramBlendMode) {
    if (this.mProgressTintInfo == null)
      this.mProgressTintInfo = new ProgressTintInfo(); 
    this.mProgressTintInfo.mSecondaryProgressBlendMode = paramBlendMode;
    this.mProgressTintInfo.mHasSecondaryProgressTintMode = true;
    if (this.mProgressDrawable != null)
      applySecondaryProgressTint(); 
  }
  
  public PorterDuff.Mode getSecondaryProgressTintMode() {
    BlendMode blendMode = getSecondaryProgressTintBlendMode();
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getSecondaryProgressTintBlendMode() {
    ProgressTintInfo progressTintInfo = this.mProgressTintInfo;
    if (progressTintInfo != null) {
      BlendMode blendMode = progressTintInfo.mSecondaryProgressBlendMode;
    } else {
      progressTintInfo = null;
    } 
    return (BlendMode)progressTintInfo;
  }
  
  private Drawable getTintTarget(int paramInt, boolean paramBoolean) {
    Drawable drawable1 = null, drawable2 = null;
    Drawable drawable3 = this.mProgressDrawable;
    if (drawable3 != null) {
      this.mProgressDrawable = drawable3.mutate();
      if (drawable3 instanceof LayerDrawable)
        drawable2 = ((LayerDrawable)drawable3).findDrawableByLayerId(paramInt); 
      drawable1 = drawable2;
      if (paramBoolean) {
        drawable1 = drawable2;
        if (drawable2 == null)
          drawable1 = drawable3; 
      } 
    } 
    return drawable1;
  }
  
  public void setProgressDrawableTiled(Drawable paramDrawable) {
    Drawable drawable = paramDrawable;
    if (paramDrawable != null)
      drawable = tileify(paramDrawable, false); 
    setProgressDrawable(drawable);
  }
  
  public Drawable getCurrentDrawable() {
    return this.mCurrentDrawable;
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (paramDrawable == this.mProgressDrawable || paramDrawable == this.mIndeterminateDrawable || super.verifyDrawable(paramDrawable));
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mProgressDrawable;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
    drawable = this.mIndeterminateDrawable;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
  }
  
  public void onResolveDrawables(int paramInt) {
    Drawable drawable = this.mCurrentDrawable;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
    drawable = this.mIndeterminateDrawable;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
    drawable = this.mProgressDrawable;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
  }
  
  public void postInvalidate() {
    if (!this.mNoInvalidate)
      super.postInvalidate(); 
  }
  
  class RefreshProgressRunnable implements Runnable {
    final ProgressBar this$0;
    
    private RefreshProgressRunnable() {}
    
    public void run() {
      synchronized (ProgressBar.this) {
        int i = ProgressBar.this.mRefreshData.size();
        for (byte b = 0; b < i; b++) {
          ProgressBar.RefreshData refreshData = ProgressBar.this.mRefreshData.get(b);
          ProgressBar.this.doRefreshProgress(refreshData.id, refreshData.progress, refreshData.fromUser, true, refreshData.animate);
          refreshData.recycle();
        } 
        ProgressBar.this.mRefreshData.clear();
        ProgressBar.access$302(ProgressBar.this, false);
        return;
      } 
    }
  }
  
  class RefreshData {
    private static final int POOL_MAX = 24;
    
    private static final Pools.SynchronizedPool<RefreshData> sPool = new Pools.SynchronizedPool<>(24);
    
    public boolean animate;
    
    public boolean fromUser;
    
    public int id;
    
    public int progress;
    
    public static RefreshData obtain(int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2) {
      RefreshData refreshData1 = sPool.acquire();
      RefreshData refreshData2 = refreshData1;
      if (refreshData1 == null)
        refreshData2 = new RefreshData(); 
      refreshData2.id = param1Int1;
      refreshData2.progress = param1Int2;
      refreshData2.fromUser = param1Boolean1;
      refreshData2.animate = param1Boolean2;
      return refreshData2;
    }
    
    public void recycle() {
      sPool.release(this);
    }
  }
  
  private void doRefreshProgress(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mMax : I
    //   6: aload_0
    //   7: getfield mMin : I
    //   10: isub
    //   11: istore #6
    //   13: iload #6
    //   15: ifle -> 34
    //   18: iload_2
    //   19: aload_0
    //   20: getfield mMin : I
    //   23: isub
    //   24: i2f
    //   25: iload #6
    //   27: i2f
    //   28: fdiv
    //   29: fstore #7
    //   31: goto -> 37
    //   34: fconst_0
    //   35: fstore #7
    //   37: iload_1
    //   38: ldc_w 16908301
    //   41: if_icmpne -> 50
    //   44: iconst_1
    //   45: istore #6
    //   47: goto -> 53
    //   50: iconst_0
    //   51: istore #6
    //   53: iload #6
    //   55: ifeq -> 112
    //   58: iload #5
    //   60: ifeq -> 112
    //   63: aload_0
    //   64: aload_0
    //   65: getfield VISUAL_PROGRESS : Landroid/util/FloatProperty;
    //   68: iconst_1
    //   69: newarray float
    //   71: dup
    //   72: iconst_0
    //   73: fload #7
    //   75: fastore
    //   76: invokestatic ofFloat : (Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;
    //   79: astore #8
    //   81: aload #8
    //   83: iconst_1
    //   84: invokevirtual setAutoCancel : (Z)V
    //   87: aload #8
    //   89: ldc2_w 80
    //   92: invokevirtual setDuration : (J)Landroid/animation/ObjectAnimator;
    //   95: pop
    //   96: aload #8
    //   98: getstatic android/widget/ProgressBar.PROGRESS_ANIM_INTERPOLATOR : Landroid/view/animation/DecelerateInterpolator;
    //   101: invokevirtual setInterpolator : (Landroid/animation/TimeInterpolator;)V
    //   104: aload #8
    //   106: invokevirtual start : ()V
    //   109: goto -> 119
    //   112: aload_0
    //   113: iload_1
    //   114: fload #7
    //   116: invokespecial setVisualProgress : (IF)V
    //   119: iload #6
    //   121: ifeq -> 137
    //   124: iload #4
    //   126: ifeq -> 137
    //   129: aload_0
    //   130: fload #7
    //   132: iload_3
    //   133: iload_2
    //   134: invokevirtual onProgressRefresh : (FZI)V
    //   137: aload_0
    //   138: monitorexit
    //   139: return
    //   140: astore #8
    //   142: aload_0
    //   143: monitorexit
    //   144: aload #8
    //   146: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1538	-> 2
    //   #1539	-> 13
    //   #1540	-> 37
    //   #1542	-> 53
    //   #1543	-> 63
    //   #1544	-> 81
    //   #1545	-> 87
    //   #1546	-> 96
    //   #1547	-> 104
    //   #1548	-> 109
    //   #1549	-> 112
    //   #1552	-> 119
    //   #1553	-> 129
    //   #1555	-> 137
    //   #1537	-> 140
    // Exception table:
    //   from	to	target	type
    //   2	13	140	finally
    //   18	31	140	finally
    //   63	81	140	finally
    //   81	87	140	finally
    //   87	96	140	finally
    //   96	104	140	finally
    //   104	109	140	finally
    //   112	119	140	finally
    //   129	137	140	finally
  }
  
  private float getPercent(int paramInt) {
    float f1 = getMax();
    float f2 = getMin();
    float f3 = paramInt;
    f1 -= f2;
    if (f1 <= 0.0F)
      return 0.0F; 
    f2 = (f3 - f2) / f1;
    return Math.max(0.0F, Math.min(1.0F, f2));
  }
  
  private CharSequence formatStateDescription(int paramInt) {
    NumberFormat numberFormat = NumberFormat.getPercentInstance((this.mContext.getResources().getConfiguration()).locale);
    return numberFormat.format(getPercent(paramInt));
  }
  
  public void setStateDescription(CharSequence paramCharSequence) {
    this.mCustomStateDescription = paramCharSequence;
    if (paramCharSequence == null) {
      super.setStateDescription(formatStateDescription(this.mProgress));
    } else {
      super.setStateDescription(paramCharSequence);
    } 
  }
  
  void onProgressRefresh(float paramFloat, boolean paramBoolean, int paramInt) {
    if (AccessibilityManager.getInstance(this.mContext).isEnabled() && this.mCustomStateDescription == null)
      super.setStateDescription(formatStateDescription(this.mProgress)); 
  }
  
  private void setVisualProgress(int paramInt, float paramFloat) {
    this.mVisualProgress = paramFloat;
    Drawable drawable1 = this.mCurrentDrawable;
    Drawable drawable2 = drawable1;
    if (drawable1 instanceof LayerDrawable) {
      drawable1 = ((LayerDrawable)drawable1).findDrawableByLayerId(paramInt);
      drawable2 = drawable1;
      if (drawable1 == null)
        drawable2 = this.mCurrentDrawable; 
    } 
    if (drawable2 != null) {
      int i = (int)(10000.0F * paramFloat);
      drawable2.setLevel(i);
    } else {
      invalidate();
    } 
    onVisualProgressChanged(paramInt, paramFloat);
  }
  
  void onVisualProgressChanged(int paramInt, float paramFloat) {}
  
  private void refreshProgress(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mUiThreadId : J
    //   6: invokestatic currentThread : ()Ljava/lang/Thread;
    //   9: invokevirtual getId : ()J
    //   12: lcmp
    //   13: ifne -> 29
    //   16: aload_0
    //   17: iload_1
    //   18: iload_2
    //   19: iload_3
    //   20: iconst_1
    //   21: iload #4
    //   23: invokespecial doRefreshProgress : (IIZZZ)V
    //   26: goto -> 102
    //   29: aload_0
    //   30: getfield mRefreshProgressRunnable : Landroid/widget/ProgressBar$RefreshProgressRunnable;
    //   33: ifnonnull -> 54
    //   36: new android/widget/ProgressBar$RefreshProgressRunnable
    //   39: astore #5
    //   41: aload #5
    //   43: aload_0
    //   44: aconst_null
    //   45: invokespecial <init> : (Landroid/widget/ProgressBar;Landroid/widget/ProgressBar$1;)V
    //   48: aload_0
    //   49: aload #5
    //   51: putfield mRefreshProgressRunnable : Landroid/widget/ProgressBar$RefreshProgressRunnable;
    //   54: iload_1
    //   55: iload_2
    //   56: iload_3
    //   57: iload #4
    //   59: invokestatic obtain : (IIZZ)Landroid/widget/ProgressBar$RefreshData;
    //   62: astore #5
    //   64: aload_0
    //   65: getfield mRefreshData : Ljava/util/ArrayList;
    //   68: aload #5
    //   70: invokevirtual add : (Ljava/lang/Object;)Z
    //   73: pop
    //   74: aload_0
    //   75: getfield mAttached : Z
    //   78: ifeq -> 102
    //   81: aload_0
    //   82: getfield mRefreshIsPosted : Z
    //   85: ifne -> 102
    //   88: aload_0
    //   89: aload_0
    //   90: getfield mRefreshProgressRunnable : Landroid/widget/ProgressBar$RefreshProgressRunnable;
    //   93: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   96: pop
    //   97: aload_0
    //   98: iconst_1
    //   99: putfield mRefreshIsPosted : Z
    //   102: aload_0
    //   103: monitorexit
    //   104: return
    //   105: astore #5
    //   107: aload_0
    //   108: monitorexit
    //   109: aload #5
    //   111: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1652	-> 2
    //   #1653	-> 16
    //   #1655	-> 29
    //   #1656	-> 36
    //   #1659	-> 54
    //   #1660	-> 64
    //   #1661	-> 74
    //   #1662	-> 88
    //   #1663	-> 97
    //   #1666	-> 102
    //   #1651	-> 105
    // Exception table:
    //   from	to	target	type
    //   2	16	105	finally
    //   16	26	105	finally
    //   29	36	105	finally
    //   36	54	105	finally
    //   54	64	105	finally
    //   64	74	105	finally
    //   74	88	105	finally
    //   88	97	105	finally
    //   97	102	105	finally
  }
  
  @RemotableViewMethod
  public void setProgress(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: iconst_0
    //   5: iconst_0
    //   6: invokevirtual setProgressInternal : (IZZ)Z
    //   9: pop
    //   10: aload_0
    //   11: monitorexit
    //   12: return
    //   13: astore_2
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_2
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1685	-> 2
    //   #1686	-> 10
    //   #1684	-> 13
    // Exception table:
    //   from	to	target	type
    //   2	10	13	finally
  }
  
  public void setProgress(int paramInt, boolean paramBoolean) {
    setProgressInternal(paramInt, false, paramBoolean);
  }
  
  @RemotableViewMethod
  boolean setProgressInternal(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIndeterminate : Z
    //   6: istore #4
    //   8: iload #4
    //   10: ifeq -> 17
    //   13: aload_0
    //   14: monitorexit
    //   15: iconst_0
    //   16: ireturn
    //   17: iload_1
    //   18: aload_0
    //   19: getfield mMin : I
    //   22: aload_0
    //   23: getfield mMax : I
    //   26: invokestatic constrain : (III)I
    //   29: istore_1
    //   30: aload_0
    //   31: getfield mProgress : I
    //   34: istore #5
    //   36: iload_1
    //   37: iload #5
    //   39: if_icmpne -> 46
    //   42: aload_0
    //   43: monitorexit
    //   44: iconst_0
    //   45: ireturn
    //   46: aload_0
    //   47: iload_1
    //   48: putfield mProgress : I
    //   51: aload_0
    //   52: ldc_w 16908301
    //   55: iload_1
    //   56: iload_2
    //   57: iload_3
    //   58: invokespecial refreshProgress : (IIZZ)V
    //   61: aload_0
    //   62: monitorexit
    //   63: iconst_1
    //   64: ireturn
    //   65: astore #6
    //   67: aload_0
    //   68: monitorexit
    //   69: aload #6
    //   71: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1706	-> 2
    //   #1708	-> 13
    //   #1711	-> 17
    //   #1713	-> 30
    //   #1715	-> 42
    //   #1718	-> 46
    //   #1719	-> 51
    //   #1720	-> 61
    //   #1705	-> 65
    // Exception table:
    //   from	to	target	type
    //   2	8	65	finally
    //   17	30	65	finally
    //   30	36	65	finally
    //   46	51	65	finally
    //   51	61	65	finally
  }
  
  @RemotableViewMethod
  public void setSecondaryProgress(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIndeterminate : Z
    //   6: istore_2
    //   7: iload_2
    //   8: ifeq -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: iload_1
    //   15: istore_3
    //   16: iload_1
    //   17: aload_0
    //   18: getfield mMin : I
    //   21: if_icmpge -> 29
    //   24: aload_0
    //   25: getfield mMin : I
    //   28: istore_3
    //   29: iload_3
    //   30: istore_1
    //   31: iload_3
    //   32: aload_0
    //   33: getfield mMax : I
    //   36: if_icmple -> 44
    //   39: aload_0
    //   40: getfield mMax : I
    //   43: istore_1
    //   44: iload_1
    //   45: aload_0
    //   46: getfield mSecondaryProgress : I
    //   49: if_icmpeq -> 67
    //   52: aload_0
    //   53: iload_1
    //   54: putfield mSecondaryProgress : I
    //   57: aload_0
    //   58: ldc_w 16908303
    //   61: iload_1
    //   62: iconst_0
    //   63: iconst_0
    //   64: invokespecial refreshProgress : (IIZZ)V
    //   67: aload_0
    //   68: monitorexit
    //   69: return
    //   70: astore #4
    //   72: aload_0
    //   73: monitorexit
    //   74: aload #4
    //   76: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1738	-> 2
    //   #1739	-> 11
    //   #1742	-> 14
    //   #1743	-> 24
    //   #1746	-> 29
    //   #1747	-> 39
    //   #1750	-> 44
    //   #1751	-> 52
    //   #1752	-> 57
    //   #1754	-> 67
    //   #1737	-> 70
    // Exception table:
    //   from	to	target	type
    //   2	7	70	finally
    //   16	24	70	finally
    //   24	29	70	finally
    //   31	39	70	finally
    //   39	44	70	finally
    //   44	52	70	finally
    //   52	57	70	finally
    //   57	67	70	finally
  }
  
  @ExportedProperty(category = "progress")
  public int getProgress() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIndeterminate : Z
    //   6: ifeq -> 14
    //   9: iconst_0
    //   10: istore_1
    //   11: goto -> 19
    //   14: aload_0
    //   15: getfield mProgress : I
    //   18: istore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: iload_1
    //   22: ireturn
    //   23: astore_2
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_2
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1771	-> 2
    //   #1771	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	9	23	finally
    //   14	19	23	finally
  }
  
  @ExportedProperty(category = "progress")
  public int getSecondaryProgress() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIndeterminate : Z
    //   6: ifeq -> 14
    //   9: iconst_0
    //   10: istore_1
    //   11: goto -> 19
    //   14: aload_0
    //   15: getfield mSecondaryProgress : I
    //   18: istore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: iload_1
    //   22: ireturn
    //   23: astore_2
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_2
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1789	-> 2
    //   #1789	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	9	23	finally
    //   14	19	23	finally
  }
  
  @ExportedProperty(category = "progress")
  public int getMin() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mMin : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1804	-> 2
    //   #1804	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  @ExportedProperty(category = "progress")
  public int getMax() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mMax : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1819	-> 2
    //   #1819	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  @RemotableViewMethod
  public void setMin(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: istore_2
    //   4: aload_0
    //   5: getfield mMaxInitialized : Z
    //   8: ifeq -> 26
    //   11: iload_1
    //   12: istore_2
    //   13: iload_1
    //   14: aload_0
    //   15: getfield mMax : I
    //   18: if_icmple -> 26
    //   21: aload_0
    //   22: getfield mMax : I
    //   25: istore_2
    //   26: aload_0
    //   27: iconst_1
    //   28: putfield mMinInitialized : Z
    //   31: aload_0
    //   32: getfield mMaxInitialized : Z
    //   35: ifeq -> 84
    //   38: iload_2
    //   39: aload_0
    //   40: getfield mMin : I
    //   43: if_icmpeq -> 84
    //   46: aload_0
    //   47: iload_2
    //   48: putfield mMin : I
    //   51: aload_0
    //   52: invokevirtual postInvalidate : ()V
    //   55: aload_0
    //   56: getfield mProgress : I
    //   59: iload_2
    //   60: if_icmpge -> 68
    //   63: aload_0
    //   64: iload_2
    //   65: putfield mProgress : I
    //   68: aload_0
    //   69: ldc_w 16908301
    //   72: aload_0
    //   73: getfield mProgress : I
    //   76: iconst_0
    //   77: iconst_0
    //   78: invokespecial refreshProgress : (IIZZ)V
    //   81: goto -> 89
    //   84: aload_0
    //   85: iload_2
    //   86: putfield mMin : I
    //   89: aload_0
    //   90: monitorexit
    //   91: return
    //   92: astore_3
    //   93: aload_0
    //   94: monitorexit
    //   95: aload_3
    //   96: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1833	-> 2
    //   #1834	-> 11
    //   #1835	-> 21
    //   #1838	-> 26
    //   #1839	-> 31
    //   #1840	-> 46
    //   #1841	-> 51
    //   #1843	-> 55
    //   #1844	-> 63
    //   #1846	-> 68
    //   #1848	-> 84
    //   #1850	-> 89
    //   #1832	-> 92
    // Exception table:
    //   from	to	target	type
    //   4	11	92	finally
    //   13	21	92	finally
    //   21	26	92	finally
    //   26	31	92	finally
    //   31	46	92	finally
    //   46	51	92	finally
    //   51	55	92	finally
    //   55	63	92	finally
    //   63	68	92	finally
    //   68	81	92	finally
    //   84	89	92	finally
  }
  
  @RemotableViewMethod
  public void setMax(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: istore_2
    //   4: aload_0
    //   5: getfield mMinInitialized : Z
    //   8: ifeq -> 26
    //   11: iload_1
    //   12: istore_2
    //   13: iload_1
    //   14: aload_0
    //   15: getfield mMin : I
    //   18: if_icmpge -> 26
    //   21: aload_0
    //   22: getfield mMin : I
    //   25: istore_2
    //   26: aload_0
    //   27: iconst_1
    //   28: putfield mMaxInitialized : Z
    //   31: aload_0
    //   32: getfield mMinInitialized : Z
    //   35: ifeq -> 84
    //   38: iload_2
    //   39: aload_0
    //   40: getfield mMax : I
    //   43: if_icmpeq -> 84
    //   46: aload_0
    //   47: iload_2
    //   48: putfield mMax : I
    //   51: aload_0
    //   52: invokevirtual postInvalidate : ()V
    //   55: aload_0
    //   56: getfield mProgress : I
    //   59: iload_2
    //   60: if_icmple -> 68
    //   63: aload_0
    //   64: iload_2
    //   65: putfield mProgress : I
    //   68: aload_0
    //   69: ldc_w 16908301
    //   72: aload_0
    //   73: getfield mProgress : I
    //   76: iconst_0
    //   77: iconst_0
    //   78: invokespecial refreshProgress : (IIZZ)V
    //   81: goto -> 89
    //   84: aload_0
    //   85: iload_2
    //   86: putfield mMax : I
    //   89: aload_0
    //   90: monitorexit
    //   91: return
    //   92: astore_3
    //   93: aload_0
    //   94: monitorexit
    //   95: aload_3
    //   96: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1863	-> 2
    //   #1864	-> 11
    //   #1865	-> 21
    //   #1868	-> 26
    //   #1869	-> 31
    //   #1870	-> 46
    //   #1871	-> 51
    //   #1873	-> 55
    //   #1874	-> 63
    //   #1876	-> 68
    //   #1878	-> 84
    //   #1880	-> 89
    //   #1862	-> 92
    // Exception table:
    //   from	to	target	type
    //   4	11	92	finally
    //   13	21	92	finally
    //   21	26	92	finally
    //   26	31	92	finally
    //   31	46	92	finally
    //   46	51	92	finally
    //   51	55	92	finally
    //   55	63	92	finally
    //   63	68	92	finally
    //   68	81	92	finally
    //   84	89	92	finally
  }
  
  public final void incrementProgressBy(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_0
    //   4: getfield mProgress : I
    //   7: iload_1
    //   8: iadd
    //   9: invokevirtual setProgress : (I)V
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_2
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1890	-> 2
    //   #1891	-> 12
    //   #1889	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	12	15	finally
  }
  
  public final void incrementSecondaryProgressBy(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_0
    //   4: getfield mSecondaryProgress : I
    //   7: iload_1
    //   8: iadd
    //   9: invokevirtual setSecondaryProgress : (I)V
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_2
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1901	-> 2
    //   #1902	-> 12
    //   #1900	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	12	15	finally
  }
  
  void startAnimation() {
    if (getVisibility() != 0 || getWindowVisibility() != 0)
      return; 
    if (this.mIndeterminateDrawable instanceof Animatable) {
      this.mShouldStartAnimationDrawable = true;
      this.mHasAnimation = false;
    } else {
      this.mHasAnimation = true;
      if (this.mInterpolator == null)
        this.mInterpolator = new LinearInterpolator(); 
      Transformation transformation = this.mTransformation;
      if (transformation == null) {
        this.mTransformation = new Transformation();
      } else {
        transformation.clear();
      } 
      AlphaAnimation alphaAnimation = this.mAnimation;
      if (alphaAnimation == null) {
        this.mAnimation = new AlphaAnimation(0.0F, 1.0F);
      } else {
        alphaAnimation.reset();
      } 
      this.mAnimation.setRepeatMode(this.mBehavior);
      this.mAnimation.setRepeatCount(-1);
      this.mAnimation.setDuration(this.mDuration);
      this.mAnimation.setInterpolator(this.mInterpolator);
      this.mAnimation.setStartTime(-1L);
    } 
    postInvalidate();
  }
  
  void stopAnimation() {
    this.mHasAnimation = false;
    Drawable drawable = this.mIndeterminateDrawable;
    if (drawable instanceof Animatable) {
      ((Animatable)drawable).stop();
      this.mShouldStartAnimationDrawable = false;
    } 
    postInvalidate();
  }
  
  public void setInterpolator(Context paramContext, int paramInt) {
    setInterpolator(AnimationUtils.loadInterpolator(paramContext, paramInt));
  }
  
  public void setInterpolator(Interpolator paramInterpolator) {
    this.mInterpolator = paramInterpolator;
  }
  
  public Interpolator getInterpolator() {
    return this.mInterpolator;
  }
  
  public void onVisibilityAggregated(boolean paramBoolean) {
    super.onVisibilityAggregated(paramBoolean);
    if (paramBoolean != this.mAggregatedIsVisible) {
      this.mAggregatedIsVisible = paramBoolean;
      if (this.mIndeterminate)
        if (paramBoolean) {
          startAnimation();
        } else {
          stopAnimation();
        }  
      Drawable drawable = this.mCurrentDrawable;
      if (drawable != null)
        drawable.setVisible(paramBoolean, false); 
    } 
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    if (!this.mInDrawing) {
      Rect rect;
      if (verifyDrawable(paramDrawable)) {
        rect = paramDrawable.getBounds();
        int i = this.mScrollX + this.mPaddingLeft;
        int j = this.mScrollY + this.mPaddingTop;
        invalidate(rect.left + i, rect.top + j, rect.right + i, rect.bottom + j);
      } else {
        super.invalidateDrawable((Drawable)rect);
      } 
    } 
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    updateDrawableBounds(paramInt1, paramInt2);
  }
  
  private void updateDrawableBounds(int paramInt1, int paramInt2) {
    int i = paramInt1 - this.mPaddingRight + this.mPaddingLeft;
    int j = paramInt2 - this.mPaddingTop + this.mPaddingBottom;
    paramInt1 = i;
    paramInt2 = j;
    byte b = 0;
    boolean bool = false;
    Drawable drawable = this.mIndeterminateDrawable;
    int k = paramInt1, m = paramInt2;
    if (drawable != null) {
      int n = paramInt1;
      m = paramInt2;
      int i1 = b;
      k = bool;
      if (this.mOnlyIndeterminate) {
        n = paramInt1;
        m = paramInt2;
        i1 = b;
        k = bool;
        if (!(drawable instanceof AnimationDrawable)) {
          m = drawable.getIntrinsicWidth();
          k = this.mIndeterminateDrawable.getIntrinsicHeight();
          float f1 = m / k;
          float f2 = i / j;
          n = paramInt1;
          m = paramInt2;
          i1 = b;
          k = bool;
          if (f1 != f2)
            if (f2 > f1) {
              paramInt1 = (int)(j * f1);
              k = (i - paramInt1) / 2;
              n = k + paramInt1;
              m = paramInt2;
              i1 = b;
            } else {
              paramInt2 = (int)(i * 1.0F / f1);
              i1 = (j - paramInt2) / 2;
              m = i1 + paramInt2;
              k = bool;
              n = paramInt1;
            }  
        } 
      } 
      paramInt1 = n;
      paramInt2 = k;
      if (isLayoutRtl()) {
        paramInt1 = n;
        paramInt2 = k;
        if (this.mMirrorForRtl) {
          paramInt2 = i - n;
          paramInt1 = i - k;
        } 
      } 
      this.mIndeterminateDrawable.setBounds(paramInt2, i1, paramInt1, m);
      k = paramInt1;
    } 
    drawable = this.mProgressDrawable;
    if (drawable != null)
      drawable.setBounds(0, 0, k, m); 
  }
  
  protected void onDraw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial onDraw : (Landroid/graphics/Canvas;)V
    //   7: aload_0
    //   8: aload_1
    //   9: invokevirtual drawTrack : (Landroid/graphics/Canvas;)V
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2104	-> 2
    //   #2106	-> 7
    //   #2107	-> 12
    //   #2103	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
  }
  
  void drawTrack(Canvas paramCanvas) {
    Drawable drawable = this.mCurrentDrawable;
    if (drawable != null) {
      int i = paramCanvas.save();
      if (isLayoutRtl() && this.mMirrorForRtl) {
        paramCanvas.translate((getWidth() - this.mPaddingRight), this.mPaddingTop);
        paramCanvas.scale(-1.0F, 1.0F);
      } else {
        paramCanvas.translate(this.mPaddingLeft, this.mPaddingTop);
      } 
      long l = getDrawingTime();
      if (this.mHasAnimation) {
        this.mAnimation.getTransformation(l, this.mTransformation);
        float f = this.mTransformation.getAlpha();
        try {
          this.mInDrawing = true;
          drawable.setLevel((int)(10000.0F * f));
          this.mInDrawing = false;
        } finally {
          this.mInDrawing = false;
        } 
      } 
      drawable.draw(paramCanvas);
      paramCanvas.restoreToCount(i);
      if (this.mShouldStartAnimationDrawable && drawable instanceof Animatable) {
        ((Animatable)drawable).start();
        this.mShouldStartAnimationDrawable = false;
      } 
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_0
    //   3: istore_3
    //   4: iconst_0
    //   5: istore #4
    //   7: aload_0
    //   8: getfield mCurrentDrawable : Landroid/graphics/drawable/Drawable;
    //   11: astore #5
    //   13: aload #5
    //   15: ifnull -> 59
    //   18: aload_0
    //   19: getfield mMinWidth : I
    //   22: aload_0
    //   23: getfield mMaxWidth : I
    //   26: aload #5
    //   28: invokevirtual getIntrinsicWidth : ()I
    //   31: invokestatic min : (II)I
    //   34: invokestatic max : (II)I
    //   37: istore_3
    //   38: aload_0
    //   39: getfield mMinHeight : I
    //   42: aload_0
    //   43: getfield mMaxHeight : I
    //   46: aload #5
    //   48: invokevirtual getIntrinsicHeight : ()I
    //   51: invokestatic min : (II)I
    //   54: invokestatic max : (II)I
    //   57: istore #4
    //   59: aload_0
    //   60: invokespecial updateDrawableState : ()V
    //   63: aload_0
    //   64: getfield mPaddingLeft : I
    //   67: istore #6
    //   69: aload_0
    //   70: getfield mPaddingRight : I
    //   73: istore #7
    //   75: aload_0
    //   76: getfield mPaddingTop : I
    //   79: istore #8
    //   81: aload_0
    //   82: getfield mPaddingBottom : I
    //   85: istore #9
    //   87: iload_3
    //   88: iload #6
    //   90: iload #7
    //   92: iadd
    //   93: iadd
    //   94: iload_1
    //   95: iconst_0
    //   96: invokestatic resolveSizeAndState : (III)I
    //   99: istore_1
    //   100: iload #4
    //   102: iload #8
    //   104: iload #9
    //   106: iadd
    //   107: iadd
    //   108: iload_2
    //   109: iconst_0
    //   110: invokestatic resolveSizeAndState : (III)I
    //   113: istore_2
    //   114: aload_0
    //   115: iload_1
    //   116: iload_2
    //   117: invokevirtual setMeasuredDimension : (II)V
    //   120: aload_0
    //   121: monitorexit
    //   122: return
    //   123: astore #5
    //   125: aload_0
    //   126: monitorexit
    //   127: aload #5
    //   129: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2151	-> 2
    //   #2152	-> 4
    //   #2154	-> 7
    //   #2155	-> 13
    //   #2156	-> 18
    //   #2157	-> 38
    //   #2160	-> 59
    //   #2162	-> 63
    //   #2163	-> 75
    //   #2165	-> 87
    //   #2166	-> 100
    //   #2167	-> 114
    //   #2168	-> 120
    //   #2150	-> 123
    // Exception table:
    //   from	to	target	type
    //   7	13	123	finally
    //   18	38	123	finally
    //   38	59	123	finally
    //   59	63	123	finally
    //   63	75	123	finally
    //   75	87	123	finally
    //   87	100	123	finally
    //   100	114	123	finally
    //   114	120	123	finally
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    updateDrawableState();
  }
  
  private void updateDrawableState() {
    boolean bool;
    int[] arrayOfInt = getDrawableState();
    int i = 0;
    Drawable drawable = this.mProgressDrawable;
    int j = i;
    if (drawable != null) {
      j = i;
      if (drawable.isStateful())
        j = false | drawable.setState(arrayOfInt); 
    } 
    drawable = this.mIndeterminateDrawable;
    i = j;
    if (drawable != null) {
      i = j;
      if (drawable.isStateful())
        bool = j | drawable.setState(arrayOfInt); 
    } 
    if (bool)
      invalidate(); 
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Drawable drawable = this.mProgressDrawable;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
    drawable = this.mIndeterminateDrawable;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState() {}
    
    private SavedState(ProgressBar this$0) {
      this.progress = this$0.readInt();
      this.secondaryProgress = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.progress);
      param1Parcel.writeInt(this.secondaryProgress);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    int progress;
    
    int secondaryProgress;
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    parcelable = new SavedState();
    ((SavedState)parcelable).progress = this.mProgress;
    ((SavedState)parcelable).secondaryProgress = this.mSecondaryProgress;
    return parcelable;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    setProgress(((SavedState)paramParcelable).progress);
    setSecondaryProgress(((SavedState)paramParcelable).secondaryProgress);
  }
  
  protected void onAttachedToWindow() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial onAttachedToWindow : ()V
    //   4: aload_0
    //   5: getfield mIndeterminate : Z
    //   8: ifeq -> 15
    //   11: aload_0
    //   12: invokevirtual startAnimation : ()V
    //   15: aload_0
    //   16: getfield mRefreshData : Ljava/util/ArrayList;
    //   19: ifnull -> 99
    //   22: aload_0
    //   23: monitorenter
    //   24: aload_0
    //   25: getfield mRefreshData : Ljava/util/ArrayList;
    //   28: invokevirtual size : ()I
    //   31: istore_1
    //   32: iconst_0
    //   33: istore_2
    //   34: iload_2
    //   35: iload_1
    //   36: if_icmpge -> 82
    //   39: aload_0
    //   40: getfield mRefreshData : Ljava/util/ArrayList;
    //   43: iload_2
    //   44: invokevirtual get : (I)Ljava/lang/Object;
    //   47: checkcast android/widget/ProgressBar$RefreshData
    //   50: astore_3
    //   51: aload_0
    //   52: aload_3
    //   53: getfield id : I
    //   56: aload_3
    //   57: getfield progress : I
    //   60: aload_3
    //   61: getfield fromUser : Z
    //   64: iconst_1
    //   65: aload_3
    //   66: getfield animate : Z
    //   69: invokespecial doRefreshProgress : (IIZZZ)V
    //   72: aload_3
    //   73: invokevirtual recycle : ()V
    //   76: iinc #2, 1
    //   79: goto -> 34
    //   82: aload_0
    //   83: getfield mRefreshData : Ljava/util/ArrayList;
    //   86: invokevirtual clear : ()V
    //   89: aload_0
    //   90: monitorexit
    //   91: goto -> 99
    //   94: astore_3
    //   95: aload_0
    //   96: monitorexit
    //   97: aload_3
    //   98: athrow
    //   99: aload_0
    //   100: iconst_1
    //   101: putfield mAttached : Z
    //   104: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2270	-> 0
    //   #2271	-> 4
    //   #2272	-> 11
    //   #2274	-> 15
    //   #2275	-> 22
    //   #2276	-> 24
    //   #2277	-> 32
    //   #2278	-> 39
    //   #2279	-> 51
    //   #2280	-> 72
    //   #2277	-> 76
    //   #2282	-> 82
    //   #2283	-> 89
    //   #2285	-> 99
    //   #2286	-> 104
    // Exception table:
    //   from	to	target	type
    //   24	32	94	finally
    //   39	51	94	finally
    //   51	72	94	finally
    //   72	76	94	finally
    //   82	89	94	finally
    //   89	91	94	finally
    //   95	97	94	finally
  }
  
  protected void onDetachedFromWindow() {
    if (this.mIndeterminate)
      stopAnimation(); 
    RefreshProgressRunnable refreshProgressRunnable = this.mRefreshProgressRunnable;
    if (refreshProgressRunnable != null) {
      removeCallbacks(refreshProgressRunnable);
      this.mRefreshIsPosted = false;
    } 
    super.onDetachedFromWindow();
    this.mAttached = false;
  }
  
  public CharSequence getAccessibilityClassName() {
    return ProgressBar.class.getName();
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    paramAccessibilityEvent.setItemCount(this.mMax - this.mMin);
    paramAccessibilityEvent.setCurrentItemIndex(this.mProgress);
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (!isIndeterminate()) {
      float f1 = getMin(), f2 = getMax();
      float f3 = getProgress();
      AccessibilityNodeInfo.RangeInfo rangeInfo = AccessibilityNodeInfo.RangeInfo.obtain(0, f1, f2, f3);
      paramAccessibilityNodeInfo.setRangeInfo(rangeInfo);
      paramAccessibilityNodeInfo.setStateDescription(formatStateDescription(this.mProgress));
    } 
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("progress:max", getMax());
    paramViewHierarchyEncoder.addProperty("progress:progress", getProgress());
    paramViewHierarchyEncoder.addProperty("progress:secondaryProgress", getSecondaryProgress());
    paramViewHierarchyEncoder.addProperty("progress:indeterminate", isIndeterminate());
  }
  
  public boolean isAnimating() {
    boolean bool;
    if (isIndeterminate() && getWindowVisibility() == 0 && isShown()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class ProgressTintInfo {
    boolean mHasIndeterminateTint;
    
    boolean mHasIndeterminateTintMode;
    
    boolean mHasProgressBackgroundTint;
    
    boolean mHasProgressBackgroundTintMode;
    
    boolean mHasProgressTint;
    
    boolean mHasProgressTintMode;
    
    boolean mHasSecondaryProgressTint;
    
    boolean mHasSecondaryProgressTintMode;
    
    BlendMode mIndeterminateBlendMode;
    
    ColorStateList mIndeterminateTintList;
    
    BlendMode mProgressBackgroundBlendMode;
    
    ColorStateList mProgressBackgroundTintList;
    
    BlendMode mProgressBlendMode;
    
    ColorStateList mProgressTintList;
    
    BlendMode mSecondaryProgressBlendMode;
    
    ColorStateList mSecondaryProgressTintList;
    
    private ProgressTintInfo() {}
  }
}
