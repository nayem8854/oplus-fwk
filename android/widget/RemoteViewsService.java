package android.widget;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.android.internal.widget.IRemoteViewsFactory;
import java.util.HashMap;

public abstract class RemoteViewsService extends Service {
  private static final String LOG_TAG = "RemoteViewsService";
  
  private static final Object sLock;
  
  private static final HashMap<Intent.FilterComparison, RemoteViewsFactory> sRemoteViewFactories = new HashMap<>();
  
  static {
    sLock = new Object();
  }
  
  class RemoteViewsFactory {
    public abstract int getCount();
    
    public abstract long getItemId(int param1Int);
    
    public abstract RemoteViews getLoadingView();
    
    public abstract RemoteViews getViewAt(int param1Int);
    
    public abstract int getViewTypeCount();
    
    public abstract boolean hasStableIds();
    
    public abstract void onCreate();
    
    public abstract void onDataSetChanged();
    
    public abstract void onDestroy();
  }
  
  private static class RemoteViewsFactoryAdapter extends IRemoteViewsFactory.Stub {
    private RemoteViewsService.RemoteViewsFactory mFactory;
    
    private boolean mIsCreated;
    
    public RemoteViewsFactoryAdapter(RemoteViewsService.RemoteViewsFactory param1RemoteViewsFactory, boolean param1Boolean) {
      this.mFactory = param1RemoteViewsFactory;
      this.mIsCreated = param1Boolean;
    }
    
    public boolean isCreated() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mIsCreated : Z
      //   6: istore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: iload_1
      //   10: ireturn
      //   11: astore_2
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_2
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #138	-> 2
      //   #138	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
    
    public void onDataSetChanged() {
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      try {
        this.mFactory.onDataSetChanged();
      } catch (Exception exception) {
        Thread thread = Thread.currentThread();
        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
      } finally {
        Exception exception;
      } 
      /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
    }
    
    public void onDataSetChangedAsync() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: invokevirtual onDataSetChanged : ()V
      //   6: aload_0
      //   7: monitorexit
      //   8: return
      //   9: astore_1
      //   10: aload_0
      //   11: monitorexit
      //   12: aload_1
      //   13: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #149	-> 2
      //   #150	-> 6
      //   #148	-> 9
      // Exception table:
      //   from	to	target	type
      //   2	6	9	finally
    }
    
    public int getCount() {
      boolean bool2;
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      boolean bool1 = false;
      try {
        bool2 = this.mFactory.getCount();
      } catch (Exception exception) {
        Thread thread = Thread.currentThread();
        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
        bool2 = bool1;
      } finally {
        Exception exception;
      } 
      /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      return bool2;
    }
    
    public RemoteViews getViewAt(int param1Int) {
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      RemoteViews remoteViews = null;
      try {
        RemoteViews remoteViews1 = this.mFactory.getViewAt(param1Int);
        if (remoteViews1 != null) {
          remoteViews = remoteViews1;
          remoteViews1.addFlags(2);
        } 
        remoteViews = remoteViews1;
      } catch (Exception exception) {
        Thread thread = Thread.currentThread();
        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
      } finally {}
      /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      return remoteViews;
    }
    
    public RemoteViews getLoadingView() {
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      RemoteViews remoteViews = null;
      try {
        RemoteViews remoteViews1 = this.mFactory.getLoadingView();
      } catch (Exception exception) {
        Thread thread = Thread.currentThread();
        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
      } finally {}
      /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      return remoteViews;
    }
    
    public int getViewTypeCount() {
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      int i = 0;
      try {
        int j = this.mFactory.getViewTypeCount();
      } catch (Exception exception) {
        Thread thread = Thread.currentThread();
        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
      } finally {
        Exception exception;
      } 
      /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      return i;
    }
    
    public long getItemId(int param1Int) {
      long l2;
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      long l1 = 0L;
      try {
        l2 = this.mFactory.getItemId(param1Int);
      } catch (Exception exception) {
        Thread thread = Thread.currentThread();
        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
        l2 = l1;
      } finally {
        Exception exception;
      } 
      /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      return l2;
    }
    
    public boolean hasStableIds() {
      boolean bool2;
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      boolean bool1 = false;
      try {
        bool2 = this.mFactory.hasStableIds();
      } catch (Exception exception) {
        Thread thread = Thread.currentThread();
        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
        bool2 = bool1;
      } finally {
        Exception exception;
      } 
      /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/widget/RemoteViewsService}.Landroid/widget/RemoteViewsService$RemoteViewsFactoryAdapter;}} */
      return bool2;
    }
    
    public void onDestroy(Intent param1Intent) {
      synchronized (RemoteViewsService.sLock) {
        Intent.FilterComparison filterComparison = new Intent.FilterComparison();
        this(param1Intent);
        if (RemoteViewsService.sRemoteViewFactories.containsKey(filterComparison)) {
          RemoteViewsService.RemoteViewsFactory remoteViewsFactory = (RemoteViewsService.RemoteViewsFactory)RemoteViewsService.sRemoteViewFactories.get(filterComparison);
          try {
            remoteViewsFactory.onDestroy();
          } catch (Exception exception) {
            Thread thread = Thread.currentThread();
            Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
          } 
          RemoteViewsService.sRemoteViewFactories.remove(filterComparison);
        } 
        return;
      } 
    }
  }
  
  public IBinder onBind(Intent paramIntent) {
    synchronized (sLock) {
      RemoteViewsFactory remoteViewsFactory;
      boolean bool;
      Intent.FilterComparison filterComparison = new Intent.FilterComparison();
      this(paramIntent);
      if (!sRemoteViewFactories.containsKey(filterComparison)) {
        remoteViewsFactory = onGetViewFactory(paramIntent);
        sRemoteViewFactories.put(filterComparison, remoteViewsFactory);
        remoteViewsFactory.onCreate();
        bool = false;
      } else {
        remoteViewsFactory = sRemoteViewFactories.get(filterComparison);
        bool = true;
      } 
      RemoteViewsFactoryAdapter remoteViewsFactoryAdapter = new RemoteViewsFactoryAdapter();
      this(remoteViewsFactory, bool);
      return (IBinder)remoteViewsFactoryAdapter;
    } 
  }
  
  public abstract RemoteViewsFactory onGetViewFactory(Intent paramIntent);
}
