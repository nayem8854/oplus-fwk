package android.widget;

import android.database.DataSetObservable;
import android.database.DataSetObserver;

public abstract class BaseExpandableListAdapter implements ExpandableListAdapter, HeterogeneousExpandableList {
  private final DataSetObservable mDataSetObservable = new DataSetObservable();
  
  public void registerDataSetObserver(DataSetObserver paramDataSetObserver) {
    this.mDataSetObservable.registerObserver(paramDataSetObserver);
  }
  
  public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver) {
    this.mDataSetObservable.unregisterObserver(paramDataSetObserver);
  }
  
  public void notifyDataSetInvalidated() {
    this.mDataSetObservable.notifyInvalidated();
  }
  
  public void notifyDataSetChanged() {
    this.mDataSetObservable.notifyChanged();
  }
  
  public boolean areAllItemsEnabled() {
    return true;
  }
  
  public void onGroupCollapsed(int paramInt) {}
  
  public void onGroupExpanded(int paramInt) {}
  
  public long getCombinedChildId(long paramLong1, long paramLong2) {
    return (0x7FFFFFFFL & paramLong1) << 32L | Long.MIN_VALUE | 0xFFFFFFFFFFFFFFFFL & paramLong2;
  }
  
  public long getCombinedGroupId(long paramLong) {
    return (0x7FFFFFFFL & paramLong) << 32L;
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (getGroupCount() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getChildType(int paramInt1, int paramInt2) {
    return 0;
  }
  
  public int getChildTypeCount() {
    return 1;
  }
  
  public int getGroupType(int paramInt) {
    return 0;
  }
  
  public int getGroupTypeCount() {
    return 1;
  }
}
