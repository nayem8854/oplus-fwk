package android.widget;

public interface HeterogeneousExpandableList {
  int getChildType(int paramInt1, int paramInt2);
  
  int getChildTypeCount();
  
  int getGroupType(int paramInt);
  
  int getGroupTypeCount();
}
