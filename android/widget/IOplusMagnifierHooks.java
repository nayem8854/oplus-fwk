package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.RecordingCanvas;

public interface IOplusMagnifierHooks extends IOplusCommonFeature {
  public static final IOplusMagnifierHooks DEFAULT = (IOplusMagnifierHooks)new Object();
  
  default IOplusMagnifierHooks getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusMagnifierHooks;
  }
  
  default int getMagnifierWidth(TypedArray paramTypedArray, Context paramContext) {
    return paramTypedArray.getDimensionPixelSize(5, 0);
  }
  
  default int getMagnifierHeight(TypedArray paramTypedArray, Context paramContext) {
    return paramTypedArray.getDimensionPixelSize(2, 0);
  }
  
  default float getMagnifierCornerRadius(TypedArray paramTypedArray, Context paramContext) {
    return paramTypedArray.getDimension(0, 0.0F);
  }
  
  default void decodeShadowBitmap(Context paramContext) {}
  
  default void recycleShadowBitmap() {}
  
  default void drawShadowBitmap(int paramInt1, int paramInt2, RecordingCanvas paramRecordingCanvas, Paint paramPaint) {}
}
