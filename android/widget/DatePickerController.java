package android.widget;

import java.util.Calendar;

interface DatePickerController {
  Calendar getSelectedDay();
  
  void onYearSelected(int paramInt);
  
  void registerOnDateChangedListener(OnDateChangedListener paramOnDateChangedListener);
  
  void tryVibrate();
}
