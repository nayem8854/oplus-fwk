package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.regex.Pattern;

public class TableLayout extends LinearLayout {
  private SparseBooleanArray mCollapsedColumns;
  
  private boolean mInitialized;
  
  private int[] mMaxWidths;
  
  private PassThroughHierarchyChangeListener mPassThroughListener;
  
  private boolean mShrinkAllColumns;
  
  private SparseBooleanArray mShrinkableColumns;
  
  private boolean mStretchAllColumns;
  
  private SparseBooleanArray mStretchableColumns;
  
  public TableLayout(Context paramContext) {
    super(paramContext);
    initTableLayout();
  }
  
  public TableLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TableLayout);
    String str = typedArray.getString(0);
    if (str != null)
      if (str.charAt(0) == '*') {
        this.mStretchAllColumns = true;
      } else {
        this.mStretchableColumns = parseColumns(str);
      }  
    str = typedArray.getString(1);
    if (str != null)
      if (str.charAt(0) == '*') {
        this.mShrinkAllColumns = true;
      } else {
        this.mShrinkableColumns = parseColumns(str);
      }  
    str = typedArray.getString(2);
    if (str != null)
      this.mCollapsedColumns = parseColumns(str); 
    typedArray.recycle();
    initTableLayout();
  }
  
  private static SparseBooleanArray parseColumns(String paramString) {
    SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
    Pattern pattern = Pattern.compile("\\s*,\\s*");
    for (String str : pattern.split(paramString)) {
      try {
        int i = Integer.parseInt(str);
        if (i >= 0)
          sparseBooleanArray.put(i, true); 
      } catch (NumberFormatException numberFormatException) {}
    } 
    return sparseBooleanArray;
  }
  
  private void initTableLayout() {
    if (this.mCollapsedColumns == null)
      this.mCollapsedColumns = new SparseBooleanArray(); 
    if (this.mStretchableColumns == null)
      this.mStretchableColumns = new SparseBooleanArray(); 
    if (this.mShrinkableColumns == null)
      this.mShrinkableColumns = new SparseBooleanArray(); 
    setOrientation(1);
    PassThroughHierarchyChangeListener passThroughHierarchyChangeListener = new PassThroughHierarchyChangeListener();
    super.setOnHierarchyChangeListener(passThroughHierarchyChangeListener);
    this.mInitialized = true;
  }
  
  public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener paramOnHierarchyChangeListener) {
    PassThroughHierarchyChangeListener.access$102(this.mPassThroughListener, paramOnHierarchyChangeListener);
  }
  
  private void requestRowsLayout() {
    if (this.mInitialized) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++)
        getChildAt(b).requestLayout(); 
    } 
  }
  
  public void requestLayout() {
    if (this.mInitialized) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++)
        getChildAt(b).forceLayout(); 
    } 
    super.requestLayout();
  }
  
  public boolean isShrinkAllColumns() {
    return this.mShrinkAllColumns;
  }
  
  public void setShrinkAllColumns(boolean paramBoolean) {
    this.mShrinkAllColumns = paramBoolean;
  }
  
  public boolean isStretchAllColumns() {
    return this.mStretchAllColumns;
  }
  
  public void setStretchAllColumns(boolean paramBoolean) {
    this.mStretchAllColumns = paramBoolean;
  }
  
  public void setColumnCollapsed(int paramInt, boolean paramBoolean) {
    this.mCollapsedColumns.put(paramInt, paramBoolean);
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view instanceof TableRow)
        ((TableRow)view).setColumnCollapsed(paramInt, paramBoolean); 
    } 
    requestRowsLayout();
  }
  
  public boolean isColumnCollapsed(int paramInt) {
    return this.mCollapsedColumns.get(paramInt);
  }
  
  public void setColumnStretchable(int paramInt, boolean paramBoolean) {
    this.mStretchableColumns.put(paramInt, paramBoolean);
    requestRowsLayout();
  }
  
  public boolean isColumnStretchable(int paramInt) {
    return (this.mStretchAllColumns || this.mStretchableColumns.get(paramInt));
  }
  
  public void setColumnShrinkable(int paramInt, boolean paramBoolean) {
    this.mShrinkableColumns.put(paramInt, paramBoolean);
    requestRowsLayout();
  }
  
  public boolean isColumnShrinkable(int paramInt) {
    return (this.mShrinkAllColumns || this.mShrinkableColumns.get(paramInt));
  }
  
  private void trackCollapsedColumns(View paramView) {
    if (paramView instanceof TableRow) {
      TableRow tableRow = (TableRow)paramView;
      SparseBooleanArray sparseBooleanArray = this.mCollapsedColumns;
      int i = sparseBooleanArray.size();
      for (byte b = 0; b < i; b++) {
        int j = sparseBooleanArray.keyAt(b);
        boolean bool = sparseBooleanArray.valueAt(b);
        if (bool)
          tableRow.setColumnCollapsed(j, bool); 
      } 
    } 
  }
  
  public void addView(View paramView) {
    super.addView(paramView);
    requestRowsLayout();
  }
  
  public void addView(View paramView, int paramInt) {
    super.addView(paramView, paramInt);
    requestRowsLayout();
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    super.addView(paramView, paramLayoutParams);
    requestRowsLayout();
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    super.addView(paramView, paramInt, paramLayoutParams);
    requestRowsLayout();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    measureVertical(paramInt1, paramInt2);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    layoutVertical(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  void measureChildBeforeLayout(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    if (paramView instanceof TableRow)
      ((TableRow)paramView).setColumnsWidthConstraints(this.mMaxWidths); 
    super.measureChildBeforeLayout(paramView, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
  }
  
  void measureVertical(int paramInt1, int paramInt2) {
    findLargestCells(paramInt1, paramInt2);
    shrinkAndStretchColumns(paramInt1);
    super.measureVertical(paramInt1, paramInt2);
  }
  
  private void findLargestCells(int paramInt1, int paramInt2) {
    boolean bool = true;
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.getVisibility() != 8)
        if (view instanceof TableRow) {
          view = view;
          ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
          layoutParams.height = -2;
          int[] arrayOfInt = view.getColumnsWidths(paramInt1, paramInt2);
          int j = arrayOfInt.length;
          if (bool) {
            int[] arrayOfInt1 = this.mMaxWidths;
            if (arrayOfInt1 == null || arrayOfInt1.length != j)
              this.mMaxWidths = new int[j]; 
            System.arraycopy(arrayOfInt, 0, this.mMaxWidths, 0, j);
            bool = false;
          } else {
            int k = this.mMaxWidths.length;
            int m = j - k;
            if (m > 0) {
              int[] arrayOfInt3 = this.mMaxWidths;
              int[] arrayOfInt2 = new int[j];
              System.arraycopy(arrayOfInt3, 0, arrayOfInt2, 0, arrayOfInt3.length);
              System.arraycopy(arrayOfInt, arrayOfInt3.length, this.mMaxWidths, arrayOfInt3.length, m);
            } 
            int[] arrayOfInt1 = this.mMaxWidths;
            m = Math.min(k, j);
            for (k = 0; k < m; k++)
              arrayOfInt1[k] = Math.max(arrayOfInt1[k], arrayOfInt[k]); 
          } 
        }  
    } 
  }
  
  private void shrinkAndStretchColumns(int paramInt) {
    int[] arrayOfInt = this.mMaxWidths;
    if (arrayOfInt == null)
      return; 
    int i = 0;
    int j;
    byte b;
    for (j = arrayOfInt.length, b = 0; b < j; ) {
      int k = arrayOfInt[b];
      i += k;
      b++;
    } 
    paramInt = View.MeasureSpec.getSize(paramInt) - this.mPaddingLeft - this.mPaddingRight;
    if (i > paramInt && (this.mShrinkAllColumns || this.mShrinkableColumns.size() > 0)) {
      mutateColumnsWidth(this.mShrinkableColumns, this.mShrinkAllColumns, paramInt, i);
    } else if (i < paramInt && (this.mStretchAllColumns || this.mStretchableColumns.size() > 0)) {
      mutateColumnsWidth(this.mStretchableColumns, this.mStretchAllColumns, paramInt, i);
    } 
  }
  
  private void mutateColumnsWidth(SparseBooleanArray paramSparseBooleanArray, boolean paramBoolean, int paramInt1, int paramInt2) {
    int k, i = 0;
    int[] arrayOfInt = this.mMaxWidths;
    int j = arrayOfInt.length;
    if (paramBoolean) {
      k = j;
    } else {
      k = paramSparseBooleanArray.size();
    } 
    int m = (paramInt1 - paramInt2) / k;
    paramInt2 = getChildCount();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      View view = getChildAt(paramInt1);
      if (view instanceof TableRow)
        view.forceLayout(); 
    } 
    if (!paramBoolean) {
      for (paramInt1 = 0; paramInt1 < k; paramInt1++, i = paramInt2) {
        int n = paramSparseBooleanArray.keyAt(paramInt1);
        paramInt2 = i;
        if (paramSparseBooleanArray.valueAt(paramInt1))
          if (n < j) {
            arrayOfInt[n] = arrayOfInt[n] + m;
            paramInt2 = i;
          } else {
            paramInt2 = i + 1;
          }  
      } 
      if (i > 0 && i < k) {
        paramInt2 = i * m / (k - i);
        for (paramInt1 = 0; paramInt1 < k; paramInt1++) {
          i = paramSparseBooleanArray.keyAt(paramInt1);
          if (paramSparseBooleanArray.valueAt(paramInt1) && i < j)
            if (paramInt2 > arrayOfInt[i]) {
              arrayOfInt[i] = 0;
            } else {
              arrayOfInt[i] = arrayOfInt[i] + paramInt2;
            }  
        } 
      } 
      return;
    } 
    for (paramInt1 = 0; paramInt1 < k; paramInt1++)
      arrayOfInt[paramInt1] = arrayOfInt[paramInt1] + m; 
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LinearLayout.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams();
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  protected LinearLayout.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return new LayoutParams(paramLayoutParams);
  }
  
  public CharSequence getAccessibilityClassName() {
    return TableLayout.class.getName();
  }
  
  class LayoutParams extends LinearLayout.LayoutParams {
    public LayoutParams(TableLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(TableLayout this$0, int param1Int1) {
      super(-1, param1Int1);
    }
    
    public LayoutParams(TableLayout this$0, int param1Int1, float param1Float) {
      super(-1, param1Int1, param1Float);
    }
    
    public LayoutParams() {
      super(-1, -2);
    }
    
    public LayoutParams(TableLayout this$0) {
      super((ViewGroup.LayoutParams)this$0);
      this.width = -1;
    }
    
    public LayoutParams(TableLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
      this.width = -1;
      if (this$0 instanceof LayoutParams)
        this.weight = ((LayoutParams)this$0).weight; 
    }
    
    protected void setBaseAttributes(TypedArray param1TypedArray, int param1Int1, int param1Int2) {
      this.width = -1;
      if (param1TypedArray.hasValue(param1Int2)) {
        this.height = param1TypedArray.getLayoutDimension(param1Int2, "layout_height");
      } else {
        this.height = -2;
      } 
    }
  }
  
  class PassThroughHierarchyChangeListener implements ViewGroup.OnHierarchyChangeListener {
    private ViewGroup.OnHierarchyChangeListener mOnHierarchyChangeListener;
    
    final TableLayout this$0;
    
    private PassThroughHierarchyChangeListener() {}
    
    public void onChildViewAdded(View param1View1, View param1View2) {
      TableLayout.this.trackCollapsedColumns(param1View2);
      ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener = this.mOnHierarchyChangeListener;
      if (onHierarchyChangeListener != null)
        onHierarchyChangeListener.onChildViewAdded(param1View1, param1View2); 
    }
    
    public void onChildViewRemoved(View param1View1, View param1View2) {
      ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener = this.mOnHierarchyChangeListener;
      if (onHierarchyChangeListener != null)
        onHierarchyChangeListener.onChildViewRemoved(param1View1, param1View2); 
    }
  }
}
