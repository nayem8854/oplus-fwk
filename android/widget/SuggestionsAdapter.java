package android.widget;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.WeakHashMap;

class SuggestionsAdapter extends ResourceCursorAdapter implements View.OnClickListener {
  private boolean mClosed = false;
  
  private int mQueryRefinement = 1;
  
  private int mText1Col = -1;
  
  private int mText2Col = -1;
  
  private int mText2UrlCol = -1;
  
  private int mIconName1Col = -1;
  
  private int mIconName2Col = -1;
  
  private int mFlagsCol = -1;
  
  private static final boolean DBG = false;
  
  private static final long DELETE_KEY_POST_DELAY = 500L;
  
  static final int INVALID_INDEX = -1;
  
  private static final String LOG_TAG = "SuggestionsAdapter";
  
  private static final int QUERY_LIMIT = 50;
  
  static final int REFINE_ALL = 2;
  
  static final int REFINE_BY_ENTRY = 1;
  
  static final int REFINE_NONE = 0;
  
  private final int mCommitIconResId;
  
  private final WeakHashMap<String, Drawable.ConstantState> mOutsideDrawablesCache;
  
  private final Context mProviderContext;
  
  private final SearchManager mSearchManager;
  
  private final SearchView mSearchView;
  
  private final SearchableInfo mSearchable;
  
  private ColorStateList mUrlColor;
  
  public SuggestionsAdapter(Context paramContext, SearchView paramSearchView, SearchableInfo paramSearchableInfo, WeakHashMap<String, Drawable.ConstantState> paramWeakHashMap) {
    super(paramContext, paramSearchView.getSuggestionRowLayout(), (Cursor)null, true);
    this.mSearchManager = (SearchManager)this.mContext.getSystemService("search");
    this.mSearchView = paramSearchView;
    this.mSearchable = paramSearchableInfo;
    this.mCommitIconResId = paramSearchView.getSuggestionCommitIconResId();
    paramContext = this.mSearchable.getActivityContext(this.mContext);
    this.mProviderContext = this.mSearchable.getProviderContext(this.mContext, paramContext);
    this.mOutsideDrawablesCache = paramWeakHashMap;
    getFilter().setDelayer((Filter.Delayer)new Object(this));
  }
  
  public void setQueryRefinement(int paramInt) {
    this.mQueryRefinement = paramInt;
  }
  
  public int getQueryRefinement() {
    return this.mQueryRefinement;
  }
  
  public boolean hasStableIds() {
    return false;
  }
  
  public Cursor runQueryOnBackgroundThread(CharSequence paramCharSequence) {
    if (paramCharSequence == null) {
      paramCharSequence = "";
    } else {
      paramCharSequence = paramCharSequence.toString();
    } 
    if (this.mSearchView.getVisibility() == 0) {
      SearchView searchView = this.mSearchView;
      if (searchView.getWindowVisibility() == 0) {
        try {
          Cursor cursor = this.mSearchManager.getSuggestions(this.mSearchable, (String)paramCharSequence, 50);
          if (cursor != null) {
            cursor.getCount();
            return cursor;
          } 
        } catch (RuntimeException runtimeException) {
          Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", runtimeException);
        } 
        return null;
      } 
    } 
    return null;
  }
  
  public void close() {
    changeCursor((Cursor)null);
    this.mClosed = true;
  }
  
  public void notifyDataSetChanged() {
    super.notifyDataSetChanged();
    updateSpinnerState(getCursor());
  }
  
  public void notifyDataSetInvalidated() {
    super.notifyDataSetInvalidated();
    updateSpinnerState(getCursor());
  }
  
  private void updateSpinnerState(Cursor paramCursor) {
    if (paramCursor != null) {
      Bundle bundle = paramCursor.getExtras();
    } else {
      paramCursor = null;
    } 
    if (paramCursor != null && 
      paramCursor.getBoolean("in_progress"))
      return; 
  }
  
  public void changeCursor(Cursor paramCursor) {
    if (this.mClosed) {
      Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
      if (paramCursor != null)
        paramCursor.close(); 
      return;
    } 
    try {
      super.changeCursor(paramCursor);
      if (paramCursor != null) {
        this.mText1Col = paramCursor.getColumnIndex("suggest_text_1");
        this.mText2Col = paramCursor.getColumnIndex("suggest_text_2");
        this.mText2UrlCol = paramCursor.getColumnIndex("suggest_text_2_url");
        this.mIconName1Col = paramCursor.getColumnIndex("suggest_icon_1");
        this.mIconName2Col = paramCursor.getColumnIndex("suggest_icon_2");
        this.mFlagsCol = paramCursor.getColumnIndex("suggest_flags");
      } 
    } catch (Exception exception) {
      Log.e("SuggestionsAdapter", "error changing cursor and caching columns", exception);
    } 
  }
  
  public View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup) {
    View view = super.newView(paramContext, paramCursor, paramViewGroup);
    view.setTag(new ChildViewCache(view));
    ImageView imageView = view.<ImageView>findViewById(16908938);
    imageView.setImageResource(this.mCommitIconResId);
    return view;
  }
  
  class ChildViewCache {
    public final ImageView mIcon1;
    
    public final ImageView mIcon2;
    
    public final ImageView mIconRefine;
    
    public final TextView mText1;
    
    public final TextView mText2;
    
    public ChildViewCache(SuggestionsAdapter this$0) {
      this.mText1 = this$0.<TextView>findViewById(16908308);
      this.mText2 = this$0.<TextView>findViewById(16908309);
      this.mIcon1 = this$0.<ImageView>findViewById(16908295);
      this.mIcon2 = this$0.<ImageView>findViewById(16908296);
      this.mIconRefine = this$0.<ImageView>findViewById(16908938);
    }
  }
  
  public void bindView(View paramView, Context paramContext, Cursor paramCursor) {
    ChildViewCache childViewCache = (ChildViewCache)paramView.getTag();
    int i = 0;
    int j = this.mFlagsCol;
    if (j != -1)
      i = paramCursor.getInt(j); 
    if (childViewCache.mText1 != null) {
      String str = getStringOrNull(paramCursor, this.mText1Col);
      setViewText(childViewCache.mText1, str);
    } 
    if (childViewCache.mText2 != null) {
      String str = getStringOrNull(paramCursor, this.mText2UrlCol);
      if (str != null) {
        CharSequence charSequence = formatUrl(paramContext, str);
      } else {
        str = getStringOrNull(paramCursor, this.mText2Col);
      } 
      if (TextUtils.isEmpty(str)) {
        if (childViewCache.mText1 != null) {
          childViewCache.mText1.setSingleLine(false);
          childViewCache.mText1.setMaxLines(2);
        } 
      } else if (childViewCache.mText1 != null) {
        childViewCache.mText1.setSingleLine(true);
        childViewCache.mText1.setMaxLines(1);
      } 
      setViewText(childViewCache.mText2, str);
    } 
    if (childViewCache.mIcon1 != null)
      setViewDrawable(childViewCache.mIcon1, getIcon1(paramCursor), 4); 
    if (childViewCache.mIcon2 != null)
      setViewDrawable(childViewCache.mIcon2, getIcon2(paramCursor), 8); 
    j = this.mQueryRefinement;
    if (j == 2 || (j == 1 && (i & 0x1) != 0)) {
      childViewCache.mIconRefine.setVisibility(0);
      childViewCache.mIconRefine.setTag(childViewCache.mText1.getText());
      childViewCache.mIconRefine.setOnClickListener(this);
      return;
    } 
    childViewCache.mIconRefine.setVisibility(8);
  }
  
  public void onClick(View paramView) {
    Object object = paramView.getTag();
    if (object instanceof CharSequence)
      this.mSearchView.onQueryRefine((CharSequence)object); 
  }
  
  private CharSequence formatUrl(Context paramContext, CharSequence paramCharSequence) {
    if (this.mUrlColor == null) {
      TypedValue typedValue = new TypedValue();
      paramContext.getTheme().resolveAttribute(17957092, typedValue, true);
      this.mUrlColor = paramContext.getColorStateList(typedValue.resourceId);
    } 
    SpannableString spannableString = new SpannableString(paramCharSequence);
    TextAppearanceSpan textAppearanceSpan = new TextAppearanceSpan(null, 0, 0, this.mUrlColor, null);
    int i = paramCharSequence.length();
    spannableString.setSpan(textAppearanceSpan, 0, i, 33);
    return spannableString;
  }
  
  private void setViewText(TextView paramTextView, CharSequence paramCharSequence) {
    paramTextView.setText(paramCharSequence);
    if (TextUtils.isEmpty(paramCharSequence)) {
      paramTextView.setVisibility(8);
    } else {
      paramTextView.setVisibility(0);
    } 
  }
  
  private Drawable getIcon1(Cursor paramCursor) {
    int i = this.mIconName1Col;
    if (i == -1)
      return null; 
    String str = paramCursor.getString(i);
    Drawable drawable = getDrawableFromResourceValue(str);
    if (drawable != null)
      return drawable; 
    return getDefaultIcon1(paramCursor);
  }
  
  private Drawable getIcon2(Cursor paramCursor) {
    int i = this.mIconName2Col;
    if (i == -1)
      return null; 
    String str = paramCursor.getString(i);
    return getDrawableFromResourceValue(str);
  }
  
  private void setViewDrawable(ImageView paramImageView, Drawable paramDrawable, int paramInt) {
    paramImageView.setImageDrawable(paramDrawable);
    if (paramDrawable == null) {
      paramImageView.setVisibility(paramInt);
    } else {
      paramImageView.setVisibility(0);
      paramDrawable.setVisible(false, false);
      paramDrawable.setVisible(true, false);
    } 
  }
  
  public CharSequence convertToString(Cursor paramCursor) {
    if (paramCursor == null)
      return null; 
    String str = getColumnString(paramCursor, "suggest_intent_query");
    if (str != null)
      return str; 
    if (this.mSearchable.shouldRewriteQueryFromData()) {
      str = getColumnString(paramCursor, "suggest_intent_data");
      if (str != null)
        return str; 
    } 
    if (this.mSearchable.shouldRewriteQueryFromText()) {
      String str1 = getColumnString(paramCursor, "suggest_text_1");
      if (str1 != null)
        return str1; 
    } 
    return null;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    try {
      return super.getView(paramInt, paramView, paramViewGroup);
    } catch (RuntimeException runtimeException) {
      Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", runtimeException);
      View view = newView(this.mContext, this.mCursor, paramViewGroup);
      if (view != null) {
        ChildViewCache childViewCache = (ChildViewCache)view.getTag();
        TextView textView = childViewCache.mText1;
        textView.setText(runtimeException.toString());
      } 
      return view;
    } 
  }
  
  public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    try {
      return super.getDropDownView(paramInt, paramView, paramViewGroup);
    } catch (RuntimeException runtimeException) {
      Context context;
      Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", runtimeException);
      if (this.mDropDownContext == null) {
        context = this.mContext;
      } else {
        context = this.mDropDownContext;
      } 
      View view = newDropDownView(context, this.mCursor, paramViewGroup);
      if (view != null) {
        ChildViewCache childViewCache = (ChildViewCache)view.getTag();
        TextView textView = childViewCache.mText1;
        textView.setText(runtimeException.toString());
      } 
      return view;
    } 
  }
  
  private Drawable getDrawableFromResourceValue(String paramString) {
    if (paramString == null || paramString.length() == 0 || "0".equals(paramString))
      return null; 
    try {
      int i = Integer.parseInt(paramString);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("android.resource://");
      Context context = this.mProviderContext;
      stringBuilder.append(context.getPackageName());
      stringBuilder.append("/");
      stringBuilder.append(i);
      String str = stringBuilder.toString();
      Drawable drawable = checkIconCache(str);
      if (drawable != null)
        return drawable; 
      drawable = this.mProviderContext.getDrawable(i);
      storeInIconCache(str, drawable);
      return drawable;
    } catch (NumberFormatException numberFormatException) {
      Drawable drawable2 = checkIconCache(paramString);
      if (drawable2 != null)
        return drawable2; 
      Uri uri = Uri.parse(paramString);
      Drawable drawable1 = getDrawable(uri);
      storeInIconCache(paramString, drawable1);
      return drawable1;
    } catch (android.content.res.Resources.NotFoundException notFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Icon resource not found: ");
      stringBuilder.append(paramString);
      Log.w("SuggestionsAdapter", stringBuilder.toString());
      return null;
    } 
  }
  
  private Drawable getDrawable(Uri paramUri) {
    try {
      String str = paramUri.getScheme();
      if ("android.resource".equals(str)) {
        Context context = this.mProviderContext;
        ContentResolver.OpenResourceIdResult openResourceIdResult = context.getContentResolver().getResourceId(paramUri);
        try {
          return openResourceIdResult.r.getDrawable(openResourceIdResult.id, this.mProviderContext.getTheme());
        } catch (android.content.res.Resources.NotFoundException notFoundException) {
          FileNotFoundException fileNotFoundException1 = new FileNotFoundException();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Resource does not exist: ");
          stringBuilder1.append(paramUri);
          this(stringBuilder1.toString());
          throw fileNotFoundException1;
        } 
      } 
      InputStream inputStream = this.mProviderContext.getContentResolver().openInputStream(paramUri);
      if (inputStream != null) {
        StringBuilder stringBuilder1;
        try {
          return Drawable.createFromStream(inputStream, null);
        } finally {
          try {
            stringBuilder1.close();
          } catch (IOException iOException) {
            stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("Error closing icon stream for ");
            stringBuilder1.append(paramUri);
            Log.e("SuggestionsAdapter", stringBuilder1.toString(), iOException);
          } 
        } 
      } 
      FileNotFoundException fileNotFoundException = new FileNotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to open ");
      stringBuilder.append(paramUri);
      this(stringBuilder.toString());
      throw fileNotFoundException;
    } catch (FileNotFoundException fileNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Icon not found: ");
      stringBuilder.append(paramUri);
      stringBuilder.append(", ");
      stringBuilder.append(fileNotFoundException.getMessage());
      Log.w("SuggestionsAdapter", stringBuilder.toString());
      return null;
    } 
  }
  
  private Drawable checkIconCache(String paramString) {
    Drawable.ConstantState constantState = this.mOutsideDrawablesCache.get(paramString);
    if (constantState == null)
      return null; 
    return constantState.newDrawable();
  }
  
  private void storeInIconCache(String paramString, Drawable paramDrawable) {
    if (paramDrawable != null)
      this.mOutsideDrawablesCache.put(paramString, paramDrawable.getConstantState()); 
  }
  
  private Drawable getDefaultIcon1(Cursor paramCursor) {
    Drawable drawable = getActivityIconWithCache(this.mSearchable.getSearchActivity());
    if (drawable != null)
      return drawable; 
    return this.mContext.getPackageManager().getDefaultActivityIcon();
  }
  
  private Drawable getActivityIconWithCache(ComponentName paramComponentName) {
    Drawable drawable1;
    Drawable.ConstantState constantState1;
    String str = paramComponentName.flattenToShortString();
    boolean bool = this.mOutsideDrawablesCache.containsKey(str);
    Drawable drawable2 = null;
    Drawable.ConstantState constantState2 = null;
    if (bool) {
      constantState1 = this.mOutsideDrawablesCache.get(str);
      if (constantState1 == null) {
        constantState1 = constantState2;
      } else {
        drawable1 = constantState1.newDrawable(this.mProviderContext.getResources());
      } 
      return drawable1;
    } 
    Drawable drawable3 = getActivityIcon((ComponentName)drawable1);
    if (drawable3 == null) {
      drawable1 = drawable2;
    } else {
      constantState1 = drawable3.getConstantState();
    } 
    this.mOutsideDrawablesCache.put(str, constantState1);
    return drawable3;
  }
  
  private Drawable getActivityIcon(ComponentName paramComponentName) {
    PackageManager packageManager = this.mContext.getPackageManager();
    try {
      StringBuilder stringBuilder;
      ActivityInfo activityInfo = packageManager.getActivityInfo(paramComponentName, 128);
      int i = activityInfo.getIconResource();
      if (i == 0)
        return null; 
      String str = paramComponentName.getPackageName();
      Drawable drawable = packageManager.getDrawable(str, i, activityInfo.applicationInfo);
      if (drawable == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid icon resource ");
        stringBuilder.append(i);
        stringBuilder.append(" for ");
        stringBuilder.append(paramComponentName.flattenToShortString());
        String str1 = stringBuilder.toString();
        Log.w("SuggestionsAdapter", str1);
        return null;
      } 
      return (Drawable)stringBuilder;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      Log.w("SuggestionsAdapter", nameNotFoundException.toString());
      return null;
    } 
  }
  
  public static String getColumnString(Cursor paramCursor, String paramString) {
    int i = paramCursor.getColumnIndex(paramString);
    return getStringOrNull(paramCursor, i);
  }
  
  private static String getStringOrNull(Cursor paramCursor, int paramInt) {
    if (paramInt == -1)
      return null; 
    try {
      return paramCursor.getString(paramInt);
    } catch (Exception exception) {
      Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", exception);
      return null;
    } 
  }
}
