package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.icu.text.DisplayContext;
import android.icu.text.RelativeDateTimeFormatter;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.IntArray;
import android.util.MathUtils;
import android.util.StateSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R;
import com.android.internal.widget.ExploreByTouchHelper;
import java.text.NumberFormat;
import java.util.Locale;
import libcore.icu.LocaleData;

class SimpleMonthView extends View {
  private final TextPaint mMonthPaint = new TextPaint();
  
  private final TextPaint mDayOfWeekPaint = new TextPaint();
  
  private final TextPaint mDayPaint = new TextPaint();
  
  private final Paint mDaySelectorPaint = new Paint();
  
  private final Paint mDayHighlightPaint = new Paint();
  
  private final Paint mDayHighlightSelectorPaint = new Paint();
  
  private final String[] mDayOfWeekLabels = new String[7];
  
  private int mActivatedDay = -1;
  
  private int mToday = -1;
  
  private int mWeekStart = 1;
  
  private int mEnabledDayStart = 1;
  
  private int mEnabledDayEnd = 31;
  
  private int mHighlightedDay = -1;
  
  private int mPreviouslyHighlightedDay = -1;
  
  private boolean mIsTouchHighlighted = false;
  
  private static final int DAYS_IN_WEEK = 7;
  
  private static final int DEFAULT_SELECTED_DAY = -1;
  
  private static final int DEFAULT_WEEK_START = 1;
  
  private static final int MAX_WEEKS_IN_MONTH = 6;
  
  private static final String MONTH_YEAR_FORMAT = "MMMMy";
  
  private static final int SELECTED_HIGHLIGHT_ALPHA = 176;
  
  private final Calendar mCalendar;
  
  private int mCellWidth;
  
  private final NumberFormat mDayFormatter;
  
  private int mDayHeight;
  
  private int mDayOfWeekHeight;
  
  private int mDayOfWeekStart;
  
  private int mDaySelectorRadius;
  
  private ColorStateList mDayTextColor;
  
  private int mDaysInMonth;
  
  private final int mDesiredCellWidth;
  
  private final int mDesiredDayHeight;
  
  private final int mDesiredDayOfWeekHeight;
  
  private final int mDesiredDaySelectorRadius;
  
  private final int mDesiredMonthHeight;
  
  private final Locale mLocale;
  
  private int mMonth;
  
  private int mMonthHeight;
  
  private String mMonthYearLabel;
  
  private OnDayClickListener mOnDayClickListener;
  
  private int mPaddedHeight;
  
  private int mPaddedWidth;
  
  private final MonthViewTouchHelper mTouchHelper;
  
  private int mYear;
  
  public SimpleMonthView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public SimpleMonthView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843612);
  }
  
  public SimpleMonthView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SimpleMonthView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    Resources resources = paramContext.getResources();
    this.mDesiredMonthHeight = resources.getDimensionPixelSize(17105125);
    this.mDesiredDayOfWeekHeight = resources.getDimensionPixelSize(17105120);
    this.mDesiredDayHeight = resources.getDimensionPixelSize(17105119);
    this.mDesiredCellWidth = resources.getDimensionPixelSize(17105124);
    this.mDesiredDaySelectorRadius = resources.getDimensionPixelSize(17105122);
    MonthViewTouchHelper monthViewTouchHelper = new MonthViewTouchHelper(this);
    setAccessibilityDelegate((View.AccessibilityDelegate)monthViewTouchHelper);
    setImportantForAccessibility(1);
    Locale locale = (resources.getConfiguration()).locale;
    this.mCalendar = Calendar.getInstance(locale);
    this.mDayFormatter = NumberFormat.getIntegerInstance(this.mLocale);
    updateMonthYearLabel();
    updateDayOfWeekLabels();
    initPaints(resources);
  }
  
  private void updateMonthYearLabel() {
    String str = DateFormat.getBestDateTimePattern(this.mLocale, "MMMMy");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str, this.mLocale);
    simpleDateFormat.setContext(DisplayContext.CAPITALIZATION_FOR_STANDALONE);
    this.mMonthYearLabel = simpleDateFormat.format(this.mCalendar.getTime());
  }
  
  private void updateDayOfWeekLabels() {
    String[] arrayOfString = (LocaleData.get(this.mLocale)).tinyWeekdayNames;
    for (byte b = 0; b < 7; b++)
      this.mDayOfWeekLabels[b] = arrayOfString[(this.mWeekStart + b - 1) % 7 + 1]; 
  }
  
  private ColorStateList applyTextAppearance(Paint paramPaint, int paramInt) {
    TypedArray typedArray = this.mContext.obtainStyledAttributes(null, R.styleable.TextAppearance, 0, paramInt);
    String str = typedArray.getString(12);
    if (str != null)
      paramPaint.setTypeface(Typeface.create(str, 0)); 
    paramInt = (int)paramPaint.getTextSize();
    paramPaint.setTextSize(typedArray.getDimensionPixelSize(0, paramInt));
    ColorStateList colorStateList = typedArray.getColorStateList(3);
    if (colorStateList != null) {
      paramInt = colorStateList.getColorForState(ENABLED_STATE_SET, 0);
      paramPaint.setColor(paramInt);
    } 
    typedArray.recycle();
    return colorStateList;
  }
  
  public int getMonthHeight() {
    return this.mMonthHeight;
  }
  
  public int getCellWidth() {
    return this.mCellWidth;
  }
  
  public void setMonthTextAppearance(int paramInt) {
    applyTextAppearance(this.mMonthPaint, paramInt);
    invalidate();
  }
  
  public void setDayOfWeekTextAppearance(int paramInt) {
    applyTextAppearance(this.mDayOfWeekPaint, paramInt);
    invalidate();
  }
  
  public void setDayTextAppearance(int paramInt) {
    ColorStateList colorStateList = applyTextAppearance(this.mDayPaint, paramInt);
    if (colorStateList != null)
      this.mDayTextColor = colorStateList; 
    invalidate();
  }
  
  private void initPaints(Resources paramResources) {
    String str1 = paramResources.getString(17040036);
    String str2 = paramResources.getString(17040026);
    String str3 = paramResources.getString(17040027);
    int i = paramResources.getDimensionPixelSize(17105126);
    int j = paramResources.getDimensionPixelSize(17105121);
    int k = paramResources.getDimensionPixelSize(17105123);
    this.mMonthPaint.setAntiAlias(true);
    this.mMonthPaint.setTextSize(i);
    this.mMonthPaint.setTypeface(Typeface.create(str1, 0));
    this.mMonthPaint.setTextAlign(Paint.Align.CENTER);
    this.mMonthPaint.setStyle(Paint.Style.FILL);
    this.mDayOfWeekPaint.setAntiAlias(true);
    this.mDayOfWeekPaint.setTextSize(j);
    this.mDayOfWeekPaint.setTypeface(Typeface.create(str2, 0));
    this.mDayOfWeekPaint.setTextAlign(Paint.Align.CENTER);
    this.mDayOfWeekPaint.setStyle(Paint.Style.FILL);
    this.mDaySelectorPaint.setAntiAlias(true);
    this.mDaySelectorPaint.setStyle(Paint.Style.FILL);
    this.mDayHighlightPaint.setAntiAlias(true);
    this.mDayHighlightPaint.setStyle(Paint.Style.FILL);
    this.mDayHighlightSelectorPaint.setAntiAlias(true);
    this.mDayHighlightSelectorPaint.setStyle(Paint.Style.FILL);
    this.mDayPaint.setAntiAlias(true);
    this.mDayPaint.setTextSize(k);
    this.mDayPaint.setTypeface(Typeface.create(str3, 0));
    this.mDayPaint.setTextAlign(Paint.Align.CENTER);
    this.mDayPaint.setStyle(Paint.Style.FILL);
  }
  
  void setMonthTextColor(ColorStateList paramColorStateList) {
    int i = paramColorStateList.getColorForState(ENABLED_STATE_SET, 0);
    this.mMonthPaint.setColor(i);
    invalidate();
  }
  
  void setDayOfWeekTextColor(ColorStateList paramColorStateList) {
    int i = paramColorStateList.getColorForState(ENABLED_STATE_SET, 0);
    this.mDayOfWeekPaint.setColor(i);
    invalidate();
  }
  
  void setDayTextColor(ColorStateList paramColorStateList) {
    this.mDayTextColor = paramColorStateList;
    invalidate();
  }
  
  void setDaySelectorColor(ColorStateList paramColorStateList) {
    int[] arrayOfInt = StateSet.get(40);
    int i = paramColorStateList.getColorForState(arrayOfInt, 0);
    this.mDaySelectorPaint.setColor(i);
    this.mDayHighlightSelectorPaint.setColor(i);
    this.mDayHighlightSelectorPaint.setAlpha(176);
    invalidate();
  }
  
  void setDayHighlightColor(ColorStateList paramColorStateList) {
    int[] arrayOfInt = StateSet.get(24);
    int i = paramColorStateList.getColorForState(arrayOfInt, 0);
    this.mDayHighlightPaint.setColor(i);
    invalidate();
  }
  
  public void setOnDayClickListener(OnDayClickListener paramOnDayClickListener) {
    this.mOnDayClickListener = paramOnDayClickListener;
  }
  
  public boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    return (this.mTouchHelper.dispatchHoverEvent(paramMotionEvent) || super.dispatchHoverEvent(paramMotionEvent));
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getX : ()F
    //   4: ldc_w 0.5
    //   7: fadd
    //   8: f2i
    //   9: istore_2
    //   10: aload_1
    //   11: invokevirtual getY : ()F
    //   14: ldc_w 0.5
    //   17: fadd
    //   18: f2i
    //   19: istore_3
    //   20: aload_1
    //   21: invokevirtual getAction : ()I
    //   24: istore #4
    //   26: iload #4
    //   28: ifeq -> 84
    //   31: iload #4
    //   33: iconst_1
    //   34: if_icmpeq -> 52
    //   37: iload #4
    //   39: iconst_2
    //   40: if_icmpeq -> 84
    //   43: iload #4
    //   45: iconst_3
    //   46: if_icmpeq -> 67
    //   49: goto -> 129
    //   52: aload_0
    //   53: iload_2
    //   54: iload_3
    //   55: invokespecial getDayAtLocation : (II)I
    //   58: istore #4
    //   60: aload_0
    //   61: iload #4
    //   63: invokespecial onDayClicked : (I)Z
    //   66: pop
    //   67: aload_0
    //   68: iconst_m1
    //   69: putfield mHighlightedDay : I
    //   72: aload_0
    //   73: iconst_0
    //   74: putfield mIsTouchHighlighted : Z
    //   77: aload_0
    //   78: invokevirtual invalidate : ()V
    //   81: goto -> 129
    //   84: aload_0
    //   85: iload_2
    //   86: iload_3
    //   87: invokespecial getDayAtLocation : (II)I
    //   90: istore_3
    //   91: aload_0
    //   92: iconst_1
    //   93: putfield mIsTouchHighlighted : Z
    //   96: aload_0
    //   97: getfield mHighlightedDay : I
    //   100: iload_3
    //   101: if_icmpeq -> 118
    //   104: aload_0
    //   105: iload_3
    //   106: putfield mHighlightedDay : I
    //   109: aload_0
    //   110: iload_3
    //   111: putfield mPreviouslyHighlightedDay : I
    //   114: aload_0
    //   115: invokevirtual invalidate : ()V
    //   118: iload #4
    //   120: ifne -> 129
    //   123: iload_3
    //   124: ifge -> 129
    //   127: iconst_0
    //   128: ireturn
    //   129: iconst_1
    //   130: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #350	-> 0
    //   #351	-> 10
    //   #353	-> 20
    //   #354	-> 26
    //   #371	-> 52
    //   #372	-> 60
    //   #376	-> 67
    //   #377	-> 72
    //   #378	-> 77
    //   #357	-> 84
    //   #358	-> 91
    //   #359	-> 96
    //   #360	-> 104
    //   #361	-> 109
    //   #362	-> 114
    //   #364	-> 118
    //   #366	-> 127
    //   #381	-> 129
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool2, bool1 = false;
    int i = paramKeyEvent.getKeyCode();
    if (i != 61) {
      if (i != 66 && i != 160) {
        switch (i) {
          default:
            bool2 = bool1;
            break;
          case 22:
            bool2 = bool1;
            if (paramKeyEvent.hasNoModifiers())
              bool2 = moveOneDay(isLayoutRtl() ^ true); 
            break;
          case 21:
            bool2 = bool1;
            if (paramKeyEvent.hasNoModifiers())
              bool2 = moveOneDay(isLayoutRtl()); 
            break;
          case 20:
            bool2 = bool1;
            if (paramKeyEvent.hasNoModifiers()) {
              ensureFocusedDay();
              i = this.mHighlightedDay;
              bool2 = bool1;
              if (i <= this.mDaysInMonth - 7) {
                this.mHighlightedDay = i + 7;
                bool2 = true;
              } 
            } 
            break;
          case 19:
            bool2 = bool1;
            if (paramKeyEvent.hasNoModifiers()) {
              ensureFocusedDay();
              i = this.mHighlightedDay;
              bool2 = bool1;
              if (i > 7) {
                this.mHighlightedDay = i - 7;
                bool2 = true;
              } 
            } 
            break;
          case 23:
            i = this.mHighlightedDay;
            bool2 = bool1;
            if (i != -1) {
              onDayClicked(i);
              return true;
            } 
            break;
        } 
      } else {
      
      } 
    } else {
      i = 0;
      if (paramKeyEvent.hasNoModifiers()) {
        i = 2;
      } else if (paramKeyEvent.hasModifiers(1)) {
        i = 1;
      } 
      bool2 = bool1;
      if (i != 0) {
        View view;
        ViewParent viewParent = getParent();
        SimpleMonthView simpleMonthView = this;
        while (true) {
          view = simpleMonthView.focusSearch(i);
          if (view != null && view != this) {
            View view1 = view;
            if (view.getParent() != viewParent)
              break; 
            continue;
          } 
          break;
        } 
        bool2 = bool1;
        if (view != null) {
          view.requestFocus();
          return true;
        } 
      } 
    } 
    if (bool2) {
      invalidate();
      return true;
    } 
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  private boolean moveOneDay(boolean paramBoolean) {
    ensureFocusedDay();
    boolean bool = false;
    if (paramBoolean) {
      paramBoolean = bool;
      if (!isLastDayOfWeek(this.mHighlightedDay)) {
        int i = this.mHighlightedDay;
        paramBoolean = bool;
        if (i < this.mDaysInMonth) {
          this.mHighlightedDay = i + 1;
          paramBoolean = true;
        } 
      } 
    } else {
      paramBoolean = bool;
      if (!isFirstDayOfWeek(this.mHighlightedDay)) {
        int i = this.mHighlightedDay;
        paramBoolean = bool;
        if (i > 1) {
          this.mHighlightedDay = i - 1;
          paramBoolean = true;
        } 
      } 
    } 
    return paramBoolean;
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    if (paramBoolean) {
      int i = findDayOffset();
      int j = 1;
      if (paramInt != 17) {
        if (paramInt != 33) {
          if (paramInt != 66) {
            if (paramInt == 130) {
              j = findClosestColumn(paramRect);
              j = j - i + 1;
              if (j < 1)
                j += 7; 
              this.mHighlightedDay = j;
            } 
          } else {
            int k = findClosestRow(paramRect);
            if (k != 0)
              j = 1 + k * 7 - i; 
            this.mHighlightedDay = j;
          } 
        } else {
          j = findClosestColumn(paramRect);
          int k = this.mDaysInMonth, m = (i + k) / 7;
          j = j - i + m * 7 + 1;
          if (j > k)
            j -= 7; 
          this.mHighlightedDay = j;
        } 
      } else {
        j = findClosestRow(paramRect);
        this.mHighlightedDay = Math.min(this.mDaysInMonth, (j + 1) * 7 - i);
      } 
      ensureFocusedDay();
      invalidate();
    } 
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
  }
  
  private int findClosestRow(Rect paramRect) {
    if (paramRect == null)
      return 3; 
    if (this.mDayHeight == 0)
      return 0; 
    int i = paramRect.centerY();
    TextPaint textPaint = this.mDayPaint;
    int j = this.mMonthHeight, k = this.mDayOfWeekHeight;
    int m = this.mDayHeight;
    float f = (textPaint.ascent() + textPaint.descent()) / 2.0F;
    int n = m / 2;
    j = (int)(i - (n + j + k) - f);
    j = Math.round(j / m);
    m = findDayOffset() + this.mDaysInMonth;
    n = m / 7;
    if (m % 7 == 0) {
      m = 1;
    } else {
      m = 0;
    } 
    m = MathUtils.constrain(j, 0, n - m);
    return m;
  }
  
  private int findClosestColumn(Rect paramRect) {
    if (paramRect == null)
      return 3; 
    if (this.mCellWidth == 0)
      return 0; 
    int i = paramRect.centerX(), j = this.mPaddingLeft;
    j = (i - j) / this.mCellWidth;
    j = MathUtils.constrain(j, 0, 6);
    if (isLayoutRtl())
      j = 7 - j - 1; 
    return j;
  }
  
  public void getFocusedRect(Rect paramRect) {
    int i = this.mHighlightedDay;
    if (i > 0) {
      getBoundsForDay(i, paramRect);
    } else {
      super.getFocusedRect(paramRect);
    } 
  }
  
  protected void onFocusLost() {
    if (!this.mIsTouchHighlighted) {
      this.mPreviouslyHighlightedDay = this.mHighlightedDay;
      this.mHighlightedDay = -1;
      invalidate();
    } 
    super.onFocusLost();
  }
  
  private void ensureFocusedDay() {
    if (this.mHighlightedDay != -1)
      return; 
    int i = this.mPreviouslyHighlightedDay;
    if (i != -1) {
      this.mHighlightedDay = i;
      return;
    } 
    i = this.mActivatedDay;
    if (i != -1) {
      this.mHighlightedDay = i;
      return;
    } 
    this.mHighlightedDay = 1;
  }
  
  private boolean isFirstDayOfWeek(int paramInt) {
    int i = findDayOffset();
    boolean bool = true;
    if ((i + paramInt - 1) % 7 != 0)
      bool = false; 
    return bool;
  }
  
  private boolean isLastDayOfWeek(int paramInt) {
    boolean bool;
    int i = findDayOffset();
    if ((i + paramInt) % 7 == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    int i = getPaddingLeft();
    int j = getPaddingTop();
    paramCanvas.translate(i, j);
    drawMonth(paramCanvas);
    drawDaysOfWeek(paramCanvas);
    drawDays(paramCanvas);
    paramCanvas.translate(-i, -j);
  }
  
  private void drawMonth(Canvas paramCanvas) {
    float f1 = this.mPaddedWidth / 2.0F;
    float f2 = this.mMonthPaint.ascent(), f3 = this.mMonthPaint.descent();
    f2 = (this.mMonthHeight - f2 + f3) / 2.0F;
    paramCanvas.drawText(this.mMonthYearLabel, f1, f2, this.mMonthPaint);
  }
  
  public String getMonthYearLabel() {
    return this.mMonthYearLabel;
  }
  
  private void drawDaysOfWeek(Canvas paramCanvas) {
    TextPaint textPaint = this.mDayOfWeekPaint;
    int i = this.mMonthHeight;
    int j = this.mDayOfWeekHeight;
    int k = this.mCellWidth;
    float f = (textPaint.ascent() + textPaint.descent()) / 2.0F;
    int m = j / 2;
    for (j = 0; j < 7; j++) {
      int n = k * j + k / 2;
      if (isLayoutRtl())
        n = this.mPaddedWidth - n; 
      String str = this.mDayOfWeekLabels[j];
      paramCanvas.drawText(str, n, (m + i) - f, textPaint);
    } 
  }
  
  private void drawDays(Canvas paramCanvas) {
    TextPaint textPaint = this.mDayPaint;
    int i = this.mMonthHeight + this.mDayOfWeekHeight;
    int j = this.mDayHeight;
    int k = this.mCellWidth;
    float f = (textPaint.ascent() + textPaint.descent()) / 2.0F;
    int m = j / 2 + i;
    int n;
    for (byte b = 1; b <= this.mDaysInMonth; b++, m = i3, n = i2) {
      boolean bool2, bool3;
      int i1 = k * n + k / 2;
      if (isLayoutRtl())
        i1 = this.mPaddedWidth - i1; 
      int i2 = 0;
      boolean bool = isDayEnabled(b);
      if (bool)
        i2 = 0x0 | 0x8; 
      int i3 = this.mActivatedDay;
      boolean bool1 = true;
      if (i3 == b) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (this.mHighlightedDay == b) {
        bool3 = true;
      } else {
        bool3 = false;
      } 
      if (bool2) {
        Paint paint;
        if (bool3) {
          paint = this.mDayHighlightSelectorPaint;
        } else {
          paint = this.mDaySelectorPaint;
        } 
        paramCanvas.drawCircle(i1, m, this.mDaySelectorRadius, paint);
        i3 = i2 | 0x20;
      } else {
        i3 = i2;
        if (bool3) {
          i3 = i2 | 0x10;
          if (bool)
            paramCanvas.drawCircle(i1, m, this.mDaySelectorRadius, this.mDayHighlightPaint); 
        } 
      } 
      if (this.mToday == b) {
        i2 = bool1;
      } else {
        i2 = 0;
      } 
      if (i2 != 0 && !bool2) {
        i2 = this.mDaySelectorPaint.getColor();
      } else {
        int[] arrayOfInt = StateSet.get(i3);
        i2 = this.mDayTextColor.getColorForState(arrayOfInt, 0);
      } 
      textPaint.setColor(i2);
      paramCanvas.drawText(this.mDayFormatter.format(b), i1, m - f, textPaint);
      n++;
      i3 = m;
      i2 = n;
      if (n == 7) {
        i3 = m + j;
        i2 = 0;
      } 
    } 
  }
  
  private boolean isDayEnabled(int paramInt) {
    boolean bool;
    if (paramInt >= this.mEnabledDayStart && paramInt <= this.mEnabledDayEnd) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isValidDayOfMonth(int paramInt) {
    boolean bool = true;
    if (paramInt < 1 || paramInt > this.mDaysInMonth)
      bool = false; 
    return bool;
  }
  
  private static boolean isValidDayOfWeek(int paramInt) {
    boolean bool = true;
    if (paramInt < 1 || paramInt > 7)
      bool = false; 
    return bool;
  }
  
  private static boolean isValidMonth(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 11) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setSelectedDay(int paramInt) {
    this.mActivatedDay = paramInt;
    this.mTouchHelper.invalidateRoot();
    invalidate();
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    if (isValidDayOfWeek(paramInt)) {
      this.mWeekStart = paramInt;
    } else {
      this.mWeekStart = this.mCalendar.getFirstDayOfWeek();
    } 
    updateDayOfWeekLabels();
    this.mTouchHelper.invalidateRoot();
    invalidate();
  }
  
  void setMonthParams(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this.mActivatedDay = paramInt1;
    if (isValidMonth(paramInt2))
      this.mMonth = paramInt2; 
    this.mYear = paramInt3;
    this.mCalendar.set(2, this.mMonth);
    this.mCalendar.set(1, this.mYear);
    this.mCalendar.set(5, 1);
    this.mDayOfWeekStart = this.mCalendar.get(7);
    if (isValidDayOfWeek(paramInt4)) {
      this.mWeekStart = paramInt4;
    } else {
      this.mWeekStart = this.mCalendar.getFirstDayOfWeek();
    } 
    Calendar calendar = Calendar.getInstance();
    this.mToday = -1;
    this.mDaysInMonth = getDaysInMonth(this.mMonth, this.mYear);
    paramInt1 = 0;
    while (true) {
      paramInt2 = this.mDaysInMonth;
      if (paramInt1 < paramInt2) {
        paramInt2 = paramInt1 + 1;
        if (sameDay(paramInt2, calendar))
          this.mToday = paramInt2; 
        paramInt1++;
        continue;
      } 
      break;
    } 
    this.mEnabledDayStart = paramInt1 = MathUtils.constrain(paramInt5, 1, paramInt2);
    this.mEnabledDayEnd = MathUtils.constrain(paramInt6, paramInt1, this.mDaysInMonth);
    updateMonthYearLabel();
    updateDayOfWeekLabels();
    this.mTouchHelper.invalidateRoot();
    invalidate();
  }
  
  private static int getDaysInMonth(int paramInt1, int paramInt2) {
    switch (paramInt1) {
      default:
        throw new IllegalArgumentException("Invalid Month");
      case 3:
      case 5:
      case 8:
      case 10:
        return 30;
      case 1:
        if (paramInt2 % 4 == 0 && (paramInt2 % 100 != 0 || paramInt2 % 400 == 0)) {
          paramInt1 = 29;
        } else {
          paramInt1 = 28;
        } 
        return paramInt1;
      case 0:
      case 2:
      case 4:
      case 6:
      case 7:
      case 9:
      case 11:
        break;
    } 
    return 31;
  }
  
  private boolean sameDay(int paramInt, Calendar paramCalendar) {
    int i = this.mYear;
    boolean bool = true;
    if (i != paramCalendar.get(1) || this.mMonth != paramCalendar.get(2) || 
      paramInt != paramCalendar.get(5))
      bool = false; 
    return bool;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = this.mDesiredDayHeight, j = this.mDesiredDayOfWeekHeight, k = this.mDesiredMonthHeight;
    int m = getPaddingTop(), n = getPaddingBottom();
    int i1 = this.mDesiredCellWidth;
    int i2 = getPaddingStart(), i3 = getPaddingEnd();
    paramInt1 = resolveSize(i1 * 7 + i2 + i3, paramInt1);
    paramInt2 = resolveSize(i * 6 + j + k + m + n, paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    requestLayout();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!paramBoolean)
      return; 
    int i = getPaddingLeft();
    int j = getPaddingTop();
    int k = getPaddingRight();
    int m = getPaddingBottom();
    paramInt1 = paramInt3 - paramInt1 - k - i;
    paramInt2 = paramInt4 - paramInt2 - m - j;
    if (paramInt1 == this.mPaddedWidth || paramInt2 == this.mPaddedHeight)
      return; 
    this.mPaddedWidth = paramInt1;
    this.mPaddedHeight = paramInt2;
    paramInt1 = getMeasuredHeight();
    float f = paramInt2 / (paramInt1 - j - m);
    paramInt2 = (int)(this.mDesiredMonthHeight * f);
    paramInt1 = this.mPaddedWidth / 7;
    this.mMonthHeight = paramInt2;
    this.mDayOfWeekHeight = (int)(this.mDesiredDayOfWeekHeight * f);
    this.mDayHeight = (int)(this.mDesiredDayHeight * f);
    this.mCellWidth = paramInt1;
    paramInt1 /= 2;
    paramInt3 = Math.min(i, k);
    paramInt4 = this.mDayHeight / 2;
    paramInt2 = this.mDesiredDaySelectorRadius;
    paramInt1 = Math.min(paramInt1 + paramInt3, paramInt4 + m);
    this.mDaySelectorRadius = Math.min(paramInt2, paramInt1);
    this.mTouchHelper.invalidateRoot();
  }
  
  private int findDayOffset() {
    int i = this.mDayOfWeekStart, j = this.mWeekStart, k = i - j;
    if (i < j)
      return k + 7; 
    return k;
  }
  
  private int getDayAtLocation(int paramInt1, int paramInt2) {
    paramInt1 -= getPaddingLeft();
    if (paramInt1 < 0 || paramInt1 >= this.mPaddedWidth)
      return -1; 
    int i = this.mMonthHeight + this.mDayOfWeekHeight;
    paramInt2 -= getPaddingTop();
    if (paramInt2 < i || paramInt2 >= this.mPaddedHeight)
      return -1; 
    if (isLayoutRtl())
      paramInt1 = this.mPaddedWidth - paramInt1; 
    paramInt2 = (paramInt2 - i) / this.mDayHeight;
    paramInt1 = paramInt1 * 7 / this.mPaddedWidth;
    paramInt1 = paramInt2 * 7 + paramInt1 + 1 - findDayOffset();
    if (!isValidDayOfMonth(paramInt1))
      return -1; 
    return paramInt1;
  }
  
  public boolean getBoundsForDay(int paramInt, Rect paramRect) {
    if (!isValidDayOfMonth(paramInt))
      return false; 
    int i = paramInt - 1 + findDayOffset();
    paramInt = i % 7;
    int j = this.mCellWidth;
    if (isLayoutRtl()) {
      paramInt = getWidth() - getPaddingRight() - (paramInt + 1) * j;
    } else {
      paramInt = getPaddingLeft() + paramInt * j;
    } 
    int k = i / 7;
    i = this.mDayHeight;
    int m = this.mMonthHeight, n = this.mDayOfWeekHeight;
    n = getPaddingTop() + m + n + k * i;
    paramRect.set(paramInt, n, paramInt + j, n + i);
    return true;
  }
  
  private boolean onDayClicked(int paramInt) {
    if (!isValidDayOfMonth(paramInt) || !isDayEnabled(paramInt))
      return false; 
    if (this.mOnDayClickListener != null) {
      Calendar calendar = Calendar.getInstance();
      calendar.set(this.mYear, this.mMonth, paramInt);
      this.mOnDayClickListener.onDayClick(this, calendar);
    } 
    this.mTouchHelper.sendEventForVirtualView(paramInt, 1);
    return true;
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    if (!isEnabled())
      return null; 
    int i = (int)(paramMotionEvent.getX() + 0.5F);
    int j = (int)(paramMotionEvent.getY() + 0.5F);
    i = getDayAtLocation(i, j);
    if (i >= 0)
      return PointerIcon.getSystemIcon(getContext(), 1002); 
    return super.onResolvePointerIcon(paramMotionEvent, paramInt);
  }
  
  class MonthViewTouchHelper extends ExploreByTouchHelper {
    final SimpleMonthView this$0;
    
    private final Rect mTempRect = new Rect();
    
    private final Calendar mTempCalendar = Calendar.getInstance();
    
    private static final String DATE_FORMAT = "dd MMMM yyyy";
    
    public MonthViewTouchHelper(View param1View) {
      super(param1View);
    }
    
    protected int getVirtualViewAt(float param1Float1, float param1Float2) {
      int i = SimpleMonthView.this.getDayAtLocation((int)(param1Float1 + 0.5F), (int)(0.5F + param1Float2));
      if (i != -1)
        return i; 
      return Integer.MIN_VALUE;
    }
    
    protected void getVisibleVirtualViews(IntArray param1IntArray) {
      for (byte b = 1; b <= SimpleMonthView.this.mDaysInMonth; b++)
        param1IntArray.add(b); 
    }
    
    protected void onPopulateEventForVirtualView(int param1Int, AccessibilityEvent param1AccessibilityEvent) {
      param1AccessibilityEvent.setContentDescription(getDayDescription(param1Int));
    }
    
    protected void onPopulateNodeForVirtualView(int param1Int, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      boolean bool = SimpleMonthView.this.getBoundsForDay(param1Int, this.mTempRect);
      if (!bool) {
        this.mTempRect.setEmpty();
        param1AccessibilityNodeInfo.setContentDescription("");
        param1AccessibilityNodeInfo.setBoundsInParent(this.mTempRect);
        param1AccessibilityNodeInfo.setVisibleToUser(false);
        return;
      } 
      param1AccessibilityNodeInfo.setText(getDayText(param1Int));
      param1AccessibilityNodeInfo.setContentDescription(getDayDescription(param1Int));
      if (param1Int == SimpleMonthView.this.mToday) {
        RelativeDateTimeFormatter relativeDateTimeFormatter = RelativeDateTimeFormatter.getInstance();
        param1AccessibilityNodeInfo.setStateDescription(relativeDateTimeFormatter.format(RelativeDateTimeFormatter.Direction.THIS, RelativeDateTimeFormatter.AbsoluteUnit.DAY));
      } 
      if (param1Int == SimpleMonthView.this.mActivatedDay)
        param1AccessibilityNodeInfo.setSelected(true); 
      param1AccessibilityNodeInfo.setBoundsInParent(this.mTempRect);
      bool = SimpleMonthView.this.isDayEnabled(param1Int);
      if (bool)
        param1AccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_CLICK); 
      param1AccessibilityNodeInfo.setEnabled(bool);
      param1AccessibilityNodeInfo.setClickable(true);
      if (param1Int == SimpleMonthView.this.mActivatedDay)
        param1AccessibilityNodeInfo.setChecked(true); 
    }
    
    protected boolean onPerformActionForVirtualView(int param1Int1, int param1Int2, Bundle param1Bundle) {
      if (param1Int2 != 16)
        return false; 
      return SimpleMonthView.this.onDayClicked(param1Int1);
    }
    
    private CharSequence getDayDescription(int param1Int) {
      if (SimpleMonthView.this.isValidDayOfMonth(param1Int)) {
        this.mTempCalendar.set(SimpleMonthView.this.mYear, SimpleMonthView.this.mMonth, param1Int);
        return DateFormat.format("dd MMMM yyyy", this.mTempCalendar.getTimeInMillis());
      } 
      return "";
    }
    
    private CharSequence getDayText(int param1Int) {
      if (SimpleMonthView.this.isValidDayOfMonth(param1Int))
        return SimpleMonthView.this.mDayFormatter.format(param1Int); 
      return null;
    }
  }
  
  class OnDayClickListener {
    public abstract void onDayClick(SimpleMonthView param1SimpleMonthView, Calendar param1Calendar);
  }
}
