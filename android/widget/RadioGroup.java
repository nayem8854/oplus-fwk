package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.autofill.AutofillManager;
import android.view.autofill.AutofillValue;
import com.android.internal.R;

public class RadioGroup extends LinearLayout {
  private static final String LOG_TAG = RadioGroup.class.getSimpleName();
  
  private int mCheckedId = -1;
  
  private boolean mProtectFromCheckedChange = false;
  
  private int mInitialCheckedId = -1;
  
  private CompoundButton.OnCheckedChangeListener mChildOnCheckedChangeListener;
  
  private OnCheckedChangeListener mOnCheckedChangeListener;
  
  private PassThroughHierarchyChangeListener mPassThroughListener;
  
  public RadioGroup(Context paramContext) {
    super(paramContext);
    setOrientation(1);
    init();
  }
  
  public RadioGroup(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    if (getImportantForAutofill() == 0)
      setImportantForAutofill(1); 
    setImportantForAccessibility(1);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RadioGroup, 16842878, 0);
    saveAttributeDataForStyleable(paramContext, R.styleable.RadioGroup, paramAttributeSet, typedArray, 16842878, 0);
    int i = typedArray.getResourceId(1, -1);
    if (i != -1) {
      this.mCheckedId = i;
      this.mInitialCheckedId = i;
    } 
    i = typedArray.getInt(0, 1);
    setOrientation(i);
    typedArray.recycle();
    init();
  }
  
  private void init() {
    this.mChildOnCheckedChangeListener = new CheckedStateTracker();
    PassThroughHierarchyChangeListener passThroughHierarchyChangeListener = new PassThroughHierarchyChangeListener();
    super.setOnHierarchyChangeListener(passThroughHierarchyChangeListener);
  }
  
  public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener paramOnHierarchyChangeListener) {
    PassThroughHierarchyChangeListener.access$202(this.mPassThroughListener, paramOnHierarchyChangeListener);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    int i = this.mCheckedId;
    if (i != -1) {
      this.mProtectFromCheckedChange = true;
      setCheckedStateForView(i, true);
      this.mProtectFromCheckedChange = false;
      setCheckedId(this.mCheckedId);
    } 
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    if (paramView instanceof RadioButton) {
      RadioButton radioButton = (RadioButton)paramView;
      if (radioButton.isChecked()) {
        this.mProtectFromCheckedChange = true;
        int i = this.mCheckedId;
        if (i != -1)
          setCheckedStateForView(i, false); 
        this.mProtectFromCheckedChange = false;
        setCheckedId(radioButton.getId());
      } 
    } 
    super.addView(paramView, paramInt, paramLayoutParams);
  }
  
  public void check(int paramInt) {
    if (paramInt != -1 && paramInt == this.mCheckedId)
      return; 
    int i = this.mCheckedId;
    if (i != -1)
      setCheckedStateForView(i, false); 
    if (paramInt != -1)
      setCheckedStateForView(paramInt, true); 
    setCheckedId(paramInt);
  }
  
  private void setCheckedId(int paramInt) {
    boolean bool;
    if (paramInt != this.mCheckedId) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mCheckedId = paramInt;
    OnCheckedChangeListener onCheckedChangeListener = this.mOnCheckedChangeListener;
    if (onCheckedChangeListener != null)
      onCheckedChangeListener.onCheckedChanged(this, paramInt); 
    if (bool) {
      AutofillManager autofillManager = (AutofillManager)this.mContext.getSystemService(AutofillManager.class);
      if (autofillManager != null)
        autofillManager.notifyValueChanged(this); 
    } 
  }
  
  private void setCheckedStateForView(int paramInt, boolean paramBoolean) {
    RadioButton radioButton = (RadioButton)findViewById(paramInt);
    if (radioButton != null && radioButton instanceof RadioButton)
      ((RadioButton)radioButton).setChecked(paramBoolean); 
  }
  
  public int getCheckedRadioButtonId() {
    return this.mCheckedId;
  }
  
  public void clearCheck() {
    check(-1);
  }
  
  public void setOnCheckedChangeListener(OnCheckedChangeListener paramOnCheckedChangeListener) {
    this.mOnCheckedChangeListener = paramOnCheckedChangeListener;
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  protected LinearLayout.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-2, -2);
  }
  
  public CharSequence getAccessibilityClassName() {
    return RadioGroup.class.getName();
  }
  
  class LayoutParams extends LinearLayout.LayoutParams {
    public LayoutParams(RadioGroup this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(RadioGroup this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(RadioGroup this$0, int param1Int1, float param1Float) {
      super(this$0, param1Int1, param1Float);
    }
    
    public LayoutParams(RadioGroup this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public LayoutParams(RadioGroup this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
    }
    
    protected void setBaseAttributes(TypedArray param1TypedArray, int param1Int1, int param1Int2) {
      if (param1TypedArray.hasValue(param1Int1)) {
        this.width = param1TypedArray.getLayoutDimension(param1Int1, "layout_width");
      } else {
        this.width = -2;
      } 
      if (param1TypedArray.hasValue(param1Int2)) {
        this.height = param1TypedArray.getLayoutDimension(param1Int2, "layout_height");
      } else {
        this.height = -2;
      } 
    }
  }
  
  class CheckedStateTracker implements CompoundButton.OnCheckedChangeListener {
    final RadioGroup this$0;
    
    private CheckedStateTracker() {}
    
    public void onCheckedChanged(CompoundButton param1CompoundButton, boolean param1Boolean) {
      if (RadioGroup.this.mProtectFromCheckedChange)
        return; 
      RadioGroup.access$302(RadioGroup.this, true);
      if (RadioGroup.this.mCheckedId != -1) {
        RadioGroup radioGroup = RadioGroup.this;
        radioGroup.setCheckedStateForView(radioGroup.mCheckedId, false);
      } 
      RadioGroup.access$302(RadioGroup.this, false);
      int i = param1CompoundButton.getId();
      RadioGroup.this.setCheckedId(i);
    }
  }
  
  class PassThroughHierarchyChangeListener implements ViewGroup.OnHierarchyChangeListener {
    private ViewGroup.OnHierarchyChangeListener mOnHierarchyChangeListener;
    
    final RadioGroup this$0;
    
    private PassThroughHierarchyChangeListener() {}
    
    public void onChildViewAdded(View param1View1, View param1View2) {
      if (param1View1 == RadioGroup.this && param1View2 instanceof RadioButton) {
        int i = param1View2.getId();
        if (i == -1) {
          i = View.generateViewId();
          param1View2.setId(i);
        } 
        RadioButton radioButton = (RadioButton)param1View2;
        RadioGroup radioGroup = RadioGroup.this;
        CompoundButton.OnCheckedChangeListener onCheckedChangeListener = radioGroup.mChildOnCheckedChangeListener;
        radioButton.setOnCheckedChangeWidgetListener(onCheckedChangeListener);
      } 
      ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener = this.mOnHierarchyChangeListener;
      if (onHierarchyChangeListener != null)
        onHierarchyChangeListener.onChildViewAdded(param1View1, param1View2); 
    }
    
    public void onChildViewRemoved(View param1View1, View param1View2) {
      if (param1View1 == RadioGroup.this && param1View2 instanceof RadioButton)
        ((RadioButton)param1View2).setOnCheckedChangeWidgetListener((CompoundButton.OnCheckedChangeListener)null); 
      ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener = this.mOnHierarchyChangeListener;
      if (onHierarchyChangeListener != null)
        onHierarchyChangeListener.onChildViewRemoved(param1View1, param1View2); 
    }
  }
  
  protected void onProvideStructure(ViewStructure paramViewStructure, int paramInt1, int paramInt2) {
    super.onProvideStructure(paramViewStructure, paramInt1, paramInt2);
    boolean bool = true;
    if (paramInt1 == 1) {
      if (this.mCheckedId == this.mInitialCheckedId)
        bool = false; 
      paramViewStructure.setDataIsSensitive(bool);
    } 
  }
  
  public void autofill(AutofillValue paramAutofillValue) {
    StringBuilder stringBuilder;
    if (!isEnabled())
      return; 
    if (!paramAutofillValue.isList()) {
      String str = LOG_TAG;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramAutofillValue);
      stringBuilder1.append(" could not be autofilled into ");
      stringBuilder1.append(this);
      Log.w(str, stringBuilder1.toString());
      return;
    } 
    int i = paramAutofillValue.getListValue();
    View view = getChildAt(i);
    if (view == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("RadioGroup.autoFill(): no child with index ");
      stringBuilder.append(i);
      Log.w("View", stringBuilder.toString());
      return;
    } 
    check(stringBuilder.getId());
  }
  
  public int getAutofillType() {
    boolean bool;
    if (isEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public AutofillValue getAutofillValue() {
    if (!isEnabled())
      return null; 
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.getId() == this.mCheckedId)
        return AutofillValue.forList(b); 
    } 
    return null;
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    if (getOrientation() == 0) {
      int i = getVisibleChildWithTextCount();
      paramAccessibilityNodeInfo.setCollectionInfo(AccessibilityNodeInfo.CollectionInfo.obtain(1, i, false, 1));
    } else {
      AccessibilityNodeInfo.CollectionInfo collectionInfo = AccessibilityNodeInfo.CollectionInfo.obtain(getVisibleChildWithTextCount(), 1, false, 1);
      paramAccessibilityNodeInfo.setCollectionInfo(collectionInfo);
    } 
  }
  
  private int getVisibleChildWithTextCount() {
    int i = 0;
    for (byte b = 0; b < getChildCount(); b++, i = j) {
      int j = i;
      if (getChildAt(b) instanceof RadioButton) {
        j = i;
        if (isVisibleWithText((RadioButton)getChildAt(b)))
          j = i + 1; 
      } 
    } 
    return i;
  }
  
  int getIndexWithinVisibleButtons(View paramView) {
    if (!(paramView instanceof RadioButton))
      return -1; 
    int i = 0;
    for (byte b = 0; b < getChildCount(); b++, i = j) {
      int j = i;
      if (getChildAt(b) instanceof RadioButton) {
        RadioButton radioButton = (RadioButton)getChildAt(b);
        if (radioButton == paramView)
          return i; 
        j = i;
        if (isVisibleWithText(radioButton))
          j = i + 1; 
      } 
    } 
    return -1;
  }
  
  private boolean isVisibleWithText(RadioButton paramRadioButton) {
    boolean bool;
    if (paramRadioButton.getVisibility() == 0 && !TextUtils.isEmpty(paramRadioButton.getText())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class OnCheckedChangeListener {
    public abstract void onCheckedChanged(RadioGroup param1RadioGroup, int param1Int);
  }
}
