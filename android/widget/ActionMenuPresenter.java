package android.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.view.ActionBarPolicy;
import com.android.internal.view.menu.ActionMenuItemView;
import com.android.internal.view.menu.BaseMenuPresenter;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuItemImpl;
import com.android.internal.view.menu.MenuPopupHelper;
import com.android.internal.view.menu.MenuPresenter;
import com.android.internal.view.menu.MenuView;
import com.android.internal.view.menu.ShowableListMenu;
import com.android.internal.view.menu.SubMenuBuilder;
import java.util.ArrayList;
import java.util.List;

public class ActionMenuPresenter extends BaseMenuPresenter implements ActionProvider.SubUiVisibilityListener {
  private final SparseBooleanArray mActionButtonGroups = new SparseBooleanArray();
  
  final PopupPresenterCallback mPopupPresenterCallback = new PopupPresenterCallback();
  
  private SparseArray<MenuItemLayoutInfo> mPreLayoutItems = new SparseArray<>();
  
  private SparseArray<MenuItemLayoutInfo> mPostLayoutItems = new SparseArray<>();
  
  private List<ItemAnimationInfo> mRunningItemAnimations = new ArrayList<>();
  
  private ViewTreeObserver.OnPreDrawListener mItemAnimationPreDrawListener = (ViewTreeObserver.OnPreDrawListener)new Object(this);
  
  private View.OnAttachStateChangeListener mAttachStateChangeListener = (View.OnAttachStateChangeListener)new Object(this);
  
  private static final boolean ACTIONBAR_ANIMATIONS_ENABLED = false;
  
  private static final int ITEM_ANIMATION_DURATION = 150;
  
  private ActionButtonSubmenu mActionButtonPopup;
  
  private int mActionItemWidthLimit;
  
  private boolean mExpandedActionViewsExclusive;
  
  private int mMaxItems;
  
  private boolean mMaxItemsSet;
  
  private int mMinCellSize;
  
  int mOpenSubMenuId;
  
  private OverflowMenuButton mOverflowButton;
  
  private OverflowPopup mOverflowPopup;
  
  private Drawable mPendingOverflowIcon;
  
  private boolean mPendingOverflowIconSet;
  
  private ActionMenuPopupCallback mPopupCallback;
  
  private OpenOverflowRunnable mPostedOpenRunnable;
  
  private boolean mReserveOverflow;
  
  private boolean mReserveOverflowSet;
  
  private boolean mStrictWidthLimit;
  
  private int mWidthLimit;
  
  private boolean mWidthLimitSet;
  
  public ActionMenuPresenter(Context paramContext) {
    super(paramContext, 17367073, 17367072);
  }
  
  public void initForMenu(Context paramContext, MenuBuilder paramMenuBuilder) {
    super.initForMenu(paramContext, paramMenuBuilder);
    Resources resources = paramContext.getResources();
    ActionBarPolicy actionBarPolicy = ActionBarPolicy.get(paramContext);
    if (!this.mReserveOverflowSet)
      this.mReserveOverflow = actionBarPolicy.showsOverflowMenuButton(); 
    if (!this.mWidthLimitSet)
      this.mWidthLimit = actionBarPolicy.getEmbeddedMenuWidthLimit(); 
    if (!this.mMaxItemsSet)
      this.mMaxItems = actionBarPolicy.getMaxActionButtons(); 
    int i = this.mWidthLimit;
    if (this.mReserveOverflow) {
      if (this.mOverflowButton == null) {
        OverflowMenuButton overflowMenuButton = new OverflowMenuButton(this.mSystemContext);
        if (this.mPendingOverflowIconSet) {
          overflowMenuButton.setImageDrawable(this.mPendingOverflowIcon);
          this.mPendingOverflowIcon = null;
          this.mPendingOverflowIconSet = false;
        } 
        int j = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.mOverflowButton.measure(j, j);
      } 
      i -= this.mOverflowButton.getMeasuredWidth();
    } else {
      this.mOverflowButton = null;
    } 
    this.mActionItemWidthLimit = i;
    this.mMinCellSize = (int)((resources.getDisplayMetrics()).density * 56.0F);
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    if (!this.mMaxItemsSet)
      this.mMaxItems = ActionBarPolicy.get(this.mContext).getMaxActionButtons(); 
    if (this.mMenu != null)
      this.mMenu.onItemsChanged(true); 
  }
  
  public void setWidthLimit(int paramInt, boolean paramBoolean) {
    this.mWidthLimit = paramInt;
    this.mStrictWidthLimit = paramBoolean;
    this.mWidthLimitSet = true;
  }
  
  public void setReserveOverflow(boolean paramBoolean) {
    this.mReserveOverflow = paramBoolean;
    this.mReserveOverflowSet = true;
  }
  
  public void setItemLimit(int paramInt) {
    this.mMaxItems = paramInt;
    this.mMaxItemsSet = true;
  }
  
  public void setExpandedActionViewsExclusive(boolean paramBoolean) {
    this.mExpandedActionViewsExclusive = paramBoolean;
  }
  
  public void setOverflowIcon(Drawable paramDrawable) {
    OverflowMenuButton overflowMenuButton = this.mOverflowButton;
    if (overflowMenuButton != null) {
      overflowMenuButton.setImageDrawable(paramDrawable);
    } else {
      this.mPendingOverflowIconSet = true;
      this.mPendingOverflowIcon = paramDrawable;
    } 
  }
  
  public Drawable getOverflowIcon() {
    OverflowMenuButton overflowMenuButton = this.mOverflowButton;
    if (overflowMenuButton != null)
      return overflowMenuButton.getDrawable(); 
    if (this.mPendingOverflowIconSet)
      return this.mPendingOverflowIcon; 
    return null;
  }
  
  public MenuView getMenuView(ViewGroup paramViewGroup) {
    MenuView menuView2 = this.mMenuView;
    MenuView menuView1 = super.getMenuView(paramViewGroup);
    if (menuView2 != menuView1) {
      ((ActionMenuView)menuView1).setPresenter(this);
      if (menuView2 != null)
        ((View)menuView2).removeOnAttachStateChangeListener(this.mAttachStateChangeListener); 
      ((View)menuView1).addOnAttachStateChangeListener(this.mAttachStateChangeListener);
    } 
    return menuView1;
  }
  
  public View getItemView(MenuItemImpl paramMenuItemImpl, View paramView, ViewGroup paramViewGroup) {
    boolean bool;
    View view = paramMenuItemImpl.getActionView();
    if (view == null || paramMenuItemImpl.hasCollapsibleActionView())
      view = super.getItemView(paramMenuItemImpl, paramView, paramViewGroup); 
    if (paramMenuItemImpl.isActionViewExpanded()) {
      bool = true;
    } else {
      bool = false;
    } 
    view.setVisibility(bool);
    ActionMenuView actionMenuView = (ActionMenuView)paramViewGroup;
    ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
    if (!actionMenuView.checkLayoutParams(layoutParams))
      view.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams)); 
    return view;
  }
  
  public void bindItemView(MenuItemImpl paramMenuItemImpl, MenuView.ItemView paramItemView) {
    paramItemView.initialize(paramMenuItemImpl, 0);
    ActionMenuView actionMenuView = (ActionMenuView)this.mMenuView;
    ActionMenuItemView actionMenuItemView = (ActionMenuItemView)paramItemView;
    actionMenuItemView.setItemInvoker(actionMenuView);
    if (this.mPopupCallback == null)
      this.mPopupCallback = new ActionMenuPopupCallback(); 
    actionMenuItemView.setPopupCallback(this.mPopupCallback);
  }
  
  public boolean shouldIncludeItem(int paramInt, MenuItemImpl paramMenuItemImpl) {
    return paramMenuItemImpl.isActionButton();
  }
  
  private void computeMenuItemAnimationInfo(boolean paramBoolean) {
    SparseArray<MenuItemLayoutInfo> sparseArray;
    ViewGroup viewGroup = (ViewGroup)this.mMenuView;
    int i = viewGroup.getChildCount();
    if (paramBoolean) {
      sparseArray = this.mPreLayoutItems;
    } else {
      sparseArray = this.mPostLayoutItems;
    } 
    for (byte b = 0; b < i; b++) {
      View view = viewGroup.getChildAt(b);
      int j = view.getId();
      if (j > 0 && view.getWidth() != 0 && view.getHeight() != 0) {
        MenuItemLayoutInfo menuItemLayoutInfo = new MenuItemLayoutInfo(view, paramBoolean);
        sparseArray.put(j, menuItemLayoutInfo);
      } 
    } 
  }
  
  private void runItemAnimations() {
    byte b;
    for (b = 0; b < this.mPreLayoutItems.size(); b++) {
      final ItemAnimationInfo menuItemLayoutInfoPre;
      int i = this.mPreLayoutItems.keyAt(b);
      MenuItemLayoutInfo menuItemLayoutInfo = this.mPreLayoutItems.get(i);
      int j = this.mPostLayoutItems.indexOfKey(i);
      if (j >= 0) {
        PropertyValuesHolder propertyValuesHolder1;
        SparseArray<MenuItemLayoutInfo> sparseArray = this.mPostLayoutItems;
        MenuItemLayoutInfo menuItemLayoutInfo1 = sparseArray.valueAt(j);
        sparseArray = null;
        PropertyValuesHolder propertyValuesHolder2 = null;
        if (menuItemLayoutInfo.left != menuItemLayoutInfo1.left)
          propertyValuesHolder1 = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, new float[] { (menuItemLayoutInfo.left - menuItemLayoutInfo1.left), 0.0F }); 
        if (menuItemLayoutInfo.top != menuItemLayoutInfo1.top)
          propertyValuesHolder2 = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[] { (menuItemLayoutInfo.top - menuItemLayoutInfo1.top), 0.0F }); 
        if (propertyValuesHolder1 != null || propertyValuesHolder2 != null) {
          ObjectAnimator objectAnimator;
          for (j = 0; j < this.mRunningItemAnimations.size(); j++) {
            itemAnimationInfo = this.mRunningItemAnimations.get(j);
            if (itemAnimationInfo.id == i && itemAnimationInfo.animType == 0)
              itemAnimationInfo.animator.cancel(); 
          } 
          if (propertyValuesHolder1 != null) {
            if (propertyValuesHolder2 != null) {
              objectAnimator = ObjectAnimator.ofPropertyValuesHolder(menuItemLayoutInfo1.view, new PropertyValuesHolder[] { propertyValuesHolder1, propertyValuesHolder2 });
            } else {
              objectAnimator = ObjectAnimator.ofPropertyValuesHolder(menuItemLayoutInfo1.view, new PropertyValuesHolder[] { (PropertyValuesHolder)objectAnimator });
            } 
          } else {
            objectAnimator = ObjectAnimator.ofPropertyValuesHolder(menuItemLayoutInfo1.view, new PropertyValuesHolder[] { propertyValuesHolder2 });
          } 
          objectAnimator.setDuration(150L);
          objectAnimator.start();
          ItemAnimationInfo itemAnimationInfo1 = new ItemAnimationInfo(i, menuItemLayoutInfo1, (Animator)objectAnimator, 0);
          this.mRunningItemAnimations.add(itemAnimationInfo1);
          objectAnimator.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
                final ActionMenuPresenter this$0;
                
                public void onAnimationEnd(Animator param1Animator) {
                  for (byte b = 0; b < ActionMenuPresenter.this.mRunningItemAnimations.size(); b++) {
                    if ((ActionMenuPresenter.this.mRunningItemAnimations.get(b)).animator == param1Animator) {
                      ActionMenuPresenter.this.mRunningItemAnimations.remove(b);
                      break;
                    } 
                  } 
                }
              });
        } 
        this.mPostLayoutItems.remove(i);
      } else {
        float f = 1.0F;
        for (j = 0; j < this.mRunningItemAnimations.size(); j++, f = f1) {
          ItemAnimationInfo itemAnimationInfo2 = this.mRunningItemAnimations.get(j);
          float f1 = f;
          if (itemAnimationInfo2.id == i) {
            f1 = f;
            if (itemAnimationInfo2.animType == 1) {
              f1 = itemAnimationInfo2.menuItemLayoutInfo.view.getAlpha();
              itemAnimationInfo2.animator.cancel();
            } 
          } 
        } 
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(((MenuItemLayoutInfo)itemAnimationInfo).view, View.ALPHA, new float[] { f, 0.0F });
        ((ViewGroup)this.mMenuView).getOverlay().add(((MenuItemLayoutInfo)itemAnimationInfo).view);
        objectAnimator.setDuration(150L);
        objectAnimator.start();
        ItemAnimationInfo itemAnimationInfo1 = new ItemAnimationInfo(i, (MenuItemLayoutInfo)itemAnimationInfo, (Animator)objectAnimator, 2);
        this.mRunningItemAnimations.add(itemAnimationInfo1);
        objectAnimator.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
              final ActionMenuPresenter this$0;
              
              final ActionMenuPresenter.MenuItemLayoutInfo val$menuItemLayoutInfoPre;
              
              public void onAnimationEnd(Animator param1Animator) {
                for (byte b = 0; b < ActionMenuPresenter.this.mRunningItemAnimations.size(); b++) {
                  if ((ActionMenuPresenter.this.mRunningItemAnimations.get(b)).animator == param1Animator) {
                    ActionMenuPresenter.this.mRunningItemAnimations.remove(b);
                    break;
                  } 
                } 
                ((ViewGroup)ActionMenuPresenter.this.mMenuView).getOverlay().remove(menuItemLayoutInfoPre.view);
              }
            });
      } 
    } 
    for (b = 0; b < this.mPostLayoutItems.size(); b++) {
      int i = this.mPostLayoutItems.keyAt(b);
      int j = this.mPostLayoutItems.indexOfKey(i);
      if (j >= 0) {
        SparseArray<MenuItemLayoutInfo> sparseArray = this.mPostLayoutItems;
        MenuItemLayoutInfo menuItemLayoutInfo = sparseArray.valueAt(j);
        float f = 0.0F;
        for (j = 0; j < this.mRunningItemAnimations.size(); j++, f = f1) {
          ItemAnimationInfo itemAnimationInfo1 = this.mRunningItemAnimations.get(j);
          float f1 = f;
          if (itemAnimationInfo1.id == i) {
            f1 = f;
            if (itemAnimationInfo1.animType == 2) {
              f1 = itemAnimationInfo1.menuItemLayoutInfo.view.getAlpha();
              itemAnimationInfo1.animator.cancel();
            } 
          } 
        } 
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(menuItemLayoutInfo.view, View.ALPHA, new float[] { f, 1.0F });
        objectAnimator.start();
        objectAnimator.setDuration(150L);
        final ItemAnimationInfo menuItemLayoutInfoPre = new ItemAnimationInfo(i, menuItemLayoutInfo, (Animator)objectAnimator, 1);
        this.mRunningItemAnimations.add(itemAnimationInfo);
        objectAnimator.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
              final ActionMenuPresenter this$0;
              
              public void onAnimationEnd(Animator param1Animator) {
                for (byte b = 0; b < ActionMenuPresenter.this.mRunningItemAnimations.size(); b++) {
                  if ((ActionMenuPresenter.this.mRunningItemAnimations.get(b)).animator == param1Animator) {
                    ActionMenuPresenter.this.mRunningItemAnimations.remove(b);
                    break;
                  } 
                } 
              }
            });
      } 
    } 
    this.mPreLayoutItems.clear();
    this.mPostLayoutItems.clear();
  }
  
  private void setupItemAnimations() {
    computeMenuItemAnimationInfo(true);
    ViewTreeObserver viewTreeObserver = ((View)this.mMenuView).getViewTreeObserver();
    ViewTreeObserver.OnPreDrawListener onPreDrawListener = this.mItemAnimationPreDrawListener;
    viewTreeObserver.addOnPreDrawListener(onPreDrawListener);
  }
  
  public void updateMenuView(boolean paramBoolean) {
    ViewGroup viewGroup = (ViewGroup)((View)this.mMenuView).getParent();
    super.updateMenuView(paramBoolean);
    ((View)this.mMenuView).requestLayout();
    if (this.mMenu != null) {
      ArrayList<MenuItemImpl> arrayList = this.mMenu.getActionItems();
      int k = arrayList.size();
      for (byte b = 0; b < k; b++) {
        ActionProvider actionProvider = ((MenuItemImpl)arrayList.get(b)).getActionProvider();
        if (actionProvider != null)
          actionProvider.setSubUiVisibilityListener(this); 
      } 
    } 
    if (this.mMenu != null) {
      ArrayList arrayList = this.mMenu.getNonActionItems();
    } else {
      viewGroup = null;
    } 
    int i = 0;
    int j = i;
    if (this.mReserveOverflow) {
      j = i;
      if (viewGroup != null) {
        i = viewGroup.size();
        j = 0;
        if (i == 1) {
          j = ((MenuItemImpl)viewGroup.get(0)).isActionViewExpanded() ^ true;
        } else if (i > 0) {
          j = 1;
        } 
      } 
    } 
    if (j != 0) {
      if (this.mOverflowButton == null)
        this.mOverflowButton = new OverflowMenuButton(this.mSystemContext); 
      viewGroup = (ViewGroup)this.mOverflowButton.getParent();
      if (viewGroup != this.mMenuView) {
        if (viewGroup != null)
          viewGroup.removeView(this.mOverflowButton); 
        viewGroup = (ActionMenuView)this.mMenuView;
        viewGroup.addView(this.mOverflowButton, viewGroup.generateOverflowButtonLayoutParams());
      } 
    } else {
      OverflowMenuButton overflowMenuButton = this.mOverflowButton;
      if (overflowMenuButton != null && overflowMenuButton.getParent() == this.mMenuView)
        ((ViewGroup)this.mMenuView).removeView(this.mOverflowButton); 
    } 
    ((ActionMenuView)this.mMenuView).setOverflowReserved(this.mReserveOverflow);
  }
  
  public boolean filterLeftoverView(ViewGroup paramViewGroup, int paramInt) {
    if (paramViewGroup.getChildAt(paramInt) == this.mOverflowButton)
      return false; 
    return super.filterLeftoverView(paramViewGroup, paramInt);
  }
  
  public boolean onSubMenuSelected(SubMenuBuilder paramSubMenuBuilder) {
    boolean bool2;
    if (!paramSubMenuBuilder.hasVisibleItems())
      return false; 
    SubMenuBuilder subMenuBuilder = paramSubMenuBuilder;
    while (subMenuBuilder.getParentMenu() != this.mMenu)
      subMenuBuilder = (SubMenuBuilder)subMenuBuilder.getParentMenu(); 
    View view = findViewForItem(subMenuBuilder.getItem());
    if (view == null)
      return false; 
    this.mOpenSubMenuId = paramSubMenuBuilder.getItem().getItemId();
    boolean bool1 = false;
    int i = paramSubMenuBuilder.size();
    byte b = 0;
    while (true) {
      bool2 = bool1;
      if (b < i) {
        MenuItem menuItem = paramSubMenuBuilder.getItem(b);
        if (menuItem.isVisible() && menuItem.getIcon() != null) {
          bool2 = true;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    ActionButtonSubmenu actionButtonSubmenu = new ActionButtonSubmenu(this.mContext, paramSubMenuBuilder, view);
    actionButtonSubmenu.setForceShowIcon(bool2);
    this.mActionButtonPopup.show();
    super.onSubMenuSelected(paramSubMenuBuilder);
    return true;
  }
  
  private View findViewForItem(MenuItem paramMenuItem) {
    ViewGroup viewGroup = (ViewGroup)this.mMenuView;
    if (viewGroup == null)
      return null; 
    int i = viewGroup.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = viewGroup.getChildAt(b);
      if (view instanceof MenuView.ItemView) {
        MenuView.ItemView itemView = (MenuView.ItemView)view;
        if (itemView.getItemData() == paramMenuItem)
          return view; 
      } 
    } 
    return null;
  }
  
  public boolean showOverflowMenu() {
    if (this.mReserveOverflow && !isOverflowMenuShowing() && this.mMenu != null && this.mMenuView != null && this.mPostedOpenRunnable == null) {
      MenuBuilder menuBuilder = this.mMenu;
      if (!menuBuilder.getNonActionItems().isEmpty()) {
        OverflowPopup overflowPopup = new OverflowPopup(this.mContext, this.mMenu, this.mOverflowButton, true);
        this.mPostedOpenRunnable = new OpenOverflowRunnable(overflowPopup);
        ((View)this.mMenuView).post(this.mPostedOpenRunnable);
        super.onSubMenuSelected(null);
        return true;
      } 
    } 
    return false;
  }
  
  public boolean hideOverflowMenu() {
    if (this.mPostedOpenRunnable != null && this.mMenuView != null) {
      ((View)this.mMenuView).removeCallbacks(this.mPostedOpenRunnable);
      this.mPostedOpenRunnable = null;
      return true;
    } 
    OverflowPopup overflowPopup = this.mOverflowPopup;
    if (overflowPopup != null) {
      overflowPopup.dismiss();
      return true;
    } 
    return false;
  }
  
  public boolean dismissPopupMenus() {
    boolean bool1 = hideOverflowMenu();
    boolean bool2 = hideSubMenus();
    return bool1 | bool2;
  }
  
  public boolean hideSubMenus() {
    ActionButtonSubmenu actionButtonSubmenu = this.mActionButtonPopup;
    if (actionButtonSubmenu != null) {
      actionButtonSubmenu.dismiss();
      return true;
    } 
    return false;
  }
  
  public boolean isOverflowMenuShowing() {
    boolean bool;
    OverflowPopup overflowPopup = this.mOverflowPopup;
    if (overflowPopup != null && overflowPopup.isShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isOverflowMenuShowPending() {
    return (this.mPostedOpenRunnable != null || isOverflowMenuShowing());
  }
  
  public boolean isOverflowReserved() {
    return this.mReserveOverflow;
  }
  
  public boolean flagActionItems() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mMenu : Lcom/android/internal/view/menu/MenuBuilder;
    //   4: ifnull -> 23
    //   7: aload_0
    //   8: getfield mMenu : Lcom/android/internal/view/menu/MenuBuilder;
    //   11: invokevirtual getVisibleItems : ()Ljava/util/ArrayList;
    //   14: astore_1
    //   15: aload_1
    //   16: invokevirtual size : ()I
    //   19: istore_2
    //   20: goto -> 27
    //   23: aconst_null
    //   24: astore_1
    //   25: iconst_0
    //   26: istore_2
    //   27: aload_0
    //   28: getfield mMaxItems : I
    //   31: istore_3
    //   32: aload_0
    //   33: getfield mActionItemWidthLimit : I
    //   36: istore #4
    //   38: iconst_0
    //   39: iconst_0
    //   40: invokestatic makeMeasureSpec : (II)I
    //   43: istore #5
    //   45: aload_0
    //   46: getfield mMenuView : Lcom/android/internal/view/menu/MenuView;
    //   49: checkcast android/view/ViewGroup
    //   52: astore #6
    //   54: iconst_0
    //   55: istore #7
    //   57: iconst_0
    //   58: istore #8
    //   60: iconst_0
    //   61: istore #9
    //   63: iconst_0
    //   64: istore #10
    //   66: iconst_0
    //   67: istore #11
    //   69: iload #11
    //   71: iload_2
    //   72: if_icmpge -> 150
    //   75: aload_1
    //   76: iload #11
    //   78: invokevirtual get : (I)Ljava/lang/Object;
    //   81: checkcast com/android/internal/view/menu/MenuItemImpl
    //   84: astore #12
    //   86: aload #12
    //   88: invokevirtual requiresActionButton : ()Z
    //   91: ifeq -> 100
    //   94: iinc #7, 1
    //   97: goto -> 117
    //   100: aload #12
    //   102: invokevirtual requestsActionButton : ()Z
    //   105: ifeq -> 114
    //   108: iinc #8, 1
    //   111: goto -> 117
    //   114: iconst_1
    //   115: istore #10
    //   117: iload_3
    //   118: istore #13
    //   120: aload_0
    //   121: getfield mExpandedActionViewsExclusive : Z
    //   124: ifeq -> 141
    //   127: iload_3
    //   128: istore #13
    //   130: aload #12
    //   132: invokevirtual isActionViewExpanded : ()Z
    //   135: ifeq -> 141
    //   138: iconst_0
    //   139: istore #13
    //   141: iinc #11, 1
    //   144: iload #13
    //   146: istore_3
    //   147: goto -> 69
    //   150: iload_3
    //   151: istore #11
    //   153: aload_0
    //   154: getfield mReserveOverflow : Z
    //   157: ifeq -> 182
    //   160: iload #10
    //   162: ifne -> 177
    //   165: iload_3
    //   166: istore #11
    //   168: iload #7
    //   170: iload #8
    //   172: iadd
    //   173: iload_3
    //   174: if_icmple -> 182
    //   177: iload_3
    //   178: iconst_1
    //   179: isub
    //   180: istore #11
    //   182: iload #11
    //   184: iload #7
    //   186: isub
    //   187: istore #14
    //   189: aload_0
    //   190: getfield mActionButtonGroups : Landroid/util/SparseBooleanArray;
    //   193: astore #12
    //   195: aload #12
    //   197: invokevirtual clear : ()V
    //   200: iconst_0
    //   201: istore #8
    //   203: iconst_0
    //   204: istore #11
    //   206: aload_0
    //   207: getfield mStrictWidthLimit : Z
    //   210: ifeq -> 235
    //   213: aload_0
    //   214: getfield mMinCellSize : I
    //   217: istore_3
    //   218: iload #4
    //   220: iload_3
    //   221: idiv
    //   222: istore #11
    //   224: iload_3
    //   225: iload #4
    //   227: iload_3
    //   228: irem
    //   229: iload #11
    //   231: idiv
    //   232: iadd
    //   233: istore #8
    //   235: iconst_0
    //   236: istore #13
    //   238: iload #9
    //   240: istore_3
    //   241: iload #7
    //   243: istore #9
    //   245: iload #4
    //   247: istore #10
    //   249: iload #14
    //   251: istore #7
    //   253: iload_2
    //   254: istore #4
    //   256: iload #13
    //   258: iload #4
    //   260: if_icmpge -> 734
    //   263: aload_1
    //   264: iload #13
    //   266: invokevirtual get : (I)Ljava/lang/Object;
    //   269: checkcast com/android/internal/view/menu/MenuItemImpl
    //   272: astore #15
    //   274: aload #15
    //   276: invokevirtual requiresActionButton : ()Z
    //   279: ifeq -> 383
    //   282: aload_0
    //   283: aload #15
    //   285: aconst_null
    //   286: aload #6
    //   288: invokevirtual getItemView : (Lcom/android/internal/view/menu/MenuItemImpl;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    //   291: astore #16
    //   293: aload_0
    //   294: getfield mStrictWidthLimit : Z
    //   297: ifeq -> 320
    //   300: iload #11
    //   302: aload #16
    //   304: iload #8
    //   306: iload #11
    //   308: iload #5
    //   310: iconst_0
    //   311: invokestatic measureChildForCells : (Landroid/view/View;IIII)I
    //   314: isub
    //   315: istore #11
    //   317: goto -> 329
    //   320: aload #16
    //   322: iload #5
    //   324: iload #5
    //   326: invokevirtual measure : (II)V
    //   329: aload #16
    //   331: invokevirtual getMeasuredWidth : ()I
    //   334: istore #14
    //   336: iload #10
    //   338: iload #14
    //   340: isub
    //   341: istore #10
    //   343: iload_3
    //   344: istore_2
    //   345: iload_3
    //   346: ifne -> 352
    //   349: iload #14
    //   351: istore_2
    //   352: aload #15
    //   354: invokevirtual getGroupId : ()I
    //   357: istore_3
    //   358: iload_3
    //   359: ifeq -> 372
    //   362: aload #12
    //   364: iload_3
    //   365: iconst_1
    //   366: invokevirtual put : (IZ)V
    //   369: goto -> 372
    //   372: aload #15
    //   374: iconst_1
    //   375: invokevirtual setIsActionButton : (Z)V
    //   378: iload_2
    //   379: istore_3
    //   380: goto -> 728
    //   383: aload #15
    //   385: invokevirtual requestsActionButton : ()Z
    //   388: ifeq -> 722
    //   391: aload #15
    //   393: invokevirtual getGroupId : ()I
    //   396: istore #17
    //   398: aload #12
    //   400: iload #17
    //   402: invokevirtual get : (I)Z
    //   405: istore #18
    //   407: iload #7
    //   409: ifgt -> 417
    //   412: iload #18
    //   414: ifeq -> 440
    //   417: iload #10
    //   419: ifle -> 440
    //   422: aload_0
    //   423: getfield mStrictWidthLimit : Z
    //   426: ifeq -> 434
    //   429: iload #11
    //   431: ifle -> 440
    //   434: iconst_1
    //   435: istore #19
    //   437: goto -> 443
    //   440: iconst_0
    //   441: istore #19
    //   443: iload #19
    //   445: ifeq -> 581
    //   448: aload_0
    //   449: aload #15
    //   451: aconst_null
    //   452: aload #6
    //   454: invokevirtual getItemView : (Lcom/android/internal/view/menu/MenuItemImpl;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    //   457: astore #16
    //   459: aload_0
    //   460: getfield mStrictWidthLimit : Z
    //   463: ifeq -> 498
    //   466: aload #16
    //   468: iload #8
    //   470: iload #11
    //   472: iload #5
    //   474: iconst_0
    //   475: invokestatic measureChildForCells : (Landroid/view/View;IIII)I
    //   478: istore_2
    //   479: iload #11
    //   481: iload_2
    //   482: isub
    //   483: istore #11
    //   485: iload_2
    //   486: ifne -> 495
    //   489: iconst_0
    //   490: istore #19
    //   492: goto -> 495
    //   495: goto -> 507
    //   498: aload #16
    //   500: iload #5
    //   502: iload #5
    //   504: invokevirtual measure : (II)V
    //   507: aload #16
    //   509: invokevirtual getMeasuredWidth : ()I
    //   512: istore #14
    //   514: iload #10
    //   516: iload #14
    //   518: isub
    //   519: istore #10
    //   521: iload_3
    //   522: istore_2
    //   523: iload_3
    //   524: ifne -> 530
    //   527: iload #14
    //   529: istore_2
    //   530: aload_0
    //   531: getfield mStrictWidthLimit : Z
    //   534: ifeq -> 558
    //   537: iload #10
    //   539: iflt -> 547
    //   542: iconst_1
    //   543: istore_3
    //   544: goto -> 549
    //   547: iconst_0
    //   548: istore_3
    //   549: iload_3
    //   550: iload #19
    //   552: iand
    //   553: istore #19
    //   555: goto -> 583
    //   558: iload #10
    //   560: iload_2
    //   561: iadd
    //   562: ifle -> 570
    //   565: iconst_1
    //   566: istore_3
    //   567: goto -> 572
    //   570: iconst_0
    //   571: istore_3
    //   572: iload_3
    //   573: iload #19
    //   575: iand
    //   576: istore #19
    //   578: goto -> 583
    //   581: iload_3
    //   582: istore_2
    //   583: iload #7
    //   585: istore_3
    //   586: iload #19
    //   588: ifeq -> 607
    //   591: iload #17
    //   593: ifeq -> 607
    //   596: aload #12
    //   598: iload #17
    //   600: iconst_1
    //   601: invokevirtual put : (IZ)V
    //   604: goto -> 691
    //   607: iload #18
    //   609: ifeq -> 691
    //   612: aload #12
    //   614: iload #17
    //   616: iconst_0
    //   617: invokevirtual put : (IZ)V
    //   620: iconst_0
    //   621: istore #14
    //   623: iload #14
    //   625: iload #13
    //   627: if_icmpge -> 685
    //   630: aload_1
    //   631: iload #14
    //   633: invokevirtual get : (I)Ljava/lang/Object;
    //   636: checkcast com/android/internal/view/menu/MenuItemImpl
    //   639: astore #16
    //   641: iload_3
    //   642: istore #7
    //   644: aload #16
    //   646: invokevirtual getGroupId : ()I
    //   649: iload #17
    //   651: if_icmpne -> 676
    //   654: iload_3
    //   655: istore #7
    //   657: aload #16
    //   659: invokevirtual isActionButton : ()Z
    //   662: ifeq -> 670
    //   665: iload_3
    //   666: iconst_1
    //   667: iadd
    //   668: istore #7
    //   670: aload #16
    //   672: iconst_0
    //   673: invokevirtual setIsActionButton : (Z)V
    //   676: iinc #14, 1
    //   679: iload #7
    //   681: istore_3
    //   682: goto -> 623
    //   685: iload_3
    //   686: istore #7
    //   688: goto -> 694
    //   691: iload_3
    //   692: istore #7
    //   694: iload #7
    //   696: istore_3
    //   697: iload #19
    //   699: ifeq -> 707
    //   702: iload #7
    //   704: iconst_1
    //   705: isub
    //   706: istore_3
    //   707: aload #15
    //   709: iload #19
    //   711: invokevirtual setIsActionButton : (Z)V
    //   714: iload_3
    //   715: istore #7
    //   717: iload_2
    //   718: istore_3
    //   719: goto -> 728
    //   722: aload #15
    //   724: iconst_0
    //   725: invokevirtual setIsActionButton : (Z)V
    //   728: iinc #13, 1
    //   731: goto -> 256
    //   734: iconst_1
    //   735: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #632	-> 0
    //   #633	-> 7
    //   #634	-> 15
    //   #636	-> 23
    //   #637	-> 25
    //   #640	-> 27
    //   #641	-> 32
    //   #642	-> 38
    //   #643	-> 45
    //   #645	-> 54
    //   #646	-> 57
    //   #647	-> 60
    //   #648	-> 63
    //   #649	-> 66
    //   #650	-> 75
    //   #651	-> 86
    //   #652	-> 94
    //   #653	-> 100
    //   #654	-> 108
    //   #656	-> 114
    //   #658	-> 117
    //   #661	-> 138
    //   #649	-> 141
    //   #666	-> 150
    //   #668	-> 177
    //   #670	-> 182
    //   #672	-> 189
    //   #673	-> 195
    //   #675	-> 200
    //   #676	-> 203
    //   #677	-> 206
    //   #678	-> 213
    //   #679	-> 224
    //   #680	-> 224
    //   #684	-> 235
    //   #685	-> 263
    //   #687	-> 274
    //   #688	-> 282
    //   #689	-> 293
    //   #690	-> 300
    //   #693	-> 320
    //   #695	-> 329
    //   #696	-> 336
    //   #697	-> 343
    //   #698	-> 349
    //   #700	-> 352
    //   #701	-> 358
    //   #702	-> 362
    //   #701	-> 372
    //   #704	-> 372
    //   #705	-> 378
    //   #708	-> 391
    //   #709	-> 398
    //   #710	-> 407
    //   #713	-> 443
    //   #714	-> 448
    //   #715	-> 459
    //   #716	-> 466
    //   #718	-> 479
    //   #719	-> 485
    //   #720	-> 489
    //   #719	-> 495
    //   #722	-> 495
    //   #723	-> 498
    //   #725	-> 507
    //   #726	-> 514
    //   #727	-> 521
    //   #728	-> 527
    //   #731	-> 530
    //   #732	-> 537
    //   #735	-> 558
    //   #713	-> 581
    //   #739	-> 583
    //   #740	-> 596
    //   #741	-> 607
    //   #743	-> 612
    //   #744	-> 620
    //   #745	-> 630
    //   #746	-> 641
    //   #748	-> 654
    //   #749	-> 670
    //   #744	-> 676
    //   #741	-> 691
    //   #754	-> 691
    //   #756	-> 707
    //   #757	-> 714
    //   #759	-> 722
    //   #684	-> 728
    //   #762	-> 734
  }
  
  public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean) {
    dismissPopupMenus();
    super.onCloseMenu(paramMenuBuilder, paramBoolean);
  }
  
  public Parcelable onSaveInstanceState() {
    SavedState savedState = new SavedState();
    savedState.openSubMenuId = this.mOpenSubMenuId;
    return savedState;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    if (((SavedState)paramParcelable).openSubMenuId > 0) {
      MenuItem menuItem = this.mMenu.findItem(((SavedState)paramParcelable).openSubMenuId);
      if (menuItem != null) {
        SubMenuBuilder subMenuBuilder = (SubMenuBuilder)menuItem.getSubMenu();
        onSubMenuSelected(subMenuBuilder);
      } 
    } 
  }
  
  public void onSubUiVisibilityChanged(boolean paramBoolean) {
    if (paramBoolean) {
      super.onSubMenuSelected(null);
    } else if (this.mMenu != null) {
      this.mMenu.close(false);
    } 
  }
  
  public void setMenuView(ActionMenuView paramActionMenuView) {
    if (paramActionMenuView != this.mMenuView) {
      if (this.mMenuView != null)
        ((View)this.mMenuView).removeOnAttachStateChangeListener(this.mAttachStateChangeListener); 
      this.mMenuView = paramActionMenuView;
      paramActionMenuView.initialize(this.mMenu);
      paramActionMenuView.addOnAttachStateChangeListener(this.mAttachStateChangeListener);
    } 
  }
  
  class SavedState implements Parcelable {
    SavedState() {}
    
    SavedState(ActionMenuPresenter this$0) {
      this.openSubMenuId = this$0.readInt();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.openSubMenuId);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
        public ActionMenuPresenter.SavedState createFromParcel(Parcel param2Parcel) {
          return new ActionMenuPresenter.SavedState(param2Parcel);
        }
        
        public ActionMenuPresenter.SavedState[] newArray(int param2Int) {
          return new ActionMenuPresenter.SavedState[param2Int];
        }
      };
    
    public int openSubMenuId;
  }
  
  class OverflowMenuButton extends ImageButton implements ActionMenuView.ActionMenuChildView {
    final ActionMenuPresenter this$0;
    
    public OverflowMenuButton(Context param1Context) {
      super(param1Context, (AttributeSet)null, 16843510);
      setClickable(true);
      setFocusable(true);
      setVisibility(0);
      setEnabled(true);
      setOnTouchListener(new ForwardingListener(this) {
            final ActionMenuPresenter.OverflowMenuButton this$1;
            
            final ActionMenuPresenter val$this$0;
            
            ForwardingListener(View param1View) {
              super(param1View);
            }
            
            public ShowableListMenu getPopup() {
              if (ActionMenuPresenter.this.mOverflowPopup == null)
                return null; 
              return (ShowableListMenu)ActionMenuPresenter.this.mOverflowPopup.getPopup();
            }
            
            public boolean onForwardingStarted() {
              ActionMenuPresenter.this.showOverflowMenu();
              return true;
            }
            
            public boolean onForwardingStopped() {
              if (ActionMenuPresenter.this.mPostedOpenRunnable != null)
                return false; 
              ActionMenuPresenter.this.hideOverflowMenu();
              return true;
            }
          });
    }
    
    public boolean performClick() {
      if (super.performClick())
        return true; 
      playSoundEffect(0);
      ActionMenuPresenter.this.showOverflowMenu();
      return true;
    }
    
    public boolean needsDividerBefore() {
      return false;
    }
    
    public boolean needsDividerAfter() {
      return false;
    }
    
    public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      super.onInitializeAccessibilityNodeInfoInternal(param1AccessibilityNodeInfo);
      param1AccessibilityNodeInfo.setCanOpenPopup(true);
    }
    
    protected boolean setFrame(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      boolean bool = super.setFrame(param1Int1, param1Int2, param1Int3, param1Int4);
      Drawable drawable1 = getDrawable();
      Drawable drawable2 = getBackground();
      if (drawable1 != null && drawable2 != null) {
        int i = getWidth();
        param1Int4 = getHeight();
        param1Int1 = Math.max(i, param1Int4) / 2;
        int j = getPaddingLeft(), k = getPaddingRight();
        param1Int3 = getPaddingTop();
        param1Int2 = getPaddingBottom();
        j = (i + j - k) / 2;
        param1Int2 = (param1Int4 + param1Int3 - param1Int2) / 2;
        drawable2.setHotspotBounds(j - param1Int1, param1Int2 - param1Int1, j + param1Int1, param1Int2 + param1Int1);
      } 
      return bool;
    }
  }
  
  class null extends ForwardingListener {
    final ActionMenuPresenter.OverflowMenuButton this$1;
    
    final ActionMenuPresenter val$this$0;
    
    null(View param1View) {
      super(param1View);
    }
    
    public ShowableListMenu getPopup() {
      if (ActionMenuPresenter.this.mOverflowPopup == null)
        return null; 
      return (ShowableListMenu)ActionMenuPresenter.this.mOverflowPopup.getPopup();
    }
    
    public boolean onForwardingStarted() {
      ActionMenuPresenter.this.showOverflowMenu();
      return true;
    }
    
    public boolean onForwardingStopped() {
      if (ActionMenuPresenter.this.mPostedOpenRunnable != null)
        return false; 
      ActionMenuPresenter.this.hideOverflowMenu();
      return true;
    }
  }
  
  private class OverflowPopup extends MenuPopupHelper {
    final ActionMenuPresenter this$0;
    
    public OverflowPopup(Context param1Context, MenuBuilder param1MenuBuilder, View param1View, boolean param1Boolean) {
      super(param1Context, param1MenuBuilder, param1View, param1Boolean, 16843844);
      setGravity(8388613);
      setPresenterCallback(ActionMenuPresenter.this.mPopupPresenterCallback);
    }
    
    protected void onDismiss() {
      if (ActionMenuPresenter.this.mMenu != null)
        ActionMenuPresenter.this.mMenu.close(); 
      ActionMenuPresenter.access$1102(ActionMenuPresenter.this, (OverflowPopup)null);
      super.onDismiss();
    }
  }
  
  private class ActionButtonSubmenu extends MenuPopupHelper {
    final ActionMenuPresenter this$0;
    
    public ActionButtonSubmenu(Context param1Context, SubMenuBuilder param1SubMenuBuilder, View param1View) {
      super(param1Context, (MenuBuilder)param1SubMenuBuilder, param1View, false, 16843844);
      MenuItemImpl menuItemImpl = (MenuItemImpl)param1SubMenuBuilder.getItem();
      if (!menuItemImpl.isActionButton()) {
        View view;
        if (ActionMenuPresenter.this.mOverflowButton == null) {
          view = (View)ActionMenuPresenter.this.mMenuView;
        } else {
          view = ActionMenuPresenter.this.mOverflowButton;
        } 
        setAnchorView(view);
      } 
      setPresenterCallback(ActionMenuPresenter.this.mPopupPresenterCallback);
    }
    
    protected void onDismiss() {
      ActionMenuPresenter.access$1702(ActionMenuPresenter.this, (ActionButtonSubmenu)null);
      ActionMenuPresenter.this.mOpenSubMenuId = 0;
      super.onDismiss();
    }
  }
  
  class PopupPresenterCallback implements MenuPresenter.Callback {
    final ActionMenuPresenter this$0;
    
    private PopupPresenterCallback() {}
    
    public boolean onOpenSubMenu(MenuBuilder param1MenuBuilder) {
      boolean bool = false;
      if (param1MenuBuilder == null)
        return false; 
      ActionMenuPresenter.this.mOpenSubMenuId = ((SubMenuBuilder)param1MenuBuilder).getItem().getItemId();
      MenuPresenter.Callback callback = ActionMenuPresenter.this.getCallback();
      if (callback != null)
        bool = callback.onOpenSubMenu(param1MenuBuilder); 
      return bool;
    }
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {
      if (param1MenuBuilder instanceof SubMenuBuilder)
        param1MenuBuilder.getRootMenu().close(false); 
      MenuPresenter.Callback callback = ActionMenuPresenter.this.getCallback();
      if (callback != null)
        callback.onCloseMenu(param1MenuBuilder, param1Boolean); 
    }
  }
  
  class OpenOverflowRunnable implements Runnable {
    private ActionMenuPresenter.OverflowPopup mPopup;
    
    final ActionMenuPresenter this$0;
    
    public OpenOverflowRunnable(ActionMenuPresenter.OverflowPopup param1OverflowPopup) {
      this.mPopup = param1OverflowPopup;
    }
    
    public void run() {
      if (ActionMenuPresenter.this.mMenu != null)
        ActionMenuPresenter.this.mMenu.changeMenuMode(); 
      View view = (View)ActionMenuPresenter.this.mMenuView;
      if (view != null && view.getWindowToken() != null && this.mPopup.tryShow())
        ActionMenuPresenter.access$1102(ActionMenuPresenter.this, this.mPopup); 
      ActionMenuPresenter.access$1202(ActionMenuPresenter.this, (OpenOverflowRunnable)null);
    }
  }
  
  class ActionMenuPopupCallback extends ActionMenuItemView.PopupCallback {
    final ActionMenuPresenter this$0;
    
    private ActionMenuPopupCallback() {}
    
    public ShowableListMenu getPopup() {
      ShowableListMenu showableListMenu;
      if (ActionMenuPresenter.this.mActionButtonPopup != null) {
        showableListMenu = (ShowableListMenu)ActionMenuPresenter.this.mActionButtonPopup.getPopup();
      } else {
        showableListMenu = null;
      } 
      return showableListMenu;
    }
  }
  
  class MenuItemLayoutInfo {
    int left;
    
    int top;
    
    View view;
    
    MenuItemLayoutInfo(ActionMenuPresenter this$0, boolean param1Boolean) {
      this.left = this$0.getLeft();
      this.top = this$0.getTop();
      if (param1Boolean) {
        this.left = (int)(this.left + this$0.getTranslationX());
        this.top = (int)(this.top + this$0.getTranslationY());
      } 
      this.view = (View)this$0;
    }
  }
  
  class ItemAnimationInfo {
    static final int FADE_IN = 1;
    
    static final int FADE_OUT = 2;
    
    static final int MOVE = 0;
    
    int animType;
    
    Animator animator;
    
    int id;
    
    ActionMenuPresenter.MenuItemLayoutInfo menuItemLayoutInfo;
    
    ItemAnimationInfo(ActionMenuPresenter this$0, ActionMenuPresenter.MenuItemLayoutInfo param1MenuItemLayoutInfo, Animator param1Animator, int param1Int1) {
      this.id = this$0;
      this.menuItemLayoutInfo = param1MenuItemLayoutInfo;
      this.animator = param1Animator;
      this.animType = param1Int1;
    }
  }
}
