package android.widget;

import android.content.Context;
import android.os.LocaleList;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.MathUtils;

public class TextInputTimePickerView extends RelativeLayout {
  private static final int AM = 0;
  
  public static final int AMPM = 2;
  
  public static final int HOURS = 0;
  
  public static final int MINUTES = 1;
  
  private static final int PM = 1;
  
  private final Spinner mAmPmSpinner;
  
  private final TextView mErrorLabel;
  
  private boolean mErrorShowing;
  
  private final EditText mHourEditText;
  
  private boolean mHourFormatStartsAtZero;
  
  private final TextView mHourLabel;
  
  private final TextView mInputSeparatorView;
  
  private boolean mIs24Hour;
  
  private OnValueTypedListener mListener;
  
  private final EditText mMinuteEditText;
  
  private final TextView mMinuteLabel;
  
  private boolean mTimeSet;
  
  public TextInputTimePickerView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public TextInputTimePickerView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public TextInputTimePickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TextInputTimePickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    inflate(paramContext, 17367340, this);
    this.mHourEditText = findViewById(16909082);
    this.mMinuteEditText = findViewById(16909083);
    this.mInputSeparatorView = findViewById(16909085);
    this.mErrorLabel = findViewById(16909111);
    this.mHourLabel = findViewById(16909112);
    this.mMinuteLabel = findViewById(16909113);
    this.mHourEditText.addTextChangedListener((TextWatcher)new Object(this, paramContext));
    this.mMinuteEditText.addTextChangedListener((TextWatcher)new Object(this));
    this.mAmPmSpinner = findViewById(16908754);
    String[] arrayOfString = TimePicker.getAmPmStrings(paramContext);
    ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter(paramContext, 17367049);
    arrayAdapter.add(TimePickerClockDelegate.obtainVerbatim(arrayOfString[0]));
    arrayAdapter.add(TimePickerClockDelegate.obtainVerbatim(arrayOfString[1]));
    this.mAmPmSpinner.setAdapter(arrayAdapter);
    this.mAmPmSpinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener)new Object(this));
  }
  
  void setListener(OnValueTypedListener paramOnValueTypedListener) {
    this.mListener = paramOnValueTypedListener;
  }
  
  void setHourFormat(int paramInt) {
    this.mHourEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(paramInt) });
    this.mMinuteEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(paramInt) });
    LocaleList localeList = this.mContext.getResources().getConfiguration().getLocales();
    this.mHourEditText.setImeHintLocales(localeList);
    this.mMinuteEditText.setImeHintLocales(localeList);
  }
  
  boolean validateInput() {
    String str1, str2;
    if (TextUtils.isEmpty(this.mHourEditText.getText())) {
      str1 = this.mHourEditText.getHint().toString();
    } else {
      str1 = this.mHourEditText.getText().toString();
    } 
    if (TextUtils.isEmpty(this.mMinuteEditText.getText())) {
      str2 = this.mMinuteEditText.getHint().toString();
    } else {
      str2 = this.mMinuteEditText.getText().toString();
    } 
    boolean bool = parseAndSetHourInternal(str1);
    boolean bool1 = true;
    if (bool && 
      parseAndSetMinuteInternal(str2)) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool)
      bool1 = false; 
    setError(bool1);
    return bool;
  }
  
  void updateSeparator(String paramString) {
    this.mInputSeparatorView.setText(paramString);
  }
  
  private void setError(boolean paramBoolean) {
    this.mErrorShowing = paramBoolean;
    TextView textView = this.mErrorLabel;
    boolean bool = false;
    if (paramBoolean) {
      b = 0;
    } else {
      b = 4;
    } 
    textView.setVisibility(b);
    textView = this.mHourLabel;
    if (paramBoolean) {
      b = 4;
    } else {
      b = 0;
    } 
    textView.setVisibility(b);
    textView = this.mMinuteLabel;
    byte b = bool;
    if (paramBoolean)
      b = 4; 
    textView.setVisibility(b);
  }
  
  private void setTimeSet(boolean paramBoolean) {
    if (this.mTimeSet || paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    this.mTimeSet = paramBoolean;
  }
  
  private boolean isTimeSet() {
    return this.mTimeSet;
  }
  
  void updateTextInputValues(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool;
    this.mIs24Hour = paramBoolean1;
    this.mHourFormatStartsAtZero = paramBoolean2;
    Spinner spinner = this.mAmPmSpinner;
    if (paramBoolean1) {
      bool = true;
    } else {
      bool = false;
    } 
    spinner.setVisibility(bool);
    if (paramInt3 == 0) {
      this.mAmPmSpinner.setSelection(0);
    } else {
      this.mAmPmSpinner.setSelection(1);
    } 
    if (isTimeSet()) {
      this.mHourEditText.setText(String.format("%d", new Object[] { Integer.valueOf(paramInt1) }));
      this.mMinuteEditText.setText(String.format("%02d", new Object[] { Integer.valueOf(paramInt2) }));
    } else {
      this.mHourEditText.setHint(String.format("%d", new Object[] { Integer.valueOf(paramInt1) }));
      this.mMinuteEditText.setHint(String.format("%02d", new Object[] { Integer.valueOf(paramInt2) }));
    } 
    if (this.mErrorShowing)
      validateInput(); 
  }
  
  private boolean parseAndSetHourInternal(String paramString) {
    try {
      int i = Integer.parseInt(paramString);
      boolean bool = isValidLocalizedHour(i);
      int j = 1;
      if (!bool) {
        int k;
        if (this.mHourFormatStartsAtZero)
          j = 0; 
        if (this.mIs24Hour) {
          k = 23;
        } else {
          k = j + 11;
        } 
        OnValueTypedListener onValueTypedListener = this.mListener;
        j = MathUtils.constrain(i, j, k);
        onValueTypedListener.onValueChanged(0, getHourOfDayFromLocalizedHour(j));
        return false;
      } 
      this.mListener.onValueChanged(0, getHourOfDayFromLocalizedHour(i));
      setTimeSet(true);
      return true;
    } catch (NumberFormatException numberFormatException) {
      return false;
    } 
  }
  
  private boolean parseAndSetMinuteInternal(String paramString) {
    try {
      int i = Integer.parseInt(paramString);
      if (i < 0 || i > 59) {
        this.mListener.onValueChanged(1, MathUtils.constrain(i, 0, 59));
        return false;
      } 
      this.mListener.onValueChanged(1, i);
      setTimeSet(true);
      return true;
    } catch (NumberFormatException numberFormatException) {
      return false;
    } 
  }
  
  private boolean isValidLocalizedHour(int paramInt) {
    byte b;
    boolean bool = this.mHourFormatStartsAtZero;
    boolean bool1 = true;
    int i = bool ^ true;
    if (this.mIs24Hour) {
      b = 23;
    } else {
      b = 11;
    } 
    if (paramInt < i || paramInt > b + i)
      bool1 = false; 
    return bool1;
  }
  
  private int getHourOfDayFromLocalizedHour(int paramInt) {
    int j, i = paramInt;
    if (this.mIs24Hour) {
      j = i;
      if (!this.mHourFormatStartsAtZero) {
        j = i;
        if (paramInt == 24)
          j = 0; 
      } 
    } else {
      int k = i;
      if (!this.mHourFormatStartsAtZero) {
        k = i;
        if (paramInt == 12)
          k = 0; 
      } 
      j = k;
      if (this.mAmPmSpinner.getSelectedItemPosition() == 1)
        j = k + 12; 
    } 
    return j;
  }
  
  class OnValueTypedListener {
    public abstract void onValueChanged(int param1Int1, int param1Int2);
  }
}
