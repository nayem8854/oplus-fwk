package android.widget;

import android.database.DataSetObserver;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class HeaderViewListAdapter implements WrapperListAdapter, Filterable {
  static final ArrayList<ListView.FixedViewInfo> EMPTY_INFO_LIST = new ArrayList<>();
  
  private static final String TAG = "HeaderViewListAdapter";
  
  private final ListAdapter mAdapter;
  
  boolean mAreAllFixedViewsSelectable;
  
  ArrayList<ListView.FixedViewInfo> mFooterViewInfos;
  
  ArrayList<ListView.FixedViewInfo> mHeaderViewInfos;
  
  private final boolean mIsFilterable;
  
  public HeaderViewListAdapter(ArrayList<ListView.FixedViewInfo> paramArrayList1, ArrayList<ListView.FixedViewInfo> paramArrayList2, ListAdapter paramListAdapter) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_0
    //   5: aload_3
    //   6: putfield mAdapter : Landroid/widget/ListAdapter;
    //   9: aload_0
    //   10: aload_3
    //   11: instanceof android/widget/Filterable
    //   14: putfield mIsFilterable : Z
    //   17: aload_1
    //   18: ifnonnull -> 31
    //   21: aload_0
    //   22: getstatic android/widget/HeaderViewListAdapter.EMPTY_INFO_LIST : Ljava/util/ArrayList;
    //   25: putfield mHeaderViewInfos : Ljava/util/ArrayList;
    //   28: goto -> 36
    //   31: aload_0
    //   32: aload_1
    //   33: putfield mHeaderViewInfos : Ljava/util/ArrayList;
    //   36: aload_2
    //   37: ifnonnull -> 50
    //   40: aload_0
    //   41: getstatic android/widget/HeaderViewListAdapter.EMPTY_INFO_LIST : Ljava/util/ArrayList;
    //   44: putfield mFooterViewInfos : Ljava/util/ArrayList;
    //   47: goto -> 55
    //   50: aload_0
    //   51: aload_2
    //   52: putfield mFooterViewInfos : Ljava/util/ArrayList;
    //   55: aload_0
    //   56: getfield mHeaderViewInfos : Ljava/util/ArrayList;
    //   59: astore_1
    //   60: aload_0
    //   61: aload_1
    //   62: invokespecial areAllListInfosSelectable : (Ljava/util/ArrayList;)Z
    //   65: ifeq -> 87
    //   68: aload_0
    //   69: getfield mFooterViewInfos : Ljava/util/ArrayList;
    //   72: astore_1
    //   73: aload_0
    //   74: aload_1
    //   75: invokespecial areAllListInfosSelectable : (Ljava/util/ArrayList;)Z
    //   78: ifeq -> 87
    //   81: iconst_1
    //   82: istore #4
    //   84: goto -> 90
    //   87: iconst_0
    //   88: istore #4
    //   90: aload_0
    //   91: iload #4
    //   93: putfield mAreAllFixedViewsSelectable : Z
    //   96: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #64	-> 0
    //   #65	-> 4
    //   #66	-> 9
    //   #68	-> 17
    //   #69	-> 21
    //   #71	-> 31
    //   #74	-> 36
    //   #75	-> 40
    //   #77	-> 50
    //   #80	-> 55
    //   #81	-> 60
    //   #82	-> 73
    //   #83	-> 96
  }
  
  public int getHeadersCount() {
    return this.mHeaderViewInfos.size();
  }
  
  public int getFootersCount() {
    return this.mFooterViewInfos.size();
  }
  
  public boolean isEmpty() {
    ListAdapter listAdapter = this.mAdapter;
    return (listAdapter == null || listAdapter.isEmpty());
  }
  
  private boolean areAllListInfosSelectable(ArrayList<ListView.FixedViewInfo> paramArrayList) {
    if (paramArrayList != null)
      for (ListView.FixedViewInfo fixedViewInfo : paramArrayList) {
        if (!fixedViewInfo.isSelectable)
          return false; 
      }  
    return true;
  }
  
  public boolean removeHeader(View paramView) {
    byte b = 0;
    while (true) {
      int i = this.mHeaderViewInfos.size();
      boolean bool = false;
      if (b < i) {
        ListView.FixedViewInfo fixedViewInfo = this.mHeaderViewInfos.get(b);
        if (fixedViewInfo.view == paramView) {
          this.mHeaderViewInfos.remove(b);
          ArrayList<ListView.FixedViewInfo> arrayList = this.mHeaderViewInfos;
          if (areAllListInfosSelectable(arrayList)) {
            arrayList = this.mFooterViewInfos;
            if (areAllListInfosSelectable(arrayList))
              bool = true; 
          } 
          this.mAreAllFixedViewsSelectable = bool;
          return true;
        } 
        b++;
        continue;
      } 
      break;
    } 
    return false;
  }
  
  public boolean removeFooter(View paramView) {
    byte b = 0;
    while (true) {
      int i = this.mFooterViewInfos.size();
      boolean bool = false;
      if (b < i) {
        ListView.FixedViewInfo fixedViewInfo = this.mFooterViewInfos.get(b);
        if (fixedViewInfo.view == paramView) {
          this.mFooterViewInfos.remove(b);
          ArrayList<ListView.FixedViewInfo> arrayList = this.mHeaderViewInfos;
          if (areAllListInfosSelectable(arrayList)) {
            arrayList = this.mFooterViewInfos;
            if (areAllListInfosSelectable(arrayList))
              bool = true; 
          } 
          this.mAreAllFixedViewsSelectable = bool;
          return true;
        } 
        b++;
        continue;
      } 
      break;
    } 
    return false;
  }
  
  public int getCount() {
    if (this.mAdapter != null)
      return getFootersCount() + getHeadersCount() + this.mAdapter.getCount(); 
    return getFootersCount() + getHeadersCount();
  }
  
  public boolean areAllItemsEnabled() {
    ListAdapter listAdapter = this.mAdapter;
    boolean bool = true;
    if (listAdapter != null) {
      if (!this.mAreAllFixedViewsSelectable || !listAdapter.areAllItemsEnabled())
        bool = false; 
      return bool;
    } 
    return true;
  }
  
  public boolean isEnabled(int paramInt) {
    int i = getHeadersCount();
    if (paramInt < i)
      return ((ListView.FixedViewInfo)this.mHeaderViewInfos.get(paramInt)).isSelectable; 
    int j = paramInt - i;
    paramInt = 0;
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null) {
      i = listAdapter.getCount();
      paramInt = i;
      if (j < i)
        return this.mAdapter.isEnabled(j); 
    } 
    if (j - paramInt >= this.mFooterViewInfos.size()) {
      Log.e("HeaderViewListAdapter", "throw an IndexOutOfBoundsException", new IndexOutOfBoundsException());
      if (this.mFooterViewInfos.size() > 0) {
        ArrayList<ListView.FixedViewInfo> arrayList = this.mFooterViewInfos;
        return ((ListView.FixedViewInfo)arrayList.get(arrayList.size() - 1)).isSelectable;
      } 
      return false;
    } 
    return ((ListView.FixedViewInfo)this.mFooterViewInfos.get(j - paramInt)).isSelectable;
  }
  
  public Object getItem(int paramInt) {
    int i = getHeadersCount();
    if (paramInt < i)
      return ((ListView.FixedViewInfo)this.mHeaderViewInfos.get(paramInt)).data; 
    int j = paramInt - i;
    paramInt = 0;
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null) {
      i = listAdapter.getCount();
      paramInt = i;
      if (j < i)
        return this.mAdapter.getItem(j); 
    } 
    if (j - paramInt >= this.mFooterViewInfos.size()) {
      Log.e("HeaderViewListAdapter", "throw an IndexOutOfBoundsException", new IndexOutOfBoundsException());
      if (this.mFooterViewInfos.size() > 0) {
        ArrayList<ListView.FixedViewInfo> arrayList = this.mFooterViewInfos;
        return ((ListView.FixedViewInfo)arrayList.get(arrayList.size() - 1)).data;
      } 
      return null;
    } 
    return ((ListView.FixedViewInfo)this.mFooterViewInfos.get(j - paramInt)).data;
  }
  
  public long getItemId(int paramInt) {
    int i = getHeadersCount();
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null && paramInt >= i) {
      paramInt -= i;
      i = listAdapter.getCount();
      if (paramInt < i)
        return this.mAdapter.getItemId(paramInt); 
    } 
    return -1L;
  }
  
  public boolean hasStableIds() {
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null)
      return listAdapter.hasStableIds(); 
    return false;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    int i = getHeadersCount();
    if (paramInt < i)
      return ((ListView.FixedViewInfo)this.mHeaderViewInfos.get(paramInt)).view; 
    int j = paramInt - i;
    paramInt = 0;
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null) {
      i = listAdapter.getCount();
      paramInt = i;
      if (j < i)
        return this.mAdapter.getView(j, paramView, paramViewGroup); 
    } 
    if (j - paramInt >= this.mFooterViewInfos.size()) {
      Log.e("HeaderViewListAdapter", "throw an IndexOutOfBoundsException", new IndexOutOfBoundsException());
      if (this.mFooterViewInfos.size() > 0) {
        ArrayList<ListView.FixedViewInfo> arrayList = this.mFooterViewInfos;
        return ((ListView.FixedViewInfo)arrayList.get(arrayList.size() - 1)).view;
      } 
      return null;
    } 
    return ((ListView.FixedViewInfo)this.mFooterViewInfos.get(j - paramInt)).view;
  }
  
  public int getItemViewType(int paramInt) {
    int i = getHeadersCount();
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null && paramInt >= i) {
      paramInt -= i;
      i = listAdapter.getCount();
      if (paramInt < i)
        return this.mAdapter.getItemViewType(paramInt); 
    } 
    return -2;
  }
  
  public int getViewTypeCount() {
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null)
      return listAdapter.getViewTypeCount(); 
    return 1;
  }
  
  public void registerDataSetObserver(DataSetObserver paramDataSetObserver) {
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null)
      listAdapter.registerDataSetObserver(paramDataSetObserver); 
  }
  
  public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver) {
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null)
      listAdapter.unregisterDataSetObserver(paramDataSetObserver); 
  }
  
  public Filter getFilter() {
    if (this.mIsFilterable)
      return ((Filterable)this.mAdapter).getFilter(); 
    return null;
  }
  
  public ListAdapter getWrappedAdapter() {
    return this.mAdapter;
  }
}
