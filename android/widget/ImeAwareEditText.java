package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;

public class ImeAwareEditText extends EditText {
  private boolean mHasPendingShowSoftInputRequest;
  
  final Runnable mRunShowSoftInputIfNecessary = new _$$Lambda$ImeAwareEditText$eLozNZ28l7LCrAM2viVrmqyIggA(this);
  
  public ImeAwareEditText(Context paramContext) {
    super(paramContext, (AttributeSet)null);
  }
  
  public ImeAwareEditText(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public ImeAwareEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public ImeAwareEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo) {
    InputConnection inputConnection = super.onCreateInputConnection(paramEditorInfo);
    if (this.mHasPendingShowSoftInputRequest) {
      removeCallbacks(this.mRunShowSoftInputIfNecessary);
      post(this.mRunShowSoftInputIfNecessary);
    } 
    return inputConnection;
  }
  
  private void showSoftInputIfNecessary() {
    if (this.mHasPendingShowSoftInputRequest) {
      InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
      inputMethodManager.showSoftInput(this, 0);
      this.mHasPendingShowSoftInputRequest = false;
    } 
  }
  
  public void scheduleShowSoftInput() {
    InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
    if (inputMethodManager.isActive(this)) {
      this.mHasPendingShowSoftInputRequest = false;
      removeCallbacks(this.mRunShowSoftInputIfNecessary);
      inputMethodManager.showSoftInput(this, 0);
      return;
    } 
    this.mHasPendingShowSoftInputRequest = true;
  }
}
