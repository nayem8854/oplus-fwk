package android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.SystemProperties;
import android.text.Layout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class OplusTextViewRTLUtilForUG implements IOplusTextViewRTLUtilForUG {
  private static volatile OplusTextViewRTLUtilForUG sInstance = null;
  
  public boolean hasInit;
  
  public boolean mForceAnyRtl;
  
  public boolean mForceViewStart;
  
  private Locale mLastUpdateLocale = Locale.ENGLISH;
  
  public boolean mSupportRtl;
  
  public static OplusTextViewRTLUtilForUG getInstance() {
    // Byte code:
    //   0: getstatic android/widget/OplusTextViewRTLUtilForUG.sInstance : Landroid/widget/OplusTextViewRTLUtilForUG;
    //   3: ifnonnull -> 39
    //   6: ldc android/widget/OplusTextViewRTLUtilForUG
    //   8: monitorenter
    //   9: getstatic android/widget/OplusTextViewRTLUtilForUG.sInstance : Landroid/widget/OplusTextViewRTLUtilForUG;
    //   12: ifnonnull -> 27
    //   15: new android/widget/OplusTextViewRTLUtilForUG
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/widget/OplusTextViewRTLUtilForUG.sInstance : Landroid/widget/OplusTextViewRTLUtilForUG;
    //   27: ldc android/widget/OplusTextViewRTLUtilForUG
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/widget/OplusTextViewRTLUtilForUG
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/widget/OplusTextViewRTLUtilForUG.sInstance : Landroid/widget/OplusTextViewRTLUtilForUG;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #44	-> 0
    //   #45	-> 6
    //   #46	-> 9
    //   #47	-> 15
    //   #49	-> 27
    //   #51	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  OplusTextViewRTLUtilForUG() {
    this.hasInit = false;
    this.mSupportRtl = true;
    this.mForceAnyRtl = false;
    this.mForceViewStart = false;
    this.mLastUpdateLocale = Locale.getDefault();
  }
  
  public void initRtlParameter(Resources paramResources) {
    if (!this.hasInit && paramResources != null && paramResources.getConfiguration() != null && (paramResources.getConfiguration()).locale != null) {
      String str1 = SystemProperties.get("persist.sys.locale", "zh_CN");
      String str2 = (paramResources.getConfiguration()).locale.toString();
      if (str1 != null && str1.equalsIgnoreCase("ug-CN")) {
        this.mForceAnyRtl = true;
        if (str2 != null && str2.equalsIgnoreCase("ug_CN")) {
          String[] arrayOfString = paramResources.getAssets().getNonSystemLocales();
          if (arrayOfString != null)
            if (arrayOfString.length > 0) {
              List<String> list = Arrays.asList(arrayOfString);
              if (list.contains("ug-CN")) {
                this.mForceViewStart = true;
              } else {
                this.mSupportRtl = false;
              } 
            } else {
              this.mSupportRtl = false;
            }  
        } 
      } 
      this.hasInit = true;
    } 
  }
  
  public boolean getOplusSupportRtl() {
    return this.mSupportRtl;
  }
  
  public boolean getDirectionAnyRtl() {
    return this.mForceAnyRtl;
  }
  
  public boolean getTextViewStart() {
    return this.mForceViewStart;
  }
  
  public Layout.Alignment getLayoutAlignmentForTextView(Layout.Alignment paramAlignment, Context paramContext, TextView paramTextView) {
    int i;
    boolean bool = getTextViewStart();
    switch (paramTextView.getTextAlignment()) {
      default:
        if (bool) {
          paramAlignment = Layout.Alignment.ALIGN_RIGHT;
          return paramAlignment;
        } 
        break;
      case 6:
        if (paramTextView.getLayoutDirection() == 1) {
          paramAlignment = Layout.Alignment.ALIGN_LEFT;
        } else {
          paramAlignment = Layout.Alignment.ALIGN_RIGHT;
        } 
        return paramAlignment;
      case 5:
        if (paramTextView.getLayoutDirection() == 1) {
          paramAlignment = Layout.Alignment.ALIGN_RIGHT;
        } else {
          paramAlignment = Layout.Alignment.ALIGN_LEFT;
        } 
        return paramAlignment;
      case 4:
        paramAlignment = Layout.Alignment.ALIGN_CENTER;
        return paramAlignment;
      case 3:
        paramAlignment = Layout.Alignment.ALIGN_OPPOSITE;
        return paramAlignment;
      case 2:
        paramAlignment = Layout.Alignment.ALIGN_NORMAL;
        return paramAlignment;
      case 1:
        i = paramTextView.getGravity() & 0x800007;
        if (i != 1) {
          if (i != 3) {
            if (i != 5) {
              if (i != 8388611) {
                if (i != 8388613) {
                  if (bool) {
                    paramAlignment = Layout.Alignment.ALIGN_RIGHT;
                  } else {
                    paramAlignment = Layout.Alignment.ALIGN_NORMAL;
                  } 
                } else if (bool) {
                  paramAlignment = Layout.Alignment.ALIGN_LEFT;
                } else {
                  paramAlignment = Layout.Alignment.ALIGN_OPPOSITE;
                } 
              } else if (bool) {
                paramAlignment = Layout.Alignment.ALIGN_RIGHT;
              } else {
                paramAlignment = Layout.Alignment.ALIGN_NORMAL;
              } 
            } else {
              paramAlignment = Layout.Alignment.ALIGN_RIGHT;
            } 
          } else {
            paramAlignment = Layout.Alignment.ALIGN_LEFT;
          } 
        } else {
          paramAlignment = Layout.Alignment.ALIGN_CENTER;
        } 
        return paramAlignment;
    } 
    paramAlignment = Layout.Alignment.ALIGN_NORMAL;
    return paramAlignment;
  }
  
  public TextDirectionHeuristic getTextDirectionHeuristicForTextView(boolean paramBoolean) {
    TextDirectionHeuristic textDirectionHeuristic;
    if (getDirectionAnyRtl()) {
      textDirectionHeuristic = TextDirectionHeuristics.ANYRTL_LTR;
    } else if (paramBoolean) {
      textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_RTL;
    } else {
      textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
    } 
    return textDirectionHeuristic;
  }
  
  public boolean hasRtlSupportForView(Context paramContext) {
    boolean bool;
    if (getOplusSupportRtl() && paramContext.getApplicationInfo().hasRtlSupport()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void updateRtlParameterForUG(Resources paramResources, Configuration paramConfiguration) {
    if (paramResources != null && paramResources.getAssets() != null)
      updateRtlParameterForUG(paramResources.getAssets().getNonSystemLocales(), paramConfiguration); 
  }
  
  public void updateRtlParameterForUG(String[] paramArrayOfString, Configuration paramConfiguration) {
    if (this.hasInit && paramArrayOfString != null && paramConfiguration != null && paramConfiguration.locale != null && !paramConfiguration.locale.equals(this.mLastUpdateLocale)) {
      this.mForceAnyRtl = false;
      this.mForceViewStart = false;
      this.mSupportRtl = true;
      String str = paramConfiguration.locale.toLanguageTag();
      if (str != null && str.equalsIgnoreCase("ug-CN")) {
        this.mForceAnyRtl = true;
        if (paramArrayOfString != null)
          if (paramArrayOfString.length > 0) {
            List<String> list = Arrays.asList(paramArrayOfString);
            if (list.contains("ug-CN")) {
              this.mForceViewStart = true;
            } else {
              this.mSupportRtl = false;
            } 
          } else {
            this.mSupportRtl = false;
          }  
      } 
      this.mLastUpdateLocale = paramConfiguration.locale;
      this.hasInit = true;
    } 
  }
}
