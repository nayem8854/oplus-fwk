package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.R;

@Deprecated
public class TabWidget extends LinearLayout implements View.OnFocusChangeListener {
  private final Rect mBounds = new Rect();
  
  private int mSelectedTab = -1;
  
  private boolean mDrawBottomStrips = true;
  
  private int mImposedTabsHeight = -1;
  
  private int[] mImposedTabWidths;
  
  private Drawable mLeftStrip;
  
  private Drawable mRightStrip;
  
  private OnTabSelectionChanged mSelectionChangedListener;
  
  private boolean mStripMoved;
  
  public TabWidget(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public TabWidget(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842883);
  }
  
  public TabWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TabWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TabWidget, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.TabWidget, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mDrawBottomStrips = typedArray.getBoolean(3, this.mDrawBottomStrips);
    if ((paramContext.getApplicationInfo()).targetSdkVersion <= 4) {
      paramInt1 = 1;
    } else {
      paramInt1 = 0;
    } 
    boolean bool = typedArray.hasValueOrEmpty(1);
    if (bool) {
      this.mLeftStrip = typedArray.getDrawable(1);
    } else if (paramInt1 != 0) {
      this.mLeftStrip = paramContext.getDrawable(17303700);
    } else {
      this.mLeftStrip = paramContext.getDrawable(17303699);
    } 
    bool = typedArray.hasValueOrEmpty(2);
    if (bool) {
      this.mRightStrip = typedArray.getDrawable(2);
    } else if (paramInt1 != 0) {
      this.mRightStrip = paramContext.getDrawable(17303702);
    } else {
      this.mRightStrip = paramContext.getDrawable(17303701);
    } 
    typedArray.recycle();
    setChildrenDrawingOrderEnabled(true);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mStripMoved = true;
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2) {
    int i = this.mSelectedTab;
    if (i == -1)
      return paramInt2; 
    if (paramInt2 == paramInt1 - 1)
      return i; 
    if (paramInt2 >= i)
      return paramInt2 + 1; 
    return paramInt2;
  }
  
  void measureChildBeforeLayout(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    int i = paramInt2, j = paramInt4;
    if (!isMeasureWithLargestChildEnabled()) {
      i = paramInt2;
      j = paramInt4;
      if (this.mImposedTabsHeight >= 0) {
        i = View.MeasureSpec.makeMeasureSpec(this.mImposedTabWidths[paramInt1] + paramInt3, 1073741824);
        j = View.MeasureSpec.makeMeasureSpec(this.mImposedTabsHeight, 1073741824);
      } 
    } 
    super.measureChildBeforeLayout(paramView, paramInt1, i, paramInt3, j, paramInt5);
  }
  
  void measureHorizontal(int paramInt1, int paramInt2) {
    if (View.MeasureSpec.getMode(paramInt1) == 0) {
      super.measureHorizontal(paramInt1, paramInt2);
      return;
    } 
    int i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.makeSafeMeasureSpec(i, 0);
    this.mImposedTabsHeight = -1;
    super.measureHorizontal(j, paramInt2);
    int k = getMeasuredWidth() - i;
    if (k > 0) {
      int m = getChildCount();
      j = 0;
      for (i = 0; i < m; i++) {
        View view = getChildAt(i);
        if (view.getVisibility() != 8)
          j++; 
      } 
      if (j > 0) {
        int[] arrayOfInt = this.mImposedTabWidths;
        if (arrayOfInt == null || arrayOfInt.length != m)
          this.mImposedTabWidths = new int[m]; 
        for (i = 0; i < m; i++) {
          View view = getChildAt(i);
          if (view.getVisibility() != 8) {
            int n = view.getMeasuredWidth();
            int i1 = k / j;
            i1 = Math.max(0, n - i1);
            this.mImposedTabWidths[i] = i1;
            k -= n - i1;
            j--;
            this.mImposedTabsHeight = Math.max(this.mImposedTabsHeight, view.getMeasuredHeight());
          } 
        } 
      } 
    } 
    super.measureHorizontal(paramInt1, paramInt2);
  }
  
  public View getChildTabViewAt(int paramInt) {
    return getChildAt(paramInt);
  }
  
  public int getTabCount() {
    return getChildCount();
  }
  
  public void setDividerDrawable(Drawable paramDrawable) {
    super.setDividerDrawable(paramDrawable);
  }
  
  public void setDividerDrawable(int paramInt) {
    setDividerDrawable(this.mContext.getDrawable(paramInt));
  }
  
  public void setLeftStripDrawable(Drawable paramDrawable) {
    this.mLeftStrip = paramDrawable;
    requestLayout();
    invalidate();
  }
  
  public void setLeftStripDrawable(int paramInt) {
    setLeftStripDrawable(this.mContext.getDrawable(paramInt));
  }
  
  public Drawable getLeftStripDrawable() {
    return this.mLeftStrip;
  }
  
  public void setRightStripDrawable(Drawable paramDrawable) {
    this.mRightStrip = paramDrawable;
    requestLayout();
    invalidate();
  }
  
  public void setRightStripDrawable(int paramInt) {
    setRightStripDrawable(this.mContext.getDrawable(paramInt));
  }
  
  public Drawable getRightStripDrawable() {
    return this.mRightStrip;
  }
  
  public void setStripEnabled(boolean paramBoolean) {
    this.mDrawBottomStrips = paramBoolean;
    invalidate();
  }
  
  public boolean isStripEnabled() {
    return this.mDrawBottomStrips;
  }
  
  public void childDrawableStateChanged(View paramView) {
    if (getTabCount() > 0 && paramView == getChildTabViewAt(this.mSelectedTab))
      invalidate(); 
    super.childDrawableStateChanged(paramView);
  }
  
  public void dispatchDraw(Canvas paramCanvas) {
    super.dispatchDraw(paramCanvas);
    if (getTabCount() == 0)
      return; 
    if (!this.mDrawBottomStrips)
      return; 
    View view = getChildTabViewAt(this.mSelectedTab);
    Drawable drawable1 = this.mLeftStrip;
    Drawable drawable2 = this.mRightStrip;
    if (drawable1 != null)
      drawable1.setState(view.getDrawableState()); 
    if (drawable2 != null)
      drawable2.setState(view.getDrawableState()); 
    if (this.mStripMoved) {
      Rect rect = this.mBounds;
      rect.left = view.getLeft();
      rect.right = view.getRight();
      int i = getHeight();
      if (drawable1 != null) {
        int j = Math.min(0, rect.left - drawable1.getIntrinsicWidth());
        int k = drawable1.getIntrinsicHeight(), m = rect.left;
        drawable1.setBounds(j, i - k, m, i);
      } 
      if (drawable2 != null) {
        int j = rect.right, m = drawable2.getIntrinsicHeight();
        int k = Math.max(getWidth(), rect.right + drawable2.getIntrinsicWidth());
        drawable2.setBounds(j, i - m, k, i);
      } 
      this.mStripMoved = false;
    } 
    if (drawable1 != null)
      drawable1.draw(paramCanvas); 
    if (drawable2 != null)
      drawable2.draw(paramCanvas); 
  }
  
  public void setCurrentTab(int paramInt) {
    if (paramInt >= 0 && paramInt < getTabCount()) {
      int i = this.mSelectedTab;
      if (paramInt != i) {
        if (i != -1)
          getChildTabViewAt(i).setSelected(false); 
        this.mSelectedTab = paramInt;
        getChildTabViewAt(paramInt).setSelected(true);
        this.mStripMoved = true;
        return;
      } 
    } 
  }
  
  public CharSequence getAccessibilityClassName() {
    return TabWidget.class.getName();
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    paramAccessibilityEvent.setItemCount(getTabCount());
    paramAccessibilityEvent.setCurrentItemIndex(this.mSelectedTab);
  }
  
  public void focusCurrentTab(int paramInt) {
    int i = this.mSelectedTab;
    setCurrentTab(paramInt);
    if (i != paramInt)
      getChildTabViewAt(paramInt).requestFocus(); 
  }
  
  public void setEnabled(boolean paramBoolean) {
    super.setEnabled(paramBoolean);
    int i = getTabCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildTabViewAt(b);
      view.setEnabled(paramBoolean);
    } 
  }
  
  public void addView(View paramView) {
    if (paramView.getLayoutParams() == null) {
      LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, -1, 1.0F);
      layoutParams.setMargins(0, 0, 0, 0);
      paramView.setLayoutParams(layoutParams);
    } 
    paramView.setFocusable(true);
    paramView.setClickable(true);
    if (paramView.getPointerIcon() == null)
      paramView.setPointerIcon(PointerIcon.getSystemIcon(getContext(), 1002)); 
    super.addView(paramView);
    paramView.setOnClickListener(new TabClickListener(getTabCount() - 1));
  }
  
  public void removeAllViews() {
    super.removeAllViews();
    this.mSelectedTab = -1;
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    if (!isEnabled())
      return null; 
    return super.onResolvePointerIcon(paramMotionEvent, paramInt);
  }
  
  void setTabSelectionListener(OnTabSelectionChanged paramOnTabSelectionChanged) {
    this.mSelectionChangedListener = paramOnTabSelectionChanged;
  }
  
  public void onFocusChange(View paramView, boolean paramBoolean) {}
  
  class OnTabSelectionChanged {
    public abstract void onTabSelectionChanged(int param1Int, boolean param1Boolean);
  }
  
  class TabClickListener implements View.OnClickListener {
    private final int mTabIndex;
    
    final TabWidget this$0;
    
    private TabClickListener(int param1Int) {
      this.mTabIndex = param1Int;
    }
    
    public void onClick(View param1View) {
      TabWidget.this.mSelectionChangedListener.onTabSelectionChanged(this.mTabIndex, true);
    }
  }
}
