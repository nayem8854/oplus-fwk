package android.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.android.internal.widget.ScrollBarUtils;
import com.oplus.view.IOplusScrollBarEffect;
import com.oplus.view.OplusScrollBarEffect;

public class ScrollBarDrawable extends Drawable implements Drawable.Callback {
  private int mAlpha = 255;
  
  private boolean mAlwaysDrawHorizontalTrack;
  
  private boolean mAlwaysDrawVerticalTrack;
  
  private boolean mBoundsChanged;
  
  private ColorFilter mColorFilter;
  
  private final IOplusScrollBarEffect mEffect;
  
  private int mExtent;
  
  private boolean mHasSetAlpha;
  
  private boolean mHasSetColorFilter;
  
  private Drawable mHorizontalThumb;
  
  private Drawable mHorizontalTrack;
  
  private boolean mMutated;
  
  private int mOffset;
  
  private int mRange;
  
  private boolean mRangeChanged;
  
  private boolean mVertical;
  
  private Drawable mVerticalThumb;
  
  private Drawable mVerticalTrack;
  
  public ScrollBarDrawable() {
    this(OplusScrollBarEffect.NO_EFFECT);
  }
  
  public void setAlwaysDrawHorizontalTrack(boolean paramBoolean) {
    this.mAlwaysDrawHorizontalTrack = paramBoolean;
  }
  
  public void setAlwaysDrawVerticalTrack(boolean paramBoolean) {
    this.mAlwaysDrawVerticalTrack = paramBoolean;
  }
  
  public boolean getAlwaysDrawVerticalTrack() {
    return this.mAlwaysDrawVerticalTrack;
  }
  
  public boolean getAlwaysDrawHorizontalTrack() {
    return this.mAlwaysDrawHorizontalTrack;
  }
  
  public void setParameters(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    if (this.mVertical != paramBoolean) {
      this.mVertical = paramBoolean;
      this.mBoundsChanged = true;
    } 
    if (this.mRange != paramInt1 || this.mOffset != paramInt2 || this.mExtent != paramInt3) {
      this.mRange = paramInt1;
      this.mOffset = paramInt2;
      this.mExtent = paramInt3;
      this.mRangeChanged = true;
    } 
  }
  
  public void draw(Canvas paramCanvas) {
    boolean bool1;
    int k;
    boolean bool = this.mVertical;
    int i = this.mExtent;
    int j = this.mRange;
    if (i <= 0 || j <= i) {
      if (bool) {
        bool1 = this.mAlwaysDrawVerticalTrack;
      } else {
        bool1 = this.mAlwaysDrawHorizontalTrack;
      } 
      k = 0;
    } else {
      bool1 = true;
      k = 1;
    } 
    Rect rect = getBounds();
    if (paramCanvas.quickReject(rect.left, rect.top, rect.right, rect.bottom))
      return; 
    this.mEffect.getDrawRect(rect);
    if (bool1)
      drawTrack(paramCanvas, rect, bool); 
    if (k) {
      if (bool) {
        k = rect.height();
      } else {
        k = rect.width();
      } 
      if (bool) {
        m = rect.width();
      } else {
        m = rect.height();
      } 
      int m = this.mEffect.getThumbLength(k, m, i, j);
      int n = this.mOffset;
      k = ScrollBarUtils.getThumbOffset(k, m, i, j, n);
      drawThumb(paramCanvas, rect, k, m, bool);
    } 
  }
  
  protected void onBoundsChange(Rect paramRect) {
    super.onBoundsChange(paramRect);
    this.mBoundsChanged = true;
  }
  
  public boolean isStateful() {
    Drawable drawable = this.mVerticalTrack;
    if (drawable == null || !drawable.isStateful()) {
      drawable = this.mVerticalThumb;
      if (drawable == null || 
        !drawable.isStateful()) {
        drawable = this.mHorizontalTrack;
        if (drawable == null || 
          !drawable.isStateful()) {
          drawable = this.mHorizontalThumb;
          return ((drawable != null && 
            drawable.isStateful()) || 
            super.isStateful());
        } 
      } 
    } 
    return true;
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    boolean bool1 = super.onStateChange(paramArrayOfint);
    Drawable drawable = this.mVerticalTrack;
    boolean bool2 = bool1;
    if (drawable != null)
      bool2 = bool1 | drawable.setState(paramArrayOfint); 
    drawable = this.mVerticalThumb;
    bool1 = bool2;
    if (drawable != null)
      bool1 = bool2 | drawable.setState(paramArrayOfint); 
    drawable = this.mHorizontalTrack;
    bool2 = bool1;
    if (drawable != null)
      bool2 = bool1 | drawable.setState(paramArrayOfint); 
    drawable = this.mHorizontalThumb;
    bool1 = bool2;
    if (drawable != null)
      bool1 = bool2 | drawable.setState(paramArrayOfint); 
    return bool1;
  }
  
  private void drawTrack(Canvas paramCanvas, Rect paramRect, boolean paramBoolean) {
    Drawable drawable;
    if (paramBoolean) {
      drawable = this.mVerticalTrack;
    } else {
      drawable = this.mHorizontalTrack;
    } 
    if (drawable != null) {
      if (this.mBoundsChanged)
        drawable.setBounds(paramRect); 
      drawable.draw(paramCanvas);
    } 
  }
  
  private void drawThumb(Canvas paramCanvas, Rect paramRect, int paramInt1, int paramInt2, boolean paramBoolean) {
    boolean bool;
    if (this.mRangeChanged || this.mBoundsChanged) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean) {
      if (this.mVerticalThumb != null) {
        Drawable drawable = this.mVerticalThumb;
        if (bool)
          drawable.setBounds(paramRect.left, paramRect.top + paramInt1, paramRect.right, paramRect.top + paramInt1 + paramInt2); 
        drawable.draw(paramCanvas);
      } 
    } else if (this.mHorizontalThumb != null) {
      Drawable drawable = this.mHorizontalThumb;
      if (bool)
        drawable.setBounds(paramRect.left + paramInt1, paramRect.top, paramRect.left + paramInt1 + paramInt2, paramRect.bottom); 
      drawable.draw(paramCanvas);
    } 
  }
  
  public void setVerticalThumbDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mVerticalThumb;
    if (drawable != null)
      drawable.setCallback(null); 
    propagateCurrentState(paramDrawable);
    this.mVerticalThumb = paramDrawable;
  }
  
  public Drawable getVerticalTrackDrawable() {
    return this.mVerticalTrack;
  }
  
  public Drawable getVerticalThumbDrawable() {
    return this.mVerticalThumb;
  }
  
  public Drawable getHorizontalTrackDrawable() {
    return this.mHorizontalTrack;
  }
  
  public Drawable getHorizontalThumbDrawable() {
    return this.mHorizontalThumb;
  }
  
  public void setVerticalTrackDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mVerticalTrack;
    if (drawable != null)
      drawable.setCallback(null); 
    propagateCurrentState(paramDrawable);
    this.mVerticalTrack = paramDrawable;
  }
  
  public void setHorizontalThumbDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mHorizontalThumb;
    if (drawable != null)
      drawable.setCallback(null); 
    propagateCurrentState(paramDrawable);
    this.mHorizontalThumb = paramDrawable;
  }
  
  public void setHorizontalTrackDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mHorizontalTrack;
    if (drawable != null)
      drawable.setCallback(null); 
    propagateCurrentState(paramDrawable);
    this.mHorizontalTrack = paramDrawable;
  }
  
  private void propagateCurrentState(Drawable paramDrawable) {
    if (paramDrawable != null) {
      if (this.mMutated)
        paramDrawable.mutate(); 
      paramDrawable.setState(getState());
      paramDrawable.setCallback(this);
      if (this.mHasSetAlpha)
        paramDrawable.setAlpha(this.mAlpha); 
      if (this.mHasSetColorFilter)
        paramDrawable.setColorFilter(this.mColorFilter); 
    } 
  }
  
  public int getSize(boolean paramBoolean) {
    int i = 0;
    boolean bool = false;
    if (paramBoolean) {
      Drawable drawable1 = this.mVerticalTrack;
      if (drawable1 != null) {
        i = drawable1.getIntrinsicWidth();
      } else {
        drawable1 = this.mVerticalThumb;
        i = bool;
        if (drawable1 != null)
          i = drawable1.getIntrinsicWidth(); 
      } 
      return i;
    } 
    Drawable drawable = this.mHorizontalTrack;
    if (drawable != null) {
      i = drawable.getIntrinsicHeight();
    } else {
      drawable = this.mHorizontalThumb;
      if (drawable != null)
        i = drawable.getIntrinsicHeight(); 
    } 
    return i;
  }
  
  public ScrollBarDrawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      Drawable drawable = this.mVerticalTrack;
      if (drawable != null)
        drawable.mutate(); 
      drawable = this.mVerticalThumb;
      if (drawable != null)
        drawable.mutate(); 
      drawable = this.mHorizontalTrack;
      if (drawable != null)
        drawable.mutate(); 
      drawable = this.mHorizontalThumb;
      if (drawable != null)
        drawable.mutate(); 
      this.mMutated = true;
    } 
    return this;
  }
  
  public void setAlpha(int paramInt) {
    this.mAlpha = paramInt;
    this.mHasSetAlpha = true;
    Drawable drawable = this.mVerticalTrack;
    if (drawable != null)
      drawable.setAlpha(paramInt); 
    drawable = this.mVerticalThumb;
    if (drawable != null)
      drawable.setAlpha(paramInt); 
    drawable = this.mHorizontalTrack;
    if (drawable != null)
      drawable.setAlpha(paramInt); 
    drawable = this.mHorizontalThumb;
    if (drawable != null)
      drawable.setAlpha(paramInt); 
  }
  
  public int getAlpha() {
    return this.mAlpha;
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    this.mColorFilter = paramColorFilter;
    this.mHasSetColorFilter = true;
    Drawable drawable = this.mVerticalTrack;
    if (drawable != null)
      drawable.setColorFilter(paramColorFilter); 
    drawable = this.mVerticalThumb;
    if (drawable != null)
      drawable.setColorFilter(paramColorFilter); 
    drawable = this.mHorizontalTrack;
    if (drawable != null)
      drawable.setColorFilter(paramColorFilter); 
    drawable = this.mHorizontalThumb;
    if (drawable != null)
      drawable.setColorFilter(paramColorFilter); 
  }
  
  public ColorFilter getColorFilter() {
    return this.mColorFilter;
  }
  
  public int getOpacity() {
    return -3;
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    invalidateSelf();
  }
  
  public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong) {
    scheduleSelf(paramRunnable, paramLong);
  }
  
  public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable) {
    unscheduleSelf(paramRunnable);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ScrollBarDrawable: range=");
    stringBuilder.append(this.mRange);
    stringBuilder.append(" offset=");
    stringBuilder.append(this.mOffset);
    stringBuilder.append(" extent=");
    stringBuilder.append(this.mExtent);
    if (this.mVertical) {
      null = " V";
    } else {
      null = " H";
    } 
    stringBuilder.append(null);
    return stringBuilder.toString();
  }
  
  public ScrollBarDrawable(IOplusScrollBarEffect paramIOplusScrollBarEffect) {
    this.mEffect = paramIOplusScrollBarEffect;
  }
}
