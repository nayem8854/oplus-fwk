package android.widget;

import android.view.MotionEvent;
import android.view.ViewConfiguration;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class EditorTouchState {
  private boolean mIsDragCloseToVertical;
  
  private boolean mIsOnHandle;
  
  private long mLastDownMillis;
  
  private float mLastDownX;
  
  private float mLastDownY;
  
  private long mLastUpMillis;
  
  private float mLastUpX;
  
  private float mLastUpY;
  
  private boolean mMovedEnoughForDrag;
  
  private boolean mMultiTapInSameArea;
  
  private int mMultiTapStatus = 0;
  
  public float getLastDownX() {
    return this.mLastDownX;
  }
  
  public float getLastDownY() {
    return this.mLastDownY;
  }
  
  public float getLastUpX() {
    return this.mLastUpX;
  }
  
  public float getLastUpY() {
    return this.mLastUpY;
  }
  
  public boolean isDoubleTap() {
    boolean bool;
    if (this.mMultiTapStatus == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTripleClick() {
    boolean bool;
    if (this.mMultiTapStatus == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isMultiTap() {
    int i = this.mMultiTapStatus;
    return (i == 2 || i == 3);
  }
  
  public boolean isMultiTapInSameArea() {
    boolean bool;
    if (isMultiTap() && this.mMultiTapInSameArea) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isMovedEnoughForDrag() {
    return this.mMovedEnoughForDrag;
  }
  
  public boolean isDragCloseToVertical() {
    boolean bool;
    if (this.mIsDragCloseToVertical && !this.mIsOnHandle) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setIsOnHandle(boolean paramBoolean) {
    this.mIsOnHandle = paramBoolean;
  }
  
  public boolean isOnHandle() {
    return this.mIsOnHandle;
  }
  
  public void update(MotionEvent paramMotionEvent, ViewConfiguration paramViewConfiguration) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getActionMasked : ()I
    //   4: istore_3
    //   5: iconst_1
    //   6: istore #4
    //   8: iload_3
    //   9: ifne -> 201
    //   12: aload_1
    //   13: sipush #8194
    //   16: invokevirtual isFromSource : (I)Z
    //   19: istore #5
    //   21: aload_1
    //   22: invokevirtual getEventTime : ()J
    //   25: lstore #6
    //   27: aload_0
    //   28: getfield mLastUpMillis : J
    //   31: lstore #8
    //   33: aload_0
    //   34: getfield mLastDownMillis : J
    //   37: lstore #10
    //   39: lload #6
    //   41: lload #8
    //   43: lsub
    //   44: invokestatic getDoubleTapTimeout : ()I
    //   47: i2l
    //   48: lcmp
    //   49: ifgt -> 154
    //   52: lload #8
    //   54: lload #10
    //   56: lsub
    //   57: invokestatic getDoubleTapTimeout : ()I
    //   60: i2l
    //   61: lcmp
    //   62: ifgt -> 154
    //   65: aload_0
    //   66: getfield mMultiTapStatus : I
    //   69: istore_3
    //   70: iload_3
    //   71: iconst_1
    //   72: if_icmpeq -> 85
    //   75: iload_3
    //   76: iconst_2
    //   77: if_icmpne -> 154
    //   80: iload #5
    //   82: ifeq -> 154
    //   85: aload_0
    //   86: getfield mMultiTapStatus : I
    //   89: iconst_1
    //   90: if_icmpne -> 101
    //   93: aload_0
    //   94: iconst_2
    //   95: putfield mMultiTapStatus : I
    //   98: goto -> 106
    //   101: aload_0
    //   102: iconst_3
    //   103: putfield mMultiTapStatus : I
    //   106: aload_0
    //   107: getfield mLastDownX : F
    //   110: fstore #12
    //   112: aload_0
    //   113: getfield mLastDownY : F
    //   116: fstore #13
    //   118: aload_1
    //   119: invokevirtual getX : ()F
    //   122: fstore #14
    //   124: aload_1
    //   125: invokevirtual getY : ()F
    //   128: fstore #15
    //   130: aload_2
    //   131: invokevirtual getScaledDoubleTapSlop : ()I
    //   134: istore_3
    //   135: aload_0
    //   136: fload #12
    //   138: fload #13
    //   140: fload #14
    //   142: fload #15
    //   144: iload_3
    //   145: invokestatic isDistanceWithin : (FFFFI)Z
    //   148: putfield mMultiTapInSameArea : Z
    //   151: goto -> 164
    //   154: aload_0
    //   155: iconst_1
    //   156: putfield mMultiTapStatus : I
    //   159: aload_0
    //   160: iconst_0
    //   161: putfield mMultiTapInSameArea : Z
    //   164: aload_0
    //   165: aload_1
    //   166: invokevirtual getX : ()F
    //   169: putfield mLastDownX : F
    //   172: aload_0
    //   173: aload_1
    //   174: invokevirtual getY : ()F
    //   177: putfield mLastDownY : F
    //   180: aload_0
    //   181: aload_1
    //   182: invokevirtual getEventTime : ()J
    //   185: putfield mLastDownMillis : J
    //   188: aload_0
    //   189: iconst_0
    //   190: putfield mMovedEnoughForDrag : Z
    //   193: aload_0
    //   194: iconst_0
    //   195: putfield mIsDragCloseToVertical : Z
    //   198: goto -> 389
    //   201: iload_3
    //   202: iconst_1
    //   203: if_icmpne -> 243
    //   206: aload_0
    //   207: aload_1
    //   208: invokevirtual getX : ()F
    //   211: putfield mLastUpX : F
    //   214: aload_0
    //   215: aload_1
    //   216: invokevirtual getY : ()F
    //   219: putfield mLastUpY : F
    //   222: aload_0
    //   223: aload_1
    //   224: invokevirtual getEventTime : ()J
    //   227: putfield mLastUpMillis : J
    //   230: aload_0
    //   231: iconst_0
    //   232: putfield mMovedEnoughForDrag : Z
    //   235: aload_0
    //   236: iconst_0
    //   237: putfield mIsDragCloseToVertical : Z
    //   240: goto -> 389
    //   243: iload_3
    //   244: iconst_2
    //   245: if_icmpne -> 354
    //   248: aload_0
    //   249: getfield mMovedEnoughForDrag : Z
    //   252: ifne -> 389
    //   255: aload_1
    //   256: invokevirtual getX : ()F
    //   259: aload_0
    //   260: getfield mLastDownX : F
    //   263: fsub
    //   264: fstore #12
    //   266: aload_1
    //   267: invokevirtual getY : ()F
    //   270: aload_0
    //   271: getfield mLastDownY : F
    //   274: fsub
    //   275: fstore #15
    //   277: aload_2
    //   278: invokevirtual getScaledTouchSlop : ()I
    //   281: istore_3
    //   282: fload #15
    //   284: fload #15
    //   286: fmul
    //   287: fload #12
    //   289: fload #12
    //   291: fmul
    //   292: fadd
    //   293: iload_3
    //   294: iload_3
    //   295: imul
    //   296: i2f
    //   297: fcmpl
    //   298: ifle -> 307
    //   301: iconst_1
    //   302: istore #5
    //   304: goto -> 310
    //   307: iconst_0
    //   308: istore #5
    //   310: aload_0
    //   311: iload #5
    //   313: putfield mMovedEnoughForDrag : Z
    //   316: iload #5
    //   318: ifeq -> 351
    //   321: fload #12
    //   323: invokestatic abs : (F)F
    //   326: fload #15
    //   328: invokestatic abs : (F)F
    //   331: fcmpg
    //   332: ifgt -> 342
    //   335: iload #4
    //   337: istore #5
    //   339: goto -> 345
    //   342: iconst_0
    //   343: istore #5
    //   345: aload_0
    //   346: iload #5
    //   348: putfield mIsDragCloseToVertical : Z
    //   351: goto -> 389
    //   354: iload_3
    //   355: iconst_3
    //   356: if_icmpne -> 389
    //   359: aload_0
    //   360: lconst_0
    //   361: putfield mLastDownMillis : J
    //   364: aload_0
    //   365: lconst_0
    //   366: putfield mLastUpMillis : J
    //   369: aload_0
    //   370: iconst_0
    //   371: putfield mMultiTapStatus : I
    //   374: aload_0
    //   375: iconst_0
    //   376: putfield mMultiTapInSameArea : Z
    //   379: aload_0
    //   380: iconst_0
    //   381: putfield mMovedEnoughForDrag : Z
    //   384: aload_0
    //   385: iconst_0
    //   386: putfield mIsDragCloseToVertical : Z
    //   389: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #117	-> 0
    //   #118	-> 5
    //   #119	-> 12
    //   #126	-> 21
    //   #127	-> 33
    //   #130	-> 39
    //   #131	-> 52
    //   #134	-> 85
    //   #135	-> 93
    //   #137	-> 101
    //   #139	-> 106
    //   #140	-> 118
    //   #139	-> 135
    //   #148	-> 154
    //   #149	-> 159
    //   #154	-> 164
    //   #155	-> 172
    //   #156	-> 180
    //   #157	-> 188
    //   #158	-> 193
    //   #159	-> 198
    //   #163	-> 206
    //   #164	-> 214
    //   #165	-> 222
    //   #166	-> 230
    //   #167	-> 235
    //   #168	-> 243
    //   #169	-> 248
    //   #170	-> 255
    //   #171	-> 266
    //   #172	-> 277
    //   #173	-> 277
    //   #174	-> 277
    //   #175	-> 282
    //   #176	-> 316
    //   #179	-> 321
    //   #181	-> 351
    //   #182	-> 354
    //   #183	-> 359
    //   #184	-> 364
    //   #185	-> 369
    //   #186	-> 374
    //   #187	-> 379
    //   #188	-> 384
    //   #190	-> 389
  }
  
  public static boolean isDistanceWithin(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt) {
    boolean bool;
    paramFloat1 = paramFloat3 - paramFloat1;
    paramFloat2 = paramFloat4 - paramFloat2;
    if (paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2 <= (paramInt * paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface MultiTapStatus {
    public static final int DOUBLE_TAP = 2;
    
    public static final int FIRST_TAP = 1;
    
    public static final int NONE = 0;
    
    public static final int TRIPLE_CLICK = 3;
  }
}
