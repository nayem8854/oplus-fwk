package android.widget.inline;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.SurfaceControl;
import android.view.SurfaceControlViewHost;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;
import java.util.function.Consumer;

public class InlineContentView extends ViewGroup {
  private final SurfaceView mSurfaceView;
  
  private SurfacePackageUpdater mSurfacePackageUpdater;
  
  private SurfaceControlCallback mSurfaceControlCallback;
  
  private final SurfaceHolder.Callback mSurfaceCallback = (SurfaceHolder.Callback)new Object(this);
  
  private WeakReference<SurfaceView> mParentSurfaceOwnerView;
  
  private PointF mParentScale;
  
  private int[] mParentPosition;
  
  private final SurfaceControl.OnReparentListener mOnReparentListener = (SurfaceControl.OnReparentListener)new Object(this);
  
  private final ViewTreeObserver.OnDrawListener mOnDrawListener = (ViewTreeObserver.OnDrawListener)new Object(this);
  
  private static final String TAG = "InlineContentView";
  
  private static final boolean DEBUG = false;
  
  public InlineContentView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public InlineContentView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public InlineContentView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
    this.mSurfaceView.setEnableSurfaceClipping(true);
  }
  
  public SurfaceControl getSurfaceControl() {
    return this.mSurfaceView.getSurfaceControl();
  }
  
  public void setClipBounds(Rect paramRect) {
    super.setClipBounds(paramRect);
    this.mSurfaceView.setClipBounds(paramRect);
  }
  
  public InlineContentView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    SurfaceView surfaceView = new SurfaceView(paramContext, paramAttributeSet, paramInt1, paramInt2) {
        final InlineContentView this$0;
        
        protected void onSetSurfacePositionAndScaleRT(SurfaceControl.Transaction param1Transaction, SurfaceControl param1SurfaceControl, int param1Int1, int param1Int2, float param1Float1, float param1Float2) {
          int i = param1Int1, j = param1Int2;
          if (InlineContentView.this.mParentPosition != null) {
            i = (int)((param1Int1 - InlineContentView.this.mParentPosition[0]) / InlineContentView.this.mParentScale.x);
            j = (int)((param1Int2 - InlineContentView.this.mParentPosition[1]) / InlineContentView.this.mParentScale.y);
          } 
          param1Float1 = InlineContentView.this.getScaleX();
          param1Float2 = InlineContentView.this.getScaleY();
          super.onSetSurfacePositionAndScaleRT(param1Transaction, param1SurfaceControl, i, j, param1Float1, param1Float2);
        }
      };
    surfaceView.setZOrderOnTop(true);
    this.mSurfaceView.getHolder().setFormat(-2);
    addView(this.mSurfaceView);
    setImportantForAccessibility(2);
  }
  
  public void setChildSurfacePackageUpdater(SurfacePackageUpdater paramSurfacePackageUpdater) {
    this.mSurfacePackageUpdater = paramSurfacePackageUpdater;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    SurfacePackageUpdater surfacePackageUpdater = this.mSurfacePackageUpdater;
    if (surfacePackageUpdater != null)
      surfacePackageUpdater.getSurfacePackage(new _$$Lambda$InlineContentView$Jo1eoRaZ6vcvcDkOB4jkMXqcJkE(this)); 
    this.mSurfaceView.setVisibility(getVisibility());
    getViewTreeObserver().addOnDrawListener(this.mOnDrawListener);
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    SurfacePackageUpdater surfacePackageUpdater = this.mSurfacePackageUpdater;
    if (surfacePackageUpdater != null)
      surfacePackageUpdater.onSurfacePackageReleased(); 
    getViewTreeObserver().removeOnDrawListener(this.mOnDrawListener);
    this.mSurfaceView.setVisibility(8);
  }
  
  public void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mSurfaceView.layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
  }
  
  public void setSurfaceControlCallback(SurfaceControlCallback paramSurfaceControlCallback) {
    if (this.mSurfaceControlCallback != null)
      this.mSurfaceView.getHolder().removeCallback(this.mSurfaceCallback); 
    this.mSurfaceControlCallback = paramSurfaceControlCallback;
    if (paramSurfaceControlCallback != null)
      this.mSurfaceView.getHolder().addCallback(this.mSurfaceCallback); 
  }
  
  public boolean isZOrderedOnTop() {
    return this.mSurfaceView.isZOrderedOnTop();
  }
  
  public boolean setZOrderedOnTop(boolean paramBoolean) {
    return this.mSurfaceView.setZOrderedOnTop(paramBoolean, true);
  }
  
  private void computeParentPositionAndScale() {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: iconst_0
    //   3: istore_2
    //   4: aload_0
    //   5: getfield mParentSurfaceOwnerView : Ljava/lang/ref/WeakReference;
    //   8: astore_3
    //   9: aload_3
    //   10: ifnull -> 24
    //   13: aload_3
    //   14: invokevirtual get : ()Ljava/lang/Object;
    //   17: checkcast android/view/SurfaceView
    //   20: astore_3
    //   21: goto -> 26
    //   24: aconst_null
    //   25: astore_3
    //   26: aload_3
    //   27: ifnull -> 283
    //   30: aload_0
    //   31: getfield mParentPosition : [I
    //   34: ifnonnull -> 44
    //   37: aload_0
    //   38: iconst_2
    //   39: newarray int
    //   41: putfield mParentPosition : [I
    //   44: aload_0
    //   45: getfield mParentPosition : [I
    //   48: astore #4
    //   50: aload #4
    //   52: iconst_0
    //   53: iaload
    //   54: istore #5
    //   56: aload #4
    //   58: iconst_1
    //   59: iaload
    //   60: istore_1
    //   61: aload_3
    //   62: aload #4
    //   64: invokevirtual getLocationInSurface : ([I)V
    //   67: aload_0
    //   68: getfield mParentPosition : [I
    //   71: astore #4
    //   73: iload #5
    //   75: aload #4
    //   77: iconst_0
    //   78: iaload
    //   79: if_icmpne -> 90
    //   82: iload_1
    //   83: aload #4
    //   85: iconst_1
    //   86: iaload
    //   87: if_icmpeq -> 92
    //   90: iconst_1
    //   91: istore_2
    //   92: aload_0
    //   93: getfield mParentScale : Landroid/graphics/PointF;
    //   96: ifnonnull -> 110
    //   99: aload_0
    //   100: new android/graphics/PointF
    //   103: dup
    //   104: invokespecial <init> : ()V
    //   107: putfield mParentScale : Landroid/graphics/PointF;
    //   110: aload_3
    //   111: invokevirtual getSurfaceRenderPosition : ()Landroid/graphics/Rect;
    //   114: invokevirtual width : ()I
    //   117: i2f
    //   118: fstore #6
    //   120: aload_0
    //   121: getfield mParentScale : Landroid/graphics/PointF;
    //   124: getfield x : F
    //   127: fstore #7
    //   129: fload #6
    //   131: fconst_0
    //   132: fcmpl
    //   133: ifle -> 158
    //   136: aload_0
    //   137: getfield mParentScale : Landroid/graphics/PointF;
    //   140: astore #4
    //   142: aload #4
    //   144: fload #6
    //   146: aload_3
    //   147: invokevirtual getWidth : ()I
    //   150: i2f
    //   151: fdiv
    //   152: putfield x : F
    //   155: goto -> 166
    //   158: aload_0
    //   159: getfield mParentScale : Landroid/graphics/PointF;
    //   162: fconst_1
    //   163: putfield x : F
    //   166: iload_2
    //   167: istore_1
    //   168: iload_2
    //   169: ifne -> 195
    //   172: aload_0
    //   173: getfield mParentScale : Landroid/graphics/PointF;
    //   176: getfield x : F
    //   179: fstore #6
    //   181: iload_2
    //   182: istore_1
    //   183: fload #7
    //   185: fload #6
    //   187: invokestatic compare : (FF)I
    //   190: ifeq -> 195
    //   193: iconst_1
    //   194: istore_1
    //   195: aload_3
    //   196: invokevirtual getSurfaceRenderPosition : ()Landroid/graphics/Rect;
    //   199: invokevirtual height : ()I
    //   202: i2f
    //   203: fstore #6
    //   205: aload_0
    //   206: getfield mParentScale : Landroid/graphics/PointF;
    //   209: getfield y : F
    //   212: fstore #7
    //   214: fload #6
    //   216: fconst_0
    //   217: fcmpl
    //   218: ifle -> 243
    //   221: aload_0
    //   222: getfield mParentScale : Landroid/graphics/PointF;
    //   225: astore #4
    //   227: aload #4
    //   229: fload #6
    //   231: aload_3
    //   232: invokevirtual getHeight : ()I
    //   235: i2f
    //   236: fdiv
    //   237: putfield y : F
    //   240: goto -> 251
    //   243: aload_0
    //   244: getfield mParentScale : Landroid/graphics/PointF;
    //   247: fconst_1
    //   248: putfield y : F
    //   251: iload_1
    //   252: istore_2
    //   253: iload_1
    //   254: ifne -> 302
    //   257: aload_0
    //   258: getfield mParentScale : Landroid/graphics/PointF;
    //   261: getfield y : F
    //   264: fstore #6
    //   266: iload_1
    //   267: istore_2
    //   268: fload #7
    //   270: fload #6
    //   272: invokestatic compare : (FF)I
    //   275: ifeq -> 302
    //   278: iconst_1
    //   279: istore_2
    //   280: goto -> 302
    //   283: aload_0
    //   284: getfield mParentPosition : [I
    //   287: ifnonnull -> 305
    //   290: iload_1
    //   291: istore_2
    //   292: aload_0
    //   293: getfield mParentScale : Landroid/graphics/PointF;
    //   296: ifnull -> 302
    //   299: goto -> 305
    //   302: goto -> 317
    //   305: iconst_1
    //   306: istore_2
    //   307: aload_0
    //   308: aconst_null
    //   309: putfield mParentPosition : [I
    //   312: aload_0
    //   313: aconst_null
    //   314: putfield mParentScale : Landroid/graphics/PointF;
    //   317: iload_2
    //   318: ifeq -> 328
    //   321: aload_0
    //   322: getfield mSurfaceView : Landroid/view/SurfaceView;
    //   325: invokevirtual requestUpdateSurfacePositionAndScale : ()V
    //   328: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #349	-> 0
    //   #353	-> 4
    //   #354	-> 13
    //   #356	-> 26
    //   #357	-> 30
    //   #358	-> 37
    //   #360	-> 44
    //   #361	-> 56
    //   #362	-> 61
    //   #363	-> 67
    //   #365	-> 90
    //   #368	-> 92
    //   #369	-> 99
    //   #372	-> 110
    //   #373	-> 110
    //   #374	-> 120
    //   #375	-> 129
    //   #376	-> 136
    //   #377	-> 142
    //   #379	-> 158
    //   #381	-> 166
    //   #382	-> 181
    //   #383	-> 193
    //   #386	-> 195
    //   #387	-> 195
    //   #388	-> 205
    //   #389	-> 214
    //   #390	-> 221
    //   #391	-> 227
    //   #393	-> 243
    //   #395	-> 251
    //   #396	-> 266
    //   #397	-> 278
    //   #399	-> 283
    //   #400	-> 305
    //   #401	-> 307
    //   #402	-> 312
    //   #405	-> 317
    //   #406	-> 321
    //   #408	-> 328
  }
  
  class SurfaceControlCallback {
    public abstract void onCreated(SurfaceControl param1SurfaceControl);
    
    public abstract void onDestroyed(SurfaceControl param1SurfaceControl);
  }
  
  class SurfacePackageUpdater {
    public abstract void getSurfacePackage(Consumer<SurfaceControlViewHost.SurfacePackage> param1Consumer);
    
    public abstract void onSurfacePackageReleased();
  }
}
