package android.widget.inline;

import android.annotation.NonNull;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Size;
import com.android.internal.util.AnnotationValidations;
import com.android.internal.widget.InlinePresentationStyleUtils;
import java.util.Objects;

public final class InlinePresentationSpec implements Parcelable {
  private static Bundle defaultStyle() {
    return Bundle.EMPTY;
  }
  
  private boolean styleEquals(Bundle paramBundle) {
    return InlinePresentationStyleUtils.bundleEquals(this.mStyle, paramBundle);
  }
  
  public void filterContentTypes() {
    InlinePresentationStyleUtils.filterContentTypes(this.mStyle);
  }
  
  class BaseBuilder {}
  
  InlinePresentationSpec(Size paramSize1, Size paramSize2, Bundle paramBundle) {
    this.mMinSize = paramSize1;
    AnnotationValidations.validate(NonNull.class, null, paramSize1);
    this.mMaxSize = paramSize2;
    AnnotationValidations.validate(NonNull.class, null, paramSize2);
    this.mStyle = paramBundle;
    AnnotationValidations.validate(NonNull.class, null, paramBundle);
  }
  
  public Size getMinSize() {
    return this.mMinSize;
  }
  
  public Size getMaxSize() {
    return this.mMaxSize;
  }
  
  public Bundle getStyle() {
    return this.mStyle;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InlinePresentationSpec { minSize = ");
    stringBuilder.append(this.mMinSize);
    stringBuilder.append(", maxSize = ");
    stringBuilder.append(this.mMaxSize);
    stringBuilder.append(", style = ");
    stringBuilder.append(this.mStyle);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    Size size1 = this.mMinSize, size2 = ((InlinePresentationSpec)paramObject).mMinSize;
    if (Objects.equals(size1, size2)) {
      size1 = this.mMaxSize;
      size2 = ((InlinePresentationSpec)paramObject).mMaxSize;
      if (Objects.equals(size1, size2)) {
        paramObject = ((InlinePresentationSpec)paramObject).mStyle;
        if (styleEquals((Bundle)paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mMinSize);
    int j = Objects.hashCode(this.mMaxSize);
    int k = Objects.hashCode(this.mStyle);
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeSize(this.mMinSize);
    paramParcel.writeSize(this.mMaxSize);
    paramParcel.writeBundle(this.mStyle);
  }
  
  public int describeContents() {
    return 0;
  }
  
  InlinePresentationSpec(Parcel paramParcel) {
    Size size1 = paramParcel.readSize();
    Size size2 = paramParcel.readSize();
    Bundle bundle = paramParcel.readBundle();
    this.mMinSize = size1;
    AnnotationValidations.validate(NonNull.class, null, size1);
    this.mMaxSize = size2;
    AnnotationValidations.validate(NonNull.class, null, size2);
    this.mStyle = bundle;
    AnnotationValidations.validate(NonNull.class, null, bundle);
  }
  
  public static final Parcelable.Creator<InlinePresentationSpec> CREATOR = new Parcelable.Creator<InlinePresentationSpec>() {
      public InlinePresentationSpec[] newArray(int param1Int) {
        return new InlinePresentationSpec[param1Int];
      }
      
      public InlinePresentationSpec createFromParcel(Parcel param1Parcel) {
        return new InlinePresentationSpec(param1Parcel);
      }
    };
  
  private final Size mMaxSize;
  
  private final Size mMinSize;
  
  private final Bundle mStyle;
  
  public static final class Builder extends BaseBuilder {
    private long mBuilderFieldsSet = 0L;
    
    private Size mMaxSize;
    
    private Size mMinSize;
    
    private Bundle mStyle;
    
    public Builder(Size param1Size1, Size param1Size2) {
      this.mMinSize = param1Size1;
      AnnotationValidations.validate(NonNull.class, null, param1Size1);
      this.mMaxSize = param1Size2;
      AnnotationValidations.validate(NonNull.class, null, param1Size2);
    }
    
    public Builder setStyle(Bundle param1Bundle) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x4L;
      this.mStyle = param1Bundle;
      return this;
    }
    
    public InlinePresentationSpec build() {
      checkNotUsed();
      long l = this.mBuilderFieldsSet | 0x8L;
      if ((l & 0x4L) == 0L)
        this.mStyle = InlinePresentationSpec.defaultStyle(); 
      return new InlinePresentationSpec(this.mMinSize, this.mMaxSize, this.mStyle);
    }
    
    private void checkNotUsed() {
      if ((this.mBuilderFieldsSet & 0x8L) == 0L)
        return; 
      throw new IllegalStateException("This Builder should not be reused. Use a new Builder instance instead");
    }
  }
  
  @Deprecated
  private void __metadata() {}
}
