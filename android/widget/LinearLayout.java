package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewDebug.FlagToString;
import android.view.ViewDebug.IntToString;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import com.android.internal.R;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@RemoteView
public class LinearLayout extends ViewGroup {
  public static final int HORIZONTAL = 0;
  
  private static final int INDEX_BOTTOM = 2;
  
  private static final int INDEX_CENTER_VERTICAL = 0;
  
  private static final int INDEX_FILL = 3;
  
  private static final int INDEX_TOP = 1;
  
  public static final int SHOW_DIVIDER_BEGINNING = 1;
  
  public static final int SHOW_DIVIDER_END = 4;
  
  public static final int SHOW_DIVIDER_MIDDLE = 2;
  
  public static final int SHOW_DIVIDER_NONE = 0;
  
  public static final int VERTICAL = 1;
  
  private static final int VERTICAL_GRAVITY_COUNT = 4;
  
  private static boolean sCompatibilityDone = false;
  
  private static boolean sRemeasureWeightedChildren = true;
  
  private final boolean mAllowInconsistentMeasurement;
  
  @ExportedProperty(category = "layout")
  private boolean mBaselineAligned;
  
  @ExportedProperty(category = "layout")
  private int mBaselineAlignedChildIndex;
  
  @ExportedProperty(category = "measurement")
  private int mBaselineChildTop;
  
  private Drawable mDivider;
  
  private int mDividerHeight;
  
  private int mDividerPadding;
  
  private int mDividerWidth;
  
  @ExportedProperty(category = "measurement", flagMapping = {@FlagToString(equals = -1, mask = -1, name = "NONE"), @FlagToString(equals = 0, mask = 0, name = "NONE"), @FlagToString(equals = 48, mask = 48, name = "TOP"), @FlagToString(equals = 80, mask = 80, name = "BOTTOM"), @FlagToString(equals = 3, mask = 3, name = "LEFT"), @FlagToString(equals = 5, mask = 5, name = "RIGHT"), @FlagToString(equals = 8388611, mask = 8388611, name = "START"), @FlagToString(equals = 8388613, mask = 8388613, name = "END"), @FlagToString(equals = 16, mask = 16, name = "CENTER_VERTICAL"), @FlagToString(equals = 112, mask = 112, name = "FILL_VERTICAL"), @FlagToString(equals = 1, mask = 1, name = "CENTER_HORIZONTAL"), @FlagToString(equals = 7, mask = 7, name = "FILL_HORIZONTAL"), @FlagToString(equals = 17, mask = 17, name = "CENTER"), @FlagToString(equals = 119, mask = 119, name = "FILL"), @FlagToString(equals = 8388608, mask = 8388608, name = "RELATIVE")}, formatToHexString = true)
  private int mGravity;
  
  private int mLayoutDirection;
  
  private int[] mMaxAscent;
  
  private int[] mMaxDescent;
  
  @ExportedProperty(category = "measurement")
  private int mOrientation;
  
  private int mShowDividers;
  
  @ExportedProperty(category = "measurement")
  private int mTotalLength;
  
  @ExportedProperty(category = "layout")
  private boolean mUseLargestChild;
  
  @ExportedProperty(category = "layout")
  private float mWeightSum;
  
  public LinearLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public LinearLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public LinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public LinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    boolean bool = true;
    this.mBaselineAligned = true;
    this.mBaselineAlignedChildIndex = -1;
    this.mBaselineChildTop = 0;
    this.mGravity = 8388659;
    this.mLayoutDirection = -1;
    if (!sCompatibilityDone && paramContext != null) {
      boolean bool2;
      int i = (paramContext.getApplicationInfo()).targetSdkVersion;
      if (i >= 28) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      sRemeasureWeightedChildren = bool2;
      sCompatibilityDone = true;
    } 
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LinearLayout, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.LinearLayout, paramAttributeSet, typedArray, paramInt1, paramInt2);
    paramInt1 = typedArray.getInt(1, -1);
    if (paramInt1 >= 0)
      setOrientation(paramInt1); 
    paramInt1 = typedArray.getInt(0, -1);
    if (paramInt1 >= 0)
      setGravity(paramInt1); 
    boolean bool1 = typedArray.getBoolean(2, true);
    if (!bool1)
      setBaselineAligned(bool1); 
    this.mWeightSum = typedArray.getFloat(4, -1.0F);
    this.mBaselineAlignedChildIndex = typedArray.getInt(3, -1);
    this.mUseLargestChild = typedArray.getBoolean(6, false);
    this.mShowDividers = typedArray.getInt(7, 0);
    this.mDividerPadding = typedArray.getDimensionPixelSize(8, 0);
    setDividerDrawable(typedArray.getDrawable(5));
    paramInt1 = (paramContext.getApplicationInfo()).targetSdkVersion;
    if (paramInt1 <= 23) {
      bool1 = bool;
    } else {
      bool1 = false;
    } 
    this.mAllowInconsistentMeasurement = bool1;
    typedArray.recycle();
  }
  
  private boolean isShowingDividers() {
    boolean bool;
    if (this.mShowDividers != 0 && this.mDivider != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setShowDividers(int paramInt) {
    if (paramInt == this.mShowDividers)
      return; 
    this.mShowDividers = paramInt;
    setWillNotDraw(isShowingDividers() ^ true);
    requestLayout();
  }
  
  public boolean shouldDelayChildPressedState() {
    return false;
  }
  
  public int getShowDividers() {
    return this.mShowDividers;
  }
  
  public Drawable getDividerDrawable() {
    return this.mDivider;
  }
  
  public void setDividerDrawable(Drawable paramDrawable) {
    if (paramDrawable == this.mDivider)
      return; 
    this.mDivider = paramDrawable;
    if (paramDrawable != null) {
      this.mDividerWidth = paramDrawable.getIntrinsicWidth();
      this.mDividerHeight = paramDrawable.getIntrinsicHeight();
    } else {
      this.mDividerWidth = 0;
      this.mDividerHeight = 0;
    } 
    setWillNotDraw(isShowingDividers() ^ true);
    requestLayout();
  }
  
  public void setDividerPadding(int paramInt) {
    if (paramInt == this.mDividerPadding)
      return; 
    this.mDividerPadding = paramInt;
    if (isShowingDividers()) {
      requestLayout();
      invalidate();
    } 
  }
  
  public int getDividerPadding() {
    return this.mDividerPadding;
  }
  
  public int getDividerWidth() {
    return this.mDividerWidth;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    if (this.mDivider == null)
      return; 
    if (this.mOrientation == 1) {
      drawDividersVertical(paramCanvas);
    } else {
      drawDividersHorizontal(paramCanvas);
    } 
  }
  
  void drawDividersVertical(Canvas paramCanvas) {
    int i = getVirtualChildCount();
    int j;
    for (j = 0; j < i; j++) {
      View view = getVirtualChildAt(j);
      if (view != null && view.getVisibility() != 8 && 
        hasDividerBeforeChildAt(j)) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        int k = view.getTop(), m = layoutParams.topMargin, n = this.mDividerHeight;
        drawHorizontalDivider(paramCanvas, k - m - n);
      } 
    } 
    if (hasDividerBeforeChildAt(i)) {
      View view = getLastNonGoneChild();
      if (view == null) {
        j = getHeight() - getPaddingBottom() - this.mDividerHeight;
      } else {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        j = view.getBottom() + layoutParams.bottomMargin;
      } 
      drawHorizontalDivider(paramCanvas, j);
    } 
  }
  
  private View getLastNonGoneChild() {
    for (int i = getVirtualChildCount() - 1; i >= 0; i--) {
      View view = getVirtualChildAt(i);
      if (view != null && view.getVisibility() != 8)
        return view; 
    } 
    return null;
  }
  
  void drawDividersHorizontal(Canvas paramCanvas) {
    int i = getVirtualChildCount();
    boolean bool = isLayoutRtl();
    int j;
    for (j = 0; j < i; j++) {
      View view = getVirtualChildAt(j);
      if (view != null && view.getVisibility() != 8 && 
        hasDividerBeforeChildAt(j)) {
        int k;
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (bool) {
          k = view.getRight() + layoutParams.rightMargin;
        } else {
          k = view.getLeft() - layoutParams.leftMargin - this.mDividerWidth;
        } 
        drawVerticalDivider(paramCanvas, k);
      } 
    } 
    if (hasDividerBeforeChildAt(i)) {
      View view = getLastNonGoneChild();
      if (view == null) {
        if (bool) {
          j = getPaddingLeft();
        } else {
          j = getWidth() - getPaddingRight() - this.mDividerWidth;
        } 
      } else {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (bool) {
          j = view.getLeft() - layoutParams.leftMargin - this.mDividerWidth;
        } else {
          j = view.getRight() + layoutParams.rightMargin;
        } 
      } 
      drawVerticalDivider(paramCanvas, j);
    } 
  }
  
  void drawHorizontalDivider(Canvas paramCanvas, int paramInt) {
    Drawable drawable = this.mDivider;
    int i = getPaddingLeft(), j = this.mDividerPadding;
    int k = getWidth(), m = getPaddingRight(), n = this.mDividerPadding, i1 = this.mDividerHeight;
    drawable.setBounds(i + j, paramInt, k - m - n, i1 + paramInt);
    this.mDivider.draw(paramCanvas);
  }
  
  void drawVerticalDivider(Canvas paramCanvas, int paramInt) {
    Drawable drawable = this.mDivider;
    int i = getPaddingTop(), j = this.mDividerPadding, k = this.mDividerWidth;
    int m = getHeight(), n = getPaddingBottom(), i1 = this.mDividerPadding;
    drawable.setBounds(paramInt, i + j, k + paramInt, m - n - i1);
    this.mDivider.draw(paramCanvas);
  }
  
  public boolean isBaselineAligned() {
    return this.mBaselineAligned;
  }
  
  @RemotableViewMethod
  public void setBaselineAligned(boolean paramBoolean) {
    this.mBaselineAligned = paramBoolean;
  }
  
  public boolean isMeasureWithLargestChildEnabled() {
    return this.mUseLargestChild;
  }
  
  @RemotableViewMethod
  public void setMeasureWithLargestChildEnabled(boolean paramBoolean) {
    this.mUseLargestChild = paramBoolean;
  }
  
  public int getBaseline() {
    if (this.mBaselineAlignedChildIndex < 0)
      return super.getBaseline(); 
    int i = getChildCount(), j = this.mBaselineAlignedChildIndex;
    if (i > j) {
      View view = getChildAt(j);
      int k = view.getBaseline();
      if (k == -1) {
        if (this.mBaselineAlignedChildIndex == 0)
          return -1; 
        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
      } 
      j = this.mBaselineChildTop;
      i = j;
      if (this.mOrientation == 1) {
        int m = this.mGravity & 0x70;
        i = j;
        if (m != 48)
          if (m != 16) {
            if (m != 80) {
              i = j;
            } else {
              i = this.mBottom - this.mTop - this.mPaddingBottom - this.mTotalLength;
            } 
          } else {
            i = j + (this.mBottom - this.mTop - this.mPaddingTop - this.mPaddingBottom - this.mTotalLength) / 2;
          }  
      } 
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      return layoutParams.topMargin + i + k;
    } 
    throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
  }
  
  public int getBaselineAlignedChildIndex() {
    return this.mBaselineAlignedChildIndex;
  }
  
  @RemotableViewMethod
  public void setBaselineAlignedChildIndex(int paramInt) {
    if (paramInt >= 0 && paramInt < getChildCount()) {
      this.mBaselineAlignedChildIndex = paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("base aligned child index out of range (0, ");
    stringBuilder.append(getChildCount());
    stringBuilder.append(")");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  View getVirtualChildAt(int paramInt) {
    return getChildAt(paramInt);
  }
  
  int getVirtualChildCount() {
    return getChildCount();
  }
  
  public float getWeightSum() {
    return this.mWeightSum;
  }
  
  @RemotableViewMethod
  public void setWeightSum(float paramFloat) {
    this.mWeightSum = Math.max(0.0F, paramFloat);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    if (this.mOrientation == 1) {
      measureVertical(paramInt1, paramInt2);
    } else {
      measureHorizontal(paramInt1, paramInt2);
    } 
  }
  
  protected boolean hasDividerBeforeChildAt(int paramInt) {
    int i = getVirtualChildCount();
    boolean bool1 = false;
    boolean bool = false;
    boolean bool2 = false;
    if (paramInt == i) {
      bool1 = bool2;
      if ((this.mShowDividers & 0x4) != 0)
        bool1 = true; 
      return bool1;
    } 
    bool2 = allViewsAreGoneBefore(paramInt);
    if (bool2) {
      if ((this.mShowDividers & 0x1) != 0)
        bool1 = true; 
      return bool1;
    } 
    bool1 = bool;
    if ((this.mShowDividers & 0x2) != 0)
      bool1 = true; 
    return bool1;
  }
  
  private boolean allViewsAreGoneBefore(int paramInt) {
    for (; --paramInt >= 0; paramInt--) {
      View view = getVirtualChildAt(paramInt);
      if (view != null && view.getVisibility() != 8)
        return false; 
    } 
    return true;
  }
  
  void measureVertical(int paramInt1, int paramInt2) {
    this.mTotalLength = 0;
    int i = 0;
    int j = getVirtualChildCount();
    int k = View.MeasureSpec.getMode(paramInt1);
    int m = View.MeasureSpec.getMode(paramInt2);
    int n = this.mBaselineAlignedChildIndex;
    boolean bool = this.mUseLargestChild;
    int i1;
    float f;
    int i2, i3, i4, i5, i6, i7, i8, i9, i10;
    for (i1 = 0, f = 0.0F, i2 = 0, i3 = 0, i4 = 0, i5 = 0, i6 = Integer.MIN_VALUE, i7 = 1, i8 = 0, i9 = 0, i10 = 0; i9 < j; i9++) {
      View view = getVirtualChildAt(i9);
      if (view == null) {
        this.mTotalLength += measureNullChild(i9);
      } else if (view.getVisibility() == 8) {
        i9 += getChildrenSkipCount(view, i9);
      } else {
        if (hasDividerBeforeChildAt(i9))
          this.mTotalLength += this.mDividerHeight; 
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        f += layoutParams.weight;
        if (layoutParams.height == 0 && layoutParams.weight > 0.0F) {
          i12 = 1;
        } else {
          i12 = 0;
        } 
        if (m == 1073741824 && i12) {
          i4 = this.mTotalLength;
          this.mTotalLength = Math.max(i4, layoutParams.topMargin + i4 + layoutParams.bottomMargin);
          i4 = 1;
        } else {
          if (i12)
            layoutParams.height = -2; 
          if (f == 0.0F) {
            i14 = this.mTotalLength;
          } else {
            i14 = 0;
          } 
          measureChildBeforeLayout(view, i9, paramInt1, 0, paramInt2, i14);
          int i15 = view.getMeasuredHeight();
          int i14 = i10;
          if (i12) {
            layoutParams.height = 0;
            i14 = i10 + i15;
          } 
          i10 = this.mTotalLength;
          i12 = layoutParams.topMargin;
          int i16 = layoutParams.bottomMargin;
          int i17 = getNextLocationOffset(view);
          this.mTotalLength = Math.max(i10, i10 + i15 + i12 + i16 + i17);
          if (bool) {
            i6 = Math.max(i15, i6);
            i10 = i14;
          } else {
            i10 = i14;
          } 
        } 
        int i13 = i5, i12 = i;
        if (n >= 0 && n == i9 + 1)
          this.mBaselineChildTop = this.mTotalLength; 
        if (i9 >= n || layoutParams.weight <= 0.0F) {
          int i14 = 0;
          i = i14;
          i5 = i8;
          if (k != 1073741824) {
            i = i14;
            i5 = i8;
            if (layoutParams.width == -1) {
              i5 = 1;
              i = 1;
            } 
          } 
          i14 = layoutParams.leftMargin + layoutParams.rightMargin;
          i8 = view.getMeasuredWidth() + i14;
          int i15 = Math.max(i1, i8);
          int i16 = combineMeasuredStates(i2, view.getMeasuredState());
          if (i7 && layoutParams.width == -1) {
            i2 = 1;
          } else {
            i2 = 0;
          } 
          if (layoutParams.weight > 0.0F) {
            if (i != 0)
              i8 = i14; 
            i7 = Math.max(i12, i8);
            i = i13;
          } else {
            if (i != 0) {
              i = i14;
            } else {
              i = i8;
            } 
            i = Math.max(i13, i);
            i7 = i12;
          } 
          i8 = getChildrenSkipCount(view, i9);
          i1 = i2;
          i12 = i15;
          i13 = i7;
          i2 = i16;
          i3++;
          i9 += i8;
          i8 = i5;
          i7 = i1;
          i1 = i12;
          i5 = i;
          i = i13;
        } else {
          throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
        } 
      } 
    } 
    i9 = i5;
    if (i3 > 0 && hasDividerBeforeChildAt(j))
      this.mTotalLength += this.mDividerHeight; 
    if (bool && (m == Integer.MIN_VALUE || m == 0)) {
      this.mTotalLength = 0;
      for (i5 = 0; i5 < j; i5++) {
        View view = getVirtualChildAt(i5);
        if (view == null) {
          this.mTotalLength += measureNullChild(i5);
        } else if (view.getVisibility() == 8) {
          i5 += getChildrenSkipCount(view, i5);
        } else {
          LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
          int i14 = this.mTotalLength;
          int i12 = layoutParams.topMargin, i13 = layoutParams.bottomMargin;
          int i15 = getNextLocationOffset(view);
          this.mTotalLength = Math.max(i14, i14 + i6 + i12 + i13 + i15);
        } 
      } 
    } 
    this.mTotalLength += this.mPaddingTop + this.mPaddingBottom;
    i5 = this.mTotalLength;
    i5 = Math.max(i5, getSuggestedMinimumHeight());
    int i11 = resolveSizeAndState(i5, paramInt2, 0);
    i5 = this.mTotalLength;
    if (this.mAllowInconsistentMeasurement)
      i10 = 0; 
    i10 = (i11 & 0xFFFFFF) - i5 + i10;
    if (i4 != 0 || ((sRemeasureWeightedChildren || i10 != 0) && f > 0.0F)) {
      float f1 = this.mWeightSum;
      if (f1 > 0.0F)
        f = f1; 
      this.mTotalLength = 0;
      for (i3 = 0, i5 = i2, i = n, i2 = i9; i3 < j; i3++) {
        View view = getVirtualChildAt(i3);
        if (view != null && view.getVisibility() != 8) {
          LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
          f1 = layoutParams.weight;
          if (f1 > 0.0F) {
            i4 = (int)(i10 * f1 / f);
            i9 = i10 - i4;
            if (this.mUseLargestChild && m != 1073741824) {
              i10 = i6;
            } else if (layoutParams.height == 0 && (!this.mAllowInconsistentMeasurement || m == 1073741824)) {
              i10 = i4;
            } else {
              i10 = view.getMeasuredHeight() + i4;
            } 
            i10 = Math.max(0, i10);
            i10 = View.MeasureSpec.makeMeasureSpec(i10, 1073741824);
            i4 = getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight + layoutParams.leftMargin + layoutParams.rightMargin, layoutParams.width);
            view.measure(i4, i10);
            i5 = combineMeasuredStates(i5, view.getMeasuredState() & 0xFFFFFF00);
            f -= f1;
            i10 = i9;
          } 
          int i12 = layoutParams.leftMargin + layoutParams.rightMargin;
          i4 = view.getMeasuredWidth() + i12;
          i9 = Math.max(i1, i4);
          if (k != 1073741824 && layoutParams.width == -1) {
            i1 = 1;
          } else {
            i1 = 0;
          } 
          if (i1 != 0) {
            i1 = i12;
          } else {
            i1 = i4;
          } 
          i1 = Math.max(i2, i1);
          if (i7 != 0 && layoutParams.width == -1) {
            i2 = 1;
          } else {
            i2 = 0;
          } 
          i4 = this.mTotalLength;
          n = view.getMeasuredHeight();
          i7 = layoutParams.topMargin;
          int i13 = layoutParams.bottomMargin;
          i12 = getNextLocationOffset(view);
          this.mTotalLength = Math.max(i4, i4 + n + i7 + i13 + i12);
          i7 = i2;
          i2 = i1;
          i1 = i9;
        } 
      } 
      this.mTotalLength += this.mPaddingTop + this.mPaddingBottom;
      i6 = i2;
      i2 = j;
    } else {
      i5 = Math.max(i9, i);
      if (bool && m != 1073741824) {
        for (i10 = 0; i10 < j; i10++) {
          View view = getVirtualChildAt(i10);
          if (view != null && view.getVisibility() != 8) {
            LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
            float f1 = layoutParams.weight;
            if (f1 > 0.0F) {
              m = View.MeasureSpec.makeMeasureSpec(view.getMeasuredWidth(), 1073741824);
              i3 = View.MeasureSpec.makeMeasureSpec(i6, 1073741824);
              view.measure(m, i3);
            } 
          } 
        } 
        i6 = i2;
      } else {
        i6 = i2;
      } 
      i2 = j;
      i10 = i6;
      i6 = i5;
      i5 = i10;
    } 
    i10 = i1;
    if (i7 == 0) {
      i10 = i1;
      if (k != 1073741824)
        i10 = i6; 
    } 
    i = this.mPaddingLeft;
    i6 = this.mPaddingRight;
    i6 = Math.max(i10 + i + i6, getSuggestedMinimumWidth());
    setMeasuredDimension(resolveSizeAndState(i6, paramInt1, i5), i11);
    if (i8 != 0)
      forceUniformWidth(i2, paramInt2); 
  }
  
  private void forceUniformWidth(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
    for (byte b = 0; b < paramInt1; b++) {
      View view = getVirtualChildAt(b);
      if (view != null && view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams.width == -1) {
          int j = layoutParams.height;
          layoutParams.height = view.getMeasuredHeight();
          measureChildWithMargins(view, i, 0, paramInt2, 0);
          layoutParams.height = j;
        } 
      } 
    } 
  }
  
  void measureHorizontal(int paramInt1, int paramInt2) {
    int n;
    this.mTotalLength = 0;
    int i = 0;
    int j = getVirtualChildCount();
    int k = View.MeasureSpec.getMode(paramInt1);
    int m = View.MeasureSpec.getMode(paramInt2);
    if (this.mMaxAscent == null || this.mMaxDescent == null) {
      this.mMaxAscent = new int[4];
      this.mMaxDescent = new int[4];
    } 
    int[] arrayOfInt1 = this.mMaxAscent;
    int[] arrayOfInt2 = this.mMaxDescent;
    arrayOfInt1[3] = -1;
    arrayOfInt1[2] = -1;
    arrayOfInt1[1] = -1;
    arrayOfInt1[0] = -1;
    arrayOfInt2[3] = -1;
    arrayOfInt2[2] = -1;
    arrayOfInt2[1] = -1;
    arrayOfInt2[0] = -1;
    boolean bool1 = this.mBaselineAligned;
    boolean bool2 = this.mUseLargestChild;
    if (k == 1073741824) {
      n = 1;
    } else {
      n = 0;
    } 
    int i1 = 0;
    int i2 = 0;
    int i3, i4;
    float f;
    int i5, i6, i7, i8, i9, i10;
    for (i3 = 0, i4 = 0, f = 0.0F, i5 = 0, i6 = 0, i7 = 1, i8 = Integer.MIN_VALUE, i9 = 0, i10 = 0; i3 < j; i3++) {
      View view = getVirtualChildAt(i3);
      if (view == null) {
        this.mTotalLength += measureNullChild(i3);
      } else if (view.getVisibility() == 8) {
        i3 += getChildrenSkipCount(view, i3);
      } else {
        int i12 = i2 + 1;
        if (hasDividerBeforeChildAt(i3))
          this.mTotalLength += this.mDividerWidth; 
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        f += layoutParams.weight;
        if (layoutParams.width == 0 && layoutParams.weight > 0.0F) {
          i13 = 1;
        } else {
          i13 = 0;
        } 
        if (k == 1073741824 && i13) {
          if (n) {
            this.mTotalLength += layoutParams.leftMargin + layoutParams.rightMargin;
          } else {
            i13 = this.mTotalLength;
            this.mTotalLength = Math.max(i13, layoutParams.leftMargin + i13 + layoutParams.rightMargin);
          } 
          if (bool1) {
            i13 = View.MeasureSpec.getSize(paramInt1);
            i13 = View.MeasureSpec.makeSafeMeasureSpec(i13, 0);
            i2 = View.MeasureSpec.getSize(paramInt2);
            i2 = View.MeasureSpec.makeSafeMeasureSpec(i2, 0);
            view.measure(i13, i2);
            i13 = i5;
          } else {
            i9 = 1;
            i13 = i5;
          } 
        } else {
          if (i13 != 0)
            layoutParams.width = -2; 
          if (f == 0.0F) {
            i2 = this.mTotalLength;
          } else {
            i2 = 0;
          } 
          int i17 = i5;
          measureChildBeforeLayout(view, i3, paramInt1, i2, paramInt2, 0);
          i2 = view.getMeasuredWidth();
          if (i13 != 0) {
            layoutParams.width = 0;
            i1 += i2;
          } 
          if (n) {
            i13 = this.mTotalLength;
            i17 = layoutParams.leftMargin;
            int i18 = layoutParams.rightMargin;
            this.mTotalLength = i13 + i17 + i2 + i18 + getNextLocationOffset(view);
          } else {
            int i19 = this.mTotalLength;
            int i18 = layoutParams.leftMargin;
            i17 = layoutParams.rightMargin;
            i13 = getNextLocationOffset(view);
            this.mTotalLength = Math.max(i19, i19 + i2 + i18 + i17 + i13);
          } 
          if (bool2)
            i8 = Math.max(i2, i8); 
        } 
        int i15 = i5, i13 = i;
        i = 0;
        i2 = i;
        i5 = i10;
        if (m != 1073741824) {
          i2 = i;
          i5 = i10;
          if (layoutParams.height == -1) {
            i5 = 1;
            i2 = 1;
          } 
        } 
        i10 = layoutParams.topMargin + layoutParams.bottomMargin;
        int i14 = view.getMeasuredHeight() + i10;
        i15 = combineMeasuredStates(i15, view.getMeasuredState());
        if (bool1) {
          int i17 = view.getBaseline();
          if (i17 != -1) {
            if (layoutParams.gravity < 0) {
              i = this.mGravity;
            } else {
              i = layoutParams.gravity;
            } 
            i = ((i & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
            arrayOfInt1[i] = Math.max(arrayOfInt1[i], i17);
            arrayOfInt2[i] = Math.max(arrayOfInt2[i], i14 - i17);
          } 
        } 
        int i16 = Math.max(i4, i14);
        if (i7 && layoutParams.height == -1) {
          i = 1;
        } else {
          i = 0;
        } 
        if (layoutParams.weight > 0.0F) {
          if (i2 == 0)
            i10 = i14; 
          i7 = Math.max(i6, i10);
          i6 = i13;
        } else {
          if (i2 == 0)
            i10 = i14; 
          i10 = Math.max(i13, i10);
          i7 = i6;
          i6 = i10;
        } 
        i3 += getChildrenSkipCount(view, i3);
        i4 = i;
        i = i15;
        i13 = i7;
        i14 = i16;
        i2 = i12;
        i10 = i5;
        i7 = i4;
        i4 = i14;
        i5 = i;
        i = i6;
        i6 = i13;
      } 
    } 
    int i11 = i;
    i3 = i8;
    if (i2 > 0 && hasDividerBeforeChildAt(j))
      this.mTotalLength += this.mDividerWidth; 
    if (arrayOfInt1[1] != -1 || arrayOfInt1[0] != -1 || arrayOfInt1[2] != -1 || arrayOfInt1[3] != -1) {
      i = arrayOfInt1[3];
      i8 = arrayOfInt1[0];
      int i13 = arrayOfInt1[1];
      i2 = arrayOfInt1[2];
      i2 = Math.max(i13, i2);
      i8 = Math.max(i8, i2);
      i = Math.max(i, i8);
      i8 = arrayOfInt2[3];
      i2 = arrayOfInt2[0];
      i13 = arrayOfInt2[1];
      int i12 = arrayOfInt2[2];
      i13 = Math.max(i13, i12);
      i2 = Math.max(i2, i13);
      i8 = Math.max(i8, i2);
      i = Math.max(i4, i + i8);
    } else {
      i = i4;
    } 
    if (bool2 && (k == Integer.MIN_VALUE || k == 0)) {
      this.mTotalLength = 0;
      for (i8 = 0; i8 < j; i8++) {
        View view = getVirtualChildAt(i8);
        if (view == null) {
          this.mTotalLength += measureNullChild(i8);
        } else if (view.getVisibility() == 8) {
          i8 += getChildrenSkipCount(view, i8);
        } else {
          LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
          if (n) {
            i2 = this.mTotalLength;
            int i12 = layoutParams.leftMargin;
            i4 = layoutParams.rightMargin;
            this.mTotalLength = i2 + i12 + i3 + i4 + getNextLocationOffset(view);
          } else {
            int i12 = this.mTotalLength;
            i2 = layoutParams.leftMargin;
            i4 = layoutParams.rightMargin;
            int i13 = getNextLocationOffset(view);
            this.mTotalLength = Math.max(i12, i12 + i3 + i2 + i4 + i13);
          } 
        } 
      } 
    } 
    i4 = k;
    this.mTotalLength += this.mPaddingLeft + this.mPaddingRight;
    i8 = this.mTotalLength;
    i8 = Math.max(i8, getSuggestedMinimumWidth());
    i8 = resolveSizeAndState(i8, paramInt1, 0);
    k = i8 & 0xFFFFFF;
    i2 = this.mTotalLength;
    if (this.mAllowInconsistentMeasurement)
      i1 = 0; 
    i1 = k - i2 + i1;
    if (i9 || ((sRemeasureWeightedChildren || i1 != 0) && f > 0.0F)) {
      float f1 = this.mWeightSum;
      if (f1 > 0.0F)
        f = f1; 
      arrayOfInt1[3] = -1;
      arrayOfInt1[2] = -1;
      arrayOfInt1[1] = -1;
      arrayOfInt1[0] = -1;
      arrayOfInt2[3] = -1;
      arrayOfInt2[2] = -1;
      arrayOfInt2[1] = -1;
      arrayOfInt2[0] = -1;
      this.mTotalLength = 0;
      for (i2 = 0, k = -1, i6 = i1, i = i8, i8 = i3, i1 = i11; i2 < j; i2++) {
        View view = getVirtualChildAt(i2);
        if (view != null && view.getVisibility() != 8) {
          LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
          f1 = layoutParams.weight;
          if (f1 > 0.0F) {
            i3 = (int)(i6 * f1 / f);
            if (this.mUseLargestChild && i4 != 1073741824) {
              i9 = i8;
            } else if (layoutParams.width == 0 && (!this.mAllowInconsistentMeasurement || i4 == 1073741824)) {
              i9 = i3;
            } else {
              i9 = view.getMeasuredWidth() + i3;
            } 
            i9 = Math.max(0, i9);
            i11 = View.MeasureSpec.makeMeasureSpec(i9, 1073741824);
            i9 = getChildMeasureSpec(paramInt2, this.mPaddingTop + this.mPaddingBottom + layoutParams.topMargin + layoutParams.bottomMargin, layoutParams.height);
            view.measure(i11, i9);
            i9 = view.getMeasuredState();
            i5 = combineMeasuredStates(i5, i9 & 0xFF000000);
            i6 -= i3;
            f -= f1;
          } 
          if (n) {
            i9 = this.mTotalLength;
            i3 = view.getMeasuredWidth();
            i11 = layoutParams.leftMargin;
            int i13 = layoutParams.rightMargin;
            this.mTotalLength = i9 + i3 + i11 + i13 + getNextLocationOffset(view);
          } else {
            int i14 = this.mTotalLength;
            i9 = view.getMeasuredWidth();
            i3 = layoutParams.leftMargin;
            i11 = layoutParams.rightMargin;
            int i13 = getNextLocationOffset(view);
            this.mTotalLength = Math.max(i14, i9 + i14 + i3 + i11 + i13);
          } 
          if (m != 1073741824 && layoutParams.height == -1) {
            i9 = 1;
          } else {
            i9 = 0;
          } 
          int i12 = layoutParams.topMargin + layoutParams.bottomMargin;
          i11 = view.getMeasuredHeight() + i12;
          i3 = Math.max(k, i11);
          if (i9 != 0) {
            k = i12;
          } else {
            k = i11;
          } 
          k = Math.max(i1, k);
          if (i7 != 0 && layoutParams.height == -1) {
            i1 = 1;
          } else {
            i1 = 0;
          } 
          if (bool1) {
            i9 = view.getBaseline();
            if (i9 != -1) {
              if (layoutParams.gravity < 0) {
                i7 = this.mGravity;
              } else {
                i7 = layoutParams.gravity;
              } 
              i7 = ((i7 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
              arrayOfInt1[i7] = Math.max(arrayOfInt1[i7], i9);
              arrayOfInt2[i7] = Math.max(arrayOfInt2[i7], i11 - i9);
            } 
          } 
          i7 = i1;
          i1 = k;
          k = i3;
        } 
      } 
      this.mTotalLength += this.mPaddingLeft + this.mPaddingRight;
      if (arrayOfInt1[1] != -1 || arrayOfInt1[0] != -1 || arrayOfInt1[2] != -1 || arrayOfInt1[3] != -1) {
        i6 = arrayOfInt1[3];
        i8 = arrayOfInt1[0];
        i9 = arrayOfInt1[1];
        i4 = arrayOfInt1[2];
        i9 = Math.max(i9, i4);
        i8 = Math.max(i8, i9);
        i6 = Math.max(i6, i8);
        i8 = arrayOfInt2[3];
        i9 = arrayOfInt2[0];
        i4 = arrayOfInt2[1];
        i3 = arrayOfInt2[2];
        i4 = Math.max(i4, i3);
        i9 = Math.max(i9, i4);
        i8 = Math.max(i8, i9);
        i6 = Math.max(k, i6 + i8);
      } else {
        i6 = k;
      } 
      i8 = i5;
      i5 = i6;
      i6 = i8;
    } else {
      n = Math.max(i11, i6);
      if (bool2 && i4 != 1073741824) {
        for (i9 = 0, i6 = i1, i1 = n; i9 < j; i9++) {
          View view = getVirtualChildAt(i9);
          if (view != null && view.getVisibility() != 8) {
            LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
            float f1 = layoutParams.weight;
            if (f1 > 0.0F) {
              i4 = View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
              n = View.MeasureSpec.makeMeasureSpec(view.getMeasuredHeight(), 1073741824);
              view.measure(i4, n);
            } 
          } 
        } 
        k = i6;
        i6 = i1;
        i1 = k;
      } else {
        i6 = n;
      } 
      i1 = i6;
      i6 = i5;
      i5 = i;
      i = i8;
    } 
    i8 = i5;
    if (i7 == 0) {
      i8 = i5;
      if (m != 1073741824)
        i8 = i1; 
    } 
    i1 = this.mPaddingTop;
    i5 = this.mPaddingBottom;
    i5 = Math.max(i8 + i1 + i5, getSuggestedMinimumHeight());
    paramInt2 = resolveSizeAndState(i5, paramInt2, i6 << 16);
    setMeasuredDimension(i | 0xFF000000 & i6, paramInt2);
    if (i10 != 0)
      forceUniformHeight(j, paramInt1); 
  }
  
  private void forceUniformHeight(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
    for (byte b = 0; b < paramInt1; b++) {
      View view = getVirtualChildAt(b);
      if (view != null && view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams.height == -1) {
          int j = layoutParams.width;
          layoutParams.width = view.getMeasuredWidth();
          measureChildWithMargins(view, paramInt2, 0, i, 0);
          layoutParams.width = j;
        } 
      } 
    } 
  }
  
  int getChildrenSkipCount(View paramView, int paramInt) {
    return 0;
  }
  
  int measureNullChild(int paramInt) {
    return 0;
  }
  
  void measureChildBeforeLayout(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    measureChildWithMargins(paramView, paramInt2, paramInt3, paramInt4, paramInt5);
  }
  
  int getLocationOffset(View paramView) {
    return 0;
  }
  
  int getNextLocationOffset(View paramView) {
    return 0;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mOrientation == 1) {
      layoutVertical(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      layoutHorizontal(paramInt1, paramInt2, paramInt3, paramInt4);
    } 
  }
  
  void layoutVertical(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = this.mPaddingLeft;
    int j = paramInt3 - paramInt1;
    int k = this.mPaddingRight;
    int m = this.mPaddingRight;
    int n = getVirtualChildCount();
    int i1 = this.mGravity;
    paramInt1 = i1 & 0x70;
    if (paramInt1 != 16) {
      if (paramInt1 != 80) {
        paramInt1 = this.mPaddingTop;
      } else {
        paramInt1 = this.mPaddingTop + paramInt4 - paramInt2 - this.mTotalLength;
      } 
    } else {
      paramInt1 = this.mPaddingTop + (paramInt4 - paramInt2 - this.mTotalLength) / 2;
    } 
    paramInt2 = 0;
    paramInt3 = i;
    while (true) {
      paramInt4 = paramInt3;
      if (paramInt2 < n) {
        View view = getVirtualChildAt(paramInt2);
        if (view == null) {
          paramInt1 += measureNullChild(paramInt2);
        } else if (view.getVisibility() != 8) {
          int i2 = view.getMeasuredWidth();
          int i3 = view.getMeasuredHeight();
          LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
          paramInt3 = layoutParams.gravity;
          if (paramInt3 < 0)
            paramInt3 = i1 & 0x800007; 
          int i4 = getLayoutDirection();
          paramInt3 = Gravity.getAbsoluteGravity(paramInt3, i4);
          paramInt3 &= 0x7;
          if (paramInt3 != 1) {
            if (paramInt3 != 5) {
              paramInt3 = layoutParams.leftMargin + paramInt4;
            } else {
              paramInt3 = layoutParams.rightMargin;
              paramInt3 = j - k - i2 - paramInt3;
            } 
          } else {
            paramInt3 = (j - i - m - i2) / 2;
            i4 = layoutParams.leftMargin;
            int i5 = layoutParams.rightMargin;
            paramInt3 = paramInt3 + paramInt4 + i4 - i5;
          } 
          i4 = paramInt1;
          if (hasDividerBeforeChildAt(paramInt2))
            i4 = paramInt1 + this.mDividerHeight; 
          paramInt1 = i4 + layoutParams.topMargin;
          setChildFrame(view, paramInt3, paramInt1 + getLocationOffset(view), i2, i3);
          i4 = layoutParams.bottomMargin;
          paramInt3 = getNextLocationOffset(view);
          paramInt2 += getChildrenSkipCount(view, paramInt2);
          paramInt1 += i3 + i4 + paramInt3;
        } 
        paramInt2++;
        paramInt3 = paramInt4;
        continue;
      } 
      break;
    } 
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    if (paramInt != this.mLayoutDirection) {
      this.mLayoutDirection = paramInt;
      if (this.mOrientation == 0)
        requestLayout(); 
    } 
  }
  
  void layoutHorizontal(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    byte b1, b2;
    boolean bool1 = isLayoutRtl();
    int i = this.mPaddingTop;
    int j = paramInt4 - paramInt2;
    int k = this.mPaddingBottom;
    int m = this.mPaddingBottom;
    int n = getVirtualChildCount();
    int i1 = this.mGravity;
    boolean bool2 = this.mBaselineAligned;
    int[] arrayOfInt1 = this.mMaxAscent;
    int[] arrayOfInt2 = this.mMaxDescent;
    int i2 = getLayoutDirection();
    paramInt2 = Gravity.getAbsoluteGravity(i1 & 0x800007, i2);
    if (paramInt2 != 1) {
      if (paramInt2 != 5) {
        paramInt1 = this.mPaddingLeft;
      } else {
        paramInt1 = this.mPaddingLeft + paramInt3 - paramInt1 - this.mTotalLength;
      } 
    } else {
      paramInt1 = this.mPaddingLeft + (paramInt3 - paramInt1 - this.mTotalLength) / 2;
    } 
    if (bool1) {
      b1 = n - 1;
      b2 = -1;
    } else {
      b1 = 0;
      b2 = 1;
    } 
    int i3;
    for (paramInt2 = 0, i3 = j, paramInt3 = i, paramInt4 = paramInt1; paramInt2 < n; paramInt2++) {
      int i4 = b1 + b2 * paramInt2;
      View view = getVirtualChildAt(i4);
      if (view == null) {
        paramInt4 += measureNullChild(i4);
      } else if (view.getVisibility() != 8) {
        int i5 = view.getMeasuredWidth();
        int i6 = view.getMeasuredHeight();
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (bool2 && layoutParams.height != -1) {
          paramInt1 = view.getBaseline();
        } else {
          paramInt1 = -1;
        } 
        int i7 = layoutParams.gravity;
        if (i7 < 0)
          i7 = i1 & 0x70; 
        i7 &= 0x70;
        if (i7 != 16) {
          if (i7 != 48) {
            if (i7 != 80) {
              paramInt1 = paramInt3;
            } else {
              i7 = j - k - i6 - layoutParams.bottomMargin;
              if (paramInt1 != -1) {
                int i8 = view.getMeasuredHeight();
                int i9 = arrayOfInt2[2];
                paramInt1 = i7 - i9 - i8 - paramInt1;
              } else {
                paramInt1 = i7;
              } 
            } 
          } else {
            i7 = layoutParams.topMargin + paramInt3;
            if (paramInt1 != -1) {
              paramInt1 = i7 + arrayOfInt1[1] - paramInt1;
            } else {
              paramInt1 = i7;
            } 
          } 
        } else {
          int i8 = (j - i - m - i6) / 2;
          paramInt1 = layoutParams.topMargin;
          i7 = layoutParams.bottomMargin;
          paramInt1 = i8 + paramInt3 + paramInt1 - i7;
        } 
        i7 = paramInt4;
        if (!isLayoutRtl()) {
          i7 = paramInt4;
          if (hasDividerBeforeChildAt(i4))
            i7 = paramInt4 + this.mDividerWidth; 
        } 
        paramInt4 = i7 + layoutParams.leftMargin;
        setChildFrame(view, paramInt4 + getLocationOffset(view), paramInt1, i5, i6);
        paramInt1 = layoutParams.rightMargin;
        paramInt4 += i5 + paramInt1 + getNextLocationOffset(view);
        paramInt1 = paramInt4;
        if (isLayoutRtl()) {
          paramInt1 = paramInt4;
          if (hasDividerBeforeChildAt(i4))
            paramInt1 = paramInt4 + this.mDividerWidth; 
        } 
        paramInt2 += getChildrenSkipCount(view, i4);
        paramInt4 = paramInt1;
      } 
    } 
  }
  
  private void setChildFrame(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramView.layout(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
  }
  
  public void setOrientation(int paramInt) {
    if (this.mOrientation != paramInt) {
      this.mOrientation = paramInt;
      requestLayout();
    } 
  }
  
  public int getOrientation() {
    return this.mOrientation;
  }
  
  @RemotableViewMethod
  public void setGravity(int paramInt) {
    if (this.mGravity != paramInt) {
      int i = paramInt;
      if ((0x800007 & paramInt) == 0)
        i = paramInt | 0x800003; 
      paramInt = i;
      if ((i & 0x70) == 0)
        paramInt = i | 0x30; 
      this.mGravity = paramInt;
      requestLayout();
    } 
  }
  
  public int getGravity() {
    return this.mGravity;
  }
  
  @RemotableViewMethod
  public void setHorizontalGravity(int paramInt) {
    paramInt &= 0x800007;
    int i = this.mGravity;
    if ((0x800007 & i) != paramInt) {
      this.mGravity = 0xFF7FFFF8 & i | paramInt;
      requestLayout();
    } 
  }
  
  @RemotableViewMethod
  public void setVerticalGravity(int paramInt) {
    paramInt &= 0x70;
    int i = this.mGravity;
    if ((i & 0x70) != paramInt) {
      this.mGravity = i & 0xFFFFFF8F | paramInt;
      requestLayout();
    } 
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams generateDefaultLayoutParams() {
    int i = this.mOrientation;
    if (i == 0)
      return new LayoutParams(-2, -2); 
    if (i == 1)
      return new LayoutParams(-1, -2); 
    return null;
  }
  
  protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    if (sPreserveMarginParamsInLayoutParamConversion) {
      if (paramLayoutParams instanceof LayoutParams)
        return new LayoutParams((LayoutParams)paramLayoutParams); 
      if (paramLayoutParams instanceof ViewGroup.MarginLayoutParams)
        return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams); 
    } 
    return new LayoutParams(paramLayoutParams);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public CharSequence getAccessibilityClassName() {
    return LinearLayout.class.getName();
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("layout:baselineAligned", this.mBaselineAligned);
    paramViewHierarchyEncoder.addProperty("layout:baselineAlignedChildIndex", this.mBaselineAlignedChildIndex);
    paramViewHierarchyEncoder.addProperty("measurement:baselineChildTop", this.mBaselineChildTop);
    paramViewHierarchyEncoder.addProperty("measurement:orientation", this.mOrientation);
    paramViewHierarchyEncoder.addProperty("measurement:gravity", this.mGravity);
    paramViewHierarchyEncoder.addProperty("measurement:totalLength", this.mTotalLength);
    paramViewHierarchyEncoder.addProperty("layout:totalLength", this.mTotalLength);
    paramViewHierarchyEncoder.addProperty("layout:useLargestChild", this.mUseLargestChild);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class DividerMode implements Annotation {}
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    @ExportedProperty(category = "layout", mapping = {@IntToString(from = -1, to = "NONE"), @IntToString(from = 0, to = "NONE"), @IntToString(from = 48, to = "TOP"), @IntToString(from = 80, to = "BOTTOM"), @IntToString(from = 3, to = "LEFT"), @IntToString(from = 5, to = "RIGHT"), @IntToString(from = 8388611, to = "START"), @IntToString(from = 8388613, to = "END"), @IntToString(from = 16, to = "CENTER_VERTICAL"), @IntToString(from = 112, to = "FILL_VERTICAL"), @IntToString(from = 1, to = "CENTER_HORIZONTAL"), @IntToString(from = 7, to = "FILL_HORIZONTAL"), @IntToString(from = 17, to = "CENTER"), @IntToString(from = 119, to = "FILL")})
    public int gravity = -1;
    
    @ExportedProperty(category = "layout")
    public float weight;
    
    public LayoutParams(LinearLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      int[] arrayOfInt = R.styleable.LinearLayout_Layout;
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, arrayOfInt);
      this.weight = typedArray.getFloat(3, 0.0F);
      this.gravity = typedArray.getInt(0, -1);
      typedArray.recycle();
    }
    
    public LayoutParams(LinearLayout this$0, int param1Int1) {
      super(this$0, param1Int1);
      this.weight = 0.0F;
    }
    
    public LayoutParams(LinearLayout this$0, int param1Int1, float param1Float) {
      super(this$0, param1Int1);
      this.weight = param1Float;
    }
    
    public LayoutParams(LinearLayout this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public LayoutParams(LinearLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
    }
    
    public LayoutParams(LinearLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
      this.weight = ((LayoutParams)this$0).weight;
      this.gravity = ((LayoutParams)this$0).gravity;
    }
    
    public String debug(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append("LinearLayout.LayoutParams={width=");
      stringBuilder.append(sizeToString(this.width));
      stringBuilder.append(", height=");
      int i = this.height;
      stringBuilder.append(sizeToString(i));
      stringBuilder.append(" weight=");
      stringBuilder.append(this.weight);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    protected void encodeProperties(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      super.encodeProperties(param1ViewHierarchyEncoder);
      param1ViewHierarchyEncoder.addProperty("layout:weight", this.weight);
      param1ViewHierarchyEncoder.addProperty("layout:gravity", this.gravity);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class OrientationMode implements Annotation {}
}
