package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.IBinder;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import com.android.internal.R;
import com.oplus.darkmode.IOplusDarkModeManager;
import java.lang.ref.WeakReference;
import java.util.List;

public class PopupWindow {
  private static final int[] ABOVE_ANCHOR_STATE_SET = new int[] { 16842922 };
  
  private static final int ANIMATION_STYLE_DEFAULT = -1;
  
  private static final int DEFAULT_ANCHORED_GRAVITY = 8388659;
  
  public static final int INPUT_METHOD_FROM_FOCUSABLE = 0;
  
  public static final int INPUT_METHOD_NEEDED = 1;
  
  public static final int INPUT_METHOD_NOT_NEEDED = 2;
  
  private boolean mAboveAnchor;
  
  private Drawable mAboveAnchorBackgroundDrawable;
  
  private boolean mAllowScrollingAnchorParent;
  
  private WeakReference<View> mAnchor;
  
  private WeakReference<View> mAnchorRoot;
  
  private int mAnchorXoff;
  
  private int mAnchorYoff;
  
  private int mAnchoredGravity;
  
  private int mAnimationStyle;
  
  private boolean mAttachedInDecor;
  
  private boolean mAttachedInDecorSet;
  
  private Drawable mBackground;
  
  private View mBackgroundView;
  
  private Drawable mBelowAnchorBackgroundDrawable;
  
  private boolean mClipToScreen;
  
  private boolean mClippingEnabled;
  
  private View mContentView;
  
  private Context mContext;
  
  private PopupDecorView mDecorView;
  
  private float mElevation;
  
  private Transition mEnterTransition;
  
  private Rect mEpicenterBounds;
  
  private Transition mExitTransition;
  
  private boolean mFocusable;
  
  private int mGravity;
  
  private int mHeight;
  
  private int mHeightMode;
  
  private boolean mIgnoreCheekPress;
  
  private int mInputMethodMode;
  
  private boolean mIsAnchorRootAttached;
  
  private boolean mIsDropdown;
  
  private boolean mIsShowing;
  
  private boolean mIsTransitioningToDismiss;
  
  private int mLastHeight;
  
  private int mLastWidth;
  
  private boolean mLayoutInScreen;
  
  private boolean mLayoutInsetDecor;
  
  private boolean mNotTouchModal;
  
  private final View.OnAttachStateChangeListener mOnAnchorDetachedListener;
  
  private final View.OnAttachStateChangeListener mOnAnchorRootDetachedListener;
  
  private OnDismissListener mOnDismissListener;
  
  private final View.OnLayoutChangeListener mOnLayoutChangeListener;
  
  private final ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;
  
  private boolean mOutsideTouchable;
  
  private boolean mOverlapAnchor;
  
  private WeakReference<View> mParentRootView;
  
  private boolean mPopupViewInitialLayoutDirectionInherited;
  
  private int mSoftInputMode;
  
  private int mSplitTouchEnabled;
  
  private final Rect mTempRect;
  
  private final int[] mTmpAppLocation;
  
  private final int[] mTmpDrawingLocation;
  
  private final int[] mTmpScreenLocation;
  
  private View.OnTouchListener mTouchInterceptor;
  
  private boolean mTouchable;
  
  private int mWidth;
  
  private int mWidthMode;
  
  private int mWindowLayoutType;
  
  private WindowManager mWindowManager;
  
  public PopupWindow(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public PopupWindow(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842870);
  }
  
  public PopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public PopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    Transition transition1;
    this.mTmpDrawingLocation = new int[2];
    this.mTmpScreenLocation = new int[2];
    this.mTmpAppLocation = new int[2];
    this.mTempRect = new Rect();
    this.mInputMethodMode = 0;
    this.mSoftInputMode = 1;
    this.mTouchable = true;
    this.mOutsideTouchable = false;
    this.mClippingEnabled = true;
    this.mSplitTouchEnabled = -1;
    this.mAllowScrollingAnchorParent = true;
    this.mLayoutInsetDecor = false;
    this.mAttachedInDecor = true;
    this.mAttachedInDecorSet = false;
    this.mWidth = -2;
    this.mHeight = -2;
    this.mWindowLayoutType = 1000;
    this.mIgnoreCheekPress = false;
    this.mAnimationStyle = -1;
    this.mGravity = 0;
    this.mOnAnchorDetachedListener = (View.OnAttachStateChangeListener)new Object(this);
    this.mOnAnchorRootDetachedListener = (View.OnAttachStateChangeListener)new Object(this);
    this.mOnScrollChangedListener = new _$$Lambda$PopupWindow$nV1HS3Nc6Ck5JRIbIHe3mkyHWzc(this);
    this.mOnLayoutChangeListener = new _$$Lambda$PopupWindow$8Gc2stI5cSJZbuKX7X4Qr_vU2nI(this);
    this.mContext = paramContext;
    this.mWindowManager = (WindowManager)paramContext.getSystemService("window");
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PopupWindow, paramInt1, paramInt2);
    Drawable drawable = typedArray.getDrawable(0);
    this.mElevation = typedArray.getDimension(3, 0.0F);
    this.mOverlapAnchor = typedArray.getBoolean(2, false);
    if (typedArray.hasValueOrEmpty(1)) {
      paramInt1 = typedArray.getResourceId(1, 0);
      if (paramInt1 == 16974594) {
        this.mAnimationStyle = -1;
      } else {
        this.mAnimationStyle = paramInt1;
      } 
    } else {
      this.mAnimationStyle = -1;
    } 
    Transition transition2 = getTransition(typedArray.getResourceId(4, 0));
    if (typedArray.hasValueOrEmpty(5)) {
      transition1 = getTransition(typedArray.getResourceId(5, 0));
    } else if (transition2 == null) {
      paramContext = null;
    } else {
      transition1 = transition2.clone();
    } 
    typedArray.recycle();
    setEnterTransition(transition2);
    setExitTransition(transition1);
    setBackgroundDrawable(drawable);
  }
  
  public PopupWindow() {
    this((View)null, 0, 0);
  }
  
  public PopupWindow(View paramView) {
    this(paramView, 0, 0);
  }
  
  public PopupWindow(int paramInt1, int paramInt2) {
    this((View)null, paramInt1, paramInt2);
  }
  
  public PopupWindow(View paramView, int paramInt1, int paramInt2) {
    this(paramView, paramInt1, paramInt2, false);
  }
  
  public PopupWindow(View paramView, int paramInt1, int paramInt2, boolean paramBoolean) {
    this.mTmpDrawingLocation = new int[2];
    this.mTmpScreenLocation = new int[2];
    this.mTmpAppLocation = new int[2];
    this.mTempRect = new Rect();
    this.mInputMethodMode = 0;
    this.mSoftInputMode = 1;
    this.mTouchable = true;
    this.mOutsideTouchable = false;
    this.mClippingEnabled = true;
    this.mSplitTouchEnabled = -1;
    this.mAllowScrollingAnchorParent = true;
    this.mLayoutInsetDecor = false;
    this.mAttachedInDecor = true;
    this.mAttachedInDecorSet = false;
    this.mWidth = -2;
    this.mHeight = -2;
    this.mWindowLayoutType = 1000;
    this.mIgnoreCheekPress = false;
    this.mAnimationStyle = -1;
    this.mGravity = 0;
    this.mOnAnchorDetachedListener = (View.OnAttachStateChangeListener)new Object(this);
    this.mOnAnchorRootDetachedListener = (View.OnAttachStateChangeListener)new Object(this);
    this.mOnScrollChangedListener = new _$$Lambda$PopupWindow$nV1HS3Nc6Ck5JRIbIHe3mkyHWzc(this);
    this.mOnLayoutChangeListener = new _$$Lambda$PopupWindow$8Gc2stI5cSJZbuKX7X4Qr_vU2nI(this);
    if (paramView != null) {
      Context context = paramView.getContext();
      this.mWindowManager = (WindowManager)context.getSystemService("window");
    } 
    setContentView(paramView);
    setWidth(paramInt1);
    setHeight(paramInt2);
    setFocusable(paramBoolean);
  }
  
  public void setEnterTransition(Transition paramTransition) {
    this.mEnterTransition = paramTransition;
  }
  
  public Transition getEnterTransition() {
    return this.mEnterTransition;
  }
  
  public void setExitTransition(Transition paramTransition) {
    this.mExitTransition = paramTransition;
  }
  
  public Transition getExitTransition() {
    return this.mExitTransition;
  }
  
  public Rect getEpicenterBounds() {
    Rect rect;
    if (this.mEpicenterBounds != null) {
      rect = new Rect(this.mEpicenterBounds);
    } else {
      rect = null;
    } 
    return rect;
  }
  
  public void setEpicenterBounds(Rect paramRect) {
    if (paramRect != null) {
      paramRect = new Rect(paramRect);
    } else {
      paramRect = null;
    } 
    this.mEpicenterBounds = paramRect;
  }
  
  private Transition getTransition(int paramInt) {
    // Byte code:
    //   0: iload_1
    //   1: ifeq -> 61
    //   4: iload_1
    //   5: ldc_w 17760256
    //   8: if_icmpeq -> 61
    //   11: aload_0
    //   12: getfield mContext : Landroid/content/Context;
    //   15: invokestatic from : (Landroid/content/Context;)Landroid/transition/TransitionInflater;
    //   18: astore_2
    //   19: aload_2
    //   20: iload_1
    //   21: invokevirtual inflateTransition : (I)Landroid/transition/Transition;
    //   24: astore_2
    //   25: aload_2
    //   26: ifnull -> 61
    //   29: aload_2
    //   30: instanceof android/transition/TransitionSet
    //   33: ifeq -> 53
    //   36: aload_2
    //   37: checkcast android/transition/TransitionSet
    //   40: astore_3
    //   41: aload_3
    //   42: invokevirtual getTransitionCount : ()I
    //   45: ifne -> 53
    //   48: iconst_1
    //   49: istore_1
    //   50: goto -> 55
    //   53: iconst_0
    //   54: istore_1
    //   55: iload_1
    //   56: ifne -> 61
    //   59: aload_2
    //   60: areturn
    //   61: aconst_null
    //   62: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #521	-> 0
    //   #522	-> 11
    //   #523	-> 19
    //   #524	-> 25
    //   #525	-> 29
    //   #526	-> 41
    //   #527	-> 55
    //   #528	-> 59
    //   #532	-> 61
  }
  
  public Drawable getBackground() {
    return this.mBackground;
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable) {
    this.mBackground = paramDrawable;
    if (paramDrawable instanceof StateListDrawable) {
      byte b2;
      StateListDrawable stateListDrawable = (StateListDrawable)paramDrawable;
      int i = stateListDrawable.findStateDrawableIndex(ABOVE_ANCHOR_STATE_SET);
      int j = stateListDrawable.getStateCount();
      byte b1 = -1;
      byte b = 0;
      while (true) {
        b2 = b1;
        if (b < j) {
          if (b != i) {
            b2 = b;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (i != -1 && b2 != -1) {
        this.mAboveAnchorBackgroundDrawable = stateListDrawable.getStateDrawable(i);
        this.mBelowAnchorBackgroundDrawable = stateListDrawable.getStateDrawable(b2);
      } else {
        this.mBelowAnchorBackgroundDrawable = null;
        this.mAboveAnchorBackgroundDrawable = null;
      } 
    } 
  }
  
  public float getElevation() {
    return this.mElevation;
  }
  
  public void setElevation(float paramFloat) {
    this.mElevation = paramFloat;
  }
  
  public int getAnimationStyle() {
    return this.mAnimationStyle;
  }
  
  public void setIgnoreCheekPress() {
    this.mIgnoreCheekPress = true;
  }
  
  public void setAnimationStyle(int paramInt) {
    this.mAnimationStyle = paramInt;
  }
  
  public View getContentView() {
    return this.mContentView;
  }
  
  public void setContentView(View paramView) {
    if (isShowing())
      return; 
    this.mContentView = paramView;
    if (this.mContext == null && paramView != null)
      this.mContext = paramView.getContext(); 
    if (this.mWindowManager == null && this.mContentView != null)
      this.mWindowManager = (WindowManager)this.mContext.getSystemService("window"); 
    Context context = this.mContext;
    if (context != null && !this.mAttachedInDecorSet) {
      boolean bool;
      if ((context.getApplicationInfo()).targetSdkVersion >= 22) {
        bool = true;
      } else {
        bool = false;
      } 
      setAttachedInDecor(bool);
    } 
  }
  
  public void setTouchInterceptor(View.OnTouchListener paramOnTouchListener) {
    this.mTouchInterceptor = paramOnTouchListener;
  }
  
  public boolean isFocusable() {
    return this.mFocusable;
  }
  
  public void setFocusable(boolean paramBoolean) {
    this.mFocusable = paramBoolean;
  }
  
  public int getInputMethodMode() {
    return this.mInputMethodMode;
  }
  
  public void setInputMethodMode(int paramInt) {
    this.mInputMethodMode = paramInt;
  }
  
  public void setSoftInputMode(int paramInt) {
    this.mSoftInputMode = paramInt;
  }
  
  public int getSoftInputMode() {
    return this.mSoftInputMode;
  }
  
  public boolean isTouchable() {
    return this.mTouchable;
  }
  
  public void setTouchable(boolean paramBoolean) {
    this.mTouchable = paramBoolean;
  }
  
  public boolean isOutsideTouchable() {
    return this.mOutsideTouchable;
  }
  
  public void setOutsideTouchable(boolean paramBoolean) {
    this.mOutsideTouchable = paramBoolean;
  }
  
  public boolean isClippingEnabled() {
    return this.mClippingEnabled;
  }
  
  public void setClippingEnabled(boolean paramBoolean) {
    this.mClippingEnabled = paramBoolean;
  }
  
  @Deprecated
  public boolean isClipToScreenEnabled() {
    return this.mClipToScreen;
  }
  
  @Deprecated
  public void setClipToScreenEnabled(boolean paramBoolean) {
    this.mClipToScreen = paramBoolean;
  }
  
  public boolean isClippedToScreen() {
    return this.mClipToScreen;
  }
  
  public void setIsClippedToScreen(boolean paramBoolean) {
    this.mClipToScreen = paramBoolean;
  }
  
  void setAllowScrollingAnchorParent(boolean paramBoolean) {
    this.mAllowScrollingAnchorParent = paramBoolean;
  }
  
  protected final boolean getAllowScrollingAnchorParent() {
    return this.mAllowScrollingAnchorParent;
  }
  
  public boolean isSplitTouchEnabled() {
    int i = this.mSplitTouchEnabled;
    boolean bool1 = false, bool2 = false;
    if (i < 0) {
      Context context = this.mContext;
      if (context != null) {
        if ((context.getApplicationInfo()).targetSdkVersion >= 11)
          bool2 = true; 
        return bool2;
      } 
    } 
    bool2 = bool1;
    if (this.mSplitTouchEnabled == 1)
      bool2 = true; 
    return bool2;
  }
  
  public void setSplitTouchEnabled(boolean paramBoolean) {
    this.mSplitTouchEnabled = paramBoolean;
  }
  
  @Deprecated
  public boolean isLayoutInScreenEnabled() {
    return this.mLayoutInScreen;
  }
  
  @Deprecated
  public void setLayoutInScreenEnabled(boolean paramBoolean) {
    this.mLayoutInScreen = paramBoolean;
  }
  
  public boolean isLaidOutInScreen() {
    return this.mLayoutInScreen;
  }
  
  public void setIsLaidOutInScreen(boolean paramBoolean) {
    this.mLayoutInScreen = paramBoolean;
  }
  
  public boolean isAttachedInDecor() {
    return this.mAttachedInDecor;
  }
  
  public void setAttachedInDecor(boolean paramBoolean) {
    this.mAttachedInDecor = paramBoolean;
    this.mAttachedInDecorSet = true;
  }
  
  public void setLayoutInsetDecor(boolean paramBoolean) {
    this.mLayoutInsetDecor = paramBoolean;
  }
  
  protected final boolean isLayoutInsetDecor() {
    return this.mLayoutInsetDecor;
  }
  
  public void setWindowLayoutType(int paramInt) {
    this.mWindowLayoutType = paramInt;
  }
  
  public int getWindowLayoutType() {
    return this.mWindowLayoutType;
  }
  
  public boolean isTouchModal() {
    return this.mNotTouchModal ^ true;
  }
  
  public void setTouchModal(boolean paramBoolean) {
    this.mNotTouchModal = paramBoolean ^ true;
  }
  
  @Deprecated
  public void setWindowLayoutMode(int paramInt1, int paramInt2) {
    this.mWidthMode = paramInt1;
    this.mHeightMode = paramInt2;
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  public void setHeight(int paramInt) {
    this.mHeight = paramInt;
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public void setWidth(int paramInt) {
    this.mWidth = paramInt;
  }
  
  public void setOverlapAnchor(boolean paramBoolean) {
    this.mOverlapAnchor = paramBoolean;
  }
  
  public boolean getOverlapAnchor() {
    return this.mOverlapAnchor;
  }
  
  public boolean isShowing() {
    return this.mIsShowing;
  }
  
  protected final void setShowing(boolean paramBoolean) {
    this.mIsShowing = paramBoolean;
  }
  
  protected final void setDropDown(boolean paramBoolean) {
    this.mIsDropdown = paramBoolean;
  }
  
  protected final void setTransitioningToDismiss(boolean paramBoolean) {
    this.mIsTransitioningToDismiss = paramBoolean;
  }
  
  protected final boolean isTransitioningToDismiss() {
    return this.mIsTransitioningToDismiss;
  }
  
  public void showAtLocation(View paramView, int paramInt1, int paramInt2, int paramInt3) {
    this.mParentRootView = new WeakReference<>(paramView.getRootView());
    showAtLocation(paramView.getWindowToken(), paramInt1, paramInt2, paramInt3);
  }
  
  public void showAtLocation(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3) {
    if (isShowing() || this.mContentView == null)
      return; 
    TransitionManager.endTransitions(this.mDecorView);
    detachFromAnchor();
    this.mIsShowing = true;
    this.mIsDropdown = false;
    this.mGravity = paramInt1;
    WindowManager.LayoutParams layoutParams = createPopupLayoutParams(paramIBinder);
    preparePopup(layoutParams);
    layoutParams.x = paramInt2;
    layoutParams.y = paramInt3;
    invokePopup(layoutParams);
  }
  
  public void showAsDropDown(View paramView) {
    showAsDropDown(paramView, 0, 0);
  }
  
  public void showAsDropDown(View paramView, int paramInt1, int paramInt2) {
    showAsDropDown(paramView, paramInt1, paramInt2, 8388659);
  }
  
  public void showAsDropDown(View paramView, int paramInt1, int paramInt2, int paramInt3) {
    long l;
    if (isShowing() || !hasContentView())
      return; 
    TransitionManager.endTransitions(this.mDecorView);
    attachToAnchor(paramView, paramInt1, paramInt2, paramInt3);
    this.mIsShowing = true;
    this.mIsDropdown = true;
    WindowManager.LayoutParams layoutParams = createPopupLayoutParams(paramView.getApplicationWindowToken());
    preparePopup(layoutParams);
    boolean bool = findDropDownPosition(paramView, layoutParams, paramInt1, paramInt2, layoutParams.width, layoutParams.height, paramInt3, this.mAllowScrollingAnchorParent);
    updateAboveAnchor(bool);
    if (paramView != null) {
      l = paramView.getAccessibilityViewId();
    } else {
      l = -1L;
    } 
    layoutParams.accessibilityIdOfAnchor = l;
    invokePopup(layoutParams);
  }
  
  protected final void updateAboveAnchor(boolean paramBoolean) {
    if (paramBoolean != this.mAboveAnchor) {
      this.mAboveAnchor = paramBoolean;
      if (this.mBackground != null) {
        View view = this.mBackgroundView;
        if (view != null) {
          Drawable drawable = this.mAboveAnchorBackgroundDrawable;
          if (drawable != null) {
            if (paramBoolean) {
              view.setBackground(drawable);
            } else {
              view.setBackground(this.mBelowAnchorBackgroundDrawable);
            } 
          } else {
            view.refreshDrawableState();
          } 
        } 
      } 
    } 
  }
  
  public boolean isAboveAnchor() {
    return this.mAboveAnchor;
  }
  
  private void preparePopup(WindowManager.LayoutParams paramLayoutParams) {
    if (this.mContentView != null && this.mContext != null && this.mWindowManager != null) {
      if (paramLayoutParams.accessibilityTitle == null)
        paramLayoutParams.accessibilityTitle = this.mContext.getString(17041118); 
      PopupDecorView popupDecorView = this.mDecorView;
      if (popupDecorView != null)
        popupDecorView.cancelTransitions(); 
      if (this.mBackground != null) {
        PopupBackgroundView popupBackgroundView = createBackgroundView(this.mContentView);
        popupBackgroundView.setBackground(this.mBackground);
      } else {
        this.mBackgroundView = this.mContentView;
      } 
      this.mDecorView = popupDecorView = createDecorView(this.mBackgroundView);
      boolean bool = true;
      popupDecorView.setIsRootNamespace(true);
      ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).changeUsageForceDarkAlgorithmType(this.mDecorView, 2);
      this.mBackgroundView.setElevation(this.mElevation);
      paramLayoutParams.setSurfaceInsets(this.mBackgroundView, true, true);
      View view = this.mContentView;
      if (view.getRawLayoutDirection() != 2)
        bool = false; 
      this.mPopupViewInitialLayoutDirectionInherited = bool;
      return;
    } 
    throw new IllegalStateException("You must specify a valid content view by calling setContentView() before attempting to show the popup.");
  }
  
  private PopupBackgroundView createBackgroundView(View paramView) {
    byte b;
    ViewGroup.LayoutParams layoutParams = this.mContentView.getLayoutParams();
    if (layoutParams != null && layoutParams.height == -2) {
      b = -2;
    } else {
      b = -1;
    } 
    PopupBackgroundView popupBackgroundView = new PopupBackgroundView(this.mContext);
    layoutParams = new FrameLayout.LayoutParams(-1, b);
    popupBackgroundView.addView(paramView, layoutParams);
    return popupBackgroundView;
  }
  
  private PopupDecorView createDecorView(View paramView) {
    byte b;
    ViewGroup.LayoutParams layoutParams = this.mContentView.getLayoutParams();
    if (layoutParams != null && layoutParams.height == -2) {
      b = -2;
    } else {
      b = -1;
    } 
    PopupDecorView popupDecorView = new PopupDecorView(this.mContext);
    popupDecorView.addView(paramView, -1, b);
    popupDecorView.setClipChildren(false);
    popupDecorView.setClipToPadding(false);
    return popupDecorView;
  }
  
  private void invokePopup(WindowManager.LayoutParams paramLayoutParams) {
    Context context = this.mContext;
    if (context != null)
      paramLayoutParams.packageName = context.getPackageName(); 
    PopupDecorView popupDecorView = this.mDecorView;
    popupDecorView.setFitsSystemWindows(this.mLayoutInsetDecor);
    setLayoutDirectionFromAnchor();
    this.mWindowManager.addView(popupDecorView, paramLayoutParams);
    Transition transition = this.mEnterTransition;
    if (transition != null)
      popupDecorView.requestEnterTransition(transition); 
  }
  
  private void setLayoutDirectionFromAnchor() {
    WeakReference<View> weakReference = this.mAnchor;
    if (weakReference != null) {
      View view = weakReference.get();
      if (view != null && this.mPopupViewInitialLayoutDirectionInherited)
        this.mDecorView.setLayoutDirection(view.getLayoutDirection()); 
    } 
  }
  
  private int computeGravity() {
    int i = this.mGravity, j = i;
    if (i == 0)
      j = 8388659; 
    i = j;
    if (this.mIsDropdown) {
      if (!this.mClipToScreen) {
        i = j;
        if (this.mClippingEnabled) {
          i = j | 0x10000000;
          return i;
        } 
        return i;
      } 
    } else {
      return i;
    } 
    i = j | 0x10000000;
    return i;
  }
  
  protected final WindowManager.LayoutParams createPopupLayoutParams(IBinder paramIBinder) {
    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
    layoutParams.gravity = computeGravity();
    layoutParams.flags = computeFlags(layoutParams.flags);
    layoutParams.type = this.mWindowLayoutType;
    layoutParams.token = paramIBinder;
    layoutParams.softInputMode = this.mSoftInputMode;
    layoutParams.windowAnimations = computeAnimationResource();
    Drawable drawable = this.mBackground;
    if (drawable != null) {
      layoutParams.format = drawable.getOpacity();
    } else {
      layoutParams.format = -3;
    } 
    int i = this.mHeightMode;
    if (i < 0) {
      this.mLastHeight = i;
      layoutParams.height = i;
    } else {
      this.mLastHeight = i = this.mHeight;
      layoutParams.height = i;
    } 
    i = this.mWidthMode;
    if (i < 0) {
      this.mLastWidth = i;
      layoutParams.width = i;
    } else {
      this.mLastWidth = i = this.mWidth;
      layoutParams.width = i;
    } 
    layoutParams.privateFlags = 98304;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PopupWindow:");
    stringBuilder.append(Integer.toHexString(hashCode()));
    layoutParams.setTitle(stringBuilder.toString());
    return layoutParams;
  }
  
  private int computeFlags(int paramInt) {
    // Byte code:
    //   0: iload_1
    //   1: ldc_w -8815129
    //   4: iand
    //   5: istore_1
    //   6: iload_1
    //   7: istore_2
    //   8: aload_0
    //   9: getfield mIgnoreCheekPress : Z
    //   12: ifeq -> 21
    //   15: iload_1
    //   16: ldc_w 32768
    //   19: ior
    //   20: istore_2
    //   21: aload_0
    //   22: getfield mFocusable : Z
    //   25: ifne -> 52
    //   28: iload_2
    //   29: bipush #8
    //   31: ior
    //   32: istore_2
    //   33: iload_2
    //   34: istore_1
    //   35: aload_0
    //   36: getfield mInputMethodMode : I
    //   39: iconst_1
    //   40: if_icmpne -> 68
    //   43: iload_2
    //   44: ldc_w 131072
    //   47: ior
    //   48: istore_1
    //   49: goto -> 68
    //   52: iload_2
    //   53: istore_1
    //   54: aload_0
    //   55: getfield mInputMethodMode : I
    //   58: iconst_2
    //   59: if_icmpne -> 68
    //   62: iload_2
    //   63: ldc_w 131072
    //   66: ior
    //   67: istore_1
    //   68: iload_1
    //   69: istore_3
    //   70: aload_0
    //   71: getfield mTouchable : Z
    //   74: ifne -> 82
    //   77: iload_1
    //   78: bipush #16
    //   80: ior
    //   81: istore_3
    //   82: iload_3
    //   83: istore_2
    //   84: aload_0
    //   85: getfield mOutsideTouchable : Z
    //   88: ifeq -> 97
    //   91: iload_3
    //   92: ldc_w 262144
    //   95: ior
    //   96: istore_2
    //   97: aload_0
    //   98: getfield mClippingEnabled : Z
    //   101: ifeq -> 113
    //   104: iload_2
    //   105: istore_1
    //   106: aload_0
    //   107: getfield mClipToScreen : Z
    //   110: ifeq -> 119
    //   113: iload_2
    //   114: sipush #512
    //   117: ior
    //   118: istore_1
    //   119: iload_1
    //   120: istore_3
    //   121: aload_0
    //   122: invokevirtual isSplitTouchEnabled : ()Z
    //   125: ifeq -> 134
    //   128: iload_1
    //   129: ldc_w 8388608
    //   132: ior
    //   133: istore_3
    //   134: iload_3
    //   135: istore_2
    //   136: aload_0
    //   137: getfield mLayoutInScreen : Z
    //   140: ifeq -> 149
    //   143: iload_3
    //   144: sipush #256
    //   147: ior
    //   148: istore_2
    //   149: iload_2
    //   150: istore_1
    //   151: aload_0
    //   152: getfield mLayoutInsetDecor : Z
    //   155: ifeq -> 164
    //   158: iload_2
    //   159: ldc_w 65536
    //   162: ior
    //   163: istore_1
    //   164: iload_1
    //   165: istore_2
    //   166: aload_0
    //   167: getfield mNotTouchModal : Z
    //   170: ifeq -> 178
    //   173: iload_1
    //   174: bipush #32
    //   176: ior
    //   177: istore_2
    //   178: iload_2
    //   179: istore_1
    //   180: aload_0
    //   181: getfield mAttachedInDecor : Z
    //   184: ifeq -> 193
    //   187: iload_2
    //   188: ldc_w 1073741824
    //   191: ior
    //   192: istore_1
    //   193: iload_1
    //   194: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1662	-> 0
    //   #1670	-> 6
    //   #1671	-> 15
    //   #1673	-> 21
    //   #1674	-> 28
    //   #1675	-> 33
    //   #1676	-> 43
    //   #1678	-> 52
    //   #1679	-> 62
    //   #1681	-> 68
    //   #1682	-> 77
    //   #1684	-> 82
    //   #1685	-> 91
    //   #1687	-> 97
    //   #1688	-> 113
    //   #1690	-> 119
    //   #1691	-> 128
    //   #1693	-> 134
    //   #1694	-> 143
    //   #1696	-> 149
    //   #1697	-> 158
    //   #1699	-> 164
    //   #1700	-> 173
    //   #1702	-> 178
    //   #1703	-> 187
    //   #1705	-> 193
  }
  
  private int computeAnimationResource() {
    int i = this.mAnimationStyle;
    if (i == -1) {
      if (this.mIsDropdown) {
        if (this.mAboveAnchor) {
          i = 16974582;
        } else {
          i = 16974581;
        } 
        return i;
      } 
      return 0;
    } 
    return i;
  }
  
  protected boolean findDropDownPosition(View paramView, WindowManager.LayoutParams paramLayoutParams, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean) {
    int i = paramView.getHeight();
    int j = paramView.getWidth();
    if (this.mOverlapAnchor)
      paramInt2 -= i; 
    int[] arrayOfInt1 = this.mTmpAppLocation;
    View view = getAppRootView(paramView);
    view.getLocationOnScreen(arrayOfInt1);
    int[] arrayOfInt2 = this.mTmpScreenLocation;
    paramView.getLocationOnScreen(arrayOfInt2);
    int[] arrayOfInt3 = this.mTmpDrawingLocation;
    boolean bool = false;
    arrayOfInt3[0] = arrayOfInt2[0] - arrayOfInt1[0];
    arrayOfInt3[1] = arrayOfInt2[1] - arrayOfInt1[1];
    paramLayoutParams.x = arrayOfInt3[0] + paramInt1;
    paramLayoutParams.y = arrayOfInt3[1] + i + paramInt2;
    Rect rect = new Rect();
    view.getWindowVisibleDisplayFrame(rect);
    if (paramInt3 == -1)
      paramInt3 = rect.right - rect.left; 
    if (paramInt4 == -1)
      paramInt4 = rect.bottom - rect.top; 
    paramLayoutParams.gravity = computeGravity();
    paramLayoutParams.width = paramInt3;
    paramLayoutParams.height = paramInt4;
    paramInt5 = Gravity.getAbsoluteGravity(paramInt5, paramView.getLayoutDirection()) & 0x7;
    if (paramInt5 == 5)
      paramLayoutParams.x -= paramInt3 - j; 
    boolean bool1 = tryFitVertical(paramLayoutParams, paramInt2, paramInt4, i, arrayOfInt3[1], arrayOfInt2[1], rect.top, rect.bottom, false);
    boolean bool2 = tryFitHorizontal(paramLayoutParams, paramInt1, paramInt3, j, arrayOfInt3[0], arrayOfInt2[0], rect.left, rect.right, false);
    if (!bool1 || !bool2) {
      int k = paramView.getScrollX();
      int m = paramView.getScrollY();
      Rect rect1 = new Rect(k, m, k + paramInt3 + paramInt1, m + paramInt4 + i + paramInt2);
      if (paramBoolean && paramView.requestRectangleOnScreen(rect1, true)) {
        paramView.getLocationOnScreen(arrayOfInt2);
        arrayOfInt3[0] = arrayOfInt2[0] - arrayOfInt1[0];
        arrayOfInt3[1] = arrayOfInt2[1] - arrayOfInt1[1];
        m = arrayOfInt3[0];
        paramLayoutParams.x = m + paramInt1;
        paramLayoutParams.y = arrayOfInt3[1] + i + paramInt2;
        if (paramInt5 == 5)
          paramLayoutParams.x -= paramInt3 - j; 
      } 
      m = arrayOfInt3[1];
      int n = arrayOfInt2[1];
      paramInt5 = rect.top;
      k = rect.bottom;
      paramBoolean = this.mClipToScreen;
      WindowManager.LayoutParams layoutParams = paramLayoutParams;
      tryFitVertical(paramLayoutParams, paramInt2, paramInt4, i, m, n, paramInt5, k, paramBoolean);
      tryFitHorizontal(paramLayoutParams, paramInt1, paramInt3, j, arrayOfInt3[0], arrayOfInt2[0], rect.left, rect.right, this.mClipToScreen);
    } else {
      WindowManager.LayoutParams layoutParams = paramLayoutParams;
    } 
    paramBoolean = bool;
    if (paramLayoutParams.y < arrayOfInt3[1])
      paramBoolean = true; 
    return paramBoolean;
  }
  
  private boolean tryFitVertical(WindowManager.LayoutParams paramLayoutParams, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean) {
    int i = paramLayoutParams.y + paramInt5 - paramInt4;
    if (i >= paramInt6 && paramInt2 <= paramInt7 - i)
      return true; 
    if (paramInt2 <= i - paramInt3 - paramInt6) {
      if (this.mOverlapAnchor)
        paramInt1 += paramInt3; 
      paramLayoutParams.y = paramInt4 - paramInt2 + paramInt1;
      return true;
    } 
    if (positionInDisplayVertical(paramLayoutParams, paramInt2, paramInt4, paramInt5, paramInt6, paramInt7, paramBoolean))
      return true; 
    return false;
  }
  
  private boolean positionInDisplayVertical(WindowManager.LayoutParams paramLayoutParams, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean) {
    boolean bool1 = true;
    paramInt2 = paramInt3 - paramInt2;
    paramLayoutParams.y += paramInt2;
    paramLayoutParams.height = paramInt1;
    paramInt3 = paramLayoutParams.y + paramInt1;
    if (paramInt3 > paramInt5)
      paramLayoutParams.y -= paramInt3 - paramInt5; 
    boolean bool2 = bool1;
    if (paramLayoutParams.y < paramInt4) {
      paramLayoutParams.y = paramInt4;
      paramInt3 = paramInt5 - paramInt4;
      if (paramBoolean && paramInt1 > paramInt3) {
        paramLayoutParams.height = paramInt3;
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
    } 
    paramLayoutParams.y -= paramInt2;
    return bool2;
  }
  
  private boolean tryFitHorizontal(WindowManager.LayoutParams paramLayoutParams, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean) {
    paramInt1 = paramLayoutParams.x + paramInt5 - paramInt4;
    if (paramInt1 >= paramInt6 && paramInt2 <= paramInt7 - paramInt1)
      return true; 
    if (positionInDisplayHorizontal(paramLayoutParams, paramInt2, paramInt4, paramInt5, paramInt6, paramInt7, paramBoolean))
      return true; 
    return false;
  }
  
  private boolean positionInDisplayHorizontal(WindowManager.LayoutParams paramLayoutParams, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean) {
    boolean bool1 = true;
    paramInt2 = paramInt3 - paramInt2;
    paramLayoutParams.x += paramInt2;
    paramInt3 = paramLayoutParams.x + paramInt1;
    if (paramInt3 > paramInt5)
      paramLayoutParams.x -= paramInt3 - paramInt5; 
    boolean bool2 = bool1;
    if (paramLayoutParams.x < paramInt4) {
      paramLayoutParams.x = paramInt4;
      paramInt3 = paramInt5 - paramInt4;
      if (paramBoolean && paramInt1 > paramInt3) {
        paramLayoutParams.width = paramInt3;
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
    } 
    paramLayoutParams.x -= paramInt2;
    return bool2;
  }
  
  public int getMaxAvailableHeight(View paramView) {
    return getMaxAvailableHeight(paramView, 0);
  }
  
  public int getMaxAvailableHeight(View paramView, int paramInt) {
    return getMaxAvailableHeight(paramView, paramInt, false);
  }
  
  public int getMaxAvailableHeight(View paramView, int paramInt, boolean paramBoolean) {
    Rect rect = new Rect();
    View view = getAppRootView(paramView);
    view.getWindowVisibleDisplayFrame(rect);
    if (paramBoolean) {
      Rect rect1 = new Rect();
      paramView.getWindowDisplayFrame(rect1);
      rect1.top = rect.top;
      rect1.right = rect.right;
      rect1.left = rect.left;
      rect = rect1;
    } 
    int[] arrayOfInt = this.mTmpDrawingLocation;
    paramView.getLocationOnScreen(arrayOfInt);
    int i = rect.bottom;
    if (this.mOverlapAnchor) {
      i = i - arrayOfInt[1] - paramInt;
    } else {
      i = i - arrayOfInt[1] + paramView.getHeight() - paramInt;
    } 
    int j = arrayOfInt[1], k = rect.top;
    i = Math.max(i, j - k + paramInt);
    Drawable drawable = this.mBackground;
    paramInt = i;
    if (drawable != null) {
      drawable.getPadding(this.mTempRect);
      paramInt = i - this.mTempRect.top + this.mTempRect.bottom;
    } 
    return paramInt;
  }
  
  public void dismiss() {
    if (!isShowing() || isTransitioningToDismiss())
      return; 
    PopupDecorView popupDecorView = this.mDecorView;
    View view = this.mContentView;
    ViewParent viewParent = view.getParent();
    if (viewParent instanceof ViewGroup) {
      viewParent = viewParent;
    } else {
      viewParent = null;
    } 
    popupDecorView.cancelTransitions();
    this.mIsShowing = false;
    this.mIsTransitioningToDismiss = true;
    Transition transition = this.mExitTransition;
    if (transition != null && popupDecorView.isLaidOut() && (this.mIsAnchorRootAttached || this.mAnchorRoot == null)) {
      WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)popupDecorView.getLayoutParams();
      layoutParams.flags |= 0x10;
      layoutParams.flags |= 0x8;
      layoutParams.flags &= 0xFFFDFFFF;
      this.mWindowManager.updateViewLayout(popupDecorView, layoutParams);
      WeakReference<View> weakReference = this.mAnchorRoot;
      if (weakReference != null) {
        View view1 = weakReference.get();
      } else {
        weakReference = null;
      } 
      Rect rect = getTransitionEpicenter();
      popupDecorView.startExitTransition(transition, (View)weakReference, rect, (Transition.TransitionListener)new Object(this, popupDecorView, (ViewGroup)viewParent, view));
    } else {
      dismissImmediate(popupDecorView, (ViewGroup)viewParent, view);
    } 
    detachFromAnchor();
    OnDismissListener onDismissListener = this.mOnDismissListener;
    if (onDismissListener != null)
      onDismissListener.onDismiss(); 
  }
  
  protected final Rect getTransitionEpicenter() {
    WeakReference<View> weakReference = this.mAnchor;
    if (weakReference != null) {
      View view = weakReference.get();
    } else {
      weakReference = null;
    } 
    PopupDecorView popupDecorView = this.mDecorView;
    if (weakReference == null || popupDecorView == null)
      return null; 
    int[] arrayOfInt1 = weakReference.getLocationOnScreen();
    int[] arrayOfInt2 = this.mDecorView.getLocationOnScreen();
    Rect rect = new Rect(0, 0, weakReference.getWidth(), weakReference.getHeight());
    rect.offset(arrayOfInt1[0] - arrayOfInt2[0], arrayOfInt1[1] - arrayOfInt2[1]);
    if (this.mEpicenterBounds != null) {
      int i = rect.left;
      int j = rect.top;
      rect.set(this.mEpicenterBounds);
      rect.offset(i, j);
    } 
    return rect;
  }
  
  private void dismissImmediate(View paramView1, ViewGroup paramViewGroup, View paramView2) {
    if (paramView1.getParent() != null)
      this.mWindowManager.removeViewImmediate(paramView1); 
    if (paramViewGroup != null)
      paramViewGroup.removeView(paramView2); 
    this.mDecorView = null;
    this.mBackgroundView = null;
    this.mIsTransitioningToDismiss = false;
  }
  
  public void setOnDismissListener(OnDismissListener paramOnDismissListener) {
    this.mOnDismissListener = paramOnDismissListener;
  }
  
  protected final OnDismissListener getOnDismissListener() {
    return this.mOnDismissListener;
  }
  
  public void update() {
    if (!isShowing() || !hasContentView())
      return; 
    WindowManager.LayoutParams layoutParams = getDecorViewLayoutParams();
    boolean bool = false;
    int i = computeAnimationResource();
    if (i != layoutParams.windowAnimations) {
      layoutParams.windowAnimations = i;
      bool = true;
    } 
    i = computeFlags(layoutParams.flags);
    if (i != layoutParams.flags) {
      layoutParams.flags = i;
      bool = true;
    } 
    i = computeGravity();
    if (i != layoutParams.gravity) {
      layoutParams.gravity = i;
      bool = true;
    } 
    if (bool) {
      WeakReference<View> weakReference = this.mAnchor;
      if (weakReference != null) {
        View view = weakReference.get();
      } else {
        weakReference = null;
      } 
      update((View)weakReference, layoutParams);
    } 
  }
  
  protected void update(View paramView, WindowManager.LayoutParams paramLayoutParams) {
    setLayoutDirectionFromAnchor();
    this.mWindowManager.updateViewLayout(this.mDecorView, paramLayoutParams);
  }
  
  public void update(int paramInt1, int paramInt2) {
    WindowManager.LayoutParams layoutParams = getDecorViewLayoutParams();
    update(layoutParams.x, layoutParams.y, paramInt1, paramInt2, false);
  }
  
  public void update(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    update(paramInt1, paramInt2, paramInt3, paramInt4, false);
  }
  
  public void update(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean) {
    if (paramInt3 >= 0) {
      this.mLastWidth = paramInt3;
      setWidth(paramInt3);
    } 
    if (paramInt4 >= 0) {
      this.mLastHeight = paramInt4;
      setHeight(paramInt4);
    } 
    if (!isShowing() || !hasContentView())
      return; 
    WindowManager.LayoutParams layoutParams = getDecorViewLayoutParams();
    boolean bool = paramBoolean;
    int i = this.mWidthMode;
    if (i >= 0)
      i = this.mLastWidth; 
    paramBoolean = bool;
    if (paramInt3 != -1) {
      paramBoolean = bool;
      if (layoutParams.width != i) {
        this.mLastWidth = i;
        layoutParams.width = i;
        paramBoolean = true;
      } 
    } 
    paramInt3 = this.mHeightMode;
    if (paramInt3 >= 0)
      paramInt3 = this.mLastHeight; 
    bool = paramBoolean;
    if (paramInt4 != -1) {
      bool = paramBoolean;
      if (layoutParams.height != paramInt3) {
        this.mLastHeight = paramInt3;
        layoutParams.height = paramInt3;
        bool = true;
      } 
    } 
    paramBoolean = bool;
    if (layoutParams.x != paramInt1) {
      layoutParams.x = paramInt1;
      paramBoolean = true;
    } 
    if (layoutParams.y != paramInt2) {
      layoutParams.y = paramInt2;
      paramBoolean = true;
    } 
    paramInt1 = computeAnimationResource();
    if (paramInt1 != layoutParams.windowAnimations) {
      layoutParams.windowAnimations = paramInt1;
      paramBoolean = true;
    } 
    paramInt1 = computeFlags(layoutParams.flags);
    if (paramInt1 != layoutParams.flags) {
      layoutParams.flags = paramInt1;
      paramBoolean = true;
    } 
    paramInt1 = computeGravity();
    if (paramInt1 != layoutParams.gravity) {
      layoutParams.gravity = paramInt1;
      paramBoolean = true;
    } 
    View view1 = null;
    paramInt2 = -1;
    WeakReference<View> weakReference = this.mAnchor;
    View view2 = view1;
    paramInt1 = paramInt2;
    if (weakReference != null) {
      view2 = view1;
      paramInt1 = paramInt2;
      if (weakReference.get() != null) {
        view2 = this.mAnchor.get();
        paramInt1 = view2.getAccessibilityViewId();
      } 
    } 
    if (paramInt1 != layoutParams.accessibilityIdOfAnchor) {
      layoutParams.accessibilityIdOfAnchor = paramInt1;
      paramBoolean = true;
    } 
    if (paramBoolean)
      update(view2, layoutParams); 
  }
  
  protected boolean hasContentView() {
    boolean bool;
    if (this.mContentView != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected boolean hasDecorView() {
    boolean bool;
    if (this.mDecorView != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected WindowManager.LayoutParams getDecorViewLayoutParams() {
    return (WindowManager.LayoutParams)this.mDecorView.getLayoutParams();
  }
  
  public void update(View paramView, int paramInt1, int paramInt2) {
    update(paramView, false, 0, 0, paramInt1, paramInt2);
  }
  
  public void update(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    update(paramView, true, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  private void update(View paramView, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!isShowing() || !hasContentView())
      return; 
    WeakReference<View> weakReference = this.mAnchor;
    int i = this.mAnchoredGravity;
    if (paramBoolean && (this.mAnchorXoff != paramInt1 || this.mAnchorYoff != paramInt2)) {
      j = 1;
    } else {
      j = 0;
    } 
    if (weakReference == null || weakReference.get() != paramView || (j && !this.mIsDropdown)) {
      attachToAnchor(paramView, paramInt1, paramInt2, i);
    } else if (j) {
      this.mAnchorXoff = paramInt1;
      this.mAnchorYoff = paramInt2;
    } 
    WindowManager.LayoutParams layoutParams = getDecorViewLayoutParams();
    int k = layoutParams.gravity;
    int m = layoutParams.width;
    int n = layoutParams.height;
    int j = layoutParams.x;
    int i1 = layoutParams.y;
    if (paramInt3 < 0) {
      paramInt2 = this.mWidth;
    } else {
      paramInt2 = paramInt3;
    } 
    if (paramInt4 < 0) {
      paramInt1 = this.mHeight;
    } else {
      paramInt1 = paramInt4;
    } 
    paramBoolean = findDropDownPosition(paramView, layoutParams, this.mAnchorXoff, this.mAnchorYoff, paramInt2, paramInt1, i, this.mAllowScrollingAnchorParent);
    updateAboveAnchor(paramBoolean);
    if (k != layoutParams.gravity || j != layoutParams.x || i1 != layoutParams.y || m != layoutParams.width || n != layoutParams.height) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    if (paramInt2 >= 0)
      paramInt2 = layoutParams.width; 
    if (paramInt1 >= 0)
      paramInt1 = layoutParams.height; 
    update(layoutParams.x, layoutParams.y, paramInt2, paramInt1, paramBoolean);
  }
  
  protected void detachFromAnchor() {
    View view = getAnchor();
    if (view != null) {
      ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
      viewTreeObserver.removeOnScrollChangedListener(this.mOnScrollChangedListener);
      view.removeOnAttachStateChangeListener(this.mOnAnchorDetachedListener);
    } 
    WeakReference<View> weakReference = this.mAnchorRoot;
    if (weakReference != null) {
      View view1 = weakReference.get();
    } else {
      weakReference = null;
    } 
    if (weakReference != null) {
      weakReference.removeOnAttachStateChangeListener(this.mOnAnchorRootDetachedListener);
      weakReference.removeOnLayoutChangeListener(this.mOnLayoutChangeListener);
    } 
    this.mAnchor = null;
    this.mAnchorRoot = null;
    this.mIsAnchorRootAttached = false;
  }
  
  protected void attachToAnchor(View paramView, int paramInt1, int paramInt2, int paramInt3) {
    detachFromAnchor();
    ViewTreeObserver viewTreeObserver = paramView.getViewTreeObserver();
    if (viewTreeObserver != null)
      viewTreeObserver.addOnScrollChangedListener(this.mOnScrollChangedListener); 
    paramView.addOnAttachStateChangeListener(this.mOnAnchorDetachedListener);
    View view = paramView.getRootView();
    view.addOnAttachStateChangeListener(this.mOnAnchorRootDetachedListener);
    view.addOnLayoutChangeListener(this.mOnLayoutChangeListener);
    this.mAnchor = new WeakReference<>(paramView);
    this.mAnchorRoot = new WeakReference<>(view);
    this.mIsAnchorRootAttached = view.isAttachedToWindow();
    this.mParentRootView = this.mAnchorRoot;
    this.mAnchorXoff = paramInt1;
    this.mAnchorYoff = paramInt2;
    this.mAnchoredGravity = paramInt3;
  }
  
  protected View getAnchor() {
    WeakReference<View> weakReference = this.mAnchor;
    if (weakReference != null) {
      View view = weakReference.get();
    } else {
      weakReference = null;
    } 
    return (View)weakReference;
  }
  
  private void alignToAnchor() {
    WeakReference<View> weakReference = this.mAnchor;
    if (weakReference != null) {
      View view = weakReference.get();
    } else {
      weakReference = null;
    } 
    if (weakReference != null && weakReference.isAttachedToWindow() && hasDecorView()) {
      WindowManager.LayoutParams layoutParams = getDecorViewLayoutParams();
      updateAboveAnchor(findDropDownPosition((View)weakReference, layoutParams, this.mAnchorXoff, this.mAnchorYoff, layoutParams.width, layoutParams.height, this.mAnchoredGravity, false));
      update(layoutParams.x, layoutParams.y, -1, -1, true);
    } 
  }
  
  private View getAppRootView(View paramView) {
    WindowManagerGlobal windowManagerGlobal = WindowManagerGlobal.getInstance();
    IBinder iBinder = paramView.getApplicationWindowToken();
    View view = windowManagerGlobal.getWindowView(iBinder);
    if (view != null)
      return view; 
    return paramView.getRootView();
  }
  
  class PopupDecorView extends FrameLayout {
    private Runnable mCleanupAfterExit;
    
    private final View.OnAttachStateChangeListener mOnAnchorRootDetachedListener;
    
    final PopupWindow this$0;
    
    public PopupDecorView(Context param1Context) {
      super(param1Context);
      this.mOnAnchorRootDetachedListener = (View.OnAttachStateChangeListener)new Object(this);
    }
    
    public boolean dispatchKeyEvent(KeyEvent param1KeyEvent) {
      if (param1KeyEvent.getKeyCode() == 4) {
        if (getKeyDispatcherState() == null)
          return super.dispatchKeyEvent(param1KeyEvent); 
        if (param1KeyEvent.getAction() == 0 && param1KeyEvent.getRepeatCount() == 0) {
          KeyEvent.DispatcherState dispatcherState = getKeyDispatcherState();
          if (dispatcherState != null)
            dispatcherState.startTracking(param1KeyEvent, this); 
          return true;
        } 
        if (param1KeyEvent.getAction() == 1) {
          KeyEvent.DispatcherState dispatcherState = getKeyDispatcherState();
          if (dispatcherState != null && dispatcherState.isTracking(param1KeyEvent) && !param1KeyEvent.isCanceled()) {
            PopupWindow.this.dismiss();
            return true;
          } 
        } 
        return super.dispatchKeyEvent(param1KeyEvent);
      } 
      return super.dispatchKeyEvent(param1KeyEvent);
    }
    
    public boolean dispatchTouchEvent(MotionEvent param1MotionEvent) {
      if (PopupWindow.this.mTouchInterceptor != null && PopupWindow.this.mTouchInterceptor.onTouch(this, param1MotionEvent))
        return true; 
      return super.dispatchTouchEvent(param1MotionEvent);
    }
    
    public boolean onTouchEvent(MotionEvent param1MotionEvent) {
      int i = (int)param1MotionEvent.getX();
      int j = (int)param1MotionEvent.getY();
      if (param1MotionEvent.getAction() == 0 && (i < 0 || i >= getWidth() || j < 0 || j >= getHeight())) {
        PopupWindow.this.dismiss();
        return true;
      } 
      if (param1MotionEvent.getAction() == 4) {
        PopupWindow.this.dismiss();
        return true;
      } 
      return super.onTouchEvent(param1MotionEvent);
    }
    
    public void requestEnterTransition(Transition param1Transition) {
      ViewTreeObserver viewTreeObserver = getViewTreeObserver();
      if (viewTreeObserver != null && param1Transition != null) {
        param1Transition = param1Transition.clone();
        viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)new Object(this, param1Transition));
      } 
    }
    
    private void startEnterTransition(Transition param1Transition) {
      int i = getChildCount();
      byte b;
      for (b = 0; b < i; b++) {
        View view = getChildAt(b);
        param1Transition.addTarget(view);
        view.setTransitionVisibility(4);
      } 
      TransitionManager.beginDelayedTransition(this, param1Transition);
      for (b = 0; b < i; b++) {
        View view = getChildAt(b);
        view.setTransitionVisibility(0);
      } 
    }
    
    public void startExitTransition(Transition param1Transition, View param1View, Rect param1Rect, Transition.TransitionListener param1TransitionListener) {
      if (param1Transition == null)
        return; 
      if (param1View != null)
        param1View.addOnAttachStateChangeListener(this.mOnAnchorRootDetachedListener); 
      this.mCleanupAfterExit = new _$$Lambda$PopupWindow$PopupDecorView$T99WKEnQefOCXbbKvW95WY38p_I(this, param1TransitionListener, param1Transition, param1View);
      param1Transition = param1Transition.clone();
      param1Transition.addListener((Transition.TransitionListener)new Object(this));
      param1Transition.setEpicenterCallback((Transition.EpicenterCallback)new Object(this, param1Rect));
      int i = getChildCount();
      byte b;
      for (b = 0; b < i; b++) {
        param1View = getChildAt(b);
        param1Transition.addTarget(param1View);
      } 
      TransitionManager.beginDelayedTransition(this, param1Transition);
      for (b = 0; b < i; b++) {
        View view = getChildAt(b);
        view.setVisibility(4);
      } 
    }
    
    public void cancelTransitions() {
      TransitionManager.endTransitions(this);
      Runnable runnable = this.mCleanupAfterExit;
      if (runnable != null)
        runnable.run(); 
    }
    
    public void requestKeyboardShortcuts(List<KeyboardShortcutGroup> param1List, int param1Int) {
      if (PopupWindow.this.mParentRootView != null) {
        View view = PopupWindow.this.mParentRootView.get();
        if (view != null)
          view.requestKeyboardShortcuts(param1List, param1Int); 
      } 
    }
  }
  
  class PopupBackgroundView extends FrameLayout {
    final PopupWindow this$0;
    
    public PopupBackgroundView(Context param1Context) {
      super(param1Context);
    }
    
    protected int[] onCreateDrawableState(int param1Int) {
      if (PopupWindow.this.mAboveAnchor) {
        int[] arrayOfInt = super.onCreateDrawableState(param1Int + 1);
        View.mergeDrawableStates(arrayOfInt, PopupWindow.ABOVE_ANCHOR_STATE_SET);
        return arrayOfInt;
      } 
      return super.onCreateDrawableState(param1Int);
    }
  }
  
  public static interface OnDismissListener {
    void onDismiss();
  }
}
