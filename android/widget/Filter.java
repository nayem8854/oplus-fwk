package android.widget;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public abstract class Filter {
  private static final int FILTER_TOKEN = -791613427;
  
  private static final int FINISH_TOKEN = -559038737;
  
  private static final String LOG_TAG = "Filter";
  
  private static final String THREAD_NAME = "Filter";
  
  private Delayer mDelayer;
  
  private final Object mLock = new Object();
  
  private Handler mResultHandler;
  
  private Handler mThreadHandler;
  
  public Filter() {
    this.mResultHandler = new ResultsHandler();
  }
  
  public void setDelayer(Delayer paramDelayer) {
    synchronized (this.mLock) {
      this.mDelayer = paramDelayer;
      return;
    } 
  }
  
  public final void filter(CharSequence paramCharSequence) {
    filter(paramCharSequence, null);
  }
  
  public final void filter(CharSequence paramCharSequence, FilterListener paramFilterListener) {
    synchronized (this.mLock) {
      long l;
      if (this.mThreadHandler == null) {
        HandlerThread handlerThread = new HandlerThread();
        this("Filter", 10);
        handlerThread.start();
        RequestHandler requestHandler = new RequestHandler();
        this(this, handlerThread.getLooper());
        this.mThreadHandler = requestHandler;
      } 
      if (this.mDelayer == null) {
        l = 0L;
      } else {
        l = this.mDelayer.getPostingDelay(paramCharSequence);
      } 
      Message message = this.mThreadHandler.obtainMessage(-791613427);
      RequestArguments requestArguments = new RequestArguments();
      String str = null;
      this();
      if (paramCharSequence != null)
        str = paramCharSequence.toString(); 
      requestArguments.constraint = str;
      requestArguments.listener = paramFilterListener;
      message.obj = requestArguments;
      this.mThreadHandler.removeMessages(-791613427);
      this.mThreadHandler.removeMessages(-559038737);
      this.mThreadHandler.sendMessageDelayed(message, l);
      return;
    } 
  }
  
  public CharSequence convertResultToString(Object paramObject) {
    if (paramObject == null) {
      paramObject = "";
    } else {
      paramObject = paramObject.toString();
    } 
    return (CharSequence)paramObject;
  }
  
  protected abstract FilterResults performFiltering(CharSequence paramCharSequence);
  
  protected abstract void publishResults(CharSequence paramCharSequence, FilterResults paramFilterResults);
  
  protected static class FilterResults {
    public int count;
    
    public Object values;
  }
  
  class RequestHandler extends Handler {
    final Filter this$0;
    
    public RequestHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != -791613427) {
        if (i == -559038737)
          synchronized (Filter.this.mLock) {
            if (Filter.this.mThreadHandler != null) {
              Filter.this.mThreadHandler.getLooper().quit();
              Filter.access$402(Filter.this, null);
            } 
          }  
      } else {
        Filter.RequestArguments requestArguments = (Filter.RequestArguments)param1Message.obj;
        try {
          requestArguments.results = Filter.this.performFiltering(requestArguments.constraint);
          Message message = Filter.this.mResultHandler.obtainMessage(i);
          message.obj = requestArguments;
          message.sendToTarget();
        } catch (Exception exception) {
          Filter.FilterResults filterResults = new Filter.FilterResults();
          this();
          requestArguments.results = filterResults;
          Log.w("Filter", "An exception occured during performFiltering()!", exception);
          Message message = Filter.this.mResultHandler.obtainMessage(i);
          message.obj = requestArguments;
          message.sendToTarget();
        } finally {
          Exception exception;
        } 
        synchronized (Filter.this.mLock) {
          if (Filter.this.mThreadHandler != null) {
            Message message = Filter.this.mThreadHandler.obtainMessage(-559038737);
            Filter.this.mThreadHandler.sendMessageDelayed(message, 3000L);
          } 
          return;
        } 
      } 
    }
  }
  
  class ResultsHandler extends Handler {
    final Filter this$0;
    
    private ResultsHandler() {}
    
    public void handleMessage(Message param1Message) {
      Filter.RequestArguments requestArguments = (Filter.RequestArguments)param1Message.obj;
      Filter.this.publishResults(requestArguments.constraint, requestArguments.results);
      if (requestArguments.listener != null) {
        byte b;
        if (requestArguments.results != null) {
          b = requestArguments.results.count;
        } else {
          b = -1;
        } 
        requestArguments.listener.onFilterComplete(b);
      } 
    }
  }
  
  private static class RequestArguments {
    CharSequence constraint;
    
    Filter.FilterListener listener;
    
    Filter.FilterResults results;
    
    private RequestArguments() {}
  }
  
  public static interface Delayer {
    long getPostingDelay(CharSequence param1CharSequence);
  }
  
  public static interface FilterListener {
    void onFilterComplete(int param1Int);
  }
}
