package android.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.IntProperty;
import android.util.MathUtils;
import android.util.Property;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import com.android.internal.R;

class ColorFastScroller extends FastScroller {
  private static final long TAP_TIMEOUT = ViewConfiguration.getTapTimeout();
  
  private final Rect mTempBounds = new Rect();
  
  private final Rect mTempMargins = new Rect();
  
  private final Rect mContainerRect = new Rect();
  
  private final int[] mPreviewResId = new int[2];
  
  private int mCurrentSection = -1;
  
  private int mScrollbarPosition = -1;
  
  private long mPendingDrag = -1L;
  
  private final Runnable mDeferHide = new Runnable() {
      final ColorFastScroller this$0;
      
      public void run() {
        ColorFastScroller.this.setState(3);
      }
    };
  
  private final Animator.AnimatorListener mSwitchPrimaryListener = (Animator.AnimatorListener)new AnimatorListenerAdapter() {
      final ColorFastScroller this$0;
      
      public void onAnimationEnd(Animator param1Animator) {
        ColorFastScroller colorFastScroller = ColorFastScroller.this;
        ColorFastScroller.access$102(colorFastScroller, colorFastScroller.mShowingPrimary ^ true);
      }
    };
  
  private static Property<View, Integer> BOTTOM;
  
  private static final int COLOR_MIN_PAGES = 3;
  
  private static final int COLOR_STATE_HIDE = 3;
  
  private static final int DURATION_CROSS_FADE = 50;
  
  private static final int DURATION_FADE_IN = 150;
  
  private static final int DURATION_FADE_OUT = 300;
  
  private static final int DURATION_RESIZE = 100;
  
  private static final long FADE_TIMEOUT = 1500L;
  
  private static Property<View, Integer> LEFT;
  
  private static final int MIN_PAGES = 4;
  
  private static final int OVERLAY_ABOVE_THUMB = 2;
  
  private static final int OVERLAY_AT_THUMB = 1;
  
  private static final int OVERLAY_FLOATING = 0;
  
  private static final int PREVIEW_LEFT = 0;
  
  private static final int PREVIEW_RIGHT = 1;
  
  private static Property<View, Integer> RIGHT;
  
  private static final int STATE_DRAGGING = 2;
  
  private static final int STATE_NONE = 0;
  
  private static final int STATE_VISIBLE = 1;
  
  private static final int THUMB_POSITION_INSIDE = 1;
  
  private static final int THUMB_POSITION_MIDPOINT = 0;
  
  private static Property<View, Integer> TOP;
  
  private boolean mAlwaysShow;
  
  private AnimatorSet mDecorAnimation;
  
  private boolean mEnabled;
  
  private int mFirstVisibleItem;
  
  private int mHeaderCount;
  
  private float mInitialTouchY;
  
  private boolean mLayoutFromRight;
  
  private final AbsListView mList;
  
  private Adapter mListAdapter;
  
  private boolean mLongList;
  
  private boolean mMatchDragPosition;
  
  private final int mMinimumTouchTarget;
  
  private int mOldChildCount;
  
  private int mOldItemCount;
  
  private final ViewGroupOverlay mOverlay;
  
  private int mOverlayPosition;
  
  private AnimatorSet mPreviewAnimation;
  
  private final View mPreviewImage;
  
  private int mPreviewMinHeight;
  
  private int mPreviewMinWidth;
  
  private int mPreviewPadding;
  
  private final TextView mPrimaryText;
  
  private int mScaledTouchSlop;
  
  private int mScrollBarStyle;
  
  private boolean mScrollCompleted;
  
  private final TextView mSecondaryText;
  
  private SectionIndexer mSectionIndexer;
  
  private Object[] mSections;
  
  private boolean mShowingPreview;
  
  private boolean mShowingPrimary;
  
  private int mState;
  
  private int mTextAppearance;
  
  private ColorStateList mTextColor;
  
  private float mTextSize;
  
  private Drawable mThumbDrawable;
  
  private final ImageView mThumbImage;
  
  private int mThumbMinHeight;
  
  private int mThumbMinWidth;
  
  private float mThumbOffset;
  
  private int mThumbPosition;
  
  private float mThumbRange;
  
  private Drawable mTrackDrawable;
  
  private final ImageView mTrackImage;
  
  private boolean mUpdatingLayout;
  
  private int mWidth;
  
  public ColorFastScroller(AbsListView paramAbsListView, int paramInt) {
    super(paramAbsListView, paramInt);
    this.mList = paramAbsListView;
    this.mOldItemCount = paramAbsListView.getCount();
    this.mOldChildCount = paramAbsListView.getChildCount();
    Context context = paramAbsListView.getContext();
    this.mScaledTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    this.mScrollBarStyle = paramAbsListView.getScrollBarStyle();
    boolean bool = true;
    this.mScrollCompleted = true;
    this.mState = 1;
    if ((context.getApplicationInfo()).targetSdkVersion < 11)
      bool = false; 
    this.mMatchDragPosition = bool;
    ImageView imageView = new ImageView(context);
    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
    this.mThumbImage = imageView = new ImageView(context);
    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
    this.mPreviewImage = new View(context);
    this.mThumbImage.setAlpha(0.0F);
    this.mPreviewImage.setAlpha(0.0F);
    this.mPrimaryText = createPreviewTextView(context);
    this.mSecondaryText = createPreviewTextView(context);
    this.mMinimumTouchTarget = paramAbsListView.getResources().getDimensionPixelSize(17105185);
    setStyle(paramInt);
    ViewGroupOverlay viewGroupOverlay = paramAbsListView.getOverlay();
    this.mOverlay = viewGroupOverlay;
    viewGroupOverlay.add((View)this.mTrackImage);
    viewGroupOverlay.add((View)this.mThumbImage);
    viewGroupOverlay.add(this.mPreviewImage);
    viewGroupOverlay.add((View)this.mPrimaryText);
    viewGroupOverlay.add((View)this.mSecondaryText);
    getSectionsFromIndexer();
    updateLongList(this.mOldChildCount, this.mOldItemCount);
    setScrollbarPosition(paramAbsListView.getVerticalScrollbarPosition());
    postAutoHide();
  }
  
  private void updateAppearance() {
    int i = 0;
    this.mTrackImage.setImageDrawable(this.mTrackDrawable);
    Drawable drawable = this.mTrackDrawable;
    if (drawable != null)
      i = Math.max(0, drawable.getIntrinsicWidth()); 
    this.mThumbImage.setImageDrawable(this.mThumbDrawable);
    this.mThumbImage.setMinimumWidth(this.mThumbMinWidth);
    this.mThumbImage.setMinimumHeight(this.mThumbMinHeight);
    drawable = this.mThumbDrawable;
    int j = i;
    if (drawable != null)
      j = Math.max(i, drawable.getIntrinsicWidth()); 
    this.mWidth = Math.max(j, this.mThumbMinWidth);
    i = this.mTextAppearance;
    if (i != 0) {
      this.mPrimaryText.setTextAppearance(i);
      this.mSecondaryText.setTextAppearance(this.mTextAppearance);
    } 
    ColorStateList colorStateList = this.mTextColor;
    if (colorStateList != null) {
      this.mPrimaryText.setTextColor(colorStateList);
      this.mSecondaryText.setTextColor(this.mTextColor);
    } 
    float f = this.mTextSize;
    if (f > 0.0F) {
      this.mPrimaryText.setTextSize(0, f);
      this.mSecondaryText.setTextSize(0, this.mTextSize);
    } 
    i = this.mPreviewPadding;
    this.mPrimaryText.setIncludeFontPadding(false);
    this.mPrimaryText.setPadding(i, i, i, i);
    this.mSecondaryText.setIncludeFontPadding(false);
    this.mSecondaryText.setPadding(i, i, i, i);
    refreshDrawablePressedState();
  }
  
  public void setStyle(int paramInt) {
    AbsListView absListView = this.mList;
    if (absListView == null)
      return; 
    Context context = absListView.getContext();
    TypedArray typedArray = context.obtainStyledAttributes(null, R.styleable.FastScroll, 16843767, paramInt);
    int i = typedArray.getIndexCount();
    for (paramInt = 0; paramInt < i; paramInt++) {
      int j = typedArray.getIndex(paramInt);
      switch (j) {
        case 13:
          this.mTrackDrawable = typedArray.getDrawable(j);
          break;
        case 12:
          this.mThumbMinWidth = typedArray.getDimensionPixelSize(j, 0);
          break;
        case 11:
          this.mThumbMinHeight = typedArray.getDimensionPixelSize(j, 0);
          break;
        case 10:
          this.mThumbDrawable = typedArray.getDrawable(j);
          break;
        case 9:
          this.mOverlayPosition = typedArray.getInt(j, 0);
          break;
        case 8:
          this.mPreviewResId[1] = typedArray.getResourceId(j, 0);
          break;
        case 7:
          this.mPreviewResId[0] = typedArray.getResourceId(j, 0);
          break;
        case 6:
          this.mThumbPosition = typedArray.getInt(j, 0);
          break;
        case 5:
          this.mPreviewMinHeight = typedArray.getDimensionPixelSize(j, 0);
          break;
        case 4:
          this.mPreviewMinWidth = typedArray.getDimensionPixelSize(j, 0);
          break;
        case 3:
          this.mPreviewPadding = typedArray.getDimensionPixelSize(j, 0);
          break;
        case 2:
          this.mTextColor = typedArray.getColorStateList(j);
          break;
        case 1:
          this.mTextSize = typedArray.getDimensionPixelSize(j, 0);
          break;
        case 0:
          this.mTextAppearance = typedArray.getResourceId(j, 0);
          break;
      } 
    } 
    typedArray.recycle();
    updateAppearance();
  }
  
  public void remove() {
    this.mOverlay.remove((View)this.mTrackImage);
    this.mOverlay.remove((View)this.mThumbImage);
    this.mOverlay.remove(this.mPreviewImage);
    this.mOverlay.remove((View)this.mPrimaryText);
    this.mOverlay.remove((View)this.mSecondaryText);
  }
  
  public void setEnabled(boolean paramBoolean) {
    if (this.mEnabled != paramBoolean) {
      this.mEnabled = paramBoolean;
      onStateDependencyChanged(true);
    } 
  }
  
  public boolean isEnabled() {
    boolean bool;
    if (this.mEnabled && (this.mLongList || this.mAlwaysShow)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setAlwaysShow(boolean paramBoolean) {
    if (this.mAlwaysShow != paramBoolean) {
      this.mAlwaysShow = paramBoolean;
      onStateDependencyChanged(false);
    } 
  }
  
  public boolean isAlwaysShowEnabled() {
    return this.mAlwaysShow;
  }
  
  private void onStateDependencyChanged(boolean paramBoolean) {
    if (isEnabled()) {
      if (isAlwaysShowEnabled()) {
        setState(1);
      } else if (this.mState == 1) {
        postAutoHide();
      } else if (paramBoolean) {
        setState(1);
        postAutoHide();
      } 
    } else {
      stop();
    } 
    this.mList.resolvePadding();
  }
  
  public void setScrollBarStyle(int paramInt) {
    if (this.mScrollBarStyle != paramInt) {
      this.mScrollBarStyle = paramInt;
      updateLayout();
    } 
  }
  
  public void stop() {
    setState(0);
  }
  
  public void setScrollbarPosition(int paramInt) {
    AbsListView absListView = this.mList;
    if (absListView == null)
      return; 
    int i = 1, j = paramInt;
    if (paramInt == 0) {
      if (absListView.isLayoutRtl()) {
        paramInt = 1;
      } else {
        paramInt = 2;
      } 
      j = paramInt;
    } 
    if (this.mScrollbarPosition != j) {
      this.mScrollbarPosition = j;
      if (j != 1) {
        paramInt = i;
      } else {
        paramInt = 0;
      } 
      this.mLayoutFromRight = paramInt;
      paramInt = this.mPreviewResId[paramInt];
      this.mPreviewImage.setBackgroundResource(paramInt);
      j = this.mPreviewMinWidth;
      i = this.mPreviewImage.getPaddingLeft();
      View view = this.mPreviewImage;
      paramInt = view.getPaddingRight();
      paramInt = Math.max(0, j - i - paramInt);
      this.mPrimaryText.setMinimumWidth(paramInt);
      this.mSecondaryText.setMinimumWidth(paramInt);
      i = this.mPreviewMinHeight;
      j = this.mPreviewImage.getPaddingTop();
      view = this.mPreviewImage;
      paramInt = view.getPaddingBottom();
      paramInt = Math.max(0, i - j - paramInt);
      this.mPrimaryText.setMinimumHeight(paramInt);
      this.mSecondaryText.setMinimumHeight(paramInt);
      updateLayout();
    } 
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    updateLayout();
  }
  
  public void onItemCountChanged(int paramInt1, int paramInt2) {
    if (this.mOldItemCount != paramInt2) {
      int i;
      this.mOldItemCount = paramInt2;
      this.mOldChildCount = paramInt1;
      if (paramInt2 - paramInt1 > 0) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i && this.mState != 2) {
        i = this.mList.getFirstVisiblePosition();
        setThumbPos(getPosFromItemCount(i, paramInt1, paramInt2));
      } 
      updateLongList(paramInt1, paramInt2);
    } 
  }
  
  private void updateLongList(int paramInt1, int paramInt2) {
    boolean bool;
    if (paramInt1 > 0 && paramInt2 / paramInt1 >= 3) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mLongList != bool) {
      this.mLongList = bool;
      onStateDependencyChanged(false);
    } 
  }
  
  private TextView createPreviewTextView(Context paramContext) {
    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -2);
    TextView textView = new TextView(paramContext);
    textView.setLayoutParams(layoutParams);
    textView.setSingleLine(true);
    textView.setEllipsize(TextUtils.TruncateAt.MIDDLE);
    textView.setGravity(17);
    textView.setAlpha(0.0F);
    textView.setLayoutDirection(this.mList.getLayoutDirection());
    return textView;
  }
  
  public void updateLayout() {
    if (this.mUpdatingLayout)
      return; 
    this.mUpdatingLayout = true;
    updateContainerRect();
    layoutThumb();
    layoutTrack();
    updateOffsetAndRange();
    Rect rect = this.mTempBounds;
    measurePreview((View)this.mPrimaryText, rect);
    applyLayout((View)this.mPrimaryText, rect);
    measurePreview((View)this.mSecondaryText, rect);
    applyLayout((View)this.mSecondaryText, rect);
    if (this.mPreviewImage != null) {
      rect.left -= this.mPreviewImage.getPaddingLeft();
      rect.top -= this.mPreviewImage.getPaddingTop();
      rect.right += this.mPreviewImage.getPaddingRight();
      rect.bottom += this.mPreviewImage.getPaddingBottom();
      applyLayout(this.mPreviewImage, rect);
    } 
    this.mUpdatingLayout = false;
  }
  
  private void applyLayout(View paramView, Rect paramRect) {
    float f;
    paramView.layout(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    if (this.mLayoutFromRight) {
      f = (paramRect.right - paramRect.left);
    } else {
      f = 0.0F;
    } 
    paramView.setPivotX(f);
  }
  
  private void measurePreview(View paramView, Rect paramRect) {
    Rect rect = this.mTempMargins;
    View view = this.mPreviewImage;
    if (view != null) {
      rect.left = view.getPaddingLeft();
      rect.top = this.mPreviewImage.getPaddingTop();
      rect.right = this.mPreviewImage.getPaddingRight();
      rect.bottom = this.mPreviewImage.getPaddingBottom();
    } 
    if (this.mOverlayPosition == 0) {
      measureFloating(paramView, rect, paramRect);
    } else {
      measureViewToSide(paramView, (View)this.mThumbImage, rect, paramRect);
    } 
  }
  
  private void measureViewToSide(View paramView1, View paramView2, Rect paramRect1, Rect paramRect2) {
    int j, k;
    if (paramRect1 == null) {
      i = 0;
      j = 0;
      k = 0;
    } else {
      i = paramRect1.left;
      j = paramRect1.top;
      k = paramRect1.right;
    } 
    paramRect1 = this.mContainerRect;
    int m = paramRect1.width();
    if (paramView2 != null)
      if (this.mLayoutFromRight) {
        m = paramView2.getLeft();
      } else {
        m -= paramView2.getRight();
      }  
    int n = Math.max(0, paramRect1.height());
    m = Math.max(0, m - i - k);
    int i1 = View.MeasureSpec.makeMeasureSpec(m, -2147483648);
    n = View.MeasureSpec.makeSafeMeasureSpec(n, 0);
    paramView1.measure(i1, n);
    n = Math.min(m, paramView1.getMeasuredWidth());
    if (this.mLayoutFromRight) {
      if (paramView2 == null) {
        m = paramRect1.right;
      } else {
        m = paramView2.getLeft();
      } 
      m -= k;
      k = m - n;
    } else {
      if (paramView2 == null) {
        m = paramRect1.left;
      } else {
        m = paramView2.getRight();
      } 
      k = m + i;
      m = k + n;
    } 
    int i = paramView1.getMeasuredHeight();
    paramRect2.set(k, j, m, j + i);
  }
  
  private void measureFloating(View paramView, Rect paramRect1, Rect paramRect2) {
    if (paramRect1 == null) {
      i = 0;
      j = 0;
      k = 0;
    } else {
      i = paramRect1.left;
      j = paramRect1.top;
      k = paramRect1.right;
    } 
    paramRect1 = this.mContainerRect;
    int m = paramRect1.width();
    int n = Math.max(0, paramRect1.height());
    int i = Math.max(0, m - i - k);
    i = View.MeasureSpec.makeMeasureSpec(i, -2147483648);
    int k = View.MeasureSpec.makeSafeMeasureSpec(n, 0);
    paramView.measure(i, k);
    k = paramRect1.height();
    i = paramView.getMeasuredWidth();
    k = k / 10 + j + paramRect1.top;
    int j = paramView.getMeasuredHeight();
    m = (m - i) / 2 + paramRect1.left;
    paramRect2.set(m, k, m + i, j + k);
  }
  
  private void updateContainerRect() {
    AbsListView absListView = this.mList;
    absListView.resolvePadding();
    Rect rect = this.mContainerRect;
    rect.left = 0;
    rect.top = 0;
    rect.right = absListView.getWidth();
    rect.bottom = absListView.getHeight();
    int i = this.mScrollBarStyle;
    if (i == 16777216 || i == 0) {
      rect.left += absListView.getPaddingLeft();
      rect.top += absListView.getPaddingTop();
      rect.right -= absListView.getPaddingRight();
      rect.bottom -= absListView.getPaddingBottom();
      if (i == 16777216) {
        i = getWidth();
        if (this.mScrollbarPosition == 2) {
          rect.right += i;
        } else {
          rect.left -= i;
        } 
      } 
    } 
  }
  
  private void layoutThumb() {
    Rect rect = this.mTempBounds;
    measureViewToSide((View)this.mThumbImage, null, null, rect);
    applyLayout((View)this.mThumbImage, rect);
  }
  
  private void layoutTrack() {
    ImageView imageView1 = this.mTrackImage;
    ImageView imageView2 = this.mThumbImage;
    Rect rect = this.mContainerRect;
    int i = Math.max(0, rect.width());
    int j = Math.max(0, rect.height());
    i = View.MeasureSpec.makeMeasureSpec(i, -2147483648);
    j = View.MeasureSpec.makeSafeMeasureSpec(j, 0);
    imageView1.measure(i, j);
    if (this.mThumbPosition == 1) {
      j = rect.top;
      i = rect.bottom;
    } else {
      int n = imageView2.getHeight() / 2;
      j = rect.top;
      i = rect.bottom;
      j += n;
      i -= n;
    } 
    int k = imageView1.getMeasuredWidth();
    int m = imageView2.getLeft() + (imageView2.getWidth() - k) / 2;
    imageView1.layout(m, j, m + k, i);
  }
  
  private void updateOffsetAndRange() {
    float f1, f2;
    ImageView imageView1 = this.mTrackImage;
    ImageView imageView2 = this.mThumbImage;
    if (this.mThumbPosition == 1) {
      f1 = imageView2.getHeight() / 2.0F;
      f2 = imageView1.getTop() + f1;
      f1 = imageView1.getBottom() - f1;
    } else {
      f2 = imageView1.getTop();
      f1 = imageView1.getBottom();
    } 
    this.mThumbOffset = f2;
    this.mThumbRange = f1 - f2;
  }
  
  private void setState(int paramInt) {
    AbsListView absListView = this.mList;
    if (absListView == null)
      return; 
    absListView.removeCallbacks(this.mDeferHide);
    int i = paramInt;
    if (this.mAlwaysShow) {
      i = paramInt;
      if (paramInt == 0)
        i = 1; 
    } 
    if (i == this.mState)
      return; 
    if (i != 0)
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            this.mState = i;
            refreshDrawablePressedState();
          } 
        } else {
          if (transitionPreviewLayout(this.mCurrentSection)) {
            transitionToDragging();
          } else {
            transitionToVisible();
          } 
          this.mState = i;
          refreshDrawablePressedState();
        } 
      } else {
        transitionToVisible();
        this.mState = i;
        refreshDrawablePressedState();
      }  
    transitionToHidden();
    this.mState = i;
    refreshDrawablePressedState();
  }
  
  private void refreshDrawablePressedState() {
    boolean bool;
    if (this.mState == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mThumbImage.setPressed(bool);
    this.mTrackImage.setPressed(bool);
  }
  
  private void transitionToHidden() {
    int i;
    AnimatorSet animatorSet1 = this.mDecorAnimation;
    if (animatorSet1 != null)
      animatorSet1.cancel(); 
    Animator animator1 = groupAnimatorOfFloat(View.ALPHA, 0.0F, new View[] { (View)this.mThumbImage, (View)this.mTrackImage, this.mPreviewImage, (View)this.mPrimaryText, (View)this.mSecondaryText });
    animator1 = animator1.setDuration(300L);
    if (this.mLayoutFromRight) {
      i = this.mThumbImage.getWidth();
    } else {
      i = -this.mThumbImage.getWidth();
    } 
    float f = i;
    Animator animator2 = groupAnimatorOfFloat(View.TRANSLATION_X, f, new View[] { (View)this.mThumbImage, (View)this.mTrackImage });
    animator2.setDuration(300L);
    AnimatorSet animatorSet2 = new AnimatorSet();
    animatorSet2.playTogether(new Animator[] { animator1 });
    this.mDecorAnimation.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
          final ColorFastScroller this$0;
          
          public void onAnimationEnd(Animator param1Animator) {
            if (ColorFastScroller.this.mState == 3)
              ColorFastScroller.access$202(ColorFastScroller.this, 0); 
          }
        });
    this.mDecorAnimation.start();
    this.mShowingPreview = false;
  }
  
  private void transitionToVisible() {
    AnimatorSet animatorSet1 = this.mDecorAnimation;
    if (animatorSet1 != null)
      animatorSet1.cancel(); 
    Animator animator1 = groupAnimatorOfFloat(View.ALPHA, 1.0F, new View[] { (View)this.mThumbImage, (View)this.mTrackImage });
    animator1 = animator1.setDuration(150L);
    Animator animator2 = groupAnimatorOfFloat(View.ALPHA, 0.0F, new View[] { this.mPreviewImage, (View)this.mPrimaryText, (View)this.mSecondaryText });
    animator2 = animator2.setDuration(300L);
    Animator animator3 = groupAnimatorOfFloat(View.TRANSLATION_X, 0.0F, new View[] { (View)this.mThumbImage, (View)this.mTrackImage });
    animator3.setDuration(150L);
    AnimatorSet animatorSet2 = new AnimatorSet();
    animatorSet2.playTogether(new Animator[] { animator1, animator2 });
    this.mDecorAnimation.start();
    this.mShowingPreview = false;
  }
  
  private void transitionToDragging() {
    AnimatorSet animatorSet1 = this.mDecorAnimation;
    if (animatorSet1 != null)
      animatorSet1.cancel(); 
    Animator animator1 = groupAnimatorOfFloat(View.ALPHA, 1.0F, new View[] { (View)this.mThumbImage, (View)this.mTrackImage, this.mPreviewImage });
    animator1 = animator1.setDuration(150L);
    Animator animator2 = groupAnimatorOfFloat(View.TRANSLATION_X, 0.0F, new View[] { (View)this.mThumbImage, (View)this.mTrackImage });
    animator2.setDuration(150L);
    AnimatorSet animatorSet2 = new AnimatorSet();
    animatorSet2.playTogether(new Animator[] { animator1 });
    this.mDecorAnimation.start();
    this.mShowingPreview = true;
  }
  
  private void postAutoHide() {
    this.mList.removeCallbacks(this.mDeferHide);
    this.mList.postDelayed(this.mDeferHide, 1500L);
  }
  
  public void onScroll(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = isEnabled();
    boolean bool1 = false;
    if (!bool) {
      setState(0);
      return;
    } 
    if (paramInt3 - paramInt2 > 0)
      bool1 = true; 
    if (bool1 && this.mState != 2)
      setThumbPos(getPosFromItemCount(paramInt1, paramInt2, paramInt3)); 
    this.mScrollCompleted = true;
    if (this.mFirstVisibleItem != paramInt1) {
      this.mFirstVisibleItem = paramInt1;
      if (this.mState != 2) {
        setState(1);
        postAutoHide();
      } 
    } 
  }
  
  private void getSectionsFromIndexer() {
    ListAdapter listAdapter;
    this.mSectionIndexer = null;
    Adapter adapter1 = this.mList.getAdapter();
    Adapter adapter2 = adapter1;
    if (adapter1 instanceof HeaderViewListAdapter) {
      this.mHeaderCount = ((HeaderViewListAdapter)adapter1).getHeadersCount();
      listAdapter = ((HeaderViewListAdapter)adapter1).getWrappedAdapter();
    } 
    if (listAdapter instanceof ExpandableListConnector) {
      ExpandableListConnector expandableListConnector = (ExpandableListConnector)listAdapter;
      ExpandableListAdapter expandableListAdapter = expandableListConnector.getAdapter();
      if (expandableListAdapter instanceof SectionIndexer) {
        SectionIndexer sectionIndexer = (SectionIndexer)expandableListAdapter;
        this.mListAdapter = (Adapter)listAdapter;
        this.mSections = sectionIndexer.getSections();
      } 
    } else {
      SectionIndexer sectionIndexer;
      if (listAdapter instanceof SectionIndexer) {
        this.mListAdapter = (Adapter)listAdapter;
        this.mSectionIndexer = sectionIndexer = (SectionIndexer)listAdapter;
        this.mSections = sectionIndexer.getSections();
      } else {
        this.mListAdapter = (Adapter)sectionIndexer;
        this.mSections = null;
      } 
    } 
  }
  
  public void onSectionsChanged() {
    this.mListAdapter = null;
  }
  
  private void scrollTo(float paramFloat) {
    int j, k;
    this.mScrollCompleted = false;
    int i = this.mList.getCount();
    Object[] arrayOfObject = this.mSections;
    if (arrayOfObject == null) {
      j = 0;
    } else {
      j = arrayOfObject.length;
    } 
    if (arrayOfObject != null && j > 1) {
      ExpandableListView expandableListView;
      float f3;
      int m = MathUtils.constrain((int)(j * paramFloat), 0, j - 1);
      int n = m;
      int i1 = this.mSectionIndexer.getPositionForSection(n);
      int i2 = n;
      int i3 = i;
      int i4 = i1;
      int i5 = n;
      int i6 = n + 1;
      if (n < j - 1)
        i3 = this.mSectionIndexer.getPositionForSection(n + 1); 
      k = i2;
      int i7 = i4, i8 = i5;
      if (i3 == i1) {
        i7 = i4;
        i4 = n;
        while (true) {
          k = i2;
          i8 = i5;
          if (i4 > 0) {
            k = i4 - 1;
            n = this.mSectionIndexer.getPositionForSection(k);
            if (n != i1) {
              i8 = k;
              i7 = n;
              break;
            } 
            i4 = k;
            i7 = n;
            if (k == 0) {
              k = 0;
              i8 = i5;
              i7 = n;
              break;
            } 
            continue;
          } 
          break;
        } 
      } 
      n = i6 + 1;
      i4 = i6;
      while (n < j) {
        SectionIndexer sectionIndexer = this.mSectionIndexer;
        if (sectionIndexer.getPositionForSection(n) == i3) {
          n++;
          i4++;
        } 
      } 
      float f1 = i8 / j;
      float f2 = i4 / j;
      if (i == 0) {
        f3 = Float.MAX_VALUE;
      } else {
        f3 = 0.125F / i;
      } 
      if (i8 != m || paramFloat - f1 >= f3)
        i7 = (int)((i3 - i7) * (paramFloat - f1) / (f2 - f1)) + i7; 
      i7 = MathUtils.constrain(i7, 0, i - 1);
      AbsListView absListView = this.mList;
      if (absListView instanceof ExpandableListView) {
        expandableListView = (ExpandableListView)absListView;
        n = this.mHeaderCount;
        long l = ExpandableListView.getPackedPositionForGroup(n + i7);
        expandableListView.setSelectionFromTop(expandableListView.getFlatListPosition(l), 0);
      } else if (expandableListView instanceof ListView) {
        ((ListView)expandableListView).setSelectionFromTop(this.mHeaderCount + i7, 0);
      } else {
        expandableListView.setSelection(this.mHeaderCount + i7);
      } 
    } else {
      ExpandableListView expandableListView;
      byte b = 0;
      k = this.mList.getChildCount();
      View view2 = this.mList.getChildAt(0);
      View view1 = this.mList.getChildAt(k - 1);
      float f1 = 0.0F;
      float f2 = f1;
      if (view2 != null) {
        f2 = f1;
        if (view2.getHeight() != 0) {
          f2 = (view2.getTop() - this.mList.getTop());
          AbsListView absListView1 = this.mList;
          f2 = (f2 + (absListView1.getBottom() - view1.getBottom())) / view2.getHeight();
        } 
      } 
      j = MathUtils.constrain((int)(((i - k) - f2) * paramFloat * 1000.0F), 0, (i - 1) * 1000);
      int m = j / 1000;
      int n = b;
      k = m;
      if (view2 != null) {
        n = b;
        k = m;
        if (view2.getHeight() != 0) {
          n = view2.getHeight() - view2.getHeight() * j % 1000 / 1000;
          k = m + 1;
        } 
      } 
      AbsListView absListView = this.mList;
      if (absListView instanceof ExpandableListView) {
        expandableListView = (ExpandableListView)absListView;
        n = this.mHeaderCount;
        long l = ExpandableListView.getPackedPositionForGroup(n + k);
        expandableListView.setSelectionFromTop(expandableListView.getFlatListPosition(l), 0);
      } else if (expandableListView instanceof ListView) {
        if (paramFloat >= 1.0F && n != 0) {
          ((ListView)expandableListView).setSelectionFromTop(k + 1 + this.mHeaderCount, 0);
        } else {
          ((ListView)this.mList).setSelectionFromTop(this.mHeaderCount + k, n);
        } 
      } else {
        expandableListView.setSelection(this.mHeaderCount + k);
      } 
      k = -1;
    } 
    if (this.mCurrentSection != k) {
      this.mCurrentSection = k;
      boolean bool = transitionPreviewLayout(k);
      if (!this.mShowingPreview && bool) {
        transitionToDragging();
      } else if (this.mShowingPreview && !bool) {
        transitionToVisible();
      } 
    } 
  }
  
  private boolean transitionPreviewLayout(int paramInt) {
    Object object;
    TextView textView;
    Object[] arrayOfObject = this.mSections;
    String str1 = null;
    String str2 = str1;
    if (arrayOfObject != null) {
      str2 = str1;
      if (paramInt >= 0) {
        str2 = str1;
        if (paramInt < arrayOfObject.length) {
          object = arrayOfObject[paramInt];
          str2 = str1;
          if (object != null)
            str2 = object.toString(); 
        } 
      } 
    } 
    Rect rect = this.mTempBounds;
    View view = this.mPreviewImage;
    if (this.mShowingPrimary) {
      textView = this.mPrimaryText;
      object = this.mSecondaryText;
    } else {
      textView = this.mSecondaryText;
      object = this.mPrimaryText;
    } 
    object.setText(str2);
    measurePreview((View)object, rect);
    applyLayout((View)object, rect);
    AnimatorSet animatorSet1 = this.mPreviewAnimation;
    if (animatorSet1 != null)
      animatorSet1.cancel(); 
    Animator animator2 = animateAlpha((View)object, 1.0F).setDuration(50L);
    Animator animator3 = animateAlpha((View)textView, 0.0F).setDuration(50L);
    animator3.addListener(this.mSwitchPrimaryListener);
    rect.left -= view.getPaddingLeft();
    rect.top -= view.getPaddingTop();
    rect.right += view.getPaddingRight();
    rect.bottom += view.getPaddingBottom();
    Animator animator1 = animateBounds(view, rect);
    animator1.setDuration(100L);
    AnimatorSet animatorSet2 = new AnimatorSet();
    AnimatorSet.Builder builder = animatorSet2.play(animator3).with(animator2);
    builder.with(animator1);
    paramInt = view.getWidth();
    int i = view.getPaddingLeft();
    i = paramInt - i - view.getPaddingRight();
    paramInt = object.getWidth();
    if (paramInt > i) {
      object.setScaleX(i / paramInt);
      object = animateScaleX((View)object, 1.0F).setDuration(100L);
      builder.with((Animator)object);
    } else {
      object.setScaleX(1.0F);
    } 
    i = textView.getWidth();
    if (i > paramInt) {
      float f = paramInt / i;
      Animator animator = animateScaleX((View)textView, f).setDuration(100L);
      builder.with(animator);
    } 
    this.mPreviewAnimation.start();
    return TextUtils.isEmpty(str2) ^ true;
  }
  
  private void setThumbPos(float paramFloat) {
    paramFloat = this.mThumbRange * paramFloat + this.mThumbOffset;
    ImageView imageView = this.mThumbImage;
    imageView.setTranslationY(paramFloat - imageView.getHeight() / 2.0F);
    View view = this.mPreviewImage;
    float f1 = view.getHeight() / 2.0F;
    int i = this.mOverlayPosition;
    if (i != 1)
      if (i != 2) {
        paramFloat = 0.0F;
      } else {
        paramFloat -= f1;
      }  
    Rect rect = this.mContainerRect;
    int j = rect.top;
    i = rect.bottom;
    float f2 = j;
    float f3 = i;
    paramFloat = MathUtils.constrain(paramFloat, f2 + f1, f3 - f1);
    paramFloat -= f1;
    view.setTranslationY(paramFloat);
    this.mPrimaryText.setTranslationY(paramFloat);
    this.mSecondaryText.setTranslationY(paramFloat);
  }
  
  private float getPosFromMotionEvent(float paramFloat) {
    float f = this.mThumbRange;
    if (f <= 0.0F)
      return 0.0F; 
    return MathUtils.constrain((paramFloat - this.mThumbOffset) / f, 0.0F, 1.0F);
  }
  
  private float getPosFromItemCount(int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSectionIndexer : Landroid/widget/SectionIndexer;
    //   4: astore #4
    //   6: aload #4
    //   8: ifnull -> 18
    //   11: aload_0
    //   12: getfield mListAdapter : Landroid/widget/Adapter;
    //   15: ifnonnull -> 22
    //   18: aload_0
    //   19: invokespecial getSectionsFromIndexer : ()V
    //   22: iload_2
    //   23: ifeq -> 504
    //   26: iload_3
    //   27: ifne -> 33
    //   30: goto -> 504
    //   33: aload #4
    //   35: ifnull -> 61
    //   38: aload_0
    //   39: getfield mSections : [Ljava/lang/Object;
    //   42: astore #5
    //   44: aload #5
    //   46: ifnull -> 61
    //   49: aload #5
    //   51: arraylength
    //   52: ifle -> 61
    //   55: iconst_1
    //   56: istore #6
    //   58: goto -> 64
    //   61: iconst_0
    //   62: istore #6
    //   64: iload #6
    //   66: ifeq -> 398
    //   69: aload_0
    //   70: getfield mMatchDragPosition : Z
    //   73: ifne -> 79
    //   76: goto -> 398
    //   79: aload_0
    //   80: getfield mHeaderCount : I
    //   83: istore #7
    //   85: iload_1
    //   86: iload #7
    //   88: isub
    //   89: istore #6
    //   91: iload #6
    //   93: ifge -> 98
    //   96: fconst_0
    //   97: freturn
    //   98: iload_3
    //   99: iload #7
    //   101: isub
    //   102: istore_3
    //   103: aload_0
    //   104: getfield mList : Landroid/widget/AbsListView;
    //   107: iconst_0
    //   108: invokevirtual getChildAt : (I)Landroid/view/View;
    //   111: astore #5
    //   113: aload #5
    //   115: ifnull -> 155
    //   118: aload #5
    //   120: invokevirtual getHeight : ()I
    //   123: ifne -> 129
    //   126: goto -> 155
    //   129: aload_0
    //   130: getfield mList : Landroid/widget/AbsListView;
    //   133: invokevirtual getPaddingTop : ()I
    //   136: aload #5
    //   138: invokevirtual getTop : ()I
    //   141: isub
    //   142: i2f
    //   143: aload #5
    //   145: invokevirtual getHeight : ()I
    //   148: i2f
    //   149: fdiv
    //   150: fstore #8
    //   152: goto -> 158
    //   155: fconst_0
    //   156: fstore #8
    //   158: aload #4
    //   160: iload #6
    //   162: invokeinterface getSectionForPosition : (I)I
    //   167: istore #7
    //   169: aload #4
    //   171: iload #7
    //   173: invokeinterface getPositionForSection : (I)I
    //   178: istore #9
    //   180: aload_0
    //   181: getfield mSections : [Ljava/lang/Object;
    //   184: arraylength
    //   185: istore #10
    //   187: iload #7
    //   189: iload #10
    //   191: iconst_1
    //   192: isub
    //   193: if_icmpge -> 232
    //   196: iload #7
    //   198: iconst_1
    //   199: iadd
    //   200: iload #10
    //   202: if_icmpge -> 220
    //   205: aload #4
    //   207: iload #7
    //   209: iconst_1
    //   210: iadd
    //   211: invokeinterface getPositionForSection : (I)I
    //   216: istore_1
    //   217: goto -> 224
    //   220: iload_3
    //   221: iconst_1
    //   222: isub
    //   223: istore_1
    //   224: iload_1
    //   225: iload #9
    //   227: isub
    //   228: istore_1
    //   229: goto -> 237
    //   232: iload_3
    //   233: iload #9
    //   235: isub
    //   236: istore_1
    //   237: iload_1
    //   238: ifne -> 247
    //   241: fconst_0
    //   242: fstore #8
    //   244: goto -> 262
    //   247: iload #6
    //   249: i2f
    //   250: fload #8
    //   252: fadd
    //   253: iload #9
    //   255: i2f
    //   256: fsub
    //   257: iload_1
    //   258: i2f
    //   259: fdiv
    //   260: fstore #8
    //   262: iload #7
    //   264: i2f
    //   265: fload #8
    //   267: fadd
    //   268: iload #10
    //   270: i2f
    //   271: fdiv
    //   272: fstore #8
    //   274: iload #6
    //   276: ifle -> 395
    //   279: iload #6
    //   281: iload_2
    //   282: iadd
    //   283: iload_3
    //   284: if_icmpne -> 395
    //   287: aload_0
    //   288: getfield mList : Landroid/widget/AbsListView;
    //   291: iload_2
    //   292: iconst_1
    //   293: isub
    //   294: invokevirtual getChildAt : (I)Landroid/view/View;
    //   297: astore #4
    //   299: aload_0
    //   300: getfield mList : Landroid/widget/AbsListView;
    //   303: invokevirtual getPaddingBottom : ()I
    //   306: istore_3
    //   307: aload_0
    //   308: getfield mList : Landroid/widget/AbsListView;
    //   311: invokevirtual getClipToPadding : ()Z
    //   314: ifeq -> 342
    //   317: aload #4
    //   319: invokevirtual getHeight : ()I
    //   322: istore_1
    //   323: aload_0
    //   324: getfield mList : Landroid/widget/AbsListView;
    //   327: invokevirtual getHeight : ()I
    //   330: iload_3
    //   331: isub
    //   332: aload #4
    //   334: invokevirtual getTop : ()I
    //   337: isub
    //   338: istore_2
    //   339: goto -> 366
    //   342: aload #4
    //   344: invokevirtual getHeight : ()I
    //   347: istore_1
    //   348: aload_0
    //   349: getfield mList : Landroid/widget/AbsListView;
    //   352: invokevirtual getHeight : ()I
    //   355: aload #4
    //   357: invokevirtual getTop : ()I
    //   360: isub
    //   361: istore_2
    //   362: iload_1
    //   363: iload_3
    //   364: iadd
    //   365: istore_1
    //   366: iload_2
    //   367: ifle -> 392
    //   370: iload_1
    //   371: ifle -> 392
    //   374: fload #8
    //   376: fconst_1
    //   377: fload #8
    //   379: fsub
    //   380: iload_2
    //   381: i2f
    //   382: iload_1
    //   383: i2f
    //   384: fdiv
    //   385: fmul
    //   386: fadd
    //   387: fstore #8
    //   389: goto -> 395
    //   392: goto -> 395
    //   395: fload #8
    //   397: freturn
    //   398: iload_2
    //   399: iload_3
    //   400: if_icmpne -> 405
    //   403: fconst_0
    //   404: freturn
    //   405: aload_0
    //   406: getfield mList : Landroid/widget/AbsListView;
    //   409: iconst_0
    //   410: invokevirtual getChildAt : (I)Landroid/view/View;
    //   413: astore #4
    //   415: aload #4
    //   417: ifnull -> 481
    //   420: aload #4
    //   422: invokevirtual getHeight : ()I
    //   425: ifne -> 431
    //   428: goto -> 481
    //   431: aload_0
    //   432: getfield mList : Landroid/widget/AbsListView;
    //   435: invokevirtual getPaddingTop : ()I
    //   438: aload #4
    //   440: invokevirtual getTop : ()I
    //   443: isub
    //   444: i2f
    //   445: aload #4
    //   447: invokevirtual getHeight : ()I
    //   450: i2f
    //   451: fdiv
    //   452: fstore #8
    //   454: iload_1
    //   455: i2f
    //   456: fload #8
    //   458: fadd
    //   459: iload_3
    //   460: aload_0
    //   461: getfield mList : Landroid/widget/AbsListView;
    //   464: invokevirtual getHeight : ()I
    //   467: aload #4
    //   469: invokevirtual getHeight : ()I
    //   472: idiv
    //   473: isub
    //   474: i2f
    //   475: fdiv
    //   476: fstore #8
    //   478: goto -> 492
    //   481: iload_1
    //   482: i2f
    //   483: fconst_0
    //   484: fadd
    //   485: iload_3
    //   486: iload_2
    //   487: isub
    //   488: i2f
    //   489: fdiv
    //   490: fstore #8
    //   492: fload #8
    //   494: fconst_1
    //   495: fcmpl
    //   496: ifle -> 501
    //   499: fconst_1
    //   500: freturn
    //   501: fload #8
    //   503: freturn
    //   504: fconst_0
    //   505: freturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1369	-> 0
    //   #1370	-> 6
    //   #1371	-> 18
    //   #1374	-> 22
    //   #1379	-> 33
    //   #1381	-> 64
    //   #1406	-> 79
    //   #1407	-> 91
    //   #1408	-> 96
    //   #1410	-> 98
    //   #1413	-> 103
    //   #1415	-> 113
    //   #1418	-> 129
    //   #1416	-> 155
    //   #1422	-> 158
    //   #1423	-> 169
    //   #1424	-> 180
    //   #1426	-> 187
    //   #1428	-> 196
    //   #1429	-> 205
    //   #1431	-> 220
    //   #1433	-> 224
    //   #1434	-> 229
    //   #1435	-> 232
    //   #1440	-> 237
    //   #1441	-> 241
    //   #1443	-> 247
    //   #1447	-> 262
    //   #1452	-> 274
    //   #1453	-> 287
    //   #1454	-> 299
    //   #1457	-> 307
    //   #1458	-> 317
    //   #1459	-> 323
    //   #1461	-> 342
    //   #1462	-> 348
    //   #1464	-> 366
    //   #1465	-> 374
    //   #1464	-> 392
    //   #1452	-> 395
    //   #1469	-> 395
    //   #1381	-> 398
    //   #1382	-> 398
    //   #1384	-> 403
    //   #1387	-> 405
    //   #1390	-> 415
    //   #1394	-> 431
    //   #1395	-> 454
    //   #1391	-> 481
    //   #1392	-> 481
    //   #1397	-> 492
    //   #1398	-> 499
    //   #1400	-> 501
    //   #1374	-> 504
    //   #1376	-> 504
  }
  
  private void cancelFling() {
    MotionEvent motionEvent = MotionEvent.obtain(0L, 0L, 3, 0.0F, 0.0F, 0);
    this.mList.onTouchEvent(motionEvent);
    motionEvent.recycle();
  }
  
  private void cancelPendingDrag() {
    this.mPendingDrag = -1L;
  }
  
  private void startPendingDrag() {
    this.mPendingDrag = SystemClock.uptimeMillis() + TAP_TIMEOUT;
  }
  
  private void beginDrag() {
    this.mPendingDrag = -1L;
    setState(2);
    if (this.mListAdapter == null && this.mList != null)
      getSectionsFromIndexer(); 
    AbsListView absListView = this.mList;
    if (absListView != null) {
      absListView.requestDisallowInterceptTouchEvent(true);
      this.mList.reportScrollStateChange(1);
    } 
    cancelFling();
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    if (!isEnabled())
      return false; 
    int i = paramMotionEvent.getActionMasked();
    if (i != 0) {
      if (i != 1)
        if (i != 2) {
          if (i != 3)
            return false; 
        } else {
          if (!isPointInside(paramMotionEvent.getX(), paramMotionEvent.getY())) {
            cancelPendingDrag();
          } else {
            long l = this.mPendingDrag;
            if (l >= 0L && l <= SystemClock.uptimeMillis()) {
              beginDrag();
              float f = getPosFromMotionEvent(this.mInitialTouchY);
              scrollTo(f);
              return onTouchEvent(paramMotionEvent);
            } 
          } 
          return false;
        }  
      cancelPendingDrag();
    } else if (isPointInside(paramMotionEvent.getX(), paramMotionEvent.getY())) {
      if (!this.mList.isInScrollingContainer() && this.mState != 0)
        return true; 
      this.mInitialTouchY = paramMotionEvent.getY();
      startPendingDrag();
    } 
    return false;
  }
  
  public boolean onInterceptHoverEvent(MotionEvent paramMotionEvent) {
    if (!isEnabled())
      return false; 
    int i = paramMotionEvent.getActionMasked();
    if ((i == 9 || i == 7) && this.mState == 0)
      if (isPointInside(paramMotionEvent.getX(), paramMotionEvent.getY())) {
        setState(1);
        postAutoHide();
      }  
    return false;
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    if (this.mState == 2 || isPointInside(paramMotionEvent.getX(), paramMotionEvent.getY()))
      return PointerIcon.getSystemIcon(this.mList.getContext(), 1000); 
    return null;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    AbsListView absListView;
    if (!isEnabled())
      return false; 
    int i = paramMotionEvent.getActionMasked();
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i == 3)
            cancelPendingDrag(); 
        } else {
          if (this.mPendingDrag >= 0L && Math.abs(paramMotionEvent.getY() - this.mInitialTouchY) > this.mScaledTouchSlop)
            beginDrag(); 
          if (this.mState == 2) {
            float f = getPosFromMotionEvent(paramMotionEvent.getY());
            setThumbPos(f);
            if (this.mScrollCompleted)
              scrollTo(f); 
            return true;
          } 
        } 
      } else {
        if (this.mPendingDrag >= 0L) {
          i = this.mState;
          if (i == 1 || i == 2) {
            beginDrag();
            float f = getPosFromMotionEvent(paramMotionEvent.getY());
            setThumbPos(f);
            scrollTo(f);
          } 
        } 
        if (this.mState == 2) {
          absListView = this.mList;
          if (absListView != null) {
            absListView.requestDisallowInterceptTouchEvent(false);
            this.mList.reportScrollStateChange(0);
          } 
          setState(1);
          postAutoHide();
          return true;
        } 
      } 
    } else if (isPointInside(absListView.getX(), absListView.getY()) && !this.mList.isInScrollingContainer()) {
      if (this.mState != 0) {
        beginDrag();
        return true;
      } 
    } 
    return false;
  }
  
  private boolean isPointInside(float paramFloat1, float paramFloat2) {
    boolean bool;
    if (isPointInsideX(paramFloat1) && (this.mTrackDrawable != null || isPointInsideY(paramFloat2))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isPointInsideX(float paramFloat) {
    float f1 = this.mThumbImage.getTranslationX();
    float f2 = this.mThumbImage.getLeft();
    float f3 = this.mThumbImage.getRight();
    f2 = this.mMinimumTouchTarget - f3 + f1 - f2 + f1;
    f3 = 0.0F;
    if (f2 > 0.0F)
      f3 = f2; 
    boolean bool = this.mLayoutFromRight;
    boolean bool1 = true, bool2 = true;
    if (bool) {
      if (paramFloat < this.mThumbImage.getLeft() - f3)
        bool2 = false; 
      return bool2;
    } 
    if (paramFloat <= this.mThumbImage.getRight() + f3) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  private boolean isPointInsideY(float paramFloat) {
    boolean bool;
    float f1 = this.mThumbImage.getTranslationY();
    float f2 = this.mThumbImage.getTop() + f1;
    float f3 = this.mThumbImage.getBottom() + f1;
    float f4 = this.mMinimumTouchTarget - f3 - f2;
    f1 = 0.0F;
    if (f4 > 0.0F)
      f1 = f4 / 2.0F; 
    if (paramFloat >= f2 - f1 && paramFloat <= f3 + f1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static Animator groupAnimatorOfFloat(Property<View, Float> paramProperty, float paramFloat, View... paramVarArgs) {
    AnimatorSet animatorSet = new AnimatorSet();
    AnimatorSet.Builder builder = null;
    for (int i = paramVarArgs.length - 1; i >= 0; i--) {
      ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(paramVarArgs[i], paramProperty, new float[] { paramFloat });
      if (builder == null) {
        builder = animatorSet.play((Animator)objectAnimator);
      } else {
        builder.with((Animator)objectAnimator);
      } 
    } 
    return (Animator)animatorSet;
  }
  
  private static Animator animateScaleX(View paramView, float paramFloat) {
    return (Animator)ObjectAnimator.ofFloat(paramView, View.SCALE_X, new float[] { paramFloat });
  }
  
  private static Animator animateAlpha(View paramView, float paramFloat) {
    return (Animator)ObjectAnimator.ofFloat(paramView, View.ALPHA, new float[] { paramFloat });
  }
  
  static {
    LEFT = (Property)new IntProperty<View>("left") {
        public void setValue(View param1View, int param1Int) {
          param1View.setLeft(param1Int);
        }
        
        public Integer get(View param1View) {
          return Integer.valueOf(param1View.getLeft());
        }
      };
    TOP = (Property)new IntProperty<View>("top") {
        public void setValue(View param1View, int param1Int) {
          param1View.setTop(param1Int);
        }
        
        public Integer get(View param1View) {
          return Integer.valueOf(param1View.getTop());
        }
      };
    RIGHT = (Property)new IntProperty<View>("right") {
        public void setValue(View param1View, int param1Int) {
          param1View.setRight(param1Int);
        }
        
        public Integer get(View param1View) {
          return Integer.valueOf(param1View.getRight());
        }
      };
    BOTTOM = (Property)new IntProperty<View>("bottom") {
        public void setValue(View param1View, int param1Int) {
          param1View.setBottom(param1Int);
        }
        
        public Integer get(View param1View) {
          return Integer.valueOf(param1View.getBottom());
        }
      };
  }
  
  private static Animator animateBounds(View paramView, Rect paramRect) {
    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofInt(LEFT, new int[] { paramRect.left });
    PropertyValuesHolder propertyValuesHolder3 = PropertyValuesHolder.ofInt(TOP, new int[] { paramRect.top });
    PropertyValuesHolder propertyValuesHolder4 = PropertyValuesHolder.ofInt(RIGHT, new int[] { paramRect.right });
    PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofInt(BOTTOM, new int[] { paramRect.bottom });
    return (Animator)ObjectAnimator.ofPropertyValuesHolder(paramView, new PropertyValuesHolder[] { propertyValuesHolder2, propertyValuesHolder3, propertyValuesHolder4, propertyValuesHolder1 });
  }
}
