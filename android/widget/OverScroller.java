package android.widget;

import android.common.ColorFrameworkFactory;
import android.content.Context;
import android.os.OplusSystemProperties;
import android.util.BoostFramework;
import android.util.Log;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.oplus.content.OplusFeatureConfigManager;

public class OverScroller {
  private static final int DEFAULT_DURATION = 250;
  
  private static final int FLING_MODE = 1;
  
  private static boolean FLING_OPT = false;
  
  private static final int SCROLL_MODE = 0;
  
  private static boolean customizationFling;
  
  private static FlingOptimizerOverScroller mFlingOptimizerOverScroller = null;
  
  private static int sVelocityToSplineOverScroller;
  
  private IOplusOverScrollerHelper mColorOverScrollerHelper;
  
  private final boolean mFlywheel;
  
  private Interpolator mInterpolator;
  
  private int mMode;
  
  private final SplineOverScroller mScrollerX;
  
  private final SplineOverScroller mScrollerY;
  
  static {
    customizationFling = false;
  }
  
  public OverScroller(Context paramContext) {
    this(paramContext, null);
  }
  
  public OverScroller(Context paramContext, Interpolator paramInterpolator) {
    this(paramContext, paramInterpolator, true);
  }
  
  public OverScroller(Context paramContext, Interpolator paramInterpolator, boolean paramBoolean) {
    if (paramInterpolator == null) {
      this.mInterpolator = new Scroller.ViscousFluidInterpolator();
    } else {
      this.mInterpolator = paramInterpolator;
    } 
    this.mFlywheel = paramBoolean;
    this.mScrollerX = new SplineOverScroller(paramContext);
    this.mScrollerY = new SplineOverScroller(paramContext);
    this.mColorOverScrollerHelper = (IOplusOverScrollerHelper)ColorFrameworkFactory.getInstance().getFeature(IOplusOverScrollerHelper.DEFAULT, new Object[] { this });
  }
  
  @Deprecated
  public OverScroller(Context paramContext, Interpolator paramInterpolator, float paramFloat1, float paramFloat2) {
    this(paramContext, paramInterpolator, true);
  }
  
  @Deprecated
  public OverScroller(Context paramContext, Interpolator paramInterpolator, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    this(paramContext, paramInterpolator, paramBoolean);
  }
  
  void setInterpolator(Interpolator paramInterpolator) {
    if (paramInterpolator == null) {
      this.mInterpolator = new Scroller.ViscousFluidInterpolator();
    } else {
      this.mInterpolator = paramInterpolator;
    } 
  }
  
  public final void setFriction(float paramFloat) {
    if (this.mColorOverScrollerHelper.setFriction(paramFloat))
      return; 
    this.mScrollerX.setFriction(paramFloat);
    this.mScrollerY.setFriction(paramFloat);
  }
  
  public final boolean isFinished() {
    boolean bool;
    IOplusOverScrollerHelper iOplusOverScrollerHelper = this.mColorOverScrollerHelper;
    if (this.mScrollerX.mFinished && this.mScrollerY.mFinished) {
      bool = true;
    } else {
      bool = false;
    } 
    return iOplusOverScrollerHelper.isFinished(bool);
  }
  
  public final void forceFinished(boolean paramBoolean) {
    SplineOverScroller.access$002(this.mScrollerX, SplineOverScroller.access$002(this.mScrollerY, paramBoolean));
    if (paramBoolean)
      BoostFramework.ScrollOptimizer.setFlingFlag(0); 
  }
  
  public final int getCurrX() {
    return this.mColorOverScrollerHelper.getCurrX(this.mScrollerX.mCurrentPosition);
  }
  
  public final int getCurrY() {
    return this.mColorOverScrollerHelper.getCurrY(this.mScrollerY.mCurrentPosition);
  }
  
  public float getCurrVelocity() {
    return (float)Math.hypot(this.mScrollerX.mCurrVelocity, this.mScrollerY.mCurrVelocity);
  }
  
  public final int getStartX() {
    return this.mScrollerX.mStart;
  }
  
  public final int getStartY() {
    return this.mScrollerY.mStart;
  }
  
  public final int getFinalX() {
    return this.mColorOverScrollerHelper.getFinalX(this.mScrollerX.mFinal);
  }
  
  public final int getFinalY() {
    return this.mColorOverScrollerHelper.getFinalY(this.mScrollerY.mFinal);
  }
  
  @Deprecated
  public final int getDuration() {
    return Math.max(this.mScrollerX.mDuration, this.mScrollerY.mDuration);
  }
  
  @Deprecated
  public void extendDuration(int paramInt) {
    this.mScrollerX.extendDuration(paramInt);
    this.mScrollerY.extendDuration(paramInt);
  }
  
  @Deprecated
  public void setFinalX(int paramInt) {
    this.mScrollerX.setFinalPosition(paramInt);
  }
  
  @Deprecated
  public void setFinalY(int paramInt) {
    this.mScrollerY.setFinalPosition(paramInt);
  }
  
  public boolean computeScrollOffset() {
    if (isFinished()) {
      BoostFramework.ScrollOptimizer.setFlingFlag(0);
      return false;
    } 
    int i = this.mMode;
    if (i != 0) {
      if (i == 1) {
        if (!this.mScrollerX.mFinished && 
          !this.mScrollerX.update() && 
          !this.mScrollerX.continueWhenFinished())
          this.mScrollerX.finish(); 
        if (!this.mScrollerY.mFinished && 
          !this.mScrollerY.update() && 
          !this.mScrollerY.continueWhenFinished())
          this.mScrollerY.finish(); 
      } 
    } else {
      long l = AnimationUtils.currentAnimationTimeMillis();
      l -= this.mScrollerX.mStartTime;
      i = this.mScrollerX.mDuration;
      if (l < i) {
        float f = this.mInterpolator.getInterpolation((float)l / i);
        this.mScrollerX.updateScroll(f);
        this.mScrollerY.updateScroll(f);
      } else {
        abortAnimation();
      } 
    } 
    if (isFinished())
      BoostFramework.ScrollOptimizer.setFlingFlag(0); 
    return true;
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    startScroll(paramInt1, paramInt2, paramInt3, paramInt4, 250);
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mMode = 0;
    this.mScrollerX.startScroll(paramInt1, paramInt3, paramInt5);
    this.mScrollerY.startScroll(paramInt2, paramInt4, paramInt5);
  }
  
  public boolean springBack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    boolean bool1 = true;
    this.mMode = 1;
    boolean bool2 = this.mScrollerX.springback(paramInt1, paramInt3, paramInt4);
    boolean bool3 = this.mScrollerY.springback(paramInt2, paramInt5, paramInt6);
    boolean bool4 = bool1;
    if (!bool2)
      if (bool3) {
        bool4 = bool1;
      } else {
        bool4 = false;
      }  
    return bool4;
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    fling(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, 0, 0);
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    boolean bool;
    if ((paramInt5 != Integer.MIN_VALUE && paramInt5 != 0) || (paramInt7 != Integer.MIN_VALUE && paramInt7 != 0) || (paramInt6 != Integer.MAX_VALUE && paramInt6 != 0) || (paramInt8 != Integer.MAX_VALUE && paramInt8 != 0)) {
      bool = true;
    } else {
      bool = false;
    } 
    customizationFling = bool;
    if (OplusFeatureConfigManager.getInstance().hasFeature("oplus.software.list_optimize"))
      FLING_OPT = OplusSystemProperties.getBoolean("persist.sys.flingopts.enable", false); 
    if (FLING_OPT && !customizationFling) {
      flingOpt(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9, paramInt10);
      return;
    } 
    if (this.mFlywheel && !isFinished()) {
      float f1 = this.mScrollerX.mCurrVelocity;
      float f2 = this.mScrollerY.mCurrVelocity;
      if (Math.signum(paramInt3) == Math.signum(f1)) {
        float f = paramInt4;
        if (Math.signum(f) == Math.signum(f2)) {
          paramInt3 = (int)(paramInt3 + f1);
          paramInt4 = (int)(paramInt4 + f2);
        } 
      } 
    } 
    this.mMode = 1;
    this.mScrollerX.fling(paramInt1, paramInt3, paramInt5, paramInt6, paramInt9);
    this.mScrollerY.fling(paramInt2, paramInt4, paramInt7, paramInt8, paramInt10);
  }
  
  private void flingOpt(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    if (this.mFlywheel && !isFinished()) {
      float f1 = this.mScrollerX.mCurrVelocity;
      float f2 = this.mScrollerY.mCurrVelocity;
      if (Math.signum(paramInt3) == Math.signum(f1)) {
        float f = paramInt4;
        if (Math.signum(f) == Math.signum(f2)) {
          paramInt3 = (int)(paramInt3 + f1);
          paramInt4 = (int)(paramInt4 + f2);
        } 
      } 
    } 
    sVelocityToSplineOverScroller = paramInt4;
    BoostFramework.ScrollOptimizer.setFlingFlag(1);
    this.mMode = 1;
    this.mScrollerX.fling(paramInt1, paramInt3, paramInt5, paramInt6, paramInt9);
    this.mScrollerY.fling(paramInt2, paramInt4, paramInt7, paramInt8, paramInt10);
  }
  
  public void notifyHorizontalEdgeReached(int paramInt1, int paramInt2, int paramInt3) {
    this.mScrollerX.notifyEdgeReached(paramInt1, paramInt2, paramInt3);
  }
  
  public void notifyVerticalEdgeReached(int paramInt1, int paramInt2, int paramInt3) {
    this.mScrollerY.notifyEdgeReached(paramInt1, paramInt2, paramInt3);
  }
  
  public boolean isOverScrolled() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mScrollerX : Landroid/widget/OverScroller$SplineOverScroller;
    //   4: invokestatic access$000 : (Landroid/widget/OverScroller$SplineOverScroller;)Z
    //   7: ifne -> 22
    //   10: aload_0
    //   11: getfield mScrollerX : Landroid/widget/OverScroller$SplineOverScroller;
    //   14: astore_1
    //   15: aload_1
    //   16: invokestatic access$700 : (Landroid/widget/OverScroller$SplineOverScroller;)I
    //   19: ifne -> 46
    //   22: aload_0
    //   23: getfield mScrollerY : Landroid/widget/OverScroller$SplineOverScroller;
    //   26: astore_1
    //   27: aload_1
    //   28: invokestatic access$000 : (Landroid/widget/OverScroller$SplineOverScroller;)Z
    //   31: ifne -> 51
    //   34: aload_0
    //   35: getfield mScrollerY : Landroid/widget/OverScroller$SplineOverScroller;
    //   38: astore_1
    //   39: aload_1
    //   40: invokestatic access$700 : (Landroid/widget/OverScroller$SplineOverScroller;)I
    //   43: ifeq -> 51
    //   46: iconst_1
    //   47: istore_2
    //   48: goto -> 53
    //   51: iconst_0
    //   52: istore_2
    //   53: iload_2
    //   54: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #617	-> 0
    //   #618	-> 15
    //   #619	-> 27
    //   #620	-> 39
    //   #617	-> 53
  }
  
  public void abortAnimation() {
    BoostFramework.ScrollOptimizer.setFlingFlag(0);
    this.mScrollerX.finish();
    this.mScrollerY.finish();
  }
  
  public int timePassed() {
    long l1 = AnimationUtils.currentAnimationTimeMillis();
    long l2 = Math.min(this.mScrollerX.mStartTime, this.mScrollerY.mStartTime);
    return (int)(l1 - l2);
  }
  
  public boolean isScrollingInDirection(float paramFloat1, float paramFloat2) {
    boolean bool;
    int i = this.mScrollerX.mFinal, j = this.mScrollerX.mStart;
    int k = this.mScrollerY.mFinal, m = this.mScrollerY.mStart;
    if (!isFinished() && Math.signum(paramFloat1) == Math.signum((i - j)) && 
      Math.signum(paramFloat2) == Math.signum((k - m))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  static class SplineOverScroller {
    private float mFlingFriction = ViewConfiguration.getScrollFriction();
    
    private int mState = 0;
    
    private static float DECELERATION_RATE = (float)(Math.log(0.78D) / Math.log(0.9D));
    
    private static final float[] SPLINE_POSITION = new float[101];
    
    private static final float[] SPLINE_TIME = new float[101];
    
    private static final int BALLISTIC = 2;
    
    private static final int CUBIC = 1;
    
    private static final float END_TENSION = 1.0F;
    
    private static final float GRAVITY = 2000.0F;
    
    private static final float INFLEXION = 0.35F;
    
    private static final int NB_SAMPLES = 100;
    
    private static final float P1 = 0.175F;
    
    private static final float P2 = 0.35000002F;
    
    private static final int SPLINE = 0;
    
    private static final float START_TENSION = 0.5F;
    
    private Context mContext;
    
    private float mCurrVelocity;
    
    private int mCurrentPosition;
    
    private float mDeceleration;
    
    private int mDuration;
    
    private int mFinal;
    
    private boolean mFinished;
    
    private int mOver;
    
    private float mPhysicalCoeff;
    
    private int mSplineDistance;
    
    private int mSplineDuration;
    
    private int mStart;
    
    private long mStartTime;
    
    private int mVelocity;
    
    static {
      float f1 = 0.0F;
      float f2 = 0.0F;
      for (byte b = 0; b < 100; ) {
        float f3 = b / 100.0F;
        float f4 = 1.0F;
        while (true) {
          float f5 = (f4 - f1) / 2.0F + f1;
          float f6 = f5 * 3.0F * (1.0F - f5);
          float f7 = ((1.0F - f5) * 0.175F + f5 * 0.35000002F) * f6 + f5 * f5 * f5;
          if (Math.abs(f7 - f3) < 1.0E-5D) {
            SPLINE_POSITION[b] = ((1.0F - f5) * 0.5F + f5) * f6 + f5 * f5 * f5;
            f4 = 1.0F;
            while (true) {
              f5 = (f4 - f2) / 2.0F + f2;
              f7 = f5 * 3.0F * (1.0F - f5);
              f6 = ((1.0F - f5) * 0.5F + f5) * f7 + f5 * f5 * f5;
              if (Math.abs(f6 - f3) < 1.0E-5D) {
                SPLINE_TIME[b] = f7 * ((1.0F - f5) * 0.175F + 0.35000002F * f5) + f5 * f5 * f5;
                b++;
              } 
              if (f6 > f3) {
                f4 = f5;
                continue;
              } 
              f2 = f5;
            } 
            break;
          } 
          if (f7 > f3) {
            f4 = f5;
            continue;
          } 
          f1 = f5;
        } 
      } 
      float[] arrayOfFloat = SPLINE_POSITION;
      SPLINE_TIME[100] = 1.0F;
      arrayOfFloat[100] = 1.0F;
    }
    
    void setFriction(float param1Float) {
      this.mFlingFriction = param1Float;
    }
    
    SplineOverScroller(Context param1Context) {
      this.mContext = param1Context;
      this.mFinished = true;
      float f = (param1Context.getResources().getDisplayMetrics()).density;
      this.mPhysicalCoeff = 386.0878F * f * 160.0F * 0.84F;
    }
    
    void updateScroll(float param1Float) {
      int i = this.mStart;
      this.mCurrentPosition = i + Math.round((this.mFinal - i) * param1Float);
    }
    
    private static float getDeceleration(int param1Int) {
      float f;
      if (param1Int > 0) {
        f = -2000.0F;
      } else {
        f = 2000.0F;
      } 
      return f;
    }
    
    private void adjustDuration(int param1Int1, int param1Int2, int param1Int3) {
      float f = Math.abs((param1Int3 - param1Int1) / (param1Int2 - param1Int1));
      param1Int1 = (int)(f * 100.0F);
      if (param1Int1 < 100) {
        float f1 = param1Int1 / 100.0F;
        float f2 = (param1Int1 + 1) / 100.0F;
        float arrayOfFloat[] = SPLINE_TIME, f3 = arrayOfFloat[param1Int1];
        float f4 = arrayOfFloat[param1Int1 + 1];
        f2 = (f - f1) / (f2 - f1);
        this.mDuration = (int)(this.mDuration * (f2 * (f4 - f3) + f3));
      } 
    }
    
    void startScroll(int param1Int1, int param1Int2, int param1Int3) {
      this.mFinished = false;
      this.mStart = param1Int1;
      this.mCurrentPosition = param1Int1;
      this.mFinal = param1Int1 + param1Int2;
      this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
      this.mDuration = param1Int3;
      this.mDeceleration = 0.0F;
      this.mVelocity = 0;
    }
    
    void finish() {
      this.mCurrentPosition = this.mFinal;
      this.mFinished = true;
    }
    
    void setFinalPosition(int param1Int) {
      this.mFinal = param1Int;
      this.mFinished = false;
    }
    
    void extendDuration(int param1Int) {
      long l = AnimationUtils.currentAnimationTimeMillis();
      int i = (int)(l - this.mStartTime);
      this.mDuration = i + param1Int;
      this.mFinished = false;
    }
    
    boolean springback(int param1Int1, int param1Int2, int param1Int3) {
      this.mFinished = true;
      this.mFinal = param1Int1;
      this.mStart = param1Int1;
      this.mCurrentPosition = param1Int1;
      this.mVelocity = 0;
      this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
      this.mDuration = 0;
      if (param1Int1 < param1Int2) {
        startSpringback(param1Int1, param1Int2, 0);
      } else if (param1Int1 > param1Int3) {
        startSpringback(param1Int1, param1Int3, 0);
      } 
      return true ^ this.mFinished;
    }
    
    private void startSpringback(int param1Int1, int param1Int2, int param1Int3) {
      this.mFinished = false;
      this.mState = 1;
      this.mStart = param1Int1;
      this.mCurrentPosition = param1Int1;
      this.mFinal = param1Int2;
      param1Int1 -= param1Int2;
      this.mDeceleration = getDeceleration(param1Int1);
      this.mVelocity = -param1Int1;
      this.mOver = Math.abs(param1Int1);
      this.mDuration = (int)(Math.sqrt(param1Int1 * -2.0D / this.mDeceleration) * 1000.0D);
    }
    
    void fling(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) {
      this.mOver = param1Int5;
      this.mFinished = false;
      this.mVelocity = param1Int2;
      this.mCurrVelocity = param1Int2;
      this.mSplineDuration = 0;
      this.mDuration = 0;
      this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
      this.mStart = param1Int1;
      this.mCurrentPosition = param1Int1;
      if (param1Int1 > param1Int4 || param1Int1 < param1Int3) {
        startAfterEdge(param1Int1, param1Int3, param1Int4, param1Int2);
        return;
      } 
      this.mState = 0;
      double d = 0.0D;
      if (param1Int2 != 0)
        if (OverScroller.FLING_OPT && !OverScroller.customizationFling) {
          if (OverScroller.mFlingOptimizerOverScroller == null)
            OverScroller.access$1002(new FlingOptimizerOverScroller(this.mContext)); 
          this.mSplineDuration = param1Int5 = OverScroller.mFlingOptimizerOverScroller.getSplineFlingDurationTuning(param1Int2, this.mFlingFriction);
          this.mDuration = param1Int5;
          d = OverScroller.mFlingOptimizerOverScroller.getSplineFlingDistanceTuning(param1Int2, this.mFlingFriction);
        } else {
          this.mSplineDuration = param1Int5 = getSplineFlingDuration(param1Int2);
          this.mDuration = param1Int5;
          d = getSplineFlingDistance(param1Int2);
        }  
      this.mSplineDistance = param1Int2 = (int)(Math.signum(param1Int2) * d);
      this.mFinal = param1Int1 = param1Int2 + param1Int1;
      if (param1Int1 < param1Int3) {
        adjustDuration(this.mStart, param1Int1, param1Int3);
        this.mFinal = param1Int3;
      } 
      param1Int1 = this.mFinal;
      if (param1Int1 > param1Int4) {
        adjustDuration(this.mStart, param1Int1, param1Int4);
        this.mFinal = param1Int4;
      } 
    }
    
    private double getSplineDeceleration(int param1Int) {
      return Math.log((Math.abs(param1Int) * 0.35F / this.mFlingFriction * this.mPhysicalCoeff));
    }
    
    private double getSplineFlingDistance(int param1Int) {
      double d1 = getSplineDeceleration(param1Int);
      float f = DECELERATION_RATE;
      double d2 = f;
      return (this.mFlingFriction * this.mPhysicalCoeff) * Math.exp(f / (d2 - 1.0D) * d1);
    }
    
    private int getSplineFlingDuration(int param1Int) {
      double d1 = getSplineDeceleration(param1Int);
      double d2 = DECELERATION_RATE;
      return (int)(Math.exp(d1 / (d2 - 1.0D)) * 1000.0D);
    }
    
    private void fitOnBounceCurve(int param1Int1, int param1Int2, int param1Int3) {
      float f1 = -param1Int3, f2 = this.mDeceleration;
      f1 /= f2;
      float f3 = param1Int3, f4 = param1Int3;
      f2 = f3 * f4 / 2.0F / Math.abs(f2);
      f3 = Math.abs(param1Int2 - param1Int1);
      double d = (f2 + f3);
      f2 = this.mDeceleration;
      d = d * 2.0D / Math.abs(f2);
      f2 = (float)Math.sqrt(d);
      this.mStartTime -= (int)((f2 - f1) * 1000.0F);
      this.mStart = param1Int2;
      this.mCurrentPosition = param1Int2;
      this.mVelocity = (int)(-this.mDeceleration * f2);
    }
    
    private void startBounceAfterEdge(int param1Int1, int param1Int2, int param1Int3) {
      int i;
      if (param1Int3 == 0) {
        i = param1Int1 - param1Int2;
      } else {
        i = param1Int3;
      } 
      this.mDeceleration = getDeceleration(i);
      fitOnBounceCurve(param1Int1, param1Int2, param1Int3);
      onEdgeReached();
    }
    
    private void startAfterEdge(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      boolean bool2;
      int i;
      boolean bool1 = true;
      if (param1Int1 > param1Int2 && param1Int1 < param1Int3) {
        Log.e("OverScroller", "startAfterEdge called from a valid position");
        this.mFinished = true;
        return;
      } 
      if (param1Int1 > param1Int3) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool2) {
        i = param1Int3;
      } else {
        i = param1Int2;
      } 
      int j = param1Int1 - i;
      if (j * param1Int4 < 0)
        bool1 = false; 
      if (bool1) {
        startBounceAfterEdge(param1Int1, i, param1Int4);
      } else {
        double d = getSplineFlingDistance(param1Int4);
        if (d > Math.abs(j)) {
          if (!bool2)
            param1Int2 = param1Int1; 
          if (bool2)
            param1Int3 = param1Int1; 
          fling(param1Int1, param1Int4, param1Int2, param1Int3, this.mOver);
        } else {
          startSpringback(param1Int1, i, param1Int4);
        } 
      } 
    }
    
    void notifyEdgeReached(int param1Int1, int param1Int2, int param1Int3) {
      if (this.mState == 0) {
        this.mOver = param1Int3;
        this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
        startAfterEdge(param1Int1, param1Int2, param1Int2, (int)this.mCurrVelocity);
      } 
    }
    
    private void onEdgeReached() {
      int i = this.mVelocity;
      float f1 = i * i;
      float f2 = f1 / Math.abs(this.mDeceleration) * 2.0F;
      float f3 = Math.signum(this.mVelocity);
      i = this.mOver;
      float f4 = f2;
      if (f2 > i) {
        this.mDeceleration = -f3 * f1 / i * 2.0F;
        f4 = i;
      } 
      this.mOver = (int)f4;
      this.mState = 2;
      i = this.mStart;
      if (this.mVelocity <= 0)
        f4 = -f4; 
      this.mFinal = i + (int)f4;
      this.mDuration = -((int)(this.mVelocity * 1000.0F / this.mDeceleration));
    }
    
    boolean continueWhenFinished() {
      int i = this.mState;
      if (i != 0) {
        if (i != 1) {
          if (i == 2) {
            this.mStartTime += this.mDuration;
            startSpringback(this.mFinal, this.mStart, 0);
          } 
        } else {
          return false;
        } 
      } else {
        if (this.mDuration < this.mSplineDuration) {
          this.mStart = i = this.mFinal;
          this.mCurrentPosition = i;
          this.mVelocity = i = (int)this.mCurrVelocity;
          this.mDeceleration = getDeceleration(i);
          this.mStartTime += this.mDuration;
          onEdgeReached();
          update();
          return true;
        } 
        return false;
      } 
      update();
      return true;
    }
    
    boolean update() {
      long l = AnimationUtils.currentAnimationTimeMillis();
      l -= this.mStartTime;
      boolean bool = false;
      if (l == 0L) {
        if (this.mDuration > 0)
          bool = true; 
        return bool;
      } 
      int i = this.mDuration;
      if (l > i)
        return false; 
      double d = 0.0D;
      int j = this.mState;
      if (j != 0) {
        if (j != 1) {
          if (j == 2) {
            float f1 = (float)l / 1000.0F;
            i = this.mVelocity;
            float f2 = i, f3 = this.mDeceleration;
            this.mCurrVelocity = f2 + f3 * f1;
            d = (i * f1 + f3 * f1 * f1 / 2.0F);
          } 
        } else {
          float f1 = (float)l / i;
          float f3 = f1 * f1;
          float f2 = Math.signum(this.mVelocity);
          i = this.mOver;
          d = (i * f2 * (3.0F * f3 - 2.0F * f1 * f3));
          this.mCurrVelocity = i * f2 * 6.0F * (-f1 + f3);
        } 
      } else {
        float f4 = (float)l / this.mSplineDuration;
        i = (int)(f4 * 100.0F);
        float f5 = 1.0F;
        float f2 = 0.0F;
        float f3 = f5, f1 = f2;
        if (i < 100) {
          f3 = f5;
          f1 = f2;
          if (i >= 0) {
            f2 = i / 100.0F;
            f1 = (i + 1) / 100.0F;
            float[] arrayOfFloat = SPLINE_POSITION;
            f3 = arrayOfFloat[i];
            f5 = arrayOfFloat[i + 1];
            f1 = (f5 - f3) / (f1 - f2);
            f3 += (f4 - f2) * f1;
          } 
        } 
        if (OverScroller.FLING_OPT && !OverScroller.customizationFling) {
          if (OverScroller.mFlingOptimizerOverScroller == null)
            OverScroller.access$1002(new FlingOptimizerOverScroller(this.mContext)); 
          d = OverScroller.mFlingOptimizerOverScroller.getUpdateDistance(l, this.mSplineDuration, this.mSplineDistance);
          this.mCurrVelocity = OverScroller.mFlingOptimizerOverScroller.getUpdateVelocity(l, this.mSplineDuration, OverScroller.sVelocityToSplineOverScroller);
        } else {
          i = this.mSplineDistance;
          d = (i * f3);
          this.mCurrVelocity = i * f1 / this.mSplineDuration * 1000.0F;
        } 
      } 
      this.mCurrentPosition = this.mStart + (int)Math.round(d);
      return true;
    }
  }
}
