package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;

public class OplusMagnifierHooksImpl implements IOplusMagnifierHooks {
  private static Bitmap mShadowBitmap;
  
  public int getMagnifierWidth(TypedArray paramTypedArray, Context paramContext) {
    return (int)paramContext.getResources().getDimension(201654554);
  }
  
  public int getMagnifierHeight(TypedArray paramTypedArray, Context paramContext) {
    return (int)paramContext.getResources().getDimension(201654552);
  }
  
  public float getMagnifierCornerRadius(TypedArray paramTypedArray, Context paramContext) {
    return paramContext.getResources().getDimension(201654551);
  }
  
  public void decodeShadowBitmap(Context paramContext) {
    mShadowBitmap = BitmapFactory.decodeResource(paramContext.getResources(), 201851371);
  }
  
  public void recycleShadowBitmap() {
    Bitmap bitmap = mShadowBitmap;
    if (bitmap != null)
      bitmap.recycle(); 
  }
  
  public void drawShadowBitmap(int paramInt1, int paramInt2, RecordingCanvas paramRecordingCanvas, Paint paramPaint) {
    Rect rect1 = new Rect(0, 0, mShadowBitmap.getWidth(), mShadowBitmap.getHeight());
    Rect rect2 = new Rect(0, 0, paramInt1, paramInt2);
    paramRecordingCanvas.drawBitmap(mShadowBitmap, rect1, rect2, paramPaint);
  }
}
