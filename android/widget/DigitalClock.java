package android.widget;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import java.util.Calendar;

@Deprecated
public class DigitalClock extends TextView {
  Calendar mCalendar;
  
  String mFormat;
  
  private FormatChangeObserver mFormatChangeObserver;
  
  private Handler mHandler;
  
  private Runnable mTicker;
  
  private boolean mTickerStopped = false;
  
  public DigitalClock(Context paramContext) {
    super(paramContext);
    initClock();
  }
  
  public DigitalClock(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    initClock();
  }
  
  private void initClock() {
    if (this.mCalendar == null)
      this.mCalendar = Calendar.getInstance(); 
  }
  
  protected void onAttachedToWindow() {
    this.mTickerStopped = false;
    super.onAttachedToWindow();
    this.mFormatChangeObserver = new FormatChangeObserver();
    getContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this.mFormatChangeObserver);
    setFormat();
    this.mHandler = new Handler();
    Object object = new Object(this);
    object.run();
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    this.mTickerStopped = true;
    getContext().getContentResolver().unregisterContentObserver(this.mFormatChangeObserver);
  }
  
  private void setFormat() {
    this.mFormat = DateFormat.getTimeFormatString(getContext());
  }
  
  class FormatChangeObserver extends ContentObserver {
    final DigitalClock this$0;
    
    public FormatChangeObserver() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      DigitalClock.this.setFormat();
    }
  }
  
  public CharSequence getAccessibilityClassName() {
    return DigitalClock.class.getName();
  }
}
