package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewHierarchyEncoder;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R;

public class CheckedTextView extends TextView implements Checkable {
  private ColorStateList mCheckMarkTintList = null;
  
  private BlendMode mCheckMarkBlendMode = null;
  
  private boolean mHasCheckMarkTint = false;
  
  private boolean mHasCheckMarkTintMode = false;
  
  private int mCheckMarkGravity = 8388613;
  
  private static final int[] CHECKED_STATE_SET = new int[] { 16842912 };
  
  private int mBasePadding;
  
  private Drawable mCheckMarkDrawable;
  
  private int mCheckMarkResource;
  
  private int mCheckMarkWidth;
  
  private boolean mChecked;
  
  private boolean mNeedRequestlayout;
  
  public CheckedTextView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public CheckedTextView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843720);
  }
  
  public CheckedTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public CheckedTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CheckedTextView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.CheckedTextView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    Drawable drawable = typedArray.getDrawable(1);
    if (drawable != null)
      setCheckMarkDrawable(drawable); 
    if (typedArray.hasValue(3)) {
      this.mCheckMarkBlendMode = Drawable.parseBlendMode(typedArray.getInt(3, -1), this.mCheckMarkBlendMode);
      this.mHasCheckMarkTintMode = true;
    } 
    if (typedArray.hasValue(2)) {
      this.mCheckMarkTintList = typedArray.getColorStateList(2);
      this.mHasCheckMarkTint = true;
    } 
    this.mCheckMarkGravity = typedArray.getInt(4, 8388613);
    boolean bool = typedArray.getBoolean(0, false);
    setChecked(bool);
    typedArray.recycle();
    applyCheckMarkTint();
  }
  
  public void toggle() {
    setChecked(this.mChecked ^ true);
  }
  
  @ExportedProperty
  public boolean isChecked() {
    return this.mChecked;
  }
  
  public void setChecked(boolean paramBoolean) {
    if (this.mChecked != paramBoolean) {
      this.mChecked = paramBoolean;
      refreshDrawableState();
      notifyViewAccessibilityStateChangedIfNeeded(0);
    } 
  }
  
  public void setCheckMarkDrawable(int paramInt) {
    Drawable drawable;
    if (paramInt != 0 && paramInt == this.mCheckMarkResource)
      return; 
    if (paramInt != 0) {
      drawable = getContext().getDrawable(paramInt);
    } else {
      drawable = null;
    } 
    setCheckMarkDrawableInternal(drawable, paramInt);
  }
  
  public void setCheckMarkDrawable(Drawable paramDrawable) {
    setCheckMarkDrawableInternal(paramDrawable, 0);
  }
  
  private void setCheckMarkDrawableInternal(Drawable paramDrawable, int paramInt) {
    boolean bool2;
    Drawable drawable = this.mCheckMarkDrawable;
    if (drawable != null) {
      drawable.setCallback(null);
      unscheduleDrawable(this.mCheckMarkDrawable);
    } 
    drawable = this.mCheckMarkDrawable;
    boolean bool1 = true;
    if (paramDrawable != drawable) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mNeedRequestlayout = bool2;
    if (paramDrawable != null) {
      paramDrawable.setCallback(this);
      if (getVisibility() == 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      paramDrawable.setVisible(bool2, false);
      paramDrawable.setState(CHECKED_STATE_SET);
      setMinHeight(paramDrawable.getIntrinsicHeight());
      this.mCheckMarkWidth = paramDrawable.getIntrinsicWidth();
      paramDrawable.setState(getDrawableState());
    } else {
      this.mCheckMarkWidth = 0;
    } 
    this.mCheckMarkDrawable = paramDrawable;
    this.mCheckMarkResource = paramInt;
    applyCheckMarkTint();
    resolvePadding();
  }
  
  public void setCheckMarkTintList(ColorStateList paramColorStateList) {
    this.mCheckMarkTintList = paramColorStateList;
    this.mHasCheckMarkTint = true;
    applyCheckMarkTint();
  }
  
  public ColorStateList getCheckMarkTintList() {
    return this.mCheckMarkTintList;
  }
  
  public void setCheckMarkTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setCheckMarkTintBlendMode((BlendMode)paramMode);
  }
  
  public void setCheckMarkTintBlendMode(BlendMode paramBlendMode) {
    this.mCheckMarkBlendMode = paramBlendMode;
    this.mHasCheckMarkTintMode = true;
    applyCheckMarkTint();
  }
  
  public PorterDuff.Mode getCheckMarkTintMode() {
    BlendMode blendMode = this.mCheckMarkBlendMode;
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getCheckMarkTintBlendMode() {
    return this.mCheckMarkBlendMode;
  }
  
  private void applyCheckMarkTint() {
    if (this.mCheckMarkDrawable != null && (this.mHasCheckMarkTint || this.mHasCheckMarkTintMode)) {
      Drawable drawable = this.mCheckMarkDrawable.mutate();
      if (this.mHasCheckMarkTint)
        drawable.setTintList(this.mCheckMarkTintList); 
      if (this.mHasCheckMarkTintMode)
        this.mCheckMarkDrawable.setTintBlendMode(this.mCheckMarkBlendMode); 
      if (this.mCheckMarkDrawable.isStateful())
        this.mCheckMarkDrawable.setState(getDrawableState()); 
    } 
  }
  
  @RemotableViewMethod
  public void setVisibility(int paramInt) {
    super.setVisibility(paramInt);
    Drawable drawable = this.mCheckMarkDrawable;
    if (drawable != null) {
      boolean bool;
      if (paramInt == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      drawable.setVisible(bool, false);
    } 
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mCheckMarkDrawable;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (paramDrawable == this.mCheckMarkDrawable || super.verifyDrawable(paramDrawable));
  }
  
  public Drawable getCheckMarkDrawable() {
    return this.mCheckMarkDrawable;
  }
  
  protected void internalSetPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.internalSetPadding(paramInt1, paramInt2, paramInt3, paramInt4);
    setBasePadding(isCheckMarkAtStart());
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    updatePadding();
  }
  
  private void updatePadding() {
    int i;
    resetPaddingToInitialValues();
    if (this.mCheckMarkDrawable != null) {
      i = this.mCheckMarkWidth + this.mBasePadding;
    } else {
      i = this.mBasePadding;
    } 
    boolean bool1 = isCheckMarkAtStart();
    boolean bool = true;
    boolean bool2 = true;
    if (bool1) {
      bool1 = this.mNeedRequestlayout;
      if (this.mPaddingLeft == i)
        bool2 = false; 
      this.mNeedRequestlayout = bool1 | bool2;
      this.mPaddingLeft = i;
    } else {
      bool1 = this.mNeedRequestlayout;
      if (this.mPaddingRight != i) {
        bool2 = bool;
      } else {
        bool2 = false;
      } 
      this.mNeedRequestlayout = bool1 | bool2;
      this.mPaddingRight = i;
    } 
    if (this.mNeedRequestlayout) {
      requestLayout();
      this.mNeedRequestlayout = false;
    } 
  }
  
  private void setBasePadding(boolean paramBoolean) {
    if (paramBoolean) {
      this.mBasePadding = this.mPaddingLeft;
    } else {
      this.mBasePadding = this.mPaddingRight;
    } 
  }
  
  private boolean isCheckMarkAtStart() {
    boolean bool;
    int i = Gravity.getAbsoluteGravity(this.mCheckMarkGravity, getLayoutDirection());
    if ((i & 0x7) == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    Drawable drawable = this.mCheckMarkDrawable;
    if (drawable != null) {
      int i = getGravity() & 0x70;
      int j = drawable.getIntrinsicHeight();
      int k = 0;
      if (i != 16) {
        if (i == 80)
          k = getHeight() - j; 
      } else {
        k = (getHeight() - j) / 2;
      } 
      boolean bool = isCheckMarkAtStart();
      i = getWidth();
      int m = k + j;
      if (bool) {
        j = this.mBasePadding;
        i = this.mCheckMarkWidth + j;
      } else {
        i -= this.mBasePadding;
        j = i - this.mCheckMarkWidth;
      } 
      drawable.setBounds(this.mScrollX + j, k, this.mScrollX + i, m);
      drawable.draw(paramCanvas);
      Drawable drawable1 = getBackground();
      if (drawable1 != null)
        drawable1.setHotspotBounds(this.mScrollX + j, k, this.mScrollX + i, m); 
    } 
  }
  
  protected int[] onCreateDrawableState(int paramInt) {
    int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
    if (isChecked())
      mergeDrawableStates(arrayOfInt, CHECKED_STATE_SET); 
    return arrayOfInt;
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = this.mCheckMarkDrawable;
    if (drawable != null && drawable.isStateful() && 
      drawable.setState(getDrawableState()))
      invalidateDrawable(drawable); 
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Drawable drawable = this.mCheckMarkDrawable;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  public CharSequence getAccessibilityClassName() {
    return CheckedTextView.class.getName();
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState() {}
    
    private SavedState(CheckedTextView this$0) {
      this.checked = ((Boolean)this$0.readValue(null)).booleanValue();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeValue(Boolean.valueOf(this.checked));
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CheckedTextView.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" checked=");
      stringBuilder.append(this.checked);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    boolean checked;
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    parcelable = new SavedState();
    ((SavedState)parcelable).checked = isChecked();
    return parcelable;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    setChecked(((SavedState)paramParcelable).checked);
    requestLayout();
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    paramAccessibilityEvent.setChecked(this.mChecked);
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setCheckable(true);
    paramAccessibilityNodeInfo.setChecked(this.mChecked);
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("text:checked", isChecked());
  }
}
