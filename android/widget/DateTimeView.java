package android.widget;

import android.app.ActivityThread;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.ContentObserver;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R;
import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.JulianFields;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Date;

@RemoteView
public class DateTimeView extends TextView {
  private long mUpdateTimeMillis;
  
  private long mTimeMillis;
  
  private boolean mShowRelativeTime;
  
  private String mNowText;
  
  private LocalDateTime mLocalTime;
  
  DateFormat mLastFormat;
  
  int mLastDisplay = -1;
  
  private static final ThreadLocal<ReceiverInfo> sReceiverInfo = new ThreadLocal<>();
  
  private static final int SHOW_TIME = 0;
  
  private static final int SHOW_MONTH_DAY_YEAR = 1;
  
  public DateTimeView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public DateTimeView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DateTimeView, 0, 0);
    int i = typedArray.getIndexCount();
    for (byte b = 0; b < i; b++) {
      int j = typedArray.getIndex(b);
      if (j == 0) {
        boolean bool = typedArray.getBoolean(b, false);
        setShowRelativeTime(bool);
      } 
    } 
    typedArray.recycle();
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    ReceiverInfo receiverInfo1 = sReceiverInfo.get();
    ReceiverInfo receiverInfo2 = receiverInfo1;
    if (receiverInfo1 == null) {
      receiverInfo2 = new ReceiverInfo();
      sReceiverInfo.set(receiverInfo2);
    } 
    receiverInfo2.addView(this);
    if (this.mShowRelativeTime)
      update(); 
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    ReceiverInfo receiverInfo = sReceiverInfo.get();
    if (receiverInfo != null)
      receiverInfo.removeView(this); 
  }
  
  @RemotableViewMethod
  public void setTime(long paramLong) {
    this.mTimeMillis = paramLong;
    LocalDateTime localDateTime = toLocalDateTime(paramLong, ZoneId.systemDefault());
    this.mLocalTime = localDateTime.withSecond(0);
    update();
  }
  
  @RemotableViewMethod
  public void setShowRelativeTime(boolean paramBoolean) {
    this.mShowRelativeTime = paramBoolean;
    updateNowText();
    update();
  }
  
  public boolean isShowRelativeTime() {
    return this.mShowRelativeTime;
  }
  
  @RemotableViewMethod
  public void setVisibility(int paramInt) {
    boolean bool;
    if (paramInt != 8 && getVisibility() == 8) {
      bool = true;
    } else {
      bool = false;
    } 
    super.setVisibility(paramInt);
    if (bool)
      update(); 
  }
  
  void update() {
    DateFormat dateFormat;
    boolean bool;
    if (this.mLocalTime == null || getVisibility() == 8)
      return; 
    if (this.mShowRelativeTime) {
      updateRelativeTime();
      return;
    } 
    ZoneId zoneId = ZoneId.systemDefault();
    LocalDateTime localDateTime1 = this.mLocalTime;
    LocalDateTime localDateTime2 = LocalDateTime.of(localDateTime1.toLocalDate(), LocalTime.MIDNIGHT);
    LocalDateTime localDateTime3 = localDateTime2.plusDays(1L);
    LocalDateTime localDateTime4 = LocalDateTime.now(zoneId).withSecond(0);
    long l1 = toEpochMillis(localDateTime1.minusHours(12L), zoneId);
    long l2 = toEpochMillis(localDateTime1.plusHours(12L), zoneId);
    long l3 = toEpochMillis(localDateTime2, zoneId);
    long l4 = toEpochMillis(localDateTime3, zoneId);
    long l5 = toEpochMillis(localDateTime1, zoneId);
    long l6 = toEpochMillis(localDateTime4, zoneId);
    if ((l6 >= l3 && l6 < l4) || (l6 >= l1 && l6 < l2)) {
      bool = false;
    } else {
      bool = true;
    } 
    if (bool == this.mLastDisplay && this.mLastFormat != null) {
      dateFormat = this.mLastFormat;
    } else {
      if (bool) {
        if (bool == true) {
          dateFormat = DateFormat.getDateInstance(3);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("unknown display value: ");
          stringBuilder.append(bool);
          throw new RuntimeException(stringBuilder.toString());
        } 
      } else {
        dateFormat = getTimeFormat();
      } 
      this.mLastFormat = dateFormat;
    } 
    String str = dateFormat.format(new Date(l5));
    setText(str);
    if (!bool) {
      if (l2 <= l4)
        l2 = l4; 
      this.mUpdateTimeMillis = l2;
    } else if (this.mTimeMillis < l6) {
      this.mUpdateTimeMillis = 0L;
    } else {
      if (l1 < l3) {
        l2 = l1;
      } else {
        l2 = l3;
      } 
      this.mUpdateTimeMillis = l2;
    } 
  }
  
  private void updateRelativeTime() {
    boolean bool;
    String str;
    int i;
    long l1 = System.currentTimeMillis();
    long l2 = Math.abs(l1 - this.mTimeMillis);
    if (l1 >= this.mTimeMillis) {
      bool = true;
    } else {
      bool = false;
    } 
    if (l2 < 60000L) {
      setText(this.mNowText);
      this.mUpdateTimeMillis = this.mTimeMillis + 60000L + 1L;
      return;
    } 
    if (l2 < 3600000L) {
      int j = (int)(l2 / 60000L);
      Resources resources = getContext().getResources();
      if (bool) {
        i = 18153484;
      } else {
        i = 18153485;
      } 
      str = resources.getQuantityString(i, j);
      str = String.format(str, new Object[] { Integer.valueOf(j) });
      l2 = 60000L;
      i = j;
    } else if (l2 < 86400000L) {
      int j = (int)(l2 / 3600000L);
      Resources resources = getContext().getResources();
      if (bool) {
        i = 18153480;
      } else {
        i = 18153481;
      } 
      str = resources.getQuantityString(i, j);
      str = String.format(str, new Object[] { Integer.valueOf(j) });
      l2 = 3600000L;
      i = j;
    } else if (l2 < 31449600000L) {
      LocalDateTime localDateTime1 = this.mLocalTime;
      ZoneId zoneId = ZoneId.systemDefault();
      LocalDateTime localDateTime2 = toLocalDateTime(l1, zoneId);
      int j = Math.max(Math.abs(dayDistance(localDateTime1, localDateTime2)), 1);
      Resources resources = getContext().getResources();
      if (bool) {
        i = 18153476;
      } else {
        i = 18153477;
      } 
      str = resources.getQuantityString(i, j);
      str = String.format(str, new Object[] { Integer.valueOf(j) });
      if (bool || j != 1) {
        this.mUpdateTimeMillis = computeNextMidnight(localDateTime2, zoneId);
        l2 = -1L;
      } else {
        l2 = 86400000L;
      } 
      i = j;
    } else {
      int j = (int)(l2 / 31449600000L);
      Resources resources = getContext().getResources();
      if (bool) {
        i = 18153488;
      } else {
        i = 18153489;
      } 
      str = resources.getQuantityString(i, j);
      str = String.format(str, new Object[] { Integer.valueOf(j) });
      l2 = 31449600000L;
      i = j;
    } 
    if (l2 != -1L)
      if (bool) {
        this.mUpdateTimeMillis = this.mTimeMillis + (i + 1) * l2 + 1L;
      } else {
        this.mUpdateTimeMillis = this.mTimeMillis - i * l2 + 1L;
      }  
    setText(str);
  }
  
  private static long computeNextMidnight(LocalDateTime paramLocalDateTime, ZoneId paramZoneId) {
    LocalDate localDate = paramLocalDateTime.toLocalDate().plusDays(1L);
    LocalDateTime localDateTime = LocalDateTime.of(localDate, LocalTime.MIDNIGHT);
    return toEpochMillis(localDateTime, paramZoneId);
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    updateNowText();
    update();
  }
  
  private void updateNowText() {
    if (!this.mShowRelativeTime)
      return; 
    this.mNowText = getContext().getResources().getString(17040747);
  }
  
  private static int dayDistance(LocalDateTime paramLocalDateTime1, LocalDateTime paramLocalDateTime2) {
    long l = paramLocalDateTime2.getLong(JulianFields.JULIAN_DAY);
    TemporalField temporalField = JulianFields.JULIAN_DAY;
    return 
      (int)(l - paramLocalDateTime1.getLong(temporalField));
  }
  
  private DateFormat getTimeFormat() {
    return DateFormat.getTimeFormat(getContext());
  }
  
  void clearFormatAndUpdate() {
    this.mLastFormat = null;
    update();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (this.mShowRelativeTime) {
      int i;
      String str;
      long l1 = System.currentTimeMillis();
      long l2 = Math.abs(l1 - this.mTimeMillis);
      if (l1 >= this.mTimeMillis) {
        i = 1;
      } else {
        i = 0;
      } 
      if (l2 < 60000L) {
        str = this.mNowText;
      } else if (l2 < 3600000L) {
        int j = (int)(l2 / 60000L);
        Resources resources = getContext().getResources();
        if (i) {
          i = 18153482;
        } else {
          i = 18153483;
        } 
        str = resources.getQuantityString(i, j);
        str = String.format(str, new Object[] { Integer.valueOf(j) });
      } else if (l2 < 86400000L) {
        int j = (int)(l2 / 3600000L);
        Resources resources = getContext().getResources();
        if (i != 0) {
          i = 18153478;
        } else {
          i = 18153479;
        } 
        str = resources.getQuantityString(i, j);
        str = String.format(str, new Object[] { Integer.valueOf(j) });
      } else if (l2 < 31449600000L) {
        LocalDateTime localDateTime1 = this.mLocalTime;
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDateTime2 = toLocalDateTime(l1, zoneId);
        int j = Math.max(Math.abs(dayDistance(localDateTime1, localDateTime2)), 1);
        Resources resources = getContext().getResources();
        if (i != 0) {
          i = 18153474;
        } else {
          i = 18153475;
        } 
        str = resources.getQuantityString(i, j);
        str = String.format(str, new Object[] { Integer.valueOf(j) });
      } else {
        int j = (int)(l2 / 31449600000L);
        Resources resources = getContext().getResources();
        if (i != 0) {
          i = 18153486;
        } else {
          i = 18153487;
        } 
        str = resources.getQuantityString(i, j);
        str = String.format(str, new Object[] { Integer.valueOf(j) });
      } 
      paramAccessibilityNodeInfo.setText(str);
    } 
  }
  
  public static void setReceiverHandler(Handler paramHandler) {
    ReceiverInfo receiverInfo1 = sReceiverInfo.get();
    ReceiverInfo receiverInfo2 = receiverInfo1;
    if (receiverInfo1 == null) {
      receiverInfo2 = new ReceiverInfo();
      sReceiverInfo.set(receiverInfo2);
    } 
    receiverInfo2.setHandler(paramHandler);
  }
  
  class ReceiverInfo {
    private final ArrayList<DateTimeView> mAttachedViews = new ArrayList<>();
    
    private final BroadcastReceiver mReceiver = (BroadcastReceiver)new Object(this);
    
    private final ContentObserver mObserver = (ContentObserver)new Object(this, new Handler());
    
    private Handler mHandler = new Handler();
    
    public void addView(DateTimeView param1DateTimeView) {
      synchronized (this.mAttachedViews) {
        boolean bool = this.mAttachedViews.isEmpty();
        this.mAttachedViews.add(param1DateTimeView);
        if (bool)
          register(getApplicationContextIfAvailable(param1DateTimeView.getContext())); 
        return;
      } 
    }
    
    public void removeView(DateTimeView param1DateTimeView) {
      synchronized (this.mAttachedViews) {
        boolean bool = this.mAttachedViews.remove(param1DateTimeView);
        if (bool && this.mAttachedViews.isEmpty())
          unregister(getApplicationContextIfAvailable(param1DateTimeView.getContext())); 
        return;
      } 
    }
    
    void updateAll() {
      synchronized (this.mAttachedViews) {
        int i = this.mAttachedViews.size();
        for (byte b = 0; b < i; b++) {
          DateTimeView dateTimeView = this.mAttachedViews.get(b);
          _$$Lambda$DateTimeView$ReceiverInfo$AVLnX7U5lTcE9jLnlKKNAT1GUeI _$$Lambda$DateTimeView$ReceiverInfo$AVLnX7U5lTcE9jLnlKKNAT1GUeI = new _$$Lambda$DateTimeView$ReceiverInfo$AVLnX7U5lTcE9jLnlKKNAT1GUeI();
          this(dateTimeView);
          dateTimeView.post(_$$Lambda$DateTimeView$ReceiverInfo$AVLnX7U5lTcE9jLnlKKNAT1GUeI);
        } 
        return;
      } 
    }
    
    long getSoonestUpdateTime() {
      long l = Long.MAX_VALUE;
      synchronized (this.mAttachedViews) {
        int i = this.mAttachedViews.size();
        for (byte b = 0; b < i; b++, l = l2) {
          long l1 = (this.mAttachedViews.get(b)).mUpdateTimeMillis;
          long l2 = l;
          if (l1 < l)
            l2 = l1; 
        } 
        return l;
      } 
    }
    
    static final Context getApplicationContextIfAvailable(Context param1Context) {
      param1Context = param1Context.getApplicationContext();
      if (param1Context == null)
        param1Context = ActivityThread.currentApplication().getApplicationContext(); 
      return param1Context;
    }
    
    void register(Context param1Context) {
      IntentFilter intentFilter = new IntentFilter();
      intentFilter.addAction("android.intent.action.TIME_TICK");
      intentFilter.addAction("android.intent.action.TIME_SET");
      intentFilter.addAction("android.intent.action.CONFIGURATION_CHANGED");
      intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
      param1Context.registerReceiver(this.mReceiver, intentFilter, null, this.mHandler);
    }
    
    void unregister(Context param1Context) {
      param1Context.unregisterReceiver(this.mReceiver);
    }
    
    public void setHandler(Handler param1Handler) {
      this.mHandler = param1Handler;
      synchronized (this.mAttachedViews) {
        if (!this.mAttachedViews.isEmpty()) {
          unregister(((DateTimeView)this.mAttachedViews.get(0)).getContext());
          register(((DateTimeView)this.mAttachedViews.get(0)).getContext());
        } 
        return;
      } 
    }
    
    private ReceiverInfo() {}
  }
  
  private static LocalDateTime toLocalDateTime(long paramLong, ZoneId paramZoneId) {
    Instant instant = Instant.ofEpochMilli(paramLong);
    return LocalDateTime.ofInstant(instant, paramZoneId);
  }
  
  private static long toEpochMillis(LocalDateTime paramLocalDateTime, ZoneId paramZoneId) {
    Instant instant = paramLocalDateTime.toInstant(paramZoneId.getRules().getOffset(paramLocalDateTime));
    return instant.toEpochMilli();
  }
}
