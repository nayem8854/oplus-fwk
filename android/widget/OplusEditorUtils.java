package android.widget;

import android.view.Menu;

public class OplusEditorUtils {
  public static final int FLAG_DISABLE_CUT_PASTE = 32;
  
  public static final int FLAG_DISABLE_INSERT_MENU = 4;
  
  public static final int FLAG_DISABLE_MENU = 1;
  
  public static final int FLAG_DISABLE_REPLACE = 16;
  
  public static final int FLAG_DISABLE_SELECT_MENU = 2;
  
  public static final int FLAG_DISABLE_SHARE = 8;
  
  public static final int FLAG_HIDE_MENU = 128;
  
  public static final int FLAG_SELECT_ALL = 64;
  
  private Editor mEditor;
  
  private int mMenuFlag = 0;
  
  private TextView mTextView;
  
  public OplusEditorUtils(TextView paramTextView) {
    this.mTextView = paramTextView;
  }
  
  public OplusEditorUtils(Editor paramEditor) {
    this.mEditor = paramEditor;
  }
  
  public void setMenuFlag(int paramInt) {
    this.mMenuFlag = paramInt = this.mMenuFlag | paramInt;
    if ((paramInt & 0x80) == 128)
      this.mEditor.stopTextActionMode(); 
  }
  
  public boolean isMenuEnabled() {
    if ((this.mMenuFlag & 0x1) == 1)
      return false; 
    return true;
  }
  
  public boolean isSelectMenuEnabled() {
    if ((this.mMenuFlag & 0x2) == 2)
      return false; 
    return true;
  }
  
  public boolean isInsertMenuEnabled() {
    if ((this.mMenuFlag & 0x4) == 4)
      return false; 
    return true;
  }
  
  public boolean isShareEnabled() {
    if ((this.mMenuFlag & 0x8) == 8)
      return false; 
    return true;
  }
  
  public boolean needAllSelected() {
    if ((this.mMenuFlag & 0x40) == 64)
      return true; 
    return false;
  }
  
  public boolean isCutAndPasteEnabled() {
    if ((this.mMenuFlag & 0x20) == 32)
      return false; 
    return true;
  }
  
  public void updateItems(Menu paramMenu) {
    if (!isCutAndPasteEnabled()) {
      paramMenu.removeItem(16908320);
      paramMenu.removeItem(16908322);
    } 
    if (!isShareEnabled())
      paramMenu.removeItem(16908341); 
  }
  
  public OplusEditorUtils() {}
}
