package android.widget;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class AdapterViewAnimator extends AdapterView<Adapter> implements RemoteViewsAdapter.RemoteAdapterConnectionCallback, Advanceable {
  int mWhichChild = 0;
  
  private int mRestoreWhichChild = -1;
  
  boolean mAnimateFirstTime = true;
  
  int mActiveOffset = 0;
  
  int mMaxNumActiveViews = 1;
  
  HashMap<Integer, ViewAndMetaData> mViewsMap = new HashMap<>();
  
  int mCurrentWindowStart = 0;
  
  int mCurrentWindowEnd = -1;
  
  int mCurrentWindowStartUnbounded = 0;
  
  boolean mDeferNotifyDataSetChanged = false;
  
  boolean mFirstTime = true;
  
  boolean mLoopViews = true;
  
  int mReferenceChildWidth = -1;
  
  int mReferenceChildHeight = -1;
  
  private int mTouchMode = 0;
  
  private static final int DEFAULT_ANIMATION_DURATION = 200;
  
  private static final String TAG = "RemoteViewAnimator";
  
  static final int TOUCH_MODE_DOWN_IN_CURRENT_VIEW = 1;
  
  static final int TOUCH_MODE_HANDLED = 2;
  
  static final int TOUCH_MODE_NONE = 0;
  
  Adapter mAdapter;
  
  AdapterView<Adapter>.AdapterDataSetObserver mDataSetObserver;
  
  ObjectAnimator mInAnimation;
  
  ObjectAnimator mOutAnimation;
  
  private Runnable mPendingCheckForTap;
  
  ArrayList<Integer> mPreviousViews;
  
  RemoteViewsAdapter mRemoteViewsAdapter;
  
  public AdapterViewAnimator(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public AdapterViewAnimator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public AdapterViewAnimator(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AdapterViewAnimator(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AdapterViewAnimator, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.AdapterViewAnimator, paramAttributeSet, typedArray, paramInt1, paramInt2);
    paramInt1 = typedArray.getResourceId(0, 0);
    if (paramInt1 > 0) {
      setInAnimation(paramContext, paramInt1);
    } else {
      setInAnimation(getDefaultInAnimation());
    } 
    paramInt1 = typedArray.getResourceId(1, 0);
    if (paramInt1 > 0) {
      setOutAnimation(paramContext, paramInt1);
    } else {
      setOutAnimation(getDefaultOutAnimation());
    } 
    boolean bool = typedArray.getBoolean(2, true);
    setAnimateFirstView(bool);
    this.mLoopViews = typedArray.getBoolean(3, false);
    typedArray.recycle();
    initViewAnimator();
  }
  
  private void initViewAnimator() {
    this.mPreviousViews = new ArrayList<>();
  }
  
  class ViewAndMetaData {
    int adapterPosition;
    
    long itemId;
    
    int relativeIndex;
    
    final AdapterViewAnimator this$0;
    
    View view;
    
    ViewAndMetaData(View param1View, int param1Int1, int param1Int2, long param1Long) {
      this.view = param1View;
      this.relativeIndex = param1Int1;
      this.adapterPosition = param1Int2;
      this.itemId = param1Long;
    }
  }
  
  void configureViewAnimator(int paramInt1, int paramInt2) {
    this.mMaxNumActiveViews = paramInt1;
    this.mActiveOffset = paramInt2;
    this.mPreviousViews.clear();
    this.mViewsMap.clear();
    removeAllViewsInLayout();
    this.mCurrentWindowStart = 0;
    this.mCurrentWindowEnd = -1;
  }
  
  void transformViewForTransition(int paramInt1, int paramInt2, View paramView, boolean paramBoolean) {
    if (paramInt1 == -1) {
      this.mInAnimation.setTarget(paramView);
      this.mInAnimation.start();
    } else if (paramInt2 == -1) {
      this.mOutAnimation.setTarget(paramView);
      this.mOutAnimation.start();
    } 
  }
  
  ObjectAnimator getDefaultInAnimation() {
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(null, "alpha", new float[] { 0.0F, 1.0F });
    objectAnimator.setDuration(200L);
    return objectAnimator;
  }
  
  ObjectAnimator getDefaultOutAnimation() {
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(null, "alpha", new float[] { 1.0F, 0.0F });
    objectAnimator.setDuration(200L);
    return objectAnimator;
  }
  
  @RemotableViewMethod
  public void setDisplayedChild(int paramInt) {
    setDisplayedChild(paramInt, true);
  }
  
  private void setDisplayedChild(int paramInt, boolean paramBoolean) {
    if (this.mAdapter != null) {
      this.mWhichChild = paramInt;
      int i = getWindowSize();
      boolean bool = false;
      if (paramInt >= i) {
        if (this.mLoopViews) {
          paramInt = 0;
        } else {
          paramInt = getWindowSize() - 1;
        } 
        this.mWhichChild = paramInt;
      } else if (paramInt < 0) {
        if (this.mLoopViews) {
          paramInt = getWindowSize() - 1;
        } else {
          paramInt = 0;
        } 
        this.mWhichChild = paramInt;
      } 
      paramInt = bool;
      if (getFocusedChild() != null)
        paramInt = 1; 
      showOnly(this.mWhichChild, paramBoolean);
      if (paramInt != 0)
        requestFocus(2); 
    } 
  }
  
  void applyTransformForChildAtIndex(View paramView, int paramInt) {}
  
  public int getDisplayedChild() {
    return this.mWhichChild;
  }
  
  public void showNext() {
    setDisplayedChild(this.mWhichChild + 1);
  }
  
  public void showPrevious() {
    setDisplayedChild(this.mWhichChild - 1);
  }
  
  int modulo(int paramInt1, int paramInt2) {
    if (paramInt2 > 0)
      return (paramInt1 % paramInt2 + paramInt2) % paramInt2; 
    return 0;
  }
  
  View getViewAtRelativeIndex(int paramInt) {
    if (paramInt >= 0 && paramInt <= getNumActiveViews() - 1 && this.mAdapter != null) {
      paramInt = modulo(this.mCurrentWindowStartUnbounded + paramInt, getWindowSize());
      if (this.mViewsMap.get(Integer.valueOf(paramInt)) != null)
        return ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(paramInt))).view; 
    } 
    return null;
  }
  
  int getNumActiveViews() {
    if (this.mAdapter != null)
      return Math.min(getCount() + 1, this.mMaxNumActiveViews); 
    return this.mMaxNumActiveViews;
  }
  
  int getWindowSize() {
    if (this.mAdapter != null) {
      int i = getCount();
      if (i <= getNumActiveViews() && this.mLoopViews)
        return this.mMaxNumActiveViews * i; 
      return i;
    } 
    return 0;
  }
  
  private ViewAndMetaData getMetaDataForChild(View paramView) {
    for (ViewAndMetaData viewAndMetaData : this.mViewsMap.values()) {
      if (viewAndMetaData.view == paramView)
        return viewAndMetaData; 
    } 
    return null;
  }
  
  ViewGroup.LayoutParams createOrReuseLayoutParams(View paramView) {
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    if (layoutParams != null)
      return layoutParams; 
    return new ViewGroup.LayoutParams(0, 0);
  }
  
  void refreshChildren() {
    if (this.mAdapter == null)
      return; 
    for (int i = this.mCurrentWindowStart; i <= this.mCurrentWindowEnd; i++) {
      int j = modulo(i, getWindowSize());
      int k = getCount();
      View view = this.mAdapter.getView(modulo(i, k), null, this);
      if (view.getImportantForAccessibility() == 0)
        view.setImportantForAccessibility(1); 
      if (this.mViewsMap.containsKey(Integer.valueOf(j))) {
        FrameLayout frameLayout = (FrameLayout)((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(j))).view;
        if (view != null) {
          frameLayout.removeAllViewsInLayout();
          frameLayout.addView(view);
        } 
      } 
    } 
  }
  
  FrameLayout getFrameForChild() {
    return new FrameLayout(this.mContext);
  }
  
  void showOnly(int paramInt, boolean paramBoolean) {
    int i2;
    if (this.mAdapter == null)
      return; 
    int i = getCount();
    if (i == 0)
      return; 
    int j;
    for (j = 0; j < this.mPreviousViews.size(); j++) {
      View view = ((ViewAndMetaData)this.mViewsMap.get(this.mPreviousViews.get(j))).view;
      this.mViewsMap.remove(this.mPreviousViews.get(j));
      view.clearAnimation();
      if (view instanceof ViewGroup) {
        ViewGroup viewGroup = (ViewGroup)view;
        viewGroup.removeAllViewsInLayout();
      } 
      applyTransformForChildAtIndex(view, -1);
      removeViewInLayout(view);
    } 
    this.mPreviousViews.clear();
    int k = paramInt - this.mActiveOffset;
    int m = getNumActiveViews() + k - 1;
    paramInt = Math.max(0, k);
    j = Math.min(i - 1, m);
    if (this.mLoopViews) {
      paramInt = k;
      j = m;
    } 
    int n = modulo(paramInt, getWindowSize());
    int i1 = modulo(j, getWindowSize());
    if (n > i1) {
      i2 = 1;
    } else {
      i2 = 0;
    } 
    for (Integer integer : this.mViewsMap.keySet()) {
      int i3;
      byte b = 0;
      if (!i2 && (integer.intValue() < n || integer.intValue() > i1)) {
        i3 = 1;
      } else {
        i3 = b;
        if (i2) {
          i3 = b;
          if (integer.intValue() > i1) {
            i3 = b;
            if (integer.intValue() < n)
              i3 = 1; 
          } 
        } 
      } 
      if (i3) {
        View view = ((ViewAndMetaData)this.mViewsMap.get(integer)).view;
        i3 = ((ViewAndMetaData)this.mViewsMap.get(integer)).relativeIndex;
        this.mPreviousViews.add(integer);
        transformViewForTransition(i3, -1, view, paramBoolean);
      } 
    } 
    if (paramInt != this.mCurrentWindowStart || j != this.mCurrentWindowEnd || k != this.mCurrentWindowStartUnbounded) {
      for (int i3 = paramInt, i4 = m; i3 <= i; i3++) {
        int i5 = modulo(i3, getWindowSize());
        if (this.mViewsMap.containsKey(Integer.valueOf(i5))) {
          i1 = ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i5))).relativeIndex;
        } else {
          i1 = -1;
        } 
        int i6 = i3 - k;
        if (this.mViewsMap.containsKey(Integer.valueOf(i5)) && !this.mPreviousViews.contains(Integer.valueOf(i5))) {
          n = 1;
        } else {
          n = 0;
        } 
        if (n != 0) {
          View view = ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i5))).view;
          ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i5))).relativeIndex = i6;
          applyTransformForChildAtIndex(view, i6);
          transformViewForTransition(i1, i6, view, paramBoolean);
        } else {
          i1 = modulo(i3, i2);
          View view = this.mAdapter.getView(i1, null, this);
          long l = this.mAdapter.getItemId(i1);
          FrameLayout frameLayout = getFrameForChild();
          if (view != null)
            frameLayout.addView(view); 
          this.mViewsMap.put(Integer.valueOf(i5), new ViewAndMetaData(frameLayout, i6, i1, l));
          addChild(frameLayout);
          applyTransformForChildAtIndex(frameLayout, i6);
          transformViewForTransition(-1, i6, frameLayout, paramBoolean);
        } 
        ((ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i5))).view.bringToFront();
      } 
      this.mCurrentWindowStart = paramInt;
      this.mCurrentWindowEnd = i;
      this.mCurrentWindowStartUnbounded = k;
      if (this.mRemoteViewsAdapter != null) {
        paramInt = modulo(paramInt, i2);
        j = modulo(this.mCurrentWindowEnd, i2);
        this.mRemoteViewsAdapter.setVisibleRangeHint(paramInt, j);
      } 
    } 
    requestLayout();
    invalidate();
  }
  
  private void addChild(View paramView) {
    addViewInLayout(paramView, -1, createOrReuseLayoutParams(paramView));
    if (this.mReferenceChildWidth == -1 || this.mReferenceChildHeight == -1) {
      int i = View.MeasureSpec.makeMeasureSpec(0, 0);
      paramView.measure(i, i);
      this.mReferenceChildWidth = paramView.getMeasuredWidth();
      this.mReferenceChildHeight = paramView.getMeasuredHeight();
    } 
  }
  
  void showTapFeedback(View paramView) {
    paramView.setPressed(true);
  }
  
  void hideTapFeedback(View paramView) {
    paramView.setPressed(false);
  }
  
  void cancelHandleClick() {
    View view = getCurrentView();
    if (view != null)
      hideTapFeedback(view); 
    this.mTouchMode = 0;
  }
  
  class CheckForTap implements Runnable {
    final AdapterViewAnimator this$0;
    
    public void run() {
      if (AdapterViewAnimator.this.mTouchMode == 1) {
        View view = AdapterViewAnimator.this.getCurrentView();
        AdapterViewAnimator.this.showTapFeedback(view);
      } 
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    Object object;
    boolean bool3;
    int i = paramMotionEvent.getAction();
    boolean bool1 = false, bool2 = false;
    if (i != 0) {
      if (i != 1) {
        if (i != 3) {
          bool3 = bool1;
        } else {
          object = getCurrentView();
          if (object != null)
            hideTapFeedback((View)object); 
          this.mTouchMode = 0;
          bool3 = bool1;
        } 
      } else {
        bool3 = bool2;
        if (this.mTouchMode == 1) {
          View view = getCurrentView();
          ViewAndMetaData viewAndMetaData = getMetaDataForChild(view);
          bool3 = bool2;
          if (view != null) {
            bool3 = bool2;
            if (isTransformedTouchPointInView(object.getX(), object.getY(), view, (PointF)null)) {
              Handler handler = getHandler();
              if (handler != null)
                handler.removeCallbacks(this.mPendingCheckForTap); 
              showTapFeedback(view);
              object = new Object(this, view, viewAndMetaData);
              long l = ViewConfiguration.getPressedStateDuration();
              postDelayed((Runnable)object, l);
              bool3 = true;
            } 
          } 
        } 
        this.mTouchMode = 0;
      } 
    } else {
      View view = getCurrentView();
      bool3 = bool1;
      if (view != null) {
        bool3 = bool1;
        if (isTransformedTouchPointInView(object.getX(), object.getY(), view, (PointF)null)) {
          if (this.mPendingCheckForTap == null)
            this.mPendingCheckForTap = new CheckForTap(); 
          this.mTouchMode = 1;
          postDelayed(this.mPendingCheckForTap, ViewConfiguration.getTapTimeout());
          bool3 = bool1;
        } 
      } 
    } 
    return bool3;
  }
  
  private void measureChildren() {
    int i = getChildCount();
    int j = getMeasuredWidth(), k = this.mPaddingLeft, m = this.mPaddingRight;
    int n = getMeasuredHeight(), i1 = this.mPaddingTop, i2 = this.mPaddingBottom;
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      int i3 = View.MeasureSpec.makeMeasureSpec(j - k - m, 1073741824);
      int i4 = View.MeasureSpec.makeMeasureSpec(n - i1 - i2, 1073741824);
      view.measure(i3, i4);
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int n, i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt2);
    int k = View.MeasureSpec.getMode(paramInt1);
    int m = View.MeasureSpec.getMode(paramInt2);
    paramInt1 = this.mReferenceChildWidth;
    paramInt2 = 0;
    if (paramInt1 != -1 && this.mReferenceChildHeight != -1) {
      n = 1;
    } else {
      n = 0;
    } 
    if (m == 0) {
      if (n) {
        paramInt1 = this.mReferenceChildHeight + this.mPaddingTop + this.mPaddingBottom;
      } else {
        paramInt1 = 0;
      } 
    } else {
      paramInt1 = j;
      if (m == Integer.MIN_VALUE) {
        paramInt1 = j;
        if (n) {
          paramInt1 = this.mReferenceChildHeight + this.mPaddingTop + this.mPaddingBottom;
          if (paramInt1 > j)
            paramInt1 = j | 0x1000000; 
        } 
      } 
    } 
    if (k == 0) {
      if (n) {
        paramInt2 = this.mReferenceChildWidth;
        n = this.mPaddingLeft;
        paramInt2 = this.mPaddingRight + paramInt2 + n;
      } 
    } else {
      paramInt2 = i;
      if (m == Integer.MIN_VALUE) {
        paramInt2 = i;
        if (n != 0) {
          paramInt2 = this.mReferenceChildWidth + this.mPaddingLeft + this.mPaddingRight;
          if (paramInt2 > i)
            paramInt2 = i | 0x1000000; 
        } 
      } 
    } 
    setMeasuredDimension(paramInt2, paramInt1);
    measureChildren();
  }
  
  void checkForAndHandleDataChanged() {
    boolean bool = this.mDataChanged;
    if (bool)
      post((Runnable)new Object(this)); 
    this.mDataChanged = false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    checkForAndHandleDataChanged();
    paramInt2 = getChildCount();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      View view = getChildAt(paramInt1);
      paramInt4 = this.mPaddingLeft;
      paramInt3 = view.getMeasuredWidth();
      int i = this.mPaddingTop, j = view.getMeasuredHeight();
      view.layout(this.mPaddingLeft, this.mPaddingTop, paramInt4 + paramInt3, i + j);
    } 
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState(int param1Int) {
      this.whichChild = param1Int;
    }
    
    private SavedState(AdapterViewAnimator this$0) {
      this.whichChild = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.whichChild);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AdapterViewAnimator.SavedState{ whichChild = ");
      stringBuilder.append(this.whichChild);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    int whichChild;
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    RemoteViewsAdapter remoteViewsAdapter = this.mRemoteViewsAdapter;
    if (remoteViewsAdapter != null)
      remoteViewsAdapter.saveRemoteViewsCache(); 
    return new SavedState(this.mWhichChild);
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    int i = ((SavedState)paramParcelable).whichChild;
    if (this.mRemoteViewsAdapter != null && this.mAdapter == null) {
      this.mRestoreWhichChild = i;
    } else {
      setDisplayedChild(this.mWhichChild, false);
    } 
  }
  
  public View getCurrentView() {
    return getViewAtRelativeIndex(this.mActiveOffset);
  }
  
  public ObjectAnimator getInAnimation() {
    return this.mInAnimation;
  }
  
  public void setInAnimation(ObjectAnimator paramObjectAnimator) {
    this.mInAnimation = paramObjectAnimator;
  }
  
  public ObjectAnimator getOutAnimation() {
    return this.mOutAnimation;
  }
  
  public void setOutAnimation(ObjectAnimator paramObjectAnimator) {
    this.mOutAnimation = paramObjectAnimator;
  }
  
  public void setInAnimation(Context paramContext, int paramInt) {
    setInAnimation((ObjectAnimator)AnimatorInflater.loadAnimator(paramContext, paramInt));
  }
  
  public void setOutAnimation(Context paramContext, int paramInt) {
    setOutAnimation((ObjectAnimator)AnimatorInflater.loadAnimator(paramContext, paramInt));
  }
  
  public void setAnimateFirstView(boolean paramBoolean) {
    this.mAnimateFirstTime = paramBoolean;
  }
  
  public int getBaseline() {
    int i;
    if (getCurrentView() != null) {
      i = getCurrentView().getBaseline();
    } else {
      i = super.getBaseline();
    } 
    return i;
  }
  
  public Adapter getAdapter() {
    return this.mAdapter;
  }
  
  public void setAdapter(Adapter paramAdapter) {
    Adapter adapter = this.mAdapter;
    if (adapter != null) {
      AdapterView<Adapter>.AdapterDataSetObserver adapterDataSetObserver = this.mDataSetObserver;
      if (adapterDataSetObserver != null)
        adapter.unregisterDataSetObserver(adapterDataSetObserver); 
    } 
    this.mAdapter = paramAdapter;
    checkFocus();
    if (this.mAdapter != null) {
      AdapterView.AdapterDataSetObserver adapterDataSetObserver = new AdapterView.AdapterDataSetObserver(this);
      this.mAdapter.registerDataSetObserver(adapterDataSetObserver);
      this.mItemCount = this.mAdapter.getCount();
    } 
    setFocusable(true);
    this.mWhichChild = 0;
    showOnly(0, false);
  }
  
  @RemotableViewMethod(asyncImpl = "setRemoteViewsAdapterAsync")
  public void setRemoteViewsAdapter(Intent paramIntent) {
    setRemoteViewsAdapter(paramIntent, false);
  }
  
  public Runnable setRemoteViewsAdapterAsync(Intent paramIntent) {
    return new RemoteViewsAdapter.AsyncRemoteAdapterAction(this, paramIntent);
  }
  
  public void setRemoteViewsAdapter(Intent paramIntent, boolean paramBoolean) {
    if (this.mRemoteViewsAdapter != null) {
      Intent.FilterComparison filterComparison1 = new Intent.FilterComparison(paramIntent);
      RemoteViewsAdapter remoteViewsAdapter1 = this.mRemoteViewsAdapter;
      Intent.FilterComparison filterComparison2 = new Intent.FilterComparison(remoteViewsAdapter1.getRemoteViewsServiceIntent());
      if (filterComparison1.equals(filterComparison2))
        return; 
    } 
    this.mDeferNotifyDataSetChanged = false;
    RemoteViewsAdapter remoteViewsAdapter = new RemoteViewsAdapter(getContext(), paramIntent, this, paramBoolean);
    if (remoteViewsAdapter.isDataReady())
      setAdapter(this.mRemoteViewsAdapter); 
  }
  
  public void setRemoteViewsOnClickHandler(RemoteViews.OnClickHandler paramOnClickHandler) {
    RemoteViewsAdapter remoteViewsAdapter = this.mRemoteViewsAdapter;
    if (remoteViewsAdapter != null)
      remoteViewsAdapter.setRemoteViewsOnClickHandler(paramOnClickHandler); 
  }
  
  public void setSelection(int paramInt) {
    setDisplayedChild(paramInt);
  }
  
  public View getSelectedView() {
    return getViewAtRelativeIndex(this.mActiveOffset);
  }
  
  public void deferNotifyDataSetChanged() {
    this.mDeferNotifyDataSetChanged = true;
  }
  
  public boolean onRemoteAdapterConnected() {
    RemoteViewsAdapter remoteViewsAdapter = this.mRemoteViewsAdapter;
    if (remoteViewsAdapter != this.mAdapter) {
      setAdapter(remoteViewsAdapter);
      if (this.mDeferNotifyDataSetChanged) {
        this.mRemoteViewsAdapter.notifyDataSetChanged();
        this.mDeferNotifyDataSetChanged = false;
      } 
      int i = this.mRestoreWhichChild;
      if (i > -1) {
        setDisplayedChild(i, false);
        this.mRestoreWhichChild = -1;
      } 
      return false;
    } 
    if (remoteViewsAdapter != null) {
      remoteViewsAdapter.superNotifyDataSetChanged();
      return true;
    } 
    return false;
  }
  
  public void onRemoteAdapterDisconnected() {}
  
  public void advance() {
    showNext();
  }
  
  public void fyiWillBeAdvancedByHostKThx() {}
  
  public CharSequence getAccessibilityClassName() {
    return AdapterViewAnimator.class.getName();
  }
}
