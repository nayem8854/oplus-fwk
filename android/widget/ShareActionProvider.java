package android.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class ShareActionProvider extends ActionProvider {
  private static final int DEFAULT_INITIAL_ACTIVITY_COUNT = 4;
  
  public static final String DEFAULT_SHARE_HISTORY_FILE_NAME = "share_history.xml";
  
  private final Context mContext;
  
  private int mMaxShownActivityCount = 4;
  
  private ActivityChooserModel.OnChooseActivityListener mOnChooseActivityListener;
  
  private final ShareMenuItemOnMenuItemClickListener mOnMenuItemClickListener = new ShareMenuItemOnMenuItemClickListener();
  
  private OnShareTargetSelectedListener mOnShareTargetSelectedListener;
  
  private String mShareHistoryFileName = "share_history.xml";
  
  public ShareActionProvider(Context paramContext) {
    super(paramContext);
    this.mContext = paramContext;
  }
  
  public void setOnShareTargetSelectedListener(OnShareTargetSelectedListener paramOnShareTargetSelectedListener) {
    this.mOnShareTargetSelectedListener = paramOnShareTargetSelectedListener;
    setActivityChooserPolicyIfNeeded();
  }
  
  public View onCreateActionView() {
    ActivityChooserView activityChooserView = new ActivityChooserView(this.mContext);
    if (!activityChooserView.isInEditMode()) {
      ActivityChooserModel activityChooserModel = ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName);
      activityChooserView.setActivityChooserModel(activityChooserModel);
    } 
    TypedValue typedValue = new TypedValue();
    this.mContext.getTheme().resolveAttribute(16843897, typedValue, true);
    Drawable drawable = this.mContext.getDrawable(typedValue.resourceId);
    activityChooserView.setExpandActivityOverflowButtonDrawable(drawable);
    activityChooserView.setProvider(this);
    activityChooserView.setDefaultActionButtonContentDescription(17041279);
    activityChooserView.setExpandActivityOverflowButtonContentDescription(17041278);
    return activityChooserView;
  }
  
  public boolean hasSubMenu() {
    return true;
  }
  
  public void onPrepareSubMenu(SubMenu paramSubMenu) {
    paramSubMenu.clear();
    ActivityChooserModel activityChooserModel = ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName);
    PackageManager packageManager = this.mContext.getPackageManager();
    int i = activityChooserModel.getActivityCount();
    int j = Math.min(i, this.mMaxShownActivityCount);
    byte b;
    for (b = 0; b < j; b++) {
      ResolveInfo resolveInfo = activityChooserModel.getActivity(b);
      MenuItem menuItem2 = paramSubMenu.add(0, b, b, resolveInfo.loadLabel(packageManager));
      MenuItem menuItem1 = menuItem2.setIcon(resolveInfo.loadIcon(packageManager));
      ShareMenuItemOnMenuItemClickListener shareMenuItemOnMenuItemClickListener = this.mOnMenuItemClickListener;
      menuItem1.setOnMenuItemClickListener(shareMenuItemOnMenuItemClickListener);
    } 
    if (j < i) {
      Context context = this.mContext;
      String str = context.getString(17039596);
      paramSubMenu = paramSubMenu.addSubMenu(0, j, j, str);
      for (b = 0; b < i; b++) {
        ResolveInfo resolveInfo = activityChooserModel.getActivity(b);
        MenuItem menuItem = paramSubMenu.add(0, b, b, resolveInfo.loadLabel(packageManager));
        menuItem = menuItem.setIcon(resolveInfo.loadIcon(packageManager));
        ShareMenuItemOnMenuItemClickListener shareMenuItemOnMenuItemClickListener = this.mOnMenuItemClickListener;
        menuItem.setOnMenuItemClickListener(shareMenuItemOnMenuItemClickListener);
      } 
    } 
  }
  
  public void setShareHistoryFileName(String paramString) {
    this.mShareHistoryFileName = paramString;
    setActivityChooserPolicyIfNeeded();
  }
  
  public void setShareIntent(Intent paramIntent) {
    if (paramIntent != null) {
      String str = paramIntent.getAction();
      if ("android.intent.action.SEND".equals(str) || "android.intent.action.SEND_MULTIPLE".equals(str))
        paramIntent.addFlags(134742016); 
    } 
    ActivityChooserModel activityChooserModel = ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName);
    activityChooserModel.setIntent(paramIntent);
  }
  
  private class ShareMenuItemOnMenuItemClickListener implements MenuItem.OnMenuItemClickListener {
    final ShareActionProvider this$0;
    
    private ShareMenuItemOnMenuItemClickListener() {}
    
    public boolean onMenuItemClick(MenuItem param1MenuItem) {
      Context context = ShareActionProvider.this.mContext;
      ShareActionProvider shareActionProvider = ShareActionProvider.this;
      String str = shareActionProvider.mShareHistoryFileName;
      ActivityChooserModel activityChooserModel = ActivityChooserModel.get(context, str);
      int i = param1MenuItem.getItemId();
      Intent intent = activityChooserModel.chooseActivity(i);
      if (intent != null) {
        String str1 = intent.getAction();
        if ("android.intent.action.SEND".equals(str1) || 
          "android.intent.action.SEND_MULTIPLE".equals(str1))
          intent.addFlags(134742016); 
        ShareActionProvider.this.mContext.startActivity(intent);
      } 
      return true;
    }
  }
  
  private void setActivityChooserPolicyIfNeeded() {
    if (this.mOnShareTargetSelectedListener == null)
      return; 
    if (this.mOnChooseActivityListener == null)
      this.mOnChooseActivityListener = new ShareActivityChooserModelPolicy(); 
    ActivityChooserModel activityChooserModel = ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName);
    activityChooserModel.setOnChooseActivityListener(this.mOnChooseActivityListener);
  }
  
  private class ShareActivityChooserModelPolicy implements ActivityChooserModel.OnChooseActivityListener {
    final ShareActionProvider this$0;
    
    private ShareActivityChooserModelPolicy() {}
    
    public boolean onChooseActivity(ActivityChooserModel param1ActivityChooserModel, Intent param1Intent) {
      if (ShareActionProvider.this.mOnShareTargetSelectedListener != null)
        ShareActionProvider.this.mOnShareTargetSelectedListener.onShareTargetSelected(ShareActionProvider.this, param1Intent); 
      return false;
    }
  }
  
  class OnShareTargetSelectedListener {
    public abstract boolean onShareTargetSelected(ShareActionProvider param1ShareActionProvider, Intent param1Intent);
  }
}
