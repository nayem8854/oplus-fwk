package android.widget;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.view.Display;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.oplus.util.OplusContextUtil;

public class SpringOverScroller extends OverScroller {
  private static final int FLING_MODE = 1;
  
  public static final float OPLUS_FLING_FRICTION_FAST = 0.76F;
  
  public static final float OPLUS_FLING_FRICTION_LIST_OPTIMIZE = 0.008F;
  
  public static final float OPLUS_FLING_FRICTION_NORMAL = 1.06F;
  
  public static final int OPLUS_FLING_MODE_FAST = 0;
  
  public static final int OPLUS_FLING_MODE_NORMAL = 1;
  
  private static final int REST_MODE = 2;
  
  private static final int SCROLL_DEFAULT_DURATION = 250;
  
  private static final int SCROLL_MODE = 0;
  
  private static final float SOLVER_TIMESTEP_SEC = 0.016F;
  
  private Interpolator mInterpolator;
  
  private int mMode;
  
  private ReboundOverScroller mScrollerX;
  
  private ReboundOverScroller mScrollerY;
  
  public static OverScroller newInstance(Context paramContext) {
    OverScroller overScroller;
    if (OplusContextUtil.isOplusOSStyle(paramContext)) {
      overScroller = new SpringOverScroller(paramContext);
    } else {
      overScroller = new OverScroller((Context)overScroller);
    } 
    return overScroller;
  }
  
  public static OverScroller newInstance(Context paramContext, boolean paramBoolean) {
    OverScroller overScroller;
    if (OplusContextUtil.isOplusOSStyle(paramContext)) {
      overScroller = new SpringOverScroller(paramContext);
      SpringOverScroller springOverScroller = (SpringOverScroller)overScroller;
      ReboundOverScroller.access$002(springOverScroller.mScrollerX, paramBoolean);
      ReboundOverScroller.access$002(springOverScroller.mScrollerY, paramBoolean);
    } else {
      overScroller = new OverScroller((Context)overScroller);
    } 
    return overScroller;
  }
  
  public SpringOverScroller(Context paramContext, Interpolator paramInterpolator) {
    super(paramContext, paramInterpolator);
    float f;
    this.mMode = 2;
    DisplayManager displayManager = (DisplayManager)paramContext.getSystemService(DisplayManager.class);
    Display display = displayManager.getDisplay(0);
    if (display == null) {
      f = 0.016F;
    } else {
      f = Math.round(10000.0F / display.getRefreshRate()) / 10000.0F;
    } 
    this.mScrollerX = new ReboundOverScroller(f);
    this.mScrollerY = new ReboundOverScroller(f);
    if (paramInterpolator == null) {
      this.mInterpolator = new Scroller.ViscousFluidInterpolator();
    } else {
      this.mInterpolator = paramInterpolator;
    } 
  }
  
  public SpringOverScroller(Context paramContext) {
    this(paramContext, null);
  }
  
  public void setFlingFriction(float paramFloat) {
    ReboundOverScroller.access$102(this.mScrollerX, paramFloat);
    ReboundOverScroller.access$102(this.mScrollerY, paramFloat);
  }
  
  public void setInterpolator(Interpolator paramInterpolator) {
    if (paramInterpolator == null) {
      this.mInterpolator = new Scroller.ViscousFluidInterpolator();
    } else {
      this.mInterpolator = paramInterpolator;
    } 
  }
  
  public boolean computeScrollOffset() {
    if (isOplusFinished())
      return false; 
    int i = this.mMode;
    if (i != 0) {
      if (i == 1)
        if (!this.mScrollerX.update() && !this.mScrollerY.update())
          abortAnimation();  
    } else {
      long l = AnimationUtils.currentAnimationTimeMillis();
      l -= this.mScrollerX.mScrollStartTime;
      i = this.mScrollerX.mScrollDuration;
      if (l < i) {
        float f = this.mInterpolator.getInterpolation((float)l / i);
        this.mScrollerX.updateScroll(f);
        this.mScrollerY.updateScroll(f);
      } else {
        this.mScrollerX.updateScroll(1.0F);
        this.mScrollerY.updateScroll(1.0F);
        abortAnimation();
      } 
    } 
    return true;
  }
  
  public final boolean isOplusFinished() {
    boolean bool;
    if (this.mScrollerX.isAtRest() && this.mScrollerY.isAtRest() && this.mMode != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final int getOplusCurrX() {
    return (int)Math.round(this.mScrollerX.getCurrentValue());
  }
  
  public final int getOplusCurrY() {
    return (int)Math.round(this.mScrollerY.getCurrentValue());
  }
  
  public final int getOplusFinalX() {
    return (int)this.mScrollerX.getEndValue();
  }
  
  public final int getOplusFinalY() {
    return (int)this.mScrollerY.getEndValue();
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    if (paramInt2 > paramInt8 || paramInt2 < paramInt7) {
      springBack(paramInt1, paramInt2, paramInt5, paramInt6, paramInt7, paramInt8);
      return;
    } 
    fling(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8);
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    fling(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mMode = 1;
    this.mScrollerX.fling(paramInt1, paramInt3);
    this.mScrollerY.fling(paramInt2, paramInt4);
  }
  
  public boolean springBack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    boolean bool1 = this.mScrollerX.springBack(paramInt1, paramInt3, paramInt4);
    boolean bool2 = this.mScrollerY.springBack(paramInt2, paramInt5, paramInt6);
    boolean bool3 = true;
    if (bool1 || bool2)
      this.mMode = 1; 
    boolean bool4 = bool3;
    if (!bool1)
      if (bool2) {
        bool4 = bool3;
      } else {
        bool4 = false;
      }  
    return bool4;
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    startScroll(paramInt1, paramInt2, paramInt3, paramInt4, 250);
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mMode = 0;
    this.mScrollerX.startScroll(paramInt1, paramInt3, paramInt5);
    this.mScrollerY.startScroll(paramInt2, paramInt4, paramInt5);
  }
  
  public void abortAnimation() {
    this.mMode = 2;
    this.mScrollerX.setAtRest();
    this.mScrollerY.setAtRest();
  }
  
  public boolean isScrollingInDirection(float paramFloat1, float paramFloat2) {
    boolean bool;
    int i = (int)(this.mScrollerX.mEndValue - this.mScrollerX.mStartValue);
    int j = (int)(this.mScrollerY.mEndValue - this.mScrollerY.mStartValue);
    if (!isFinished() && 
      Math.signum(paramFloat1) == Math.signum(i) && 
      Math.signum(paramFloat2) == Math.signum(j)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void notifyVerticalEdgeReached(int paramInt1, int paramInt2, int paramInt3) {
    this.mScrollerY.notifyEdgeReached(paramInt1, paramInt2, paramInt3);
    springBack(0, paramInt1, 0, 0, 0, 0);
  }
  
  public void notifyHorizontalEdgeReached(int paramInt1, int paramInt2, int paramInt3) {
    this.mScrollerX.notifyEdgeReached(paramInt1, paramInt2, paramInt3);
    springBack(paramInt1, 0, 0, 0, 0, 0);
  }
  
  public float getCurrVelocity() {
    double d1 = this.mScrollerX.getVelocity();
    double d2 = this.mScrollerY.getVelocity();
    return (int)Math.sqrt(d1 * d1 + d2 * d2);
  }
  
  public void setOplusFriction(float paramFloat) {}
  
  class ReboundOverScroller {
    private static float sTimeIncrease = 1.0F;
    
    private PhysicsState mCurrentState = new PhysicsState();
    
    private PhysicsState mPreviousState = new PhysicsState();
    
    private PhysicsState mTempState = new PhysicsState();
    
    private float mFlingFriction = 1.06F;
    
    private double mRestSpeedThreshold = 100.0D;
    
    private double mDisplacementFromRestThreshold = 0.05D;
    
    private int mOppoCount = 1;
    
    private boolean mIsScrollView = false;
    
    private static final float FLING_CHANGE_INCREASE_STEP = 1.2F;
    
    private static final float FLING_CHANGE_REDUCE_STEP = 0.6F;
    
    private static final float FLING_DXDT_RATIO = 0.167F;
    
    private static final float FLOAT_1 = 1.0F;
    
    private static final float FLOAT_2 = 2.0F;
    
    private static final int NUM_60 = 60;
    
    private static final int SPRING_BACK_ADJUST_TENSION_VALUE = 100;
    
    private static final int SPRING_BACK_ADJUST_THRESHOLD = 180;
    
    private static final float SPRING_BACK_FRICTION = 12.19F;
    
    private static final int SPRING_BACK_STOP_THRESHOLD = 2;
    
    private static final float SPRING_BACK_TENSION = 16.0F;
    
    private ReboundConfig mConfig;
    
    private double mEndValue;
    
    private ReboundConfig mFlingConfig;
    
    private boolean mIsSpringBack;
    
    private float mRefreshTime;
    
    private int mScrollDuration;
    
    private int mScrollFinal;
    
    private int mScrollStart;
    
    private long mScrollStartTime;
    
    private ReboundConfig mSpringBackConfig;
    
    private double mStartValue;
    
    private boolean mTensionAdjusted;
    
    ReboundOverScroller(SpringOverScroller this$0) {
      this.mRefreshTime = this$0;
      this.mFlingConfig = new ReboundConfig(1.06F, 0.0D);
      this.mSpringBackConfig = new ReboundConfig(12.1899995803833D, 16.0D);
      setConfig(this.mFlingConfig);
    }
    
    void fling(int param1Int1, int param1Int2) {
      this.mOppoCount = 1;
      sTimeIncrease = 1.0F;
      this.mFlingConfig.setFriction(this.mFlingFriction);
      this.mFlingConfig.setTension(0.0D);
      setConfig(this.mFlingConfig);
      setCurrentValue(param1Int1, true);
      setVelocity(param1Int2);
    }
    
    boolean springBack(int param1Int1, int param1Int2, int param1Int3) {
      setCurrentValue(param1Int1, false);
      if (param1Int1 > param1Int3 || param1Int1 < param1Int2) {
        if (param1Int1 > param1Int3) {
          setEndValue(param1Int3);
        } else if (param1Int1 < param1Int2) {
          setEndValue(param1Int2);
        } 
        this.mIsSpringBack = true;
        this.mSpringBackConfig.setFriction(12.1899995803833D);
        this.mSpringBackConfig.setTension(16.0D);
        setConfig(this.mSpringBackConfig);
        return true;
      } 
      setConfig(new ReboundConfig(this.mFlingFriction, 0.0D));
      return false;
    }
    
    void startScroll(int param1Int1, int param1Int2, int param1Int3) {
      this.mScrollStart = param1Int1;
      this.mScrollFinal = param1Int1 + param1Int2;
      this.mScrollDuration = param1Int3;
      this.mScrollStartTime = AnimationUtils.currentAnimationTimeMillis();
      setConfig(this.mFlingConfig);
    }
    
    void setConfig(ReboundConfig param1ReboundConfig) {
      if (param1ReboundConfig != null) {
        this.mConfig = param1ReboundConfig;
        return;
      } 
      throw new IllegalArgumentException("springConfig is required");
    }
    
    void setCurrentValue(double param1Double, boolean param1Boolean) {
      this.mStartValue = param1Double;
      if (!this.mIsScrollView) {
        this.mPreviousState.mPosition = 0.0D;
        this.mTempState.mPosition = 0.0D;
      } 
      this.mCurrentState.mPosition = param1Double;
      if (param1Boolean)
        setAtRest(); 
    }
    
    double getCurrentValue() {
      return this.mCurrentState.mPosition;
    }
    
    double getVelocity() {
      return this.mCurrentState.mVelocity;
    }
    
    void setVelocity(double param1Double) {
      if (param1Double == this.mCurrentState.mVelocity)
        return; 
      this.mCurrentState.mVelocity = param1Double;
    }
    
    double getEndValue() {
      return this.mEndValue;
    }
    
    void setEndValue(double param1Double) {
      if (this.mEndValue == param1Double)
        return; 
      this.mStartValue = getCurrentValue();
      this.mEndValue = param1Double;
    }
    
    void setAtRest() {
      this.mEndValue = this.mCurrentState.mPosition;
      this.mTempState.mPosition = this.mCurrentState.mPosition;
      this.mCurrentState.mVelocity = 0.0D;
      this.mIsSpringBack = false;
    }
    
    boolean isAtRest() {
      if (Math.abs(this.mCurrentState.mVelocity) <= this.mRestSpeedThreshold) {
        PhysicsState physicsState = this.mCurrentState;
        if (getDisplacementDistanceForState(physicsState) <= this.mDisplacementFromRestThreshold || this.mConfig.mTension == 0.0D)
          return true; 
      } 
      return false;
    }
    
    void updateScroll(float param1Float) {
      PhysicsState physicsState = this.mCurrentState;
      int i = this.mScrollStart;
      physicsState.mPosition = (i + Math.round((this.mScrollFinal - i) * param1Float));
    }
    
    void notifyEdgeReached(int param1Int1, int param1Int2, int param1Int3) {
      this.mCurrentState.mPosition = param1Int1;
      this.mPreviousState.mPosition = 0.0D;
      this.mPreviousState.mVelocity = 0.0D;
      this.mTempState.mPosition = 0.0D;
      this.mTempState.mVelocity = 0.0D;
    }
    
    double getDisplacementDistanceForState(PhysicsState param1PhysicsState) {
      return Math.abs(this.mEndValue - param1PhysicsState.mPosition);
    }
    
    boolean update() {
      if (isAtRest())
        return false; 
      double d1 = this.mCurrentState.mPosition;
      double d2 = this.mCurrentState.mVelocity;
      double d3 = this.mTempState.mPosition;
      double d4 = this.mTempState.mVelocity;
      if (!this.mIsSpringBack) {
        if (this.mOppoCount < 60) {
          sTimeIncrease += 0.020000001F;
          ReboundConfig reboundConfig = this.mConfig;
          reboundConfig.mFriction += 0.020000001415610313D;
        } else {
          float f1 = sTimeIncrease;
          sTimeIncrease = f1 - (f1 - 0.6F) / 60.0F;
          ReboundConfig reboundConfig = this.mConfig;
          reboundConfig.mFriction -= ((sTimeIncrease - 0.6F) / 60.0F);
        } 
      } else {
        d4 = getDisplacementDistanceForState(this.mCurrentState);
        if (!this.mTensionAdjusted && d4 < 180.0D) {
          ReboundConfig reboundConfig = this.mConfig;
          reboundConfig.mTension += 100.0D;
          this.mTensionAdjusted = true;
        } else if (d4 < 2.0D) {
          this.mCurrentState.mPosition = this.mEndValue;
          this.mTensionAdjusted = false;
          this.mIsSpringBack = false;
          return false;
        } 
      } 
      d3 = this.mConfig.mTension * (this.mEndValue - d3) - this.mConfig.mFriction * this.mPreviousState.mVelocity;
      float f = this.mRefreshTime;
      double d5 = f * d2 / 2.0D;
      d4 = f * d3 / 2.0D + d2;
      double d6 = this.mConfig.mTension * (this.mEndValue - d5 + d1) - this.mConfig.mFriction * d4;
      f = this.mRefreshTime;
      double d7 = f * d4 / 2.0D;
      d5 = f * d6 / 2.0D + d2;
      double d8 = this.mConfig.mTension * (this.mEndValue - d7 + d1) - this.mConfig.mFriction * d5;
      f = this.mRefreshTime;
      double d9 = f * d5 + d1;
      double d10 = f * d8 + d2;
      double d11 = this.mConfig.mTension, d12 = this.mEndValue;
      d7 = this.mConfig.mFriction;
      f = this.mRefreshTime;
      double d13 = f;
      double d14 = f;
      this.mTempState.mVelocity = d10;
      this.mTempState.mPosition = d9;
      this.mCurrentState.mVelocity = d2 + d14 * (d3 + (d6 + d8) * 2.0D + d11 * (d12 - d9) - d7 * d10) * 0.16699999570846558D;
      this.mCurrentState.mPosition = d1 + d13 * (d2 + (d4 + d5) * 2.0D + d10) * 0.16699999570846558D;
      this.mOppoCount++;
      return true;
    }
    
    static class ReboundConfig {
      double mFriction;
      
      double mTension;
      
      ReboundConfig(double param2Double1, double param2Double2) {
        this.mFriction = frictionFromOrigamiValue((float)param2Double1);
        this.mTension = tensionFromOrigamiValue((float)param2Double2);
      }
      
      void setFriction(double param2Double) {
        this.mFriction = frictionFromOrigamiValue((float)param2Double);
      }
      
      void setTension(double param2Double) {
        this.mTension = tensionFromOrigamiValue((float)param2Double);
      }
      
      private float frictionFromOrigamiValue(float param2Float) {
        float f = 0.0F;
        if (param2Float == 0.0F) {
          param2Float = f;
        } else {
          param2Float = (param2Float - 8.0F) * 3.0F + 25.0F;
        } 
        return param2Float;
      }
      
      private double tensionFromOrigamiValue(float param2Float) {
        double d;
        if (param2Float == 0.0F) {
          d = 0.0D;
        } else {
          d = ((param2Float - 30.0F) * 3.62F + 194.0F);
        } 
        return d;
      }
    }
    
    static class PhysicsState {
      double mPosition;
      
      double mVelocity;
    }
  }
}
