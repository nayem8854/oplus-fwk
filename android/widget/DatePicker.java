package android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.icu.util.Calendar;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityEvent;
import android.view.autofill.AutofillManager;
import android.view.autofill.AutofillValue;
import com.android.internal.R;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Locale;

public class DatePicker extends FrameLayout {
  private static final String LOG_TAG = DatePicker.class.getSimpleName();
  
  public static final int MODE_CALENDAR = 2;
  
  public static final int MODE_SPINNER = 1;
  
  private final DatePickerDelegate mDelegate;
  
  private final int mMode;
  
  public DatePicker(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public DatePicker(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843612);
  }
  
  public DatePicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public DatePicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    if (getImportantForAutofill() == 0)
      setImportantForAutofill(1); 
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DatePicker, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.DatePicker, paramAttributeSet, typedArray, paramInt1, paramInt2);
    boolean bool = typedArray.getBoolean(17, false);
    int i = typedArray.getInt(16, 1);
    int j = typedArray.getInt(3, 0);
    typedArray.recycle();
    if (i == 2 && bool) {
      this.mMode = paramContext.getResources().getInteger(17694925);
    } else {
      this.mMode = i;
    } 
    if (this.mMode != 2) {
      this.mDelegate = createSpinnerUIDelegate(paramContext, paramAttributeSet, paramInt1, paramInt2);
    } else {
      this.mDelegate = createCalendarUIDelegate(paramContext, paramAttributeSet, paramInt1, paramInt2);
    } 
    if (j != 0)
      setFirstDayOfWeek(j); 
    this.mDelegate.setAutoFillChangeListener(new _$$Lambda$DatePicker$AnJPL5BrPXPJa_Oc_WUAB_HJq84(this, paramContext));
  }
  
  private DatePickerDelegate createSpinnerUIDelegate(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    return new DatePickerSpinnerDelegate(this, paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  private DatePickerDelegate createCalendarUIDelegate(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    return new DatePickerCalendarDelegate(this, paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public int getMode() {
    return this.mMode;
  }
  
  public void init(int paramInt1, int paramInt2, int paramInt3, OnDateChangedListener paramOnDateChangedListener) {
    this.mDelegate.init(paramInt1, paramInt2, paramInt3, paramOnDateChangedListener);
  }
  
  public void setOnDateChangedListener(OnDateChangedListener paramOnDateChangedListener) {
    this.mDelegate.setOnDateChangedListener(paramOnDateChangedListener);
  }
  
  public void updateDate(int paramInt1, int paramInt2, int paramInt3) {
    this.mDelegate.updateDate(paramInt1, paramInt2, paramInt3);
  }
  
  public int getYear() {
    return this.mDelegate.getYear();
  }
  
  public int getMonth() {
    return this.mDelegate.getMonth();
  }
  
  public int getDayOfMonth() {
    return this.mDelegate.getDayOfMonth();
  }
  
  public long getMinDate() {
    return this.mDelegate.getMinDate().getTimeInMillis();
  }
  
  public void setMinDate(long paramLong) {
    this.mDelegate.setMinDate(paramLong);
  }
  
  public long getMaxDate() {
    return this.mDelegate.getMaxDate().getTimeInMillis();
  }
  
  public void setMaxDate(long paramLong) {
    this.mDelegate.setMaxDate(paramLong);
  }
  
  public void setValidationCallback(ValidationCallback paramValidationCallback) {
    this.mDelegate.setValidationCallback(paramValidationCallback);
  }
  
  public void setEnabled(boolean paramBoolean) {
    if (this.mDelegate.isEnabled() == paramBoolean)
      return; 
    super.setEnabled(paramBoolean);
    this.mDelegate.setEnabled(paramBoolean);
  }
  
  public boolean isEnabled() {
    return this.mDelegate.isEnabled();
  }
  
  public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    return this.mDelegate.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent);
  }
  
  public void onPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onPopulateAccessibilityEventInternal(paramAccessibilityEvent);
    this.mDelegate.onPopulateAccessibilityEvent(paramAccessibilityEvent);
  }
  
  public CharSequence getAccessibilityClassName() {
    return DatePicker.class.getName();
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    this.mDelegate.onConfigurationChanged(paramConfiguration);
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    if (paramInt >= 1 && paramInt <= 7) {
      this.mDelegate.setFirstDayOfWeek(paramInt);
      return;
    } 
    throw new IllegalArgumentException("firstDayOfWeek must be between 1 and 7");
  }
  
  public int getFirstDayOfWeek() {
    return this.mDelegate.getFirstDayOfWeek();
  }
  
  @Deprecated
  public boolean getCalendarViewShown() {
    return this.mDelegate.getCalendarViewShown();
  }
  
  @Deprecated
  public CalendarView getCalendarView() {
    return this.mDelegate.getCalendarView();
  }
  
  @Deprecated
  public void setCalendarViewShown(boolean paramBoolean) {
    this.mDelegate.setCalendarViewShown(paramBoolean);
  }
  
  @Deprecated
  public boolean getSpinnersShown() {
    return this.mDelegate.getSpinnersShown();
  }
  
  @Deprecated
  public void setSpinnersShown(boolean paramBoolean) {
    this.mDelegate.setSpinnersShown(paramBoolean);
  }
  
  protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray) {
    dispatchThawSelfOnly(paramSparseArray);
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    return this.mDelegate.onSaveInstanceState(parcelable);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    this.mDelegate.onRestoreInstanceState(paramParcelable);
  }
  
  class AbstractDatePickerDelegate implements DatePickerDelegate {
    protected DatePicker.OnDateChangedListener mAutoFillChangeListener;
    
    private long mAutofilledValue;
    
    protected Context mContext;
    
    protected Calendar mCurrentDate;
    
    protected Locale mCurrentLocale;
    
    protected DatePicker mDelegator;
    
    protected DatePicker.OnDateChangedListener mOnDateChangedListener;
    
    protected DatePicker.ValidationCallback mValidationCallback;
    
    public AbstractDatePickerDelegate(DatePicker this$0, Context param1Context) {
      this.mDelegator = this$0;
      this.mContext = param1Context;
      setCurrentLocale(Locale.getDefault());
    }
    
    protected void setCurrentLocale(Locale param1Locale) {
      if (!param1Locale.equals(this.mCurrentLocale)) {
        this.mCurrentLocale = param1Locale;
        onLocaleChanged(param1Locale);
      } 
    }
    
    public void setOnDateChangedListener(DatePicker.OnDateChangedListener param1OnDateChangedListener) {
      this.mOnDateChangedListener = param1OnDateChangedListener;
    }
    
    public void setAutoFillChangeListener(DatePicker.OnDateChangedListener param1OnDateChangedListener) {
      this.mAutoFillChangeListener = param1OnDateChangedListener;
    }
    
    public void setValidationCallback(DatePicker.ValidationCallback param1ValidationCallback) {
      this.mValidationCallback = param1ValidationCallback;
    }
    
    public final void autofill(AutofillValue param1AutofillValue) {
      if (param1AutofillValue == null || !param1AutofillValue.isDate()) {
        String str = DatePicker.LOG_TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(param1AutofillValue);
        stringBuilder.append(" could not be autofilled into ");
        stringBuilder.append(this);
        Log.w(str, stringBuilder.toString());
        return;
      } 
      long l = param1AutofillValue.getDateValue();
      Calendar calendar = Calendar.getInstance(this.mCurrentLocale);
      calendar.setTimeInMillis(l);
      int i = calendar.get(1), j = calendar.get(2);
      int k = calendar.get(5);
      updateDate(i, j, k);
      this.mAutofilledValue = l;
    }
    
    public final AutofillValue getAutofillValue() {
      long l = this.mAutofilledValue;
      if (l == 0L)
        l = this.mCurrentDate.getTimeInMillis(); 
      return AutofillValue.forDate(l);
    }
    
    protected void resetAutofilledValue() {
      this.mAutofilledValue = 0L;
    }
    
    protected void onValidationChanged(boolean param1Boolean) {
      DatePicker.ValidationCallback validationCallback = this.mValidationCallback;
      if (validationCallback != null)
        validationCallback.onValidationChanged(param1Boolean); 
    }
    
    protected void onLocaleChanged(Locale param1Locale) {}
    
    public void onPopulateAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent) {
      param1AccessibilityEvent.getText().add(getFormattedCurrentDate());
    }
    
    protected String getFormattedCurrentDate() {
      return DateUtils.formatDateTime(this.mContext, this.mCurrentDate.getTimeInMillis(), 22);
    }
    
    class SavedState extends View.BaseSavedState {
      public SavedState(int param2Int1, int param2Int2, int param2Int3, long param2Long1, long param2Long2) {
        this((Parcelable)this$0, param2Int1, param2Int2, param2Int3, param2Long1, param2Long2, 0, 0, 0);
      }
      
      public SavedState(int param2Int1, int param2Int2, int param2Int3, long param2Long1, long param2Long2, int param2Int4, int param2Int5, int param2Int6) {
        super((Parcelable)this$0);
        this.mSelectedYear = param2Int1;
        this.mSelectedMonth = param2Int2;
        this.mSelectedDay = param2Int3;
        this.mMinDate = param2Long1;
        this.mMaxDate = param2Long2;
        this.mCurrentView = param2Int4;
        this.mListPosition = param2Int5;
        this.mListPositionOffset = param2Int6;
      }
      
      private SavedState(DatePicker.AbstractDatePickerDelegate this$0) {
        super((Parcel)this$0);
        this.mSelectedYear = this$0.readInt();
        this.mSelectedMonth = this$0.readInt();
        this.mSelectedDay = this$0.readInt();
        this.mMinDate = this$0.readLong();
        this.mMaxDate = this$0.readLong();
        this.mCurrentView = this$0.readInt();
        this.mListPosition = this$0.readInt();
        this.mListPositionOffset = this$0.readInt();
      }
      
      public void writeToParcel(Parcel param2Parcel, int param2Int) {
        super.writeToParcel(param2Parcel, param2Int);
        param2Parcel.writeInt(this.mSelectedYear);
        param2Parcel.writeInt(this.mSelectedMonth);
        param2Parcel.writeInt(this.mSelectedDay);
        param2Parcel.writeLong(this.mMinDate);
        param2Parcel.writeLong(this.mMaxDate);
        param2Parcel.writeInt(this.mCurrentView);
        param2Parcel.writeInt(this.mListPosition);
        param2Parcel.writeInt(this.mListPositionOffset);
      }
      
      public int getSelectedDay() {
        return this.mSelectedDay;
      }
      
      public int getSelectedMonth() {
        return this.mSelectedMonth;
      }
      
      public int getSelectedYear() {
        return this.mSelectedYear;
      }
      
      public long getMinDate() {
        return this.mMinDate;
      }
      
      public long getMaxDate() {
        return this.mMaxDate;
      }
      
      public int getCurrentView() {
        return this.mCurrentView;
      }
      
      public int getListPosition() {
        return this.mListPosition;
      }
      
      public int getListPositionOffset() {
        return this.mListPositionOffset;
      }
      
      public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
          public DatePicker.AbstractDatePickerDelegate.SavedState createFromParcel(Parcel param2Parcel) {
            return new DatePicker.AbstractDatePickerDelegate.SavedState();
          }
          
          public DatePicker.AbstractDatePickerDelegate.SavedState[] newArray(int param2Int) {
            return new DatePicker.AbstractDatePickerDelegate.SavedState[param2Int];
          }
        };
      
      private final int mCurrentView;
      
      private final int mListPosition;
      
      private final int mListPositionOffset;
      
      private final long mMaxDate;
      
      private final long mMinDate;
      
      private final int mSelectedDay;
      
      private final int mSelectedMonth;
      
      private final int mSelectedYear;
    }
    
    class null implements Parcelable.Creator<SavedState> {
      public DatePicker.AbstractDatePickerDelegate.SavedState createFromParcel(Parcel param2Parcel) {
        return new DatePicker.AbstractDatePickerDelegate.SavedState();
      }
      
      public DatePicker.AbstractDatePickerDelegate.SavedState[] newArray(int param2Int) {
        return new DatePicker.AbstractDatePickerDelegate.SavedState[param2Int];
      }
    }
  }
  
  public void dispatchProvideAutofillStructure(ViewStructure paramViewStructure, int paramInt) {
    paramViewStructure.setAutofillId(getAutofillId());
    onProvideAutofillStructure(paramViewStructure, paramInt);
  }
  
  public void autofill(AutofillValue paramAutofillValue) {
    if (!isEnabled())
      return; 
    this.mDelegate.autofill(paramAutofillValue);
  }
  
  public int getAutofillType() {
    boolean bool;
    if (isEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public AutofillValue getAutofillValue() {
    AutofillValue autofillValue;
    if (isEnabled()) {
      autofillValue = this.mDelegate.getAutofillValue();
    } else {
      autofillValue = null;
    } 
    return autofillValue;
  }
  
  class DatePickerDelegate {
    public abstract void autofill(AutofillValue param1AutofillValue);
    
    public abstract boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent);
    
    public abstract AutofillValue getAutofillValue();
    
    public abstract CalendarView getCalendarView();
    
    public abstract boolean getCalendarViewShown();
    
    public abstract int getDayOfMonth();
    
    public abstract int getFirstDayOfWeek();
    
    public abstract Calendar getMaxDate();
    
    public abstract Calendar getMinDate();
    
    public abstract int getMonth();
    
    public abstract boolean getSpinnersShown();
    
    public abstract int getYear();
    
    public abstract void init(int param1Int1, int param1Int2, int param1Int3, DatePicker.OnDateChangedListener param1OnDateChangedListener);
    
    public abstract boolean isEnabled();
    
    public abstract void onConfigurationChanged(Configuration param1Configuration);
    
    public abstract void onPopulateAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent);
    
    public abstract void onRestoreInstanceState(Parcelable param1Parcelable);
    
    public abstract Parcelable onSaveInstanceState(Parcelable param1Parcelable);
    
    public abstract void setAutoFillChangeListener(DatePicker.OnDateChangedListener param1OnDateChangedListener);
    
    public abstract void setCalendarViewShown(boolean param1Boolean);
    
    public abstract void setEnabled(boolean param1Boolean);
    
    public abstract void setFirstDayOfWeek(int param1Int);
    
    public abstract void setMaxDate(long param1Long);
    
    public abstract void setMinDate(long param1Long);
    
    public abstract void setOnDateChangedListener(DatePicker.OnDateChangedListener param1OnDateChangedListener);
    
    public abstract void setSpinnersShown(boolean param1Boolean);
    
    public abstract void setValidationCallback(DatePicker.ValidationCallback param1ValidationCallback);
    
    public abstract void updateDate(int param1Int1, int param1Int2, int param1Int3);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class DatePickerMode implements Annotation {}
  
  class OnDateChangedListener {
    public abstract void onDateChanged(DatePicker param1DatePicker, int param1Int1, int param1Int2, int param1Int3);
  }
  
  class ValidationCallback {
    public abstract void onValidationChanged(boolean param1Boolean);
  }
}
