package android.widget;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;

public interface ExpandableListAdapter {
  boolean areAllItemsEnabled();
  
  Object getChild(int paramInt1, int paramInt2);
  
  long getChildId(int paramInt1, int paramInt2);
  
  View getChildView(int paramInt1, int paramInt2, boolean paramBoolean, View paramView, ViewGroup paramViewGroup);
  
  int getChildrenCount(int paramInt);
  
  long getCombinedChildId(long paramLong1, long paramLong2);
  
  long getCombinedGroupId(long paramLong);
  
  Object getGroup(int paramInt);
  
  int getGroupCount();
  
  long getGroupId(int paramInt);
  
  View getGroupView(int paramInt, boolean paramBoolean, View paramView, ViewGroup paramViewGroup);
  
  boolean hasStableIds();
  
  boolean isChildSelectable(int paramInt1, int paramInt2);
  
  boolean isEmpty();
  
  void onGroupCollapsed(int paramInt);
  
  void onGroupExpanded(int paramInt);
  
  void registerDataSetObserver(DataSetObserver paramDataSetObserver);
  
  void unregisterDataSetObserver(DataSetObserver paramDataSetObserver);
}
