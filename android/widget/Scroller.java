package android.widget;

import android.content.Context;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

public class Scroller {
  private float mFlingFriction = ViewConfiguration.getScrollFriction();
  
  private static float DECELERATION_RATE = (float)(Math.log(0.78D) / Math.log(0.9D));
  
  private static final float[] SPLINE_POSITION = new float[101];
  
  private static final float[] SPLINE_TIME = new float[101];
  
  private static final int DEFAULT_DURATION = 250;
  
  private static final float END_TENSION = 1.0F;
  
  private static final int FLING_MODE = 1;
  
  private static final float INFLEXION = 0.35F;
  
  private static final int NB_SAMPLES = 100;
  
  private static final float P1 = 0.175F;
  
  private static final float P2 = 0.35000002F;
  
  private static final int SCROLL_MODE = 0;
  
  private static final float START_TENSION = 0.5F;
  
  private Context mContext;
  
  private float mCurrVelocity;
  
  private int mCurrX;
  
  private int mCurrY;
  
  private float mDeceleration;
  
  private float mDeltaX;
  
  private float mDeltaY;
  
  private int mDistance;
  
  private int mDuration;
  
  private float mDurationReciprocal;
  
  private int mFinalX;
  
  private int mFinalY;
  
  private boolean mFinished;
  
  private boolean mFlywheel;
  
  private final Interpolator mInterpolator;
  
  private int mMaxX;
  
  private int mMaxY;
  
  private int mMinX;
  
  private int mMinY;
  
  private int mMode;
  
  private float mPhysicalCoeff;
  
  private final float mPpi;
  
  private long mStartTime;
  
  private int mStartX;
  
  private int mStartY;
  
  private float mVelocity;
  
  static {
    float f1 = 0.0F;
    float f2 = 0.0F;
    for (byte b = 0; b < 100; ) {
      float f3 = b / 100.0F;
      float f4 = 1.0F;
      while (true) {
        float f5 = (f4 - f1) / 2.0F + f1;
        float f6 = f5 * 3.0F * (1.0F - f5);
        float f7 = ((1.0F - f5) * 0.175F + f5 * 0.35000002F) * f6 + f5 * f5 * f5;
        if (Math.abs(f7 - f3) < 1.0E-5D) {
          SPLINE_POSITION[b] = ((1.0F - f5) * 0.5F + f5) * f6 + f5 * f5 * f5;
          f4 = 1.0F;
          while (true) {
            f5 = (f4 - f2) / 2.0F + f2;
            f7 = f5 * 3.0F * (1.0F - f5);
            f6 = ((1.0F - f5) * 0.5F + f5) * f7 + f5 * f5 * f5;
            if (Math.abs(f6 - f3) < 1.0E-5D) {
              SPLINE_TIME[b] = f7 * ((1.0F - f5) * 0.175F + 0.35000002F * f5) + f5 * f5 * f5;
              b++;
            } 
            if (f6 > f3) {
              f4 = f5;
              continue;
            } 
            f2 = f5;
          } 
          break;
        } 
        if (f7 > f3) {
          f4 = f5;
          continue;
        } 
        f1 = f5;
      } 
    } 
    float[] arrayOfFloat = SPLINE_POSITION;
    SPLINE_TIME[100] = 1.0F;
    arrayOfFloat[100] = 1.0F;
  }
  
  public Scroller(Context paramContext) {
    this(paramContext, null);
  }
  
  public Scroller(Context paramContext, Interpolator paramInterpolator) {
    this(paramContext, paramInterpolator, bool);
  }
  
  public Scroller(Context paramContext, Interpolator paramInterpolator, boolean paramBoolean) {
    this.mFinished = true;
    this.mContext = paramContext;
    if (paramInterpolator == null) {
      this.mInterpolator = new ViscousFluidInterpolator();
    } else {
      this.mInterpolator = paramInterpolator;
    } 
    this.mPpi = (paramContext.getResources().getDisplayMetrics()).density * 160.0F;
    this.mDeceleration = computeDeceleration(ViewConfiguration.getScrollFriction());
    this.mFlywheel = paramBoolean;
    this.mPhysicalCoeff = computeDeceleration(0.84F);
  }
  
  public final void setFriction(float paramFloat) {
    this.mDeceleration = computeDeceleration(paramFloat);
    this.mFlingFriction = paramFloat;
  }
  
  private float computeDeceleration(float paramFloat) {
    return this.mPpi * 386.0878F * paramFloat;
  }
  
  public final boolean isFinished() {
    return this.mFinished;
  }
  
  public final void forceFinished(boolean paramBoolean) {
    this.mFinished = paramBoolean;
  }
  
  public final int getDuration() {
    return this.mDuration;
  }
  
  public final int getCurrX() {
    return this.mCurrX;
  }
  
  public final int getCurrY() {
    return this.mCurrY;
  }
  
  public float getCurrVelocity() {
    float f;
    if (this.mMode == 1) {
      f = this.mCurrVelocity;
    } else {
      f = this.mVelocity - this.mDeceleration * timePassed() / 2000.0F;
    } 
    return f;
  }
  
  public final int getStartX() {
    return this.mStartX;
  }
  
  public final int getStartY() {
    return this.mStartY;
  }
  
  public final int getFinalX() {
    return this.mFinalX;
  }
  
  public final int getFinalY() {
    return this.mFinalY;
  }
  
  public boolean computeScrollOffset() {
    if (this.mFinished)
      return false; 
    int i = (int)(AnimationUtils.currentAnimationTimeMillis() - this.mStartTime);
    int j = this.mDuration;
    if (i < j) {
      int k = this.mMode;
      if (k != 0) {
        if (k == 1) {
          float f1 = i / j;
          i = (int)(f1 * 100.0F);
          float f2 = 1.0F;
          float f3 = 0.0F;
          if (i < 100) {
            f2 = i / 100.0F;
            f3 = (i + 1) / 100.0F;
            float arrayOfFloat[] = SPLINE_POSITION, f4 = arrayOfFloat[i];
            float f5 = arrayOfFloat[i + 1];
            f3 = (f5 - f4) / (f3 - f2);
            f2 = f4 + (f1 - f2) * f3;
          } 
          this.mCurrVelocity = this.mDistance * f3 / this.mDuration * 1000.0F;
          i = this.mStartX;
          this.mCurrX = i += Math.round((this.mFinalX - i) * f2);
          this.mCurrX = i = Math.min(i, this.mMaxX);
          this.mCurrX = Math.max(i, this.mMinX);
          i = this.mStartY;
          this.mCurrY = i += Math.round((this.mFinalY - i) * f2);
          this.mCurrY = i = Math.min(i, this.mMaxY);
          this.mCurrY = i = Math.max(i, this.mMinY);
          if (this.mCurrX == this.mFinalX && i == this.mFinalY)
            this.mFinished = true; 
        } 
      } else {
        float f = this.mInterpolator.getInterpolation(i * this.mDurationReciprocal);
        this.mCurrX = this.mStartX + Math.round(this.mDeltaX * f);
        this.mCurrY = this.mStartY + Math.round(this.mDeltaY * f);
      } 
    } else {
      this.mCurrX = this.mFinalX;
      this.mCurrY = this.mFinalY;
      this.mFinished = true;
    } 
    return true;
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    startScroll(paramInt1, paramInt2, paramInt3, paramInt4, 250);
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mMode = 0;
    this.mFinished = false;
    this.mDuration = paramInt5;
    this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
    this.mStartX = paramInt1;
    this.mStartY = paramInt2;
    this.mFinalX = paramInt1 + paramInt3;
    this.mFinalY = paramInt2 + paramInt4;
    this.mDeltaX = paramInt3;
    this.mDeltaY = paramInt4;
    this.mDurationReciprocal = 1.0F / this.mDuration;
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    float f1;
    int i = paramInt3, j = paramInt4;
    if (this.mFlywheel) {
      i = paramInt3;
      j = paramInt4;
      if (!this.mFinished) {
        f1 = getCurrVelocity();
        float f4 = (this.mFinalX - this.mStartX);
        float f5 = (this.mFinalY - this.mStartY);
        float f6 = (float)Math.hypot(f4, f5);
        f4 /= f6;
        f5 /= f6;
        f4 *= f1;
        f1 = f5 * f1;
        i = paramInt3;
        j = paramInt4;
        if (Math.signum(paramInt3) == Math.signum(f4)) {
          f5 = paramInt4;
          i = paramInt3;
          j = paramInt4;
          if (Math.signum(f5) == Math.signum(f1)) {
            i = (int)(paramInt3 + f4);
            j = (int)(paramInt4 + f1);
          } 
        } 
      } 
    } 
    this.mMode = 1;
    this.mFinished = false;
    float f3 = (float)Math.hypot(i, j);
    this.mVelocity = f3;
    this.mDuration = getSplineFlingDuration(f3);
    this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
    this.mStartX = paramInt1;
    this.mStartY = paramInt2;
    float f2 = 1.0F;
    if (f3 == 0.0F) {
      f1 = 1.0F;
    } else {
      f1 = i / f3;
    } 
    if (f3 != 0.0F)
      f2 = j / f3; 
    double d = getSplineFlingDistance(f3);
    this.mDistance = (int)(Math.signum(f3) * d);
    this.mMinX = paramInt5;
    this.mMaxX = paramInt6;
    this.mMinY = paramInt7;
    this.mMaxY = paramInt8;
    this.mFinalX = paramInt1 = (int)Math.round(f1 * d) + paramInt1;
    this.mFinalX = paramInt1 = Math.min(paramInt1, this.mMaxX);
    this.mFinalX = Math.max(paramInt1, this.mMinX);
    this.mFinalY = paramInt1 = (int)Math.round(f2 * d) + paramInt2;
    this.mFinalY = paramInt1 = Math.min(paramInt1, this.mMaxY);
    this.mFinalY = Math.max(paramInt1, this.mMinY);
  }
  
  private double getSplineDeceleration(float paramFloat) {
    return Math.log((Math.abs(paramFloat) * 0.35F / this.mFlingFriction * this.mPhysicalCoeff));
  }
  
  private int getSplineFlingDuration(float paramFloat) {
    double d1 = getSplineDeceleration(paramFloat);
    double d2 = DECELERATION_RATE;
    return (int)(Math.exp(d1 / (d2 - 1.0D)) * 1000.0D);
  }
  
  private double getSplineFlingDistance(float paramFloat) {
    double d1 = getSplineDeceleration(paramFloat);
    paramFloat = DECELERATION_RATE;
    double d2 = paramFloat;
    return (this.mFlingFriction * this.mPhysicalCoeff) * Math.exp(paramFloat / (d2 - 1.0D) * d1);
  }
  
  public void abortAnimation() {
    this.mCurrX = this.mFinalX;
    this.mCurrY = this.mFinalY;
    this.mFinished = true;
  }
  
  public void extendDuration(int paramInt) {
    int i = timePassed();
    this.mDuration = paramInt = i + paramInt;
    this.mDurationReciprocal = 1.0F / paramInt;
    this.mFinished = false;
  }
  
  public int timePassed() {
    return (int)(AnimationUtils.currentAnimationTimeMillis() - this.mStartTime);
  }
  
  public void setFinalX(int paramInt) {
    this.mFinalX = paramInt;
    this.mDeltaX = (paramInt - this.mStartX);
    this.mFinished = false;
  }
  
  public void setFinalY(int paramInt) {
    this.mFinalY = paramInt;
    this.mDeltaY = (paramInt - this.mStartY);
    this.mFinished = false;
  }
  
  public boolean isScrollingInDirection(float paramFloat1, float paramFloat2) {
    boolean bool;
    if (!this.mFinished && Math.signum(paramFloat1) == Math.signum((this.mFinalX - this.mStartX)) && 
      Math.signum(paramFloat2) == Math.signum((this.mFinalY - this.mStartY))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class ViscousFluidInterpolator implements Interpolator {
    private static final float VISCOUS_FLUID_NORMALIZE;
    
    private static final float VISCOUS_FLUID_OFFSET;
    
    private static final float VISCOUS_FLUID_SCALE = 8.0F;
    
    static {
      float f = 1.0F / viscousFluid(1.0F);
      VISCOUS_FLUID_OFFSET = 1.0F - f * viscousFluid(1.0F);
    }
    
    private static float viscousFluid(float param1Float) {
      param1Float *= 8.0F;
      if (param1Float < 1.0F) {
        param1Float -= 1.0F - (float)Math.exp(-param1Float);
      } else {
        param1Float = (float)Math.exp((1.0F - param1Float));
        param1Float = 0.36787945F + (1.0F - 0.36787945F) * (1.0F - param1Float);
      } 
      return param1Float;
    }
    
    public float getInterpolation(float param1Float) {
      param1Float = VISCOUS_FLUID_NORMALIZE * viscousFluid(param1Float);
      if (param1Float > 0.0F)
        return VISCOUS_FLUID_OFFSET + param1Float; 
      return param1Float;
    }
  }
}
