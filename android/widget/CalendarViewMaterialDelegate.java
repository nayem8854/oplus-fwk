package android.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

class CalendarViewMaterialDelegate extends CalendarView.AbstractCalendarViewDelegate {
  private final DayPickerView mDayPickerView;
  
  private CalendarView.OnDateChangeListener mOnDateChangeListener;
  
  private final DayPickerView.OnDaySelectedListener mOnDaySelectedListener;
  
  public CalendarViewMaterialDelegate(CalendarView paramCalendarView, Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramCalendarView, paramContext);
    this.mOnDaySelectedListener = (DayPickerView.OnDaySelectedListener)new Object(this);
    DayPickerView dayPickerView = new DayPickerView(paramContext, paramAttributeSet, paramInt1, paramInt2);
    dayPickerView.setOnDaySelectedListener(this.mOnDaySelectedListener);
    paramCalendarView.addView(this.mDayPickerView);
  }
  
  public void setWeekDayTextAppearance(int paramInt) {
    this.mDayPickerView.setDayOfWeekTextAppearance(paramInt);
  }
  
  public int getWeekDayTextAppearance() {
    return this.mDayPickerView.getDayOfWeekTextAppearance();
  }
  
  public void setDateTextAppearance(int paramInt) {
    this.mDayPickerView.setDayTextAppearance(paramInt);
  }
  
  public int getDateTextAppearance() {
    return this.mDayPickerView.getDayTextAppearance();
  }
  
  public void setMinDate(long paramLong) {
    this.mDayPickerView.setMinDate(paramLong);
  }
  
  public long getMinDate() {
    return this.mDayPickerView.getMinDate();
  }
  
  public void setMaxDate(long paramLong) {
    this.mDayPickerView.setMaxDate(paramLong);
  }
  
  public long getMaxDate() {
    return this.mDayPickerView.getMaxDate();
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    this.mDayPickerView.setFirstDayOfWeek(paramInt);
  }
  
  public int getFirstDayOfWeek() {
    return this.mDayPickerView.getFirstDayOfWeek();
  }
  
  public void setDate(long paramLong) {
    this.mDayPickerView.setDate(paramLong, true);
  }
  
  public void setDate(long paramLong, boolean paramBoolean1, boolean paramBoolean2) {
    this.mDayPickerView.setDate(paramLong, paramBoolean1);
  }
  
  public long getDate() {
    return this.mDayPickerView.getDate();
  }
  
  public void setOnDateChangeListener(CalendarView.OnDateChangeListener paramOnDateChangeListener) {
    this.mOnDateChangeListener = paramOnDateChangeListener;
  }
  
  public boolean getBoundsForDate(long paramLong, Rect paramRect) {
    boolean bool = this.mDayPickerView.getBoundsForDate(paramLong, paramRect);
    if (bool) {
      int[] arrayOfInt1 = new int[2];
      int[] arrayOfInt2 = new int[2];
      this.mDayPickerView.getLocationOnScreen(arrayOfInt1);
      this.mDelegator.getLocationOnScreen(arrayOfInt2);
      int i = arrayOfInt1[1] - arrayOfInt2[1];
      paramRect.top += i;
      paramRect.bottom += i;
      return true;
    } 
    return false;
  }
}
