package android.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewDebug.CapturedViewProperty;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;

public abstract class AdapterView<T extends Adapter> extends ViewGroup {
  @ExportedProperty(category = "scrolling")
  int mFirstPosition = 0;
  
  long mSyncRowId = Long.MIN_VALUE;
  
  boolean mNeedSync = false;
  
  boolean mInLayout = false;
  
  @ExportedProperty(category = "list")
  int mNextSelectedPosition = -1;
  
  long mNextSelectedRowId = Long.MIN_VALUE;
  
  @ExportedProperty(category = "list")
  int mSelectedPosition = -1;
  
  long mSelectedRowId = Long.MIN_VALUE;
  
  int mOldSelectedPosition = -1;
  
  long mOldSelectedRowId = Long.MIN_VALUE;
  
  private int mDesiredFocusableState = 16;
  
  boolean mBlockLayoutRequests = false;
  
  public static final int INVALID_POSITION = -1;
  
  public static final long INVALID_ROW_ID = -9223372036854775808L;
  
  public static final int ITEM_VIEW_TYPE_HEADER_OR_FOOTER = -2;
  
  public static final int ITEM_VIEW_TYPE_IGNORE = -1;
  
  static final int SYNC_FIRST_POSITION = 1;
  
  static final int SYNC_MAX_DURATION_MILLIS = 100;
  
  static final int SYNC_SELECTED_POSITION = 0;
  
  boolean mDataChanged;
  
  private boolean mDesiredFocusableInTouchModeState;
  
  private View mEmptyView;
  
  @ExportedProperty(category = "list")
  int mItemCount;
  
  private int mLayoutHeight;
  
  int mOldItemCount;
  
  OnItemClickListener mOnItemClickListener;
  
  OnItemLongClickListener mOnItemLongClickListener;
  
  OnItemSelectedListener mOnItemSelectedListener;
  
  private SelectionNotifier mPendingSelectionNotifier;
  
  private SelectionNotifier mSelectionNotifier;
  
  int mSpecificTop;
  
  long mSyncHeight;
  
  int mSyncMode;
  
  int mSyncPosition;
  
  public AdapterView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public AdapterView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public AdapterView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AdapterView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    if (getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
    this.mDesiredFocusableState = paramInt1 = getFocusable();
    if (paramInt1 == 16)
      super.setFocusable(0); 
  }
  
  public void setOnItemClickListener(OnItemClickListener paramOnItemClickListener) {
    this.mOnItemClickListener = paramOnItemClickListener;
  }
  
  public final OnItemClickListener getOnItemClickListener() {
    return this.mOnItemClickListener;
  }
  
  public boolean performItemClick(View paramView, int paramInt, long paramLong) {
    boolean bool;
    hookPerformClick();
    if (this.mOnItemClickListener != null) {
      if (!(this instanceof Spinner))
        if (paramView != null && paramView.isSoundEffectsEnabled())
          playSoundEffect(0);  
      this.mOnItemClickListener.onItemClick(this, paramView, paramInt, paramLong);
      bool = true;
    } else {
      bool = false;
    } 
    if (paramView != null)
      paramView.sendAccessibilityEvent(1); 
    return bool;
  }
  
  public void setOnItemLongClickListener(OnItemLongClickListener paramOnItemLongClickListener) {
    if (!isLongClickable())
      setLongClickable(true); 
    this.mOnItemLongClickListener = paramOnItemLongClickListener;
  }
  
  public final OnItemLongClickListener getOnItemLongClickListener() {
    return this.mOnItemLongClickListener;
  }
  
  public void setOnItemSelectedListener(OnItemSelectedListener paramOnItemSelectedListener) {
    this.mOnItemSelectedListener = paramOnItemSelectedListener;
  }
  
  public final OnItemSelectedListener getOnItemSelectedListener() {
    return this.mOnItemSelectedListener;
  }
  
  class AdapterContextMenuInfo implements ContextMenu.ContextMenuInfo {
    public long id;
    
    public int position;
    
    public View targetView;
    
    public AdapterContextMenuInfo(AdapterView this$0, int param1Int, long param1Long) {
      this.targetView = this$0;
      this.position = param1Int;
      this.id = param1Long;
    }
  }
  
  public void addView(View paramView) {
    throw new UnsupportedOperationException("addView(View) is not supported in AdapterView");
  }
  
  public void addView(View paramView, int paramInt) {
    throw new UnsupportedOperationException("addView(View, int) is not supported in AdapterView");
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    throw new UnsupportedOperationException("addView(View, LayoutParams) is not supported in AdapterView");
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    throw new UnsupportedOperationException("addView(View, int, LayoutParams) is not supported in AdapterView");
  }
  
  public void removeView(View paramView) {
    throw new UnsupportedOperationException("removeView(View) is not supported in AdapterView");
  }
  
  public void removeViewAt(int paramInt) {
    throw new UnsupportedOperationException("removeViewAt(int) is not supported in AdapterView");
  }
  
  public void removeAllViews() {
    throw new UnsupportedOperationException("removeAllViews() is not supported in AdapterView");
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mLayoutHeight = getHeight();
  }
  
  @CapturedViewProperty
  public int getSelectedItemPosition() {
    return this.mNextSelectedPosition;
  }
  
  @CapturedViewProperty
  public long getSelectedItemId() {
    return this.mNextSelectedRowId;
  }
  
  public Object getSelectedItem() {
    T t = getAdapter();
    int i = getSelectedItemPosition();
    if (t != null && t.getCount() > 0 && i >= 0)
      return t.getItem(i); 
    return null;
  }
  
  @CapturedViewProperty
  public int getCount() {
    return this.mItemCount;
  }
  
  public int getPositionForView(View paramView) {
    try {
      while (true) {
        View view = (View)paramView.getParent();
        if (view != null) {
          boolean bool = view.equals(this);
          if (!bool) {
            paramView = view;
            continue;
          } 
        } 
        break;
      } 
      if (paramView != null) {
        int i = getChildCount();
        for (byte b = 0; b < i; b++) {
          if (getChildAt(b).equals(paramView))
            return this.mFirstPosition + b; 
        } 
      } 
      return -1;
    } catch (ClassCastException classCastException) {
      return -1;
    } 
  }
  
  public int getFirstVisiblePosition() {
    return this.mFirstPosition;
  }
  
  public int getLastVisiblePosition() {
    return this.mFirstPosition + getChildCount() - 1;
  }
  
  @RemotableViewMethod
  public void setEmptyView(View paramView) {
    this.mEmptyView = paramView;
    boolean bool1 = true;
    if (paramView != null && 
      paramView.getImportantForAccessibility() == 0)
      paramView.setImportantForAccessibility(1); 
    paramView = (View)getAdapter();
    boolean bool2 = bool1;
    if (paramView != null)
      if (paramView.isEmpty()) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    updateEmptyStatus(bool2);
  }
  
  public View getEmptyView() {
    return this.mEmptyView;
  }
  
  boolean isInFilterMode() {
    return false;
  }
  
  public void setFocusable(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getAdapter : ()Landroid/widget/Adapter;
    //   4: astore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: aload_2
    //   8: ifnull -> 29
    //   11: aload_2
    //   12: invokeinterface getCount : ()I
    //   17: ifne -> 23
    //   20: goto -> 29
    //   23: iconst_0
    //   24: istore #4
    //   26: goto -> 32
    //   29: iconst_1
    //   30: istore #4
    //   32: aload_0
    //   33: iload_1
    //   34: putfield mDesiredFocusableState : I
    //   37: iload_1
    //   38: bipush #17
    //   40: iand
    //   41: ifne -> 49
    //   44: aload_0
    //   45: iconst_0
    //   46: putfield mDesiredFocusableInTouchModeState : Z
    //   49: iload #4
    //   51: ifeq -> 64
    //   54: iload_3
    //   55: istore #4
    //   57: aload_0
    //   58: invokevirtual isInFilterMode : ()Z
    //   61: ifeq -> 67
    //   64: iload_1
    //   65: istore #4
    //   67: aload_0
    //   68: iload #4
    //   70: invokespecial setFocusable : (I)V
    //   73: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #753	-> 0
    //   #754	-> 5
    //   #756	-> 32
    //   #757	-> 37
    //   #758	-> 44
    //   #761	-> 49
    //   #762	-> 73
  }
  
  public void setFocusableInTouchMode(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getAdapter : ()Landroid/widget/Adapter;
    //   4: astore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: aload_2
    //   8: ifnull -> 29
    //   11: aload_2
    //   12: invokeinterface getCount : ()I
    //   17: ifne -> 23
    //   20: goto -> 29
    //   23: iconst_0
    //   24: istore #4
    //   26: goto -> 32
    //   29: iconst_1
    //   30: istore #4
    //   32: aload_0
    //   33: iload_1
    //   34: putfield mDesiredFocusableInTouchModeState : Z
    //   37: iload_1
    //   38: ifeq -> 46
    //   41: aload_0
    //   42: iconst_1
    //   43: putfield mDesiredFocusableState : I
    //   46: iload_3
    //   47: istore #5
    //   49: iload_1
    //   50: ifeq -> 71
    //   53: iload #4
    //   55: ifeq -> 68
    //   58: iload_3
    //   59: istore #5
    //   61: aload_0
    //   62: invokevirtual isInFilterMode : ()Z
    //   65: ifeq -> 71
    //   68: iconst_1
    //   69: istore #5
    //   71: aload_0
    //   72: iload #5
    //   74: invokespecial setFocusableInTouchMode : (Z)V
    //   77: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #766	-> 0
    //   #767	-> 5
    //   #769	-> 32
    //   #770	-> 37
    //   #771	-> 41
    //   #774	-> 46
    //   #775	-> 77
  }
  
  void checkFocus() {
    int i;
    boolean bool2;
    T t = getAdapter();
    boolean bool1 = true;
    if (t == null || t.getCount() == 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (!i || isInFilterMode()) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i && this.mDesiredFocusableInTouchModeState) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    super.setFocusableInTouchMode(bool2);
    if (i) {
      i = this.mDesiredFocusableState;
    } else {
      i = 0;
    } 
    super.setFocusable(i);
    if (this.mEmptyView != null) {
      bool2 = bool1;
      if (t != null)
        if (t.isEmpty()) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
      updateEmptyStatus(bool2);
    } 
  }
  
  private void updateEmptyStatus(boolean paramBoolean) {
    if (isInFilterMode())
      paramBoolean = false; 
    if (paramBoolean) {
      View view = this.mEmptyView;
      if (view != null) {
        view.setVisibility(0);
        setVisibility(8);
      } else {
        setVisibility(0);
      } 
      if (this.mDataChanged)
        onLayout(false, this.mLeft, this.mTop, this.mRight, this.mBottom); 
    } else {
      View view = this.mEmptyView;
      if (view != null)
        view.setVisibility(8); 
      setVisibility(0);
    } 
  }
  
  public Object getItemAtPosition(int paramInt) {
    null = getAdapter();
    return (null == null || paramInt < 0) ? null : null.getItem(paramInt);
  }
  
  public long getItemIdAtPosition(int paramInt) {
    T t = getAdapter();
    return (t == null || paramInt < 0) ? Long.MIN_VALUE : t.getItemId(paramInt);
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener) {
    throw new RuntimeException("Don't call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead");
  }
  
  protected void dispatchSaveInstanceState(SparseArray<Parcelable> paramSparseArray) {
    dispatchFreezeSelfOnly(paramSparseArray);
  }
  
  protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray) {
    dispatchThawSelfOnly(paramSparseArray);
  }
  
  class AdapterDataSetObserver extends DataSetObserver {
    private Parcelable mInstanceState = null;
    
    final AdapterView this$0;
    
    public void onChanged() {
      AdapterView.this.mDataChanged = true;
      AdapterView<T> adapterView = AdapterView.this;
      adapterView.mOldItemCount = adapterView.mItemCount;
      adapterView = AdapterView.this;
      adapterView.mItemCount = adapterView.getAdapter().getCount();
      if (AdapterView.this.getAdapter().hasStableIds() && this.mInstanceState != null && AdapterView.this.mOldItemCount == 0 && AdapterView.this.mItemCount > 0) {
        AdapterView.this.onRestoreInstanceState(this.mInstanceState);
        this.mInstanceState = null;
      } else {
        AdapterView.this.rememberSyncState();
      } 
      AdapterView.this.checkFocus();
      AdapterView.this.requestLayout();
    }
    
    public void onInvalidated() {
      AdapterView.this.mDataChanged = true;
      if (AdapterView.this.getAdapter().hasStableIds())
        this.mInstanceState = AdapterView.this.onSaveInstanceState(); 
      AdapterView adapterView = AdapterView.this;
      adapterView.mOldItemCount = adapterView.mItemCount;
      AdapterView.this.mItemCount = 0;
      AdapterView.this.mSelectedPosition = -1;
      AdapterView.this.mSelectedRowId = Long.MIN_VALUE;
      AdapterView.this.mNextSelectedPosition = -1;
      AdapterView.this.mNextSelectedRowId = Long.MIN_VALUE;
      AdapterView.this.mNeedSync = false;
      AdapterView.this.checkFocus();
      AdapterView.this.requestLayout();
    }
    
    public void clearSavedState() {
      this.mInstanceState = null;
    }
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    removeCallbacks(this.mSelectionNotifier);
  }
  
  class OnItemClickListener {
    public abstract void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long);
  }
  
  class OnItemLongClickListener {
    public abstract boolean onItemLongClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long);
  }
  
  class OnItemSelectedListener {
    public abstract void onItemSelected(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long);
    
    public abstract void onNothingSelected(AdapterView<?> param1AdapterView);
  }
  
  class SelectionNotifier implements Runnable {
    final AdapterView this$0;
    
    private SelectionNotifier() {}
    
    public void run() {
      AdapterView.access$202(AdapterView.this, (SelectionNotifier)null);
      if (AdapterView.this.mDataChanged && AdapterView.this.getViewRootImpl() != null) {
        AdapterView adapterView = AdapterView.this;
        if (adapterView.getViewRootImpl().isLayoutRequested()) {
          if (AdapterView.this.getAdapter() != null)
            AdapterView.access$202(AdapterView.this, this); 
          return;
        } 
      } 
      AdapterView.this.dispatchOnItemSelected();
    }
  }
  
  void selectionChanged() {
    // Byte code:
    //   0: aload_0
    //   1: aconst_null
    //   2: putfield mPendingSelectionNotifier : Landroid/widget/AdapterView$SelectionNotifier;
    //   5: aload_0
    //   6: getfield mOnItemSelectedListener : Landroid/widget/AdapterView$OnItemSelectedListener;
    //   9: ifnonnull -> 27
    //   12: aload_0
    //   13: getfield mContext : Landroid/content/Context;
    //   16: astore_1
    //   17: aload_1
    //   18: invokestatic getInstance : (Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;
    //   21: invokevirtual isEnabled : ()Z
    //   24: ifeq -> 91
    //   27: aload_0
    //   28: getfield mInLayout : Z
    //   31: ifne -> 51
    //   34: aload_0
    //   35: getfield mBlockLayoutRequests : Z
    //   38: ifeq -> 44
    //   41: goto -> 51
    //   44: aload_0
    //   45: invokespecial dispatchOnItemSelected : ()V
    //   48: goto -> 91
    //   51: aload_0
    //   52: getfield mSelectionNotifier : Landroid/widget/AdapterView$SelectionNotifier;
    //   55: astore_1
    //   56: aload_1
    //   57: ifnonnull -> 76
    //   60: aload_0
    //   61: new android/widget/AdapterView$SelectionNotifier
    //   64: dup
    //   65: aload_0
    //   66: aconst_null
    //   67: invokespecial <init> : (Landroid/widget/AdapterView;Landroid/widget/AdapterView$1;)V
    //   70: putfield mSelectionNotifier : Landroid/widget/AdapterView$SelectionNotifier;
    //   73: goto -> 82
    //   76: aload_0
    //   77: aload_1
    //   78: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)Z
    //   81: pop
    //   82: aload_0
    //   83: aload_0
    //   84: getfield mSelectionNotifier : Landroid/widget/AdapterView$SelectionNotifier;
    //   87: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   90: pop
    //   91: aload_0
    //   92: getfield mContext : Landroid/content/Context;
    //   95: ldc_w android/view/autofill/AutofillManager
    //   98: invokevirtual getSystemService : (Ljava/lang/Class;)Ljava/lang/Object;
    //   101: checkcast android/view/autofill/AutofillManager
    //   104: astore_1
    //   105: aload_1
    //   106: ifnull -> 114
    //   109: aload_1
    //   110: aload_0
    //   111: invokevirtual notifyValueChanged : (Landroid/view/View;)V
    //   114: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #939	-> 0
    //   #941	-> 5
    //   #942	-> 17
    //   #943	-> 27
    //   #955	-> 44
    //   #948	-> 51
    //   #949	-> 60
    //   #951	-> 76
    //   #953	-> 82
    //   #959	-> 91
    //   #960	-> 105
    //   #961	-> 109
    //   #963	-> 114
  }
  
  private void dispatchOnItemSelected() {
    fireOnSelected();
    performAccessibilityActionsOnSelected();
  }
  
  private void fireOnSelected() {
    if (this.mOnItemSelectedListener == null)
      return; 
    int i = getSelectedItemPosition();
    if (i >= 0) {
      View view = getSelectedView();
      OnItemSelectedListener onItemSelectedListener = this.mOnItemSelectedListener;
      long l = getAdapter().getItemId(i);
      onItemSelectedListener.onItemSelected(this, view, i, l);
    } else {
      this.mOnItemSelectedListener.onNothingSelected(this);
    } 
  }
  
  private void performAccessibilityActionsOnSelected() {
    if (!AccessibilityManager.getInstance(this.mContext).isEnabled())
      return; 
    int i = getSelectedItemPosition();
    if (i >= 0)
      sendAccessibilityEvent(4); 
  }
  
  public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    View view = getSelectedView();
    if (view != null && view.getVisibility() == 0 && 
      view.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent))
      return true; 
    return false;
  }
  
  public boolean onRequestSendAccessibilityEventInternal(View paramView, AccessibilityEvent paramAccessibilityEvent) {
    if (super.onRequestSendAccessibilityEventInternal(paramView, paramAccessibilityEvent)) {
      AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain();
      onInitializeAccessibilityEvent(accessibilityEvent);
      paramView.dispatchPopulateAccessibilityEvent(accessibilityEvent);
      paramAccessibilityEvent.appendRecord(accessibilityEvent);
      return true;
    } 
    return false;
  }
  
  public CharSequence getAccessibilityClassName() {
    return AdapterView.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setScrollable(isScrollableForAccessibility());
    View view = getSelectedView();
    if (view != null)
      paramAccessibilityNodeInfo.setEnabled(view.isEnabled()); 
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    paramAccessibilityEvent.setScrollable(isScrollableForAccessibility());
    View view = getSelectedView();
    if (view != null)
      paramAccessibilityEvent.setEnabled(view.isEnabled()); 
    paramAccessibilityEvent.setCurrentItemIndex(getSelectedItemPosition());
    paramAccessibilityEvent.setFromIndex(getFirstVisiblePosition());
    paramAccessibilityEvent.setToIndex(getLastVisiblePosition());
    paramAccessibilityEvent.setItemCount(getCount());
  }
  
  private boolean isScrollableForAccessibility() {
    T t = getAdapter();
    boolean bool = false;
    if (t != null) {
      int i = t.getCount();
      if (i > 0 && (
        getFirstVisiblePosition() > 0 || getLastVisiblePosition() < i - 1))
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  protected boolean canAnimate() {
    boolean bool;
    if (super.canAnimate() && this.mItemCount > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void handleDataChanged() {
    int i = this.mItemCount;
    int j = 0, k = 0;
    if (i > 0) {
      int m = k;
      if (this.mNeedSync) {
        this.mNeedSync = false;
        j = findSyncPosition();
        m = k;
        if (j >= 0) {
          int n = lookForSelectablePosition(j, true);
          m = k;
          if (n == j) {
            setNextSelectedPositionInt(j);
            m = 1;
          } 
        } 
      } 
      j = m;
      if (!m) {
        k = getSelectedItemPosition();
        j = k;
        if (k >= i)
          j = i - 1; 
        i = j;
        if (j < 0)
          i = 0; 
        j = lookForSelectablePosition(i, true);
        k = j;
        if (j < 0)
          k = lookForSelectablePosition(i, false); 
        j = m;
        if (k >= 0) {
          setNextSelectedPositionInt(k);
          checkSelectionChanged();
          j = 1;
        } 
      } 
    } 
    if (j == 0) {
      this.mSelectedPosition = -1;
      this.mSelectedRowId = Long.MIN_VALUE;
      this.mNextSelectedPosition = -1;
      this.mNextSelectedRowId = Long.MIN_VALUE;
      this.mNeedSync = false;
      checkSelectionChanged();
    } 
    notifySubtreeAccessibilityStateChangedIfNeeded();
  }
  
  void checkSelectionChanged() {
    if (this.mSelectedPosition != this.mOldSelectedPosition || this.mSelectedRowId != this.mOldSelectedRowId) {
      selectionChanged();
      this.mOldSelectedPosition = this.mSelectedPosition;
      this.mOldSelectedRowId = this.mSelectedRowId;
    } 
    SelectionNotifier selectionNotifier = this.mPendingSelectionNotifier;
    if (selectionNotifier != null)
      selectionNotifier.run(); 
  }
  
  int findSyncPosition() {
    int i = this.mItemCount;
    if (i == 0)
      return -1; 
    long l1 = this.mSyncRowId;
    int j = this.mSyncPosition;
    if (l1 == Long.MIN_VALUE)
      return -1; 
    j = Math.max(0, j);
    j = Math.min(i - 1, j);
    long l2 = SystemClock.uptimeMillis();
    int k = j;
    int m = j;
    boolean bool = false;
    T t = getAdapter();
    if (t == null)
      return -1; 
    while (SystemClock.uptimeMillis() <= l2 + 100L) {
      boolean bool2;
      long l = t.getItemId(j);
      if (l == l1)
        return j; 
      boolean bool1 = true;
      if (m == i - 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (k != 0)
        bool1 = false; 
      if (bool2 && bool1)
        break; 
      if (bool1 || (bool && !bool2)) {
        j = ++m;
        bool = false;
        continue;
      } 
      if (bool2 || (!bool && !bool1)) {
        j = --k;
        bool = true;
      } 
    } 
    return -1;
  }
  
  int lookForSelectablePosition(int paramInt, boolean paramBoolean) {
    return paramInt;
  }
  
  void setSelectedPositionInt(int paramInt) {
    this.mSelectedPosition = paramInt;
    this.mSelectedRowId = getItemIdAtPosition(paramInt);
  }
  
  void setNextSelectedPositionInt(int paramInt) {
    this.mNextSelectedPosition = paramInt;
    long l = getItemIdAtPosition(paramInt);
    if (this.mNeedSync && this.mSyncMode == 0 && paramInt >= 0) {
      this.mSyncPosition = paramInt;
      this.mSyncRowId = l;
    } 
  }
  
  void rememberSyncState() {
    if (getChildCount() > 0) {
      this.mNeedSync = true;
      this.mSyncHeight = this.mLayoutHeight;
      int i = this.mSelectedPosition;
      if (i >= 0) {
        View view = getChildAt(i - this.mFirstPosition);
        this.mSyncRowId = this.mNextSelectedRowId;
        this.mSyncPosition = this.mNextSelectedPosition;
        if (view != null)
          this.mSpecificTop = view.getTop(); 
        this.mSyncMode = 0;
      } else {
        View view = getChildAt(0);
        T t = getAdapter();
        i = this.mFirstPosition;
        if (i >= 0 && i < t.getCount()) {
          this.mSyncRowId = t.getItemId(this.mFirstPosition);
        } else {
          this.mSyncRowId = -1L;
        } 
        this.mSyncPosition = this.mFirstPosition;
        if (view != null)
          this.mSpecificTop = view.getTop(); 
        this.mSyncMode = 1;
      } 
    } 
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("scrolling:firstPosition", this.mFirstPosition);
    paramViewHierarchyEncoder.addProperty("list:nextSelectedPosition", this.mNextSelectedPosition);
    paramViewHierarchyEncoder.addProperty("list:nextSelectedRowId", (float)this.mNextSelectedRowId);
    paramViewHierarchyEncoder.addProperty("list:selectedPosition", this.mSelectedPosition);
    paramViewHierarchyEncoder.addProperty("list:itemCount", this.mItemCount);
  }
  
  public void onProvideAutofillStructure(ViewStructure paramViewStructure, int paramInt) {
    super.onProvideAutofillStructure(paramViewStructure, paramInt);
  }
  
  protected void onProvideStructure(ViewStructure paramViewStructure, int paramInt1, int paramInt2) {
    super.onProvideStructure(paramViewStructure, paramInt1, paramInt2);
    if (paramInt1 == 1 || paramInt1 == 2) {
      T t = getAdapter();
      if (t == null)
        return; 
      CharSequence[] arrayOfCharSequence = t.getAutofillOptions();
      if (arrayOfCharSequence != null)
        paramViewStructure.setAutofillOptions(arrayOfCharSequence); 
    } 
  }
  
  public abstract T getAdapter();
  
  public abstract View getSelectedView();
  
  public abstract void setAdapter(T paramT);
  
  public abstract void setSelection(int paramInt);
}
