package android.widget;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.Insets;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.AllCapsTransformationMethod;
import android.text.method.TransformationMethod2;
import android.util.AttributeSet;
import android.util.FloatProperty;
import android.util.MathUtils;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.R;

public class Switch extends CompoundButton {
  private static final int[] CHECKED_STATE_SET = new int[] { 16842912 };
  
  private static final int MONOSPACE = 3;
  
  private static final int SANS = 1;
  
  private static final int SERIF = 2;
  
  private static final int THUMB_ANIMATION_DURATION = 250;
  
  public Switch(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public Switch(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843839);
  }
  
  public Switch(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public Switch(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    boolean bool;
    this.mThumbTintList = null;
    this.mThumbBlendMode = null;
    this.mHasThumbTint = false;
    this.mHasThumbTintMode = false;
    this.mTrackTintList = null;
    this.mTrackBlendMode = null;
    this.mHasTrackTint = false;
    this.mHasTrackTintMode = false;
    this.mVelocityTracker = VelocityTracker.obtain();
    this.mTempRect = new Rect();
    this.mTextPaint = new TextPaint(1);
    Resources resources = getResources();
    this.mTextPaint.density = (resources.getDisplayMetrics()).density;
    this.mTextPaint.setCompatibilityScaling((resources.getCompatibilityInfo()).applicationScale);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Switch, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.Switch, paramAttributeSet, typedArray, paramInt1, paramInt2);
    Drawable drawable = typedArray.getDrawable(2);
    if (drawable != null)
      drawable.setCallback(this); 
    this.mTrackDrawable = drawable = typedArray.getDrawable(4);
    if (drawable != null)
      drawable.setCallback(this); 
    this.mTextOn = typedArray.getText(0);
    this.mTextOff = typedArray.getText(1);
    this.mShowText = typedArray.getBoolean(11, true);
    this.mThumbTextPadding = typedArray.getDimensionPixelSize(7, 0);
    this.mSwitchMinWidth = typedArray.getDimensionPixelSize(5, 0);
    this.mSwitchPadding = typedArray.getDimensionPixelSize(6, 0);
    this.mSplitTrack = typedArray.getBoolean(8, false);
    if ((paramContext.getApplicationInfo()).targetSdkVersion >= 28) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mUseFallbackLineSpacing = bool;
    ColorStateList colorStateList2 = typedArray.getColorStateList(9);
    if (colorStateList2 != null) {
      this.mThumbTintList = colorStateList2;
      this.mHasThumbTint = true;
    } 
    paramInt1 = typedArray.getInt(10, -1);
    BlendMode blendMode2 = Drawable.parseBlendMode(paramInt1, null);
    if (this.mThumbBlendMode != blendMode2) {
      this.mThumbBlendMode = blendMode2;
      this.mHasThumbTintMode = true;
    } 
    if (this.mHasThumbTint || this.mHasThumbTintMode)
      applyThumbTint(); 
    ColorStateList colorStateList1 = typedArray.getColorStateList(12);
    if (colorStateList1 != null) {
      this.mTrackTintList = colorStateList1;
      this.mHasTrackTint = true;
    } 
    paramInt1 = typedArray.getInt(13, -1);
    BlendMode blendMode1 = Drawable.parseBlendMode(paramInt1, null);
    if (this.mTrackBlendMode != blendMode1) {
      this.mTrackBlendMode = blendMode1;
      this.mHasTrackTintMode = true;
    } 
    if (this.mHasTrackTint || this.mHasTrackTintMode)
      applyTrackTint(); 
    paramInt1 = typedArray.getResourceId(3, 0);
    if (paramInt1 != 0)
      setSwitchTextAppearance(paramContext, paramInt1); 
    typedArray.recycle();
    ViewConfiguration viewConfiguration = ViewConfiguration.get(paramContext);
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    this.mMinFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
    refreshDrawableState();
    setDefaultStateDescritption();
    setChecked(isChecked());
  }
  
  public void setSwitchTextAppearance(Context paramContext, int paramInt) {
    int[] arrayOfInt = R.styleable.TextAppearance;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramInt, arrayOfInt);
    ColorStateList colorStateList = typedArray.getColorStateList(3);
    if (colorStateList != null) {
      this.mTextColors = colorStateList;
    } else {
      this.mTextColors = getTextColors();
    } 
    paramInt = typedArray.getDimensionPixelSize(0, 0);
    if (paramInt != 0 && 
      paramInt != this.mTextPaint.getTextSize()) {
      this.mTextPaint.setTextSize(paramInt);
      requestLayout();
    } 
    paramInt = typedArray.getInt(1, -1);
    int i = typedArray.getInt(2, -1);
    setSwitchTypefaceByIndex(paramInt, i);
    boolean bool = typedArray.getBoolean(11, false);
    if (bool) {
      AllCapsTransformationMethod allCapsTransformationMethod = new AllCapsTransformationMethod(getContext());
      allCapsTransformationMethod.setLengthChangesAllowed(true);
    } else {
      this.mSwitchTransformationMethod = null;
    } 
    typedArray.recycle();
  }
  
  private void setSwitchTypefaceByIndex(int paramInt1, int paramInt2) {
    Typeface typeface = null;
    if (paramInt1 != 1) {
      if (paramInt1 != 2) {
        if (paramInt1 == 3)
          typeface = Typeface.MONOSPACE; 
      } else {
        typeface = Typeface.SERIF;
      } 
    } else {
      typeface = Typeface.SANS_SERIF;
    } 
    setSwitchTypeface(typeface, paramInt2);
  }
  
  public void setSwitchTypeface(Typeface paramTypeface, int paramInt) {
    TextPaint textPaint;
    float f = 0.0F;
    boolean bool = false;
    if (paramInt > 0) {
      boolean bool1;
      if (paramTypeface == null) {
        paramTypeface = Typeface.defaultFromStyle(paramInt);
      } else {
        paramTypeface = Typeface.create(paramTypeface, paramInt);
      } 
      setSwitchTypeface(paramTypeface);
      if (paramTypeface != null) {
        bool1 = paramTypeface.getStyle();
      } else {
        bool1 = false;
      } 
      paramInt = (bool1 ^ 0xFFFFFFFF) & paramInt;
      textPaint = this.mTextPaint;
      if ((paramInt & 0x1) != 0)
        bool = true; 
      textPaint.setFakeBoldText(bool);
      textPaint = this.mTextPaint;
      if ((paramInt & 0x2) != 0)
        f = -0.25F; 
      textPaint.setTextSkewX(f);
    } else {
      this.mTextPaint.setFakeBoldText(false);
      this.mTextPaint.setTextSkewX(0.0F);
      setSwitchTypeface((Typeface)textPaint);
    } 
  }
  
  public void setSwitchTypeface(Typeface paramTypeface) {
    if (this.mTextPaint.getTypeface() != paramTypeface) {
      this.mTextPaint.setTypeface(paramTypeface);
      requestLayout();
      invalidate();
    } 
  }
  
  public void setSwitchPadding(int paramInt) {
    this.mSwitchPadding = paramInt;
    requestLayout();
  }
  
  public int getSwitchPadding() {
    return this.mSwitchPadding;
  }
  
  public void setSwitchMinWidth(int paramInt) {
    this.mSwitchMinWidth = paramInt;
    requestLayout();
  }
  
  public int getSwitchMinWidth() {
    return this.mSwitchMinWidth;
  }
  
  public void setThumbTextPadding(int paramInt) {
    this.mThumbTextPadding = paramInt;
    requestLayout();
  }
  
  public int getThumbTextPadding() {
    return this.mThumbTextPadding;
  }
  
  public void setTrackDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mTrackDrawable;
    if (drawable != null)
      drawable.setCallback(null); 
    this.mTrackDrawable = paramDrawable;
    if (paramDrawable != null)
      paramDrawable.setCallback(this); 
    requestLayout();
  }
  
  public void setTrackResource(int paramInt) {
    setTrackDrawable(getContext().getDrawable(paramInt));
  }
  
  public Drawable getTrackDrawable() {
    return this.mTrackDrawable;
  }
  
  public void setTrackTintList(ColorStateList paramColorStateList) {
    this.mTrackTintList = paramColorStateList;
    this.mHasTrackTint = true;
    applyTrackTint();
  }
  
  public ColorStateList getTrackTintList() {
    return this.mTrackTintList;
  }
  
  public void setTrackTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setTrackTintBlendMode((BlendMode)paramMode);
  }
  
  public void setTrackTintBlendMode(BlendMode paramBlendMode) {
    this.mTrackBlendMode = paramBlendMode;
    this.mHasTrackTintMode = true;
    applyTrackTint();
  }
  
  public PorterDuff.Mode getTrackTintMode() {
    BlendMode blendMode = getTrackTintBlendMode();
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getTrackTintBlendMode() {
    return this.mTrackBlendMode;
  }
  
  private void applyTrackTint() {
    if (this.mTrackDrawable != null && (this.mHasTrackTint || this.mHasTrackTintMode)) {
      Drawable drawable = this.mTrackDrawable.mutate();
      if (this.mHasTrackTint)
        drawable.setTintList(this.mTrackTintList); 
      if (this.mHasTrackTintMode)
        this.mTrackDrawable.setTintBlendMode(this.mTrackBlendMode); 
      if (this.mTrackDrawable.isStateful())
        this.mTrackDrawable.setState(getDrawableState()); 
    } 
  }
  
  public void setThumbDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mThumbDrawable;
    if (drawable != null)
      drawable.setCallback(null); 
    this.mThumbDrawable = paramDrawable;
    if (paramDrawable != null)
      paramDrawable.setCallback(this); 
    requestLayout();
  }
  
  public void setThumbResource(int paramInt) {
    setThumbDrawable(getContext().getDrawable(paramInt));
  }
  
  public Drawable getThumbDrawable() {
    return this.mThumbDrawable;
  }
  
  public void setThumbTintList(ColorStateList paramColorStateList) {
    this.mThumbTintList = paramColorStateList;
    this.mHasThumbTint = true;
    applyThumbTint();
  }
  
  public ColorStateList getThumbTintList() {
    return this.mThumbTintList;
  }
  
  public void setThumbTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setThumbTintBlendMode((BlendMode)paramMode);
  }
  
  public void setThumbTintBlendMode(BlendMode paramBlendMode) {
    this.mThumbBlendMode = paramBlendMode;
    this.mHasThumbTintMode = true;
    applyThumbTint();
  }
  
  public PorterDuff.Mode getThumbTintMode() {
    BlendMode blendMode = getThumbTintBlendMode();
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getThumbTintBlendMode() {
    return this.mThumbBlendMode;
  }
  
  private void applyThumbTint() {
    if (this.mThumbDrawable != null && (this.mHasThumbTint || this.mHasThumbTintMode)) {
      Drawable drawable = this.mThumbDrawable.mutate();
      if (this.mHasThumbTint)
        drawable.setTintList(this.mThumbTintList); 
      if (this.mHasThumbTintMode)
        this.mThumbDrawable.setTintBlendMode(this.mThumbBlendMode); 
      if (this.mThumbDrawable.isStateful())
        this.mThumbDrawable.setState(getDrawableState()); 
    } 
  }
  
  public void setSplitTrack(boolean paramBoolean) {
    this.mSplitTrack = paramBoolean;
    invalidate();
  }
  
  public boolean getSplitTrack() {
    return this.mSplitTrack;
  }
  
  public CharSequence getTextOn() {
    return this.mTextOn;
  }
  
  public void setTextOn(CharSequence paramCharSequence) {
    this.mTextOn = paramCharSequence;
    requestLayout();
    setDefaultStateDescritption();
  }
  
  public CharSequence getTextOff() {
    return this.mTextOff;
  }
  
  public void setTextOff(CharSequence paramCharSequence) {
    this.mTextOff = paramCharSequence;
    requestLayout();
    setDefaultStateDescritption();
  }
  
  public void setShowText(boolean paramBoolean) {
    if (this.mShowText != paramBoolean) {
      this.mShowText = paramBoolean;
      requestLayout();
    } 
  }
  
  public boolean getShowText() {
    return this.mShowText;
  }
  
  public void onMeasure(int paramInt1, int paramInt2) {
    int i;
    if (this.mShowText) {
      if (this.mOnLayout == null)
        this.mOnLayout = makeLayout(this.mTextOn); 
      if (this.mOffLayout == null)
        this.mOffLayout = makeLayout(this.mTextOff); 
    } 
    Rect rect = this.mTempRect;
    Drawable drawable2 = this.mThumbDrawable;
    if (drawable2 != null) {
      drawable2.getPadding(rect);
      i = this.mThumbDrawable.getIntrinsicWidth() - rect.left - rect.right;
      j = this.mThumbDrawable.getIntrinsicHeight();
    } else {
      i = 0;
      j = 0;
    } 
    if (this.mShowText) {
      k = Math.max(this.mOnLayout.getWidth(), this.mOffLayout.getWidth()) + this.mThumbTextPadding * 2;
    } else {
      k = 0;
    } 
    this.mThumbWidth = Math.max(k, i);
    drawable2 = this.mTrackDrawable;
    if (drawable2 != null) {
      drawable2.getPadding(rect);
      i = this.mTrackDrawable.getIntrinsicHeight();
    } else {
      rect.setEmpty();
      i = 0;
    } 
    int m = rect.left;
    int n = rect.right;
    Drawable drawable1 = this.mThumbDrawable;
    int i1 = m, k = n;
    if (drawable1 != null) {
      Insets insets = drawable1.getOpticalInsets();
      i1 = Math.max(m, insets.left);
      k = Math.max(n, insets.right);
    } 
    k = Math.max(this.mSwitchMinWidth, this.mThumbWidth * 2 + i1 + k);
    int j = Math.max(i, j);
    this.mSwitchWidth = k;
    this.mSwitchHeight = j;
    super.onMeasure(paramInt1, paramInt2);
    paramInt1 = getMeasuredHeight();
    if (paramInt1 < j)
      setMeasuredDimension(getMeasuredWidthAndState(), j); 
  }
  
  public void onPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    CharSequence charSequence;
    super.onPopulateAccessibilityEventInternal(paramAccessibilityEvent);
    if (isChecked()) {
      charSequence = this.mTextOn;
    } else {
      charSequence = this.mTextOff;
    } 
    if (charSequence != null)
      paramAccessibilityEvent.getText().add(charSequence); 
  }
  
  private Layout makeLayout(CharSequence paramCharSequence) {
    TransformationMethod2 transformationMethod2 = this.mSwitchTransformationMethod;
    if (transformationMethod2 != null)
      paramCharSequence = transformationMethod2.getTransformation(paramCharSequence, this); 
    int i = paramCharSequence.length();
    TextPaint textPaint = this.mTextPaint;
    TextDirectionHeuristic textDirectionHeuristic = getTextDirectionHeuristic();
    i = (int)Math.ceil(Layout.getDesiredWidth(paramCharSequence, 0, i, textPaint, textDirectionHeuristic));
    StaticLayout.Builder builder = StaticLayout.Builder.obtain(paramCharSequence, 0, paramCharSequence.length(), this.mTextPaint, i);
    boolean bool = this.mUseFallbackLineSpacing;
    builder = builder.setUseLineSpacingFromFallbacks(bool);
    return builder.build();
  }
  
  private boolean hitThumb(float paramFloat1, float paramFloat2) {
    Drawable drawable = this.mThumbDrawable;
    boolean bool1 = false;
    if (drawable == null)
      return false; 
    int i = getThumbOffset();
    this.mThumbDrawable.getPadding(this.mTempRect);
    int j = this.mSwitchTop, k = this.mTouchSlop;
    int m = this.mSwitchLeft + i - k;
    int n = this.mThumbWidth, i1 = this.mTempRect.left, i2 = this.mTempRect.right;
    i = this.mTouchSlop;
    int i3 = this.mSwitchBottom;
    boolean bool2 = bool1;
    if (paramFloat1 > m) {
      bool2 = bool1;
      if (paramFloat1 < (n + m + i1 + i2 + i)) {
        bool2 = bool1;
        if (paramFloat2 > (j - k)) {
          bool2 = bool1;
          if (paramFloat2 < (i3 + i))
            bool2 = true; 
        } 
      } 
    } 
    return bool2;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    this.mVelocityTracker.addMovement(paramMotionEvent);
    int i = paramMotionEvent.getActionMasked();
    if (i != 0) {
      if (i != 1)
        if (i != 2) {
          if (i != 3)
            return super.onTouchEvent(paramMotionEvent); 
        } else {
          i = this.mTouchMode;
          if (i != 1) {
            if (i == 2) {
              float f1 = paramMotionEvent.getX();
              i = getThumbScrollRange();
              float f2 = f1 - this.mTouchX;
              if (i != 0) {
                f2 /= i;
              } else if (f2 > 0.0F) {
                f2 = 1.0F;
              } else {
                f2 = -1.0F;
              } 
              float f3 = f2;
              if (isLayoutRtl())
                f3 = -f2; 
              f2 = MathUtils.constrain(this.mThumbPosition + f3, 0.0F, 1.0F);
              if (f2 != this.mThumbPosition) {
                this.mTouchX = f1;
                setThumbPosition(f2);
              } 
              return true;
            } 
          } else {
            float f2 = paramMotionEvent.getX();
            float f1 = paramMotionEvent.getY();
            if (Math.abs(f2 - this.mTouchX) <= this.mTouchSlop) {
              float f = this.mTouchY;
              if (Math.abs(f1 - f) > this.mTouchSlop) {
                this.mTouchMode = 2;
                getParent().requestDisallowInterceptTouchEvent(true);
                this.mTouchX = f2;
                this.mTouchY = f1;
                return true;
              } 
            } else {
              this.mTouchMode = 2;
              getParent().requestDisallowInterceptTouchEvent(true);
              this.mTouchX = f2;
              this.mTouchY = f1;
              return true;
            } 
          } 
          return super.onTouchEvent(paramMotionEvent);
        }  
      if (this.mTouchMode == 2) {
        stopDrag(paramMotionEvent);
        super.onTouchEvent(paramMotionEvent);
        return true;
      } 
      this.mTouchMode = 0;
      this.mVelocityTracker.clear();
    } else {
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      if (isEnabled() && hitThumb(f1, f2)) {
        this.mTouchMode = 1;
        this.mTouchX = f1;
        this.mTouchY = f2;
      } 
    } 
    return super.onTouchEvent(paramMotionEvent);
  }
  
  private void cancelSuperTouch(MotionEvent paramMotionEvent) {
    paramMotionEvent = MotionEvent.obtain(paramMotionEvent);
    paramMotionEvent.setAction(3);
    super.onTouchEvent(paramMotionEvent);
    paramMotionEvent.recycle();
  }
  
  private void stopDrag(MotionEvent paramMotionEvent) {
    this.mTouchMode = 0;
    int i = paramMotionEvent.getAction();
    boolean bool1 = true;
    if (i == 1 && isEnabled()) {
      i = 1;
    } else {
      i = 0;
    } 
    boolean bool2 = isChecked();
    if (i != 0) {
      this.mVelocityTracker.computeCurrentVelocity(1000);
      float f = this.mVelocityTracker.getXVelocity();
      if (Math.abs(f) > this.mMinFlingVelocity) {
        if (isLayoutRtl() ? (f < 0.0F) : (f > 0.0F))
          bool1 = false; 
      } else {
        bool1 = getTargetCheckedState();
      } 
    } else {
      bool1 = bool2;
    } 
    if (bool1 != bool2)
      playSoundEffect(0); 
    setChecked(bool1);
    cancelSuperTouch(paramMotionEvent);
  }
  
  private void animateThumbToCheckedState(boolean paramBoolean) {
    float f;
    if (paramBoolean) {
      f = 1.0F;
    } else {
      f = 0.0F;
    } 
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, THUMB_POS, new float[] { f });
    objectAnimator.setDuration(250L);
    this.mPositionAnimator.setAutoCancel(true);
    this.mPositionAnimator.start();
  }
  
  private void cancelPositionAnimator() {
    ObjectAnimator objectAnimator = this.mPositionAnimator;
    if (objectAnimator != null)
      objectAnimator.cancel(); 
  }
  
  private boolean getTargetCheckedState() {
    boolean bool;
    if (this.mThumbPosition > 0.5F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setThumbPosition(float paramFloat) {
    this.mThumbPosition = paramFloat;
    invalidate();
  }
  
  public void toggle() {
    setChecked(isChecked() ^ true);
  }
  
  protected CharSequence getButtonStateDescription() {
    if (isChecked()) {
      CharSequence charSequence3 = this.mTextOn, charSequence4 = charSequence3;
      if (charSequence3 == null)
        charSequence4 = getResources().getString(17039803); 
      return charSequence4;
    } 
    CharSequence charSequence1 = this.mTextOff, charSequence2 = charSequence1;
    if (charSequence1 == null)
      charSequence2 = getResources().getString(17039802); 
    return charSequence2;
  }
  
  public void setChecked(boolean paramBoolean) {
    super.setChecked(paramBoolean);
    paramBoolean = isChecked();
    if (isAttachedToWindow() && isLaidOut()) {
      animateThumbToCheckedState(paramBoolean);
    } else {
      float f;
      cancelPositionAnimator();
      if (paramBoolean) {
        f = 1.0F;
      } else {
        f = 0.0F;
      } 
      setThumbPosition(f);
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    paramInt2 = 0;
    paramInt1 = 0;
    if (this.mThumbDrawable != null) {
      Rect rect = this.mTempRect;
      Drawable drawable = this.mTrackDrawable;
      if (drawable != null) {
        drawable.getPadding(rect);
      } else {
        rect.setEmpty();
      } 
      Insets insets = this.mThumbDrawable.getOpticalInsets();
      paramInt2 = Math.max(0, insets.left - rect.left);
      paramInt1 = Math.max(0, insets.right - rect.right);
    } 
    if (isLayoutRtl()) {
      paramInt4 = getPaddingLeft() + paramInt2;
      paramInt3 = this.mSwitchWidth + paramInt4 - paramInt2 - paramInt1;
    } else {
      paramInt3 = getWidth() - getPaddingRight() - paramInt1;
      paramInt4 = paramInt3 - this.mSwitchWidth + paramInt2 + paramInt1;
    } 
    paramInt1 = getGravity() & 0x70;
    if (paramInt1 != 16) {
      if (paramInt1 != 80) {
        paramInt1 = getPaddingTop();
        paramInt2 = this.mSwitchHeight + paramInt1;
      } else {
        paramInt2 = getHeight() - getPaddingBottom();
        paramInt1 = paramInt2 - this.mSwitchHeight;
      } 
    } else {
      paramInt1 = (getPaddingTop() + getHeight() - getPaddingBottom()) / 2;
      paramInt2 = this.mSwitchHeight;
      paramInt1 -= paramInt2 / 2;
      paramInt2 += paramInt1;
    } 
    this.mSwitchLeft = paramInt4;
    this.mSwitchTop = paramInt1;
    this.mSwitchBottom = paramInt2;
    this.mSwitchRight = paramInt3;
  }
  
  public void draw(Canvas paramCanvas) {
    Insets insets;
    Rect rect = this.mTempRect;
    int i = this.mSwitchLeft;
    int j = this.mSwitchTop;
    int k = this.mSwitchRight;
    int m = this.mSwitchBottom;
    int n = getThumbOffset() + i;
    Drawable drawable2 = this.mThumbDrawable;
    if (drawable2 != null) {
      insets = drawable2.getOpticalInsets();
    } else {
      insets = Insets.NONE;
    } 
    Drawable drawable3 = this.mTrackDrawable;
    int i1 = n;
    if (drawable3 != null) {
      drawable3.getPadding(rect);
      int i2 = n + rect.left;
      n = j;
      int i3 = m;
      int i4 = i, i5 = n, i6 = k, i7 = i3;
      if (insets != Insets.NONE) {
        i1 = i;
        if (insets.left > rect.left)
          i1 = i + insets.left - rect.left; 
        i = n;
        if (insets.top > rect.top)
          i = n + insets.top - rect.top; 
        n = k;
        if (insets.right > rect.right)
          n = k - insets.right - rect.right; 
        i4 = i1;
        i5 = i;
        i6 = n;
        i7 = i3;
        if (insets.bottom > rect.bottom) {
          i7 = i3 - insets.bottom - rect.bottom;
          i6 = n;
          i5 = i;
          i4 = i1;
        } 
      } 
      this.mTrackDrawable.setBounds(i4, i5, i6, i7);
      i1 = i2;
    } 
    Drawable drawable1 = this.mThumbDrawable;
    if (drawable1 != null) {
      drawable1.getPadding(rect);
      k = i1 - rect.left;
      i1 = this.mThumbWidth + i1 + rect.right;
      this.mThumbDrawable.setBounds(k, j, i1, m);
      drawable1 = getBackground();
      if (drawable1 != null)
        drawable1.setHotspotBounds(k, j, i1, m); 
    } 
    super.draw(paramCanvas);
  }
  
  protected void onDraw(Canvas paramCanvas) {
    Layout layout;
    super.onDraw(paramCanvas);
    Rect rect = this.mTempRect;
    Drawable drawable1 = this.mTrackDrawable;
    if (drawable1 != null) {
      drawable1.getPadding(rect);
    } else {
      rect.setEmpty();
    } 
    int i = this.mSwitchTop;
    int j = this.mSwitchBottom;
    int k = rect.top;
    int m = rect.bottom;
    Drawable drawable2 = this.mThumbDrawable;
    if (drawable1 != null)
      if (this.mSplitTrack && drawable2 != null) {
        Insets insets = drawable2.getOpticalInsets();
        drawable2.copyBounds(rect);
        rect.left += insets.left;
        rect.right -= insets.right;
        int i1 = paramCanvas.save();
        paramCanvas.clipRect(rect, Region.Op.DIFFERENCE);
        drawable1.draw(paramCanvas);
        paramCanvas.restoreToCount(i1);
      } else {
        drawable1.draw(paramCanvas);
      }  
    int n = paramCanvas.save();
    if (drawable2 != null)
      drawable2.draw(paramCanvas); 
    if (getTargetCheckedState()) {
      layout = this.mOnLayout;
    } else {
      layout = this.mOffLayout;
    } 
    if (layout != null) {
      int[] arrayOfInt = getDrawableState();
      ColorStateList colorStateList = this.mTextColors;
      if (colorStateList != null)
        this.mTextPaint.setColor(colorStateList.getColorForState(arrayOfInt, 0)); 
      this.mTextPaint.drawableState = arrayOfInt;
      if (drawable2 != null) {
        Rect rect1 = drawable2.getBounds();
        i1 = rect1.left + rect1.right;
      } else {
        i1 = getWidth();
      } 
      int i2 = i1 / 2, i1 = layout.getWidth() / 2;
      j = (k + i + j - m) / 2;
      k = layout.getHeight() / 2;
      paramCanvas.translate((i2 - i1), (j - k));
      layout.draw(paramCanvas);
    } 
    paramCanvas.restoreToCount(n);
  }
  
  public int getCompoundPaddingLeft() {
    if (!isLayoutRtl())
      return super.getCompoundPaddingLeft(); 
    int i = super.getCompoundPaddingLeft() + this.mSwitchWidth;
    int j = i;
    if (!TextUtils.isEmpty(getText()))
      j = i + this.mSwitchPadding; 
    return j;
  }
  
  public int getCompoundPaddingRight() {
    if (isLayoutRtl())
      return super.getCompoundPaddingRight(); 
    int i = super.getCompoundPaddingRight() + this.mSwitchWidth;
    int j = i;
    if (!TextUtils.isEmpty(getText()))
      j = i + this.mSwitchPadding; 
    return j;
  }
  
  private int getThumbOffset() {
    float f;
    if (isLayoutRtl()) {
      f = 1.0F - this.mThumbPosition;
    } else {
      f = this.mThumbPosition;
    } 
    return (int)(getThumbScrollRange() * f + 0.5F);
  }
  
  private int getThumbScrollRange() {
    Drawable drawable = this.mTrackDrawable;
    if (drawable != null) {
      Insets insets;
      Rect rect = this.mTempRect;
      drawable.getPadding(rect);
      drawable = this.mThumbDrawable;
      if (drawable != null) {
        insets = drawable.getOpticalInsets();
      } else {
        insets = Insets.NONE;
      } 
      return this.mSwitchWidth - this.mThumbWidth - rect.left - rect.right - insets.left - insets.right;
    } 
    return 0;
  }
  
  protected int[] onCreateDrawableState(int paramInt) {
    int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
    if (isChecked())
      mergeDrawableStates(arrayOfInt, CHECKED_STATE_SET); 
    return arrayOfInt;
  }
  
  protected void drawableStateChanged() {
    boolean bool;
    super.drawableStateChanged();
    int[] arrayOfInt = getDrawableState();
    int i = 0;
    Drawable drawable = this.mThumbDrawable;
    int j = i;
    if (drawable != null) {
      j = i;
      if (drawable.isStateful())
        j = false | drawable.setState(arrayOfInt); 
    } 
    drawable = this.mTrackDrawable;
    i = j;
    if (drawable != null) {
      i = j;
      if (drawable.isStateful())
        bool = j | drawable.setState(arrayOfInt); 
    } 
    if (bool)
      invalidate(); 
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Drawable drawable = this.mThumbDrawable;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
    drawable = this.mTrackDrawable;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (super.verifyDrawable(paramDrawable) || paramDrawable == this.mThumbDrawable || paramDrawable == this.mTrackDrawable);
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mThumbDrawable;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
    drawable = this.mTrackDrawable;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
    ObjectAnimator objectAnimator = this.mPositionAnimator;
    if (objectAnimator != null && objectAnimator.isStarted()) {
      this.mPositionAnimator.end();
      this.mPositionAnimator = null;
    } 
  }
  
  public CharSequence getAccessibilityClassName() {
    return Switch.class.getName();
  }
  
  protected void onProvideStructure(ViewStructure paramViewStructure, int paramInt1, int paramInt2) {
    CharSequence charSequence;
    if (isChecked()) {
      charSequence = this.mTextOn;
    } else {
      charSequence = this.mTextOff;
    } 
    if (!TextUtils.isEmpty(charSequence)) {
      CharSequence charSequence1 = paramViewStructure.getText();
      if (TextUtils.isEmpty(charSequence1)) {
        paramViewStructure.setText(charSequence);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(charSequence1);
        stringBuilder.append(' ');
        stringBuilder.append(charSequence);
        paramViewStructure.setText(stringBuilder);
      } 
    } 
  }
  
  private static final FloatProperty<Switch> THUMB_POS = (FloatProperty<Switch>)new Object("thumbPos");
  
  private static final int TOUCH_MODE_DOWN = 1;
  
  private static final int TOUCH_MODE_DRAGGING = 2;
  
  private static final int TOUCH_MODE_IDLE = 0;
  
  private boolean mHasThumbTint;
  
  private boolean mHasThumbTintMode;
  
  private boolean mHasTrackTint;
  
  private boolean mHasTrackTintMode;
  
  private int mMinFlingVelocity;
  
  private Layout mOffLayout;
  
  private Layout mOnLayout;
  
  private ObjectAnimator mPositionAnimator;
  
  private boolean mShowText;
  
  private boolean mSplitTrack;
  
  private int mSwitchBottom;
  
  private int mSwitchHeight;
  
  private int mSwitchLeft;
  
  private int mSwitchMinWidth;
  
  private int mSwitchPadding;
  
  private int mSwitchRight;
  
  private int mSwitchTop;
  
  private TransformationMethod2 mSwitchTransformationMethod;
  
  private int mSwitchWidth;
  
  private final Rect mTempRect;
  
  private ColorStateList mTextColors;
  
  private CharSequence mTextOff;
  
  private CharSequence mTextOn;
  
  private TextPaint mTextPaint;
  
  private BlendMode mThumbBlendMode;
  
  private Drawable mThumbDrawable;
  
  private float mThumbPosition;
  
  private int mThumbTextPadding;
  
  private ColorStateList mThumbTintList;
  
  private int mThumbWidth;
  
  private int mTouchMode;
  
  private int mTouchSlop;
  
  private float mTouchX;
  
  private float mTouchY;
  
  private BlendMode mTrackBlendMode;
  
  private Drawable mTrackDrawable;
  
  private ColorStateList mTrackTintList;
  
  private boolean mUseFallbackLineSpacing;
  
  private VelocityTracker mVelocityTracker;
}
