package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;

public class AutoCompleteTextView extends EditText implements Filter.FilterListener {
  static final boolean DEBUG = false;
  
  static final int EXPAND_MAX = 3;
  
  static final String TAG = "AutoCompleteTextView";
  
  private ListAdapter mAdapter;
  
  private MyWatcher mAutoCompleteTextWatcher;
  
  private boolean mBlockCompletion;
  
  private int mDropDownAnchorId;
  
  private boolean mDropDownDismissedOnCompletion;
  
  private Filter mFilter;
  
  private int mHintResource;
  
  private CharSequence mHintText;
  
  private TextView mHintView;
  
  private AdapterView.OnItemClickListener mItemClickListener;
  
  private AdapterView.OnItemSelectedListener mItemSelectedListener;
  
  private int mLastKeyCode;
  
  private PopupDataSetObserver mObserver;
  
  private final PassThroughClickListener mPassThroughClickListener;
  
  private final ListPopupWindow mPopup;
  
  private boolean mPopupCanBeUpdated;
  
  private final Context mPopupContext;
  
  private int mThreshold;
  
  private Validator mValidator;
  
  public AutoCompleteTextView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public AutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842859);
  }
  
  public AutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    this(paramContext, paramAttributeSet, paramInt1, paramInt2, (Resources.Theme)null);
  }
  
  public AutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2, Resources.Theme paramTheme) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray1;
    this.mDropDownDismissedOnCompletion = true;
    this.mLastKeyCode = 0;
    this.mValidator = null;
    this.mPopupCanBeUpdated = true;
    TypedArray typedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AutoCompleteTextView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.AutoCompleteTextView, paramAttributeSet, typedArray2, paramInt1, paramInt2);
    if (paramTheme != null) {
      this.mPopupContext = (Context)new ContextThemeWrapper(paramContext, paramTheme);
    } else {
      int m = typedArray2.getResourceId(8, 0);
      if (m != 0) {
        this.mPopupContext = (Context)new ContextThemeWrapper(paramContext, m);
      } else {
        this.mPopupContext = paramContext;
      } 
    } 
    Context context = this.mPopupContext;
    if (context != paramContext) {
      TypedArray typedArray = context.obtainStyledAttributes(paramAttributeSet, R.styleable.AutoCompleteTextView, paramInt1, paramInt2);
      saveAttributeDataForStyleable(paramContext, R.styleable.AutoCompleteTextView, paramAttributeSet, typedArray2, paramInt1, paramInt2);
      typedArray1 = typedArray;
    } else {
      typedArray1 = typedArray2;
    } 
    Drawable drawable = typedArray1.getDrawable(3);
    int j = typedArray1.getLayoutDimension(5, -2);
    int k = typedArray1.getLayoutDimension(7, -2);
    int i = typedArray1.getResourceId(1, 17367303);
    CharSequence charSequence = typedArray1.getText(0);
    if (typedArray1 != typedArray2)
      typedArray1.recycle(); 
    ListPopupWindow listPopupWindow = new ListPopupWindow(this.mPopupContext, paramAttributeSet, paramInt1, paramInt2);
    listPopupWindow.setSoftInputMode(16);
    this.mPopup.setPromptPosition(1);
    this.mPopup.setListSelector(drawable);
    this.mPopup.setOnItemClickListener(new DropDownItemClickListener());
    this.mPopup.setWidth(j);
    this.mPopup.setHeight(k);
    this.mHintResource = i;
    setCompletionHint(charSequence);
    this.mDropDownAnchorId = typedArray2.getResourceId(6, -1);
    this.mThreshold = typedArray2.getInt(2, 2);
    typedArray2.recycle();
    paramInt1 = getInputType();
    if ((paramInt1 & 0xF) == 1)
      setRawInputType(paramInt1 | 0x10000); 
    setFocusable(true);
    MyWatcher myWatcher = new MyWatcher();
    addTextChangedListener(myWatcher);
    PassThroughClickListener passThroughClickListener = new PassThroughClickListener();
    super.setOnClickListener(passThroughClickListener);
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener) {
    PassThroughClickListener.access$302(this.mPassThroughClickListener, paramOnClickListener);
  }
  
  private void onClickImpl() {
    if (isPopupShowing())
      ensureImeVisible(true); 
  }
  
  public void setCompletionHint(CharSequence paramCharSequence) {
    this.mHintText = paramCharSequence;
    if (paramCharSequence != null) {
      View view;
      TextView textView = this.mHintView;
      if (textView == null) {
        view = LayoutInflater.from(this.mPopupContext).inflate(this.mHintResource, (ViewGroup)null);
        view = view.<TextView>findViewById(16908308);
        view.setText(this.mHintText);
        this.mHintView = (TextView)view;
        this.mPopup.setPromptView(view);
      } else {
        textView.setText((CharSequence)view);
      } 
    } else {
      this.mPopup.setPromptView(null);
      this.mHintView = null;
    } 
  }
  
  public CharSequence getCompletionHint() {
    return this.mHintText;
  }
  
  public int getDropDownWidth() {
    return this.mPopup.getWidth();
  }
  
  public void setDropDownWidth(int paramInt) {
    this.mPopup.setWidth(paramInt);
  }
  
  public int getDropDownHeight() {
    return this.mPopup.getHeight();
  }
  
  public void setDropDownHeight(int paramInt) {
    this.mPopup.setHeight(paramInt);
  }
  
  public int getDropDownAnchor() {
    return this.mDropDownAnchorId;
  }
  
  public void setDropDownAnchor(int paramInt) {
    this.mDropDownAnchorId = paramInt;
    this.mPopup.setAnchorView(null);
  }
  
  public Drawable getDropDownBackground() {
    return this.mPopup.getBackground();
  }
  
  public void setDropDownBackgroundDrawable(Drawable paramDrawable) {
    this.mPopup.setBackgroundDrawable(paramDrawable);
  }
  
  public void setDropDownBackgroundResource(int paramInt) {
    this.mPopup.setBackgroundDrawable(getContext().getDrawable(paramInt));
  }
  
  public void setDropDownVerticalOffset(int paramInt) {
    this.mPopup.setVerticalOffset(paramInt);
  }
  
  public int getDropDownVerticalOffset() {
    return this.mPopup.getVerticalOffset();
  }
  
  public void setDropDownHorizontalOffset(int paramInt) {
    this.mPopup.setHorizontalOffset(paramInt);
  }
  
  public int getDropDownHorizontalOffset() {
    return this.mPopup.getHorizontalOffset();
  }
  
  public void setDropDownAnimationStyle(int paramInt) {
    this.mPopup.setAnimationStyle(paramInt);
  }
  
  public int getDropDownAnimationStyle() {
    return this.mPopup.getAnimationStyle();
  }
  
  public boolean isDropDownAlwaysVisible() {
    return this.mPopup.isDropDownAlwaysVisible();
  }
  
  public void setDropDownAlwaysVisible(boolean paramBoolean) {
    this.mPopup.setDropDownAlwaysVisible(paramBoolean);
  }
  
  public boolean isDropDownDismissedOnCompletion() {
    return this.mDropDownDismissedOnCompletion;
  }
  
  public void setDropDownDismissedOnCompletion(boolean paramBoolean) {
    this.mDropDownDismissedOnCompletion = paramBoolean;
  }
  
  public int getThreshold() {
    return this.mThreshold;
  }
  
  public void setThreshold(int paramInt) {
    int i = paramInt;
    if (paramInt <= 0)
      i = 1; 
    this.mThreshold = i;
  }
  
  public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener) {
    this.mItemClickListener = paramOnItemClickListener;
  }
  
  public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener paramOnItemSelectedListener) {
    this.mItemSelectedListener = paramOnItemSelectedListener;
  }
  
  @Deprecated
  public AdapterView.OnItemClickListener getItemClickListener() {
    return this.mItemClickListener;
  }
  
  @Deprecated
  public AdapterView.OnItemSelectedListener getItemSelectedListener() {
    return this.mItemSelectedListener;
  }
  
  public AdapterView.OnItemClickListener getOnItemClickListener() {
    return this.mItemClickListener;
  }
  
  public AdapterView.OnItemSelectedListener getOnItemSelectedListener() {
    return this.mItemSelectedListener;
  }
  
  public void setOnDismissListener(OnDismissListener paramOnDismissListener) {
    Object object = null;
    if (paramOnDismissListener != null)
      object = new Object(this, paramOnDismissListener); 
    this.mPopup.setOnDismissListener((PopupWindow.OnDismissListener)object);
  }
  
  public ListAdapter getAdapter() {
    return this.mAdapter;
  }
  
  public <T extends ListAdapter & Filterable> void setAdapter(T paramT) {
    PopupDataSetObserver popupDataSetObserver = this.mObserver;
    if (popupDataSetObserver == null) {
      this.mObserver = new PopupDataSetObserver();
    } else {
      ListAdapter listAdapter = this.mAdapter;
      if (listAdapter != null)
        listAdapter.unregisterDataSetObserver(popupDataSetObserver); 
    } 
    this.mAdapter = (ListAdapter)paramT;
    if (paramT != null) {
      this.mFilter = ((Filterable)paramT).getFilter();
      paramT.registerDataSetObserver(this.mObserver);
    } else {
      this.mFilter = null;
    } 
    this.mPopup.setAdapter(this.mAdapter);
  }
  
  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent) {
    if (paramInt == 4 && isPopupShowing()) {
      ListPopupWindow listPopupWindow = this.mPopup;
      if (!listPopupWindow.isDropDownAlwaysVisible()) {
        if (paramKeyEvent.getAction() == 0 && paramKeyEvent.getRepeatCount() == 0) {
          KeyEvent.DispatcherState dispatcherState = getKeyDispatcherState();
          if (dispatcherState != null)
            dispatcherState.startTracking(paramKeyEvent, this); 
          return true;
        } 
        if (paramKeyEvent.getAction() == 1) {
          KeyEvent.DispatcherState dispatcherState = getKeyDispatcherState();
          if (dispatcherState != null)
            dispatcherState.handleUpEvent(paramKeyEvent); 
          if (paramKeyEvent.isTracking() && !paramKeyEvent.isCanceled()) {
            dismissDropDown();
            return true;
          } 
        } 
      } 
    } 
    return super.onKeyPreIme(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool = this.mPopup.onKeyUp(paramInt, paramKeyEvent);
    if (!bool || (
      paramInt != 23 && paramInt != 61 && paramInt != 66 && paramInt != 160)) {
      if (isPopupShowing() && paramInt == 61 && paramKeyEvent.hasNoModifiers()) {
        performCompletion();
        return true;
      } 
      return super.onKeyUp(paramInt, paramKeyEvent);
    } 
    if (paramKeyEvent.hasNoModifiers())
      performCompletion(); 
    return true;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (this.mPopup.onKeyDown(paramInt, paramKeyEvent))
      return true; 
    if (!isPopupShowing() && 
      paramInt == 20)
      if (paramKeyEvent.hasNoModifiers())
        performValidation();  
    if (isPopupShowing() && paramInt == 61 && paramKeyEvent.hasNoModifiers())
      return true; 
    this.mLastKeyCode = paramInt;
    boolean bool = super.onKeyDown(paramInt, paramKeyEvent);
    this.mLastKeyCode = 0;
    if (bool && isPopupShowing())
      clearListSelection(); 
    return bool;
  }
  
  public boolean enoughToFilter() {
    boolean bool;
    if (getText().length() >= this.mThreshold) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class MyWatcher implements TextWatcher {
    private boolean mOpenBefore;
    
    final AutoCompleteTextView this$0;
    
    private MyWatcher() {}
    
    public void beforeTextChanged(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3) {
      if (AutoCompleteTextView.this.mBlockCompletion)
        return; 
      this.mOpenBefore = AutoCompleteTextView.this.isPopupShowing();
    }
    
    public void afterTextChanged(Editable param1Editable) {
      if (AutoCompleteTextView.this.mBlockCompletion)
        return; 
      if (this.mOpenBefore && !AutoCompleteTextView.this.isPopupShowing())
        return; 
      AutoCompleteTextView.this.refreshAutoCompleteResults();
    }
    
    public void onTextChanged(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3) {}
  }
  
  void doBeforeTextChanged() {
    this.mAutoCompleteTextWatcher.beforeTextChanged(null, 0, 0, 0);
  }
  
  void doAfterTextChanged() {
    this.mAutoCompleteTextWatcher.afterTextChanged(null);
  }
  
  public final void refreshAutoCompleteResults() {
    if (enoughToFilter()) {
      if (this.mFilter != null) {
        this.mPopupCanBeUpdated = true;
        performFiltering(getText(), this.mLastKeyCode);
      } 
    } else {
      if (!this.mPopup.isDropDownAlwaysVisible())
        dismissDropDown(); 
      Filter filter = this.mFilter;
      if (filter != null)
        filter.filter(null); 
    } 
  }
  
  public boolean isPopupShowing() {
    return this.mPopup.isShowing();
  }
  
  protected CharSequence convertSelectionToString(Object paramObject) {
    return this.mFilter.convertResultToString(paramObject);
  }
  
  public void clearListSelection() {
    this.mPopup.clearListSelection();
  }
  
  public void setListSelection(int paramInt) {
    this.mPopup.setSelection(paramInt);
  }
  
  public int getListSelection() {
    return this.mPopup.getSelectedItemPosition();
  }
  
  protected void performFiltering(CharSequence paramCharSequence, int paramInt) {
    this.mFilter.filter(paramCharSequence, this);
  }
  
  public void performCompletion() {
    performCompletion((View)null, -1, -1L);
  }
  
  public void onCommitCompletion(CompletionInfo paramCompletionInfo) {
    if (isPopupShowing())
      this.mPopup.performItemClick(paramCompletionInfo.getPosition()); 
  }
  
  private void performCompletion(View paramView, int paramInt, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isPopupShowing : ()Z
    //   4: ifeq -> 131
    //   7: iload_2
    //   8: ifge -> 23
    //   11: aload_0
    //   12: getfield mPopup : Landroid/widget/ListPopupWindow;
    //   15: invokevirtual getSelectedItem : ()Ljava/lang/Object;
    //   18: astore #5
    //   20: goto -> 35
    //   23: aload_0
    //   24: getfield mAdapter : Landroid/widget/ListAdapter;
    //   27: iload_2
    //   28: invokeinterface getItem : (I)Ljava/lang/Object;
    //   33: astore #5
    //   35: aload #5
    //   37: ifnonnull -> 50
    //   40: ldc 'AutoCompleteTextView'
    //   42: ldc_w 'performCompletion: no selected item'
    //   45: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   48: pop
    //   49: return
    //   50: aload_0
    //   51: iconst_1
    //   52: putfield mBlockCompletion : Z
    //   55: aload_0
    //   56: aload_0
    //   57: aload #5
    //   59: invokevirtual convertSelectionToString : (Ljava/lang/Object;)Ljava/lang/CharSequence;
    //   62: invokevirtual replaceText : (Ljava/lang/CharSequence;)V
    //   65: aload_0
    //   66: iconst_0
    //   67: putfield mBlockCompletion : Z
    //   70: aload_0
    //   71: getfield mItemClickListener : Landroid/widget/AdapterView$OnItemClickListener;
    //   74: ifnull -> 131
    //   77: aload_0
    //   78: getfield mPopup : Landroid/widget/ListPopupWindow;
    //   81: astore #5
    //   83: aload_1
    //   84: ifnull -> 94
    //   87: iload_2
    //   88: istore #6
    //   90: iload_2
    //   91: ifge -> 113
    //   94: aload #5
    //   96: invokevirtual getSelectedView : ()Landroid/view/View;
    //   99: astore_1
    //   100: aload #5
    //   102: invokevirtual getSelectedItemPosition : ()I
    //   105: istore #6
    //   107: aload #5
    //   109: invokevirtual getSelectedItemId : ()J
    //   112: lstore_3
    //   113: aload_0
    //   114: getfield mItemClickListener : Landroid/widget/AdapterView$OnItemClickListener;
    //   117: aload #5
    //   119: invokevirtual getListView : ()Landroid/widget/ListView;
    //   122: aload_1
    //   123: iload #6
    //   125: lload_3
    //   126: invokeinterface onItemClick : (Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    //   131: aload_0
    //   132: getfield mDropDownDismissedOnCompletion : Z
    //   135: ifeq -> 152
    //   138: aload_0
    //   139: getfield mPopup : Landroid/widget/ListPopupWindow;
    //   142: invokevirtual isDropDownAlwaysVisible : ()Z
    //   145: ifne -> 152
    //   148: aload_0
    //   149: invokevirtual dismissDropDown : ()V
    //   152: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1047	-> 0
    //   #1049	-> 7
    //   #1050	-> 11
    //   #1052	-> 23
    //   #1054	-> 35
    //   #1055	-> 40
    //   #1056	-> 49
    //   #1059	-> 50
    //   #1060	-> 55
    //   #1061	-> 65
    //   #1063	-> 70
    //   #1064	-> 77
    //   #1066	-> 83
    //   #1067	-> 94
    //   #1068	-> 100
    //   #1069	-> 107
    //   #1071	-> 113
    //   #1075	-> 131
    //   #1076	-> 148
    //   #1078	-> 152
  }
  
  public boolean isPerformingCompletion() {
    return this.mBlockCompletion;
  }
  
  public void setText(CharSequence paramCharSequence, boolean paramBoolean) {
    if (paramBoolean) {
      setText(paramCharSequence);
    } else {
      this.mBlockCompletion = true;
      setText(paramCharSequence);
      this.mBlockCompletion = false;
    } 
  }
  
  protected void replaceText(CharSequence paramCharSequence) {
    clearComposingText();
    setText(paramCharSequence);
    paramCharSequence = getText();
    Selection.setSelection((Spannable)paramCharSequence, paramCharSequence.length());
  }
  
  public void onFilterComplete(int paramInt) {
    updateDropDownForFilter(paramInt);
  }
  
  private void updateDropDownForFilter(int paramInt) {
    if (getWindowVisibility() == 8)
      return; 
    boolean bool1 = this.mPopup.isDropDownAlwaysVisible();
    boolean bool2 = enoughToFilter();
    if ((paramInt > 0 || bool1) && bool2) {
      if (hasFocus() && hasWindowFocus() && this.mPopupCanBeUpdated)
        showDropDown(); 
    } else if (!bool1 && isPopupShowing()) {
      dismissDropDown();
      this.mPopupCanBeUpdated = true;
    } 
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    super.onWindowFocusChanged(paramBoolean);
    if (!paramBoolean && !this.mPopup.isDropDownAlwaysVisible())
      dismissDropDown(); 
  }
  
  protected void onDisplayHint(int paramInt) {
    super.onDisplayHint(paramInt);
    if (paramInt == 4)
      if (!this.mPopup.isDropDownAlwaysVisible())
        dismissDropDown();  
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    if (isTemporarilyDetached())
      return; 
    if (!paramBoolean)
      performValidation(); 
    if (!paramBoolean && !this.mPopup.isDropDownAlwaysVisible())
      dismissDropDown(); 
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
  }
  
  protected void onDetachedFromWindow() {
    dismissDropDown();
    super.onDetachedFromWindow();
  }
  
  public void dismissDropDown() {
    InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
    if (inputMethodManager != null)
      inputMethodManager.displayCompletions(this, null); 
    this.mPopup.dismiss();
    this.mPopupCanBeUpdated = false;
  }
  
  protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
    if (isPopupShowing())
      showDropDown(); 
    return bool;
  }
  
  public void showDropDownAfterLayout() {
    this.mPopup.postShow();
  }
  
  public void ensureImeVisible(boolean paramBoolean) {
    byte b;
    ListPopupWindow listPopupWindow = this.mPopup;
    if (paramBoolean) {
      b = 1;
    } else {
      b = 2;
    } 
    listPopupWindow.setInputMethodMode(b);
    if (this.mPopup.isDropDownAlwaysVisible() || (this.mFilter != null && enoughToFilter()))
      showDropDown(); 
  }
  
  public boolean isInputMethodNotNeeded() {
    boolean bool;
    if (this.mPopup.getInputMethodMode() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getInputMethodMode() {
    return this.mPopup.getInputMethodMode();
  }
  
  public void setInputMethodMode(int paramInt) {
    this.mPopup.setInputMethodMode(paramInt);
  }
  
  public void showDropDown() {
    buildImeCompletions();
    if (this.mPopup.getAnchorView() == null)
      if (this.mDropDownAnchorId != -1) {
        this.mPopup.setAnchorView(getRootView().findViewById(this.mDropDownAnchorId));
      } else {
        this.mPopup.setAnchorView(this);
      }  
    if (!isPopupShowing()) {
      this.mPopup.setInputMethodMode(1);
      this.mPopup.setListItemExpandMax(3);
    } 
    this.mPopup.show();
    this.mPopup.getListView().setOverScrollMode(0);
  }
  
  public void setForceIgnoreOutsideTouch(boolean paramBoolean) {
    this.mPopup.setForceIgnoreOutsideTouch(paramBoolean);
  }
  
  private void buildImeCompletions() {
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter != null) {
      InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
      if (inputMethodManager != null) {
        int i = Math.min(listAdapter.getCount(), 20);
        CompletionInfo[] arrayOfCompletionInfo2 = new CompletionInfo[i];
        int j = 0;
        for (byte b = 0; b < i; b++, j = k) {
          int k = j;
          if (listAdapter.isEnabled(b)) {
            Object object = listAdapter.getItem(b);
            long l = listAdapter.getItemId(b);
            arrayOfCompletionInfo2[j] = new CompletionInfo(l, j, convertSelectionToString(object));
            k = j + 1;
          } 
        } 
        CompletionInfo[] arrayOfCompletionInfo1 = arrayOfCompletionInfo2;
        if (j != i) {
          arrayOfCompletionInfo1 = new CompletionInfo[j];
          System.arraycopy(arrayOfCompletionInfo2, 0, arrayOfCompletionInfo1, 0, j);
        } 
        inputMethodManager.displayCompletions(this, arrayOfCompletionInfo1);
      } 
    } 
  }
  
  public void setValidator(Validator paramValidator) {
    this.mValidator = paramValidator;
  }
  
  public Validator getValidator() {
    return this.mValidator;
  }
  
  public void performValidation() {
    if (this.mValidator == null)
      return; 
    Editable editable = getText();
    if (!TextUtils.isEmpty(editable) && !this.mValidator.isValid(editable))
      setText(this.mValidator.fixText(editable)); 
  }
  
  protected Filter getFilter() {
    return this.mFilter;
  }
  
  class DropDownItemClickListener implements AdapterView.OnItemClickListener {
    final AutoCompleteTextView this$0;
    
    private DropDownItemClickListener() {}
    
    public void onItemClick(AdapterView param1AdapterView, View param1View, int param1Int, long param1Long) {
      AutoCompleteTextView.this.performCompletion(param1View, param1Int, param1Long);
    }
  }
  
  class PassThroughClickListener implements View.OnClickListener {
    private View.OnClickListener mWrapped;
    
    final AutoCompleteTextView this$0;
    
    private PassThroughClickListener() {}
    
    public void onClick(View param1View) {
      AutoCompleteTextView.this.onClickImpl();
      View.OnClickListener onClickListener = this.mWrapped;
      if (onClickListener != null)
        onClickListener.onClick(param1View); 
    }
  }
  
  class PopupDataSetObserver extends DataSetObserver {
    private final WeakReference<AutoCompleteTextView> mViewReference;
    
    private final Runnable updateRunnable;
    
    private PopupDataSetObserver(AutoCompleteTextView this$0) {
      this.updateRunnable = (Runnable)new Object(this);
      this.mViewReference = new WeakReference<>(AutoCompleteTextView.this);
    }
    
    public void onChanged() {
      AutoCompleteTextView autoCompleteTextView = this.mViewReference.get();
      if (autoCompleteTextView != null && autoCompleteTextView.mAdapter != null)
        autoCompleteTextView.post(this.updateRunnable); 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class InputMethodMode implements Annotation {}
  
  class OnDismissListener {
    public abstract void onDismiss();
  }
  
  class Validator {
    public abstract CharSequence fixText(CharSequence param1CharSequence);
    
    public abstract boolean isValid(CharSequence param1CharSequence);
  }
}
