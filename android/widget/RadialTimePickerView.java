package android.widget;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.FloatProperty;
import android.util.IntArray;
import android.util.Log;
import android.util.MathUtils;
import android.util.StateSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R;
import com.android.internal.widget.ExploreByTouchHelper;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Calendar;
import java.util.Locale;

public class RadialTimePickerView extends View {
  private static final int[] HOURS_NUMBERS = new int[] { 
      12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
      10, 11 };
  
  private static final int[] HOURS_NUMBERS_24 = new int[] { 
      0, 13, 14, 15, 16, 17, 18, 19, 20, 21, 
      22, 23 };
  
  private static final int[] MINUTES_NUMBERS = new int[] { 
      0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 
      50, 55 };
  
  private static final int[] SNAP_PREFER_30S_MAP = new int[361];
  
  static {
    COS_30 = new float[12];
    SIN_30 = new float[12];
    preparePrefer30sMap();
    double d = 1.5707963267948966D;
    for (byte b = 0; b < 12; b++) {
      COS_30[b] = (float)Math.cos(d);
      SIN_30[b] = (float)Math.sin(d);
      d += 0.5235987755982988D;
    } 
  }
  
  private final FloatProperty<RadialTimePickerView> HOURS_TO_MINUTES = (FloatProperty<RadialTimePickerView>)new Object(this, "hoursToMinutes");
  
  private final String[] mHours12Texts = new String[12];
  
  private final String[] mOuterHours24Texts = new String[12];
  
  private final String[] mInnerHours24Texts = new String[12];
  
  private final String[] mMinutesTexts = new String[12];
  
  private final Paint[] mPaint = new Paint[2];
  
  private final Paint mPaintCenter = new Paint();
  
  private final Paint[] mPaintSelector = new Paint[3];
  
  private final Paint mPaintBackground = new Paint();
  
  private final ColorStateList[] mTextColor = new ColorStateList[3];
  
  private final int[] mTextSize = new int[3];
  
  private final int[] mTextInset = new int[3];
  
  private final float[][] mOuterTextX = new float[2][12];
  
  private final float[][] mOuterTextY = new float[2][12];
  
  private final float[] mInnerTextX = new float[12];
  
  private final float[] mInnerTextY = new float[12];
  
  private final int[] mSelectionDegrees = new int[2];
  
  private final Path mSelectorPath = new Path();
  
  private boolean mInputEnabled = true;
  
  private static final int AM = 0;
  
  private static final int ANIM_DURATION_NORMAL = 500;
  
  private static final int ANIM_DURATION_TOUCH = 60;
  
  private static final float[] COS_30;
  
  private static final int DEGREES_FOR_ONE_HOUR = 30;
  
  private static final int DEGREES_FOR_ONE_MINUTE = 6;
  
  public static final int HOURS = 0;
  
  private static final int HOURS_INNER = 2;
  
  private static final int HOURS_IN_CIRCLE = 12;
  
  public static final int MINUTES = 1;
  
  private static final int MINUTES_IN_CIRCLE = 60;
  
  private static final int MISSING_COLOR = -65281;
  
  private static final int NUM_POSITIONS = 12;
  
  private static final int PM = 1;
  
  private static final int SELECTOR_CIRCLE = 0;
  
  private static final int SELECTOR_DOT = 1;
  
  private static final int SELECTOR_LINE = 2;
  
  private static final float[] SIN_30;
  
  private static final String TAG = "RadialTimePickerView";
  
  private int mAmOrPm;
  
  private int mCenterDotRadius;
  
  boolean mChangedDuringTouch;
  
  private int mCircleRadius;
  
  private float mDisabledAlpha;
  
  private int mHalfwayDist;
  
  private float mHoursToMinutes;
  
  private ObjectAnimator mHoursToMinutesAnimator;
  
  private String[] mInnerTextHours;
  
  private boolean mIs24HourMode;
  
  private boolean mIsOnInnerCircle;
  
  private OnValueSelectedListener mListener;
  
  private int mMaxDistForOuterNumber;
  
  private int mMinDistForInnerNumber;
  
  private String[] mMinutesText;
  
  private String[] mOuterTextHours;
  
  private int mSelectorColor;
  
  private int mSelectorDotColor;
  
  private int mSelectorDotRadius;
  
  private int mSelectorRadius;
  
  private int mSelectorStroke;
  
  private boolean mShowHours;
  
  private final RadialPickerTouchHelper mTouchHelper;
  
  private final Typeface mTypeface;
  
  private int mXCenter;
  
  private int mYCenter;
  
  private static void preparePrefer30sMap() {
    int i = 0;
    int j = 1;
    int k = 8;
    for (byte b = 0; b < 'ũ'; b++, i = j, j = k, k = m) {
      int m;
      SNAP_PREFER_30S_MAP[b] = i;
      if (j == k) {
        j = i + 6;
        if (j == 360) {
          i = 7;
        } else if (j % 30 == 0) {
          i = 14;
        } else {
          i = 4;
        } 
        k = 1;
        m = i;
      } else {
        j++;
        m = k;
        k = j;
        j = i;
      } 
    } 
  }
  
  private static int snapPrefer30s(int paramInt) {
    int[] arrayOfInt = SNAP_PREFER_30S_MAP;
    if (arrayOfInt == null)
      return -1; 
    return arrayOfInt[paramInt];
  }
  
  private static int snapOnly30s(int paramInt1, int paramInt2) {
    int i = paramInt1 / 30 * 30;
    int j = i + 30;
    if (paramInt2 == 1) {
      paramInt1 = j;
    } else if (paramInt2 == -1) {
      paramInt2 = i;
      if (paramInt1 == i)
        paramInt2 = i - 30; 
      paramInt1 = paramInt2;
    } else if (paramInt1 - i < j - paramInt1) {
      paramInt1 = i;
    } else {
      paramInt1 = j;
    } 
    return paramInt1;
  }
  
  public RadialTimePickerView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public RadialTimePickerView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843933);
  }
  
  public RadialTimePickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public RadialTimePickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet);
    this.mChangedDuringTouch = false;
    applyAttributes(paramAttributeSet, paramInt1, paramInt2);
    TypedValue typedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(16842803, typedValue, true);
    this.mDisabledAlpha = typedValue.getFloat();
    this.mTypeface = Typeface.create("sans-serif", 0);
    this.mPaint[0] = new Paint();
    this.mPaint[0].setAntiAlias(true);
    this.mPaint[0].setTextAlign(Paint.Align.CENTER);
    this.mPaint[1] = new Paint();
    this.mPaint[1].setAntiAlias(true);
    this.mPaint[1].setTextAlign(Paint.Align.CENTER);
    this.mPaintCenter.setAntiAlias(true);
    this.mPaintSelector[0] = new Paint();
    this.mPaintSelector[0].setAntiAlias(true);
    this.mPaintSelector[1] = new Paint();
    this.mPaintSelector[1].setAntiAlias(true);
    this.mPaintSelector[2] = new Paint();
    this.mPaintSelector[2].setAntiAlias(true);
    this.mPaintSelector[2].setStrokeWidth(2.0F);
    this.mPaintBackground.setAntiAlias(true);
    Resources resources = getResources();
    this.mSelectorRadius = resources.getDimensionPixelSize(17105528);
    this.mSelectorStroke = resources.getDimensionPixelSize(17105529);
    this.mSelectorDotRadius = resources.getDimensionPixelSize(17105527);
    this.mCenterDotRadius = resources.getDimensionPixelSize(17105519);
    this.mTextSize[0] = resources.getDimensionPixelSize(17105534);
    this.mTextSize[1] = resources.getDimensionPixelSize(17105534);
    this.mTextSize[2] = resources.getDimensionPixelSize(17105533);
    this.mTextInset[0] = resources.getDimensionPixelSize(17105532);
    this.mTextInset[1] = resources.getDimensionPixelSize(17105532);
    this.mTextInset[2] = resources.getDimensionPixelSize(17105531);
    this.mShowHours = true;
    this.mHoursToMinutes = 0.0F;
    this.mIs24HourMode = false;
    this.mAmOrPm = 0;
    RadialPickerTouchHelper radialPickerTouchHelper = new RadialPickerTouchHelper();
    setAccessibilityDelegate((View.AccessibilityDelegate)radialPickerTouchHelper);
    if (getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
    initHoursAndMinutesText();
    initData();
    Calendar calendar = Calendar.getInstance(Locale.getDefault());
    paramInt1 = calendar.get(11);
    paramInt2 = calendar.get(12);
    setCurrentHourInternal(paramInt1, false, false);
    setCurrentMinuteInternal(paramInt2, false);
    setHapticFeedbackEnabled(true);
  }
  
  void applyAttributes(AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    Context context = getContext();
    TypedArray typedArray = getContext().obtainStyledAttributes(paramAttributeSet, R.styleable.TimePicker, paramInt1, paramInt2);
    saveAttributeDataForStyleable(context, R.styleable.TimePicker, paramAttributeSet, typedArray, paramInt1, paramInt2);
    ColorStateList colorStateList1 = typedArray.getColorStateList(3);
    ColorStateList colorStateList2 = typedArray.getColorStateList(9);
    ColorStateList[] arrayOfColorStateList2 = this.mTextColor;
    if (colorStateList1 == null)
      colorStateList1 = ColorStateList.valueOf(-65281); 
    arrayOfColorStateList2[0] = colorStateList1;
    arrayOfColorStateList2 = this.mTextColor;
    if (colorStateList2 == null) {
      colorStateList1 = ColorStateList.valueOf(-65281);
    } else {
      colorStateList1 = colorStateList2;
    } 
    arrayOfColorStateList2[2] = colorStateList1;
    ColorStateList[] arrayOfColorStateList1 = this.mTextColor;
    arrayOfColorStateList1[1] = arrayOfColorStateList1[0];
    colorStateList2 = typedArray.getColorStateList(5);
    if (colorStateList2 != null) {
      int[] arrayOfInt1 = StateSet.get(40);
      paramInt1 = colorStateList2.getColorForState(arrayOfInt1, 0);
    } else {
      paramInt1 = -65281;
    } 
    this.mPaintCenter.setColor(paramInt1);
    int[] arrayOfInt = StateSet.get(40);
    this.mSelectorColor = paramInt1;
    this.mSelectorDotColor = this.mTextColor[0].getColorForState(arrayOfInt, 0);
    Paint paint = this.mPaintBackground;
    paramInt1 = context.getColor(17171007);
    paint.setColor(typedArray.getColor(4, paramInt1));
    typedArray.recycle();
  }
  
  public void initialize(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (this.mIs24HourMode != paramBoolean) {
      this.mIs24HourMode = paramBoolean;
      initData();
    } 
    setCurrentHourInternal(paramInt1, false, false);
    setCurrentMinuteInternal(paramInt2, false);
  }
  
  public void setCurrentItemShowing(int paramInt, boolean paramBoolean) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ClockView does not support showing item ");
        stringBuilder.append(paramInt);
        Log.e("RadialTimePickerView", stringBuilder.toString());
      } else {
        showMinutes(paramBoolean);
      } 
    } else {
      showHours(paramBoolean);
    } 
  }
  
  public int getCurrentItemShowing() {
    return this.mShowHours ^ true;
  }
  
  public void setOnValueSelectedListener(OnValueSelectedListener paramOnValueSelectedListener) {
    this.mListener = paramOnValueSelectedListener;
  }
  
  public void setCurrentHour(int paramInt) {
    setCurrentHourInternal(paramInt, true, false);
  }
  
  private void setCurrentHourInternal(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool;
    this.mSelectionDegrees[0] = paramInt % 12 * 30;
    if (paramInt == 0 || paramInt % 24 < 12) {
      bool = false;
    } else {
      bool = true;
    } 
    boolean bool1 = getInnerCircleForHour(paramInt);
    if (this.mAmOrPm != bool || this.mIsOnInnerCircle != bool1) {
      this.mAmOrPm = bool;
      this.mIsOnInnerCircle = bool1;
      initData();
      this.mTouchHelper.invalidateRoot();
    } 
    invalidate();
    if (paramBoolean1) {
      OnValueSelectedListener onValueSelectedListener = this.mListener;
      if (onValueSelectedListener != null)
        onValueSelectedListener.onValueSelected(0, paramInt, paramBoolean2); 
    } 
  }
  
  public int getCurrentHour() {
    return getHourForDegrees(this.mSelectionDegrees[0], this.mIsOnInnerCircle);
  }
  
  private int getHourForDegrees(int paramInt, boolean paramBoolean) {
    int i = paramInt / 30 % 12;
    if (this.mIs24HourMode) {
      if (!paramBoolean && i == 0) {
        paramInt = 12;
      } else {
        paramInt = i;
        if (paramBoolean) {
          paramInt = i;
          if (i != 0)
            paramInt = i + 12; 
        } 
      } 
    } else {
      paramInt = i;
      if (this.mAmOrPm == 1)
        paramInt = i + 12; 
    } 
    return paramInt;
  }
  
  private int getDegreesForHour(int paramInt) {
    int i;
    if (this.mIs24HourMode) {
      i = paramInt;
      if (paramInt >= 12)
        i = paramInt - 12; 
    } else {
      i = paramInt;
      if (paramInt == 12)
        i = 0; 
    } 
    return i * 30;
  }
  
  private boolean getInnerCircleForHour(int paramInt) {
    boolean bool;
    if (this.mIs24HourMode && (paramInt == 0 || paramInt > 12)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setCurrentMinute(int paramInt) {
    setCurrentMinuteInternal(paramInt, true);
  }
  
  private void setCurrentMinuteInternal(int paramInt, boolean paramBoolean) {
    this.mSelectionDegrees[1] = paramInt % 60 * 6;
    invalidate();
    if (paramBoolean) {
      OnValueSelectedListener onValueSelectedListener = this.mListener;
      if (onValueSelectedListener != null)
        onValueSelectedListener.onValueSelected(1, paramInt, false); 
    } 
  }
  
  public int getCurrentMinute() {
    return getMinuteForDegrees(this.mSelectionDegrees[1]);
  }
  
  private int getMinuteForDegrees(int paramInt) {
    return paramInt / 6;
  }
  
  private int getDegreesForMinute(int paramInt) {
    return paramInt * 6;
  }
  
  public boolean setAmOrPm(int paramInt) {
    if (this.mAmOrPm == paramInt || this.mIs24HourMode)
      return false; 
    this.mAmOrPm = paramInt;
    invalidate();
    this.mTouchHelper.invalidateRoot();
    return true;
  }
  
  public int getAmOrPm() {
    return this.mAmOrPm;
  }
  
  public void showHours(boolean paramBoolean) {
    showPicker(true, paramBoolean);
  }
  
  public void showMinutes(boolean paramBoolean) {
    showPicker(false, paramBoolean);
  }
  
  private void initHoursAndMinutesText() {
    for (byte b = 0; b < 12; b++) {
      this.mHours12Texts[b] = String.format("%d", new Object[] { Integer.valueOf(HOURS_NUMBERS[b]) });
      this.mInnerHours24Texts[b] = String.format("%02d", new Object[] { Integer.valueOf(HOURS_NUMBERS_24[b]) });
      this.mOuterHours24Texts[b] = String.format("%d", new Object[] { Integer.valueOf(HOURS_NUMBERS[b]) });
      this.mMinutesTexts[b] = String.format("%02d", new Object[] { Integer.valueOf(MINUTES_NUMBERS[b]) });
    } 
  }
  
  private void initData() {
    if (this.mIs24HourMode) {
      this.mOuterTextHours = this.mOuterHours24Texts;
      this.mInnerTextHours = this.mInnerHours24Texts;
    } else {
      String[] arrayOfString = this.mHours12Texts;
      this.mInnerTextHours = arrayOfString;
    } 
    this.mMinutesText = this.mMinutesTexts;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!paramBoolean)
      return; 
    this.mXCenter = getWidth() / 2;
    this.mYCenter = paramInt1 = getHeight() / 2;
    this.mCircleRadius = paramInt1 = Math.min(this.mXCenter, paramInt1);
    int[] arrayOfInt = this.mTextInset;
    paramInt2 = arrayOfInt[2];
    paramInt3 = this.mSelectorRadius;
    this.mMinDistForInnerNumber = paramInt1 - paramInt2 - paramInt3;
    this.mMaxDistForOuterNumber = paramInt1 - arrayOfInt[0] + paramInt3;
    this.mHalfwayDist = paramInt1 - (arrayOfInt[0] + arrayOfInt[2]) / 2;
    calculatePositionsHours();
    calculatePositionsMinutes();
    this.mTouchHelper.invalidateRoot();
  }
  
  public void onDraw(Canvas paramCanvas) {
    float f;
    if (this.mInputEnabled) {
      f = 1.0F;
    } else {
      f = this.mDisabledAlpha;
    } 
    drawCircleBackground(paramCanvas);
    Path path = this.mSelectorPath;
    drawSelector(paramCanvas, path);
    drawHours(paramCanvas, path, f);
    drawMinutes(paramCanvas, path, f);
    drawCenter(paramCanvas, f);
  }
  
  private void showPicker(boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mShowHours == paramBoolean1)
      return; 
    this.mShowHours = paramBoolean1;
    if (paramBoolean2) {
      animatePicker(paramBoolean1, 500L);
    } else {
      float f;
      ObjectAnimator objectAnimator = this.mHoursToMinutesAnimator;
      if (objectAnimator != null && objectAnimator.isStarted()) {
        this.mHoursToMinutesAnimator.cancel();
        this.mHoursToMinutesAnimator = null;
      } 
      if (paramBoolean1) {
        f = 0.0F;
      } else {
        f = 1.0F;
      } 
      this.mHoursToMinutes = f;
    } 
    initData();
    invalidate();
    this.mTouchHelper.invalidateRoot();
  }
  
  private void animatePicker(boolean paramBoolean, long paramLong) {
    float f;
    if (paramBoolean) {
      f = 0.0F;
    } else {
      f = 1.0F;
    } 
    if (this.mHoursToMinutes == f) {
      ObjectAnimator objectAnimator1 = this.mHoursToMinutesAnimator;
      if (objectAnimator1 != null && objectAnimator1.isStarted()) {
        this.mHoursToMinutesAnimator.cancel();
        this.mHoursToMinutesAnimator = null;
      } 
      return;
    } 
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, this.HOURS_TO_MINUTES, new float[] { f });
    objectAnimator.setAutoCancel(true);
    this.mHoursToMinutesAnimator.setDuration(paramLong);
    this.mHoursToMinutesAnimator.start();
  }
  
  private void drawCircleBackground(Canvas paramCanvas) {
    paramCanvas.drawCircle(this.mXCenter, this.mYCenter, this.mCircleRadius, this.mPaintBackground);
  }
  
  private void drawHours(Canvas paramCanvas, Path paramPath, float paramFloat) {
    int i = (int)((1.0F - this.mHoursToMinutes) * 255.0F * paramFloat + 0.5F);
    if (i > 0) {
      paramCanvas.save(2);
      paramCanvas.clipPath(paramPath, Region.Op.DIFFERENCE);
      drawHoursClipped(paramCanvas, i, false);
      paramCanvas.restore();
      paramCanvas.save(2);
      paramCanvas.clipPath(paramPath, Region.Op.INTERSECT);
      drawHoursClipped(paramCanvas, i, true);
      paramCanvas.restore();
    } 
  }
  
  private void drawHoursClipped(Canvas paramCanvas, int paramInt, boolean paramBoolean) {
    boolean bool;
    float f = this.mTextSize[0];
    Typeface typeface = this.mTypeface;
    ColorStateList colorStateList = this.mTextColor[0];
    String[] arrayOfString = this.mOuterTextHours;
    float[] arrayOfFloat1 = this.mOuterTextX[0], arrayOfFloat2 = this.mOuterTextY[0];
    Paint paint = this.mPaint[0];
    if (paramBoolean && !this.mIsOnInnerCircle) {
      bool = true;
    } else {
      bool = false;
    } 
    drawTextElements(paramCanvas, f, typeface, colorStateList, arrayOfString, arrayOfFloat1, arrayOfFloat2, paint, paramInt, bool, this.mSelectionDegrees[0], paramBoolean);
    if (this.mIs24HourMode) {
      String[] arrayOfString1 = this.mInnerTextHours;
      if (arrayOfString1 != null) {
        f = this.mTextSize[2];
        Typeface typeface1 = this.mTypeface;
        ColorStateList colorStateList1 = this.mTextColor[2];
        float[] arrayOfFloat = this.mInnerTextX;
        arrayOfFloat1 = this.mInnerTextY;
        Paint paint1 = this.mPaint[0];
        if (paramBoolean && this.mIsOnInnerCircle) {
          bool = true;
        } else {
          bool = false;
        } 
        drawTextElements(paramCanvas, f, typeface1, colorStateList1, arrayOfString1, arrayOfFloat, arrayOfFloat1, paint1, paramInt, bool, this.mSelectionDegrees[0], paramBoolean);
      } 
    } 
  }
  
  private void drawMinutes(Canvas paramCanvas, Path paramPath, float paramFloat) {
    int i = (int)(this.mHoursToMinutes * 255.0F * paramFloat + 0.5F);
    if (i > 0) {
      paramCanvas.save(2);
      paramCanvas.clipPath(paramPath, Region.Op.DIFFERENCE);
      drawMinutesClipped(paramCanvas, i, false);
      paramCanvas.restore();
      paramCanvas.save(2);
      paramCanvas.clipPath(paramPath, Region.Op.INTERSECT);
      drawMinutesClipped(paramCanvas, i, true);
      paramCanvas.restore();
    } 
  }
  
  private void drawMinutesClipped(Canvas paramCanvas, int paramInt, boolean paramBoolean) {
    drawTextElements(paramCanvas, this.mTextSize[1], this.mTypeface, this.mTextColor[1], this.mMinutesText, this.mOuterTextX[1], this.mOuterTextY[1], this.mPaint[1], paramInt, paramBoolean, this.mSelectionDegrees[1], paramBoolean);
  }
  
  private void drawCenter(Canvas paramCanvas, float paramFloat) {
    this.mPaintCenter.setAlpha((int)(255.0F * paramFloat + 0.5F));
    paramCanvas.drawCircle(this.mXCenter, this.mYCenter, this.mCenterDotRadius, this.mPaintCenter);
  }
  
  private int getMultipliedAlpha(int paramInt1, int paramInt2) {
    return (int)(Color.alpha(paramInt1) * paramInt2 / 255.0D + 0.5D);
  }
  
  private void drawSelector(Canvas paramCanvas, Path paramPath) {
    if (this.mIsOnInnerCircle) {
      i = 2;
    } else {
      i = 0;
    } 
    int j = this.mTextInset[i];
    int arrayOfInt[] = this.mSelectionDegrees, k = arrayOfInt[i % 2];
    int i = arrayOfInt[i % 2];
    float f1 = 1.0F;
    if (i % 30 != 0) {
      f2 = 1.0F;
    } else {
      f2 = 0.0F;
    } 
    int m = this.mTextInset[1];
    arrayOfInt = this.mSelectionDegrees;
    int n = arrayOfInt[1];
    if (arrayOfInt[1] % 30 == 0)
      f1 = 0.0F; 
    i = this.mSelectorRadius;
    float f3 = this.mCircleRadius, f4 = j, f5 = m, f6 = this.mHoursToMinutes;
    f6 = f3 - MathUtils.lerp(f4, f5, f6);
    f3 = k;
    f5 = n;
    f4 = this.mHoursToMinutes;
    double d1 = Math.toRadians(MathUtils.lerpDeg(f3, f5, f4));
    f5 = this.mXCenter + (float)Math.sin(d1) * f6;
    f3 = this.mYCenter - (float)Math.cos(d1) * f6;
    Paint paint2 = this.mPaintSelector[0];
    paint2.setColor(this.mSelectorColor);
    paramCanvas.drawCircle(f5, f3, i, paint2);
    if (paramPath != null) {
      paramPath.reset();
      paramPath.addCircle(f5, f3, i, Path.Direction.CCW);
    } 
    float f2 = MathUtils.lerp(f2, f1, this.mHoursToMinutes);
    if (f2 > 0.0F) {
      Paint paint = this.mPaintSelector[1];
      paint.setColor(this.mSelectorDotColor);
      paramCanvas.drawCircle(f5, f3, this.mSelectorDotRadius * f2, paint);
    } 
    double d2 = Math.sin(d1);
    d1 = Math.cos(d1);
    f1 = f6 - i;
    j = this.mXCenter;
    n = this.mCenterDotRadius;
    k = (int)(n * d2);
    i = this.mYCenter;
    n = (int)(n * d1);
    f2 = ((int)(f1 * d2) + j + k);
    f1 = (i - n - (int)(f1 * d1));
    Paint paint1 = this.mPaintSelector[2];
    paint1.setColor(this.mSelectorColor);
    paint1.setStrokeWidth(this.mSelectorStroke);
    paramCanvas.drawLine(this.mXCenter, this.mYCenter, f2, f1, paint1);
  }
  
  private void calculatePositionsHours() {
    float f = (this.mCircleRadius - this.mTextInset[0]);
    calculatePositions(this.mPaint[0], f, this.mXCenter, this.mYCenter, this.mTextSize[0], this.mOuterTextX[0], this.mOuterTextY[0]);
    if (this.mIs24HourMode) {
      int i = this.mCircleRadius, j = this.mTextInset[2];
      calculatePositions(this.mPaint[0], (i - j), this.mXCenter, this.mYCenter, this.mTextSize[2], this.mInnerTextX, this.mInnerTextY);
    } 
  }
  
  private void calculatePositionsMinutes() {
    float f = (this.mCircleRadius - this.mTextInset[1]);
    calculatePositions(this.mPaint[1], f, this.mXCenter, this.mYCenter, this.mTextSize[1], this.mOuterTextX[1], this.mOuterTextY[1]);
  }
  
  private static void calculatePositions(Paint paramPaint, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    paramPaint.setTextSize(paramFloat4);
    paramFloat4 = (paramPaint.descent() + paramPaint.ascent()) / 2.0F;
    for (byte b = 0; b < 12; b++) {
      paramArrayOffloat1[b] = paramFloat2 - COS_30[b] * paramFloat1;
      paramArrayOffloat2[b] = paramFloat3 - paramFloat4 - SIN_30[b] * paramFloat1;
    } 
  }
  
  private void drawTextElements(Canvas paramCanvas, float paramFloat, Typeface paramTypeface, ColorStateList paramColorStateList, String[] paramArrayOfString, float[] paramArrayOffloat1, float[] paramArrayOffloat2, Paint paramPaint, int paramInt1, boolean paramBoolean1, int paramInt2, boolean paramBoolean2) {
    paramPaint.setTextSize(paramFloat);
    paramPaint.setTypeface(paramTypeface);
    paramFloat = paramInt2 / 30.0F;
    int i = (int)paramFloat;
    int j = (int)Math.ceil(paramFloat);
    for (paramInt2 = 0; paramInt2 < 12; paramInt2++) {
      int k;
      if (i == paramInt2 || j % 12 == paramInt2) {
        k = 1;
      } else {
        k = 0;
      } 
      if (!paramBoolean2 || k) {
        if (paramBoolean1 && k) {
          k = 32;
        } else {
          k = 0;
        } 
        k = paramColorStateList.getColorForState(StateSet.get(0x8 | k), 0);
        paramPaint.setColor(k);
        paramPaint.setAlpha(getMultipliedAlpha(k, paramInt1));
        paramCanvas.drawText(paramArrayOfString[paramInt2], paramArrayOffloat1[paramInt2], paramArrayOffloat2[paramInt2], paramPaint);
      } 
    } 
  }
  
  private int getDegreesFromXY(float paramFloat1, float paramFloat2, boolean paramBoolean) {
    int j;
    if (this.mIs24HourMode && this.mShowHours) {
      i = this.mMinDistForInnerNumber;
      j = this.mMaxDistForOuterNumber;
    } else {
      boolean bool = this.mShowHours;
      int k = this.mCircleRadius - this.mTextInset[bool ^ true];
      i = this.mSelectorRadius;
      j = i + k;
      i = k - i;
    } 
    double d1 = (paramFloat1 - this.mXCenter);
    double d2 = (paramFloat2 - this.mYCenter);
    double d3 = Math.sqrt(d1 * d1 + d2 * d2);
    if (d3 < i || (paramBoolean && d3 > j))
      return -1; 
    int i = (int)(Math.toDegrees(Math.atan2(d2, d1) + 1.5707963267948966D) + 0.5D);
    if (i < 0)
      return i + 360; 
    return i;
  }
  
  private boolean getInnerCircleFromXY(float paramFloat1, float paramFloat2) {
    boolean bool = this.mIs24HourMode;
    boolean bool1 = false;
    if (bool && this.mShowHours) {
      double d1 = (paramFloat1 - this.mXCenter);
      double d2 = (paramFloat2 - this.mYCenter);
      d2 = Math.sqrt(d1 * d1 + d2 * d2);
      if (d2 <= this.mHalfwayDist)
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (!this.mInputEnabled)
      return true; 
    int i = paramMotionEvent.getActionMasked();
    if (i == 2 || i == 1 || i == 0) {
      boolean bool2, bool1 = false;
      boolean bool = false;
      if (i == 0) {
        this.mChangedDuringTouch = false;
        bool2 = bool1;
      } else {
        bool2 = bool1;
        if (i == 1) {
          boolean bool3 = true;
          bool2 = bool1;
          bool = bool3;
          if (!this.mChangedDuringTouch) {
            bool2 = true;
            bool = bool3;
          } 
        } 
      } 
      bool1 = this.mChangedDuringTouch;
      float f1 = paramMotionEvent.getX(), f2 = paramMotionEvent.getY();
      this.mChangedDuringTouch = bool1 | handleTouchInput(f1, f2, bool2, bool);
    } 
    return true;
  }
  
  private boolean handleTouchInput(float paramFloat1, float paramFloat2, boolean paramBoolean1, boolean paramBoolean2) {
    int j, k;
    boolean bool = getInnerCircleFromXY(paramFloat1, paramFloat2);
    int i = getDegreesFromXY(paramFloat1, paramFloat2, false);
    if (i == -1)
      return false; 
    animatePicker(this.mShowHours, 60L);
    if (this.mShowHours) {
      j = snapOnly30s(i, 0) % 360;
      if (this.mIsOnInnerCircle != bool || this.mSelectionDegrees[0] != j) {
        i = 1;
      } else {
        i = 0;
      } 
      this.mIsOnInnerCircle = bool;
      this.mSelectionDegrees[0] = j;
      j = 0;
      k = getCurrentHour();
    } else {
      j = snapPrefer30s(i) % 360;
      if (this.mSelectionDegrees[1] != j) {
        i = 1;
      } else {
        i = 0;
      } 
      this.mSelectionDegrees[1] = j;
      j = 1;
      k = getCurrentMinute();
    } 
    if (i != 0 || paramBoolean1 || paramBoolean2) {
      OnValueSelectedListener onValueSelectedListener = this.mListener;
      if (onValueSelectedListener != null)
        onValueSelectedListener.onValueSelected(j, k, paramBoolean2); 
      if (i != 0 || paramBoolean1) {
        performHapticFeedback(4);
        invalidate();
      } 
      return true;
    } 
    return false;
  }
  
  public boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    if (this.mTouchHelper.dispatchHoverEvent(paramMotionEvent))
      return true; 
    return super.dispatchHoverEvent(paramMotionEvent);
  }
  
  public void setInputEnabled(boolean paramBoolean) {
    this.mInputEnabled = paramBoolean;
    invalidate();
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    if (!isEnabled())
      return null; 
    int i = getDegreesFromXY(paramMotionEvent.getX(), paramMotionEvent.getY(), false);
    if (i != -1)
      return PointerIcon.getSystemIcon(getContext(), 1002); 
    return super.onResolvePointerIcon(paramMotionEvent, paramInt);
  }
  
  class OnValueSelectedListener {
    public abstract void onValueSelected(int param1Int1, int param1Int2, boolean param1Boolean);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class PickerType implements Annotation {}
  
  class RadialPickerTouchHelper extends ExploreByTouchHelper {
    private final Rect mTempRect = new Rect();
    
    private final int TYPE_HOUR = 1;
    
    private final int TYPE_MINUTE = 2;
    
    private final int SHIFT_TYPE = 0;
    
    private final int MASK_TYPE = 15;
    
    private final int SHIFT_VALUE = 8;
    
    private final int MASK_VALUE = 255;
    
    private final int MINUTE_INCREMENT = 5;
    
    final RadialTimePickerView this$0;
    
    public void onInitializeAccessibilityNodeInfo(View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      super.onInitializeAccessibilityNodeInfo(param1View, param1AccessibilityNodeInfo);
      param1AccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
      param1AccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
    }
    
    public boolean performAccessibilityAction(View param1View, int param1Int, Bundle param1Bundle) {
      if (super.performAccessibilityAction(param1View, param1Int, param1Bundle))
        return true; 
      if (param1Int != 4096) {
        if (param1Int != 8192)
          return false; 
        adjustPicker(-1);
        return true;
      } 
      adjustPicker(1);
      return true;
    }
    
    private void adjustPicker(int param1Int) {
      byte b1;
      int i;
      boolean bool;
      byte b2;
      if (RadialTimePickerView.this.mShowHours) {
        b1 = 1;
        i = RadialTimePickerView.this.getCurrentHour();
        if (RadialTimePickerView.this.mIs24HourMode) {
          bool = false;
          b2 = 23;
        } else {
          i = hour24To12(i);
          bool = true;
          b2 = 12;
        } 
      } else {
        b1 = 5;
        i = RadialTimePickerView.this.getCurrentMinute() / 5;
        bool = false;
        b2 = 55;
      } 
      param1Int = MathUtils.constrain((i + param1Int) * b1, bool, b2);
      if (RadialTimePickerView.this.mShowHours) {
        RadialTimePickerView.this.setCurrentHour(param1Int);
      } else {
        RadialTimePickerView.this.setCurrentMinute(param1Int);
      } 
    }
    
    protected int getVirtualViewAt(float param1Float1, float param1Float2) {
      int j, i = RadialTimePickerView.this.getDegreesFromXY(param1Float1, param1Float2, true);
      if (i != -1) {
        int k = RadialTimePickerView.snapOnly30s(i, 0) % 360;
        if (RadialTimePickerView.this.mShowHours) {
          boolean bool = RadialTimePickerView.this.getInnerCircleFromXY(param1Float1, param1Float2);
          j = RadialTimePickerView.this.getHourForDegrees(k, bool);
          if (!RadialTimePickerView.this.mIs24HourMode)
            j = hour24To12(j); 
          j = makeId(1, j);
        } else {
          j = RadialTimePickerView.this.getCurrentMinute();
          int m = RadialTimePickerView.this.getMinuteForDegrees(i);
          k = RadialTimePickerView.this.getMinuteForDegrees(k);
          i = getCircularDiff(j, m, 60);
          m = getCircularDiff(k, m, 60);
          if (i >= m)
            j = k; 
          j = makeId(2, j);
        } 
      } else {
        j = Integer.MIN_VALUE;
      } 
      return j;
    }
    
    private int getCircularDiff(int param1Int1, int param1Int2, int param1Int3) {
      param1Int1 = Math.abs(param1Int1 - param1Int2);
      param1Int2 = param1Int3 / 2;
      if (param1Int1 > param1Int2)
        param1Int1 = param1Int3 - param1Int1; 
      return param1Int1;
    }
    
    protected void getVisibleVirtualViews(IntArray param1IntArray) {
      if (RadialTimePickerView.this.mShowHours) {
        byte b;
        boolean bool = RadialTimePickerView.this.mIs24HourMode;
        if (RadialTimePickerView.this.mIs24HourMode) {
          b = 23;
        } else {
          b = 12;
        } 
        for (int i = bool ^ true; i <= b; i++)
          param1IntArray.add(makeId(1, i)); 
      } else {
        int i = RadialTimePickerView.this.getCurrentMinute();
        for (byte b = 0; b < 60; b += 5) {
          param1IntArray.add(makeId(2, b));
          if (i > b && i < b + 5)
            param1IntArray.add(makeId(2, i)); 
        } 
      } 
    }
    
    protected void onPopulateEventForVirtualView(int param1Int, AccessibilityEvent param1AccessibilityEvent) {
      param1AccessibilityEvent.setClassName(getClass().getName());
      int i = getTypeFromId(param1Int);
      param1Int = getValueFromId(param1Int);
      CharSequence charSequence = getVirtualViewDescription(i, param1Int);
      param1AccessibilityEvent.setContentDescription(charSequence);
    }
    
    protected void onPopulateNodeForVirtualView(int param1Int, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      param1AccessibilityNodeInfo.setClassName(getClass().getName());
      param1AccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_CLICK);
      int i = getTypeFromId(param1Int);
      int j = getValueFromId(param1Int);
      CharSequence charSequence = getVirtualViewDescription(i, j);
      param1AccessibilityNodeInfo.setContentDescription(charSequence);
      getBoundsForVirtualView(param1Int, this.mTempRect);
      param1AccessibilityNodeInfo.setBoundsInParent(this.mTempRect);
      boolean bool = isVirtualViewSelected(i, j);
      param1AccessibilityNodeInfo.setSelected(bool);
      param1Int = getVirtualViewIdAfter(i, j);
      if (param1Int != Integer.MIN_VALUE)
        param1AccessibilityNodeInfo.setTraversalBefore(RadialTimePickerView.this, param1Int); 
    }
    
    private int getVirtualViewIdAfter(int param1Int1, int param1Int2) {
      if (param1Int1 == 1) {
        int i = param1Int2 + 1;
        if (RadialTimePickerView.this.mIs24HourMode) {
          param1Int2 = 23;
        } else {
          param1Int2 = 12;
        } 
        if (i <= param1Int2)
          return makeId(param1Int1, i); 
      } else if (param1Int1 == 2) {
        int i = RadialTimePickerView.this.getCurrentMinute();
        int j = param1Int2 - param1Int2 % 5 + 5;
        if (param1Int2 < i && j > i)
          return makeId(param1Int1, i); 
        if (j < 60)
          return makeId(param1Int1, j); 
      } 
      return Integer.MIN_VALUE;
    }
    
    protected boolean onPerformActionForVirtualView(int param1Int1, int param1Int2, Bundle param1Bundle) {
      if (param1Int2 == 16) {
        param1Int2 = getTypeFromId(param1Int1);
        param1Int1 = getValueFromId(param1Int1);
        if (param1Int2 == 1) {
          if (!RadialTimePickerView.this.mIs24HourMode)
            param1Int1 = hour12To24(param1Int1, RadialTimePickerView.this.mAmOrPm); 
          RadialTimePickerView.this.setCurrentHour(param1Int1);
          return true;
        } 
        if (param1Int2 == 2) {
          RadialTimePickerView.this.setCurrentMinute(param1Int1);
          return true;
        } 
      } 
      return false;
    }
    
    private int hour12To24(int param1Int1, int param1Int2) {
      int i = param1Int1;
      if (param1Int1 == 12) {
        param1Int1 = i;
        if (param1Int2 == 0)
          param1Int1 = 0; 
      } else {
        param1Int1 = i;
        if (param1Int2 == 1)
          param1Int1 = i + 12; 
      } 
      return param1Int1;
    }
    
    private int hour24To12(int param1Int) {
      if (param1Int == 0)
        return 12; 
      if (param1Int > 12)
        return param1Int - 12; 
      return param1Int;
    }
    
    private void getBoundsForVirtualView(int param1Int, Rect param1Rect) {
      float f2;
      int i = getTypeFromId(param1Int);
      param1Int = getValueFromId(param1Int);
      if (i == 1) {
        boolean bool = RadialTimePickerView.this.getInnerCircleForHour(param1Int);
        if (bool) {
          f1 = (RadialTimePickerView.this.mCircleRadius - RadialTimePickerView.this.mTextInset[2]);
          f2 = RadialTimePickerView.this.mSelectorRadius;
        } else {
          f1 = (RadialTimePickerView.this.mCircleRadius - RadialTimePickerView.this.mTextInset[0]);
          f2 = RadialTimePickerView.this.mSelectorRadius;
        } 
        f3 = RadialTimePickerView.this.getDegreesForHour(param1Int);
      } else if (i == 2) {
        f1 = (RadialTimePickerView.this.mCircleRadius - RadialTimePickerView.this.mTextInset[1]);
        f3 = RadialTimePickerView.this.getDegreesForMinute(param1Int);
        f2 = RadialTimePickerView.this.mSelectorRadius;
      } else {
        f1 = 0.0F;
        f3 = 0.0F;
        f2 = 0.0F;
      } 
      double d = Math.toRadians(f3);
      float f3 = RadialTimePickerView.this.mXCenter + (float)Math.sin(d) * f1;
      float f1 = RadialTimePickerView.this.mYCenter - (float)Math.cos(d) * f1;
      param1Rect.set((int)(f3 - f2), (int)(f1 - f2), (int)(f3 + f2), (int)(f1 + f2));
    }
    
    private CharSequence getVirtualViewDescription(int param1Int1, int param1Int2) {
      if (param1Int1 == 1 || param1Int1 == 2)
        return Integer.toString(param1Int2); 
      return null;
    }
    
    private boolean isVirtualViewSelected(int param1Int1, int param1Int2) {
      boolean bool1 = false, bool2 = false;
      if (param1Int1 == 1) {
        bool1 = bool2;
        if (RadialTimePickerView.this.getCurrentHour() == param1Int2)
          bool1 = true; 
      } else if (param1Int1 == 2) {
        if (RadialTimePickerView.this.getCurrentMinute() == param1Int2)
          bool1 = true; 
      } else {
        bool1 = false;
      } 
      return bool1;
    }
    
    private int makeId(int param1Int1, int param1Int2) {
      return param1Int1 << 0 | param1Int2 << 8;
    }
    
    private int getTypeFromId(int param1Int) {
      return param1Int >>> 0 & 0xF;
    }
    
    private int getValueFromId(int param1Int) {
      return param1Int >>> 8 & 0xFF;
    }
    
    public RadialPickerTouchHelper() {
      super(RadialTimePickerView.this);
    }
  }
}
