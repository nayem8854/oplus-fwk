package android.widget;

import android.content.Context;
import android.os.SystemProperties;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.util.Log;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.PathInterpolator;
import com.oplus.util.OplusContextUtil;

public class OplusOverScroller extends OverScroller {
  private static final boolean DBG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final int DEFAULT_DURATION = 250;
  
  private static final float DEFAULT_TIME_GAP = 16.0F;
  
  private static final int FLING_MODE = 1;
  
  private static final int MAXIMUM_FLING_VELOCITY_LIST = 2500;
  
  private static final int MAX_VELOCITY = 9000;
  
  public static final float OPLUS_FLING_FRICTION_FAST = 1.65F;
  
  public static final float OPLUS_FLING_FRICTION_NORMAL = 2.05F;
  
  public static final int OPLUS_FLING_MODE_FAST = 0;
  
  public static final int OPLUS_FLING_MODE_NORMAL = 1;
  
  private static final double PI = 3.1415926D;
  
  private static final int SCROLL_MODE = 0;
  
  private static final int SPRING_BACK_DURATION = 600;
  
  private static final String TAG = "OplusOverScroller";
  
  private static int sMaximumVelocity;
  
  private static int sOverscrollDistance;
  
  private final boolean mFlywheel;
  
  private Interpolator mInterpolator;
  
  private int mMode;
  
  private final OplusSplineOverScroller mScrollerX;
  
  private final OplusSplineOverScroller mScrollerY;
  
  public OplusOverScroller(Context paramContext) {
    this(paramContext, null);
  }
  
  public OplusOverScroller(Context paramContext, Interpolator paramInterpolator) {
    this(paramContext, paramInterpolator, true);
  }
  
  public OplusOverScroller(Context paramContext, Interpolator paramInterpolator, boolean paramBoolean) {
    super(paramContext, paramInterpolator, paramBoolean);
    if (paramInterpolator == null) {
      this.mInterpolator = new Scroller.ViscousFluidInterpolator();
    } else {
      this.mInterpolator = paramInterpolator;
    } 
    this.mFlywheel = paramBoolean;
    this.mScrollerX = new OplusSplineOverScroller(paramContext);
    this.mScrollerY = new OplusSplineOverScroller(paramContext);
    DisplayMetrics displayMetrics = paramContext.getResources().getDisplayMetrics();
    sOverscrollDistance = displayMetrics.heightPixels;
  }
  
  public OplusOverScroller(Context paramContext, Interpolator paramInterpolator, float paramFloat1, float paramFloat2) {
    this(paramContext, paramInterpolator, true);
  }
  
  public OplusOverScroller(Context paramContext, Interpolator paramInterpolator, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    this(paramContext, paramInterpolator, paramBoolean);
  }
  
  public void setEnableScrollList(boolean paramBoolean) {
    OplusSplineOverScroller.access$002(this.mScrollerX, paramBoolean);
    OplusSplineOverScroller.access$002(this.mScrollerY, paramBoolean);
  }
  
  public static OverScroller newInstance(Context paramContext) {
    OverScroller overScroller;
    if (OplusContextUtil.isOplusOSStyle(paramContext)) {
      overScroller = new OplusOverScroller(paramContext);
    } else {
      overScroller = new OverScroller((Context)overScroller);
    } 
    return overScroller;
  }
  
  public static OverScroller newInstance(Context paramContext, boolean paramBoolean) {
    OverScroller overScroller;
    if (OplusContextUtil.isOplusOSStyle(paramContext)) {
      overScroller = new OplusOverScroller(paramContext);
      OplusOverScroller oplusOverScroller = (OplusOverScroller)overScroller;
      OplusSplineOverScroller.access$002(oplusOverScroller.mScrollerX, paramBoolean);
      OplusSplineOverScroller.access$002(oplusOverScroller.mScrollerY, paramBoolean);
    } else {
      overScroller = new OverScroller((Context)overScroller);
    } 
    return overScroller;
  }
  
  void setInterpolator(Interpolator paramInterpolator) {
    if (paramInterpolator == null) {
      this.mInterpolator = new Scroller.ViscousFluidInterpolator();
    } else {
      this.mInterpolator = paramInterpolator;
    } 
  }
  
  public void setOplusFriction(float paramFloat) {
    this.mScrollerX.setFriction(paramFloat);
    this.mScrollerY.setFriction(paramFloat);
  }
  
  public boolean isOplusFinished() {
    boolean bool;
    if (this.mScrollerX.mFinished && this.mScrollerY.mFinished) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void oplusForceFinished(boolean paramBoolean) {
    OplusSplineOverScroller.access$102(this.mScrollerX, paramBoolean);
    OplusSplineOverScroller.access$102(this.mScrollerY, paramBoolean);
  }
  
  public int getOplusCurrX() {
    return this.mScrollerX.mCurrentPosition;
  }
  
  public int getOplusCurrY() {
    return this.mScrollerY.mCurrentPosition;
  }
  
  public float getCurrVelocity() {
    float f1 = this.mScrollerX.mCurrVelocity, f2 = this.mScrollerX.mCurrVelocity;
    float f3 = this.mScrollerY.mCurrVelocity, f4 = this.mScrollerY.mCurrVelocity;
    return FloatMath.sqrt(f1 * f2 + f3 * f4);
  }
  
  public final int getOplusStartX() {
    return this.mScrollerX.mStart;
  }
  
  public final int getOplusStartY() {
    return this.mScrollerY.mStart;
  }
  
  public int getOplusFinalX() {
    return this.mScrollerX.mFinal;
  }
  
  public int getOplusFinalY() {
    return this.mScrollerY.mFinal;
  }
  
  @Deprecated
  public int getOplusDuration() {
    return Math.max(this.mScrollerX.mDuration, this.mScrollerY.mDuration);
  }
  
  @Deprecated
  public void extendDuration(int paramInt) {
    this.mScrollerX.extendDuration(paramInt);
    this.mScrollerY.extendDuration(paramInt);
  }
  
  @Deprecated
  public void setFinalX(int paramInt) {
    this.mScrollerX.setFinalPosition(paramInt);
  }
  
  @Deprecated
  public void setFinalY(int paramInt) {
    this.mScrollerY.setFinalPosition(paramInt);
  }
  
  public boolean computeScrollOffset() {
    if (isOplusFinished())
      return false; 
    int i = this.mMode;
    if (i != 0) {
      if (i == 1) {
        if (!this.mScrollerX.mFinished && 
          !this.mScrollerX.update() && 
          !this.mScrollerX.continueWhenFinished())
          this.mScrollerX.finish(); 
        if (!this.mScrollerY.mFinished && 
          !this.mScrollerY.update() && 
          !this.mScrollerY.continueWhenFinished())
          this.mScrollerY.finish(); 
      } 
    } else {
      long l = AnimationUtils.currentAnimationTimeMillis();
      l -= this.mScrollerX.mStartTime;
      i = this.mScrollerX.mDuration;
      if (l < i) {
        float f = this.mInterpolator.getInterpolation((float)l / i);
        this.mScrollerX.updateScroll(f);
        this.mScrollerY.updateScroll(f);
      } else {
        abortAnimation();
      } 
    } 
    return true;
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    startScroll(paramInt1, paramInt2, paramInt3, paramInt4, 250);
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mMode = 0;
    this.mScrollerX.startScroll(paramInt1, paramInt3, paramInt5);
    this.mScrollerY.startScroll(paramInt2, paramInt4, paramInt5);
  }
  
  public boolean springBack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    boolean bool1 = true;
    this.mMode = 1;
    boolean bool2 = this.mScrollerX.springback(paramInt1, paramInt3, paramInt4);
    boolean bool3 = this.mScrollerY.springback(paramInt2, paramInt5, paramInt6);
    boolean bool4 = bool1;
    if (!bool2)
      if (bool3) {
        bool4 = bool1;
      } else {
        bool4 = false;
      }  
    return bool4;
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    fling(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, 0, 0);
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    if (this.mFlywheel && !isOplusFinished()) {
      float f1 = this.mScrollerX.mCurrVelocity;
      float f2 = this.mScrollerY.mCurrVelocity;
      if (Math.signum(paramInt3) == Math.signum(f1)) {
        float f = paramInt4;
        if (Math.signum(f) == Math.signum(f2)) {
          paramInt3 = (int)(paramInt3 + f1);
          paramInt4 = (int)(paramInt4 + f2);
        } 
      } 
    } 
    this.mMode = 1;
    this.mScrollerX.fling(paramInt1, paramInt3, paramInt5, paramInt6, paramInt9);
    this.mScrollerY.fling(paramInt2, paramInt4, paramInt7, paramInt8, paramInt10);
  }
  
  public void notifyHorizontalEdgeReached(int paramInt1, int paramInt2, int paramInt3) {
    this.mScrollerX.notifyEdgeReached(paramInt1, paramInt2, paramInt3);
  }
  
  public void notifyVerticalEdgeReached(int paramInt1, int paramInt2, int paramInt3) {
    this.mScrollerY.notifyEdgeReached(paramInt1, paramInt2, paramInt3);
  }
  
  public boolean isOverScrolled() {
    if (this.mScrollerX.mFinished || this.mScrollerX.mState == 0) {
      OplusSplineOverScroller oplusSplineOverScroller = this.mScrollerY;
      return 
        (!oplusSplineOverScroller.mFinished && this.mScrollerY.mState != 0);
    } 
    return true;
  }
  
  public void abortAnimation() {
    this.mScrollerX.finish();
    this.mScrollerY.finish();
  }
  
  public int timePassed() {
    long l1 = AnimationUtils.currentAnimationTimeMillis();
    long l2 = Math.min(this.mScrollerX.mStartTime, this.mScrollerY.mStartTime);
    return (int)(l1 - l2);
  }
  
  public boolean isScrollingInDirection(float paramFloat1, float paramFloat2) {
    boolean bool;
    int i = this.mScrollerX.mFinal, j = this.mScrollerX.mStart;
    int k = this.mScrollerY.mFinal, m = this.mScrollerY.mStart;
    if (!isOplusFinished() && Math.signum(paramFloat1) == Math.signum((i - j)) && 
      Math.signum(paramFloat2) == Math.signum((k - m))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setFlingFriction(float paramFloat) {
    OplusSplineOverScroller.access$902(this.mScrollerX, paramFloat);
    OplusSplineOverScroller.access$902(this.mScrollerY, paramFloat);
  }
  
  public void setOplusFlingMode(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1)
        throw new IllegalArgumentException("wrong fling argument"); 
    } else {
      setFlingFriction(1.65F);
    } 
  }
  
  class OplusSplineOverScroller {
    private static final float DECELERATION_RATE = (float)(Math.log(0.78D) / Math.log(0.9D));
    
    private static final float[] SPLINE_POSITION = new float[101];
    
    private static final float[] SPLINE_TIME = new float[101];
    
    private static float sTimeIncrease = 1.0F;
    
    private int mOplusCount = 1;
    
    private float mStartV = 0.0F;
    
    private double mLastDetla = 0.0D;
    
    private boolean mIsScrollList = false;
    
    private boolean mOverSpring = false;
    
    private float mFlingFriction = ViewConfiguration.getScrollFriction();
    
    private int mState = 0;
    
    private float mOrigamiFriction = 2.05F;
    
    private float mReboundTension = 0.0F;
    
    private int mRestThreshold = 40;
    
    private static final int BALLISTIC = 2;
    
    private static final float BALLISTIC_THRESHOLD = 0.91F;
    
    private static final float BASE_DENSITY_FACTOR = 160.0F;
    
    private static final int CUBIC = 1;
    
    private static final float END_TENSION = 1.0F;
    
    private static final float FLING_CHANGE_INCREASE_STEP = 1.2F;
    
    private static final float FLING_CHANGE_REDUCE_STEP = 0.6F;
    
    private static final float FLING_CONTROL_ONE_X = 0.0F;
    
    private static final float FLING_CONTROL_ONE_Y = 0.17F;
    
    private static final float FLING_CONTROL_TWO_X = 0.25F;
    
    private static final float FLING_CONTROL_TWO_Y = 0.85F;
    
    private static final float FLING_DXDT_RATIO = 0.0694F;
    
    private static final int FLING_SPLINE = 3;
    
    private static final float FLOAT_1 = 1.0F;
    
    private static final float FLOAT_2 = 2.0F;
    
    private static final float FLOAT_25 = 25.0F;
    
    private static final float FLOAT_3 = 3.0F;
    
    private static final float FLOAT_8 = 8.0F;
    
    private static final float GRAVITY = 2000.0F;
    
    private static final float INCH_METER = 39.37F;
    
    private static final float INFLEXION = 0.35F;
    
    private static final int NB_SAMPLES = 100;
    
    private static final int NUM_10 = 10;
    
    private static final int NUM_100 = 100;
    
    private static final int NUM_1000 = 1000;
    
    private static final int NUM_60 = 60;
    
    private static final int NUM_800 = 800;
    
    private static final int OVER_SPLINE = 4;
    
    private static final float P1 = 0.175F;
    
    private static final float P2 = 0.35000002F;
    
    private static final float PHYSICAL_COFF_FACTOR = 0.84F;
    
    private static final double SOLVER_TIMESTEP_SEC = 0.016D;
    
    private static final int SPLINE = 0;
    
    private static final float START_TENSION = 0.5F;
    
    private static final float VISCOUS_FLUID_SCALE = 14.0F;
    
    private static float sViscousFluidNormalize;
    
    private float mCurrVelocity;
    
    private int mCurrentPosition;
    
    private float mDeceleration;
    
    private int mDuration;
    
    private double mEndValue;
    
    private int mFinal;
    
    private boolean mFinished;
    
    private PathInterpolator mFlingInterpolator;
    
    private int mLastPosition;
    
    private double mLastVelocity;
    
    private int mOver;
    
    private boolean mOverSplineStart;
    
    private float mPhysicalCoeff;
    
    private float mReboundFriction;
    
    private int mScrollerDistance;
    
    private int mSplineDistance;
    
    private int mSplineState;
    
    private int mStart;
    
    private long mStartTime;
    
    private int mVelocity;
    
    void setFriction(float param1Float) {
      this.mFlingFriction = param1Float;
    }
    
    OplusSplineOverScroller(OplusOverScroller this$0) {
      this.mFinished = true;
      float f = (this$0.getResources().getDisplayMetrics()).density;
      this.mPhysicalCoeff = 386.0878F * f * 160.0F * 0.84F;
      sViscousFluidNormalize = 1.0F;
      sViscousFluidNormalize = 1.0F / viscousFluid(1.0F, 14.0F);
      this.mFlingInterpolator = new PathInterpolator(0.0F, 0.17F, 0.25F, 0.85F);
    }
    
    void updateScroll(float param1Float) {
      int i = this.mStart;
      this.mCurrentPosition = i + Math.round((this.mFinal - i) * param1Float);
    }
    
    private static float getDeceleration(int param1Int) {
      float f;
      if (param1Int > 0) {
        f = -2000.0F;
      } else {
        f = 2000.0F;
      } 
      return f;
    }
    
    private void adjustDuration(int param1Int1, int param1Int2, int param1Int3) {
      float f = Math.abs((param1Int3 - param1Int1) / (param1Int2 - param1Int1));
      param1Int1 = (int)(f * 100.0F);
      if (param1Int1 < 100) {
        float f1 = param1Int1 / 100.0F;
        float f2 = (param1Int1 + 1) / 100.0F;
        float arrayOfFloat[] = SPLINE_TIME, f3 = arrayOfFloat[param1Int1];
        float f4 = arrayOfFloat[param1Int1 + 1];
        f1 = (f - f1) / (f2 - f1);
        this.mDuration = (int)(this.mDuration * (f1 * (f4 - f3) + f3));
      } 
    }
    
    void startScroll(int param1Int1, int param1Int2, int param1Int3) {
      this.mFinished = false;
      this.mCurrentPosition = param1Int1;
      this.mStart = param1Int1;
      this.mFinal = param1Int1 + param1Int2;
      this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
      this.mDuration = param1Int3;
      this.mDeceleration = 0.0F;
      this.mVelocity = 0;
    }
    
    void finish() {
      this.mCurrentPosition = this.mFinal;
      sTimeIncrease = 1.0F;
      this.mFinished = true;
    }
    
    void setFinalPosition(int param1Int) {
      this.mFinal = param1Int;
      this.mFinished = false;
    }
    
    void extendDuration(int param1Int) {
      long l = AnimationUtils.currentAnimationTimeMillis();
      int i = (int)(l - this.mStartTime);
      this.mDuration = i + param1Int;
      this.mFinished = false;
    }
    
    boolean springback(int param1Int1, int param1Int2, int param1Int3) {
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("springback start=");
        stringBuilder.append(param1Int1);
        stringBuilder.append(", min=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(", max=");
        stringBuilder.append(param1Int3);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      this.mFinished = true;
      this.mCurrentPosition = param1Int1;
      this.mStart = param1Int1;
      this.mFinal = param1Int1;
      this.mVelocity = 0;
      this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
      this.mDuration = 0;
      if (param1Int1 < param1Int2) {
        startSpringback(param1Int1, param1Int2, 0);
      } else if (param1Int1 > param1Int3) {
        startSpringback(param1Int1, param1Int3, 0);
      } 
      return true ^ this.mFinished;
    }
    
    private void startSpringback(int param1Int1, int param1Int2, int param1Int3) {
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("startSpringback start=");
        stringBuilder.append(param1Int1);
        stringBuilder.append(", end=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(", velocity=");
        stringBuilder.append(param1Int3);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      this.mOplusCount = 1;
      this.mFinished = false;
      this.mState = 1;
      this.mCurrentPosition = param1Int1;
      this.mStart = param1Int1;
      this.mFinal = param1Int2;
      param1Int1 -= param1Int2;
      this.mDeceleration = getDeceleration(param1Int1);
      this.mVelocity = -param1Int1;
      this.mOver = Math.abs(param1Int1);
      this.mDuration = (int)(Math.sqrt(param1Int1 * -2.0D / this.mDeceleration) * 1000.0D);
    }
    
    void fling(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) {
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("fling start=");
        stringBuilder.append(param1Int1);
        stringBuilder.append(", velocity=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(", min=");
        stringBuilder.append(param1Int3);
        stringBuilder.append(", max=");
        stringBuilder.append(param1Int4);
        stringBuilder.append(", over=");
        stringBuilder.append(param1Int5);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      this.mOplusCount = 1;
      this.mOver = param1Int5;
      this.mFinished = false;
      this.mReboundFriction = frictionFromOrigamiValue(this.mOrigamiFriction);
      this.mCurrVelocity = param1Int2;
      this.mVelocity = param1Int2;
      this.mDuration = 0;
      this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
      this.mCurrentPosition = param1Int1;
      this.mStart = param1Int1;
      this.mStartV = param1Int2;
      this.mLastPosition = param1Int1;
      this.mOverSpring = false;
      if (param1Int1 > param1Int4 || param1Int1 < param1Int3) {
        this.mOverSpring = true;
        startAfterEdge(param1Int1, param1Int3, param1Int4, param1Int2);
        return;
      } 
      this.mState = 0;
      double d = 0.0D;
      if (param1Int2 != 0) {
        this.mDuration = getSplineFlingDuration(param1Int2) + 100;
        d = getSplineFlingDistance(param1Int2);
        this.mEndValue = d;
        this.mLastVelocity = param1Int2;
      } 
      int i = (int)(Math.signum(param1Int2) * d);
      this.mFinal = param1Int1 = i + param1Int1;
      if (param1Int1 < param1Int3)
        this.mFinal = param1Int3; 
      if (this.mFinal > param1Int4)
        this.mFinal = param1Int4; 
      if (param1Int5 != 0 && !this.mIsScrollList) {
        this.mFinal = param1Int1 = this.mStart;
        if (param1Int1 > OplusOverScroller.sOverscrollDistance || this.mFinal < -OplusOverScroller.sOverscrollDistance) {
          float f = Math.signum(param1Int2);
          this.mFinal = (int)f * OplusOverScroller.sOverscrollDistance;
        } 
        this.mStart = 0;
        this.mSplineState = 3;
        this.mState = 2;
      } 
    }
    
    private double getSplineDeceleration(int param1Int) {
      return Math.log((Math.abs(param1Int) * 0.35F / this.mPhysicalCoeff * 0.006F));
    }
    
    private double getSplineFlingDistance(int param1Int) {
      double d1 = getSplineDeceleration(param1Int);
      float f = DECELERATION_RATE;
      double d2 = f;
      return (this.mFlingFriction * this.mPhysicalCoeff) * Math.exp(f / (d2 - 1.0D) * d1);
    }
    
    private int getSplineFlingDuration(int param1Int) {
      double d1 = getSplineDeceleration(param1Int);
      double d2 = DECELERATION_RATE;
      return (int)(Math.exp(d1 / (d2 - 1.0D)) * 1000.0D);
    }
    
    private void fitOnBounceCurve(int param1Int1, int param1Int2, int param1Int3) {
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("fitOnBounceCurve() start=");
        stringBuilder.append(param1Int1);
        stringBuilder.append(", end=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(", velocity=");
        stringBuilder.append(param1Int3);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      float f1 = -param1Int3, f2 = this.mDeceleration;
      f1 /= f2;
      f2 = (param1Int3 * param1Int3) / 2.0F / Math.abs(f2);
      float f3 = Math.abs(param1Int2 - param1Int1);
      double d = (f2 + f3);
      f2 = this.mDeceleration;
      d = d * 2.0D / Math.abs(f2);
      f2 = (float)Math.sqrt(d);
      this.mStartTime -= (int)((f2 - f1) * 1000.0F);
      this.mStart = param1Int2;
      this.mVelocity = (int)(-this.mDeceleration * f2);
    }
    
    private void startBounceAfterEdge(int param1Int1, int param1Int2, int param1Int3) {
      int i;
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("startBounceAfterEdge() start=");
        stringBuilder.append(param1Int1);
        stringBuilder.append(", end=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(", velocity=");
        stringBuilder.append(param1Int3);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      this.mScrollerDistance = param1Int1;
      if (param1Int3 == 0) {
        i = param1Int1 - param1Int2;
      } else {
        i = param1Int3;
      } 
      this.mDeceleration = getDeceleration(i);
      fitOnBounceCurve(param1Int1, param1Int2, param1Int3);
      onEdgeReached();
    }
    
    private void startAfterEdge(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      boolean bool2;
      int i;
      boolean bool1 = true;
      if (param1Int1 > param1Int2 && param1Int1 < param1Int3) {
        Log.e("OverScroller", "startAfterEdge called from a valid position");
        this.mFinished = true;
        return;
      } 
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("startAfterEdge() start=");
        stringBuilder.append(param1Int1);
        stringBuilder.append(", min=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(", max=");
        stringBuilder.append(param1Int3);
        stringBuilder.append(", velocity=");
        stringBuilder.append(param1Int4);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      if (param1Int1 > param1Int3) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool2) {
        i = param1Int3;
      } else {
        i = param1Int2;
      } 
      int j = param1Int1 - i;
      if (j * param1Int4 < 0)
        bool1 = false; 
      if (bool1) {
        startBounceAfterEdge(param1Int1, i, param1Int4);
      } else {
        double d = getSplineFlingDistance(param1Int4);
        if (d > Math.abs(j)) {
          if (!bool2)
            param1Int2 = param1Int1; 
          if (bool2)
            param1Int3 = param1Int1; 
          fling(param1Int1, param1Int4, param1Int2, param1Int3, this.mOver);
        } else {
          startSpringback(param1Int1, i, param1Int4);
        } 
      } 
    }
    
    void notifyEdgeReached(int param1Int1, int param1Int2, int param1Int3) {
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("notifyEdgeReached() start=");
        stringBuilder.append(param1Int1);
        stringBuilder.append(", end=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(", over=");
        stringBuilder.append(param1Int3);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      if (this.mState == 0) {
        this.mOver = param1Int3;
        this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
        this.mState = 1;
        startAfterEdge(param1Int1, param1Int2, param1Int2, (int)(this.mCurrVelocity / 1000.0F));
      } 
    }
    
    private void onEdgeReached() {
      float f1 = Math.signum(this.mVelocity);
      int i = this.mVelocity;
      float f2 = (i * i) / 1600.0F;
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onEdgeReached() mVelocity=");
        stringBuilder.append(this.mVelocity);
        stringBuilder.append(", distance=");
        stringBuilder.append(f2);
        stringBuilder.append(", mOver=");
        stringBuilder.append(this.mOver);
        stringBuilder.append(", mDeceleration=");
        stringBuilder.append(this.mDeceleration);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      int j = this.mOver;
      float f3 = f2;
      if (f2 > j) {
        f3 = -f1;
        i = this.mVelocity;
        this.mDeceleration = f3 * i * i / j * 2.0F;
        f3 = j;
      } 
      this.mOplusCount = 1;
      this.mOver = (int)f3;
      this.mState = 2;
      i = this.mStart;
      if (this.mVelocity <= 0)
        f3 = -f3; 
      this.mFinal = i + (int)f3;
      if (this.mVelocity > 0) {
        i = -800;
      } else {
        i = 800;
      } 
      this.mDuration = -(this.mVelocity * 1000 / i);
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onEdgeReached() mFinal=");
        stringBuilder.append(this.mFinal);
        stringBuilder.append(", mDuration=");
        stringBuilder.append(this.mDuration);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      this.mSplineState = 4;
      this.mOverSplineStart = true;
    }
    
    boolean continueWhenFinished() {
      if (OplusOverScroller.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("continueWhenFinished mState=");
        stringBuilder.append(this.mState);
        Log.d("OplusOverScroller", stringBuilder.toString());
      } 
      int i = this.mState;
      if (i != 0) {
        if (i != 1) {
          if (i == 2) {
            this.mStartTime += this.mDuration;
            startSpringback(this.mFinal, this.mStart, 0);
          } 
        } else {
          return false;
        } 
      } else {
        if (this.mIsScrollList && this.mOver != 0) {
          this.mCurrentPosition = i = this.mFinal;
          this.mStart = i;
          this.mVelocity = (int)this.mCurrVelocity / 10;
          this.mScrollerDistance = 0;
          onEdgeReached();
          update();
          return true;
        } 
        return false;
      } 
      update();
      return true;
    }
    
    boolean update() {
      // Byte code:
      //   0: invokestatic currentAnimationTimeMillis : ()J
      //   3: pop2
      //   4: aload_0
      //   5: getfield mStartTime : J
      //   8: lstore_1
      //   9: aload_0
      //   10: getfield mOplusCount : I
      //   13: i2f
      //   14: ldc_w 16.0
      //   17: fmul
      //   18: f2i
      //   19: istore_3
      //   20: invokestatic access$1000 : ()Z
      //   23: ifeq -> 104
      //   26: new java/lang/StringBuilder
      //   29: dup
      //   30: invokespecial <init> : ()V
      //   33: astore #4
      //   35: aload #4
      //   37: ldc_w 'update() mState='
      //   40: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   43: pop
      //   44: aload #4
      //   46: aload_0
      //   47: getfield mState : I
      //   50: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   53: pop
      //   54: aload #4
      //   56: ldc_w ', mOplusCount='
      //   59: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   62: pop
      //   63: aload #4
      //   65: aload_0
      //   66: getfield mOplusCount : I
      //   69: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   72: pop
      //   73: aload #4
      //   75: ldc_w ', mCurrentPosition='
      //   78: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   81: pop
      //   82: aload #4
      //   84: aload_0
      //   85: getfield mCurrentPosition : I
      //   88: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   91: pop
      //   92: ldc_w 'OplusOverScroller'
      //   95: aload #4
      //   97: invokevirtual toString : ()Ljava/lang/String;
      //   100: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   103: pop
      //   104: dconst_0
      //   105: dstore #5
      //   107: aload_0
      //   108: getfield mState : I
      //   111: istore #7
      //   113: iload #7
      //   115: ifeq -> 931
      //   118: iload #7
      //   120: iconst_1
      //   121: if_icmpeq -> 719
      //   124: iload #7
      //   126: iconst_2
      //   127: if_icmpeq -> 133
      //   130: goto -> 904
      //   133: aload_0
      //   134: getfield mSplineState : I
      //   137: iconst_4
      //   138: if_icmpne -> 709
      //   141: aload_0
      //   142: getfield mIsScrollList : Z
      //   145: ifeq -> 161
      //   148: aload_0
      //   149: getfield mOverSpring : Z
      //   152: ifne -> 158
      //   155: goto -> 161
      //   158: goto -> 709
      //   161: iload_3
      //   162: i2f
      //   163: fstore #8
      //   165: fconst_1
      //   166: aload_0
      //   167: getfield mDuration : I
      //   170: i2f
      //   171: fdiv
      //   172: fstore #9
      //   174: fload #8
      //   176: fload #9
      //   178: fmul
      //   179: ldc 14.0
      //   181: invokestatic viscousFluid : (FF)F
      //   184: fstore #9
      //   186: aload_0
      //   187: getfield mFinal : I
      //   190: aload_0
      //   191: getfield mStart : I
      //   194: isub
      //   195: i2f
      //   196: fload #9
      //   198: fmul
      //   199: f2d
      //   200: dstore #10
      //   202: fload #9
      //   204: fstore #8
      //   206: dload #10
      //   208: dstore #5
      //   210: invokestatic access$1000 : ()Z
      //   213: ifeq -> 317
      //   216: new java/lang/StringBuilder
      //   219: dup
      //   220: invokespecial <init> : ()V
      //   223: astore #4
      //   225: aload #4
      //   227: ldc_w 'update mSplineState == OVER_SPLINE x='
      //   230: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   233: pop
      //   234: aload #4
      //   236: fload #9
      //   238: invokevirtual append : (F)Ljava/lang/StringBuilder;
      //   241: pop
      //   242: aload #4
      //   244: ldc_w ', distance='
      //   247: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   250: pop
      //   251: aload #4
      //   253: dload #10
      //   255: invokevirtual append : (D)Ljava/lang/StringBuilder;
      //   258: pop
      //   259: aload #4
      //   261: ldc_w ', mFinal='
      //   264: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   267: pop
      //   268: aload #4
      //   270: aload_0
      //   271: getfield mFinal : I
      //   274: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   277: pop
      //   278: aload #4
      //   280: ldc_w ', mScrollerDistance='
      //   283: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   286: pop
      //   287: aload #4
      //   289: aload_0
      //   290: getfield mScrollerDistance : I
      //   293: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   296: pop
      //   297: ldc_w 'OplusOverScroller'
      //   300: aload #4
      //   302: invokevirtual toString : ()Ljava/lang/String;
      //   305: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   308: pop
      //   309: dload #10
      //   311: dstore #5
      //   313: fload #9
      //   315: fstore #8
      //   317: aload_0
      //   318: getfield mFinal : I
      //   321: ifge -> 335
      //   324: dload #5
      //   326: aload_0
      //   327: getfield mScrollerDistance : I
      //   330: i2d
      //   331: dcmpl
      //   332: ifge -> 353
      //   335: aload_0
      //   336: getfield mFinal : I
      //   339: ifle -> 492
      //   342: dload #5
      //   344: aload_0
      //   345: getfield mScrollerDistance : I
      //   348: i2d
      //   349: dcmpg
      //   350: ifgt -> 492
      //   353: aload_0
      //   354: getfield mIsScrollList : Z
      //   357: ifne -> 492
      //   360: aload_0
      //   361: getfield mOplusCount : I
      //   364: iconst_1
      //   365: iadd
      //   366: istore_3
      //   367: aload_0
      //   368: iload_3
      //   369: putfield mOplusCount : I
      //   372: iload_3
      //   373: i2f
      //   374: ldc_w 16.0
      //   377: fmul
      //   378: f2i
      //   379: istore_3
      //   380: iload_3
      //   381: i2f
      //   382: fstore #8
      //   384: fconst_1
      //   385: aload_0
      //   386: getfield mDuration : I
      //   389: i2f
      //   390: fdiv
      //   391: fstore #9
      //   393: fload #8
      //   395: fload #9
      //   397: fmul
      //   398: ldc 14.0
      //   400: invokestatic viscousFluid : (FF)F
      //   403: fstore #8
      //   405: aload_0
      //   406: getfield mFinal : I
      //   409: aload_0
      //   410: getfield mStart : I
      //   413: isub
      //   414: i2f
      //   415: fload #8
      //   417: fmul
      //   418: f2d
      //   419: dstore #5
      //   421: invokestatic access$1000 : ()Z
      //   424: ifeq -> 484
      //   427: new java/lang/StringBuilder
      //   430: dup
      //   431: invokespecial <init> : ()V
      //   434: astore #4
      //   436: aload #4
      //   438: ldc_w 'update while mOplusCount='
      //   441: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   444: pop
      //   445: aload #4
      //   447: aload_0
      //   448: getfield mOplusCount : I
      //   451: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   454: pop
      //   455: aload #4
      //   457: ldc_w ', distance='
      //   460: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   463: pop
      //   464: aload #4
      //   466: dload #5
      //   468: invokevirtual append : (D)Ljava/lang/StringBuilder;
      //   471: pop
      //   472: ldc_w 'OplusOverScroller'
      //   475: aload #4
      //   477: invokevirtual toString : ()Ljava/lang/String;
      //   480: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   483: pop
      //   484: aload_0
      //   485: iconst_1
      //   486: putfield mOverSplineStart : Z
      //   489: goto -> 317
      //   492: aload_0
      //   493: getfield mOverSplineStart : Z
      //   496: ifeq -> 634
      //   499: aload_0
      //   500: getfield mOplusCount : I
      //   503: iconst_1
      //   504: iadd
      //   505: i2f
      //   506: ldc_w 16.0
      //   509: fmul
      //   510: f2i
      //   511: istore_3
      //   512: iload_3
      //   513: i2f
      //   514: fstore #9
      //   516: fconst_1
      //   517: aload_0
      //   518: getfield mDuration : I
      //   521: i2f
      //   522: fdiv
      //   523: fstore #8
      //   525: fload #9
      //   527: fload #8
      //   529: fmul
      //   530: ldc 14.0
      //   532: invokestatic viscousFluid : (FF)F
      //   535: fstore #8
      //   537: aload_0
      //   538: getfield mFinal : I
      //   541: istore_3
      //   542: iload_3
      //   543: aload_0
      //   544: getfield mStart : I
      //   547: isub
      //   548: i2f
      //   549: fload #8
      //   551: fmul
      //   552: f2d
      //   553: dstore #12
      //   555: iload_3
      //   556: ifge -> 576
      //   559: dload #12
      //   561: dload #5
      //   563: dsub
      //   564: dload #5
      //   566: aload_0
      //   567: getfield mScrollerDistance : I
      //   570: i2d
      //   571: dsub
      //   572: dcmpg
      //   573: iflt -> 608
      //   576: dload #5
      //   578: dstore #10
      //   580: aload_0
      //   581: getfield mFinal : I
      //   584: ifle -> 622
      //   587: dload #5
      //   589: dstore #10
      //   591: dload #12
      //   593: dload #5
      //   595: dsub
      //   596: dload #5
      //   598: aload_0
      //   599: getfield mScrollerDistance : I
      //   602: i2d
      //   603: dsub
      //   604: dcmpl
      //   605: ifle -> 622
      //   608: dload #12
      //   610: dstore #10
      //   612: aload_0
      //   613: aload_0
      //   614: getfield mOplusCount : I
      //   617: iconst_1
      //   618: iadd
      //   619: putfield mOplusCount : I
      //   622: aload_0
      //   623: iconst_0
      //   624: putfield mOverSplineStart : Z
      //   627: dload #10
      //   629: dstore #5
      //   631: goto -> 634
      //   634: aload_0
      //   635: getfield mFinal : I
      //   638: istore_3
      //   639: iload_3
      //   640: ifge -> 651
      //   643: aload_0
      //   644: getfield mCurrentPosition : I
      //   647: iload_3
      //   648: if_icmple -> 692
      //   651: aload_0
      //   652: getfield mFinal : I
      //   655: istore_3
      //   656: iload_3
      //   657: ifle -> 668
      //   660: aload_0
      //   661: getfield mCurrentPosition : I
      //   664: iload_3
      //   665: if_icmpge -> 692
      //   668: fload #8
      //   670: ldc 0.91
      //   672: fcmpl
      //   673: ifgt -> 692
      //   676: dload #5
      //   678: invokestatic round : (D)J
      //   681: lconst_0
      //   682: lcmp
      //   683: ifne -> 689
      //   686: goto -> 692
      //   689: goto -> 904
      //   692: aload_0
      //   693: aload_0
      //   694: getfield mStart : I
      //   697: dload #5
      //   699: invokestatic round : (D)J
      //   702: l2i
      //   703: iadd
      //   704: putfield mFinal : I
      //   707: iconst_0
      //   708: ireturn
      //   709: aload_0
      //   710: aload_0
      //   711: getfield mCurrentPosition : I
      //   714: putfield mFinal : I
      //   717: iconst_0
      //   718: ireturn
      //   719: iload_3
      //   720: i2f
      //   721: fstore #8
      //   723: aload_0
      //   724: getfield mFlingInterpolator : Landroid/view/animation/PathInterpolator;
      //   727: fload #8
      //   729: ldc_w 0.0016666667
      //   732: fmul
      //   733: fconst_1
      //   734: invokestatic min : (FF)F
      //   737: invokevirtual getInterpolation : (F)F
      //   740: fstore #8
      //   742: aload_0
      //   743: getfield mFinal : I
      //   746: istore_3
      //   747: aload_0
      //   748: getfield mStart : I
      //   751: istore #7
      //   753: iload_3
      //   754: iload #7
      //   756: isub
      //   757: i2f
      //   758: fload #8
      //   760: fmul
      //   761: f2d
      //   762: dstore #5
      //   764: aload_0
      //   765: iload #7
      //   767: dload #5
      //   769: invokestatic round : (D)J
      //   772: l2i
      //   773: iadd
      //   774: putfield mCurrentPosition : I
      //   777: invokestatic access$1000 : ()Z
      //   780: ifeq -> 876
      //   783: new java/lang/StringBuilder
      //   786: dup
      //   787: invokespecial <init> : ()V
      //   790: astore #4
      //   792: aload #4
      //   794: ldc_w 'update CUBIC x='
      //   797: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   800: pop
      //   801: aload #4
      //   803: fload #8
      //   805: invokevirtual append : (F)Ljava/lang/StringBuilder;
      //   808: pop
      //   809: aload #4
      //   811: ldc_w ', distance='
      //   814: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   817: pop
      //   818: aload #4
      //   820: dload #5
      //   822: invokevirtual append : (D)Ljava/lang/StringBuilder;
      //   825: pop
      //   826: aload #4
      //   828: ldc_w ', mFinal='
      //   831: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   834: pop
      //   835: aload #4
      //   837: aload_0
      //   838: getfield mFinal : I
      //   841: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   844: pop
      //   845: aload #4
      //   847: ldc_w ', mCurrentPosition='
      //   850: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   853: pop
      //   854: aload #4
      //   856: aload_0
      //   857: getfield mCurrentPosition : I
      //   860: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   863: pop
      //   864: ldc_w 'OplusOverScroller'
      //   867: aload #4
      //   869: invokevirtual toString : ()Ljava/lang/String;
      //   872: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   875: pop
      //   876: aload_0
      //   877: getfield mCurrentPosition : I
      //   880: istore #7
      //   882: aload_0
      //   883: getfield mFinal : I
      //   886: istore_3
      //   887: iload #7
      //   889: iload_3
      //   890: if_icmpne -> 904
      //   893: aload_0
      //   894: iload_3
      //   895: putfield mCurrentPosition : I
      //   898: aload_0
      //   899: invokevirtual finish : ()V
      //   902: iconst_0
      //   903: ireturn
      //   904: aload_0
      //   905: aload_0
      //   906: getfield mOplusCount : I
      //   909: iconst_1
      //   910: iadd
      //   911: putfield mOplusCount : I
      //   914: aload_0
      //   915: aload_0
      //   916: getfield mStart : I
      //   919: dload #5
      //   921: invokestatic round : (D)J
      //   924: l2i
      //   925: iadd
      //   926: putfield mCurrentPosition : I
      //   929: iconst_1
      //   930: ireturn
      //   931: aload_0
      //   932: getfield mLastPosition : I
      //   935: i2d
      //   936: dstore #10
      //   938: aload_0
      //   939: getfield mLastVelocity : D
      //   942: dstore #5
      //   944: aload_0
      //   945: getfield mOplusCount : I
      //   948: bipush #60
      //   950: if_icmpge -> 978
      //   953: getstatic android/widget/OplusOverScroller$OplusSplineOverScroller.sTimeIncrease : F
      //   956: ldc_w 0.020000001
      //   959: fadd
      //   960: putstatic android/widget/OplusOverScroller$OplusSplineOverScroller.sTimeIncrease : F
      //   963: aload_0
      //   964: aload_0
      //   965: getfield mReboundFriction : F
      //   968: ldc_w 0.020000001
      //   971: fadd
      //   972: putfield mReboundFriction : F
      //   975: goto -> 1020
      //   978: getstatic android/widget/OplusOverScroller$OplusSplineOverScroller.sTimeIncrease : F
      //   981: fstore #8
      //   983: fload #8
      //   985: fload #8
      //   987: ldc 0.6
      //   989: fsub
      //   990: ldc_w 60.0
      //   993: fdiv
      //   994: fsub
      //   995: fstore #8
      //   997: fload #8
      //   999: putstatic android/widget/OplusOverScroller$OplusSplineOverScroller.sTimeIncrease : F
      //   1002: aload_0
      //   1003: aload_0
      //   1004: getfield mReboundFriction : F
      //   1007: fload #8
      //   1009: ldc 0.6
      //   1011: fsub
      //   1012: ldc_w 60.0
      //   1015: fdiv
      //   1016: fsub
      //   1017: putfield mReboundFriction : F
      //   1020: aload_0
      //   1021: getfield mReboundTension : F
      //   1024: fstore #8
      //   1026: fload #8
      //   1028: f2d
      //   1029: dstore #14
      //   1031: aload_0
      //   1032: getfield mEndValue : D
      //   1035: dstore #12
      //   1037: aload_0
      //   1038: getfield mReboundFriction : F
      //   1041: fstore #9
      //   1043: dload #14
      //   1045: dload #12
      //   1047: dconst_0
      //   1048: dsub
      //   1049: dmul
      //   1050: fload #9
      //   1052: f2d
      //   1053: aload_0
      //   1054: getfield mLastVelocity : D
      //   1057: dmul
      //   1058: dsub
      //   1059: dstore #14
      //   1061: dload #5
      //   1063: ldc2_w 0.016
      //   1066: dmul
      //   1067: ldc2_w 2.0
      //   1070: ddiv
      //   1071: dstore #16
      //   1073: dload #5
      //   1075: dload #14
      //   1077: ldc2_w 0.016
      //   1080: dmul
      //   1081: ldc2_w 2.0
      //   1084: ddiv
      //   1085: dadd
      //   1086: dstore #18
      //   1088: fload #8
      //   1090: f2d
      //   1091: dload #12
      //   1093: dload #16
      //   1095: dload #10
      //   1097: dadd
      //   1098: dsub
      //   1099: dmul
      //   1100: fload #9
      //   1102: f2d
      //   1103: dload #18
      //   1105: dmul
      //   1106: dsub
      //   1107: dstore #16
      //   1109: dload #18
      //   1111: ldc2_w 0.016
      //   1114: dmul
      //   1115: ldc2_w 2.0
      //   1118: ddiv
      //   1119: dstore #20
      //   1121: dload #5
      //   1123: dload #16
      //   1125: ldc2_w 0.016
      //   1128: dmul
      //   1129: ldc2_w 2.0
      //   1132: ddiv
      //   1133: dadd
      //   1134: dstore #18
      //   1136: fload #8
      //   1138: f2d
      //   1139: dload #12
      //   1141: dload #10
      //   1143: dload #20
      //   1145: dadd
      //   1146: dsub
      //   1147: dmul
      //   1148: fload #9
      //   1150: f2d
      //   1151: dload #18
      //   1153: dmul
      //   1154: dsub
      //   1155: dstore #22
      //   1157: dload #5
      //   1159: dload #22
      //   1161: ldc2_w 0.016
      //   1164: dmul
      //   1165: dadd
      //   1166: dstore #24
      //   1168: fload #8
      //   1170: f2d
      //   1171: dstore #26
      //   1173: fload #9
      //   1175: f2d
      //   1176: dstore #20
      //   1178: dload #5
      //   1180: dload #14
      //   1182: dload #16
      //   1184: dload #22
      //   1186: dadd
      //   1187: ldc2_w 2.0
      //   1190: dmul
      //   1191: dadd
      //   1192: dload #26
      //   1194: dload #12
      //   1196: dload #10
      //   1198: dload #18
      //   1200: ldc2_w 0.016
      //   1203: dmul
      //   1204: dadd
      //   1205: dsub
      //   1206: dmul
      //   1207: dload #20
      //   1209: dload #24
      //   1211: dmul
      //   1212: dsub
      //   1213: dadd
      //   1214: ldc2_w 0.06939999759197235
      //   1217: dmul
      //   1218: ldc2_w 0.016
      //   1221: dmul
      //   1222: dadd
      //   1223: d2f
      //   1224: fstore #9
      //   1226: fload #9
      //   1228: f2d
      //   1229: ldc2_w 0.016
      //   1232: dmul
      //   1233: dstore #5
      //   1235: dload #5
      //   1237: invokestatic abs : (D)D
      //   1240: aload_0
      //   1241: getfield mLastDetla : D
      //   1244: dcmpl
      //   1245: ifle -> 1262
      //   1248: aload_0
      //   1249: getfield mOplusCount : I
      //   1252: iconst_1
      //   1253: if_icmpgt -> 1259
      //   1256: goto -> 1262
      //   1259: goto -> 1290
      //   1262: aload_0
      //   1263: getfield mCurrVelocity : F
      //   1266: fstore #8
      //   1268: aload_0
      //   1269: getfield mRestThreshold : I
      //   1272: istore_3
      //   1273: fload #8
      //   1275: iload_3
      //   1276: ineg
      //   1277: i2f
      //   1278: fcmpl
      //   1279: ifle -> 1304
      //   1282: fload #8
      //   1284: iload_3
      //   1285: i2f
      //   1286: fcmpg
      //   1287: ifge -> 1304
      //   1290: aload_0
      //   1291: aload_0
      //   1292: getfield mCurrentPosition : I
      //   1295: putfield mFinal : I
      //   1298: aload_0
      //   1299: invokevirtual finish : ()V
      //   1302: iconst_0
      //   1303: ireturn
      //   1304: dload #5
      //   1306: invokestatic round : (D)J
      //   1309: l2i
      //   1310: istore #7
      //   1312: iload #7
      //   1314: istore_3
      //   1315: iload #7
      //   1317: ifne -> 1330
      //   1320: dload #5
      //   1322: invokestatic abs : (D)D
      //   1325: dload #5
      //   1327: ddiv
      //   1328: d2i
      //   1329: istore_3
      //   1330: aload_0
      //   1331: aload_0
      //   1332: getfield mLastPosition : I
      //   1335: iload_3
      //   1336: iadd
      //   1337: putfield mCurrentPosition : I
      //   1340: aload_0
      //   1341: dload #5
      //   1343: invokestatic abs : (D)D
      //   1346: putfield mLastDetla : D
      //   1349: aload_0
      //   1350: getfield mCurrentPosition : I
      //   1353: istore #7
      //   1355: aload_0
      //   1356: iload #7
      //   1358: putfield mLastPosition : I
      //   1361: aload_0
      //   1362: aload_0
      //   1363: getfield mOplusCount : I
      //   1366: iconst_1
      //   1367: iadd
      //   1368: putfield mOplusCount : I
      //   1371: aload_0
      //   1372: fload #9
      //   1374: putfield mCurrVelocity : F
      //   1377: aload_0
      //   1378: fload #9
      //   1380: f2d
      //   1381: putfield mLastVelocity : D
      //   1384: aload_0
      //   1385: getfield mIsScrollList : Z
      //   1388: ifeq -> 1429
      //   1391: iload_3
      //   1392: ifle -> 1404
      //   1395: iload #7
      //   1397: aload_0
      //   1398: getfield mFinal : I
      //   1401: if_icmpge -> 1419
      //   1404: iload_3
      //   1405: ifge -> 1429
      //   1408: aload_0
      //   1409: getfield mCurrentPosition : I
      //   1412: aload_0
      //   1413: getfield mFinal : I
      //   1416: if_icmpgt -> 1429
      //   1419: aload_0
      //   1420: aload_0
      //   1421: getfield mFinal : I
      //   1424: putfield mCurrentPosition : I
      //   1427: iconst_0
      //   1428: ireturn
      //   1429: iconst_1
      //   1430: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1136	-> 0
      //   #1137	-> 4
      //   #1138	-> 9
      //   #1144	-> 20
      //   #1145	-> 26
      //   #1150	-> 104
      //   #1151	-> 107
      //   #1152	-> 107
      //   #1246	-> 133
      //   #1247	-> 161
      //   #1248	-> 174
      //   #1249	-> 186
      //   #1250	-> 202
      //   #1251	-> 216
      //   #1256	-> 317
      //   #1259	-> 360
      //   #1260	-> 372
      //   #1262	-> 380
      //   #1263	-> 393
      //   #1264	-> 405
      //   #1265	-> 421
      //   #1266	-> 427
      //   #1270	-> 484
      //   #1273	-> 492
      //   #1274	-> 499
      //   #1275	-> 512
      //   #1277	-> 525
      //   #1278	-> 537
      //   #1279	-> 555
      //   #1281	-> 608
      //   #1282	-> 612
      //   #1284	-> 622
      //   #1273	-> 634
      //   #1287	-> 634
      //   #1288	-> 676
      //   #1290	-> 692
      //   #1291	-> 707
      //   #1246	-> 709
      //   #1295	-> 709
      //   #1296	-> 717
      //   #1301	-> 719
      //   #1308	-> 723
      //   #1311	-> 742
      //   #1312	-> 764
      //   #1313	-> 777
      //   #1314	-> 783
      //   #1319	-> 876
      //   #1320	-> 893
      //   #1321	-> 898
      //   #1322	-> 902
      //   #1327	-> 904
      //   #1328	-> 914
      //   #1330	-> 929
      //   #1164	-> 931
      //   #1165	-> 938
      //   #1166	-> 944
      //   #1167	-> 944
      //   #1168	-> 944
      //   #1169	-> 944
      //   #1170	-> 944
      //   #1171	-> 944
      //   #1172	-> 944
      //   #1173	-> 944
      //   #1174	-> 944
      //   #1175	-> 944
      //   #1176	-> 944
      //   #1177	-> 944
      //   #1179	-> 944
      //   #1180	-> 953
      //   #1181	-> 963
      //   #1183	-> 978
      //   #1184	-> 1002
      //   #1187	-> 1020
      //   #1188	-> 1020
      //   #1190	-> 1061
      //   #1191	-> 1073
      //   #1192	-> 1088
      //   #1193	-> 1088
      //   #1195	-> 1109
      //   #1196	-> 1121
      //   #1197	-> 1136
      //   #1198	-> 1136
      //   #1200	-> 1157
      //   #1201	-> 1157
      //   #1202	-> 1168
      //   #1203	-> 1168
      //   #1206	-> 1178
      //   #1207	-> 1178
      //   #1209	-> 1178
      //   #1210	-> 1178
      //   #1212	-> 1178
      //   #1213	-> 1226
      //   #1216	-> 1235
      //   #1218	-> 1290
      //   #1219	-> 1298
      //   #1220	-> 1302
      //   #1222	-> 1304
      //   #1223	-> 1312
      //   #1224	-> 1320
      //   #1226	-> 1330
      //   #1228	-> 1340
      //   #1229	-> 1349
      //   #1230	-> 1361
      //   #1231	-> 1371
      //   #1232	-> 1377
      //   #1236	-> 1384
      //   #1238	-> 1419
      //   #1239	-> 1427
      //   #1242	-> 1429
    }
    
    private static float viscousFluid(float param1Float1, float param1Float2) {
      float f = (1.0F - (float)Math.log((1.0F / (1.0F - 0.36787945F)))) / param1Float2;
      param1Float1 = (float)Math.exp((1.0F - (f + param1Float1) * param1Float2));
      param1Float2 = sViscousFluidNormalize;
      return ((1.0F - 0.36787945F) * (1.0F - param1Float1) + 0.36787945F) * param1Float2;
    }
    
    private float frictionFromOrigamiValue(float param1Float) {
      float f = 0.0F;
      if (param1Float == 0.0F) {
        param1Float = f;
      } else {
        param1Float = (param1Float - 8.0F) * 3.0F + 25.0F;
      } 
      return param1Float;
    }
  }
}
