package android.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R;

public class Spinner extends AbsSpinner implements DialogInterface.OnClickListener {
  private static final int MAX_ITEMS_MEASURED = 15;
  
  public static final int MODE_DIALOG = 0;
  
  public static final int MODE_DROPDOWN = 1;
  
  private static final int MODE_THEME = -1;
  
  private static final String TAG = "Spinner";
  
  private boolean mDisableChildrenWhenDisabled;
  
  int mDropDownWidth;
  
  private ForwardingListener mForwardingListener;
  
  private int mGravity;
  
  private SpinnerPopup mPopup;
  
  private final Context mPopupContext;
  
  private SpinnerAdapter mTempAdapter;
  
  private final Rect mTempRect = new Rect();
  
  public Spinner(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public Spinner(Context paramContext, int paramInt) {
    this(paramContext, (AttributeSet)null, 16842881, paramInt);
  }
  
  public Spinner(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842881);
  }
  
  public Spinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0, -1);
  }
  
  public Spinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    this(paramContext, paramAttributeSet, paramInt1, 0, paramInt2);
  }
  
  public Spinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2, int paramInt3) {
    this(paramContext, paramAttributeSet, paramInt1, paramInt2, paramInt3, (Resources.Theme)null);
  }
  
  public Spinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2, int paramInt3, Resources.Theme paramTheme) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Spinner, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.Spinner, paramAttributeSet, typedArray, paramInt1, paramInt2);
    if (paramTheme != null) {
      this.mPopupContext = (Context)new ContextThemeWrapper(paramContext, paramTheme);
    } else {
      int i = typedArray.getResourceId(7, 0);
      if (i != 0) {
        this.mPopupContext = (Context)new ContextThemeWrapper(paramContext, i);
      } else {
        this.mPopupContext = paramContext;
      } 
    } 
    if (paramInt3 == -1)
      paramInt3 = typedArray.getInt(5, 0); 
    if (paramInt3 != 0) {
      if (paramInt3 == 1) {
        DropdownPopup dropdownPopup = new DropdownPopup(this.mPopupContext, paramAttributeSet, paramInt1, paramInt2);
        TypedArray typedArray1 = this.mPopupContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Spinner, paramInt1, paramInt2);
        this.mDropDownWidth = typedArray1.getLayoutDimension(4, -2);
        if (typedArray1.hasValueOrEmpty(1))
          dropdownPopup.setListSelector(typedArray1.getDrawable(1)); 
        dropdownPopup.setBackgroundDrawable(typedArray1.getDrawable(2));
        dropdownPopup.setPromptText(typedArray.getString(3));
        typedArray1.recycle();
        this.mPopup = dropdownPopup;
        this.mForwardingListener = (ForwardingListener)new Object(this, this, dropdownPopup);
      } 
    } else {
      DialogPopup dialogPopup = new DialogPopup();
      dialogPopup.setPromptText(typedArray.getString(3));
    } 
    this.mGravity = typedArray.getInt(0, 17);
    this.mDisableChildrenWhenDisabled = typedArray.getBoolean(8, false);
    typedArray.recycle();
    SpinnerAdapter spinnerAdapter = this.mTempAdapter;
    if (spinnerAdapter != null) {
      setAdapter(spinnerAdapter);
      this.mTempAdapter = null;
    } 
  }
  
  public Context getPopupContext() {
    return this.mPopupContext;
  }
  
  public void setPopupBackgroundDrawable(Drawable paramDrawable) {
    SpinnerPopup spinnerPopup = this.mPopup;
    if (!(spinnerPopup instanceof DropdownPopup)) {
      Log.e("Spinner", "setPopupBackgroundDrawable: incompatible spinner mode; ignoring...");
      return;
    } 
    spinnerPopup.setBackgroundDrawable(paramDrawable);
  }
  
  public void setPopupBackgroundResource(int paramInt) {
    setPopupBackgroundDrawable(getPopupContext().getDrawable(paramInt));
  }
  
  public Drawable getPopupBackground() {
    return this.mPopup.getBackground();
  }
  
  public boolean isPopupShowing() {
    boolean bool;
    SpinnerPopup spinnerPopup = this.mPopup;
    if (spinnerPopup != null && spinnerPopup.isShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setDropDownVerticalOffset(int paramInt) {
    this.mPopup.setVerticalOffset(paramInt);
  }
  
  public int getDropDownVerticalOffset() {
    return this.mPopup.getVerticalOffset();
  }
  
  public void setDropDownHorizontalOffset(int paramInt) {
    this.mPopup.setHorizontalOffset(paramInt);
  }
  
  public int getDropDownHorizontalOffset() {
    return this.mPopup.getHorizontalOffset();
  }
  
  public void setDropDownWidth(int paramInt) {
    if (!(this.mPopup instanceof DropdownPopup)) {
      Log.e("Spinner", "Cannot set dropdown width for MODE_DIALOG, ignoring");
      return;
    } 
    this.mDropDownWidth = paramInt;
  }
  
  public int getDropDownWidth() {
    return this.mDropDownWidth;
  }
  
  public void setEnabled(boolean paramBoolean) {
    super.setEnabled(paramBoolean);
    if (this.mDisableChildrenWhenDisabled) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++)
        getChildAt(b).setEnabled(paramBoolean); 
    } 
  }
  
  public void setGravity(int paramInt) {
    if (this.mGravity != paramInt) {
      int i = paramInt;
      if ((paramInt & 0x7) == 0)
        i = paramInt | 0x800003; 
      this.mGravity = i;
      requestLayout();
    } 
  }
  
  public int getGravity() {
    return this.mGravity;
  }
  
  public void setAdapter(SpinnerAdapter paramSpinnerAdapter) {
    if (this.mPopup == null) {
      this.mTempAdapter = paramSpinnerAdapter;
      return;
    } 
    super.setAdapter(paramSpinnerAdapter);
    this.mRecycler.clear();
    int i = (this.mContext.getApplicationInfo()).targetSdkVersion;
    if (i < 21 || paramSpinnerAdapter == null || 
      paramSpinnerAdapter.getViewTypeCount() == 1) {
      Context context1 = this.mPopupContext, context2 = context1;
      if (context1 == null)
        context2 = this.mContext; 
      this.mPopup.setAdapter(new DropDownAdapter(paramSpinnerAdapter, context2.getTheme()));
      return;
    } 
    throw new IllegalArgumentException("Spinner adapter view type count must be 1");
  }
  
  public int getBaseline() {
    View view2, view1 = null;
    if (getChildCount() > 0) {
      view2 = getChildAt(0);
    } else {
      view2 = view1;
      if (this.mAdapter != null) {
        view2 = view1;
        if (this.mAdapter.getCount() > 0) {
          view2 = makeView(0, false);
          this.mRecycler.put(0, view2);
        } 
      } 
    } 
    int i = -1;
    if (view2 != null) {
      int j = view2.getBaseline();
      if (j >= 0)
        i = view2.getTop() + j; 
      return i;
    } 
    return -1;
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    SpinnerPopup spinnerPopup = this.mPopup;
    if (spinnerPopup != null && spinnerPopup.isShowing())
      this.mPopup.dismiss(); 
  }
  
  public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener) {
    throw new RuntimeException("setOnItemClickListener cannot be used with a spinner.");
  }
  
  public void setOnItemClickListenerInt(AdapterView.OnItemClickListener paramOnItemClickListener) {
    super.setOnItemClickListener(paramOnItemClickListener);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    ForwardingListener forwardingListener = this.mForwardingListener;
    if (forwardingListener != null && forwardingListener.onTouch(this, paramMotionEvent))
      return true; 
    return super.onTouchEvent(paramMotionEvent);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    if (this.mPopup != null && View.MeasureSpec.getMode(paramInt1) == Integer.MIN_VALUE) {
      paramInt2 = getMeasuredWidth();
      int i = measureContentWidth(getAdapter(), getBackground());
      paramInt2 = Math.max(paramInt2, i);
      paramInt1 = View.MeasureSpec.getSize(paramInt1);
      paramInt2 = Math.min(paramInt2, paramInt1);
      paramInt1 = getMeasuredHeight();
      setMeasuredDimension(paramInt2, paramInt1);
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    this.mInLayout = true;
    layout(0, false);
    this.mInLayout = false;
  }
  
  void layout(int paramInt, boolean paramBoolean) {
    int i = this.mSpinnerPadding.left;
    int j = this.mRight - this.mLeft - this.mSpinnerPadding.left - this.mSpinnerPadding.right;
    if (this.mDataChanged)
      handleDataChanged(); 
    if (this.mItemCount == 0) {
      resetList();
      return;
    } 
    if (this.mNextSelectedPosition >= 0)
      setSelectedPositionInt(this.mNextSelectedPosition); 
    recycleAllViews();
    removeAllViewsInLayout();
    this.mFirstPosition = this.mSelectedPosition;
    if (this.mAdapter != null) {
      View view = makeView(this.mSelectedPosition, true);
      int k = view.getMeasuredWidth();
      paramInt = i;
      int m = getLayoutDirection();
      m = Gravity.getAbsoluteGravity(this.mGravity, m);
      m &= 0x7;
      if (m != 1) {
        if (m == 5)
          paramInt = i + j - k; 
      } else {
        paramInt = j / 2 + i - k / 2;
      } 
      view.offsetLeftAndRight(paramInt);
    } 
    this.mRecycler.clear();
    invalidate();
    checkSelectionChanged();
    this.mDataChanged = false;
    this.mNeedSync = false;
    setNextSelectedPositionInt(this.mSelectedPosition);
  }
  
  private View makeView(int paramInt, boolean paramBoolean) {
    if (!this.mDataChanged) {
      View view1 = this.mRecycler.get(paramInt);
      if (view1 != null) {
        setUpChild(view1, paramBoolean);
        return view1;
      } 
    } 
    View view = this.mAdapter.getView(paramInt, null, this);
    setUpChild(view, paramBoolean);
    return view;
  }
  
  private void setUpChild(View paramView, boolean paramBoolean) {
    ViewGroup.LayoutParams layoutParams1 = paramView.getLayoutParams();
    ViewGroup.LayoutParams layoutParams2 = layoutParams1;
    if (layoutParams1 == null)
      layoutParams2 = generateDefaultLayoutParams(); 
    addViewInLayout(paramView, 0, layoutParams2);
    paramView.setSelected(hasFocus());
    if (this.mDisableChildrenWhenDisabled)
      paramView.setEnabled(isEnabled()); 
    int i = ViewGroup.getChildMeasureSpec(this.mHeightMeasureSpec, this.mSpinnerPadding.top + this.mSpinnerPadding.bottom, layoutParams2.height);
    int j = ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.mSpinnerPadding.left + this.mSpinnerPadding.right, layoutParams2.width);
    paramView.measure(j, i);
    int k = this.mSpinnerPadding.top;
    j = getMeasuredHeight();
    int m = this.mSpinnerPadding.bottom;
    i = this.mSpinnerPadding.top;
    i = k + (j - m - i - paramView.getMeasuredHeight()) / 2;
    j = paramView.getMeasuredHeight();
    k = paramView.getMeasuredWidth();
    paramView.layout(0, i, 0 + k, j + i);
    if (!paramBoolean)
      removeViewInLayout(paramView); 
  }
  
  public boolean performClick() {
    boolean bool1 = super.performClick();
    boolean bool2 = bool1;
    if (!bool1) {
      bool1 = true;
      bool2 = bool1;
      if (!this.mPopup.isShowing()) {
        this.mPopup.show(getTextDirection(), getTextAlignment());
        bool2 = bool1;
      } 
    } 
    return bool2;
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    setSelection(paramInt);
    paramDialogInterface.dismiss();
  }
  
  public CharSequence getAccessibilityClassName() {
    return Spinner.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (this.mAdapter != null)
      paramAccessibilityNodeInfo.setCanOpenPopup(true); 
  }
  
  public void setPrompt(CharSequence paramCharSequence) {
    this.mPopup.setPromptText(paramCharSequence);
  }
  
  public void setPromptId(int paramInt) {
    setPrompt(getContext().getText(paramInt));
  }
  
  public CharSequence getPrompt() {
    return this.mPopup.getHintText();
  }
  
  int measureContentWidth(SpinnerAdapter paramSpinnerAdapter, Drawable paramDrawable) {
    if (paramSpinnerAdapter == null)
      return 0; 
    int i = 0;
    View view = null;
    int j = 0;
    int k = View.MeasureSpec.makeSafeMeasureSpec(getMeasuredWidth(), 0);
    int m = View.MeasureSpec.makeSafeMeasureSpec(getMeasuredHeight(), 0);
    int n = Math.max(0, getSelectedItemPosition());
    int i1 = Math.min(paramSpinnerAdapter.getCount(), n + 15);
    n = Math.max(0, n - 15 - i1 - n);
    for (; n < i1; n++, j = i3) {
      int i2 = paramSpinnerAdapter.getItemViewType(n);
      int i3 = j;
      if (i2 != j) {
        i3 = i2;
        view = null;
      } 
      view = paramSpinnerAdapter.getView(n, view, this);
      if (view.getLayoutParams() == null)
        view.setLayoutParams(new ViewGroup.LayoutParams(-2, -2)); 
      view.measure(k, m);
      i = Math.max(i, view.getMeasuredWidth());
    } 
    n = i;
    if (paramDrawable != null) {
      paramDrawable.getPadding(this.mTempRect);
      n = i + this.mTempRect.left + this.mTempRect.right;
    } 
    return n;
  }
  
  public Parcelable onSaveInstanceState() {
    boolean bool;
    SavedState savedState = new SavedState();
    SpinnerPopup spinnerPopup = this.mPopup;
    if (spinnerPopup != null && spinnerPopup.isShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    savedState.showDropdown = bool;
    return savedState;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    if (((SavedState)paramParcelable).showDropdown) {
      ViewTreeObserver viewTreeObserver = getViewTreeObserver();
      if (viewTreeObserver != null) {
        Object object = new Object(this);
        viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)object);
      } 
    } 
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    if (getPointerIcon() == null && isClickable() && isEnabled())
      return PointerIcon.getSystemIcon(getContext(), 1002); 
    return super.onResolvePointerIcon(paramMotionEvent, paramInt);
  }
  
  class SavedState extends AbsSpinner.SavedState {
    SavedState() {}
    
    private SavedState(Spinner this$0) {
      boolean bool;
      if (this$0.readByte() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.showDropdown = bool;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeByte((byte)this.showDropdown);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    boolean showDropdown;
  }
  
  class DropDownAdapter implements ListAdapter, SpinnerAdapter {
    private SpinnerAdapter mAdapter;
    
    private ListAdapter mListAdapter;
    
    public DropDownAdapter(Spinner this$0, Resources.Theme param1Theme) {
      this.mAdapter = (SpinnerAdapter)this$0;
      if (this$0 instanceof ListAdapter)
        this.mListAdapter = (ListAdapter)this$0; 
      if (param1Theme != null && this$0 instanceof ThemedSpinnerAdapter) {
        ThemedSpinnerAdapter themedSpinnerAdapter = (ThemedSpinnerAdapter)this$0;
        if (themedSpinnerAdapter.getDropDownViewTheme() == null)
          themedSpinnerAdapter.setDropDownViewTheme(param1Theme); 
      } 
    }
    
    public int getCount() {
      int i;
      SpinnerAdapter spinnerAdapter = this.mAdapter;
      if (spinnerAdapter == null) {
        i = 0;
      } else {
        i = spinnerAdapter.getCount();
      } 
      return i;
    }
    
    public Object getItem(int param1Int) {
      Object object = this.mAdapter;
      if (object == null) {
        object = null;
      } else {
        object = object.getItem(param1Int);
      } 
      return object;
    }
    
    public long getItemId(int param1Int) {
      long l;
      SpinnerAdapter spinnerAdapter = this.mAdapter;
      if (spinnerAdapter == null) {
        l = -1L;
      } else {
        l = spinnerAdapter.getItemId(param1Int);
      } 
      return l;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      return getDropDownView(param1Int, param1View, param1ViewGroup);
    }
    
    public View getDropDownView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      SpinnerAdapter spinnerAdapter = this.mAdapter;
      if (spinnerAdapter == null) {
        param1View = null;
      } else {
        param1View = spinnerAdapter.getDropDownView(param1Int, param1View, param1ViewGroup);
      } 
      return param1View;
    }
    
    public boolean hasStableIds() {
      boolean bool;
      SpinnerAdapter spinnerAdapter = this.mAdapter;
      if (spinnerAdapter != null && spinnerAdapter.hasStableIds()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void registerDataSetObserver(DataSetObserver param1DataSetObserver) {
      SpinnerAdapter spinnerAdapter = this.mAdapter;
      if (spinnerAdapter != null)
        spinnerAdapter.registerDataSetObserver(param1DataSetObserver); 
    }
    
    public void unregisterDataSetObserver(DataSetObserver param1DataSetObserver) {
      SpinnerAdapter spinnerAdapter = this.mAdapter;
      if (spinnerAdapter != null)
        spinnerAdapter.unregisterDataSetObserver(param1DataSetObserver); 
    }
    
    public boolean areAllItemsEnabled() {
      ListAdapter listAdapter = this.mListAdapter;
      if (listAdapter != null)
        return listAdapter.areAllItemsEnabled(); 
      return true;
    }
    
    public boolean isEnabled(int param1Int) {
      ListAdapter listAdapter = this.mListAdapter;
      if (listAdapter != null)
        return listAdapter.isEnabled(param1Int); 
      return true;
    }
    
    public int getItemViewType(int param1Int) {
      return 0;
    }
    
    public int getViewTypeCount() {
      return 1;
    }
    
    public boolean isEmpty() {
      boolean bool;
      if (getCount() == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  class DialogPopup implements SpinnerPopup, DialogInterface.OnClickListener {
    private ListAdapter mListAdapter;
    
    private AlertDialog mPopup;
    
    private CharSequence mPrompt;
    
    final Spinner this$0;
    
    private DialogPopup() {}
    
    public void dismiss() {
      AlertDialog alertDialog = this.mPopup;
      if (alertDialog != null) {
        alertDialog.dismiss();
        this.mPopup = null;
      } 
    }
    
    public boolean isShowing() {
      boolean bool;
      AlertDialog alertDialog = this.mPopup;
      if (alertDialog != null) {
        bool = alertDialog.isShowing();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void setAdapter(ListAdapter param1ListAdapter) {
      this.mListAdapter = param1ListAdapter;
    }
    
    public void setPromptText(CharSequence param1CharSequence) {
      this.mPrompt = param1CharSequence;
    }
    
    public CharSequence getHintText() {
      return this.mPrompt;
    }
    
    public void show(int param1Int1, int param1Int2) {
      if (this.mListAdapter == null)
        return; 
      AlertDialog.Builder builder = new AlertDialog.Builder(Spinner.this.getPopupContext());
      CharSequence charSequence = this.mPrompt;
      if (charSequence != null)
        builder.setTitle(charSequence); 
      ListAdapter listAdapter = this.mListAdapter;
      Spinner spinner = Spinner.this;
      int i = spinner.getSelectedItemPosition();
      builder = builder.setSingleChoiceItems(listAdapter, i, this);
      AlertDialog alertDialog = builder.create();
      ListView listView = alertDialog.getListView();
      listView.setTextDirection(param1Int1);
      listView.setTextAlignment(param1Int2);
      this.mPopup.show();
    }
    
    public void onClick(DialogInterface param1DialogInterface, int param1Int) {
      Spinner.this.setSelection(param1Int);
      if (Spinner.this.mOnItemClickListener != null)
        Spinner.this.performItemClick((View)null, param1Int, this.mListAdapter.getItemId(param1Int)); 
      dismiss();
    }
    
    public void setBackgroundDrawable(Drawable param1Drawable) {
      Log.e("Spinner", "Cannot set popup background for MODE_DIALOG, ignoring");
    }
    
    public void setVerticalOffset(int param1Int) {
      Log.e("Spinner", "Cannot set vertical offset for MODE_DIALOG, ignoring");
    }
    
    public void setHorizontalOffset(int param1Int) {
      Log.e("Spinner", "Cannot set horizontal offset for MODE_DIALOG, ignoring");
    }
    
    public Drawable getBackground() {
      return null;
    }
    
    public int getVerticalOffset() {
      return 0;
    }
    
    public int getHorizontalOffset() {
      return 0;
    }
  }
  
  class DropdownPopup extends ListPopupWindow implements SpinnerPopup {
    private ListAdapter mAdapter;
    
    private CharSequence mHintText;
    
    final Spinner this$0;
    
    public DropdownPopup(Context param1Context, AttributeSet param1AttributeSet, int param1Int1, int param1Int2) {
      super(param1Context, param1AttributeSet, param1Int1, param1Int2);
      setAnchorView(Spinner.this);
      setModal(true);
      setPromptPosition(0);
      setOnItemClickListener((AdapterView.OnItemClickListener)new Object(this, Spinner.this));
    }
    
    public void setAdapter(ListAdapter param1ListAdapter) {
      super.setAdapter(param1ListAdapter);
      this.mAdapter = param1ListAdapter;
    }
    
    public CharSequence getHintText() {
      return this.mHintText;
    }
    
    public void setPromptText(CharSequence param1CharSequence) {
      this.mHintText = param1CharSequence;
    }
    
    void computeContentWidth() {
      Drawable drawable = getBackground();
      int i = 0;
      if (drawable != null) {
        drawable.getPadding(Spinner.this.mTempRect);
        if (Spinner.this.isLayoutRtl()) {
          i = Spinner.this.mTempRect.right;
        } else {
          i = -Spinner.this.mTempRect.left;
        } 
      } else {
        Rect rect = Spinner.this.mTempRect;
        Spinner.this.mTempRect.right = 0;
        rect.left = 0;
      } 
      int j = Spinner.this.getPaddingLeft();
      int k = Spinner.this.getPaddingRight();
      int m = Spinner.this.getWidth();
      if (Spinner.this.mDropDownWidth == -2) {
        Spinner spinner = Spinner.this;
        SpinnerAdapter spinnerAdapter = (SpinnerAdapter)this.mAdapter;
        Drawable drawable1 = getBackground();
        int n = spinner.measureContentWidth(spinnerAdapter, drawable1);
        Resources resources = Spinner.this.mContext.getResources();
        int i1 = (resources.getDisplayMetrics()).widthPixels - Spinner.this.mTempRect.left - Spinner.this.mTempRect.right;
        int i2 = n;
        if (n > i1)
          i2 = i1; 
        setContentWidth(Math.max(i2, m - j - k));
      } else if (Spinner.this.mDropDownWidth == -1) {
        setContentWidth(m - j - k);
      } else {
        setContentWidth(Spinner.this.mDropDownWidth);
      } 
      if (Spinner.this.isLayoutRtl()) {
        i += m - k - getWidth();
      } else {
        i += j;
      } 
      setHorizontalOffset(i);
    }
    
    public void show(int param1Int1, int param1Int2) {
      boolean bool = isShowing();
      computeContentWidth();
      setInputMethodMode(2);
      show();
      ListView listView = getListView();
      listView.setChoiceMode(1);
      listView.setTextDirection(param1Int1);
      listView.setTextAlignment(param1Int2);
      setSelection(Spinner.this.getSelectedItemPosition());
      if (bool)
        return; 
      ViewTreeObserver viewTreeObserver = Spinner.this.getViewTreeObserver();
      if (viewTreeObserver != null) {
        Object object = new Object(this);
        viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)object);
        setOnDismissListener((PopupWindow.OnDismissListener)new Object(this, (ViewTreeObserver.OnGlobalLayoutListener)object));
      } 
    }
  }
  
  class SpinnerPopup {
    public abstract void dismiss();
    
    public abstract Drawable getBackground();
    
    public abstract CharSequence getHintText();
    
    public abstract int getHorizontalOffset();
    
    public abstract int getVerticalOffset();
    
    public abstract boolean isShowing();
    
    public abstract void setAdapter(ListAdapter param1ListAdapter);
    
    public abstract void setBackgroundDrawable(Drawable param1Drawable);
    
    public abstract void setHorizontalOffset(int param1Int);
    
    public abstract void setPromptText(CharSequence param1CharSequence);
    
    public abstract void setVerticalOffset(int param1Int);
    
    public abstract void show(int param1Int1, int param1Int2);
  }
}
