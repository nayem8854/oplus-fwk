package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.icu.util.Calendar;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.MathUtils;
import android.view.View;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityEvent;
import android.view.autofill.AutofillManager;
import android.view.autofill.AutofillValue;
import com.android.internal.R;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Locale;
import libcore.icu.LocaleData;

public class TimePicker extends FrameLayout {
  private static final String LOG_TAG = TimePicker.class.getSimpleName();
  
  public static final int MODE_CLOCK = 2;
  
  public static final int MODE_SPINNER = 1;
  
  private final TimePickerDelegate mDelegate;
  
  private final int mMode;
  
  public TimePicker(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public TimePicker(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843933);
  }
  
  public TimePicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TimePicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    if (getImportantForAutofill() == 0)
      setImportantForAutofill(1); 
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TimePicker, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.TimePicker, paramAttributeSet, typedArray, paramInt1, paramInt2);
    boolean bool = typedArray.getBoolean(10, false);
    int i = typedArray.getInt(8, 1);
    typedArray.recycle();
    if (i == 2 && bool) {
      this.mMode = paramContext.getResources().getInteger(17694965);
    } else {
      this.mMode = i;
    } 
    if (this.mMode != 2) {
      this.mDelegate = new TimePickerSpinnerDelegate(this, paramContext, paramAttributeSet, paramInt1, paramInt2);
    } else {
      this.mDelegate = new TimePickerClockDelegate(this, paramContext, paramAttributeSet, paramInt1, paramInt2);
    } 
    this.mDelegate.setAutoFillChangeListener(new _$$Lambda$TimePicker$2FhAB9WgnLgn4zn4f9rRT7DNfjw(this, paramContext));
  }
  
  public int getMode() {
    return this.mMode;
  }
  
  public void setHour(int paramInt) {
    this.mDelegate.setHour(MathUtils.constrain(paramInt, 0, 23));
  }
  
  public int getHour() {
    return this.mDelegate.getHour();
  }
  
  public void setMinute(int paramInt) {
    this.mDelegate.setMinute(MathUtils.constrain(paramInt, 0, 59));
  }
  
  public int getMinute() {
    return this.mDelegate.getMinute();
  }
  
  @Deprecated
  public void setCurrentHour(Integer paramInteger) {
    setHour(paramInteger.intValue());
  }
  
  @Deprecated
  public Integer getCurrentHour() {
    return Integer.valueOf(getHour());
  }
  
  @Deprecated
  public void setCurrentMinute(Integer paramInteger) {
    setMinute(paramInteger.intValue());
  }
  
  @Deprecated
  public Integer getCurrentMinute() {
    return Integer.valueOf(getMinute());
  }
  
  public void setIs24HourView(Boolean paramBoolean) {
    if (paramBoolean == null)
      return; 
    this.mDelegate.setIs24Hour(paramBoolean.booleanValue());
  }
  
  public boolean is24HourView() {
    return this.mDelegate.is24Hour();
  }
  
  public void setOnTimeChangedListener(OnTimeChangedListener paramOnTimeChangedListener) {
    this.mDelegate.setOnTimeChangedListener(paramOnTimeChangedListener);
  }
  
  public void setEnabled(boolean paramBoolean) {
    super.setEnabled(paramBoolean);
    this.mDelegate.setEnabled(paramBoolean);
  }
  
  public boolean isEnabled() {
    return this.mDelegate.isEnabled();
  }
  
  public int getBaseline() {
    return this.mDelegate.getBaseline();
  }
  
  public boolean validateInput() {
    return this.mDelegate.validateInput();
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    return this.mDelegate.onSaveInstanceState(parcelable);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    this.mDelegate.onRestoreInstanceState(paramParcelable);
  }
  
  public CharSequence getAccessibilityClassName() {
    return TimePicker.class.getName();
  }
  
  public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    return this.mDelegate.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent);
  }
  
  public View getHourView() {
    return this.mDelegate.getHourView();
  }
  
  public View getMinuteView() {
    return this.mDelegate.getMinuteView();
  }
  
  public View getAmView() {
    return this.mDelegate.getAmView();
  }
  
  public View getPmView() {
    return this.mDelegate.getPmView();
  }
  
  static String[] getAmPmStrings(Context paramContext) {
    String str1, str2;
    Locale locale = (paramContext.getResources().getConfiguration()).locale;
    LocaleData localeData = LocaleData.get(locale);
    if (localeData.amPm[0].length() > 4) {
      str1 = localeData.narrowAm;
    } else {
      str1 = localeData.amPm[0];
    } 
    if (localeData.amPm[1].length() > 4) {
      str2 = localeData.narrowPm;
    } else {
      str2 = ((LocaleData)str2).amPm[1];
    } 
    return new String[] { str1, str2 };
  }
  
  class AbstractTimePickerDelegate implements TimePickerDelegate {
    protected TimePicker.OnTimeChangedListener mAutoFillChangeListener;
    
    private long mAutofilledValue;
    
    protected final Context mContext;
    
    protected final TimePicker mDelegator;
    
    protected final Locale mLocale;
    
    protected TimePicker.OnTimeChangedListener mOnTimeChangedListener;
    
    public AbstractTimePickerDelegate(TimePicker this$0, Context param1Context) {
      this.mDelegator = this$0;
      this.mContext = param1Context;
      this.mLocale = (param1Context.getResources().getConfiguration()).locale;
    }
    
    public void setOnTimeChangedListener(TimePicker.OnTimeChangedListener param1OnTimeChangedListener) {
      this.mOnTimeChangedListener = param1OnTimeChangedListener;
    }
    
    public void setAutoFillChangeListener(TimePicker.OnTimeChangedListener param1OnTimeChangedListener) {
      this.mAutoFillChangeListener = param1OnTimeChangedListener;
    }
    
    public final void autofill(AutofillValue param1AutofillValue) {
      if (param1AutofillValue == null || !param1AutofillValue.isDate()) {
        String str = TimePicker.LOG_TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(param1AutofillValue);
        stringBuilder.append(" could not be autofilled into ");
        stringBuilder.append(this);
        Log.w(str, stringBuilder.toString());
        return;
      } 
      long l = param1AutofillValue.getDateValue();
      Calendar calendar = Calendar.getInstance(this.mLocale);
      calendar.setTimeInMillis(l);
      setDate(calendar.get(11), calendar.get(12));
      this.mAutofilledValue = l;
    }
    
    public final AutofillValue getAutofillValue() {
      long l = this.mAutofilledValue;
      if (l != 0L)
        return AutofillValue.forDate(l); 
      Calendar calendar = Calendar.getInstance(this.mLocale);
      calendar.set(11, getHour());
      calendar.set(12, getMinute());
      return AutofillValue.forDate(calendar.getTimeInMillis());
    }
    
    protected void resetAutofilledValue() {
      this.mAutofilledValue = 0L;
    }
    
    class SavedState extends View.BaseSavedState {
      public SavedState(int param2Int1, int param2Int2, boolean param2Boolean) {
        this((Parcelable)this$0, param2Int1, param2Int2, param2Boolean, 0);
      }
      
      public SavedState(int param2Int1, int param2Int2, boolean param2Boolean, int param2Int3) {
        super((Parcelable)this$0);
        this.mHour = param2Int1;
        this.mMinute = param2Int2;
        this.mIs24HourMode = param2Boolean;
        this.mCurrentItemShowing = param2Int3;
      }
      
      private SavedState(TimePicker.AbstractTimePickerDelegate this$0) {
        super((Parcel)this$0);
        this.mHour = this$0.readInt();
        this.mMinute = this$0.readInt();
        int i = this$0.readInt();
        boolean bool = true;
        if (i != 1)
          bool = false; 
        this.mIs24HourMode = bool;
        this.mCurrentItemShowing = this$0.readInt();
      }
      
      public int getHour() {
        return this.mHour;
      }
      
      public int getMinute() {
        return this.mMinute;
      }
      
      public boolean is24HourMode() {
        return this.mIs24HourMode;
      }
      
      public int getCurrentItemShowing() {
        return this.mCurrentItemShowing;
      }
      
      public void writeToParcel(Parcel param2Parcel, int param2Int) {
        super.writeToParcel(param2Parcel, param2Int);
        param2Parcel.writeInt(this.mHour);
        param2Parcel.writeInt(this.mMinute);
        param2Parcel.writeInt(this.mIs24HourMode);
        param2Parcel.writeInt(this.mCurrentItemShowing);
      }
      
      public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
          public TimePicker.AbstractTimePickerDelegate.SavedState createFromParcel(Parcel param2Parcel) {
            return new TimePicker.AbstractTimePickerDelegate.SavedState();
          }
          
          public TimePicker.AbstractTimePickerDelegate.SavedState[] newArray(int param2Int) {
            return new TimePicker.AbstractTimePickerDelegate.SavedState[param2Int];
          }
        };
      
      private final int mCurrentItemShowing;
      
      private final int mHour;
      
      private final boolean mIs24HourMode;
      
      private final int mMinute;
    }
    
    class null implements Parcelable.Creator<SavedState> {
      public TimePicker.AbstractTimePickerDelegate.SavedState createFromParcel(Parcel param2Parcel) {
        return new TimePicker.AbstractTimePickerDelegate.SavedState();
      }
      
      public TimePicker.AbstractTimePickerDelegate.SavedState[] newArray(int param2Int) {
        return new TimePicker.AbstractTimePickerDelegate.SavedState[param2Int];
      }
    }
  }
  
  public void dispatchProvideAutofillStructure(ViewStructure paramViewStructure, int paramInt) {
    paramViewStructure.setAutofillId(getAutofillId());
    onProvideAutofillStructure(paramViewStructure, paramInt);
  }
  
  public void autofill(AutofillValue paramAutofillValue) {
    if (!isEnabled())
      return; 
    this.mDelegate.autofill(paramAutofillValue);
  }
  
  public int getAutofillType() {
    boolean bool;
    if (isEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public AutofillValue getAutofillValue() {
    AutofillValue autofillValue;
    if (isEnabled()) {
      autofillValue = this.mDelegate.getAutofillValue();
    } else {
      autofillValue = null;
    } 
    return autofillValue;
  }
  
  class OnTimeChangedListener {
    public abstract void onTimeChanged(TimePicker param1TimePicker, int param1Int1, int param1Int2);
  }
  
  class TimePickerDelegate {
    public abstract void autofill(AutofillValue param1AutofillValue);
    
    public abstract boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent);
    
    public abstract View getAmView();
    
    public abstract AutofillValue getAutofillValue();
    
    public abstract int getBaseline();
    
    public abstract int getHour();
    
    public abstract View getHourView();
    
    public abstract int getMinute();
    
    public abstract View getMinuteView();
    
    public abstract View getPmView();
    
    public abstract boolean is24Hour();
    
    public abstract boolean isEnabled();
    
    public abstract void onPopulateAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent);
    
    public abstract void onRestoreInstanceState(Parcelable param1Parcelable);
    
    public abstract Parcelable onSaveInstanceState(Parcelable param1Parcelable);
    
    public abstract void setAutoFillChangeListener(TimePicker.OnTimeChangedListener param1OnTimeChangedListener);
    
    public abstract void setDate(int param1Int1, int param1Int2);
    
    public abstract void setEnabled(boolean param1Boolean);
    
    public abstract void setHour(int param1Int);
    
    public abstract void setIs24Hour(boolean param1Boolean);
    
    public abstract void setMinute(int param1Int);
    
    public abstract void setOnTimeChangedListener(TimePicker.OnTimeChangedListener param1OnTimeChangedListener);
    
    public abstract boolean validateInput();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class TimePickerMode implements Annotation {}
}
