package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.Shape;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R;

public class RatingBar extends AbsSeekBar {
  private int mNumStars = 5;
  
  private OnRatingBarChangeListener mOnRatingBarChangeListener;
  
  private int mProgressOnStartTracking;
  
  public RatingBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public RatingBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RatingBar, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.RatingBar, paramAttributeSet, typedArray, paramInt1, paramInt2);
    paramInt1 = typedArray.getInt(0, this.mNumStars);
    setIsIndicator(typedArray.getBoolean(3, this.mIsUserSeekable ^ true));
    float f1 = typedArray.getFloat(1, -1.0F);
    float f2 = typedArray.getFloat(2, -1.0F);
    typedArray.recycle();
    if (paramInt1 > 0 && paramInt1 != this.mNumStars)
      setNumStars(paramInt1); 
    if (f2 >= 0.0F) {
      setStepSize(f2);
    } else {
      setStepSize(0.5F);
    } 
    if (f1 >= 0.0F)
      setRating(f1); 
    this.mTouchProgressOffset = 0.6F;
  }
  
  public RatingBar(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842876);
  }
  
  public RatingBar(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void setOnRatingBarChangeListener(OnRatingBarChangeListener paramOnRatingBarChangeListener) {
    this.mOnRatingBarChangeListener = paramOnRatingBarChangeListener;
  }
  
  public OnRatingBarChangeListener getOnRatingBarChangeListener() {
    return this.mOnRatingBarChangeListener;
  }
  
  public void setIsIndicator(boolean paramBoolean) {
    this.mIsUserSeekable = paramBoolean ^ true;
    if (paramBoolean) {
      setFocusable(16);
    } else {
      setFocusable(1);
    } 
  }
  
  public boolean isIndicator() {
    return this.mIsUserSeekable ^ true;
  }
  
  public void setNumStars(int paramInt) {
    if (paramInt <= 0)
      return; 
    this.mNumStars = paramInt;
    requestLayout();
  }
  
  public int getNumStars() {
    return this.mNumStars;
  }
  
  public void setRating(float paramFloat) {
    setProgress(Math.round(getProgressPerStar() * paramFloat));
  }
  
  public float getRating() {
    return getProgress() / getProgressPerStar();
  }
  
  public void setStepSize(float paramFloat) {
    if (paramFloat <= 0.0F)
      return; 
    paramFloat = this.mNumStars / paramFloat;
    int i = (int)(paramFloat / getMax() * getProgress());
    setMax((int)paramFloat);
    setProgress(i);
  }
  
  public float getStepSize() {
    return getNumStars() / getMax();
  }
  
  private float getProgressPerStar() {
    if (this.mNumStars > 0)
      return getMax() * 1.0F / this.mNumStars; 
    return 1.0F;
  }
  
  Shape getDrawableShape() {
    return (Shape)new RectShape();
  }
  
  void onProgressRefresh(float paramFloat, boolean paramBoolean, int paramInt) {
    super.onProgressRefresh(paramFloat, paramBoolean, paramInt);
    updateSecondaryProgress(paramInt);
    if (!paramBoolean)
      dispatchRatingChange(false); 
  }
  
  private void updateSecondaryProgress(int paramInt) {
    float f = getProgressPerStar();
    if (f > 0.0F) {
      float f1 = paramInt / f;
      paramInt = (int)(Math.ceil(f1) * f);
      setSecondaryProgress(paramInt);
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: iload_2
    //   5: invokespecial onMeasure : (II)V
    //   8: aload_0
    //   9: getfield mSampleWidth : I
    //   12: ifle -> 45
    //   15: aload_0
    //   16: getfield mSampleWidth : I
    //   19: istore_2
    //   20: aload_0
    //   21: getfield mNumStars : I
    //   24: istore_3
    //   25: iload_2
    //   26: iload_3
    //   27: imul
    //   28: iload_1
    //   29: iconst_0
    //   30: invokestatic resolveSizeAndState : (III)I
    //   33: istore_2
    //   34: aload_0
    //   35: invokevirtual getMeasuredHeight : ()I
    //   38: istore_1
    //   39: aload_0
    //   40: iload_2
    //   41: iload_1
    //   42: invokevirtual setMeasuredDimension : (II)V
    //   45: aload_0
    //   46: monitorexit
    //   47: return
    //   48: astore #4
    //   50: aload_0
    //   51: monitorexit
    //   52: aload #4
    //   54: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #296	-> 2
    //   #298	-> 8
    //   #299	-> 15
    //   #300	-> 25
    //   #301	-> 34
    //   #300	-> 39
    //   #303	-> 45
    //   #295	-> 48
    // Exception table:
    //   from	to	target	type
    //   2	8	48	finally
    //   8	15	48	finally
    //   15	25	48	finally
    //   25	34	48	finally
    //   34	39	48	finally
    //   39	45	48	finally
  }
  
  void onStartTrackingTouch() {
    this.mProgressOnStartTracking = getProgress();
    super.onStartTrackingTouch();
  }
  
  void onStopTrackingTouch() {
    super.onStopTrackingTouch();
    if (getProgress() != this.mProgressOnStartTracking)
      dispatchRatingChange(true); 
  }
  
  void onKeyChange() {
    super.onKeyChange();
    dispatchRatingChange(true);
  }
  
  void dispatchRatingChange(boolean paramBoolean) {
    OnRatingBarChangeListener onRatingBarChangeListener = this.mOnRatingBarChangeListener;
    if (onRatingBarChangeListener != null)
      onRatingBarChangeListener.onRatingChanged(this, getRating(), paramBoolean); 
  }
  
  public void setMax(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: ifgt -> 9
    //   6: aload_0
    //   7: monitorexit
    //   8: return
    //   9: aload_0
    //   10: iload_1
    //   11: invokespecial setMax : (I)V
    //   14: aload_0
    //   15: monitorexit
    //   16: return
    //   17: astore_2
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_2
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #337	-> 2
    //   #338	-> 6
    //   #341	-> 9
    //   #342	-> 14
    //   #336	-> 17
    // Exception table:
    //   from	to	target	type
    //   9	14	17	finally
  }
  
  public CharSequence getAccessibilityClassName() {
    return RatingBar.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (canUserSetProgress())
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS); 
  }
  
  boolean canUserSetProgress() {
    boolean bool;
    if (super.canUserSetProgress() && !isIndicator()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class OnRatingBarChangeListener {
    public abstract void onRatingChanged(RatingBar param1RatingBar, float param1Float, boolean param1Boolean);
  }
}
