package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SimpleAdapter extends BaseAdapter implements Filterable, ThemedSpinnerAdapter {
  private List<? extends Map<String, ?>> mData;
  
  private LayoutInflater mDropDownInflater;
  
  private int mDropDownResource;
  
  private SimpleFilter mFilter;
  
  private String[] mFrom;
  
  private final LayoutInflater mInflater;
  
  private int mResource;
  
  private int[] mTo;
  
  private ArrayList<Map<String, ?>> mUnfilteredData;
  
  private ViewBinder mViewBinder;
  
  public SimpleAdapter(Context paramContext, List<? extends Map<String, ?>> paramList, int paramInt, String[] paramArrayOfString, int[] paramArrayOfint) {
    this.mData = paramList;
    this.mDropDownResource = paramInt;
    this.mResource = paramInt;
    this.mFrom = paramArrayOfString;
    this.mTo = paramArrayOfint;
    this.mInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
  }
  
  public int getCount() {
    return this.mData.size();
  }
  
  public Object getItem(int paramInt) {
    return this.mData.get(paramInt);
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    return createViewFromResource(this.mInflater, paramInt, paramView, paramViewGroup, this.mResource);
  }
  
  private View createViewFromResource(LayoutInflater paramLayoutInflater, int paramInt1, View paramView, ViewGroup paramViewGroup, int paramInt2) {
    View view;
    if (paramView == null) {
      view = paramLayoutInflater.inflate(paramInt2, paramViewGroup, false);
    } else {
      view = paramView;
    } 
    bindView(paramInt1, view);
    return view;
  }
  
  public void setDropDownViewResource(int paramInt) {
    this.mDropDownResource = paramInt;
  }
  
  public void setDropDownViewTheme(Resources.Theme paramTheme) {
    if (paramTheme == null) {
      this.mDropDownInflater = null;
    } else if (paramTheme == this.mInflater.getContext().getTheme()) {
      this.mDropDownInflater = this.mInflater;
    } else {
      ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(this.mInflater.getContext(), paramTheme);
      this.mDropDownInflater = LayoutInflater.from((Context)contextThemeWrapper);
    } 
  }
  
  public Resources.Theme getDropDownViewTheme() {
    Resources.Theme theme;
    LayoutInflater layoutInflater = this.mDropDownInflater;
    if (layoutInflater == null) {
      layoutInflater = null;
    } else {
      theme = layoutInflater.getContext().getTheme();
    } 
    return theme;
  }
  
  public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    LayoutInflater layoutInflater1 = this.mDropDownInflater, layoutInflater2 = layoutInflater1;
    if (layoutInflater1 == null)
      layoutInflater2 = this.mInflater; 
    return createViewFromResource(layoutInflater2, paramInt, paramView, paramViewGroup, this.mDropDownResource);
  }
  
  private void bindView(int paramInt, View paramView) {
    Map map = this.mData.get(paramInt);
    if (map == null)
      return; 
    ViewBinder viewBinder = this.mViewBinder;
    String[] arrayOfString = this.mFrom;
    int[] arrayOfInt = this.mTo;
    int i = arrayOfInt.length;
    for (paramInt = 0; paramInt < i; paramInt++) {
      View view = (View)paramView.findViewById(arrayOfInt[paramInt]);
      if (view != null) {
        String str1;
        Object object = map.get(arrayOfString[paramInt]);
        if (object == null) {
          str1 = "";
        } else {
          str1 = object.toString();
        } 
        String str2 = str1;
        if (str1 == null)
          str2 = ""; 
        boolean bool = false;
        if (viewBinder != null)
          bool = viewBinder.setViewValue(view, object, str2); 
        if (!bool)
          if (view instanceof Checkable) {
            if (object instanceof Boolean) {
              ((Checkable)view).setChecked(((Boolean)object).booleanValue());
            } else if (view instanceof TextView) {
              setViewText((TextView)view, str2);
            } else {
              Class<?> clazz;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(view.getClass().getName());
              stringBuilder.append(" should be bound to a Boolean, not a ");
              if (object == null) {
                String str = "<unknown type>";
              } else {
                clazz = object.getClass();
              } 
              stringBuilder.append(clazz);
              throw new IllegalStateException(stringBuilder.toString());
            } 
          } else if (view instanceof TextView) {
            setViewText((TextView)view, str2);
          } else if (view instanceof ImageView) {
            if (object instanceof Integer) {
              setViewImage((ImageView)view, ((Integer)object).intValue());
            } else {
              setViewImage((ImageView)view, str2);
            } 
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(view.getClass().getName());
            stringBuilder.append(" is not a  view that can be bounds by this SimpleAdapter");
            throw new IllegalStateException(stringBuilder.toString());
          }  
      } 
    } 
  }
  
  public ViewBinder getViewBinder() {
    return this.mViewBinder;
  }
  
  public void setViewBinder(ViewBinder paramViewBinder) {
    this.mViewBinder = paramViewBinder;
  }
  
  public void setViewImage(ImageView paramImageView, int paramInt) {
    paramImageView.setImageResource(paramInt);
  }
  
  public void setViewImage(ImageView paramImageView, String paramString) {
    try {
      paramImageView.setImageResource(Integer.parseInt(paramString));
    } catch (NumberFormatException numberFormatException) {
      paramImageView.setImageURI(Uri.parse(paramString));
    } 
  }
  
  public void setViewText(TextView paramTextView, String paramString) {
    paramTextView.setText(paramString);
  }
  
  public Filter getFilter() {
    if (this.mFilter == null)
      this.mFilter = new SimpleFilter(); 
    return this.mFilter;
  }
  
  class SimpleFilter extends Filter {
    final SimpleAdapter this$0;
    
    private SimpleFilter() {}
    
    protected Filter.FilterResults performFiltering(CharSequence param1CharSequence) {
      ArrayList arrayList;
      Filter.FilterResults filterResults = new Filter.FilterResults();
      if (SimpleAdapter.this.mUnfilteredData == null)
        SimpleAdapter.access$102(SimpleAdapter.this, new ArrayList(SimpleAdapter.this.mData)); 
      if (param1CharSequence == null || param1CharSequence.length() == 0) {
        arrayList = SimpleAdapter.this.mUnfilteredData;
        filterResults.values = arrayList;
        filterResults.count = arrayList.size();
        return filterResults;
      } 
      String str = arrayList.toString().toLowerCase();
      ArrayList<Map> arrayList1 = (ArrayList)SimpleAdapter.this.mUnfilteredData;
      int i = arrayList1.size();
      ArrayList<Map> arrayList2 = new ArrayList(i);
      for (byte b = 0; b < i; b++) {
        Map map = arrayList1.get(b);
        if (map != null) {
          int j = SimpleAdapter.this.mTo.length;
          for (byte b1 = 0; b1 < j; b1++) {
            String str1 = (String)map.get(SimpleAdapter.this.mFrom[b1]);
            String[] arrayOfString = str1.split(" ");
            int k = arrayOfString.length;
            for (byte b2 = 0; b2 < k; b2++) {
              String str2 = arrayOfString[b2];
              if (str2.toLowerCase().startsWith(str)) {
                arrayList2.add(map);
                break;
              } 
            } 
          } 
        } 
      } 
      filterResults.values = arrayList2;
      filterResults.count = arrayList2.size();
      return filterResults;
    }
    
    protected void publishResults(CharSequence param1CharSequence, Filter.FilterResults param1FilterResults) {
      SimpleAdapter.access$202(SimpleAdapter.this, (List)param1FilterResults.values);
      if (param1FilterResults.count > 0) {
        SimpleAdapter.this.notifyDataSetChanged();
      } else {
        SimpleAdapter.this.notifyDataSetInvalidated();
      } 
    }
  }
  
  class ViewBinder {
    public abstract boolean setViewValue(View param1View, Object param1Object, String param1String);
  }
}
