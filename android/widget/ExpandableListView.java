package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;
import com.android.internal.R;
import java.util.ArrayList;

public class ExpandableListView extends ListView {
  public static final int CHILD_INDICATOR_INHERIT = -1;
  
  private static final int[] CHILD_LAST_STATE_SET;
  
  private static final int[] EMPTY_STATE_SET;
  
  private static final int[] GROUP_EMPTY_STATE_SET;
  
  private static final int[] GROUP_EXPANDED_EMPTY_STATE_SET;
  
  private static final int[] GROUP_EXPANDED_STATE_SET;
  
  private static final int[][] GROUP_STATE_SETS;
  
  private static final int INDICATOR_UNDEFINED = -2;
  
  private static final long PACKED_POSITION_INT_MASK_CHILD = -1L;
  
  private static final long PACKED_POSITION_INT_MASK_GROUP = 2147483647L;
  
  private static final long PACKED_POSITION_MASK_CHILD = 4294967295L;
  
  private static final long PACKED_POSITION_MASK_GROUP = 9223372032559808512L;
  
  private static final long PACKED_POSITION_MASK_TYPE = -9223372036854775808L;
  
  private static final long PACKED_POSITION_SHIFT_GROUP = 32L;
  
  private static final long PACKED_POSITION_SHIFT_TYPE = 63L;
  
  public static final int PACKED_POSITION_TYPE_CHILD = 1;
  
  public static final int PACKED_POSITION_TYPE_GROUP = 0;
  
  public static final int PACKED_POSITION_TYPE_NULL = 2;
  
  public static final long PACKED_POSITION_VALUE_NULL = 4294967295L;
  
  private ExpandableListAdapter mAdapter;
  
  private Drawable mChildDivider;
  
  private Drawable mChildIndicator;
  
  private int mChildIndicatorEnd;
  
  private int mChildIndicatorLeft;
  
  private int mChildIndicatorRight;
  
  private int mChildIndicatorStart;
  
  private ExpandableListConnector mConnector;
  
  private Drawable mGroupIndicator;
  
  private int mIndicatorEnd;
  
  private int mIndicatorLeft;
  
  static {
    int[] arrayOfInt1 = new int[0];
    int[] arrayOfInt2 = new int[1];
    arrayOfInt2[0] = 16842920;
    GROUP_EXPANDED_STATE_SET = arrayOfInt2;
    int[] arrayOfInt3 = new int[1];
    arrayOfInt3[0] = 16842921;
    GROUP_EMPTY_STATE_SET = arrayOfInt3;
    int[] arrayOfInt4 = new int[2];
    arrayOfInt4[0] = 16842920;
    arrayOfInt4[1] = 16842921;
    GROUP_EXPANDED_EMPTY_STATE_SET = arrayOfInt4;
    GROUP_STATE_SETS = new int[][] { arrayOfInt1, arrayOfInt2, arrayOfInt3, arrayOfInt4 };
    CHILD_LAST_STATE_SET = new int[] { 16842918 };
  }
  
  private final Rect mIndicatorRect = new Rect();
  
  private int mIndicatorRight;
  
  private int mIndicatorStart;
  
  private OnChildClickListener mOnChildClickListener;
  
  private OnGroupClickListener mOnGroupClickListener;
  
  private OnGroupCollapseListener mOnGroupCollapseListener;
  
  private OnGroupExpandListener mOnGroupExpandListener;
  
  public ExpandableListView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ExpandableListView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842863);
  }
  
  public ExpandableListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ExpandableListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ExpandableListView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ExpandableListView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mGroupIndicator = typedArray.getDrawable(0);
    this.mChildIndicator = typedArray.getDrawable(1);
    this.mIndicatorLeft = typedArray.getDimensionPixelSize(2, 0);
    this.mIndicatorRight = paramInt1 = typedArray.getDimensionPixelSize(3, 0);
    if (paramInt1 == 0) {
      Drawable drawable = this.mGroupIndicator;
      if (drawable != null)
        this.mIndicatorRight = this.mIndicatorLeft + drawable.getIntrinsicWidth(); 
    } 
    this.mChildIndicatorLeft = typedArray.getDimensionPixelSize(4, -1);
    this.mChildIndicatorRight = typedArray.getDimensionPixelSize(5, -1);
    this.mChildDivider = typedArray.getDrawable(6);
    if (!isRtlCompatibilityMode()) {
      this.mIndicatorStart = typedArray.getDimensionPixelSize(7, -2);
      this.mIndicatorEnd = typedArray.getDimensionPixelSize(8, -2);
      this.mChildIndicatorStart = typedArray.getDimensionPixelSize(9, -1);
      this.mChildIndicatorEnd = typedArray.getDimensionPixelSize(10, -1);
    } 
    typedArray.recycle();
  }
  
  private boolean isRtlCompatibilityMode() {
    int i = (this.mContext.getApplicationInfo()).targetSdkVersion;
    return (i < 17 || !hasRtlSupport());
  }
  
  private boolean hasRtlSupport() {
    return this.mContext.getApplicationInfo().hasRtlSupport();
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    resolveIndicator();
    resolveChildIndicator();
  }
  
  private void resolveIndicator() {
    boolean bool = isLayoutRtl();
    if (bool) {
      int i = this.mIndicatorStart;
      if (i >= 0)
        this.mIndicatorRight = i; 
      i = this.mIndicatorEnd;
      if (i >= 0)
        this.mIndicatorLeft = i; 
    } else {
      int i = this.mIndicatorStart;
      if (i >= 0)
        this.mIndicatorLeft = i; 
      i = this.mIndicatorEnd;
      if (i >= 0)
        this.mIndicatorRight = i; 
    } 
    if (this.mIndicatorRight == 0) {
      Drawable drawable = this.mGroupIndicator;
      if (drawable != null)
        this.mIndicatorRight = this.mIndicatorLeft + drawable.getIntrinsicWidth(); 
    } 
  }
  
  private void resolveChildIndicator() {
    boolean bool = isLayoutRtl();
    if (bool) {
      int i = this.mChildIndicatorStart;
      if (i >= -1)
        this.mChildIndicatorRight = i; 
      i = this.mChildIndicatorEnd;
      if (i >= -1)
        this.mChildIndicatorLeft = i; 
    } else {
      int i = this.mChildIndicatorStart;
      if (i >= -1)
        this.mChildIndicatorLeft = i; 
      i = this.mChildIndicatorEnd;
      if (i >= -1)
        this.mChildIndicatorRight = i; 
    } 
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    boolean bool;
    super.dispatchDraw(paramCanvas);
    if (this.mChildIndicator == null && this.mGroupIndicator == null)
      return; 
    int i = 0;
    if ((this.mGroupFlags & 0x22) == 34) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      i = paramCanvas.save();
      int i3 = this.mScrollX;
      int i4 = this.mScrollY;
      paramCanvas.clipRect(this.mPaddingLeft + i3, this.mPaddingTop + i4, this.mRight + i3 - this.mLeft - this.mPaddingRight, this.mBottom + i4 - this.mTop - this.mPaddingBottom);
    } 
    int m = getHeaderViewsCount();
    int n = this.mItemCount - getFooterViewsCount() - m - 1;
    int i1 = this.mBottom;
    int j = -4;
    Rect rect = this.mIndicatorRect;
    int i2 = getChildCount();
    int k;
    byte b;
    for (b = 0, k = this.mFirstPosition - m; b < i2; 
      b++, k++) {
      if (k >= 0) {
        if (k > n)
          break; 
        View view = getChildAt(b);
        int i3 = view.getTop();
        int i4 = view.getBottom();
        if (i4 >= 0 && i3 <= i1) {
          ExpandableListConnector.PositionMetadata positionMetadata = this.mConnector.getUnflattenedPos(k);
          boolean bool1 = isLayoutRtl();
          int i5 = getWidth();
          if (positionMetadata.position.type != j) {
            if (positionMetadata.position.type == 1) {
              j = this.mChildIndicatorLeft;
              if (j == -1)
                j = this.mIndicatorLeft; 
              rect.left = j;
              j = this.mChildIndicatorRight;
              if (j == -1)
                j = this.mIndicatorRight; 
              rect.right = j;
            } else {
              rect.left = this.mIndicatorLeft;
              rect.right = this.mIndicatorRight;
            } 
            if (bool1) {
              j = rect.left;
              rect.left = i5 - rect.right;
              rect.right = i5 - j;
              rect.left -= this.mPaddingRight;
              rect.right -= this.mPaddingRight;
            } else {
              rect.left += this.mPaddingLeft;
              rect.right += this.mPaddingLeft;
            } 
            j = positionMetadata.position.type;
          } 
          if (rect.left != rect.right) {
            if (this.mStackFromBottom) {
              rect.top = i3;
              rect.bottom = i4;
            } else {
              rect.top = i3;
              rect.bottom = i4;
            } 
            Drawable drawable = getIndicator(positionMetadata);
            if (drawable != null) {
              drawable.setBounds(rect);
              drawable.draw(paramCanvas);
            } 
          } 
          positionMetadata.recycle();
        } 
      } 
    } 
    if (bool)
      paramCanvas.restoreToCount(i); 
  }
  
  private Drawable getIndicator(ExpandableListConnector.PositionMetadata paramPositionMetadata) {
    Drawable drawable;
    int i = paramPositionMetadata.position.type, j = 2;
    if (i == 2) {
      Drawable drawable1 = this.mGroupIndicator;
      drawable = drawable1;
      if (drawable1 != null) {
        drawable = drawable1;
        if (drawable1.isStateful()) {
          if (paramPositionMetadata.groupMetadata == null || paramPositionMetadata.groupMetadata.lastChildFlPos == paramPositionMetadata.groupMetadata.flPos) {
            i = 1;
          } else {
            i = 0;
          } 
          int k = paramPositionMetadata.isExpanded();
          if (i != 0) {
            i = j;
          } else {
            i = 0;
          } 
          drawable1.setState(GROUP_STATE_SETS[i | k]);
          drawable = drawable1;
        } 
      } 
    } else {
      Drawable drawable1 = this.mChildIndicator;
      drawable = drawable1;
      if (drawable1 != null) {
        drawable = drawable1;
        if (drawable1.isStateful()) {
          int[] arrayOfInt;
          if (paramPositionMetadata.position.flatListPos == paramPositionMetadata.groupMetadata.lastChildFlPos) {
            arrayOfInt = CHILD_LAST_STATE_SET;
          } else {
            arrayOfInt = EMPTY_STATE_SET;
          } 
          drawable1.setState(arrayOfInt);
          drawable = drawable1;
        } 
      } 
    } 
    return drawable;
  }
  
  public void setChildDivider(Drawable paramDrawable) {
    this.mChildDivider = paramDrawable;
  }
  
  void drawDivider(Canvas paramCanvas, Rect paramRect, int paramInt) {
    int i = this.mFirstPosition + paramInt;
    if (i >= 0) {
      paramInt = getFlatPositionForConnector(i);
      ExpandableListConnector.PositionMetadata positionMetadata = this.mConnector.getUnflattenedPos(paramInt);
      if (positionMetadata.position.type == 1 || (positionMetadata.isExpanded() && positionMetadata.groupMetadata.lastChildFlPos != positionMetadata.groupMetadata.flPos)) {
        Drawable drawable = this.mChildDivider;
        drawable.setBounds(paramRect);
        drawable.draw(paramCanvas);
        positionMetadata.recycle();
        return;
      } 
      positionMetadata.recycle();
    } 
    super.drawDivider(paramCanvas, paramRect, i);
  }
  
  public void setAdapter(ListAdapter paramListAdapter) {
    throw new RuntimeException("For ExpandableListView, use setAdapter(ExpandableListAdapter) instead of setAdapter(ListAdapter)");
  }
  
  public ListAdapter getAdapter() {
    return super.getAdapter();
  }
  
  public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener) {
    super.setOnItemClickListener(paramOnItemClickListener);
  }
  
  public void setAdapter(ExpandableListAdapter paramExpandableListAdapter) {
    this.mAdapter = paramExpandableListAdapter;
    if (paramExpandableListAdapter != null) {
      this.mConnector = new ExpandableListConnector(paramExpandableListAdapter);
    } else {
      this.mConnector = null;
    } 
    super.setAdapter(this.mConnector);
  }
  
  public ExpandableListAdapter getExpandableListAdapter() {
    return this.mAdapter;
  }
  
  private boolean isHeaderOrFooterPosition(int paramInt) {
    int i = this.mItemCount, j = getFooterViewsCount();
    return (paramInt < getHeaderViewsCount() || paramInt >= i - j);
  }
  
  private int getFlatPositionForConnector(int paramInt) {
    return paramInt - getHeaderViewsCount();
  }
  
  private int getAbsoluteFlatPosition(int paramInt) {
    return getHeaderViewsCount() + paramInt;
  }
  
  public boolean performItemClick(View paramView, int paramInt, long paramLong) {
    if (isHeaderOrFooterPosition(paramInt))
      return super.performItemClick(paramView, paramInt, paramLong); 
    paramInt = getFlatPositionForConnector(paramInt);
    return handleItemClick(paramView, paramInt, paramLong);
  }
  
  boolean handleItemClick(View paramView, int paramInt, long paramLong) {
    OnGroupExpandListener onGroupExpandListener;
    boolean bool;
    ExpandableListConnector.PositionMetadata positionMetadata = this.mConnector.getUnflattenedPos(paramInt);
    paramLong = getChildOrGroupId(positionMetadata.position);
    if (positionMetadata.position.type == 2) {
      OnGroupClickListener onGroupClickListener = this.mOnGroupClickListener;
      if (onGroupClickListener != null && 
        onGroupClickListener.onGroupClick(this, paramView, positionMetadata.position.groupPos, paramLong)) {
        positionMetadata.recycle();
        return true;
      } 
      if (positionMetadata.isExpanded()) {
        this.mConnector.collapseGroup(positionMetadata);
        playSoundEffect(0);
        OnGroupCollapseListener onGroupCollapseListener = this.mOnGroupCollapseListener;
        if (onGroupCollapseListener != null)
          onGroupCollapseListener.onGroupCollapse(positionMetadata.position.groupPos); 
      } else {
        this.mConnector.expandGroup(positionMetadata);
        playSoundEffect(0);
        onGroupExpandListener = this.mOnGroupExpandListener;
        if (onGroupExpandListener != null)
          onGroupExpandListener.onGroupExpand(positionMetadata.position.groupPos); 
        paramInt = positionMetadata.position.groupPos;
        int i = positionMetadata.position.flatListPos;
        i = getHeaderViewsCount() + i;
        smoothScrollToPosition(this.mAdapter.getChildrenCount(paramInt) + i, i);
      } 
      bool = true;
    } else {
      if (this.mOnChildClickListener != null) {
        playSoundEffect(0);
        return this.mOnChildClickListener.onChildClick(this, (View)onGroupExpandListener, positionMetadata.position.groupPos, positionMetadata.position.childPos, paramLong);
      } 
      bool = false;
    } 
    positionMetadata.recycle();
    return bool;
  }
  
  public boolean expandGroup(int paramInt) {
    return expandGroup(paramInt, false);
  }
  
  public boolean expandGroup(int paramInt, boolean paramBoolean) {
    ExpandableListPosition expandableListPosition = ExpandableListPosition.obtain(2, paramInt, -1, -1);
    ExpandableListConnector.PositionMetadata positionMetadata = this.mConnector.getFlattenedPos(expandableListPosition);
    expandableListPosition.recycle();
    boolean bool = this.mConnector.expandGroup(positionMetadata);
    OnGroupExpandListener onGroupExpandListener = this.mOnGroupExpandListener;
    if (onGroupExpandListener != null)
      onGroupExpandListener.onGroupExpand(paramInt); 
    if (paramBoolean) {
      int i = positionMetadata.position.flatListPos;
      i = getHeaderViewsCount() + i;
      smoothScrollToPosition(this.mAdapter.getChildrenCount(paramInt) + i, i);
    } 
    positionMetadata.recycle();
    return bool;
  }
  
  public boolean collapseGroup(int paramInt) {
    boolean bool = this.mConnector.collapseGroup(paramInt);
    OnGroupCollapseListener onGroupCollapseListener = this.mOnGroupCollapseListener;
    if (onGroupCollapseListener != null)
      onGroupCollapseListener.onGroupCollapse(paramInt); 
    return bool;
  }
  
  public void setOnGroupCollapseListener(OnGroupCollapseListener paramOnGroupCollapseListener) {
    this.mOnGroupCollapseListener = paramOnGroupCollapseListener;
  }
  
  public void setOnGroupExpandListener(OnGroupExpandListener paramOnGroupExpandListener) {
    this.mOnGroupExpandListener = paramOnGroupExpandListener;
  }
  
  public void setOnGroupClickListener(OnGroupClickListener paramOnGroupClickListener) {
    this.mOnGroupClickListener = paramOnGroupClickListener;
  }
  
  public void setOnChildClickListener(OnChildClickListener paramOnChildClickListener) {
    this.mOnChildClickListener = paramOnChildClickListener;
  }
  
  public long getExpandableListPosition(int paramInt) {
    if (isHeaderOrFooterPosition(paramInt))
      return 4294967295L; 
    paramInt = getFlatPositionForConnector(paramInt);
    ExpandableListConnector.PositionMetadata positionMetadata = this.mConnector.getUnflattenedPos(paramInt);
    long l = positionMetadata.position.getPackedPosition();
    positionMetadata.recycle();
    return l;
  }
  
  public int getFlatListPosition(long paramLong) {
    ExpandableListPosition expandableListPosition = ExpandableListPosition.obtainPosition(paramLong);
    ExpandableListConnector.PositionMetadata positionMetadata = this.mConnector.getFlattenedPos(expandableListPosition);
    expandableListPosition.recycle();
    int i = positionMetadata.position.flatListPos;
    positionMetadata.recycle();
    return getAbsoluteFlatPosition(i);
  }
  
  public long getSelectedPosition() {
    int i = getSelectedItemPosition();
    return getExpandableListPosition(i);
  }
  
  public long getSelectedId() {
    long l = getSelectedPosition();
    if (l == 4294967295L)
      return -1L; 
    int i = getPackedPositionGroup(l);
    if (getPackedPositionType(l) == 0)
      return this.mAdapter.getGroupId(i); 
    return this.mAdapter.getChildId(i, getPackedPositionChild(l));
  }
  
  public void setSelectedGroup(int paramInt) {
    ExpandableListPosition expandableListPosition = ExpandableListPosition.obtainGroupPosition(paramInt);
    ExpandableListConnector.PositionMetadata positionMetadata = this.mConnector.getFlattenedPos(expandableListPosition);
    expandableListPosition.recycle();
    paramInt = getAbsoluteFlatPosition(positionMetadata.position.flatListPos);
    setSelection(paramInt);
    positionMetadata.recycle();
  }
  
  public boolean setSelectedChild(int paramInt1, int paramInt2, boolean paramBoolean) {
    ExpandableListPosition expandableListPosition = ExpandableListPosition.obtainChildPosition(paramInt1, paramInt2);
    ExpandableListConnector.PositionMetadata positionMetadata1 = this.mConnector.getFlattenedPos(expandableListPosition);
    ExpandableListConnector.PositionMetadata positionMetadata2 = positionMetadata1;
    if (positionMetadata1 == null) {
      if (!paramBoolean)
        return false; 
      expandGroup(paramInt1);
      positionMetadata2 = this.mConnector.getFlattenedPos(expandableListPosition);
      if (positionMetadata2 == null)
        throw new IllegalStateException("Could not find child"); 
    } 
    paramInt1 = getAbsoluteFlatPosition(positionMetadata2.position.flatListPos);
    setSelection(paramInt1);
    expandableListPosition.recycle();
    positionMetadata2.recycle();
    return true;
  }
  
  public boolean isGroupExpanded(int paramInt) {
    return this.mConnector.isGroupExpanded(paramInt);
  }
  
  public static int getPackedPositionType(long paramLong) {
    boolean bool;
    if (paramLong == 4294967295L)
      return 2; 
    if ((paramLong & Long.MIN_VALUE) == Long.MIN_VALUE) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int getPackedPositionGroup(long paramLong) {
    if (paramLong == 4294967295L)
      return -1; 
    return (int)((0x7FFFFFFF00000000L & paramLong) >> 32L);
  }
  
  public static int getPackedPositionChild(long paramLong) {
    if (paramLong == 4294967295L)
      return -1; 
    if ((paramLong & Long.MIN_VALUE) != Long.MIN_VALUE)
      return -1; 
    return (int)(0xFFFFFFFFL & paramLong);
  }
  
  public static long getPackedPositionForChild(int paramInt1, int paramInt2) {
    return (paramInt1 & 0x7FFFFFFFL) << 32L | Long.MIN_VALUE | paramInt2 & 0xFFFFFFFFFFFFFFFFL;
  }
  
  public static long getPackedPositionForGroup(int paramInt) {
    return (paramInt & 0x7FFFFFFFL) << 32L;
  }
  
  ContextMenu.ContextMenuInfo createContextMenuInfo(View paramView, int paramInt, long paramLong) {
    if (isHeaderOrFooterPosition(paramInt))
      return new AdapterView.AdapterContextMenuInfo(paramView, paramInt, paramLong); 
    paramInt = getFlatPositionForConnector(paramInt);
    ExpandableListConnector.PositionMetadata positionMetadata = this.mConnector.getUnflattenedPos(paramInt);
    ExpandableListPosition expandableListPosition = positionMetadata.position;
    long l = getChildOrGroupId(expandableListPosition);
    paramLong = expandableListPosition.getPackedPosition();
    positionMetadata.recycle();
    return new ExpandableListContextMenuInfo(paramView, paramLong, l);
  }
  
  private long getChildOrGroupId(ExpandableListPosition paramExpandableListPosition) {
    if (paramExpandableListPosition.type == 1)
      return this.mAdapter.getChildId(paramExpandableListPosition.groupPos, paramExpandableListPosition.childPos); 
    return this.mAdapter.getGroupId(paramExpandableListPosition.groupPos);
  }
  
  public void setChildIndicator(Drawable paramDrawable) {
    this.mChildIndicator = paramDrawable;
  }
  
  public void setChildIndicatorBounds(int paramInt1, int paramInt2) {
    this.mChildIndicatorLeft = paramInt1;
    this.mChildIndicatorRight = paramInt2;
    resolveChildIndicator();
  }
  
  public void setChildIndicatorBoundsRelative(int paramInt1, int paramInt2) {
    this.mChildIndicatorStart = paramInt1;
    this.mChildIndicatorEnd = paramInt2;
    resolveChildIndicator();
  }
  
  public void setGroupIndicator(Drawable paramDrawable) {
    this.mGroupIndicator = paramDrawable;
    if (this.mIndicatorRight == 0 && paramDrawable != null)
      this.mIndicatorRight = this.mIndicatorLeft + paramDrawable.getIntrinsicWidth(); 
  }
  
  public void setIndicatorBounds(int paramInt1, int paramInt2) {
    this.mIndicatorLeft = paramInt1;
    this.mIndicatorRight = paramInt2;
    resolveIndicator();
  }
  
  public void setIndicatorBoundsRelative(int paramInt1, int paramInt2) {
    this.mIndicatorStart = paramInt1;
    this.mIndicatorEnd = paramInt2;
    resolveIndicator();
  }
  
  class ExpandableListContextMenuInfo implements ContextMenu.ContextMenuInfo {
    public long id;
    
    public long packedPosition;
    
    public View targetView;
    
    public ExpandableListContextMenuInfo(ExpandableListView this$0, long param1Long1, long param1Long2) {
      this.targetView = this$0;
      this.packedPosition = param1Long1;
      this.id = param1Long2;
    }
  }
  
  class OnChildClickListener {
    public abstract boolean onChildClick(ExpandableListView param1ExpandableListView, View param1View, int param1Int1, int param1Int2, long param1Long);
  }
  
  class OnGroupClickListener {
    public abstract boolean onGroupClick(ExpandableListView param1ExpandableListView, View param1View, int param1Int, long param1Long);
  }
  
  class OnGroupCollapseListener {
    public abstract void onGroupCollapse(int param1Int);
  }
  
  class OnGroupExpandListener {
    public abstract void onGroupExpand(int param1Int);
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState(ArrayList<ExpandableListConnector.GroupMetadata> param1ArrayList) {
      this.expandedGroupMetadataList = param1ArrayList;
    }
    
    private SavedState(ExpandableListView this$0) {
      ArrayList<ExpandableListConnector.GroupMetadata> arrayList = new ArrayList();
      this$0.readList(arrayList, ExpandableListConnector.class.getClassLoader());
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeList(this.expandedGroupMetadataList);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    ArrayList<ExpandableListConnector.GroupMetadata> expandedGroupMetadataList;
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    ExpandableListConnector expandableListConnector = this.mConnector;
    if (expandableListConnector != null) {
      ArrayList<ExpandableListConnector.GroupMetadata> arrayList = expandableListConnector.getExpandedGroupMetadataList();
    } else {
      expandableListConnector = null;
    } 
    return new SavedState((ArrayList<ExpandableListConnector.GroupMetadata>)expandableListConnector);
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (!(paramParcelable instanceof SavedState)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    if (this.mConnector != null && ((SavedState)paramParcelable).expandedGroupMetadataList != null)
      this.mConnector.setExpandedGroupMetadataList(((SavedState)paramParcelable).expandedGroupMetadataList); 
  }
  
  public CharSequence getAccessibilityClassName() {
    return ExpandableListView.class.getName();
  }
}
