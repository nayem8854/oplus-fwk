package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.icu.util.Calendar;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.widget.PagerAdapter;

class DayPickerPagerAdapter extends PagerAdapter {
  private final Calendar mMinDate = Calendar.getInstance();
  
  private final Calendar mMaxDate = Calendar.getInstance();
  
  private final SparseArray<ViewHolder> mItems = new SparseArray<>();
  
  private Calendar mSelectedDay = null;
  
  private static final int MONTHS_IN_YEAR = 12;
  
  private ColorStateList mCalendarTextColor;
  
  private final int mCalendarViewId;
  
  private int mCount;
  
  private ColorStateList mDayHighlightColor;
  
  private int mDayOfWeekTextAppearance;
  
  private ColorStateList mDaySelectorColor;
  
  private int mDayTextAppearance;
  
  private int mFirstDayOfWeek;
  
  private final LayoutInflater mInflater;
  
  private final int mLayoutResId;
  
  private int mMonthTextAppearance;
  
  private final SimpleMonthView.OnDayClickListener mOnDayClickListener;
  
  private OnDaySelectedListener mOnDaySelectedListener;
  
  public void setRange(Calendar paramCalendar1, Calendar paramCalendar2) {
    this.mMinDate.setTimeInMillis(paramCalendar1.getTimeInMillis());
    this.mMaxDate.setTimeInMillis(paramCalendar2.getTimeInMillis());
    int i = this.mMaxDate.get(1), j = this.mMinDate.get(1);
    int k = this.mMaxDate.get(2), m = this.mMinDate.get(2);
    this.mCount = (i - j) * 12 + k - m + 1;
    notifyDataSetChanged();
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    this.mFirstDayOfWeek = paramInt;
    int i = this.mItems.size();
    for (byte b = 0; b < i; b++) {
      SimpleMonthView simpleMonthView = ((ViewHolder)this.mItems.valueAt(b)).calendar;
      simpleMonthView.setFirstDayOfWeek(paramInt);
    } 
  }
  
  public int getFirstDayOfWeek() {
    return this.mFirstDayOfWeek;
  }
  
  public boolean getBoundsForDate(Calendar paramCalendar, Rect paramRect) {
    int i = getPositionForDay(paramCalendar);
    ViewHolder viewHolder = this.mItems.get(i, null);
    if (viewHolder == null)
      return false; 
    i = paramCalendar.get(5);
    return viewHolder.calendar.getBoundsForDay(i, paramRect);
  }
  
  public void setSelectedDay(Calendar paramCalendar) {
    int i = getPositionForDay(this.mSelectedDay);
    int j = getPositionForDay(paramCalendar);
    if (i != j && i >= 0) {
      ViewHolder viewHolder = this.mItems.get(i, null);
      if (viewHolder != null)
        viewHolder.calendar.setSelectedDay(-1); 
    } 
    if (j >= 0) {
      ViewHolder viewHolder = this.mItems.get(j, null);
      if (viewHolder != null) {
        i = paramCalendar.get(5);
        viewHolder.calendar.setSelectedDay(i);
      } 
    } 
    this.mSelectedDay = paramCalendar;
  }
  
  public void setOnDaySelectedListener(OnDaySelectedListener paramOnDaySelectedListener) {
    this.mOnDaySelectedListener = paramOnDaySelectedListener;
  }
  
  void setCalendarTextColor(ColorStateList paramColorStateList) {
    this.mCalendarTextColor = paramColorStateList;
    notifyDataSetChanged();
  }
  
  void setDaySelectorColor(ColorStateList paramColorStateList) {
    this.mDaySelectorColor = paramColorStateList;
    notifyDataSetChanged();
  }
  
  void setMonthTextAppearance(int paramInt) {
    this.mMonthTextAppearance = paramInt;
    notifyDataSetChanged();
  }
  
  void setDayOfWeekTextAppearance(int paramInt) {
    this.mDayOfWeekTextAppearance = paramInt;
    notifyDataSetChanged();
  }
  
  int getDayOfWeekTextAppearance() {
    return this.mDayOfWeekTextAppearance;
  }
  
  void setDayTextAppearance(int paramInt) {
    this.mDayTextAppearance = paramInt;
    notifyDataSetChanged();
  }
  
  int getDayTextAppearance() {
    return this.mDayTextAppearance;
  }
  
  public int getCount() {
    return this.mCount;
  }
  
  public boolean isViewFromObject(View paramView, Object paramObject) {
    boolean bool;
    paramObject = paramObject;
    if (paramView == ((ViewHolder)paramObject).container) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int getMonthForPosition(int paramInt) {
    return (this.mMinDate.get(2) + paramInt) % 12;
  }
  
  private int getYearForPosition(int paramInt) {
    paramInt = (this.mMinDate.get(2) + paramInt) / 12;
    return this.mMinDate.get(1) + paramInt;
  }
  
  private int getPositionForDay(Calendar paramCalendar) {
    if (paramCalendar == null)
      return -1; 
    int i = paramCalendar.get(1), j = this.mMinDate.get(1);
    int k = paramCalendar.get(2), m = this.mMinDate.get(2);
    return (i - j) * 12 + k - m;
  }
  
  public Object instantiateItem(ViewGroup paramViewGroup, int paramInt) {
    byte b;
    boolean bool;
    byte b1;
    View view = this.mInflater.inflate(this.mLayoutResId, paramViewGroup, false);
    SimpleMonthView simpleMonthView = view.<SimpleMonthView>findViewById(this.mCalendarViewId);
    simpleMonthView.setOnDayClickListener(this.mOnDayClickListener);
    simpleMonthView.setMonthTextAppearance(this.mMonthTextAppearance);
    simpleMonthView.setDayOfWeekTextAppearance(this.mDayOfWeekTextAppearance);
    simpleMonthView.setDayTextAppearance(this.mDayTextAppearance);
    ColorStateList colorStateList = this.mDaySelectorColor;
    if (colorStateList != null)
      simpleMonthView.setDaySelectorColor(colorStateList); 
    colorStateList = this.mDayHighlightColor;
    if (colorStateList != null)
      simpleMonthView.setDayHighlightColor(colorStateList); 
    colorStateList = this.mCalendarTextColor;
    if (colorStateList != null) {
      simpleMonthView.setMonthTextColor(colorStateList);
      simpleMonthView.setDayOfWeekTextColor(this.mCalendarTextColor);
      simpleMonthView.setDayTextColor(this.mCalendarTextColor);
    } 
    int i = getMonthForPosition(paramInt);
    int j = getYearForPosition(paramInt);
    Calendar calendar = this.mSelectedDay;
    if (calendar != null && calendar.get(2) == i && this.mSelectedDay.get(1) == j) {
      b = this.mSelectedDay.get(5);
    } else {
      b = -1;
    } 
    if (this.mMinDate.get(2) == i && this.mMinDate.get(1) == j) {
      bool = this.mMinDate.get(5);
    } else {
      bool = true;
    } 
    if (this.mMaxDate.get(2) == i && this.mMaxDate.get(1) == j) {
      b1 = this.mMaxDate.get(5);
    } else {
      b1 = 31;
    } 
    simpleMonthView.setMonthParams(b, i, j, this.mFirstDayOfWeek, bool, b1);
    ViewHolder viewHolder = new ViewHolder(paramInt, view, simpleMonthView);
    this.mItems.put(paramInt, viewHolder);
    paramViewGroup.addView(view);
    return viewHolder;
  }
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject) {
    paramObject = paramObject;
    paramViewGroup.removeView(((ViewHolder)paramObject).container);
    this.mItems.remove(paramInt);
  }
  
  public int getItemPosition(Object paramObject) {
    paramObject = paramObject;
    return ((ViewHolder)paramObject).position;
  }
  
  public CharSequence getPageTitle(int paramInt) {
    SimpleMonthView simpleMonthView = ((ViewHolder)this.mItems.get(paramInt)).calendar;
    if (simpleMonthView != null)
      return simpleMonthView.getMonthYearLabel(); 
    return null;
  }
  
  SimpleMonthView getView(Object paramObject) {
    if (paramObject == null)
      return null; 
    paramObject = paramObject;
    return ((ViewHolder)paramObject).calendar;
  }
  
  public DayPickerPagerAdapter(Context paramContext, int paramInt1, int paramInt2) {
    this.mOnDayClickListener = new SimpleMonthView.OnDayClickListener() {
        final DayPickerPagerAdapter this$0;
        
        public void onDayClick(SimpleMonthView param1SimpleMonthView, Calendar param1Calendar) {
          if (param1Calendar != null) {
            DayPickerPagerAdapter.this.setSelectedDay(param1Calendar);
            if (DayPickerPagerAdapter.this.mOnDaySelectedListener != null)
              DayPickerPagerAdapter.this.mOnDaySelectedListener.onDaySelected(DayPickerPagerAdapter.this, param1Calendar); 
          } 
        }
      };
    this.mInflater = LayoutInflater.from(paramContext);
    this.mLayoutResId = paramInt1;
    this.mCalendarViewId = paramInt2;
    TypedArray typedArray = paramContext.obtainStyledAttributes(new int[] { 16843820 });
    this.mDayHighlightColor = typedArray.getColorStateList(0);
    typedArray.recycle();
  }
  
  class OnDaySelectedListener {
    public abstract void onDaySelected(DayPickerPagerAdapter param1DayPickerPagerAdapter, Calendar param1Calendar);
  }
  
  class ViewHolder {
    public final SimpleMonthView calendar;
    
    public final View container;
    
    public final int position;
    
    public ViewHolder(DayPickerPagerAdapter this$0, View param1View, SimpleMonthView param1SimpleMonthView) {
      this.position = this$0;
      this.container = param1View;
      this.calendar = param1SimpleMonthView;
    }
  }
}
