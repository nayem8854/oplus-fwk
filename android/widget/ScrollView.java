package android.widget;

import android.common.ColorFrameworkFactory;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.IOplusViewConfigHelper;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AnimationUtils;
import com.android.internal.R;

public class ScrollView extends FrameLayout {
  private final Rect mTempRect = new Rect();
  
  private EdgeEffect mEdgeGlowTop = new EdgeEffect(getContext());
  
  private EdgeEffect mEdgeGlowBottom = new EdgeEffect(getContext());
  
  private boolean mIsLayoutDirty = true;
  
  private View mChildToScrollTo = null;
  
  private boolean mIsBeingDragged = false;
  
  private boolean mSmoothScrollingEnabled = true;
  
  private int mActivePointerId = -1;
  
  private final int[] mScrollOffset = new int[2];
  
  private final int[] mScrollConsumed = new int[2];
  
  private StrictMode.Span mScrollStrictSpan = null;
  
  private StrictMode.Span mFlingStrictSpan = null;
  
  static final int ANIMATED_SCROLL_GAP = 250;
  
  private static final int INVALID_POINTER = -1;
  
  static final float MAX_SCROLL_FACTOR = 0.5F;
  
  private static final String TAG = "ScrollView";
  
  private IOplusViewConfigHelper mColorViewConfigHelper;
  
  @ExportedProperty(category = "layout")
  private boolean mFillViewport;
  
  private int mLastMotionY;
  
  private long mLastScroll;
  
  private int mMaximumVelocity;
  
  private int mMinimumVelocity;
  
  private int mNestedYOffset;
  
  private int mOverflingDistance;
  
  private int mOverscrollDistance;
  
  private SavedState mSavedState;
  
  private OverScroller mScroller;
  
  private int mTouchSlop;
  
  private VelocityTracker mVelocityTracker;
  
  private float mVerticalScrollFactor;
  
  public ScrollView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ScrollView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842880);
  }
  
  public ScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    initScrollView();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ScrollView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ScrollView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    setFillViewport(typedArray.getBoolean(0, false));
    typedArray.recycle();
    if ((paramContext.getResources().getConfiguration()).uiMode == 6)
      setRevealOnFocusHint(false); 
  }
  
  public boolean shouldDelayChildPressedState() {
    return true;
  }
  
  protected float getTopFadingEdgeStrength() {
    if (getChildCount() == 0)
      return 0.0F; 
    int i = getVerticalFadingEdgeLength();
    if (this.mScrollY < i)
      return this.mScrollY / i; 
    return 1.0F;
  }
  
  protected float getBottomFadingEdgeStrength() {
    if (getChildCount() == 0)
      return 0.0F; 
    int i = getVerticalFadingEdgeLength();
    int j = getHeight(), k = this.mPaddingBottom;
    k = getChildAt(0).getBottom() - this.mScrollY - j - k;
    if (k < i)
      return k / i; 
    return 1.0F;
  }
  
  public void setEdgeEffectColor(int paramInt) {
    setTopEdgeEffectColor(paramInt);
    setBottomEdgeEffectColor(paramInt);
  }
  
  public void setBottomEdgeEffectColor(int paramInt) {
    this.mEdgeGlowBottom.setColor(paramInt);
  }
  
  public void setTopEdgeEffectColor(int paramInt) {
    this.mEdgeGlowTop.setColor(paramInt);
  }
  
  public int getTopEdgeEffectColor() {
    return this.mEdgeGlowTop.getColor();
  }
  
  public int getBottomEdgeEffectColor() {
    return this.mEdgeGlowBottom.getColor();
  }
  
  public int getMaxScrollAmount() {
    return (int)((this.mBottom - this.mTop) * 0.5F);
  }
  
  private void initScrollView() {
    this.mScroller = SpringOverScroller.newInstance(getContext(), true);
    setFocusable(true);
    setDescendantFocusability(262144);
    setWillNotDraw(false);
    ViewConfiguration viewConfiguration = ViewConfiguration.get(this.mContext);
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    this.mMinimumVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
    this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    this.mOverscrollDistance = viewConfiguration.getScaledOverscrollDistance();
    this.mOverflingDistance = viewConfiguration.getScaledOverflingDistance();
    this.mVerticalScrollFactor = viewConfiguration.getScaledVerticalScrollFactor();
    IOplusViewConfigHelper iOplusViewConfigHelper = ColorFrameworkFactory.getInstance().getOplusViewHooks(null, null).getOplusViewConfigHelper(this.mContext);
    this.mOverscrollDistance = iOplusViewConfigHelper.getScaledOverscrollDistance(this.mOverscrollDistance);
    this.mOverflingDistance = this.mColorViewConfigHelper.getScaledOverflingDistance(this.mOverflingDistance);
  }
  
  public void addView(View paramView) {
    if (getChildCount() <= 0) {
      super.addView(paramView);
      return;
    } 
    throw new IllegalStateException("ScrollView can host only one direct child");
  }
  
  public void addView(View paramView, int paramInt) {
    if (getChildCount() <= 0) {
      super.addView(paramView, paramInt);
      return;
    } 
    throw new IllegalStateException("ScrollView can host only one direct child");
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    if (getChildCount() <= 0) {
      super.addView(paramView, paramLayoutParams);
      return;
    } 
    throw new IllegalStateException("ScrollView can host only one direct child");
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    if (getChildCount() <= 0) {
      super.addView(paramView, paramInt, paramLayoutParams);
      return;
    } 
    throw new IllegalStateException("ScrollView can host only one direct child");
  }
  
  private boolean canScroll() {
    boolean bool = false;
    View view = getChildAt(0);
    if (view != null) {
      int i = view.getHeight();
      if (getHeight() < this.mPaddingTop + i + this.mPaddingBottom)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public boolean isFillViewport() {
    return this.mFillViewport;
  }
  
  public void setFillViewport(boolean paramBoolean) {
    if (paramBoolean != this.mFillViewport) {
      this.mFillViewport = paramBoolean;
      requestLayout();
    } 
  }
  
  public boolean isSmoothScrollingEnabled() {
    return this.mSmoothScrollingEnabled;
  }
  
  public void setSmoothScrollingEnabled(boolean paramBoolean) {
    this.mSmoothScrollingEnabled = paramBoolean;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    if (!this.mFillViewport)
      return; 
    paramInt2 = View.MeasureSpec.getMode(paramInt2);
    if (paramInt2 == 0)
      return; 
    if (getChildCount() > 0) {
      View view = getChildAt(0);
      paramInt2 = (getContext().getApplicationInfo()).targetSdkVersion;
      FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)view.getLayoutParams();
      if (paramInt2 >= 23) {
        paramInt2 = this.mPaddingLeft + this.mPaddingRight + layoutParams.leftMargin + layoutParams.rightMargin;
        i = this.mPaddingTop + this.mPaddingBottom + layoutParams.topMargin + layoutParams.bottomMargin;
      } else {
        paramInt2 = this.mPaddingLeft + this.mPaddingRight;
        i = this.mPaddingTop + this.mPaddingBottom;
      } 
      int i = getMeasuredHeight() - i;
      if (view.getMeasuredHeight() < i) {
        paramInt1 = getChildMeasureSpec(paramInt1, paramInt2, layoutParams.width);
        paramInt2 = View.MeasureSpec.makeMeasureSpec(i, 1073741824);
        view.measure(paramInt1, paramInt2);
      } 
    } 
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    return (super.dispatchKeyEvent(paramKeyEvent) || executeKeyEvent(paramKeyEvent));
  }
  
  public boolean executeKeyEvent(KeyEvent paramKeyEvent) {
    View view;
    this.mTempRect.setEmpty();
    boolean bool = canScroll();
    char c = '';
    if (!bool) {
      boolean bool2 = isFocused();
      bool = false;
      if (bool2 && paramKeyEvent.getKeyCode() != 4) {
        View view1 = findFocus();
        view = view1;
        if (view1 == this)
          view = null; 
        view = FocusFinder.getInstance().findNextFocus(this, view, 130);
        if (view != null && view != this)
          if (view.requestFocus(130))
            bool = true;  
        return bool;
      } 
      return false;
    } 
    boolean bool1 = false;
    bool = bool1;
    if (view.getAction() == 0) {
      int i = view.getKeyCode();
      if (i != 19) {
        if (i != 20) {
          if (i != 62) {
            bool = bool1;
          } else {
            if (view.isShiftPressed())
              c = '!'; 
            pageScroll(c);
            bool = bool1;
          } 
        } else if (!view.isAltPressed()) {
          bool = arrowScroll(130);
        } else {
          bool = fullScroll(130);
        } 
      } else if (!view.isAltPressed()) {
        bool = arrowScroll(33);
      } else {
        bool = fullScroll(33);
      } 
    } 
    return bool;
  }
  
  private boolean inChild(int paramInt1, int paramInt2) {
    int i = getChildCount();
    boolean bool = false;
    if (i > 0) {
      i = this.mScrollY;
      View view = getChildAt(0);
      if (paramInt2 >= view.getTop() - i && 
        paramInt2 < view.getBottom() - i && 
        paramInt1 >= view.getLeft() && 
        paramInt1 < view.getRight())
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  private void initOrResetVelocityTracker() {
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker == null) {
      this.mVelocityTracker = VelocityTracker.obtain();
    } else {
      velocityTracker.clear();
    } 
  }
  
  private void initVelocityTrackerIfNotExists() {
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain(); 
  }
  
  private void recycleVelocityTracker() {
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null) {
      velocityTracker.recycle();
      this.mVelocityTracker = null;
    } 
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean) {
    if (paramBoolean)
      recycleVelocityTracker(); 
    super.requestDisallowInterceptTouchEvent(paramBoolean);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    ViewParent viewParent;
    int i = paramMotionEvent.getAction();
    if (i == 2 && this.mIsBeingDragged)
      return true; 
    if (super.onInterceptTouchEvent(paramMotionEvent))
      return true; 
    if (getScrollY() == 0 && !canScrollVertically(1))
      return false; 
    i &= 0xFF;
    if (i != 0) {
      if (i != 1)
        if (i != 2) {
          if (i != 3) {
            if (i == 6)
              onSecondaryPointerUp(paramMotionEvent); 
            return this.mIsBeingDragged;
          } 
        } else {
          int j = this.mActivePointerId;
          if (j != -1) {
            StringBuilder stringBuilder;
            i = paramMotionEvent.findPointerIndex(j);
            if (i == -1) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Invalid pointerId=");
              stringBuilder.append(j);
              stringBuilder.append(" in onInterceptTouchEvent");
              Log.e("ScrollView", stringBuilder.toString());
            } else {
              j = (int)stringBuilder.getY(i);
              i = Math.abs(j - this.mLastMotionY);
              if (i > this.mTouchSlop && (0x2 & getNestedScrollAxes()) == 0) {
                this.mIsBeingDragged = true;
                this.mLastMotionY = j;
                initVelocityTrackerIfNotExists();
                this.mVelocityTracker.addMovement((MotionEvent)stringBuilder);
                this.mNestedYOffset = 0;
                if (this.mScrollStrictSpan == null)
                  this.mScrollStrictSpan = StrictMode.enterCriticalSpan("ScrollView-scroll"); 
                viewParent = getParent();
                if (viewParent != null)
                  viewParent.requestDisallowInterceptTouchEvent(true); 
              } 
            } 
          } 
          return this.mIsBeingDragged;
        }  
      this.mIsBeingDragged = false;
      this.mActivePointerId = -1;
      recycleVelocityTracker();
      if (this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, 0, 0, getScrollRange()))
        postInvalidateOnAnimation(); 
      stopNestedScroll();
    } else {
      i = (int)viewParent.getY();
      if (!inChild((int)viewParent.getX(), i)) {
        this.mIsBeingDragged = false;
        recycleVelocityTracker();
      } else {
        this.mLastMotionY = i;
        this.mActivePointerId = viewParent.getPointerId(0);
        initOrResetVelocityTracker();
        this.mVelocityTracker.addMovement((MotionEvent)viewParent);
        this.mScroller.computeScrollOffset();
        int j = true ^ this.mScroller.isFinished();
        if (j != 0 && this.mScrollStrictSpan == null)
          this.mScrollStrictSpan = StrictMode.enterCriticalSpan("ScrollView-scroll"); 
        startNestedScroll(2);
      } 
    } 
    return this.mIsBeingDragged;
  }
  
  private boolean shouldDisplayEdgeEffects() {
    boolean bool;
    if (getOverScrollMode() != 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial initVelocityTrackerIfNotExists : ()V
    //   4: aload_1
    //   5: invokestatic obtain : (Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    //   8: astore_2
    //   9: aload_1
    //   10: invokevirtual getActionMasked : ()I
    //   13: istore_3
    //   14: iconst_0
    //   15: istore #4
    //   17: iload_3
    //   18: ifne -> 26
    //   21: aload_0
    //   22: iconst_0
    //   23: putfield mNestedYOffset : I
    //   26: aload_2
    //   27: fconst_0
    //   28: aload_0
    //   29: getfield mNestedYOffset : I
    //   32: i2f
    //   33: invokevirtual offsetLocation : (FF)V
    //   36: iload_3
    //   37: ifeq -> 872
    //   40: iload_3
    //   41: iconst_1
    //   42: if_icmpeq -> 765
    //   45: iload_3
    //   46: iconst_2
    //   47: if_icmpeq -> 176
    //   50: iload_3
    //   51: iconst_3
    //   52: if_icmpeq -> 121
    //   55: iload_3
    //   56: iconst_5
    //   57: if_icmpeq -> 94
    //   60: iload_3
    //   61: bipush #6
    //   63: if_icmpeq -> 69
    //   66: goto -> 984
    //   69: aload_0
    //   70: aload_1
    //   71: invokespecial onSecondaryPointerUp : (Landroid/view/MotionEvent;)V
    //   74: aload_0
    //   75: aload_1
    //   76: aload_1
    //   77: aload_0
    //   78: getfield mActivePointerId : I
    //   81: invokevirtual findPointerIndex : (I)I
    //   84: invokevirtual getY : (I)F
    //   87: f2i
    //   88: putfield mLastMotionY : I
    //   91: goto -> 984
    //   94: aload_1
    //   95: invokevirtual getActionIndex : ()I
    //   98: istore_3
    //   99: aload_0
    //   100: aload_1
    //   101: iload_3
    //   102: invokevirtual getY : (I)F
    //   105: f2i
    //   106: putfield mLastMotionY : I
    //   109: aload_0
    //   110: aload_1
    //   111: iload_3
    //   112: invokevirtual getPointerId : (I)I
    //   115: putfield mActivePointerId : I
    //   118: goto -> 984
    //   121: aload_0
    //   122: getfield mIsBeingDragged : Z
    //   125: ifeq -> 984
    //   128: aload_0
    //   129: invokevirtual getChildCount : ()I
    //   132: ifle -> 984
    //   135: aload_0
    //   136: getfield mScroller : Landroid/widget/OverScroller;
    //   139: aload_0
    //   140: getfield mScrollX : I
    //   143: aload_0
    //   144: getfield mScrollY : I
    //   147: iconst_0
    //   148: iconst_0
    //   149: iconst_0
    //   150: aload_0
    //   151: invokespecial getScrollRange : ()I
    //   154: invokevirtual springBack : (IIIIII)Z
    //   157: ifeq -> 164
    //   160: aload_0
    //   161: invokevirtual postInvalidateOnAnimation : ()V
    //   164: aload_0
    //   165: iconst_m1
    //   166: putfield mActivePointerId : I
    //   169: aload_0
    //   170: invokespecial endDrag : ()V
    //   173: goto -> 984
    //   176: aload_1
    //   177: aload_0
    //   178: getfield mActivePointerId : I
    //   181: invokevirtual findPointerIndex : (I)I
    //   184: istore #5
    //   186: iload #5
    //   188: iconst_m1
    //   189: if_icmpne -> 238
    //   192: new java/lang/StringBuilder
    //   195: dup
    //   196: invokespecial <init> : ()V
    //   199: astore_1
    //   200: aload_1
    //   201: ldc_w 'Invalid pointerId='
    //   204: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   207: pop
    //   208: aload_1
    //   209: aload_0
    //   210: getfield mActivePointerId : I
    //   213: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   216: pop
    //   217: aload_1
    //   218: ldc_w ' in onTouchEvent'
    //   221: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   224: pop
    //   225: ldc 'ScrollView'
    //   227: aload_1
    //   228: invokevirtual toString : ()Ljava/lang/String;
    //   231: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   234: pop
    //   235: goto -> 984
    //   238: aload_1
    //   239: iload #5
    //   241: invokevirtual getY : (I)F
    //   244: f2i
    //   245: istore #6
    //   247: aload_0
    //   248: getfield mLastMotionY : I
    //   251: iload #6
    //   253: isub
    //   254: istore_3
    //   255: iload_3
    //   256: istore #7
    //   258: aload_0
    //   259: iconst_0
    //   260: iload_3
    //   261: aload_0
    //   262: getfield mScrollConsumed : [I
    //   265: aload_0
    //   266: getfield mScrollOffset : [I
    //   269: invokevirtual dispatchNestedPreScroll : (II[I[I)Z
    //   272: ifeq -> 312
    //   275: iload_3
    //   276: aload_0
    //   277: getfield mScrollConsumed : [I
    //   280: iconst_1
    //   281: iaload
    //   282: isub
    //   283: istore #7
    //   285: aload_2
    //   286: fconst_0
    //   287: aload_0
    //   288: getfield mScrollOffset : [I
    //   291: iconst_1
    //   292: iaload
    //   293: i2f
    //   294: invokevirtual offsetLocation : (FF)V
    //   297: aload_0
    //   298: aload_0
    //   299: getfield mNestedYOffset : I
    //   302: aload_0
    //   303: getfield mScrollOffset : [I
    //   306: iconst_1
    //   307: iaload
    //   308: iadd
    //   309: putfield mNestedYOffset : I
    //   312: iload #7
    //   314: istore_3
    //   315: aload_0
    //   316: getfield mIsBeingDragged : Z
    //   319: ifne -> 385
    //   322: iload #7
    //   324: istore_3
    //   325: iload #7
    //   327: invokestatic abs : (I)I
    //   330: aload_0
    //   331: getfield mTouchSlop : I
    //   334: if_icmple -> 385
    //   337: aload_0
    //   338: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   341: astore #8
    //   343: aload #8
    //   345: ifnull -> 356
    //   348: aload #8
    //   350: iconst_1
    //   351: invokeinterface requestDisallowInterceptTouchEvent : (Z)V
    //   356: aload_0
    //   357: iconst_1
    //   358: putfield mIsBeingDragged : Z
    //   361: iload #7
    //   363: ifle -> 377
    //   366: iload #7
    //   368: aload_0
    //   369: getfield mTouchSlop : I
    //   372: isub
    //   373: istore_3
    //   374: goto -> 385
    //   377: iload #7
    //   379: aload_0
    //   380: getfield mTouchSlop : I
    //   383: iadd
    //   384: istore_3
    //   385: aload_0
    //   386: getfield mIsBeingDragged : Z
    //   389: ifeq -> 762
    //   392: aload_0
    //   393: iload #6
    //   395: aload_0
    //   396: getfield mScrollOffset : [I
    //   399: iconst_1
    //   400: iaload
    //   401: isub
    //   402: putfield mLastMotionY : I
    //   405: aload_0
    //   406: getfield mScrollY : I
    //   409: istore #9
    //   411: aload_0
    //   412: invokespecial getScrollRange : ()I
    //   415: istore #6
    //   417: aload_0
    //   418: invokevirtual getOverScrollMode : ()I
    //   421: istore #10
    //   423: iload #10
    //   425: ifeq -> 447
    //   428: iload #4
    //   430: istore #7
    //   432: iload #10
    //   434: iconst_1
    //   435: if_icmpne -> 450
    //   438: iload #4
    //   440: istore #7
    //   442: iload #6
    //   444: ifle -> 450
    //   447: iconst_1
    //   448: istore #7
    //   450: aload_0
    //   451: getfield mColorViewConfigHelper : Landroid/view/IOplusViewConfigHelper;
    //   454: iload_3
    //   455: aload_0
    //   456: getfield mScrollY : I
    //   459: aload_0
    //   460: invokespecial getScrollRange : ()I
    //   463: invokeinterface calcRealOverScrollDist : (III)I
    //   468: istore_3
    //   469: aload_0
    //   470: iconst_0
    //   471: iload_3
    //   472: iconst_0
    //   473: aload_0
    //   474: getfield mScrollY : I
    //   477: iconst_0
    //   478: iload #6
    //   480: iconst_0
    //   481: aload_0
    //   482: getfield mOverscrollDistance : I
    //   485: iconst_1
    //   486: invokevirtual overScrollBy : (IIIIIIIIZ)Z
    //   489: ifeq -> 506
    //   492: aload_0
    //   493: invokevirtual hasNestedScrollingParent : ()Z
    //   496: ifne -> 506
    //   499: aload_0
    //   500: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   503: invokevirtual clear : ()V
    //   506: aload_0
    //   507: getfield mScrollY : I
    //   510: iload #9
    //   512: isub
    //   513: istore #4
    //   515: aload_0
    //   516: iconst_0
    //   517: iload #4
    //   519: iconst_0
    //   520: iload_3
    //   521: iload #4
    //   523: isub
    //   524: aload_0
    //   525: getfield mScrollOffset : [I
    //   528: invokevirtual dispatchNestedScroll : (IIII[I)Z
    //   531: ifeq -> 580
    //   534: aload_0
    //   535: getfield mLastMotionY : I
    //   538: istore_3
    //   539: aload_0
    //   540: getfield mScrollOffset : [I
    //   543: astore_1
    //   544: aload_0
    //   545: iload_3
    //   546: aload_1
    //   547: iconst_1
    //   548: iaload
    //   549: isub
    //   550: putfield mLastMotionY : I
    //   553: aload_2
    //   554: fconst_0
    //   555: aload_1
    //   556: iconst_1
    //   557: iaload
    //   558: i2f
    //   559: invokevirtual offsetLocation : (FF)V
    //   562: aload_0
    //   563: aload_0
    //   564: getfield mNestedYOffset : I
    //   567: aload_0
    //   568: getfield mScrollOffset : [I
    //   571: iconst_1
    //   572: iaload
    //   573: iadd
    //   574: putfield mNestedYOffset : I
    //   577: goto -> 759
    //   580: iload #7
    //   582: ifeq -> 759
    //   585: iload #9
    //   587: iload_3
    //   588: iadd
    //   589: istore #7
    //   591: iload #7
    //   593: ifge -> 658
    //   596: aload_0
    //   597: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   600: astore #8
    //   602: iload_3
    //   603: i2f
    //   604: aload_0
    //   605: invokevirtual getHeight : ()I
    //   608: i2f
    //   609: fdiv
    //   610: fstore #11
    //   612: aload_1
    //   613: iload #5
    //   615: invokevirtual getX : (I)F
    //   618: aload_0
    //   619: invokevirtual getWidth : ()I
    //   622: i2f
    //   623: fdiv
    //   624: fstore #12
    //   626: aload #8
    //   628: fload #11
    //   630: fload #12
    //   632: invokevirtual onPull : (FF)V
    //   635: aload_0
    //   636: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   639: invokevirtual isFinished : ()Z
    //   642: ifne -> 655
    //   645: aload_0
    //   646: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   649: invokevirtual onRelease : ()V
    //   652: goto -> 723
    //   655: goto -> 723
    //   658: iload #7
    //   660: iload #6
    //   662: if_icmple -> 723
    //   665: aload_0
    //   666: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   669: astore #8
    //   671: iload_3
    //   672: i2f
    //   673: aload_0
    //   674: invokevirtual getHeight : ()I
    //   677: i2f
    //   678: fdiv
    //   679: fstore #11
    //   681: aload_1
    //   682: iload #5
    //   684: invokevirtual getX : (I)F
    //   687: aload_0
    //   688: invokevirtual getWidth : ()I
    //   691: i2f
    //   692: fdiv
    //   693: fstore #12
    //   695: aload #8
    //   697: fload #11
    //   699: fconst_1
    //   700: fload #12
    //   702: fsub
    //   703: invokevirtual onPull : (FF)V
    //   706: aload_0
    //   707: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   710: invokevirtual isFinished : ()Z
    //   713: ifne -> 723
    //   716: aload_0
    //   717: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   720: invokevirtual onRelease : ()V
    //   723: aload_0
    //   724: invokespecial shouldDisplayEdgeEffects : ()Z
    //   727: ifeq -> 759
    //   730: aload_0
    //   731: getfield mEdgeGlowTop : Landroid/widget/EdgeEffect;
    //   734: astore_1
    //   735: aload_1
    //   736: invokevirtual isFinished : ()Z
    //   739: ifeq -> 752
    //   742: aload_0
    //   743: getfield mEdgeGlowBottom : Landroid/widget/EdgeEffect;
    //   746: invokevirtual isFinished : ()Z
    //   749: ifne -> 759
    //   752: aload_0
    //   753: invokevirtual postInvalidateOnAnimation : ()V
    //   756: goto -> 759
    //   759: goto -> 984
    //   762: goto -> 984
    //   765: aload_0
    //   766: getfield mIsBeingDragged : Z
    //   769: ifeq -> 984
    //   772: aload_0
    //   773: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   776: astore_1
    //   777: aload_1
    //   778: sipush #1000
    //   781: aload_0
    //   782: getfield mMaximumVelocity : I
    //   785: i2f
    //   786: invokevirtual computeCurrentVelocity : (IF)V
    //   789: aload_1
    //   790: aload_0
    //   791: getfield mActivePointerId : I
    //   794: invokevirtual getYVelocity : (I)F
    //   797: f2i
    //   798: istore_3
    //   799: iload_3
    //   800: invokestatic abs : (I)I
    //   803: aload_0
    //   804: getfield mMinimumVelocity : I
    //   807: if_icmple -> 819
    //   810: aload_0
    //   811: iload_3
    //   812: ineg
    //   813: invokespecial flingWithNestedDispatch : (I)V
    //   816: goto -> 860
    //   819: aload_0
    //   820: getfield mScroller : Landroid/widget/OverScroller;
    //   823: astore_1
    //   824: aload_0
    //   825: getfield mScrollX : I
    //   828: istore_3
    //   829: aload_0
    //   830: getfield mScrollY : I
    //   833: istore #4
    //   835: aload_0
    //   836: invokespecial getScrollRange : ()I
    //   839: istore #7
    //   841: aload_1
    //   842: iload_3
    //   843: iload #4
    //   845: iconst_0
    //   846: iconst_0
    //   847: iconst_0
    //   848: iload #7
    //   850: invokevirtual springBack : (IIIIII)Z
    //   853: ifeq -> 860
    //   856: aload_0
    //   857: invokevirtual postInvalidateOnAnimation : ()V
    //   860: aload_0
    //   861: iconst_m1
    //   862: putfield mActivePointerId : I
    //   865: aload_0
    //   866: invokespecial endDrag : ()V
    //   869: goto -> 984
    //   872: aload_0
    //   873: invokevirtual getChildCount : ()I
    //   876: ifne -> 881
    //   879: iconst_0
    //   880: ireturn
    //   881: aload_0
    //   882: getfield mScroller : Landroid/widget/OverScroller;
    //   885: invokevirtual isFinished : ()Z
    //   888: iconst_1
    //   889: ixor
    //   890: istore #13
    //   892: aload_0
    //   893: iload #13
    //   895: putfield mIsBeingDragged : Z
    //   898: iload #13
    //   900: ifeq -> 922
    //   903: aload_0
    //   904: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   907: astore #8
    //   909: aload #8
    //   911: ifnull -> 922
    //   914: aload #8
    //   916: iconst_1
    //   917: invokeinterface requestDisallowInterceptTouchEvent : (Z)V
    //   922: aload_0
    //   923: getfield mScroller : Landroid/widget/OverScroller;
    //   926: invokevirtual isFinished : ()Z
    //   929: ifne -> 960
    //   932: aload_0
    //   933: getfield mScroller : Landroid/widget/OverScroller;
    //   936: invokevirtual abortAnimation : ()V
    //   939: aload_0
    //   940: getfield mFlingStrictSpan : Landroid/os/StrictMode$Span;
    //   943: astore #8
    //   945: aload #8
    //   947: ifnull -> 960
    //   950: aload #8
    //   952: invokevirtual finish : ()V
    //   955: aload_0
    //   956: aconst_null
    //   957: putfield mFlingStrictSpan : Landroid/os/StrictMode$Span;
    //   960: aload_0
    //   961: aload_1
    //   962: invokevirtual getY : ()F
    //   965: f2i
    //   966: putfield mLastMotionY : I
    //   969: aload_0
    //   970: aload_1
    //   971: iconst_0
    //   972: invokevirtual getPointerId : (I)I
    //   975: putfield mActivePointerId : I
    //   978: aload_0
    //   979: iconst_2
    //   980: invokevirtual startNestedScroll : (I)Z
    //   983: pop
    //   984: aload_0
    //   985: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   988: astore_1
    //   989: aload_1
    //   990: ifnull -> 998
    //   993: aload_1
    //   994: aload_2
    //   995: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   998: aload_2
    //   999: invokevirtual recycle : ()V
    //   1002: iconst_1
    //   1003: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #742	-> 0
    //   #744	-> 4
    //   #746	-> 9
    //   #748	-> 14
    //   #749	-> 21
    //   #751	-> 26
    //   #753	-> 36
    //   #892	-> 69
    //   #893	-> 74
    //   #886	-> 94
    //   #887	-> 99
    //   #888	-> 109
    //   #889	-> 118
    //   #877	-> 121
    //   #878	-> 135
    //   #879	-> 160
    //   #881	-> 164
    //   #882	-> 169
    //   #784	-> 176
    //   #785	-> 186
    //   #786	-> 192
    //   #787	-> 235
    //   #790	-> 238
    //   #791	-> 247
    //   #792	-> 255
    //   #793	-> 275
    //   #794	-> 285
    //   #795	-> 297
    //   #797	-> 312
    //   #798	-> 337
    //   #799	-> 343
    //   #800	-> 348
    //   #802	-> 356
    //   #803	-> 361
    //   #804	-> 366
    //   #806	-> 377
    //   #809	-> 385
    //   #811	-> 392
    //   #813	-> 405
    //   #814	-> 411
    //   #815	-> 417
    //   #816	-> 423
    //   #820	-> 450
    //   #825	-> 469
    //   #826	-> 492
    //   #828	-> 499
    //   #831	-> 506
    //   #832	-> 515
    //   #833	-> 515
    //   #834	-> 534
    //   #835	-> 553
    //   #836	-> 562
    //   #837	-> 580
    //   #838	-> 585
    //   #839	-> 591
    //   #840	-> 596
    //   #841	-> 612
    //   #840	-> 626
    //   #842	-> 635
    //   #843	-> 645
    //   #842	-> 655
    //   #845	-> 658
    //   #846	-> 665
    //   #847	-> 681
    //   #846	-> 695
    //   #848	-> 706
    //   #849	-> 716
    //   #852	-> 723
    //   #853	-> 735
    //   #854	-> 752
    //   #837	-> 759
    //   #857	-> 759
    //   #809	-> 762
    //   #860	-> 765
    //   #861	-> 772
    //   #862	-> 777
    //   #863	-> 789
    //   #865	-> 799
    //   #866	-> 810
    //   #867	-> 819
    //   #868	-> 835
    //   #867	-> 841
    //   #869	-> 856
    //   #872	-> 860
    //   #873	-> 865
    //   #874	-> 869
    //   #755	-> 872
    //   #756	-> 879
    //   #758	-> 881
    //   #759	-> 903
    //   #760	-> 909
    //   #761	-> 914
    //   #769	-> 922
    //   #770	-> 932
    //   #771	-> 939
    //   #772	-> 950
    //   #773	-> 955
    //   #778	-> 960
    //   #779	-> 969
    //   #780	-> 978
    //   #781	-> 984
    //   #897	-> 984
    //   #898	-> 993
    //   #900	-> 998
    //   #901	-> 1002
  }
  
  private void onSecondaryPointerUp(MotionEvent paramMotionEvent) {
    int i = (paramMotionEvent.getAction() & 0xFF00) >> 8;
    int j = paramMotionEvent.getPointerId(i);
    if (j == this.mActivePointerId) {
      if (i == 0) {
        j = 1;
      } else {
        j = 0;
      } 
      this.mLastMotionY = (int)paramMotionEvent.getY(j);
      this.mActivePointerId = paramMotionEvent.getPointerId(j);
      VelocityTracker velocityTracker = this.mVelocityTracker;
      if (velocityTracker != null)
        velocityTracker.clear(); 
    } 
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.getAction() == 8) {
      float f;
      if (paramMotionEvent.isFromSource(2)) {
        f = paramMotionEvent.getAxisValue(9);
      } else if (paramMotionEvent.isFromSource(4194304)) {
        f = paramMotionEvent.getAxisValue(26);
      } else {
        f = 0.0F;
      } 
      int i = Math.round(this.mVerticalScrollFactor * f);
      if (i != 0) {
        int j = getScrollRange();
        int k = this.mScrollY;
        int m = k - i;
        if (m < 0) {
          i = 0;
        } else {
          i = m;
          if (m > j)
            i = j; 
        } 
        if (i != k) {
          super.scrollTo(this.mScrollX, i);
          return true;
        } 
      } 
    } 
    return super.onGenericMotionEvent(paramMotionEvent);
  }
  
  protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    if (!this.mScroller.isFinished()) {
      int i = this.mScrollX;
      int j = this.mScrollY;
      this.mScrollX = paramInt1;
      this.mScrollY = paramInt2;
      invalidateParentIfNeeded();
      onScrollChanged(this.mScrollX, this.mScrollY, i, j);
      if (paramBoolean2)
        this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, 0, 0, getScrollRange()); 
      if (isOplusOSStyle())
        this.mScroller.springBack(this.mScrollX, this.mScrollY, 0, 0, 0, getScrollRange()); 
    } else {
      super.scrollTo(paramInt1, paramInt2);
    } 
    awakenScrollBars();
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    if (!isEnabled())
      return false; 
    if (paramInt != 4096)
      if (paramInt != 8192 && paramInt != 16908344) {
        if (paramInt != 16908346)
          return false; 
      } else {
        paramInt = getHeight();
        int k = this.mPaddingBottom, m = this.mPaddingTop;
        paramInt = Math.max(this.mScrollY - paramInt - k - m, 0);
        if (paramInt != this.mScrollY) {
          smoothScrollTo(0, paramInt);
          return true;
        } 
        return false;
      }  
    paramInt = getHeight();
    int i = this.mPaddingBottom, j = this.mPaddingTop;
    paramInt = Math.min(this.mScrollY + paramInt - i - j, getScrollRange());
    if (paramInt != this.mScrollY) {
      smoothScrollTo(0, paramInt);
      return true;
    } 
    return false;
  }
  
  public CharSequence getAccessibilityClassName() {
    return ScrollView.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (isEnabled()) {
      int i = getScrollRange();
      if (i > 0) {
        paramAccessibilityNodeInfo.setScrollable(true);
        if (this.mScrollY > 0) {
          paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
          paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP);
        } 
        if (this.mScrollY < i) {
          paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
          paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN);
        } 
      } 
    } 
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    boolean bool;
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    if (getScrollRange() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    paramAccessibilityEvent.setScrollable(bool);
    paramAccessibilityEvent.setMaxScrollX(this.mScrollX);
    paramAccessibilityEvent.setMaxScrollY(getScrollRange());
  }
  
  private int getScrollRange() {
    int i = 0;
    if (getChildCount() > 0) {
      View view = getChildAt(0);
      int j = view.getHeight(), k = getHeight(), m = this.mPaddingBottom;
      i = this.mPaddingTop;
      i = Math.max(0, j - k - m - i);
    } 
    return i;
  }
  
  private View findFocusableViewInBounds(boolean paramBoolean, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: iconst_2
    //   2: invokevirtual getFocusables : (I)Ljava/util/ArrayList;
    //   5: astore #4
    //   7: aconst_null
    //   8: astore #5
    //   10: iconst_0
    //   11: istore #6
    //   13: aload #4
    //   15: invokeinterface size : ()I
    //   20: istore #7
    //   22: iconst_0
    //   23: istore #8
    //   25: iload #8
    //   27: iload #7
    //   29: if_icmpge -> 254
    //   32: aload #4
    //   34: iload #8
    //   36: invokeinterface get : (I)Ljava/lang/Object;
    //   41: checkcast android/view/View
    //   44: astore #9
    //   46: aload #9
    //   48: invokevirtual getTop : ()I
    //   51: istore #10
    //   53: aload #9
    //   55: invokevirtual getBottom : ()I
    //   58: istore #11
    //   60: aload #5
    //   62: astore #12
    //   64: iload #6
    //   66: istore #13
    //   68: iload_2
    //   69: iload #11
    //   71: if_icmpge -> 240
    //   74: aload #5
    //   76: astore #12
    //   78: iload #6
    //   80: istore #13
    //   82: iload #10
    //   84: iload_3
    //   85: if_icmpge -> 240
    //   88: iconst_0
    //   89: istore #14
    //   91: iload_2
    //   92: iload #10
    //   94: if_icmpge -> 109
    //   97: iload #11
    //   99: iload_3
    //   100: if_icmpge -> 109
    //   103: iconst_1
    //   104: istore #15
    //   106: goto -> 112
    //   109: iconst_0
    //   110: istore #15
    //   112: aload #5
    //   114: ifnonnull -> 128
    //   117: aload #9
    //   119: astore #12
    //   121: iload #15
    //   123: istore #13
    //   125: goto -> 240
    //   128: iload_1
    //   129: ifeq -> 142
    //   132: iload #10
    //   134: aload #5
    //   136: invokevirtual getTop : ()I
    //   139: if_icmplt -> 156
    //   142: iload_1
    //   143: ifne -> 162
    //   146: iload #11
    //   148: aload #5
    //   150: invokevirtual getBottom : ()I
    //   153: if_icmple -> 162
    //   156: iconst_1
    //   157: istore #14
    //   159: goto -> 162
    //   162: iload #6
    //   164: ifeq -> 204
    //   167: aload #5
    //   169: astore #12
    //   171: iload #6
    //   173: istore #13
    //   175: iload #15
    //   177: ifeq -> 240
    //   180: aload #5
    //   182: astore #12
    //   184: iload #6
    //   186: istore #13
    //   188: iload #14
    //   190: ifeq -> 240
    //   193: aload #9
    //   195: astore #12
    //   197: iload #6
    //   199: istore #13
    //   201: goto -> 240
    //   204: iload #15
    //   206: ifeq -> 219
    //   209: aload #9
    //   211: astore #12
    //   213: iconst_1
    //   214: istore #13
    //   216: goto -> 240
    //   219: aload #5
    //   221: astore #12
    //   223: iload #6
    //   225: istore #13
    //   227: iload #14
    //   229: ifeq -> 240
    //   232: aload #9
    //   234: astore #12
    //   236: iload #6
    //   238: istore #13
    //   240: iinc #8, 1
    //   243: aload #12
    //   245: astore #5
    //   247: iload #13
    //   249: istore #6
    //   251: goto -> 25
    //   254: aload #5
    //   256: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1077	-> 0
    //   #1078	-> 7
    //   #1087	-> 10
    //   #1089	-> 13
    //   #1090	-> 22
    //   #1091	-> 32
    //   #1092	-> 46
    //   #1093	-> 53
    //   #1095	-> 60
    //   #1101	-> 88
    //   #1104	-> 112
    //   #1106	-> 117
    //   #1107	-> 121
    //   #1109	-> 128
    //   #1110	-> 132
    //   #1112	-> 146
    //   #1114	-> 162
    //   #1115	-> 167
    //   #1121	-> 193
    //   #1124	-> 204
    //   #1126	-> 209
    //   #1127	-> 213
    //   #1128	-> 219
    //   #1133	-> 232
    //   #1090	-> 240
    //   #1140	-> 254
  }
  
  public boolean pageScroll(int paramInt) {
    int i;
    if (paramInt == 130) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = getHeight();
    if (i) {
      this.mTempRect.top = getScrollY() + j;
      i = getChildCount();
      if (i > 0) {
        View view = getChildAt(i - 1);
        if (this.mTempRect.top + j > view.getBottom())
          this.mTempRect.top = view.getBottom() - j; 
      } 
    } else {
      this.mTempRect.top = getScrollY() - j;
      if (this.mTempRect.top < 0)
        this.mTempRect.top = 0; 
    } 
    Rect rect = this.mTempRect;
    rect.bottom = rect.top + j;
    return scrollAndFocus(paramInt, this.mTempRect.top, this.mTempRect.bottom);
  }
  
  public boolean fullScroll(int paramInt) {
    int i;
    if (paramInt == 130) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = getHeight();
    this.mTempRect.top = 0;
    this.mTempRect.bottom = j;
    if (i) {
      i = getChildCount();
      if (i > 0) {
        View view = getChildAt(i - 1);
        this.mTempRect.bottom = view.getBottom() + this.mPaddingBottom;
        Rect rect = this.mTempRect;
        rect.top = rect.bottom - j;
      } 
    } 
    return scrollAndFocus(paramInt, this.mTempRect.top, this.mTempRect.bottom);
  }
  
  private boolean scrollAndFocus(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool2, bool1 = true;
    int i = getHeight();
    int j = getScrollY();
    i = j + i;
    if (paramInt1 == 33) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    View view1 = findFocusableViewInBounds(bool2, paramInt2, paramInt3);
    View view2 = view1;
    if (view1 == null)
      view2 = this; 
    if (paramInt2 >= j && paramInt3 <= i) {
      bool2 = false;
    } else {
      if (bool2) {
        paramInt2 -= j;
      } else {
        paramInt2 = paramInt3 - i;
      } 
      doScrollY(paramInt2);
      bool2 = bool1;
    } 
    if (view2 != findFocus())
      view2.requestFocus(paramInt1); 
    return bool2;
  }
  
  public boolean arrowScroll(int paramInt) {
    View view1 = findFocus();
    View view2 = view1;
    if (view1 == this)
      view2 = null; 
    view1 = FocusFinder.getInstance().findNextFocus(this, view2, paramInt);
    int i = getMaxScrollAmount();
    if (view1 != null && isWithinDeltaOfScreen(view1, i, getHeight())) {
      view1.getDrawingRect(this.mTempRect);
      offsetDescendantRectToMyCoords(view1, this.mTempRect);
      int j = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
      doScrollY(j);
      view1.requestFocus(paramInt);
    } else {
      int j, k = i;
      if (paramInt == 33 && getScrollY() < k) {
        j = getScrollY();
      } else {
        j = k;
        if (paramInt == 130) {
          j = k;
          if (getChildCount() > 0) {
            int m = getChildAt(0).getBottom();
            int n = getScrollY() + getHeight() - this.mPaddingBottom;
            j = k;
            if (m - n < i)
              j = m - n; 
          } 
        } 
      } 
      if (j == 0)
        return false; 
      if (paramInt == 130) {
        paramInt = j;
      } else {
        paramInt = -j;
      } 
      doScrollY(paramInt);
    } 
    if (view2 != null && view2.isFocused() && 
      isOffScreen(view2)) {
      paramInt = getDescendantFocusability();
      setDescendantFocusability(131072);
      requestFocus();
      setDescendantFocusability(paramInt);
    } 
    return true;
  }
  
  private boolean isOffScreen(View paramView) {
    return isWithinDeltaOfScreen(paramView, 0, getHeight()) ^ true;
  }
  
  private boolean isWithinDeltaOfScreen(View paramView, int paramInt1, int paramInt2) {
    paramView.getDrawingRect(this.mTempRect);
    offsetDescendantRectToMyCoords(paramView, this.mTempRect);
    if (this.mTempRect.bottom + paramInt1 >= getScrollY()) {
      int i = this.mTempRect.top;
      if (i - paramInt1 <= getScrollY() + paramInt2)
        return true; 
    } 
    return false;
  }
  
  private void doScrollY(int paramInt) {
    if (paramInt != 0)
      if (this.mSmoothScrollingEnabled) {
        smoothScrollBy(0, paramInt);
      } else {
        scrollBy(0, paramInt);
      }  
  }
  
  public final void smoothScrollBy(int paramInt1, int paramInt2) {
    if (getChildCount() == 0)
      return; 
    long l1 = AnimationUtils.currentAnimationTimeMillis(), l2 = this.mLastScroll;
    if (l1 - l2 > 250L) {
      int i = getHeight(), j = this.mPaddingBottom, k = this.mPaddingTop;
      paramInt1 = getChildAt(0).getHeight();
      j = Math.max(0, paramInt1 - i - j - k);
      paramInt1 = this.mScrollY;
      paramInt2 = Math.max(0, Math.min(paramInt1 + paramInt2, j));
      this.mScroller.startScroll(this.mScrollX, paramInt1, 0, paramInt2 - paramInt1);
      postInvalidateOnAnimation();
    } else {
      if (!this.mScroller.isFinished()) {
        this.mScroller.abortAnimation();
        StrictMode.Span span = this.mFlingStrictSpan;
        if (span != null) {
          span.finish();
          this.mFlingStrictSpan = null;
        } 
      } 
      scrollBy(paramInt1, paramInt2);
    } 
    this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
  }
  
  public final void smoothScrollTo(int paramInt1, int paramInt2) {
    smoothScrollBy(paramInt1 - this.mScrollX, paramInt2 - this.mScrollY);
  }
  
  protected int computeVerticalScrollRange() {
    int i = getChildCount();
    int j = getHeight() - this.mPaddingBottom - this.mPaddingTop;
    if (i == 0)
      return j; 
    i = getChildAt(0).getBottom();
    int k = this.mScrollY;
    int m = Math.max(0, i - j);
    if (k < 0) {
      j = i - k;
    } else {
      j = i;
      if (k > m)
        j = i + k - m; 
    } 
    return j;
  }
  
  protected int computeVerticalScrollOffset() {
    return Math.max(0, super.computeVerticalScrollOffset());
  }
  
  protected void measureChild(View paramView, int paramInt1, int paramInt2) {
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    paramInt1 = getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight, layoutParams.width);
    int i = this.mPaddingTop, j = this.mPaddingBottom;
    paramInt2 = Math.max(0, View.MeasureSpec.getSize(paramInt2) - i + j);
    paramInt2 = View.MeasureSpec.makeSafeMeasureSpec(paramInt2, 0);
    paramView.measure(paramInt1, paramInt2);
  }
  
  protected void measureChildWithMargins(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    paramInt1 = getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + paramInt2, marginLayoutParams.width);
    int i = this.mPaddingTop;
    paramInt2 = this.mPaddingBottom;
    int j = marginLayoutParams.topMargin, k = marginLayoutParams.bottomMargin;
    paramInt2 = Math.max(0, View.MeasureSpec.getSize(paramInt3) - i + paramInt2 + j + k + paramInt4);
    paramInt2 = View.MeasureSpec.makeSafeMeasureSpec(paramInt2, 0);
    paramView.measure(paramInt1, paramInt2);
  }
  
  public void computeScroll() {
    if (this.mScroller.computeScrollOffset()) {
      int i = this.mScrollX;
      int j = this.mScrollY;
      int k = this.mScroller.getCurrX();
      int m = this.mScroller.getCurrY();
      if (i != k || j != m) {
        int n = getScrollRange();
        int i1 = getOverScrollMode();
        boolean bool1 = true, bool2 = bool1;
        if (i1 != 0)
          if (i1 == 1 && n > 0) {
            bool2 = bool1;
          } else {
            bool2 = false;
          }  
        overScrollBy(k - i, m - j, i, j, 0, n, 0, this.mOverflingDistance, false);
        onScrollChanged(this.mScrollX, this.mScrollY, i, j);
        if (bool2)
          if (m < 0 && j >= 0) {
            this.mEdgeGlowTop.onAbsorb((int)this.mScroller.getCurrVelocity());
          } else if (m > n && j <= n) {
            this.mEdgeGlowBottom.onAbsorb((int)this.mScroller.getCurrVelocity());
          }  
      } 
      if (!awakenScrollBars())
        postInvalidateOnAnimation(); 
    } else {
      StrictMode.Span span = this.mFlingStrictSpan;
      if (span != null) {
        span.finish();
        this.mFlingStrictSpan = null;
      } 
    } 
  }
  
  public void scrollToDescendant(View paramView) {
    if (!this.mIsLayoutDirty) {
      paramView.getDrawingRect(this.mTempRect);
      offsetDescendantRectToMyCoords(paramView, this.mTempRect);
      int i = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
      if (i != 0)
        scrollBy(0, i); 
    } else {
      this.mChildToScrollTo = paramView;
    } 
  }
  
  private boolean scrollToChildRect(Rect paramRect, boolean paramBoolean) {
    boolean bool;
    int i = computeScrollDeltaToGetChildRectOnScreen(paramRect);
    if (i != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool)
      if (paramBoolean) {
        scrollBy(0, i);
      } else {
        smoothScrollBy(0, i);
      }  
    return bool;
  }
  
  protected int computeScrollDeltaToGetChildRectOnScreen(Rect paramRect) {
    if (getChildCount() == 0)
      return 0; 
    int i = getHeight();
    int j = getScrollY();
    int k = j + i;
    int m = getVerticalFadingEdgeLength();
    int n = j;
    if (paramRect.top > 0)
      n = j + m; 
    j = k;
    if (paramRect.bottom < getChildAt(0).getHeight())
      j = k - m; 
    m = 0;
    if (paramRect.bottom > j && paramRect.top > n) {
      if (paramRect.height() > i) {
        k = 0 + paramRect.top - n;
      } else {
        k = 0 + paramRect.bottom - j;
      } 
      n = getChildAt(0).getBottom();
      k = Math.min(k, n - j);
    } else {
      k = m;
      if (paramRect.top < n) {
        k = m;
        if (paramRect.bottom < j) {
          if (paramRect.height() > i) {
            k = 0 - j - paramRect.bottom;
          } else {
            k = 0 - n - paramRect.top;
          } 
          k = Math.max(k, -getScrollY());
        } 
      } 
    } 
    return k;
  }
  
  public void requestChildFocus(View paramView1, View paramView2) {
    if (paramView2 != null && paramView2.getRevealOnFocusHint())
      if (!this.mIsLayoutDirty) {
        scrollToDescendant(paramView2);
      } else {
        this.mChildToScrollTo = paramView2;
      }  
    super.requestChildFocus(paramView1, paramView2);
  }
  
  protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect) {
    int i;
    View view;
    if (paramInt == 2) {
      i = 130;
    } else {
      i = paramInt;
      if (paramInt == 1)
        i = 33; 
    } 
    if (paramRect == null) {
      view = FocusFinder.getInstance().findNextFocus(this, null, i);
    } else {
      view = FocusFinder.getInstance().findNextFocusFromRect(this, paramRect, i);
    } 
    if (view == null)
      return false; 
    if (isOffScreen(view))
      return false; 
    return view.requestFocus(i, paramRect);
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean) {
    int i = paramView.getLeft(), j = paramView.getScrollX();
    int k = paramView.getTop(), m = paramView.getScrollY();
    paramRect.offset(i - j, k - m);
    return scrollToChildRect(paramRect, paramBoolean);
  }
  
  public void requestLayout() {
    this.mIsLayoutDirty = true;
    super.requestLayout();
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    StrictMode.Span span = this.mScrollStrictSpan;
    if (span != null) {
      span.finish();
      this.mScrollStrictSpan = null;
    } 
    span = this.mFlingStrictSpan;
    if (span != null) {
      span.finish();
      this.mFlingStrictSpan = null;
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    this.mIsLayoutDirty = false;
    View view = this.mChildToScrollTo;
    if (view != null && isViewDescendantOf(view, this))
      scrollToDescendant(this.mChildToScrollTo); 
    this.mChildToScrollTo = null;
    if (!isLaidOut()) {
      SavedState savedState = this.mSavedState;
      if (savedState != null) {
        this.mScrollY = savedState.scrollPosition;
        this.mSavedState = null;
      } 
      if (getChildCount() > 0) {
        paramInt1 = getChildAt(0).getMeasuredHeight();
      } else {
        paramInt1 = 0;
      } 
      paramInt1 = Math.max(0, paramInt1 - paramInt4 - paramInt2 - this.mPaddingBottom - this.mPaddingTop);
      if (this.mScrollY > paramInt1) {
        this.mScrollY = paramInt1;
      } else if (this.mScrollY < 0) {
        this.mScrollY = 0;
      } 
    } 
    scrollTo(this.mScrollX, this.mScrollY);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    View view = findFocus();
    if (view == null || this == view)
      return; 
    if (isWithinDeltaOfScreen(view, 0, paramInt4)) {
      view.getDrawingRect(this.mTempRect);
      offsetDescendantRectToMyCoords(view, this.mTempRect);
      paramInt1 = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
      doScrollY(paramInt1);
    } 
  }
  
  private static boolean isViewDescendantOf(View paramView1, View paramView2) {
    boolean bool = true;
    if (paramView1 == paramView2)
      return true; 
    ViewParent viewParent = paramView1.getParent();
    if (!(viewParent instanceof ViewGroup) || !isViewDescendantOf((View)viewParent, paramView2))
      bool = false; 
    return bool;
  }
  
  public void fling(int paramInt) {
    if (getChildCount() > 0) {
      int i = getHeight() - this.mPaddingBottom - this.mPaddingTop;
      int j = getChildAt(0).getHeight();
      OverScroller overScroller = this.mScroller;
      int k = this.mScrollX, m = this.mScrollY;
      j = Math.max(0, j - i);
      i /= 2;
      overScroller.fling(k, m, 0, paramInt, 0, 0, 0, j, 0, i);
      if (this.mFlingStrictSpan == null)
        this.mFlingStrictSpan = StrictMode.enterCriticalSpan("ScrollView-fling"); 
      postInvalidateOnAnimation();
    } 
  }
  
  private void flingWithNestedDispatch(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mScrollY : I
    //   4: ifgt -> 11
    //   7: iload_1
    //   8: ifle -> 31
    //   11: aload_0
    //   12: getfield mScrollY : I
    //   15: istore_2
    //   16: iload_2
    //   17: aload_0
    //   18: invokespecial getScrollRange : ()I
    //   21: if_icmplt -> 36
    //   24: iload_1
    //   25: ifge -> 31
    //   28: goto -> 36
    //   31: iconst_0
    //   32: istore_3
    //   33: goto -> 38
    //   36: iconst_1
    //   37: istore_3
    //   38: aload_0
    //   39: fconst_0
    //   40: iload_1
    //   41: i2f
    //   42: invokevirtual dispatchNestedPreFling : (FF)Z
    //   45: ifne -> 73
    //   48: aload_0
    //   49: fconst_0
    //   50: iload_1
    //   51: i2f
    //   52: iload_3
    //   53: invokevirtual dispatchNestedFling : (FFZ)Z
    //   56: pop
    //   57: iload_3
    //   58: ifne -> 68
    //   61: aload_0
    //   62: invokevirtual isOplusOSStyle : ()Z
    //   65: ifeq -> 73
    //   68: aload_0
    //   69: iload_1
    //   70: invokevirtual fling : (I)V
    //   73: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1778	-> 0
    //   #1779	-> 16
    //   #1780	-> 38
    //   #1781	-> 48
    //   #1787	-> 57
    //   #1789	-> 68
    //   #1792	-> 73
  }
  
  private void endDrag() {
    this.mIsBeingDragged = false;
    recycleVelocityTracker();
    if (shouldDisplayEdgeEffects()) {
      this.mEdgeGlowTop.onRelease();
      this.mEdgeGlowBottom.onRelease();
    } 
    StrictMode.Span span = this.mScrollStrictSpan;
    if (span != null) {
      span.finish();
      this.mScrollStrictSpan = null;
    } 
  }
  
  public void scrollTo(int paramInt1, int paramInt2) {
    if (getChildCount() > 0) {
      View view = getChildAt(0);
      paramInt1 = clamp(paramInt1, getWidth() - this.mPaddingRight - this.mPaddingLeft, view.getWidth());
      paramInt2 = clamp(paramInt2, getHeight() - this.mPaddingBottom - this.mPaddingTop, view.getHeight());
      if (paramInt1 != this.mScrollX || paramInt2 != this.mScrollY)
        super.scrollTo(paramInt1, paramInt2); 
    } 
  }
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt) {
    boolean bool;
    if ((paramInt & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt) {
    super.onNestedScrollAccepted(paramView1, paramView2, paramInt);
    startNestedScroll(2);
  }
  
  public void onStopNestedScroll(View paramView) {
    super.onStopNestedScroll(paramView);
  }
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt1 = this.mScrollY;
    scrollBy(0, paramInt4);
    paramInt1 = this.mScrollY - paramInt1;
    dispatchNestedScroll(0, paramInt1, 0, paramInt4 - paramInt1, (int[])null);
  }
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    if (!paramBoolean) {
      flingWithNestedDispatch((int)paramFloat2);
      return true;
    } 
    return false;
  }
  
  public void draw(Canvas paramCanvas) {
    super.draw(paramCanvas);
    if (shouldDisplayEdgeEffects() && !isOplusOSStyle()) {
      int i = this.mScrollY;
      boolean bool = getClipToPadding();
      if (!this.mEdgeGlowTop.isFinished()) {
        int k, m;
        float f1, f2;
        int j = paramCanvas.save();
        if (bool) {
          k = getWidth() - this.mPaddingLeft - this.mPaddingRight;
          m = getHeight() - this.mPaddingTop - this.mPaddingBottom;
          f1 = this.mPaddingLeft;
          f2 = this.mPaddingTop;
        } else {
          k = getWidth();
          m = getHeight();
          f1 = 0.0F;
          f2 = 0.0F;
        } 
        paramCanvas.translate(f1, Math.min(0, i) + f2);
        this.mEdgeGlowTop.setSize(k, m);
        if (this.mEdgeGlowTop.draw(paramCanvas))
          postInvalidateOnAnimation(); 
        paramCanvas.restoreToCount(j);
      } 
      EdgeEffect edgeEffect = this.mEdgeGlowBottom;
      if (edgeEffect != null && !edgeEffect.isFinished()) {
        int k, m;
        float f1, f2;
        int j = paramCanvas.save();
        if (bool) {
          m = getWidth() - this.mPaddingLeft - this.mPaddingRight;
          k = getHeight() - this.mPaddingTop - this.mPaddingBottom;
          f2 = this.mPaddingLeft;
          f1 = this.mPaddingTop;
        } else {
          m = getWidth();
          k = getHeight();
          f2 = 0.0F;
          f1 = 0.0F;
        } 
        float f3 = -m;
        float f4 = (Math.max(getScrollRange(), i) + k);
        paramCanvas.translate(f3 + f2, f4 + f1);
        paramCanvas.rotate(180.0F, m, 0.0F);
        this.mEdgeGlowBottom.setSize(m, k);
        if (this.mEdgeGlowBottom.draw(paramCanvas))
          postInvalidateOnAnimation(); 
        paramCanvas.restoreToCount(j);
      } 
    } 
  }
  
  private static int clamp(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt2 >= paramInt3 || paramInt1 < 0)
      return 0; 
    if (paramInt2 + paramInt1 > paramInt3)
      return paramInt3 - paramInt2; 
    return paramInt1;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if ((this.mContext.getApplicationInfo()).targetSdkVersion <= 18) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    this.mSavedState = (SavedState)paramParcelable;
    requestLayout();
  }
  
  protected Parcelable onSaveInstanceState() {
    if ((this.mContext.getApplicationInfo()).targetSdkVersion <= 18)
      return super.onSaveInstanceState(); 
    Parcelable parcelable = super.onSaveInstanceState();
    parcelable = new SavedState(parcelable);
    ((SavedState)parcelable).scrollPosition = this.mScrollY;
    return parcelable;
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("fillViewport", this.mFillViewport);
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState(ScrollView this$0) {
      super((Parcelable)this$0);
    }
    
    public SavedState(ScrollView this$0) {
      super((Parcel)this$0);
      this.scrollPosition = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.scrollPosition);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ScrollView.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" scrollPosition=");
      stringBuilder.append(this.scrollPosition);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    public int scrollPosition;
  }
}
