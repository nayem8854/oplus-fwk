package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.android.internal.widget.ViewPager;
import java.util.ArrayList;
import java.util.function.Predicate;

class DayPickerViewPager extends ViewPager {
  private final ArrayList<View> mMatchParentChildren = new ArrayList<>(1);
  
  public DayPickerViewPager(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public DayPickerViewPager(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public DayPickerViewPager(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public DayPickerViewPager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual populate : ()V
    //   4: aload_0
    //   5: invokevirtual getChildCount : ()I
    //   8: istore_3
    //   9: iload_1
    //   10: invokestatic getMode : (I)I
    //   13: ldc 1073741824
    //   15: if_icmpne -> 36
    //   18: iload_2
    //   19: invokestatic getMode : (I)I
    //   22: ldc 1073741824
    //   24: if_icmpeq -> 30
    //   27: goto -> 36
    //   30: iconst_0
    //   31: istore #4
    //   33: goto -> 39
    //   36: iconst_1
    //   37: istore #4
    //   39: iconst_0
    //   40: istore #5
    //   42: iconst_0
    //   43: istore #6
    //   45: iconst_0
    //   46: istore #7
    //   48: iconst_0
    //   49: istore #8
    //   51: iload #8
    //   53: iload_3
    //   54: if_icmpge -> 228
    //   57: aload_0
    //   58: iload #8
    //   60: invokevirtual getChildAt : (I)Landroid/view/View;
    //   63: astore #9
    //   65: iload #5
    //   67: istore #10
    //   69: iload #6
    //   71: istore #11
    //   73: iload #7
    //   75: istore #12
    //   77: aload #9
    //   79: invokevirtual getVisibility : ()I
    //   82: bipush #8
    //   84: if_icmpeq -> 210
    //   87: aload_0
    //   88: aload #9
    //   90: iload_1
    //   91: iload_2
    //   92: invokevirtual measureChild : (Landroid/view/View;II)V
    //   95: aload #9
    //   97: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   100: checkcast com/android/internal/widget/ViewPager$LayoutParams
    //   103: astore #13
    //   105: iload #6
    //   107: aload #9
    //   109: invokevirtual getMeasuredWidth : ()I
    //   112: invokestatic max : (II)I
    //   115: istore #6
    //   117: iload #5
    //   119: aload #9
    //   121: invokevirtual getMeasuredHeight : ()I
    //   124: invokestatic max : (II)I
    //   127: istore #5
    //   129: iload #7
    //   131: aload #9
    //   133: invokevirtual getMeasuredState : ()I
    //   136: invokestatic combineMeasuredStates : (II)I
    //   139: istore #7
    //   141: iload #5
    //   143: istore #10
    //   145: iload #6
    //   147: istore #11
    //   149: iload #7
    //   151: istore #12
    //   153: iload #4
    //   155: ifeq -> 210
    //   158: aload #13
    //   160: getfield width : I
    //   163: iconst_m1
    //   164: if_icmpeq -> 188
    //   167: iload #5
    //   169: istore #10
    //   171: iload #6
    //   173: istore #11
    //   175: iload #7
    //   177: istore #12
    //   179: aload #13
    //   181: getfield height : I
    //   184: iconst_m1
    //   185: if_icmpne -> 210
    //   188: aload_0
    //   189: getfield mMatchParentChildren : Ljava/util/ArrayList;
    //   192: aload #9
    //   194: invokevirtual add : (Ljava/lang/Object;)Z
    //   197: pop
    //   198: iload #7
    //   200: istore #12
    //   202: iload #6
    //   204: istore #11
    //   206: iload #5
    //   208: istore #10
    //   210: iinc #8, 1
    //   213: iload #10
    //   215: istore #5
    //   217: iload #11
    //   219: istore #6
    //   221: iload #12
    //   223: istore #7
    //   225: goto -> 51
    //   228: aload_0
    //   229: invokevirtual getPaddingLeft : ()I
    //   232: istore #11
    //   234: aload_0
    //   235: invokevirtual getPaddingRight : ()I
    //   238: istore #4
    //   240: aload_0
    //   241: invokevirtual getPaddingTop : ()I
    //   244: istore #8
    //   246: aload_0
    //   247: invokevirtual getPaddingBottom : ()I
    //   250: istore #10
    //   252: iload #5
    //   254: iload #8
    //   256: iload #10
    //   258: iadd
    //   259: iadd
    //   260: aload_0
    //   261: invokevirtual getSuggestedMinimumHeight : ()I
    //   264: invokestatic max : (II)I
    //   267: istore #8
    //   269: iload #6
    //   271: iload #11
    //   273: iload #4
    //   275: iadd
    //   276: iadd
    //   277: aload_0
    //   278: invokevirtual getSuggestedMinimumWidth : ()I
    //   281: invokestatic max : (II)I
    //   284: istore #5
    //   286: aload_0
    //   287: invokevirtual getForeground : ()Landroid/graphics/drawable/Drawable;
    //   290: astore #9
    //   292: iload #8
    //   294: istore #4
    //   296: iload #5
    //   298: istore #6
    //   300: aload #9
    //   302: ifnull -> 329
    //   305: iload #8
    //   307: aload #9
    //   309: invokevirtual getMinimumHeight : ()I
    //   312: invokestatic max : (II)I
    //   315: istore #4
    //   317: iload #5
    //   319: aload #9
    //   321: invokevirtual getMinimumWidth : ()I
    //   324: invokestatic max : (II)I
    //   327: istore #6
    //   329: iload #6
    //   331: iload_1
    //   332: iload #7
    //   334: invokestatic resolveSizeAndState : (III)I
    //   337: istore #6
    //   339: iload #4
    //   341: iload_2
    //   342: iload #7
    //   344: bipush #16
    //   346: ishl
    //   347: invokestatic resolveSizeAndState : (III)I
    //   350: istore #7
    //   352: aload_0
    //   353: iload #6
    //   355: iload #7
    //   357: invokevirtual setMeasuredDimension : (II)V
    //   360: aload_0
    //   361: getfield mMatchParentChildren : Ljava/util/ArrayList;
    //   364: invokevirtual size : ()I
    //   367: istore #8
    //   369: iload #8
    //   371: iconst_1
    //   372: if_icmple -> 578
    //   375: iconst_0
    //   376: istore #7
    //   378: iload #7
    //   380: iload #8
    //   382: if_icmpge -> 578
    //   385: aload_0
    //   386: getfield mMatchParentChildren : Ljava/util/ArrayList;
    //   389: iload #7
    //   391: invokevirtual get : (I)Ljava/lang/Object;
    //   394: checkcast android/view/View
    //   397: astore #13
    //   399: aload #13
    //   401: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   404: checkcast com/android/internal/widget/ViewPager$LayoutParams
    //   407: astore #9
    //   409: aload #9
    //   411: getfield width : I
    //   414: iconst_m1
    //   415: if_icmpne -> 454
    //   418: aload_0
    //   419: invokevirtual getMeasuredWidth : ()I
    //   422: istore #4
    //   424: aload_0
    //   425: invokevirtual getPaddingLeft : ()I
    //   428: istore #6
    //   430: aload_0
    //   431: invokevirtual getPaddingRight : ()I
    //   434: istore #5
    //   436: iload #4
    //   438: iload #6
    //   440: isub
    //   441: iload #5
    //   443: isub
    //   444: ldc 1073741824
    //   446: invokestatic makeMeasureSpec : (II)I
    //   449: istore #4
    //   451: goto -> 486
    //   454: aload_0
    //   455: invokevirtual getPaddingLeft : ()I
    //   458: istore #6
    //   460: aload_0
    //   461: invokevirtual getPaddingRight : ()I
    //   464: istore #5
    //   466: aload #9
    //   468: getfield width : I
    //   471: istore #4
    //   473: iload_1
    //   474: iload #6
    //   476: iload #5
    //   478: iadd
    //   479: iload #4
    //   481: invokestatic getChildMeasureSpec : (III)I
    //   484: istore #4
    //   486: aload #9
    //   488: getfield height : I
    //   491: iconst_m1
    //   492: if_icmpne -> 531
    //   495: aload_0
    //   496: invokevirtual getMeasuredHeight : ()I
    //   499: istore #6
    //   501: aload_0
    //   502: invokevirtual getPaddingTop : ()I
    //   505: istore #11
    //   507: aload_0
    //   508: invokevirtual getPaddingBottom : ()I
    //   511: istore #5
    //   513: iload #6
    //   515: iload #11
    //   517: isub
    //   518: iload #5
    //   520: isub
    //   521: ldc 1073741824
    //   523: invokestatic makeMeasureSpec : (II)I
    //   526: istore #6
    //   528: goto -> 563
    //   531: aload_0
    //   532: invokevirtual getPaddingTop : ()I
    //   535: istore #5
    //   537: aload_0
    //   538: invokevirtual getPaddingBottom : ()I
    //   541: istore #6
    //   543: aload #9
    //   545: getfield height : I
    //   548: istore #11
    //   550: iload_2
    //   551: iload #5
    //   553: iload #6
    //   555: iadd
    //   556: iload #11
    //   558: invokestatic getChildMeasureSpec : (III)I
    //   561: istore #6
    //   563: aload #13
    //   565: iload #4
    //   567: iload #6
    //   569: invokevirtual measure : (II)V
    //   572: iinc #7, 1
    //   575: goto -> 378
    //   578: aload_0
    //   579: getfield mMatchParentChildren : Ljava/util/ArrayList;
    //   582: invokevirtual clear : ()V
    //   585: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #54	-> 0
    //   #57	-> 4
    //   #59	-> 9
    //   #60	-> 9
    //   #61	-> 18
    //   #63	-> 39
    //   #64	-> 42
    //   #65	-> 45
    //   #67	-> 48
    //   #68	-> 57
    //   #69	-> 65
    //   #70	-> 87
    //   #71	-> 95
    //   #72	-> 105
    //   #73	-> 117
    //   #74	-> 129
    //   #75	-> 141
    //   #76	-> 158
    //   #78	-> 188
    //   #67	-> 210
    //   #85	-> 228
    //   #86	-> 240
    //   #89	-> 252
    //   #90	-> 269
    //   #93	-> 286
    //   #94	-> 292
    //   #95	-> 305
    //   #96	-> 317
    //   #99	-> 329
    //   #100	-> 339
    //   #99	-> 352
    //   #103	-> 360
    //   #104	-> 369
    //   #105	-> 375
    //   #106	-> 385
    //   #108	-> 399
    //   #112	-> 409
    //   #113	-> 418
    //   #114	-> 418
    //   #113	-> 436
    //   #117	-> 454
    //   #118	-> 454
    //   #117	-> 473
    //   #122	-> 486
    //   #123	-> 495
    //   #124	-> 495
    //   #123	-> 513
    //   #127	-> 531
    //   #128	-> 531
    //   #127	-> 550
    //   #132	-> 563
    //   #105	-> 572
    //   #136	-> 578
    //   #137	-> 585
  }
  
  protected <T extends View> T findViewByPredicateTraversal(Predicate<View> paramPredicate, View paramView) {
    if (paramPredicate.test(this))
      return (T)this; 
    DayPickerPagerAdapter dayPickerPagerAdapter = (DayPickerPagerAdapter)getAdapter();
    SimpleMonthView simpleMonthView = dayPickerPagerAdapter.getView(getCurrent());
    if (simpleMonthView != paramView && simpleMonthView != null) {
      T t = (T)simpleMonthView.findViewByPredicate(paramPredicate);
      if (t != null)
        return t; 
    } 
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view != paramView && view != simpleMonthView) {
        view = view.findViewByPredicate(paramPredicate);
        if (view != null)
          return (T)view; 
      } 
    } 
    return null;
  }
}
