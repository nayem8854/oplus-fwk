package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.icu.util.Calendar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

class YearPickerView extends ListView {
  private final YearAdapter mAdapter;
  
  private final int mChildSize;
  
  private OnYearSelectedListener mOnYearSelectedListener;
  
  private final int mViewSize;
  
  public YearPickerView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842868);
  }
  
  public YearPickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public YearPickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(-1, -2);
    setLayoutParams(layoutParams);
    Resources resources = paramContext.getResources();
    this.mViewSize = resources.getDimensionPixelOffset(17105137);
    this.mChildSize = resources.getDimensionPixelOffset(17105138);
    setOnItemClickListener((AdapterView.OnItemClickListener)new Object(this));
    YearAdapter yearAdapter = new YearAdapter(getContext());
    setAdapter(yearAdapter);
  }
  
  public void setOnYearSelectedListener(OnYearSelectedListener paramOnYearSelectedListener) {
    this.mOnYearSelectedListener = paramOnYearSelectedListener;
  }
  
  public void setYear(int paramInt) {
    this.mAdapter.setSelection(paramInt);
    post((Runnable)new Object(this, paramInt));
  }
  
  public void setSelectionCentered(int paramInt) {
    int i = this.mViewSize / 2, j = this.mChildSize / 2;
    setSelectionFromTop(paramInt, i - j);
  }
  
  public void setRange(Calendar paramCalendar1, Calendar paramCalendar2) {
    this.mAdapter.setRange(paramCalendar1, paramCalendar2);
  }
  
  class OnYearSelectedListener {
    public abstract void onYearChanged(YearPickerView param1YearPickerView, int param1Int);
  }
  
  class YearAdapter extends BaseAdapter {
    private static final int ITEM_LAYOUT = 17367356;
    
    private static final int ITEM_TEXT_ACTIVATED_APPEARANCE = 16974770;
    
    private static final int ITEM_TEXT_APPEARANCE = 16974769;
    
    private int mActivatedYear;
    
    private int mCount;
    
    private final LayoutInflater mInflater;
    
    private int mMinYear;
    
    public YearAdapter(YearPickerView this$0) {
      this.mInflater = LayoutInflater.from((Context)this$0);
    }
    
    public void setRange(Calendar param1Calendar1, Calendar param1Calendar2) {
      int i = param1Calendar1.get(1);
      int j = param1Calendar2.get(1) - i + 1;
      if (this.mMinYear != i || this.mCount != j) {
        this.mMinYear = i;
        this.mCount = j;
        notifyDataSetInvalidated();
      } 
    }
    
    public boolean setSelection(int param1Int) {
      if (this.mActivatedYear != param1Int) {
        this.mActivatedYear = param1Int;
        notifyDataSetChanged();
        return true;
      } 
      return false;
    }
    
    public int getCount() {
      return this.mCount;
    }
    
    public Integer getItem(int param1Int) {
      return Integer.valueOf(getYearForPosition(param1Int));
    }
    
    public long getItemId(int param1Int) {
      return getYearForPosition(param1Int);
    }
    
    public int getPositionForYear(int param1Int) {
      return param1Int - this.mMinYear;
    }
    
    public int getYearForPosition(int param1Int) {
      return this.mMinYear + param1Int;
    }
    
    public boolean hasStableIds() {
      return true;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      boolean bool1;
      boolean bool = true;
      if (param1View == null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (bool1) {
        param1View = this.mInflater.inflate(17367356, param1ViewGroup, false);
      } else {
        param1View = param1View;
      } 
      int i = getYearForPosition(param1Int);
      if (this.mActivatedYear != i)
        bool = false; 
      if (bool1 || param1View.isActivated() != bool) {
        if (bool) {
          param1Int = 16974770;
        } else {
          param1Int = 16974769;
        } 
        param1View.setTextAppearance(param1Int);
        param1View.setActivated(bool);
      } 
      param1View.setText(Integer.toString(i));
      return param1View;
    }
    
    public int getItemViewType(int param1Int) {
      return 0;
    }
    
    public int getViewTypeCount() {
      return 1;
    }
    
    public boolean isEmpty() {
      return false;
    }
    
    public boolean areAllItemsEnabled() {
      return true;
    }
    
    public boolean isEnabled(int param1Int) {
      return true;
    }
  }
  
  public int getFirstPositionOffset() {
    View view = getChildAt(0);
    if (view == null)
      return 0; 
    return view.getTop();
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    if (paramAccessibilityEvent.getEventType() == 4096) {
      paramAccessibilityEvent.setFromIndex(0);
      paramAccessibilityEvent.setToIndex(0);
    } 
  }
}
