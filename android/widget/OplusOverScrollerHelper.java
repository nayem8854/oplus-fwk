package android.widget;

public class OplusOverScrollerHelper implements IOplusOverScrollerHelper {
  private boolean mForSpringOverScroller;
  
  protected OverScroller mScroller;
  
  public OplusOverScrollerHelper(OverScroller paramOverScroller) {
    this.mScroller = paramOverScroller;
    if (paramOverScroller instanceof SpringOverScroller)
      this.mForSpringOverScroller = true; 
  }
  
  public int getFinalX(int paramInt) {
    if (this.mForSpringOverScroller)
      paramInt = ((SpringOverScroller)this.mScroller).getOplusFinalX(); 
    return paramInt;
  }
  
  public int getFinalY(int paramInt) {
    if (this.mForSpringOverScroller)
      paramInt = ((SpringOverScroller)this.mScroller).getOplusFinalY(); 
    return paramInt;
  }
  
  public int getCurrX(int paramInt) {
    if (this.mForSpringOverScroller)
      paramInt = ((SpringOverScroller)this.mScroller).getOplusCurrX(); 
    return paramInt;
  }
  
  public int getCurrY(int paramInt) {
    if (this.mForSpringOverScroller)
      paramInt = ((SpringOverScroller)this.mScroller).getOplusCurrY(); 
    return paramInt;
  }
  
  public boolean setFriction(float paramFloat) {
    if (this.mForSpringOverScroller) {
      ((SpringOverScroller)this.mScroller).setOplusFriction(paramFloat);
      return true;
    } 
    return false;
  }
  
  public boolean isFinished(boolean paramBoolean) {
    if (this.mForSpringOverScroller)
      paramBoolean = ((SpringOverScroller)this.mScroller).isOplusFinished(); 
    return paramBoolean;
  }
}
