package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.android.internal.R;

public class ViewAnimator extends FrameLayout {
  int mWhichChild = 0;
  
  Animation mOutAnimation;
  
  Animation mInAnimation;
  
  boolean mFirstTime = true;
  
  boolean mAnimateFirstTime = true;
  
  public ViewAnimator(Context paramContext) {
    super(paramContext);
    initViewAnimator(paramContext, (AttributeSet)null);
  }
  
  public ViewAnimator(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewAnimator);
    saveAttributeDataForStyleable(paramContext, R.styleable.ViewAnimator, paramAttributeSet, typedArray, 0, 0);
    int i = typedArray.getResourceId(0, 0);
    if (i > 0)
      setInAnimation(paramContext, i); 
    i = typedArray.getResourceId(1, 0);
    if (i > 0)
      setOutAnimation(paramContext, i); 
    boolean bool = typedArray.getBoolean(2, true);
    setAnimateFirstView(bool);
    typedArray.recycle();
    initViewAnimator(paramContext, paramAttributeSet);
  }
  
  private void initViewAnimator(Context paramContext, AttributeSet paramAttributeSet) {
    if (paramAttributeSet == null) {
      this.mMeasureAllChildren = true;
      return;
    } 
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FrameLayout);
    saveAttributeDataForStyleable(paramContext, R.styleable.FrameLayout, paramAttributeSet, typedArray, 0, 0);
    boolean bool = typedArray.getBoolean(0, true);
    setMeasureAllChildren(bool);
    typedArray.recycle();
  }
  
  @RemotableViewMethod
  public void setDisplayedChild(int paramInt) {
    this.mWhichChild = paramInt;
    int i = getChildCount();
    boolean bool = true;
    if (paramInt >= i) {
      this.mWhichChild = 0;
    } else if (paramInt < 0) {
      this.mWhichChild = getChildCount() - 1;
    } 
    if (getFocusedChild() != null) {
      paramInt = bool;
    } else {
      paramInt = 0;
    } 
    showOnly(this.mWhichChild);
    if (paramInt != 0)
      requestFocus(2); 
  }
  
  public int getDisplayedChild() {
    return this.mWhichChild;
  }
  
  @RemotableViewMethod
  public void showNext() {
    setDisplayedChild(this.mWhichChild + 1);
  }
  
  @RemotableViewMethod
  public void showPrevious() {
    setDisplayedChild(this.mWhichChild - 1);
  }
  
  void showOnly(int paramInt, boolean paramBoolean) {
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (b == paramInt) {
        if (paramBoolean) {
          Animation animation = this.mInAnimation;
          if (animation != null)
            view.startAnimation(animation); 
        } 
        view.setVisibility(0);
        this.mFirstTime = false;
      } else {
        if (paramBoolean && this.mOutAnimation != null && view.getVisibility() == 0) {
          view.startAnimation(this.mOutAnimation);
        } else if (view.getAnimation() == this.mInAnimation) {
          view.clearAnimation();
        } 
        view.setVisibility(8);
      } 
    } 
  }
  
  void showOnly(int paramInt) {
    boolean bool;
    if (!this.mFirstTime || this.mAnimateFirstTime) {
      bool = true;
    } else {
      bool = false;
    } 
    showOnly(paramInt, bool);
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    super.addView(paramView, paramInt, paramLayoutParams);
    if (getChildCount() == 1) {
      paramView.setVisibility(0);
    } else {
      paramView.setVisibility(8);
    } 
    if (paramInt >= 0) {
      int i = this.mWhichChild;
      if (i >= paramInt)
        setDisplayedChild(i + 1); 
    } 
  }
  
  public void removeAllViews() {
    super.removeAllViews();
    this.mWhichChild = 0;
    this.mFirstTime = true;
  }
  
  public void removeView(View paramView) {
    int i = indexOfChild(paramView);
    if (i >= 0)
      removeViewAt(i); 
  }
  
  public void removeViewAt(int paramInt) {
    super.removeViewAt(paramInt);
    int i = getChildCount();
    if (i == 0) {
      this.mWhichChild = 0;
      this.mFirstTime = true;
    } else {
      int j = this.mWhichChild;
      if (j >= i) {
        setDisplayedChild(i - 1);
      } else if (j == paramInt) {
        setDisplayedChild(j);
      } 
    } 
  }
  
  public void removeViewInLayout(View paramView) {
    removeView(paramView);
  }
  
  public void removeViews(int paramInt1, int paramInt2) {
    super.removeViews(paramInt1, paramInt2);
    if (getChildCount() == 0) {
      this.mWhichChild = 0;
      this.mFirstTime = true;
    } else {
      int i = this.mWhichChild;
      if (i >= paramInt1 && i < paramInt1 + paramInt2)
        setDisplayedChild(i); 
    } 
  }
  
  public void removeViewsInLayout(int paramInt1, int paramInt2) {
    removeViews(paramInt1, paramInt2);
  }
  
  public View getCurrentView() {
    return getChildAt(this.mWhichChild);
  }
  
  public Animation getInAnimation() {
    return this.mInAnimation;
  }
  
  public void setInAnimation(Animation paramAnimation) {
    this.mInAnimation = paramAnimation;
  }
  
  public Animation getOutAnimation() {
    return this.mOutAnimation;
  }
  
  public void setOutAnimation(Animation paramAnimation) {
    this.mOutAnimation = paramAnimation;
  }
  
  public void setInAnimation(Context paramContext, int paramInt) {
    setInAnimation(AnimationUtils.loadAnimation(paramContext, paramInt));
  }
  
  public void setOutAnimation(Context paramContext, int paramInt) {
    setOutAnimation(AnimationUtils.loadAnimation(paramContext, paramInt));
  }
  
  public boolean getAnimateFirstView() {
    return this.mAnimateFirstTime;
  }
  
  public void setAnimateFirstView(boolean paramBoolean) {
    this.mAnimateFirstTime = paramBoolean;
  }
  
  public int getBaseline() {
    int i;
    if (getCurrentView() != null) {
      i = getCurrentView().getBaseline();
    } else {
      i = super.getBaseline();
    } 
    return i;
  }
  
  public CharSequence getAccessibilityClassName() {
    return ViewAnimator.class.getName();
  }
}
