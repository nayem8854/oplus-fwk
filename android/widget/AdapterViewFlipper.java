package android.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Process;
import android.os.UserHandle;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import com.android.internal.R;

@RemoteView
public class AdapterViewFlipper extends AdapterViewAnimator {
  private int mFlipInterval = 10000;
  
  private boolean mAutoStart = false;
  
  private boolean mRunning = false;
  
  private boolean mStarted = false;
  
  private boolean mVisible = false;
  
  private boolean mUserPresent = true;
  
  private boolean mAdvancedByHost = false;
  
  private static final int DEFAULT_INTERVAL = 10000;
  
  private static final boolean LOGD = false;
  
  private static final String TAG = "ViewFlipper";
  
  private final Runnable mFlipRunnable;
  
  private final BroadcastReceiver mReceiver;
  
  public AdapterViewFlipper(Context paramContext) {
    super(paramContext);
    this.mReceiver = (BroadcastReceiver)new Object(this);
    this.mFlipRunnable = (Runnable)new Object(this);
  }
  
  public AdapterViewFlipper(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public AdapterViewFlipper(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AdapterViewFlipper(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mReceiver = (BroadcastReceiver)new Object(this);
    this.mFlipRunnable = (Runnable)new Object(this);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AdapterViewFlipper, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.AdapterViewFlipper, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mFlipInterval = typedArray.getInt(0, 10000);
    this.mAutoStart = typedArray.getBoolean(1, false);
    this.mLoopViews = true;
    typedArray.recycle();
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.SCREEN_OFF");
    intentFilter.addAction("android.intent.action.USER_PRESENT");
    Context context = getContext();
    BroadcastReceiver broadcastReceiver = this.mReceiver;
    UserHandle userHandle = Process.myUserHandle();
    Handler handler = getHandler();
    context.registerReceiverAsUser(broadcastReceiver, userHandle, intentFilter, null, handler);
    if (this.mAutoStart)
      startFlipping(); 
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    this.mVisible = false;
    getContext().unregisterReceiver(this.mReceiver);
    updateRunning();
  }
  
  protected void onWindowVisibilityChanged(int paramInt) {
    boolean bool;
    super.onWindowVisibilityChanged(paramInt);
    if (paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mVisible = bool;
    updateRunning(false);
  }
  
  public void setAdapter(Adapter paramAdapter) {
    super.setAdapter(paramAdapter);
    updateRunning();
  }
  
  public int getFlipInterval() {
    return this.mFlipInterval;
  }
  
  public void setFlipInterval(int paramInt) {
    this.mFlipInterval = paramInt;
  }
  
  public void startFlipping() {
    this.mStarted = true;
    updateRunning();
  }
  
  public void stopFlipping() {
    this.mStarted = false;
    updateRunning();
  }
  
  @RemotableViewMethod
  public void showNext() {
    if (this.mRunning) {
      removeCallbacks(this.mFlipRunnable);
      postDelayed(this.mFlipRunnable, this.mFlipInterval);
    } 
    super.showNext();
  }
  
  @RemotableViewMethod
  public void showPrevious() {
    if (this.mRunning) {
      removeCallbacks(this.mFlipRunnable);
      postDelayed(this.mFlipRunnable, this.mFlipInterval);
    } 
    super.showPrevious();
  }
  
  private void updateRunning() {
    updateRunning(true);
  }
  
  private void updateRunning(boolean paramBoolean) {
    boolean bool;
    if (!this.mAdvancedByHost && this.mVisible && this.mStarted && this.mUserPresent && this.mAdapter != null) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool != this.mRunning) {
      if (bool) {
        showOnly(this.mWhichChild, paramBoolean);
        postDelayed(this.mFlipRunnable, this.mFlipInterval);
      } else {
        removeCallbacks(this.mFlipRunnable);
      } 
      this.mRunning = bool;
    } 
  }
  
  public boolean isFlipping() {
    return this.mStarted;
  }
  
  public void setAutoStart(boolean paramBoolean) {
    this.mAutoStart = paramBoolean;
  }
  
  public boolean isAutoStart() {
    return this.mAutoStart;
  }
  
  public void fyiWillBeAdvancedByHostKThx() {
    this.mAdvancedByHost = true;
    updateRunning(false);
  }
  
  public CharSequence getAccessibilityClassName() {
    return AdapterViewFlipper.class.getName();
  }
}
