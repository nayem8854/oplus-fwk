package android.widget;

import android.app.INotificationManager;
import android.app.ITransientNotification;
import android.app.ITransientNotificationCallback;
import android.compat.Compatibility;
import android.content.Context;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.IAccessibilityManager;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Toast {
  private static final long CHANGE_TEXT_TOASTS_IN_THE_SYSTEM = 147798919L;
  
  public static final int LENGTH_LONG = 1;
  
  public static final int LENGTH_SHORT = 0;
  
  static final String TAG = "Toast";
  
  static final boolean localLOGV = false;
  
  private static INotificationManager sService;
  
  private final List<Callback> mCallbacks;
  
  private final Context mContext;
  
  int mDuration;
  
  private final Handler mHandler;
  
  private View mNextView;
  
  final TN mTN;
  
  private CharSequence mText;
  
  private final Binder mToken;
  
  public Toast(Context paramContext) {
    this(paramContext, null);
  }
  
  public Toast(Context paramContext, Looper paramLooper) {
    this.mContext = paramContext;
    this.mToken = new Binder();
    paramLooper = getLooper(paramLooper);
    this.mHandler = new Handler(paramLooper);
    this.mCallbacks = new ArrayList<>();
    TN tN = new TN(paramContext, paramContext.getPackageName(), this.mToken, this.mCallbacks, paramLooper);
    tN.mY = paramContext.getResources().getDimensionPixelSize(17105536);
    this.mTN.mGravity = paramContext.getResources().getInteger(17694911);
  }
  
  private Looper getLooper(Looper paramLooper) {
    if (paramLooper != null)
      return paramLooper; 
    return (Looper)Preconditions.checkNotNull(Looper.myLooper(), "Can't toast on a thread that has not called Looper.prepare()");
  }
  
  public void show() {
    if (Compatibility.isChangeEnabled(147798919L)) {
      boolean bool;
      if (this.mNextView != null || this.mText != null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "You must either set a text or a view");
    } else if (this.mNextView == null) {
      throw new RuntimeException("setView must have been called");
    } 
    INotificationManager iNotificationManager = getService();
    String str = this.mContext.getOpPackageName();
    TN tN = this.mTN;
    tN.mNextView = this.mNextView;
    int i = this.mContext.getDisplayId();
    try {
      CallbackBinder callbackBinder;
      if (Compatibility.isChangeEnabled(147798919L)) {
        if (this.mNextView != null) {
          iNotificationManager.enqueueToast(str, (IBinder)this.mToken, (ITransientNotification)tN, this.mDuration, i);
        } else {
          callbackBinder = new CallbackBinder();
          this(this.mCallbacks, this.mHandler);
          iNotificationManager.enqueueTextToast(str, (IBinder)this.mToken, this.mText, this.mDuration, i, (ITransientNotificationCallback)callbackBinder);
        } 
      } else {
        iNotificationManager.enqueueToast(str, (IBinder)this.mToken, (ITransientNotification)callbackBinder, this.mDuration, i);
      } 
    } catch (RemoteException remoteException) {}
  }
  
  public void cancel() {
    if (Compatibility.isChangeEnabled(147798919L) && this.mNextView == null) {
      try {
        getService().cancelToast(this.mContext.getOpPackageName(), (IBinder)this.mToken);
      } catch (RemoteException remoteException) {}
    } else {
      this.mTN.cancel();
    } 
  }
  
  @Deprecated
  public void setView(View paramView) {
    this.mNextView = paramView;
  }
  
  @Deprecated
  public View getView() {
    return this.mNextView;
  }
  
  public void setDuration(int paramInt) {
    this.mDuration = paramInt;
    this.mTN.mDuration = paramInt;
  }
  
  public int getDuration() {
    return this.mDuration;
  }
  
  public void setMargin(float paramFloat1, float paramFloat2) {
    if (isSystemRenderedTextToast())
      Log.e("Toast", "setMargin() shouldn't be called on text toasts, the values won't be used"); 
    this.mTN.mHorizontalMargin = paramFloat1;
    this.mTN.mVerticalMargin = paramFloat2;
  }
  
  public float getHorizontalMargin() {
    if (isSystemRenderedTextToast())
      Log.e("Toast", "getHorizontalMargin() shouldn't be called on text toasts, the result may not reflect actual values."); 
    return this.mTN.mHorizontalMargin;
  }
  
  public float getVerticalMargin() {
    if (isSystemRenderedTextToast())
      Log.e("Toast", "getVerticalMargin() shouldn't be called on text toasts, the result may not reflect actual values."); 
    return this.mTN.mVerticalMargin;
  }
  
  public void setGravity(int paramInt1, int paramInt2, int paramInt3) {
    if (isSystemRenderedTextToast())
      Log.e("Toast", "setGravity() shouldn't be called on text toasts, the values won't be used"); 
    this.mTN.mGravity = paramInt1;
    this.mTN.mX = paramInt2;
    this.mTN.mY = paramInt3;
  }
  
  public int getGravity() {
    if (isSystemRenderedTextToast())
      Log.e("Toast", "getGravity() shouldn't be called on text toasts, the result may not reflect actual values."); 
    return this.mTN.mGravity;
  }
  
  public int getXOffset() {
    if (isSystemRenderedTextToast())
      Log.e("Toast", "getXOffset() shouldn't be called on text toasts, the result may not reflect actual values."); 
    return this.mTN.mX;
  }
  
  public int getYOffset() {
    if (isSystemRenderedTextToast())
      Log.e("Toast", "getYOffset() shouldn't be called on text toasts, the result may not reflect actual values."); 
    return this.mTN.mY;
  }
  
  private boolean isSystemRenderedTextToast() {
    boolean bool;
    if (Compatibility.isChangeEnabled(147798919L) && this.mNextView == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void addCallback(Callback paramCallback) {
    Preconditions.checkNotNull(paramCallback);
    synchronized (this.mCallbacks) {
      this.mCallbacks.add(paramCallback);
      return;
    } 
  }
  
  public void removeCallback(Callback paramCallback) {
    synchronized (this.mCallbacks) {
      this.mCallbacks.remove(paramCallback);
      return;
    } 
  }
  
  public WindowManager.LayoutParams getWindowParams() {
    if (Compatibility.isChangeEnabled(147798919L)) {
      if (this.mNextView != null)
        return this.mTN.mParams; 
      return null;
    } 
    return this.mTN.mParams;
  }
  
  public static Toast makeText(Context paramContext, CharSequence paramCharSequence, int paramInt) {
    return makeText(paramContext, null, paramCharSequence, paramInt);
  }
  
  public static Toast makeText(Context paramContext, Looper paramLooper, CharSequence paramCharSequence, int paramInt) {
    Toast toast1;
    if (Compatibility.isChangeEnabled(147798919L)) {
      toast1 = new Toast(paramContext, paramLooper);
      toast1.mText = paramCharSequence;
      toast1.mDuration = paramInt;
      return toast1;
    } 
    Toast toast2 = new Toast((Context)toast1, paramLooper);
    View view = ToastPresenter.getTextToastView((Context)toast1, paramCharSequence);
    toast2.mNextView = view;
    toast2.mDuration = paramInt;
    return toast2;
  }
  
  public static Toast makeText(Context paramContext, int paramInt1, int paramInt2) throws Resources.NotFoundException {
    return makeText(paramContext, paramContext.getResources().getText(paramInt1), paramInt2);
  }
  
  public void setText(int paramInt) {
    setText(this.mContext.getText(paramInt));
  }
  
  public void setText(CharSequence paramCharSequence) {
    if (Compatibility.isChangeEnabled(147798919L)) {
      if (this.mNextView == null) {
        this.mText = paramCharSequence;
      } else {
        throw new IllegalStateException("Text provided for custom toast, remove previous setView() calls if you want a text toast instead.");
      } 
    } else {
      View view = this.mNextView;
      if (view != null) {
        view = view.<TextView>findViewById(16908299);
        if (view != null) {
          view.setText(paramCharSequence);
          return;
        } 
        throw new RuntimeException("This Toast was not created with Toast.makeText()");
      } 
      throw new RuntimeException("This Toast was not created with Toast.makeText()");
    } 
  }
  
  private static INotificationManager getService() {
    INotificationManager iNotificationManager2 = sService;
    if (iNotificationManager2 != null)
      return iNotificationManager2; 
    IBinder iBinder = ServiceManager.getService("notification");
    INotificationManager iNotificationManager1 = INotificationManager.Stub.asInterface(iBinder);
    return iNotificationManager1;
  }
  
  class TN extends ITransientNotification.Stub {
    private static final int CANCEL = 2;
    
    private static final int HIDE = 1;
    
    private static final int SHOW = 0;
    
    private final List<Toast.Callback> mCallbacks;
    
    int mDuration;
    
    int mGravity;
    
    final Handler mHandler;
    
    float mHorizontalMargin;
    
    View mNextView;
    
    final String mPackageName;
    
    private final WindowManager.LayoutParams mParams;
    
    private final ToastPresenter mPresenter;
    
    final Binder mToken;
    
    float mVerticalMargin;
    
    View mView;
    
    WindowManager mWM;
    
    int mX;
    
    int mY;
    
    TN(Toast this$0, String param1String, Binder param1Binder, List<Toast.Callback> param1List, Looper param1Looper) {
      IBinder iBinder = ServiceManager.getService("accessibility");
      IAccessibilityManager iAccessibilityManager = IAccessibilityManager.Stub.asInterface(iBinder);
      ToastPresenter toastPresenter = new ToastPresenter((Context)this$0, iAccessibilityManager, Toast.getService(), param1String);
      this.mParams = toastPresenter.getLayoutParams();
      this.mPackageName = param1String;
      this.mToken = param1Binder;
      this.mCallbacks = param1List;
      this.mHandler = (Handler)new Object(this, param1Looper, null);
    }
    
    private List<Toast.Callback> getCallbacks() {
      synchronized (this.mCallbacks) {
        ArrayList<Toast.Callback> arrayList = new ArrayList();
        this((Collection)this.mCallbacks);
        return arrayList;
      } 
    }
    
    public void show(IBinder param1IBinder) {
      this.mHandler.obtainMessage(0, param1IBinder).sendToTarget();
    }
    
    public void hide() {
      this.mHandler.obtainMessage(1).sendToTarget();
    }
    
    public void cancel() {
      this.mHandler.obtainMessage(2).sendToTarget();
    }
    
    public void handleShow(IBinder param1IBinder) {
      if (this.mHandler.hasMessages(2) || this.mHandler.hasMessages(1))
        return; 
      if (this.mView != this.mNextView) {
        handleHide();
        View view = this.mNextView;
        ToastPresenter toastPresenter = this.mPresenter;
        Binder binder = this.mToken;
        int i = this.mDuration, j = this.mGravity, k = this.mX, m = this.mY;
        float f1 = this.mHorizontalMargin, f2 = this.mVerticalMargin;
        Toast.CallbackBinder callbackBinder = new Toast.CallbackBinder(this.mHandler);
        toastPresenter.show(view, (IBinder)binder, param1IBinder, i, j, k, m, f1, f2, (ITransientNotificationCallback)callbackBinder);
      } 
    }
    
    public void handleHide() {
      View view = this.mView;
      if (view != null) {
        boolean bool;
        if (view == this.mPresenter.getView()) {
          bool = true;
        } else {
          bool = false;
        } 
        Preconditions.checkState(bool, "Trying to hide toast view different than the last one displayed");
        this.mPresenter.hide((ITransientNotificationCallback)new Toast.CallbackBinder(this.mHandler));
        this.mView = null;
      } 
    }
  }
  
  public static abstract class Callback {
    public void onToastShown() {}
    
    public void onToastHidden() {}
  }
  
  class CallbackBinder extends ITransientNotificationCallback.Stub {
    private final List<Toast.Callback> mCallbacks;
    
    private final Handler mHandler;
    
    private CallbackBinder(Toast this$0, Handler param1Handler) {
      this.mCallbacks = (List<Toast.Callback>)this$0;
      this.mHandler = param1Handler;
    }
    
    public void onToastShown() {
      this.mHandler.post(new _$$Lambda$Toast$CallbackBinder$_s9yPuiT4nCWyRQ8LFD5klzoGtY(this));
    }
    
    public void onToastHidden() {
      this.mHandler.post(new _$$Lambda$Toast$CallbackBinder$SR1aRrAMSFwOe15ZWVhbrCRpoJE(this));
    }
    
    private List<Toast.Callback> getCallbacks() {
      synchronized (this.mCallbacks) {
        ArrayList<Toast.Callback> arrayList = new ArrayList();
        this((Collection)this.mCallbacks);
        return arrayList;
      } 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Duration {}
}
