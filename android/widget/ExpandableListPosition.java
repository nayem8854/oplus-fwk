package android.widget;

import java.util.ArrayList;

class ExpandableListPosition {
  public static final int CHILD = 1;
  
  public static final int GROUP = 2;
  
  private static final int MAX_POOL_SIZE = 5;
  
  private static ArrayList<ExpandableListPosition> sPool = new ArrayList<>(5);
  
  public int childPos;
  
  int flatListPos;
  
  public int groupPos;
  
  public int type;
  
  private void resetState() {
    this.groupPos = 0;
    this.childPos = 0;
    this.flatListPos = 0;
    this.type = 0;
  }
  
  long getPackedPosition() {
    if (this.type == 1)
      return ExpandableListView.getPackedPositionForChild(this.groupPos, this.childPos); 
    return ExpandableListView.getPackedPositionForGroup(this.groupPos);
  }
  
  static ExpandableListPosition obtainGroupPosition(int paramInt) {
    return obtain(2, paramInt, 0, 0);
  }
  
  static ExpandableListPosition obtainChildPosition(int paramInt1, int paramInt2) {
    return obtain(1, paramInt1, paramInt2, 0);
  }
  
  static ExpandableListPosition obtainPosition(long paramLong) {
    if (paramLong == 4294967295L)
      return null; 
    ExpandableListPosition expandableListPosition = getRecycledOrCreate();
    expandableListPosition.groupPos = ExpandableListView.getPackedPositionGroup(paramLong);
    if (ExpandableListView.getPackedPositionType(paramLong) == 1) {
      expandableListPosition.type = 1;
      expandableListPosition.childPos = ExpandableListView.getPackedPositionChild(paramLong);
    } else {
      expandableListPosition.type = 2;
    } 
    return expandableListPosition;
  }
  
  static ExpandableListPosition obtain(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    ExpandableListPosition expandableListPosition = getRecycledOrCreate();
    expandableListPosition.type = paramInt1;
    expandableListPosition.groupPos = paramInt2;
    expandableListPosition.childPos = paramInt3;
    expandableListPosition.flatListPos = paramInt4;
    return expandableListPosition;
  }
  
  private static ExpandableListPosition getRecycledOrCreate() {
    synchronized (sPool) {
      if (sPool.size() > 0) {
        ExpandableListPosition expandableListPosition1 = sPool.remove(0);
        expandableListPosition1.resetState();
        return expandableListPosition1;
      } 
      ExpandableListPosition expandableListPosition = new ExpandableListPosition();
      this();
      return expandableListPosition;
    } 
  }
  
  public void recycle() {
    synchronized (sPool) {
      if (sPool.size() < 5)
        sPool.add(this); 
      return;
    } 
  }
}
