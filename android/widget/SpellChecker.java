package android.widget;

import android.text.Editable;
import android.text.TextUtils;
import android.text.method.WordIterator;
import android.text.style.SpellCheckSpan;
import android.text.style.SuggestionSpan;
import android.util.Log;
import android.util.LruCache;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextServicesManager;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import java.util.Locale;

public class SpellChecker implements SpellCheckerSession.SpellCheckerSessionListener {
  private static final String TAG = SpellChecker.class.getSimpleName();
  
  private SpellParser[] mSpellParsers = new SpellParser[0];
  
  private int mSpanSequenceCounter = 0;
  
  private final LruCache<Long, SuggestionSpan> mSuggestionSpanCache = new LruCache<>(10);
  
  public static final int AVERAGE_WORD_LENGTH = 7;
  
  private static final boolean DBG = false;
  
  public static final int MAX_NUMBER_OF_WORDS = 50;
  
  private static final int MIN_SENTENCE_LENGTH = 50;
  
  private static final int SPELL_PAUSE_DURATION = 400;
  
  private static final int SUGGESTION_SPAN_CACHE_SIZE = 10;
  
  private static final int USE_SPAN_RANGE = -1;
  
  public static final int WORD_ITERATOR_INTERVAL = 350;
  
  final int mCookie;
  
  private Locale mCurrentLocale;
  
  private int[] mIds;
  
  private boolean mIsSentenceSpellCheckSupported;
  
  private int mLength;
  
  private SpellCheckSpan[] mSpellCheckSpans;
  
  SpellCheckerSession mSpellCheckerSession;
  
  private Runnable mSpellRunnable;
  
  private TextServicesManager mTextServicesManager;
  
  private final TextView mTextView;
  
  private WordIterator mWordIterator;
  
  public SpellChecker(TextView paramTextView) {
    this.mTextView = paramTextView;
    int[] arrayOfInt = ArrayUtils.newUnpaddedIntArray(1);
    this.mSpellCheckSpans = new SpellCheckSpan[arrayOfInt.length];
    setLocale(this.mTextView.getSpellCheckerLocale());
    this.mCookie = hashCode();
  }
  
  void resetSession() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual closeSession : ()V
    //   4: aload_0
    //   5: getfield mTextView : Landroid/widget/TextView;
    //   8: invokevirtual getTextServicesManagerForUser : ()Landroid/view/textservice/TextServicesManager;
    //   11: astore_1
    //   12: aload_0
    //   13: aload_1
    //   14: putfield mTextServicesManager : Landroid/view/textservice/TextServicesManager;
    //   17: aload_0
    //   18: getfield mCurrentLocale : Ljava/util/Locale;
    //   21: ifnull -> 94
    //   24: aload_1
    //   25: ifnull -> 94
    //   28: aload_0
    //   29: getfield mTextView : Landroid/widget/TextView;
    //   32: astore_1
    //   33: aload_1
    //   34: invokevirtual length : ()I
    //   37: ifeq -> 94
    //   40: aload_0
    //   41: getfield mTextServicesManager : Landroid/view/textservice/TextServicesManager;
    //   44: astore_1
    //   45: aload_1
    //   46: invokevirtual isSpellCheckerEnabled : ()Z
    //   49: ifeq -> 94
    //   52: aload_0
    //   53: getfield mTextServicesManager : Landroid/view/textservice/TextServicesManager;
    //   56: astore_1
    //   57: aload_1
    //   58: iconst_1
    //   59: invokevirtual getCurrentSpellCheckerSubtype : (Z)Landroid/view/textservice/SpellCheckerSubtype;
    //   62: ifnonnull -> 68
    //   65: goto -> 94
    //   68: aload_0
    //   69: aload_0
    //   70: getfield mTextServicesManager : Landroid/view/textservice/TextServicesManager;
    //   73: aconst_null
    //   74: aload_0
    //   75: getfield mCurrentLocale : Ljava/util/Locale;
    //   78: aload_0
    //   79: iconst_0
    //   80: invokevirtual newSpellCheckerSession : (Landroid/os/Bundle;Ljava/util/Locale;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;Z)Landroid/view/textservice/SpellCheckerSession;
    //   83: putfield mSpellCheckerSession : Landroid/view/textservice/SpellCheckerSession;
    //   86: aload_0
    //   87: iconst_1
    //   88: putfield mIsSentenceSpellCheckSupported : Z
    //   91: goto -> 99
    //   94: aload_0
    //   95: aconst_null
    //   96: putfield mSpellCheckerSession : Landroid/view/textservice/SpellCheckerSession;
    //   99: iconst_0
    //   100: istore_2
    //   101: iload_2
    //   102: aload_0
    //   103: getfield mLength : I
    //   106: if_icmpge -> 122
    //   109: aload_0
    //   110: getfield mIds : [I
    //   113: iload_2
    //   114: iconst_m1
    //   115: iastore
    //   116: iinc #2, 1
    //   119: goto -> 101
    //   122: aload_0
    //   123: iconst_0
    //   124: putfield mLength : I
    //   127: aload_0
    //   128: getfield mTextView : Landroid/widget/TextView;
    //   131: astore_1
    //   132: aload_1
    //   133: aload_1
    //   134: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   137: checkcast android/text/Editable
    //   140: invokevirtual removeMisspelledSpans : (Landroid/text/Spannable;)V
    //   143: aload_0
    //   144: getfield mSuggestionSpanCache : Landroid/util/LruCache;
    //   147: invokevirtual evictAll : ()V
    //   150: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #119	-> 0
    //   #121	-> 4
    //   #122	-> 17
    //   #124	-> 33
    //   #125	-> 45
    //   #126	-> 57
    //   #129	-> 68
    //   #133	-> 86
    //   #127	-> 94
    //   #137	-> 99
    //   #138	-> 109
    //   #137	-> 116
    //   #140	-> 122
    //   #143	-> 127
    //   #144	-> 143
    //   #145	-> 150
  }
  
  private void setLocale(Locale paramLocale) {
    this.mCurrentLocale = paramLocale;
    resetSession();
    if (paramLocale != null)
      this.mWordIterator = new WordIterator(paramLocale); 
    this.mTextView.onLocaleChanged();
  }
  
  private boolean isSessionActive() {
    boolean bool;
    if (this.mSpellCheckerSession != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void closeSession() {
    SpellCheckerSession spellCheckerSession = this.mSpellCheckerSession;
    if (spellCheckerSession != null)
      spellCheckerSession.close(); 
    int i = this.mSpellParsers.length;
    for (byte b = 0; b < i; b++)
      this.mSpellParsers[b].stop(); 
    Runnable runnable = this.mSpellRunnable;
    if (runnable != null)
      this.mTextView.removeCallbacks(runnable); 
  }
  
  private int nextSpellCheckSpanIndex() {
    int j, i = 0;
    while (true) {
      j = this.mLength;
      if (i < j) {
        if (this.mIds[i] < 0)
          return i; 
        i++;
        continue;
      } 
      break;
    } 
    this.mIds = GrowingArrayUtils.append(this.mIds, j, 0);
    this.mSpellCheckSpans = (SpellCheckSpan[])GrowingArrayUtils.append((Object[])this.mSpellCheckSpans, this.mLength, new SpellCheckSpan());
    this.mLength = i = this.mLength + 1;
    return i - 1;
  }
  
  private void addSpellCheckSpan(Editable paramEditable, int paramInt1, int paramInt2) {
    int i = nextSpellCheckSpanIndex();
    SpellCheckSpan spellCheckSpan = this.mSpellCheckSpans[i];
    paramEditable.setSpan(spellCheckSpan, paramInt1, paramInt2, 33);
    spellCheckSpan.setSpellCheckInProgress(false);
    int[] arrayOfInt = this.mIds;
    paramInt1 = this.mSpanSequenceCounter;
    this.mSpanSequenceCounter = paramInt1 + 1;
    arrayOfInt[i] = paramInt1;
  }
  
  public void onSpellCheckSpanRemoved(SpellCheckSpan paramSpellCheckSpan) {
    for (byte b = 0; b < this.mLength; b++) {
      if (this.mSpellCheckSpans[b] == paramSpellCheckSpan) {
        this.mIds[b] = -1;
        return;
      } 
    } 
  }
  
  public void onSelectionChanged() {
    spellCheck();
  }
  
  public void spellCheck(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTextView : Landroid/widget/TextView;
    //   4: invokevirtual getSpellCheckerLocale : ()Ljava/util/Locale;
    //   7: astore_3
    //   8: aload_0
    //   9: invokespecial isSessionActive : ()Z
    //   12: istore #4
    //   14: aload_3
    //   15: ifnull -> 92
    //   18: aload_0
    //   19: getfield mCurrentLocale : Ljava/util/Locale;
    //   22: astore #5
    //   24: aload #5
    //   26: ifnull -> 92
    //   29: aload #5
    //   31: aload_3
    //   32: invokevirtual equals : (Ljava/lang/Object;)Z
    //   35: ifne -> 41
    //   38: goto -> 92
    //   41: aload_0
    //   42: getfield mTextServicesManager : Landroid/view/textservice/TextServicesManager;
    //   45: astore_3
    //   46: aload_3
    //   47: ifnull -> 63
    //   50: aload_3
    //   51: invokevirtual isSpellCheckerEnabled : ()Z
    //   54: ifeq -> 63
    //   57: iconst_1
    //   58: istore #6
    //   60: goto -> 66
    //   63: iconst_0
    //   64: istore #6
    //   66: iload_1
    //   67: istore #7
    //   69: iload_2
    //   70: istore #8
    //   72: iload #4
    //   74: iload #6
    //   76: if_icmpeq -> 114
    //   79: aload_0
    //   80: invokevirtual resetSession : ()V
    //   83: iload_1
    //   84: istore #7
    //   86: iload_2
    //   87: istore #8
    //   89: goto -> 114
    //   92: aload_0
    //   93: aload_3
    //   94: invokespecial setLocale : (Ljava/util/Locale;)V
    //   97: iconst_0
    //   98: istore #7
    //   100: aload_0
    //   101: getfield mTextView : Landroid/widget/TextView;
    //   104: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   107: invokeinterface length : ()I
    //   112: istore #8
    //   114: iload #4
    //   116: ifne -> 120
    //   119: return
    //   120: aload_0
    //   121: getfield mSpellParsers : [Landroid/widget/SpellChecker$SpellParser;
    //   124: arraylength
    //   125: istore_2
    //   126: iconst_0
    //   127: istore_1
    //   128: iload_1
    //   129: iload_2
    //   130: if_icmpge -> 162
    //   133: aload_0
    //   134: getfield mSpellParsers : [Landroid/widget/SpellChecker$SpellParser;
    //   137: iload_1
    //   138: aaload
    //   139: astore_3
    //   140: aload_3
    //   141: invokevirtual isFinished : ()Z
    //   144: ifeq -> 156
    //   147: aload_3
    //   148: iload #7
    //   150: iload #8
    //   152: invokevirtual parse : (II)V
    //   155: return
    //   156: iinc #1, 1
    //   159: goto -> 128
    //   162: iload_2
    //   163: iconst_1
    //   164: iadd
    //   165: anewarray android/widget/SpellChecker$SpellParser
    //   168: astore_3
    //   169: aload_0
    //   170: getfield mSpellParsers : [Landroid/widget/SpellChecker$SpellParser;
    //   173: iconst_0
    //   174: aload_3
    //   175: iconst_0
    //   176: iload_2
    //   177: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   180: aload_0
    //   181: aload_3
    //   182: putfield mSpellParsers : [Landroid/widget/SpellChecker$SpellParser;
    //   185: new android/widget/SpellChecker$SpellParser
    //   188: dup
    //   189: aload_0
    //   190: aconst_null
    //   191: invokespecial <init> : (Landroid/widget/SpellChecker;Landroid/widget/SpellChecker$1;)V
    //   194: astore_3
    //   195: aload_0
    //   196: getfield mSpellParsers : [Landroid/widget/SpellChecker$SpellParser;
    //   199: iload_2
    //   200: aload_3
    //   201: aastore
    //   202: aload_3
    //   203: iload #7
    //   205: iload #8
    //   207: invokevirtual parse : (II)V
    //   210: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #222	-> 0
    //   #223	-> 8
    //   #224	-> 14
    //   #230	-> 41
    //   #231	-> 50
    //   #232	-> 66
    //   #234	-> 79
    //   #225	-> 92
    //   #227	-> 97
    //   #228	-> 100
    //   #238	-> 114
    //   #241	-> 120
    //   #242	-> 126
    //   #243	-> 133
    //   #244	-> 140
    //   #245	-> 147
    //   #246	-> 155
    //   #242	-> 156
    //   #254	-> 162
    //   #255	-> 169
    //   #256	-> 180
    //   #258	-> 185
    //   #259	-> 195
    //   #260	-> 202
    //   #261	-> 210
  }
  
  private void spellCheck() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSpellCheckerSession : Landroid/view/textservice/SpellCheckerSession;
    //   4: ifnonnull -> 8
    //   7: return
    //   8: aload_0
    //   9: getfield mTextView : Landroid/widget/TextView;
    //   12: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   15: checkcast android/text/Editable
    //   18: astore_1
    //   19: aload_1
    //   20: invokestatic getSelectionStart : (Ljava/lang/CharSequence;)I
    //   23: istore_2
    //   24: aload_1
    //   25: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   28: istore_3
    //   29: aload_0
    //   30: getfield mLength : I
    //   33: anewarray android/view/textservice/TextInfo
    //   36: astore #4
    //   38: iconst_0
    //   39: istore #5
    //   41: iconst_0
    //   42: istore #6
    //   44: aload_0
    //   45: getfield mLength : I
    //   48: istore #7
    //   50: iconst_0
    //   51: istore #8
    //   53: iconst_0
    //   54: istore #9
    //   56: iload #6
    //   58: iload #7
    //   60: if_icmpge -> 288
    //   63: aload_0
    //   64: getfield mSpellCheckSpans : [Landroid/text/style/SpellCheckSpan;
    //   67: iload #6
    //   69: aaload
    //   70: astore #10
    //   72: iload #5
    //   74: istore #7
    //   76: aload_0
    //   77: getfield mIds : [I
    //   80: iload #6
    //   82: iaload
    //   83: iflt -> 278
    //   86: aload #10
    //   88: invokevirtual isSpellCheckInProgress : ()Z
    //   91: ifeq -> 101
    //   94: iload #5
    //   96: istore #7
    //   98: goto -> 278
    //   101: aload_1
    //   102: aload #10
    //   104: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   109: istore #11
    //   111: aload_1
    //   112: aload #10
    //   114: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   119: istore #12
    //   121: iload_2
    //   122: iload #12
    //   124: iconst_1
    //   125: iadd
    //   126: if_icmpne -> 161
    //   129: aload_0
    //   130: getfield mCurrentLocale : Ljava/util/Locale;
    //   133: astore #13
    //   135: aload_1
    //   136: iload #12
    //   138: iconst_1
    //   139: iadd
    //   140: invokestatic codePointBefore : (Ljava/lang/CharSequence;I)I
    //   143: istore #7
    //   145: aload #13
    //   147: iload #7
    //   149: invokestatic isMidWordPunctuation : (Ljava/util/Locale;I)Z
    //   152: ifeq -> 161
    //   155: iconst_0
    //   156: istore #9
    //   158: goto -> 205
    //   161: aload_0
    //   162: getfield mIsSentenceSpellCheckSupported : Z
    //   165: ifeq -> 186
    //   168: iload_3
    //   169: iload #11
    //   171: if_icmple -> 180
    //   174: iload_2
    //   175: iload #12
    //   177: if_icmple -> 183
    //   180: iconst_1
    //   181: istore #9
    //   183: goto -> 205
    //   186: iload_3
    //   187: iload #11
    //   189: if_icmplt -> 202
    //   192: iload #8
    //   194: istore #9
    //   196: iload_2
    //   197: iload #12
    //   199: if_icmple -> 205
    //   202: iconst_1
    //   203: istore #9
    //   205: iload #5
    //   207: istore #7
    //   209: iload #11
    //   211: iflt -> 278
    //   214: iload #5
    //   216: istore #7
    //   218: iload #12
    //   220: iload #11
    //   222: if_icmple -> 278
    //   225: iload #5
    //   227: istore #7
    //   229: iload #9
    //   231: ifeq -> 278
    //   234: aload #10
    //   236: iconst_1
    //   237: invokevirtual setSpellCheckInProgress : (Z)V
    //   240: new android/view/textservice/TextInfo
    //   243: dup
    //   244: aload_1
    //   245: iload #11
    //   247: iload #12
    //   249: aload_0
    //   250: getfield mCookie : I
    //   253: aload_0
    //   254: getfield mIds : [I
    //   257: iload #6
    //   259: iaload
    //   260: invokespecial <init> : (Ljava/lang/CharSequence;IIII)V
    //   263: astore #10
    //   265: aload #4
    //   267: iload #5
    //   269: aload #10
    //   271: aastore
    //   272: iload #5
    //   274: iconst_1
    //   275: iadd
    //   276: istore #7
    //   278: iinc #6, 1
    //   281: iload #7
    //   283: istore #5
    //   285: goto -> 44
    //   288: iload #5
    //   290: ifle -> 349
    //   293: aload #4
    //   295: astore_1
    //   296: iload #5
    //   298: aload #4
    //   300: arraylength
    //   301: if_icmpge -> 320
    //   304: iload #5
    //   306: anewarray android/view/textservice/TextInfo
    //   309: astore_1
    //   310: aload #4
    //   312: iconst_0
    //   313: aload_1
    //   314: iconst_0
    //   315: iload #5
    //   317: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   320: aload_0
    //   321: getfield mIsSentenceSpellCheckSupported : Z
    //   324: ifeq -> 339
    //   327: aload_0
    //   328: getfield mSpellCheckerSession : Landroid/view/textservice/SpellCheckerSession;
    //   331: aload_1
    //   332: iconst_5
    //   333: invokevirtual getSentenceSuggestions : ([Landroid/view/textservice/TextInfo;I)V
    //   336: goto -> 349
    //   339: aload_0
    //   340: getfield mSpellCheckerSession : Landroid/view/textservice/SpellCheckerSession;
    //   343: aload_1
    //   344: iconst_5
    //   345: iconst_0
    //   346: invokevirtual getSuggestions : ([Landroid/view/textservice/TextInfo;IZ)V
    //   349: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #264	-> 0
    //   #266	-> 8
    //   #267	-> 19
    //   #268	-> 24
    //   #270	-> 29
    //   #271	-> 38
    //   #273	-> 38
    //   #274	-> 63
    //   #275	-> 72
    //   #277	-> 101
    //   #278	-> 111
    //   #285	-> 121
    //   #287	-> 135
    //   #286	-> 145
    //   #288	-> 155
    //   #289	-> 161
    //   #293	-> 168
    //   #295	-> 186
    //   #297	-> 205
    //   #298	-> 234
    //   #299	-> 240
    //   #300	-> 265
    //   #273	-> 278
    //   #310	-> 288
    //   #311	-> 293
    //   #312	-> 304
    //   #313	-> 310
    //   #314	-> 320
    //   #317	-> 320
    //   #318	-> 327
    //   #321	-> 339
    //   #325	-> 349
  }
  
  private SpellCheckSpan onGetSuggestionsInternal(SuggestionsInfo paramSuggestionsInfo, int paramInt1, int paramInt2) {
    if (paramSuggestionsInfo == null || paramSuggestionsInfo.getCookie() != this.mCookie)
      return null; 
    Editable editable = (Editable)this.mTextView.getText();
    int i = paramSuggestionsInfo.getSequence();
    for (int j = 0; j < this.mLength; j++) {
      if (i == this.mIds[j]) {
        int k = paramSuggestionsInfo.getSuggestionsAttributes();
        int m = 0;
        if ((k & 0x1) > 0) {
          i = 1;
        } else {
          i = 0;
        } 
        if ((k & 0x2) > 0)
          m = 1; 
        SpellCheckSpan spellCheckSpan = this.mSpellCheckSpans[j];
        if (i == 0 && m) {
          createMisspelledSuggestionSpan(editable, paramSuggestionsInfo, spellCheckSpan, paramInt1, paramInt2);
        } else if (this.mIsSentenceSpellCheckSupported) {
          i = editable.getSpanStart(spellCheckSpan);
          j = editable.getSpanEnd(spellCheckSpan);
          if (paramInt1 != -1 && paramInt2 != -1) {
            paramInt1 = i + paramInt1;
            m = paramInt1 + paramInt2;
            paramInt2 = paramInt1;
            paramInt1 = m;
          } else {
            paramInt2 = i;
            paramInt1 = j;
          } 
          if (i >= 0 && j > i && paramInt1 > paramInt2) {
            Long long_ = Long.valueOf(TextUtils.packRangeInLong(paramInt2, paramInt1));
            SuggestionSpan suggestionSpan = this.mSuggestionSpanCache.get(long_);
            if (suggestionSpan != null) {
              editable.removeSpan(suggestionSpan);
              this.mSuggestionSpanCache.remove(long_);
            } 
          } 
        } 
        return spellCheckSpan;
      } 
    } 
    return null;
  }
  
  public void onGetSuggestions(SuggestionsInfo[] paramArrayOfSuggestionsInfo) {
    Editable editable = (Editable)this.mTextView.getText();
    for (byte b = 0; b < paramArrayOfSuggestionsInfo.length; b++) {
      SuggestionsInfo suggestionsInfo = paramArrayOfSuggestionsInfo[b];
      SpellCheckSpan spellCheckSpan = onGetSuggestionsInternal(suggestionsInfo, -1, -1);
      if (spellCheckSpan != null)
        editable.removeSpan(spellCheckSpan); 
    } 
    scheduleNewSpellCheck();
  }
  
  public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] paramArrayOfSentenceSuggestionsInfo) {
    Editable editable = (Editable)this.mTextView.getText();
    for (byte b = 0; b < paramArrayOfSentenceSuggestionsInfo.length; b++) {
      SentenceSuggestionsInfo sentenceSuggestionsInfo = paramArrayOfSentenceSuggestionsInfo[b];
      if (sentenceSuggestionsInfo != null) {
        SuggestionsInfo suggestionsInfo = null;
        SpellCheckSpan spellCheckSpan;
        for (byte b1 = 0; b1 < sentenceSuggestionsInfo.getSuggestionsCount(); b1++, spellCheckSpan = spellCheckSpan1) {
          SpellCheckSpan spellCheckSpan1;
          SuggestionsInfo suggestionsInfo1 = sentenceSuggestionsInfo.getSuggestionsInfoAt(b1);
          if (suggestionsInfo1 == null) {
            suggestionsInfo1 = suggestionsInfo;
          } else {
            int i = sentenceSuggestionsInfo.getOffsetAt(b1);
            int j = sentenceSuggestionsInfo.getLengthAt(b1);
            SpellCheckSpan spellCheckSpan2 = onGetSuggestionsInternal(suggestionsInfo1, i, j);
            suggestionsInfo1 = suggestionsInfo;
            if (suggestionsInfo == null) {
              suggestionsInfo1 = suggestionsInfo;
              if (spellCheckSpan2 != null)
                spellCheckSpan1 = spellCheckSpan2; 
            } 
          } 
        } 
        if (spellCheckSpan != null)
          editable.removeSpan(spellCheckSpan); 
      } 
    } 
    scheduleNewSpellCheck();
  }
  
  private void scheduleNewSpellCheck() {
    Runnable runnable = this.mSpellRunnable;
    if (runnable == null) {
      this.mSpellRunnable = (Runnable)new Object(this);
    } else {
      this.mTextView.removeCallbacks(runnable);
    } 
    this.mTextView.postDelayed(this.mSpellRunnable, 400L);
  }
  
  private void createMisspelledSuggestionSpan(Editable paramEditable, SuggestionsInfo paramSuggestionsInfo, SpellCheckSpan paramSpellCheckSpan, int paramInt1, int paramInt2) {
    String[] arrayOfString;
    int i = paramEditable.getSpanStart(paramSpellCheckSpan);
    int j = paramEditable.getSpanEnd(paramSpellCheckSpan);
    if (i < 0 || j <= i)
      return; 
    if (paramInt1 != -1 && paramInt2 != -1) {
      paramInt1 = i + paramInt1;
      j = paramInt1 + paramInt2;
      paramInt2 = paramInt1;
      paramInt1 = j;
    } else {
      paramInt2 = i;
      paramInt1 = j;
    } 
    i = paramSuggestionsInfo.getSuggestionsCount();
    if (i > 0) {
      String[] arrayOfString1 = new String[i];
      for (j = 0; j < i; j++)
        arrayOfString1[j] = paramSuggestionsInfo.getSuggestionAt(j); 
      arrayOfString = arrayOfString1;
    } else {
      arrayOfString = (String[])ArrayUtils.emptyArray(String.class);
    } 
    SuggestionSpan suggestionSpan = new SuggestionSpan(this.mTextView.getContext(), arrayOfString, 3);
    if (this.mIsSentenceSpellCheckSupported) {
      Long long_ = Long.valueOf(TextUtils.packRangeInLong(paramInt2, paramInt1));
      SuggestionSpan suggestionSpan1 = this.mSuggestionSpanCache.get(long_);
      if (suggestionSpan1 != null)
        paramEditable.removeSpan(suggestionSpan1); 
      this.mSuggestionSpanCache.put(long_, suggestionSpan);
    } 
    paramEditable.setSpan(suggestionSpan, paramInt2, paramInt1, 33);
    this.mTextView.invalidateRegion(paramInt2, paramInt1, false);
  }
  
  class SpellParser {
    private Object mRange = new Object();
    
    final SpellChecker this$0;
    
    public void parse(int param1Int1, int param1Int2) {
      int i = SpellChecker.this.mTextView.length();
      if (param1Int2 > i) {
        String str = SpellChecker.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Parse invalid region, from ");
        stringBuilder.append(param1Int1);
        stringBuilder.append(" to ");
        stringBuilder.append(param1Int2);
        Log.w(str, stringBuilder.toString());
        param1Int2 = i;
      } 
      if (param1Int2 > param1Int1) {
        setRangeSpan((Editable)SpellChecker.this.mTextView.getText(), param1Int1, param1Int2);
        parse();
      } 
    }
    
    public boolean isFinished() {
      boolean bool;
      if (((Editable)SpellChecker.this.mTextView.getText()).getSpanStart(this.mRange) < 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void stop() {
      removeRangeSpan((Editable)SpellChecker.this.mTextView.getText());
    }
    
    private void setRangeSpan(Editable param1Editable, int param1Int1, int param1Int2) {
      param1Editable.setSpan(this.mRange, param1Int1, param1Int2, 33);
    }
    
    private void removeRangeSpan(Editable param1Editable) {
      param1Editable.removeSpan(this.mRange);
    }
    
    public void parse() {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/widget/SpellChecker;
      //   4: invokestatic access$200 : (Landroid/widget/SpellChecker;)Landroid/widget/TextView;
      //   7: invokevirtual getText : ()Ljava/lang/CharSequence;
      //   10: checkcast android/text/Editable
      //   13: astore_1
      //   14: aload_0
      //   15: getfield this$0 : Landroid/widget/SpellChecker;
      //   18: invokestatic access$400 : (Landroid/widget/SpellChecker;)Z
      //   21: ifeq -> 50
      //   24: aload_0
      //   25: getfield mRange : Ljava/lang/Object;
      //   28: astore_2
      //   29: aload_1
      //   30: aload_2
      //   31: invokeinterface getSpanStart : (Ljava/lang/Object;)I
      //   36: istore_3
      //   37: iconst_0
      //   38: iload_3
      //   39: bipush #50
      //   41: isub
      //   42: invokestatic max : (II)I
      //   45: istore #4
      //   47: goto -> 62
      //   50: aload_1
      //   51: aload_0
      //   52: getfield mRange : Ljava/lang/Object;
      //   55: invokeinterface getSpanStart : (Ljava/lang/Object;)I
      //   60: istore #4
      //   62: aload_1
      //   63: aload_0
      //   64: getfield mRange : Ljava/lang/Object;
      //   67: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
      //   72: istore #5
      //   74: iload #5
      //   76: iload #4
      //   78: sipush #350
      //   81: iadd
      //   82: invokestatic min : (II)I
      //   85: istore #6
      //   87: aload_0
      //   88: getfield this$0 : Landroid/widget/SpellChecker;
      //   91: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   94: aload_1
      //   95: iload #4
      //   97: iload #6
      //   99: invokevirtual setCharSequence : (Ljava/lang/CharSequence;II)V
      //   102: aload_0
      //   103: getfield this$0 : Landroid/widget/SpellChecker;
      //   106: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   109: iload #4
      //   111: invokevirtual preceding : (I)I
      //   114: istore_3
      //   115: iload_3
      //   116: iconst_m1
      //   117: if_icmpne -> 164
      //   120: aload_0
      //   121: getfield this$0 : Landroid/widget/SpellChecker;
      //   124: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   127: iload #4
      //   129: invokevirtual following : (I)I
      //   132: istore #7
      //   134: iload #7
      //   136: istore #8
      //   138: iload #7
      //   140: iconst_m1
      //   141: if_icmpeq -> 177
      //   144: aload_0
      //   145: getfield this$0 : Landroid/widget/SpellChecker;
      //   148: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   151: iload #7
      //   153: invokevirtual getBeginning : (I)I
      //   156: istore_3
      //   157: iload #7
      //   159: istore #8
      //   161: goto -> 177
      //   164: aload_0
      //   165: getfield this$0 : Landroid/widget/SpellChecker;
      //   168: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   171: iload_3
      //   172: invokevirtual getEnd : (I)I
      //   175: istore #8
      //   177: iload #8
      //   179: iconst_m1
      //   180: if_icmpne -> 189
      //   183: aload_0
      //   184: aload_1
      //   185: invokespecial removeRangeSpan : (Landroid/text/Editable;)V
      //   188: return
      //   189: aload_1
      //   190: iload #4
      //   192: iconst_1
      //   193: isub
      //   194: iload #5
      //   196: iconst_1
      //   197: iadd
      //   198: ldc android/text/style/SpellCheckSpan
      //   200: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
      //   205: checkcast [Landroid/text/style/SpellCheckSpan;
      //   208: astore #9
      //   210: aload_1
      //   211: iload #4
      //   213: iconst_1
      //   214: isub
      //   215: iload #5
      //   217: iconst_1
      //   218: iadd
      //   219: ldc android/text/style/SuggestionSpan
      //   221: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
      //   226: checkcast [Landroid/text/style/SuggestionSpan;
      //   229: astore_2
      //   230: iconst_0
      //   231: istore #10
      //   233: iconst_0
      //   234: istore #11
      //   236: iconst_0
      //   237: istore #7
      //   239: aload_0
      //   240: getfield this$0 : Landroid/widget/SpellChecker;
      //   243: invokestatic access$400 : (Landroid/widget/SpellChecker;)Z
      //   246: ifeq -> 631
      //   249: iload #6
      //   251: iload #5
      //   253: if_icmpge -> 259
      //   256: iconst_1
      //   257: istore #7
      //   259: aload_0
      //   260: getfield this$0 : Landroid/widget/SpellChecker;
      //   263: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   266: iload #6
      //   268: invokevirtual preceding : (I)I
      //   271: istore #11
      //   273: iload #11
      //   275: iconst_m1
      //   276: if_icmpeq -> 285
      //   279: iconst_1
      //   280: istore #12
      //   282: goto -> 288
      //   285: iconst_0
      //   286: istore #12
      //   288: iload #11
      //   290: istore #10
      //   292: iload #12
      //   294: istore #13
      //   296: iload #12
      //   298: ifeq -> 338
      //   301: aload_0
      //   302: getfield this$0 : Landroid/widget/SpellChecker;
      //   305: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   308: iload #11
      //   310: invokevirtual getEnd : (I)I
      //   313: istore #12
      //   315: iload #12
      //   317: iconst_m1
      //   318: if_icmpeq -> 327
      //   321: iconst_1
      //   322: istore #10
      //   324: goto -> 330
      //   327: iconst_0
      //   328: istore #10
      //   330: iload #10
      //   332: istore #13
      //   334: iload #12
      //   336: istore #10
      //   338: iload #13
      //   340: ifne -> 349
      //   343: aload_0
      //   344: aload_1
      //   345: invokespecial removeRangeSpan : (Landroid/text/Editable;)V
      //   348: return
      //   349: iload_3
      //   350: istore #14
      //   352: iconst_1
      //   353: istore #15
      //   355: iconst_0
      //   356: istore #13
      //   358: iload_3
      //   359: istore #12
      //   361: iload #6
      //   363: istore #11
      //   365: iload #14
      //   367: istore #6
      //   369: iload #10
      //   371: istore_3
      //   372: iload #13
      //   374: aload_0
      //   375: getfield this$0 : Landroid/widget/SpellChecker;
      //   378: invokestatic access$600 : (Landroid/widget/SpellChecker;)I
      //   381: if_icmpge -> 539
      //   384: aload_0
      //   385: getfield this$0 : Landroid/widget/SpellChecker;
      //   388: invokestatic access$700 : (Landroid/widget/SpellChecker;)[Landroid/text/style/SpellCheckSpan;
      //   391: iload #13
      //   393: aaload
      //   394: astore_2
      //   395: aload_0
      //   396: getfield this$0 : Landroid/widget/SpellChecker;
      //   399: invokestatic access$800 : (Landroid/widget/SpellChecker;)[I
      //   402: iload #13
      //   404: iaload
      //   405: iflt -> 519
      //   408: aload_2
      //   409: invokevirtual isSpellCheckInProgress : ()Z
      //   412: ifeq -> 425
      //   415: iload_3
      //   416: istore #10
      //   418: iload #6
      //   420: istore #14
      //   422: goto -> 526
      //   425: aload_1
      //   426: aload_2
      //   427: invokeinterface getSpanStart : (Ljava/lang/Object;)I
      //   432: istore #16
      //   434: aload_1
      //   435: aload_2
      //   436: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
      //   441: istore #17
      //   443: iload_3
      //   444: istore #10
      //   446: iload #6
      //   448: istore #14
      //   450: iload #17
      //   452: iload #6
      //   454: if_icmplt -> 526
      //   457: iload_3
      //   458: iload #16
      //   460: if_icmpge -> 473
      //   463: iload_3
      //   464: istore #10
      //   466: iload #6
      //   468: istore #14
      //   470: goto -> 526
      //   473: iload #16
      //   475: iload #6
      //   477: if_icmpgt -> 492
      //   480: iload_3
      //   481: iload #17
      //   483: if_icmpgt -> 492
      //   486: iconst_0
      //   487: istore #8
      //   489: goto -> 543
      //   492: aload_1
      //   493: aload_2
      //   494: invokeinterface removeSpan : (Ljava/lang/Object;)V
      //   499: iload #16
      //   501: iload #6
      //   503: invokestatic min : (II)I
      //   506: istore #14
      //   508: iload #17
      //   510: iload_3
      //   511: invokestatic max : (II)I
      //   514: istore #10
      //   516: goto -> 526
      //   519: iload #6
      //   521: istore #14
      //   523: iload_3
      //   524: istore #10
      //   526: iinc #13, 1
      //   529: iload #10
      //   531: istore_3
      //   532: iload #14
      //   534: istore #6
      //   536: goto -> 372
      //   539: iload #15
      //   541: istore #8
      //   543: iload_3
      //   544: iload #4
      //   546: if_icmpge -> 552
      //   549: goto -> 628
      //   552: iload_3
      //   553: iload #6
      //   555: if_icmpgt -> 612
      //   558: invokestatic access$300 : ()Ljava/lang/String;
      //   561: astore #9
      //   563: new java/lang/StringBuilder
      //   566: dup
      //   567: invokespecial <init> : ()V
      //   570: astore_2
      //   571: aload_2
      //   572: ldc 'Trying to spellcheck invalid region, from '
      //   574: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   577: pop
      //   578: aload_2
      //   579: iload #4
      //   581: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   584: pop
      //   585: aload_2
      //   586: ldc ' to '
      //   588: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   591: pop
      //   592: aload_2
      //   593: iload #5
      //   595: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   598: pop
      //   599: aload #9
      //   601: aload_2
      //   602: invokevirtual toString : ()Ljava/lang/String;
      //   605: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   608: pop
      //   609: goto -> 628
      //   612: iload #8
      //   614: ifeq -> 628
      //   617: aload_0
      //   618: getfield this$0 : Landroid/widget/SpellChecker;
      //   621: aload_1
      //   622: iload #6
      //   624: iload_3
      //   625: invokestatic access$900 : (Landroid/widget/SpellChecker;Landroid/text/Editable;II)V
      //   628: goto -> 1007
      //   631: iload #8
      //   633: istore #7
      //   635: iload_3
      //   636: iload #5
      //   638: if_icmpgt -> 1003
      //   641: iload #10
      //   643: istore #12
      //   645: iload #7
      //   647: iload #4
      //   649: if_icmplt -> 875
      //   652: iload #10
      //   654: istore #12
      //   656: iload #7
      //   658: iload_3
      //   659: if_icmple -> 875
      //   662: iload #10
      //   664: bipush #50
      //   666: if_icmplt -> 675
      //   669: iconst_1
      //   670: istore #7
      //   672: goto -> 1007
      //   675: iload_3
      //   676: iload #4
      //   678: if_icmpge -> 705
      //   681: iload #7
      //   683: iload #4
      //   685: if_icmple -> 705
      //   688: aload_0
      //   689: aload_1
      //   690: iload #4
      //   692: aload #9
      //   694: invokespecial removeSpansAt : (Landroid/text/Editable;I[Ljava/lang/Object;)V
      //   697: aload_0
      //   698: aload_1
      //   699: iload #4
      //   701: aload_2
      //   702: invokespecial removeSpansAt : (Landroid/text/Editable;I[Ljava/lang/Object;)V
      //   705: iload_3
      //   706: iload #5
      //   708: if_icmpge -> 735
      //   711: iload #7
      //   713: iload #5
      //   715: if_icmple -> 735
      //   718: aload_0
      //   719: aload_1
      //   720: iload #5
      //   722: aload #9
      //   724: invokespecial removeSpansAt : (Landroid/text/Editable;I[Ljava/lang/Object;)V
      //   727: aload_0
      //   728: aload_1
      //   729: iload #5
      //   731: aload_2
      //   732: invokespecial removeSpansAt : (Landroid/text/Editable;I[Ljava/lang/Object;)V
      //   735: iconst_1
      //   736: istore #13
      //   738: iload #13
      //   740: istore #8
      //   742: iload #7
      //   744: iload #4
      //   746: if_icmpne -> 796
      //   749: iconst_0
      //   750: istore #12
      //   752: iload #13
      //   754: istore #8
      //   756: iload #12
      //   758: aload #9
      //   760: arraylength
      //   761: if_icmpge -> 796
      //   764: aload_1
      //   765: aload #9
      //   767: iload #12
      //   769: aaload
      //   770: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
      //   775: istore #8
      //   777: iload #8
      //   779: iload #4
      //   781: if_icmpne -> 790
      //   784: iconst_0
      //   785: istore #8
      //   787: goto -> 796
      //   790: iinc #12, 1
      //   793: goto -> 752
      //   796: iload #8
      //   798: istore #13
      //   800: iload_3
      //   801: iload #5
      //   803: if_icmpne -> 853
      //   806: iconst_0
      //   807: istore #12
      //   809: iload #8
      //   811: istore #13
      //   813: iload #12
      //   815: aload #9
      //   817: arraylength
      //   818: if_icmpge -> 853
      //   821: aload_1
      //   822: aload #9
      //   824: iload #12
      //   826: aaload
      //   827: invokeinterface getSpanStart : (Ljava/lang/Object;)I
      //   832: istore #13
      //   834: iload #13
      //   836: iload #5
      //   838: if_icmpne -> 847
      //   841: iconst_0
      //   842: istore #13
      //   844: goto -> 853
      //   847: iinc #12, 1
      //   850: goto -> 809
      //   853: iload #13
      //   855: ifeq -> 869
      //   858: aload_0
      //   859: getfield this$0 : Landroid/widget/SpellChecker;
      //   862: aload_1
      //   863: iload_3
      //   864: iload #7
      //   866: invokestatic access$900 : (Landroid/widget/SpellChecker;Landroid/text/Editable;II)V
      //   869: iload #10
      //   871: iconst_1
      //   872: iadd
      //   873: istore #12
      //   875: aload_0
      //   876: getfield this$0 : Landroid/widget/SpellChecker;
      //   879: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   882: iload #7
      //   884: invokevirtual following : (I)I
      //   887: istore #8
      //   889: iload #6
      //   891: iload #5
      //   893: if_icmpge -> 954
      //   896: iload #8
      //   898: iconst_m1
      //   899: if_icmpeq -> 909
      //   902: iload #8
      //   904: iload #6
      //   906: if_icmplt -> 954
      //   909: iload #5
      //   911: iload #7
      //   913: sipush #350
      //   916: iadd
      //   917: invokestatic min : (II)I
      //   920: istore #6
      //   922: aload_0
      //   923: getfield this$0 : Landroid/widget/SpellChecker;
      //   926: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   929: aload_1
      //   930: iload #7
      //   932: iload #6
      //   934: invokevirtual setCharSequence : (Ljava/lang/CharSequence;II)V
      //   937: aload_0
      //   938: getfield this$0 : Landroid/widget/SpellChecker;
      //   941: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   944: iload #7
      //   946: invokevirtual following : (I)I
      //   949: istore #8
      //   951: goto -> 954
      //   954: iload #8
      //   956: iconst_m1
      //   957: if_icmpne -> 967
      //   960: iload #11
      //   962: istore #7
      //   964: goto -> 1007
      //   967: aload_0
      //   968: getfield this$0 : Landroid/widget/SpellChecker;
      //   971: invokestatic access$500 : (Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
      //   974: iload #8
      //   976: invokevirtual getBeginning : (I)I
      //   979: istore_3
      //   980: iload_3
      //   981: iconst_m1
      //   982: if_icmpne -> 992
      //   985: iload #11
      //   987: istore #7
      //   989: goto -> 1007
      //   992: iload #8
      //   994: istore #7
      //   996: iload #12
      //   998: istore #10
      //   1000: goto -> 635
      //   1003: iload #11
      //   1005: istore #7
      //   1007: iload #7
      //   1009: ifeq -> 1034
      //   1012: iload_3
      //   1013: iconst_m1
      //   1014: if_icmpeq -> 1034
      //   1017: iload_3
      //   1018: iload #5
      //   1020: if_icmpgt -> 1034
      //   1023: aload_0
      //   1024: aload_1
      //   1025: iload_3
      //   1026: iload #5
      //   1028: invokespecial setRangeSpan : (Landroid/text/Editable;II)V
      //   1031: goto -> 1039
      //   1034: aload_0
      //   1035: aload_1
      //   1036: invokespecial removeRangeSpan : (Landroid/text/Editable;)V
      //   1039: aload_0
      //   1040: getfield this$0 : Landroid/widget/SpellChecker;
      //   1043: invokestatic access$1000 : (Landroid/widget/SpellChecker;)V
      //   1046: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #548	-> 0
      //   #551	-> 14
      //   #554	-> 24
      //   #555	-> 29
      //   #554	-> 37
      //   #557	-> 50
      //   #560	-> 62
      //   #562	-> 74
      //   #563	-> 87
      //   #566	-> 102
      //   #568	-> 115
      //   #569	-> 120
      //   #570	-> 134
      //   #571	-> 144
      //   #574	-> 164
      //   #576	-> 177
      //   #580	-> 183
      //   #581	-> 188
      //   #586	-> 189
      //   #588	-> 210
      //   #591	-> 230
      //   #592	-> 233
      //   #594	-> 239
      //   #595	-> 249
      //   #600	-> 256
      //   #602	-> 259
      //   #603	-> 273
      //   #604	-> 288
      //   #605	-> 301
      //   #606	-> 315
      //   #608	-> 338
      //   #612	-> 343
      //   #613	-> 348
      //   #617	-> 349
      //   #618	-> 352
      //   #620	-> 355
      //   #621	-> 384
      //   #622	-> 395
      //   #623	-> 415
      //   #625	-> 425
      //   #626	-> 434
      //   #627	-> 443
      //   #629	-> 463
      //   #631	-> 473
      //   #634	-> 486
      //   #638	-> 489
      //   #641	-> 492
      //   #642	-> 499
      //   #643	-> 508
      //   #622	-> 519
      //   #620	-> 526
      //   #654	-> 543
      //   #655	-> 549
      //   #657	-> 552
      //   #658	-> 558
      //   #660	-> 609
      //   #662	-> 612
      //   #663	-> 617
      //   #666	-> 628
      //   #667	-> 628
      //   #594	-> 631
      //   #668	-> 635
      //   #669	-> 641
      //   #670	-> 662
      //   #671	-> 669
      //   #672	-> 672
      //   #677	-> 675
      //   #678	-> 688
      //   #679	-> 697
      //   #682	-> 705
      //   #683	-> 718
      //   #684	-> 727
      //   #688	-> 735
      //   #689	-> 738
      //   #690	-> 749
      //   #691	-> 764
      //   #692	-> 777
      //   #693	-> 784
      //   #694	-> 787
      //   #690	-> 790
      //   #699	-> 796
      //   #700	-> 806
      //   #701	-> 821
      //   #702	-> 834
      //   #703	-> 841
      //   #704	-> 844
      //   #700	-> 847
      //   #709	-> 853
      //   #710	-> 858
      //   #712	-> 869
      //   #716	-> 875
      //   #717	-> 875
      //   #718	-> 889
      //   #720	-> 909
      //   #721	-> 909
      //   #722	-> 922
      //   #724	-> 937
      //   #726	-> 954
      //   #727	-> 967
      //   #728	-> 980
      //   #729	-> 985
      //   #731	-> 992
      //   #668	-> 1003
      //   #734	-> 1007
      //   #736	-> 1023
      //   #738	-> 1034
      //   #741	-> 1039
      //   #742	-> 1046
    }
    
    private <T> void removeSpansAt(Editable param1Editable, int param1Int, T[] param1ArrayOfT) {
      int i = param1ArrayOfT.length;
      for (byte b = 0; b < i; b++) {
        T t = param1ArrayOfT[b];
        int j = param1Editable.getSpanStart(t);
        if (j <= param1Int) {
          j = param1Editable.getSpanEnd(t);
          if (j >= param1Int)
            param1Editable.removeSpan(t); 
        } 
      } 
    }
    
    private SpellParser() {}
  }
  
  public static boolean haveWordBoundariesChanged(Editable paramEditable, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool;
    if (paramInt4 != paramInt1 && paramInt3 != paramInt2) {
      bool = true;
    } else if (paramInt4 == paramInt1 && paramInt1 < paramEditable.length()) {
      paramInt1 = Character.codePointAt(paramEditable, paramInt1);
      bool = Character.isLetterOrDigit(paramInt1);
    } else if (paramInt3 == paramInt2 && paramInt2 > 0) {
      paramInt1 = Character.codePointBefore(paramEditable, paramInt2);
      bool = Character.isLetterOrDigit(paramInt1);
    } else {
      bool = false;
    } 
    return bool;
  }
}
