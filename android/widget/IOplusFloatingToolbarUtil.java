package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.view.Menu;

public interface IOplusFloatingToolbarUtil extends IOplusCommonFeature {
  public static final IOplusFloatingToolbarUtil DEFAULT = (IOplusFloatingToolbarUtil)new Object();
  
  public static final String NAME = "IOplusFloatingToolbarUtil";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusFloatingToolbarUtil;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default boolean setSearchMenuItem(int paramInt, Intent paramIntent, CharSequence paramCharSequence, ResolveInfo paramResolveInfo, Menu paramMenu) {
    return false;
  }
  
  default boolean[] handleCursorControllersEnabled(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5) {
    return new boolean[] { paramBoolean4, paramBoolean5 };
  }
  
  default boolean needAllSelected(boolean paramBoolean) {
    return false;
  }
  
  default boolean needHook() {
    return false;
  }
  
  default void updateSelectAllItem(Menu paramMenu, TextView paramTextView, int paramInt) {}
}
