package android.widget;

import android.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;

@Deprecated
public class SlidingDrawer extends ViewGroup {
  private static final int ANIMATION_FRAME_DURATION = 16;
  
  private static final int COLLAPSED_FULL_CLOSED = -10002;
  
  private static final int EXPANDED_FULL_OPEN = -10001;
  
  private static final float MAXIMUM_ACCELERATION = 2000.0F;
  
  private static final float MAXIMUM_MAJOR_VELOCITY = 200.0F;
  
  private static final float MAXIMUM_MINOR_VELOCITY = 150.0F;
  
  private static final float MAXIMUM_TAP_VELOCITY = 100.0F;
  
  public static final int ORIENTATION_HORIZONTAL = 0;
  
  public static final int ORIENTATION_VERTICAL = 1;
  
  private static final int TAP_THRESHOLD = 6;
  
  private static final int VELOCITY_UNITS = 1000;
  
  private boolean mAllowSingleTap;
  
  private boolean mAnimateOnClick;
  
  private float mAnimatedAcceleration;
  
  private float mAnimatedVelocity;
  
  private boolean mAnimating;
  
  private long mAnimationLastTime;
  
  private float mAnimationPosition;
  
  private int mBottomOffset;
  
  private View mContent;
  
  private final int mContentId;
  
  private long mCurrentAnimationTime;
  
  private boolean mExpanded;
  
  private final Rect mFrame;
  
  private View mHandle;
  
  private int mHandleHeight;
  
  private final int mHandleId;
  
  private int mHandleWidth;
  
  private final Rect mInvalidate;
  
  private boolean mLocked;
  
  private final int mMaximumAcceleration;
  
  private final int mMaximumMajorVelocity;
  
  private final int mMaximumMinorVelocity;
  
  private final int mMaximumTapVelocity;
  
  private OnDrawerCloseListener mOnDrawerCloseListener;
  
  private OnDrawerOpenListener mOnDrawerOpenListener;
  
  private OnDrawerScrollListener mOnDrawerScrollListener;
  
  private final Runnable mSlidingRunnable;
  
  private final int mTapThreshold;
  
  private int mTopOffset;
  
  private int mTouchDelta;
  
  private boolean mTracking;
  
  private VelocityTracker mVelocityTracker;
  
  private final int mVelocityUnits;
  
  private boolean mVertical;
  
  public SlidingDrawer(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public SlidingDrawer(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SlidingDrawer(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    boolean bool;
    this.mFrame = new Rect();
    this.mInvalidate = new Rect();
    this.mSlidingRunnable = (Runnable)new Object(this);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SlidingDrawer, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.SlidingDrawer, paramAttributeSet, typedArray, paramInt1, paramInt2);
    paramInt1 = typedArray.getInt(0, 1);
    if (paramInt1 == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mVertical = bool;
    this.mBottomOffset = (int)typedArray.getDimension(1, 0.0F);
    this.mTopOffset = (int)typedArray.getDimension(2, 0.0F);
    this.mAllowSingleTap = typedArray.getBoolean(3, true);
    this.mAnimateOnClick = typedArray.getBoolean(6, true);
    paramInt1 = typedArray.getResourceId(4, 0);
    if (paramInt1 != 0) {
      paramInt2 = typedArray.getResourceId(5, 0);
      if (paramInt2 != 0) {
        if (paramInt1 != paramInt2) {
          this.mHandleId = paramInt1;
          this.mContentId = paramInt2;
          float f = (getResources().getDisplayMetrics()).density;
          this.mTapThreshold = (int)(6.0F * f + 0.5F);
          this.mMaximumTapVelocity = (int)(100.0F * f + 0.5F);
          this.mMaximumMinorVelocity = (int)(150.0F * f + 0.5F);
          this.mMaximumMajorVelocity = (int)(200.0F * f + 0.5F);
          this.mMaximumAcceleration = (int)(2000.0F * f + 0.5F);
          this.mVelocityUnits = (int)(1000.0F * f + 0.5F);
          typedArray.recycle();
          setAlwaysDrawnWithCacheEnabled(false);
          return;
        } 
        throw new IllegalArgumentException("The content and handle attributes must refer to different children.");
      } 
      throw new IllegalArgumentException("The content attribute is required and must refer to a valid child.");
    } 
    throw new IllegalArgumentException("The handle attribute is required and must refer to a valid child.");
  }
  
  protected void onFinishInflate() {
    View view = (View)findViewById(this.mHandleId);
    if (view != null) {
      view.setOnClickListener(new DrawerToggler());
      this.mContent = view = findViewById(this.mContentId);
      if (view != null) {
        view.setVisibility(8);
        return;
      } 
      throw new IllegalArgumentException("The content attribute is must refer to an existing child.");
    } 
    throw new IllegalArgumentException("The handle attribute is must refer to an existing child.");
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getMode(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt1);
    int k = View.MeasureSpec.getMode(paramInt2);
    int m = View.MeasureSpec.getSize(paramInt2);
    if (i != 0 && k != 0) {
      View view = this.mHandle;
      measureChild(view, paramInt1, paramInt2);
      if (this.mVertical) {
        paramInt2 = view.getMeasuredHeight();
        k = this.mTopOffset;
        view = this.mContent;
        paramInt1 = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
        paramInt2 = View.MeasureSpec.makeMeasureSpec(m - paramInt2 - k, 1073741824);
        view.measure(paramInt1, paramInt2);
      } else {
        paramInt1 = view.getMeasuredWidth();
        paramInt2 = this.mTopOffset;
        view = this.mContent;
        paramInt2 = View.MeasureSpec.makeMeasureSpec(j - paramInt1 - paramInt2, 1073741824);
        paramInt1 = View.MeasureSpec.makeMeasureSpec(m, 1073741824);
        view.measure(paramInt2, paramInt1);
      } 
      setMeasuredDimension(j, m);
      return;
    } 
    throw new RuntimeException("SlidingDrawer cannot have UNSPECIFIED dimensions");
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    long l = getDrawingTime();
    View view = this.mHandle;
    boolean bool = this.mVertical;
    drawChild(paramCanvas, view, l);
    if (this.mTracking || this.mAnimating) {
      Bitmap bitmap = this.mContent.getDrawingCache();
      float f = 0.0F;
      if (bitmap != null) {
        if (bool) {
          paramCanvas.drawBitmap(bitmap, 0.0F, view.getBottom(), null);
        } else {
          paramCanvas.drawBitmap(bitmap, view.getRight(), 0.0F, null);
        } 
      } else {
        float f1;
        paramCanvas.save();
        if (bool) {
          f1 = 0.0F;
        } else {
          f1 = (view.getLeft() - this.mTopOffset);
        } 
        if (bool)
          f = (view.getTop() - this.mTopOffset); 
        paramCanvas.translate(f1, f);
        drawChild(paramCanvas, this.mContent, l);
        paramCanvas.restore();
      } 
      return;
    } 
    if (this.mExpanded)
      drawChild(paramCanvas, this.mContent, l); 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mTracking)
      return; 
    paramInt1 = paramInt3 - paramInt1;
    paramInt2 = paramInt4 - paramInt2;
    View view1 = this.mHandle;
    int i = view1.getMeasuredWidth();
    paramInt4 = view1.getMeasuredHeight();
    View view2 = this.mContent;
    if (this.mVertical) {
      paramInt3 = (paramInt1 - i) / 2;
      if (this.mExpanded) {
        paramInt1 = this.mTopOffset;
      } else {
        paramInt1 = paramInt2 - paramInt4 + this.mBottomOffset;
      } 
      int j = this.mTopOffset;
      paramInt2 = view2.getMeasuredWidth();
      int k = this.mTopOffset;
      int m = view2.getMeasuredHeight();
      view2.layout(0, j + paramInt4, paramInt2, k + paramInt4 + m);
      paramInt2 = paramInt1;
    } else {
      if (this.mExpanded) {
        paramInt1 = this.mTopOffset;
      } else {
        paramInt1 = paramInt1 - i + this.mBottomOffset;
      } 
      paramInt2 = (paramInt2 - paramInt4) / 2;
      int k = this.mTopOffset;
      int j = view2.getMeasuredWidth();
      paramInt3 = view2.getMeasuredHeight();
      view2.layout(k + i, 0, k + i + j, paramInt3);
      paramInt3 = paramInt1;
    } 
    view1.layout(paramInt3, paramInt2, paramInt3 + i, paramInt2 + paramInt4);
    this.mHandleHeight = view1.getHeight();
    this.mHandleWidth = view1.getWidth();
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    if (this.mLocked)
      return false; 
    int i = paramMotionEvent.getAction();
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    Rect rect = this.mFrame;
    View view = this.mHandle;
    view.getHitRect(rect);
    if (!this.mTracking && !rect.contains((int)f1, (int)f2))
      return false; 
    if (i == 0) {
      this.mTracking = true;
      view.setPressed(true);
      prepareContent();
      OnDrawerScrollListener onDrawerScrollListener = this.mOnDrawerScrollListener;
      if (onDrawerScrollListener != null)
        onDrawerScrollListener.onScrollStarted(); 
      if (this.mVertical) {
        i = this.mHandle.getTop();
        this.mTouchDelta = (int)f2 - i;
        prepareTracking(i);
      } else {
        i = this.mHandle.getLeft();
        this.mTouchDelta = (int)f1 - i;
        prepareTracking(i);
      } 
      this.mVelocityTracker.addMovement(paramMotionEvent);
    } 
    return true;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    boolean bool = this.mLocked;
    boolean bool1 = true;
    if (bool)
      return true; 
    if (this.mTracking) {
      this.mVelocityTracker.addMovement(paramMotionEvent);
      int i = paramMotionEvent.getAction();
      if (i != 1)
        if (i != 2) {
          if (i != 3)
            bool = bool1; 
        } else {
          if (this.mVertical) {
            f1 = paramMotionEvent.getY();
          } else {
            f1 = paramMotionEvent.getX();
          } 
          moveHandle((int)f1 - this.mTouchDelta);
          bool = bool1;
        }  
      VelocityTracker velocityTracker = this.mVelocityTracker;
      velocityTracker.computeCurrentVelocity(this.mVelocityUnits);
      float f2 = velocityTracker.getYVelocity();
      float f3 = velocityTracker.getXVelocity();
      boolean bool2 = this.mVertical;
      if (bool2) {
        if (f2 < 0.0F) {
          i = 1;
        } else {
          i = 0;
        } 
        float f = f3;
        if (f3 < 0.0F)
          f = -f3; 
        int k = this.mMaximumMinorVelocity;
        f1 = f2;
        f4 = f;
        j = i;
        if (f > k) {
          f4 = k;
          f1 = f2;
          j = i;
        } 
      } else {
        if (f3 < 0.0F) {
          i = 1;
        } else {
          i = 0;
        } 
        float f = f2;
        if (f2 < 0.0F)
          f = -f2; 
        int k = this.mMaximumMinorVelocity;
        f1 = f;
        f4 = f3;
        j = i;
        if (f > k) {
          f1 = k;
          j = i;
          f4 = f3;
        } 
      } 
      float f4 = (float)Math.hypot(f4, f1);
      float f1 = f4;
      if (j != 0)
        f1 = -f4; 
      int j = this.mHandle.getTop();
      i = this.mHandle.getLeft();
      if (Math.abs(f1) < this.mMaximumTapVelocity) {
        bool = this.mExpanded;
        if (bool2 ? ((bool && j < this.mTapThreshold + this.mTopOffset) || (!this.mExpanded && j > this.mBottomOffset + this.mBottom - this.mTop - this.mHandleHeight - this.mTapThreshold)) : ((bool && i < this.mTapThreshold + this.mTopOffset) || (!this.mExpanded && i > this.mBottomOffset + this.mRight - this.mLeft - this.mHandleWidth - this.mTapThreshold))) {
          if (this.mAllowSingleTap) {
            playSoundEffect(0);
            if (this.mExpanded) {
              if (bool2)
                i = j; 
              animateClose(i, true);
            } else {
              if (bool2)
                i = j; 
              animateOpen(i, true);
            } 
          } else {
            if (bool2)
              i = j; 
            performFling(i, f1, false, true);
          } 
        } else {
          if (bool2)
            i = j; 
          performFling(i, f1, false, true);
        } 
      } else {
        if (bool2)
          i = j; 
        performFling(i, f1, false, true);
      } 
    } 
    bool = bool1;
  }
  
  private void animateClose(int paramInt, boolean paramBoolean) {
    prepareTracking(paramInt);
    performFling(paramInt, this.mMaximumAcceleration, true, paramBoolean);
  }
  
  private void animateOpen(int paramInt, boolean paramBoolean) {
    prepareTracking(paramInt);
    performFling(paramInt, -this.mMaximumAcceleration, true, paramBoolean);
  }
  
  private void performFling(int paramInt, float paramFloat, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: i2f
    //   3: putfield mAnimationPosition : F
    //   6: aload_0
    //   7: fload_2
    //   8: putfield mAnimatedVelocity : F
    //   11: aload_0
    //   12: getfield mExpanded : Z
    //   15: ifeq -> 130
    //   18: iload_3
    //   19: ifne -> 107
    //   22: fload_2
    //   23: aload_0
    //   24: getfield mMaximumMajorVelocity : I
    //   27: i2f
    //   28: fcmpl
    //   29: ifgt -> 107
    //   32: aload_0
    //   33: getfield mTopOffset : I
    //   36: istore #5
    //   38: aload_0
    //   39: getfield mVertical : Z
    //   42: ifeq -> 54
    //   45: aload_0
    //   46: getfield mHandleHeight : I
    //   49: istore #6
    //   51: goto -> 60
    //   54: aload_0
    //   55: getfield mHandleWidth : I
    //   58: istore #6
    //   60: iload_1
    //   61: iload #5
    //   63: iload #6
    //   65: iadd
    //   66: if_icmple -> 83
    //   69: fload_2
    //   70: aload_0
    //   71: getfield mMaximumMajorVelocity : I
    //   74: ineg
    //   75: i2f
    //   76: fcmpl
    //   77: ifle -> 83
    //   80: goto -> 107
    //   83: aload_0
    //   84: aload_0
    //   85: getfield mMaximumAcceleration : I
    //   88: ineg
    //   89: i2f
    //   90: putfield mAnimatedAcceleration : F
    //   93: fload_2
    //   94: fconst_0
    //   95: fcmpl
    //   96: ifle -> 229
    //   99: aload_0
    //   100: fconst_0
    //   101: putfield mAnimatedVelocity : F
    //   104: goto -> 229
    //   107: aload_0
    //   108: aload_0
    //   109: getfield mMaximumAcceleration : I
    //   112: i2f
    //   113: putfield mAnimatedAcceleration : F
    //   116: fload_2
    //   117: fconst_0
    //   118: fcmpg
    //   119: ifge -> 229
    //   122: aload_0
    //   123: fconst_0
    //   124: putfield mAnimatedVelocity : F
    //   127: goto -> 229
    //   130: iload_3
    //   131: ifne -> 208
    //   134: fload_2
    //   135: aload_0
    //   136: getfield mMaximumMajorVelocity : I
    //   139: i2f
    //   140: fcmpl
    //   141: ifgt -> 185
    //   144: aload_0
    //   145: getfield mVertical : Z
    //   148: ifeq -> 160
    //   151: aload_0
    //   152: invokevirtual getHeight : ()I
    //   155: istore #6
    //   157: goto -> 166
    //   160: aload_0
    //   161: invokevirtual getWidth : ()I
    //   164: istore #6
    //   166: iload_1
    //   167: iload #6
    //   169: iconst_2
    //   170: idiv
    //   171: if_icmple -> 208
    //   174: fload_2
    //   175: aload_0
    //   176: getfield mMaximumMajorVelocity : I
    //   179: ineg
    //   180: i2f
    //   181: fcmpl
    //   182: ifle -> 208
    //   185: aload_0
    //   186: aload_0
    //   187: getfield mMaximumAcceleration : I
    //   190: i2f
    //   191: putfield mAnimatedAcceleration : F
    //   194: fload_2
    //   195: fconst_0
    //   196: fcmpg
    //   197: ifge -> 229
    //   200: aload_0
    //   201: fconst_0
    //   202: putfield mAnimatedVelocity : F
    //   205: goto -> 229
    //   208: aload_0
    //   209: aload_0
    //   210: getfield mMaximumAcceleration : I
    //   213: ineg
    //   214: i2f
    //   215: putfield mAnimatedAcceleration : F
    //   218: fload_2
    //   219: fconst_0
    //   220: fcmpl
    //   221: ifle -> 229
    //   224: aload_0
    //   225: fconst_0
    //   226: putfield mAnimatedVelocity : F
    //   229: invokestatic uptimeMillis : ()J
    //   232: lstore #7
    //   234: aload_0
    //   235: lload #7
    //   237: putfield mAnimationLastTime : J
    //   240: aload_0
    //   241: lload #7
    //   243: ldc2_w 16
    //   246: ladd
    //   247: putfield mCurrentAnimationTime : J
    //   250: aload_0
    //   251: iconst_1
    //   252: putfield mAnimating : Z
    //   255: aload_0
    //   256: aload_0
    //   257: getfield mSlidingRunnable : Ljava/lang/Runnable;
    //   260: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)Z
    //   263: pop
    //   264: aload_0
    //   265: aload_0
    //   266: getfield mSlidingRunnable : Ljava/lang/Runnable;
    //   269: ldc2_w 16
    //   272: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   275: pop
    //   276: aload_0
    //   277: iload #4
    //   279: invokespecial stopTracking : (Z)V
    //   282: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #517	-> 0
    //   #518	-> 6
    //   #520	-> 11
    //   #521	-> 18
    //   #522	-> 38
    //   #532	-> 83
    //   #533	-> 93
    //   #534	-> 99
    //   #526	-> 107
    //   #527	-> 116
    //   #528	-> 122
    //   #538	-> 130
    //   #539	-> 144
    //   #542	-> 185
    //   #543	-> 194
    //   #544	-> 200
    //   #549	-> 208
    //   #550	-> 218
    //   #551	-> 224
    //   #556	-> 229
    //   #557	-> 234
    //   #558	-> 240
    //   #559	-> 250
    //   #560	-> 255
    //   #561	-> 264
    //   #562	-> 276
    //   #563	-> 282
  }
  
  private void prepareTracking(int paramInt) {
    this.mTracking = true;
    this.mVelocityTracker = VelocityTracker.obtain();
    boolean bool = this.mExpanded;
    if ((bool ^ true) != 0) {
      int j;
      this.mAnimatedAcceleration = this.mMaximumAcceleration;
      this.mAnimatedVelocity = this.mMaximumMajorVelocity;
      int i = this.mBottomOffset;
      if (this.mVertical) {
        paramInt = getHeight();
        j = this.mHandleHeight;
      } else {
        paramInt = getWidth();
        j = this.mHandleWidth;
      } 
      float f = (i + paramInt - j);
      moveHandle((int)f);
      this.mAnimating = true;
      removeCallbacks(this.mSlidingRunnable);
      long l = SystemClock.uptimeMillis();
      this.mAnimationLastTime = l;
      this.mCurrentAnimationTime = 16L + l;
      this.mAnimating = true;
    } else {
      if (this.mAnimating) {
        this.mAnimating = false;
        removeCallbacks(this.mSlidingRunnable);
      } 
      moveHandle(paramInt);
    } 
  }
  
  private void moveHandle(int paramInt) {
    View view = this.mHandle;
    if (this.mVertical) {
      if (paramInt == -10001) {
        view.offsetTopAndBottom(this.mTopOffset - view.getTop());
        invalidate();
      } else if (paramInt == -10002) {
        int i = this.mBottomOffset;
        paramInt = this.mBottom;
        int j = this.mTop, k = this.mHandleHeight;
        int m = view.getTop();
        view.offsetTopAndBottom(i + paramInt - j - k - m);
        invalidate();
      } else {
        int m = view.getTop();
        int j = paramInt - m;
        int i = this.mTopOffset;
        if (paramInt < i) {
          paramInt = i - m;
        } else {
          paramInt = j;
          if (j > this.mBottomOffset + this.mBottom - this.mTop - this.mHandleHeight - m)
            paramInt = this.mBottomOffset + this.mBottom - this.mTop - this.mHandleHeight - m; 
        } 
        view.offsetTopAndBottom(paramInt);
        Rect rect1 = this.mFrame;
        Rect rect2 = this.mInvalidate;
        view.getHitRect(rect1);
        rect2.set(rect1);
        rect2.union(rect1.left, rect1.top - paramInt, rect1.right, rect1.bottom - paramInt);
        i = rect1.bottom;
        m = getWidth();
        int k = rect1.bottom;
        view = this.mContent;
        j = view.getHeight();
        rect2.union(0, i - paramInt, m, k - paramInt + j);
        invalidate(rect2);
      } 
    } else if (paramInt == -10001) {
      view.offsetLeftAndRight(this.mTopOffset - view.getLeft());
      invalidate();
    } else if (paramInt == -10002) {
      int i = this.mBottomOffset, k = this.mRight, m = this.mLeft, j = this.mHandleWidth;
      paramInt = view.getLeft();
      view.offsetLeftAndRight(i + k - m - j - paramInt);
      invalidate();
    } else {
      int m = view.getLeft();
      int j = paramInt - m;
      int i = this.mTopOffset;
      if (paramInt < i) {
        paramInt = i - m;
      } else {
        paramInt = j;
        if (j > this.mBottomOffset + this.mRight - this.mLeft - this.mHandleWidth - m)
          paramInt = this.mBottomOffset + this.mRight - this.mLeft - this.mHandleWidth - m; 
      } 
      view.offsetLeftAndRight(paramInt);
      Rect rect1 = this.mFrame;
      Rect rect2 = this.mInvalidate;
      view.getHitRect(rect1);
      rect2.set(rect1);
      rect2.union(rect1.left - paramInt, rect1.top, rect1.right - paramInt, rect1.bottom);
      m = rect1.right;
      i = rect1.right;
      view = this.mContent;
      int k = view.getWidth();
      j = getHeight();
      rect2.union(m - paramInt, 0, i - paramInt + k, j);
      invalidate(rect2);
    } 
  }
  
  private void prepareContent() {
    if (this.mAnimating)
      return; 
    View view = this.mContent;
    if (view.isLayoutRequested())
      if (this.mVertical) {
        int i = this.mHandleHeight;
        int j = this.mBottom, k = this.mTop, m = this.mTopOffset;
        int n = View.MeasureSpec.makeMeasureSpec(this.mRight - this.mLeft, 1073741824);
        m = View.MeasureSpec.makeMeasureSpec(j - k - i - m, 1073741824);
        view.measure(n, m);
        k = this.mTopOffset;
        m = view.getMeasuredWidth();
        n = this.mTopOffset;
        j = view.getMeasuredHeight();
        view.layout(0, k + i, m, n + i + j);
      } else {
        int i = this.mHandle.getWidth();
        int k = this.mRight, j = this.mLeft, m = this.mTopOffset;
        m = View.MeasureSpec.makeMeasureSpec(k - j - i - m, 1073741824);
        j = this.mBottom;
        k = this.mTop;
        k = View.MeasureSpec.makeMeasureSpec(j - k, 1073741824);
        view.measure(m, k);
        k = this.mTopOffset;
        m = view.getMeasuredWidth();
        j = view.getMeasuredHeight();
        view.layout(i + k, 0, k + i + m, j);
      }  
    view.getViewTreeObserver().dispatchOnPreDraw();
    if (!view.isHardwareAccelerated())
      view.buildDrawingCache(); 
    view.setVisibility(8);
  }
  
  private void stopTracking(boolean paramBoolean) {
    this.mHandle.setPressed(false);
    this.mTracking = false;
    if (paramBoolean) {
      OnDrawerScrollListener onDrawerScrollListener = this.mOnDrawerScrollListener;
      if (onDrawerScrollListener != null)
        onDrawerScrollListener.onScrollEnded(); 
    } 
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null) {
      velocityTracker.recycle();
      this.mVelocityTracker = null;
    } 
  }
  
  private void doAnimation() {
    if (this.mAnimating) {
      int j;
      incrementAnimation();
      float f = this.mAnimationPosition;
      int i = this.mBottomOffset;
      if (this.mVertical) {
        j = getHeight();
      } else {
        j = getWidth();
      } 
      if (f >= (i + j - 1)) {
        this.mAnimating = false;
        closeDrawer();
      } else {
        f = this.mAnimationPosition;
        if (f < this.mTopOffset) {
          this.mAnimating = false;
          openDrawer();
        } else {
          moveHandle((int)f);
          this.mCurrentAnimationTime += 16L;
          postDelayed(this.mSlidingRunnable, 16L);
        } 
      } 
    } 
  }
  
  private void incrementAnimation() {
    long l = SystemClock.uptimeMillis();
    float f1 = (float)(l - this.mAnimationLastTime) / 1000.0F;
    float f2 = this.mAnimationPosition;
    float f3 = this.mAnimatedVelocity;
    float f4 = this.mAnimatedAcceleration;
    this.mAnimationPosition = f3 * f1 + f2 + 0.5F * f4 * f1 * f1;
    this.mAnimatedVelocity = f4 * f1 + f3;
    this.mAnimationLastTime = l;
  }
  
  public void toggle() {
    if (!this.mExpanded) {
      openDrawer();
    } else {
      closeDrawer();
    } 
    invalidate();
    requestLayout();
  }
  
  public void animateToggle() {
    if (!this.mExpanded) {
      animateOpen();
    } else {
      animateClose();
    } 
  }
  
  public void open() {
    openDrawer();
    invalidate();
    requestLayout();
    sendAccessibilityEvent(32);
  }
  
  public void close() {
    closeDrawer();
    invalidate();
    requestLayout();
  }
  
  public void animateClose() {
    int i;
    prepareContent();
    OnDrawerScrollListener onDrawerScrollListener = this.mOnDrawerScrollListener;
    if (onDrawerScrollListener != null)
      onDrawerScrollListener.onScrollStarted(); 
    if (this.mVertical) {
      i = this.mHandle.getTop();
    } else {
      i = this.mHandle.getLeft();
    } 
    animateClose(i, false);
    if (onDrawerScrollListener != null)
      onDrawerScrollListener.onScrollEnded(); 
  }
  
  public void animateOpen() {
    int i;
    prepareContent();
    OnDrawerScrollListener onDrawerScrollListener = this.mOnDrawerScrollListener;
    if (onDrawerScrollListener != null)
      onDrawerScrollListener.onScrollStarted(); 
    if (this.mVertical) {
      i = this.mHandle.getTop();
    } else {
      i = this.mHandle.getLeft();
    } 
    animateOpen(i, false);
    sendAccessibilityEvent(32);
    if (onDrawerScrollListener != null)
      onDrawerScrollListener.onScrollEnded(); 
  }
  
  public CharSequence getAccessibilityClassName() {
    return SlidingDrawer.class.getName();
  }
  
  private void closeDrawer() {
    moveHandle(-10002);
    this.mContent.setVisibility(8);
    this.mContent.destroyDrawingCache();
    if (!this.mExpanded)
      return; 
    this.mExpanded = false;
    OnDrawerCloseListener onDrawerCloseListener = this.mOnDrawerCloseListener;
    if (onDrawerCloseListener != null)
      onDrawerCloseListener.onDrawerClosed(); 
  }
  
  private void openDrawer() {
    moveHandle(-10001);
    this.mContent.setVisibility(0);
    if (this.mExpanded)
      return; 
    this.mExpanded = true;
    OnDrawerOpenListener onDrawerOpenListener = this.mOnDrawerOpenListener;
    if (onDrawerOpenListener != null)
      onDrawerOpenListener.onDrawerOpened(); 
  }
  
  public void setOnDrawerOpenListener(OnDrawerOpenListener paramOnDrawerOpenListener) {
    this.mOnDrawerOpenListener = paramOnDrawerOpenListener;
  }
  
  public void setOnDrawerCloseListener(OnDrawerCloseListener paramOnDrawerCloseListener) {
    this.mOnDrawerCloseListener = paramOnDrawerCloseListener;
  }
  
  public void setOnDrawerScrollListener(OnDrawerScrollListener paramOnDrawerScrollListener) {
    this.mOnDrawerScrollListener = paramOnDrawerScrollListener;
  }
  
  public View getHandle() {
    return this.mHandle;
  }
  
  public View getContent() {
    return this.mContent;
  }
  
  public void unlock() {
    this.mLocked = false;
  }
  
  public void lock() {
    this.mLocked = true;
  }
  
  public boolean isOpened() {
    return this.mExpanded;
  }
  
  public boolean isMoving() {
    return (this.mTracking || this.mAnimating);
  }
  
  class DrawerToggler implements View.OnClickListener {
    final SlidingDrawer this$0;
    
    private DrawerToggler() {}
    
    public void onClick(View param1View) {
      if (SlidingDrawer.this.mLocked)
        return; 
      if (SlidingDrawer.this.mAnimateOnClick) {
        SlidingDrawer.this.animateToggle();
      } else {
        SlidingDrawer.this.toggle();
      } 
    }
  }
  
  class OnDrawerCloseListener {
    public abstract void onDrawerClosed();
  }
  
  class OnDrawerOpenListener {
    public abstract void onDrawerOpened();
  }
  
  class OnDrawerScrollListener {
    public abstract void onScrollEnded();
    
    public abstract void onScrollStarted();
  }
}
