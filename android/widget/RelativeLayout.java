package android.widget;

import android.content.Context;
import android.content.res.ResourceId;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.Pools;
import android.util.SparseArray;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewDebug.IntToString;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.accessibility.AccessibilityEvent;
import android.view.inspector.PropertyMapper;
import android.view.inspector.PropertyReader;
import com.android.internal.R;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

@RemoteView
public class RelativeLayout extends ViewGroup {
  private static final int[] RULES_VERTICAL = new int[] { 2, 3, 4, 6, 8 };
  
  static {
    RULES_HORIZONTAL = new int[] { 0, 1, 5, 7, 16, 17, 18, 19 };
  }
  
  private View mBaselineView = null;
  
  private int mGravity = 8388659;
  
  private final Rect mContentBounds = new Rect();
  
  private final Rect mSelfBounds = new Rect();
  
  private SortedSet<View> mTopToBottomLeftToRightSet = null;
  
  private final DependencyGraph mGraph = new DependencyGraph();
  
  private boolean mAllowBrokenMeasureSpecs = false;
  
  private boolean mMeasureVerticalWithPaddingMargin = false;
  
  public static final int ABOVE = 2;
  
  public static final int ALIGN_BASELINE = 4;
  
  public static final int ALIGN_BOTTOM = 8;
  
  public static final int ALIGN_END = 19;
  
  public static final int ALIGN_LEFT = 5;
  
  public static final int ALIGN_PARENT_BOTTOM = 12;
  
  public static final int ALIGN_PARENT_END = 21;
  
  public static final int ALIGN_PARENT_LEFT = 9;
  
  public static final int ALIGN_PARENT_RIGHT = 11;
  
  public static final int ALIGN_PARENT_START = 20;
  
  public static final int ALIGN_PARENT_TOP = 10;
  
  public static final int ALIGN_RIGHT = 7;
  
  public static final int ALIGN_START = 18;
  
  public static final int ALIGN_TOP = 6;
  
  public static final int BELOW = 3;
  
  public static final int CENTER_HORIZONTAL = 14;
  
  public static final int CENTER_IN_PARENT = 13;
  
  public static final int CENTER_VERTICAL = 15;
  
  private static final int DEFAULT_WIDTH = 65536;
  
  public static final int END_OF = 17;
  
  public static final int LEFT_OF = 0;
  
  public static final int RIGHT_OF = 1;
  
  private static final int[] RULES_HORIZONTAL;
  
  public static final int START_OF = 16;
  
  public static final int TRUE = -1;
  
  private static final int VALUE_NOT_SET = -2147483648;
  
  private static final int VERB_COUNT = 22;
  
  private boolean mDirtyHierarchy;
  
  private int mIgnoreGravity;
  
  private View[] mSortedHorizontalChildren;
  
  private View[] mSortedVerticalChildren;
  
  public RelativeLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public RelativeLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public RelativeLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public RelativeLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    initFromAttributes(paramContext, paramAttributeSet, paramInt1, paramInt2);
    queryCompatibilityModes(paramContext);
  }
  
  private void initFromAttributes(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RelativeLayout, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.RelativeLayout, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mIgnoreGravity = typedArray.getResourceId(1, -1);
    this.mGravity = typedArray.getInt(0, this.mGravity);
    typedArray.recycle();
  }
  
  private void queryCompatibilityModes(Context paramContext) {
    boolean bool2;
    int i = (paramContext.getApplicationInfo()).targetSdkVersion;
    boolean bool1 = true;
    if (i <= 17) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mAllowBrokenMeasureSpecs = bool2;
    if (i >= 18) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mMeasureVerticalWithPaddingMargin = bool2;
  }
  
  public boolean shouldDelayChildPressedState() {
    return false;
  }
  
  @RemotableViewMethod
  public void setIgnoreGravity(int paramInt) {
    this.mIgnoreGravity = paramInt;
  }
  
  public int getIgnoreGravity() {
    return this.mIgnoreGravity;
  }
  
  public int getGravity() {
    return this.mGravity;
  }
  
  @RemotableViewMethod
  public void setGravity(int paramInt) {
    if (this.mGravity != paramInt) {
      int i = paramInt;
      if ((0x800007 & paramInt) == 0)
        i = paramInt | 0x800003; 
      paramInt = i;
      if ((i & 0x70) == 0)
        paramInt = i | 0x30; 
      this.mGravity = paramInt;
      requestLayout();
    } 
  }
  
  @RemotableViewMethod
  public void setHorizontalGravity(int paramInt) {
    int i = paramInt & 0x800007;
    paramInt = this.mGravity;
    if ((0x800007 & paramInt) != i) {
      this.mGravity = 0xFF7FFFF8 & paramInt | i;
      requestLayout();
    } 
  }
  
  @RemotableViewMethod
  public void setVerticalGravity(int paramInt) {
    paramInt &= 0x70;
    int i = this.mGravity;
    if ((i & 0x70) != paramInt) {
      this.mGravity = i & 0xFFFFFF8F | paramInt;
      requestLayout();
    } 
  }
  
  public int getBaseline() {
    int i;
    View view = this.mBaselineView;
    if (view != null) {
      i = view.getBaseline();
    } else {
      i = super.getBaseline();
    } 
    return i;
  }
  
  public void requestLayout() {
    super.requestLayout();
    this.mDirtyHierarchy = true;
  }
  
  private void sortChildren() {
    int i = getChildCount();
    View[] arrayOfView = this.mSortedVerticalChildren;
    if (arrayOfView == null || arrayOfView.length != i)
      this.mSortedVerticalChildren = new View[i]; 
    arrayOfView = this.mSortedHorizontalChildren;
    if (arrayOfView == null || arrayOfView.length != i)
      this.mSortedHorizontalChildren = new View[i]; 
    DependencyGraph dependencyGraph = this.mGraph;
    dependencyGraph.clear();
    for (byte b = 0; b < i; b++)
      dependencyGraph.add(getChildAt(b)); 
    dependencyGraph.getSortedViews(this.mSortedVerticalChildren, RULES_VERTICAL);
    dependencyGraph.getSortedViews(this.mSortedHorizontalChildren, RULES_HORIZONTAL);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDirtyHierarchy : Z
    //   4: ifeq -> 16
    //   7: aload_0
    //   8: iconst_0
    //   9: putfield mDirtyHierarchy : Z
    //   12: aload_0
    //   13: invokespecial sortChildren : ()V
    //   16: iconst_m1
    //   17: istore_3
    //   18: iconst_m1
    //   19: istore #4
    //   21: iconst_0
    //   22: istore #5
    //   24: iconst_0
    //   25: istore #6
    //   27: iload_1
    //   28: invokestatic getMode : (I)I
    //   31: istore #7
    //   33: iload_2
    //   34: invokestatic getMode : (I)I
    //   37: istore #8
    //   39: iload_1
    //   40: invokestatic getSize : (I)I
    //   43: istore #9
    //   45: iload_2
    //   46: invokestatic getSize : (I)I
    //   49: istore #10
    //   51: iload #7
    //   53: ifeq -> 59
    //   56: iload #9
    //   58: istore_3
    //   59: iload #8
    //   61: ifeq -> 68
    //   64: iload #10
    //   66: istore #4
    //   68: iload #7
    //   70: ldc 1073741824
    //   72: if_icmpne -> 78
    //   75: iload_3
    //   76: istore #5
    //   78: iload #8
    //   80: ldc 1073741824
    //   82: if_icmpne -> 89
    //   85: iload #4
    //   87: istore #6
    //   89: aconst_null
    //   90: astore #11
    //   92: aload_0
    //   93: getfield mGravity : I
    //   96: ldc_w 8388615
    //   99: iand
    //   100: istore #10
    //   102: iload #10
    //   104: ldc_w 8388611
    //   107: if_icmpeq -> 121
    //   110: iload #10
    //   112: ifeq -> 121
    //   115: iconst_1
    //   116: istore #12
    //   118: goto -> 124
    //   121: iconst_0
    //   122: istore #12
    //   124: aload_0
    //   125: getfield mGravity : I
    //   128: bipush #112
    //   130: iand
    //   131: istore #10
    //   133: iload #10
    //   135: bipush #48
    //   137: if_icmpeq -> 151
    //   140: iload #10
    //   142: ifeq -> 151
    //   145: iconst_1
    //   146: istore #13
    //   148: goto -> 154
    //   151: iconst_0
    //   152: istore #13
    //   154: iconst_0
    //   155: istore #14
    //   157: iconst_0
    //   158: istore #15
    //   160: iload #12
    //   162: ifne -> 174
    //   165: aload #11
    //   167: astore #16
    //   169: iload #13
    //   171: ifeq -> 198
    //   174: aload_0
    //   175: getfield mIgnoreGravity : I
    //   178: istore #10
    //   180: aload #11
    //   182: astore #16
    //   184: iload #10
    //   186: iconst_m1
    //   187: if_icmpeq -> 198
    //   190: aload_0
    //   191: iload #10
    //   193: invokevirtual findViewById : (I)Landroid/view/View;
    //   196: astore #16
    //   198: iload #7
    //   200: ldc 1073741824
    //   202: if_icmpeq -> 211
    //   205: iconst_1
    //   206: istore #17
    //   208: goto -> 214
    //   211: iconst_0
    //   212: istore #17
    //   214: iload #8
    //   216: ldc 1073741824
    //   218: if_icmpeq -> 227
    //   221: iconst_1
    //   222: istore #18
    //   224: goto -> 230
    //   227: iconst_0
    //   228: istore #18
    //   230: aload_0
    //   231: invokevirtual getLayoutDirection : ()I
    //   234: istore #19
    //   236: aload_0
    //   237: invokevirtual isLayoutRtl : ()Z
    //   240: ifeq -> 258
    //   243: iload_3
    //   244: istore #10
    //   246: iload_3
    //   247: iconst_m1
    //   248: if_icmpne -> 261
    //   251: ldc 65536
    //   253: istore #10
    //   255: goto -> 261
    //   258: iload_3
    //   259: istore #10
    //   261: aload_0
    //   262: getfield mSortedHorizontalChildren : [Landroid/view/View;
    //   265: astore #11
    //   267: aload #11
    //   269: arraylength
    //   270: istore #9
    //   272: iconst_0
    //   273: istore #7
    //   275: iload #8
    //   277: istore_3
    //   278: iload #7
    //   280: iload #9
    //   282: if_icmpge -> 379
    //   285: aload #11
    //   287: iload #7
    //   289: aaload
    //   290: astore #20
    //   292: iload #14
    //   294: istore #8
    //   296: aload #20
    //   298: invokevirtual getVisibility : ()I
    //   301: bipush #8
    //   303: if_icmpeq -> 369
    //   306: aload #20
    //   308: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   311: checkcast android/widget/RelativeLayout$LayoutParams
    //   314: astore #21
    //   316: aload #21
    //   318: iload #19
    //   320: invokevirtual getRules : (I)[I
    //   323: astore #22
    //   325: aload_0
    //   326: aload #21
    //   328: iload #10
    //   330: aload #22
    //   332: invokespecial applyHorizontalSizeRules : (Landroid/widget/RelativeLayout$LayoutParams;I[I)V
    //   335: aload_0
    //   336: aload #20
    //   338: aload #21
    //   340: iload #10
    //   342: iload #4
    //   344: invokespecial measureChildHorizontal : (Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;II)V
    //   347: iload #14
    //   349: istore #8
    //   351: aload_0
    //   352: aload #20
    //   354: aload #21
    //   356: iload #10
    //   358: iload #17
    //   360: invokespecial positionChildHorizontal : (Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;IZ)Z
    //   363: ifeq -> 369
    //   366: iconst_1
    //   367: istore #8
    //   369: iinc #7, 1
    //   372: iload #8
    //   374: istore #14
    //   376: goto -> 278
    //   379: aload_0
    //   380: getfield mSortedVerticalChildren : [Landroid/view/View;
    //   383: astore #22
    //   385: aload #22
    //   387: arraylength
    //   388: istore #23
    //   390: aload_0
    //   391: invokevirtual getContext : ()Landroid/content/Context;
    //   394: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   397: getfield targetSdkVersion : I
    //   400: istore #24
    //   402: ldc -2147483648
    //   404: istore #25
    //   406: ldc -2147483648
    //   408: istore #7
    //   410: iload #19
    //   412: istore_3
    //   413: ldc_w 2147483647
    //   416: istore #9
    //   418: ldc_w 2147483647
    //   421: istore #26
    //   423: iconst_0
    //   424: istore #27
    //   426: iload #5
    //   428: istore #8
    //   430: iload #7
    //   432: istore #19
    //   434: iload #15
    //   436: istore #7
    //   438: iload #6
    //   440: istore #5
    //   442: iload #23
    //   444: istore #15
    //   446: iload #4
    //   448: istore #23
    //   450: iload #26
    //   452: istore #6
    //   454: iload #27
    //   456: iload #15
    //   458: if_icmpge -> 889
    //   461: aload #22
    //   463: iload #27
    //   465: aaload
    //   466: astore #11
    //   468: aload #11
    //   470: invokevirtual getVisibility : ()I
    //   473: bipush #8
    //   475: if_icmpeq -> 843
    //   478: aload #11
    //   480: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   483: checkcast android/widget/RelativeLayout$LayoutParams
    //   486: astore #21
    //   488: aload_0
    //   489: aload #21
    //   491: iload #23
    //   493: aload #11
    //   495: invokevirtual getBaseline : ()I
    //   498: invokespecial applyVerticalSizeRules : (Landroid/widget/RelativeLayout$LayoutParams;II)V
    //   501: aload_0
    //   502: aload #11
    //   504: aload #21
    //   506: iload #10
    //   508: iload #23
    //   510: invokespecial measureChild : (Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;II)V
    //   513: aload_0
    //   514: aload #11
    //   516: aload #21
    //   518: iload #23
    //   520: iload #18
    //   522: invokespecial positionChildVertical : (Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;IZ)Z
    //   525: ifeq -> 531
    //   528: iconst_1
    //   529: istore #7
    //   531: iload #17
    //   533: ifeq -> 635
    //   536: aload_0
    //   537: invokevirtual isLayoutRtl : ()Z
    //   540: ifeq -> 592
    //   543: iload #24
    //   545: bipush #19
    //   547: if_icmpge -> 568
    //   550: iload #8
    //   552: iload #10
    //   554: aload #21
    //   556: invokestatic access$100 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   559: isub
    //   560: invokestatic max : (II)I
    //   563: istore #4
    //   565: goto -> 639
    //   568: iload #8
    //   570: iload #10
    //   572: aload #21
    //   574: invokestatic access$100 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   577: isub
    //   578: aload #21
    //   580: getfield leftMargin : I
    //   583: iadd
    //   584: invokestatic max : (II)I
    //   587: istore #4
    //   589: goto -> 639
    //   592: iload #24
    //   594: bipush #19
    //   596: if_icmpge -> 614
    //   599: iload #8
    //   601: aload #21
    //   603: invokestatic access$200 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   606: invokestatic max : (II)I
    //   609: istore #4
    //   611: goto -> 639
    //   614: iload #8
    //   616: aload #21
    //   618: invokestatic access$200 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   621: aload #21
    //   623: getfield rightMargin : I
    //   626: iadd
    //   627: invokestatic max : (II)I
    //   630: istore #4
    //   632: goto -> 639
    //   635: iload #8
    //   637: istore #4
    //   639: iload #5
    //   641: istore #8
    //   643: iload #18
    //   645: ifeq -> 688
    //   648: iload #24
    //   650: bipush #19
    //   652: if_icmpge -> 670
    //   655: iload #5
    //   657: aload #21
    //   659: invokestatic access$300 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   662: invokestatic max : (II)I
    //   665: istore #8
    //   667: goto -> 688
    //   670: iload #5
    //   672: aload #21
    //   674: invokestatic access$300 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   677: aload #21
    //   679: getfield bottomMargin : I
    //   682: iadd
    //   683: invokestatic max : (II)I
    //   686: istore #8
    //   688: aload #11
    //   690: aload #16
    //   692: if_acmpne -> 708
    //   695: iload #6
    //   697: istore #5
    //   699: iload #9
    //   701: istore #26
    //   703: iload #13
    //   705: ifeq -> 744
    //   708: iload #9
    //   710: aload #21
    //   712: invokestatic access$100 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   715: aload #21
    //   717: getfield leftMargin : I
    //   720: isub
    //   721: invokestatic min : (II)I
    //   724: istore #26
    //   726: iload #6
    //   728: aload #21
    //   730: invokestatic access$400 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   733: aload #21
    //   735: getfield topMargin : I
    //   738: isub
    //   739: invokestatic min : (II)I
    //   742: istore #5
    //   744: aload #11
    //   746: aload #16
    //   748: if_acmpne -> 784
    //   751: iload #5
    //   753: istore #6
    //   755: iload #4
    //   757: istore #28
    //   759: iload #8
    //   761: istore #29
    //   763: iload #26
    //   765: istore #9
    //   767: iload #7
    //   769: istore #30
    //   771: iload #25
    //   773: istore #31
    //   775: iload #19
    //   777: istore #32
    //   779: iload #12
    //   781: ifeq -> 863
    //   784: iload #25
    //   786: aload #21
    //   788: invokestatic access$200 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   791: aload #21
    //   793: getfield rightMargin : I
    //   796: iadd
    //   797: invokestatic max : (II)I
    //   800: istore #31
    //   802: iload #19
    //   804: aload #21
    //   806: invokestatic access$300 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   809: aload #21
    //   811: getfield bottomMargin : I
    //   814: iadd
    //   815: invokestatic max : (II)I
    //   818: istore #32
    //   820: iload #5
    //   822: istore #6
    //   824: iload #4
    //   826: istore #28
    //   828: iload #8
    //   830: istore #29
    //   832: iload #26
    //   834: istore #9
    //   836: iload #7
    //   838: istore #30
    //   840: goto -> 863
    //   843: iload #19
    //   845: istore #32
    //   847: iload #25
    //   849: istore #31
    //   851: iload #7
    //   853: istore #30
    //   855: iload #5
    //   857: istore #29
    //   859: iload #8
    //   861: istore #28
    //   863: iinc #27, 1
    //   866: iload #28
    //   868: istore #8
    //   870: iload #29
    //   872: istore #5
    //   874: iload #30
    //   876: istore #7
    //   878: iload #31
    //   880: istore #25
    //   882: iload #32
    //   884: istore #19
    //   886: goto -> 454
    //   889: iload #15
    //   891: istore #4
    //   893: aconst_null
    //   894: astore #11
    //   896: aconst_null
    //   897: astore #21
    //   899: iconst_0
    //   900: istore #15
    //   902: iload #15
    //   904: iload #4
    //   906: if_icmpge -> 995
    //   909: aload #22
    //   911: iload #15
    //   913: aaload
    //   914: astore #33
    //   916: aload #11
    //   918: astore #34
    //   920: aload #21
    //   922: astore #20
    //   924: aload #33
    //   926: invokevirtual getVisibility : ()I
    //   929: bipush #8
    //   931: if_icmpeq -> 981
    //   934: aload #33
    //   936: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   939: checkcast android/widget/RelativeLayout$LayoutParams
    //   942: astore #35
    //   944: aload #11
    //   946: ifnull -> 973
    //   949: aload #21
    //   951: ifnull -> 973
    //   954: aload #11
    //   956: astore #34
    //   958: aload #21
    //   960: astore #20
    //   962: aload_0
    //   963: aload #35
    //   965: aload #21
    //   967: invokespecial compareLayoutPosition : (Landroid/widget/RelativeLayout$LayoutParams;Landroid/widget/RelativeLayout$LayoutParams;)I
    //   970: ifge -> 981
    //   973: aload #33
    //   975: astore #34
    //   977: aload #35
    //   979: astore #20
    //   981: iinc #15, 1
    //   984: aload #34
    //   986: astore #11
    //   988: aload #20
    //   990: astore #21
    //   992: goto -> 902
    //   995: aload_0
    //   996: aload #11
    //   998: putfield mBaselineView : Landroid/view/View;
    //   1001: iload #17
    //   1003: ifeq -> 1215
    //   1006: iload #8
    //   1008: aload_0
    //   1009: getfield mPaddingRight : I
    //   1012: iadd
    //   1013: istore #15
    //   1015: iload #15
    //   1017: istore #8
    //   1019: aload_0
    //   1020: getfield mLayoutParams : Landroid/view/ViewGroup$LayoutParams;
    //   1023: ifnull -> 1054
    //   1026: iload #15
    //   1028: istore #8
    //   1030: aload_0
    //   1031: getfield mLayoutParams : Landroid/view/ViewGroup$LayoutParams;
    //   1034: getfield width : I
    //   1037: iflt -> 1054
    //   1040: iload #15
    //   1042: aload_0
    //   1043: getfield mLayoutParams : Landroid/view/ViewGroup$LayoutParams;
    //   1046: getfield width : I
    //   1049: invokestatic max : (II)I
    //   1052: istore #8
    //   1054: iload #8
    //   1056: aload_0
    //   1057: invokevirtual getSuggestedMinimumWidth : ()I
    //   1060: invokestatic max : (II)I
    //   1063: istore #8
    //   1065: iload #8
    //   1067: iload_1
    //   1068: invokestatic resolveSize : (II)I
    //   1071: istore #8
    //   1073: iload #14
    //   1075: ifeq -> 1210
    //   1078: iconst_0
    //   1079: istore_1
    //   1080: iload_1
    //   1081: iload #4
    //   1083: if_icmpge -> 1205
    //   1086: aload #22
    //   1088: iload_1
    //   1089: aaload
    //   1090: astore #35
    //   1092: aload #35
    //   1094: invokevirtual getVisibility : ()I
    //   1097: bipush #8
    //   1099: if_icmpeq -> 1199
    //   1102: aload #35
    //   1104: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   1107: checkcast android/widget/RelativeLayout$LayoutParams
    //   1110: astore #34
    //   1112: aload #34
    //   1114: iload_3
    //   1115: invokevirtual getRules : (I)[I
    //   1118: astore #20
    //   1120: aload #20
    //   1122: bipush #13
    //   1124: iaload
    //   1125: ifne -> 1187
    //   1128: aload #20
    //   1130: bipush #14
    //   1132: iaload
    //   1133: ifeq -> 1139
    //   1136: goto -> 1187
    //   1139: aload #20
    //   1141: bipush #11
    //   1143: iaload
    //   1144: ifeq -> 1199
    //   1147: aload #35
    //   1149: invokevirtual getMeasuredWidth : ()I
    //   1152: istore #15
    //   1154: aload #34
    //   1156: iload #8
    //   1158: aload_0
    //   1159: getfield mPaddingRight : I
    //   1162: isub
    //   1163: iload #15
    //   1165: isub
    //   1166: invokestatic access$102 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1169: pop
    //   1170: aload #34
    //   1172: aload #34
    //   1174: invokestatic access$100 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   1177: iload #15
    //   1179: iadd
    //   1180: invokestatic access$202 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1183: pop
    //   1184: goto -> 1199
    //   1187: aload #35
    //   1189: aload #34
    //   1191: iload #8
    //   1193: invokestatic centerHorizontal : (Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;I)V
    //   1196: goto -> 1199
    //   1199: iinc #1, 1
    //   1202: goto -> 1080
    //   1205: iload_3
    //   1206: istore_1
    //   1207: goto -> 1217
    //   1210: iload_3
    //   1211: istore_1
    //   1212: goto -> 1217
    //   1215: iload_3
    //   1216: istore_1
    //   1217: iload #18
    //   1219: ifeq -> 1421
    //   1222: iload #5
    //   1224: aload_0
    //   1225: getfield mPaddingBottom : I
    //   1228: iadd
    //   1229: istore #5
    //   1231: iload #5
    //   1233: istore_3
    //   1234: aload_0
    //   1235: getfield mLayoutParams : Landroid/view/ViewGroup$LayoutParams;
    //   1238: ifnull -> 1267
    //   1241: iload #5
    //   1243: istore_3
    //   1244: aload_0
    //   1245: getfield mLayoutParams : Landroid/view/ViewGroup$LayoutParams;
    //   1248: getfield height : I
    //   1251: iflt -> 1267
    //   1254: iload #5
    //   1256: aload_0
    //   1257: getfield mLayoutParams : Landroid/view/ViewGroup$LayoutParams;
    //   1260: getfield height : I
    //   1263: invokestatic max : (II)I
    //   1266: istore_3
    //   1267: iload_3
    //   1268: aload_0
    //   1269: invokevirtual getSuggestedMinimumHeight : ()I
    //   1272: invokestatic max : (II)I
    //   1275: istore_3
    //   1276: iload_3
    //   1277: iload_2
    //   1278: invokestatic resolveSize : (II)I
    //   1281: istore #5
    //   1283: iload #7
    //   1285: ifeq -> 1418
    //   1288: iconst_0
    //   1289: istore_2
    //   1290: iload_2
    //   1291: iload #4
    //   1293: if_icmpge -> 1415
    //   1296: aload #22
    //   1298: iload_2
    //   1299: aaload
    //   1300: astore #20
    //   1302: aload #20
    //   1304: invokevirtual getVisibility : ()I
    //   1307: bipush #8
    //   1309: if_icmpeq -> 1409
    //   1312: aload #20
    //   1314: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   1317: checkcast android/widget/RelativeLayout$LayoutParams
    //   1320: astore #11
    //   1322: aload #11
    //   1324: iload_1
    //   1325: invokevirtual getRules : (I)[I
    //   1328: astore #21
    //   1330: aload #21
    //   1332: bipush #13
    //   1334: iaload
    //   1335: ifne -> 1397
    //   1338: aload #21
    //   1340: bipush #15
    //   1342: iaload
    //   1343: ifeq -> 1349
    //   1346: goto -> 1397
    //   1349: aload #21
    //   1351: bipush #12
    //   1353: iaload
    //   1354: ifeq -> 1394
    //   1357: aload #20
    //   1359: invokevirtual getMeasuredHeight : ()I
    //   1362: istore_3
    //   1363: aload #11
    //   1365: iload #5
    //   1367: aload_0
    //   1368: getfield mPaddingBottom : I
    //   1371: isub
    //   1372: iload_3
    //   1373: isub
    //   1374: invokestatic access$402 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1377: pop
    //   1378: aload #11
    //   1380: aload #11
    //   1382: invokestatic access$400 : (Landroid/widget/RelativeLayout$LayoutParams;)I
    //   1385: iload_3
    //   1386: iadd
    //   1387: invokestatic access$302 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1390: pop
    //   1391: goto -> 1409
    //   1394: goto -> 1409
    //   1397: aload #20
    //   1399: aload #11
    //   1401: iload #5
    //   1403: invokestatic centerVertical : (Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;I)V
    //   1406: goto -> 1409
    //   1409: iinc #2, 1
    //   1412: goto -> 1290
    //   1415: goto -> 1421
    //   1418: goto -> 1421
    //   1421: iload #12
    //   1423: ifne -> 1437
    //   1426: iload #13
    //   1428: ifeq -> 1434
    //   1431: goto -> 1437
    //   1434: goto -> 1626
    //   1437: aload_0
    //   1438: getfield mSelfBounds : Landroid/graphics/Rect;
    //   1441: astore #11
    //   1443: aload #11
    //   1445: aload_0
    //   1446: getfield mPaddingLeft : I
    //   1449: aload_0
    //   1450: getfield mPaddingTop : I
    //   1453: iload #8
    //   1455: aload_0
    //   1456: getfield mPaddingRight : I
    //   1459: isub
    //   1460: iload #5
    //   1462: aload_0
    //   1463: getfield mPaddingBottom : I
    //   1466: isub
    //   1467: invokevirtual set : (IIII)V
    //   1470: aload_0
    //   1471: getfield mContentBounds : Landroid/graphics/Rect;
    //   1474: astore #20
    //   1476: aload_0
    //   1477: getfield mGravity : I
    //   1480: iload #25
    //   1482: iload #9
    //   1484: isub
    //   1485: iload #19
    //   1487: iload #6
    //   1489: isub
    //   1490: aload #11
    //   1492: aload #20
    //   1494: iload_1
    //   1495: invokestatic apply : (IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V
    //   1498: aload #20
    //   1500: getfield left : I
    //   1503: iload #9
    //   1505: isub
    //   1506: istore_2
    //   1507: aload #20
    //   1509: getfield top : I
    //   1512: iload #6
    //   1514: isub
    //   1515: istore_3
    //   1516: iload_2
    //   1517: ifne -> 1530
    //   1520: iload_3
    //   1521: ifeq -> 1527
    //   1524: goto -> 1530
    //   1527: goto -> 1626
    //   1530: iconst_0
    //   1531: istore_1
    //   1532: aload #16
    //   1534: astore #21
    //   1536: aload #20
    //   1538: astore #16
    //   1540: iload_1
    //   1541: iload #4
    //   1543: if_icmpge -> 1626
    //   1546: aload #22
    //   1548: iload_1
    //   1549: aaload
    //   1550: astore #20
    //   1552: aload #20
    //   1554: invokevirtual getVisibility : ()I
    //   1557: bipush #8
    //   1559: if_icmpeq -> 1620
    //   1562: aload #20
    //   1564: aload #21
    //   1566: if_acmpeq -> 1620
    //   1569: aload #20
    //   1571: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   1574: checkcast android/widget/RelativeLayout$LayoutParams
    //   1577: astore #20
    //   1579: iload #12
    //   1581: ifeq -> 1598
    //   1584: aload #20
    //   1586: iload_2
    //   1587: invokestatic access$112 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1590: pop
    //   1591: aload #20
    //   1593: iload_2
    //   1594: invokestatic access$212 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1597: pop
    //   1598: iload #13
    //   1600: ifeq -> 1620
    //   1603: aload #20
    //   1605: iload_3
    //   1606: invokestatic access$412 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1609: pop
    //   1610: aload #20
    //   1612: iload_3
    //   1613: invokestatic access$312 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1616: pop
    //   1617: goto -> 1620
    //   1620: iinc #1, 1
    //   1623: goto -> 1540
    //   1626: aload_0
    //   1627: invokevirtual isLayoutRtl : ()Z
    //   1630: ifeq -> 1693
    //   1633: iload #10
    //   1635: iload #8
    //   1637: isub
    //   1638: istore_2
    //   1639: iconst_0
    //   1640: istore_1
    //   1641: iload_1
    //   1642: iload #4
    //   1644: if_icmpge -> 1693
    //   1647: aload #22
    //   1649: iload_1
    //   1650: aaload
    //   1651: astore #16
    //   1653: aload #16
    //   1655: invokevirtual getVisibility : ()I
    //   1658: bipush #8
    //   1660: if_icmpeq -> 1687
    //   1663: aload #16
    //   1665: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   1668: checkcast android/widget/RelativeLayout$LayoutParams
    //   1671: astore #16
    //   1673: aload #16
    //   1675: iload_2
    //   1676: invokestatic access$120 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1679: pop
    //   1680: aload #16
    //   1682: iload_2
    //   1683: invokestatic access$220 : (Landroid/widget/RelativeLayout$LayoutParams;I)I
    //   1686: pop
    //   1687: iinc #1, 1
    //   1690: goto -> 1641
    //   1693: aload_0
    //   1694: iload #8
    //   1696: iload #5
    //   1698: invokevirtual setMeasuredDimension : (II)V
    //   1701: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #407	-> 0
    //   #408	-> 7
    //   #409	-> 12
    //   #412	-> 16
    //   #413	-> 18
    //   #415	-> 21
    //   #416	-> 24
    //   #418	-> 27
    //   #419	-> 33
    //   #420	-> 39
    //   #421	-> 45
    //   #424	-> 51
    //   #425	-> 56
    //   #428	-> 59
    //   #429	-> 64
    //   #432	-> 68
    //   #433	-> 75
    //   #436	-> 78
    //   #437	-> 85
    //   #440	-> 89
    //   #441	-> 92
    //   #442	-> 102
    //   #443	-> 124
    //   #444	-> 133
    //   #446	-> 154
    //   #447	-> 154
    //   #448	-> 154
    //   #449	-> 154
    //   #451	-> 154
    //   #452	-> 157
    //   #454	-> 160
    //   #455	-> 190
    //   #458	-> 198
    //   #459	-> 214
    //   #466	-> 230
    //   #467	-> 236
    //   #468	-> 251
    //   #467	-> 258
    //   #471	-> 261
    //   #472	-> 267
    //   #474	-> 272
    //   #475	-> 285
    //   #476	-> 292
    //   #477	-> 306
    //   #478	-> 316
    //   #480	-> 325
    //   #481	-> 335
    //   #483	-> 347
    //   #484	-> 366
    //   #474	-> 369
    //   #489	-> 379
    //   #490	-> 385
    //   #491	-> 390
    //   #493	-> 402
    //   #494	-> 461
    //   #495	-> 468
    //   #496	-> 478
    //   #498	-> 488
    //   #499	-> 501
    //   #500	-> 513
    //   #501	-> 528
    //   #504	-> 531
    //   #505	-> 536
    //   #506	-> 543
    //   #507	-> 550
    //   #509	-> 568
    //   #512	-> 592
    //   #513	-> 599
    //   #515	-> 614
    //   #504	-> 635
    //   #520	-> 639
    //   #521	-> 648
    //   #522	-> 655
    //   #524	-> 670
    //   #528	-> 688
    //   #529	-> 708
    //   #530	-> 726
    //   #533	-> 744
    //   #534	-> 784
    //   #535	-> 802
    //   #495	-> 843
    //   #493	-> 863
    //   #542	-> 893
    //   #543	-> 896
    //   #544	-> 899
    //   #545	-> 909
    //   #546	-> 916
    //   #547	-> 934
    //   #548	-> 944
    //   #549	-> 954
    //   #550	-> 973
    //   #551	-> 977
    //   #544	-> 981
    //   #555	-> 995
    //   #557	-> 1001
    //   #560	-> 1006
    //   #562	-> 1015
    //   #563	-> 1040
    //   #566	-> 1054
    //   #567	-> 1065
    //   #569	-> 1073
    //   #570	-> 1078
    //   #571	-> 1086
    //   #572	-> 1092
    //   #573	-> 1102
    //   #574	-> 1112
    //   #575	-> 1120
    //   #577	-> 1139
    //   #578	-> 1147
    //   #579	-> 1154
    //   #580	-> 1170
    //   #576	-> 1187
    //   #572	-> 1199
    //   #570	-> 1199
    //   #569	-> 1210
    //   #557	-> 1215
    //   #587	-> 1217
    //   #590	-> 1222
    //   #592	-> 1231
    //   #593	-> 1254
    //   #596	-> 1267
    //   #597	-> 1276
    //   #599	-> 1283
    //   #600	-> 1288
    //   #601	-> 1296
    //   #602	-> 1302
    //   #603	-> 1312
    //   #604	-> 1322
    //   #605	-> 1330
    //   #607	-> 1349
    //   #608	-> 1357
    //   #609	-> 1363
    //   #610	-> 1378
    //   #607	-> 1394
    //   #605	-> 1397
    //   #606	-> 1397
    //   #602	-> 1409
    //   #600	-> 1409
    //   #599	-> 1418
    //   #587	-> 1421
    //   #617	-> 1421
    //   #618	-> 1437
    //   #619	-> 1443
    //   #622	-> 1470
    //   #623	-> 1476
    //   #626	-> 1498
    //   #627	-> 1507
    //   #628	-> 1516
    //   #629	-> 1530
    //   #630	-> 1546
    //   #631	-> 1552
    //   #632	-> 1569
    //   #633	-> 1579
    //   #634	-> 1584
    //   #635	-> 1591
    //   #637	-> 1598
    //   #638	-> 1603
    //   #639	-> 1610
    //   #631	-> 1620
    //   #629	-> 1620
    //   #646	-> 1626
    //   #647	-> 1633
    //   #648	-> 1639
    //   #649	-> 1647
    //   #650	-> 1653
    //   #651	-> 1663
    //   #652	-> 1673
    //   #653	-> 1680
    //   #648	-> 1687
    //   #658	-> 1693
    //   #659	-> 1701
  }
  
  private int compareLayoutPosition(LayoutParams paramLayoutParams1, LayoutParams paramLayoutParams2) {
    int i = paramLayoutParams1.mTop - paramLayoutParams2.mTop;
    if (i != 0)
      return i; 
    return paramLayoutParams1.mLeft - paramLayoutParams2.mLeft;
  }
  
  private void measureChild(View paramView, LayoutParams paramLayoutParams, int paramInt1, int paramInt2) {
    int i = paramLayoutParams.mLeft;
    int j = paramLayoutParams.mRight, k = paramLayoutParams.width, m = paramLayoutParams.leftMargin, n = paramLayoutParams.rightMargin, i1 = this.mPaddingLeft, i2 = this.mPaddingRight;
    paramInt1 = getChildMeasureSpec(i, j, k, m, n, i1, i2, paramInt1);
    j = paramLayoutParams.mTop;
    m = paramLayoutParams.mBottom;
    i1 = paramLayoutParams.height;
    k = paramLayoutParams.topMargin;
    i = paramLayoutParams.bottomMargin;
    i2 = this.mPaddingTop;
    n = this.mPaddingBottom;
    paramInt2 = getChildMeasureSpec(j, m, i1, k, i, i2, n, paramInt2);
    paramView.measure(paramInt1, paramInt2);
  }
  
  private void measureChildHorizontal(View paramView, LayoutParams paramLayoutParams, int paramInt1, int paramInt2) {
    int i = getChildMeasureSpec(paramLayoutParams.mLeft, paramLayoutParams.mRight, paramLayoutParams.width, paramLayoutParams.leftMargin, paramLayoutParams.rightMargin, this.mPaddingLeft, this.mPaddingRight, paramInt1);
    if (paramInt2 < 0 && !this.mAllowBrokenMeasureSpecs) {
      if (paramLayoutParams.height >= 0) {
        paramInt1 = View.MeasureSpec.makeMeasureSpec(paramLayoutParams.height, 1073741824);
      } else {
        paramInt1 = View.MeasureSpec.makeMeasureSpec(0, 0);
      } 
    } else {
      if (this.mMeasureVerticalWithPaddingMargin) {
        paramInt1 = Math.max(0, paramInt2 - this.mPaddingTop - this.mPaddingBottom - paramLayoutParams.topMargin - paramLayoutParams.bottomMargin);
      } else {
        paramInt1 = Math.max(0, paramInt2);
      } 
      if (paramLayoutParams.height == -1) {
        paramInt2 = 1073741824;
      } else {
        paramInt2 = Integer.MIN_VALUE;
      } 
      paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, paramInt2);
    } 
    paramView.measure(i, paramInt1);
  }
  
  private int getChildMeasureSpec(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    boolean bool3, bool1 = false;
    boolean bool2 = false;
    if (paramInt8 < 0) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if (bool3 && !this.mAllowBrokenMeasureSpecs) {
      if (paramInt1 != Integer.MIN_VALUE && paramInt2 != Integer.MIN_VALUE) {
        paramInt2 = Math.max(0, paramInt2 - paramInt1);
        paramInt1 = 1073741824;
      } else if (paramInt3 >= 0) {
        paramInt2 = paramInt3;
        paramInt1 = 1073741824;
      } else {
        paramInt2 = 0;
        paramInt1 = 0;
      } 
      return View.MeasureSpec.makeMeasureSpec(paramInt2, paramInt1);
    } 
    int i = paramInt1;
    int j = paramInt2;
    int k = i;
    if (i == Integer.MIN_VALUE)
      k = paramInt6 + paramInt4; 
    paramInt4 = j;
    if (j == Integer.MIN_VALUE)
      paramInt4 = paramInt8 - paramInt7 - paramInt5; 
    paramInt5 = paramInt4 - k;
    paramInt4 = 1073741824;
    if (paramInt1 != Integer.MIN_VALUE && paramInt2 != Integer.MIN_VALUE) {
      paramInt1 = paramInt4;
      if (bool3)
        paramInt1 = 0; 
      paramInt3 = Math.max(0, paramInt5);
      paramInt2 = paramInt1;
      paramInt1 = paramInt3;
    } else if (paramInt3 >= 0) {
      paramInt2 = 1073741824;
      if (paramInt5 >= 0) {
        paramInt1 = Math.min(paramInt5, paramInt3);
      } else {
        paramInt1 = paramInt3;
      } 
    } else if (paramInt3 == -1) {
      paramInt1 = paramInt4;
      if (bool3)
        paramInt1 = 0; 
      paramInt3 = Math.max(0, paramInt5);
      paramInt2 = paramInt1;
      paramInt1 = paramInt3;
    } else {
      paramInt2 = bool1;
      paramInt1 = bool2;
      if (paramInt3 == -2)
        if (paramInt5 >= 0) {
          paramInt2 = Integer.MIN_VALUE;
          paramInt1 = paramInt5;
        } else {
          paramInt2 = 0;
          paramInt1 = 0;
        }  
    } 
    return View.MeasureSpec.makeMeasureSpec(paramInt1, paramInt2);
  }
  
  private boolean positionChildHorizontal(View paramView, LayoutParams paramLayoutParams, int paramInt, boolean paramBoolean) {
    int i = getLayoutDirection();
    int[] arrayOfInt = paramLayoutParams.getRules(i);
    i = paramLayoutParams.mLeft;
    boolean bool = true;
    if (i == Integer.MIN_VALUE && paramLayoutParams.mRight != Integer.MIN_VALUE) {
      LayoutParams.access$102(paramLayoutParams, paramLayoutParams.mRight - paramView.getMeasuredWidth());
    } else if (paramLayoutParams.mLeft != Integer.MIN_VALUE && paramLayoutParams.mRight == Integer.MIN_VALUE) {
      LayoutParams.access$202(paramLayoutParams, paramLayoutParams.mLeft + paramView.getMeasuredWidth());
    } else if (paramLayoutParams.mLeft == Integer.MIN_VALUE && paramLayoutParams.mRight == Integer.MIN_VALUE) {
      if (arrayOfInt[13] != 0 || arrayOfInt[14] != 0) {
        if (!paramBoolean) {
          centerHorizontal(paramView, paramLayoutParams, paramInt);
        } else {
          positionAtEdge(paramView, paramLayoutParams, paramInt);
        } 
        return true;
      } 
      positionAtEdge(paramView, paramLayoutParams, paramInt);
    } 
    if (arrayOfInt[21] != 0) {
      paramBoolean = bool;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  private void positionAtEdge(View paramView, LayoutParams paramLayoutParams, int paramInt) {
    if (isLayoutRtl()) {
      LayoutParams.access$202(paramLayoutParams, paramInt - this.mPaddingRight - paramLayoutParams.rightMargin);
      LayoutParams.access$102(paramLayoutParams, paramLayoutParams.mRight - paramView.getMeasuredWidth());
    } else {
      LayoutParams.access$102(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin);
      LayoutParams.access$202(paramLayoutParams, paramLayoutParams.mLeft + paramView.getMeasuredWidth());
    } 
  }
  
  private boolean positionChildVertical(View paramView, LayoutParams paramLayoutParams, int paramInt, boolean paramBoolean) {
    int[] arrayOfInt = paramLayoutParams.getRules();
    int i = paramLayoutParams.mTop;
    boolean bool = true;
    if (i == Integer.MIN_VALUE && paramLayoutParams.mBottom != Integer.MIN_VALUE) {
      LayoutParams.access$402(paramLayoutParams, paramLayoutParams.mBottom - paramView.getMeasuredHeight());
    } else if (paramLayoutParams.mTop != Integer.MIN_VALUE && paramLayoutParams.mBottom == Integer.MIN_VALUE) {
      LayoutParams.access$302(paramLayoutParams, paramLayoutParams.mTop + paramView.getMeasuredHeight());
    } else if (paramLayoutParams.mTop == Integer.MIN_VALUE && paramLayoutParams.mBottom == Integer.MIN_VALUE) {
      if (arrayOfInt[13] != 0 || arrayOfInt[15] != 0) {
        if (!paramBoolean) {
          centerVertical(paramView, paramLayoutParams, paramInt);
        } else {
          LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
          LayoutParams.access$302(paramLayoutParams, paramLayoutParams.mTop + paramView.getMeasuredHeight());
        } 
        return true;
      } 
      LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
      LayoutParams.access$302(paramLayoutParams, paramLayoutParams.mTop + paramView.getMeasuredHeight());
    } 
    if (arrayOfInt[12] != 0) {
      paramBoolean = bool;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  private void applyHorizontalSizeRules(LayoutParams paramLayoutParams, int paramInt, int[] paramArrayOfint) {
    LayoutParams.access$102(paramLayoutParams, -2147483648);
    LayoutParams.access$202(paramLayoutParams, -2147483648);
    LayoutParams layoutParams = getRelatedViewParams(paramArrayOfint, 0);
    if (layoutParams != null) {
      LayoutParams.access$202(paramLayoutParams, layoutParams.mLeft - layoutParams.leftMargin + paramLayoutParams.rightMargin);
    } else if (paramLayoutParams.alignWithParent && paramArrayOfint[0] != 0 && 
      paramInt >= 0) {
      LayoutParams.access$202(paramLayoutParams, paramInt - this.mPaddingRight - paramLayoutParams.rightMargin);
    } 
    layoutParams = getRelatedViewParams(paramArrayOfint, 1);
    if (layoutParams != null) {
      LayoutParams.access$102(paramLayoutParams, layoutParams.mRight + layoutParams.rightMargin + paramLayoutParams.leftMargin);
    } else if (paramLayoutParams.alignWithParent && paramArrayOfint[1] != 0) {
      LayoutParams.access$102(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin);
    } 
    layoutParams = getRelatedViewParams(paramArrayOfint, 5);
    if (layoutParams != null) {
      LayoutParams.access$102(paramLayoutParams, layoutParams.mLeft + paramLayoutParams.leftMargin);
    } else if (paramLayoutParams.alignWithParent && paramArrayOfint[5] != 0) {
      LayoutParams.access$102(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin);
    } 
    layoutParams = getRelatedViewParams(paramArrayOfint, 7);
    if (layoutParams != null) {
      LayoutParams.access$202(paramLayoutParams, layoutParams.mRight - paramLayoutParams.rightMargin);
    } else if (paramLayoutParams.alignWithParent && paramArrayOfint[7] != 0 && 
      paramInt >= 0) {
      LayoutParams.access$202(paramLayoutParams, paramInt - this.mPaddingRight - paramLayoutParams.rightMargin);
    } 
    if (paramArrayOfint[9] != 0)
      LayoutParams.access$102(paramLayoutParams, this.mPaddingLeft + paramLayoutParams.leftMargin); 
    if (paramArrayOfint[11] != 0 && 
      paramInt >= 0)
      LayoutParams.access$202(paramLayoutParams, paramInt - this.mPaddingRight - paramLayoutParams.rightMargin); 
  }
  
  private void applyVerticalSizeRules(LayoutParams paramLayoutParams, int paramInt1, int paramInt2) {
    int[] arrayOfInt = paramLayoutParams.getRules();
    int i = getRelatedViewBaselineOffset(arrayOfInt);
    if (i != -1) {
      paramInt1 = i;
      if (paramInt2 != -1)
        paramInt1 = i - paramInt2; 
      LayoutParams.access$402(paramLayoutParams, paramInt1);
      LayoutParams.access$302(paramLayoutParams, -2147483648);
      return;
    } 
    LayoutParams.access$402(paramLayoutParams, -2147483648);
    LayoutParams.access$302(paramLayoutParams, -2147483648);
    LayoutParams layoutParams = getRelatedViewParams(arrayOfInt, 2);
    if (layoutParams != null) {
      LayoutParams.access$302(paramLayoutParams, layoutParams.mTop - layoutParams.topMargin + paramLayoutParams.bottomMargin);
    } else if (paramLayoutParams.alignWithParent && arrayOfInt[2] != 0 && 
      paramInt1 >= 0) {
      LayoutParams.access$302(paramLayoutParams, paramInt1 - this.mPaddingBottom - paramLayoutParams.bottomMargin);
    } 
    layoutParams = getRelatedViewParams(arrayOfInt, 3);
    if (layoutParams != null) {
      LayoutParams.access$402(paramLayoutParams, layoutParams.mBottom + layoutParams.bottomMargin + paramLayoutParams.topMargin);
    } else if (paramLayoutParams.alignWithParent && arrayOfInt[3] != 0) {
      LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
    } 
    layoutParams = getRelatedViewParams(arrayOfInt, 6);
    if (layoutParams != null) {
      LayoutParams.access$402(paramLayoutParams, layoutParams.mTop + paramLayoutParams.topMargin);
    } else if (paramLayoutParams.alignWithParent && arrayOfInt[6] != 0) {
      LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin);
    } 
    layoutParams = getRelatedViewParams(arrayOfInt, 8);
    if (layoutParams != null) {
      LayoutParams.access$302(paramLayoutParams, layoutParams.mBottom - paramLayoutParams.bottomMargin);
    } else if (paramLayoutParams.alignWithParent && arrayOfInt[8] != 0 && 
      paramInt1 >= 0) {
      LayoutParams.access$302(paramLayoutParams, paramInt1 - this.mPaddingBottom - paramLayoutParams.bottomMargin);
    } 
    if (arrayOfInt[10] != 0)
      LayoutParams.access$402(paramLayoutParams, this.mPaddingTop + paramLayoutParams.topMargin); 
    if (arrayOfInt[12] != 0 && 
      paramInt1 >= 0)
      LayoutParams.access$302(paramLayoutParams, paramInt1 - this.mPaddingBottom - paramLayoutParams.bottomMargin); 
  }
  
  private View getRelatedView(int[] paramArrayOfint, int paramInt) {
    int i = paramArrayOfint[paramInt];
    if (i != 0) {
      DependencyGraph.Node node = this.mGraph.mKeyNodes.get(i);
      if (node == null)
        return null; 
      View view = node.view;
      while (view.getVisibility() == 8) {
        int[] arrayOfInt = ((LayoutParams)view.getLayoutParams()).getRules(view.getLayoutDirection());
        DependencyGraph.Node node1 = this.mGraph.mKeyNodes.get(arrayOfInt[paramInt]);
        if (node1 == null || view == node1.view)
          return null; 
        view = node1.view;
      } 
      return view;
    } 
    return null;
  }
  
  private LayoutParams getRelatedViewParams(int[] paramArrayOfint, int paramInt) {
    View view = getRelatedView(paramArrayOfint, paramInt);
    if (view != null) {
      ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
      if (layoutParams instanceof LayoutParams)
        return (LayoutParams)view.getLayoutParams(); 
    } 
    return null;
  }
  
  private int getRelatedViewBaselineOffset(int[] paramArrayOfint) {
    View view = getRelatedView(paramArrayOfint, 4);
    if (view != null) {
      int i = view.getBaseline();
      if (i != -1) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof LayoutParams) {
          layoutParams = view.getLayoutParams();
          return ((LayoutParams)layoutParams).mTop + i;
        } 
      } 
    } 
    return -1;
  }
  
  private static void centerHorizontal(View paramView, LayoutParams paramLayoutParams, int paramInt) {
    int i = paramView.getMeasuredWidth();
    paramInt = (paramInt - i) / 2;
    LayoutParams.access$102(paramLayoutParams, paramInt);
    LayoutParams.access$202(paramLayoutParams, paramInt + i);
  }
  
  private static void centerVertical(View paramView, LayoutParams paramLayoutParams, int paramInt) {
    int i = paramView.getMeasuredHeight();
    paramInt = (paramInt - i) / 2;
    LayoutParams.access$402(paramLayoutParams, paramInt);
    LayoutParams.access$302(paramLayoutParams, paramInt + i);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt2 = getChildCount();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      View view = getChildAt(paramInt1);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        view.layout(layoutParams.mLeft, layoutParams.mTop, layoutParams.mRight, layoutParams.mBottom);
      } 
    } 
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-2, -2);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    if (sPreserveMarginParamsInLayoutParamConversion) {
      if (paramLayoutParams instanceof LayoutParams)
        return new LayoutParams((LayoutParams)paramLayoutParams); 
      if (paramLayoutParams instanceof ViewGroup.MarginLayoutParams)
        return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams); 
    } 
    return new LayoutParams(paramLayoutParams);
  }
  
  public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    if (this.mTopToBottomLeftToRightSet == null)
      this.mTopToBottomLeftToRightSet = new TreeSet<>(new TopToBottomLeftToRightComparator()); 
    byte b;
    int i;
    for (b = 0, i = getChildCount(); b < i; b++)
      this.mTopToBottomLeftToRightSet.add(getChildAt(b)); 
    for (View view : this.mTopToBottomLeftToRightSet) {
      if (view.getVisibility() == 0 && 
        view.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent)) {
        this.mTopToBottomLeftToRightSet.clear();
        return true;
      } 
    } 
    this.mTopToBottomLeftToRightSet.clear();
    return false;
  }
  
  public CharSequence getAccessibilityClassName() {
    return RelativeLayout.class.getName();
  }
  
  class TopToBottomLeftToRightComparator implements Comparator<View> {
    final RelativeLayout this$0;
    
    private TopToBottomLeftToRightComparator() {}
    
    public int compare(View param1View1, View param1View2) {
      int i = param1View1.getTop() - param1View2.getTop();
      if (i != 0)
        return i; 
      i = param1View1.getLeft() - param1View2.getLeft();
      if (i != 0)
        return i; 
      i = param1View1.getHeight() - param1View2.getHeight();
      if (i != 0)
        return i; 
      i = param1View1.getWidth() - param1View2.getWidth();
      if (i != 0)
        return i; 
      return 0;
    }
  }
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    @ExportedProperty(category = "layout")
    public boolean alignWithParent;
    
    private int mBottom;
    
    private int[] mInitialRules;
    
    private boolean mIsRtlCompatibilityMode;
    
    private int mLeft;
    
    private boolean mNeedsLayoutResolution;
    
    private int mRight;
    
    @ExportedProperty(category = "layout", indexMapping = {@IntToString(from = 2, to = "above"), @IntToString(from = 4, to = "alignBaseline"), @IntToString(from = 8, to = "alignBottom"), @IntToString(from = 5, to = "alignLeft"), @IntToString(from = 12, to = "alignParentBottom"), @IntToString(from = 9, to = "alignParentLeft"), @IntToString(from = 11, to = "alignParentRight"), @IntToString(from = 10, to = "alignParentTop"), @IntToString(from = 7, to = "alignRight"), @IntToString(from = 6, to = "alignTop"), @IntToString(from = 3, to = "below"), @IntToString(from = 14, to = "centerHorizontal"), @IntToString(from = 13, to = "center"), @IntToString(from = 15, to = "centerVertical"), @IntToString(from = 0, to = "leftOf"), @IntToString(from = 1, to = "rightOf"), @IntToString(from = 18, to = "alignStart"), @IntToString(from = 19, to = "alignEnd"), @IntToString(from = 20, to = "alignParentStart"), @IntToString(from = 21, to = "alignParentEnd"), @IntToString(from = 16, to = "startOf"), @IntToString(from = 17, to = "endOf")}, mapping = {@IntToString(from = -1, to = "true"), @IntToString(from = 0, to = "false/NO_ID")}, resolveId = true)
    private int[] mRules;
    
    private boolean mRulesChanged;
    
    private int mTop;
    
    public LayoutParams(RelativeLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      boolean bool;
      this.mRules = new int[22];
      this.mInitialRules = new int[22];
      this.mRulesChanged = false;
      this.mIsRtlCompatibilityMode = false;
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, R.styleable.RelativeLayout_Layout);
      int i = (this$0.getApplicationInfo()).targetSdkVersion;
      if (i < 17 || 
        !this$0.getApplicationInfo().hasRtlSupport()) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mIsRtlCompatibilityMode = bool;
      int[] arrayOfInt2 = this.mRules;
      int[] arrayOfInt1 = this.mInitialRules;
      int j = typedArray.getIndexCount();
      for (byte b = 0; b < j; b++) {
        int k = typedArray.getIndex(b);
        i = -1;
        switch (k) {
          case 22:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[21] = i;
            break;
          case 21:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[20] = i;
            break;
          case 20:
            arrayOfInt2[19] = typedArray.getResourceId(k, 0);
            break;
          case 19:
            arrayOfInt2[18] = typedArray.getResourceId(k, 0);
            break;
          case 18:
            arrayOfInt2[17] = typedArray.getResourceId(k, 0);
            break;
          case 17:
            arrayOfInt2[16] = typedArray.getResourceId(k, 0);
            break;
          case 16:
            this.alignWithParent = typedArray.getBoolean(k, false);
            break;
          case 15:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[15] = i;
            break;
          case 14:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[14] = i;
            break;
          case 13:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[13] = i;
            break;
          case 12:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[12] = i;
            break;
          case 11:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[11] = i;
            break;
          case 10:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[10] = i;
            break;
          case 9:
            if (!typedArray.getBoolean(k, false))
              i = 0; 
            arrayOfInt2[9] = i;
            break;
          case 8:
            arrayOfInt2[8] = typedArray.getResourceId(k, 0);
            break;
          case 7:
            arrayOfInt2[7] = typedArray.getResourceId(k, 0);
            break;
          case 6:
            arrayOfInt2[6] = typedArray.getResourceId(k, 0);
            break;
          case 5:
            arrayOfInt2[5] = typedArray.getResourceId(k, 0);
            break;
          case 4:
            arrayOfInt2[4] = typedArray.getResourceId(k, 0);
            break;
          case 3:
            arrayOfInt2[3] = typedArray.getResourceId(k, 0);
            break;
          case 2:
            arrayOfInt2[2] = typedArray.getResourceId(k, 0);
            break;
          case 1:
            arrayOfInt2[1] = typedArray.getResourceId(k, 0);
            break;
          case 0:
            arrayOfInt2[0] = typedArray.getResourceId(k, 0);
            break;
        } 
      } 
      this.mRulesChanged = true;
      System.arraycopy(arrayOfInt2, 0, arrayOfInt1, 0, 22);
      typedArray.recycle();
    }
    
    public LayoutParams(RelativeLayout this$0, int param1Int1) {
      super(this$0, param1Int1);
      this.mRules = new int[22];
      this.mInitialRules = new int[22];
      this.mRulesChanged = false;
      this.mIsRtlCompatibilityMode = false;
    }
    
    public LayoutParams(RelativeLayout this$0) {
      super((ViewGroup.LayoutParams)this$0);
      this.mRules = new int[22];
      this.mInitialRules = new int[22];
      this.mRulesChanged = false;
      this.mIsRtlCompatibilityMode = false;
    }
    
    public LayoutParams(RelativeLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
      this.mRules = new int[22];
      this.mInitialRules = new int[22];
      this.mRulesChanged = false;
      this.mIsRtlCompatibilityMode = false;
    }
    
    public LayoutParams(RelativeLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
      int[] arrayOfInt = new int[22];
      this.mInitialRules = new int[22];
      this.mRulesChanged = false;
      this.mIsRtlCompatibilityMode = false;
      this.mIsRtlCompatibilityMode = ((LayoutParams)this$0).mIsRtlCompatibilityMode;
      this.mRulesChanged = ((LayoutParams)this$0).mRulesChanged;
      this.alignWithParent = ((LayoutParams)this$0).alignWithParent;
      System.arraycopy(((LayoutParams)this$0).mRules, 0, arrayOfInt, 0, 22);
      System.arraycopy(((LayoutParams)this$0).mInitialRules, 0, this.mInitialRules, 0, 22);
    }
    
    public String debug(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append("ViewGroup.LayoutParams={ width=");
      stringBuilder.append(sizeToString(this.width));
      stringBuilder.append(", height=");
      int i = this.height;
      stringBuilder.append(sizeToString(i));
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    public void addRule(int param1Int) {
      addRule(param1Int, -1);
    }
    
    public void addRule(int param1Int1, int param1Int2) {
      if (!this.mNeedsLayoutResolution && isRelativeRule(param1Int1) && this.mInitialRules[param1Int1] != 0 && param1Int2 == 0)
        this.mNeedsLayoutResolution = true; 
      this.mRules[param1Int1] = param1Int2;
      this.mInitialRules[param1Int1] = param1Int2;
      this.mRulesChanged = true;
    }
    
    public void removeRule(int param1Int) {
      addRule(param1Int, 0);
    }
    
    public int getRule(int param1Int) {
      return this.mRules[param1Int];
    }
    
    private boolean hasRelativeRules() {
      int[] arrayOfInt = this.mInitialRules;
      return (arrayOfInt[16] != 0 || arrayOfInt[17] != 0 || arrayOfInt[18] != 0 || arrayOfInt[19] != 0 || arrayOfInt[20] != 0 || arrayOfInt[21] != 0);
    }
    
    private boolean isRelativeRule(int param1Int) {
      return (param1Int == 16 || param1Int == 17 || param1Int == 18 || param1Int == 19 || param1Int == 20 || param1Int == 21);
    }
    
    private void resolveRules(int param1Int) {
      boolean bool = true;
      if (param1Int == 1) {
        param1Int = 1;
      } else {
        param1Int = 0;
      } 
      System.arraycopy(this.mInitialRules, 0, this.mRules, 0, 22);
      boolean bool1 = this.mIsRtlCompatibilityMode;
      byte b = 11;
      if (bool1) {
        int[] arrayOfInt = this.mRules;
        if (arrayOfInt[18] != 0) {
          if (arrayOfInt[5] == 0)
            arrayOfInt[5] = arrayOfInt[18]; 
          this.mRules[18] = 0;
        } 
        arrayOfInt = this.mRules;
        if (arrayOfInt[19] != 0) {
          if (arrayOfInt[7] == 0)
            arrayOfInt[7] = arrayOfInt[19]; 
          this.mRules[19] = 0;
        } 
        arrayOfInt = this.mRules;
        if (arrayOfInt[16] != 0) {
          if (arrayOfInt[0] == 0)
            arrayOfInt[0] = arrayOfInt[16]; 
          this.mRules[16] = 0;
        } 
        arrayOfInt = this.mRules;
        if (arrayOfInt[17] != 0) {
          if (arrayOfInt[1] == 0)
            arrayOfInt[1] = arrayOfInt[17]; 
          this.mRules[17] = 0;
        } 
        arrayOfInt = this.mRules;
        if (arrayOfInt[20] != 0) {
          if (arrayOfInt[9] == 0)
            arrayOfInt[9] = arrayOfInt[20]; 
          this.mRules[20] = 0;
        } 
        arrayOfInt = this.mRules;
        if (arrayOfInt[21] != 0) {
          if (arrayOfInt[11] == 0)
            arrayOfInt[11] = arrayOfInt[21]; 
          this.mRules[21] = 0;
        } 
      } else {
        int[] arrayOfInt1 = this.mRules;
        if (arrayOfInt1[18] != 0 || arrayOfInt1[19] != 0) {
          arrayOfInt1 = this.mRules;
          if (arrayOfInt1[5] != 0 || arrayOfInt1[7] != 0) {
            arrayOfInt1 = this.mRules;
            arrayOfInt1[5] = 0;
            arrayOfInt1[7] = 0;
          } 
        } 
        arrayOfInt1 = this.mRules;
        if (arrayOfInt1[18] != 0) {
          byte b1;
          if (param1Int != 0) {
            b1 = 7;
          } else {
            b1 = 5;
          } 
          int[] arrayOfInt = this.mRules;
          arrayOfInt1[b1] = arrayOfInt[18];
          arrayOfInt[18] = 0;
        } 
        int[] arrayOfInt2 = this.mRules;
        if (arrayOfInt2[19] != 0) {
          byte b1;
          if (param1Int != 0) {
            b1 = 5;
          } else {
            b1 = 7;
          } 
          arrayOfInt1 = this.mRules;
          arrayOfInt2[b1] = arrayOfInt1[19];
          arrayOfInt1[19] = 0;
        } 
        arrayOfInt1 = this.mRules;
        if (arrayOfInt1[16] != 0 || arrayOfInt1[17] != 0) {
          arrayOfInt1 = this.mRules;
          if (arrayOfInt1[0] != 0 || arrayOfInt1[1] != 0) {
            arrayOfInt1 = this.mRules;
            arrayOfInt1[0] = 0;
            arrayOfInt1[1] = 0;
          } 
        } 
        arrayOfInt2 = this.mRules;
        if (arrayOfInt2[16] != 0) {
          boolean bool2;
          if (param1Int != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          arrayOfInt1 = this.mRules;
          arrayOfInt2[bool2] = arrayOfInt1[16];
          arrayOfInt1[16] = 0;
        } 
        arrayOfInt1 = this.mRules;
        if (arrayOfInt1[17] != 0) {
          boolean bool2 = bool;
          if (param1Int != 0)
            bool2 = false; 
          arrayOfInt2 = this.mRules;
          arrayOfInt1[bool2] = arrayOfInt2[17];
          arrayOfInt2[17] = 0;
        } 
        arrayOfInt1 = this.mRules;
        if (arrayOfInt1[20] != 0 || arrayOfInt1[21] != 0) {
          arrayOfInt1 = this.mRules;
          if (arrayOfInt1[9] != 0 || arrayOfInt1[11] != 0) {
            arrayOfInt1 = this.mRules;
            arrayOfInt1[9] = 0;
            arrayOfInt1[11] = 0;
          } 
        } 
        arrayOfInt1 = this.mRules;
        if (arrayOfInt1[20] != 0) {
          byte b1;
          if (param1Int != 0) {
            b1 = 11;
          } else {
            b1 = 9;
          } 
          arrayOfInt2 = this.mRules;
          arrayOfInt1[b1] = arrayOfInt2[20];
          arrayOfInt2[20] = 0;
        } 
        arrayOfInt1 = this.mRules;
        if (arrayOfInt1[21] != 0) {
          byte b1 = b;
          if (param1Int != 0)
            b1 = 9; 
          arrayOfInt2 = this.mRules;
          arrayOfInt1[b1] = arrayOfInt2[21];
          arrayOfInt2[21] = 0;
        } 
      } 
      this.mRulesChanged = false;
      this.mNeedsLayoutResolution = false;
    }
    
    public int[] getRules(int param1Int) {
      resolveLayoutDirection(param1Int);
      return this.mRules;
    }
    
    public int[] getRules() {
      return this.mRules;
    }
    
    public void resolveLayoutDirection(int param1Int) {
      if (shouldResolveLayoutDirection(param1Int))
        resolveRules(param1Int); 
      super.resolveLayoutDirection(param1Int);
    }
    
    private boolean shouldResolveLayoutDirection(int param1Int) {
      return ((this.mNeedsLayoutResolution || hasRelativeRules()) && (this.mRulesChanged || 
        param1Int != getLayoutDirection()));
    }
    
    protected void encodeProperties(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      super.encodeProperties(param1ViewHierarchyEncoder);
      param1ViewHierarchyEncoder.addProperty("layout:alignWithParent", this.alignWithParent);
    }
    
    class InspectionCompanion implements android.view.inspector.InspectionCompanion<LayoutParams> {
      private int mAboveId;
      
      private int mAlignBaselineId;
      
      private int mAlignBottomId;
      
      private int mAlignEndId;
      
      private int mAlignLeftId;
      
      private int mAlignParentBottomId;
      
      private int mAlignParentEndId;
      
      private int mAlignParentLeftId;
      
      private int mAlignParentRightId;
      
      private int mAlignParentStartId;
      
      private int mAlignParentTopId;
      
      private int mAlignRightId;
      
      private int mAlignStartId;
      
      private int mAlignTopId;
      
      private int mAlignWithParentIfMissingId;
      
      private int mBelowId;
      
      private int mCenterHorizontalId;
      
      private int mCenterInParentId;
      
      private int mCenterVerticalId;
      
      private boolean mPropertiesMapped;
      
      private int mToEndOfId;
      
      private int mToLeftOfId;
      
      private int mToRightOfId;
      
      private int mToStartOfId;
      
      public void mapProperties(PropertyMapper param2PropertyMapper) {
        this.mPropertiesMapped = true;
        this.mAboveId = param2PropertyMapper.mapResourceId("layout_above", 16843140);
        this.mAlignBaselineId = param2PropertyMapper.mapResourceId("layout_alignBaseline", 16843142);
        this.mAlignBottomId = param2PropertyMapper.mapResourceId("layout_alignBottom", 16843146);
        this.mAlignEndId = param2PropertyMapper.mapResourceId("layout_alignEnd", 16843706);
        this.mAlignLeftId = param2PropertyMapper.mapResourceId("layout_alignLeft", 16843143);
        this.mAlignParentBottomId = param2PropertyMapper.mapBoolean("layout_alignParentBottom", 16843150);
        this.mAlignParentEndId = param2PropertyMapper.mapBoolean("layout_alignParentEnd", 16843708);
        this.mAlignParentLeftId = param2PropertyMapper.mapBoolean("layout_alignParentLeft", 16843147);
        this.mAlignParentRightId = param2PropertyMapper.mapBoolean("layout_alignParentRight", 16843149);
        this.mAlignParentStartId = param2PropertyMapper.mapBoolean("layout_alignParentStart", 16843707);
        this.mAlignParentTopId = param2PropertyMapper.mapBoolean("layout_alignParentTop", 16843148);
        this.mAlignRightId = param2PropertyMapper.mapResourceId("layout_alignRight", 16843145);
        this.mAlignStartId = param2PropertyMapper.mapResourceId("layout_alignStart", 16843705);
        this.mAlignTopId = param2PropertyMapper.mapResourceId("layout_alignTop", 16843144);
        this.mAlignWithParentIfMissingId = param2PropertyMapper.mapBoolean("layout_alignWithParentIfMissing", 16843154);
        this.mBelowId = param2PropertyMapper.mapResourceId("layout_below", 16843141);
        this.mCenterHorizontalId = param2PropertyMapper.mapBoolean("layout_centerHorizontal", 16843152);
        this.mCenterInParentId = param2PropertyMapper.mapBoolean("layout_centerInParent", 16843151);
        this.mCenterVerticalId = param2PropertyMapper.mapBoolean("layout_centerVertical", 16843153);
        this.mToEndOfId = param2PropertyMapper.mapResourceId("layout_toEndOf", 16843704);
        this.mToLeftOfId = param2PropertyMapper.mapResourceId("layout_toLeftOf", 16843138);
        this.mToRightOfId = param2PropertyMapper.mapResourceId("layout_toRightOf", 16843139);
        this.mToStartOfId = param2PropertyMapper.mapResourceId("layout_toStartOf", 16843703);
      }
      
      public void readProperties(RelativeLayout.LayoutParams param2LayoutParams, PropertyReader param2PropertyReader) {
        if (this.mPropertiesMapped) {
          boolean bool;
          int[] arrayOfInt = param2LayoutParams.getRules();
          param2PropertyReader.readResourceId(this.mAboveId, arrayOfInt[2]);
          param2PropertyReader.readResourceId(this.mAlignBaselineId, arrayOfInt[4]);
          param2PropertyReader.readResourceId(this.mAlignBottomId, arrayOfInt[8]);
          param2PropertyReader.readResourceId(this.mAlignEndId, arrayOfInt[19]);
          param2PropertyReader.readResourceId(this.mAlignLeftId, arrayOfInt[5]);
          int i = this.mAlignParentBottomId;
          if (arrayOfInt[12] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          i = this.mAlignParentEndId;
          if (arrayOfInt[21] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          i = this.mAlignParentLeftId;
          if (arrayOfInt[9] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          i = this.mAlignParentRightId;
          if (arrayOfInt[11] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          i = this.mAlignParentStartId;
          if (arrayOfInt[20] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          i = this.mAlignParentTopId;
          if (arrayOfInt[10] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          param2PropertyReader.readResourceId(this.mAlignRightId, arrayOfInt[7]);
          param2PropertyReader.readResourceId(this.mAlignStartId, arrayOfInt[18]);
          param2PropertyReader.readResourceId(this.mAlignTopId, arrayOfInt[6]);
          param2PropertyReader.readBoolean(this.mAlignWithParentIfMissingId, param2LayoutParams.alignWithParent);
          param2PropertyReader.readResourceId(this.mBelowId, arrayOfInt[3]);
          i = this.mCenterHorizontalId;
          if (arrayOfInt[14] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          i = this.mCenterInParentId;
          if (arrayOfInt[13] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          i = this.mCenterVerticalId;
          if (arrayOfInt[15] == -1) {
            bool = true;
          } else {
            bool = false;
          } 
          param2PropertyReader.readBoolean(i, bool);
          param2PropertyReader.readResourceId(this.mToEndOfId, arrayOfInt[17]);
          param2PropertyReader.readResourceId(this.mToLeftOfId, arrayOfInt[0]);
          param2PropertyReader.readResourceId(this.mToRightOfId, arrayOfInt[1]);
          param2PropertyReader.readResourceId(this.mToStartOfId, arrayOfInt[16]);
          return;
        } 
        throw new android.view.inspector.InspectionCompanion.UninitializedPropertyMapException();
      }
    }
  }
  
  class DependencyGraph {
    private ArrayList<Node> mNodes = new ArrayList<>();
    
    private SparseArray<Node> mKeyNodes = new SparseArray<>();
    
    private ArrayDeque<Node> mRoots = new ArrayDeque<>();
    
    void clear() {
      ArrayList<Node> arrayList = this.mNodes;
      int i = arrayList.size();
      for (byte b = 0; b < i; b++)
        ((Node)arrayList.get(b)).release(); 
      arrayList.clear();
      this.mKeyNodes.clear();
      this.mRoots.clear();
    }
    
    void add(View param1View) {
      int i = param1View.getId();
      Node node = Node.acquire(param1View);
      if (i != -1)
        this.mKeyNodes.put(i, node); 
      this.mNodes.add(node);
    }
    
    void getSortedViews(View[] param1ArrayOfView, int... param1VarArgs) {
      ArrayDeque<Node> arrayDeque = findRoots(param1VarArgs);
      byte b = 0;
      while (true) {
        Node node = arrayDeque.pollLast();
        if (node != null) {
          View view = node.view;
          int i = view.getId();
          param1ArrayOfView[b] = view;
          ArrayMap<Node, DependencyGraph> arrayMap = node.dependents;
          int j = arrayMap.size();
          for (byte b1 = 0; b1 < j; b1++) {
            Node node1 = arrayMap.keyAt(b1);
            SparseArray<Node> sparseArray = node1.dependencies;
            sparseArray.remove(i);
            if (sparseArray.size() == 0)
              arrayDeque.add(node1); 
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (b >= param1ArrayOfView.length)
        return; 
      throw new IllegalStateException("Circular dependencies cannot exist in RelativeLayout");
    }
    
    private ArrayDeque<Node> findRoots(int[] param1ArrayOfint) {
      SparseArray<Node> sparseArray = this.mKeyNodes;
      ArrayList<Node> arrayList = this.mNodes;
      int i = arrayList.size();
      byte b;
      for (b = 0; b < i; b++) {
        Node node = arrayList.get(b);
        node.dependents.clear();
        node.dependencies.clear();
      } 
      for (b = 0; b < i; b++) {
        Node node = arrayList.get(b);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)node.view.getLayoutParams();
        int[] arrayOfInt = layoutParams.mRules;
        int j = param1ArrayOfint.length;
        for (byte b1 = 0; b1 < j; b1++) {
          int k = arrayOfInt[param1ArrayOfint[b1]];
          if (k > 0 || ResourceId.isValid(k)) {
            Node node1 = sparseArray.get(k);
            if (node1 != null && node1 != node) {
              node1.dependents.put(node, this);
              node.dependencies.put(k, node1);
            } 
          } 
        } 
      } 
      ArrayDeque<Node> arrayDeque = this.mRoots;
      arrayDeque.clear();
      for (b = 0; b < i; b++) {
        Node node = arrayList.get(b);
        if (node.dependencies.size() == 0)
          arrayDeque.addLast(node); 
      } 
      return arrayDeque;
    }
    
    private DependencyGraph() {}
    
    class Node {
      View view;
      
      final ArrayMap<Node, RelativeLayout.DependencyGraph> dependents = new ArrayMap<>();
      
      final SparseArray<Node> dependencies = new SparseArray<>();
      
      private static final Pools.SynchronizedPool<Node> sPool = new Pools.SynchronizedPool<>(100);
      
      private static final int POOL_LIMIT = 100;
      
      static Node acquire(View param2View) {
        Node node1 = sPool.acquire();
        Node node2 = node1;
        if (node1 == null)
          node2 = new Node(); 
        node2.view = param2View;
        return node2;
      }
      
      void release() {
        this.view = null;
        this.dependents.clear();
        this.dependencies.clear();
        sPool.release(this);
      }
    }
  }
}
