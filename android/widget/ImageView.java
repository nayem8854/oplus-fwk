package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ImageDecoder;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewHierarchyEncoder;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.R;
import com.oplus.rp.bridge.IOplusRedPacketManager;
import java.io.IOException;

@RemoteView
public class ImageView extends View {
  private int mResource = 0;
  
  private boolean mHaveFrame = false;
  
  private boolean mAdjustViewBounds = false;
  
  private int mMaxWidth = Integer.MAX_VALUE;
  
  private int mMaxHeight = Integer.MAX_VALUE;
  
  private ColorFilter mColorFilter = null;
  
  private boolean mHasColorFilter = false;
  
  private boolean mHasXfermode = false;
  
  private int mAlpha = 255;
  
  private boolean mHasAlpha = false;
  
  private final int mViewAlphaScale = 256;
  
  private Drawable mDrawable = null;
  
  private BitmapDrawable mRecycleableBitmapDrawable = null;
  
  private ColorStateList mDrawableTintList = null;
  
  private BlendMode mDrawableBlendMode = null;
  
  private boolean mHasDrawableTint = false;
  
  private boolean mHasDrawableBlendMode = false;
  
  private int[] mState = null;
  
  private boolean mMergeState = false;
  
  private int mLevel = 0;
  
  private Matrix mDrawMatrix = null;
  
  private final RectF mTempSrc = new RectF();
  
  private final RectF mTempDst = new RectF();
  
  private int mBaseline = -1;
  
  private boolean mBaselineAlignBottom = false;
  
  private static final ScaleType[] sScaleTypeArray = new ScaleType[] { ScaleType.MATRIX, ScaleType.FIT_XY, ScaleType.FIT_START, ScaleType.FIT_CENTER, ScaleType.FIT_END, ScaleType.CENTER, ScaleType.CENTER_CROP, ScaleType.CENTER_INSIDE };
  
  private static final String LOG_TAG = "ImageView";
  
  private static boolean sCompatAdjustViewBounds;
  
  private static boolean sCompatDone;
  
  private static boolean sCompatDrawableVisibilityDispatch;
  
  private static boolean sCompatUseCorrectStreamDensity;
  
  private static final Matrix.ScaleToFit[] sS2FArray;
  
  private boolean mCropToPadding;
  
  private int mDrawableHeight;
  
  private int mDrawableWidth;
  
  private Matrix mMatrix;
  
  private ScaleType mScaleType;
  
  private Uri mUri;
  
  private Xfermode mXfermode;
  
  public ImageView(Context paramContext) {
    super(paramContext);
    initImageView();
  }
  
  public ImageView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    initImageView();
    if (getImportantForAutofill() == 0)
      setImportantForAutofill(2); 
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ImageView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ImageView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    Drawable drawable = typedArray.getDrawable(0);
    if (drawable != null)
      setImageDrawable(drawable); 
    this.mBaselineAlignBottom = typedArray.getBoolean(6, false);
    this.mBaseline = typedArray.getDimensionPixelSize(8, -1);
    setAdjustViewBounds(typedArray.getBoolean(2, false));
    setMaxWidth(typedArray.getDimensionPixelSize(3, 2147483647));
    setMaxHeight(typedArray.getDimensionPixelSize(4, 2147483647));
    paramInt1 = typedArray.getInt(1, -1);
    if (paramInt1 >= 0)
      setScaleType(sScaleTypeArray[paramInt1]); 
    if (typedArray.hasValue(5)) {
      this.mDrawableTintList = typedArray.getColorStateList(5);
      this.mHasDrawableTint = true;
      this.mDrawableBlendMode = BlendMode.SRC_ATOP;
      this.mHasDrawableBlendMode = true;
    } 
    if (typedArray.hasValue(9)) {
      this.mDrawableBlendMode = Drawable.parseBlendMode(typedArray.getInt(9, -1), this.mDrawableBlendMode);
      this.mHasDrawableBlendMode = true;
    } 
    applyImageTint();
    paramInt1 = typedArray.getInt(10, 255);
    if (paramInt1 != 255)
      setImageAlpha(paramInt1); 
    this.mCropToPadding = typedArray.getBoolean(7, false);
    typedArray.recycle();
  }
  
  private void initImageView() {
    this.mMatrix = new Matrix();
    this.mScaleType = ScaleType.FIT_CENTER;
    if (!sCompatDone) {
      int i = (this.mContext.getApplicationInfo()).targetSdkVersion;
      boolean bool1 = false;
      if (i <= 17) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      sCompatAdjustViewBounds = bool2;
      if (i > 23) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      sCompatUseCorrectStreamDensity = bool2;
      boolean bool2 = bool1;
      if (i < 24)
        bool2 = true; 
      sCompatDrawableVisibilityDispatch = bool2;
      sCompatDone = true;
    } 
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (this.mDrawable == paramDrawable || super.verifyDrawable(paramDrawable));
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mDrawable;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    if (paramDrawable == this.mDrawable) {
      if (paramDrawable != null) {
        int i = paramDrawable.getIntrinsicWidth();
        int j = paramDrawable.getIntrinsicHeight();
        if (i != this.mDrawableWidth || j != this.mDrawableHeight) {
          this.mDrawableWidth = i;
          this.mDrawableHeight = j;
          configureBounds();
        } 
      } 
      invalidate();
    } else {
      super.invalidateDrawable(paramDrawable);
    } 
  }
  
  public boolean hasOverlappingRendering() {
    boolean bool;
    if (getBackground() != null && getBackground().getCurrent() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onPopulateAccessibilityEventInternal(paramAccessibilityEvent);
    CharSequence charSequence = getContentDescription();
    if (!TextUtils.isEmpty(charSequence))
      paramAccessibilityEvent.getText().add(charSequence); 
  }
  
  public boolean getAdjustViewBounds() {
    return this.mAdjustViewBounds;
  }
  
  @RemotableViewMethod
  public void setAdjustViewBounds(boolean paramBoolean) {
    this.mAdjustViewBounds = paramBoolean;
    if (paramBoolean)
      setScaleType(ScaleType.FIT_CENTER); 
  }
  
  public int getMaxWidth() {
    return this.mMaxWidth;
  }
  
  @RemotableViewMethod
  public void setMaxWidth(int paramInt) {
    this.mMaxWidth = paramInt;
  }
  
  public int getMaxHeight() {
    return this.mMaxHeight;
  }
  
  @RemotableViewMethod
  public void setMaxHeight(int paramInt) {
    this.mMaxHeight = paramInt;
  }
  
  public Drawable getDrawable() {
    if (this.mDrawable == this.mRecycleableBitmapDrawable)
      this.mRecycleableBitmapDrawable = null; 
    return this.mDrawable;
  }
  
  class ImageDrawableCallback implements Runnable {
    private final Drawable drawable;
    
    private final int resource;
    
    final ImageView this$0;
    
    private final Uri uri;
    
    ImageDrawableCallback(Drawable param1Drawable, Uri param1Uri, int param1Int) {
      this.drawable = param1Drawable;
      this.uri = param1Uri;
      this.resource = param1Int;
    }
    
    public void run() {
      ImageView.this.setImageDrawable(this.drawable);
      ImageView.access$002(ImageView.this, this.uri);
      ImageView.access$102(ImageView.this, this.resource);
    }
  }
  
  @RemotableViewMethod(asyncImpl = "setImageResourceAsync")
  public void setImageResource(int paramInt) {
    int i = this.mDrawableWidth;
    int j = this.mDrawableHeight;
    updateDrawable((Drawable)null);
    this.mResource = paramInt;
    this.mUri = null;
    resolveUri();
    if (i != this.mDrawableWidth || j != this.mDrawableHeight)
      requestLayout(); 
    invalidate();
  }
  
  public Runnable setImageResourceAsync(int paramInt) {
    Drawable drawable1 = null;
    Drawable drawable2 = drawable1;
    int i = paramInt;
    if (paramInt != 0)
      try {
        drawable2 = getContext().getDrawable(paramInt);
        i = paramInt;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to find resource: ");
        stringBuilder.append(paramInt);
        Log.w("ImageView", stringBuilder.toString(), exception);
        i = 0;
        drawable2 = drawable1;
      }  
    return new ImageDrawableCallback(drawable2, null, i);
  }
  
  @RemotableViewMethod(asyncImpl = "setImageURIAsync")
  public void setImageURI(Uri paramUri) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mResource : I
    //   4: ifne -> 33
    //   7: aload_0
    //   8: getfield mUri : Landroid/net/Uri;
    //   11: astore_2
    //   12: aload_2
    //   13: aload_1
    //   14: if_acmpeq -> 88
    //   17: aload_1
    //   18: ifnull -> 33
    //   21: aload_2
    //   22: ifnull -> 33
    //   25: aload_1
    //   26: aload_2
    //   27: invokevirtual equals : (Ljava/lang/Object;)Z
    //   30: ifne -> 88
    //   33: aload_0
    //   34: aconst_null
    //   35: invokespecial updateDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   38: aload_0
    //   39: iconst_0
    //   40: putfield mResource : I
    //   43: aload_0
    //   44: aload_1
    //   45: putfield mUri : Landroid/net/Uri;
    //   48: aload_0
    //   49: getfield mDrawableWidth : I
    //   52: istore_3
    //   53: aload_0
    //   54: getfield mDrawableHeight : I
    //   57: istore #4
    //   59: aload_0
    //   60: invokespecial resolveUri : ()V
    //   63: iload_3
    //   64: aload_0
    //   65: getfield mDrawableWidth : I
    //   68: if_icmpne -> 80
    //   71: iload #4
    //   73: aload_0
    //   74: getfield mDrawableHeight : I
    //   77: if_icmpeq -> 84
    //   80: aload_0
    //   81: invokevirtual requestLayout : ()V
    //   84: aload_0
    //   85: invokevirtual invalidate : ()V
    //   88: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #554	-> 0
    //   #555	-> 33
    //   #556	-> 38
    //   #557	-> 43
    //   #559	-> 48
    //   #560	-> 53
    //   #562	-> 59
    //   #564	-> 63
    //   #565	-> 80
    //   #567	-> 84
    //   #569	-> 88
  }
  
  public Runnable setImageURIAsync(Uri paramUri) {
    int i = this.mResource;
    Drawable drawable = null;
    if (i == 0) {
      Uri uri = this.mUri;
      if (uri == paramUri || (paramUri != null && uri != null && paramUri.equals(uri)))
        return null; 
    } 
    if (paramUri != null)
      drawable = getDrawableFromUri(paramUri); 
    if (drawable == null)
      paramUri = null; 
    return new ImageDrawableCallback(drawable, paramUri, 0);
  }
  
  public void setImageDrawable(Drawable paramDrawable) {
    if (this.mDrawable != paramDrawable) {
      this.mResource = 0;
      this.mUri = null;
      int i = this.mDrawableWidth;
      int j = this.mDrawableHeight;
      updateDrawable(paramDrawable);
      ViewParent viewParent = getParent();
      if (viewParent instanceof View)
        ((View)viewParent).adjustLayerType(this.mDrawableWidth, this.mDrawableHeight); 
      if (i != this.mDrawableWidth || j != this.mDrawableHeight)
        requestLayout(); 
      invalidate();
    } 
  }
  
  @RemotableViewMethod(asyncImpl = "setImageIconAsync")
  public void setImageIcon(Icon paramIcon) {
    Drawable drawable;
    if (paramIcon == null) {
      paramIcon = null;
    } else {
      drawable = paramIcon.loadDrawable(this.mContext);
    } 
    setImageDrawable(drawable);
  }
  
  public Runnable setImageIconAsync(Icon paramIcon) {
    Drawable drawable;
    if (paramIcon == null) {
      paramIcon = null;
    } else {
      drawable = paramIcon.loadDrawable(this.mContext);
    } 
    return new ImageDrawableCallback(drawable, null, 0);
  }
  
  public void setImageTintList(ColorStateList paramColorStateList) {
    this.mDrawableTintList = paramColorStateList;
    this.mHasDrawableTint = true;
    applyImageTint();
  }
  
  public ColorStateList getImageTintList() {
    return this.mDrawableTintList;
  }
  
  public void setImageTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setImageTintBlendMode((BlendMode)paramMode);
  }
  
  public void setImageTintBlendMode(BlendMode paramBlendMode) {
    this.mDrawableBlendMode = paramBlendMode;
    this.mHasDrawableBlendMode = true;
    applyImageTint();
  }
  
  public PorterDuff.Mode getImageTintMode() {
    BlendMode blendMode = this.mDrawableBlendMode;
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getImageTintBlendMode() {
    return this.mDrawableBlendMode;
  }
  
  private void applyImageTint() {
    if (this.mDrawable != null && (this.mHasDrawableTint || this.mHasDrawableBlendMode)) {
      Drawable drawable = this.mDrawable.mutate();
      if (this.mHasDrawableTint)
        drawable.setTintList(this.mDrawableTintList); 
      if (this.mHasDrawableBlendMode)
        this.mDrawable.setTintBlendMode(this.mDrawableBlendMode); 
      if (this.mDrawable.isStateful())
        this.mDrawable.setState(getDrawableState()); 
    } 
  }
  
  @RemotableViewMethod
  public void setImageBitmap(Bitmap paramBitmap) {
    this.mDrawable = null;
    BitmapDrawable bitmapDrawable = this.mRecycleableBitmapDrawable;
    if (bitmapDrawable == null) {
      this.mRecycleableBitmapDrawable = new BitmapDrawable(this.mContext.getResources(), paramBitmap);
    } else {
      bitmapDrawable.setBitmap(paramBitmap);
    } 
    setImageDrawable((Drawable)this.mRecycleableBitmapDrawable);
    if (this instanceof ImageView && this.mDrawableWidth > 0 && this.mDrawableHeight > 0 && 
      getClass().getSimpleName().equals("MultiTouchImageView"))
      adjustLayerType(this.mDrawableWidth, this.mDrawableHeight); 
  }
  
  public void setImageState(int[] paramArrayOfint, boolean paramBoolean) {
    this.mState = paramArrayOfint;
    this.mMergeState = paramBoolean;
    if (this.mDrawable != null) {
      refreshDrawableState();
      resizeFromDrawable();
    } 
  }
  
  public void setSelected(boolean paramBoolean) {
    super.setSelected(paramBoolean);
    resizeFromDrawable();
  }
  
  @RemotableViewMethod
  public void setImageLevel(int paramInt) {
    this.mLevel = paramInt;
    Drawable drawable = this.mDrawable;
    if (drawable != null) {
      drawable.setLevel(paramInt);
      resizeFromDrawable();
    } 
  }
  
  class ScaleType extends Enum<ScaleType> {
    private static final ScaleType[] $VALUES;
    
    public static final ScaleType CENTER;
    
    public static final ScaleType CENTER_CROP;
    
    public static final ScaleType CENTER_INSIDE;
    
    public static final ScaleType FIT_CENTER;
    
    public static final ScaleType FIT_END;
    
    public static final ScaleType FIT_START;
    
    public static final ScaleType FIT_XY = new ScaleType("FIT_XY", 1, 1);
    
    public static ScaleType valueOf(String param1String) {
      return Enum.<ScaleType>valueOf(ScaleType.class, param1String);
    }
    
    public static ScaleType[] values() {
      return (ScaleType[])$VALUES.clone();
    }
    
    public static final ScaleType MATRIX = new ScaleType("MATRIX", 0, 0);
    
    final int nativeInt;
    
    static {
      FIT_START = new ScaleType("FIT_START", 2, 2);
      FIT_CENTER = new ScaleType("FIT_CENTER", 3, 3);
      FIT_END = new ScaleType("FIT_END", 4, 4);
      CENTER = new ScaleType("CENTER", 5, 5);
      CENTER_CROP = new ScaleType("CENTER_CROP", 6, 6);
      ScaleType scaleType = new ScaleType("CENTER_INSIDE", 7, 7);
      $VALUES = new ScaleType[] { MATRIX, FIT_XY, FIT_START, FIT_CENTER, FIT_END, CENTER, CENTER_CROP, scaleType };
    }
    
    private ScaleType(ImageView this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.nativeInt = param1Int2;
    }
  }
  
  public void setScaleType(ScaleType paramScaleType) {
    if (paramScaleType != null) {
      if (this.mScaleType != paramScaleType) {
        this.mScaleType = paramScaleType;
        requestLayout();
        invalidate();
      } 
      return;
    } 
    throw null;
  }
  
  public ScaleType getScaleType() {
    return this.mScaleType;
  }
  
  public Matrix getImageMatrix() {
    Matrix matrix = this.mDrawMatrix;
    if (matrix == null)
      return new Matrix(Matrix.IDENTITY_MATRIX); 
    return matrix;
  }
  
  public void setImageMatrix(Matrix paramMatrix) {
    Matrix matrix = paramMatrix;
    if (paramMatrix != null) {
      matrix = paramMatrix;
      if (paramMatrix.isIdentity())
        matrix = null; 
    } 
    if (matrix != null || this.mMatrix.isIdentity()) {
      if (matrix != null) {
        paramMatrix = this.mMatrix;
        if (!paramMatrix.equals(matrix)) {
          this.mMatrix.set(matrix);
          configureBounds();
          invalidate();
          return;
        } 
      } 
      return;
    } 
    this.mMatrix.set(matrix);
    configureBounds();
    invalidate();
  }
  
  public boolean getCropToPadding() {
    return this.mCropToPadding;
  }
  
  public void setCropToPadding(boolean paramBoolean) {
    if (this.mCropToPadding != paramBoolean) {
      this.mCropToPadding = paramBoolean;
      requestLayout();
      invalidate();
    } 
  }
  
  private void resolveUri() {
    if (this.mDrawable != null)
      return; 
    if (getResources() == null)
      return; 
    Drawable drawable = null;
    if (this.mResource != 0) {
      try {
        Drawable drawable1 = this.mContext.getDrawable(this.mResource);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to find resource: ");
        stringBuilder.append(this.mResource);
        Log.w("ImageView", stringBuilder.toString(), exception);
        this.mResource = 0;
      } 
    } else {
      Uri uri = this.mUri;
      if (uri != null) {
        Drawable drawable2 = getDrawableFromUri(uri);
        Drawable drawable1 = drawable2;
        if (drawable2 == null) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("resolveUri failed on bad bitmap uri: ");
          stringBuilder.append(this.mUri);
          Log.w("ImageView", stringBuilder.toString());
          this.mUri = null;
          drawable = drawable2;
        } 
      } else {
        return;
      } 
    } 
    updateDrawable(drawable);
  }
  
  private Drawable getDrawableFromUri(Uri paramUri) {
    String str = paramUri.getScheme();
    if ("android.resource".equals(str)) {
      try {
        Context context = this.mContext;
        ContentResolver.OpenResourceIdResult openResourceIdResult = context.getContentResolver().getResourceId(paramUri);
        return openResourceIdResult.r.getDrawable(openResourceIdResult.id, this.mContext.getTheme());
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to open content: ");
        stringBuilder.append(paramUri);
        Log.w("ImageView", stringBuilder.toString(), exception);
      } 
    } else {
      if ("content".equals(exception) || 
        "file".equals(exception)) {
        try {
          if (sCompatUseCorrectStreamDensity) {
            Resources resources = getResources();
          } else {
            exception = null;
          } 
          ImageDecoder.Source source = ImageDecoder.createSource(this.mContext.getContentResolver(), paramUri, (Resources)exception);
          return ImageDecoder.decodeDrawable(source, (ImageDecoder.OnHeaderDecodedListener)_$$Lambda$ImageView$GWf2_Z_LHjSbTbrF_I3WzfR0LeM.INSTANCE);
        } catch (IOException iOException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to open content: ");
          stringBuilder.append(paramUri);
          Log.w("ImageView", stringBuilder.toString(), iOException);
        } 
        return null;
      } 
      return Drawable.createFromPath(paramUri.toString());
    } 
    return null;
  }
  
  public int[] onCreateDrawableState(int paramInt) {
    int[] arrayOfInt1 = this.mState;
    if (arrayOfInt1 == null)
      return super.onCreateDrawableState(paramInt); 
    if (!this.mMergeState)
      return arrayOfInt1; 
    int i = arrayOfInt1.length;
    int[] arrayOfInt2 = super.onCreateDrawableState(i + paramInt);
    arrayOfInt1 = this.mState;
    return mergeDrawableStates(arrayOfInt2, arrayOfInt1);
  }
  
  private void updateDrawable(Drawable paramDrawable) {
    BitmapDrawable bitmapDrawable = this.mRecycleableBitmapDrawable;
    if (paramDrawable != bitmapDrawable && bitmapDrawable != null)
      bitmapDrawable.setBitmap(null); 
    boolean bool = false;
    Drawable drawable = this.mDrawable;
    if (drawable != null) {
      boolean bool1;
      if (drawable == paramDrawable) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      this.mDrawable.setCallback(null);
      unscheduleDrawable(this.mDrawable);
      bool = bool1;
      if (!sCompatDrawableVisibilityDispatch) {
        bool = bool1;
        if (!bool1) {
          bool = bool1;
          if (isAttachedToWindow()) {
            this.mDrawable.setVisible(false, false);
            bool = bool1;
          } 
        } 
      } 
    } 
    this.mDrawable = paramDrawable;
    if (paramDrawable != null) {
      paramDrawable.setCallback(this);
      paramDrawable.setLayoutDirection(getLayoutDirection());
      if (paramDrawable.isStateful())
        paramDrawable.setState(getDrawableState()); 
      if (!bool || sCompatDrawableVisibilityDispatch) {
        boolean bool1;
        if (sCompatDrawableVisibilityDispatch) {
          if (getVisibility() == 0) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
        } else if (isAttachedToWindow() && getWindowVisibility() == 0 && isShown()) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        paramDrawable.setVisible(bool1, true);
      } 
      paramDrawable.setLevel(this.mLevel);
      this.mDrawableWidth = paramDrawable.getIntrinsicWidth();
      this.mDrawableHeight = paramDrawable.getIntrinsicHeight();
      applyImageTint();
      applyColorFilter();
      applyAlpha();
      applyXfermode();
      configureBounds();
      paramDrawable = this.mDrawable;
      if (paramDrawable != null && paramDrawable instanceof BitmapDrawable) {
        Bitmap bitmap = ((BitmapDrawable)paramDrawable).getBitmap();
        if (bitmap != null) {
          IOplusRedPacketManager iOplusRedPacketManager = (IOplusRedPacketManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusRedPacketManager.DEFAULT, new Object[0]);
          if (iOplusRedPacketManager.verifyBeforeEnter())
            iOplusRedPacketManager.notify(bitmap, false); 
        } 
      } 
    } else {
      this.mDrawableHeight = -1;
      this.mDrawableWidth = -1;
    } 
  }
  
  private void resizeFromDrawable() {
    Drawable drawable = this.mDrawable;
    if (drawable != null) {
      int i = drawable.getIntrinsicWidth();
      int j = i;
      if (i < 0)
        j = this.mDrawableWidth; 
      int k = drawable.getIntrinsicHeight();
      i = k;
      if (k < 0)
        i = this.mDrawableHeight; 
      if (j != this.mDrawableWidth || i != this.mDrawableHeight) {
        this.mDrawableWidth = j;
        this.mDrawableHeight = i;
        requestLayout();
      } 
    } 
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    Drawable drawable = this.mDrawable;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
  }
  
  static {
    sS2FArray = new Matrix.ScaleToFit[] { Matrix.ScaleToFit.FILL, Matrix.ScaleToFit.START, Matrix.ScaleToFit.CENTER, Matrix.ScaleToFit.END };
  }
  
  private static Matrix.ScaleToFit scaleTypeToScaleToFit(ScaleType paramScaleType) {
    return sS2FArray[paramScaleType.nativeInt - 1];
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int n, i1;
    resolveUri();
    float f = 0.0F;
    int i = 0;
    int j = 0;
    int k = View.MeasureSpec.getMode(paramInt1);
    int m = View.MeasureSpec.getMode(paramInt2);
    if (this.mDrawable == null) {
      this.mDrawableWidth = -1;
      this.mDrawableHeight = -1;
      n = 0;
      i1 = 0;
    } else {
      i1 = this.mDrawableWidth;
      int i4 = this.mDrawableHeight;
      n = i1;
      if (i1 <= 0)
        n = 1; 
      i1 = i4;
      if (i4 <= 0)
        i1 = 1; 
      if (this.mAdjustViewBounds) {
        boolean bool = true;
        if (k != 1073741824) {
          i4 = 1;
        } else {
          i4 = 0;
        } 
        i = i4;
        if (m != 1073741824) {
          i4 = bool;
        } else {
          i4 = 0;
        } 
        j = i4;
        f = n / i1;
        i4 = n;
        n = i1;
        i1 = i4;
      } else {
        i4 = i1;
        i1 = n;
        n = i4;
      } 
    } 
    int i2 = this.mPaddingLeft;
    int i3 = this.mPaddingRight;
    m = this.mPaddingTop;
    k = this.mPaddingBottom;
    if (i != 0 || j != 0) {
      i1 = resolveAdjustedSize(i1 + i2 + i3, this.mMaxWidth, paramInt1);
      int i4 = resolveAdjustedSize(n + m + k, this.mMaxHeight, paramInt2);
      if (f != 0.0F) {
        float f1 = (i1 - i2 - i3) / (i4 - m - k);
        n = i4;
        int i5 = i1;
        if (Math.abs(f1 - f) > 1.0E-7D) {
          n = 0;
          if (i != 0) {
            n = (int)((i4 - m - k) * f) + i2 + i3;
            if (j == 0 && !sCompatAdjustViewBounds) {
              paramInt1 = resolveAdjustedSize(n, this.mMaxWidth, paramInt1);
            } else {
              paramInt1 = i1;
            } 
            if (n <= paramInt1) {
              paramInt1 = n;
              i1 = 1;
            } else {
              i1 = 0;
            } 
          } else {
            paramInt1 = i1;
            i1 = n;
          } 
          n = i4;
          i5 = paramInt1;
          if (i1 == 0) {
            n = i4;
            i5 = paramInt1;
            if (j != 0) {
              j = (int)((paramInt1 - i2 - i3) / f) + m + k;
              i1 = i4;
              if (i == 0) {
                i1 = i4;
                if (!sCompatAdjustViewBounds)
                  i1 = resolveAdjustedSize(j, this.mMaxHeight, paramInt2); 
              } 
              n = i1;
              i5 = paramInt1;
              if (j <= i1) {
                n = j;
                i5 = paramInt1;
              } 
            } 
          } 
        } 
        i1 = i5;
      } else {
        n = i4;
      } 
    } else {
      i1 = Math.max(i1 + i2 + i3, getSuggestedMinimumWidth());
      n = Math.max(n + m + k, getSuggestedMinimumHeight());
      i1 = resolveSizeAndState(i1, paramInt1, 0);
      n = resolveSizeAndState(n, paramInt2, 0);
    } 
    setMeasuredDimension(i1, n);
  }
  
  private int resolveAdjustedSize(int paramInt1, int paramInt2, int paramInt3) {
    int i = paramInt1;
    int j = View.MeasureSpec.getMode(paramInt3);
    paramInt3 = View.MeasureSpec.getSize(paramInt3);
    if (j != Integer.MIN_VALUE) {
      if (j != 0) {
        if (j != 1073741824) {
          paramInt1 = i;
        } else {
          paramInt1 = paramInt3;
        } 
      } else {
        paramInt1 = Math.min(paramInt1, paramInt2);
      } 
    } else {
      paramInt1 = Math.min(Math.min(paramInt1, paramInt3), paramInt2);
    } 
    return paramInt1;
  }
  
  protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mHaveFrame = true;
    configureBounds();
    return bool;
  }
  
  private void configureBounds() {
    boolean bool;
    if (this.mDrawable == null || !this.mHaveFrame)
      return; 
    int i = this.mDrawableWidth;
    int j = this.mDrawableHeight;
    int k = getWidth() - this.mPaddingLeft - this.mPaddingRight;
    int m = getHeight() - this.mPaddingTop - this.mPaddingBottom;
    if ((i < 0 || k == i) && (j < 0 || m == j)) {
      bool = true;
    } else {
      bool = false;
    } 
    if (i <= 0 || j <= 0 || ScaleType.FIT_XY == this.mScaleType) {
      this.mDrawable.setBounds(0, 0, k, m);
      this.mDrawMatrix = null;
      return;
    } 
    this.mDrawable.setBounds(0, 0, i, j);
    if (ScaleType.MATRIX == this.mScaleType) {
      if (this.mMatrix.isIdentity()) {
        this.mDrawMatrix = null;
      } else {
        this.mDrawMatrix = this.mMatrix;
      } 
    } else if (bool) {
      this.mDrawMatrix = null;
    } else if (ScaleType.CENTER == this.mScaleType) {
      Matrix matrix = this.mMatrix;
      float f1 = Math.round((k - i) * 0.5F), f2 = (m - j);
      f2 = Math.round(f2 * 0.5F);
      matrix.setTranslate(f1, f2);
    } else if (ScaleType.CENTER_CROP == this.mScaleType) {
      float f2;
      this.mDrawMatrix = this.mMatrix;
      float f1 = 0.0F, f3 = 0.0F;
      if (i * m > k * j) {
        f2 = m / j;
        f1 = (k - i * f2) * 0.5F;
      } else {
        f2 = k / i;
        f3 = (m - j * f2) * 0.5F;
      } 
      this.mDrawMatrix.setScale(f2, f2);
      this.mDrawMatrix.postTranslate(Math.round(f1), Math.round(f3));
    } else if (ScaleType.CENTER_INSIDE == this.mScaleType) {
      float f1;
      this.mDrawMatrix = this.mMatrix;
      if (i <= k && j <= m) {
        f1 = 1.0F;
      } else {
        f1 = Math.min(k / i, m / j);
      } 
      float f2 = Math.round((k - i * f1) * 0.5F);
      float f3 = Math.round((m - j * f1) * 0.5F);
      this.mDrawMatrix.setScale(f1, f1);
      this.mDrawMatrix.postTranslate(f2, f3);
    } else {
      this.mTempSrc.set(0.0F, 0.0F, i, j);
      this.mTempDst.set(0.0F, 0.0F, k, m);
      Matrix matrix = this.mMatrix;
      matrix.setRectToRect(this.mTempSrc, this.mTempDst, scaleTypeToScaleToFit(this.mScaleType));
    } 
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = this.mDrawable;
    if (drawable != null && drawable.isStateful() && 
      drawable.setState(getDrawableState()))
      invalidateDrawable(drawable); 
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Drawable drawable = this.mDrawable;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  public void animateTransform(Matrix paramMatrix) {
    Drawable drawable = this.mDrawable;
    if (drawable == null)
      return; 
    if (paramMatrix == null) {
      int i = getWidth(), j = this.mPaddingLeft, k = this.mPaddingRight;
      int m = getHeight(), n = this.mPaddingTop, i1 = this.mPaddingBottom;
      this.mDrawable.setBounds(0, 0, i - j - k, m - n - i1);
      this.mDrawMatrix = null;
    } else {
      drawable.setBounds(0, 0, this.mDrawableWidth, this.mDrawableHeight);
      if (this.mDrawMatrix == null)
        this.mDrawMatrix = new Matrix(); 
      this.mDrawMatrix.set(paramMatrix);
    } 
    invalidate();
  }
  
  protected void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    if (this.mDrawable == null)
      return; 
    if (this.mDrawableWidth == 0 || this.mDrawableHeight == 0)
      return; 
    if (this.mDrawMatrix == null && this.mPaddingTop == 0 && this.mPaddingLeft == 0) {
      this.mDrawable.draw(paramCanvas);
    } else {
      int i = paramCanvas.getSaveCount();
      paramCanvas.save();
      if (this.mCropToPadding) {
        int j = this.mScrollX;
        int k = this.mScrollY;
        paramCanvas.clipRect(this.mPaddingLeft + j, this.mPaddingTop + k, this.mRight + j - this.mLeft - this.mPaddingRight, this.mBottom + k - this.mTop - this.mPaddingBottom);
      } 
      paramCanvas.translate(this.mPaddingLeft, this.mPaddingTop);
      Matrix matrix = this.mDrawMatrix;
      if (matrix != null)
        paramCanvas.concat(matrix); 
      this.mDrawable.draw(paramCanvas);
      paramCanvas.restoreToCount(i);
    } 
  }
  
  @ExportedProperty(category = "layout")
  public int getBaseline() {
    if (this.mBaselineAlignBottom)
      return getMeasuredHeight(); 
    return this.mBaseline;
  }
  
  public void setBaseline(int paramInt) {
    if (this.mBaseline != paramInt) {
      this.mBaseline = paramInt;
      requestLayout();
    } 
  }
  
  public void setBaselineAlignBottom(boolean paramBoolean) {
    if (this.mBaselineAlignBottom != paramBoolean) {
      this.mBaselineAlignBottom = paramBoolean;
      requestLayout();
    } 
  }
  
  public boolean getBaselineAlignBottom() {
    return this.mBaselineAlignBottom;
  }
  
  public final void setColorFilter(int paramInt, PorterDuff.Mode paramMode) {
    setColorFilter((ColorFilter)new PorterDuffColorFilter(paramInt, paramMode));
  }
  
  @RemotableViewMethod
  public final void setColorFilter(int paramInt) {
    setColorFilter(paramInt, PorterDuff.Mode.SRC_ATOP);
  }
  
  public final void clearColorFilter() {
    setColorFilter((ColorFilter)null);
  }
  
  public final void setXfermode(Xfermode paramXfermode) {
    if (this.mXfermode != paramXfermode) {
      this.mXfermode = paramXfermode;
      this.mHasXfermode = true;
      applyXfermode();
      invalidate();
    } 
  }
  
  public ColorFilter getColorFilter() {
    return this.mColorFilter;
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    if (this.mColorFilter != paramColorFilter) {
      this.mColorFilter = paramColorFilter;
      this.mHasColorFilter = true;
      applyColorFilter();
      invalidate();
    } 
  }
  
  public int getImageAlpha() {
    return this.mAlpha;
  }
  
  @RemotableViewMethod
  public void setImageAlpha(int paramInt) {
    setAlpha(paramInt);
  }
  
  @RemotableViewMethod
  @Deprecated
  public void setAlpha(int paramInt) {
    paramInt &= 0xFF;
    if (this.mAlpha != paramInt) {
      this.mAlpha = paramInt;
      this.mHasAlpha = true;
      applyAlpha();
      invalidate();
    } 
  }
  
  private void applyXfermode() {
    Drawable drawable = this.mDrawable;
    if (drawable != null && this.mHasXfermode) {
      this.mDrawable = drawable = drawable.mutate();
      drawable.setXfermode(this.mXfermode);
    } 
  }
  
  private void applyColorFilter() {
    Drawable drawable = this.mDrawable;
    if (drawable != null && this.mHasColorFilter) {
      this.mDrawable = drawable = drawable.mutate();
      drawable.setColorFilter(this.mColorFilter);
    } 
  }
  
  private void applyAlpha() {
    Drawable drawable = this.mDrawable;
    if (drawable != null && this.mHasAlpha) {
      this.mDrawable = drawable = drawable.mutate();
      drawable.setAlpha(this.mAlpha * 256 >> 8);
    } 
  }
  
  public boolean isOpaque() {
    if (!super.isOpaque()) {
      Drawable drawable = this.mDrawable;
      if (drawable != null && this.mXfermode == null && 
        drawable.getOpacity() == -1 && this.mAlpha * 256 >> 8 == 255)
        if (isFilledByImage())
          return true;  
      return false;
    } 
    return true;
  }
  
  private boolean isFilledByImage() {
    Drawable drawable = this.mDrawable;
    boolean bool1 = false, bool2 = false;
    if (drawable == null)
      return false; 
    Rect rect = drawable.getBounds();
    Matrix matrix = this.mDrawMatrix;
    if (matrix == null) {
      if (rect.left <= 0 && rect.top <= 0 && rect.right >= getWidth()) {
        int i = rect.bottom;
        if (i >= getHeight())
          return true; 
      } 
      return bool2;
    } 
    if (matrix.rectStaysRect()) {
      RectF rectF1 = this.mTempSrc;
      RectF rectF2 = this.mTempDst;
      rectF1.set(rect);
      matrix.mapRect(rectF2, rectF1);
      if (rectF2.left <= 0.0F && rectF2.top <= 0.0F && rectF2.right >= getWidth()) {
        float f = rectF2.bottom;
        if (f >= getHeight())
          bool1 = true; 
      } 
      return bool1;
    } 
    return false;
  }
  
  public void onVisibilityAggregated(boolean paramBoolean) {
    super.onVisibilityAggregated(paramBoolean);
    Drawable drawable = this.mDrawable;
    if (drawable != null && !sCompatDrawableVisibilityDispatch)
      drawable.setVisible(paramBoolean, false); 
  }
  
  @RemotableViewMethod
  public void setVisibility(int paramInt) {
    super.setVisibility(paramInt);
    Drawable drawable = this.mDrawable;
    if (drawable != null && sCompatDrawableVisibilityDispatch) {
      boolean bool;
      if (paramInt == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      drawable.setVisible(bool, false);
    } 
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    Drawable drawable = this.mDrawable;
    if (drawable != null && sCompatDrawableVisibilityDispatch) {
      boolean bool;
      if (getVisibility() == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      drawable.setVisible(bool, false);
    } 
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    Drawable drawable = this.mDrawable;
    if (drawable != null && sCompatDrawableVisibilityDispatch)
      drawable.setVisible(false, false); 
  }
  
  public CharSequence getAccessibilityClassName() {
    return ImageView.class.getName();
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("layout:baseline", getBaseline());
  }
  
  public boolean isDefaultFocusHighlightNeeded(Drawable paramDrawable1, Drawable paramDrawable2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDrawable : Landroid/graphics/drawable/Drawable;
    //   4: astore_3
    //   5: iconst_0
    //   6: istore #4
    //   8: aload_3
    //   9: ifnull -> 40
    //   12: aload_3
    //   13: invokevirtual isStateful : ()Z
    //   16: ifeq -> 40
    //   19: aload_0
    //   20: getfield mDrawable : Landroid/graphics/drawable/Drawable;
    //   23: astore_3
    //   24: aload_3
    //   25: invokevirtual hasFocusStateSpecified : ()Z
    //   28: ifne -> 34
    //   31: goto -> 40
    //   34: iconst_0
    //   35: istore #5
    //   37: goto -> 43
    //   40: iconst_1
    //   41: istore #5
    //   43: iload #4
    //   45: istore #6
    //   47: aload_0
    //   48: aload_1
    //   49: aload_2
    //   50: invokespecial isDefaultFocusHighlightNeeded : (Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z
    //   53: ifeq -> 68
    //   56: iload #4
    //   58: istore #6
    //   60: iload #5
    //   62: ifeq -> 68
    //   65: iconst_1
    //   66: istore #6
    //   68: iload #6
    //   70: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1761	-> 0
    //   #1762	-> 24
    //   #1763	-> 43
  }
}
