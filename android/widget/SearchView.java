package android.widget;

import android.app.PendingIntent;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.CollapsibleActionView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R;
import java.util.WeakHashMap;

public class SearchView extends LinearLayout implements CollapsibleActionView {
  private Rect mSearchSrcTextViewBounds = new Rect();
  
  private Rect mSearchSrtTextViewBoundsExpanded = new Rect();
  
  private int[] mTemp = new int[2];
  
  private int[] mTemp2 = new int[2];
  
  private Runnable mUpdateDrawableStateRunnable = (Runnable)new Object(this);
  
  private Runnable mReleaseCursorRunnable = (Runnable)new Object(this);
  
  private final WeakHashMap<String, Drawable.ConstantState> mOutsideDrawablesCache = new WeakHashMap<>();
  
  private static final boolean DBG = false;
  
  private static final String IME_OPTION_NO_MICROPHONE = "nm";
  
  private static final String LOG_TAG = "SearchView";
  
  private Bundle mAppSearchData;
  
  private boolean mClearingFocus;
  
  private final ImageView mCloseButton;
  
  private final ImageView mCollapsedIcon;
  
  private int mCollapsedImeOptions;
  
  private final CharSequence mDefaultQueryHint;
  
  private final View mDropDownAnchor;
  
  private boolean mExpandedInActionView;
  
  private final ImageView mGoButton;
  
  private boolean mIconified;
  
  private boolean mIconifiedByDefault;
  
  private int mMaxWidth;
  
  private CharSequence mOldQueryText;
  
  private final View.OnClickListener mOnClickListener;
  
  private OnCloseListener mOnCloseListener;
  
  private final TextView.OnEditorActionListener mOnEditorActionListener;
  
  private final AdapterView.OnItemClickListener mOnItemClickListener;
  
  private final AdapterView.OnItemSelectedListener mOnItemSelectedListener;
  
  private OnQueryTextListener mOnQueryChangeListener;
  
  private View.OnFocusChangeListener mOnQueryTextFocusChangeListener;
  
  private View.OnClickListener mOnSearchClickListener;
  
  private OnSuggestionListener mOnSuggestionListener;
  
  private CharSequence mQueryHint;
  
  private boolean mQueryRefinement;
  
  private final ImageView mSearchButton;
  
  private final View mSearchEditFrame;
  
  private final Drawable mSearchHintIcon;
  
  private final View mSearchPlate;
  
  private final SearchAutoComplete mSearchSrcTextView;
  
  private SearchableInfo mSearchable;
  
  private final View mSubmitArea;
  
  private boolean mSubmitButtonEnabled;
  
  private final int mSuggestionCommitIconResId;
  
  private final int mSuggestionRowLayout;
  
  private CursorAdapter mSuggestionsAdapter;
  
  View.OnKeyListener mTextKeyListener;
  
  private TextWatcher mTextWatcher;
  
  private UpdatableTouchDelegate mTouchDelegate;
  
  private CharSequence mUserQuery;
  
  private final Intent mVoiceAppSearchIntent;
  
  private final ImageView mVoiceButton;
  
  private boolean mVoiceButtonEnabled;
  
  private final Intent mVoiceWebSearchIntent;
  
  public SearchView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public SearchView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843904);
  }
  
  public SearchView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SearchView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mOnClickListener = (View.OnClickListener)new Object(this);
    this.mTextKeyListener = (View.OnKeyListener)new Object(this);
    this.mOnEditorActionListener = (TextView.OnEditorActionListener)new Object(this);
    this.mOnItemClickListener = (AdapterView.OnItemClickListener)new Object(this);
    this.mOnItemSelectedListener = (AdapterView.OnItemSelectedListener)new Object(this);
    this.mTextWatcher = (TextWatcher)new Object(this);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SearchView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.SearchView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    LayoutInflater layoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    paramInt1 = typedArray.getResourceId(0, 17367291);
    layoutInflater.inflate(paramInt1, this, true);
    SearchAutoComplete searchAutoComplete = findViewById(16909398);
    searchAutoComplete.setSearchView(this);
    this.mSearchEditFrame = findViewById(16909394);
    this.mSearchPlate = findViewById(16909397);
    this.mSubmitArea = findViewById(16909490);
    this.mSearchButton = findViewById(16909392);
    this.mGoButton = findViewById(16909395);
    this.mCloseButton = findViewById(16909393);
    this.mVoiceButton = findViewById(16909400);
    this.mCollapsedIcon = findViewById(16909396);
    this.mSearchPlate.setBackground(typedArray.getDrawable(12));
    this.mSubmitArea.setBackground(typedArray.getDrawable(13));
    this.mSearchButton.setImageDrawable(typedArray.getDrawable(8));
    this.mGoButton.setImageDrawable(typedArray.getDrawable(7));
    this.mCloseButton.setImageDrawable(typedArray.getDrawable(6));
    this.mVoiceButton.setImageDrawable(typedArray.getDrawable(9));
    this.mCollapsedIcon.setImageDrawable(typedArray.getDrawable(8));
    if (typedArray.hasValueOrEmpty(14)) {
      this.mSearchHintIcon = typedArray.getDrawable(14);
    } else {
      this.mSearchHintIcon = typedArray.getDrawable(8);
    } 
    this.mSuggestionRowLayout = typedArray.getResourceId(11, 17367290);
    this.mSuggestionCommitIconResId = typedArray.getResourceId(10, 0);
    this.mSearchButton.setOnClickListener(this.mOnClickListener);
    this.mCloseButton.setOnClickListener(this.mOnClickListener);
    this.mGoButton.setOnClickListener(this.mOnClickListener);
    this.mVoiceButton.setOnClickListener(this.mOnClickListener);
    this.mSearchSrcTextView.setOnClickListener(this.mOnClickListener);
    this.mSearchSrcTextView.addTextChangedListener(this.mTextWatcher);
    this.mSearchSrcTextView.setOnEditorActionListener(this.mOnEditorActionListener);
    this.mSearchSrcTextView.setOnItemClickListener(this.mOnItemClickListener);
    this.mSearchSrcTextView.setOnItemSelectedListener(this.mOnItemSelectedListener);
    this.mSearchSrcTextView.setOnKeyListener(this.mTextKeyListener);
    this.mSearchSrcTextView.setOnFocusChangeListener((View.OnFocusChangeListener)new Object(this));
    setIconifiedByDefault(typedArray.getBoolean(4, true));
    paramInt1 = typedArray.getDimensionPixelSize(1, -1);
    if (paramInt1 != -1)
      setMaxWidth(paramInt1); 
    this.mDefaultQueryHint = typedArray.getText(15);
    this.mQueryHint = typedArray.getText(5);
    paramInt1 = typedArray.getInt(3, -1);
    if (paramInt1 != -1)
      setImeOptions(paramInt1); 
    paramInt1 = typedArray.getInt(2, -1);
    if (paramInt1 != -1)
      setInputType(paramInt1); 
    if (getFocusable() == 16)
      setFocusable(1); 
    typedArray.recycle();
    Intent intent = new Intent("android.speech.action.WEB_SEARCH");
    intent.addFlags(268435456);
    this.mVoiceWebSearchIntent.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
    this.mVoiceAppSearchIntent = intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
    intent.addFlags(268435456);
    this.mDropDownAnchor = (View)(intent = findViewById(this.mSearchSrcTextView.getDropDownAnchor()));
    if (intent != null)
      intent.addOnLayoutChangeListener((View.OnLayoutChangeListener)new Object(this)); 
    updateViewsVisibility(this.mIconifiedByDefault);
    updateQueryHint();
  }
  
  int getSuggestionRowLayout() {
    return this.mSuggestionRowLayout;
  }
  
  int getSuggestionCommitIconResId() {
    return this.mSuggestionCommitIconResId;
  }
  
  public void setSearchableInfo(SearchableInfo paramSearchableInfo) {
    this.mSearchable = paramSearchableInfo;
    if (paramSearchableInfo != null) {
      updateSearchAutoComplete();
      updateQueryHint();
    } 
    boolean bool = hasVoiceSearch();
    if (bool)
      this.mSearchSrcTextView.setPrivateImeOptions("nm"); 
    updateViewsVisibility(isIconified());
  }
  
  public void setAppSearchData(Bundle paramBundle) {
    this.mAppSearchData = paramBundle;
  }
  
  public void setImeOptions(int paramInt) {
    this.mSearchSrcTextView.setImeOptions(paramInt);
  }
  
  public int getImeOptions() {
    return this.mSearchSrcTextView.getImeOptions();
  }
  
  public void setInputType(int paramInt) {
    this.mSearchSrcTextView.setInputType(paramInt);
  }
  
  public int getInputType() {
    return this.mSearchSrcTextView.getInputType();
  }
  
  public boolean requestFocus(int paramInt, Rect paramRect) {
    if (this.mClearingFocus)
      return false; 
    if (!isFocusable())
      return false; 
    if (!isIconified()) {
      boolean bool = this.mSearchSrcTextView.requestFocus(paramInt, paramRect);
      if (bool)
        updateViewsVisibility(false); 
      return bool;
    } 
    return super.requestFocus(paramInt, paramRect);
  }
  
  public void clearFocus() {
    this.mClearingFocus = true;
    super.clearFocus();
    this.mSearchSrcTextView.clearFocus();
    this.mSearchSrcTextView.setImeVisibility(false);
    this.mClearingFocus = false;
  }
  
  public void setOnQueryTextListener(OnQueryTextListener paramOnQueryTextListener) {
    this.mOnQueryChangeListener = paramOnQueryTextListener;
  }
  
  public void setOnCloseListener(OnCloseListener paramOnCloseListener) {
    this.mOnCloseListener = paramOnCloseListener;
  }
  
  public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener paramOnFocusChangeListener) {
    this.mOnQueryTextFocusChangeListener = paramOnFocusChangeListener;
  }
  
  public void setOnSuggestionListener(OnSuggestionListener paramOnSuggestionListener) {
    this.mOnSuggestionListener = paramOnSuggestionListener;
  }
  
  public void setOnSearchClickListener(View.OnClickListener paramOnClickListener) {
    this.mOnSearchClickListener = paramOnClickListener;
  }
  
  public CharSequence getQuery() {
    return this.mSearchSrcTextView.getText();
  }
  
  public void setQuery(CharSequence paramCharSequence, boolean paramBoolean) {
    this.mSearchSrcTextView.setText(paramCharSequence);
    if (paramCharSequence != null) {
      SearchAutoComplete searchAutoComplete = this.mSearchSrcTextView;
      searchAutoComplete.setSelection(searchAutoComplete.length());
      this.mUserQuery = paramCharSequence;
    } 
    if (paramBoolean && !TextUtils.isEmpty(paramCharSequence))
      onSubmitQuery(); 
  }
  
  public void setQueryHint(CharSequence paramCharSequence) {
    this.mQueryHint = paramCharSequence;
    updateQueryHint();
  }
  
  public CharSequence getQueryHint() {
    CharSequence charSequence;
    if (this.mQueryHint != null) {
      charSequence = this.mQueryHint;
    } else {
      SearchableInfo searchableInfo = this.mSearchable;
      if (searchableInfo != null && searchableInfo.getHintId() != 0) {
        charSequence = getContext().getText(this.mSearchable.getHintId());
      } else {
        charSequence = this.mDefaultQueryHint;
      } 
    } 
    return charSequence;
  }
  
  public void setIconifiedByDefault(boolean paramBoolean) {
    if (this.mIconifiedByDefault == paramBoolean)
      return; 
    this.mIconifiedByDefault = paramBoolean;
    updateViewsVisibility(paramBoolean);
    updateQueryHint();
  }
  
  @Deprecated
  public boolean isIconfiedByDefault() {
    return this.mIconifiedByDefault;
  }
  
  public boolean isIconifiedByDefault() {
    return this.mIconifiedByDefault;
  }
  
  public void setIconified(boolean paramBoolean) {
    if (paramBoolean) {
      onCloseClicked();
    } else {
      onSearchClicked();
    } 
  }
  
  public boolean isIconified() {
    return this.mIconified;
  }
  
  public void setSubmitButtonEnabled(boolean paramBoolean) {
    this.mSubmitButtonEnabled = paramBoolean;
    updateViewsVisibility(isIconified());
  }
  
  public boolean isSubmitButtonEnabled() {
    return this.mSubmitButtonEnabled;
  }
  
  public void setQueryRefinementEnabled(boolean paramBoolean) {
    this.mQueryRefinement = paramBoolean;
    CursorAdapter cursorAdapter = this.mSuggestionsAdapter;
    if (cursorAdapter instanceof SuggestionsAdapter) {
      boolean bool;
      cursorAdapter = cursorAdapter;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = true;
      } 
      cursorAdapter.setQueryRefinement(bool);
    } 
  }
  
  public boolean isQueryRefinementEnabled() {
    return this.mQueryRefinement;
  }
  
  public void setSuggestionsAdapter(CursorAdapter paramCursorAdapter) {
    this.mSuggestionsAdapter = paramCursorAdapter;
    this.mSearchSrcTextView.setAdapter(paramCursorAdapter);
  }
  
  public CursorAdapter getSuggestionsAdapter() {
    return this.mSuggestionsAdapter;
  }
  
  public void setMaxWidth(int paramInt) {
    this.mMaxWidth = paramInt;
    requestLayout();
  }
  
  public int getMaxWidth() {
    return this.mMaxWidth;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    if (isIconified()) {
      super.onMeasure(paramInt1, paramInt2);
      return;
    } 
    int i = View.MeasureSpec.getMode(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt1);
    if (i != Integer.MIN_VALUE) {
      if (i != 0) {
        if (i != 1073741824) {
          paramInt1 = j;
        } else {
          i = this.mMaxWidth;
          paramInt1 = j;
          if (i > 0)
            paramInt1 = Math.min(i, j); 
        } 
      } else {
        paramInt1 = this.mMaxWidth;
        if (paramInt1 <= 0)
          paramInt1 = getPreferredWidth(); 
      } 
    } else {
      paramInt1 = this.mMaxWidth;
      if (paramInt1 > 0) {
        paramInt1 = Math.min(paramInt1, j);
      } else {
        paramInt1 = Math.min(getPreferredWidth(), j);
      } 
    } 
    j = View.MeasureSpec.getMode(paramInt2);
    paramInt2 = View.MeasureSpec.getSize(paramInt2);
    if (j != Integer.MIN_VALUE) {
      if (j == 0)
        paramInt2 = getPreferredHeight(); 
    } else {
      paramInt2 = Math.min(getPreferredHeight(), paramInt2);
    } 
    paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    paramInt2 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    super.onMeasure(paramInt1, paramInt2);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramBoolean) {
      getChildBoundsWithinSearchView(this.mSearchSrcTextView, this.mSearchSrcTextViewBounds);
      this.mSearchSrtTextViewBoundsExpanded.set(this.mSearchSrcTextViewBounds.left, 0, this.mSearchSrcTextViewBounds.right, paramInt4 - paramInt2);
      UpdatableTouchDelegate updatableTouchDelegate = this.mTouchDelegate;
      if (updatableTouchDelegate == null) {
        this.mTouchDelegate = updatableTouchDelegate = new UpdatableTouchDelegate(this.mSearchSrtTextViewBoundsExpanded, this.mSearchSrcTextViewBounds, this.mSearchSrcTextView);
        setTouchDelegate(updatableTouchDelegate);
      } else {
        updatableTouchDelegate.setBounds(this.mSearchSrtTextViewBoundsExpanded, this.mSearchSrcTextViewBounds);
      } 
    } 
  }
  
  private void getChildBoundsWithinSearchView(View paramView, Rect paramRect) {
    paramView.getLocationInWindow(this.mTemp);
    getLocationInWindow(this.mTemp2);
    int arrayOfInt1[] = this.mTemp, i = arrayOfInt1[1], arrayOfInt2[] = this.mTemp2, j = i - arrayOfInt2[1];
    i = arrayOfInt1[0] - arrayOfInt2[0];
    paramRect.set(i, j, paramView.getWidth() + i, paramView.getHeight() + j);
  }
  
  private int getPreferredWidth() {
    Resources resources = getContext().getResources();
    return resources.getDimensionPixelSize(17105465);
  }
  
  private int getPreferredHeight() {
    Resources resources = getContext().getResources();
    return resources.getDimensionPixelSize(17105464);
  }
  
  private void updateViewsVisibility(boolean paramBoolean) {
    byte b2;
    this.mIconified = paramBoolean;
    byte b1 = 8;
    boolean bool = false;
    if (paramBoolean) {
      b2 = 0;
    } else {
      b2 = 8;
    } 
    int i = TextUtils.isEmpty(this.mSearchSrcTextView.getText()) ^ true;
    this.mSearchButton.setVisibility(b2);
    updateSubmitButton(i);
    View view = this.mSearchEditFrame;
    if (paramBoolean) {
      b2 = b1;
    } else {
      b2 = 0;
    } 
    view.setVisibility(b2);
    if (this.mCollapsedIcon.getDrawable() == null || this.mIconifiedByDefault) {
      b2 = 8;
    } else {
      b2 = 0;
    } 
    this.mCollapsedIcon.setVisibility(b2);
    updateCloseButton();
    paramBoolean = bool;
    if (i == 0)
      paramBoolean = true; 
    updateVoiceButton(paramBoolean);
    updateSubmitArea();
  }
  
  private boolean hasVoiceSearch() {
    SearchableInfo searchableInfo = this.mSearchable;
    boolean bool = false;
    if (searchableInfo != null && searchableInfo.getVoiceSearchEnabled()) {
      Intent intent;
      searchableInfo = null;
      if (this.mSearchable.getVoiceSearchLaunchWebSearch()) {
        intent = this.mVoiceWebSearchIntent;
      } else if (this.mSearchable.getVoiceSearchLaunchRecognizer()) {
        intent = this.mVoiceAppSearchIntent;
      } 
      if (intent != null) {
        ResolveInfo resolveInfo = getContext().getPackageManager().resolveActivity(intent, 65536);
        if (resolveInfo != null)
          bool = true; 
        return bool;
      } 
    } 
    return false;
  }
  
  private boolean isSubmitAreaEnabled() {
    boolean bool;
    if ((this.mSubmitButtonEnabled || this.mVoiceButtonEnabled) && !isIconified()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void updateSubmitButton(boolean paramBoolean) {
    // Byte code:
    //   0: bipush #8
    //   2: istore_2
    //   3: iload_2
    //   4: istore_3
    //   5: aload_0
    //   6: getfield mSubmitButtonEnabled : Z
    //   9: ifeq -> 45
    //   12: iload_2
    //   13: istore_3
    //   14: aload_0
    //   15: invokespecial isSubmitAreaEnabled : ()Z
    //   18: ifeq -> 45
    //   21: iload_2
    //   22: istore_3
    //   23: aload_0
    //   24: invokevirtual hasFocus : ()Z
    //   27: ifeq -> 45
    //   30: iload_1
    //   31: ifne -> 43
    //   34: iload_2
    //   35: istore_3
    //   36: aload_0
    //   37: getfield mVoiceButtonEnabled : Z
    //   40: ifne -> 45
    //   43: iconst_0
    //   44: istore_3
    //   45: aload_0
    //   46: getfield mGoButton : Landroid/widget/ImageView;
    //   49: iload_3
    //   50: invokevirtual setVisibility : (I)V
    //   53: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #942	-> 0
    //   #943	-> 3
    //   #945	-> 43
    //   #947	-> 45
    //   #948	-> 53
  }
  
  private void updateSubmitArea() {
    // Byte code:
    //   0: bipush #8
    //   2: istore_1
    //   3: iload_1
    //   4: istore_2
    //   5: aload_0
    //   6: invokespecial isSubmitAreaEnabled : ()Z
    //   9: ifeq -> 40
    //   12: aload_0
    //   13: getfield mGoButton : Landroid/widget/ImageView;
    //   16: astore_3
    //   17: aload_3
    //   18: invokevirtual getVisibility : ()I
    //   21: ifeq -> 38
    //   24: aload_0
    //   25: getfield mVoiceButton : Landroid/widget/ImageView;
    //   28: astore_3
    //   29: iload_1
    //   30: istore_2
    //   31: aload_3
    //   32: invokevirtual getVisibility : ()I
    //   35: ifne -> 40
    //   38: iconst_0
    //   39: istore_2
    //   40: aload_0
    //   41: getfield mSubmitArea : Landroid/view/View;
    //   44: iload_2
    //   45: invokevirtual setVisibility : (I)V
    //   48: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #952	-> 0
    //   #953	-> 3
    //   #954	-> 17
    //   #955	-> 29
    //   #956	-> 38
    //   #958	-> 40
    //   #959	-> 48
  }
  
  private void updateCloseButton() {
    boolean bool = TextUtils.isEmpty(this.mSearchSrcTextView.getText());
    byte b1 = 1;
    int i = bool ^ true;
    byte b2 = 0, b3 = b1;
    if (i == 0)
      if (this.mIconifiedByDefault && !this.mExpandedInActionView) {
        b3 = b1;
      } else {
        b3 = 0;
      }  
    ImageView imageView = this.mCloseButton;
    if (b3) {
      b3 = b2;
    } else {
      b3 = 8;
    } 
    imageView.setVisibility(b3);
    Drawable drawable = this.mCloseButton.getDrawable();
    if (drawable != null) {
      int[] arrayOfInt;
      if (i != 0) {
        arrayOfInt = ENABLED_STATE_SET;
      } else {
        arrayOfInt = EMPTY_STATE_SET;
      } 
      drawable.setState(arrayOfInt);
    } 
  }
  
  private void postUpdateFocusedState() {
    post(this.mUpdateDrawableStateRunnable);
  }
  
  private void updateFocusedState() {
    int[] arrayOfInt;
    boolean bool = this.mSearchSrcTextView.hasFocus();
    if (bool) {
      arrayOfInt = FOCUSED_STATE_SET;
    } else {
      arrayOfInt = EMPTY_STATE_SET;
    } 
    Drawable drawable = this.mSearchPlate.getBackground();
    if (drawable != null)
      drawable.setState(arrayOfInt); 
    drawable = this.mSubmitArea.getBackground();
    if (drawable != null)
      drawable.setState(arrayOfInt); 
    invalidate();
  }
  
  protected void onDetachedFromWindow() {
    removeCallbacks(this.mUpdateDrawableStateRunnable);
    post(this.mReleaseCursorRunnable);
    super.onDetachedFromWindow();
  }
  
  void onQueryRefine(CharSequence paramCharSequence) {
    setQuery(paramCharSequence);
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    String str;
    SearchableInfo searchableInfo = this.mSearchable;
    if (searchableInfo == null)
      return false; 
    SearchableInfo.ActionKeyInfo actionKeyInfo = searchableInfo.findActionKey(paramInt);
    if (actionKeyInfo != null && actionKeyInfo.getQueryActionMsg() != null) {
      str = actionKeyInfo.getQueryActionMsg();
      Editable editable = this.mSearchSrcTextView.getText();
      String str1 = editable.toString();
      launchQuerySearch(paramInt, str, str1);
      return true;
    } 
    return super.onKeyDown(paramInt, (KeyEvent)str);
  }
  
  private boolean onSuggestionsKey(View paramView, int paramInt, KeyEvent paramKeyEvent) {
    if (this.mSearchable == null)
      return false; 
    if (this.mSuggestionsAdapter == null)
      return false; 
    if (paramKeyEvent.getAction() == 0 && paramKeyEvent.hasNoModifiers()) {
      if (paramInt == 66 || paramInt == 84 || paramInt == 61) {
        paramInt = this.mSearchSrcTextView.getListSelection();
        return onItemClicked(paramInt, 0, (String)null);
      } 
      if (paramInt == 21 || paramInt == 22) {
        if (paramInt == 21) {
          paramInt = 0;
        } else {
          paramInt = this.mSearchSrcTextView.length();
        } 
        this.mSearchSrcTextView.setSelection(paramInt);
        this.mSearchSrcTextView.setListSelection(0);
        this.mSearchSrcTextView.clearListSelection();
        this.mSearchSrcTextView.ensureImeVisible(true);
        return true;
      } 
      if (paramInt == 19 && this.mSearchSrcTextView.getListSelection() == 0)
        return false; 
      SearchableInfo.ActionKeyInfo actionKeyInfo = this.mSearchable.findActionKey(paramInt);
      if (actionKeyInfo != null && (actionKeyInfo.getSuggestActionMsg() != null || actionKeyInfo.getSuggestActionMsgColumn() != null)) {
        int i = this.mSearchSrcTextView.getListSelection();
        if (i != -1) {
          Cursor cursor = this.mSuggestionsAdapter.getCursor();
          if (cursor.moveToPosition(i)) {
            String str = getActionKeyMessage(cursor, actionKeyInfo);
            if (str != null && str.length() > 0)
              return onItemClicked(i, paramInt, str); 
          } 
        } 
      } 
    } 
    return false;
  }
  
  private static String getActionKeyMessage(Cursor paramCursor, SearchableInfo.ActionKeyInfo paramActionKeyInfo) {
    String str2 = null;
    String str3 = paramActionKeyInfo.getSuggestActionMsgColumn();
    if (str3 != null)
      str2 = SuggestionsAdapter.getColumnString(paramCursor, str3); 
    String str1 = str2;
    if (str2 == null)
      str1 = paramActionKeyInfo.getSuggestActionMsg(); 
    return str1;
  }
  
  private CharSequence getDecoratedHint(CharSequence paramCharSequence) {
    if (!this.mIconifiedByDefault || this.mSearchHintIcon == null)
      return paramCharSequence; 
    int i = (int)(this.mSearchSrcTextView.getTextSize() * 1.25D);
    this.mSearchHintIcon.setBounds(0, 0, i, i);
    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
    spannableStringBuilder.setSpan(new ImageSpan(this.mSearchHintIcon), 1, 2, 33);
    spannableStringBuilder.append(paramCharSequence);
    return spannableStringBuilder;
  }
  
  private void updateQueryHint() {
    CharSequence charSequence = getQueryHint();
    SearchAutoComplete searchAutoComplete = this.mSearchSrcTextView;
    if (charSequence == null)
      charSequence = ""; 
    searchAutoComplete.setHint(getDecoratedHint(charSequence));
  }
  
  private void updateSearchAutoComplete() {
    this.mSearchSrcTextView.setDropDownAnimationStyle(0);
    this.mSearchSrcTextView.setThreshold(this.mSearchable.getSuggestThreshold());
    this.mSearchSrcTextView.setImeOptions(this.mSearchable.getImeOptions());
    int i = this.mSearchable.getInputType();
    boolean bool = true;
    int j = i;
    if ((i & 0xF) == 1) {
      i &= 0xFFFEFFFF;
      j = i;
      if (this.mSearchable.getSuggestAuthority() != null)
        j = i | 0x10000 | 0x80000; 
    } 
    this.mSearchSrcTextView.setInputType(j);
    CursorAdapter cursorAdapter = this.mSuggestionsAdapter;
    if (cursorAdapter != null)
      cursorAdapter.changeCursor(null); 
    if (this.mSearchable.getSuggestAuthority() != null) {
      this.mSuggestionsAdapter = cursorAdapter = new SuggestionsAdapter(getContext(), this, this.mSearchable, this.mOutsideDrawablesCache);
      this.mSearchSrcTextView.setAdapter(cursorAdapter);
      cursorAdapter = this.mSuggestionsAdapter;
      if (this.mQueryRefinement) {
        j = 2;
      } else {
        j = bool;
      } 
      cursorAdapter.setQueryRefinement(j);
    } 
  }
  
  private void updateVoiceButton(boolean paramBoolean) {
    byte b1 = 8;
    byte b2 = b1;
    if (this.mVoiceButtonEnabled) {
      b2 = b1;
      if (!isIconified()) {
        b2 = b1;
        if (paramBoolean) {
          b2 = 0;
          this.mGoButton.setVisibility(8);
        } 
      } 
    } 
    this.mVoiceButton.setVisibility(b2);
  }
  
  private void onTextChanged(CharSequence paramCharSequence) {
    Editable editable = this.mSearchSrcTextView.getText();
    this.mUserQuery = editable;
    boolean bool = TextUtils.isEmpty(editable);
    boolean bool1 = true;
    int i = bool ^ true;
    updateSubmitButton(i);
    if (i != 0)
      bool1 = false; 
    updateVoiceButton(bool1);
    updateCloseButton();
    updateSubmitArea();
    if (this.mOnQueryChangeListener != null && !TextUtils.equals(paramCharSequence, this.mOldQueryText))
      this.mOnQueryChangeListener.onQueryTextChange(paramCharSequence.toString()); 
    this.mOldQueryText = paramCharSequence.toString();
  }
  
  private void onSubmitQuery() {
    Editable editable = this.mSearchSrcTextView.getText();
    if (editable != null && TextUtils.getTrimmedLength(editable) > 0) {
      OnQueryTextListener onQueryTextListener = this.mOnQueryChangeListener;
      if (onQueryTextListener == null || !onQueryTextListener.onQueryTextSubmit(editable.toString())) {
        if (this.mSearchable != null)
          launchQuerySearch(0, (String)null, editable.toString()); 
        this.mSearchSrcTextView.setImeVisibility(false);
        dismissSuggestions();
      } 
    } 
  }
  
  private void dismissSuggestions() {
    this.mSearchSrcTextView.dismissDropDown();
  }
  
  private void onCloseClicked() {
    Editable editable = this.mSearchSrcTextView.getText();
    if (TextUtils.isEmpty(editable)) {
      if (this.mIconifiedByDefault) {
        OnCloseListener onCloseListener = this.mOnCloseListener;
        if (onCloseListener == null || !onCloseListener.onClose()) {
          clearFocus();
          updateViewsVisibility(true);
        } 
      } 
    } else {
      this.mSearchSrcTextView.setText("");
      this.mSearchSrcTextView.requestFocus();
      this.mSearchSrcTextView.setImeVisibility(true);
    } 
  }
  
  private void onSearchClicked() {
    updateViewsVisibility(false);
    this.mSearchSrcTextView.requestFocus();
    this.mSearchSrcTextView.setImeVisibility(true);
    View.OnClickListener onClickListener = this.mOnSearchClickListener;
    if (onClickListener != null)
      onClickListener.onClick(this); 
  }
  
  private void onVoiceClicked() {
    if (this.mSearchable == null)
      return; 
    SearchableInfo searchableInfo = this.mSearchable;
    try {
      Intent intent;
      if (searchableInfo.getVoiceSearchLaunchWebSearch()) {
        intent = createVoiceWebSearchIntent(this.mVoiceWebSearchIntent, searchableInfo);
        getContext().startActivity(intent);
      } else if (intent.getVoiceSearchLaunchRecognizer()) {
        intent = createVoiceAppSearchIntent(this.mVoiceAppSearchIntent, (SearchableInfo)intent);
        getContext().startActivity(intent);
      } 
    } catch (ActivityNotFoundException activityNotFoundException) {
      Log.w("SearchView", "Could not find voice search activity");
    } 
  }
  
  void onTextFocusChanged() {
    updateViewsVisibility(isIconified());
    postUpdateFocusedState();
    if (this.mSearchSrcTextView.hasFocus())
      forceSuggestionQuery(); 
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    super.onWindowFocusChanged(paramBoolean);
    postUpdateFocusedState();
  }
  
  public void onActionViewCollapsed() {
    setQuery("", false);
    clearFocus();
    updateViewsVisibility(true);
    this.mSearchSrcTextView.setImeOptions(this.mCollapsedImeOptions);
    this.mExpandedInActionView = false;
  }
  
  public void onActionViewExpanded() {
    if (this.mExpandedInActionView)
      return; 
    this.mExpandedInActionView = true;
    int i = this.mSearchSrcTextView.getImeOptions();
    this.mSearchSrcTextView.setImeOptions(i | 0x2000000);
    this.mSearchSrcTextView.setText("");
    setIconified(false);
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState(SearchView this$0) {
      super((Parcelable)this$0);
    }
    
    public SavedState(SearchView this$0) {
      super((Parcel)this$0);
      this.isIconified = ((Boolean)this$0.readValue(null)).booleanValue();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeValue(Boolean.valueOf(this.isIconified));
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SearchView.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" isIconified=");
      stringBuilder.append(this.isIconified);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    boolean isIconified;
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    parcelable = new SavedState(parcelable);
    ((SavedState)parcelable).isIconified = isIconified();
    return parcelable;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    updateViewsVisibility(((SavedState)paramParcelable).isIconified);
    requestLayout();
  }
  
  public CharSequence getAccessibilityClassName() {
    return SearchView.class.getName();
  }
  
  private void adjustDropDownSizeAndPosition() {
    if (this.mDropDownAnchor.getWidth() > 1) {
      byte b;
      Resources resources = getContext().getResources();
      int i = this.mSearchPlate.getPaddingLeft();
      Rect rect = new Rect();
      boolean bool = isLayoutRtl();
      if (this.mIconifiedByDefault) {
        b = resources.getDimensionPixelSize(17105176);
        b += resources.getDimensionPixelSize(17105177);
      } else {
        b = 0;
      } 
      this.mSearchSrcTextView.getDropDownBackground().getPadding(rect);
      if (bool) {
        j = -rect.left;
      } else {
        j = i - rect.left + b;
      } 
      this.mSearchSrcTextView.setDropDownHorizontalOffset(j);
      int k = this.mDropDownAnchor.getWidth(), j = rect.left, m = rect.right;
      this.mSearchSrcTextView.setDropDownWidth(k + j + m + b - i);
    } 
  }
  
  private boolean onItemClicked(int paramInt1, int paramInt2, String paramString) {
    OnSuggestionListener onSuggestionListener = this.mOnSuggestionListener;
    if (onSuggestionListener == null || !onSuggestionListener.onSuggestionClick(paramInt1)) {
      launchSuggestion(paramInt1, 0, (String)null);
      this.mSearchSrcTextView.setImeVisibility(false);
      dismissSuggestions();
      return true;
    } 
    return false;
  }
  
  private boolean onItemSelected(int paramInt) {
    OnSuggestionListener onSuggestionListener = this.mOnSuggestionListener;
    if (onSuggestionListener == null || !onSuggestionListener.onSuggestionSelect(paramInt)) {
      rewriteQueryFromSuggestion(paramInt);
      return true;
    } 
    return false;
  }
  
  private void rewriteQueryFromSuggestion(int paramInt) {
    Editable editable = this.mSearchSrcTextView.getText();
    Cursor cursor = this.mSuggestionsAdapter.getCursor();
    if (cursor == null)
      return; 
    if (cursor.moveToPosition(paramInt)) {
      CharSequence charSequence = this.mSuggestionsAdapter.convertToString(cursor);
      if (charSequence != null) {
        setQuery(charSequence);
      } else {
        setQuery(editable);
      } 
    } else {
      setQuery(editable);
    } 
  }
  
  private boolean launchSuggestion(int paramInt1, int paramInt2, String paramString) {
    Cursor cursor = this.mSuggestionsAdapter.getCursor();
    if (cursor != null && cursor.moveToPosition(paramInt1)) {
      Intent intent = createIntentFromSuggestion(cursor, paramInt2, paramString);
      launchIntent(intent);
      return true;
    } 
    return false;
  }
  
  private void launchIntent(Intent paramIntent) {
    if (paramIntent == null)
      return; 
    try {
      getContext().startActivity(paramIntent);
    } catch (RuntimeException runtimeException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed launch activity: ");
      stringBuilder.append(paramIntent);
      Log.e("SearchView", stringBuilder.toString(), runtimeException);
    } 
  }
  
  private void setQuery(CharSequence paramCharSequence) {
    int i;
    this.mSearchSrcTextView.setText(paramCharSequence, true);
    SearchAutoComplete searchAutoComplete = this.mSearchSrcTextView;
    if (TextUtils.isEmpty(paramCharSequence)) {
      i = 0;
    } else {
      i = paramCharSequence.length();
    } 
    searchAutoComplete.setSelection(i);
  }
  
  private void launchQuerySearch(int paramInt, String paramString1, String paramString2) {
    Intent intent = createIntent("android.intent.action.SEARCH", (Uri)null, (String)null, paramString2, paramInt, paramString1);
    getContext().startActivity(intent);
  }
  
  private Intent createIntent(String paramString1, Uri paramUri, String paramString2, String paramString3, int paramInt, String paramString4) {
    Intent intent = new Intent(paramString1);
    intent.addFlags(268435456);
    if (paramUri != null)
      intent.setData(paramUri); 
    intent.putExtra("user_query", this.mUserQuery);
    if (paramString3 != null)
      intent.putExtra("query", paramString3); 
    if (paramString2 != null)
      intent.putExtra("intent_extra_data_key", paramString2); 
    Bundle bundle = this.mAppSearchData;
    if (bundle != null)
      intent.putExtra("app_data", bundle); 
    if (paramInt != 0) {
      intent.putExtra("action_key", paramInt);
      intent.putExtra("action_msg", paramString4);
    } 
    intent.setComponent(this.mSearchable.getSearchActivity());
    return intent;
  }
  
  private Intent createVoiceWebSearchIntent(Intent paramIntent, SearchableInfo paramSearchableInfo) {
    String str;
    Intent intent = new Intent(paramIntent);
    ComponentName componentName = paramSearchableInfo.getSearchActivity();
    if (componentName == null) {
      componentName = null;
    } else {
      str = componentName.flattenToShortString();
    } 
    intent.putExtra("calling_package", str);
    return intent;
  }
  
  private Intent createVoiceAppSearchIntent(Intent paramIntent, SearchableInfo paramSearchableInfo) {
    String str2;
    ComponentName componentName = paramSearchableInfo.getSearchActivity();
    Intent intent1 = new Intent("android.intent.action.SEARCH");
    intent1.setComponent(componentName);
    PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 0, intent1, 1073741824);
    Bundle bundle2 = new Bundle();
    Bundle bundle1 = this.mAppSearchData;
    if (bundle1 != null)
      bundle2.putParcelable("app_data", (Parcelable)bundle1); 
    Intent intent2 = new Intent(paramIntent);
    String str1 = "free_form";
    bundle1 = null;
    String str3 = null;
    int i = 1;
    Resources resources = getResources();
    if (paramSearchableInfo.getVoiceLanguageModeId() != 0)
      str1 = resources.getString(paramSearchableInfo.getVoiceLanguageModeId()); 
    if (paramSearchableInfo.getVoicePromptTextId() != 0)
      str2 = resources.getString(paramSearchableInfo.getVoicePromptTextId()); 
    if (paramSearchableInfo.getVoiceLanguageId() != 0)
      str3 = resources.getString(paramSearchableInfo.getVoiceLanguageId()); 
    if (paramSearchableInfo.getVoiceMaxResults() != 0)
      i = paramSearchableInfo.getVoiceMaxResults(); 
    intent2.putExtra("android.speech.extra.LANGUAGE_MODEL", str1);
    intent2.putExtra("android.speech.extra.PROMPT", str2);
    intent2.putExtra("android.speech.extra.LANGUAGE", str3);
    intent2.putExtra("android.speech.extra.MAX_RESULTS", i);
    if (componentName == null) {
      str1 = null;
    } else {
      str1 = componentName.flattenToShortString();
    } 
    intent2.putExtra("calling_package", str1);
    intent2.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", (Parcelable)pendingIntent);
    intent2.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", bundle2);
    return intent2;
  }
  
  private Intent createIntentFromSuggestion(Cursor paramCursor, int paramInt, String paramString) {
    try {
      Uri uri;
      String str1 = SuggestionsAdapter.getColumnString(paramCursor, "suggest_intent_action");
      String str2 = str1;
      if (str1 == null)
        str2 = this.mSearchable.getSuggestIntentAction(); 
      str1 = str2;
      if (str2 == null)
        str1 = "android.intent.action.SEARCH"; 
      String str3 = SuggestionsAdapter.getColumnString(paramCursor, "suggest_intent_data");
      str2 = str3;
      if (str3 == null)
        str2 = this.mSearchable.getSuggestIntentData(); 
      if (str2 != null) {
        str3 = SuggestionsAdapter.getColumnString(paramCursor, "suggest_intent_data_id");
        if (str3 != null) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(str2);
          stringBuilder.append("/");
          stringBuilder.append(Uri.encode(str3));
          str2 = stringBuilder.toString();
        } 
      } 
      if (str2 == null) {
        str2 = null;
      } else {
        uri = Uri.parse(str2);
      } 
      str3 = SuggestionsAdapter.getColumnString(paramCursor, "suggest_intent_query");
      String str4 = SuggestionsAdapter.getColumnString(paramCursor, "suggest_intent_extra_data");
      return createIntent(str1, uri, str4, str3, paramInt, paramString);
    } catch (RuntimeException runtimeException) {
      try {
        paramInt = paramCursor.getPosition();
      } catch (RuntimeException runtimeException1) {
        paramInt = -1;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Search suggestions cursor at row ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" returned exception.");
      Log.w("SearchView", stringBuilder.toString(), runtimeException);
      return null;
    } 
  }
  
  private void forceSuggestionQuery() {
    this.mSearchSrcTextView.doBeforeTextChanged();
    this.mSearchSrcTextView.doAfterTextChanged();
  }
  
  static boolean isLandscapeMode(Context paramContext) {
    boolean bool;
    if ((paramContext.getResources().getConfiguration()).orientation == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class UpdatableTouchDelegate extends TouchDelegate {
    private final Rect mActualBounds;
    
    private boolean mDelegateTargeted;
    
    private final View mDelegateView;
    
    private final int mSlop;
    
    private final Rect mSlopBounds;
    
    private final Rect mTargetBounds;
    
    public UpdatableTouchDelegate(SearchView this$0, Rect param1Rect1, View param1View) {
      super((Rect)this$0, param1View);
      this.mSlop = ViewConfiguration.get(param1View.getContext()).getScaledTouchSlop();
      this.mTargetBounds = new Rect();
      this.mSlopBounds = new Rect();
      this.mActualBounds = new Rect();
      setBounds((Rect)this$0, param1Rect1);
      this.mDelegateView = param1View;
    }
    
    public void setBounds(Rect param1Rect1, Rect param1Rect2) {
      this.mTargetBounds.set(param1Rect1);
      this.mSlopBounds.set(param1Rect1);
      param1Rect1 = this.mSlopBounds;
      int i = this.mSlop;
      param1Rect1.inset(-i, -i);
      this.mActualBounds.set(param1Rect2);
    }
    
    public boolean onTouchEvent(MotionEvent param1MotionEvent) {
      int i = (int)param1MotionEvent.getX();
      int j = (int)param1MotionEvent.getY();
      boolean bool1 = false;
      boolean bool = true;
      boolean bool2 = false;
      int k = param1MotionEvent.getAction();
      if (k != 0) {
        if (k != 1 && k != 2) {
          if (k != 3) {
            k = bool;
          } else {
            bool1 = this.mDelegateTargeted;
            this.mDelegateTargeted = false;
            k = bool;
          } 
        } else {
          boolean bool3 = this.mDelegateTargeted;
          bool1 = bool3;
          k = bool;
          if (bool3) {
            bool1 = bool3;
            k = bool;
            if (!this.mSlopBounds.contains(i, j)) {
              k = 0;
              bool1 = bool3;
            } 
          } 
        } 
      } else {
        k = bool;
        if (this.mTargetBounds.contains(i, j)) {
          this.mDelegateTargeted = true;
          bool1 = true;
          k = bool;
        } 
      } 
      if (bool1) {
        if (k != 0 && !this.mActualBounds.contains(i, j)) {
          float f1 = (this.mDelegateView.getWidth() / 2);
          View view = this.mDelegateView;
          float f2 = (view.getHeight() / 2);
          param1MotionEvent.setLocation(f1, f2);
        } else {
          param1MotionEvent.setLocation((i - this.mActualBounds.left), (j - this.mActualBounds.top));
        } 
        bool2 = this.mDelegateView.dispatchTouchEvent(param1MotionEvent);
      } 
      return bool2;
    }
  }
  
  public static class SearchAutoComplete extends AutoCompleteTextView {
    private boolean mHasPendingShowSoftInputRequest;
    
    final Runnable mRunShowSoftInputIfNecessary = new _$$Lambda$SearchView$SearchAutoComplete$qdPU54FiW6QTzCbsg7P4cSs3cJ8(this);
    
    private SearchView mSearchView;
    
    private int mThreshold;
    
    public SearchAutoComplete(Context param1Context) {
      super(param1Context);
      this.mThreshold = getThreshold();
    }
    
    public SearchAutoComplete(Context param1Context, AttributeSet param1AttributeSet) {
      super(param1Context, param1AttributeSet);
      this.mThreshold = getThreshold();
    }
    
    public SearchAutoComplete(Context param1Context, AttributeSet param1AttributeSet, int param1Int) {
      super(param1Context, param1AttributeSet, param1Int);
      this.mThreshold = getThreshold();
    }
    
    public SearchAutoComplete(Context param1Context, AttributeSet param1AttributeSet, int param1Int1, int param1Int2) {
      super(param1Context, param1AttributeSet, param1Int1, param1Int2);
      this.mThreshold = getThreshold();
    }
    
    protected void onFinishInflate() {
      super.onFinishInflate();
      DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
      float f = getSearchViewTextMinWidthDp();
      setMinWidth((int)TypedValue.applyDimension(1, f, displayMetrics));
    }
    
    void setSearchView(SearchView param1SearchView) {
      this.mSearchView = param1SearchView;
    }
    
    public void setThreshold(int param1Int) {
      super.setThreshold(param1Int);
      this.mThreshold = param1Int;
    }
    
    private boolean isEmpty() {
      boolean bool;
      if (TextUtils.getTrimmedLength(getText()) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    protected void replaceText(CharSequence param1CharSequence) {}
    
    public void performCompletion() {}
    
    public void onWindowFocusChanged(boolean param1Boolean) {
      super.onWindowFocusChanged(param1Boolean);
      if (param1Boolean && this.mSearchView.hasFocus() && getVisibility() == 0) {
        this.mHasPendingShowSoftInputRequest = true;
        if (SearchView.isLandscapeMode(getContext()))
          ensureImeVisible(true); 
      } 
    }
    
    protected void onFocusChanged(boolean param1Boolean, int param1Int, Rect param1Rect) {
      super.onFocusChanged(param1Boolean, param1Int, param1Rect);
      this.mSearchView.onTextFocusChanged();
    }
    
    public boolean enoughToFilter() {
      return (this.mThreshold <= 0 || super.enoughToFilter());
    }
    
    public boolean onKeyPreIme(int param1Int, KeyEvent param1KeyEvent) {
      boolean bool = super.onKeyPreIme(param1Int, param1KeyEvent);
      if (bool && param1Int == 4 && 
        param1KeyEvent.getAction() == 1)
        setImeVisibility(false); 
      return bool;
    }
    
    private int getSearchViewTextMinWidthDp() {
      Configuration configuration = getResources().getConfiguration();
      int i = configuration.screenWidthDp;
      int j = configuration.screenHeightDp;
      int k = configuration.orientation;
      if (i >= 960 && j >= 720 && k == 2)
        return 256; 
      if (i >= 600 || (i >= 640 && j >= 480))
        return 192; 
      return 160;
    }
    
    public InputConnection onCreateInputConnection(EditorInfo param1EditorInfo) {
      InputConnection inputConnection = super.onCreateInputConnection(param1EditorInfo);
      if (this.mHasPendingShowSoftInputRequest) {
        removeCallbacks(this.mRunShowSoftInputIfNecessary);
        post(this.mRunShowSoftInputIfNecessary);
      } 
      return inputConnection;
    }
    
    public boolean checkInputConnectionProxy(View param1View) {
      boolean bool;
      if (param1View == this.mSearchView) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private void showSoftInputIfNecessary() {
      if (this.mHasPendingShowSoftInputRequest) {
        InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
        inputMethodManager.showSoftInput(this, 0);
        this.mHasPendingShowSoftInputRequest = false;
      } 
    }
    
    private void setImeVisibility(boolean param1Boolean) {
      InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
      if (!param1Boolean) {
        this.mHasPendingShowSoftInputRequest = false;
        removeCallbacks(this.mRunShowSoftInputIfNecessary);
        inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        return;
      } 
      if (inputMethodManager.isActive(this)) {
        this.mHasPendingShowSoftInputRequest = false;
        removeCallbacks(this.mRunShowSoftInputIfNecessary);
        inputMethodManager.showSoftInput(this, 0);
        return;
      } 
      this.mHasPendingShowSoftInputRequest = true;
    }
  }
  
  class OnCloseListener {
    public abstract boolean onClose();
  }
  
  class OnQueryTextListener {
    public abstract boolean onQueryTextChange(String param1String);
    
    public abstract boolean onQueryTextSubmit(String param1String);
  }
  
  class OnSuggestionListener {
    public abstract boolean onSuggestionClick(int param1Int);
    
    public abstract boolean onSuggestionSelect(int param1Int);
  }
}
