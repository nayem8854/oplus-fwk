package android.widget;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.icu.text.MeasureFormat;
import android.icu.util.Measure;
import android.icu.util.MeasureUnit;
import android.net.Uri;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.View;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Locale;

@RemoteView
public class Chronometer extends TextView {
  private static final int HOUR_IN_SEC = 3600;
  
  private static final int MIN_IN_SEC = 60;
  
  private static final String TAG = "Chronometer";
  
  private long mBase;
  
  private boolean mCountDown;
  
  private String mFormat;
  
  private StringBuilder mFormatBuilder;
  
  private Formatter mFormatter;
  
  private Object[] mFormatterArgs = new Object[1];
  
  private Locale mFormatterLocale;
  
  private boolean mLogged;
  
  private long mNow;
  
  private OnChronometerTickListener mOnChronometerTickListener;
  
  private StringBuilder mRecycle = new StringBuilder(8);
  
  private boolean mRunning;
  
  private boolean mStarted;
  
  private final Runnable mTickRunnable;
  
  private boolean mVisible;
  
  public Chronometer(Context paramContext) {
    this(paramContext, (AttributeSet)null, 0);
  }
  
  public Chronometer(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public Chronometer(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public Chronometer(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mTickRunnable = (Runnable)new Object(this);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Chronometer, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.Chronometer, paramAttributeSet, typedArray, paramInt1, paramInt2);
    setFormat(typedArray.getString(0));
    setCountDown(typedArray.getBoolean(1, false));
    typedArray.recycle();
    init();
  }
  
  private void init() {
    long l = SystemClock.elapsedRealtime();
    updateText(l);
  }
  
  @RemotableViewMethod
  public void setCountDown(boolean paramBoolean) {
    this.mCountDown = paramBoolean;
    updateText(SystemClock.elapsedRealtime());
  }
  
  public boolean isCountDown() {
    return this.mCountDown;
  }
  
  public boolean isTheFinalCountDown() {
    try {
      Context context = getContext();
      Intent intent = new Intent();
      this("android.intent.action.VIEW", Uri.parse("https://youtu.be/9jK-NcRmVcw"));
      intent = intent.addCategory("android.intent.category.BROWSABLE");
      intent = intent.addFlags(528384);
      context.startActivity(intent);
      return true;
    } catch (Exception exception) {
      return false;
    } 
  }
  
  @RemotableViewMethod
  public void setBase(long paramLong) {
    this.mBase = paramLong;
    dispatchChronometerTick();
    updateText(SystemClock.elapsedRealtime());
  }
  
  public long getBase() {
    return this.mBase;
  }
  
  @RemotableViewMethod
  public void setFormat(String paramString) {
    this.mFormat = paramString;
    if (paramString != null && this.mFormatBuilder == null)
      this.mFormatBuilder = new StringBuilder(paramString.length() * 2); 
  }
  
  public String getFormat() {
    return this.mFormat;
  }
  
  public void setOnChronometerTickListener(OnChronometerTickListener paramOnChronometerTickListener) {
    this.mOnChronometerTickListener = paramOnChronometerTickListener;
  }
  
  public OnChronometerTickListener getOnChronometerTickListener() {
    return this.mOnChronometerTickListener;
  }
  
  public void start() {
    this.mStarted = true;
    updateRunning();
  }
  
  public void stop() {
    this.mStarted = false;
    updateRunning();
  }
  
  @RemotableViewMethod
  public void setStarted(boolean paramBoolean) {
    this.mStarted = paramBoolean;
    updateRunning();
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    this.mVisible = false;
    updateRunning();
  }
  
  protected void onWindowVisibilityChanged(int paramInt) {
    boolean bool;
    super.onWindowVisibilityChanged(paramInt);
    if (paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mVisible = bool;
    updateRunning();
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt) {
    super.onVisibilityChanged(paramView, paramInt);
    updateRunning();
  }
  
  private void updateText(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: lload_1
    //   4: putfield mNow : J
    //   7: aload_0
    //   8: getfield mCountDown : Z
    //   11: ifeq -> 24
    //   14: aload_0
    //   15: getfield mBase : J
    //   18: lload_1
    //   19: lsub
    //   20: lstore_1
    //   21: goto -> 31
    //   24: lload_1
    //   25: aload_0
    //   26: getfield mBase : J
    //   29: lsub
    //   30: lstore_1
    //   31: lload_1
    //   32: ldc2_w 1000
    //   35: ldiv
    //   36: lstore_3
    //   37: iconst_0
    //   38: istore #5
    //   40: lload_3
    //   41: lstore_1
    //   42: lload_3
    //   43: lconst_0
    //   44: lcmp
    //   45: ifge -> 54
    //   48: lload_3
    //   49: lneg
    //   50: lstore_1
    //   51: iconst_1
    //   52: istore #5
    //   54: aload_0
    //   55: getfield mRecycle : Ljava/lang/StringBuilder;
    //   58: lload_1
    //   59: invokestatic formatElapsedTime : (Ljava/lang/StringBuilder;J)Ljava/lang/String;
    //   62: astore #6
    //   64: aload #6
    //   66: astore #7
    //   68: iload #5
    //   70: ifeq -> 93
    //   73: aload_0
    //   74: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   77: ldc 17040678
    //   79: iconst_1
    //   80: anewarray java/lang/Object
    //   83: dup
    //   84: iconst_0
    //   85: aload #6
    //   87: aastore
    //   88: invokevirtual getString : (I[Ljava/lang/Object;)Ljava/lang/String;
    //   91: astore #7
    //   93: aload #7
    //   95: astore #6
    //   97: aload_0
    //   98: getfield mFormat : Ljava/lang/String;
    //   101: ifnull -> 262
    //   104: invokestatic getDefault : ()Ljava/util/Locale;
    //   107: astore #6
    //   109: aload_0
    //   110: getfield mFormatter : Ljava/util/Formatter;
    //   113: ifnull -> 128
    //   116: aload #6
    //   118: aload_0
    //   119: getfield mFormatterLocale : Ljava/util/Locale;
    //   122: invokevirtual equals : (Ljava/lang/Object;)Z
    //   125: ifne -> 156
    //   128: aload_0
    //   129: aload #6
    //   131: putfield mFormatterLocale : Ljava/util/Locale;
    //   134: new java/util/Formatter
    //   137: astore #8
    //   139: aload #8
    //   141: aload_0
    //   142: getfield mFormatBuilder : Ljava/lang/StringBuilder;
    //   145: aload #6
    //   147: invokespecial <init> : (Ljava/lang/Appendable;Ljava/util/Locale;)V
    //   150: aload_0
    //   151: aload #8
    //   153: putfield mFormatter : Ljava/util/Formatter;
    //   156: aload_0
    //   157: getfield mFormatBuilder : Ljava/lang/StringBuilder;
    //   160: iconst_0
    //   161: invokevirtual setLength : (I)V
    //   164: aload_0
    //   165: getfield mFormatterArgs : [Ljava/lang/Object;
    //   168: iconst_0
    //   169: aload #7
    //   171: aastore
    //   172: aload_0
    //   173: getfield mFormatter : Ljava/util/Formatter;
    //   176: aload_0
    //   177: getfield mFormat : Ljava/lang/String;
    //   180: aload_0
    //   181: getfield mFormatterArgs : [Ljava/lang/Object;
    //   184: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   187: pop
    //   188: aload_0
    //   189: getfield mFormatBuilder : Ljava/lang/StringBuilder;
    //   192: invokevirtual toString : ()Ljava/lang/String;
    //   195: astore #6
    //   197: goto -> 262
    //   200: astore #6
    //   202: aload #7
    //   204: astore #6
    //   206: aload_0
    //   207: getfield mLogged : Z
    //   210: ifne -> 262
    //   213: new java/lang/StringBuilder
    //   216: astore #6
    //   218: aload #6
    //   220: invokespecial <init> : ()V
    //   223: aload #6
    //   225: ldc_w 'Illegal format string: '
    //   228: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   231: pop
    //   232: aload #6
    //   234: aload_0
    //   235: getfield mFormat : Ljava/lang/String;
    //   238: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   241: pop
    //   242: ldc 'Chronometer'
    //   244: aload #6
    //   246: invokevirtual toString : ()Ljava/lang/String;
    //   249: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   252: pop
    //   253: aload_0
    //   254: iconst_1
    //   255: putfield mLogged : Z
    //   258: aload #7
    //   260: astore #6
    //   262: aload_0
    //   263: aload #6
    //   265: invokevirtual setText : (Ljava/lang/CharSequence;)V
    //   268: aload_0
    //   269: monitorexit
    //   270: return
    //   271: astore #7
    //   273: aload_0
    //   274: monitorexit
    //   275: aload #7
    //   277: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #291	-> 2
    //   #292	-> 7
    //   #293	-> 31
    //   #294	-> 37
    //   #295	-> 40
    //   #296	-> 48
    //   #297	-> 51
    //   #299	-> 54
    //   #300	-> 64
    //   #301	-> 73
    //   #304	-> 93
    //   #305	-> 104
    //   #306	-> 109
    //   #307	-> 128
    //   #308	-> 134
    //   #310	-> 156
    //   #311	-> 164
    //   #313	-> 172
    //   #314	-> 188
    //   #320	-> 197
    //   #315	-> 200
    //   #316	-> 202
    //   #317	-> 213
    //   #318	-> 253
    //   #322	-> 262
    //   #323	-> 268
    //   #290	-> 271
    // Exception table:
    //   from	to	target	type
    //   2	7	271	finally
    //   7	21	271	finally
    //   24	31	271	finally
    //   31	37	271	finally
    //   54	64	271	finally
    //   73	93	271	finally
    //   97	104	271	finally
    //   104	109	271	finally
    //   109	128	271	finally
    //   128	134	271	finally
    //   134	156	271	finally
    //   156	164	271	finally
    //   164	172	271	finally
    //   172	188	200	java/util/IllegalFormatException
    //   172	188	271	finally
    //   188	197	200	java/util/IllegalFormatException
    //   188	197	271	finally
    //   206	213	271	finally
    //   213	253	271	finally
    //   253	258	271	finally
    //   262	268	271	finally
  }
  
  private void updateRunning() {
    boolean bool;
    if (this.mVisible && this.mStarted && isShown()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool != this.mRunning) {
      if (bool) {
        updateText(SystemClock.elapsedRealtime());
        dispatchChronometerTick();
        postDelayed(this.mTickRunnable, 1000L);
      } else {
        removeCallbacks(this.mTickRunnable);
      } 
      this.mRunning = bool;
    } 
  }
  
  void dispatchChronometerTick() {
    OnChronometerTickListener onChronometerTickListener = this.mOnChronometerTickListener;
    if (onChronometerTickListener != null)
      onChronometerTickListener.onChronometerTick(this); 
  }
  
  private static String formatDuration(long paramLong) {
    int i = (int)(paramLong / 1000L);
    int j = i;
    if (i < 0)
      j = -i; 
    int k = 0;
    int m = 0;
    i = j;
    if (j >= 3600) {
      k = j / 3600;
      i = j - k * 3600;
    } 
    j = i;
    if (i >= 60) {
      m = i / 60;
      j = i - m * 60;
    } 
    ArrayList<Measure> arrayList = new ArrayList();
    if (k > 0)
      arrayList.add(new Measure(Integer.valueOf(k), (MeasureUnit)MeasureUnit.HOUR)); 
    if (m > 0)
      arrayList.add(new Measure(Integer.valueOf(m), (MeasureUnit)MeasureUnit.MINUTE)); 
    arrayList.add(new Measure(Integer.valueOf(j), (MeasureUnit)MeasureUnit.SECOND));
    MeasureFormat measureFormat = MeasureFormat.getInstance(Locale.getDefault(), MeasureFormat.FormatWidth.WIDE);
    return 
      measureFormat.formatMeasures(arrayList.<Measure>toArray(new Measure[arrayList.size()]));
  }
  
  public CharSequence getContentDescription() {
    return formatDuration(this.mNow - this.mBase);
  }
  
  public CharSequence getAccessibilityClassName() {
    return Chronometer.class.getName();
  }
  
  class OnChronometerTickListener {
    public abstract void onChronometerTick(Chronometer param1Chronometer);
  }
}
