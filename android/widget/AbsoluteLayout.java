package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;

@RemoteView
@Deprecated
public class AbsoluteLayout extends ViewGroup {
  public AbsoluteLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public AbsoluteLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public AbsoluteLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AbsoluteLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = getChildCount();
    int j = 0;
    int k = 0;
    measureChildren(paramInt1, paramInt2);
    int m;
    for (m = 0; m < i; m++, j = i2, k = i3) {
      View view = getChildAt(m);
      int i2 = j, i3 = k;
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        int i4 = layoutParams.x;
        i3 = view.getMeasuredWidth();
        i2 = layoutParams.y;
        int i5 = view.getMeasuredHeight();
        i3 = Math.max(k, i4 + i3);
        i2 = Math.max(j, i2 + i5);
      } 
    } 
    m = this.mPaddingLeft;
    int i1 = this.mPaddingRight;
    i = this.mPaddingTop;
    int n = this.mPaddingBottom;
    j = Math.max(j + i + n, getSuggestedMinimumHeight());
    k = Math.max(k + m + i1, getSuggestedMinimumWidth());
    paramInt1 = resolveSizeAndState(k, paramInt1, 0);
    paramInt2 = resolveSizeAndState(j, paramInt2, 0);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-2, -2, 0, 0);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt2 = getChildCount();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      View view = getChildAt(paramInt1);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        paramInt4 = this.mPaddingLeft + layoutParams.x;
        int i = this.mPaddingTop + layoutParams.y;
        int j = view.getMeasuredWidth();
        paramInt3 = view.getMeasuredHeight();
        view.layout(paramInt4, i, j + paramInt4, paramInt3 + i);
      } 
    } 
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return new LayoutParams(paramLayoutParams);
  }
  
  public boolean shouldDelayChildPressedState() {
    return false;
  }
  
  class LayoutParams extends ViewGroup.LayoutParams {
    public int x;
    
    public int y;
    
    public LayoutParams(AbsoluteLayout this$0, int param1Int1, int param1Int2, int param1Int3) {
      super(this$0, param1Int1);
      this.x = param1Int2;
      this.y = param1Int3;
    }
    
    public LayoutParams(AbsoluteLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, R.styleable.AbsoluteLayout_Layout);
      this.x = typedArray.getDimensionPixelOffset(0, 0);
      this.y = typedArray.getDimensionPixelOffset(1, 0);
      typedArray.recycle();
    }
    
    public LayoutParams(AbsoluteLayout this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public String debug(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append("Absolute.LayoutParams={width=");
      int i = this.width;
      stringBuilder.append(sizeToString(i));
      stringBuilder.append(", height=");
      stringBuilder.append(sizeToString(this.height));
      stringBuilder.append(" x=");
      stringBuilder.append(this.x);
      stringBuilder.append(" y=");
      stringBuilder.append(this.y);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
}
