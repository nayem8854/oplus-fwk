package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.text.format.Time;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

@RemoteView
public class OplusDateTimeView extends DateTimeView {
  private LocalDateTime mLocalTime;
  
  private long mTimeMillis;
  
  public OplusDateTimeView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusDateTimeView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  @RemotableViewMethod
  public void setTime(long paramLong) {
    this.mTimeMillis = paramLong;
    LocalDateTime localDateTime = toLocalDateTime(paramLong, ZoneId.systemDefault());
    this.mLocalTime = localDateTime.withSecond(0);
    super.setTime(paramLong);
  }
  
  void update() {
    if (this.mLocalTime == null || getVisibility() == 8)
      return; 
    super.update();
    if (isShowRelativeTime())
      updateColorRelativeTime(); 
  }
  
  private void updateColorRelativeTime() {
    int i;
    String str;
    long l1 = System.currentTimeMillis();
    long l2 = Math.abs(l1 - this.mTimeMillis);
    if (l1 >= this.mTimeMillis) {
      i = 1;
    } else {
      i = 0;
    } 
    if (l2 < 60000L)
      return; 
    if (l2 < 3600000L) {
      int j = (int)(l2 / 60000L);
      Resources resources = getContext().getResources();
      if (i) {
        i = 18153482;
      } else {
        i = 18153483;
      } 
      str = resources.getQuantityString(i, j);
      str = String.format(str, new Object[] { Integer.valueOf(j) });
    } else if (l2 < 86400000L) {
      int j = (int)(l2 / 3600000L);
      Resources resources = getContext().getResources();
      if (i != 0) {
        i = 18153478;
      } else {
        i = 18153479;
      } 
      str = resources.getQuantityString(i, j);
      str = String.format(str, new Object[] { Integer.valueOf(j) });
    } else if (l2 < 31449600000L) {
      TimeZone timeZone = TimeZone.getDefault();
      int j = Math.max(Math.abs(dayDistance(timeZone, this.mTimeMillis, l1)), 1);
      Resources resources = getContext().getResources();
      if (i != 0) {
        i = 18153474;
      } else {
        i = 18153475;
      } 
      str = resources.getQuantityString(i, j);
      str = String.format(str, new Object[] { Integer.valueOf(j) });
    } else {
      int j = (int)(l2 / 31449600000L);
      Resources resources = getContext().getResources();
      if (i != 0) {
        i = 18153486;
      } else {
        i = 18153487;
      } 
      str = resources.getQuantityString(i, j);
      str = String.format(str, new Object[] { Integer.valueOf(j) });
    } 
    setText(str);
  }
  
  private static int dayDistance(TimeZone paramTimeZone, long paramLong1, long paramLong2) {
    int i = Time.getJulianDay(paramLong2, (paramTimeZone.getOffset(paramLong2) / 1000));
    int j = Time.getJulianDay(paramLong1, (paramTimeZone.getOffset(paramLong1) / 1000));
    return i - j;
  }
  
  private static LocalDateTime toLocalDateTime(long paramLong, ZoneId paramZoneId) {
    Instant instant = Instant.ofEpochMilli(paramLong);
    return LocalDateTime.ofInstant(instant, paramZoneId);
  }
}
