package android.widget;

import android.content.Context;
import android.os.OplusSystemProperties;
import android.os.Process;
import android.util.AttributeSet;
import android.util.Log;
import android.view.OplusBaseView;
import com.oplus.content.OplusFeatureConfigManager;
import com.oplus.util.OplusTypeCastingHelper;

public abstract class OplusBaseAbsListView extends AdapterView<ListAdapter> {
  static final int EXCEPTION_NUM = 100;
  
  static final int EXCEPTION_TIME_GAP = 50;
  
  protected static final int FLYWHEEL_TIMEOUT_OPLUS = 0;
  
  static final int[] LONG_FORMAT;
  
  private static final String TAG = "OplusBaseAbsListView";
  
  static long constantEndFlingNum;
  
  static boolean isEnableEndFlingProtect = false;
  
  static long lastEndFlingTime = 0L;
  
  protected float mFlingFriction;
  
  private OplusBaseView mOplusBaseView;
  
  protected float mRealmeFlingFriction;
  
  static {
    constantEndFlingNum = 0L;
    LONG_FORMAT = new int[] { 8224 };
  }
  
  public OplusBaseAbsListView(Context paramContext) {
    super(paramContext);
    this.mFlingFriction = 1.06F;
    this.mRealmeFlingFriction = 0.008F;
    this.mOplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, this);
  }
  
  public OplusBaseAbsListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mFlingFriction = 1.06F;
    this.mRealmeFlingFriction = 0.008F;
    this.mOplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, this);
  }
  
  public boolean enableEndFlingProtectIfNeeded() {
    String str = getContext().getPackageName();
    if (str.equals("com.tencent.mm") || str.equals("gavin.example.abslistviewtest")) {
      isEnableEndFlingProtect = true;
      return true;
    } 
    return false;
  }
  
  public void execEndFlingProtectIfNeeded() {
    if (isEnableEndFlingProtect) {
      long l = System.currentTimeMillis();
      if (l - lastEndFlingTime < 50L) {
        long l1 = constantEndFlingNum + 1L;
        if (l1 >= 100L) {
          long[] arrayOfLong = new long[1];
          int i = Process.myPid();
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("/proc/");
          stringBuilder.append(i);
          stringBuilder.append("/oom_adj");
          Process.readProcFile(stringBuilder.toString(), LONG_FORMAT, null, arrayOfLong, null);
          if (arrayOfLong[0] > 1L) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("pid=");
            stringBuilder1.append(i);
            stringBuilder1.append(" killed");
            Log.d("OplusBaseAbsListView", stringBuilder1.toString());
            Process.sendSignal(i, 9);
          } else {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("waiting pid=");
            stringBuilder1.append(i);
            stringBuilder1.append(" to be background");
            Log.d("OplusBaseAbsListView", stringBuilder1.toString());
          } 
        } 
      } else {
        constantEndFlingNum = 0L;
      } 
      lastEndFlingTime = l;
    } 
  }
  
  protected OverScroller getOverScroller() {
    OverScroller overScroller;
    OplusBaseView oplusBaseView = this.mOplusBaseView;
    if (oplusBaseView != null && oplusBaseView.isOplusOSStyle()) {
      overScroller = new SpringOverScroller(this.mContext);
      overScroller.setFlingFriction(this.mFlingFriction);
      if (OplusFeatureConfigManager.getInstance().hasFeature("oplus.software.list_optimize") && 
        OplusSystemProperties.getBoolean("persist.sys.flingopts.enable", false))
        overScroller.setFlingFriction(this.mRealmeFlingFriction); 
    } else {
      overScroller = new OverScroller(this.mContext);
    } 
    return overScroller;
  }
  
  public abstract int getTouchMode();
  
  public abstract void oplusStartSpringback();
  
  public void setOplusFlingFriction(float paramFloat) {
    this.mFlingFriction = paramFloat;
  }
  
  public void setOplusFlingMode(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1)
        throw new IllegalArgumentException("wrong fling argument"); 
    } else {
      setOplusFlingFriction(0.76F);
    } 
  }
  
  public abstract void setTouchMode(int paramInt);
}
