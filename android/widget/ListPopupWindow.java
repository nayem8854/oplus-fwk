package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import com.android.internal.R;
import com.android.internal.view.menu.ShowableListMenu;

public class ListPopupWindow implements ShowableListMenu {
  private int mDropDownHeight = -2;
  
  private int mDropDownWidth = -2;
  
  private int mDropDownWindowLayoutType = 1002;
  
  private boolean mIsAnimatedFromAnchor = true;
  
  private int mDropDownGravity = 0;
  
  private boolean mDropDownAlwaysVisible = false;
  
  private boolean mForceIgnoreOutsideTouch = false;
  
  int mListItemExpandMaximum = Integer.MAX_VALUE;
  
  private int mPromptPosition = 0;
  
  private final ResizePopupRunnable mResizePopupRunnable = new ResizePopupRunnable();
  
  private final PopupTouchInterceptor mTouchInterceptor = new PopupTouchInterceptor();
  
  private final PopupScrollListener mScrollListener = new PopupScrollListener();
  
  private final ListSelectorHider mHideSelector = new ListSelectorHider();
  
  private final Rect mTempRect = new Rect();
  
  private static final boolean DEBUG = false;
  
  private static final int EXPAND_LIST_TIMEOUT = 250;
  
  public static final int INPUT_METHOD_FROM_FOCUSABLE = 0;
  
  public static final int INPUT_METHOD_NEEDED = 1;
  
  public static final int INPUT_METHOD_NOT_NEEDED = 2;
  
  public static final int MATCH_PARENT = -1;
  
  public static final int POSITION_PROMPT_ABOVE = 0;
  
  public static final int POSITION_PROMPT_BELOW = 1;
  
  private static final String TAG = "ListPopupWindow";
  
  public static final int WRAP_CONTENT = -2;
  
  private ListAdapter mAdapter;
  
  private Context mContext;
  
  private View mDropDownAnchorView;
  
  private int mDropDownHorizontalOffset;
  
  private DropDownListView mDropDownList;
  
  private Drawable mDropDownListHighlight;
  
  private int mDropDownVerticalOffset;
  
  private boolean mDropDownVerticalOffsetSet;
  
  private Rect mEpicenterBounds;
  
  private final Handler mHandler;
  
  private AdapterView.OnItemClickListener mItemClickListener;
  
  private AdapterView.OnItemSelectedListener mItemSelectedListener;
  
  private boolean mModal;
  
  private DataSetObserver mObserver;
  
  private boolean mOverlapAnchor;
  
  private boolean mOverlapAnchorSet;
  
  PopupWindow mPopup;
  
  private View mPromptView;
  
  private Runnable mShowDropDownRunnable;
  
  public ListPopupWindow(Context paramContext) {
    this(paramContext, null, 16843519, 0);
  }
  
  public ListPopupWindow(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843519, 0);
  }
  
  public ListPopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ListPopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    this.mContext = paramContext;
    this.mHandler = new Handler(paramContext.getMainLooper());
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ListPopupWindow, paramInt1, paramInt2);
    this.mDropDownHorizontalOffset = typedArray.getDimensionPixelOffset(0, 0);
    int i = typedArray.getDimensionPixelOffset(1, 0);
    if (i != 0)
      this.mDropDownVerticalOffsetSet = true; 
    typedArray.recycle();
    PopupWindow popupWindow = new PopupWindow(paramContext, paramAttributeSet, paramInt1, paramInt2);
    popupWindow.setInputMethodMode(1);
  }
  
  public void setAdapter(ListAdapter paramListAdapter) {
    DataSetObserver dataSetObserver = this.mObserver;
    if (dataSetObserver == null) {
      this.mObserver = new PopupDataSetObserver();
    } else {
      ListAdapter listAdapter = this.mAdapter;
      if (listAdapter != null)
        listAdapter.unregisterDataSetObserver(dataSetObserver); 
    } 
    this.mAdapter = paramListAdapter;
    if (paramListAdapter != null)
      paramListAdapter.registerDataSetObserver(this.mObserver); 
    DropDownListView dropDownListView = this.mDropDownList;
    if (dropDownListView != null)
      dropDownListView.setAdapter(this.mAdapter); 
  }
  
  public void setPromptPosition(int paramInt) {
    this.mPromptPosition = paramInt;
  }
  
  public int getPromptPosition() {
    return this.mPromptPosition;
  }
  
  public void setModal(boolean paramBoolean) {
    this.mModal = paramBoolean;
    this.mPopup.setFocusable(paramBoolean);
  }
  
  public boolean isModal() {
    return this.mModal;
  }
  
  public void setForceIgnoreOutsideTouch(boolean paramBoolean) {
    this.mForceIgnoreOutsideTouch = paramBoolean;
  }
  
  public void setDropDownAlwaysVisible(boolean paramBoolean) {
    this.mDropDownAlwaysVisible = paramBoolean;
  }
  
  public boolean isDropDownAlwaysVisible() {
    return this.mDropDownAlwaysVisible;
  }
  
  public void setSoftInputMode(int paramInt) {
    this.mPopup.setSoftInputMode(paramInt);
  }
  
  public int getSoftInputMode() {
    return this.mPopup.getSoftInputMode();
  }
  
  public void setListSelector(Drawable paramDrawable) {
    this.mDropDownListHighlight = paramDrawable;
  }
  
  public Drawable getBackground() {
    return this.mPopup.getBackground();
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable) {
    this.mPopup.setBackgroundDrawable(paramDrawable);
  }
  
  public void setAnimationStyle(int paramInt) {
    this.mPopup.setAnimationStyle(paramInt);
  }
  
  public int getAnimationStyle() {
    return this.mPopup.getAnimationStyle();
  }
  
  public View getAnchorView() {
    return this.mDropDownAnchorView;
  }
  
  public void setAnchorView(View paramView) {
    this.mDropDownAnchorView = paramView;
  }
  
  public int getHorizontalOffset() {
    return this.mDropDownHorizontalOffset;
  }
  
  public void setHorizontalOffset(int paramInt) {
    this.mDropDownHorizontalOffset = paramInt;
  }
  
  public int getVerticalOffset() {
    if (!this.mDropDownVerticalOffsetSet)
      return 0; 
    return this.mDropDownVerticalOffset;
  }
  
  public void setVerticalOffset(int paramInt) {
    this.mDropDownVerticalOffset = paramInt;
    this.mDropDownVerticalOffsetSet = true;
  }
  
  public void setEpicenterBounds(Rect paramRect) {
    if (paramRect != null) {
      paramRect = new Rect(paramRect);
    } else {
      paramRect = null;
    } 
    this.mEpicenterBounds = paramRect;
  }
  
  public Rect getEpicenterBounds() {
    Rect rect;
    if (this.mEpicenterBounds != null) {
      rect = new Rect(this.mEpicenterBounds);
    } else {
      rect = null;
    } 
    return rect;
  }
  
  public void setDropDownGravity(int paramInt) {
    this.mDropDownGravity = paramInt;
  }
  
  public int getWidth() {
    return this.mDropDownWidth;
  }
  
  public void setWidth(int paramInt) {
    this.mDropDownWidth = paramInt;
  }
  
  public void setContentWidth(int paramInt) {
    Drawable drawable = this.mPopup.getBackground();
    if (drawable != null) {
      drawable.getPadding(this.mTempRect);
      this.mDropDownWidth = this.mTempRect.left + this.mTempRect.right + paramInt;
    } else {
      setWidth(paramInt);
    } 
  }
  
  public int getHeight() {
    return this.mDropDownHeight;
  }
  
  public void setHeight(int paramInt) {
    if (paramInt < 0 && -2 != paramInt && -1 != paramInt)
      if ((this.mContext.getApplicationInfo()).targetSdkVersion < 26) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Negative value ");
        stringBuilder.append(paramInt);
        stringBuilder.append(" passed to ListPopupWindow#setHeight produces undefined results");
        Log.e("ListPopupWindow", stringBuilder.toString());
      } else {
        throw new IllegalArgumentException("Invalid height. Must be a positive value, MATCH_PARENT, or WRAP_CONTENT.");
      }  
    this.mDropDownHeight = paramInt;
  }
  
  public void setWindowLayoutType(int paramInt) {
    this.mDropDownWindowLayoutType = paramInt;
  }
  
  public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener) {
    this.mItemClickListener = paramOnItemClickListener;
  }
  
  public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener paramOnItemSelectedListener) {
    this.mItemSelectedListener = paramOnItemSelectedListener;
  }
  
  public void setPromptView(View paramView) {
    boolean bool = isShowing();
    if (bool)
      removePromptView(); 
    this.mPromptView = paramView;
    if (bool)
      show(); 
  }
  
  public void postShow() {
    this.mHandler.post(this.mShowDropDownRunnable);
  }
  
  public void show() {
    int i = buildDropDown();
    boolean bool1 = isInputMethodNotNeeded();
    this.mPopup.setAllowScrollingAnchorParent(bool1 ^ true);
    this.mPopup.setWindowLayoutType(this.mDropDownWindowLayoutType);
    boolean bool2 = this.mPopup.isShowing();
    boolean bool3 = true, bool4 = true;
    if (bool2) {
      if (!getAnchorView().isAttachedToWindow())
        return; 
      int j = this.mDropDownWidth;
      if (j == -1) {
        j = -1;
      } else if (j == -2) {
        j = getAnchorView().getWidth();
      } else {
        j = this.mDropDownWidth;
      } 
      int k = this.mDropDownHeight;
      if (k == -1) {
        if (!bool1)
          i = -1; 
        if (bool1) {
          PopupWindow popupWindow1 = this.mPopup;
          if (this.mDropDownWidth == -1) {
            k = -1;
          } else {
            k = 0;
          } 
          popupWindow1.setWidth(k);
          this.mPopup.setHeight(0);
        } else {
          PopupWindow popupWindow1 = this.mPopup;
          if (this.mDropDownWidth == -1) {
            k = -1;
          } else {
            k = 0;
          } 
          popupWindow1.setWidth(k);
          this.mPopup.setHeight(-1);
        } 
      } else if (k != -2) {
        i = this.mDropDownHeight;
      } 
      PopupWindow popupWindow = this.mPopup;
      if (!this.mForceIgnoreOutsideTouch && !this.mDropDownAlwaysVisible) {
        bool3 = bool4;
      } else {
        bool3 = false;
      } 
      popupWindow.setOutsideTouchable(bool3);
      popupWindow = this.mPopup;
      View view = getAnchorView();
      k = this.mDropDownHorizontalOffset;
      int m = this.mDropDownVerticalOffset;
      if (j < 0)
        j = -1; 
      if (i < 0)
        i = -1; 
      popupWindow.update(view, k, m, j, i);
      this.mPopup.getContentView().restoreDefaultFocus();
    } else {
      int j = this.mDropDownWidth;
      if (j == -1) {
        j = -1;
      } else if (j == -2) {
        j = getAnchorView().getWidth();
      } else {
        j = this.mDropDownWidth;
      } 
      int k = this.mDropDownHeight;
      if (k == -1) {
        i = -1;
      } else if (k != -2) {
        i = this.mDropDownHeight;
      } 
      this.mPopup.setWidth(j);
      this.mPopup.setHeight(i);
      this.mPopup.setIsClippedToScreen(true);
      PopupWindow popupWindow = this.mPopup;
      if (this.mForceIgnoreOutsideTouch || this.mDropDownAlwaysVisible)
        bool3 = false; 
      popupWindow.setOutsideTouchable(bool3);
      this.mPopup.setTouchInterceptor(this.mTouchInterceptor);
      this.mPopup.setEpicenterBounds(this.mEpicenterBounds);
      if (this.mOverlapAnchorSet)
        this.mPopup.setOverlapAnchor(this.mOverlapAnchor); 
      this.mPopup.showAsDropDown(getAnchorView(), this.mDropDownHorizontalOffset, this.mDropDownVerticalOffset, this.mDropDownGravity);
      this.mDropDownList.setSelection(-1);
      this.mPopup.getContentView().restoreDefaultFocus();
      if (!this.mModal || this.mDropDownList.isInTouchMode())
        clearListSelection(); 
      if (!this.mModal)
        this.mHandler.post(this.mHideSelector); 
    } 
  }
  
  public void dismiss() {
    this.mPopup.dismiss();
    removePromptView();
    this.mPopup.setContentView(null);
    this.mDropDownList = null;
    this.mHandler.removeCallbacks(this.mResizePopupRunnable);
  }
  
  public void dismissImmediate() {
    this.mPopup.setExitTransition(null);
    dismiss();
  }
  
  public void setOnDismissListener(PopupWindow.OnDismissListener paramOnDismissListener) {
    this.mPopup.setOnDismissListener(paramOnDismissListener);
  }
  
  private void removePromptView() {
    View view = this.mPromptView;
    if (view != null) {
      ViewParent viewParent = view.getParent();
      if (viewParent instanceof android.view.ViewGroup) {
        viewParent = viewParent;
        viewParent.removeView(this.mPromptView);
      } 
    } 
  }
  
  public void setInputMethodMode(int paramInt) {
    this.mPopup.setInputMethodMode(paramInt);
  }
  
  public int getInputMethodMode() {
    return this.mPopup.getInputMethodMode();
  }
  
  public void setSelection(int paramInt) {
    DropDownListView dropDownListView = this.mDropDownList;
    if (isShowing() && dropDownListView != null) {
      dropDownListView.setListSelectionHidden(false);
      dropDownListView.setSelection(paramInt);
      if (dropDownListView.getChoiceMode() != 0)
        dropDownListView.setItemChecked(paramInt, true); 
    } 
  }
  
  public void clearListSelection() {
    DropDownListView dropDownListView = this.mDropDownList;
    if (dropDownListView != null) {
      dropDownListView.setListSelectionHidden(true);
      dropDownListView.hideSelector();
      dropDownListView.requestLayout();
    } 
  }
  
  public boolean isShowing() {
    return this.mPopup.isShowing();
  }
  
  public boolean isInputMethodNotNeeded() {
    boolean bool;
    if (this.mPopup.getInputMethodMode() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean performItemClick(int paramInt) {
    if (isShowing()) {
      if (this.mItemClickListener != null) {
        DropDownListView dropDownListView = this.mDropDownList;
        View view = dropDownListView.getChildAt(paramInt - dropDownListView.getFirstVisiblePosition());
        ListAdapter listAdapter = dropDownListView.getAdapter();
        this.mItemClickListener.onItemClick(dropDownListView, view, paramInt, listAdapter.getItemId(paramInt));
      } 
      return true;
    } 
    return false;
  }
  
  public Object getSelectedItem() {
    if (!isShowing())
      return null; 
    return this.mDropDownList.getSelectedItem();
  }
  
  public int getSelectedItemPosition() {
    if (!isShowing())
      return -1; 
    return this.mDropDownList.getSelectedItemPosition();
  }
  
  public long getSelectedItemId() {
    if (!isShowing())
      return Long.MIN_VALUE; 
    return this.mDropDownList.getSelectedItemId();
  }
  
  public View getSelectedView() {
    if (!isShowing())
      return null; 
    return this.mDropDownList.getSelectedView();
  }
  
  public ListView getListView() {
    return this.mDropDownList;
  }
  
  DropDownListView createDropDownListView(Context paramContext, boolean paramBoolean) {
    return new DropDownListView(paramContext, paramBoolean);
  }
  
  void setListItemExpandMax(int paramInt) {
    this.mListItemExpandMaximum = paramInt;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (isShowing())
      if (paramInt != 62) {
        DropDownListView dropDownListView = this.mDropDownList;
        if (dropDownListView.getSelectedItemPosition() >= 0 || 
          !KeyEvent.isConfirmKey(paramInt)) {
          int i = this.mDropDownList.getSelectedItemPosition();
          int j = this.mPopup.isAboveAnchor() ^ true;
          ListAdapter listAdapter = this.mAdapter;
          int k = Integer.MAX_VALUE;
          int m = Integer.MIN_VALUE;
          if (listAdapter != null) {
            boolean bool1 = listAdapter.areAllItemsEnabled();
            if (bool1) {
              m = 0;
            } else {
              m = this.mDropDownList.lookForSelectablePosition(0, true);
            } 
            k = m;
            if (bool1) {
              m = listAdapter.getCount() - 1;
            } else {
              m = this.mDropDownList.lookForSelectablePosition(listAdapter.getCount() - 1, false);
            } 
          } 
          if ((j != 0 && paramInt == 19 && i <= k) || (j == 0 && paramInt == 20 && i >= m)) {
            clearListSelection();
            this.mPopup.setInputMethodMode(1);
            show();
            return true;
          } 
          this.mDropDownList.setListSelectionHidden(false);
          boolean bool = this.mDropDownList.onKeyDown(paramInt, paramKeyEvent);
          if (bool) {
            this.mPopup.setInputMethodMode(2);
            this.mDropDownList.requestFocusFromTouch();
            show();
            if (paramInt == 19 || paramInt == 20 || paramInt == 23 || paramInt == 66 || paramInt == 160)
              return true; 
          } else if (j != 0 && paramInt == 20) {
            if (i == m)
              return true; 
          } else if (j == 0 && paramInt == 19 && i == k) {
            return true;
          } 
        } 
      }  
    return false;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    if (isShowing() && this.mDropDownList.getSelectedItemPosition() >= 0) {
      boolean bool = this.mDropDownList.onKeyUp(paramInt, paramKeyEvent);
      if (bool && KeyEvent.isConfirmKey(paramInt))
        dismiss(); 
      return bool;
    } 
    return false;
  }
  
  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent) {
    if (paramInt == 4 && isShowing()) {
      KeyEvent.DispatcherState dispatcherState;
      View view = this.mDropDownAnchorView;
      if (paramKeyEvent.getAction() == 0 && paramKeyEvent.getRepeatCount() == 0) {
        dispatcherState = view.getKeyDispatcherState();
        if (dispatcherState != null)
          dispatcherState.startTracking(paramKeyEvent, this); 
        return true;
      } 
      if (paramKeyEvent.getAction() == 1) {
        dispatcherState = dispatcherState.getKeyDispatcherState();
        if (dispatcherState != null)
          dispatcherState.handleUpEvent(paramKeyEvent); 
        if (paramKeyEvent.isTracking() && !paramKeyEvent.isCanceled()) {
          dismiss();
          return true;
        } 
      } 
    } 
    return false;
  }
  
  public View.OnTouchListener createDragToOpenListener(View paramView) {
    return (View.OnTouchListener)new Object(this, paramView);
  }
  
  private int buildDropDown() {
    byte b;
    int i = 0, j = 0;
    DropDownListView dropDownListView = this.mDropDownList;
    boolean bool = false;
    if (dropDownListView == null) {
      LinearLayout linearLayout;
      Context context = this.mContext;
      this.mShowDropDownRunnable = (Runnable)new Object(this);
      DropDownListView dropDownListView2 = createDropDownListView(context, this.mModal ^ true);
      Drawable drawable1 = this.mDropDownListHighlight;
      if (drawable1 != null)
        dropDownListView2.setSelector(drawable1); 
      this.mDropDownList.setAdapter(this.mAdapter);
      this.mDropDownList.setOnItemClickListener(this.mItemClickListener);
      this.mDropDownList.setFocusable(true);
      this.mDropDownList.setFocusableInTouchMode(true);
      this.mDropDownList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            final ListPopupWindow this$0;
            
            public void onItemSelected(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
              if (param1Int != -1) {
                param1AdapterView = ListPopupWindow.this.mDropDownList;
                if (param1AdapterView != null)
                  param1AdapterView.setListSelectionHidden(false); 
              } 
            }
            
            public void onNothingSelected(AdapterView<?> param1AdapterView) {}
          });
      this.mDropDownList.setOnScrollListener(this.mScrollListener);
      AdapterView.OnItemSelectedListener onItemSelectedListener = this.mItemSelectedListener;
      if (onItemSelectedListener != null)
        this.mDropDownList.setOnItemSelectedListener(onItemSelectedListener); 
      dropDownListView2 = this.mDropDownList;
      View view1 = this.mPromptView;
      i = j;
      DropDownListView dropDownListView1 = dropDownListView2;
      if (view1 != null) {
        StringBuilder stringBuilder;
        linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(-1, 0, 1.0F);
        i = this.mPromptPosition;
        if (i != 0) {
          if (i != 1) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Invalid hint position ");
            stringBuilder.append(this.mPromptPosition);
            Log.e("ListPopupWindow", stringBuilder.toString());
          } else {
            linearLayout.addView((View)stringBuilder, layoutParams1);
            linearLayout.addView(view1);
          } 
        } else {
          linearLayout.addView(view1);
          linearLayout.addView((View)stringBuilder, layoutParams1);
        } 
        if (this.mDropDownWidth >= 0) {
          i = Integer.MIN_VALUE;
          j = this.mDropDownWidth;
        } else {
          i = 0;
          j = 0;
        } 
        i = View.MeasureSpec.makeMeasureSpec(j, i);
        view1.measure(i, 0);
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams)view1.getLayoutParams();
        j = view1.getMeasuredHeight();
        b = layoutParams2.topMargin;
        i = layoutParams2.bottomMargin;
        i = j + b + i;
      } 
      this.mPopup.setContentView(linearLayout);
    } else {
      View view1 = this.mPromptView;
      if (view1 != null) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)view1.getLayoutParams();
        i = view1.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
      } 
    } 
    Drawable drawable = this.mPopup.getBackground();
    if (drawable != null) {
      drawable.getPadding(this.mTempRect);
      j = this.mTempRect.top + this.mTempRect.bottom;
      b = j;
      if (!this.mDropDownVerticalOffsetSet) {
        this.mDropDownVerticalOffset = -this.mTempRect.top;
        b = j;
      } 
    } else {
      this.mTempRect.setEmpty();
      b = 0;
    } 
    PopupWindow popupWindow = this.mPopup;
    if (popupWindow.getInputMethodMode() == 2)
      bool = true; 
    popupWindow = this.mPopup;
    View view = getAnchorView();
    j = this.mDropDownVerticalOffset;
    int k = popupWindow.getMaxAvailableHeight(view, j, bool);
    if (this.mDropDownAlwaysVisible || this.mDropDownHeight == -1)
      return k + b; 
    j = this.mDropDownWidth;
    if (j != -2) {
      if (j != -1) {
        j = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
      } else {
        Context context = this.mContext;
        j = (context.getResources().getDisplayMetrics()).widthPixels;
        int m = this.mTempRect.left, n = this.mTempRect.right;
        j = View.MeasureSpec.makeMeasureSpec(j - m + n, 1073741824);
      } 
    } else {
      Context context = this.mContext;
      j = (context.getResources().getDisplayMetrics()).widthPixels;
      int m = this.mTempRect.left, n = this.mTempRect.right;
      j = View.MeasureSpec.makeMeasureSpec(j - m + n, -2147483648);
    } 
    k = this.mDropDownList.measureHeightOfChildren(j, 0, -1, k - i, -1);
    j = i;
    if (k > 0) {
      j = this.mDropDownList.getPaddingTop();
      DropDownListView dropDownListView1 = this.mDropDownList;
      int m = dropDownListView1.getPaddingBottom();
      j = i + b + j + m;
    } 
    return k + j;
  }
  
  public void setOverlapAnchor(boolean paramBoolean) {
    this.mOverlapAnchorSet = true;
    this.mOverlapAnchor = paramBoolean;
  }
  
  private class PopupDataSetObserver extends DataSetObserver {
    final ListPopupWindow this$0;
    
    private PopupDataSetObserver() {}
    
    public void onChanged() {
      if (ListPopupWindow.this.isShowing())
        ListPopupWindow.this.show(); 
    }
    
    public void onInvalidated() {
      ListPopupWindow.this.dismiss();
    }
  }
  
  class ListSelectorHider implements Runnable {
    final ListPopupWindow this$0;
    
    private ListSelectorHider() {}
    
    public void run() {
      ListPopupWindow.this.clearListSelection();
    }
  }
  
  class ResizePopupRunnable implements Runnable {
    final ListPopupWindow this$0;
    
    private ResizePopupRunnable() {}
    
    public void run() {
      if (ListPopupWindow.this.mDropDownList != null && ListPopupWindow.this.mDropDownList.isAttachedToWindow()) {
        ListPopupWindow listPopupWindow = ListPopupWindow.this;
        if (listPopupWindow.mDropDownList.getCount() > ListPopupWindow.this.mDropDownList.getChildCount()) {
          listPopupWindow = ListPopupWindow.this;
          if (listPopupWindow.mDropDownList.getChildCount() <= ListPopupWindow.this.mListItemExpandMaximum) {
            ListPopupWindow.this.mPopup.setInputMethodMode(2);
            ListPopupWindow.this.show();
          } 
        } 
      } 
    }
  }
  
  private class PopupTouchInterceptor implements View.OnTouchListener {
    final ListPopupWindow this$0;
    
    private PopupTouchInterceptor() {}
    
    public boolean onTouch(View param1View, MotionEvent param1MotionEvent) {
      int i = param1MotionEvent.getAction();
      int j = (int)param1MotionEvent.getX();
      int k = (int)param1MotionEvent.getY();
      if (i == 0 && ListPopupWindow.this.mPopup != null) {
        PopupWindow popupWindow = ListPopupWindow.this.mPopup;
        if (popupWindow.isShowing() && j >= 0) {
          popupWindow = ListPopupWindow.this.mPopup;
          if (j < popupWindow.getWidth() && k >= 0 && k < ListPopupWindow.this.mPopup.getHeight()) {
            ListPopupWindow.this.mHandler.postDelayed(ListPopupWindow.this.mResizePopupRunnable, 250L);
            return false;
          } 
        } 
      } 
      if (i == 1)
        ListPopupWindow.this.mHandler.removeCallbacks(ListPopupWindow.this.mResizePopupRunnable); 
      return false;
    }
  }
  
  private class PopupScrollListener implements AbsListView.OnScrollListener {
    final ListPopupWindow this$0;
    
    private PopupScrollListener() {}
    
    public void onScroll(AbsListView param1AbsListView, int param1Int1, int param1Int2, int param1Int3) {}
    
    public void onScrollStateChanged(AbsListView param1AbsListView, int param1Int) {
      if (param1Int == 1) {
        ListPopupWindow listPopupWindow = ListPopupWindow.this;
        if (!listPopupWindow.isInputMethodNotNeeded() && ListPopupWindow.this.mPopup.getContentView() != null) {
          ListPopupWindow.this.mHandler.removeCallbacks(ListPopupWindow.this.mResizePopupRunnable);
          ListPopupWindow.this.mResizePopupRunnable.run();
        } 
      } 
    }
  }
}
