package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

public interface IOplusFtHooks extends IOplusCommonFeature {
  public static final IOplusFtHooks DEFAULT = (IOplusFtHooks)new Object();
  
  default IOplusFtHooks getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusFtHooks;
  }
  
  default int getMinOverscrollSize() {
    return 2;
  }
  
  default int getMaxOverscrollSize() {
    return 4;
  }
  
  default Drawable getArrowDrawable(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      resources.getDrawable(17302273, paramContext.getTheme());
  }
  
  default Drawable getOverflowDrawable(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      resources.getDrawable(17302271, paramContext.getTheme());
  }
  
  default AnimatedVectorDrawable getToArrowAnim(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      (AnimatedVectorDrawable)resources.getDrawable(17302272, paramContext.getTheme());
  }
  
  default AnimatedVectorDrawable getToOverflowAnim(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      (AnimatedVectorDrawable)resources.getDrawable(17302274, paramContext.getTheme());
  }
  
  default int getFirstItemPaddingStart(Context paramContext, int paramInt) {
    return (int)(paramInt * 1.5D);
  }
  
  default int getLastItemPaddingEnd(Context paramContext, int paramInt) {
    return (int)(paramInt * 1.5D);
  }
  
  default void setOverflowMenuCount(int paramInt) {}
  
  default int calOverflowExtension(int paramInt) {
    return (int)(paramInt * 0.5F);
  }
  
  default int getOverflowButtonRes() {
    return 17367162;
  }
  
  default void setOverflowScrollBarSize(ListView paramListView) {}
  
  default void setConvertViewPosition(int paramInt) {}
  
  default void setConvertViewPadding(View paramView, boolean paramBoolean, int paramInt1, int paramInt2) {}
  
  default void setScrollIndicators(ListView paramListView) {
    paramListView.setScrollIndicators(3);
  }
  
  default int getMenuItemButtonRes() {
    return 17367160;
  }
  
  default int getButtonTextId() {
    return 16909010;
  }
  
  default int getButtonIconId() {
    return 16909008;
  }
  
  default int getContentContainerRes() {
    return 17367159;
  }
  
  default int getFloatingToolBarHeightRes() {
    return 17105186;
  }
}
