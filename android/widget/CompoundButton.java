package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewHierarchyEncoder;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.autofill.AutofillManager;
import android.view.autofill.AutofillValue;
import com.android.internal.R;

public abstract class CompoundButton extends Button implements Checkable {
  private static final String LOG_TAG = CompoundButton.class.getSimpleName();
  
  private ColorStateList mButtonTintList = null;
  
  private BlendMode mButtonBlendMode = null;
  
  private boolean mHasButtonTint = false;
  
  private boolean mHasButtonBlendMode = false;
  
  private boolean mCheckedFromResource = false;
  
  private CharSequence mCustomStateDescription = null;
  
  private static final int[] CHECKED_STATE_SET;
  
  private boolean mBroadcasting;
  
  private Drawable mButtonDrawable;
  
  private boolean mChecked;
  
  private OnCheckedChangeListener mOnCheckedChangeListener;
  
  private OnCheckedChangeListener mOnCheckedChangeWidgetListener;
  
  static {
    CHECKED_STATE_SET = new int[] { 16842912 };
  }
  
  public CompoundButton(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public CompoundButton(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public CompoundButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public CompoundButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CompoundButton, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.CompoundButton, paramAttributeSet, typedArray, paramInt1, paramInt2);
    Drawable drawable = typedArray.getDrawable(1);
    if (drawable != null)
      setButtonDrawable(drawable); 
    if (typedArray.hasValue(3)) {
      this.mButtonBlendMode = Drawable.parseBlendMode(typedArray.getInt(3, -1), this.mButtonBlendMode);
      this.mHasButtonBlendMode = true;
    } 
    if (typedArray.hasValue(2)) {
      this.mButtonTintList = typedArray.getColorStateList(2);
      this.mHasButtonTint = true;
    } 
    boolean bool = typedArray.getBoolean(0, false);
    setChecked(bool);
    this.mCheckedFromResource = true;
    typedArray.recycle();
    applyButtonTint();
  }
  
  public void toggle() {
    setChecked(this.mChecked ^ true);
  }
  
  public boolean performClick() {
    toggle();
    boolean bool = super.performClick();
    if (!bool)
      playSoundEffect(0); 
    return bool;
  }
  
  @ExportedProperty
  public boolean isChecked() {
    return this.mChecked;
  }
  
  protected CharSequence getButtonStateDescription() {
    if (isChecked())
      return getResources().getString(17039815); 
    return getResources().getString(17040700);
  }
  
  public void setStateDescription(CharSequence paramCharSequence) {
    this.mCustomStateDescription = paramCharSequence;
    if (paramCharSequence == null) {
      setDefaultStateDescritption();
    } else {
      super.setStateDescription(paramCharSequence);
    } 
  }
  
  protected void setDefaultStateDescritption() {
    if (this.mCustomStateDescription == null)
      super.setStateDescription(getButtonStateDescription()); 
  }
  
  public void setChecked(boolean paramBoolean) {
    if (this.mChecked != paramBoolean) {
      this.mCheckedFromResource = false;
      this.mChecked = paramBoolean;
      refreshDrawableState();
      if (this.mBroadcasting)
        return; 
      this.mBroadcasting = true;
      OnCheckedChangeListener onCheckedChangeListener = this.mOnCheckedChangeListener;
      if (onCheckedChangeListener != null)
        onCheckedChangeListener.onCheckedChanged(this, this.mChecked); 
      onCheckedChangeListener = this.mOnCheckedChangeWidgetListener;
      if (onCheckedChangeListener != null)
        onCheckedChangeListener.onCheckedChanged(this, this.mChecked); 
      AutofillManager autofillManager = (AutofillManager)this.mContext.getSystemService(AutofillManager.class);
      if (autofillManager != null)
        autofillManager.notifyValueChanged(this); 
      this.mBroadcasting = false;
    } 
    setDefaultStateDescritption();
  }
  
  public void setOnCheckedChangeListener(OnCheckedChangeListener paramOnCheckedChangeListener) {
    this.mOnCheckedChangeListener = paramOnCheckedChangeListener;
  }
  
  void setOnCheckedChangeWidgetListener(OnCheckedChangeListener paramOnCheckedChangeListener) {
    this.mOnCheckedChangeWidgetListener = paramOnCheckedChangeListener;
  }
  
  public void setButtonDrawable(int paramInt) {
    Drawable drawable;
    if (paramInt != 0) {
      drawable = getContext().getDrawable(paramInt);
    } else {
      drawable = null;
    } 
    setButtonDrawable(drawable);
  }
  
  public void setButtonDrawable(Drawable paramDrawable) {
    Drawable drawable = this.mButtonDrawable;
    if (drawable != paramDrawable) {
      if (drawable != null) {
        drawable.setCallback(null);
        unscheduleDrawable(this.mButtonDrawable);
      } 
      this.mButtonDrawable = paramDrawable;
      if (paramDrawable != null) {
        boolean bool;
        paramDrawable.setCallback(this);
        paramDrawable.setLayoutDirection(getLayoutDirection());
        if (paramDrawable.isStateful())
          paramDrawable.setState(getDrawableState()); 
        if (getVisibility() == 0) {
          bool = true;
        } else {
          bool = false;
        } 
        paramDrawable.setVisible(bool, false);
        setMinHeight(paramDrawable.getIntrinsicHeight());
        applyButtonTint();
      } 
    } 
  }
  
  public void onResolveDrawables(int paramInt) {
    super.onResolveDrawables(paramInt);
    Drawable drawable = this.mButtonDrawable;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
  }
  
  public Drawable getButtonDrawable() {
    return this.mButtonDrawable;
  }
  
  public void setButtonTintList(ColorStateList paramColorStateList) {
    this.mButtonTintList = paramColorStateList;
    this.mHasButtonTint = true;
    applyButtonTint();
  }
  
  public ColorStateList getButtonTintList() {
    return this.mButtonTintList;
  }
  
  public void setButtonTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setButtonTintBlendMode((BlendMode)paramMode);
  }
  
  public void setButtonTintBlendMode(BlendMode paramBlendMode) {
    this.mButtonBlendMode = paramBlendMode;
    this.mHasButtonBlendMode = true;
    applyButtonTint();
  }
  
  public PorterDuff.Mode getButtonTintMode() {
    BlendMode blendMode = this.mButtonBlendMode;
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getButtonTintBlendMode() {
    return this.mButtonBlendMode;
  }
  
  private void applyButtonTint() {
    if (this.mButtonDrawable != null && (this.mHasButtonTint || this.mHasButtonBlendMode)) {
      Drawable drawable = this.mButtonDrawable.mutate();
      if (this.mHasButtonTint)
        drawable.setTintList(this.mButtonTintList); 
      if (this.mHasButtonBlendMode)
        this.mButtonDrawable.setTintBlendMode(this.mButtonBlendMode); 
      if (this.mButtonDrawable.isStateful())
        this.mButtonDrawable.setState(getDrawableState()); 
    } 
  }
  
  public CharSequence getAccessibilityClassName() {
    return CompoundButton.class.getName();
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    paramAccessibilityEvent.setChecked(this.mChecked);
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setCheckable(true);
    paramAccessibilityNodeInfo.setChecked(this.mChecked);
  }
  
  public int getCompoundPaddingLeft() {
    int i = super.getCompoundPaddingLeft();
    int j = i;
    if (!isLayoutRtl()) {
      Drawable drawable = this.mButtonDrawable;
      j = i;
      if (drawable != null)
        j = i + drawable.getIntrinsicWidth(); 
    } 
    return j;
  }
  
  public int getCompoundPaddingRight() {
    int i = super.getCompoundPaddingRight();
    int j = i;
    if (isLayoutRtl()) {
      Drawable drawable = this.mButtonDrawable;
      j = i;
      if (drawable != null)
        j = i + drawable.getIntrinsicWidth(); 
    } 
    return j;
  }
  
  public int getHorizontalOffsetForDrawables() {
    boolean bool;
    Drawable drawable = this.mButtonDrawable;
    if (drawable != null) {
      bool = drawable.getIntrinsicWidth();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    Drawable drawable = this.mButtonDrawable;
    if (drawable != null) {
      int i = getGravity() & 0x70;
      int j = drawable.getIntrinsicHeight();
      int k = drawable.getIntrinsicWidth();
      if (i != 16) {
        if (i != 80) {
          i = 0;
        } else {
          i = getHeight() - j;
        } 
      } else {
        i = (getHeight() - j) / 2;
      } 
      int m = i + j;
      if (isLayoutRtl()) {
        j = getWidth() - k;
      } else {
        j = 0;
      } 
      if (isLayoutRtl())
        k = getWidth(); 
      drawable.setBounds(j, i, k, m);
      Drawable drawable1 = getBackground();
      if (drawable1 != null)
        drawable1.setHotspotBounds(j, i, k, m); 
    } 
    super.onDraw(paramCanvas);
    if (drawable != null) {
      int i = this.mScrollX;
      int j = this.mScrollY;
      if (i == 0 && j == 0) {
        drawable.draw(paramCanvas);
      } else {
        paramCanvas.translate(i, j);
        drawable.draw(paramCanvas);
        paramCanvas.translate(-i, -j);
      } 
    } 
  }
  
  protected int[] onCreateDrawableState(int paramInt) {
    int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
    if (isChecked())
      mergeDrawableStates(arrayOfInt, CHECKED_STATE_SET); 
    return arrayOfInt;
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = this.mButtonDrawable;
    if (drawable != null && drawable.isStateful() && 
      drawable.setState(getDrawableState()))
      invalidateDrawable(drawable); 
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Drawable drawable = this.mButtonDrawable;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (super.verifyDrawable(paramDrawable) || paramDrawable == this.mButtonDrawable);
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mButtonDrawable;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
  }
  
  class OnCheckedChangeListener {
    public abstract void onCheckedChanged(CompoundButton param1CompoundButton, boolean param1Boolean);
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState() {}
    
    private SavedState(CompoundButton this$0) {
      this.checked = ((Boolean)this$0.readValue(null)).booleanValue();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeValue(Boolean.valueOf(this.checked));
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CompoundButton.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" checked=");
      stringBuilder.append(this.checked);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    boolean checked;
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    parcelable = new SavedState();
    ((SavedState)parcelable).checked = isChecked();
    return parcelable;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    setChecked(((SavedState)paramParcelable).checked);
    requestLayout();
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("checked", isChecked());
  }
  
  protected void onProvideStructure(ViewStructure paramViewStructure, int paramInt1, int paramInt2) {
    super.onProvideStructure(paramViewStructure, paramInt1, paramInt2);
    if (paramInt1 == 1)
      paramViewStructure.setDataIsSensitive(true ^ this.mCheckedFromResource); 
  }
  
  public void autofill(AutofillValue paramAutofillValue) {
    if (!isEnabled())
      return; 
    if (!paramAutofillValue.isToggle()) {
      String str = LOG_TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramAutofillValue);
      stringBuilder.append(" could not be autofilled into ");
      stringBuilder.append(this);
      Log.w(str, stringBuilder.toString());
      return;
    } 
    setChecked(paramAutofillValue.getToggleValue());
  }
  
  public int getAutofillType() {
    boolean bool;
    if (isEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public AutofillValue getAutofillValue() {
    AutofillValue autofillValue;
    if (isEnabled()) {
      autofillValue = AutofillValue.forToggle(isChecked());
    } else {
      autofillValue = null;
    } 
    return autofillValue;
  }
}
