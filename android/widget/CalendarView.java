package android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.util.AttributeSet;
import android.util.Log;
import com.android.internal.R;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CalendarView extends FrameLayout {
  private static final String DATE_FORMAT = "MM/dd/yyyy";
  
  public CalendarView(Context paramContext) {
    this(paramContext, null);
  }
  
  public CalendarView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843613);
  }
  
  public CalendarView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public CalendarView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CalendarView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.CalendarView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    int i = typedArray.getInt(13, 0);
    typedArray.recycle();
    if (i != 0) {
      if (i == 1) {
        this.mDelegate = new CalendarViewMaterialDelegate(this, paramContext, paramAttributeSet, paramInt1, paramInt2);
      } else {
        throw new IllegalArgumentException("invalid calendarViewMode attribute");
      } 
    } else {
      this.mDelegate = new CalendarViewLegacyDelegate(this, paramContext, paramAttributeSet, paramInt1, paramInt2);
    } 
  }
  
  @Deprecated
  public void setShownWeekCount(int paramInt) {
    this.mDelegate.setShownWeekCount(paramInt);
  }
  
  @Deprecated
  public int getShownWeekCount() {
    return this.mDelegate.getShownWeekCount();
  }
  
  @Deprecated
  public void setSelectedWeekBackgroundColor(int paramInt) {
    this.mDelegate.setSelectedWeekBackgroundColor(paramInt);
  }
  
  @Deprecated
  public int getSelectedWeekBackgroundColor() {
    return this.mDelegate.getSelectedWeekBackgroundColor();
  }
  
  @Deprecated
  public void setFocusedMonthDateColor(int paramInt) {
    this.mDelegate.setFocusedMonthDateColor(paramInt);
  }
  
  @Deprecated
  public int getFocusedMonthDateColor() {
    return this.mDelegate.getFocusedMonthDateColor();
  }
  
  @Deprecated
  public void setUnfocusedMonthDateColor(int paramInt) {
    this.mDelegate.setUnfocusedMonthDateColor(paramInt);
  }
  
  @Deprecated
  public int getUnfocusedMonthDateColor() {
    return this.mDelegate.getUnfocusedMonthDateColor();
  }
  
  @Deprecated
  public void setWeekNumberColor(int paramInt) {
    this.mDelegate.setWeekNumberColor(paramInt);
  }
  
  @Deprecated
  public int getWeekNumberColor() {
    return this.mDelegate.getWeekNumberColor();
  }
  
  @Deprecated
  public void setWeekSeparatorLineColor(int paramInt) {
    this.mDelegate.setWeekSeparatorLineColor(paramInt);
  }
  
  @Deprecated
  public int getWeekSeparatorLineColor() {
    return this.mDelegate.getWeekSeparatorLineColor();
  }
  
  @Deprecated
  public void setSelectedDateVerticalBar(int paramInt) {
    this.mDelegate.setSelectedDateVerticalBar(paramInt);
  }
  
  @Deprecated
  public void setSelectedDateVerticalBar(Drawable paramDrawable) {
    this.mDelegate.setSelectedDateVerticalBar(paramDrawable);
  }
  
  @Deprecated
  public Drawable getSelectedDateVerticalBar() {
    return this.mDelegate.getSelectedDateVerticalBar();
  }
  
  public void setWeekDayTextAppearance(int paramInt) {
    this.mDelegate.setWeekDayTextAppearance(paramInt);
  }
  
  public int getWeekDayTextAppearance() {
    return this.mDelegate.getWeekDayTextAppearance();
  }
  
  public void setDateTextAppearance(int paramInt) {
    this.mDelegate.setDateTextAppearance(paramInt);
  }
  
  public int getDateTextAppearance() {
    return this.mDelegate.getDateTextAppearance();
  }
  
  public long getMinDate() {
    return this.mDelegate.getMinDate();
  }
  
  public void setMinDate(long paramLong) {
    this.mDelegate.setMinDate(paramLong);
  }
  
  public long getMaxDate() {
    return this.mDelegate.getMaxDate();
  }
  
  public void setMaxDate(long paramLong) {
    this.mDelegate.setMaxDate(paramLong);
  }
  
  @Deprecated
  public void setShowWeekNumber(boolean paramBoolean) {
    this.mDelegate.setShowWeekNumber(paramBoolean);
  }
  
  @Deprecated
  public boolean getShowWeekNumber() {
    return this.mDelegate.getShowWeekNumber();
  }
  
  public int getFirstDayOfWeek() {
    return this.mDelegate.getFirstDayOfWeek();
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    this.mDelegate.setFirstDayOfWeek(paramInt);
  }
  
  public void setOnDateChangeListener(OnDateChangeListener paramOnDateChangeListener) {
    this.mDelegate.setOnDateChangeListener(paramOnDateChangeListener);
  }
  
  public long getDate() {
    return this.mDelegate.getDate();
  }
  
  public void setDate(long paramLong) {
    this.mDelegate.setDate(paramLong);
  }
  
  public void setDate(long paramLong, boolean paramBoolean1, boolean paramBoolean2) {
    this.mDelegate.setDate(paramLong, paramBoolean1, paramBoolean2);
  }
  
  public boolean getBoundsForDate(long paramLong, Rect paramRect) {
    return this.mDelegate.getBoundsForDate(paramLong, paramRect);
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    this.mDelegate.onConfigurationChanged(paramConfiguration);
  }
  
  public CharSequence getAccessibilityClassName() {
    return CalendarView.class.getName();
  }
  
  class AbstractCalendarViewDelegate implements CalendarViewDelegate {
    protected static final String DEFAULT_MAX_DATE = "01/01/2100";
    
    protected static final String DEFAULT_MIN_DATE = "01/01/1900";
    
    protected Context mContext;
    
    protected Locale mCurrentLocale;
    
    protected CalendarView mDelegator;
    
    AbstractCalendarViewDelegate(CalendarView this$0, Context param1Context) {
      this.mDelegator = this$0;
      this.mContext = param1Context;
      setCurrentLocale(Locale.getDefault());
    }
    
    protected void setCurrentLocale(Locale param1Locale) {
      if (param1Locale.equals(this.mCurrentLocale))
        return; 
      this.mCurrentLocale = param1Locale;
    }
    
    public void setShownWeekCount(int param1Int) {}
    
    public int getShownWeekCount() {
      return 0;
    }
    
    public void setSelectedWeekBackgroundColor(int param1Int) {}
    
    public int getSelectedWeekBackgroundColor() {
      return 0;
    }
    
    public void setFocusedMonthDateColor(int param1Int) {}
    
    public int getFocusedMonthDateColor() {
      return 0;
    }
    
    public void setUnfocusedMonthDateColor(int param1Int) {}
    
    public int getUnfocusedMonthDateColor() {
      return 0;
    }
    
    public void setWeekNumberColor(int param1Int) {}
    
    public int getWeekNumberColor() {
      return 0;
    }
    
    public void setWeekSeparatorLineColor(int param1Int) {}
    
    public int getWeekSeparatorLineColor() {
      return 0;
    }
    
    public void setSelectedDateVerticalBar(int param1Int) {}
    
    public void setSelectedDateVerticalBar(Drawable param1Drawable) {}
    
    public Drawable getSelectedDateVerticalBar() {
      return null;
    }
    
    public void setShowWeekNumber(boolean param1Boolean) {}
    
    public boolean getShowWeekNumber() {
      return false;
    }
    
    public void onConfigurationChanged(Configuration param1Configuration) {}
  }
  
  private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("MM/dd/yyyy");
  
  private static final String LOG_TAG = "CalendarView";
  
  private static final int MODE_HOLO = 0;
  
  private static final int MODE_MATERIAL = 1;
  
  private final CalendarViewDelegate mDelegate;
  
  public static boolean parseDate(String paramString, Calendar paramCalendar) {
    if (paramString == null || paramString.isEmpty())
      return false; 
    try {
      Date date = DATE_FORMATTER.parse(paramString);
      paramCalendar.setTime(date);
      return true;
    } catch (ParseException parseException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Date: ");
      stringBuilder.append(paramString);
      stringBuilder.append(" not in format: ");
      stringBuilder.append("MM/dd/yyyy");
      Log.w("CalendarView", stringBuilder.toString());
      return false;
    } 
  }
  
  class CalendarViewDelegate {
    public abstract boolean getBoundsForDate(long param1Long, Rect param1Rect);
    
    public abstract long getDate();
    
    public abstract int getDateTextAppearance();
    
    public abstract int getFirstDayOfWeek();
    
    public abstract int getFocusedMonthDateColor();
    
    public abstract long getMaxDate();
    
    public abstract long getMinDate();
    
    public abstract Drawable getSelectedDateVerticalBar();
    
    public abstract int getSelectedWeekBackgroundColor();
    
    public abstract boolean getShowWeekNumber();
    
    public abstract int getShownWeekCount();
    
    public abstract int getUnfocusedMonthDateColor();
    
    public abstract int getWeekDayTextAppearance();
    
    public abstract int getWeekNumberColor();
    
    public abstract int getWeekSeparatorLineColor();
    
    public abstract void onConfigurationChanged(Configuration param1Configuration);
    
    public abstract void setDate(long param1Long);
    
    public abstract void setDate(long param1Long, boolean param1Boolean1, boolean param1Boolean2);
    
    public abstract void setDateTextAppearance(int param1Int);
    
    public abstract void setFirstDayOfWeek(int param1Int);
    
    public abstract void setFocusedMonthDateColor(int param1Int);
    
    public abstract void setMaxDate(long param1Long);
    
    public abstract void setMinDate(long param1Long);
    
    public abstract void setOnDateChangeListener(CalendarView.OnDateChangeListener param1OnDateChangeListener);
    
    public abstract void setSelectedDateVerticalBar(int param1Int);
    
    public abstract void setSelectedDateVerticalBar(Drawable param1Drawable);
    
    public abstract void setSelectedWeekBackgroundColor(int param1Int);
    
    public abstract void setShowWeekNumber(boolean param1Boolean);
    
    public abstract void setShownWeekCount(int param1Int);
    
    public abstract void setUnfocusedMonthDateColor(int param1Int);
    
    public abstract void setWeekDayTextAppearance(int param1Int);
    
    public abstract void setWeekNumberColor(int param1Int);
    
    public abstract void setWeekSeparatorLineColor(int param1Int);
  }
  
  class OnDateChangeListener {
    public abstract void onSelectedDayChange(CalendarView param1CalendarView, int param1Int1, int param1Int2, int param1Int3);
  }
}
