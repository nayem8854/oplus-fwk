package android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.transition.Transition;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import com.android.internal.view.menu.ListMenuItemView;
import com.android.internal.view.menu.MenuAdapter;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuItemImpl;

public class MenuPopupWindow extends ListPopupWindow implements MenuItemHoverListener {
  private MenuItemHoverListener mHoverListener;
  
  public MenuPopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  DropDownListView createDropDownListView(Context paramContext, boolean paramBoolean) {
    MenuDropDownListView menuDropDownListView = new MenuDropDownListView(paramContext, paramBoolean);
    menuDropDownListView.setHoverListener(this);
    return menuDropDownListView;
  }
  
  public void setEnterTransition(Transition paramTransition) {
    this.mPopup.setEnterTransition(paramTransition);
  }
  
  public void setExitTransition(Transition paramTransition) {
    this.mPopup.setExitTransition(paramTransition);
  }
  
  public void setHoverListener(MenuItemHoverListener paramMenuItemHoverListener) {
    this.mHoverListener = paramMenuItemHoverListener;
  }
  
  public void setTouchModal(boolean paramBoolean) {
    this.mPopup.setTouchModal(paramBoolean);
  }
  
  public void onItemHoverEnter(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem) {
    MenuItemHoverListener menuItemHoverListener = this.mHoverListener;
    if (menuItemHoverListener != null)
      menuItemHoverListener.onItemHoverEnter(paramMenuBuilder, paramMenuItem); 
  }
  
  public void onItemHoverExit(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem) {
    MenuItemHoverListener menuItemHoverListener = this.mHoverListener;
    if (menuItemHoverListener != null)
      menuItemHoverListener.onItemHoverExit(paramMenuBuilder, paramMenuItem); 
  }
  
  class MenuDropDownListView extends DropDownListView {
    final int mAdvanceKey;
    
    private MenuItemHoverListener mHoverListener;
    
    private MenuItem mHoveredMenuItem;
    
    final int mRetreatKey;
    
    public MenuDropDownListView(MenuPopupWindow this$0, boolean param1Boolean) {
      super((Context)this$0, param1Boolean);
      Resources resources = this$0.getResources();
      Configuration configuration = resources.getConfiguration();
      if (configuration.getLayoutDirection() == 1) {
        this.mAdvanceKey = 21;
        this.mRetreatKey = 22;
      } else {
        this.mAdvanceKey = 22;
        this.mRetreatKey = 21;
      } 
    }
    
    public void setHoverListener(MenuItemHoverListener param1MenuItemHoverListener) {
      this.mHoverListener = param1MenuItemHoverListener;
    }
    
    public void clearSelection() {
      setSelectedPositionInt(-1);
      setNextSelectedPositionInt(-1);
    }
    
    public boolean onKeyDown(int param1Int, KeyEvent param1KeyEvent) {
      ListMenuItemView listMenuItemView = (ListMenuItemView)getSelectedView();
      if (listMenuItemView != null && param1Int == this.mAdvanceKey) {
        if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
          param1Int = getSelectedItemPosition();
          long l = getSelectedItemId();
          performItemClick((View)listMenuItemView, param1Int, l);
        } 
        return true;
      } 
      if (listMenuItemView != null && param1Int == this.mRetreatKey) {
        setSelectedPositionInt(-1);
        setNextSelectedPositionInt(-1);
        ((MenuAdapter)getAdapter()).getAdapterMenu().close(false);
        return true;
      } 
      return super.onKeyDown(param1Int, param1KeyEvent);
    }
    
    public boolean onHoverEvent(MotionEvent param1MotionEvent) {
      if (this.mHoverListener != null) {
        MenuAdapter menuAdapter;
        int i;
        ListAdapter listAdapter = getAdapter();
        if (listAdapter instanceof HeaderViewListAdapter) {
          listAdapter = listAdapter;
          i = listAdapter.getHeadersCount();
          menuAdapter = (MenuAdapter)listAdapter.getWrappedAdapter();
        } else {
          i = 0;
          menuAdapter = menuAdapter;
        } 
        MenuItemImpl menuItemImpl1 = null;
        MenuItemImpl menuItemImpl2 = menuItemImpl1;
        if (param1MotionEvent.getAction() != 10) {
          int j = pointToPosition((int)param1MotionEvent.getX(), (int)param1MotionEvent.getY());
          menuItemImpl2 = menuItemImpl1;
          if (j != -1) {
            i = j - i;
            menuItemImpl2 = menuItemImpl1;
            if (i >= 0) {
              menuItemImpl2 = menuItemImpl1;
              if (i < menuAdapter.getCount())
                menuItemImpl2 = menuAdapter.getItem(i); 
            } 
          } 
        } 
        MenuItem menuItem = this.mHoveredMenuItem;
        if (menuItem != menuItemImpl2) {
          MenuBuilder menuBuilder = menuAdapter.getAdapterMenu();
          if (menuItem != null)
            this.mHoverListener.onItemHoverExit(menuBuilder, menuItem); 
          this.mHoveredMenuItem = (MenuItem)menuItemImpl2;
          if (menuItemImpl2 != null)
            this.mHoverListener.onItemHoverEnter(menuBuilder, (MenuItem)menuItemImpl2); 
        } 
      } 
      return super.onHoverEvent(param1MotionEvent);
    }
  }
}
