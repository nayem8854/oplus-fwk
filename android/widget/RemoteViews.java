package android.widget;

import android.app.ActivityOptions;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.appwidget.AppWidgetHostView;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.OplusPackageManager;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.graphics.drawable.RippleDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.IntArray;
import android.util.Log;
import android.util.Pair;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import com.android.internal.util.ContrastColorUtil;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.invoke.MethodHandle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public class RemoteViews implements Parcelable, LayoutInflater.Filter {
  private static final LayoutInflater.Filter INFLATER_FILTER = (LayoutInflater.Filter)_$$Lambda$RemoteViews$FAOkoZgPKPkiYdtkDxAhkeoykww.INSTANCE;
  
  private int mLightBackgroundLayoutId = 0;
  
  private boolean mIsRoot = true;
  
  private RemoteViews mLandscape = null;
  
  private RemoteViews mPortrait = null;
  
  private int mApplyFlags = 0;
  
  private static final Action ACTION_NOOP;
  
  private static final int BITMAP_REFLECTION_ACTION_TAG = 12;
  
  public static final Parcelable.Creator<RemoteViews> CREATOR;
  
  private static final OnClickHandler DEFAULT_ON_CLICK_HANDLER;
  
  static final String EXTRA_REMOTEADAPTER_APPWIDGET_ID = "remoteAdapterAppWidgetId";
  
  static final String EXTRA_REMOTEADAPTER_ON_LIGHT_BACKGROUND = "remoteAdapterOnLightBackground";
  
  public static final String EXTRA_SHARED_ELEMENT_BOUNDS = "android.widget.extra.SHARED_ELEMENT_BOUNDS";
  
  public static final int FLAG_REAPPLY_DISALLOWED = 1;
  
  public static final int FLAG_USE_LIGHT_BACKGROUND_LAYOUT = 4;
  
  public static final int FLAG_WIDGET_IS_COLLECTION_CHILD = 2;
  
  private static final int LAYOUT_PARAM_ACTION_TAG = 19;
  
  private static final String LOG_TAG = "RemoteViews";
  
  private static final int MAX_NESTED_VIEWS = 10;
  
  private static final int MODE_HAS_LANDSCAPE_AND_PORTRAIT = 1;
  
  private static final int MODE_NORMAL = 0;
  
  private static final int OVERRIDE_TEXT_COLORS_TAG = 20;
  
  private static final int REFLECTION_ACTION_TAG = 2;
  
  private static final int SET_DRAWABLE_TINT_TAG = 3;
  
  private static final int SET_EMPTY_VIEW_ACTION_TAG = 6;
  
  private static final int SET_INT_TAG_TAG = 22;
  
  private static final int SET_ON_CLICK_RESPONSE_TAG = 1;
  
  private static final int SET_PENDING_INTENT_TEMPLATE_TAG = 8;
  
  private static final int SET_REMOTE_INPUTS_ACTION_TAG = 18;
  
  private static final int SET_REMOTE_VIEW_ADAPTER_INTENT_TAG = 10;
  
  private static final int SET_REMOTE_VIEW_ADAPTER_LIST_TAG = 15;
  
  private static final int SET_RIPPLE_DRAWABLE_COLOR_TAG = 21;
  
  private static final int TEXT_VIEW_DRAWABLE_ACTION_TAG = 11;
  
  private static final int TEXT_VIEW_SIZE_ACTION_TAG = 13;
  
  private static final int VIEW_CONTENT_NAVIGATION_TAG = 5;
  
  private static final int VIEW_GROUP_ACTION_ADD_TAG = 4;
  
  private static final int VIEW_GROUP_ACTION_REMOVE_TAG = 7;
  
  private static final int VIEW_PADDING_ACTION_TAG = 14;
  
  private static final MethodKey sLookupKey;
  
  private static final ArrayMap<MethodKey, MethodArgs> sMethods;
  
  private ArrayList<Action> mActions;
  
  public ApplicationInfo mApplication;
  
  private BitmapCache mBitmapCache;
  
  private final Map<Class, Object> mClassCookies;
  
  private final int mLayoutId;
  
  static {
    DEFAULT_ON_CLICK_HANDLER = (OnClickHandler)_$$Lambda$RemoteViews$Ld8XNSMwygf42608Zln_rjTyHy0.INSTANCE;
    sMethods = new ArrayMap<>();
    sLookupKey = new MethodKey();
    ACTION_NOOP = (Action)new Object();
    CREATOR = new Parcelable.Creator<RemoteViews>() {
        public RemoteViews createFromParcel(Parcel param1Parcel) {
          return new RemoteViews(param1Parcel);
        }
        
        public RemoteViews[] newArray(int param1Int) {
          return new RemoteViews[param1Int];
        }
      };
  }
  
  public void setRemoteInputs(int paramInt, RemoteInput[] paramArrayOfRemoteInput) {
    this.mActions.add(new SetRemoteInputsAction(paramInt, paramArrayOfRemoteInput));
  }
  
  public void reduceImageSizes(int paramInt1, int paramInt2) {
    ArrayList<Bitmap> arrayList = this.mBitmapCache.mBitmaps;
    for (byte b = 0; b < arrayList.size(); b++) {
      Bitmap bitmap = arrayList.get(b);
      arrayList.set(b, Icon.scaleDownIfNecessary(bitmap, paramInt1, paramInt2));
    } 
  }
  
  public void overrideTextColors(int paramInt) {
    addAction(new OverrideTextColorsAction(paramInt));
  }
  
  public void setIntTag(int paramInt1, int paramInt2, int paramInt3) {
    addAction(new SetIntTagAction(paramInt1, paramInt2, paramInt3));
  }
  
  public void addFlags(int paramInt) {
    this.mApplyFlags |= paramInt;
  }
  
  public boolean hasFlags(int paramInt) {
    boolean bool;
    if ((this.mApplyFlags & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class MethodKey {
    public String methodName;
    
    public Class paramClass;
    
    public Class targetClass;
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof MethodKey;
      boolean bool1 = false;
      if (!bool)
        return false; 
      MethodKey methodKey = (MethodKey)param1Object;
      if (Objects.equals(methodKey.targetClass, this.targetClass)) {
        param1Object = methodKey.paramClass;
        Class clazz = this.paramClass;
        if (Objects.equals(param1Object, clazz)) {
          param1Object = methodKey.methodName;
          String str = this.methodName;
          if (Objects.equals(param1Object, str))
            bool1 = true; 
        } 
      } 
      return bool1;
    }
    
    public int hashCode() {
      int i = Objects.hashCode(this.targetClass), j = Objects.hashCode(this.paramClass);
      String str = this.methodName;
      int k = Objects.hashCode(str);
      return i ^ j ^ k;
    }
    
    public void set(Class param1Class1, Class param1Class2, String param1String) {
      this.targetClass = param1Class1;
      this.paramClass = param1Class2;
      this.methodName = param1String;
    }
  }
  
  class MethodArgs {
    public MethodHandle asyncMethod;
    
    public String asyncMethodName;
    
    public MethodHandle syncMethod;
  }
  
  class ActionException extends RuntimeException {
    public ActionException(RemoteViews this$0) {
      super((Throwable)this$0);
    }
    
    public ActionException(RemoteViews this$0) {
      super((String)this$0);
    }
    
    public ActionException(RemoteViews this$0) {
      super((Throwable)this$0);
    }
  }
  
  private static abstract class Action implements Parcelable {
    public static final int MERGE_APPEND = 1;
    
    public static final int MERGE_IGNORE = 2;
    
    public static final int MERGE_REPLACE = 0;
    
    int viewId;
    
    private Action() {}
    
    public int describeContents() {
      return 0;
    }
    
    public void setBitmapCache(RemoteViews.BitmapCache param1BitmapCache) {}
    
    public int mergeBehavior() {
      return 0;
    }
    
    public String getUniqueKey() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getActionTag());
      stringBuilder.append("_");
      stringBuilder.append(this.viewId);
      return stringBuilder.toString();
    }
    
    public Action initActionAsync(RemoteViews.ViewTree param1ViewTree, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      return this;
    }
    
    public boolean prefersAsyncApply() {
      return false;
    }
    
    public boolean hasSameAppInfo(ApplicationInfo param1ApplicationInfo) {
      return true;
    }
    
    public void visitUris(Consumer<Uri> param1Consumer) {}
    
    public abstract void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) throws RemoteViews.ActionException;
    
    public abstract int getActionTag();
  }
  
  class RuntimeAction extends Action {
    private RuntimeAction() {}
    
    public final int getActionTag() {
      return 0;
    }
    
    public final void writeToParcel(Parcel param1Parcel, int param1Int) {
      throw new UnsupportedOperationException();
    }
  }
  
  public void mergeRemoteViews(RemoteViews paramRemoteViews) {
    if (paramRemoteViews == null)
      return; 
    RemoteViews remoteViews = new RemoteViews(paramRemoteViews);
    HashMap<Object, Object> hashMap = new HashMap<>();
    if (this.mActions == null)
      this.mActions = new ArrayList<>(); 
    int i = this.mActions.size();
    byte b;
    for (b = 0; b < i; b++) {
      Action action = this.mActions.get(b);
      hashMap.put(action.getUniqueKey(), action);
    } 
    ArrayList<Action> arrayList = remoteViews.mActions;
    if (arrayList == null)
      return; 
    i = arrayList.size();
    for (b = 0; b < i; b++) {
      Action action = arrayList.get(b);
      String str = ((Action)arrayList.get(b)).getUniqueKey();
      int j = ((Action)arrayList.get(b)).mergeBehavior();
      if (hashMap.containsKey(str) && j == 0) {
        this.mActions.remove(hashMap.get(str));
        hashMap.remove(str);
      } 
      if (j == 0 || j == 1)
        this.mActions.add(action); 
    } 
    BitmapCache bitmapCache = new BitmapCache();
    setBitmapCache(bitmapCache);
  }
  
  public void visitUris(Consumer<Uri> paramConsumer) {
    if (this.mActions != null)
      for (byte b = 0; b < this.mActions.size(); b++)
        ((Action)this.mActions.get(b)).visitUris(paramConsumer);  
  }
  
  private static void visitIconUri(Icon paramIcon, Consumer<Uri> paramConsumer) {
    if (paramIcon != null && (paramIcon.getType() == 4 || paramIcon.getType() == 6))
      paramConsumer.accept(paramIcon.getUri()); 
  }
  
  class RemoteViewsContextWrapper extends ContextWrapper {
    private final Context mContextForResources;
    
    RemoteViewsContextWrapper(RemoteViews this$0, Context param1Context1) {
      super((Context)this$0);
      this.mContextForResources = param1Context1;
    }
    
    public Resources getResources() {
      return this.mContextForResources.getResources();
    }
    
    public Resources.Theme getTheme() {
      return this.mContextForResources.getTheme();
    }
    
    public String getPackageName() {
      return this.mContextForResources.getPackageName();
    }
  }
  
  class SetEmptyView extends Action {
    int emptyViewId;
    
    final RemoteViews this$0;
    
    SetEmptyView(int param1Int1, int param1Int2) {
      this.viewId = param1Int1;
      this.emptyViewId = param1Int2;
    }
    
    SetEmptyView(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.emptyViewId = param1Parcel.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.emptyViewId);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1ViewGroup = param1View.findViewById(this.viewId);
      if (!(param1ViewGroup instanceof AdapterView))
        return; 
      param1ViewGroup = param1ViewGroup;
      param1View = param1View.findViewById(this.emptyViewId);
      if (param1View == null)
        return; 
      param1ViewGroup.setEmptyView(param1View);
    }
    
    public int getActionTag() {
      return 6;
    }
  }
  
  class SetPendingIntentTemplate extends Action {
    PendingIntent pendingIntentTemplate;
    
    final RemoteViews this$0;
    
    public SetPendingIntentTemplate(int param1Int, PendingIntent param1PendingIntent) {
      this.viewId = param1Int;
      this.pendingIntentTemplate = param1PendingIntent;
    }
    
    public SetPendingIntentTemplate(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.pendingIntentTemplate = PendingIntent.readPendingIntentOrNullFromParcel(param1Parcel);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      PendingIntent.writePendingIntentOrNullToParcel(this.pendingIntentTemplate, param1Parcel);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, final RemoteViews.OnClickHandler handler) {
      param1View = param1View.findViewById(this.viewId);
      if (param1View == null)
        return; 
      if (param1View instanceof AdapterView) {
        param1View = param1View;
        AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
            final RemoteViews.SetPendingIntentTemplate this$1;
            
            final RemoteViews.OnClickHandler val$handler;
            
            public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
              if (param1View instanceof ViewGroup) {
                Object object;
                ViewGroup viewGroup1 = (ViewGroup)param1View;
                ViewGroup viewGroup2 = viewGroup1;
                if (param1AdapterView instanceof AdapterViewAnimator)
                  viewGroup2 = (ViewGroup)viewGroup1.getChildAt(0); 
                if (viewGroup2 == null)
                  return; 
                viewGroup1 = null;
                int i = viewGroup2.getChildCount();
                param1Int = 0;
                while (true) {
                  object = viewGroup1;
                  if (param1Int < i) {
                    object = viewGroup2.getChildAt(param1Int).getTag(16908968);
                    if (object instanceof RemoteViews.RemoteResponse) {
                      object = object;
                      break;
                    } 
                    param1Int++;
                    continue;
                  } 
                  break;
                } 
                if (object == null)
                  return; 
                object.handleViewClick(param1View, handler);
              } 
            }
          };
        param1View.setOnItemClickListener(onItemClickListener);
        param1View.setTag(this.pendingIntentTemplate);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot setPendingIntentTemplate on a view which is notan AdapterView (id: ");
      stringBuilder.append(this.viewId);
      stringBuilder.append(")");
      Log.e("RemoteViews", stringBuilder.toString());
    }
    
    public int getActionTag() {
      return 8;
    }
  }
  
  class null implements AdapterView.OnItemClickListener {
    final RemoteViews.SetPendingIntentTemplate this$1;
    
    final RemoteViews.OnClickHandler val$handler;
    
    public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      if (param1View instanceof ViewGroup) {
        Object object;
        ViewGroup viewGroup1 = (ViewGroup)param1View;
        ViewGroup viewGroup2 = viewGroup1;
        if (param1AdapterView instanceof AdapterViewAnimator)
          viewGroup2 = (ViewGroup)viewGroup1.getChildAt(0); 
        if (viewGroup2 == null)
          return; 
        viewGroup1 = null;
        int i = viewGroup2.getChildCount();
        param1Int = 0;
        while (true) {
          object = viewGroup1;
          if (param1Int < i) {
            object = viewGroup2.getChildAt(param1Int).getTag(16908968);
            if (object instanceof RemoteViews.RemoteResponse) {
              object = object;
              break;
            } 
            param1Int++;
            continue;
          } 
          break;
        } 
        if (object == null)
          return; 
        object.handleViewClick(param1View, handler);
      } 
    }
  }
  
  class SetRemoteViewsAdapterList extends Action {
    ArrayList<RemoteViews> list;
    
    final RemoteViews this$0;
    
    int viewTypeCount;
    
    public SetRemoteViewsAdapterList(int param1Int1, ArrayList<RemoteViews> param1ArrayList, int param1Int2) {
      this.viewId = param1Int1;
      this.list = param1ArrayList;
      this.viewTypeCount = param1Int2;
    }
    
    public SetRemoteViewsAdapterList(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.viewTypeCount = param1Parcel.readInt();
      this.list = param1Parcel.createTypedArrayList(RemoteViews.CREATOR);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.viewTypeCount);
      param1Parcel.writeTypedList(this.list, param1Int);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      StringBuilder stringBuilder;
      AbsListView absListView;
      param1View = param1View.findViewById(this.viewId);
      if (param1View == null)
        return; 
      if (!(param1ViewGroup instanceof AppWidgetHostView)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("SetRemoteViewsAdapterIntent action can only be used for AppWidgets (root id: ");
        stringBuilder.append(this.viewId);
        stringBuilder.append(")");
        Log.e("RemoteViews", stringBuilder.toString());
        return;
      } 
      if (!(stringBuilder instanceof AbsListView) && !(stringBuilder instanceof AdapterViewAnimator)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot setRemoteViewsAdapter on a view which is not an AbsListView or AdapterViewAnimator (id: ");
        stringBuilder.append(this.viewId);
        stringBuilder.append(")");
        Log.e("RemoteViews", stringBuilder.toString());
        return;
      } 
      if (stringBuilder instanceof AbsListView) {
        absListView = (AbsListView)stringBuilder;
        ListAdapter listAdapter = absListView.getAdapter();
        if (listAdapter instanceof RemoteViewsListAdapter && this.viewTypeCount <= listAdapter.getViewTypeCount()) {
          ((RemoteViewsListAdapter)listAdapter).setViewsList(this.list);
        } else {
          absListView.setAdapter(new RemoteViewsListAdapter(absListView.getContext(), this.list, this.viewTypeCount));
        } 
      } else if (absListView instanceof AdapterViewAnimator) {
        AdapterViewAnimator adapterViewAnimator = (AdapterViewAnimator)absListView;
        Adapter adapter = adapterViewAnimator.getAdapter();
        if (adapter instanceof RemoteViewsListAdapter && this.viewTypeCount <= adapter.getViewTypeCount()) {
          ((RemoteViewsListAdapter)adapter).setViewsList(this.list);
        } else {
          adapterViewAnimator.setAdapter(new RemoteViewsListAdapter(adapterViewAnimator.getContext(), this.list, this.viewTypeCount));
        } 
      } 
    }
    
    public int getActionTag() {
      return 15;
    }
  }
  
  class SetRemoteViewsAdapterIntent extends Action {
    Intent intent;
    
    boolean isAsync;
    
    final RemoteViews this$0;
    
    public SetRemoteViewsAdapterIntent(int param1Int, Intent param1Intent) {
      this.isAsync = false;
      this.viewId = param1Int;
      this.intent = param1Intent;
    }
    
    public SetRemoteViewsAdapterIntent(Parcel param1Parcel) {
      this.isAsync = false;
      this.viewId = param1Parcel.readInt();
      this.intent = (Intent)param1Parcel.readTypedObject(Intent.CREATOR);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeTypedObject((Parcelable)this.intent, param1Int);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      StringBuilder stringBuilder;
      AbsListView absListView;
      param1View = param1View.findViewById(this.viewId);
      if (param1View == null)
        return; 
      if (!(param1ViewGroup instanceof AppWidgetHostView)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("SetRemoteViewsAdapterIntent action can only be used for AppWidgets (root id: ");
        stringBuilder.append(this.viewId);
        stringBuilder.append(")");
        Log.e("RemoteViews", stringBuilder.toString());
        return;
      } 
      if (!(stringBuilder instanceof AbsListView) && !(stringBuilder instanceof AdapterViewAnimator)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot setRemoteViewsAdapter on a view which is not an AbsListView or AdapterViewAnimator (id: ");
        stringBuilder.append(this.viewId);
        stringBuilder.append(")");
        Log.e("RemoteViews", stringBuilder.toString());
        return;
      } 
      AppWidgetHostView appWidgetHostView = (AppWidgetHostView)param1ViewGroup;
      Intent intent = this.intent.putExtra("remoteAdapterAppWidgetId", appWidgetHostView.getAppWidgetId());
      RemoteViews remoteViews = RemoteViews.this;
      boolean bool = remoteViews.hasFlags(4);
      intent.putExtra("remoteAdapterOnLightBackground", bool);
      if (stringBuilder instanceof AbsListView) {
        absListView = (AbsListView)stringBuilder;
        absListView.setRemoteViewsAdapter(this.intent, this.isAsync);
        absListView.setRemoteViewsOnClickHandler(param1OnClickHandler);
      } else if (absListView instanceof AdapterViewAnimator) {
        AdapterViewAnimator adapterViewAnimator = (AdapterViewAnimator)absListView;
        adapterViewAnimator.setRemoteViewsAdapter(this.intent, this.isAsync);
        adapterViewAnimator.setRemoteViewsOnClickHandler(param1OnClickHandler);
      } 
    }
    
    public RemoteViews.Action initActionAsync(RemoteViews.ViewTree param1ViewTree, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      SetRemoteViewsAdapterIntent setRemoteViewsAdapterIntent = new SetRemoteViewsAdapterIntent(this.viewId, this.intent);
      setRemoteViewsAdapterIntent.isAsync = true;
      return setRemoteViewsAdapterIntent;
    }
    
    public int getActionTag() {
      return 10;
    }
  }
  
  class SetOnClickResponse extends Action {
    final RemoteViews.RemoteResponse mResponse;
    
    final RemoteViews this$0;
    
    SetOnClickResponse(int param1Int, RemoteViews.RemoteResponse param1RemoteResponse) {
      this.viewId = param1Int;
      this.mResponse = param1RemoteResponse;
    }
    
    SetOnClickResponse(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      RemoteViews.RemoteResponse remoteResponse = new RemoteViews.RemoteResponse();
      remoteResponse.readFromParcel(param1Parcel);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      this.mResponse.writeToParcel(param1Parcel, param1Int);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      ApplicationInfo applicationInfo;
      param1ViewGroup = param1View.findViewById(this.viewId);
      if (param1ViewGroup == null)
        return; 
      if (this.mResponse.mPendingIntent != null) {
        if (RemoteViews.this.hasFlags(2)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Cannot SetOnClickResponse for collection item (id: ");
          stringBuilder.append(this.viewId);
          stringBuilder.append(")");
          Log.w("RemoteViews", stringBuilder.toString());
          applicationInfo = param1View.getContext().getApplicationInfo();
          if (applicationInfo != null && applicationInfo.targetSdkVersion >= 16)
            return; 
        } 
        param1ViewGroup.setTagInternal(16909282, this.mResponse.mPendingIntent);
      } else if (this.mResponse.mFillIntent != null) {
        if (!RemoteViews.this.hasFlags(2)) {
          Log.e("RemoteViews", "The method setOnClickFillInIntent is available only from RemoteViewsFactory (ie. on collection items).");
          return;
        } 
        if (param1ViewGroup == applicationInfo) {
          param1ViewGroup.setTagInternal(16908968, this.mResponse);
          return;
        } 
      } else {
        param1ViewGroup.setOnClickListener(null);
        return;
      } 
      param1ViewGroup.setOnClickListener(new _$$Lambda$RemoteViews$SetOnClickResponse$9rKnU2QqCzJhBC39ZrKYXob0_MA(this, param1OnClickHandler));
    }
    
    public int getActionTag() {
      return 1;
    }
  }
  
  public static Rect getSourceBounds(View paramView) {
    Resources resources = paramView.getContext().getResources();
    float f = (resources.getCompatibilityInfo()).applicationScale;
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    Rect rect = new Rect();
    rect.left = (int)(arrayOfInt[0] * f + 0.5F);
    rect.top = (int)(arrayOfInt[1] * f + 0.5F);
    rect.right = (int)((arrayOfInt[0] + paramView.getWidth()) * f + 0.5F);
    rect.bottom = (int)((arrayOfInt[1] + paramView.getHeight()) * f + 0.5F);
    return rect;
  }
  
  private MethodHandle getMethod(View paramView, String paramString, Class<?> paramClass, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getClass : ()Ljava/lang/Class;
    //   4: astore #5
    //   6: getstatic android/widget/RemoteViews.sMethods : Landroid/util/ArrayMap;
    //   9: astore #6
    //   11: aload #6
    //   13: monitorenter
    //   14: getstatic android/widget/RemoteViews.sLookupKey : Landroid/widget/RemoteViews$MethodKey;
    //   17: aload #5
    //   19: aload_3
    //   20: aload_2
    //   21: invokevirtual set : (Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)V
    //   24: getstatic android/widget/RemoteViews.sMethods : Landroid/util/ArrayMap;
    //   27: getstatic android/widget/RemoteViews.sLookupKey : Landroid/widget/RemoteViews$MethodKey;
    //   30: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   33: checkcast android/widget/RemoteViews$MethodArgs
    //   36: astore #7
    //   38: aload #7
    //   40: astore_1
    //   41: aload #7
    //   43: ifnonnull -> 301
    //   46: aload_3
    //   47: ifnonnull -> 64
    //   50: aload #5
    //   52: aload_2
    //   53: iconst_0
    //   54: anewarray java/lang/Class
    //   57: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   60: astore_1
    //   61: goto -> 79
    //   64: aload #5
    //   66: aload_2
    //   67: iconst_1
    //   68: anewarray java/lang/Class
    //   71: dup
    //   72: iconst_0
    //   73: aload_3
    //   74: aastore
    //   75: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   78: astore_1
    //   79: aload_1
    //   80: ldc_w android/view/RemotableViewMethod
    //   83: invokevirtual isAnnotationPresent : (Ljava/lang/Class;)Z
    //   86: ifeq -> 163
    //   89: new android/widget/RemoteViews$MethodArgs
    //   92: astore #7
    //   94: aload #7
    //   96: invokespecial <init> : ()V
    //   99: aload #7
    //   101: invokestatic publicLookup : ()Ljava/lang/invoke/MethodHandles$Lookup;
    //   104: aload_1
    //   105: invokevirtual unreflect : (Ljava/lang/reflect/Method;)Ljava/lang/invoke/MethodHandle;
    //   108: putfield syncMethod : Ljava/lang/invoke/MethodHandle;
    //   111: aload #7
    //   113: aload_1
    //   114: ldc_w android/view/RemotableViewMethod
    //   117: invokevirtual getAnnotation : (Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    //   120: checkcast android/view/RemotableViewMethod
    //   123: invokeinterface asyncImpl : ()Ljava/lang/String;
    //   128: putfield asyncMethodName : Ljava/lang/String;
    //   131: new android/widget/RemoteViews$MethodKey
    //   134: astore_1
    //   135: aload_1
    //   136: invokespecial <init> : ()V
    //   139: aload_1
    //   140: aload #5
    //   142: aload_3
    //   143: aload_2
    //   144: invokevirtual set : (Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)V
    //   147: getstatic android/widget/RemoteViews.sMethods : Landroid/util/ArrayMap;
    //   150: aload_1
    //   151: aload #7
    //   153: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   156: pop
    //   157: aload #7
    //   159: astore_1
    //   160: goto -> 301
    //   163: new android/widget/RemoteViews$ActionException
    //   166: astore_1
    //   167: new java/lang/StringBuilder
    //   170: astore #7
    //   172: aload #7
    //   174: invokespecial <init> : ()V
    //   177: aload #7
    //   179: ldc_w 'view: '
    //   182: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   185: pop
    //   186: aload #7
    //   188: aload #5
    //   190: invokevirtual getName : ()Ljava/lang/String;
    //   193: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload #7
    //   199: ldc_w ' can't use method with RemoteViews: '
    //   202: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   205: pop
    //   206: aload #7
    //   208: aload_2
    //   209: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   212: pop
    //   213: aload #7
    //   215: aload_3
    //   216: invokestatic getParameters : (Ljava/lang/Class;)Ljava/lang/String;
    //   219: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   222: pop
    //   223: aload_1
    //   224: aload #7
    //   226: invokevirtual toString : ()Ljava/lang/String;
    //   229: invokespecial <init> : (Ljava/lang/String;)V
    //   232: aload_1
    //   233: athrow
    //   234: astore_1
    //   235: new android/widget/RemoteViews$ActionException
    //   238: astore #7
    //   240: new java/lang/StringBuilder
    //   243: astore_1
    //   244: aload_1
    //   245: invokespecial <init> : ()V
    //   248: aload_1
    //   249: ldc_w 'view: '
    //   252: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   255: pop
    //   256: aload_1
    //   257: aload #5
    //   259: invokevirtual getName : ()Ljava/lang/String;
    //   262: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   265: pop
    //   266: aload_1
    //   267: ldc_w ' doesn't have method: '
    //   270: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   273: pop
    //   274: aload_1
    //   275: aload_2
    //   276: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   279: pop
    //   280: aload_1
    //   281: aload_3
    //   282: invokestatic getParameters : (Ljava/lang/Class;)Ljava/lang/String;
    //   285: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   288: pop
    //   289: aload #7
    //   291: aload_1
    //   292: invokevirtual toString : ()Ljava/lang/String;
    //   295: invokespecial <init> : (Ljava/lang/String;)V
    //   298: aload #7
    //   300: athrow
    //   301: iload #4
    //   303: ifne -> 316
    //   306: aload_1
    //   307: getfield syncMethod : Ljava/lang/invoke/MethodHandle;
    //   310: astore_1
    //   311: aload #6
    //   313: monitorexit
    //   314: aload_1
    //   315: areturn
    //   316: aload_1
    //   317: getfield asyncMethodName : Ljava/lang/String;
    //   320: invokevirtual isEmpty : ()Z
    //   323: ifeq -> 331
    //   326: aload #6
    //   328: monitorexit
    //   329: aconst_null
    //   330: areturn
    //   331: aload_1
    //   332: getfield asyncMethod : Ljava/lang/invoke/MethodHandle;
    //   335: ifnonnull -> 497
    //   338: aload_1
    //   339: getfield syncMethod : Ljava/lang/invoke/MethodHandle;
    //   342: invokevirtual type : ()Ljava/lang/invoke/MethodType;
    //   345: astore_3
    //   346: aload_3
    //   347: iconst_0
    //   348: iconst_1
    //   349: invokevirtual dropParameterTypes : (II)Ljava/lang/invoke/MethodType;
    //   352: ldc_w java/lang/Runnable
    //   355: invokevirtual changeReturnType : (Ljava/lang/Class;)Ljava/lang/invoke/MethodType;
    //   358: astore_3
    //   359: aload_1
    //   360: invokestatic publicLookup : ()Ljava/lang/invoke/MethodHandles$Lookup;
    //   363: aload #5
    //   365: aload_1
    //   366: getfield asyncMethodName : Ljava/lang/String;
    //   369: aload_3
    //   370: invokevirtual findVirtual : (Ljava/lang/Class;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/MethodHandle;
    //   373: putfield asyncMethod : Ljava/lang/invoke/MethodHandle;
    //   376: goto -> 497
    //   379: astore #7
    //   381: new android/widget/RemoteViews$ActionException
    //   384: astore #7
    //   386: new java/lang/StringBuilder
    //   389: astore #5
    //   391: aload #5
    //   393: invokespecial <init> : ()V
    //   396: aload #5
    //   398: ldc_w 'Async implementation declared as '
    //   401: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   404: pop
    //   405: aload #5
    //   407: aload_1
    //   408: getfield asyncMethodName : Ljava/lang/String;
    //   411: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   414: pop
    //   415: aload #5
    //   417: ldc_w ' but not defined for '
    //   420: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   423: pop
    //   424: aload #5
    //   426: aload_2
    //   427: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   430: pop
    //   431: aload #5
    //   433: ldc_w ': public Runnable '
    //   436: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   439: pop
    //   440: aload #5
    //   442: aload_1
    //   443: getfield asyncMethodName : Ljava/lang/String;
    //   446: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   449: pop
    //   450: aload #5
    //   452: ldc_w ' ('
    //   455: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   458: pop
    //   459: aload #5
    //   461: ldc_w ','
    //   464: aload_3
    //   465: invokevirtual parameterArray : ()[Ljava/lang/Class;
    //   468: invokestatic join : (Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;
    //   471: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   474: pop
    //   475: aload #5
    //   477: ldc_w ')'
    //   480: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   483: pop
    //   484: aload #7
    //   486: aload #5
    //   488: invokevirtual toString : ()Ljava/lang/String;
    //   491: invokespecial <init> : (Ljava/lang/String;)V
    //   494: aload #7
    //   496: athrow
    //   497: aload_1
    //   498: getfield asyncMethod : Ljava/lang/invoke/MethodHandle;
    //   501: astore_1
    //   502: aload #6
    //   504: monitorexit
    //   505: aload_1
    //   506: areturn
    //   507: astore_1
    //   508: aload #6
    //   510: monitorexit
    //   511: aload_1
    //   512: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #937	-> 0
    //   #939	-> 6
    //   #941	-> 14
    //   #942	-> 24
    //   #944	-> 38
    //   #947	-> 46
    //   #948	-> 50
    //   #950	-> 64
    //   #952	-> 79
    //   #958	-> 89
    //   #959	-> 99
    //   #960	-> 111
    //   #961	-> 111
    //   #965	-> 131
    //   #967	-> 131
    //   #968	-> 139
    //   #969	-> 147
    //   #953	-> 163
    //   #955	-> 213
    //   #962	-> 234
    //   #963	-> 235
    //   #964	-> 280
    //   #972	-> 301
    //   #973	-> 306
    //   #976	-> 316
    //   #977	-> 326
    //   #980	-> 331
    //   #981	-> 338
    //   #982	-> 346
    //   #984	-> 359
    //   #991	-> 376
    //   #986	-> 379
    //   #987	-> 381
    //   #990	-> 459
    //   #993	-> 497
    //   #994	-> 507
    // Exception table:
    //   from	to	target	type
    //   14	24	507	finally
    //   24	38	507	finally
    //   50	61	234	java/lang/NoSuchMethodException
    //   50	61	234	java/lang/IllegalAccessException
    //   50	61	507	finally
    //   64	79	234	java/lang/NoSuchMethodException
    //   64	79	234	java/lang/IllegalAccessException
    //   64	79	507	finally
    //   79	89	234	java/lang/NoSuchMethodException
    //   79	89	234	java/lang/IllegalAccessException
    //   79	89	507	finally
    //   89	99	234	java/lang/NoSuchMethodException
    //   89	99	234	java/lang/IllegalAccessException
    //   89	99	507	finally
    //   99	111	234	java/lang/NoSuchMethodException
    //   99	111	234	java/lang/IllegalAccessException
    //   99	111	507	finally
    //   111	131	234	java/lang/NoSuchMethodException
    //   111	131	234	java/lang/IllegalAccessException
    //   111	131	507	finally
    //   131	139	507	finally
    //   139	147	507	finally
    //   147	157	507	finally
    //   163	213	234	java/lang/NoSuchMethodException
    //   163	213	234	java/lang/IllegalAccessException
    //   163	213	507	finally
    //   213	234	234	java/lang/NoSuchMethodException
    //   213	234	234	java/lang/IllegalAccessException
    //   213	234	507	finally
    //   235	280	507	finally
    //   280	301	507	finally
    //   306	314	507	finally
    //   316	326	507	finally
    //   326	329	507	finally
    //   331	338	507	finally
    //   338	346	507	finally
    //   346	359	507	finally
    //   359	376	379	java/lang/NoSuchMethodException
    //   359	376	379	java/lang/IllegalAccessException
    //   359	376	507	finally
    //   381	459	507	finally
    //   459	497	507	finally
    //   497	505	507	finally
    //   508	511	507	finally
  }
  
  private static String getParameters(Class<?> paramClass) {
    if (paramClass == null)
      return "()"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("(");
    stringBuilder.append(paramClass);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  class SetDrawableTint extends Action {
    int colorFilter;
    
    PorterDuff.Mode filterMode;
    
    boolean targetBackground;
    
    final RemoteViews this$0;
    
    SetDrawableTint(int param1Int1, boolean param1Boolean, int param1Int2, PorterDuff.Mode param1Mode) {
      this.viewId = param1Int1;
      this.targetBackground = param1Boolean;
      this.colorFilter = param1Int2;
      this.filterMode = param1Mode;
    }
    
    SetDrawableTint(Parcel param1Parcel) {
      boolean bool;
      this.viewId = param1Parcel.readInt();
      if (param1Parcel.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.targetBackground = bool;
      this.colorFilter = param1Parcel.readInt();
      this.filterMode = PorterDuff.intToMode(param1Parcel.readInt());
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.targetBackground);
      param1Parcel.writeInt(this.colorFilter);
      param1Parcel.writeInt(PorterDuff.modeToInt(this.filterMode));
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      Drawable drawable;
      param1ViewGroup = param1View.findViewById(this.viewId);
      if (param1ViewGroup == null)
        return; 
      param1View = null;
      if (this.targetBackground) {
        drawable = param1ViewGroup.getBackground();
      } else if (param1ViewGroup instanceof ImageView) {
        param1View = param1ViewGroup;
        drawable = param1View.getDrawable();
      } 
      if (drawable != null)
        drawable.mutate().setColorFilter(this.colorFilter, this.filterMode); 
    }
    
    public int getActionTag() {
      return 3;
    }
  }
  
  class SetRippleDrawableColor extends Action {
    ColorStateList mColorStateList;
    
    final RemoteViews this$0;
    
    SetRippleDrawableColor(int param1Int, ColorStateList param1ColorStateList) {
      this.viewId = param1Int;
      this.mColorStateList = param1ColorStateList;
    }
    
    SetRippleDrawableColor(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.mColorStateList = (ColorStateList)param1Parcel.readParcelable(null);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeParcelable((Parcelable)this.mColorStateList, 0);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1View = param1View.findViewById(this.viewId);
      if (param1View == null)
        return; 
      Drawable drawable = param1View.getBackground();
      if (drawable instanceof RippleDrawable)
        ((RippleDrawable)drawable.mutate()).setColor(this.mColorStateList); 
    }
    
    public int getActionTag() {
      return 21;
    }
  }
  
  class ViewContentNavigation extends Action {
    final boolean mNext;
    
    final RemoteViews this$0;
    
    ViewContentNavigation(int param1Int, boolean param1Boolean) {
      this.viewId = param1Int;
      this.mNext = param1Boolean;
    }
    
    ViewContentNavigation(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.mNext = param1Parcel.readBoolean();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeBoolean(this.mNext);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1ViewGroup = param1View.findViewById(this.viewId);
      if (param1ViewGroup == null)
        return; 
      try {
        String str;
        RemoteViews remoteViews = RemoteViews.this;
        if (this.mNext) {
          str = "showNext";
        } else {
          str = "showPrevious";
        } 
        MethodHandle methodHandle = remoteViews.getMethod(param1ViewGroup, str, null, false);
        return;
      } finally {
        param1View = null;
      } 
    }
    
    public int mergeBehavior() {
      return 2;
    }
    
    public int getActionTag() {
      return 5;
    }
  }
  
  class BitmapCache {
    int mBitmapMemory = -1;
    
    ArrayList<Bitmap> mBitmaps;
    
    public BitmapCache() {
      this.mBitmaps = new ArrayList<>();
    }
    
    public BitmapCache(RemoteViews this$0) {
      this.mBitmaps = this$0.createTypedArrayList(Bitmap.CREATOR);
    }
    
    public int getBitmapId(Bitmap param1Bitmap) {
      if (param1Bitmap == null)
        return -1; 
      if (this.mBitmaps.contains(param1Bitmap))
        return this.mBitmaps.indexOf(param1Bitmap); 
      this.mBitmaps.add(param1Bitmap);
      this.mBitmapMemory = -1;
      return this.mBitmaps.size() - 1;
    }
    
    public Bitmap getBitmapForId(int param1Int) {
      if (param1Int == -1 || param1Int >= this.mBitmaps.size())
        return null; 
      return this.mBitmaps.get(param1Int);
    }
    
    public void writeBitmapsToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeTypedList(this.mBitmaps, param1Int);
    }
    
    public int getBitmapMemory() {
      if (this.mBitmapMemory < 0) {
        this.mBitmapMemory = 0;
        int i = this.mBitmaps.size();
        for (byte b = 0; b < i; b++)
          this.mBitmapMemory += ((Bitmap)this.mBitmaps.get(b)).getAllocationByteCount(); 
      } 
      return this.mBitmapMemory;
    }
  }
  
  class BitmapReflectionAction extends Action {
    Bitmap bitmap;
    
    int bitmapId;
    
    String methodName;
    
    final RemoteViews this$0;
    
    BitmapReflectionAction(int param1Int, String param1String, Bitmap param1Bitmap) {
      this.bitmap = param1Bitmap;
      this.viewId = param1Int;
      this.methodName = param1String;
      this.bitmapId = RemoteViews.this.mBitmapCache.getBitmapId(param1Bitmap);
    }
    
    BitmapReflectionAction(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.methodName = param1Parcel.readString8();
      this.bitmapId = param1Parcel.readInt();
      this.bitmap = RemoteViews.this.mBitmapCache.getBitmapForId(this.bitmapId);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeString8(this.methodName);
      param1Parcel.writeInt(this.bitmapId);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) throws RemoteViews.ActionException {
      RemoteViews.ReflectionAction reflectionAction = new RemoteViews.ReflectionAction(this.viewId, this.methodName, 12, this.bitmap);
      reflectionAction.apply(param1View, param1ViewGroup, param1OnClickHandler);
    }
    
    public void setBitmapCache(RemoteViews.BitmapCache param1BitmapCache) {
      this.bitmapId = param1BitmapCache.getBitmapId(this.bitmap);
    }
    
    public int getActionTag() {
      return 12;
    }
  }
  
  class ReflectionAction extends Action {
    static final int BITMAP = 12;
    
    static final int BOOLEAN = 1;
    
    static final int BUNDLE = 13;
    
    static final int BYTE = 2;
    
    static final int CHAR = 8;
    
    static final int CHAR_SEQUENCE = 10;
    
    static final int COLOR_STATE_LIST = 15;
    
    static final int DOUBLE = 7;
    
    static final int FLOAT = 6;
    
    static final int ICON = 16;
    
    static final int INT = 4;
    
    static final int INTENT = 14;
    
    static final int LONG = 5;
    
    static final int SHORT = 3;
    
    static final int STRING = 9;
    
    static final int URI = 11;
    
    String methodName;
    
    final RemoteViews this$0;
    
    int type;
    
    Object value;
    
    ReflectionAction(int param1Int1, String param1String, int param1Int2, Object param1Object) {
      this.viewId = param1Int1;
      this.methodName = param1String;
      this.type = param1Int2;
      this.value = param1Object;
    }
    
    ReflectionAction(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.methodName = param1Parcel.readString8();
      int i = param1Parcel.readInt();
      switch (i) {
        default:
          return;
        case 16:
          this.value = param1Parcel.readTypedObject(Icon.CREATOR);
        case 15:
          this.value = param1Parcel.readTypedObject(ColorStateList.CREATOR);
        case 14:
          this.value = param1Parcel.readTypedObject(Intent.CREATOR);
        case 13:
          this.value = param1Parcel.readBundle();
        case 12:
          this.value = param1Parcel.readTypedObject(Bitmap.CREATOR);
        case 11:
          this.value = param1Parcel.readTypedObject(Uri.CREATOR);
        case 10:
          this.value = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        case 9:
          this.value = param1Parcel.readString8();
        case 8:
          this.value = Character.valueOf((char)param1Parcel.readInt());
        case 7:
          this.value = Double.valueOf(param1Parcel.readDouble());
        case 6:
          this.value = Float.valueOf(param1Parcel.readFloat());
        case 5:
          this.value = Long.valueOf(param1Parcel.readLong());
        case 4:
          this.value = Integer.valueOf(param1Parcel.readInt());
        case 3:
          this.value = Short.valueOf((short)param1Parcel.readInt());
        case 2:
          this.value = Byte.valueOf(param1Parcel.readByte());
        case 1:
          break;
      } 
      this.value = Boolean.valueOf(param1Parcel.readBoolean());
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeString8(this.methodName);
      param1Parcel.writeInt(this.type);
      switch (this.type) {
        default:
          return;
        case 13:
          param1Parcel.writeBundle((Bundle)this.value);
        case 11:
        case 12:
        case 14:
        case 15:
        case 16:
          param1Parcel.writeTypedObject((Parcelable)this.value, param1Int);
        case 10:
          TextUtils.writeToParcel((CharSequence)this.value, param1Parcel, param1Int);
        case 9:
          param1Parcel.writeString8((String)this.value);
        case 8:
          param1Parcel.writeInt(((Character)this.value).charValue());
        case 7:
          param1Parcel.writeDouble(((Double)this.value).doubleValue());
        case 6:
          param1Parcel.writeFloat(((Float)this.value).floatValue());
        case 5:
          param1Parcel.writeLong(((Long)this.value).longValue());
        case 4:
          param1Parcel.writeInt(((Integer)this.value).intValue());
        case 3:
          param1Parcel.writeInt(((Short)this.value).shortValue());
        case 2:
          param1Parcel.writeByte(((Byte)this.value).byteValue());
        case 1:
          break;
      } 
      param1Parcel.writeBoolean(((Boolean)this.value).booleanValue());
    }
    
    private Class<?> getParameterType() {
      switch (this.type) {
        default:
          return null;
        case 16:
          return Icon.class;
        case 15:
          return ColorStateList.class;
        case 14:
          return Intent.class;
        case 13:
          return Bundle.class;
        case 12:
          return Bitmap.class;
        case 11:
          return Uri.class;
        case 10:
          return CharSequence.class;
        case 9:
          return String.class;
        case 8:
          return char.class;
        case 7:
          return double.class;
        case 6:
          return float.class;
        case 5:
          return long.class;
        case 4:
          return int.class;
        case 3:
          return short.class;
        case 2:
          return byte.class;
        case 1:
          break;
      } 
      return boolean.class;
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1ViewGroup = param1View.findViewById(this.viewId);
      if (param1ViewGroup == null)
        return; 
      Class<?> clazz = getParameterType();
      if (clazz != null)
        try {
          return;
        } finally {
          clazz = null;
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("bad type: ");
      stringBuilder.append(this.type);
      throw new RemoteViews.ActionException(stringBuilder.toString());
    }
    
    public RemoteViews.Action initActionAsync(RemoteViews.ViewTree param1ViewTree, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1ViewGroup = param1ViewTree.findViewById(this.viewId);
      if (param1ViewGroup == null)
        return RemoteViews.ACTION_NOOP; 
      Class<?> clazz = getParameterType();
      if (clazz != null)
        try {
          MethodHandle methodHandle = RemoteViews.this.getMethod(param1ViewGroup, this.methodName, clazz, true);
          return this;
        } finally {
          param1ViewTree = null;
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("bad type: ");
      stringBuilder.append(this.type);
      throw new RemoteViews.ActionException(stringBuilder.toString());
    }
    
    public int mergeBehavior() {
      if (this.methodName.equals("smoothScrollBy"))
        return 1; 
      return 0;
    }
    
    public int getActionTag() {
      return 2;
    }
    
    public String getUniqueKey() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.getUniqueKey());
      stringBuilder.append(this.methodName);
      stringBuilder.append(this.type);
      return stringBuilder.toString();
    }
    
    public boolean prefersAsyncApply() {
      int i = this.type;
      return (i == 11 || i == 16);
    }
    
    public void visitUris(Consumer<Uri> param1Consumer) {
      int i = this.type;
      if (i != 11) {
        if (i == 16) {
          Icon icon = (Icon)this.value;
          RemoteViews.visitIconUri(icon, param1Consumer);
        } 
      } else {
        Uri uri = (Uri)this.value;
        param1Consumer.accept(uri);
      } 
    }
  }
  
  class RunnableAction extends RuntimeAction {
    private final Runnable mRunnable;
    
    RunnableAction(RemoteViews this$0) {
      this.mRunnable = (Runnable)this$0;
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      this.mRunnable.run();
    }
  }
  
  private void configureRemoteViewsAsChild(RemoteViews paramRemoteViews) {
    paramRemoteViews.setBitmapCache(this.mBitmapCache);
    paramRemoteViews.setNotRoot();
  }
  
  void setNotRoot() {
    this.mIsRoot = false;
  }
  
  class ViewGroupActionAdd extends Action {
    private int mIndex;
    
    private RemoteViews mNestedViews;
    
    final RemoteViews this$0;
    
    ViewGroupActionAdd(int param1Int, RemoteViews param1RemoteViews1) {
      this(param1Int, param1RemoteViews1, -1);
    }
    
    ViewGroupActionAdd(int param1Int1, RemoteViews param1RemoteViews1, int param1Int2) {
      this.viewId = param1Int1;
      this.mNestedViews = param1RemoteViews1;
      this.mIndex = param1Int2;
      if (param1RemoteViews1 != null)
        RemoteViews.this.configureRemoteViewsAsChild(param1RemoteViews1); 
    }
    
    ViewGroupActionAdd(Parcel param1Parcel, RemoteViews.BitmapCache param1BitmapCache, ApplicationInfo param1ApplicationInfo, int param1Int, Map<Class, Object> param1Map) {
      this.viewId = param1Parcel.readInt();
      this.mIndex = param1Parcel.readInt();
      RemoteViews remoteViews = new RemoteViews(param1Parcel, param1BitmapCache, param1ApplicationInfo, param1Int, param1Map);
      remoteViews.addFlags(RemoteViews.this.mApplyFlags);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.mIndex);
      this.mNestedViews.writeToParcel(param1Parcel, param1Int);
    }
    
    public boolean hasSameAppInfo(ApplicationInfo param1ApplicationInfo) {
      return this.mNestedViews.hasSameAppInfo(param1ApplicationInfo);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      Context context = param1View.getContext();
      param1View = param1View.<ViewGroup>findViewById(this.viewId);
      if (param1View == null)
        return; 
      param1View.addView(this.mNestedViews.apply(context, (ViewGroup)param1View, param1OnClickHandler), this.mIndex);
    }
    
    public RemoteViews.Action initActionAsync(RemoteViews.ViewTree param1ViewTree, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1ViewTree.createTree();
      RemoteViews.ViewTree viewTree2 = param1ViewTree.findViewTreeById(this.viewId);
      if (viewTree2 == null || !(viewTree2.mRoot instanceof ViewGroup))
        return RemoteViews.ACTION_NOOP; 
      param1ViewGroup = (ViewGroup)viewTree2.mRoot;
      Context context = param1ViewTree.mRoot.getContext();
      RemoteViews.AsyncApplyTask asyncApplyTask = this.mNestedViews.getAsyncApplyTask(context, param1ViewGroup, null, param1OnClickHandler);
      RemoteViews.ViewTree viewTree1 = asyncApplyTask.doInBackground(new Void[0]);
      if (viewTree1 != null) {
        viewTree2.addChild(viewTree1, this.mIndex);
        return (RemoteViews.Action)new Object(this, asyncApplyTask, viewTree1, param1ViewGroup);
      } 
      throw new RemoteViews.ActionException(asyncApplyTask.mError);
    }
    
    public void setBitmapCache(RemoteViews.BitmapCache param1BitmapCache) {
      this.mNestedViews.setBitmapCache(param1BitmapCache);
    }
    
    public int mergeBehavior() {
      return 1;
    }
    
    public boolean prefersAsyncApply() {
      return this.mNestedViews.prefersAsyncApply();
    }
    
    public int getActionTag() {
      return 4;
    }
  }
  
  class ViewGroupActionRemove extends Action {
    private static final int REMOVE_ALL_VIEWS_ID = -2;
    
    private int mViewIdToKeep;
    
    final RemoteViews this$0;
    
    ViewGroupActionRemove(int param1Int) {
      this(param1Int, -2);
    }
    
    ViewGroupActionRemove(int param1Int1, int param1Int2) {
      this.viewId = param1Int1;
      this.mViewIdToKeep = param1Int2;
    }
    
    ViewGroupActionRemove(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.mViewIdToKeep = param1Parcel.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.mViewIdToKeep);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1View = param1View.<ViewGroup>findViewById(this.viewId);
      if (param1View == null)
        return; 
      if (this.mViewIdToKeep == -2) {
        param1View.removeAllViews();
        return;
      } 
      removeAllViewsExceptIdToKeep((ViewGroup)param1View);
    }
    
    public RemoteViews.Action initActionAsync(RemoteViews.ViewTree param1ViewTree, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1ViewTree.createTree();
      RemoteViews.ViewTree viewTree = param1ViewTree.findViewTreeById(this.viewId);
      if (viewTree == null || !(viewTree.mRoot instanceof ViewGroup))
        return RemoteViews.ACTION_NOOP; 
      ViewGroup viewGroup = (ViewGroup)viewTree.mRoot;
      RemoteViews.ViewTree.access$2002(viewTree, null);
      return (RemoteViews.Action)new Object(this, viewGroup);
    }
    
    private void removeAllViewsExceptIdToKeep(ViewGroup param1ViewGroup) {
      int i = param1ViewGroup.getChildCount() - 1;
      while (i >= 0) {
        if (param1ViewGroup.getChildAt(i).getId() != this.mViewIdToKeep)
          param1ViewGroup.removeViewAt(i); 
        i--;
      } 
    }
    
    public int getActionTag() {
      return 7;
    }
    
    public int mergeBehavior() {
      return 1;
    }
  }
  
  class TextViewDrawableAction extends Action {
    int d1;
    
    int d2;
    
    int d3;
    
    int d4;
    
    boolean drawablesLoaded;
    
    Icon i1;
    
    Icon i2;
    
    Icon i3;
    
    Icon i4;
    
    Drawable id1;
    
    Drawable id2;
    
    Drawable id3;
    
    Drawable id4;
    
    boolean isRelative;
    
    final RemoteViews this$0;
    
    boolean useIcons;
    
    public TextViewDrawableAction(int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3, int param1Int4, int param1Int5) {
      this.isRelative = false;
      this.useIcons = false;
      this.drawablesLoaded = false;
      this.viewId = param1Int1;
      this.isRelative = param1Boolean;
      this.useIcons = false;
      this.d1 = param1Int2;
      this.d2 = param1Int3;
      this.d3 = param1Int4;
      this.d4 = param1Int5;
    }
    
    public TextViewDrawableAction(int param1Int, boolean param1Boolean, Icon param1Icon1, Icon param1Icon2, Icon param1Icon3, Icon param1Icon4) {
      this.isRelative = false;
      this.useIcons = false;
      this.drawablesLoaded = false;
      this.viewId = param1Int;
      this.isRelative = param1Boolean;
      this.useIcons = true;
      this.i1 = param1Icon1;
      this.i2 = param1Icon2;
      this.i3 = param1Icon3;
      this.i4 = param1Icon4;
    }
    
    public TextViewDrawableAction(Parcel param1Parcel) {
      boolean bool1 = false;
      this.isRelative = false;
      this.useIcons = false;
      this.drawablesLoaded = false;
      this.viewId = param1Parcel.readInt();
      if (param1Parcel.readInt() != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.isRelative = bool2;
      boolean bool2 = bool1;
      if (param1Parcel.readInt() != 0)
        bool2 = true; 
      this.useIcons = bool2;
      if (bool2) {
        this.i1 = (Icon)param1Parcel.readTypedObject(Icon.CREATOR);
        this.i2 = (Icon)param1Parcel.readTypedObject(Icon.CREATOR);
        this.i3 = (Icon)param1Parcel.readTypedObject(Icon.CREATOR);
        this.i4 = (Icon)param1Parcel.readTypedObject(Icon.CREATOR);
      } else {
        this.d1 = param1Parcel.readInt();
        this.d2 = param1Parcel.readInt();
        this.d3 = param1Parcel.readInt();
        this.d4 = param1Parcel.readInt();
      } 
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.isRelative);
      param1Parcel.writeInt(this.useIcons);
      if (this.useIcons) {
        param1Parcel.writeTypedObject((Parcelable)this.i1, 0);
        param1Parcel.writeTypedObject((Parcelable)this.i2, 0);
        param1Parcel.writeTypedObject((Parcelable)this.i3, 0);
        param1Parcel.writeTypedObject((Parcelable)this.i4, 0);
      } else {
        param1Parcel.writeInt(this.d1);
        param1Parcel.writeInt(this.d2);
        param1Parcel.writeInt(this.d3);
        param1Parcel.writeInt(this.d4);
      } 
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      TextView textView = param1View.<TextView>findViewById(this.viewId);
      if (textView == null)
        return; 
      if (this.drawablesLoaded) {
        if (this.isRelative) {
          textView.setCompoundDrawablesRelativeWithIntrinsicBounds(this.id1, this.id2, this.id3, this.id4);
        } else {
          textView.setCompoundDrawablesWithIntrinsicBounds(this.id1, this.id2, this.id3, this.id4);
        } 
      } else if (this.useIcons) {
        Drawable drawable1, drawable2, drawable3;
        Context context = textView.getContext();
        Icon icon1 = this.i1;
        Drawable drawable4 = null;
        if (icon1 == null) {
          icon1 = null;
        } else {
          drawable1 = icon1.loadDrawable(context);
        } 
        Icon icon2 = this.i2;
        if (icon2 == null) {
          icon2 = null;
        } else {
          drawable2 = icon2.loadDrawable(context);
        } 
        Icon icon3 = this.i3;
        if (icon3 == null) {
          icon3 = null;
        } else {
          drawable3 = icon3.loadDrawable(context);
        } 
        Icon icon4 = this.i4;
        if (icon4 != null)
          drawable4 = icon4.loadDrawable(context); 
        if (this.isRelative) {
          textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable1, drawable2, drawable3, drawable4);
        } else {
          textView.setCompoundDrawablesWithIntrinsicBounds(drawable1, drawable2, drawable3, drawable4);
        } 
      } else if (this.isRelative) {
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(this.d1, this.d2, this.d3, this.d4);
      } else {
        textView.setCompoundDrawablesWithIntrinsicBounds(this.d1, this.d2, this.d3, this.d4);
      } 
    }
    
    public RemoteViews.Action initActionAsync(RemoteViews.ViewTree param1ViewTree, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      TextViewDrawableAction textViewDrawableAction;
      TextView textView = param1ViewTree.<TextView>findViewById(this.viewId);
      if (textView == null)
        return RemoteViews.ACTION_NOOP; 
      if (this.useIcons) {
        textViewDrawableAction = new TextViewDrawableAction(this.viewId, this.isRelative, this.i1, this.i2, this.i3, this.i4);
      } else {
        textViewDrawableAction = new TextViewDrawableAction(this.viewId, this.isRelative, this.d1, this.d2, this.d3, this.d4);
      } 
      textViewDrawableAction.drawablesLoaded = true;
      Context context = textView.getContext();
      boolean bool = this.useIcons;
      Drawable drawable = null;
      param1OnClickHandler = null;
      if (bool) {
        Drawable drawable4, drawable3, drawable2;
        RemoteViews.OnClickHandler onClickHandler;
        Drawable drawable1;
        Icon icon4 = this.i1;
        if (icon4 == null) {
          icon4 = null;
        } else {
          drawable4 = icon4.loadDrawable(context);
        } 
        textViewDrawableAction.id1 = drawable4;
        Icon icon3 = this.i2;
        if (icon3 == null) {
          icon3 = null;
        } else {
          drawable3 = icon3.loadDrawable(context);
        } 
        textViewDrawableAction.id2 = drawable3;
        Icon icon2 = this.i3;
        if (icon2 == null) {
          icon2 = null;
        } else {
          drawable2 = icon2.loadDrawable(context);
        } 
        textViewDrawableAction.id3 = drawable2;
        Icon icon1 = this.i4;
        if (icon1 == null) {
          onClickHandler = param1OnClickHandler;
        } else {
          drawable1 = onClickHandler.loadDrawable(context);
        } 
        textViewDrawableAction.id4 = drawable1;
      } else {
        Drawable drawable1;
        int i = this.d1;
        if (i == 0) {
          textView = null;
        } else {
          drawable1 = context.getDrawable(i);
        } 
        textViewDrawableAction.id1 = drawable1;
        i = this.d2;
        if (i == 0) {
          drawable1 = null;
        } else {
          drawable1 = context.getDrawable(i);
        } 
        textViewDrawableAction.id2 = drawable1;
        i = this.d3;
        if (i == 0) {
          drawable1 = null;
        } else {
          drawable1 = context.getDrawable(i);
        } 
        textViewDrawableAction.id3 = drawable1;
        i = this.d4;
        if (i == 0) {
          drawable1 = drawable;
        } else {
          drawable1 = context.getDrawable(i);
        } 
        textViewDrawableAction.id4 = drawable1;
      } 
      return textViewDrawableAction;
    }
    
    public boolean prefersAsyncApply() {
      return this.useIcons;
    }
    
    public int getActionTag() {
      return 11;
    }
    
    public void visitUris(Consumer<Uri> param1Consumer) {
      if (this.useIcons) {
        RemoteViews.visitIconUri(this.i1, param1Consumer);
        RemoteViews.visitIconUri(this.i2, param1Consumer);
        RemoteViews.visitIconUri(this.i3, param1Consumer);
        RemoteViews.visitIconUri(this.i4, param1Consumer);
      } 
    }
  }
  
  class TextViewSizeAction extends Action {
    float size;
    
    final RemoteViews this$0;
    
    int units;
    
    public TextViewSizeAction(int param1Int1, int param1Int2, float param1Float) {
      this.viewId = param1Int1;
      this.units = param1Int2;
      this.size = param1Float;
    }
    
    public TextViewSizeAction(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.units = param1Parcel.readInt();
      this.size = param1Parcel.readFloat();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.units);
      param1Parcel.writeFloat(this.size);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1View = param1View.<TextView>findViewById(this.viewId);
      if (param1View == null)
        return; 
      param1View.setTextSize(this.units, this.size);
    }
    
    public int getActionTag() {
      return 13;
    }
  }
  
  class ViewPaddingAction extends Action {
    int bottom;
    
    int left;
    
    int right;
    
    final RemoteViews this$0;
    
    int top;
    
    public ViewPaddingAction(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) {
      this.viewId = param1Int1;
      this.left = param1Int2;
      this.top = param1Int3;
      this.right = param1Int4;
      this.bottom = param1Int5;
    }
    
    public ViewPaddingAction(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.left = param1Parcel.readInt();
      this.top = param1Parcel.readInt();
      this.right = param1Parcel.readInt();
      this.bottom = param1Parcel.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.left);
      param1Parcel.writeInt(this.top);
      param1Parcel.writeInt(this.right);
      param1Parcel.writeInt(this.bottom);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1View = param1View.findViewById(this.viewId);
      if (param1View == null)
        return; 
      param1View.setPadding(this.left, this.top, this.right, this.bottom);
    }
    
    public int getActionTag() {
      return 14;
    }
  }
  
  class LayoutParamAction extends Action {
    public static final int LAYOUT_MARGIN_BOTTOM_DIMEN = 3;
    
    public static final int LAYOUT_MARGIN_END = 4;
    
    public static final int LAYOUT_MARGIN_END_DIMEN = 1;
    
    public static final int LAYOUT_WIDTH = 2;
    
    final int mProperty;
    
    final int mValue;
    
    public LayoutParamAction(RemoteViews this$0, int param1Int1, int param1Int2) {
      this.viewId = this$0;
      this.mProperty = param1Int1;
      this.mValue = param1Int2;
    }
    
    public LayoutParamAction(RemoteViews this$0) {
      this.viewId = this$0.readInt();
      this.mProperty = this$0.readInt();
      this.mValue = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeInt(this.mProperty);
      param1Parcel.writeInt(this.mValue);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      StringBuilder stringBuilder;
      param1ViewGroup = param1View.findViewById(this.viewId);
      if (param1ViewGroup == null)
        return; 
      ViewGroup.LayoutParams layoutParams = param1ViewGroup.getLayoutParams();
      if (layoutParams == null)
        return; 
      int i = this.mValue;
      int j = this.mProperty;
      if (j != 1) {
        if (j != 2) {
          if (j != 3) {
            if (j != 4) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown property ");
              stringBuilder.append(this.mProperty);
              throw new IllegalArgumentException(stringBuilder.toString());
            } 
          } else {
            if (stringBuilder instanceof ViewGroup.MarginLayoutParams) {
              i = resolveDimenPixelOffset(param1ViewGroup, this.mValue);
              ((ViewGroup.MarginLayoutParams)stringBuilder).bottomMargin = i;
              param1ViewGroup.setLayoutParams((ViewGroup.LayoutParams)stringBuilder);
            } 
            return;
          } 
        } else {
          ((ViewGroup.LayoutParams)stringBuilder).width = this.mValue;
          param1ViewGroup.setLayoutParams((ViewGroup.LayoutParams)stringBuilder);
          return;
        } 
      } else {
        i = resolveDimenPixelOffset(param1ViewGroup, this.mValue);
      } 
      if (stringBuilder instanceof ViewGroup.MarginLayoutParams) {
        ((ViewGroup.MarginLayoutParams)stringBuilder).setMarginEnd(i);
        param1ViewGroup.setLayoutParams((ViewGroup.LayoutParams)stringBuilder);
      } 
    }
    
    private static int resolveDimenPixelOffset(View param1View, int param1Int) {
      if (param1Int == 0)
        return 0; 
      return param1View.getContext().getResources().getDimensionPixelOffset(param1Int);
    }
    
    public int getActionTag() {
      return 19;
    }
    
    public String getUniqueKey() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.getUniqueKey());
      stringBuilder.append(this.mProperty);
      return stringBuilder.toString();
    }
  }
  
  class SetRemoteInputsAction extends Action {
    final Parcelable[] remoteInputs;
    
    final RemoteViews this$0;
    
    public SetRemoteInputsAction(int param1Int, RemoteInput[] param1ArrayOfRemoteInput) {
      this.viewId = param1Int;
      this.remoteInputs = (Parcelable[])param1ArrayOfRemoteInput;
    }
    
    public SetRemoteInputsAction(Parcel param1Parcel) {
      this.viewId = param1Parcel.readInt();
      this.remoteInputs = (Parcelable[])param1Parcel.createTypedArray(RemoteInput.CREATOR);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.viewId);
      param1Parcel.writeTypedArray(this.remoteInputs, param1Int);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1View = param1View.findViewById(this.viewId);
      if (param1View == null)
        return; 
      param1View.setTagInternal(16909343, this.remoteInputs);
    }
    
    public int getActionTag() {
      return 18;
    }
  }
  
  class OverrideTextColorsAction extends Action {
    private final int textColor;
    
    final RemoteViews this$0;
    
    public OverrideTextColorsAction(int param1Int) {
      this.textColor = param1Int;
    }
    
    public OverrideTextColorsAction(Parcel param1Parcel) {
      this.textColor = param1Parcel.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.textColor);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      Stack<View> stack = new Stack();
      stack.add(param1View);
      while (!stack.isEmpty()) {
        param1View = stack.pop();
        if (param1View instanceof TextView) {
          TextView textView = (TextView)param1View;
          textView.setText(ContrastColorUtil.clearColorSpans(textView.getText()));
          textView.setTextColor(this.textColor);
        } 
        if (param1View instanceof ViewGroup) {
          param1View = param1View;
          for (byte b = 0; b < param1View.getChildCount(); b++)
            stack.push(param1View.getChildAt(b)); 
        } 
      } 
    }
    
    public int getActionTag() {
      return 20;
    }
  }
  
  class SetIntTagAction extends Action {
    private final int mKey;
    
    private final int mTag;
    
    private final int mViewId;
    
    final RemoteViews this$0;
    
    SetIntTagAction(int param1Int1, int param1Int2, int param1Int3) {
      this.mViewId = param1Int1;
      this.mKey = param1Int2;
      this.mTag = param1Int3;
    }
    
    SetIntTagAction(Parcel param1Parcel) {
      this.mViewId = param1Parcel.readInt();
      this.mKey = param1Parcel.readInt();
      this.mTag = param1Parcel.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mViewId);
      param1Parcel.writeInt(this.mKey);
      param1Parcel.writeInt(this.mTag);
    }
    
    public void apply(View param1View, ViewGroup param1ViewGroup, RemoteViews.OnClickHandler param1OnClickHandler) {
      param1View = param1View.findViewById(this.mViewId);
      if (param1View == null)
        return; 
      param1View.setTagInternal(this.mKey, Integer.valueOf(this.mTag));
    }
    
    public int getActionTag() {
      return 22;
    }
  }
  
  public RemoteViews(String paramString, int paramInt) {
    this(getApplicationInfo(paramString, UserHandle.myUserId()), paramInt);
  }
  
  public RemoteViews(String paramString, int paramInt1, int paramInt2) {
    this(getApplicationInfo(paramString, paramInt1), paramInt2);
  }
  
  protected RemoteViews(ApplicationInfo paramApplicationInfo, int paramInt) {
    this.mApplication = paramApplicationInfo;
    this.mLayoutId = paramInt;
    this.mBitmapCache = new BitmapCache();
    this.mClassCookies = null;
    OplusPackageManager oplusPackageManager = new OplusPackageManager();
    if (paramApplicationInfo != null && paramApplicationInfo.processName != null) {
      String str = paramApplicationInfo.processName;
      if (oplusPackageManager.inCptWhiteList(723, str))
        addFlags(1); 
    } 
  }
  
  private boolean hasLandscapeAndPortraitLayouts() {
    boolean bool;
    if (this.mLandscape != null && this.mPortrait != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public RemoteViews(RemoteViews paramRemoteViews1, RemoteViews paramRemoteViews2) {
    if (paramRemoteViews1 != null && paramRemoteViews2 != null) {
      if (paramRemoteViews1.hasSameAppInfo(paramRemoteViews2.mApplication)) {
        Map<Class, Object> map1;
        this.mApplication = paramRemoteViews2.mApplication;
        this.mLayoutId = paramRemoteViews2.mLayoutId;
        this.mLightBackgroundLayoutId = paramRemoteViews2.mLightBackgroundLayoutId;
        this.mLandscape = paramRemoteViews1;
        this.mPortrait = paramRemoteViews2;
        this.mBitmapCache = new BitmapCache();
        configureRemoteViewsAsChild(paramRemoteViews1);
        configureRemoteViewsAsChild(paramRemoteViews2);
        Map<Class, Object> map2 = paramRemoteViews2.mClassCookies;
        if (map2 != null) {
          map1 = map2;
        } else {
          map1 = ((RemoteViews)map1).mClassCookies;
        } 
        this.mClassCookies = map1;
        return;
      } 
      throw new RuntimeException("Both RemoteViews must share the same package and user");
    } 
    throw new RuntimeException("Both RemoteViews must be non-null");
  }
  
  public RemoteViews(RemoteViews paramRemoteViews) {
    this.mBitmapCache = paramRemoteViews.mBitmapCache;
    this.mApplication = paramRemoteViews.mApplication;
    this.mIsRoot = paramRemoteViews.mIsRoot;
    this.mLayoutId = paramRemoteViews.mLayoutId;
    this.mLightBackgroundLayoutId = paramRemoteViews.mLightBackgroundLayoutId;
    this.mApplyFlags = paramRemoteViews.mApplyFlags;
    this.mClassCookies = paramRemoteViews.mClassCookies;
    if (paramRemoteViews.hasLandscapeAndPortraitLayouts()) {
      this.mLandscape = new RemoteViews(paramRemoteViews.mLandscape);
      this.mPortrait = new RemoteViews(paramRemoteViews.mPortrait);
    } 
    if (paramRemoteViews.mActions != null) {
      Parcel parcel = Parcel.obtain();
      parcel.putClassCookies(this.mClassCookies);
      paramRemoteViews.writeActionsToParcel(parcel);
      parcel.setDataPosition(0);
      readActionsFromParcel(parcel, 0);
      parcel.recycle();
    } 
    setBitmapCache(new BitmapCache());
  }
  
  public RemoteViews(Parcel paramParcel) {
    this(paramParcel, null, null, 0, null);
  }
  
  private RemoteViews(Parcel paramParcel, BitmapCache paramBitmapCache, ApplicationInfo paramApplicationInfo, int paramInt, Map<Class, Object> paramMap) {
    if (paramInt <= 10 || UserHandle.getAppId(Binder.getCallingUid()) == 1000) {
      int i = paramInt + 1;
      paramInt = paramParcel.readInt();
      if (paramBitmapCache == null) {
        this.mBitmapCache = new BitmapCache(paramParcel);
        this.mClassCookies = paramParcel.copyClassCookies();
      } else {
        setBitmapCache(paramBitmapCache);
        this.mClassCookies = paramMap;
        setNotRoot();
      } 
      if (paramInt == 0) {
        ApplicationInfo applicationInfo;
        if (paramParcel.readInt() == 0) {
          applicationInfo = paramApplicationInfo;
        } else {
          applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(paramParcel);
        } 
        this.mApplication = applicationInfo;
        this.mLayoutId = paramParcel.readInt();
        this.mLightBackgroundLayoutId = paramParcel.readInt();
        readActionsFromParcel(paramParcel, i);
      } else {
        this.mLandscape = new RemoteViews(paramParcel, this.mBitmapCache, paramApplicationInfo, i, this.mClassCookies);
        RemoteViews remoteViews = new RemoteViews(paramParcel, this.mBitmapCache, this.mLandscape.mApplication, i, this.mClassCookies);
        this.mApplication = remoteViews.mApplication;
        this.mLayoutId = remoteViews.mLayoutId;
        this.mLightBackgroundLayoutId = remoteViews.mLightBackgroundLayoutId;
      } 
      this.mApplyFlags = paramParcel.readInt();
      return;
    } 
    throw new IllegalArgumentException("Too many nested views.");
  }
  
  private void readActionsFromParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.readInt();
    if (i > 0) {
      this.mActions = new ArrayList<>(i);
      for (byte b = 0; b < i; b++)
        this.mActions.add(getActionFromParcel(paramParcel, paramInt)); 
    } 
  }
  
  private Action getActionFromParcel(Parcel paramParcel, int paramInt) {
    StringBuilder stringBuilder;
    int i = paramParcel.readInt();
    switch (i) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Tag ");
        stringBuilder.append(i);
        stringBuilder.append(" not found");
        throw new ActionException(stringBuilder.toString());
      case 22:
        return new SetIntTagAction((Parcel)stringBuilder);
      case 21:
        return new SetRippleDrawableColor((Parcel)stringBuilder);
      case 20:
        return new OverrideTextColorsAction((Parcel)stringBuilder);
      case 19:
        return new LayoutParamAction((Parcel)stringBuilder);
      case 18:
        return new SetRemoteInputsAction((Parcel)stringBuilder);
      case 15:
        return new SetRemoteViewsAdapterList((Parcel)stringBuilder);
      case 14:
        return new ViewPaddingAction((Parcel)stringBuilder);
      case 13:
        return new TextViewSizeAction((Parcel)stringBuilder);
      case 12:
        return new BitmapReflectionAction((Parcel)stringBuilder);
      case 11:
        return new TextViewDrawableAction((Parcel)stringBuilder);
      case 10:
        return new SetRemoteViewsAdapterIntent((Parcel)stringBuilder);
      case 8:
        return new SetPendingIntentTemplate((Parcel)stringBuilder);
      case 7:
        return new ViewGroupActionRemove((Parcel)stringBuilder);
      case 6:
        return new SetEmptyView((Parcel)stringBuilder);
      case 5:
        return new ViewContentNavigation((Parcel)stringBuilder);
      case 4:
        return new ViewGroupActionAdd((Parcel)stringBuilder, this.mBitmapCache, this.mApplication, paramInt, this.mClassCookies);
      case 3:
        return new SetDrawableTint((Parcel)stringBuilder);
      case 2:
        return new ReflectionAction((Parcel)stringBuilder);
      case 1:
        break;
    } 
    return new SetOnClickResponse((Parcel)stringBuilder);
  }
  
  @Deprecated
  public RemoteViews clone() {
    Preconditions.checkState(this.mIsRoot, "RemoteView has been attached to another RemoteView. May only clone the root of a RemoteView hierarchy.");
    return new RemoteViews(this);
  }
  
  public String getPackage() {
    ApplicationInfo applicationInfo = this.mApplication;
    if (applicationInfo != null) {
      String str = applicationInfo.packageName;
    } else {
      applicationInfo = null;
    } 
    return (String)applicationInfo;
  }
  
  public int getLayoutId() {
    if (hasFlags(4)) {
      int i = this.mLightBackgroundLayoutId;
      if (i != 0)
        return i; 
    } 
    return this.mLayoutId;
  }
  
  private void setBitmapCache(BitmapCache paramBitmapCache) {
    this.mBitmapCache = paramBitmapCache;
    if (!hasLandscapeAndPortraitLayouts()) {
      ArrayList<Action> arrayList = this.mActions;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Action)this.mActions.get(b)).setBitmapCache(paramBitmapCache); 
      } 
    } else {
      this.mLandscape.setBitmapCache(paramBitmapCache);
      this.mPortrait.setBitmapCache(paramBitmapCache);
    } 
  }
  
  public int estimateMemoryUsage() {
    return this.mBitmapCache.getBitmapMemory();
  }
  
  private void addAction(Action paramAction) {
    if (!hasLandscapeAndPortraitLayouts()) {
      if (this.mActions == null)
        this.mActions = new ArrayList<>(); 
      this.mActions.add(paramAction);
      return;
    } 
    throw new RuntimeException("RemoteViews specifying separate landscape and portrait layouts cannot be modified. Instead, fully configure the landscape and portrait layouts individually before constructing the combined layout.");
  }
  
  public void addView(int paramInt, RemoteViews paramRemoteViews) {
    ViewGroupActionRemove viewGroupActionRemove;
    ViewGroupActionAdd viewGroupActionAdd;
    if (paramRemoteViews == null) {
      viewGroupActionRemove = new ViewGroupActionRemove(paramInt);
    } else {
      viewGroupActionAdd = new ViewGroupActionAdd(paramInt, (RemoteViews)viewGroupActionRemove);
    } 
    addAction(viewGroupActionAdd);
  }
  
  public void addView(int paramInt1, RemoteViews paramRemoteViews, int paramInt2) {
    addAction(new ViewGroupActionAdd(paramInt1, paramRemoteViews, paramInt2));
  }
  
  public void removeAllViews(int paramInt) {
    addAction(new ViewGroupActionRemove(paramInt));
  }
  
  public void removeAllViewsExceptId(int paramInt1, int paramInt2) {
    addAction(new ViewGroupActionRemove(paramInt1, paramInt2));
  }
  
  public void showNext(int paramInt) {
    addAction(new ViewContentNavigation(paramInt, true));
  }
  
  public void showPrevious(int paramInt) {
    addAction(new ViewContentNavigation(paramInt, false));
  }
  
  public void setDisplayedChild(int paramInt1, int paramInt2) {
    setInt(paramInt1, "setDisplayedChild", paramInt2);
  }
  
  public void setViewVisibility(int paramInt1, int paramInt2) {
    setInt(paramInt1, "setVisibility", paramInt2);
  }
  
  public void setTextViewText(int paramInt, CharSequence paramCharSequence) {
    setCharSequence(paramInt, "setText", paramCharSequence);
  }
  
  public void setTextViewTextSize(int paramInt1, int paramInt2, float paramFloat) {
    addAction(new TextViewSizeAction(paramInt1, paramInt2, paramFloat));
  }
  
  public void setTextViewCompoundDrawables(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    addAction(new TextViewDrawableAction(paramInt1, false, paramInt2, paramInt3, paramInt4, paramInt5));
  }
  
  public void setTextViewCompoundDrawablesRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    addAction(new TextViewDrawableAction(paramInt1, true, paramInt2, paramInt3, paramInt4, paramInt5));
  }
  
  public void setTextViewCompoundDrawables(int paramInt, Icon paramIcon1, Icon paramIcon2, Icon paramIcon3, Icon paramIcon4) {
    addAction(new TextViewDrawableAction(paramInt, false, paramIcon1, paramIcon2, paramIcon3, paramIcon4));
  }
  
  public void setTextViewCompoundDrawablesRelative(int paramInt, Icon paramIcon1, Icon paramIcon2, Icon paramIcon3, Icon paramIcon4) {
    addAction(new TextViewDrawableAction(paramInt, true, paramIcon1, paramIcon2, paramIcon3, paramIcon4));
  }
  
  public void setImageViewResource(int paramInt1, int paramInt2) {
    setInt(paramInt1, "setImageResource", paramInt2);
  }
  
  public void setImageViewUri(int paramInt, Uri paramUri) {
    setUri(paramInt, "setImageURI", paramUri);
  }
  
  public void setImageViewBitmap(int paramInt, Bitmap paramBitmap) {
    setBitmap(paramInt, "setImageBitmap", paramBitmap);
  }
  
  public void setImageViewIcon(int paramInt, Icon paramIcon) {
    setIcon(paramInt, "setImageIcon", paramIcon);
  }
  
  public void setEmptyView(int paramInt1, int paramInt2) {
    addAction(new SetEmptyView(paramInt1, paramInt2));
  }
  
  public void setChronometer(int paramInt, long paramLong, String paramString, boolean paramBoolean) {
    setLong(paramInt, "setBase", paramLong);
    setString(paramInt, "setFormat", paramString);
    setBoolean(paramInt, "setStarted", paramBoolean);
  }
  
  public void setChronometerCountDown(int paramInt, boolean paramBoolean) {
    setBoolean(paramInt, "setCountDown", paramBoolean);
  }
  
  public void setProgressBar(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    setBoolean(paramInt1, "setIndeterminate", paramBoolean);
    if (!paramBoolean) {
      setInt(paramInt1, "setMax", paramInt2);
      setInt(paramInt1, "setProgress", paramInt3);
    } 
  }
  
  public void setOnClickPendingIntent(int paramInt, PendingIntent paramPendingIntent) {
    setOnClickResponse(paramInt, RemoteResponse.fromPendingIntent(paramPendingIntent));
  }
  
  public void setOnClickResponse(int paramInt, RemoteResponse paramRemoteResponse) {
    addAction(new SetOnClickResponse(paramInt, paramRemoteResponse));
  }
  
  public void setPendingIntentTemplate(int paramInt, PendingIntent paramPendingIntent) {
    addAction(new SetPendingIntentTemplate(paramInt, paramPendingIntent));
  }
  
  public void setOnClickFillInIntent(int paramInt, Intent paramIntent) {
    setOnClickResponse(paramInt, RemoteResponse.fromFillInIntent(paramIntent));
  }
  
  public void setDrawableTint(int paramInt1, boolean paramBoolean, int paramInt2, PorterDuff.Mode paramMode) {
    addAction(new SetDrawableTint(paramInt1, paramBoolean, paramInt2, paramMode));
  }
  
  public void setRippleDrawableColor(int paramInt, ColorStateList paramColorStateList) {
    addAction(new SetRippleDrawableColor(paramInt, paramColorStateList));
  }
  
  public void setProgressTintList(int paramInt, ColorStateList paramColorStateList) {
    addAction(new ReflectionAction(paramInt, "setProgressTintList", 15, paramColorStateList));
  }
  
  public void setProgressBackgroundTintList(int paramInt, ColorStateList paramColorStateList) {
    addAction(new ReflectionAction(paramInt, "setProgressBackgroundTintList", 15, paramColorStateList));
  }
  
  public void setProgressIndeterminateTintList(int paramInt, ColorStateList paramColorStateList) {
    addAction(new ReflectionAction(paramInt, "setIndeterminateTintList", 15, paramColorStateList));
  }
  
  public void setTextColor(int paramInt1, int paramInt2) {
    setInt(paramInt1, "setTextColor", paramInt2);
  }
  
  public void setTextColor(int paramInt, ColorStateList paramColorStateList) {
    addAction(new ReflectionAction(paramInt, "setTextColor", 15, paramColorStateList));
  }
  
  @Deprecated
  public void setRemoteAdapter(int paramInt1, int paramInt2, Intent paramIntent) {
    setRemoteAdapter(paramInt2, paramIntent);
  }
  
  public void setRemoteAdapter(int paramInt, Intent paramIntent) {
    addAction(new SetRemoteViewsAdapterIntent(paramInt, paramIntent));
  }
  
  @Deprecated
  public void setRemoteAdapter(int paramInt1, ArrayList<RemoteViews> paramArrayList, int paramInt2) {
    addAction(new SetRemoteViewsAdapterList(paramInt1, paramArrayList, paramInt2));
  }
  
  public void setScrollPosition(int paramInt1, int paramInt2) {
    setInt(paramInt1, "smoothScrollToPosition", paramInt2);
  }
  
  public void setRelativeScrollPosition(int paramInt1, int paramInt2) {
    setInt(paramInt1, "smoothScrollByOffset", paramInt2);
  }
  
  public void setViewPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    addAction(new ViewPaddingAction(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5));
  }
  
  public void setViewLayoutMarginEndDimen(int paramInt1, int paramInt2) {
    addAction(new LayoutParamAction(paramInt1, 1, paramInt2));
  }
  
  public void setViewLayoutMarginEnd(int paramInt1, int paramInt2) {
    addAction(new LayoutParamAction(paramInt1, 4, paramInt2));
  }
  
  public void setViewLayoutMarginBottomDimen(int paramInt1, int paramInt2) {
    addAction(new LayoutParamAction(paramInt1, 3, paramInt2));
  }
  
  public void setViewLayoutWidth(int paramInt1, int paramInt2) {
    if (paramInt2 == 0 || paramInt2 == -1 || paramInt2 == -2) {
      this.mActions.add(new LayoutParamAction(paramInt1, 2, paramInt2));
      return;
    } 
    throw new IllegalArgumentException("Only supports 0, WRAP_CONTENT and MATCH_PARENT");
  }
  
  public void setBoolean(int paramInt, String paramString, boolean paramBoolean) {
    addAction(new ReflectionAction(paramInt, paramString, 1, Boolean.valueOf(paramBoolean)));
  }
  
  public void setByte(int paramInt, String paramString, byte paramByte) {
    addAction(new ReflectionAction(paramInt, paramString, 2, Byte.valueOf(paramByte)));
  }
  
  public void setShort(int paramInt, String paramString, short paramShort) {
    addAction(new ReflectionAction(paramInt, paramString, 3, Short.valueOf(paramShort)));
  }
  
  public void setInt(int paramInt1, String paramString, int paramInt2) {
    addAction(new ReflectionAction(paramInt1, paramString, 4, Integer.valueOf(paramInt2)));
  }
  
  public void setColorStateList(int paramInt, String paramString, ColorStateList paramColorStateList) {
    addAction(new ReflectionAction(paramInt, paramString, 15, paramColorStateList));
  }
  
  public void setLong(int paramInt, String paramString, long paramLong) {
    addAction(new ReflectionAction(paramInt, paramString, 5, Long.valueOf(paramLong)));
  }
  
  public void setFloat(int paramInt, String paramString, float paramFloat) {
    addAction(new ReflectionAction(paramInt, paramString, 6, Float.valueOf(paramFloat)));
  }
  
  public void setDouble(int paramInt, String paramString, double paramDouble) {
    addAction(new ReflectionAction(paramInt, paramString, 7, Double.valueOf(paramDouble)));
  }
  
  public void setChar(int paramInt, String paramString, char paramChar) {
    addAction(new ReflectionAction(paramInt, paramString, 8, Character.valueOf(paramChar)));
  }
  
  public void setString(int paramInt, String paramString1, String paramString2) {
    addAction(new ReflectionAction(paramInt, paramString1, 9, paramString2));
  }
  
  public void setCharSequence(int paramInt, String paramString, CharSequence paramCharSequence) {
    addAction(new ReflectionAction(paramInt, paramString, 10, paramCharSequence));
  }
  
  public void setUri(int paramInt, String paramString, Uri paramUri) {
    Uri uri = paramUri;
    if (paramUri != null) {
      paramUri = paramUri.getCanonicalUri();
      uri = paramUri;
      if (StrictMode.vmFileUriExposureEnabled()) {
        paramUri.checkFileUriExposed("RemoteViews.setUri()");
        uri = paramUri;
      } 
    } 
    addAction(new ReflectionAction(paramInt, paramString, 11, uri));
  }
  
  public void setBitmap(int paramInt, String paramString, Bitmap paramBitmap) {
    addAction(new BitmapReflectionAction(paramInt, paramString, paramBitmap));
  }
  
  public void setBundle(int paramInt, String paramString, Bundle paramBundle) {
    addAction(new ReflectionAction(paramInt, paramString, 13, paramBundle));
  }
  
  public void setIntent(int paramInt, String paramString, Intent paramIntent) {
    addAction(new ReflectionAction(paramInt, paramString, 14, paramIntent));
  }
  
  public void setIcon(int paramInt, String paramString, Icon paramIcon) {
    addAction(new ReflectionAction(paramInt, paramString, 16, paramIcon));
  }
  
  public void setContentDescription(int paramInt, CharSequence paramCharSequence) {
    setCharSequence(paramInt, "setContentDescription", paramCharSequence);
  }
  
  public void setAccessibilityTraversalBefore(int paramInt1, int paramInt2) {
    setInt(paramInt1, "setAccessibilityTraversalBefore", paramInt2);
  }
  
  public void setAccessibilityTraversalAfter(int paramInt1, int paramInt2) {
    setInt(paramInt1, "setAccessibilityTraversalAfter", paramInt2);
  }
  
  public void setLabelFor(int paramInt1, int paramInt2) {
    setInt(paramInt1, "setLabelFor", paramInt2);
  }
  
  public void setLightBackgroundLayoutId(int paramInt) {
    this.mLightBackgroundLayoutId = paramInt;
  }
  
  public RemoteViews getDarkTextViews() {
    if (hasFlags(4))
      return this; 
    try {
      addFlags(4);
      return new RemoteViews(this);
    } finally {
      this.mApplyFlags &= 0xFFFFFFFB;
    } 
  }
  
  private RemoteViews getRemoteViewsToApply(Context paramContext) {
    if (hasLandscapeAndPortraitLayouts()) {
      int i = (paramContext.getResources().getConfiguration()).orientation;
      if (i == 2)
        return this.mLandscape; 
      return this.mPortrait;
    } 
    return this;
  }
  
  public View apply(Context paramContext, ViewGroup paramViewGroup) {
    return apply(paramContext, paramViewGroup, null);
  }
  
  public View apply(Context paramContext, ViewGroup paramViewGroup, OnClickHandler paramOnClickHandler) {
    RemoteViews remoteViews = getRemoteViewsToApply(paramContext);
    View view = inflateView(paramContext, remoteViews, paramViewGroup);
    remoteViews.performApply(view, paramViewGroup, paramOnClickHandler);
    return view;
  }
  
  public View applyWithTheme(Context paramContext, ViewGroup paramViewGroup, OnClickHandler paramOnClickHandler, int paramInt) {
    RemoteViews remoteViews = getRemoteViewsToApply(paramContext);
    View view = inflateView(paramContext, remoteViews, paramViewGroup, paramInt);
    remoteViews.performApply(view, paramViewGroup, paramOnClickHandler);
    return view;
  }
  
  private View inflateView(Context paramContext, RemoteViews paramRemoteViews, ViewGroup paramViewGroup) {
    return inflateView(paramContext, paramRemoteViews, paramViewGroup, 0);
  }
  
  private View inflateView(Context paramContext, RemoteViews paramRemoteViews, ViewGroup paramViewGroup, int paramInt) {
    LayoutInflater.Filter filter;
    ContextThemeWrapper contextThemeWrapper;
    Context context = getContextForResources(paramContext);
    RemoteViewsContextWrapper remoteViewsContextWrapper2 = new RemoteViewsContextWrapper(paramContext, context);
    RemoteViewsContextWrapper remoteViewsContextWrapper1 = remoteViewsContextWrapper2;
    if (paramInt != 0)
      contextThemeWrapper = new ContextThemeWrapper((Context)remoteViewsContextWrapper2, paramInt); 
    LayoutInflater layoutInflater1 = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    LayoutInflater layoutInflater2 = layoutInflater1.cloneInContext((Context)contextThemeWrapper);
    if (shouldUseStaticFilter()) {
      filter = INFLATER_FILTER;
    } else {
      filter = this;
    } 
    layoutInflater2.setFilter(filter);
    View view = layoutInflater2.inflate(paramRemoteViews.getLayoutId(), paramViewGroup, false);
    view.setTagInternal(16908312, Integer.valueOf(paramRemoteViews.getLayoutId()));
    return view;
  }
  
  protected boolean shouldUseStaticFilter() {
    return getClass().equals(RemoteViews.class);
  }
  
  class OnViewAppliedListener {
    public abstract void onError(Exception param1Exception);
    
    public abstract void onViewApplied(View param1View);
    
    public void onViewInflated(View param1View) {}
  }
  
  public CancellationSignal applyAsync(Context paramContext, ViewGroup paramViewGroup, Executor paramExecutor, OnViewAppliedListener paramOnViewAppliedListener) {
    return applyAsync(paramContext, paramViewGroup, paramExecutor, paramOnViewAppliedListener, null);
  }
  
  public CancellationSignal applyAsync(Context paramContext, ViewGroup paramViewGroup, Executor paramExecutor, OnViewAppliedListener paramOnViewAppliedListener, OnClickHandler paramOnClickHandler) {
    return getAsyncApplyTask(paramContext, paramViewGroup, paramOnViewAppliedListener, paramOnClickHandler).startTaskOnExecutor(paramExecutor);
  }
  
  private AsyncApplyTask getAsyncApplyTask(Context paramContext, ViewGroup paramViewGroup, OnViewAppliedListener paramOnViewAppliedListener, OnClickHandler paramOnClickHandler) {
    return new AsyncApplyTask(getRemoteViewsToApply(paramContext), paramViewGroup, paramContext, paramOnViewAppliedListener, paramOnClickHandler, null);
  }
  
  private class AsyncApplyTask extends AsyncTask<Void, Void, ViewTree> implements CancellationSignal.OnCancelListener {
    private RemoteViews.Action[] mActions;
    
    final CancellationSignal mCancelSignal = new CancellationSignal();
    
    final Context mContext;
    
    private Exception mError;
    
    final RemoteViews.OnClickHandler mHandler;
    
    final RemoteViews.OnViewAppliedListener mListener;
    
    final ViewGroup mParent;
    
    final RemoteViews mRV;
    
    private View mResult;
    
    private RemoteViews.ViewTree mTree;
    
    final RemoteViews this$0;
    
    private AsyncApplyTask(RemoteViews param1RemoteViews1, ViewGroup param1ViewGroup, Context param1Context, RemoteViews.OnViewAppliedListener param1OnViewAppliedListener, RemoteViews.OnClickHandler param1OnClickHandler, View param1View) {
      this.mRV = param1RemoteViews1;
      this.mParent = param1ViewGroup;
      this.mContext = param1Context;
      this.mListener = param1OnViewAppliedListener;
      this.mHandler = param1OnClickHandler;
      this.mResult = param1View;
    }
    
    protected RemoteViews.ViewTree doInBackground(Void... param1VarArgs) {
      try {
        if (this.mResult == null)
          this.mResult = RemoteViews.this.inflateView(this.mContext, this.mRV, this.mParent); 
        null = new RemoteViews.ViewTree();
        this(this.mResult);
        this.mTree = null;
        if (this.mRV.mActions != null) {
          int i = this.mRV.mActions.size();
          this.mActions = new RemoteViews.Action[i];
          for (byte b = 0; b < i && !isCancelled(); b++)
            this.mActions[b] = ((RemoteViews.Action)this.mRV.mActions.get(b)).initActionAsync(this.mTree, this.mParent, this.mHandler); 
        } else {
          this.mActions = null;
        } 
        return this.mTree;
      } catch (Exception exception) {
        this.mError = exception;
        return null;
      } 
    }
    
    protected void onPostExecute(RemoteViews.ViewTree param1ViewTree) {
      this.mCancelSignal.setOnCancelListener(null);
      if (this.mError == null) {
        RemoteViews.OnViewAppliedListener onViewAppliedListener1 = this.mListener;
        if (onViewAppliedListener1 != null)
          onViewAppliedListener1.onViewInflated(param1ViewTree.mRoot); 
        try {
          if (this.mActions != null) {
            RemoteViews.OnClickHandler onClickHandler;
            if (this.mHandler == null) {
              onClickHandler = RemoteViews.DEFAULT_ON_CLICK_HANDLER;
            } else {
              onClickHandler = this.mHandler;
            } 
            for (RemoteViews.Action action : this.mActions)
              action.apply(param1ViewTree.mRoot, this.mParent, onClickHandler); 
          } 
        } catch (Exception exception) {
          this.mError = exception;
        } 
      } 
      RemoteViews.OnViewAppliedListener onViewAppliedListener = this.mListener;
      if (onViewAppliedListener != null) {
        Exception exception = this.mError;
        if (exception != null) {
          onViewAppliedListener.onError(exception);
        } else {
          onViewAppliedListener.onViewApplied(param1ViewTree.mRoot);
        } 
      } else {
        Exception exception = this.mError;
        if (exception != null) {
          if (exception instanceof RemoteViews.ActionException)
            throw (RemoteViews.ActionException)exception; 
          throw new RemoteViews.ActionException(this.mError);
        } 
      } 
    }
    
    public void onCancel() {
      cancel(true);
    }
    
    private CancellationSignal startTaskOnExecutor(Executor param1Executor) {
      this.mCancelSignal.setOnCancelListener(this);
      if (param1Executor == null)
        param1Executor = AsyncTask.THREAD_POOL_EXECUTOR; 
      executeOnExecutor(param1Executor, (Object[])new Void[0]);
      return this.mCancelSignal;
    }
  }
  
  public void reapply(Context paramContext, View paramView) {
    reapply(paramContext, paramView, null);
  }
  
  public void reapply(Context paramContext, View paramView, OnClickHandler paramOnClickHandler) {
    RemoteViews remoteViews = getRemoteViewsToApply(paramContext);
    if (!hasLandscapeAndPortraitLayouts() || ((Integer)paramView.getTag(16908312)).intValue() == remoteViews.getLayoutId()) {
      remoteViews.performApply(paramView, (ViewGroup)paramView.getParent(), paramOnClickHandler);
      return;
    } 
    throw new RuntimeException("Attempting to re-apply RemoteViews to a view that that does not share the same root layout id.");
  }
  
  public CancellationSignal reapplyAsync(Context paramContext, View paramView, Executor paramExecutor, OnViewAppliedListener paramOnViewAppliedListener) {
    return reapplyAsync(paramContext, paramView, paramExecutor, paramOnViewAppliedListener, null);
  }
  
  public CancellationSignal reapplyAsync(Context paramContext, View paramView, Executor paramExecutor, OnViewAppliedListener paramOnViewAppliedListener, OnClickHandler paramOnClickHandler) {
    RemoteViews remoteViews = getRemoteViewsToApply(paramContext);
    if (!hasLandscapeAndPortraitLayouts() || ((Integer)paramView.getTag(16908312)).intValue() == remoteViews.getLayoutId()) {
      AsyncApplyTask asyncApplyTask = new AsyncApplyTask(remoteViews, (ViewGroup)paramView.getParent(), paramContext, paramOnViewAppliedListener, paramOnClickHandler, paramView);
      return asyncApplyTask.startTaskOnExecutor(paramExecutor);
    } 
    throw new RuntimeException("Attempting to re-apply RemoteViews to a view that that does not share the same root layout id.");
  }
  
  private void performApply(View paramView, ViewGroup paramViewGroup, OnClickHandler paramOnClickHandler) {
    if (this.mActions != null) {
      if (paramOnClickHandler == null)
        paramOnClickHandler = DEFAULT_ON_CLICK_HANDLER; 
      int i = this.mActions.size();
      for (byte b = 0; b < i; b++) {
        Action action = this.mActions.get(b);
        action.apply(paramView, paramViewGroup, paramOnClickHandler);
      } 
    } 
  }
  
  public boolean prefersAsyncApply() {
    ArrayList<Action> arrayList = this.mActions;
    if (arrayList != null) {
      int i = arrayList.size();
      for (byte b = 0; b < i; b++) {
        if (((Action)this.mActions.get(b)).prefersAsyncApply())
          return true; 
      } 
    } 
    return false;
  }
  
  private Context getContextForResources(Context paramContext) {
    if (this.mApplication != null) {
      if (paramContext.getUserId() == UserHandle.getUserId(this.mApplication.uid) && paramContext.getPackageName().equals(this.mApplication.packageName))
        return paramContext; 
      try {
        return paramContext.createApplicationContext(this.mApplication, 4);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Package name ");
        stringBuilder.append(this.mApplication.packageName);
        stringBuilder.append(" not found");
        Log.e("RemoteViews", stringBuilder.toString());
      } 
    } 
    return paramContext;
  }
  
  public int getSequenceNumber() {
    int i;
    ArrayList<Action> arrayList = this.mActions;
    if (arrayList == null) {
      i = 0;
    } else {
      i = arrayList.size();
    } 
    return i;
  }
  
  @Deprecated
  public boolean onLoadClass(Class paramClass) {
    return paramClass.isAnnotationPresent((Class)RemoteView.class);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (!hasLandscapeAndPortraitLayouts()) {
      paramParcel.writeInt(0);
      if (this.mIsRoot)
        this.mBitmapCache.writeBitmapsToParcel(paramParcel, paramInt); 
      if (!this.mIsRoot && (paramInt & 0x2) != 0) {
        paramParcel.writeInt(0);
      } else {
        paramParcel.writeInt(1);
        this.mApplication.writeToParcel(paramParcel, paramInt);
      } 
      paramParcel.writeInt(this.mLayoutId);
      paramParcel.writeInt(this.mLightBackgroundLayoutId);
      writeActionsToParcel(paramParcel);
    } else {
      paramParcel.writeInt(1);
      if (this.mIsRoot)
        this.mBitmapCache.writeBitmapsToParcel(paramParcel, paramInt); 
      this.mLandscape.writeToParcel(paramParcel, paramInt);
      this.mPortrait.writeToParcel(paramParcel, paramInt | 0x2);
    } 
    paramParcel.writeInt(this.mApplyFlags);
  }
  
  private void writeActionsToParcel(Parcel paramParcel) {
    byte b1;
    ArrayList<Action> arrayList = this.mActions;
    if (arrayList != null) {
      b1 = arrayList.size();
    } else {
      b1 = 0;
    } 
    paramParcel.writeInt(b1);
    for (byte b2 = 0; b2 < b1; b2++) {
      boolean bool;
      Action action = this.mActions.get(b2);
      paramParcel.writeInt(action.getActionTag());
      if (action.hasSameAppInfo(this.mApplication)) {
        bool = true;
      } else {
        bool = false;
      } 
      action.writeToParcel(paramParcel, bool);
    } 
  }
  
  private static ApplicationInfo getApplicationInfo(String paramString, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 6
    //   4: aconst_null
    //   5: areturn
    //   6: invokestatic currentApplication : ()Landroid/app/Application;
    //   9: astore_2
    //   10: aload_2
    //   11: ifnull -> 109
    //   14: aload_2
    //   15: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   18: astore_3
    //   19: aload_3
    //   20: getfield uid : I
    //   23: invokestatic getUserId : (I)I
    //   26: iload_1
    //   27: if_icmpne -> 45
    //   30: aload_3
    //   31: getfield packageName : Ljava/lang/String;
    //   34: astore #4
    //   36: aload #4
    //   38: aload_0
    //   39: invokevirtual equals : (Ljava/lang/Object;)Z
    //   42: ifne -> 72
    //   45: aload_2
    //   46: invokevirtual getBaseContext : ()Landroid/content/Context;
    //   49: astore_2
    //   50: new android/os/UserHandle
    //   53: astore_3
    //   54: aload_3
    //   55: iload_1
    //   56: invokespecial <init> : (I)V
    //   59: aload_2
    //   60: aload_0
    //   61: iconst_0
    //   62: aload_3
    //   63: invokevirtual createPackageContextAsUser : (Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    //   66: astore_3
    //   67: aload_3
    //   68: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   71: astore_3
    //   72: aload_3
    //   73: areturn
    //   74: astore_3
    //   75: new java/lang/StringBuilder
    //   78: dup
    //   79: invokespecial <init> : ()V
    //   82: astore_3
    //   83: aload_3
    //   84: ldc_w 'No such package '
    //   87: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   90: pop
    //   91: aload_3
    //   92: aload_0
    //   93: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: new java/lang/IllegalArgumentException
    //   100: dup
    //   101: aload_3
    //   102: invokevirtual toString : ()Ljava/lang/String;
    //   105: invokespecial <init> : (Ljava/lang/String;)V
    //   108: athrow
    //   109: new java/lang/IllegalStateException
    //   112: dup
    //   113: ldc_w 'Cannot create remote views out of an aplication.'
    //   116: invokespecial <init> : (Ljava/lang/String;)V
    //   119: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3788	-> 0
    //   #3789	-> 4
    //   #3793	-> 6
    //   #3794	-> 10
    //   #3798	-> 14
    //   #3799	-> 19
    //   #3800	-> 36
    //   #3802	-> 45
    //   #3804	-> 67
    //   #3807	-> 72
    //   #3810	-> 72
    //   #3805	-> 74
    //   #3806	-> 75
    //   #3795	-> 109
    // Exception table:
    //   from	to	target	type
    //   45	67	74	android/content/pm/PackageManager$NameNotFoundException
    //   67	72	74	android/content/pm/PackageManager$NameNotFoundException
  }
  
  public boolean hasSameAppInfo(ApplicationInfo paramApplicationInfo) {
    boolean bool;
    if (this.mApplication.packageName.equals(paramApplicationInfo.packageName) && this.mApplication.uid == paramApplicationInfo.uid) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class ViewTree {
    private static final int INSERT_AT_END_INDEX = -1;
    
    private ArrayList<ViewTree> mChildren;
    
    private View mRoot;
    
    private ViewTree(RemoteViews this$0) {
      this.mRoot = (View)this$0;
    }
    
    public void createTree() {
      if (this.mChildren != null)
        return; 
      this.mChildren = new ArrayList<>();
      View view = this.mRoot;
      if (view instanceof ViewGroup) {
        view = view;
        int i = view.getChildCount();
        for (byte b = 0; b < i; b++)
          addViewChild(view.getChildAt(b)); 
      } 
    }
    
    public ViewTree findViewTreeById(int param1Int) {
      if (this.mRoot.getId() == param1Int)
        return this; 
      ArrayList<ViewTree> arrayList = this.mChildren;
      if (arrayList == null)
        return null; 
      for (ViewTree viewTree : arrayList) {
        viewTree = viewTree.findViewTreeById(param1Int);
        if (viewTree != null)
          return viewTree; 
      } 
      return null;
    }
    
    public void replaceView(View param1View) {
      this.mRoot = param1View;
      this.mChildren = null;
      createTree();
    }
    
    public <T extends View> T findViewById(int param1Int) {
      View view;
      if (this.mChildren == null)
        return this.mRoot.findViewById(param1Int); 
      ViewTree viewTree = findViewTreeById(param1Int);
      if (viewTree == null) {
        viewTree = null;
      } else {
        view = viewTree.mRoot;
      } 
      return (T)view;
    }
    
    public void addChild(ViewTree param1ViewTree) {
      addChild(param1ViewTree, -1);
    }
    
    public void addChild(ViewTree param1ViewTree, int param1Int) {
      if (this.mChildren == null)
        this.mChildren = new ArrayList<>(); 
      param1ViewTree.createTree();
      if (param1Int == -1) {
        this.mChildren.add(param1ViewTree);
        return;
      } 
      this.mChildren.add(param1Int, param1ViewTree);
    }
    
    private void addViewChild(View param1View) {
      ViewTree viewTree;
      if (param1View.isRootNamespace())
        return; 
      if (param1View.getId() != 0) {
        viewTree = new ViewTree();
        this.mChildren.add(viewTree);
      } else {
        viewTree = this;
      } 
      if (param1View instanceof ViewGroup && 
        viewTree.mChildren == null) {
        viewTree.mChildren = new ArrayList<>();
        param1View = param1View;
        int i = param1View.getChildCount();
        for (byte b = 0; b < i; b++)
          viewTree.addViewChild(param1View.getChildAt(b)); 
      } 
    }
  }
  
  class RemoteResponse {
    private ArrayList<String> mElementNames;
    
    private Intent mFillIntent;
    
    private PendingIntent mPendingIntent;
    
    private IntArray mViewIds;
    
    public static RemoteResponse fromPendingIntent(PendingIntent param1PendingIntent) {
      RemoteResponse remoteResponse = new RemoteResponse();
      remoteResponse.mPendingIntent = param1PendingIntent;
      return remoteResponse;
    }
    
    public static RemoteResponse fromFillInIntent(Intent param1Intent) {
      RemoteResponse remoteResponse = new RemoteResponse();
      remoteResponse.mFillIntent = param1Intent;
      return remoteResponse;
    }
    
    public RemoteResponse addSharedElement(int param1Int, String param1String) {
      if (this.mViewIds == null) {
        this.mViewIds = new IntArray();
        this.mElementNames = new ArrayList<>();
      } 
      this.mViewIds.add(param1Int);
      this.mElementNames.add(param1String);
      return this;
    }
    
    private void writeToParcel(Parcel param1Parcel, int param1Int) {
      int[] arrayOfInt;
      PendingIntent.writePendingIntentOrNullToParcel(this.mPendingIntent, param1Parcel);
      if (this.mPendingIntent == null)
        param1Parcel.writeTypedObject((Parcelable)this.mFillIntent, param1Int); 
      IntArray intArray = this.mViewIds;
      if (intArray == null) {
        intArray = null;
      } else {
        arrayOfInt = intArray.toArray();
      } 
      param1Parcel.writeIntArray(arrayOfInt);
      param1Parcel.writeStringList(this.mElementNames);
    }
    
    private void readFromParcel(Parcel param1Parcel) {
      IntArray intArray;
      PendingIntent pendingIntent = PendingIntent.readPendingIntentOrNullFromParcel(param1Parcel);
      if (pendingIntent == null)
        this.mFillIntent = (Intent)param1Parcel.readTypedObject(Intent.CREATOR); 
      int[] arrayOfInt = param1Parcel.createIntArray();
      if (arrayOfInt == null) {
        arrayOfInt = null;
      } else {
        intArray = IntArray.wrap(arrayOfInt);
      } 
      this.mViewIds = intArray;
      this.mElementNames = param1Parcel.createStringArrayList();
    }
    
    private void handleViewClick(View param1View, RemoteViews.OnClickHandler param1OnClickHandler) {
      PendingIntent pendingIntent;
      if (this.mPendingIntent != null) {
        pendingIntent = this.mPendingIntent;
      } else if (this.mFillIntent != null) {
        View view = (View)param1View.getParent();
        while (view != null && !(view instanceof AdapterView) && (!(view instanceof AppWidgetHostView) || view instanceof RemoteViewsAdapter.RemoteViewsFrameLayout))
          view = (View)view.getParent(); 
        if (!(view instanceof AdapterView)) {
          Log.e("RemoteViews", "Collection item doesn't have AdapterView parent");
          return;
        } 
        if (!(view.getTag() instanceof PendingIntent)) {
          Log.e("RemoteViews", "Attempting setOnClickFillInIntent without calling setPendingIntentTemplate on parent.");
          return;
        } 
        pendingIntent = (PendingIntent)view.getTag();
      } else {
        Log.e("RemoteViews", "Response has neither pendingIntent nor fillInIntent");
        return;
      } 
      param1OnClickHandler.onClickHandler(param1View, pendingIntent, this);
    }
    
    public Pair<Intent, ActivityOptions> getLaunchOptions(View param1View) {
      Intent intent;
      ActivityOptions activityOptions3;
      if (this.mPendingIntent != null) {
        intent = new Intent();
      } else {
        intent = new Intent(this.mFillIntent);
      } 
      intent.setSourceBounds(RemoteViews.getSourceBounds(param1View));
      TypedArray typedArray1 = null;
      Context context = param1View.getContext();
      TypedArray typedArray2 = typedArray1;
      if (context.getResources().getBoolean(17891498)) {
        typedArray2 = context.getTheme().obtainStyledAttributes(R.styleable.Window);
        int i = typedArray2.getResourceId(8, 0);
        TypedArray typedArray = context.obtainStyledAttributes(i, R.styleable.WindowAnimation);
        i = typedArray.getResourceId(26, 0);
        typedArray2.recycle();
        typedArray.recycle();
        typedArray2 = typedArray1;
        if (i != 0) {
          activityOptions3 = ActivityOptions.makeCustomAnimation(context, i, 0);
          activityOptions3.setPendingIntentLaunchFlags(268435456);
        } 
      } 
      ActivityOptions activityOptions2 = activityOptions3;
      if (activityOptions3 == null) {
        activityOptions2 = activityOptions3;
        if (this.mViewIds != null) {
          activityOptions2 = activityOptions3;
          if (this.mElementNames != null) {
            param1View = (View)param1View.getParent();
            while (param1View != null && !(param1View instanceof AppWidgetHostView))
              param1View = (View)param1View.getParent(); 
            activityOptions2 = activityOptions3;
            if (param1View instanceof AppWidgetHostView) {
              AppWidgetHostView appWidgetHostView = (AppWidgetHostView)param1View;
              IntArray intArray = this.mViewIds;
              int[] arrayOfInt = intArray.toArray();
              ArrayList<String> arrayList = this.mElementNames;
              String[] arrayOfString = arrayList.<String>toArray(new String[arrayList.size()]);
              activityOptions2 = appWidgetHostView.createSharedElementActivityOptions(arrayOfInt, arrayOfString, intent);
            } 
          } 
        } 
      } 
      ActivityOptions activityOptions1 = activityOptions2;
      if (activityOptions2 == null) {
        activityOptions1 = ActivityOptions.makeBasic();
        activityOptions1.setPendingIntentLaunchFlags(268435456);
      } 
      return Pair.create(intent, activityOptions1);
    }
  }
  
  public static boolean startPendingIntent(View paramView, PendingIntent paramPendingIntent, Pair<Intent, ActivityOptions> paramPair) {
    try {
      Context context = paramView.getContext();
      IntentSender intentSender = paramPendingIntent.getIntentSender();
      Intent intent = (Intent)paramPair.first;
      ActivityOptions activityOptions = (ActivityOptions)paramPair.second;
      Bundle bundle = activityOptions.toBundle();
      context.startIntentSender(intentSender, intent, 0, 0, 0, bundle);
      return true;
    } catch (android.content.IntentSender.SendIntentException sendIntentException) {
      Log.e("RemoteViews", "Cannot send pending intent: ", (Throwable)sendIntentException);
      return false;
    } catch (Exception exception) {
      Log.e("RemoteViews", "Cannot send pending intent due to unknown exception: ", exception);
      return false;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ApplyFlags implements Annotation {}
  
  class OnClickHandler {
    public abstract boolean onClickHandler(View param1View, PendingIntent param1PendingIntent, RemoteViews.RemoteResponse param1RemoteResponse);
  }
  
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.TYPE})
  class RemoteView implements Annotation {}
}
