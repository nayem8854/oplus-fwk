package android.widget;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;

public abstract class SimpleCursorTreeAdapter extends ResourceCursorTreeAdapter {
  private int[] mChildFrom;
  
  private String[] mChildFromNames;
  
  private int[] mChildTo;
  
  private int[] mGroupFrom;
  
  private String[] mGroupFromNames;
  
  private int[] mGroupTo;
  
  private ViewBinder mViewBinder;
  
  public SimpleCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, int paramInt2, String[] paramArrayOfString1, int[] paramArrayOfint1, int paramInt3, int paramInt4, String[] paramArrayOfString2, int[] paramArrayOfint2) {
    super(paramContext, paramCursor, paramInt1, paramInt2, paramInt3, paramInt4);
    init(paramArrayOfString1, paramArrayOfint1, paramArrayOfString2, paramArrayOfint2);
  }
  
  public SimpleCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, int paramInt2, String[] paramArrayOfString1, int[] paramArrayOfint1, int paramInt3, String[] paramArrayOfString2, int[] paramArrayOfint2) {
    super(paramContext, paramCursor, paramInt1, paramInt2, paramInt3);
    init(paramArrayOfString1, paramArrayOfint1, paramArrayOfString2, paramArrayOfint2);
  }
  
  public SimpleCursorTreeAdapter(Context paramContext, Cursor paramCursor, int paramInt1, String[] paramArrayOfString1, int[] paramArrayOfint1, int paramInt2, String[] paramArrayOfString2, int[] paramArrayOfint2) {
    super(paramContext, paramCursor, paramInt1, paramInt2);
    init(paramArrayOfString1, paramArrayOfint1, paramArrayOfString2, paramArrayOfint2);
  }
  
  private void init(String[] paramArrayOfString1, int[] paramArrayOfint1, String[] paramArrayOfString2, int[] paramArrayOfint2) {
    this.mGroupFromNames = paramArrayOfString1;
    this.mGroupTo = paramArrayOfint1;
    this.mChildFromNames = paramArrayOfString2;
    this.mChildTo = paramArrayOfint2;
  }
  
  public ViewBinder getViewBinder() {
    return this.mViewBinder;
  }
  
  public void setViewBinder(ViewBinder paramViewBinder) {
    this.mViewBinder = paramViewBinder;
  }
  
  private void bindView(View paramView, Context paramContext, Cursor paramCursor, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    ViewBinder viewBinder = this.mViewBinder;
    for (byte b = 0; b < paramArrayOfint2.length; b++) {
      View view = (View)paramView.findViewById(paramArrayOfint2[b]);
      if (view != null) {
        boolean bool = false;
        if (viewBinder != null)
          bool = viewBinder.setViewValue(view, paramCursor, paramArrayOfint1[b]); 
        if (!bool) {
          String str2 = paramCursor.getString(paramArrayOfint1[b]);
          String str1 = str2;
          if (str2 == null)
            str1 = ""; 
          if (view instanceof TextView) {
            setViewText((TextView)view, str1);
          } else if (view instanceof ImageView) {
            setViewImage((ImageView)view, str1);
          } else {
            throw new IllegalStateException("SimpleCursorTreeAdapter can bind values only to TextView and ImageView!");
          } 
        } 
      } 
    } 
  }
  
  private void initFromColumns(Cursor paramCursor, String[] paramArrayOfString, int[] paramArrayOfint) {
    for (int i = paramArrayOfString.length - 1; i >= 0; i--)
      paramArrayOfint[i] = paramCursor.getColumnIndexOrThrow(paramArrayOfString[i]); 
  }
  
  protected void bindChildView(View paramView, Context paramContext, Cursor paramCursor, boolean paramBoolean) {
    if (this.mChildFrom == null) {
      String[] arrayOfString = this.mChildFromNames;
      int[] arrayOfInt = new int[arrayOfString.length];
      initFromColumns(paramCursor, arrayOfString, arrayOfInt);
    } 
    bindView(paramView, paramContext, paramCursor, this.mChildFrom, this.mChildTo);
  }
  
  protected void bindGroupView(View paramView, Context paramContext, Cursor paramCursor, boolean paramBoolean) {
    if (this.mGroupFrom == null) {
      String[] arrayOfString = this.mGroupFromNames;
      int[] arrayOfInt = new int[arrayOfString.length];
      initFromColumns(paramCursor, arrayOfString, arrayOfInt);
    } 
    bindView(paramView, paramContext, paramCursor, this.mGroupFrom, this.mGroupTo);
  }
  
  protected void setViewImage(ImageView paramImageView, String paramString) {
    try {
      paramImageView.setImageResource(Integer.parseInt(paramString));
    } catch (NumberFormatException numberFormatException) {
      paramImageView.setImageURI(Uri.parse(paramString));
    } 
  }
  
  public void setViewText(TextView paramTextView, String paramString) {
    paramTextView.setText(paramString);
  }
  
  class ViewBinder {
    public abstract boolean setViewValue(View param1View, Cursor param1Cursor, int param1Int);
  }
}
