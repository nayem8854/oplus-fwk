package android.widget;

import android.R;
import android.animation.ValueAnimator;
import android.app.AppGlobals;
import android.app.PendingIntent;
import android.app.RemoteAction;
import android.common.OplusFeatureCache;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.UndoManager;
import android.content.UndoOperation;
import android.content.UndoOwner;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.RenderNode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ParcelableParcel;
import android.os.SystemClock;
import android.text.DynamicLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Layout;
import android.text.ParcelableSpan;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.text.method.KeyListener;
import android.text.method.MetaKeyKeyListener;
import android.text.method.WordIterator;
import android.text.style.EasyEditSpan;
import android.text.style.SuggestionRangeSpan;
import android.text.style.SuggestionSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextThemeWrapper;
import android.view.DragAndDropPermissions;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputMethodManager;
import android.view.textclassifier.TextClassification;
import android.view.textclassifier.TextClassificationConstants;
import android.view.textclassifier.TextClassificationManager;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.util.GrowingArrayUtils;
import com.android.internal.util.Preconditions;
import com.android.internal.view.FloatingActionMode;
import com.android.internal.widget.EditableInputConnection;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Editor {
  static final int BLINK = 500;
  
  private static final boolean DEBUG_UNDO = false;
  
  private static final int DELAY_BEFORE_HANDLE_FADES_OUT = 4000;
  
  private static final int DRAG_SHADOW_MAX_TEXT_LENGTH = 20;
  
  static final int EXTRACT_NOTHING = -2;
  
  static final int EXTRACT_UNKNOWN = -1;
  
  private static final boolean FLAG_USE_MAGNIFIER = true;
  
  public static final int HANDLE_TYPE_SELECTION_END = 1;
  
  public static final int HANDLE_TYPE_SELECTION_START = 0;
  
  private static final int LINE_CHANGE_SLOP_MAX_DP = 45;
  
  private static final int LINE_CHANGE_SLOP_MIN_DP = 12;
  
  private static final float LINE_SLOP_MULTIPLIER_FOR_HANDLEVIEWS = 0.5F;
  
  private static final int MAX_LINE_HEIGHT_FOR_MAGNIFIER = 32;
  
  private static final int MENU_ITEM_ORDER_ASSIST = 0;
  
  private static final int MENU_ITEM_ORDER_AUTOFILL = 10;
  
  private static final int MENU_ITEM_ORDER_COPY = 5;
  
  private static final int MENU_ITEM_ORDER_CUT = 4;
  
  private static final int MENU_ITEM_ORDER_PASTE = 6;
  
  private static final int MENU_ITEM_ORDER_PASTE_AS_PLAIN_TEXT = 11;
  
  private static final int MENU_ITEM_ORDER_PROCESS_TEXT_INTENT_ACTIONS_START = 100;
  
  private static final int MENU_ITEM_ORDER_REDO = 3;
  
  private static final int MENU_ITEM_ORDER_REPLACE = 9;
  
  private static final int MENU_ITEM_ORDER_SECONDARY_ASSIST_ACTIONS_START = 50;
  
  private static final int MENU_ITEM_ORDER_SELECT = 11;
  
  private static final int MENU_ITEM_ORDER_SELECT_ALL = 8;
  
  private static final int MENU_ITEM_ORDER_SHARE = 7;
  
  private static final int MENU_ITEM_ORDER_UNDO = 2;
  
  private static final int MIN_LINE_HEIGHT_FOR_MAGNIFIER = 20;
  
  private static final int RECENT_CUT_COPY_DURATION_MS = 15000;
  
  private static final String TAG = "Editor";
  
  private static final String UNDO_OWNER_TAG = "Editor";
  
  private static final int UNSET_LINE = -1;
  
  private static final int UNSET_X_VALUE = -1;
  
  boolean mAllowUndo;
  
  private Blink mBlink;
  
  private float mContextMenuAnchorX;
  
  private float mContextMenuAnchorY;
  
  private CorrectionHighlighter mCorrectionHighlighter;
  
  boolean mCreatedWithASelection;
  
  private final CursorAnchorInfoNotifier mCursorAnchorInfoNotifier;
  
  boolean mCursorVisible;
  
  ActionMode.Callback mCustomInsertionActionModeCallback;
  
  ActionMode.Callback mCustomSelectionActionModeCallback;
  
  boolean mDiscardNextActionUp;
  
  Drawable mDrawableForCursor;
  
  CharSequence mError;
  
  private ErrorPopup mErrorPopup;
  
  boolean mErrorWasChanged;
  
  private boolean mFlagCursorDragFromAnywhereEnabled;
  
  private boolean mFlagInsertionHandleGesturesEnabled;
  
  boolean mFrozenWithFocus;
  
  private final boolean mHapticTextHandleEnabled;
  
  boolean mIgnoreActionUpEvent;
  
  boolean mInBatchEditControllers;
  
  private float mInitialZoom;
  
  InputContentType mInputContentType;
  
  InputMethodState mInputMethodState;
  
  int mInputType;
  
  private Runnable mInsertionActionModeRunnable;
  
  private boolean mInsertionControllerEnabled;
  
  InsertionPointCursorController mInsertionPointCursorController;
  
  boolean mIsBeingLongClicked;
  
  boolean mIsBeingLongClickedByAccessibility;
  
  private boolean mIsFousedBeforeTouch;
  
  boolean mIsInsertionActionModeStartPending;
  
  KeyListener mKeyListener;
  
  private int mLastButtonState;
  
  private int mLineChangeSlopMax;
  
  private int mLineChangeSlopMin;
  
  private MagnifierMotionAnimator mMagnifierAnimator;
  
  private final ViewTreeObserver.OnDrawListener mMagnifierOnDrawListener;
  
  private int mMaxLineHeightForMagnifier;
  
  private final MetricsLogger mMetricsLogger;
  
  private int mMinLineHeightForMagnifier;
  
  private final boolean mNewMagnifierEnabled;
  
  private final MenuItem.OnMenuItemClickListener mOnContextMenuItemClickListener;
  
  private OplusEditorUtils mOplusEditorUtils;
  
  private PositionListener mPositionListener;
  
  private boolean mPreserveSelection;
  
  final ProcessTextIntentActionsHandler mProcessTextIntentActionsHandler;
  
  private boolean mRenderCursorRegardlessTiming;
  
  private boolean mRequestingLinkActionMode;
  
  private boolean mRestartActionModeOnNextRefresh;
  
  boolean mSelectAllOnFocus;
  
  Drawable mSelectHandleCenter;
  
  Drawable mSelectHandleLeft;
  
  Drawable mSelectHandleRight;
  
  private SelectionActionModeHelper mSelectionActionModeHelper;
  
  private boolean mSelectionControllerEnabled;
  
  SelectionModifierCursorController mSelectionModifierCursorController;
  
  boolean mSelectionMoved;
  
  private long mShowCursor;
  
  private boolean mShowErrorAfterAttach;
  
  private final Runnable mShowFloatingToolbar;
  
  boolean mShowSoftInputOnFocus;
  
  private Runnable mShowSuggestionRunnable;
  
  private SpanController mSpanController;
  
  SpellChecker mSpellChecker;
  
  private final SuggestionHelper mSuggestionHelper;
  
  SuggestionRangeSpan mSuggestionRangeSpan;
  
  private SuggestionsPopupWindow mSuggestionsPopupWindow;
  
  private Rect mTempRect;
  
  private ActionMode mTextActionMode;
  
  boolean mTextIsSelectable;
  
  private TextRenderNode[] mTextRenderNodes;
  
  private final TextView mTextView;
  
  boolean mTouchFocusSelected;
  
  private final EditorTouchState mTouchState;
  
  final UndoInputFilter mUndoInputFilter;
  
  private final UndoManager mUndoManager;
  
  private UndoOwner mUndoOwner;
  
  private final Runnable mUpdateMagnifierRunnable;
  
  private boolean mUpdateWordIteratorText;
  
  private WordIterator mWordIterator;
  
  private WordIterator mWordIteratorWithText;
  
  class TextRenderNode {
    boolean isDirty;
    
    boolean needsToBeShifted;
    
    RenderNode renderNode;
    
    public TextRenderNode(Editor this$0) {
      this.renderNode = RenderNode.create((String)this$0, null);
      this.isDirty = true;
      this.needsToBeShifted = true;
    }
    
    boolean needsRecord() {
      return (this.isDirty || !this.renderNode.hasDisplayList());
    }
  }
  
  Editor(TextView paramTextView) {
    UndoManager undoManager = new UndoManager();
    this.mUndoOwner = undoManager.getOwner("Editor", this);
    this.mUndoInputFilter = new UndoInputFilter(this);
    this.mAllowUndo = true;
    this.mMetricsLogger = new MetricsLogger();
    this.mUpdateMagnifierRunnable = (Runnable)new Object(this);
    this.mMagnifierOnDrawListener = (ViewTreeObserver.OnDrawListener)new Object(this);
    boolean bool1 = false;
    this.mInputType = 0;
    this.mCursorVisible = true;
    this.mShowSoftInputOnFocus = true;
    this.mDrawableForCursor = null;
    this.mTouchState = new EditorTouchState();
    this.mCursorAnchorInfoNotifier = new CursorAnchorInfoNotifier();
    this.mShowFloatingToolbar = (Runnable)new Object(this);
    this.mIsInsertionActionModeStartPending = false;
    this.mSuggestionHelper = new SuggestionHelper();
    this.mInitialZoom = 1.0F;
    this.mOnContextMenuItemClickListener = (MenuItem.OnMenuItemClickListener)new Object(this);
    this.mIsFousedBeforeTouch = false;
    this.mTextView = paramTextView;
    paramTextView.setFilters(paramTextView.getFilters());
    this.mProcessTextIntentActionsHandler = new ProcessTextIntentActionsHandler();
    this.mHapticTextHandleEnabled = this.mTextView.getContext().getResources().getBoolean(17891443);
    if (AppGlobals.getIntCoreSetting("widget__enable_cursor_drag_from_anywhere", 1) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mFlagCursorDragFromAnywhereEnabled = bool2;
    if (AppGlobals.getIntCoreSetting("widget__enable_insertion_handle_gestures", 0) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mFlagInsertionHandleGesturesEnabled = bool2;
    boolean bool2 = bool1;
    if (AppGlobals.getIntCoreSetting("widget__enable_new_magnifier", 0) != 0)
      bool2 = true; 
    this.mNewMagnifierEnabled = bool2;
    this.mOplusEditorUtils = new OplusEditorUtils(this);
    paramTextView = this.mTextView;
    DisplayMetrics displayMetrics2 = paramTextView.getContext().getResources().getDisplayMetrics();
    this.mLineChangeSlopMax = (int)TypedValue.applyDimension(1, 45.0F, displayMetrics2);
    TextView textView = this.mTextView;
    DisplayMetrics displayMetrics1 = textView.getContext().getResources().getDisplayMetrics();
    this.mLineChangeSlopMin = (int)TypedValue.applyDimension(1, 12.0F, displayMetrics1);
  }
  
  public boolean getFlagCursorDragFromAnywhereEnabled() {
    return this.mFlagCursorDragFromAnywhereEnabled;
  }
  
  public void setFlagCursorDragFromAnywhereEnabled(boolean paramBoolean) {
    this.mFlagCursorDragFromAnywhereEnabled = paramBoolean;
  }
  
  public boolean getFlagInsertionHandleGesturesEnabled() {
    return this.mFlagInsertionHandleGesturesEnabled;
  }
  
  public void setFlagInsertionHandleGesturesEnabled(boolean paramBoolean) {
    this.mFlagInsertionHandleGesturesEnabled = paramBoolean;
  }
  
  private MagnifierMotionAnimator getMagnifierAnimator() {
    if (this.mMagnifierAnimator == null) {
      Magnifier.Builder builder;
      if (this.mNewMagnifierEnabled) {
        builder = createBuilderWithInlineMagnifierDefaults();
      } else {
        builder = Magnifier.createBuilderWithOldMagnifierDefaults(this.mTextView);
      } 
      this.mMagnifierAnimator = new MagnifierMotionAnimator();
    } 
    return this.mMagnifierAnimator;
  }
  
  private Magnifier.Builder createBuilderWithInlineMagnifierDefaults() {
    // Byte code:
    //   0: new android/widget/Magnifier$Builder
    //   3: dup
    //   4: aload_0
    //   5: getfield mTextView : Landroid/widget/TextView;
    //   8: invokespecial <init> : (Landroid/view/View;)V
    //   11: astore_1
    //   12: ldc_w 'widget__magnifier_zoom_factor'
    //   15: ldc_w 1.5
    //   18: invokestatic getFloatCoreSetting : (Ljava/lang/String;F)F
    //   21: fstore_2
    //   22: ldc_w 'widget__magnifier_aspect_ratio'
    //   25: ldc_w 5.5
    //   28: invokestatic getFloatCoreSetting : (Ljava/lang/String;F)F
    //   31: fstore_3
    //   32: fload_2
    //   33: ldc_w 1.2
    //   36: fcmpg
    //   37: iflt -> 51
    //   40: fload_2
    //   41: fstore #4
    //   43: fload_2
    //   44: ldc_w 1.8
    //   47: fcmpl
    //   48: ifle -> 56
    //   51: ldc_w 1.5
    //   54: fstore #4
    //   56: fload_3
    //   57: ldc_w 3.0
    //   60: fcmpg
    //   61: iflt -> 74
    //   64: fload_3
    //   65: fstore_2
    //   66: fload_3
    //   67: ldc_w 8.0
    //   70: fcmpl
    //   71: ifle -> 78
    //   74: ldc_w 5.5
    //   77: fstore_2
    //   78: aload_0
    //   79: fload #4
    //   81: putfield mInitialZoom : F
    //   84: aload_0
    //   85: getfield mTextView : Landroid/widget/TextView;
    //   88: astore #5
    //   90: aload #5
    //   92: invokevirtual getContext : ()Landroid/content/Context;
    //   95: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   98: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   101: astore #5
    //   103: aload_0
    //   104: iconst_1
    //   105: ldc_w 20.0
    //   108: aload #5
    //   110: invokestatic applyDimension : (IFLandroid/util/DisplayMetrics;)F
    //   113: f2i
    //   114: putfield mMinLineHeightForMagnifier : I
    //   117: aload_0
    //   118: getfield mTextView : Landroid/widget/TextView;
    //   121: astore #5
    //   123: aload #5
    //   125: invokevirtual getContext : ()Landroid/content/Context;
    //   128: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   131: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   134: astore #5
    //   136: aload_0
    //   137: iconst_1
    //   138: ldc_w 32.0
    //   141: aload #5
    //   143: invokestatic applyDimension : (IFLandroid/util/DisplayMetrics;)F
    //   146: f2i
    //   147: putfield mMaxLineHeightForMagnifier : I
    //   150: aload_0
    //   151: getfield mTextView : Landroid/widget/TextView;
    //   154: invokevirtual getLayout : ()Landroid/text/Layout;
    //   157: astore #5
    //   159: aload #5
    //   161: aload_0
    //   162: getfield mTextView : Landroid/widget/TextView;
    //   165: invokevirtual getSelectionStart : ()I
    //   168: invokevirtual getLineForOffset : (I)I
    //   171: istore #6
    //   173: aload #5
    //   175: iload #6
    //   177: invokevirtual getLineBottomWithoutSpacing : (I)I
    //   180: aload #5
    //   182: iload #6
    //   184: invokevirtual getLineTop : (I)I
    //   187: isub
    //   188: istore #7
    //   190: iload #7
    //   192: i2f
    //   193: fload #4
    //   195: fmul
    //   196: f2i
    //   197: istore #8
    //   199: iload #7
    //   201: aload_0
    //   202: getfield mMinLineHeightForMagnifier : I
    //   205: invokestatic max : (II)I
    //   208: i2f
    //   209: fload_2
    //   210: fmul
    //   211: f2i
    //   212: istore #6
    //   214: aload_1
    //   215: invokevirtual setFishEyeStyle : ()Landroid/widget/Magnifier$Builder;
    //   218: astore #5
    //   220: aload #5
    //   222: iload #6
    //   224: iload #8
    //   226: invokevirtual setSize : (II)Landroid/widget/Magnifier$Builder;
    //   229: astore #5
    //   231: aload #5
    //   233: iload #6
    //   235: iload #7
    //   237: invokevirtual setSourceSize : (II)Landroid/widget/Magnifier$Builder;
    //   240: astore #5
    //   242: aload #5
    //   244: fconst_0
    //   245: invokevirtual setElevation : (F)Landroid/widget/Magnifier$Builder;
    //   248: astore #5
    //   250: aload #5
    //   252: fload #4
    //   254: invokevirtual setInitialZoom : (F)Landroid/widget/Magnifier$Builder;
    //   257: astore #5
    //   259: aload #5
    //   261: iconst_0
    //   262: invokevirtual setClippingEnabled : (Z)Landroid/widget/Magnifier$Builder;
    //   265: pop
    //   266: aload_0
    //   267: getfield mTextView : Landroid/widget/TextView;
    //   270: invokevirtual getContext : ()Landroid/content/Context;
    //   273: astore #5
    //   275: aload #5
    //   277: aconst_null
    //   278: getstatic com/android/internal/R$styleable.Magnifier : [I
    //   281: ldc_w 17956984
    //   284: iconst_0
    //   285: invokevirtual obtainStyledAttributes : (Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    //   288: astore #5
    //   290: aload #5
    //   292: iconst_3
    //   293: iconst_0
    //   294: invokevirtual getDimensionPixelSize : (II)I
    //   297: istore #8
    //   299: aload #5
    //   301: iconst_4
    //   302: iconst_0
    //   303: invokevirtual getDimensionPixelSize : (II)I
    //   306: istore #6
    //   308: aload_1
    //   309: iload #8
    //   311: iload #6
    //   313: invokevirtual setDefaultSourceToMagnifierOffset : (II)Landroid/widget/Magnifier$Builder;
    //   316: pop
    //   317: aload #5
    //   319: invokevirtual recycle : ()V
    //   322: aload_1
    //   323: iconst_1
    //   324: iconst_0
    //   325: iconst_1
    //   326: iconst_0
    //   327: invokevirtual setSourceBounds : (IIII)Landroid/widget/Magnifier$Builder;
    //   330: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #495	-> 0
    //   #497	-> 12
    //   #500	-> 22
    //   #504	-> 32
    //   #505	-> 51
    //   #507	-> 56
    //   #508	-> 74
    //   #511	-> 78
    //   #512	-> 84
    //   #514	-> 90
    //   #512	-> 103
    //   #515	-> 117
    //   #517	-> 123
    //   #515	-> 136
    //   #519	-> 150
    //   #520	-> 159
    //   #521	-> 173
    //   #522	-> 173
    //   #523	-> 190
    //   #524	-> 199
    //   #526	-> 214
    //   #527	-> 220
    //   #528	-> 231
    //   #529	-> 242
    //   #530	-> 250
    //   #531	-> 259
    //   #533	-> 266
    //   #534	-> 275
    //   #537	-> 290
    //   #538	-> 290
    //   #540	-> 299
    //   #537	-> 308
    //   #542	-> 317
    //   #544	-> 322
  }
  
  ParcelableParcel saveInstanceState() {
    ParcelableParcel parcelableParcel = new ParcelableParcel(getClass().getClassLoader());
    Parcel parcel = parcelableParcel.getParcel();
    this.mUndoManager.saveInstanceState(parcel);
    this.mUndoInputFilter.saveInstanceState(parcel);
    return parcelableParcel;
  }
  
  void restoreInstanceState(ParcelableParcel paramParcelableParcel) {
    Parcel parcel = paramParcelableParcel.getParcel();
    this.mUndoManager.restoreInstanceState(parcel, paramParcelableParcel.getClassLoader());
    this.mUndoInputFilter.restoreInstanceState(parcel);
    this.mUndoOwner = this.mUndoManager.getOwner("Editor", this);
  }
  
  void forgetUndoRedo() {
    UndoOwner[] arrayOfUndoOwner = new UndoOwner[1];
    arrayOfUndoOwner[0] = this.mUndoOwner;
    this.mUndoManager.forgetUndos(arrayOfUndoOwner, -1);
    this.mUndoManager.forgetRedos(arrayOfUndoOwner, -1);
  }
  
  boolean canUndo() {
    boolean bool = true;
    UndoOwner undoOwner = this.mUndoOwner;
    if (!this.mAllowUndo || this.mUndoManager.countUndos(new UndoOwner[] { undoOwner }) <= 0)
      bool = false; 
    return bool;
  }
  
  boolean canRedo() {
    boolean bool = true;
    UndoOwner undoOwner = this.mUndoOwner;
    if (!this.mAllowUndo || this.mUndoManager.countRedos(new UndoOwner[] { undoOwner }) <= 0)
      bool = false; 
    return bool;
  }
  
  void undo() {
    if (!this.mAllowUndo)
      return; 
    UndoOwner undoOwner = this.mUndoOwner;
    this.mUndoManager.undo(new UndoOwner[] { undoOwner }, 1);
  }
  
  void redo() {
    if (!this.mAllowUndo)
      return; 
    UndoOwner undoOwner = this.mUndoOwner;
    this.mUndoManager.redo(new UndoOwner[] { undoOwner }, 1);
  }
  
  void replace() {
    if (this.mSuggestionsPopupWindow == null)
      this.mSuggestionsPopupWindow = new SuggestionsPopupWindow(); 
    hideCursorAndSpanControllers();
    this.mSuggestionsPopupWindow.show();
    int i = (this.mTextView.getSelectionStart() + this.mTextView.getSelectionEnd()) / 2;
    Selection.setSelection((Spannable)this.mTextView.getText(), i);
  }
  
  void onAttachedToWindow() {
    if (this.mShowErrorAfterAttach) {
      showError();
      this.mShowErrorAfterAttach = false;
    } 
    ViewTreeObserver viewTreeObserver = this.mTextView.getViewTreeObserver();
    if (viewTreeObserver.isAlive()) {
      InsertionPointCursorController insertionPointCursorController = this.mInsertionPointCursorController;
      if (insertionPointCursorController != null)
        viewTreeObserver.addOnTouchModeChangeListener(insertionPointCursorController); 
      SelectionModifierCursorController selectionModifierCursorController = this.mSelectionModifierCursorController;
      if (selectionModifierCursorController != null) {
        selectionModifierCursorController.resetTouchOffsets();
        viewTreeObserver.addOnTouchModeChangeListener(this.mSelectionModifierCursorController);
      } 
      viewTreeObserver.addOnDrawListener(this.mMagnifierOnDrawListener);
    } 
    updateSpellCheckSpans(0, this.mTextView.getText().length(), true);
    if (this.mTextView.hasSelection())
      refreshTextActionMode(); 
    getPositionListener().addSubscriber(this.mCursorAnchorInfoNotifier, true);
    resumeBlink();
  }
  
  void onDetachedFromWindow() {
    getPositionListener().removeSubscriber(this.mCursorAnchorInfoNotifier);
    if (this.mError != null)
      hideError(); 
    suspendBlink();
    InsertionPointCursorController insertionPointCursorController = this.mInsertionPointCursorController;
    if (insertionPointCursorController != null)
      insertionPointCursorController.onDetached(); 
    SelectionModifierCursorController selectionModifierCursorController = this.mSelectionModifierCursorController;
    if (selectionModifierCursorController != null)
      selectionModifierCursorController.onDetached(); 
    Runnable runnable = this.mShowSuggestionRunnable;
    if (runnable != null)
      this.mTextView.removeCallbacks(runnable); 
    runnable = this.mInsertionActionModeRunnable;
    if (runnable != null)
      this.mTextView.removeCallbacks(runnable); 
    this.mTextView.removeCallbacks(this.mShowFloatingToolbar);
    discardTextDisplayLists();
    SpellChecker spellChecker = this.mSpellChecker;
    if (spellChecker != null) {
      spellChecker.closeSession();
      this.mSpellChecker = null;
    } 
    ViewTreeObserver viewTreeObserver = this.mTextView.getViewTreeObserver();
    if (viewTreeObserver.isAlive())
      viewTreeObserver.removeOnDrawListener(this.mMagnifierOnDrawListener); 
    hideCursorAndSpanControllers();
    stopTextActionModeWithPreservingSelection();
  }
  
  private void discardTextDisplayLists() {
    if (this.mTextRenderNodes != null) {
      byte b = 0;
      while (true) {
        TextRenderNode[] arrayOfTextRenderNode = this.mTextRenderNodes;
        if (b < arrayOfTextRenderNode.length) {
          if (arrayOfTextRenderNode[b] != null) {
            RenderNode renderNode = (arrayOfTextRenderNode[b]).renderNode;
          } else {
            arrayOfTextRenderNode = null;
          } 
          if (arrayOfTextRenderNode != null && arrayOfTextRenderNode.hasDisplayList())
            arrayOfTextRenderNode.discardDisplayList(); 
          b++;
          continue;
        } 
        break;
      } 
    } 
  }
  
  private void showError() {
    if (this.mTextView.getWindowToken() == null) {
      this.mShowErrorAfterAttach = true;
      return;
    } 
    if (this.mErrorPopup == null) {
      LayoutInflater layoutInflater = LayoutInflater.from(this.mTextView.getContext());
      TextView textView1 = (TextView)layoutInflater.inflate(17367334, (ViewGroup)null);
      float f = (this.mTextView.getResources().getDisplayMetrics()).density;
      ErrorPopup errorPopup1 = new ErrorPopup(textView1, (int)(200.0F * f + 0.5F), (int)(50.0F * f + 0.5F));
      errorPopup1.setFocusable(false);
      this.mErrorPopup.setInputMethodMode(1);
    } 
    TextView textView = (TextView)this.mErrorPopup.getContentView();
    chooseSize(this.mErrorPopup, this.mError, textView);
    textView.setText(this.mError);
    this.mErrorPopup.showAsDropDown(this.mTextView, getErrorX(), getErrorY(), 51);
    ErrorPopup errorPopup = this.mErrorPopup;
    errorPopup.fixDirection(errorPopup.isAboveAnchor());
  }
  
  public void setError(CharSequence paramCharSequence, Drawable paramDrawable) {
    this.mError = paramCharSequence = TextUtils.stringOrSpannedString(paramCharSequence);
    this.mErrorWasChanged = true;
    if (paramCharSequence == null) {
      setErrorIcon(null);
      ErrorPopup errorPopup = this.mErrorPopup;
      if (errorPopup != null) {
        if (errorPopup.isShowing())
          this.mErrorPopup.dismiss(); 
        this.mErrorPopup = null;
      } 
      this.mShowErrorAfterAttach = false;
    } else {
      setErrorIcon(paramDrawable);
      if (this.mTextView.isFocused())
        showError(); 
    } 
  }
  
  private void setErrorIcon(Drawable paramDrawable) {
    TextView.Drawables drawables1 = this.mTextView.mDrawables;
    TextView.Drawables drawables2 = drawables1;
    if (drawables1 == null) {
      TextView textView = this.mTextView;
      drawables2 = drawables1 = new TextView.Drawables(textView.getContext());
      textView.mDrawables = drawables1;
    } 
    drawables2.setErrorDrawable(paramDrawable, this.mTextView);
    this.mTextView.resetResolvedDrawables();
    this.mTextView.invalidate();
    this.mTextView.requestLayout();
  }
  
  private void hideError() {
    ErrorPopup errorPopup = this.mErrorPopup;
    if (errorPopup != null && errorPopup.isShowing())
      this.mErrorPopup.dismiss(); 
    this.mShowErrorAfterAttach = false;
  }
  
  private int getErrorX() {
    TextView textView;
    float f = (this.mTextView.getResources().getDisplayMetrics()).density;
    TextView.Drawables drawables = this.mTextView.mDrawables;
    int i = this.mTextView.getLayoutDirection();
    int j = 0, k = 0;
    if (i != 1) {
      j = k;
      if (drawables != null)
        j = drawables.mDrawableSizeRight; 
      k = -j / 2;
      j = (int)(25.0F * f + 0.5F);
      i = this.mTextView.getWidth();
      int m = this.mErrorPopup.getWidth();
      textView = this.mTextView;
      j = i - m - textView.getPaddingRight() + k + j;
    } else {
      if (textView != null)
        j = ((TextView.Drawables)textView).mDrawableSizeLeft; 
      k = j / 2;
      j = (int)(25.0F * f + 0.5F);
      j = this.mTextView.getPaddingLeft() + k - j;
    } 
    return j;
  }
  
  private int getErrorY() {
    int i = this.mTextView.getCompoundPaddingTop();
    int j = this.mTextView.getBottom(), k = this.mTextView.getTop();
    TextView textView = this.mTextView;
    int m = textView.getCompoundPaddingBottom();
    TextView.Drawables drawables = this.mTextView.mDrawables;
    int n = this.mTextView.getLayoutDirection();
    int i1 = 0, i2 = 0;
    if (n != 1) {
      i1 = i2;
      if (drawables != null)
        i1 = drawables.mDrawableHeightRight; 
    } else if (drawables != null) {
      i1 = drawables.mDrawableHeightLeft;
    } 
    i2 = (j - k - m - i - i1) / 2;
    float f = (this.mTextView.getResources().getDisplayMetrics()).density;
    return i2 + i + i1 - this.mTextView.getHeight() - (int)(2.0F * f + 0.5F);
  }
  
  void createInputContentTypeIfNeeded() {
    if (this.mInputContentType == null)
      this.mInputContentType = new InputContentType(); 
  }
  
  void createInputMethodStateIfNeeded() {
    if (this.mInputMethodState == null)
      this.mInputMethodState = new InputMethodState(); 
  }
  
  private boolean isCursorVisible() {
    boolean bool;
    if (this.mCursorVisible && this.mTextView.isTextEditable()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean shouldRenderCursor() {
    boolean bool = isCursorVisible();
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (this.mRenderCursorRegardlessTiming)
      return true; 
    long l1 = SystemClock.uptimeMillis(), l2 = this.mShowCursor;
    if ((l1 - l2) % 1000L < 500L)
      bool1 = true; 
    return bool1;
  }
  
  void prepareCursorControllers() {
    boolean bool = false;
    ViewGroup.LayoutParams layoutParams = this.mTextView.getRootView().getLayoutParams();
    if (layoutParams instanceof WindowManager.LayoutParams) {
      layoutParams = layoutParams;
      if (((WindowManager.LayoutParams)layoutParams).type < 1000 || ((WindowManager.LayoutParams)layoutParams).type > 1999) {
        bool = true;
      } else {
        bool = false;
      } 
    } 
    if (bool && this.mTextView.getLayout() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool && isCursorVisible()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    this.mInsertionControllerEnabled = bool1;
    if (bool && this.mTextView.textCanBeSelected()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    this.mSelectionControllerEnabled = bool1;
    IOplusFloatingToolbarUtil iOplusFloatingToolbarUtil = (IOplusFloatingToolbarUtil)OplusFeatureCache.getOrCreate(IOplusFloatingToolbarUtil.DEFAULT, new Object[0]);
    OplusEditorUtils oplusEditorUtils = this.mOplusEditorUtils;
    boolean bool2 = oplusEditorUtils.isMenuEnabled(), bool1 = this.mOplusEditorUtils.isInsertMenuEnabled(), bool3 = this.mOplusEditorUtils.isSelectMenuEnabled(), bool4 = this.mInsertionControllerEnabled, bool5 = this.mSelectionControllerEnabled;
    boolean[] arrayOfBoolean = iOplusFloatingToolbarUtil.handleCursorControllersEnabled(bool2, bool1, bool3, bool4, bool5);
    this.mInsertionControllerEnabled = bool1 = arrayOfBoolean[0];
    this.mSelectionControllerEnabled = arrayOfBoolean[1];
    if (!bool1) {
      hideInsertionPointCursorController();
      InsertionPointCursorController insertionPointCursorController = this.mInsertionPointCursorController;
      if (insertionPointCursorController != null) {
        insertionPointCursorController.onDetached();
        this.mInsertionPointCursorController = null;
      } 
    } 
    if (!this.mSelectionControllerEnabled) {
      stopTextActionMode();
      SelectionModifierCursorController selectionModifierCursorController = this.mSelectionModifierCursorController;
      if (selectionModifierCursorController != null) {
        selectionModifierCursorController.onDetached();
        this.mSelectionModifierCursorController = null;
      } 
    } 
  }
  
  void hideInsertionPointCursorController() {
    InsertionPointCursorController insertionPointCursorController = this.mInsertionPointCursorController;
    if (insertionPointCursorController != null)
      insertionPointCursorController.hide(); 
  }
  
  void hideCursorAndSpanControllers() {
    hideCursorControllers();
    hideSpanControllers();
  }
  
  private void hideSpanControllers() {
    SpanController spanController = this.mSpanController;
    if (spanController != null)
      spanController.hide(); 
  }
  
  private void hideCursorControllers() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSuggestionsPopupWindow : Landroid/widget/Editor$SuggestionsPopupWindow;
    //   4: ifnull -> 36
    //   7: aload_0
    //   8: getfield mTextView : Landroid/widget/TextView;
    //   11: invokevirtual isInExtractedMode : ()Z
    //   14: ifne -> 29
    //   17: aload_0
    //   18: getfield mSuggestionsPopupWindow : Landroid/widget/Editor$SuggestionsPopupWindow;
    //   21: astore_1
    //   22: aload_1
    //   23: invokevirtual isShowingUp : ()Z
    //   26: ifne -> 36
    //   29: aload_0
    //   30: getfield mSuggestionsPopupWindow : Landroid/widget/Editor$SuggestionsPopupWindow;
    //   33: invokevirtual hide : ()V
    //   36: aload_0
    //   37: invokevirtual hideInsertionPointCursorController : ()V
    //   40: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #939	-> 0
    //   #940	-> 22
    //   #942	-> 29
    //   #944	-> 36
    //   #945	-> 40
  }
  
  private void updateSpellCheckSpans(int paramInt1, int paramInt2, boolean paramBoolean) {
    this.mTextView.removeAdjacentSuggestionSpans(paramInt1);
    this.mTextView.removeAdjacentSuggestionSpans(paramInt2);
    if (this.mTextView.isTextEditable() && this.mTextView.isSuggestionsEnabled()) {
      TextView textView = this.mTextView;
      if (!textView.isInExtractedMode()) {
        if (this.mSpellChecker == null && paramBoolean)
          this.mSpellChecker = new SpellChecker(this.mTextView); 
        SpellChecker spellChecker = this.mSpellChecker;
        if (spellChecker != null)
          spellChecker.spellCheck(paramInt1, paramInt2); 
      } 
    } 
  }
  
  void onScreenStateChanged(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        resumeBlink(); 
    } else {
      suspendBlink();
    } 
  }
  
  private void suspendBlink() {
    Blink blink = this.mBlink;
    if (blink != null)
      blink.cancel(); 
  }
  
  private void resumeBlink() {
    Blink blink = this.mBlink;
    if (blink != null) {
      blink.uncancel();
      makeBlink();
    } 
  }
  
  void adjustInputType(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4) {
    int i = this.mInputType;
    if ((i & 0xF) == 1) {
      if (paramBoolean1 || paramBoolean2)
        this.mInputType = this.mInputType & 0xFFFFF00F | 0x80; 
      if (paramBoolean3)
        this.mInputType = this.mInputType & 0xFFFFF00F | 0xE0; 
    } else if ((i & 0xF) == 2 && paramBoolean4) {
      this.mInputType = i & 0xFFFFF00F | 0x10;
    } 
  }
  
  private void chooseSize(PopupWindow paramPopupWindow, CharSequence paramCharSequence, TextView paramTextView) {
    int i = paramTextView.getPaddingLeft(), j = paramTextView.getPaddingRight();
    int k = paramTextView.getPaddingTop(), m = paramTextView.getPaddingBottom();
    int n = this.mTextView.getResources().getDimensionPixelSize(17105515);
    StaticLayout.Builder builder = StaticLayout.Builder.obtain(paramCharSequence, 0, paramCharSequence.length(), paramTextView.getPaint(), n);
    boolean bool = paramTextView.mUseFallbackLineSpacing;
    builder = builder.setUseLineSpacingFromFallbacks(bool);
    StaticLayout staticLayout = builder.build();
    float f = 0.0F;
    for (n = 0; n < staticLayout.getLineCount(); n++)
      f = Math.max(f, staticLayout.getLineWidth(n)); 
    paramPopupWindow.setWidth((int)Math.ceil(f) + i + j);
    paramPopupWindow.setHeight(staticLayout.getHeight() + k + m);
  }
  
  void setFrame() {
    ErrorPopup errorPopup = this.mErrorPopup;
    if (errorPopup != null) {
      TextView textView = (TextView)errorPopup.getContentView();
      chooseSize(this.mErrorPopup, this.mError, textView);
      ErrorPopup errorPopup1 = this.mErrorPopup;
      textView = this.mTextView;
      int i = getErrorX(), j = getErrorY();
      ErrorPopup errorPopup2 = this.mErrorPopup;
      int k = errorPopup2.getWidth(), m = this.mErrorPopup.getHeight();
      errorPopup1.update(textView, i, j, k, m);
    } 
  }
  
  private int getWordStart(int paramInt) {
    int i = getWordIteratorWithText().prevBoundary(paramInt);
    if (getWordIteratorWithText().isOnPunctuation(i)) {
      i = getWordIteratorWithText().getPunctuationBeginning(paramInt);
    } else {
      i = getWordIteratorWithText().getPrevWordBeginningOnTwoWordsBoundary(paramInt);
    } 
    if (i == -1)
      return paramInt; 
    return i;
  }
  
  private int getWordEnd(int paramInt) {
    int i = getWordIteratorWithText().nextBoundary(paramInt);
    if (getWordIteratorWithText().isAfterPunctuation(i)) {
      i = getWordIteratorWithText().getPunctuationEnd(paramInt);
    } else {
      i = getWordIteratorWithText().getNextWordEndOnTwoWordBoundary(paramInt);
    } 
    if (i == -1)
      return paramInt; 
    return i;
  }
  
  private boolean needsToSelectAllToSelectWordOrParagraph() {
    if (((IOplusFloatingToolbarUtil)OplusFeatureCache.getOrCreate(IOplusFloatingToolbarUtil.DEFAULT, new Object[0])).needAllSelected(this.mOplusEditorUtils.needAllSelected()))
      return this.mTextView.selectAllText(); 
    if (this.mTextView.hasPasswordTransformationMethod())
      return true; 
    int i = this.mTextView.getInputType();
    int j = i & 0xF;
    i &= 0xFF0;
    if (j == 2 || j == 3 || j == 4 || i == 16 || i == 32 || i == 208 || i == 176)
      return true; 
    return false;
  }
  
  boolean selectCurrentWord() {
    int k;
    boolean bool = this.mTextView.canSelectText();
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (needsToSelectAllToSelectWordOrParagraph())
      return this.mTextView.selectAllText(); 
    long l = getLastTouchOffsets();
    int i = TextUtils.unpackRangeStartFromLong(l);
    int j = TextUtils.unpackRangeEndFromLong(l);
    if (i < 0 || i > this.mTextView.getText().length())
      return false; 
    if (j < 0 || j > this.mTextView.getText().length())
      return false; 
    TextView textView = this.mTextView;
    URLSpan[] arrayOfURLSpan = ((Spanned)textView.getText()).<URLSpan>getSpans(i, j, URLSpan.class);
    if (arrayOfURLSpan.length >= 1) {
      URLSpan uRLSpan = arrayOfURLSpan[0];
      k = ((Spanned)this.mTextView.getText()).getSpanStart(uRLSpan);
      j = ((Spanned)this.mTextView.getText()).getSpanEnd(uRLSpan);
    } else {
      WordIterator wordIterator = getWordIterator();
      wordIterator.setCharSequence(this.mTextView.getText(), i, j);
      k = wordIterator.getBeginning(i);
      j = wordIterator.getEnd(j);
      if (k == -1 || j == -1 || k == j) {
        l = getCharClusterRange(i);
        k = TextUtils.unpackRangeStartFromLong(l);
        j = TextUtils.unpackRangeEndFromLong(l);
      } 
    } 
    Selection.setSelection((Spannable)this.mTextView.getText(), k, j);
    if (j > k)
      bool1 = true; 
    return bool1;
  }
  
  private boolean selectCurrentParagraph() {
    if (!this.mTextView.canSelectText())
      return false; 
    if (needsToSelectAllToSelectWordOrParagraph())
      return this.mTextView.selectAllText(); 
    long l = getLastTouchOffsets();
    int i = TextUtils.unpackRangeStartFromLong(l);
    int j = TextUtils.unpackRangeEndFromLong(l);
    l = getParagraphsRange(i, j);
    j = TextUtils.unpackRangeStartFromLong(l);
    i = TextUtils.unpackRangeEndFromLong(l);
    if (j < i) {
      Selection.setSelection((Spannable)this.mTextView.getText(), j, i);
      return true;
    } 
    return false;
  }
  
  private long getParagraphsRange(int paramInt1, int paramInt2) {
    Layout layout = this.mTextView.getLayout();
    if (layout == null)
      return TextUtils.packRangeInLong(-1, -1); 
    CharSequence charSequence = this.mTextView.getText();
    paramInt1 = layout.getLineForOffset(paramInt1);
    while (paramInt1 > 0) {
      int i = layout.getLineEnd(paramInt1 - 1);
      if (charSequence.charAt(i - 1) == '\n')
        break; 
      paramInt1--;
    } 
    paramInt2 = layout.getLineForOffset(paramInt2);
    while (paramInt2 < layout.getLineCount() - 1) {
      int i = layout.getLineEnd(paramInt2);
      if (charSequence.charAt(i - 1) == '\n')
        break; 
      paramInt2++;
    } 
    return TextUtils.packRangeInLong(layout.getLineStart(paramInt1), layout.getLineEnd(paramInt2));
  }
  
  void onLocaleChanged() {
    this.mWordIterator = null;
    this.mWordIteratorWithText = null;
  }
  
  public WordIterator getWordIterator() {
    if (this.mWordIterator == null)
      this.mWordIterator = new WordIterator(this.mTextView.getTextServicesLocale()); 
    return this.mWordIterator;
  }
  
  private WordIterator getWordIteratorWithText() {
    if (this.mWordIteratorWithText == null) {
      this.mWordIteratorWithText = new WordIterator(this.mTextView.getTextServicesLocale());
      this.mUpdateWordIteratorText = true;
    } 
    if (this.mUpdateWordIteratorText) {
      CharSequence charSequence = this.mTextView.getText();
      this.mWordIteratorWithText.setCharSequence(charSequence, 0, charSequence.length());
      this.mUpdateWordIteratorText = false;
    } 
    return this.mWordIteratorWithText;
  }
  
  private int getNextCursorOffset(int paramInt, boolean paramBoolean) {
    Layout layout = this.mTextView.getLayout();
    if (layout == null)
      return paramInt; 
    if (paramBoolean == layout.isRtlCharAt(paramInt)) {
      paramInt = layout.getOffsetToLeftOf(paramInt);
    } else {
      paramInt = layout.getOffsetToRightOf(paramInt);
    } 
    return paramInt;
  }
  
  private long getCharClusterRange(int paramInt) {
    int i = this.mTextView.getText().length();
    if (paramInt < i) {
      paramInt = getNextCursorOffset(paramInt, true);
      i = getNextCursorOffset(paramInt, false);
      return TextUtils.packRangeInLong(i, paramInt);
    } 
    if (paramInt - 1 >= 0) {
      i = getNextCursorOffset(paramInt, false);
      paramInt = getNextCursorOffset(i, true);
      return TextUtils.packRangeInLong(i, paramInt);
    } 
    return TextUtils.packRangeInLong(paramInt, paramInt);
  }
  
  private boolean touchPositionIsInSelection() {
    int i = this.mTextView.getSelectionStart();
    int j = this.mTextView.getSelectionEnd();
    boolean bool1 = false;
    if (i == j)
      return false; 
    int k = i, m = j;
    if (i > j) {
      k = j;
      m = i;
      Selection.setSelection((Spannable)this.mTextView.getText(), k, m);
    } 
    SelectionModifierCursorController selectionModifierCursorController = getSelectionController();
    i = selectionModifierCursorController.getMinTouchOffset();
    j = selectionModifierCursorController.getMaxTouchOffset();
    boolean bool2 = bool1;
    if (i >= k) {
      bool2 = bool1;
      if (j < m)
        bool2 = true; 
    } 
    return bool2;
  }
  
  private PositionListener getPositionListener() {
    if (this.mPositionListener == null)
      this.mPositionListener = new PositionListener(); 
    return this.mPositionListener;
  }
  
  private boolean isOffsetVisible(int paramInt) {
    Layout layout = this.mTextView.getLayout();
    if (layout == null)
      return false; 
    int i = layout.getLineForOffset(paramInt);
    i = layout.getLineBottom(i);
    paramInt = (int)layout.getPrimaryHorizontal(paramInt);
    TextView textView1 = this.mTextView;
    float f1 = (textView1.viewportToContentHorizontalOffset() + paramInt);
    TextView textView2 = this.mTextView;
    float f2 = (textView2.viewportToContentVerticalOffset() + i);
    return textView1.isPositionVisible(f1, f2);
  }
  
  private boolean isPositionOnText(float paramFloat1, float paramFloat2) {
    Layout layout = this.mTextView.getLayout();
    if (layout == null)
      return false; 
    int i = this.mTextView.getLineAtCoordinate(paramFloat2);
    paramFloat1 = this.mTextView.convertToLocalHorizontalCoordinate(paramFloat1);
    if (paramFloat1 < layout.getLineLeft(i))
      return false; 
    if (paramFloat1 > layout.getLineRight(i))
      return false; 
    return true;
  }
  
  private void startDragAndDrop() {
    getSelectionActionModeHelper().onSelectionDrag();
    if (this.mTextView.isInExtractedMode())
      return; 
    int i = this.mTextView.getSelectionStart();
    int j = this.mTextView.getSelectionEnd();
    CharSequence charSequence = this.mTextView.getTransformedText(i, j);
    ClipData clipData = ClipData.newPlainText(null, charSequence);
    DragLocalState dragLocalState = new DragLocalState(this.mTextView, i, j);
    IOplusDragTextShadowHelper iOplusDragTextShadowHelper = IOplusDragTextShadowHelper.DEFAULT;
    iOplusDragTextShadowHelper = (IOplusDragTextShadowHelper)OplusFeatureCache.getOrCreate(iOplusDragTextShadowHelper, new Object[0]);
    TextView textView = this.mTextView;
    View.DragShadowBuilder dragShadowBuilder = iOplusDragTextShadowHelper.getColorTextThumbnailBuilder(textView, textView.getTransformedText(i, j).toString());
    textView = this.mTextView;
    if (dragShadowBuilder == null)
      dragShadowBuilder = getTextThumbnailBuilder(i, j); 
    textView.startDragAndDrop(clipData, dragShadowBuilder, dragLocalState, 768);
    stopTextActionMode();
    if (hasSelectionController())
      getSelectionController().resetTouchOffsets(); 
  }
  
  public boolean performLongClick(boolean paramBoolean) {
    if (this.mIsBeingLongClickedByAccessibility) {
      if (!paramBoolean)
        toggleInsertionActionMode(); 
      return true;
    } 
    boolean bool = paramBoolean;
    if (!paramBoolean) {
      bool = paramBoolean;
      if (!isPositionOnText(this.mTouchState.getLastDownX(), this.mTouchState.getLastDownY())) {
        EditorTouchState editorTouchState = this.mTouchState;
        bool = paramBoolean;
        if (!editorTouchState.isOnHandle()) {
          bool = paramBoolean;
          if (this.mInsertionControllerEnabled) {
            TextView textView2 = this.mTextView;
            float f1 = this.mTouchState.getLastDownX();
            editorTouchState = this.mTouchState;
            float f2 = editorTouchState.getLastDownY();
            int i = textView2.getOffsetForPosition(f1, f2);
            Selection.setSelection((Spannable)this.mTextView.getText(), i);
            getInsertionController().show();
            this.mIsInsertionActionModeStartPending = true;
            bool = true;
            TextView textView1 = this.mTextView;
            Context context = textView1.getContext();
            MetricsLogger.action(context, 629, 0);
          } 
        } 
      } 
    } 
    paramBoolean = bool;
    if (!bool) {
      paramBoolean = bool;
      if (this.mTextActionMode != null) {
        if (touchPositionIsInSelection()) {
          startDragAndDrop();
          TextView textView = this.mTextView;
          Context context = textView.getContext();
          MetricsLogger.action(context, 629, 2);
        } else {
          stopTextActionMode();
          selectCurrentWordAndStartDrag();
          TextView textView = this.mTextView;
          Context context = textView.getContext();
          MetricsLogger.action(context, 629, 1);
        } 
        paramBoolean = true;
      } 
    } 
    bool = paramBoolean;
    if (!paramBoolean) {
      paramBoolean = selectCurrentWordAndStartDrag();
      bool = paramBoolean;
      if (paramBoolean) {
        TextView textView = this.mTextView;
        Context context = textView.getContext();
        MetricsLogger.action(context, 629, 1);
        bool = paramBoolean;
      } 
    } 
    return bool;
  }
  
  private void toggleInsertionActionMode() {
    if (this.mTextActionMode != null) {
      stopTextActionMode();
    } else {
      startInsertionActionMode();
    } 
  }
  
  float getLastUpPositionX() {
    return this.mTouchState.getLastUpX();
  }
  
  float getLastUpPositionY() {
    return this.mTouchState.getLastUpY();
  }
  
  private long getLastTouchOffsets() {
    SelectionModifierCursorController selectionModifierCursorController = getSelectionController();
    int i = selectionModifierCursorController.getMinTouchOffset();
    int j = selectionModifierCursorController.getMaxTouchOffset();
    return TextUtils.packRangeInLong(i, j);
  }
  
  void onFocusChanged(boolean paramBoolean, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic uptimeMillis : ()J
    //   4: putfield mShowCursor : J
    //   7: aload_0
    //   8: invokevirtual ensureEndedBatchEdit : ()V
    //   11: iload_1
    //   12: ifeq -> 277
    //   15: aload_0
    //   16: getfield mTextView : Landroid/widget/TextView;
    //   19: invokevirtual getSelectionStart : ()I
    //   22: istore_3
    //   23: aload_0
    //   24: getfield mTextView : Landroid/widget/TextView;
    //   27: invokevirtual getSelectionEnd : ()I
    //   30: istore #4
    //   32: aload_0
    //   33: getfield mSelectAllOnFocus : Z
    //   36: ifeq -> 70
    //   39: iload_3
    //   40: ifne -> 70
    //   43: aload_0
    //   44: getfield mTextView : Landroid/widget/TextView;
    //   47: astore #5
    //   49: iload #4
    //   51: aload #5
    //   53: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   56: invokeinterface length : ()I
    //   61: if_icmpne -> 70
    //   64: iconst_1
    //   65: istore #6
    //   67: goto -> 73
    //   70: iconst_0
    //   71: istore #6
    //   73: aload_0
    //   74: getfield mFrozenWithFocus : Z
    //   77: ifeq -> 100
    //   80: aload_0
    //   81: getfield mTextView : Landroid/widget/TextView;
    //   84: invokevirtual hasSelection : ()Z
    //   87: ifeq -> 100
    //   90: iload #6
    //   92: ifne -> 100
    //   95: iconst_1
    //   96: istore_1
    //   97: goto -> 102
    //   100: iconst_0
    //   101: istore_1
    //   102: aload_0
    //   103: iload_1
    //   104: putfield mCreatedWithASelection : Z
    //   107: aload_0
    //   108: getfield mFrozenWithFocus : Z
    //   111: ifeq -> 123
    //   114: iload_3
    //   115: iflt -> 123
    //   118: iload #4
    //   120: ifge -> 249
    //   123: aload_0
    //   124: invokespecial getLastTapPosition : ()I
    //   127: istore #6
    //   129: iload #6
    //   131: iflt -> 149
    //   134: aload_0
    //   135: getfield mTextView : Landroid/widget/TextView;
    //   138: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   141: checkcast android/text/Spannable
    //   144: iload #6
    //   146: invokestatic setSelection : (Landroid/text/Spannable;I)V
    //   149: aload_0
    //   150: getfield mTextView : Landroid/widget/TextView;
    //   153: invokevirtual getMovementMethod : ()Landroid/text/method/MovementMethod;
    //   156: astore #5
    //   158: aload #5
    //   160: ifnull -> 187
    //   163: aload_0
    //   164: getfield mTextView : Landroid/widget/TextView;
    //   167: astore #7
    //   169: aload #5
    //   171: aload #7
    //   173: aload #7
    //   175: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   178: checkcast android/text/Spannable
    //   181: iload_2
    //   182: invokeinterface onTakeFocus : (Landroid/widget/TextView;Landroid/text/Spannable;I)V
    //   187: aload_0
    //   188: getfield mTextView : Landroid/widget/TextView;
    //   191: invokevirtual isInExtractedMode : ()Z
    //   194: ifne -> 204
    //   197: aload_0
    //   198: getfield mSelectionMoved : Z
    //   201: ifeq -> 229
    //   204: iload_3
    //   205: iflt -> 229
    //   208: iload #4
    //   210: iflt -> 229
    //   213: aload_0
    //   214: getfield mTextView : Landroid/widget/TextView;
    //   217: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   220: checkcast android/text/Spannable
    //   223: iload_3
    //   224: iload #4
    //   226: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   229: aload_0
    //   230: getfield mSelectAllOnFocus : Z
    //   233: ifeq -> 244
    //   236: aload_0
    //   237: getfield mTextView : Landroid/widget/TextView;
    //   240: invokevirtual selectAllText : ()Z
    //   243: pop
    //   244: aload_0
    //   245: iconst_1
    //   246: putfield mTouchFocusSelected : Z
    //   249: aload_0
    //   250: iconst_0
    //   251: putfield mFrozenWithFocus : Z
    //   254: aload_0
    //   255: iconst_0
    //   256: putfield mSelectionMoved : Z
    //   259: aload_0
    //   260: getfield mError : Ljava/lang/CharSequence;
    //   263: ifnull -> 270
    //   266: aload_0
    //   267: invokespecial showError : ()V
    //   270: aload_0
    //   271: invokevirtual makeBlink : ()V
    //   274: goto -> 365
    //   277: aload_0
    //   278: getfield mError : Ljava/lang/CharSequence;
    //   281: ifnull -> 288
    //   284: aload_0
    //   285: invokespecial hideError : ()V
    //   288: aload_0
    //   289: getfield mTextView : Landroid/widget/TextView;
    //   292: invokevirtual onEndBatchEdit : ()V
    //   295: aload_0
    //   296: getfield mTextView : Landroid/widget/TextView;
    //   299: invokevirtual isInExtractedMode : ()Z
    //   302: ifeq -> 316
    //   305: aload_0
    //   306: invokevirtual hideCursorAndSpanControllers : ()V
    //   309: aload_0
    //   310: invokespecial stopTextActionModeWithPreservingSelection : ()V
    //   313: goto -> 345
    //   316: aload_0
    //   317: invokevirtual hideCursorAndSpanControllers : ()V
    //   320: aload_0
    //   321: getfield mTextView : Landroid/widget/TextView;
    //   324: invokevirtual isTemporarilyDetached : ()Z
    //   327: ifeq -> 337
    //   330: aload_0
    //   331: invokespecial stopTextActionModeWithPreservingSelection : ()V
    //   334: goto -> 341
    //   337: aload_0
    //   338: invokevirtual stopTextActionMode : ()V
    //   341: aload_0
    //   342: invokespecial downgradeEasyCorrectionSpans : ()V
    //   345: aload_0
    //   346: getfield mSelectionModifierCursorController : Landroid/widget/Editor$SelectionModifierCursorController;
    //   349: astore #5
    //   351: aload #5
    //   353: ifnull -> 361
    //   356: aload #5
    //   358: invokevirtual resetTouchOffsets : ()V
    //   361: aload_0
    //   362: invokespecial ensureNoSelectionIfNonSelectable : ()V
    //   365: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1445	-> 0
    //   #1446	-> 7
    //   #1448	-> 11
    //   #1449	-> 15
    //   #1450	-> 23
    //   #1454	-> 32
    //   #1455	-> 49
    //   #1457	-> 73
    //   #1460	-> 107
    //   #1463	-> 123
    //   #1464	-> 129
    //   #1468	-> 134
    //   #1472	-> 149
    //   #1473	-> 158
    //   #1474	-> 163
    //   #1482	-> 187
    //   #1493	-> 213
    //   #1496	-> 229
    //   #1497	-> 236
    //   #1500	-> 244
    //   #1503	-> 249
    //   #1504	-> 254
    //   #1506	-> 259
    //   #1507	-> 266
    //   #1510	-> 270
    //   #1511	-> 274
    //   #1512	-> 277
    //   #1513	-> 284
    //   #1516	-> 288
    //   #1518	-> 295
    //   #1519	-> 305
    //   #1520	-> 309
    //   #1522	-> 316
    //   #1523	-> 320
    //   #1524	-> 330
    //   #1526	-> 337
    //   #1528	-> 341
    //   #1531	-> 345
    //   #1532	-> 356
    //   #1535	-> 361
    //   #1537	-> 365
  }
  
  private void ensureNoSelectionIfNonSelectable() {
    if (!this.mTextView.textCanBeSelected() && this.mTextView.hasSelection()) {
      Spannable spannable = (Spannable)this.mTextView.getText();
      TextView textView = this.mTextView;
      int i = textView.length(), j = this.mTextView.length();
      Selection.setSelection(spannable, i, j);
    } 
  }
  
  private void downgradeEasyCorrectionSpans() {
    CharSequence charSequence = this.mTextView.getText();
    if (charSequence instanceof Spannable) {
      charSequence = charSequence;
      int i = charSequence.length();
      SuggestionSpan[] arrayOfSuggestionSpan = charSequence.<SuggestionSpan>getSpans(0, i, SuggestionSpan.class);
      for (i = 0; i < arrayOfSuggestionSpan.length; i++) {
        int j = arrayOfSuggestionSpan[i].getFlags();
        if ((j & 0x1) != 0 && (j & 0x2) == 0)
          arrayOfSuggestionSpan[i].setFlags(j & 0xFFFFFFFE); 
      } 
    } 
  }
  
  void sendOnTextChanged(int paramInt1, int paramInt2, int paramInt3) {
    getSelectionActionModeHelper().onTextChanged(paramInt1, paramInt1 + paramInt2);
    updateSpellCheckSpans(paramInt1, paramInt1 + paramInt3, false);
    this.mUpdateWordIteratorText = true;
    hideCursorControllers();
    SelectionModifierCursorController selectionModifierCursorController = this.mSelectionModifierCursorController;
    if (selectionModifierCursorController != null)
      selectionModifierCursorController.resetTouchOffsets(); 
    stopTextActionMode();
  }
  
  private int getLastTapPosition() {
    SelectionModifierCursorController selectionModifierCursorController = this.mSelectionModifierCursorController;
    if (selectionModifierCursorController != null) {
      int i = selectionModifierCursorController.getMinTouchOffset();
      if (i >= 0) {
        int j = i;
        if (i > this.mTextView.getText().length())
          j = this.mTextView.getText().length(); 
        return j;
      } 
    } 
    return -1;
  }
  
  void onWindowFocusChanged(boolean paramBoolean) {
    if (paramBoolean) {
      Blink blink = this.mBlink;
      if (blink != null) {
        blink.uncancel();
        makeBlink();
      } 
      if (this.mTextView.hasSelection() && !extractedTextModeWillBeStarted())
        refreshTextActionMode(); 
    } else {
      Blink blink = this.mBlink;
      if (blink != null)
        blink.cancel(); 
      InputContentType inputContentType = this.mInputContentType;
      if (inputContentType != null)
        inputContentType.enterDown = false; 
      hideCursorAndSpanControllers();
      stopTextActionModeWithPreservingSelection();
      SuggestionsPopupWindow suggestionsPopupWindow = this.mSuggestionsPopupWindow;
      if (suggestionsPopupWindow != null)
        suggestionsPopupWindow.onParentLostFocus(); 
      ensureEndedBatchEdit();
      ensureNoSelectionIfNonSelectable();
    } 
  }
  
  private boolean shouldFilterOutTouchEvent(MotionEvent paramMotionEvent) {
    if (!paramMotionEvent.isFromSource(8194))
      return false; 
    int i = this.mLastButtonState;
    if (((i ^ paramMotionEvent.getButtonState()) & 0x1) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = paramMotionEvent.getActionMasked();
    if ((j == 0 || j == 1) && i == 0)
      return true; 
    if (j == 2 && !paramMotionEvent.isButtonPressed(1))
      return true; 
    return false;
  }
  
  public void onTouchEvent(MotionEvent paramMotionEvent) {
    boolean bool = shouldFilterOutTouchEvent(paramMotionEvent);
    this.mLastButtonState = paramMotionEvent.getButtonState();
    if (bool) {
      if (paramMotionEvent.getActionMasked() == 1)
        this.mDiscardNextActionUp = true; 
      return;
    } 
    ViewConfiguration viewConfiguration = ViewConfiguration.get(this.mTextView.getContext());
    this.mTouchState.update(paramMotionEvent, viewConfiguration);
    updateFloatingToolbarVisibility(paramMotionEvent);
    if (hasInsertionController())
      getInsertionController().onTouchEvent(paramMotionEvent); 
    if (hasSelectionController())
      getSelectionController().onTouchEvent(paramMotionEvent); 
    Runnable runnable = this.mShowSuggestionRunnable;
    if (runnable != null) {
      this.mTextView.removeCallbacks(runnable);
      this.mShowSuggestionRunnable = null;
    } 
    if (paramMotionEvent.getActionMasked() == 0) {
      this.mTouchFocusSelected = false;
      this.mIgnoreActionUpEvent = false;
      this.mIsFousedBeforeTouch = this.mTextView.isFocused();
    } 
  }
  
  private void updateFloatingToolbarVisibility(MotionEvent paramMotionEvent) {
    if (this.mTextActionMode != null) {
      int i = paramMotionEvent.getActionMasked();
      if (i != 1)
        if (i != 2) {
          if (i != 3)
            return; 
        } else {
          hideFloatingToolbar(-1);
          return;
        }  
      showFloatingToolbar();
    } 
  }
  
  void hideFloatingToolbar(int paramInt) {
    if (this.mTextActionMode != null) {
      this.mTextView.removeCallbacks(this.mShowFloatingToolbar);
      this.mTextActionMode.hide(paramInt);
    } 
  }
  
  private void showFloatingToolbar() {
    if (this.mTextActionMode != null) {
      int i = ViewConfiguration.getDoubleTapTimeout();
      this.mTextView.postDelayed(this.mShowFloatingToolbar, i);
      invalidateActionModeAsync();
    } 
  }
  
  private InputMethodManager getInputMethodManager() {
    return (InputMethodManager)this.mTextView.getContext().getSystemService(InputMethodManager.class);
  }
  
  public void beginBatchEdit() {
    this.mInBatchEditControllers = true;
    InputMethodState inputMethodState = this.mInputMethodState;
    if (inputMethodState != null) {
      int i = inputMethodState.mBatchEditNesting + 1;
      if (i == 1) {
        inputMethodState.mCursorChanged = false;
        inputMethodState.mChangedDelta = 0;
        if (inputMethodState.mContentChanged) {
          inputMethodState.mChangedStart = 0;
          inputMethodState.mChangedEnd = this.mTextView.getText().length();
        } else {
          inputMethodState.mChangedStart = -1;
          inputMethodState.mChangedEnd = -1;
          inputMethodState.mContentChanged = false;
        } 
        this.mUndoInputFilter.beginBatchEdit();
        this.mTextView.onBeginBatchEdit();
      } 
    } 
  }
  
  public void endBatchEdit() {
    this.mInBatchEditControllers = false;
    InputMethodState inputMethodState = this.mInputMethodState;
    if (inputMethodState != null) {
      int i = inputMethodState.mBatchEditNesting - 1;
      if (i == 0)
        finishBatchEdit(inputMethodState); 
    } 
  }
  
  void ensureEndedBatchEdit() {
    InputMethodState inputMethodState = this.mInputMethodState;
    if (inputMethodState != null && inputMethodState.mBatchEditNesting != 0) {
      inputMethodState.mBatchEditNesting = 0;
      finishBatchEdit(inputMethodState);
    } 
  }
  
  void finishBatchEdit(InputMethodState paramInputMethodState) {
    this.mTextView.onEndBatchEdit();
    this.mUndoInputFilter.endBatchEdit();
    if (paramInputMethodState.mContentChanged || paramInputMethodState.mSelectionModeChanged) {
      this.mTextView.updateAfterEdit();
      reportExtractedText();
    } else if (paramInputMethodState.mCursorChanged) {
      this.mTextView.invalidateCursor();
    } 
    sendUpdateSelection();
    if (this.mTextActionMode != null) {
      InsertionPointCursorController insertionPointCursorController;
      if (this.mTextView.hasSelection()) {
        SelectionModifierCursorController selectionModifierCursorController = getSelectionController();
      } else {
        insertionPointCursorController = getInsertionController();
      } 
      if (insertionPointCursorController != null && !insertionPointCursorController.isActive() && !insertionPointCursorController.isCursorBeingModified())
        insertionPointCursorController.show(); 
    } 
  }
  
  boolean extractText(ExtractedTextRequest paramExtractedTextRequest, ExtractedText paramExtractedText) {
    return extractTextInternal(paramExtractedTextRequest, -1, -1, -1, paramExtractedText);
  }
  
  private boolean extractTextInternal(ExtractedTextRequest paramExtractedTextRequest, int paramInt1, int paramInt2, int paramInt3, ExtractedText paramExtractedText) {
    if (paramExtractedTextRequest == null || paramExtractedText == null)
      return false; 
    CharSequence charSequence = this.mTextView.getText();
    if (charSequence == null)
      return false; 
    if (paramInt1 != -2) {
      int i = charSequence.length();
      if (paramInt1 < 0) {
        paramExtractedText.partialEndOffset = -1;
        paramExtractedText.partialStartOffset = -1;
        paramInt3 = 0;
        paramInt2 = i;
      } else {
        paramInt2 += paramInt3;
        int j = paramInt1, k = paramInt2;
        if (charSequence instanceof Spanned) {
          Spanned spanned = (Spanned)charSequence;
          Object[] arrayOfObject = spanned.getSpans(paramInt1, paramInt2, (Class)ParcelableSpan.class);
          int m = arrayOfObject.length;
          while (true) {
            j = paramInt1;
            k = paramInt2;
            if (m > 0) {
              m--;
              j = spanned.getSpanStart(arrayOfObject[m]);
              k = paramInt1;
              if (j < paramInt1)
                k = j; 
              paramInt1 = spanned.getSpanEnd(arrayOfObject[m]);
              j = paramInt2;
              if (paramInt1 > paramInt2)
                j = paramInt1; 
              paramInt1 = k;
              paramInt2 = j;
              continue;
            } 
            break;
          } 
        } 
        paramExtractedText.partialStartOffset = j;
        paramExtractedText.partialEndOffset = k - paramInt3;
        if (j > i) {
          paramInt1 = i;
        } else {
          paramInt1 = j;
          if (j < 0)
            paramInt1 = 0; 
        } 
        if (k > i) {
          paramInt2 = i;
          paramInt3 = paramInt1;
        } else {
          paramInt3 = paramInt1;
          paramInt2 = k;
          if (k < 0) {
            paramInt2 = 0;
            paramInt3 = paramInt1;
          } 
        } 
      } 
      if ((paramExtractedTextRequest.flags & 0x1) != 0) {
        paramExtractedText.text = charSequence.subSequence(paramInt3, paramInt2);
      } else {
        paramExtractedText.text = TextUtils.substring(charSequence, paramInt3, paramInt2);
      } 
    } else {
      paramExtractedText.partialStartOffset = 0;
      paramExtractedText.partialEndOffset = 0;
      paramExtractedText.text = "";
    } 
    paramExtractedText.flags = 0;
    if (MetaKeyKeyListener.getMetaState(charSequence, 2048) != 0)
      paramExtractedText.flags |= 0x2; 
    if (this.mTextView.isSingleLine())
      paramExtractedText.flags |= 0x1; 
    paramExtractedText.startOffset = 0;
    paramExtractedText.selectionStart = this.mTextView.getSelectionStart();
    paramExtractedText.selectionEnd = this.mTextView.getSelectionEnd();
    paramExtractedText.hint = this.mTextView.getHint();
    return true;
  }
  
  boolean reportExtractedText() {
    InputMethodState inputMethodState = this.mInputMethodState;
    if (inputMethodState == null)
      return false; 
    boolean bool = inputMethodState.mContentChanged;
    if (!bool && !inputMethodState.mSelectionModeChanged)
      return false; 
    inputMethodState.mContentChanged = false;
    inputMethodState.mSelectionModeChanged = false;
    ExtractedTextRequest extractedTextRequest = inputMethodState.mExtractedTextRequest;
    if (extractedTextRequest == null)
      return false; 
    InputMethodManager inputMethodManager = getInputMethodManager();
    if (inputMethodManager == null)
      return false; 
    if (inputMethodState.mChangedStart < 0 && !bool)
      inputMethodState.mChangedStart = -2; 
    if (extractTextInternal(extractedTextRequest, inputMethodState.mChangedStart, inputMethodState.mChangedEnd, inputMethodState.mChangedDelta, inputMethodState.mExtractedText)) {
      inputMethodManager.updateExtractedText(this.mTextView, extractedTextRequest.token, inputMethodState.mExtractedText);
      inputMethodState.mChangedStart = -1;
      inputMethodState.mChangedEnd = -1;
      inputMethodState.mChangedDelta = 0;
      inputMethodState.mContentChanged = false;
      return true;
    } 
    return false;
  }
  
  private void sendUpdateSelection() {
    InputMethodState inputMethodState = this.mInputMethodState;
    if (inputMethodState != null && inputMethodState.mBatchEditNesting <= 0) {
      InputMethodManager inputMethodManager = getInputMethodManager();
      if (inputMethodManager != null) {
        byte b1, b2;
        int i = this.mTextView.getSelectionStart();
        int j = this.mTextView.getSelectionEnd();
        if (this.mTextView.getText() instanceof Spannable) {
          Spannable spannable = (Spannable)this.mTextView.getText();
          b1 = EditableInputConnection.getComposingSpanStart(spannable);
          int k = EditableInputConnection.getComposingSpanEnd(spannable);
          b2 = b1;
          b1 = k;
        } else {
          b2 = -1;
          b1 = -1;
        } 
        inputMethodManager.updateSelection(this.mTextView, i, j, b2, b1);
      } 
    } 
  }
  
  void onDraw(Canvas paramCanvas, Layout paramLayout, Path paramPath, Paint paramPaint, int paramInt) {
    int i = this.mTextView.getSelectionStart();
    int j = this.mTextView.getSelectionEnd();
    InputMethodState inputMethodState = this.mInputMethodState;
    if (inputMethodState != null && inputMethodState.mBatchEditNesting == 0) {
      InputMethodManager inputMethodManager = getInputMethodManager();
      if (inputMethodManager != null && inputMethodManager.isActive(this.mTextView) && (inputMethodState.mContentChanged || inputMethodState.mSelectionModeChanged))
        reportExtractedText(); 
    } 
    CorrectionHighlighter correctionHighlighter = this.mCorrectionHighlighter;
    if (correctionHighlighter != null)
      correctionHighlighter.draw(paramCanvas, paramInt); 
    Path path = paramPath;
    if (paramPath != null) {
      path = paramPath;
      if (i == j) {
        path = paramPath;
        if (this.mDrawableForCursor != null) {
          drawCursor(paramCanvas, paramInt);
          path = null;
        } 
      } 
    } 
    SelectionActionModeHelper selectionActionModeHelper = this.mSelectionActionModeHelper;
    paramPath = path;
    if (selectionActionModeHelper != null) {
      selectionActionModeHelper.onDraw(paramCanvas);
      paramPath = path;
      if (this.mSelectionActionModeHelper.isDrawingHighlight())
        paramPath = null; 
    } 
    if (this.mTextView.canHaveDisplayList() && paramCanvas.isHardwareAccelerated()) {
      drawHardwareAccelerated(paramCanvas, paramLayout, paramPath, paramPaint, paramInt);
    } else {
      paramLayout.draw(paramCanvas, paramPath, paramPaint, paramInt);
    } 
  }
  
  private void drawHardwareAccelerated(Canvas paramCanvas, Layout paramLayout, Path paramPath, Paint paramPaint, int paramInt) {
    // Byte code:
    //   0: aload_2
    //   1: aload_1
    //   2: invokevirtual getLineRangeForDraw : (Landroid/graphics/Canvas;)J
    //   5: lstore #6
    //   7: lload #6
    //   9: invokestatic unpackRangeStartFromLong : (J)I
    //   12: istore #8
    //   14: lload #6
    //   16: invokestatic unpackRangeEndFromLong : (J)I
    //   19: istore #9
    //   21: iload #9
    //   23: ifge -> 27
    //   26: return
    //   27: aload_2
    //   28: aload_1
    //   29: aload_3
    //   30: aload #4
    //   32: iload #5
    //   34: iload #8
    //   36: iload #9
    //   38: invokevirtual drawBackground : (Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;III)V
    //   41: aload_2
    //   42: instanceof android/text/DynamicLayout
    //   45: ifeq -> 494
    //   48: aload_0
    //   49: getfield mTextRenderNodes : [Landroid/widget/Editor$TextRenderNode;
    //   52: ifnonnull -> 67
    //   55: aload_0
    //   56: ldc android/widget/Editor$TextRenderNode
    //   58: invokestatic emptyArray : (Ljava/lang/Class;)[Ljava/lang/Object;
    //   61: checkcast [Landroid/widget/Editor$TextRenderNode;
    //   64: putfield mTextRenderNodes : [Landroid/widget/Editor$TextRenderNode;
    //   67: aload_2
    //   68: checkcast android/text/DynamicLayout
    //   71: astore #10
    //   73: aload #10
    //   75: invokevirtual getBlockEndLines : ()[I
    //   78: astore #11
    //   80: aload #10
    //   82: invokevirtual getBlockIndices : ()[I
    //   85: astore #12
    //   87: aload #10
    //   89: invokevirtual getNumberOfBlocks : ()I
    //   92: istore #13
    //   94: aload #10
    //   96: invokevirtual getIndexFirstChangedBlock : ()I
    //   99: istore #14
    //   101: aload #10
    //   103: invokevirtual getBlocksAlwaysNeedToBeRedrawn : ()Landroid/util/ArraySet;
    //   106: astore #15
    //   108: iconst_1
    //   109: istore #16
    //   111: aload #15
    //   113: ifnull -> 184
    //   116: iconst_0
    //   117: istore #17
    //   119: iload #17
    //   121: aload #15
    //   123: invokevirtual size : ()I
    //   126: if_icmpge -> 184
    //   129: aload #10
    //   131: aload #15
    //   133: iload #17
    //   135: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   138: checkcast java/lang/Integer
    //   141: invokevirtual intValue : ()I
    //   144: invokevirtual getBlockIndex : (I)I
    //   147: istore #18
    //   149: iload #18
    //   151: iconst_m1
    //   152: if_icmpeq -> 178
    //   155: aload_0
    //   156: getfield mTextRenderNodes : [Landroid/widget/Editor$TextRenderNode;
    //   159: astore #19
    //   161: aload #19
    //   163: iload #18
    //   165: aaload
    //   166: ifnull -> 178
    //   169: aload #19
    //   171: iload #18
    //   173: aaload
    //   174: iconst_1
    //   175: putfield needsToBeShifted : Z
    //   178: iinc #17, 1
    //   181: goto -> 119
    //   184: aload #11
    //   186: iconst_0
    //   187: iload #13
    //   189: iload #8
    //   191: invokestatic binarySearch : ([IIII)I
    //   194: istore #18
    //   196: iload #18
    //   198: istore #17
    //   200: iload #18
    //   202: ifge -> 212
    //   205: iload #18
    //   207: iconst_1
    //   208: iadd
    //   209: ineg
    //   210: istore #17
    //   212: iload #14
    //   214: iload #17
    //   216: invokestatic min : (II)I
    //   219: istore #20
    //   221: iconst_0
    //   222: istore #17
    //   224: iload #13
    //   226: istore #18
    //   228: iload #18
    //   230: istore #21
    //   232: iload #20
    //   234: iload #21
    //   236: if_icmpge -> 357
    //   239: aload #12
    //   241: iload #20
    //   243: iaload
    //   244: istore #18
    //   246: iload #20
    //   248: iload #14
    //   250: if_icmplt -> 283
    //   253: iload #18
    //   255: iconst_m1
    //   256: if_icmpeq -> 283
    //   259: aload_0
    //   260: getfield mTextRenderNodes : [Landroid/widget/Editor$TextRenderNode;
    //   263: astore #19
    //   265: aload #19
    //   267: iload #18
    //   269: aaload
    //   270: ifnull -> 283
    //   273: aload #19
    //   275: iload #18
    //   277: aaload
    //   278: iload #16
    //   280: putfield needsToBeShifted : Z
    //   283: aload #11
    //   285: iload #20
    //   287: iaload
    //   288: iload #8
    //   290: if_icmpge -> 296
    //   293: goto -> 347
    //   296: aload_0
    //   297: aload_1
    //   298: aload_2
    //   299: aload_3
    //   300: aload #4
    //   302: iload #5
    //   304: aload #11
    //   306: aload #12
    //   308: iload #20
    //   310: iload #21
    //   312: iload #17
    //   314: invokespecial drawHardwareAcceleratedInner : (Landroid/graphics/Canvas;Landroid/text/Layout;Landroid/graphics/Path;Landroid/graphics/Paint;I[I[IIII)I
    //   317: istore #18
    //   319: iload #18
    //   321: istore #17
    //   323: aload #11
    //   325: iload #20
    //   327: iaload
    //   328: iload #9
    //   330: if_icmplt -> 347
    //   333: iload #14
    //   335: iload #20
    //   337: iconst_1
    //   338: iadd
    //   339: invokestatic max : (II)I
    //   342: istore #17
    //   344: goto -> 365
    //   347: iinc #20, 1
    //   350: iload #21
    //   352: istore #18
    //   354: goto -> 228
    //   357: iload #17
    //   359: istore #18
    //   361: iload #13
    //   363: istore #17
    //   365: aload #15
    //   367: ifnull -> 484
    //   370: iconst_0
    //   371: istore #8
    //   373: iload #18
    //   375: istore #13
    //   377: iload #8
    //   379: istore #18
    //   381: iload #18
    //   383: aload #15
    //   385: invokevirtual size : ()I
    //   388: if_icmpge -> 481
    //   391: aload #15
    //   393: iload #18
    //   395: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   398: checkcast java/lang/Integer
    //   401: invokevirtual intValue : ()I
    //   404: istore #8
    //   406: aload #10
    //   408: iload #8
    //   410: invokevirtual getBlockIndex : (I)I
    //   413: istore #14
    //   415: iload #14
    //   417: iconst_m1
    //   418: if_icmpeq -> 452
    //   421: aload_0
    //   422: getfield mTextRenderNodes : [Landroid/widget/Editor$TextRenderNode;
    //   425: astore #19
    //   427: aload #19
    //   429: iload #14
    //   431: aaload
    //   432: ifnull -> 452
    //   435: aload #19
    //   437: iload #14
    //   439: aaload
    //   440: getfield needsToBeShifted : Z
    //   443: ifeq -> 449
    //   446: goto -> 452
    //   449: goto -> 475
    //   452: aload_0
    //   453: aload_1
    //   454: aload_2
    //   455: aload_3
    //   456: aload #4
    //   458: iload #5
    //   460: aload #11
    //   462: aload #12
    //   464: iload #8
    //   466: iload #21
    //   468: iload #13
    //   470: invokespecial drawHardwareAcceleratedInner : (Landroid/graphics/Canvas;Landroid/text/Layout;Landroid/graphics/Path;Landroid/graphics/Paint;I[I[IIII)I
    //   473: istore #13
    //   475: iinc #18, 1
    //   478: goto -> 381
    //   481: goto -> 484
    //   484: aload #10
    //   486: iload #17
    //   488: invokevirtual setIndexFirstChangedBlock : (I)V
    //   491: goto -> 503
    //   494: aload_2
    //   495: aload_1
    //   496: iload #8
    //   498: iload #9
    //   500: invokevirtual drawText : (Landroid/graphics/Canvas;II)V
    //   503: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1999	-> 0
    //   #2000	-> 7
    //   #2001	-> 14
    //   #2002	-> 21
    //   #2004	-> 27
    //   #2007	-> 41
    //   #2008	-> 48
    //   #2009	-> 55
    //   #2012	-> 67
    //   #2013	-> 73
    //   #2014	-> 80
    //   #2015	-> 87
    //   #2016	-> 94
    //   #2018	-> 101
    //   #2019	-> 108
    //   #2020	-> 116
    //   #2021	-> 129
    //   #2022	-> 149
    //   #2024	-> 169
    //   #2020	-> 178
    //   #2029	-> 184
    //   #2030	-> 196
    //   #2031	-> 205
    //   #2033	-> 212
    //   #2035	-> 221
    //   #2036	-> 221
    //   #2038	-> 221
    //   #2039	-> 239
    //   #2040	-> 246
    //   #2043	-> 273
    //   #2045	-> 283
    //   #2048	-> 293
    //   #2050	-> 296
    //   #2053	-> 319
    //   #2054	-> 333
    //   #2055	-> 344
    //   #2038	-> 347
    //   #2058	-> 365
    //   #2059	-> 370
    //   #2060	-> 391
    //   #2061	-> 406
    //   #2062	-> 415
    //   #2065	-> 452
    //   #2059	-> 475
    //   #2058	-> 484
    //   #2073	-> 484
    //   #2074	-> 491
    //   #2076	-> 494
    //   #2078	-> 503
  }
  
  private int drawHardwareAcceleratedInner(Canvas paramCanvas, Layout paramLayout, Path paramPath, Paint paramPaint, int paramInt1, int[] paramArrayOfint1, int[] paramArrayOfint2, int paramInt2, int paramInt3, int paramInt4) {
    int i = paramArrayOfint1[paramInt2];
    int j = paramArrayOfint2[paramInt2];
    if (j == -1) {
      paramInt1 = 1;
    } else {
      paramInt1 = 0;
    } 
    if (paramInt1 != 0) {
      paramInt4 = getAvailableDisplayListIndex(paramArrayOfint2, paramInt3, paramInt4);
      paramArrayOfint2[paramInt2] = paramInt4;
      TextRenderNode[] arrayOfTextRenderNode1 = this.mTextRenderNodes;
      if (arrayOfTextRenderNode1[paramInt4] != null)
        (arrayOfTextRenderNode1[paramInt4]).isDirty = true; 
      paramInt3 = paramInt4 + 1;
    } else {
      paramInt3 = paramInt4;
      paramInt4 = j;
    } 
    TextRenderNode[] arrayOfTextRenderNode = this.mTextRenderNodes;
    if (arrayOfTextRenderNode[paramInt4] == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Text ");
      stringBuilder.append(paramInt4);
      arrayOfTextRenderNode[paramInt4] = new TextRenderNode(stringBuilder.toString());
    } 
    boolean bool = this.mTextRenderNodes[paramInt4].needsRecord();
    RenderNode renderNode = (this.mTextRenderNodes[paramInt4]).renderNode;
    if ((this.mTextRenderNodes[paramInt4]).needsToBeShifted || bool) {
      if (paramInt2 == 0) {
        paramInt2 = 0;
      } else {
        paramInt2 = paramArrayOfint1[paramInt2 - 1] + 1;
      } 
      int k = paramLayout.getLineTop(paramInt2);
      int m = paramLayout.getLineBottom(i);
      j = this.mTextView.getWidth();
      if (this.mTextView.getHorizontallyScrolling()) {
        float f1 = Float.MAX_VALUE;
        float f2;
        for (f2 = Float.MIN_VALUE, j = paramInt2; j <= i; j++) {
          f1 = Math.min(f1, paramLayout.getLineLeft(j));
          f2 = Math.max(f2, paramLayout.getLineRight(j));
        } 
        paramInt1 = (int)f1;
        j = (int)(0.5F + f2);
      } else {
        paramInt1 = 0;
      } 
      if (bool) {
        RecordingCanvas recordingCanvas = renderNode.beginRecording(j - paramInt1, m - k);
        float f2 = -paramInt1, f1 = -k;
        try {
          recordingCanvas.translate(f2, f1);
          paramLayout.drawText((Canvas)recordingCanvas, paramInt2, i);
          (this.mTextRenderNodes[paramInt4]).isDirty = false;
        } finally {
          renderNode.endRecording();
          renderNode.setClipToBounds(false);
        } 
      } 
      renderNode.setLeftTopRightBottom(paramInt1, k, j, m);
      (this.mTextRenderNodes[paramInt4]).needsToBeShifted = false;
    } 
    ((RecordingCanvas)paramCanvas).drawRenderNode(renderNode);
    return paramInt3;
  }
  
  private int getAvailableDisplayListIndex(int[] paramArrayOfint, int paramInt1, int paramInt2) {
    int i = this.mTextRenderNodes.length;
    while (paramInt2 < i) {
      boolean bool2, bool1 = false;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < paramInt1) {
          if (paramArrayOfint[b] == paramInt2) {
            bool2 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (bool2) {
        paramInt2++;
        continue;
      } 
      return paramInt2;
    } 
    this.mTextRenderNodes = (TextRenderNode[])GrowingArrayUtils.append((Object[])this.mTextRenderNodes, i, null);
    return i;
  }
  
  private void drawCursor(Canvas paramCanvas, int paramInt) {
    boolean bool;
    if (paramInt != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool)
      paramCanvas.translate(0.0F, paramInt); 
    Drawable drawable = this.mDrawableForCursor;
    if (drawable != null)
      drawable.draw(paramCanvas); 
    if (bool)
      paramCanvas.translate(0.0F, -paramInt); 
  }
  
  void invalidateHandlesAndActionMode() {
    SelectionModifierCursorController selectionModifierCursorController = this.mSelectionModifierCursorController;
    if (selectionModifierCursorController != null)
      selectionModifierCursorController.invalidateHandles(); 
    InsertionPointCursorController insertionPointCursorController = this.mInsertionPointCursorController;
    if (insertionPointCursorController != null)
      insertionPointCursorController.invalidateHandle(); 
    if (this.mTextActionMode != null)
      invalidateActionMode(); 
  }
  
  void invalidateTextDisplayList(Layout paramLayout, int paramInt1, int paramInt2) {
    if (this.mTextRenderNodes != null && paramLayout instanceof DynamicLayout) {
      int i = paramLayout.getLineForOffset(paramInt1);
      int j = paramLayout.getLineForOffset(paramInt2);
      DynamicLayout dynamicLayout = (DynamicLayout)paramLayout;
      int[] arrayOfInt2 = dynamicLayout.getBlockEndLines();
      int[] arrayOfInt1 = dynamicLayout.getBlockIndices();
      int k = dynamicLayout.getNumberOfBlocks();
      paramInt1 = 0;
      while (true) {
        paramInt2 = paramInt1;
        if (paramInt1 < k) {
          if (arrayOfInt2[paramInt1] >= i) {
            paramInt2 = paramInt1;
            break;
          } 
          paramInt1++;
          continue;
        } 
        break;
      } 
      while (paramInt2 < k) {
        paramInt1 = arrayOfInt1[paramInt2];
        if (paramInt1 != -1)
          (this.mTextRenderNodes[paramInt1]).isDirty = true; 
        if (arrayOfInt2[paramInt2] >= j)
          break; 
        paramInt2++;
      } 
    } 
  }
  
  void invalidateTextDisplayList() {
    if (this.mTextRenderNodes != null) {
      byte b = 0;
      while (true) {
        TextRenderNode[] arrayOfTextRenderNode = this.mTextRenderNodes;
        if (b < arrayOfTextRenderNode.length) {
          if (arrayOfTextRenderNode[b] != null)
            (arrayOfTextRenderNode[b]).isDirty = true; 
          b++;
          continue;
        } 
        break;
      } 
    } 
  }
  
  void updateCursorPosition() {
    loadCursorDrawable();
    if (this.mDrawableForCursor == null)
      return; 
    Layout layout = this.mTextView.getLayout();
    int i = this.mTextView.getSelectionStart();
    int j = layout.getLineForOffset(i);
    int k = layout.getLineTop(j);
    int m = layout.getLineBottomWithoutSpacing(j);
    boolean bool = layout.shouldClampCursor(j);
    updateCursorPosition(k, m, layout.getPrimaryHorizontal(i, bool));
  }
  
  void refreshTextActionMode() {
    if (extractedTextModeWillBeStarted()) {
      this.mRestartActionModeOnNextRefresh = false;
      return;
    } 
    boolean bool = this.mTextView.hasSelection();
    SelectionModifierCursorController selectionModifierCursorController = getSelectionController();
    InsertionPointCursorController insertionPointCursorController = getInsertionController();
    if ((selectionModifierCursorController != null && selectionModifierCursorController.isCursorBeingModified()) || (insertionPointCursorController != null && insertionPointCursorController.isCursorBeingModified())) {
      this.mRestartActionModeOnNextRefresh = false;
      return;
    } 
    if (bool) {
      hideInsertionPointCursorController();
      if (this.mTextActionMode == null) {
        if (this.mRestartActionModeOnNextRefresh)
          startSelectionActionModeAsync(false); 
      } else if (selectionModifierCursorController == null || !selectionModifierCursorController.isActive()) {
        stopTextActionModeWithPreservingSelection();
        startSelectionActionModeAsync(false);
      } else {
        this.mTextActionMode.invalidateContentRect();
      } 
    } else if (insertionPointCursorController == null || !insertionPointCursorController.isActive()) {
      stopTextActionMode();
    } else {
      ActionMode actionMode = this.mTextActionMode;
      if (actionMode != null)
        actionMode.invalidateContentRect(); 
    } 
    this.mRestartActionModeOnNextRefresh = false;
  }
  
  void startInsertionActionMode() {
    Runnable runnable = this.mInsertionActionModeRunnable;
    if (runnable != null)
      this.mTextView.removeCallbacks(runnable); 
    if (extractedTextModeWillBeStarted())
      return; 
    stopTextActionMode();
    TextActionModeCallback textActionModeCallback = new TextActionModeCallback(1);
    ActionMode actionMode = this.mTextView.startActionMode(textActionModeCallback, 1);
    if (actionMode != null && getInsertionController() != null)
      getInsertionController().show(); 
  }
  
  TextView getTextView() {
    return this.mTextView;
  }
  
  ActionMode getTextActionMode() {
    return this.mTextActionMode;
  }
  
  void setRestartActionModeOnNextRefresh(boolean paramBoolean) {
    this.mRestartActionModeOnNextRefresh = paramBoolean;
  }
  
  void startSelectionActionModeAsync(boolean paramBoolean) {
    getSelectionActionModeHelper().startSelectionActionModeAsync(paramBoolean);
  }
  
  void startLinkActionModeAsync(int paramInt1, int paramInt2) {
    if (!(this.mTextView.getText() instanceof Spannable))
      return; 
    stopTextActionMode();
    this.mRequestingLinkActionMode = true;
    getSelectionActionModeHelper().startLinkActionModeAsync(paramInt1, paramInt2);
  }
  
  void invalidateActionModeAsync() {
    getSelectionActionModeHelper().invalidateActionModeAsync();
  }
  
  private void invalidateActionMode() {
    ActionMode actionMode = this.mTextActionMode;
    if (actionMode != null)
      actionMode.invalidate(); 
  }
  
  private SelectionActionModeHelper getSelectionActionModeHelper() {
    if (this.mSelectionActionModeHelper == null)
      this.mSelectionActionModeHelper = new SelectionActionModeHelper(this); 
    return this.mSelectionActionModeHelper;
  }
  
  private boolean selectCurrentWordAndStartDrag() {
    Runnable runnable = this.mInsertionActionModeRunnable;
    if (runnable != null)
      this.mTextView.removeCallbacks(runnable); 
    if (extractedTextModeWillBeStarted())
      return false; 
    if (!checkField())
      return false; 
    if (!this.mTextView.hasSelection() && !selectCurrentWord())
      return false; 
    stopTextActionModeWithPreservingSelection();
    getSelectionController().enterDrag(2);
    return true;
  }
  
  boolean checkField() {
    if (!this.mTextView.canSelectText() || !this.mTextView.requestFocus()) {
      Log.w("TextView", "TextView does not support text selection. Selection cancelled.");
      return false;
    } 
    return true;
  }
  
  boolean startActionModeInternal(int paramInt) {
    boolean bool2;
    if (extractedTextModeWillBeStarted())
      return false; 
    if (this.mTextActionMode != null) {
      invalidateActionMode();
      return false;
    } 
    if (paramInt != 2 && (!checkField() || !this.mTextView.hasSelection()))
      return false; 
    TextActionModeCallback textActionModeCallback = new TextActionModeCallback(paramInt);
    TextView textView = this.mTextView;
    boolean bool1 = true;
    this.mTextActionMode = textView.startActionMode(textActionModeCallback, 1);
    if (this.mTextView.isTextEditable() || this.mTextView.isTextSelectable()) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (paramInt == 2 && !bool2) {
      ActionMode actionMode = this.mTextActionMode;
      if (actionMode instanceof FloatingActionMode)
        ((FloatingActionMode)actionMode).setOutsideTouchable(true, new _$$Lambda$Editor$TdqUlJ6RRep0wXYHaRH51nTa08I(this)); 
    } 
    if (this.mTextActionMode == null)
      bool1 = false; 
    if (bool1) {
      textView = this.mTextView;
      if (textView.isTextEditable() && !this.mTextView.isTextSelectable() && this.mShowSoftInputOnFocus) {
        InputMethodManager inputMethodManager = getInputMethodManager();
        if (inputMethodManager != null)
          inputMethodManager.showSoftInput(this.mTextView, 0, null); 
      } 
    } 
    return bool1;
  }
  
  private boolean extractedTextModeWillBeStarted() {
    boolean bool = this.mTextView.isInExtractedMode();
    boolean bool1 = false;
    if (!bool) {
      InputMethodManager inputMethodManager = getInputMethodManager();
      bool = bool1;
      if (inputMethodManager != null) {
        bool = bool1;
        if (inputMethodManager.isFullscreenMode())
          bool = true; 
      } 
      return bool;
    } 
    return false;
  }
  
  private boolean shouldOfferToShowSuggestions() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTextView : Landroid/widget/TextView;
    //   4: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   7: astore_1
    //   8: aload_1
    //   9: instanceof android/text/Spannable
    //   12: ifne -> 17
    //   15: iconst_0
    //   16: ireturn
    //   17: aload_1
    //   18: checkcast android/text/Spannable
    //   21: astore_2
    //   22: aload_0
    //   23: getfield mTextView : Landroid/widget/TextView;
    //   26: invokevirtual getSelectionStart : ()I
    //   29: istore_3
    //   30: aload_0
    //   31: getfield mTextView : Landroid/widget/TextView;
    //   34: invokevirtual getSelectionEnd : ()I
    //   37: istore #4
    //   39: aload_2
    //   40: iload_3
    //   41: iload #4
    //   43: ldc_w android/text/style/SuggestionSpan
    //   46: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   51: checkcast [Landroid/text/style/SuggestionSpan;
    //   54: astore_1
    //   55: aload_1
    //   56: arraylength
    //   57: ifne -> 62
    //   60: iconst_0
    //   61: ireturn
    //   62: iload_3
    //   63: iload #4
    //   65: if_icmpne -> 99
    //   68: iconst_0
    //   69: istore #4
    //   71: iload #4
    //   73: aload_1
    //   74: arraylength
    //   75: if_icmpge -> 97
    //   78: aload_1
    //   79: iload #4
    //   81: aaload
    //   82: invokevirtual getSuggestions : ()[Ljava/lang/String;
    //   85: arraylength
    //   86: ifle -> 91
    //   89: iconst_1
    //   90: ireturn
    //   91: iinc #4, 1
    //   94: goto -> 71
    //   97: iconst_0
    //   98: ireturn
    //   99: aload_0
    //   100: getfield mTextView : Landroid/widget/TextView;
    //   103: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   106: invokeinterface length : ()I
    //   111: istore #5
    //   113: iconst_0
    //   114: istore #6
    //   116: aload_0
    //   117: getfield mTextView : Landroid/widget/TextView;
    //   120: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   123: invokeinterface length : ()I
    //   128: istore #7
    //   130: iconst_0
    //   131: istore #8
    //   133: iconst_0
    //   134: istore #4
    //   136: iconst_0
    //   137: istore #9
    //   139: iload #9
    //   141: aload_1
    //   142: arraylength
    //   143: if_icmpge -> 299
    //   146: aload_2
    //   147: aload_1
    //   148: iload #9
    //   150: aaload
    //   151: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   156: istore #10
    //   158: aload_2
    //   159: aload_1
    //   160: iload #9
    //   162: aaload
    //   163: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   168: istore #11
    //   170: iload #5
    //   172: iload #10
    //   174: invokestatic min : (II)I
    //   177: istore #5
    //   179: iload #6
    //   181: iload #11
    //   183: invokestatic max : (II)I
    //   186: istore #6
    //   188: iload #7
    //   190: istore #12
    //   192: iload #8
    //   194: istore #13
    //   196: iload #4
    //   198: istore #14
    //   200: iload_3
    //   201: iload #10
    //   203: if_icmplt -> 281
    //   206: iload_3
    //   207: iload #11
    //   209: if_icmple -> 227
    //   212: iload #7
    //   214: istore #12
    //   216: iload #8
    //   218: istore #13
    //   220: iload #4
    //   222: istore #14
    //   224: goto -> 281
    //   227: iload #4
    //   229: ifne -> 256
    //   232: aload_1
    //   233: iload #9
    //   235: aaload
    //   236: astore #15
    //   238: aload #15
    //   240: invokevirtual getSuggestions : ()[Ljava/lang/String;
    //   243: arraylength
    //   244: ifle -> 250
    //   247: goto -> 256
    //   250: iconst_0
    //   251: istore #4
    //   253: goto -> 259
    //   256: iconst_1
    //   257: istore #4
    //   259: iload #7
    //   261: iload #10
    //   263: invokestatic min : (II)I
    //   266: istore #12
    //   268: iload #8
    //   270: iload #11
    //   272: invokestatic max : (II)I
    //   275: istore #13
    //   277: iload #4
    //   279: istore #14
    //   281: iinc #9, 1
    //   284: iload #12
    //   286: istore #7
    //   288: iload #13
    //   290: istore #8
    //   292: iload #14
    //   294: istore #4
    //   296: goto -> 139
    //   299: iload #4
    //   301: ifne -> 306
    //   304: iconst_0
    //   305: ireturn
    //   306: iload #7
    //   308: iload #8
    //   310: if_icmplt -> 315
    //   313: iconst_0
    //   314: ireturn
    //   315: iload #5
    //   317: iload #7
    //   319: if_icmplt -> 334
    //   322: iload #6
    //   324: iload #8
    //   326: if_icmple -> 332
    //   329: goto -> 334
    //   332: iconst_1
    //   333: ireturn
    //   334: iconst_0
    //   335: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2457	-> 0
    //   #2458	-> 8
    //   #2460	-> 17
    //   #2461	-> 22
    //   #2462	-> 30
    //   #2463	-> 39
    //   #2465	-> 55
    //   #2466	-> 60
    //   #2468	-> 62
    //   #2470	-> 68
    //   #2471	-> 78
    //   #2472	-> 89
    //   #2470	-> 91
    //   #2475	-> 97
    //   #2477	-> 99
    //   #2478	-> 113
    //   #2479	-> 116
    //   #2480	-> 130
    //   #2481	-> 133
    //   #2482	-> 136
    //   #2483	-> 146
    //   #2484	-> 158
    //   #2485	-> 170
    //   #2486	-> 179
    //   #2487	-> 188
    //   #2489	-> 212
    //   #2491	-> 227
    //   #2492	-> 238
    //   #2493	-> 259
    //   #2494	-> 259
    //   #2495	-> 268
    //   #2496	-> 268
    //   #2482	-> 281
    //   #2498	-> 299
    //   #2499	-> 304
    //   #2501	-> 306
    //   #2503	-> 313
    //   #2505	-> 315
    //   #2511	-> 332
    //   #2509	-> 334
  }
  
  private boolean isCursorInsideEasyCorrectionSpan() {
    Spannable spannable = (Spannable)this.mTextView.getText();
    int i = this.mTextView.getSelectionStart();
    TextView textView = this.mTextView;
    int j = textView.getSelectionEnd();
    SuggestionSpan[] arrayOfSuggestionSpan = spannable.<SuggestionSpan>getSpans(i, j, SuggestionSpan.class);
    for (j = 0; j < arrayOfSuggestionSpan.length; j++) {
      if ((arrayOfSuggestionSpan[j].getFlags() & 0x1) != 0)
        return true; 
    } 
    return false;
  }
  
  void onTouchUpEvent(MotionEvent paramMotionEvent) {
    SelectionActionModeHelper selectionActionModeHelper = getSelectionActionModeHelper();
    int i = getTextView().getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY());
    if (selectionActionModeHelper.resetSelection(i))
      return; 
    if (this.mSelectAllOnFocus && this.mTextView.didTouchFocusSelect()) {
      i = 1;
    } else {
      i = 0;
    } 
    hideCursorAndSpanControllers();
    if (!((IOplusFloatingToolbarUtil)OplusFeatureCache.getOrCreate(IOplusFloatingToolbarUtil.DEFAULT, new Object[0])).needHook())
      stopTextActionMode(); 
    int j = this.mTextView.getSelectionStart();
    CharSequence charSequence = this.mTextView.getText();
    if (i == 0 && charSequence.length() > 0) {
      i = this.mTextView.getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY());
      int k = true ^ this.mRequestingLinkActionMode;
      if (k != 0) {
        Selection.setSelection((Spannable)charSequence, i);
        SpellChecker spellChecker = this.mSpellChecker;
        if (spellChecker != null)
          spellChecker.onSelectionChanged(); 
      } 
      if (!extractedTextModeWillBeStarted())
        if (isCursorInsideEasyCorrectionSpan()) {
          Runnable runnable = this.mInsertionActionModeRunnable;
          if (runnable != null)
            this.mTextView.removeCallbacks(runnable); 
          this.mShowSuggestionRunnable = runnable = new _$$Lambda$DZXn7FbDDFyBvNjI_iG9_hfa7kw(this);
          TextView textView = this.mTextView;
          long l = ViewConfiguration.getDoubleTapTimeout();
          textView.postDelayed(runnable, l);
        } else if (hasInsertionController()) {
          if (k != 0) {
            if (((IOplusFloatingToolbarUtil)OplusFeatureCache.getOrCreate(IOplusFloatingToolbarUtil.DEFAULT, new Object[0])).needHook()) {
              ActionMode actionMode = this.mTextActionMode;
              if (actionMode != null) {
                actionMode.finish();
              } else if (j == i && this.mIsFousedBeforeTouch) {
                startInsertionActionMode();
              } 
            } 
            getInsertionController().show();
          } else {
            getInsertionController().hide();
          } 
        }  
    } 
  }
  
  final void onTextOperationUserChanged() {
    SpellChecker spellChecker = this.mSpellChecker;
    if (spellChecker != null)
      spellChecker.resetSession(); 
  }
  
  protected void stopTextActionMode() {
    ActionMode actionMode = this.mTextActionMode;
    if (actionMode != null)
      actionMode.finish(); 
  }
  
  private void stopTextActionModeWithPreservingSelection() {
    if (this.mTextActionMode != null)
      this.mRestartActionModeOnNextRefresh = true; 
    this.mPreserveSelection = true;
    stopTextActionMode();
    this.mPreserveSelection = false;
  }
  
  boolean hasInsertionController() {
    return this.mInsertionControllerEnabled;
  }
  
  boolean hasSelectionController() {
    return this.mSelectionControllerEnabled;
  }
  
  public InsertionPointCursorController getInsertionController() {
    if (!this.mInsertionControllerEnabled)
      return null; 
    if (this.mInsertionPointCursorController == null) {
      this.mInsertionPointCursorController = new InsertionPointCursorController();
      ViewTreeObserver viewTreeObserver = this.mTextView.getViewTreeObserver();
      viewTreeObserver.addOnTouchModeChangeListener(this.mInsertionPointCursorController);
    } 
    return this.mInsertionPointCursorController;
  }
  
  public SelectionModifierCursorController getSelectionController() {
    if (!this.mSelectionControllerEnabled)
      return null; 
    if (this.mSelectionModifierCursorController == null) {
      this.mSelectionModifierCursorController = new SelectionModifierCursorController();
      ViewTreeObserver viewTreeObserver = this.mTextView.getViewTreeObserver();
      viewTreeObserver.addOnTouchModeChangeListener(this.mSelectionModifierCursorController);
    } 
    return this.mSelectionModifierCursorController;
  }
  
  public Drawable getCursorDrawable() {
    return this.mDrawableForCursor;
  }
  
  private void updateCursorPosition(int paramInt1, int paramInt2, float paramFloat) {
    loadCursorDrawable();
    int i = clampHorizontalPosition(this.mDrawableForCursor, paramFloat);
    int j = this.mDrawableForCursor.getIntrinsicWidth();
    this.mDrawableForCursor.setBounds(i, paramInt1 - this.mTempRect.top, i + j, this.mTempRect.bottom + paramInt2);
  }
  
  private int clampHorizontalPosition(Drawable paramDrawable, float paramFloat) {
    float f = Math.max(0.5F, paramFloat - 0.5F);
    if (this.mTempRect == null)
      this.mTempRect = new Rect(); 
    int i = 0;
    if (paramDrawable != null) {
      paramDrawable.getPadding(this.mTempRect);
      i = paramDrawable.getIntrinsicWidth();
    } else {
      this.mTempRect.setEmpty();
    } 
    int j = this.mTextView.getScrollX();
    paramFloat = f - j;
    int k = this.mTextView.getWidth(), m = this.mTextView.getCompoundPaddingLeft();
    TextView textView = this.mTextView;
    m = k - m - textView.getCompoundPaddingRight();
    if (paramFloat >= m - 1.0F) {
      i = m + j - i - this.mTempRect.right;
    } else {
      if (Math.abs(paramFloat) > 1.0F) {
        textView = this.mTextView;
        if (TextUtils.isEmpty(textView.getText()) && (1048576 - j) <= m + 1.0F && f <= 1.0F)
          i = j - this.mTempRect.left; 
        i = (int)f - this.mTempRect.left;
        return i;
      } 
      i = j - this.mTempRect.left;
    } 
    return i;
  }
  
  public void onCommitCorrection(CorrectionInfo paramCorrectionInfo) {
    CorrectionHighlighter correctionHighlighter = this.mCorrectionHighlighter;
    if (correctionHighlighter == null) {
      this.mCorrectionHighlighter = new CorrectionHighlighter();
    } else {
      correctionHighlighter.invalidate(false);
    } 
    this.mCorrectionHighlighter.highlight(paramCorrectionInfo);
    this.mUndoInputFilter.freezeLastEdit();
  }
  
  void onScrollChanged() {
    PositionListener positionListener = this.mPositionListener;
    if (positionListener != null)
      positionListener.onScrollChanged(); 
    ActionMode actionMode = this.mTextActionMode;
    if (actionMode != null)
      actionMode.invalidateContentRect(); 
  }
  
  private boolean shouldBlink() {
    boolean bool = isCursorVisible();
    boolean bool1 = false;
    if (!bool || !this.mTextView.isFocused())
      return false; 
    int i = this.mTextView.getSelectionStart();
    if (i < 0)
      return false; 
    int j = this.mTextView.getSelectionEnd();
    if (j < 0)
      return false; 
    if (i == j)
      bool1 = true; 
    return bool1;
  }
  
  void makeBlink() {
    if (shouldBlink()) {
      this.mShowCursor = SystemClock.uptimeMillis();
      if (this.mBlink == null)
        this.mBlink = new Blink(); 
      this.mTextView.removeCallbacks(this.mBlink);
      this.mTextView.postDelayed(this.mBlink, 500L);
    } else {
      Blink blink = this.mBlink;
      if (blink != null)
        this.mTextView.removeCallbacks(blink); 
    } 
  }
  
  class Blink implements Runnable {
    private boolean mCancelled;
    
    final Editor this$0;
    
    private Blink() {}
    
    public void run() {
      if (this.mCancelled)
        return; 
      Editor.this.mTextView.removeCallbacks(this);
      if (Editor.this.shouldBlink()) {
        if (Editor.this.mTextView.getLayout() != null)
          Editor.this.mTextView.invalidateCursorPath(); 
        Editor.this.mTextView.postDelayed(this, 500L);
      } 
    }
    
    void cancel() {
      if (!this.mCancelled) {
        Editor.this.mTextView.removeCallbacks(this);
        this.mCancelled = true;
      } 
    }
    
    void uncancel() {
      this.mCancelled = false;
    }
  }
  
  private View.DragShadowBuilder getTextThumbnailBuilder(int paramInt1, int paramInt2) {
    TextView textView = (TextView)View.inflate(this.mTextView.getContext(), 17367323, null);
    if (textView != null) {
      int i = paramInt2;
      if (paramInt2 - paramInt1 > 20) {
        long l = getCharClusterRange(paramInt1 + 20);
        i = TextUtils.unpackRangeEndFromLong(l);
      } 
      CharSequence charSequence = this.mTextView.getTransformedText(paramInt1, i);
      textView.setText(charSequence);
      textView.setTextColor(this.mTextView.getTextColors());
      textView.setTextAppearance(16);
      textView.setGravity(17);
      textView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
      paramInt1 = View.MeasureSpec.makeMeasureSpec(0, 0);
      textView.measure(paramInt1, paramInt1);
      i = textView.getMeasuredWidth();
      int j = textView.getMeasuredHeight();
      paramInt2 = i;
      paramInt1 = j;
      if (i == 0) {
        paramInt2 = i;
        paramInt1 = j;
        if (charSequence != null) {
          paramInt2 = i;
          paramInt1 = j;
          if (charSequence.length() > 0) {
            paramInt2 = 1;
            paramInt1 = 1;
          } 
        } 
      } 
      textView.layout(0, 0, paramInt2, paramInt1);
      textView.invalidate();
      return new View.DragShadowBuilder(textView);
    } 
    throw new IllegalArgumentException("Unable to inflate text drag thumbnail");
  }
  
  class DragLocalState {
    public int end;
    
    public TextView sourceTextView;
    
    public int start;
    
    public DragLocalState(Editor this$0, int param1Int1, int param1Int2) {
      this.sourceTextView = (TextView)this$0;
      this.start = param1Int1;
      this.end = param1Int2;
    }
  }
  
  void onDrop(DragEvent paramDragEvent) {
    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
    Object object = DragAndDropPermissions.obtain(paramDragEvent);
    if (object != null)
      object.takeTransient(); 
    try {
      ClipData clipData = paramDragEvent.getClipData();
      int i = clipData.getItemCount();
      int j;
      for (j = 0; j < i; j++) {
        ClipData.Item item = clipData.getItemAt(j);
        spannableStringBuilder.append(item.coerceToStyledText(this.mTextView.getContext()));
      } 
      if (object != null)
        object.release(); 
      this.mTextView.beginBatchEdit();
      this.mUndoInputFilter.freezeLastEdit();
    } finally {
      if (object != null)
        object.release(); 
    } 
  }
  
  public void addSpanWatchers(Spannable paramSpannable) {
    int i = paramSpannable.length();
    KeyListener keyListener = this.mKeyListener;
    if (keyListener != null)
      paramSpannable.setSpan(keyListener, 0, i, 18); 
    if (this.mSpanController == null)
      this.mSpanController = new SpanController(); 
    paramSpannable.setSpan(this.mSpanController, 0, i, 18);
  }
  
  void setContextMenuAnchor(float paramFloat1, float paramFloat2) {
    this.mContextMenuAnchorX = paramFloat1;
    this.mContextMenuAnchorY = paramFloat2;
  }
  
  void onCreateContextMenu(ContextMenu paramContextMenu) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mIsBeingLongClicked : Z
    //   4: ifne -> 801
    //   7: aload_0
    //   8: getfield mContextMenuAnchorX : F
    //   11: invokestatic isNaN : (F)Z
    //   14: ifne -> 801
    //   17: aload_0
    //   18: getfield mContextMenuAnchorY : F
    //   21: fstore_2
    //   22: fload_2
    //   23: invokestatic isNaN : (F)Z
    //   26: ifeq -> 32
    //   29: goto -> 801
    //   32: aload_0
    //   33: getfield mTextView : Landroid/widget/TextView;
    //   36: aload_0
    //   37: getfield mContextMenuAnchorX : F
    //   40: aload_0
    //   41: getfield mContextMenuAnchorY : F
    //   44: invokevirtual getOffsetForPosition : (FF)I
    //   47: istore_3
    //   48: iload_3
    //   49: iconst_m1
    //   50: if_icmpne -> 54
    //   53: return
    //   54: aload_0
    //   55: invokespecial stopTextActionModeWithPreservingSelection : ()V
    //   58: aload_0
    //   59: getfield mTextView : Landroid/widget/TextView;
    //   62: invokevirtual canSelectText : ()Z
    //   65: ifeq -> 140
    //   68: aload_0
    //   69: getfield mTextView : Landroid/widget/TextView;
    //   72: invokevirtual hasSelection : ()Z
    //   75: ifeq -> 114
    //   78: aload_0
    //   79: getfield mTextView : Landroid/widget/TextView;
    //   82: astore #4
    //   84: iload_3
    //   85: aload #4
    //   87: invokevirtual getSelectionStart : ()I
    //   90: if_icmplt -> 114
    //   93: aload_0
    //   94: getfield mTextView : Landroid/widget/TextView;
    //   97: astore #4
    //   99: iload_3
    //   100: aload #4
    //   102: invokevirtual getSelectionEnd : ()I
    //   105: if_icmpgt -> 114
    //   108: iconst_1
    //   109: istore #5
    //   111: goto -> 117
    //   114: iconst_0
    //   115: istore #5
    //   117: iload #5
    //   119: ifne -> 140
    //   122: aload_0
    //   123: getfield mTextView : Landroid/widget/TextView;
    //   126: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   129: checkcast android/text/Spannable
    //   132: iload_3
    //   133: invokestatic setSelection : (Landroid/text/Spannable;I)V
    //   136: aload_0
    //   137: invokevirtual stopTextActionMode : ()V
    //   140: aload_0
    //   141: invokespecial shouldOfferToShowSuggestions : ()Z
    //   144: ifeq -> 271
    //   147: iconst_5
    //   148: anewarray android/widget/Editor$SuggestionInfo
    //   151: astore #6
    //   153: iconst_0
    //   154: istore #5
    //   156: iload #5
    //   158: aload #6
    //   160: arraylength
    //   161: if_icmpge -> 183
    //   164: aload #6
    //   166: iload #5
    //   168: new android/widget/Editor$SuggestionInfo
    //   171: dup
    //   172: aconst_null
    //   173: invokespecial <init> : (Landroid/widget/Editor$1;)V
    //   176: aastore
    //   177: iinc #5, 1
    //   180: goto -> 156
    //   183: aload_1
    //   184: iconst_0
    //   185: iconst_0
    //   186: bipush #9
    //   188: ldc_w 17041169
    //   191: invokeinterface addSubMenu : (IIII)Landroid/view/SubMenu;
    //   196: astore #7
    //   198: aload_0
    //   199: getfield mSuggestionHelper : Landroid/widget/Editor$SuggestionHelper;
    //   202: aload #6
    //   204: aconst_null
    //   205: invokevirtual getSuggestionInfo : ([Landroid/widget/Editor$SuggestionInfo;Landroid/widget/Editor$SuggestionSpanInfo;)I
    //   208: istore_3
    //   209: iconst_0
    //   210: istore #5
    //   212: iload #5
    //   214: iload_3
    //   215: if_icmpge -> 271
    //   218: aload #6
    //   220: iload #5
    //   222: aaload
    //   223: astore #8
    //   225: aload #7
    //   227: iconst_0
    //   228: iconst_0
    //   229: iload #5
    //   231: aload #8
    //   233: getfield mText : Landroid/text/SpannableStringBuilder;
    //   236: invokeinterface add : (IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    //   241: astore #4
    //   243: new android/widget/Editor$4
    //   246: dup
    //   247: aload_0
    //   248: aload #8
    //   250: invokespecial <init> : (Landroid/widget/Editor;Landroid/widget/Editor$SuggestionInfo;)V
    //   253: astore #8
    //   255: aload #4
    //   257: aload #8
    //   259: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   264: pop
    //   265: iinc #5, 1
    //   268: goto -> 212
    //   271: aload_1
    //   272: iconst_0
    //   273: ldc_w 16908338
    //   276: iconst_2
    //   277: ldc_w 17041421
    //   280: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   285: astore #4
    //   287: aload #4
    //   289: bipush #122
    //   291: invokeinterface setAlphabeticShortcut : (C)Landroid/view/MenuItem;
    //   296: astore #6
    //   298: aload_0
    //   299: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   302: astore #4
    //   304: aload #6
    //   306: aload #4
    //   308: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   313: astore #4
    //   315: aload_0
    //   316: getfield mTextView : Landroid/widget/TextView;
    //   319: astore #6
    //   321: aload #4
    //   323: aload #6
    //   325: invokevirtual canUndo : ()Z
    //   328: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   333: pop
    //   334: aload_1
    //   335: iconst_0
    //   336: ldc_w 16908339
    //   339: iconst_3
    //   340: ldc_w 17041151
    //   343: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   348: astore #6
    //   350: aload_0
    //   351: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   354: astore #4
    //   356: aload #6
    //   358: aload #4
    //   360: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   365: astore #6
    //   367: aload_0
    //   368: getfield mTextView : Landroid/widget/TextView;
    //   371: astore #4
    //   373: aload #6
    //   375: aload #4
    //   377: invokevirtual canRedo : ()Z
    //   380: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   385: pop
    //   386: aload_1
    //   387: iconst_0
    //   388: ldc_w 16908320
    //   391: iconst_4
    //   392: ldc_w 17039363
    //   395: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   400: astore #4
    //   402: aload #4
    //   404: bipush #120
    //   406: invokeinterface setAlphabeticShortcut : (C)Landroid/view/MenuItem;
    //   411: astore #4
    //   413: aload_0
    //   414: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   417: astore #6
    //   419: aload #4
    //   421: aload #6
    //   423: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   428: astore #4
    //   430: aload_0
    //   431: getfield mTextView : Landroid/widget/TextView;
    //   434: astore #6
    //   436: aload #4
    //   438: aload #6
    //   440: invokevirtual canCut : ()Z
    //   443: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   448: pop
    //   449: aload_1
    //   450: iconst_0
    //   451: ldc_w 16908321
    //   454: iconst_5
    //   455: ldc_w 17039361
    //   458: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   463: astore #4
    //   465: aload #4
    //   467: bipush #99
    //   469: invokeinterface setAlphabeticShortcut : (C)Landroid/view/MenuItem;
    //   474: astore #6
    //   476: aload_0
    //   477: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   480: astore #4
    //   482: aload #6
    //   484: aload #4
    //   486: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   491: astore #4
    //   493: aload_0
    //   494: getfield mTextView : Landroid/widget/TextView;
    //   497: astore #6
    //   499: aload #4
    //   501: aload #6
    //   503: invokevirtual canCopy : ()Z
    //   506: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   511: pop
    //   512: aload_1
    //   513: iconst_0
    //   514: ldc_w 16908322
    //   517: bipush #6
    //   519: ldc_w 17039371
    //   522: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   527: astore #4
    //   529: aload #4
    //   531: bipush #118
    //   533: invokeinterface setAlphabeticShortcut : (C)Landroid/view/MenuItem;
    //   538: astore #6
    //   540: aload_0
    //   541: getfield mTextView : Landroid/widget/TextView;
    //   544: astore #4
    //   546: aload #6
    //   548: aload #4
    //   550: invokevirtual canPaste : ()Z
    //   553: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   558: astore #4
    //   560: aload_0
    //   561: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   564: astore #6
    //   566: aload #4
    //   568: aload #6
    //   570: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   575: pop
    //   576: aload_1
    //   577: iconst_0
    //   578: ldc_w 16908337
    //   581: bipush #11
    //   583: ldc_w 17039385
    //   586: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   591: astore #6
    //   593: aload_0
    //   594: getfield mTextView : Landroid/widget/TextView;
    //   597: astore #4
    //   599: aload #6
    //   601: aload #4
    //   603: invokevirtual canPasteAsPlainText : ()Z
    //   606: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   611: astore #6
    //   613: aload_0
    //   614: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   617: astore #4
    //   619: aload #6
    //   621: aload #4
    //   623: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   628: pop
    //   629: aload_1
    //   630: iconst_0
    //   631: ldc_w 16908341
    //   634: bipush #7
    //   636: ldc_w 17041273
    //   639: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   644: astore #4
    //   646: aload_0
    //   647: getfield mTextView : Landroid/widget/TextView;
    //   650: astore #6
    //   652: aload #4
    //   654: aload #6
    //   656: invokevirtual canShare : ()Z
    //   659: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   664: astore #4
    //   666: aload_0
    //   667: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   670: astore #6
    //   672: aload #4
    //   674: aload #6
    //   676: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   681: pop
    //   682: aload_1
    //   683: iconst_0
    //   684: ldc_w 16908319
    //   687: bipush #8
    //   689: ldc_w 17039373
    //   692: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   697: astore #4
    //   699: aload #4
    //   701: bipush #97
    //   703: invokeinterface setAlphabeticShortcut : (C)Landroid/view/MenuItem;
    //   708: astore #6
    //   710: aload_0
    //   711: getfield mTextView : Landroid/widget/TextView;
    //   714: astore #4
    //   716: aload #6
    //   718: aload #4
    //   720: invokevirtual canSelectAllText : ()Z
    //   723: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   728: astore #6
    //   730: aload_0
    //   731: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   734: astore #4
    //   736: aload #6
    //   738: aload #4
    //   740: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   745: pop
    //   746: aload_1
    //   747: iconst_0
    //   748: ldc_w 16908355
    //   751: bipush #10
    //   753: ldc_w 17039386
    //   756: invokeinterface add : (IIII)Landroid/view/MenuItem;
    //   761: astore_1
    //   762: aload_0
    //   763: getfield mTextView : Landroid/widget/TextView;
    //   766: astore #4
    //   768: aload_1
    //   769: aload #4
    //   771: invokevirtual canRequestAutofill : ()Z
    //   774: invokeinterface setEnabled : (Z)Landroid/view/MenuItem;
    //   779: astore_1
    //   780: aload_0
    //   781: getfield mOnContextMenuItemClickListener : Landroid/view/MenuItem$OnMenuItemClickListener;
    //   784: astore #4
    //   786: aload_1
    //   787: aload #4
    //   789: invokeinterface setOnMenuItemClickListener : (Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    //   794: pop
    //   795: aload_0
    //   796: iconst_1
    //   797: putfield mPreserveSelection : Z
    //   800: return
    //   801: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2968	-> 0
    //   #2969	-> 22
    //   #2972	-> 32
    //   #2973	-> 48
    //   #2974	-> 53
    //   #2977	-> 54
    //   #2978	-> 58
    //   #2979	-> 68
    //   #2980	-> 84
    //   #2981	-> 99
    //   #2982	-> 117
    //   #2985	-> 122
    //   #2986	-> 136
    //   #2990	-> 140
    //   #2991	-> 147
    //   #2993	-> 153
    //   #2994	-> 164
    //   #2993	-> 177
    //   #2996	-> 183
    //   #2998	-> 198
    //   #2999	-> 209
    //   #3000	-> 218
    //   #3001	-> 225
    //   #3002	-> 255
    //   #2999	-> 265
    //   #3012	-> 271
    //   #3014	-> 287
    //   #3015	-> 304
    //   #3016	-> 321
    //   #3017	-> 334
    //   #3019	-> 356
    //   #3020	-> 373
    //   #3022	-> 386
    //   #3024	-> 402
    //   #3025	-> 419
    //   #3026	-> 436
    //   #3027	-> 449
    //   #3029	-> 465
    //   #3030	-> 482
    //   #3031	-> 499
    //   #3032	-> 512
    //   #3034	-> 529
    //   #3035	-> 546
    //   #3036	-> 566
    //   #3037	-> 576
    //   #3039	-> 599
    //   #3040	-> 619
    //   #3041	-> 629
    //   #3043	-> 652
    //   #3044	-> 672
    //   #3045	-> 682
    //   #3047	-> 699
    //   #3048	-> 716
    //   #3049	-> 736
    //   #3050	-> 746
    //   #3052	-> 768
    //   #3053	-> 786
    //   #3055	-> 795
    //   #3056	-> 800
    //   #2970	-> 801
  }
  
  private SuggestionSpan findEquivalentSuggestionSpan(SuggestionSpanInfo paramSuggestionSpanInfo) {
    Editable editable = (Editable)this.mTextView.getText();
    if (editable.getSpanStart(paramSuggestionSpanInfo.mSuggestionSpan) >= 0)
      return paramSuggestionSpanInfo.mSuggestionSpan; 
    for (SuggestionSpan suggestionSpan : (SuggestionSpan[])editable.<SuggestionSpan>getSpans(paramSuggestionSpanInfo.mSpanStart, paramSuggestionSpanInfo.mSpanEnd, SuggestionSpan.class)) {
      int i = editable.getSpanStart(suggestionSpan);
      if (i == paramSuggestionSpanInfo.mSpanStart) {
        i = editable.getSpanEnd(suggestionSpan);
        if (i == paramSuggestionSpanInfo.mSpanEnd)
          if (suggestionSpan.equals(paramSuggestionSpanInfo.mSuggestionSpan))
            return suggestionSpan;  
      } 
    } 
    return null;
  }
  
  private void replaceWithSuggestion(SuggestionInfo paramSuggestionInfo) {
    SuggestionSpan suggestionSpan = findEquivalentSuggestionSpan(paramSuggestionInfo.mSuggestionSpanInfo);
    if (suggestionSpan == null)
      return; 
    Editable editable = (Editable)this.mTextView.getText();
    int i = editable.getSpanStart(suggestionSpan);
    int j = editable.getSpanEnd(suggestionSpan);
    if (i < 0 || j <= i)
      return; 
    String str = TextUtils.substring(editable, i, j);
    SuggestionSpan[] arrayOfSuggestionSpan2 = editable.<SuggestionSpan>getSpans(i, j, SuggestionSpan.class);
    int k = arrayOfSuggestionSpan2.length;
    int[] arrayOfInt1 = new int[k];
    int[] arrayOfInt2 = new int[k];
    int[] arrayOfInt3 = new int[k];
    int m;
    for (m = 0; m < k; m++) {
      SuggestionSpan suggestionSpan1 = arrayOfSuggestionSpan2[m];
      arrayOfInt1[m] = editable.getSpanStart(suggestionSpan1);
      arrayOfInt2[m] = editable.getSpanEnd(suggestionSpan1);
      arrayOfInt3[m] = editable.getSpanFlags(suggestionSpan1);
      int i2 = suggestionSpan1.getFlags();
      if ((i2 & 0x2) != 0)
        suggestionSpan1.setFlags(i2 & 0xFFFFFFFD & 0xFFFFFFFE); 
    } 
    int n = paramSuggestionInfo.mSuggestionStart;
    m = paramSuggestionInfo.mSuggestionEnd;
    CharSequence charSequence = paramSuggestionInfo.mText.subSequence(n, m);
    charSequence = charSequence.toString();
    this.mTextView.replaceText_internal(i, j, charSequence);
    String[] arrayOfString = suggestionSpan.getSuggestions();
    arrayOfString[paramSuggestionInfo.mSuggestionIndex] = str;
    int i1 = charSequence.length() - j - i;
    SuggestionSpan[] arrayOfSuggestionSpan1;
    for (n = 0, m = k, arrayOfSuggestionSpan1 = arrayOfSuggestionSpan2, k = n; k < m; k++) {
      if (arrayOfInt1[k] <= i && arrayOfInt2[k] >= j)
        this.mTextView.setSpan_internal(arrayOfSuggestionSpan1[k], arrayOfInt1[k], arrayOfInt2[k] + i1, arrayOfInt3[k]); 
    } 
    m = j + i1;
    this.mTextView.setCursorPosition_internal(m, m);
  }
  
  class SpanController implements SpanWatcher {
    private static final int DISPLAY_TIMEOUT_MS = 3000;
    
    private Runnable mHidePopup;
    
    private Editor.EasyEditPopupWindow mPopupWindow;
    
    final Editor this$0;
    
    private SpanController() {}
    
    private boolean isNonIntermediateSelectionSpan(Spannable param1Spannable, Object param1Object) {
      boolean bool;
      if ((Selection.SELECTION_START == param1Object || Selection.SELECTION_END == param1Object) && (param1Spannable.getSpanFlags(param1Object) & 0x200) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void onSpanAdded(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2) {
      if (isNonIntermediateSelectionSpan(param1Spannable, param1Object)) {
        Editor.this.sendUpdateSelection();
      } else if (param1Object instanceof EasyEditSpan) {
        if (this.mPopupWindow == null) {
          this.mPopupWindow = new Editor.EasyEditPopupWindow();
          this.mHidePopup = (Runnable)new Object(this);
        } 
        if (this.mPopupWindow.mEasyEditSpan != null)
          this.mPopupWindow.mEasyEditSpan.setDeleteEnabled(false); 
        this.mPopupWindow.setEasyEditSpan((EasyEditSpan)param1Object);
        this.mPopupWindow.setOnDeleteListener((Editor.EasyEditDeleteListener)new Object(this));
        if (Editor.this.mTextView.getWindowVisibility() != 0)
          return; 
        if (Editor.this.mTextView.getLayout() == null)
          return; 
        if (Editor.this.extractedTextModeWillBeStarted())
          return; 
        this.mPopupWindow.show();
        Editor.this.mTextView.removeCallbacks(this.mHidePopup);
        Editor.this.mTextView.postDelayed(this.mHidePopup, 3000L);
      } 
    }
    
    public void onSpanRemoved(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2) {
      if (isNonIntermediateSelectionSpan(param1Spannable, param1Object)) {
        Editor.this.sendUpdateSelection();
      } else {
        Editor.EasyEditPopupWindow easyEditPopupWindow = this.mPopupWindow;
        if (easyEditPopupWindow != null && param1Object == easyEditPopupWindow.mEasyEditSpan)
          hide(); 
      } 
    }
    
    public void onSpanChanged(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      if (isNonIntermediateSelectionSpan(param1Spannable, param1Object)) {
        Editor.this.sendUpdateSelection();
      } else if (this.mPopupWindow != null && param1Object instanceof EasyEditSpan) {
        param1Object = param1Object;
        sendEasySpanNotification(2, (EasyEditSpan)param1Object);
        param1Spannable.removeSpan(param1Object);
      } 
    }
    
    public void hide() {
      Editor.EasyEditPopupWindow easyEditPopupWindow = this.mPopupWindow;
      if (easyEditPopupWindow != null) {
        easyEditPopupWindow.hide();
        Editor.this.mTextView.removeCallbacks(this.mHidePopup);
      } 
    }
    
    private void sendEasySpanNotification(int param1Int, EasyEditSpan param1EasyEditSpan) {
      try {
        PendingIntent pendingIntent = param1EasyEditSpan.getPendingIntent();
        if (pendingIntent != null) {
          Intent intent = new Intent();
          this();
          intent.putExtra("android.text.style.EXTRA_TEXT_CHANGED_TYPE", param1Int);
          pendingIntent.send(Editor.this.mTextView.getContext(), 0, intent);
        } 
      } catch (android.app.PendingIntent.CanceledException canceledException) {
        Log.w("Editor", "PendingIntent for notification cannot be sent", (Throwable)canceledException);
      } 
    }
  }
  
  class EasyEditPopupWindow extends PinnedPopupWindow implements View.OnClickListener {
    private static final int POPUP_TEXT_LAYOUT = 17367324;
    
    private TextView mDeleteTextView;
    
    private EasyEditSpan mEasyEditSpan;
    
    private Editor.EasyEditDeleteListener mOnDeleteListener;
    
    final Editor this$0;
    
    private EasyEditPopupWindow() {}
    
    protected void createPopupWindow() {
      this.mPopupWindow = new PopupWindow(Editor.this.mTextView.getContext(), null, 16843464);
      this.mPopupWindow.setInputMethodMode(2);
      this.mPopupWindow.setClippingEnabled(true);
    }
    
    protected void initContentView() {
      LinearLayout linearLayout = new LinearLayout(Editor.this.mTextView.getContext());
      linearLayout.setOrientation(0);
      this.mContentView = linearLayout;
      this.mContentView.setBackgroundResource(17303735);
      Context context = Editor.this.mTextView.getContext();
      LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
      ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -2);
      TextView textView = (TextView)layoutInflater.inflate(17367324, (ViewGroup)null);
      textView.setLayoutParams(layoutParams);
      this.mDeleteTextView.setText(17040061);
      this.mDeleteTextView.setOnClickListener(this);
      this.mContentView.addView(this.mDeleteTextView);
    }
    
    public void setEasyEditSpan(EasyEditSpan param1EasyEditSpan) {
      this.mEasyEditSpan = param1EasyEditSpan;
    }
    
    private void setOnDeleteListener(Editor.EasyEditDeleteListener param1EasyEditDeleteListener) {
      this.mOnDeleteListener = param1EasyEditDeleteListener;
    }
    
    public void onClick(View param1View) {
      if (param1View == this.mDeleteTextView) {
        EasyEditSpan easyEditSpan = this.mEasyEditSpan;
        if (easyEditSpan != null && easyEditSpan.isDeleteEnabled()) {
          Editor.EasyEditDeleteListener easyEditDeleteListener = this.mOnDeleteListener;
          if (easyEditDeleteListener != null)
            easyEditDeleteListener.onDeleteClick(this.mEasyEditSpan); 
        } 
      } 
    }
    
    public void hide() {
      EasyEditSpan easyEditSpan = this.mEasyEditSpan;
      if (easyEditSpan != null)
        easyEditSpan.setDeleteEnabled(false); 
      this.mOnDeleteListener = null;
      super.hide();
    }
    
    protected int getTextOffset() {
      Editable editable = (Editable)Editor.this.mTextView.getText();
      return editable.getSpanEnd(this.mEasyEditSpan);
    }
    
    protected int getVerticalLocalPosition(int param1Int) {
      Layout layout = Editor.this.mTextView.getLayout();
      return layout.getLineBottomWithoutSpacing(param1Int);
    }
    
    protected int clipVertically(int param1Int) {
      return param1Int;
    }
  }
  
  class PositionListener implements ViewTreeObserver.OnPreDrawListener {
    private Editor.TextViewPositionListener[] mPositionListeners = new Editor.TextViewPositionListener[7];
    
    private boolean[] mCanMove = new boolean[7];
    
    private boolean mPositionHasChanged = true;
    
    final int[] mTempCoords = new int[2];
    
    private static final int MAXIMUM_NUMBER_OF_LISTENERS = 7;
    
    private int mNumberOfListeners;
    
    private int mPositionX;
    
    private int mPositionXOnScreen;
    
    private int mPositionY;
    
    private int mPositionYOnScreen;
    
    private boolean mScrollHasChanged;
    
    final Editor this$0;
    
    public void addSubscriber(Editor.TextViewPositionListener param1TextViewPositionListener, boolean param1Boolean) {
      if (this.mNumberOfListeners == 0) {
        updatePosition();
        ViewTreeObserver viewTreeObserver = Editor.this.mTextView.getViewTreeObserver();
        viewTreeObserver.addOnPreDrawListener(this);
      } 
      byte b = -1;
      for (byte b1 = 0; b1 < 7; b1++, b = b2) {
        Editor.TextViewPositionListener textViewPositionListener = this.mPositionListeners[b1];
        if (textViewPositionListener == param1TextViewPositionListener)
          return; 
        byte b2 = b;
        if (b < 0) {
          b2 = b;
          if (textViewPositionListener == null)
            b2 = b1; 
        } 
      } 
      this.mPositionListeners[b] = param1TextViewPositionListener;
      this.mCanMove[b] = param1Boolean;
      this.mNumberOfListeners++;
    }
    
    public void removeSubscriber(Editor.TextViewPositionListener param1TextViewPositionListener) {
      for (byte b = 0; b < 7; b++) {
        Editor.TextViewPositionListener[] arrayOfTextViewPositionListener = this.mPositionListeners;
        if (arrayOfTextViewPositionListener[b] == param1TextViewPositionListener) {
          arrayOfTextViewPositionListener[b] = null;
          this.mNumberOfListeners--;
          break;
        } 
      } 
      if (this.mNumberOfListeners == 0) {
        ViewTreeObserver viewTreeObserver = Editor.this.mTextView.getViewTreeObserver();
        viewTreeObserver.removeOnPreDrawListener(this);
      } 
    }
    
    public int getPositionX() {
      return this.mPositionX;
    }
    
    public int getPositionY() {
      return this.mPositionY;
    }
    
    public int getPositionXOnScreen() {
      return this.mPositionXOnScreen;
    }
    
    public int getPositionYOnScreen() {
      return this.mPositionYOnScreen;
    }
    
    public boolean onPreDraw() {
      updatePosition();
      for (byte b = 0; b < 7; b++) {
        if (this.mPositionHasChanged || this.mScrollHasChanged || this.mCanMove[b]) {
          Editor.TextViewPositionListener textViewPositionListener = this.mPositionListeners[b];
          if (textViewPositionListener != null)
            textViewPositionListener.updatePosition(this.mPositionX, this.mPositionY, this.mPositionHasChanged, this.mScrollHasChanged); 
        } 
      } 
      this.mScrollHasChanged = false;
      return true;
    }
    
    private void updatePosition() {
      boolean bool;
      Editor.this.mTextView.getLocationInWindow(this.mTempCoords);
      int[] arrayOfInt = this.mTempCoords;
      if (arrayOfInt[0] != this.mPositionX || arrayOfInt[1] != this.mPositionY) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mPositionHasChanged = bool;
      arrayOfInt = this.mTempCoords;
      this.mPositionX = arrayOfInt[0];
      this.mPositionY = arrayOfInt[1];
      Editor.this.mTextView.getLocationOnScreen(this.mTempCoords);
      arrayOfInt = this.mTempCoords;
      this.mPositionXOnScreen = arrayOfInt[0];
      this.mPositionYOnScreen = arrayOfInt[1];
    }
    
    public void onScrollChanged() {
      this.mScrollHasChanged = true;
    }
    
    private PositionListener() {}
  }
  
  class PinnedPopupWindow implements TextViewPositionListener {
    int mClippingLimitLeft;
    
    int mClippingLimitRight;
    
    protected ViewGroup mContentView;
    
    protected PopupWindow mPopupWindow;
    
    int mPositionX;
    
    int mPositionY;
    
    final Editor this$0;
    
    protected void setUp() {}
    
    public PinnedPopupWindow() {
      setUp();
      createPopupWindow();
      this.mPopupWindow.setWindowLayoutType(1005);
      this.mPopupWindow.setWidth(-2);
      this.mPopupWindow.setHeight(-2);
      initContentView();
      ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -2);
      this.mContentView.setLayoutParams(layoutParams);
      this.mPopupWindow.setContentView(this.mContentView);
    }
    
    public void show() {
      Editor.this.getPositionListener().addSubscriber(this, false);
      computeLocalPosition();
      Editor.PositionListener positionListener = Editor.this.getPositionListener();
      updatePosition(positionListener.getPositionX(), positionListener.getPositionY());
    }
    
    protected void measureContent() {
      DisplayMetrics displayMetrics = Editor.this.mTextView.getResources().getDisplayMetrics();
      ViewGroup viewGroup = this.mContentView;
      int i = displayMetrics.widthPixels;
      i = View.MeasureSpec.makeMeasureSpec(i, -2147483648);
      int j = displayMetrics.heightPixels;
      j = View.MeasureSpec.makeMeasureSpec(j, -2147483648);
      viewGroup.measure(i, j);
    }
    
    private void computeLocalPosition() {
      measureContent();
      int i = this.mContentView.getMeasuredWidth();
      int j = getTextOffset();
      this.mPositionX = i = (int)(Editor.this.mTextView.getLayout().getPrimaryHorizontal(j) - i / 2.0F);
      this.mPositionX = i + Editor.this.mTextView.viewportToContentHorizontalOffset();
      j = Editor.this.mTextView.getLayout().getLineForOffset(j);
      this.mPositionY = j = getVerticalLocalPosition(j);
      this.mPositionY = j + Editor.this.mTextView.viewportToContentVerticalOffset();
    }
    
    private void updatePosition(int param1Int1, int param1Int2) {
      int i = this.mPositionX;
      int j = this.mPositionY;
      param1Int2 = clipVertically(j + param1Int2);
      DisplayMetrics displayMetrics = Editor.this.mTextView.getResources().getDisplayMetrics();
      j = this.mContentView.getMeasuredWidth();
      param1Int1 = Math.min(displayMetrics.widthPixels - j + this.mClippingLimitRight, i + param1Int1);
      param1Int1 = Math.max(-this.mClippingLimitLeft, param1Int1);
      if (isShowing()) {
        this.mPopupWindow.update(param1Int1, param1Int2, -1, -1);
      } else {
        this.mPopupWindow.showAtLocation(Editor.this.mTextView, 0, param1Int1, param1Int2);
      } 
    }
    
    public void hide() {
      if (!isShowing())
        return; 
      this.mPopupWindow.dismiss();
      Editor.this.getPositionListener().removeSubscriber(this);
    }
    
    public void updatePosition(int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2) {
      if (isShowing() && Editor.this.isOffsetVisible(getTextOffset())) {
        if (param1Boolean2)
          computeLocalPosition(); 
        updatePosition(param1Int1, param1Int2);
      } else {
        hide();
      } 
    }
    
    public boolean isShowing() {
      return this.mPopupWindow.isShowing();
    }
    
    protected abstract int clipVertically(int param1Int);
    
    protected abstract void createPopupWindow();
    
    protected abstract int getTextOffset();
    
    protected abstract int getVerticalLocalPosition(int param1Int);
    
    protected abstract void initContentView();
  }
  
  class SuggestionInfo {
    int mSuggestionEnd;
    
    int mSuggestionIndex;
    
    private SuggestionInfo() {}
    
    final Editor.SuggestionSpanInfo mSuggestionSpanInfo = new Editor.SuggestionSpanInfo();
    
    int mSuggestionStart;
    
    final SpannableStringBuilder mText = new SpannableStringBuilder();
    
    void clear() {
      this.mSuggestionSpanInfo.clear();
      this.mText.clear();
    }
    
    void setSpanInfo(SuggestionSpan param1SuggestionSpan, int param1Int1, int param1Int2) {
      this.mSuggestionSpanInfo.mSuggestionSpan = param1SuggestionSpan;
      this.mSuggestionSpanInfo.mSpanStart = param1Int1;
      this.mSuggestionSpanInfo.mSpanEnd = param1Int2;
    }
  }
  
  class SuggestionSpanInfo {
    int mSpanEnd;
    
    int mSpanStart;
    
    SuggestionSpan mSuggestionSpan;
    
    private SuggestionSpanInfo() {}
    
    void clear() {
      this.mSuggestionSpan = null;
    }
  }
  
  class SuggestionHelper {
    final Editor this$0;
    
    private SuggestionHelper() {}
    
    private final Comparator<SuggestionSpan> mSuggestionSpanComparator = new SuggestionSpanComparator();
    
    private final HashMap<SuggestionSpan, Integer> mSpansLengths = new HashMap<>();
    
    private class SuggestionSpanComparator implements Comparator<SuggestionSpan> {
      final Editor.SuggestionHelper this$1;
      
      private SuggestionSpanComparator() {}
      
      public int compare(SuggestionSpan param2SuggestionSpan1, SuggestionSpan param2SuggestionSpan2) {
        int i = param2SuggestionSpan1.getFlags();
        int j = param2SuggestionSpan2.getFlags();
        if (i != j) {
          boolean bool2, bool3, bool1 = false;
          if ((i & 0x1) != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          if ((j & 0x1) != 0) {
            bool3 = true;
          } else {
            bool3 = false;
          } 
          if ((i & 0x2) != 0) {
            i = 1;
          } else {
            i = 0;
          } 
          if ((j & 0x2) != 0)
            bool1 = true; 
          if (bool2 && i == 0)
            return -1; 
          if (bool3 && !bool1)
            return 1; 
          if (i != 0)
            return -1; 
          if (bool1)
            return 1; 
        } 
        return ((Integer)Editor.SuggestionHelper.this.mSpansLengths.get(param2SuggestionSpan1)).intValue() - ((Integer)Editor.SuggestionHelper.this.mSpansLengths.get(param2SuggestionSpan2)).intValue();
      }
    }
    
    private SuggestionSpan[] getSortedSuggestionSpans() {
      int i = Editor.this.mTextView.getSelectionStart();
      Spannable spannable = (Spannable)Editor.this.mTextView.getText();
      SuggestionSpan[] arrayOfSuggestionSpan = spannable.<SuggestionSpan>getSpans(i, i, SuggestionSpan.class);
      this.mSpansLengths.clear();
      for (int j = arrayOfSuggestionSpan.length; i < j; ) {
        SuggestionSpan suggestionSpan = arrayOfSuggestionSpan[i];
        int k = spannable.getSpanStart(suggestionSpan);
        int m = spannable.getSpanEnd(suggestionSpan);
        this.mSpansLengths.put(suggestionSpan, Integer.valueOf(m - k));
        i++;
      } 
      Arrays.sort(arrayOfSuggestionSpan, this.mSuggestionSpanComparator);
      this.mSpansLengths.clear();
      return arrayOfSuggestionSpan;
    }
    
    public int getSuggestionInfo(Editor.SuggestionInfo[] param1ArrayOfSuggestionInfo, Editor.SuggestionSpanInfo param1SuggestionSpanInfo) {
      Spannable spannable = (Spannable)Editor.this.mTextView.getText();
      SuggestionSpan[] arrayOfSuggestionSpan = getSortedSuggestionSpans();
      int i = arrayOfSuggestionSpan.length;
      boolean bool = false;
      if (i == 0)
        return 0; 
      int j = 0;
      for (int k = arrayOfSuggestionSpan.length; i < k; ) {
        SuggestionSpan suggestionSpan = arrayOfSuggestionSpan[i];
        int m = spannable.getSpanStart(suggestionSpan);
        int n = spannable.getSpanEnd(suggestionSpan);
        if (param1SuggestionSpanInfo != null && (suggestionSpan.getFlags() & 0x2) != 0) {
          param1SuggestionSpanInfo.mSuggestionSpan = suggestionSpan;
          param1SuggestionSpanInfo.mSpanStart = m;
          param1SuggestionSpanInfo.mSpanEnd = n;
        } 
        String[] arrayOfString = suggestionSpan.getSuggestions();
        int i1 = arrayOfString.length;
        byte b;
        label32: for (b = 0; b < i1; b++, bool = false) {
          String str = arrayOfString[b];
          int i2;
          for (i2 = 0; i2 < j; i2++) {
            Editor.SuggestionInfo suggestionInfo1 = param1ArrayOfSuggestionInfo[i2];
            if (suggestionInfo1.mText.toString().equals(str)) {
              int i3 = suggestionInfo1.mSuggestionSpanInfo.mSpanStart;
              int i4 = suggestionInfo1.mSuggestionSpanInfo.mSpanEnd;
              if (m == i3 && n == i4)
                continue label32; 
            } 
          } 
          Editor.SuggestionInfo suggestionInfo = param1ArrayOfSuggestionInfo[j];
          suggestionInfo.setSpanInfo(suggestionSpan, m, n);
          suggestionInfo.mSuggestionIndex = b;
          suggestionInfo.mSuggestionStart = 0;
          suggestionInfo.mSuggestionEnd = str.length();
          suggestionInfo.mText.replace(0, suggestionInfo.mText.length(), str);
          i2 = j + 1;
          j = i2;
          if (i2 >= param1ArrayOfSuggestionInfo.length)
            return i2; 
        } 
        i++;
      } 
      return j;
    }
  }
  
  class SuggestionsPopupWindow extends PinnedPopupWindow implements AdapterView.OnItemClickListener {
    private static final int MAX_NUMBER_SUGGESTIONS = 5;
    
    private static final String USER_DICTIONARY_EXTRA_LOCALE = "locale";
    
    private static final String USER_DICTIONARY_EXTRA_WORD = "word";
    
    private TextView mAddToDictionaryButton;
    
    private int mContainerMarginTop;
    
    private int mContainerMarginWidth;
    
    private LinearLayout mContainerView;
    
    private Context mContext;
    
    private boolean mCursorWasVisibleBeforeSuggestions;
    
    private TextView mDeleteButton;
    
    private TextAppearanceSpan mHighlightSpan;
    
    private boolean mIsShowingUp = false;
    
    private final Editor.SuggestionSpanInfo mMisspelledSpanInfo = new Editor.SuggestionSpanInfo();
    
    private int mNumberOfSuggestions;
    
    private Editor.SuggestionInfo[] mSuggestionInfos;
    
    private ListView mSuggestionListView;
    
    private SuggestionAdapter mSuggestionsAdapter;
    
    final Editor this$0;
    
    class CustomPopupWindow extends PopupWindow {
      final Editor.SuggestionsPopupWindow this$1;
      
      private CustomPopupWindow() {}
      
      public void dismiss() {
        if (!isShowing())
          return; 
        super.dismiss();
        Editor.this.getPositionListener().removeSubscriber(Editor.SuggestionsPopupWindow.this);
        ((Spannable)Editor.this.mTextView.getText()).removeSpan(Editor.this.mSuggestionRangeSpan);
        Editor.this.mTextView.setCursorVisible(Editor.SuggestionsPopupWindow.this.mCursorWasVisibleBeforeSuggestions);
        if (Editor.this.hasInsertionController() && !Editor.this.extractedTextModeWillBeStarted())
          Editor.this.getInsertionController().show(); 
      }
    }
    
    public SuggestionsPopupWindow() {
      this.mCursorWasVisibleBeforeSuggestions = Editor.this.mCursorVisible;
    }
    
    protected void setUp() {
      Context context = applyDefaultTheme(Editor.this.mTextView.getContext());
      Editor editor = Editor.this;
      this.mHighlightSpan = new TextAppearanceSpan(context, editor.mTextView.mTextEditSuggestionHighlightStyle);
    }
    
    private Context applyDefaultTheme(Context param1Context) {
      int i;
      TypedArray typedArray = param1Context.obtainStyledAttributes(new int[] { 16844176 });
      boolean bool = typedArray.getBoolean(0, true);
      if (bool) {
        i = 16974410;
      } else {
        i = 16974411;
      } 
      typedArray.recycle();
      return (Context)new ContextThemeWrapper(param1Context, i);
    }
    
    protected void createPopupWindow() {
      this.mPopupWindow = new CustomPopupWindow();
      this.mPopupWindow.setInputMethodMode(2);
      this.mPopupWindow.setBackgroundDrawable((Drawable)new ColorDrawable(0));
      this.mPopupWindow.setFocusable(true);
      this.mPopupWindow.setClippingEnabled(false);
    }
    
    protected void initContentView() {
      LayoutInflater layoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
      Editor editor = Editor.this;
      int i = editor.mTextView.mTextEditSuggestionContainerLayout;
      this.mContentView = (ViewGroup)layoutInflater.inflate(i, (ViewGroup)null);
      LinearLayout linearLayout = this.mContentView.<LinearLayout>findViewById(16909492);
      ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)linearLayout.getLayoutParams();
      this.mContainerMarginWidth = marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
      this.mContainerMarginTop = marginLayoutParams.topMargin;
      this.mClippingLimitLeft = marginLayoutParams.leftMargin;
      this.mClippingLimitRight = marginLayoutParams.rightMargin;
      this.mSuggestionListView = this.mContentView.<ListView>findViewById(16909491);
      SuggestionAdapter suggestionAdapter = new SuggestionAdapter();
      this.mSuggestionListView.setAdapter(suggestionAdapter);
      this.mSuggestionListView.setOnItemClickListener(this);
      this.mSuggestionListView.setBackground((Drawable)new ColorDrawable(0));
      this.mSuggestionInfos = new Editor.SuggestionInfo[5];
      i = 0;
      while (true) {
        Editor.SuggestionInfo[] arrayOfSuggestionInfo = this.mSuggestionInfos;
        if (i < arrayOfSuggestionInfo.length) {
          arrayOfSuggestionInfo[i] = new Editor.SuggestionInfo();
          i++;
          continue;
        } 
        break;
      } 
      TextView textView = this.mContentView.<TextView>findViewById(16908726);
      textView.setOnClickListener((View.OnClickListener)new Object(this));
      this.mDeleteButton = textView = this.mContentView.<TextView>findViewById(16908923);
      textView.setOnClickListener((View.OnClickListener)new Object(this));
    }
    
    public boolean isShowingUp() {
      return this.mIsShowingUp;
    }
    
    public void onParentLostFocus() {
      this.mIsShowingUp = false;
    }
    
    class SuggestionAdapter extends BaseAdapter {
      private LayoutInflater mInflater;
      
      final Editor.SuggestionsPopupWindow this$1;
      
      private SuggestionAdapter() {
        this.mInflater = (LayoutInflater)Editor.SuggestionsPopupWindow.this.mContext.getSystemService("layout_inflater");
      }
      
      public int getCount() {
        return Editor.SuggestionsPopupWindow.this.mNumberOfSuggestions;
      }
      
      public Object getItem(int param2Int) {
        return Editor.SuggestionsPopupWindow.this.mSuggestionInfos[param2Int];
      }
      
      public long getItemId(int param2Int) {
        return param2Int;
      }
      
      public View getView(int param2Int, View param2View, ViewGroup param2ViewGroup) {
        TextView textView = (TextView)param2View;
        param2View = textView;
        if (textView == null)
          param2View = this.mInflater.inflate(Editor.this.mTextView.mTextEditSuggestionItemLayout, param2ViewGroup, false); 
        Editor.SuggestionInfo suggestionInfo = Editor.SuggestionsPopupWindow.this.mSuggestionInfos[param2Int];
        param2View.setText(suggestionInfo.mText);
        return param2View;
      }
    }
    
    public void show() {
      if (!(Editor.this.mTextView.getText() instanceof Editable))
        return; 
      if (Editor.this.extractedTextModeWillBeStarted())
        return; 
      boolean bool = updateSuggestions();
      byte b = 0;
      if (bool) {
        this.mCursorWasVisibleBeforeSuggestions = Editor.this.mCursorVisible;
        Editor.this.mTextView.setCursorVisible(false);
        this.mIsShowingUp = true;
        super.show();
      } 
      ListView listView = this.mSuggestionListView;
      if (this.mNumberOfSuggestions == 0)
        b = 8; 
      listView.setVisibility(b);
    }
    
    protected void measureContent() {
      DisplayMetrics displayMetrics = Editor.this.mTextView.getResources().getDisplayMetrics();
      int i = View.MeasureSpec.makeMeasureSpec(displayMetrics.widthPixels, -2147483648);
      int j = View.MeasureSpec.makeMeasureSpec(displayMetrics.heightPixels, -2147483648);
      int k = 0;
      displayMetrics = null;
      int m;
      for (m = 0; m < this.mNumberOfSuggestions; m++) {
        View view = this.mSuggestionsAdapter.getView(m, (View)displayMetrics, this.mContentView);
        (view.getLayoutParams()).width = -2;
        view.measure(i, j);
        k = Math.max(k, view.getMeasuredWidth());
      } 
      m = k;
      if (this.mAddToDictionaryButton.getVisibility() != 8) {
        this.mAddToDictionaryButton.measure(i, j);
        m = Math.max(k, this.mAddToDictionaryButton.getMeasuredWidth());
      } 
      this.mDeleteButton.measure(i, j);
      k = Math.max(m, this.mDeleteButton.getMeasuredWidth());
      m = k + this.mContainerView.getPaddingLeft() + this.mContainerView.getPaddingRight() + this.mContainerMarginWidth;
      ViewGroup viewGroup = this.mContentView;
      k = View.MeasureSpec.makeMeasureSpec(m, 1073741824);
      viewGroup.measure(k, j);
      Drawable drawable = this.mPopupWindow.getBackground();
      k = m;
      if (drawable != null) {
        if (Editor.this.mTempRect == null)
          Editor.access$3702(Editor.this, new Rect()); 
        drawable.getPadding(Editor.this.mTempRect);
        k = m + Editor.this.mTempRect.left + Editor.this.mTempRect.right;
      } 
      this.mPopupWindow.setWidth(k);
    }
    
    protected int getTextOffset() {
      return (Editor.this.mTextView.getSelectionStart() + Editor.this.mTextView.getSelectionStart()) / 2;
    }
    
    protected int getVerticalLocalPosition(int param1Int) {
      Layout layout = Editor.this.mTextView.getLayout();
      return layout.getLineBottomWithoutSpacing(param1Int) - this.mContainerMarginTop;
    }
    
    protected int clipVertically(int param1Int) {
      int i = this.mContentView.getMeasuredHeight();
      DisplayMetrics displayMetrics = Editor.this.mTextView.getResources().getDisplayMetrics();
      return Math.min(param1Int, displayMetrics.heightPixels - i);
    }
    
    private void hideWithCleanUp() {
      for (Editor.SuggestionInfo suggestionInfo : this.mSuggestionInfos)
        suggestionInfo.clear(); 
      this.mMisspelledSpanInfo.clear();
      hide();
    }
    
    private boolean updateSuggestions() {
      Spannable spannable = (Spannable)Editor.this.mTextView.getText();
      Editor editor = Editor.this;
      int i = editor.mSuggestionHelper.getSuggestionInfo(this.mSuggestionInfos, this.mMisspelledSpanInfo);
      if (i == 0 && this.mMisspelledSpanInfo.mSuggestionSpan == null)
        return false; 
      int j = Editor.this.mTextView.getText().length();
      i = 0;
      int k;
      for (k = 0; k < this.mNumberOfSuggestions; k++) {
        Editor.SuggestionSpanInfo suggestionSpanInfo = (this.mSuggestionInfos[k]).mSuggestionSpanInfo;
        j = Math.min(j, suggestionSpanInfo.mSpanStart);
        i = Math.max(i, suggestionSpanInfo.mSpanEnd);
      } 
      int m = j;
      k = i;
      if (this.mMisspelledSpanInfo.mSuggestionSpan != null) {
        m = Math.min(j, this.mMisspelledSpanInfo.mSpanStart);
        k = Math.max(i, this.mMisspelledSpanInfo.mSpanEnd);
      } 
      for (i = 0; i < this.mNumberOfSuggestions; i++)
        highlightTextDifferences(this.mSuggestionInfos[i], m, k); 
      j = 8;
      i = j;
      if (this.mMisspelledSpanInfo.mSuggestionSpan != null) {
        i = j;
        if (this.mMisspelledSpanInfo.mSpanStart >= 0) {
          i = j;
          if (this.mMisspelledSpanInfo.mSpanEnd > this.mMisspelledSpanInfo.mSpanStart)
            i = 0; 
        } 
      } 
      this.mAddToDictionaryButton.setVisibility(i);
      if (Editor.this.mSuggestionRangeSpan == null)
        Editor.this.mSuggestionRangeSpan = new SuggestionRangeSpan(); 
      if (this.mNumberOfSuggestions != 0) {
        SuggestionSpan suggestionSpan = (this.mSuggestionInfos[0]).mSuggestionSpanInfo.mSuggestionSpan;
        i = suggestionSpan.getUnderlineColor();
      } else {
        i = this.mMisspelledSpanInfo.mSuggestionSpan.getUnderlineColor();
      } 
      if (i == 0) {
        Editor.this.mSuggestionRangeSpan.setBackgroundColor(Editor.this.mTextView.mHighlightColor);
      } else {
        j = (int)(Color.alpha(i) * 0.4F);
        Editor.this.mSuggestionRangeSpan.setBackgroundColor((0xFFFFFF & i) + (j << 24));
      } 
      spannable.setSpan(Editor.this.mSuggestionRangeSpan, m, k, 33);
      this.mSuggestionsAdapter.notifyDataSetChanged();
      return true;
    }
    
    private void highlightTextDifferences(Editor.SuggestionInfo param1SuggestionInfo, int param1Int1, int param1Int2) {
      Spannable spannable = (Spannable)Editor.this.mTextView.getText();
      int i = param1SuggestionInfo.mSuggestionSpanInfo.mSpanStart;
      int j = param1SuggestionInfo.mSuggestionSpanInfo.mSpanEnd;
      param1SuggestionInfo.mSuggestionStart = i - param1Int1;
      int k = param1SuggestionInfo.mSuggestionStart;
      SpannableStringBuilder spannableStringBuilder = param1SuggestionInfo.mText;
      param1SuggestionInfo.mSuggestionEnd = k + spannableStringBuilder.length();
      param1SuggestionInfo.mText.setSpan(this.mHighlightSpan, 0, param1SuggestionInfo.mText.length(), 33);
      String str = spannable.toString();
      param1SuggestionInfo.mText.insert(0, str.substring(param1Int1, i));
      param1SuggestionInfo.mText.append(str.substring(j, param1Int2));
    }
    
    public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      Editor.SuggestionInfo suggestionInfo = this.mSuggestionInfos[param1Int];
      Editor.this.replaceWithSuggestion(suggestionInfo);
      hideWithCleanUp();
    }
  }
  
  class TextActionModeCallback extends ActionMode.Callback2 {
    private final Map<MenuItem, View.OnClickListener> mAssistClickHandlers;
    
    private final int mHandleHeight;
    
    private final boolean mHasSelection;
    
    private final RectF mSelectionBounds;
    
    private final Path mSelectionPath;
    
    final Editor this$0;
    
    TextActionModeCallback(int param1Int) {
      Drawable drawable;
      boolean bool;
      this.mSelectionPath = new Path();
      this.mSelectionBounds = new RectF();
      this.mAssistClickHandlers = new HashMap<>();
      if (param1Int == 0 || (Editor.this.mTextIsSelectable && param1Int == 2)) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mHasSelection = bool;
      if (bool) {
        Editor.SelectionModifierCursorController selectionModifierCursorController = Editor.this.getSelectionController();
        if (selectionModifierCursorController.mStartHandle == null) {
          Editor.this.loadHandleDrawables(false);
          selectionModifierCursorController.initHandles();
          selectionModifierCursorController.hide();
        } 
        Drawable drawable1 = Editor.this.mSelectHandleLeft;
        int i = drawable1.getMinimumHeight();
        drawable = Editor.this.mSelectHandleRight;
        param1Int = drawable.getMinimumHeight();
        this.mHandleHeight = Math.max(i, param1Int);
      } else {
        Editor.InsertionPointCursorController insertionPointCursorController = drawable.getInsertionController();
        if (insertionPointCursorController != null) {
          insertionPointCursorController.getHandle();
          this.mHandleHeight = ((Editor)drawable).mSelectHandleCenter.getMinimumHeight();
        } else {
          this.mHandleHeight = 0;
        } 
      } 
    }
    
    public boolean onCreateActionMode(ActionMode param1ActionMode, Menu param1Menu) {
      Spannable spannable;
      this.mAssistClickHandlers.clear();
      param1ActionMode.setTitle((CharSequence)null);
      param1ActionMode.setSubtitle((CharSequence)null);
      param1ActionMode.setTitleOptionalHint(true);
      populateMenuWithItems(param1Menu);
      ActionMode.Callback callback = getCustomCallback();
      if (callback != null && !callback.onCreateActionMode(param1ActionMode, param1Menu)) {
        spannable = (Spannable)Editor.this.mTextView.getText();
        Editor editor = Editor.this;
        int i = editor.mTextView.getSelectionEnd();
        Selection.setSelection(spannable, i);
        return false;
      } 
      if (Editor.this.mTextView.canProcessText())
        Editor.this.mProcessTextIntentActionsHandler.onInitializeMenu((Menu)spannable); 
      if (this.mHasSelection && !Editor.this.mTextView.hasTransientState())
        Editor.this.mTextView.setHasTransientState(true); 
      return true;
    }
    
    private ActionMode.Callback getCustomCallback() {
      ActionMode.Callback callback;
      if (this.mHasSelection) {
        callback = Editor.this.mCustomSelectionActionModeCallback;
      } else {
        callback = Editor.this.mCustomInsertionActionModeCallback;
      } 
      return callback;
    }
    
    private void populateMenuWithItems(Menu param1Menu) {
      if (Editor.this.mTextView.canCut()) {
        MenuItem menuItem = param1Menu.add(0, 16908320, 4, 17039363);
        menuItem = menuItem.setAlphabeticShortcut('x');
        menuItem.setShowAsAction(2);
      } 
      if (Editor.this.mTextView.canCopy()) {
        MenuItem menuItem = param1Menu.add(0, 16908321, 5, 17039361);
        menuItem = menuItem.setAlphabeticShortcut('c');
        menuItem.setShowAsAction(2);
      } 
      if (Editor.this.mTextView.canPaste()) {
        MenuItem menuItem = param1Menu.add(0, 16908322, 6, 17039371);
        menuItem = menuItem.setAlphabeticShortcut('v');
        menuItem.setShowAsAction(2);
      } 
      if (Editor.this.mTextView.canShare()) {
        MenuItem menuItem = param1Menu.add(0, 16908341, 7, 17041273);
        menuItem.setShowAsAction(1);
      } 
      if (Editor.this.mTextView.canRequestAutofill()) {
        String str = Editor.this.mTextView.getSelectedText();
        if (str == null || str.isEmpty()) {
          MenuItem menuItem = param1Menu.add(0, 16908355, 10, 17039386);
          menuItem.setShowAsAction(0);
        } 
      } 
      if (Editor.this.mTextView.canPasteAsPlainText()) {
        MenuItem menuItem = param1Menu.add(0, 16908337, 11, 17039385);
        menuItem.setShowAsAction(1);
      } 
      updateSelectAllItem(param1Menu);
      updateReplaceItem(param1Menu);
      updateAssistMenuItems(param1Menu);
    }
    
    public boolean onPrepareActionMode(ActionMode param1ActionMode, Menu param1Menu) {
      updateSelectAllItem(param1Menu);
      updateReplaceItem(param1Menu);
      updateAssistMenuItems(param1Menu);
      ActionMode.Callback callback = getCustomCallback();
      if (callback != null)
        return callback.onPrepareActionMode(param1ActionMode, param1Menu); 
      return true;
    }
    
    private void updateSelectAllItem(Menu param1Menu) {
      boolean bool1;
      boolean bool = Editor.this.mTextView.canSelectAllText();
      if (param1Menu.findItem(16908319) != null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (bool && !bool1) {
        MenuItem menuItem = param1Menu.add(0, 16908319, 8, 17039373);
        menuItem.setShowAsAction(1);
      } else if (!bool && bool1) {
        param1Menu.removeItem(16908319);
      } 
      ((IOplusFloatingToolbarUtil)OplusFeatureCache.getOrCreate(IOplusFloatingToolbarUtil.DEFAULT, new Object[0])).updateSelectAllItem(param1Menu, Editor.this.mTextView, 11);
    }
    
    private void updateReplaceItem(Menu param1Menu) {
      MenuItem menuItem;
      boolean bool1;
      boolean bool2;
      if (Editor.this.mTextView.isSuggestionsEnabled() && Editor.this.shouldOfferToShowSuggestions()) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (param1Menu.findItem(16908340) != null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool1 && !bool2) {
        menuItem = param1Menu.add(0, 16908340, 9, 17041169);
        menuItem.setShowAsAction(1);
      } else if (!bool1 && bool2) {
        menuItem.removeItem(16908340);
      } 
    }
    
    private void updateAssistMenuItems(Menu param1Menu) {
      clearAssistMenuItems(param1Menu);
      if (!shouldEnableAssistMenuItems())
        return; 
      Editor editor = Editor.this;
      TextClassification textClassification = editor.getSelectionActionModeHelper().getTextClassification();
      if (textClassification == null)
        return; 
      if (!textClassification.getActions().isEmpty()) {
        RemoteAction remoteAction = textClassification.getActions().get(0);
        MenuItem menuItem = addAssistMenuItem(param1Menu, remoteAction, 16908353, 0, 2);
        menuItem.setIntent(textClassification.getIntent());
      } else if (hasLegacyAssistItem(textClassification)) {
        CharSequence charSequence = textClassification.getLabel();
        MenuItem menuItem1 = param1Menu.add(16908353, 16908353, 0, charSequence);
        menuItem1 = menuItem1.setIcon(textClassification.getIcon());
        MenuItem menuItem2 = menuItem1.setIntent(textClassification.getIntent());
        menuItem2.setShowAsAction(2);
        Map<MenuItem, View.OnClickListener> map = this.mAssistClickHandlers;
        Editor editor1 = Editor.this;
        Context context = editor1.mTextView.getContext();
        Intent intent = textClassification.getIntent();
        int j = createAssistMenuItemPendingIntentRequestCode();
        PendingIntent pendingIntent = TextClassification.createPendingIntent(context, intent, j);
        map.put(menuItem2, TextClassification.createIntentOnClickListener(pendingIntent));
      } 
      int i = textClassification.getActions().size();
      for (byte b = 1; b < i; b++)
        addAssistMenuItem(param1Menu, textClassification.getActions().get(b), 0, b + 50 - 1, 0); 
    }
    
    private MenuItem addAssistMenuItem(Menu param1Menu, RemoteAction param1RemoteAction, int param1Int1, int param1Int2, int param1Int3) {
      MenuItem menuItem = param1Menu.add(16908353, param1Int1, param1Int2, param1RemoteAction.getTitle());
      menuItem = menuItem.setContentDescription(param1RemoteAction.getContentDescription());
      if (param1RemoteAction.shouldShowIcon())
        menuItem.setIcon(param1RemoteAction.getIcon().loadDrawable(Editor.this.mTextView.getContext())); 
      menuItem.setShowAsAction(param1Int3);
      Map<MenuItem, View.OnClickListener> map = this.mAssistClickHandlers;
      View.OnClickListener onClickListener = TextClassification.createIntentOnClickListener(param1RemoteAction.getActionIntent());
      map.put(menuItem, onClickListener);
      return menuItem;
    }
    
    private void clearAssistMenuItems(Menu param1Menu) {
      byte b = 0;
      while (b < param1Menu.size()) {
        MenuItem menuItem = param1Menu.getItem(b);
        if (menuItem.getGroupId() == 16908353) {
          param1Menu.removeItem(menuItem.getItemId());
          continue;
        } 
        b++;
      } 
    }
    
    private boolean hasLegacyAssistItem(TextClassification param1TextClassification) {
      // Byte code:
      //   0: aload_1
      //   1: invokevirtual getIcon : ()Landroid/graphics/drawable/Drawable;
      //   4: ifnonnull -> 19
      //   7: aload_1
      //   8: invokevirtual getLabel : ()Ljava/lang/CharSequence;
      //   11: astore_2
      //   12: aload_2
      //   13: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
      //   16: ifne -> 36
      //   19: aload_1
      //   20: invokevirtual getIntent : ()Landroid/content/Intent;
      //   23: ifnonnull -> 41
      //   26: aload_1
      //   27: invokevirtual getOnClickListener : ()Landroid/view/View$OnClickListener;
      //   30: ifnull -> 36
      //   33: goto -> 41
      //   36: iconst_0
      //   37: istore_3
      //   38: goto -> 43
      //   41: iconst_1
      //   42: istore_3
      //   43: iload_3
      //   44: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #4365	-> 0
      //   #4366	-> 7
      //   #4365	-> 12
      //   #4366	-> 19
      //   #4367	-> 26
      //   #4365	-> 43
    }
    
    private boolean onAssistMenuItemClicked(MenuItem param1MenuItem) {
      boolean bool;
      if (param1MenuItem.getGroupId() == 16908353) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      Editor editor = Editor.this;
      TextClassification textClassification = editor.getSelectionActionModeHelper().getTextClassification();
      if (!shouldEnableAssistMenuItems() || textClassification == null)
        return true; 
      View.OnClickListener onClickListener2 = this.mAssistClickHandlers.get(param1MenuItem);
      View.OnClickListener onClickListener1 = onClickListener2;
      if (onClickListener2 == null) {
        Intent intent = param1MenuItem.getIntent();
        onClickListener1 = onClickListener2;
        if (intent != null) {
          Editor editor1 = Editor.this;
          Context context = editor1.mTextView.getContext();
          int i = createAssistMenuItemPendingIntentRequestCode();
          PendingIntent pendingIntent = TextClassification.createPendingIntent(context, intent, i);
          onClickListener1 = TextClassification.createIntentOnClickListener(pendingIntent);
        } 
      } 
      if (onClickListener1 != null) {
        onClickListener1.onClick(Editor.this.mTextView);
        Editor.this.stopTextActionMode();
      } 
      return true;
    }
    
    private int createAssistMenuItemPendingIntentRequestCode() {
      boolean bool;
      if (Editor.this.mTextView.hasSelection()) {
        Editor editor = Editor.this;
        CharSequence charSequence2 = editor.mTextView.getText();
        editor = Editor.this;
        bool = editor.mTextView.getSelectionStart();
        int i = Editor.this.mTextView.getSelectionEnd();
        CharSequence charSequence1 = charSequence2.subSequence(bool, i);
        bool = charSequence1.hashCode();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean shouldEnableAssistMenuItems() {
      if (Editor.this.mTextView.isDeviceProvisioned()) {
        Editor editor = Editor.this;
        TextClassificationConstants textClassificationConstants = TextClassificationManager.getSettings(editor.mTextView.getContext());
        if (textClassificationConstants.isSmartTextShareEnabled())
          return true; 
      } 
      return false;
    }
    
    public boolean onActionItemClicked(ActionMode param1ActionMode, MenuItem param1MenuItem) {
      SelectionActionModeHelper selectionActionModeHelper = Editor.this.getSelectionActionModeHelper();
      selectionActionModeHelper.onSelectionAction(param1MenuItem.getItemId(), param1MenuItem.getTitle().toString());
      if (Editor.this.mProcessTextIntentActionsHandler.performMenuItemAction(param1MenuItem))
        return true; 
      ActionMode.Callback callback = getCustomCallback();
      if (callback != null && callback.onActionItemClicked(param1ActionMode, param1MenuItem))
        return true; 
      if (param1MenuItem.getGroupId() == 16908353 && onAssistMenuItemClicked(param1MenuItem))
        return true; 
      if (((IOplusFloatingToolbarUtil)OplusFeatureCache.getOrCreate(IOplusFloatingToolbarUtil.DEFAULT, new Object[0])).needHook())
        handleItemClicked(param1MenuItem.getItemId()); 
      return Editor.this.mTextView.onTextContextMenuItem(param1MenuItem.getItemId());
    }
    
    public void onDestroyActionMode(ActionMode param1ActionMode) {
      Editor.this.getSelectionActionModeHelper().onDestroyActionMode();
      Editor.access$502(Editor.this, null);
      ActionMode.Callback callback = getCustomCallback();
      if (callback != null)
        callback.onDestroyActionMode(param1ActionMode); 
      if (!Editor.this.mPreserveSelection) {
        Spannable spannable = (Spannable)Editor.this.mTextView.getText();
        Editor editor = Editor.this;
        int i = editor.mTextView.getSelectionEnd();
        Selection.setSelection(spannable, i);
      } 
      if (Editor.this.mSelectionModifierCursorController != null)
        Editor.this.mSelectionModifierCursorController.hide(); 
      this.mAssistClickHandlers.clear();
      Editor.access$4402(Editor.this, false);
    }
    
    public void onGetContentRect(ActionMode param1ActionMode, View param1View, Rect param1Rect) {
      if (!param1View.equals(Editor.this.mTextView) || Editor.this.mTextView.getLayout() == null) {
        super.onGetContentRect(param1ActionMode, param1View, param1Rect);
        return;
      } 
      if (Editor.this.mTextView.getSelectionStart() != Editor.this.mTextView.getSelectionEnd()) {
        this.mSelectionPath.reset();
        Layout layout = Editor.this.mTextView.getLayout();
        Editor editor = Editor.this;
        int n = editor.mTextView.getSelectionStart(), i1 = Editor.this.mTextView.getSelectionEnd();
        Path path = this.mSelectionPath;
        layout.getSelectionPath(n, i1, path);
        this.mSelectionPath.computeBounds(this.mSelectionBounds, true);
        RectF rectF = this.mSelectionBounds;
        rectF.bottom += this.mHandleHeight;
      } else {
        Layout layout = Editor.this.mTextView.getLayout();
        int n = layout.getLineForOffset(Editor.this.mTextView.getSelectionStart());
        Editor editor = Editor.this;
        float f1 = layout.getPrimaryHorizontal(editor.mTextView.getSelectionStart());
        float f2 = editor.clampHorizontalPosition(null, f1);
        RectF rectF = this.mSelectionBounds;
        float f3 = layout.getLineTop(n);
        f1 = (layout.getLineBottom(n) + this.mHandleHeight);
        rectF.set(f2, f3, f2, f1);
      } 
      int k = Editor.this.mTextView.viewportToContentHorizontalOffset();
      int m = Editor.this.mTextView.viewportToContentVerticalOffset();
      double d = (this.mSelectionBounds.left + k);
      int i = (int)Math.floor(d);
      d = (this.mSelectionBounds.top + m);
      int j = (int)Math.floor(d);
      d = (this.mSelectionBounds.right + k);
      k = (int)Math.ceil(d);
      d = (this.mSelectionBounds.bottom + m);
      m = (int)Math.ceil(d);
      param1Rect.set(i, j, k, m);
    }
    
    private void handleItemClicked(int param1Int) {
      Context context = Editor.this.mTextView.getContext();
      switch (param1Int) {
        default:
          return;
        case 201457759:
          param1Int = Editor.this.mTextView.getSelectionStart();
          Editor.this.getSelectionController().setMinMaxOffset(param1Int);
          Editor.this.selectCurrentWord();
        case 16908321:
          str = context.getString(201589024);
          toast = Toast.makeText(context, str, 0);
          toast.show();
        case 16908320:
          break;
      } 
      String str = toast.getString(201589025);
      Toast toast = Toast.makeText((Context)toast, str, 0);
      toast.show();
    }
  }
  
  class CursorAnchorInfoNotifier implements TextViewPositionListener {
    private CursorAnchorInfoNotifier() {}
    
    final CursorAnchorInfo.Builder mSelectionInfoBuilder = new CursorAnchorInfo.Builder();
    
    final int[] mTmpIntOffset = new int[2];
    
    final Matrix mViewToScreenMatrix = new Matrix();
    
    final Editor this$0;
    
    public void updatePosition(int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2) {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/widget/Editor;
      //   4: getfield mInputMethodState : Landroid/widget/Editor$InputMethodState;
      //   7: astore #5
      //   9: aload #5
      //   11: ifnull -> 535
      //   14: aload #5
      //   16: getfield mBatchEditNesting : I
      //   19: ifle -> 25
      //   22: goto -> 535
      //   25: aload_0
      //   26: getfield this$0 : Landroid/widget/Editor;
      //   29: invokestatic access$4600 : (Landroid/widget/Editor;)Landroid/view/inputmethod/InputMethodManager;
      //   32: astore #6
      //   34: aload #6
      //   36: ifnonnull -> 40
      //   39: return
      //   40: aload #6
      //   42: aload_0
      //   43: getfield this$0 : Landroid/widget/Editor;
      //   46: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   49: invokevirtual isActive : (Landroid/view/View;)Z
      //   52: ifne -> 56
      //   55: return
      //   56: aload #6
      //   58: invokevirtual isCursorAnchorInfoEnabled : ()Z
      //   61: ifne -> 65
      //   64: return
      //   65: aload_0
      //   66: getfield this$0 : Landroid/widget/Editor;
      //   69: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   72: invokevirtual getLayout : ()Landroid/text/Layout;
      //   75: astore #5
      //   77: aload #5
      //   79: ifnonnull -> 83
      //   82: return
      //   83: aload_0
      //   84: getfield mSelectionInfoBuilder : Landroid/view/inputmethod/CursorAnchorInfo$Builder;
      //   87: astore #7
      //   89: aload #7
      //   91: invokevirtual reset : ()V
      //   94: aload_0
      //   95: getfield this$0 : Landroid/widget/Editor;
      //   98: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   101: invokevirtual getSelectionStart : ()I
      //   104: istore #8
      //   106: aload #7
      //   108: iload #8
      //   110: aload_0
      //   111: getfield this$0 : Landroid/widget/Editor;
      //   114: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   117: invokevirtual getSelectionEnd : ()I
      //   120: invokevirtual setSelectionRange : (II)Landroid/view/inputmethod/CursorAnchorInfo$Builder;
      //   123: pop
      //   124: aload_0
      //   125: getfield mViewToScreenMatrix : Landroid/graphics/Matrix;
      //   128: aload_0
      //   129: getfield this$0 : Landroid/widget/Editor;
      //   132: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   135: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
      //   138: invokevirtual set : (Landroid/graphics/Matrix;)V
      //   141: aload_0
      //   142: getfield this$0 : Landroid/widget/Editor;
      //   145: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   148: aload_0
      //   149: getfield mTmpIntOffset : [I
      //   152: invokevirtual getLocationOnScreen : ([I)V
      //   155: aload_0
      //   156: getfield mViewToScreenMatrix : Landroid/graphics/Matrix;
      //   159: astore #9
      //   161: aload_0
      //   162: getfield mTmpIntOffset : [I
      //   165: astore #10
      //   167: iconst_0
      //   168: istore #11
      //   170: aload #9
      //   172: aload #10
      //   174: iconst_0
      //   175: iaload
      //   176: i2f
      //   177: aload #10
      //   179: iconst_1
      //   180: iaload
      //   181: i2f
      //   182: invokevirtual postTranslate : (FF)Z
      //   185: pop
      //   186: aload #7
      //   188: aload_0
      //   189: getfield mViewToScreenMatrix : Landroid/graphics/Matrix;
      //   192: invokevirtual setMatrix : (Landroid/graphics/Matrix;)Landroid/view/inputmethod/CursorAnchorInfo$Builder;
      //   195: pop
      //   196: aload_0
      //   197: getfield this$0 : Landroid/widget/Editor;
      //   200: astore #10
      //   202: aload #10
      //   204: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   207: invokevirtual viewportToContentHorizontalOffset : ()I
      //   210: i2f
      //   211: fstore #12
      //   213: aload_0
      //   214: getfield this$0 : Landroid/widget/Editor;
      //   217: astore #10
      //   219: aload #10
      //   221: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   224: invokevirtual viewportToContentVerticalOffset : ()I
      //   227: i2f
      //   228: fstore #13
      //   230: aload_0
      //   231: getfield this$0 : Landroid/widget/Editor;
      //   234: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   237: invokevirtual getText : ()Ljava/lang/CharSequence;
      //   240: astore #9
      //   242: aload #9
      //   244: instanceof android/text/Spannable
      //   247: ifeq -> 353
      //   250: aload #9
      //   252: checkcast android/text/Spannable
      //   255: astore #10
      //   257: aload #10
      //   259: invokestatic getComposingSpanStart : (Landroid/text/Spannable;)I
      //   262: istore_1
      //   263: aload #10
      //   265: invokestatic getComposingSpanEnd : (Landroid/text/Spannable;)I
      //   268: istore #14
      //   270: iload #14
      //   272: iload_1
      //   273: if_icmpge -> 282
      //   276: iload #14
      //   278: istore_2
      //   279: goto -> 287
      //   282: iload_1
      //   283: istore_2
      //   284: iload #14
      //   286: istore_1
      //   287: iload #11
      //   289: istore #14
      //   291: iload_2
      //   292: iflt -> 307
      //   295: iload #11
      //   297: istore #14
      //   299: iload_2
      //   300: iload_1
      //   301: if_icmpge -> 307
      //   304: iconst_1
      //   305: istore #14
      //   307: iload #14
      //   309: ifeq -> 353
      //   312: aload #9
      //   314: iload_2
      //   315: iload_1
      //   316: invokeinterface subSequence : (II)Ljava/lang/CharSequence;
      //   321: astore #10
      //   323: aload #7
      //   325: iload_2
      //   326: aload #10
      //   328: invokevirtual setComposingText : (ILjava/lang/CharSequence;)Landroid/view/inputmethod/CursorAnchorInfo$Builder;
      //   331: pop
      //   332: aload_0
      //   333: getfield this$0 : Landroid/widget/Editor;
      //   336: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   339: aload #7
      //   341: iload_2
      //   342: iload_1
      //   343: fload #12
      //   345: fload #13
      //   347: invokevirtual populateCharacterBounds : (Landroid/view/inputmethod/CursorAnchorInfo$Builder;IIFF)V
      //   350: goto -> 353
      //   353: iload #8
      //   355: iflt -> 517
      //   358: aload #5
      //   360: iload #8
      //   362: invokevirtual getLineForOffset : (I)I
      //   365: istore_1
      //   366: aload #5
      //   368: iload #8
      //   370: invokevirtual getPrimaryHorizontal : (I)F
      //   373: fload #12
      //   375: fadd
      //   376: fstore #15
      //   378: aload #5
      //   380: iload_1
      //   381: invokevirtual getLineTop : (I)I
      //   384: i2f
      //   385: fload #13
      //   387: fadd
      //   388: fstore #16
      //   390: aload #5
      //   392: iload_1
      //   393: invokevirtual getLineBaseline : (I)I
      //   396: i2f
      //   397: fstore #17
      //   399: aload #5
      //   401: iload_1
      //   402: invokevirtual getLineBottomWithoutSpacing : (I)I
      //   405: i2f
      //   406: fload #13
      //   408: fadd
      //   409: fstore #12
      //   411: aload_0
      //   412: getfield this$0 : Landroid/widget/Editor;
      //   415: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   418: astore #10
      //   420: aload #10
      //   422: fload #15
      //   424: fload #16
      //   426: invokevirtual isPositionVisible : (FF)Z
      //   429: istore_3
      //   430: aload_0
      //   431: getfield this$0 : Landroid/widget/Editor;
      //   434: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   437: astore #10
      //   439: aload #10
      //   441: fload #15
      //   443: fload #12
      //   445: invokevirtual isPositionVisible : (FF)Z
      //   448: istore #4
      //   450: iconst_0
      //   451: istore_1
      //   452: iload_3
      //   453: ifne -> 461
      //   456: iload #4
      //   458: ifeq -> 465
      //   461: iconst_0
      //   462: iconst_1
      //   463: ior
      //   464: istore_1
      //   465: iload_3
      //   466: ifeq -> 476
      //   469: iload_1
      //   470: istore_2
      //   471: iload #4
      //   473: ifne -> 480
      //   476: iload_1
      //   477: iconst_2
      //   478: ior
      //   479: istore_2
      //   480: aload #5
      //   482: iload #8
      //   484: invokevirtual isRtlCharAt : (I)Z
      //   487: ifeq -> 497
      //   490: iload_2
      //   491: iconst_4
      //   492: ior
      //   493: istore_1
      //   494: goto -> 499
      //   497: iload_2
      //   498: istore_1
      //   499: aload #7
      //   501: fload #15
      //   503: fload #16
      //   505: fload #17
      //   507: fload #13
      //   509: fadd
      //   510: fload #12
      //   512: iload_1
      //   513: invokevirtual setInsertionMarkerLocation : (FFFFI)Landroid/view/inputmethod/CursorAnchorInfo$Builder;
      //   516: pop
      //   517: aload #6
      //   519: aload_0
      //   520: getfield this$0 : Landroid/widget/Editor;
      //   523: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   526: aload #7
      //   528: invokevirtual build : ()Landroid/view/inputmethod/CursorAnchorInfo;
      //   531: invokevirtual updateCursorAnchorInfo : (Landroid/view/View;Landroid/view/inputmethod/CursorAnchorInfo;)V
      //   534: return
      //   535: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #4536	-> 0
      //   #4537	-> 9
      //   #4540	-> 25
      //   #4541	-> 34
      //   #4542	-> 39
      //   #4544	-> 40
      //   #4545	-> 55
      //   #4548	-> 56
      //   #4549	-> 64
      //   #4551	-> 65
      //   #4552	-> 77
      //   #4553	-> 82
      //   #4556	-> 83
      //   #4557	-> 89
      //   #4559	-> 94
      //   #4560	-> 106
      //   #4563	-> 124
      //   #4564	-> 141
      //   #4565	-> 155
      //   #4566	-> 186
      //   #4568	-> 196
      //   #4569	-> 202
      //   #4570	-> 213
      //   #4571	-> 219
      //   #4573	-> 230
      //   #4574	-> 242
      //   #4575	-> 250
      //   #4576	-> 257
      //   #4577	-> 263
      //   #4578	-> 270
      //   #4579	-> 276
      //   #4580	-> 276
      //   #4581	-> 276
      //   #4578	-> 282
      //   #4583	-> 287
      //   #4585	-> 307
      //   #4586	-> 312
      //   #4588	-> 323
      //   #4589	-> 332
      //   #4585	-> 353
      //   #4596	-> 353
      //   #4597	-> 358
      //   #4598	-> 358
      //   #4599	-> 366
      //   #4601	-> 378
      //   #4603	-> 390
      //   #4605	-> 399
      //   #4607	-> 411
      //   #4608	-> 420
      //   #4609	-> 430
      //   #4610	-> 439
      //   #4611	-> 450
      //   #4612	-> 452
      //   #4613	-> 461
      //   #4615	-> 465
      //   #4616	-> 476
      //   #4618	-> 480
      //   #4619	-> 490
      //   #4618	-> 497
      //   #4621	-> 499
      //   #4625	-> 517
      //   #4626	-> 534
      //   #4538	-> 535
    }
  }
  
  class MagnifierMotionAnimator {
    private static final long DURATION = 100L;
    
    private float mAnimationCurrentX;
    
    private float mAnimationCurrentY;
    
    private float mAnimationStartX;
    
    private float mAnimationStartY;
    
    private final ValueAnimator mAnimator;
    
    private float mLastX;
    
    private float mLastY;
    
    private final Magnifier mMagnifier;
    
    private boolean mMagnifierIsShowing;
    
    private MagnifierMotionAnimator(Editor this$0) {
      this.mMagnifier = (Magnifier)this$0;
      ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
      valueAnimator.setDuration(100L);
      this.mAnimator.setInterpolator(new LinearInterpolator());
      this.mAnimator.addUpdateListener(new _$$Lambda$Editor$MagnifierMotionAnimator$E_RaelOMgCHAzvKgSSZE_hDYeIg(this));
    }
    
    private void show(float param1Float1, float param1Float2) {
      boolean bool;
      if (this.mMagnifierIsShowing && param1Float2 != this.mLastY) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        if (this.mAnimator.isRunning()) {
          this.mAnimator.cancel();
          this.mAnimationStartX = this.mAnimationCurrentX;
          this.mAnimationStartY = this.mAnimationCurrentY;
        } else {
          this.mAnimationStartX = this.mLastX;
          this.mAnimationStartY = this.mLastY;
        } 
        this.mAnimator.start();
      } else if (!this.mAnimator.isRunning()) {
        this.mMagnifier.show(param1Float1, param1Float2);
      } 
      this.mLastX = param1Float1;
      this.mLastY = param1Float2;
      this.mMagnifierIsShowing = true;
    }
    
    private void update() {
      this.mMagnifier.update();
    }
    
    private void dismiss() {
      this.mMagnifier.dismiss();
      this.mAnimator.cancel();
      this.mMagnifierIsShowing = false;
    }
  }
  
  class HandleView extends View implements TextViewPositionListener {
    protected int mPreviousOffset = -1;
    
    private boolean mPositionHasChanged = true;
    
    protected int mPrevLine = -1;
    
    protected int mPreviousLineTouched = -1;
    
    private float mCurrentDragInitialTouchRawX = -1.0F;
    
    private static final int HISTORY_SIZE = 5;
    
    private static final int TOUCH_UP_FILTER_DELAY_AFTER = 150;
    
    private static final int TOUCH_UP_FILTER_DELAY_BEFORE = 350;
    
    private final PopupWindow mContainer;
    
    protected Drawable mDrawable;
    
    protected Drawable mDrawableLtr;
    
    protected Drawable mDrawableRtl;
    
    protected int mHorizontalGravity;
    
    protected int mHotspotX;
    
    private final int mIdealFingerToCursorOffset;
    
    private final float mIdealVerticalOffset;
    
    private boolean mIsDragging;
    
    private int mLastParentX;
    
    private int mLastParentXOnScreen;
    
    private int mLastParentY;
    
    private int mLastParentYOnScreen;
    
    private int mMinSize;
    
    private int mNumberPreviousOffsets;
    
    private int mPositionX;
    
    private int mPositionY;
    
    private int mPreviousOffsetIndex;
    
    private final int[] mPreviousOffsets;
    
    private final long[] mPreviousOffsetsTimes;
    
    private float mTextViewScaleX;
    
    private float mTextViewScaleY;
    
    private float mTouchOffsetY;
    
    private float mTouchToWindowOffsetX;
    
    private float mTouchToWindowOffsetY;
    
    final Editor this$0;
    
    private HandleView(Drawable param1Drawable1, Drawable param1Drawable2, int param1Int) {
      super(Editor.this.mTextView.getContext());
      this.mPreviousOffsetsTimes = new long[5];
      this.mPreviousOffsets = new int[5];
      this.mPreviousOffsetIndex = 0;
      this.mNumberPreviousOffsets = 0;
      setId(param1Int);
      PopupWindow popupWindow = new PopupWindow(Editor.this.mTextView.getContext(), null, 16843464);
      popupWindow.setSplitTouchEnabled(true);
      this.mContainer.setClippingEnabled(false);
      this.mContainer.setWindowLayoutType(1002);
      this.mContainer.setWidth(-2);
      this.mContainer.setHeight(-2);
      this.mContainer.setContentView(this);
      setDrawables(param1Drawable1, param1Drawable2);
      this.mMinSize = Editor.this.mTextView.getContext().getResources().getDimensionPixelSize(17105493);
      int i = getPreferredHeight();
      this.mTouchOffsetY = i * -0.3F;
      param1Int = AppGlobals.getIntCoreSetting("widget__finger_to_cursor_distance", -1);
      if (param1Int < 0 || param1Int > 100) {
        float f1 = i * 0.7F;
        this.mIdealFingerToCursorOffset = (int)(f1 - this.mTouchOffsetY);
        return;
      } 
      float f = param1Int;
      DisplayMetrics displayMetrics = Editor.this.mTextView.getContext().getResources().getDisplayMetrics();
      this.mIdealFingerToCursorOffset = param1Int = (int)TypedValue.applyDimension(1, f, displayMetrics);
      this.mIdealVerticalOffset = param1Int + this.mTouchOffsetY;
    }
    
    public float getIdealVerticalOffset() {
      return this.mIdealVerticalOffset;
    }
    
    final int getIdealFingerToCursorOffset() {
      return this.mIdealFingerToCursorOffset;
    }
    
    void setDrawables(Drawable param1Drawable1, Drawable param1Drawable2) {
      this.mDrawableLtr = param1Drawable1;
      this.mDrawableRtl = param1Drawable2;
      updateDrawable(true);
    }
    
    protected void updateDrawable(boolean param1Boolean) {
      Drawable drawable2;
      if (!param1Boolean && this.mIsDragging)
        return; 
      Layout layout = Editor.this.mTextView.getLayout();
      if (layout == null)
        return; 
      int i = getCurrentCursorOffset();
      param1Boolean = isAtRtlRun(layout, i);
      Drawable drawable1 = this.mDrawable;
      if (param1Boolean) {
        drawable2 = this.mDrawableRtl;
      } else {
        drawable2 = this.mDrawableLtr;
      } 
      this.mDrawable = drawable2;
      this.mHotspotX = getHotspotX(drawable2, param1Boolean);
      this.mHorizontalGravity = getHorizontalGravity(param1Boolean);
      if (drawable1 != this.mDrawable && isShowing()) {
        i = getCursorHorizontalPosition(layout, i);
        int j = this.mHotspotX;
        this.mPositionX = i = i - j - getHorizontalOffset() + getCursorOffset();
        this.mPositionX = i + Editor.this.mTextView.viewportToContentHorizontalOffset();
        this.mPositionHasChanged = true;
        updatePosition(this.mLastParentX, this.mLastParentY, false, false);
        postInvalidate();
      } 
    }
    
    private void startTouchUpFilter(int param1Int) {
      this.mNumberPreviousOffsets = 0;
      addPositionToTouchUpFilter(param1Int);
    }
    
    private void addPositionToTouchUpFilter(int param1Int) {
      int i = (this.mPreviousOffsetIndex + 1) % 5;
      this.mPreviousOffsets[i] = param1Int;
      this.mPreviousOffsetsTimes[i] = SystemClock.uptimeMillis();
      this.mNumberPreviousOffsets++;
    }
    
    private void filterOnTouchUp(boolean param1Boolean) {
      long l = SystemClock.uptimeMillis();
      byte b = 0;
      int i = this.mPreviousOffsetIndex;
      int j = Math.min(this.mNumberPreviousOffsets, 5);
      while (b < j && l - this.mPreviousOffsetsTimes[i] < 150L) {
        b++;
        i = (this.mPreviousOffsetIndex - b + 5) % 5;
      } 
      if (b > 0 && b < j && l - this.mPreviousOffsetsTimes[i] > 350L)
        positionAtCursorOffset(this.mPreviousOffsets[i], false, param1Boolean); 
    }
    
    public boolean offsetHasBeenChanged() {
      int i = this.mNumberPreviousOffsets;
      boolean bool = true;
      if (i <= 1)
        bool = false; 
      return bool;
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      setMeasuredDimension(getPreferredWidth(), getPreferredHeight());
    }
    
    public void invalidate() {
      super.invalidate();
      if (isShowing())
        positionAtCursorOffset(getCurrentCursorOffset(), true, false); 
    }
    
    protected final int getPreferredWidth() {
      return Math.max(this.mDrawable.getIntrinsicWidth(), this.mMinSize);
    }
    
    protected final int getPreferredHeight() {
      return Math.max(this.mDrawable.getIntrinsicHeight(), this.mMinSize);
    }
    
    public void show() {
      if (isShowing())
        return; 
      Editor.this.getPositionListener().addSubscriber(this, true);
      this.mPreviousOffset = -1;
      positionAtCursorOffset(getCurrentCursorOffset(), false, false);
    }
    
    protected void dismiss() {
      this.mIsDragging = false;
      this.mContainer.dismiss();
      onDetached();
    }
    
    public void hide() {
      dismiss();
      Editor.this.getPositionListener().removeSubscriber(this);
    }
    
    public boolean isShowing() {
      return this.mContainer.isShowing();
    }
    
    private boolean shouldShow() {
      if (this.mIsDragging)
        return true; 
      if (Editor.this.mTextView.isInBatchEditMode())
        return false; 
      TextView textView = Editor.this.mTextView;
      int i = this.mPositionX, j = this.mHotspotX;
      float f1 = (i + j + getHorizontalOffset()), f2 = this.mPositionY;
      return textView.isPositionVisible(f1, f2);
    }
    
    private void setVisible(boolean param1Boolean) {
      byte b;
      View view = this.mContainer.getContentView();
      if (param1Boolean) {
        b = 0;
      } else {
        b = 4;
      } 
      view.setVisibility(b);
    }
    
    protected boolean isAtRtlRun(Layout param1Layout, int param1Int) {
      return param1Layout.isRtlCharAt(param1Int);
    }
    
    public float getHorizontal(Layout param1Layout, int param1Int) {
      return param1Layout.getPrimaryHorizontal(param1Int);
    }
    
    protected int getOffsetAtCoordinate(Layout param1Layout, int param1Int, float param1Float) {
      return Editor.this.mTextView.getOffsetAtCoordinate(param1Int, param1Float);
    }
    
    protected void positionAtCursorOffset(int param1Int, boolean param1Boolean1, boolean param1Boolean2) {
      int i;
      Layout layout = Editor.this.mTextView.getLayout();
      if (layout == null) {
        Editor.this.prepareCursorControllers();
        return;
      } 
      layout = Editor.this.mTextView.getLayout();
      if (param1Int != this.mPreviousOffset) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i || param1Boolean1) {
        if (i) {
          updateSelection(param1Int);
          if (param1Boolean2 && Editor.this.mHapticTextHandleEnabled)
            Editor.this.mTextView.performHapticFeedback(9); 
          addPositionToTouchUpFilter(param1Int);
        } 
        int j = layout.getLineForOffset(param1Int);
        this.mPrevLine = j;
        i = getCursorHorizontalPosition(layout, param1Int);
        int k = this.mHotspotX;
        this.mPositionX = i - k - getHorizontalOffset() + getCursorOffset();
        this.mPositionY = layout.getLineBottomWithoutSpacing(j);
        this.mPositionX += Editor.this.mTextView.viewportToContentHorizontalOffset();
        this.mPositionY += Editor.this.mTextView.viewportToContentVerticalOffset();
        this.mPreviousOffset = param1Int;
        this.mPositionHasChanged = true;
      } 
    }
    
    int getCursorHorizontalPosition(Layout param1Layout, int param1Int) {
      return (int)(getHorizontal(param1Layout, param1Int) - 0.5F);
    }
    
    public void updatePosition(int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2) {
      positionAtCursorOffset(getCurrentCursorOffset(), param1Boolean2, false);
      if (param1Boolean1 || this.mPositionHasChanged) {
        if (this.mIsDragging) {
          if (param1Int1 != this.mLastParentX || param1Int2 != this.mLastParentY) {
            this.mTouchToWindowOffsetX += (param1Int1 - this.mLastParentX);
            this.mTouchToWindowOffsetY += (param1Int2 - this.mLastParentY);
            this.mLastParentX = param1Int1;
            this.mLastParentY = param1Int2;
          } 
          onHandleMoved();
        } 
        if (shouldShow()) {
          int[] arrayOfInt = new int[2];
          arrayOfInt[0] = this.mPositionX + this.mHotspotX + getHorizontalOffset();
          arrayOfInt[1] = this.mPositionY;
          Editor.this.mTextView.transformFromViewToWindowSpace(arrayOfInt);
          arrayOfInt[0] = arrayOfInt[0] - this.mHotspotX + getHorizontalOffset();
          if (isShowing()) {
            this.mContainer.update(arrayOfInt[0], arrayOfInt[1], -1, -1);
          } else {
            this.mContainer.showAtLocation(Editor.this.mTextView, 0, arrayOfInt[0], arrayOfInt[1]);
          } 
        } else if (isShowing()) {
          dismiss();
        } 
        this.mPositionHasChanged = false;
      } 
    }
    
    protected void onDraw(Canvas param1Canvas) {
      int i = this.mDrawable.getIntrinsicWidth();
      int j = getHorizontalOffset();
      Drawable drawable = this.mDrawable;
      drawable.setBounds(j, 0, j + i, drawable.getIntrinsicHeight());
      this.mDrawable.draw(param1Canvas);
    }
    
    private int getHorizontalOffset() {
      int i = getPreferredWidth();
      int j = this.mDrawable.getIntrinsicWidth();
      int k = this.mHorizontalGravity;
      if (k != 3) {
        if (k != 5) {
          k = (i - j) / 2;
        } else {
          k = i - j;
        } 
      } else {
        k = 0;
      } 
      return k;
    }
    
    protected int getCursorOffset() {
      return 0;
    }
    
    private boolean tooLargeTextForMagnifier() {
      boolean bool = Editor.this.mNewMagnifierEnabled;
      boolean bool1 = true, bool2 = true;
      if (bool) {
        Layout layout = Editor.this.mTextView.getLayout();
        int i = layout.getLineForOffset(getCurrentCursorOffset());
        int j = layout.getLineBottomWithoutSpacing(i);
        i = layout.getLineTop(i);
        Editor editor1 = Editor.this;
        if (j - i < editor1.mMaxLineHeightForMagnifier)
          bool2 = false; 
        return bool2;
      } 
      Editor editor = Editor.this;
      float f1 = editor.mMagnifierAnimator.mMagnifier.getHeight();
      editor = Editor.this;
      f1 /= editor.mMagnifierAnimator.mMagnifier.getZoom();
      float f2 = Math.round(f1);
      Paint.FontMetrics fontMetrics = Editor.this.mTextView.getPaint().getFontMetrics();
      float f3 = fontMetrics.descent;
      f1 = fontMetrics.ascent;
      if (this.mTextViewScaleY * (f3 - f1) > f2) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      return bool2;
    }
    
    private boolean checkForTransforms() {
      if (Editor.this.mMagnifierAnimator.mMagnifierIsShowing)
        return true; 
      if (Editor.this.mTextView.getRotation() == 0.0F && Editor.this.mTextView.getRotationX() == 0.0F) {
        Editor editor = Editor.this;
        if (editor.mTextView.getRotationY() == 0.0F) {
          this.mTextViewScaleX = Editor.this.mTextView.getScaleX();
          this.mTextViewScaleY = Editor.this.mTextView.getScaleY();
          ViewParent viewParent = Editor.this.mTextView.getParent();
          while (viewParent != null) {
            if (viewParent instanceof View) {
              View view = (View)viewParent;
              if (view.getRotation() != 0.0F || view.getRotationX() != 0.0F || view.getRotationY() != 0.0F)
                return false; 
              this.mTextViewScaleX *= view.getScaleX();
              this.mTextViewScaleY *= view.getScaleY();
            } 
            viewParent = viewParent.getParent();
          } 
          return true;
        } 
      } 
      return false;
    }
    
    private boolean obtainMagnifierShowCoordinates(MotionEvent param1MotionEvent, PointF param1PointF) {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual getMagnifierHandleTrigger : ()I
      //   4: istore_3
      //   5: iload_3
      //   6: ifeq -> 82
      //   9: iload_3
      //   10: iconst_1
      //   11: if_icmpeq -> 55
      //   14: iload_3
      //   15: iconst_2
      //   16: if_icmpeq -> 28
      //   19: iconst_m1
      //   20: istore #4
      //   22: iconst_m1
      //   23: istore #5
      //   25: goto -> 97
      //   28: aload_0
      //   29: getfield this$0 : Landroid/widget/Editor;
      //   32: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   35: invokevirtual getSelectionEnd : ()I
      //   38: istore #4
      //   40: aload_0
      //   41: getfield this$0 : Landroid/widget/Editor;
      //   44: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   47: invokevirtual getSelectionStart : ()I
      //   50: istore #5
      //   52: goto -> 97
      //   55: aload_0
      //   56: getfield this$0 : Landroid/widget/Editor;
      //   59: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   62: invokevirtual getSelectionStart : ()I
      //   65: istore #4
      //   67: aload_0
      //   68: getfield this$0 : Landroid/widget/Editor;
      //   71: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   74: invokevirtual getSelectionEnd : ()I
      //   77: istore #5
      //   79: goto -> 97
      //   82: aload_0
      //   83: getfield this$0 : Landroid/widget/Editor;
      //   86: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   89: invokevirtual getSelectionStart : ()I
      //   92: istore #4
      //   94: iconst_m1
      //   95: istore #5
      //   97: iload #4
      //   99: iconst_m1
      //   100: if_icmpne -> 105
      //   103: iconst_0
      //   104: ireturn
      //   105: aload_1
      //   106: invokevirtual getActionMasked : ()I
      //   109: ifne -> 123
      //   112: aload_0
      //   113: aload_1
      //   114: invokevirtual getRawX : ()F
      //   117: putfield mCurrentDragInitialTouchRawX : F
      //   120: goto -> 137
      //   123: aload_1
      //   124: invokevirtual getActionMasked : ()I
      //   127: iconst_1
      //   128: if_icmpne -> 137
      //   131: aload_0
      //   132: ldc -1.0
      //   134: putfield mCurrentDragInitialTouchRawX : F
      //   137: aload_0
      //   138: getfield this$0 : Landroid/widget/Editor;
      //   141: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   144: invokevirtual getLayout : ()Landroid/text/Layout;
      //   147: astore #6
      //   149: aload #6
      //   151: iload #4
      //   153: invokevirtual getLineForOffset : (I)I
      //   156: istore #7
      //   158: iload #5
      //   160: iconst_m1
      //   161: if_icmpeq -> 182
      //   164: iload #7
      //   166: aload #6
      //   168: iload #5
      //   170: invokevirtual getLineForOffset : (I)I
      //   173: if_icmpne -> 182
      //   176: iconst_1
      //   177: istore #8
      //   179: goto -> 185
      //   182: iconst_0
      //   183: istore #8
      //   185: iload #8
      //   187: ifeq -> 276
      //   190: iload #4
      //   192: iload #5
      //   194: if_icmpge -> 203
      //   197: iconst_1
      //   198: istore #9
      //   200: goto -> 206
      //   203: iconst_0
      //   204: istore #9
      //   206: aload_0
      //   207: getfield this$0 : Landroid/widget/Editor;
      //   210: astore #6
      //   212: aload_0
      //   213: aload #6
      //   215: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   218: invokevirtual getLayout : ()Landroid/text/Layout;
      //   221: iload #4
      //   223: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   226: fstore #10
      //   228: aload_0
      //   229: getfield this$0 : Landroid/widget/Editor;
      //   232: astore #6
      //   234: fload #10
      //   236: aload_0
      //   237: aload #6
      //   239: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   242: invokevirtual getLayout : ()Landroid/text/Layout;
      //   245: iload #5
      //   247: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   250: fcmpg
      //   251: ifge -> 260
      //   254: iconst_1
      //   255: istore #4
      //   257: goto -> 263
      //   260: iconst_0
      //   261: istore #4
      //   263: iload #9
      //   265: iload #4
      //   267: if_icmpeq -> 276
      //   270: iconst_1
      //   271: istore #4
      //   273: goto -> 279
      //   276: iconst_0
      //   277: istore #4
      //   279: iconst_2
      //   280: newarray int
      //   282: astore #6
      //   284: aload_0
      //   285: getfield this$0 : Landroid/widget/Editor;
      //   288: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   291: aload #6
      //   293: invokevirtual getLocationOnScreen : ([I)V
      //   296: aload_1
      //   297: invokevirtual getRawX : ()F
      //   300: aload #6
      //   302: iconst_0
      //   303: iaload
      //   304: i2f
      //   305: fsub
      //   306: fstore #11
      //   308: aload_0
      //   309: getfield this$0 : Landroid/widget/Editor;
      //   312: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   315: invokevirtual getTotalPaddingLeft : ()I
      //   318: aload_0
      //   319: getfield this$0 : Landroid/widget/Editor;
      //   322: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   325: invokevirtual getScrollX : ()I
      //   328: isub
      //   329: i2f
      //   330: fstore #10
      //   332: aload_0
      //   333: getfield this$0 : Landroid/widget/Editor;
      //   336: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   339: invokevirtual getTotalPaddingLeft : ()I
      //   342: aload_0
      //   343: getfield this$0 : Landroid/widget/Editor;
      //   346: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   349: invokevirtual getScrollX : ()I
      //   352: isub
      //   353: i2f
      //   354: fstore #12
      //   356: iload #8
      //   358: ifeq -> 407
      //   361: iload_3
      //   362: iconst_2
      //   363: if_icmpne -> 372
      //   366: iconst_1
      //   367: istore #9
      //   369: goto -> 375
      //   372: iconst_0
      //   373: istore #9
      //   375: iload #9
      //   377: iload #4
      //   379: ixor
      //   380: ifeq -> 407
      //   383: fload #10
      //   385: aload_0
      //   386: aload_0
      //   387: getfield this$0 : Landroid/widget/Editor;
      //   390: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   393: invokevirtual getLayout : ()Landroid/text/Layout;
      //   396: iload #5
      //   398: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   401: fadd
      //   402: fstore #10
      //   404: goto -> 427
      //   407: fload #10
      //   409: aload_0
      //   410: getfield this$0 : Landroid/widget/Editor;
      //   413: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   416: invokevirtual getLayout : ()Landroid/text/Layout;
      //   419: iload #7
      //   421: invokevirtual getLineLeft : (I)F
      //   424: fadd
      //   425: fstore #10
      //   427: iload #8
      //   429: ifeq -> 478
      //   432: iload_3
      //   433: iconst_1
      //   434: if_icmpne -> 443
      //   437: iconst_1
      //   438: istore #8
      //   440: goto -> 446
      //   443: iconst_0
      //   444: istore #8
      //   446: iload #8
      //   448: iload #4
      //   450: ixor
      //   451: ifeq -> 478
      //   454: fload #12
      //   456: aload_0
      //   457: aload_0
      //   458: getfield this$0 : Landroid/widget/Editor;
      //   461: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   464: invokevirtual getLayout : ()Landroid/text/Layout;
      //   467: iload #5
      //   469: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   472: fadd
      //   473: fstore #12
      //   475: goto -> 498
      //   478: fload #12
      //   480: aload_0
      //   481: getfield this$0 : Landroid/widget/Editor;
      //   484: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   487: invokevirtual getLayout : ()Landroid/text/Layout;
      //   490: iload #7
      //   492: invokevirtual getLineRight : (I)F
      //   495: fadd
      //   496: fstore #12
      //   498: aload_0
      //   499: getfield mTextViewScaleX : F
      //   502: fstore #13
      //   504: fload #10
      //   506: fload #13
      //   508: fmul
      //   509: fstore #14
      //   511: fload #12
      //   513: fload #13
      //   515: fmul
      //   516: fstore #12
      //   518: aload_0
      //   519: getfield this$0 : Landroid/widget/Editor;
      //   522: invokestatic access$000 : (Landroid/widget/Editor;)Landroid/widget/Editor$MagnifierMotionAnimator;
      //   525: invokestatic access$5000 : (Landroid/widget/Editor$MagnifierMotionAnimator;)Landroid/widget/Magnifier;
      //   528: invokevirtual getWidth : ()I
      //   531: i2f
      //   532: fstore #10
      //   534: aload_0
      //   535: getfield this$0 : Landroid/widget/Editor;
      //   538: astore #15
      //   540: fload #10
      //   542: aload #15
      //   544: invokestatic access$000 : (Landroid/widget/Editor;)Landroid/widget/Editor$MagnifierMotionAnimator;
      //   547: invokestatic access$5000 : (Landroid/widget/Editor$MagnifierMotionAnimator;)Landroid/widget/Magnifier;
      //   550: invokevirtual getZoom : ()F
      //   553: fdiv
      //   554: fstore #10
      //   556: fload #10
      //   558: invokestatic round : (F)I
      //   561: i2f
      //   562: fstore #10
      //   564: fload #11
      //   566: fload #14
      //   568: fload #10
      //   570: fconst_2
      //   571: fdiv
      //   572: fsub
      //   573: fcmpg
      //   574: iflt -> 739
      //   577: fload #11
      //   579: fload #12
      //   581: fload #10
      //   583: fconst_2
      //   584: fdiv
      //   585: fadd
      //   586: fcmpl
      //   587: ifle -> 593
      //   590: goto -> 739
      //   593: aload_0
      //   594: getfield mTextViewScaleX : F
      //   597: fconst_1
      //   598: fcmpl
      //   599: ifne -> 609
      //   602: fload #11
      //   604: fstore #10
      //   606: goto -> 642
      //   609: aload_1
      //   610: invokevirtual getRawX : ()F
      //   613: fstore #11
      //   615: aload_0
      //   616: getfield mCurrentDragInitialTouchRawX : F
      //   619: fstore #10
      //   621: fload #11
      //   623: fload #10
      //   625: fsub
      //   626: aload_0
      //   627: getfield mTextViewScaleX : F
      //   630: fmul
      //   631: fload #10
      //   633: fadd
      //   634: aload #6
      //   636: iconst_0
      //   637: iaload
      //   638: i2f
      //   639: fsub
      //   640: fstore #10
      //   642: aload_2
      //   643: fload #14
      //   645: fload #12
      //   647: fload #10
      //   649: invokestatic min : (FF)F
      //   652: invokestatic max : (FF)F
      //   655: putfield x : F
      //   658: aload_0
      //   659: getfield this$0 : Landroid/widget/Editor;
      //   662: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   665: invokevirtual getLayout : ()Landroid/text/Layout;
      //   668: iload #7
      //   670: invokevirtual getLineTop : (I)I
      //   673: istore #5
      //   675: aload_0
      //   676: getfield this$0 : Landroid/widget/Editor;
      //   679: astore_1
      //   680: iload #5
      //   682: aload_1
      //   683: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   686: invokevirtual getLayout : ()Landroid/text/Layout;
      //   689: iload #7
      //   691: invokevirtual getLineBottomWithoutSpacing : (I)I
      //   694: iadd
      //   695: i2f
      //   696: fconst_2
      //   697: fdiv
      //   698: fstore #10
      //   700: aload_0
      //   701: getfield this$0 : Landroid/widget/Editor;
      //   704: astore_1
      //   705: aload_2
      //   706: fload #10
      //   708: aload_1
      //   709: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   712: invokevirtual getTotalPaddingTop : ()I
      //   715: i2f
      //   716: fadd
      //   717: aload_0
      //   718: getfield this$0 : Landroid/widget/Editor;
      //   721: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   724: invokevirtual getScrollY : ()I
      //   727: i2f
      //   728: fsub
      //   729: aload_0
      //   730: getfield mTextViewScaleY : F
      //   733: fmul
      //   734: putfield y : F
      //   737: iconst_1
      //   738: ireturn
      //   739: iconst_0
      //   740: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5156	-> 0
      //   #5159	-> 5
      //   #5173	-> 19
      //   #5174	-> 22
      //   #5169	-> 28
      //   #5170	-> 40
      //   #5171	-> 52
      //   #5165	-> 55
      //   #5166	-> 67
      //   #5167	-> 79
      //   #5161	-> 82
      //   #5162	-> 94
      //   #5163	-> 97
      //   #5178	-> 97
      //   #5179	-> 103
      //   #5182	-> 105
      //   #5183	-> 112
      //   #5184	-> 123
      //   #5185	-> 131
      //   #5188	-> 137
      //   #5189	-> 149
      //   #5192	-> 158
      //   #5193	-> 164
      //   #5194	-> 185
      //   #5196	-> 212
      //   #5197	-> 234
      //   #5200	-> 279
      //   #5201	-> 284
      //   #5202	-> 296
      //   #5203	-> 308
      //   #5204	-> 332
      //   #5205	-> 356
      //   #5206	-> 383
      //   #5208	-> 407
      //   #5210	-> 427
      //   #5211	-> 454
      //   #5213	-> 478
      //   #5215	-> 498
      //   #5216	-> 511
      //   #5217	-> 518
      //   #5218	-> 540
      //   #5217	-> 556
      //   #5219	-> 564
      //   #5226	-> 593
      //   #5233	-> 602
      //   #5235	-> 609
      //   #5239	-> 642
      //   #5242	-> 658
      //   #5243	-> 680
      //   #5244	-> 705
      //   #5245	-> 737
      //   #5219	-> 739
      //   #5222	-> 739
    }
    
    private boolean handleOverlapsMagnifier(HandleView param1HandleView, Rect param1Rect) {
      PopupWindow popupWindow = param1HandleView.mContainer;
      if (!popupWindow.hasDecorView())
        return false; 
      int i = (popupWindow.getDecorViewLayoutParams()).x;
      int j = (popupWindow.getDecorViewLayoutParams()).y;
      int k = (popupWindow.getDecorViewLayoutParams()).x, m = popupWindow.getContentView().getWidth();
      Rect rect = new Rect(i, j, k + m, (popupWindow.getDecorViewLayoutParams()).y + popupWindow.getContentView().getHeight());
      return Rect.intersects(rect, param1Rect);
    }
    
    private HandleView getOtherSelectionHandle() {
      Editor.SelectionHandleView selectionHandleView;
      Editor.SelectionModifierCursorController selectionModifierCursorController = Editor.this.getSelectionController();
      if (selectionModifierCursorController == null || !selectionModifierCursorController.isActive())
        return null; 
      if (selectionModifierCursorController.mStartHandle != this) {
        selectionHandleView = selectionModifierCursorController.mStartHandle;
      } else {
        selectionHandleView = ((Editor.SelectionModifierCursorController)selectionHandleView).mEndHandle;
      } 
      return selectionHandleView;
    }
    
    private void updateHandlesVisibility() {
      Point point = Editor.this.mMagnifierAnimator.mMagnifier.getPosition();
      if (point == null)
        return; 
      int i = point.x, j = point.y, k = point.x;
      Editor editor = Editor.this;
      int m = editor.mMagnifierAnimator.mMagnifier.getWidth(), n = point.y;
      editor = Editor.this;
      Rect rect = new Rect(i, j, k + m, n + editor.mMagnifierAnimator.mMagnifier.getHeight());
      setVisible(handleOverlapsMagnifier(this, rect) ^ true);
      HandleView handleView = getOtherSelectionHandle();
      if (handleView != null)
        handleView.setVisible(handleOverlapsMagnifier(handleView, rect) ^ true); 
    }
    
    protected final void updateMagnifier(MotionEvent param1MotionEvent) {
      int i;
      if (Editor.this.getMagnifierAnimator() == null)
        return; 
      PointF pointF = new PointF();
      if (checkForTransforms() && !tooLargeTextForMagnifier() && obtainMagnifierShowCoordinates(param1MotionEvent, pointF)) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i) {
        Editor.access$5402(Editor.this, true);
        Editor.this.mTextView.invalidateCursorPath();
        Editor.this.suspendBlink();
        if (Editor.this.mNewMagnifierEnabled) {
          Layout layout = Editor.this.mTextView.getLayout();
          int j = layout.getLineForOffset(getCurrentCursorOffset());
          int k = (int)layout.getLineLeft(j);
          int m = Editor.this.mTextView.getTotalPaddingLeft(), n = Editor.this.mTextView.getScrollX();
          int i1 = (int)layout.getLineRight(j);
          int i2 = Editor.this.mTextView.getTotalPaddingLeft();
          i = Editor.this.mTextView.getScrollX();
          Editor.this.mMagnifierAnimator.mMagnifier.setSourceHorizontalBounds(k + m - n, i1 + i2 - i);
          i = layout.getLineBottomWithoutSpacing(j) - layout.getLineTop(j);
          float f1 = Editor.this.mInitialZoom;
          float f2 = f1;
          if (i < Editor.this.mMinLineHeightForMagnifier)
            f2 = Editor.this.mMinLineHeightForMagnifier * f1 / i; 
          Editor.this.mMagnifierAnimator.mMagnifier.updateSourceFactors(i, f2);
          Editor.this.mMagnifierAnimator.mMagnifier.show(pointF.x, pointF.y);
        } else {
          Editor.this.mMagnifierAnimator.show(pointF.x, pointF.y);
        } 
        updateHandlesVisibility();
      } else {
        dismissMagnifier();
      } 
    }
    
    protected final void dismissMagnifier() {
      if (Editor.this.mMagnifierAnimator != null) {
        Editor.this.mMagnifierAnimator.dismiss();
        Editor.access$5402(Editor.this, false);
        Editor.this.resumeBlink();
        setVisible(true);
        HandleView handleView = getOtherSelectionHandle();
        if (handleView != null)
          handleView.setVisible(true); 
      } 
    }
    
    public boolean onTouchEvent(MotionEvent param1MotionEvent) {
      Editor.this.updateFloatingToolbarVisibility(param1MotionEvent);
      int i = param1MotionEvent.getActionMasked();
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3)
              return true; 
          } else {
            float f1 = param1MotionEvent.getRawX(), f2 = this.mLastParentXOnScreen, f3 = this.mLastParentX;
            float f4 = param1MotionEvent.getRawY(), f5 = this.mLastParentYOnScreen;
            i = this.mLastParentY;
            f5 = f4 - f5 + i;
            f4 = this.mTouchToWindowOffsetY - i;
            float f6 = f5 - this.mPositionY - i;
            float f7 = this.mIdealVerticalOffset;
            if (f4 < f7) {
              f6 = Math.min(f6, f7);
              f4 = Math.max(f6, f4);
            } else {
              f6 = Math.max(f6, f7);
              f4 = Math.min(f6, f4);
            } 
            this.mTouchToWindowOffsetY = this.mLastParentY + f4;
            float f8 = this.mTouchToWindowOffsetX;
            f7 = this.mHotspotX;
            f4 = getHorizontalOffset();
            f6 = this.mTouchToWindowOffsetY;
            float f9 = this.mTouchOffsetY;
            boolean bool = param1MotionEvent.isFromSource(4098);
            updatePosition(f1 - f2 + f3 - f8 + f7 + f4, f5 - f6 + f9, bool);
            return true;
          } 
        } else {
          filterOnTouchUp(param1MotionEvent.isFromSource(4098));
        } 
        this.mIsDragging = false;
        updateDrawable(false);
      } else {
        startTouchUpFilter(getCurrentCursorOffset());
        Editor.PositionListener positionListener = Editor.this.getPositionListener();
        this.mLastParentX = positionListener.getPositionX();
        this.mLastParentY = positionListener.getPositionY();
        this.mLastParentXOnScreen = positionListener.getPositionXOnScreen();
        this.mLastParentYOnScreen = positionListener.getPositionYOnScreen();
        float f2 = param1MotionEvent.getRawX(), f3 = this.mLastParentXOnScreen, f1 = this.mLastParentX;
        float f4 = param1MotionEvent.getRawY(), f6 = this.mLastParentYOnScreen, f5 = this.mLastParentY;
        this.mTouchToWindowOffsetX = f2 - f3 + f1 - this.mPositionX;
        this.mTouchToWindowOffsetY = f4 - f6 + f5 - this.mPositionY;
        this.mIsDragging = true;
        this.mPreviousLineTouched = -1;
      } 
      return true;
    }
    
    public boolean isDragging() {
      return this.mIsDragging;
    }
    
    void onHandleMoved() {}
    
    public void onDetached() {}
    
    protected void onSizeChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      super.onSizeChanged(param1Int1, param1Int2, param1Int3, param1Int4);
      setSystemGestureExclusionRects(Collections.singletonList(new Rect(0, 0, param1Int1, param1Int2)));
    }
    
    public abstract int getCurrentCursorOffset();
    
    protected abstract int getHorizontalGravity(boolean param1Boolean);
    
    protected abstract int getHotspotX(Drawable param1Drawable, boolean param1Boolean);
    
    protected abstract int getMagnifierHandleTrigger();
    
    protected abstract void updatePosition(float param1Float1, float param1Float2, boolean param1Boolean);
    
    protected abstract void updateSelection(int param1Int);
  }
  
  class InsertionHandleView extends HandleView {
    private final int mDeltaHeight;
    
    private final int mDrawableOpacity;
    
    private Runnable mHider;
    
    private boolean mIsInActionMode;
    
    private boolean mIsTouchDown;
    
    private float mLastDownRawX;
    
    private float mLastDownRawY;
    
    private long mLastUpTime;
    
    private boolean mOffsetChanged;
    
    private int mOffsetDown;
    
    private boolean mPendingDismissOnUp;
    
    private float mTouchDownX;
    
    private float mTouchDownY;
    
    final Editor this$0;
    
    InsertionHandleView(Editor this$0, Drawable param1Drawable) {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: putfield this$0 : Landroid/widget/Editor;
      //   5: aload_0
      //   6: aload_1
      //   7: aload_2
      //   8: aload_2
      //   9: ldc 16909086
      //   11: aconst_null
      //   12: invokespecial <init> : (Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;ILandroid/widget/Editor$1;)V
      //   15: aload_0
      //   16: iconst_0
      //   17: putfield mIsTouchDown : Z
      //   20: aload_0
      //   21: iconst_0
      //   22: putfield mPendingDismissOnUp : Z
      //   25: iconst_0
      //   26: istore_3
      //   27: sipush #255
      //   30: istore #4
      //   32: aload_1
      //   33: invokestatic access$6300 : (Landroid/widget/Editor;)Z
      //   36: ifeq -> 110
      //   39: ldc 'widget__insertion_handle_delta_height'
      //   41: bipush #25
      //   43: invokestatic getIntCoreSetting : (Ljava/lang/String;I)I
      //   46: istore #4
      //   48: ldc 'widget__insertion_handle_opacity'
      //   50: bipush #50
      //   52: invokestatic getIntCoreSetting : (Ljava/lang/String;I)I
      //   55: istore #5
      //   57: iload #4
      //   59: bipush #-25
      //   61: if_icmplt -> 74
      //   64: iload #4
      //   66: istore_3
      //   67: iload #4
      //   69: bipush #50
      //   71: if_icmple -> 77
      //   74: bipush #25
      //   76: istore_3
      //   77: iload #5
      //   79: bipush #10
      //   81: if_icmplt -> 95
      //   84: iload #5
      //   86: istore #4
      //   88: iload #5
      //   90: bipush #100
      //   92: if_icmple -> 99
      //   95: bipush #50
      //   97: istore #4
      //   99: iload #4
      //   101: sipush #255
      //   104: imul
      //   105: bipush #100
      //   107: idiv
      //   108: istore #4
      //   110: aload_0
      //   111: iload_3
      //   112: putfield mDeltaHeight : I
      //   115: aload_0
      //   116: iload #4
      //   118: putfield mDrawableOpacity : I
      //   121: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5461	-> 0
      //   #5462	-> 5
      //   #5436	-> 15
      //   #5438	-> 20
      //   #5464	-> 25
      //   #5465	-> 27
      //   #5466	-> 32
      //   #5467	-> 39
      //   #5470	-> 48
      //   #5474	-> 57
      //   #5475	-> 74
      //   #5477	-> 77
      //   #5478	-> 95
      //   #5481	-> 99
      //   #5483	-> 110
      //   #5484	-> 115
      //   #5485	-> 121
    }
    
    private void hideAfterDelay() {
      if (this.mHider == null) {
        this.mHider = (Runnable)new Object(this);
      } else {
        removeHiderCallback();
      } 
      this.this$0.mTextView.postDelayed(this.mHider, 4000L);
    }
    
    private void removeHiderCallback() {
      if (this.mHider != null)
        this.this$0.mTextView.removeCallbacks(this.mHider); 
    }
    
    protected int getHotspotX(Drawable param1Drawable, boolean param1Boolean) {
      return param1Drawable.getIntrinsicWidth() / 2;
    }
    
    protected int getHorizontalGravity(boolean param1Boolean) {
      return 1;
    }
    
    protected int getCursorOffset() {
      int i = super.getCursorOffset();
      int j = i;
      if (this.this$0.mDrawableForCursor != null) {
        this.this$0.mDrawableForCursor.getPadding(this.this$0.mTempRect);
        j = this.this$0.mDrawableForCursor.getIntrinsicWidth();
        Editor editor = this.this$0;
        j = i + (j - editor.mTempRect.left - this.this$0.mTempRect.right) / 2;
      } 
      return j;
    }
    
    int getCursorHorizontalPosition(Layout param1Layout, int param1Int) {
      Editor editor;
      if (this.this$0.mDrawableForCursor != null) {
        float f = getHorizontal(param1Layout, param1Int);
        editor = this.this$0;
        return editor.clampHorizontalPosition(editor.mDrawableForCursor, f) + this.this$0.mTempRect.left;
      } 
      return super.getCursorHorizontalPosition((Layout)editor, param1Int);
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      if (this.this$0.mFlagInsertionHandleGesturesEnabled) {
        param1Int1 = getPreferredHeight();
        int i = this.mDeltaHeight;
        param1Int2 = this.mDrawable.getIntrinsicHeight();
        param1Int1 = Math.max(param1Int1 + i, param1Int2);
        setMeasuredDimension(getPreferredWidth(), param1Int1);
        return;
      } 
      super.onMeasure(param1Int1, param1Int2);
    }
    
    public boolean onTouchEvent(MotionEvent param1MotionEvent) {
      if (!this.this$0.mTextView.isFromPrimePointer(param1MotionEvent, true))
        return true; 
      if (this.this$0.mFlagInsertionHandleGesturesEnabled && this.this$0.mFlagCursorDragFromAnywhereEnabled)
        return touchThrough(param1MotionEvent); 
      boolean bool = super.onTouchEvent(param1MotionEvent);
      int i = param1MotionEvent.getActionMasked();
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3)
              return bool; 
          } else {
            updateMagnifier(param1MotionEvent);
            return bool;
          } 
        } else if (!offsetHasBeenChanged()) {
          ViewConfiguration viewConfiguration = ViewConfiguration.get(this.this$0.mTextView.getContext());
          float f1 = this.mLastDownRawX, f2 = this.mLastDownRawY;
          float f3 = param1MotionEvent.getRawX(), f4 = param1MotionEvent.getRawY();
          i = viewConfiguration.getScaledTouchSlop();
          boolean bool1 = EditorTouchState.isDistanceWithin(f1, f2, f3, f4, i);
          if (bool1)
            this.this$0.toggleInsertionActionMode(); 
        } else if (this.this$0.mTextActionMode != null) {
          this.this$0.mTextActionMode.invalidateContentRect();
        } 
        hideAfterDelay();
        dismissMagnifier();
      } else {
        this.mLastDownRawX = param1MotionEvent.getRawX();
        this.mLastDownRawY = param1MotionEvent.getRawY();
        updateMagnifier(param1MotionEvent);
      } 
      return bool;
    }
    
    private boolean touchThrough(MotionEvent param1MotionEvent) {
      int i = param1MotionEvent.getActionMasked();
      if (i != 0) {
        if (i == 1)
          this.mLastUpTime = param1MotionEvent.getEventTime(); 
      } else {
        boolean bool1;
        this.mIsTouchDown = true;
        this.mOffsetChanged = false;
        this.mOffsetDown = this.this$0.mTextView.getSelectionStart();
        this.mTouchDownX = param1MotionEvent.getX();
        this.mTouchDownY = param1MotionEvent.getY();
        if (this.this$0.mTextActionMode != null) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        this.mIsInActionMode = bool1;
        if (param1MotionEvent.getEventTime() - this.mLastUpTime < ViewConfiguration.getDoubleTapTimeout())
          this.this$0.stopTextActionMode(); 
        this.this$0.mTouchState.setIsOnHandle(true);
      } 
      boolean bool = this.this$0.mTextView.onTouchEvent(transformEventForTouchThrough(param1MotionEvent));
      if (i == 1 || i == 3) {
        this.mIsTouchDown = false;
        if (this.mPendingDismissOnUp)
          dismiss(); 
        this.this$0.mTouchState.setIsOnHandle(false);
      } 
      if (!this.mOffsetChanged) {
        int j = this.this$0.mTextView.getSelectionStart();
        int k = this.this$0.mTextView.getSelectionEnd();
        if (j != k || this.mOffsetDown != j)
          this.mOffsetChanged = true; 
      } 
      if (!this.mOffsetChanged && i == 1)
        if (this.mIsInActionMode) {
          this.this$0.stopTextActionMode();
        } else {
          this.this$0.startInsertionActionMode();
        }  
      return bool;
    }
    
    private MotionEvent transformEventForTouchThrough(MotionEvent param1MotionEvent) {
      Layout layout = this.this$0.mTextView.getLayout();
      int i = layout.getLineForOffset(getCurrentCursorOffset());
      int j = layout.getLineBottomWithoutSpacing(i);
      i = layout.getLineTop(i);
      Matrix matrix = new Matrix();
      float f1 = param1MotionEvent.getRawX(), f2 = param1MotionEvent.getX(), f3 = (getMeasuredWidth() >> 1), f4 = this.mTouchDownX;
      float f5 = param1MotionEvent.getRawY(), f6 = param1MotionEvent.getY(), f7 = (j - i >> 1), f8 = this.mTouchDownY;
      matrix.setTranslate(f1 - f2 + f3 - f4, f5 - f6 - f7 - f8);
      param1MotionEvent.transform(matrix);
      this.this$0.mTextView.toLocalMotionEvent(param1MotionEvent);
      return param1MotionEvent;
    }
    
    public boolean isShowing() {
      if (this.mPendingDismissOnUp)
        return false; 
      return super.isShowing();
    }
    
    public void show() {
      super.show();
      this.mPendingDismissOnUp = false;
      this.mDrawable.setAlpha(this.mDrawableOpacity);
    }
    
    public void dismiss() {
      if (this.mIsTouchDown) {
        this.mPendingDismissOnUp = true;
        this.mDrawable.setAlpha(0);
      } else {
        super.dismiss();
        this.mPendingDismissOnUp = false;
      } 
    }
    
    protected void updateDrawable(boolean param1Boolean) {
      super.updateDrawable(param1Boolean);
      this.mDrawable.setAlpha(this.mDrawableOpacity);
    }
    
    public int getCurrentCursorOffset() {
      return this.this$0.mTextView.getSelectionStart();
    }
    
    public void updateSelection(int param1Int) {
      Selection.setSelection((Spannable)this.this$0.mTextView.getText(), param1Int);
    }
    
    protected void updatePosition(float param1Float1, float param1Float2, boolean param1Boolean) {
      byte b;
      Layout layout = this.this$0.mTextView.getLayout();
      if (layout != null) {
        if (this.mPreviousLineTouched == -1)
          this.mPreviousLineTouched = this.this$0.mTextView.getLineAtCoordinate(param1Float2); 
        int i = this.this$0.getCurrentLineAdjustedForSlop(layout, this.mPreviousLineTouched, param1Float2);
        b = getOffsetAtCoordinate(layout, i, param1Float1);
        this.mPreviousLineTouched = i;
      } else {
        b = -1;
      } 
      positionAtCursorOffset(b, false, param1Boolean);
      if (this.this$0.mTextActionMode != null)
        this.this$0.invalidateActionMode(); 
    }
    
    void onHandleMoved() {
      super.onHandleMoved();
      removeHiderCallback();
    }
    
    public void onDetached() {
      super.onDetached();
      removeHiderCallback();
    }
    
    protected int getMagnifierHandleTrigger() {
      return 0;
    }
  }
  
  class SelectionHandleView extends HandleView {
    private final int mHandleType;
    
    private boolean mInWord = false;
    
    private boolean mLanguageDirectionChanged = false;
    
    private float mPrevX;
    
    private final float mTextViewEdgeSlop;
    
    private final int[] mTextViewLocation = new int[2];
    
    private float mTouchWordDelta;
    
    final Editor this$0;
    
    public SelectionHandleView(Drawable param1Drawable1, Drawable param1Drawable2, int param1Int1, int param1Int2) {
      super(param1Drawable1, param1Drawable2, param1Int1);
      this.mHandleType = param1Int2;
      ViewConfiguration viewConfiguration = ViewConfiguration.get(Editor.this.mTextView.getContext());
      this.mTextViewEdgeSlop = (viewConfiguration.getScaledTouchSlop() * 4);
    }
    
    private boolean isStartHandle() {
      boolean bool;
      if (this.mHandleType == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    protected int getHotspotX(Drawable param1Drawable, boolean param1Boolean) {
      if (param1Boolean == isStartHandle())
        return param1Drawable.getIntrinsicWidth() / 4; 
      return param1Drawable.getIntrinsicWidth() * 3 / 4;
    }
    
    protected int getHorizontalGravity(boolean param1Boolean) {
      byte b;
      if (param1Boolean == isStartHandle()) {
        b = 3;
      } else {
        b = 5;
      } 
      return b;
    }
    
    public int getCurrentCursorOffset() {
      int i;
      if (isStartHandle()) {
        i = Editor.this.mTextView.getSelectionStart();
      } else {
        i = Editor.this.mTextView.getSelectionEnd();
      } 
      return i;
    }
    
    protected void updateSelection(int param1Int) {
      if (isStartHandle()) {
        Spannable spannable = (Spannable)Editor.this.mTextView.getText();
        Editor editor = Editor.this;
        int i = editor.mTextView.getSelectionEnd();
        Selection.setSelection(spannable, param1Int, i);
      } else {
        Spannable spannable = (Spannable)Editor.this.mTextView.getText();
        Editor editor = Editor.this;
        int i = editor.mTextView.getSelectionStart();
        Selection.setSelection(spannable, i, param1Int);
      } 
      updateDrawable(false);
      if (Editor.this.mTextActionMode != null)
        Editor.this.invalidateActionMode(); 
    }
    
    protected void updatePosition(float param1Float1, float param1Float2, boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/widget/Editor;
      //   4: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   7: invokevirtual getLayout : ()Landroid/text/Layout;
      //   10: astore #4
      //   12: aload #4
      //   14: ifnonnull -> 35
      //   17: aload_0
      //   18: aload_0
      //   19: getfield this$0 : Landroid/widget/Editor;
      //   22: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   25: fload_1
      //   26: fload_2
      //   27: invokevirtual getOffsetForPosition : (FF)I
      //   30: iload_3
      //   31: invokespecial positionAndAdjustForCrossingHandles : (IZ)V
      //   34: return
      //   35: aload_0
      //   36: getfield mPreviousLineTouched : I
      //   39: iconst_m1
      //   40: if_icmpne -> 58
      //   43: aload_0
      //   44: aload_0
      //   45: getfield this$0 : Landroid/widget/Editor;
      //   48: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   51: fload_2
      //   52: invokevirtual getLineAtCoordinate : (F)I
      //   55: putfield mPreviousLineTouched : I
      //   58: aload_0
      //   59: invokespecial isStartHandle : ()Z
      //   62: ifeq -> 80
      //   65: aload_0
      //   66: getfield this$0 : Landroid/widget/Editor;
      //   69: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   72: invokevirtual getSelectionEnd : ()I
      //   75: istore #5
      //   77: goto -> 92
      //   80: aload_0
      //   81: getfield this$0 : Landroid/widget/Editor;
      //   84: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   87: invokevirtual getSelectionStart : ()I
      //   90: istore #5
      //   92: aload_0
      //   93: getfield this$0 : Landroid/widget/Editor;
      //   96: aload #4
      //   98: aload_0
      //   99: getfield mPreviousLineTouched : I
      //   102: fload_2
      //   103: invokevirtual getCurrentLineAdjustedForSlop : (Landroid/text/Layout;IF)I
      //   106: istore #6
      //   108: aload_0
      //   109: aload #4
      //   111: iload #6
      //   113: fload_1
      //   114: invokevirtual getOffsetAtCoordinate : (Landroid/text/Layout;IF)I
      //   117: istore #7
      //   119: aload_0
      //   120: invokespecial isStartHandle : ()Z
      //   123: ifeq -> 133
      //   126: iload #7
      //   128: iload #5
      //   130: if_icmpge -> 163
      //   133: iload #6
      //   135: istore #8
      //   137: iload #7
      //   139: istore #9
      //   141: aload_0
      //   142: invokespecial isStartHandle : ()Z
      //   145: ifne -> 183
      //   148: iload #6
      //   150: istore #8
      //   152: iload #7
      //   154: istore #9
      //   156: iload #7
      //   158: iload #5
      //   160: if_icmpgt -> 183
      //   163: aload #4
      //   165: iload #5
      //   167: invokevirtual getLineForOffset : (I)I
      //   170: istore #8
      //   172: aload_0
      //   173: aload #4
      //   175: iload #8
      //   177: fload_1
      //   178: invokevirtual getOffsetAtCoordinate : (Landroid/text/Layout;IF)I
      //   181: istore #9
      //   183: iload #9
      //   185: istore #6
      //   187: aload_0
      //   188: getfield this$0 : Landroid/widget/Editor;
      //   191: iload #6
      //   193: invokestatic access$6800 : (Landroid/widget/Editor;I)I
      //   196: istore #10
      //   198: aload_0
      //   199: getfield this$0 : Landroid/widget/Editor;
      //   202: iload #6
      //   204: invokestatic access$6900 : (Landroid/widget/Editor;I)I
      //   207: istore #11
      //   209: aload_0
      //   210: getfield mPrevX : F
      //   213: ldc -1.0
      //   215: fcmpl
      //   216: ifne -> 224
      //   219: aload_0
      //   220: fload_1
      //   221: putfield mPrevX : F
      //   224: aload_0
      //   225: invokevirtual getCurrentCursorOffset : ()I
      //   228: istore #12
      //   230: aload_0
      //   231: aload #4
      //   233: iload #12
      //   235: invokevirtual isAtRtlRun : (Landroid/text/Layout;I)Z
      //   238: istore #13
      //   240: aload_0
      //   241: aload #4
      //   243: iload #6
      //   245: invokevirtual isAtRtlRun : (Landroid/text/Layout;I)Z
      //   248: istore #14
      //   250: aload #4
      //   252: iload #6
      //   254: invokevirtual isLevelBoundary : (I)Z
      //   257: istore #15
      //   259: iload #15
      //   261: ifne -> 1237
      //   264: iload #13
      //   266: ifeq -> 280
      //   269: iload #14
      //   271: ifeq -> 277
      //   274: goto -> 280
      //   277: goto -> 1237
      //   280: iload #13
      //   282: ifne -> 293
      //   285: iload #14
      //   287: ifeq -> 293
      //   290: goto -> 277
      //   293: aload_0
      //   294: getfield mLanguageDirectionChanged : Z
      //   297: ifeq -> 323
      //   300: iload #15
      //   302: ifne -> 323
      //   305: aload_0
      //   306: iload #6
      //   308: iload_3
      //   309: invokespecial positionAndAdjustForCrossingHandles : (IZ)V
      //   312: aload_0
      //   313: fconst_0
      //   314: putfield mTouchWordDelta : F
      //   317: aload_0
      //   318: iconst_0
      //   319: putfield mLanguageDirectionChanged : Z
      //   322: return
      //   323: fload_1
      //   324: aload_0
      //   325: getfield mPrevX : F
      //   328: fsub
      //   329: fstore_2
      //   330: aload_0
      //   331: invokespecial isStartHandle : ()Z
      //   334: ifeq -> 358
      //   337: iload #8
      //   339: aload_0
      //   340: getfield mPreviousLineTouched : I
      //   343: if_icmpge -> 352
      //   346: iconst_1
      //   347: istore #5
      //   349: goto -> 355
      //   352: iconst_0
      //   353: istore #5
      //   355: goto -> 376
      //   358: iload #8
      //   360: aload_0
      //   361: getfield mPreviousLineTouched : I
      //   364: if_icmple -> 373
      //   367: iconst_1
      //   368: istore #5
      //   370: goto -> 376
      //   373: iconst_0
      //   374: istore #5
      //   376: iload #14
      //   378: aload_0
      //   379: invokespecial isStartHandle : ()Z
      //   382: if_icmpne -> 410
      //   385: fload_2
      //   386: fconst_0
      //   387: fcmpl
      //   388: ifle -> 397
      //   391: iconst_1
      //   392: istore #7
      //   394: goto -> 400
      //   397: iconst_0
      //   398: istore #7
      //   400: iload #5
      //   402: iload #7
      //   404: ior
      //   405: istore #5
      //   407: goto -> 432
      //   410: fload_2
      //   411: fconst_0
      //   412: fcmpg
      //   413: ifge -> 422
      //   416: iconst_1
      //   417: istore #7
      //   419: goto -> 425
      //   422: iconst_0
      //   423: istore #7
      //   425: iload #5
      //   427: iload #7
      //   429: ior
      //   430: istore #5
      //   432: aload_0
      //   433: getfield this$0 : Landroid/widget/Editor;
      //   436: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   439: invokevirtual getHorizontallyScrolling : ()Z
      //   442: ifeq -> 616
      //   445: aload_0
      //   446: fload_1
      //   447: iload #14
      //   449: invokespecial positionNearEdgeOfScrollingView : (FZ)Z
      //   452: ifeq -> 613
      //   455: aload_0
      //   456: invokespecial isStartHandle : ()Z
      //   459: ifeq -> 481
      //   462: aload_0
      //   463: getfield this$0 : Landroid/widget/Editor;
      //   466: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   469: invokevirtual getScrollX : ()I
      //   472: ifne -> 478
      //   475: goto -> 481
      //   478: goto -> 525
      //   481: aload_0
      //   482: invokespecial isStartHandle : ()Z
      //   485: ifne -> 610
      //   488: aload_0
      //   489: getfield this$0 : Landroid/widget/Editor;
      //   492: astore #16
      //   494: aload #16
      //   496: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   499: astore #16
      //   501: iload #14
      //   503: ifeq -> 512
      //   506: iconst_m1
      //   507: istore #7
      //   509: goto -> 515
      //   512: iconst_1
      //   513: istore #7
      //   515: aload #16
      //   517: iload #7
      //   519: invokevirtual canScrollHorizontally : (I)Z
      //   522: ifeq -> 616
      //   525: iload #5
      //   527: ifeq -> 558
      //   530: aload_0
      //   531: invokespecial isStartHandle : ()Z
      //   534: ifeq -> 544
      //   537: iload #6
      //   539: iload #12
      //   541: if_icmplt -> 563
      //   544: aload_0
      //   545: invokespecial isStartHandle : ()Z
      //   548: ifne -> 558
      //   551: iload #6
      //   553: iload #12
      //   555: if_icmpgt -> 563
      //   558: iload #5
      //   560: ifne -> 616
      //   563: aload_0
      //   564: fconst_0
      //   565: putfield mTouchWordDelta : F
      //   568: iload #14
      //   570: aload_0
      //   571: invokespecial isStartHandle : ()Z
      //   574: if_icmpne -> 591
      //   577: aload #4
      //   579: aload_0
      //   580: getfield mPreviousOffset : I
      //   583: invokevirtual getOffsetToRightOf : (I)I
      //   586: istore #5
      //   588: goto -> 602
      //   591: aload #4
      //   593: aload_0
      //   594: getfield mPreviousOffset : I
      //   597: invokevirtual getOffsetToLeftOf : (I)I
      //   600: istore #5
      //   602: aload_0
      //   603: iload #5
      //   605: iload_3
      //   606: invokespecial positionAndAdjustForCrossingHandles : (IZ)V
      //   609: return
      //   610: goto -> 616
      //   613: goto -> 616
      //   616: iload #5
      //   618: ifeq -> 943
      //   621: aload_0
      //   622: invokespecial isStartHandle : ()Z
      //   625: ifeq -> 635
      //   628: iload #11
      //   630: istore #7
      //   632: goto -> 639
      //   635: iload #10
      //   637: istore #7
      //   639: aload_0
      //   640: getfield mInWord : Z
      //   643: ifeq -> 674
      //   646: aload_0
      //   647: invokespecial isStartHandle : ()Z
      //   650: ifeq -> 665
      //   653: iload #8
      //   655: aload_0
      //   656: getfield mPrevLine : I
      //   659: if_icmpge -> 693
      //   662: goto -> 674
      //   665: iload #8
      //   667: aload_0
      //   668: getfield mPrevLine : I
      //   671: if_icmple -> 693
      //   674: iload #14
      //   676: aload_0
      //   677: aload #4
      //   679: iload #7
      //   681: invokevirtual isAtRtlRun : (Landroid/text/Layout;I)Z
      //   684: if_icmpne -> 693
      //   687: iconst_1
      //   688: istore #5
      //   690: goto -> 696
      //   693: iconst_0
      //   694: istore #5
      //   696: iload #5
      //   698: ifeq -> 867
      //   701: iload #7
      //   703: istore #5
      //   705: aload #4
      //   707: iload #7
      //   709: invokevirtual getLineForOffset : (I)I
      //   712: iload #8
      //   714: if_icmpeq -> 745
      //   717: aload_0
      //   718: invokespecial isStartHandle : ()Z
      //   721: ifeq -> 736
      //   724: aload #4
      //   726: iload #8
      //   728: invokevirtual getLineStart : (I)I
      //   731: istore #5
      //   733: goto -> 745
      //   736: aload #4
      //   738: iload #8
      //   740: invokevirtual getLineEnd : (I)I
      //   743: istore #5
      //   745: aload_0
      //   746: invokespecial isStartHandle : ()Z
      //   749: ifeq -> 767
      //   752: iload #10
      //   754: iload #10
      //   756: iload #5
      //   758: isub
      //   759: iconst_2
      //   760: idiv
      //   761: isub
      //   762: istore #7
      //   764: goto -> 779
      //   767: iload #5
      //   769: iload #11
      //   771: isub
      //   772: iconst_2
      //   773: idiv
      //   774: iload #11
      //   776: iadd
      //   777: istore #7
      //   779: aload_0
      //   780: invokespecial isStartHandle : ()Z
      //   783: ifeq -> 816
      //   786: iload #6
      //   788: iload #7
      //   790: if_icmple -> 805
      //   793: iload #8
      //   795: aload_0
      //   796: getfield mPrevLine : I
      //   799: if_icmpge -> 816
      //   802: goto -> 805
      //   805: iload #5
      //   807: istore #7
      //   809: iload #11
      //   811: istore #5
      //   813: goto -> 871
      //   816: aload_0
      //   817: invokespecial isStartHandle : ()Z
      //   820: ifne -> 850
      //   823: iload #6
      //   825: iload #7
      //   827: if_icmpge -> 839
      //   830: iload #8
      //   832: aload_0
      //   833: getfield mPrevLine : I
      //   836: if_icmple -> 850
      //   839: iload #5
      //   841: istore #7
      //   843: iload #10
      //   845: istore #5
      //   847: goto -> 871
      //   850: aload_0
      //   851: getfield mPreviousOffset : I
      //   854: istore #6
      //   856: iload #5
      //   858: istore #7
      //   860: iload #6
      //   862: istore #5
      //   864: goto -> 871
      //   867: iload #6
      //   869: istore #5
      //   871: aload_0
      //   872: invokespecial isStartHandle : ()Z
      //   875: ifeq -> 885
      //   878: iload #5
      //   880: iload #9
      //   882: if_icmplt -> 899
      //   885: aload_0
      //   886: invokespecial isStartHandle : ()Z
      //   889: ifne -> 932
      //   892: iload #5
      //   894: iload #9
      //   896: if_icmple -> 932
      //   899: aload_0
      //   900: aload #4
      //   902: iload #5
      //   904: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   907: fstore_2
      //   908: aload_0
      //   909: getfield this$0 : Landroid/widget/Editor;
      //   912: astore #4
      //   914: aload_0
      //   915: aload #4
      //   917: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   920: fload_1
      //   921: invokevirtual convertToLocalHorizontalCoordinate : (F)F
      //   924: fload_2
      //   925: fsub
      //   926: putfield mTouchWordDelta : F
      //   929: goto -> 937
      //   932: aload_0
      //   933: fconst_0
      //   934: putfield mTouchWordDelta : F
      //   937: iconst_1
      //   938: istore #7
      //   940: goto -> 1213
      //   943: aload_0
      //   944: getfield mTouchWordDelta : F
      //   947: fstore_2
      //   948: aload_0
      //   949: aload #4
      //   951: iload #8
      //   953: fload_1
      //   954: fload_2
      //   955: fsub
      //   956: invokevirtual getOffsetAtCoordinate : (Landroid/text/Layout;IF)I
      //   959: istore #7
      //   961: aload_0
      //   962: invokespecial isStartHandle : ()Z
      //   965: ifeq -> 1001
      //   968: iload #7
      //   970: aload_0
      //   971: getfield mPreviousOffset : I
      //   974: if_icmpgt -> 995
      //   977: iload #8
      //   979: aload_0
      //   980: getfield mPrevLine : I
      //   983: if_icmple -> 989
      //   986: goto -> 995
      //   989: iconst_0
      //   990: istore #5
      //   992: goto -> 1031
      //   995: iconst_1
      //   996: istore #5
      //   998: goto -> 1031
      //   1001: iload #7
      //   1003: aload_0
      //   1004: getfield mPreviousOffset : I
      //   1007: if_icmplt -> 1028
      //   1010: iload #8
      //   1012: aload_0
      //   1013: getfield mPrevLine : I
      //   1016: if_icmpge -> 1022
      //   1019: goto -> 1028
      //   1022: iconst_0
      //   1023: istore #5
      //   1025: goto -> 1031
      //   1028: iconst_1
      //   1029: istore #5
      //   1031: iload #5
      //   1033: ifeq -> 1142
      //   1036: iload #8
      //   1038: aload_0
      //   1039: getfield mPrevLine : I
      //   1042: if_icmpeq -> 1132
      //   1045: aload_0
      //   1046: invokespecial isStartHandle : ()Z
      //   1049: ifeq -> 1059
      //   1052: iload #11
      //   1054: istore #5
      //   1056: goto -> 1063
      //   1059: iload #10
      //   1061: istore #5
      //   1063: aload_0
      //   1064: invokespecial isStartHandle : ()Z
      //   1067: ifeq -> 1077
      //   1070: iload #5
      //   1072: iload #9
      //   1074: if_icmplt -> 1091
      //   1077: aload_0
      //   1078: invokespecial isStartHandle : ()Z
      //   1081: ifne -> 1124
      //   1084: iload #5
      //   1086: iload #9
      //   1088: if_icmple -> 1124
      //   1091: aload_0
      //   1092: aload #4
      //   1094: iload #5
      //   1096: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   1099: fstore_2
      //   1100: aload_0
      //   1101: getfield this$0 : Landroid/widget/Editor;
      //   1104: astore #4
      //   1106: aload_0
      //   1107: aload #4
      //   1109: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   1112: fload_1
      //   1113: invokevirtual convertToLocalHorizontalCoordinate : (F)F
      //   1116: fload_2
      //   1117: fsub
      //   1118: putfield mTouchWordDelta : F
      //   1121: goto -> 1129
      //   1124: aload_0
      //   1125: fconst_0
      //   1126: putfield mTouchWordDelta : F
      //   1129: goto -> 1136
      //   1132: iload #7
      //   1134: istore #5
      //   1136: iconst_1
      //   1137: istore #7
      //   1139: goto -> 1213
      //   1142: aload_0
      //   1143: invokespecial isStartHandle : ()Z
      //   1146: ifeq -> 1158
      //   1149: iload #7
      //   1151: aload_0
      //   1152: getfield mPreviousOffset : I
      //   1155: if_icmplt -> 1174
      //   1158: aload_0
      //   1159: invokespecial isStartHandle : ()Z
      //   1162: ifne -> 1206
      //   1165: iload #7
      //   1167: aload_0
      //   1168: getfield mPreviousOffset : I
      //   1171: if_icmple -> 1206
      //   1174: aload_0
      //   1175: getfield this$0 : Landroid/widget/Editor;
      //   1178: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   1181: fload_1
      //   1182: invokevirtual convertToLocalHorizontalCoordinate : (F)F
      //   1185: fstore_2
      //   1186: aload_0
      //   1187: getfield mPreviousOffset : I
      //   1190: istore #5
      //   1192: aload_0
      //   1193: fload_2
      //   1194: aload_0
      //   1195: aload #4
      //   1197: iload #5
      //   1199: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   1202: fsub
      //   1203: putfield mTouchWordDelta : F
      //   1206: iconst_0
      //   1207: istore #7
      //   1209: iload #6
      //   1211: istore #5
      //   1213: iload #7
      //   1215: ifeq -> 1231
      //   1218: aload_0
      //   1219: iload #8
      //   1221: putfield mPreviousLineTouched : I
      //   1224: aload_0
      //   1225: iload #5
      //   1227: iload_3
      //   1228: invokespecial positionAndAdjustForCrossingHandles : (IZ)V
      //   1231: aload_0
      //   1232: fload_1
      //   1233: putfield mPrevX : F
      //   1236: return
      //   1237: aload_0
      //   1238: iconst_1
      //   1239: putfield mLanguageDirectionChanged : Z
      //   1242: aload_0
      //   1243: fconst_0
      //   1244: putfield mTouchWordDelta : F
      //   1247: aload_0
      //   1248: iload #6
      //   1250: iload_3
      //   1251: invokespecial positionAndAdjustForCrossingHandles : (IZ)V
      //   1254: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5840	-> 0
      //   #5841	-> 12
      //   #5844	-> 17
      //   #5846	-> 34
      //   #5849	-> 35
      //   #5850	-> 43
      //   #5853	-> 58
      //   #5855	-> 58
      //   #5856	-> 92
      //   #5857	-> 108
      //   #5859	-> 119
      //   #5860	-> 133
      //   #5863	-> 163
      //   #5864	-> 172
      //   #5867	-> 183
      //   #5868	-> 187
      //   #5869	-> 198
      //   #5871	-> 209
      //   #5872	-> 219
      //   #5875	-> 224
      //   #5876	-> 230
      //   #5877	-> 240
      //   #5878	-> 250
      //   #5883	-> 259
      //   #5890	-> 293
      //   #5893	-> 305
      //   #5894	-> 312
      //   #5895	-> 317
      //   #5896	-> 322
      //   #5900	-> 323
      //   #5901	-> 330
      //   #5902	-> 337
      //   #5904	-> 358
      //   #5906	-> 376
      //   #5907	-> 385
      //   #5909	-> 410
      //   #5912	-> 432
      //   #5913	-> 445
      //   #5914	-> 455
      //   #5915	-> 481
      //   #5916	-> 494
      //   #5917	-> 530
      //   #5918	-> 544
      //   #5923	-> 563
      //   #5924	-> 568
      //   #5925	-> 577
      //   #5926	-> 591
      //   #5927	-> 602
      //   #5928	-> 609
      //   #5915	-> 610
      //   #5913	-> 613
      //   #5912	-> 616
      //   #5932	-> 616
      //   #5934	-> 621
      //   #5935	-> 639
      //   #5936	-> 646
      //   #5937	-> 674
      //   #5938	-> 696
      //   #5942	-> 701
      //   #5943	-> 717
      //   #5944	-> 724
      //   #5946	-> 745
      //   #5947	-> 752
      //   #5948	-> 767
      //   #5949	-> 779
      //   #5953	-> 805
      //   #5949	-> 816
      //   #5954	-> 816
      //   #5958	-> 839
      //   #5960	-> 850
      //   #5938	-> 867
      //   #5963	-> 871
      //   #5964	-> 885
      //   #5965	-> 899
      //   #5966	-> 908
      //   #5967	-> 914
      //   #5968	-> 929
      //   #5964	-> 932
      //   #5969	-> 932
      //   #5971	-> 937
      //   #5972	-> 940
      //   #5973	-> 943
      //   #5974	-> 948
      //   #5975	-> 961
      //   #5976	-> 968
      //   #5977	-> 1001
      //   #5978	-> 1031
      //   #5980	-> 1036
      //   #5982	-> 1045
      //   #5983	-> 1063
      //   #5984	-> 1077
      //   #5985	-> 1091
      //   #5986	-> 1100
      //   #5987	-> 1106
      //   #5988	-> 1121
      //   #5984	-> 1124
      //   #5989	-> 1124
      //   #5994	-> 1129
      //   #5992	-> 1132
      //   #5994	-> 1136
      //   #5995	-> 1142
      //   #5996	-> 1158
      //   #5999	-> 1174
      //   #6000	-> 1192
      //   #6004	-> 1206
      //   #6005	-> 1218
      //   #6006	-> 1224
      //   #6008	-> 1231
      //   #6009	-> 1236
      //   #5883	-> 1237
      //   #5886	-> 1237
      //   #5887	-> 1242
      //   #5888	-> 1247
      //   #5889	-> 1254
    }
    
    protected void positionAtCursorOffset(int param1Int, boolean param1Boolean1, boolean param1Boolean2) {
      super.positionAtCursorOffset(param1Int, param1Boolean1, param1Boolean2);
      if (param1Int != -1 && !Editor.this.getWordIteratorWithText().isBoundary(param1Int)) {
        param1Boolean1 = true;
      } else {
        param1Boolean1 = false;
      } 
      this.mInWord = param1Boolean1;
    }
    
    public boolean onTouchEvent(MotionEvent param1MotionEvent) {
      if (!Editor.this.mTextView.isFromPrimePointer(param1MotionEvent, true))
        return true; 
      boolean bool = super.onTouchEvent(param1MotionEvent);
      int i = param1MotionEvent.getActionMasked();
      if (i != 0) {
        if (i != 1)
          if (i != 2) {
            if (i != 3)
              return bool; 
          } else {
            updateMagnifier(param1MotionEvent);
            return bool;
          }  
        dismissMagnifier();
      } else {
        this.mTouchWordDelta = 0.0F;
        this.mPrevX = -1.0F;
        updateMagnifier(param1MotionEvent);
      } 
      return bool;
    }
    
    private void positionAndAdjustForCrossingHandles(int param1Int, boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial isStartHandle : ()Z
      //   4: ifeq -> 21
      //   7: aload_0
      //   8: getfield this$0 : Landroid/widget/Editor;
      //   11: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   14: invokevirtual getSelectionEnd : ()I
      //   17: istore_3
      //   18: goto -> 32
      //   21: aload_0
      //   22: getfield this$0 : Landroid/widget/Editor;
      //   25: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   28: invokevirtual getSelectionStart : ()I
      //   31: istore_3
      //   32: aload_0
      //   33: invokespecial isStartHandle : ()Z
      //   36: ifeq -> 44
      //   39: iload_1
      //   40: iload_3
      //   41: if_icmpge -> 62
      //   44: iload_1
      //   45: istore #4
      //   47: aload_0
      //   48: invokespecial isStartHandle : ()Z
      //   51: ifne -> 238
      //   54: iload_1
      //   55: istore #4
      //   57: iload_1
      //   58: iload_3
      //   59: if_icmpgt -> 238
      //   62: aload_0
      //   63: fconst_0
      //   64: putfield mTouchWordDelta : F
      //   67: aload_0
      //   68: getfield this$0 : Landroid/widget/Editor;
      //   71: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   74: invokevirtual getLayout : ()Landroid/text/Layout;
      //   77: astore #5
      //   79: aload #5
      //   81: ifnull -> 222
      //   84: iload_1
      //   85: iload_3
      //   86: if_icmpeq -> 222
      //   89: aload_0
      //   90: aload #5
      //   92: iload_1
      //   93: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   96: fstore #6
      //   98: aload_0
      //   99: invokespecial isStartHandle : ()Z
      //   102: istore #7
      //   104: aload_0
      //   105: aload #5
      //   107: iload_3
      //   108: iload #7
      //   110: iconst_1
      //   111: ixor
      //   112: invokespecial getHorizontal : (Landroid/text/Layout;IZ)F
      //   115: fstore #8
      //   117: aload_0
      //   118: aload #5
      //   120: aload_0
      //   121: getfield mPreviousOffset : I
      //   124: invokevirtual getHorizontal : (Landroid/text/Layout;I)F
      //   127: fstore #9
      //   129: fload #9
      //   131: fload #8
      //   133: fcmpg
      //   134: ifge -> 145
      //   137: fload #6
      //   139: fload #8
      //   141: fcmpg
      //   142: iflt -> 161
      //   145: fload #9
      //   147: fload #8
      //   149: fcmpl
      //   150: ifle -> 222
      //   153: fload #6
      //   155: fload #8
      //   157: fcmpl
      //   158: ifle -> 222
      //   161: aload_0
      //   162: invokevirtual getCurrentCursorOffset : ()I
      //   165: istore_1
      //   166: aload_0
      //   167: invokespecial isStartHandle : ()Z
      //   170: ifeq -> 176
      //   173: goto -> 184
      //   176: iload_1
      //   177: iconst_1
      //   178: isub
      //   179: iconst_0
      //   180: invokestatic max : (II)I
      //   183: istore_1
      //   184: aload #5
      //   186: iload_1
      //   187: invokevirtual getRunRange : (I)J
      //   190: lstore #10
      //   192: aload_0
      //   193: invokespecial isStartHandle : ()Z
      //   196: ifeq -> 208
      //   199: lload #10
      //   201: invokestatic unpackRangeStartFromLong : (J)I
      //   204: istore_1
      //   205: goto -> 214
      //   208: lload #10
      //   210: invokestatic unpackRangeEndFromLong : (J)I
      //   213: istore_1
      //   214: aload_0
      //   215: iload_1
      //   216: iconst_0
      //   217: iload_2
      //   218: invokevirtual positionAtCursorOffset : (IZZ)V
      //   221: return
      //   222: aload_0
      //   223: getfield this$0 : Landroid/widget/Editor;
      //   226: iload_3
      //   227: aload_0
      //   228: invokespecial isStartHandle : ()Z
      //   231: iconst_1
      //   232: ixor
      //   233: invokestatic access$7100 : (Landroid/widget/Editor;IZ)I
      //   236: istore #4
      //   238: aload_0
      //   239: iload #4
      //   241: iconst_0
      //   242: iload_2
      //   243: invokevirtual positionAtCursorOffset : (IZZ)V
      //   246: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #6049	-> 0
      //   #6050	-> 32
      //   #6051	-> 44
      //   #6052	-> 62
      //   #6053	-> 67
      //   #6054	-> 79
      //   #6055	-> 89
      //   #6056	-> 98
      //   #6057	-> 98
      //   #6056	-> 104
      //   #6058	-> 117
      //   #6059	-> 129
      //   #6063	-> 161
      //   #6064	-> 166
      //   #6065	-> 173
      //   #6066	-> 184
      //   #6067	-> 192
      //   #6068	-> 199
      //   #6070	-> 208
      //   #6072	-> 214
      //   #6073	-> 221
      //   #6077	-> 222
      //   #6079	-> 238
      //   #6080	-> 246
    }
    
    private boolean positionNearEdgeOfScrollingView(float param1Float, boolean param1Boolean) {
      Editor.this.mTextView.getLocationOnScreen(this.mTextViewLocation);
      boolean bool = isStartHandle();
      boolean bool1 = true, bool2 = true;
      if (param1Boolean == bool) {
        int i = this.mTextViewLocation[0], j = Editor.this.mTextView.getWidth();
        Editor editor = Editor.this;
        int k = editor.mTextView.getPaddingRight();
        if (param1Float > (i + j - k) - this.mTextViewEdgeSlop) {
          param1Boolean = bool2;
        } else {
          param1Boolean = false;
        } 
      } else {
        int i = this.mTextViewLocation[0], j = Editor.this.mTextView.getPaddingLeft();
        if (param1Float < (i + j) + this.mTextViewEdgeSlop) {
          param1Boolean = bool1;
        } else {
          param1Boolean = false;
        } 
      } 
      return param1Boolean;
    }
    
    protected boolean isAtRtlRun(Layout param1Layout, int param1Int) {
      if (!isStartHandle())
        param1Int = Math.max(param1Int - 1, 0); 
      return param1Layout.isRtlCharAt(param1Int);
    }
    
    public float getHorizontal(Layout param1Layout, int param1Int) {
      return getHorizontal(param1Layout, param1Int, isStartHandle());
    }
    
    private float getHorizontal(Layout param1Layout, int param1Int, boolean param1Boolean) {
      int j;
      float f;
      int i = param1Layout.getLineForOffset(param1Int);
      boolean bool = false;
      if (param1Boolean) {
        j = param1Int;
      } else {
        j = Math.max(param1Int - 1, 0);
      } 
      boolean bool1 = param1Layout.isRtlCharAt(j);
      param1Boolean = bool;
      if (param1Layout.getParagraphDirection(i) == -1)
        param1Boolean = true; 
      if (bool1 == param1Boolean) {
        f = param1Layout.getPrimaryHorizontal(param1Int);
      } else {
        f = param1Layout.getSecondaryHorizontal(param1Int);
      } 
      return f;
    }
    
    protected int getOffsetAtCoordinate(Layout param1Layout, int param1Int, float param1Float) {
      param1Float = Editor.this.mTextView.convertToLocalHorizontalCoordinate(param1Float);
      boolean bool1 = true;
      int i = param1Layout.getOffsetForHorizontal(param1Int, param1Float, true);
      if (!param1Layout.isLevelBoundary(i))
        return i; 
      int j = param1Layout.getOffsetForHorizontal(param1Int, param1Float, false);
      int k = getCurrentCursorOffset();
      int m = Math.abs(i - k);
      int n = Math.abs(j - k);
      if (m < n)
        return i; 
      if (m > n)
        return j; 
      if (!isStartHandle())
        k = Math.max(k - 1, 0); 
      boolean bool2 = param1Layout.isRtlCharAt(k);
      if (param1Layout.getParagraphDirection(param1Int) != -1)
        bool1 = false; 
      if (bool2 == bool1) {
        param1Int = i;
      } else {
        param1Int = j;
      } 
      return param1Int;
    }
    
    protected int getMagnifierHandleTrigger() {
      byte b;
      if (isStartHandle()) {
        b = 1;
      } else {
        b = 2;
      } 
      return b;
    }
  }
  
  public void setLineChangeSlopMinMaxForTesting(int paramInt1, int paramInt2) {
    this.mLineChangeSlopMin = paramInt1;
    this.mLineChangeSlopMax = paramInt2;
  }
  
  public int getCurrentLineAdjustedForSlop(Layout paramLayout, int paramInt, float paramFloat) {
    int i = this.mTextView.getLineAtCoordinate(paramFloat);
    if (paramLayout == null || paramInt > paramLayout.getLineCount() || paramLayout.getLineCount() <= 0 || paramInt < 0)
      return i; 
    if (Math.abs(i - paramInt) >= 2)
      return i; 
    int j = paramLayout.getLineBottom(paramInt) - paramLayout.getLineTop(paramInt);
    int k = (int)((paramLayout.getLineBottom(i) - paramLayout.getLineTop(i)) * 0.5F);
    int m = this.mLineChangeSlopMin, n = this.mLineChangeSlopMax;
    n = Math.min(n, j + k);
    m = Math.max(m, n);
    j = Math.max(0, m - j);
    float f = this.mTextView.viewportToContentVerticalOffset();
    if (i > paramInt && paramFloat >= (paramLayout.getLineBottom(paramInt) + j) + f)
      return i; 
    if (i < paramInt && paramFloat <= (paramLayout.getLineTop(paramInt) - j) + f)
      return i; 
    return paramInt;
  }
  
  void loadCursorDrawable() {
    if (this.mDrawableForCursor == null)
      this.mDrawableForCursor = this.mTextView.getTextCursorDrawable(); 
  }
  
  class InsertionPointCursorController implements CursorController {
    private Editor.InsertionHandleView mHandle;
    
    private boolean mIsDraggingCursor;
    
    private boolean mIsTouchSnappedToHandleDuringDrag;
    
    private int mPrevLineDuringDrag;
    
    final Editor this$0;
    
    public void onTouchEvent(MotionEvent param1MotionEvent) {
      if (Editor.this.hasSelectionController() && Editor.this.getSelectionController().isCursorBeingModified())
        return; 
      int i = param1MotionEvent.getActionMasked();
      if (i != 1)
        if (i != 2) {
          if (i != 3)
            return; 
        } else {
          if (!param1MotionEvent.isFromSource(8194))
            if (this.mIsDraggingCursor) {
              performCursorDrag(param1MotionEvent);
            } else if (Editor.this.mFlagCursorDragFromAnywhereEnabled) {
              Editor editor = Editor.this;
              if (editor.mTextView.getLayout() != null) {
                editor = Editor.this;
                if (editor.mTextView.isFocused()) {
                  editor = Editor.this;
                  if (editor.mTouchState.isMovedEnoughForDrag()) {
                    editor = Editor.this;
                    if (!editor.mTouchState.isDragCloseToVertical())
                      startCursorDrag(param1MotionEvent); 
                  } 
                } 
              } 
            }  
          return;
        }  
      if (this.mIsDraggingCursor)
        endCursorDrag(param1MotionEvent); 
    }
    
    private void positionCursorDuringDrag(MotionEvent param1MotionEvent) {
      this.mPrevLineDuringDrag = getLineDuringDrag(param1MotionEvent);
      int i = Editor.this.mTextView.getOffsetAtCoordinate(this.mPrevLineDuringDrag, param1MotionEvent.getX());
      int j = Editor.this.mTextView.getSelectionStart();
      int k = Editor.this.mTextView.getSelectionEnd();
      if (i == j && i == k)
        return; 
      Selection.setSelection((Spannable)Editor.this.mTextView.getText(), i);
      Editor.this.updateCursorPosition();
      if (Editor.this.mHapticTextHandleEnabled)
        Editor.this.mTextView.performHapticFeedback(9); 
    }
    
    private int getLineDuringDrag(MotionEvent param1MotionEvent) {
      float f1;
      Layout layout = Editor.this.mTextView.getLayout();
      int i = this.mPrevLineDuringDrag;
      if (i == -1)
        return Editor.this.getCurrentLineAdjustedForSlop(layout, i, param1MotionEvent.getY()); 
      if (Editor.this.mTouchState.isOnHandle()) {
        f1 = param1MotionEvent.getRawY() - Editor.this.mTextView.getLocationOnScreen()[1];
      } else {
        f1 = param1MotionEvent.getY();
      } 
      float f2 = getHandle().getIdealFingerToCursorOffset();
      int j = Editor.this.getCurrentLineAdjustedForSlop(layout, this.mPrevLineDuringDrag, f1 - f2);
      if (this.mIsTouchSnappedToHandleDuringDrag)
        return j; 
      i = this.mPrevLineDuringDrag;
      if (j < i) {
        Editor editor = Editor.this;
        j = editor.getCurrentLineAdjustedForSlop(layout, i, f1);
        return Math.min(i, j);
      } 
      this.mIsTouchSnappedToHandleDuringDrag = true;
      return j;
    }
    
    private void startCursorDrag(MotionEvent param1MotionEvent) {
      this.mIsDraggingCursor = true;
      this.mIsTouchSnappedToHandleDuringDrag = false;
      this.mPrevLineDuringDrag = -1;
      Editor.this.mTextView.getParent().requestDisallowInterceptTouchEvent(true);
      Editor.this.mTextView.cancelLongPress();
      positionCursorDuringDrag(param1MotionEvent);
      show();
      getHandle().removeHiderCallback();
      getHandle().updateMagnifier(param1MotionEvent);
    }
    
    private void performCursorDrag(MotionEvent param1MotionEvent) {
      positionCursorDuringDrag(param1MotionEvent);
      getHandle().updateMagnifier(param1MotionEvent);
    }
    
    private void endCursorDrag(MotionEvent param1MotionEvent) {
      this.mIsDraggingCursor = false;
      this.mIsTouchSnappedToHandleDuringDrag = false;
      this.mPrevLineDuringDrag = -1;
      getHandle().dismissMagnifier();
      getHandle().hideAfterDelay();
      Editor.this.mTextView.getParent().requestDisallowInterceptTouchEvent(false);
    }
    
    public void show() {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual getHandle : ()Landroid/widget/Editor$InsertionHandleView;
      //   4: invokevirtual show : ()V
      //   7: invokestatic uptimeMillis : ()J
      //   10: lstore_1
      //   11: getstatic android/widget/TextView.sLastCutCopyOrTextChangedTime : J
      //   14: lstore_3
      //   15: aload_0
      //   16: getfield this$0 : Landroid/widget/Editor;
      //   19: invokestatic access$7400 : (Landroid/widget/Editor;)Ljava/lang/Runnable;
      //   22: ifnull -> 81
      //   25: aload_0
      //   26: getfield mIsDraggingCursor : Z
      //   29: ifne -> 63
      //   32: aload_0
      //   33: getfield this$0 : Landroid/widget/Editor;
      //   36: astore #5
      //   38: aload #5
      //   40: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   43: invokevirtual isMultiTap : ()Z
      //   46: ifne -> 63
      //   49: aload_0
      //   50: getfield this$0 : Landroid/widget/Editor;
      //   53: astore #5
      //   55: aload #5
      //   57: invokestatic access$7500 : (Landroid/widget/Editor;)Z
      //   60: ifeq -> 81
      //   63: aload_0
      //   64: getfield this$0 : Landroid/widget/Editor;
      //   67: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   70: aload_0
      //   71: getfield this$0 : Landroid/widget/Editor;
      //   74: invokestatic access$7400 : (Landroid/widget/Editor;)Ljava/lang/Runnable;
      //   77: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)Z
      //   80: pop
      //   81: aload_0
      //   82: getfield mIsDraggingCursor : Z
      //   85: ifne -> 203
      //   88: aload_0
      //   89: getfield this$0 : Landroid/widget/Editor;
      //   92: astore #5
      //   94: aload #5
      //   96: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   99: invokevirtual isMultiTap : ()Z
      //   102: ifne -> 203
      //   105: aload_0
      //   106: getfield this$0 : Landroid/widget/Editor;
      //   109: astore #5
      //   111: aload #5
      //   113: invokestatic access$7500 : (Landroid/widget/Editor;)Z
      //   116: ifne -> 203
      //   119: lload_1
      //   120: lload_3
      //   121: lsub
      //   122: ldc2_w 15000
      //   125: lcmp
      //   126: ifge -> 203
      //   129: aload_0
      //   130: getfield this$0 : Landroid/widget/Editor;
      //   133: invokestatic access$500 : (Landroid/widget/Editor;)Landroid/view/ActionMode;
      //   136: ifnonnull -> 203
      //   139: aload_0
      //   140: getfield this$0 : Landroid/widget/Editor;
      //   143: invokestatic access$7400 : (Landroid/widget/Editor;)Ljava/lang/Runnable;
      //   146: ifnonnull -> 165
      //   149: aload_0
      //   150: getfield this$0 : Landroid/widget/Editor;
      //   153: new android/widget/Editor$InsertionPointCursorController$1
      //   156: dup
      //   157: aload_0
      //   158: invokespecial <init> : (Landroid/widget/Editor$InsertionPointCursorController;)V
      //   161: invokestatic access$7402 : (Landroid/widget/Editor;Ljava/lang/Runnable;)Ljava/lang/Runnable;
      //   164: pop
      //   165: aload_0
      //   166: getfield this$0 : Landroid/widget/Editor;
      //   169: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   172: astore #5
      //   174: aload_0
      //   175: getfield this$0 : Landroid/widget/Editor;
      //   178: astore #6
      //   180: aload #6
      //   182: invokestatic access$7400 : (Landroid/widget/Editor;)Ljava/lang/Runnable;
      //   185: astore #6
      //   187: invokestatic getDoubleTapTimeout : ()I
      //   190: iconst_1
      //   191: iadd
      //   192: i2l
      //   193: lstore_3
      //   194: aload #5
      //   196: aload #6
      //   198: lload_3
      //   199: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
      //   202: pop
      //   203: aload_0
      //   204: getfield mIsDraggingCursor : Z
      //   207: ifne -> 217
      //   210: aload_0
      //   211: invokevirtual getHandle : ()Landroid/widget/Editor$InsertionHandleView;
      //   214: invokestatic access$7300 : (Landroid/widget/Editor$InsertionHandleView;)V
      //   217: aload_0
      //   218: getfield this$0 : Landroid/widget/Editor;
      //   221: getfield mSelectionModifierCursorController : Landroid/widget/Editor$SelectionModifierCursorController;
      //   224: ifnull -> 237
      //   227: aload_0
      //   228: getfield this$0 : Landroid/widget/Editor;
      //   231: getfield mSelectionModifierCursorController : Landroid/widget/Editor$SelectionModifierCursorController;
      //   234: invokevirtual hide : ()V
      //   237: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #6356	-> 0
      //   #6359	-> 7
      //   #6361	-> 15
      //   #6362	-> 25
      //   #6363	-> 38
      //   #6364	-> 55
      //   #6366	-> 63
      //   #6372	-> 81
      //   #6373	-> 94
      //   #6374	-> 111
      //   #6376	-> 129
      //   #6377	-> 139
      //   #6378	-> 149
      //   #6385	-> 165
      //   #6386	-> 180
      //   #6387	-> 187
      //   #6385	-> 194
      //   #6391	-> 203
      //   #6392	-> 210
      //   #6395	-> 217
      //   #6396	-> 227
      //   #6398	-> 237
    }
    
    public void hide() {
      Editor.InsertionHandleView insertionHandleView = this.mHandle;
      if (insertionHandleView != null)
        insertionHandleView.hide(); 
    }
    
    public void onTouchModeChanged(boolean param1Boolean) {
      if (!param1Boolean)
        hide(); 
    }
    
    public Editor.InsertionHandleView getHandle() {
      if (this.mHandle == null) {
        Editor.this.loadHandleDrawables(false);
        Editor editor = Editor.this;
        this.mHandle = new Editor.InsertionHandleView(editor, editor.mSelectHandleCenter);
      } 
      return this.mHandle;
    }
    
    private void reloadHandleDrawable() {
      Editor.InsertionHandleView insertionHandleView = this.mHandle;
      if (insertionHandleView == null)
        return; 
      insertionHandleView.setDrawables(Editor.this.mSelectHandleCenter, Editor.this.mSelectHandleCenter);
    }
    
    public void onDetached() {
      ViewTreeObserver viewTreeObserver = Editor.this.mTextView.getViewTreeObserver();
      viewTreeObserver.removeOnTouchModeChangeListener(this);
      Editor.InsertionHandleView insertionHandleView = this.mHandle;
      if (insertionHandleView != null)
        insertionHandleView.onDetached(); 
    }
    
    public boolean isCursorBeingModified() {
      if (!this.mIsDraggingCursor) {
        Editor.InsertionHandleView insertionHandleView = this.mHandle;
        return (insertionHandleView != null && insertionHandleView.isDragging());
      } 
      return true;
    }
    
    public boolean isActive() {
      boolean bool;
      Editor.InsertionHandleView insertionHandleView = this.mHandle;
      if (insertionHandleView != null && insertionHandleView.isShowing()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void invalidateHandle() {
      Editor.InsertionHandleView insertionHandleView = this.mHandle;
      if (insertionHandleView != null)
        insertionHandleView.invalidate(); 
    }
  }
  
  class SelectionModifierCursorController implements CursorController {
    private int mStartOffset = -1;
    
    private int mLineSelectionIsOn = -1;
    
    private boolean mSwitchedLines = false;
    
    private int mDragAcceleratorMode = 0;
    
    private static final int DRAG_ACCELERATOR_MODE_CHARACTER = 1;
    
    private static final int DRAG_ACCELERATOR_MODE_INACTIVE = 0;
    
    private static final int DRAG_ACCELERATOR_MODE_PARAGRAPH = 3;
    
    private static final int DRAG_ACCELERATOR_MODE_WORD = 2;
    
    private Editor.SelectionHandleView mEndHandle;
    
    private boolean mGestureStayedInTapRegion;
    
    private boolean mHaventMovedEnoughToStartDrag;
    
    private int mMaxTouchOffset;
    
    private int mMinTouchOffset;
    
    private Editor.SelectionHandleView mStartHandle;
    
    final Editor this$0;
    
    SelectionModifierCursorController() {
      resetTouchOffsets();
    }
    
    public void show() {
      if (Editor.this.mTextView.isInBatchEditMode())
        return; 
      Editor.this.loadHandleDrawables(false);
      initHandles();
    }
    
    private void initHandles() {
      if (this.mStartHandle == null) {
        Editor editor = Editor.this;
        this.mStartHandle = new Editor.SelectionHandleView(editor.mSelectHandleLeft, Editor.this.mSelectHandleRight, 16909407, 0);
      } 
      if (this.mEndHandle == null) {
        Editor editor = Editor.this;
        this.mEndHandle = new Editor.SelectionHandleView(editor.mSelectHandleRight, Editor.this.mSelectHandleLeft, 16909406, 1);
      } 
      this.mStartHandle.show();
      this.mEndHandle.show();
      Editor.this.hideInsertionPointCursorController();
    }
    
    private void reloadHandleDrawables() {
      Editor.SelectionHandleView selectionHandleView = this.mStartHandle;
      if (selectionHandleView == null)
        return; 
      selectionHandleView.setDrawables(Editor.this.mSelectHandleLeft, Editor.this.mSelectHandleRight);
      this.mEndHandle.setDrawables(Editor.this.mSelectHandleRight, Editor.this.mSelectHandleLeft);
    }
    
    public void hide() {
      Editor.SelectionHandleView selectionHandleView = this.mStartHandle;
      if (selectionHandleView != null)
        selectionHandleView.hide(); 
      selectionHandleView = this.mEndHandle;
      if (selectionHandleView != null)
        selectionHandleView.hide(); 
    }
    
    public void enterDrag(int param1Int) {
      show();
      this.mDragAcceleratorMode = param1Int;
      TextView textView = Editor.this.mTextView;
      float f1 = Editor.this.mTouchState.getLastDownX();
      Editor editor = Editor.this;
      float f2 = editor.mTouchState.getLastDownY();
      this.mStartOffset = textView.getOffsetForPosition(f1, f2);
      this.mLineSelectionIsOn = Editor.this.mTextView.getLineAtCoordinate(Editor.this.mTouchState.getLastDownY());
      hide();
      Editor.this.mTextView.getParent().requestDisallowInterceptTouchEvent(true);
      Editor.this.mTextView.cancelLongPress();
    }
    
    public void onTouchEvent(MotionEvent param1MotionEvent) {
      // Byte code:
      //   0: aload_1
      //   1: invokevirtual getX : ()F
      //   4: fstore_2
      //   5: aload_1
      //   6: invokevirtual getY : ()F
      //   9: fstore_3
      //   10: aload_1
      //   11: sipush #8194
      //   14: invokevirtual isFromSource : (I)Z
      //   17: istore #4
      //   19: aload_1
      //   20: invokevirtual getActionMasked : ()I
      //   23: istore #5
      //   25: iload #5
      //   27: ifeq -> 420
      //   30: iload #5
      //   32: iconst_1
      //   33: if_icmpeq -> 358
      //   36: iload #5
      //   38: iconst_2
      //   39: if_icmpeq -> 88
      //   42: iload #5
      //   44: iconst_5
      //   45: if_icmpeq -> 58
      //   48: iload #5
      //   50: bipush #6
      //   52: if_icmpeq -> 58
      //   55: goto -> 579
      //   58: aload_0
      //   59: getfield this$0 : Landroid/widget/Editor;
      //   62: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   65: invokevirtual getContext : ()Landroid/content/Context;
      //   68: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
      //   71: ldc_w 'android.hardware.touchscreen.multitouch.distinct'
      //   74: invokevirtual hasSystemFeature : (Ljava/lang/String;)Z
      //   77: ifeq -> 579
      //   80: aload_0
      //   81: aload_1
      //   82: invokespecial updateMinAndMaxOffsets : (Landroid/view/MotionEvent;)V
      //   85: goto -> 579
      //   88: aload_0
      //   89: getfield mGestureStayedInTapRegion : Z
      //   92: ifeq -> 168
      //   95: aload_0
      //   96: getfield this$0 : Landroid/widget/Editor;
      //   99: astore #6
      //   101: aload #6
      //   103: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   106: invokevirtual getContext : ()Landroid/content/Context;
      //   109: astore #6
      //   111: aload #6
      //   113: invokestatic get : (Landroid/content/Context;)Landroid/view/ViewConfiguration;
      //   116: astore #7
      //   118: aload_0
      //   119: getfield this$0 : Landroid/widget/Editor;
      //   122: astore #6
      //   124: aload #6
      //   126: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   129: invokevirtual getLastDownX : ()F
      //   132: fstore #8
      //   134: aload_0
      //   135: getfield this$0 : Landroid/widget/Editor;
      //   138: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   141: invokevirtual getLastDownY : ()F
      //   144: fstore #9
      //   146: aload #7
      //   148: invokevirtual getScaledDoubleTapTouchSlop : ()I
      //   151: istore #5
      //   153: aload_0
      //   154: fload #8
      //   156: fload #9
      //   158: fload_2
      //   159: fload_3
      //   160: iload #5
      //   162: invokestatic isDistanceWithin : (FFFFI)Z
      //   165: putfield mGestureStayedInTapRegion : Z
      //   168: aload_0
      //   169: getfield mHaventMovedEnoughToStartDrag : Z
      //   172: ifeq -> 191
      //   175: aload_0
      //   176: aload_0
      //   177: getfield this$0 : Landroid/widget/Editor;
      //   180: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   183: invokevirtual isMovedEnoughForDrag : ()Z
      //   186: iconst_1
      //   187: ixor
      //   188: putfield mHaventMovedEnoughToStartDrag : Z
      //   191: iload #4
      //   193: ifeq -> 328
      //   196: aload_0
      //   197: invokevirtual isDragAcceleratorActive : ()Z
      //   200: ifne -> 328
      //   203: aload_0
      //   204: getfield this$0 : Landroid/widget/Editor;
      //   207: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   210: fload_2
      //   211: fload_3
      //   212: invokevirtual getOffsetForPosition : (FF)I
      //   215: istore #5
      //   217: aload_0
      //   218: getfield this$0 : Landroid/widget/Editor;
      //   221: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   224: invokevirtual hasSelection : ()Z
      //   227: ifeq -> 294
      //   230: aload_0
      //   231: getfield mHaventMovedEnoughToStartDrag : Z
      //   234: ifeq -> 246
      //   237: aload_0
      //   238: getfield mStartOffset : I
      //   241: iload #5
      //   243: if_icmpeq -> 294
      //   246: aload_0
      //   247: getfield this$0 : Landroid/widget/Editor;
      //   250: astore #6
      //   252: iload #5
      //   254: aload #6
      //   256: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   259: invokevirtual getSelectionStart : ()I
      //   262: if_icmplt -> 294
      //   265: aload_0
      //   266: getfield this$0 : Landroid/widget/Editor;
      //   269: astore #6
      //   271: iload #5
      //   273: aload #6
      //   275: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   278: invokevirtual getSelectionEnd : ()I
      //   281: if_icmpgt -> 294
      //   284: aload_0
      //   285: getfield this$0 : Landroid/widget/Editor;
      //   288: invokestatic access$7800 : (Landroid/widget/Editor;)V
      //   291: goto -> 579
      //   294: aload_0
      //   295: getfield mStartOffset : I
      //   298: iload #5
      //   300: if_icmpeq -> 328
      //   303: aload_0
      //   304: getfield this$0 : Landroid/widget/Editor;
      //   307: invokevirtual stopTextActionMode : ()V
      //   310: aload_0
      //   311: iconst_1
      //   312: invokevirtual enterDrag : (I)V
      //   315: aload_0
      //   316: getfield this$0 : Landroid/widget/Editor;
      //   319: iconst_1
      //   320: putfield mDiscardNextActionUp : Z
      //   323: aload_0
      //   324: iconst_0
      //   325: putfield mHaventMovedEnoughToStartDrag : Z
      //   328: aload_0
      //   329: getfield mStartHandle : Landroid/widget/Editor$SelectionHandleView;
      //   332: astore #6
      //   334: aload #6
      //   336: ifnull -> 350
      //   339: aload #6
      //   341: invokevirtual isShowing : ()Z
      //   344: ifeq -> 350
      //   347: goto -> 579
      //   350: aload_0
      //   351: aload_1
      //   352: invokespecial updateSelection : (Landroid/view/MotionEvent;)V
      //   355: goto -> 579
      //   358: aload_0
      //   359: invokevirtual isDragAcceleratorActive : ()Z
      //   362: ifne -> 368
      //   365: goto -> 579
      //   368: aload_0
      //   369: aload_1
      //   370: invokespecial updateSelection : (Landroid/view/MotionEvent;)V
      //   373: aload_0
      //   374: getfield this$0 : Landroid/widget/Editor;
      //   377: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   380: invokevirtual getParent : ()Landroid/view/ViewParent;
      //   383: iconst_0
      //   384: invokeinterface requestDisallowInterceptTouchEvent : (Z)V
      //   389: aload_0
      //   390: invokespecial resetDragAcceleratorState : ()V
      //   393: aload_0
      //   394: getfield this$0 : Landroid/widget/Editor;
      //   397: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   400: invokevirtual hasSelection : ()Z
      //   403: ifeq -> 579
      //   406: aload_0
      //   407: getfield this$0 : Landroid/widget/Editor;
      //   410: aload_0
      //   411: getfield mHaventMovedEnoughToStartDrag : Z
      //   414: invokevirtual startSelectionActionModeAsync : (Z)V
      //   417: goto -> 579
      //   420: aload_0
      //   421: getfield this$0 : Landroid/widget/Editor;
      //   424: invokestatic access$2100 : (Landroid/widget/Editor;)Z
      //   427: ifeq -> 437
      //   430: aload_0
      //   431: invokevirtual hide : ()V
      //   434: goto -> 579
      //   437: aload_0
      //   438: getfield this$0 : Landroid/widget/Editor;
      //   441: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   444: fload_2
      //   445: fload_3
      //   446: invokevirtual getOffsetForPosition : (FF)I
      //   449: istore #5
      //   451: aload_0
      //   452: iload #5
      //   454: putfield mMaxTouchOffset : I
      //   457: aload_0
      //   458: iload #5
      //   460: putfield mMinTouchOffset : I
      //   463: aload_0
      //   464: getfield mGestureStayedInTapRegion : Z
      //   467: ifeq -> 569
      //   470: aload_0
      //   471: getfield this$0 : Landroid/widget/Editor;
      //   474: astore_1
      //   475: aload_1
      //   476: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   479: invokevirtual isMultiTapInSameArea : ()Z
      //   482: ifeq -> 569
      //   485: iload #4
      //   487: ifne -> 519
      //   490: aload_0
      //   491: getfield this$0 : Landroid/widget/Editor;
      //   494: astore_1
      //   495: aload_1
      //   496: fload_2
      //   497: fload_3
      //   498: invokestatic access$7600 : (Landroid/widget/Editor;FF)Z
      //   501: ifne -> 519
      //   504: aload_0
      //   505: getfield this$0 : Landroid/widget/Editor;
      //   508: astore_1
      //   509: aload_1
      //   510: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   513: invokevirtual isOnHandle : ()Z
      //   516: ifeq -> 569
      //   519: aload_0
      //   520: getfield this$0 : Landroid/widget/Editor;
      //   523: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   526: invokevirtual isDoubleTap : ()Z
      //   529: ifeq -> 543
      //   532: aload_0
      //   533: getfield this$0 : Landroid/widget/Editor;
      //   536: invokestatic access$7700 : (Landroid/widget/Editor;)Z
      //   539: pop
      //   540: goto -> 561
      //   543: aload_0
      //   544: getfield this$0 : Landroid/widget/Editor;
      //   547: invokestatic access$6600 : (Landroid/widget/Editor;)Landroid/widget/EditorTouchState;
      //   550: invokevirtual isTripleClick : ()Z
      //   553: ifeq -> 561
      //   556: aload_0
      //   557: invokespecial selectCurrentParagraphAndStartDrag : ()Z
      //   560: pop
      //   561: aload_0
      //   562: getfield this$0 : Landroid/widget/Editor;
      //   565: iconst_1
      //   566: putfield mDiscardNextActionUp : Z
      //   569: aload_0
      //   570: iconst_1
      //   571: putfield mGestureStayedInTapRegion : Z
      //   574: aload_0
      //   575: iconst_1
      //   576: putfield mHaventMovedEnoughToStartDrag : Z
      //   579: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #6557	-> 0
      //   #6558	-> 5
      //   #6559	-> 10
      //   #6560	-> 19
      //   #6595	-> 58
      //   #6597	-> 80
      //   #6602	-> 88
      //   #6603	-> 95
      //   #6604	-> 101
      //   #6603	-> 111
      //   #6605	-> 118
      //   #6606	-> 124
      //   #6607	-> 146
      //   #6605	-> 153
      //   #6610	-> 168
      //   #6611	-> 175
      //   #6614	-> 191
      //   #6615	-> 203
      //   #6616	-> 217
      //   #6618	-> 252
      //   #6619	-> 271
      //   #6620	-> 284
      //   #6621	-> 291
      //   #6624	-> 294
      //   #6626	-> 303
      //   #6627	-> 310
      //   #6628	-> 315
      //   #6629	-> 323
      //   #6633	-> 328
      //   #6635	-> 347
      //   #6638	-> 350
      //   #6639	-> 355
      //   #6645	-> 358
      //   #6646	-> 365
      //   #6648	-> 368
      //   #6651	-> 373
      //   #6654	-> 389
      //   #6656	-> 393
      //   #6658	-> 406
      //   #6562	-> 420
      //   #6564	-> 430
      //   #6567	-> 437
      //   #6571	-> 463
      //   #6572	-> 475
      //   #6573	-> 495
      //   #6574	-> 509
      //   #6579	-> 519
      //   #6580	-> 532
      //   #6581	-> 543
      //   #6582	-> 556
      //   #6584	-> 561
      //   #6586	-> 569
      //   #6587	-> 574
      //   #6589	-> 579
      //   #6662	-> 579
    }
    
    private void updateSelection(MotionEvent param1MotionEvent) {
      if (Editor.this.mTextView.getLayout() != null) {
        int i = this.mDragAcceleratorMode;
        if (i != 1) {
          if (i != 2) {
            if (i == 3)
              updateParagraphBasedSelection(param1MotionEvent); 
          } else {
            updateWordBasedSelection(param1MotionEvent);
          } 
        } else {
          updateCharacterBasedSelection(param1MotionEvent);
        } 
      } 
    }
    
    private boolean selectCurrentParagraphAndStartDrag() {
      if (Editor.this.mInsertionActionModeRunnable != null)
        Editor.this.mTextView.removeCallbacks(Editor.this.mInsertionActionModeRunnable); 
      Editor.this.stopTextActionMode();
      if (!Editor.this.selectCurrentParagraph())
        return false; 
      enterDrag(3);
      return true;
    }
    
    private void updateCharacterBasedSelection(MotionEvent param1MotionEvent) {
      int i = Editor.this.mTextView.getOffsetForPosition(param1MotionEvent.getX(), param1MotionEvent.getY());
      int j = this.mStartOffset;
      boolean bool = param1MotionEvent.isFromSource(4098);
      updateSelectionInternal(j, i, bool);
    }
    
    private void updateWordBasedSelection(MotionEvent param1MotionEvent) {
      int i, k;
      if (this.mHaventMovedEnoughToStartDrag)
        return; 
      boolean bool = param1MotionEvent.isFromSource(8194);
      Editor editor = Editor.this;
      Context context = editor.mTextView.getContext();
      ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
      float f1 = param1MotionEvent.getX();
      float f2 = param1MotionEvent.getY();
      if (bool) {
        i = Editor.this.mTextView.getLineAtCoordinate(f2);
      } else {
        float f = f2;
        if (this.mSwitchedLines) {
          int m = viewConfiguration.getScaledTouchSlop();
          Editor.SelectionHandleView selectionHandleView = this.mStartHandle;
          if (selectionHandleView != null) {
            f = selectionHandleView.getIdealVerticalOffset();
          } else {
            f = m;
          } 
          f = f2 - f;
        } 
        Editor editor1 = Editor.this;
        i = editor1.getCurrentLineAdjustedForSlop(editor1.mTextView.getLayout(), this.mLineSelectionIsOn, f);
        if (!this.mSwitchedLines && i != this.mLineSelectionIsOn) {
          this.mSwitchedLines = true;
          return;
        } 
      } 
      int j = Editor.this.mTextView.getOffsetAtCoordinate(i, f1);
      if (this.mStartOffset < j) {
        j = Editor.this.getWordEnd(j);
        k = Editor.this.getWordStart(this.mStartOffset);
      } else {
        int m = Editor.this.getWordStart(j);
        int n = Editor.this.getWordEnd(this.mStartOffset);
        j = m;
        k = n;
        if (n == m) {
          j = Editor.this.getNextCursorOffset(m, false);
          k = n;
        } 
      } 
      this.mLineSelectionIsOn = i;
      bool = param1MotionEvent.isFromSource(4098);
      updateSelectionInternal(k, j, bool);
    }
    
    private void updateParagraphBasedSelection(MotionEvent param1MotionEvent) {
      int i = Editor.this.mTextView.getOffsetForPosition(param1MotionEvent.getX(), param1MotionEvent.getY());
      int j = Math.min(i, this.mStartOffset);
      i = Math.max(i, this.mStartOffset);
      long l = Editor.this.getParagraphsRange(j, i);
      j = TextUtils.unpackRangeStartFromLong(l);
      i = TextUtils.unpackRangeEndFromLong(l);
      boolean bool = param1MotionEvent.isFromSource(4098);
      updateSelectionInternal(j, i, bool);
    }
    
    private void updateSelectionInternal(int param1Int1, int param1Int2, boolean param1Boolean) {
      // Byte code:
      //   0: iload_3
      //   1: ifeq -> 56
      //   4: aload_0
      //   5: getfield this$0 : Landroid/widget/Editor;
      //   8: invokestatic access$4700 : (Landroid/widget/Editor;)Z
      //   11: ifeq -> 56
      //   14: aload_0
      //   15: getfield this$0 : Landroid/widget/Editor;
      //   18: astore #4
      //   20: aload #4
      //   22: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   25: invokevirtual getSelectionStart : ()I
      //   28: iload_1
      //   29: if_icmpne -> 50
      //   32: aload_0
      //   33: getfield this$0 : Landroid/widget/Editor;
      //   36: astore #4
      //   38: aload #4
      //   40: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   43: invokevirtual getSelectionEnd : ()I
      //   46: iload_2
      //   47: if_icmpeq -> 56
      //   50: iconst_1
      //   51: istore #5
      //   53: goto -> 59
      //   56: iconst_0
      //   57: istore #5
      //   59: aload_0
      //   60: getfield this$0 : Landroid/widget/Editor;
      //   63: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   66: invokevirtual getText : ()Ljava/lang/CharSequence;
      //   69: checkcast android/text/Spannable
      //   72: iload_1
      //   73: iload_2
      //   74: invokestatic setSelection : (Landroid/text/Spannable;II)V
      //   77: iload #5
      //   79: ifeq -> 95
      //   82: aload_0
      //   83: getfield this$0 : Landroid/widget/Editor;
      //   86: invokestatic access$300 : (Landroid/widget/Editor;)Landroid/widget/TextView;
      //   89: bipush #9
      //   91: invokevirtual performHapticFeedback : (I)Z
      //   94: pop
      //   95: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #6776	-> 0
      //   #6777	-> 20
      //   #6778	-> 38
      //   #6779	-> 59
      //   #6780	-> 77
      //   #6781	-> 82
      //   #6783	-> 95
    }
    
    private void updateMinAndMaxOffsets(MotionEvent param1MotionEvent) {
      int i = param1MotionEvent.getPointerCount();
      for (byte b = 0; b < i; b++) {
        int j = Editor.this.mTextView.getOffsetForPosition(param1MotionEvent.getX(b), param1MotionEvent.getY(b));
        if (j < this.mMinTouchOffset)
          this.mMinTouchOffset = j; 
        if (j > this.mMaxTouchOffset)
          this.mMaxTouchOffset = j; 
      } 
    }
    
    public int getMinTouchOffset() {
      return this.mMinTouchOffset;
    }
    
    public int getMaxTouchOffset() {
      return this.mMaxTouchOffset;
    }
    
    public void resetTouchOffsets() {
      this.mMaxTouchOffset = -1;
      this.mMinTouchOffset = -1;
      resetDragAcceleratorState();
    }
    
    private void resetDragAcceleratorState() {
      this.mStartOffset = -1;
      this.mDragAcceleratorMode = 0;
      this.mSwitchedLines = false;
      int i = Editor.this.mTextView.getSelectionStart();
      int j = Editor.this.mTextView.getSelectionEnd();
      if (i < 0 || j < 0) {
        Selection.removeSelection((Spannable)Editor.this.mTextView.getText());
        return;
      } 
      if (i > j)
        Selection.setSelection((Spannable)Editor.this.mTextView.getText(), j, i); 
    }
    
    public boolean isSelectionStartDragged() {
      boolean bool;
      Editor.SelectionHandleView selectionHandleView = this.mStartHandle;
      if (selectionHandleView != null && selectionHandleView.isDragging()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isCursorBeingModified() {
      if (!isDragAcceleratorActive() && !isSelectionStartDragged()) {
        Editor.SelectionHandleView selectionHandleView = this.mEndHandle;
        return (selectionHandleView != null && selectionHandleView.isDragging());
      } 
      return true;
    }
    
    public boolean isDragAcceleratorActive() {
      boolean bool;
      if (this.mDragAcceleratorMode != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void onTouchModeChanged(boolean param1Boolean) {
      if (!param1Boolean)
        hide(); 
    }
    
    public void onDetached() {
      ViewTreeObserver viewTreeObserver = Editor.this.mTextView.getViewTreeObserver();
      viewTreeObserver.removeOnTouchModeChangeListener(this);
      Editor.SelectionHandleView selectionHandleView = this.mStartHandle;
      if (selectionHandleView != null)
        selectionHandleView.onDetached(); 
      selectionHandleView = this.mEndHandle;
      if (selectionHandleView != null)
        selectionHandleView.onDetached(); 
    }
    
    public boolean isActive() {
      boolean bool;
      Editor.SelectionHandleView selectionHandleView = this.mStartHandle;
      if (selectionHandleView != null && selectionHandleView.isShowing()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void invalidateHandles() {
      Editor.SelectionHandleView selectionHandleView = this.mStartHandle;
      if (selectionHandleView != null)
        selectionHandleView.invalidate(); 
      selectionHandleView = this.mEndHandle;
      if (selectionHandleView != null)
        selectionHandleView.invalidate(); 
    }
    
    public void setMinMaxOffset(int param1Int) {
      this.mMaxTouchOffset = param1Int;
      this.mMinTouchOffset = param1Int;
    }
  }
  
  void loadHandleDrawables(boolean paramBoolean) {
    if (this.mSelectHandleCenter == null || paramBoolean) {
      this.mSelectHandleCenter = this.mTextView.getTextSelectHandle();
      if (hasInsertionController())
        getInsertionController().reloadHandleDrawable(); 
    } 
    if (this.mSelectHandleLeft == null || this.mSelectHandleRight == null || paramBoolean) {
      this.mSelectHandleLeft = this.mTextView.getTextSelectHandleLeft();
      this.mSelectHandleRight = this.mTextView.getTextSelectHandleRight();
      if (hasSelectionController())
        getSelectionController().reloadHandleDrawables(); 
    } 
  }
  
  class CorrectionHighlighter {
    private static final int FADE_OUT_DURATION = 400;
    
    private int mEnd;
    
    private long mFadingStartTime;
    
    private final Paint mPaint;
    
    private final Path mPath = new Path();
    
    private int mStart;
    
    private RectF mTempRectF;
    
    final Editor this$0;
    
    public CorrectionHighlighter() {
      Paint paint = new Paint(1);
      float f = (Editor.this.mTextView.getResources().getCompatibilityInfo()).applicationScale;
      paint.setCompatibilityScaling(f);
      this.mPaint.setStyle(Paint.Style.FILL);
    }
    
    public void highlight(CorrectionInfo param1CorrectionInfo) {
      int i = param1CorrectionInfo.getOffset();
      this.mEnd = i + param1CorrectionInfo.getNewText().length();
      this.mFadingStartTime = SystemClock.uptimeMillis();
      if (this.mStart < 0 || this.mEnd < 0)
        stopAnimation(); 
    }
    
    public void draw(Canvas param1Canvas, int param1Int) {
      if (updatePath() && updatePaint()) {
        if (param1Int != 0)
          param1Canvas.translate(0.0F, param1Int); 
        param1Canvas.drawPath(this.mPath, this.mPaint);
        if (param1Int != 0)
          param1Canvas.translate(0.0F, -param1Int); 
        invalidate(true);
      } else {
        stopAnimation();
        invalidate(false);
      } 
    }
    
    private boolean updatePaint() {
      long l = SystemClock.uptimeMillis() - this.mFadingStartTime;
      if (l > 400L)
        return false; 
      float f = (float)l / 400.0F;
      int i = Color.alpha(Editor.this.mTextView.mHighlightColor);
      int j = Editor.this.mTextView.mHighlightColor;
      i = (int)(i * (1.0F - f));
      this.mPaint.setColor((j & 0xFFFFFF) + (i << 24));
      return true;
    }
    
    private boolean updatePath() {
      Layout layout = Editor.this.mTextView.getLayout();
      if (layout == null)
        return false; 
      int i = Editor.this.mTextView.getText().length();
      int j = Math.min(i, this.mStart);
      i = Math.min(i, this.mEnd);
      this.mPath.reset();
      layout.getSelectionPath(j, i, this.mPath);
      return true;
    }
    
    private void invalidate(boolean param1Boolean) {
      if (Editor.this.mTextView.getLayout() == null)
        return; 
      if (this.mTempRectF == null)
        this.mTempRectF = new RectF(); 
      this.mPath.computeBounds(this.mTempRectF, false);
      int i = Editor.this.mTextView.getCompoundPaddingLeft();
      int j = Editor.this.mTextView.getExtendedPaddingTop() + Editor.this.mTextView.getVerticalOffset(true);
      if (param1Boolean) {
        Editor.this.mTextView.postInvalidateOnAnimation((int)this.mTempRectF.left + i, (int)this.mTempRectF.top + j, (int)this.mTempRectF.right + i, (int)this.mTempRectF.bottom + j);
      } else {
        Editor.this.mTextView.postInvalidate((int)this.mTempRectF.left, (int)this.mTempRectF.top, (int)this.mTempRectF.right, (int)this.mTempRectF.bottom);
      } 
    }
    
    private void stopAnimation() {
      Editor.access$8302(Editor.this, null);
    }
  }
  
  class ErrorPopup extends PopupWindow {
    private boolean mAbove = false;
    
    private int mPopupInlineErrorBackgroundId = 0;
    
    private int mPopupInlineErrorAboveBackgroundId = 0;
    
    private final TextView mView;
    
    ErrorPopup(Editor this$0, int param1Int1, int param1Int2) {
      super((View)this$0, param1Int1, param1Int2);
      this.mView = (TextView)this$0;
      this.mPopupInlineErrorBackgroundId = param1Int1 = getResourceId(0, 297);
      this.mView.setBackgroundResource(param1Int1);
    }
    
    void fixDirection(boolean param1Boolean) {
      int i;
      this.mAbove = param1Boolean;
      if (param1Boolean) {
        i = this.mPopupInlineErrorAboveBackgroundId;
        this.mPopupInlineErrorAboveBackgroundId = getResourceId(i, 296);
      } else {
        this.mPopupInlineErrorBackgroundId = getResourceId(this.mPopupInlineErrorBackgroundId, 297);
      } 
      TextView textView = this.mView;
      if (param1Boolean) {
        i = this.mPopupInlineErrorAboveBackgroundId;
      } else {
        i = this.mPopupInlineErrorBackgroundId;
      } 
      textView.setBackgroundResource(i);
    }
    
    private int getResourceId(int param1Int1, int param1Int2) {
      int i = param1Int1;
      if (param1Int1 == 0) {
        TypedArray typedArray = this.mView.getContext().obtainStyledAttributes(R.styleable.Theme);
        i = typedArray.getResourceId(param1Int2, 0);
        typedArray.recycle();
      } 
      return i;
    }
    
    public void update(int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean) {
      super.update(param1Int1, param1Int2, param1Int3, param1Int4, param1Boolean);
      param1Boolean = isAboveAnchor();
      if (param1Boolean != this.mAbove)
        fixDirection(param1Boolean); 
    }
  }
  
  class InputContentType {
    boolean enterDown;
    
    Bundle extras;
    
    int imeActionId;
    
    CharSequence imeActionLabel;
    
    LocaleList imeHintLocales;
    
    int imeOptions = 0;
    
    TextView.OnEditorActionListener onEditorActionListener;
    
    String privateImeOptions;
  }
  
  class InputMethodState {
    int mBatchEditNesting;
    
    int mChangedDelta;
    
    int mChangedEnd;
    
    int mChangedStart;
    
    boolean mContentChanged;
    
    boolean mCursorChanged;
    
    final ExtractedText mExtractedText = new ExtractedText();
    
    ExtractedTextRequest mExtractedTextRequest;
    
    boolean mSelectionModeChanged;
  }
  
  private static boolean isValidRange(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    boolean bool;
    if (paramInt1 >= 0 && paramInt1 <= paramInt2 && paramInt2 <= paramCharSequence.length()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class UndoInputFilter implements InputFilter {
    private static final int MERGE_EDIT_MODE_FORCE_MERGE = 0;
    
    private static final int MERGE_EDIT_MODE_NEVER_MERGE = 1;
    
    private static final int MERGE_EDIT_MODE_NORMAL = 2;
    
    private final Editor mEditor;
    
    private boolean mExpanding;
    
    private boolean mHasComposition;
    
    private boolean mIsUserEdit;
    
    private boolean mPreviousOperationWasInSameBatchEdit;
    
    public UndoInputFilter(Editor this$0) {
      this.mEditor = this$0;
    }
    
    public void saveInstanceState(Parcel param1Parcel) {
      param1Parcel.writeInt(this.mIsUserEdit);
      param1Parcel.writeInt(this.mHasComposition);
      param1Parcel.writeInt(this.mExpanding);
      param1Parcel.writeInt(this.mPreviousOperationWasInSameBatchEdit);
    }
    
    public void restoreInstanceState(Parcel param1Parcel) {
      boolean bool2;
      int i = param1Parcel.readInt();
      boolean bool1 = true;
      if (i != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mIsUserEdit = bool2;
      if (param1Parcel.readInt() != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mHasComposition = bool2;
      if (param1Parcel.readInt() != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mExpanding = bool2;
      if (param1Parcel.readInt() != 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      this.mPreviousOperationWasInSameBatchEdit = bool2;
    }
    
    public void beginBatchEdit() {
      this.mIsUserEdit = true;
    }
    
    public void endBatchEdit() {
      this.mIsUserEdit = false;
      this.mPreviousOperationWasInSameBatchEdit = false;
    }
    
    public CharSequence filter(CharSequence param1CharSequence, int param1Int1, int param1Int2, Spanned param1Spanned, int param1Int3, int param1Int4) {
      if (!canUndoEdit(param1CharSequence, param1Int1, param1Int2, param1Spanned, param1Int3, param1Int4))
        return null; 
      boolean bool1 = this.mHasComposition;
      this.mHasComposition = isComposition(param1CharSequence);
      boolean bool2 = this.mExpanding;
      if (param1Int2 - param1Int1 != param1Int4 - param1Int3) {
        boolean bool3;
        if (param1Int2 - param1Int1 > param1Int4 - param1Int3) {
          bool3 = true;
        } else {
          bool3 = false;
        } 
        this.mExpanding = bool3;
        if (bool1 && bool3 != bool2) {
          bool3 = true;
          handleEdit(param1CharSequence, param1Int1, param1Int2, param1Spanned, param1Int3, param1Int4, bool3);
          return null;
        } 
      } 
      boolean bool = false;
      handleEdit(param1CharSequence, param1Int1, param1Int2, param1Spanned, param1Int3, param1Int4, bool);
      return null;
    }
    
    void freezeLastEdit() {
      this.mEditor.mUndoManager.beginUpdate("Edit text");
      Editor.EditOperation editOperation = getLastEdit();
      if (editOperation != null)
        Editor.EditOperation.access$8502(editOperation, true); 
      this.mEditor.mUndoManager.endUpdate();
    }
    
    private void handleEdit(CharSequence param1CharSequence, int param1Int1, int param1Int2, Spanned param1Spanned, int param1Int3, int param1Int4, boolean param1Boolean) {
      byte b;
      if (isInTextWatcher() || this.mPreviousOperationWasInSameBatchEdit) {
        b = 0;
      } else if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      param1CharSequence = TextUtils.substring(param1CharSequence, param1Int1, param1Int2);
      String str = TextUtils.substring(param1Spanned, param1Int3, param1Int4);
      Editor.EditOperation editOperation = new Editor.EditOperation(this.mEditor, str, param1Int3, (String)param1CharSequence, this.mHasComposition);
      if (this.mHasComposition && TextUtils.equals(editOperation.mNewText, editOperation.mOldText))
        return; 
      recordEdit(editOperation, b);
    }
    
    private Editor.EditOperation getLastEdit() {
      UndoManager undoManager = this.mEditor.mUndoManager;
      Editor editor = this.mEditor;
      UndoOwner undoOwner = editor.mUndoOwner;
      return (Editor.EditOperation)undoManager.getLastOperation(Editor.EditOperation.class, undoOwner, 1);
    }
    
    private void recordEdit(Editor.EditOperation param1EditOperation, int param1Int) {
      UndoManager undoManager = this.mEditor.mUndoManager;
      undoManager.beginUpdate("Edit text");
      Editor.EditOperation editOperation = getLastEdit();
      if (editOperation == null) {
        undoManager.addOperation(param1EditOperation, 0);
      } else if (param1Int == 0) {
        editOperation.forceMergeWith(param1EditOperation);
      } else if (!this.mIsUserEdit) {
        undoManager.commitState(this.mEditor.mUndoOwner);
        undoManager.addOperation(param1EditOperation, 0);
      } else if (param1Int != 2 || !editOperation.mergeWith(param1EditOperation)) {
        undoManager.commitState(this.mEditor.mUndoOwner);
        undoManager.addOperation(param1EditOperation, 0);
      } 
      this.mPreviousOperationWasInSameBatchEdit = this.mIsUserEdit;
      undoManager.endUpdate();
    }
    
    private boolean canUndoEdit(CharSequence param1CharSequence, int param1Int1, int param1Int2, Spanned param1Spanned, int param1Int3, int param1Int4) {
      if (!this.mEditor.mAllowUndo)
        return false; 
      if (this.mEditor.mUndoManager.isInUndo())
        return false; 
      if (!Editor.isValidRange(param1CharSequence, param1Int1, param1Int2) || !Editor.isValidRange(param1Spanned, param1Int3, param1Int4))
        return false; 
      if (param1Int1 == param1Int2 && param1Int3 == param1Int4)
        return false; 
      return true;
    }
    
    private static boolean isComposition(CharSequence param1CharSequence) {
      boolean bool = param1CharSequence instanceof Spannable;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1CharSequence = param1CharSequence;
      int i = EditableInputConnection.getComposingSpanStart((Spannable)param1CharSequence);
      int j = EditableInputConnection.getComposingSpanEnd((Spannable)param1CharSequence);
      if (i < j)
        bool1 = true; 
      return bool1;
    }
    
    private boolean isInTextWatcher() {
      CharSequence charSequence = this.mEditor.mTextView.getText();
      if (charSequence instanceof SpannableStringBuilder) {
        charSequence = charSequence;
        if (charSequence.getTextWatcherDepth() > 0)
          return true; 
      } 
      return false;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class MergeMode implements Annotation {}
  }
  
  class EditOperation extends UndoOperation<Editor> {
    public EditOperation(Editor this$0, String param1String1, int param1Int, String param1String2, boolean param1Boolean) {
      super(this$0.mUndoOwner);
      this.mOldText = param1String1;
      this.mNewText = param1String2;
      if (param1String2.length() > 0 && this.mOldText.length() == 0) {
        this.mType = 0;
      } else if (this.mNewText.length() == 0 && this.mOldText.length() > 0) {
        this.mType = 1;
      } else {
        this.mType = 2;
      } 
      this.mStart = param1Int;
      this.mOldCursorPos = this$0.mTextView.getSelectionStart();
      this.mNewCursorPos = this.mNewText.length() + param1Int;
      this.mIsComposition = param1Boolean;
    }
    
    public EditOperation(Editor this$0, ClassLoader param1ClassLoader) {
      super((Parcel)this$0, param1ClassLoader);
      this.mType = this$0.readInt();
      this.mOldText = this$0.readString();
      this.mNewText = this$0.readString();
      this.mStart = this$0.readInt();
      this.mOldCursorPos = this$0.readInt();
      this.mNewCursorPos = this$0.readInt();
      int i = this$0.readInt();
      boolean bool1 = false;
      if (i == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mFrozen = bool2;
      boolean bool2 = bool1;
      if (this$0.readInt() == 1)
        bool2 = true; 
      this.mIsComposition = bool2;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mType);
      param1Parcel.writeString(this.mOldText);
      param1Parcel.writeString(this.mNewText);
      param1Parcel.writeInt(this.mStart);
      param1Parcel.writeInt(this.mOldCursorPos);
      param1Parcel.writeInt(this.mNewCursorPos);
      param1Parcel.writeInt(this.mFrozen);
      param1Parcel.writeInt(this.mIsComposition);
    }
    
    private int getNewTextEnd() {
      return this.mStart + this.mNewText.length();
    }
    
    private int getOldTextEnd() {
      return this.mStart + this.mOldText.length();
    }
    
    public void commit() {}
    
    public void undo() {
      Editor editor = (Editor)getOwnerData();
      Editable editable = (Editable)editor.mTextView.getText();
      modifyText(editable, this.mStart, getNewTextEnd(), this.mOldText, this.mStart, this.mOldCursorPos);
    }
    
    public void redo() {
      Editor editor = (Editor)getOwnerData();
      Editable editable = (Editable)editor.mTextView.getText();
      modifyText(editable, this.mStart, getOldTextEnd(), this.mNewText, this.mStart, this.mNewCursorPos);
    }
    
    private boolean mergeWith(EditOperation param1EditOperation) {
      if (this.mFrozen)
        return false; 
      int i = this.mType;
      if (i != 0) {
        if (i != 1) {
          if (i != 2)
            return false; 
          return mergeReplaceWith(param1EditOperation);
        } 
        return mergeDeleteWith(param1EditOperation);
      } 
      return mergeInsertWith(param1EditOperation);
    }
    
    private boolean mergeInsertWith(EditOperation param1EditOperation) {
      int i = param1EditOperation.mType;
      if (i == 0) {
        if (getNewTextEnd() != param1EditOperation.mStart)
          return false; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mNewText);
        stringBuilder.append(param1EditOperation.mNewText);
        this.mNewText = stringBuilder.toString();
        this.mNewCursorPos = param1EditOperation.mNewCursorPos;
        this.mFrozen = param1EditOperation.mFrozen;
        this.mIsComposition = param1EditOperation.mIsComposition;
        return true;
      } 
      if (this.mIsComposition && i == 2 && this.mStart <= param1EditOperation.mStart && getNewTextEnd() >= param1EditOperation.getOldTextEnd()) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mNewText.substring(0, param1EditOperation.mStart - this.mStart));
        stringBuilder.append(param1EditOperation.mNewText);
        String str = this.mNewText;
        stringBuilder.append(str.substring(param1EditOperation.getOldTextEnd() - this.mStart, this.mNewText.length()));
        this.mNewText = stringBuilder.toString();
        this.mNewCursorPos = param1EditOperation.mNewCursorPos;
        this.mIsComposition = param1EditOperation.mIsComposition;
        return true;
      } 
      return false;
    }
    
    private boolean mergeDeleteWith(EditOperation param1EditOperation) {
      if (param1EditOperation.mType != 1)
        return false; 
      if (this.mStart != param1EditOperation.getOldTextEnd())
        return false; 
      this.mStart = param1EditOperation.mStart;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1EditOperation.mOldText);
      stringBuilder.append(this.mOldText);
      this.mOldText = stringBuilder.toString();
      this.mNewCursorPos = param1EditOperation.mNewCursorPos;
      this.mIsComposition = param1EditOperation.mIsComposition;
      return true;
    }
    
    private boolean mergeReplaceWith(EditOperation param1EditOperation) {
      if (param1EditOperation.mType == 0 && getNewTextEnd() == param1EditOperation.mStart) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mNewText);
        stringBuilder.append(param1EditOperation.mNewText);
        this.mNewText = stringBuilder.toString();
        this.mNewCursorPos = param1EditOperation.mNewCursorPos;
        return true;
      } 
      if (!this.mIsComposition)
        return false; 
      if (param1EditOperation.mType == 1 && this.mStart <= param1EditOperation.mStart && getNewTextEnd() >= param1EditOperation.getOldTextEnd()) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mNewText.substring(0, param1EditOperation.mStart - this.mStart));
        String str = this.mNewText;
        stringBuilder.append(str.substring(param1EditOperation.getOldTextEnd() - this.mStart, this.mNewText.length()));
        this.mNewText = str = stringBuilder.toString();
        if (str.isEmpty())
          this.mType = 1; 
        this.mNewCursorPos = param1EditOperation.mNewCursorPos;
        this.mIsComposition = param1EditOperation.mIsComposition;
        return true;
      } 
      if (param1EditOperation.mType == 2 && this.mStart == param1EditOperation.mStart) {
        String str1 = this.mNewText, str2 = param1EditOperation.mOldText;
        if (TextUtils.equals(str1, str2)) {
          this.mNewText = param1EditOperation.mNewText;
          this.mNewCursorPos = param1EditOperation.mNewCursorPos;
          this.mIsComposition = param1EditOperation.mIsComposition;
          return true;
        } 
      } 
      return false;
    }
    
    public void forceMergeWith(EditOperation param1EditOperation) {
      if (mergeWith(param1EditOperation))
        return; 
      Editor editor = (Editor)getOwnerData();
      Editable editable = (Editable)editor.mTextView.getText();
      SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(editable.toString());
      modifyText(spannableStringBuilder, this.mStart, getNewTextEnd(), this.mOldText, this.mStart, this.mOldCursorPos);
      editable = new SpannableStringBuilder(editable.toString());
      modifyText(editable, param1EditOperation.mStart, param1EditOperation.getOldTextEnd(), param1EditOperation.mNewText, param1EditOperation.mStart, param1EditOperation.mNewCursorPos);
      this.mType = 2;
      this.mNewText = editable.toString();
      this.mOldText = spannableStringBuilder.toString();
      this.mStart = 0;
      this.mNewCursorPos = param1EditOperation.mNewCursorPos;
      this.mIsComposition = param1EditOperation.mIsComposition;
    }
    
    private static void modifyText(Editable param1Editable, int param1Int1, int param1Int2, CharSequence param1CharSequence, int param1Int3, int param1Int4) {
      if (Editor.isValidRange(param1Editable, param1Int1, param1Int2) && param1Int3 <= param1Editable.length() - param1Int2 - param1Int1) {
        if (param1Int1 != param1Int2)
          param1Editable.delete(param1Int1, param1Int2); 
        if (param1CharSequence.length() != 0)
          param1Editable.insert(param1Int3, param1CharSequence); 
      } 
      if (param1Int4 >= 0 && param1Int4 <= param1Editable.length())
        Selection.setSelection(param1Editable, param1Int4); 
    }
    
    private String getTypeString() {
      int i = this.mType;
      if (i != 0) {
        if (i != 1) {
          if (i != 2)
            return ""; 
          return "replace";
        } 
        return "delete";
      } 
      return "insert";
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[mType=");
      stringBuilder.append(getTypeString());
      stringBuilder.append(", mOldText=");
      stringBuilder.append(this.mOldText);
      stringBuilder.append(", mNewText=");
      stringBuilder.append(this.mNewText);
      stringBuilder.append(", mStart=");
      stringBuilder.append(this.mStart);
      stringBuilder.append(", mOldCursorPos=");
      stringBuilder.append(this.mOldCursorPos);
      stringBuilder.append(", mNewCursorPos=");
      stringBuilder.append(this.mNewCursorPos);
      stringBuilder.append(", mFrozen=");
      stringBuilder.append(this.mFrozen);
      stringBuilder.append(", mIsComposition=");
      stringBuilder.append(this.mIsComposition);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.ClassLoaderCreator<EditOperation> CREATOR = new Parcelable.ClassLoaderCreator<EditOperation>() {
        public Editor.EditOperation createFromParcel(Parcel param2Parcel) {
          return new Editor.EditOperation(param2Parcel, null);
        }
        
        public Editor.EditOperation createFromParcel(Parcel param2Parcel, ClassLoader param2ClassLoader) {
          return new Editor.EditOperation(param2Parcel, param2ClassLoader);
        }
        
        public Editor.EditOperation[] newArray(int param2Int) {
          return new Editor.EditOperation[param2Int];
        }
      };
    
    private static final int TYPE_DELETE = 1;
    
    private static final int TYPE_INSERT = 0;
    
    private static final int TYPE_REPLACE = 2;
    
    private boolean mFrozen;
    
    private boolean mIsComposition;
    
    private int mNewCursorPos;
    
    private String mNewText;
    
    private int mOldCursorPos;
    
    private String mOldText;
    
    private int mStart;
    
    private int mType;
  }
  
  class ProcessTextIntentActionsHandler {
    private final SparseArray<Intent> mAccessibilityIntents = new SparseArray<>();
    
    private final SparseArray<AccessibilityNodeInfo.AccessibilityAction> mAccessibilityActions = new SparseArray<>();
    
    private final List<ResolveInfo> mSupportedActivities = new ArrayList<>();
    
    private final Context mContext;
    
    private final Editor mEditor;
    
    private final PackageManager mPackageManager;
    
    private final String mPackageName;
    
    private final TextView mTextView;
    
    private ProcessTextIntentActionsHandler(Editor this$0) {
      Objects.requireNonNull(Editor.this);
      this.mEditor = Editor.this = Editor.this;
      TextView textView = Editor.this.mTextView;
      Objects.requireNonNull(textView);
      this.mTextView = textView = textView;
      Context context = textView.getContext();
      Objects.requireNonNull(context);
      this.mContext = context = context;
      PackageManager packageManager = context.getPackageManager();
      Objects.requireNonNull(packageManager);
      this.mPackageManager = packageManager;
      String str = this.mContext.getPackageName();
      Objects.requireNonNull(str);
      this.mPackageName = str;
    }
    
    public void onInitializeMenu(Menu param1Menu) {
      loadSupportedActivities();
      int i = this.mSupportedActivities.size();
      for (byte b = 0; b < i; b++) {
        ResolveInfo resolveInfo = this.mSupportedActivities.get(b);
        IOplusFloatingToolbarUtil iOplusFloatingToolbarUtil = (IOplusFloatingToolbarUtil)OplusFeatureCache.getOrCreate(IOplusFloatingToolbarUtil.DEFAULT, new Object[0]);
        Intent intent = createProcessTextIntentForResolveInfo(resolveInfo);
        CharSequence charSequence = getLabel(resolveInfo);
        if (!iOplusFloatingToolbarUtil.setSearchMenuItem(b + 100, intent, charSequence, resolveInfo, param1Menu)) {
          CharSequence charSequence1 = getLabel(resolveInfo);
          MenuItem menuItem2 = param1Menu.add(0, 0, b + 100, charSequence1);
          MenuItem menuItem1 = menuItem2.setIntent(createProcessTextIntentForResolveInfo(resolveInfo));
          menuItem1.setShowAsAction(0);
        } 
      } 
    }
    
    public boolean performMenuItemAction(MenuItem param1MenuItem) {
      return fireIntent(param1MenuItem.getIntent());
    }
    
    public void initializeAccessibilityActions() {
      this.mAccessibilityIntents.clear();
      this.mAccessibilityActions.clear();
      byte b = 0;
      loadSupportedActivities();
      for (ResolveInfo resolveInfo : this.mSupportedActivities) {
        int i = b + 268435712;
        SparseArray<AccessibilityNodeInfo.AccessibilityAction> sparseArray = this.mAccessibilityActions;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction = new AccessibilityNodeInfo.AccessibilityAction(i, getLabel(resolveInfo));
        sparseArray.put(i, accessibilityAction);
        SparseArray<Intent> sparseArray1 = this.mAccessibilityIntents;
        Intent intent = createProcessTextIntentForResolveInfo(resolveInfo);
        sparseArray1.put(i, intent);
        b++;
      } 
    }
    
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      for (byte b = 0; b < this.mAccessibilityActions.size(); b++)
        param1AccessibilityNodeInfo.addAction(this.mAccessibilityActions.valueAt(b)); 
    }
    
    public boolean performAccessibilityAction(int param1Int) {
      return fireIntent(this.mAccessibilityIntents.get(param1Int));
    }
    
    private boolean fireIntent(Intent param1Intent) {
      if (param1Intent != null && "android.intent.action.PROCESS_TEXT".equals(param1Intent.getAction())) {
        String str = this.mTextView.getSelectedText();
        str = TextUtils.<String>trimToParcelableSize(str);
        param1Intent.putExtra("android.intent.extra.PROCESS_TEXT", str);
        Editor.access$4302(this.mEditor, true);
        this.mTextView.startActivityForResult(param1Intent, 100);
        return true;
      } 
      return false;
    }
    
    private void loadSupportedActivities() {
      this.mSupportedActivities.clear();
      if (!this.mContext.canStartActivityForResult())
        return; 
      PackageManager packageManager = this.mTextView.getContext().getPackageManager();
      List list = packageManager.queryIntentActivities(createProcessTextIntent(), 0);
      for (ResolveInfo resolveInfo : list) {
        if (isSupportedActivity(resolveInfo))
          this.mSupportedActivities.add(resolveInfo); 
      } 
    }
    
    private boolean isSupportedActivity(ResolveInfo param1ResolveInfo) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mPackageName : Ljava/lang/String;
      //   4: aload_1
      //   5: getfield activityInfo : Landroid/content/pm/ActivityInfo;
      //   8: getfield packageName : Ljava/lang/String;
      //   11: invokevirtual equals : (Ljava/lang/Object;)Z
      //   14: ifne -> 66
      //   17: aload_1
      //   18: getfield activityInfo : Landroid/content/pm/ActivityInfo;
      //   21: getfield exported : Z
      //   24: ifeq -> 61
      //   27: aload_1
      //   28: getfield activityInfo : Landroid/content/pm/ActivityInfo;
      //   31: getfield permission : Ljava/lang/String;
      //   34: ifnull -> 66
      //   37: aload_0
      //   38: getfield mContext : Landroid/content/Context;
      //   41: astore_2
      //   42: aload_1
      //   43: getfield activityInfo : Landroid/content/pm/ActivityInfo;
      //   46: getfield permission : Ljava/lang/String;
      //   49: astore_1
      //   50: aload_2
      //   51: aload_1
      //   52: invokevirtual checkSelfPermission : (Ljava/lang/String;)I
      //   55: ifne -> 61
      //   58: goto -> 66
      //   61: iconst_0
      //   62: istore_3
      //   63: goto -> 68
      //   66: iconst_1
      //   67: istore_3
      //   68: iload_3
      //   69: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #7727	-> 0
      //   #7730	-> 50
      //   #7727	-> 68
    }
    
    private Intent createProcessTextIntentForResolveInfo(ResolveInfo param1ResolveInfo) {
      Intent intent = createProcessTextIntent();
      TextView textView = this.mTextView;
      intent = intent.putExtra("android.intent.extra.PROCESS_TEXT_READONLY", textView.isTextEditable() ^ true);
      String str2 = param1ResolveInfo.activityInfo.packageName, str1 = param1ResolveInfo.activityInfo.name;
      return intent.setClassName(str2, str1);
    }
    
    private Intent createProcessTextIntent() {
      null = new Intent();
      null = null.setAction("android.intent.action.PROCESS_TEXT");
      return null.setType("text/plain");
    }
    
    private CharSequence getLabel(ResolveInfo param1ResolveInfo) {
      return param1ResolveInfo.loadLabel(this.mPackageManager);
    }
  }
  
  static void logCursor(String paramString1, String paramString2, Object... paramVarArgs) {
    if (paramString2 == null) {
      Log.d("Editor", paramString1);
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(String.format(paramString2, paramVarArgs));
      Log.d("Editor", stringBuilder.toString());
    } 
  }
  
  class CursorController implements ViewTreeObserver.OnTouchModeChangeListener {
    public abstract void hide();
    
    public abstract boolean isActive();
    
    public abstract boolean isCursorBeingModified();
    
    public abstract void onDetached();
    
    public abstract void show();
  }
  
  class EasyEditDeleteListener {
    public abstract void onDeleteClick(EasyEditSpan param1EasyEditSpan);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class HandleType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class MagnifierHandleTrigger implements Annotation {
    public static final int INSERTION = 0;
    
    public static final int SELECTION_END = 2;
    
    public static final int SELECTION_START = 1;
  }
  
  class TextActionMode implements Annotation {
    public static final int INSERTION = 1;
    
    public static final int SELECTION = 0;
    
    public static final int TEXT_LINK = 2;
  }
  
  private static interface TextViewPositionListener {
    void updatePosition(int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  private static @interface MergeMode {}
}
