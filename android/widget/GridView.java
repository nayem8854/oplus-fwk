package android.widget;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Trace;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.RemotableViewMethod;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.GridLayoutAnimationController;
import com.android.internal.R;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@RemoteView
public class GridView extends AbsListView {
  private int mNumColumns = -1;
  
  private int mHorizontalSpacing = 0;
  
  private int mVerticalSpacing = 0;
  
  private int mStretchMode = 2;
  
  private View mReferenceView = null;
  
  private View mReferenceViewInSelectedRow = null;
  
  private int mGravity = 8388611;
  
  private final Rect mTempRect = new Rect();
  
  public static final int AUTO_FIT = -1;
  
  public static final int NO_STRETCH = 0;
  
  public static final int STRETCH_COLUMN_WIDTH = 2;
  
  public static final int STRETCH_SPACING = 1;
  
  public static final int STRETCH_SPACING_UNIFORM = 3;
  
  private int mColumnWidth;
  
  private int mRequestedColumnWidth;
  
  private int mRequestedHorizontalSpacing;
  
  private int mRequestedNumColumns;
  
  public GridView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public GridView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842865);
  }
  
  public GridView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public GridView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.GridView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.GridView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    paramInt1 = typedArray.getDimensionPixelOffset(1, 0);
    setHorizontalSpacing(paramInt1);
    paramInt1 = typedArray.getDimensionPixelOffset(2, 0);
    setVerticalSpacing(paramInt1);
    paramInt1 = typedArray.getInt(3, 2);
    if (paramInt1 >= 0)
      setStretchMode(paramInt1); 
    paramInt1 = typedArray.getDimensionPixelOffset(4, -1);
    if (paramInt1 > 0)
      setColumnWidth(paramInt1); 
    paramInt1 = typedArray.getInt(5, 1);
    setNumColumns(paramInt1);
    paramInt1 = typedArray.getInt(0, -1);
    if (paramInt1 >= 0)
      setGravity(paramInt1); 
    typedArray.recycle();
  }
  
  public ListAdapter getAdapter() {
    return this.mAdapter;
  }
  
  @RemotableViewMethod(asyncImpl = "setRemoteViewsAdapterAsync")
  public void setRemoteViewsAdapter(Intent paramIntent) {
    super.setRemoteViewsAdapter(paramIntent);
  }
  
  public void setAdapter(ListAdapter paramListAdapter) {
    if (this.mAdapter != null && this.mDataSetObserver != null)
      this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver); 
    resetList();
    this.mRecycler.clear();
    this.mAdapter = paramListAdapter;
    this.mOldSelectedPosition = -1;
    this.mOldSelectedRowId = Long.MIN_VALUE;
    super.setAdapter(paramListAdapter);
    if (this.mAdapter != null) {
      int i;
      this.mOldItemCount = this.mItemCount;
      this.mItemCount = this.mAdapter.getCount();
      this.mDataChanged = true;
      checkFocus();
      this.mDataSetObserver = new AbsListView.AdapterDataSetObserver(this);
      this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
      this.mRecycler.setViewTypeCount(this.mAdapter.getViewTypeCount());
      if (this.mStackFromBottom) {
        i = lookForSelectablePosition(this.mItemCount - 1, false);
      } else {
        i = lookForSelectablePosition(0, true);
      } 
      setSelectedPositionInt(i);
      setNextSelectedPositionInt(i);
      checkSelectionChanged();
    } else {
      checkFocus();
      checkSelectionChanged();
    } 
    requestLayout();
  }
  
  int lookForSelectablePosition(int paramInt, boolean paramBoolean) {
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter == null || isInTouchMode())
      return -1; 
    if (paramInt < 0 || paramInt >= this.mItemCount)
      return -1; 
    return paramInt;
  }
  
  void fillGap(boolean paramBoolean) {
    int i = this.mNumColumns;
    int j = this.mVerticalSpacing;
    int k = getChildCount();
    if (paramBoolean) {
      int m = 0;
      if ((this.mGroupFlags & 0x22) == 34)
        m = getListPaddingTop(); 
      if (k > 0)
        m = getChildAt(k - 1).getBottom() + j; 
      int n = this.mFirstPosition + k;
      k = n;
      if (this.mStackFromBottom)
        k = n + i - 1; 
      fillDown(k, m);
      correctTooHigh(i, j, getChildCount());
    } else {
      int m = 0;
      if ((this.mGroupFlags & 0x22) == 34)
        m = getListPaddingBottom(); 
      if (k > 0) {
        m = getChildAt(0).getTop() - j;
      } else {
        m = getHeight() - m;
      } 
      k = this.mFirstPosition;
      if (!this.mStackFromBottom) {
        k -= i;
      } else {
        k--;
      } 
      fillUp(k, m);
      correctTooLow(i, j, getChildCount());
    } 
  }
  
  private View fillDown(int paramInt1, int paramInt2) {
    View view1 = null;
    int i = this.mBottom - this.mTop;
    View view2 = view1;
    int j = i, k = paramInt1, m = paramInt2;
    if ((this.mGroupFlags & 0x22) == 34) {
      j = i - this.mListPadding.bottom;
      m = paramInt2;
      k = paramInt1;
      view2 = view1;
    } 
    while (m < j && k < this.mItemCount) {
      view1 = makeRow(k, m, true);
      if (view1 != null)
        view2 = view1; 
      m = this.mReferenceView.getBottom() + this.mVerticalSpacing;
      k += this.mNumColumns;
    } 
    setVisibleRangeHint(this.mFirstPosition, this.mFirstPosition + getChildCount() - 1);
    return view2;
  }
  
  private View makeRow(int paramInt1, int paramInt2, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mColumnWidth : I
    //   4: istore #4
    //   6: aload_0
    //   7: getfield mHorizontalSpacing : I
    //   10: istore #5
    //   12: aload_0
    //   13: invokevirtual isLayoutRtl : ()Z
    //   16: istore #6
    //   18: iload #6
    //   20: ifeq -> 72
    //   23: aload_0
    //   24: invokevirtual getWidth : ()I
    //   27: istore #7
    //   29: aload_0
    //   30: getfield mListPadding : Landroid/graphics/Rect;
    //   33: getfield right : I
    //   36: istore #8
    //   38: aload_0
    //   39: getfield mStretchMode : I
    //   42: iconst_3
    //   43: if_icmpne -> 53
    //   46: iload #5
    //   48: istore #9
    //   50: goto -> 56
    //   53: iconst_0
    //   54: istore #9
    //   56: iload #7
    //   58: iload #8
    //   60: isub
    //   61: iload #4
    //   63: isub
    //   64: iload #9
    //   66: isub
    //   67: istore #7
    //   69: goto -> 106
    //   72: aload_0
    //   73: getfield mListPadding : Landroid/graphics/Rect;
    //   76: getfield left : I
    //   79: istore #7
    //   81: aload_0
    //   82: getfield mStretchMode : I
    //   85: iconst_3
    //   86: if_icmpne -> 96
    //   89: iload #5
    //   91: istore #9
    //   93: goto -> 99
    //   96: iconst_0
    //   97: istore #9
    //   99: iload #7
    //   101: iload #9
    //   103: iadd
    //   104: istore #7
    //   106: aload_0
    //   107: getfield mStackFromBottom : Z
    //   110: ifne -> 131
    //   113: iload_1
    //   114: aload_0
    //   115: getfield mNumColumns : I
    //   118: iadd
    //   119: aload_0
    //   120: getfield mItemCount : I
    //   123: invokestatic min : (II)I
    //   126: istore #9
    //   128: goto -> 202
    //   131: iload_1
    //   132: iconst_1
    //   133: iadd
    //   134: istore #9
    //   136: iconst_0
    //   137: iload_1
    //   138: aload_0
    //   139: getfield mNumColumns : I
    //   142: isub
    //   143: iconst_1
    //   144: iadd
    //   145: invokestatic max : (II)I
    //   148: istore_1
    //   149: aload_0
    //   150: getfield mNumColumns : I
    //   153: istore #10
    //   155: iload #9
    //   157: iload_1
    //   158: isub
    //   159: iload #10
    //   161: if_icmpge -> 202
    //   164: iload #6
    //   166: ifeq -> 175
    //   169: iconst_m1
    //   170: istore #8
    //   172: goto -> 178
    //   175: iconst_1
    //   176: istore #8
    //   178: iload #7
    //   180: iload #8
    //   182: iload #10
    //   184: iload #9
    //   186: iload_1
    //   187: isub
    //   188: isub
    //   189: iload #4
    //   191: iload #5
    //   193: iadd
    //   194: imul
    //   195: imul
    //   196: iadd
    //   197: istore #7
    //   199: goto -> 202
    //   202: aload_0
    //   203: invokevirtual shouldShowSelector : ()Z
    //   206: istore #11
    //   208: aload_0
    //   209: invokevirtual touchModeDrawsInPressedState : ()Z
    //   212: istore #12
    //   214: aload_0
    //   215: getfield mSelectedPosition : I
    //   218: istore #10
    //   220: iload #6
    //   222: ifeq -> 231
    //   225: iconst_m1
    //   226: istore #8
    //   228: goto -> 234
    //   231: iconst_1
    //   232: istore #8
    //   234: iload_1
    //   235: istore #13
    //   237: aconst_null
    //   238: astore #14
    //   240: aconst_null
    //   241: astore #15
    //   243: iload #13
    //   245: iload #9
    //   247: if_icmpge -> 368
    //   250: iload #13
    //   252: iload #10
    //   254: if_icmpne -> 263
    //   257: iconst_1
    //   258: istore #6
    //   260: goto -> 266
    //   263: iconst_0
    //   264: istore #6
    //   266: iload_3
    //   267: ifeq -> 276
    //   270: iconst_m1
    //   271: istore #16
    //   273: goto -> 282
    //   276: iload #13
    //   278: iload_1
    //   279: isub
    //   280: istore #16
    //   282: aload_0
    //   283: iload #13
    //   285: iload_2
    //   286: iload_3
    //   287: iload #7
    //   289: iload #6
    //   291: iload #16
    //   293: invokespecial makeAndAddView : (IIZIZI)Landroid/view/View;
    //   296: astore #15
    //   298: iload #7
    //   300: iload #8
    //   302: iload #4
    //   304: imul
    //   305: iadd
    //   306: istore #16
    //   308: iload #16
    //   310: istore #7
    //   312: iload #13
    //   314: iload #9
    //   316: iconst_1
    //   317: isub
    //   318: if_icmpge -> 331
    //   321: iload #16
    //   323: iload #8
    //   325: iload #5
    //   327: imul
    //   328: iadd
    //   329: istore #7
    //   331: aload #14
    //   333: astore #17
    //   335: iload #6
    //   337: ifeq -> 358
    //   340: iload #11
    //   342: ifne -> 354
    //   345: aload #14
    //   347: astore #17
    //   349: iload #12
    //   351: ifeq -> 358
    //   354: aload #15
    //   356: astore #17
    //   358: iinc #13, 1
    //   361: aload #17
    //   363: astore #14
    //   365: goto -> 243
    //   368: aload_0
    //   369: aload #15
    //   371: putfield mReferenceView : Landroid/view/View;
    //   374: aload #14
    //   376: ifnull -> 385
    //   379: aload_0
    //   380: aload #15
    //   382: putfield mReferenceViewInSelectedRow : Landroid/view/View;
    //   385: aload #14
    //   387: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #342	-> 0
    //   #343	-> 6
    //   #345	-> 12
    //   #350	-> 18
    //   #351	-> 23
    //   #352	-> 38
    //   #354	-> 72
    //   #355	-> 81
    //   #358	-> 106
    //   #359	-> 113
    //   #361	-> 131
    //   #362	-> 136
    //   #364	-> 149
    //   #365	-> 164
    //   #366	-> 164
    //   #364	-> 202
    //   #370	-> 202
    //   #372	-> 202
    //   #373	-> 208
    //   #374	-> 214
    //   #376	-> 220
    //   #377	-> 220
    //   #378	-> 234
    //   #380	-> 250
    //   #383	-> 266
    //   #384	-> 282
    //   #386	-> 298
    //   #387	-> 308
    //   #388	-> 321
    //   #391	-> 331
    //   #392	-> 354
    //   #378	-> 358
    //   #396	-> 368
    //   #398	-> 374
    //   #399	-> 379
    //   #402	-> 385
  }
  
  private View fillUp(int paramInt1, int paramInt2) {
    View view1 = null;
    int i = 0;
    View view2 = view1;
    int j = paramInt1, k = paramInt2;
    if ((this.mGroupFlags & 0x22) == 34) {
      i = this.mListPadding.top;
      k = paramInt2;
      j = paramInt1;
      view2 = view1;
    } 
    while (k > i && j >= 0) {
      view1 = makeRow(j, k, false);
      if (view1 != null)
        view2 = view1; 
      k = this.mReferenceView.getTop() - this.mVerticalSpacing;
      this.mFirstPosition = j;
      j -= this.mNumColumns;
    } 
    if (this.mStackFromBottom)
      this.mFirstPosition = Math.max(0, j + 1); 
    setVisibleRangeHint(this.mFirstPosition, this.mFirstPosition + getChildCount() - 1);
    return view2;
  }
  
  private View fillFromTop(int paramInt) {
    this.mFirstPosition = Math.min(this.mFirstPosition, this.mSelectedPosition);
    this.mFirstPosition = Math.min(this.mFirstPosition, this.mItemCount - 1);
    if (this.mFirstPosition < 0)
      this.mFirstPosition = 0; 
    this.mFirstPosition -= this.mFirstPosition % this.mNumColumns;
    return fillDown(this.mFirstPosition, paramInt);
  }
  
  private View fillFromBottom(int paramInt1, int paramInt2) {
    paramInt1 = Math.max(paramInt1, this.mSelectedPosition);
    paramInt1 = Math.min(paramInt1, this.mItemCount - 1);
    int i = this.mItemCount - 1 - paramInt1;
    paramInt1 = this.mItemCount;
    int j = this.mNumColumns;
    return fillUp(paramInt1 - 1 - i - i % j, paramInt2);
  }
  
  private View fillSelection(int paramInt1, int paramInt2) {
    int i2, i = reconcileSelectedPosition();
    int j = this.mNumColumns;
    int k = this.mVerticalSpacing;
    int m = -1;
    if (!this.mStackFromBottom) {
      i -= i % j;
    } else {
      i = this.mItemCount - 1 - i;
      m = this.mItemCount - 1 - i - i % j;
      i = Math.max(0, m - j + 1);
    } 
    int n = getVerticalFadingEdgeLength();
    int i1 = getTopSelectionPixel(paramInt1, n, i);
    if (this.mStackFromBottom) {
      i2 = m;
    } else {
      i2 = i;
    } 
    View view1 = makeRow(i2, i1, true);
    this.mFirstPosition = i;
    View view2 = this.mReferenceView;
    if (!this.mStackFromBottom) {
      fillDown(i + j, view2.getBottom() + k);
      pinToBottom(paramInt2);
      fillUp(i - j, view2.getTop() - k);
      adjustViewsUpOrDown();
    } else {
      i2 = getBottomSelectionPixel(paramInt2, n, j, i);
      paramInt2 = view2.getBottom();
      offsetChildrenTopAndBottom(i2 - paramInt2);
      fillUp(i - 1, view2.getTop() - k);
      pinToTop(paramInt1);
      fillDown(m + j, view2.getBottom() + k);
      adjustViewsUpOrDown();
    } 
    return view1;
  }
  
  private void pinToTop(int paramInt) {
    if (this.mFirstPosition == 0) {
      int i = getChildAt(0).getTop();
      paramInt -= i;
      if (paramInt < 0)
        offsetChildrenTopAndBottom(paramInt); 
    } 
  }
  
  private void pinToBottom(int paramInt) {
    int i = getChildCount();
    if (this.mFirstPosition + i == this.mItemCount) {
      i = getChildAt(i - 1).getBottom();
      paramInt -= i;
      if (paramInt > 0)
        offsetChildrenTopAndBottom(paramInt); 
    } 
  }
  
  int findMotionRow(int paramInt) {
    int i = getChildCount();
    if (i > 0) {
      int j = this.mNumColumns;
      if (!this.mStackFromBottom) {
        int k;
        for (k = 0; k < i; k += j) {
          if (paramInt <= getChildAt(k).getBottom())
            return this.mFirstPosition + k; 
        } 
      } else {
        int k;
        for (k = i - 1; k >= 0; k -= j) {
          if (paramInt >= getChildAt(k).getTop())
            return this.mFirstPosition + k; 
        } 
      } 
    } 
    return -1;
  }
  
  private View fillSpecific(int paramInt1, int paramInt2) {
    int k;
    View view3;
    int i = this.mNumColumns;
    int j = -1;
    if (!this.mStackFromBottom) {
      paramInt1 -= paramInt1 % i;
    } else {
      paramInt1 = this.mItemCount - 1 - paramInt1;
      j = this.mItemCount - 1 - paramInt1 - paramInt1 % i;
      paramInt1 = Math.max(0, j - i + 1);
    } 
    if (this.mStackFromBottom) {
      k = j;
    } else {
      k = paramInt1;
    } 
    View view1 = makeRow(k, paramInt2, true);
    this.mFirstPosition = paramInt1;
    View view2 = this.mReferenceView;
    if (view2 == null)
      return null; 
    paramInt2 = this.mVerticalSpacing;
    if (!this.mStackFromBottom) {
      view3 = fillUp(paramInt1 - i, view2.getTop() - paramInt2);
      adjustViewsUpOrDown();
      view2 = fillDown(paramInt1 + i, view2.getBottom() + paramInt2);
      paramInt1 = getChildCount();
      if (paramInt1 > 0)
        correctTooHigh(i, paramInt2, paramInt1); 
    } else {
      View view4 = fillDown(j + i, view2.getBottom() + paramInt2);
      adjustViewsUpOrDown();
      View view5 = fillUp(paramInt1 - 1, view2.getTop() - paramInt2);
      paramInt1 = getChildCount();
      view3 = view5;
      view2 = view4;
      if (paramInt1 > 0) {
        correctTooLow(i, paramInt2, paramInt1);
        view2 = view4;
        view3 = view5;
      } 
    } 
    if (view1 != null)
      return view1; 
    if (view3 != null)
      return view3; 
    return view2;
  }
  
  private void correctTooHigh(int paramInt1, int paramInt2, int paramInt3) {
    int i = this.mFirstPosition, j = 1;
    if (i + paramInt3 - 1 == this.mItemCount - 1 && paramInt3 > 0) {
      View view = getChildAt(paramInt3 - 1);
      int k = view.getBottom();
      paramInt3 = this.mBottom;
      int m = this.mTop;
      i = this.mListPadding.bottom;
      i = paramInt3 - m - i - k;
      view = getChildAt(0);
      m = view.getTop();
      if (i > 0 && (this.mFirstPosition > 0 || m < this.mListPadding.top)) {
        paramInt3 = i;
        if (this.mFirstPosition == 0)
          paramInt3 = Math.min(i, this.mListPadding.top - m); 
        offsetChildrenTopAndBottom(paramInt3);
        if (this.mFirstPosition > 0) {
          paramInt3 = this.mFirstPosition;
          if (this.mStackFromBottom)
            paramInt1 = j; 
          j = view.getTop();
          fillUp(paramInt3 - paramInt1, j - paramInt2);
          adjustViewsUpOrDown();
        } 
      } 
    } 
  }
  
  private void correctTooLow(int paramInt1, int paramInt2, int paramInt3) {
    if (this.mFirstPosition == 0 && paramInt3 > 0) {
      View view = getChildAt(0);
      int i = view.getTop();
      int j = this.mListPadding.top;
      int k = this.mBottom - this.mTop - this.mListPadding.bottom;
      j = i - j;
      view = getChildAt(paramInt3 - 1);
      int m = view.getBottom();
      int n = this.mFirstPosition;
      i = 1;
      n = n + paramInt3 - 1;
      if (j > 0 && (n < this.mItemCount - 1 || m > k)) {
        paramInt3 = j;
        if (n == this.mItemCount - 1)
          paramInt3 = Math.min(j, m - k); 
        offsetChildrenTopAndBottom(-paramInt3);
        if (n < this.mItemCount - 1) {
          if (!this.mStackFromBottom)
            paramInt1 = i; 
          paramInt3 = view.getBottom();
          fillDown(paramInt1 + n, paramInt3 + paramInt2);
          adjustViewsUpOrDown();
        } 
      } 
    } 
  }
  
  private View fillFromSelection(int paramInt1, int paramInt2, int paramInt3) {
    int i = getVerticalFadingEdgeLength();
    int j = this.mSelectedPosition;
    int k = this.mNumColumns;
    int m = this.mVerticalSpacing;
    int n = -1;
    if (!this.mStackFromBottom) {
      j -= j % k;
    } else {
      n = this.mItemCount - 1 - j;
      n = this.mItemCount - 1 - n - n % k;
      j = Math.max(0, n - k + 1);
    } 
    int i1 = getTopSelectionPixel(paramInt2, i, j);
    paramInt3 = getBottomSelectionPixel(paramInt3, i, k, j);
    if (this.mStackFromBottom) {
      paramInt2 = n;
    } else {
      paramInt2 = j;
    } 
    View view1 = makeRow(paramInt2, paramInt1, true);
    this.mFirstPosition = j;
    View view2 = this.mReferenceView;
    adjustForTopFadingEdge(view2, i1, paramInt3);
    adjustForBottomFadingEdge(view2, i1, paramInt3);
    if (!this.mStackFromBottom) {
      fillUp(j - k, view2.getTop() - m);
      adjustViewsUpOrDown();
      fillDown(j + k, view2.getBottom() + m);
    } else {
      fillDown(n + k, view2.getBottom() + m);
      adjustViewsUpOrDown();
      fillUp(j - 1, view2.getTop() - m);
    } 
    return view1;
  }
  
  private int getBottomSelectionPixel(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = paramInt1;
    if (paramInt4 + paramInt3 - 1 < this.mItemCount - 1)
      i = paramInt1 - paramInt2; 
    return i;
  }
  
  private int getTopSelectionPixel(int paramInt1, int paramInt2, int paramInt3) {
    int i = paramInt1;
    if (paramInt3 > 0)
      i = paramInt1 + paramInt2; 
    return i;
  }
  
  private void adjustForBottomFadingEdge(View paramView, int paramInt1, int paramInt2) {
    if (paramView.getBottom() > paramInt2) {
      int i = paramView.getTop();
      int j = paramView.getBottom();
      paramInt1 = Math.min(i - paramInt1, j - paramInt2);
      offsetChildrenTopAndBottom(-paramInt1);
    } 
  }
  
  private void adjustForTopFadingEdge(View paramView, int paramInt1, int paramInt2) {
    if (paramView.getTop() < paramInt1) {
      int i = paramView.getTop();
      int j = paramView.getBottom();
      paramInt1 = Math.min(paramInt1 - i, paramInt2 - j);
      offsetChildrenTopAndBottom(paramInt1);
    } 
  }
  
  @RemotableViewMethod
  public void smoothScrollToPosition(int paramInt) {
    super.smoothScrollToPosition(paramInt);
  }
  
  @RemotableViewMethod
  public void smoothScrollByOffset(int paramInt) {
    super.smoothScrollByOffset(paramInt);
  }
  
  private View moveSelection(int paramInt1, int paramInt2, int paramInt3) {
    View view1, view2;
    int i = getVerticalFadingEdgeLength();
    int j = this.mSelectedPosition;
    int k = this.mNumColumns;
    int m = this.mVerticalSpacing;
    int n = -1;
    boolean bool = this.mStackFromBottom;
    boolean bool1 = false, bool2 = false;
    if (!bool) {
      i1 = j - paramInt1 - (j - paramInt1) % k;
      int i3 = j - j % k;
      paramInt1 = n;
      n = i3;
    } else {
      n = this.mItemCount - 1 - j;
      int i3 = this.mItemCount - 1 - n - n % k;
      n = Math.max(0, i3 - k + 1);
      i1 = this.mItemCount - 1 - j - paramInt1;
      paramInt1 = this.mItemCount;
      i1 = Math.max(0, paramInt1 - 1 - i1 - i1 % k - k + 1);
      paramInt1 = i3;
    } 
    j = n - i1;
    int i1 = getTopSelectionPixel(paramInt2, i, n);
    int i2 = getBottomSelectionPixel(paramInt3, i, k, n);
    this.mFirstPosition = n;
    if (j > 0) {
      view1 = this.mReferenceViewInSelectedRow;
      if (view1 == null) {
        paramInt2 = bool2;
      } else {
        paramInt2 = view1.getBottom();
      } 
      if (this.mStackFromBottom) {
        paramInt3 = paramInt1;
      } else {
        paramInt3 = n;
      } 
      view1 = makeRow(paramInt3, paramInt2 + m, true);
      view2 = this.mReferenceView;
      adjustForBottomFadingEdge(view2, i1, i2);
    } else if (j < 0) {
      view1 = this.mReferenceViewInSelectedRow;
      if (view1 == null) {
        paramInt2 = 0;
      } else {
        paramInt2 = view1.getTop();
      } 
      if (this.mStackFromBottom) {
        paramInt3 = paramInt1;
      } else {
        paramInt3 = n;
      } 
      view1 = makeRow(paramInt3, paramInt2 - m, false);
      view2 = this.mReferenceView;
      adjustForTopFadingEdge(view2, i1, i2);
    } else {
      view1 = this.mReferenceViewInSelectedRow;
      if (view1 == null) {
        paramInt2 = bool1;
      } else {
        paramInt2 = view1.getTop();
      } 
      if (this.mStackFromBottom) {
        paramInt3 = paramInt1;
      } else {
        paramInt3 = n;
      } 
      view1 = makeRow(paramInt3, paramInt2, true);
      view2 = this.mReferenceView;
    } 
    if (!this.mStackFromBottom) {
      fillUp(n - k, view2.getTop() - m);
      adjustViewsUpOrDown();
      fillDown(n + k, view2.getBottom() + m);
    } else {
      fillDown(paramInt1 + k, view2.getBottom() + m);
      adjustViewsUpOrDown();
      fillUp(n - 1, view2.getTop() - m);
    } 
    return view1;
  }
  
  private boolean determineColumns(int paramInt) {
    int i = this.mRequestedHorizontalSpacing;
    int j = this.mStretchMode;
    int k = this.mRequestedColumnWidth;
    boolean bool1 = false, bool2 = false;
    int m = this.mRequestedNumColumns;
    if (m == -1) {
      if (k > 0) {
        this.mNumColumns = (paramInt + i) / (k + i);
      } else {
        this.mNumColumns = 2;
      } 
    } else {
      this.mNumColumns = m;
    } 
    if (this.mNumColumns <= 0)
      this.mNumColumns = 1; 
    if (j != 0) {
      m = this.mNumColumns;
      paramInt = paramInt - m * k - (m - 1) * i;
      if (paramInt < 0)
        bool2 = true; 
      if (j != 1) {
        if (j != 2) {
          if (j == 3) {
            this.mColumnWidth = k;
            k = this.mNumColumns;
            if (k > 1) {
              this.mHorizontalSpacing = paramInt / (k + 1) + i;
            } else {
              this.mHorizontalSpacing = i + paramInt;
            } 
          } 
        } else {
          this.mColumnWidth = paramInt / this.mNumColumns + k;
          this.mHorizontalSpacing = i;
        } 
      } else {
        this.mColumnWidth = k;
        k = this.mNumColumns;
        if (k > 1) {
          this.mHorizontalSpacing = paramInt / (k - 1) + i;
        } else {
          this.mHorizontalSpacing = i + paramInt;
        } 
      } 
    } else {
      this.mColumnWidth = k;
      this.mHorizontalSpacing = i;
      bool2 = bool1;
    } 
    return bool2;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: iload_2
    //   3: invokespecial onMeasure : (II)V
    //   6: iload_1
    //   7: invokestatic getMode : (I)I
    //   10: istore_3
    //   11: iload_2
    //   12: invokestatic getMode : (I)I
    //   15: istore #4
    //   17: iload_1
    //   18: invokestatic getSize : (I)I
    //   21: istore #5
    //   23: iload_2
    //   24: invokestatic getSize : (I)I
    //   27: istore #6
    //   29: iload_3
    //   30: ifne -> 93
    //   33: aload_0
    //   34: getfield mColumnWidth : I
    //   37: istore #5
    //   39: iload #5
    //   41: ifle -> 67
    //   44: iload #5
    //   46: aload_0
    //   47: getfield mListPadding : Landroid/graphics/Rect;
    //   50: getfield left : I
    //   53: iadd
    //   54: aload_0
    //   55: getfield mListPadding : Landroid/graphics/Rect;
    //   58: getfield right : I
    //   61: iadd
    //   62: istore #5
    //   64: goto -> 84
    //   67: aload_0
    //   68: getfield mListPadding : Landroid/graphics/Rect;
    //   71: getfield left : I
    //   74: aload_0
    //   75: getfield mListPadding : Landroid/graphics/Rect;
    //   78: getfield right : I
    //   81: iadd
    //   82: istore #5
    //   84: aload_0
    //   85: invokevirtual getVerticalScrollbarWidth : ()I
    //   88: iload #5
    //   90: iadd
    //   91: istore #5
    //   93: aload_0
    //   94: getfield mListPadding : Landroid/graphics/Rect;
    //   97: getfield left : I
    //   100: istore #7
    //   102: aload_0
    //   103: getfield mListPadding : Landroid/graphics/Rect;
    //   106: getfield right : I
    //   109: istore #8
    //   111: aload_0
    //   112: iload #5
    //   114: iload #7
    //   116: isub
    //   117: iload #8
    //   119: isub
    //   120: invokespecial determineColumns : (I)Z
    //   123: istore #9
    //   125: iconst_0
    //   126: istore #8
    //   128: aload_0
    //   129: getfield mAdapter : Landroid/widget/ListAdapter;
    //   132: ifnonnull -> 141
    //   135: iconst_0
    //   136: istore #7
    //   138: goto -> 152
    //   141: aload_0
    //   142: getfield mAdapter : Landroid/widget/ListAdapter;
    //   145: invokeinterface getCount : ()I
    //   150: istore #7
    //   152: aload_0
    //   153: iload #7
    //   155: putfield mItemCount : I
    //   158: aload_0
    //   159: getfield mItemCount : I
    //   162: istore #10
    //   164: iload #10
    //   166: ifle -> 363
    //   169: aload_0
    //   170: iconst_0
    //   171: aload_0
    //   172: getfield mIsScrap : [Z
    //   175: invokevirtual obtainView : (I[Z)Landroid/view/View;
    //   178: astore #11
    //   180: aload #11
    //   182: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   185: checkcast android/widget/AbsListView$LayoutParams
    //   188: astore #12
    //   190: aload #12
    //   192: astore #13
    //   194: aload #12
    //   196: ifnonnull -> 215
    //   199: aload_0
    //   200: invokevirtual generateDefaultLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   203: checkcast android/widget/AbsListView$LayoutParams
    //   206: astore #13
    //   208: aload #11
    //   210: aload #13
    //   212: invokevirtual setLayoutParams : (Landroid/view/ViewGroup$LayoutParams;)V
    //   215: aload #13
    //   217: aload_0
    //   218: getfield mAdapter : Landroid/widget/ListAdapter;
    //   221: iconst_0
    //   222: invokeinterface getItemViewType : (I)I
    //   227: putfield viewType : I
    //   230: aload #13
    //   232: aload_0
    //   233: getfield mAdapter : Landroid/widget/ListAdapter;
    //   236: iconst_0
    //   237: invokeinterface isEnabled : (I)Z
    //   242: putfield isEnabled : Z
    //   245: aload #13
    //   247: iconst_1
    //   248: putfield forceAdd : Z
    //   251: iload_2
    //   252: invokestatic getSize : (I)I
    //   255: iconst_0
    //   256: invokestatic makeSafeMeasureSpec : (II)I
    //   259: istore_2
    //   260: aload #13
    //   262: getfield height : I
    //   265: istore #7
    //   267: iload_2
    //   268: iconst_0
    //   269: iload #7
    //   271: invokestatic getChildMeasureSpec : (III)I
    //   274: istore_2
    //   275: aload_0
    //   276: getfield mColumnWidth : I
    //   279: istore #7
    //   281: iload #7
    //   283: ldc_w 1073741824
    //   286: invokestatic makeMeasureSpec : (II)I
    //   289: istore #7
    //   291: aload #13
    //   293: getfield width : I
    //   296: istore #8
    //   298: iload #7
    //   300: iconst_0
    //   301: iload #8
    //   303: invokestatic getChildMeasureSpec : (III)I
    //   306: istore #7
    //   308: aload #11
    //   310: iload #7
    //   312: iload_2
    //   313: invokevirtual measure : (II)V
    //   316: aload #11
    //   318: invokevirtual getMeasuredHeight : ()I
    //   321: istore_2
    //   322: iconst_0
    //   323: aload #11
    //   325: invokevirtual getMeasuredState : ()I
    //   328: invokestatic combineMeasuredStates : (II)I
    //   331: pop
    //   332: iload_2
    //   333: istore #8
    //   335: aload_0
    //   336: getfield mRecycler : Landroid/widget/AbsListView$RecycleBin;
    //   339: aload #13
    //   341: getfield viewType : I
    //   344: invokevirtual shouldRecycleViewType : (I)Z
    //   347: ifeq -> 363
    //   350: aload_0
    //   351: getfield mRecycler : Landroid/widget/AbsListView$RecycleBin;
    //   354: aload #11
    //   356: iconst_m1
    //   357: invokevirtual addScrapView : (Landroid/view/View;I)V
    //   360: iload_2
    //   361: istore #8
    //   363: iload #6
    //   365: istore_2
    //   366: iload #4
    //   368: ifne -> 403
    //   371: aload_0
    //   372: getfield mListPadding : Landroid/graphics/Rect;
    //   375: getfield top : I
    //   378: istore_2
    //   379: aload_0
    //   380: getfield mListPadding : Landroid/graphics/Rect;
    //   383: getfield bottom : I
    //   386: istore #7
    //   388: iload_2
    //   389: iload #7
    //   391: iadd
    //   392: iload #8
    //   394: iadd
    //   395: aload_0
    //   396: invokevirtual getVerticalFadingEdgeLength : ()I
    //   399: iconst_2
    //   400: imul
    //   401: iadd
    //   402: istore_2
    //   403: iload_2
    //   404: istore #7
    //   406: iload #4
    //   408: ldc_w -2147483648
    //   411: if_icmpne -> 507
    //   414: aload_0
    //   415: getfield mListPadding : Landroid/graphics/Rect;
    //   418: getfield top : I
    //   421: aload_0
    //   422: getfield mListPadding : Landroid/graphics/Rect;
    //   425: getfield bottom : I
    //   428: iadd
    //   429: istore #7
    //   431: aload_0
    //   432: getfield mNumColumns : I
    //   435: istore #14
    //   437: iconst_0
    //   438: istore #4
    //   440: iload #7
    //   442: istore #6
    //   444: iload #4
    //   446: iload #10
    //   448: if_icmpge -> 503
    //   451: iload #7
    //   453: iload #8
    //   455: iadd
    //   456: istore #6
    //   458: iload #6
    //   460: istore #7
    //   462: iload #4
    //   464: iload #14
    //   466: iadd
    //   467: iload #10
    //   469: if_icmpge -> 481
    //   472: iload #6
    //   474: aload_0
    //   475: getfield mVerticalSpacing : I
    //   478: iadd
    //   479: istore #7
    //   481: iload #7
    //   483: iload_2
    //   484: if_icmplt -> 493
    //   487: iload_2
    //   488: istore #6
    //   490: goto -> 503
    //   493: iload #4
    //   495: iload #14
    //   497: iadd
    //   498: istore #4
    //   500: goto -> 440
    //   503: iload #6
    //   505: istore #7
    //   507: iload #5
    //   509: istore_2
    //   510: iload_3
    //   511: ldc_w -2147483648
    //   514: if_icmpne -> 597
    //   517: aload_0
    //   518: getfield mRequestedNumColumns : I
    //   521: istore #6
    //   523: iload #5
    //   525: istore_2
    //   526: iload #6
    //   528: iconst_m1
    //   529: if_icmpeq -> 597
    //   532: aload_0
    //   533: getfield mColumnWidth : I
    //   536: istore_2
    //   537: aload_0
    //   538: getfield mHorizontalSpacing : I
    //   541: istore #8
    //   543: aload_0
    //   544: getfield mListPadding : Landroid/graphics/Rect;
    //   547: getfield left : I
    //   550: istore #4
    //   552: aload_0
    //   553: getfield mListPadding : Landroid/graphics/Rect;
    //   556: getfield right : I
    //   559: istore_3
    //   560: iload_2
    //   561: iload #6
    //   563: imul
    //   564: iload #6
    //   566: iconst_1
    //   567: isub
    //   568: iload #8
    //   570: imul
    //   571: iadd
    //   572: iload #4
    //   574: iadd
    //   575: iload_3
    //   576: iadd
    //   577: iload #5
    //   579: if_icmpgt -> 590
    //   582: iload #5
    //   584: istore_2
    //   585: iload #9
    //   587: ifeq -> 597
    //   590: iload #5
    //   592: ldc_w 16777216
    //   595: ior
    //   596: istore_2
    //   597: aload_0
    //   598: iload_2
    //   599: iload #7
    //   601: invokevirtual setMeasuredDimension : (II)V
    //   604: aload_0
    //   605: iload_1
    //   606: putfield mWidthMeasureSpec : I
    //   609: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1060	-> 0
    //   #1062	-> 6
    //   #1063	-> 11
    //   #1064	-> 17
    //   #1065	-> 23
    //   #1067	-> 29
    //   #1068	-> 33
    //   #1069	-> 44
    //   #1071	-> 67
    //   #1073	-> 84
    //   #1076	-> 93
    //   #1077	-> 111
    //   #1079	-> 125
    //   #1080	-> 128
    //   #1082	-> 128
    //   #1083	-> 158
    //   #1084	-> 164
    //   #1085	-> 169
    //   #1087	-> 180
    //   #1088	-> 190
    //   #1089	-> 199
    //   #1090	-> 208
    //   #1092	-> 215
    //   #1093	-> 230
    //   #1094	-> 245
    //   #1096	-> 251
    //   #1097	-> 251
    //   #1096	-> 267
    //   #1099	-> 275
    //   #1100	-> 281
    //   #1099	-> 298
    //   #1101	-> 308
    //   #1103	-> 316
    //   #1104	-> 322
    //   #1106	-> 332
    //   #1107	-> 350
    //   #1111	-> 363
    //   #1112	-> 371
    //   #1113	-> 388
    //   #1116	-> 403
    //   #1117	-> 414
    //   #1119	-> 431
    //   #1120	-> 437
    //   #1121	-> 451
    //   #1122	-> 458
    //   #1123	-> 472
    //   #1125	-> 481
    //   #1126	-> 487
    //   #1127	-> 487
    //   #1120	-> 493
    //   #1130	-> 503
    //   #1133	-> 507
    //   #1134	-> 532
    //   #1137	-> 560
    //   #1138	-> 590
    //   #1142	-> 597
    //   #1143	-> 604
    //   #1144	-> 609
  }
  
  protected void attachLayoutAnimationParameters(View paramView, ViewGroup.LayoutParams paramLayoutParams, int paramInt1, int paramInt2) {
    GridLayoutAnimationController.AnimationParameters animationParameters2 = (GridLayoutAnimationController.AnimationParameters)paramLayoutParams.layoutAnimationParameters;
    GridLayoutAnimationController.AnimationParameters animationParameters1 = animationParameters2;
    if (animationParameters2 == null) {
      animationParameters1 = new GridLayoutAnimationController.AnimationParameters();
      paramLayoutParams.layoutAnimationParameters = animationParameters1;
    } 
    animationParameters1.count = paramInt2;
    animationParameters1.index = paramInt1;
    animationParameters1.columnsCount = this.mNumColumns;
    animationParameters1.rowsCount = paramInt2 / this.mNumColumns;
    if (!this.mStackFromBottom) {
      animationParameters1.column = paramInt1 % this.mNumColumns;
      animationParameters1.row = paramInt1 / this.mNumColumns;
    } else {
      paramInt2 = paramInt2 - 1 - paramInt1;
      paramInt1 = this.mNumColumns;
      animationParameters1.column = paramInt1 - 1 - paramInt2 % paramInt1;
      animationParameters1.row = animationParameters1.rowsCount - 1 - paramInt2 / this.mNumColumns;
    } 
  }
  
  protected void layoutChildren() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mBlockLayoutRequests : Z
    //   4: istore_1
    //   5: iload_1
    //   6: ifne -> 14
    //   9: aload_0
    //   10: iconst_1
    //   11: putfield mBlockLayoutRequests : Z
    //   14: aload_0
    //   15: invokespecial layoutChildren : ()V
    //   18: aload_0
    //   19: invokevirtual invalidate : ()V
    //   22: aload_0
    //   23: getfield mAdapter : Landroid/widget/ListAdapter;
    //   26: astore_2
    //   27: aload_2
    //   28: ifnonnull -> 53
    //   31: aload_0
    //   32: invokevirtual resetList : ()V
    //   35: aload_0
    //   36: invokevirtual invokeOnItemScrollListener : ()V
    //   39: iload_1
    //   40: ifne -> 48
    //   43: aload_0
    //   44: iconst_0
    //   45: putfield mBlockLayoutRequests : Z
    //   48: return
    //   49: astore_2
    //   50: goto -> 1519
    //   53: aload_0
    //   54: getfield mListPadding : Landroid/graphics/Rect;
    //   57: getfield top : I
    //   60: istore_3
    //   61: aload_0
    //   62: getfield mBottom : I
    //   65: aload_0
    //   66: getfield mTop : I
    //   69: isub
    //   70: aload_0
    //   71: getfield mListPadding : Landroid/graphics/Rect;
    //   74: getfield bottom : I
    //   77: isub
    //   78: istore #4
    //   80: aload_0
    //   81: invokevirtual getChildCount : ()I
    //   84: istore #5
    //   86: iconst_0
    //   87: istore #6
    //   89: aconst_null
    //   90: astore #7
    //   92: aconst_null
    //   93: astore #8
    //   95: aconst_null
    //   96: astore_2
    //   97: aconst_null
    //   98: astore #9
    //   100: aload_0
    //   101: getfield mLayoutMode : I
    //   104: tableswitch default -> 144, 1 -> 275, 2 -> 200, 3 -> 275, 4 -> 275, 5 -> 275, 6 -> 153
    //   144: aload_0
    //   145: getfield mSelectedPosition : I
    //   148: istore #10
    //   150: goto -> 293
    //   153: iload #6
    //   155: istore #10
    //   157: aload #7
    //   159: astore #11
    //   161: aload_2
    //   162: astore #8
    //   164: aload #9
    //   166: astore #12
    //   168: aload_0
    //   169: getfield mNextSelectedPosition : I
    //   172: iflt -> 349
    //   175: aload_0
    //   176: getfield mNextSelectedPosition : I
    //   179: aload_0
    //   180: getfield mSelectedPosition : I
    //   183: isub
    //   184: istore #10
    //   186: aload #7
    //   188: astore #11
    //   190: aload_2
    //   191: astore #8
    //   193: aload #9
    //   195: astore #12
    //   197: goto -> 349
    //   200: aload_0
    //   201: getfield mNextSelectedPosition : I
    //   204: aload_0
    //   205: getfield mFirstPosition : I
    //   208: isub
    //   209: istore #13
    //   211: iload #6
    //   213: istore #10
    //   215: aload #7
    //   217: astore #11
    //   219: aload_2
    //   220: astore #8
    //   222: aload #9
    //   224: astore #12
    //   226: iload #13
    //   228: iflt -> 349
    //   231: iload #6
    //   233: istore #10
    //   235: aload #7
    //   237: astore #11
    //   239: aload_2
    //   240: astore #8
    //   242: aload #9
    //   244: astore #12
    //   246: iload #13
    //   248: iload #5
    //   250: if_icmpge -> 349
    //   253: aload_0
    //   254: iload #13
    //   256: invokevirtual getChildAt : (I)Landroid/view/View;
    //   259: astore #12
    //   261: iload #6
    //   263: istore #10
    //   265: aload #7
    //   267: astore #11
    //   269: aload_2
    //   270: astore #8
    //   272: goto -> 349
    //   275: iload #6
    //   277: istore #10
    //   279: aload #7
    //   281: astore #11
    //   283: aload_2
    //   284: astore #8
    //   286: aload #9
    //   288: astore #12
    //   290: goto -> 349
    //   293: aload_0
    //   294: getfield mFirstPosition : I
    //   297: istore #13
    //   299: iload #10
    //   301: iload #13
    //   303: isub
    //   304: istore #10
    //   306: aload #8
    //   308: astore_2
    //   309: iload #10
    //   311: iflt -> 331
    //   314: aload #8
    //   316: astore_2
    //   317: iload #10
    //   319: iload #5
    //   321: if_icmpge -> 331
    //   324: aload_0
    //   325: iload #10
    //   327: invokevirtual getChildAt : (I)Landroid/view/View;
    //   330: astore_2
    //   331: aload_0
    //   332: iconst_0
    //   333: invokevirtual getChildAt : (I)Landroid/view/View;
    //   336: astore #8
    //   338: aload #9
    //   340: astore #12
    //   342: aload_2
    //   343: astore #11
    //   345: iload #6
    //   347: istore #10
    //   349: aload_0
    //   350: getfield mDataChanged : Z
    //   353: istore #14
    //   355: iload #14
    //   357: ifeq -> 364
    //   360: aload_0
    //   361: invokevirtual handleDataChanged : ()V
    //   364: aload_0
    //   365: getfield mItemCount : I
    //   368: istore #6
    //   370: iload #6
    //   372: ifne -> 393
    //   375: aload_0
    //   376: invokevirtual resetList : ()V
    //   379: aload_0
    //   380: invokevirtual invokeOnItemScrollListener : ()V
    //   383: iload_1
    //   384: ifne -> 392
    //   387: aload_0
    //   388: iconst_0
    //   389: putfield mBlockLayoutRequests : Z
    //   392: return
    //   393: aload_0
    //   394: aload_0
    //   395: getfield mNextSelectedPosition : I
    //   398: invokevirtual setSelectedPositionInt : (I)V
    //   401: aconst_null
    //   402: astore #9
    //   404: aconst_null
    //   405: astore_2
    //   406: iconst_m1
    //   407: istore #6
    //   409: aload_0
    //   410: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   413: astore #15
    //   415: aload #15
    //   417: ifnull -> 508
    //   420: aload #15
    //   422: invokevirtual getAccessibilityFocusedHost : ()Landroid/view/View;
    //   425: astore #7
    //   427: aload #7
    //   429: ifnull -> 505
    //   432: aload_0
    //   433: aload #7
    //   435: invokevirtual getAccessibilityFocusedChild : (Landroid/view/View;)Landroid/view/View;
    //   438: astore #16
    //   440: aload #16
    //   442: ifnull -> 502
    //   445: iload #14
    //   447: ifeq -> 474
    //   450: aload #16
    //   452: invokevirtual hasTransientState : ()Z
    //   455: ifne -> 474
    //   458: aload_0
    //   459: getfield mAdapterHasStableIds : Z
    //   462: ifeq -> 468
    //   465: goto -> 474
    //   468: aconst_null
    //   469: astore #9
    //   471: goto -> 484
    //   474: aload #7
    //   476: astore_2
    //   477: aload #15
    //   479: invokevirtual getAccessibilityFocusedVirtualView : ()Landroid/view/accessibility/AccessibilityNodeInfo;
    //   482: astore #9
    //   484: aload_0
    //   485: aload #16
    //   487: invokevirtual getPositionForView : (Landroid/view/View;)I
    //   490: istore #6
    //   492: aload #9
    //   494: astore #7
    //   496: aload_2
    //   497: astore #9
    //   499: goto -> 511
    //   502: goto -> 508
    //   505: goto -> 508
    //   508: aconst_null
    //   509: astore #7
    //   511: aload_0
    //   512: getfield mFirstPosition : I
    //   515: istore #17
    //   517: aload_0
    //   518: getfield mRecycler : Landroid/widget/AbsListView$RecycleBin;
    //   521: astore #16
    //   523: iload #14
    //   525: ifeq -> 575
    //   528: iconst_0
    //   529: istore #13
    //   531: iload #13
    //   533: iload #5
    //   535: if_icmpge -> 569
    //   538: iload_1
    //   539: istore #14
    //   541: iload #14
    //   543: istore_1
    //   544: aload #16
    //   546: aload_0
    //   547: iload #13
    //   549: invokevirtual getChildAt : (I)Landroid/view/View;
    //   552: iload #17
    //   554: iload #13
    //   556: iadd
    //   557: invokevirtual addScrapView : (Landroid/view/View;I)V
    //   560: iinc #13, 1
    //   563: iload #14
    //   565: istore_1
    //   566: goto -> 531
    //   569: iload_1
    //   570: istore #14
    //   572: goto -> 590
    //   575: iload_1
    //   576: istore #14
    //   578: iload #14
    //   580: istore_1
    //   581: aload #16
    //   583: iload #5
    //   585: iload #17
    //   587: invokevirtual fillActiveViews : (II)V
    //   590: iload #14
    //   592: istore_1
    //   593: aload_0
    //   594: invokevirtual detachAllViewsFromParent : ()V
    //   597: iload #14
    //   599: istore_1
    //   600: aload #16
    //   602: invokevirtual removeSkippedScrap : ()V
    //   605: iload #14
    //   607: istore_1
    //   608: aload_0
    //   609: getfield mLayoutMode : I
    //   612: tableswitch default -> 652, 1 -> 787, 2 -> 749, 3 -> 723, 4 -> 704, 5 -> 685, 6 -> 669
    //   652: iload #5
    //   654: ifne -> 937
    //   657: iload #14
    //   659: istore_1
    //   660: aload_0
    //   661: getfield mStackFromBottom : Z
    //   664: istore #18
    //   666: goto -> 814
    //   669: iload #14
    //   671: istore_1
    //   672: aload_0
    //   673: iload #10
    //   675: iload_3
    //   676: iload #4
    //   678: invokespecial moveSelection : (III)Landroid/view/View;
    //   681: astore_2
    //   682: goto -> 1065
    //   685: iload #14
    //   687: istore_1
    //   688: aload_0
    //   689: aload_0
    //   690: getfield mSyncPosition : I
    //   693: aload_0
    //   694: getfield mSpecificTop : I
    //   697: invokespecial fillSpecific : (II)Landroid/view/View;
    //   700: astore_2
    //   701: goto -> 1065
    //   704: iload #14
    //   706: istore_1
    //   707: aload_0
    //   708: aload_0
    //   709: getfield mSelectedPosition : I
    //   712: aload_0
    //   713: getfield mSpecificTop : I
    //   716: invokespecial fillSpecific : (II)Landroid/view/View;
    //   719: astore_2
    //   720: goto -> 1065
    //   723: iload #14
    //   725: istore_1
    //   726: aload_0
    //   727: aload_0
    //   728: getfield mItemCount : I
    //   731: iconst_1
    //   732: isub
    //   733: iload #4
    //   735: invokespecial fillUp : (II)Landroid/view/View;
    //   738: astore_2
    //   739: iload #14
    //   741: istore_1
    //   742: aload_0
    //   743: invokespecial adjustViewsUpOrDown : ()V
    //   746: goto -> 1065
    //   749: aload #12
    //   751: ifnull -> 773
    //   754: iload #14
    //   756: istore_1
    //   757: aload_0
    //   758: aload #12
    //   760: invokevirtual getTop : ()I
    //   763: iload_3
    //   764: iload #4
    //   766: invokespecial fillFromSelection : (III)Landroid/view/View;
    //   769: astore_2
    //   770: goto -> 1065
    //   773: iload #14
    //   775: istore_1
    //   776: aload_0
    //   777: iload_3
    //   778: iload #4
    //   780: invokespecial fillSelection : (II)Landroid/view/View;
    //   783: astore_2
    //   784: goto -> 1065
    //   787: iload #14
    //   789: istore_1
    //   790: aload_0
    //   791: iconst_0
    //   792: putfield mFirstPosition : I
    //   795: iload #14
    //   797: istore_1
    //   798: aload_0
    //   799: iload_3
    //   800: invokespecial fillFromTop : (I)Landroid/view/View;
    //   803: astore_2
    //   804: iload #14
    //   806: istore_1
    //   807: aload_0
    //   808: invokespecial adjustViewsUpOrDown : ()V
    //   811: goto -> 1065
    //   814: iload #18
    //   816: ifne -> 872
    //   819: iload #14
    //   821: istore_1
    //   822: aload_0
    //   823: getfield mAdapter : Landroid/widget/ListAdapter;
    //   826: ifnull -> 848
    //   829: iload #14
    //   831: istore_1
    //   832: aload_0
    //   833: invokevirtual isInTouchMode : ()Z
    //   836: ifeq -> 842
    //   839: goto -> 848
    //   842: iconst_0
    //   843: istore #10
    //   845: goto -> 851
    //   848: iconst_m1
    //   849: istore #10
    //   851: iload #14
    //   853: istore_1
    //   854: aload_0
    //   855: iload #10
    //   857: invokevirtual setSelectedPositionInt : (I)V
    //   860: iload #14
    //   862: istore_1
    //   863: aload_0
    //   864: iload_3
    //   865: invokespecial fillFromTop : (I)Landroid/view/View;
    //   868: astore_2
    //   869: goto -> 1065
    //   872: iload #14
    //   874: istore_1
    //   875: aload_0
    //   876: getfield mItemCount : I
    //   879: iconst_1
    //   880: isub
    //   881: istore #10
    //   883: iload #14
    //   885: istore_1
    //   886: aload_0
    //   887: getfield mAdapter : Landroid/widget/ListAdapter;
    //   890: ifnull -> 912
    //   893: iload #14
    //   895: istore_1
    //   896: aload_0
    //   897: invokevirtual isInTouchMode : ()Z
    //   900: ifeq -> 906
    //   903: goto -> 912
    //   906: iload #10
    //   908: istore_3
    //   909: goto -> 914
    //   912: iconst_m1
    //   913: istore_3
    //   914: iload #14
    //   916: istore_1
    //   917: aload_0
    //   918: iload_3
    //   919: invokevirtual setSelectedPositionInt : (I)V
    //   922: iload #14
    //   924: istore_1
    //   925: aload_0
    //   926: iload #10
    //   928: iload #4
    //   930: invokespecial fillFromBottom : (II)Landroid/view/View;
    //   933: astore_2
    //   934: goto -> 1065
    //   937: iload #14
    //   939: istore_1
    //   940: aload_0
    //   941: getfield mSelectedPosition : I
    //   944: iflt -> 1001
    //   947: iload #14
    //   949: istore_1
    //   950: aload_0
    //   951: getfield mSelectedPosition : I
    //   954: aload_0
    //   955: getfield mItemCount : I
    //   958: if_icmpge -> 1001
    //   961: iload #14
    //   963: istore_1
    //   964: aload_0
    //   965: getfield mSelectedPosition : I
    //   968: istore #10
    //   970: aload #11
    //   972: ifnonnull -> 978
    //   975: goto -> 987
    //   978: iload #14
    //   980: istore_1
    //   981: aload #11
    //   983: invokevirtual getTop : ()I
    //   986: istore_3
    //   987: iload #14
    //   989: istore_1
    //   990: aload_0
    //   991: iload #10
    //   993: iload_3
    //   994: invokespecial fillSpecific : (II)Landroid/view/View;
    //   997: astore_2
    //   998: goto -> 1065
    //   1001: iload #14
    //   1003: istore_1
    //   1004: aload_0
    //   1005: getfield mFirstPosition : I
    //   1008: aload_0
    //   1009: getfield mItemCount : I
    //   1012: if_icmpge -> 1055
    //   1015: iload #14
    //   1017: istore_1
    //   1018: aload_0
    //   1019: getfield mFirstPosition : I
    //   1022: istore #10
    //   1024: aload #8
    //   1026: ifnonnull -> 1032
    //   1029: goto -> 1041
    //   1032: iload #14
    //   1034: istore_1
    //   1035: aload #8
    //   1037: invokevirtual getTop : ()I
    //   1040: istore_3
    //   1041: iload #14
    //   1043: istore_1
    //   1044: aload_0
    //   1045: iload #10
    //   1047: iload_3
    //   1048: invokespecial fillSpecific : (II)Landroid/view/View;
    //   1051: astore_2
    //   1052: goto -> 1065
    //   1055: iload #14
    //   1057: istore_1
    //   1058: aload_0
    //   1059: iconst_0
    //   1060: iload_3
    //   1061: invokespecial fillSpecific : (II)Landroid/view/View;
    //   1064: astore_2
    //   1065: iload #14
    //   1067: istore_1
    //   1068: aload #16
    //   1070: invokevirtual scrapActiveViews : ()V
    //   1073: aload_2
    //   1074: ifnull -> 1100
    //   1077: iload #14
    //   1079: istore_1
    //   1080: aload_0
    //   1081: iconst_m1
    //   1082: aload_2
    //   1083: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   1086: iload #14
    //   1088: istore_1
    //   1089: aload_0
    //   1090: aload_2
    //   1091: invokevirtual getTop : ()I
    //   1094: putfield mSelectedTop : I
    //   1097: goto -> 1233
    //   1100: iload #14
    //   1102: istore_1
    //   1103: aload_0
    //   1104: getfield mTouchMode : I
    //   1107: ifle -> 1126
    //   1110: iload #14
    //   1112: istore_1
    //   1113: aload_0
    //   1114: getfield mTouchMode : I
    //   1117: iconst_3
    //   1118: if_icmpge -> 1126
    //   1121: iconst_1
    //   1122: istore_3
    //   1123: goto -> 1128
    //   1126: iconst_0
    //   1127: istore_3
    //   1128: iload_3
    //   1129: ifeq -> 1168
    //   1132: iload #14
    //   1134: istore_1
    //   1135: aload_0
    //   1136: aload_0
    //   1137: getfield mMotionPosition : I
    //   1140: aload_0
    //   1141: getfield mFirstPosition : I
    //   1144: isub
    //   1145: invokevirtual getChildAt : (I)Landroid/view/View;
    //   1148: astore_2
    //   1149: aload_2
    //   1150: ifnull -> 1165
    //   1153: iload #14
    //   1155: istore_1
    //   1156: aload_0
    //   1157: aload_0
    //   1158: getfield mMotionPosition : I
    //   1161: aload_2
    //   1162: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   1165: goto -> 1233
    //   1168: iload #14
    //   1170: istore_1
    //   1171: aload_0
    //   1172: getfield mSelectedPosition : I
    //   1175: iconst_m1
    //   1176: if_icmpeq -> 1215
    //   1179: iload #14
    //   1181: istore_1
    //   1182: aload_0
    //   1183: aload_0
    //   1184: getfield mSelectorPosition : I
    //   1187: aload_0
    //   1188: getfield mFirstPosition : I
    //   1191: isub
    //   1192: invokevirtual getChildAt : (I)Landroid/view/View;
    //   1195: astore_2
    //   1196: aload_2
    //   1197: ifnull -> 1212
    //   1200: iload #14
    //   1202: istore_1
    //   1203: aload_0
    //   1204: aload_0
    //   1205: getfield mSelectorPosition : I
    //   1208: aload_2
    //   1209: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   1212: goto -> 1233
    //   1215: iload #14
    //   1217: istore_1
    //   1218: aload_0
    //   1219: iconst_0
    //   1220: putfield mSelectedTop : I
    //   1223: iload #14
    //   1225: istore_1
    //   1226: aload_0
    //   1227: getfield mSelectorRect : Landroid/graphics/Rect;
    //   1230: invokevirtual setEmpty : ()V
    //   1233: aload #15
    //   1235: ifnull -> 1407
    //   1238: iload #14
    //   1240: istore_1
    //   1241: aload #15
    //   1243: invokevirtual getAccessibilityFocusedHost : ()Landroid/view/View;
    //   1246: astore_2
    //   1247: aload_2
    //   1248: ifnonnull -> 1404
    //   1251: aload #9
    //   1253: ifnull -> 1336
    //   1256: iload #14
    //   1258: istore_1
    //   1259: aload #9
    //   1261: invokevirtual isAttachedToWindow : ()Z
    //   1264: ifeq -> 1333
    //   1267: iload #14
    //   1269: istore_1
    //   1270: aload #9
    //   1272: invokevirtual getAccessibilityNodeProvider : ()Landroid/view/accessibility/AccessibilityNodeProvider;
    //   1275: astore_2
    //   1276: aload #7
    //   1278: ifnull -> 1321
    //   1281: aload_2
    //   1282: ifnull -> 1321
    //   1285: iload #14
    //   1287: istore_1
    //   1288: aload #7
    //   1290: invokevirtual getSourceNodeId : ()J
    //   1293: lstore #19
    //   1295: iload #14
    //   1297: istore_1
    //   1298: lload #19
    //   1300: invokestatic getVirtualDescendantId : (J)I
    //   1303: istore #6
    //   1305: iload #14
    //   1307: istore_1
    //   1308: aload_2
    //   1309: iload #6
    //   1311: bipush #64
    //   1313: aconst_null
    //   1314: invokevirtual performAction : (IILandroid/os/Bundle;)Z
    //   1317: pop
    //   1318: goto -> 1330
    //   1321: iload #14
    //   1323: istore_1
    //   1324: aload #9
    //   1326: invokevirtual requestAccessibilityFocus : ()Z
    //   1329: pop
    //   1330: goto -> 1407
    //   1333: goto -> 1336
    //   1336: iload #6
    //   1338: iconst_m1
    //   1339: if_icmpeq -> 1401
    //   1342: iload #14
    //   1344: istore_1
    //   1345: aload_0
    //   1346: getfield mFirstPosition : I
    //   1349: istore_3
    //   1350: iload #14
    //   1352: istore_1
    //   1353: aload_0
    //   1354: invokevirtual getChildCount : ()I
    //   1357: istore #10
    //   1359: iload #14
    //   1361: istore_1
    //   1362: iload #6
    //   1364: iload_3
    //   1365: isub
    //   1366: iconst_0
    //   1367: iload #10
    //   1369: iconst_1
    //   1370: isub
    //   1371: invokestatic constrain : (III)I
    //   1374: istore #6
    //   1376: iload #14
    //   1378: istore_1
    //   1379: aload_0
    //   1380: iload #6
    //   1382: invokevirtual getChildAt : (I)Landroid/view/View;
    //   1385: astore_2
    //   1386: aload_2
    //   1387: ifnull -> 1407
    //   1390: iload #14
    //   1392: istore_1
    //   1393: aload_2
    //   1394: invokevirtual requestAccessibilityFocus : ()Z
    //   1397: pop
    //   1398: goto -> 1407
    //   1401: goto -> 1407
    //   1404: goto -> 1407
    //   1407: iload #14
    //   1409: istore_1
    //   1410: aload_0
    //   1411: iconst_0
    //   1412: putfield mLayoutMode : I
    //   1415: iload #14
    //   1417: istore_1
    //   1418: aload_0
    //   1419: iconst_0
    //   1420: putfield mDataChanged : Z
    //   1423: iload #14
    //   1425: istore_1
    //   1426: aload_0
    //   1427: getfield mPositionScrollAfterLayout : Ljava/lang/Runnable;
    //   1430: ifnull -> 1453
    //   1433: iload #14
    //   1435: istore_1
    //   1436: aload_0
    //   1437: aload_0
    //   1438: getfield mPositionScrollAfterLayout : Ljava/lang/Runnable;
    //   1441: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   1444: pop
    //   1445: iload #14
    //   1447: istore_1
    //   1448: aload_0
    //   1449: aconst_null
    //   1450: putfield mPositionScrollAfterLayout : Ljava/lang/Runnable;
    //   1453: iload #14
    //   1455: istore_1
    //   1456: aload_0
    //   1457: iconst_0
    //   1458: putfield mNeedSync : Z
    //   1461: iload #14
    //   1463: istore_1
    //   1464: aload_0
    //   1465: aload_0
    //   1466: getfield mSelectedPosition : I
    //   1469: invokevirtual setNextSelectedPositionInt : (I)V
    //   1472: iload #14
    //   1474: istore_1
    //   1475: aload_0
    //   1476: invokevirtual updateScrollIndicators : ()V
    //   1479: iload #14
    //   1481: istore_1
    //   1482: aload_0
    //   1483: getfield mItemCount : I
    //   1486: ifle -> 1496
    //   1489: iload #14
    //   1491: istore_1
    //   1492: aload_0
    //   1493: invokevirtual checkSelectionChanged : ()V
    //   1496: iload #14
    //   1498: istore_1
    //   1499: aload_0
    //   1500: invokevirtual invokeOnItemScrollListener : ()V
    //   1503: iload #14
    //   1505: ifne -> 1513
    //   1508: aload_0
    //   1509: iconst_0
    //   1510: putfield mBlockLayoutRequests : Z
    //   1513: return
    //   1514: astore_2
    //   1515: goto -> 1519
    //   1518: astore_2
    //   1519: iload_1
    //   1520: ifne -> 1528
    //   1523: aload_0
    //   1524: iconst_0
    //   1525: putfield mBlockLayoutRequests : Z
    //   1528: aload_2
    //   1529: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1176	-> 0
    //   #1177	-> 5
    //   #1178	-> 9
    //   #1182	-> 14
    //   #1184	-> 18
    //   #1186	-> 22
    //   #1187	-> 31
    //   #1188	-> 35
    //   #1422	-> 39
    //   #1423	-> 43
    //   #1189	-> 48
    //   #1422	-> 49
    //   #1192	-> 53
    //   #1193	-> 61
    //   #1195	-> 80
    //   #1197	-> 86
    //   #1200	-> 89
    //   #1201	-> 95
    //   #1202	-> 97
    //   #1205	-> 100
    //   #1224	-> 144
    //   #1218	-> 153
    //   #1219	-> 175
    //   #1207	-> 200
    //   #1208	-> 211
    //   #1209	-> 253
    //   #1216	-> 275
    //   #1224	-> 293
    //   #1225	-> 306
    //   #1226	-> 324
    //   #1230	-> 331
    //   #1233	-> 349
    //   #1234	-> 355
    //   #1235	-> 360
    //   #1240	-> 364
    //   #1241	-> 375
    //   #1242	-> 379
    //   #1422	-> 383
    //   #1423	-> 387
    //   #1243	-> 392
    //   #1246	-> 393
    //   #1248	-> 401
    //   #1249	-> 401
    //   #1250	-> 406
    //   #1255	-> 409
    //   #1256	-> 415
    //   #1257	-> 420
    //   #1258	-> 427
    //   #1259	-> 432
    //   #1260	-> 440
    //   #1261	-> 445
    //   #1265	-> 474
    //   #1266	-> 477
    //   #1267	-> 477
    //   #1271	-> 484
    //   #1260	-> 502
    //   #1258	-> 505
    //   #1256	-> 508
    //   #1278	-> 508
    //   #1279	-> 517
    //   #1281	-> 523
    //   #1282	-> 528
    //   #1283	-> 538
    //   #1282	-> 560
    //   #1286	-> 575
    //   #1290	-> 590
    //   #1291	-> 597
    //   #1293	-> 605
    //   #1321	-> 652
    //   #1322	-> 657
    //   #1318	-> 669
    //   #1319	-> 682
    //   #1314	-> 685
    //   #1315	-> 701
    //   #1311	-> 704
    //   #1312	-> 720
    //   #1307	-> 723
    //   #1308	-> 739
    //   #1309	-> 746
    //   #1295	-> 749
    //   #1296	-> 754
    //   #1298	-> 773
    //   #1300	-> 784
    //   #1302	-> 787
    //   #1303	-> 795
    //   #1304	-> 804
    //   #1305	-> 811
    //   #1322	-> 814
    //   #1323	-> 819
    //   #1324	-> 842
    //   #1323	-> 851
    //   #1325	-> 860
    //   #1327	-> 872
    //   #1328	-> 883
    //   #1329	-> 906
    //   #1328	-> 914
    //   #1330	-> 922
    //   #1331	-> 934
    //   #1333	-> 937
    //   #1334	-> 961
    //   #1335	-> 975
    //   #1334	-> 987
    //   #1336	-> 1001
    //   #1337	-> 1015
    //   #1338	-> 1029
    //   #1337	-> 1041
    //   #1340	-> 1055
    //   #1347	-> 1065
    //   #1349	-> 1073
    //   #1350	-> 1077
    //   #1351	-> 1086
    //   #1353	-> 1100
    //   #1355	-> 1128
    //   #1357	-> 1132
    //   #1358	-> 1149
    //   #1359	-> 1153
    //   #1361	-> 1165
    //   #1365	-> 1179
    //   #1366	-> 1196
    //   #1367	-> 1200
    //   #1369	-> 1212
    //   #1371	-> 1215
    //   #1372	-> 1223
    //   #1377	-> 1233
    //   #1378	-> 1238
    //   #1379	-> 1247
    //   #1380	-> 1251
    //   #1381	-> 1256
    //   #1382	-> 1267
    //   #1383	-> 1267
    //   #1384	-> 1276
    //   #1385	-> 1285
    //   #1386	-> 1285
    //   #1385	-> 1295
    //   #1387	-> 1305
    //   #1389	-> 1318
    //   #1384	-> 1321
    //   #1390	-> 1321
    //   #1392	-> 1330
    //   #1381	-> 1333
    //   #1380	-> 1336
    //   #1392	-> 1336
    //   #1394	-> 1342
    //   #1396	-> 1350
    //   #1394	-> 1359
    //   #1397	-> 1376
    //   #1398	-> 1386
    //   #1399	-> 1390
    //   #1392	-> 1401
    //   #1379	-> 1404
    //   #1377	-> 1407
    //   #1405	-> 1407
    //   #1406	-> 1415
    //   #1407	-> 1423
    //   #1408	-> 1433
    //   #1409	-> 1445
    //   #1411	-> 1453
    //   #1412	-> 1461
    //   #1414	-> 1472
    //   #1416	-> 1479
    //   #1417	-> 1489
    //   #1420	-> 1496
    //   #1422	-> 1503
    //   #1423	-> 1508
    //   #1426	-> 1513
    //   #1422	-> 1514
    //   #1423	-> 1523
    //   #1425	-> 1528
    // Exception table:
    //   from	to	target	type
    //   14	18	1518	finally
    //   18	22	1518	finally
    //   22	27	1518	finally
    //   31	35	49	finally
    //   35	39	49	finally
    //   53	61	1518	finally
    //   61	80	1518	finally
    //   80	86	1518	finally
    //   100	144	1518	finally
    //   144	150	1518	finally
    //   168	175	49	finally
    //   175	186	49	finally
    //   200	211	49	finally
    //   253	261	49	finally
    //   293	299	1518	finally
    //   324	331	49	finally
    //   331	338	1518	finally
    //   349	355	1518	finally
    //   360	364	49	finally
    //   364	370	1518	finally
    //   375	379	49	finally
    //   379	383	49	finally
    //   393	401	1518	finally
    //   409	415	1518	finally
    //   420	427	49	finally
    //   432	440	49	finally
    //   450	465	49	finally
    //   477	484	49	finally
    //   484	492	49	finally
    //   511	517	1518	finally
    //   517	523	1518	finally
    //   544	560	1514	finally
    //   581	590	1514	finally
    //   593	597	1514	finally
    //   600	605	1514	finally
    //   608	652	1514	finally
    //   660	666	1514	finally
    //   672	682	1514	finally
    //   688	701	1514	finally
    //   707	720	1514	finally
    //   726	739	1514	finally
    //   742	746	1514	finally
    //   757	770	1514	finally
    //   776	784	1514	finally
    //   790	795	1514	finally
    //   798	804	1514	finally
    //   807	811	1514	finally
    //   822	829	1514	finally
    //   832	839	1514	finally
    //   854	860	1514	finally
    //   863	869	1514	finally
    //   875	883	1514	finally
    //   886	893	1514	finally
    //   896	903	1514	finally
    //   917	922	1514	finally
    //   925	934	1514	finally
    //   940	947	1514	finally
    //   950	961	1514	finally
    //   964	970	1514	finally
    //   981	987	1514	finally
    //   990	998	1514	finally
    //   1004	1015	1514	finally
    //   1018	1024	1514	finally
    //   1035	1041	1514	finally
    //   1044	1052	1514	finally
    //   1058	1065	1514	finally
    //   1068	1073	1514	finally
    //   1080	1086	1514	finally
    //   1089	1097	1514	finally
    //   1103	1110	1514	finally
    //   1113	1121	1514	finally
    //   1135	1149	1514	finally
    //   1156	1165	1514	finally
    //   1171	1179	1514	finally
    //   1182	1196	1514	finally
    //   1203	1212	1514	finally
    //   1218	1223	1514	finally
    //   1226	1233	1514	finally
    //   1241	1247	1514	finally
    //   1259	1267	1514	finally
    //   1270	1276	1514	finally
    //   1288	1295	1514	finally
    //   1298	1305	1514	finally
    //   1308	1318	1514	finally
    //   1324	1330	1514	finally
    //   1345	1350	1514	finally
    //   1353	1359	1514	finally
    //   1362	1376	1514	finally
    //   1379	1386	1514	finally
    //   1393	1398	1514	finally
    //   1410	1415	1514	finally
    //   1418	1423	1514	finally
    //   1426	1433	1514	finally
    //   1436	1445	1514	finally
    //   1448	1453	1514	finally
    //   1456	1461	1514	finally
    //   1464	1472	1514	finally
    //   1475	1479	1514	finally
    //   1482	1489	1514	finally
    //   1492	1496	1514	finally
    //   1499	1503	1514	finally
  }
  
  private View makeAndAddView(int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, int paramInt4) {
    if (!this.mDataChanged) {
      View view1 = this.mRecycler.getActiveView(paramInt1);
      if (view1 != null) {
        setupChild(view1, paramInt1, paramInt2, paramBoolean1, paramInt3, paramBoolean2, true, paramInt4);
        return view1;
      } 
    } 
    View view = obtainView(paramInt1, this.mIsScrap);
    setupChild(view, paramInt1, paramInt2, paramBoolean1, paramInt3, paramBoolean2, this.mIsScrap[0], paramInt4);
    return view;
  }
  
  private void setupChild(View paramView, int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, boolean paramBoolean3, int paramInt4) {
    boolean bool;
    boolean bool1;
    Trace.traceBegin(8L, "setupGridItem");
    if (paramBoolean2 && shouldShowSelector()) {
      paramBoolean2 = true;
    } else {
      paramBoolean2 = false;
    } 
    if (paramBoolean2 != paramView.isSelected()) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = this.mTouchMode;
    if (j > 0 && j < 3 && this.mMotionPosition == paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool != paramView.isPressed()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (!paramBoolean3 || i || 
      paramView.isLayoutRequested()) {
      j = 1;
    } else {
      j = 0;
    } 
    AbsListView.LayoutParams layoutParams1 = (AbsListView.LayoutParams)paramView.getLayoutParams();
    AbsListView.LayoutParams layoutParams2 = layoutParams1;
    if (layoutParams1 == null)
      layoutParams2 = (AbsListView.LayoutParams)generateDefaultLayoutParams(); 
    layoutParams2.viewType = this.mAdapter.getItemViewType(paramInt1);
    layoutParams2.isEnabled = this.mAdapter.isEnabled(paramInt1);
    if (i) {
      paramView.setSelected(paramBoolean2);
      if (paramBoolean2)
        requestFocus(); 
    } 
    if (bool1)
      paramView.setPressed(bool); 
    if (this.mChoiceMode != 0 && this.mCheckStates != null)
      if (paramView instanceof Checkable) {
        ((Checkable)paramView).setChecked(this.mCheckStates.get(paramInt1));
      } else if ((getContext().getApplicationInfo()).targetSdkVersion >= 11) {
        paramView.setActivated(this.mCheckStates.get(paramInt1));
      }  
    if (paramBoolean3 && !layoutParams2.forceAdd) {
      attachViewToParent(paramView, paramInt4, layoutParams2);
      if (!paramBoolean3 || 
        ((AbsListView.LayoutParams)paramView.getLayoutParams()).scrappedFromPosition != paramInt1)
        paramView.jumpDrawablesToCurrentState(); 
    } else {
      layoutParams2.forceAdd = false;
      addViewInLayout(paramView, paramInt4, layoutParams2, true);
    } 
    if (j != 0) {
      paramInt4 = View.MeasureSpec.makeMeasureSpec(0, 0);
      paramInt1 = layoutParams2.height;
      paramInt1 = ViewGroup.getChildMeasureSpec(paramInt4, 0, paramInt1);
      paramInt4 = this.mColumnWidth;
      i = View.MeasureSpec.makeMeasureSpec(paramInt4, 1073741824);
      paramInt4 = layoutParams2.width;
      paramInt4 = ViewGroup.getChildMeasureSpec(i, 0, paramInt4);
      paramView.measure(paramInt4, paramInt1);
    } else {
      cleanupLayoutState(paramView);
    } 
    paramInt4 = paramView.getMeasuredWidth();
    int i = paramView.getMeasuredHeight();
    if (!paramBoolean1)
      paramInt2 -= i; 
    paramInt1 = getLayoutDirection();
    paramInt1 = Gravity.getAbsoluteGravity(this.mGravity, paramInt1);
    paramInt1 &= 0x7;
    if (paramInt1 != 1) {
      if (paramInt1 != 3) {
        if (paramInt1 != 5) {
          paramInt1 = paramInt3;
        } else {
          paramInt1 = paramInt3 + this.mColumnWidth - paramInt4;
        } 
      } else {
        paramInt1 = paramInt3;
      } 
    } else {
      paramInt1 = paramInt3 + (this.mColumnWidth - paramInt4) / 2;
    } 
    if (j != 0) {
      paramView.layout(paramInt1, paramInt2, paramInt1 + paramInt4, paramInt2 + i);
    } else {
      paramView.offsetLeftAndRight(paramInt1 - paramView.getLeft());
      paramView.offsetTopAndBottom(paramInt2 - paramView.getTop());
    } 
    if (this.mCachingStarted && !paramView.isDrawingCacheEnabled())
      paramView.setDrawingCacheEnabled(true); 
    Trace.traceEnd(8L);
  }
  
  public void setSelection(int paramInt) {
    if (!isInTouchMode()) {
      setNextSelectedPositionInt(paramInt);
    } else {
      this.mResurrectToPosition = paramInt;
    } 
    this.mLayoutMode = 2;
    if (this.mPositionScroller != null)
      this.mPositionScroller.stop(); 
    requestLayout();
  }
  
  void setSelectionInt(int paramInt) {
    int i = this.mNextSelectedPosition;
    if (this.mPositionScroller != null)
      this.mPositionScroller.stop(); 
    setNextSelectedPositionInt(paramInt);
    layoutChildren();
    if (this.mStackFromBottom) {
      paramInt = this.mItemCount - 1 - this.mNextSelectedPosition;
    } else {
      paramInt = this.mNextSelectedPosition;
    } 
    if (this.mStackFromBottom)
      i = this.mItemCount - 1 - i; 
    int j = this.mNumColumns;
    paramInt /= j;
    i /= j;
    if (paramInt != i)
      awakenScrollBars(); 
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    return commonKey(paramInt, 1, paramKeyEvent);
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    return commonKey(paramInt1, paramInt2, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    return commonKey(paramInt, 1, paramKeyEvent);
  }
  
  private boolean commonKey(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    if (this.mAdapter == null)
      return false; 
    if (this.mDataChanged)
      layoutChildren(); 
    boolean bool1 = false;
    int i = paramKeyEvent.getAction();
    boolean bool2 = bool1;
    if (KeyEvent.isConfirmKey(paramInt1)) {
      bool2 = bool1;
      if (paramKeyEvent.hasNoModifiers()) {
        bool2 = bool1;
        if (i != 1) {
          bool1 = resurrectSelectionIfNeeded();
          bool2 = bool1;
          if (!bool1) {
            bool2 = bool1;
            if (paramKeyEvent.getRepeatCount() == 0) {
              bool2 = bool1;
              if (getChildCount() > 0) {
                keyPressed();
                bool2 = true;
              } 
            } 
          } 
        } 
      } 
    } 
    bool1 = bool2;
    if (!bool2) {
      bool1 = bool2;
      if (i != 1)
        if (paramInt1 != 61) {
          if (paramInt1 != 92) {
            if (paramInt1 != 93) {
              if (paramInt1 != 122) {
                if (paramInt1 != 123) {
                  switch (paramInt1) {
                    default:
                      bool1 = bool2;
                      break;
                    case 22:
                      bool1 = bool2;
                      if (paramKeyEvent.hasNoModifiers()) {
                        if (resurrectSelectionIfNeeded() || arrowScroll(66)) {
                          bool1 = true;
                          break;
                        } 
                        bool1 = false;
                      } 
                      break;
                    case 21:
                      bool1 = bool2;
                      if (paramKeyEvent.hasNoModifiers()) {
                        if (resurrectSelectionIfNeeded() || arrowScroll(17)) {
                          bool1 = true;
                          break;
                        } 
                        bool1 = false;
                      } 
                      break;
                    case 20:
                      if (paramKeyEvent.hasNoModifiers()) {
                        if (resurrectSelectionIfNeeded() || arrowScroll(130)) {
                          bool1 = true;
                          break;
                        } 
                        bool1 = false;
                        break;
                      } 
                      bool1 = bool2;
                      if (paramKeyEvent.hasModifiers(2)) {
                        if (resurrectSelectionIfNeeded() || fullScroll(130)) {
                          bool1 = true;
                          break;
                        } 
                        bool1 = false;
                      } 
                      break;
                    case 19:
                      if (paramKeyEvent.hasNoModifiers()) {
                        if (resurrectSelectionIfNeeded() || arrowScroll(33)) {
                          bool1 = true;
                          break;
                        } 
                        bool1 = false;
                        break;
                      } 
                      bool1 = bool2;
                      if (paramKeyEvent.hasModifiers(2)) {
                        if (resurrectSelectionIfNeeded() || fullScroll(33)) {
                          bool1 = true;
                          break;
                        } 
                        bool1 = false;
                      } 
                      break;
                  } 
                } else {
                  bool1 = bool2;
                  if (paramKeyEvent.hasNoModifiers())
                    if (resurrectSelectionIfNeeded() || fullScroll(130)) {
                      bool1 = true;
                    } else {
                      bool1 = false;
                    }  
                } 
              } else {
                bool1 = bool2;
                if (paramKeyEvent.hasNoModifiers())
                  if (resurrectSelectionIfNeeded() || fullScroll(33)) {
                    bool1 = true;
                  } else {
                    bool1 = false;
                  }  
              } 
            } else if (paramKeyEvent.hasNoModifiers()) {
              if (resurrectSelectionIfNeeded() || pageScroll(130)) {
                bool1 = true;
              } else {
                bool1 = false;
              } 
            } else {
              bool1 = bool2;
              if (paramKeyEvent.hasModifiers(2))
                if (resurrectSelectionIfNeeded() || fullScroll(130)) {
                  bool1 = true;
                } else {
                  bool1 = false;
                }  
            } 
          } else if (paramKeyEvent.hasNoModifiers()) {
            if (resurrectSelectionIfNeeded() || pageScroll(33)) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
          } else {
            bool1 = bool2;
            if (paramKeyEvent.hasModifiers(2))
              if (resurrectSelectionIfNeeded() || fullScroll(33)) {
                bool1 = true;
              } else {
                bool1 = false;
              }  
          } 
        } else if (paramKeyEvent.hasNoModifiers()) {
          if (resurrectSelectionIfNeeded() || 
            sequenceScroll(2)) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
        } else {
          bool1 = bool2;
          if (paramKeyEvent.hasModifiers(1))
            if (resurrectSelectionIfNeeded() || 
              sequenceScroll(1)) {
              bool1 = true;
            } else {
              bool1 = false;
            }  
        }  
    } 
    if (bool1)
      return true; 
    if (sendToTextFilter(paramInt1, paramInt2, paramKeyEvent))
      return true; 
    if (i != 0) {
      if (i != 1) {
        if (i != 2)
          return false; 
        return super.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent);
      } 
      return super.onKeyUp(paramInt1, paramKeyEvent);
    } 
    return super.onKeyDown(paramInt1, paramKeyEvent);
  }
  
  boolean pageScroll(int paramInt) {
    int i = -1;
    if (paramInt == 33) {
      i = Math.max(0, this.mSelectedPosition - getChildCount());
    } else if (paramInt == 130) {
      i = Math.min(this.mItemCount - 1, this.mSelectedPosition + getChildCount());
    } 
    if (i >= 0) {
      setSelectionInt(i);
      invokeOnItemScrollListener();
      awakenScrollBars();
      return true;
    } 
    return false;
  }
  
  boolean fullScroll(int paramInt) {
    boolean bool = false;
    if (paramInt == 33) {
      this.mLayoutMode = 2;
      setSelectionInt(0);
      invokeOnItemScrollListener();
      bool = true;
    } else if (paramInt == 130) {
      this.mLayoutMode = 2;
      setSelectionInt(this.mItemCount - 1);
      invokeOnItemScrollListener();
      bool = true;
    } 
    if (bool)
      awakenScrollBars(); 
    return bool;
  }
  
  boolean arrowScroll(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSelectedPosition : I
    //   4: istore_2
    //   5: aload_0
    //   6: getfield mNumColumns : I
    //   9: istore_3
    //   10: iconst_0
    //   11: istore #4
    //   13: aload_0
    //   14: getfield mStackFromBottom : Z
    //   17: ifne -> 47
    //   20: iload_2
    //   21: iload_3
    //   22: idiv
    //   23: iload_3
    //   24: imul
    //   25: istore #5
    //   27: iload #5
    //   29: iload_3
    //   30: iadd
    //   31: iconst_1
    //   32: isub
    //   33: aload_0
    //   34: getfield mItemCount : I
    //   37: iconst_1
    //   38: isub
    //   39: invokestatic min : (II)I
    //   42: istore #6
    //   44: goto -> 84
    //   47: aload_0
    //   48: getfield mItemCount : I
    //   51: istore #5
    //   53: aload_0
    //   54: getfield mItemCount : I
    //   57: iconst_1
    //   58: isub
    //   59: iload #5
    //   61: iconst_1
    //   62: isub
    //   63: iload_2
    //   64: isub
    //   65: iload_3
    //   66: idiv
    //   67: iload_3
    //   68: imul
    //   69: isub
    //   70: istore #6
    //   72: iconst_0
    //   73: iload #6
    //   75: iload_3
    //   76: isub
    //   77: iconst_1
    //   78: iadd
    //   79: invokestatic max : (II)I
    //   82: istore #5
    //   84: iload_1
    //   85: bipush #33
    //   87: if_icmpeq -> 139
    //   90: iload_1
    //   91: sipush #130
    //   94: if_icmpeq -> 100
    //   97: goto -> 164
    //   100: iload #6
    //   102: aload_0
    //   103: getfield mItemCount : I
    //   106: iconst_1
    //   107: isub
    //   108: if_icmpge -> 164
    //   111: aload_0
    //   112: bipush #6
    //   114: putfield mLayoutMode : I
    //   117: aload_0
    //   118: iload_2
    //   119: iload_3
    //   120: iadd
    //   121: aload_0
    //   122: getfield mItemCount : I
    //   125: iconst_1
    //   126: isub
    //   127: invokestatic min : (II)I
    //   130: invokevirtual setSelectionInt : (I)V
    //   133: iconst_1
    //   134: istore #4
    //   136: goto -> 164
    //   139: iload #5
    //   141: ifle -> 164
    //   144: aload_0
    //   145: bipush #6
    //   147: putfield mLayoutMode : I
    //   150: aload_0
    //   151: iconst_0
    //   152: iload_2
    //   153: iload_3
    //   154: isub
    //   155: invokestatic max : (II)I
    //   158: invokevirtual setSelectionInt : (I)V
    //   161: iconst_1
    //   162: istore #4
    //   164: aload_0
    //   165: invokevirtual isLayoutRtl : ()Z
    //   168: istore #7
    //   170: iload_2
    //   171: iload #5
    //   173: if_icmple -> 221
    //   176: iload_1
    //   177: bipush #17
    //   179: if_icmpne -> 187
    //   182: iload #7
    //   184: ifeq -> 198
    //   187: iload_1
    //   188: bipush #66
    //   190: if_icmpne -> 221
    //   193: iload #7
    //   195: ifeq -> 221
    //   198: aload_0
    //   199: bipush #6
    //   201: putfield mLayoutMode : I
    //   204: aload_0
    //   205: iconst_0
    //   206: iload_2
    //   207: iconst_1
    //   208: isub
    //   209: invokestatic max : (II)I
    //   212: invokevirtual setSelectionInt : (I)V
    //   215: iconst_1
    //   216: istore #8
    //   218: goto -> 286
    //   221: iload #4
    //   223: istore #8
    //   225: iload_2
    //   226: iload #6
    //   228: if_icmpge -> 286
    //   231: iload_1
    //   232: bipush #17
    //   234: if_icmpne -> 242
    //   237: iload #7
    //   239: ifne -> 261
    //   242: iload #4
    //   244: istore #8
    //   246: iload_1
    //   247: bipush #66
    //   249: if_icmpne -> 286
    //   252: iload #4
    //   254: istore #8
    //   256: iload #7
    //   258: ifne -> 286
    //   261: aload_0
    //   262: bipush #6
    //   264: putfield mLayoutMode : I
    //   267: aload_0
    //   268: iload_2
    //   269: iconst_1
    //   270: iadd
    //   271: aload_0
    //   272: getfield mItemCount : I
    //   275: iconst_1
    //   276: isub
    //   277: invokestatic min : (II)I
    //   280: invokevirtual setSelectionInt : (I)V
    //   283: iconst_1
    //   284: istore #8
    //   286: iload #8
    //   288: ifeq -> 303
    //   291: aload_0
    //   292: iload_1
    //   293: invokestatic getContantForFocusDirection : (I)I
    //   296: invokevirtual playSoundEffect : (I)V
    //   299: aload_0
    //   300: invokevirtual invokeOnItemScrollListener : ()V
    //   303: iload #8
    //   305: ifeq -> 313
    //   308: aload_0
    //   309: invokevirtual awakenScrollBars : ()Z
    //   312: pop
    //   313: iload #8
    //   315: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1839	-> 0
    //   #1840	-> 5
    //   #1845	-> 10
    //   #1847	-> 13
    //   #1848	-> 20
    //   #1849	-> 27
    //   #1851	-> 47
    //   #1852	-> 53
    //   #1853	-> 72
    //   #1856	-> 84
    //   #1865	-> 100
    //   #1866	-> 111
    //   #1867	-> 117
    //   #1868	-> 133
    //   #1858	-> 139
    //   #1859	-> 144
    //   #1860	-> 150
    //   #1861	-> 161
    //   #1873	-> 164
    //   #1874	-> 170
    //   #1876	-> 198
    //   #1877	-> 204
    //   #1878	-> 215
    //   #1879	-> 221
    //   #1881	-> 261
    //   #1882	-> 267
    //   #1883	-> 283
    //   #1886	-> 286
    //   #1887	-> 291
    //   #1888	-> 299
    //   #1891	-> 303
    //   #1892	-> 308
    //   #1895	-> 313
  }
  
  boolean sequenceScroll(int paramInt) {
    int m, n, i = this.mSelectedPosition;
    int j = this.mNumColumns;
    int k = this.mItemCount;
    boolean bool = this.mStackFromBottom;
    boolean bool1 = false, bool2 = false;
    if (!bool) {
      m = i / j * j;
      n = Math.min(m + j - 1, k - 1);
    } else {
      n = k - 1 - (k - 1 - i) / j * j;
      m = Math.max(0, n - j + 1);
    } 
    bool = false;
    j = 0;
    if (paramInt != 1) {
      if (paramInt == 2)
        if (i < k - 1) {
          this.mLayoutMode = 6;
          setSelectionInt(i + 1);
          bool = true;
          j = bool2;
          if (i == n)
            j = 1; 
        }  
    } else if (i > 0) {
      this.mLayoutMode = 6;
      setSelectionInt(i - 1);
      bool = true;
      j = bool1;
      if (i == m)
        j = 1; 
    } 
    if (bool) {
      playSoundEffect(SoundEffectConstants.getContantForFocusDirection(paramInt));
      invokeOnItemScrollListener();
    } 
    if (j != 0)
      awakenScrollBars(); 
    return bool;
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    byte b = -1;
    int i = b;
    if (paramBoolean) {
      i = b;
      if (paramRect != null) {
        paramRect.offset(this.mScrollX, this.mScrollY);
        Rect rect = this.mTempRect;
        int j = Integer.MAX_VALUE;
        int k = getChildCount();
        byte b1 = 0;
        while (true) {
          i = b;
          if (b1 < k) {
            if (!isCandidateSelection(b1, paramInt)) {
              i = j;
            } else {
              View view = getChildAt(b1);
              view.getDrawingRect(rect);
              offsetDescendantRectToMyCoords(view, rect);
              int m = getDistance(paramRect, rect, paramInt);
              i = j;
              if (m < j) {
                i = m;
                b = b1;
              } 
            } 
            b1++;
            j = i;
            continue;
          } 
          break;
        } 
      } 
    } 
    if (i >= 0) {
      setSelection(this.mFirstPosition + i);
    } else {
      requestLayout();
    } 
  }
  
  private boolean isCandidateSelection(int paramInt1, int paramInt2) {
    int k, i = getChildCount();
    int j = i - 1 - paramInt1;
    boolean bool = this.mStackFromBottom;
    boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false;
    if (!bool) {
      j = this.mNumColumns;
      k = paramInt1 - paramInt1 % j;
      j = Math.min(j + k - 1, i);
    } else {
      k = this.mNumColumns;
      j = i - 1 - j - j % k;
      k = Math.max(0, j - k + 1);
    } 
    if (paramInt2 != 1) {
      if (paramInt2 != 2) {
        if (paramInt2 != 17) {
          if (paramInt2 != 33) {
            if (paramInt2 != 66) {
              if (paramInt2 == 130) {
                bool3 = bool6;
                if (k == 0)
                  bool3 = true; 
                return bool3;
              } 
              throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}.");
            } 
            bool3 = bool1;
            if (paramInt1 == k)
              bool3 = true; 
            return bool3;
          } 
          bool3 = bool2;
          if (j == i - 1)
            bool3 = true; 
          return bool3;
        } 
        if (paramInt1 == j)
          bool3 = true; 
        return bool3;
      } 
      bool3 = bool4;
      if (paramInt1 == k) {
        bool3 = bool4;
        if (k == 0)
          bool3 = true; 
      } 
      return bool3;
    } 
    bool3 = bool5;
    if (paramInt1 == j) {
      bool3 = bool5;
      if (j == i - 1)
        bool3 = true; 
    } 
    return bool3;
  }
  
  public void setGravity(int paramInt) {
    if (this.mGravity != paramInt) {
      this.mGravity = paramInt;
      requestLayoutIfNecessary();
    } 
  }
  
  public int getGravity() {
    return this.mGravity;
  }
  
  public void setHorizontalSpacing(int paramInt) {
    if (paramInt != this.mRequestedHorizontalSpacing) {
      this.mRequestedHorizontalSpacing = paramInt;
      requestLayoutIfNecessary();
    } 
  }
  
  public int getHorizontalSpacing() {
    return this.mHorizontalSpacing;
  }
  
  public int getRequestedHorizontalSpacing() {
    return this.mRequestedHorizontalSpacing;
  }
  
  public void setVerticalSpacing(int paramInt) {
    if (paramInt != this.mVerticalSpacing) {
      this.mVerticalSpacing = paramInt;
      requestLayoutIfNecessary();
    } 
  }
  
  public int getVerticalSpacing() {
    return this.mVerticalSpacing;
  }
  
  public void setStretchMode(int paramInt) {
    if (paramInt != this.mStretchMode) {
      this.mStretchMode = paramInt;
      requestLayoutIfNecessary();
    } 
  }
  
  public int getStretchMode() {
    return this.mStretchMode;
  }
  
  public void setColumnWidth(int paramInt) {
    if (paramInt != this.mRequestedColumnWidth) {
      this.mRequestedColumnWidth = paramInt;
      requestLayoutIfNecessary();
    } 
  }
  
  public int getColumnWidth() {
    return this.mColumnWidth;
  }
  
  public int getRequestedColumnWidth() {
    return this.mRequestedColumnWidth;
  }
  
  public void setNumColumns(int paramInt) {
    if (paramInt != this.mRequestedNumColumns) {
      this.mRequestedNumColumns = paramInt;
      requestLayoutIfNecessary();
    } 
  }
  
  @ExportedProperty
  public int getNumColumns() {
    return this.mNumColumns;
  }
  
  private void adjustViewsUpOrDown() {
    int i = getChildCount();
    if (i > 0) {
      int j;
      if (!this.mStackFromBottom) {
        View view = getChildAt(0);
        j = view.getTop() - this.mListPadding.top;
        int k = j;
        if (this.mFirstPosition != 0)
          k = j - this.mVerticalSpacing; 
        j = k;
        if (k < 0)
          j = 0; 
      } else {
        View view = getChildAt(i - 1);
        j = view.getBottom() - getHeight() - this.mListPadding.bottom;
        int k = j;
        if (this.mFirstPosition + i < this.mItemCount)
          k = j + this.mVerticalSpacing; 
        j = k;
        if (k > 0)
          j = 0; 
      } 
      if (j != 0)
        offsetChildrenTopAndBottom(-j); 
    } 
  }
  
  protected int computeVerticalScrollExtent() {
    int i = getChildCount();
    if (i > 0) {
      int j = this.mNumColumns;
      j = (i + j - 1) / j;
      int k = j * 100;
      View view = getChildAt(0);
      int m = view.getTop();
      int n = view.getHeight();
      j = k;
      if (n > 0)
        j = k + m * 100 / n; 
      view = getChildAt(i - 1);
      m = view.getBottom();
      i = view.getHeight();
      k = j;
      if (i > 0)
        k = j - (m - getHeight()) * 100 / i; 
      return k;
    } 
    return 0;
  }
  
  protected int computeVerticalScrollOffset() {
    if (this.mFirstPosition >= 0 && getChildCount() > 0) {
      View view = getChildAt(0);
      int i = view.getTop();
      int j = view.getHeight();
      if (j > 0) {
        int k = this.mNumColumns;
        int m = (this.mItemCount + k - 1) / k;
        if (isStackFromBottom()) {
          n = m * k - this.mItemCount;
        } else {
          n = 0;
        } 
        int n = (this.mFirstPosition + n) / k;
        j = i * 100 / j;
        float f = this.mScrollY;
        m = (int)(f / getHeight() * m * 100.0F);
        return Math.max(n * 100 - j + m, 0);
      } 
    } 
    return 0;
  }
  
  protected int computeVerticalScrollRange() {
    int i = this.mNumColumns;
    int j = (this.mItemCount + i - 1) / i;
    int k = Math.max(j * 100, 0);
    i = k;
    if (this.mScrollY != 0)
      i = k + Math.abs((int)(this.mScrollY / getHeight() * j * 100.0F)); 
    return i;
  }
  
  public CharSequence getAccessibilityClassName() {
    return GridView.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    int i = getNumColumns();
    int j = getCount() / i;
    int k = getSelectionModeForAccessibility();
    AccessibilityNodeInfo.CollectionInfo collectionInfo = AccessibilityNodeInfo.CollectionInfo.obtain(j, i, false, k);
    paramAccessibilityNodeInfo.setCollectionInfo(collectionInfo);
    if (i > 0 || j > 0)
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION); 
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    if (paramInt == 16908343) {
      int i = getNumColumns();
      paramInt = paramBundle.getInt("android.view.accessibility.action.ARGUMENT_ROW_INT", -1);
      i = Math.min(paramInt * i, getCount() - 1);
      if (paramInt >= 0) {
        smoothScrollToPosition(i);
        return true;
      } 
    } 
    return false;
  }
  
  public void onInitializeAccessibilityNodeInfoForItem(View paramView, int paramInt, AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    boolean bool;
    super.onInitializeAccessibilityNodeInfoForItem(paramView, paramInt, paramAccessibilityNodeInfo);
    int i = getCount();
    int j = getNumColumns();
    int k = i / j;
    if (!this.mStackFromBottom) {
      i = paramInt % j;
      j = paramInt / j;
    } else {
      i = i - 1 - paramInt;
      int m = i / j;
      i = j - 1 - i % j;
      j = k - 1 - m;
    } 
    AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)paramView.getLayoutParams();
    if (layoutParams != null && layoutParams.viewType == -2) {
      bool = true;
    } else {
      bool = false;
    } 
    boolean bool1 = isItemChecked(paramInt);
    AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo = AccessibilityNodeInfo.CollectionItemInfo.obtain(j, 1, i, 1, bool, bool1);
    paramAccessibilityNodeInfo.setCollectionItemInfo(collectionItemInfo);
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("numColumns", getNumColumns());
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class StretchMode implements Annotation {}
}
