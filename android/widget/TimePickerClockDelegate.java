package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.icu.text.DecimalFormatSymbols;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.style.TtsSpan;
import android.util.AttributeSet;
import android.util.StateSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R;
import com.android.internal.widget.NumericTextView;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

class TimePickerClockDelegate extends TimePicker.AbstractTimePickerDelegate {
  private static final int AM = 0;
  
  private static final int[] ATTRS_DISABLED_ALPHA;
  
  private static final int[] ATTRS_TEXT_COLOR = new int[] { 16842904 };
  
  private static final long DELAY_COMMIT_MILLIS = 2000L;
  
  private static final int FROM_EXTERNAL_API = 0;
  
  private static final int FROM_INPUT_PICKER = 2;
  
  private static final int FROM_RADIAL_PICKER = 1;
  
  private static final int HOURS_IN_HALF_DAY = 12;
  
  private static final int HOUR_INDEX = 0;
  
  private static final int MINUTE_INDEX = 1;
  
  private static final int PM = 1;
  
  private boolean mAllowAutoAdvance;
  
  private final RadioButton mAmLabel;
  
  private final View mAmPmLayout;
  
  private final View.OnClickListener mClickListener;
  
  private final Runnable mCommitHour;
  
  private final Runnable mCommitMinute;
  
  private int mCurrentHour;
  
  private int mCurrentMinute;
  
  private final NumericTextView.OnValueChangedListener mDigitEnteredListener;
  
  private final View.OnFocusChangeListener mFocusListener;
  
  private boolean mHourFormatShowLeadingZero;
  
  private boolean mHourFormatStartsAtZero;
  
  private final NumericTextView mHourView;
  
  private boolean mIs24Hour;
  
  private boolean mIsAmPmAtLeft;
  
  private boolean mIsAmPmAtTop;
  
  private boolean mIsEnabled;
  
  private boolean mLastAnnouncedIsHour;
  
  private CharSequence mLastAnnouncedText;
  
  private final NumericTextView mMinuteView;
  
  private final RadialTimePickerView.OnValueSelectedListener mOnValueSelectedListener;
  
  private final TextInputTimePickerView.OnValueTypedListener mOnValueTypedListener;
  
  private final RadioButton mPmLabel;
  
  private boolean mRadialPickerModeEnabled;
  
  private final View mRadialTimePickerHeader;
  
  private final ImageButton mRadialTimePickerModeButton;
  
  private final String mRadialTimePickerModeEnabledDescription;
  
  private final RadialTimePickerView mRadialTimePickerView;
  
  private final String mSelectHours;
  
  private final String mSelectMinutes;
  
  private final TextView mSeparatorView;
  
  private final Calendar mTempCalendar;
  
  private final View mTextInputPickerHeader;
  
  private final String mTextInputPickerModeEnabledDescription;
  
  private final TextInputTimePickerView mTextInputPickerView;
  
  static {
    ATTRS_DISABLED_ALPHA = new int[] { 16842803 };
  }
  
  public TimePickerClockDelegate(TimePicker paramTimePicker, Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramTimePicker, paramContext);
    ColorStateList colorStateList1;
    this.mRadialPickerModeEnabled = true;
    this.mIsEnabled = true;
    this.mIsAmPmAtLeft = false;
    this.mIsAmPmAtTop = false;
    this.mOnValueSelectedListener = (RadialTimePickerView.OnValueSelectedListener)new Object(this);
    this.mOnValueTypedListener = (TextInputTimePickerView.OnValueTypedListener)new Object(this);
    this.mDigitEnteredListener = (NumericTextView.OnValueChangedListener)new Object(this);
    this.mCommitHour = (Runnable)new Object(this);
    this.mCommitMinute = (Runnable)new Object(this);
    this.mFocusListener = (View.OnFocusChangeListener)new Object(this);
    this.mClickListener = (View.OnClickListener)new Object(this);
    TypedArray typedArray = this.mContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TimePicker, paramInt1, paramInt2);
    LayoutInflater layoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
    Resources resources = this.mContext.getResources();
    this.mSelectHours = resources.getString(17041246);
    this.mSelectMinutes = resources.getString(17041250);
    int i = typedArray.getResourceId(12, 17367339);
    View view = layoutInflater.inflate(i, paramTimePicker);
    view.setSaveFromParentEnabled(false);
    this.mRadialTimePickerHeader = paramTimePicker = view.findViewById(16909541);
    paramTimePicker.setOnTouchListener(new NearestTouchDelegate());
    NumericTextView numericTextView = view.<NumericTextView>findViewById(16909050);
    numericTextView.setOnClickListener(this.mClickListener);
    this.mHourView.setOnFocusChangeListener(this.mFocusListener);
    this.mHourView.setOnDigitEnteredListener(this.mDigitEnteredListener);
    this.mHourView.setAccessibilityDelegate(new ClickActionDelegate(paramContext, 17041246));
    this.mSeparatorView = view.<TextView>findViewById(16909412);
    this.mMinuteView = numericTextView = view.<NumericTextView>findViewById(16909179);
    numericTextView.setOnClickListener(this.mClickListener);
    this.mMinuteView.setOnFocusChangeListener(this.mFocusListener);
    this.mMinuteView.setOnDigitEnteredListener(this.mDigitEnteredListener);
    this.mMinuteView.setAccessibilityDelegate(new ClickActionDelegate(paramContext, 17041250));
    this.mMinuteView.setRange(0, 59);
    this.mAmPmLayout = (View)(numericTextView = view.findViewById(16908755));
    numericTextView.setOnTouchListener(new NearestTouchDelegate());
    String[] arrayOfString = TimePicker.getAmPmStrings(paramContext);
    RadioButton radioButton = this.mAmPmLayout.<RadioButton>findViewById(16908753);
    radioButton.setText(obtainVerbatim(arrayOfString[0]));
    this.mAmLabel.setOnClickListener(this.mClickListener);
    ensureMinimumTextWidth(this.mAmLabel);
    this.mPmLabel = radioButton = this.mAmPmLayout.<RadioButton>findViewById(16909306);
    radioButton.setText(obtainVerbatim(arrayOfString[1]));
    this.mPmLabel.setOnClickListener(this.mClickListener);
    ensureMinimumTextWidth(this.mPmLabel);
    arrayOfString = null;
    i = typedArray.getResourceId(1, 0);
    if (i != 0) {
      TypedArray typedArray1 = this.mContext.obtainStyledAttributes(null, ATTRS_TEXT_COLOR, 0, i);
      colorStateList1 = typedArray1.getColorStateList(0);
      colorStateList1 = applyLegacyColorFixes(colorStateList1);
      typedArray1.recycle();
    } 
    ColorStateList colorStateList2 = colorStateList1;
    if (colorStateList1 == null)
      colorStateList2 = typedArray.getColorStateList(11); 
    this.mTextInputPickerHeader = view.findViewById(16909081);
    if (colorStateList2 != null) {
      this.mHourView.setTextColor(colorStateList2);
      this.mSeparatorView.setTextColor(colorStateList2);
      this.mMinuteView.setTextColor(colorStateList2);
      this.mAmLabel.setTextColor(colorStateList2);
      this.mPmLabel.setTextColor(colorStateList2);
    } 
    if (typedArray.hasValueOrEmpty(0)) {
      this.mRadialTimePickerHeader.setBackground(typedArray.getDrawable(0));
      this.mTextInputPickerHeader.setBackground(typedArray.getDrawable(0));
    } 
    typedArray.recycle();
    RadialTimePickerView radialTimePickerView = view.<RadialTimePickerView>findViewById(16909333);
    radialTimePickerView.applyAttributes(paramAttributeSet, paramInt1, paramInt2);
    this.mRadialTimePickerView.setOnValueSelectedListener(this.mOnValueSelectedListener);
    TextInputTimePickerView textInputTimePickerView = view.<TextInputTimePickerView>findViewById(16909084);
    textInputTimePickerView.setListener(this.mOnValueTypedListener);
    ImageButton imageButton = view.<ImageButton>findViewById(16909556);
    imageButton.setOnClickListener((View.OnClickListener)new Object(this));
    this.mRadialTimePickerModeEnabledDescription = paramContext.getResources().getString(17041410);
    this.mTextInputPickerModeEnabledDescription = paramContext.getResources().getString(17041411);
    this.mAllowAutoAdvance = true;
    updateHourFormat();
    Calendar calendar = Calendar.getInstance(this.mLocale);
    paramInt2 = calendar.get(11);
    paramInt1 = this.mTempCalendar.get(12);
    initialize(paramInt2, paramInt1, this.mIs24Hour, 0);
  }
  
  private void toggleRadialPickerMode() {
    if (this.mRadialPickerModeEnabled) {
      this.mRadialTimePickerView.setVisibility(8);
      this.mRadialTimePickerHeader.setVisibility(8);
      this.mTextInputPickerHeader.setVisibility(0);
      this.mTextInputPickerView.setVisibility(0);
      this.mRadialTimePickerModeButton.setImageResource(17301803);
      this.mRadialTimePickerModeButton.setContentDescription(this.mRadialTimePickerModeEnabledDescription);
      this.mRadialPickerModeEnabled = false;
    } else {
      this.mRadialTimePickerView.setVisibility(0);
      this.mRadialTimePickerHeader.setVisibility(0);
      this.mTextInputPickerHeader.setVisibility(8);
      this.mTextInputPickerView.setVisibility(8);
      this.mRadialTimePickerModeButton.setImageResource(17301881);
      this.mRadialTimePickerModeButton.setContentDescription(this.mTextInputPickerModeEnabledDescription);
      updateTextInputPicker();
      InputMethodManager inputMethodManager = (InputMethodManager)this.mContext.getSystemService(InputMethodManager.class);
      if (inputMethodManager != null)
        inputMethodManager.hideSoftInputFromWindow(this.mDelegator.getWindowToken(), 0); 
      this.mRadialPickerModeEnabled = true;
    } 
  }
  
  public boolean validateInput() {
    return this.mTextInputPickerView.validateInput();
  }
  
  private static void ensureMinimumTextWidth(TextView paramTextView) {
    paramTextView.measure(0, 0);
    int i = paramTextView.getMeasuredWidth();
    paramTextView.setMinWidth(i);
    paramTextView.setMinimumWidth(i);
  }
  
  private void updateHourFormat() {
    boolean bool2;
    Locale locale = this.mLocale;
    if (this.mIs24Hour) {
      str = "Hm";
    } else {
      str = "hm";
    } 
    String str = DateFormat.getBestDateTimePattern(locale, str);
    int i = str.length();
    boolean bool1 = false;
    char c1 = Character.MIN_VALUE;
    int j = 0;
    while (true) {
      bool2 = bool1;
      c2 = c1;
      if (j < i) {
        char c = str.charAt(j);
        if (c == 'H' || c == 'h' || c == 'K' || c == 'k') {
          c1 = c;
          bool2 = bool1;
          c2 = c1;
          if (j + 1 < i) {
            bool2 = bool1;
            c2 = c1;
            if (c == str.charAt(j + 1)) {
              bool2 = true;
              c2 = c1;
            } 
          } 
          break;
        } 
        j++;
        continue;
      } 
      break;
    } 
    this.mHourFormatShowLeadingZero = bool2;
    if (c2 == 'K' || c2 == 'H') {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mHourFormatStartsAtZero = bool2;
    j = true ^ bool2;
    if (this.mIs24Hour) {
      c2 = '\027';
    } else {
      c2 = '\013';
    } 
    this.mHourView.setRange(j, c2 + j);
    this.mHourView.setShowLeadingZeroes(this.mHourFormatShowLeadingZero);
    String[] arrayOfString = DecimalFormatSymbols.getInstance(this.mLocale).getDigitStrings();
    j = 0;
    for (char c2 = Character.MIN_VALUE; c2 < '\n'; c2++)
      j = Math.max(j, arrayOfString[c2].length()); 
    this.mTextInputPickerView.setHourFormat(j * 2);
  }
  
  static final CharSequence obtainVerbatim(String paramString) {
    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
    TtsSpan.VerbatimBuilder verbatimBuilder = new TtsSpan.VerbatimBuilder(paramString);
    TtsSpan ttsSpan = verbatimBuilder.build();
    return spannableStringBuilder.append(paramString, ttsSpan, 0);
  }
  
  private ColorStateList applyLegacyColorFixes(ColorStateList paramColorStateList) {
    int i;
    int j;
    if (paramColorStateList == null || paramColorStateList.hasState(16843518))
      return paramColorStateList; 
    if (paramColorStateList.hasState(16842913)) {
      i = paramColorStateList.getColorForState(StateSet.get(10), 0);
      j = paramColorStateList.getColorForState(StateSet.get(8), 0);
    } else {
      i = paramColorStateList.getDefaultColor();
      TypedArray typedArray = this.mContext.obtainStyledAttributes(ATTRS_DISABLED_ALPHA);
      float f = typedArray.getFloat(0, 0.3F);
      j = multiplyAlphaComponent(i, f);
    } 
    if (i == 0 || j == 0)
      return null; 
    return new ColorStateList(new int[][] { { 16843518 }, , {} }, new int[] { i, j });
  }
  
  private int multiplyAlphaComponent(int paramInt, float paramFloat) {
    int i = (int)((paramInt >> 24 & 0xFF) * paramFloat + 0.5F);
    return i << 24 | 0xFFFFFF & paramInt;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ChangeSource implements Annotation {}
  
  class ClickActionDelegate extends View.AccessibilityDelegate {
    private final AccessibilityNodeInfo.AccessibilityAction mClickAction;
    
    public ClickActionDelegate(TimePickerClockDelegate this$0, int param1Int) {
      this.mClickAction = new AccessibilityNodeInfo.AccessibilityAction(16, this$0.getString(param1Int));
    }
    
    public void onInitializeAccessibilityNodeInfo(View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      super.onInitializeAccessibilityNodeInfo(param1View, param1AccessibilityNodeInfo);
      param1AccessibilityNodeInfo.addAction(this.mClickAction);
    }
  }
  
  private void initialize(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    this.mCurrentHour = paramInt1;
    this.mCurrentMinute = paramInt2;
    this.mIs24Hour = paramBoolean;
    updateUI(paramInt3);
  }
  
  private void updateUI(int paramInt) {
    updateHeaderAmPm();
    updateHeaderHour(this.mCurrentHour, false);
    updateHeaderSeparator();
    updateHeaderMinute(this.mCurrentMinute, false);
    updateRadialPicker(paramInt);
    updateTextInputPicker();
    this.mDelegator.invalidate();
  }
  
  private void updateTextInputPicker() {
    boolean bool;
    TextInputTimePickerView textInputTimePickerView = this.mTextInputPickerView;
    int i = getLocalizedHour(this.mCurrentHour), j = this.mCurrentMinute;
    if (this.mCurrentHour < 12) {
      bool = false;
    } else {
      bool = true;
    } 
    boolean bool1 = this.mIs24Hour, bool2 = this.mHourFormatStartsAtZero;
    textInputTimePickerView.updateTextInputValues(i, j, bool, bool1, bool2);
  }
  
  private void updateRadialPicker(int paramInt) {
    this.mRadialTimePickerView.initialize(this.mCurrentHour, this.mCurrentMinute, this.mIs24Hour);
    setCurrentItemShowing(paramInt, false, true);
  }
  
  private void updateHeaderAmPm() {
    if (this.mIs24Hour) {
      this.mAmPmLayout.setVisibility(8);
    } else {
      boolean bool1;
      String str = DateFormat.getBestDateTimePattern(this.mLocale, "hm");
      boolean bool = str.startsWith("a");
      setAmPmStart(bool);
      if (this.mCurrentHour < 12) {
        bool1 = false;
      } else {
        bool1 = true;
      } 
      updateAmPmLabelStates(bool1);
    } 
  }
  
  private void setAmPmStart(boolean paramBoolean) {
    View view = this.mAmPmLayout;
    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)view.getLayoutParams();
    if (layoutParams.getRule(1) != 0 || layoutParams.getRule(0) != 0) {
      int j, i = (int)((this.mContext.getResources().getDisplayMetrics()).density * 8.0F);
      if (TextUtils.getLayoutDirectionFromLocale(this.mLocale) == 0) {
        boolean bool = paramBoolean;
      } else {
        j = paramBoolean ^ true;
      } 
      if (j != 0) {
        layoutParams.removeRule(1);
        layoutParams.addRule(0, this.mHourView.getId());
      } else {
        layoutParams.removeRule(0);
        layoutParams.addRule(1, this.mMinuteView.getId());
      } 
      if (paramBoolean) {
        layoutParams.setMarginStart(0);
        layoutParams.setMarginEnd(i);
      } else {
        layoutParams.setMarginStart(i);
        layoutParams.setMarginEnd(0);
      } 
      this.mIsAmPmAtLeft = j;
    } else if (layoutParams.getRule(3) != 0 || layoutParams.getRule(2) != 0) {
      if (this.mIsAmPmAtTop == paramBoolean)
        return; 
      if (paramBoolean) {
        i = layoutParams.getRule(3);
        layoutParams.removeRule(3);
        layoutParams.addRule(2, i);
      } else {
        i = layoutParams.getRule(2);
        layoutParams.removeRule(2);
        layoutParams.addRule(3, i);
      } 
      view = this.mRadialTimePickerHeader.findViewById(i);
      int j = view.getPaddingTop();
      int k = view.getPaddingBottom();
      int m = view.getPaddingLeft();
      int i = view.getPaddingRight();
      view.setPadding(m, k, i, j);
      this.mIsAmPmAtTop = paramBoolean;
    } 
    this.mAmPmLayout.setLayoutParams(layoutParams);
  }
  
  public void setDate(int paramInt1, int paramInt2) {
    setHourInternal(paramInt1, 0, true, false);
    setMinuteInternal(paramInt2, 0, false);
    onTimeChanged();
  }
  
  public void setHour(int paramInt) {
    setHourInternal(paramInt, 0, true, true);
  }
  
  private void setHourInternal(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mCurrentHour == paramInt1)
      return; 
    resetAutofilledValue();
    this.mCurrentHour = paramInt1;
    updateHeaderHour(paramInt1, paramBoolean1);
    updateHeaderAmPm();
    boolean bool = true;
    if (paramInt2 != 1) {
      this.mRadialTimePickerView.setCurrentHour(paramInt1);
      RadialTimePickerView radialTimePickerView = this.mRadialTimePickerView;
      if (paramInt1 < 12)
        bool = false; 
      radialTimePickerView.setAmOrPm(bool);
    } 
    if (paramInt2 != 2)
      updateTextInputPicker(); 
    this.mDelegator.invalidate();
    if (paramBoolean2)
      onTimeChanged(); 
  }
  
  public int getHour() {
    int i = this.mRadialTimePickerView.getCurrentHour();
    if (this.mIs24Hour)
      return i; 
    if (this.mRadialTimePickerView.getAmOrPm() == 1)
      return i % 12 + 12; 
    return i % 12;
  }
  
  public void setMinute(int paramInt) {
    setMinuteInternal(paramInt, 0, true);
  }
  
  private void setMinuteInternal(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (this.mCurrentMinute == paramInt1)
      return; 
    resetAutofilledValue();
    this.mCurrentMinute = paramInt1;
    updateHeaderMinute(paramInt1, true);
    if (paramInt2 != 1)
      this.mRadialTimePickerView.setCurrentMinute(paramInt1); 
    if (paramInt2 != 2)
      updateTextInputPicker(); 
    this.mDelegator.invalidate();
    if (paramBoolean)
      onTimeChanged(); 
  }
  
  public int getMinute() {
    return this.mRadialTimePickerView.getCurrentMinute();
  }
  
  public void setIs24Hour(boolean paramBoolean) {
    if (this.mIs24Hour != paramBoolean) {
      this.mIs24Hour = paramBoolean;
      this.mCurrentHour = getHour();
      updateHourFormat();
      updateUI(this.mRadialTimePickerView.getCurrentItemShowing());
    } 
  }
  
  public boolean is24Hour() {
    return this.mIs24Hour;
  }
  
  public void setEnabled(boolean paramBoolean) {
    this.mHourView.setEnabled(paramBoolean);
    this.mMinuteView.setEnabled(paramBoolean);
    this.mAmLabel.setEnabled(paramBoolean);
    this.mPmLabel.setEnabled(paramBoolean);
    this.mRadialTimePickerView.setEnabled(paramBoolean);
    this.mIsEnabled = paramBoolean;
  }
  
  public boolean isEnabled() {
    return this.mIsEnabled;
  }
  
  public int getBaseline() {
    return -1;
  }
  
  public Parcelable onSaveInstanceState(Parcelable paramParcelable) {
    int i = getHour(), j = getMinute();
    return new TimePicker.AbstractTimePickerDelegate.SavedState(paramParcelable, i, j, is24Hour(), getCurrentItemShowing());
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable instanceof TimePicker.AbstractTimePickerDelegate.SavedState) {
      paramParcelable = paramParcelable;
      initialize(paramParcelable.getHour(), paramParcelable.getMinute(), paramParcelable.is24HourMode(), paramParcelable.getCurrentItemShowing());
      this.mRadialTimePickerView.invalidate();
    } 
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    onPopulateAccessibilityEvent(paramAccessibilityEvent);
    return true;
  }
  
  public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    int i;
    String str2;
    if (this.mIs24Hour) {
      i = 0x1 | 0x80;
    } else {
      i = 0x1 | 0x40;
    } 
    this.mTempCalendar.set(11, getHour());
    this.mTempCalendar.set(12, getMinute());
    Context context = this.mContext;
    Calendar calendar = this.mTempCalendar;
    long l = calendar.getTimeInMillis();
    String str1 = DateUtils.formatDateTime(context, l, i);
    if (this.mRadialTimePickerView.getCurrentItemShowing() == 0) {
      str2 = this.mSelectHours;
    } else {
      str2 = this.mSelectMinutes;
    } 
    List<CharSequence> list = paramAccessibilityEvent.getText();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str1);
    stringBuilder.append(" ");
    stringBuilder.append(str2);
    list.add(stringBuilder.toString());
  }
  
  public View getHourView() {
    return (View)this.mHourView;
  }
  
  public View getMinuteView() {
    return (View)this.mMinuteView;
  }
  
  public View getAmView() {
    return this.mAmLabel;
  }
  
  public View getPmView() {
    return this.mPmLabel;
  }
  
  private int getCurrentItemShowing() {
    return this.mRadialTimePickerView.getCurrentItemShowing();
  }
  
  private void onTimeChanged() {
    this.mDelegator.sendAccessibilityEvent(4);
    if (this.mOnTimeChangedListener != null)
      this.mOnTimeChangedListener.onTimeChanged(this.mDelegator, getHour(), getMinute()); 
    if (this.mAutoFillChangeListener != null)
      this.mAutoFillChangeListener.onTimeChanged(this.mDelegator, getHour(), getMinute()); 
  }
  
  private void tryVibrate() {
    this.mDelegator.performHapticFeedback(4);
  }
  
  private void updateAmPmLabelStates(int paramInt) {
    boolean bool1 = false;
    if (paramInt == 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mAmLabel.setActivated(bool2);
    this.mAmLabel.setChecked(bool2);
    boolean bool2 = bool1;
    if (paramInt == 1)
      bool2 = true; 
    this.mPmLabel.setActivated(bool2);
    this.mPmLabel.setChecked(bool2);
  }
  
  private int getLocalizedHour(int paramInt) {
    int i = paramInt;
    if (!this.mIs24Hour)
      i = paramInt % 12; 
    paramInt = i;
    if (!this.mHourFormatStartsAtZero) {
      paramInt = i;
      if (i == 0)
        if (this.mIs24Hour) {
          paramInt = 24;
        } else {
          paramInt = 12;
        }  
    } 
    return paramInt;
  }
  
  private void updateHeaderHour(int paramInt, boolean paramBoolean) {
    paramInt = getLocalizedHour(paramInt);
    this.mHourView.setValue(paramInt);
    if (paramBoolean)
      tryAnnounceForAccessibility(this.mHourView.getText(), true); 
  }
  
  private void updateHeaderMinute(int paramInt, boolean paramBoolean) {
    this.mMinuteView.setValue(paramInt);
    if (paramBoolean)
      tryAnnounceForAccessibility(this.mMinuteView.getText(), false); 
  }
  
  private void updateHeaderSeparator() {
    Locale locale = this.mLocale;
    if (this.mIs24Hour) {
      str = "Hm";
    } else {
      str = "hm";
    } 
    String str = DateFormat.getBestDateTimePattern(locale, str);
    str = getHourMinSeparatorFromPattern(str);
    this.mSeparatorView.setText(str);
    this.mTextInputPickerView.updateSeparator(str);
  }
  
  private static String getHourMinSeparatorFromPattern(String paramString) {
    boolean bool = false;
    for (int i = 0; i < paramString.length(); i++) {
      char c = paramString.charAt(i);
      if (c != ' ')
        if (c != '\'') {
          if (c != 'H' && c != 'K' && c != 'h' && c != 'k') {
            if (bool)
              return Character.toString(paramString.charAt(i)); 
          } else {
            bool = true;
          } 
        } else if (bool) {
          SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(paramString.substring(i));
          i = DateFormat.appendQuotedText(spannableStringBuilder, 0);
          return spannableStringBuilder.subSequence(0, i).toString();
        }  
    } 
    return ":";
  }
  
  private static int lastIndexOfAny(String paramString, char[] paramArrayOfchar) {
    int i = paramArrayOfchar.length;
    if (i > 0)
      for (int j = paramString.length() - 1; j >= 0; j--) {
        char c = paramString.charAt(j);
        for (byte b = 0; b < i; b++) {
          if (c == paramArrayOfchar[b])
            return j; 
        } 
      }  
    return -1;
  }
  
  private void tryAnnounceForAccessibility(CharSequence paramCharSequence, boolean paramBoolean) {
    if (this.mLastAnnouncedIsHour != paramBoolean || !paramCharSequence.equals(this.mLastAnnouncedText)) {
      this.mDelegator.announceForAccessibility(paramCharSequence);
      this.mLastAnnouncedText = paramCharSequence;
      this.mLastAnnouncedIsHour = paramBoolean;
    } 
  }
  
  private void setCurrentItemShowing(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    this.mRadialTimePickerView.setCurrentItemShowing(paramInt, paramBoolean1);
    if (paramInt == 0) {
      if (paramBoolean2)
        this.mDelegator.announceForAccessibility(this.mSelectHours); 
    } else if (paramBoolean2) {
      this.mDelegator.announceForAccessibility(this.mSelectMinutes);
    } 
    NumericTextView numericTextView = this.mHourView;
    paramBoolean2 = false;
    if (paramInt == 0) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    } 
    numericTextView.setActivated(paramBoolean1);
    numericTextView = this.mMinuteView;
    paramBoolean1 = paramBoolean2;
    if (paramInt == 1)
      paramBoolean1 = true; 
    numericTextView.setActivated(paramBoolean1);
  }
  
  private void setAmOrPm(int paramInt) {
    updateAmPmLabelStates(paramInt);
    if (this.mRadialTimePickerView.setAmOrPm(paramInt)) {
      this.mCurrentHour = getHour();
      updateTextInputPicker();
      if (this.mOnTimeChangedListener != null)
        this.mOnTimeChangedListener.onTimeChanged(this.mDelegator, getHour(), getMinute()); 
    } 
  }
  
  class NearestTouchDelegate implements View.OnTouchListener {
    private View mInitialTouchTarget;
    
    private NearestTouchDelegate() {}
    
    public boolean onTouch(View param1View, MotionEvent param1MotionEvent) {
      int i = param1MotionEvent.getActionMasked();
      if (i == 0)
        if (param1View instanceof ViewGroup) {
          ViewGroup viewGroup = (ViewGroup)param1View;
          int j = (int)param1MotionEvent.getX(), k = (int)param1MotionEvent.getY();
          this.mInitialTouchTarget = findNearestChild(viewGroup, j, k);
        } else {
          this.mInitialTouchTarget = null;
        }  
      View view = this.mInitialTouchTarget;
      if (view == null)
        return false; 
      float f1 = (param1View.getScrollX() - view.getLeft());
      float f2 = (param1View.getScrollY() - view.getTop());
      param1MotionEvent.offsetLocation(f1, f2);
      boolean bool = view.dispatchTouchEvent(param1MotionEvent);
      param1MotionEvent.offsetLocation(-f1, -f2);
      if (i == 1 || i == 3)
        this.mInitialTouchTarget = null; 
      return bool;
    }
    
    private View findNearestChild(ViewGroup param1ViewGroup, int param1Int1, int param1Int2) {
      View view = null;
      int i = Integer.MAX_VALUE;
      byte b;
      int j;
      for (b = 0, j = param1ViewGroup.getChildCount(); b < j; b++, i = k) {
        View view1 = param1ViewGroup.getChildAt(b);
        int k = param1Int1 - view1.getLeft() + view1.getWidth() / 2;
        int m = param1Int2 - view1.getTop() + view1.getHeight() / 2;
        m = k * k + m * m;
        k = i;
        if (i > m) {
          view = view1;
          k = m;
        } 
      } 
      return view;
    }
  }
}
