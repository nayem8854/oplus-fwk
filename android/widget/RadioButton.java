package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityNodeInfo;

public class RadioButton extends CompoundButton {
  public RadioButton(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public RadioButton(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842878);
  }
  
  public RadioButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public RadioButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public void toggle() {
    if (!isChecked())
      super.toggle(); 
  }
  
  public CharSequence getAccessibilityClassName() {
    return RadioButton.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    if (getParent() instanceof RadioGroup) {
      RadioGroup radioGroup = (RadioGroup)getParent();
      if (radioGroup.getOrientation() == 0) {
        int i = radioGroup.getIndexWithinVisibleButtons(this);
        boolean bool = isChecked();
        paramAccessibilityNodeInfo.setCollectionItemInfo(AccessibilityNodeInfo.CollectionItemInfo.obtain(0, 1, i, 1, false, bool));
      } else {
        int i = radioGroup.getIndexWithinVisibleButtons(this);
        boolean bool = isChecked();
        paramAccessibilityNodeInfo.setCollectionItemInfo(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, 1, 0, 1, false, bool));
      } 
    } 
  }
}
