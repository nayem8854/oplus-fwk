package android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.Locale;
import libcore.icu.LocaleData;

class CalendarViewLegacyDelegate extends CalendarView.AbstractCalendarViewDelegate {
  private int mListScrollTopOffset = 2;
  
  private int mWeekMinVisibleHeight = 12;
  
  private int mBottomBuffer = 20;
  
  private int mDaysPerWeek = 7;
  
  private float mFriction = 0.05F;
  
  private float mVelocityScale = 0.333F;
  
  private int mCurrentMonthDisplayed = -1;
  
  private boolean mIsScrollingUp = false;
  
  private int mPreviousScrollState = 0;
  
  private int mCurrentScrollState = 0;
  
  private ScrollStateRunnable mScrollStateChangedRunnable = new ScrollStateRunnable();
  
  private static final int ADJUSTMENT_SCROLL_DURATION = 500;
  
  private static final int DAYS_PER_WEEK = 7;
  
  private static final int DEFAULT_DATE_TEXT_SIZE = 14;
  
  private static final int DEFAULT_SHOWN_WEEK_COUNT = 6;
  
  private static final boolean DEFAULT_SHOW_WEEK_NUMBER = true;
  
  private static final int DEFAULT_WEEK_DAY_TEXT_APPEARANCE_RES_ID = -1;
  
  private static final int GOTO_SCROLL_DURATION = 1000;
  
  private static final long MILLIS_IN_DAY = 86400000L;
  
  private static final long MILLIS_IN_WEEK = 604800000L;
  
  private static final int SCROLL_CHANGE_DELAY = 40;
  
  private static final int SCROLL_HYST_WEEKS = 2;
  
  private static final int UNSCALED_BOTTOM_BUFFER = 20;
  
  private static final int UNSCALED_LIST_SCROLL_TOP_OFFSET = 2;
  
  private static final int UNSCALED_SELECTED_DATE_VERTICAL_BAR_WIDTH = 6;
  
  private static final int UNSCALED_WEEK_MIN_VISIBLE_HEIGHT = 12;
  
  private static final int UNSCALED_WEEK_SEPARATOR_LINE_WIDTH = 1;
  
  private WeeksAdapter mAdapter;
  
  private int mDateTextAppearanceResId;
  
  private int mDateTextSize;
  
  private ViewGroup mDayNamesHeader;
  
  private String[] mDayNamesLong;
  
  private String[] mDayNamesShort;
  
  private Calendar mFirstDayOfMonth;
  
  private int mFirstDayOfWeek;
  
  private int mFocusedMonthDateColor;
  
  private ListView mListView;
  
  private Calendar mMaxDate;
  
  private Calendar mMinDate;
  
  private TextView mMonthName;
  
  private CalendarView.OnDateChangeListener mOnDateChangeListener;
  
  private long mPreviousScrollPosition;
  
  private Drawable mSelectedDateVerticalBar;
  
  private final int mSelectedDateVerticalBarWidth;
  
  private int mSelectedWeekBackgroundColor;
  
  private boolean mShowWeekNumber;
  
  private int mShownWeekCount;
  
  private Calendar mTempDate;
  
  private int mUnfocusedMonthDateColor;
  
  private int mWeekDayTextAppearanceResId;
  
  private int mWeekNumberColor;
  
  private int mWeekSeparatorLineColor;
  
  private final int mWeekSeparatorLineWidth;
  
  CalendarViewLegacyDelegate(CalendarView paramCalendarView, Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramCalendarView, paramContext);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CalendarView, paramInt1, paramInt2);
    this.mShowWeekNumber = typedArray.getBoolean(1, true);
    paramInt1 = (LocaleData.get(Locale.getDefault())).firstDayOfWeek.intValue();
    this.mFirstDayOfWeek = typedArray.getInt(0, paramInt1);
    String str = typedArray.getString(2);
    if (!CalendarView.parseDate(str, this.mMinDate))
      CalendarView.parseDate("01/01/1900", this.mMinDate); 
    str = typedArray.getString(3);
    if (!CalendarView.parseDate(str, this.mMaxDate))
      CalendarView.parseDate("01/01/2100", this.mMaxDate); 
    if (!this.mMaxDate.before(this.mMinDate)) {
      this.mShownWeekCount = typedArray.getInt(4, 6);
      this.mSelectedWeekBackgroundColor = typedArray.getColor(5, 0);
      this.mFocusedMonthDateColor = typedArray.getColor(6, 0);
      this.mUnfocusedMonthDateColor = typedArray.getColor(7, 0);
      this.mWeekSeparatorLineColor = typedArray.getColor(9, 0);
      this.mWeekNumberColor = typedArray.getColor(8, 0);
      this.mSelectedDateVerticalBar = typedArray.getDrawable(10);
      this.mDateTextAppearanceResId = typedArray.getResourceId(12, 16973894);
      updateDateTextSize();
      this.mWeekDayTextAppearanceResId = typedArray.getResourceId(11, -1);
      typedArray.recycle();
      DisplayMetrics displayMetrics = this.mDelegator.getResources().getDisplayMetrics();
      this.mWeekMinVisibleHeight = (int)TypedValue.applyDimension(1, 12.0F, displayMetrics);
      this.mListScrollTopOffset = (int)TypedValue.applyDimension(1, 2.0F, displayMetrics);
      this.mBottomBuffer = (int)TypedValue.applyDimension(1, 20.0F, displayMetrics);
      this.mSelectedDateVerticalBarWidth = (int)TypedValue.applyDimension(1, 6.0F, displayMetrics);
      this.mWeekSeparatorLineWidth = (int)TypedValue.applyDimension(1, 1.0F, displayMetrics);
      Context context = this.mContext;
      LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
      View view = layoutInflater.inflate(17367108, (ViewGroup)null, false);
      this.mDelegator.addView(view);
      this.mListView = this.mDelegator.<ListView>findViewById(16908298);
      this.mDayNamesHeader = view.<ViewGroup>findViewById(16908914);
      this.mMonthName = view.<TextView>findViewById(16909191);
      setUpHeader();
      setUpListView();
      setUpAdapter();
      this.mTempDate.setTimeInMillis(System.currentTimeMillis());
      if (this.mTempDate.before(this.mMinDate)) {
        goTo(this.mMinDate, false, true, true);
      } else if (this.mMaxDate.before(this.mTempDate)) {
        goTo(this.mMaxDate, false, true, true);
      } else {
        goTo(this.mTempDate, false, true, true);
      } 
      this.mDelegator.invalidate();
      return;
    } 
    throw new IllegalArgumentException("Max date cannot be before min date.");
  }
  
  public void setShownWeekCount(int paramInt) {
    if (this.mShownWeekCount != paramInt) {
      this.mShownWeekCount = paramInt;
      this.mDelegator.invalidate();
    } 
  }
  
  public int getShownWeekCount() {
    return this.mShownWeekCount;
  }
  
  public void setSelectedWeekBackgroundColor(int paramInt) {
    if (this.mSelectedWeekBackgroundColor != paramInt) {
      this.mSelectedWeekBackgroundColor = paramInt;
      int i = this.mListView.getChildCount();
      for (paramInt = 0; paramInt < i; paramInt++) {
        WeekView weekView = (WeekView)this.mListView.getChildAt(paramInt);
        if (weekView.mHasSelectedDay)
          weekView.invalidate(); 
      } 
    } 
  }
  
  public int getSelectedWeekBackgroundColor() {
    return this.mSelectedWeekBackgroundColor;
  }
  
  public void setFocusedMonthDateColor(int paramInt) {
    if (this.mFocusedMonthDateColor != paramInt) {
      this.mFocusedMonthDateColor = paramInt;
      int i = this.mListView.getChildCount();
      for (paramInt = 0; paramInt < i; paramInt++) {
        WeekView weekView = (WeekView)this.mListView.getChildAt(paramInt);
        if (weekView.mHasFocusedDay)
          weekView.invalidate(); 
      } 
    } 
  }
  
  public int getFocusedMonthDateColor() {
    return this.mFocusedMonthDateColor;
  }
  
  public void setUnfocusedMonthDateColor(int paramInt) {
    if (this.mUnfocusedMonthDateColor != paramInt) {
      this.mUnfocusedMonthDateColor = paramInt;
      int i = this.mListView.getChildCount();
      for (paramInt = 0; paramInt < i; paramInt++) {
        WeekView weekView = (WeekView)this.mListView.getChildAt(paramInt);
        if (weekView.mHasUnfocusedDay)
          weekView.invalidate(); 
      } 
    } 
  }
  
  public int getUnfocusedMonthDateColor() {
    return this.mUnfocusedMonthDateColor;
  }
  
  public void setWeekNumberColor(int paramInt) {
    if (this.mWeekNumberColor != paramInt) {
      this.mWeekNumberColor = paramInt;
      if (this.mShowWeekNumber)
        invalidateAllWeekViews(); 
    } 
  }
  
  public int getWeekNumberColor() {
    return this.mWeekNumberColor;
  }
  
  public void setWeekSeparatorLineColor(int paramInt) {
    if (this.mWeekSeparatorLineColor != paramInt) {
      this.mWeekSeparatorLineColor = paramInt;
      invalidateAllWeekViews();
    } 
  }
  
  public int getWeekSeparatorLineColor() {
    return this.mWeekSeparatorLineColor;
  }
  
  public void setSelectedDateVerticalBar(int paramInt) {
    Drawable drawable = this.mDelegator.getContext().getDrawable(paramInt);
    setSelectedDateVerticalBar(drawable);
  }
  
  public void setSelectedDateVerticalBar(Drawable paramDrawable) {
    if (this.mSelectedDateVerticalBar != paramDrawable) {
      this.mSelectedDateVerticalBar = paramDrawable;
      int i = this.mListView.getChildCount();
      for (byte b = 0; b < i; b++) {
        WeekView weekView = (WeekView)this.mListView.getChildAt(b);
        if (weekView.mHasSelectedDay)
          weekView.invalidate(); 
      } 
    } 
  }
  
  public Drawable getSelectedDateVerticalBar() {
    return this.mSelectedDateVerticalBar;
  }
  
  public void setWeekDayTextAppearance(int paramInt) {
    if (this.mWeekDayTextAppearanceResId != paramInt) {
      this.mWeekDayTextAppearanceResId = paramInt;
      setUpHeader();
    } 
  }
  
  public int getWeekDayTextAppearance() {
    return this.mWeekDayTextAppearanceResId;
  }
  
  public void setDateTextAppearance(int paramInt) {
    if (this.mDateTextAppearanceResId != paramInt) {
      this.mDateTextAppearanceResId = paramInt;
      updateDateTextSize();
      invalidateAllWeekViews();
    } 
  }
  
  public int getDateTextAppearance() {
    return this.mDateTextAppearanceResId;
  }
  
  public void setMinDate(long paramLong) {
    this.mTempDate.setTimeInMillis(paramLong);
    if (isSameDate(this.mTempDate, this.mMinDate))
      return; 
    this.mMinDate.setTimeInMillis(paramLong);
    Calendar calendar = this.mAdapter.mSelectedDate;
    if (calendar.before(this.mMinDate))
      this.mAdapter.setSelectedDay(this.mMinDate); 
    this.mAdapter.init();
    if (calendar.before(this.mMinDate)) {
      setDate(this.mTempDate.getTimeInMillis());
    } else {
      goTo(calendar, false, true, false);
    } 
  }
  
  public long getMinDate() {
    return this.mMinDate.getTimeInMillis();
  }
  
  public void setMaxDate(long paramLong) {
    this.mTempDate.setTimeInMillis(paramLong);
    if (isSameDate(this.mTempDate, this.mMaxDate))
      return; 
    this.mMaxDate.setTimeInMillis(paramLong);
    this.mAdapter.init();
    Calendar calendar = this.mAdapter.mSelectedDate;
    if (calendar.after(this.mMaxDate)) {
      setDate(this.mMaxDate.getTimeInMillis());
    } else {
      goTo(calendar, false, true, false);
    } 
  }
  
  public long getMaxDate() {
    return this.mMaxDate.getTimeInMillis();
  }
  
  public void setShowWeekNumber(boolean paramBoolean) {
    if (this.mShowWeekNumber == paramBoolean)
      return; 
    this.mShowWeekNumber = paramBoolean;
    this.mAdapter.notifyDataSetChanged();
    setUpHeader();
  }
  
  public boolean getShowWeekNumber() {
    return this.mShowWeekNumber;
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    if (this.mFirstDayOfWeek == paramInt)
      return; 
    this.mFirstDayOfWeek = paramInt;
    this.mAdapter.init();
    this.mAdapter.notifyDataSetChanged();
    setUpHeader();
  }
  
  public int getFirstDayOfWeek() {
    return this.mFirstDayOfWeek;
  }
  
  public void setDate(long paramLong) {
    setDate(paramLong, false, false);
  }
  
  public void setDate(long paramLong, boolean paramBoolean1, boolean paramBoolean2) {
    this.mTempDate.setTimeInMillis(paramLong);
    if (isSameDate(this.mTempDate, this.mAdapter.mSelectedDate))
      return; 
    goTo(this.mTempDate, paramBoolean1, true, paramBoolean2);
  }
  
  public long getDate() {
    return this.mAdapter.mSelectedDate.getTimeInMillis();
  }
  
  public void setOnDateChangeListener(CalendarView.OnDateChangeListener paramOnDateChangeListener) {
    this.mOnDateChangeListener = paramOnDateChangeListener;
  }
  
  public boolean getBoundsForDate(long paramLong, Rect paramRect) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(paramLong);
    int i = this.mListView.getCount();
    for (int j = 0; j < i; j++) {
      WeekView weekView = (WeekView)this.mListView.getChildAt(j);
      if (weekView.getBoundsForDate(calendar, paramRect)) {
        int[] arrayOfInt1 = new int[2];
        int[] arrayOfInt2 = new int[2];
        weekView.getLocationOnScreen(arrayOfInt1);
        this.mDelegator.getLocationOnScreen(arrayOfInt2);
        j = arrayOfInt1[1] - arrayOfInt2[1];
        paramRect.top += j;
        paramRect.bottom += j;
        return true;
      } 
    } 
    return false;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    setCurrentLocale(paramConfiguration.locale);
  }
  
  protected void setCurrentLocale(Locale paramLocale) {
    super.setCurrentLocale(paramLocale);
    this.mTempDate = getCalendarForLocale(this.mTempDate, paramLocale);
    this.mFirstDayOfMonth = getCalendarForLocale(this.mFirstDayOfMonth, paramLocale);
    this.mMinDate = getCalendarForLocale(this.mMinDate, paramLocale);
    this.mMaxDate = getCalendarForLocale(this.mMaxDate, paramLocale);
  }
  
  private void updateDateTextSize() {
    TypedArray typedArray = this.mDelegator.getContext().obtainStyledAttributes(this.mDateTextAppearanceResId, R.styleable.TextAppearance);
    this.mDateTextSize = typedArray.getDimensionPixelSize(0, 14);
    typedArray.recycle();
  }
  
  private void invalidateAllWeekViews() {
    int i = this.mListView.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = this.mListView.getChildAt(b);
      view.invalidate();
    } 
  }
  
  private static Calendar getCalendarForLocale(Calendar paramCalendar, Locale paramLocale) {
    if (paramCalendar == null)
      return Calendar.getInstance(paramLocale); 
    long l = paramCalendar.getTimeInMillis();
    paramCalendar = Calendar.getInstance(paramLocale);
    paramCalendar.setTimeInMillis(l);
    return paramCalendar;
  }
  
  private static boolean isSameDate(Calendar paramCalendar1, Calendar paramCalendar2) {
    int i = paramCalendar1.get(6), j = paramCalendar2.get(6);
    boolean bool = true;
    if (i != j || 
      paramCalendar1.get(1) != paramCalendar2.get(1))
      bool = false; 
    return bool;
  }
  
  private void setUpAdapter() {
    if (this.mAdapter == null) {
      WeeksAdapter weeksAdapter = new WeeksAdapter(this.mContext);
      weeksAdapter.registerDataSetObserver((DataSetObserver)new Object(this));
      this.mListView.setAdapter(this.mAdapter);
    } 
    this.mAdapter.notifyDataSetChanged();
  }
  
  private void setUpHeader() {
    int i = this.mDaysPerWeek;
    this.mDayNamesShort = new String[i];
    this.mDayNamesLong = new String[i];
    int j, k;
    for (j = this.mFirstDayOfWeek, k = this.mFirstDayOfWeek; j < k + i; j++) {
      int n;
      if (j > 7) {
        n = j - 7;
      } else {
        n = j;
      } 
      this.mDayNamesShort[j - this.mFirstDayOfWeek] = DateUtils.getDayOfWeekString(n, 50);
      this.mDayNamesLong[j - this.mFirstDayOfWeek] = DateUtils.getDayOfWeekString(n, 10);
    } 
    TextView textView = (TextView)this.mDayNamesHeader.getChildAt(0);
    if (this.mShowWeekNumber) {
      textView.setVisibility(0);
    } else {
      textView.setVisibility(8);
    } 
    int m;
    for (j = 1, m = this.mDayNamesHeader.getChildCount(); j < m; j++) {
      textView = (TextView)this.mDayNamesHeader.getChildAt(j);
      k = this.mWeekDayTextAppearanceResId;
      if (k > -1)
        textView.setTextAppearance(k); 
      if (j < this.mDaysPerWeek + 1) {
        textView.setText(this.mDayNamesShort[j - 1]);
        textView.setContentDescription(this.mDayNamesLong[j - 1]);
        textView.setVisibility(0);
      } else {
        textView.setVisibility(8);
      } 
    } 
    this.mDayNamesHeader.invalidate();
  }
  
  private void setUpListView() {
    this.mListView.setDivider((Drawable)null);
    this.mListView.setItemsCanFocus(true);
    this.mListView.setVerticalScrollBarEnabled(false);
    this.mListView.setOnScrollListener((AbsListView.OnScrollListener)new Object(this));
    this.mListView.setFriction(this.mFriction);
    this.mListView.setVelocityScale(this.mVelocityScale);
  }
  
  private void goTo(Calendar paramCalendar, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    if (!paramCalendar.before(this.mMinDate) && !paramCalendar.after(this.mMaxDate)) {
      int i = this.mListView.getFirstVisiblePosition();
      View view = this.mListView.getChildAt(0);
      int j = i;
      if (view != null) {
        j = i;
        if (view.getTop() < 0)
          j = i + 1; 
      } 
      int k = this.mShownWeekCount + j - 1;
      i = k;
      if (view != null) {
        i = k;
        if (view.getTop() > this.mBottomBuffer)
          i = k - 1; 
      } 
      if (paramBoolean2)
        this.mAdapter.setSelectedDay(paramCalendar); 
      k = getWeeksSinceMinDate(paramCalendar);
      if (k < j || k > i || paramBoolean3) {
        this.mFirstDayOfMonth.setTimeInMillis(paramCalendar.getTimeInMillis());
        this.mFirstDayOfMonth.set(5, 1);
        setMonthDisplayed(this.mFirstDayOfMonth);
        if (this.mFirstDayOfMonth.before(this.mMinDate)) {
          j = 0;
        } else {
          j = getWeeksSinceMinDate(this.mFirstDayOfMonth);
        } 
        this.mPreviousScrollState = 2;
        if (paramBoolean1) {
          this.mListView.smoothScrollToPositionFromTop(j, this.mListScrollTopOffset, 1000);
        } else {
          this.mListView.setSelectionFromTop(j, this.mListScrollTopOffset);
          onScrollStateChanged(this.mListView, 0);
        } 
        return;
      } 
      if (paramBoolean2)
        setMonthDisplayed(paramCalendar); 
      return;
    } 
    throw new IllegalArgumentException("timeInMillis must be between the values of getMinDate() and getMaxDate()");
  }
  
  private void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
    this.mScrollStateChangedRunnable.doScrollStateChange(paramAbsListView, paramInt);
  }
  
  private void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3) {
    paramInt1 = 0;
    WeekView weekView = (WeekView)paramAbsListView.getChildAt(0);
    if (weekView == null)
      return; 
    long l1 = (paramAbsListView.getFirstVisiblePosition() * weekView.getHeight() - weekView.getBottom());
    long l2 = this.mPreviousScrollPosition;
    if (l1 < l2) {
      this.mIsScrollingUp = true;
    } else if (l1 > l2) {
      this.mIsScrollingUp = false;
    } else {
      return;
    } 
    if (weekView.getBottom() < this.mWeekMinVisibleHeight)
      paramInt1 = 1; 
    if (this.mIsScrollingUp) {
      weekView = (WeekView)paramAbsListView.getChildAt(paramInt1 + 2);
    } else if (paramInt1 != 0) {
      weekView = (WeekView)paramAbsListView.getChildAt(paramInt1);
    } 
    if (weekView != null) {
      if (this.mIsScrollingUp) {
        paramInt1 = weekView.getMonthOfFirstWeekDay();
      } else {
        paramInt1 = weekView.getMonthOfLastWeekDay();
      } 
      if (this.mCurrentMonthDisplayed == 11 && paramInt1 == 0) {
        paramInt1 = 1;
      } else if (this.mCurrentMonthDisplayed == 0 && paramInt1 == 11) {
        paramInt1 = -1;
      } else {
        paramInt1 -= this.mCurrentMonthDisplayed;
      } 
      if ((!this.mIsScrollingUp && paramInt1 > 0) || (this.mIsScrollingUp && paramInt1 < 0)) {
        Calendar calendar = weekView.getFirstDay();
        if (this.mIsScrollingUp) {
          calendar.add(5, -7);
        } else {
          calendar.add(5, 7);
        } 
        setMonthDisplayed(calendar);
      } 
    } 
    this.mPreviousScrollPosition = l1;
    this.mPreviousScrollState = this.mCurrentScrollState;
  }
  
  private void setMonthDisplayed(Calendar paramCalendar) {
    int i = paramCalendar.get(2);
    this.mAdapter.setFocusMonth(i);
    long l = paramCalendar.getTimeInMillis();
    String str = DateUtils.formatDateRange(this.mContext, l, l, 52);
    this.mMonthName.setText(str);
    this.mMonthName.invalidate();
  }
  
  private int getWeeksSinceMinDate(Calendar paramCalendar) {
    if (!paramCalendar.before(this.mMinDate)) {
      long l1 = paramCalendar.getTimeInMillis();
      long l2 = paramCalendar.getTimeZone().getOffset(paramCalendar.getTimeInMillis());
      long l3 = this.mMinDate.getTimeInMillis();
      paramCalendar = this.mMinDate;
      long l4 = paramCalendar.getTimeZone().getOffset(this.mMinDate.getTimeInMillis());
      long l5 = (this.mMinDate.get(7) - this.mFirstDayOfWeek);
      return (int)((l1 + l2 - l3 + l4 + l5 * 86400000L) / 604800000L);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("fromDate: ");
    stringBuilder.append(this.mMinDate.getTime());
    stringBuilder.append(" does not precede toDate: ");
    stringBuilder.append(paramCalendar.getTime());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  class ScrollStateRunnable implements Runnable {
    private int mNewState;
    
    private AbsListView mView;
    
    final CalendarViewLegacyDelegate this$0;
    
    private ScrollStateRunnable() {}
    
    public void doScrollStateChange(AbsListView param1AbsListView, int param1Int) {
      this.mView = param1AbsListView;
      this.mNewState = param1Int;
      CalendarViewLegacyDelegate.this.mDelegator.removeCallbacks(this);
      CalendarViewLegacyDelegate.this.mDelegator.postDelayed(this, 40L);
    }
    
    public void run() {
      CalendarViewLegacyDelegate.access$1002(CalendarViewLegacyDelegate.this, this.mNewState);
      if (this.mNewState == 0) {
        CalendarViewLegacyDelegate calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
        if (calendarViewLegacyDelegate.mPreviousScrollState != 0) {
          View view = this.mView.getChildAt(0);
          if (view == null)
            return; 
          int i = view.getBottom() - CalendarViewLegacyDelegate.this.mListScrollTopOffset;
          if (i > CalendarViewLegacyDelegate.this.mListScrollTopOffset)
            if (CalendarViewLegacyDelegate.this.mIsScrollingUp) {
              this.mView.smoothScrollBy(i - view.getHeight(), 500);
            } else {
              this.mView.smoothScrollBy(i, 500);
            }  
        } 
      } 
      CalendarViewLegacyDelegate.access$1102(CalendarViewLegacyDelegate.this, this.mNewState);
    }
  }
  
  class WeeksAdapter extends BaseAdapter implements View.OnTouchListener {
    private int mFocusedMonth;
    
    private GestureDetector mGestureDetector;
    
    private final Calendar mSelectedDate = Calendar.getInstance();
    
    private int mSelectedWeek;
    
    private int mTotalWeekCount;
    
    final CalendarViewLegacyDelegate this$0;
    
    public WeeksAdapter(Context param1Context) {
      CalendarViewLegacyDelegate.this.mContext = param1Context;
      this.mGestureDetector = new GestureDetector(CalendarViewLegacyDelegate.this.mContext, new CalendarGestureListener());
      init();
    }
    
    private void init() {
      // Byte code:
      //   0: aload_0
      //   1: aload_0
      //   2: getfield this$0 : Landroid/widget/CalendarViewLegacyDelegate;
      //   5: aload_0
      //   6: getfield mSelectedDate : Landroid/icu/util/Calendar;
      //   9: invokestatic access$1400 : (Landroid/widget/CalendarViewLegacyDelegate;Landroid/icu/util/Calendar;)I
      //   12: putfield mSelectedWeek : I
      //   15: aload_0
      //   16: getfield this$0 : Landroid/widget/CalendarViewLegacyDelegate;
      //   19: astore_1
      //   20: aload_0
      //   21: aload_1
      //   22: aload_1
      //   23: invokestatic access$1500 : (Landroid/widget/CalendarViewLegacyDelegate;)Landroid/icu/util/Calendar;
      //   26: invokestatic access$1400 : (Landroid/widget/CalendarViewLegacyDelegate;Landroid/icu/util/Calendar;)I
      //   29: putfield mTotalWeekCount : I
      //   32: aload_0
      //   33: getfield this$0 : Landroid/widget/CalendarViewLegacyDelegate;
      //   36: invokestatic access$1600 : (Landroid/widget/CalendarViewLegacyDelegate;)Landroid/icu/util/Calendar;
      //   39: bipush #7
      //   41: invokevirtual get : (I)I
      //   44: aload_0
      //   45: getfield this$0 : Landroid/widget/CalendarViewLegacyDelegate;
      //   48: invokestatic access$1700 : (Landroid/widget/CalendarViewLegacyDelegate;)I
      //   51: if_icmpne -> 78
      //   54: aload_0
      //   55: getfield this$0 : Landroid/widget/CalendarViewLegacyDelegate;
      //   58: astore_1
      //   59: aload_1
      //   60: invokestatic access$1500 : (Landroid/widget/CalendarViewLegacyDelegate;)Landroid/icu/util/Calendar;
      //   63: bipush #7
      //   65: invokevirtual get : (I)I
      //   68: aload_0
      //   69: getfield this$0 : Landroid/widget/CalendarViewLegacyDelegate;
      //   72: invokestatic access$1700 : (Landroid/widget/CalendarViewLegacyDelegate;)I
      //   75: if_icmpeq -> 88
      //   78: aload_0
      //   79: aload_0
      //   80: getfield mTotalWeekCount : I
      //   83: iconst_1
      //   84: iadd
      //   85: putfield mTotalWeekCount : I
      //   88: aload_0
      //   89: invokevirtual notifyDataSetChanged : ()V
      //   92: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1037	-> 0
      //   #1038	-> 15
      //   #1039	-> 32
      //   #1040	-> 59
      //   #1041	-> 78
      //   #1043	-> 88
      //   #1044	-> 92
    }
    
    public void setSelectedDay(Calendar param1Calendar) {
      if (param1Calendar.get(6) == this.mSelectedDate.get(6) && 
        param1Calendar.get(1) == this.mSelectedDate.get(1))
        return; 
      this.mSelectedDate.setTimeInMillis(param1Calendar.getTimeInMillis());
      this.mSelectedWeek = CalendarViewLegacyDelegate.this.getWeeksSinceMinDate(this.mSelectedDate);
      this.mFocusedMonth = this.mSelectedDate.get(2);
      notifyDataSetChanged();
    }
    
    public Calendar getSelectedDay() {
      return this.mSelectedDate;
    }
    
    public int getCount() {
      return this.mTotalWeekCount;
    }
    
    public Object getItem(int param1Int) {
      return null;
    }
    
    public long getItemId(int param1Int) {
      return param1Int;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      CalendarViewLegacyDelegate.WeekView weekView;
      byte b;
      if (param1View != null) {
        param1View = param1View;
      } else {
        CalendarViewLegacyDelegate calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
        weekView = new CalendarViewLegacyDelegate.WeekView(calendarViewLegacyDelegate.mContext);
        AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(-2, -2);
        weekView.setLayoutParams(layoutParams);
        weekView.setClickable(true);
        weekView.setOnTouchListener(this);
      } 
      if (this.mSelectedWeek == param1Int) {
        b = this.mSelectedDate.get(7);
      } else {
        b = -1;
      } 
      weekView.init(param1Int, b, this.mFocusedMonth);
      return weekView;
    }
    
    public void setFocusMonth(int param1Int) {
      if (this.mFocusedMonth == param1Int)
        return; 
      this.mFocusedMonth = param1Int;
      notifyDataSetChanged();
    }
    
    public boolean onTouch(View param1View, MotionEvent param1MotionEvent) {
      if (CalendarViewLegacyDelegate.this.mListView.isEnabled() && this.mGestureDetector.onTouchEvent(param1MotionEvent)) {
        param1View = param1View;
        if (!param1View.getDayFromLocation(param1MotionEvent.getX(), CalendarViewLegacyDelegate.this.mTempDate))
          return true; 
        if (CalendarViewLegacyDelegate.this.mTempDate.before(CalendarViewLegacyDelegate.this.mMinDate) || CalendarViewLegacyDelegate.this.mTempDate.after(CalendarViewLegacyDelegate.this.mMaxDate))
          return true; 
        onDateTapped(CalendarViewLegacyDelegate.this.mTempDate);
        return true;
      } 
      return false;
    }
    
    private void onDateTapped(Calendar param1Calendar) {
      setSelectedDay(param1Calendar);
      CalendarViewLegacyDelegate.this.setMonthDisplayed(param1Calendar);
    }
    
    class CalendarGestureListener extends GestureDetector.SimpleOnGestureListener {
      final CalendarViewLegacyDelegate.WeeksAdapter this$1;
      
      public boolean onSingleTapUp(MotionEvent param2MotionEvent) {
        return true;
      }
    }
  }
  
  class CalendarGestureListener extends GestureDetector.SimpleOnGestureListener {
    final CalendarViewLegacyDelegate.WeeksAdapter this$1;
    
    public boolean onSingleTapUp(MotionEvent param1MotionEvent) {
      return true;
    }
  }
  
  class WeekView extends View {
    private final Rect mTempRect = new Rect();
    
    private final Paint mDrawPaint = new Paint();
    
    private final Paint mMonthNumDrawPaint = new Paint();
    
    private int mMonthOfFirstWeekDay = -1;
    
    private int mLastWeekDayMonth = -1;
    
    private int mWeek = -1;
    
    private boolean mHasSelectedDay = false;
    
    private int mSelectedDay = -1;
    
    private int mSelectedLeft = -1;
    
    private int mSelectedRight = -1;
    
    private String[] mDayNumbers;
    
    private Calendar mFirstDay;
    
    private boolean[] mFocusDay;
    
    private boolean mHasFocusedDay;
    
    private boolean mHasUnfocusedDay;
    
    private int mHeight;
    
    private int mNumCells;
    
    private int mWidth;
    
    final CalendarViewLegacyDelegate this$0;
    
    public WeekView(Context param1Context) {
      super(param1Context);
      initializePaints();
    }
    
    public void init(int param1Int1, int param1Int2, int param1Int3) {
      boolean bool;
      this.mSelectedDay = param1Int2;
      if (param1Int2 != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mHasSelectedDay = bool;
      if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
        param1Int2 = CalendarViewLegacyDelegate.this.mDaysPerWeek + 1;
      } else {
        param1Int2 = CalendarViewLegacyDelegate.this.mDaysPerWeek;
      } 
      this.mNumCells = param1Int2;
      this.mWeek = param1Int1;
      CalendarViewLegacyDelegate.this.mTempDate.setTimeInMillis(CalendarViewLegacyDelegate.this.mMinDate.getTimeInMillis());
      CalendarViewLegacyDelegate.this.mTempDate.add(3, this.mWeek);
      CalendarViewLegacyDelegate.this.mTempDate.setFirstDayOfWeek(CalendarViewLegacyDelegate.this.mFirstDayOfWeek);
      param1Int1 = this.mNumCells;
      this.mDayNumbers = new String[param1Int1];
      this.mFocusDay = new boolean[param1Int1];
      param1Int1 = 0;
      if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
        String[] arrayOfString = this.mDayNumbers;
        Locale locale = Locale.getDefault();
        CalendarViewLegacyDelegate calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
        param1Int1 = calendarViewLegacyDelegate.mTempDate.get(3);
        arrayOfString[0] = String.format(locale, "%d", new Object[] { Integer.valueOf(param1Int1) });
        param1Int1 = 0 + 1;
      } 
      param1Int2 = CalendarViewLegacyDelegate.this.mFirstDayOfWeek;
      int i = CalendarViewLegacyDelegate.this.mTempDate.get(7);
      CalendarViewLegacyDelegate.this.mTempDate.add(5, param1Int2 - i);
      this.mFirstDay = (Calendar)CalendarViewLegacyDelegate.this.mTempDate.clone();
      this.mMonthOfFirstWeekDay = CalendarViewLegacyDelegate.this.mTempDate.get(2);
      this.mHasUnfocusedDay = true;
      for (; param1Int1 < this.mNumCells; param1Int1++) {
        if (CalendarViewLegacyDelegate.this.mTempDate.get(2) == param1Int3) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mFocusDay[param1Int1] = bool;
        this.mHasFocusedDay |= bool;
        int j = this.mHasUnfocusedDay;
        if (!bool) {
          param1Int2 = 1;
        } else {
          param1Int2 = 0;
        } 
        this.mHasUnfocusedDay = j & param1Int2;
        if (CalendarViewLegacyDelegate.this.mTempDate.before(CalendarViewLegacyDelegate.this.mMinDate) || CalendarViewLegacyDelegate.this.mTempDate.after(CalendarViewLegacyDelegate.this.mMaxDate)) {
          this.mDayNumbers[param1Int1] = "";
        } else {
          String[] arrayOfString = this.mDayNumbers;
          Locale locale = Locale.getDefault();
          CalendarViewLegacyDelegate calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
          int k = calendarViewLegacyDelegate.mTempDate.get(5);
          arrayOfString[param1Int1] = String.format(locale, "%d", new Object[] { Integer.valueOf(k) });
        } 
        CalendarViewLegacyDelegate.this.mTempDate.add(5, 1);
      } 
      if (CalendarViewLegacyDelegate.this.mTempDate.get(5) == 1)
        CalendarViewLegacyDelegate.this.mTempDate.add(5, -1); 
      this.mLastWeekDayMonth = CalendarViewLegacyDelegate.this.mTempDate.get(2);
      updateSelectionPositions();
    }
    
    private void initializePaints() {
      this.mDrawPaint.setFakeBoldText(false);
      this.mDrawPaint.setAntiAlias(true);
      this.mDrawPaint.setStyle(Paint.Style.FILL);
      this.mMonthNumDrawPaint.setFakeBoldText(true);
      this.mMonthNumDrawPaint.setAntiAlias(true);
      this.mMonthNumDrawPaint.setStyle(Paint.Style.FILL);
      this.mMonthNumDrawPaint.setTextAlign(Paint.Align.CENTER);
      this.mMonthNumDrawPaint.setTextSize(CalendarViewLegacyDelegate.this.mDateTextSize);
    }
    
    public int getMonthOfFirstWeekDay() {
      return this.mMonthOfFirstWeekDay;
    }
    
    public int getMonthOfLastWeekDay() {
      return this.mLastWeekDayMonth;
    }
    
    public Calendar getFirstDay() {
      return this.mFirstDay;
    }
    
    public boolean getDayFromLocation(float param1Float, Calendar param1Calendar) {
      int k;
      boolean bool = isLayoutRtl();
      if (bool) {
        int m;
        i = 0;
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
          m = this.mWidth;
          m -= m / this.mNumCells;
        } else {
          m = this.mWidth;
        } 
        k = m;
      } else {
        byte b;
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
          b = this.mWidth / this.mNumCells;
        } else {
          b = 0;
        } 
        k = this.mWidth;
        i = b;
      } 
      if (param1Float < i || param1Float > k) {
        param1Calendar.clear();
        return false;
      } 
      int i = (int)((param1Float - i) * CalendarViewLegacyDelegate.this.mDaysPerWeek / (k - i));
      int j = i;
      if (bool)
        j = CalendarViewLegacyDelegate.this.mDaysPerWeek - 1 - i; 
      param1Calendar.setTimeInMillis(this.mFirstDay.getTimeInMillis());
      param1Calendar.add(5, j);
      return true;
    }
    
    public boolean getBoundsForDate(Calendar param1Calendar, Rect param1Rect) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(this.mFirstDay.getTime());
      for (int i = 0; i < CalendarViewLegacyDelegate.this.mDaysPerWeek; i++) {
        if (param1Calendar.get(1) == calendar.get(1) && 
          param1Calendar.get(2) == calendar.get(2) && 
          param1Calendar.get(5) == calendar.get(5)) {
          int j = this.mWidth / this.mNumCells;
          if (isLayoutRtl()) {
            if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
              i = this.mNumCells - i - 2;
            } else {
              i = this.mNumCells - i - 1;
            } 
            param1Rect.left = i * j;
          } else {
            if (CalendarViewLegacyDelegate.this.mShowWeekNumber)
              i++; 
            param1Rect.left = i * j;
          } 
          param1Rect.top = 0;
          param1Rect.right = param1Rect.left + j;
          param1Rect.bottom = getHeight();
          return true;
        } 
        calendar.add(5, 1);
      } 
      return false;
    }
    
    protected void onDraw(Canvas param1Canvas) {
      drawBackground(param1Canvas);
      drawWeekNumbersAndDates(param1Canvas);
      drawWeekSeparators(param1Canvas);
      drawSelectedDateVerticalBars(param1Canvas);
    }
    
    private void drawBackground(Canvas param1Canvas) {
      if (!this.mHasSelectedDay)
        return; 
      this.mDrawPaint.setColor(CalendarViewLegacyDelegate.this.mSelectedWeekBackgroundColor);
      this.mTempRect.top = CalendarViewLegacyDelegate.this.mWeekSeparatorLineWidth;
      this.mTempRect.bottom = this.mHeight;
      boolean bool = isLayoutRtl();
      int i = 0;
      if (bool) {
        this.mTempRect.left = 0;
        this.mTempRect.right = this.mSelectedLeft - 2;
      } else {
        Rect rect = this.mTempRect;
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber)
          i = this.mWidth / this.mNumCells; 
        rect.left = i;
        this.mTempRect.right = this.mSelectedLeft - 2;
      } 
      param1Canvas.drawRect(this.mTempRect, this.mDrawPaint);
      if (bool) {
        this.mTempRect.left = this.mSelectedRight + 3;
        Rect rect = this.mTempRect;
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
          i = this.mWidth;
          i -= i / this.mNumCells;
        } else {
          i = this.mWidth;
        } 
        rect.right = i;
      } else {
        this.mTempRect.left = this.mSelectedRight + 3;
        this.mTempRect.right = this.mWidth;
      } 
      param1Canvas.drawRect(this.mTempRect, this.mDrawPaint);
    }
    
    private void drawWeekNumbersAndDates(Canvas param1Canvas) {
      float f = this.mDrawPaint.getTextSize();
      int i = (int)((this.mHeight + f) / 2.0F) - CalendarViewLegacyDelegate.this.mWeekSeparatorLineWidth;
      int j = this.mNumCells;
      int k = j * 2;
      this.mDrawPaint.setTextAlign(Paint.Align.CENTER);
      this.mDrawPaint.setTextSize(CalendarViewLegacyDelegate.this.mDateTextSize);
      int m = 0, n = 0;
      if (isLayoutRtl()) {
        m = n;
        for (; m < j - 1; m++) {
          Paint paint = this.mMonthNumDrawPaint;
          if (this.mFocusDay[m]) {
            n = CalendarViewLegacyDelegate.this.mFocusedMonthDateColor;
          } else {
            n = CalendarViewLegacyDelegate.this.mUnfocusedMonthDateColor;
          } 
          paint.setColor(n);
          n = (m * 2 + 1) * this.mWidth / k;
          param1Canvas.drawText(this.mDayNumbers[j - 1 - m], n, i, this.mMonthNumDrawPaint);
        } 
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
          this.mDrawPaint.setColor(CalendarViewLegacyDelegate.this.mWeekNumberColor);
          m = this.mWidth;
          n = m / k;
          param1Canvas.drawText(this.mDayNumbers[0], (m - n), i, this.mDrawPaint);
        } 
      } else {
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
          this.mDrawPaint.setColor(CalendarViewLegacyDelegate.this.mWeekNumberColor);
          m = this.mWidth / k;
          param1Canvas.drawText(this.mDayNumbers[0], m, i, this.mDrawPaint);
          m = 0 + 1;
        } 
        for (; m < j; m++) {
          Paint paint = this.mMonthNumDrawPaint;
          if (this.mFocusDay[m]) {
            n = CalendarViewLegacyDelegate.this.mFocusedMonthDateColor;
          } else {
            n = CalendarViewLegacyDelegate.this.mUnfocusedMonthDateColor;
          } 
          paint.setColor(n);
          n = (m * 2 + 1) * this.mWidth / k;
          param1Canvas.drawText(this.mDayNumbers[m], n, i, this.mMonthNumDrawPaint);
        } 
      } 
    }
    
    private void drawWeekSeparators(Canvas param1Canvas) {
      float f1, f2;
      int i = CalendarViewLegacyDelegate.this.mListView.getFirstVisiblePosition();
      int j = i;
      if (CalendarViewLegacyDelegate.this.mListView.getChildAt(0).getTop() < 0)
        j = i + 1; 
      if (j == this.mWeek)
        return; 
      this.mDrawPaint.setColor(CalendarViewLegacyDelegate.this.mWeekSeparatorLineColor);
      this.mDrawPaint.setStrokeWidth(CalendarViewLegacyDelegate.this.mWeekSeparatorLineWidth);
      if (isLayoutRtl()) {
        f1 = 0.0F;
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
          j = this.mWidth;
          j -= j / this.mNumCells;
        } else {
          j = this.mWidth;
        } 
        f2 = j;
      } else {
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
          f1 = (this.mWidth / this.mNumCells);
        } else {
          f1 = 0.0F;
        } 
        f2 = this.mWidth;
      } 
      param1Canvas.drawLine(f1, 0.0F, f2, 0.0F, this.mDrawPaint);
    }
    
    private void drawSelectedDateVerticalBars(Canvas param1Canvas) {
      if (!this.mHasSelectedDay)
        return; 
      Drawable drawable = CalendarViewLegacyDelegate.this.mSelectedDateVerticalBar;
      int i = this.mSelectedLeft;
      CalendarViewLegacyDelegate calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
      int j = calendarViewLegacyDelegate.mSelectedDateVerticalBarWidth / 2;
      calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
      int k = calendarViewLegacyDelegate.mWeekSeparatorLineWidth, m = this.mSelectedLeft;
      calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
      int n = calendarViewLegacyDelegate.mSelectedDateVerticalBarWidth / 2, i1 = this.mHeight;
      drawable.setBounds(i - j, k, m + n, i1);
      CalendarViewLegacyDelegate.this.mSelectedDateVerticalBar.draw(param1Canvas);
      drawable = CalendarViewLegacyDelegate.this.mSelectedDateVerticalBar;
      m = this.mSelectedRight;
      calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
      i = calendarViewLegacyDelegate.mSelectedDateVerticalBarWidth / 2;
      calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
      j = calendarViewLegacyDelegate.mWeekSeparatorLineWidth;
      i1 = this.mSelectedRight;
      calendarViewLegacyDelegate = CalendarViewLegacyDelegate.this;
      n = calendarViewLegacyDelegate.mSelectedDateVerticalBarWidth / 2;
      k = this.mHeight;
      drawable.setBounds(m - i, j, i1 + n, k);
      CalendarViewLegacyDelegate.this.mSelectedDateVerticalBar.draw(param1Canvas);
    }
    
    protected void onSizeChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.mWidth = param1Int1;
      updateSelectionPositions();
    }
    
    private void updateSelectionPositions() {
      if (this.mHasSelectedDay) {
        boolean bool = isLayoutRtl();
        int i = this.mSelectedDay - CalendarViewLegacyDelegate.this.mFirstDayOfWeek;
        int j = i;
        if (i < 0)
          j = i + 7; 
        i = j;
        if (CalendarViewLegacyDelegate.this.mShowWeekNumber) {
          i = j;
          if (!bool)
            i = j + 1; 
        } 
        if (bool) {
          this.mSelectedLeft = (CalendarViewLegacyDelegate.this.mDaysPerWeek - 1 - i) * this.mWidth / this.mNumCells;
        } else {
          this.mSelectedLeft = this.mWidth * i / this.mNumCells;
        } 
        this.mSelectedRight = this.mSelectedLeft + this.mWidth / this.mNumCells;
      } 
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      param1Int2 = CalendarViewLegacyDelegate.this.mListView.getHeight();
      int i = CalendarViewLegacyDelegate.this.mListView.getPaddingTop();
      ListView listView = CalendarViewLegacyDelegate.this.mListView;
      this.mHeight = (param1Int2 - i - listView.getPaddingBottom()) / CalendarViewLegacyDelegate.this.mShownWeekCount;
      setMeasuredDimension(View.MeasureSpec.getSize(param1Int1), this.mHeight);
    }
  }
}
