package android.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.android.internal.R;

public class ActivityChooserView extends ViewGroup implements ActivityChooserModel.ActivityChooserModelClient {
  private final DataSetObserver mModelDataSetOberver = (DataSetObserver)new Object(this);
  
  private final ViewTreeObserver.OnGlobalLayoutListener mOnGlobalLayoutListener = (ViewTreeObserver.OnGlobalLayoutListener)new Object(this);
  
  private int mInitialActivityCount = 4;
  
  private static final String LOG_TAG = "ActivityChooserView";
  
  private final LinearLayout mActivityChooserContent;
  
  private final Drawable mActivityChooserContentBackground;
  
  private final ActivityChooserViewAdapter mAdapter;
  
  private final Callbacks mCallbacks;
  
  private int mDefaultActionButtonContentDescription;
  
  private final FrameLayout mDefaultActivityButton;
  
  private final ImageView mDefaultActivityButtonImage;
  
  private final FrameLayout mExpandActivityOverflowButton;
  
  private final ImageView mExpandActivityOverflowButtonImage;
  
  private boolean mIsAttachedToWindow;
  
  private boolean mIsSelectingDefaultActivity;
  
  private final int mListPopupMaxWidth;
  
  private ListPopupWindow mListPopupWindow;
  
  private PopupWindow.OnDismissListener mOnDismissListener;
  
  ActionProvider mProvider;
  
  public ActivityChooserView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ActivityChooserView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ActivityChooserView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ActivityChooserView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActivityChooserView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ActivityChooserView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mInitialActivityCount = typedArray.getInt(1, 4);
    Drawable drawable = typedArray.getDrawable(0);
    typedArray.recycle();
    LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);
    layoutInflater.inflate(17367077, this, true);
    this.mCallbacks = new Callbacks();
    LinearLayout linearLayout = findViewById(16908724);
    this.mActivityChooserContentBackground = linearLayout.getBackground();
    FrameLayout frameLayout = findViewById(16908921);
    frameLayout.setOnClickListener(this.mCallbacks);
    this.mDefaultActivityButton.setOnLongClickListener(this.mCallbacks);
    this.mDefaultActivityButtonImage = this.mDefaultActivityButton.<ImageView>findViewById(16909057);
    frameLayout = findViewById(16908949);
    frameLayout.setOnClickListener(this.mCallbacks);
    frameLayout.setAccessibilityDelegate((View.AccessibilityDelegate)new Object(this));
    frameLayout.setOnTouchListener((View.OnTouchListener)new Object(this, frameLayout));
    this.mExpandActivityOverflowButton = frameLayout;
    ImageView imageView = frameLayout.<ImageView>findViewById(16909057);
    imageView.setImageDrawable(drawable);
    ActivityChooserViewAdapter activityChooserViewAdapter = new ActivityChooserViewAdapter();
    activityChooserViewAdapter.registerDataSetObserver((DataSetObserver)new Object(this));
    Resources resources = paramContext.getResources();
    paramInt1 = (resources.getDisplayMetrics()).widthPixels / 2;
    paramInt2 = resources.getDimensionPixelSize(17105073);
    this.mListPopupMaxWidth = Math.max(paramInt1, paramInt2);
  }
  
  public void setActivityChooserModel(ActivityChooserModel paramActivityChooserModel) {
    this.mAdapter.setDataModel(paramActivityChooserModel);
    if (isShowingPopup()) {
      dismissPopup();
      showPopup();
    } 
  }
  
  public void setExpandActivityOverflowButtonDrawable(Drawable paramDrawable) {
    this.mExpandActivityOverflowButtonImage.setImageDrawable(paramDrawable);
  }
  
  public void setExpandActivityOverflowButtonContentDescription(int paramInt) {
    String str = this.mContext.getString(paramInt);
    this.mExpandActivityOverflowButtonImage.setContentDescription(str);
  }
  
  public void setProvider(ActionProvider paramActionProvider) {
    this.mProvider = paramActionProvider;
  }
  
  public boolean showPopup() {
    if (isShowingPopup() || !this.mIsAttachedToWindow)
      return false; 
    this.mIsSelectingDefaultActivity = false;
    showPopupUnchecked(this.mInitialActivityCount);
    return true;
  }
  
  private void showPopupUnchecked(int paramInt) {
    if (this.mAdapter.getDataModel() != null) {
      boolean bool;
      byte b;
      getViewTreeObserver().addOnGlobalLayoutListener(this.mOnGlobalLayoutListener);
      FrameLayout frameLayout = this.mDefaultActivityButton;
      if (frameLayout.getVisibility() == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      int i = this.mAdapter.getActivityCount();
      if (bool) {
        b = 1;
      } else {
        b = 0;
      } 
      if (paramInt != Integer.MAX_VALUE && i > paramInt + b) {
        this.mAdapter.setShowFooterView(true);
        this.mAdapter.setMaxActivityCount(paramInt - 1);
      } else {
        this.mAdapter.setShowFooterView(false);
        this.mAdapter.setMaxActivityCount(paramInt);
      } 
      ListPopupWindow listPopupWindow = getListPopupWindow();
      if (!listPopupWindow.isShowing()) {
        if (this.mIsSelectingDefaultActivity || !bool) {
          this.mAdapter.setShowDefaultActivity(true, bool);
        } else {
          this.mAdapter.setShowDefaultActivity(false, false);
        } 
        paramInt = Math.min(this.mAdapter.measureContentWidth(), this.mListPopupMaxWidth);
        listPopupWindow.setContentWidth(paramInt);
        listPopupWindow.show();
        ActionProvider actionProvider = this.mProvider;
        if (actionProvider != null)
          actionProvider.subUiVisibilityChanged(true); 
        listPopupWindow.getListView().setContentDescription(this.mContext.getString(17039602));
        listPopupWindow.getListView().setSelector((Drawable)new ColorDrawable(0));
      } 
      return;
    } 
    throw new IllegalStateException("No data model. Did you call #setDataModel?");
  }
  
  public boolean dismissPopup() {
    if (isShowingPopup()) {
      getListPopupWindow().dismiss();
      ViewTreeObserver viewTreeObserver = getViewTreeObserver();
      if (viewTreeObserver.isAlive())
        viewTreeObserver.removeOnGlobalLayoutListener(this.mOnGlobalLayoutListener); 
    } 
    return true;
  }
  
  public boolean isShowingPopup() {
    return getListPopupWindow().isShowing();
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    ActivityChooserModel activityChooserModel = this.mAdapter.getDataModel();
    if (activityChooserModel != null)
      activityChooserModel.registerObserver(this.mModelDataSetOberver); 
    this.mIsAttachedToWindow = true;
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    ActivityChooserModel activityChooserModel = this.mAdapter.getDataModel();
    if (activityChooserModel != null)
      activityChooserModel.unregisterObserver(this.mModelDataSetOberver); 
    ViewTreeObserver viewTreeObserver = getViewTreeObserver();
    if (viewTreeObserver.isAlive())
      viewTreeObserver.removeOnGlobalLayoutListener(this.mOnGlobalLayoutListener); 
    if (isShowingPopup())
      dismissPopup(); 
    this.mIsAttachedToWindow = false;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    LinearLayout linearLayout = this.mActivityChooserContent;
    int i = paramInt2;
    if (this.mDefaultActivityButton.getVisibility() != 0)
      i = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt2), 1073741824); 
    measureChild(linearLayout, paramInt1, i);
    setMeasuredDimension(linearLayout.getMeasuredWidth(), linearLayout.getMeasuredHeight());
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mActivityChooserContent.layout(0, 0, paramInt3 - paramInt1, paramInt4 - paramInt2);
    if (!isShowingPopup())
      dismissPopup(); 
  }
  
  public ActivityChooserModel getDataModel() {
    return this.mAdapter.getDataModel();
  }
  
  public void setOnDismissListener(PopupWindow.OnDismissListener paramOnDismissListener) {
    this.mOnDismissListener = paramOnDismissListener;
  }
  
  public void setInitialActivityCount(int paramInt) {
    this.mInitialActivityCount = paramInt;
  }
  
  public void setDefaultActionButtonContentDescription(int paramInt) {
    this.mDefaultActionButtonContentDescription = paramInt;
  }
  
  private ListPopupWindow getListPopupWindow() {
    if (this.mListPopupWindow == null) {
      ListPopupWindow listPopupWindow = new ListPopupWindow(getContext());
      listPopupWindow.setAdapter(this.mAdapter);
      this.mListPopupWindow.setAnchorView(this);
      this.mListPopupWindow.setModal(true);
      this.mListPopupWindow.setOnItemClickListener(this.mCallbacks);
      this.mListPopupWindow.setOnDismissListener(this.mCallbacks);
    } 
    return this.mListPopupWindow;
  }
  
  private void updateAppearance() {
    if (this.mAdapter.getCount() > 0) {
      this.mExpandActivityOverflowButton.setEnabled(true);
    } else {
      this.mExpandActivityOverflowButton.setEnabled(false);
    } 
    int i = this.mAdapter.getActivityCount();
    int j = this.mAdapter.getHistorySize();
    if (i == 1 || (i > 1 && j > 0)) {
      this.mDefaultActivityButton.setVisibility(0);
      ResolveInfo resolveInfo = this.mAdapter.getDefaultActivity();
      PackageManager packageManager = this.mContext.getPackageManager();
      this.mDefaultActivityButtonImage.setImageDrawable(resolveInfo.loadIcon(packageManager));
      if (this.mDefaultActionButtonContentDescription != 0) {
        CharSequence charSequence = resolveInfo.loadLabel(packageManager);
        charSequence = this.mContext.getString(this.mDefaultActionButtonContentDescription, new Object[] { charSequence });
        this.mDefaultActivityButton.setContentDescription(charSequence);
      } 
    } else {
      this.mDefaultActivityButton.setVisibility(8);
    } 
    if (this.mDefaultActivityButton.getVisibility() == 0) {
      this.mActivityChooserContent.setBackground(this.mActivityChooserContentBackground);
    } else {
      this.mActivityChooserContent.setBackground(null);
    } 
  }
  
  class Callbacks implements AdapterView.OnItemClickListener, View.OnClickListener, View.OnLongClickListener, PopupWindow.OnDismissListener {
    final ActivityChooserView this$0;
    
    private Callbacks() {}
    
    public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      ActivityChooserView.ActivityChooserViewAdapter activityChooserViewAdapter = (ActivityChooserView.ActivityChooserViewAdapter)param1AdapterView.getAdapter();
      int i = activityChooserViewAdapter.getItemViewType(param1Int);
      if (i != 0) {
        if (i == 1) {
          ActivityChooserView.this.showPopupUnchecked(2147483647);
        } else {
          throw new IllegalArgumentException();
        } 
      } else {
        ActivityChooserView.this.dismissPopup();
        if (ActivityChooserView.this.mIsSelectingDefaultActivity) {
          if (param1Int > 0)
            ActivityChooserView.this.mAdapter.getDataModel().setDefaultActivity(param1Int); 
        } else {
          if (!ActivityChooserView.this.mAdapter.getShowDefaultActivity())
            param1Int++; 
          Intent intent = ActivityChooserView.this.mAdapter.getDataModel().chooseActivity(param1Int);
          if (intent != null) {
            intent.addFlags(524288);
            ResolveInfo resolveInfo = ActivityChooserView.this.mAdapter.getDataModel().getActivity(param1Int);
            startActivity(intent, resolveInfo);
          } 
        } 
      } 
    }
    
    public void onClick(View param1View) {
      ResolveInfo resolveInfo;
      if (param1View == ActivityChooserView.this.mDefaultActivityButton) {
        ActivityChooserView.this.dismissPopup();
        resolveInfo = ActivityChooserView.this.mAdapter.getDefaultActivity();
        int i = ActivityChooserView.this.mAdapter.getDataModel().getActivityIndex(resolveInfo);
        Intent intent = ActivityChooserView.this.mAdapter.getDataModel().chooseActivity(i);
        if (intent != null) {
          intent.addFlags(524288);
          startActivity(intent, resolveInfo);
        } 
      } else {
        if (resolveInfo == ActivityChooserView.this.mExpandActivityOverflowButton) {
          ActivityChooserView.access$602(ActivityChooserView.this, false);
          ActivityChooserView activityChooserView = ActivityChooserView.this;
          activityChooserView.showPopupUnchecked(activityChooserView.mInitialActivityCount);
          return;
        } 
        throw new IllegalArgumentException();
      } 
    }
    
    public boolean onLongClick(View param1View) {
      if (param1View == ActivityChooserView.this.mDefaultActivityButton) {
        if (ActivityChooserView.this.mAdapter.getCount() > 0) {
          ActivityChooserView.access$602(ActivityChooserView.this, true);
          param1View = ActivityChooserView.this;
          param1View.showPopupUnchecked(((ActivityChooserView)param1View).mInitialActivityCount);
        } 
        return true;
      } 
      throw new IllegalArgumentException();
    }
    
    public void onDismiss() {
      notifyOnDismissListener();
      if (ActivityChooserView.this.mProvider != null)
        ActivityChooserView.this.mProvider.subUiVisibilityChanged(false); 
    }
    
    private void notifyOnDismissListener() {
      if (ActivityChooserView.this.mOnDismissListener != null)
        ActivityChooserView.this.mOnDismissListener.onDismiss(); 
    }
    
    private void startActivity(Intent param1Intent, ResolveInfo param1ResolveInfo) {
      try {
        ActivityChooserView.this.mContext.startActivity(param1Intent);
      } catch (RuntimeException runtimeException) {
        CharSequence charSequence = param1ResolveInfo.loadLabel(ActivityChooserView.this.mContext.getPackageManager());
        charSequence = ActivityChooserView.this.mContext.getString(17039603, new Object[] { charSequence });
        Log.e("ActivityChooserView", (String)charSequence);
        Toast.makeText(ActivityChooserView.this.mContext, charSequence, 0).show();
      } 
    }
  }
  
  class ActivityChooserViewAdapter extends BaseAdapter {
    private static final int ITEM_VIEW_TYPE_ACTIVITY = 0;
    
    private static final int ITEM_VIEW_TYPE_COUNT = 3;
    
    private static final int ITEM_VIEW_TYPE_FOOTER = 1;
    
    public static final int MAX_ACTIVITY_COUNT_DEFAULT = 4;
    
    public static final int MAX_ACTIVITY_COUNT_UNLIMITED = 2147483647;
    
    private ActivityChooserModel mDataModel;
    
    private boolean mHighlightDefaultActivity;
    
    private int mMaxActivityCount = 4;
    
    private boolean mShowDefaultActivity;
    
    private boolean mShowFooterView;
    
    final ActivityChooserView this$0;
    
    public void setDataModel(ActivityChooserModel param1ActivityChooserModel) {
      ActivityChooserModel activityChooserModel = ActivityChooserView.this.mAdapter.getDataModel();
      if (activityChooserModel != null && ActivityChooserView.this.isShown())
        activityChooserModel.unregisterObserver(ActivityChooserView.this.mModelDataSetOberver); 
      this.mDataModel = param1ActivityChooserModel;
      if (param1ActivityChooserModel != null && ActivityChooserView.this.isShown())
        param1ActivityChooserModel.registerObserver(ActivityChooserView.this.mModelDataSetOberver); 
      notifyDataSetChanged();
    }
    
    public int getItemViewType(int param1Int) {
      if (this.mShowFooterView && param1Int == getCount() - 1)
        return 1; 
      return 0;
    }
    
    public int getViewTypeCount() {
      return 3;
    }
    
    public int getCount() {
      int i = this.mDataModel.getActivityCount();
      int j = i;
      if (!this.mShowDefaultActivity) {
        j = i;
        if (this.mDataModel.getDefaultActivity() != null)
          j = i - 1; 
      } 
      i = Math.min(j, this.mMaxActivityCount);
      j = i;
      if (this.mShowFooterView)
        j = i + 1; 
      return j;
    }
    
    public Object getItem(int param1Int) {
      int i = getItemViewType(param1Int);
      if (i != 0) {
        if (i == 1)
          return null; 
        throw new IllegalArgumentException();
      } 
      i = param1Int;
      if (!this.mShowDefaultActivity) {
        i = param1Int;
        if (this.mDataModel.getDefaultActivity() != null)
          i = param1Int + 1; 
      } 
      return this.mDataModel.getActivity(i);
    }
    
    public long getItemId(int param1Int) {
      return param1Int;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      // Byte code:
      //   0: aload_0
      //   1: iload_1
      //   2: invokevirtual getItemViewType : (I)I
      //   5: istore #4
      //   7: iload #4
      //   9: ifeq -> 96
      //   12: iload #4
      //   14: iconst_1
      //   15: if_icmpne -> 88
      //   18: aload_2
      //   19: ifnull -> 33
      //   22: aload_2
      //   23: astore #5
      //   25: aload_2
      //   26: invokevirtual getId : ()I
      //   29: iconst_1
      //   30: if_icmpeq -> 85
      //   33: aload_0
      //   34: getfield this$0 : Landroid/widget/ActivityChooserView;
      //   37: invokevirtual getContext : ()Landroid/content/Context;
      //   40: invokestatic from : (Landroid/content/Context;)Landroid/view/LayoutInflater;
      //   43: ldc 17367078
      //   45: aload_3
      //   46: iconst_0
      //   47: invokevirtual inflate : (ILandroid/view/ViewGroup;Z)Landroid/view/View;
      //   50: astore #5
      //   52: aload #5
      //   54: iconst_1
      //   55: invokevirtual setId : (I)V
      //   58: aload #5
      //   60: ldc 16908310
      //   62: invokevirtual findViewById : (I)Landroid/view/View;
      //   65: checkcast android/widget/TextView
      //   68: astore_2
      //   69: aload_2
      //   70: aload_0
      //   71: getfield this$0 : Landroid/widget/ActivityChooserView;
      //   74: invokestatic access$1600 : (Landroid/widget/ActivityChooserView;)Landroid/content/Context;
      //   77: ldc 17039596
      //   79: invokevirtual getString : (I)Ljava/lang/String;
      //   82: invokevirtual setText : (Ljava/lang/CharSequence;)V
      //   85: aload #5
      //   87: areturn
      //   88: new java/lang/IllegalArgumentException
      //   91: dup
      //   92: invokespecial <init> : ()V
      //   95: athrow
      //   96: aload_2
      //   97: ifnull -> 112
      //   100: aload_2
      //   101: astore #5
      //   103: aload_2
      //   104: invokevirtual getId : ()I
      //   107: ldc 16909131
      //   109: if_icmpeq -> 131
      //   112: aload_0
      //   113: getfield this$0 : Landroid/widget/ActivityChooserView;
      //   116: invokevirtual getContext : ()Landroid/content/Context;
      //   119: invokestatic from : (Landroid/content/Context;)Landroid/view/LayoutInflater;
      //   122: ldc 17367078
      //   124: aload_3
      //   125: iconst_0
      //   126: invokevirtual inflate : (ILandroid/view/ViewGroup;Z)Landroid/view/View;
      //   129: astore #5
      //   131: aload_0
      //   132: getfield this$0 : Landroid/widget/ActivityChooserView;
      //   135: invokestatic access$1700 : (Landroid/widget/ActivityChooserView;)Landroid/content/Context;
      //   138: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
      //   141: astore_3
      //   142: aload #5
      //   144: ldc 16908294
      //   146: invokevirtual findViewById : (I)Landroid/view/View;
      //   149: checkcast android/widget/ImageView
      //   152: astore #6
      //   154: aload_0
      //   155: iload_1
      //   156: invokevirtual getItem : (I)Ljava/lang/Object;
      //   159: checkcast android/content/pm/ResolveInfo
      //   162: astore_2
      //   163: aload #6
      //   165: aload_2
      //   166: aload_3
      //   167: invokevirtual loadIcon : (Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
      //   170: invokevirtual setImageDrawable : (Landroid/graphics/drawable/Drawable;)V
      //   173: aload #5
      //   175: ldc 16908310
      //   177: invokevirtual findViewById : (I)Landroid/view/View;
      //   180: checkcast android/widget/TextView
      //   183: astore #6
      //   185: aload #6
      //   187: aload_2
      //   188: aload_3
      //   189: invokevirtual loadLabel : (Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
      //   192: invokevirtual setText : (Ljava/lang/CharSequence;)V
      //   195: aload_0
      //   196: getfield mShowDefaultActivity : Z
      //   199: ifeq -> 222
      //   202: iload_1
      //   203: ifne -> 222
      //   206: aload_0
      //   207: getfield mHighlightDefaultActivity : Z
      //   210: ifeq -> 222
      //   213: aload #5
      //   215: iconst_1
      //   216: invokevirtual setActivated : (Z)V
      //   219: goto -> 228
      //   222: aload #5
      //   224: iconst_0
      //   225: invokevirtual setActivated : (Z)V
      //   228: aload #5
      //   230: areturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #763	-> 0
      //   #764	-> 7
      //   #766	-> 18
      //   #767	-> 33
      //   #769	-> 52
      //   #770	-> 58
      //   #771	-> 69
      //   #774	-> 85
      //   #796	-> 88
      //   #776	-> 96
      //   #777	-> 112
      //   #780	-> 131
      //   #782	-> 142
      //   #783	-> 154
      //   #784	-> 163
      //   #786	-> 173
      //   #787	-> 185
      //   #789	-> 195
      //   #790	-> 213
      //   #792	-> 222
      //   #794	-> 228
    }
    
    public int measureContentWidth() {
      int i = this.mMaxActivityCount;
      this.mMaxActivityCount = Integer.MAX_VALUE;
      int j = 0;
      View view = null;
      int k = View.MeasureSpec.makeMeasureSpec(0, 0);
      int m = View.MeasureSpec.makeMeasureSpec(0, 0);
      int n = getCount();
      for (byte b = 0; b < n; b++) {
        view = getView(b, view, (ViewGroup)null);
        view.measure(k, m);
        j = Math.max(j, view.getMeasuredWidth());
      } 
      this.mMaxActivityCount = i;
      return j;
    }
    
    public void setMaxActivityCount(int param1Int) {
      if (this.mMaxActivityCount != param1Int) {
        this.mMaxActivityCount = param1Int;
        notifyDataSetChanged();
      } 
    }
    
    public ResolveInfo getDefaultActivity() {
      return this.mDataModel.getDefaultActivity();
    }
    
    public void setShowFooterView(boolean param1Boolean) {
      if (this.mShowFooterView != param1Boolean) {
        this.mShowFooterView = param1Boolean;
        notifyDataSetChanged();
      } 
    }
    
    public int getActivityCount() {
      return this.mDataModel.getActivityCount();
    }
    
    public int getHistorySize() {
      return this.mDataModel.getHistorySize();
    }
    
    public ActivityChooserModel getDataModel() {
      return this.mDataModel;
    }
    
    public void setShowDefaultActivity(boolean param1Boolean1, boolean param1Boolean2) {
      if (this.mShowDefaultActivity != param1Boolean1 || this.mHighlightDefaultActivity != param1Boolean2) {
        this.mShowDefaultActivity = param1Boolean1;
        this.mHighlightDefaultActivity = param1Boolean2;
        notifyDataSetChanged();
      } 
    }
    
    public boolean getShowDefaultActivity() {
      return this.mShowDefaultActivity;
    }
    
    private ActivityChooserViewAdapter() {}
  }
}
