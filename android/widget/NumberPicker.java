package android.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import libcore.icu.LocaleData;

public class NumberPicker extends LinearLayout {
  private static final int DEFAULT_LAYOUT_RESOURCE_ID = 17367227;
  
  private static final long DEFAULT_LONG_PRESS_UPDATE_INTERVAL = 300L;
  
  private static final char[] DIGIT_CHARACTERS;
  
  private static final int SELECTOR_ADJUSTMENT_DURATION_MILLIS = 800;
  
  private static final int SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT = 8;
  
  private static final int SELECTOR_MIDDLE_ITEM_INDEX = 1;
  
  private static final int SELECTOR_WHEEL_ITEM_COUNT = 3;
  
  private static final int SIZE_UNSPECIFIED = -1;
  
  private static final int SNAP_SCROLL_DURATION = 300;
  
  private static final float TOP_AND_BOTTOM_FADING_EDGE_STRENGTH = 0.9F;
  
  private static final int UNSCALED_DEFAULT_SELECTION_DIVIDERS_DISTANCE = 48;
  
  private static final int UNSCALED_DEFAULT_SELECTION_DIVIDER_HEIGHT = 2;
  
  class TwoDigitFormatter implements Formatter {
    char mZeroDigit;
    
    java.util.Formatter mFmt;
    
    final StringBuilder mBuilder = new StringBuilder();
    
    final Object[] mArgs = new Object[1];
    
    TwoDigitFormatter() {
      Locale locale = Locale.getDefault();
      init(locale);
    }
    
    private void init(Locale param1Locale) {
      this.mFmt = createFormatter(param1Locale);
      this.mZeroDigit = getZeroDigit(param1Locale);
    }
    
    public String format(int param1Int) {
      Locale locale = Locale.getDefault();
      if (this.mZeroDigit != getZeroDigit(locale))
        init(locale); 
      this.mArgs[0] = Integer.valueOf(param1Int);
      StringBuilder stringBuilder = this.mBuilder;
      stringBuilder.delete(0, stringBuilder.length());
      this.mFmt.format("%02d", this.mArgs);
      return this.mFmt.toString();
    }
    
    private static char getZeroDigit(Locale param1Locale) {
      return (LocaleData.get(param1Locale)).zeroDigit;
    }
    
    private java.util.Formatter createFormatter(Locale param1Locale) {
      return new java.util.Formatter(this.mBuilder, param1Locale);
    }
  }
  
  private static final TwoDigitFormatter sTwoDigitFormatter = new TwoDigitFormatter();
  
  private AccessibilityNodeProviderImpl mAccessibilityNodeProvider;
  
  private final Scroller mAdjustScroller;
  
  private BeginSoftInputOnLongPressCommand mBeginSoftInputOnLongPressCommand;
  
  private int mBottomSelectionDividerBottom;
  
  private ChangeCurrentByOneFromLongPressCommand mChangeCurrentByOneFromLongPressCommand;
  
  private final boolean mComputeMaxWidth;
  
  private int mCurrentScrollOffset;
  
  private final ImageButton mDecrementButton;
  
  private boolean mDecrementVirtualButtonPressed;
  
  private String[] mDisplayedValues;
  
  private final Scroller mFlingScroller;
  
  private Formatter mFormatter;
  
  private final boolean mHasSelectorWheel;
  
  private boolean mHideWheelUntilFocused;
  
  private boolean mIgnoreMoveEvents;
  
  private final ImageButton mIncrementButton;
  
  private boolean mIncrementVirtualButtonPressed;
  
  private int mInitialScrollOffset;
  
  private final EditText mInputText;
  
  private long mLastDownEventTime;
  
  private float mLastDownEventY;
  
  private float mLastDownOrMoveEventY;
  
  private int mLastHandledDownDpadKeyCode;
  
  private int mLastHoveredChildVirtualViewId;
  
  private long mLongPressUpdateInterval;
  
  private final int mMaxHeight;
  
  private int mMaxValue;
  
  private int mMaxWidth;
  
  private int mMaximumFlingVelocity;
  
  private final int mMinHeight;
  
  private int mMinValue;
  
  private final int mMinWidth;
  
  private int mMinimumFlingVelocity;
  
  private OnScrollListener mOnScrollListener;
  
  private OnValueChangeListener mOnValueChangeListener;
  
  private boolean mPerformClickOnTap;
  
  private final PressedStateHelper mPressedStateHelper;
  
  private int mPreviousScrollerY;
  
  private int mScrollState;
  
  private final Drawable mSelectionDivider;
  
  private int mSelectionDividerHeight;
  
  private final int mSelectionDividersDistance;
  
  private int mSelectorElementHeight;
  
  private final SparseArray<String> mSelectorIndexToStringCache;
  
  private final int[] mSelectorIndices;
  
  private int mSelectorTextGapHeight;
  
  private final Paint mSelectorWheelPaint;
  
  private SetSelectionCommand mSetSelectionCommand;
  
  private final int mSolidColor;
  
  private final int mTextSize;
  
  private int mTopSelectionDividerTop;
  
  private int mTouchSlop;
  
  private int mValue;
  
  private VelocityTracker mVelocityTracker;
  
  private final Drawable mVirtualButtonPressedDrawable;
  
  private boolean mWrapSelectorWheel;
  
  private boolean mWrapSelectorWheelPreferred;
  
  public static final Formatter getTwoDigitFormatter() {
    return sTwoDigitFormatter;
  }
  
  public NumberPicker(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public NumberPicker(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16844068);
  }
  
  public NumberPicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public NumberPicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    boolean bool;
    this.mWrapSelectorWheelPreferred = true;
    this.mLongPressUpdateInterval = 300L;
    this.mSelectorIndexToStringCache = new SparseArray<>();
    this.mSelectorIndices = new int[3];
    this.mInitialScrollOffset = Integer.MIN_VALUE;
    this.mScrollState = 0;
    this.mLastHandledDownDpadKeyCode = -1;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.NumberPicker, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.NumberPicker, paramAttributeSet, typedArray, paramInt1, paramInt2);
    paramInt1 = typedArray.getResourceId(3, 17367227);
    if (paramInt1 != 17367227) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mHasSelectorWheel = bool;
    this.mHideWheelUntilFocused = typedArray.getBoolean(2, false);
    this.mSolidColor = typedArray.getColor(0, 0);
    Drawable drawable = typedArray.getDrawable(8);
    if (drawable != null) {
      drawable.setCallback(this);
      drawable.setLayoutDirection(getLayoutDirection());
      if (drawable.isStateful())
        drawable.setState(getDrawableState()); 
    } 
    this.mSelectionDivider = drawable;
    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
    paramInt2 = (int)TypedValue.applyDimension(1, 2.0F, displayMetrics);
    this.mSelectionDividerHeight = typedArray.getDimensionPixelSize(1, paramInt2);
    displayMetrics = getResources().getDisplayMetrics();
    paramInt2 = (int)TypedValue.applyDimension(1, 48.0F, displayMetrics);
    this.mSelectionDividersDistance = typedArray.getDimensionPixelSize(9, paramInt2);
    this.mMinHeight = typedArray.getDimensionPixelSize(6, -1);
    this.mMaxHeight = paramInt2 = typedArray.getDimensionPixelSize(4, -1);
    int i = this.mMinHeight;
    if (i == -1 || paramInt2 == -1 || i <= paramInt2) {
      this.mMinWidth = typedArray.getDimensionPixelSize(7, -1);
      this.mMaxWidth = paramInt2 = typedArray.getDimensionPixelSize(5, -1);
      i = this.mMinWidth;
      if (i == -1 || paramInt2 == -1 || i <= paramInt2) {
        if (this.mMaxWidth == -1) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mComputeMaxWidth = bool;
        this.mVirtualButtonPressedDrawable = typedArray.getDrawable(10);
        typedArray.recycle();
        this.mPressedStateHelper = new PressedStateHelper();
        setWillNotDraw(this.mHasSelectorWheel ^ true);
        LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService("layout_inflater");
        layoutInflater.inflate(paramInt1, this, true);
        Object object1 = new Object(this);
        Object object2 = new Object(this);
        if (!this.mHasSelectorWheel) {
          ImageButton imageButton = findViewById(16909073);
          imageButton.setOnClickListener((View.OnClickListener)object1);
          this.mIncrementButton.setOnLongClickListener((View.OnLongClickListener)object2);
        } else {
          this.mIncrementButton = null;
        } 
        if (!this.mHasSelectorWheel) {
          ImageButton imageButton = findViewById(16908918);
          imageButton.setOnClickListener((View.OnClickListener)object1);
          this.mDecrementButton.setOnLongClickListener((View.OnLongClickListener)object2);
        } else {
          this.mDecrementButton = null;
        } 
        this.mInputText = (EditText)(object1 = findViewById(16909248));
        object1.setOnFocusChangeListener((View.OnFocusChangeListener)new Object(this));
        this.mInputText.setFilters(new InputFilter[] { new InputTextFilter() });
        this.mInputText.setRawInputType(2);
        this.mInputText.setImeOptions(6);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(paramContext);
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
        this.mMinimumFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaximumFlingVelocity = viewConfiguration.getScaledMaximumFlingVelocity() / 8;
        this.mTextSize = (int)this.mInputText.getTextSize();
        object1 = new Paint();
        object1.setAntiAlias(true);
        object1.setTextAlign(Paint.Align.CENTER);
        object1.setTextSize(this.mTextSize);
        object1.setTypeface(this.mInputText.getTypeface());
        ColorStateList colorStateList = this.mInputText.getTextColors();
        paramInt1 = colorStateList.getColorForState(ENABLED_STATE_SET, -1);
        object1.setColor(paramInt1);
        this.mSelectorWheelPaint = (Paint)object1;
        this.mFlingScroller = new Scroller(getContext(), null, true);
        this.mAdjustScroller = new Scroller(getContext(), new DecelerateInterpolator(2.5F));
        updateInputTextView();
        if (getImportantForAccessibility() == 0)
          setImportantForAccessibility(1); 
        if (getFocusable() == 16) {
          setFocusable(1);
          setFocusableInTouchMode(true);
        } 
        return;
      } 
      throw new IllegalArgumentException("minWidth > maxWidth");
    } 
    throw new IllegalArgumentException("minHeight > maxHeight");
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!this.mHasSelectorWheel) {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    } 
    paramInt4 = getMeasuredWidth();
    paramInt3 = getMeasuredHeight();
    paramInt1 = this.mInputText.getMeasuredWidth();
    paramInt2 = this.mInputText.getMeasuredHeight();
    paramInt4 = (paramInt4 - paramInt1) / 2;
    paramInt3 = (paramInt3 - paramInt2) / 2;
    this.mInputText.layout(paramInt4, paramInt3, paramInt4 + paramInt1, paramInt3 + paramInt2);
    if (paramBoolean) {
      initializeSelectorWheel();
      initializeFadingEdges();
      paramInt2 = getHeight();
      paramInt1 = this.mSelectionDividersDistance;
      paramInt3 = (paramInt2 - paramInt1) / 2;
      paramInt2 = this.mSelectionDividerHeight;
      this.mTopSelectionDividerTop = paramInt3 -= paramInt2;
      this.mBottomSelectionDividerBottom = paramInt3 + paramInt2 * 2 + paramInt1;
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    if (!this.mHasSelectorWheel) {
      super.onMeasure(paramInt1, paramInt2);
      return;
    } 
    int i = makeMeasureSpec(paramInt1, this.mMaxWidth);
    int j = makeMeasureSpec(paramInt2, this.mMaxHeight);
    super.onMeasure(i, j);
    paramInt1 = resolveSizeAndStateRespectingMinSize(this.mMinWidth, getMeasuredWidth(), paramInt1);
    paramInt2 = resolveSizeAndStateRespectingMinSize(this.mMinHeight, getMeasuredHeight(), paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  private boolean moveToFinalScrollerPosition(Scroller paramScroller) {
    paramScroller.forceFinished(true);
    int i = paramScroller.getFinalY() - paramScroller.getCurrY();
    int j = this.mCurrentScrollOffset, k = this.mSelectorElementHeight;
    k = this.mInitialScrollOffset - (j + i) % k;
    if (k != 0) {
      int m = Math.abs(k), n = this.mSelectorElementHeight;
      j = k;
      if (m > n / 2)
        if (k > 0) {
          j = k - n;
        } else {
          j = k + n;
        }  
      scrollBy(0, i + j);
      return true;
    } 
    return false;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    if (!this.mHasSelectorWheel || !isEnabled())
      return false; 
    int i = paramMotionEvent.getActionMasked();
    if (i != 0)
      return false; 
    removeAllCallbacks();
    hideSoftInput();
    float f = paramMotionEvent.getY();
    this.mLastDownOrMoveEventY = f;
    this.mLastDownEventTime = paramMotionEvent.getEventTime();
    this.mIgnoreMoveEvents = false;
    this.mPerformClickOnTap = false;
    f = this.mLastDownEventY;
    if (f < this.mTopSelectionDividerTop) {
      if (this.mScrollState == 0)
        this.mPressedStateHelper.buttonPressDelayed(2); 
    } else if (f > this.mBottomSelectionDividerBottom && this.mScrollState == 0) {
      this.mPressedStateHelper.buttonPressDelayed(1);
    } 
    getParent().requestDisallowInterceptTouchEvent(true);
    if (!this.mFlingScroller.isFinished()) {
      this.mFlingScroller.forceFinished(true);
      this.mAdjustScroller.forceFinished(true);
      onScrollerFinished(this.mFlingScroller);
      onScrollStateChange(0);
    } else if (!this.mAdjustScroller.isFinished()) {
      this.mFlingScroller.forceFinished(true);
      this.mAdjustScroller.forceFinished(true);
      onScrollerFinished(this.mAdjustScroller);
    } else {
      f = this.mLastDownEventY;
      if (f < this.mTopSelectionDividerTop) {
        long l = ViewConfiguration.getLongPressTimeout();
        postChangeCurrentByOneFromLongPress(false, l);
      } else if (f > this.mBottomSelectionDividerBottom) {
        long l = ViewConfiguration.getLongPressTimeout();
        postChangeCurrentByOneFromLongPress(true, l);
      } else {
        this.mPerformClickOnTap = true;
        postBeginSoftInputOnLongPressCommand();
      } 
    } 
    return true;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (!isEnabled() || !this.mHasSelectorWheel)
      return false; 
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain(); 
    this.mVelocityTracker.addMovement(paramMotionEvent);
    int i = paramMotionEvent.getActionMasked();
    if (i != 1) {
      if (i == 2)
        if (!this.mIgnoreMoveEvents) {
          float f = paramMotionEvent.getY();
          if (this.mScrollState != 1) {
            i = (int)Math.abs(f - this.mLastDownEventY);
            if (i > this.mTouchSlop) {
              removeAllCallbacks();
              onScrollStateChange(1);
            } 
          } else {
            i = (int)(f - this.mLastDownOrMoveEventY);
            scrollBy(0, i);
            invalidate();
          } 
          this.mLastDownOrMoveEventY = f;
        }  
    } else {
      removeBeginSoftInputCommand();
      removeChangeCurrentByOneFromLongPress();
      this.mPressedStateHelper.cancel();
      VelocityTracker velocityTracker = this.mVelocityTracker;
      velocityTracker.computeCurrentVelocity(1000, this.mMaximumFlingVelocity);
      i = (int)velocityTracker.getYVelocity();
      if (Math.abs(i) > this.mMinimumFlingVelocity) {
        fling(i);
        onScrollStateChange(2);
      } else {
        i = (int)paramMotionEvent.getY();
        int j = (int)Math.abs(i - this.mLastDownEventY);
        long l1 = paramMotionEvent.getEventTime(), l2 = this.mLastDownEventTime;
        if (j <= this.mTouchSlop && l1 - l2 < ViewConfiguration.getTapTimeout()) {
          if (this.mPerformClickOnTap) {
            this.mPerformClickOnTap = false;
            performClick();
          } else {
            i = i / this.mSelectorElementHeight - 1;
            if (i > 0) {
              changeValueByOne(true);
              this.mPressedStateHelper.buttonTapped(1);
            } else if (i < 0) {
              changeValueByOne(false);
              this.mPressedStateHelper.buttonTapped(2);
            } 
          } 
        } else {
          ensureScrollWheelAdjusted();
        } 
        onScrollStateChange(0);
      } 
      this.mVelocityTracker.recycle();
      this.mVelocityTracker = null;
    } 
    return true;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    if (i == 1 || i == 3)
      removeAllCallbacks(); 
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    int i = paramKeyEvent.getKeyCode();
    if (i != 19 && i != 20) {
      if (i == 23 || i == 66 || i == 160)
        removeAllCallbacks(); 
    } else if (this.mHasSelectorWheel) {
      int j = paramKeyEvent.getAction();
      if (j != 0) {
        if (j == 1)
          if (this.mLastHandledDownDpadKeyCode == i) {
            this.mLastHandledDownDpadKeyCode = -1;
            return true;
          }  
      } else if (this.mWrapSelectorWheel || ((i == 20) ? (getValue() < getMaxValue()) : (getValue() > getMinValue()))) {
        requestFocus();
        this.mLastHandledDownDpadKeyCode = i;
        removeAllCallbacks();
        if (this.mFlingScroller.isFinished()) {
          boolean bool;
          if (i == 20) {
            bool = true;
          } else {
            bool = false;
          } 
          changeValueByOne(bool);
        } 
        return true;
      } 
    } 
    return super.dispatchKeyEvent(paramKeyEvent);
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    if (i == 1 || i == 3)
      removeAllCallbacks(); 
    return super.dispatchTrackballEvent(paramMotionEvent);
  }
  
  protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    if (!this.mHasSelectorWheel)
      return super.dispatchHoverEvent(paramMotionEvent); 
    if (AccessibilityManager.getInstance(this.mContext).isEnabled()) {
      int i = (int)paramMotionEvent.getY();
      if (i < this.mTopSelectionDividerTop) {
        i = 3;
      } else if (i > this.mBottomSelectionDividerBottom) {
        i = 1;
      } else {
        i = 2;
      } 
      int j = paramMotionEvent.getActionMasked();
      AccessibilityNodeProviderImpl accessibilityNodeProviderImpl = (AccessibilityNodeProviderImpl)getAccessibilityNodeProvider();
      if (j != 7) {
        if (j != 9) {
          if (j == 10) {
            accessibilityNodeProviderImpl.sendAccessibilityEventForVirtualView(i, 256);
            this.mLastHoveredChildVirtualViewId = -1;
          } 
        } else {
          accessibilityNodeProviderImpl.sendAccessibilityEventForVirtualView(i, 128);
          this.mLastHoveredChildVirtualViewId = i;
          accessibilityNodeProviderImpl.performAction(i, 64, null);
        } 
      } else {
        j = this.mLastHoveredChildVirtualViewId;
        if (j != i && j != -1) {
          accessibilityNodeProviderImpl.sendAccessibilityEventForVirtualView(j, 256);
          accessibilityNodeProviderImpl.sendAccessibilityEventForVirtualView(i, 128);
          this.mLastHoveredChildVirtualViewId = i;
          accessibilityNodeProviderImpl.performAction(i, 64, null);
        } 
      } 
    } 
    return false;
  }
  
  public void computeScroll() {
    Scroller scroller1 = this.mFlingScroller;
    Scroller scroller2 = scroller1;
    if (scroller1.isFinished()) {
      scroller1 = this.mAdjustScroller;
      scroller2 = scroller1;
      if (scroller1.isFinished())
        return; 
    } 
    scroller2.computeScrollOffset();
    int i = scroller2.getCurrY();
    if (this.mPreviousScrollerY == 0)
      this.mPreviousScrollerY = scroller2.getStartY(); 
    scrollBy(0, i - this.mPreviousScrollerY);
    this.mPreviousScrollerY = i;
    if (scroller2.isFinished()) {
      onScrollerFinished(scroller2);
    } else {
      invalidate();
    } 
  }
  
  public void setEnabled(boolean paramBoolean) {
    super.setEnabled(paramBoolean);
    if (!this.mHasSelectorWheel)
      this.mIncrementButton.setEnabled(paramBoolean); 
    if (!this.mHasSelectorWheel)
      this.mDecrementButton.setEnabled(paramBoolean); 
    this.mInputText.setEnabled(paramBoolean);
  }
  
  public void scrollBy(int paramInt1, int paramInt2) {
    int[] arrayOfInt = this.mSelectorIndices;
    paramInt1 = this.mCurrentScrollOffset;
    if (!this.mWrapSelectorWheel && paramInt2 > 0 && arrayOfInt[1] <= this.mMinValue) {
      this.mCurrentScrollOffset = this.mInitialScrollOffset;
      return;
    } 
    if (!this.mWrapSelectorWheel && paramInt2 < 0 && arrayOfInt[1] >= this.mMaxValue) {
      this.mCurrentScrollOffset = this.mInitialScrollOffset;
      return;
    } 
    this.mCurrentScrollOffset += paramInt2;
    while (true) {
      paramInt2 = this.mCurrentScrollOffset;
      if (paramInt2 - this.mInitialScrollOffset > this.mSelectorTextGapHeight) {
        this.mCurrentScrollOffset = paramInt2 - this.mSelectorElementHeight;
        decrementSelectorIndices(arrayOfInt);
        setValueInternal(arrayOfInt[1], true);
        if (!this.mWrapSelectorWheel && arrayOfInt[1] <= this.mMinValue)
          this.mCurrentScrollOffset = this.mInitialScrollOffset; 
        continue;
      } 
      break;
    } 
    while (true) {
      paramInt2 = this.mCurrentScrollOffset;
      if (paramInt2 - this.mInitialScrollOffset < -this.mSelectorTextGapHeight) {
        this.mCurrentScrollOffset = paramInt2 + this.mSelectorElementHeight;
        incrementSelectorIndices(arrayOfInt);
        setValueInternal(arrayOfInt[1], true);
        if (!this.mWrapSelectorWheel && arrayOfInt[1] >= this.mMaxValue)
          this.mCurrentScrollOffset = this.mInitialScrollOffset; 
        continue;
      } 
      break;
    } 
    if (paramInt1 != paramInt2)
      onScrollChanged(0, paramInt2, 0, paramInt1); 
  }
  
  protected int computeVerticalScrollOffset() {
    return this.mCurrentScrollOffset;
  }
  
  protected int computeVerticalScrollRange() {
    return (this.mMaxValue - this.mMinValue + 1) * this.mSelectorElementHeight;
  }
  
  protected int computeVerticalScrollExtent() {
    return getHeight();
  }
  
  public int getSolidColor() {
    return this.mSolidColor;
  }
  
  public void setOnValueChangedListener(OnValueChangeListener paramOnValueChangeListener) {
    this.mOnValueChangeListener = paramOnValueChangeListener;
  }
  
  public void setOnScrollListener(OnScrollListener paramOnScrollListener) {
    this.mOnScrollListener = paramOnScrollListener;
  }
  
  public void setFormatter(Formatter paramFormatter) {
    if (paramFormatter == this.mFormatter)
      return; 
    this.mFormatter = paramFormatter;
    initializeSelectorWheelIndices();
    updateInputTextView();
  }
  
  public void setValue(int paramInt) {
    setValueInternal(paramInt, false);
  }
  
  public boolean performClick() {
    if (!this.mHasSelectorWheel)
      return super.performClick(); 
    if (!super.performClick())
      showSoftInput(); 
    return true;
  }
  
  public boolean performLongClick() {
    if (!this.mHasSelectorWheel)
      return super.performLongClick(); 
    if (!super.performLongClick()) {
      showSoftInput();
      this.mIgnoreMoveEvents = true;
    } 
    return true;
  }
  
  private void showSoftInput() {
    InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
    if (inputMethodManager != null) {
      if (this.mHasSelectorWheel)
        this.mInputText.setVisibility(0); 
      this.mInputText.requestFocus();
      inputMethodManager.showSoftInput(this.mInputText, 0);
    } 
  }
  
  private void hideSoftInput() {
    InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
    if (inputMethodManager != null && inputMethodManager.isActive(this.mInputText))
      inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0); 
    if (this.mHasSelectorWheel)
      this.mInputText.setVisibility(4); 
  }
  
  private void tryComputeMaxWidth() {
    int k;
    if (!this.mComputeMaxWidth)
      return; 
    int i = 0;
    String[] arrayOfString = this.mDisplayedValues;
    if (arrayOfString == null) {
      float f = 0.0F;
      for (i = 0; i <= 9; i++, f = f2) {
        float f1 = this.mSelectorWheelPaint.measureText(formatNumberWithLocale(i));
        float f2 = f;
        if (f1 > f)
          f2 = f1; 
      } 
      byte b = 0;
      i = this.mMaxValue;
      while (i > 0) {
        b++;
        i /= 10;
      } 
      k = (int)(b * f);
    } else {
      int m = arrayOfString.length;
      byte b = 0;
      while (true) {
        k = i;
        if (b < m) {
          float f = this.mSelectorWheelPaint.measureText(this.mDisplayedValues[b]);
          k = i;
          if (f > i)
            k = (int)f; 
          b++;
          i = k;
          continue;
        } 
        break;
      } 
    } 
    int j = k + this.mInputText.getPaddingLeft() + this.mInputText.getPaddingRight();
    if (this.mMaxWidth != j) {
      i = this.mMinWidth;
      if (j > i) {
        this.mMaxWidth = j;
      } else {
        this.mMaxWidth = i;
      } 
      invalidate();
    } 
  }
  
  public boolean getWrapSelectorWheel() {
    return this.mWrapSelectorWheel;
  }
  
  public void setWrapSelectorWheel(boolean paramBoolean) {
    this.mWrapSelectorWheelPreferred = paramBoolean;
    updateWrapSelectorWheel();
  }
  
  private void updateWrapSelectorWheel() {
    int i = this.mMaxValue, j = this.mMinValue, k = this.mSelectorIndices.length;
    boolean bool = true;
    if (i - j >= k) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i == 0 || !this.mWrapSelectorWheelPreferred)
      bool = false; 
    this.mWrapSelectorWheel = bool;
  }
  
  public void setOnLongPressUpdateInterval(long paramLong) {
    this.mLongPressUpdateInterval = paramLong;
  }
  
  public int getValue() {
    return this.mValue;
  }
  
  public int getMinValue() {
    return this.mMinValue;
  }
  
  public void setMinValue(int paramInt) {
    if (this.mMinValue == paramInt)
      return; 
    if (paramInt >= 0) {
      this.mMinValue = paramInt;
      if (paramInt > this.mValue)
        this.mValue = paramInt; 
      updateWrapSelectorWheel();
      initializeSelectorWheelIndices();
      updateInputTextView();
      tryComputeMaxWidth();
      invalidate();
      return;
    } 
    throw new IllegalArgumentException("minValue must be >= 0");
  }
  
  public int getMaxValue() {
    return this.mMaxValue;
  }
  
  public void setMaxValue(int paramInt) {
    if (this.mMaxValue == paramInt)
      return; 
    if (paramInt >= 0) {
      this.mMaxValue = paramInt;
      if (paramInt < this.mValue)
        this.mValue = paramInt; 
      updateWrapSelectorWheel();
      initializeSelectorWheelIndices();
      updateInputTextView();
      tryComputeMaxWidth();
      invalidate();
      return;
    } 
    throw new IllegalArgumentException("maxValue must be >= 0");
  }
  
  public String[] getDisplayedValues() {
    return this.mDisplayedValues;
  }
  
  public void setDisplayedValues(String[] paramArrayOfString) {
    if (this.mDisplayedValues == paramArrayOfString)
      return; 
    this.mDisplayedValues = paramArrayOfString;
    if (paramArrayOfString != null) {
      this.mInputText.setRawInputType(524289);
    } else {
      this.mInputText.setRawInputType(2);
    } 
    updateInputTextView();
    initializeSelectorWheelIndices();
    tryComputeMaxWidth();
  }
  
  public CharSequence getDisplayedValueForCurrentSelection() {
    return this.mSelectorIndexToStringCache.get(getValue());
  }
  
  public void setSelectionDividerHeight(int paramInt) {
    this.mSelectionDividerHeight = paramInt;
    invalidate();
  }
  
  public int getSelectionDividerHeight() {
    return this.mSelectionDividerHeight;
  }
  
  protected float getTopFadingEdgeStrength() {
    return 0.9F;
  }
  
  protected float getBottomFadingEdgeStrength() {
    return 0.9F;
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    removeAllCallbacks();
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = this.mSelectionDivider;
    if (drawable != null && drawable.isStateful() && 
      drawable.setState(getDrawableState()))
      invalidateDrawable(drawable); 
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mSelectionDivider;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
  }
  
  public void onResolveDrawables(int paramInt) {
    super.onResolveDrawables(paramInt);
    Drawable drawable = this.mSelectionDivider;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
  }
  
  protected void onDraw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mHasSelectorWheel : Z
    //   4: ifne -> 13
    //   7: aload_0
    //   8: aload_1
    //   9: invokespecial onDraw : (Landroid/graphics/Canvas;)V
    //   12: return
    //   13: aload_0
    //   14: getfield mHideWheelUntilFocused : Z
    //   17: ifeq -> 28
    //   20: aload_0
    //   21: invokevirtual hasFocus : ()Z
    //   24: istore_2
    //   25: goto -> 30
    //   28: iconst_1
    //   29: istore_2
    //   30: aload_0
    //   31: getfield mRight : I
    //   34: aload_0
    //   35: getfield mLeft : I
    //   38: isub
    //   39: iconst_2
    //   40: idiv
    //   41: i2f
    //   42: fstore_3
    //   43: aload_0
    //   44: getfield mCurrentScrollOffset : I
    //   47: i2f
    //   48: fstore #4
    //   50: iload_2
    //   51: ifeq -> 159
    //   54: aload_0
    //   55: getfield mVirtualButtonPressedDrawable : Landroid/graphics/drawable/Drawable;
    //   58: astore #5
    //   60: aload #5
    //   62: ifnull -> 159
    //   65: aload_0
    //   66: getfield mScrollState : I
    //   69: ifne -> 159
    //   72: aload_0
    //   73: getfield mDecrementVirtualButtonPressed : Z
    //   76: ifeq -> 113
    //   79: aload #5
    //   81: getstatic android/widget/NumberPicker.PRESSED_STATE_SET : [I
    //   84: invokevirtual setState : ([I)Z
    //   87: pop
    //   88: aload_0
    //   89: getfield mVirtualButtonPressedDrawable : Landroid/graphics/drawable/Drawable;
    //   92: iconst_0
    //   93: iconst_0
    //   94: aload_0
    //   95: getfield mRight : I
    //   98: aload_0
    //   99: getfield mTopSelectionDividerTop : I
    //   102: invokevirtual setBounds : (IIII)V
    //   105: aload_0
    //   106: getfield mVirtualButtonPressedDrawable : Landroid/graphics/drawable/Drawable;
    //   109: aload_1
    //   110: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   113: aload_0
    //   114: getfield mIncrementVirtualButtonPressed : Z
    //   117: ifeq -> 159
    //   120: aload_0
    //   121: getfield mVirtualButtonPressedDrawable : Landroid/graphics/drawable/Drawable;
    //   124: getstatic android/widget/NumberPicker.PRESSED_STATE_SET : [I
    //   127: invokevirtual setState : ([I)Z
    //   130: pop
    //   131: aload_0
    //   132: getfield mVirtualButtonPressedDrawable : Landroid/graphics/drawable/Drawable;
    //   135: iconst_0
    //   136: aload_0
    //   137: getfield mBottomSelectionDividerBottom : I
    //   140: aload_0
    //   141: getfield mRight : I
    //   144: aload_0
    //   145: getfield mBottom : I
    //   148: invokevirtual setBounds : (IIII)V
    //   151: aload_0
    //   152: getfield mVirtualButtonPressedDrawable : Landroid/graphics/drawable/Drawable;
    //   155: aload_1
    //   156: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   159: aload_0
    //   160: getfield mSelectorIndices : [I
    //   163: astore #6
    //   165: iconst_0
    //   166: istore #7
    //   168: iload #7
    //   170: aload #6
    //   172: arraylength
    //   173: if_icmpge -> 256
    //   176: aload #6
    //   178: iload #7
    //   180: iaload
    //   181: istore #8
    //   183: aload_0
    //   184: getfield mSelectorIndexToStringCache : Landroid/util/SparseArray;
    //   187: iload #8
    //   189: invokevirtual get : (I)Ljava/lang/Object;
    //   192: checkcast java/lang/String
    //   195: astore #9
    //   197: iload_2
    //   198: ifeq -> 207
    //   201: iload #7
    //   203: iconst_1
    //   204: if_icmpne -> 227
    //   207: iload #7
    //   209: iconst_1
    //   210: if_icmpne -> 240
    //   213: aload_0
    //   214: getfield mInputText : Landroid/widget/EditText;
    //   217: astore #5
    //   219: aload #5
    //   221: invokevirtual getVisibility : ()I
    //   224: ifeq -> 240
    //   227: aload_1
    //   228: aload #9
    //   230: fload_3
    //   231: fload #4
    //   233: aload_0
    //   234: getfield mSelectorWheelPaint : Landroid/graphics/Paint;
    //   237: invokevirtual drawText : (Ljava/lang/String;FFLandroid/graphics/Paint;)V
    //   240: fload #4
    //   242: aload_0
    //   243: getfield mSelectorElementHeight : I
    //   246: i2f
    //   247: fadd
    //   248: fstore #4
    //   250: iinc #7, 1
    //   253: goto -> 168
    //   256: iload_2
    //   257: ifeq -> 347
    //   260: aload_0
    //   261: getfield mSelectionDivider : Landroid/graphics/drawable/Drawable;
    //   264: astore #5
    //   266: aload #5
    //   268: ifnull -> 347
    //   271: aload_0
    //   272: getfield mTopSelectionDividerTop : I
    //   275: istore #8
    //   277: aload_0
    //   278: getfield mSelectionDividerHeight : I
    //   281: istore #7
    //   283: aload #5
    //   285: iconst_0
    //   286: iload #8
    //   288: aload_0
    //   289: getfield mRight : I
    //   292: iload #7
    //   294: iload #8
    //   296: iadd
    //   297: invokevirtual setBounds : (IIII)V
    //   300: aload_0
    //   301: getfield mSelectionDivider : Landroid/graphics/drawable/Drawable;
    //   304: aload_1
    //   305: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   308: aload_0
    //   309: getfield mBottomSelectionDividerBottom : I
    //   312: istore #8
    //   314: aload_0
    //   315: getfield mSelectionDividerHeight : I
    //   318: istore #7
    //   320: aload_0
    //   321: getfield mSelectionDivider : Landroid/graphics/drawable/Drawable;
    //   324: iconst_0
    //   325: iload #8
    //   327: iload #7
    //   329: isub
    //   330: aload_0
    //   331: getfield mRight : I
    //   334: iload #8
    //   336: invokevirtual setBounds : (IIII)V
    //   339: aload_0
    //   340: getfield mSelectionDivider : Landroid/graphics/drawable/Drawable;
    //   343: aload_1
    //   344: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   347: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1647	-> 0
    //   #1648	-> 7
    //   #1649	-> 12
    //   #1651	-> 13
    //   #1652	-> 30
    //   #1653	-> 43
    //   #1656	-> 50
    //   #1658	-> 72
    //   #1659	-> 79
    //   #1660	-> 88
    //   #1661	-> 105
    //   #1663	-> 113
    //   #1664	-> 120
    //   #1665	-> 131
    //   #1667	-> 151
    //   #1672	-> 159
    //   #1673	-> 165
    //   #1674	-> 176
    //   #1675	-> 183
    //   #1681	-> 197
    //   #1682	-> 219
    //   #1683	-> 227
    //   #1685	-> 240
    //   #1673	-> 250
    //   #1689	-> 256
    //   #1691	-> 271
    //   #1692	-> 277
    //   #1693	-> 283
    //   #1694	-> 300
    //   #1697	-> 308
    //   #1698	-> 314
    //   #1699	-> 320
    //   #1700	-> 339
    //   #1702	-> 347
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(NumberPicker.class.getName());
    paramAccessibilityEvent.setScrollable(true);
    paramAccessibilityEvent.setScrollY((this.mMinValue + this.mValue) * this.mSelectorElementHeight);
    paramAccessibilityEvent.setMaxScrollY((this.mMaxValue - this.mMinValue) * this.mSelectorElementHeight);
  }
  
  public AccessibilityNodeProvider getAccessibilityNodeProvider() {
    if (!this.mHasSelectorWheel)
      return super.getAccessibilityNodeProvider(); 
    if (this.mAccessibilityNodeProvider == null)
      this.mAccessibilityNodeProvider = new AccessibilityNodeProviderImpl(); 
    return this.mAccessibilityNodeProvider;
  }
  
  public void setTextColor(int paramInt) {
    this.mSelectorWheelPaint.setColor(paramInt);
    this.mInputText.setTextColor(paramInt);
    invalidate();
  }
  
  public int getTextColor() {
    return this.mSelectorWheelPaint.getColor();
  }
  
  public void setTextSize(float paramFloat) {
    this.mSelectorWheelPaint.setTextSize(paramFloat);
    this.mInputText.setTextSize(0, paramFloat);
    invalidate();
  }
  
  public float getTextSize() {
    return this.mSelectorWheelPaint.getTextSize();
  }
  
  private int makeMeasureSpec(int paramInt1, int paramInt2) {
    if (paramInt2 == -1)
      return paramInt1; 
    int i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getMode(paramInt1);
    if (j != Integer.MIN_VALUE) {
      if (j != 0) {
        if (j == 1073741824)
          return paramInt1; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown measure mode: ");
        stringBuilder.append(j);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    } 
    return View.MeasureSpec.makeMeasureSpec(Math.min(i, paramInt2), 1073741824);
  }
  
  private int resolveSizeAndStateRespectingMinSize(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 != -1) {
      paramInt1 = Math.max(paramInt1, paramInt2);
      return resolveSizeAndState(paramInt1, paramInt3, 0);
    } 
    return paramInt2;
  }
  
  private void initializeSelectorWheelIndices() {
    this.mSelectorIndexToStringCache.clear();
    int[] arrayOfInt = this.mSelectorIndices;
    int i = getValue();
    for (byte b = 0; b < this.mSelectorIndices.length; b++) {
      int j = b - 1 + i;
      int k = j;
      if (this.mWrapSelectorWheel)
        k = getWrappedSelectorIndex(j); 
      arrayOfInt[b] = k;
      ensureCachedScrollSelectorValue(arrayOfInt[b]);
    } 
  }
  
  private void setValueInternal(int paramInt, boolean paramBoolean) {
    if (this.mValue == paramInt)
      return; 
    if (this.mWrapSelectorWheel) {
      paramInt = getWrappedSelectorIndex(paramInt);
    } else {
      paramInt = Math.max(paramInt, this.mMinValue);
      paramInt = Math.min(paramInt, this.mMaxValue);
    } 
    int i = this.mValue;
    this.mValue = paramInt;
    if (this.mScrollState != 2)
      updateInputTextView(); 
    if (paramBoolean)
      notifyChange(i, paramInt); 
    initializeSelectorWheelIndices();
    invalidate();
  }
  
  private void changeValueByOne(boolean paramBoolean) {
    if (this.mHasSelectorWheel) {
      hideSoftInput();
      if (!moveToFinalScrollerPosition(this.mFlingScroller))
        moveToFinalScrollerPosition(this.mAdjustScroller); 
      this.mPreviousScrollerY = 0;
      if (paramBoolean) {
        this.mFlingScroller.startScroll(0, 0, 0, -this.mSelectorElementHeight, 300);
      } else {
        this.mFlingScroller.startScroll(0, 0, 0, this.mSelectorElementHeight, 300);
      } 
      invalidate();
    } else if (paramBoolean) {
      setValueInternal(this.mValue + 1, true);
    } else {
      setValueInternal(this.mValue - 1, true);
    } 
  }
  
  private void initializeSelectorWheel() {
    initializeSelectorWheelIndices();
    int[] arrayOfInt = this.mSelectorIndices;
    int i = arrayOfInt.length, j = this.mTextSize;
    float f1 = (this.mBottom - this.mTop - i * j);
    float f2 = arrayOfInt.length;
    this.mSelectorTextGapHeight = j = (int)(f1 / f2 + 0.5F);
    this.mSelectorElementHeight = this.mTextSize + j;
    j = this.mInputText.getBaseline();
    i = this.mInputText.getTop();
    this.mInitialScrollOffset = j = j + i - this.mSelectorElementHeight * 1;
    this.mCurrentScrollOffset = j;
    updateInputTextView();
  }
  
  private void initializeFadingEdges() {
    setVerticalFadingEdgeEnabled(true);
    setFadingEdgeLength((this.mBottom - this.mTop - this.mTextSize) / 2);
  }
  
  private void onScrollerFinished(Scroller paramScroller) {
    if (paramScroller == this.mFlingScroller) {
      ensureScrollWheelAdjusted();
      updateInputTextView();
      onScrollStateChange(0);
    } else if (this.mScrollState != 1) {
      updateInputTextView();
    } 
  }
  
  private void onScrollStateChange(int paramInt) {
    if (this.mScrollState == paramInt)
      return; 
    this.mScrollState = paramInt;
    OnScrollListener onScrollListener = this.mOnScrollListener;
    if (onScrollListener != null)
      onScrollListener.onScrollStateChange(this, paramInt); 
  }
  
  private void fling(int paramInt) {
    this.mPreviousScrollerY = 0;
    if (paramInt > 0) {
      this.mFlingScroller.fling(0, 0, 0, paramInt, 0, 0, 0, 2147483647);
    } else {
      this.mFlingScroller.fling(0, 2147483647, 0, paramInt, 0, 0, 0, 2147483647);
    } 
    invalidate();
  }
  
  private int getWrappedSelectorIndex(int paramInt) {
    int i = this.mMaxValue;
    if (paramInt > i) {
      int k = this.mMinValue;
      return k + (paramInt - i) % (i - k) - 1;
    } 
    int j = this.mMinValue;
    if (paramInt < j)
      return i - (j - paramInt) % (i - j) + 1; 
    return paramInt;
  }
  
  private void incrementSelectorIndices(int[] paramArrayOfint) {
    int i;
    for (i = 0; i < paramArrayOfint.length - 1; i++)
      paramArrayOfint[i] = paramArrayOfint[i + 1]; 
    int j = paramArrayOfint[paramArrayOfint.length - 2] + 1;
    i = j;
    if (this.mWrapSelectorWheel) {
      i = j;
      if (j > this.mMaxValue)
        i = this.mMinValue; 
    } 
    paramArrayOfint[paramArrayOfint.length - 1] = i;
    ensureCachedScrollSelectorValue(i);
  }
  
  private void decrementSelectorIndices(int[] paramArrayOfint) {
    int i;
    for (i = paramArrayOfint.length - 1; i > 0; i--)
      paramArrayOfint[i] = paramArrayOfint[i - 1]; 
    int j = paramArrayOfint[1] - 1;
    i = j;
    if (this.mWrapSelectorWheel) {
      i = j;
      if (j < this.mMinValue)
        i = this.mMaxValue; 
    } 
    paramArrayOfint[0] = i;
    ensureCachedScrollSelectorValue(i);
  }
  
  private void ensureCachedScrollSelectorValue(int paramInt) {
    SparseArray<String> sparseArray = this.mSelectorIndexToStringCache;
    String str = sparseArray.get(paramInt);
    if (str != null)
      return; 
    int i = this.mMinValue;
    if (paramInt < i || paramInt > this.mMaxValue) {
      str = "";
    } else {
      String[] arrayOfString = this.mDisplayedValues;
      if (arrayOfString != null) {
        String str1 = arrayOfString[paramInt - i];
      } else {
        str = formatNumber(paramInt);
      } 
    } 
    sparseArray.put(paramInt, str);
  }
  
  private String formatNumber(int paramInt) {
    String str;
    Formatter formatter = this.mFormatter;
    if (formatter != null) {
      str = formatter.format(paramInt);
    } else {
      str = formatNumberWithLocale(paramInt);
    } 
    return str;
  }
  
  private void validateInputTextView(View paramView) {
    String str = String.valueOf(((TextView)paramView).getText());
    if (TextUtils.isEmpty(str)) {
      updateInputTextView();
    } else {
      int i = getSelectedPos(str.toString());
      setValueInternal(i, true);
    } 
  }
  
  private boolean updateInputTextView() {
    String str, arrayOfString[] = this.mDisplayedValues;
    if (arrayOfString == null) {
      str = formatNumber(this.mValue);
    } else {
      str = str[this.mValue - this.mMinValue];
    } 
    if (!TextUtils.isEmpty(str)) {
      Editable editable = this.mInputText.getText();
      if (!str.equals(editable.toString())) {
        this.mInputText.setText(str);
        if (AccessibilityManager.getInstance(this.mContext).isEnabled()) {
          AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(16);
          this.mInputText.onInitializeAccessibilityEvent(accessibilityEvent);
          this.mInputText.onPopulateAccessibilityEvent(accessibilityEvent);
          accessibilityEvent.setFromIndex(0);
          accessibilityEvent.setRemovedCount(editable.length());
          accessibilityEvent.setAddedCount(str.length());
          accessibilityEvent.setBeforeText(editable);
          accessibilityEvent.setSource(this, 2);
          requestSendAccessibilityEvent(this, accessibilityEvent);
        } 
        return true;
      } 
    } 
    return false;
  }
  
  private void notifyChange(int paramInt1, int paramInt2) {
    OnValueChangeListener onValueChangeListener = this.mOnValueChangeListener;
    if (onValueChangeListener != null)
      onValueChangeListener.onValueChange(this, paramInt1, this.mValue); 
  }
  
  private void postChangeCurrentByOneFromLongPress(boolean paramBoolean, long paramLong) {
    ChangeCurrentByOneFromLongPressCommand changeCurrentByOneFromLongPressCommand = this.mChangeCurrentByOneFromLongPressCommand;
    if (changeCurrentByOneFromLongPressCommand == null) {
      this.mChangeCurrentByOneFromLongPressCommand = new ChangeCurrentByOneFromLongPressCommand();
    } else {
      removeCallbacks(changeCurrentByOneFromLongPressCommand);
    } 
    this.mChangeCurrentByOneFromLongPressCommand.setStep(paramBoolean);
    postDelayed(this.mChangeCurrentByOneFromLongPressCommand, paramLong);
  }
  
  private void removeChangeCurrentByOneFromLongPress() {
    ChangeCurrentByOneFromLongPressCommand changeCurrentByOneFromLongPressCommand = this.mChangeCurrentByOneFromLongPressCommand;
    if (changeCurrentByOneFromLongPressCommand != null)
      removeCallbacks(changeCurrentByOneFromLongPressCommand); 
  }
  
  private void postBeginSoftInputOnLongPressCommand() {
    BeginSoftInputOnLongPressCommand beginSoftInputOnLongPressCommand = this.mBeginSoftInputOnLongPressCommand;
    if (beginSoftInputOnLongPressCommand == null) {
      this.mBeginSoftInputOnLongPressCommand = new BeginSoftInputOnLongPressCommand();
    } else {
      removeCallbacks(beginSoftInputOnLongPressCommand);
    } 
    postDelayed(this.mBeginSoftInputOnLongPressCommand, ViewConfiguration.getLongPressTimeout());
  }
  
  private void removeBeginSoftInputCommand() {
    BeginSoftInputOnLongPressCommand beginSoftInputOnLongPressCommand = this.mBeginSoftInputOnLongPressCommand;
    if (beginSoftInputOnLongPressCommand != null)
      removeCallbacks(beginSoftInputOnLongPressCommand); 
  }
  
  private void removeAllCallbacks() {
    ChangeCurrentByOneFromLongPressCommand changeCurrentByOneFromLongPressCommand = this.mChangeCurrentByOneFromLongPressCommand;
    if (changeCurrentByOneFromLongPressCommand != null)
      removeCallbacks(changeCurrentByOneFromLongPressCommand); 
    SetSelectionCommand setSelectionCommand = this.mSetSelectionCommand;
    if (setSelectionCommand != null)
      setSelectionCommand.cancel(); 
    BeginSoftInputOnLongPressCommand beginSoftInputOnLongPressCommand = this.mBeginSoftInputOnLongPressCommand;
    if (beginSoftInputOnLongPressCommand != null)
      removeCallbacks(beginSoftInputOnLongPressCommand); 
    this.mPressedStateHelper.cancel();
  }
  
  private int getSelectedPos(String paramString) {
    if (this.mDisplayedValues == null) {
      try {
        return Integer.parseInt(paramString);
      } catch (NumberFormatException numberFormatException) {}
    } else {
      String str;
      int i;
      for (i = 0; i < this.mDisplayedValues.length; i++) {
        str = numberFormatException.toLowerCase();
        if (this.mDisplayedValues[i].toLowerCase().startsWith(str))
          return this.mMinValue + i; 
      } 
      try {
        return Integer.parseInt(str);
      } catch (NumberFormatException numberFormatException1) {}
    } 
    return this.mMinValue;
  }
  
  private void postSetSelectionCommand(int paramInt1, int paramInt2) {
    if (this.mSetSelectionCommand == null)
      this.mSetSelectionCommand = new SetSelectionCommand(this.mInputText); 
    this.mSetSelectionCommand.post(paramInt1, paramInt2);
  }
  
  static {
    DIGIT_CHARACTERS = new char[] { 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩', 
        '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', 
        '०', '१', '२', '३', '४', '५', '६', '७', '८', '९', 
        '০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', 
        '೦', '೧', '೨', '೩', '೪', '೫', '೬', '೭', '೮', '೯' };
  }
  
  class InputTextFilter extends NumberKeyListener {
    final NumberPicker this$0;
    
    public int getInputType() {
      return 1;
    }
    
    protected char[] getAcceptedChars() {
      return NumberPicker.DIGIT_CHARACTERS;
    }
    
    public CharSequence filter(CharSequence param1CharSequence, int param1Int1, int param1Int2, Spanned param1Spanned, int param1Int3, int param1Int4) {
      if (NumberPicker.this.mSetSelectionCommand != null)
        NumberPicker.this.mSetSelectionCommand.cancel(); 
      String[] arrayOfString2 = NumberPicker.this.mDisplayedValues;
      boolean bool = false;
      if (arrayOfString2 == null) {
        CharSequence charSequence2 = super.filter(param1CharSequence, param1Int1, param1Int2, param1Spanned, param1Int3, param1Int4);
        CharSequence charSequence1 = charSequence2;
        if (charSequence2 == null)
          charSequence1 = param1CharSequence.subSequence(param1Int1, param1Int2); 
        param1CharSequence = new StringBuilder();
        param1CharSequence.append(String.valueOf(param1Spanned.subSequence(0, param1Int3)));
        param1CharSequence.append(charSequence1);
        param1CharSequence.append(param1Spanned.subSequence(param1Int4, param1Spanned.length()));
        param1CharSequence = param1CharSequence.toString();
        if ("".equals(param1CharSequence))
          return param1CharSequence; 
        param1Int1 = NumberPicker.this.getSelectedPos((String)param1CharSequence);
        if (param1Int1 > NumberPicker.this.mMaxValue || param1CharSequence.length() > String.valueOf(NumberPicker.this.mMaxValue).length())
          return ""; 
        return charSequence1;
      } 
      String str1 = String.valueOf(param1CharSequence.subSequence(param1Int1, param1Int2));
      if (TextUtils.isEmpty(str1))
        return ""; 
      param1CharSequence = new StringBuilder();
      param1CharSequence.append(String.valueOf(param1Spanned.subSequence(0, param1Int3)));
      param1CharSequence.append(str1);
      param1CharSequence.append(param1Spanned.subSequence(param1Int4, param1Spanned.length()));
      String str2 = param1CharSequence.toString();
      String str3 = String.valueOf(str2).toLowerCase();
      for (String[] arrayOfString1 = NumberPicker.this.mDisplayedValues; param1Int1 < param1Int2; ) {
        str1 = arrayOfString1[param1Int1];
        param1CharSequence = str1.toLowerCase();
        if (param1CharSequence.startsWith(str3)) {
          NumberPicker.this.postSetSelectionCommand(str2.length(), str1.length());
          return str1.subSequence(param1Int3, str1.length());
        } 
        param1Int1++;
      } 
      return "";
    }
  }
  
  private boolean ensureScrollWheelAdjusted() {
    int i = this.mInitialScrollOffset - this.mCurrentScrollOffset;
    if (i != 0) {
      this.mPreviousScrollerY = 0;
      int j = Math.abs(i), k = this.mSelectorElementHeight, m = i;
      if (j > k / 2) {
        m = k;
        if (i > 0)
          m = -k; 
        m = i + m;
      } 
      this.mAdjustScroller.startScroll(0, 0, 0, m, 800);
      invalidate();
      return true;
    } 
    return false;
  }
  
  class PressedStateHelper implements Runnable {
    public static final int BUTTON_DECREMENT = 2;
    
    public static final int BUTTON_INCREMENT = 1;
    
    private final int MODE_PRESS = 1;
    
    private final int MODE_TAPPED = 2;
    
    private int mManagedButton;
    
    private int mMode;
    
    final NumberPicker this$0;
    
    public void cancel() {
      this.mMode = 0;
      this.mManagedButton = 0;
      NumberPicker.this.removeCallbacks(this);
      if (NumberPicker.this.mIncrementVirtualButtonPressed) {
        NumberPicker.access$1202(NumberPicker.this, false);
        NumberPicker numberPicker = NumberPicker.this;
        numberPicker.invalidate(0, numberPicker.mBottomSelectionDividerBottom, NumberPicker.this.mRight, NumberPicker.this.mBottom);
      } 
      NumberPicker.access$1602(NumberPicker.this, false);
      if (NumberPicker.this.mDecrementVirtualButtonPressed) {
        NumberPicker numberPicker = NumberPicker.this;
        numberPicker.invalidate(0, 0, numberPicker.mRight, NumberPicker.this.mTopSelectionDividerTop);
      } 
    }
    
    public void buttonPressDelayed(int param1Int) {
      cancel();
      this.mMode = 1;
      this.mManagedButton = param1Int;
      NumberPicker.this.postDelayed(this, ViewConfiguration.getTapTimeout());
    }
    
    public void buttonTapped(int param1Int) {
      cancel();
      this.mMode = 2;
      this.mManagedButton = param1Int;
      NumberPicker.this.post(this);
    }
    
    public void run() {
      int i = this.mMode;
      if (i != 1) {
        if (i == 2) {
          i = this.mManagedButton;
          if (i != 1) {
            if (i == 2) {
              if (!NumberPicker.this.mDecrementVirtualButtonPressed) {
                NumberPicker numberPicker1 = NumberPicker.this;
                long l = ViewConfiguration.getPressedStateDuration();
                numberPicker1.postDelayed(this, l);
              } 
              NumberPicker.access$1680(NumberPicker.this, 1);
              NumberPicker numberPicker = NumberPicker.this;
              numberPicker.invalidate(0, 0, numberPicker.mRight, NumberPicker.this.mTopSelectionDividerTop);
            } 
          } else {
            if (!NumberPicker.this.mIncrementVirtualButtonPressed) {
              NumberPicker numberPicker1 = NumberPicker.this;
              long l = ViewConfiguration.getPressedStateDuration();
              numberPicker1.postDelayed(this, l);
            } 
            NumberPicker.access$1280(NumberPicker.this, 1);
            NumberPicker numberPicker = NumberPicker.this;
            numberPicker.invalidate(0, numberPicker.mBottomSelectionDividerBottom, NumberPicker.this.mRight, NumberPicker.this.mBottom);
          } 
        } 
      } else {
        i = this.mManagedButton;
        if (i != 1) {
          if (i == 2) {
            NumberPicker.access$1602(NumberPicker.this, true);
            NumberPicker numberPicker = NumberPicker.this;
            numberPicker.invalidate(0, 0, numberPicker.mRight, NumberPicker.this.mTopSelectionDividerTop);
          } 
        } else {
          NumberPicker.access$1202(NumberPicker.this, true);
          NumberPicker numberPicker = NumberPicker.this;
          numberPicker.invalidate(0, numberPicker.mBottomSelectionDividerBottom, NumberPicker.this.mRight, NumberPicker.this.mBottom);
        } 
      } 
    }
  }
  
  class SetSelectionCommand implements Runnable {
    private final EditText mInputText;
    
    private boolean mPosted;
    
    private int mSelectionEnd;
    
    private int mSelectionStart;
    
    public SetSelectionCommand(NumberPicker this$0) {
      this.mInputText = (EditText)this$0;
    }
    
    public void post(int param1Int1, int param1Int2) {
      this.mSelectionStart = param1Int1;
      this.mSelectionEnd = param1Int2;
      if (!this.mPosted) {
        this.mInputText.post(this);
        this.mPosted = true;
      } 
    }
    
    public void cancel() {
      if (this.mPosted) {
        this.mInputText.removeCallbacks(this);
        this.mPosted = false;
      } 
    }
    
    public void run() {
      this.mPosted = false;
      this.mInputText.setSelection(this.mSelectionStart, this.mSelectionEnd);
    }
  }
  
  class ChangeCurrentByOneFromLongPressCommand implements Runnable {
    private boolean mIncrement;
    
    final NumberPicker this$0;
    
    private void setStep(boolean param1Boolean) {
      this.mIncrement = param1Boolean;
    }
    
    public void run() {
      NumberPicker.this.changeValueByOne(this.mIncrement);
      NumberPicker numberPicker = NumberPicker.this;
      numberPicker.postDelayed(this, numberPicker.mLongPressUpdateInterval);
    }
  }
  
  public static class CustomEditText extends EditText {
    public CustomEditText(Context param1Context, AttributeSet param1AttributeSet) {
      super(param1Context, param1AttributeSet);
    }
    
    public void onEditorAction(int param1Int) {
      super.onEditorAction(param1Int);
      if (param1Int == 6)
        clearFocus(); 
    }
  }
  
  class BeginSoftInputOnLongPressCommand implements Runnable {
    final NumberPicker this$0;
    
    public void run() {
      NumberPicker.this.performLongClick();
    }
  }
  
  class AccessibilityNodeProviderImpl extends AccessibilityNodeProvider {
    final NumberPicker this$0;
    
    private final Rect mTempRect = new Rect();
    
    private final int[] mTempArray = new int[2];
    
    private int mAccessibilityFocusedView = Integer.MIN_VALUE;
    
    private static final int VIRTUAL_VIEW_ID_INPUT = 2;
    
    private static final int VIRTUAL_VIEW_ID_INCREMENT = 1;
    
    private static final int VIRTUAL_VIEW_ID_DECREMENT = 3;
    
    private static final int UNDEFINED = -2147483648;
    
    public AccessibilityNodeInfo createAccessibilityNodeInfo(int param1Int) {
      if (param1Int != -1) {
        if (param1Int != 1) {
          if (param1Int != 2) {
            if (param1Int != 3)
              return super.createAccessibilityNodeInfo(param1Int); 
            String str1 = getVirtualDecrementButtonText();
            int i18 = NumberPicker.this.mScrollX, i19 = NumberPicker.this.mScrollY;
            NumberPicker numberPicker3 = NumberPicker.this;
            int i20 = numberPicker3.mScrollX;
            param1Int = NumberPicker.this.mRight;
            int i21 = NumberPicker.this.mLeft;
            numberPicker3 = NumberPicker.this;
            int i22 = numberPicker3.mTopSelectionDividerTop, i23 = NumberPicker.this.mSelectionDividerHeight;
            return createAccessibilityNodeInfoForVirtualButton(3, str1, i18, i19, i20 + param1Int - i21, i22 + i23);
          } 
          int i17 = NumberPicker.this.mScrollX;
          NumberPicker numberPicker2 = NumberPicker.this;
          param1Int = numberPicker2.mTopSelectionDividerTop;
          int i11 = NumberPicker.this.mSelectionDividerHeight;
          numberPicker2 = NumberPicker.this;
          int i14 = numberPicker2.mScrollX, i12 = NumberPicker.this.mRight, i16 = NumberPicker.this.mLeft;
          numberPicker2 = NumberPicker.this;
          int i15 = numberPicker2.mBottomSelectionDividerBottom, i13 = NumberPicker.this.mSelectionDividerHeight;
          return createAccessibiltyNodeInfoForInputText(i17, param1Int + i11, i14 + i12 - i16, i15 - i13);
        } 
        String str = getVirtualIncrementButtonText();
        int i7 = NumberPicker.this.mScrollX;
        NumberPicker numberPicker1 = NumberPicker.this;
        int i4 = numberPicker1.mBottomSelectionDividerBottom, i9 = NumberPicker.this.mSelectionDividerHeight;
        numberPicker1 = NumberPicker.this;
        int i3 = numberPicker1.mScrollX, i8 = NumberPicker.this.mRight;
        param1Int = NumberPicker.this.mLeft;
        int i6 = NumberPicker.this.mScrollY, i10 = NumberPicker.this.mBottom, i5 = NumberPicker.this.mTop;
        return createAccessibilityNodeInfoForVirtualButton(1, str, i7, i4 - i9, i3 + i8 - param1Int, i6 + i10 - i5);
      } 
      param1Int = NumberPicker.this.mScrollX;
      int m = NumberPicker.this.mScrollY;
      NumberPicker numberPicker = NumberPicker.this;
      int k = numberPicker.mScrollX, n = NumberPicker.this.mRight, i2 = NumberPicker.this.mLeft, i = NumberPicker.this.mScrollY, i1 = NumberPicker.this.mBottom, j = NumberPicker.this.mTop;
      return createAccessibilityNodeInfoForNumberPicker(param1Int, m, k + n - i2, i + i1 - j);
    }
    
    public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String param1String, int param1Int) {
      if (TextUtils.isEmpty(param1String))
        return Collections.emptyList(); 
      String str = param1String.toLowerCase();
      ArrayList<AccessibilityNodeInfo> arrayList = new ArrayList();
      if (param1Int != -1) {
        if (param1Int != 1 && param1Int != 2 && param1Int != 3)
          return super.findAccessibilityNodeInfosByText(param1String, param1Int); 
        findAccessibilityNodeInfosByTextInChild(str, param1Int, arrayList);
        return arrayList;
      } 
      findAccessibilityNodeInfosByTextInChild(str, 3, arrayList);
      findAccessibilityNodeInfosByTextInChild(str, 2, arrayList);
      findAccessibilityNodeInfosByTextInChild(str, 1, arrayList);
      return arrayList;
    }
    
    public boolean performAction(int param1Int1, int param1Int2, Bundle param1Bundle) {
      NumberPicker numberPicker;
      boolean bool = false;
      if (param1Int1 != -1) {
        if (param1Int1 != 1) {
          if (param1Int1 != 2) {
            if (param1Int1 == 3) {
              if (param1Int2 != 16) {
                if (param1Int2 != 64) {
                  if (param1Int2 != 128)
                    return false; 
                  if (this.mAccessibilityFocusedView == param1Int1) {
                    this.mAccessibilityFocusedView = Integer.MIN_VALUE;
                    sendAccessibilityEventForVirtualView(param1Int1, 65536);
                    numberPicker = NumberPicker.this;
                    numberPicker.invalidate(0, 0, numberPicker.mRight, NumberPicker.this.mTopSelectionDividerTop);
                    return true;
                  } 
                  return false;
                } 
                if (this.mAccessibilityFocusedView != param1Int1) {
                  this.mAccessibilityFocusedView = param1Int1;
                  sendAccessibilityEventForVirtualView(param1Int1, 32768);
                  numberPicker = NumberPicker.this;
                  numberPicker.invalidate(0, 0, numberPicker.mRight, NumberPicker.this.mTopSelectionDividerTop);
                  return true;
                } 
                return false;
              } 
              if (NumberPicker.this.isEnabled()) {
                if (param1Int1 == 1)
                  bool = true; 
                NumberPicker.this.changeValueByOne(bool);
                sendAccessibilityEventForVirtualView(param1Int1, 1);
                return true;
              } 
              return false;
            } 
          } else {
            if (param1Int2 != 1) {
              if (param1Int2 != 2) {
                if (param1Int2 != 16) {
                  if (param1Int2 != 32) {
                    if (param1Int2 != 64) {
                      if (param1Int2 != 128)
                        return NumberPicker.this.mInputText.performAccessibilityAction(param1Int2, (Bundle)numberPicker); 
                      if (this.mAccessibilityFocusedView == param1Int1) {
                        this.mAccessibilityFocusedView = Integer.MIN_VALUE;
                        sendAccessibilityEventForVirtualView(param1Int1, 65536);
                        NumberPicker.this.mInputText.invalidate();
                        return true;
                      } 
                      return false;
                    } 
                    if (this.mAccessibilityFocusedView != param1Int1) {
                      this.mAccessibilityFocusedView = param1Int1;
                      sendAccessibilityEventForVirtualView(param1Int1, 32768);
                      NumberPicker.this.mInputText.invalidate();
                      return true;
                    } 
                    return false;
                  } 
                  if (NumberPicker.this.isEnabled()) {
                    NumberPicker.this.performLongClick();
                    return true;
                  } 
                  return false;
                } 
                if (NumberPicker.this.isEnabled()) {
                  NumberPicker.this.performClick();
                  return true;
                } 
                return false;
              } 
              if (NumberPicker.this.isEnabled() && NumberPicker.this.mInputText.isFocused()) {
                NumberPicker.this.mInputText.clearFocus();
                return true;
              } 
              return false;
            } 
            if (NumberPicker.this.isEnabled() && !NumberPicker.this.mInputText.isFocused())
              return NumberPicker.this.mInputText.requestFocus(); 
            return false;
          } 
        } else {
          if (param1Int2 != 16) {
            if (param1Int2 != 64) {
              if (param1Int2 != 128)
                return false; 
              if (this.mAccessibilityFocusedView == param1Int1) {
                this.mAccessibilityFocusedView = Integer.MIN_VALUE;
                sendAccessibilityEventForVirtualView(param1Int1, 65536);
                numberPicker = NumberPicker.this;
                numberPicker.invalidate(0, numberPicker.mBottomSelectionDividerBottom, NumberPicker.this.mRight, NumberPicker.this.mBottom);
                return true;
              } 
              return false;
            } 
            if (this.mAccessibilityFocusedView != param1Int1) {
              this.mAccessibilityFocusedView = param1Int1;
              sendAccessibilityEventForVirtualView(param1Int1, 32768);
              numberPicker = NumberPicker.this;
              numberPicker.invalidate(0, numberPicker.mBottomSelectionDividerBottom, NumberPicker.this.mRight, NumberPicker.this.mBottom);
              return true;
            } 
            return false;
          } 
          if (NumberPicker.this.isEnabled()) {
            NumberPicker.this.changeValueByOne(true);
            sendAccessibilityEventForVirtualView(param1Int1, 1);
            return true;
          } 
          return false;
        } 
      } else {
        if (param1Int2 != 64) {
          if (param1Int2 != 128) {
            if (param1Int2 != 4096)
              if (param1Int2 != 8192 && param1Int2 != 16908344) {
                if (param1Int2 != 16908346)
                  return super.performAction(param1Int1, param1Int2, (Bundle)numberPicker); 
              } else {
                if (NumberPicker.this.isEnabled()) {
                  numberPicker = NumberPicker.this;
                  if (numberPicker.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue()) {
                    NumberPicker.this.changeValueByOne(false);
                    return true;
                  } 
                } 
                return false;
              }  
            if (NumberPicker.this.isEnabled()) {
              numberPicker = NumberPicker.this;
              if (numberPicker.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue()) {
                NumberPicker.this.changeValueByOne(true);
                return true;
              } 
            } 
            return false;
          } 
          if (this.mAccessibilityFocusedView == param1Int1) {
            this.mAccessibilityFocusedView = Integer.MIN_VALUE;
            NumberPicker.this.clearAccessibilityFocus();
            return true;
          } 
          return false;
        } 
        if (this.mAccessibilityFocusedView != param1Int1) {
          this.mAccessibilityFocusedView = param1Int1;
          NumberPicker.this.requestAccessibilityFocus();
          return true;
        } 
        return false;
      } 
      return super.performAction(param1Int1, param1Int2, (Bundle)numberPicker);
    }
    
    public void sendAccessibilityEventForVirtualView(int param1Int1, int param1Int2) {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 == 3)
            if (hasVirtualDecrementButton()) {
              String str = getVirtualDecrementButtonText();
              sendAccessibilityEventForVirtualButton(param1Int1, param1Int2, str);
            }  
        } else {
          sendAccessibilityEventForVirtualText(param1Int2);
        } 
      } else if (hasVirtualIncrementButton()) {
        String str = getVirtualIncrementButtonText();
        sendAccessibilityEventForVirtualButton(param1Int1, param1Int2, str);
      } 
    }
    
    private void sendAccessibilityEventForVirtualText(int param1Int) {
      if (AccessibilityManager.getInstance(NumberPicker.this.mContext).isEnabled()) {
        AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(param1Int);
        NumberPicker.this.mInputText.onInitializeAccessibilityEvent(accessibilityEvent);
        NumberPicker.this.mInputText.onPopulateAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setSource(NumberPicker.this, 2);
        NumberPicker numberPicker = NumberPicker.this;
        numberPicker.requestSendAccessibilityEvent(numberPicker, accessibilityEvent);
      } 
    }
    
    private void sendAccessibilityEventForVirtualButton(int param1Int1, int param1Int2, String param1String) {
      if (AccessibilityManager.getInstance(NumberPicker.this.mContext).isEnabled()) {
        AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(param1Int2);
        accessibilityEvent.setClassName(Button.class.getName());
        accessibilityEvent.setPackageName(NumberPicker.this.mContext.getPackageName());
        accessibilityEvent.getText().add(param1String);
        accessibilityEvent.setEnabled(NumberPicker.this.isEnabled());
        accessibilityEvent.setSource(NumberPicker.this, param1Int1);
        NumberPicker numberPicker = NumberPicker.this;
        numberPicker.requestSendAccessibilityEvent(numberPicker, accessibilityEvent);
      } 
    }
    
    private void findAccessibilityNodeInfosByTextInChild(String param1String, int param1Int, List<AccessibilityNodeInfo> param1List) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int == 3) {
            String str1 = getVirtualDecrementButtonText();
            if (!TextUtils.isEmpty(str1) && 
              str1.toString().toLowerCase().contains(param1String))
              param1List.add(createAccessibilityNodeInfo(3)); 
            return;
          } 
        } else {
          Editable editable = NumberPicker.this.mInputText.getText();
          if (!TextUtils.isEmpty(editable) && 
            editable.toString().toLowerCase().contains(param1String)) {
            param1List.add(createAccessibilityNodeInfo(2));
            return;
          } 
          editable = NumberPicker.this.mInputText.getText();
          if (!TextUtils.isEmpty(editable) && 
            editable.toString().toLowerCase().contains(param1String)) {
            param1List.add(createAccessibilityNodeInfo(2));
            return;
          } 
        } 
        return;
      } 
      String str = getVirtualIncrementButtonText();
      if (!TextUtils.isEmpty(str) && 
        str.toString().toLowerCase().contains(param1String))
        param1List.add(createAccessibilityNodeInfo(1)); 
    }
    
    private AccessibilityNodeInfo createAccessibiltyNodeInfoForInputText(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      AccessibilityNodeInfo accessibilityNodeInfo = NumberPicker.this.mInputText.createAccessibilityNodeInfo();
      accessibilityNodeInfo.setSource(NumberPicker.this, 2);
      if (this.mAccessibilityFocusedView != 2)
        accessibilityNodeInfo.addAction(64); 
      if (this.mAccessibilityFocusedView == 2)
        accessibilityNodeInfo.addAction(128); 
      Rect rect = this.mTempRect;
      rect.set(param1Int1, param1Int2, param1Int3, param1Int4);
      accessibilityNodeInfo.setVisibleToUser(NumberPicker.this.isVisibleToUser(rect));
      accessibilityNodeInfo.setBoundsInParent(rect);
      int[] arrayOfInt = this.mTempArray;
      NumberPicker.this.getLocationOnScreen(arrayOfInt);
      rect.offset(arrayOfInt[0], arrayOfInt[1]);
      accessibilityNodeInfo.setBoundsInScreen(rect);
      return accessibilityNodeInfo;
    }
    
    private AccessibilityNodeInfo createAccessibilityNodeInfoForVirtualButton(int param1Int1, String param1String, int param1Int2, int param1Int3, int param1Int4, int param1Int5) {
      AccessibilityNodeInfo accessibilityNodeInfo = AccessibilityNodeInfo.obtain();
      accessibilityNodeInfo.setClassName(Button.class.getName());
      accessibilityNodeInfo.setPackageName(NumberPicker.this.mContext.getPackageName());
      accessibilityNodeInfo.setSource(NumberPicker.this, param1Int1);
      accessibilityNodeInfo.setParent(NumberPicker.this);
      accessibilityNodeInfo.setText(param1String);
      accessibilityNodeInfo.setClickable(true);
      accessibilityNodeInfo.setLongClickable(true);
      accessibilityNodeInfo.setEnabled(NumberPicker.this.isEnabled());
      Rect rect = this.mTempRect;
      rect.set(param1Int2, param1Int3, param1Int4, param1Int5);
      accessibilityNodeInfo.setVisibleToUser(NumberPicker.this.isVisibleToUser(rect));
      accessibilityNodeInfo.setBoundsInParent(rect);
      int[] arrayOfInt = this.mTempArray;
      NumberPicker.this.getLocationOnScreen(arrayOfInt);
      rect.offset(arrayOfInt[0], arrayOfInt[1]);
      accessibilityNodeInfo.setBoundsInScreen(rect);
      if (this.mAccessibilityFocusedView != param1Int1)
        accessibilityNodeInfo.addAction(64); 
      if (this.mAccessibilityFocusedView == param1Int1)
        accessibilityNodeInfo.addAction(128); 
      if (NumberPicker.this.isEnabled())
        accessibilityNodeInfo.addAction(16); 
      return accessibilityNodeInfo;
    }
    
    private AccessibilityNodeInfo createAccessibilityNodeInfoForNumberPicker(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      AccessibilityNodeInfo accessibilityNodeInfo = AccessibilityNodeInfo.obtain();
      accessibilityNodeInfo.setClassName(NumberPicker.class.getName());
      accessibilityNodeInfo.setPackageName(NumberPicker.this.mContext.getPackageName());
      accessibilityNodeInfo.setSource(NumberPicker.this);
      if (hasVirtualDecrementButton())
        accessibilityNodeInfo.addChild(NumberPicker.this, 3); 
      accessibilityNodeInfo.addChild(NumberPicker.this, 2);
      if (hasVirtualIncrementButton())
        accessibilityNodeInfo.addChild(NumberPicker.this, 1); 
      accessibilityNodeInfo.setParent((View)NumberPicker.this.getParentForAccessibility());
      accessibilityNodeInfo.setEnabled(NumberPicker.this.isEnabled());
      accessibilityNodeInfo.setScrollable(true);
      NumberPicker numberPicker = NumberPicker.this;
      float f = (numberPicker.getContext().getResources().getCompatibilityInfo()).applicationScale;
      Rect rect = this.mTempRect;
      rect.set(param1Int1, param1Int2, param1Int3, param1Int4);
      rect.scale(f);
      accessibilityNodeInfo.setBoundsInParent(rect);
      accessibilityNodeInfo.setVisibleToUser(NumberPicker.this.isVisibleToUser());
      int[] arrayOfInt = this.mTempArray;
      NumberPicker.this.getLocationOnScreen(arrayOfInt);
      rect.offset(arrayOfInt[0], arrayOfInt[1]);
      rect.scale(f);
      accessibilityNodeInfo.setBoundsInScreen(rect);
      if (this.mAccessibilityFocusedView != -1)
        accessibilityNodeInfo.addAction(64); 
      if (this.mAccessibilityFocusedView == -1)
        accessibilityNodeInfo.addAction(128); 
      if (NumberPicker.this.isEnabled()) {
        if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue()) {
          accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
          accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN);
        } 
        if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue()) {
          accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
          accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP);
        } 
      } 
      return accessibilityNodeInfo;
    }
    
    private boolean hasVirtualDecrementButton() {
      return (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue());
    }
    
    private boolean hasVirtualIncrementButton() {
      return (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue());
    }
    
    private String getVirtualDecrementButtonText() {
      int i = NumberPicker.this.mValue - 1;
      int j = i;
      if (NumberPicker.this.mWrapSelectorWheel)
        j = NumberPicker.this.getWrappedSelectorIndex(i); 
      if (j >= NumberPicker.this.mMinValue) {
        String str;
        if (NumberPicker.this.mDisplayedValues == null) {
          str = NumberPicker.this.formatNumber(j);
        } else {
          str = NumberPicker.this.mDisplayedValues[j - NumberPicker.this.mMinValue];
        } 
        return str;
      } 
      return null;
    }
    
    private String getVirtualIncrementButtonText() {
      int i = NumberPicker.this.mValue + 1;
      int j = i;
      if (NumberPicker.this.mWrapSelectorWheel)
        j = NumberPicker.this.getWrappedSelectorIndex(i); 
      if (j <= NumberPicker.this.mMaxValue) {
        String str;
        if (NumberPicker.this.mDisplayedValues == null) {
          str = NumberPicker.this.formatNumber(j);
        } else {
          str = NumberPicker.this.mDisplayedValues[j - NumberPicker.this.mMinValue];
        } 
        return str;
      } 
      return null;
    }
  }
  
  private static String formatNumberWithLocale(int paramInt) {
    return String.format(Locale.getDefault(), "%d", new Object[] { Integer.valueOf(paramInt) });
  }
  
  class Formatter {
    public abstract String format(int param1Int);
  }
  
  class OnScrollListener {
    public static final int SCROLL_STATE_FLING = 2;
    
    public static final int SCROLL_STATE_IDLE = 0;
    
    public static final int SCROLL_STATE_TOUCH_SCROLL = 1;
    
    public abstract void onScrollStateChange(NumberPicker param1NumberPicker, int param1Int);
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface ScrollState {}
  }
  
  class OnValueChangeListener {
    public abstract void onValueChange(NumberPicker param1NumberPicker, int param1Int1, int param1Int2);
  }
}
