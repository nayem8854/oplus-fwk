package android.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Cea708CaptionRenderer;
import android.media.ClosedCaptionRenderer;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.media.SubtitleController;
import android.media.SubtitleTrack;
import android.media.TtmlRenderer;
import android.media.WebVttRenderer;
import android.net.Uri;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Vector;

public class VideoView extends SurfaceView implements MediaController.MediaPlayerControl, SubtitleController.Anchor {
  private final Vector<Pair<InputStream, MediaFormat>> mPendingSubtitleTracks = new Vector<>();
  
  private int mCurrentState = 0;
  
  private int mTargetState = 0;
  
  private SurfaceHolder mSurfaceHolder = null;
  
  private MediaPlayer mMediaPlayer = null;
  
  private int mAudioFocusType = 1;
  
  private static final int STATE_ERROR = -1;
  
  private static final int STATE_IDLE = 0;
  
  private static final int STATE_PAUSED = 4;
  
  private static final int STATE_PLAYBACK_COMPLETED = 5;
  
  private static final int STATE_PLAYING = 3;
  
  private static final int STATE_PREPARED = 2;
  
  private static final int STATE_PREPARING = 1;
  
  private static final String TAG = "VideoView";
  
  private AudioAttributes mAudioAttributes;
  
  private AudioManager mAudioManager;
  
  private int mAudioSession;
  
  private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener;
  
  private boolean mCanPause;
  
  private boolean mCanSeekBack;
  
  private boolean mCanSeekForward;
  
  private MediaPlayer.OnCompletionListener mCompletionListener;
  
  private int mCurrentBufferPercentage;
  
  private MediaPlayer.OnErrorListener mErrorListener;
  
  private Map<String, String> mHeaders;
  
  private MediaPlayer.OnInfoListener mInfoListener;
  
  private MediaController mMediaController;
  
  private MediaPlayer.OnCompletionListener mOnCompletionListener;
  
  private MediaPlayer.OnErrorListener mOnErrorListener;
  
  private MediaPlayer.OnInfoListener mOnInfoListener;
  
  private MediaPlayer.OnPreparedListener mOnPreparedListener;
  
  MediaPlayer.OnPreparedListener mPreparedListener;
  
  SurfaceHolder.Callback mSHCallback;
  
  private int mSeekWhenPrepared;
  
  MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener;
  
  private SubtitleTrack.RenderingWidget mSubtitleWidget;
  
  private SubtitleTrack.RenderingWidget.OnChangedListener mSubtitlesChangedListener;
  
  private int mSurfaceHeight;
  
  private int mSurfaceWidth;
  
  private Uri mUri;
  
  private int mVideoHeight;
  
  private int mVideoWidth;
  
  public VideoView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public VideoView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public VideoView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public VideoView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mSizeChangedListener = (MediaPlayer.OnVideoSizeChangedListener)new Object(this);
    this.mPreparedListener = (MediaPlayer.OnPreparedListener)new Object(this);
    this.mCompletionListener = (MediaPlayer.OnCompletionListener)new Object(this);
    this.mInfoListener = (MediaPlayer.OnInfoListener)new Object(this);
    this.mErrorListener = (MediaPlayer.OnErrorListener)new Object(this);
    this.mBufferingUpdateListener = (MediaPlayer.OnBufferingUpdateListener)new Object(this);
    this.mSHCallback = (SurfaceHolder.Callback)new Object(this);
    this.mVideoWidth = 0;
    this.mVideoHeight = 0;
    this.mAudioManager = (AudioManager)this.mContext.getSystemService("audio");
    AudioAttributes.Builder builder = (new AudioAttributes.Builder()).setUsage(1);
    this.mAudioAttributes = builder.setContentType(3).build();
    getHolder().addCallback(this.mSHCallback);
    getHolder().setType(3);
    setFocusable(true);
    setFocusableInTouchMode(true);
    requestFocus();
    this.mCurrentState = 0;
    this.mTargetState = 0;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = getDefaultSize(this.mVideoWidth, paramInt1);
    int j = getDefaultSize(this.mVideoHeight, paramInt2);
    int k = i, m = j;
    if (this.mVideoWidth > 0) {
      k = i;
      m = j;
      if (this.mVideoHeight > 0) {
        int n = View.MeasureSpec.getMode(paramInt1);
        paramInt1 = View.MeasureSpec.getSize(paramInt1);
        int i1 = View.MeasureSpec.getMode(paramInt2);
        paramInt2 = View.MeasureSpec.getSize(paramInt2);
        if (n == 1073741824 && i1 == 1073741824) {
          j = this.mVideoWidth;
          i = this.mVideoHeight;
          if (j * paramInt2 < paramInt1 * i) {
            k = j * paramInt2 / i;
            m = paramInt2;
          } else {
            k = paramInt1;
            m = paramInt2;
            if (j * paramInt2 > paramInt1 * i) {
              m = i * paramInt1 / j;
              k = paramInt1;
            } 
          } 
        } else if (n == 1073741824) {
          j = this.mVideoHeight * paramInt1 / this.mVideoWidth;
          k = paramInt1;
          m = j;
          if (i1 == Integer.MIN_VALUE) {
            k = paramInt1;
            m = j;
            if (j > paramInt2) {
              k = paramInt1;
              m = paramInt2;
            } 
          } 
        } else if (i1 == 1073741824) {
          j = this.mVideoWidth * paramInt2 / this.mVideoHeight;
          k = j;
          m = paramInt2;
          if (n == Integer.MIN_VALUE) {
            k = j;
            m = paramInt2;
            if (j > paramInt1) {
              k = paramInt1;
              m = paramInt2;
            } 
          } 
        } else {
          k = this.mVideoWidth;
          m = this.mVideoHeight;
          i = k;
          j = m;
          if (i1 == Integer.MIN_VALUE) {
            i = k;
            j = m;
            if (m > paramInt2) {
              i = this.mVideoWidth * paramInt2 / this.mVideoHeight;
              j = paramInt2;
            } 
          } 
          k = i;
          m = j;
          if (n == Integer.MIN_VALUE) {
            k = i;
            m = j;
            if (i > paramInt1) {
              m = this.mVideoHeight * paramInt1 / this.mVideoWidth;
              k = paramInt1;
            } 
          } 
        } 
      } 
    } 
    setMeasuredDimension(k, m);
  }
  
  public CharSequence getAccessibilityClassName() {
    return VideoView.class.getName();
  }
  
  public int resolveAdjustedSize(int paramInt1, int paramInt2) {
    return getDefaultSize(paramInt1, paramInt2);
  }
  
  public void setVideoPath(String paramString) {
    setVideoURI(Uri.parse(paramString));
  }
  
  public void setVideoURI(Uri paramUri) {
    setVideoURI(paramUri, (Map<String, String>)null);
  }
  
  public void setVideoURI(Uri paramUri, Map<String, String> paramMap) {
    this.mUri = paramUri;
    this.mHeaders = paramMap;
    this.mSeekWhenPrepared = 0;
    openVideo();
    requestLayout();
    invalidate();
  }
  
  public void setAudioFocusRequest(int paramInt) {
    if (paramInt == 0 || paramInt == 1 || paramInt == 2 || paramInt == 3 || paramInt == 4) {
      this.mAudioFocusType = paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal audio focus type ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setAudioAttributes(AudioAttributes paramAudioAttributes) {
    if (paramAudioAttributes != null) {
      this.mAudioAttributes = paramAudioAttributes;
      return;
    } 
    throw new IllegalArgumentException("Illegal null AudioAttributes");
  }
  
  public void addSubtitleSource(InputStream paramInputStream, MediaFormat paramMediaFormat) {
    MediaPlayer mediaPlayer = this.mMediaPlayer;
    if (mediaPlayer == null) {
      this.mPendingSubtitleTracks.add(Pair.create(paramInputStream, paramMediaFormat));
    } else {
      try {
        mediaPlayer.addSubtitleSource(paramInputStream, paramMediaFormat);
      } catch (IllegalStateException illegalStateException) {
        this.mInfoListener.onInfo(this.mMediaPlayer, 901, 0);
      } 
    } 
  }
  
  public void stopPlayback() {
    MediaPlayer mediaPlayer = this.mMediaPlayer;
    if (mediaPlayer != null) {
      mediaPlayer.stop();
      this.mMediaPlayer.release();
      this.mMediaPlayer = null;
      this.mCurrentState = 0;
      this.mTargetState = 0;
      this.mAudioManager.abandonAudioFocus(null);
    } 
  }
  
  private void openVideo() {
    Exception exception;
    if (this.mUri == null || this.mSurfaceHolder == null)
      return; 
    release(false);
    int i = this.mAudioFocusType;
    if (i != 0)
      this.mAudioManager.requestAudioFocus(null, this.mAudioAttributes, i, 0); 
    try {
      MediaPlayer mediaPlayer1 = new MediaPlayer();
      this();
      this.mMediaPlayer = mediaPlayer1;
      Context context = getContext();
      SubtitleController subtitleController = new SubtitleController();
      MediaPlayer mediaPlayer2 = this.mMediaPlayer;
      this(context, mediaPlayer2.getMediaTimeProvider(), (SubtitleController.Listener)this.mMediaPlayer);
      WebVttRenderer webVttRenderer = new WebVttRenderer();
      this(context);
      subtitleController.registerRenderer((SubtitleController.Renderer)webVttRenderer);
      TtmlRenderer ttmlRenderer = new TtmlRenderer();
      this(context);
      subtitleController.registerRenderer((SubtitleController.Renderer)ttmlRenderer);
      Cea708CaptionRenderer cea708CaptionRenderer = new Cea708CaptionRenderer();
      this(context);
      subtitleController.registerRenderer((SubtitleController.Renderer)cea708CaptionRenderer);
      ClosedCaptionRenderer closedCaptionRenderer = new ClosedCaptionRenderer();
      this(context);
      subtitleController.registerRenderer((SubtitleController.Renderer)closedCaptionRenderer);
      this.mMediaPlayer.setSubtitleAnchor(subtitleController, this);
      if (this.mAudioSession != 0) {
        this.mMediaPlayer.setAudioSessionId(this.mAudioSession);
      } else {
        this.mAudioSession = this.mMediaPlayer.getAudioSessionId();
      } 
      this.mMediaPlayer.setOnPreparedListener(this.mPreparedListener);
      this.mMediaPlayer.setOnVideoSizeChangedListener(this.mSizeChangedListener);
      this.mMediaPlayer.setOnCompletionListener(this.mCompletionListener);
      this.mMediaPlayer.setOnErrorListener(this.mErrorListener);
      this.mMediaPlayer.setOnInfoListener(this.mInfoListener);
      this.mMediaPlayer.setOnBufferingUpdateListener(this.mBufferingUpdateListener);
      this.mCurrentBufferPercentage = 0;
      this.mMediaPlayer.setDataSource(this.mContext, this.mUri, this.mHeaders);
      this.mMediaPlayer.setDisplay(this.mSurfaceHolder);
      this.mMediaPlayer.setAudioAttributes(this.mAudioAttributes);
      this.mMediaPlayer.setScreenOnWhilePlaying(true);
      this.mMediaPlayer.prepareAsync();
      for (Pair<InputStream, MediaFormat> pair : this.mPendingSubtitleTracks) {
        try {
          this.mMediaPlayer.addSubtitleSource((InputStream)pair.first, (MediaFormat)pair.second);
        } catch (IllegalStateException illegalStateException) {
          this.mInfoListener.onInfo(this.mMediaPlayer, 901, 0);
        } 
      } 
      this.mCurrentState = 1;
      attachMediaController();
      this.mPendingSubtitleTracks.clear();
      return;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Unable to open content: ");
      stringBuilder.append(this.mUri);
      Log.w("VideoView", stringBuilder.toString(), iOException);
      this.mCurrentState = -1;
      this.mTargetState = -1;
      this.mErrorListener.onError(this.mMediaPlayer, 1, 0);
      this.mPendingSubtitleTracks.clear();
      return;
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Unable to open content: ");
      stringBuilder.append(this.mUri);
      Log.w("VideoView", stringBuilder.toString(), illegalArgumentException);
      this.mCurrentState = -1;
      this.mTargetState = -1;
      this.mErrorListener.onError(this.mMediaPlayer, 1, 0);
      this.mPendingSubtitleTracks.clear();
      return;
    } finally {}
    this.mPendingSubtitleTracks.clear();
    throw exception;
  }
  
  public void setMediaController(MediaController paramMediaController) {
    MediaController mediaController = this.mMediaController;
    if (mediaController != null)
      mediaController.hide(); 
    this.mMediaController = paramMediaController;
    attachMediaController();
  }
  
  private void attachMediaController() {
    if (this.mMediaPlayer != null) {
      MediaController mediaController = this.mMediaController;
      if (mediaController != null) {
        View view;
        mediaController.setMediaPlayer(this);
        if (getParent() instanceof View) {
          view = (View)getParent();
        } else {
          view = this;
        } 
        this.mMediaController.setAnchorView(view);
        this.mMediaController.setEnabled(isInPlaybackState());
      } 
    } 
  }
  
  public void setOnPreparedListener(MediaPlayer.OnPreparedListener paramOnPreparedListener) {
    this.mOnPreparedListener = paramOnPreparedListener;
  }
  
  public void setOnCompletionListener(MediaPlayer.OnCompletionListener paramOnCompletionListener) {
    this.mOnCompletionListener = paramOnCompletionListener;
  }
  
  public void setOnErrorListener(MediaPlayer.OnErrorListener paramOnErrorListener) {
    this.mOnErrorListener = paramOnErrorListener;
  }
  
  public void setOnInfoListener(MediaPlayer.OnInfoListener paramOnInfoListener) {
    this.mOnInfoListener = paramOnInfoListener;
  }
  
  private void release(boolean paramBoolean) {
    MediaPlayer mediaPlayer = this.mMediaPlayer;
    if (mediaPlayer != null) {
      mediaPlayer.reset();
      this.mMediaPlayer.release();
      this.mMediaPlayer = null;
      this.mPendingSubtitleTracks.clear();
      this.mCurrentState = 0;
      if (paramBoolean)
        this.mTargetState = 0; 
      if (this.mAudioFocusType != 0)
        this.mAudioManager.abandonAudioFocus(null); 
    } 
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.getAction() == 0 && 
      isInPlaybackState() && this.mMediaController != null)
      toggleMediaControlsVisiblity(); 
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.getAction() == 0 && 
      isInPlaybackState() && this.mMediaController != null)
      toggleMediaControlsVisiblity(); 
    return super.onTrackballEvent(paramMotionEvent);
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool;
    if (paramInt != 4 && paramInt != 24 && paramInt != 25 && paramInt != 164 && paramInt != 82 && paramInt != 5 && paramInt != 6) {
      bool = true;
    } else {
      bool = false;
    } 
    if (isInPlaybackState() && bool && this.mMediaController != null) {
      if (paramInt == 79 || paramInt == 85) {
        if (this.mMediaPlayer.isPlaying()) {
          pause();
          this.mMediaController.show();
        } else {
          start();
          this.mMediaController.hide();
        } 
        return true;
      } 
      if (paramInt == 126) {
        if (!this.mMediaPlayer.isPlaying()) {
          start();
          this.mMediaController.hide();
        } 
        return true;
      } 
      if (paramInt == 86 || paramInt == 127) {
        if (this.mMediaPlayer.isPlaying()) {
          pause();
          this.mMediaController.show();
        } 
        return true;
      } 
      toggleMediaControlsVisiblity();
    } 
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  private void toggleMediaControlsVisiblity() {
    if (this.mMediaController.isShowing()) {
      this.mMediaController.hide();
    } else {
      this.mMediaController.show();
    } 
  }
  
  public void start() {
    if (isInPlaybackState()) {
      this.mMediaPlayer.start();
      this.mCurrentState = 3;
    } 
    this.mTargetState = 3;
  }
  
  public void pause() {
    if (isInPlaybackState() && 
      this.mMediaPlayer.isPlaying()) {
      this.mMediaPlayer.pause();
      this.mCurrentState = 4;
    } 
    this.mTargetState = 4;
  }
  
  public void suspend() {
    release(false);
  }
  
  public void resume() {
    openVideo();
  }
  
  public int getDuration() {
    if (isInPlaybackState())
      return this.mMediaPlayer.getDuration(); 
    return -1;
  }
  
  public int getCurrentPosition() {
    if (isInPlaybackState())
      return this.mMediaPlayer.getCurrentPosition(); 
    return 0;
  }
  
  public void seekTo(int paramInt) {
    if (isInPlaybackState()) {
      this.mMediaPlayer.seekTo(paramInt);
      this.mSeekWhenPrepared = 0;
    } else {
      this.mSeekWhenPrepared = paramInt;
    } 
  }
  
  public boolean isPlaying() {
    boolean bool;
    if (isInPlaybackState() && this.mMediaPlayer.isPlaying()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getBufferPercentage() {
    if (this.mMediaPlayer != null)
      return this.mCurrentBufferPercentage; 
    return 0;
  }
  
  private boolean isInPlaybackState() {
    MediaPlayer mediaPlayer = this.mMediaPlayer;
    null = true;
    if (mediaPlayer != null) {
      int i = this.mCurrentState;
      if (i != -1 && i != 0 && i != 1)
        return null; 
    } 
    return false;
  }
  
  public boolean canPause() {
    return this.mCanPause;
  }
  
  public boolean canSeekBackward() {
    return this.mCanSeekBack;
  }
  
  public boolean canSeekForward() {
    return this.mCanSeekForward;
  }
  
  public int getAudioSessionId() {
    if (this.mAudioSession == 0) {
      MediaPlayer mediaPlayer = new MediaPlayer();
      this.mAudioSession = mediaPlayer.getAudioSessionId();
      mediaPlayer.release();
    } 
    return this.mAudioSession;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    SubtitleTrack.RenderingWidget renderingWidget = this.mSubtitleWidget;
    if (renderingWidget != null)
      renderingWidget.onAttachedToWindow(); 
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    SubtitleTrack.RenderingWidget renderingWidget = this.mSubtitleWidget;
    if (renderingWidget != null)
      renderingWidget.onDetachedFromWindow(); 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (this.mSubtitleWidget != null)
      measureAndLayoutSubtitleWidget(); 
  }
  
  public void draw(Canvas paramCanvas) {
    super.draw(paramCanvas);
    if (this.mSubtitleWidget != null) {
      int i = paramCanvas.save();
      paramCanvas.translate(getPaddingLeft(), getPaddingTop());
      this.mSubtitleWidget.draw(paramCanvas);
      paramCanvas.restoreToCount(i);
    } 
  }
  
  private void measureAndLayoutSubtitleWidget() {
    int i = getWidth(), j = getPaddingLeft(), k = getPaddingRight();
    int m = getHeight(), n = getPaddingTop(), i1 = getPaddingBottom();
    this.mSubtitleWidget.setSize(i - j - k, m - n - i1);
  }
  
  public void setSubtitleWidget(SubtitleTrack.RenderingWidget paramRenderingWidget) {
    if (this.mSubtitleWidget == paramRenderingWidget)
      return; 
    boolean bool = isAttachedToWindow();
    SubtitleTrack.RenderingWidget renderingWidget = this.mSubtitleWidget;
    if (renderingWidget != null) {
      if (bool)
        renderingWidget.onDetachedFromWindow(); 
      this.mSubtitleWidget.setOnChangedListener(null);
    } 
    this.mSubtitleWidget = paramRenderingWidget;
    if (paramRenderingWidget != null) {
      if (this.mSubtitlesChangedListener == null)
        this.mSubtitlesChangedListener = (SubtitleTrack.RenderingWidget.OnChangedListener)new Object(this); 
      setWillNotDraw(false);
      paramRenderingWidget.setOnChangedListener(this.mSubtitlesChangedListener);
      if (bool) {
        paramRenderingWidget.onAttachedToWindow();
        requestLayout();
      } 
    } else {
      setWillNotDraw(true);
    } 
    invalidate();
  }
  
  public Looper getSubtitleLooper() {
    return Looper.getMainLooper();
  }
}
