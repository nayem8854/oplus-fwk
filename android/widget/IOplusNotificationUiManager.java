package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusNotificationUiManager extends IOplusCommonFeature {
  public static final IOplusNotificationUiManager DEFAULT = (IOplusNotificationUiManager)new Object();
  
  public static final String NAME = "INotificationUiManager";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusNotificationUiManager;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default int getConversationType(int paramInt) {
    return paramInt;
  }
}
