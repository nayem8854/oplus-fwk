package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.view.View;

public interface IOplusDragTextShadowHelper extends IOplusCommonFeature {
  public static final IOplusDragTextShadowHelper DEFAULT = (IOplusDragTextShadowHelper)new Object();
  
  default IOplusDragTextShadowHelper getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusDragTextShadowHelper;
  }
  
  default View.DragShadowBuilder getColorTextThumbnailBuilder(View paramView, String paramString) {
    return null;
  }
}
