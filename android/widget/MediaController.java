package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import com.android.internal.policy.PhoneWindow;
import java.util.Formatter;
import java.util.Locale;

public class MediaController extends FrameLayout {
  private static final int sDefaultTimeout = 3000;
  
  private final AccessibilityManager mAccessibilityManager;
  
  private View mAnchor;
  
  private final Context mContext;
  
  private TextView mCurrentTime;
  
  private View mDecor;
  
  private WindowManager.LayoutParams mDecorLayoutParams;
  
  private boolean mDragging;
  
  private TextView mEndTime;
  
  private final Runnable mFadeOut;
  
  private ImageButton mFfwdButton;
  
  private final View.OnClickListener mFfwdListener;
  
  StringBuilder mFormatBuilder;
  
  Formatter mFormatter;
  
  private boolean mFromXml;
  
  private final View.OnLayoutChangeListener mLayoutChangeListener;
  
  private boolean mListenersSet;
  
  private ImageButton mNextButton;
  
  private View.OnClickListener mNextListener;
  
  private ImageButton mPauseButton;
  
  private CharSequence mPauseDescription;
  
  private final View.OnClickListener mPauseListener;
  
  private CharSequence mPlayDescription;
  
  private MediaPlayerControl mPlayer;
  
  private ImageButton mPrevButton;
  
  private View.OnClickListener mPrevListener;
  
  private ProgressBar mProgress;
  
  private ImageButton mRewButton;
  
  private final View.OnClickListener mRewListener;
  
  private View mRoot;
  
  private final SeekBar.OnSeekBarChangeListener mSeekListener;
  
  private final Runnable mShowProgress;
  
  private boolean mShowing;
  
  private final View.OnTouchListener mTouchListener;
  
  private final boolean mUseFastForward;
  
  private Window mWindow;
  
  private WindowManager mWindowManager;
  
  public MediaController(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    this.mLayoutChangeListener = (View.OnLayoutChangeListener)new Object(this);
    this.mTouchListener = (View.OnTouchListener)new Object(this);
    this.mFadeOut = (Runnable)new Object(this);
    this.mShowProgress = (Runnable)new Object(this);
    this.mPauseListener = (View.OnClickListener)new Object(this);
    this.mSeekListener = (SeekBar.OnSeekBarChangeListener)new Object(this);
    this.mRewListener = (View.OnClickListener)new Object(this);
    this.mFfwdListener = (View.OnClickListener)new Object(this);
    this.mRoot = this;
    this.mContext = paramContext;
    this.mUseFastForward = true;
    this.mFromXml = true;
    this.mAccessibilityManager = AccessibilityManager.getInstance(paramContext);
  }
  
  public void onFinishInflate() {
    View view = this.mRoot;
    if (view != null)
      initControllerView(view); 
  }
  
  public MediaController(Context paramContext, boolean paramBoolean) {
    super(paramContext);
    this.mLayoutChangeListener = (View.OnLayoutChangeListener)new Object(this);
    this.mTouchListener = (View.OnTouchListener)new Object(this);
    this.mFadeOut = (Runnable)new Object(this);
    this.mShowProgress = (Runnable)new Object(this);
    this.mPauseListener = (View.OnClickListener)new Object(this);
    this.mSeekListener = (SeekBar.OnSeekBarChangeListener)new Object(this);
    this.mRewListener = (View.OnClickListener)new Object(this);
    this.mFfwdListener = (View.OnClickListener)new Object(this);
    this.mContext = paramContext;
    this.mUseFastForward = paramBoolean;
    initFloatingWindowLayout();
    initFloatingWindow();
    this.mAccessibilityManager = AccessibilityManager.getInstance(paramContext);
  }
  
  public MediaController(Context paramContext) {
    this(paramContext, true);
  }
  
  private void initFloatingWindow() {
    this.mWindowManager = (WindowManager)this.mContext.getSystemService("window");
    PhoneWindow phoneWindow = new PhoneWindow(this.mContext);
    phoneWindow.setWindowManager(this.mWindowManager, null, null);
    this.mWindow.requestFeature(1);
    View view = this.mWindow.getDecorView();
    view.setOnTouchListener(this.mTouchListener);
    this.mWindow.setContentView(this);
    this.mWindow.setBackgroundDrawableResource(17170445);
    this.mWindow.setVolumeControlStream(3);
    setFocusable(true);
    setFocusableInTouchMode(true);
    setDescendantFocusability(262144);
    requestFocus();
  }
  
  private void initFloatingWindowLayout() {
    this.mDecorLayoutParams = new WindowManager.LayoutParams();
    WindowManager.LayoutParams layoutParams = this.mDecorLayoutParams;
    layoutParams.gravity = 51;
    layoutParams.height = -2;
    layoutParams.x = 0;
    layoutParams.format = -3;
    layoutParams.type = 1000;
    layoutParams.flags |= 0x820020;
    layoutParams.token = null;
    layoutParams.windowAnimations = 0;
  }
  
  private void updateFloatingWindowLayout() {
    int[] arrayOfInt = new int[2];
    this.mAnchor.getLocationOnScreen(arrayOfInt);
    View view1 = this.mDecor;
    int i = View.MeasureSpec.makeMeasureSpec(this.mAnchor.getWidth(), -2147483648);
    View view2 = this.mAnchor;
    int j = View.MeasureSpec.makeMeasureSpec(view2.getHeight(), -2147483648);
    view1.measure(i, j);
    WindowManager.LayoutParams layoutParams = this.mDecorLayoutParams;
    layoutParams.width = this.mAnchor.getWidth();
    layoutParams.x = arrayOfInt[0] + (this.mAnchor.getWidth() - layoutParams.width) / 2;
    layoutParams.y = arrayOfInt[1] + this.mAnchor.getHeight() - this.mDecor.getMeasuredHeight();
  }
  
  public void setMediaPlayer(MediaPlayerControl paramMediaPlayerControl) {
    this.mPlayer = paramMediaPlayerControl;
    updatePausePlay();
  }
  
  public void setAnchorView(View paramView) {
    View view = this.mAnchor;
    if (view != null)
      view.removeOnLayoutChangeListener(this.mLayoutChangeListener); 
    this.mAnchor = paramView;
    if (paramView != null)
      paramView.addOnLayoutChangeListener(this.mLayoutChangeListener); 
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
    removeAllViews();
    view = makeControllerView();
    addView(view, layoutParams);
  }
  
  protected View makeControllerView() {
    LayoutInflater layoutInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
    View view = layoutInflater.inflate(17367193, (ViewGroup)null);
    initControllerView(view);
    return this.mRoot;
  }
  
  private void initControllerView(View paramView) {
    Resources resources = this.mContext.getResources();
    this.mPlayDescription = resources.getText(17040513);
    this.mPauseDescription = resources.getText(17040512);
    ImageButton imageButton = paramView.<ImageButton>findViewById(16909281);
    if (imageButton != null) {
      imageButton.requestFocus();
      this.mPauseButton.setOnClickListener(this.mPauseListener);
    } 
    this.mFfwdButton = imageButton = paramView.<ImageButton>findViewById(16908966);
    boolean bool = false;
    if (imageButton != null) {
      imageButton.setOnClickListener(this.mFfwdListener);
      if (!this.mFromXml) {
        byte b;
        imageButton = this.mFfwdButton;
        if (this.mUseFastForward) {
          b = 0;
        } else {
          b = 8;
        } 
        imageButton.setVisibility(b);
      } 
    } 
    this.mRewButton = imageButton = paramView.<ImageButton>findViewById(16909365);
    if (imageButton != null) {
      imageButton.setOnClickListener(this.mRewListener);
      if (!this.mFromXml) {
        byte b;
        imageButton = this.mRewButton;
        if (this.mUseFastForward) {
          b = bool;
        } else {
          b = 8;
        } 
        imageButton.setVisibility(b);
      } 
    } 
    this.mNextButton = imageButton = paramView.<ImageButton>findViewById(16909207);
    if (imageButton != null && !this.mFromXml && !this.mListenersSet)
      imageButton.setVisibility(8); 
    this.mPrevButton = imageButton = paramView.<ImageButton>findViewById(16909317);
    if (imageButton != null && !this.mFromXml && !this.mListenersSet)
      imageButton.setVisibility(8); 
    ProgressBar progressBar = paramView.<ProgressBar>findViewById(16909163);
    if (progressBar != null) {
      if (progressBar instanceof SeekBar) {
        progressBar = progressBar;
        progressBar.setOnSeekBarChangeListener(this.mSeekListener);
      } 
      this.mProgress.setMax(1000);
    } 
    this.mEndTime = paramView.<TextView>findViewById(16909536);
    this.mCurrentTime = paramView.<TextView>findViewById(16909539);
    this.mFormatBuilder = new StringBuilder();
    this.mFormatter = new Formatter(this.mFormatBuilder, Locale.getDefault());
    installPrevNextListeners();
  }
  
  public void show() {
    show(3000);
  }
  
  private void disableUnsupportedButtons() {
    try {
      if (this.mPauseButton != null && !this.mPlayer.canPause())
        this.mPauseButton.setEnabled(false); 
      if (this.mRewButton != null && !this.mPlayer.canSeekBackward())
        this.mRewButton.setEnabled(false); 
      if (this.mFfwdButton != null && !this.mPlayer.canSeekForward())
        this.mFfwdButton.setEnabled(false); 
      if (this.mProgress != null && !this.mPlayer.canSeekBackward() && !this.mPlayer.canSeekForward())
        this.mProgress.setEnabled(false); 
    } catch (IncompatibleClassChangeError incompatibleClassChangeError) {}
  }
  
  public void show(int paramInt) {
    if (!this.mShowing && this.mAnchor != null) {
      setProgress();
      ImageButton imageButton = this.mPauseButton;
      if (imageButton != null)
        imageButton.requestFocus(); 
      disableUnsupportedButtons();
      updateFloatingWindowLayout();
      this.mWindowManager.addView(this.mDecor, this.mDecorLayoutParams);
      this.mShowing = true;
    } 
    updatePausePlay();
    post(this.mShowProgress);
    if (paramInt != 0 && !this.mAccessibilityManager.isTouchExplorationEnabled()) {
      removeCallbacks(this.mFadeOut);
      postDelayed(this.mFadeOut, paramInt);
    } 
  }
  
  public boolean isShowing() {
    return this.mShowing;
  }
  
  public void hide() {
    if (this.mAnchor == null)
      return; 
    if (this.mShowing) {
      try {
        removeCallbacks(this.mShowProgress);
        this.mWindowManager.removeView(this.mDecor);
      } catch (IllegalArgumentException illegalArgumentException) {
        Log.w("MediaController", "already removed");
      } 
      this.mShowing = false;
    } 
  }
  
  private String stringForTime(int paramInt) {
    int i = paramInt / 1000;
    paramInt = i % 60;
    int j = i / 60 % 60;
    i /= 3600;
    this.mFormatBuilder.setLength(0);
    if (i > 0)
      return this.mFormatter.format("%d:%02d:%02d", new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(paramInt) }).toString(); 
    return this.mFormatter.format("%02d:%02d", new Object[] { Integer.valueOf(j), Integer.valueOf(paramInt) }).toString();
  }
  
  private int setProgress() {
    MediaPlayerControl mediaPlayerControl = this.mPlayer;
    if (mediaPlayerControl == null || this.mDragging)
      return 0; 
    int i = mediaPlayerControl.getCurrentPosition();
    int j = this.mPlayer.getDuration();
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null) {
      if (j > 0) {
        long l = i * 1000L / j;
        progressBar.setProgress((int)l);
      } 
      int k = this.mPlayer.getBufferPercentage();
      this.mProgress.setSecondaryProgress(k * 10);
    } 
    TextView textView = this.mEndTime;
    if (textView != null)
      textView.setText(stringForTime(j)); 
    textView = this.mCurrentTime;
    if (textView != null)
      textView.setText(stringForTime(i)); 
    return i;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    if (i != 0) {
      if (i != 1) {
        if (i == 3)
          hide(); 
      } else {
        show(3000);
      } 
    } else {
      show(0);
    } 
    return true;
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    show(3000);
    return false;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    ImageButton imageButton;
    boolean bool;
    int i = paramKeyEvent.getKeyCode();
    if (paramKeyEvent.getRepeatCount() == 0 && paramKeyEvent.getAction() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (i == 79 || i == 85 || i == 62) {
      if (bool) {
        doPauseResume();
        show(3000);
        imageButton = this.mPauseButton;
        if (imageButton != null)
          imageButton.requestFocus(); 
      } 
      return true;
    } 
    if (i == 126) {
      if (bool && !this.mPlayer.isPlaying()) {
        this.mPlayer.start();
        updatePausePlay();
        show(3000);
      } 
      return true;
    } 
    if (i == 86 || i == 127) {
      if (bool && this.mPlayer.isPlaying()) {
        this.mPlayer.pause();
        updatePausePlay();
        show(3000);
      } 
      return true;
    } 
    if (i == 25 || i == 24 || i == 164 || i == 27)
      return super.dispatchKeyEvent((KeyEvent)imageButton); 
    if (i == 4 || i == 82) {
      if (bool)
        hide(); 
      return true;
    } 
    show(3000);
    return super.dispatchKeyEvent((KeyEvent)imageButton);
  }
  
  private void updatePausePlay() {
    if (this.mRoot == null || this.mPauseButton == null)
      return; 
    if (this.mPlayer.isPlaying()) {
      this.mPauseButton.setImageResource(17301539);
      this.mPauseButton.setContentDescription(this.mPauseDescription);
    } else {
      this.mPauseButton.setImageResource(17301540);
      this.mPauseButton.setContentDescription(this.mPlayDescription);
    } 
  }
  
  private void doPauseResume() {
    if (this.mPlayer.isPlaying()) {
      this.mPlayer.pause();
    } else {
      this.mPlayer.start();
    } 
    updatePausePlay();
  }
  
  public void setEnabled(boolean paramBoolean) {
    ImageButton imageButton = this.mPauseButton;
    if (imageButton != null)
      imageButton.setEnabled(paramBoolean); 
    imageButton = this.mFfwdButton;
    if (imageButton != null)
      imageButton.setEnabled(paramBoolean); 
    imageButton = this.mRewButton;
    if (imageButton != null)
      imageButton.setEnabled(paramBoolean); 
    imageButton = this.mNextButton;
    boolean bool = true;
    if (imageButton != null) {
      boolean bool1;
      if (paramBoolean && this.mNextListener != null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      imageButton.setEnabled(bool1);
    } 
    imageButton = this.mPrevButton;
    if (imageButton != null) {
      boolean bool1;
      if (paramBoolean && this.mPrevListener != null) {
        bool1 = bool;
      } else {
        bool1 = false;
      } 
      imageButton.setEnabled(bool1);
    } 
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null)
      progressBar.setEnabled(paramBoolean); 
    disableUnsupportedButtons();
    super.setEnabled(paramBoolean);
  }
  
  public CharSequence getAccessibilityClassName() {
    return MediaController.class.getName();
  }
  
  private void installPrevNextListeners() {
    ImageButton imageButton = this.mNextButton;
    boolean bool = true;
    if (imageButton != null) {
      boolean bool1;
      imageButton.setOnClickListener(this.mNextListener);
      imageButton = this.mNextButton;
      if (this.mNextListener != null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      imageButton.setEnabled(bool1);
    } 
    imageButton = this.mPrevButton;
    if (imageButton != null) {
      boolean bool1;
      imageButton.setOnClickListener(this.mPrevListener);
      imageButton = this.mPrevButton;
      if (this.mPrevListener != null) {
        bool1 = bool;
      } else {
        bool1 = false;
      } 
      imageButton.setEnabled(bool1);
    } 
  }
  
  public void setPrevNextListeners(View.OnClickListener paramOnClickListener1, View.OnClickListener paramOnClickListener2) {
    this.mNextListener = paramOnClickListener1;
    this.mPrevListener = paramOnClickListener2;
    this.mListenersSet = true;
    if (this.mRoot != null) {
      installPrevNextListeners();
      ImageButton imageButton = this.mNextButton;
      if (imageButton != null && !this.mFromXml)
        imageButton.setVisibility(0); 
      imageButton = this.mPrevButton;
      if (imageButton != null && !this.mFromXml)
        imageButton.setVisibility(0); 
    } 
  }
  
  class MediaPlayerControl {
    public abstract boolean canPause();
    
    public abstract boolean canSeekBackward();
    
    public abstract boolean canSeekForward();
    
    public abstract int getAudioSessionId();
    
    public abstract int getBufferPercentage();
    
    public abstract int getCurrentPosition();
    
    public abstract int getDuration();
    
    public abstract boolean isPlaying();
    
    public abstract void pause();
    
    public abstract void seekTo(int param1Int);
    
    public abstract void start();
  }
}
