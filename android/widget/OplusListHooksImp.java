package android.widget;

import android.view.OplusBaseView;
import com.oplus.util.OplusTypeCastingHelper;

public class OplusListHooksImp implements IOplusListHooks {
  public FastScroller getFastScroller(AbsListView paramAbsListView, int paramInt) {
    OplusBaseView oplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, paramAbsListView);
    if (oplusBaseView.isOplusOSStyle())
      return new ColorFastScroller(paramAbsListView, paramInt); 
    return new FastScroller(paramAbsListView, paramInt);
  }
}
