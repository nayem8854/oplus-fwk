package android.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;

public class ImageSwitcher extends ViewSwitcher {
  public ImageSwitcher(Context paramContext) {
    super(paramContext);
  }
  
  public ImageSwitcher(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public void setImageResource(int paramInt) {
    ImageView imageView = (ImageView)getNextView();
    imageView.setImageResource(paramInt);
    showNext();
  }
  
  public void setImageURI(Uri paramUri) {
    ImageView imageView = (ImageView)getNextView();
    imageView.setImageURI(paramUri);
    showNext();
  }
  
  public void setImageDrawable(Drawable paramDrawable) {
    ImageView imageView = (ImageView)getNextView();
    imageView.setImageDrawable(paramDrawable);
    showNext();
  }
  
  public CharSequence getAccessibilityClassName() {
    return ImageSwitcher.class.getName();
  }
}
