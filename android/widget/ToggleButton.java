package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import com.android.internal.R;

public class ToggleButton extends CompoundButton {
  private static final int NO_ALPHA = 255;
  
  private float mDisabledAlpha;
  
  private Drawable mIndicatorDrawable;
  
  private CharSequence mTextOff;
  
  private CharSequence mTextOn;
  
  public ToggleButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ToggleButton, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ToggleButton, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mTextOn = typedArray.getText(1);
    this.mTextOff = typedArray.getText(2);
    this.mDisabledAlpha = typedArray.getFloat(0, 0.5F);
    syncTextState();
    setDefaultStateDescritption();
    typedArray.recycle();
  }
  
  public ToggleButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ToggleButton(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842827);
  }
  
  public ToggleButton(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void setChecked(boolean paramBoolean) {
    super.setChecked(paramBoolean);
    syncTextState();
  }
  
  private void syncTextState() {
    boolean bool = isChecked();
    if (bool) {
      CharSequence charSequence = this.mTextOn;
      if (charSequence != null) {
        setText(charSequence);
        return;
      } 
    } 
    if (!bool) {
      CharSequence charSequence = this.mTextOff;
      if (charSequence != null)
        setText(charSequence); 
    } 
  }
  
  public CharSequence getTextOn() {
    return this.mTextOn;
  }
  
  public void setTextOn(CharSequence paramCharSequence) {
    this.mTextOn = paramCharSequence;
    setDefaultStateDescritption();
  }
  
  public CharSequence getTextOff() {
    return this.mTextOff;
  }
  
  public void setTextOff(CharSequence paramCharSequence) {
    this.mTextOff = paramCharSequence;
    setDefaultStateDescritption();
  }
  
  public float getDisabledAlpha() {
    return this.mDisabledAlpha;
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    updateReferenceToIndicatorDrawable(getBackground());
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable) {
    super.setBackgroundDrawable(paramDrawable);
    updateReferenceToIndicatorDrawable(paramDrawable);
  }
  
  private void updateReferenceToIndicatorDrawable(Drawable paramDrawable) {
    if (paramDrawable instanceof LayerDrawable) {
      LayerDrawable layerDrawable = (LayerDrawable)paramDrawable;
      this.mIndicatorDrawable = layerDrawable.findDrawableByLayerId(16908311);
    } else {
      this.mIndicatorDrawable = null;
    } 
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = this.mIndicatorDrawable;
    if (drawable != null) {
      int i;
      if (isEnabled()) {
        i = 255;
      } else {
        i = (int)(this.mDisabledAlpha * 255.0F);
      } 
      drawable.setAlpha(i);
    } 
  }
  
  public CharSequence getAccessibilityClassName() {
    return ToggleButton.class.getName();
  }
  
  protected CharSequence getButtonStateDescription() {
    if (isChecked()) {
      CharSequence charSequence3 = this.mTextOn, charSequence4 = charSequence3;
      if (charSequence3 == null)
        charSequence4 = getResources().getString(17039803); 
      return charSequence4;
    } 
    CharSequence charSequence1 = this.mTextOff, charSequence2 = charSequence1;
    if (charSequence1 == null)
      charSequence2 = getResources().getString(17039802); 
    return charSequence2;
  }
}
