package android.widget;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.MaskFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.TableMaskFilter;
import android.graphics.Xfermode;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.RemotableViewMethod;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.LinearInterpolator;
import com.android.internal.R;
import java.lang.ref.WeakReference;

@RemoteView
public class StackView extends AdapterViewAnimator {
  private final String TAG = "StackView";
  
  private final Rect mTouchRect = new Rect();
  
  private int mYVelocity = 0;
  
  private int mSwipeGestureType = 0;
  
  private boolean mTransitionIsSetup = false;
  
  private boolean mClickFeedbackIsValid = false;
  
  private boolean mFirstLayoutHappened = false;
  
  private long mLastInteractionTime = 0L;
  
  private final Rect stackInvalidateRect = new Rect();
  
  private static final int DEFAULT_ANIMATION_DURATION = 400;
  
  private static final int FRAME_PADDING = 4;
  
  private static final int GESTURE_NONE = 0;
  
  private static final int GESTURE_SLIDE_DOWN = 2;
  
  private static final int GESTURE_SLIDE_UP = 1;
  
  private static final int INVALID_POINTER = -1;
  
  private static final int ITEMS_SLIDE_DOWN = 1;
  
  private static final int ITEMS_SLIDE_UP = 0;
  
  private static final int MINIMUM_ANIMATION_DURATION = 50;
  
  private static final int MIN_TIME_BETWEEN_INTERACTION_AND_AUTOADVANCE = 5000;
  
  private static final long MIN_TIME_BETWEEN_SCROLLS = 100L;
  
  private static final int NUM_ACTIVE_VIEWS = 5;
  
  private static final float PERSPECTIVE_SCALE_FACTOR = 0.0F;
  
  private static final float PERSPECTIVE_SHIFT_FACTOR_X = 0.1F;
  
  private static final float PERSPECTIVE_SHIFT_FACTOR_Y = 0.1F;
  
  private static final float SLIDE_UP_RATIO = 0.7F;
  
  private static final int STACK_RELAYOUT_DURATION = 100;
  
  private static final float SWIPE_THRESHOLD_RATIO = 0.2F;
  
  private static HolographicHelper sHolographicHelper;
  
  private int mActivePointerId;
  
  private int mClickColor;
  
  private ImageView mClickFeedback;
  
  private int mFramePadding;
  
  private ImageView mHighlight;
  
  private float mInitialX;
  
  private float mInitialY;
  
  private long mLastScrollTime;
  
  private int mMaximumVelocity;
  
  private float mNewPerspectiveShiftX;
  
  private float mNewPerspectiveShiftY;
  
  private float mPerspectiveShiftX;
  
  private float mPerspectiveShiftY;
  
  private int mResOutColor;
  
  private int mSlideAmount;
  
  private int mStackMode;
  
  private StackSlider mStackSlider;
  
  private int mSwipeThreshold;
  
  private int mTouchSlop;
  
  private VelocityTracker mVelocityTracker;
  
  public StackView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public StackView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843838);
  }
  
  public StackView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public StackView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.StackView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.StackView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mResOutColor = typedArray.getColor(1, 0);
    this.mClickColor = typedArray.getColor(0, 0);
    typedArray.recycle();
    initStackView();
  }
  
  private void initStackView() {
    configureViewAnimator(5, 1);
    setStaticTransformationsEnabled(true);
    ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    this.mActivePointerId = -1;
    ImageView imageView = new ImageView(getContext());
    imageView.setLayoutParams(new LayoutParams(imageView));
    imageView = this.mHighlight;
    addViewInLayout(imageView, -1, new LayoutParams(imageView));
    this.mClickFeedback = imageView = new ImageView(getContext());
    imageView.setLayoutParams(new LayoutParams(imageView));
    imageView = this.mClickFeedback;
    addViewInLayout(imageView, -1, new LayoutParams(imageView));
    this.mClickFeedback.setVisibility(4);
    this.mStackSlider = new StackSlider();
    if (sHolographicHelper == null)
      sHolographicHelper = new HolographicHelper(this.mContext); 
    setClipChildren(false);
    setClipToPadding(false);
    this.mStackMode = 1;
    this.mWhichChild = -1;
    float f = (this.mContext.getResources().getDisplayMetrics()).density;
    this.mFramePadding = (int)Math.ceil((4.0F * f));
  }
  
  void transformViewForTransition(int paramInt1, int paramInt2, View paramView, boolean paramBoolean) {
    if (!paramBoolean) {
      ((StackFrame)paramView).cancelSliderAnimator();
      paramView.setRotationX(0.0F);
      LayoutParams layoutParams = (LayoutParams)paramView.getLayoutParams();
      layoutParams.setVerticalOffset(0);
      layoutParams.setHorizontalOffset(0);
    } 
    if (paramInt1 == -1 && paramInt2 == getNumActiveViews() - 1) {
      transformViewAtIndex(paramInt2, paramView, false);
      paramView.setVisibility(0);
      paramView.setAlpha(1.0F);
    } else if (paramInt1 == 0 && paramInt2 == 1) {
      ObjectAnimator objectAnimator;
      ((StackFrame)paramView).cancelSliderAnimator();
      paramView.setVisibility(0);
      paramInt1 = Math.round(this.mStackSlider.getDurationForNeutralPosition(this.mYVelocity));
      StackSlider stackSlider = new StackSlider(this.mStackSlider);
      stackSlider.setView(paramView);
      if (paramBoolean) {
        PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofFloat("YProgress", new float[] { 0.0F });
        PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat("XProgress", new float[] { 0.0F });
        objectAnimator = ObjectAnimator.ofPropertyValuesHolder(stackSlider, new PropertyValuesHolder[] { propertyValuesHolder2, propertyValuesHolder1 });
        objectAnimator.setDuration(paramInt1);
        objectAnimator.setInterpolator(new LinearInterpolator());
        ((StackFrame)paramView).setSliderAnimator(objectAnimator);
        objectAnimator.start();
      } else {
        objectAnimator.setYProgress(0.0F);
        objectAnimator.setXProgress(0.0F);
      } 
    } else if (paramInt1 == 1 && paramInt2 == 0) {
      ((StackFrame)paramView).cancelSliderAnimator();
      paramInt1 = Math.round(this.mStackSlider.getDurationForOffscreenPosition(this.mYVelocity));
      StackSlider stackSlider = new StackSlider(this.mStackSlider);
      stackSlider.setView(paramView);
      if (paramBoolean) {
        PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofFloat("YProgress", new float[] { 1.0F });
        PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat("XProgress", new float[] { 0.0F });
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(stackSlider, new PropertyValuesHolder[] { propertyValuesHolder2, propertyValuesHolder1 });
        objectAnimator.setDuration(paramInt1);
        objectAnimator.setInterpolator(new LinearInterpolator());
        ((StackFrame)paramView).setSliderAnimator(objectAnimator);
        objectAnimator.start();
      } else {
        stackSlider.setYProgress(1.0F);
        stackSlider.setXProgress(0.0F);
      } 
    } else if (paramInt2 == 0) {
      paramView.setAlpha(0.0F);
      paramView.setVisibility(4);
    } else if ((paramInt1 == 0 || paramInt1 == 1) && paramInt2 > 1) {
      paramView.setVisibility(0);
      paramView.setAlpha(1.0F);
      paramView.setRotationX(0.0F);
      LayoutParams layoutParams = (LayoutParams)paramView.getLayoutParams();
      layoutParams.setVerticalOffset(0);
      layoutParams.setHorizontalOffset(0);
    } else if (paramInt1 == -1) {
      paramView.setAlpha(1.0F);
      paramView.setVisibility(0);
    } else if (paramInt2 == -1) {
      if (paramBoolean) {
        postDelayed((Runnable)new Object(this, paramView), 100L);
      } else {
        paramView.setAlpha(0.0F);
      } 
    } 
    if (paramInt2 != -1)
      transformViewAtIndex(paramInt2, paramView, paramBoolean); 
  }
  
  private void transformViewAtIndex(int paramInt, View paramView, boolean paramBoolean) {
    float f1 = this.mPerspectiveShiftY;
    float f2 = this.mPerspectiveShiftX;
    if (this.mStackMode == 1) {
      int i = this.mMaxNumActiveViews - paramInt - 1;
      paramInt = i;
      if (i == this.mMaxNumActiveViews - 1)
        paramInt = i - 1; 
    } else {
      int i = paramInt - 1;
      paramInt = i;
      if (i < 0)
        paramInt = i + 1; 
    } 
    float f3 = paramInt * 1.0F / (this.mMaxNumActiveViews - 2);
    float f4 = 1.0F - (1.0F - f3) * 0.0F;
    float f5 = getMeasuredHeight() * 0.9F / 2.0F;
    f5 = f3 * f1 + (f4 - 1.0F) * f5;
    f1 = getMeasuredWidth() * 0.9F / 2.0F;
    f2 = (1.0F - f3) * f2 + (1.0F - f4) * f1;
    if (paramView instanceof StackFrame)
      ((StackFrame)paramView).cancelTransformAnimator(); 
    if (paramBoolean) {
      PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofFloat("translationX", new float[] { f2 });
      PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat("translationY", new float[] { f5 });
      PropertyValuesHolder propertyValuesHolder3 = PropertyValuesHolder.ofFloat("scaleX", new float[] { f4 });
      PropertyValuesHolder propertyValuesHolder4 = PropertyValuesHolder.ofFloat("scaleY", new float[] { f4 });
      ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(paramView, new PropertyValuesHolder[] { propertyValuesHolder3, propertyValuesHolder4, propertyValuesHolder2, propertyValuesHolder1 });
      objectAnimator.setDuration(100L);
      if (paramView instanceof StackFrame)
        ((StackFrame)paramView).setTransformAnimator(objectAnimator); 
      objectAnimator.start();
    } else {
      paramView.setTranslationX(f2);
      paramView.setTranslationY(f5);
      paramView.setScaleX(f4);
      paramView.setScaleY(f4);
    } 
  }
  
  private void setupStackSlider(View paramView, int paramInt) {
    this.mStackSlider.setMode(paramInt);
    if (paramView != null) {
      this.mHighlight.setImageBitmap(sHolographicHelper.createResOutline(paramView, this.mResOutColor));
      this.mHighlight.setRotation(paramView.getRotation());
      this.mHighlight.setTranslationY(paramView.getTranslationY());
      this.mHighlight.setTranslationX(paramView.getTranslationX());
      this.mHighlight.bringToFront();
      paramView.bringToFront();
      this.mStackSlider.setView(paramView);
      paramView.setVisibility(0);
    } 
  }
  
  @RemotableViewMethod
  public void showNext() {
    if (this.mSwipeGestureType != 0)
      return; 
    if (!this.mTransitionIsSetup) {
      View view = getViewAtRelativeIndex(1);
      if (view != null) {
        setupStackSlider(view, 0);
        this.mStackSlider.setYProgress(0.0F);
        this.mStackSlider.setXProgress(0.0F);
      } 
    } 
    super.showNext();
  }
  
  @RemotableViewMethod
  public void showPrevious() {
    if (this.mSwipeGestureType != 0)
      return; 
    if (!this.mTransitionIsSetup) {
      View view = getViewAtRelativeIndex(0);
      if (view != null) {
        setupStackSlider(view, 0);
        this.mStackSlider.setYProgress(1.0F);
        this.mStackSlider.setXProgress(0.0F);
      } 
    } 
    super.showPrevious();
  }
  
  void showOnly(int paramInt, boolean paramBoolean) {
    super.showOnly(paramInt, paramBoolean);
    for (paramInt = this.mCurrentWindowEnd; paramInt >= this.mCurrentWindowStart; paramInt--) {
      int i = modulo(paramInt, getWindowSize());
      AdapterViewAnimator.ViewAndMetaData viewAndMetaData = this.mViewsMap.get(Integer.valueOf(i));
      if (viewAndMetaData != null) {
        View view = ((AdapterViewAnimator.ViewAndMetaData)this.mViewsMap.get(Integer.valueOf(i))).view;
        if (view != null)
          view.bringToFront(); 
      } 
    } 
    ImageView imageView = this.mHighlight;
    if (imageView != null)
      imageView.bringToFront(); 
    this.mTransitionIsSetup = false;
    this.mClickFeedbackIsValid = false;
  }
  
  void updateClickFeedback() {
    if (!this.mClickFeedbackIsValid) {
      View view = getViewAtRelativeIndex(1);
      if (view != null) {
        ImageView imageView = this.mClickFeedback;
        HolographicHelper holographicHelper = sHolographicHelper;
        int i = this.mClickColor;
        Bitmap bitmap = holographicHelper.createClickOutline(view, i);
        imageView.setImageBitmap(bitmap);
        this.mClickFeedback.setTranslationX(view.getTranslationX());
        this.mClickFeedback.setTranslationY(view.getTranslationY());
      } 
      this.mClickFeedbackIsValid = true;
    } 
  }
  
  void showTapFeedback(View paramView) {
    updateClickFeedback();
    this.mClickFeedback.setVisibility(0);
    this.mClickFeedback.bringToFront();
    invalidate();
  }
  
  void hideTapFeedback(View paramView) {
    this.mClickFeedback.setVisibility(4);
    invalidate();
  }
  
  private void updateChildTransforms() {
    for (byte b = 0; b < getNumActiveViews(); b++) {
      View view = getViewAtRelativeIndex(b);
      if (view != null)
        transformViewAtIndex(b, view, false); 
    } 
  }
  
  private static class StackFrame extends FrameLayout {
    WeakReference<ObjectAnimator> sliderAnimator;
    
    WeakReference<ObjectAnimator> transformAnimator;
    
    public StackFrame(Context param1Context) {
      super(param1Context);
    }
    
    void setTransformAnimator(ObjectAnimator param1ObjectAnimator) {
      this.transformAnimator = new WeakReference<>(param1ObjectAnimator);
    }
    
    void setSliderAnimator(ObjectAnimator param1ObjectAnimator) {
      this.sliderAnimator = new WeakReference<>(param1ObjectAnimator);
    }
    
    boolean cancelTransformAnimator() {
      WeakReference<ObjectAnimator> weakReference = this.transformAnimator;
      if (weakReference != null) {
        ObjectAnimator objectAnimator = weakReference.get();
        if (objectAnimator != null) {
          objectAnimator.cancel();
          return true;
        } 
      } 
      return false;
    }
    
    boolean cancelSliderAnimator() {
      WeakReference<ObjectAnimator> weakReference = this.sliderAnimator;
      if (weakReference != null) {
        ObjectAnimator objectAnimator = weakReference.get();
        if (objectAnimator != null) {
          objectAnimator.cancel();
          return true;
        } 
      } 
      return false;
    }
  }
  
  FrameLayout getFrameForChild() {
    StackFrame stackFrame = new StackFrame(this.mContext);
    int i = this.mFramePadding;
    stackFrame.setPadding(i, i, i, i);
    return stackFrame;
  }
  
  void applyTransformForChildAtIndex(View paramView, int paramInt) {}
  
  protected void dispatchDraw(Canvas paramCanvas) {
    boolean bool = false;
    paramCanvas.getClipBounds(this.stackInvalidateRect);
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      if ((layoutParams.horizontalOffset == 0 && layoutParams.verticalOffset == 0) || 
        view.getAlpha() == 0.0F || view.getVisibility() != 0)
        layoutParams.resetInvalidateRect(); 
      Rect rect = layoutParams.getInvalidateRect();
      if (!rect.isEmpty()) {
        bool = true;
        this.stackInvalidateRect.union(rect);
      } 
    } 
    if (bool) {
      paramCanvas.save();
      paramCanvas.clipRectUnion(this.stackInvalidateRect);
      super.dispatchDraw(paramCanvas);
      paramCanvas.restore();
    } else {
      super.dispatchDraw(paramCanvas);
    } 
  }
  
  private void onLayout() {
    if (!this.mFirstLayoutHappened) {
      this.mFirstLayoutHappened = true;
      updateChildTransforms();
    } 
    int i = Math.round(getMeasuredHeight() * 0.7F);
    if (this.mSlideAmount != i) {
      this.mSlideAmount = i;
      this.mSwipeThreshold = Math.round(i * 0.2F);
    } 
    if (Float.compare(this.mPerspectiveShiftY, this.mNewPerspectiveShiftY) == 0) {
      float f1 = this.mPerspectiveShiftX, f2 = this.mNewPerspectiveShiftX;
      if (Float.compare(f1, f2) != 0) {
        this.mPerspectiveShiftY = this.mNewPerspectiveShiftY;
        this.mPerspectiveShiftX = this.mNewPerspectiveShiftX;
        updateChildTransforms();
        return;
      } 
      return;
    } 
    this.mPerspectiveShiftY = this.mNewPerspectiveShiftY;
    this.mPerspectiveShiftX = this.mNewPerspectiveShiftX;
    updateChildTransforms();
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    if ((paramMotionEvent.getSource() & 0x2) != 0 && 
      paramMotionEvent.getAction() == 8) {
      float f = paramMotionEvent.getAxisValue(9);
      if (f < 0.0F) {
        pacedScroll(false);
        return true;
      } 
      if (f > 0.0F) {
        pacedScroll(true);
        return true;
      } 
    } 
    return super.onGenericMotionEvent(paramMotionEvent);
  }
  
  private void pacedScroll(boolean paramBoolean) {
    long l1 = System.currentTimeMillis(), l2 = this.mLastScrollTime;
    if (l1 - l2 > 100L) {
      if (paramBoolean) {
        showPrevious();
      } else {
        showNext();
      } 
      this.mLastScrollTime = System.currentTimeMillis();
    } 
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getAction : ()I
    //   4: istore_2
    //   5: iload_2
    //   6: sipush #255
    //   9: iand
    //   10: istore_2
    //   11: iconst_1
    //   12: istore_3
    //   13: iload_2
    //   14: ifeq -> 112
    //   17: iload_2
    //   18: iconst_1
    //   19: if_icmpeq -> 99
    //   22: iload_2
    //   23: iconst_2
    //   24: if_icmpeq -> 49
    //   27: iload_2
    //   28: iconst_3
    //   29: if_icmpeq -> 99
    //   32: iload_2
    //   33: bipush #6
    //   35: if_icmpeq -> 41
    //   38: goto -> 145
    //   41: aload_0
    //   42: aload_1
    //   43: invokespecial onSecondaryPointerUp : (Landroid/view/MotionEvent;)V
    //   46: goto -> 145
    //   49: aload_1
    //   50: aload_0
    //   51: getfield mActivePointerId : I
    //   54: invokevirtual findPointerIndex : (I)I
    //   57: istore_2
    //   58: iload_2
    //   59: iconst_m1
    //   60: if_icmpne -> 74
    //   63: ldc 'StackView'
    //   65: ldc_w 'Error: No data for our primary pointer.'
    //   68: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   71: pop
    //   72: iconst_0
    //   73: ireturn
    //   74: aload_1
    //   75: iload_2
    //   76: invokevirtual getY : (I)F
    //   79: fstore #4
    //   81: aload_0
    //   82: getfield mInitialY : F
    //   85: fstore #5
    //   87: aload_0
    //   88: fload #4
    //   90: fload #5
    //   92: fsub
    //   93: invokespecial beginGestureIfNeeded : (F)V
    //   96: goto -> 145
    //   99: aload_0
    //   100: iconst_m1
    //   101: putfield mActivePointerId : I
    //   104: aload_0
    //   105: iconst_0
    //   106: putfield mSwipeGestureType : I
    //   109: goto -> 145
    //   112: aload_0
    //   113: getfield mActivePointerId : I
    //   116: iconst_m1
    //   117: if_icmpne -> 145
    //   120: aload_0
    //   121: aload_1
    //   122: invokevirtual getX : ()F
    //   125: putfield mInitialX : F
    //   128: aload_0
    //   129: aload_1
    //   130: invokevirtual getY : ()F
    //   133: putfield mInitialY : F
    //   136: aload_0
    //   137: aload_1
    //   138: iconst_0
    //   139: invokevirtual getPointerId : (I)I
    //   142: putfield mActivePointerId : I
    //   145: aload_0
    //   146: getfield mSwipeGestureType : I
    //   149: ifeq -> 155
    //   152: goto -> 157
    //   155: iconst_0
    //   156: istore_3
    //   157: iload_3
    //   158: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #623	-> 0
    //   #624	-> 5
    //   #647	-> 41
    //   #648	-> 46
    //   #634	-> 49
    //   #635	-> 58
    //   #637	-> 63
    //   #638	-> 72
    //   #640	-> 74
    //   #641	-> 81
    //   #643	-> 87
    //   #644	-> 96
    //   #652	-> 99
    //   #653	-> 104
    //   #626	-> 112
    //   #627	-> 120
    //   #628	-> 128
    //   #629	-> 136
    //   #657	-> 145
  }
  
  private void beginGestureIfNeeded(float paramFloat) {
    if ((int)Math.abs(paramFloat) > this.mTouchSlop && this.mSwipeGestureType == 0) {
      byte b1, b2;
      int j;
      byte b3;
      boolean bool = true;
      if (paramFloat < 0.0F) {
        b1 = 1;
      } else {
        b1 = 2;
      } 
      cancelLongPress();
      requestDisallowInterceptTouchEvent(true);
      if (this.mAdapter == null)
        return; 
      int i = getCount();
      if (this.mStackMode == 0) {
        if (b1 == 2) {
          b2 = 0;
        } else {
          b2 = 1;
        } 
      } else if (b1 == 2) {
        b2 = 1;
      } else {
        b2 = 0;
      } 
      if (this.mLoopViews && i == 1 && ((this.mStackMode == 0 && b1 == 1) || (this.mStackMode == 1 && b1 == 2))) {
        j = 1;
      } else {
        j = 0;
      } 
      if (this.mLoopViews && i == 1 && ((this.mStackMode == 1 && b1 == 1) || (this.mStackMode == 0 && b1 == 2))) {
        b3 = 1;
      } else {
        b3 = 0;
      } 
      if (this.mLoopViews && !b3 && !j) {
        b3 = 0;
        j = b2;
        b2 = b3;
      } else if (this.mCurrentWindowStartUnbounded + b2 == -1 || b3 != 0) {
        j = b2 + 1;
        b2 = 1;
      } else if (this.mCurrentWindowStartUnbounded + b2 == i - 1 || j != 0) {
        b3 = 2;
        j = b2;
        b2 = b3;
      } else {
        b3 = 0;
        j = b2;
        b2 = b3;
      } 
      if (b2 != 0)
        bool = false; 
      this.mTransitionIsSetup = bool;
      View view = getViewAtRelativeIndex(j);
      if (view == null)
        return; 
      setupStackSlider(view, b2);
      this.mSwipeGestureType = b1;
      cancelHandleClick();
    } 
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    super.onTouchEvent(paramMotionEvent);
    int i = paramMotionEvent.getAction();
    int j = paramMotionEvent.findPointerIndex(this.mActivePointerId);
    if (j == -1) {
      Log.d("StackView", "Error: No data for our primary pointer.");
      return false;
    } 
    float f1 = paramMotionEvent.getY(j);
    float f2 = paramMotionEvent.getX(j);
    f1 -= this.mInitialY;
    float f3 = this.mInitialX;
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain(); 
    this.mVelocityTracker.addMovement(paramMotionEvent);
    j = i & 0xFF;
    if (j != 1) {
      if (j != 2) {
        if (j != 3) {
          if (j == 6)
            onSecondaryPointerUp(paramMotionEvent); 
        } else {
          this.mActivePointerId = -1;
          this.mSwipeGestureType = 0;
        } 
      } else {
        beginGestureIfNeeded(f1);
        i = this.mSlideAmount;
        f3 = (f2 - f3) / i * 1.0F;
        j = this.mSwipeGestureType;
        if (j == 2) {
          f1 = (f1 - this.mTouchSlop * 1.0F) / i * 1.0F;
          f2 = f1;
          if (this.mStackMode == 1)
            f2 = 1.0F - f1; 
          this.mStackSlider.setYProgress(1.0F - f2);
          this.mStackSlider.setXProgress(f3);
          return true;
        } 
        if (j == 1) {
          f1 = -(this.mTouchSlop * 1.0F + f1) / i * 1.0F;
          f2 = f1;
          if (this.mStackMode == 1)
            f2 = 1.0F - f1; 
          this.mStackSlider.setYProgress(f2);
          this.mStackSlider.setXProgress(f3);
          return true;
        } 
      } 
    } else {
      handlePointerUp(paramMotionEvent);
    } 
    return true;
  }
  
  private void onSecondaryPointerUp(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionIndex();
    int j = paramMotionEvent.getPointerId(i);
    if (j == this.mActivePointerId) {
      VelocityTracker velocityTracker;
      if (this.mSwipeGestureType == 2) {
        j = 0;
      } else {
        j = 1;
      } 
      View view = getViewAtRelativeIndex(j);
      if (view == null)
        return; 
      for (j = 0; j < paramMotionEvent.getPointerCount(); j++) {
        if (j != i) {
          float f1 = paramMotionEvent.getX(j);
          float f2 = paramMotionEvent.getY(j);
          this.mTouchRect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
          if (this.mTouchRect.contains(Math.round(f1), Math.round(f2))) {
            float f3 = paramMotionEvent.getX(i);
            float f4 = paramMotionEvent.getY(i);
            this.mInitialY += f2 - f4;
            this.mInitialX += f1 - f3;
            this.mActivePointerId = paramMotionEvent.getPointerId(j);
            velocityTracker = this.mVelocityTracker;
            if (velocityTracker != null)
              velocityTracker.clear(); 
            return;
          } 
        } 
      } 
      handlePointerUp((MotionEvent)velocityTracker);
    } 
  }
  
  private void handlePointerUp(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.findPointerIndex(this.mActivePointerId);
    float f = paramMotionEvent.getY(i);
    i = (int)(f - this.mInitialY);
    this.mLastInteractionTime = System.currentTimeMillis();
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null) {
      velocityTracker.computeCurrentVelocity(1000, this.mMaximumVelocity);
      this.mYVelocity = (int)this.mVelocityTracker.getYVelocity(this.mActivePointerId);
    } 
    velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null) {
      velocityTracker.recycle();
      this.mVelocityTracker = null;
    } 
    if (i > this.mSwipeThreshold && this.mSwipeGestureType == 2 && this.mStackSlider.mMode == 0) {
      this.mSwipeGestureType = 0;
      if (this.mStackMode == 0) {
        showPrevious();
      } else {
        showNext();
      } 
      this.mHighlight.bringToFront();
    } else if (i < -this.mSwipeThreshold && this.mSwipeGestureType == 1 && this.mStackSlider.mMode == 0) {
      this.mSwipeGestureType = 0;
      if (this.mStackMode == 0) {
        showNext();
      } else {
        showPrevious();
      } 
      this.mHighlight.bringToFront();
    } else {
      i = this.mSwipeGestureType;
      f = 1.0F;
      if (i == 1) {
        if (this.mStackMode != 1)
          f = 0.0F; 
        if (this.mStackMode == 0 || this.mStackSlider.mMode != 0) {
          i = Math.round(this.mStackSlider.getDurationForNeutralPosition());
        } else {
          i = Math.round(this.mStackSlider.getDurationForOffscreenPosition());
        } 
        StackSlider stackSlider = new StackSlider(this.mStackSlider);
        PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofFloat("YProgress", new float[] { f });
        PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat("XProgress", new float[] { 0.0F });
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(stackSlider, new PropertyValuesHolder[] { propertyValuesHolder2, propertyValuesHolder1 });
        objectAnimator.setDuration(i);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.start();
      } else if (i == 2) {
        if (this.mStackMode == 1)
          f = 0.0F; 
        if (this.mStackMode == 1 || this.mStackSlider.mMode != 0) {
          i = Math.round(this.mStackSlider.getDurationForNeutralPosition());
        } else {
          i = Math.round(this.mStackSlider.getDurationForOffscreenPosition());
        } 
        StackSlider stackSlider = new StackSlider(this.mStackSlider);
        PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofFloat("YProgress", new float[] { f });
        PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat("XProgress", new float[] { 0.0F });
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(stackSlider, new PropertyValuesHolder[] { propertyValuesHolder2, propertyValuesHolder1 });
        objectAnimator.setDuration(i);
        objectAnimator.start();
      } 
    } 
    this.mActivePointerId = -1;
    this.mSwipeGestureType = 0;
  }
  
  class StackSlider {
    static final int BEGINNING_OF_STACK_MODE = 1;
    
    static final int END_OF_STACK_MODE = 2;
    
    static final int NORMAL_MODE = 0;
    
    int mMode = 0;
    
    View mView;
    
    float mXProgress;
    
    float mYProgress;
    
    final StackView this$0;
    
    public StackSlider(StackSlider param1StackSlider) {
      this.mView = param1StackSlider.mView;
      this.mYProgress = param1StackSlider.mYProgress;
      this.mXProgress = param1StackSlider.mXProgress;
      this.mMode = param1StackSlider.mMode;
    }
    
    private float cubic(float param1Float) {
      return (float)(Math.pow((param1Float * 2.0F - 1.0F), 3.0D) + 1.0D) / 2.0F;
    }
    
    private float highlightAlphaInterpolator(float param1Float) {
      if (param1Float < 0.4F)
        return cubic(param1Float / 0.4F) * 0.85F; 
      return cubic(1.0F - (param1Float - 0.4F) / (1.0F - 0.4F)) * 0.85F;
    }
    
    private float viewAlphaInterpolator(float param1Float) {
      if (param1Float > 0.3F)
        return (param1Float - 0.3F) / (1.0F - 0.3F); 
      return 0.0F;
    }
    
    private float rotationInterpolator(float param1Float) {
      if (param1Float < 0.2F)
        return 0.0F; 
      return (param1Float - 0.2F) / (1.0F - 0.2F);
    }
    
    void setView(View param1View) {
      this.mView = param1View;
    }
    
    public void setYProgress(float param1Float) {
      byte b;
      param1Float = Math.min(1.0F, param1Float);
      param1Float = Math.max(0.0F, param1Float);
      this.mYProgress = param1Float;
      View view = this.mView;
      if (view == null)
        return; 
      StackView.LayoutParams layoutParams2 = (StackView.LayoutParams)view.getLayoutParams();
      StackView.LayoutParams layoutParams1 = (StackView.LayoutParams)StackView.this.mHighlight.getLayoutParams();
      if (StackView.this.mStackMode == 0) {
        b = 1;
      } else {
        b = -1;
      } 
      if (Float.compare(0.0F, this.mYProgress) != 0 && Float.compare(1.0F, this.mYProgress) != 0) {
        if (this.mView.getLayerType() == 0)
          this.mView.setLayerType(2, null); 
      } else if (this.mView.getLayerType() != 0) {
        this.mView.setLayerType(0, null);
      } 
      int i = this.mMode;
      if (i != 0) {
        if (i != 1) {
          if (i == 2) {
            param1Float *= 0.2F;
            layoutParams2.setVerticalOffset(Math.round(-b * param1Float * StackView.this.mSlideAmount));
            layoutParams1.setVerticalOffset(Math.round(-b * param1Float * StackView.this.mSlideAmount));
            StackView.this.mHighlight.setAlpha(highlightAlphaInterpolator(param1Float));
          } 
        } else {
          param1Float = (1.0F - param1Float) * 0.2F;
          layoutParams2.setVerticalOffset(Math.round(b * param1Float * StackView.this.mSlideAmount));
          layoutParams1.setVerticalOffset(Math.round(b * param1Float * StackView.this.mSlideAmount));
          StackView.this.mHighlight.setAlpha(highlightAlphaInterpolator(param1Float));
        } 
      } else {
        layoutParams2.setVerticalOffset(Math.round(-param1Float * b * StackView.this.mSlideAmount));
        layoutParams1.setVerticalOffset(Math.round(-param1Float * b * StackView.this.mSlideAmount));
        StackView.this.mHighlight.setAlpha(highlightAlphaInterpolator(param1Float));
        float f = viewAlphaInterpolator(1.0F - param1Float);
        if (this.mView.getAlpha() == 0.0F && f != 0.0F && this.mView.getVisibility() != 0) {
          this.mView.setVisibility(0);
        } else if (f == 0.0F && this.mView.getAlpha() != 0.0F) {
          View view1 = this.mView;
          if (view1.getVisibility() == 0)
            this.mView.setVisibility(4); 
        } 
        this.mView.setAlpha(f);
        this.mView.setRotationX(b * 90.0F * rotationInterpolator(param1Float));
        StackView.this.mHighlight.setRotationX(b * 90.0F * rotationInterpolator(param1Float));
      } 
    }
    
    public void setXProgress(float param1Float) {
      param1Float = Math.min(2.0F, param1Float);
      param1Float = Math.max(-2.0F, param1Float);
      this.mXProgress = param1Float;
      View view = this.mView;
      if (view == null)
        return; 
      StackView.LayoutParams layoutParams1 = (StackView.LayoutParams)view.getLayoutParams();
      StackView.LayoutParams layoutParams2 = (StackView.LayoutParams)StackView.this.mHighlight.getLayoutParams();
      param1Float *= 0.2F;
      layoutParams1.setHorizontalOffset(Math.round(StackView.this.mSlideAmount * param1Float));
      layoutParams2.setHorizontalOffset(Math.round(StackView.this.mSlideAmount * param1Float));
    }
    
    void setMode(int param1Int) {
      this.mMode = param1Int;
    }
    
    float getDurationForNeutralPosition() {
      return getDuration(false, 0.0F);
    }
    
    float getDurationForOffscreenPosition() {
      return getDuration(true, 0.0F);
    }
    
    float getDurationForNeutralPosition(float param1Float) {
      return getDuration(false, param1Float);
    }
    
    float getDurationForOffscreenPosition(float param1Float) {
      return getDuration(true, param1Float);
    }
    
    private float getDuration(boolean param1Boolean, float param1Float) {
      View view = this.mView;
      if (view != null) {
        StackView.LayoutParams layoutParams = (StackView.LayoutParams)view.getLayoutParams();
        float f1 = (float)Math.hypot(layoutParams.horizontalOffset, layoutParams.verticalOffset);
        float f2 = (float)Math.hypot(StackView.this.mSlideAmount, (StackView.this.mSlideAmount * 0.4F));
        float f3 = f1;
        if (f1 > f2)
          f3 = f2; 
        if (param1Float == 0.0F) {
          if (param1Boolean) {
            param1Float = 1.0F - f3 / f2;
          } else {
            param1Float = f3 / f2;
          } 
          return param1Float * 400.0F;
        } 
        if (param1Boolean) {
          param1Float = f3 / Math.abs(param1Float);
        } else {
          param1Float = (f2 - f3) / Math.abs(param1Float);
        } 
        if (param1Float < 50.0F || param1Float > 400.0F)
          return getDuration(param1Boolean, 0.0F); 
        return param1Float;
      } 
      return 0.0F;
    }
    
    public float getYProgress() {
      return this.mYProgress;
    }
    
    public float getXProgress() {
      return this.mXProgress;
    }
    
    public StackSlider() {}
  }
  
  LayoutParams createOrReuseLayoutParams(View paramView) {
    LayoutParams layoutParams;
    ViewGroup.LayoutParams layoutParams1 = paramView.getLayoutParams();
    if (layoutParams1 instanceof LayoutParams) {
      layoutParams = (LayoutParams)layoutParams1;
      layoutParams.setHorizontalOffset(0);
      layoutParams.setVerticalOffset(0);
      layoutParams.width = 0;
      layoutParams.width = 0;
      return layoutParams;
    } 
    return new LayoutParams((View)layoutParams);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    checkForAndHandleDataChanged();
    paramInt2 = getChildCount();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      View view = getChildAt(paramInt1);
      int i = this.mPaddingLeft, j = view.getMeasuredWidth();
      paramInt3 = this.mPaddingTop;
      paramInt4 = view.getMeasuredHeight();
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      view.layout(this.mPaddingLeft + layoutParams.horizontalOffset, this.mPaddingTop + layoutParams.verticalOffset, layoutParams.horizontalOffset + i + j, layoutParams.verticalOffset + paramInt3 + paramInt4);
    } 
    onLayout();
  }
  
  public void advance() {
    long l1 = System.currentTimeMillis(), l2 = this.mLastInteractionTime;
    if (this.mAdapter == null)
      return; 
    int i = getCount();
    if (i == 1 && this.mLoopViews)
      return; 
    if (this.mSwipeGestureType == 0 && l1 - l2 > 5000L)
      showNext(); 
  }
  
  private void measureChildren() {
    int i = getChildCount();
    int j = getMeasuredWidth();
    int k = getMeasuredHeight();
    int m = Math.round(j * 0.9F) - this.mPaddingLeft - this.mPaddingRight;
    int n = Math.round(k * 0.9F) - this.mPaddingTop - this.mPaddingBottom;
    int i1 = 0;
    int i2 = 0;
    for (byte b = 0; b < i; b++, i1 = i3, i2 = i5) {
      View view = getChildAt(b);
      int i3 = View.MeasureSpec.makeMeasureSpec(m, -2147483648);
      int i4 = View.MeasureSpec.makeMeasureSpec(n, -2147483648);
      view.measure(i3, i4);
      i3 = i1;
      int i5 = i2;
      if (view != this.mHighlight) {
        i3 = i1;
        i5 = i2;
        if (view != this.mClickFeedback) {
          i3 = view.getMeasuredWidth();
          int i6 = view.getMeasuredHeight();
          i4 = i1;
          if (i3 > i1)
            i4 = i3; 
          i3 = i4;
          i5 = i2;
          if (i6 > i2) {
            i5 = i6;
            i3 = i4;
          } 
        } 
      } 
    } 
    this.mNewPerspectiveShiftX = j * 0.1F;
    this.mNewPerspectiveShiftY = k * 0.1F;
    if (i1 > 0 && i > 0 && i1 < m)
      this.mNewPerspectiveShiftX = (j - i1); 
    if (i2 > 0 && i > 0 && i2 < n)
      this.mNewPerspectiveShiftY = (k - i2); 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    boolean bool;
    int i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt2);
    int k = View.MeasureSpec.getMode(paramInt1);
    int m = View.MeasureSpec.getMode(paramInt2);
    paramInt1 = this.mReferenceChildWidth;
    paramInt2 = 0;
    if (paramInt1 != -1 && this.mReferenceChildHeight != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (m == 0) {
      if (bool) {
        float f = this.mReferenceChildHeight;
        paramInt1 = Math.round(f * (1.1111112F + 1.0F)) + this.mPaddingTop + this.mPaddingBottom;
      } else {
        paramInt1 = 0;
      } 
    } else {
      paramInt1 = j;
      if (m == Integer.MIN_VALUE)
        if (bool) {
          paramInt1 = Math.round(this.mReferenceChildHeight * (1.1111112F + 1.0F)) + this.mPaddingTop + this.mPaddingBottom;
          if (paramInt1 > j)
            paramInt1 = j | 0x1000000; 
        } else {
          paramInt1 = 0;
        }  
    } 
    if (k == 0) {
      if (bool) {
        float f = this.mReferenceChildWidth;
        paramInt2 = Math.round(f * (1.0F + 1.1111112F)) + this.mPaddingLeft + this.mPaddingRight;
      } 
    } else {
      paramInt2 = i;
      if (m == Integer.MIN_VALUE)
        if (bool) {
          paramInt2 = this.mReferenceChildWidth + this.mPaddingLeft + this.mPaddingRight;
          if (paramInt2 > i)
            paramInt2 = i | 0x1000000; 
        } else {
          paramInt2 = 0;
        }  
    } 
    setMeasuredDimension(paramInt2, paramInt1);
    measureChildren();
  }
  
  public CharSequence getAccessibilityClassName() {
    return StackView.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    boolean bool;
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (getChildCount() > 1) {
      bool = true;
    } else {
      bool = false;
    } 
    paramAccessibilityNodeInfo.setScrollable(bool);
    if (isEnabled()) {
      if (getDisplayedChild() < getChildCount() - 1) {
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
        if (this.mStackMode == 0) {
          paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_DOWN);
        } else {
          paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_UP);
        } 
      } 
      if (getDisplayedChild() > 0) {
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
        if (this.mStackMode == 0) {
          paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_UP);
        } else {
          paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_DOWN);
        } 
      } 
    } 
  }
  
  private boolean goForward() {
    if (getDisplayedChild() < getChildCount() - 1) {
      showNext();
      return true;
    } 
    return false;
  }
  
  private boolean goBackward() {
    if (getDisplayedChild() > 0) {
      showPrevious();
      return true;
    } 
    return false;
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    if (!isEnabled())
      return false; 
    if (paramInt != 4096) {
      if (paramInt != 8192) {
        switch (paramInt) {
          default:
            return false;
          case 16908359:
            if (this.mStackMode == 0)
              return goForward(); 
            return goBackward();
          case 16908358:
            break;
        } 
        if (this.mStackMode == 0)
          return goBackward(); 
        return goForward();
      } 
      return goBackward();
    } 
    return goForward();
  }
  
  class LayoutParams extends ViewGroup.LayoutParams {
    private final Rect parentRect = new Rect();
    
    private final Rect invalidateRect = new Rect();
    
    private final RectF invalidateRectf = new RectF();
    
    private final Rect globalInvalidateRect = new Rect();
    
    int horizontalOffset;
    
    View mView;
    
    final StackView this$0;
    
    int verticalOffset;
    
    LayoutParams(View param1View) {
      super(0, 0);
      this.width = 0;
      this.height = 0;
      this.horizontalOffset = 0;
      this.verticalOffset = 0;
      this.mView = param1View;
    }
    
    LayoutParams(Context param1Context, AttributeSet param1AttributeSet) {
      super(param1Context, param1AttributeSet);
      this.horizontalOffset = 0;
      this.verticalOffset = 0;
      this.width = 0;
      this.height = 0;
    }
    
    void invalidateGlobalRegion(View param1View, Rect param1Rect) {
      this.globalInvalidateRect.set(param1Rect);
      this.globalInvalidateRect.union(0, 0, StackView.this.getWidth(), StackView.this.getHeight());
      View view = param1View;
      if (param1View.getParent() == null || !(param1View.getParent() instanceof View))
        return; 
      int i = 1;
      this.parentRect.set(0, 0, 0, 0);
      param1View = view;
      while (param1View.getParent() != null && param1View.getParent() instanceof View) {
        Rect rect1 = this.parentRect, rect2 = this.globalInvalidateRect;
        if (!rect1.contains(rect2)) {
          if (!i) {
            rect1 = this.globalInvalidateRect;
            int i3 = param1View.getLeft(), i4 = param1View.getScrollX(), i5 = param1View.getTop();
            i = param1View.getScrollY();
            rect1.offset(i3 - i4, i5 - i);
          } 
          i = 0;
          param1View = (View)param1View.getParent();
          rect1 = this.parentRect;
          int n = param1View.getScrollX(), j = param1View.getScrollY();
          int i1 = param1View.getWidth(), i2 = param1View.getScrollX(), m = param1View.getHeight(), k = param1View.getScrollY();
          rect1.set(n, j, i1 + i2, m + k);
          param1View.invalidate(this.globalInvalidateRect.left, this.globalInvalidateRect.top, this.globalInvalidateRect.right, this.globalInvalidateRect.bottom);
        } 
      } 
      param1View.invalidate(this.globalInvalidateRect.left, this.globalInvalidateRect.top, this.globalInvalidateRect.right, this.globalInvalidateRect.bottom);
    }
    
    Rect getInvalidateRect() {
      return this.invalidateRect;
    }
    
    void resetInvalidateRect() {
      this.invalidateRect.set(0, 0, 0, 0);
    }
    
    public void setVerticalOffset(int param1Int) {
      setOffsets(this.horizontalOffset, param1Int);
    }
    
    public void setHorizontalOffset(int param1Int) {
      setOffsets(param1Int, this.verticalOffset);
    }
    
    public void setOffsets(int param1Int1, int param1Int2) {
      int i = param1Int1 - this.horizontalOffset;
      this.horizontalOffset = param1Int1;
      int j = param1Int2 - this.verticalOffset;
      this.verticalOffset = param1Int2;
      View view = this.mView;
      if (view != null) {
        view.requestLayout();
        param1Int1 = Math.min(this.mView.getLeft() + i, this.mView.getLeft());
        i = Math.max(this.mView.getRight() + i, this.mView.getRight());
        param1Int2 = Math.min(this.mView.getTop() + j, this.mView.getTop());
        j = Math.max(this.mView.getBottom() + j, this.mView.getBottom());
        this.invalidateRectf.set(param1Int1, param1Int2, i, j);
        float f1 = -this.invalidateRectf.left;
        float f2 = -this.invalidateRectf.top;
        this.invalidateRectf.offset(f1, f2);
        this.mView.getMatrix().mapRect(this.invalidateRectf);
        this.invalidateRectf.offset(-f1, -f2);
        Rect rect = this.invalidateRect;
        param1Int2 = (int)Math.floor(this.invalidateRectf.left);
        double d = this.invalidateRectf.top;
        i = (int)Math.floor(d);
        d = this.invalidateRectf.right;
        param1Int1 = (int)Math.ceil(d);
        d = this.invalidateRectf.bottom;
        j = (int)Math.ceil(d);
        rect.set(param1Int2, i, param1Int1, j);
        invalidateGlobalRegion(this.mView, this.invalidateRect);
      } 
    }
  }
  
  class HolographicHelper {
    private final Paint mHolographicPaint = new Paint();
    
    private final Paint mErasePaint = new Paint();
    
    private final Paint mBlurPaint = new Paint();
    
    private final Canvas mCanvas = new Canvas();
    
    private final Canvas mMaskCanvas = new Canvas();
    
    private final int[] mTmpXY = new int[2];
    
    private final Matrix mIdentityMatrix = new Matrix();
    
    private static final int CLICK_FEEDBACK = 1;
    
    private static final int RES_OUT = 0;
    
    private float mDensity;
    
    private BlurMaskFilter mLargeBlurMaskFilter;
    
    private BlurMaskFilter mSmallBlurMaskFilter;
    
    HolographicHelper(StackView this$0) {
      this.mDensity = (this$0.getResources().getDisplayMetrics()).density;
      this.mHolographicPaint.setFilterBitmap(true);
      this.mHolographicPaint.setMaskFilter((MaskFilter)TableMaskFilter.CreateClipTable(0, 30));
      this.mErasePaint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
      this.mErasePaint.setFilterBitmap(true);
      this.mSmallBlurMaskFilter = new BlurMaskFilter(this.mDensity * 2.0F, BlurMaskFilter.Blur.NORMAL);
      this.mLargeBlurMaskFilter = new BlurMaskFilter(this.mDensity * 4.0F, BlurMaskFilter.Blur.NORMAL);
    }
    
    Bitmap createClickOutline(View param1View, int param1Int) {
      return createOutline(param1View, 1, param1Int);
    }
    
    Bitmap createResOutline(View param1View, int param1Int) {
      return createOutline(param1View, 0, param1Int);
    }
    
    Bitmap createOutline(View param1View, int param1Int1, int param1Int2) {
      this.mHolographicPaint.setColor(param1Int2);
      if (param1Int1 == 0) {
        this.mBlurPaint.setMaskFilter((MaskFilter)this.mSmallBlurMaskFilter);
      } else if (param1Int1 == 1) {
        this.mBlurPaint.setMaskFilter((MaskFilter)this.mLargeBlurMaskFilter);
      } 
      if (param1View.getMeasuredWidth() == 0 || param1View.getMeasuredHeight() == 0)
        return null; 
      DisplayMetrics displayMetrics = param1View.getResources().getDisplayMetrics();
      param1Int1 = param1View.getMeasuredWidth();
      param1Int2 = param1View.getMeasuredHeight();
      Bitmap.Config config = Bitmap.Config.ARGB_8888;
      Bitmap bitmap = Bitmap.createBitmap(displayMetrics, param1Int1, param1Int2, config);
      this.mCanvas.setBitmap(bitmap);
      float f1 = param1View.getRotationX();
      float f2 = param1View.getRotation();
      float f3 = param1View.getTranslationY();
      float f4 = param1View.getTranslationX();
      param1View.setRotationX(0.0F);
      param1View.setRotation(0.0F);
      param1View.setTranslationY(0.0F);
      param1View.setTranslationX(0.0F);
      param1View.draw(this.mCanvas);
      param1View.setRotationX(f1);
      param1View.setRotation(f2);
      param1View.setTranslationY(f3);
      param1View.setTranslationX(f4);
      drawOutline(this.mCanvas, bitmap);
      this.mCanvas.setBitmap(null);
      return bitmap;
    }
    
    void drawOutline(Canvas param1Canvas, Bitmap param1Bitmap) {
      int[] arrayOfInt = this.mTmpXY;
      Bitmap bitmap = param1Bitmap.extractAlpha(this.mBlurPaint, arrayOfInt);
      this.mMaskCanvas.setBitmap(bitmap);
      this.mMaskCanvas.drawBitmap(param1Bitmap, -arrayOfInt[0], -arrayOfInt[1], this.mErasePaint);
      param1Canvas.drawColor(0, PorterDuff.Mode.CLEAR);
      param1Canvas.setMatrix(this.mIdentityMatrix);
      param1Canvas.drawBitmap(bitmap, arrayOfInt[0], arrayOfInt[1], this.mHolographicPaint);
      this.mMaskCanvas.setBitmap(null);
      bitmap.recycle();
    }
  }
}
