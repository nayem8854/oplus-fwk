package android.widget;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.view.Menu;
import android.view.MenuItem;

public class OplusFloatingToolbarUtil implements IOplusFloatingToolbarUtil {
  private static final String SEARCH_PACKAGE_NAME = "com.heytap.browser";
  
  public boolean setSearchMenuItem(int paramInt, Intent paramIntent, CharSequence paramCharSequence, ResolveInfo paramResolveInfo, Menu paramMenu) {
    if ("com.heytap.browser".equals(paramResolveInfo.activityInfo.packageName)) {
      MenuItem menuItem2 = paramMenu.add(0, 0, paramInt, paramCharSequence);
      MenuItem menuItem1 = menuItem2.setIntent(paramIntent);
      menuItem1.setShowAsAction(1);
      return true;
    } 
    return false;
  }
  
  public boolean[] handleCursorControllersEnabled(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5) {
    if (!paramBoolean1) {
      paramBoolean1 = false;
      paramBoolean5 = false;
    } else if (!paramBoolean2) {
      paramBoolean1 = false;
    } else {
      paramBoolean1 = paramBoolean4;
      if (!paramBoolean3) {
        paramBoolean5 = false;
        paramBoolean1 = paramBoolean4;
      } 
    } 
    return new boolean[] { paramBoolean1, paramBoolean5 };
  }
  
  public boolean needAllSelected(boolean paramBoolean) {
    return paramBoolean;
  }
  
  public boolean needHook() {
    return true;
  }
  
  public void updateSelectAllItem(Menu paramMenu, TextView paramTextView, int paramInt) {
    MenuItem menuItem;
    boolean bool1;
    boolean bool2;
    if (paramTextView.canSelectText() && 
      !paramTextView.hasPasswordTransformationMethod() && 
      paramTextView.getSelectionStart() == paramTextView.getSelectionEnd()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (paramMenu.findItem(201457759) != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (bool1 && !bool2) {
      menuItem = paramMenu.add(0, 201457759, paramInt, 201589021);
      menuItem.setShowAsAction(1);
    } else if (!bool1 && bool2) {
      menuItem.removeItem(201457759);
    } 
  }
}
