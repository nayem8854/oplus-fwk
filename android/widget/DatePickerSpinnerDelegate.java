package android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.icu.util.Calendar;
import android.os.Parcelable;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;
import libcore.icu.ICU;

class DatePickerSpinnerDelegate extends DatePicker.AbstractDatePickerDelegate {
  private static final String DATE_FORMAT = "MM/dd/yyyy";
  
  private static final boolean DEFAULT_CALENDAR_VIEW_SHOWN = true;
  
  private static final boolean DEFAULT_ENABLED_STATE = true;
  
  private static final int DEFAULT_END_YEAR = 2100;
  
  private static final boolean DEFAULT_SPINNERS_SHOWN = true;
  
  private static final int DEFAULT_START_YEAR = 1900;
  
  private final CalendarView mCalendarView;
  
  private final DateFormat mDateFormat = new SimpleDateFormat("MM/dd/yyyy");
  
  private final NumberPicker mDaySpinner;
  
  private final EditText mDaySpinnerInput;
  
  private boolean mIsEnabled = true;
  
  private Calendar mMaxDate;
  
  private Calendar mMinDate;
  
  private final NumberPicker mMonthSpinner;
  
  private final EditText mMonthSpinnerInput;
  
  private int mNumberOfMonths;
  
  private String[] mShortMonths;
  
  private final LinearLayout mSpinners;
  
  private Calendar mTempDate;
  
  private final NumberPicker mYearSpinner;
  
  private final EditText mYearSpinnerInput;
  
  DatePickerSpinnerDelegate(DatePicker paramDatePicker, Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramDatePicker, paramContext);
    this.mDelegator = paramDatePicker;
    this.mContext = paramContext;
    setCurrentLocale(Locale.getDefault());
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DatePicker, paramInt1, paramInt2);
    boolean bool1 = typedArray.getBoolean(6, true);
    boolean bool2 = typedArray.getBoolean(7, true);
    paramInt1 = typedArray.getInt(1, 1900);
    paramInt2 = typedArray.getInt(2, 2100);
    String str2 = typedArray.getString(4);
    String str1 = typedArray.getString(5);
    int i = typedArray.getResourceId(20, 17367140);
    typedArray.recycle();
    LayoutInflater layoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    View view = layoutInflater.inflate(i, this.mDelegator, true);
    view.setSaveFromParentEnabled(false);
    Object object = new Object(this);
    this.mSpinners = this.mDelegator.<LinearLayout>findViewById(16909297);
    CalendarView calendarView = this.mDelegator.<CalendarView>findViewById(16908822);
    calendarView.setOnDateChangeListener((CalendarView.OnDateChangeListener)new Object(this));
    NumberPicker numberPicker = this.mDelegator.<NumberPicker>findViewById(16908913);
    numberPicker.setFormatter(NumberPicker.getTwoDigitFormatter());
    this.mDaySpinner.setOnLongPressUpdateInterval(100L);
    this.mDaySpinner.setOnValueChangedListener((NumberPicker.OnValueChangeListener)object);
    this.mDaySpinnerInput = this.mDaySpinner.<EditText>findViewById(16909248);
    this.mMonthSpinner = numberPicker = this.mDelegator.<NumberPicker>findViewById(16909190);
    numberPicker.setMinValue(0);
    this.mMonthSpinner.setMaxValue(this.mNumberOfMonths - 1);
    this.mMonthSpinner.setDisplayedValues(this.mShortMonths);
    this.mMonthSpinner.setOnLongPressUpdateInterval(200L);
    this.mMonthSpinner.setOnValueChangedListener((NumberPicker.OnValueChangeListener)object);
    this.mMonthSpinnerInput = this.mMonthSpinner.<EditText>findViewById(16909248);
    this.mYearSpinner = numberPicker = this.mDelegator.<NumberPicker>findViewById(16909645);
    numberPicker.setOnLongPressUpdateInterval(100L);
    this.mYearSpinner.setOnValueChangedListener((NumberPicker.OnValueChangeListener)object);
    this.mYearSpinnerInput = this.mYearSpinner.<EditText>findViewById(16909248);
    if (!bool1 && !bool2) {
      setSpinnersShown(true);
    } else {
      setSpinnersShown(bool1);
      setCalendarViewShown(bool2);
    } 
    this.mTempDate.clear();
    if (!TextUtils.isEmpty(str2)) {
      if (!parseDate(str2, this.mTempDate))
        this.mTempDate.set(paramInt1, 0, 1); 
    } else {
      CalendarView.parseDate("01/01/1900", this.mTempDate);
    } 
    setMinDate(this.mTempDate.getTimeInMillis());
    this.mTempDate.clear();
    if (!TextUtils.isEmpty(str1)) {
      if (!parseDate(str1, this.mTempDate))
        this.mTempDate.set(paramInt2, 11, 31); 
    } else {
      this.mTempDate.set(paramInt2, 11, 31);
    } 
    setMaxDate(this.mTempDate.getTimeInMillis());
    this.mCurrentDate.setTimeInMillis(System.currentTimeMillis());
    i = this.mCurrentDate.get(1);
    paramInt2 = this.mCurrentDate.get(2);
    Calendar calendar = this.mCurrentDate;
    paramInt1 = calendar.get(5);
    init(i, paramInt2, paramInt1, (DatePicker.OnDateChangedListener)null);
    reorderSpinners();
    setContentDescriptions();
    if (this.mDelegator.getImportantForAccessibility() == 0)
      this.mDelegator.setImportantForAccessibility(1); 
  }
  
  public void init(int paramInt1, int paramInt2, int paramInt3, DatePicker.OnDateChangedListener paramOnDateChangedListener) {
    setDate(paramInt1, paramInt2, paramInt3);
    updateSpinners();
    updateCalendarView();
    this.mOnDateChangedListener = paramOnDateChangedListener;
  }
  
  public void updateDate(int paramInt1, int paramInt2, int paramInt3) {
    if (!isNewDate(paramInt1, paramInt2, paramInt3))
      return; 
    setDate(paramInt1, paramInt2, paramInt3);
    updateSpinners();
    updateCalendarView();
    notifyDateChanged();
  }
  
  public int getYear() {
    return this.mCurrentDate.get(1);
  }
  
  public int getMonth() {
    return this.mCurrentDate.get(2);
  }
  
  public int getDayOfMonth() {
    return this.mCurrentDate.get(5);
  }
  
  public void setFirstDayOfWeek(int paramInt) {
    this.mCalendarView.setFirstDayOfWeek(paramInt);
  }
  
  public int getFirstDayOfWeek() {
    return this.mCalendarView.getFirstDayOfWeek();
  }
  
  public void setMinDate(long paramLong) {
    this.mTempDate.setTimeInMillis(paramLong);
    if (this.mTempDate.get(1) == this.mMinDate.get(1)) {
      Calendar calendar = this.mTempDate;
      if (calendar.get(6) == this.mMinDate.get(6))
        return; 
    } 
    this.mMinDate.setTimeInMillis(paramLong);
    this.mCalendarView.setMinDate(paramLong);
    if (this.mCurrentDate.before(this.mMinDate)) {
      this.mCurrentDate.setTimeInMillis(this.mMinDate.getTimeInMillis());
      updateCalendarView();
    } 
    updateSpinners();
  }
  
  public Calendar getMinDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(this.mCalendarView.getMinDate());
    return calendar;
  }
  
  public void setMaxDate(long paramLong) {
    this.mTempDate.setTimeInMillis(paramLong);
    if (this.mTempDate.get(1) == this.mMaxDate.get(1)) {
      Calendar calendar = this.mTempDate;
      if (calendar.get(6) == this.mMaxDate.get(6))
        return; 
    } 
    this.mMaxDate.setTimeInMillis(paramLong);
    this.mCalendarView.setMaxDate(paramLong);
    if (this.mCurrentDate.after(this.mMaxDate)) {
      this.mCurrentDate.setTimeInMillis(this.mMaxDate.getTimeInMillis());
      updateCalendarView();
    } 
    updateSpinners();
  }
  
  public Calendar getMaxDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(this.mCalendarView.getMaxDate());
    return calendar;
  }
  
  public void setEnabled(boolean paramBoolean) {
    this.mDaySpinner.setEnabled(paramBoolean);
    this.mMonthSpinner.setEnabled(paramBoolean);
    this.mYearSpinner.setEnabled(paramBoolean);
    this.mCalendarView.setEnabled(paramBoolean);
    this.mIsEnabled = paramBoolean;
  }
  
  public boolean isEnabled() {
    return this.mIsEnabled;
  }
  
  public CalendarView getCalendarView() {
    return this.mCalendarView;
  }
  
  public void setCalendarViewShown(boolean paramBoolean) {
    byte b;
    CalendarView calendarView = this.mCalendarView;
    if (paramBoolean) {
      b = 0;
    } else {
      b = 8;
    } 
    calendarView.setVisibility(b);
  }
  
  public boolean getCalendarViewShown() {
    boolean bool;
    if (this.mCalendarView.getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setSpinnersShown(boolean paramBoolean) {
    byte b;
    LinearLayout linearLayout = this.mSpinners;
    if (paramBoolean) {
      b = 0;
    } else {
      b = 8;
    } 
    linearLayout.setVisibility(b);
  }
  
  public boolean getSpinnersShown() {
    return this.mSpinners.isShown();
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    setCurrentLocale(paramConfiguration.locale);
  }
  
  public Parcelable onSaveInstanceState(Parcelable paramParcelable) {
    int i = getYear(), j = getMonth(), k = getDayOfMonth();
    return 
      new DatePicker.AbstractDatePickerDelegate.SavedState(paramParcelable, i, j, k, getMinDate().getTimeInMillis(), getMaxDate().getTimeInMillis());
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable instanceof DatePicker.AbstractDatePickerDelegate.SavedState) {
      paramParcelable = paramParcelable;
      setDate(paramParcelable.getSelectedYear(), paramParcelable.getSelectedMonth(), paramParcelable.getSelectedDay());
      updateSpinners();
      updateCalendarView();
    } 
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    onPopulateAccessibilityEvent(paramAccessibilityEvent);
    return true;
  }
  
  protected void setCurrentLocale(Locale paramLocale) {
    super.setCurrentLocale(paramLocale);
    this.mTempDate = getCalendarForLocale(this.mTempDate, paramLocale);
    this.mMinDate = getCalendarForLocale(this.mMinDate, paramLocale);
    this.mMaxDate = getCalendarForLocale(this.mMaxDate, paramLocale);
    this.mCurrentDate = getCalendarForLocale(this.mCurrentDate, paramLocale);
    this.mNumberOfMonths = this.mTempDate.getActualMaximum(2) + 1;
    this.mShortMonths = (new DateFormatSymbols()).getShortMonths();
    if (usingNumericMonths()) {
      this.mShortMonths = new String[this.mNumberOfMonths];
      for (byte b = 0; b < this.mNumberOfMonths; b++) {
        this.mShortMonths[b] = String.format("%d", new Object[] { Integer.valueOf(b + 1) });
      } 
    } 
  }
  
  private boolean usingNumericMonths() {
    return Character.isDigit(this.mShortMonths[0].charAt(0));
  }
  
  private Calendar getCalendarForLocale(Calendar paramCalendar, Locale paramLocale) {
    if (paramCalendar == null)
      return Calendar.getInstance(paramLocale); 
    long l = paramCalendar.getTimeInMillis();
    paramCalendar = Calendar.getInstance(paramLocale);
    paramCalendar.setTimeInMillis(l);
    return paramCalendar;
  }
  
  private void reorderSpinners() {
    this.mSpinners.removeAllViews();
    String str = DateFormat.getBestDateTimePattern(Locale.getDefault(), "yyyyMMMdd");
    char[] arrayOfChar = ICU.getDateFormatOrder(str);
    int i = arrayOfChar.length;
    for (byte b = 0; b < i; b++) {
      char c = arrayOfChar[b];
      if (c != 'M') {
        if (c != 'd') {
          if (c == 'y') {
            this.mSpinners.addView(this.mYearSpinner);
            setImeOptions(this.mYearSpinner, i, b);
          } else {
            throw new IllegalArgumentException(Arrays.toString(arrayOfChar));
          } 
        } else {
          this.mSpinners.addView(this.mDaySpinner);
          setImeOptions(this.mDaySpinner, i, b);
        } 
      } else {
        this.mSpinners.addView(this.mMonthSpinner);
        setImeOptions(this.mMonthSpinner, i, b);
      } 
    } 
  }
  
  private boolean parseDate(String paramString, Calendar paramCalendar) {
    try {
      paramCalendar.setTime(this.mDateFormat.parse(paramString));
      return true;
    } catch (ParseException parseException) {
      parseException.printStackTrace();
      return false;
    } 
  }
  
  private boolean isNewDate(int paramInt1, int paramInt2, int paramInt3) {
    Calendar calendar = this.mCurrentDate;
    boolean bool = true;
    if (calendar.get(1) == paramInt1) {
      calendar = this.mCurrentDate;
      if (calendar.get(2) == paramInt2) {
        calendar = this.mCurrentDate;
        if (calendar.get(5) == paramInt3)
          bool = false; 
      } 
    } 
    return bool;
  }
  
  private void setDate(int paramInt1, int paramInt2, int paramInt3) {
    this.mCurrentDate.set(paramInt1, paramInt2, paramInt3);
    resetAutofilledValue();
    if (this.mCurrentDate.before(this.mMinDate)) {
      this.mCurrentDate.setTimeInMillis(this.mMinDate.getTimeInMillis());
    } else if (this.mCurrentDate.after(this.mMaxDate)) {
      this.mCurrentDate.setTimeInMillis(this.mMaxDate.getTimeInMillis());
    } 
  }
  
  private void updateSpinners() {
    if (this.mCurrentDate.equals(this.mMinDate)) {
      this.mDaySpinner.setMinValue(this.mCurrentDate.get(5));
      this.mDaySpinner.setMaxValue(this.mCurrentDate.getActualMaximum(5));
      this.mDaySpinner.setWrapSelectorWheel(false);
      this.mMonthSpinner.setDisplayedValues((String[])null);
      this.mMonthSpinner.setMinValue(this.mCurrentDate.get(2));
      this.mMonthSpinner.setMaxValue(this.mCurrentDate.getActualMaximum(2));
      this.mMonthSpinner.setWrapSelectorWheel(false);
    } else if (this.mCurrentDate.equals(this.mMaxDate)) {
      this.mDaySpinner.setMinValue(this.mCurrentDate.getActualMinimum(5));
      this.mDaySpinner.setMaxValue(this.mCurrentDate.get(5));
      this.mDaySpinner.setWrapSelectorWheel(false);
      this.mMonthSpinner.setDisplayedValues((String[])null);
      this.mMonthSpinner.setMinValue(this.mCurrentDate.getActualMinimum(2));
      this.mMonthSpinner.setMaxValue(this.mCurrentDate.get(2));
      this.mMonthSpinner.setWrapSelectorWheel(false);
    } else {
      this.mDaySpinner.setMinValue(1);
      this.mDaySpinner.setMaxValue(this.mCurrentDate.getActualMaximum(5));
      this.mDaySpinner.setWrapSelectorWheel(true);
      this.mMonthSpinner.setDisplayedValues((String[])null);
      this.mMonthSpinner.setMinValue(0);
      this.mMonthSpinner.setMaxValue(11);
      this.mMonthSpinner.setWrapSelectorWheel(true);
    } 
    String[] arrayOfString1 = this.mShortMonths;
    NumberPicker numberPicker = this.mMonthSpinner;
    int i = numberPicker.getMinValue(), j = this.mMonthSpinner.getMaxValue();
    String[] arrayOfString2 = Arrays.<String>copyOfRange(arrayOfString1, i, j + 1);
    this.mMonthSpinner.setDisplayedValues(arrayOfString2);
    this.mYearSpinner.setMinValue(this.mMinDate.get(1));
    this.mYearSpinner.setMaxValue(this.mMaxDate.get(1));
    this.mYearSpinner.setWrapSelectorWheel(false);
    this.mYearSpinner.setValue(this.mCurrentDate.get(1));
    this.mMonthSpinner.setValue(this.mCurrentDate.get(2));
    this.mDaySpinner.setValue(this.mCurrentDate.get(5));
    if (usingNumericMonths())
      this.mMonthSpinnerInput.setRawInputType(2); 
  }
  
  private void updateCalendarView() {
    this.mCalendarView.setDate(this.mCurrentDate.getTimeInMillis(), false, false);
  }
  
  private void notifyDateChanged() {
    this.mDelegator.sendAccessibilityEvent(4);
    if (this.mOnDateChangedListener != null) {
      DatePicker.OnDateChangedListener onDateChangedListener = this.mOnDateChangedListener;
      DatePicker datePicker = this.mDelegator;
      int i = getYear(), j = getMonth();
      int k = getDayOfMonth();
      onDateChangedListener.onDateChanged(datePicker, i, j, k);
    } 
    if (this.mAutoFillChangeListener != null) {
      DatePicker.OnDateChangedListener onDateChangedListener = this.mAutoFillChangeListener;
      DatePicker datePicker = this.mDelegator;
      int k = getYear(), j = getMonth();
      int i = getDayOfMonth();
      onDateChangedListener.onDateChanged(datePicker, k, j, i);
    } 
  }
  
  private void setImeOptions(NumberPicker paramNumberPicker, int paramInt1, int paramInt2) {
    if (paramInt2 < paramInt1 - 1) {
      paramInt1 = 5;
    } else {
      paramInt1 = 6;
    } 
    TextView textView = paramNumberPicker.<TextView>findViewById(16909248);
    textView.setImeOptions(paramInt1);
  }
  
  private void setContentDescriptions() {
    trySetContentDescription(this.mDaySpinner, 16909073, 17040032);
    trySetContentDescription(this.mDaySpinner, 16908918, 17040028);
    trySetContentDescription(this.mMonthSpinner, 16909073, 17040033);
    trySetContentDescription(this.mMonthSpinner, 16908918, 17040029);
    trySetContentDescription(this.mYearSpinner, 16909073, 17040034);
    trySetContentDescription(this.mYearSpinner, 16908918, 17040030);
  }
  
  private void trySetContentDescription(View paramView, int paramInt1, int paramInt2) {
    paramView = paramView.findViewById(paramInt1);
    if (paramView != null)
      paramView.setContentDescription(this.mContext.getString(paramInt2)); 
  }
  
  private void updateInputState() {
    InputMethodManager inputMethodManager = (InputMethodManager)this.mContext.getSystemService(InputMethodManager.class);
    if (inputMethodManager != null)
      if (inputMethodManager.isActive(this.mYearSpinnerInput)) {
        this.mYearSpinnerInput.clearFocus();
        inputMethodManager.hideSoftInputFromWindow(this.mDelegator.getWindowToken(), 0);
      } else if (inputMethodManager.isActive(this.mMonthSpinnerInput)) {
        this.mMonthSpinnerInput.clearFocus();
        inputMethodManager.hideSoftInputFromWindow(this.mDelegator.getWindowToken(), 0);
      } else if (inputMethodManager.isActive(this.mDaySpinnerInput)) {
        this.mDaySpinnerInput.clearFocus();
        inputMethodManager.hideSoftInputFromWindow(this.mDelegator.getWindowToken(), 0);
      }  
  }
}
