package android.widget;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.TextWatcher;
import android.text.method.DialerKeyListener;
import android.text.method.KeyListener;
import android.text.method.MovementMethod;
import android.text.method.TextKeyListener;
import android.util.AttributeSet;
import android.view.KeyEvent;

@Deprecated
public class DialerFilter extends RelativeLayout {
  public static final int DIGITS_AND_LETTERS = 1;
  
  public static final int DIGITS_AND_LETTERS_NO_DIGITS = 2;
  
  public static final int DIGITS_AND_LETTERS_NO_LETTERS = 3;
  
  public static final int DIGITS_ONLY = 4;
  
  public static final int LETTERS_ONLY = 5;
  
  EditText mDigits;
  
  EditText mHint;
  
  ImageView mIcon;
  
  InputFilter[] mInputFilters;
  
  private boolean mIsQwerty;
  
  EditText mLetters;
  
  int mMode;
  
  EditText mPrimary;
  
  public DialerFilter(Context paramContext) {
    super(paramContext);
  }
  
  public DialerFilter(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mInputFilters = new InputFilter[] { new InputFilter.AllCaps() };
    EditText editText = findViewById(16908293);
    if (editText != null) {
      editText.setFilters(this.mInputFilters);
      this.mLetters = editText = this.mHint;
      editText.setKeyListener(TextKeyListener.getInstance());
      this.mLetters.setMovementMethod((MovementMethod)null);
      this.mLetters.setFocusable(false);
      this.mPrimary = editText = findViewById(16908300);
      if (editText != null) {
        editText.setFilters(this.mInputFilters);
        this.mDigits = editText = this.mPrimary;
        editText.setKeyListener(DialerKeyListener.getInstance());
        this.mDigits.setMovementMethod((MovementMethod)null);
        this.mDigits.setFocusable(false);
        this.mIcon = findViewById(16908294);
        setFocusable(true);
        this.mIsQwerty = true;
        setMode(1);
        return;
      } 
      throw new IllegalStateException("DialerFilter must have a child EditText named primary");
    } 
    throw new IllegalStateException("DialerFilter must have a child EditText named hint");
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    ImageView imageView = this.mIcon;
    if (imageView != null) {
      if (paramBoolean) {
        paramInt = 0;
      } else {
        paramInt = 8;
      } 
      imageView.setVisibility(paramInt);
    } 
  }
  
  public boolean isQwertyKeyboard() {
    return this.mIsQwerty;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: iconst_0
    //   3: istore #4
    //   5: iload_1
    //   6: bipush #66
    //   8: if_icmpeq -> 424
    //   11: iload_1
    //   12: bipush #67
    //   14: if_icmpeq -> 222
    //   17: iload_1
    //   18: tableswitch default -> 52, 19 -> 424, 20 -> 424, 21 -> 424, 22 -> 424, 23 -> 424
    //   52: aload_0
    //   53: getfield mMode : I
    //   56: istore #5
    //   58: iload #5
    //   60: iconst_1
    //   61: if_icmpeq -> 122
    //   64: iload #5
    //   66: iconst_2
    //   67: if_icmpeq -> 108
    //   70: iload #5
    //   72: iconst_3
    //   73: if_icmpeq -> 94
    //   76: iload #5
    //   78: iconst_4
    //   79: if_icmpeq -> 94
    //   82: iload #5
    //   84: iconst_5
    //   85: if_icmpeq -> 108
    //   88: iload_3
    //   89: istore #4
    //   91: goto -> 427
    //   94: aload_0
    //   95: getfield mDigits : Landroid/widget/EditText;
    //   98: iload_1
    //   99: aload_2
    //   100: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   103: istore #4
    //   105: goto -> 427
    //   108: aload_0
    //   109: getfield mLetters : Landroid/widget/EditText;
    //   112: iload_1
    //   113: aload_2
    //   114: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   117: istore #4
    //   119: goto -> 427
    //   122: aload_0
    //   123: getfield mLetters : Landroid/widget/EditText;
    //   126: iload_1
    //   127: aload_2
    //   128: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   131: istore_3
    //   132: iload_1
    //   133: invokestatic isModifierKey : (I)Z
    //   136: ifeq -> 155
    //   139: aload_0
    //   140: getfield mDigits : Landroid/widget/EditText;
    //   143: iload_1
    //   144: aload_2
    //   145: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   148: pop
    //   149: iconst_1
    //   150: istore #4
    //   152: goto -> 427
    //   155: aload_2
    //   156: invokevirtual isPrintingKey : ()Z
    //   159: istore #4
    //   161: iload #4
    //   163: ifne -> 181
    //   166: iload_1
    //   167: bipush #62
    //   169: if_icmpeq -> 181
    //   172: iload_3
    //   173: istore #4
    //   175: iload_1
    //   176: bipush #61
    //   178: if_icmpne -> 427
    //   181: aload_2
    //   182: getstatic android/text/method/DialerKeyListener.CHARACTERS : [C
    //   185: invokevirtual getMatch : ([C)C
    //   188: istore #5
    //   190: iload #5
    //   192: ifeq -> 211
    //   195: iload_3
    //   196: aload_0
    //   197: getfield mDigits : Landroid/widget/EditText;
    //   200: iload_1
    //   201: aload_2
    //   202: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   205: iand
    //   206: istore #4
    //   208: goto -> 219
    //   211: aload_0
    //   212: iconst_2
    //   213: invokevirtual setMode : (I)V
    //   216: iload_3
    //   217: istore #4
    //   219: goto -> 427
    //   222: aload_0
    //   223: getfield mMode : I
    //   226: istore #5
    //   228: iload #5
    //   230: iconst_1
    //   231: if_icmpeq -> 396
    //   234: iload #5
    //   236: iconst_2
    //   237: if_icmpeq -> 345
    //   240: iload #5
    //   242: iconst_3
    //   243: if_icmpeq -> 289
    //   246: iload #5
    //   248: iconst_4
    //   249: if_icmpeq -> 275
    //   252: iload #5
    //   254: iconst_5
    //   255: if_icmpeq -> 261
    //   258: goto -> 421
    //   261: aload_0
    //   262: getfield mLetters : Landroid/widget/EditText;
    //   265: iload_1
    //   266: aload_2
    //   267: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   270: istore #4
    //   272: goto -> 421
    //   275: aload_0
    //   276: getfield mDigits : Landroid/widget/EditText;
    //   279: iload_1
    //   280: aload_2
    //   281: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   284: istore #4
    //   286: goto -> 421
    //   289: aload_0
    //   290: getfield mDigits : Landroid/widget/EditText;
    //   293: invokevirtual getText : ()Landroid/text/Editable;
    //   296: invokeinterface length : ()I
    //   301: aload_0
    //   302: getfield mLetters : Landroid/widget/EditText;
    //   305: invokevirtual getText : ()Landroid/text/Editable;
    //   308: invokeinterface length : ()I
    //   313: if_icmpne -> 331
    //   316: aload_0
    //   317: getfield mLetters : Landroid/widget/EditText;
    //   320: iload_1
    //   321: aload_2
    //   322: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   325: pop
    //   326: aload_0
    //   327: iconst_1
    //   328: invokevirtual setMode : (I)V
    //   331: aload_0
    //   332: getfield mDigits : Landroid/widget/EditText;
    //   335: iload_1
    //   336: aload_2
    //   337: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   340: istore #4
    //   342: goto -> 421
    //   345: aload_0
    //   346: getfield mLetters : Landroid/widget/EditText;
    //   349: iload_1
    //   350: aload_2
    //   351: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   354: istore_3
    //   355: iload_3
    //   356: istore #4
    //   358: aload_0
    //   359: getfield mLetters : Landroid/widget/EditText;
    //   362: invokevirtual getText : ()Landroid/text/Editable;
    //   365: invokeinterface length : ()I
    //   370: aload_0
    //   371: getfield mDigits : Landroid/widget/EditText;
    //   374: invokevirtual getText : ()Landroid/text/Editable;
    //   377: invokeinterface length : ()I
    //   382: if_icmpne -> 421
    //   385: aload_0
    //   386: iconst_1
    //   387: invokevirtual setMode : (I)V
    //   390: iload_3
    //   391: istore #4
    //   393: goto -> 421
    //   396: aload_0
    //   397: getfield mDigits : Landroid/widget/EditText;
    //   400: iload_1
    //   401: aload_2
    //   402: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   405: istore #4
    //   407: iload #4
    //   409: aload_0
    //   410: getfield mLetters : Landroid/widget/EditText;
    //   413: iload_1
    //   414: aload_2
    //   415: invokevirtual onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   418: iand
    //   419: istore #4
    //   421: goto -> 427
    //   424: iload_3
    //   425: istore #4
    //   427: iload #4
    //   429: ifne -> 439
    //   432: aload_0
    //   433: iload_1
    //   434: aload_2
    //   435: invokespecial onKeyDown : (ILandroid/view/KeyEvent;)Z
    //   438: ireturn
    //   439: iconst_1
    //   440: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #111	-> 0
    //   #113	-> 5
    //   #157	-> 52
    //   #188	-> 94
    //   #189	-> 105
    //   #193	-> 108
    //   #159	-> 122
    //   #163	-> 132
    //   #164	-> 139
    //   #165	-> 149
    //   #166	-> 152
    //   #174	-> 155
    //   #175	-> 161
    //   #177	-> 181
    //   #178	-> 190
    //   #179	-> 195
    //   #181	-> 211
    //   #183	-> 219
    //   #123	-> 222
    //   #149	-> 261
    //   #145	-> 275
    //   #146	-> 286
    //   #137	-> 289
    //   #138	-> 316
    //   #139	-> 326
    //   #141	-> 331
    //   #142	-> 342
    //   #130	-> 345
    //   #131	-> 355
    //   #132	-> 385
    //   #125	-> 396
    //   #126	-> 407
    //   #127	-> 421
    //   #152	-> 421
    //   #120	-> 424
    //   #198	-> 427
    //   #199	-> 432
    //   #201	-> 439
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool = this.mLetters.onKeyUp(paramInt, paramKeyEvent);
    null = this.mDigits.onKeyUp(paramInt, paramKeyEvent);
    return (bool || null);
  }
  
  public int getMode() {
    return this.mMode;
  }
  
  public void setMode(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4) {
            if (paramInt == 5) {
              makeLettersPrimary();
              this.mLetters.setVisibility(0);
              this.mDigits.setVisibility(8);
            } 
          } else {
            makeDigitsPrimary();
            this.mLetters.setVisibility(8);
            this.mDigits.setVisibility(0);
          } 
        } else {
          makeDigitsPrimary();
          this.mLetters.setVisibility(4);
          this.mDigits.setVisibility(0);
        } 
      } else {
        makeLettersPrimary();
        this.mLetters.setVisibility(0);
        this.mDigits.setVisibility(4);
      } 
    } else {
      makeDigitsPrimary();
      this.mLetters.setVisibility(0);
      this.mDigits.setVisibility(0);
    } 
    int i = this.mMode;
    this.mMode = paramInt;
    onModeChange(i, paramInt);
  }
  
  private void makeLettersPrimary() {
    if (this.mPrimary == this.mDigits)
      swapPrimaryAndHint(true); 
  }
  
  private void makeDigitsPrimary() {
    if (this.mPrimary == this.mLetters)
      swapPrimaryAndHint(false); 
  }
  
  private void swapPrimaryAndHint(boolean paramBoolean) {
    Editable editable1 = this.mLetters.getText();
    Editable editable2 = this.mDigits.getText();
    KeyListener keyListener1 = this.mLetters.getKeyListener();
    KeyListener keyListener2 = this.mDigits.getKeyListener();
    if (paramBoolean) {
      this.mLetters = this.mPrimary;
      this.mDigits = this.mHint;
    } else {
      this.mLetters = this.mHint;
      this.mDigits = this.mPrimary;
    } 
    this.mLetters.setKeyListener(keyListener1);
    this.mLetters.setText(editable1);
    Editable editable3 = this.mLetters.getText();
    Selection.setSelection(editable3, editable3.length());
    this.mDigits.setKeyListener(keyListener2);
    this.mDigits.setText(editable2);
    Editable editable4 = this.mDigits.getText();
    Selection.setSelection(editable4, editable4.length());
    this.mPrimary.setFilters(this.mInputFilters);
    this.mHint.setFilters(this.mInputFilters);
  }
  
  public CharSequence getLetters() {
    if (this.mLetters.getVisibility() == 0)
      return this.mLetters.getText(); 
    return "";
  }
  
  public CharSequence getDigits() {
    if (this.mDigits.getVisibility() == 0)
      return this.mDigits.getText(); 
    return "";
  }
  
  public CharSequence getFilterText() {
    if (this.mMode != 4)
      return getLetters(); 
    return getDigits();
  }
  
  public void append(String paramString) {
    int i = this.mMode;
    if (i != 1) {
      if (i != 2)
        if (i != 3 && i != 4) {
          if (i != 5)
            return; 
        } else {
          this.mDigits.getText().append(paramString);
          return;
        }  
      this.mLetters.getText().append(paramString);
    } else {
      this.mDigits.getText().append(paramString);
      this.mLetters.getText().append(paramString);
    } 
  }
  
  public void clearText() {
    Editable editable = this.mLetters.getText();
    editable.clear();
    editable = this.mDigits.getText();
    editable.clear();
    if (this.mIsQwerty) {
      setMode(1);
    } else {
      setMode(4);
    } 
  }
  
  public void setLettersWatcher(TextWatcher paramTextWatcher) {
    Editable editable1 = this.mLetters.getText();
    Editable editable2 = editable1;
    editable2.setSpan(paramTextWatcher, 0, editable1.length(), 18);
  }
  
  public void setDigitsWatcher(TextWatcher paramTextWatcher) {
    Editable editable1 = this.mDigits.getText();
    Editable editable2 = editable1;
    editable2.setSpan(paramTextWatcher, 0, editable1.length(), 18);
  }
  
  public void setFilterWatcher(TextWatcher paramTextWatcher) {
    if (this.mMode != 4) {
      setLettersWatcher(paramTextWatcher);
    } else {
      setDigitsWatcher(paramTextWatcher);
    } 
  }
  
  public void removeFilterWatcher(TextWatcher paramTextWatcher) {
    Editable editable;
    if (this.mMode != 4) {
      editable = this.mLetters.getText();
    } else {
      editable = this.mDigits.getText();
    } 
    editable.removeSpan(paramTextWatcher);
  }
  
  protected void onModeChange(int paramInt1, int paramInt2) {}
}
