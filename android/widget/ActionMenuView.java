package android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.view.menu.ActionMenuItemView;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuItemImpl;
import com.android.internal.view.menu.MenuPresenter;
import com.android.internal.view.menu.MenuView;

public class ActionMenuView extends LinearLayout implements MenuBuilder.ItemInvoker, MenuView {
  static final int GENERATED_ITEM_PADDING = 4;
  
  static final int MIN_CELL_SIZE = 56;
  
  private static final String TAG = "ActionMenuView";
  
  private MenuPresenter.Callback mActionMenuPresenterCallback;
  
  private boolean mFormatItems;
  
  private int mFormatItemsWidth;
  
  private int mGeneratedItemPadding;
  
  private MenuBuilder mMenu;
  
  private MenuBuilder.Callback mMenuBuilderCallback;
  
  private int mMinCellSize;
  
  private OnMenuItemClickListener mOnMenuItemClickListener;
  
  private Context mPopupContext;
  
  private int mPopupTheme;
  
  private ActionMenuPresenter mPresenter;
  
  private boolean mReserveOverflow;
  
  public ActionMenuView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ActionMenuView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    setBaselineAligned(false);
    float f = (paramContext.getResources().getDisplayMetrics()).density;
    this.mMinCellSize = (int)(56.0F * f);
    this.mGeneratedItemPadding = (int)(4.0F * f);
    this.mPopupContext = paramContext;
    this.mPopupTheme = 0;
  }
  
  public void setPopupTheme(int paramInt) {
    if (this.mPopupTheme != paramInt) {
      this.mPopupTheme = paramInt;
      if (paramInt == 0) {
        this.mPopupContext = this.mContext;
      } else {
        this.mPopupContext = (Context)new ContextThemeWrapper(this.mContext, paramInt);
      } 
    } 
  }
  
  public int getPopupTheme() {
    return this.mPopupTheme;
  }
  
  public void setPresenter(ActionMenuPresenter paramActionMenuPresenter) {
    this.mPresenter = paramActionMenuPresenter;
    paramActionMenuPresenter.setMenuView(this);
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    ActionMenuPresenter actionMenuPresenter = this.mPresenter;
    if (actionMenuPresenter != null) {
      actionMenuPresenter.updateMenuView(false);
      if (this.mPresenter.isOverflowMenuShowing()) {
        this.mPresenter.hideOverflowMenu();
        this.mPresenter.showOverflowMenu();
      } 
    } 
  }
  
  public void setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener) {
    this.mOnMenuItemClickListener = paramOnMenuItemClickListener;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    boolean bool2, bool1 = this.mFormatItems;
    if (View.MeasureSpec.getMode(paramInt1) == 1073741824) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mFormatItems = bool2;
    if (bool1 != bool2)
      this.mFormatItemsWidth = 0; 
    int i = View.MeasureSpec.getSize(paramInt1);
    if (this.mFormatItems) {
      MenuBuilder menuBuilder = this.mMenu;
      if (menuBuilder != null && i != this.mFormatItemsWidth) {
        this.mFormatItemsWidth = i;
        menuBuilder.onItemsChanged(true);
      } 
    } 
    int j = getChildCount();
    if (this.mFormatItems && j > 0) {
      onMeasureExactFormat(paramInt1, paramInt2);
    } else {
      for (i = 0; i < j; i++) {
        View view = getChildAt(i);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        layoutParams.rightMargin = 0;
        layoutParams.leftMargin = 0;
      } 
      super.onMeasure(paramInt1, paramInt2);
    } 
  }
  
  private void onMeasureExactFormat(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getMode(paramInt2);
    paramInt1 = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt2);
    int k = getPaddingLeft(), m = getPaddingRight();
    int n = getPaddingTop() + getPaddingBottom();
    int i1 = getChildMeasureSpec(paramInt2, n, -2);
    int i2 = paramInt1 - k + m;
    paramInt1 = this.mMinCellSize;
    int i3 = i2 / paramInt1;
    int i4 = i2 % paramInt1;
    if (i3 == 0) {
      setMeasuredDimension(i2, 0);
      return;
    } 
    int i5 = paramInt1 + i4 / i3;
    paramInt1 = i3;
    m = 0;
    int i6 = 0;
    paramInt2 = 0;
    int i7 = 0;
    long l = 0L;
    int i8 = getChildCount();
    byte b1, b2;
    for (b1 = 0, b2 = 0; b2 < i8; b2++) {
      View view = getChildAt(b2);
      if (view.getVisibility() != 8) {
        boolean bool = view instanceof ActionMenuItemView;
        b1++;
        if (bool) {
          k = this.mGeneratedItemPadding;
          view.setPadding(k, 0, k, 0);
        } 
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        layoutParams.expanded = false;
        layoutParams.extraPixels = 0;
        layoutParams.cellsUsed = 0;
        layoutParams.expandable = false;
        layoutParams.leftMargin = 0;
        layoutParams.rightMargin = 0;
        if (bool && ((ActionMenuItemView)view).hasText()) {
          bool = true;
        } else {
          bool = false;
        } 
        layoutParams.preventEdgeOffset = bool;
        if (layoutParams.isOverflowButton) {
          k = 1;
        } else {
          k = paramInt1;
        } 
        int i10 = measureChildForCells(view, i5, k, i1, n);
        i6 = Math.max(i6, i10);
        k = paramInt2;
        if (layoutParams.expandable)
          k = paramInt2 + 1; 
        if (layoutParams.isOverflowButton)
          i7 = 1; 
        paramInt1 -= i10;
        m = Math.max(m, view.getMeasuredHeight());
        if (i10 == 1) {
          long l1 = (1 << b2);
          l |= l1;
          paramInt2 = k;
        } else {
          paramInt2 = k;
        } 
      } 
    } 
    if (i7 && b1 == 2) {
      b2 = 1;
    } else {
      b2 = 0;
    } 
    k = 0;
    i4 = paramInt2;
    int i9 = paramInt1;
    paramInt1 = i2;
    paramInt2 = i;
    while (true) {
      if (i4 > 0 && i9 > 0) {
        i = Integer.MAX_VALUE;
        long l1 = 0L;
        int i10;
        for (i10 = 0, i2 = 0, i3 = k; i2 < i8; i2++, i10 = k, i = i11, l1 = l2) {
          long l2;
          int i11;
          View view = getChildAt(i2);
          LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
          if (!layoutParams.expandable) {
            k = i10;
            i11 = i;
            l2 = l1;
          } else if (layoutParams.cellsUsed < i) {
            i11 = layoutParams.cellsUsed;
            l2 = (1 << i2);
            k = 1;
          } else {
            k = i10;
            i11 = i;
            l2 = l1;
            if (layoutParams.cellsUsed == i) {
              l2 = (1 << i2);
              k = i10 + 1;
              l2 = l1 | l2;
              i11 = i;
            } 
          } 
        } 
        k = paramInt2;
        paramInt2 = paramInt1;
        paramInt1 = i3;
        l |= l1;
        if (i10 > i9) {
          i3 = k;
          i4 = paramInt2;
          break;
        } 
        paramInt1 = i + 1;
        for (i3 = 0; i3 < i8; i3++) {
          View view = getChildAt(i3);
          LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
          if ((l1 & (1 << i3)) == 0L) {
            if (layoutParams.cellsUsed == paramInt1)
              l |= (1 << i3); 
          } else {
            if (b2 != 0 && layoutParams.preventEdgeOffset && i9 == 1) {
              i = this.mGeneratedItemPadding;
              view.setPadding(i + i5, 0, i, 0);
            } 
            layoutParams.cellsUsed++;
            layoutParams.expanded = true;
            i9--;
          } 
        } 
        i3 = 1;
        paramInt1 = paramInt2;
        paramInt2 = k;
        k = i3;
        continue;
      } 
      i4 = paramInt1;
      paramInt1 = k;
      i3 = paramInt2;
      break;
    } 
    if (!i7 && b1 == 1) {
      paramInt2 = 1;
    } else {
      paramInt2 = 0;
    } 
    if (i9 > 0 && l != 0L && (i9 < b1 - 1 || paramInt2 != 0 || i6 > 1)) {
      float f = Long.bitCount(l);
      if (paramInt2 == 0) {
        float f1;
        if ((l & 0x1L) != 0L) {
          LayoutParams layoutParams = (LayoutParams)getChildAt(0).getLayoutParams();
          f1 = f;
          if (!layoutParams.preventEdgeOffset)
            f1 = f - 0.5F; 
        } else {
          f1 = f;
        } 
        f = f1;
        if ((l & (1 << i8 - 1)) != 0L) {
          LayoutParams layoutParams = (LayoutParams)getChildAt(i8 - 1).getLayoutParams();
          f = f1;
          if (!layoutParams.preventEdgeOffset)
            f = f1 - 0.5F; 
        } 
      } 
      i7 = 0;
      if (f > 0.0F)
        i7 = (int)((i9 * i5) / f); 
      for (i6 = 0, k = paramInt1; i6 < i8; i6++, k = paramInt1) {
        if ((l & (1 << i6)) == 0L) {
          paramInt1 = k;
        } else {
          View view = getChildAt(i6);
          LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
          if (view instanceof ActionMenuItemView) {
            layoutParams.extraPixels = i7;
            layoutParams.expanded = true;
            if (i6 == 0 && !layoutParams.preventEdgeOffset)
              layoutParams.leftMargin = -i7 / 2; 
            paramInt1 = 1;
          } else if (layoutParams.isOverflowButton) {
            layoutParams.extraPixels = i7;
            layoutParams.expanded = true;
            layoutParams.rightMargin = -i7 / 2;
            paramInt1 = 1;
          } else {
            if (i6 != 0)
              layoutParams.leftMargin = i7 / 2; 
            paramInt1 = k;
            if (i6 != i8 - 1) {
              layoutParams.rightMargin = i7 / 2;
              paramInt1 = k;
            } 
          } 
        } 
      } 
      paramInt1 = k;
    } 
    if (paramInt1 != 0)
      for (paramInt1 = 0; paramInt1 < i8; paramInt1++) {
        View view = getChildAt(paramInt1);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams.expanded) {
          paramInt2 = layoutParams.cellsUsed;
          k = layoutParams.extraPixels;
          view.measure(View.MeasureSpec.makeMeasureSpec(paramInt2 * i5 + k, 1073741824), i1);
        } 
      }  
    if (i3 != 1073741824) {
      paramInt1 = m;
    } else {
      paramInt1 = j;
    } 
    setMeasuredDimension(i4, paramInt1);
  }
  
  static int measureChildForCells(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   4: checkcast android/widget/ActionMenuView$LayoutParams
    //   7: astore #5
    //   9: iload_3
    //   10: invokestatic getSize : (I)I
    //   13: istore #6
    //   15: iload_3
    //   16: invokestatic getMode : (I)I
    //   19: istore_3
    //   20: iload #6
    //   22: iload #4
    //   24: isub
    //   25: iload_3
    //   26: invokestatic makeMeasureSpec : (II)I
    //   29: istore #7
    //   31: aload_0
    //   32: instanceof com/android/internal/view/menu/ActionMenuItemView
    //   35: ifeq -> 47
    //   38: aload_0
    //   39: checkcast com/android/internal/view/menu/ActionMenuItemView
    //   42: astore #8
    //   44: goto -> 50
    //   47: aconst_null
    //   48: astore #8
    //   50: iconst_0
    //   51: istore #9
    //   53: aload #8
    //   55: ifnull -> 72
    //   58: aload #8
    //   60: invokevirtual hasText : ()Z
    //   63: ifeq -> 72
    //   66: iconst_1
    //   67: istore #4
    //   69: goto -> 75
    //   72: iconst_0
    //   73: istore #4
    //   75: iconst_0
    //   76: istore #6
    //   78: iload #6
    //   80: istore_3
    //   81: iload_2
    //   82: ifle -> 154
    //   85: iload #4
    //   87: ifeq -> 98
    //   90: iload #6
    //   92: istore_3
    //   93: iload_2
    //   94: iconst_2
    //   95: if_icmplt -> 154
    //   98: iload_1
    //   99: iload_2
    //   100: imul
    //   101: ldc -2147483648
    //   103: invokestatic makeMeasureSpec : (II)I
    //   106: istore_2
    //   107: aload_0
    //   108: iload_2
    //   109: iload #7
    //   111: invokevirtual measure : (II)V
    //   114: aload_0
    //   115: invokevirtual getMeasuredWidth : ()I
    //   118: istore #6
    //   120: iload #6
    //   122: iload_1
    //   123: idiv
    //   124: istore_3
    //   125: iload_3
    //   126: istore_2
    //   127: iload #6
    //   129: iload_1
    //   130: irem
    //   131: ifeq -> 138
    //   134: iload_3
    //   135: iconst_1
    //   136: iadd
    //   137: istore_2
    //   138: iload_2
    //   139: istore_3
    //   140: iload #4
    //   142: ifeq -> 154
    //   145: iload_2
    //   146: istore_3
    //   147: iload_2
    //   148: iconst_2
    //   149: if_icmpge -> 154
    //   152: iconst_2
    //   153: istore_3
    //   154: iload #9
    //   156: istore #10
    //   158: aload #5
    //   160: getfield isOverflowButton : Z
    //   163: ifne -> 178
    //   166: iload #9
    //   168: istore #10
    //   170: iload #4
    //   172: ifeq -> 178
    //   175: iconst_1
    //   176: istore #10
    //   178: aload #5
    //   180: iload #10
    //   182: putfield expandable : Z
    //   185: aload #5
    //   187: iload_3
    //   188: putfield cellsUsed : I
    //   191: aload_0
    //   192: iload_3
    //   193: iload_1
    //   194: imul
    //   195: ldc 1073741824
    //   197: invokestatic makeMeasureSpec : (II)I
    //   200: iload #7
    //   202: invokevirtual measure : (II)V
    //   205: iload_3
    //   206: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #401	-> 0
    //   #403	-> 9
    //   #405	-> 15
    //   #406	-> 20
    //   #408	-> 31
    //   #409	-> 38
    //   #410	-> 50
    //   #412	-> 75
    //   #413	-> 78
    //   #414	-> 98
    //   #416	-> 107
    //   #418	-> 114
    //   #419	-> 120
    //   #420	-> 125
    //   #421	-> 138
    //   #424	-> 154
    //   #425	-> 178
    //   #427	-> 185
    //   #428	-> 191
    //   #429	-> 191
    //   #431	-> 205
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!this.mFormatItems) {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    } 
    int i = getChildCount();
    int j = (paramInt4 - paramInt2) / 2;
    int k = getDividerWidth();
    paramInt2 = 0;
    int m = 0;
    int n = 0;
    paramInt4 = paramInt3 - paramInt1 - getPaddingRight() - getPaddingLeft();
    int i1 = 0;
    paramBoolean = isLayoutRtl();
    int i2;
    for (i2 = 0; i2 < i; i2++) {
      View view = getChildAt(i2);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams.isOverflowButton) {
          int i4;
          i1 = view.getMeasuredWidth();
          paramInt2 = i1;
          if (hasDividerBeforeChildAt(i2))
            paramInt2 = i1 + k; 
          int i3 = view.getMeasuredHeight();
          if (paramBoolean) {
            i4 = getPaddingLeft() + layoutParams.leftMargin;
            i1 = i4 + paramInt2;
          } else {
            i1 = getWidth() - getPaddingRight() - layoutParams.rightMargin;
            i4 = i1 - paramInt2;
          } 
          int i5 = j - i3 / 2;
          view.layout(i4, i5, i1, i5 + i3);
          paramInt4 -= paramInt2;
          i1 = 1;
        } else {
          int i3 = view.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin;
          int i4 = m + i3;
          paramInt4 -= i3;
          m = i4;
          if (hasDividerBeforeChildAt(i2))
            m = i4 + k; 
          n++;
        } 
      } 
    } 
    if (i == 1 && i1 == 0) {
      View view = getChildAt(0);
      paramInt2 = view.getMeasuredWidth();
      paramInt4 = view.getMeasuredHeight();
      paramInt1 = (paramInt3 - paramInt1) / 2;
      paramInt1 -= paramInt2 / 2;
      paramInt3 = j - paramInt4 / 2;
      view.layout(paramInt1, paramInt3, paramInt1 + paramInt2, paramInt3 + paramInt4);
      return;
    } 
    paramInt1 = n - (i1 ^ 0x1);
    if (paramInt1 > 0) {
      paramInt1 = paramInt4 / paramInt1;
    } else {
      paramInt1 = 0;
    } 
    m = Math.max(0, paramInt1);
    if (paramBoolean) {
      paramInt4 = getWidth() - getPaddingRight();
      for (paramInt1 = 0, paramInt3 = k; paramInt1 < i; paramInt1++) {
        View view = getChildAt(paramInt1);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (view.getVisibility() != 8 && !layoutParams.isOverflowButton) {
          paramInt4 -= layoutParams.rightMargin;
          n = view.getMeasuredWidth();
          i1 = view.getMeasuredHeight();
          i2 = j - i1 / 2;
          view.layout(paramInt4 - n, i2, paramInt4, i2 + i1);
          paramInt4 -= layoutParams.leftMargin + n + m;
        } 
      } 
    } else {
      paramInt3 = getPaddingLeft();
      for (paramInt1 = 0; paramInt1 < i; paramInt1++, paramInt3 = paramInt2) {
        View view = getChildAt(paramInt1);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        paramInt2 = paramInt3;
        if (view.getVisibility() != 8)
          if (layoutParams.isOverflowButton) {
            paramInt2 = paramInt3;
          } else {
            paramInt3 += layoutParams.leftMargin;
            paramInt2 = view.getMeasuredWidth();
            paramInt4 = view.getMeasuredHeight();
            i2 = j - paramInt4 / 2;
            view.layout(paramInt3, i2, paramInt3 + paramInt2, i2 + paramInt4);
            paramInt2 = paramInt3 + layoutParams.rightMargin + paramInt2 + m;
          }  
      } 
    } 
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    dismissPopupMenus();
  }
  
  public void setOverflowIcon(Drawable paramDrawable) {
    getMenu();
    this.mPresenter.setOverflowIcon(paramDrawable);
  }
  
  public Drawable getOverflowIcon() {
    getMenu();
    return this.mPresenter.getOverflowIcon();
  }
  
  public boolean isOverflowReserved() {
    return this.mReserveOverflow;
  }
  
  public void setOverflowReserved(boolean paramBoolean) {
    this.mReserveOverflow = paramBoolean;
  }
  
  protected LayoutParams generateDefaultLayoutParams() {
    LayoutParams layoutParams = new LayoutParams(-2, -2);
    layoutParams.gravity = 16;
    return layoutParams;
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    if (paramLayoutParams != null) {
      if (paramLayoutParams instanceof LayoutParams) {
        paramLayoutParams = new LayoutParams((LayoutParams)paramLayoutParams);
      } else {
        paramLayoutParams = new LayoutParams(paramLayoutParams);
      } 
      if (((LayoutParams)paramLayoutParams).gravity <= 0)
        ((LayoutParams)paramLayoutParams).gravity = 16; 
      return (LayoutParams)paramLayoutParams;
    } 
    return generateDefaultLayoutParams();
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    boolean bool;
    if (paramLayoutParams != null && paramLayoutParams instanceof LayoutParams) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public LayoutParams generateOverflowButtonLayoutParams() {
    LayoutParams layoutParams = generateDefaultLayoutParams();
    layoutParams.isOverflowButton = true;
    return layoutParams;
  }
  
  public boolean invokeItem(MenuItemImpl paramMenuItemImpl) {
    return this.mMenu.performItemAction((MenuItem)paramMenuItemImpl, 0);
  }
  
  public int getWindowAnimations() {
    return 0;
  }
  
  public void initialize(MenuBuilder paramMenuBuilder) {
    this.mMenu = paramMenuBuilder;
  }
  
  public Menu getMenu() {
    if (this.mMenu == null) {
      Context context = getContext();
      MenuBuilder menuBuilder = new MenuBuilder(context);
      menuBuilder.setCallback(new MenuBuilderCallback());
      ActionMenuPresenter actionMenuPresenter2 = new ActionMenuPresenter(context);
      actionMenuPresenter2.setReserveOverflow(true);
      ActionMenuPresenter actionMenuPresenter1 = this.mPresenter;
      MenuPresenter.Callback callback = this.mActionMenuPresenterCallback;
      if (callback == null)
        callback = new ActionMenuPresenterCallback(); 
      actionMenuPresenter1.setCallback(callback);
      this.mMenu.addMenuPresenter((MenuPresenter)this.mPresenter, this.mPopupContext);
      this.mPresenter.setMenuView(this);
    } 
    return (Menu)this.mMenu;
  }
  
  public void setMenuCallbacks(MenuPresenter.Callback paramCallback, MenuBuilder.Callback paramCallback1) {
    this.mActionMenuPresenterCallback = paramCallback;
    this.mMenuBuilderCallback = paramCallback1;
  }
  
  public MenuBuilder peekMenu() {
    return this.mMenu;
  }
  
  public boolean showOverflowMenu() {
    boolean bool;
    ActionMenuPresenter actionMenuPresenter = this.mPresenter;
    if (actionMenuPresenter != null && actionMenuPresenter.showOverflowMenu()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hideOverflowMenu() {
    boolean bool;
    ActionMenuPresenter actionMenuPresenter = this.mPresenter;
    if (actionMenuPresenter != null && actionMenuPresenter.hideOverflowMenu()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isOverflowMenuShowing() {
    boolean bool;
    ActionMenuPresenter actionMenuPresenter = this.mPresenter;
    if (actionMenuPresenter != null && actionMenuPresenter.isOverflowMenuShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isOverflowMenuShowPending() {
    boolean bool;
    ActionMenuPresenter actionMenuPresenter = this.mPresenter;
    if (actionMenuPresenter != null && actionMenuPresenter.isOverflowMenuShowPending()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void dismissPopupMenus() {
    ActionMenuPresenter actionMenuPresenter = this.mPresenter;
    if (actionMenuPresenter != null)
      actionMenuPresenter.dismissPopupMenus(); 
  }
  
  protected boolean hasDividerBeforeChildAt(int paramInt) {
    boolean bool;
    if (paramInt == 0)
      return false; 
    View view1 = getChildAt(paramInt - 1);
    View view2 = getChildAt(paramInt);
    int i = 0;
    int j = i;
    if (paramInt < getChildCount()) {
      j = i;
      if (view1 instanceof ActionMenuChildView)
        j = false | ((ActionMenuChildView)view1).needsDividerAfter(); 
    } 
    i = j;
    if (paramInt > 0) {
      i = j;
      if (view2 instanceof ActionMenuChildView)
        bool = j | ((ActionMenuChildView)view2).needsDividerBefore(); 
    } 
    return bool;
  }
  
  public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    return false;
  }
  
  public void setExpandedActionViewsExclusive(boolean paramBoolean) {
    this.mPresenter.setExpandedActionViewsExclusive(paramBoolean);
  }
  
  class MenuBuilderCallback implements MenuBuilder.Callback {
    final ActionMenuView this$0;
    
    private MenuBuilderCallback() {}
    
    public boolean onMenuItemSelected(MenuBuilder param1MenuBuilder, MenuItem param1MenuItem) {
      if (ActionMenuView.this.mOnMenuItemClickListener != null) {
        ActionMenuView actionMenuView = ActionMenuView.this;
        if (actionMenuView.mOnMenuItemClickListener.onMenuItemClick(param1MenuItem))
          return true; 
      } 
      return false;
    }
    
    public void onMenuModeChange(MenuBuilder param1MenuBuilder) {
      if (ActionMenuView.this.mMenuBuilderCallback != null)
        ActionMenuView.this.mMenuBuilderCallback.onMenuModeChange(param1MenuBuilder); 
    }
  }
  
  class ActionMenuPresenterCallback implements MenuPresenter.Callback {
    final ActionMenuView this$0;
    
    private ActionMenuPresenterCallback() {}
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {}
    
    public boolean onOpenSubMenu(MenuBuilder param1MenuBuilder) {
      return false;
    }
  }
  
  class LayoutParams extends LinearLayout.LayoutParams {
    @ExportedProperty(category = "layout")
    public int cellsUsed;
    
    @ExportedProperty(category = "layout")
    public boolean expandable;
    
    public boolean expanded;
    
    @ExportedProperty(category = "layout")
    public int extraPixels;
    
    @ExportedProperty(category = "layout")
    public boolean isOverflowButton;
    
    @ExportedProperty(category = "layout")
    public boolean preventEdgeOffset;
    
    public LayoutParams(ActionMenuView this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(ActionMenuView this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public LayoutParams(ActionMenuView this$0) {
      super((LinearLayout.LayoutParams)this$0);
      this.isOverflowButton = ((LayoutParams)this$0).isOverflowButton;
    }
    
    public LayoutParams(ActionMenuView this$0, int param1Int1) {
      super(this$0, param1Int1);
      this.isOverflowButton = false;
    }
    
    public LayoutParams(ActionMenuView this$0, int param1Int1, boolean param1Boolean) {
      super(this$0, param1Int1);
      this.isOverflowButton = param1Boolean;
    }
    
    protected void encodeProperties(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      super.encodeProperties(param1ViewHierarchyEncoder);
      param1ViewHierarchyEncoder.addProperty("layout:overFlowButton", this.isOverflowButton);
      param1ViewHierarchyEncoder.addProperty("layout:cellsUsed", this.cellsUsed);
      param1ViewHierarchyEncoder.addProperty("layout:extraPixels", this.extraPixels);
      param1ViewHierarchyEncoder.addProperty("layout:expandable", this.expandable);
      param1ViewHierarchyEncoder.addProperty("layout:preventEdgeOffset", this.preventEdgeOffset);
    }
  }
  
  class ActionMenuChildView {
    public abstract boolean needsDividerAfter();
    
    public abstract boolean needsDividerBefore();
  }
  
  class OnMenuItemClickListener {
    public abstract boolean onMenuItemClick(MenuItem param1MenuItem);
  }
}
