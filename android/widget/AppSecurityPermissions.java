package android.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

@Deprecated
public class AppSecurityPermissions {
  public static View getPermissionItemView(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean) {
    int i;
    LayoutInflater layoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    if (paramBoolean) {
      i = 17302341;
    } else {
      i = 17302855;
    } 
    Drawable drawable = paramContext.getDrawable(i);
    return getPermissionItemViewOld(paramContext, layoutInflater, paramCharSequence1, paramCharSequence2, paramBoolean, drawable);
  }
  
  private static View getPermissionItemViewOld(Context paramContext, LayoutInflater paramLayoutInflater, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean, Drawable paramDrawable) {
    View view = paramLayoutInflater.inflate(17367099, (ViewGroup)null);
    TextView textView2 = view.<TextView>findViewById(16909287);
    TextView textView1 = view.<TextView>findViewById(16909289);
    ImageView imageView = view.<ImageView>findViewById(16909283);
    imageView.setImageDrawable(paramDrawable);
    if (paramCharSequence1 != null) {
      textView2.setText(paramCharSequence1);
      textView1.setText(paramCharSequence2);
    } else {
      textView2.setText(paramCharSequence2);
      textView1.setVisibility(8);
    } 
    return view;
  }
}
