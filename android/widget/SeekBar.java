package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityNodeInfo;

public class SeekBar extends AbsSeekBar {
  private OnSeekBarChangeListener mOnSeekBarChangeListener;
  
  public SeekBar(Context paramContext) {
    this(paramContext, null);
  }
  
  public SeekBar(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842875);
  }
  
  public SeekBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SeekBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  void onProgressRefresh(float paramFloat, boolean paramBoolean, int paramInt) {
    super.onProgressRefresh(paramFloat, paramBoolean, paramInt);
    OnSeekBarChangeListener onSeekBarChangeListener = this.mOnSeekBarChangeListener;
    if (onSeekBarChangeListener != null)
      onSeekBarChangeListener.onProgressChanged(this, paramInt, paramBoolean); 
  }
  
  public void setOnSeekBarChangeListener(OnSeekBarChangeListener paramOnSeekBarChangeListener) {
    this.mOnSeekBarChangeListener = paramOnSeekBarChangeListener;
  }
  
  void onStartTrackingTouch() {
    super.onStartTrackingTouch();
    OnSeekBarChangeListener onSeekBarChangeListener = this.mOnSeekBarChangeListener;
    if (onSeekBarChangeListener != null)
      onSeekBarChangeListener.onStartTrackingTouch(this); 
  }
  
  void onStopTrackingTouch() {
    super.onStopTrackingTouch();
    OnSeekBarChangeListener onSeekBarChangeListener = this.mOnSeekBarChangeListener;
    if (onSeekBarChangeListener != null)
      onSeekBarChangeListener.onStopTrackingTouch(this); 
  }
  
  public CharSequence getAccessibilityClassName() {
    return SeekBar.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (canUserSetProgress())
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS); 
  }
  
  class OnSeekBarChangeListener {
    public abstract void onProgressChanged(SeekBar param1SeekBar, int param1Int, boolean param1Boolean);
    
    public abstract void onStartTrackingTouch(SeekBar param1SeekBar);
    
    public abstract void onStopTrackingTouch(SeekBar param1SeekBar);
  }
}
