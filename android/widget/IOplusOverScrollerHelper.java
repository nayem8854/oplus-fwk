package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusOverScrollerHelper extends IOplusCommonFeature {
  public static final IOplusOverScrollerHelper DEFAULT = (IOplusOverScrollerHelper)new Object();
  
  default IOplusOverScrollerHelper getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusOverScrollerHelper;
  }
  
  default int getFinalX(int paramInt) {
    return paramInt;
  }
  
  default int getFinalY(int paramInt) {
    return paramInt;
  }
  
  default boolean setFriction(float paramFloat) {
    return false;
  }
  
  default boolean isFinished(boolean paramBoolean) {
    return paramBoolean;
  }
  
  default int getCurrX(int paramInt) {
    return paramInt;
  }
  
  default int getCurrY(int paramInt) {
    return paramInt;
  }
}
