package android.widget;

import android.graphics.Rect;
import android.text.Layout;
import android.text.Spannable;

final class AccessibilityIterators {
  class LineTextSegmentIterator extends android.view.AccessibilityIterators.AbstractTextSegmentIterator {
    protected static final int DIRECTION_END = 1;
    
    protected static final int DIRECTION_START = -1;
    
    private static LineTextSegmentIterator sLineInstance;
    
    protected Layout mLayout;
    
    public static LineTextSegmentIterator getInstance() {
      if (sLineInstance == null)
        sLineInstance = new LineTextSegmentIterator(); 
      return sLineInstance;
    }
    
    public void initialize(Spannable param1Spannable, Layout param1Layout) {
      this.mText = param1Spannable.toString();
      this.mLayout = param1Layout;
    }
    
    public int[] following(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int >= this.mText.length())
        return null; 
      if (param1Int < 0) {
        param1Int = this.mLayout.getLineForOffset(0);
      } else {
        i = this.mLayout.getLineForOffset(param1Int);
        if (getLineEdgeIndex(i, -1) == param1Int) {
          param1Int = i;
        } else {
          param1Int = i + 1;
        } 
      } 
      if (param1Int >= this.mLayout.getLineCount())
        return null; 
      i = getLineEdgeIndex(param1Int, -1);
      param1Int = getLineEdgeIndex(param1Int, 1);
      return getRange(i, param1Int + 1);
    }
    
    public int[] preceding(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int <= 0)
        return null; 
      if (param1Int > this.mText.length()) {
        param1Int = this.mLayout.getLineForOffset(this.mText.length());
      } else {
        i = this.mLayout.getLineForOffset(param1Int);
        if (getLineEdgeIndex(i, 1) + 1 == param1Int) {
          param1Int = i;
        } else {
          param1Int = i - 1;
        } 
      } 
      if (param1Int < 0)
        return null; 
      i = getLineEdgeIndex(param1Int, -1);
      param1Int = getLineEdgeIndex(param1Int, 1);
      return getRange(i, param1Int + 1);
    }
    
    protected int getLineEdgeIndex(int param1Int1, int param1Int2) {
      int i = this.mLayout.getParagraphDirection(param1Int1);
      if (param1Int2 * i < 0)
        return this.mLayout.getLineStart(param1Int1); 
      return this.mLayout.getLineEnd(param1Int1) - 1;
    }
  }
  
  class PageTextSegmentIterator extends LineTextSegmentIterator {
    private static PageTextSegmentIterator sPageInstance;
    
    private final Rect mTempRect;
    
    private TextView mView;
    
    PageTextSegmentIterator() {
      this.mTempRect = new Rect();
    }
    
    public static PageTextSegmentIterator getInstance() {
      if (sPageInstance == null)
        sPageInstance = new PageTextSegmentIterator(); 
      return sPageInstance;
    }
    
    public void initialize(TextView param1TextView) {
      initialize((Spannable)param1TextView.getIterableTextForAccessibility(), param1TextView.getLayout());
      this.mView = param1TextView;
    }
    
    public int[] following(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int >= this.mText.length())
        return null; 
      if (!this.mView.getGlobalVisibleRect(this.mTempRect))
        return null; 
      i = Math.max(0, param1Int);
      param1Int = this.mLayout.getLineForOffset(i);
      param1Int = this.mLayout.getLineTop(param1Int);
      int j = this.mTempRect.height(), k = this.mView.getTotalPaddingTop();
      TextView textView = this.mView;
      int m = textView.getTotalPaddingBottom();
      param1Int += j - k - m;
      j = this.mLayout.getLineTop(this.mLayout.getLineCount() - 1);
      if (param1Int < j) {
        param1Int = this.mLayout.getLineForVertical(param1Int);
      } else {
        param1Int = this.mLayout.getLineCount();
      } 
      param1Int = getLineEdgeIndex(param1Int - 1, 1);
      return getRange(i, param1Int + 1);
    }
    
    public int[] preceding(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int <= 0)
        return null; 
      if (!this.mView.getGlobalVisibleRect(this.mTempRect))
        return null; 
      int j = Math.min(this.mText.length(), param1Int);
      int k = this.mLayout.getLineForOffset(j);
      int m = this.mLayout.getLineTop(k);
      i = this.mTempRect.height();
      int n = this.mView.getTotalPaddingTop();
      TextView textView = this.mView;
      param1Int = textView.getTotalPaddingBottom();
      param1Int = m - i - n - param1Int;
      if (param1Int > 0) {
        param1Int = this.mLayout.getLineForVertical(param1Int);
      } else {
        param1Int = 0;
      } 
      i = param1Int;
      if (j == this.mText.length()) {
        i = param1Int;
        if (param1Int < k)
          i = param1Int + 1; 
      } 
      param1Int = getLineEdgeIndex(i, -1);
      return getRange(param1Int, j);
    }
  }
}
