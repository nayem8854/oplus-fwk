package android.widget;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.util.SparseIntArray;
import java.text.Collator;

public class AlphabetIndexer extends DataSetObserver implements SectionIndexer {
  private SparseIntArray mAlphaMap;
  
  protected CharSequence mAlphabet;
  
  private String[] mAlphabetArray;
  
  private int mAlphabetLength;
  
  private Collator mCollator;
  
  protected int mColumnIndex;
  
  protected Cursor mDataCursor;
  
  public AlphabetIndexer(Cursor paramCursor, int paramInt, CharSequence paramCharSequence) {
    int i;
    this.mDataCursor = paramCursor;
    this.mColumnIndex = paramInt;
    this.mAlphabet = paramCharSequence;
    this.mAlphabetLength = paramInt = paramCharSequence.length();
    this.mAlphabetArray = new String[paramInt];
    paramInt = 0;
    while (true) {
      i = this.mAlphabetLength;
      if (paramInt < i) {
        this.mAlphabetArray[paramInt] = Character.toString(this.mAlphabet.charAt(paramInt));
        paramInt++;
        continue;
      } 
      break;
    } 
    this.mAlphaMap = new SparseIntArray(i);
    if (paramCursor != null)
      paramCursor.registerDataSetObserver(this); 
    Collator collator = Collator.getInstance();
    collator.setStrength(0);
  }
  
  public Object[] getSections() {
    return (Object[])this.mAlphabetArray;
  }
  
  public void setCursor(Cursor paramCursor) {
    Cursor cursor = this.mDataCursor;
    if (cursor != null)
      cursor.unregisterDataSetObserver(this); 
    this.mDataCursor = paramCursor;
    if (paramCursor != null)
      paramCursor.registerDataSetObserver(this); 
    this.mAlphaMap.clear();
  }
  
  protected int compare(String paramString1, String paramString2) {
    if (paramString1.length() == 0) {
      paramString1 = " ";
    } else {
      paramString1 = paramString1.substring(0, 1);
    } 
    return this.mCollator.compare(paramString1, paramString2);
  }
  
  public int getPositionForSection(int paramInt) {
    SparseIntArray sparseIntArray = this.mAlphaMap;
    Cursor cursor = this.mDataCursor;
    if (cursor == null || this.mAlphabet == null)
      return 0; 
    if (paramInt <= 0)
      return 0; 
    int i = this.mAlphabetLength, j = paramInt;
    if (paramInt >= i)
      j = i - 1; 
    int k = cursor.getPosition();
    int m = cursor.getCount();
    paramInt = 0;
    i = m;
    char c = this.mAlphabet.charAt(j);
    String str = Character.toString(c);
    int n = sparseIntArray.get(c, -2147483648);
    if (Integer.MIN_VALUE != n)
      if (n < 0) {
        i = -n;
      } else {
        return n;
      }  
    n = paramInt;
    if (j > 0) {
      CharSequence charSequence = this.mAlphabet;
      n = charSequence.charAt(j - 1);
      j = sparseIntArray.get(n, -2147483648);
      n = paramInt;
      if (j != Integer.MIN_VALUE)
        n = Math.abs(j); 
    } 
    paramInt = (i + n) / 2;
    while (true) {
      j = paramInt;
      if (paramInt < i) {
        cursor.moveToPosition(paramInt);
        String str1 = cursor.getString(this.mColumnIndex);
        if (str1 == null) {
          if (paramInt == 0) {
            j = paramInt;
            break;
          } 
          paramInt--;
          continue;
        } 
        j = compare(str1, str);
        if (j != 0) {
          if (j < 0) {
            j = paramInt + 1;
            n = j;
            paramInt = i;
            if (j >= m) {
              j = m;
              break;
            } 
          } 
        } else if (n == paramInt) {
          j = paramInt;
          break;
        } 
        j = (n + paramInt) / 2;
        i = paramInt;
        paramInt = j;
        continue;
      } 
      break;
    } 
    sparseIntArray.put(c, j);
    cursor.moveToPosition(k);
    return j;
  }
  
  public int getSectionForPosition(int paramInt) {
    int i = this.mDataCursor.getPosition();
    this.mDataCursor.moveToPosition(paramInt);
    String str = this.mDataCursor.getString(this.mColumnIndex);
    this.mDataCursor.moveToPosition(i);
    for (paramInt = 0; paramInt < this.mAlphabetLength; paramInt++) {
      char c = this.mAlphabet.charAt(paramInt);
      String str1 = Character.toString(c);
      if (compare(str, str1) == 0)
        return paramInt; 
    } 
    return 0;
  }
  
  public void onChanged() {
    super.onChanged();
    this.mAlphaMap.clear();
  }
  
  public void onInvalidated() {
    super.onInvalidated();
    this.mAlphaMap.clear();
  }
}
