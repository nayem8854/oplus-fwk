package android.widget;

import android.R;
import android.app.PendingIntent;
import android.app.RemoteAction;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.UndoManager;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.BaseCanvas;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.Insets;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.icu.text.DecimalFormatSymbols;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ParcelableParcel;
import android.os.Process;
import android.os.SystemClock;
import android.os.UserHandle;
import android.os.customize.OplusCustomizeRestrictionManager;
import android.provider.Settings;
import android.text.BoringLayout;
import android.text.Editable;
import android.text.GetChars;
import android.text.GraphicsOperations;
import android.text.InputFilter;
import android.text.Layout;
import android.text.ParcelableSpan;
import android.text.PrecomputedText;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.AllCapsTransformationMethod;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.DateKeyListener;
import android.text.method.DateTimeKeyListener;
import android.text.method.DialerKeyListener;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.text.method.LinkMovementMethod;
import android.text.method.MetaKeyKeyListener;
import android.text.method.MovementMethod;
import android.text.method.SingleLineTransformationMethod;
import android.text.method.TextKeyListener;
import android.text.method.TimeKeyListener;
import android.text.method.TransformationMethod;
import android.text.method.WordIterator;
import android.text.style.ClickableSpan;
import android.text.style.SpellCheckSpan;
import android.text.style.SuggestionSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.IntArray;
import android.util.Log;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.AccessibilityIterators;
import android.view.ActionMode;
import android.view.Choreographer;
import android.view.ContextMenu;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug.CapturedViewProperty;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewDebug.IntToString;
import android.view.ViewHierarchyEncoder;
import android.view.ViewParent;
import android.view.ViewRootImpl;
import android.view.ViewStructure;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AnimationUtils;
import android.view.autofill.AutofillManager;
import android.view.autofill.AutofillValue;
import android.view.autofill.Helper;
import android.view.contentcapture.ContentCaptureManager;
import android.view.contentcapture.ContentCaptureSession;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.textclassifier.TextClassification;
import android.view.textclassifier.TextClassificationContext;
import android.view.textclassifier.TextClassificationManager;
import android.view.textclassifier.TextClassifier;
import android.view.textclassifier.TextLinks;
import android.view.textservice.SpellCheckerSubtype;
import android.view.textservice.TextServicesManager;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.util.FastMath;
import com.android.internal.util.Preconditions;
import com.android.internal.widget.EditableInputConnection;
import com.oplus.font.IOplusFontManager;
import com.oplus.neuron.NeuronSystemManager;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import libcore.util.EmptyArray;
import org.xmlpull.v1.XmlPullParserException;

@RemoteView
public class TextView extends View implements ViewTreeObserver.OnPreDrawListener {
  static final int ACCESSIBILITY_ACTION_PROCESS_TEXT_START_ID = 268435712;
  
  private static final int ACCESSIBILITY_ACTION_SHARE = 268435456;
  
  private static final int ANIMATED_SCROLL_GAP = 250;
  
  public static final int AUTO_SIZE_TEXT_TYPE_NONE = 0;
  
  public static final int AUTO_SIZE_TEXT_TYPE_UNIFORM = 1;
  
  private static final int CHANGE_WATCHER_PRIORITY = 100;
  
  static final boolean DEBUG_CURSOR = false;
  
  static final boolean DEBUG_EXTRACT = false;
  
  private static final int DECIMAL = 4;
  
  private static final int DEFAULT_AUTO_SIZE_GRANULARITY_IN_PX = 1;
  
  private static final int DEFAULT_AUTO_SIZE_MAX_TEXT_SIZE_IN_SP = 112;
  
  private static final int DEFAULT_AUTO_SIZE_MIN_TEXT_SIZE_IN_SP = 12;
  
  private static final int DEFAULT_TYPEFACE = -1;
  
  private static final int DEVICE_PROVISIONED_NO = 1;
  
  private static final int DEVICE_PROVISIONED_UNKNOWN = 0;
  
  private static final int DEVICE_PROVISIONED_YES = 2;
  
  private static final int ELLIPSIZE_END = 3;
  
  private static final int ELLIPSIZE_MARQUEE = 4;
  
  private static final int ELLIPSIZE_MIDDLE = 2;
  
  private static final int ELLIPSIZE_NONE = 0;
  
  private static final int ELLIPSIZE_NOT_SET = -1;
  
  private static final int ELLIPSIZE_START = 1;
  
  private static final Spanned EMPTY_SPANNED;
  
  private static final int EMS = 1;
  
  private static final int FLOATING_TOOLBAR_SELECT_ALL_REFRESH_DELAY = 500;
  
  static final int ID_ASSIST = 16908353;
  
  static final int ID_AUTOFILL = 16908355;
  
  static final int ID_COPY = 16908321;
  
  static final int ID_CUT = 16908320;
  
  static final int ID_PASTE = 16908322;
  
  static final int ID_PASTE_AS_PLAIN_TEXT = 16908337;
  
  static final int ID_REDO = 16908339;
  
  static final int ID_REPLACE = 16908340;
  
  static final int ID_SELECT_ALL = 16908319;
  
  static final int ID_SHARE = 16908341;
  
  static final int ID_UNDO = 16908338;
  
  private static final int KEY_DOWN_HANDLED_BY_KEY_LISTENER = 1;
  
  private static final int KEY_DOWN_HANDLED_BY_MOVEMENT_METHOD = 2;
  
  private static final int KEY_EVENT_HANDLED = -1;
  
  private static final int KEY_EVENT_NOT_HANDLED = 0;
  
  private static final int LINES = 1;
  
  static final String LOG_TAG = "TextView";
  
  private static final int MARQUEE_FADE_NORMAL = 0;
  
  private static final int MARQUEE_FADE_SWITCH_SHOW_ELLIPSIS = 1;
  
  private static final int MARQUEE_FADE_SWITCH_SHOW_FADE = 2;
  
  private static final int MONOSPACE = 3;
  
  private static final int[] MULTILINE_STATE_SET;
  
  private static final InputFilter[] NO_FILTERS;
  
  private static final int NO_POINTER_ID = -1;
  
  private static final int PIXELS = 2;
  
  static final int PROCESS_TEXT_REQUEST_CODE = 100;
  
  private static final int SANS = 1;
  
  private static final int SERIF = 2;
  
  private static final int SIGNED = 2;
  
  private static final float[] TEMP_POSITION = new float[2];
  
  private static final RectF TEMP_RECTF = new RectF();
  
  public static final BoringLayout.Metrics UNKNOWN_BORING;
  
  private static final float UNSET_AUTO_SIZE_UNIFORM_CONFIGURATION_VALUE = -1.0F;
  
  static final int VERY_WIDE = 1048576;
  
  private static OplusCustomizeRestrictionManager mOplusCustomizeRestrictionManager;
  
  private static final SparseIntArray sAppearanceValues;
  
  static long sLastCutCopyOrTextChangedTime;
  
  private boolean mAllowTransformationLengthChange;
  
  private int mAutoLinkMask;
  
  private float mAutoSizeMaxTextSizeInPx;
  
  private float mAutoSizeMinTextSizeInPx;
  
  private float mAutoSizeStepGranularityInPx;
  
  private int[] mAutoSizeTextSizesInPx;
  
  private int mAutoSizeTextType;
  
  private BoringLayout.Metrics mBoring;
  
  private int mBreakStrategy;
  
  private BufferType mBufferType;
  
  private ChangeWatcher mChangeWatcher;
  
  private CharWrapper mCharWrapper;
  
  private int mCurHintTextColor;
  
  @ExportedProperty(category = "text")
  private int mCurTextColor;
  
  private volatile Locale mCurrentSpellCheckerLocaleCache;
  
  private Drawable mCursorDrawable;
  
  int mCursorDrawableRes;
  
  private int mDeferScroll;
  
  private int mDesiredHeightAtMeasure;
  
  private int mDeviceProvisionedState;
  
  Drawables mDrawables;
  
  private Editable.Factory mEditableFactory;
  
  private Editor mEditor;
  
  private TextUtils.TruncateAt mEllipsize;
  
  private InputFilter[] mFilters;
  
  private boolean mFreezesText;
  
  @ExportedProperty(category = "text")
  private int mGravity;
  
  private boolean mHasPresetAutoSizeValues;
  
  int mHighlightColor;
  
  private final Paint mHighlightPaint;
  
  private Path mHighlightPath;
  
  private boolean mHighlightPathBogus;
  
  private CharSequence mHint;
  
  private BoringLayout.Metrics mHintBoring;
  
  private int mHintId;
  
  private Layout mHintLayout;
  
  private ColorStateList mHintTextColor;
  
  private boolean mHorizontallyScrolling;
  
  private int mHyphenationFrequency;
  
  private boolean mIncludePad;
  
  private boolean mIsPrimePointerFromHandleView;
  
  private int mJustificationMode;
  
  private int mLastLayoutDirection;
  
  private long mLastScroll;
  
  private Layout mLayout;
  
  private ColorStateList mLinkTextColor;
  
  private boolean mLinksClickable;
  
  private boolean mListenerChanged;
  
  private ArrayList<TextWatcher> mListeners;
  
  private boolean mLocalesChanged;
  
  private Marquee mMarquee;
  
  private int mMarqueeFadeMode;
  
  private int mMarqueeRepeatLimit;
  
  private int mMaxMode;
  
  private int mMaxWidth;
  
  private int mMaxWidthMode;
  
  private int mMaximum;
  
  private int mMinMode;
  
  private int mMinWidth;
  
  private int mMinWidthMode;
  
  private int mMinimum;
  
  private MovementMethod mMovement;
  
  private boolean mNeedsAutoSizeText;
  
  private int mOldMaxMode;
  
  private int mOldMaximum;
  
  private boolean mPreDrawListenerDetached;
  
  private boolean mPreDrawRegistered;
  
  private PrecomputedText mPrecomputed;
  
  private boolean mPreventDefaultMovement;
  
  private int mPrimePointerId;
  
  private boolean mRestartMarquee;
  
  private BoringLayout mSavedHintLayout;
  
  private BoringLayout mSavedLayout;
  
  private Layout mSavedMarqueeModeLayout;
  
  private Scroller mScroller;
  
  private int mShadowColor;
  
  private float mShadowDx;
  
  private float mShadowDy;
  
  private float mShadowRadius;
  
  private boolean mSingleLine;
  
  private float mSpacingAdd;
  
  private float mSpacingMult;
  
  private Spannable mSpannable;
  
  private Spannable.Factory mSpannableFactory;
  
  private Rect mTempRect;
  
  private TextPaint mTempTextPaint;
  
  @ExportedProperty(category = "text")
  private CharSequence mText;
  
  private TextClassificationContext mTextClassificationContext;
  
  private TextClassifier mTextClassificationSession;
  
  private TextClassifier mTextClassifier;
  
  private ColorStateList mTextColor;
  
  private TextDirectionHeuristic mTextDir;
  
  int mTextEditSuggestionContainerLayout;
  
  int mTextEditSuggestionHighlightStyle;
  
  int mTextEditSuggestionItemLayout;
  
  private int mTextId;
  
  private UserHandle mTextOperationUser;
  
  private final TextPaint mTextPaint;
  
  private Drawable mTextSelectHandle;
  
  private Drawable mTextSelectHandleLeft;
  
  int mTextSelectHandleLeftRes;
  
  int mTextSelectHandleRes;
  
  private Drawable mTextSelectHandleRight;
  
  int mTextSelectHandleRightRes;
  
  private boolean mTextSetFromXmlOrResourceId;
  
  private int mTextSizeUnit;
  
  private TransformationMethod mTransformation;
  
  private CharSequence mTransformed;
  
  boolean mUseFallbackLineSpacing;
  
  private final boolean mUseInternationalizedInput;
  
  private boolean mUserSetTextScaleX;
  
  static {
    NO_FILTERS = new InputFilter[0];
    EMPTY_SPANNED = new SpannedString("");
    MULTILINE_STATE_SET = new int[] { 16843597 };
    SparseIntArray sparseIntArray = new SparseIntArray();
    sparseIntArray.put(6, 4);
    sAppearanceValues.put(5, 3);
    sAppearanceValues.put(7, 5);
    sAppearanceValues.put(8, 6);
    sAppearanceValues.put(2, 0);
    sAppearanceValues.put(96, 19);
    sAppearanceValues.put(3, 1);
    sAppearanceValues.put(75, 12);
    sAppearanceValues.put(4, 2);
    sAppearanceValues.put(95, 18);
    sAppearanceValues.put(72, 11);
    sAppearanceValues.put(36, 7);
    sAppearanceValues.put(37, 8);
    sAppearanceValues.put(38, 9);
    sAppearanceValues.put(39, 10);
    sAppearanceValues.put(76, 13);
    sAppearanceValues.put(91, 17);
    sAppearanceValues.put(77, 14);
    sAppearanceValues.put(78, 15);
    sAppearanceValues.put(90, 16);
    UNKNOWN_BORING = new BoringLayout.Metrics();
  }
  
  class Drawables {
    static final int BOTTOM = 3;
    
    static final int DRAWABLE_LEFT = 1;
    
    static final int DRAWABLE_NONE = -1;
    
    static final int DRAWABLE_RIGHT = 0;
    
    static final int LEFT = 0;
    
    static final int RIGHT = 2;
    
    static final int TOP = 1;
    
    BlendMode mBlendMode;
    
    final Rect mCompoundRect;
    
    Drawable mDrawableEnd;
    
    Drawable mDrawableError;
    
    int mDrawableHeightEnd;
    
    int mDrawableHeightError;
    
    int mDrawableHeightLeft;
    
    int mDrawableHeightRight;
    
    int mDrawableHeightStart;
    
    int mDrawableHeightTemp;
    
    Drawable mDrawableLeftInitial;
    
    int mDrawablePadding;
    
    Drawable mDrawableRightInitial;
    
    int mDrawableSaved;
    
    int mDrawableSizeBottom;
    
    int mDrawableSizeEnd;
    
    int mDrawableSizeError;
    
    int mDrawableSizeLeft;
    
    int mDrawableSizeRight;
    
    int mDrawableSizeStart;
    
    int mDrawableSizeTemp;
    
    int mDrawableSizeTop;
    
    Drawable mDrawableStart;
    
    Drawable mDrawableTemp;
    
    int mDrawableWidthBottom;
    
    int mDrawableWidthTop;
    
    boolean mHasTint;
    
    boolean mHasTintMode;
    
    boolean mIsRtlCompatibilityMode;
    
    boolean mOverride;
    
    final Drawable[] mShowing;
    
    ColorStateList mTintList;
    
    public Drawables(TextView this$0) {
      boolean bool;
      this.mCompoundRect = new Rect();
      this.mShowing = new Drawable[4];
      this.mDrawableSaved = -1;
      int i = (this$0.getApplicationInfo()).targetSdkVersion;
      if (i < 17 || !this$0.getApplicationInfo().hasRtlSupport()) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mIsRtlCompatibilityMode = bool;
      this.mOverride = false;
    }
    
    public boolean hasMetadata() {
      return (this.mDrawablePadding != 0 || this.mHasTintMode || this.mHasTint);
    }
    
    public boolean resolveWithLayoutDirection(int param1Int) {
      Drawable[] arrayOfDrawable = this.mShowing;
      boolean bool = false;
      Drawable drawable1 = arrayOfDrawable[0];
      Drawable drawable2 = arrayOfDrawable[2];
      arrayOfDrawable[0] = this.mDrawableLeftInitial;
      arrayOfDrawable[2] = this.mDrawableRightInitial;
      if (this.mIsRtlCompatibilityMode) {
        Drawable drawable = this.mDrawableStart;
        if (drawable != null && arrayOfDrawable[0] == null) {
          arrayOfDrawable[0] = drawable;
          this.mDrawableSizeLeft = this.mDrawableSizeStart;
          this.mDrawableHeightLeft = this.mDrawableHeightStart;
        } 
        drawable = this.mDrawableEnd;
        if (drawable != null) {
          arrayOfDrawable = this.mShowing;
          if (arrayOfDrawable[2] == null) {
            arrayOfDrawable[2] = drawable;
            this.mDrawableSizeRight = this.mDrawableSizeEnd;
            this.mDrawableHeightRight = this.mDrawableHeightEnd;
          } 
        } 
      } else if (param1Int != 1) {
        if (this.mOverride) {
          arrayOfDrawable[0] = this.mDrawableStart;
          this.mDrawableSizeLeft = this.mDrawableSizeStart;
          this.mDrawableHeightLeft = this.mDrawableHeightStart;
          arrayOfDrawable[2] = this.mDrawableEnd;
          this.mDrawableSizeRight = this.mDrawableSizeEnd;
          this.mDrawableHeightRight = this.mDrawableHeightEnd;
        } 
      } else if (this.mOverride) {
        arrayOfDrawable[2] = this.mDrawableStart;
        this.mDrawableSizeRight = this.mDrawableSizeStart;
        this.mDrawableHeightRight = this.mDrawableHeightStart;
        arrayOfDrawable[0] = this.mDrawableEnd;
        this.mDrawableSizeLeft = this.mDrawableSizeEnd;
        this.mDrawableHeightLeft = this.mDrawableHeightEnd;
      } 
      applyErrorDrawableIfNeeded(param1Int);
      arrayOfDrawable = this.mShowing;
      if (arrayOfDrawable[0] != drawable1 || arrayOfDrawable[2] != drawable2)
        bool = true; 
      return bool;
    }
    
    public void setErrorDrawable(Drawable param1Drawable, TextView param1TextView) {
      Drawable drawable = this.mDrawableError;
      if (drawable != param1Drawable && drawable != null)
        drawable.setCallback(null); 
      this.mDrawableError = param1Drawable;
      if (param1Drawable != null) {
        Rect rect = this.mCompoundRect;
        int[] arrayOfInt = param1TextView.getDrawableState();
        this.mDrawableError.setState(arrayOfInt);
        this.mDrawableError.copyBounds(rect);
        this.mDrawableError.setCallback(param1TextView);
        this.mDrawableSizeError = rect.width();
        this.mDrawableHeightError = rect.height();
      } else {
        this.mDrawableHeightError = 0;
        this.mDrawableSizeError = 0;
      } 
    }
    
    private void applyErrorDrawableIfNeeded(int param1Int) {
      int i = this.mDrawableSaved;
      if (i != 0) {
        if (i == 1) {
          this.mShowing[0] = this.mDrawableTemp;
          this.mDrawableSizeLeft = this.mDrawableSizeTemp;
          this.mDrawableHeightLeft = this.mDrawableHeightTemp;
        } 
      } else {
        this.mShowing[2] = this.mDrawableTemp;
        this.mDrawableSizeRight = this.mDrawableSizeTemp;
        this.mDrawableHeightRight = this.mDrawableHeightTemp;
      } 
      Drawable drawable = this.mDrawableError;
      if (drawable != null)
        if (param1Int != 1) {
          this.mDrawableSaved = 0;
          Drawable[] arrayOfDrawable = this.mShowing;
          this.mDrawableTemp = arrayOfDrawable[2];
          this.mDrawableSizeTemp = this.mDrawableSizeRight;
          this.mDrawableHeightTemp = this.mDrawableHeightRight;
          arrayOfDrawable[2] = drawable;
          this.mDrawableSizeRight = this.mDrawableSizeError;
          this.mDrawableHeightRight = this.mDrawableHeightError;
        } else {
          this.mDrawableSaved = 1;
          Drawable[] arrayOfDrawable = this.mShowing;
          this.mDrawableTemp = arrayOfDrawable[0];
          this.mDrawableSizeTemp = this.mDrawableSizeLeft;
          this.mDrawableHeightTemp = this.mDrawableHeightLeft;
          arrayOfDrawable[0] = drawable;
          this.mDrawableSizeLeft = this.mDrawableSizeError;
          this.mDrawableHeightLeft = this.mDrawableHeightError;
        }  
    }
  }
  
  public static void preloadFontCache() {
    Paint paint = new Paint();
    paint.setAntiAlias(true);
    paint.setTypeface(Typeface.DEFAULT);
    paint.measureText("H");
  }
  
  public TextView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public TextView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842884);
  }
  
  public TextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_2
    //   3: iload_3
    //   4: iload #4
    //   6: invokespecial <init> : (Landroid/content/Context;Landroid/util/AttributeSet;II)V
    //   9: aload_0
    //   10: invokestatic getInstance : ()Landroid/text/Editable$Factory;
    //   13: putfield mEditableFactory : Landroid/text/Editable$Factory;
    //   16: aload_0
    //   17: invokestatic getInstance : ()Landroid/text/Spannable$Factory;
    //   20: putfield mSpannableFactory : Landroid/text/Spannable$Factory;
    //   23: aload_0
    //   24: iconst_3
    //   25: putfield mMarqueeRepeatLimit : I
    //   28: aload_0
    //   29: iconst_m1
    //   30: putfield mLastLayoutDirection : I
    //   33: aload_0
    //   34: iconst_0
    //   35: putfield mMarqueeFadeMode : I
    //   38: aload_0
    //   39: getstatic android/widget/TextView$BufferType.NORMAL : Landroid/widget/TextView$BufferType;
    //   42: putfield mBufferType : Landroid/widget/TextView$BufferType;
    //   45: aload_0
    //   46: iconst_0
    //   47: putfield mLocalesChanged : Z
    //   50: aload_0
    //   51: iconst_m1
    //   52: putfield mTextSizeUnit : I
    //   55: aload_0
    //   56: iconst_0
    //   57: putfield mListenerChanged : Z
    //   60: aload_0
    //   61: ldc_w 8388659
    //   64: putfield mGravity : I
    //   67: aload_0
    //   68: iconst_1
    //   69: putfield mLinksClickable : Z
    //   72: aload_0
    //   73: fconst_1
    //   74: putfield mSpacingMult : F
    //   77: aload_0
    //   78: fconst_0
    //   79: putfield mSpacingAdd : F
    //   82: aload_0
    //   83: ldc_w 2147483647
    //   86: putfield mMaximum : I
    //   89: aload_0
    //   90: iconst_1
    //   91: putfield mMaxMode : I
    //   94: aload_0
    //   95: iconst_0
    //   96: putfield mMinimum : I
    //   99: aload_0
    //   100: iconst_1
    //   101: putfield mMinMode : I
    //   104: aload_0
    //   105: ldc_w 2147483647
    //   108: putfield mOldMaximum : I
    //   111: aload_0
    //   112: iconst_1
    //   113: putfield mOldMaxMode : I
    //   116: aload_0
    //   117: ldc_w 2147483647
    //   120: putfield mMaxWidth : I
    //   123: aload_0
    //   124: iconst_2
    //   125: putfield mMaxWidthMode : I
    //   128: aload_0
    //   129: iconst_0
    //   130: putfield mMinWidth : I
    //   133: aload_0
    //   134: iconst_2
    //   135: putfield mMinWidthMode : I
    //   138: aload_0
    //   139: iconst_m1
    //   140: putfield mDesiredHeightAtMeasure : I
    //   143: aload_0
    //   144: iconst_1
    //   145: putfield mIncludePad : Z
    //   148: aload_0
    //   149: iconst_m1
    //   150: putfield mDeferScroll : I
    //   153: aload_0
    //   154: getstatic android/widget/TextView.NO_FILTERS : [Landroid/text/InputFilter;
    //   157: putfield mFilters : [Landroid/text/InputFilter;
    //   160: aload_0
    //   161: ldc_w 1714664933
    //   164: putfield mHighlightColor : I
    //   167: aload_0
    //   168: iconst_1
    //   169: putfield mHighlightPathBogus : Z
    //   172: aload_0
    //   173: iconst_m1
    //   174: putfield mPrimePointerId : I
    //   177: aload_0
    //   178: iconst_0
    //   179: putfield mDeviceProvisionedState : I
    //   182: aload_0
    //   183: iconst_0
    //   184: putfield mAutoSizeTextType : I
    //   187: aload_0
    //   188: iconst_0
    //   189: putfield mNeedsAutoSizeText : Z
    //   192: aload_0
    //   193: ldc -1.0
    //   195: putfield mAutoSizeStepGranularityInPx : F
    //   198: aload_0
    //   199: ldc -1.0
    //   201: putfield mAutoSizeMinTextSizeInPx : F
    //   204: aload_0
    //   205: ldc -1.0
    //   207: putfield mAutoSizeMaxTextSizeInPx : F
    //   210: aload_0
    //   211: getstatic libcore/util/EmptyArray.INT : [I
    //   214: putfield mAutoSizeTextSizesInPx : [I
    //   217: aload_0
    //   218: iconst_0
    //   219: putfield mHasPresetAutoSizeValues : Z
    //   222: aload_0
    //   223: iconst_0
    //   224: putfield mTextSetFromXmlOrResourceId : Z
    //   227: aload_0
    //   228: iconst_0
    //   229: putfield mTextId : I
    //   232: aload_0
    //   233: iconst_0
    //   234: putfield mHintId : I
    //   237: aload_0
    //   238: invokestatic getInstance : ()Landroid/common/ColorFrameworkFactory;
    //   241: getstatic android/text/ITextJustificationHooks.DEFAULT : Landroid/text/ITextJustificationHooks;
    //   244: iconst_0
    //   245: anewarray java/lang/Object
    //   248: invokevirtual getFeature : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   251: checkcast android/text/ITextJustificationHooks
    //   254: putfield mTextJustificationHooksImpl : Landroid/text/ITextJustificationHooks;
    //   257: aload_0
    //   258: aload_0
    //   259: getfield mLayout : Landroid/text/Layout;
    //   262: putfield mLayout : Landroid/text/Layout;
    //   265: aload_1
    //   266: invokevirtual getApplicationContext : ()Landroid/content/Context;
    //   269: invokestatic getInstance : (Landroid/content/Context;)Landroid/os/customize/OplusCustomizeRestrictionManager;
    //   272: putstatic android/widget/TextView.mOplusCustomizeRestrictionManager : Landroid/os/customize/OplusCustomizeRestrictionManager;
    //   275: aload_0
    //   276: invokevirtual getImportantForAutofill : ()I
    //   279: ifne -> 287
    //   282: aload_0
    //   283: iconst_1
    //   284: invokevirtual setImportantForAutofill : (I)V
    //   287: aload_0
    //   288: invokevirtual getImportantForContentCapture : ()I
    //   291: ifne -> 299
    //   294: aload_0
    //   295: iconst_1
    //   296: invokevirtual setImportantForContentCapture : (I)V
    //   299: aload_0
    //   300: ldc_w ''
    //   303: invokespecial setTextInternal : (Ljava/lang/CharSequence;)V
    //   306: aload_0
    //   307: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   310: astore #5
    //   312: aload #5
    //   314: invokevirtual getCompatibilityInfo : ()Landroid/content/res/CompatibilityInfo;
    //   317: astore #6
    //   319: new android/text/TextPaint
    //   322: dup
    //   323: iconst_1
    //   324: invokespecial <init> : (I)V
    //   327: astore #7
    //   329: aload_0
    //   330: aload #7
    //   332: putfield mTextPaint : Landroid/text/TextPaint;
    //   335: aload #7
    //   337: aload #5
    //   339: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   342: getfield density : F
    //   345: putfield density : F
    //   348: aload_0
    //   349: getfield mTextPaint : Landroid/text/TextPaint;
    //   352: aload #6
    //   354: getfield applicationScale : F
    //   357: invokevirtual setCompatibilityScaling : (F)V
    //   360: new android/graphics/Paint
    //   363: dup
    //   364: iconst_1
    //   365: invokespecial <init> : (I)V
    //   368: astore #7
    //   370: aload_0
    //   371: aload #7
    //   373: putfield mHighlightPaint : Landroid/graphics/Paint;
    //   376: aload #7
    //   378: aload #6
    //   380: getfield applicationScale : F
    //   383: invokevirtual setCompatibilityScaling : (F)V
    //   386: aload_0
    //   387: aload_0
    //   388: invokevirtual getDefaultMovementMethod : ()Landroid/text/method/MovementMethod;
    //   391: putfield mMovement : Landroid/text/method/MovementMethod;
    //   394: aload_0
    //   395: aconst_null
    //   396: putfield mTransformation : Landroid/text/method/TransformationMethod;
    //   399: new android/widget/TextView$TextAppearanceAttributes
    //   402: dup
    //   403: aconst_null
    //   404: invokespecial <init> : (Landroid/widget/TextView$1;)V
    //   407: astore #8
    //   409: aload #8
    //   411: ldc_w -16777216
    //   414: invokestatic valueOf : (I)Landroid/content/res/ColorStateList;
    //   417: putfield mTextColor : Landroid/content/res/ColorStateList;
    //   420: aload #8
    //   422: bipush #15
    //   424: putfield mTextSize : I
    //   427: aload_0
    //   428: iconst_0
    //   429: putfield mBreakStrategy : I
    //   432: aload_0
    //   433: iconst_0
    //   434: putfield mHyphenationFrequency : I
    //   437: aload_0
    //   438: iconst_0
    //   439: putfield mJustificationMode : I
    //   442: ldc_w 'Failure reading input extras'
    //   445: astore #9
    //   447: aload_1
    //   448: invokevirtual getTheme : ()Landroid/content/res/Resources$Theme;
    //   451: astore #5
    //   453: aload #5
    //   455: aload_2
    //   456: getstatic com/android/internal/R$styleable.TextViewAppearance : [I
    //   459: iload_3
    //   460: iload #4
    //   462: invokevirtual obtainStyledAttributes : (Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    //   465: astore #6
    //   467: getstatic com/android/internal/R$styleable.TextViewAppearance : [I
    //   470: astore #7
    //   472: ldc 'TextView'
    //   474: astore #10
    //   476: aload_0
    //   477: aload_1
    //   478: aload #7
    //   480: aload_2
    //   481: aload #6
    //   483: iload_3
    //   484: iload #4
    //   486: invokevirtual saveAttributeDataForStyleable : (Landroid/content/Context;[ILandroid/util/AttributeSet;Landroid/content/res/TypedArray;II)V
    //   489: aload #6
    //   491: iconst_0
    //   492: iconst_m1
    //   493: invokevirtual getResourceId : (II)I
    //   496: istore #11
    //   498: aload #6
    //   500: invokevirtual recycle : ()V
    //   503: iload #11
    //   505: iconst_m1
    //   506: if_icmpeq -> 538
    //   509: aload #5
    //   511: iload #11
    //   513: getstatic com/android/internal/R$styleable.TextAppearance : [I
    //   516: invokevirtual obtainStyledAttributes : (I[I)Landroid/content/res/TypedArray;
    //   519: astore #6
    //   521: aload_0
    //   522: aload_1
    //   523: getstatic com/android/internal/R$styleable.TextAppearance : [I
    //   526: aconst_null
    //   527: aload #6
    //   529: iconst_0
    //   530: iload #11
    //   532: invokevirtual saveAttributeDataForStyleable : (Landroid/content/Context;[ILandroid/util/AttributeSet;Landroid/content/res/TypedArray;II)V
    //   535: goto -> 541
    //   538: aconst_null
    //   539: astore #6
    //   541: aload #6
    //   543: ifnull -> 567
    //   546: aload_0
    //   547: aload_1
    //   548: aload #6
    //   550: aload #8
    //   552: iconst_0
    //   553: invokespecial readTextAppearance : (Landroid/content/Context;Landroid/content/res/TypedArray;Landroid/widget/TextView$TextAppearanceAttributes;Z)V
    //   556: aload #8
    //   558: iconst_0
    //   559: putfield mFontFamilyExplicit : Z
    //   562: aload #6
    //   564: invokevirtual recycle : ()V
    //   567: aload_0
    //   568: invokevirtual getDefaultEditable : ()Z
    //   571: istore #12
    //   573: aconst_null
    //   574: astore #13
    //   576: aconst_null
    //   577: astore #7
    //   579: ldc -1.0
    //   581: fstore #14
    //   583: ldc -1.0
    //   585: fstore #15
    //   587: ldc -1.0
    //   589: fstore #16
    //   591: aload #5
    //   593: aload_2
    //   594: getstatic com/android/internal/R$styleable.TextView : [I
    //   597: iload_3
    //   598: iload #4
    //   600: invokevirtual obtainStyledAttributes : (Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    //   603: astore #17
    //   605: aload_0
    //   606: aload_1
    //   607: getstatic com/android/internal/R$styleable.TextView : [I
    //   610: aload_2
    //   611: aload #17
    //   613: iload_3
    //   614: iload #4
    //   616: invokevirtual saveAttributeDataForStyleable : (Landroid/content/Context;[ILandroid/util/AttributeSet;Landroid/content/res/TypedArray;II)V
    //   619: aload_0
    //   620: aload_1
    //   621: aload #17
    //   623: aload #8
    //   625: iconst_1
    //   626: invokespecial readTextAppearance : (Landroid/content/Context;Landroid/content/res/TypedArray;Landroid/widget/TextView$TextAppearanceAttributes;Z)V
    //   629: aload #17
    //   631: invokevirtual getIndexCount : ()I
    //   634: istore #18
    //   636: aload_0
    //   637: aload_0
    //   638: getfield mTextJustificationHooksImpl : Landroid/text/ITextJustificationHooks;
    //   641: aload_0
    //   642: aload #8
    //   644: getfield mTextSize : I
    //   647: i2f
    //   648: aload_0
    //   649: getfield mSpacingMult : F
    //   652: invokeinterface getTextViewDefaultLineMulti : (Ljava/lang/Object;FF)F
    //   657: putfield mSpacingMult : F
    //   660: iconst_0
    //   661: istore #19
    //   663: iconst_m1
    //   664: istore #20
    //   666: iconst_0
    //   667: istore #11
    //   669: iconst_0
    //   670: istore #21
    //   672: aconst_null
    //   673: astore #22
    //   675: aconst_null
    //   676: astore #23
    //   678: aconst_null
    //   679: astore #24
    //   681: aconst_null
    //   682: astore #25
    //   684: iconst_0
    //   685: istore #26
    //   687: iconst_m1
    //   688: istore #27
    //   690: iconst_0
    //   691: istore #28
    //   693: iconst_m1
    //   694: istore #29
    //   696: ldc_w ''
    //   699: astore #30
    //   701: aconst_null
    //   702: astore #5
    //   704: iconst_0
    //   705: istore #31
    //   707: iconst_0
    //   708: istore #32
    //   710: iconst_0
    //   711: istore #33
    //   713: iconst_0
    //   714: istore #34
    //   716: aconst_null
    //   717: astore #35
    //   719: aconst_null
    //   720: astore #36
    //   722: aconst_null
    //   723: astore #37
    //   725: aconst_null
    //   726: astore #38
    //   728: iconst_m1
    //   729: istore #39
    //   731: iconst_0
    //   732: istore #40
    //   734: iconst_m1
    //   735: istore #41
    //   737: iconst_0
    //   738: istore #42
    //   740: iconst_m1
    //   741: istore #43
    //   743: aload #10
    //   745: astore #6
    //   747: aload #38
    //   749: astore #10
    //   751: iload #19
    //   753: iload #18
    //   755: if_icmpge -> 2409
    //   758: aload #17
    //   760: iload #19
    //   762: invokevirtual getIndex : (I)I
    //   765: istore #44
    //   767: iload #44
    //   769: ifeq -> 2388
    //   772: iload #44
    //   774: bipush #67
    //   776: if_icmpeq -> 2373
    //   779: iload #44
    //   781: bipush #70
    //   783: if_icmpeq -> 2358
    //   786: iload #44
    //   788: bipush #71
    //   790: if_icmpeq -> 2343
    //   793: iload #44
    //   795: bipush #73
    //   797: if_icmpeq -> 2331
    //   800: iload #44
    //   802: bipush #74
    //   804: if_icmpeq -> 2319
    //   807: iload #44
    //   809: bipush #97
    //   811: if_icmpeq -> 2304
    //   814: iload #44
    //   816: bipush #98
    //   818: if_icmpeq -> 2289
    //   821: iload #44
    //   823: tableswitch default -> 944, 9 -> 2275, 10 -> 2260, 11 -> 2245, 12 -> 2230, 13 -> 2215, 14 -> 2200, 15 -> 2185, 16 -> 2170, 17 -> 2156, 18 -> 2129, 19 -> 2105, 20 -> 2090, 21 -> 2068, 22 -> 2053, 23 -> 2038, 24 -> 2023, 25 -> 2008, 26 -> 1993, 27 -> 1978, 28 -> 1963, 29 -> 1948, 30 -> 1926, 31 -> 1912, 32 -> 1898, 33 -> 1884, 34 -> 1862, 35 -> 1849
    //   944: iload #44
    //   946: tableswitch default -> 1060, 40 -> 1835, 41 -> 1823, 42 -> 1809, 43 -> 1797, 44 -> 1783, 45 -> 1769, 46 -> 1755, 47 -> 1740, 48 -> 1728, 49 -> 1716, 50 -> 1704, 51 -> 1692, 52 -> 1678, 53 -> 1658, 54 -> 1640, 55 -> 1622, 56 -> 1609, 57 -> 1595, 58 -> 1550, 59 -> 1509, 60 -> 1478, 61 -> 1437, 62 -> 1422, 63 -> 1407, 64 -> 1392
    //   1060: iload #44
    //   1062: tableswitch default -> 1120, 79 -> 1380, 80 -> 1362, 81 -> 1347, 82 -> 1332, 83 -> 1310, 84 -> 1295, 85 -> 1281, 86 -> 1233, 87 -> 1219, 88 -> 1205, 89 -> 1190
    //   1120: iload #44
    //   1122: tableswitch default -> 1148, 92 -> 1177, 93 -> 1164, 94 -> 1151
    //   1148: goto -> 2403
    //   1151: aload #17
    //   1153: iload #44
    //   1155: iconst_m1
    //   1156: invokevirtual getDimensionPixelSize : (II)I
    //   1159: istore #39
    //   1161: goto -> 2403
    //   1164: aload #17
    //   1166: iload #44
    //   1168: iconst_m1
    //   1169: invokevirtual getDimensionPixelSize : (II)I
    //   1172: istore #41
    //   1174: goto -> 2403
    //   1177: aload #17
    //   1179: iload #44
    //   1181: iconst_m1
    //   1182: invokevirtual getDimensionPixelSize : (II)I
    //   1185: istore #43
    //   1187: goto -> 2403
    //   1190: aload_0
    //   1191: aload #17
    //   1193: iload #44
    //   1195: iconst_0
    //   1196: invokevirtual getInt : (II)I
    //   1199: putfield mJustificationMode : I
    //   1202: goto -> 2403
    //   1205: aload #17
    //   1207: iload #44
    //   1209: ldc -1.0
    //   1211: invokevirtual getDimension : (IF)F
    //   1214: fstore #15
    //   1216: goto -> 2403
    //   1219: aload #17
    //   1221: iload #44
    //   1223: ldc -1.0
    //   1225: invokevirtual getDimension : (IF)F
    //   1228: fstore #14
    //   1230: goto -> 2403
    //   1233: aload #17
    //   1235: iload #44
    //   1237: iconst_0
    //   1238: invokevirtual getResourceId : (II)I
    //   1241: istore #44
    //   1243: iload #44
    //   1245: ifle -> 1278
    //   1248: aload #17
    //   1250: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   1253: astore #38
    //   1255: aload #38
    //   1257: iload #44
    //   1259: invokevirtual obtainTypedArray : (I)Landroid/content/res/TypedArray;
    //   1262: astore #38
    //   1264: aload_0
    //   1265: aload #38
    //   1267: invokespecial setupAutoSizeUniformPresetSizes : (Landroid/content/res/TypedArray;)V
    //   1270: aload #38
    //   1272: invokevirtual recycle : ()V
    //   1275: goto -> 2403
    //   1278: goto -> 2403
    //   1281: aload #17
    //   1283: iload #44
    //   1285: ldc -1.0
    //   1287: invokevirtual getDimension : (IF)F
    //   1290: fstore #16
    //   1292: goto -> 2403
    //   1295: aload_0
    //   1296: aload #17
    //   1298: iload #44
    //   1300: iconst_0
    //   1301: invokevirtual getInt : (II)I
    //   1304: putfield mAutoSizeTextType : I
    //   1307: goto -> 2403
    //   1310: aload_0
    //   1311: invokespecial createEditorIfNeeded : ()V
    //   1314: aload_0
    //   1315: getfield mEditor : Landroid/widget/Editor;
    //   1318: aload #17
    //   1320: iload #44
    //   1322: iconst_1
    //   1323: invokevirtual getBoolean : (IZ)Z
    //   1326: putfield mAllowUndo : Z
    //   1329: goto -> 2403
    //   1332: aload_0
    //   1333: aload #17
    //   1335: iload #44
    //   1337: iconst_0
    //   1338: invokevirtual getInt : (II)I
    //   1341: putfield mHyphenationFrequency : I
    //   1344: goto -> 2403
    //   1347: aload_0
    //   1348: aload #17
    //   1350: iload #44
    //   1352: iconst_0
    //   1353: invokevirtual getInt : (II)I
    //   1356: putfield mBreakStrategy : I
    //   1359: goto -> 2403
    //   1362: aload #17
    //   1364: iload #44
    //   1366: iconst_m1
    //   1367: invokevirtual getInt : (II)I
    //   1370: aload #10
    //   1372: invokestatic parseBlendMode : (ILandroid/graphics/BlendMode;)Landroid/graphics/BlendMode;
    //   1375: astore #10
    //   1377: goto -> 2403
    //   1380: aload #17
    //   1382: iload #44
    //   1384: invokevirtual getColorStateList : (I)Landroid/content/res/ColorStateList;
    //   1387: astore #37
    //   1389: goto -> 2403
    //   1392: aload_0
    //   1393: aload #17
    //   1395: iload #44
    //   1397: iconst_0
    //   1398: invokevirtual getResourceId : (II)I
    //   1401: putfield mTextSelectHandleRes : I
    //   1404: goto -> 2403
    //   1407: aload_0
    //   1408: aload #17
    //   1410: iload #44
    //   1412: iconst_0
    //   1413: invokevirtual getResourceId : (II)I
    //   1416: putfield mTextSelectHandleRightRes : I
    //   1419: goto -> 2403
    //   1422: aload_0
    //   1423: aload #17
    //   1425: iload #44
    //   1427: iconst_0
    //   1428: invokevirtual getResourceId : (II)I
    //   1431: putfield mTextSelectHandleLeftRes : I
    //   1434: goto -> 2403
    //   1437: aload_0
    //   1438: invokespecial createEditorIfNeeded : ()V
    //   1441: aload_0
    //   1442: getfield mEditor : Landroid/widget/Editor;
    //   1445: invokevirtual createInputContentTypeIfNeeded : ()V
    //   1448: aload_0
    //   1449: getfield mEditor : Landroid/widget/Editor;
    //   1452: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   1455: aload #17
    //   1457: iload #44
    //   1459: aload_0
    //   1460: getfield mEditor : Landroid/widget/Editor;
    //   1463: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   1466: getfield imeActionId : I
    //   1469: invokevirtual getInt : (II)I
    //   1472: putfield imeActionId : I
    //   1475: goto -> 2403
    //   1478: aload_0
    //   1479: invokespecial createEditorIfNeeded : ()V
    //   1482: aload_0
    //   1483: getfield mEditor : Landroid/widget/Editor;
    //   1486: invokevirtual createInputContentTypeIfNeeded : ()V
    //   1489: aload_0
    //   1490: getfield mEditor : Landroid/widget/Editor;
    //   1493: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   1496: aload #17
    //   1498: iload #44
    //   1500: invokevirtual getText : (I)Ljava/lang/CharSequence;
    //   1503: putfield imeActionLabel : Ljava/lang/CharSequence;
    //   1506: goto -> 2403
    //   1509: aload_0
    //   1510: invokespecial createEditorIfNeeded : ()V
    //   1513: aload_0
    //   1514: getfield mEditor : Landroid/widget/Editor;
    //   1517: invokevirtual createInputContentTypeIfNeeded : ()V
    //   1520: aload_0
    //   1521: getfield mEditor : Landroid/widget/Editor;
    //   1524: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   1527: aload #17
    //   1529: iload #44
    //   1531: aload_0
    //   1532: getfield mEditor : Landroid/widget/Editor;
    //   1535: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   1538: getfield imeOptions : I
    //   1541: invokevirtual getInt : (II)I
    //   1544: putfield imeOptions : I
    //   1547: goto -> 2403
    //   1550: aload_0
    //   1551: aload #17
    //   1553: iload #44
    //   1555: iconst_0
    //   1556: invokevirtual getResourceId : (II)I
    //   1559: invokevirtual setInputExtras : (I)V
    //   1562: goto -> 1592
    //   1565: astore #38
    //   1567: aload #6
    //   1569: aload #9
    //   1571: aload #38
    //   1573: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1576: pop
    //   1577: goto -> 2403
    //   1580: astore #38
    //   1582: aload #6
    //   1584: aload #9
    //   1586: aload #38
    //   1588: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1591: pop
    //   1592: goto -> 2403
    //   1595: aload_0
    //   1596: aload #17
    //   1598: iload #44
    //   1600: invokevirtual getString : (I)Ljava/lang/String;
    //   1603: invokevirtual setPrivateImeOptions : (Ljava/lang/String;)V
    //   1606: goto -> 2403
    //   1609: aload #17
    //   1611: iload #44
    //   1613: iconst_0
    //   1614: invokevirtual getInt : (II)I
    //   1617: istore #32
    //   1619: goto -> 2403
    //   1622: aload_0
    //   1623: aload #17
    //   1625: iload #44
    //   1627: aload_0
    //   1628: getfield mMarqueeRepeatLimit : I
    //   1631: invokevirtual getInt : (II)I
    //   1634: invokevirtual setMarqueeRepeatLimit : (I)V
    //   1637: goto -> 2403
    //   1640: aload_0
    //   1641: aload #17
    //   1643: iload #44
    //   1645: aload_0
    //   1646: getfield mSpacingMult : F
    //   1649: invokevirtual getFloat : (IF)F
    //   1652: putfield mSpacingMult : F
    //   1655: goto -> 2403
    //   1658: aload_0
    //   1659: aload #17
    //   1661: iload #44
    //   1663: aload_0
    //   1664: getfield mSpacingAdd : F
    //   1667: f2i
    //   1668: invokevirtual getDimensionPixelSize : (II)I
    //   1671: i2f
    //   1672: putfield mSpacingAdd : F
    //   1675: goto -> 2403
    //   1678: aload #17
    //   1680: iload #44
    //   1682: iload #26
    //   1684: invokevirtual getDimensionPixelSize : (II)I
    //   1687: istore #26
    //   1689: goto -> 2403
    //   1692: aload #17
    //   1694: iload #44
    //   1696: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   1699: astore #23
    //   1701: goto -> 2403
    //   1704: aload #17
    //   1706: iload #44
    //   1708: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   1711: astore #22
    //   1713: goto -> 2403
    //   1716: aload #17
    //   1718: iload #44
    //   1720: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   1723: astore #36
    //   1725: goto -> 2403
    //   1728: aload #17
    //   1730: iload #44
    //   1732: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   1735: astore #35
    //   1737: goto -> 2403
    //   1740: aload_0
    //   1741: aload #17
    //   1743: iload #44
    //   1745: iconst_0
    //   1746: invokevirtual getBoolean : (IZ)Z
    //   1749: putfield mFreezesText : Z
    //   1752: goto -> 2403
    //   1755: aload #17
    //   1757: iload #44
    //   1759: iload #12
    //   1761: invokevirtual getBoolean : (IZ)Z
    //   1764: istore #12
    //   1766: goto -> 2403
    //   1769: aload #17
    //   1771: iload #44
    //   1773: iload #42
    //   1775: invokevirtual getBoolean : (IZ)Z
    //   1778: istore #42
    //   1780: goto -> 2403
    //   1783: aload #17
    //   1785: iload #44
    //   1787: iload #20
    //   1789: invokevirtual getInt : (II)I
    //   1792: istore #20
    //   1794: goto -> 2403
    //   1797: aload #17
    //   1799: iload #44
    //   1801: invokevirtual getText : (I)Ljava/lang/CharSequence;
    //   1804: astore #13
    //   1806: goto -> 2403
    //   1809: aload #17
    //   1811: iload #44
    //   1813: iload #40
    //   1815: invokevirtual getBoolean : (IZ)Z
    //   1818: istore #40
    //   1820: goto -> 2403
    //   1823: aload #17
    //   1825: iload #44
    //   1827: invokevirtual getText : (I)Ljava/lang/CharSequence;
    //   1830: astore #7
    //   1832: goto -> 2403
    //   1835: aload #17
    //   1837: iload #44
    //   1839: iload #34
    //   1841: invokevirtual getInt : (II)I
    //   1844: istore #34
    //   1846: goto -> 2403
    //   1849: aload #17
    //   1851: iload #44
    //   1853: iconst_m1
    //   1854: invokevirtual getInt : (II)I
    //   1857: istore #29
    //   1859: goto -> 2403
    //   1862: aload #17
    //   1864: iload #44
    //   1866: iconst_1
    //   1867: invokevirtual getBoolean : (IZ)Z
    //   1870: ifne -> 1881
    //   1873: aload_0
    //   1874: iconst_0
    //   1875: invokevirtual setIncludeFontPadding : (Z)V
    //   1878: goto -> 2403
    //   1881: goto -> 2403
    //   1884: aload #17
    //   1886: iload #44
    //   1888: iload #21
    //   1890: invokevirtual getBoolean : (IZ)Z
    //   1893: istore #21
    //   1895: goto -> 2403
    //   1898: aload #17
    //   1900: iload #44
    //   1902: iload #28
    //   1904: invokevirtual getBoolean : (IZ)Z
    //   1907: istore #28
    //   1909: goto -> 2403
    //   1912: aload #17
    //   1914: iload #44
    //   1916: iload #31
    //   1918: invokevirtual getBoolean : (IZ)Z
    //   1921: istore #31
    //   1923: goto -> 2403
    //   1926: aload #17
    //   1928: iload #44
    //   1930: iconst_0
    //   1931: invokevirtual getBoolean : (IZ)Z
    //   1934: ifeq -> 1945
    //   1937: aload_0
    //   1938: iconst_1
    //   1939: invokevirtual setHorizontallyScrolling : (Z)V
    //   1942: goto -> 2403
    //   1945: goto -> 2403
    //   1948: aload_0
    //   1949: aload #17
    //   1951: iload #44
    //   1953: iconst_m1
    //   1954: invokevirtual getInt : (II)I
    //   1957: invokevirtual setMinEms : (I)V
    //   1960: goto -> 2403
    //   1963: aload_0
    //   1964: aload #17
    //   1966: iload #44
    //   1968: iconst_m1
    //   1969: invokevirtual getDimensionPixelSize : (II)I
    //   1972: invokevirtual setWidth : (I)V
    //   1975: goto -> 2403
    //   1978: aload_0
    //   1979: aload #17
    //   1981: iload #44
    //   1983: iconst_m1
    //   1984: invokevirtual getInt : (II)I
    //   1987: invokevirtual setEms : (I)V
    //   1990: goto -> 2403
    //   1993: aload_0
    //   1994: aload #17
    //   1996: iload #44
    //   1998: iconst_m1
    //   1999: invokevirtual getInt : (II)I
    //   2002: invokevirtual setMaxEms : (I)V
    //   2005: goto -> 2403
    //   2008: aload_0
    //   2009: aload #17
    //   2011: iload #44
    //   2013: iconst_m1
    //   2014: invokevirtual getInt : (II)I
    //   2017: invokevirtual setMinLines : (I)V
    //   2020: goto -> 2403
    //   2023: aload_0
    //   2024: aload #17
    //   2026: iload #44
    //   2028: iconst_m1
    //   2029: invokevirtual getDimensionPixelSize : (II)I
    //   2032: invokevirtual setHeight : (I)V
    //   2035: goto -> 2403
    //   2038: aload_0
    //   2039: aload #17
    //   2041: iload #44
    //   2043: iconst_m1
    //   2044: invokevirtual getInt : (II)I
    //   2047: invokevirtual setLines : (I)V
    //   2050: goto -> 2403
    //   2053: aload_0
    //   2054: aload #17
    //   2056: iload #44
    //   2058: iconst_m1
    //   2059: invokevirtual getInt : (II)I
    //   2062: invokevirtual setMaxLines : (I)V
    //   2065: goto -> 2403
    //   2068: aload #17
    //   2070: iload #44
    //   2072: iconst_1
    //   2073: invokevirtual getBoolean : (IZ)Z
    //   2076: ifne -> 2087
    //   2079: aload_0
    //   2080: iconst_0
    //   2081: invokevirtual setCursorVisible : (Z)V
    //   2084: goto -> 2403
    //   2087: goto -> 2403
    //   2090: aload_0
    //   2091: aload #17
    //   2093: iload #44
    //   2095: fconst_1
    //   2096: invokevirtual getFloat : (IF)F
    //   2099: invokevirtual setTextScaleX : (F)V
    //   2102: goto -> 2403
    //   2105: aload_0
    //   2106: aload #17
    //   2108: iload #44
    //   2110: iconst_0
    //   2111: invokevirtual getResourceId : (II)I
    //   2114: putfield mHintId : I
    //   2117: aload #17
    //   2119: iload #44
    //   2121: invokevirtual getText : (I)Ljava/lang/CharSequence;
    //   2124: astore #5
    //   2126: goto -> 2403
    //   2129: aload_0
    //   2130: aload #17
    //   2132: iload #44
    //   2134: iconst_0
    //   2135: invokevirtual getResourceId : (II)I
    //   2138: putfield mTextId : I
    //   2141: aload #17
    //   2143: iload #44
    //   2145: invokevirtual getText : (I)Ljava/lang/CharSequence;
    //   2148: astore #30
    //   2150: iconst_1
    //   2151: istore #33
    //   2153: goto -> 2403
    //   2156: aload #17
    //   2158: iload #44
    //   2160: iload #11
    //   2162: invokevirtual getInt : (II)I
    //   2165: istore #11
    //   2167: goto -> 2403
    //   2170: aload_0
    //   2171: aload #17
    //   2173: iload #44
    //   2175: iconst_m1
    //   2176: invokevirtual getDimensionPixelSize : (II)I
    //   2179: invokevirtual setMinHeight : (I)V
    //   2182: goto -> 2403
    //   2185: aload_0
    //   2186: aload #17
    //   2188: iload #44
    //   2190: iconst_m1
    //   2191: invokevirtual getDimensionPixelSize : (II)I
    //   2194: invokevirtual setMinWidth : (I)V
    //   2197: goto -> 2403
    //   2200: aload_0
    //   2201: aload #17
    //   2203: iload #44
    //   2205: iconst_m1
    //   2206: invokevirtual getDimensionPixelSize : (II)I
    //   2209: invokevirtual setMaxHeight : (I)V
    //   2212: goto -> 2403
    //   2215: aload_0
    //   2216: aload #17
    //   2218: iload #44
    //   2220: iconst_m1
    //   2221: invokevirtual getDimensionPixelSize : (II)I
    //   2224: invokevirtual setMaxWidth : (I)V
    //   2227: goto -> 2403
    //   2230: aload_0
    //   2231: aload #17
    //   2233: iload #44
    //   2235: iconst_1
    //   2236: invokevirtual getBoolean : (IZ)Z
    //   2239: putfield mLinksClickable : Z
    //   2242: goto -> 2403
    //   2245: aload_0
    //   2246: aload #17
    //   2248: iload #44
    //   2250: iconst_0
    //   2251: invokevirtual getInt : (II)I
    //   2254: putfield mAutoLinkMask : I
    //   2257: goto -> 2403
    //   2260: aload_0
    //   2261: aload #17
    //   2263: iload #44
    //   2265: iconst_m1
    //   2266: invokevirtual getInt : (II)I
    //   2269: invokevirtual setGravity : (I)V
    //   2272: goto -> 2403
    //   2275: aload #17
    //   2277: iload #44
    //   2279: iload #27
    //   2281: invokevirtual getInt : (II)I
    //   2284: istore #27
    //   2286: goto -> 2403
    //   2289: aload_0
    //   2290: aload #17
    //   2292: iload #44
    //   2294: iconst_0
    //   2295: invokevirtual getResourceId : (II)I
    //   2298: putfield mTextEditSuggestionHighlightStyle : I
    //   2301: goto -> 2403
    //   2304: aload_0
    //   2305: aload #17
    //   2307: iload #44
    //   2309: iconst_0
    //   2310: invokevirtual getResourceId : (II)I
    //   2313: putfield mTextEditSuggestionContainerLayout : I
    //   2316: goto -> 2403
    //   2319: aload #17
    //   2321: iload #44
    //   2323: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   2326: astore #25
    //   2328: goto -> 2403
    //   2331: aload #17
    //   2333: iload #44
    //   2335: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   2338: astore #24
    //   2340: goto -> 2403
    //   2343: aload_0
    //   2344: aload #17
    //   2346: iload #44
    //   2348: iconst_0
    //   2349: invokevirtual getResourceId : (II)I
    //   2352: putfield mTextEditSuggestionItemLayout : I
    //   2355: goto -> 2403
    //   2358: aload_0
    //   2359: aload #17
    //   2361: iload #44
    //   2363: iconst_0
    //   2364: invokevirtual getResourceId : (II)I
    //   2367: putfield mCursorDrawableRes : I
    //   2370: goto -> 2403
    //   2373: aload_0
    //   2374: aload #17
    //   2376: iload #44
    //   2378: iconst_0
    //   2379: invokevirtual getBoolean : (IZ)Z
    //   2382: invokevirtual setTextIsSelectable : (Z)V
    //   2385: goto -> 2403
    //   2388: aload_0
    //   2389: aload #17
    //   2391: iload #44
    //   2393: aload_0
    //   2394: invokevirtual isEnabled : ()Z
    //   2397: invokevirtual getBoolean : (IZ)Z
    //   2400: invokevirtual setEnabled : (Z)V
    //   2403: iinc #19, 1
    //   2406: goto -> 751
    //   2409: aload #17
    //   2411: invokevirtual recycle : ()V
    //   2414: getstatic android/widget/TextView$BufferType.EDITABLE : Landroid/widget/TextView$BufferType;
    //   2417: astore #6
    //   2419: iload #32
    //   2421: sipush #4095
    //   2424: iand
    //   2425: istore #19
    //   2427: iload #19
    //   2429: sipush #129
    //   2432: if_icmpne -> 2441
    //   2435: iconst_1
    //   2436: istore #45
    //   2438: goto -> 2444
    //   2441: iconst_0
    //   2442: istore #45
    //   2444: iload #19
    //   2446: sipush #225
    //   2449: if_icmpne -> 2458
    //   2452: iconst_1
    //   2453: istore #46
    //   2455: goto -> 2461
    //   2458: iconst_0
    //   2459: istore #46
    //   2461: iload #19
    //   2463: bipush #18
    //   2465: if_icmpne -> 2474
    //   2468: iconst_1
    //   2469: istore #47
    //   2471: goto -> 2477
    //   2474: iconst_0
    //   2475: istore #47
    //   2477: aload_1
    //   2478: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   2481: getfield targetSdkVersion : I
    //   2484: istore #19
    //   2486: iload #19
    //   2488: bipush #26
    //   2490: if_icmplt -> 2499
    //   2493: iconst_1
    //   2494: istore #48
    //   2496: goto -> 2502
    //   2499: iconst_0
    //   2500: istore #48
    //   2502: aload_0
    //   2503: iload #48
    //   2505: putfield mUseInternationalizedInput : Z
    //   2508: aload_0
    //   2509: invokevirtual isOplusOSStyle : ()Z
    //   2512: ifne -> 2528
    //   2515: iload #19
    //   2517: bipush #28
    //   2519: if_icmplt -> 2528
    //   2522: iconst_1
    //   2523: istore #48
    //   2525: goto -> 2531
    //   2528: iconst_0
    //   2529: istore #48
    //   2531: aload_0
    //   2532: iload #48
    //   2534: putfield mUseFallbackLineSpacing : Z
    //   2537: aload #13
    //   2539: ifnull -> 2674
    //   2542: aload #13
    //   2544: invokeinterface toString : ()Ljava/lang/String;
    //   2549: invokestatic forName : (Ljava/lang/String;)Ljava/lang/Class;
    //   2552: astore #9
    //   2554: aload_0
    //   2555: invokespecial createEditorIfNeeded : ()V
    //   2558: aload_0
    //   2559: getfield mEditor : Landroid/widget/Editor;
    //   2562: astore #7
    //   2564: aload #9
    //   2566: invokevirtual newInstance : ()Ljava/lang/Object;
    //   2569: astore #9
    //   2571: aload #7
    //   2573: aload #9
    //   2575: checkcast android/text/method/KeyListener
    //   2578: putfield mKeyListener : Landroid/text/method/KeyListener;
    //   2581: aload_0
    //   2582: getfield mEditor : Landroid/widget/Editor;
    //   2585: astore #7
    //   2587: iload #32
    //   2589: ifeq -> 2599
    //   2592: iload #32
    //   2594: istore #11
    //   2596: goto -> 2613
    //   2599: aload_0
    //   2600: getfield mEditor : Landroid/widget/Editor;
    //   2603: getfield mKeyListener : Landroid/text/method/KeyListener;
    //   2606: invokeinterface getInputType : ()I
    //   2611: istore #11
    //   2613: aload #7
    //   2615: iload #11
    //   2617: putfield mInputType : I
    //   2620: goto -> 2633
    //   2623: astore #7
    //   2625: aload_0
    //   2626: getfield mEditor : Landroid/widget/Editor;
    //   2629: iconst_1
    //   2630: putfield mInputType : I
    //   2633: goto -> 3003
    //   2636: astore_1
    //   2637: goto -> 2645
    //   2640: astore_1
    //   2641: goto -> 2655
    //   2644: astore_1
    //   2645: new java/lang/RuntimeException
    //   2648: dup
    //   2649: aload_1
    //   2650: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   2653: athrow
    //   2654: astore_1
    //   2655: new java/lang/RuntimeException
    //   2658: dup
    //   2659: aload_1
    //   2660: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   2663: athrow
    //   2664: astore_1
    //   2665: new java/lang/RuntimeException
    //   2668: dup
    //   2669: aload_1
    //   2670: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   2673: athrow
    //   2674: aload #7
    //   2676: ifnull -> 2731
    //   2679: aload_0
    //   2680: invokespecial createEditorIfNeeded : ()V
    //   2683: aload_0
    //   2684: getfield mEditor : Landroid/widget/Editor;
    //   2687: aload #7
    //   2689: invokeinterface toString : ()Ljava/lang/String;
    //   2694: invokestatic getInstance : (Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;
    //   2697: putfield mKeyListener : Landroid/text/method/KeyListener;
    //   2700: aload_0
    //   2701: getfield mEditor : Landroid/widget/Editor;
    //   2704: astore #7
    //   2706: iload #32
    //   2708: ifeq -> 2718
    //   2711: iload #32
    //   2713: istore #11
    //   2715: goto -> 2721
    //   2718: iconst_1
    //   2719: istore #11
    //   2721: aload #7
    //   2723: iload #11
    //   2725: putfield mInputType : I
    //   2728: goto -> 3003
    //   2731: iload #32
    //   2733: ifeq -> 2755
    //   2736: aload_0
    //   2737: iload #32
    //   2739: iconst_1
    //   2740: invokespecial setInputType : (IZ)V
    //   2743: iload #32
    //   2745: invokestatic isMultilineInputType : (I)Z
    //   2748: iconst_1
    //   2749: ixor
    //   2750: istore #28
    //   2752: goto -> 3132
    //   2755: iload #40
    //   2757: ifeq -> 2792
    //   2760: aload_0
    //   2761: invokespecial createEditorIfNeeded : ()V
    //   2764: aload_0
    //   2765: getfield mEditor : Landroid/widget/Editor;
    //   2768: invokestatic getInstance : ()Landroid/text/method/DialerKeyListener;
    //   2771: putfield mKeyListener : Landroid/text/method/KeyListener;
    //   2774: aload_0
    //   2775: getfield mEditor : Landroid/widget/Editor;
    //   2778: astore #7
    //   2780: iconst_3
    //   2781: istore #32
    //   2783: aload #7
    //   2785: iconst_3
    //   2786: putfield mInputType : I
    //   2789: goto -> 3132
    //   2792: iload #34
    //   2794: ifeq -> 2878
    //   2797: aload_0
    //   2798: invokespecial createEditorIfNeeded : ()V
    //   2801: aload_0
    //   2802: getfield mEditor : Landroid/widget/Editor;
    //   2805: astore #7
    //   2807: iload #34
    //   2809: iconst_2
    //   2810: iand
    //   2811: ifeq -> 2820
    //   2814: iconst_1
    //   2815: istore #12
    //   2817: goto -> 2823
    //   2820: iconst_0
    //   2821: istore #12
    //   2823: iload #34
    //   2825: iconst_4
    //   2826: iand
    //   2827: ifeq -> 2836
    //   2830: iconst_1
    //   2831: istore #40
    //   2833: goto -> 2839
    //   2836: iconst_0
    //   2837: istore #40
    //   2839: aload #7
    //   2841: aconst_null
    //   2842: iload #12
    //   2844: iload #40
    //   2846: invokestatic getInstance : (Ljava/util/Locale;ZZ)Landroid/text/method/DigitsKeyListener;
    //   2849: putfield mKeyListener : Landroid/text/method/KeyListener;
    //   2852: aload_0
    //   2853: getfield mEditor : Landroid/widget/Editor;
    //   2856: getfield mKeyListener : Landroid/text/method/KeyListener;
    //   2859: invokeinterface getInputType : ()I
    //   2864: istore #32
    //   2866: aload_0
    //   2867: getfield mEditor : Landroid/widget/Editor;
    //   2870: iload #32
    //   2872: putfield mInputType : I
    //   2875: goto -> 3132
    //   2878: iload #42
    //   2880: ifne -> 3030
    //   2883: iload #20
    //   2885: iconst_m1
    //   2886: if_icmpeq -> 2892
    //   2889: goto -> 3030
    //   2892: iload #12
    //   2894: ifeq -> 2922
    //   2897: aload_0
    //   2898: invokespecial createEditorIfNeeded : ()V
    //   2901: aload_0
    //   2902: getfield mEditor : Landroid/widget/Editor;
    //   2905: invokestatic getInstance : ()Landroid/text/method/TextKeyListener;
    //   2908: putfield mKeyListener : Landroid/text/method/KeyListener;
    //   2911: aload_0
    //   2912: getfield mEditor : Landroid/widget/Editor;
    //   2915: iconst_1
    //   2916: putfield mInputType : I
    //   2919: goto -> 3003
    //   2922: aload_0
    //   2923: invokevirtual isTextSelectable : ()Z
    //   2926: ifeq -> 2969
    //   2929: aload_0
    //   2930: getfield mEditor : Landroid/widget/Editor;
    //   2933: astore #6
    //   2935: aload #6
    //   2937: ifnull -> 2954
    //   2940: aload #6
    //   2942: aconst_null
    //   2943: putfield mKeyListener : Landroid/text/method/KeyListener;
    //   2946: aload_0
    //   2947: getfield mEditor : Landroid/widget/Editor;
    //   2950: iconst_0
    //   2951: putfield mInputType : I
    //   2954: getstatic android/widget/TextView$BufferType.SPANNABLE : Landroid/widget/TextView$BufferType;
    //   2957: astore #6
    //   2959: aload_0
    //   2960: invokestatic getInstance : ()Landroid/text/method/MovementMethod;
    //   2963: invokevirtual setMovementMethod : (Landroid/text/method/MovementMethod;)V
    //   2966: goto -> 3132
    //   2969: aload_0
    //   2970: getfield mEditor : Landroid/widget/Editor;
    //   2973: astore #7
    //   2975: aload #7
    //   2977: ifnull -> 2986
    //   2980: aload #7
    //   2982: aconst_null
    //   2983: putfield mKeyListener : Landroid/text/method/KeyListener;
    //   2986: iload #11
    //   2988: ifeq -> 3022
    //   2991: iload #11
    //   2993: iconst_1
    //   2994: if_icmpeq -> 3014
    //   2997: iload #11
    //   2999: iconst_2
    //   3000: if_icmpeq -> 3006
    //   3003: goto -> 3132
    //   3006: getstatic android/widget/TextView$BufferType.EDITABLE : Landroid/widget/TextView$BufferType;
    //   3009: astore #6
    //   3011: goto -> 3132
    //   3014: getstatic android/widget/TextView$BufferType.SPANNABLE : Landroid/widget/TextView$BufferType;
    //   3017: astore #6
    //   3019: goto -> 3132
    //   3022: getstatic android/widget/TextView$BufferType.NORMAL : Landroid/widget/TextView$BufferType;
    //   3025: astore #6
    //   3027: goto -> 3132
    //   3030: iconst_1
    //   3031: istore #11
    //   3033: iload #20
    //   3035: iconst_1
    //   3036: if_icmpeq -> 3089
    //   3039: iload #20
    //   3041: iconst_2
    //   3042: if_icmpeq -> 3074
    //   3045: iload #20
    //   3047: iconst_3
    //   3048: if_icmpeq -> 3059
    //   3051: getstatic android/text/method/TextKeyListener$Capitalize.NONE : Landroid/text/method/TextKeyListener$Capitalize;
    //   3054: astore #7
    //   3056: goto -> 3101
    //   3059: getstatic android/text/method/TextKeyListener$Capitalize.CHARACTERS : Landroid/text/method/TextKeyListener$Capitalize;
    //   3062: astore #7
    //   3064: iconst_1
    //   3065: sipush #4096
    //   3068: ior
    //   3069: istore #11
    //   3071: goto -> 3101
    //   3074: getstatic android/text/method/TextKeyListener$Capitalize.WORDS : Landroid/text/method/TextKeyListener$Capitalize;
    //   3077: astore #7
    //   3079: iconst_1
    //   3080: sipush #8192
    //   3083: ior
    //   3084: istore #11
    //   3086: goto -> 3101
    //   3089: getstatic android/text/method/TextKeyListener$Capitalize.SENTENCES : Landroid/text/method/TextKeyListener$Capitalize;
    //   3092: astore #7
    //   3094: iconst_1
    //   3095: sipush #16384
    //   3098: ior
    //   3099: istore #11
    //   3101: aload_0
    //   3102: invokespecial createEditorIfNeeded : ()V
    //   3105: aload_0
    //   3106: getfield mEditor : Landroid/widget/Editor;
    //   3109: iload #42
    //   3111: aload #7
    //   3113: invokestatic getInstance : (ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;
    //   3116: putfield mKeyListener : Landroid/text/method/KeyListener;
    //   3119: aload_0
    //   3120: getfield mEditor : Landroid/widget/Editor;
    //   3123: iload #11
    //   3125: putfield mInputType : I
    //   3128: iload #11
    //   3130: istore #32
    //   3132: aload_0
    //   3133: getfield mEditor : Landroid/widget/Editor;
    //   3136: astore #7
    //   3138: aload #7
    //   3140: ifnull -> 3159
    //   3143: aload #7
    //   3145: iload #31
    //   3147: iload #45
    //   3149: iload #46
    //   3151: iload #47
    //   3153: invokevirtual adjustInputType : (ZZZZ)V
    //   3156: goto -> 3159
    //   3159: iload #21
    //   3161: ifeq -> 3196
    //   3164: aload_0
    //   3165: invokespecial createEditorIfNeeded : ()V
    //   3168: aload_0
    //   3169: getfield mEditor : Landroid/widget/Editor;
    //   3172: iconst_1
    //   3173: putfield mSelectAllOnFocus : Z
    //   3176: aload #6
    //   3178: astore #7
    //   3180: aload #6
    //   3182: getstatic android/widget/TextView$BufferType.NORMAL : Landroid/widget/TextView$BufferType;
    //   3185: if_acmpne -> 3200
    //   3188: getstatic android/widget/TextView$BufferType.SPANNABLE : Landroid/widget/TextView$BufferType;
    //   3191: astore #7
    //   3193: goto -> 3200
    //   3196: aload #6
    //   3198: astore #7
    //   3200: aload #37
    //   3202: ifnonnull -> 3216
    //   3205: aload #10
    //   3207: ifnull -> 3213
    //   3210: goto -> 3216
    //   3213: goto -> 3285
    //   3216: aload_0
    //   3217: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   3220: ifnonnull -> 3238
    //   3223: aload_0
    //   3224: new android/widget/TextView$Drawables
    //   3227: dup
    //   3228: aload_1
    //   3229: invokespecial <init> : (Landroid/content/Context;)V
    //   3232: putfield mDrawables : Landroid/widget/TextView$Drawables;
    //   3235: goto -> 3238
    //   3238: aload #37
    //   3240: ifnull -> 3263
    //   3243: aload_0
    //   3244: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   3247: aload #37
    //   3249: putfield mTintList : Landroid/content/res/ColorStateList;
    //   3252: aload_0
    //   3253: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   3256: iconst_1
    //   3257: putfield mHasTint : Z
    //   3260: goto -> 3263
    //   3263: aload #10
    //   3265: ifnull -> 3285
    //   3268: aload_0
    //   3269: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   3272: aload #10
    //   3274: putfield mBlendMode : Landroid/graphics/BlendMode;
    //   3277: aload_0
    //   3278: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   3281: iconst_1
    //   3282: putfield mHasTintMode : Z
    //   3285: aload_0
    //   3286: aload #22
    //   3288: aload #35
    //   3290: aload #23
    //   3292: aload #36
    //   3294: invokevirtual setCompoundDrawablesWithIntrinsicBounds : (Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    //   3297: aload_0
    //   3298: aload #24
    //   3300: aload #25
    //   3302: invokespecial setRelativeDrawablesIfNeeded : (Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    //   3305: aload_0
    //   3306: iload #26
    //   3308: invokevirtual setCompoundDrawablePadding : (I)V
    //   3311: aload_0
    //   3312: iload #28
    //   3314: invokespecial setInputTypeSingleLine : (Z)V
    //   3317: aload_0
    //   3318: iload #28
    //   3320: iload #28
    //   3322: iload #28
    //   3324: invokespecial applySingleLine : (ZZZ)V
    //   3327: iload #28
    //   3329: ifeq -> 3351
    //   3332: aload_0
    //   3333: invokevirtual getKeyListener : ()Landroid/text/method/KeyListener;
    //   3336: ifnonnull -> 3351
    //   3339: iload #27
    //   3341: iconst_m1
    //   3342: if_icmpne -> 3351
    //   3345: iconst_3
    //   3346: istore #11
    //   3348: goto -> 3355
    //   3351: iload #27
    //   3353: istore #11
    //   3355: iload #11
    //   3357: iconst_1
    //   3358: if_icmpeq -> 3445
    //   3361: iload #11
    //   3363: iconst_2
    //   3364: if_icmpeq -> 3435
    //   3367: iload #11
    //   3369: iconst_3
    //   3370: if_icmpeq -> 3425
    //   3373: iload #11
    //   3375: iconst_4
    //   3376: if_icmpeq -> 3382
    //   3379: goto -> 3452
    //   3382: aload_1
    //   3383: invokestatic get : (Landroid/content/Context;)Landroid/view/ViewConfiguration;
    //   3386: invokevirtual isFadingMarqueeEnabled : ()Z
    //   3389: ifeq -> 3405
    //   3392: aload_0
    //   3393: iconst_1
    //   3394: invokevirtual setHorizontalFadingEdgeEnabled : (Z)V
    //   3397: aload_0
    //   3398: iconst_0
    //   3399: putfield mMarqueeFadeMode : I
    //   3402: goto -> 3415
    //   3405: aload_0
    //   3406: iconst_0
    //   3407: invokevirtual setHorizontalFadingEdgeEnabled : (Z)V
    //   3410: aload_0
    //   3411: iconst_1
    //   3412: putfield mMarqueeFadeMode : I
    //   3415: aload_0
    //   3416: getstatic android/text/TextUtils$TruncateAt.MARQUEE : Landroid/text/TextUtils$TruncateAt;
    //   3419: invokevirtual setEllipsize : (Landroid/text/TextUtils$TruncateAt;)V
    //   3422: goto -> 3452
    //   3425: aload_0
    //   3426: getstatic android/text/TextUtils$TruncateAt.END : Landroid/text/TextUtils$TruncateAt;
    //   3429: invokevirtual setEllipsize : (Landroid/text/TextUtils$TruncateAt;)V
    //   3432: goto -> 3452
    //   3435: aload_0
    //   3436: getstatic android/text/TextUtils$TruncateAt.MIDDLE : Landroid/text/TextUtils$TruncateAt;
    //   3439: invokevirtual setEllipsize : (Landroid/text/TextUtils$TruncateAt;)V
    //   3442: goto -> 3452
    //   3445: aload_0
    //   3446: getstatic android/text/TextUtils$TruncateAt.START : Landroid/text/TextUtils$TruncateAt;
    //   3449: invokevirtual setEllipsize : (Landroid/text/TextUtils$TruncateAt;)V
    //   3452: iload #31
    //   3454: ifne -> 3481
    //   3457: iload #45
    //   3459: ifne -> 3481
    //   3462: iload #46
    //   3464: ifne -> 3481
    //   3467: iload #47
    //   3469: ifeq -> 3475
    //   3472: goto -> 3481
    //   3475: iconst_0
    //   3476: istore #27
    //   3478: goto -> 3484
    //   3481: iconst_1
    //   3482: istore #27
    //   3484: iload #27
    //   3486: ifne -> 3524
    //   3489: aload_0
    //   3490: getfield mEditor : Landroid/widget/Editor;
    //   3493: astore #6
    //   3495: aload #6
    //   3497: ifnull -> 3518
    //   3500: aload #6
    //   3502: getfield mInputType : I
    //   3505: sipush #4095
    //   3508: iand
    //   3509: sipush #129
    //   3512: if_icmpne -> 3518
    //   3515: goto -> 3524
    //   3518: iconst_0
    //   3519: istore #32
    //   3521: goto -> 3527
    //   3524: iconst_1
    //   3525: istore #32
    //   3527: iload #32
    //   3529: ifeq -> 3560
    //   3532: aload #8
    //   3534: getstatic com/oplus/font/IOplusFontManager.DEFAULT : Lcom/oplus/font/IOplusFontManager;
    //   3537: iconst_0
    //   3538: anewarray java/lang/Object
    //   3541: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   3544: checkcast com/oplus/font/IOplusFontManager
    //   3547: iconst_3
    //   3548: iconst_1
    //   3549: invokeinterface getTypefaceIndex : (II)I
    //   3554: putfield mTypefaceIndex : I
    //   3557: goto -> 3560
    //   3560: aload_0
    //   3561: aload #8
    //   3563: invokespecial applyTextAppearance : (Landroid/widget/TextView$TextAppearanceAttributes;)V
    //   3566: iload #27
    //   3568: ifeq -> 3578
    //   3571: aload_0
    //   3572: invokestatic getInstance : ()Landroid/text/method/PasswordTransformationMethod;
    //   3575: invokevirtual setTransformationMethod : (Landroid/text/method/TransformationMethod;)V
    //   3578: iload #29
    //   3580: iflt -> 3606
    //   3583: aload_0
    //   3584: iconst_1
    //   3585: anewarray android/text/InputFilter
    //   3588: dup
    //   3589: iconst_0
    //   3590: new android/text/InputFilter$LengthFilter
    //   3593: dup
    //   3594: iload #29
    //   3596: invokespecial <init> : (I)V
    //   3599: aastore
    //   3600: invokevirtual setFilters : ([Landroid/text/InputFilter;)V
    //   3603: goto -> 3613
    //   3606: aload_0
    //   3607: getstatic android/widget/TextView.NO_FILTERS : [Landroid/text/InputFilter;
    //   3610: invokevirtual setFilters : ([Landroid/text/InputFilter;)V
    //   3613: aload_0
    //   3614: aload #30
    //   3616: aload #7
    //   3618: invokevirtual setText : (Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    //   3621: aload_0
    //   3622: getfield mText : Ljava/lang/CharSequence;
    //   3625: ifnonnull -> 3638
    //   3628: aload_0
    //   3629: ldc_w ''
    //   3632: putfield mText : Ljava/lang/CharSequence;
    //   3635: goto -> 3638
    //   3638: aload_0
    //   3639: getfield mTransformed : Ljava/lang/CharSequence;
    //   3642: ifnonnull -> 3652
    //   3645: aload_0
    //   3646: ldc_w ''
    //   3649: putfield mTransformed : Ljava/lang/CharSequence;
    //   3652: iload #33
    //   3654: ifeq -> 3662
    //   3657: aload_0
    //   3658: iconst_1
    //   3659: putfield mTextSetFromXmlOrResourceId : Z
    //   3662: aload #5
    //   3664: ifnull -> 3673
    //   3667: aload_0
    //   3668: aload #5
    //   3670: invokevirtual setHint : (Ljava/lang/CharSequence;)V
    //   3673: aload_1
    //   3674: aload_2
    //   3675: getstatic com/android/internal/R$styleable.View : [I
    //   3678: iload_3
    //   3679: iload #4
    //   3681: invokevirtual obtainStyledAttributes : (Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    //   3684: astore_1
    //   3685: aload_0
    //   3686: getfield mMovement : Landroid/text/method/MovementMethod;
    //   3689: ifnonnull -> 3707
    //   3692: aload_0
    //   3693: invokevirtual getKeyListener : ()Landroid/text/method/KeyListener;
    //   3696: ifnull -> 3702
    //   3699: goto -> 3707
    //   3702: iconst_0
    //   3703: istore_3
    //   3704: goto -> 3709
    //   3707: iconst_1
    //   3708: istore_3
    //   3709: iload_3
    //   3710: ifne -> 3729
    //   3713: aload_0
    //   3714: invokevirtual isClickable : ()Z
    //   3717: ifeq -> 3723
    //   3720: goto -> 3729
    //   3723: iconst_0
    //   3724: istore #28
    //   3726: goto -> 3732
    //   3729: iconst_1
    //   3730: istore #28
    //   3732: iload_3
    //   3733: ifne -> 3752
    //   3736: aload_0
    //   3737: invokevirtual isLongClickable : ()Z
    //   3740: ifeq -> 3746
    //   3743: goto -> 3752
    //   3746: iconst_0
    //   3747: istore #21
    //   3749: goto -> 3755
    //   3752: iconst_1
    //   3753: istore #21
    //   3755: aload_0
    //   3756: invokevirtual getFocusable : ()I
    //   3759: istore_3
    //   3760: aload_1
    //   3761: invokevirtual getIndexCount : ()I
    //   3764: istore #4
    //   3766: iconst_0
    //   3767: istore #32
    //   3769: iload #28
    //   3771: istore #31
    //   3773: iload #21
    //   3775: istore #28
    //   3777: iload #31
    //   3779: istore #21
    //   3781: iload #32
    //   3783: iload #4
    //   3785: if_icmpge -> 3904
    //   3788: aload_1
    //   3789: iload #32
    //   3791: invokevirtual getIndex : (I)I
    //   3794: istore #26
    //   3796: iload #26
    //   3798: bipush #19
    //   3800: if_icmpeq -> 3846
    //   3803: iload #26
    //   3805: bipush #30
    //   3807: if_icmpeq -> 3833
    //   3810: iload #26
    //   3812: bipush #31
    //   3814: if_icmpeq -> 3820
    //   3817: goto -> 3898
    //   3820: aload_1
    //   3821: iload #26
    //   3823: iload #28
    //   3825: invokevirtual getBoolean : (IZ)Z
    //   3828: istore #28
    //   3830: goto -> 3898
    //   3833: aload_1
    //   3834: iload #26
    //   3836: iload #21
    //   3838: invokevirtual getBoolean : (IZ)Z
    //   3841: istore #21
    //   3843: goto -> 3898
    //   3846: new android/util/TypedValue
    //   3849: dup
    //   3850: invokespecial <init> : ()V
    //   3853: astore_2
    //   3854: aload_1
    //   3855: iload #26
    //   3857: aload_2
    //   3858: invokevirtual getValue : (ILandroid/util/TypedValue;)Z
    //   3861: ifeq -> 3898
    //   3864: aload_2
    //   3865: getfield type : I
    //   3868: bipush #18
    //   3870: if_icmpne -> 3890
    //   3873: aload_2
    //   3874: getfield data : I
    //   3877: ifne -> 3885
    //   3880: iconst_0
    //   3881: istore_3
    //   3882: goto -> 3895
    //   3885: iconst_1
    //   3886: istore_3
    //   3887: goto -> 3895
    //   3890: aload_2
    //   3891: getfield data : I
    //   3894: istore_3
    //   3895: goto -> 3898
    //   3898: iinc #32, 1
    //   3901: goto -> 3781
    //   3904: aload_1
    //   3905: invokevirtual recycle : ()V
    //   3908: iload_3
    //   3909: aload_0
    //   3910: invokevirtual getFocusable : ()I
    //   3913: if_icmpeq -> 3921
    //   3916: aload_0
    //   3917: iload_3
    //   3918: invokevirtual setFocusable : (I)V
    //   3921: aload_0
    //   3922: iload #21
    //   3924: invokevirtual setClickable : (Z)V
    //   3927: aload_0
    //   3928: iload #28
    //   3930: invokevirtual setLongClickable : (Z)V
    //   3933: aload_0
    //   3934: getfield mEditor : Landroid/widget/Editor;
    //   3937: astore_1
    //   3938: aload_1
    //   3939: ifnull -> 3946
    //   3942: aload_1
    //   3943: invokevirtual prepareCursorControllers : ()V
    //   3946: aload_0
    //   3947: invokevirtual getImportantForAccessibility : ()I
    //   3950: ifne -> 3961
    //   3953: aload_0
    //   3954: iconst_1
    //   3955: invokevirtual setImportantForAccessibility : (I)V
    //   3958: goto -> 3961
    //   3961: aload_0
    //   3962: invokevirtual supportsAutoSizeText : ()Z
    //   3965: ifeq -> 4065
    //   3968: aload_0
    //   3969: getfield mAutoSizeTextType : I
    //   3972: iconst_1
    //   3973: if_icmpne -> 4070
    //   3976: aload_0
    //   3977: getfield mHasPresetAutoSizeValues : Z
    //   3980: ifne -> 4057
    //   3983: aload_0
    //   3984: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   3987: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   3990: astore_1
    //   3991: fload #14
    //   3993: ldc -1.0
    //   3995: fcmpl
    //   3996: ifne -> 4012
    //   3999: iconst_2
    //   4000: ldc_w 12.0
    //   4003: aload_1
    //   4004: invokestatic applyDimension : (IFLandroid/util/DisplayMetrics;)F
    //   4007: fstore #14
    //   4009: goto -> 4012
    //   4012: fload #15
    //   4014: ldc -1.0
    //   4016: fcmpl
    //   4017: ifne -> 4033
    //   4020: iconst_2
    //   4021: ldc_w 112.0
    //   4024: aload_1
    //   4025: invokestatic applyDimension : (IFLandroid/util/DisplayMetrics;)F
    //   4028: fstore #15
    //   4030: goto -> 4033
    //   4033: fload #16
    //   4035: ldc -1.0
    //   4037: fcmpl
    //   4038: ifne -> 4047
    //   4041: fconst_1
    //   4042: fstore #16
    //   4044: goto -> 4047
    //   4047: aload_0
    //   4048: fload #14
    //   4050: fload #15
    //   4052: fload #16
    //   4054: invokespecial validateAndSetAutoSizeTextTypeUniformConfiguration : (FFF)V
    //   4057: aload_0
    //   4058: invokespecial setupAutoSizeText : ()Z
    //   4061: pop
    //   4062: goto -> 4070
    //   4065: aload_0
    //   4066: iconst_0
    //   4067: putfield mAutoSizeTextType : I
    //   4070: iload #43
    //   4072: iflt -> 4084
    //   4075: aload_0
    //   4076: iload #43
    //   4078: invokevirtual setFirstBaselineToTopHeight : (I)V
    //   4081: goto -> 4084
    //   4084: iload #41
    //   4086: iflt -> 4098
    //   4089: aload_0
    //   4090: iload #41
    //   4092: invokevirtual setLastBaselineToBottomHeight : (I)V
    //   4095: goto -> 4098
    //   4098: iload #39
    //   4100: iflt -> 4112
    //   4103: aload_0
    //   4104: iload #39
    //   4106: invokevirtual setLineHeight : (I)V
    //   4109: goto -> 4112
    //   4112: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1020	-> 0
    //   #478	-> 9
    //   #479	-> 9
    //   #480	-> 16
    //   #481	-> 16
    //   #701	-> 23
    //   #703	-> 28
    //   #710	-> 33
    //   #729	-> 38
    //   #754	-> 45
    //   #755	-> 50
    //   #758	-> 55
    //   #764	-> 60
    //   #771	-> 67
    //   #773	-> 72
    //   #775	-> 77
    //   #782	-> 82
    //   #784	-> 89
    //   #786	-> 94
    //   #788	-> 99
    //   #791	-> 104
    //   #793	-> 111
    //   #796	-> 116
    //   #798	-> 123
    //   #800	-> 128
    //   #802	-> 133
    //   #807	-> 138
    //   #809	-> 143
    //   #811	-> 148
    //   #831	-> 153
    //   #850	-> 160
    //   #855	-> 167
    //   #887	-> 172
    //   #910	-> 177
    //   #939	-> 182
    //   #941	-> 187
    //   #943	-> 192
    //   #945	-> 198
    //   #947	-> 204
    //   #950	-> 210
    //   #954	-> 217
    //   #960	-> 222
    //   #962	-> 227
    //   #964	-> 232
    //   #1023	-> 237
    //   #1024	-> 257
    //   #1028	-> 265
    //   #1031	-> 275
    //   #1032	-> 282
    //   #1034	-> 287
    //   #1035	-> 294
    //   #1038	-> 299
    //   #1040	-> 306
    //   #1041	-> 312
    //   #1043	-> 319
    //   #1044	-> 335
    //   #1045	-> 348
    //   #1047	-> 360
    //   #1048	-> 376
    //   #1050	-> 386
    //   #1052	-> 394
    //   #1054	-> 399
    //   #1055	-> 409
    //   #1056	-> 420
    //   #1057	-> 427
    //   #1058	-> 432
    //   #1059	-> 437
    //   #1061	-> 442
    //   #1069	-> 453
    //   #1071	-> 467
    //   #1073	-> 489
    //   #1074	-> 489
    //   #1076	-> 498
    //   #1077	-> 503
    //   #1078	-> 509
    //   #1080	-> 521
    //   #1077	-> 538
    //   #1083	-> 541
    //   #1084	-> 546
    //   #1085	-> 556
    //   #1086	-> 562
    //   #1089	-> 567
    //   #1090	-> 573
    //   #1091	-> 576
    //   #1092	-> 576
    //   #1093	-> 579
    //   #1094	-> 579
    //   #1095	-> 579
    //   #1096	-> 579
    //   #1097	-> 579
    //   #1098	-> 579
    //   #1099	-> 579
    //   #1100	-> 579
    //   #1101	-> 579
    //   #1102	-> 579
    //   #1103	-> 579
    //   #1104	-> 579
    //   #1105	-> 579
    //   #1106	-> 579
    //   #1107	-> 579
    //   #1108	-> 579
    //   #1109	-> 579
    //   #1110	-> 583
    //   #1111	-> 587
    //   #1112	-> 591
    //   #1113	-> 591
    //   #1115	-> 605
    //   #1117	-> 619
    //   #1118	-> 619
    //   #1119	-> 619
    //   #1121	-> 619
    //   #1123	-> 629
    //   #1127	-> 636
    //   #1131	-> 660
    //   #1132	-> 660
    //   #1133	-> 758
    //   #1135	-> 767
    //   #1462	-> 1151
    //   #1458	-> 1164
    //   #1459	-> 1174
    //   #1454	-> 1177
    //   #1455	-> 1187
    //   #1450	-> 1190
    //   #1451	-> 1202
    //   #1436	-> 1205
    //   #1438	-> 1216
    //   #1431	-> 1219
    //   #1433	-> 1230
    //   #1441	-> 1233
    //   #1442	-> 1243
    //   #1443	-> 1248
    //   #1444	-> 1255
    //   #1445	-> 1264
    //   #1446	-> 1270
    //   #1447	-> 1275
    //   #1442	-> 1278
    //   #1426	-> 1281
    //   #1428	-> 1292
    //   #1422	-> 1295
    //   #1423	-> 1307
    //   #1343	-> 1310
    //   #1344	-> 1314
    //   #1345	-> 1329
    //   #1418	-> 1332
    //   #1419	-> 1344
    //   #1414	-> 1347
    //   #1415	-> 1359
    //   #1209	-> 1362
    //   #1211	-> 1377
    //   #1205	-> 1380
    //   #1206	-> 1389
    //   #1394	-> 1392
    //   #1395	-> 1404
    //   #1390	-> 1407
    //   #1391	-> 1419
    //   #1386	-> 1422
    //   #1387	-> 1434
    //   #1361	-> 1437
    //   #1362	-> 1441
    //   #1363	-> 1448
    //   #1365	-> 1475
    //   #1355	-> 1478
    //   #1356	-> 1482
    //   #1357	-> 1489
    //   #1358	-> 1506
    //   #1348	-> 1509
    //   #1349	-> 1513
    //   #1350	-> 1520
    //   #1352	-> 1547
    //   #1373	-> 1550
    //   #1378	-> 1562
    //   #1376	-> 1565
    //   #1377	-> 1567
    //   #1379	-> 1577
    //   #1374	-> 1580
    //   #1375	-> 1582
    //   #1378	-> 1592
    //   #1368	-> 1595
    //   #1369	-> 1606
    //   #1339	-> 1609
    //   #1340	-> 1619
    //   #1295	-> 1622
    //   #1296	-> 1637
    //   #1335	-> 1640
    //   #1336	-> 1655
    //   #1331	-> 1658
    //   #1332	-> 1675
    //   #1214	-> 1678
    //   #1215	-> 1689
    //   #1189	-> 1692
    //   #1190	-> 1701
    //   #1181	-> 1704
    //   #1182	-> 1713
    //   #1193	-> 1716
    //   #1194	-> 1725
    //   #1185	-> 1728
    //   #1186	-> 1737
    //   #1319	-> 1740
    //   #1320	-> 1752
    //   #1137	-> 1755
    //   #1138	-> 1766
    //   #1157	-> 1769
    //   #1158	-> 1780
    //   #1161	-> 1783
    //   #1162	-> 1794
    //   #1141	-> 1797
    //   #1142	-> 1806
    //   #1153	-> 1809
    //   #1154	-> 1820
    //   #1149	-> 1823
    //   #1150	-> 1832
    //   #1145	-> 1835
    //   #1146	-> 1846
    //   #1311	-> 1849
    //   #1312	-> 1859
    //   #1299	-> 1862
    //   #1300	-> 1873
    //   #1299	-> 1881
    //   #1169	-> 1884
    //   #1170	-> 1895
    //   #1287	-> 1898
    //   #1288	-> 1909
    //   #1327	-> 1912
    //   #1328	-> 1923
    //   #1281	-> 1926
    //   #1282	-> 1937
    //   #1281	-> 1945
    //   #1258	-> 1948
    //   #1259	-> 1960
    //   #1254	-> 1963
    //   #1255	-> 1975
    //   #1250	-> 1978
    //   #1251	-> 1990
    //   #1242	-> 1993
    //   #1243	-> 2005
    //   #1234	-> 2008
    //   #1235	-> 2020
    //   #1230	-> 2023
    //   #1231	-> 2035
    //   #1226	-> 2038
    //   #1227	-> 2050
    //   #1218	-> 2053
    //   #1219	-> 2065
    //   #1305	-> 2068
    //   #1306	-> 2079
    //   #1305	-> 2087
    //   #1315	-> 2090
    //   #1316	-> 2102
    //   #1270	-> 2105
    //   #1271	-> 2117
    //   #1272	-> 2126
    //   #1275	-> 2129
    //   #1276	-> 2129
    //   #1277	-> 2141
    //   #1278	-> 2150
    //   #1165	-> 2156
    //   #1166	-> 2167
    //   #1238	-> 2170
    //   #1239	-> 2182
    //   #1262	-> 2185
    //   #1263	-> 2197
    //   #1222	-> 2200
    //   #1223	-> 2212
    //   #1246	-> 2215
    //   #1247	-> 2227
    //   #1177	-> 2230
    //   #1178	-> 2242
    //   #1173	-> 2245
    //   #1174	-> 2257
    //   #1266	-> 2260
    //   #1267	-> 2272
    //   #1291	-> 2275
    //   #1292	-> 2286
    //   #1406	-> 2289
    //   #1407	-> 2301
    //   #1402	-> 2304
    //   #1403	-> 2316
    //   #1201	-> 2319
    //   #1202	-> 2328
    //   #1197	-> 2331
    //   #1198	-> 2340
    //   #1398	-> 2343
    //   #1399	-> 2355
    //   #1382	-> 2358
    //   #1383	-> 2370
    //   #1410	-> 2373
    //   #1411	-> 2385
    //   #1323	-> 2388
    //   #1324	-> 2403
    //   #1132	-> 2403
    //   #1467	-> 2409
    //   #1469	-> 2414
    //   #1471	-> 2419
    //   #1473	-> 2427
    //   #1475	-> 2444
    //   #1477	-> 2461
    //   #1480	-> 2477
    //   #1481	-> 2486
    //   #1487	-> 2508
    //   #1489	-> 2537
    //   #1493	-> 2542
    //   #1496	-> 2554
    //   #1499	-> 2554
    //   #1500	-> 2558
    //   #1505	-> 2581
    //   #1507	-> 2581
    //   #1508	-> 2592
    //   #1509	-> 2599
    //   #1512	-> 2620
    //   #1510	-> 2623
    //   #1511	-> 2625
    //   #1513	-> 2633
    //   #1503	-> 2636
    //   #1501	-> 2640
    //   #1503	-> 2644
    //   #1504	-> 2645
    //   #1501	-> 2654
    //   #1502	-> 2655
    //   #1494	-> 2664
    //   #1495	-> 2665
    //   #1513	-> 2674
    //   #1514	-> 2679
    //   #1515	-> 2683
    //   #1519	-> 2700
    //   #1520	-> 2711
    //   #1521	-> 2731
    //   #1522	-> 2736
    //   #1524	-> 2743
    //   #1525	-> 2755
    //   #1526	-> 2760
    //   #1527	-> 2764
    //   #1528	-> 2774
    //   #1529	-> 2792
    //   #1530	-> 2797
    //   #1531	-> 2801
    //   #1535	-> 2852
    //   #1536	-> 2866
    //   #1537	-> 2878
    //   #1566	-> 2892
    //   #1567	-> 2897
    //   #1568	-> 2901
    //   #1569	-> 2911
    //   #1570	-> 2922
    //   #1572	-> 2929
    //   #1573	-> 2940
    //   #1574	-> 2946
    //   #1576	-> 2954
    //   #1578	-> 2959
    //   #1580	-> 2969
    //   #1582	-> 2986
    //   #1595	-> 3003
    //   #1590	-> 3006
    //   #1587	-> 3014
    //   #1588	-> 3019
    //   #1584	-> 3022
    //   #1585	-> 3027
    //   #1537	-> 3030
    //   #1540	-> 3030
    //   #1542	-> 3033
    //   #1559	-> 3051
    //   #1554	-> 3059
    //   #1555	-> 3064
    //   #1556	-> 3071
    //   #1549	-> 3074
    //   #1550	-> 3079
    //   #1551	-> 3086
    //   #1544	-> 3089
    //   #1545	-> 3094
    //   #1546	-> 3101
    //   #1563	-> 3101
    //   #1564	-> 3105
    //   #1565	-> 3119
    //   #1566	-> 3128
    //   #1595	-> 3132
    //   #1596	-> 3143
    //   #1595	-> 3159
    //   #1600	-> 3159
    //   #1601	-> 3164
    //   #1602	-> 3168
    //   #1604	-> 3176
    //   #1605	-> 3188
    //   #1600	-> 3196
    //   #1611	-> 3200
    //   #1612	-> 3216
    //   #1613	-> 3223
    //   #1612	-> 3238
    //   #1615	-> 3238
    //   #1616	-> 3243
    //   #1617	-> 3252
    //   #1615	-> 3263
    //   #1619	-> 3263
    //   #1620	-> 3268
    //   #1621	-> 3277
    //   #1626	-> 3285
    //   #1628	-> 3297
    //   #1629	-> 3305
    //   #1633	-> 3311
    //   #1634	-> 3317
    //   #1636	-> 3327
    //   #1637	-> 3345
    //   #1636	-> 3351
    //   #1640	-> 3351
    //   #1651	-> 3382
    //   #1652	-> 3392
    //   #1653	-> 3397
    //   #1655	-> 3405
    //   #1656	-> 3410
    //   #1658	-> 3415
    //   #1648	-> 3425
    //   #1649	-> 3432
    //   #1645	-> 3435
    //   #1646	-> 3442
    //   #1642	-> 3445
    //   #1643	-> 3452
    //   #1662	-> 3452
    //   #1664	-> 3484
    //   #1668	-> 3527
    //   #1674	-> 3532
    //   #1668	-> 3560
    //   #1678	-> 3560
    //   #1680	-> 3566
    //   #1681	-> 3571
    //   #1684	-> 3578
    //   #1685	-> 3583
    //   #1687	-> 3606
    //   #1690	-> 3613
    //   #1691	-> 3621
    //   #1692	-> 3628
    //   #1691	-> 3638
    //   #1694	-> 3638
    //   #1695	-> 3645
    //   #1698	-> 3652
    //   #1699	-> 3657
    //   #1702	-> 3662
    //   #1710	-> 3673
    //   #1712	-> 3685
    //   #1713	-> 3709
    //   #1714	-> 3732
    //   #1715	-> 3755
    //   #1717	-> 3760
    //   #1718	-> 3766
    //   #1719	-> 3788
    //   #1721	-> 3796
    //   #1736	-> 3820
    //   #1732	-> 3833
    //   #1733	-> 3843
    //   #1723	-> 3846
    //   #1724	-> 3854
    //   #1725	-> 3864
    //   #1726	-> 3873
    //   #1727	-> 3890
    //   #1724	-> 3898
    //   #1718	-> 3898
    //   #1740	-> 3904
    //   #1746	-> 3908
    //   #1747	-> 3916
    //   #1749	-> 3921
    //   #1750	-> 3927
    //   #1752	-> 3933
    //   #1755	-> 3946
    //   #1756	-> 3953
    //   #1755	-> 3961
    //   #1759	-> 3961
    //   #1760	-> 3968
    //   #1764	-> 3976
    //   #1765	-> 3983
    //   #1767	-> 3991
    //   #1768	-> 3999
    //   #1767	-> 4012
    //   #1774	-> 4012
    //   #1775	-> 4020
    //   #1774	-> 4033
    //   #1781	-> 4033
    //   #1783	-> 4041
    //   #1781	-> 4047
    //   #1786	-> 4047
    //   #1791	-> 4057
    //   #1794	-> 4065
    //   #1797	-> 4070
    //   #1798	-> 4075
    //   #1797	-> 4084
    //   #1800	-> 4084
    //   #1801	-> 4089
    //   #1800	-> 4098
    //   #1803	-> 4098
    //   #1804	-> 4103
    //   #1803	-> 4112
    //   #1806	-> 4112
    // Exception table:
    //   from	to	target	type
    //   1550	1562	1580	org/xmlpull/v1/XmlPullParserException
    //   1550	1562	1565	java/io/IOException
    //   2542	2554	2664	java/lang/ClassNotFoundException
    //   2554	2558	2654	java/lang/InstantiationException
    //   2554	2558	2644	java/lang/IllegalAccessException
    //   2558	2571	2654	java/lang/InstantiationException
    //   2558	2571	2644	java/lang/IllegalAccessException
    //   2571	2581	2640	java/lang/InstantiationException
    //   2571	2581	2636	java/lang/IllegalAccessException
    //   2581	2587	2623	java/lang/IncompatibleClassChangeError
    //   2599	2613	2623	java/lang/IncompatibleClassChangeError
    //   2613	2620	2623	java/lang/IncompatibleClassChangeError
  }
  
  private void setTextInternal(CharSequence paramCharSequence) {
    this.mText = paramCharSequence;
    boolean bool = paramCharSequence instanceof Spannable;
    Spannable spannable1 = null;
    if (bool) {
      spannable2 = (Spannable)paramCharSequence;
    } else {
      spannable2 = null;
    } 
    this.mSpannable = spannable2;
    Spannable spannable2 = spannable1;
    if (paramCharSequence instanceof PrecomputedText)
      spannable2 = (PrecomputedText)paramCharSequence; 
    this.mPrecomputed = (PrecomputedText)spannable2;
    NeuronSystemManager.notifyTextViewContent(this.mContext, getId(), this.mText.toString());
  }
  
  public void setAutoSizeTextTypeWithDefaults(int paramInt) {
    if (supportsAutoSizeText())
      if (paramInt != 0) {
        if (paramInt == 1) {
          DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
          float f1 = TypedValue.applyDimension(2, 12.0F, displayMetrics);
          float f2 = TypedValue.applyDimension(2, 112.0F, displayMetrics);
          validateAndSetAutoSizeTextTypeUniformConfiguration(f1, f2, 1.0F);
          if (setupAutoSizeText()) {
            autoSizeText();
            invalidate();
          } 
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown auto-size text type: ");
          stringBuilder.append(paramInt);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        clearAutoSizeConfiguration();
      }  
  }
  
  public void setAutoSizeTextTypeUniformWithConfiguration(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (supportsAutoSizeText()) {
      DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
      float f1 = TypedValue.applyDimension(paramInt4, paramInt1, displayMetrics);
      float f2 = TypedValue.applyDimension(paramInt4, paramInt2, displayMetrics);
      float f3 = TypedValue.applyDimension(paramInt4, paramInt3, displayMetrics);
      validateAndSetAutoSizeTextTypeUniformConfiguration(f1, f2, f3);
      if (setupAutoSizeText()) {
        autoSizeText();
        invalidate();
      } 
    } 
  }
  
  public void setAutoSizeTextTypeUniformWithPresetSizes(int[] paramArrayOfint, int paramInt) {
    if (supportsAutoSizeText()) {
      int i = paramArrayOfint.length;
      if (i > 0) {
        int[] arrayOfInt2, arrayOfInt1 = new int[i];
        if (paramInt == 0) {
          arrayOfInt2 = Arrays.copyOf(paramArrayOfint, i);
        } else {
          DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
          byte b = 0;
          while (true) {
            arrayOfInt2 = arrayOfInt1;
            if (b < i) {
              arrayOfInt1[b] = Math.round(TypedValue.applyDimension(paramInt, paramArrayOfint[b], displayMetrics));
              b++;
              continue;
            } 
            break;
          } 
        } 
        this.mAutoSizeTextSizesInPx = cleanupAutoSizePresetSizes(arrayOfInt2);
        if (!setupAutoSizeUniformPresetSizesConfiguration()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("None of the preset sizes is valid: ");
          stringBuilder.append(Arrays.toString(paramArrayOfint));
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        this.mHasPresetAutoSizeValues = false;
      } 
      if (setupAutoSizeText()) {
        autoSizeText();
        invalidate();
      } 
    } 
  }
  
  public int getAutoSizeTextType() {
    return this.mAutoSizeTextType;
  }
  
  public int getAutoSizeStepGranularity() {
    return Math.round(this.mAutoSizeStepGranularityInPx);
  }
  
  public int getAutoSizeMinTextSize() {
    return Math.round(this.mAutoSizeMinTextSizeInPx);
  }
  
  public int getAutoSizeMaxTextSize() {
    return Math.round(this.mAutoSizeMaxTextSizeInPx);
  }
  
  public int[] getAutoSizeTextAvailableSizes() {
    return this.mAutoSizeTextSizesInPx;
  }
  
  private void setupAutoSizeUniformPresetSizes(TypedArray paramTypedArray) {
    int i = paramTypedArray.length();
    int[] arrayOfInt = new int[i];
    if (i > 0) {
      for (byte b = 0; b < i; b++)
        arrayOfInt[b] = paramTypedArray.getDimensionPixelSize(b, -1); 
      this.mAutoSizeTextSizesInPx = cleanupAutoSizePresetSizes(arrayOfInt);
      setupAutoSizeUniformPresetSizesConfiguration();
    } 
  }
  
  private boolean setupAutoSizeUniformPresetSizesConfiguration() {
    boolean bool;
    int i = this.mAutoSizeTextSizesInPx.length;
    if (i > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mHasPresetAutoSizeValues = bool;
    if (bool) {
      this.mAutoSizeTextType = 1;
      int[] arrayOfInt = this.mAutoSizeTextSizesInPx;
      this.mAutoSizeMinTextSizeInPx = arrayOfInt[0];
      this.mAutoSizeMaxTextSizeInPx = arrayOfInt[i - 1];
      this.mAutoSizeStepGranularityInPx = -1.0F;
    } 
    return this.mHasPresetAutoSizeValues;
  }
  
  private void validateAndSetAutoSizeTextTypeUniformConfiguration(float paramFloat1, float paramFloat2, float paramFloat3) {
    if (paramFloat1 > 0.0F) {
      if (paramFloat2 > paramFloat1) {
        if (paramFloat3 > 0.0F) {
          this.mAutoSizeTextType = 1;
          this.mAutoSizeMinTextSizeInPx = paramFloat1;
          this.mAutoSizeMaxTextSizeInPx = paramFloat2;
          this.mAutoSizeStepGranularityInPx = paramFloat3;
          this.mHasPresetAutoSizeValues = false;
          return;
        } 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("The auto-size step granularity (");
        stringBuilder2.append(paramFloat3);
        stringBuilder2.append("px) is less or equal to (0px)");
        throw new IllegalArgumentException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Maximum auto-size text size (");
      stringBuilder1.append(paramFloat2);
      stringBuilder1.append("px) is less or equal to minimum auto-size text size (");
      stringBuilder1.append(paramFloat1);
      stringBuilder1.append("px)");
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Minimum auto-size text size (");
    stringBuilder.append(paramFloat1);
    stringBuilder.append("px) is less or equal to (0px)");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void clearAutoSizeConfiguration() {
    this.mAutoSizeTextType = 0;
    this.mAutoSizeMinTextSizeInPx = -1.0F;
    this.mAutoSizeMaxTextSizeInPx = -1.0F;
    this.mAutoSizeStepGranularityInPx = -1.0F;
    this.mAutoSizeTextSizesInPx = EmptyArray.INT;
    this.mNeedsAutoSizeText = false;
  }
  
  private int[] cleanupAutoSizePresetSizes(int[] paramArrayOfint) {
    int i = paramArrayOfint.length;
    if (i == 0)
      return paramArrayOfint; 
    Arrays.sort(paramArrayOfint);
    IntArray intArray = new IntArray();
    for (byte b = 0; b < i; b++) {
      int j = paramArrayOfint[b];
      if (j > 0 && intArray.binarySearch(j) < 0)
        intArray.add(j); 
    } 
    if (i != intArray.size())
      paramArrayOfint = intArray.toArray(); 
    return paramArrayOfint;
  }
  
  private boolean setupAutoSizeText() {
    if (supportsAutoSizeText() && this.mAutoSizeTextType == 1) {
      if (!this.mHasPresetAutoSizeValues || this.mAutoSizeTextSizesInPx.length == 0) {
        int i = (int)Math.floor(((this.mAutoSizeMaxTextSizeInPx - this.mAutoSizeMinTextSizeInPx) / this.mAutoSizeStepGranularityInPx)) + 1;
        int[] arrayOfInt = new int[i];
        for (byte b = 0; b < i; b++)
          arrayOfInt[b] = Math.round(this.mAutoSizeMinTextSizeInPx + b * this.mAutoSizeStepGranularityInPx); 
        this.mAutoSizeTextSizesInPx = cleanupAutoSizePresetSizes(arrayOfInt);
      } 
      this.mNeedsAutoSizeText = true;
    } else {
      this.mNeedsAutoSizeText = false;
    } 
    return this.mNeedsAutoSizeText;
  }
  
  private int[] parseDimensionArray(TypedArray paramTypedArray) {
    if (paramTypedArray == null)
      return null; 
    int[] arrayOfInt = new int[paramTypedArray.length()];
    for (byte b = 0; b < arrayOfInt.length; b++)
      arrayOfInt[b] = paramTypedArray.getDimensionPixelSize(b, 0); 
    return arrayOfInt;
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
    if (paramInt1 == 100)
      if (paramInt2 == -1 && paramIntent != null) {
        CharSequence charSequence = paramIntent.getCharSequenceExtra("android.intent.extra.PROCESS_TEXT");
        if (charSequence != null) {
          Editor editor;
          if (isTextEditable()) {
            replaceSelectionWithText(charSequence);
            editor = this.mEditor;
            if (editor != null)
              editor.refreshTextActionMode(); 
          } else if (editor.length() > 0) {
            Toast toast = Toast.makeText(getContext(), String.valueOf(editor), 1);
            toast.show();
          } 
        } 
      } else {
        Spannable spannable = this.mSpannable;
        if (spannable != null)
          Selection.setSelection(spannable, getSelectionEnd()); 
      }  
  }
  
  private void setTypefaceFromAttrs(Typeface paramTypeface, String paramString, int paramInt1, int paramInt2, int paramInt3) {
    if (paramTypeface == null && paramString != null) {
      paramTypeface = Typeface.create(paramString, 0);
      resolveStyleAndSetTypeface(paramTypeface, paramInt2, paramInt3);
    } else if (paramTypeface != null) {
      resolveStyleAndSetTypeface(paramTypeface, paramInt2, paramInt3);
    } else if (paramInt1 != 1) {
      if (paramInt1 != 2) {
        if (paramInt1 != 3) {
          resolveStyleAndSetTypeface((Typeface)null, paramInt2, paramInt3);
        } else {
          resolveStyleAndSetTypeface(Typeface.MONOSPACE, paramInt2, paramInt3);
        } 
      } else {
        resolveStyleAndSetTypeface(Typeface.SERIF, paramInt2, paramInt3);
      } 
    } else {
      resolveStyleAndSetTypeface(Typeface.SANS_SERIF, paramInt2, paramInt3);
    } 
  }
  
  private void resolveStyleAndSetTypeface(Typeface paramTypeface, int paramInt1, int paramInt2) {
    boolean bool = false;
    if (paramInt2 >= 0) {
      paramInt2 = Math.min(1000, paramInt2);
      if ((paramInt1 & 0x2) != 0)
        bool = true; 
      setTypeface(Typeface.create(paramTypeface, paramInt2, bool));
    } else {
      ((IOplusFontManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFontManager.DEFAULT, new Object[0])).replaceFakeBoldToMedium(this, paramTypeface, paramInt1);
    } 
  }
  
  private void setRelativeDrawablesIfNeeded(Drawable paramDrawable1, Drawable paramDrawable2) {
    boolean bool;
    if (paramDrawable1 != null || paramDrawable2 != null) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      Drawables drawables1 = this.mDrawables;
      Drawables drawables2 = drawables1;
      if (drawables1 == null) {
        drawables2 = drawables1 = new Drawables(getContext());
        this.mDrawables = drawables1;
      } 
      this.mDrawables.mOverride = true;
      Rect rect = drawables2.mCompoundRect;
      int[] arrayOfInt = getDrawableState();
      if (paramDrawable1 != null) {
        paramDrawable1.setBounds(0, 0, paramDrawable1.getIntrinsicWidth(), paramDrawable1.getIntrinsicHeight());
        paramDrawable1.setState(arrayOfInt);
        paramDrawable1.copyBounds(rect);
        paramDrawable1.setCallback(this);
        drawables2.mDrawableStart = paramDrawable1;
        drawables2.mDrawableSizeStart = rect.width();
        drawables2.mDrawableHeightStart = rect.height();
      } else {
        drawables2.mDrawableHeightStart = 0;
        drawables2.mDrawableSizeStart = 0;
      } 
      if (paramDrawable2 != null) {
        paramDrawable2.setBounds(0, 0, paramDrawable2.getIntrinsicWidth(), paramDrawable2.getIntrinsicHeight());
        paramDrawable2.setState(arrayOfInt);
        paramDrawable2.copyBounds(rect);
        paramDrawable2.setCallback(this);
        drawables2.mDrawableEnd = paramDrawable2;
        drawables2.mDrawableSizeEnd = rect.width();
        drawables2.mDrawableHeightEnd = rect.height();
      } else {
        drawables2.mDrawableHeightEnd = 0;
        drawables2.mDrawableSizeEnd = 0;
      } 
      resetResolvedDrawables();
      resolveDrawables();
      applyCompoundDrawableTint();
    } 
  }
  
  @RemotableViewMethod
  public void setEnabled(boolean paramBoolean) {
    if (paramBoolean == isEnabled())
      return; 
    if (!paramBoolean) {
      InputMethodManager inputMethodManager = getInputMethodManager();
      if (inputMethodManager != null && inputMethodManager.isActive(this))
        inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0); 
    } 
    super.setEnabled(paramBoolean);
    if (paramBoolean) {
      InputMethodManager inputMethodManager = getInputMethodManager();
      if (inputMethodManager != null)
        inputMethodManager.restartInput(this); 
    } 
    Editor editor = this.mEditor;
    if (editor != null) {
      editor.invalidateTextDisplayList();
      this.mEditor.prepareCursorControllers();
      this.mEditor.makeBlink();
    } 
  }
  
  public void setTypeface(Typeface paramTypeface, int paramInt) {
    TextPaint textPaint;
    float f = 0.0F;
    boolean bool = false;
    if (paramInt > 0) {
      boolean bool1;
      if (paramTypeface == null) {
        paramTypeface = Typeface.defaultFromStyle(paramInt);
      } else {
        paramTypeface = Typeface.create(paramTypeface, paramInt);
      } 
      setTypeface(paramTypeface);
      if (paramTypeface != null) {
        bool1 = paramTypeface.getStyle();
      } else {
        bool1 = false;
      } 
      paramInt = (bool1 ^ 0xFFFFFFFF) & paramInt;
      textPaint = this.mTextPaint;
      if ((paramInt & 0x1) != 0)
        bool = true; 
      textPaint.setFakeBoldText(bool);
      textPaint = this.mTextPaint;
      if ((paramInt & 0x2) != 0)
        f = -0.25F; 
      textPaint.setTextSkewX(f);
    } else {
      this.mTextPaint.setFakeBoldText(false);
      this.mTextPaint.setTextSkewX(0.0F);
      setTypeface((Typeface)textPaint);
    } 
  }
  
  protected boolean getDefaultEditable() {
    return false;
  }
  
  protected MovementMethod getDefaultMovementMethod() {
    return null;
  }
  
  @CapturedViewProperty
  public CharSequence getText() {
    return this.mText;
  }
  
  public int length() {
    return this.mText.length();
  }
  
  public Editable getEditableText() {
    CharSequence charSequence = this.mText;
    if (charSequence instanceof Editable) {
      charSequence = charSequence;
    } else {
      charSequence = null;
    } 
    return (Editable)charSequence;
  }
  
  public CharSequence getTransformed() {
    return this.mTransformed;
  }
  
  public int getLineHeight() {
    return FastMath.round(this.mTextPaint.getFontMetricsInt(null) * this.mSpacingMult + this.mSpacingAdd);
  }
  
  public final Layout getLayout() {
    return this.mLayout;
  }
  
  final Layout getHintLayout() {
    return this.mHintLayout;
  }
  
  public final UndoManager getUndoManager() {
    throw new UnsupportedOperationException("not implemented");
  }
  
  public final Editor getEditorForTesting() {
    return this.mEditor;
  }
  
  public final void setUndoManager(UndoManager paramUndoManager, String paramString) {
    throw new UnsupportedOperationException("not implemented");
  }
  
  public final KeyListener getKeyListener() {
    KeyListener keyListener;
    Editor editor = this.mEditor;
    if (editor == null) {
      editor = null;
    } else {
      keyListener = editor.mKeyListener;
    } 
    return keyListener;
  }
  
  public void setKeyListener(KeyListener paramKeyListener) {
    this.mListenerChanged = true;
    setKeyListenerOnly(paramKeyListener);
    fixFocusableAndClickableSettings();
    if (paramKeyListener != null) {
      createEditorIfNeeded();
      setInputTypeFromEditor();
    } else {
      Editor editor = this.mEditor;
      if (editor != null)
        editor.mInputType = 0; 
    } 
    InputMethodManager inputMethodManager = getInputMethodManager();
    if (inputMethodManager != null)
      inputMethodManager.restartInput(this); 
  }
  
  private void setInputTypeFromEditor() {
    try {
      this.mEditor.mInputType = this.mEditor.mKeyListener.getInputType();
    } catch (IncompatibleClassChangeError incompatibleClassChangeError) {
      this.mEditor.mInputType = 1;
    } 
    setInputTypeSingleLine(this.mSingleLine);
  }
  
  private void setKeyListenerOnly(KeyListener paramKeyListener) {
    if (this.mEditor == null && paramKeyListener == null)
      return; 
    createEditorIfNeeded();
    if (this.mEditor.mKeyListener != paramKeyListener) {
      this.mEditor.mKeyListener = paramKeyListener;
      if (paramKeyListener != null) {
        CharSequence charSequence = this.mText;
        if (!(charSequence instanceof Editable))
          setText(charSequence); 
      } 
      setFilters((Editable)this.mText, this.mFilters);
    } 
  }
  
  public final MovementMethod getMovementMethod() {
    return this.mMovement;
  }
  
  public final void setMovementMethod(MovementMethod paramMovementMethod) {
    if (this.mMovement != paramMovementMethod) {
      this.mMovement = paramMovementMethod;
      if (paramMovementMethod != null && this.mSpannable == null)
        setText(this.mText); 
      fixFocusableAndClickableSettings();
      Editor editor = this.mEditor;
      if (editor != null)
        editor.prepareCursorControllers(); 
    } 
  }
  
  private void fixFocusableAndClickableSettings() {
    if (this.mMovement == null) {
      Editor editor = this.mEditor;
      if (editor != null && editor.mKeyListener != null) {
        setFocusable(1);
        setClickable(true);
        setLongClickable(true);
        return;
      } 
      setFocusable(16);
      setClickable(false);
      setLongClickable(false);
      return;
    } 
    setFocusable(1);
    setClickable(true);
    setLongClickable(true);
  }
  
  public final TransformationMethod getTransformationMethod() {
    return this.mTransformation;
  }
  
  public final void setTransformationMethod(TransformationMethod paramTransformationMethod) {
    TransformationMethod transformationMethod = this.mTransformation;
    if (paramTransformationMethod == transformationMethod)
      return; 
    if (transformationMethod != null) {
      Spannable spannable = this.mSpannable;
      if (spannable != null)
        spannable.removeSpan(transformationMethod); 
    } 
    this.mTransformation = paramTransformationMethod;
    if (paramTransformationMethod instanceof android.text.method.TransformationMethod2) {
      boolean bool;
      paramTransformationMethod = paramTransformationMethod;
      if (!isTextSelectable() && !(this.mText instanceof Editable)) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mAllowTransformationLengthChange = bool;
      paramTransformationMethod.setLengthChangesAllowed(bool);
    } else {
      this.mAllowTransformationLengthChange = false;
    } 
    setText(this.mText);
    if (hasPasswordTransformationMethod())
      notifyViewAccessibilityStateChangedIfNeeded(0); 
    this.mTextDir = getTextDirectionHeuristic();
  }
  
  public int getCompoundPaddingTop() {
    Drawables drawables = this.mDrawables;
    if (drawables == null || drawables.mShowing[1] == null)
      return this.mPaddingTop; 
    return this.mPaddingTop + drawables.mDrawablePadding + drawables.mDrawableSizeTop;
  }
  
  public int getCompoundPaddingBottom() {
    Drawables drawables = this.mDrawables;
    if (drawables == null || drawables.mShowing[3] == null)
      return this.mPaddingBottom; 
    return this.mPaddingBottom + drawables.mDrawablePadding + drawables.mDrawableSizeBottom;
  }
  
  public int getCompoundPaddingLeft() {
    Drawables drawables = this.mDrawables;
    if (drawables == null || drawables.mShowing[0] == null)
      return this.mPaddingLeft; 
    return this.mPaddingLeft + drawables.mDrawablePadding + drawables.mDrawableSizeLeft;
  }
  
  public int getCompoundPaddingRight() {
    Drawables drawables = this.mDrawables;
    if (drawables == null || drawables.mShowing[2] == null)
      return this.mPaddingRight; 
    return this.mPaddingRight + drawables.mDrawablePadding + drawables.mDrawableSizeRight;
  }
  
  public int getCompoundPaddingStart() {
    resolveDrawables();
    if (getLayoutDirection() != 1)
      return getCompoundPaddingLeft(); 
    return getCompoundPaddingRight();
  }
  
  public int getCompoundPaddingEnd() {
    resolveDrawables();
    if (getLayoutDirection() != 1)
      return getCompoundPaddingRight(); 
    return getCompoundPaddingLeft();
  }
  
  public int getExtendedPaddingTop() {
    if (this.mMaxMode != 1)
      return getCompoundPaddingTop(); 
    if (this.mLayout == null)
      assumeLayout(); 
    if (this.mLayout.getLineCount() <= this.mMaximum)
      return getCompoundPaddingTop(); 
    int i = getCompoundPaddingTop();
    int j = getCompoundPaddingBottom();
    int k = getHeight() - i - j;
    j = this.mLayout.getLineTop(this.mMaximum);
    if (j >= k)
      return i; 
    int m = this.mGravity & 0x70;
    if (m == 48)
      return i; 
    if (m == 80)
      return i + k - j; 
    return (k - j) / 2 + i;
  }
  
  public int getExtendedPaddingBottom() {
    if (this.mMaxMode != 1)
      return getCompoundPaddingBottom(); 
    if (this.mLayout == null)
      assumeLayout(); 
    if (this.mLayout.getLineCount() <= this.mMaximum)
      return getCompoundPaddingBottom(); 
    int i = getCompoundPaddingTop();
    int j = getCompoundPaddingBottom();
    int k = getHeight() - i - j;
    int m = this.mLayout.getLineTop(this.mMaximum);
    if (m >= k)
      return j; 
    i = this.mGravity & 0x70;
    if (i == 48)
      return j + k - m; 
    if (i == 80)
      return j; 
    return (k - m) / 2 + j;
  }
  
  public int getTotalPaddingLeft() {
    return getCompoundPaddingLeft();
  }
  
  public int getTotalPaddingRight() {
    return getCompoundPaddingRight();
  }
  
  public int getTotalPaddingStart() {
    return getCompoundPaddingStart();
  }
  
  public int getTotalPaddingEnd() {
    return getCompoundPaddingEnd();
  }
  
  public int getTotalPaddingTop() {
    return getExtendedPaddingTop() + getVerticalOffset(true);
  }
  
  public int getTotalPaddingBottom() {
    return getExtendedPaddingBottom() + getBottomVerticalOffset(true);
  }
  
  public void setCompoundDrawables(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4) {
    int i;
    Drawables drawables2, drawables1 = this.mDrawables;
    if (drawables1 != null) {
      if (drawables1.mDrawableStart != null)
        drawables1.mDrawableStart.setCallback(null); 
      drawables1.mDrawableStart = null;
      if (drawables1.mDrawableEnd != null)
        drawables1.mDrawableEnd.setCallback(null); 
      drawables1.mDrawableEnd = null;
      drawables1.mDrawableHeightStart = 0;
      drawables1.mDrawableSizeStart = 0;
      drawables1.mDrawableHeightEnd = 0;
      drawables1.mDrawableSizeEnd = 0;
    } 
    if (paramDrawable1 != null || paramDrawable2 != null || paramDrawable3 != null || paramDrawable4 != null) {
      i = 1;
    } else {
      i = 0;
    } 
    if (!i) {
      drawables2 = drawables1;
      if (drawables1 != null)
        if (!drawables1.hasMetadata()) {
          this.mDrawables = null;
          drawables2 = drawables1;
        } else {
          for (i = drawables1.mShowing.length - 1; i >= 0; i--) {
            if (drawables1.mShowing[i] != null)
              drawables1.mShowing[i].setCallback(null); 
            drawables1.mShowing[i] = null;
          } 
          drawables1.mDrawableHeightLeft = 0;
          drawables1.mDrawableSizeLeft = 0;
          drawables1.mDrawableHeightRight = 0;
          drawables1.mDrawableSizeRight = 0;
          drawables1.mDrawableWidthTop = 0;
          drawables1.mDrawableSizeTop = 0;
          drawables1.mDrawableWidthBottom = 0;
          drawables1.mDrawableSizeBottom = 0;
          drawables2 = drawables1;
        }  
    } else {
      drawables2 = drawables1;
      if (drawables1 == null) {
        drawables2 = drawables1 = new Drawables(getContext());
        this.mDrawables = drawables1;
      } 
      this.mDrawables.mOverride = false;
      if (drawables2.mShowing[0] != paramDrawable1 && drawables2.mShowing[0] != null)
        drawables2.mShowing[0].setCallback(null); 
      drawables2.mShowing[0] = paramDrawable1;
      if (drawables2.mShowing[1] != paramDrawable2 && drawables2.mShowing[1] != null)
        drawables2.mShowing[1].setCallback(null); 
      drawables2.mShowing[1] = paramDrawable2;
      if (drawables2.mShowing[2] != paramDrawable3 && drawables2.mShowing[2] != null)
        drawables2.mShowing[2].setCallback(null); 
      drawables2.mShowing[2] = paramDrawable3;
      if (drawables2.mShowing[3] != paramDrawable4 && drawables2.mShowing[3] != null)
        drawables2.mShowing[3].setCallback(null); 
      drawables2.mShowing[3] = paramDrawable4;
      Rect rect = drawables2.mCompoundRect;
      int[] arrayOfInt = getDrawableState();
      if (paramDrawable1 != null) {
        paramDrawable1.setState(arrayOfInt);
        paramDrawable1.copyBounds(rect);
        paramDrawable1.setCallback(this);
        drawables2.mDrawableSizeLeft = rect.width();
        drawables2.mDrawableHeightLeft = rect.height();
      } else {
        drawables2.mDrawableHeightLeft = 0;
        drawables2.mDrawableSizeLeft = 0;
      } 
      if (paramDrawable3 != null) {
        paramDrawable3.setState(arrayOfInt);
        paramDrawable3.copyBounds(rect);
        paramDrawable3.setCallback(this);
        drawables2.mDrawableSizeRight = rect.width();
        drawables2.mDrawableHeightRight = rect.height();
      } else {
        drawables2.mDrawableHeightRight = 0;
        drawables2.mDrawableSizeRight = 0;
      } 
      if (paramDrawable2 != null) {
        paramDrawable2.setState(arrayOfInt);
        paramDrawable2.copyBounds(rect);
        paramDrawable2.setCallback(this);
        drawables2.mDrawableSizeTop = rect.height();
        drawables2.mDrawableWidthTop = rect.width();
      } else {
        drawables2.mDrawableWidthTop = 0;
        drawables2.mDrawableSizeTop = 0;
      } 
      if (paramDrawable4 != null) {
        paramDrawable4.setState(arrayOfInt);
        paramDrawable4.copyBounds(rect);
        paramDrawable4.setCallback(this);
        drawables2.mDrawableSizeBottom = rect.height();
        drawables2.mDrawableWidthBottom = rect.width();
      } else {
        drawables2.mDrawableWidthBottom = 0;
        drawables2.mDrawableSizeBottom = 0;
      } 
    } 
    if (drawables2 != null) {
      drawables2.mDrawableLeftInitial = paramDrawable1;
      drawables2.mDrawableRightInitial = paramDrawable3;
    } 
    resetResolvedDrawables();
    resolveDrawables();
    applyCompoundDrawableTint();
    invalidate();
    requestLayout();
  }
  
  @RemotableViewMethod
  public void setCompoundDrawablesWithIntrinsicBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    Drawable drawable2, drawable3, drawable4;
    Context context = getContext();
    Drawable drawable1 = null;
    if (paramInt1 != 0) {
      drawable2 = context.getDrawable(paramInt1);
    } else {
      drawable2 = null;
    } 
    if (paramInt2 != 0) {
      drawable3 = context.getDrawable(paramInt2);
    } else {
      drawable3 = null;
    } 
    if (paramInt3 != 0) {
      drawable4 = context.getDrawable(paramInt3);
    } else {
      drawable4 = null;
    } 
    if (paramInt4 != 0)
      drawable1 = context.getDrawable(paramInt4); 
    setCompoundDrawablesWithIntrinsicBounds(drawable2, drawable3, drawable4, drawable1);
  }
  
  @RemotableViewMethod
  public void setCompoundDrawablesWithIntrinsicBounds(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4) {
    if (paramDrawable1 != null)
      paramDrawable1.setBounds(0, 0, paramDrawable1.getIntrinsicWidth(), paramDrawable1.getIntrinsicHeight()); 
    if (paramDrawable3 != null)
      paramDrawable3.setBounds(0, 0, paramDrawable3.getIntrinsicWidth(), paramDrawable3.getIntrinsicHeight()); 
    if (paramDrawable2 != null)
      paramDrawable2.setBounds(0, 0, paramDrawable2.getIntrinsicWidth(), paramDrawable2.getIntrinsicHeight()); 
    if (paramDrawable4 != null)
      paramDrawable4.setBounds(0, 0, paramDrawable4.getIntrinsicWidth(), paramDrawable4.getIntrinsicHeight()); 
    setCompoundDrawables(paramDrawable1, paramDrawable2, paramDrawable3, paramDrawable4);
  }
  
  @RemotableViewMethod
  public void setCompoundDrawablesRelative(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4) {
    boolean bool;
    Drawables drawables = this.mDrawables;
    if (drawables != null) {
      if (drawables.mShowing[0] != null)
        drawables.mShowing[0].setCallback(null); 
      Drawable[] arrayOfDrawable = drawables.mShowing;
      drawables.mDrawableLeftInitial = null;
      arrayOfDrawable[0] = null;
      if (drawables.mShowing[2] != null)
        drawables.mShowing[2].setCallback(null); 
      arrayOfDrawable = drawables.mShowing;
      drawables.mDrawableRightInitial = null;
      arrayOfDrawable[2] = null;
      drawables.mDrawableHeightLeft = 0;
      drawables.mDrawableSizeLeft = 0;
      drawables.mDrawableHeightRight = 0;
      drawables.mDrawableSizeRight = 0;
    } 
    if (paramDrawable1 != null || paramDrawable2 != null || paramDrawable3 != null || paramDrawable4 != null) {
      bool = true;
    } else {
      bool = false;
    } 
    if (!bool) {
      if (drawables != null)
        if (!drawables.hasMetadata()) {
          this.mDrawables = null;
        } else {
          if (drawables.mDrawableStart != null)
            drawables.mDrawableStart.setCallback(null); 
          drawables.mDrawableStart = null;
          if (drawables.mShowing[1] != null)
            drawables.mShowing[1].setCallback(null); 
          drawables.mShowing[1] = null;
          if (drawables.mDrawableEnd != null)
            drawables.mDrawableEnd.setCallback(null); 
          drawables.mDrawableEnd = null;
          if (drawables.mShowing[3] != null)
            drawables.mShowing[3].setCallback(null); 
          drawables.mShowing[3] = null;
          drawables.mDrawableHeightStart = 0;
          drawables.mDrawableSizeStart = 0;
          drawables.mDrawableHeightEnd = 0;
          drawables.mDrawableSizeEnd = 0;
          drawables.mDrawableWidthTop = 0;
          drawables.mDrawableSizeTop = 0;
          drawables.mDrawableWidthBottom = 0;
          drawables.mDrawableSizeBottom = 0;
        }  
    } else {
      Drawables drawables1 = drawables;
      if (drawables == null) {
        drawables1 = drawables = new Drawables(getContext());
        this.mDrawables = drawables;
      } 
      this.mDrawables.mOverride = true;
      if (drawables1.mDrawableStart != paramDrawable1 && drawables1.mDrawableStart != null)
        drawables1.mDrawableStart.setCallback(null); 
      drawables1.mDrawableStart = paramDrawable1;
      if (drawables1.mShowing[1] != paramDrawable2 && drawables1.mShowing[1] != null)
        drawables1.mShowing[1].setCallback(null); 
      drawables1.mShowing[1] = paramDrawable2;
      if (drawables1.mDrawableEnd != paramDrawable3 && drawables1.mDrawableEnd != null)
        drawables1.mDrawableEnd.setCallback(null); 
      drawables1.mDrawableEnd = paramDrawable3;
      if (drawables1.mShowing[3] != paramDrawable4 && drawables1.mShowing[3] != null)
        drawables1.mShowing[3].setCallback(null); 
      drawables1.mShowing[3] = paramDrawable4;
      Rect rect = drawables1.mCompoundRect;
      int[] arrayOfInt = getDrawableState();
      if (paramDrawable1 != null) {
        paramDrawable1.setState(arrayOfInt);
        paramDrawable1.copyBounds(rect);
        paramDrawable1.setCallback(this);
        drawables1.mDrawableSizeStart = rect.width();
        drawables1.mDrawableHeightStart = rect.height();
      } else {
        drawables1.mDrawableHeightStart = 0;
        drawables1.mDrawableSizeStart = 0;
      } 
      if (paramDrawable3 != null) {
        paramDrawable3.setState(arrayOfInt);
        paramDrawable3.copyBounds(rect);
        paramDrawable3.setCallback(this);
        drawables1.mDrawableSizeEnd = rect.width();
        drawables1.mDrawableHeightEnd = rect.height();
      } else {
        drawables1.mDrawableHeightEnd = 0;
        drawables1.mDrawableSizeEnd = 0;
      } 
      if (paramDrawable2 != null) {
        paramDrawable2.setState(arrayOfInt);
        paramDrawable2.copyBounds(rect);
        paramDrawable2.setCallback(this);
        drawables1.mDrawableSizeTop = rect.height();
        drawables1.mDrawableWidthTop = rect.width();
      } else {
        drawables1.mDrawableWidthTop = 0;
        drawables1.mDrawableSizeTop = 0;
      } 
      if (paramDrawable4 != null) {
        paramDrawable4.setState(arrayOfInt);
        paramDrawable4.copyBounds(rect);
        paramDrawable4.setCallback(this);
        drawables1.mDrawableSizeBottom = rect.height();
        drawables1.mDrawableWidthBottom = rect.width();
      } else {
        drawables1.mDrawableWidthBottom = 0;
        drawables1.mDrawableSizeBottom = 0;
      } 
    } 
    resetResolvedDrawables();
    resolveDrawables();
    invalidate();
    requestLayout();
  }
  
  @RemotableViewMethod
  public void setCompoundDrawablesRelativeWithIntrinsicBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    Drawable drawable2, drawable3, drawable4;
    Context context = getContext();
    Drawable drawable1 = null;
    if (paramInt1 != 0) {
      drawable2 = context.getDrawable(paramInt1);
    } else {
      drawable2 = null;
    } 
    if (paramInt2 != 0) {
      drawable3 = context.getDrawable(paramInt2);
    } else {
      drawable3 = null;
    } 
    if (paramInt3 != 0) {
      drawable4 = context.getDrawable(paramInt3);
    } else {
      drawable4 = null;
    } 
    if (paramInt4 != 0)
      drawable1 = context.getDrawable(paramInt4); 
    setCompoundDrawablesRelativeWithIntrinsicBounds(drawable2, drawable3, drawable4, drawable1);
  }
  
  @RemotableViewMethod
  public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4) {
    if (paramDrawable1 != null)
      paramDrawable1.setBounds(0, 0, paramDrawable1.getIntrinsicWidth(), paramDrawable1.getIntrinsicHeight()); 
    if (paramDrawable3 != null)
      paramDrawable3.setBounds(0, 0, paramDrawable3.getIntrinsicWidth(), paramDrawable3.getIntrinsicHeight()); 
    if (paramDrawable2 != null)
      paramDrawable2.setBounds(0, 0, paramDrawable2.getIntrinsicWidth(), paramDrawable2.getIntrinsicHeight()); 
    if (paramDrawable4 != null)
      paramDrawable4.setBounds(0, 0, paramDrawable4.getIntrinsicWidth(), paramDrawable4.getIntrinsicHeight()); 
    setCompoundDrawablesRelative(paramDrawable1, paramDrawable2, paramDrawable3, paramDrawable4);
  }
  
  public Drawable[] getCompoundDrawables() {
    Drawables drawables = this.mDrawables;
    if (drawables != null)
      return (Drawable[])drawables.mShowing.clone(); 
    return new Drawable[] { null, null, null, null };
  }
  
  public Drawable[] getCompoundDrawablesRelative() {
    Drawables drawables = this.mDrawables;
    if (drawables != null)
      return new Drawable[] { drawables.mDrawableStart, drawables.mShowing[1], drawables.mDrawableEnd, drawables.mShowing[3] }; 
    return new Drawable[] { null, null, null, null };
  }
  
  @RemotableViewMethod
  public void setCompoundDrawablePadding(int paramInt) {
    Drawables drawables = this.mDrawables;
    if (paramInt == 0) {
      if (drawables != null)
        drawables.mDrawablePadding = paramInt; 
    } else {
      Drawables drawables1 = drawables;
      if (drawables == null) {
        drawables1 = drawables = new Drawables(getContext());
        this.mDrawables = drawables;
      } 
      drawables1.mDrawablePadding = paramInt;
    } 
    invalidate();
    requestLayout();
  }
  
  public int getCompoundDrawablePadding() {
    boolean bool;
    Drawables drawables = this.mDrawables;
    if (drawables != null) {
      bool = drawables.mDrawablePadding;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setCompoundDrawableTintList(ColorStateList paramColorStateList) {
    if (this.mDrawables == null)
      this.mDrawables = new Drawables(getContext()); 
    this.mDrawables.mTintList = paramColorStateList;
    this.mDrawables.mHasTint = true;
    applyCompoundDrawableTint();
  }
  
  public ColorStateList getCompoundDrawableTintList() {
    Drawables drawables = this.mDrawables;
    if (drawables != null) {
      ColorStateList colorStateList = drawables.mTintList;
    } else {
      drawables = null;
    } 
    return (ColorStateList)drawables;
  }
  
  public void setCompoundDrawableTintMode(PorterDuff.Mode paramMode) {
    if (paramMode != null) {
      BlendMode blendMode = BlendMode.fromValue(paramMode.nativeInt);
    } else {
      paramMode = null;
    } 
    setCompoundDrawableTintBlendMode((BlendMode)paramMode);
  }
  
  public void setCompoundDrawableTintBlendMode(BlendMode paramBlendMode) {
    if (this.mDrawables == null)
      this.mDrawables = new Drawables(getContext()); 
    this.mDrawables.mBlendMode = paramBlendMode;
    this.mDrawables.mHasTintMode = true;
    applyCompoundDrawableTint();
  }
  
  public PorterDuff.Mode getCompoundDrawableTintMode() {
    BlendMode blendMode = getCompoundDrawableTintBlendMode();
    if (blendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(blendMode);
    } else {
      blendMode = null;
    } 
    return (PorterDuff.Mode)blendMode;
  }
  
  public BlendMode getCompoundDrawableTintBlendMode() {
    Drawables drawables = this.mDrawables;
    if (drawables != null) {
      BlendMode blendMode = drawables.mBlendMode;
    } else {
      drawables = null;
    } 
    return (BlendMode)drawables;
  }
  
  private void applyCompoundDrawableTint() {
    Drawables drawables = this.mDrawables;
    if (drawables == null)
      return; 
    if (drawables.mHasTint || this.mDrawables.mHasTintMode) {
      ColorStateList colorStateList = this.mDrawables.mTintList;
      BlendMode blendMode = this.mDrawables.mBlendMode;
      boolean bool1 = this.mDrawables.mHasTint;
      boolean bool2 = this.mDrawables.mHasTintMode;
      int[] arrayOfInt = getDrawableState();
      for (Drawable drawable : this.mDrawables.mShowing) {
        if (drawable != null)
          if (drawable != this.mDrawables.mDrawableError) {
            drawable.mutate();
            if (bool1)
              drawable.setTintList(colorStateList); 
            if (bool2)
              drawable.setTintBlendMode(blendMode); 
            if (drawable.isStateful())
              drawable.setState(arrayOfInt); 
          }  
      } 
    } 
  }
  
  public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt1 != this.mPaddingLeft || paramInt3 != this.mPaddingRight || paramInt2 != this.mPaddingTop || paramInt4 != this.mPaddingBottom)
      nullLayouts(); 
    super.setPadding(paramInt1, paramInt2, paramInt3, paramInt4);
    invalidate();
  }
  
  public void setPaddingRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt1 != getPaddingStart() || paramInt3 != getPaddingEnd() || paramInt2 != this.mPaddingTop || paramInt4 != this.mPaddingBottom)
      nullLayouts(); 
    super.setPaddingRelative(paramInt1, paramInt2, paramInt3, paramInt4);
    invalidate();
  }
  
  public void setFirstBaselineToTopHeight(int paramInt) {
    int i;
    Preconditions.checkArgumentNonnegative(paramInt);
    Paint.FontMetricsInt fontMetricsInt = getPaint().getFontMetricsInt();
    if (getIncludeFontPadding()) {
      i = fontMetricsInt.top;
    } else {
      i = fontMetricsInt.ascent;
    } 
    if (paramInt > Math.abs(i)) {
      i = -i;
      setPadding(getPaddingLeft(), paramInt - i, getPaddingRight(), getPaddingBottom());
    } 
  }
  
  public void setLastBaselineToBottomHeight(int paramInt) {
    int i;
    Preconditions.checkArgumentNonnegative(paramInt);
    Paint.FontMetricsInt fontMetricsInt = getPaint().getFontMetricsInt();
    if (getIncludeFontPadding()) {
      i = fontMetricsInt.bottom;
    } else {
      i = fontMetricsInt.descent;
    } 
    if (paramInt > Math.abs(i))
      setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), paramInt - i); 
  }
  
  public int getFirstBaselineToTopHeight() {
    return getPaddingTop() - (getPaint().getFontMetricsInt()).top;
  }
  
  public int getLastBaselineToBottomHeight() {
    return getPaddingBottom() + (getPaint().getFontMetricsInt()).bottom;
  }
  
  public final int getAutoLinkMask() {
    return this.mAutoLinkMask;
  }
  
  @RemotableViewMethod
  public void setTextSelectHandle(Drawable paramDrawable) {
    Preconditions.checkNotNull(paramDrawable, "The text select handle should not be null.");
    this.mTextSelectHandle = paramDrawable;
    this.mTextSelectHandleRes = 0;
    Editor editor = this.mEditor;
    if (editor != null)
      editor.loadHandleDrawables(true); 
  }
  
  @RemotableViewMethod
  public void setTextSelectHandle(int paramInt) {
    boolean bool;
    if (paramInt != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "The text select handle should be a valid drawable resource id.");
    setTextSelectHandle(this.mContext.getDrawable(paramInt));
  }
  
  public Drawable getTextSelectHandle() {
    if (this.mTextSelectHandle == null && this.mTextSelectHandleRes != 0)
      this.mTextSelectHandle = this.mContext.getDrawable(this.mTextSelectHandleRes); 
    return this.mTextSelectHandle;
  }
  
  @RemotableViewMethod
  public void setTextSelectHandleLeft(Drawable paramDrawable) {
    Preconditions.checkNotNull(paramDrawable, "The left text select handle should not be null.");
    this.mTextSelectHandleLeft = paramDrawable;
    this.mTextSelectHandleLeftRes = 0;
    Editor editor = this.mEditor;
    if (editor != null)
      editor.loadHandleDrawables(true); 
  }
  
  @RemotableViewMethod
  public void setTextSelectHandleLeft(int paramInt) {
    boolean bool;
    if (paramInt != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "The text select left handle should be a valid drawable resource id.");
    setTextSelectHandleLeft(this.mContext.getDrawable(paramInt));
  }
  
  public Drawable getTextSelectHandleLeft() {
    if (this.mTextSelectHandleLeft == null && this.mTextSelectHandleLeftRes != 0)
      this.mTextSelectHandleLeft = this.mContext.getDrawable(this.mTextSelectHandleLeftRes); 
    return this.mTextSelectHandleLeft;
  }
  
  @RemotableViewMethod
  public void setTextSelectHandleRight(Drawable paramDrawable) {
    Preconditions.checkNotNull(paramDrawable, "The right text select handle should not be null.");
    this.mTextSelectHandleRight = paramDrawable;
    this.mTextSelectHandleRightRes = 0;
    Editor editor = this.mEditor;
    if (editor != null)
      editor.loadHandleDrawables(true); 
  }
  
  @RemotableViewMethod
  public void setTextSelectHandleRight(int paramInt) {
    boolean bool;
    if (paramInt != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "The text select right handle should be a valid drawable resource id.");
    setTextSelectHandleRight(this.mContext.getDrawable(paramInt));
  }
  
  public Drawable getTextSelectHandleRight() {
    if (this.mTextSelectHandleRight == null && this.mTextSelectHandleRightRes != 0)
      this.mTextSelectHandleRight = this.mContext.getDrawable(this.mTextSelectHandleRightRes); 
    return this.mTextSelectHandleRight;
  }
  
  public void setTextCursorDrawable(Drawable paramDrawable) {
    this.mCursorDrawable = paramDrawable;
    this.mCursorDrawableRes = 0;
    Editor editor = this.mEditor;
    if (editor != null)
      editor.loadCursorDrawable(); 
  }
  
  public void setTextCursorDrawable(int paramInt) {
    Drawable drawable;
    if (paramInt != 0) {
      drawable = this.mContext.getDrawable(paramInt);
    } else {
      drawable = null;
    } 
    setTextCursorDrawable(drawable);
  }
  
  public Drawable getTextCursorDrawable() {
    if (this.mCursorDrawable == null && this.mCursorDrawableRes != 0)
      this.mCursorDrawable = this.mContext.getDrawable(this.mCursorDrawableRes); 
    return this.mCursorDrawable;
  }
  
  public void setTextAppearance(int paramInt) {
    setTextAppearance(this.mContext, paramInt);
  }
  
  @Deprecated
  public void setTextAppearance(Context paramContext, int paramInt) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramInt, R.styleable.TextAppearance);
    TextAppearanceAttributes textAppearanceAttributes = new TextAppearanceAttributes();
    readTextAppearance(paramContext, typedArray, textAppearanceAttributes, false);
    typedArray.recycle();
    applyTextAppearance(textAppearanceAttributes);
  }
  
  class TextAppearanceAttributes {
    private TextAppearanceAttributes() {}
    
    int mTextColorHighlight = 0;
    
    ColorStateList mTextColor = null;
    
    ColorStateList mTextColorHint = null;
    
    ColorStateList mTextColorLink = null;
    
    int mTextSize = -1;
    
    int mTextSizeUnit = -1;
    
    LocaleList mTextLocales = null;
    
    String mFontFamily = null;
    
    Typeface mFontTypeface = null;
    
    boolean mFontFamilyExplicit = false;
    
    int mTypefaceIndex = -1;
    
    int mTextStyle = 0;
    
    int mFontWeight = -1;
    
    boolean mAllCaps = false;
    
    int mShadowColor = 0;
    
    float mShadowDx = 0.0F;
    
    float mShadowDy = 0.0F;
    
    float mShadowRadius = 0.0F;
    
    boolean mHasElegant = false;
    
    boolean mElegant = false;
    
    boolean mHasFallbackLineSpacing = false;
    
    boolean mFallbackLineSpacing = false;
    
    boolean mHasLetterSpacing = false;
    
    float mLetterSpacing = 0.0F;
    
    String mFontFeatureSettings = null;
    
    String mFontVariationSettings = null;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("TextAppearanceAttributes {\n    mTextColorHighlight:");
      stringBuilder.append(this.mTextColorHighlight);
      stringBuilder.append("\n    mTextColor:");
      stringBuilder.append(this.mTextColor);
      stringBuilder.append("\n    mTextColorHint:");
      stringBuilder.append(this.mTextColorHint);
      stringBuilder.append("\n    mTextColorLink:");
      stringBuilder.append(this.mTextColorLink);
      stringBuilder.append("\n    mTextSize:");
      stringBuilder.append(this.mTextSize);
      stringBuilder.append("\n    mTextSizeUnit:");
      stringBuilder.append(this.mTextSizeUnit);
      stringBuilder.append("\n    mTextLocales:");
      stringBuilder.append(this.mTextLocales);
      stringBuilder.append("\n    mFontFamily:");
      stringBuilder.append(this.mFontFamily);
      stringBuilder.append("\n    mFontTypeface:");
      stringBuilder.append(this.mFontTypeface);
      stringBuilder.append("\n    mFontFamilyExplicit:");
      stringBuilder.append(this.mFontFamilyExplicit);
      stringBuilder.append("\n    mTypefaceIndex:");
      stringBuilder.append(this.mTypefaceIndex);
      stringBuilder.append("\n    mTextStyle:");
      stringBuilder.append(this.mTextStyle);
      stringBuilder.append("\n    mFontWeight:");
      stringBuilder.append(this.mFontWeight);
      stringBuilder.append("\n    mAllCaps:");
      stringBuilder.append(this.mAllCaps);
      stringBuilder.append("\n    mShadowColor:");
      stringBuilder.append(this.mShadowColor);
      stringBuilder.append("\n    mShadowDx:");
      stringBuilder.append(this.mShadowDx);
      stringBuilder.append("\n    mShadowDy:");
      stringBuilder.append(this.mShadowDy);
      stringBuilder.append("\n    mShadowRadius:");
      stringBuilder.append(this.mShadowRadius);
      stringBuilder.append("\n    mHasElegant:");
      stringBuilder.append(this.mHasElegant);
      stringBuilder.append("\n    mElegant:");
      stringBuilder.append(this.mElegant);
      stringBuilder.append("\n    mHasFallbackLineSpacing:");
      stringBuilder.append(this.mHasFallbackLineSpacing);
      stringBuilder.append("\n    mFallbackLineSpacing:");
      stringBuilder.append(this.mFallbackLineSpacing);
      stringBuilder.append("\n    mHasLetterSpacing:");
      stringBuilder.append(this.mHasLetterSpacing);
      stringBuilder.append("\n    mLetterSpacing:");
      stringBuilder.append(this.mLetterSpacing);
      stringBuilder.append("\n    mFontFeatureSettings:");
      stringBuilder.append(this.mFontFeatureSettings);
      stringBuilder.append("\n    mFontVariationSettings:");
      stringBuilder.append(this.mFontVariationSettings);
      stringBuilder.append("\n}");
      return stringBuilder.toString();
    }
  }
  
  private void readTextAppearance(Context paramContext, TypedArray paramTypedArray, TextAppearanceAttributes paramTextAppearanceAttributes, boolean paramBoolean) {
    int i = paramTypedArray.getIndexCount();
    for (byte b = 0; b < i; b++) {
      String str;
      float f;
      int j = paramTypedArray.getIndex(b);
      int k = j;
      if (paramBoolean) {
        int m = sAppearanceValues.get(j, -1);
        k = m;
        if (m == -1)
          continue; 
      } 
      switch (k) {
        case 19:
          str = paramTypedArray.getString(j);
          if (str != null) {
            LocaleList localeList = LocaleList.forLanguageTags(str);
            if (!localeList.isEmpty())
              paramTextAppearanceAttributes.mTextLocales = localeList; 
          } 
          break;
        case 18:
          paramTextAppearanceAttributes.mFontWeight = paramTypedArray.getInt(j, paramTextAppearanceAttributes.mFontWeight);
          break;
        case 17:
          paramTextAppearanceAttributes.mHasFallbackLineSpacing = true;
          paramTextAppearanceAttributes.mFallbackLineSpacing = paramTypedArray.getBoolean(j, paramTextAppearanceAttributes.mFallbackLineSpacing);
          break;
        case 16:
          paramTextAppearanceAttributes.mFontVariationSettings = paramTypedArray.getString(j);
          break;
        case 15:
          paramTextAppearanceAttributes.mFontFeatureSettings = paramTypedArray.getString(j);
          break;
        case 14:
          paramTextAppearanceAttributes.mHasLetterSpacing = true;
          f = paramTextAppearanceAttributes.mLetterSpacing;
          paramTextAppearanceAttributes.mLetterSpacing = paramTypedArray.getFloat(j, f);
          break;
        case 13:
          paramTextAppearanceAttributes.mHasElegant = true;
          paramTextAppearanceAttributes.mElegant = paramTypedArray.getBoolean(j, paramTextAppearanceAttributes.mElegant);
          break;
        case 12:
          if (!paramContext.isRestricted() && paramContext.canLoadUnsafeResources())
            try {
              paramTextAppearanceAttributes.mFontTypeface = paramTypedArray.getFont(j);
            } catch (UnsupportedOperationException|android.content.res.Resources.NotFoundException unsupportedOperationException) {} 
          if (paramTextAppearanceAttributes.mFontTypeface == null)
            paramTextAppearanceAttributes.mFontFamily = paramTypedArray.getString(j); 
          paramTextAppearanceAttributes.mFontFamilyExplicit = true;
          break;
        case 11:
          paramTextAppearanceAttributes.mAllCaps = paramTypedArray.getBoolean(j, paramTextAppearanceAttributes.mAllCaps);
          break;
        case 10:
          paramTextAppearanceAttributes.mShadowRadius = paramTypedArray.getFloat(j, paramTextAppearanceAttributes.mShadowRadius);
          break;
        case 9:
          paramTextAppearanceAttributes.mShadowDy = paramTypedArray.getFloat(j, paramTextAppearanceAttributes.mShadowDy);
          break;
        case 8:
          paramTextAppearanceAttributes.mShadowDx = paramTypedArray.getFloat(j, paramTextAppearanceAttributes.mShadowDx);
          break;
        case 7:
          paramTextAppearanceAttributes.mShadowColor = paramTypedArray.getInt(j, paramTextAppearanceAttributes.mShadowColor);
          break;
        case 6:
          paramTextAppearanceAttributes.mTextColorLink = paramTypedArray.getColorStateList(j);
          break;
        case 5:
          paramTextAppearanceAttributes.mTextColorHint = paramTypedArray.getColorStateList(j);
          break;
        case 4:
          k = paramTextAppearanceAttributes.mTextColorHighlight;
          paramTextAppearanceAttributes.mTextColorHighlight = paramTypedArray.getColor(j, k);
          break;
        case 3:
          paramTextAppearanceAttributes.mTextColor = paramTypedArray.getColorStateList(j);
          break;
        case 2:
          paramTextAppearanceAttributes.mTextStyle = paramTypedArray.getInt(j, paramTextAppearanceAttributes.mTextStyle);
          break;
        case 1:
          paramTextAppearanceAttributes.mTypefaceIndex = paramTypedArray.getInt(j, paramTextAppearanceAttributes.mTypefaceIndex);
          if (paramTextAppearanceAttributes.mTypefaceIndex != -1 && !paramTextAppearanceAttributes.mFontFamilyExplicit)
            paramTextAppearanceAttributes.mFontFamily = null; 
          break;
        case 0:
          k = paramTextAppearanceAttributes.mTextSize;
          paramTextAppearanceAttributes.mTextSize = paramTypedArray.getDimensionPixelSize(j, k);
          paramTextAppearanceAttributes.mTextSizeUnit = paramTypedArray.peekValue(j).getComplexUnit();
          break;
      } 
      continue;
    } 
  }
  
  private void applyTextAppearance(TextAppearanceAttributes paramTextAppearanceAttributes) {
    if (paramTextAppearanceAttributes.mTextColor != null)
      setTextColor(paramTextAppearanceAttributes.mTextColor); 
    if (paramTextAppearanceAttributes.mTextColorHint != null)
      setHintTextColor(paramTextAppearanceAttributes.mTextColorHint); 
    if (paramTextAppearanceAttributes.mTextColorLink != null)
      setLinkTextColor(paramTextAppearanceAttributes.mTextColorLink); 
    if (paramTextAppearanceAttributes.mTextColorHighlight != 0)
      setHighlightColor(paramTextAppearanceAttributes.mTextColorHighlight); 
    if (paramTextAppearanceAttributes.mTextSize != -1) {
      this.mTextSizeUnit = paramTextAppearanceAttributes.mTextSizeUnit;
      setRawTextSize(paramTextAppearanceAttributes.mTextSize, true);
    } 
    if (paramTextAppearanceAttributes.mTextLocales != null)
      setTextLocales(paramTextAppearanceAttributes.mTextLocales); 
    if (paramTextAppearanceAttributes.mTypefaceIndex != -1 && !paramTextAppearanceAttributes.mFontFamilyExplicit)
      paramTextAppearanceAttributes.mFontFamily = null; 
    setTypefaceFromAttrs(paramTextAppearanceAttributes.mFontTypeface, paramTextAppearanceAttributes.mFontFamily, paramTextAppearanceAttributes.mTypefaceIndex, paramTextAppearanceAttributes.mTextStyle, paramTextAppearanceAttributes.mFontWeight);
    if (paramTextAppearanceAttributes.mShadowColor != 0)
      setShadowLayer(paramTextAppearanceAttributes.mShadowRadius, paramTextAppearanceAttributes.mShadowDx, paramTextAppearanceAttributes.mShadowDy, paramTextAppearanceAttributes.mShadowColor); 
    if (paramTextAppearanceAttributes.mAllCaps)
      setTransformationMethod(new AllCapsTransformationMethod(getContext())); 
    if (paramTextAppearanceAttributes.mHasElegant)
      setElegantTextHeight(paramTextAppearanceAttributes.mElegant); 
    if (paramTextAppearanceAttributes.mHasFallbackLineSpacing)
      setFallbackLineSpacing(paramTextAppearanceAttributes.mFallbackLineSpacing); 
    if (paramTextAppearanceAttributes.mHasLetterSpacing)
      setLetterSpacing(paramTextAppearanceAttributes.mLetterSpacing); 
    if (paramTextAppearanceAttributes.mFontFeatureSettings != null)
      setFontFeatureSettings(paramTextAppearanceAttributes.mFontFeatureSettings); 
    if (paramTextAppearanceAttributes.mFontVariationSettings != null)
      setFontVariationSettings(paramTextAppearanceAttributes.mFontVariationSettings); 
  }
  
  public Locale getTextLocale() {
    return this.mTextPaint.getTextLocale();
  }
  
  public LocaleList getTextLocales() {
    return this.mTextPaint.getTextLocales();
  }
  
  private void changeListenerLocaleTo(Locale paramLocale) {
    if (this.mListenerChanged)
      return; 
    Editor editor = this.mEditor;
    if (editor != null) {
      DigitsKeyListener digitsKeyListener;
      DateTimeKeyListener dateTimeKeyListener;
      KeyListener keyListener = editor.mKeyListener;
      if (keyListener instanceof DigitsKeyListener) {
        digitsKeyListener = DigitsKeyListener.getInstance(paramLocale, (DigitsKeyListener)keyListener);
      } else {
        DateKeyListener dateKeyListener;
        if (keyListener instanceof DateKeyListener) {
          dateKeyListener = DateKeyListener.getInstance((Locale)digitsKeyListener);
        } else {
          TimeKeyListener timeKeyListener;
          if (keyListener instanceof TimeKeyListener) {
            timeKeyListener = TimeKeyListener.getInstance((Locale)dateKeyListener);
          } else if (keyListener instanceof DateTimeKeyListener) {
            dateTimeKeyListener = DateTimeKeyListener.getInstance((Locale)timeKeyListener);
          } else {
            return;
          } 
        } 
      } 
      boolean bool = isPasswordInputType(this.mEditor.mInputType);
      setKeyListenerOnly(dateTimeKeyListener);
      setInputTypeFromEditor();
      if (bool) {
        int i = this.mEditor.mInputType & 0xF;
        if (i == 1) {
          Editor editor1 = this.mEditor;
          editor1.mInputType |= 0x80;
        } else if (i == 2) {
          Editor editor1 = this.mEditor;
          editor1.mInputType |= 0x10;
        } 
      } 
    } 
  }
  
  public void setTextLocale(Locale paramLocale) {
    this.mLocalesChanged = true;
    this.mTextPaint.setTextLocale(paramLocale);
    if (this.mLayout != null) {
      nullLayouts();
      requestLayout();
      invalidate();
    } 
  }
  
  public void setTextLocales(LocaleList paramLocaleList) {
    this.mLocalesChanged = true;
    this.mTextPaint.setTextLocales(paramLocaleList);
    if (this.mLayout != null) {
      nullLayouts();
      requestLayout();
      invalidate();
    } 
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    if (!this.mLocalesChanged) {
      this.mTextPaint.setTextLocales(LocaleList.getDefault());
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  @ExportedProperty(category = "text")
  public float getTextSize() {
    return this.mTextPaint.getTextSize();
  }
  
  @ExportedProperty(category = "text")
  public float getScaledTextSize() {
    return this.mTextPaint.getTextSize() / this.mTextPaint.density;
  }
  
  @ExportedProperty(category = "text", mapping = {@IntToString(from = 0, to = "NORMAL"), @IntToString(from = 1, to = "BOLD"), @IntToString(from = 2, to = "ITALIC"), @IntToString(from = 3, to = "BOLD_ITALIC")})
  public int getTypefaceStyle() {
    boolean bool;
    Typeface typeface = this.mTextPaint.getTypeface();
    if (typeface != null) {
      bool = typeface.getStyle();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @RemotableViewMethod
  public void setTextSize(float paramFloat) {
    setTextSize(2, paramFloat);
  }
  
  public void setTextSize(int paramInt, float paramFloat) {
    if (!isAutoSizeEnabled())
      setTextSizeInternal(paramInt, paramFloat, true); 
  }
  
  private void setTextSizeInternal(int paramInt, float paramFloat, boolean paramBoolean) {
    Resources resources;
    Context context = getContext();
    if (context == null) {
      resources = Resources.getSystem();
    } else {
      resources = resources.getResources();
    } 
    this.mTextSizeUnit = paramInt;
    setRawTextSize(TypedValue.applyDimension(paramInt, paramFloat, resources.getDisplayMetrics()), paramBoolean);
  }
  
  private void setRawTextSize(float paramFloat, boolean paramBoolean) {
    if (paramFloat != this.mTextPaint.getTextSize()) {
      this.mTextPaint.setTextSize(paramFloat);
      if (paramBoolean && this.mLayout != null) {
        this.mNeedsAutoSizeText = false;
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public int getTextSizeUnit() {
    return this.mTextSizeUnit;
  }
  
  public float getTextScaleX() {
    return this.mTextPaint.getTextScaleX();
  }
  
  @RemotableViewMethod
  public void setTextScaleX(float paramFloat) {
    if (paramFloat != this.mTextPaint.getTextScaleX()) {
      this.mUserSetTextScaleX = true;
      this.mTextPaint.setTextScaleX(paramFloat);
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public void setTypeface(Typeface paramTypeface) {
    paramTypeface = ((IOplusFontManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFontManager.DEFAULT, new Object[0])).flipTypeface(paramTypeface);
    if (this.mTextPaint.getTypeface() != paramTypeface) {
      this.mTextPaint.setTypeface(paramTypeface);
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public Typeface getTypeface() {
    return this.mTextPaint.getTypeface();
  }
  
  public void setElegantTextHeight(boolean paramBoolean) {
    if (paramBoolean != this.mTextPaint.isElegantTextHeight()) {
      this.mTextPaint.setElegantTextHeight(paramBoolean);
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public void setFallbackLineSpacing(boolean paramBoolean) {
    if (this.mUseFallbackLineSpacing != paramBoolean) {
      this.mUseFallbackLineSpacing = paramBoolean;
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public boolean isFallbackLineSpacing() {
    return this.mUseFallbackLineSpacing;
  }
  
  public boolean isElegantTextHeight() {
    return this.mTextPaint.isElegantTextHeight();
  }
  
  public float getLetterSpacing() {
    return this.mTextPaint.getLetterSpacing();
  }
  
  @RemotableViewMethod
  public void setLetterSpacing(float paramFloat) {
    if (paramFloat != this.mTextPaint.getLetterSpacing()) {
      this.mTextPaint.setLetterSpacing(paramFloat);
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public String getFontFeatureSettings() {
    return this.mTextPaint.getFontFeatureSettings();
  }
  
  public String getFontVariationSettings() {
    return this.mTextPaint.getFontVariationSettings();
  }
  
  public void setBreakStrategy(int paramInt) {
    this.mBreakStrategy = paramInt;
    if (this.mLayout != null) {
      nullLayouts();
      requestLayout();
      invalidate();
    } 
  }
  
  public int getBreakStrategy() {
    return this.mBreakStrategy;
  }
  
  public void setHyphenationFrequency(int paramInt) {
    this.mHyphenationFrequency = paramInt;
    if (this.mLayout != null) {
      nullLayouts();
      requestLayout();
      invalidate();
    } 
  }
  
  public int getHyphenationFrequency() {
    return this.mHyphenationFrequency;
  }
  
  public PrecomputedText.Params getTextMetricsParams() {
    return new PrecomputedText.Params(new TextPaint(this.mTextPaint), getTextDirectionHeuristic(), this.mBreakStrategy, this.mHyphenationFrequency);
  }
  
  public void setTextMetricsParams(PrecomputedText.Params paramParams) {
    this.mTextPaint.set(paramParams.getTextPaint());
    this.mUserSetTextScaleX = true;
    this.mTextDir = paramParams.getTextDirection();
    this.mBreakStrategy = paramParams.getBreakStrategy();
    this.mHyphenationFrequency = paramParams.getHyphenationFrequency();
    if (this.mLayout != null) {
      nullLayouts();
      requestLayout();
      invalidate();
    } 
  }
  
  public void setJustificationMode(int paramInt) {
    this.mJustificationMode = paramInt;
    if (this.mLayout != null) {
      nullLayouts();
      requestLayout();
      invalidate();
    } 
  }
  
  public int getJustificationMode() {
    return this.mJustificationMode;
  }
  
  @RemotableViewMethod
  public void setFontFeatureSettings(String paramString) {
    if (paramString != this.mTextPaint.getFontFeatureSettings()) {
      this.mTextPaint.setFontFeatureSettings(paramString);
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  @RemotableViewMethod
  public boolean setFontVariationSettings(String paramString) {
    String str = this.mTextPaint.getFontVariationSettings();
    if (paramString != str) {
      if (paramString != null)
        if (paramString.equals(str))
          return true;  
      boolean bool = this.mTextPaint.setFontVariationSettings(paramString);
      if (bool && this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
      return bool;
    } 
    return true;
  }
  
  @RemotableViewMethod
  public void setTextColor(int paramInt) {
    this.mTextColor = ColorStateList.valueOf(paramInt);
    updateTextColors();
  }
  
  @RemotableViewMethod
  public void setTextColor(ColorStateList paramColorStateList) {
    if (paramColorStateList != null) {
      this.mTextColor = paramColorStateList;
      updateTextColors();
      return;
    } 
    throw null;
  }
  
  public final ColorStateList getTextColors() {
    return this.mTextColor;
  }
  
  public final int getCurrentTextColor() {
    return this.mCurTextColor;
  }
  
  @RemotableViewMethod
  public void setHighlightColor(int paramInt) {
    if (this.mHighlightColor != paramInt) {
      this.mHighlightColor = paramInt;
      invalidate();
    } 
  }
  
  public int getHighlightColor() {
    return this.mHighlightColor;
  }
  
  @RemotableViewMethod
  public final void setShowSoftInputOnFocus(boolean paramBoolean) {
    createEditorIfNeeded();
    this.mEditor.mShowSoftInputOnFocus = paramBoolean;
  }
  
  public final boolean getShowSoftInputOnFocus() {
    Editor editor = this.mEditor;
    return (editor == null || editor.mShowSoftInputOnFocus);
  }
  
  public void setShadowLayer(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt) {
    this.mTextPaint.setShadowLayer(paramFloat1, paramFloat2, paramFloat3, paramInt);
    this.mShadowRadius = paramFloat1;
    this.mShadowDx = paramFloat2;
    this.mShadowDy = paramFloat3;
    this.mShadowColor = paramInt;
    Editor editor = this.mEditor;
    if (editor != null) {
      editor.invalidateTextDisplayList();
      this.mEditor.invalidateHandlesAndActionMode();
    } 
    invalidate();
  }
  
  public float getShadowRadius() {
    return this.mShadowRadius;
  }
  
  public float getShadowDx() {
    return this.mShadowDx;
  }
  
  public float getShadowDy() {
    return this.mShadowDy;
  }
  
  public int getShadowColor() {
    return this.mShadowColor;
  }
  
  public TextPaint getPaint() {
    return this.mTextPaint;
  }
  
  @RemotableViewMethod
  public final void setAutoLinkMask(int paramInt) {
    this.mAutoLinkMask = paramInt;
  }
  
  @RemotableViewMethod
  public final void setLinksClickable(boolean paramBoolean) {
    this.mLinksClickable = paramBoolean;
  }
  
  public final boolean getLinksClickable() {
    return this.mLinksClickable;
  }
  
  public URLSpan[] getUrls() {
    CharSequence charSequence = this.mText;
    if (charSequence instanceof Spanned)
      return ((Spanned)charSequence).<URLSpan>getSpans(0, charSequence.length(), URLSpan.class); 
    return new URLSpan[0];
  }
  
  @RemotableViewMethod
  public final void setHintTextColor(int paramInt) {
    this.mHintTextColor = ColorStateList.valueOf(paramInt);
    updateTextColors();
  }
  
  public final void setHintTextColor(ColorStateList paramColorStateList) {
    this.mHintTextColor = paramColorStateList;
    updateTextColors();
  }
  
  public final ColorStateList getHintTextColors() {
    return this.mHintTextColor;
  }
  
  public final int getCurrentHintTextColor() {
    int i;
    if (this.mHintTextColor != null) {
      i = this.mCurHintTextColor;
    } else {
      i = this.mCurTextColor;
    } 
    return i;
  }
  
  @RemotableViewMethod
  public final void setLinkTextColor(int paramInt) {
    this.mLinkTextColor = ColorStateList.valueOf(paramInt);
    updateTextColors();
  }
  
  public final void setLinkTextColor(ColorStateList paramColorStateList) {
    this.mLinkTextColor = paramColorStateList;
    updateTextColors();
  }
  
  public final ColorStateList getLinkTextColors() {
    return this.mLinkTextColor;
  }
  
  public void setGravity(int paramInt) {
    int i = paramInt;
    if ((paramInt & 0x800007) == 0)
      i = paramInt | 0x800003; 
    paramInt = i;
    if ((i & 0x70) == 0)
      paramInt = i | 0x30; 
    i = 0;
    if ((paramInt & 0x800007) != (0x800007 & this.mGravity))
      i = 1; 
    if (paramInt != this.mGravity)
      invalidate(); 
    this.mGravity = paramInt;
    Layout layout = this.mLayout;
    if (layout != null && i != 0) {
      i = layout.getWidth();
      layout = this.mHintLayout;
      if (layout == null) {
        paramInt = 0;
      } else {
        paramInt = layout.getWidth();
      } 
      BoringLayout.Metrics metrics = UNKNOWN_BORING;
      int j = this.mRight, k = this.mLeft;
      int m = getCompoundPaddingLeft(), n = getCompoundPaddingRight();
      makeNewLayout(i, paramInt, metrics, metrics, j - k - m - n, true);
    } 
  }
  
  public int getGravity() {
    return this.mGravity;
  }
  
  public int getPaintFlags() {
    return this.mTextPaint.getFlags();
  }
  
  @RemotableViewMethod
  public void setPaintFlags(int paramInt) {
    if (this.mTextPaint.getFlags() != paramInt) {
      this.mTextPaint.setFlags(paramInt);
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public void setHorizontallyScrolling(boolean paramBoolean) {
    if (this.mHorizontallyScrolling != paramBoolean) {
      this.mHorizontallyScrolling = paramBoolean;
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public final boolean isHorizontallyScrollable() {
    return this.mHorizontallyScrolling;
  }
  
  public boolean getHorizontallyScrolling() {
    return this.mHorizontallyScrolling;
  }
  
  @RemotableViewMethod
  public void setMinLines(int paramInt) {
    this.mMinimum = paramInt;
    this.mMinMode = 1;
    requestLayout();
    invalidate();
  }
  
  public int getMinLines() {
    byte b;
    if (this.mMinMode == 1) {
      b = this.mMinimum;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @RemotableViewMethod
  public void setMinHeight(int paramInt) {
    this.mMinimum = paramInt;
    this.mMinMode = 2;
    requestLayout();
    invalidate();
  }
  
  public int getMinHeight() {
    byte b;
    if (this.mMinMode == 2) {
      b = this.mMinimum;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @RemotableViewMethod
  public void setMaxLines(int paramInt) {
    this.mMaximum = paramInt;
    this.mMaxMode = 1;
    requestLayout();
    invalidate();
  }
  
  public int getMaxLines() {
    byte b;
    if (this.mMaxMode == 1) {
      b = this.mMaximum;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @RemotableViewMethod
  public void setMaxHeight(int paramInt) {
    this.mMaximum = paramInt;
    this.mMaxMode = 2;
    requestLayout();
    invalidate();
  }
  
  public int getMaxHeight() {
    byte b;
    if (this.mMaxMode == 2) {
      b = this.mMaximum;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @RemotableViewMethod
  public void setLines(int paramInt) {
    this.mMinimum = paramInt;
    this.mMaximum = paramInt;
    this.mMinMode = 1;
    this.mMaxMode = 1;
    requestLayout();
    invalidate();
  }
  
  @RemotableViewMethod
  public void setHeight(int paramInt) {
    this.mMinimum = paramInt;
    this.mMaximum = paramInt;
    this.mMinMode = 2;
    this.mMaxMode = 2;
    requestLayout();
    invalidate();
  }
  
  @RemotableViewMethod
  public void setMinEms(int paramInt) {
    this.mMinWidth = paramInt;
    this.mMinWidthMode = 1;
    requestLayout();
    invalidate();
  }
  
  public int getMinEms() {
    byte b;
    if (this.mMinWidthMode == 1) {
      b = this.mMinWidth;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @RemotableViewMethod
  public void setMinWidth(int paramInt) {
    this.mMinWidth = paramInt;
    this.mMinWidthMode = 2;
    requestLayout();
    invalidate();
  }
  
  public int getMinWidth() {
    byte b;
    if (this.mMinWidthMode == 2) {
      b = this.mMinWidth;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @RemotableViewMethod
  public void setMaxEms(int paramInt) {
    this.mMaxWidth = paramInt;
    this.mMaxWidthMode = 1;
    requestLayout();
    invalidate();
  }
  
  public int getMaxEms() {
    byte b;
    if (this.mMaxWidthMode == 1) {
      b = this.mMaxWidth;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @RemotableViewMethod
  public void setMaxWidth(int paramInt) {
    this.mMaxWidth = paramInt;
    this.mMaxWidthMode = 2;
    requestLayout();
    invalidate();
  }
  
  public int getMaxWidth() {
    byte b;
    if (this.mMaxWidthMode == 2) {
      b = this.mMaxWidth;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @RemotableViewMethod
  public void setEms(int paramInt) {
    this.mMinWidth = paramInt;
    this.mMaxWidth = paramInt;
    this.mMinWidthMode = 1;
    this.mMaxWidthMode = 1;
    requestLayout();
    invalidate();
  }
  
  @RemotableViewMethod
  public void setWidth(int paramInt) {
    this.mMinWidth = paramInt;
    this.mMaxWidth = paramInt;
    this.mMinWidthMode = 2;
    this.mMaxWidthMode = 2;
    requestLayout();
    invalidate();
  }
  
  public void setLineSpacing(float paramFloat1, float paramFloat2) {
    if (this.mSpacingAdd != paramFloat1 || this.mSpacingMult != paramFloat2) {
      this.mSpacingAdd = paramFloat1;
      this.mSpacingMult = paramFloat2;
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public float getLineSpacingMultiplier() {
    return this.mSpacingMult;
  }
  
  public float getLineSpacingExtra() {
    return this.mSpacingAdd;
  }
  
  public void setLineHeight(int paramInt) {
    Preconditions.checkArgumentNonnegative(paramInt);
    int i = getPaint().getFontMetricsInt(null);
    if (paramInt != i)
      setLineSpacing((paramInt - i), 1.0F); 
  }
  
  public final void append(CharSequence paramCharSequence) {
    append(paramCharSequence, 0, paramCharSequence.length());
  }
  
  public void append(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    CharSequence charSequence = this.mText;
    if (!(charSequence instanceof Editable))
      setText(charSequence, BufferType.EDITABLE); 
    ((Editable)this.mText).append(paramCharSequence, paramInt1, paramInt2);
    paramInt1 = this.mAutoLinkMask;
    if (paramInt1 != 0) {
      boolean bool = Linkify.addLinks(this.mSpannable, paramInt1);
      if (bool && this.mLinksClickable && !textCanBeSelected())
        setMovementMethod(LinkMovementMethod.getInstance()); 
    } 
  }
  
  private void updateTextColors() {
    int i = 0;
    int[] arrayOfInt = getDrawableState();
    int j = this.mTextColor.getColorForState(arrayOfInt, 0);
    if (j != this.mCurTextColor) {
      this.mCurTextColor = j;
      i = 1;
    } 
    ColorStateList colorStateList = this.mLinkTextColor;
    j = i;
    if (colorStateList != null) {
      int k = colorStateList.getColorForState(arrayOfInt, 0);
      j = i;
      if (k != this.mTextPaint.linkColor) {
        this.mTextPaint.linkColor = k;
        j = 1;
      } 
    } 
    colorStateList = this.mHintTextColor;
    i = j;
    if (colorStateList != null) {
      int k = colorStateList.getColorForState(arrayOfInt, 0);
      i = j;
      if (k != this.mCurHintTextColor) {
        this.mCurHintTextColor = k;
        i = j;
        if (this.mText.length() == 0)
          i = 1; 
      } 
    } 
    if (i != 0) {
      Editor editor = this.mEditor;
      if (editor != null)
        editor.invalidateTextDisplayList(); 
      invalidate();
    } 
  }
  
  protected void drawableStateChanged() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial drawableStateChanged : ()V
    //   4: aload_0
    //   5: getfield mTextColor : Landroid/content/res/ColorStateList;
    //   8: astore_1
    //   9: aload_1
    //   10: ifnull -> 20
    //   13: aload_1
    //   14: invokevirtual isStateful : ()Z
    //   17: ifne -> 52
    //   20: aload_0
    //   21: getfield mHintTextColor : Landroid/content/res/ColorStateList;
    //   24: astore_1
    //   25: aload_1
    //   26: ifnull -> 36
    //   29: aload_1
    //   30: invokevirtual isStateful : ()Z
    //   33: ifne -> 52
    //   36: aload_0
    //   37: getfield mLinkTextColor : Landroid/content/res/ColorStateList;
    //   40: astore_1
    //   41: aload_1
    //   42: ifnull -> 56
    //   45: aload_1
    //   46: invokevirtual isStateful : ()Z
    //   49: ifeq -> 56
    //   52: aload_0
    //   53: invokespecial updateTextColors : ()V
    //   56: aload_0
    //   57: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   60: ifnull -> 128
    //   63: aload_0
    //   64: invokevirtual getDrawableState : ()[I
    //   67: astore_2
    //   68: aload_0
    //   69: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   72: getfield mShowing : [Landroid/graphics/drawable/Drawable;
    //   75: astore_1
    //   76: aload_1
    //   77: arraylength
    //   78: istore_3
    //   79: iconst_0
    //   80: istore #4
    //   82: iload #4
    //   84: iload_3
    //   85: if_icmpge -> 128
    //   88: aload_1
    //   89: iload #4
    //   91: aaload
    //   92: astore #5
    //   94: aload #5
    //   96: ifnull -> 122
    //   99: aload #5
    //   101: invokevirtual isStateful : ()Z
    //   104: ifeq -> 122
    //   107: aload #5
    //   109: aload_2
    //   110: invokevirtual setState : ([I)Z
    //   113: ifeq -> 122
    //   116: aload_0
    //   117: aload #5
    //   119: invokevirtual invalidateDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   122: iinc #4, 1
    //   125: goto -> 82
    //   128: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5912	-> 0
    //   #5914	-> 4
    //   #5915	-> 29
    //   #5916	-> 45
    //   #5917	-> 52
    //   #5920	-> 56
    //   #5921	-> 63
    //   #5922	-> 68
    //   #5923	-> 94
    //   #5924	-> 116
    //   #5922	-> 122
    //   #5928	-> 128
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    super.drawableHotspotChanged(paramFloat1, paramFloat2);
    Drawables drawables = this.mDrawables;
    if (drawables != null)
      for (Drawable drawable : drawables.mShowing) {
        if (drawable != null)
          drawable.setHotspot(paramFloat1, paramFloat2); 
      }  
  }
  
  public Parcelable onSaveInstanceState() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial onSaveInstanceState : ()Landroid/os/Parcelable;
    //   4: astore_1
    //   5: aload_0
    //   6: invokevirtual getFreezesText : ()Z
    //   9: istore_2
    //   10: iconst_0
    //   11: istore_3
    //   12: iconst_m1
    //   13: istore #4
    //   15: iconst_m1
    //   16: istore #5
    //   18: iload_3
    //   19: istore #6
    //   21: aload_0
    //   22: getfield mText : Ljava/lang/CharSequence;
    //   25: ifnull -> 72
    //   28: aload_0
    //   29: invokevirtual getSelectionStart : ()I
    //   32: istore #7
    //   34: aload_0
    //   35: invokevirtual getSelectionEnd : ()I
    //   38: istore #8
    //   40: iload #7
    //   42: ifge -> 61
    //   45: iload_3
    //   46: istore #6
    //   48: iload #7
    //   50: istore #4
    //   52: iload #8
    //   54: istore #5
    //   56: iload #8
    //   58: iflt -> 72
    //   61: iconst_1
    //   62: istore #6
    //   64: iload #8
    //   66: istore #5
    //   68: iload #7
    //   70: istore #4
    //   72: iload_2
    //   73: ifne -> 86
    //   76: iload #6
    //   78: ifeq -> 84
    //   81: goto -> 86
    //   84: aload_1
    //   85: areturn
    //   86: new android/widget/TextView$SavedState
    //   89: dup
    //   90: aload_1
    //   91: invokespecial <init> : (Landroid/os/Parcelable;)V
    //   94: astore_1
    //   95: iload_2
    //   96: ifeq -> 173
    //   99: aload_0
    //   100: getfield mText : Ljava/lang/CharSequence;
    //   103: astore #9
    //   105: aload #9
    //   107: instanceof android/text/Spanned
    //   110: ifeq -> 162
    //   113: new android/text/SpannableStringBuilder
    //   116: dup
    //   117: aload_0
    //   118: getfield mText : Ljava/lang/CharSequence;
    //   121: invokespecial <init> : (Ljava/lang/CharSequence;)V
    //   124: astore #9
    //   126: aload_0
    //   127: getfield mEditor : Landroid/widget/Editor;
    //   130: ifnull -> 153
    //   133: aload_0
    //   134: aload #9
    //   136: invokevirtual removeMisspelledSpans : (Landroid/text/Spannable;)V
    //   139: aload #9
    //   141: aload_0
    //   142: getfield mEditor : Landroid/widget/Editor;
    //   145: getfield mSuggestionRangeSpan : Landroid/text/style/SuggestionRangeSpan;
    //   148: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   153: aload_1
    //   154: aload #9
    //   156: putfield text : Ljava/lang/CharSequence;
    //   159: goto -> 173
    //   162: aload_1
    //   163: aload #9
    //   165: invokeinterface toString : ()Ljava/lang/String;
    //   170: putfield text : Ljava/lang/CharSequence;
    //   173: iload #6
    //   175: ifeq -> 190
    //   178: aload_1
    //   179: iload #4
    //   181: putfield selStart : I
    //   184: aload_1
    //   185: iload #5
    //   187: putfield selEnd : I
    //   190: aload_0
    //   191: invokevirtual isFocused : ()Z
    //   194: ifeq -> 212
    //   197: iload #4
    //   199: iflt -> 212
    //   202: iload #5
    //   204: iflt -> 212
    //   207: aload_1
    //   208: iconst_1
    //   209: putfield frozenWithFocus : Z
    //   212: aload_1
    //   213: aload_0
    //   214: invokevirtual getError : ()Ljava/lang/CharSequence;
    //   217: putfield error : Ljava/lang/CharSequence;
    //   220: aload_0
    //   221: getfield mEditor : Landroid/widget/Editor;
    //   224: astore #9
    //   226: aload #9
    //   228: ifnull -> 240
    //   231: aload_1
    //   232: aload #9
    //   234: invokevirtual saveInstanceState : ()Landroid/os/ParcelableParcel;
    //   237: putfield editorState : Landroid/os/ParcelableParcel;
    //   240: aload_1
    //   241: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5945	-> 0
    //   #5948	-> 5
    //   #5949	-> 10
    //   #5950	-> 12
    //   #5951	-> 15
    //   #5953	-> 18
    //   #5954	-> 28
    //   #5955	-> 34
    //   #5956	-> 40
    //   #5958	-> 61
    //   #5962	-> 72
    //   #5998	-> 84
    //   #5963	-> 86
    //   #5965	-> 95
    //   #5966	-> 99
    //   #5967	-> 113
    //   #5969	-> 126
    //   #5970	-> 133
    //   #5971	-> 139
    //   #5974	-> 153
    //   #5975	-> 159
    //   #5976	-> 162
    //   #5980	-> 173
    //   #5982	-> 178
    //   #5983	-> 184
    //   #5986	-> 190
    //   #5987	-> 207
    //   #5990	-> 212
    //   #5992	-> 220
    //   #5993	-> 231
    //   #5995	-> 240
  }
  
  void removeMisspelledSpans(Spannable paramSpannable) {
    SuggestionSpan[] arrayOfSuggestionSpan = paramSpannable.<SuggestionSpan>getSpans(0, paramSpannable.length(), SuggestionSpan.class);
    for (byte b = 0; b < arrayOfSuggestionSpan.length; b++) {
      int i = arrayOfSuggestionSpan[b].getFlags();
      if ((i & 0x1) != 0 && (i & 0x2) != 0)
        paramSpannable.removeSpan(arrayOfSuggestionSpan[b]); 
    } 
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (!(paramParcelable instanceof SavedState)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    if (savedState.text != null)
      setText(savedState.text); 
    if (savedState.selStart >= 0 && savedState.selEnd >= 0 && this.mSpannable != null) {
      int i = this.mText.length();
      if (savedState.selStart > i || savedState.selEnd > i) {
        String str = "";
        if (savedState.text != null)
          str = "(restored) "; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Saved cursor position ");
        stringBuilder.append(savedState.selStart);
        stringBuilder.append("/");
        stringBuilder.append(savedState.selEnd);
        stringBuilder.append(" out of range for ");
        stringBuilder.append(str);
        stringBuilder.append("text ");
        stringBuilder.append(this.mText);
        Log.e("TextView", stringBuilder.toString());
      } else {
        Selection.setSelection(this.mSpannable, savedState.selStart, savedState.selEnd);
        if (savedState.frozenWithFocus) {
          createEditorIfNeeded();
          this.mEditor.mFrozenWithFocus = true;
        } 
      } 
    } 
    if (savedState.error != null) {
      CharSequence charSequence = savedState.error;
      post((Runnable)new Object(this, charSequence));
    } 
    if (savedState.editorState != null) {
      createEditorIfNeeded();
      this.mEditor.restoreInstanceState(savedState.editorState);
    } 
  }
  
  @RemotableViewMethod
  public void setFreezesText(boolean paramBoolean) {
    this.mFreezesText = paramBoolean;
  }
  
  public boolean getFreezesText() {
    return this.mFreezesText;
  }
  
  public final void setEditableFactory(Editable.Factory paramFactory) {
    this.mEditableFactory = paramFactory;
    setText(this.mText);
  }
  
  public final void setSpannableFactory(Spannable.Factory paramFactory) {
    this.mSpannableFactory = paramFactory;
    setText(this.mText);
  }
  
  @RemotableViewMethod
  public final void setText(CharSequence paramCharSequence) {
    setText(paramCharSequence, this.mBufferType);
  }
  
  @RemotableViewMethod
  public final void setTextKeepState(CharSequence paramCharSequence) {
    setTextKeepState(paramCharSequence, this.mBufferType);
  }
  
  public void setText(CharSequence paramCharSequence, BufferType paramBufferType) {
    setText(paramCharSequence, paramBufferType, true, 0);
    paramCharSequence = this.mCharWrapper;
    if (paramCharSequence != null)
      CharWrapper.access$202((CharWrapper)paramCharSequence, null); 
  }
  
  private void setText(CharSequence paramCharSequence, BufferType paramBufferType, boolean paramBoolean, int paramInt) {
    CharSequence charSequence1;
    InputMethodManager inputMethodManager;
    BufferType bufferType1 = paramBufferType;
    this.mTextSetFromXmlOrResourceId = false;
    if (paramCharSequence == null)
      paramCharSequence = ""; 
    CharSequence charSequence2 = paramCharSequence;
    if (!isSuggestionsEnabled())
      charSequence2 = removeSuggestionSpans(paramCharSequence); 
    if (!this.mUserSetTextScaleX)
      this.mTextPaint.setTextScaleX(1.0F); 
    if (charSequence2 instanceof Spanned) {
      paramCharSequence = charSequence2;
      TextUtils.TruncateAt truncateAt = TextUtils.TruncateAt.MARQUEE;
      if (paramCharSequence.getSpanStart(truncateAt) >= 0) {
        if (ViewConfiguration.get(this.mContext).isFadingMarqueeEnabled()) {
          setHorizontalFadingEdgeEnabled(true);
          this.mMarqueeFadeMode = 0;
        } else {
          setHorizontalFadingEdgeEnabled(false);
          this.mMarqueeFadeMode = 1;
        } 
        setEllipsize(TextUtils.TruncateAt.MARQUEE);
      } 
    } 
    int i = this.mFilters.length;
    int j;
    for (j = 0; j < i; j++) {
      paramCharSequence = this.mFilters[j].filter(charSequence2, 0, charSequence2.length(), EMPTY_SPANNED, 0, 0);
      if (paramCharSequence != null)
        charSequence2 = paramCharSequence; 
    } 
    if (paramBoolean) {
      paramCharSequence = this.mText;
      if (paramCharSequence != null) {
        paramInt = paramCharSequence.length();
        sendBeforeTextChanged(this.mText, 0, paramInt, charSequence2.length());
      } else {
        sendBeforeTextChanged("", 0, 0, charSequence2.length());
      } 
    } 
    i = 0;
    ArrayList<TextWatcher> arrayList = this.mListeners;
    j = i;
    if (arrayList != null) {
      j = i;
      if (arrayList.size() != 0)
        j = 1; 
    } 
    if (charSequence2 instanceof PrecomputedText) {
      charSequence1 = charSequence2;
    } else {
      arrayList = null;
    } 
    if (bufferType1 == BufferType.EDITABLE || getKeyListener() != null || j != 0) {
      createEditorIfNeeded();
      this.mEditor.forgetUndoRedo();
      charSequence2 = this.mEditableFactory.newEditable(charSequence2);
      charSequence1 = charSequence2;
      setFilters((Editable)charSequence2, this.mFilters);
      inputMethodManager = getInputMethodManager();
      if (inputMethodManager != null)
        inputMethodManager.restartInput(this); 
    } else {
      StringBuilder stringBuilder;
      if (charSequence1 != null) {
        InputMethodManager inputMethodManager1;
        if (this.mTextDir == null)
          this.mTextDir = getTextDirectionHeuristic(); 
        i = charSequence1.getParams().checkResultUsable(getPaint(), this.mTextDir, this.mBreakStrategy, this.mHyphenationFrequency);
        if (i != 0) {
          if (i == 1)
            PrecomputedText.create(charSequence1, getTextMetricsParams()); 
          inputMethodManager1 = inputMethodManager;
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("PrecomputedText's Parameters don't match the parameters of this TextView.Consider using setTextMetricsParams(precomputedText.getParams()) to override the settings of this TextView: PrecomputedText: ");
          stringBuilder.append(inputMethodManager1.getParams());
          stringBuilder.append("TextView: ");
          stringBuilder.append(getTextMetricsParams());
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else if (bufferType1 == BufferType.SPANNABLE || this.mMovement != null) {
        charSequence1 = this.mSpannableFactory.newSpannable(stringBuilder);
      } else {
        charSequence1 = stringBuilder;
        if (!(stringBuilder instanceof CharWrapper))
          charSequence1 = TextUtils.stringOrSpannedString(stringBuilder); 
      } 
    } 
    BufferType bufferType2 = bufferType1;
    CharSequence charSequence3 = charSequence1;
    if (this.mAutoLinkMask != 0) {
      Spannable spannable;
      if (bufferType1 == BufferType.EDITABLE || charSequence1 instanceof Spannable) {
        spannable = (Spannable)charSequence1;
      } else {
        spannable = this.mSpannableFactory.newSpannable(charSequence1);
      } 
      bufferType2 = bufferType1;
      charSequence3 = charSequence1;
      if (Linkify.addLinks(spannable, this.mAutoLinkMask)) {
        BufferType bufferType;
        if (bufferType1 == BufferType.EDITABLE) {
          bufferType = BufferType.EDITABLE;
        } else {
          bufferType = BufferType.SPANNABLE;
        } 
        setTextInternal(spannable);
        bufferType2 = bufferType;
        charSequence3 = spannable;
        if (this.mLinksClickable) {
          bufferType2 = bufferType;
          charSequence3 = spannable;
          if (!textCanBeSelected()) {
            setMovementMethod(LinkMovementMethod.getInstance());
            charSequence3 = spannable;
            bufferType2 = bufferType;
          } 
        } 
      } 
    } 
    this.mBufferType = bufferType2;
    setTextInternal(charSequence3);
    TransformationMethod transformationMethod = this.mTransformation;
    if (transformationMethod == null) {
      this.mTransformed = charSequence3;
    } else {
      this.mTransformed = transformationMethod.getTransformation(charSequence3, this);
    } 
    if (this.mTransformed == null)
      this.mTransformed = ""; 
    int k = charSequence3.length();
    if (charSequence3 instanceof Spannable && !this.mAllowTransformationLengthChange) {
      Spannable spannable = (Spannable)charSequence3;
      ChangeWatcher[] arrayOfChangeWatcher = spannable.<ChangeWatcher>getSpans(0, spannable.length(), ChangeWatcher.class);
      int m = arrayOfChangeWatcher.length;
      for (i = 0; i < m; i++)
        spannable.removeSpan(arrayOfChangeWatcher[i]); 
      if (this.mChangeWatcher == null)
        this.mChangeWatcher = new ChangeWatcher(); 
      spannable.setSpan(this.mChangeWatcher, 0, k, 6553618);
      Editor editor1 = this.mEditor;
      if (editor1 != null)
        editor1.addSpanWatchers(spannable); 
      TransformationMethod transformationMethod1 = this.mTransformation;
      if (transformationMethod1 != null)
        spannable.setSpan(transformationMethod1, 0, k, 18); 
      MovementMethod movementMethod = this.mMovement;
      if (movementMethod != null) {
        movementMethod.initialize(this, (Spannable)charSequence3);
        Editor editor2 = this.mEditor;
        if (editor2 != null)
          editor2.mSelectionMoved = false; 
      } 
    } 
    if (this.mLayout != null)
      checkForRelayout(); 
    sendOnTextChanged(charSequence3, 0, paramInt, k);
    onTextChanged(charSequence3, 0, paramInt, k);
    notifyViewAccessibilityStateChangedIfNeeded(2);
    if (j != 0) {
      sendAfterTextChanged((Editable)charSequence3);
    } else {
      notifyListeningManagersAfterTextChanged();
    } 
    Editor editor = this.mEditor;
    if (editor != null)
      editor.prepareCursorControllers(); 
  }
  
  public final void setText(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    int i = 0;
    if (paramInt1 >= 0 && paramInt2 >= 0 && paramInt1 + paramInt2 <= paramArrayOfchar.length) {
      CharSequence charSequence = this.mText;
      if (charSequence != null) {
        i = charSequence.length();
        sendBeforeTextChanged(this.mText, 0, i, paramInt2);
      } else {
        sendBeforeTextChanged("", 0, 0, paramInt2);
      } 
      charSequence = this.mCharWrapper;
      if (charSequence == null) {
        this.mCharWrapper = new CharWrapper(paramArrayOfchar, paramInt1, paramInt2);
      } else {
        charSequence.set(paramArrayOfchar, paramInt1, paramInt2);
      } 
      setText(this.mCharWrapper, this.mBufferType, false, i);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt1);
    stringBuilder.append(", ");
    stringBuilder.append(paramInt2);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public final void setTextKeepState(CharSequence paramCharSequence, BufferType paramBufferType) {
    int i = getSelectionStart();
    int j = getSelectionEnd();
    int k = paramCharSequence.length();
    setText(paramCharSequence, paramBufferType);
    if (i >= 0 || j >= 0) {
      paramCharSequence = this.mSpannable;
      if (paramCharSequence != null) {
        i = Math.max(0, Math.min(i, k));
        j = Math.max(0, Math.min(j, k));
        Selection.setSelection((Spannable)paramCharSequence, i, j);
      } 
    } 
  }
  
  @RemotableViewMethod
  public final void setText(int paramInt) {
    setText(getContext().getResources().getText(paramInt));
    this.mTextSetFromXmlOrResourceId = true;
    this.mTextId = paramInt;
  }
  
  public final void setText(int paramInt, BufferType paramBufferType) {
    setText(getContext().getResources().getText(paramInt), paramBufferType);
    this.mTextSetFromXmlOrResourceId = true;
    this.mTextId = paramInt;
  }
  
  @RemotableViewMethod
  public final void setHint(CharSequence paramCharSequence) {
    setHintInternal(paramCharSequence);
    if (this.mEditor != null && isInputMethodTarget())
      this.mEditor.reportExtractedText(); 
  }
  
  private void setHintInternal(CharSequence paramCharSequence) {
    this.mHint = TextUtils.stringOrSpannedString(paramCharSequence);
    if (this.mLayout != null)
      checkForRelayout(); 
    if (this.mText.length() == 0)
      invalidate(); 
    if (this.mEditor != null && this.mText.length() == 0 && this.mHint != null)
      this.mEditor.invalidateTextDisplayList(); 
  }
  
  @RemotableViewMethod
  public final void setHint(int paramInt) {
    this.mHintId = paramInt;
    setHint(getContext().getResources().getText(paramInt));
  }
  
  @CapturedViewProperty
  public CharSequence getHint() {
    return this.mHint;
  }
  
  public boolean isSingleLine() {
    return this.mSingleLine;
  }
  
  private static boolean isMultilineInputType(int paramInt) {
    boolean bool;
    if ((0x2000F & paramInt) == 131073) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  CharSequence removeSuggestionSpans(CharSequence paramCharSequence) {
    CharSequence charSequence = paramCharSequence;
    if (paramCharSequence instanceof Spanned) {
      Spannable spannable;
      if (paramCharSequence instanceof Spannable) {
        spannable = (Spannable)paramCharSequence;
      } else {
        spannable = this.mSpannableFactory.newSpannable(paramCharSequence);
      } 
      SuggestionSpan[] arrayOfSuggestionSpan = spannable.<SuggestionSpan>getSpans(0, paramCharSequence.length(), SuggestionSpan.class);
      if (arrayOfSuggestionSpan.length == 0)
        return paramCharSequence; 
      paramCharSequence = spannable;
      byte b = 0;
      while (true) {
        charSequence = paramCharSequence;
        if (b < arrayOfSuggestionSpan.length) {
          spannable.removeSpan(arrayOfSuggestionSpan[b]);
          b++;
          continue;
        } 
        break;
      } 
    } 
    return charSequence;
  }
  
  public void setInputType(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getInputType : ()I
    //   4: invokestatic isPasswordInputType : (I)Z
    //   7: istore_2
    //   8: aload_0
    //   9: invokevirtual getInputType : ()I
    //   12: invokestatic isVisiblePasswordInputType : (I)Z
    //   15: istore_3
    //   16: aload_0
    //   17: iload_1
    //   18: iconst_0
    //   19: invokespecial setInputType : (IZ)V
    //   22: iload_1
    //   23: invokestatic isPasswordInputType : (I)Z
    //   26: istore #4
    //   28: iload_1
    //   29: invokestatic isVisiblePasswordInputType : (I)Z
    //   32: istore #5
    //   34: iconst_0
    //   35: istore #6
    //   37: iconst_0
    //   38: istore #7
    //   40: iload #4
    //   42: ifeq -> 95
    //   45: aload_0
    //   46: invokestatic getInstance : ()Landroid/text/method/PasswordTransformationMethod;
    //   49: invokevirtual setTransformationMethod : (Landroid/text/method/TransformationMethod;)V
    //   52: getstatic com/oplus/font/IOplusFontManager.DEFAULT : Lcom/oplus/font/IOplusFontManager;
    //   55: astore #8
    //   57: aload #8
    //   59: iconst_0
    //   60: anewarray java/lang/Object
    //   63: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   66: checkcast com/oplus/font/IOplusFontManager
    //   69: iconst_3
    //   70: iconst_1
    //   71: invokeinterface getTypefaceIndex : (II)I
    //   76: istore #7
    //   78: aload_0
    //   79: aconst_null
    //   80: aconst_null
    //   81: iload #7
    //   83: iconst_0
    //   84: iconst_m1
    //   85: invokespecial setTypefaceFromAttrs : (Landroid/graphics/Typeface;Ljava/lang/String;III)V
    //   88: iload #6
    //   90: istore #7
    //   92: goto -> 190
    //   95: iload #5
    //   97: ifeq -> 152
    //   100: aload_0
    //   101: getfield mTransformation : Landroid/text/method/TransformationMethod;
    //   104: invokestatic getInstance : ()Landroid/text/method/PasswordTransformationMethod;
    //   107: if_acmpne -> 113
    //   110: iconst_1
    //   111: istore #7
    //   113: getstatic com/oplus/font/IOplusFontManager.DEFAULT : Lcom/oplus/font/IOplusFontManager;
    //   116: astore #8
    //   118: aload #8
    //   120: iconst_0
    //   121: anewarray java/lang/Object
    //   124: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   127: checkcast com/oplus/font/IOplusFontManager
    //   130: iconst_3
    //   131: iconst_1
    //   132: invokeinterface getTypefaceIndex : (II)I
    //   137: istore #6
    //   139: aload_0
    //   140: aconst_null
    //   141: aconst_null
    //   142: iload #6
    //   144: iconst_0
    //   145: iconst_m1
    //   146: invokespecial setTypefaceFromAttrs : (Landroid/graphics/Typeface;Ljava/lang/String;III)V
    //   149: goto -> 190
    //   152: iload_2
    //   153: ifne -> 164
    //   156: iload #6
    //   158: istore #7
    //   160: iload_3
    //   161: ifeq -> 190
    //   164: aload_0
    //   165: aconst_null
    //   166: aconst_null
    //   167: iconst_m1
    //   168: iconst_0
    //   169: iconst_m1
    //   170: invokespecial setTypefaceFromAttrs : (Landroid/graphics/Typeface;Ljava/lang/String;III)V
    //   173: iload #6
    //   175: istore #7
    //   177: aload_0
    //   178: getfield mTransformation : Landroid/text/method/TransformationMethod;
    //   181: invokestatic getInstance : ()Landroid/text/method/PasswordTransformationMethod;
    //   184: if_acmpne -> 190
    //   187: iconst_1
    //   188: istore #7
    //   190: iload_1
    //   191: invokestatic isMultilineInputType : (I)Z
    //   194: iconst_1
    //   195: ixor
    //   196: istore_2
    //   197: aload_0
    //   198: getfield mSingleLine : Z
    //   201: iload_2
    //   202: if_icmpne -> 210
    //   205: iload #7
    //   207: ifeq -> 220
    //   210: aload_0
    //   211: iload_2
    //   212: iload #4
    //   214: iconst_1
    //   215: ixor
    //   216: iconst_1
    //   217: invokespecial applySingleLine : (ZZZ)V
    //   220: aload_0
    //   221: invokevirtual isSuggestionsEnabled : ()Z
    //   224: ifne -> 239
    //   227: aload_0
    //   228: aload_0
    //   229: aload_0
    //   230: getfield mText : Ljava/lang/CharSequence;
    //   233: invokevirtual removeSuggestionSpans : (Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    //   236: invokespecial setTextInternal : (Ljava/lang/CharSequence;)V
    //   239: aload_0
    //   240: invokespecial getInputMethodManager : ()Landroid/view/inputmethod/InputMethodManager;
    //   243: astore #8
    //   245: aload #8
    //   247: ifnull -> 256
    //   250: aload #8
    //   252: aload_0
    //   253: invokevirtual restartInput : (Landroid/view/View;)V
    //   256: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #6627	-> 0
    //   #6628	-> 8
    //   #6629	-> 16
    //   #6630	-> 22
    //   #6631	-> 28
    //   #6632	-> 34
    //   #6633	-> 40
    //   #6634	-> 45
    //   #6640	-> 52
    //   #6641	-> 57
    //   #6640	-> 78
    //   #6644	-> 95
    //   #6645	-> 100
    //   #6646	-> 110
    //   #6653	-> 113
    //   #6654	-> 118
    //   #6653	-> 139
    //   #6657	-> 152
    //   #6659	-> 164
    //   #6662	-> 173
    //   #6663	-> 187
    //   #6667	-> 190
    //   #6671	-> 197
    //   #6674	-> 210
    //   #6677	-> 220
    //   #6678	-> 227
    //   #6681	-> 239
    //   #6682	-> 245
    //   #6683	-> 256
  }
  
  boolean hasPasswordTransformationMethod() {
    return this.mTransformation instanceof android.text.method.PasswordTransformationMethod;
  }
  
  public boolean isAnyPasswordInputType() {
    int i = getInputType();
    return (isPasswordInputType(i) || isVisiblePasswordInputType(i));
  }
  
  static boolean isPasswordInputType(int paramInt) {
    paramInt &= 0xFFF;
    return (paramInt == 129 || paramInt == 225 || paramInt == 18);
  }
  
  private static boolean isVisiblePasswordInputType(int paramInt) {
    boolean bool;
    if ((paramInt & 0xFFF) == 145) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setRawInputType(int paramInt) {
    if (paramInt == 0 && this.mEditor == null)
      return; 
    createEditorIfNeeded();
    this.mEditor.mInputType = paramInt;
  }
  
  private Locale getCustomLocaleForKeyListenerOrNull() {
    if (!this.mUseInternationalizedInput)
      return null; 
    LocaleList localeList = getImeHintLocales();
    if (localeList == null)
      return null; 
    return localeList.get(0);
  }
  
  private void setInputType(int paramInt, boolean paramBoolean) {
    TextKeyListener textKeyListener;
    int i = paramInt & 0xF;
    boolean bool1 = true, bool2 = true;
    if (i == 1) {
      TextKeyListener.Capitalize capitalize;
      if ((0x8000 & paramInt) == 0)
        bool2 = false; 
      if ((paramInt & 0x1000) != 0) {
        capitalize = TextKeyListener.Capitalize.CHARACTERS;
      } else if ((paramInt & 0x2000) != 0) {
        capitalize = TextKeyListener.Capitalize.WORDS;
      } else if ((paramInt & 0x4000) != 0) {
        capitalize = TextKeyListener.Capitalize.SENTENCES;
      } else {
        capitalize = TextKeyListener.Capitalize.NONE;
      } 
      textKeyListener = TextKeyListener.getInstance(bool2, capitalize);
    } else if (i == 2) {
      Locale locale = getCustomLocaleForKeyListenerOrNull();
      if ((paramInt & 0x1000) != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if ((paramInt & 0x2000) == 0)
        bool1 = false; 
      DigitsKeyListener digitsKeyListener = DigitsKeyListener.getInstance(locale, bool2, bool1);
      i = paramInt;
      if (locale != null) {
        int j = digitsKeyListener.getInputType();
        i = paramInt;
        if ((j & 0xF) != 2) {
          i = j;
          if ((paramInt & 0x10) != 0)
            i = j | 0x80; 
        } 
      } 
      paramInt = i;
    } else if (i == 4) {
      TimeKeyListener timeKeyListener;
      DateKeyListener dateKeyListener;
      Locale locale = getCustomLocaleForKeyListenerOrNull();
      i = paramInt & 0xFF0;
      if (i != 16) {
        DateTimeKeyListener dateTimeKeyListener;
        if (i != 32) {
          dateTimeKeyListener = DateTimeKeyListener.getInstance(locale);
        } else {
          timeKeyListener = TimeKeyListener.getInstance((Locale)dateTimeKeyListener);
        } 
      } else {
        dateKeyListener = DateKeyListener.getInstance((Locale)timeKeyListener);
      } 
      if (this.mUseInternationalizedInput)
        paramInt = dateKeyListener.getInputType(); 
    } else if (i == 3) {
      DialerKeyListener dialerKeyListener = DialerKeyListener.getInstance();
    } else {
      textKeyListener = TextKeyListener.getInstance();
    } 
    setRawInputType(paramInt);
    this.mListenerChanged = false;
    if (paramBoolean) {
      createEditorIfNeeded();
      this.mEditor.mKeyListener = textKeyListener;
    } else {
      setKeyListenerOnly(textKeyListener);
    } 
  }
  
  public int getInputType() {
    int i;
    Editor editor = this.mEditor;
    if (editor == null) {
      i = 0;
    } else {
      i = editor.mInputType;
    } 
    return i;
  }
  
  public void setImeOptions(int paramInt) {
    createEditorIfNeeded();
    this.mEditor.createInputContentTypeIfNeeded();
    this.mEditor.mInputContentType.imeOptions = paramInt;
  }
  
  public int getImeOptions() {
    boolean bool;
    Editor editor = this.mEditor;
    if (editor != null && editor.mInputContentType != null) {
      bool = this.mEditor.mInputContentType.imeOptions;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setImeActionLabel(CharSequence paramCharSequence, int paramInt) {
    createEditorIfNeeded();
    this.mEditor.createInputContentTypeIfNeeded();
    this.mEditor.mInputContentType.imeActionLabel = paramCharSequence;
    this.mEditor.mInputContentType.imeActionId = paramInt;
  }
  
  public CharSequence getImeActionLabel() {
    Editor editor = this.mEditor;
    if (editor != null && editor.mInputContentType != null) {
      CharSequence charSequence = this.mEditor.mInputContentType.imeActionLabel;
    } else {
      editor = null;
    } 
    return (CharSequence)editor;
  }
  
  public int getImeActionId() {
    boolean bool;
    Editor editor = this.mEditor;
    if (editor != null && editor.mInputContentType != null) {
      bool = this.mEditor.mInputContentType.imeActionId;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setOnEditorActionListener(OnEditorActionListener paramOnEditorActionListener) {
    createEditorIfNeeded();
    this.mEditor.createInputContentTypeIfNeeded();
    this.mEditor.mInputContentType.onEditorActionListener = paramOnEditorActionListener;
  }
  
  public void onEditorAction(int paramInt) {
    Editor.InputContentType inputContentType;
    Editor editor = this.mEditor;
    if (editor == null) {
      editor = null;
    } else {
      inputContentType = editor.mInputContentType;
    } 
    if (inputContentType != null) {
      if (inputContentType.onEditorActionListener != null && inputContentType.onEditorActionListener.onEditorAction(this, paramInt, null))
        return; 
      if (paramInt == 5) {
        View view = focusSearch(2);
        if (view == null || view.requestFocus(2))
          return; 
        throw new IllegalStateException("focus search returned a view that wasn't able to take focus!");
      } 
      if (paramInt == 7) {
        View view = focusSearch(1);
        if (view == null || view.requestFocus(1))
          return; 
        throw new IllegalStateException("focus search returned a view that wasn't able to take focus!");
      } 
      if (paramInt == 6) {
        InputMethodManager inputMethodManager = getInputMethodManager();
        if (inputMethodManager != null && inputMethodManager.isActive(this))
          inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0); 
        return;
      } 
    } 
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl != null) {
      long l = SystemClock.uptimeMillis();
      viewRootImpl.dispatchKeyFromIme(new KeyEvent(l, l, 0, 66, 0, 0, -1, 0, 22));
      KeyEvent keyEvent = new KeyEvent(SystemClock.uptimeMillis(), l, 1, 66, 0, 0, -1, 0, 22);
      viewRootImpl.dispatchKeyFromIme(keyEvent);
    } 
  }
  
  public void setPrivateImeOptions(String paramString) {
    createEditorIfNeeded();
    this.mEditor.createInputContentTypeIfNeeded();
    this.mEditor.mInputContentType.privateImeOptions = paramString;
  }
  
  public String getPrivateImeOptions() {
    Editor editor = this.mEditor;
    if (editor != null && editor.mInputContentType != null) {
      String str = this.mEditor.mInputContentType.privateImeOptions;
    } else {
      editor = null;
    } 
    return (String)editor;
  }
  
  public void setInputExtras(int paramInt) throws XmlPullParserException, IOException {
    createEditorIfNeeded();
    XmlResourceParser xmlResourceParser = getResources().getXml(paramInt);
    this.mEditor.createInputContentTypeIfNeeded();
    this.mEditor.mInputContentType.extras = new Bundle();
    getResources().parseBundleExtras(xmlResourceParser, this.mEditor.mInputContentType.extras);
  }
  
  public Bundle getInputExtras(boolean paramBoolean) {
    if (this.mEditor == null && !paramBoolean)
      return null; 
    createEditorIfNeeded();
    if (this.mEditor.mInputContentType == null) {
      if (!paramBoolean)
        return null; 
      this.mEditor.createInputContentTypeIfNeeded();
    } 
    if (this.mEditor.mInputContentType.extras == null) {
      if (!paramBoolean)
        return null; 
      this.mEditor.mInputContentType.extras = new Bundle();
    } 
    return this.mEditor.mInputContentType.extras;
  }
  
  public void setImeHintLocales(LocaleList paramLocaleList) {
    createEditorIfNeeded();
    this.mEditor.createInputContentTypeIfNeeded();
    this.mEditor.mInputContentType.imeHintLocales = paramLocaleList;
    if (this.mUseInternationalizedInput) {
      Locale locale;
      if (paramLocaleList == null) {
        paramLocaleList = null;
      } else {
        locale = paramLocaleList.get(0);
      } 
      changeListenerLocaleTo(locale);
    } 
  }
  
  public LocaleList getImeHintLocales() {
    Editor editor = this.mEditor;
    if (editor == null)
      return null; 
    if (editor.mInputContentType == null)
      return null; 
    return this.mEditor.mInputContentType.imeHintLocales;
  }
  
  public CharSequence getError() {
    CharSequence charSequence;
    Editor editor = this.mEditor;
    if (editor == null) {
      editor = null;
    } else {
      charSequence = editor.mError;
    } 
    return charSequence;
  }
  
  @RemotableViewMethod
  public void setError(CharSequence paramCharSequence) {
    if (paramCharSequence == null) {
      setError((CharSequence)null, (Drawable)null);
    } else {
      Drawable drawable = getContext().getDrawable(17302901);
      drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
      setError(paramCharSequence, drawable);
    } 
  }
  
  public void setError(CharSequence paramCharSequence, Drawable paramDrawable) {
    createEditorIfNeeded();
    this.mEditor.setError(paramCharSequence, paramDrawable);
    notifyViewAccessibilityStateChangedIfNeeded(0);
  }
  
  protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
    Editor editor = this.mEditor;
    if (editor != null)
      editor.setFrame(); 
    restartMarqueeIfNeeded();
    return bool;
  }
  
  private void restartMarqueeIfNeeded() {
    if (this.mRestartMarquee && this.mEllipsize == TextUtils.TruncateAt.MARQUEE) {
      this.mRestartMarquee = false;
      startMarquee();
    } 
  }
  
  public void setFilters(InputFilter[] paramArrayOfInputFilter) {
    if (paramArrayOfInputFilter != null) {
      this.mFilters = paramArrayOfInputFilter;
      CharSequence charSequence = this.mText;
      if (charSequence instanceof Editable)
        setFilters((Editable)charSequence, paramArrayOfInputFilter); 
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  private void setFilters(Editable paramEditable, InputFilter[] paramArrayOfInputFilter) {
    Editor editor = this.mEditor;
    if (editor != null) {
      boolean bool;
      if (editor.mUndoInputFilter != null) {
        bool = true;
      } else {
        bool = false;
      } 
      boolean bool1 = this.mEditor.mKeyListener instanceof InputFilter;
      int i = 0;
      if (bool)
        i = 0 + 1; 
      int j = i;
      if (bool1)
        j = i + 1; 
      if (j > 0) {
        InputFilter[] arrayOfInputFilter = new InputFilter[paramArrayOfInputFilter.length + j];
        System.arraycopy(paramArrayOfInputFilter, 0, arrayOfInputFilter, 0, paramArrayOfInputFilter.length);
        i = 0;
        if (bool) {
          arrayOfInputFilter[paramArrayOfInputFilter.length] = this.mEditor.mUndoInputFilter;
          i = 0 + 1;
        } 
        if (bool1)
          arrayOfInputFilter[paramArrayOfInputFilter.length + i] = (InputFilter)this.mEditor.mKeyListener; 
        paramEditable.setFilters(arrayOfInputFilter);
        return;
      } 
    } 
    paramEditable.setFilters(paramArrayOfInputFilter);
  }
  
  public InputFilter[] getFilters() {
    return this.mFilters;
  }
  
  private int getBoxHeight(Layout paramLayout) {
    Insets insets;
    int i;
    if (isLayoutModeOptical(this.mParent)) {
      insets = getOpticalInsets();
    } else {
      insets = Insets.NONE;
    } 
    if (paramLayout == this.mHintLayout) {
      i = getCompoundPaddingTop() + getCompoundPaddingBottom();
    } else {
      i = getExtendedPaddingTop() + getExtendedPaddingBottom();
    } 
    return getMeasuredHeight() - i + insets.top + insets.bottom;
  }
  
  int getVerticalOffset(boolean paramBoolean) {
    byte b = 0;
    int i = this.mGravity & 0x70;
    Layout layout1 = this.mLayout;
    Layout layout2 = layout1;
    if (!paramBoolean) {
      layout2 = layout1;
      if (this.mText.length() == 0) {
        layout2 = layout1;
        if (this.mHintLayout != null)
          layout2 = this.mHintLayout; 
      } 
    } 
    int j = b;
    if (i != 48) {
      int k = getBoxHeight(layout2);
      int m = layout2.getHeight();
      j = b;
      if (m < k)
        if (i == 80) {
          j = k - m;
        } else {
          j = k - m >> 1;
        }  
    } 
    return j;
  }
  
  private int getBottomVerticalOffset(boolean paramBoolean) {
    byte b = 0;
    int i = this.mGravity & 0x70;
    Layout layout1 = this.mLayout;
    Layout layout2 = layout1;
    if (!paramBoolean) {
      layout2 = layout1;
      if (this.mText.length() == 0) {
        layout2 = layout1;
        if (this.mHintLayout != null)
          layout2 = this.mHintLayout; 
      } 
    } 
    int j = b;
    if (i != 80) {
      int k = getBoxHeight(layout2);
      int m = layout2.getHeight();
      j = b;
      if (m < k)
        if (i == 48) {
          j = k - m;
        } else {
          j = k - m >> 1;
        }  
    } 
    return j;
  }
  
  void invalidateCursorPath() {
    if (this.mHighlightPathBogus) {
      invalidateCursor();
    } else {
      int i = getCompoundPaddingLeft();
      int j = getExtendedPaddingTop() + getVerticalOffset(true);
      if (this.mEditor.mDrawableForCursor == null) {
        synchronized (TEMP_RECTF) {
          float f1 = (float)Math.ceil(this.mTextPaint.getStrokeWidth());
          float f2 = f1;
          if (f1 < 1.0F)
            f2 = 1.0F; 
          f2 /= 2.0F;
          this.mHighlightPath.computeBounds(TEMP_RECTF, false);
          int k = (int)Math.floor((i + TEMP_RECTF.left - f2));
          double d = (j + TEMP_RECTF.top - f2);
          int m = (int)Math.floor(d);
          d = (i + TEMP_RECTF.right + f2);
          i = (int)Math.ceil(d);
          d = (j + TEMP_RECTF.bottom + f2);
          j = (int)Math.ceil(d);
          invalidate(k, m, i, j);
        } 
      } else {
        Rect rect = this.mEditor.mDrawableForCursor.getBounds();
        invalidate(rect.left + i, rect.top + j, rect.right + i, rect.bottom + j);
      } 
    } 
  }
  
  void invalidateCursor() {
    int i = getSelectionEnd();
    invalidateCursor(i, i, i);
  }
  
  private void invalidateCursor(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 >= 0 || paramInt2 >= 0 || paramInt3 >= 0) {
      int i = Math.min(Math.min(paramInt1, paramInt2), paramInt3);
      paramInt1 = Math.max(Math.max(paramInt1, paramInt2), paramInt3);
      invalidateRegion(i, paramInt1, true);
    } 
  }
  
  void invalidateRegion(int paramInt1, int paramInt2, boolean paramBoolean) {
    Layout layout = this.mLayout;
    if (layout == null) {
      invalidate();
    } else {
      int m, i = layout.getLineForOffset(paramInt1);
      int j = this.mLayout.getLineTop(i);
      int k = j;
      if (i > 0)
        k = j - this.mLayout.getLineDescent(i - 1); 
      if (paramInt1 == paramInt2) {
        m = i;
      } else {
        m = this.mLayout.getLineForOffset(paramInt2);
      } 
      int n = this.mLayout.getLineBottom(m);
      int i1 = k;
      j = n;
      if (paramBoolean) {
        Editor editor = this.mEditor;
        i1 = k;
        j = n;
        if (editor != null) {
          i1 = k;
          j = n;
          if (editor.mDrawableForCursor != null) {
            Rect rect = this.mEditor.mDrawableForCursor.getBounds();
            i1 = Math.min(k, rect.top);
            j = Math.max(n, rect.bottom);
          } 
        } 
      } 
      k = getCompoundPaddingLeft();
      n = getExtendedPaddingTop() + getVerticalOffset(true);
      if (i == m && !paramBoolean) {
        paramInt1 = (int)this.mLayout.getPrimaryHorizontal(paramInt1);
        paramInt2 = (int)(this.mLayout.getPrimaryHorizontal(paramInt2) + 1.0D);
        paramInt1 += k;
        paramInt2 += k;
      } else {
        paramInt1 = k;
        paramInt2 = getWidth() - getCompoundPaddingRight();
      } 
      invalidate(this.mScrollX + paramInt1, n + i1, this.mScrollX + paramInt2, n + j);
    } 
  }
  
  private void registerForPreDraw() {
    if (!this.mPreDrawRegistered) {
      getViewTreeObserver().addOnPreDrawListener(this);
      this.mPreDrawRegistered = true;
    } 
  }
  
  private void unregisterForPreDraw() {
    getViewTreeObserver().removeOnPreDrawListener(this);
    this.mPreDrawRegistered = false;
    this.mPreDrawListenerDetached = false;
  }
  
  public boolean onPreDraw() {
    if (this.mLayout == null)
      assumeLayout(); 
    if (this.mMovement != null) {
      int i = getSelectionEnd();
      Editor editor1 = this.mEditor;
      int j = i;
      if (editor1 != null) {
        j = i;
        if (editor1.mSelectionModifierCursorController != null) {
          Editor.SelectionModifierCursorController selectionModifierCursorController = this.mEditor.mSelectionModifierCursorController;
          j = i;
          if (selectionModifierCursorController.isSelectionStartDragged())
            j = getSelectionStart(); 
        } 
      } 
      i = j;
      if (j < 0) {
        i = j;
        if ((this.mGravity & 0x70) == 80)
          i = this.mText.length(); 
      } 
      if (i >= 0)
        bringPointIntoView(i); 
    } else {
      bringTextIntoView();
    } 
    Editor editor = this.mEditor;
    if (editor != null && editor.mCreatedWithASelection) {
      this.mEditor.refreshTextActionMode();
      this.mEditor.mCreatedWithASelection = false;
    } 
    unregisterForPreDraw();
    return true;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onAttachedToWindow(); 
    if (this.mPreDrawListenerDetached) {
      getViewTreeObserver().addOnPreDrawListener(this);
      this.mPreDrawListenerDetached = false;
    } 
  }
  
  protected void onDetachedFromWindowInternal() {
    if (this.mPreDrawRegistered) {
      getViewTreeObserver().removeOnPreDrawListener(this);
      this.mPreDrawListenerDetached = true;
    } 
    resetResolvedDrawables();
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onDetachedFromWindow(); 
    super.onDetachedFromWindowInternal();
  }
  
  public void onScreenStateChanged(int paramInt) {
    super.onScreenStateChanged(paramInt);
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onScreenStateChanged(paramInt); 
  }
  
  protected boolean isPaddingOffsetRequired() {
    return (this.mShadowRadius != 0.0F || this.mDrawables != null);
  }
  
  protected int getLeftPaddingOffset() {
    int i = getCompoundPaddingLeft(), j = this.mPaddingLeft;
    float f1 = this.mShadowDx, f2 = this.mShadowRadius;
    int k = (int)Math.min(0.0F, f1 - f2);
    return i - j + k;
  }
  
  protected int getTopPaddingOffset() {
    return (int)Math.min(0.0F, this.mShadowDy - this.mShadowRadius);
  }
  
  protected int getBottomPaddingOffset() {
    return (int)Math.max(0.0F, this.mShadowDy + this.mShadowRadius);
  }
  
  protected int getRightPaddingOffset() {
    int i = -(getCompoundPaddingRight() - this.mPaddingRight);
    float f1 = this.mShadowDx, f2 = this.mShadowRadius;
    int j = (int)Math.max(0.0F, f1 + f2);
    return i + j;
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    boolean bool = super.verifyDrawable(paramDrawable);
    if (!bool) {
      Drawables drawables = this.mDrawables;
      if (drawables != null)
        for (Drawable drawable : drawables.mShowing) {
          if (paramDrawable == drawable)
            return true; 
        }  
    } 
    return bool;
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawables drawables = this.mDrawables;
    if (drawables != null)
      for (Drawable drawable : drawables.mShowing) {
        if (drawable != null)
          drawable.jumpToCurrentState(); 
      }  
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    int i = 0, j = 0, k = 0;
    if (verifyDrawable(paramDrawable)) {
      Rect rect = paramDrawable.getBounds();
      int m = this.mScrollX;
      j = this.mScrollY;
      Drawables drawables = this.mDrawables;
      int n = m, i1 = j;
      if (drawables != null)
        if (paramDrawable == drawables.mShowing[0]) {
          i = getCompoundPaddingTop();
          int i2 = getCompoundPaddingBottom();
          i1 = this.mBottom;
          k = this.mTop;
          n = m + this.mPaddingLeft;
          i1 = j + (i1 - k - i2 - i - drawables.mDrawableHeightLeft) / 2 + i;
          i = 1;
        } else if (paramDrawable == drawables.mShowing[2]) {
          int i2 = getCompoundPaddingTop();
          i1 = getCompoundPaddingBottom();
          k = this.mBottom;
          i = this.mTop;
          n = m + this.mRight - this.mLeft - this.mPaddingRight - drawables.mDrawableSizeRight;
          i1 = j + (k - i - i1 - i2 - drawables.mDrawableHeightRight) / 2 + i2;
          i = 1;
        } else if (paramDrawable == drawables.mShowing[1]) {
          i = getCompoundPaddingLeft();
          n = getCompoundPaddingRight();
          k = this.mRight;
          i1 = this.mLeft;
          n = m + (k - i1 - n - i - drawables.mDrawableWidthTop) / 2 + i;
          i1 = j + this.mPaddingTop;
          i = 1;
        } else {
          i = k;
          n = m;
          i1 = j;
          if (paramDrawable == drawables.mShowing[3]) {
            k = getCompoundPaddingLeft();
            n = getCompoundPaddingRight();
            i1 = this.mRight;
            i = this.mLeft;
            n = m + (i1 - i - n - k - drawables.mDrawableWidthBottom) / 2 + k;
            i1 = j + this.mBottom - this.mTop - this.mPaddingBottom - drawables.mDrawableSizeBottom;
            i = 1;
          } 
        }  
      j = i;
      if (i != 0) {
        invalidate(rect.left + n, rect.top + i1, rect.right + n, rect.bottom + i1);
        j = i;
      } 
    } 
    if (j == 0)
      super.invalidateDrawable(paramDrawable); 
  }
  
  public boolean hasOverlappingRendering() {
    return ((getBackground() != null && getBackground().getCurrent() != null) || this.mSpannable != null || hasSelection() || isHorizontalFadingEdgeEnabled() || this.mShadowColor != 0);
  }
  
  public boolean isTextSelectable() {
    boolean bool;
    Editor editor = this.mEditor;
    if (editor == null) {
      bool = false;
    } else {
      bool = editor.mTextIsSelectable;
    } 
    return bool;
  }
  
  public void setTextIsSelectable(boolean paramBoolean) {
    MovementMethod movementMethod;
    BufferType bufferType;
    if (!paramBoolean && this.mEditor == null)
      return; 
    createEditorIfNeeded();
    if (this.mEditor.mTextIsSelectable == paramBoolean)
      return; 
    this.mEditor.mTextIsSelectable = paramBoolean;
    setFocusableInTouchMode(paramBoolean);
    setFocusable(16);
    setClickable(paramBoolean);
    setLongClickable(paramBoolean);
    if (paramBoolean) {
      movementMethod = ArrowKeyMovementMethod.getInstance();
    } else {
      movementMethod = null;
    } 
    setMovementMethod(movementMethod);
    CharSequence charSequence = this.mText;
    if (paramBoolean) {
      bufferType = BufferType.SPANNABLE;
    } else {
      bufferType = BufferType.NORMAL;
    } 
    setText(charSequence, bufferType);
    this.mEditor.prepareCursorControllers();
  }
  
  protected int[] onCreateDrawableState(int paramInt) {
    int[] arrayOfInt;
    if (this.mSingleLine) {
      arrayOfInt = super.onCreateDrawableState(paramInt);
    } else {
      arrayOfInt = super.onCreateDrawableState(paramInt + 1);
      mergeDrawableStates(arrayOfInt, MULTILINE_STATE_SET);
    } 
    if (isTextSelectable()) {
      int i = arrayOfInt.length;
      for (paramInt = 0; paramInt < i; paramInt++) {
        if (arrayOfInt[paramInt] == 16842919) {
          int[] arrayOfInt1 = new int[i - 1];
          System.arraycopy(arrayOfInt, 0, arrayOfInt1, 0, paramInt);
          System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt1, paramInt, i - paramInt - 1);
          return arrayOfInt1;
        } 
      } 
    } 
    return arrayOfInt;
  }
  
  private Path getUpdatedHighlightPath() {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: getfield mHighlightPaint : Landroid/graphics/Paint;
    //   6: astore_2
    //   7: aload_0
    //   8: invokevirtual getSelectionStart : ()I
    //   11: istore_3
    //   12: aload_0
    //   13: invokevirtual getSelectionEnd : ()I
    //   16: istore #4
    //   18: aload_1
    //   19: astore #5
    //   21: aload_0
    //   22: getfield mMovement : Landroid/text/method/MovementMethod;
    //   25: ifnull -> 239
    //   28: aload_0
    //   29: invokevirtual isFocused : ()Z
    //   32: ifne -> 45
    //   35: aload_1
    //   36: astore #5
    //   38: aload_0
    //   39: invokevirtual isPressed : ()Z
    //   42: ifeq -> 239
    //   45: aload_1
    //   46: astore #5
    //   48: iload_3
    //   49: iflt -> 239
    //   52: iload_3
    //   53: iload #4
    //   55: if_icmpne -> 167
    //   58: aload_0
    //   59: getfield mEditor : Landroid/widget/Editor;
    //   62: astore #6
    //   64: aload_1
    //   65: astore #5
    //   67: aload #6
    //   69: ifnull -> 239
    //   72: aload_1
    //   73: astore #5
    //   75: aload #6
    //   77: invokevirtual shouldRenderCursor : ()Z
    //   80: ifeq -> 239
    //   83: aload_0
    //   84: getfield mHighlightPathBogus : Z
    //   87: ifeq -> 143
    //   90: aload_0
    //   91: getfield mHighlightPath : Landroid/graphics/Path;
    //   94: ifnonnull -> 108
    //   97: aload_0
    //   98: new android/graphics/Path
    //   101: dup
    //   102: invokespecial <init> : ()V
    //   105: putfield mHighlightPath : Landroid/graphics/Path;
    //   108: aload_0
    //   109: getfield mHighlightPath : Landroid/graphics/Path;
    //   112: invokevirtual reset : ()V
    //   115: aload_0
    //   116: getfield mLayout : Landroid/text/Layout;
    //   119: iload_3
    //   120: aload_0
    //   121: getfield mHighlightPath : Landroid/graphics/Path;
    //   124: aload_0
    //   125: getfield mText : Ljava/lang/CharSequence;
    //   128: invokevirtual getCursorPath : (ILandroid/graphics/Path;Ljava/lang/CharSequence;)V
    //   131: aload_0
    //   132: getfield mEditor : Landroid/widget/Editor;
    //   135: invokevirtual updateCursorPosition : ()V
    //   138: aload_0
    //   139: iconst_0
    //   140: putfield mHighlightPathBogus : Z
    //   143: aload_2
    //   144: aload_0
    //   145: getfield mCurTextColor : I
    //   148: invokevirtual setColor : (I)V
    //   151: aload_2
    //   152: getstatic android/graphics/Paint$Style.STROKE : Landroid/graphics/Paint$Style;
    //   155: invokevirtual setStyle : (Landroid/graphics/Paint$Style;)V
    //   158: aload_0
    //   159: getfield mHighlightPath : Landroid/graphics/Path;
    //   162: astore #5
    //   164: goto -> 239
    //   167: aload_0
    //   168: getfield mHighlightPathBogus : Z
    //   171: ifeq -> 218
    //   174: aload_0
    //   175: getfield mHighlightPath : Landroid/graphics/Path;
    //   178: ifnonnull -> 192
    //   181: aload_0
    //   182: new android/graphics/Path
    //   185: dup
    //   186: invokespecial <init> : ()V
    //   189: putfield mHighlightPath : Landroid/graphics/Path;
    //   192: aload_0
    //   193: getfield mHighlightPath : Landroid/graphics/Path;
    //   196: invokevirtual reset : ()V
    //   199: aload_0
    //   200: getfield mLayout : Landroid/text/Layout;
    //   203: iload_3
    //   204: iload #4
    //   206: aload_0
    //   207: getfield mHighlightPath : Landroid/graphics/Path;
    //   210: invokevirtual getSelectionPath : (IILandroid/graphics/Path;)V
    //   213: aload_0
    //   214: iconst_0
    //   215: putfield mHighlightPathBogus : Z
    //   218: aload_2
    //   219: aload_0
    //   220: getfield mHighlightColor : I
    //   223: invokevirtual setColor : (I)V
    //   226: aload_2
    //   227: getstatic android/graphics/Paint$Style.FILL : Landroid/graphics/Paint$Style;
    //   230: invokevirtual setStyle : (Landroid/graphics/Paint$Style;)V
    //   233: aload_0
    //   234: getfield mHighlightPath : Landroid/graphics/Path;
    //   237: astore #5
    //   239: aload #5
    //   241: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #7909	-> 0
    //   #7910	-> 2
    //   #7912	-> 7
    //   #7913	-> 12
    //   #7914	-> 18
    //   #7915	-> 52
    //   #7916	-> 58
    //   #7917	-> 83
    //   #7918	-> 90
    //   #7919	-> 108
    //   #7920	-> 115
    //   #7921	-> 131
    //   #7922	-> 138
    //   #7926	-> 143
    //   #7927	-> 151
    //   #7928	-> 158
    //   #7931	-> 167
    //   #7932	-> 174
    //   #7933	-> 192
    //   #7934	-> 199
    //   #7935	-> 213
    //   #7939	-> 218
    //   #7940	-> 226
    //   #7942	-> 233
    //   #7945	-> 239
  }
  
  public int getHorizontalOffsetForDrawables() {
    return 0;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    float f2;
    restartMarqueeIfNeeded();
    super.onDraw(paramCanvas);
    int i = getCompoundPaddingLeft();
    int j = getCompoundPaddingTop();
    int k = getCompoundPaddingRight();
    int m = getCompoundPaddingBottom();
    int n = this.mScrollX;
    int i1 = this.mScrollY;
    int i2 = this.mRight;
    int i3 = this.mLeft;
    int i4 = this.mBottom;
    int i5 = this.mTop;
    boolean bool = isLayoutRtl();
    int i6 = getHorizontalOffsetForDrawables();
    if (bool) {
      i7 = 0;
    } else {
      i7 = i6;
    } 
    if (!bool)
      i6 = 0; 
    Drawables drawables = this.mDrawables;
    if (drawables != null) {
      int i10 = i4 - i5 - m - j;
      k = i2 - i3 - k - i;
      if (drawables.mShowing[0] != null) {
        paramCanvas.save();
        paramCanvas.translate((this.mPaddingLeft + n + i7), (i1 + j + (i10 - drawables.mDrawableHeightLeft) / 2));
        drawables.mShowing[0].draw(paramCanvas);
        paramCanvas.restore();
      } 
      if (drawables.mShowing[2] != null) {
        paramCanvas.save();
        paramCanvas.translate((n + i2 - i3 - this.mPaddingRight - drawables.mDrawableSizeRight - i6), (i1 + j + (i10 - drawables.mDrawableHeightRight) / 2));
        drawables.mShowing[2].draw(paramCanvas);
        paramCanvas.restore();
      } 
      if (drawables.mShowing[1] != null) {
        paramCanvas.save();
        paramCanvas.translate((n + i + (k - drawables.mDrawableWidthTop) / 2), (this.mPaddingTop + i1));
        drawables.mShowing[1].draw(paramCanvas);
        paramCanvas.restore();
      } 
      if (drawables.mShowing[3] != null) {
        paramCanvas.save();
        paramCanvas.translate((n + i + (k - drawables.mDrawableWidthBottom) / 2), (i1 + i4 - i5 - this.mPaddingBottom - drawables.mDrawableSizeBottom));
        drawables.mShowing[3].draw(paramCanvas);
        paramCanvas.restore();
      } 
    } 
    int i7 = this.mCurTextColor;
    if (this.mLayout == null)
      assumeLayout(); 
    Layout layout = this.mLayout;
    if (this.mHint != null && this.mText.length() == 0) {
      if (this.mHintTextColor != null)
        i7 = this.mCurHintTextColor; 
      layout = this.mHintLayout;
    } 
    this.mTextPaint.setColor(i7);
    this.mTextPaint.drawableState = getDrawableState();
    paramCanvas.save();
    k = getExtendedPaddingTop();
    i7 = getExtendedPaddingBottom();
    int i9 = this.mBottom;
    i6 = this.mTop;
    int i8 = this.mLayout.getHeight();
    float f1 = (i + n);
    if (i1 == 0) {
      f2 = 0.0F;
    } else {
      f2 = (k + i1);
    } 
    float f3 = (i2 - i3 - getCompoundPaddingRight() + n);
    if (i1 == i8 - i9 - i6 - m - j)
      i7 = 0; 
    float f4 = (i4 - i5 + i1 - i7);
    float f5 = this.mShadowRadius;
    if (f5 != 0.0F) {
      f5 = Math.min(0.0F, this.mShadowDx - f5);
      float f6 = Math.max(0.0F, this.mShadowDx + this.mShadowRadius);
      float f7 = Math.min(0.0F, this.mShadowDy - this.mShadowRadius);
      f4 += Math.max(0.0F, this.mShadowDy + this.mShadowRadius);
      f3 += f6;
      f2 += f7;
      f1 += f5;
    } 
    paramCanvas.clipRect(f1, f2, f3, f4);
    i7 = this.mGravity;
    i6 = 0;
    if ((i7 & 0x70) != 48) {
      i6 = getVerticalOffset(false);
      i7 = getVerticalOffset(true);
    } else {
      i7 = 0;
    } 
    paramCanvas.translate(i, (k + i6));
    i = getLayoutDirection();
    i = Gravity.getAbsoluteGravity(this.mGravity, i);
    if (isMarqueeFadeEnabled()) {
      if (!this.mSingleLine && getLineCount() == 1 && canMarquee() && (i & 0x7) != 3) {
        i4 = this.mRight;
        i1 = this.mLeft;
        i5 = getCompoundPaddingLeft();
        i = getCompoundPaddingRight();
        f3 = this.mLayout.getLineRight(0);
        f2 = (i4 - i1 - i5 + i);
        paramCanvas.translate(layout.getParagraphDirection(0) * (f3 - f2), 0.0F);
      } 
      Marquee marquee1 = this.mMarquee;
      if (marquee1 != null && marquee1.isRunning()) {
        f2 = -this.mMarquee.getScroll();
        paramCanvas.translate(layout.getParagraphDirection(0) * f2, 0.0F);
      } 
    } 
    i7 -= i6;
    Path path = getUpdatedHighlightPath();
    Editor editor = this.mEditor;
    if (editor != null) {
      editor.onDraw(paramCanvas, layout, path, this.mHighlightPaint, i7);
    } else {
      layout.draw(paramCanvas, path, this.mHighlightPaint, i7);
    } 
    Marquee marquee = this.mMarquee;
    if (marquee != null && marquee.shouldDrawGhost()) {
      f2 = this.mMarquee.getGhostOffset();
      paramCanvas.translate(layout.getParagraphDirection(0) * f2, 0.0F);
      layout.draw(paramCanvas, path, this.mHighlightPaint, i7);
    } 
    paramCanvas.restore();
  }
  
  public void getFocusedRect(Rect paramRect) {
    if (this.mLayout == null) {
      super.getFocusedRect(paramRect);
      return;
    } 
    int i = getSelectionEnd();
    if (i < 0) {
      super.getFocusedRect(paramRect);
      return;
    } 
    int j = getSelectionStart();
    if (j < 0 || j >= i) {
      int n = this.mLayout.getLineForOffset(i);
      paramRect.top = this.mLayout.getLineTop(n);
      paramRect.bottom = this.mLayout.getLineBottom(n);
      paramRect.left = (int)this.mLayout.getPrimaryHorizontal(i) - 2;
      paramRect.right = paramRect.left + 4;
    } else {
      int n = this.mLayout.getLineForOffset(j);
      int i1 = this.mLayout.getLineForOffset(i);
      paramRect.top = this.mLayout.getLineTop(n);
      paramRect.bottom = this.mLayout.getLineBottom(i1);
      if (n == i1) {
        paramRect.left = (int)this.mLayout.getPrimaryHorizontal(j);
        paramRect.right = (int)this.mLayout.getPrimaryHorizontal(i);
      } else {
        if (this.mHighlightPathBogus) {
          if (this.mHighlightPath == null)
            this.mHighlightPath = new Path(); 
          this.mHighlightPath.reset();
          this.mLayout.getSelectionPath(j, i, this.mHighlightPath);
          this.mHighlightPathBogus = false;
        } 
        synchronized (TEMP_RECTF) {
          this.mHighlightPath.computeBounds(TEMP_RECTF, true);
          paramRect.left = (int)TEMP_RECTF.left - 1;
          paramRect.right = (int)TEMP_RECTF.right + 1;
        } 
      } 
    } 
    int m = getCompoundPaddingLeft();
    int k = getExtendedPaddingTop();
    i = k;
    if ((this.mGravity & 0x70) != 48)
      i = k + getVerticalOffset(false); 
    paramRect.offset(m, i);
    i = getExtendedPaddingBottom();
    paramRect.bottom += i;
  }
  
  public int getLineCount() {
    boolean bool;
    Layout layout = this.mLayout;
    if (layout != null) {
      bool = layout.getLineCount();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getLineBounds(int paramInt, Rect paramRect) {
    Layout layout = this.mLayout;
    if (layout == null) {
      if (paramRect != null)
        paramRect.set(0, 0, 0, 0); 
      return 0;
    } 
    int i = layout.getLineBounds(paramInt, paramRect);
    int j = getExtendedPaddingTop();
    paramInt = j;
    if ((this.mGravity & 0x70) != 48)
      paramInt = j + getVerticalOffset(true); 
    if (paramRect != null)
      paramRect.offset(getCompoundPaddingLeft(), paramInt); 
    return i + paramInt;
  }
  
  public int getBaseline() {
    if (this.mLayout == null)
      return super.getBaseline(); 
    return getBaselineOffset() + this.mLayout.getLineBaseline(0);
  }
  
  int getBaselineOffset() {
    int i = 0;
    if ((this.mGravity & 0x70) != 48)
      i = getVerticalOffset(true); 
    int j = i;
    if (isLayoutModeOptical(this.mParent))
      j = i - (getOpticalInsets()).top; 
    return getExtendedPaddingTop() + j;
  }
  
  protected int getFadeTop(boolean paramBoolean) {
    if (this.mLayout == null)
      return 0; 
    int i = 0;
    if ((this.mGravity & 0x70) != 48)
      i = getVerticalOffset(true); 
    int j = i;
    if (paramBoolean)
      j = i + getTopPaddingOffset(); 
    return getExtendedPaddingTop() + j;
  }
  
  protected int getFadeHeight(boolean paramBoolean) {
    boolean bool;
    Layout layout = this.mLayout;
    if (layout != null) {
      bool = layout.getHeight();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    if (this.mSpannable != null && this.mLinksClickable) {
      float f1 = paramMotionEvent.getX(paramInt);
      float f2 = paramMotionEvent.getY(paramInt);
      int i = getOffsetForPosition(f1, f2);
      ClickableSpan[] arrayOfClickableSpan = this.mSpannable.<ClickableSpan>getSpans(i, i, ClickableSpan.class);
      if (arrayOfClickableSpan.length > 0)
        return PointerIcon.getSystemIcon(this.mContext, 1002); 
    } 
    if (isTextSelectable() || isTextEditable())
      return PointerIcon.getSystemIcon(this.mContext, 1008); 
    return super.onResolvePointerIcon(paramMotionEvent, paramInt);
  }
  
  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent) {
    if (paramInt == 4 && handleBackInTextActionModeIfNeeded(paramKeyEvent))
      return true; 
    return super.onKeyPreIme(paramInt, paramKeyEvent);
  }
  
  public boolean handleBackInTextActionModeIfNeeded(KeyEvent paramKeyEvent) {
    Editor editor = this.mEditor;
    if (editor == null || editor.getTextActionMode() == null)
      return false; 
    if (paramKeyEvent.getAction() == 0 && paramKeyEvent.getRepeatCount() == 0) {
      KeyEvent.DispatcherState dispatcherState = getKeyDispatcherState();
      if (dispatcherState != null)
        dispatcherState.startTracking(paramKeyEvent, this); 
      return true;
    } 
    if (paramKeyEvent.getAction() == 1) {
      KeyEvent.DispatcherState dispatcherState = getKeyDispatcherState();
      if (dispatcherState != null)
        dispatcherState.handleUpEvent(paramKeyEvent); 
      if (paramKeyEvent.isTracking() && !paramKeyEvent.isCanceled()) {
        stopTextActionMode();
        return true;
      } 
    } 
    return false;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    int i = doKeyDown(paramInt, paramKeyEvent, (KeyEvent)null);
    if (i == 0)
      return super.onKeyDown(paramInt, paramKeyEvent); 
    return true;
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    KeyEvent keyEvent = KeyEvent.changeAction(paramKeyEvent, 0);
    int i = doKeyDown(paramInt1, keyEvent, paramKeyEvent);
    if (i == 0)
      return super.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent); 
    if (i == -1)
      return true; 
    paramInt2--;
    paramKeyEvent = KeyEvent.changeAction(paramKeyEvent, 1);
    if (i == 1) {
      this.mEditor.mKeyListener.onKeyUp(this, (Editable)this.mText, paramInt1, paramKeyEvent);
      while (--paramInt2 > 0) {
        this.mEditor.mKeyListener.onKeyDown(this, (Editable)this.mText, paramInt1, keyEvent);
        this.mEditor.mKeyListener.onKeyUp(this, (Editable)this.mText, paramInt1, paramKeyEvent);
      } 
      hideErrorIfUnchanged();
    } else if (i == 2) {
      this.mMovement.onKeyUp(this, this.mSpannable, paramInt1, paramKeyEvent);
      while (--paramInt2 > 0) {
        this.mMovement.onKeyDown(this, this.mSpannable, paramInt1, keyEvent);
        this.mMovement.onKeyUp(this, this.mSpannable, paramInt1, paramKeyEvent);
      } 
    } 
    return true;
  }
  
  private boolean shouldAdvanceFocusOnEnter() {
    if (getKeyListener() == null)
      return false; 
    if (this.mSingleLine)
      return true; 
    Editor editor = this.mEditor;
    if (editor != null && (editor.mInputType & 0xF) == 1) {
      int i = this.mEditor.mInputType & 0xFF0;
      if (i == 32 || i == 48)
        return true; 
    } 
    return false;
  }
  
  private boolean isDirectionalNavigationKey(int paramInt) {
    switch (paramInt) {
      default:
        return false;
      case 19:
      case 20:
      case 21:
      case 22:
        break;
    } 
    return true;
  }
  
  private int doKeyDown(int paramInt, KeyEvent paramKeyEvent1, KeyEvent paramKeyEvent2) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isEnabled : ()Z
    //   4: istore #4
    //   6: iconst_0
    //   7: istore #5
    //   9: iload #4
    //   11: ifne -> 16
    //   14: iconst_0
    //   15: ireturn
    //   16: aload_2
    //   17: invokevirtual getRepeatCount : ()I
    //   20: ifne -> 35
    //   23: iload_1
    //   24: invokestatic isModifierKey : (I)Z
    //   27: ifne -> 35
    //   30: aload_0
    //   31: iconst_0
    //   32: putfield mPreventDefaultMovement : Z
    //   35: iload_1
    //   36: iconst_4
    //   37: if_icmpeq -> 400
    //   40: iload_1
    //   41: bipush #23
    //   43: if_icmpeq -> 384
    //   46: iload_1
    //   47: bipush #61
    //   49: if_icmpeq -> 367
    //   52: iload_1
    //   53: bipush #66
    //   55: if_icmpeq -> 262
    //   58: iload_1
    //   59: bipush #112
    //   61: if_icmpeq -> 236
    //   64: iload_1
    //   65: bipush #124
    //   67: if_icmpeq -> 182
    //   70: iload_1
    //   71: sipush #160
    //   74: if_icmpeq -> 262
    //   77: iload_1
    //   78: tableswitch default -> 104, 277 -> 157, 278 -> 132, 279 -> 107
    //   104: goto -> 425
    //   107: aload_2
    //   108: invokevirtual hasNoModifiers : ()Z
    //   111: ifeq -> 425
    //   114: aload_0
    //   115: invokevirtual canPaste : ()Z
    //   118: ifeq -> 425
    //   121: aload_0
    //   122: ldc 16908322
    //   124: invokevirtual onTextContextMenuItem : (I)Z
    //   127: ifeq -> 425
    //   130: iconst_m1
    //   131: ireturn
    //   132: aload_2
    //   133: invokevirtual hasNoModifiers : ()Z
    //   136: ifeq -> 425
    //   139: aload_0
    //   140: invokevirtual canCopy : ()Z
    //   143: ifeq -> 425
    //   146: aload_0
    //   147: ldc 16908321
    //   149: invokevirtual onTextContextMenuItem : (I)Z
    //   152: ifeq -> 425
    //   155: iconst_m1
    //   156: ireturn
    //   157: aload_2
    //   158: invokevirtual hasNoModifiers : ()Z
    //   161: ifeq -> 425
    //   164: aload_0
    //   165: invokevirtual canCut : ()Z
    //   168: ifeq -> 425
    //   171: aload_0
    //   172: ldc 16908320
    //   174: invokevirtual onTextContextMenuItem : (I)Z
    //   177: ifeq -> 425
    //   180: iconst_m1
    //   181: ireturn
    //   182: aload_2
    //   183: sipush #4096
    //   186: invokevirtual hasModifiers : (I)Z
    //   189: ifeq -> 210
    //   192: aload_0
    //   193: invokevirtual canCopy : ()Z
    //   196: ifeq -> 210
    //   199: aload_0
    //   200: ldc 16908321
    //   202: invokevirtual onTextContextMenuItem : (I)Z
    //   205: ifeq -> 425
    //   208: iconst_m1
    //   209: ireturn
    //   210: aload_2
    //   211: iconst_1
    //   212: invokevirtual hasModifiers : (I)Z
    //   215: ifeq -> 425
    //   218: aload_0
    //   219: invokevirtual canPaste : ()Z
    //   222: ifeq -> 425
    //   225: aload_0
    //   226: ldc 16908322
    //   228: invokevirtual onTextContextMenuItem : (I)Z
    //   231: ifeq -> 425
    //   234: iconst_m1
    //   235: ireturn
    //   236: aload_2
    //   237: iconst_1
    //   238: invokevirtual hasModifiers : (I)Z
    //   241: ifeq -> 425
    //   244: aload_0
    //   245: invokevirtual canCut : ()Z
    //   248: ifeq -> 425
    //   251: aload_0
    //   252: ldc 16908320
    //   254: invokevirtual onTextContextMenuItem : (I)Z
    //   257: ifeq -> 425
    //   260: iconst_m1
    //   261: ireturn
    //   262: aload_2
    //   263: invokevirtual hasNoModifiers : ()Z
    //   266: ifeq -> 425
    //   269: aload_0
    //   270: getfield mEditor : Landroid/widget/Editor;
    //   273: astore #6
    //   275: aload #6
    //   277: ifnull -> 339
    //   280: aload #6
    //   282: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   285: ifnull -> 339
    //   288: aload_0
    //   289: getfield mEditor : Landroid/widget/Editor;
    //   292: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   295: getfield onEditorActionListener : Landroid/widget/TextView$OnEditorActionListener;
    //   298: ifnull -> 339
    //   301: aload_0
    //   302: getfield mEditor : Landroid/widget/Editor;
    //   305: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   308: getfield onEditorActionListener : Landroid/widget/TextView$OnEditorActionListener;
    //   311: astore #6
    //   313: aload #6
    //   315: aload_0
    //   316: iconst_0
    //   317: aload_2
    //   318: invokeinterface onEditorAction : (Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    //   323: ifeq -> 339
    //   326: aload_0
    //   327: getfield mEditor : Landroid/widget/Editor;
    //   330: getfield mInputContentType : Landroid/widget/Editor$InputContentType;
    //   333: iconst_1
    //   334: putfield enterDown : Z
    //   337: iconst_m1
    //   338: ireturn
    //   339: aload_2
    //   340: invokevirtual getFlags : ()I
    //   343: bipush #16
    //   345: iand
    //   346: ifne -> 356
    //   349: aload_0
    //   350: invokespecial shouldAdvanceFocusOnEnter : ()Z
    //   353: ifeq -> 425
    //   356: aload_0
    //   357: invokevirtual hasOnClickListeners : ()Z
    //   360: ifeq -> 365
    //   363: iconst_0
    //   364: ireturn
    //   365: iconst_m1
    //   366: ireturn
    //   367: aload_2
    //   368: invokevirtual hasNoModifiers : ()Z
    //   371: ifne -> 382
    //   374: aload_2
    //   375: iconst_1
    //   376: invokevirtual hasModifiers : (I)Z
    //   379: ifeq -> 425
    //   382: iconst_0
    //   383: ireturn
    //   384: aload_2
    //   385: invokevirtual hasNoModifiers : ()Z
    //   388: ifeq -> 425
    //   391: aload_0
    //   392: invokespecial shouldAdvanceFocusOnEnter : ()Z
    //   395: ifeq -> 425
    //   398: iconst_0
    //   399: ireturn
    //   400: aload_0
    //   401: getfield mEditor : Landroid/widget/Editor;
    //   404: astore #6
    //   406: aload #6
    //   408: ifnull -> 425
    //   411: aload #6
    //   413: invokevirtual getTextActionMode : ()Landroid/view/ActionMode;
    //   416: ifnull -> 425
    //   419: aload_0
    //   420: invokevirtual stopTextActionMode : ()V
    //   423: iconst_m1
    //   424: ireturn
    //   425: aload_0
    //   426: getfield mEditor : Landroid/widget/Editor;
    //   429: astore #6
    //   431: aload #6
    //   433: ifnull -> 570
    //   436: aload #6
    //   438: getfield mKeyListener : Landroid/text/method/KeyListener;
    //   441: ifnull -> 570
    //   444: iconst_1
    //   445: istore #7
    //   447: iconst_1
    //   448: istore #8
    //   450: aload_3
    //   451: ifnull -> 522
    //   454: aload_0
    //   455: invokevirtual beginBatchEdit : ()V
    //   458: aload_0
    //   459: getfield mEditor : Landroid/widget/Editor;
    //   462: getfield mKeyListener : Landroid/text/method/KeyListener;
    //   465: aload_0
    //   466: aload_0
    //   467: getfield mText : Ljava/lang/CharSequence;
    //   470: checkcast android/text/Editable
    //   473: aload_3
    //   474: invokeinterface onKeyOther : (Landroid/view/View;Landroid/text/Editable;Landroid/view/KeyEvent;)Z
    //   479: istore #4
    //   481: aload_0
    //   482: invokevirtual hideErrorIfUnchanged : ()V
    //   485: iconst_0
    //   486: istore #7
    //   488: iload #4
    //   490: ifeq -> 499
    //   493: aload_0
    //   494: invokevirtual endBatchEdit : ()V
    //   497: iconst_m1
    //   498: ireturn
    //   499: aload_0
    //   500: invokevirtual endBatchEdit : ()V
    //   503: goto -> 522
    //   506: astore_2
    //   507: aload_0
    //   508: invokevirtual endBatchEdit : ()V
    //   511: aload_2
    //   512: athrow
    //   513: astore #6
    //   515: iload #8
    //   517: istore #7
    //   519: goto -> 499
    //   522: iload #7
    //   524: ifeq -> 570
    //   527: aload_0
    //   528: invokevirtual beginBatchEdit : ()V
    //   531: aload_0
    //   532: getfield mEditor : Landroid/widget/Editor;
    //   535: getfield mKeyListener : Landroid/text/method/KeyListener;
    //   538: aload_0
    //   539: aload_0
    //   540: getfield mText : Ljava/lang/CharSequence;
    //   543: checkcast android/text/Editable
    //   546: iload_1
    //   547: aload_2
    //   548: invokeinterface onKeyDown : (Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    //   553: istore #4
    //   555: aload_0
    //   556: invokevirtual endBatchEdit : ()V
    //   559: aload_0
    //   560: invokevirtual hideErrorIfUnchanged : ()V
    //   563: iload #4
    //   565: ifeq -> 570
    //   568: iconst_1
    //   569: ireturn
    //   570: aload_0
    //   571: getfield mMovement : Landroid/text/method/MovementMethod;
    //   574: astore #6
    //   576: aload #6
    //   578: ifnull -> 697
    //   581: aload_0
    //   582: getfield mLayout : Landroid/text/Layout;
    //   585: ifnull -> 697
    //   588: iconst_1
    //   589: istore #8
    //   591: iload #8
    //   593: istore #7
    //   595: aload_3
    //   596: ifnull -> 632
    //   599: aload #6
    //   601: aload_0
    //   602: aload_0
    //   603: getfield mSpannable : Landroid/text/Spannable;
    //   606: aload_3
    //   607: invokeinterface onKeyOther : (Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/KeyEvent;)Z
    //   612: istore #4
    //   614: iconst_0
    //   615: istore #7
    //   617: iload #4
    //   619: ifeq -> 624
    //   622: iconst_m1
    //   623: ireturn
    //   624: goto -> 632
    //   627: astore_3
    //   628: iload #8
    //   630: istore #7
    //   632: iload #7
    //   634: ifeq -> 677
    //   637: aload_0
    //   638: getfield mMovement : Landroid/text/method/MovementMethod;
    //   641: aload_0
    //   642: aload_0
    //   643: getfield mSpannable : Landroid/text/Spannable;
    //   646: iload_1
    //   647: aload_2
    //   648: invokeinterface onKeyDown : (Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z
    //   653: ifeq -> 677
    //   656: aload_2
    //   657: invokevirtual getRepeatCount : ()I
    //   660: ifne -> 675
    //   663: iload_1
    //   664: invokestatic isModifierKey : (I)Z
    //   667: ifne -> 675
    //   670: aload_0
    //   671: iconst_1
    //   672: putfield mPreventDefaultMovement : Z
    //   675: iconst_2
    //   676: ireturn
    //   677: aload_2
    //   678: invokevirtual getSource : ()I
    //   681: sipush #257
    //   684: if_icmpne -> 697
    //   687: aload_0
    //   688: iload_1
    //   689: invokespecial isDirectionalNavigationKey : (I)Z
    //   692: ifeq -> 697
    //   695: iconst_m1
    //   696: ireturn
    //   697: aload_0
    //   698: getfield mPreventDefaultMovement : Z
    //   701: ifeq -> 716
    //   704: iload_1
    //   705: invokestatic isModifierKey : (I)Z
    //   708: ifne -> 716
    //   711: iconst_m1
    //   712: istore_1
    //   713: goto -> 719
    //   716: iload #5
    //   718: istore_1
    //   719: iload_1
    //   720: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #8411	-> 0
    //   #8412	-> 14
    //   #8420	-> 16
    //   #8421	-> 30
    //   #8424	-> 35
    //   #8497	-> 107
    //   #8498	-> 121
    //   #8499	-> 130
    //   #8489	-> 132
    //   #8490	-> 146
    //   #8491	-> 155
    //   #8481	-> 157
    //   #8482	-> 171
    //   #8483	-> 180
    //   #8513	-> 182
    //   #8514	-> 199
    //   #8515	-> 208
    //   #8517	-> 210
    //   #8518	-> 225
    //   #8519	-> 234
    //   #8505	-> 236
    //   #8506	-> 251
    //   #8507	-> 260
    //   #8427	-> 262
    //   #8432	-> 269
    //   #8435	-> 288
    //   #8436	-> 313
    //   #8438	-> 326
    //   #8440	-> 337
    //   #8447	-> 339
    //   #8448	-> 349
    //   #8449	-> 356
    //   #8450	-> 363
    //   #8452	-> 365
    //   #8466	-> 367
    //   #8468	-> 382
    //   #8458	-> 384
    //   #8459	-> 391
    //   #8460	-> 398
    //   #8474	-> 400
    //   #8475	-> 419
    //   #8476	-> 423
    //   #8525	-> 425
    //   #8526	-> 444
    //   #8527	-> 450
    //   #8529	-> 454
    //   #8530	-> 458
    //   #8532	-> 481
    //   #8533	-> 485
    //   #8534	-> 488
    //   #8535	-> 493
    //   #8541	-> 493
    //   #8535	-> 497
    //   #8541	-> 499
    //   #8542	-> 503
    //   #8541	-> 506
    //   #8542	-> 511
    //   #8537	-> 513
    //   #8545	-> 522
    //   #8546	-> 527
    //   #8547	-> 531
    //   #8549	-> 555
    //   #8550	-> 559
    //   #8551	-> 563
    //   #8558	-> 570
    //   #8559	-> 588
    //   #8560	-> 591
    //   #8562	-> 599
    //   #8563	-> 614
    //   #8564	-> 617
    //   #8565	-> 622
    //   #8570	-> 624
    //   #8567	-> 627
    //   #8572	-> 632
    //   #8573	-> 637
    //   #8574	-> 656
    //   #8575	-> 670
    //   #8577	-> 675
    //   #8583	-> 677
    //   #8584	-> 687
    //   #8585	-> 695
    //   #8589	-> 697
    //   #8590	-> 711
    //   #8589	-> 719
    // Exception table:
    //   from	to	target	type
    //   454	458	513	java/lang/AbstractMethodError
    //   454	458	506	finally
    //   458	481	513	java/lang/AbstractMethodError
    //   458	481	506	finally
    //   481	485	513	java/lang/AbstractMethodError
    //   481	485	506	finally
    //   599	614	627	java/lang/AbstractMethodError
  }
  
  public void resetErrorChangedFlag() {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.mErrorWasChanged = false; 
  }
  
  public void hideErrorIfUnchanged() {
    Editor editor = this.mEditor;
    if (editor != null && editor.mError != null && !this.mEditor.mErrorWasChanged)
      setError((CharSequence)null, (Drawable)null); 
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    if (!isEnabled())
      return super.onKeyUp(paramInt, paramKeyEvent); 
    if (!KeyEvent.isModifierKey(paramInt))
      this.mPreventDefaultMovement = false; 
    if (paramInt != 23) {
      if (paramInt == 66 || paramInt == 160)
        if (paramKeyEvent.hasNoModifiers()) {
          Editor editor1 = this.mEditor;
          if (editor1 != null && editor1.mInputContentType != null && this.mEditor.mInputContentType.onEditorActionListener != null && this.mEditor.mInputContentType.enterDown) {
            this.mEditor.mInputContentType.enterDown = false;
            if (this.mEditor.mInputContentType.onEditorActionListener.onEditorAction(this, 0, paramKeyEvent))
              return true; 
          } 
          if ((paramKeyEvent.getFlags() & 0x10) != 0 || shouldAdvanceFocusOnEnter())
            if (!hasOnClickListeners()) {
              View view = focusSearch(130);
              if (view != null) {
                if (view.requestFocus(130)) {
                  super.onKeyUp(paramInt, paramKeyEvent);
                  return true;
                } 
                throw new IllegalStateException("focus search returned a view that wasn't able to take focus!");
              } 
              if ((paramKeyEvent.getFlags() & 0x10) != 0) {
                InputMethodManager inputMethodManager = getInputMethodManager();
                if (inputMethodManager != null && inputMethodManager.isActive(this))
                  inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0); 
              } 
            }  
          return super.onKeyUp(paramInt, paramKeyEvent);
        }  
      Editor editor = this.mEditor;
      if (editor != null && editor.mKeyListener != null && this.mEditor.mKeyListener.onKeyUp(this, (Editable)this.mText, paramInt, paramKeyEvent))
        return true; 
      MovementMethod movementMethod = this.mMovement;
      if (movementMethod != null && this.mLayout != null && movementMethod.onKeyUp(this, this.mSpannable, paramInt, paramKeyEvent))
        return true; 
      return super.onKeyUp(paramInt, paramKeyEvent);
    } 
    if (paramKeyEvent.hasNoModifiers())
      if (!hasOnClickListeners() && this.mMovement != null && this.mText instanceof Editable && this.mLayout != null && onCheckIsTextEditor()) {
        InputMethodManager inputMethodManager = getInputMethodManager();
        viewClicked(inputMethodManager);
        if (inputMethodManager != null && getShowSoftInputOnFocus())
          inputMethodManager.showSoftInput(this, 0); 
      }  
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  public boolean onCheckIsTextEditor() {
    boolean bool;
    Editor editor = this.mEditor;
    if (editor != null && editor.mInputType != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo) {
    if (onCheckIsTextEditor() && isEnabled()) {
      this.mEditor.createInputMethodStateIfNeeded();
      paramEditorInfo.inputType = getInputType();
      if (this.mEditor.mInputContentType != null) {
        paramEditorInfo.imeOptions = this.mEditor.mInputContentType.imeOptions;
        paramEditorInfo.privateImeOptions = this.mEditor.mInputContentType.privateImeOptions;
        paramEditorInfo.actionLabel = this.mEditor.mInputContentType.imeActionLabel;
        paramEditorInfo.actionId = this.mEditor.mInputContentType.imeActionId;
        paramEditorInfo.extras = this.mEditor.mInputContentType.extras;
        paramEditorInfo.hintLocales = this.mEditor.mInputContentType.imeHintLocales;
      } else {
        paramEditorInfo.imeOptions = 0;
        paramEditorInfo.hintLocales = null;
      } 
      if (focusSearch(130) != null)
        paramEditorInfo.imeOptions |= 0x8000000; 
      if (focusSearch(33) != null)
        paramEditorInfo.imeOptions |= 0x4000000; 
      if ((paramEditorInfo.imeOptions & 0xFF) == 0) {
        if ((paramEditorInfo.imeOptions & 0x8000000) != 0) {
          paramEditorInfo.imeOptions |= 0x5;
        } else {
          paramEditorInfo.imeOptions |= 0x6;
        } 
        if (!shouldAdvanceFocusOnEnter())
          paramEditorInfo.imeOptions |= 0x40000000; 
      } 
      if (isMultilineInputType(paramEditorInfo.inputType))
        paramEditorInfo.imeOptions |= 0x40000000; 
      paramEditorInfo.hintText = this.mHint;
      paramEditorInfo.targetInputMethodUser = this.mTextOperationUser;
      if (this.mText instanceof Editable) {
        EditableInputConnection editableInputConnection = new EditableInputConnection(this);
        paramEditorInfo.initialSelStart = getSelectionStart();
        paramEditorInfo.initialSelEnd = getSelectionEnd();
        paramEditorInfo.initialCapsMode = editableInputConnection.getCursorCapsMode(getInputType());
        paramEditorInfo.setInitialSurroundingText(this.mText);
        return (InputConnection)editableInputConnection;
      } 
    } 
    return null;
  }
  
  public boolean extractText(ExtractedTextRequest paramExtractedTextRequest, ExtractedText paramExtractedText) {
    createEditorIfNeeded();
    return this.mEditor.extractText(paramExtractedTextRequest, paramExtractedText);
  }
  
  static void removeParcelableSpans(Spannable paramSpannable, int paramInt1, int paramInt2) {
    Object[] arrayOfObject = paramSpannable.getSpans(paramInt1, paramInt2, (Class)ParcelableSpan.class);
    paramInt1 = arrayOfObject.length;
    while (paramInt1 > 0) {
      paramInt1--;
      paramSpannable.removeSpan(arrayOfObject[paramInt1]);
    } 
  }
  
  public void setExtractedText(ExtractedText paramExtractedText) {
    int i;
    Editable editable = getEditableText();
    if (paramExtractedText.text != null)
      if (editable == null) {
        setText(paramExtractedText.text, BufferType.EDITABLE);
      } else {
        byte b;
        i = editable.length();
        if (paramExtractedText.partialStartOffset >= 0) {
          b = editable.length();
          int n = paramExtractedText.partialStartOffset;
          i = n;
          if (n > b)
            i = b; 
          int i1 = paramExtractedText.partialEndOffset;
          n = i1;
          if (i1 > b)
            n = b; 
          b = i;
          i = n;
        } else {
          b = 0;
        } 
        removeParcelableSpans(editable, b, i);
        if (TextUtils.equals(editable.subSequence(b, i), paramExtractedText.text)) {
          if (paramExtractedText.text instanceof Spanned)
            TextUtils.copySpansFrom((Spanned)paramExtractedText.text, 0, i - b, Object.class, editable, b); 
        } else {
          editable.replace(b, i, paramExtractedText.text);
        } 
      }  
    Spannable spannable = (Spannable)getText();
    int j = spannable.length();
    int k = paramExtractedText.selectionStart;
    if (k < 0) {
      i = 0;
    } else {
      i = k;
      if (k > j)
        i = j; 
    } 
    int m = paramExtractedText.selectionEnd;
    if (m < 0) {
      k = 0;
    } else {
      k = m;
      if (m > j)
        k = j; 
    } 
    Selection.setSelection(spannable, i, k);
    if ((paramExtractedText.flags & 0x2) != 0) {
      MetaKeyKeyListener.startSelecting(this, spannable);
    } else {
      MetaKeyKeyListener.stopSelecting(this, spannable);
    } 
    setHintInternal(paramExtractedText.hint);
  }
  
  public void setExtracting(ExtractedTextRequest paramExtractedTextRequest) {
    if (this.mEditor.mInputMethodState != null)
      this.mEditor.mInputMethodState.mExtractedTextRequest = paramExtractedTextRequest; 
    this.mEditor.hideCursorAndSpanControllers();
    stopTextActionMode();
    if (this.mEditor.mSelectionModifierCursorController != null)
      this.mEditor.mSelectionModifierCursorController.resetTouchOffsets(); 
  }
  
  public void onCommitCompletion(CompletionInfo paramCompletionInfo) {}
  
  public void onCommitCorrection(CorrectionInfo paramCorrectionInfo) {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onCommitCorrection(paramCorrectionInfo); 
  }
  
  public void beginBatchEdit() {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.beginBatchEdit(); 
  }
  
  public void endBatchEdit() {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.endBatchEdit(); 
  }
  
  public void onBeginBatchEdit() {}
  
  public void onEndBatchEdit() {}
  
  public boolean onPrivateIMECommand(String paramString, Bundle paramBundle) {
    return false;
  }
  
  public void nullLayouts() {
    Layout layout = this.mLayout;
    if (layout instanceof BoringLayout && this.mSavedLayout == null)
      this.mSavedLayout = (BoringLayout)layout; 
    layout = this.mHintLayout;
    if (layout instanceof BoringLayout && this.mSavedHintLayout == null)
      this.mSavedHintLayout = (BoringLayout)layout; 
    this.mHintLayout = null;
    this.mLayout = null;
    this.mSavedMarqueeModeLayout = null;
    super.mLayout = null;
    this.mHintBoring = null;
    this.mBoring = null;
    Editor editor = this.mEditor;
    if (editor != null)
      editor.prepareCursorControllers(); 
  }
  
  private void assumeLayout() {
    int i = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
    int j = i;
    if (i < 1)
      j = 0; 
    i = j;
    if (this.mHorizontallyScrolling)
      i = 1048576; 
    BoringLayout.Metrics metrics = UNKNOWN_BORING;
    makeNewLayout(i, j, metrics, metrics, j, false);
  }
  
  private Layout.Alignment getLayoutAlignment() {
    Layout.Alignment alignment;
    switch (getTextAlignment()) {
      default:
        alignment = Layout.Alignment.ALIGN_NORMAL;
        return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).getLayoutAlignmentForTextView(alignment, this.mContext, this);
      case 6:
        if (getLayoutDirection() == 1) {
          alignment = Layout.Alignment.ALIGN_LEFT;
        } else {
          alignment = Layout.Alignment.ALIGN_RIGHT;
        } 
        return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).getLayoutAlignmentForTextView(alignment, this.mContext, this);
      case 5:
        if (getLayoutDirection() == 1) {
          alignment = Layout.Alignment.ALIGN_RIGHT;
        } else {
          alignment = Layout.Alignment.ALIGN_LEFT;
        } 
        return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).getLayoutAlignmentForTextView(alignment, this.mContext, this);
      case 4:
        alignment = Layout.Alignment.ALIGN_CENTER;
        return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).getLayoutAlignmentForTextView(alignment, this.mContext, this);
      case 3:
        alignment = Layout.Alignment.ALIGN_OPPOSITE;
        return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).getLayoutAlignmentForTextView(alignment, this.mContext, this);
      case 2:
        alignment = Layout.Alignment.ALIGN_NORMAL;
        return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).getLayoutAlignmentForTextView(alignment, this.mContext, this);
      case 1:
        break;
    } 
    int i = this.mGravity & 0x800007;
    if (i != 1) {
      if (i != 3) {
        if (i != 5) {
          if (i != 8388611) {
            if (i != 8388613) {
              alignment = Layout.Alignment.ALIGN_NORMAL;
            } else {
              alignment = Layout.Alignment.ALIGN_OPPOSITE;
            } 
          } else {
            alignment = Layout.Alignment.ALIGN_NORMAL;
          } 
        } else {
          alignment = Layout.Alignment.ALIGN_RIGHT;
        } 
      } else {
        alignment = Layout.Alignment.ALIGN_LEFT;
      } 
    } else {
      alignment = Layout.Alignment.ALIGN_CENTER;
    } 
    return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).getLayoutAlignmentForTextView(alignment, this.mContext, this);
  }
  
  public void makeNewLayout(int paramInt1, int paramInt2, BoringLayout.Metrics paramMetrics1, BoringLayout.Metrics paramMetrics2, int paramInt3, boolean paramBoolean) {
    boolean bool1;
    boolean bool;
    boolean bool2, bool3;
    stopMarquee();
    this.mOldMaximum = this.mMaximum;
    this.mOldMaxMode = this.mMaxMode;
    this.mHighlightPathBogus = true;
    if (paramInt1 < 0)
      paramInt1 = 0; 
    if (paramInt2 < 0)
      paramInt2 = 0; 
    Layout.Alignment alignment = getLayoutAlignment();
    if (this.mSingleLine && this.mLayout != null && (alignment == Layout.Alignment.ALIGN_NORMAL || alignment == Layout.Alignment.ALIGN_OPPOSITE)) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    int i = 0;
    if (bool1)
      i = this.mLayout.getParagraphDirection(0); 
    if (this.mEllipsize != null && getKeyListener() == null) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mEllipsize == TextUtils.TruncateAt.MARQUEE && this.mMarqueeFadeMode != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    TextUtils.TruncateAt truncateAt = this.mEllipsize;
    if (this.mEllipsize == TextUtils.TruncateAt.MARQUEE && this.mMarqueeFadeMode == 1)
      truncateAt = TextUtils.TruncateAt.END_SMALL; 
    if (this.mTextDir == null)
      this.mTextDir = getTextDirectionHeuristic(); 
    if (truncateAt == this.mEllipsize) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    Layout layout = makeSingleLayout(paramInt1, paramMetrics1, paramInt3, alignment, bool, truncateAt, bool3);
    super.mLayout = layout;
    if (bool2) {
      TextUtils.TruncateAt truncateAt1;
      if (truncateAt == TextUtils.TruncateAt.MARQUEE) {
        truncateAt1 = TextUtils.TruncateAt.END;
      } else {
        truncateAt1 = TextUtils.TruncateAt.MARQUEE;
      } 
      if (truncateAt != this.mEllipsize) {
        bool3 = true;
      } else {
        bool3 = false;
      } 
      this.mSavedMarqueeModeLayout = makeSingleLayout(paramInt1, paramMetrics1, paramInt3, alignment, bool, truncateAt1, bool3);
    } 
    if (this.mEllipsize != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mHintLayout = null;
    if (this.mHint != null) {
      if (bool2)
        paramInt2 = paramInt1; 
      if (paramMetrics2 == UNKNOWN_BORING) {
        paramMetrics1 = BoringLayout.isBoring(this.mHint, this.mTextPaint, this.mTextDir, this.mHintBoring);
        if (paramMetrics1 != null)
          this.mHintBoring = paramMetrics1; 
      } else {
        paramMetrics1 = paramMetrics2;
      } 
      if (paramMetrics1 != null)
        if (paramMetrics1.width <= paramInt2 && (!bool2 || paramMetrics1.width <= paramInt3)) {
          BoringLayout boringLayout = this.mSavedHintLayout;
          if (boringLayout != null) {
            this.mHintLayout = boringLayout.replaceOrMake(this.mHint, this.mTextPaint, paramInt2, alignment, this.mSpacingMult, this.mSpacingAdd, paramMetrics1, this.mIncludePad);
          } else {
            this.mHintLayout = BoringLayout.make(this.mHint, this.mTextPaint, paramInt2, alignment, this.mSpacingMult, this.mSpacingAdd, paramMetrics1, this.mIncludePad);
          } 
          this.mSavedHintLayout = (BoringLayout)this.mHintLayout;
        } else if (bool2 && paramMetrics1.width <= paramInt2) {
          BoringLayout boringLayout = this.mSavedHintLayout;
          if (boringLayout != null) {
            this.mHintLayout = boringLayout.replaceOrMake(this.mHint, this.mTextPaint, paramInt2, alignment, this.mSpacingMult, this.mSpacingAdd, paramMetrics1, this.mIncludePad, this.mEllipsize, paramInt3);
          } else {
            this.mHintLayout = BoringLayout.make(this.mHint, this.mTextPaint, paramInt2, alignment, this.mSpacingMult, this.mSpacingAdd, paramMetrics1, this.mIncludePad, this.mEllipsize, paramInt3);
          } 
        }  
      if (this.mHintLayout == null) {
        CharSequence charSequence = this.mHint;
        paramInt1 = charSequence.length();
        TextPaint textPaint = this.mTextPaint;
        StaticLayout.Builder builder2 = StaticLayout.Builder.obtain(charSequence, 0, paramInt1, textPaint, paramInt2);
        StaticLayout.Builder builder3 = builder2.setAlignment(alignment);
        TextDirectionHeuristic textDirectionHeuristic = this.mTextDir;
        StaticLayout.Builder builder1 = builder3.setTextDirection(textDirectionHeuristic);
        float f1 = this.mSpacingAdd, f2 = this.mSpacingMult;
        builder1 = builder1.setLineSpacing(f1, f2);
        bool = this.mIncludePad;
        builder1 = builder1.setIncludePad(bool);
        bool = this.mUseFallbackLineSpacing;
        builder1 = builder1.setUseLineSpacingFromFallbacks(bool);
        paramInt1 = this.mBreakStrategy;
        builder1 = builder1.setBreakStrategy(paramInt1);
        paramInt1 = this.mHyphenationFrequency;
        builder1 = builder1.setHyphenationFrequency(paramInt1);
        paramInt1 = this.mJustificationMode;
        builder1 = builder1.setJustificationMode(paramInt1);
        if (this.mMaxMode == 1) {
          paramInt1 = this.mMaximum;
        } else {
          paramInt1 = Integer.MAX_VALUE;
        } 
        builder1 = builder1.setMaxLines(paramInt1);
        f2 = this.mTextJustificationHooksImpl.getTextViewParaSpacing(this);
        builder1.mBuilderJustificationHooksImpl.setLayoutParaSpacingAdded(builder1, f2);
        if (bool2) {
          builder3 = builder1.setEllipsize(this.mEllipsize);
          builder3.setEllipsizedWidth(paramInt3);
        } 
        this.mHintLayout = builder1.build();
      } 
      bool = false;
    } 
    if (paramBoolean || (bool1 && i != this.mLayout.getParagraphDirection(0)))
      registerForPreDraw(); 
    if (this.mEllipsize == TextUtils.TruncateAt.MARQUEE && !compressText(paramInt3)) {
      paramInt1 = this.mLayoutParams.height;
      if (paramInt1 != -2 && paramInt1 != -1) {
        startMarquee();
      } else {
        this.mRestartMarquee = true;
      } 
    } 
    Editor editor = this.mEditor;
    if (editor != null)
      editor.prepareCursorControllers(); 
  }
  
  public boolean useDynamicLayout() {
    return (isTextSelectable() || (this.mSpannable != null && this.mPrecomputed == null));
  }
  
  protected Layout makeSingleLayout(int paramInt1, BoringLayout.Metrics paramMetrics, int paramInt2, Layout.Alignment paramAlignment, boolean paramBoolean1, TextUtils.TruncateAt paramTruncateAt, boolean paramBoolean2) {
    // Byte code:
    //   0: aconst_null
    //   1: astore #8
    //   3: aload_0
    //   4: invokevirtual useDynamicLayout : ()Z
    //   7: ifeq -> 179
    //   10: aload_0
    //   11: getfield mText : Ljava/lang/CharSequence;
    //   14: aload_0
    //   15: getfield mTextPaint : Landroid/text/TextPaint;
    //   18: iload_1
    //   19: invokestatic obtain : (Ljava/lang/CharSequence;Landroid/text/TextPaint;I)Landroid/text/DynamicLayout$Builder;
    //   22: astore #8
    //   24: aload_0
    //   25: getfield mTransformed : Ljava/lang/CharSequence;
    //   28: astore_2
    //   29: aload #8
    //   31: aload_2
    //   32: invokevirtual setDisplayText : (Ljava/lang/CharSequence;)Landroid/text/DynamicLayout$Builder;
    //   35: astore_2
    //   36: aload_2
    //   37: aload #4
    //   39: invokevirtual setAlignment : (Landroid/text/Layout$Alignment;)Landroid/text/DynamicLayout$Builder;
    //   42: astore_2
    //   43: aload_0
    //   44: getfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   47: astore #8
    //   49: aload_2
    //   50: aload #8
    //   52: invokevirtual setTextDirection : (Landroid/text/TextDirectionHeuristic;)Landroid/text/DynamicLayout$Builder;
    //   55: astore_2
    //   56: aload_0
    //   57: getfield mSpacingAdd : F
    //   60: fstore #9
    //   62: aload_0
    //   63: getfield mSpacingMult : F
    //   66: fstore #10
    //   68: aload_2
    //   69: fload #9
    //   71: fload #10
    //   73: invokevirtual setLineSpacing : (FF)Landroid/text/DynamicLayout$Builder;
    //   76: astore_2
    //   77: aload_0
    //   78: getfield mIncludePad : Z
    //   81: istore #7
    //   83: aload_2
    //   84: iload #7
    //   86: invokevirtual setIncludePad : (Z)Landroid/text/DynamicLayout$Builder;
    //   89: astore_2
    //   90: aload_0
    //   91: getfield mUseFallbackLineSpacing : Z
    //   94: istore #7
    //   96: aload_2
    //   97: iload #7
    //   99: invokevirtual setUseLineSpacingFromFallbacks : (Z)Landroid/text/DynamicLayout$Builder;
    //   102: astore_2
    //   103: aload_0
    //   104: getfield mBreakStrategy : I
    //   107: istore #11
    //   109: aload_2
    //   110: iload #11
    //   112: invokevirtual setBreakStrategy : (I)Landroid/text/DynamicLayout$Builder;
    //   115: astore_2
    //   116: aload_0
    //   117: getfield mHyphenationFrequency : I
    //   120: istore #11
    //   122: aload_2
    //   123: iload #11
    //   125: invokevirtual setHyphenationFrequency : (I)Landroid/text/DynamicLayout$Builder;
    //   128: astore_2
    //   129: aload_0
    //   130: getfield mJustificationMode : I
    //   133: istore #11
    //   135: aload_2
    //   136: iload #11
    //   138: invokevirtual setJustificationMode : (I)Landroid/text/DynamicLayout$Builder;
    //   141: astore #8
    //   143: aload_0
    //   144: invokevirtual getKeyListener : ()Landroid/text/method/KeyListener;
    //   147: ifnonnull -> 156
    //   150: aload #6
    //   152: astore_2
    //   153: goto -> 158
    //   156: aconst_null
    //   157: astore_2
    //   158: aload #8
    //   160: aload_2
    //   161: invokevirtual setEllipsize : (Landroid/text/TextUtils$TruncateAt;)Landroid/text/DynamicLayout$Builder;
    //   164: astore_2
    //   165: aload_2
    //   166: iload_3
    //   167: invokevirtual setEllipsizedWidth : (I)Landroid/text/DynamicLayout$Builder;
    //   170: astore_2
    //   171: aload_2
    //   172: invokevirtual build : ()Landroid/text/DynamicLayout;
    //   175: astore_2
    //   176: goto -> 444
    //   179: aload_2
    //   180: getstatic android/widget/TextView.UNKNOWN_BORING : Landroid/text/BoringLayout$Metrics;
    //   183: if_acmpne -> 218
    //   186: aload_0
    //   187: getfield mTransformed : Ljava/lang/CharSequence;
    //   190: aload_0
    //   191: getfield mTextPaint : Landroid/text/TextPaint;
    //   194: aload_0
    //   195: getfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   198: aload_0
    //   199: getfield mBoring : Landroid/text/BoringLayout$Metrics;
    //   202: invokestatic isBoring : (Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;
    //   205: astore_2
    //   206: aload_2
    //   207: ifnull -> 215
    //   210: aload_0
    //   211: aload_2
    //   212: putfield mBoring : Landroid/text/BoringLayout$Metrics;
    //   215: goto -> 218
    //   218: aload_2
    //   219: ifnull -> 441
    //   222: aload_2
    //   223: getfield width : I
    //   226: iload_1
    //   227: if_icmpgt -> 336
    //   230: aload #6
    //   232: ifnull -> 243
    //   235: aload_2
    //   236: getfield width : I
    //   239: iload_3
    //   240: if_icmpgt -> 336
    //   243: iload #7
    //   245: ifeq -> 292
    //   248: aload_0
    //   249: getfield mSavedLayout : Landroid/text/BoringLayout;
    //   252: astore #8
    //   254: aload #8
    //   256: ifnull -> 292
    //   259: aload #8
    //   261: aload_0
    //   262: getfield mTransformed : Ljava/lang/CharSequence;
    //   265: aload_0
    //   266: getfield mTextPaint : Landroid/text/TextPaint;
    //   269: iload_1
    //   270: aload #4
    //   272: aload_0
    //   273: getfield mSpacingMult : F
    //   276: aload_0
    //   277: getfield mSpacingAdd : F
    //   280: aload_2
    //   281: aload_0
    //   282: getfield mIncludePad : Z
    //   285: invokevirtual replaceOrMake : (Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;
    //   288: astore_2
    //   289: goto -> 320
    //   292: aload_0
    //   293: getfield mTransformed : Ljava/lang/CharSequence;
    //   296: aload_0
    //   297: getfield mTextPaint : Landroid/text/TextPaint;
    //   300: iload_1
    //   301: aload #4
    //   303: aload_0
    //   304: getfield mSpacingMult : F
    //   307: aload_0
    //   308: getfield mSpacingAdd : F
    //   311: aload_2
    //   312: aload_0
    //   313: getfield mIncludePad : Z
    //   316: invokestatic make : (Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;
    //   319: astore_2
    //   320: iload #7
    //   322: ifeq -> 333
    //   325: aload_0
    //   326: aload_2
    //   327: checkcast android/text/BoringLayout
    //   330: putfield mSavedLayout : Landroid/text/BoringLayout;
    //   333: goto -> 444
    //   336: iload #5
    //   338: ifeq -> 435
    //   341: aload_2
    //   342: getfield width : I
    //   345: iload_1
    //   346: if_icmpgt -> 435
    //   349: iload #7
    //   351: ifeq -> 401
    //   354: aload_0
    //   355: getfield mSavedLayout : Landroid/text/BoringLayout;
    //   358: astore #8
    //   360: aload #8
    //   362: ifnull -> 401
    //   365: aload #8
    //   367: aload_0
    //   368: getfield mTransformed : Ljava/lang/CharSequence;
    //   371: aload_0
    //   372: getfield mTextPaint : Landroid/text/TextPaint;
    //   375: iload_1
    //   376: aload #4
    //   378: aload_0
    //   379: getfield mSpacingMult : F
    //   382: aload_0
    //   383: getfield mSpacingAdd : F
    //   386: aload_2
    //   387: aload_0
    //   388: getfield mIncludePad : Z
    //   391: aload #6
    //   393: iload_3
    //   394: invokevirtual replaceOrMake : (Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;
    //   397: astore_2
    //   398: goto -> 444
    //   401: aload_0
    //   402: getfield mTransformed : Ljava/lang/CharSequence;
    //   405: aload_0
    //   406: getfield mTextPaint : Landroid/text/TextPaint;
    //   409: iload_1
    //   410: aload #4
    //   412: aload_0
    //   413: getfield mSpacingMult : F
    //   416: aload_0
    //   417: getfield mSpacingAdd : F
    //   420: aload_2
    //   421: aload_0
    //   422: getfield mIncludePad : Z
    //   425: aload #6
    //   427: iload_3
    //   428: invokestatic make : (Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;
    //   431: astore_2
    //   432: goto -> 444
    //   435: aload #8
    //   437: astore_2
    //   438: goto -> 444
    //   441: aload #8
    //   443: astore_2
    //   444: aload_2
    //   445: astore #8
    //   447: aload_2
    //   448: ifnonnull -> 658
    //   451: aload_0
    //   452: getfield mTransformed : Ljava/lang/CharSequence;
    //   455: astore #8
    //   457: aload #8
    //   459: invokeinterface length : ()I
    //   464: istore #11
    //   466: aload_0
    //   467: getfield mTextPaint : Landroid/text/TextPaint;
    //   470: astore_2
    //   471: aload #8
    //   473: iconst_0
    //   474: iload #11
    //   476: aload_2
    //   477: iload_1
    //   478: invokestatic obtain : (Ljava/lang/CharSequence;IILandroid/text/TextPaint;I)Landroid/text/StaticLayout$Builder;
    //   481: astore_2
    //   482: aload_2
    //   483: aload #4
    //   485: invokevirtual setAlignment : (Landroid/text/Layout$Alignment;)Landroid/text/StaticLayout$Builder;
    //   488: astore_2
    //   489: aload_0
    //   490: getfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   493: astore #4
    //   495: aload_2
    //   496: aload #4
    //   498: invokevirtual setTextDirection : (Landroid/text/TextDirectionHeuristic;)Landroid/text/StaticLayout$Builder;
    //   501: astore_2
    //   502: aload_0
    //   503: getfield mSpacingAdd : F
    //   506: fstore #10
    //   508: aload_0
    //   509: getfield mSpacingMult : F
    //   512: fstore #9
    //   514: aload_2
    //   515: fload #10
    //   517: fload #9
    //   519: invokevirtual setLineSpacing : (FF)Landroid/text/StaticLayout$Builder;
    //   522: astore_2
    //   523: aload_0
    //   524: getfield mIncludePad : Z
    //   527: istore #7
    //   529: aload_2
    //   530: iload #7
    //   532: invokevirtual setIncludePad : (Z)Landroid/text/StaticLayout$Builder;
    //   535: astore_2
    //   536: aload_0
    //   537: getfield mUseFallbackLineSpacing : Z
    //   540: istore #7
    //   542: aload_2
    //   543: iload #7
    //   545: invokevirtual setUseLineSpacingFromFallbacks : (Z)Landroid/text/StaticLayout$Builder;
    //   548: astore_2
    //   549: aload_0
    //   550: getfield mBreakStrategy : I
    //   553: istore_1
    //   554: aload_2
    //   555: iload_1
    //   556: invokevirtual setBreakStrategy : (I)Landroid/text/StaticLayout$Builder;
    //   559: astore_2
    //   560: aload_0
    //   561: getfield mHyphenationFrequency : I
    //   564: istore_1
    //   565: aload_2
    //   566: iload_1
    //   567: invokevirtual setHyphenationFrequency : (I)Landroid/text/StaticLayout$Builder;
    //   570: astore_2
    //   571: aload_0
    //   572: getfield mJustificationMode : I
    //   575: istore_1
    //   576: aload_2
    //   577: iload_1
    //   578: invokevirtual setJustificationMode : (I)Landroid/text/StaticLayout$Builder;
    //   581: astore_2
    //   582: aload_0
    //   583: getfield mMaxMode : I
    //   586: iconst_1
    //   587: if_icmpne -> 598
    //   590: aload_0
    //   591: getfield mMaximum : I
    //   594: istore_1
    //   595: goto -> 602
    //   598: ldc_w 2147483647
    //   601: istore_1
    //   602: aload_2
    //   603: iload_1
    //   604: invokevirtual setMaxLines : (I)Landroid/text/StaticLayout$Builder;
    //   607: astore_2
    //   608: aload_0
    //   609: getfield mTextJustificationHooksImpl : Landroid/text/ITextJustificationHooks;
    //   612: aload_0
    //   613: invokeinterface getTextViewParaSpacing : (Ljava/lang/Object;)F
    //   618: fstore #9
    //   620: aload_2
    //   621: getfield mBuilderJustificationHooksImpl : Landroid/text/ITextJustificationHooks;
    //   624: aload_2
    //   625: fload #9
    //   627: invokeinterface setLayoutParaSpacingAdded : (Ljava/lang/Object;F)V
    //   632: iload #5
    //   634: ifeq -> 652
    //   637: aload_2
    //   638: aload #6
    //   640: invokevirtual setEllipsize : (Landroid/text/TextUtils$TruncateAt;)Landroid/text/StaticLayout$Builder;
    //   643: astore #4
    //   645: aload #4
    //   647: iload_3
    //   648: invokevirtual setEllipsizedWidth : (I)Landroid/text/StaticLayout$Builder;
    //   651: pop
    //   652: aload_2
    //   653: invokevirtual build : ()Landroid/text/StaticLayout;
    //   656: astore #8
    //   658: aload #8
    //   660: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #9219	-> 0
    //   #9220	-> 3
    //   #9221	-> 10
    //   #9223	-> 29
    //   #9224	-> 36
    //   #9225	-> 49
    //   #9226	-> 68
    //   #9227	-> 83
    //   #9228	-> 96
    //   #9229	-> 109
    //   #9230	-> 122
    //   #9231	-> 135
    //   #9232	-> 143
    //   #9233	-> 165
    //   #9234	-> 171
    //   #9235	-> 176
    //   #9236	-> 179
    //   #9237	-> 186
    //   #9238	-> 206
    //   #9239	-> 210
    //   #9243	-> 215
    //   #9236	-> 218
    //   #9243	-> 218
    //   #9244	-> 222
    //   #9246	-> 243
    //   #9247	-> 259
    //   #9251	-> 292
    //   #9256	-> 320
    //   #9257	-> 325
    //   #9274	-> 333
    //   #9259	-> 336
    //   #9260	-> 349
    //   #9261	-> 365
    //   #9260	-> 401
    //   #9266	-> 401
    //   #9259	-> 435
    //   #9243	-> 441
    //   #9274	-> 444
    //   #9275	-> 451
    //   #9276	-> 457
    //   #9275	-> 471
    //   #9277	-> 482
    //   #9278	-> 495
    //   #9279	-> 514
    //   #9280	-> 529
    //   #9281	-> 542
    //   #9282	-> 554
    //   #9283	-> 565
    //   #9284	-> 576
    //   #9285	-> 582
    //   #9288	-> 608
    //   #9289	-> 620
    //   #9291	-> 632
    //   #9292	-> 637
    //   #9293	-> 645
    //   #9295	-> 652
    //   #9297	-> 658
  }
  
  private boolean compressText(float paramFloat) {
    if (isHardwareAccelerated())
      return false; 
    if (paramFloat > 0.0F && this.mLayout != null && getLineCount() == 1 && !this.mUserSetTextScaleX) {
      TextPaint textPaint = this.mTextPaint;
      if (textPaint.getTextScaleX() == 1.0F) {
        float f = this.mLayout.getLineWidth(0);
        paramFloat = (f + 1.0F - paramFloat) / paramFloat;
        if (paramFloat > 0.0F && paramFloat <= 0.07F) {
          this.mTextPaint.setTextScaleX(1.0F - paramFloat - 0.005F);
          post((Runnable)new Object(this));
          return true;
        } 
      } 
    } 
    return false;
  }
  
  private static int desired(Layout paramLayout) {
    int i = paramLayout.getLineCount();
    CharSequence charSequence = paramLayout.getText();
    float f = 0.0F;
    byte b;
    for (b = 0; b < i - 1; b++) {
      if (charSequence.charAt(paramLayout.getLineEnd(b) - 1) != '\n')
        return -1; 
    } 
    for (b = 0; b < i; b++)
      f = Math.max(f, paramLayout.getLineWidth(b)); 
    return (int)Math.ceil(f);
  }
  
  public void setIncludeFontPadding(boolean paramBoolean) {
    if (this.mIncludePad != paramBoolean) {
      this.mIncludePad = paramBoolean;
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public boolean getIncludeFontPadding() {
    return this.mIncludePad;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: iload_1
    //   1: invokestatic getMode : (I)I
    //   4: istore_3
    //   5: iload_2
    //   6: invokestatic getMode : (I)I
    //   9: istore #4
    //   11: iload_1
    //   12: invokestatic getSize : (I)I
    //   15: istore #5
    //   17: iload_2
    //   18: invokestatic getSize : (I)I
    //   21: istore #6
    //   23: getstatic android/widget/TextView.UNKNOWN_BORING : Landroid/text/BoringLayout$Metrics;
    //   26: astore #7
    //   28: getstatic android/widget/TextView.UNKNOWN_BORING : Landroid/text/BoringLayout$Metrics;
    //   31: astore #8
    //   33: aload_0
    //   34: getfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   37: ifnonnull -> 48
    //   40: aload_0
    //   41: aload_0
    //   42: invokevirtual getTextDirectionHeuristic : ()Landroid/text/TextDirectionHeuristic;
    //   45: putfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   48: iconst_m1
    //   49: istore_1
    //   50: iconst_0
    //   51: istore #9
    //   53: iload_3
    //   54: ldc_w -2147483648
    //   57: if_icmpne -> 68
    //   60: iload #5
    //   62: i2f
    //   63: fstore #10
    //   65: goto -> 73
    //   68: ldc_w 3.4028235E38
    //   71: fstore #10
    //   73: iload_3
    //   74: ldc_w 1073741824
    //   77: if_icmpne -> 95
    //   80: aload #8
    //   82: astore #11
    //   84: iconst_m1
    //   85: istore_2
    //   86: iconst_0
    //   87: istore #9
    //   89: iload #5
    //   91: istore_1
    //   92: goto -> 641
    //   95: aload_0
    //   96: getfield mLayout : Landroid/text/Layout;
    //   99: astore #11
    //   101: iload_1
    //   102: istore_2
    //   103: aload #11
    //   105: ifnull -> 123
    //   108: iload_1
    //   109: istore_2
    //   110: aload_0
    //   111: getfield mEllipsize : Landroid/text/TextUtils$TruncateAt;
    //   114: ifnonnull -> 123
    //   117: aload #11
    //   119: invokestatic desired : (Landroid/text/Layout;)I
    //   122: istore_2
    //   123: iload_2
    //   124: ifge -> 176
    //   127: aload_0
    //   128: getfield mTransformed : Ljava/lang/CharSequence;
    //   131: aload_0
    //   132: getfield mTextPaint : Landroid/text/TextPaint;
    //   135: aload_0
    //   136: getfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   139: aload_0
    //   140: getfield mBoring : Landroid/text/BoringLayout$Metrics;
    //   143: invokestatic isBoring : (Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;
    //   146: astore #11
    //   148: aload #11
    //   150: astore #7
    //   152: iload #9
    //   154: istore_1
    //   155: aload #11
    //   157: ifnull -> 178
    //   160: aload_0
    //   161: aload #11
    //   163: putfield mBoring : Landroid/text/BoringLayout$Metrics;
    //   166: aload #11
    //   168: astore #7
    //   170: iload #9
    //   172: istore_1
    //   173: goto -> 178
    //   176: iconst_1
    //   177: istore_1
    //   178: aload #7
    //   180: ifnull -> 204
    //   183: aload #7
    //   185: getstatic android/widget/TextView.UNKNOWN_BORING : Landroid/text/BoringLayout$Metrics;
    //   188: if_acmpne -> 194
    //   191: goto -> 204
    //   194: aload #7
    //   196: getfield width : I
    //   199: istore #12
    //   201: goto -> 264
    //   204: iload_2
    //   205: istore #9
    //   207: iload_2
    //   208: ifge -> 257
    //   211: aload_0
    //   212: getfield mTransformed : Ljava/lang/CharSequence;
    //   215: astore #13
    //   217: aload #13
    //   219: invokeinterface length : ()I
    //   224: istore_2
    //   225: aload_0
    //   226: getfield mTextPaint : Landroid/text/TextPaint;
    //   229: astore #14
    //   231: aload_0
    //   232: getfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   235: astore #11
    //   237: aload #13
    //   239: iconst_0
    //   240: iload_2
    //   241: aload #14
    //   243: aload #11
    //   245: fload #10
    //   247: invokestatic getDesiredWidthWithLimit : (Ljava/lang/CharSequence;IILandroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;F)F
    //   250: f2d
    //   251: invokestatic ceil : (D)D
    //   254: d2i
    //   255: istore #9
    //   257: iload #9
    //   259: istore #12
    //   261: iload #9
    //   263: istore_2
    //   264: aload_0
    //   265: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   268: astore #11
    //   270: iload #12
    //   272: istore #9
    //   274: aload #11
    //   276: ifnull -> 303
    //   279: iload #12
    //   281: aload #11
    //   283: getfield mDrawableWidthTop : I
    //   286: invokestatic max : (II)I
    //   289: istore #9
    //   291: iload #9
    //   293: aload #11
    //   295: getfield mDrawableWidthBottom : I
    //   298: invokestatic max : (II)I
    //   301: istore #9
    //   303: aload #8
    //   305: astore #11
    //   307: iload #9
    //   309: istore #15
    //   311: aload_0
    //   312: getfield mHint : Ljava/lang/CharSequence;
    //   315: ifnull -> 509
    //   318: iconst_m1
    //   319: istore #15
    //   321: aload_0
    //   322: getfield mHintLayout : Landroid/text/Layout;
    //   325: astore #11
    //   327: iload #15
    //   329: istore #12
    //   331: aload #11
    //   333: ifnull -> 354
    //   336: iload #15
    //   338: istore #12
    //   340: aload_0
    //   341: getfield mEllipsize : Landroid/text/TextUtils$TruncateAt;
    //   344: ifnonnull -> 354
    //   347: aload #11
    //   349: invokestatic desired : (Landroid/text/Layout;)I
    //   352: istore #12
    //   354: iload #12
    //   356: ifge -> 399
    //   359: aload_0
    //   360: getfield mHint : Ljava/lang/CharSequence;
    //   363: aload_0
    //   364: getfield mTextPaint : Landroid/text/TextPaint;
    //   367: aload_0
    //   368: getfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   371: aload_0
    //   372: getfield mHintBoring : Landroid/text/BoringLayout$Metrics;
    //   375: invokestatic isBoring : (Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;
    //   378: astore #11
    //   380: aload #11
    //   382: astore #8
    //   384: aload #11
    //   386: ifnull -> 399
    //   389: aload_0
    //   390: aload #11
    //   392: putfield mHintBoring : Landroid/text/BoringLayout$Metrics;
    //   395: aload #11
    //   397: astore #8
    //   399: aload #8
    //   401: ifnull -> 425
    //   404: aload #8
    //   406: getstatic android/widget/TextView.UNKNOWN_BORING : Landroid/text/BoringLayout$Metrics;
    //   409: if_acmpne -> 415
    //   412: goto -> 425
    //   415: aload #8
    //   417: getfield width : I
    //   420: istore #12
    //   422: goto -> 486
    //   425: iload #12
    //   427: istore #15
    //   429: iload #12
    //   431: ifge -> 482
    //   434: aload_0
    //   435: getfield mHint : Ljava/lang/CharSequence;
    //   438: astore #13
    //   440: aload #13
    //   442: invokeinterface length : ()I
    //   447: istore #12
    //   449: aload_0
    //   450: getfield mTextPaint : Landroid/text/TextPaint;
    //   453: astore #14
    //   455: aload_0
    //   456: getfield mTextDir : Landroid/text/TextDirectionHeuristic;
    //   459: astore #11
    //   461: aload #13
    //   463: iconst_0
    //   464: iload #12
    //   466: aload #14
    //   468: aload #11
    //   470: fload #10
    //   472: invokestatic getDesiredWidthWithLimit : (Ljava/lang/CharSequence;IILandroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;F)F
    //   475: f2d
    //   476: invokestatic ceil : (D)D
    //   479: d2i
    //   480: istore #15
    //   482: iload #15
    //   484: istore #12
    //   486: aload #8
    //   488: astore #11
    //   490: iload #9
    //   492: istore #15
    //   494: iload #12
    //   496: iload #9
    //   498: if_icmple -> 509
    //   501: iload #12
    //   503: istore #15
    //   505: aload #8
    //   507: astore #11
    //   509: iload #15
    //   511: aload_0
    //   512: invokevirtual getCompoundPaddingLeft : ()I
    //   515: aload_0
    //   516: invokevirtual getCompoundPaddingRight : ()I
    //   519: iadd
    //   520: iadd
    //   521: istore #9
    //   523: aload_0
    //   524: getfield mMaxWidthMode : I
    //   527: iconst_1
    //   528: if_icmpne -> 550
    //   531: iload #9
    //   533: aload_0
    //   534: getfield mMaxWidth : I
    //   537: aload_0
    //   538: invokevirtual getLineHeight : ()I
    //   541: imul
    //   542: invokestatic min : (II)I
    //   545: istore #9
    //   547: goto -> 561
    //   550: iload #9
    //   552: aload_0
    //   553: getfield mMaxWidth : I
    //   556: invokestatic min : (II)I
    //   559: istore #9
    //   561: aload_0
    //   562: getfield mMinWidthMode : I
    //   565: iconst_1
    //   566: if_icmpne -> 588
    //   569: iload #9
    //   571: aload_0
    //   572: getfield mMinWidth : I
    //   575: aload_0
    //   576: invokevirtual getLineHeight : ()I
    //   579: imul
    //   580: invokestatic max : (II)I
    //   583: istore #9
    //   585: goto -> 599
    //   588: iload #9
    //   590: aload_0
    //   591: getfield mMinWidth : I
    //   594: invokestatic max : (II)I
    //   597: istore #9
    //   599: iload #9
    //   601: aload_0
    //   602: invokevirtual getSuggestedMinimumWidth : ()I
    //   605: invokestatic max : (II)I
    //   608: istore #12
    //   610: iload_3
    //   611: ldc_w -2147483648
    //   614: if_icmpne -> 635
    //   617: iload #5
    //   619: iload #12
    //   621: invokestatic min : (II)I
    //   624: istore #12
    //   626: iload_1
    //   627: istore #9
    //   629: iload #12
    //   631: istore_1
    //   632: goto -> 641
    //   635: iload_1
    //   636: istore #9
    //   638: iload #12
    //   640: istore_1
    //   641: iload_1
    //   642: aload_0
    //   643: invokevirtual getCompoundPaddingLeft : ()I
    //   646: isub
    //   647: aload_0
    //   648: invokevirtual getCompoundPaddingRight : ()I
    //   651: isub
    //   652: istore #5
    //   654: iload #5
    //   656: istore #12
    //   658: aload_0
    //   659: getfield mHorizontallyScrolling : Z
    //   662: ifeq -> 669
    //   665: ldc 1048576
    //   667: istore #12
    //   669: aload_0
    //   670: getfield mHintLayout : Landroid/text/Layout;
    //   673: astore #8
    //   675: aload #8
    //   677: ifnonnull -> 687
    //   680: iload #12
    //   682: istore #15
    //   684: goto -> 694
    //   687: aload #8
    //   689: invokevirtual getWidth : ()I
    //   692: istore #15
    //   694: aload_0
    //   695: getfield mLayout : Landroid/text/Layout;
    //   698: astore #8
    //   700: aload #8
    //   702: ifnonnull -> 738
    //   705: aload_0
    //   706: invokevirtual getCompoundPaddingLeft : ()I
    //   709: istore_2
    //   710: aload_0
    //   711: invokevirtual getCompoundPaddingRight : ()I
    //   714: istore #9
    //   716: aload_0
    //   717: iload #12
    //   719: iload #12
    //   721: aload #7
    //   723: aload #11
    //   725: iload_1
    //   726: iload_2
    //   727: isub
    //   728: iload #9
    //   730: isub
    //   731: iconst_0
    //   732: invokevirtual makeNewLayout : (IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V
    //   735: goto -> 959
    //   738: aload #8
    //   740: invokevirtual getWidth : ()I
    //   743: iload #12
    //   745: if_icmpne -> 793
    //   748: iload #15
    //   750: iload #12
    //   752: if_icmpne -> 793
    //   755: aload_0
    //   756: getfield mLayout : Landroid/text/Layout;
    //   759: astore #8
    //   761: aload #8
    //   763: invokevirtual getEllipsizedWidth : ()I
    //   766: istore #15
    //   768: iload #15
    //   770: iload_1
    //   771: aload_0
    //   772: invokevirtual getCompoundPaddingLeft : ()I
    //   775: isub
    //   776: aload_0
    //   777: invokevirtual getCompoundPaddingRight : ()I
    //   780: isub
    //   781: if_icmpeq -> 787
    //   784: goto -> 793
    //   787: iconst_0
    //   788: istore #15
    //   790: goto -> 796
    //   793: iconst_1
    //   794: istore #15
    //   796: aload_0
    //   797: getfield mHint : Ljava/lang/CharSequence;
    //   800: ifnonnull -> 856
    //   803: aload_0
    //   804: getfield mEllipsize : Landroid/text/TextUtils$TruncateAt;
    //   807: ifnonnull -> 856
    //   810: aload_0
    //   811: getfield mLayout : Landroid/text/Layout;
    //   814: astore #8
    //   816: iload #12
    //   818: aload #8
    //   820: invokevirtual getWidth : ()I
    //   823: if_icmple -> 856
    //   826: aload_0
    //   827: getfield mLayout : Landroid/text/Layout;
    //   830: instanceof android/text/BoringLayout
    //   833: ifne -> 851
    //   836: iload #9
    //   838: ifeq -> 856
    //   841: iload_2
    //   842: iflt -> 856
    //   845: iload_2
    //   846: iload #12
    //   848: if_icmpgt -> 856
    //   851: iconst_1
    //   852: istore_2
    //   853: goto -> 858
    //   856: iconst_0
    //   857: istore_2
    //   858: aload_0
    //   859: getfield mMaxMode : I
    //   862: aload_0
    //   863: getfield mOldMaxMode : I
    //   866: if_icmpne -> 889
    //   869: aload_0
    //   870: getfield mMaximum : I
    //   873: aload_0
    //   874: getfield mOldMaximum : I
    //   877: if_icmpeq -> 883
    //   880: goto -> 889
    //   883: iconst_0
    //   884: istore #9
    //   886: goto -> 892
    //   889: iconst_1
    //   890: istore #9
    //   892: iload #15
    //   894: ifne -> 908
    //   897: iload #9
    //   899: ifeq -> 905
    //   902: goto -> 908
    //   905: goto -> 959
    //   908: iload #9
    //   910: ifne -> 929
    //   913: iload_2
    //   914: ifeq -> 929
    //   917: aload_0
    //   918: getfield mLayout : Landroid/text/Layout;
    //   921: iload #12
    //   923: invokevirtual increaseWidthTo : (I)V
    //   926: goto -> 959
    //   929: aload_0
    //   930: invokevirtual getCompoundPaddingLeft : ()I
    //   933: istore #9
    //   935: aload_0
    //   936: invokevirtual getCompoundPaddingRight : ()I
    //   939: istore_2
    //   940: aload_0
    //   941: iload #12
    //   943: iload #12
    //   945: aload #7
    //   947: aload #11
    //   949: iload_1
    //   950: iload #9
    //   952: isub
    //   953: iload_2
    //   954: isub
    //   955: iconst_0
    //   956: invokevirtual makeNewLayout : (IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V
    //   959: iload #4
    //   961: ldc_w 1073741824
    //   964: if_icmpne -> 978
    //   967: iload #6
    //   969: istore_2
    //   970: aload_0
    //   971: iconst_m1
    //   972: putfield mDesiredHeightAtMeasure : I
    //   975: goto -> 1006
    //   978: aload_0
    //   979: invokespecial getDesiredHeight : ()I
    //   982: istore_2
    //   983: aload_0
    //   984: iload_2
    //   985: putfield mDesiredHeightAtMeasure : I
    //   988: iload #4
    //   990: ldc_w -2147483648
    //   993: if_icmpne -> 1006
    //   996: iload_2
    //   997: iload #6
    //   999: invokestatic min : (II)I
    //   1002: istore_2
    //   1003: goto -> 1006
    //   1006: iload_2
    //   1007: aload_0
    //   1008: invokevirtual getCompoundPaddingTop : ()I
    //   1011: isub
    //   1012: aload_0
    //   1013: invokevirtual getCompoundPaddingBottom : ()I
    //   1016: isub
    //   1017: istore #12
    //   1019: iload #12
    //   1021: istore #9
    //   1023: aload_0
    //   1024: getfield mMaxMode : I
    //   1027: iconst_1
    //   1028: if_icmpne -> 1073
    //   1031: aload_0
    //   1032: getfield mLayout : Landroid/text/Layout;
    //   1035: invokevirtual getLineCount : ()I
    //   1038: istore #15
    //   1040: aload_0
    //   1041: getfield mMaximum : I
    //   1044: istore #6
    //   1046: iload #12
    //   1048: istore #9
    //   1050: iload #15
    //   1052: iload #6
    //   1054: if_icmple -> 1073
    //   1057: iload #12
    //   1059: aload_0
    //   1060: getfield mLayout : Landroid/text/Layout;
    //   1063: iload #6
    //   1065: invokevirtual getLineTop : (I)I
    //   1068: invokestatic min : (II)I
    //   1071: istore #9
    //   1073: aload_0
    //   1074: getfield mMovement : Landroid/text/method/MovementMethod;
    //   1077: ifnonnull -> 1124
    //   1080: aload_0
    //   1081: getfield mLayout : Landroid/text/Layout;
    //   1084: astore #7
    //   1086: aload #7
    //   1088: invokevirtual getWidth : ()I
    //   1091: iload #5
    //   1093: if_icmpgt -> 1124
    //   1096: aload_0
    //   1097: getfield mLayout : Landroid/text/Layout;
    //   1100: astore #7
    //   1102: aload #7
    //   1104: invokevirtual getHeight : ()I
    //   1107: iload #9
    //   1109: if_icmple -> 1115
    //   1112: goto -> 1124
    //   1115: aload_0
    //   1116: iconst_0
    //   1117: iconst_0
    //   1118: invokevirtual scrollTo : (II)V
    //   1121: goto -> 1128
    //   1124: aload_0
    //   1125: invokespecial registerForPreDraw : ()V
    //   1128: aload_0
    //   1129: iload_1
    //   1130: iload_2
    //   1131: invokevirtual setMeasuredDimension : (II)V
    //   1134: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #9384	-> 0
    //   #9385	-> 5
    //   #9386	-> 11
    //   #9387	-> 17
    //   #9392	-> 23
    //   #9393	-> 28
    //   #9395	-> 33
    //   #9396	-> 40
    //   #9399	-> 48
    //   #9400	-> 50
    //   #9401	-> 53
    //   #9402	-> 60
    //   #9404	-> 73
    //   #9406	-> 80
    //   #9408	-> 95
    //   #9409	-> 117
    //   #9412	-> 123
    //   #9413	-> 127
    //   #9414	-> 148
    //   #9415	-> 160
    //   #9418	-> 176
    //   #9421	-> 178
    //   #9428	-> 194
    //   #9422	-> 204
    //   #9423	-> 211
    //   #9424	-> 217
    //   #9423	-> 237
    //   #9426	-> 257
    //   #9431	-> 264
    //   #9432	-> 270
    //   #9433	-> 279
    //   #9434	-> 291
    //   #9437	-> 303
    //   #9438	-> 318
    //   #9441	-> 321
    //   #9442	-> 347
    //   #9445	-> 354
    //   #9446	-> 359
    //   #9447	-> 380
    //   #9448	-> 389
    //   #9452	-> 399
    //   #9459	-> 415
    //   #9453	-> 425
    //   #9454	-> 434
    //   #9455	-> 440
    //   #9454	-> 461
    //   #9457	-> 482
    //   #9462	-> 486
    //   #9463	-> 501
    //   #9467	-> 509
    //   #9469	-> 523
    //   #9470	-> 531
    //   #9472	-> 550
    //   #9475	-> 561
    //   #9476	-> 569
    //   #9478	-> 588
    //   #9482	-> 599
    //   #9484	-> 610
    //   #9485	-> 617
    //   #9484	-> 635
    //   #9489	-> 641
    //   #9490	-> 654
    //   #9492	-> 654
    //   #9494	-> 669
    //   #9495	-> 669
    //   #9497	-> 694
    //   #9498	-> 705
    //   #9499	-> 705
    //   #9498	-> 716
    //   #9501	-> 738
    //   #9502	-> 761
    //   #9503	-> 768
    //   #9501	-> 793
    //   #9503	-> 793
    //   #9505	-> 796
    //   #9506	-> 816
    //   #9510	-> 858
    //   #9512	-> 892
    //   #9513	-> 908
    //   #9514	-> 917
    //   #9516	-> 929
    //   #9517	-> 929
    //   #9516	-> 940
    //   #9524	-> 959
    //   #9526	-> 967
    //   #9527	-> 970
    //   #9529	-> 978
    //   #9531	-> 983
    //   #9532	-> 983
    //   #9534	-> 988
    //   #9535	-> 996
    //   #9534	-> 1006
    //   #9539	-> 1006
    //   #9540	-> 1019
    //   #9541	-> 1057
    //   #9548	-> 1073
    //   #9549	-> 1086
    //   #9550	-> 1102
    //   #9553	-> 1115
    //   #9548	-> 1124
    //   #9551	-> 1124
    //   #9556	-> 1128
    //   #9557	-> 1134
  }
  
  private void autoSizeText() {
    if (!isAutoSizeEnabled())
      return; 
    if (this.mNeedsAutoSizeText) {
      int i;
      if (getMeasuredWidth() <= 0 || getMeasuredHeight() <= 0)
        return; 
      if (this.mHorizontallyScrolling) {
        i = 1048576;
      } else {
        i = getMeasuredWidth() - getTotalPaddingLeft() - getTotalPaddingRight();
      } 
      int j = getMeasuredHeight(), k = getExtendedPaddingBottom();
      k = j - k - getExtendedPaddingTop();
      if (i <= 0 || k <= 0)
        return; 
      synchronized (TEMP_RECTF) {
        TEMP_RECTF.setEmpty();
        TEMP_RECTF.right = i;
        TEMP_RECTF.bottom = k;
        float f = findLargestTextSizeWhichFits(TEMP_RECTF);
        if (f != getTextSize()) {
          setTextSizeInternal(0, f, false);
          BoringLayout.Metrics metrics1 = UNKNOWN_BORING, metrics2 = UNKNOWN_BORING;
          j = this.mRight;
          int m = this.mLeft;
          int n = getCompoundPaddingLeft();
          k = getCompoundPaddingRight();
          makeNewLayout(i, 0, metrics1, metrics2, j - m - n - k, false);
        } 
      } 
    } 
    this.mNeedsAutoSizeText = true;
  }
  
  private int findLargestTextSizeWhichFits(RectF paramRectF) {
    int i = this.mAutoSizeTextSizesInPx.length;
    if (i != 0) {
      int j = 0;
      int k = 0 + 1;
      i--;
      while (k <= i) {
        j = (k + i) / 2;
        if (suggestedSizeFitsInSpace(this.mAutoSizeTextSizesInPx[j], paramRectF)) {
          int m = j + 1;
          j = k;
          k = m;
          continue;
        } 
        i = j - 1;
        j = i;
      } 
      return this.mAutoSizeTextSizesInPx[j];
    } 
    throw new IllegalStateException("No available text sizes to choose from.");
  }
  
  private boolean suggestedSizeFitsInSpace(int paramInt, RectF paramRectF) {
    CharSequence charSequence = this.mTransformed;
    if (charSequence == null)
      charSequence = getText(); 
    int i = getMaxLines();
    TextPaint textPaint = this.mTempTextPaint;
    if (textPaint == null) {
      this.mTempTextPaint = new TextPaint();
    } else {
      textPaint.reset();
    } 
    this.mTempTextPaint.set(getPaint());
    this.mTempTextPaint.setTextSize(paramInt);
    paramInt = charSequence.length();
    textPaint = this.mTempTextPaint;
    int j = Math.round(paramRectF.right);
    StaticLayout.Builder builder1 = StaticLayout.Builder.obtain(charSequence, 0, paramInt, textPaint, j);
    StaticLayout.Builder builder2 = builder1.setAlignment(getLayoutAlignment());
    builder2 = builder2.setLineSpacing(getLineSpacingExtra(), getLineSpacingMultiplier());
    builder2 = builder2.setIncludePad(getIncludeFontPadding());
    boolean bool = this.mUseFallbackLineSpacing;
    builder2 = builder2.setUseLineSpacingFromFallbacks(bool);
    builder2 = builder2.setBreakStrategy(getBreakStrategy());
    builder2 = builder2.setHyphenationFrequency(getHyphenationFrequency());
    builder2 = builder2.setJustificationMode(getJustificationMode());
    if (this.mMaxMode == 1) {
      paramInt = this.mMaximum;
    } else {
      paramInt = Integer.MAX_VALUE;
    } 
    builder2 = builder2.setMaxLines(paramInt);
    builder2.setTextDirection(getTextDirectionHeuristic());
    StaticLayout staticLayout = builder1.build();
    if (i != -1 && staticLayout.getLineCount() > i)
      return false; 
    if (staticLayout.getHeight() > paramRectF.bottom)
      return false; 
    return true;
  }
  
  private int getDesiredHeight() {
    Layout layout = this.mLayout;
    boolean bool = true;
    int i = getDesiredHeight(layout, true);
    layout = this.mHintLayout;
    if (this.mEllipsize == null)
      bool = false; 
    int j = getDesiredHeight(layout, bool);
    return Math.max(i, j);
  }
  
  private int getDesiredHeight(Layout paramLayout, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 6
    //   4: iconst_0
    //   5: ireturn
    //   6: aload_1
    //   7: iload_2
    //   8: invokevirtual getHeight : (Z)I
    //   11: istore_3
    //   12: aload_0
    //   13: getfield mDrawables : Landroid/widget/TextView$Drawables;
    //   16: astore #4
    //   18: iload_3
    //   19: istore #5
    //   21: aload #4
    //   23: ifnull -> 49
    //   26: iload_3
    //   27: aload #4
    //   29: getfield mDrawableHeightLeft : I
    //   32: invokestatic max : (II)I
    //   35: istore #5
    //   37: iload #5
    //   39: aload #4
    //   41: getfield mDrawableHeightRight : I
    //   44: invokestatic max : (II)I
    //   47: istore #5
    //   49: aload_1
    //   50: invokevirtual getLineCount : ()I
    //   53: istore_3
    //   54: aload_0
    //   55: invokevirtual getCompoundPaddingTop : ()I
    //   58: aload_0
    //   59: invokevirtual getCompoundPaddingBottom : ()I
    //   62: iadd
    //   63: istore #6
    //   65: iload #5
    //   67: iload #6
    //   69: iadd
    //   70: istore #7
    //   72: aload_0
    //   73: getfield mMaxMode : I
    //   76: iconst_1
    //   77: if_icmpeq -> 97
    //   80: iload #7
    //   82: aload_0
    //   83: getfield mMaximum : I
    //   86: invokestatic min : (II)I
    //   89: istore #5
    //   91: iload_3
    //   92: istore #8
    //   94: goto -> 197
    //   97: iload #7
    //   99: istore #5
    //   101: iload_3
    //   102: istore #8
    //   104: iload_2
    //   105: ifeq -> 197
    //   108: iload #7
    //   110: istore #5
    //   112: iload_3
    //   113: istore #8
    //   115: iload_3
    //   116: aload_0
    //   117: getfield mMaximum : I
    //   120: if_icmple -> 197
    //   123: aload_1
    //   124: instanceof android/text/DynamicLayout
    //   127: ifne -> 144
    //   130: iload #7
    //   132: istore #5
    //   134: iload_3
    //   135: istore #8
    //   137: aload_1
    //   138: instanceof android/text/BoringLayout
    //   141: ifeq -> 197
    //   144: aload_1
    //   145: aload_0
    //   146: getfield mMaximum : I
    //   149: invokevirtual getLineTop : (I)I
    //   152: istore_3
    //   153: iload_3
    //   154: istore #5
    //   156: aload #4
    //   158: ifnull -> 184
    //   161: iload_3
    //   162: aload #4
    //   164: getfield mDrawableHeightLeft : I
    //   167: invokestatic max : (II)I
    //   170: istore #5
    //   172: iload #5
    //   174: aload #4
    //   176: getfield mDrawableHeightRight : I
    //   179: invokestatic max : (II)I
    //   182: istore #5
    //   184: iload #5
    //   186: iload #6
    //   188: iadd
    //   189: istore #5
    //   191: aload_0
    //   192: getfield mMaximum : I
    //   195: istore #8
    //   197: aload_0
    //   198: getfield mMinMode : I
    //   201: iconst_1
    //   202: if_icmpne -> 236
    //   205: iload #5
    //   207: istore_3
    //   208: iload #8
    //   210: aload_0
    //   211: getfield mMinimum : I
    //   214: if_icmpge -> 246
    //   217: iload #5
    //   219: aload_0
    //   220: invokevirtual getLineHeight : ()I
    //   223: aload_0
    //   224: getfield mMinimum : I
    //   227: iload #8
    //   229: isub
    //   230: imul
    //   231: iadd
    //   232: istore_3
    //   233: goto -> 246
    //   236: iload #5
    //   238: aload_0
    //   239: getfield mMinimum : I
    //   242: invokestatic max : (II)I
    //   245: istore_3
    //   246: iload_3
    //   247: aload_0
    //   248: invokevirtual getSuggestedMinimumHeight : ()I
    //   251: invokestatic max : (II)I
    //   254: istore #5
    //   256: iload #5
    //   258: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #9679	-> 0
    //   #9680	-> 4
    //   #9687	-> 6
    //   #9689	-> 12
    //   #9690	-> 18
    //   #9691	-> 26
    //   #9692	-> 37
    //   #9695	-> 49
    //   #9696	-> 54
    //   #9697	-> 65
    //   #9699	-> 72
    //   #9700	-> 80
    //   #9701	-> 97
    //   #9703	-> 144
    //   #9705	-> 153
    //   #9706	-> 161
    //   #9707	-> 172
    //   #9710	-> 184
    //   #9711	-> 191
    //   #9714	-> 197
    //   #9715	-> 205
    //   #9716	-> 217
    //   #9719	-> 236
    //   #9723	-> 246
    //   #9725	-> 256
  }
  
  private void checkForResize() {
    boolean bool1 = false, bool2 = false;
    if (this.mLayout != null) {
      if (this.mLayoutParams.width == -2) {
        bool2 = true;
        invalidate();
      } 
      if (this.mLayoutParams.height == -2) {
        int i = getDesiredHeight();
        bool1 = bool2;
        if (i != getHeight())
          bool1 = true; 
      } else {
        bool1 = bool2;
        if (this.mLayoutParams.height == -1) {
          bool1 = bool2;
          if (this.mDesiredHeightAtMeasure >= 0) {
            int i = getDesiredHeight();
            bool1 = bool2;
            if (i != this.mDesiredHeightAtMeasure)
              bool1 = true; 
          } 
        } 
      } 
    } 
    if (bool1)
      requestLayout(); 
  }
  
  private void checkForRelayout() {
    if ((this.mLayoutParams.width != -2 || (this.mMaxWidthMode == this.mMinWidthMode && this.mMaxWidth == this.mMinWidth)) && (this.mHint == null || this.mHintLayout != null)) {
      int i = this.mRight, j = this.mLeft;
      if (i - j - getCompoundPaddingLeft() - getCompoundPaddingRight() > 0) {
        j = this.mLayout.getHeight();
        int k = this.mLayout.getWidth();
        Layout layout = this.mHintLayout;
        if (layout == null) {
          i = 0;
        } else {
          i = layout.getWidth();
        } 
        BoringLayout.Metrics metrics = UNKNOWN_BORING;
        int m = this.mRight, n = this.mLeft;
        int i1 = getCompoundPaddingLeft(), i2 = getCompoundPaddingRight();
        makeNewLayout(k, i, metrics, metrics, m - n - i1 - i2, false);
        if (this.mEllipsize != TextUtils.TruncateAt.MARQUEE) {
          if (this.mLayoutParams.height != -2 && this.mLayoutParams.height != -1) {
            autoSizeText();
            invalidate();
            return;
          } 
          if (this.mLayout.getHeight() == j) {
            Layout layout1 = this.mHintLayout;
            if (layout1 == null || 
              layout1.getHeight() == j) {
              autoSizeText();
              invalidate();
              return;
            } 
          } 
        } 
        requestLayout();
        invalidate();
        return;
      } 
    } 
    nullLayouts();
    requestLayout();
    invalidate();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (this.mDeferScroll >= 0) {
      paramInt1 = this.mDeferScroll;
      this.mDeferScroll = -1;
      bringPointIntoView(Math.min(paramInt1, this.mText.length()));
    } 
    autoSizeText();
  }
  
  private boolean isShowingHint() {
    boolean bool;
    if (TextUtils.isEmpty(this.mText) && !TextUtils.isEmpty(this.mHint)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean bringTextIntoView() {
    Layout layout;
    Layout.Alignment alignment2;
    boolean bool;
    if (isShowingHint()) {
      layout = this.mHintLayout;
    } else {
      layout = this.mLayout;
    } 
    int i = 0;
    if ((this.mGravity & 0x70) == 80)
      i = layout.getLineCount() - 1; 
    Layout.Alignment alignment1 = layout.getParagraphAlignment(i);
    int j = layout.getParagraphDirection(i);
    int k = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
    int m = this.mBottom - this.mTop - getExtendedPaddingTop() - getExtendedPaddingBottom();
    int n = layout.getHeight();
    if (alignment1 == Layout.Alignment.ALIGN_NORMAL) {
      if (j == 1) {
        alignment2 = Layout.Alignment.ALIGN_LEFT;
      } else {
        alignment2 = Layout.Alignment.ALIGN_RIGHT;
      } 
    } else {
      alignment2 = alignment1;
      if (alignment1 == Layout.Alignment.ALIGN_OPPOSITE)
        if (j == 1) {
          alignment2 = Layout.Alignment.ALIGN_RIGHT;
        } else {
          alignment2 = Layout.Alignment.ALIGN_LEFT;
        }  
    } 
    if (alignment2 == Layout.Alignment.ALIGN_CENTER) {
      bool = (int)Math.floor(layout.getLineLeft(i));
      i = (int)Math.ceil(layout.getLineRight(i));
      if (i - bool < k) {
        i = (i + bool) / 2 - k / 2;
      } else if (j < 0) {
        i -= k;
      } else {
        i = bool;
      } 
    } else if (alignment2 == Layout.Alignment.ALIGN_RIGHT) {
      i = (int)Math.ceil(layout.getLineRight(i));
      i -= k;
    } else {
      i = (int)Math.floor(layout.getLineLeft(i));
    } 
    if (n < m) {
      bool = false;
    } else if ((this.mGravity & 0x70) == 80) {
      bool = n - m;
    } else {
      bool = false;
    } 
    if (i != this.mScrollX || bool != this.mScrollY) {
      scrollTo(i, bool);
      return true;
    } 
    return false;
  }
  
  public boolean bringPointIntoView(int paramInt) {
    Layout layout;
    boolean bool = isLayoutRequested();
    boolean bool1 = false;
    if (bool) {
      this.mDeferScroll = paramInt;
      return false;
    } 
    if (isShowingHint()) {
      layout = this.mHintLayout;
    } else {
      layout = this.mLayout;
    } 
    if (layout == null)
      return false; 
    int i = layout.getLineForOffset(paramInt);
    int j = null.$SwitchMap$android$text$Layout$Alignment[layout.getParagraphAlignment(i).ordinal()];
    if (j != 1) {
      if (j != 2) {
        if (j != 3) {
          if (j != 4) {
            j = 0;
          } else {
            j = -layout.getParagraphDirection(i);
          } 
        } else {
          j = layout.getParagraphDirection(i);
        } 
      } else {
        j = -1;
      } 
    } else {
      j = 1;
    } 
    if (j > 0)
      bool1 = true; 
    int k = (int)layout.getPrimaryHorizontal(paramInt, bool1);
    int m = layout.getLineTop(i);
    int n = layout.getLineTop(i + 1);
    int i1 = (int)Math.floor(layout.getLineLeft(i));
    paramInt = (int)Math.ceil(layout.getLineRight(i));
    int i2 = layout.getHeight();
    int i3 = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
    int i4 = this.mBottom - this.mTop - getExtendedPaddingTop() - getExtendedPaddingBottom();
    int i5 = paramInt;
    if (!this.mHorizontallyScrolling) {
      i5 = paramInt;
      if (paramInt - i1 > i3) {
        i5 = paramInt;
        if (paramInt > k)
          i5 = Math.max(k, i1 + i3); 
      } 
    } 
    paramInt = (n - m) / 2;
    int i6 = i4 / 4, i7 = paramInt, i8 = i7;
    if (i7 > i6)
      i8 = i4 / 4; 
    i7 = paramInt;
    if (paramInt > i3 / 4)
      i7 = i3 / 4; 
    i6 = this.mScrollX;
    int i9 = this.mScrollY;
    paramInt = i9;
    if (m - i9 < i8)
      paramInt = m - i8; 
    if (n - paramInt > i4 - i8) {
      i8 = n - i4 - i8;
    } else {
      i8 = paramInt;
    } 
    paramInt = i8;
    if (i2 - i8 < i4)
      paramInt = i2 - i4; 
    i8 = paramInt;
    if (0 - paramInt > 0)
      i8 = 0; 
    paramInt = i6;
    if (j != 0) {
      paramInt = i6;
      if (k - i6 < i7)
        paramInt = k - i7; 
      if (k - paramInt > i3 - i7)
        paramInt = k - i3 - i7; 
    } 
    if (j < 0) {
      j = paramInt;
      if (i1 - paramInt > 0)
        j = i1; 
      paramInt = j;
      if (i5 - j < i3)
        paramInt = i5 - i3; 
    } else if (j > 0) {
      j = paramInt;
      if (i5 - paramInt < i3)
        j = i5 - i3; 
      paramInt = j;
      if (i1 - j > 0)
        paramInt = i1; 
    } else if (i5 - i1 <= i3) {
      paramInt = i1 - (i3 - i5 - i1) / 2;
    } else if (k > i5 - i7) {
      paramInt = i5 - i3;
    } else if (k < i1 + i7) {
      paramInt = i1;
    } else if (i1 > paramInt) {
      paramInt = i1;
    } else if (i5 < paramInt + i3) {
      paramInt = i5 - i3;
    } else {
      j = paramInt;
      if (k - paramInt < i7)
        j = k - i7; 
      if (k - j > i3 - i7) {
        paramInt = k - i3 - i7;
      } else {
        paramInt = j;
      } 
    } 
    if (paramInt != this.mScrollX || i8 != this.mScrollY) {
      if (this.mScroller == null) {
        scrollTo(paramInt, i8);
      } else {
        long l1 = AnimationUtils.currentAnimationTimeMillis(), l2 = this.mLastScroll;
        paramInt -= this.mScrollX;
        j = i8 - this.mScrollY;
        if (l1 - l2 > 250L) {
          this.mScroller.startScroll(this.mScrollX, this.mScrollY, paramInt, j);
          awakenScrollBars(this.mScroller.getDuration());
          invalidate();
        } else {
          if (!this.mScroller.isFinished())
            this.mScroller.abortAnimation(); 
          scrollBy(paramInt, j);
        } 
        this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
      } 
      bool1 = true;
    } else {
      bool1 = false;
    } 
    bool = bool1;
    if (isFocused()) {
      if (this.mTempRect == null)
        this.mTempRect = new Rect(); 
      this.mTempRect.set(k - 2, m, k + 2, n);
      getInterestingRect(this.mTempRect, i);
      this.mTempRect.offset(this.mScrollX, this.mScrollY);
      bool = bool1;
      if (requestRectangleOnScreen(this.mTempRect))
        bool = true; 
    } 
    return bool;
  }
  
  public boolean moveCursorToVisibleOffset() {
    if (!(this.mText instanceof Spannable))
      return false; 
    int i = getSelectionStart();
    int j = getSelectionEnd();
    if (i != j)
      return false; 
    j = this.mLayout.getLineForOffset(i);
    int k = this.mLayout.getLineTop(j);
    int m = this.mLayout.getLineTop(j + 1);
    int n = this.mBottom - this.mTop - getExtendedPaddingTop() - getExtendedPaddingBottom();
    int i1 = (m - k) / 2;
    int i2 = i1;
    if (i1 > n / 4)
      i2 = n / 4; 
    i1 = this.mScrollY;
    if (k < i1 + i2) {
      j = this.mLayout.getLineForVertical(i1 + i2 + m - k);
    } else if (m > n + i1 - i2) {
      j = this.mLayout.getLineForVertical(n + i1 - i2 - m - k);
    } 
    m = this.mRight;
    int i3 = this.mLeft;
    i1 = getCompoundPaddingLeft();
    k = getCompoundPaddingRight();
    n = this.mScrollX;
    i2 = this.mLayout.getOffsetForHorizontal(j, n);
    i1 = this.mLayout.getOffsetForHorizontal(j, (m - i3 - i1 - k + n));
    if (i2 < i1) {
      j = i2;
    } else {
      j = i1;
    } 
    if (i2 <= i1)
      i2 = i1; 
    i1 = i;
    if (i1 >= j) {
      j = i1;
      if (i1 > i2)
        j = i2; 
    } 
    if (j != i) {
      Selection.setSelection(this.mSpannable, j);
      return true;
    } 
    return false;
  }
  
  public void computeScroll() {
    Scroller scroller = this.mScroller;
    if (scroller != null && 
      scroller.computeScrollOffset()) {
      this.mScrollX = this.mScroller.getCurrX();
      this.mScrollY = this.mScroller.getCurrY();
      invalidateParentCaches();
      postInvalidate();
    } 
  }
  
  private void getInterestingRect(Rect paramRect, int paramInt) {
    convertFromViewportToContentCoordinates(paramRect);
    if (paramInt == 0)
      paramRect.top -= getExtendedPaddingTop(); 
    if (paramInt == this.mLayout.getLineCount() - 1)
      paramRect.bottom += getExtendedPaddingBottom(); 
  }
  
  private void convertFromViewportToContentCoordinates(Rect paramRect) {
    int i = viewportToContentHorizontalOffset();
    paramRect.left += i;
    paramRect.right += i;
    i = viewportToContentVerticalOffset();
    paramRect.top += i;
    paramRect.bottom += i;
  }
  
  int viewportToContentHorizontalOffset() {
    return getCompoundPaddingLeft() - this.mScrollX;
  }
  
  int viewportToContentVerticalOffset() {
    int i = getExtendedPaddingTop() - this.mScrollY;
    int j = i;
    if ((this.mGravity & 0x70) != 48)
      j = i + getVerticalOffset(false); 
    return j;
  }
  
  public void debug(int paramInt) {
    super.debug(paramInt);
    String str = debugIndent(paramInt);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append("frame={");
    stringBuilder.append(this.mLeft);
    stringBuilder.append(", ");
    stringBuilder.append(this.mTop);
    stringBuilder.append(", ");
    stringBuilder.append(this.mRight);
    stringBuilder.append(", ");
    stringBuilder.append(this.mBottom);
    stringBuilder.append("} scroll={");
    stringBuilder.append(this.mScrollX);
    stringBuilder.append(", ");
    stringBuilder.append(this.mScrollY);
    stringBuilder.append("} ");
    str = stringBuilder.toString();
    if (this.mText != null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append("mText=\"");
      stringBuilder.append(this.mText);
      stringBuilder.append("\" ");
      String str1 = stringBuilder.toString();
      str = str1;
      if (this.mLayout != null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str1);
        stringBuilder1.append("mLayout width=");
        stringBuilder1.append(this.mLayout.getWidth());
        stringBuilder1.append(" height=");
        Layout layout = this.mLayout;
        stringBuilder1.append(layout.getHeight());
        str = stringBuilder1.toString();
      } 
    } else {
      stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append("mText=NULL");
      str = stringBuilder.toString();
    } 
    Log.d("View", str);
  }
  
  @ExportedProperty(category = "text")
  public int getSelectionStart() {
    return Selection.getSelectionStart(getText());
  }
  
  @ExportedProperty(category = "text")
  public int getSelectionEnd() {
    return Selection.getSelectionEnd(getText());
  }
  
  public boolean hasSelection() {
    boolean bool;
    int i = getSelectionStart();
    int j = getSelectionEnd();
    if (i >= 0 && j > 0 && i != j) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  String getSelectedText() {
    if (!hasSelection())
      return null; 
    int i = getSelectionStart();
    int j = getSelectionEnd();
    CharSequence charSequence = this.mText;
    if (i > j) {
      charSequence = charSequence.subSequence(j, i);
    } else {
      charSequence = charSequence.subSequence(i, j);
    } 
    return String.valueOf(charSequence);
  }
  
  public void setSingleLine() {
    setSingleLine(true);
  }
  
  public void setAllCaps(boolean paramBoolean) {
    if (paramBoolean) {
      setTransformationMethod(new AllCapsTransformationMethod(getContext()));
    } else {
      setTransformationMethod((TransformationMethod)null);
    } 
  }
  
  public boolean isAllCaps() {
    boolean bool;
    TransformationMethod transformationMethod = getTransformationMethod();
    if (transformationMethod != null && transformationMethod instanceof AllCapsTransformationMethod) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @RemotableViewMethod
  public void setSingleLine(boolean paramBoolean) {
    setInputTypeSingleLine(paramBoolean);
    applySingleLine(paramBoolean, true, true);
  }
  
  private void setInputTypeSingleLine(boolean paramBoolean) {
    Editor editor = this.mEditor;
    if (editor != null && (editor.mInputType & 0xF) == 1)
      if (paramBoolean) {
        editor = this.mEditor;
        editor.mInputType &= 0xFFFDFFFF;
      } else {
        editor = this.mEditor;
        editor.mInputType |= 0x20000;
      }  
  }
  
  private void applySingleLine(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    this.mSingleLine = paramBoolean1;
    if (paramBoolean1) {
      setLines(1);
      setHorizontallyScrolling(true);
      if (paramBoolean2)
        setTransformationMethod(SingleLineTransformationMethod.getInstance()); 
    } else {
      if (paramBoolean3)
        setMaxLines(2147483647); 
      setHorizontallyScrolling(false);
      if (paramBoolean2)
        setTransformationMethod((TransformationMethod)null); 
    } 
  }
  
  public void setEllipsize(TextUtils.TruncateAt paramTruncateAt) {
    if (this.mEllipsize != paramTruncateAt) {
      this.mEllipsize = paramTruncateAt;
      if (this.mLayout != null) {
        nullLayouts();
        requestLayout();
        invalidate();
      } 
    } 
  }
  
  public void setMarqueeRepeatLimit(int paramInt) {
    this.mMarqueeRepeatLimit = paramInt;
  }
  
  public int getMarqueeRepeatLimit() {
    return this.mMarqueeRepeatLimit;
  }
  
  @ExportedProperty
  public TextUtils.TruncateAt getEllipsize() {
    return this.mEllipsize;
  }
  
  @RemotableViewMethod
  public void setSelectAllOnFocus(boolean paramBoolean) {
    createEditorIfNeeded();
    this.mEditor.mSelectAllOnFocus = paramBoolean;
    if (paramBoolean) {
      CharSequence charSequence = this.mText;
      if (!(charSequence instanceof Spannable))
        setText(charSequence, BufferType.SPANNABLE); 
    } 
  }
  
  @RemotableViewMethod
  public void setCursorVisible(boolean paramBoolean) {
    if (paramBoolean && this.mEditor == null)
      return; 
    createEditorIfNeeded();
    if (this.mEditor.mCursorVisible != paramBoolean) {
      this.mEditor.mCursorVisible = paramBoolean;
      invalidate();
      this.mEditor.makeBlink();
      this.mEditor.prepareCursorControllers();
    } 
  }
  
  public boolean isCursorVisible() {
    boolean bool;
    Editor editor = this.mEditor;
    if (editor == null) {
      bool = true;
    } else {
      bool = editor.mCursorVisible;
    } 
    return bool;
  }
  
  private boolean canMarquee() {
    int i = this.mRight - this.mLeft - getCompoundPaddingLeft() - getCompoundPaddingRight();
    null = false;
    if (i > 0) {
      if (this.mLayout.getLineWidth(0) <= i) {
        if (this.mMarqueeFadeMode != 0) {
          Layout layout = this.mSavedMarqueeModeLayout;
          if (layout != null)
            if (layout.getLineWidth(0) > i)
              return true;  
        } 
        return null;
      } 
    } else {
      return null;
    } 
    return true;
  }
  
  private void startMarquee() {
    if (getKeyListener() != null)
      return; 
    if (compressText((getWidth() - getCompoundPaddingLeft() - getCompoundPaddingRight())))
      return; 
    Marquee marquee = this.mMarquee;
    if ((marquee == null || marquee.isStopped()) && (isFocused() || isSelected()) && 
      getLineCount() == 1 && canMarquee()) {
      if (this.mMarqueeFadeMode == 1) {
        this.mMarqueeFadeMode = 2;
        Layout layout2 = this.mLayout;
        Layout layout1 = this.mSavedMarqueeModeLayout;
        super.mLayout = layout1;
        this.mSavedMarqueeModeLayout = layout2;
        setHorizontalFadingEdgeEnabled(true);
        requestLayout();
        invalidate();
      } 
      if (this.mMarquee == null)
        this.mMarquee = new Marquee(this); 
      this.mMarquee.start(this.mMarqueeRepeatLimit);
    } 
  }
  
  private void stopMarquee() {
    Marquee marquee = this.mMarquee;
    if (marquee != null && !marquee.isStopped())
      this.mMarquee.stop(); 
    if (this.mMarqueeFadeMode == 2) {
      this.mMarqueeFadeMode = 1;
      Layout layout = this.mSavedMarqueeModeLayout;
      this.mSavedMarqueeModeLayout = this.mLayout;
      this.mLayout = layout;
      super.mLayout = layout;
      setHorizontalFadingEdgeEnabled(false);
      requestLayout();
      invalidate();
    } 
  }
  
  private void startStopMarquee(boolean paramBoolean) {
    if (this.mEllipsize == TextUtils.TruncateAt.MARQUEE)
      if (paramBoolean) {
        startMarquee();
      } else {
        stopMarquee();
      }  
  }
  
  protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  protected void onSelectionChanged(int paramInt1, int paramInt2) {
    sendAccessibilityEvent(8192);
  }
  
  public void addTextChangedListener(TextWatcher paramTextWatcher) {
    if (this.mListeners == null)
      this.mListeners = new ArrayList<>(); 
    this.mListeners.add(paramTextWatcher);
  }
  
  public void removeTextChangedListener(TextWatcher paramTextWatcher) {
    ArrayList<TextWatcher> arrayList = this.mListeners;
    if (arrayList != null) {
      int i = arrayList.indexOf(paramTextWatcher);
      if (i >= 0)
        this.mListeners.remove(i); 
    } 
  }
  
  private void sendBeforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    if (this.mListeners != null) {
      ArrayList<TextWatcher> arrayList = this.mListeners;
      int i = arrayList.size();
      for (byte b = 0; b < i; b++)
        ((TextWatcher)arrayList.get(b)).beforeTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3); 
    } 
    removeIntersectingNonAdjacentSpans(paramInt1, paramInt1 + paramInt2, SpellCheckSpan.class);
    removeIntersectingNonAdjacentSpans(paramInt1, paramInt1 + paramInt2, SuggestionSpan.class);
  }
  
  private <T> void removeIntersectingNonAdjacentSpans(int paramInt1, int paramInt2, Class<T> paramClass) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mText : Ljava/lang/CharSequence;
    //   4: astore #4
    //   6: aload #4
    //   8: instanceof android/text/Editable
    //   11: ifne -> 15
    //   14: return
    //   15: aload #4
    //   17: checkcast android/text/Editable
    //   20: astore #4
    //   22: aload #4
    //   24: iload_1
    //   25: iload_2
    //   26: aload_3
    //   27: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   32: astore_3
    //   33: aload_3
    //   34: arraylength
    //   35: istore #5
    //   37: iconst_0
    //   38: istore #6
    //   40: iload #6
    //   42: iload #5
    //   44: if_icmpge -> 105
    //   47: aload #4
    //   49: aload_3
    //   50: iload #6
    //   52: aaload
    //   53: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   58: istore #7
    //   60: aload #4
    //   62: aload_3
    //   63: iload #6
    //   65: aaload
    //   66: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   71: istore #8
    //   73: iload #8
    //   75: iload_1
    //   76: if_icmpeq -> 105
    //   79: iload #7
    //   81: iload_2
    //   82: if_icmpne -> 88
    //   85: goto -> 105
    //   88: aload #4
    //   90: aload_3
    //   91: iload #6
    //   93: aaload
    //   94: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   99: iinc #6, 1
    //   102: goto -> 40
    //   105: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #10649	-> 0
    //   #10650	-> 15
    //   #10652	-> 22
    //   #10653	-> 33
    //   #10654	-> 37
    //   #10655	-> 47
    //   #10656	-> 60
    //   #10657	-> 73
    //   #10658	-> 88
    //   #10654	-> 99
    //   #10660	-> 105
  }
  
  void removeAdjacentSuggestionSpans(int paramInt) {
    CharSequence charSequence = this.mText;
    if (!(charSequence instanceof Editable))
      return; 
    charSequence = charSequence;
    SuggestionSpan[] arrayOfSuggestionSpan = charSequence.<SuggestionSpan>getSpans(paramInt, paramInt, SuggestionSpan.class);
    int i = arrayOfSuggestionSpan.length;
    for (byte b = 0; b < i; b++) {
      int j = charSequence.getSpanStart(arrayOfSuggestionSpan[b]);
      int k = charSequence.getSpanEnd(arrayOfSuggestionSpan[b]);
      if ((k == paramInt || j == paramInt) && 
        SpellChecker.haveWordBoundariesChanged((Editable)charSequence, paramInt, paramInt, j, k))
        charSequence.removeSpan(arrayOfSuggestionSpan[b]); 
    } 
  }
  
  void sendOnTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    if (this.mListeners != null) {
      ArrayList<TextWatcher> arrayList = this.mListeners;
      int i = arrayList.size();
      for (byte b = 0; b < i; b++)
        ((TextWatcher)arrayList.get(b)).onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3); 
    } 
    Editor editor = this.mEditor;
    if (editor != null)
      editor.sendOnTextChanged(paramInt1, paramInt2, paramInt3); 
  }
  
  void sendAfterTextChanged(Editable paramEditable) {
    if (this.mListeners != null) {
      ArrayList<TextWatcher> arrayList = this.mListeners;
      int i = arrayList.size();
      for (byte b = 0; b < i; b++)
        ((TextWatcher)arrayList.get(b)).afterTextChanged(paramEditable); 
    } 
    notifyListeningManagersAfterTextChanged();
    hideErrorIfUnchanged();
  }
  
  private void notifyListeningManagersAfterTextChanged() {
    if (isAutofillable()) {
      AutofillManager autofillManager = (AutofillManager)this.mContext.getSystemService(AutofillManager.class);
      if (autofillManager != null) {
        if (Helper.sVerbose)
          Log.v("TextView", "notifyAutoFillManagerAfterTextChanged"); 
        autofillManager.notifyValueChanged(this);
      } 
    } 
    if (isLaidOut() && isImportantForContentCapture() && getNotifiedContentCaptureAppeared()) {
      ContentCaptureManager contentCaptureManager = (ContentCaptureManager)this.mContext.getSystemService(ContentCaptureManager.class);
      if (contentCaptureManager != null && contentCaptureManager.isContentCaptureEnabled()) {
        ContentCaptureSession contentCaptureSession = getContentCaptureSession();
        if (contentCaptureSession != null)
          contentCaptureSession.notifyViewTextChanged(getAutofillId(), getText()); 
      } 
    } 
  }
  
  private boolean isAutofillable() {
    boolean bool;
    if (getAutofillType() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void updateAfterEdit() {
    invalidate();
    int i = getSelectionStart();
    if (i >= 0 || (this.mGravity & 0x70) == 80)
      registerForPreDraw(); 
    checkForResize();
    if (i >= 0) {
      this.mHighlightPathBogus = true;
      Editor editor = this.mEditor;
      if (editor != null)
        editor.makeBlink(); 
      bringPointIntoView(i);
    } 
  }
  
  void handleTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    Editor.InputMethodState inputMethodState;
    sLastCutCopyOrTextChangedTime = 0L;
    Editor editor = this.mEditor;
    if (editor == null) {
      editor = null;
    } else {
      inputMethodState = editor.mInputMethodState;
    } 
    if (inputMethodState == null || inputMethodState.mBatchEditNesting == 0)
      updateAfterEdit(); 
    if (inputMethodState != null) {
      inputMethodState.mContentChanged = true;
      if (inputMethodState.mChangedStart < 0) {
        inputMethodState.mChangedStart = paramInt1;
        inputMethodState.mChangedEnd = paramInt1 + paramInt2;
      } else {
        inputMethodState.mChangedStart = Math.min(inputMethodState.mChangedStart, paramInt1);
        inputMethodState.mChangedEnd = Math.max(inputMethodState.mChangedEnd, paramInt1 + paramInt2 - inputMethodState.mChangedDelta);
      } 
      inputMethodState.mChangedDelta += paramInt3 - paramInt2;
    } 
    resetErrorChangedFlag();
    sendOnTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
    onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
  }
  
  void spanChange(Spanned paramSpanned, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: iconst_0
    //   1: istore #7
    //   3: iconst_m1
    //   4: istore #8
    //   6: iconst_m1
    //   7: istore #9
    //   9: aload_0
    //   10: getfield mEditor : Landroid/widget/Editor;
    //   13: astore #10
    //   15: aload #10
    //   17: ifnonnull -> 26
    //   20: aconst_null
    //   21: astore #10
    //   23: goto -> 33
    //   26: aload #10
    //   28: getfield mInputMethodState : Landroid/widget/Editor$InputMethodState;
    //   31: astore #10
    //   33: aload_2
    //   34: getstatic android/text/Selection.SELECTION_END : Ljava/lang/Object;
    //   37: if_acmpne -> 115
    //   40: iconst_1
    //   41: istore #11
    //   43: iload #4
    //   45: istore #12
    //   47: iload_3
    //   48: ifge -> 64
    //   51: iload #11
    //   53: istore #7
    //   55: iload #12
    //   57: istore #9
    //   59: iload #4
    //   61: iflt -> 115
    //   64: aload_0
    //   65: aload_1
    //   66: invokestatic getSelectionStart : (Ljava/lang/CharSequence;)I
    //   69: iload_3
    //   70: iload #4
    //   72: invokespecial invalidateCursor : (III)V
    //   75: aload_0
    //   76: invokespecial checkForResize : ()V
    //   79: aload_0
    //   80: invokespecial registerForPreDraw : ()V
    //   83: aload_0
    //   84: getfield mEditor : Landroid/widget/Editor;
    //   87: astore #13
    //   89: iload #11
    //   91: istore #7
    //   93: iload #12
    //   95: istore #9
    //   97: aload #13
    //   99: ifnull -> 115
    //   102: aload #13
    //   104: invokevirtual makeBlink : ()V
    //   107: iload #12
    //   109: istore #9
    //   111: iload #11
    //   113: istore #7
    //   115: iload #7
    //   117: istore #12
    //   119: iload #8
    //   121: istore #7
    //   123: aload_2
    //   124: getstatic android/text/Selection.SELECTION_START : Ljava/lang/Object;
    //   127: if_acmpne -> 177
    //   130: iconst_1
    //   131: istore #11
    //   133: iload #4
    //   135: istore #8
    //   137: iload_3
    //   138: ifge -> 154
    //   141: iload #11
    //   143: istore #12
    //   145: iload #8
    //   147: istore #7
    //   149: iload #4
    //   151: iflt -> 177
    //   154: aload_1
    //   155: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   158: istore #7
    //   160: aload_0
    //   161: iload #7
    //   163: iload_3
    //   164: iload #4
    //   166: invokespecial invalidateCursor : (III)V
    //   169: iload #8
    //   171: istore #7
    //   173: iload #11
    //   175: istore #12
    //   177: iload #12
    //   179: ifeq -> 310
    //   182: aload_0
    //   183: iconst_1
    //   184: putfield mHighlightPathBogus : Z
    //   187: aload_0
    //   188: getfield mEditor : Landroid/widget/Editor;
    //   191: ifnull -> 209
    //   194: aload_0
    //   195: invokevirtual isFocused : ()Z
    //   198: ifne -> 209
    //   201: aload_0
    //   202: getfield mEditor : Landroid/widget/Editor;
    //   205: iconst_1
    //   206: putfield mSelectionMoved : Z
    //   209: aload_1
    //   210: aload_2
    //   211: invokeinterface getSpanFlags : (Ljava/lang/Object;)I
    //   216: sipush #512
    //   219: iand
    //   220: ifne -> 310
    //   223: iload #7
    //   225: istore #12
    //   227: iload #7
    //   229: ifge -> 238
    //   232: aload_1
    //   233: invokestatic getSelectionStart : (Ljava/lang/CharSequence;)I
    //   236: istore #12
    //   238: iload #9
    //   240: istore #7
    //   242: iload #9
    //   244: ifge -> 253
    //   247: aload_1
    //   248: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   251: istore #7
    //   253: aload_0
    //   254: getfield mEditor : Landroid/widget/Editor;
    //   257: astore #13
    //   259: aload #13
    //   261: ifnull -> 302
    //   264: aload #13
    //   266: invokevirtual refreshTextActionMode : ()V
    //   269: aload_0
    //   270: invokevirtual hasSelection : ()Z
    //   273: ifne -> 302
    //   276: aload_0
    //   277: getfield mEditor : Landroid/widget/Editor;
    //   280: astore #13
    //   282: aload #13
    //   284: invokevirtual getTextActionMode : ()Landroid/view/ActionMode;
    //   287: ifnonnull -> 302
    //   290: aload_0
    //   291: invokevirtual hasTransientState : ()Z
    //   294: ifeq -> 302
    //   297: aload_0
    //   298: iconst_0
    //   299: invokevirtual setHasTransientState : (Z)V
    //   302: aload_0
    //   303: iload #12
    //   305: iload #7
    //   307: invokevirtual onSelectionChanged : (II)V
    //   310: aload_2
    //   311: instanceof android/text/style/UpdateAppearance
    //   314: ifne -> 331
    //   317: aload_2
    //   318: instanceof android/text/style/ParagraphStyle
    //   321: ifne -> 331
    //   324: aload_2
    //   325: instanceof android/text/style/CharacterStyle
    //   328: ifeq -> 423
    //   331: aload #10
    //   333: ifnull -> 356
    //   336: aload #10
    //   338: getfield mBatchEditNesting : I
    //   341: ifne -> 347
    //   344: goto -> 356
    //   347: aload #10
    //   349: iconst_1
    //   350: putfield mContentChanged : Z
    //   353: goto -> 369
    //   356: aload_0
    //   357: invokevirtual invalidate : ()V
    //   360: aload_0
    //   361: iconst_1
    //   362: putfield mHighlightPathBogus : Z
    //   365: aload_0
    //   366: invokespecial checkForResize : ()V
    //   369: aload_0
    //   370: getfield mEditor : Landroid/widget/Editor;
    //   373: astore #13
    //   375: aload #13
    //   377: ifnull -> 423
    //   380: iload_3
    //   381: iflt -> 396
    //   384: aload #13
    //   386: aload_0
    //   387: getfield mLayout : Landroid/text/Layout;
    //   390: iload_3
    //   391: iload #5
    //   393: invokevirtual invalidateTextDisplayList : (Landroid/text/Layout;II)V
    //   396: iload #4
    //   398: iflt -> 416
    //   401: aload_0
    //   402: getfield mEditor : Landroid/widget/Editor;
    //   405: aload_0
    //   406: getfield mLayout : Landroid/text/Layout;
    //   409: iload #4
    //   411: iload #6
    //   413: invokevirtual invalidateTextDisplayList : (Landroid/text/Layout;II)V
    //   416: aload_0
    //   417: getfield mEditor : Landroid/widget/Editor;
    //   420: invokevirtual invalidateHandlesAndActionMode : ()V
    //   423: aload_1
    //   424: aload_2
    //   425: invokestatic isMetaTracker : (Ljava/lang/CharSequence;Ljava/lang/Object;)Z
    //   428: ifeq -> 491
    //   431: aload_0
    //   432: iconst_1
    //   433: putfield mHighlightPathBogus : Z
    //   436: aload #10
    //   438: ifnull -> 455
    //   441: aload_1
    //   442: aload_2
    //   443: invokestatic isSelectingMetaTracker : (Ljava/lang/CharSequence;Ljava/lang/Object;)Z
    //   446: ifeq -> 455
    //   449: aload #10
    //   451: iconst_1
    //   452: putfield mSelectionModeChanged : Z
    //   455: aload_1
    //   456: invokestatic getSelectionStart : (Ljava/lang/CharSequence;)I
    //   459: iflt -> 491
    //   462: aload #10
    //   464: ifnull -> 487
    //   467: aload #10
    //   469: getfield mBatchEditNesting : I
    //   472: ifne -> 478
    //   475: goto -> 487
    //   478: aload #10
    //   480: iconst_1
    //   481: putfield mCursorChanged : Z
    //   484: goto -> 491
    //   487: aload_0
    //   488: invokevirtual invalidateCursor : ()V
    //   491: aload_2
    //   492: instanceof android/text/ParcelableSpan
    //   495: ifeq -> 603
    //   498: aload #10
    //   500: ifnull -> 603
    //   503: aload #10
    //   505: getfield mExtractedTextRequest : Landroid/view/inputmethod/ExtractedTextRequest;
    //   508: ifnull -> 603
    //   511: aload #10
    //   513: getfield mBatchEditNesting : I
    //   516: ifeq -> 597
    //   519: iload_3
    //   520: iflt -> 555
    //   523: aload #10
    //   525: getfield mChangedStart : I
    //   528: iload_3
    //   529: if_icmple -> 538
    //   532: aload #10
    //   534: iload_3
    //   535: putfield mChangedStart : I
    //   538: aload #10
    //   540: getfield mChangedStart : I
    //   543: iload #5
    //   545: if_icmple -> 555
    //   548: aload #10
    //   550: iload #5
    //   552: putfield mChangedStart : I
    //   555: iload #4
    //   557: iflt -> 603
    //   560: aload #10
    //   562: getfield mChangedStart : I
    //   565: iload #4
    //   567: if_icmple -> 577
    //   570: aload #10
    //   572: iload #4
    //   574: putfield mChangedStart : I
    //   577: aload #10
    //   579: getfield mChangedStart : I
    //   582: iload #6
    //   584: if_icmple -> 603
    //   587: aload #10
    //   589: iload #6
    //   591: putfield mChangedStart : I
    //   594: goto -> 603
    //   597: aload #10
    //   599: iconst_1
    //   600: putfield mContentChanged : Z
    //   603: aload_0
    //   604: getfield mEditor : Landroid/widget/Editor;
    //   607: astore_1
    //   608: aload_1
    //   609: ifnull -> 645
    //   612: aload_1
    //   613: getfield mSpellChecker : Landroid/widget/SpellChecker;
    //   616: ifnull -> 645
    //   619: iload #4
    //   621: ifge -> 645
    //   624: aload_2
    //   625: instanceof android/text/style/SpellCheckSpan
    //   628: ifeq -> 645
    //   631: aload_0
    //   632: getfield mEditor : Landroid/widget/Editor;
    //   635: getfield mSpellChecker : Landroid/widget/SpellChecker;
    //   638: aload_2
    //   639: checkcast android/text/style/SpellCheckSpan
    //   642: invokevirtual onSpellCheckSpanRemoved : (Landroid/text/style/SpellCheckSpan;)V
    //   645: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #10807	-> 0
    //   #10808	-> 3
    //   #10810	-> 9
    //   #10812	-> 33
    //   #10813	-> 40
    //   #10814	-> 43
    //   #10816	-> 47
    //   #10817	-> 64
    //   #10818	-> 75
    //   #10819	-> 79
    //   #10820	-> 83
    //   #10824	-> 115
    //   #10825	-> 130
    //   #10826	-> 133
    //   #10828	-> 137
    //   #10829	-> 154
    //   #10830	-> 160
    //   #10834	-> 177
    //   #10835	-> 182
    //   #10836	-> 187
    //   #10838	-> 209
    //   #10839	-> 223
    //   #10840	-> 232
    //   #10842	-> 238
    //   #10843	-> 247
    //   #10846	-> 253
    //   #10847	-> 264
    //   #10848	-> 269
    //   #10849	-> 282
    //   #10851	-> 297
    //   #10854	-> 302
    //   #10858	-> 310
    //   #10860	-> 331
    //   #10865	-> 347
    //   #10861	-> 356
    //   #10862	-> 360
    //   #10863	-> 365
    //   #10867	-> 369
    //   #10868	-> 380
    //   #10869	-> 396
    //   #10870	-> 416
    //   #10874	-> 423
    //   #10875	-> 431
    //   #10876	-> 436
    //   #10877	-> 449
    //   #10880	-> 455
    //   #10881	-> 462
    //   #10884	-> 478
    //   #10882	-> 487
    //   #10889	-> 491
    //   #10892	-> 498
    //   #10893	-> 511
    //   #10894	-> 519
    //   #10895	-> 523
    //   #10896	-> 532
    //   #10898	-> 538
    //   #10899	-> 548
    //   #10902	-> 555
    //   #10903	-> 560
    //   #10904	-> 570
    //   #10906	-> 577
    //   #10907	-> 587
    //   #10916	-> 597
    //   #10921	-> 603
    //   #10923	-> 631
    //   #10925	-> 645
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    if (isTemporarilyDetached()) {
      super.onFocusChanged(paramBoolean, paramInt, paramRect);
      return;
    } 
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onFocusChanged(paramBoolean, paramInt); 
    if (paramBoolean) {
      Spannable spannable = this.mSpannable;
      if (spannable != null)
        MetaKeyKeyListener.resetMetaState(spannable); 
    } 
    startStopMarquee(paramBoolean);
    TransformationMethod transformationMethod = this.mTransformation;
    if (transformationMethod != null)
      transformationMethod.onFocusChanged(this, this.mText, paramBoolean, paramInt, paramRect); 
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    super.onWindowFocusChanged(paramBoolean);
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onWindowFocusChanged(paramBoolean); 
    startStopMarquee(paramBoolean);
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt) {
    super.onVisibilityChanged(paramView, paramInt);
    Editor editor = this.mEditor;
    if (editor != null && paramInt != 0) {
      editor.hideCursorAndSpanControllers();
      stopTextActionMode();
    } 
  }
  
  public void clearComposingText() {
    if (this.mText instanceof Spannable)
      BaseInputConnection.removeComposingSpans(this.mSpannable); 
  }
  
  public void setSelected(boolean paramBoolean) {
    boolean bool = isSelected();
    super.setSelected(paramBoolean);
    if (paramBoolean != bool && this.mEllipsize == TextUtils.TruncateAt.MARQUEE)
      if (paramBoolean) {
        startMarquee();
      } else {
        stopMarquee();
      }  
  }
  
  boolean isFromPrimePointer(MotionEvent paramMotionEvent, boolean paramBoolean) {
    boolean bool1 = true;
    int i = this.mPrimePointerId;
    boolean bool2 = false;
    if (i == -1) {
      this.mPrimePointerId = paramMotionEvent.getPointerId(0);
      this.mIsPrimePointerFromHandleView = paramBoolean;
    } else if (i != paramMotionEvent.getPointerId(0)) {
      bool1 = bool2;
      if (this.mIsPrimePointerFromHandleView) {
        bool1 = bool2;
        if (paramBoolean)
          bool1 = true; 
      } 
    } 
    if (paramMotionEvent.getActionMasked() == 1 || 
      paramMotionEvent.getActionMasked() == 3)
      this.mPrimePointerId = -1; 
    return bool1;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getActionMasked : ()I
    //   4: istore_2
    //   5: aload_0
    //   6: getfield mEditor : Landroid/widget/Editor;
    //   9: ifnull -> 85
    //   12: aload_0
    //   13: aload_1
    //   14: iconst_0
    //   15: invokevirtual isFromPrimePointer : (Landroid/view/MotionEvent;Z)Z
    //   18: ifne -> 23
    //   21: iconst_1
    //   22: ireturn
    //   23: aload_0
    //   24: getfield mEditor : Landroid/widget/Editor;
    //   27: aload_1
    //   28: invokevirtual onTouchEvent : (Landroid/view/MotionEvent;)V
    //   31: aload_0
    //   32: getfield mEditor : Landroid/widget/Editor;
    //   35: getfield mInsertionPointCursorController : Landroid/widget/Editor$InsertionPointCursorController;
    //   38: ifnull -> 58
    //   41: aload_0
    //   42: getfield mEditor : Landroid/widget/Editor;
    //   45: getfield mInsertionPointCursorController : Landroid/widget/Editor$InsertionPointCursorController;
    //   48: astore_3
    //   49: aload_3
    //   50: invokevirtual isCursorBeingModified : ()Z
    //   53: ifeq -> 58
    //   56: iconst_1
    //   57: ireturn
    //   58: aload_0
    //   59: getfield mEditor : Landroid/widget/Editor;
    //   62: getfield mSelectionModifierCursorController : Landroid/widget/Editor$SelectionModifierCursorController;
    //   65: ifnull -> 85
    //   68: aload_0
    //   69: getfield mEditor : Landroid/widget/Editor;
    //   72: getfield mSelectionModifierCursorController : Landroid/widget/Editor$SelectionModifierCursorController;
    //   75: astore_3
    //   76: aload_3
    //   77: invokevirtual isDragAcceleratorActive : ()Z
    //   80: ifeq -> 85
    //   83: iconst_1
    //   84: ireturn
    //   85: aload_0
    //   86: aload_1
    //   87: invokespecial onTouchEvent : (Landroid/view/MotionEvent;)Z
    //   90: istore #4
    //   92: aload_0
    //   93: getfield mEditor : Landroid/widget/Editor;
    //   96: astore_3
    //   97: aload_3
    //   98: ifnull -> 149
    //   101: aload_3
    //   102: getfield mDiscardNextActionUp : Z
    //   105: ifeq -> 149
    //   108: iload_2
    //   109: iconst_1
    //   110: if_icmpne -> 149
    //   113: aload_0
    //   114: getfield mEditor : Landroid/widget/Editor;
    //   117: iconst_0
    //   118: putfield mDiscardNextActionUp : Z
    //   121: aload_0
    //   122: getfield mEditor : Landroid/widget/Editor;
    //   125: getfield mIsInsertionActionModeStartPending : Z
    //   128: ifeq -> 146
    //   131: aload_0
    //   132: getfield mEditor : Landroid/widget/Editor;
    //   135: invokevirtual startInsertionActionMode : ()V
    //   138: aload_0
    //   139: getfield mEditor : Landroid/widget/Editor;
    //   142: iconst_0
    //   143: putfield mIsInsertionActionModeStartPending : Z
    //   146: iload #4
    //   148: ireturn
    //   149: iload_2
    //   150: iconst_1
    //   151: if_icmpne -> 183
    //   154: aload_0
    //   155: getfield mEditor : Landroid/widget/Editor;
    //   158: astore_3
    //   159: aload_3
    //   160: ifnull -> 170
    //   163: aload_3
    //   164: getfield mIgnoreActionUpEvent : Z
    //   167: ifne -> 183
    //   170: aload_0
    //   171: invokevirtual isFocused : ()Z
    //   174: ifeq -> 183
    //   177: iconst_1
    //   178: istore #5
    //   180: goto -> 186
    //   183: iconst_0
    //   184: istore #5
    //   186: aload_0
    //   187: getfield mMovement : Landroid/text/method/MovementMethod;
    //   190: ifnonnull -> 200
    //   193: aload_0
    //   194: invokevirtual onCheckIsTextEditor : ()Z
    //   197: ifeq -> 421
    //   200: aload_0
    //   201: invokevirtual isEnabled : ()Z
    //   204: ifeq -> 421
    //   207: aload_0
    //   208: getfield mText : Ljava/lang/CharSequence;
    //   211: instanceof android/text/Spannable
    //   214: ifeq -> 421
    //   217: aload_0
    //   218: getfield mLayout : Landroid/text/Layout;
    //   221: ifnull -> 421
    //   224: iconst_0
    //   225: istore_2
    //   226: aload_0
    //   227: getfield mMovement : Landroid/text/method/MovementMethod;
    //   230: astore_3
    //   231: aload_3
    //   232: ifnull -> 250
    //   235: iconst_0
    //   236: aload_3
    //   237: aload_0
    //   238: aload_0
    //   239: getfield mSpannable : Landroid/text/Spannable;
    //   242: aload_1
    //   243: invokeinterface onTouchEvent : (Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    //   248: ior
    //   249: istore_2
    //   250: aload_0
    //   251: invokevirtual isTextSelectable : ()Z
    //   254: istore #6
    //   256: iload_2
    //   257: istore #7
    //   259: iload #5
    //   261: ifeq -> 344
    //   264: iload_2
    //   265: istore #7
    //   267: aload_0
    //   268: getfield mLinksClickable : Z
    //   271: ifeq -> 344
    //   274: iload_2
    //   275: istore #7
    //   277: aload_0
    //   278: getfield mAutoLinkMask : I
    //   281: ifeq -> 344
    //   284: iload_2
    //   285: istore #7
    //   287: iload #6
    //   289: ifeq -> 344
    //   292: aload_0
    //   293: getfield mSpannable : Landroid/text/Spannable;
    //   296: astore_3
    //   297: aload_0
    //   298: invokevirtual getSelectionStart : ()I
    //   301: istore #7
    //   303: aload_0
    //   304: invokevirtual getSelectionEnd : ()I
    //   307: istore #8
    //   309: aload_3
    //   310: iload #7
    //   312: iload #8
    //   314: ldc_w android/text/style/ClickableSpan
    //   317: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   322: checkcast [Landroid/text/style/ClickableSpan;
    //   325: astore_3
    //   326: iload_2
    //   327: istore #7
    //   329: aload_3
    //   330: arraylength
    //   331: ifle -> 344
    //   334: aload_3
    //   335: iconst_0
    //   336: aaload
    //   337: aload_0
    //   338: invokevirtual onClick : (Landroid/view/View;)V
    //   341: iconst_1
    //   342: istore #7
    //   344: iload #7
    //   346: istore_2
    //   347: iload #5
    //   349: ifeq -> 415
    //   352: aload_0
    //   353: invokevirtual isTextEditable : ()Z
    //   356: ifne -> 367
    //   359: iload #7
    //   361: istore_2
    //   362: iload #6
    //   364: ifeq -> 415
    //   367: aload_0
    //   368: invokespecial getInputMethodManager : ()Landroid/view/inputmethod/InputMethodManager;
    //   371: astore_3
    //   372: aload_0
    //   373: aload_3
    //   374: invokevirtual viewClicked : (Landroid/view/inputmethod/InputMethodManager;)V
    //   377: aload_0
    //   378: invokevirtual isTextEditable : ()Z
    //   381: ifeq -> 405
    //   384: aload_0
    //   385: getfield mEditor : Landroid/widget/Editor;
    //   388: getfield mShowSoftInputOnFocus : Z
    //   391: ifeq -> 405
    //   394: aload_3
    //   395: ifnull -> 405
    //   398: aload_3
    //   399: aload_0
    //   400: iconst_0
    //   401: invokevirtual showSoftInput : (Landroid/view/View;I)Z
    //   404: pop
    //   405: aload_0
    //   406: getfield mEditor : Landroid/widget/Editor;
    //   409: aload_1
    //   410: invokevirtual onTouchUpEvent : (Landroid/view/MotionEvent;)V
    //   413: iconst_1
    //   414: istore_2
    //   415: iload_2
    //   416: ifeq -> 421
    //   419: iconst_1
    //   420: ireturn
    //   421: iload #4
    //   423: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #11053	-> 0
    //   #11054	-> 5
    //   #11055	-> 12
    //   #11056	-> 21
    //   #11059	-> 23
    //   #11061	-> 31
    //   #11062	-> 49
    //   #11063	-> 56
    //   #11065	-> 58
    //   #11066	-> 76
    //   #11067	-> 83
    //   #11071	-> 85
    //   #11081	-> 92
    //   #11082	-> 113
    //   #11086	-> 121
    //   #11087	-> 131
    //   #11088	-> 138
    //   #11090	-> 146
    //   #11093	-> 149
    //   #11094	-> 170
    //   #11096	-> 186
    //   #11098	-> 224
    //   #11100	-> 226
    //   #11101	-> 235
    //   #11104	-> 250
    //   #11105	-> 256
    //   #11109	-> 292
    //   #11110	-> 303
    //   #11109	-> 309
    //   #11112	-> 326
    //   #11113	-> 334
    //   #11114	-> 341
    //   #11118	-> 344
    //   #11120	-> 367
    //   #11121	-> 372
    //   #11122	-> 377
    //   #11123	-> 398
    //   #11127	-> 405
    //   #11129	-> 413
    //   #11132	-> 415
    //   #11133	-> 419
    //   #11137	-> 421
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    MovementMethod movementMethod = this.mMovement;
    if (movementMethod != null && this.mText instanceof Spannable && this.mLayout != null)
      try {
        boolean bool = movementMethod.onGenericMotionEvent(this, this.mSpannable, paramMotionEvent);
        if (bool)
          return true; 
      } catch (AbstractMethodError abstractMethodError) {} 
    return super.onGenericMotionEvent(paramMotionEvent);
  }
  
  protected void onCreateContextMenu(ContextMenu paramContextMenu) {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onCreateContextMenu(paramContextMenu); 
  }
  
  public boolean showContextMenu() {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.setContextMenuAnchor(Float.NaN, Float.NaN); 
    return super.showContextMenu();
  }
  
  public boolean showContextMenu(float paramFloat1, float paramFloat2) {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.setContextMenuAnchor(paramFloat1, paramFloat2); 
    return super.showContextMenu(paramFloat1, paramFloat2);
  }
  
  boolean isTextEditable() {
    boolean bool;
    if (this.mText instanceof Editable && onCheckIsTextEditor() && isEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean didTouchFocusSelect() {
    boolean bool;
    Editor editor = this.mEditor;
    if (editor != null && editor.mTouchFocusSelected) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void cancelLongPress() {
    super.cancelLongPress();
    Editor editor = this.mEditor;
    if (editor != null)
      editor.mIgnoreActionUpEvent = true; 
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    MovementMethod movementMethod = this.mMovement;
    if (movementMethod != null) {
      Spannable spannable = this.mSpannable;
      if (spannable != null && this.mLayout != null && 
        movementMethod.onTrackballEvent(this, spannable, paramMotionEvent))
        return true; 
    } 
    return super.onTrackballEvent(paramMotionEvent);
  }
  
  public void setScroller(Scroller paramScroller) {
    this.mScroller = paramScroller;
  }
  
  protected float getLeftFadingEdgeStrength() {
    if (isMarqueeFadeEnabled()) {
      Marquee marquee = this.mMarquee;
      if (marquee != null && !marquee.isStopped()) {
        marquee = this.mMarquee;
        if (marquee.shouldDrawLeftFade())
          return getHorizontalFadingEdgeStrength(marquee.getScroll(), 0.0F); 
        return 0.0F;
      } 
    } 
    if (getLineCount() == 1) {
      float f = getLayout().getLineLeft(0);
      if (f > this.mScrollX)
        return 0.0F; 
      return getHorizontalFadingEdgeStrength(this.mScrollX, f);
    } 
    return super.getLeftFadingEdgeStrength();
  }
  
  protected float getRightFadingEdgeStrength() {
    if (isMarqueeFadeEnabled()) {
      Marquee marquee = this.mMarquee;
      if (marquee != null && !marquee.isStopped()) {
        marquee = this.mMarquee;
        return getHorizontalFadingEdgeStrength(marquee.getMaxFadeScroll(), marquee.getScroll());
      } 
    } 
    if (getLineCount() == 1) {
      int i = this.mScrollX;
      float f1 = (i + getWidth() - getCompoundPaddingLeft() - getCompoundPaddingRight());
      float f2 = getLayout().getLineRight(0);
      if (f2 < f1)
        return 0.0F; 
      return getHorizontalFadingEdgeStrength(f1, f2);
    } 
    return super.getRightFadingEdgeStrength();
  }
  
  private float getHorizontalFadingEdgeStrength(float paramFloat1, float paramFloat2) {
    int i = getHorizontalFadingEdgeLength();
    if (i == 0)
      return 0.0F; 
    paramFloat1 = Math.abs(paramFloat1 - paramFloat2);
    if (paramFloat1 > i)
      return 1.0F; 
    return paramFloat1 / i;
  }
  
  private boolean isMarqueeFadeEnabled() {
    TextUtils.TruncateAt truncateAt1 = this.mEllipsize, truncateAt2 = TextUtils.TruncateAt.MARQUEE;
    boolean bool = true;
    if (truncateAt1 != truncateAt2 || this.mMarqueeFadeMode == 1)
      bool = false; 
    return bool;
  }
  
  protected int computeHorizontalScrollRange() {
    Layout layout = this.mLayout;
    if (layout != null) {
      int i;
      if (this.mSingleLine && (this.mGravity & 0x7) == 3) {
        i = (int)layout.getLineWidth(0);
      } else {
        i = this.mLayout.getWidth();
      } 
      return i;
    } 
    return super.computeHorizontalScrollRange();
  }
  
  protected int computeVerticalScrollRange() {
    Layout layout = this.mLayout;
    if (layout != null)
      return layout.getHeight(); 
    return super.computeVerticalScrollRange();
  }
  
  protected int computeVerticalScrollExtent() {
    return getHeight() - getCompoundPaddingTop() - getCompoundPaddingBottom();
  }
  
  public void findViewsWithText(ArrayList<View> paramArrayList, CharSequence paramCharSequence, int paramInt) {
    super.findViewsWithText(paramArrayList, paramCharSequence, paramInt);
    if (!paramArrayList.contains(this) && (paramInt & 0x1) != 0 && 
      !TextUtils.isEmpty(paramCharSequence) && !TextUtils.isEmpty(this.mText)) {
      String str = paramCharSequence.toString().toLowerCase();
      paramCharSequence = this.mText.toString().toLowerCase();
      if (paramCharSequence.contains(str))
        paramArrayList.add(this); 
    } 
  }
  
  class BufferType extends Enum<BufferType> {
    private static final BufferType[] $VALUES;
    
    public static final BufferType EDITABLE;
    
    public static BufferType[] values() {
      return (BufferType[])$VALUES.clone();
    }
    
    public static BufferType valueOf(String param1String) {
      return Enum.<BufferType>valueOf(BufferType.class, param1String);
    }
    
    private BufferType(TextView this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final BufferType NORMAL = new BufferType("NORMAL", 0), SPANNABLE = new BufferType("SPANNABLE", 1);
    
    static {
      BufferType bufferType = new BufferType("EDITABLE", 2);
      $VALUES = new BufferType[] { NORMAL, SPANNABLE, bufferType };
    }
  }
  
  public static ColorStateList getTextColors(Context paramContext, TypedArray paramTypedArray) {
    if (paramTypedArray != null) {
      TypedArray typedArray = paramContext.obtainStyledAttributes(R.styleable.TextView);
      ColorStateList colorStateList2 = typedArray.getColorStateList(5);
      ColorStateList colorStateList1 = colorStateList2;
      if (colorStateList2 == null) {
        int i = typedArray.getResourceId(1, 0);
        colorStateList1 = colorStateList2;
        if (i != 0) {
          TypedArray typedArray1 = paramContext.obtainStyledAttributes(i, R.styleable.TextAppearance);
          colorStateList1 = typedArray1.getColorStateList(3);
          typedArray1.recycle();
        } 
      } 
      typedArray.recycle();
      return colorStateList1;
    } 
    throw null;
  }
  
  public static int getTextColor(Context paramContext, TypedArray paramTypedArray, int paramInt) {
    ColorStateList colorStateList = getTextColors(paramContext, paramTypedArray);
    if (colorStateList == null)
      return paramInt; 
    return colorStateList.getDefaultColor();
  }
  
  public boolean onKeyShortcut(int paramInt, KeyEvent paramKeyEvent) {
    if (paramKeyEvent.hasModifiers(4096)) {
      if (paramInt != 29) {
        if (paramInt != 31) {
          if (paramInt != 50) {
            if (paramInt != 52) {
              if (paramInt == 54)
                if (canUndo())
                  return onTextContextMenuItem(16908338);  
            } else if (canCut()) {
              return onTextContextMenuItem(16908320);
            } 
          } else if (canPaste()) {
            return onTextContextMenuItem(16908322);
          } 
        } else if (canCopy()) {
          return onTextContextMenuItem(16908321);
        } 
      } else if (canSelectText()) {
        return onTextContextMenuItem(16908319);
      } 
    } else if (paramKeyEvent.hasModifiers(4097)) {
      if (paramInt != 50) {
        if (paramInt == 54)
          if (canRedo())
            return onTextContextMenuItem(16908339);  
      } else if (canPaste()) {
        return onTextContextMenuItem(16908337);
      } 
    } 
    return super.onKeyShortcut(paramInt, paramKeyEvent);
  }
  
  boolean canSelectText() {
    if (this.mText.length() != 0) {
      Editor editor = this.mEditor;
      if (editor != null && editor.hasSelectionController())
        return true; 
    } 
    return false;
  }
  
  boolean textCanBeSelected() {
    MovementMethod movementMethod = this.mMovement;
    boolean bool = false;
    if (movementMethod == null || !movementMethod.canSelectArbitrarily())
      return false; 
    if (!isTextEditable()) {
      boolean bool1 = bool;
      if (isTextSelectable()) {
        bool1 = bool;
        if (this.mText instanceof Spannable) {
          bool1 = bool;
          if (isEnabled())
            return true; 
        } 
      } 
      return bool1;
    } 
    return true;
  }
  
  private Locale getTextServicesLocale(boolean paramBoolean) {
    Locale locale;
    updateTextServicesLocaleAsync();
    if (this.mCurrentSpellCheckerLocaleCache == null && !paramBoolean) {
      locale = Locale.getDefault();
    } else {
      locale = this.mCurrentSpellCheckerLocaleCache;
    } 
    return locale;
  }
  
  public final void setTextOperationUser(UserHandle paramUserHandle) {
    if (Objects.equals(this.mTextOperationUser, paramUserHandle))
      return; 
    if (paramUserHandle != null && !Process.myUserHandle().equals(paramUserHandle))
      if (getContext().checkSelfPermission("android.permission.INTERACT_ACROSS_USERS_FULL") != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INTERACT_ACROSS_USERS_FULL is required. userId=");
        stringBuilder.append(paramUserHandle.getIdentifier());
        stringBuilder.append(" callingUserId");
        stringBuilder.append(UserHandle.myUserId());
        throw new SecurityException(stringBuilder.toString());
      }  
    this.mTextOperationUser = paramUserHandle;
    this.mCurrentSpellCheckerLocaleCache = null;
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onTextOperationUserChanged(); 
  }
  
  final TextServicesManager getTextServicesManagerForUser() {
    return getServiceManagerForUser("android", TextServicesManager.class);
  }
  
  final ClipboardManager getClipboardManagerForUser() {
    return getServiceManagerForUser(getContext().getPackageName(), ClipboardManager.class);
  }
  
  final TextClassificationManager getTextClassificationManagerForUser() {
    String str = getContext().getPackageName();
    return getServiceManagerForUser(str, TextClassificationManager.class);
  }
  
  final <T> T getServiceManagerForUser(String paramString, Class<T> paramClass) {
    if (this.mTextOperationUser == null)
      return (T)getContext().getSystemService(paramClass); 
    try {
      Context context = getContext().createPackageContextAsUser(paramString, 0, this.mTextOperationUser);
      return (T)context.getSystemService(paramClass);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      return null;
    } 
  }
  
  void startActivityAsTextOperationUserIfNecessary(Intent paramIntent) {
    if (this.mTextOperationUser != null) {
      getContext().startActivityAsUser(paramIntent, this.mTextOperationUser);
    } else {
      getContext().startActivity(paramIntent);
    } 
  }
  
  public Locale getTextServicesLocale() {
    return getTextServicesLocale(false);
  }
  
  public boolean isInExtractedMode() {
    return false;
  }
  
  private boolean isAutoSizeEnabled() {
    boolean bool;
    if (supportsAutoSizeText() && this.mAutoSizeTextType != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected boolean supportsAutoSizeText() {
    return true;
  }
  
  public Locale getSpellCheckerLocale() {
    return getTextServicesLocale(true);
  }
  
  private void updateTextServicesLocaleAsync() {
    AsyncTask.execute((Runnable)new Object(this));
  }
  
  private void updateTextServicesLocaleLocked() {
    TextServicesManager textServicesManager = getTextServicesManagerForUser();
    if (textServicesManager == null)
      return; 
    SpellCheckerSubtype spellCheckerSubtype = textServicesManager.getCurrentSpellCheckerSubtype(true);
    if (spellCheckerSubtype != null) {
      Locale locale = spellCheckerSubtype.getLocaleObject();
    } else {
      spellCheckerSubtype = null;
    } 
    this.mCurrentSpellCheckerLocaleCache = (Locale)spellCheckerSubtype;
  }
  
  void onLocaleChanged() {
    this.mEditor.onLocaleChanged();
  }
  
  public WordIterator getWordIterator() {
    Editor editor = this.mEditor;
    if (editor != null)
      return editor.getWordIterator(); 
    return null;
  }
  
  public void onPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onPopulateAccessibilityEventInternal(paramAccessibilityEvent);
    CharSequence charSequence = getTextForAccessibility();
    if (!TextUtils.isEmpty(charSequence))
      paramAccessibilityEvent.getText().add(charSequence); 
  }
  
  public CharSequence getAccessibilityClassName() {
    return TextView.class.getName();
  }
  
  protected void onProvideStructure(ViewStructure paramViewStructure, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: iload_2
    //   3: iload_3
    //   4: invokespecial onProvideStructure : (Landroid/view/ViewStructure;II)V
    //   7: aload_0
    //   8: invokevirtual hasPasswordTransformationMethod : ()Z
    //   11: ifne -> 32
    //   14: aload_0
    //   15: invokevirtual getInputType : ()I
    //   18: invokestatic isPasswordInputType : (I)Z
    //   21: ifeq -> 27
    //   24: goto -> 32
    //   27: iconst_0
    //   28: istore_3
    //   29: goto -> 34
    //   32: iconst_1
    //   33: istore_3
    //   34: iload_2
    //   35: iconst_1
    //   36: if_icmpeq -> 44
    //   39: iload_2
    //   40: iconst_2
    //   41: if_icmpne -> 155
    //   44: iload_2
    //   45: iconst_1
    //   46: if_icmpne -> 59
    //   49: aload_1
    //   50: aload_0
    //   51: getfield mTextSetFromXmlOrResourceId : Z
    //   54: iconst_1
    //   55: ixor
    //   56: invokevirtual setDataIsSensitive : (Z)V
    //   59: aload_0
    //   60: getfield mTextId : I
    //   63: ifeq -> 155
    //   66: aload_1
    //   67: aload_0
    //   68: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   71: aload_0
    //   72: getfield mTextId : I
    //   75: invokevirtual getResourceEntryName : (I)Ljava/lang/String;
    //   78: invokevirtual setTextIdEntry : (Ljava/lang/String;)V
    //   81: goto -> 155
    //   84: astore #4
    //   86: getstatic android/view/autofill/Helper.sVerbose : Z
    //   89: ifeq -> 155
    //   92: new java/lang/StringBuilder
    //   95: dup
    //   96: invokespecial <init> : ()V
    //   99: astore #5
    //   101: aload #5
    //   103: ldc_w 'onProvideAutofillStructure(): cannot set name for text id '
    //   106: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   109: pop
    //   110: aload #5
    //   112: aload_0
    //   113: getfield mTextId : I
    //   116: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   119: pop
    //   120: aload #5
    //   122: ldc_w ': '
    //   125: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   128: pop
    //   129: aload #5
    //   131: aload #4
    //   133: invokevirtual getMessage : ()Ljava/lang/String;
    //   136: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   139: pop
    //   140: aload #5
    //   142: invokevirtual toString : ()Ljava/lang/String;
    //   145: astore #4
    //   147: ldc 'TextView'
    //   149: aload #4
    //   151: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   154: pop
    //   155: iload_3
    //   156: ifeq -> 175
    //   159: iload_2
    //   160: iconst_1
    //   161: if_icmpeq -> 175
    //   164: iload_2
    //   165: iconst_2
    //   166: if_icmpne -> 172
    //   169: goto -> 175
    //   172: goto -> 860
    //   175: aload_0
    //   176: getfield mLayout : Landroid/text/Layout;
    //   179: ifnonnull -> 200
    //   182: iload_2
    //   183: iconst_2
    //   184: if_icmpne -> 196
    //   187: ldc 'TextView'
    //   189: ldc_w 'onProvideContentCaptureStructure(): calling assumeLayout()'
    //   192: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   195: pop
    //   196: aload_0
    //   197: invokespecial assumeLayout : ()V
    //   200: aload_0
    //   201: getfield mLayout : Landroid/text/Layout;
    //   204: astore #6
    //   206: aload #6
    //   208: invokevirtual getLineCount : ()I
    //   211: istore #7
    //   213: iload #7
    //   215: iconst_1
    //   216: if_icmpgt -> 256
    //   219: aload_0
    //   220: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   223: astore #4
    //   225: iload_2
    //   226: iconst_1
    //   227: if_icmpne -> 239
    //   230: aload_1
    //   231: aload #4
    //   233: invokevirtual setText : (Ljava/lang/CharSequence;)V
    //   236: goto -> 253
    //   239: aload_1
    //   240: aload #4
    //   242: aload_0
    //   243: invokevirtual getSelectionStart : ()I
    //   246: aload_0
    //   247: invokevirtual getSelectionEnd : ()I
    //   250: invokevirtual setText : (Ljava/lang/CharSequence;II)V
    //   253: goto -> 648
    //   256: iconst_2
    //   257: newarray int
    //   259: astore #4
    //   261: aload_0
    //   262: aload #4
    //   264: invokevirtual getLocationInWindow : ([I)V
    //   267: aload #4
    //   269: iconst_1
    //   270: iaload
    //   271: istore #8
    //   273: aload_0
    //   274: astore #4
    //   276: aload_0
    //   277: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   280: astore #5
    //   282: aload #5
    //   284: instanceof android/view/View
    //   287: ifeq -> 307
    //   290: aload #5
    //   292: checkcast android/view/View
    //   295: astore #4
    //   297: aload #4
    //   299: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   302: astore #5
    //   304: goto -> 282
    //   307: aload #4
    //   309: invokevirtual getHeight : ()I
    //   312: istore #9
    //   314: iload #8
    //   316: iflt -> 339
    //   319: aload_0
    //   320: fconst_0
    //   321: invokevirtual getLineAtCoordinateUnclamped : (F)I
    //   324: istore_3
    //   325: aload_0
    //   326: iload #9
    //   328: iconst_1
    //   329: isub
    //   330: i2f
    //   331: invokevirtual getLineAtCoordinateUnclamped : (F)I
    //   334: istore #8
    //   336: goto -> 362
    //   339: aload_0
    //   340: iload #8
    //   342: ineg
    //   343: i2f
    //   344: invokevirtual getLineAtCoordinateUnclamped : (F)I
    //   347: istore_3
    //   348: aload_0
    //   349: iload #9
    //   351: iconst_1
    //   352: isub
    //   353: iload #8
    //   355: isub
    //   356: i2f
    //   357: invokevirtual getLineAtCoordinateUnclamped : (F)I
    //   360: istore #8
    //   362: iload_3
    //   363: iload #8
    //   365: iload_3
    //   366: isub
    //   367: iconst_2
    //   368: idiv
    //   369: isub
    //   370: istore #10
    //   372: iload #10
    //   374: ifge -> 383
    //   377: iconst_0
    //   378: istore #10
    //   380: goto -> 383
    //   383: iload #8
    //   385: iload #8
    //   387: iload_3
    //   388: isub
    //   389: iconst_2
    //   390: idiv
    //   391: iadd
    //   392: istore #11
    //   394: iload #11
    //   396: istore #9
    //   398: iload #11
    //   400: iload #7
    //   402: if_icmplt -> 411
    //   405: iload #7
    //   407: iconst_1
    //   408: isub
    //   409: istore #9
    //   411: aload #6
    //   413: iload #10
    //   415: invokevirtual getLineStart : (I)I
    //   418: istore #10
    //   420: aload #6
    //   422: iload #9
    //   424: invokevirtual getLineEnd : (I)I
    //   427: istore #12
    //   429: aload_0
    //   430: invokevirtual getSelectionStart : ()I
    //   433: istore #13
    //   435: aload_0
    //   436: invokevirtual getSelectionEnd : ()I
    //   439: istore #14
    //   441: iload #12
    //   443: istore #7
    //   445: iload #10
    //   447: istore #11
    //   449: iload #13
    //   451: iload #14
    //   453: if_icmpge -> 494
    //   456: iload #10
    //   458: istore #9
    //   460: iload #13
    //   462: iload #10
    //   464: if_icmpge -> 471
    //   467: iload #13
    //   469: istore #9
    //   471: iload #12
    //   473: istore #7
    //   475: iload #9
    //   477: istore #11
    //   479: iload #14
    //   481: iload #12
    //   483: if_icmple -> 494
    //   486: iload #14
    //   488: istore #7
    //   490: iload #9
    //   492: istore #11
    //   494: aload_0
    //   495: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   498: astore #5
    //   500: iload #11
    //   502: ifgt -> 524
    //   505: aload #5
    //   507: astore #4
    //   509: iload #7
    //   511: aload #5
    //   513: invokeinterface length : ()I
    //   518: if_icmpge -> 537
    //   521: goto -> 524
    //   524: aload #5
    //   526: iload #11
    //   528: iload #7
    //   530: invokeinterface subSequence : (II)Ljava/lang/CharSequence;
    //   535: astore #4
    //   537: iload_2
    //   538: iconst_1
    //   539: if_icmpne -> 551
    //   542: aload_1
    //   543: aload #4
    //   545: invokevirtual setText : (Ljava/lang/CharSequence;)V
    //   548: goto -> 648
    //   551: aload_1
    //   552: aload #4
    //   554: iload #13
    //   556: iload #11
    //   558: isub
    //   559: iload #14
    //   561: iload #11
    //   563: isub
    //   564: invokevirtual setText : (Ljava/lang/CharSequence;II)V
    //   567: iload #8
    //   569: iload_3
    //   570: isub
    //   571: iconst_1
    //   572: iadd
    //   573: newarray int
    //   575: astore #4
    //   577: iload #8
    //   579: iload_3
    //   580: isub
    //   581: iconst_1
    //   582: iadd
    //   583: newarray int
    //   585: astore #5
    //   587: aload_0
    //   588: invokevirtual getBaselineOffset : ()I
    //   591: istore #10
    //   593: iload_3
    //   594: istore #9
    //   596: iload #9
    //   598: iload #8
    //   600: if_icmpgt -> 640
    //   603: aload #4
    //   605: iload #9
    //   607: iload_3
    //   608: isub
    //   609: aload #6
    //   611: iload #9
    //   613: invokevirtual getLineStart : (I)I
    //   616: iastore
    //   617: aload #5
    //   619: iload #9
    //   621: iload_3
    //   622: isub
    //   623: aload #6
    //   625: iload #9
    //   627: invokevirtual getLineBaseline : (I)I
    //   630: iload #10
    //   632: iadd
    //   633: iastore
    //   634: iinc #9, 1
    //   637: goto -> 596
    //   640: aload_1
    //   641: aload #4
    //   643: aload #5
    //   645: invokevirtual setTextLines : ([I[I)V
    //   648: iload_2
    //   649: ifeq -> 663
    //   652: iload_2
    //   653: iconst_2
    //   654: if_icmpne -> 660
    //   657: goto -> 663
    //   660: goto -> 772
    //   663: iconst_0
    //   664: istore #9
    //   666: aload_0
    //   667: invokevirtual getTypefaceStyle : ()I
    //   670: istore #8
    //   672: iload #8
    //   674: iconst_1
    //   675: iand
    //   676: ifeq -> 684
    //   679: iconst_0
    //   680: iconst_1
    //   681: ior
    //   682: istore #9
    //   684: iload #9
    //   686: istore_3
    //   687: iload #8
    //   689: iconst_2
    //   690: iand
    //   691: ifeq -> 699
    //   694: iload #9
    //   696: iconst_2
    //   697: ior
    //   698: istore_3
    //   699: aload_0
    //   700: getfield mTextPaint : Landroid/text/TextPaint;
    //   703: invokevirtual getFlags : ()I
    //   706: istore #8
    //   708: iload_3
    //   709: istore #9
    //   711: iload #8
    //   713: bipush #32
    //   715: iand
    //   716: ifeq -> 724
    //   719: iload_3
    //   720: iconst_1
    //   721: ior
    //   722: istore #9
    //   724: iload #9
    //   726: istore_3
    //   727: iload #8
    //   729: bipush #8
    //   731: iand
    //   732: ifeq -> 740
    //   735: iload #9
    //   737: iconst_4
    //   738: ior
    //   739: istore_3
    //   740: iload_3
    //   741: istore #9
    //   743: iload #8
    //   745: bipush #16
    //   747: iand
    //   748: ifeq -> 757
    //   751: iload_3
    //   752: bipush #8
    //   754: ior
    //   755: istore #9
    //   757: aload_1
    //   758: aload_0
    //   759: invokevirtual getTextSize : ()F
    //   762: aload_0
    //   763: invokevirtual getCurrentTextColor : ()I
    //   766: iconst_1
    //   767: iload #9
    //   769: invokevirtual setTextStyle : (FIII)V
    //   772: iload_2
    //   773: iconst_1
    //   774: if_icmpeq -> 782
    //   777: iload_2
    //   778: iconst_2
    //   779: if_icmpne -> 860
    //   782: aload_1
    //   783: aload_0
    //   784: invokevirtual getMinEms : ()I
    //   787: invokevirtual setMinTextEms : (I)V
    //   790: aload_1
    //   791: aload_0
    //   792: invokevirtual getMaxEms : ()I
    //   795: invokevirtual setMaxTextEms : (I)V
    //   798: iconst_m1
    //   799: istore #9
    //   801: aload_0
    //   802: invokevirtual getFilters : ()[Landroid/text/InputFilter;
    //   805: astore #5
    //   807: aload #5
    //   809: arraylength
    //   810: istore #8
    //   812: iconst_0
    //   813: istore_2
    //   814: iload #9
    //   816: istore_3
    //   817: iload_2
    //   818: iload #8
    //   820: if_icmpge -> 855
    //   823: aload #5
    //   825: iload_2
    //   826: aaload
    //   827: astore #4
    //   829: aload #4
    //   831: instanceof android/text/InputFilter$LengthFilter
    //   834: ifeq -> 849
    //   837: aload #4
    //   839: checkcast android/text/InputFilter$LengthFilter
    //   842: invokevirtual getMax : ()I
    //   845: istore_3
    //   846: goto -> 855
    //   849: iinc #2, 1
    //   852: goto -> 814
    //   855: aload_1
    //   856: iload_3
    //   857: invokevirtual setMaxTextLength : (I)V
    //   860: aload_0
    //   861: getfield mHintId : I
    //   864: ifeq -> 956
    //   867: aload_1
    //   868: aload_0
    //   869: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   872: aload_0
    //   873: getfield mHintId : I
    //   876: invokevirtual getResourceEntryName : (I)Ljava/lang/String;
    //   879: invokevirtual setHintIdEntry : (Ljava/lang/String;)V
    //   882: goto -> 956
    //   885: astore #4
    //   887: getstatic android/view/autofill/Helper.sVerbose : Z
    //   890: ifeq -> 956
    //   893: new java/lang/StringBuilder
    //   896: dup
    //   897: invokespecial <init> : ()V
    //   900: astore #5
    //   902: aload #5
    //   904: ldc_w 'onProvideAutofillStructure(): cannot set name for hint id '
    //   907: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   910: pop
    //   911: aload #5
    //   913: aload_0
    //   914: getfield mHintId : I
    //   917: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   920: pop
    //   921: aload #5
    //   923: ldc_w ': '
    //   926: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   929: pop
    //   930: aload #5
    //   932: aload #4
    //   934: invokevirtual getMessage : ()Ljava/lang/String;
    //   937: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   940: pop
    //   941: aload #5
    //   943: invokevirtual toString : ()Ljava/lang/String;
    //   946: astore #4
    //   948: ldc 'TextView'
    //   950: aload #4
    //   952: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   955: pop
    //   956: aload_1
    //   957: aload_0
    //   958: invokevirtual getHint : ()Ljava/lang/CharSequence;
    //   961: invokevirtual setHint : (Ljava/lang/CharSequence;)V
    //   964: aload_1
    //   965: aload_0
    //   966: invokevirtual getInputType : ()I
    //   969: invokevirtual setInputType : (I)V
    //   972: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #11660	-> 0
    //   #11662	-> 7
    //   #11663	-> 14
    //   #11664	-> 34
    //   #11666	-> 44
    //   #11667	-> 49
    //   #11669	-> 59
    //   #11671	-> 66
    //   #11677	-> 81
    //   #11672	-> 84
    //   #11673	-> 86
    //   #11674	-> 92
    //   #11675	-> 129
    //   #11674	-> 147
    //   #11681	-> 155
    //   #11683	-> 175
    //   #11684	-> 182
    //   #11685	-> 187
    //   #11687	-> 196
    //   #11689	-> 200
    //   #11690	-> 206
    //   #11691	-> 213
    //   #11693	-> 219
    //   #11694	-> 225
    //   #11695	-> 230
    //   #11697	-> 239
    //   #11699	-> 253
    //   #11702	-> 256
    //   #11703	-> 261
    //   #11704	-> 267
    //   #11705	-> 273
    //   #11706	-> 276
    //   #11707	-> 282
    //   #11708	-> 290
    //   #11709	-> 297
    //   #11711	-> 307
    //   #11714	-> 314
    //   #11716	-> 319
    //   #11717	-> 325
    //   #11721	-> 339
    //   #11722	-> 348
    //   #11726	-> 362
    //   #11727	-> 372
    //   #11728	-> 377
    //   #11727	-> 383
    //   #11730	-> 383
    //   #11731	-> 394
    //   #11732	-> 405
    //   #11736	-> 411
    //   #11737	-> 420
    //   #11741	-> 429
    //   #11742	-> 435
    //   #11743	-> 441
    //   #11744	-> 456
    //   #11745	-> 467
    //   #11747	-> 471
    //   #11748	-> 486
    //   #11753	-> 494
    //   #11754	-> 500
    //   #11755	-> 524
    //   #11758	-> 537
    //   #11759	-> 542
    //   #11761	-> 551
    //   #11763	-> 567
    //   #11764	-> 577
    //   #11765	-> 587
    //   #11766	-> 593
    //   #11767	-> 603
    //   #11768	-> 617
    //   #11766	-> 634
    //   #11770	-> 640
    //   #11774	-> 648
    //   #11777	-> 663
    //   #11778	-> 666
    //   #11779	-> 672
    //   #11780	-> 679
    //   #11782	-> 684
    //   #11783	-> 694
    //   #11787	-> 699
    //   #11788	-> 708
    //   #11789	-> 719
    //   #11791	-> 724
    //   #11792	-> 735
    //   #11794	-> 740
    //   #11795	-> 751
    //   #11800	-> 757
    //   #11803	-> 772
    //   #11805	-> 782
    //   #11806	-> 790
    //   #11807	-> 798
    //   #11808	-> 801
    //   #11809	-> 829
    //   #11810	-> 837
    //   #11811	-> 846
    //   #11808	-> 849
    //   #11814	-> 855
    //   #11817	-> 860
    //   #11819	-> 867
    //   #11825	-> 882
    //   #11820	-> 885
    //   #11821	-> 887
    //   #11822	-> 893
    //   #11823	-> 930
    //   #11822	-> 948
    //   #11827	-> 956
    //   #11828	-> 964
    //   #11829	-> 972
    // Exception table:
    //   from	to	target	type
    //   66	81	84	android/content/res/Resources$NotFoundException
    //   867	882	885	android/content/res/Resources$NotFoundException
  }
  
  boolean canRequestAutofill() {
    if (!isAutofillable())
      return false; 
    AutofillManager autofillManager = (AutofillManager)this.mContext.getSystemService(AutofillManager.class);
    if (autofillManager != null)
      return autofillManager.isEnabled(); 
    return false;
  }
  
  private void requestAutofill() {
    AutofillManager autofillManager = (AutofillManager)this.mContext.getSystemService(AutofillManager.class);
    if (autofillManager != null)
      autofillManager.requestAutofill(this); 
  }
  
  public void autofill(AutofillValue paramAutofillValue) {
    if (!paramAutofillValue.isText() || !isTextEditable()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramAutofillValue);
      stringBuilder.append(" could not be autofilled into ");
      stringBuilder.append(this);
      Log.w("TextView", stringBuilder.toString());
      return;
    } 
    CharSequence charSequence = paramAutofillValue.getTextValue();
    setText(charSequence, this.mBufferType, true, 0);
    charSequence = getText();
    if (charSequence instanceof Spannable)
      Selection.setSelection((Spannable)charSequence, charSequence.length()); 
  }
  
  public int getAutofillType() {
    return isTextEditable();
  }
  
  public AutofillValue getAutofillValue() {
    if (isTextEditable()) {
      CharSequence charSequence = (CharSequence)TextUtils.trimToParcelableSize(getText());
      return AutofillValue.forText(charSequence);
    } 
    return null;
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    boolean bool = hasPasswordTransformationMethod();
    paramAccessibilityEvent.setPassword(bool);
    if (paramAccessibilityEvent.getEventType() == 8192) {
      paramAccessibilityEvent.setFromIndex(Selection.getSelectionStart(this.mText));
      paramAccessibilityEvent.setToIndex(Selection.getSelectionEnd(this.mText));
      paramAccessibilityEvent.setItemCount(this.mText.length());
    } 
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    boolean bool = hasPasswordTransformationMethod();
    paramAccessibilityNodeInfo.setPassword(bool);
    paramAccessibilityNodeInfo.setText(getTextForAccessibility());
    paramAccessibilityNodeInfo.setHintText(this.mHint);
    paramAccessibilityNodeInfo.setShowingHintText(isShowingHint());
    if (this.mBufferType == BufferType.EDITABLE) {
      paramAccessibilityNodeInfo.setEditable(true);
      if (isEnabled())
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_TEXT); 
    } 
    Editor editor = this.mEditor;
    if (editor != null) {
      paramAccessibilityNodeInfo.setInputType(editor.mInputType);
      if (this.mEditor.mError != null) {
        paramAccessibilityNodeInfo.setContentInvalid(true);
        paramAccessibilityNodeInfo.setError(this.mEditor.mError);
      } 
      if (isTextEditable() && isFocused()) {
        CharSequence charSequence = this.mContext.getResources().getString(17040375);
        if (getImeActionLabel() != null)
          charSequence = getImeActionLabel(); 
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction = new AccessibilityNodeInfo.AccessibilityAction(16908372, charSequence);
        paramAccessibilityNodeInfo.addAction(accessibilityAction);
      } 
    } 
    if (!TextUtils.isEmpty(this.mText)) {
      paramAccessibilityNodeInfo.addAction(256);
      paramAccessibilityNodeInfo.addAction(512);
      paramAccessibilityNodeInfo.setMovementGranularities(31);
      paramAccessibilityNodeInfo.addAction(131072);
      paramAccessibilityNodeInfo.setAvailableExtraData(Arrays.asList(new String[] { "android.view.accessibility.extra.DATA_RENDERING_INFO_KEY", "android.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_KEY" }));
    } else {
      paramAccessibilityNodeInfo.setAvailableExtraData(Arrays.asList(new String[] { "android.view.accessibility.extra.DATA_RENDERING_INFO_KEY" }));
    } 
    if (isFocused()) {
      if (canCopy())
        paramAccessibilityNodeInfo.addAction(16384); 
      if (canPaste())
        paramAccessibilityNodeInfo.addAction(32768); 
      if (canCut())
        paramAccessibilityNodeInfo.addAction(65536); 
      if (canShare()) {
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction = new AccessibilityNodeInfo.AccessibilityAction(268435456, getResources().getString(17041273));
        paramAccessibilityNodeInfo.addAction(accessibilityAction);
      } 
      if (canProcessText())
        this.mEditor.mProcessTextIntentActionsHandler.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo); 
    } 
    int i = this.mFilters.length;
    for (byte b = 0; b < i; b++) {
      InputFilter inputFilter = this.mFilters[b];
      if (inputFilter instanceof InputFilter.LengthFilter)
        paramAccessibilityNodeInfo.setMaxTextLength(((InputFilter.LengthFilter)inputFilter).getMax()); 
    } 
    if (!isSingleLine())
      paramAccessibilityNodeInfo.setMultiLine(true); 
    if ((paramAccessibilityNodeInfo.isClickable() || paramAccessibilityNodeInfo.isLongClickable()) && this.mMovement instanceof LinkMovementMethod) {
      if (!hasOnClickListeners()) {
        paramAccessibilityNodeInfo.setClickable(false);
        paramAccessibilityNodeInfo.removeAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_CLICK);
      } 
      if (!hasOnLongClickListeners()) {
        paramAccessibilityNodeInfo.setLongClickable(false);
        paramAccessibilityNodeInfo.removeAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_LONG_CLICK);
      } 
    } 
  }
  
  public void addExtraDataToAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo, String paramString, Bundle paramBundle) {
    if (paramBundle != null && paramString.equals("android.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_KEY")) {
      int i = paramBundle.getInt("android.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_ARG_START_INDEX", -1);
      int j = paramBundle.getInt("android.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_ARG_LENGTH", -1);
      if (j > 0 && i >= 0) {
        CharSequence charSequence = this.mText;
        if (i < charSequence.length()) {
          RectF[] arrayOfRectF = new RectF[j];
          CursorAnchorInfo.Builder builder = new CursorAnchorInfo.Builder();
          float f1 = viewportToContentHorizontalOffset(), f2 = viewportToContentVerticalOffset();
          populateCharacterBounds(builder, i, i + j, f1, f2);
          CursorAnchorInfo cursorAnchorInfo = builder.setMatrix(null).build();
          for (byte b = 0; b < j; b++) {
            int k = cursorAnchorInfo.getCharacterBoundsFlags(i + b);
            if ((k & 0x1) == 1) {
              RectF rectF = cursorAnchorInfo.getCharacterBounds(i + b);
              if (rectF != null) {
                mapRectFromViewToScreenCoords(rectF, true);
                arrayOfRectF[b] = rectF;
              } 
            } 
          } 
          paramAccessibilityNodeInfo.getExtras().putParcelableArray(paramString, (Parcelable[])arrayOfRectF);
          return;
        } 
      } 
      Log.e("TextView", "Invalid arguments for accessibility character locations");
      return;
    } 
    if (paramString.equals("android.view.accessibility.extra.DATA_RENDERING_INFO_KEY")) {
      AccessibilityNodeInfo.ExtraRenderingInfo extraRenderingInfo = AccessibilityNodeInfo.ExtraRenderingInfo.obtain();
      extraRenderingInfo.setLayoutSize((getLayoutParams()).width, (getLayoutParams()).height);
      extraRenderingInfo.setTextSizeInPx(getTextSize());
      extraRenderingInfo.setTextSizeUnit(getTextSizeUnit());
      paramAccessibilityNodeInfo.setExtraRenderingInfo(extraRenderingInfo);
    } 
  }
  
  public void populateCharacterBounds(CursorAnchorInfo.Builder paramBuilder, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2) {
    int i = this.mLayout.getLineForOffset(paramInt1);
    int j = this.mLayout.getLineForOffset(paramInt2 - 1);
    for (int k = i; k <= j; k++) {
      int m = this.mLayout.getLineStart(k);
      int n = this.mLayout.getLineEnd(k);
      int i1 = Math.max(m, paramInt1);
      int i2 = Math.min(n, paramInt2);
      Layout layout = this.mLayout;
      n = layout.getParagraphDirection(k);
      boolean bool = true;
      if (n != 1)
        bool = false; 
      float[] arrayOfFloat = new float[i2 - i1];
      this.mLayout.getPaint().getTextWidths(this.mTransformed, i1, i2, arrayOfFloat);
      float f1 = this.mLayout.getLineTop(k);
      float f2 = this.mLayout.getLineBottom(k);
      for (int i3 = i1;; i3++);
      continue;
    } 
  }
  
  public boolean isPositionVisible(float paramFloat1, float paramFloat2) {
    synchronized (TEMP_POSITION) {
      float[] arrayOfFloat = TEMP_POSITION;
      arrayOfFloat[0] = paramFloat1;
      arrayOfFloat[1] = paramFloat2;
      TextView textView = this;
      while (textView != null) {
        if (textView != this) {
          arrayOfFloat[0] = arrayOfFloat[0] - textView.getScrollX();
          arrayOfFloat[1] = arrayOfFloat[1] - textView.getScrollY();
        } 
        if (arrayOfFloat[0] >= 0.0F && arrayOfFloat[1] >= 0.0F && arrayOfFloat[0] <= textView.getWidth()) {
          paramFloat1 = arrayOfFloat[1];
          if (paramFloat1 <= textView.getHeight()) {
            if (!textView.getMatrix().isIdentity())
              textView.getMatrix().mapPoints(arrayOfFloat); 
            arrayOfFloat[0] = arrayOfFloat[0] + textView.getLeft();
            arrayOfFloat[1] = arrayOfFloat[1] + textView.getTop();
            ViewParent viewParent = textView.getParent();
            if (viewParent instanceof View) {
              View view = (View)viewParent;
              continue;
            } 
            viewParent = null;
            continue;
          } 
        } 
        return false;
      } 
      return true;
    } 
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    CharSequence charSequence1;
    Editor editor1;
    CharSequence charSequence2;
    byte b;
    Editor editor2 = this.mEditor;
    if (editor2 != null) {
      Editor.ProcessTextIntentActionsHandler processTextIntentActionsHandler = editor2.mProcessTextIntentActionsHandler;
      if (processTextIntentActionsHandler.performAccessibilityAction(paramInt))
        return true; 
    } 
    switch (paramInt) {
      default:
        return super.performAccessibilityActionInternal(paramInt, paramBundle);
      case 268435456:
        if (isFocused() && canShare() && onTextContextMenuItem(16908341))
          return true; 
        return false;
      case 16908372:
        if (isFocused() && isTextEditable())
          onEditorAction(getImeActionId()); 
        return true;
      case 2097152:
        if (!isEnabled() || this.mBufferType != BufferType.EDITABLE)
          return false; 
        if (paramBundle != null) {
          CharSequence charSequence = paramBundle.getCharSequence("ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE");
        } else {
          paramBundle = null;
        } 
        setText((CharSequence)paramBundle);
        charSequence1 = this.mText;
        if (charSequence1 != null) {
          paramInt = charSequence1.length();
          if (paramInt > 0)
            Selection.setSelection(this.mSpannable, paramInt); 
        } 
        return true;
      case 131072:
        ensureIterableTextForAccessibilitySelectable();
        charSequence2 = getIterableTextForAccessibility();
        if (charSequence2 == null)
          return false; 
        if (charSequence1 != null) {
          paramInt = charSequence1.getInt("ACTION_ARGUMENT_SELECTION_START_INT", -1);
        } else {
          paramInt = -1;
        } 
        if (charSequence1 != null) {
          b = charSequence1.getInt("ACTION_ARGUMENT_SELECTION_END_INT", -1);
        } else {
          b = -1;
        } 
        if (getSelectionStart() != paramInt || getSelectionEnd() != b) {
          if (paramInt == b && b == -1) {
            Selection.removeSelection((Spannable)charSequence2);
            return true;
          } 
          if (paramInt >= 0 && paramInt <= b && b <= charSequence2.length()) {
            Selection.setSelection((Spannable)charSequence2, paramInt, b);
            editor1 = this.mEditor;
            if (editor1 != null)
              editor1.startSelectionActionModeAsync(false); 
            return true;
          } 
        } 
        return false;
      case 65536:
        if (isFocused() && canCut() && onTextContextMenuItem(16908320))
          return true; 
        return false;
      case 32768:
        if (isFocused() && canPaste() && onTextContextMenuItem(16908322))
          return true; 
        return false;
      case 16384:
        if (isFocused() && canCopy() && onTextContextMenuItem(16908321))
          return true; 
        return false;
      case 256:
      case 512:
        ensureIterableTextForAccessibilitySelectable();
        return super.performAccessibilityActionInternal(paramInt, (Bundle)editor1);
      case 32:
        if (isLongClickable()) {
          boolean bool;
          if (isEnabled() && this.mBufferType == BufferType.EDITABLE) {
            this.mEditor.mIsBeingLongClickedByAccessibility = true;
            try {
              bool = performLongClick();
            } finally {
              this.mEditor.mIsBeingLongClickedByAccessibility = false;
            } 
          } else {
            bool = performLongClick();
          } 
          return bool;
        } 
        return false;
      case 16:
        break;
    } 
    return performAccessibilityActionClick((Bundle)editor1);
  }
  
  private boolean performAccessibilityActionClick(Bundle paramBundle) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_0
    //   3: invokevirtual isEnabled : ()Z
    //   6: ifne -> 11
    //   9: iconst_0
    //   10: ireturn
    //   11: aload_0
    //   12: invokevirtual isClickable : ()Z
    //   15: ifne -> 25
    //   18: aload_0
    //   19: invokevirtual isLongClickable : ()Z
    //   22: ifeq -> 51
    //   25: aload_0
    //   26: invokevirtual isFocusable : ()Z
    //   29: ifeq -> 44
    //   32: aload_0
    //   33: invokevirtual isFocused : ()Z
    //   36: ifne -> 44
    //   39: aload_0
    //   40: invokevirtual requestFocus : ()Z
    //   43: pop
    //   44: aload_0
    //   45: invokevirtual performClick : ()Z
    //   48: pop
    //   49: iconst_1
    //   50: istore_2
    //   51: aload_0
    //   52: getfield mMovement : Landroid/text/method/MovementMethod;
    //   55: ifnonnull -> 67
    //   58: iload_2
    //   59: istore_3
    //   60: aload_0
    //   61: invokevirtual onCheckIsTextEditor : ()Z
    //   64: ifeq -> 156
    //   67: iload_2
    //   68: istore_3
    //   69: aload_0
    //   70: invokespecial hasSpannableText : ()Z
    //   73: ifeq -> 156
    //   76: iload_2
    //   77: istore_3
    //   78: aload_0
    //   79: getfield mLayout : Landroid/text/Layout;
    //   82: ifnull -> 156
    //   85: aload_0
    //   86: invokevirtual isTextEditable : ()Z
    //   89: ifne -> 101
    //   92: iload_2
    //   93: istore_3
    //   94: aload_0
    //   95: invokevirtual isTextSelectable : ()Z
    //   98: ifeq -> 156
    //   101: iload_2
    //   102: istore_3
    //   103: aload_0
    //   104: invokevirtual isFocused : ()Z
    //   107: ifeq -> 156
    //   110: aload_0
    //   111: invokespecial getInputMethodManager : ()Landroid/view/inputmethod/InputMethodManager;
    //   114: astore_1
    //   115: aload_0
    //   116: aload_1
    //   117: invokevirtual viewClicked : (Landroid/view/inputmethod/InputMethodManager;)V
    //   120: iload_2
    //   121: istore_3
    //   122: aload_0
    //   123: invokevirtual isTextSelectable : ()Z
    //   126: ifne -> 156
    //   129: iload_2
    //   130: istore_3
    //   131: aload_0
    //   132: getfield mEditor : Landroid/widget/Editor;
    //   135: getfield mShowSoftInputOnFocus : Z
    //   138: ifeq -> 156
    //   141: iload_2
    //   142: istore_3
    //   143: aload_1
    //   144: ifnull -> 156
    //   147: iload_2
    //   148: aload_1
    //   149: aload_0
    //   150: iconst_0
    //   151: invokevirtual showSoftInput : (Landroid/view/View;I)Z
    //   154: ior
    //   155: istore_3
    //   156: iload_3
    //   157: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #12294	-> 0
    //   #12296	-> 2
    //   #12297	-> 9
    //   #12300	-> 11
    //   #12302	-> 25
    //   #12303	-> 39
    //   #12306	-> 44
    //   #12307	-> 49
    //   #12311	-> 51
    //   #12312	-> 85
    //   #12313	-> 110
    //   #12314	-> 115
    //   #12315	-> 120
    //   #12316	-> 147
    //   #12320	-> 156
  }
  
  private boolean hasSpannableText() {
    boolean bool;
    CharSequence charSequence = this.mText;
    if (charSequence != null && charSequence instanceof Spannable) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void sendAccessibilityEventInternal(int paramInt) {
    if (paramInt == 32768) {
      Editor editor = this.mEditor;
      if (editor != null)
        editor.mProcessTextIntentActionsHandler.initializeAccessibilityActions(); 
    } 
    super.sendAccessibilityEventInternal(paramInt);
  }
  
  public void sendAccessibilityEventUnchecked(AccessibilityEvent paramAccessibilityEvent) {
    if (paramAccessibilityEvent.getEventType() == 4096)
      return; 
    super.sendAccessibilityEventUnchecked(paramAccessibilityEvent);
  }
  
  private CharSequence getTextForAccessibility() {
    if (TextUtils.isEmpty(this.mText))
      return this.mHint; 
    return TextUtils.trimToParcelableSize(this.mTransformed);
  }
  
  void sendAccessibilityEventTypeViewTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(16);
    accessibilityEvent.setFromIndex(paramInt1);
    accessibilityEvent.setRemovedCount(paramInt2);
    accessibilityEvent.setAddedCount(paramInt3);
    accessibilityEvent.setBeforeText(paramCharSequence);
    sendAccessibilityEventUnchecked(accessibilityEvent);
  }
  
  private InputMethodManager getInputMethodManager() {
    return (InputMethodManager)getContext().getSystemService(InputMethodManager.class);
  }
  
  public boolean isInputMethodTarget() {
    boolean bool;
    InputMethodManager inputMethodManager = getInputMethodManager();
    if (inputMethodManager != null && inputMethodManager.isActive(this)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean onTextContextMenuItem(int paramInt) {
    int i = 0;
    int j = this.mText.length();
    if (isFocused()) {
      int k = getSelectionStart();
      j = getSelectionEnd();
      i = Math.max(0, Math.min(k, j));
      j = Math.max(0, Math.max(k, j));
    } 
    if (paramInt != 16908355) {
      Editor editor2;
      ClipData clipData;
      switch (paramInt) {
        default:
          switch (paramInt) {
            default:
              return false;
            case 16908341:
              shareSelectedText();
              return true;
            case 16908340:
              editor2 = this.mEditor;
              if (editor2 != null)
                editor2.replace(); 
              return true;
            case 16908339:
              editor2 = this.mEditor;
              if (editor2 != null)
                editor2.redo(); 
              return true;
            case 16908338:
              editor2 = this.mEditor;
              if (editor2 != null)
                editor2.undo(); 
              return true;
            case 16908337:
              break;
          } 
          paste(i, j, false);
          return true;
        case 16908322:
          paste(i, j, true);
          return true;
        case 16908321:
          i = getSelectionStart();
          j = getSelectionEnd();
          paramInt = Math.max(0, Math.min(i, j));
          j = Math.max(0, Math.max(i, j));
          clipData = ClipData.newPlainText(null, getTransformedText(paramInt, j));
          if (setPrimaryClip(clipData)) {
            stopTextActionMode();
          } else {
            Toast toast = Toast.makeText(getContext(), 17040213, 0);
            toast.show();
          } 
          return true;
        case 16908320:
          clipData = ClipData.newPlainText(null, getTransformedText(i, j));
          if (setPrimaryClip(clipData)) {
            deleteText_internal(i, j);
          } else {
            Toast toast = Toast.makeText(getContext(), 17040213, 0);
            toast.show();
          } 
          return true;
        case 16908319:
          break;
      } 
      boolean bool = hasSelection();
      selectAllText();
      Editor editor1 = this.mEditor;
      if (editor1 != null && bool)
        editor1.invalidateActionModeAsync(); 
      return true;
    } 
    requestAutofill();
    stopTextActionMode();
    return true;
  }
  
  CharSequence getTransformedText(int paramInt1, int paramInt2) {
    return removeSuggestionSpans(this.mTransformed.subSequence(paramInt1, paramInt2));
  }
  
  public boolean performLongClick() {
    boolean bool1 = false;
    boolean bool = false;
    Editor editor = this.mEditor;
    if (editor != null)
      editor.mIsBeingLongClicked = true; 
    if (super.performLongClick()) {
      bool1 = true;
      bool = true;
    } 
    editor = this.mEditor;
    boolean bool2 = bool1;
    if (editor != null) {
      bool2 = bool1 | editor.performLongClick(bool1);
      this.mEditor.mIsBeingLongClicked = false;
    } 
    if (bool2) {
      if (!bool)
        performHapticFeedback(0); 
      editor = this.mEditor;
      if (editor != null)
        editor.mDiscardNextActionUp = true; 
    } else {
      MetricsLogger.action(this.mContext, 629, 0);
    } 
    return bool2;
  }
  
  protected void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Editor editor = this.mEditor;
    if (editor != null)
      editor.onScrollChanged(); 
  }
  
  public boolean isSuggestionsEnabled() {
    Editor editor = this.mEditor;
    boolean bool = false;
    if (editor == null)
      return false; 
    if ((editor.mInputType & 0xF) != 1)
      return false; 
    if ((this.mEditor.mInputType & 0x80000) > 0)
      return false; 
    int i = this.mEditor.mInputType & 0xFF0;
    if (i == 0 || i == 48 || i == 80 || i == 64 || i == 160)
      bool = true; 
    return bool;
  }
  
  public void setCustomSelectionActionModeCallback(ActionMode.Callback paramCallback) {
    createEditorIfNeeded();
    this.mEditor.mCustomSelectionActionModeCallback = paramCallback;
  }
  
  public ActionMode.Callback getCustomSelectionActionModeCallback() {
    ActionMode.Callback callback;
    Editor editor = this.mEditor;
    if (editor == null) {
      editor = null;
    } else {
      callback = editor.mCustomSelectionActionModeCallback;
    } 
    return callback;
  }
  
  public void setCustomInsertionActionModeCallback(ActionMode.Callback paramCallback) {
    createEditorIfNeeded();
    this.mEditor.mCustomInsertionActionModeCallback = paramCallback;
  }
  
  public ActionMode.Callback getCustomInsertionActionModeCallback() {
    ActionMode.Callback callback;
    Editor editor = this.mEditor;
    if (editor == null) {
      editor = null;
    } else {
      callback = editor.mCustomInsertionActionModeCallback;
    } 
    return callback;
  }
  
  public void setTextClassifier(TextClassifier paramTextClassifier) {
    this.mTextClassifier = paramTextClassifier;
  }
  
  public TextClassifier getTextClassifier() {
    TextClassificationManager textClassificationManager;
    TextClassifier textClassifier = this.mTextClassifier;
    if (textClassifier == null) {
      textClassificationManager = getTextClassificationManagerForUser();
      if (textClassificationManager != null)
        return textClassificationManager.getTextClassifier(); 
      return TextClassifier.NO_OP;
    } 
    return (TextClassifier)textClassificationManager;
  }
  
  TextClassifier getTextClassificationSession() {
    TextClassifier textClassifier = this.mTextClassificationSession;
    if (textClassifier == null || textClassifier.isDestroyed()) {
      TextClassificationManager textClassificationManager = getTextClassificationManagerForUser();
      if (textClassificationManager != null) {
        String str;
        if (isTextEditable()) {
          str = "edittext";
        } else if (isTextSelectable()) {
          str = "textview";
        } else {
          str = "nosel-textview";
        } 
        Context context = this.mContext;
        TextClassificationContext.Builder builder = new TextClassificationContext.Builder(context.getPackageName(), str);
        TextClassificationContext textClassificationContext = builder.build();
        TextClassifier textClassifier1 = this.mTextClassifier;
        if (textClassifier1 != null) {
          this.mTextClassificationSession = textClassificationManager.createTextClassificationSession(textClassificationContext, textClassifier1);
        } else {
          this.mTextClassificationSession = textClassificationManager.createTextClassificationSession(textClassificationContext);
        } 
      } else {
        this.mTextClassificationSession = TextClassifier.NO_OP;
      } 
    } 
    return this.mTextClassificationSession;
  }
  
  TextClassificationContext getTextClassificationContext() {
    return this.mTextClassificationContext;
  }
  
  boolean usesNoOpTextClassifier() {
    boolean bool;
    if (getTextClassifier() == TextClassifier.NO_OP) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean requestActionMode(TextLinks.TextLinkSpan paramTextLinkSpan) {
    Preconditions.checkNotNull(paramTextLinkSpan);
    CharSequence charSequence = this.mText;
    if (!(charSequence instanceof Spanned))
      return false; 
    int i = ((Spanned)charSequence).getSpanStart(paramTextLinkSpan);
    int j = ((Spanned)this.mText).getSpanEnd(paramTextLinkSpan);
    if (i < 0 || j > this.mText.length() || i >= j)
      return false; 
    createEditorIfNeeded();
    this.mEditor.startLinkActionModeAsync(i, j);
    return true;
  }
  
  public boolean handleClick(TextLinks.TextLinkSpan paramTextLinkSpan) {
    Preconditions.checkNotNull(paramTextLinkSpan);
    CharSequence charSequence = this.mText;
    if (charSequence instanceof Spanned) {
      charSequence = charSequence;
      int i = charSequence.getSpanStart(paramTextLinkSpan);
      int j = charSequence.getSpanEnd(paramTextLinkSpan);
      if (i >= 0 && j <= this.mText.length() && i < j) {
        TextClassification.Request.Builder builder = new TextClassification.Request.Builder(this.mText, i, j);
        builder = builder.setDefaultLocales(getTextLocales());
        TextClassification.Request request = builder.build();
        _$$Lambda$TextView$DJlzb7VS7J_1890Kto7GAApQDN0 _$$Lambda$TextView$DJlzb7VS7J_1890Kto7GAApQDN0 = new _$$Lambda$TextView$DJlzb7VS7J_1890Kto7GAApQDN0(this, request);
        -$.Lambda.TextView.jQz3_DIfGrNeNdu_95_wi6UkW4E jQz3_DIfGrNeNdu_95_wi6UkW4E = _$$Lambda$TextView$jQz3_DIfGrNeNdu_95_wi6UkW4E.INSTANCE;
        CompletableFuture<?> completableFuture2 = CompletableFuture.supplyAsync(_$$Lambda$TextView$DJlzb7VS7J_1890Kto7GAApQDN0);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        CompletableFuture<?> completableFuture1 = completableFuture2.completeOnTimeout(null, 1L, timeUnit);
        completableFuture1.thenAccept((Consumer<?>)jQz3_DIfGrNeNdu_95_wi6UkW4E);
        return true;
      } 
    } 
    return false;
  }
  
  protected void stopTextActionMode() {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.stopTextActionMode(); 
  }
  
  public void hideFloatingToolbar(int paramInt) {
    Editor editor = this.mEditor;
    if (editor != null)
      editor.hideFloatingToolbar(paramInt); 
  }
  
  boolean canUndo() {
    boolean bool;
    Editor editor = this.mEditor;
    if (editor != null && editor.canUndo()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean canRedo() {
    boolean bool;
    Editor editor = this.mEditor;
    if (editor != null && editor.canRedo()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean canCut() {
    if (hasPasswordTransformationMethod())
      return false; 
    if (!mOplusCustomizeRestrictionManager.getClipboardStatus())
      return false; 
    if (this.mText.length() > 0 && hasSelection() && this.mText instanceof Editable) {
      Editor editor = this.mEditor;
      if (editor != null && editor.mKeyListener != null)
        return true; 
    } 
    return false;
  }
  
  boolean canCopy() {
    if (hasPasswordTransformationMethod())
      return false; 
    if (!mOplusCustomizeRestrictionManager.getClipboardStatus())
      return false; 
    if (this.mText.length() > 0 && hasSelection() && this.mEditor != null)
      return true; 
    return false;
  }
  
  boolean canShare() {
    if (!getContext().canStartActivityForResult() || !isDeviceProvisioned())
      return false; 
    return canCopy();
  }
  
  boolean isDeviceProvisioned() {
    int i = this.mDeviceProvisionedState;
    boolean bool = true;
    if (i == 0) {
      ContentResolver contentResolver = this.mContext.getContentResolver();
      if (Settings.Global.getInt(contentResolver, "device_provisioned", 0) != 0) {
        i = 2;
      } else {
        i = 1;
      } 
      this.mDeviceProvisionedState = i;
    } 
    if (this.mDeviceProvisionedState != 2)
      bool = false; 
    return bool;
  }
  
  boolean canPaste() {
    boolean bool = mOplusCustomizeRestrictionManager.getClipboardStatus();
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (this.mText instanceof Editable) {
      Editor editor = this.mEditor;
      if (editor != null && editor.mKeyListener != null)
        if (getSelectionStart() >= 0 && 
          getSelectionEnd() >= 0 && 
          getClipboardManagerForUser().hasPrimaryClip())
          bool1 = true;  
    } 
    return bool1;
  }
  
  boolean canPasteAsPlainText() {
    if (!canPaste())
      return false; 
    ClipData clipData = getClipboardManagerForUser().getPrimaryClip();
    ClipDescription clipDescription = clipData.getDescription();
    boolean bool = clipDescription.hasMimeType("text/plain");
    CharSequence charSequence = clipData.getItemAt(0).getText();
    if (bool && charSequence instanceof Spanned) {
      charSequence = charSequence;
      if (TextUtils.hasStyleSpan((Spanned)charSequence))
        return true; 
    } 
    return clipDescription.hasMimeType("text/html");
  }
  
  boolean canProcessText() {
    if (getId() == -1)
      return false; 
    return canShare();
  }
  
  boolean canSelectAllText() {
    boolean bool;
    if (canSelectText() && !hasPasswordTransformationMethod() && (
      getSelectionStart() != 0 || getSelectionEnd() != this.mText.length())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean selectAllText() {
    if (this.mEditor != null)
      hideFloatingToolbar(500); 
    int i = this.mText.length();
    Spannable spannable = this.mSpannable;
    boolean bool = false;
    Selection.setSelection(spannable, 0, i);
    if (i > 0)
      bool = true; 
    return bool;
  }
  
  void replaceSelectionWithText(CharSequence paramCharSequence) {
    ((Editable)this.mText).replace(getSelectionStart(), getSelectionEnd(), paramCharSequence);
  }
  
  private void paste(int paramInt1, int paramInt2, boolean paramBoolean) {
    ClipboardManager clipboardManager = getClipboardManagerForUser();
    ClipData clipData = clipboardManager.getPrimaryClip();
    if (clipData != null) {
      boolean bool = false;
      for (byte b = 0; b < clipData.getItemCount(); b++, bool = bool1) {
        CharSequence charSequence;
        if (paramBoolean) {
          charSequence = clipData.getItemAt(b).coerceToStyledText(getContext());
        } else {
          charSequence = clipData.getItemAt(b).coerceToText(getContext());
          if (charSequence instanceof Spanned)
            charSequence = charSequence.toString(); 
        } 
        boolean bool1 = bool;
        if (charSequence != null)
          if (!bool) {
            Selection.setSelection(this.mSpannable, paramInt2);
            ((Editable)this.mText).replace(paramInt1, paramInt2, charSequence);
            bool1 = true;
          } else {
            ((Editable)this.mText).insert(getSelectionEnd(), "\n");
            ((Editable)this.mText).insert(getSelectionEnd(), charSequence);
            bool1 = bool;
          }  
      } 
      sLastCutCopyOrTextChangedTime = 0L;
    } 
  }
  
  private void shareSelectedText() {
    String str = getSelectedText();
    if (str != null && !str.isEmpty()) {
      Intent intent = new Intent("android.intent.action.SEND");
      intent.setType("text/plain");
      intent.removeExtra("android.intent.extra.TEXT");
      str = TextUtils.<String>trimToParcelableSize(str);
      intent.putExtra("android.intent.extra.TEXT", str);
      getContext().startActivity(Intent.createChooser(intent, null));
      Selection.setSelection(this.mSpannable, getSelectionEnd());
    } 
  }
  
  private boolean setPrimaryClip(ClipData paramClipData) {
    ClipboardManager clipboardManager = getClipboardManagerForUser();
    try {
      clipboardManager.setPrimaryClip(paramClipData);
      return true;
    } finally {
      paramClipData = null;
    } 
  }
  
  public int getOffsetForPosition(float paramFloat1, float paramFloat2) {
    if (getLayout() == null)
      return -1; 
    int i = getLineAtCoordinate(paramFloat2);
    i = getOffsetAtCoordinate(i, paramFloat1);
    return i;
  }
  
  float convertToLocalHorizontalCoordinate(float paramFloat) {
    float f = getTotalPaddingLeft();
    paramFloat = Math.max(0.0F, paramFloat - f);
    paramFloat = Math.min((getWidth() - getTotalPaddingRight() - 1), paramFloat);
    f = getScrollX();
    return paramFloat + f;
  }
  
  int getLineAtCoordinate(float paramFloat) {
    float f = getTotalPaddingTop();
    paramFloat = Math.max(0.0F, paramFloat - f);
    f = Math.min((getHeight() - getTotalPaddingBottom() - 1), paramFloat);
    paramFloat = getScrollY();
    return getLayout().getLineForVertical((int)(f + paramFloat));
  }
  
  int getLineAtCoordinateUnclamped(float paramFloat) {
    float f1 = getTotalPaddingTop();
    float f2 = getScrollY();
    return getLayout().getLineForVertical((int)(paramFloat - f1 + f2));
  }
  
  int getOffsetAtCoordinate(int paramInt, float paramFloat) {
    paramFloat = convertToLocalHorizontalCoordinate(paramFloat);
    return getLayout().getOffsetForHorizontal(paramInt, paramFloat);
  }
  
  public boolean onDragEvent(DragEvent paramDragEvent) {
    int i = paramDragEvent.getAction();
    boolean bool = true;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 5)
            return true; 
          requestFocus();
          return true;
        } 
        Editor editor1 = this.mEditor;
        if (editor1 != null)
          editor1.onDrop(paramDragEvent); 
        return true;
      } 
      if (this.mText instanceof Spannable) {
        i = getOffsetForPosition(paramDragEvent.getX(), paramDragEvent.getY());
        Selection.setSelection(this.mSpannable, i);
      } 
      return true;
    } 
    Editor editor = this.mEditor;
    if (editor == null || !editor.hasInsertionController())
      bool = false; 
    return bool;
  }
  
  boolean isInBatchEditMode() {
    Editor editor = this.mEditor;
    boolean bool = false;
    if (editor == null)
      return false; 
    Editor.InputMethodState inputMethodState = editor.mInputMethodState;
    if (inputMethodState != null) {
      if (inputMethodState.mBatchEditNesting > 0)
        bool = true; 
      return bool;
    } 
    return this.mEditor.mInBatchEditControllers;
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    TextDirectionHeuristic textDirectionHeuristic = getTextDirectionHeuristic();
    if (this.mTextDir != textDirectionHeuristic) {
      this.mTextDir = textDirectionHeuristic;
      if (this.mLayout != null)
        checkForRelayout(); 
    } 
  }
  
  public TextDirectionHeuristic getTextDirectionHeuristic() {
    if (hasPasswordTransformationMethod())
      return TextDirectionHeuristics.LTR; 
    Editor editor = this.mEditor;
    boolean bool = true;
    if (editor != null && (editor.mInputType & 0xF) == 3) {
      DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance(getTextLocale());
      String str = decimalFormatSymbols.getDigitStrings()[0];
      int i = str.codePointAt(0);
      i = Character.getDirectionality(i);
      if (i == 1 || i == 2)
        return TextDirectionHeuristics.RTL; 
      return TextDirectionHeuristics.LTR;
    } 
    if (getLayoutDirection() != 1)
      bool = false; 
    switch (getTextDirection()) {
      default:
        return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).getTextDirectionHeuristicForTextView(bool);
      case 7:
        return TextDirectionHeuristics.FIRSTSTRONG_RTL;
      case 6:
        return TextDirectionHeuristics.FIRSTSTRONG_LTR;
      case 5:
        return TextDirectionHeuristics.LOCALE;
      case 4:
        return TextDirectionHeuristics.RTL;
      case 3:
        return TextDirectionHeuristics.LTR;
      case 2:
        break;
    } 
    return TextDirectionHeuristics.ANYRTL_LTR;
  }
  
  public void onResolveDrawables(int paramInt) {
    if (this.mLastLayoutDirection == paramInt)
      return; 
    this.mLastLayoutDirection = paramInt;
    Drawables drawables = this.mDrawables;
    if (drawables != null && 
      drawables.resolveWithLayoutDirection(paramInt)) {
      prepareDrawableForDisplay(this.mDrawables.mShowing[0]);
      prepareDrawableForDisplay(this.mDrawables.mShowing[2]);
      applyCompoundDrawableTint();
    } 
  }
  
  private void prepareDrawableForDisplay(Drawable paramDrawable) {
    if (paramDrawable == null)
      return; 
    paramDrawable.setLayoutDirection(getLayoutDirection());
    if (paramDrawable.isStateful()) {
      paramDrawable.setState(getDrawableState());
      paramDrawable.jumpToCurrentState();
    } 
  }
  
  protected void resetResolvedDrawables() {
    super.resetResolvedDrawables();
    this.mLastLayoutDirection = -1;
  }
  
  protected void viewClicked(InputMethodManager paramInputMethodManager) {
    if (paramInputMethodManager != null)
      paramInputMethodManager.viewClicked(this); 
  }
  
  protected void deleteText_internal(int paramInt1, int paramInt2) {
    ((Editable)this.mText).delete(paramInt1, paramInt2);
  }
  
  protected void replaceText_internal(int paramInt1, int paramInt2, CharSequence paramCharSequence) {
    ((Editable)this.mText).replace(paramInt1, paramInt2, paramCharSequence);
  }
  
  protected void setSpan_internal(Object paramObject, int paramInt1, int paramInt2, int paramInt3) {
    ((Editable)this.mText).setSpan(paramObject, paramInt1, paramInt2, paramInt3);
  }
  
  protected void setCursorPosition_internal(int paramInt1, int paramInt2) {
    Selection.setSelection((Editable)this.mText, paramInt1, paramInt2);
  }
  
  private void createEditorIfNeeded() {
    if (this.mEditor == null)
      this.mEditor = new Editor(this); 
  }
  
  public CharSequence getIterableTextForAccessibility() {
    return this.mText;
  }
  
  private void ensureIterableTextForAccessibilitySelectable() {
    CharSequence charSequence = this.mText;
    if (!(charSequence instanceof Spannable))
      setText(charSequence, BufferType.SPANNABLE); 
  }
  
  public AccessibilityIterators.TextSegmentIterator getIteratorForGranularity(int paramInt) {
    if (paramInt != 4) {
      if (paramInt == 16) {
        Spannable spannable = (Spannable)getIterableTextForAccessibility();
        if (!TextUtils.isEmpty(spannable) && getLayout() != null) {
          AccessibilityIterators.PageTextSegmentIterator pageTextSegmentIterator = AccessibilityIterators.PageTextSegmentIterator.getInstance();
          pageTextSegmentIterator.initialize(this);
          return pageTextSegmentIterator;
        } 
      } 
    } else {
      Spannable spannable = (Spannable)getIterableTextForAccessibility();
      if (!TextUtils.isEmpty(spannable) && getLayout() != null) {
        AccessibilityIterators.LineTextSegmentIterator lineTextSegmentIterator = AccessibilityIterators.LineTextSegmentIterator.getInstance();
        lineTextSegmentIterator.initialize(spannable, getLayout());
        return lineTextSegmentIterator;
      } 
    } 
    return super.getIteratorForGranularity(paramInt);
  }
  
  public int getAccessibilitySelectionStart() {
    return getSelectionStart();
  }
  
  public boolean isAccessibilitySelectionExtendable() {
    return true;
  }
  
  public int getAccessibilitySelectionEnd() {
    return getSelectionEnd();
  }
  
  public void setAccessibilitySelection(int paramInt1, int paramInt2) {
    if (getAccessibilitySelectionStart() == paramInt1 && 
      getAccessibilitySelectionEnd() == paramInt2)
      return; 
    CharSequence charSequence = getIterableTextForAccessibility();
    if (Math.min(paramInt1, paramInt2) >= 0 && Math.max(paramInt1, paramInt2) <= charSequence.length()) {
      Selection.setSelection((Spannable)charSequence, paramInt1, paramInt2);
    } else {
      Selection.removeSelection((Spannable)charSequence);
    } 
    Editor editor = this.mEditor;
    if (editor != null) {
      editor.hideCursorAndSpanControllers();
      this.mEditor.stopTextActionMode();
    } 
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    String str;
    super.encodeProperties(paramViewHierarchyEncoder);
    TextUtils.TruncateAt truncateAt = getEllipsize();
    CharSequence charSequence2 = null;
    if (truncateAt == null) {
      truncateAt = null;
    } else {
      str = truncateAt.name();
    } 
    paramViewHierarchyEncoder.addProperty("text:ellipsize", str);
    paramViewHierarchyEncoder.addProperty("text:textSize", getTextSize());
    paramViewHierarchyEncoder.addProperty("text:scaledTextSize", getScaledTextSize());
    paramViewHierarchyEncoder.addProperty("text:typefaceStyle", getTypefaceStyle());
    paramViewHierarchyEncoder.addProperty("text:selectionStart", getSelectionStart());
    paramViewHierarchyEncoder.addProperty("text:selectionEnd", getSelectionEnd());
    paramViewHierarchyEncoder.addProperty("text:curTextColor", this.mCurTextColor);
    CharSequence charSequence1 = this.mText;
    if (charSequence1 == null) {
      charSequence1 = charSequence2;
    } else {
      charSequence1 = charSequence1.toString();
    } 
    paramViewHierarchyEncoder.addUserProperty("text:text", (String)charSequence1);
    paramViewHierarchyEncoder.addProperty("text:gravity", this.mGravity);
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState() {
      this.selStart = -1;
      this.selEnd = -1;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.selStart);
      param1Parcel.writeInt(this.selEnd);
      param1Parcel.writeInt(this.frozenWithFocus);
      TextUtils.writeToParcel(this.text, param1Parcel, param1Int);
      if (this.error == null) {
        param1Parcel.writeInt(0);
      } else {
        param1Parcel.writeInt(1);
        TextUtils.writeToParcel(this.error, param1Parcel, param1Int);
      } 
      if (this.editorState == null) {
        param1Parcel.writeInt(0);
      } else {
        param1Parcel.writeInt(1);
        this.editorState.writeToParcel(param1Parcel, param1Int);
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("TextView.SavedState{");
      stringBuilder1.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder1.append(" start=");
      stringBuilder1.append(this.selStart);
      stringBuilder1.append(" end=");
      stringBuilder1.append(this.selEnd);
      String str2 = stringBuilder1.toString();
      String str1 = str2;
      if (this.text != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str2);
        stringBuilder.append(" text=");
        stringBuilder.append(this.text);
        str1 = stringBuilder.toString();
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(str1);
      stringBuilder2.append("}");
      return stringBuilder2.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    ParcelableParcel editorState;
    
    CharSequence error;
    
    boolean frozenWithFocus;
    
    int selEnd;
    
    int selStart;
    
    CharSequence text;
    
    private SavedState(TextView this$0) {
      boolean bool;
      this.selStart = -1;
      this.selEnd = -1;
      this.selStart = this$0.readInt();
      this.selEnd = this$0.readInt();
      if (this$0.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.frozenWithFocus = bool;
      this.text = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)this$0);
      if (this$0.readInt() != 0)
        this.error = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)this$0); 
      if (this$0.readInt() != 0)
        this.editorState = (ParcelableParcel)ParcelableParcel.CREATOR.createFromParcel((Parcel)this$0); 
    }
  }
  
  class CharWrapper implements CharSequence, GetChars, GraphicsOperations {
    private char[] mChars;
    
    private int mLength;
    
    private int mStart;
    
    public CharWrapper(TextView this$0, int param1Int1, int param1Int2) {
      this.mChars = (char[])this$0;
      this.mStart = param1Int1;
      this.mLength = param1Int2;
    }
    
    void set(char[] param1ArrayOfchar, int param1Int1, int param1Int2) {
      this.mChars = param1ArrayOfchar;
      this.mStart = param1Int1;
      this.mLength = param1Int2;
    }
    
    public int length() {
      return this.mLength;
    }
    
    public char charAt(int param1Int) {
      return this.mChars[this.mStart + param1Int];
    }
    
    public String toString() {
      return new String(this.mChars, this.mStart, this.mLength);
    }
    
    public CharSequence subSequence(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0 && param1Int2 >= 0) {
        int i = this.mLength;
        if (param1Int1 <= i && param1Int2 <= i)
          return new String(this.mChars, this.mStart + param1Int1, param1Int2 - param1Int1); 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1Int1);
      stringBuilder.append(", ");
      stringBuilder.append(param1Int2);
      throw new IndexOutOfBoundsException(stringBuilder.toString());
    }
    
    public void getChars(int param1Int1, int param1Int2, char[] param1ArrayOfchar, int param1Int3) {
      if (param1Int1 >= 0 && param1Int2 >= 0) {
        int i = this.mLength;
        if (param1Int1 <= i && param1Int2 <= i) {
          System.arraycopy(this.mChars, this.mStart + param1Int1, param1ArrayOfchar, param1Int3, param1Int2 - param1Int1);
          return;
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1Int1);
      stringBuilder.append(", ");
      stringBuilder.append(param1Int2);
      throw new IndexOutOfBoundsException(stringBuilder.toString());
    }
    
    public void drawText(BaseCanvas param1BaseCanvas, int param1Int1, int param1Int2, float param1Float1, float param1Float2, Paint param1Paint) {
      param1BaseCanvas.drawText(this.mChars, param1Int1 + this.mStart, param1Int2 - param1Int1, param1Float1, param1Float2, param1Paint);
    }
    
    public void drawTextRun(BaseCanvas param1BaseCanvas, int param1Int1, int param1Int2, int param1Int3, int param1Int4, float param1Float1, float param1Float2, boolean param1Boolean, Paint param1Paint) {
      char[] arrayOfChar = this.mChars;
      int i = this.mStart;
      param1BaseCanvas.drawTextRun(arrayOfChar, param1Int1 + i, param1Int2 - param1Int1, param1Int3 + i, param1Int4 - param1Int3, param1Float1, param1Float2, param1Boolean, param1Paint);
    }
    
    public float measureText(int param1Int1, int param1Int2, Paint param1Paint) {
      return param1Paint.measureText(this.mChars, this.mStart + param1Int1, param1Int2 - param1Int1);
    }
    
    public int getTextWidths(int param1Int1, int param1Int2, float[] param1ArrayOffloat, Paint param1Paint) {
      return param1Paint.getTextWidths(this.mChars, this.mStart + param1Int1, param1Int2 - param1Int1, param1ArrayOffloat);
    }
    
    public float getTextRunAdvances(int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean, float[] param1ArrayOffloat, int param1Int5, Paint param1Paint) {
      char[] arrayOfChar = this.mChars;
      int i = this.mStart;
      return param1Paint.getTextRunAdvances(arrayOfChar, param1Int1 + i, param1Int2 - param1Int1, param1Int3 + i, param1Int4 - param1Int3, param1Boolean, param1ArrayOffloat, param1Int5);
    }
    
    public int getTextRunCursor(int param1Int1, int param1Int2, boolean param1Boolean, int param1Int3, int param1Int4, Paint param1Paint) {
      char[] arrayOfChar = this.mChars;
      int i = this.mStart;
      return param1Paint.getTextRunCursor(arrayOfChar, param1Int1 + i, param1Int2 - param1Int1, param1Boolean, param1Int3 + i, param1Int4);
    }
  }
  
  class Marquee {
    private static final int MARQUEE_DELAY = 1200;
    
    private static final float MARQUEE_DELTA_MAX = 0.07F;
    
    private static final int MARQUEE_DP_PER_SECOND = 30;
    
    private static final byte MARQUEE_RUNNING = 2;
    
    private static final byte MARQUEE_STARTING = 1;
    
    private static final byte MARQUEE_STOPPED = 0;
    
    private final Choreographer mChoreographer;
    
    private float mFadeStop;
    
    private float mGhostOffset;
    
    private float mGhostStart;
    
    private long mLastAnimationMs;
    
    private float mMaxFadeScroll;
    
    private float mMaxScroll;
    
    private final float mPixelsPerMs;
    
    private int mRepeatLimit;
    
    private Choreographer.FrameCallback mRestartCallback;
    
    private float mScroll;
    
    private Choreographer.FrameCallback mStartCallback;
    
    private byte mStatus = 0;
    
    private Choreographer.FrameCallback mTickCallback;
    
    private final WeakReference<TextView> mView;
    
    Marquee(TextView this$0) {
      this.mTickCallback = (Choreographer.FrameCallback)new Object(this);
      this.mStartCallback = (Choreographer.FrameCallback)new Object(this);
      this.mRestartCallback = (Choreographer.FrameCallback)new Object(this);
      float f = (this$0.getContext().getResources().getDisplayMetrics()).density;
      this.mPixelsPerMs = 30.0F * f / 1000.0F;
      this.mView = new WeakReference<>(this$0);
      this.mChoreographer = Choreographer.getInstance();
    }
    
    void tick() {
      if (this.mStatus != 2)
        return; 
      this.mChoreographer.removeFrameCallback(this.mTickCallback);
      TextView textView = this.mView.get();
      if (textView != null && (textView.isFocused() || textView.isSelected())) {
        long l1 = this.mChoreographer.getFrameTime();
        long l2 = this.mLastAnimationMs;
        this.mLastAnimationMs = l1;
        float f1 = (float)(l1 - l2), f2 = this.mPixelsPerMs;
        this.mScroll = f2 = this.mScroll + f1 * f2;
        f1 = this.mMaxScroll;
        if (f2 > f1) {
          this.mScroll = f1;
          this.mChoreographer.postFrameCallbackDelayed(this.mRestartCallback, 1200L);
        } else {
          this.mChoreographer.postFrameCallback(this.mTickCallback);
        } 
        textView.invalidate();
      } 
    }
    
    void stop() {
      this.mStatus = 0;
      this.mChoreographer.removeFrameCallback(this.mStartCallback);
      this.mChoreographer.removeFrameCallback(this.mRestartCallback);
      this.mChoreographer.removeFrameCallback(this.mTickCallback);
      resetScroll();
    }
    
    private void resetScroll() {
      this.mScroll = 0.0F;
      TextView textView = this.mView.get();
      if (textView != null)
        textView.invalidate(); 
    }
    
    void start(int param1Int) {
      if (param1Int == 0) {
        stop();
        return;
      } 
      this.mRepeatLimit = param1Int;
      TextView textView = this.mView.get();
      if (textView != null && textView.mLayout != null) {
        this.mStatus = 1;
        this.mScroll = 0.0F;
        param1Int = textView.getWidth();
        int i = textView.getCompoundPaddingLeft();
        param1Int = param1Int - i - textView.getCompoundPaddingRight();
        float f1 = textView.mLayout.getLineWidth(0);
        float f2 = param1Int / 3.0F;
        float f3 = f1 - param1Int + f2;
        this.mMaxScroll = param1Int + f3;
        this.mGhostOffset = f1 + f2;
        this.mFadeStop = param1Int / 6.0F + f1;
        this.mMaxFadeScroll = f3 + f1 + f1;
        textView.invalidate();
        this.mChoreographer.postFrameCallback(this.mStartCallback);
      } 
    }
    
    float getGhostOffset() {
      return this.mGhostOffset;
    }
    
    float getScroll() {
      return this.mScroll;
    }
    
    float getMaxFadeScroll() {
      return this.mMaxFadeScroll;
    }
    
    boolean shouldDrawLeftFade() {
      boolean bool;
      if (this.mScroll <= this.mFadeStop) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean shouldDrawGhost() {
      boolean bool;
      if (this.mStatus == 2 && this.mScroll > this.mGhostStart) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean isRunning() {
      boolean bool;
      if (this.mStatus == 2) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean isStopped() {
      boolean bool;
      if (this.mStatus == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  class ChangeWatcher implements TextWatcher, SpanWatcher {
    private CharSequence mBeforeText;
    
    final TextView this$0;
    
    private ChangeWatcher() {}
    
    public void beforeTextChanged(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3) {
      if (AccessibilityManager.getInstance(TextView.this.mContext).isEnabled() && TextView.this.mTransformed != null)
        this.mBeforeText = TextView.this.mTransformed.toString(); 
      TextView.this.sendBeforeTextChanged(param1CharSequence, param1Int1, param1Int2, param1Int3);
    }
    
    public void onTextChanged(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3) {
      TextView.this.handleTextChanged(param1CharSequence, param1Int1, param1Int2, param1Int3);
      if (AccessibilityManager.getInstance(TextView.this.mContext).isEnabled()) {
        TextView textView = TextView.this;
        if (textView.isFocused() || (TextView.this.isSelected() && TextView.this.isShown())) {
          TextView.this.sendAccessibilityEventTypeViewTextChanged(this.mBeforeText, param1Int1, param1Int2, param1Int3);
          this.mBeforeText = null;
        } 
      } 
    }
    
    public void afterTextChanged(Editable param1Editable) {
      TextView.this.sendAfterTextChanged(param1Editable);
      if (MetaKeyKeyListener.getMetaState(param1Editable, 2048) != 0)
        MetaKeyKeyListener.stopSelecting(TextView.this, param1Editable); 
    }
    
    public void onSpanChanged(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      TextView.this.spanChange(param1Spannable, param1Object, param1Int1, param1Int3, param1Int2, param1Int4);
    }
    
    public void onSpanAdded(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2) {
      TextView.this.spanChange(param1Spannable, param1Object, -1, param1Int1, -1, param1Int2);
    }
    
    public void onSpanRemoved(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2) {
      TextView.this.spanChange(param1Spannable, param1Object, param1Int1, -1, param1Int2, -1);
    }
  }
  
  private static void logCursor(String paramString1, String paramString2, Object... paramVarArgs) {
    if (paramString2 == null) {
      Log.d("TextView", paramString1);
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(String.format(paramString2, paramVarArgs));
      Log.d("TextView", stringBuilder.toString());
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AutoSizeTextType implements Annotation {}
  
  class OnEditorActionListener {
    public abstract boolean onEditorAction(TextView param1TextView, int param1Int, KeyEvent param1KeyEvent);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class XMLTypefaceAttr implements Annotation {}
}
