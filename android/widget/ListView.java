package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Trace;
import android.util.AttributeSet;
import android.util.Log;
import android.util.MathUtils;
import android.util.SparseBooleanArray;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.RemotableViewMethod;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.R;
import com.google.android.collect.Lists;
import com.oplus.rp.bridge.IOplusRedPacketManager;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@RemoteView
public class ListView extends AbsListView {
  class FixedViewInfo {
    public Object data;
    
    public boolean isSelectable;
    
    final ListView this$0;
    
    public View view;
  }
  
  ArrayList<FixedViewInfo> mHeaderViewInfos = Lists.newArrayList();
  
  ArrayList<FixedViewInfo> mFooterViewInfos = Lists.newArrayList();
  
  private boolean mAreAllItemsSelectable = true;
  
  private boolean mItemsCanFocus = false;
  
  private final Rect mTempRect = new Rect();
  
  private final ArrowScrollFocusResult mArrowScrollFocusResult = new ArrowScrollFocusResult();
  
  private static final float MAX_SCROLL_FACTOR = 0.33F;
  
  private static final int MIN_SCROLL_PREVIEW_PIXELS = 2;
  
  static final int NO_POSITION = -1;
  
  static final String TAG = "ListView";
  
  Drawable mDivider;
  
  int mDividerHeight;
  
  private boolean mDividerIsOpaque;
  
  private Paint mDividerPaint;
  
  private FocusSelector mFocusSelector;
  
  private boolean mFooterDividersEnabled;
  
  private boolean mHeaderDividersEnabled;
  
  private boolean mIsCacheColorOpaque;
  
  Drawable mOverScrollFooter;
  
  Drawable mOverScrollHeader;
  
  public ListView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ListView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842868);
  }
  
  public ListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ListView, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ListView, paramAttributeSet, typedArray, paramInt1, paramInt2);
    CharSequence[] arrayOfCharSequence = typedArray.getTextArray(0);
    if (arrayOfCharSequence != null)
      setAdapter(new ArrayAdapter<>(paramContext, 17367043, arrayOfCharSequence)); 
    Drawable drawable = typedArray.getDrawable(1);
    if (drawable != null)
      setDivider(drawable); 
    drawable = typedArray.getDrawable(5);
    if (drawable != null)
      setOverscrollHeader(drawable); 
    drawable = typedArray.getDrawable(6);
    if (drawable != null)
      setOverscrollFooter(drawable); 
    if (typedArray.hasValueOrEmpty(2)) {
      paramInt1 = typedArray.getDimensionPixelSize(2, 0);
      if (paramInt1 != 0)
        setDividerHeight(paramInt1); 
    } 
    this.mHeaderDividersEnabled = typedArray.getBoolean(3, true);
    this.mFooterDividersEnabled = typedArray.getBoolean(4, true);
    typedArray.recycle();
  }
  
  public int getMaxScrollAmount() {
    return (int)((this.mBottom - this.mTop) * 0.33F);
  }
  
  private void adjustViewsUpOrDown() {
    int i = getChildCount();
    if (i > 0) {
      int j;
      if (!this.mStackFromBottom) {
        View view = getChildAt(0);
        j = view.getTop() - this.mListPadding.top;
        int k = j;
        if (this.mFirstPosition != 0)
          k = j - this.mDividerHeight; 
        j = k;
        if (k < 0)
          j = 0; 
      } else {
        View view = getChildAt(i - 1);
        j = view.getBottom() - getHeight() - this.mListPadding.bottom;
        int k = j;
        if (this.mFirstPosition + i < this.mItemCount)
          k = j + this.mDividerHeight; 
        j = k;
        if (k > 0)
          j = 0; 
      } 
      if (j != 0)
        offsetChildrenTopAndBottom(-j); 
    } 
  }
  
  public void addHeaderView(View paramView, Object paramObject, boolean paramBoolean) {
    if (paramView.getParent() != null && paramView.getParent() != this && 
      Log.isLoggable("ListView", 5))
      Log.w("ListView", "The specified child already has a parent. You must call removeView() on the child's parent first."); 
    FixedViewInfo fixedViewInfo = new FixedViewInfo();
    fixedViewInfo.view = paramView;
    fixedViewInfo.data = paramObject;
    fixedViewInfo.isSelectable = paramBoolean;
    this.mHeaderViewInfos.add(fixedViewInfo);
    this.mAreAllItemsSelectable &= paramBoolean;
    if (this.mAdapter != null) {
      if (!(this.mAdapter instanceof HeaderViewListAdapter))
        wrapHeaderListAdapterInternal(); 
      if (this.mDataSetObserver != null)
        this.mDataSetObserver.onChanged(); 
    } 
  }
  
  public void addHeaderView(View paramView) {
    addHeaderView(paramView, (Object)null, true);
  }
  
  public int getHeaderViewsCount() {
    return this.mHeaderViewInfos.size();
  }
  
  public boolean removeHeaderView(View paramView) {
    if (this.mHeaderViewInfos.size() > 0) {
      boolean bool1 = false;
      boolean bool2 = bool1;
      if (this.mAdapter != null) {
        bool2 = bool1;
        if (((HeaderViewListAdapter)this.mAdapter).removeHeader(paramView)) {
          if (this.mDataSetObserver != null)
            this.mDataSetObserver.onChanged(); 
          bool2 = true;
        } 
      } 
      removeFixedViewInfo(paramView, this.mHeaderViewInfos);
      return bool2;
    } 
    return false;
  }
  
  private void removeFixedViewInfo(View paramView, ArrayList<FixedViewInfo> paramArrayList) {
    int i = paramArrayList.size();
    for (byte b = 0; b < i; b++) {
      FixedViewInfo fixedViewInfo = paramArrayList.get(b);
      if (fixedViewInfo.view == paramView) {
        paramArrayList.remove(b);
        break;
      } 
    } 
  }
  
  public void addFooterView(View paramView, Object paramObject, boolean paramBoolean) {
    if (paramView.getParent() != null && paramView.getParent() != this && 
      Log.isLoggable("ListView", 5))
      Log.w("ListView", "The specified child already has a parent. You must call removeView() on the child's parent first."); 
    FixedViewInfo fixedViewInfo = new FixedViewInfo();
    fixedViewInfo.view = paramView;
    fixedViewInfo.data = paramObject;
    fixedViewInfo.isSelectable = paramBoolean;
    this.mFooterViewInfos.add(fixedViewInfo);
    this.mAreAllItemsSelectable &= paramBoolean;
    if (this.mAdapter != null) {
      if (!(this.mAdapter instanceof HeaderViewListAdapter))
        wrapHeaderListAdapterInternal(); 
      if (this.mDataSetObserver != null)
        this.mDataSetObserver.onChanged(); 
    } 
  }
  
  public void addFooterView(View paramView) {
    addFooterView(paramView, (Object)null, true);
  }
  
  public int getFooterViewsCount() {
    return this.mFooterViewInfos.size();
  }
  
  public boolean removeFooterView(View paramView) {
    if (this.mFooterViewInfos.size() > 0) {
      boolean bool1 = false;
      boolean bool2 = bool1;
      if (this.mAdapter != null) {
        bool2 = bool1;
        if (((HeaderViewListAdapter)this.mAdapter).removeFooter(paramView)) {
          if (this.mDataSetObserver != null)
            this.mDataSetObserver.onChanged(); 
          bool2 = true;
        } 
      } 
      removeFixedViewInfo(paramView, this.mFooterViewInfos);
      return bool2;
    } 
    return false;
  }
  
  public ListAdapter getAdapter() {
    return this.mAdapter;
  }
  
  @RemotableViewMethod(asyncImpl = "setRemoteViewsAdapterAsync")
  public void setRemoteViewsAdapter(Intent paramIntent) {
    super.setRemoteViewsAdapter(paramIntent);
  }
  
  public void setAdapter(ListAdapter paramListAdapter) {
    if (this.mAdapter != null && this.mDataSetObserver != null)
      this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver); 
    resetList();
    this.mRecycler.clear();
    if (this.mHeaderViewInfos.size() > 0 || this.mFooterViewInfos.size() > 0) {
      this.mAdapter = wrapHeaderListAdapterInternal(this.mHeaderViewInfos, this.mFooterViewInfos, paramListAdapter);
    } else {
      this.mAdapter = paramListAdapter;
    } 
    this.mOldSelectedPosition = -1;
    this.mOldSelectedRowId = Long.MIN_VALUE;
    super.setAdapter(paramListAdapter);
    if (this.mAdapter != null) {
      int i;
      this.mAreAllItemsSelectable = this.mAdapter.areAllItemsEnabled();
      this.mOldItemCount = this.mItemCount;
      this.mItemCount = this.mAdapter.getCount();
      checkFocus();
      this.mDataSetObserver = new AbsListView.AdapterDataSetObserver(this);
      this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
      this.mRecycler.setViewTypeCount(this.mAdapter.getViewTypeCount());
      if (this.mStackFromBottom) {
        i = lookForSelectablePosition(this.mItemCount - 1, false);
      } else {
        i = lookForSelectablePosition(0, true);
      } 
      setSelectedPositionInt(i);
      setNextSelectedPositionInt(i);
      if (this.mItemCount == 0)
        checkSelectionChanged(); 
    } else {
      this.mAreAllItemsSelectable = true;
      checkFocus();
      checkSelectionChanged();
    } 
    requestLayout();
  }
  
  void resetList() {
    clearRecycledState(this.mHeaderViewInfos);
    clearRecycledState(this.mFooterViewInfos);
    super.resetList();
    this.mLayoutMode = 0;
  }
  
  private void clearRecycledState(ArrayList<FixedViewInfo> paramArrayList) {
    if (paramArrayList != null) {
      int i = paramArrayList.size();
      for (byte b = 0; b < i; b++) {
        View view = ((FixedViewInfo)paramArrayList.get(b)).view;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (checkLayoutParams(layoutParams))
          ((AbsListView.LayoutParams)layoutParams).recycledHeaderFooter = false; 
      } 
    } 
  }
  
  private boolean showingTopFadingEdge() {
    int i = this.mScrollY, j = this.mListPadding.top;
    int k = this.mFirstPosition;
    boolean bool = false;
    if (k > 0 || getChildAt(0).getTop() > i + j)
      bool = true; 
    return bool;
  }
  
  private boolean showingBottomFadingEdge() {
    int i = getChildCount();
    int j = getChildAt(i - 1).getBottom();
    int k = this.mFirstPosition;
    boolean bool1 = true;
    int m = this.mScrollY, n = getHeight(), i1 = this.mListPadding.bottom;
    boolean bool2 = bool1;
    if (k + i - 1 >= this.mItemCount - 1)
      if (j < m + n - i1) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean) {
    // Byte code:
    //   0: aload_2
    //   1: getfield top : I
    //   4: istore #4
    //   6: aload_2
    //   7: aload_1
    //   8: invokevirtual getLeft : ()I
    //   11: aload_1
    //   12: invokevirtual getTop : ()I
    //   15: invokevirtual offset : (II)V
    //   18: aload_2
    //   19: aload_1
    //   20: invokevirtual getScrollX : ()I
    //   23: ineg
    //   24: aload_1
    //   25: invokevirtual getScrollY : ()I
    //   28: ineg
    //   29: invokevirtual offset : (II)V
    //   32: aload_0
    //   33: invokevirtual getHeight : ()I
    //   36: istore #5
    //   38: aload_0
    //   39: invokevirtual getScrollY : ()I
    //   42: istore #6
    //   44: iload #6
    //   46: iload #5
    //   48: iadd
    //   49: istore #7
    //   51: aload_0
    //   52: invokevirtual getVerticalFadingEdgeLength : ()I
    //   55: istore #8
    //   57: iload #6
    //   59: istore #9
    //   61: aload_0
    //   62: invokespecial showingTopFadingEdge : ()Z
    //   65: ifeq -> 93
    //   68: aload_0
    //   69: getfield mSelectedPosition : I
    //   72: ifgt -> 86
    //   75: iload #6
    //   77: istore #9
    //   79: iload #4
    //   81: iload #8
    //   83: if_icmple -> 93
    //   86: iload #6
    //   88: iload #8
    //   90: iadd
    //   91: istore #9
    //   93: aload_0
    //   94: invokevirtual getChildCount : ()I
    //   97: istore #6
    //   99: aload_0
    //   100: iload #6
    //   102: iconst_1
    //   103: isub
    //   104: invokevirtual getChildAt : (I)Landroid/view/View;
    //   107: invokevirtual getBottom : ()I
    //   110: istore #4
    //   112: aload_0
    //   113: invokespecial showingBottomFadingEdge : ()Z
    //   116: istore #10
    //   118: iconst_1
    //   119: istore_3
    //   120: iload #7
    //   122: istore #6
    //   124: iload #10
    //   126: ifeq -> 165
    //   129: aload_0
    //   130: getfield mSelectedPosition : I
    //   133: aload_0
    //   134: getfield mItemCount : I
    //   137: iconst_1
    //   138: isub
    //   139: if_icmplt -> 158
    //   142: iload #7
    //   144: istore #6
    //   146: aload_2
    //   147: getfield bottom : I
    //   150: iload #4
    //   152: iload #8
    //   154: isub
    //   155: if_icmpge -> 165
    //   158: iload #7
    //   160: iload #8
    //   162: isub
    //   163: istore #6
    //   165: iconst_0
    //   166: istore #8
    //   168: aload_2
    //   169: getfield bottom : I
    //   172: iload #6
    //   174: if_icmple -> 235
    //   177: aload_2
    //   178: getfield top : I
    //   181: iload #9
    //   183: if_icmple -> 235
    //   186: aload_2
    //   187: invokevirtual height : ()I
    //   190: iload #5
    //   192: if_icmple -> 209
    //   195: iconst_0
    //   196: aload_2
    //   197: getfield top : I
    //   200: iload #9
    //   202: isub
    //   203: iadd
    //   204: istore #7
    //   206: goto -> 220
    //   209: iconst_0
    //   210: aload_2
    //   211: getfield bottom : I
    //   214: iload #6
    //   216: isub
    //   217: iadd
    //   218: istore #7
    //   220: iload #7
    //   222: iload #4
    //   224: iload #6
    //   226: isub
    //   227: invokestatic min : (II)I
    //   230: istore #7
    //   232: goto -> 317
    //   235: iload #8
    //   237: istore #7
    //   239: aload_2
    //   240: getfield top : I
    //   243: iload #9
    //   245: if_icmpge -> 232
    //   248: iload #8
    //   250: istore #7
    //   252: aload_2
    //   253: getfield bottom : I
    //   256: iload #6
    //   258: if_icmpge -> 232
    //   261: aload_2
    //   262: invokevirtual height : ()I
    //   265: iload #5
    //   267: if_icmple -> 284
    //   270: iconst_0
    //   271: iload #6
    //   273: aload_2
    //   274: getfield bottom : I
    //   277: isub
    //   278: isub
    //   279: istore #7
    //   281: goto -> 295
    //   284: iconst_0
    //   285: iload #9
    //   287: aload_2
    //   288: getfield top : I
    //   291: isub
    //   292: isub
    //   293: istore #7
    //   295: aload_0
    //   296: iconst_0
    //   297: invokevirtual getChildAt : (I)Landroid/view/View;
    //   300: invokevirtual getTop : ()I
    //   303: istore #6
    //   305: iload #7
    //   307: iload #6
    //   309: iload #9
    //   311: isub
    //   312: invokestatic max : (II)I
    //   315: istore #7
    //   317: iload #7
    //   319: ifeq -> 325
    //   322: goto -> 327
    //   325: iconst_0
    //   326: istore_3
    //   327: iload_3
    //   328: ifeq -> 356
    //   331: aload_0
    //   332: iload #7
    //   334: ineg
    //   335: invokespecial scrollListItemsBy : (I)V
    //   338: aload_0
    //   339: iconst_m1
    //   340: aload_1
    //   341: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   344: aload_0
    //   345: aload_1
    //   346: invokevirtual getTop : ()I
    //   349: putfield mSelectedTop : I
    //   352: aload_0
    //   353: invokevirtual invalidate : ()V
    //   356: iload_3
    //   357: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #673	-> 0
    //   #676	-> 6
    //   #677	-> 18
    //   #679	-> 32
    //   #680	-> 38
    //   #681	-> 44
    //   #682	-> 51
    //   #684	-> 57
    //   #686	-> 68
    //   #687	-> 86
    //   #691	-> 93
    //   #692	-> 99
    //   #694	-> 112
    //   #696	-> 129
    //   #698	-> 158
    //   #702	-> 165
    //   #704	-> 168
    //   #709	-> 186
    //   #711	-> 195
    //   #714	-> 209
    //   #718	-> 220
    //   #719	-> 220
    //   #720	-> 232
    //   #725	-> 261
    //   #727	-> 270
    //   #730	-> 284
    //   #734	-> 295
    //   #735	-> 305
    //   #736	-> 305
    //   #739	-> 317
    //   #740	-> 327
    //   #741	-> 331
    //   #742	-> 338
    //   #743	-> 344
    //   #744	-> 352
    //   #746	-> 356
  }
  
  void fillGap(boolean paramBoolean) {
    int i = getChildCount();
    if (paramBoolean) {
      int j = 0;
      if ((this.mGroupFlags & 0x22) == 34)
        j = getListPaddingTop(); 
      if (i > 0)
        j = getChildAt(i - 1).getBottom() + this.mDividerHeight; 
      fillDown(this.mFirstPosition + i, j);
      correctTooHigh(getChildCount());
    } else {
      int j = 0;
      if ((this.mGroupFlags & 0x22) == 34)
        j = getListPaddingBottom(); 
      if (i > 0) {
        j = getChildAt(0).getTop() - this.mDividerHeight;
      } else {
        j = getHeight() - j;
      } 
      fillUp(this.mFirstPosition - 1, j);
      correctTooLow(getChildCount());
    } 
  }
  
  private View fillDown(int paramInt1, int paramInt2) {
    View view1 = null;
    int i = this.mBottom - this.mTop;
    View view2 = view1;
    int j = i, k = paramInt1, m = paramInt2;
    if ((this.mGroupFlags & 0x22) == 34) {
      j = i - this.mListPadding.bottom;
      m = paramInt2;
      k = paramInt1;
      view2 = view1;
    } 
    while (true) {
      boolean bool = true;
      if (m < j && k < this.mItemCount) {
        if (k != this.mSelectedPosition)
          bool = false; 
        view1 = makeAndAddView(k, m, true, this.mListPadding.left, bool);
        m = view1.getBottom() + this.mDividerHeight;
        if (bool)
          view2 = view1; 
        k++;
        continue;
      } 
      break;
    } 
    setVisibleRangeHint(this.mFirstPosition, this.mFirstPosition + getChildCount() - 1);
    return view2;
  }
  
  private View fillUp(int paramInt1, int paramInt2) {
    View view1 = null;
    int i = 0;
    View view2 = view1;
    int j = paramInt1, k = paramInt2;
    if ((this.mGroupFlags & 0x22) == 34) {
      i = this.mListPadding.top;
      k = paramInt2;
      j = paramInt1;
      view2 = view1;
    } 
    while (true) {
      boolean bool = true;
      if (k > i && j >= 0) {
        if (j != this.mSelectedPosition)
          bool = false; 
        view1 = makeAndAddView(j, k, false, this.mListPadding.left, bool);
        k = view1.getTop() - this.mDividerHeight;
        if (bool)
          view2 = view1; 
        j--;
        continue;
      } 
      break;
    } 
    this.mFirstPosition = j + 1;
    setVisibleRangeHint(this.mFirstPosition, this.mFirstPosition + getChildCount() - 1);
    return view2;
  }
  
  private View fillFromTop(int paramInt) {
    this.mFirstPosition = Math.min(this.mFirstPosition, this.mSelectedPosition);
    this.mFirstPosition = Math.min(this.mFirstPosition, this.mItemCount - 1);
    if (this.mFirstPosition < 0)
      this.mFirstPosition = 0; 
    return fillDown(this.mFirstPosition, paramInt);
  }
  
  private View fillFromMiddle(int paramInt1, int paramInt2) {
    int i = paramInt2 - paramInt1;
    paramInt2 = reconcileSelectedPosition();
    View view = makeAndAddView(paramInt2, paramInt1, true, this.mListPadding.left, true);
    this.mFirstPosition = paramInt2;
    paramInt1 = view.getMeasuredHeight();
    if (paramInt1 <= i)
      view.offsetTopAndBottom((i - paramInt1) / 2); 
    fillAboveAndBelow(view, paramInt2);
    if (!this.mStackFromBottom) {
      correctTooHigh(getChildCount());
    } else {
      correctTooLow(getChildCount());
    } 
    return view;
  }
  
  private void fillAboveAndBelow(View paramView, int paramInt) {
    int i = this.mDividerHeight;
    if (!this.mStackFromBottom) {
      fillUp(paramInt - 1, paramView.getTop() - i);
      adjustViewsUpOrDown();
      fillDown(paramInt + 1, paramView.getBottom() + i);
    } else {
      fillDown(paramInt + 1, paramView.getBottom() + i);
      adjustViewsUpOrDown();
      fillUp(paramInt - 1, paramView.getTop() - i);
    } 
  }
  
  private View fillFromSelection(int paramInt1, int paramInt2, int paramInt3) {
    int i = getVerticalFadingEdgeLength();
    int j = this.mSelectedPosition;
    paramInt2 = getTopSelectionPixel(paramInt2, i, j);
    paramInt3 = getBottomSelectionPixel(paramInt3, i, j);
    View view = makeAndAddView(j, paramInt1, true, this.mListPadding.left, true);
    if (view.getBottom() > paramInt3) {
      paramInt1 = view.getTop();
      i = view.getBottom();
      paramInt1 = Math.min(paramInt1 - paramInt2, i - paramInt3);
      view.offsetTopAndBottom(-paramInt1);
    } else if (view.getTop() < paramInt2) {
      paramInt1 = view.getTop();
      i = view.getBottom();
      paramInt1 = Math.min(paramInt2 - paramInt1, paramInt3 - i);
      view.offsetTopAndBottom(paramInt1);
    } 
    fillAboveAndBelow(view, j);
    if (!this.mStackFromBottom) {
      correctTooHigh(getChildCount());
    } else {
      correctTooLow(getChildCount());
    } 
    return view;
  }
  
  private int getBottomSelectionPixel(int paramInt1, int paramInt2, int paramInt3) {
    int i = paramInt1;
    if (paramInt3 != this.mItemCount - 1)
      i = paramInt1 - paramInt2; 
    return i;
  }
  
  private int getTopSelectionPixel(int paramInt1, int paramInt2, int paramInt3) {
    int i = paramInt1;
    paramInt1 = i;
    if (paramInt3 > 0)
      paramInt1 = i + paramInt2; 
    return paramInt1;
  }
  
  @RemotableViewMethod
  public void smoothScrollToPosition(int paramInt) {
    super.smoothScrollToPosition(paramInt);
  }
  
  @RemotableViewMethod
  public void smoothScrollByOffset(int paramInt) {
    super.smoothScrollByOffset(paramInt);
  }
  
  private View moveSelection(View paramView1, View paramView2, int paramInt1, int paramInt2, int paramInt3) {
    int i = getVerticalFadingEdgeLength();
    int j = this.mSelectedPosition;
    int k = getTopSelectionPixel(paramInt2, i, j);
    i = getBottomSelectionPixel(paramInt2, i, j);
    if (paramInt1 > 0) {
      paramView2 = makeAndAddView(j - 1, paramView1.getTop(), true, this.mListPadding.left, false);
      paramInt1 = this.mDividerHeight;
      paramView1 = makeAndAddView(j, paramView2.getBottom() + paramInt1, true, this.mListPadding.left, true);
      if (paramView1.getBottom() > i) {
        int m = paramView1.getTop();
        j = paramView1.getBottom();
        paramInt2 = (paramInt3 - paramInt2) / 2;
        paramInt3 = Math.min(m - k, j - i);
        paramInt2 = Math.min(paramInt3, paramInt2);
        paramView2.offsetTopAndBottom(-paramInt2);
        paramView1.offsetTopAndBottom(-paramInt2);
      } 
      if (!this.mStackFromBottom) {
        fillUp(this.mSelectedPosition - 2, paramView1.getTop() - paramInt1);
        adjustViewsUpOrDown();
        fillDown(this.mSelectedPosition + 1, paramView1.getBottom() + paramInt1);
      } else {
        fillDown(this.mSelectedPosition + 1, paramView1.getBottom() + paramInt1);
        adjustViewsUpOrDown();
        fillUp(this.mSelectedPosition - 2, paramView1.getTop() - paramInt1);
      } 
    } else if (paramInt1 < 0) {
      if (paramView2 != null) {
        paramView1 = makeAndAddView(j, paramView2.getTop(), true, this.mListPadding.left, true);
      } else {
        paramView1 = makeAndAddView(j, paramView1.getTop(), false, this.mListPadding.left, true);
      } 
      if (paramView1.getTop() < k) {
        paramInt1 = paramView1.getTop();
        int m = paramView1.getBottom();
        paramInt2 = (paramInt3 - paramInt2) / 2;
        paramInt1 = Math.min(k - paramInt1, i - m);
        paramInt1 = Math.min(paramInt1, paramInt2);
        paramView1.offsetTopAndBottom(paramInt1);
      } 
      fillAboveAndBelow(paramView1, j);
    } else {
      paramInt1 = paramView1.getTop();
      paramView1 = makeAndAddView(j, paramInt1, true, this.mListPadding.left, true);
      if (paramInt1 < paramInt2) {
        paramInt1 = paramView1.getBottom();
        if (paramInt1 < paramInt2 + 20)
          paramView1.offsetTopAndBottom(paramInt2 - paramView1.getTop()); 
      } 
      fillAboveAndBelow(paramView1, j);
    } 
    return paramView1;
  }
  
  class FocusSelector implements Runnable {
    private static final int STATE_REQUEST_FOCUS = 3;
    
    private static final int STATE_SET_SELECTION = 1;
    
    private static final int STATE_WAIT_FOR_LAYOUT = 2;
    
    private int mAction;
    
    private int mPosition;
    
    private int mPositionTop;
    
    final ListView this$0;
    
    private FocusSelector() {}
    
    FocusSelector setupForSetSelection(int param1Int1, int param1Int2) {
      this.mPosition = param1Int1;
      this.mPositionTop = param1Int2;
      this.mAction = 1;
      return this;
    }
    
    public void run() {
      int i = this.mAction;
      if (i == 1) {
        ListView.this.setSelectionFromTop(this.mPosition, this.mPositionTop);
        this.mAction = 2;
      } else if (i == 3) {
        i = this.mPosition;
        int j = ListView.this.mFirstPosition;
        View view = ListView.this.getChildAt(i - j);
        if (view != null)
          view.requestFocus(); 
        this.mAction = -1;
      } 
    }
    
    Runnable setupFocusIfValid(int param1Int) {
      if (this.mAction != 2 || param1Int != this.mPosition)
        return null; 
      this.mAction = 3;
      return this;
    }
    
    void onLayoutComplete() {
      if (this.mAction == 2)
        this.mAction = -1; 
    }
  }
  
  protected void onDetachedFromWindow() {
    FocusSelector focusSelector = this.mFocusSelector;
    if (focusSelector != null) {
      removeCallbacks(focusSelector);
      this.mFocusSelector = null;
    } 
    super.onDetachedFromWindow();
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (getChildCount() > 0) {
      View view = getFocusedChild();
      if (view != null) {
        int i = this.mFirstPosition, j = indexOfChild(view);
        int k = view.getBottom();
        k = Math.max(0, k - paramInt2 - this.mPaddingTop);
        int m = view.getTop();
        if (this.mFocusSelector == null)
          this.mFocusSelector = new FocusSelector(); 
        post(this.mFocusSelector.setupForSetSelection(i + j, m - k));
      } 
    } 
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: iload_2
    //   3: invokespecial onMeasure : (II)V
    //   6: iload_1
    //   7: invokestatic getMode : (I)I
    //   10: istore_3
    //   11: iload_2
    //   12: invokestatic getMode : (I)I
    //   15: istore #4
    //   17: iload_1
    //   18: invokestatic getSize : (I)I
    //   21: istore #5
    //   23: iload_2
    //   24: invokestatic getSize : (I)I
    //   27: istore #6
    //   29: iconst_0
    //   30: istore #7
    //   32: iconst_0
    //   33: istore #8
    //   35: iconst_0
    //   36: istore #9
    //   38: aload_0
    //   39: getfield mAdapter : Landroid/widget/ListAdapter;
    //   42: ifnonnull -> 50
    //   45: iconst_0
    //   46: istore_2
    //   47: goto -> 60
    //   50: aload_0
    //   51: getfield mAdapter : Landroid/widget/ListAdapter;
    //   54: invokeinterface getCount : ()I
    //   59: istore_2
    //   60: aload_0
    //   61: iload_2
    //   62: putfield mItemCount : I
    //   65: iload #7
    //   67: istore #10
    //   69: iload #8
    //   71: istore_2
    //   72: iload #9
    //   74: istore #11
    //   76: aload_0
    //   77: getfield mItemCount : I
    //   80: ifle -> 228
    //   83: iload_3
    //   84: ifeq -> 103
    //   87: iload #7
    //   89: istore #10
    //   91: iload #8
    //   93: istore_2
    //   94: iload #9
    //   96: istore #11
    //   98: iload #4
    //   100: ifne -> 228
    //   103: aload_0
    //   104: iconst_0
    //   105: aload_0
    //   106: getfield mIsScrap : [Z
    //   109: invokevirtual obtainView : (I[Z)Landroid/view/View;
    //   112: astore #12
    //   114: aload_0
    //   115: aload #12
    //   117: iconst_0
    //   118: iload_1
    //   119: iload #6
    //   121: invokespecial measureScrapChild : (Landroid/view/View;III)V
    //   124: aload #12
    //   126: invokevirtual getMeasuredWidth : ()I
    //   129: istore #9
    //   131: aload #12
    //   133: invokevirtual getMeasuredHeight : ()I
    //   136: istore #7
    //   138: iconst_0
    //   139: aload #12
    //   141: invokevirtual getMeasuredState : ()I
    //   144: invokestatic combineMeasuredStates : (II)I
    //   147: istore #8
    //   149: iload #9
    //   151: istore #10
    //   153: iload #7
    //   155: istore_2
    //   156: iload #8
    //   158: istore #11
    //   160: aload_0
    //   161: invokevirtual recycleOnMeasure : ()Z
    //   164: ifeq -> 228
    //   167: aload_0
    //   168: getfield mRecycler : Landroid/widget/AbsListView$RecycleBin;
    //   171: astore #13
    //   173: aload #12
    //   175: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   178: checkcast android/widget/AbsListView$LayoutParams
    //   181: getfield viewType : I
    //   184: istore #14
    //   186: iload #9
    //   188: istore #10
    //   190: iload #7
    //   192: istore_2
    //   193: iload #8
    //   195: istore #11
    //   197: aload #13
    //   199: iload #14
    //   201: invokevirtual shouldRecycleViewType : (I)Z
    //   204: ifeq -> 228
    //   207: aload_0
    //   208: getfield mRecycler : Landroid/widget/AbsListView$RecycleBin;
    //   211: aload #12
    //   213: iconst_0
    //   214: invokevirtual addScrapView : (Landroid/view/View;I)V
    //   217: iload #8
    //   219: istore #11
    //   221: iload #7
    //   223: istore_2
    //   224: iload #9
    //   226: istore #10
    //   228: iload_3
    //   229: ifne -> 268
    //   232: aload_0
    //   233: getfield mListPadding : Landroid/graphics/Rect;
    //   236: getfield left : I
    //   239: istore #11
    //   241: aload_0
    //   242: getfield mListPadding : Landroid/graphics/Rect;
    //   245: getfield right : I
    //   248: istore #9
    //   250: iload #11
    //   252: iload #9
    //   254: iadd
    //   255: iload #10
    //   257: iadd
    //   258: aload_0
    //   259: invokevirtual getVerticalScrollbarWidth : ()I
    //   262: iadd
    //   263: istore #10
    //   265: goto -> 279
    //   268: ldc_w -16777216
    //   271: iload #11
    //   273: iand
    //   274: iload #5
    //   276: ior
    //   277: istore #10
    //   279: iload #4
    //   281: ifne -> 320
    //   284: aload_0
    //   285: getfield mListPadding : Landroid/graphics/Rect;
    //   288: getfield top : I
    //   291: istore #11
    //   293: aload_0
    //   294: getfield mListPadding : Landroid/graphics/Rect;
    //   297: getfield bottom : I
    //   300: istore #6
    //   302: iload #11
    //   304: iload #6
    //   306: iadd
    //   307: iload_2
    //   308: iadd
    //   309: aload_0
    //   310: invokevirtual getVerticalFadingEdgeLength : ()I
    //   313: iconst_2
    //   314: imul
    //   315: iadd
    //   316: istore_2
    //   317: goto -> 323
    //   320: iload #6
    //   322: istore_2
    //   323: iload_2
    //   324: istore #11
    //   326: iload #4
    //   328: ldc_w -2147483648
    //   331: if_icmpne -> 345
    //   334: aload_0
    //   335: iload_1
    //   336: iconst_0
    //   337: iconst_m1
    //   338: iload_2
    //   339: iconst_m1
    //   340: invokevirtual measureHeightOfChildren : (IIIII)I
    //   343: istore #11
    //   345: aload_0
    //   346: iload #10
    //   348: iload #11
    //   350: invokevirtual setMeasuredDimension : (II)V
    //   353: aload_0
    //   354: iload_1
    //   355: putfield mWidthMeasureSpec : I
    //   358: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1285	-> 0
    //   #1287	-> 6
    //   #1288	-> 11
    //   #1289	-> 17
    //   #1290	-> 23
    //   #1292	-> 29
    //   #1293	-> 32
    //   #1294	-> 35
    //   #1296	-> 38
    //   #1297	-> 65
    //   #1299	-> 103
    //   #1303	-> 114
    //   #1305	-> 124
    //   #1306	-> 131
    //   #1307	-> 138
    //   #1309	-> 149
    //   #1310	-> 173
    //   #1309	-> 186
    //   #1311	-> 207
    //   #1315	-> 228
    //   #1316	-> 232
    //   #1317	-> 250
    //   #1319	-> 268
    //   #1322	-> 279
    //   #1323	-> 284
    //   #1324	-> 302
    //   #1322	-> 320
    //   #1327	-> 323
    //   #1329	-> 334
    //   #1332	-> 345
    //   #1334	-> 353
    //   #1335	-> 358
  }
  
  private void measureScrapChild(View paramView, int paramInt1, int paramInt2, int paramInt3) {
    AbsListView.LayoutParams layoutParams1 = (AbsListView.LayoutParams)paramView.getLayoutParams();
    AbsListView.LayoutParams layoutParams2 = layoutParams1;
    if (layoutParams1 == null) {
      layoutParams2 = (AbsListView.LayoutParams)generateDefaultLayoutParams();
      paramView.setLayoutParams(layoutParams2);
    } 
    layoutParams2.viewType = this.mAdapter.getItemViewType(paramInt1);
    layoutParams2.isEnabled = this.mAdapter.isEnabled(paramInt1);
    layoutParams2.forceAdd = true;
    paramInt2 = ViewGroup.getChildMeasureSpec(paramInt2, this.mListPadding.left + this.mListPadding.right, layoutParams2.width);
    paramInt1 = layoutParams2.height;
    if (paramInt1 > 0) {
      paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    } else {
      paramInt1 = View.MeasureSpec.makeSafeMeasureSpec(paramInt3, 0);
    } 
    paramView.measure(paramInt2, paramInt1);
    paramView.forceLayout();
  }
  
  @ExportedProperty(category = "list")
  protected boolean recycleOnMeasure() {
    return true;
  }
  
  final int measureHeightOfChildren(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    int k;
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter == null)
      return this.mListPadding.top + this.mListPadding.bottom; 
    int i = this.mListPadding.top + this.mListPadding.bottom;
    int j = this.mDividerHeight;
    boolean bool = false;
    if (paramInt3 == -1) {
      k = listAdapter.getCount() - 1;
    } else {
      k = paramInt3;
    } 
    AbsListView.RecycleBin recycleBin = this.mRecycler;
    boolean bool1 = recycleOnMeasure();
    boolean[] arrayOfBoolean = this.mIsScrap;
    for (int m = paramInt2; m <= k; m++, paramInt2 = i) {
      View view = obtainView(m, arrayOfBoolean);
      measureScrapChild(view, m, paramInt1, paramInt4);
      i = paramInt3;
      if (m > 0)
        i = paramInt3 + j; 
      if (bool1) {
        paramInt3 = ((AbsListView.LayoutParams)view.getLayoutParams()).viewType;
        if (recycleBin.shouldRecycleViewType(paramInt3))
          recycleBin.addScrapView(view, -1); 
      } 
      paramInt3 = i + view.getMeasuredHeight();
      if (paramInt3 >= paramInt4) {
        if (paramInt5 >= 0 && m > paramInt5 && paramInt2 > 0 && paramInt3 != paramInt4)
          paramInt4 = paramInt2; 
        return paramInt4;
      } 
      i = paramInt2;
      if (paramInt5 >= 0) {
        i = paramInt2;
        if (m >= paramInt5)
          i = paramInt3; 
      } 
    } 
    return paramInt3;
  }
  
  int findMotionRow(int paramInt) {
    int i = getChildCount();
    if (i > 0)
      if (!this.mStackFromBottom) {
        for (byte b = 0; b < i; b++) {
          View view = getChildAt(b);
          if (paramInt <= view.getBottom())
            return this.mFirstPosition + b; 
        } 
      } else {
        for (int j = i - 1; j >= 0; j--) {
          View view = getChildAt(j);
          if (paramInt >= view.getTop())
            return this.mFirstPosition + j; 
        } 
      }  
    return -1;
  }
  
  private View fillSpecific(int paramInt1, int paramInt2) {
    boolean bool;
    View view2, view3;
    if (paramInt1 == this.mSelectedPosition) {
      bool = true;
    } else {
      bool = false;
    } 
    View view1 = makeAndAddView(paramInt1, paramInt2, true, this.mListPadding.left, bool);
    this.mFirstPosition = paramInt1;
    paramInt2 = this.mDividerHeight;
    if (!this.mStackFromBottom) {
      view2 = fillUp(paramInt1 - 1, view1.getTop() - paramInt2);
      adjustViewsUpOrDown();
      view3 = fillDown(paramInt1 + 1, view1.getBottom() + paramInt2);
      paramInt1 = getChildCount();
      if (paramInt1 > 0)
        correctTooHigh(paramInt1); 
    } else {
      View view4 = fillDown(paramInt1 + 1, view1.getBottom() + paramInt2);
      adjustViewsUpOrDown();
      View view5 = fillUp(paramInt1 - 1, view1.getTop() - paramInt2);
      paramInt1 = getChildCount();
      view2 = view5;
      view3 = view4;
      if (paramInt1 > 0) {
        correctTooLow(paramInt1);
        view3 = view4;
        view2 = view5;
      } 
    } 
    if (bool)
      return view1; 
    if (view2 != null)
      return view2; 
    return view3;
  }
  
  private void correctTooHigh(int paramInt) {
    int i = this.mFirstPosition;
    if (i + paramInt - 1 == this.mItemCount - 1 && paramInt > 0) {
      View view = getChildAt(paramInt - 1);
      paramInt = view.getBottom();
      int j = this.mBottom, k = this.mTop;
      i = this.mListPadding.bottom;
      i = j - k - i - paramInt;
      view = getChildAt(0);
      k = view.getTop();
      if (i > 0 && (this.mFirstPosition > 0 || k < this.mListPadding.top)) {
        paramInt = i;
        if (this.mFirstPosition == 0)
          paramInt = Math.min(i, this.mListPadding.top - k); 
        offsetChildrenTopAndBottom(paramInt);
        if (this.mFirstPosition > 0) {
          fillUp(this.mFirstPosition - 1, view.getTop() - this.mDividerHeight);
          adjustViewsUpOrDown();
        } 
      } 
    } 
  }
  
  private void correctTooLow(int paramInt) {
    if (this.mFirstPosition == 0 && paramInt > 0) {
      View view = getChildAt(0);
      int i = view.getTop();
      int j = this.mListPadding.top;
      int k = this.mBottom - this.mTop - this.mListPadding.bottom;
      i -= j;
      view = getChildAt(paramInt - 1);
      int m = view.getBottom();
      j = this.mFirstPosition + paramInt - 1;
      if (i > 0) {
        if (j < this.mItemCount - 1 || m > k) {
          paramInt = i;
          if (j == this.mItemCount - 1)
            paramInt = Math.min(i, m - k); 
          offsetChildrenTopAndBottom(-paramInt);
          if (j < this.mItemCount - 1) {
            fillDown(j + 1, view.getBottom() + this.mDividerHeight);
            adjustViewsUpOrDown();
          } 
          return;
        } 
        if (j == this.mItemCount - 1)
          adjustViewsUpOrDown(); 
      } 
    } 
  }
  
  protected void layoutChildren() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mBlockLayoutRequests : Z
    //   4: istore_1
    //   5: iload_1
    //   6: ifeq -> 10
    //   9: return
    //   10: aload_0
    //   11: iconst_1
    //   12: putfield mBlockLayoutRequests : Z
    //   15: aload_0
    //   16: invokespecial layoutChildren : ()V
    //   19: aload_0
    //   20: invokevirtual invalidate : ()V
    //   23: aload_0
    //   24: getfield mAdapter : Landroid/widget/ListAdapter;
    //   27: ifnonnull -> 61
    //   30: aload_0
    //   31: invokevirtual resetList : ()V
    //   34: aload_0
    //   35: invokevirtual invokeOnItemScrollListener : ()V
    //   38: aload_0
    //   39: getfield mFocusSelector : Landroid/widget/ListView$FocusSelector;
    //   42: astore_2
    //   43: aload_2
    //   44: ifnull -> 51
    //   47: aload_2
    //   48: invokevirtual onLayoutComplete : ()V
    //   51: iload_1
    //   52: ifne -> 60
    //   55: aload_0
    //   56: iconst_0
    //   57: putfield mBlockLayoutRequests : Z
    //   60: return
    //   61: aload_0
    //   62: getfield mListPadding : Landroid/graphics/Rect;
    //   65: getfield top : I
    //   68: istore_3
    //   69: aload_0
    //   70: getfield mBottom : I
    //   73: aload_0
    //   74: getfield mTop : I
    //   77: isub
    //   78: aload_0
    //   79: getfield mListPadding : Landroid/graphics/Rect;
    //   82: getfield bottom : I
    //   85: isub
    //   86: istore #4
    //   88: aload_0
    //   89: invokevirtual getChildCount : ()I
    //   92: istore #5
    //   94: iconst_0
    //   95: istore #6
    //   97: aconst_null
    //   98: astore #7
    //   100: aload_0
    //   101: getfield mLayoutMode : I
    //   104: istore #8
    //   106: iload #8
    //   108: iconst_1
    //   109: if_icmpeq -> 267
    //   112: iload #8
    //   114: iconst_2
    //   115: if_icmpeq -> 211
    //   118: iload #8
    //   120: iconst_3
    //   121: if_icmpeq -> 267
    //   124: iload #8
    //   126: iconst_4
    //   127: if_icmpeq -> 267
    //   130: iload #8
    //   132: iconst_5
    //   133: if_icmpeq -> 267
    //   136: aload_0
    //   137: getfield mSelectedPosition : I
    //   140: aload_0
    //   141: getfield mFirstPosition : I
    //   144: isub
    //   145: istore #8
    //   147: aload #7
    //   149: astore_2
    //   150: iload #8
    //   152: iflt -> 172
    //   155: aload #7
    //   157: astore_2
    //   158: iload #8
    //   160: iload #5
    //   162: if_icmpge -> 172
    //   165: aload_0
    //   166: iload #8
    //   168: invokevirtual getChildAt : (I)Landroid/view/View;
    //   171: astore_2
    //   172: aload_0
    //   173: iconst_0
    //   174: invokevirtual getChildAt : (I)Landroid/view/View;
    //   177: astore #7
    //   179: aload_0
    //   180: getfield mNextSelectedPosition : I
    //   183: iflt -> 197
    //   186: aload_0
    //   187: getfield mNextSelectedPosition : I
    //   190: aload_0
    //   191: getfield mSelectedPosition : I
    //   194: isub
    //   195: istore #6
    //   197: aload_0
    //   198: iload #8
    //   200: iload #6
    //   202: iadd
    //   203: invokevirtual getChildAt : (I)Landroid/view/View;
    //   206: astore #9
    //   208: goto -> 278
    //   211: aload_0
    //   212: getfield mNextSelectedPosition : I
    //   215: aload_0
    //   216: getfield mFirstPosition : I
    //   219: isub
    //   220: istore #6
    //   222: iload #6
    //   224: iflt -> 253
    //   227: iload #6
    //   229: iload #5
    //   231: if_icmpge -> 253
    //   234: aload_0
    //   235: iload #6
    //   237: invokevirtual getChildAt : (I)Landroid/view/View;
    //   240: astore #9
    //   242: iconst_0
    //   243: istore #6
    //   245: aconst_null
    //   246: astore_2
    //   247: aconst_null
    //   248: astore #7
    //   250: goto -> 278
    //   253: iconst_0
    //   254: istore #6
    //   256: aconst_null
    //   257: astore_2
    //   258: aconst_null
    //   259: astore #7
    //   261: aconst_null
    //   262: astore #9
    //   264: goto -> 278
    //   267: iconst_0
    //   268: istore #6
    //   270: aconst_null
    //   271: astore_2
    //   272: aconst_null
    //   273: astore #7
    //   275: aconst_null
    //   276: astore #9
    //   278: aload_0
    //   279: getfield mDataChanged : Z
    //   282: istore #10
    //   284: iload #10
    //   286: ifeq -> 293
    //   289: aload_0
    //   290: invokevirtual handleDataChanged : ()V
    //   293: aload_0
    //   294: getfield mItemCount : I
    //   297: ifne -> 331
    //   300: aload_0
    //   301: invokevirtual resetList : ()V
    //   304: aload_0
    //   305: invokevirtual invokeOnItemScrollListener : ()V
    //   308: aload_0
    //   309: getfield mFocusSelector : Landroid/widget/ListView$FocusSelector;
    //   312: astore_2
    //   313: aload_2
    //   314: ifnull -> 321
    //   317: aload_2
    //   318: invokevirtual onLayoutComplete : ()V
    //   321: iload_1
    //   322: ifne -> 330
    //   325: aload_0
    //   326: iconst_0
    //   327: putfield mBlockLayoutRequests : Z
    //   330: return
    //   331: aload_0
    //   332: getfield mItemCount : I
    //   335: aload_0
    //   336: getfield mAdapter : Landroid/widget/ListAdapter;
    //   339: invokeinterface getCount : ()I
    //   344: if_icmpne -> 1549
    //   347: aload_0
    //   348: aload_0
    //   349: getfield mNextSelectedPosition : I
    //   352: invokevirtual setSelectedPositionInt : (I)V
    //   355: aconst_null
    //   356: astore #11
    //   358: aconst_null
    //   359: astore #12
    //   361: aload_0
    //   362: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   365: astore #13
    //   367: aload #13
    //   369: ifnull -> 472
    //   372: aload #13
    //   374: invokevirtual getAccessibilityFocusedHost : ()Landroid/view/View;
    //   377: astore #14
    //   379: aload #14
    //   381: ifnull -> 472
    //   384: aload_0
    //   385: aload #14
    //   387: invokevirtual getAccessibilityFocusedChild : (Landroid/view/View;)Landroid/view/View;
    //   390: astore #15
    //   392: aload #15
    //   394: ifnull -> 472
    //   397: iload #10
    //   399: ifeq -> 442
    //   402: aload_0
    //   403: aload #15
    //   405: invokespecial isDirectChildHeaderOrFooter : (Landroid/view/View;)Z
    //   408: ifne -> 442
    //   411: aload #11
    //   413: astore #16
    //   415: aload #12
    //   417: astore #17
    //   419: aload #15
    //   421: invokevirtual hasTransientState : ()Z
    //   424: ifeq -> 453
    //   427: aload #11
    //   429: astore #16
    //   431: aload #12
    //   433: astore #17
    //   435: aload_0
    //   436: getfield mAdapterHasStableIds : Z
    //   439: ifeq -> 453
    //   442: aload #14
    //   444: astore #17
    //   446: aload #13
    //   448: invokevirtual getAccessibilityFocusedVirtualView : ()Landroid/view/accessibility/AccessibilityNodeInfo;
    //   451: astore #16
    //   453: aload_0
    //   454: aload #15
    //   456: invokevirtual getPositionForView : (Landroid/view/View;)I
    //   459: istore #8
    //   461: aload #16
    //   463: astore #14
    //   465: aload #17
    //   467: astore #12
    //   469: goto -> 481
    //   472: aconst_null
    //   473: astore #14
    //   475: aconst_null
    //   476: astore #12
    //   478: iconst_m1
    //   479: istore #8
    //   481: aconst_null
    //   482: astore #17
    //   484: aconst_null
    //   485: astore #16
    //   487: aload_0
    //   488: invokevirtual getFocusedChild : ()Landroid/view/View;
    //   491: astore #11
    //   493: aload #11
    //   495: ifnull -> 579
    //   498: iload #10
    //   500: ifeq -> 527
    //   503: aload_0
    //   504: aload #11
    //   506: invokespecial isDirectChildHeaderOrFooter : (Landroid/view/View;)Z
    //   509: ifne -> 527
    //   512: aload #11
    //   514: invokevirtual hasTransientState : ()Z
    //   517: ifne -> 527
    //   520: aload_0
    //   521: getfield mAdapterHasStableIds : Z
    //   524: ifeq -> 559
    //   527: aload_0
    //   528: invokevirtual findFocus : ()Landroid/view/View;
    //   531: astore #15
    //   533: aload #11
    //   535: astore #17
    //   537: aload #15
    //   539: astore #16
    //   541: aload #15
    //   543: ifnull -> 559
    //   546: aload #15
    //   548: invokevirtual dispatchStartTemporaryDetach : ()V
    //   551: aload #15
    //   553: astore #16
    //   555: aload #11
    //   557: astore #17
    //   559: aload_0
    //   560: invokevirtual requestFocus : ()Z
    //   563: pop
    //   564: aload #17
    //   566: astore #11
    //   568: aload #16
    //   570: astore #17
    //   572: aload #11
    //   574: astore #16
    //   576: goto -> 585
    //   579: aconst_null
    //   580: astore #16
    //   582: aconst_null
    //   583: astore #17
    //   585: aload_0
    //   586: getfield mFirstPosition : I
    //   589: istore #18
    //   591: aload_0
    //   592: getfield mRecycler : Landroid/widget/AbsListView$RecycleBin;
    //   595: astore #11
    //   597: iload #10
    //   599: ifeq -> 637
    //   602: iconst_0
    //   603: istore #19
    //   605: iload #19
    //   607: iload #5
    //   609: if_icmpge -> 634
    //   612: aload #11
    //   614: aload_0
    //   615: iload #19
    //   617: invokevirtual getChildAt : (I)Landroid/view/View;
    //   620: iload #18
    //   622: iload #19
    //   624: iadd
    //   625: invokevirtual addScrapView : (Landroid/view/View;I)V
    //   628: iinc #19, 1
    //   631: goto -> 605
    //   634: goto -> 646
    //   637: aload #11
    //   639: iload #5
    //   641: iload #18
    //   643: invokevirtual fillActiveViews : (II)V
    //   646: aload_0
    //   647: invokevirtual detachAllViewsFromParent : ()V
    //   650: aload #11
    //   652: invokevirtual removeSkippedScrap : ()V
    //   655: aload_0
    //   656: getfield mLayoutMode : I
    //   659: tableswitch default -> 696, 1 -> 852, 2 -> 820, 3 -> 800, 4 -> 742, 5 -> 726, 6 -> 710
    //   696: iload #5
    //   698: ifne -> 933
    //   701: aload_0
    //   702: getfield mStackFromBottom : Z
    //   705: istore #10
    //   707: goto -> 870
    //   710: aload_0
    //   711: aload_2
    //   712: aload #9
    //   714: iload #6
    //   716: iload_3
    //   717: iload #4
    //   719: invokespecial moveSelection : (Landroid/view/View;Landroid/view/View;III)Landroid/view/View;
    //   722: astore_2
    //   723: goto -> 1039
    //   726: aload_0
    //   727: aload_0
    //   728: getfield mSyncPosition : I
    //   731: aload_0
    //   732: getfield mSpecificTop : I
    //   735: invokespecial fillSpecific : (II)Landroid/view/View;
    //   738: astore_2
    //   739: goto -> 1039
    //   742: aload_0
    //   743: invokevirtual reconcileSelectedPosition : ()I
    //   746: istore #6
    //   748: aload_0
    //   749: iload #6
    //   751: aload_0
    //   752: getfield mSpecificTop : I
    //   755: invokespecial fillSpecific : (II)Landroid/view/View;
    //   758: astore_2
    //   759: aload_2
    //   760: ifnonnull -> 797
    //   763: aload_0
    //   764: getfield mFocusSelector : Landroid/widget/ListView$FocusSelector;
    //   767: ifnull -> 797
    //   770: aload_0
    //   771: getfield mFocusSelector : Landroid/widget/ListView$FocusSelector;
    //   774: astore #7
    //   776: aload #7
    //   778: iload #6
    //   780: invokevirtual setupFocusIfValid : (I)Ljava/lang/Runnable;
    //   783: astore #7
    //   785: aload #7
    //   787: ifnull -> 797
    //   790: aload_0
    //   791: aload #7
    //   793: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   796: pop
    //   797: goto -> 1039
    //   800: aload_0
    //   801: aload_0
    //   802: getfield mItemCount : I
    //   805: iconst_1
    //   806: isub
    //   807: iload #4
    //   809: invokespecial fillUp : (II)Landroid/view/View;
    //   812: astore_2
    //   813: aload_0
    //   814: invokespecial adjustViewsUpOrDown : ()V
    //   817: goto -> 1039
    //   820: aload #9
    //   822: ifnull -> 841
    //   825: aload_0
    //   826: aload #9
    //   828: invokevirtual getTop : ()I
    //   831: iload_3
    //   832: iload #4
    //   834: invokespecial fillFromSelection : (III)Landroid/view/View;
    //   837: astore_2
    //   838: goto -> 1039
    //   841: aload_0
    //   842: iload_3
    //   843: iload #4
    //   845: invokespecial fillFromMiddle : (II)Landroid/view/View;
    //   848: astore_2
    //   849: goto -> 1039
    //   852: aload_0
    //   853: iconst_0
    //   854: putfield mFirstPosition : I
    //   857: aload_0
    //   858: iload_3
    //   859: invokespecial fillFromTop : (I)Landroid/view/View;
    //   862: astore_2
    //   863: aload_0
    //   864: invokespecial adjustViewsUpOrDown : ()V
    //   867: goto -> 1039
    //   870: iload #10
    //   872: ifne -> 898
    //   875: aload_0
    //   876: iconst_0
    //   877: iconst_1
    //   878: invokevirtual lookForSelectablePosition : (IZ)I
    //   881: istore #6
    //   883: aload_0
    //   884: iload #6
    //   886: invokevirtual setSelectedPositionInt : (I)V
    //   889: aload_0
    //   890: iload_3
    //   891: invokespecial fillFromTop : (I)Landroid/view/View;
    //   894: astore_2
    //   895: goto -> 1039
    //   898: aload_0
    //   899: aload_0
    //   900: getfield mItemCount : I
    //   903: iconst_1
    //   904: isub
    //   905: iconst_0
    //   906: invokevirtual lookForSelectablePosition : (IZ)I
    //   909: istore #6
    //   911: aload_0
    //   912: iload #6
    //   914: invokevirtual setSelectedPositionInt : (I)V
    //   917: aload_0
    //   918: aload_0
    //   919: getfield mItemCount : I
    //   922: iconst_1
    //   923: isub
    //   924: iload #4
    //   926: invokespecial fillUp : (II)Landroid/view/View;
    //   929: astore_2
    //   930: goto -> 1039
    //   933: aload_0
    //   934: getfield mSelectedPosition : I
    //   937: iflt -> 985
    //   940: aload_0
    //   941: getfield mSelectedPosition : I
    //   944: aload_0
    //   945: getfield mItemCount : I
    //   948: if_icmpge -> 985
    //   951: aload_0
    //   952: getfield mSelectedPosition : I
    //   955: istore #19
    //   957: aload_2
    //   958: ifnonnull -> 967
    //   961: iload_3
    //   962: istore #6
    //   964: goto -> 973
    //   967: aload_2
    //   968: invokevirtual getTop : ()I
    //   971: istore #6
    //   973: aload_0
    //   974: iload #19
    //   976: iload #6
    //   978: invokespecial fillSpecific : (II)Landroid/view/View;
    //   981: astore_2
    //   982: goto -> 1039
    //   985: aload_0
    //   986: getfield mFirstPosition : I
    //   989: aload_0
    //   990: getfield mItemCount : I
    //   993: if_icmpge -> 1032
    //   996: aload_0
    //   997: getfield mFirstPosition : I
    //   1000: istore #19
    //   1002: aload #7
    //   1004: ifnonnull -> 1013
    //   1007: iload_3
    //   1008: istore #6
    //   1010: goto -> 1020
    //   1013: aload #7
    //   1015: invokevirtual getTop : ()I
    //   1018: istore #6
    //   1020: aload_0
    //   1021: iload #19
    //   1023: iload #6
    //   1025: invokespecial fillSpecific : (II)Landroid/view/View;
    //   1028: astore_2
    //   1029: goto -> 1039
    //   1032: aload_0
    //   1033: iconst_0
    //   1034: iload_3
    //   1035: invokespecial fillSpecific : (II)Landroid/view/View;
    //   1038: astore_2
    //   1039: aload #11
    //   1041: invokevirtual scrapActiveViews : ()V
    //   1044: aload_0
    //   1045: aload_0
    //   1046: getfield mHeaderViewInfos : Ljava/util/ArrayList;
    //   1049: invokespecial removeUnusedFixedViews : (Ljava/util/List;)V
    //   1052: aload_0
    //   1053: aload_0
    //   1054: getfield mFooterViewInfos : Ljava/util/ArrayList;
    //   1057: invokespecial removeUnusedFixedViews : (Ljava/util/List;)V
    //   1060: aload_2
    //   1061: ifnull -> 1182
    //   1064: aload_0
    //   1065: getfield mItemsCanFocus : Z
    //   1068: ifeq -> 1165
    //   1071: aload_0
    //   1072: invokevirtual hasFocus : ()Z
    //   1075: ifeq -> 1165
    //   1078: aload_2
    //   1079: invokevirtual hasFocus : ()Z
    //   1082: ifne -> 1165
    //   1085: aload_2
    //   1086: aload #16
    //   1088: if_acmpne -> 1104
    //   1091: aload #17
    //   1093: ifnull -> 1104
    //   1096: aload #17
    //   1098: invokevirtual requestFocus : ()Z
    //   1101: ifne -> 1111
    //   1104: aload_2
    //   1105: invokevirtual requestFocus : ()Z
    //   1108: ifeq -> 1117
    //   1111: iconst_1
    //   1112: istore #6
    //   1114: goto -> 1120
    //   1117: iconst_0
    //   1118: istore #6
    //   1120: iload #6
    //   1122: ifne -> 1150
    //   1125: aload_0
    //   1126: invokevirtual getFocusedChild : ()Landroid/view/View;
    //   1129: astore #7
    //   1131: aload #7
    //   1133: ifnull -> 1141
    //   1136: aload #7
    //   1138: invokevirtual clearFocus : ()V
    //   1141: aload_0
    //   1142: iconst_m1
    //   1143: aload_2
    //   1144: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   1147: goto -> 1162
    //   1150: aload_2
    //   1151: iconst_0
    //   1152: invokevirtual setSelected : (Z)V
    //   1155: aload_0
    //   1156: getfield mSelectorRect : Landroid/graphics/Rect;
    //   1159: invokevirtual setEmpty : ()V
    //   1162: goto -> 1171
    //   1165: aload_0
    //   1166: iconst_m1
    //   1167: aload_2
    //   1168: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   1171: aload_0
    //   1172: aload_2
    //   1173: invokevirtual getTop : ()I
    //   1176: putfield mSelectedTop : I
    //   1179: goto -> 1313
    //   1182: aload_0
    //   1183: getfield mTouchMode : I
    //   1186: iconst_1
    //   1187: if_icmpeq -> 1207
    //   1190: aload_0
    //   1191: getfield mTouchMode : I
    //   1194: iconst_2
    //   1195: if_icmpne -> 1201
    //   1198: goto -> 1207
    //   1201: iconst_0
    //   1202: istore #6
    //   1204: goto -> 1210
    //   1207: iconst_1
    //   1208: istore #6
    //   1210: iload #6
    //   1212: ifeq -> 1245
    //   1215: aload_0
    //   1216: aload_0
    //   1217: getfield mMotionPosition : I
    //   1220: aload_0
    //   1221: getfield mFirstPosition : I
    //   1224: isub
    //   1225: invokevirtual getChildAt : (I)Landroid/view/View;
    //   1228: astore_2
    //   1229: aload_2
    //   1230: ifnull -> 1242
    //   1233: aload_0
    //   1234: aload_0
    //   1235: getfield mMotionPosition : I
    //   1238: aload_2
    //   1239: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   1242: goto -> 1295
    //   1245: aload_0
    //   1246: getfield mSelectorPosition : I
    //   1249: iconst_m1
    //   1250: if_icmpeq -> 1283
    //   1253: aload_0
    //   1254: aload_0
    //   1255: getfield mSelectorPosition : I
    //   1258: aload_0
    //   1259: getfield mFirstPosition : I
    //   1262: isub
    //   1263: invokevirtual getChildAt : (I)Landroid/view/View;
    //   1266: astore_2
    //   1267: aload_2
    //   1268: ifnull -> 1280
    //   1271: aload_0
    //   1272: aload_0
    //   1273: getfield mSelectorPosition : I
    //   1276: aload_2
    //   1277: invokevirtual positionSelector : (ILandroid/view/View;)V
    //   1280: goto -> 1295
    //   1283: aload_0
    //   1284: iconst_0
    //   1285: putfield mSelectedTop : I
    //   1288: aload_0
    //   1289: getfield mSelectorRect : Landroid/graphics/Rect;
    //   1292: invokevirtual setEmpty : ()V
    //   1295: aload_0
    //   1296: invokevirtual hasFocus : ()Z
    //   1299: ifeq -> 1313
    //   1302: aload #17
    //   1304: ifnull -> 1313
    //   1307: aload #17
    //   1309: invokevirtual requestFocus : ()Z
    //   1312: pop
    //   1313: aload #13
    //   1315: ifnull -> 1445
    //   1318: aload #13
    //   1320: invokevirtual getAccessibilityFocusedHost : ()Landroid/view/View;
    //   1323: astore_2
    //   1324: aload_2
    //   1325: ifnonnull -> 1442
    //   1328: aload #12
    //   1330: ifnull -> 1392
    //   1333: aload #12
    //   1335: invokevirtual isAttachedToWindow : ()Z
    //   1338: ifeq -> 1392
    //   1341: aload #12
    //   1343: invokevirtual getAccessibilityNodeProvider : ()Landroid/view/accessibility/AccessibilityNodeProvider;
    //   1346: astore_2
    //   1347: aload #14
    //   1349: ifnull -> 1383
    //   1352: aload_2
    //   1353: ifnull -> 1383
    //   1356: aload #14
    //   1358: invokevirtual getSourceNodeId : ()J
    //   1361: lstore #20
    //   1363: lload #20
    //   1365: invokestatic getVirtualDescendantId : (J)I
    //   1368: istore #6
    //   1370: aload_2
    //   1371: iload #6
    //   1373: bipush #64
    //   1375: aconst_null
    //   1376: invokevirtual performAction : (IILandroid/os/Bundle;)Z
    //   1379: pop
    //   1380: goto -> 1389
    //   1383: aload #12
    //   1385: invokevirtual requestAccessibilityFocus : ()Z
    //   1388: pop
    //   1389: goto -> 1445
    //   1392: iload #8
    //   1394: iconst_m1
    //   1395: if_icmpeq -> 1445
    //   1398: aload_0
    //   1399: getfield mFirstPosition : I
    //   1402: istore_3
    //   1403: aload_0
    //   1404: invokevirtual getChildCount : ()I
    //   1407: istore #6
    //   1409: iload #8
    //   1411: iload_3
    //   1412: isub
    //   1413: iconst_0
    //   1414: iload #6
    //   1416: iconst_1
    //   1417: isub
    //   1418: invokestatic constrain : (III)I
    //   1421: istore #6
    //   1423: aload_0
    //   1424: iload #6
    //   1426: invokevirtual getChildAt : (I)Landroid/view/View;
    //   1429: astore_2
    //   1430: aload_2
    //   1431: ifnull -> 1445
    //   1434: aload_2
    //   1435: invokevirtual requestAccessibilityFocus : ()Z
    //   1438: pop
    //   1439: goto -> 1445
    //   1442: goto -> 1445
    //   1445: aload #17
    //   1447: ifnull -> 1463
    //   1450: aload #17
    //   1452: invokevirtual getWindowToken : ()Landroid/os/IBinder;
    //   1455: ifnull -> 1463
    //   1458: aload #17
    //   1460: invokevirtual dispatchFinishTemporaryDetach : ()V
    //   1463: aload_0
    //   1464: iconst_0
    //   1465: putfield mLayoutMode : I
    //   1468: aload_0
    //   1469: iconst_0
    //   1470: putfield mDataChanged : Z
    //   1473: aload_0
    //   1474: getfield mPositionScrollAfterLayout : Ljava/lang/Runnable;
    //   1477: ifnull -> 1494
    //   1480: aload_0
    //   1481: aload_0
    //   1482: getfield mPositionScrollAfterLayout : Ljava/lang/Runnable;
    //   1485: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   1488: pop
    //   1489: aload_0
    //   1490: aconst_null
    //   1491: putfield mPositionScrollAfterLayout : Ljava/lang/Runnable;
    //   1494: aload_0
    //   1495: iconst_0
    //   1496: putfield mNeedSync : Z
    //   1499: aload_0
    //   1500: aload_0
    //   1501: getfield mSelectedPosition : I
    //   1504: invokevirtual setNextSelectedPositionInt : (I)V
    //   1507: aload_0
    //   1508: invokevirtual updateScrollIndicators : ()V
    //   1511: aload_0
    //   1512: getfield mItemCount : I
    //   1515: ifle -> 1522
    //   1518: aload_0
    //   1519: invokevirtual checkSelectionChanged : ()V
    //   1522: aload_0
    //   1523: invokevirtual invokeOnItemScrollListener : ()V
    //   1526: aload_0
    //   1527: getfield mFocusSelector : Landroid/widget/ListView$FocusSelector;
    //   1530: astore_2
    //   1531: aload_2
    //   1532: ifnull -> 1539
    //   1535: aload_2
    //   1536: invokevirtual onLayoutComplete : ()V
    //   1539: iload_1
    //   1540: ifne -> 1548
    //   1543: aload_0
    //   1544: iconst_0
    //   1545: putfield mBlockLayoutRequests : Z
    //   1548: return
    //   1549: new java/lang/IllegalStateException
    //   1552: astore_2
    //   1553: new java/lang/StringBuilder
    //   1556: astore #9
    //   1558: aload #9
    //   1560: invokespecial <init> : ()V
    //   1563: aload #9
    //   1565: ldc_w 'The content of the adapter has changed but ListView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. Make sure your adapter calls notifyDataSetChanged() when its content changes. [in ListView('
    //   1568: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1571: pop
    //   1572: aload #9
    //   1574: aload_0
    //   1575: invokevirtual getId : ()I
    //   1578: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1581: pop
    //   1582: aload #9
    //   1584: ldc_w ', '
    //   1587: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1590: pop
    //   1591: aload #9
    //   1593: aload_0
    //   1594: invokevirtual getClass : ()Ljava/lang/Class;
    //   1597: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1600: pop
    //   1601: aload #9
    //   1603: ldc_w ') with Adapter('
    //   1606: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1609: pop
    //   1610: aload_0
    //   1611: getfield mAdapter : Landroid/widget/ListAdapter;
    //   1614: astore #7
    //   1616: aload #9
    //   1618: aload #7
    //   1620: invokevirtual getClass : ()Ljava/lang/Class;
    //   1623: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1626: pop
    //   1627: aload #9
    //   1629: ldc_w ')]'
    //   1632: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1635: pop
    //   1636: aload_2
    //   1637: aload #9
    //   1639: invokevirtual toString : ()Ljava/lang/String;
    //   1642: invokespecial <init> : (Ljava/lang/String;)V
    //   1645: aload_2
    //   1646: athrow
    //   1647: astore_2
    //   1648: aload_0
    //   1649: getfield mFocusSelector : Landroid/widget/ListView$FocusSelector;
    //   1652: astore #7
    //   1654: aload #7
    //   1656: ifnull -> 1664
    //   1659: aload #7
    //   1661: invokevirtual onLayoutComplete : ()V
    //   1664: iload_1
    //   1665: ifne -> 1673
    //   1668: aload_0
    //   1669: iconst_0
    //   1670: putfield mBlockLayoutRequests : Z
    //   1673: aload_2
    //   1674: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1643	-> 0
    //   #1644	-> 5
    //   #1645	-> 9
    //   #1648	-> 10
    //   #1651	-> 15
    //   #1653	-> 19
    //   #1655	-> 23
    //   #1656	-> 30
    //   #1657	-> 34
    //   #1978	-> 38
    //   #1979	-> 47
    //   #1981	-> 51
    //   #1982	-> 55
    //   #1658	-> 60
    //   #1661	-> 61
    //   #1662	-> 69
    //   #1663	-> 88
    //   #1665	-> 94
    //   #1666	-> 94
    //   #1669	-> 97
    //   #1670	-> 100
    //   #1671	-> 100
    //   #1674	-> 100
    //   #1689	-> 136
    //   #1690	-> 147
    //   #1691	-> 165
    //   #1695	-> 172
    //   #1697	-> 179
    //   #1698	-> 186
    //   #1702	-> 197
    //   #1676	-> 211
    //   #1677	-> 222
    //   #1678	-> 234
    //   #1706	-> 253
    //   #1685	-> 267
    //   #1706	-> 278
    //   #1707	-> 284
    //   #1708	-> 289
    //   #1713	-> 293
    //   #1714	-> 300
    //   #1715	-> 304
    //   #1978	-> 308
    //   #1979	-> 317
    //   #1981	-> 321
    //   #1982	-> 325
    //   #1716	-> 330
    //   #1717	-> 331
    //   #1726	-> 347
    //   #1728	-> 355
    //   #1729	-> 358
    //   #1730	-> 361
    //   #1735	-> 361
    //   #1736	-> 367
    //   #1737	-> 372
    //   #1738	-> 379
    //   #1739	-> 384
    //   #1740	-> 392
    //   #1741	-> 397
    //   #1742	-> 411
    //   #1745	-> 442
    //   #1746	-> 446
    //   #1747	-> 446
    //   #1752	-> 453
    //   #1757	-> 472
    //   #1758	-> 484
    //   #1763	-> 487
    //   #1764	-> 493
    //   #1770	-> 498
    //   #1771	-> 512
    //   #1772	-> 527
    //   #1774	-> 527
    //   #1775	-> 533
    //   #1777	-> 546
    //   #1780	-> 559
    //   #1764	-> 579
    //   #1785	-> 585
    //   #1786	-> 591
    //   #1787	-> 597
    //   #1788	-> 602
    //   #1789	-> 612
    //   #1788	-> 628
    //   #1792	-> 637
    //   #1796	-> 646
    //   #1797	-> 650
    //   #1799	-> 655
    //   #1840	-> 696
    //   #1841	-> 701
    //   #1837	-> 710
    //   #1838	-> 723
    //   #1808	-> 726
    //   #1809	-> 739
    //   #1820	-> 742
    //   #1821	-> 748
    //   #1828	-> 759
    //   #1829	-> 770
    //   #1830	-> 776
    //   #1831	-> 785
    //   #1832	-> 790
    //   #1865	-> 797
    //   #1811	-> 800
    //   #1812	-> 813
    //   #1813	-> 817
    //   #1801	-> 820
    //   #1802	-> 825
    //   #1804	-> 841
    //   #1806	-> 849
    //   #1815	-> 852
    //   #1816	-> 857
    //   #1817	-> 863
    //   #1818	-> 867
    //   #1841	-> 870
    //   #1842	-> 875
    //   #1843	-> 883
    //   #1844	-> 889
    //   #1845	-> 895
    //   #1846	-> 898
    //   #1847	-> 911
    //   #1848	-> 917
    //   #1849	-> 930
    //   #1851	-> 933
    //   #1852	-> 951
    //   #1853	-> 957
    //   #1852	-> 973
    //   #1854	-> 985
    //   #1855	-> 996
    //   #1856	-> 1002
    //   #1855	-> 1020
    //   #1858	-> 1032
    //   #1865	-> 1039
    //   #1868	-> 1044
    //   #1869	-> 1052
    //   #1871	-> 1060
    //   #1874	-> 1064
    //   #1875	-> 1085
    //   #1877	-> 1096
    //   #1878	-> 1120
    //   #1882	-> 1125
    //   #1883	-> 1131
    //   #1884	-> 1136
    //   #1886	-> 1141
    //   #1887	-> 1147
    //   #1888	-> 1150
    //   #1889	-> 1155
    //   #1891	-> 1162
    //   #1874	-> 1165
    //   #1892	-> 1165
    //   #1894	-> 1171
    //   #1896	-> 1182
    //   #1898	-> 1210
    //   #1900	-> 1215
    //   #1901	-> 1229
    //   #1902	-> 1233
    //   #1904	-> 1242
    //   #1908	-> 1253
    //   #1909	-> 1267
    //   #1910	-> 1271
    //   #1912	-> 1280
    //   #1914	-> 1283
    //   #1915	-> 1288
    //   #1920	-> 1295
    //   #1921	-> 1307
    //   #1926	-> 1313
    //   #1927	-> 1318
    //   #1928	-> 1324
    //   #1929	-> 1328
    //   #1930	-> 1333
    //   #1931	-> 1341
    //   #1932	-> 1341
    //   #1933	-> 1347
    //   #1934	-> 1356
    //   #1935	-> 1356
    //   #1934	-> 1363
    //   #1936	-> 1370
    //   #1938	-> 1380
    //   #1939	-> 1383
    //   #1941	-> 1389
    //   #1943	-> 1398
    //   #1945	-> 1403
    //   #1943	-> 1409
    //   #1946	-> 1423
    //   #1947	-> 1430
    //   #1948	-> 1434
    //   #1928	-> 1442
    //   #1926	-> 1445
    //   #1956	-> 1445
    //   #1957	-> 1450
    //   #1958	-> 1458
    //   #1961	-> 1463
    //   #1962	-> 1468
    //   #1963	-> 1473
    //   #1964	-> 1480
    //   #1965	-> 1489
    //   #1967	-> 1494
    //   #1968	-> 1499
    //   #1970	-> 1507
    //   #1972	-> 1511
    //   #1973	-> 1518
    //   #1976	-> 1522
    //   #1978	-> 1526
    //   #1979	-> 1535
    //   #1981	-> 1539
    //   #1982	-> 1543
    //   #1985	-> 1548
    //   #1718	-> 1549
    //   #1722	-> 1572
    //   #1723	-> 1616
    //   #1978	-> 1647
    //   #1979	-> 1659
    //   #1981	-> 1664
    //   #1982	-> 1668
    //   #1984	-> 1673
    // Exception table:
    //   from	to	target	type
    //   15	19	1647	finally
    //   19	23	1647	finally
    //   23	30	1647	finally
    //   30	34	1647	finally
    //   34	38	1647	finally
    //   61	69	1647	finally
    //   69	88	1647	finally
    //   88	94	1647	finally
    //   100	106	1647	finally
    //   136	147	1647	finally
    //   165	172	1647	finally
    //   172	179	1647	finally
    //   179	186	1647	finally
    //   186	197	1647	finally
    //   197	208	1647	finally
    //   211	222	1647	finally
    //   234	242	1647	finally
    //   278	284	1647	finally
    //   289	293	1647	finally
    //   293	300	1647	finally
    //   300	304	1647	finally
    //   304	308	1647	finally
    //   331	347	1647	finally
    //   347	355	1647	finally
    //   361	367	1647	finally
    //   372	379	1647	finally
    //   384	392	1647	finally
    //   402	411	1647	finally
    //   419	427	1647	finally
    //   435	442	1647	finally
    //   446	453	1647	finally
    //   453	461	1647	finally
    //   487	493	1647	finally
    //   503	512	1647	finally
    //   512	520	1647	finally
    //   520	527	1647	finally
    //   527	533	1647	finally
    //   546	551	1647	finally
    //   559	564	1647	finally
    //   585	591	1647	finally
    //   591	597	1647	finally
    //   612	628	1647	finally
    //   637	646	1647	finally
    //   646	650	1647	finally
    //   650	655	1647	finally
    //   655	696	1647	finally
    //   701	707	1647	finally
    //   710	723	1647	finally
    //   726	739	1647	finally
    //   742	748	1647	finally
    //   748	759	1647	finally
    //   763	770	1647	finally
    //   770	776	1647	finally
    //   776	785	1647	finally
    //   790	797	1647	finally
    //   800	813	1647	finally
    //   813	817	1647	finally
    //   825	838	1647	finally
    //   841	849	1647	finally
    //   852	857	1647	finally
    //   857	863	1647	finally
    //   863	867	1647	finally
    //   875	883	1647	finally
    //   883	889	1647	finally
    //   889	895	1647	finally
    //   898	911	1647	finally
    //   911	917	1647	finally
    //   917	930	1647	finally
    //   933	951	1647	finally
    //   951	957	1647	finally
    //   967	973	1647	finally
    //   973	982	1647	finally
    //   985	996	1647	finally
    //   996	1002	1647	finally
    //   1013	1020	1647	finally
    //   1020	1029	1647	finally
    //   1032	1039	1647	finally
    //   1039	1044	1647	finally
    //   1044	1052	1647	finally
    //   1052	1060	1647	finally
    //   1064	1085	1647	finally
    //   1096	1104	1647	finally
    //   1104	1111	1647	finally
    //   1125	1131	1647	finally
    //   1136	1141	1647	finally
    //   1141	1147	1647	finally
    //   1150	1155	1647	finally
    //   1155	1162	1647	finally
    //   1165	1171	1647	finally
    //   1171	1179	1647	finally
    //   1182	1198	1647	finally
    //   1215	1229	1647	finally
    //   1233	1242	1647	finally
    //   1245	1253	1647	finally
    //   1253	1267	1647	finally
    //   1271	1280	1647	finally
    //   1283	1288	1647	finally
    //   1288	1295	1647	finally
    //   1295	1302	1647	finally
    //   1307	1313	1647	finally
    //   1318	1324	1647	finally
    //   1333	1341	1647	finally
    //   1341	1347	1647	finally
    //   1356	1363	1647	finally
    //   1363	1370	1647	finally
    //   1370	1380	1647	finally
    //   1383	1389	1647	finally
    //   1398	1403	1647	finally
    //   1403	1409	1647	finally
    //   1409	1423	1647	finally
    //   1423	1430	1647	finally
    //   1434	1439	1647	finally
    //   1450	1458	1647	finally
    //   1458	1463	1647	finally
    //   1463	1468	1647	finally
    //   1468	1473	1647	finally
    //   1473	1480	1647	finally
    //   1480	1489	1647	finally
    //   1489	1494	1647	finally
    //   1494	1499	1647	finally
    //   1499	1507	1647	finally
    //   1507	1511	1647	finally
    //   1511	1518	1647	finally
    //   1518	1522	1647	finally
    //   1522	1526	1647	finally
    //   1549	1572	1647	finally
    //   1572	1616	1647	finally
    //   1616	1647	1647	finally
  }
  
  boolean trackMotionScroll(int paramInt1, int paramInt2) {
    boolean bool = super.trackMotionScroll(paramInt1, paramInt2);
    removeUnusedFixedViews(this.mHeaderViewInfos);
    removeUnusedFixedViews(this.mFooterViewInfos);
    return bool;
  }
  
  private void removeUnusedFixedViews(List<FixedViewInfo> paramList) {
    if (paramList == null)
      return; 
    for (int i = paramList.size() - 1; i >= 0; i--) {
      FixedViewInfo fixedViewInfo = paramList.get(i);
      View view = fixedViewInfo.view;
      AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)view.getLayoutParams();
      if (view.getParent() == null && layoutParams != null && layoutParams.recycledHeaderFooter) {
        removeDetachedView(view, false);
        layoutParams.recycledHeaderFooter = false;
      } 
    } 
  }
  
  private boolean isDirectChildHeaderOrFooter(View paramView) {
    ArrayList<FixedViewInfo> arrayList = this.mHeaderViewInfos;
    int i = arrayList.size();
    byte b;
    for (b = 0; b < i; b++) {
      if (paramView == ((FixedViewInfo)arrayList.get(b)).view)
        return true; 
    } 
    arrayList = this.mFooterViewInfos;
    i = arrayList.size();
    for (b = 0; b < i; b++) {
      if (paramView == ((FixedViewInfo)arrayList.get(b)).view)
        return true; 
    } 
    return false;
  }
  
  private View makeAndAddView(int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2) {
    boolean bool;
    if (!this.mDataChanged) {
      View view1 = this.mRecycler.getActiveView(paramInt1);
      if (view1 != null) {
        setupChild(view1, paramInt1, paramInt2, paramBoolean1, paramInt3, paramBoolean2, true);
        return view1;
      } 
    } 
    if (paramInt1 == this.mItemCount - 1) {
      bool = ((IOplusRedPacketManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusRedPacketManager.DEFAULT, new Object[0])).notifyBeforeEnter(getContext().getPackageName());
    } else {
      bool = false;
    } 
    View view = obtainView(paramInt1, this.mIsScrap);
    if (bool)
      ((IOplusRedPacketManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusRedPacketManager.DEFAULT, new Object[0])).notify(view); 
    setupChild(view, paramInt1, paramInt2, paramBoolean1, paramInt3, paramBoolean2, this.mIsScrap[0]);
    return view;
  }
  
  private void setupChild(View paramView, int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, boolean paramBoolean3) {
    boolean bool;
    boolean bool1;
    Trace.traceBegin(8L, "setupListItem");
    if (paramBoolean2 && shouldShowSelector()) {
      paramBoolean2 = true;
    } else {
      paramBoolean2 = false;
    } 
    if (paramBoolean2 != paramView.isSelected()) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = this.mTouchMode;
    if (j > 0 && j < 3 && this.mMotionPosition == paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool != paramView.isPressed()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (!paramBoolean3 || i || 
      paramView.isLayoutRequested()) {
      j = 1;
    } else {
      j = 0;
    } 
    AbsListView.LayoutParams layoutParams1 = (AbsListView.LayoutParams)paramView.getLayoutParams();
    AbsListView.LayoutParams layoutParams2 = layoutParams1;
    if (layoutParams1 == null)
      layoutParams2 = (AbsListView.LayoutParams)generateDefaultLayoutParams(); 
    layoutParams2.viewType = this.mAdapter.getItemViewType(paramInt1);
    layoutParams2.isEnabled = this.mAdapter.isEnabled(paramInt1);
    if (i)
      paramView.setSelected(paramBoolean2); 
    if (bool1)
      paramView.setPressed(bool); 
    if (this.mChoiceMode != 0 && this.mCheckStates != null)
      if (paramView instanceof Checkable) {
        ((Checkable)paramView).setChecked(this.mCheckStates.get(paramInt1));
      } else if ((getContext().getApplicationInfo()).targetSdkVersion >= 11) {
        paramView.setActivated(this.mCheckStates.get(paramInt1));
      }  
    int i = -1;
    if ((paramBoolean3 && !layoutParams2.forceAdd) || (layoutParams2.recycledHeaderFooter && layoutParams2.viewType == -2)) {
      if (!paramBoolean1)
        i = 0; 
      attachViewToParent(paramView, i, layoutParams2);
      if (paramBoolean3 && 
        ((AbsListView.LayoutParams)paramView.getLayoutParams()).scrappedFromPosition != paramInt1)
        paramView.jumpDrawablesToCurrentState(); 
    } else {
      layoutParams2.forceAdd = false;
      if (layoutParams2.viewType == -2)
        layoutParams2.recycledHeaderFooter = true; 
      if (!paramBoolean1)
        i = 0; 
      addViewInLayout(paramView, i, layoutParams2, true);
      paramView.resolveRtlPropertiesIfNeeded();
    } 
    if (j != 0) {
      i = ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.mListPadding.left + this.mListPadding.right, layoutParams2.width);
      paramInt1 = layoutParams2.height;
      if (paramInt1 > 0) {
        paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
      } else {
        paramInt1 = View.MeasureSpec.makeSafeMeasureSpec(getMeasuredHeight(), 0);
      } 
      paramView.measure(i, paramInt1);
    } else {
      cleanupLayoutState(paramView);
    } 
    i = paramView.getMeasuredWidth();
    paramInt1 = paramView.getMeasuredHeight();
    if (!paramBoolean1)
      paramInt2 -= paramInt1; 
    if (j != 0) {
      paramView.layout(paramInt3, paramInt2, paramInt3 + i, paramInt2 + paramInt1);
    } else {
      paramView.offsetLeftAndRight(paramInt3 - paramView.getLeft());
      paramView.offsetTopAndBottom(paramInt2 - paramView.getTop());
    } 
    if (this.mCachingStarted && !paramView.isDrawingCacheEnabled())
      paramView.setDrawingCacheEnabled(true); 
    Trace.traceEnd(8L);
  }
  
  protected boolean canAnimate() {
    boolean bool;
    if (super.canAnimate() && this.mItemCount > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setSelection(int paramInt) {
    setSelectionFromTop(paramInt, 0);
  }
  
  void setSelectionInt(int paramInt) {
    setNextSelectedPositionInt(paramInt);
    boolean bool1 = false;
    int i = this.mSelectedPosition;
    boolean bool2 = bool1;
    if (i >= 0)
      if (paramInt == i - 1) {
        bool2 = true;
      } else {
        bool2 = bool1;
        if (paramInt == i + 1)
          bool2 = true; 
      }  
    if (this.mPositionScroller != null)
      this.mPositionScroller.stop(); 
    layoutChildren();
    if (bool2)
      awakenScrollBars(); 
  }
  
  int lookForSelectablePosition(int paramInt, boolean paramBoolean) {
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter == null || isInTouchMode())
      return -1; 
    int i = listAdapter.getCount();
    int j = paramInt;
    if (!this.mAreAllItemsSelectable)
      if (paramBoolean) {
        paramInt = Math.max(0, paramInt);
        while (true) {
          j = paramInt;
          if (paramInt < i) {
            j = paramInt;
            if (!listAdapter.isEnabled(paramInt)) {
              paramInt++;
              continue;
            } 
          } 
          break;
        } 
      } else {
        paramInt = Math.min(paramInt, i - 1);
        while (true) {
          j = paramInt;
          if (paramInt >= 0) {
            j = paramInt;
            if (!listAdapter.isEnabled(paramInt)) {
              paramInt--;
              continue;
            } 
          } 
          break;
        } 
      }  
    if (j < 0 || j >= i)
      return -1; 
    return j;
  }
  
  int lookForSelectablePositionAfter(int paramInt1, int paramInt2, boolean paramBoolean) {
    ListAdapter listAdapter = this.mAdapter;
    if (listAdapter == null || isInTouchMode())
      return -1; 
    int i = lookForSelectablePosition(paramInt2, paramBoolean);
    if (i != -1)
      return i; 
    int j = listAdapter.getCount();
    i = MathUtils.constrain(paramInt1, -1, j - 1);
    if (paramBoolean) {
      paramInt1 = Math.min(paramInt2 - 1, j - 1);
      while (paramInt1 > i && !listAdapter.isEnabled(paramInt1))
        paramInt1--; 
      paramInt2 = paramInt1;
      if (paramInt1 <= i)
        return -1; 
    } else {
      paramInt1 = Math.max(0, paramInt2 + 1);
      while (paramInt1 < i && !listAdapter.isEnabled(paramInt1))
        paramInt1++; 
      paramInt2 = paramInt1;
      if (paramInt1 >= i)
        return -1; 
    } 
    return paramInt2;
  }
  
  public void setSelectionAfterHeaderView() {
    int i = getHeaderViewsCount();
    if (i > 0) {
      this.mNextSelectedPosition = 0;
      return;
    } 
    if (this.mAdapter != null) {
      setSelection(i);
    } else {
      this.mNextSelectedPosition = i;
      this.mLayoutMode = 2;
    } 
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    boolean bool1 = super.dispatchKeyEvent(paramKeyEvent);
    boolean bool2 = bool1;
    if (!bool1) {
      View view = getFocusedChild();
      bool2 = bool1;
      if (view != null) {
        bool2 = bool1;
        if (paramKeyEvent.getAction() == 0)
          bool2 = onKeyDown(paramKeyEvent.getKeyCode(), paramKeyEvent); 
      } 
    } 
    return bool2;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    return commonKey(paramInt, 1, paramKeyEvent);
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    return commonKey(paramInt1, paramInt2, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    return commonKey(paramInt, 1, paramKeyEvent);
  }
  
  private boolean commonKey(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    if (this.mAdapter == null || !isAttachedToWindow())
      return false; 
    if (this.mDataChanged)
      layoutChildren(); 
    boolean bool1 = false;
    int i = paramKeyEvent.getAction();
    boolean bool2 = bool1;
    if (KeyEvent.isConfirmKey(paramInt1)) {
      bool2 = bool1;
      if (paramKeyEvent.hasNoModifiers()) {
        bool2 = bool1;
        if (i != 1) {
          bool1 = resurrectSelectionIfNeeded();
          bool2 = bool1;
          if (!bool1) {
            bool2 = bool1;
            if (paramKeyEvent.getRepeatCount() == 0) {
              bool2 = bool1;
              if (getChildCount() > 0) {
                keyPressed();
                bool2 = true;
              } 
            } 
          } 
        } 
      } 
    } 
    bool1 = bool2;
    int j = paramInt2;
    if (!bool2) {
      bool1 = bool2;
      j = paramInt2;
      if (i != 1)
        if (paramInt1 != 61) {
          if (paramInt1 != 92) {
            if (paramInt1 != 93) {
              if (paramInt1 != 122) {
                if (paramInt1 != 123) {
                  switch (paramInt1) {
                    default:
                      bool1 = bool2;
                      j = paramInt2;
                      break;
                    case 22:
                      bool1 = bool2;
                      j = paramInt2;
                      if (paramKeyEvent.hasNoModifiers()) {
                        bool1 = handleHorizontalFocusWithinListItem(66);
                        j = paramInt2;
                      } 
                      break;
                    case 21:
                      bool1 = bool2;
                      j = paramInt2;
                      if (paramKeyEvent.hasNoModifiers()) {
                        bool1 = handleHorizontalFocusWithinListItem(17);
                        j = paramInt2;
                      } 
                      break;
                    case 20:
                      if (paramKeyEvent.hasNoModifiers()) {
                        bool2 = resurrectSelectionIfNeeded();
                        bool1 = bool2;
                        j = paramInt2;
                        if (!bool2) {
                          int k = paramInt2;
                          while (true) {
                            j = k - 1;
                            bool1 = bool2;
                            paramInt2 = j;
                            if (k > 0) {
                              bool1 = bool2;
                              paramInt2 = j;
                              if (arrowScroll(130)) {
                                bool2 = true;
                                k = j;
                                continue;
                              } 
                            } 
                            break;
                          } 
                        } else {
                          break;
                        } 
                      } else {
                        bool1 = bool2;
                        j = paramInt2;
                        if (paramKeyEvent.hasModifiers(2)) {
                          if (resurrectSelectionIfNeeded() || fullScroll(130)) {
                            bool1 = true;
                          } else {
                            bool1 = false;
                          } 
                          j = paramInt2;
                        } 
                        break;
                      } 
                      j = paramInt2;
                      break;
                    case 19:
                      if (paramKeyEvent.hasNoModifiers()) {
                        bool2 = resurrectSelectionIfNeeded();
                        bool1 = bool2;
                        j = paramInt2;
                        if (!bool2) {
                          j = paramInt2;
                          while (true) {
                            int k = j - 1;
                            bool1 = bool2;
                            paramInt2 = k;
                            if (j > 0) {
                              bool1 = bool2;
                              paramInt2 = k;
                              if (arrowScroll(33)) {
                                bool2 = true;
                                j = k;
                                continue;
                              } 
                            } 
                            break;
                          } 
                        } else {
                          break;
                        } 
                      } else {
                        bool1 = bool2;
                        j = paramInt2;
                        if (paramKeyEvent.hasModifiers(2)) {
                          if (resurrectSelectionIfNeeded() || fullScroll(33)) {
                            bool1 = true;
                          } else {
                            bool1 = false;
                          } 
                          j = paramInt2;
                        } 
                        break;
                      } 
                      j = paramInt2;
                      break;
                  } 
                } else {
                  bool1 = bool2;
                  j = paramInt2;
                  if (paramKeyEvent.hasNoModifiers()) {
                    if (resurrectSelectionIfNeeded() || fullScroll(130)) {
                      bool1 = true;
                    } else {
                      bool1 = false;
                    } 
                    j = paramInt2;
                  } 
                } 
              } else {
                bool1 = bool2;
                j = paramInt2;
                if (paramKeyEvent.hasNoModifiers()) {
                  if (resurrectSelectionIfNeeded() || fullScroll(33)) {
                    bool1 = true;
                  } else {
                    bool1 = false;
                  } 
                  j = paramInt2;
                } 
              } 
            } else if (paramKeyEvent.hasNoModifiers()) {
              if (resurrectSelectionIfNeeded() || pageScroll(130)) {
                bool1 = true;
              } else {
                bool1 = false;
              } 
              j = paramInt2;
            } else {
              bool1 = bool2;
              j = paramInt2;
              if (paramKeyEvent.hasModifiers(2)) {
                if (resurrectSelectionIfNeeded() || fullScroll(130)) {
                  bool1 = true;
                } else {
                  bool1 = false;
                } 
                j = paramInt2;
              } 
            } 
          } else if (paramKeyEvent.hasNoModifiers()) {
            if (resurrectSelectionIfNeeded() || pageScroll(33)) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            j = paramInt2;
          } else {
            bool1 = bool2;
            j = paramInt2;
            if (paramKeyEvent.hasModifiers(2)) {
              if (resurrectSelectionIfNeeded() || fullScroll(33)) {
                bool1 = true;
              } else {
                bool1 = false;
              } 
              j = paramInt2;
            } 
          } 
        } else if (paramKeyEvent.hasNoModifiers()) {
          if (resurrectSelectionIfNeeded() || arrowScroll(130)) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          j = paramInt2;
        } else {
          bool1 = bool2;
          j = paramInt2;
          if (paramKeyEvent.hasModifiers(1)) {
            if (resurrectSelectionIfNeeded() || arrowScroll(33)) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            j = paramInt2;
          } 
        }  
    } 
    if (bool1)
      return true; 
    if (sendToTextFilter(paramInt1, j, paramKeyEvent))
      return true; 
    if (i != 0) {
      if (i != 1) {
        if (i != 2)
          return false; 
        return super.onKeyMultiple(paramInt1, j, paramKeyEvent);
      } 
      return super.onKeyUp(paramInt1, paramKeyEvent);
    } 
    return super.onKeyDown(paramInt1, paramKeyEvent);
  }
  
  boolean pageScroll(int paramInt) {
    boolean bool;
    if (paramInt == 33) {
      paramInt = Math.max(0, this.mSelectedPosition - getChildCount() - 1);
      bool = false;
    } else if (paramInt == 130) {
      paramInt = Math.min(this.mItemCount - 1, this.mSelectedPosition + getChildCount() - 1);
      bool = true;
    } else {
      return false;
    } 
    if (paramInt >= 0) {
      paramInt = lookForSelectablePositionAfter(this.mSelectedPosition, paramInt, bool);
      if (paramInt >= 0) {
        this.mLayoutMode = 4;
        this.mSpecificTop = this.mPaddingTop + getVerticalFadingEdgeLength();
        if (bool && paramInt > this.mItemCount - getChildCount())
          this.mLayoutMode = 3; 
        if (!bool && paramInt < getChildCount())
          this.mLayoutMode = 1; 
        setSelectionInt(paramInt);
        invokeOnItemScrollListener();
        if (!awakenScrollBars())
          invalidate(); 
        return true;
      } 
    } 
    return false;
  }
  
  boolean fullScroll(int paramInt) {
    boolean bool2, bool1 = false;
    if (paramInt == 33) {
      bool2 = bool1;
      if (this.mSelectedPosition != 0) {
        paramInt = lookForSelectablePositionAfter(this.mSelectedPosition, 0, true);
        if (paramInt >= 0) {
          this.mLayoutMode = 1;
          setSelectionInt(paramInt);
          invokeOnItemScrollListener();
        } 
        bool2 = true;
      } 
    } else {
      bool2 = bool1;
      if (paramInt == 130) {
        paramInt = this.mItemCount - 1;
        bool2 = bool1;
        if (this.mSelectedPosition < paramInt) {
          paramInt = lookForSelectablePositionAfter(this.mSelectedPosition, paramInt, false);
          if (paramInt >= 0) {
            this.mLayoutMode = 3;
            setSelectionInt(paramInt);
            invokeOnItemScrollListener();
          } 
          bool2 = true;
        } 
      } 
    } 
    if (bool2 && !awakenScrollBars()) {
      awakenScrollBars();
      invalidate();
    } 
    return bool2;
  }
  
  private boolean handleHorizontalFocusWithinListItem(int paramInt) {
    if (paramInt == 17 || paramInt == 66) {
      int i = getChildCount();
      if (this.mItemsCanFocus && i > 0 && this.mSelectedPosition != -1) {
        View view = getSelectedView();
        if (view != null && view.hasFocus() && view instanceof ViewGroup) {
          View view1 = view.findFocus();
          View view2 = FocusFinder.getInstance().findNextFocus((ViewGroup)view, view1, paramInt);
          if (view2 != null) {
            Rect rect = this.mTempRect;
            if (view1 != null) {
              view1.getFocusedRect(rect);
              offsetDescendantRectToMyCoords(view1, rect);
              offsetRectIntoDescendantCoords(view2, rect);
            } else {
              rect = null;
            } 
            if (view2.requestFocus(paramInt, rect))
              return true; 
          } 
          FocusFinder focusFinder = FocusFinder.getInstance();
          view = getRootView();
          view = focusFinder.findNextFocus((ViewGroup)view, view1, paramInt);
          if (view != null)
            return isViewAncestorOf(view, this); 
        } 
      } 
      return false;
    } 
    throw new IllegalArgumentException("direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT}");
  }
  
  boolean arrowScroll(int paramInt) {
    try {
      this.mInLayout = true;
      boolean bool = arrowScrollImpl(paramInt);
      if (bool)
        playSoundEffect(SoundEffectConstants.getContantForFocusDirection(paramInt)); 
      return bool;
    } finally {
      this.mInLayout = false;
    } 
  }
  
  private final int nextSelectedPositionForDirection(View paramView, int paramInt1, int paramInt2) {
    boolean bool = true;
    if (paramInt2 == 130) {
      int i = getHeight(), j = this.mListPadding.bottom;
      if (paramView != null && paramView.getBottom() <= i - j) {
        if (paramInt1 != -1 && paramInt1 >= this.mFirstPosition) {
          paramInt1++;
        } else {
          paramInt1 = this.mFirstPosition;
        } 
      } else {
        return -1;
      } 
    } else {
      int i = this.mListPadding.top;
      if (paramView != null && paramView.getTop() >= i) {
        i = this.mFirstPosition + getChildCount() - 1;
        if (paramInt1 != -1 && paramInt1 <= i) {
          paramInt1--;
        } else {
          paramInt1 = i;
        } 
      } else {
        return -1;
      } 
    } 
    if (paramInt1 < 0 || paramInt1 >= this.mAdapter.getCount())
      return -1; 
    if (paramInt2 != 130)
      bool = false; 
    return lookForSelectablePosition(paramInt1, bool);
  }
  
  private boolean arrowScrollImpl(int paramInt) {
    ArrowScrollFocusResult arrowScrollFocusResult;
    boolean bool;
    if (getChildCount() <= 0)
      return false; 
    View view1 = getSelectedView();
    int i = this.mSelectedPosition;
    int j = nextSelectedPositionForDirection(view1, i, paramInt);
    int k = amountToScroll(paramInt, j);
    if (this.mItemsCanFocus) {
      arrowScrollFocusResult = arrowScrollFocused(paramInt);
    } else {
      arrowScrollFocusResult = null;
    } 
    if (arrowScrollFocusResult != null) {
      j = arrowScrollFocusResult.getSelectedPosition();
      k = arrowScrollFocusResult.getAmountToScroll();
    } 
    if (arrowScrollFocusResult != null) {
      bool = true;
    } else {
      bool = false;
    } 
    View view3 = view1;
    if (j != -1) {
      boolean bool1;
      if (arrowScrollFocusResult != null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      handleNewSelectionChange(view1, paramInt, j, bool1);
      setSelectedPositionInt(j);
      setNextSelectedPositionInt(j);
      view3 = getSelectedView();
      i = j;
      if (this.mItemsCanFocus && arrowScrollFocusResult == null) {
        view1 = getFocusedChild();
        if (view1 != null)
          view1.clearFocus(); 
      } 
      bool = true;
      checkSelectionChanged();
    } 
    if (k > 0) {
      if (paramInt != 33)
        k = -k; 
      scrollListItemsBy(k);
      bool = true;
    } 
    if (this.mItemsCanFocus && arrowScrollFocusResult == null && view3 != null && 
      view3.hasFocus()) {
      View view = view3.findFocus();
      if (view != null && (
        !isViewAncestorOf(view, this) || distanceToView(view) > 0))
        view.clearFocus(); 
    } 
    View view2 = view3;
    if (j == -1) {
      view2 = view3;
      if (view3 != null) {
        view2 = view3;
        if (!isViewAncestorOf(view3, this)) {
          view2 = null;
          hideSelector();
          this.mResurrectToPosition = -1;
        } 
      } 
    } 
    if (bool) {
      if (view2 != null) {
        positionSelectorLikeFocus(i, view2);
        this.mSelectedTop = view2.getTop();
      } 
      if (!awakenScrollBars())
        invalidate(); 
      invokeOnItemScrollListener();
      return true;
    } 
    return false;
  }
  
  private void handleNewSelectionChange(View paramView, int paramInt1, int paramInt2, boolean paramBoolean) {
    if (paramInt2 != -1) {
      View view1, view2;
      int i = 0;
      int j = this.mSelectedPosition - this.mFirstPosition;
      paramInt2 -= this.mFirstPosition;
      if (paramInt1 == 33) {
        paramInt1 = paramInt2;
        i = j;
        view1 = getChildAt(paramInt1);
        paramInt2 = 1;
        j = paramInt1;
        paramInt1 = i;
        view2 = paramView;
      } else {
        paramInt1 = paramInt2;
        view2 = getChildAt(paramInt1);
        view1 = paramView;
        paramInt2 = i;
      } 
      i = getChildCount();
      boolean bool = true;
      if (view1 != null) {
        boolean bool1;
        if (!paramBoolean && paramInt2 != 0) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        view1.setSelected(bool1);
        measureAndAdjustDown(view1, j, i);
      } 
      if (view2 != null) {
        if (!paramBoolean && paramInt2 == 0) {
          paramBoolean = bool;
        } else {
          paramBoolean = false;
        } 
        view2.setSelected(paramBoolean);
        measureAndAdjustDown(view2, paramInt1, i);
      } 
      return;
    } 
    throw new IllegalArgumentException("newSelectedPosition needs to be valid");
  }
  
  private void measureAndAdjustDown(View paramView, int paramInt1, int paramInt2) {
    int i = paramView.getHeight();
    measureItem(paramView);
    if (paramView.getMeasuredHeight() != i) {
      relayoutMeasuredItem(paramView);
      int j = paramView.getMeasuredHeight();
      for (; ++paramInt1 < paramInt2; paramInt1++)
        getChildAt(paramInt1).offsetTopAndBottom(j - i); 
    } 
  }
  
  private void measureItem(View paramView) {
    ViewGroup.LayoutParams layoutParams1 = paramView.getLayoutParams();
    ViewGroup.LayoutParams layoutParams2 = layoutParams1;
    if (layoutParams1 == null)
      layoutParams2 = new ViewGroup.LayoutParams(-1, -2); 
    int i = ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.mListPadding.left + this.mListPadding.right, layoutParams2.width);
    int j = layoutParams2.height;
    if (j > 0) {
      j = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
    } else {
      j = View.MeasureSpec.makeSafeMeasureSpec(getMeasuredHeight(), 0);
    } 
    paramView.measure(i, j);
  }
  
  private void relayoutMeasuredItem(View paramView) {
    int i = paramView.getMeasuredWidth();
    int j = paramView.getMeasuredHeight();
    int k = this.mListPadding.left;
    int m = paramView.getTop();
    paramView.layout(k, m, k + i, m + j);
  }
  
  private int getArrowScrollPreviewLength() {
    return Math.max(2, getVerticalFadingEdgeLength());
  }
  
  private int amountToScroll(int paramInt1, int paramInt2) {
    int i = getHeight() - this.mListPadding.bottom;
    int j = this.mListPadding.top;
    int k = getChildCount();
    if (paramInt1 == 130) {
      int n = k - 1;
      paramInt1 = k;
      if (paramInt2 != -1) {
        n = paramInt2 - this.mFirstPosition;
        paramInt1 = k;
      } 
      while (paramInt1 <= n) {
        addViewBelow(getChildAt(paramInt1 - 1), this.mFirstPosition + paramInt1 - 1);
        paramInt1++;
      } 
      int i1 = this.mFirstPosition;
      View view1 = getChildAt(n);
      j = i;
      k = j;
      if (i1 + n < this.mItemCount - 1)
        k = j - getArrowScrollPreviewLength(); 
      if (view1.getBottom() <= k)
        return 0; 
      if (paramInt2 != -1 && 
        k - view1.getTop() >= getMaxScrollAmount())
        return 0; 
      n = view1.getBottom() - k;
      paramInt2 = n;
      if (this.mFirstPosition + paramInt1 == this.mItemCount) {
        paramInt1 = getChildAt(paramInt1 - 1).getBottom();
        paramInt2 = Math.min(n, paramInt1 - i);
      } 
      return Math.min(paramInt2, getMaxScrollAmount());
    } 
    paramInt1 = 0;
    if (paramInt2 != -1)
      paramInt1 = paramInt2 - this.mFirstPosition; 
    while (paramInt1 < 0) {
      addViewAbove(getChildAt(0), this.mFirstPosition);
      this.mFirstPosition--;
      paramInt1 = paramInt2 - this.mFirstPosition;
    } 
    i = this.mFirstPosition;
    View view = getChildAt(paramInt1);
    int m = j;
    k = m;
    if (i + paramInt1 > 0)
      k = m + getArrowScrollPreviewLength(); 
    if (view.getTop() >= k)
      return 0; 
    if (paramInt2 != -1 && 
      view.getBottom() - k >= getMaxScrollAmount())
      return 0; 
    paramInt2 = k - view.getTop();
    paramInt1 = paramInt2;
    if (this.mFirstPosition == 0) {
      paramInt1 = getChildAt(0).getTop();
      paramInt1 = Math.min(paramInt2, j - paramInt1);
    } 
    return Math.min(paramInt1, getMaxScrollAmount());
  }
  
  class ArrowScrollFocusResult {
    private int mAmountToScroll;
    
    private int mSelectedPosition;
    
    private ArrowScrollFocusResult() {}
    
    void populate(int param1Int1, int param1Int2) {
      this.mSelectedPosition = param1Int1;
      this.mAmountToScroll = param1Int2;
    }
    
    public int getSelectedPosition() {
      return this.mSelectedPosition;
    }
    
    public int getAmountToScroll() {
      return this.mAmountToScroll;
    }
  }
  
  private int lookForSelectablePositionOnScreen(int paramInt) {
    int i = this.mFirstPosition;
    if (paramInt == 130) {
      if (this.mSelectedPosition != -1) {
        j = this.mSelectedPosition + 1;
      } else {
        j = i;
      } 
      if (j >= this.mAdapter.getCount())
        return -1; 
      paramInt = j;
      if (j < i)
        paramInt = i; 
      int j = getLastVisiblePosition();
      ListAdapter listAdapter = getAdapter();
      for (; paramInt <= j; paramInt++) {
        if (listAdapter.isEnabled(paramInt) && 
          getChildAt(paramInt - i).getVisibility() == 0)
          return paramInt; 
      } 
    } else {
      int j, k = getChildCount() + i - 1;
      if (this.mSelectedPosition != -1) {
        j = this.mSelectedPosition - 1;
      } else {
        j = getChildCount() + i - 1;
      } 
      if (j < 0 || j >= this.mAdapter.getCount())
        return -1; 
      paramInt = j;
      if (j > k)
        paramInt = k; 
      ListAdapter listAdapter = getAdapter();
      for (; paramInt >= i; paramInt--) {
        if (listAdapter.isEnabled(paramInt) && 
          getChildAt(paramInt - i).getVisibility() == 0)
          return paramInt; 
      } 
    } 
    return -1;
  }
  
  private ArrowScrollFocusResult arrowScrollFocused(int paramInt) {
    View view = getSelectedView();
    if (view != null && view.hasFocus()) {
      view = view.findFocus();
      view = FocusFinder.getInstance().findNextFocus(this, view, paramInt);
    } else {
      int i = 1, j = 1;
      if (paramInt == 130) {
        if (this.mFirstPosition > 0) {
          i = j;
        } else {
          i = 0;
        } 
        j = this.mListPadding.top;
        if (i != 0) {
          i = getArrowScrollPreviewLength();
        } else {
          i = 0;
        } 
        i = j + i;
        if (view != null && view.getTop() > i)
          i = view.getTop(); 
        this.mTempRect.set(0, i, 0, i);
      } else {
        j = this.mFirstPosition;
        if (j + getChildCount() - 1 >= this.mItemCount)
          i = 0; 
        int k = getHeight();
        j = this.mListPadding.bottom;
        if (i != 0) {
          i = getArrowScrollPreviewLength();
        } else {
          i = 0;
        } 
        i = k - j - i;
        if (view != null && view.getBottom() < i)
          i = view.getBottom(); 
        this.mTempRect.set(0, i, 0, i);
      } 
      view = FocusFinder.getInstance().findNextFocusFromRect(this, this.mTempRect, paramInt);
    } 
    if (view != null) {
      int i = positionOfNewFocus(view);
      if (this.mSelectedPosition != -1 && i != this.mSelectedPosition) {
        int m = lookForSelectablePositionOnScreen(paramInt);
        if (m != -1 && ((paramInt == 130 && m < i) || (paramInt == 33 && m > i)))
          return null; 
      } 
      int j = amountToScrollToNewFocus(paramInt, view, i);
      int k = getMaxScrollAmount();
      if (j < k) {
        view.requestFocus(paramInt);
        this.mArrowScrollFocusResult.populate(i, j);
        return this.mArrowScrollFocusResult;
      } 
      if (distanceToView(view) < k) {
        view.requestFocus(paramInt);
        this.mArrowScrollFocusResult.populate(i, k);
        return this.mArrowScrollFocusResult;
      } 
    } 
    return null;
  }
  
  private int positionOfNewFocus(View paramView) {
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (isViewAncestorOf(paramView, view))
        return this.mFirstPosition + b; 
    } 
    throw new IllegalArgumentException("newFocus is not a child of any of the children of the list!");
  }
  
  private boolean isViewAncestorOf(View paramView1, View paramView2) {
    boolean bool = true;
    if (paramView1 == paramView2)
      return true; 
    ViewParent viewParent = paramView1.getParent();
    if (!(viewParent instanceof ViewGroup) || !isViewAncestorOf((View)viewParent, paramView2))
      bool = false; 
    return bool;
  }
  
  private int amountToScrollToNewFocus(int paramInt1, View paramView, int paramInt2) {
    int i = 0;
    paramView.getDrawingRect(this.mTempRect);
    offsetDescendantRectToMyCoords(paramView, this.mTempRect);
    if (paramInt1 == 33) {
      paramInt1 = i;
      if (this.mTempRect.top < this.mListPadding.top) {
        i = this.mListPadding.top - this.mTempRect.top;
        paramInt1 = i;
        if (paramInt2 > 0)
          paramInt1 = i + getArrowScrollPreviewLength(); 
      } 
    } else {
      int j = getHeight() - this.mListPadding.bottom;
      paramInt1 = i;
      if (this.mTempRect.bottom > j) {
        i = this.mTempRect.bottom - j;
        paramInt1 = i;
        if (paramInt2 < this.mItemCount - 1)
          paramInt1 = i + getArrowScrollPreviewLength(); 
      } 
    } 
    return paramInt1;
  }
  
  private int distanceToView(View paramView) {
    int i = 0;
    paramView.getDrawingRect(this.mTempRect);
    offsetDescendantRectToMyCoords(paramView, this.mTempRect);
    int j = this.mBottom - this.mTop - this.mListPadding.bottom;
    if (this.mTempRect.bottom < this.mListPadding.top) {
      i = this.mListPadding.top - this.mTempRect.bottom;
    } else if (this.mTempRect.top > j) {
      i = this.mTempRect.top - j;
    } 
    return i;
  }
  
  private void scrollListItemsBy(int paramInt) {
    int i = this.mScrollX;
    int j = this.mScrollY;
    offsetChildrenTopAndBottom(paramInt);
    int k = getHeight() - this.mListPadding.bottom;
    int m = this.mListPadding.top;
    AbsListView.RecycleBin recycleBin = this.mRecycler;
    if (paramInt < 0) {
      paramInt = getChildCount();
      View view = getChildAt(paramInt - 1);
      while (view.getBottom() < k) {
        int n = this.mFirstPosition + paramInt - 1;
        if (n < this.mItemCount - 1) {
          view = addViewBelow(view, n);
          paramInt++;
        } 
      } 
      if (view.getBottom() < k)
        offsetChildrenTopAndBottom(k - view.getBottom()); 
      view = getChildAt(0);
      while (view.getBottom() < m) {
        AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)view.getLayoutParams();
        if (recycleBin.shouldRecycleViewType(layoutParams.viewType))
          recycleBin.addScrapView(view, this.mFirstPosition); 
        detachViewFromParent(view);
        view = getChildAt(0);
        this.mFirstPosition++;
      } 
    } else {
      View view = getChildAt(0);
      while (view.getTop() > m && this.mFirstPosition > 0) {
        view = addViewAbove(view, this.mFirstPosition);
        this.mFirstPosition--;
      } 
      if (view.getTop() > m)
        offsetChildrenTopAndBottom(m - view.getTop()); 
      paramInt = getChildCount() - 1;
      view = getChildAt(paramInt);
      while (view.getTop() > k) {
        AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)view.getLayoutParams();
        if (recycleBin.shouldRecycleViewType(layoutParams.viewType))
          recycleBin.addScrapView(view, this.mFirstPosition + paramInt); 
        detachViewFromParent(view);
        view = getChildAt(--paramInt);
      } 
    } 
    recycleBin.fullyDetachScrapViews();
    removeUnusedFixedViews(this.mHeaderViewInfos);
    removeUnusedFixedViews(this.mFooterViewInfos);
    onScrollChanged(this.mScrollX, this.mScrollY, i, j);
  }
  
  private View addViewAbove(View paramView, int paramInt) {
    int i = paramInt - 1;
    View view = obtainView(i, this.mIsScrap);
    int j = paramView.getTop();
    paramInt = this.mDividerHeight;
    setupChild(view, i, j - paramInt, false, this.mListPadding.left, false, this.mIsScrap[0]);
    return view;
  }
  
  private View addViewBelow(View paramView, int paramInt) {
    int i = paramInt + 1;
    View view = obtainView(i, this.mIsScrap);
    int j = paramView.getBottom();
    paramInt = this.mDividerHeight;
    setupChild(view, i, j + paramInt, true, this.mListPadding.left, false, this.mIsScrap[0]);
    return view;
  }
  
  public void setItemsCanFocus(boolean paramBoolean) {
    this.mItemsCanFocus = paramBoolean;
    if (!paramBoolean)
      setDescendantFocusability(393216); 
  }
  
  public boolean getItemsCanFocus() {
    return this.mItemsCanFocus;
  }
  
  public boolean isOpaque() {
    boolean bool;
    if ((this.mCachingActive && this.mIsCacheColorOpaque && this.mDividerIsOpaque && 
      hasOpaqueScrollbars()) || super.isOpaque()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      int i;
      if (this.mListPadding != null) {
        i = this.mListPadding.top;
      } else {
        i = this.mPaddingTop;
      } 
      View view = getChildAt(0);
      if (view == null || view.getTop() > i)
        return false; 
      int j = getHeight();
      if (this.mListPadding != null) {
        i = this.mListPadding.bottom;
      } else {
        i = this.mPaddingBottom;
      } 
      view = getChildAt(getChildCount() - 1);
      if (view == null || view.getBottom() < j - i)
        return false; 
    } 
    return bool;
  }
  
  public void setCacheColorHint(int paramInt) {
    boolean bool;
    if (paramInt >>> 24 == 255) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mIsCacheColorOpaque = bool;
    if (bool) {
      if (this.mDividerPaint == null)
        this.mDividerPaint = new Paint(); 
      this.mDividerPaint.setColor(paramInt);
    } 
    super.setCacheColorHint(paramInt);
  }
  
  void drawOverscrollHeader(Canvas paramCanvas, Drawable paramDrawable, Rect paramRect) {
    int i = paramDrawable.getMinimumHeight();
    paramCanvas.save();
    paramCanvas.clipRect(paramRect);
    int j = paramRect.bottom, k = paramRect.top;
    if (j - k < i)
      paramRect.top = paramRect.bottom - i; 
    paramDrawable.setBounds(paramRect);
    paramDrawable.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  void drawOverscrollFooter(Canvas paramCanvas, Drawable paramDrawable, Rect paramRect) {
    int i = paramDrawable.getMinimumHeight();
    paramCanvas.save();
    paramCanvas.clipRect(paramRect);
    int j = paramRect.bottom, k = paramRect.top;
    if (j - k < i)
      paramRect.bottom = paramRect.top + i; 
    paramDrawable.setBounds(paramRect);
    paramDrawable.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mCachingStarted : Z
    //   4: ifeq -> 12
    //   7: aload_0
    //   8: iconst_1
    //   9: putfield mCachingActive : Z
    //   12: aload_0
    //   13: getfield mDividerHeight : I
    //   16: istore_2
    //   17: aload_0
    //   18: getfield mOverScrollHeader : Landroid/graphics/drawable/Drawable;
    //   21: astore_3
    //   22: aload_0
    //   23: getfield mOverScrollFooter : Landroid/graphics/drawable/Drawable;
    //   26: astore #4
    //   28: aload_3
    //   29: ifnull -> 38
    //   32: iconst_1
    //   33: istore #5
    //   35: goto -> 41
    //   38: iconst_0
    //   39: istore #5
    //   41: aload #4
    //   43: ifnull -> 52
    //   46: iconst_1
    //   47: istore #6
    //   49: goto -> 55
    //   52: iconst_0
    //   53: istore #6
    //   55: iload_2
    //   56: ifle -> 72
    //   59: aload_0
    //   60: getfield mDivider : Landroid/graphics/drawable/Drawable;
    //   63: ifnull -> 72
    //   66: iconst_1
    //   67: istore #7
    //   69: goto -> 75
    //   72: iconst_0
    //   73: istore #7
    //   75: iload #7
    //   77: ifne -> 96
    //   80: iload #5
    //   82: ifne -> 96
    //   85: iload #6
    //   87: ifeq -> 93
    //   90: goto -> 96
    //   93: goto -> 1255
    //   96: aload_0
    //   97: getfield mTempRect : Landroid/graphics/Rect;
    //   100: astore #8
    //   102: aload #8
    //   104: aload_0
    //   105: getfield mPaddingLeft : I
    //   108: putfield left : I
    //   111: aload #8
    //   113: aload_0
    //   114: getfield mRight : I
    //   117: aload_0
    //   118: getfield mLeft : I
    //   121: isub
    //   122: aload_0
    //   123: getfield mPaddingRight : I
    //   126: isub
    //   127: putfield right : I
    //   130: aload_0
    //   131: invokevirtual getChildCount : ()I
    //   134: istore #9
    //   136: aload_0
    //   137: invokevirtual getHeaderViewsCount : ()I
    //   140: istore #10
    //   142: aload_0
    //   143: getfield mItemCount : I
    //   146: istore #11
    //   148: iload #11
    //   150: aload_0
    //   151: getfield mFooterViewInfos : Ljava/util/ArrayList;
    //   154: invokevirtual size : ()I
    //   157: isub
    //   158: istore #12
    //   160: aload_0
    //   161: getfield mHeaderDividersEnabled : Z
    //   164: istore #13
    //   166: aload_0
    //   167: getfield mFooterDividersEnabled : Z
    //   170: istore #14
    //   172: aload_0
    //   173: getfield mFirstPosition : I
    //   176: istore #15
    //   178: aload_0
    //   179: getfield mAreAllItemsSelectable : Z
    //   182: istore #16
    //   184: aload_0
    //   185: getfield mAdapter : Landroid/widget/ListAdapter;
    //   188: astore #17
    //   190: aload_0
    //   191: invokevirtual isOpaque : ()Z
    //   194: ifeq -> 210
    //   197: aload_0
    //   198: invokespecial isOpaque : ()Z
    //   201: ifne -> 210
    //   204: iconst_1
    //   205: istore #18
    //   207: goto -> 213
    //   210: iconst_0
    //   211: istore #18
    //   213: iload #18
    //   215: ifeq -> 262
    //   218: aload_0
    //   219: getfield mDividerPaint : Landroid/graphics/Paint;
    //   222: ifnonnull -> 259
    //   225: aload_0
    //   226: getfield mIsCacheColorOpaque : Z
    //   229: ifeq -> 259
    //   232: new android/graphics/Paint
    //   235: dup
    //   236: invokespecial <init> : ()V
    //   239: astore #19
    //   241: aload_0
    //   242: aload #19
    //   244: putfield mDividerPaint : Landroid/graphics/Paint;
    //   247: aload #19
    //   249: aload_0
    //   250: invokevirtual getCacheColorHint : ()I
    //   253: invokevirtual setColor : (I)V
    //   256: goto -> 262
    //   259: goto -> 262
    //   262: aload_0
    //   263: getfield mDividerPaint : Landroid/graphics/Paint;
    //   266: astore #19
    //   268: iconst_0
    //   269: istore #20
    //   271: aload_0
    //   272: getfield mGroupFlags : I
    //   275: istore #21
    //   277: iload #21
    //   279: bipush #34
    //   281: iand
    //   282: bipush #34
    //   284: if_icmpne -> 308
    //   287: aload_0
    //   288: getfield mListPadding : Landroid/graphics/Rect;
    //   291: getfield top : I
    //   294: istore #21
    //   296: aload_0
    //   297: getfield mListPadding : Landroid/graphics/Rect;
    //   300: getfield bottom : I
    //   303: istore #20
    //   305: goto -> 311
    //   308: iconst_0
    //   309: istore #21
    //   311: aload_0
    //   312: getfield mBottom : I
    //   315: istore #22
    //   317: iload #22
    //   319: aload_0
    //   320: getfield mTop : I
    //   323: isub
    //   324: iload #20
    //   326: isub
    //   327: aload_0
    //   328: getfield mScrollY : I
    //   331: iadd
    //   332: istore #23
    //   334: aload_0
    //   335: getfield mStackFromBottom : Z
    //   338: ifne -> 798
    //   341: aload_0
    //   342: getfield mScrollY : I
    //   345: istore #21
    //   347: iload #9
    //   349: ifle -> 415
    //   352: iload #21
    //   354: ifge -> 415
    //   357: iload #5
    //   359: ifeq -> 386
    //   362: aload #8
    //   364: iconst_0
    //   365: putfield bottom : I
    //   368: aload #8
    //   370: iload #21
    //   372: putfield top : I
    //   375: aload_0
    //   376: aload_1
    //   377: aload_3
    //   378: aload #8
    //   380: invokevirtual drawOverscrollHeader : (Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    //   383: goto -> 415
    //   386: iload #7
    //   388: ifeq -> 415
    //   391: aload #8
    //   393: iconst_0
    //   394: putfield bottom : I
    //   397: aload #8
    //   399: iload_2
    //   400: ineg
    //   401: putfield top : I
    //   404: aload_0
    //   405: aload_1
    //   406: aload #8
    //   408: iconst_m1
    //   409: invokevirtual drawDivider : (Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    //   412: goto -> 415
    //   415: iconst_0
    //   416: istore #21
    //   418: iconst_0
    //   419: istore #22
    //   421: iload #5
    //   423: istore #20
    //   425: iload #23
    //   427: istore #5
    //   429: aload_3
    //   430: astore #24
    //   432: iload #21
    //   434: iload #9
    //   436: if_icmpge -> 733
    //   439: iload #15
    //   441: iload #21
    //   443: iadd
    //   444: istore #25
    //   446: iload #25
    //   448: iload #10
    //   450: if_icmpge -> 459
    //   453: iconst_1
    //   454: istore #26
    //   456: goto -> 462
    //   459: iconst_0
    //   460: istore #26
    //   462: iload #25
    //   464: iload #12
    //   466: if_icmplt -> 475
    //   469: iconst_1
    //   470: istore #27
    //   472: goto -> 478
    //   475: iconst_0
    //   476: istore #27
    //   478: iload #13
    //   480: ifne -> 488
    //   483: iload #26
    //   485: ifne -> 501
    //   488: iload #14
    //   490: ifne -> 504
    //   493: iload #27
    //   495: ifne -> 501
    //   498: goto -> 504
    //   501: goto -> 727
    //   504: aload_0
    //   505: iload #21
    //   507: invokevirtual getChildAt : (I)Landroid/view/View;
    //   510: astore_3
    //   511: aload_3
    //   512: invokevirtual getBottom : ()I
    //   515: istore #23
    //   517: iload #21
    //   519: iload #9
    //   521: iconst_1
    //   522: isub
    //   523: if_icmpne -> 532
    //   526: iconst_1
    //   527: istore #22
    //   529: goto -> 535
    //   532: iconst_0
    //   533: istore #22
    //   535: iload #7
    //   537: ifeq -> 723
    //   540: iload #23
    //   542: iload #5
    //   544: if_icmpge -> 723
    //   547: iload #6
    //   549: ifeq -> 567
    //   552: iload #22
    //   554: ifne -> 560
    //   557: goto -> 567
    //   560: iload #23
    //   562: istore #22
    //   564: goto -> 727
    //   567: iload #25
    //   569: iconst_1
    //   570: iadd
    //   571: istore #28
    //   573: aload #17
    //   575: iload #25
    //   577: invokeinterface isEnabled : (I)Z
    //   582: ifeq -> 680
    //   585: iload #13
    //   587: ifne -> 608
    //   590: iload #26
    //   592: ifne -> 605
    //   595: iload #28
    //   597: iload #10
    //   599: if_icmplt -> 605
    //   602: goto -> 608
    //   605: goto -> 680
    //   608: iload #22
    //   610: ifne -> 648
    //   613: aload #17
    //   615: iload #28
    //   617: invokeinterface isEnabled : (I)Z
    //   622: ifeq -> 645
    //   625: iload #14
    //   627: ifne -> 642
    //   630: iload #27
    //   632: ifne -> 645
    //   635: iload #28
    //   637: iload #12
    //   639: if_icmpge -> 645
    //   642: goto -> 648
    //   645: goto -> 680
    //   648: aload #8
    //   650: iload #23
    //   652: putfield top : I
    //   655: aload #8
    //   657: iload #23
    //   659: iload_2
    //   660: iadd
    //   661: putfield bottom : I
    //   664: aload_0
    //   665: aload_1
    //   666: aload #8
    //   668: iload #21
    //   670: invokevirtual drawDivider : (Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    //   673: iload #23
    //   675: istore #22
    //   677: goto -> 727
    //   680: iload #18
    //   682: ifeq -> 716
    //   685: aload #8
    //   687: iload #23
    //   689: putfield top : I
    //   692: aload #8
    //   694: iload #23
    //   696: iload_2
    //   697: iadd
    //   698: putfield bottom : I
    //   701: aload_1
    //   702: aload #8
    //   704: aload #19
    //   706: invokevirtual drawRect : (Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    //   709: iload #23
    //   711: istore #22
    //   713: goto -> 727
    //   716: iload #23
    //   718: istore #22
    //   720: goto -> 727
    //   723: iload #23
    //   725: istore #22
    //   727: iinc #21, 1
    //   730: goto -> 432
    //   733: aload_0
    //   734: getfield mBottom : I
    //   737: aload_0
    //   738: getfield mScrollY : I
    //   741: iadd
    //   742: istore #5
    //   744: iload #6
    //   746: ifeq -> 795
    //   749: iload #15
    //   751: iload #9
    //   753: iadd
    //   754: iload #11
    //   756: if_icmpne -> 792
    //   759: iload #5
    //   761: iload #22
    //   763: if_icmple -> 792
    //   766: aload #8
    //   768: iload #22
    //   770: putfield top : I
    //   773: aload #8
    //   775: iload #5
    //   777: putfield bottom : I
    //   780: aload_0
    //   781: aload_1
    //   782: aload #4
    //   784: aload #8
    //   786: invokevirtual drawOverscrollFooter : (Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    //   789: goto -> 795
    //   792: goto -> 795
    //   795: goto -> 1255
    //   798: aload #4
    //   800: astore #24
    //   802: iload #11
    //   804: istore #20
    //   806: aload_0
    //   807: getfield mScrollY : I
    //   810: istore #25
    //   812: iload #9
    //   814: ifle -> 853
    //   817: iload #5
    //   819: ifeq -> 853
    //   822: aload #8
    //   824: iload #25
    //   826: putfield top : I
    //   829: aload #8
    //   831: aload_0
    //   832: iconst_0
    //   833: invokevirtual getChildAt : (I)Landroid/view/View;
    //   836: invokevirtual getTop : ()I
    //   839: putfield bottom : I
    //   842: aload_0
    //   843: aload_1
    //   844: aload_3
    //   845: aload #8
    //   847: invokevirtual drawOverscrollHeader : (Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    //   850: goto -> 853
    //   853: iload #5
    //   855: ifeq -> 864
    //   858: iconst_1
    //   859: istore #5
    //   861: goto -> 867
    //   864: iconst_0
    //   865: istore #5
    //   867: iload #5
    //   869: istore #26
    //   871: iload #5
    //   873: istore #22
    //   875: iload #21
    //   877: istore #5
    //   879: iload #26
    //   881: istore #21
    //   883: iload #15
    //   885: istore #26
    //   887: iload #22
    //   889: iload #9
    //   891: if_icmpge -> 1170
    //   894: iload #26
    //   896: iload #22
    //   898: iadd
    //   899: istore #28
    //   901: iload #28
    //   903: iload #10
    //   905: if_icmpge -> 914
    //   908: iconst_1
    //   909: istore #27
    //   911: goto -> 917
    //   914: iconst_0
    //   915: istore #27
    //   917: iload #28
    //   919: iload #12
    //   921: if_icmplt -> 930
    //   924: iconst_1
    //   925: istore #11
    //   927: goto -> 933
    //   930: iconst_0
    //   931: istore #11
    //   933: iload #13
    //   935: ifne -> 949
    //   938: iload #27
    //   940: ifne -> 946
    //   943: goto -> 949
    //   946: goto -> 1164
    //   949: iload #14
    //   951: ifne -> 959
    //   954: iload #11
    //   956: ifne -> 946
    //   959: aload_0
    //   960: iload #22
    //   962: invokevirtual getChildAt : (I)Landroid/view/View;
    //   965: astore #4
    //   967: aload #4
    //   969: invokevirtual getTop : ()I
    //   972: istore #29
    //   974: iload #7
    //   976: ifeq -> 1164
    //   979: iload #29
    //   981: iload #5
    //   983: if_icmple -> 1161
    //   986: iload #5
    //   988: istore #15
    //   990: iload #22
    //   992: iload #21
    //   994: if_icmpne -> 1003
    //   997: iconst_1
    //   998: istore #5
    //   1000: goto -> 1006
    //   1003: iconst_0
    //   1004: istore #5
    //   1006: iload #28
    //   1008: iconst_1
    //   1009: isub
    //   1010: istore #30
    //   1012: aload #17
    //   1014: iload #28
    //   1016: invokeinterface isEnabled : (I)Z
    //   1021: ifeq -> 1121
    //   1024: iload #13
    //   1026: ifne -> 1047
    //   1029: iload #27
    //   1031: ifne -> 1044
    //   1034: iload #30
    //   1036: iload #10
    //   1038: if_icmplt -> 1044
    //   1041: goto -> 1047
    //   1044: goto -> 1121
    //   1047: iload #5
    //   1049: ifne -> 1087
    //   1052: aload #17
    //   1054: iload #30
    //   1056: invokeinterface isEnabled : (I)Z
    //   1061: ifeq -> 1084
    //   1064: iload #14
    //   1066: ifne -> 1081
    //   1069: iload #11
    //   1071: ifne -> 1084
    //   1074: iload #30
    //   1076: iload #12
    //   1078: if_icmpge -> 1084
    //   1081: goto -> 1087
    //   1084: goto -> 1121
    //   1087: aload #8
    //   1089: iload #29
    //   1091: iload_2
    //   1092: isub
    //   1093: putfield top : I
    //   1096: aload #8
    //   1098: iload #29
    //   1100: putfield bottom : I
    //   1103: aload_0
    //   1104: aload_1
    //   1105: aload #8
    //   1107: iload #22
    //   1109: iconst_1
    //   1110: isub
    //   1111: invokevirtual drawDivider : (Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    //   1114: iload #15
    //   1116: istore #5
    //   1118: goto -> 1164
    //   1121: iload #15
    //   1123: istore #5
    //   1125: iload #18
    //   1127: ifeq -> 1164
    //   1130: aload #8
    //   1132: iload #29
    //   1134: iload_2
    //   1135: isub
    //   1136: putfield top : I
    //   1139: aload #8
    //   1141: iload #29
    //   1143: putfield bottom : I
    //   1146: aload_1
    //   1147: aload #8
    //   1149: aload #19
    //   1151: invokevirtual drawRect : (Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    //   1154: iload #15
    //   1156: istore #5
    //   1158: goto -> 1164
    //   1161: goto -> 1164
    //   1164: iinc #22, 1
    //   1167: goto -> 887
    //   1170: iload #9
    //   1172: ifle -> 1255
    //   1175: iload #25
    //   1177: ifle -> 1255
    //   1180: iload #6
    //   1182: ifeq -> 1220
    //   1185: aload_0
    //   1186: getfield mBottom : I
    //   1189: istore #5
    //   1191: aload #8
    //   1193: iload #5
    //   1195: putfield top : I
    //   1198: aload #8
    //   1200: iload #5
    //   1202: iload #25
    //   1204: iadd
    //   1205: putfield bottom : I
    //   1208: aload_0
    //   1209: aload_1
    //   1210: aload #24
    //   1212: aload #8
    //   1214: invokevirtual drawOverscrollFooter : (Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    //   1217: goto -> 1255
    //   1220: iload #7
    //   1222: ifeq -> 1252
    //   1225: aload #8
    //   1227: iload #23
    //   1229: putfield top : I
    //   1232: aload #8
    //   1234: iload #23
    //   1236: iload_2
    //   1237: iadd
    //   1238: putfield bottom : I
    //   1241: aload_0
    //   1242: aload_1
    //   1243: aload #8
    //   1245: iconst_m1
    //   1246: invokevirtual drawDivider : (Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    //   1249: goto -> 1255
    //   1252: goto -> 1255
    //   1255: aload_0
    //   1256: aload_1
    //   1257: invokespecial dispatchDraw : (Landroid/graphics/Canvas;)V
    //   1260: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3468	-> 0
    //   #3469	-> 7
    //   #3473	-> 12
    //   #3474	-> 17
    //   #3475	-> 22
    //   #3476	-> 28
    //   #3477	-> 41
    //   #3478	-> 55
    //   #3480	-> 75
    //   #3482	-> 96
    //   #3483	-> 102
    //   #3484	-> 111
    //   #3486	-> 130
    //   #3487	-> 136
    //   #3488	-> 142
    //   #3489	-> 148
    //   #3490	-> 160
    //   #3491	-> 166
    //   #3492	-> 172
    //   #3493	-> 178
    //   #3494	-> 184
    //   #3499	-> 190
    //   #3501	-> 213
    //   #3502	-> 232
    //   #3503	-> 247
    //   #3501	-> 259
    //   #3505	-> 262
    //   #3507	-> 268
    //   #3508	-> 268
    //   #3509	-> 271
    //   #3510	-> 287
    //   #3511	-> 296
    //   #3509	-> 308
    //   #3514	-> 311
    //   #3515	-> 334
    //   #3516	-> 341
    //   #3519	-> 341
    //   #3520	-> 347
    //   #3521	-> 357
    //   #3522	-> 362
    //   #3523	-> 368
    //   #3524	-> 375
    //   #3525	-> 386
    //   #3526	-> 391
    //   #3527	-> 397
    //   #3528	-> 404
    //   #3520	-> 415
    //   #3532	-> 415
    //   #3533	-> 439
    //   #3534	-> 446
    //   #3535	-> 462
    //   #3536	-> 478
    //   #3532	-> 501
    //   #3537	-> 504
    //   #3538	-> 511
    //   #3539	-> 517
    //   #3541	-> 535
    //   #3543	-> 567
    //   #3547	-> 573
    //   #3549	-> 613
    //   #3551	-> 648
    //   #3552	-> 655
    //   #3553	-> 664
    //   #3547	-> 680
    //   #3554	-> 680
    //   #3555	-> 685
    //   #3556	-> 692
    //   #3557	-> 701
    //   #3554	-> 716
    //   #3541	-> 723
    //   #3532	-> 727
    //   #3563	-> 733
    //   #3564	-> 744
    //   #3566	-> 766
    //   #3567	-> 773
    //   #3568	-> 780
    //   #3564	-> 792
    //   #3570	-> 795
    //   #3573	-> 798
    //   #3575	-> 812
    //   #3576	-> 822
    //   #3577	-> 829
    //   #3578	-> 842
    //   #3575	-> 853
    //   #3581	-> 853
    //   #3582	-> 871
    //   #3583	-> 894
    //   #3584	-> 901
    //   #3585	-> 917
    //   #3586	-> 933
    //   #3587	-> 959
    //   #3588	-> 967
    //   #3589	-> 974
    //   #3590	-> 986
    //   #3591	-> 1006
    //   #3595	-> 1012
    //   #3597	-> 1052
    //   #3599	-> 1087
    //   #3600	-> 1096
    //   #3605	-> 1103
    //   #3595	-> 1121
    //   #3606	-> 1121
    //   #3607	-> 1130
    //   #3608	-> 1139
    //   #3609	-> 1146
    //   #3589	-> 1161
    //   #3582	-> 1164
    //   #3615	-> 1170
    //   #3616	-> 1180
    //   #3617	-> 1185
    //   #3618	-> 1191
    //   #3619	-> 1198
    //   #3620	-> 1208
    //   #3621	-> 1217
    //   #3622	-> 1225
    //   #3623	-> 1232
    //   #3624	-> 1241
    //   #3621	-> 1252
    //   #3615	-> 1255
    //   #3631	-> 1255
    //   #3632	-> 1260
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong) {
    boolean bool = super.drawChild(paramCanvas, paramView, paramLong);
    if (this.mCachingActive && paramView.mCachingFailed)
      this.mCachingActive = false; 
    return bool;
  }
  
  void drawDivider(Canvas paramCanvas, Rect paramRect, int paramInt) {
    Drawable drawable = this.mDivider;
    drawable.setBounds(paramRect);
    drawable.draw(paramCanvas);
  }
  
  public Drawable getDivider() {
    return this.mDivider;
  }
  
  public void setDivider(Drawable paramDrawable) {
    boolean bool = false;
    if (paramDrawable != null) {
      this.mDividerHeight = paramDrawable.getIntrinsicHeight();
    } else {
      this.mDividerHeight = 0;
    } 
    this.mDivider = paramDrawable;
    if (paramDrawable == null || paramDrawable.getOpacity() == -1)
      bool = true; 
    this.mDividerIsOpaque = bool;
    requestLayout();
    invalidate();
  }
  
  public int getDividerHeight() {
    return this.mDividerHeight;
  }
  
  public void setDividerHeight(int paramInt) {
    this.mDividerHeight = paramInt;
    requestLayout();
    invalidate();
  }
  
  public void setHeaderDividersEnabled(boolean paramBoolean) {
    this.mHeaderDividersEnabled = paramBoolean;
    invalidate();
  }
  
  public boolean areHeaderDividersEnabled() {
    return this.mHeaderDividersEnabled;
  }
  
  public void setFooterDividersEnabled(boolean paramBoolean) {
    this.mFooterDividersEnabled = paramBoolean;
    invalidate();
  }
  
  public boolean areFooterDividersEnabled() {
    return this.mFooterDividersEnabled;
  }
  
  public void setOverscrollHeader(Drawable paramDrawable) {
    this.mOverScrollHeader = paramDrawable;
    if (this.mScrollY < 0)
      invalidate(); 
  }
  
  public Drawable getOverscrollHeader() {
    return this.mOverScrollHeader;
  }
  
  public void setOverscrollFooter(Drawable paramDrawable) {
    this.mOverScrollFooter = paramDrawable;
    invalidate();
  }
  
  public Drawable getOverscrollFooter() {
    return this.mOverScrollFooter;
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    ListAdapter listAdapter = this.mAdapter;
    byte b = -1;
    byte b1 = 0;
    int i = 0;
    int j = b, k = b1;
    if (listAdapter != null) {
      j = b;
      k = b1;
      if (paramBoolean) {
        j = b;
        k = b1;
        if (paramRect != null) {
          paramRect.offset(this.mScrollX, this.mScrollY);
          if (listAdapter.getCount() < getChildCount() + this.mFirstPosition) {
            this.mLayoutMode = 0;
            layoutChildren();
          } 
          Rect rect = this.mTempRect;
          int m = Integer.MAX_VALUE;
          int n = getChildCount();
          int i1 = this.mFirstPosition;
          b1 = 0;
          while (true) {
            j = b;
            k = i;
            if (b1 < n) {
              if (!listAdapter.isEnabled(i1 + b1)) {
                k = m;
              } else {
                View view = getChildAt(b1);
                view.getDrawingRect(rect);
                offsetDescendantRectToMyCoords(view, rect);
                j = getDistance(paramRect, rect, paramInt);
                k = m;
                if (j < m) {
                  k = j;
                  b = b1;
                  i = view.getTop();
                } 
              } 
              b1++;
              m = k;
              continue;
            } 
            break;
          } 
        } 
      } 
    } 
    if (j >= 0) {
      setSelectionFromTop(this.mFirstPosition + j, k);
    } else {
      requestLayout();
    } 
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    int i = getChildCount();
    if (i > 0) {
      for (byte b = 0; b < i; b++)
        addHeaderView(getChildAt(b)); 
      removeAllViews();
    } 
  }
  
  protected <T extends View> T findViewTraversal(int paramInt) {
    View view;
    ListAdapter listAdapter1 = super.findViewTraversal(paramInt);
    ListAdapter listAdapter2 = listAdapter1;
    if (listAdapter1 == null) {
      view = findViewInHeadersOrFooters(this.mHeaderViewInfos, paramInt);
      if (view != null)
        return (T)view; 
      View view1 = findViewInHeadersOrFooters(this.mFooterViewInfos, paramInt);
      view = view1;
      if (view1 != null)
        return (T)view1; 
    } 
    return (T)view;
  }
  
  View findViewInHeadersOrFooters(ArrayList<FixedViewInfo> paramArrayList, int paramInt) {
    if (paramArrayList != null) {
      int i = paramArrayList.size();
      for (byte b = 0; b < i; b++) {
        View view = ((FixedViewInfo)paramArrayList.get(b)).view;
        if (!view.isRootNamespace()) {
          view = view.findViewById(paramInt);
          if (view != null)
            return view; 
        } 
      } 
    } 
    return null;
  }
  
  protected <T extends View> T findViewWithTagTraversal(Object paramObject) {
    ListAdapter listAdapter = super.findViewWithTagTraversal(paramObject);
    Object object = listAdapter;
    if (listAdapter == null) {
      View view = findViewWithTagInHeadersOrFooters(this.mHeaderViewInfos, paramObject);
      if (view != null)
        return (T)view; 
      paramObject = findViewWithTagInHeadersOrFooters(this.mFooterViewInfos, paramObject);
      object = paramObject;
      if (paramObject != null)
        return (T)paramObject; 
    } 
    return (T)object;
  }
  
  View findViewWithTagInHeadersOrFooters(ArrayList<FixedViewInfo> paramArrayList, Object paramObject) {
    if (paramArrayList != null) {
      int i = paramArrayList.size();
      for (byte b = 0; b < i; b++) {
        View view = ((FixedViewInfo)paramArrayList.get(b)).view;
        if (!view.isRootNamespace()) {
          view = view.findViewWithTag(paramObject);
          if (view != null)
            return view; 
        } 
      } 
    } 
    return null;
  }
  
  protected <T extends View> T findViewByPredicateTraversal(Predicate<View> paramPredicate, View paramView) {
    View view;
    ListAdapter listAdapter1 = super.findViewByPredicateTraversal(paramPredicate, paramView);
    ListAdapter listAdapter2 = listAdapter1;
    if (listAdapter1 == null) {
      view = findViewByPredicateInHeadersOrFooters(this.mHeaderViewInfos, paramPredicate, paramView);
      if (view != null)
        return (T)view; 
      View view1 = findViewByPredicateInHeadersOrFooters(this.mFooterViewInfos, paramPredicate, paramView);
      view = view1;
      if (view1 != null)
        return (T)view1; 
    } 
    return (T)view;
  }
  
  View findViewByPredicateInHeadersOrFooters(ArrayList<FixedViewInfo> paramArrayList, Predicate<View> paramPredicate, View paramView) {
    if (paramArrayList != null) {
      int i = paramArrayList.size();
      for (byte b = 0; b < i; b++) {
        View view = ((FixedViewInfo)paramArrayList.get(b)).view;
        if (view != paramView && !view.isRootNamespace()) {
          view = view.findViewByPredicate(paramPredicate);
          if (view != null)
            return view; 
        } 
      } 
    } 
    return null;
  }
  
  @Deprecated
  public long[] getCheckItemIds() {
    if (this.mAdapter != null && this.mAdapter.hasStableIds())
      return getCheckedItemIds(); 
    if (this.mChoiceMode != 0 && this.mCheckStates != null && this.mAdapter != null) {
      SparseBooleanArray sparseBooleanArray = this.mCheckStates;
      int i = sparseBooleanArray.size();
      long[] arrayOfLong2 = new long[i];
      ListAdapter listAdapter = this.mAdapter;
      int j = 0;
      for (byte b = 0; b < i; b++, j = k) {
        int k = j;
        if (sparseBooleanArray.valueAt(b)) {
          arrayOfLong2[j] = listAdapter.getItemId(sparseBooleanArray.keyAt(b));
          k = j + 1;
        } 
      } 
      if (j == i)
        return arrayOfLong2; 
      long[] arrayOfLong1 = new long[j];
      System.arraycopy(arrayOfLong2, 0, arrayOfLong1, 0, j);
      return arrayOfLong1;
    } 
    return new long[0];
  }
  
  int getHeightForPosition(int paramInt) {
    int i = super.getHeightForPosition(paramInt);
    if (shouldAdjustHeightForDivider(paramInt))
      return this.mDividerHeight + i; 
    return i;
  }
  
  private boolean shouldAdjustHeightForDivider(int paramInt) {
    int j, i = this.mDividerHeight;
    Drawable drawable1 = this.mOverScrollHeader;
    Drawable drawable2 = this.mOverScrollFooter;
    if (drawable1 != null) {
      boolean bool = true;
    } else {
      boolean bool = false;
    } 
    if (drawable2 != null) {
      j = 1;
    } else {
      j = 0;
    } 
    if (i > 0 && this.mDivider != null) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i != 0) {
      boolean bool1, bool2;
      if (isOpaque() && !super.isOpaque()) {
        i = 1;
      } else {
        i = 0;
      } 
      int k = this.mItemCount;
      int m = getHeaderViewsCount();
      int n = k - this.mFooterViewInfos.size();
      if (paramInt < m) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (paramInt >= n) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      boolean bool3 = this.mHeaderDividersEnabled;
      boolean bool4 = this.mFooterDividersEnabled;
      if ((bool3 || !bool1) && (bool4 || !bool2)) {
        int i1;
        ListAdapter listAdapter = this.mAdapter;
        if (!this.mStackFromBottom) {
          if (paramInt == k - 1) {
            i1 = 1;
          } else {
            i1 = 0;
          } 
          if (!j || !i1) {
            j = paramInt + 1;
            if (listAdapter.isEnabled(paramInt) && (bool3 || (!bool1 && j >= m)))
              if (!i1) {
                if (listAdapter.isEnabled(j) && (bool4 || (!bool2 && j < n)))
                  return true; 
              } else {
                return true;
              }  
            if (i != 0)
              return true; 
          } 
        } else {
          if (i1) {
            j = 1;
          } else {
            j = 0;
          } 
          if (paramInt == j) {
            j = 1;
          } else {
            j = 0;
          } 
          if (j == 0) {
            i1 = paramInt - 1;
            if (listAdapter.isEnabled(paramInt) && (bool3 || (!bool1 && i1 >= m)))
              if (j == 0) {
                if (listAdapter.isEnabled(i1) && (bool4 || (!bool2 && i1 < n)))
                  return true; 
              } else {
                return true;
              }  
            if (i != 0)
              return true; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public CharSequence getAccessibilityClassName() {
    return ListView.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    int i = getCount();
    int j = getSelectionModeForAccessibility();
    AccessibilityNodeInfo.CollectionInfo collectionInfo = AccessibilityNodeInfo.CollectionInfo.obtain(-1, -1, false, j);
    paramAccessibilityNodeInfo.setCollectionInfo(collectionInfo);
    if (i > 0)
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION); 
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    if (paramInt == 16908343) {
      paramInt = paramBundle.getInt("android.view.accessibility.action.ARGUMENT_ROW_INT", -1);
      int i = Math.min(paramInt, getCount() - 1);
      if (paramInt >= 0) {
        smoothScrollToPosition(i);
        return true;
      } 
    } 
    return false;
  }
  
  public void onInitializeAccessibilityNodeInfoForItem(View paramView, int paramInt, AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    boolean bool;
    super.onInitializeAccessibilityNodeInfoForItem(paramView, paramInt, paramAccessibilityNodeInfo);
    AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams)paramView.getLayoutParams();
    if (layoutParams != null && layoutParams.viewType == -2) {
      bool = true;
    } else {
      bool = false;
    } 
    boolean bool1 = isItemChecked(paramInt);
    AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo = AccessibilityNodeInfo.CollectionItemInfo.obtain(paramInt, 1, 0, 1, bool, bool1);
    paramAccessibilityNodeInfo.setCollectionItemInfo(collectionItemInfo);
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("recycleOnMeasure", recycleOnMeasure());
  }
  
  protected HeaderViewListAdapter wrapHeaderListAdapterInternal(ArrayList<FixedViewInfo> paramArrayList1, ArrayList<FixedViewInfo> paramArrayList2, ListAdapter paramListAdapter) {
    return new HeaderViewListAdapter(paramArrayList1, paramArrayList2, paramListAdapter);
  }
  
  protected void wrapHeaderListAdapterInternal() {
    this.mAdapter = wrapHeaderListAdapterInternal(this.mHeaderViewInfos, this.mFooterViewInfos, this.mAdapter);
  }
  
  protected void dispatchDataSetObserverOnChangedInternal() {
    if (this.mDataSetObserver != null)
      this.mDataSetObserver.onChanged(); 
  }
}
