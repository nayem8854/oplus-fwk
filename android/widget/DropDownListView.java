package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.android.internal.widget.AutoScrollHelper;

public class DropDownListView extends ListView {
  private boolean mDrawsInPressedState;
  
  private boolean mHijackFocus;
  
  private boolean mListSelectionHidden;
  
  private ResolveHoverRunnable mResolveHoverRunnable;
  
  private AutoScrollHelper.AbsListViewAutoScroller mScrollHelper;
  
  public DropDownListView(Context paramContext, boolean paramBoolean) {
    this(paramContext, paramBoolean, 16842861);
  }
  
  public DropDownListView(Context paramContext, boolean paramBoolean, int paramInt) {
    super(paramContext, (AttributeSet)null, paramInt);
    this.mHijackFocus = paramBoolean;
    setCacheColorHint(0);
  }
  
  boolean shouldShowSelector() {
    return (isHovered() || super.shouldShowSelector());
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    ResolveHoverRunnable resolveHoverRunnable = this.mResolveHoverRunnable;
    if (resolveHoverRunnable != null)
      resolveHoverRunnable.cancel(); 
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public boolean onHoverEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    if (i == 10 && this.mResolveHoverRunnable == null) {
      ResolveHoverRunnable resolveHoverRunnable = new ResolveHoverRunnable();
      resolveHoverRunnable.post();
    } 
    boolean bool = super.onHoverEvent(paramMotionEvent);
    if (i == 9 || i == 7) {
      i = pointToPosition((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
      if (i != -1 && i != this.mSelectedPosition) {
        View view = getChildAt(i - getFirstVisiblePosition());
        if (view.isEnabled()) {
          requestFocus();
          positionSelector(i, view);
          setSelectedPositionInt(i);
          setNextSelectedPositionInt(i);
        } 
        updateSelectorState();
      } 
      return bool;
    } 
    if (!super.shouldShowSelector()) {
      setSelectedPositionInt(-1);
      setNextSelectedPositionInt(-1);
    } 
    return bool;
  }
  
  protected void drawableStateChanged() {
    if (this.mResolveHoverRunnable == null)
      super.drawableStateChanged(); 
  }
  
  public boolean onForwardedEvent(MotionEvent paramMotionEvent, int paramInt) {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: iconst_1
    //   3: istore #4
    //   5: iconst_0
    //   6: istore #5
    //   8: aload_1
    //   9: invokevirtual getActionMasked : ()I
    //   12: istore #6
    //   14: iload #6
    //   16: iconst_1
    //   17: if_icmpeq -> 50
    //   20: iload #6
    //   22: iconst_2
    //   23: if_icmpeq -> 53
    //   26: iload #6
    //   28: iconst_3
    //   29: if_icmpeq -> 41
    //   32: iload_3
    //   33: istore #4
    //   35: iload #5
    //   37: istore_2
    //   38: goto -> 176
    //   41: iconst_0
    //   42: istore #4
    //   44: iload #5
    //   46: istore_2
    //   47: goto -> 176
    //   50: iconst_0
    //   51: istore #4
    //   53: aload_1
    //   54: iload_2
    //   55: invokevirtual findPointerIndex : (I)I
    //   58: istore #7
    //   60: iload #7
    //   62: ifge -> 74
    //   65: iconst_0
    //   66: istore #4
    //   68: iload #5
    //   70: istore_2
    //   71: goto -> 176
    //   74: aload_1
    //   75: iload #7
    //   77: invokevirtual getX : (I)F
    //   80: f2i
    //   81: istore_2
    //   82: aload_1
    //   83: iload #7
    //   85: invokevirtual getY : (I)F
    //   88: f2i
    //   89: istore #8
    //   91: aload_0
    //   92: iload_2
    //   93: iload #8
    //   95: invokevirtual pointToPosition : (II)I
    //   98: istore #7
    //   100: iload #7
    //   102: iconst_m1
    //   103: if_icmpne -> 111
    //   106: iconst_1
    //   107: istore_2
    //   108: goto -> 176
    //   111: aload_0
    //   112: iload #7
    //   114: aload_0
    //   115: invokevirtual getFirstVisiblePosition : ()I
    //   118: isub
    //   119: invokevirtual getChildAt : (I)Landroid/view/View;
    //   122: astore #9
    //   124: aload_0
    //   125: aload #9
    //   127: iload #7
    //   129: iload_2
    //   130: i2f
    //   131: iload #8
    //   133: i2f
    //   134: invokespecial setPressedItem : (Landroid/view/View;IFF)V
    //   137: iconst_1
    //   138: istore_3
    //   139: iload_3
    //   140: istore #4
    //   142: iload #5
    //   144: istore_2
    //   145: iload #6
    //   147: iconst_1
    //   148: if_icmpne -> 176
    //   151: aload_0
    //   152: iload #7
    //   154: invokevirtual getItemIdAtPosition : (I)J
    //   157: lstore #10
    //   159: aload_0
    //   160: aload #9
    //   162: iload #7
    //   164: lload #10
    //   166: invokevirtual performItemClick : (Landroid/view/View;IJ)Z
    //   169: pop
    //   170: iload #5
    //   172: istore_2
    //   173: iload_3
    //   174: istore #4
    //   176: iload #4
    //   178: ifeq -> 185
    //   181: iload_2
    //   182: ifeq -> 189
    //   185: aload_0
    //   186: invokespecial clearPressedItem : ()V
    //   189: iload #4
    //   191: ifeq -> 235
    //   194: aload_0
    //   195: getfield mScrollHelper : Lcom/android/internal/widget/AutoScrollHelper$AbsListViewAutoScroller;
    //   198: ifnonnull -> 213
    //   201: aload_0
    //   202: new com/android/internal/widget/AutoScrollHelper$AbsListViewAutoScroller
    //   205: dup
    //   206: aload_0
    //   207: invokespecial <init> : (Landroid/widget/AbsListView;)V
    //   210: putfield mScrollHelper : Lcom/android/internal/widget/AutoScrollHelper$AbsListViewAutoScroller;
    //   213: aload_0
    //   214: getfield mScrollHelper : Lcom/android/internal/widget/AutoScrollHelper$AbsListViewAutoScroller;
    //   217: iconst_1
    //   218: invokevirtual setEnabled : (Z)Lcom/android/internal/widget/AutoScrollHelper;
    //   221: pop
    //   222: aload_0
    //   223: getfield mScrollHelper : Lcom/android/internal/widget/AutoScrollHelper$AbsListViewAutoScroller;
    //   226: aload_0
    //   227: aload_1
    //   228: invokevirtual onTouch : (Landroid/view/View;Landroid/view/MotionEvent;)Z
    //   231: pop
    //   232: goto -> 250
    //   235: aload_0
    //   236: getfield mScrollHelper : Lcom/android/internal/widget/AutoScrollHelper$AbsListViewAutoScroller;
    //   239: astore_1
    //   240: aload_1
    //   241: ifnull -> 250
    //   244: aload_1
    //   245: iconst_0
    //   246: invokevirtual setEnabled : (Z)Lcom/android/internal/widget/AutoScrollHelper;
    //   249: pop
    //   250: iload #4
    //   252: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #174	-> 0
    //   #175	-> 5
    //   #177	-> 8
    //   #178	-> 14
    //   #180	-> 41
    //   #181	-> 44
    //   #183	-> 50
    //   #186	-> 53
    //   #187	-> 60
    //   #188	-> 65
    //   #189	-> 68
    //   #192	-> 74
    //   #193	-> 82
    //   #194	-> 91
    //   #195	-> 100
    //   #196	-> 106
    //   #197	-> 108
    //   #200	-> 111
    //   #201	-> 124
    //   #202	-> 137
    //   #204	-> 139
    //   #205	-> 151
    //   #206	-> 159
    //   #212	-> 176
    //   #213	-> 185
    //   #217	-> 189
    //   #218	-> 194
    //   #219	-> 201
    //   #221	-> 213
    //   #222	-> 222
    //   #223	-> 235
    //   #224	-> 244
    //   #227	-> 250
  }
  
  public void setListSelectionHidden(boolean paramBoolean) {
    this.mListSelectionHidden = paramBoolean;
  }
  
  private void clearPressedItem() {
    this.mDrawsInPressedState = false;
    setPressed(false);
    updateSelectorState();
    View view = getChildAt(this.mMotionPosition - this.mFirstPosition);
    if (view != null)
      view.setPressed(false); 
  }
  
  private void setPressedItem(View paramView, int paramInt, float paramFloat1, float paramFloat2) {
    this.mDrawsInPressedState = true;
    drawableHotspotChanged(paramFloat1, paramFloat2);
    if (!isPressed())
      setPressed(true); 
    if (this.mDataChanged)
      layoutChildren(); 
    View view = getChildAt(this.mMotionPosition - this.mFirstPosition);
    if (view != null && view != paramView && view.isPressed())
      view.setPressed(false); 
    this.mMotionPosition = paramInt;
    float f1 = paramView.getLeft();
    float f2 = paramView.getTop();
    paramView.drawableHotspotChanged(paramFloat1 - f1, paramFloat2 - f2);
    if (!paramView.isPressed())
      paramView.setPressed(true); 
    setSelectedPositionInt(paramInt);
    positionSelectorLikeTouch(paramInt, paramView, paramFloat1, paramFloat2);
    refreshDrawableState();
  }
  
  boolean touchModeDrawsInPressedState() {
    return (this.mDrawsInPressedState || super.touchModeDrawsInPressedState());
  }
  
  View obtainView(int paramInt, boolean[] paramArrayOfboolean) {
    View view = super.obtainView(paramInt, paramArrayOfboolean);
    if (view instanceof TextView)
      ((TextView)view).setHorizontallyScrolling(true); 
    return view;
  }
  
  public boolean isInTouchMode() {
    boolean bool;
    if ((this.mHijackFocus && this.mListSelectionHidden) || super.isInTouchMode()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasWindowFocus() {
    return (this.mHijackFocus || super.hasWindowFocus());
  }
  
  public boolean isFocused() {
    return (this.mHijackFocus || super.isFocused());
  }
  
  public boolean hasFocus() {
    return (this.mHijackFocus || super.hasFocus());
  }
  
  class ResolveHoverRunnable implements Runnable {
    final DropDownListView this$0;
    
    private ResolveHoverRunnable() {}
    
    public void run() {
      DropDownListView.access$102(DropDownListView.this, (ResolveHoverRunnable)null);
      DropDownListView.this.drawableStateChanged();
    }
    
    public void cancel() {
      DropDownListView.access$102(DropDownListView.this, (ResolveHoverRunnable)null);
      DropDownListView.this.removeCallbacks(this);
    }
    
    public void post() {
      DropDownListView.this.post(this);
    }
  }
}
