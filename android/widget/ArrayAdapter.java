package android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ArrayAdapter<T> extends BaseAdapter implements Filterable, ThemedSpinnerAdapter {
  private final Object mLock = new Object();
  
  private int mFieldId = 0;
  
  private boolean mNotifyOnChange = true;
  
  private final Context mContext;
  
  private LayoutInflater mDropDownInflater;
  
  private int mDropDownResource;
  
  private ArrayFilter mFilter;
  
  private final LayoutInflater mInflater;
  
  private List<T> mObjects;
  
  private boolean mObjectsFromResources;
  
  private ArrayList<T> mOriginalValues;
  
  private final int mResource;
  
  public ArrayAdapter(Context paramContext, int paramInt) {
    this(paramContext, paramInt, 0, new ArrayList<>());
  }
  
  public ArrayAdapter(Context paramContext, int paramInt1, int paramInt2) {
    this(paramContext, paramInt1, paramInt2, new ArrayList<>());
  }
  
  public ArrayAdapter(Context paramContext, int paramInt, T[] paramArrayOfT) {
    this(paramContext, paramInt, 0, Arrays.asList(paramArrayOfT));
  }
  
  public ArrayAdapter(Context paramContext, int paramInt1, int paramInt2, T[] paramArrayOfT) {
    this(paramContext, paramInt1, paramInt2, Arrays.asList(paramArrayOfT));
  }
  
  public ArrayAdapter(Context paramContext, int paramInt, List<T> paramList) {
    this(paramContext, paramInt, 0, paramList);
  }
  
  public ArrayAdapter(Context paramContext, int paramInt1, int paramInt2, List<T> paramList) {
    this(paramContext, paramInt1, paramInt2, paramList, false);
  }
  
  private ArrayAdapter(Context paramContext, int paramInt1, int paramInt2, List<T> paramList, boolean paramBoolean) {
    this.mContext = paramContext;
    this.mInflater = LayoutInflater.from(paramContext);
    this.mDropDownResource = paramInt1;
    this.mResource = paramInt1;
    this.mObjects = paramList;
    this.mObjectsFromResources = paramBoolean;
    this.mFieldId = paramInt2;
  }
  
  public void add(T paramT) {
    synchronized (this.mLock) {
      if (this.mOriginalValues != null) {
        this.mOriginalValues.add(paramT);
      } else {
        this.mObjects.add(paramT);
      } 
      this.mObjectsFromResources = false;
      if (this.mNotifyOnChange)
        notifyDataSetChanged(); 
      return;
    } 
  }
  
  public void addAll(Collection<? extends T> paramCollection) {
    synchronized (this.mLock) {
      if (this.mOriginalValues != null) {
        this.mOriginalValues.addAll(paramCollection);
      } else {
        this.mObjects.addAll(paramCollection);
      } 
      this.mObjectsFromResources = false;
      if (this.mNotifyOnChange)
        notifyDataSetChanged(); 
      return;
    } 
  }
  
  public void addAll(T... paramVarArgs) {
    synchronized (this.mLock) {
      if (this.mOriginalValues != null) {
        Collections.addAll(this.mOriginalValues, paramVarArgs);
      } else {
        Collections.addAll(this.mObjects, paramVarArgs);
      } 
      this.mObjectsFromResources = false;
      if (this.mNotifyOnChange)
        notifyDataSetChanged(); 
      return;
    } 
  }
  
  public void insert(T paramT, int paramInt) {
    synchronized (this.mLock) {
      if (this.mOriginalValues != null) {
        this.mOriginalValues.add(paramInt, paramT);
      } else {
        this.mObjects.add(paramInt, paramT);
      } 
      this.mObjectsFromResources = false;
      if (this.mNotifyOnChange)
        notifyDataSetChanged(); 
      return;
    } 
  }
  
  public void remove(T paramT) {
    synchronized (this.mLock) {
      if (this.mOriginalValues != null) {
        this.mOriginalValues.remove(paramT);
      } else {
        this.mObjects.remove(paramT);
      } 
      this.mObjectsFromResources = false;
      if (this.mNotifyOnChange)
        notifyDataSetChanged(); 
      return;
    } 
  }
  
  public void clear() {
    synchronized (this.mLock) {
      if (this.mOriginalValues != null) {
        this.mOriginalValues.clear();
      } else {
        this.mObjects.clear();
      } 
      this.mObjectsFromResources = false;
      if (this.mNotifyOnChange)
        notifyDataSetChanged(); 
      return;
    } 
  }
  
  public void sort(Comparator<? super T> paramComparator) {
    synchronized (this.mLock) {
      if (this.mOriginalValues != null) {
        Collections.sort(this.mOriginalValues, paramComparator);
      } else {
        Collections.sort(this.mObjects, paramComparator);
      } 
      if (this.mNotifyOnChange)
        notifyDataSetChanged(); 
      return;
    } 
  }
  
  public void notifyDataSetChanged() {
    super.notifyDataSetChanged();
    this.mNotifyOnChange = true;
  }
  
  public void setNotifyOnChange(boolean paramBoolean) {
    this.mNotifyOnChange = paramBoolean;
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public int getCount() {
    return this.mObjects.size();
  }
  
  public T getItem(int paramInt) {
    return this.mObjects.get(paramInt);
  }
  
  public int getPosition(T paramT) {
    return this.mObjects.indexOf(paramT);
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    return createViewFromResource(this.mInflater, paramInt, paramView, paramViewGroup, this.mResource);
  }
  
  private View createViewFromResource(LayoutInflater paramLayoutInflater, int paramInt1, View paramView, ViewGroup paramViewGroup, int paramInt2) {
    View view;
    if (paramView == null) {
      view = paramLayoutInflater.inflate(paramInt2, paramViewGroup, false);
    } else {
      view = paramView;
    } 
    try {
      StringBuilder stringBuilder;
      Context context;
      if (this.mFieldId == 0) {
        paramView = view;
      } else {
        paramView = view.<TextView>findViewById(this.mFieldId);
        if (paramView == null) {
          RuntimeException runtimeException = new RuntimeException();
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Failed to find view with ID ");
          context = this.mContext;
          stringBuilder.append(context.getResources().getResourceName(this.mFieldId));
          stringBuilder.append(" in item layout");
          this(stringBuilder.toString());
          throw runtimeException;
        } 
      } 
      paramViewGroup = (ViewGroup)getItem(paramInt1);
      if (paramViewGroup instanceof CharSequence) {
        context.setText((CharSequence)paramViewGroup);
      } else {
        context.setText(paramViewGroup.toString());
      } 
      return (View)stringBuilder;
    } catch (ClassCastException classCastException) {
      Log.e("ArrayAdapter", "You must supply a resource ID for a TextView");
      throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", classCastException);
    } 
  }
  
  public void setDropDownViewResource(int paramInt) {
    this.mDropDownResource = paramInt;
  }
  
  public void setDropDownViewTheme(Resources.Theme paramTheme) {
    if (paramTheme == null) {
      this.mDropDownInflater = null;
    } else if (paramTheme == this.mInflater.getContext().getTheme()) {
      this.mDropDownInflater = this.mInflater;
    } else {
      ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(this.mContext, paramTheme);
      this.mDropDownInflater = LayoutInflater.from((Context)contextThemeWrapper);
    } 
  }
  
  public Resources.Theme getDropDownViewTheme() {
    Resources.Theme theme;
    LayoutInflater layoutInflater = this.mDropDownInflater;
    if (layoutInflater == null) {
      layoutInflater = null;
    } else {
      theme = layoutInflater.getContext().getTheme();
    } 
    return theme;
  }
  
  public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    LayoutInflater layoutInflater1 = this.mDropDownInflater, layoutInflater2 = layoutInflater1;
    if (layoutInflater1 == null)
      layoutInflater2 = this.mInflater; 
    return createViewFromResource(layoutInflater2, paramInt, paramView, paramViewGroup, this.mDropDownResource);
  }
  
  public static ArrayAdapter<CharSequence> createFromResource(Context paramContext, int paramInt1, int paramInt2) {
    CharSequence[] arrayOfCharSequence = paramContext.getResources().getTextArray(paramInt1);
    return new ArrayAdapter<>(paramContext, paramInt2, 0, Arrays.asList(arrayOfCharSequence), true);
  }
  
  public Filter getFilter() {
    if (this.mFilter == null)
      this.mFilter = new ArrayFilter(); 
    return this.mFilter;
  }
  
  public CharSequence[] getAutofillOptions() {
    CharSequence[] arrayOfCharSequence = super.getAutofillOptions();
    if (arrayOfCharSequence != null)
      return arrayOfCharSequence; 
    if (this.mObjectsFromResources) {
      List<T> list = this.mObjects;
      if (list != null && !list.isEmpty()) {
        int i = this.mObjects.size();
        CharSequence[] arrayOfCharSequence1 = new CharSequence[i];
        this.mObjects.toArray(arrayOfCharSequence1);
        return arrayOfCharSequence1;
      } 
    } 
    return null;
  }
  
  class ArrayFilter extends Filter {
    final ArrayAdapter this$0;
    
    private ArrayFilter() {}
    
    protected Filter.FilterResults performFiltering(CharSequence param1CharSequence) {
      Filter.FilterResults filterResults = new Filter.FilterResults();
      if (ArrayAdapter.this.mOriginalValues == null)
        synchronized (ArrayAdapter.this.mLock) {
          ArrayAdapter arrayAdapter = ArrayAdapter.this;
          ArrayList arrayList = new ArrayList();
          this(ArrayAdapter.this.mObjects);
          ArrayAdapter.access$102(arrayAdapter, arrayList);
        }  
      if (param1CharSequence == null || param1CharSequence.length() == 0)
        synchronized (ArrayAdapter.this.mLock) {
          ArrayList arrayList = new ArrayList();
          this(ArrayAdapter.this.mOriginalValues);
          filterResults.values = arrayList;
          filterResults.count = arrayList.size();
          return filterResults;
        }  
      String str = param1CharSequence.toString().toLowerCase();
      synchronized (ArrayAdapter.this.mLock) {
        ArrayList<Object> arrayList = new ArrayList();
        this(ArrayAdapter.this.mOriginalValues);
        int i = arrayList.size();
        null = new ArrayList();
        for (byte b = 0; b < i; b++) {
          Object object = arrayList.get(b);
          String str1 = object.toString().toLowerCase();
          if (str1.startsWith(str)) {
            null.add(object);
          } else {
            for (String str2 : str1.split(" ")) {
              if (str2.startsWith(str)) {
                null.add(object);
                break;
              } 
            } 
          } 
        } 
        filterResults.values = null;
        filterResults.count = null.size();
      } 
      return filterResults;
    }
    
    protected void publishResults(CharSequence param1CharSequence, Filter.FilterResults param1FilterResults) {
      ArrayAdapter.access$302(ArrayAdapter.this, (List)param1FilterResults.values);
      if (param1FilterResults.count > 0) {
        ArrayAdapter.this.notifyDataSetChanged();
      } else {
        ArrayAdapter.this.notifyDataSetInvalidated();
      } 
    }
  }
}
