package android.widget;

import android.content.Context;
import android.util.Log;

public class FlingOptimizerOverScroller {
  private static boolean DEBUG = false;
  
  private static float DECELERATION_RATE = 0.0F;
  
  private static float DECELERATION_TIME_SLOPE_GETTIME = 0.0F;
  
  private static final float INFLEXION = 0.35F;
  
  private static float SLOP_EPARAME_TERGET_DISTANCE = 0.0F;
  
  private static float SLOW_DOWN_TUNNING_DISTANCE = 5.4F;
  
  private static float SLOW_DOWN_TUNNING_VELOCITY = 4.0F;
  
  private static final float SPLINE_DISTANCE_COMPLETE = 0.9F;
  
  private static final String TAG = "FlingOptimizerOverScroller";
  
  private final float mPhysicalCoeff;
  
  private final float ppi;
  
  static {
    SLOP_EPARAME_TERGET_DISTANCE = 1.5F;
    DECELERATION_TIME_SLOPE_GETTIME = 0.56F;
    DECELERATION_RATE = (float)(Math.log(0.78D) / Math.log(0.9D));
    DEBUG = false;
  }
  
  public FlingOptimizerOverScroller(Context paramContext) {
    Log.v("FlingOptimizerOverScroller", "FlingOptimizerOverScroller Init");
    float f = (paramContext.getResources().getDisplayMetrics()).density * 160.0F;
    this.mPhysicalCoeff = f * 386.0878F * DECELERATION_TIME_SLOPE_GETTIME;
  }
  
  public int fling(int paramInt) {
    return paramInt;
  }
  
  public double getSplineFlingDistanceTuning(int paramInt, float paramFloat) {
    double d1 = Math.log((Math.abs(paramInt) * 0.35F / this.mPhysicalCoeff * paramFloat));
    float f = DECELERATION_RATE;
    double d2 = f;
    d2 = (this.mPhysicalCoeff * paramFloat) * Math.exp(f / (d2 - 1.0D) * d1);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSplineFlingDistanceTuning  distance = ");
      stringBuilder.append(d2);
      stringBuilder.append("　velocity = ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" deceleration = ");
      stringBuilder.append(d1);
      stringBuilder.append(" flingFriction = ");
      stringBuilder.append(paramFloat);
      stringBuilder.append(" mPhysicalCoeff = ");
      stringBuilder.append(this.mPhysicalCoeff);
      Log.v("FlingOptimizerOverScroller", stringBuilder.toString());
    } 
    return SLOP_EPARAME_TERGET_DISTANCE * d2;
  }
  
  public int getSplineFlingDurationTuning(int paramInt, float paramFloat) {
    double d1 = Math.log((Math.abs(paramInt) * 0.35F / this.mPhysicalCoeff * paramFloat));
    double d2 = DECELERATION_RATE - 1.0D;
    paramInt = (int)(Math.exp(d1 / d2) * 1000.0D);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSplineFlingDurationTuning  deceleration = ");
      stringBuilder.append(d1);
      stringBuilder.append(" decelMinusOne = ");
      stringBuilder.append(d2);
      stringBuilder.append(" finalDuration = ");
      stringBuilder.append(paramInt);
      Log.v("FlingOptimizerOverScroller", stringBuilder.toString());
    } 
    return (int)(paramInt * SLOP_EPARAME_TERGET_DISTANCE);
  }
  
  public double getUpdateDistance(long paramLong, int paramInt1, int paramInt2) {
    float f1 = (float)Math.exp((-SLOW_DOWN_TUNNING_DISTANCE * 590.0F / 600.0F)) + 1.0F;
    float f2 = (float)Math.exp((-SLOW_DOWN_TUNNING_DISTANCE * (float)paramLong / paramInt1));
    float f3 = f1 - f2;
    double d = (paramInt2 * f3);
    if (Math.abs(d) < (Math.abs(paramInt2) * 0.9F))
      return d; 
    float f4 = f3;
    if (f3 >= 1.0F)
      f4 = 1.0F; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getUpdateDistance distanceCoef = ");
      stringBuilder.append(f4);
      stringBuilder.append(" distance =");
      stringBuilder.append(d);
      stringBuilder.append(" splineDistance = ");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" currentTime = ");
      stringBuilder.append(paramLong);
      stringBuilder.append(" splineDuration = ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" distanceCoef1 = ");
      stringBuilder.append(f1);
      stringBuilder.append(" distanceCoef2 = ");
      stringBuilder.append(f2);
      Log.v("FlingOptimizerOverScroller", stringBuilder.toString());
    } 
    return (paramInt2 * f4);
  }
  
  public float getUpdateVelocity(long paramLong, int paramInt1, int paramInt2) {
    float f = (float)Math.exp((-SLOW_DOWN_TUNNING_VELOCITY * (float)paramLong / paramInt1));
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getUpdateVelocity  currentTime = ");
      stringBuilder.append(paramLong);
      stringBuilder.append(" splineDuration = ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" velocity = ");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" updateVelocity = ");
      stringBuilder.append(paramInt2 * f);
      Log.v("FlingOptimizerOverScroller", stringBuilder.toString());
    } 
    return paramInt2 * f;
  }
}
