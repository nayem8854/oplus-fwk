package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.R;
import java.util.Calendar;
import java.util.Locale;
import libcore.icu.LocaleData;

class TimePickerSpinnerDelegate extends TimePicker.AbstractTimePickerDelegate {
  private static final boolean DEFAULT_ENABLED_STATE = true;
  
  private static final int HOURS_IN_HALF_DAY = 12;
  
  private final Button mAmPmButton;
  
  private final NumberPicker mAmPmSpinner;
  
  private final EditText mAmPmSpinnerInput;
  
  private final String[] mAmPmStrings;
  
  private final TextView mDivider;
  
  private char mHourFormat;
  
  private final NumberPicker mHourSpinner;
  
  private final EditText mHourSpinnerInput;
  
  private boolean mHourWithTwoDigit;
  
  private boolean mIs24HourView;
  
  private boolean mIsAm;
  
  private boolean mIsEnabled = true;
  
  private final NumberPicker mMinuteSpinner;
  
  private final EditText mMinuteSpinnerInput;
  
  private final Calendar mTempCalendar;
  
  public TimePickerSpinnerDelegate(TimePicker paramTimePicker, Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramTimePicker, paramContext);
    TypedArray typedArray = this.mContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TimePicker, paramInt1, paramInt2);
    paramInt1 = typedArray.getResourceId(13, 17367337);
    typedArray.recycle();
    LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);
    View view = layoutInflater.inflate(paramInt1, this.mDelegator, true);
    view.setSaveFromParentEnabled(false);
    this.mHourSpinner = (NumberPicker)(view = paramTimePicker.<NumberPicker>findViewById(16909049));
    view.setOnValueChangedListener((NumberPicker.OnValueChangeListener)new Object(this));
    this.mHourSpinnerInput = (EditText)(view = this.mHourSpinner.<EditText>findViewById(16909248));
    view.setImeOptions(5);
    this.mDivider = (TextView)(view = this.mDelegator.<TextView>findViewById(16908933));
    if (view != null)
      setDividerText(); 
    this.mMinuteSpinner = (NumberPicker)(view = this.mDelegator.<NumberPicker>findViewById(16909178));
    view.setMinValue(0);
    this.mMinuteSpinner.setMaxValue(59);
    this.mMinuteSpinner.setOnLongPressUpdateInterval(100L);
    this.mMinuteSpinner.setFormatter(NumberPicker.getTwoDigitFormatter());
    this.mMinuteSpinner.setOnValueChangedListener((NumberPicker.OnValueChangeListener)new Object(this));
    this.mMinuteSpinnerInput = (EditText)(view = this.mMinuteSpinner.<EditText>findViewById(16909248));
    view.setImeOptions(5);
    this.mAmPmStrings = getAmPmStrings(paramContext);
    paramContext = this.mDelegator.findViewById(16908752);
    if (paramContext instanceof Button) {
      this.mAmPmSpinner = null;
      this.mAmPmSpinnerInput = null;
      this.mAmPmButton = (Button)(view = (Button)paramContext);
      view.setOnClickListener((View.OnClickListener)new Object(this));
    } else {
      this.mAmPmButton = null;
      this.mAmPmSpinner = (NumberPicker)(view = (NumberPicker)paramContext);
      view.setMinValue(0);
      this.mAmPmSpinner.setMaxValue(1);
      this.mAmPmSpinner.setDisplayedValues(this.mAmPmStrings);
      this.mAmPmSpinner.setOnValueChangedListener((NumberPicker.OnValueChangeListener)new Object(this));
      this.mAmPmSpinnerInput = (EditText)(view = this.mAmPmSpinner.<EditText>findViewById(16909248));
      view.setImeOptions(6);
    } 
    if (isAmPmAtStart()) {
      ViewGroup viewGroup = paramTimePicker.<ViewGroup>findViewById(16909538);
      viewGroup.removeView((View)paramContext);
      viewGroup.addView((View)paramContext, 0);
      ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)paramContext.getLayoutParams();
      paramInt2 = marginLayoutParams.getMarginStart();
      paramInt1 = marginLayoutParams.getMarginEnd();
      if (paramInt2 != paramInt1) {
        marginLayoutParams.setMarginStart(paramInt1);
        marginLayoutParams.setMarginEnd(paramInt2);
      } 
    } 
    getHourFormatData();
    updateHourControl();
    updateMinuteControl();
    updateAmPmControl();
    Calendar calendar = Calendar.getInstance(this.mLocale);
    setHour(calendar.get(11));
    setMinute(this.mTempCalendar.get(12));
    if (!isEnabled())
      setEnabled(false); 
    setContentDescriptions();
    if (this.mDelegator.getImportantForAccessibility() == 0)
      this.mDelegator.setImportantForAccessibility(1); 
  }
  
  public boolean validateInput() {
    return true;
  }
  
  private void getHourFormatData() {
    Locale locale = this.mLocale;
    if (this.mIs24HourView) {
      str = "Hm";
    } else {
      str = "hm";
    } 
    String str = DateFormat.getBestDateTimePattern(locale, str);
    int i = str.length();
    this.mHourWithTwoDigit = false;
    for (byte b = 0; b < i; b++) {
      char c = str.charAt(b);
      if (c == 'H' || c == 'h' || c == 'K' || c == 'k') {
        this.mHourFormat = c;
        if (b + 1 < i && c == str.charAt(b + 1))
          this.mHourWithTwoDigit = true; 
        break;
      } 
    } 
  }
  
  private boolean isAmPmAtStart() {
    String str = DateFormat.getBestDateTimePattern(this.mLocale, "hm");
    return str.startsWith("a");
  }
  
  private void setDividerText() {
    if (this.mIs24HourView) {
      str = "Hm";
    } else {
      str = "hm";
    } 
    String str = DateFormat.getBestDateTimePattern(this.mLocale, str);
    int i = str.lastIndexOf('H');
    int j = i;
    if (i == -1)
      j = str.lastIndexOf('h'); 
    if (j == -1) {
      str = ":";
    } else {
      i = str.indexOf('m', j + 1);
      if (i == -1) {
        str = Character.toString(str.charAt(j + 1));
      } else {
        str = str.substring(j + 1, i);
      } 
    } 
    this.mDivider.setText(str);
  }
  
  public void setDate(int paramInt1, int paramInt2) {
    setCurrentHour(paramInt1, false);
    setCurrentMinute(paramInt2, false);
    onTimeChanged();
  }
  
  public void setHour(int paramInt) {
    setCurrentHour(paramInt, true);
  }
  
  private void setCurrentHour(int paramInt, boolean paramBoolean) {
    if (paramInt == getHour())
      return; 
    resetAutofilledValue();
    int i = paramInt;
    if (!is24Hour()) {
      if (paramInt >= 12) {
        this.mIsAm = false;
        i = paramInt;
        if (paramInt > 12)
          i = paramInt - 12; 
      } else {
        this.mIsAm = true;
        i = paramInt;
        if (paramInt == 0)
          i = 12; 
      } 
      updateAmPmControl();
    } 
    this.mHourSpinner.setValue(i);
    if (paramBoolean)
      onTimeChanged(); 
  }
  
  public int getHour() {
    int i = this.mHourSpinner.getValue();
    if (is24Hour())
      return i; 
    if (this.mIsAm)
      return i % 12; 
    return i % 12 + 12;
  }
  
  public void setMinute(int paramInt) {
    setCurrentMinute(paramInt, true);
  }
  
  private void setCurrentMinute(int paramInt, boolean paramBoolean) {
    if (paramInt == getMinute())
      return; 
    resetAutofilledValue();
    this.mMinuteSpinner.setValue(paramInt);
    if (paramBoolean)
      onTimeChanged(); 
  }
  
  public int getMinute() {
    return this.mMinuteSpinner.getValue();
  }
  
  public void setIs24Hour(boolean paramBoolean) {
    if (this.mIs24HourView == paramBoolean)
      return; 
    int i = getHour();
    this.mIs24HourView = paramBoolean;
    getHourFormatData();
    updateHourControl();
    setCurrentHour(i, false);
    updateMinuteControl();
    updateAmPmControl();
  }
  
  public boolean is24Hour() {
    return this.mIs24HourView;
  }
  
  public void setEnabled(boolean paramBoolean) {
    this.mMinuteSpinner.setEnabled(paramBoolean);
    TextView textView = this.mDivider;
    if (textView != null)
      textView.setEnabled(paramBoolean); 
    this.mHourSpinner.setEnabled(paramBoolean);
    NumberPicker numberPicker = this.mAmPmSpinner;
    if (numberPicker != null) {
      numberPicker.setEnabled(paramBoolean);
    } else {
      this.mAmPmButton.setEnabled(paramBoolean);
    } 
    this.mIsEnabled = paramBoolean;
  }
  
  public boolean isEnabled() {
    return this.mIsEnabled;
  }
  
  public int getBaseline() {
    return this.mHourSpinner.getBaseline();
  }
  
  public Parcelable onSaveInstanceState(Parcelable paramParcelable) {
    return new TimePicker.AbstractTimePickerDelegate.SavedState(paramParcelable, getHour(), getMinute(), is24Hour());
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable instanceof TimePicker.AbstractTimePickerDelegate.SavedState) {
      paramParcelable = paramParcelable;
      setHour(paramParcelable.getHour());
      setMinute(paramParcelable.getMinute());
    } 
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    onPopulateAccessibilityEvent(paramAccessibilityEvent);
    return true;
  }
  
  public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    int i;
    if (this.mIs24HourView) {
      i = 0x1 | 0x80;
    } else {
      i = 0x1 | 0x40;
    } 
    this.mTempCalendar.set(11, getHour());
    this.mTempCalendar.set(12, getMinute());
    Context context = this.mContext;
    Calendar calendar = this.mTempCalendar;
    long l = calendar.getTimeInMillis();
    String str = DateUtils.formatDateTime(context, l, i);
    paramAccessibilityEvent.getText().add(str);
  }
  
  public View getHourView() {
    return this.mHourSpinnerInput;
  }
  
  public View getMinuteView() {
    return this.mMinuteSpinnerInput;
  }
  
  public View getAmView() {
    return this.mAmPmSpinnerInput;
  }
  
  public View getPmView() {
    return this.mAmPmSpinnerInput;
  }
  
  private void updateInputState() {
    InputMethodManager inputMethodManager = (InputMethodManager)this.mContext.getSystemService(InputMethodManager.class);
    if (inputMethodManager != null)
      if (inputMethodManager.isActive(this.mHourSpinnerInput)) {
        this.mHourSpinnerInput.clearFocus();
        inputMethodManager.hideSoftInputFromWindow(this.mDelegator.getWindowToken(), 0);
      } else if (inputMethodManager.isActive(this.mMinuteSpinnerInput)) {
        this.mMinuteSpinnerInput.clearFocus();
        inputMethodManager.hideSoftInputFromWindow(this.mDelegator.getWindowToken(), 0);
      } else if (inputMethodManager.isActive(this.mAmPmSpinnerInput)) {
        this.mAmPmSpinnerInput.clearFocus();
        inputMethodManager.hideSoftInputFromWindow(this.mDelegator.getWindowToken(), 0);
      }  
  }
  
  private void updateAmPmControl() {
    if (is24Hour()) {
      NumberPicker numberPicker = this.mAmPmSpinner;
      if (numberPicker != null) {
        numberPicker.setVisibility(8);
      } else {
        this.mAmPmButton.setVisibility(8);
      } 
    } else {
      int i = this.mIsAm ^ true;
      NumberPicker numberPicker = this.mAmPmSpinner;
      if (numberPicker != null) {
        numberPicker.setValue(i);
        this.mAmPmSpinner.setVisibility(0);
      } else {
        this.mAmPmButton.setText(this.mAmPmStrings[i]);
        this.mAmPmButton.setVisibility(0);
      } 
    } 
    this.mDelegator.sendAccessibilityEvent(4);
  }
  
  private void onTimeChanged() {
    this.mDelegator.sendAccessibilityEvent(4);
    if (this.mOnTimeChangedListener != null) {
      TimePicker.OnTimeChangedListener onTimeChangedListener = this.mOnTimeChangedListener;
      TimePicker timePicker = this.mDelegator;
      int i = getHour();
      int j = getMinute();
      onTimeChangedListener.onTimeChanged(timePicker, i, j);
    } 
    if (this.mAutoFillChangeListener != null)
      this.mAutoFillChangeListener.onTimeChanged(this.mDelegator, getHour(), getMinute()); 
  }
  
  private void updateHourControl() {
    NumberPicker.Formatter formatter;
    if (is24Hour()) {
      if (this.mHourFormat == 'k') {
        this.mHourSpinner.setMinValue(1);
        this.mHourSpinner.setMaxValue(24);
      } else {
        this.mHourSpinner.setMinValue(0);
        this.mHourSpinner.setMaxValue(23);
      } 
    } else if (this.mHourFormat == 'K') {
      this.mHourSpinner.setMinValue(0);
      this.mHourSpinner.setMaxValue(11);
    } else {
      this.mHourSpinner.setMinValue(1);
      this.mHourSpinner.setMaxValue(12);
    } 
    NumberPicker numberPicker = this.mHourSpinner;
    if (this.mHourWithTwoDigit) {
      formatter = NumberPicker.getTwoDigitFormatter();
    } else {
      formatter = null;
    } 
    numberPicker.setFormatter(formatter);
  }
  
  private void updateMinuteControl() {
    if (is24Hour()) {
      this.mMinuteSpinnerInput.setImeOptions(6);
    } else {
      this.mMinuteSpinnerInput.setImeOptions(5);
    } 
  }
  
  private void setContentDescriptions() {
    trySetContentDescription(this.mMinuteSpinner, 16909073, 17041404);
    trySetContentDescription(this.mMinuteSpinner, 16908918, 17041398);
    trySetContentDescription(this.mHourSpinner, 16909073, 17041403);
    trySetContentDescription(this.mHourSpinner, 16908918, 17041397);
    NumberPicker numberPicker = this.mAmPmSpinner;
    if (numberPicker != null) {
      trySetContentDescription(numberPicker, 16909073, 17041405);
      trySetContentDescription(this.mAmPmSpinner, 16908918, 17041399);
    } 
  }
  
  private void trySetContentDescription(View paramView, int paramInt1, int paramInt2) {
    paramView = paramView.findViewById(paramInt1);
    if (paramView != null)
      paramView.setContentDescription(this.mContext.getString(paramInt2)); 
  }
  
  public static String[] getAmPmStrings(Context paramContext) {
    String str1, str2;
    LocaleData localeData = LocaleData.get((paramContext.getResources().getConfiguration()).locale);
    if (localeData.amPm[0].length() > 4) {
      str1 = localeData.narrowAm;
    } else {
      str1 = localeData.amPm[0];
    } 
    if (localeData.amPm[1].length() > 4) {
      str2 = localeData.narrowPm;
    } else {
      str2 = ((LocaleData)str2).amPm[1];
    } 
    return new String[] { str1, str2 };
  }
}
