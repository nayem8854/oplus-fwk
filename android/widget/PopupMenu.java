package android.widget;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuPopupHelper;

public class PopupMenu {
  private final View mAnchor;
  
  private final Context mContext;
  
  private View.OnTouchListener mDragListener;
  
  private final MenuBuilder mMenu;
  
  private OnMenuItemClickListener mMenuItemClickListener;
  
  private OnDismissListener mOnDismissListener;
  
  private final MenuPopupHelper mPopup;
  
  public PopupMenu(Context paramContext, View paramView) {
    this(paramContext, paramView, 0);
  }
  
  public PopupMenu(Context paramContext, View paramView, int paramInt) {
    this(paramContext, paramView, paramInt, 16843520, 0);
  }
  
  public PopupMenu(Context paramContext, View paramView, int paramInt1, int paramInt2, int paramInt3) {
    this.mContext = paramContext;
    this.mAnchor = paramView;
    MenuBuilder menuBuilder = new MenuBuilder(paramContext);
    menuBuilder.setCallback((MenuBuilder.Callback)new Object(this));
    MenuPopupHelper menuPopupHelper = new MenuPopupHelper(paramContext, this.mMenu, paramView, false, paramInt2, paramInt3);
    menuPopupHelper.setGravity(paramInt1);
    this.mPopup.setOnDismissListener((PopupWindow.OnDismissListener)new Object(this));
  }
  
  public void setGravity(int paramInt) {
    this.mPopup.setGravity(paramInt);
  }
  
  public int getGravity() {
    return this.mPopup.getGravity();
  }
  
  public View.OnTouchListener getDragToOpenListener() {
    if (this.mDragListener == null)
      this.mDragListener = (View.OnTouchListener)new Object(this, this.mAnchor); 
    return this.mDragListener;
  }
  
  public Menu getMenu() {
    return (Menu)this.mMenu;
  }
  
  public MenuInflater getMenuInflater() {
    return new MenuInflater(this.mContext);
  }
  
  public void inflate(int paramInt) {
    getMenuInflater().inflate(paramInt, (Menu)this.mMenu);
  }
  
  public void show() {
    this.mPopup.show();
  }
  
  public void dismiss() {
    this.mPopup.dismiss();
  }
  
  public void setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener) {
    this.mMenuItemClickListener = paramOnMenuItemClickListener;
  }
  
  public void setOnDismissListener(OnDismissListener paramOnDismissListener) {
    this.mOnDismissListener = paramOnDismissListener;
  }
  
  public void setForceShowIcon(boolean paramBoolean) {
    this.mPopup.setForceShowIcon(paramBoolean);
  }
  
  public ListView getMenuListView() {
    if (!this.mPopup.isShowing())
      return null; 
    return this.mPopup.getPopup().getListView();
  }
  
  public static interface OnDismissListener {
    void onDismiss(PopupMenu param1PopupMenu);
  }
  
  public static interface OnMenuItemClickListener {
    boolean onMenuItemClick(MenuItem param1MenuItem);
  }
}
