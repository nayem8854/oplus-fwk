package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.android.internal.R;
import java.lang.reflect.Field;

public class OplusResolverGallery extends AbsSpinner implements GestureDetector.OnGestureListener {
  private int mSpacing = 0;
  
  private int mAnimationDuration = 400;
  
  private FlingRunnable mFlingRunnable = new FlingRunnable();
  
  private Runnable mDisableSuppressSelectionChangedRunnable = (Runnable)new Object(this);
  
  private boolean mShouldCallbackDuringFling = true;
  
  private boolean mShouldCallbackOnUnselectedItemClick = true;
  
  private boolean mIsRtl = true;
  
  private static final int SCROLL_TO_FLING_UNCERTAINTY_TIMEOUT = 250;
  
  private static final String TAG = "OplusResolverGallery";
  
  private boolean mBeginScroll;
  
  private AdapterView.AdapterContextMenuInfo mContextMenuInfo;
  
  private int mDownTouchPosition;
  
  private View mDownTouchView;
  
  private GestureDetector mGestureDetector;
  
  private int mGravity;
  
  private boolean mIsFirstScroll;
  
  private int mLeftMost;
  
  private OnGalleryScrollListener mOnScrollListener;
  
  private boolean mReceivedInvokeKeyDown;
  
  private int mRightMost;
  
  private int mSelectedCenterOffset;
  
  private View mSelectedChild;
  
  private boolean mShouldStopFling;
  
  private boolean mSuppressSelectionChanged;
  
  public OplusResolverGallery(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusResolverGallery(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842864);
  }
  
  public OplusResolverGallery(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public OplusResolverGallery(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Gallery, paramInt1, paramInt2);
    paramInt1 = typedArray.getInt(0, -1);
    if (paramInt1 >= 0)
      setGravity(paramInt1); 
    paramInt1 = typedArray.getInt(1, -1);
    if (paramInt1 > 0)
      setAnimationDuration(paramInt1); 
    paramInt1 = typedArray.getDimensionPixelOffset(2, 0);
    setSpacing(paramInt1);
    typedArray.recycle();
    this.mGroupFlags |= 0x400;
    this.mGroupFlags |= 0x800;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (this.mGestureDetector == null) {
      GestureDetector gestureDetector = new GestureDetector(getContext(), this);
      try {
        Field field = gestureDetector.getClass().getField("mMaximumFlingVelocity");
        field.setAccessible(true);
        field.set(this.mGestureDetector, Integer.valueOf(4));
      } catch (Exception exception) {}
    } 
  }
  
  public void setCallbackDuringFling(boolean paramBoolean) {
    this.mShouldCallbackDuringFling = paramBoolean;
  }
  
  public void setCallbackOnUnselectedItemClick(boolean paramBoolean) {
    this.mShouldCallbackOnUnselectedItemClick = paramBoolean;
  }
  
  public void setAnimationDuration(int paramInt) {
    this.mAnimationDuration = paramInt;
  }
  
  public void setSpacing(int paramInt) {
    this.mSpacing = paramInt;
  }
  
  protected int computeHorizontalScrollExtent() {
    return 1;
  }
  
  protected int computeHorizontalScrollOffset() {
    return this.mSelectedPosition;
  }
  
  protected int computeHorizontalScrollRange() {
    return this.mItemCount;
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return new LayoutParams(paramLayoutParams);
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-2, -2);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    this.mInLayout = true;
    layout(0, false);
    this.mInLayout = false;
  }
  
  int getChildHeight(View paramView) {
    return paramView.getMeasuredHeight();
  }
  
  void trackMotionScroll(int paramInt) {
    boolean bool;
    if (getChildCount() == 0)
      return; 
    if (paramInt < 0) {
      bool = true;
    } else {
      bool = false;
    } 
    int i = getLimitedMotionScrollAmount(bool, paramInt);
    if (i != paramInt) {
      this.mFlingRunnable.endFling(false);
      onFinishedMovement();
    } 
    offsetChildrenLeftAndRight(i);
    detachOffScreenChildren(bool);
    if (bool) {
      fillToGalleryRight();
    } else {
      fillToGalleryLeft();
    } 
    this.mRecycler.clear();
    setSelectionToCenterChild();
    View view = this.mSelectedChild;
    if (view != null) {
      int j = view.getLeft();
      i = view.getWidth() / 2;
      paramInt = getWidth() / 2;
      this.mSelectedCenterOffset = j + i - paramInt;
    } 
    OnGalleryScrollListener onGalleryScrollListener = this.mOnScrollListener;
    if (onGalleryScrollListener != null) {
      this.mBeginScroll = true;
      onGalleryScrollListener.onScroll(this);
    } 
    onScrollChanged(0, 0, 0, 0);
    invalidate();
  }
  
  int getLimitedMotionScrollAmount(boolean paramBoolean, int paramInt) {
    if (paramBoolean != this.mIsRtl) {
      i = this.mItemCount - 1;
    } else {
      i = 0;
    } 
    View view = getChildAt(i - this.mFirstPosition);
    if (view == null)
      return paramInt; 
    int i = getCenterOfView(view);
    int j = getCenterOfGallery();
    if (paramBoolean) {
      if (i <= j)
        return 0; 
    } else if (i >= j) {
      return 0;
    } 
    i = j - i;
    if (paramBoolean) {
      paramInt = Math.max(i, paramInt);
    } else {
      paramInt = Math.min(i, paramInt);
    } 
    return paramInt;
  }
  
  private void offsetChildrenLeftAndRight(int paramInt) {
    for (int i = getChildCount() - 1; i >= 0; i--)
      getChildAt(i).offsetLeftAndRight(paramInt); 
  }
  
  private int getCenterOfGallery() {
    return (getWidth() - this.mPaddingLeft - this.mPaddingRight) / 2 + this.mPaddingLeft;
  }
  
  private static int getCenterOfView(View paramView) {
    return paramView.getLeft() + paramView.getWidth() / 2;
  }
  
  private void detachOffScreenChildren(boolean paramBoolean) {
    int i2, i = getChildCount();
    int j = this.mFirstPosition;
    int k = 0, m = 0;
    int n = 0, i1 = 0;
    if (paramBoolean) {
      int i3 = this.mPaddingLeft;
      for (i2 = 0, n = i1, k = m; i2 < i; i2++) {
        if (this.mIsRtl) {
          m = i - 1 - i2;
        } else {
          m = i2;
        } 
        View view = getChildAt(m);
        if (view.getRight() >= i3)
          break; 
        k = m;
        n++;
        this.mRecycler.put(j + m, view);
      } 
      if (!this.mIsRtl)
        k = 0; 
      i2 = n;
    } else {
      i1 = getWidth();
      int i3 = this.mPaddingRight;
      for (i2 = i - 1; i2 >= 0; i2--) {
        if (this.mIsRtl) {
          m = i - 1 - i2;
        } else {
          m = i2;
        } 
        View view = getChildAt(m);
        if (view.getLeft() <= i1 - i3)
          break; 
        k = m;
        n++;
        this.mRecycler.put(j + m, view);
      } 
      i2 = n;
      if (this.mIsRtl) {
        k = 0;
        i2 = n;
      } 
    } 
    detachViewsFromParent(k, i2);
    if (paramBoolean != this.mIsRtl)
      this.mFirstPosition += i2; 
  }
  
  private void scrollIntoSlots() {
    if (getChildCount() != 0) {
      View view = this.mSelectedChild;
      if (view != null) {
        int i = getCenterOfView(view);
        int j = getCenterOfGallery();
        j -= i;
        if (j != 0) {
          this.mFlingRunnable.startUsingDistance(j);
        } else {
          onFinishedMovement();
        } 
        return;
      } 
    } 
  }
  
  private void onFinishedMovement() {
    if (this.mSuppressSelectionChanged) {
      this.mSuppressSelectionChanged = false;
      super.selectionChanged();
    } 
    this.mSelectedCenterOffset = 0;
    invalidate();
  }
  
  void selectionChanged() {
    if (!this.mSuppressSelectionChanged)
      super.selectionChanged(); 
  }
  
  private void setSelectionToCenterChild() {
    int n;
    View view = this.mSelectedChild;
    if (this.mSelectedChild == null)
      return; 
    int i = getCenterOfGallery();
    if (view.getLeft() <= i && view.getRight() >= i)
      return; 
    int j = Integer.MAX_VALUE;
    int k = 0;
    int m = getChildCount() - 1;
    while (true) {
      n = k;
      if (m >= 0) {
        view = getChildAt(m);
        if (view.getLeft() <= i && view.getRight() >= i) {
          n = m;
          break;
        } 
        int i1 = Math.abs(view.getLeft() - i);
        n = Math.abs(view.getRight() - i);
        i1 = Math.min(i1, n);
        n = j;
        if (i1 < j) {
          n = i1;
          k = m;
        } 
        m--;
        j = n;
        continue;
      } 
      break;
    } 
    m = this.mFirstPosition + n;
    if (m != this.mSelectedPosition) {
      setSelectedPositionInt(m);
      setNextSelectedPositionInt(m);
      checkSelectionChanged();
    } 
  }
  
  void layout(int paramInt, boolean paramBoolean) {
    this.mIsRtl = isLayoutRtl();
    paramInt = this.mSpinnerPadding.left;
    int i = this.mRight, j = this.mLeft, k = this.mSpinnerPadding.left, m = this.mSpinnerPadding.right;
    if (this.mDataChanged)
      handleDataChanged(); 
    if (this.mItemCount == 0) {
      resetList();
      return;
    } 
    if (this.mNextSelectedPosition >= 0)
      setSelectedPositionInt(this.mNextSelectedPosition); 
    recycleAllViews();
    detachAllViewsFromParent();
    this.mRightMost = 0;
    this.mLeftMost = 0;
    this.mFirstPosition = this.mSelectedPosition;
    View view = makeAndAddView(this.mSelectedPosition, 0, 0, true);
    j = (i - j - k - m) / 2;
    k = view.getWidth() / 2;
    i = this.mSelectedCenterOffset;
    view.offsetLeftAndRight(j + paramInt - k + i);
    fillToGalleryRight();
    fillToGalleryLeft();
    this.mRecycler.clear();
    invalidate();
    checkSelectionChanged();
    this.mDataChanged = false;
    this.mNeedSync = false;
    setNextSelectedPositionInt(this.mSelectedPosition);
    updateSelectedItemMetadata();
  }
  
  private void fillToGalleryLeft() {
    if (this.mIsRtl) {
      fillToGalleryLeftRtl();
    } else {
      fillToGalleryLeftLtr();
    } 
  }
  
  private void fillToGalleryLeftRtl() {
    int i = this.mSpacing;
    int j = this.mPaddingLeft;
    int k = getChildCount();
    int m = this.mItemCount;
    View view = getChildAt(k - 1);
    if (view != null) {
      m = this.mFirstPosition + k;
      k = view.getLeft() - i;
    } else {
      this.mFirstPosition = m = this.mItemCount - 1;
      int n = this.mRight;
      k = this.mLeft;
      int i1 = this.mPaddingRight;
      this.mShouldStopFling = true;
      k = n - k - i1;
    } 
    while (k > j && m < this.mItemCount) {
      view = makeAndAddView(m, m - this.mSelectedPosition, k, false);
      k = view.getLeft() - i;
      m++;
    } 
  }
  
  private void fillToGalleryLeftLtr() {
    byte b;
    int k, i = this.mSpacing;
    int j = this.mPaddingLeft;
    View view = getChildAt(0);
    if (view != null) {
      b = this.mFirstPosition - 1;
      k = view.getLeft() - i;
    } else {
      b = 0;
      int m = this.mRight;
      k = this.mLeft;
      int n = this.mPaddingRight;
      this.mShouldStopFling = true;
      k = m - k - n;
    } 
    while (k > j && b) {
      view = makeAndAddView(b, b - this.mSelectedPosition, k, false);
      this.mFirstPosition = b;
      k = view.getLeft() - i;
      b--;
    } 
  }
  
  private void fillToGalleryRight() {
    if (this.mIsRtl) {
      fillToGalleryRightRtl();
    } else {
      fillToGalleryRightLtr();
    } 
  }
  
  private void fillToGalleryRightRtl() {
    byte b;
    int n, i = this.mSpacing;
    int j = this.mRight, k = this.mLeft, m = this.mPaddingRight;
    View view = getChildAt(0);
    if (view != null) {
      b = this.mFirstPosition - 1;
      n = view.getRight() + i;
    } else {
      b = 0;
      n = this.mPaddingLeft;
      this.mShouldStopFling = true;
    } 
    while (n < j - k - m && b) {
      view = makeAndAddView(b, b - this.mSelectedPosition, n, true);
      this.mFirstPosition = b;
      n = view.getRight() + i;
      b--;
    } 
  }
  
  private void fillToGalleryRightLtr() {
    int i2, i = this.mSpacing;
    int j = this.mRight, k = this.mLeft, m = this.mPaddingRight;
    int n = getChildCount();
    int i1 = this.mItemCount;
    View view = getChildAt(n - 1);
    if (view != null) {
      n = this.mFirstPosition + n;
      i2 = view.getRight() + i;
    } else {
      this.mFirstPosition = n = this.mItemCount - 1;
      i2 = this.mPaddingLeft;
      this.mShouldStopFling = true;
    } 
    while (i2 < j - k - m && n < i1) {
      view = makeAndAddView(n, n - this.mSelectedPosition, i2, true);
      i2 = view.getRight() + i;
      n++;
    } 
  }
  
  private View makeAndAddView(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    if (!this.mDataChanged) {
      View view1 = this.mRecycler.get(paramInt1);
      if (view1 != null) {
        int i = view1.getLeft();
        int j = this.mRightMost;
        paramInt1 = view1.getMeasuredWidth();
        this.mRightMost = Math.max(j, paramInt1 + i);
        this.mLeftMost = Math.min(this.mLeftMost, i);
        setUpChild(view1, paramInt2, paramInt3, paramBoolean);
        OnGalleryScrollListener onGalleryScrollListener = this.mOnScrollListener;
        if (onGalleryScrollListener != null && !this.mBeginScroll)
          onGalleryScrollListener.onScroll(this); 
        return view1;
      } 
    } 
    View view = this.mAdapter.getView(paramInt1, null, this);
    setUpChild(view, paramInt2, paramInt3, paramBoolean);
    return view;
  }
  
  private void setUpChild(View paramView, int paramInt1, int paramInt2, boolean paramBoolean) {
    LayoutParams layoutParams1 = (LayoutParams)paramView.getLayoutParams();
    LayoutParams layoutParams2 = layoutParams1;
    if (layoutParams1 == null)
      layoutParams2 = (LayoutParams)generateDefaultLayoutParams(); 
    boolean bool = this.mIsRtl;
    boolean bool1 = false;
    if (paramBoolean != bool) {
      i = -1;
    } else {
      i = 0;
    } 
    addViewInLayout(paramView, i, layoutParams2, true);
    if (paramInt1 == 0)
      bool1 = true; 
    paramView.setSelected(bool1);
    int i = ViewGroup.getChildMeasureSpec(this.mHeightMeasureSpec, this.mSpinnerPadding.top + this.mSpinnerPadding.bottom, layoutParams2.height);
    paramInt1 = ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.mSpinnerPadding.left + this.mSpinnerPadding.right, layoutParams2.width);
    paramView.measure(paramInt1, i);
    int j = calculateTop(paramView, true);
    int k = paramView.getMeasuredHeight();
    paramInt1 = paramView.getMeasuredWidth();
    if (paramBoolean) {
      paramInt1 = paramInt2 + paramInt1;
    } else {
      i = paramInt2 - paramInt1;
      paramInt1 = paramInt2;
      paramInt2 = i;
    } 
    paramView.layout(paramInt2, j, paramInt1, k + j);
  }
  
  private int calculateTop(View paramView, boolean paramBoolean) {
    int i, j;
    if (paramBoolean) {
      i = getMeasuredHeight();
    } else {
      i = getHeight();
    } 
    if (paramBoolean) {
      j = paramView.getMeasuredHeight();
    } else {
      j = paramView.getHeight();
    } 
    int k = 0;
    int m = this.mGravity;
    if (m != 16) {
      if (m != 48) {
        if (m != 80) {
          i = k;
        } else {
          i = i - this.mSpinnerPadding.bottom - j;
        } 
      } else {
        i = this.mSpinnerPadding.top;
      } 
    } else {
      m = this.mSpinnerPadding.bottom;
      k = this.mSpinnerPadding.top;
      i = this.mSpinnerPadding.top + (i - m - k - j) / 2;
    } 
    return i;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    boolean bool = this.mGestureDetector.onTouchEvent(paramMotionEvent);
    int i = paramMotionEvent.getAction();
    if (i == 1) {
      onUp();
    } else if (i == 3) {
      onCancel();
    } 
    return bool;
  }
  
  public boolean onSingleTapUp(MotionEvent paramMotionEvent) {
    int i = this.mDownTouchPosition;
    if (i >= 0) {
      scrollToChild(i - this.mFirstPosition);
      if (this.mShouldCallbackOnUnselectedItemClick || this.mDownTouchPosition == this.mSelectedPosition) {
        View view = this.mDownTouchView;
        int j = this.mDownTouchPosition;
        SpinnerAdapter spinnerAdapter = this.mAdapter;
        i = this.mDownTouchPosition;
        long l = spinnerAdapter.getItemId(i);
        performItemClick(view, j, l);
      } 
      return true;
    } 
    return false;
  }
  
  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2) {
    if (!this.mShouldCallbackDuringFling) {
      removeCallbacks(this.mDisableSuppressSelectionChangedRunnable);
      if (!this.mSuppressSelectionChanged)
        this.mSuppressSelectionChanged = true; 
    } 
    this.mFlingRunnable.startUsingVelocity((int)-paramFloat1 / 2);
    return true;
  }
  
  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2) {
    this.mParent.requestDisallowInterceptTouchEvent(true);
    if (!this.mShouldCallbackDuringFling) {
      if (this.mIsFirstScroll) {
        if (!this.mSuppressSelectionChanged)
          this.mSuppressSelectionChanged = true; 
        postDelayed(this.mDisableSuppressSelectionChangedRunnable, 250L);
      } 
    } else if (this.mSuppressSelectionChanged) {
      this.mSuppressSelectionChanged = false;
    } 
    trackMotionScroll((int)paramFloat1 * -1);
    this.mIsFirstScroll = false;
    return true;
  }
  
  public boolean onDown(MotionEvent paramMotionEvent) {
    this.mFlingRunnable.stop(false);
    int i = pointToPosition((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
    if (i >= 0) {
      View view = getChildAt(i - this.mFirstPosition);
      view.setPressed(true);
    } 
    this.mIsFirstScroll = true;
    return true;
  }
  
  void onUp() {
    if (this.mFlingRunnable.mScroller.isFinished())
      scrollIntoSlots(); 
    dispatchUnpress();
  }
  
  void onCancel() {
    onUp();
  }
  
  public void onLongPress(MotionEvent paramMotionEvent) {
    if (this.mDownTouchPosition < 0)
      return; 
    performHapticFeedback(0);
    long l = getItemIdAtPosition(this.mDownTouchPosition);
    dispatchLongPress(this.mDownTouchView, this.mDownTouchPosition, l);
  }
  
  public void onShowPress(MotionEvent paramMotionEvent) {}
  
  private void dispatchPress(View paramView) {
    if (paramView != null)
      paramView.setPressed(true); 
    setPressed(true);
  }
  
  private void dispatchUnpress() {
    for (int i = getChildCount() - 1; i >= 0; i--)
      getChildAt(i).setPressed(false); 
    setPressed(false);
  }
  
  public void dispatchSetSelected(boolean paramBoolean) {}
  
  protected void dispatchSetPressed(boolean paramBoolean) {
    View view = this.mSelectedChild;
    if (view != null)
      view.setPressed(paramBoolean); 
  }
  
  protected ContextMenu.ContextMenuInfo getContextMenuInfo() {
    return this.mContextMenuInfo;
  }
  
  public boolean showContextMenuForChild(View paramView) {
    int i = getPositionForView(paramView);
    if (i < 0)
      return false; 
    long l = this.mAdapter.getItemId(i);
    return dispatchLongPress(paramView, i, l);
  }
  
  public boolean showContextMenu() {
    if (isPressed() && this.mSelectedPosition >= 0) {
      int i = this.mSelectedPosition, j = this.mFirstPosition;
      View view = getChildAt(i - j);
      return dispatchLongPress(view, this.mSelectedPosition, this.mSelectedRowId);
    } 
    return false;
  }
  
  private boolean dispatchLongPress(View paramView, int paramInt, long paramLong) {
    boolean bool1 = false;
    if (this.mOnItemLongClickListener != null)
      bool1 = this.mOnItemLongClickListener.onItemLongClick(this, this.mDownTouchView, this.mDownTouchPosition, paramLong); 
    boolean bool2 = bool1;
    if (!bool1) {
      this.mContextMenuInfo = new AdapterView.AdapterContextMenuInfo(paramView, paramInt, paramLong);
      bool2 = super.showContextMenuForChild(this);
    } 
    if (bool2)
      performHapticFeedback(0); 
    return bool2;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    return paramKeyEvent.dispatch(this, null, null);
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (paramInt != 66)
      switch (paramInt) {
        default:
          return super.onKeyDown(paramInt, paramKeyEvent);
        case 22:
          if (moveDirection(1)) {
            playSoundEffect(3);
            return true;
          } 
        case 21:
          if (moveDirection(-1)) {
            playSoundEffect(1);
            return true;
          } 
        case 23:
          break;
      }  
    this.mReceivedInvokeKeyDown = true;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    Object object;
    if (KeyEvent.isConfirmKey(paramInt)) {
      if (this.mReceivedInvokeKeyDown && 
        this.mItemCount > 0) {
        dispatchPress(this.mSelectedChild);
        object = new Object(this);
        long l = ViewConfiguration.getPressedStateDuration();
        postDelayed((Runnable)object, l);
        int i = this.mSelectedPosition;
        paramInt = this.mFirstPosition;
        object = getChildAt(i - paramInt);
        paramInt = this.mSelectedPosition;
        SpinnerAdapter spinnerAdapter = this.mAdapter;
        i = this.mSelectedPosition;
        l = spinnerAdapter.getItemId(i);
        performItemClick((View)object, paramInt, l);
      } 
      this.mReceivedInvokeKeyDown = false;
      return true;
    } 
    return super.onKeyUp(paramInt, (KeyEvent)object);
  }
  
  boolean moveDirection(int paramInt) {
    if (isLayoutRtl())
      paramInt = -paramInt; 
    paramInt = this.mSelectedPosition + paramInt;
    if (this.mItemCount > 0 && paramInt >= 0 && paramInt < this.mItemCount) {
      scrollToChild(paramInt - this.mFirstPosition);
      return true;
    } 
    return false;
  }
  
  private boolean scrollToChild(int paramInt) {
    View view = getChildAt(paramInt);
    if (view != null) {
      paramInt = getCenterOfGallery();
      int i = getCenterOfView(view);
      this.mFlingRunnable.startUsingDistance(paramInt - i);
      return true;
    } 
    return false;
  }
  
  void setSelectedPositionInt(int paramInt) {
    super.setSelectedPositionInt(paramInt);
    updateSelectedItemMetadata();
  }
  
  private void updateSelectedItemMetadata() {
    View view1 = this.mSelectedChild;
    View view2 = getChildAt(this.mSelectedPosition - this.mFirstPosition);
    if (view2 == null)
      return; 
    view2.setSelected(true);
    view2.setFocusable(true);
    if (hasFocus())
      view2.requestFocus(); 
    if (view1 != null && view1 != view2) {
      view1.setSelected(false);
      view1.setFocusable(false);
    } 
  }
  
  public void setGravity(int paramInt) {
    if (this.mGravity != paramInt) {
      this.mGravity = paramInt;
      requestLayout();
    } 
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2) {
    int i = this.mSelectedPosition - this.mFirstPosition;
    if (i < 0)
      return paramInt2; 
    if (paramInt2 == paramInt1 - 1)
      return i; 
    if (paramInt2 >= i)
      return paramInt2 + 1; 
    return paramInt2;
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    if (paramBoolean) {
      View view = this.mSelectedChild;
      if (view != null) {
        view.requestFocus(paramInt);
        this.mSelectedChild.setSelected(true);
      } 
    } 
  }
  
  public CharSequence getAccessibilityClassName() {
    return OplusResolverGallery.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    boolean bool;
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (this.mItemCount > 1) {
      bool = true;
    } else {
      bool = false;
    } 
    paramAccessibilityNodeInfo.setScrollable(bool);
    if (isEnabled()) {
      if (this.mItemCount > 0 && this.mSelectedPosition < this.mItemCount - 1)
        paramAccessibilityNodeInfo.addAction(4096); 
      if (isEnabled() && this.mItemCount > 0 && this.mSelectedPosition > 0)
        paramAccessibilityNodeInfo.addAction(8192); 
    } 
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    if (paramInt != 4096) {
      if (paramInt != 8192)
        return false; 
      if (isEnabled() && this.mItemCount > 0 && this.mSelectedPosition > 0) {
        paramInt = this.mSelectedPosition;
        int i = this.mFirstPosition;
        return scrollToChild(paramInt - i - 1);
      } 
      return false;
    } 
    if (isEnabled() && this.mItemCount > 0 && this.mSelectedPosition < this.mItemCount - 1) {
      paramInt = this.mSelectedPosition;
      int i = this.mFirstPosition;
      return scrollToChild(paramInt - i + 1);
    } 
    return false;
  }
  
  class FlingRunnable implements Runnable {
    private int mLastFlingX;
    
    private Scroller mScroller;
    
    final OplusResolverGallery this$0;
    
    public FlingRunnable() {
      AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
      this.mScroller = new Scroller(OplusResolverGallery.this.getContext(), accelerateDecelerateInterpolator);
    }
    
    private void startCommon() {
      OplusResolverGallery.this.removeCallbacks(this);
    }
    
    public void startUsingVelocity(int param1Int) {
      boolean bool;
      if (param1Int == 0)
        return; 
      startCommon();
      if (param1Int < 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mLastFlingX = bool;
      this.mScroller.fling(bool, 0, param1Int, 0, 0, 2147483647, 0, 2147483647);
      OplusResolverGallery.this.post(this);
    }
    
    public void startUsingDistance(int param1Int) {
      if (param1Int == 0)
        return; 
      startCommon();
      this.mLastFlingX = 0;
      this.mScroller.startScroll(0, 0, -param1Int, 0, OplusResolverGallery.this.mAnimationDuration);
      OplusResolverGallery.this.post(this);
    }
    
    public void stop(boolean param1Boolean) {
      OplusResolverGallery.this.removeCallbacks(this);
      endFling(param1Boolean);
    }
    
    private void endFling(boolean param1Boolean) {
      this.mScroller.forceFinished(true);
      if (param1Boolean)
        OplusResolverGallery.this.scrollIntoSlots(); 
    }
    
    public void run() {
      int k;
      if (OplusResolverGallery.this.mItemCount == 0) {
        endFling(true);
        return;
      } 
      OplusResolverGallery.access$602(OplusResolverGallery.this, false);
      Scroller scroller = this.mScroller;
      boolean bool = scroller.computeScrollOffset();
      int i = scroller.getCurrX();
      int j = this.mLastFlingX - i;
      if (j > 0) {
        OplusResolverGallery oplusResolverGallery = OplusResolverGallery.this;
        if (oplusResolverGallery.mIsRtl) {
          k = OplusResolverGallery.this.mFirstPosition + OplusResolverGallery.this.getChildCount() - 1;
        } else {
          k = OplusResolverGallery.this.mFirstPosition;
        } 
        OplusResolverGallery.access$702(oplusResolverGallery, k);
        k = Math.min(OplusResolverGallery.this.getWidth() - OplusResolverGallery.this.mPaddingLeft - OplusResolverGallery.this.mPaddingRight - 1, j);
      } else {
        OplusResolverGallery.this.getChildCount();
        OplusResolverGallery oplusResolverGallery = OplusResolverGallery.this;
        if (oplusResolverGallery.mIsRtl) {
          k = OplusResolverGallery.this.mFirstPosition;
        } else {
          k = OplusResolverGallery.this.mFirstPosition + OplusResolverGallery.this.getChildCount() - 1;
        } 
        OplusResolverGallery.access$702(oplusResolverGallery, k);
        k = Math.max(-(OplusResolverGallery.this.getWidth() - OplusResolverGallery.this.mPaddingRight - OplusResolverGallery.this.mPaddingLeft - 1), j);
      } 
      OplusResolverGallery.this.trackMotionScroll(k);
      if (bool && !OplusResolverGallery.this.mShouldStopFling) {
        this.mLastFlingX = i;
        OplusResolverGallery.this.post(this);
      } else {
        endFling(true);
      } 
    }
  }
  
  class LayoutParams extends ViewGroup.LayoutParams {
    public LayoutParams(OplusResolverGallery this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(OplusResolverGallery this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(OplusResolverGallery this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
  }
  
  public void setOnScrollListener(OnGalleryScrollListener paramOnGalleryScrollListener) {
    this.mOnScrollListener = paramOnGalleryScrollListener;
  }
  
  class OnGalleryScrollListener {
    public abstract void onScroll(OplusResolverGallery param1OplusResolverGallery);
  }
}
