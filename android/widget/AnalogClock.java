package android.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Process;
import android.os.UserHandle;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.View;
import com.android.internal.R;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@RemoteView
@Deprecated
public class AnalogClock extends View {
  private boolean mAttached;
  
  private boolean mChanged;
  
  private Clock mClock;
  
  private Drawable mDial;
  
  private int mDialHeight;
  
  private int mDialWidth;
  
  private float mHour;
  
  private Drawable mHourHand;
  
  private final BroadcastReceiver mIntentReceiver;
  
  private Drawable mMinuteHand;
  
  private float mMinutes;
  
  public AnalogClock(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public AnalogClock(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public AnalogClock(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AnalogClock(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mIntentReceiver = (BroadcastReceiver)new Object(this);
    paramContext.getResources();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AnalogClock, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.AnalogClock, paramAttributeSet, typedArray, paramInt1, paramInt2);
    Drawable drawable = typedArray.getDrawable(0);
    if (drawable == null)
      this.mDial = paramContext.getDrawable(17302126); 
    this.mHourHand = drawable = typedArray.getDrawable(1);
    if (drawable == null)
      this.mHourHand = paramContext.getDrawable(17302127); 
    this.mMinuteHand = drawable = typedArray.getDrawable(2);
    if (drawable == null)
      this.mMinuteHand = paramContext.getDrawable(17302128); 
    this.mClock = Clock.systemDefaultZone();
    this.mDialWidth = this.mDial.getIntrinsicWidth();
    this.mDialHeight = this.mDial.getIntrinsicHeight();
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (!this.mAttached) {
      this.mAttached = true;
      IntentFilter intentFilter = new IntentFilter();
      intentFilter.addAction("android.intent.action.TIME_TICK");
      intentFilter.addAction("android.intent.action.TIME_SET");
      intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
      Context context = getContext();
      BroadcastReceiver broadcastReceiver = this.mIntentReceiver;
      UserHandle userHandle = Process.myUserHandle();
      Handler handler = getHandler();
      context.registerReceiverAsUser(broadcastReceiver, userHandle, intentFilter, null, handler);
    } 
    this.mClock = Clock.systemDefaultZone();
    onTimeChanged();
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (this.mAttached) {
      getContext().unregisterReceiver(this.mIntentReceiver);
      this.mAttached = false;
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getMode(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt1);
    int k = View.MeasureSpec.getMode(paramInt2);
    int m = View.MeasureSpec.getSize(paramInt2);
    float f1 = 1.0F;
    float f2 = 1.0F;
    float f3 = f1;
    if (i != 0) {
      i = this.mDialWidth;
      f3 = f1;
      if (j < i)
        f3 = j / i; 
    } 
    f1 = f2;
    if (k != 0) {
      j = this.mDialHeight;
      f1 = f2;
      if (m < j)
        f1 = m / j; 
    } 
    f3 = Math.min(f3, f1);
    paramInt1 = resolveSizeAndState((int)(this.mDialWidth * f3), paramInt1, 0);
    m = (int)(this.mDialHeight * f3);
    paramInt2 = resolveSizeAndState(m, paramInt2, 0);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mChanged = true;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    boolean bool = this.mChanged;
    if (bool)
      this.mChanged = false; 
    int i = this.mRight - this.mLeft;
    int j = this.mBottom - this.mTop;
    int k = i / 2;
    int m = j / 2;
    Drawable drawable = this.mDial;
    int n = drawable.getIntrinsicWidth();
    int i1 = drawable.getIntrinsicHeight();
    boolean bool1 = false;
    if (i < n || j < i1) {
      bool1 = true;
      float f = Math.min(i / n, j / i1);
      paramCanvas.save();
      paramCanvas.scale(f, f, k, m);
    } 
    if (bool)
      drawable.setBounds(k - n / 2, m - i1 / 2, n / 2 + k, i1 / 2 + m); 
    drawable.draw(paramCanvas);
    paramCanvas.save();
    paramCanvas.rotate(this.mHour / 12.0F * 360.0F, k, m);
    drawable = this.mHourHand;
    if (bool) {
      j = drawable.getIntrinsicWidth();
      n = drawable.getIntrinsicHeight();
      drawable.setBounds(k - j / 2, m - n / 2, j / 2 + k, m + n / 2);
    } 
    drawable.draw(paramCanvas);
    paramCanvas.restore();
    paramCanvas.save();
    paramCanvas.rotate(this.mMinutes / 60.0F * 360.0F, k, m);
    drawable = this.mMinuteHand;
    if (bool) {
      n = drawable.getIntrinsicWidth();
      j = drawable.getIntrinsicHeight();
      drawable.setBounds(k - n / 2, m - j / 2, n / 2 + k, m + j / 2);
    } 
    drawable.draw(paramCanvas);
    paramCanvas.restore();
    if (bool1)
      paramCanvas.restore(); 
  }
  
  private void onTimeChanged() {
    long l = this.mClock.millis();
    LocalDateTime localDateTime = toLocalDateTime(l, this.mClock.getZone());
    int i = localDateTime.getHour();
    int j = localDateTime.getMinute();
    int k = localDateTime.getSecond();
    float f = j + k / 60.0F;
    this.mHour = i + f / 60.0F;
    this.mChanged = true;
    updateContentDescription(l);
  }
  
  private void updateContentDescription(long paramLong) {
    String str = DateUtils.formatDateTime(this.mContext, paramLong, 129);
    setContentDescription(str);
  }
  
  private static LocalDateTime toLocalDateTime(long paramLong, ZoneId paramZoneId) {
    Instant instant = Instant.ofEpochMilli(paramLong);
    return LocalDateTime.ofInstant(instant, paramZoneId);
  }
}
