package android.widget;

import android.content.Context;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

public class OplusScroller {
  final String TAG = "OplusScroller";
  
  final boolean DEBUG_SPRING = false;
  
  private int mLastCurrV = 0;
  
  private int mCurrV = 0;
  
  private int DeltaCurrV = 0;
  
  private float mCoeffX = 0.0F;
  
  private float mCoeffY = 1.0F;
  
  private int mCount = 1;
  
  private int fmLastCurrY = 0;
  
  private int mLastCurrY = 0;
  
  private static final int DEFAULT_DURATION = 250;
  
  private static final int DEFAULT_TIME_GAP = 15;
  
  private static final int FLING_MODE = 1;
  
  private static final int FLING_SCROLL_BACK_DURATION = 750;
  
  private static final int FLING_SCROLL_BACK_MODE = 3;
  
  private static final int FLING_SPRING_MODE = 2;
  
  private static final int GALLERY_LIST_MODE = 5;
  
  private static final int GALLERY_TIME_GAP = 25;
  
  private static final int SCROLL_LIST_MODE = 4;
  
  private static final int SCROLL_MODE = 0;
  
  private int fmCurrY;
  
  private int mCurrVX;
  
  private int mCurrVY;
  
  private int mCurrX;
  
  private int mCurrY;
  
  public float mDeceleration;
  
  private float mDeltaX;
  
  private float mDeltaY;
  
  private int mDuration;
  
  private float mDurationReciprocal;
  
  private int mFinalX;
  
  private int mFinalY;
  
  private boolean mFinished;
  
  private Interpolator mInterpolator;
  
  private int mMaxX;
  
  private int mMaxY;
  
  private int mMinX;
  
  private int mMinY;
  
  private int mMode;
  
  private final float mPpi;
  
  private long mStartTime;
  
  private int mStartX;
  
  private int mStartY;
  
  private float mVelocity;
  
  private float mViscousFluidNormalize;
  
  private float mViscousFluidScale;
  
  public OplusScroller(Context paramContext) {
    this(paramContext, null);
  }
  
  public OplusScroller(Context paramContext, Interpolator paramInterpolator) {
    this.mFinished = true;
    this.mInterpolator = paramInterpolator;
    float f = (paramContext.getResources().getDisplayMetrics()).density * 160.0F;
    this.mDeceleration = f * 386.0878F * ViewConfiguration.getScrollFriction();
  }
  
  public final boolean isFinished() {
    return this.mFinished;
  }
  
  public final void forceFinished(boolean paramBoolean) {
    this.mFinished = paramBoolean;
  }
  
  public final int getDuration() {
    return this.mDuration;
  }
  
  public final int getCurrX() {
    return this.mCurrX;
  }
  
  public final int getCurrY() {
    return this.mCurrY;
  }
  
  public float getCurrVelocity() {
    return this.mVelocity - this.mDeceleration * timePassed() / 2000.0F;
  }
  
  public final int getStartX() {
    return this.mStartX;
  }
  
  public final int getStartY() {
    return this.mStartY;
  }
  
  public final int getFinalX() {
    return this.mFinalX;
  }
  
  public final int getFinalY() {
    return this.mFinalY;
  }
  
  public final int getCurrVX() {
    return this.mCurrVX;
  }
  
  public final int getCurrVY() {
    return this.mCurrVY;
  }
  
  public boolean computeScrollOffset() {
    if (this.mFinished)
      return false; 
    int i = (int)(AnimationUtils.currentAnimationTimeMillis() - this.mStartTime);
    int j = this.mMode;
    if (4 == j) {
      i = this.mCount * 15;
    } else if (5 == j) {
      i = this.mCount * 25;
    } 
    if (i < this.mDuration) {
      j = this.mMode;
      if (j != 0)
        if (j != 1) {
          if (j != 2) {
            if (j != 3) {
              if (j != 4) {
                if (j != 5)
                  return true; 
              } else {
                float f1 = (i + 200) / 2000.0F;
                if (i == 15) {
                  this.DeltaCurrV = 0;
                  this.mLastCurrV = 0;
                  this.fmLastCurrY = 0;
                } 
                f1 = 1.0F - (float)Math.exp((-4.0F * f1)) - 1.0F + (float)Math.exp(-0.4000000059604645D);
                this.mCurrX = this.mStartX + (int)(this.mDeltaX * f1);
                this.fmCurrY = i = this.mStartY + (int)(this.mDeltaY * f1) - this.DeltaCurrV;
                this.mCurrV = i - this.fmLastCurrY;
                i = this.mLastCurrV;
                if (i != 0 && Math.abs(i) < Math.abs(this.mCurrV)) {
                  j = this.DeltaCurrV;
                  int k = this.mCurrV;
                  i = this.mLastCurrV;
                  this.DeltaCurrV = j + k - i;
                  this.fmCurrY -= k - i;
                  this.mCurrV = i;
                } 
                this.fmLastCurrY = i = this.fmCurrY;
                this.mLastCurrV = this.mCurrV;
                this.mCurrY = i;
                i = (i - this.mLastCurrY) * 250 / 15;
                this.mCurrVY = Math.round(i);
                this.mLastCurrY = i = this.mCurrY;
                if ((this.mCurrX == this.mFinalX && i == this.mFinalY) || this.mCurrVY == 0) {
                  this.mFinalY = this.mCurrY;
                  this.mFinished = true;
                } 
                return true;
              } 
            } else {
              float f1 = i, f2 = this.mDurationReciprocal;
              f1 = viscousFluid(f1 * f2);
              this.mCurrX = this.mStartX + Math.round(this.mDeltaX * f1);
              this.mCurrY = i = this.mStartY + Math.round(this.mDeltaY * f1);
              if (this.mCurrX == this.mFinalX && i == this.mFinalY)
                this.mFinished = true; 
              return true;
            } 
          } else {
            float f1 = i / 1000.0F;
            f1 = this.mVelocity * f1 - this.mDeceleration * f1 * f1 / 2.0F;
            this.mCurrX = i = this.mStartX + Math.round(this.mCoeffX * f1);
            this.mCurrX = i = Math.min(i, this.mMaxX);
            this.mCurrX = Math.max(i, this.mMinX);
            this.mCurrY = i = this.mStartY + Math.round(this.mCoeffY * f1);
            this.mCurrY = i = Math.min(i, this.mMaxY);
            this.mCurrY = i = Math.max(i, this.mMinY);
            if (this.mCurrX == this.mFinalX || i == this.mFinalY) {
              j = this.mCurrX;
              i = this.mCurrY;
              startScroll(j, i, (int)(this.mDeltaX + (this.mFinalX - j)), (int)(this.mDeltaY + (this.mFinalY - i)), 750);
              this.mMode = 3;
            } 
            return true;
          } 
        } else {
          float f2 = i / 1000.0F;
          float f1 = this.mVelocity * f2 - this.mDeceleration * f2 * f2 / 2.0F;
          this.mCurrX = i = this.mStartX + Math.round(this.mCoeffX * f1);
          this.mCurrX = i = Math.min(i, this.mMaxX);
          this.mCurrX = Math.max(i, this.mMinX);
          this.mCurrY = i = this.mStartY + Math.round(this.mCoeffY * f1);
          this.mCurrY = i = Math.min(i, this.mMaxY);
          this.mCurrY = Math.max(i, this.mMinY);
          f1 = this.mVelocity - this.mDeceleration * f2;
          this.mCurrVX = Math.round(this.mCoeffX * f1);
          this.mCurrVY = Math.round(this.mCoeffY * f1);
          if (this.mCurrX == this.mFinalX && this.mCurrY == this.mFinalY)
            this.mFinished = true; 
          return true;
        }  
      float f = i * this.mDurationReciprocal;
      Interpolator interpolator = this.mInterpolator;
      if (interpolator == null) {
        f = viscousFluid(f);
      } else {
        f = interpolator.getInterpolation(f);
      } 
      this.mCurrX = this.mStartX + Math.round(this.mDeltaX * f);
      this.mCurrY = i = this.mStartY + Math.round(this.mDeltaY * f);
      if (this.mCurrX == this.mFinalX && i == this.mFinalY)
        this.mFinished = true; 
    } else {
      if (this.mMode == 2) {
        i = this.mCurrX;
        j = this.mCurrY;
        startScroll(i, j, (int)(this.mDeltaX + (this.mFinalX - i)), (int)(this.mDeltaY + (this.mFinalY - j)), 750);
        this.mMode = 3;
        return true;
      } 
      this.mCurrX = this.mFinalX;
      this.mCurrY = this.mFinalY;
      this.mFinished = true;
    } 
    return true;
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    startScroll(paramInt1, paramInt2, paramInt3, paramInt4, 250);
  }
  
  public void startScrollList(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    startScroll(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    this.mMode = 4;
    this.mViscousFluidNormalize = 1.0F;
    this.mViscousFluidNormalize = 1.0F / getInterpolation(1.0F);
    this.mLastCurrY = 0;
    this.mCount = 1;
  }
  
  public void startGalleryList(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    startScroll(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    this.mMode = 5;
    this.mCount = 1;
  }
  
  public void setCount(int paramInt) {
    this.mCount = paramInt;
  }
  
  public void startScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mMode = 0;
    this.mFinished = false;
    this.mDuration = paramInt5;
    this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
    this.mStartX = paramInt1;
    this.mStartY = paramInt2;
    this.mFinalX = paramInt1 + paramInt3;
    this.mFinalY = paramInt2 + paramInt4;
    this.mDeltaX = paramInt3;
    this.mDeltaY = paramInt4;
    this.mDurationReciprocal = 1.0F / this.mDuration;
    this.mViscousFluidScale = 8.0F;
    this.mViscousFluidNormalize = 1.0F;
    this.mViscousFluidNormalize = 1.0F / viscousFluid(1.0F);
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    float f3;
    this.mMode = 1;
    this.mFinished = false;
    float f1 = (float)Math.hypot(paramInt3, paramInt4);
    this.mVelocity = f1;
    this.mDuration = (int)(1000.0F * f1 / this.mDeceleration);
    this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
    this.mStartX = paramInt1;
    this.mStartY = paramInt2;
    float f2 = 1.0F;
    if (f1 == 0.0F) {
      f3 = 1.0F;
    } else {
      f3 = paramInt3 / f1;
    } 
    this.mCoeffX = f3;
    if (f1 == 0.0F) {
      f3 = f2;
    } else {
      f3 = paramInt4 / f1;
    } 
    this.mCoeffY = f3;
    paramInt3 = (int)(f1 * f1 / this.mDeceleration * 2.0F);
    this.mMinX = paramInt5;
    this.mMaxX = paramInt6;
    this.mMinY = paramInt7;
    this.mMaxY = paramInt8;
    this.mFinalX = paramInt1 = Math.round(paramInt3 * this.mCoeffX) + paramInt1;
    this.mFinalX = paramInt1 = Math.min(paramInt1, this.mMaxX);
    this.mFinalX = Math.max(paramInt1, this.mMinX);
    this.mFinalY = paramInt1 = Math.round(paramInt3 * this.mCoeffY) + paramInt2;
    this.mFinalY = paramInt1 = Math.min(paramInt1, this.mMaxY);
    this.mFinalY = Math.max(paramInt1, this.mMinY);
  }
  
  public void fling(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    fling(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5 - paramInt9, paramInt6 + paramInt9, paramInt7 - paramInt10, paramInt8 + paramInt10);
    this.mDeltaY = 0.0F;
    this.mDeltaX = 0.0F;
    paramInt1 = this.mFinalX;
    if (paramInt1 > paramInt6 || paramInt1 < paramInt5) {
      this.mMode = 2;
      paramInt1 = this.mFinalX;
      if (paramInt1 > paramInt6) {
        this.mDeltaX = (paramInt6 - paramInt1);
      } else {
        this.mDeltaX = (paramInt5 - paramInt1);
      } 
    } 
    paramInt1 = this.mFinalY;
    if (paramInt1 > paramInt8 || paramInt1 < paramInt7) {
      this.mMode = 2;
      paramInt1 = this.mFinalY;
      if (paramInt1 > paramInt8) {
        this.mDeltaY = (paramInt8 - paramInt1);
      } else {
        this.mDeltaY = (paramInt7 - paramInt1);
      } 
    } 
  }
  
  private float viscousFluid(float paramFloat) {
    paramFloat *= this.mViscousFluidScale;
    if (paramFloat < 1.0F) {
      paramFloat -= 1.0F - (float)Math.exp(-paramFloat);
    } else {
      paramFloat = (float)Math.exp((1.0F - paramFloat));
      paramFloat = 0.36787945F + (1.0F - 0.36787945F) * (1.0F - paramFloat);
    } 
    float f = this.mViscousFluidNormalize;
    return paramFloat * f;
  }
  
  private float getInterpolation(float paramFloat) {
    paramFloat = (float)Math.exp(-(paramFloat * 12.0F));
    float f = this.mViscousFluidNormalize;
    return (1.0F - paramFloat) * (1.0F - 0.36787945F) * f;
  }
  
  public void abortAnimation() {
    this.mCurrX = this.mFinalX;
    this.mCurrY = this.mFinalY;
    this.mFinished = true;
  }
  
  public void extendDuration(int paramInt) {
    int i = timePassed();
    this.mDuration = paramInt = i + paramInt;
    this.mDurationReciprocal = 1.0F / paramInt;
    this.mFinished = false;
  }
  
  public int timePassed() {
    return (int)(AnimationUtils.currentAnimationTimeMillis() - this.mStartTime);
  }
  
  public void setFinalX(int paramInt) {
    this.mFinalX = paramInt;
    this.mDeltaX = (paramInt - this.mStartX);
    this.mFinished = false;
  }
  
  public void setFinalY(int paramInt) {
    this.mFinalY = paramInt;
    this.mDeltaY = (paramInt - this.mStartY);
    this.mFinished = false;
  }
  
  public final void setFriction(float paramFloat) {
    this.mDeceleration = computeDeceleration(paramFloat);
  }
  
  private float computeDeceleration(float paramFloat) {
    return this.mPpi * 386.0878F * paramFloat;
  }
}
