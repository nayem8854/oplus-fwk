package android.widget;

import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class TabHost extends FrameLayout implements ViewTreeObserver.OnTouchModeChangeListener {
  private List<TabSpec> mTabSpecs = new ArrayList<>(2);
  
  protected int mCurrentTab = -1;
  
  private View mCurrentView = null;
  
  protected LocalActivityManager mLocalActivityManager = null;
  
  private static final int TABWIDGET_LOCATION_BOTTOM = 3;
  
  private static final int TABWIDGET_LOCATION_LEFT = 0;
  
  private static final int TABWIDGET_LOCATION_RIGHT = 2;
  
  private static final int TABWIDGET_LOCATION_TOP = 1;
  
  private OnTabChangeListener mOnTabChangeListener;
  
  private FrameLayout mTabContent;
  
  private View.OnKeyListener mTabKeyListener;
  
  private int mTabLayoutId;
  
  private TabWidget mTabWidget;
  
  public TabHost(Context paramContext) {
    super(paramContext);
    initTabHost();
  }
  
  public TabHost(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842883);
  }
  
  public TabHost(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TabHost(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TabWidget, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.TabWidget, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mTabLayoutId = typedArray.getResourceId(4, 0);
    typedArray.recycle();
    if (this.mTabLayoutId == 0)
      this.mTabLayoutId = 17367320; 
    initTabHost();
  }
  
  private void initTabHost() {
    setFocusableInTouchMode(true);
    setDescendantFocusability(262144);
    this.mCurrentTab = -1;
    this.mCurrentView = null;
  }
  
  public TabSpec newTabSpec(String paramString) {
    if (paramString != null)
      return new TabSpec(paramString); 
    throw new IllegalArgumentException("tag must be non-null");
  }
  
  public void setup() {
    TabWidget tabWidget = findViewById(16908307);
    if (tabWidget != null) {
      this.mTabKeyListener = (View.OnKeyListener)new Object(this);
      this.mTabWidget.setTabSelectionListener((TabWidget.OnTabSelectionChanged)new Object(this));
      FrameLayout frameLayout = findViewById(16908305);
      if (frameLayout != null)
        return; 
      throw new RuntimeException("Your TabHost must have a FrameLayout whose id attribute is 'android.R.id.tabcontent'");
    } 
    throw new RuntimeException("Your TabHost must have a TabWidget whose id attribute is 'android.R.id.tabs'");
  }
  
  public void sendAccessibilityEventInternal(int paramInt) {}
  
  public void setup(LocalActivityManager paramLocalActivityManager) {
    setup();
    this.mLocalActivityManager = paramLocalActivityManager;
  }
  
  public void onTouchModeChanged(boolean paramBoolean) {}
  
  public void addTab(TabSpec paramTabSpec) {
    if (paramTabSpec.mIndicatorStrategy != null) {
      if (paramTabSpec.mContentStrategy != null) {
        View view = paramTabSpec.mIndicatorStrategy.createIndicatorView();
        view.setOnKeyListener(this.mTabKeyListener);
        if (paramTabSpec.mIndicatorStrategy instanceof ViewIndicatorStrategy)
          this.mTabWidget.setStripEnabled(false); 
        this.mTabWidget.addView(view);
        this.mTabSpecs.add(paramTabSpec);
        if (this.mCurrentTab == -1)
          setCurrentTab(0); 
        return;
      } 
      throw new IllegalArgumentException("you must specify a way to create the tab content");
    } 
    throw new IllegalArgumentException("you must specify a way to create the tab indicator.");
  }
  
  public void clearAllTabs() {
    this.mTabWidget.removeAllViews();
    initTabHost();
    this.mTabContent.removeAllViews();
    this.mTabSpecs.clear();
    requestLayout();
    invalidate();
  }
  
  public TabWidget getTabWidget() {
    return this.mTabWidget;
  }
  
  public int getCurrentTab() {
    return this.mCurrentTab;
  }
  
  public String getCurrentTabTag() {
    int i = this.mCurrentTab;
    if (i >= 0 && i < this.mTabSpecs.size())
      return ((TabSpec)this.mTabSpecs.get(this.mCurrentTab)).getTag(); 
    return null;
  }
  
  public View getCurrentTabView() {
    int i = this.mCurrentTab;
    if (i >= 0 && i < this.mTabSpecs.size())
      return this.mTabWidget.getChildTabViewAt(this.mCurrentTab); 
    return null;
  }
  
  public View getCurrentView() {
    return this.mCurrentView;
  }
  
  public void setCurrentTabByTag(String paramString) {
    byte b;
    int i;
    for (b = 0, i = this.mTabSpecs.size(); b < i; b++) {
      if (((TabSpec)this.mTabSpecs.get(b)).getTag().equals(paramString)) {
        setCurrentTab(b);
        break;
      } 
    } 
  }
  
  public FrameLayout getTabContentView() {
    return this.mTabContent;
  }
  
  private int getTabWidgetLocation() {
    int i = this.mTabWidget.getOrientation();
    byte b = 1;
    if (i != 1) {
      if (this.mTabContent.getTop() < this.mTabWidget.getTop())
        b = 3; 
    } else if (this.mTabContent.getLeft() < this.mTabWidget.getLeft()) {
      b = 2;
    } else {
      b = 0;
    } 
    return b;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    boolean bool = super.dispatchKeyEvent(paramKeyEvent);
    if (!bool && 
      paramKeyEvent.getAction() == 0) {
      View view = this.mCurrentView;
      if (view != null)
        if (view.isRootNamespace()) {
          view = this.mCurrentView;
          if (view.hasFocus()) {
            byte b;
            boolean bool1;
            int i = getTabWidgetLocation();
            if (i != 0) {
              if (i != 2) {
                if (i != 3) {
                  i = 19;
                  b = 33;
                  bool1 = true;
                } else {
                  i = 20;
                  b = 130;
                  bool1 = true;
                } 
              } else {
                i = 22;
                b = 66;
                bool1 = true;
              } 
            } else {
              i = 21;
              b = 17;
              bool1 = true;
            } 
            if (paramKeyEvent.getKeyCode() == i) {
              View view1 = this.mCurrentView;
              if (view1.findFocus().focusSearch(b) == null) {
                this.mTabWidget.getChildTabViewAt(this.mCurrentTab).requestFocus();
                playSoundEffect(bool1);
                return true;
              } 
            } 
          } 
        }  
    } 
    return bool;
  }
  
  public void dispatchWindowFocusChanged(boolean paramBoolean) {
    View view = this.mCurrentView;
    if (view != null)
      view.dispatchWindowFocusChanged(paramBoolean); 
  }
  
  public CharSequence getAccessibilityClassName() {
    return TabHost.class.getName();
  }
  
  public void setCurrentTab(int paramInt) {
    if (paramInt < 0 || paramInt >= this.mTabSpecs.size())
      return; 
    int i = this.mCurrentTab;
    if (paramInt == i)
      return; 
    if (i != -1)
      (this.mTabSpecs.get(i)).mContentStrategy.tabClosed(); 
    this.mCurrentTab = paramInt;
    TabSpec tabSpec = this.mTabSpecs.get(paramInt);
    this.mTabWidget.focusCurrentTab(this.mCurrentTab);
    View view = tabSpec.mContentStrategy.getContentView();
    if (view.getParent() == null) {
      FrameLayout frameLayout = this.mTabContent;
      View view1 = this.mCurrentView;
      ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
      frameLayout.addView(view1, layoutParams);
    } 
    if (!this.mTabWidget.hasFocus())
      this.mCurrentView.requestFocus(); 
    invokeOnTabChangeListener();
  }
  
  public void setOnTabChangedListener(OnTabChangeListener paramOnTabChangeListener) {
    this.mOnTabChangeListener = paramOnTabChangeListener;
  }
  
  private void invokeOnTabChangeListener() {
    OnTabChangeListener onTabChangeListener = this.mOnTabChangeListener;
    if (onTabChangeListener != null)
      onTabChangeListener.onTabChanged(getCurrentTabTag()); 
  }
  
  class TabSpec {
    private TabHost.ContentStrategy mContentStrategy;
    
    private TabHost.IndicatorStrategy mIndicatorStrategy;
    
    private final String mTag;
    
    final TabHost this$0;
    
    private TabSpec(String param1String) {
      this.mTag = param1String;
    }
    
    public TabSpec setIndicator(CharSequence param1CharSequence) {
      this.mIndicatorStrategy = new TabHost.LabelIndicatorStrategy(param1CharSequence);
      return this;
    }
    
    public TabSpec setIndicator(CharSequence param1CharSequence, Drawable param1Drawable) {
      this.mIndicatorStrategy = new TabHost.LabelAndIconIndicatorStrategy(param1CharSequence, param1Drawable);
      return this;
    }
    
    public TabSpec setIndicator(View param1View) {
      this.mIndicatorStrategy = new TabHost.ViewIndicatorStrategy(param1View);
      return this;
    }
    
    public TabSpec setContent(int param1Int) {
      this.mContentStrategy = new TabHost.ViewIdContentStrategy(param1Int);
      return this;
    }
    
    public TabSpec setContent(TabHost.TabContentFactory param1TabContentFactory) {
      this.mContentStrategy = new TabHost.FactoryContentStrategy(this.mTag, param1TabContentFactory);
      return this;
    }
    
    public TabSpec setContent(Intent param1Intent) {
      this.mContentStrategy = new TabHost.IntentContentStrategy(this.mTag, param1Intent);
      return this;
    }
    
    public String getTag() {
      return this.mTag;
    }
  }
  
  class LabelIndicatorStrategy implements IndicatorStrategy {
    private final CharSequence mLabel;
    
    final TabHost this$0;
    
    private LabelIndicatorStrategy(CharSequence param1CharSequence) {
      this.mLabel = param1CharSequence;
    }
    
    public View createIndicatorView() {
      Context context = TabHost.this.getContext();
      LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
      int i = TabHost.this.mTabLayoutId;
      TabHost tabHost = TabHost.this;
      TabWidget tabWidget = tabHost.mTabWidget;
      View view = layoutInflater.inflate(i, tabWidget, false);
      TextView textView = view.<TextView>findViewById(16908310);
      textView.setText(this.mLabel);
      if ((context.getApplicationInfo()).targetSdkVersion <= 4) {
        view.setBackgroundResource(17303712);
        textView.setTextColor(context.getColorStateList(17170997));
      } 
      return view;
    }
  }
  
  class LabelAndIconIndicatorStrategy implements IndicatorStrategy {
    private final Drawable mIcon;
    
    private final CharSequence mLabel;
    
    final TabHost this$0;
    
    private LabelAndIconIndicatorStrategy(CharSequence param1CharSequence, Drawable param1Drawable) {
      this.mLabel = param1CharSequence;
      this.mIcon = param1Drawable;
    }
    
    public View createIndicatorView() {
      boolean bool2;
      Context context = TabHost.this.getContext();
      LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
      int i = TabHost.this.mTabLayoutId;
      TabHost tabHost = TabHost.this;
      TabWidget tabWidget = tabHost.mTabWidget;
      View view = layoutInflater.inflate(i, tabWidget, false);
      TextView textView = view.<TextView>findViewById(16908310);
      ImageView imageView = view.<ImageView>findViewById(16908294);
      i = imageView.getVisibility();
      boolean bool1 = true;
      if (i == 8) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      i = bool1;
      if (bool2)
        if (TextUtils.isEmpty(this.mLabel)) {
          i = bool1;
        } else {
          i = 0;
        }  
      textView.setText(this.mLabel);
      if (i != 0) {
        Drawable drawable = this.mIcon;
        if (drawable != null) {
          imageView.setImageDrawable(drawable);
          imageView.setVisibility(0);
        } 
      } 
      if ((context.getApplicationInfo()).targetSdkVersion <= 4) {
        view.setBackgroundResource(17303712);
        textView.setTextColor(context.getColorStateList(17170997));
      } 
      return view;
    }
  }
  
  class ViewIndicatorStrategy implements IndicatorStrategy {
    private final View mView;
    
    final TabHost this$0;
    
    private ViewIndicatorStrategy(View param1View) {
      this.mView = param1View;
    }
    
    public View createIndicatorView() {
      return this.mView;
    }
  }
  
  class ViewIdContentStrategy implements ContentStrategy {
    private final View mView;
    
    final TabHost this$0;
    
    private ViewIdContentStrategy(int param1Int) {
      this.mView = TabHost.this = TabHost.this.mTabContent.findViewById(param1Int);
      if (TabHost.this != null) {
        TabHost.this.setVisibility(8);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not create tab content because could not find view with id ");
      stringBuilder.append(param1Int);
      throw new RuntimeException(stringBuilder.toString());
    }
    
    public View getContentView() {
      this.mView.setVisibility(0);
      return this.mView;
    }
    
    public void tabClosed() {
      this.mView.setVisibility(8);
    }
  }
  
  class FactoryContentStrategy implements ContentStrategy {
    private TabHost.TabContentFactory mFactory;
    
    private View mTabContent;
    
    private final CharSequence mTag;
    
    final TabHost this$0;
    
    public FactoryContentStrategy(CharSequence param1CharSequence, TabHost.TabContentFactory param1TabContentFactory) {
      this.mTag = param1CharSequence;
      this.mFactory = param1TabContentFactory;
    }
    
    public View getContentView() {
      if (this.mTabContent == null)
        this.mTabContent = this.mFactory.createTabContent(this.mTag.toString()); 
      this.mTabContent.setVisibility(0);
      return this.mTabContent;
    }
    
    public void tabClosed() {
      this.mTabContent.setVisibility(8);
    }
  }
  
  class IntentContentStrategy implements ContentStrategy {
    private final Intent mIntent;
    
    private View mLaunchedView;
    
    private final String mTag;
    
    final TabHost this$0;
    
    private IntentContentStrategy(String param1String, Intent param1Intent) {
      this.mTag = param1String;
      this.mIntent = param1Intent;
    }
    
    public View getContentView() {
      if (TabHost.this.mLocalActivityManager != null) {
        Window window = TabHost.this.mLocalActivityManager.startActivity(this.mTag, this.mIntent);
        if (window != null) {
          View view1 = window.getDecorView();
        } else {
          window = null;
        } 
        View view = this.mLaunchedView;
        if (view != window && view != null && 
          view.getParent() != null)
          TabHost.this.mTabContent.removeView(this.mLaunchedView); 
        this.mLaunchedView = (View)window;
        if (window != null) {
          window.setVisibility(0);
          this.mLaunchedView.setFocusableInTouchMode(true);
          ((ViewGroup)this.mLaunchedView).setDescendantFocusability(262144);
        } 
        return this.mLaunchedView;
      } 
      throw new IllegalStateException("Did you forget to call 'public void setup(LocalActivityManager activityGroup)'?");
    }
    
    public void tabClosed() {
      View view = this.mLaunchedView;
      if (view != null)
        view.setVisibility(8); 
    }
  }
  
  class ContentStrategy {
    public abstract View getContentView();
    
    public abstract void tabClosed();
  }
  
  class IndicatorStrategy {
    public abstract View createIndicatorView();
  }
  
  class OnTabChangeListener {
    public abstract void onTabChanged(String param1String);
  }
  
  class TabContentFactory {
    public abstract View createTabContent(String param1String);
  }
}
