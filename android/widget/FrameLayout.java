package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import com.android.internal.R;
import java.util.ArrayList;

@RemoteView
public class FrameLayout extends ViewGroup {
  @ExportedProperty(category = "measurement")
  boolean mMeasureAllChildren = false;
  
  @ExportedProperty(category = "padding")
  private int mForegroundPaddingLeft = 0;
  
  @ExportedProperty(category = "padding")
  private int mForegroundPaddingTop = 0;
  
  @ExportedProperty(category = "padding")
  private int mForegroundPaddingRight = 0;
  
  @ExportedProperty(category = "padding")
  private int mForegroundPaddingBottom = 0;
  
  private final ArrayList<View> mMatchParentChildren = new ArrayList<>(1);
  
  private static final int DEFAULT_CHILD_GRAVITY = 8388659;
  
  public FrameLayout(Context paramContext) {
    super(paramContext);
  }
  
  public FrameLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public FrameLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public FrameLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FrameLayout, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.FrameLayout, paramAttributeSet, typedArray, paramInt1, paramInt2);
    if (typedArray.getBoolean(0, false))
      setMeasureAllChildren(true); 
    typedArray.recycle();
  }
  
  @RemotableViewMethod
  public void setForegroundGravity(int paramInt) {
    if (getForegroundGravity() != paramInt) {
      super.setForegroundGravity(paramInt);
      Drawable drawable = getForeground();
      if (getForegroundGravity() == 119 && drawable != null) {
        Rect rect = new Rect();
        if (drawable.getPadding(rect)) {
          this.mForegroundPaddingLeft = rect.left;
          this.mForegroundPaddingTop = rect.top;
          this.mForegroundPaddingRight = rect.right;
          this.mForegroundPaddingBottom = rect.bottom;
        } 
      } else {
        this.mForegroundPaddingLeft = 0;
        this.mForegroundPaddingTop = 0;
        this.mForegroundPaddingRight = 0;
        this.mForegroundPaddingBottom = 0;
      } 
      requestLayout();
    } 
  }
  
  protected LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-1, -1);
  }
  
  int getPaddingLeftWithForeground() {
    int i;
    if (isForegroundInsidePadding()) {
      i = Math.max(this.mPaddingLeft, this.mForegroundPaddingLeft);
    } else {
      i = this.mPaddingLeft + this.mForegroundPaddingLeft;
    } 
    return i;
  }
  
  int getPaddingRightWithForeground() {
    int i;
    if (isForegroundInsidePadding()) {
      i = Math.max(this.mPaddingRight, this.mForegroundPaddingRight);
    } else {
      i = this.mPaddingRight + this.mForegroundPaddingRight;
    } 
    return i;
  }
  
  private int getPaddingTopWithForeground() {
    int i;
    if (isForegroundInsidePadding()) {
      i = Math.max(this.mPaddingTop, this.mForegroundPaddingTop);
    } else {
      i = this.mPaddingTop + this.mForegroundPaddingTop;
    } 
    return i;
  }
  
  private int getPaddingBottomWithForeground() {
    int i;
    if (isForegroundInsidePadding()) {
      i = Math.max(this.mPaddingBottom, this.mForegroundPaddingBottom);
    } else {
      i = this.mPaddingBottom + this.mForegroundPaddingBottom;
    } 
    return i;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = getChildCount();
    if (View.MeasureSpec.getMode(paramInt1) != 1073741824 || 
      View.MeasureSpec.getMode(paramInt2) != 1073741824) {
      j = 1;
    } else {
      j = 0;
    } 
    this.mMatchParentChildren.clear();
    int k, m, n, i1;
    for (k = 0, m = 0, n = 0, i1 = 0; i1 < i; i1++) {
      View view = getChildAt(i1);
      if (this.mMeasureAllChildren || view.getVisibility() != 8) {
        measureChildWithMargins(view, paramInt1, 0, paramInt2, 0);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        int i3 = view.getMeasuredWidth(), i4 = layoutParams.leftMargin, i5 = layoutParams.rightMargin;
        m = Math.max(m, i3 + i4 + i5);
        i3 = view.getMeasuredHeight();
        i4 = layoutParams.topMargin;
        i5 = layoutParams.bottomMargin;
        k = Math.max(k, i3 + i4 + i5);
        n = combineMeasuredStates(n, view.getMeasuredState());
        if (j && (
          layoutParams.width == -1 || layoutParams.height == -1))
          this.mMatchParentChildren.add(view); 
      } 
    } 
    int j = getPaddingLeftWithForeground();
    i = getPaddingRightWithForeground();
    i1 = getPaddingTopWithForeground();
    int i2 = getPaddingBottomWithForeground();
    i1 = Math.max(k + i1 + i2, getSuggestedMinimumHeight());
    m = Math.max(m + j + i, getSuggestedMinimumWidth());
    Drawable drawable = getForeground();
    k = i1;
    j = m;
    if (drawable != null) {
      k = Math.max(i1, drawable.getMinimumHeight());
      j = Math.max(m, drawable.getMinimumWidth());
    } 
    j = resolveSizeAndState(j, paramInt1, n);
    n = resolveSizeAndState(k, paramInt2, n << 16);
    setMeasuredDimension(j, n);
    i1 = this.mMatchParentChildren.size();
    if (i1 > 1)
      for (n = 0; n < i1; n++) {
        View view = this.mMatchParentChildren.get(n);
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        if (marginLayoutParams.width == -1) {
          i2 = getMeasuredWidth();
          i = getPaddingLeftWithForeground();
          j = getPaddingRightWithForeground();
          k = marginLayoutParams.leftMargin;
          m = marginLayoutParams.rightMargin;
          j = Math.max(0, i2 - i - j - k - m);
          j = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
        } else {
          j = getPaddingLeftWithForeground();
          i = getPaddingRightWithForeground();
          k = marginLayoutParams.leftMargin;
          i2 = marginLayoutParams.rightMargin;
          m = marginLayoutParams.width;
          j = getChildMeasureSpec(paramInt1, j + i + k + i2, m);
        } 
        if (marginLayoutParams.height == -1) {
          k = getMeasuredHeight();
          i2 = getPaddingTopWithForeground();
          int i3 = getPaddingBottomWithForeground();
          i = marginLayoutParams.topMargin;
          m = marginLayoutParams.bottomMargin;
          k = Math.max(0, k - i2 - i3 - i - m);
          k = View.MeasureSpec.makeMeasureSpec(k, 1073741824);
        } else {
          int i3 = getPaddingTopWithForeground();
          i2 = getPaddingBottomWithForeground();
          m = marginLayoutParams.topMargin;
          k = marginLayoutParams.bottomMargin;
          i = marginLayoutParams.height;
          k = getChildMeasureSpec(paramInt2, i3 + i2 + m + k, i);
        } 
        view.measure(j, k);
      }  
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    layoutChildren(paramInt1, paramInt2, paramInt3, paramInt4, false);
  }
  
  void layoutChildren(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getChildCount : ()I
    //   4: istore #6
    //   6: aload_0
    //   7: invokevirtual getPaddingLeftWithForeground : ()I
    //   10: istore #7
    //   12: iload_3
    //   13: iload_1
    //   14: isub
    //   15: aload_0
    //   16: invokevirtual getPaddingRightWithForeground : ()I
    //   19: isub
    //   20: istore_3
    //   21: aload_0
    //   22: invokespecial getPaddingTopWithForeground : ()I
    //   25: istore #8
    //   27: iload #4
    //   29: iload_2
    //   30: isub
    //   31: aload_0
    //   32: invokespecial getPaddingBottomWithForeground : ()I
    //   35: isub
    //   36: istore #9
    //   38: iconst_0
    //   39: istore #10
    //   41: iload #7
    //   43: istore #4
    //   45: iload #10
    //   47: iload #6
    //   49: if_icmpge -> 307
    //   52: aload_0
    //   53: iload #10
    //   55: invokevirtual getChildAt : (I)Landroid/view/View;
    //   58: astore #11
    //   60: aload #11
    //   62: invokevirtual getVisibility : ()I
    //   65: bipush #8
    //   67: if_icmpeq -> 301
    //   70: aload #11
    //   72: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   75: checkcast android/widget/FrameLayout$LayoutParams
    //   78: astore #12
    //   80: aload #11
    //   82: invokevirtual getMeasuredWidth : ()I
    //   85: istore #7
    //   87: aload #11
    //   89: invokevirtual getMeasuredHeight : ()I
    //   92: istore #13
    //   94: aload #12
    //   96: getfield gravity : I
    //   99: istore_2
    //   100: iload_2
    //   101: istore_1
    //   102: iload_2
    //   103: iconst_m1
    //   104: if_icmpne -> 110
    //   107: ldc 8388659
    //   109: istore_1
    //   110: aload_0
    //   111: invokevirtual getLayoutDirection : ()I
    //   114: istore_2
    //   115: iload_1
    //   116: iload_2
    //   117: invokestatic getAbsoluteGravity : (II)I
    //   120: istore_2
    //   121: iload_1
    //   122: bipush #112
    //   124: iand
    //   125: istore_1
    //   126: iload_2
    //   127: bipush #7
    //   129: iand
    //   130: istore_2
    //   131: iload_2
    //   132: iconst_1
    //   133: if_icmpeq -> 175
    //   136: iload_2
    //   137: iconst_5
    //   138: if_icmpeq -> 144
    //   141: goto -> 163
    //   144: iload #5
    //   146: ifne -> 163
    //   149: iload_3
    //   150: iload #7
    //   152: isub
    //   153: aload #12
    //   155: getfield rightMargin : I
    //   158: isub
    //   159: istore_2
    //   160: goto -> 200
    //   163: aload #12
    //   165: getfield leftMargin : I
    //   168: iload #4
    //   170: iadd
    //   171: istore_2
    //   172: goto -> 200
    //   175: iload_3
    //   176: iload #4
    //   178: isub
    //   179: iload #7
    //   181: isub
    //   182: iconst_2
    //   183: idiv
    //   184: iload #4
    //   186: iadd
    //   187: aload #12
    //   189: getfield leftMargin : I
    //   192: iadd
    //   193: aload #12
    //   195: getfield rightMargin : I
    //   198: isub
    //   199: istore_2
    //   200: iload_1
    //   201: bipush #16
    //   203: if_icmpeq -> 257
    //   206: iload_1
    //   207: bipush #48
    //   209: if_icmpeq -> 245
    //   212: iload_1
    //   213: bipush #80
    //   215: if_icmpeq -> 230
    //   218: aload #12
    //   220: getfield topMargin : I
    //   223: iload #8
    //   225: iadd
    //   226: istore_1
    //   227: goto -> 283
    //   230: iload #9
    //   232: iload #13
    //   234: isub
    //   235: aload #12
    //   237: getfield bottomMargin : I
    //   240: isub
    //   241: istore_1
    //   242: goto -> 283
    //   245: iload #8
    //   247: aload #12
    //   249: getfield topMargin : I
    //   252: iadd
    //   253: istore_1
    //   254: goto -> 283
    //   257: iload #9
    //   259: iload #8
    //   261: isub
    //   262: iload #13
    //   264: isub
    //   265: iconst_2
    //   266: idiv
    //   267: iload #8
    //   269: iadd
    //   270: aload #12
    //   272: getfield topMargin : I
    //   275: iadd
    //   276: aload #12
    //   278: getfield bottomMargin : I
    //   281: isub
    //   282: istore_1
    //   283: aload #11
    //   285: iload_2
    //   286: iload_1
    //   287: iload_2
    //   288: iload #7
    //   290: iadd
    //   291: iload_1
    //   292: iload #13
    //   294: iadd
    //   295: invokevirtual layout : (IIII)V
    //   298: goto -> 301
    //   301: iinc #10, 1
    //   304: goto -> 45
    //   307: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #274	-> 0
    //   #276	-> 6
    //   #277	-> 12
    //   #279	-> 21
    //   #280	-> 27
    //   #282	-> 38
    //   #283	-> 52
    //   #284	-> 60
    //   #285	-> 70
    //   #287	-> 80
    //   #288	-> 87
    //   #293	-> 94
    //   #294	-> 100
    //   #295	-> 107
    //   #298	-> 110
    //   #299	-> 115
    //   #300	-> 121
    //   #302	-> 126
    //   #308	-> 144
    //   #309	-> 149
    //   #310	-> 160
    //   #314	-> 163
    //   #304	-> 175
    //   #306	-> 200
    //   #317	-> 200
    //   #329	-> 218
    //   #326	-> 230
    //   #327	-> 242
    //   #319	-> 245
    //   #320	-> 254
    //   #322	-> 257
    //   #324	-> 283
    //   #332	-> 283
    //   #284	-> 301
    //   #282	-> 301
    //   #335	-> 307
  }
  
  @RemotableViewMethod
  public void setMeasureAllChildren(boolean paramBoolean) {
    this.mMeasureAllChildren = paramBoolean;
  }
  
  @Deprecated
  public boolean getConsiderGoneChildrenWhenMeasuring() {
    return getMeasureAllChildren();
  }
  
  public boolean getMeasureAllChildren() {
    return this.mMeasureAllChildren;
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  public boolean shouldDelayChildPressedState() {
    return false;
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    if (sPreserveMarginParamsInLayoutParamConversion) {
      if (paramLayoutParams instanceof LayoutParams)
        return new LayoutParams((LayoutParams)paramLayoutParams); 
      if (paramLayoutParams instanceof ViewGroup.MarginLayoutParams)
        return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams); 
    } 
    return new LayoutParams(paramLayoutParams);
  }
  
  public CharSequence getAccessibilityClassName() {
    return FrameLayout.class.getName();
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("measurement:measureAllChildren", this.mMeasureAllChildren);
    paramViewHierarchyEncoder.addProperty("padding:foregroundPaddingLeft", this.mForegroundPaddingLeft);
    paramViewHierarchyEncoder.addProperty("padding:foregroundPaddingTop", this.mForegroundPaddingTop);
    paramViewHierarchyEncoder.addProperty("padding:foregroundPaddingRight", this.mForegroundPaddingRight);
    paramViewHierarchyEncoder.addProperty("padding:foregroundPaddingBottom", this.mForegroundPaddingBottom);
  }
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    public static final int UNSPECIFIED_GRAVITY = -1;
    
    public int gravity = -1;
    
    public LayoutParams(FrameLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, R.styleable.FrameLayout_Layout);
      this.gravity = typedArray.getInt(0, -1);
      typedArray.recycle();
    }
    
    public LayoutParams(FrameLayout this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(FrameLayout this$0, int param1Int1, int param1Int2) {
      super(this$0, param1Int1);
      this.gravity = param1Int2;
    }
    
    public LayoutParams(FrameLayout this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public LayoutParams(FrameLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
    }
    
    public LayoutParams(FrameLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
      this.gravity = ((LayoutParams)this$0).gravity;
    }
  }
}
