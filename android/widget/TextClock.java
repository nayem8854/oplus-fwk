package android.widget;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Process;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewHierarchyEncoder;
import com.android.internal.R;
import java.util.Calendar;
import java.util.TimeZone;
import libcore.icu.LocaleData;

@RemoteView
public class TextClock extends TextView {
  @Deprecated
  public static final CharSequence DEFAULT_FORMAT_12_HOUR = "h:mm a";
  
  @Deprecated
  public static final CharSequence DEFAULT_FORMAT_24_HOUR = "H:mm";
  
  private CharSequence mDescFormat;
  
  private CharSequence mDescFormat12;
  
  private CharSequence mDescFormat24;
  
  @ExportedProperty
  private CharSequence mFormat;
  
  private CharSequence mFormat12;
  
  private CharSequence mFormat24;
  
  private ContentObserver mFormatChangeObserver;
  
  @ExportedProperty
  private boolean mHasSeconds;
  
  class FormatChangeObserver extends ContentObserver {
    final TextClock this$0;
    
    public FormatChangeObserver(Handler param1Handler) {
      super(param1Handler);
    }
    
    public void onChange(boolean param1Boolean) {
      TextClock.this.chooseFormat();
      TextClock.this.onTimeChanged();
    }
    
    public void onChange(boolean param1Boolean, Uri param1Uri) {
      TextClock.this.chooseFormat();
      TextClock.this.onTimeChanged();
    }
  }
  
  private final BroadcastReceiver mIntentReceiver = (BroadcastReceiver)new Object(this);
  
  private boolean mRegistered;
  
  private boolean mShouldRunTicker;
  
  private boolean mShowCurrentUserTime;
  
  private boolean mStopTicking;
  
  private final Runnable mTicker = (Runnable)new Object(this);
  
  private Calendar mTime;
  
  private String mTimeZone;
  
  public TextClock(Context paramContext) {
    super(paramContext);
    init();
  }
  
  public TextClock(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public TextClock(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TextClock(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TextClock, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.TextClock, paramAttributeSet, typedArray, paramInt1, paramInt2);
    try {
      this.mFormat12 = typedArray.getText(0);
      this.mFormat24 = typedArray.getText(1);
      this.mTimeZone = typedArray.getString(2);
      typedArray.recycle();
      return;
    } finally {
      typedArray.recycle();
    } 
  }
  
  private void init() {
    if (this.mFormat12 == null || this.mFormat24 == null) {
      LocaleData localeData = LocaleData.get((getContext().getResources().getConfiguration()).locale);
      if (this.mFormat12 == null)
        this.mFormat12 = localeData.timeFormat_hm; 
      if (this.mFormat24 == null)
        this.mFormat24 = localeData.timeFormat_Hm; 
    } 
    createTime(this.mTimeZone);
    chooseFormat();
  }
  
  private void createTime(String paramString) {
    if (paramString != null) {
      this.mTime = Calendar.getInstance(TimeZone.getTimeZone(paramString));
    } else {
      this.mTime = Calendar.getInstance();
    } 
  }
  
  @ExportedProperty
  public CharSequence getFormat12Hour() {
    return this.mFormat12;
  }
  
  @RemotableViewMethod
  public void setFormat12Hour(CharSequence paramCharSequence) {
    this.mFormat12 = paramCharSequence;
    chooseFormat();
    onTimeChanged();
  }
  
  public void setContentDescriptionFormat12Hour(CharSequence paramCharSequence) {
    this.mDescFormat12 = paramCharSequence;
    chooseFormat();
    onTimeChanged();
  }
  
  @ExportedProperty
  public CharSequence getFormat24Hour() {
    return this.mFormat24;
  }
  
  @RemotableViewMethod
  public void setFormat24Hour(CharSequence paramCharSequence) {
    this.mFormat24 = paramCharSequence;
    chooseFormat();
    onTimeChanged();
  }
  
  public void setContentDescriptionFormat24Hour(CharSequence paramCharSequence) {
    this.mDescFormat24 = paramCharSequence;
    chooseFormat();
    onTimeChanged();
  }
  
  public void setShowCurrentUserTime(boolean paramBoolean) {
    this.mShowCurrentUserTime = paramBoolean;
    chooseFormat();
    onTimeChanged();
    unregisterObserver();
    registerObserver();
  }
  
  public void refreshTime() {
    onTimeChanged();
    invalidate();
  }
  
  public boolean is24HourModeEnabled() {
    if (this.mShowCurrentUserTime)
      return DateFormat.is24HourFormat(getContext(), ActivityManager.getCurrentUser()); 
    return DateFormat.is24HourFormat(getContext());
  }
  
  public String getTimeZone() {
    return this.mTimeZone;
  }
  
  @RemotableViewMethod
  public void setTimeZone(String paramString) {
    this.mTimeZone = paramString;
    createTime(paramString);
    onTimeChanged();
  }
  
  public CharSequence getFormat() {
    return this.mFormat;
  }
  
  private void chooseFormat() {
    CharSequence charSequence;
    boolean bool1 = is24HourModeEnabled();
    LocaleData localeData = LocaleData.get((getContext().getResources().getConfiguration()).locale);
    if (bool1) {
      this.mFormat = charSequence = abc(this.mFormat24, this.mFormat12, localeData.timeFormat_Hm);
      this.mDescFormat = abc(this.mDescFormat24, this.mDescFormat12, charSequence);
    } else {
      this.mFormat = charSequence = abc(this.mFormat12, this.mFormat24, ((LocaleData)charSequence).timeFormat_hm);
      this.mDescFormat = abc(this.mDescFormat12, this.mDescFormat24, charSequence);
    } 
    boolean bool2 = this.mHasSeconds;
    this.mHasSeconds = bool1 = DateFormat.hasSeconds(this.mFormat);
    if (this.mShouldRunTicker && bool2 != bool1)
      if (bool2) {
        getHandler().removeCallbacks(this.mTicker);
      } else {
        this.mTicker.run();
      }  
  }
  
  private static CharSequence abc(CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3) {
    if (paramCharSequence1 == null)
      if (paramCharSequence2 == null) {
        paramCharSequence1 = paramCharSequence3;
      } else {
        paramCharSequence1 = paramCharSequence2;
      }  
    return paramCharSequence1;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (!this.mRegistered) {
      this.mRegistered = true;
      registerReceiver();
      registerObserver();
      createTime(this.mTimeZone);
    } 
  }
  
  public void onVisibilityAggregated(boolean paramBoolean) {
    super.onVisibilityAggregated(paramBoolean);
    if (!this.mShouldRunTicker && paramBoolean) {
      this.mShouldRunTicker = true;
      if (this.mHasSeconds) {
        this.mTicker.run();
      } else {
        onTimeChanged();
      } 
    } else if (this.mShouldRunTicker && !paramBoolean) {
      this.mShouldRunTicker = false;
      getHandler().removeCallbacks(this.mTicker);
    } 
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (this.mRegistered) {
      unregisterReceiver();
      unregisterObserver();
      this.mRegistered = false;
    } 
  }
  
  public void disableClockTick() {
    this.mStopTicking = true;
  }
  
  private void registerReceiver() {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.TIME_TICK");
    intentFilter.addAction("android.intent.action.TIME_SET");
    intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
    Context context = getContext();
    BroadcastReceiver broadcastReceiver = this.mIntentReceiver;
    UserHandle userHandle = Process.myUserHandle();
    Handler handler = getHandler();
    context.registerReceiverAsUser(broadcastReceiver, userHandle, intentFilter, null, handler);
  }
  
  private void registerObserver() {
    if (this.mRegistered) {
      if (this.mFormatChangeObserver == null)
        this.mFormatChangeObserver = new FormatChangeObserver(getHandler()); 
      ContentResolver contentResolver = getContext().getContentResolver();
      Uri uri = Settings.System.getUriFor("time_12_24");
      if (this.mShowCurrentUserTime) {
        contentResolver.registerContentObserver(uri, true, this.mFormatChangeObserver, -1);
      } else {
        ContentObserver contentObserver = this.mFormatChangeObserver;
        int i = UserHandle.myUserId();
        contentResolver.registerContentObserver(uri, true, contentObserver, i);
      } 
    } 
  }
  
  private void unregisterReceiver() {
    getContext().unregisterReceiver(this.mIntentReceiver);
  }
  
  private void unregisterObserver() {
    if (this.mFormatChangeObserver != null) {
      ContentResolver contentResolver = getContext().getContentResolver();
      contentResolver.unregisterContentObserver(this.mFormatChangeObserver);
    } 
  }
  
  private void onTimeChanged() {
    this.mTime.setTimeInMillis(System.currentTimeMillis());
    setText(DateFormat.format(this.mFormat, this.mTime));
    setContentDescription(DateFormat.format(this.mDescFormat, this.mTime));
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    CharSequence charSequence1 = getFormat12Hour();
    CharSequence charSequence2 = null;
    if (charSequence1 == null) {
      charSequence1 = null;
    } else {
      charSequence1 = charSequence1.toString();
    } 
    paramViewHierarchyEncoder.addProperty("format12Hour", (String)charSequence1);
    charSequence1 = getFormat24Hour();
    if (charSequence1 == null) {
      charSequence1 = null;
    } else {
      charSequence1 = charSequence1.toString();
    } 
    paramViewHierarchyEncoder.addProperty("format24Hour", (String)charSequence1);
    charSequence1 = this.mFormat;
    if (charSequence1 == null) {
      charSequence1 = charSequence2;
    } else {
      charSequence1 = charSequence1.toString();
    } 
    paramViewHierarchyEncoder.addProperty("format", (String)charSequence1);
    paramViewHierarchyEncoder.addProperty("hasSeconds", this.mHasSeconds);
  }
}
