package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;

@Deprecated
public class ZoomControls extends LinearLayout {
  private final ZoomButton mZoomIn;
  
  private final ZoomButton mZoomOut;
  
  public ZoomControls(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ZoomControls(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    setFocusable(false);
    LayoutInflater layoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    layoutInflater.inflate(17367359, this, true);
    this.mZoomIn = findViewById(16909650);
    this.mZoomOut = findViewById(16909652);
  }
  
  public void setOnZoomInClickListener(View.OnClickListener paramOnClickListener) {
    this.mZoomIn.setOnClickListener(paramOnClickListener);
  }
  
  public void setOnZoomOutClickListener(View.OnClickListener paramOnClickListener) {
    this.mZoomOut.setOnClickListener(paramOnClickListener);
  }
  
  public void setZoomSpeed(long paramLong) {
    this.mZoomIn.setZoomSpeed(paramLong);
    this.mZoomOut.setZoomSpeed(paramLong);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    return true;
  }
  
  public void show() {
    fade(0, 0.0F, 1.0F);
  }
  
  public void hide() {
    fade(8, 1.0F, 0.0F);
  }
  
  private void fade(int paramInt, float paramFloat1, float paramFloat2) {
    AlphaAnimation alphaAnimation = new AlphaAnimation(paramFloat1, paramFloat2);
    alphaAnimation.setDuration(500L);
    startAnimation(alphaAnimation);
    setVisibility(paramInt);
  }
  
  public void setIsZoomInEnabled(boolean paramBoolean) {
    this.mZoomIn.setEnabled(paramBoolean);
  }
  
  public void setIsZoomOutEnabled(boolean paramBoolean) {
    this.mZoomOut.setEnabled(paramBoolean);
  }
  
  public boolean hasFocus() {
    return (this.mZoomIn.hasFocus() || this.mZoomOut.hasFocus());
  }
  
  public CharSequence getAccessibilityClassName() {
    return ZoomControls.class.getName();
  }
}
