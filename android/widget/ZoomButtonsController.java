package android.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewRootImpl;
import android.view.WindowManager;

@Deprecated
public class ZoomButtonsController implements View.OnTouchListener {
  private static final int ZOOM_CONTROLS_TIMEOUT = (int)ViewConfiguration.getZoomControlsTimeout();
  
  private boolean mAutoDismissControls = true;
  
  private final int[] mOwnerViewRawLocation = new int[2];
  
  private final int[] mContainerRawLocation = new int[2];
  
  private final int[] mTouchTargetWindowLocation = new int[2];
  
  private final Rect mTempRect = new Rect();
  
  private final int[] mTempIntArray = new int[2];
  
  private final IntentFilter mConfigurationChangedFilter = new IntentFilter("android.intent.action.CONFIGURATION_CHANGED");
  
  private final BroadcastReceiver mConfigurationChangedReceiver = new BroadcastReceiver() {
      final ZoomButtonsController this$0;
      
      public void onReceive(Context param1Context, Intent param1Intent) {
        if (!ZoomButtonsController.this.mIsVisible)
          return; 
        ZoomButtonsController.this.mHandler.removeMessages(2);
        ZoomButtonsController.this.mHandler.sendEmptyMessage(2);
      }
    };
  
  private final Handler mHandler = new Handler() {
      final ZoomButtonsController this$0;
      
      public void handleMessage(Message param1Message) {
        int i = param1Message.what;
        if (i != 2) {
          if (i != 3) {
            if (i == 4)
              if (ZoomButtonsController.this.mOwnerView.getWindowToken() == null) {
                Log.e("ZoomButtonsController", "Cannot make the zoom controller visible if the owner view is not attached to a window.");
              } else {
                ZoomButtonsController.this.setVisible(true);
              }  
          } else {
            ZoomButtonsController.this.setVisible(false);
          } 
        } else {
          ZoomButtonsController.this.onPostConfigurationChanged();
        } 
      }
    };
  
  private static final int MSG_DISMISS_ZOOM_CONTROLS = 3;
  
  private static final int MSG_POST_CONFIGURATION_CHANGED = 2;
  
  private static final int MSG_POST_SET_VISIBLE = 4;
  
  private static final String TAG = "ZoomButtonsController";
  
  private static final int ZOOM_CONTROLS_TOUCH_PADDING = 20;
  
  private OnZoomListener mCallback;
  
  private final FrameLayout mContainer;
  
  private WindowManager.LayoutParams mContainerLayoutParams;
  
  private final Context mContext;
  
  private ZoomControls mControls;
  
  private boolean mIsVisible;
  
  private final View mOwnerView;
  
  private Runnable mPostedVisibleInitializer;
  
  private boolean mReleaseTouchListenerOnUp;
  
  private int mTouchPaddingScaledSq;
  
  private View mTouchTargetView;
  
  private final WindowManager mWindowManager;
  
  public ZoomButtonsController(View paramView) {
    Context context2 = paramView.getContext();
    this.mWindowManager = (WindowManager)context2.getSystemService("window");
    this.mOwnerView = paramView;
    Context context1 = this.mContext;
    int i = (int)((context1.getResources().getDisplayMetrics()).density * 20.0F);
    this.mTouchPaddingScaledSq = i * i;
    this.mContainer = createContainer();
  }
  
  public void setZoomInEnabled(boolean paramBoolean) {
    this.mControls.setIsZoomInEnabled(paramBoolean);
  }
  
  public void setZoomOutEnabled(boolean paramBoolean) {
    this.mControls.setIsZoomOutEnabled(paramBoolean);
  }
  
  public void setZoomSpeed(long paramLong) {
    this.mControls.setZoomSpeed(paramLong);
  }
  
  private FrameLayout createContainer() {
    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-2, -2);
    layoutParams.gravity = 8388659;
    layoutParams.flags = 131608;
    layoutParams.height = -2;
    layoutParams.width = -1;
    layoutParams.type = 1000;
    layoutParams.format = -3;
    layoutParams.windowAnimations = 16974607;
    this.mContainerLayoutParams = layoutParams;
    Container container = new Container(this.mContext);
    container.setLayoutParams(layoutParams);
    container.setMeasureAllChildren(true);
    Context context = this.mContext;
    LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
    layoutInflater.inflate(17367358, container);
    ZoomControls zoomControls = container.<ZoomControls>findViewById(16909649);
    zoomControls.setOnZoomInClickListener(new View.OnClickListener() {
          final ZoomButtonsController this$0;
          
          public void onClick(View param1View) {
            ZoomButtonsController.this.dismissControlsDelayed(ZoomButtonsController.ZOOM_CONTROLS_TIMEOUT);
            if (ZoomButtonsController.this.mCallback != null)
              ZoomButtonsController.this.mCallback.onZoom(true); 
          }
        });
    this.mControls.setOnZoomOutClickListener(new View.OnClickListener() {
          final ZoomButtonsController this$0;
          
          public void onClick(View param1View) {
            ZoomButtonsController.this.dismissControlsDelayed(ZoomButtonsController.ZOOM_CONTROLS_TIMEOUT);
            if (ZoomButtonsController.this.mCallback != null)
              ZoomButtonsController.this.mCallback.onZoom(false); 
          }
        });
    return container;
  }
  
  public void setOnZoomListener(OnZoomListener paramOnZoomListener) {
    this.mCallback = paramOnZoomListener;
  }
  
  public void setFocusable(boolean paramBoolean) {
    int i = this.mContainerLayoutParams.flags;
    if (paramBoolean) {
      WindowManager.LayoutParams layoutParams = this.mContainerLayoutParams;
      layoutParams.flags &= 0xFFFFFFF7;
    } else {
      WindowManager.LayoutParams layoutParams = this.mContainerLayoutParams;
      layoutParams.flags |= 0x8;
    } 
    if (this.mContainerLayoutParams.flags != i && this.mIsVisible)
      this.mWindowManager.updateViewLayout(this.mContainer, this.mContainerLayoutParams); 
  }
  
  public boolean isAutoDismissed() {
    return this.mAutoDismissControls;
  }
  
  public void setAutoDismissed(boolean paramBoolean) {
    if (this.mAutoDismissControls == paramBoolean)
      return; 
    this.mAutoDismissControls = paramBoolean;
  }
  
  public boolean isVisible() {
    return this.mIsVisible;
  }
  
  public void setVisible(boolean paramBoolean) {
    if (paramBoolean) {
      if (this.mOwnerView.getWindowToken() == null) {
        if (!this.mHandler.hasMessages(4))
          this.mHandler.sendEmptyMessage(4); 
        return;
      } 
      dismissControlsDelayed(ZOOM_CONTROLS_TIMEOUT);
    } 
    if (this.mIsVisible == paramBoolean)
      return; 
    this.mIsVisible = paramBoolean;
    if (paramBoolean) {
      if (this.mContainerLayoutParams.token == null)
        this.mContainerLayoutParams.token = this.mOwnerView.getWindowToken(); 
      this.mWindowManager.addView(this.mContainer, this.mContainerLayoutParams);
      if (this.mPostedVisibleInitializer == null)
        this.mPostedVisibleInitializer = (Runnable)new Object(this); 
      this.mHandler.post(this.mPostedVisibleInitializer);
      this.mContext.registerReceiver(this.mConfigurationChangedReceiver, this.mConfigurationChangedFilter);
      this.mOwnerView.setOnTouchListener(this);
      this.mReleaseTouchListenerOnUp = false;
    } else {
      if (this.mTouchTargetView != null) {
        this.mReleaseTouchListenerOnUp = true;
      } else {
        this.mOwnerView.setOnTouchListener(null);
      } 
      this.mContext.unregisterReceiver(this.mConfigurationChangedReceiver);
      this.mWindowManager.removeViewImmediate(this.mContainer);
      this.mHandler.removeCallbacks(this.mPostedVisibleInitializer);
      OnZoomListener onZoomListener = this.mCallback;
      if (onZoomListener != null)
        onZoomListener.onVisibilityChanged(false); 
    } 
  }
  
  public ViewGroup getContainer() {
    return this.mContainer;
  }
  
  public View getZoomControls() {
    return this.mControls;
  }
  
  private void dismissControlsDelayed(int paramInt) {
    if (this.mAutoDismissControls) {
      this.mHandler.removeMessages(3);
      this.mHandler.sendEmptyMessageDelayed(3, paramInt);
    } 
  }
  
  private void refreshPositioningVariables() {
    if (this.mOwnerView.getWindowToken() == null)
      return; 
    int i = this.mOwnerView.getHeight();
    int j = this.mOwnerView.getWidth();
    i -= this.mContainer.getHeight();
    this.mOwnerView.getLocationOnScreen(this.mOwnerViewRawLocation);
    int[] arrayOfInt1 = this.mContainerRawLocation, arrayOfInt2 = this.mOwnerViewRawLocation;
    arrayOfInt1[0] = arrayOfInt2[0];
    arrayOfInt1[1] = arrayOfInt2[1] + i;
    arrayOfInt1 = this.mTempIntArray;
    this.mOwnerView.getLocationInWindow(arrayOfInt1);
    this.mContainerLayoutParams.x = arrayOfInt1[0];
    this.mContainerLayoutParams.width = j;
    this.mContainerLayoutParams.y = arrayOfInt1[1] + i;
    if (this.mIsVisible)
      this.mWindowManager.updateViewLayout(this.mContainer, this.mContainerLayoutParams); 
  }
  
  private boolean onContainerKey(KeyEvent paramKeyEvent) {
    int i = paramKeyEvent.getKeyCode();
    if (isInterestingKey(i)) {
      if (i == 4) {
        if (paramKeyEvent.getAction() == 0 && 
          paramKeyEvent.getRepeatCount() == 0) {
          View view = this.mOwnerView;
          if (view != null) {
            KeyEvent.DispatcherState dispatcherState = view.getKeyDispatcherState();
            if (dispatcherState != null)
              dispatcherState.startTracking(paramKeyEvent, this); 
          } 
          return true;
        } 
        if (paramKeyEvent.getAction() == 1 && 
          paramKeyEvent.isTracking() && !paramKeyEvent.isCanceled()) {
          setVisible(false);
          return true;
        } 
      } else {
        dismissControlsDelayed(ZOOM_CONTROLS_TIMEOUT);
      } 
      return false;
    } 
    ViewRootImpl viewRootImpl = this.mOwnerView.getViewRootImpl();
    if (viewRootImpl != null)
      viewRootImpl.dispatchInputEvent(paramKeyEvent); 
    return true;
  }
  
  private boolean isInterestingKey(int paramInt) {
    if (paramInt != 4 && paramInt != 66)
      switch (paramInt) {
        default:
          return false;
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
          break;
      }  
    return true;
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    if (paramMotionEvent.getPointerCount() > 1)
      return false; 
    if (this.mReleaseTouchListenerOnUp) {
      if (i == 1 || i == 3) {
        this.mOwnerView.setOnTouchListener(null);
        setTouchTargetView(null);
        this.mReleaseTouchListenerOnUp = false;
      } 
      return true;
    } 
    dismissControlsDelayed(ZOOM_CONTROLS_TIMEOUT);
    paramView = this.mTouchTargetView;
    if (i != 0) {
      if (i == 1 || i == 3)
        setTouchTargetView(null); 
    } else {
      paramView = findViewForTouch((int)paramMotionEvent.getRawX(), (int)paramMotionEvent.getRawY());
      setTouchTargetView(paramView);
    } 
    if (paramView != null) {
      int arrayOfInt1[] = this.mContainerRawLocation, j = arrayOfInt1[0], arrayOfInt2[] = this.mTouchTargetWindowLocation, k = arrayOfInt2[0];
      int m = arrayOfInt1[1];
      i = arrayOfInt2[1];
      paramMotionEvent = MotionEvent.obtain(paramMotionEvent);
      arrayOfInt2 = this.mOwnerViewRawLocation;
      paramMotionEvent.offsetLocation((arrayOfInt2[0] - j + k), (arrayOfInt2[1] - m + i));
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      if (f1 < 0.0F && f1 > -20.0F)
        paramMotionEvent.offsetLocation(-f1, 0.0F); 
      if (f2 < 0.0F && f2 > -20.0F)
        paramMotionEvent.offsetLocation(0.0F, -f2); 
      boolean bool = paramView.dispatchTouchEvent(paramMotionEvent);
      paramMotionEvent.recycle();
      return bool;
    } 
    return false;
  }
  
  private void setTouchTargetView(View paramView) {
    this.mTouchTargetView = paramView;
    if (paramView != null)
      paramView.getLocationInWindow(this.mTouchTargetWindowLocation); 
  }
  
  private View findViewForTouch(int paramInt1, int paramInt2) {
    int arrayOfInt[] = this.mContainerRawLocation, i = paramInt1 - arrayOfInt[0];
    int j = paramInt2 - arrayOfInt[1];
    Rect rect = this.mTempRect;
    arrayOfInt = null;
    paramInt2 = Integer.MAX_VALUE;
    View view;
    for (paramInt1 = this.mContainer.getChildCount() - 1; paramInt1 >= 0; paramInt1--, view = view2, paramInt2 = k) {
      View view2;
      int k;
      View view1 = this.mContainer.getChildAt(paramInt1);
      if (view1.getVisibility() != 0) {
        int[] arrayOfInt1 = arrayOfInt;
        k = paramInt2;
      } else {
        view1.getHitRect(rect);
        if (rect.contains(i, j))
          return view1; 
        if (i >= rect.left && i <= rect.right) {
          k = 0;
        } else {
          k = Math.abs(rect.left - i);
          m = rect.right;
          m = Math.abs(i - m);
          k = Math.min(k, m);
        } 
        if (j >= rect.top && j <= rect.bottom) {
          m = 0;
        } else {
          m = Math.abs(rect.top - j);
          int n = rect.bottom;
          n = Math.abs(j - n);
          m = Math.min(m, n);
        } 
        int m = k * k + m * m;
        int[] arrayOfInt1 = arrayOfInt;
        k = paramInt2;
        if (m < this.mTouchPaddingScaledSq) {
          arrayOfInt1 = arrayOfInt;
          k = paramInt2;
          if (m < paramInt2) {
            view2 = view1;
            k = m;
          } 
        } 
      } 
    } 
    return view;
  }
  
  private void onPostConfigurationChanged() {
    dismissControlsDelayed(ZOOM_CONTROLS_TIMEOUT);
    refreshPositioningVariables();
  }
  
  class Container extends FrameLayout {
    final ZoomButtonsController this$0;
    
    public Container(Context param1Context) {
      super(param1Context);
    }
    
    public boolean dispatchKeyEvent(KeyEvent param1KeyEvent) {
      boolean bool;
      if (ZoomButtonsController.this.onContainerKey(param1KeyEvent)) {
        bool = true;
      } else {
        bool = super.dispatchKeyEvent(param1KeyEvent);
      } 
      return bool;
    }
  }
  
  class OnZoomListener {
    public abstract void onVisibilityChanged(boolean param1Boolean);
    
    public abstract void onZoom(boolean param1Boolean);
  }
}
