package android.widget;

import android.content.res.ColorStateList;
import android.text.TextUtils;

class ColorInjector {
  static class TextView {
    private static final String[] VIEWS_SPECIFIED_COLORS = new String[] { "com.alipay.android.app.ui.quickpay.widget.CustomPasswordEditText", "com.alipay.android.app.ui.quickpay.widget.CustomEditText", null };
    
    static ColorStateList getSpecifiedColors(TextView param1TextView, ColorStateList param1ColorStateList) {
      if (isViewSpecifiedColors(param1TextView.getClass().getName()))
        param1ColorStateList = ColorStateList.valueOf(param1TextView.getResources().getColor(17170435)); 
      return param1ColorStateList;
    }
    
    private static boolean isViewSpecifiedColors(String param1String) {
      for (String str : VIEWS_SPECIFIED_COLORS) {
        if (!TextUtils.isEmpty(str))
          if (str.equals(param1String))
            return true;  
      } 
      return false;
    }
  }
}
