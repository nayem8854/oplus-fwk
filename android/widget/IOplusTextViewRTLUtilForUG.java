package android.widget;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.Layout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;

public interface IOplusTextViewRTLUtilForUG extends IOplusCommonFeature {
  public static final IOplusTextViewRTLUtilForUG DEFAULT = (IOplusTextViewRTLUtilForUG)new Object();
  
  default IOplusTextViewRTLUtilForUG getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusTextViewRTLUtilForUG;
  }
  
  default Layout.Alignment getLayoutAlignmentForTextView(Layout.Alignment paramAlignment, Context paramContext, TextView paramTextView) {
    return paramAlignment;
  }
  
  default TextDirectionHeuristic getTextDirectionHeuristicForTextView(boolean paramBoolean) {
    TextDirectionHeuristic textDirectionHeuristic;
    if (paramBoolean) {
      textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_RTL;
    } else {
      textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
    } 
    return textDirectionHeuristic;
  }
  
  default void initRtlParameter(Resources paramResources) {}
  
  default boolean getOplusSupportRtl() {
    return false;
  }
  
  default boolean getDirectionAnyRtl() {
    return false;
  }
  
  default boolean getTextViewStart() {
    return false;
  }
  
  default boolean hasRtlSupportForView(Context paramContext) {
    boolean bool;
    if (paramContext != null && paramContext.getApplicationInfo().hasRtlSupport()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  default void updateRtlParameterForUG(String[] paramArrayOfString, Configuration paramConfiguration) {}
  
  @Deprecated
  default void updateRtlParameterForUG(Resources paramResources, Configuration paramConfiguration) {}
}
