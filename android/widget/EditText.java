package android.widget;

import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.MovementMethod;
import android.util.AttributeSet;

public class EditText extends TextView {
  public EditText(Context paramContext) {
    this(paramContext, null);
  }
  
  public EditText(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842862);
  }
  
  public EditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public EditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public boolean getFreezesText() {
    return true;
  }
  
  protected boolean getDefaultEditable() {
    return true;
  }
  
  protected MovementMethod getDefaultMovementMethod() {
    return ArrowKeyMovementMethod.getInstance();
  }
  
  public Editable getText() {
    CharSequence charSequence = super.getText();
    if (charSequence == null)
      return null; 
    if (charSequence instanceof Editable)
      return (Editable)super.getText(); 
    super.setText(charSequence, TextView.BufferType.EDITABLE);
    return (Editable)super.getText();
  }
  
  public void setText(CharSequence paramCharSequence, TextView.BufferType paramBufferType) {
    super.setText(paramCharSequence, TextView.BufferType.EDITABLE);
  }
  
  public void setSelection(int paramInt1, int paramInt2) {
    Selection.setSelection(getText(), paramInt1, paramInt2);
  }
  
  public void setSelection(int paramInt) {
    Selection.setSelection(getText(), paramInt);
  }
  
  public void selectAll() {
    Selection.selectAll(getText());
  }
  
  public void extendSelection(int paramInt) {
    Selection.extendSelection(getText(), paramInt);
  }
  
  public void setEllipsize(TextUtils.TruncateAt paramTruncateAt) {
    if (paramTruncateAt != TextUtils.TruncateAt.MARQUEE) {
      super.setEllipsize(paramTruncateAt);
      return;
    } 
    throw new IllegalArgumentException("EditText cannot use the ellipsize mode TextUtils.TruncateAt.MARQUEE");
  }
  
  public CharSequence getAccessibilityClassName() {
    return EditText.class.getName();
  }
  
  protected boolean supportsAutoSizeText() {
    return false;
  }
}
