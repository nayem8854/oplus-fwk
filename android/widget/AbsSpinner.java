package android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.autofill.AutofillValue;
import com.android.internal.R;

public abstract class AbsSpinner extends AdapterView<SpinnerAdapter> {
  private static final String LOG_TAG = AbsSpinner.class.getSimpleName();
  
  int mSelectionLeftPadding = 0;
  
  int mSelectionTopPadding = 0;
  
  int mSelectionRightPadding = 0;
  
  int mSelectionBottomPadding = 0;
  
  final Rect mSpinnerPadding = new Rect();
  
  final RecycleBin mRecycler = new RecycleBin();
  
  SpinnerAdapter mAdapter;
  
  private DataSetObserver mDataSetObserver;
  
  int mHeightMeasureSpec;
  
  private Rect mTouchFrame;
  
  int mWidthMeasureSpec;
  
  public AbsSpinner(Context paramContext) {
    super(paramContext);
    initAbsSpinner();
  }
  
  public AbsSpinner(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public AbsSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AbsSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    if (getImportantForAutofill() == 0)
      setImportantForAutofill(1); 
    initAbsSpinner();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AbsSpinner, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.AbsSpinner, paramAttributeSet, typedArray, paramInt1, paramInt2);
    CharSequence[] arrayOfCharSequence = typedArray.getTextArray(0);
    if (arrayOfCharSequence != null) {
      ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter<>(paramContext, 17367048, arrayOfCharSequence);
      arrayAdapter.setDropDownViewResource(17367049);
      setAdapter(arrayAdapter);
    } 
    typedArray.recycle();
  }
  
  private void initAbsSpinner() {
    setFocusable(true);
    setWillNotDraw(false);
  }
  
  public void setAdapter(SpinnerAdapter paramSpinnerAdapter) {
    SpinnerAdapter spinnerAdapter = this.mAdapter;
    if (spinnerAdapter != null) {
      spinnerAdapter.unregisterDataSetObserver(this.mDataSetObserver);
      resetList();
    } 
    this.mAdapter = paramSpinnerAdapter;
    byte b = -1;
    this.mOldSelectedPosition = -1;
    this.mOldSelectedRowId = Long.MIN_VALUE;
    if (this.mAdapter != null) {
      this.mOldItemCount = this.mItemCount;
      this.mItemCount = this.mAdapter.getCount();
      checkFocus();
      AdapterView.AdapterDataSetObserver adapterDataSetObserver = new AdapterView.AdapterDataSetObserver(this);
      this.mAdapter.registerDataSetObserver(adapterDataSetObserver);
      if (this.mItemCount > 0)
        b = 0; 
      setSelectedPositionInt(b);
      setNextSelectedPositionInt(b);
      if (this.mItemCount == 0)
        checkSelectionChanged(); 
    } else {
      checkFocus();
      resetList();
      checkSelectionChanged();
    } 
    requestLayout();
  }
  
  void resetList() {
    this.mDataChanged = false;
    this.mNeedSync = false;
    removeAllViewsInLayout();
    this.mOldSelectedPosition = -1;
    this.mOldSelectedRowId = Long.MIN_VALUE;
    setSelectedPositionInt(-1);
    setNextSelectedPositionInt(-1);
    invalidate();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getMode(paramInt1);
    Rect rect = this.mSpinnerPadding;
    int j = this.mPaddingLeft, k = this.mSelectionLeftPadding;
    if (j > k)
      k = this.mPaddingLeft; 
    rect.left = k;
    rect = this.mSpinnerPadding;
    j = this.mPaddingTop;
    k = this.mSelectionTopPadding;
    if (j > k)
      k = this.mPaddingTop; 
    rect.top = k;
    rect = this.mSpinnerPadding;
    j = this.mPaddingRight;
    k = this.mSelectionRightPadding;
    if (j > k)
      k = this.mPaddingRight; 
    rect.right = k;
    rect = this.mSpinnerPadding;
    j = this.mPaddingBottom;
    k = this.mSelectionBottomPadding;
    if (j > k)
      k = this.mPaddingBottom; 
    rect.bottom = k;
    if (this.mDataChanged)
      handleDataChanged(); 
    int m = 0;
    boolean bool = false;
    byte b = 1;
    int n = getSelectedItemPosition();
    j = m;
    k = bool;
    int i1 = b;
    if (n >= 0) {
      SpinnerAdapter spinnerAdapter = this.mAdapter;
      j = m;
      k = bool;
      i1 = b;
      if (spinnerAdapter != null) {
        j = m;
        k = bool;
        i1 = b;
        if (n < spinnerAdapter.getCount()) {
          View view2 = this.mRecycler.get(n);
          View view1 = view2;
          if (view2 == null) {
            view2 = this.mAdapter.getView(n, null, this);
            view1 = view2;
            if (view2.getImportantForAccessibility() == 0) {
              view2.setImportantForAccessibility(1);
              view1 = view2;
            } 
          } 
          j = m;
          k = bool;
          i1 = b;
          if (view1 != null) {
            this.mRecycler.put(n, view1);
            if (view1.getLayoutParams() == null) {
              this.mBlockLayoutRequests = true;
              view1.setLayoutParams(generateDefaultLayoutParams());
              this.mBlockLayoutRequests = false;
            } 
            measureChild(view1, paramInt1, paramInt2);
            j = getChildHeight(view1) + this.mSpinnerPadding.top + this.mSpinnerPadding.bottom;
            k = getChildWidth(view1) + this.mSpinnerPadding.left + this.mSpinnerPadding.right;
            i1 = 0;
          } 
        } 
      } 
    } 
    m = j;
    j = k;
    if (i1) {
      i1 = this.mSpinnerPadding.top + this.mSpinnerPadding.bottom;
      m = i1;
      j = k;
      if (i == 0) {
        j = this.mSpinnerPadding.left + this.mSpinnerPadding.right;
        m = i1;
      } 
    } 
    i1 = Math.max(m, getSuggestedMinimumHeight());
    k = Math.max(j, getSuggestedMinimumWidth());
    j = resolveSizeAndState(i1, paramInt2, 0);
    k = resolveSizeAndState(k, paramInt1, 0);
    setMeasuredDimension(k, j);
    this.mHeightMeasureSpec = paramInt2;
    this.mWidthMeasureSpec = paramInt1;
  }
  
  int getChildHeight(View paramView) {
    return paramView.getMeasuredHeight();
  }
  
  int getChildWidth(View paramView) {
    return paramView.getMeasuredWidth();
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return new ViewGroup.LayoutParams(-1, -2);
  }
  
  void recycleAllViews() {
    int i = getChildCount();
    RecycleBin recycleBin = this.mRecycler;
    int j = this.mFirstPosition;
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      recycleBin.put(j + b, view);
    } 
  }
  
  public void setSelection(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: iload_2
    //   3: ifeq -> 38
    //   6: aload_0
    //   7: getfield mFirstPosition : I
    //   10: iload_1
    //   11: if_icmpgt -> 38
    //   14: aload_0
    //   15: getfield mFirstPosition : I
    //   18: istore #4
    //   20: iload_1
    //   21: iload #4
    //   23: aload_0
    //   24: invokevirtual getChildCount : ()I
    //   27: iadd
    //   28: iconst_1
    //   29: isub
    //   30: if_icmpgt -> 38
    //   33: iload_3
    //   34: istore_2
    //   35: goto -> 40
    //   38: iconst_0
    //   39: istore_2
    //   40: aload_0
    //   41: iload_1
    //   42: iload_2
    //   43: invokevirtual setSelectionInt : (IZ)V
    //   46: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #283	-> 0
    //   #284	-> 20
    //   #285	-> 40
    //   #286	-> 46
  }
  
  public void setSelection(int paramInt) {
    setNextSelectedPositionInt(paramInt);
    requestLayout();
    invalidate();
  }
  
  void setSelectionInt(int paramInt, boolean paramBoolean) {
    if (paramInt != this.mOldSelectedPosition) {
      this.mBlockLayoutRequests = true;
      int i = this.mSelectedPosition;
      setNextSelectedPositionInt(paramInt);
      layout(paramInt - i, paramBoolean);
      this.mBlockLayoutRequests = false;
    } 
  }
  
  public View getSelectedView() {
    if (this.mItemCount > 0 && this.mSelectedPosition >= 0)
      return getChildAt(this.mSelectedPosition - this.mFirstPosition); 
    return null;
  }
  
  public void requestLayout() {
    if (!this.mBlockLayoutRequests)
      super.requestLayout(); 
  }
  
  public SpinnerAdapter getAdapter() {
    return this.mAdapter;
  }
  
  public int getCount() {
    return this.mItemCount;
  }
  
  public int pointToPosition(int paramInt1, int paramInt2) {
    Rect rect1 = this.mTouchFrame;
    Rect rect2 = rect1;
    if (rect1 == null) {
      this.mTouchFrame = new Rect();
      rect2 = this.mTouchFrame;
    } 
    int i = getChildCount();
    for (; --i >= 0; i--) {
      View view = getChildAt(i);
      if (view.getVisibility() == 0) {
        view.getHitRect(rect2);
        if (rect2.contains(paramInt1, paramInt2))
          return this.mFirstPosition + i; 
      } 
    } 
    return -1;
  }
  
  protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray) {
    super.dispatchRestoreInstanceState(paramSparseArray);
    handleDataChanged();
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState(AbsSpinner this$0) {
      super((Parcelable)this$0);
    }
    
    SavedState(AbsSpinner this$0) {
      super((Parcel)this$0);
      this.selectedId = this$0.readLong();
      this.position = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeLong(this.selectedId);
      param1Parcel.writeInt(this.position);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AbsSpinner.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" selectedId=");
      stringBuilder.append(this.selectedId);
      stringBuilder.append(" position=");
      stringBuilder.append(this.position);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    int position;
    
    long selectedId;
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    parcelable = new SavedState(parcelable);
    ((SavedState)parcelable).selectedId = getSelectedItemId();
    if (((SavedState)parcelable).selectedId >= 0L) {
      ((SavedState)parcelable).position = getSelectedItemPosition();
    } else {
      ((SavedState)parcelable).position = -1;
    } 
    return parcelable;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    paramParcelable = paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    if (((SavedState)paramParcelable).selectedId >= 0L) {
      this.mDataChanged = true;
      this.mNeedSync = true;
      this.mSyncRowId = ((SavedState)paramParcelable).selectedId;
      this.mSyncPosition = ((SavedState)paramParcelable).position;
      this.mSyncMode = 0;
      requestLayout();
    } 
  }
  
  class RecycleBin {
    private final SparseArray<View> mScrapHeap = new SparseArray<>();
    
    final AbsSpinner this$0;
    
    public void put(int param1Int, View param1View) {
      this.mScrapHeap.put(param1Int, param1View);
    }
    
    View get(int param1Int) {
      View view = this.mScrapHeap.get(param1Int);
      if (view != null)
        this.mScrapHeap.delete(param1Int); 
      return view;
    }
    
    void clear() {
      SparseArray<View> sparseArray = this.mScrapHeap;
      int i = sparseArray.size();
      for (byte b = 0; b < i; b++) {
        View view = sparseArray.valueAt(b);
        if (view != null)
          AbsSpinner.this.removeDetachedView(view, true); 
      } 
      sparseArray.clear();
    }
  }
  
  public CharSequence getAccessibilityClassName() {
    return AbsSpinner.class.getName();
  }
  
  public void autofill(AutofillValue paramAutofillValue) {
    if (!isEnabled())
      return; 
    if (!paramAutofillValue.isList()) {
      String str = LOG_TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramAutofillValue);
      stringBuilder.append(" could not be autofilled into ");
      stringBuilder.append(this);
      Log.w(str, stringBuilder.toString());
      return;
    } 
    setSelection(paramAutofillValue.getListValue());
  }
  
  public int getAutofillType() {
    boolean bool;
    if (isEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public AutofillValue getAutofillValue() {
    AutofillValue autofillValue;
    if (isEnabled()) {
      autofillValue = AutofillValue.forList(getSelectedItemPosition());
    } else {
      autofillValue = null;
    } 
    return autofillValue;
  }
  
  abstract void layout(int paramInt, boolean paramBoolean);
}
