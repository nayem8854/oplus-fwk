package android.view;

import android.graphics.GraphicBuffer;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;

public class AppTransitionAnimationSpec implements Parcelable {
  public AppTransitionAnimationSpec(int paramInt, GraphicBuffer paramGraphicBuffer, Rect paramRect) {
    this.taskId = paramInt;
    this.rect = paramRect;
    this.buffer = paramGraphicBuffer;
  }
  
  public AppTransitionAnimationSpec(Parcel paramParcel) {
    this.taskId = paramParcel.readInt();
    this.rect = (Rect)paramParcel.readParcelable(null);
    this.buffer = (GraphicBuffer)paramParcel.readParcelable(null);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.taskId);
    paramParcel.writeParcelable((Parcelable)this.rect, 0);
    paramParcel.writeParcelable((Parcelable)this.buffer, 0);
  }
  
  public static final Parcelable.Creator<AppTransitionAnimationSpec> CREATOR = (Parcelable.Creator<AppTransitionAnimationSpec>)new Object();
  
  public final GraphicBuffer buffer;
  
  public final Rect rect;
  
  public final int taskId;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{taskId: ");
    stringBuilder.append(this.taskId);
    stringBuilder.append(", buffer: ");
    stringBuilder.append(this.buffer);
    stringBuilder.append(", rect: ");
    stringBuilder.append(this.rect);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
