package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IInputFilter extends IInterface {
  void filterInputEvent(InputEvent paramInputEvent, int paramInt) throws RemoteException;
  
  void install(IInputFilterHost paramIInputFilterHost) throws RemoteException;
  
  void uninstall() throws RemoteException;
  
  class Default implements IInputFilter {
    public void install(IInputFilterHost param1IInputFilterHost) throws RemoteException {}
    
    public void uninstall() throws RemoteException {}
    
    public void filterInputEvent(InputEvent param1InputEvent, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputFilter {
    private static final String DESCRIPTOR = "android.view.IInputFilter";
    
    static final int TRANSACTION_filterInputEvent = 3;
    
    static final int TRANSACTION_install = 1;
    
    static final int TRANSACTION_uninstall = 2;
    
    public Stub() {
      attachInterface(this, "android.view.IInputFilter");
    }
    
    public static IInputFilter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IInputFilter");
      if (iInterface != null && iInterface instanceof IInputFilter)
        return (IInputFilter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "filterInputEvent";
        } 
        return "uninstall";
      } 
      return "install";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.view.IInputFilter");
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.IInputFilter");
          if (param1Parcel1.readInt() != 0) {
            InputEvent inputEvent = (InputEvent)InputEvent.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          param1Int1 = param1Parcel1.readInt();
          filterInputEvent((InputEvent)param1Parcel2, param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IInputFilter");
        uninstall();
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IInputFilter");
      IInputFilterHost iInputFilterHost = IInputFilterHost.Stub.asInterface(param1Parcel1.readStrongBinder());
      install(iInputFilterHost);
      return true;
    }
    
    private static class Proxy implements IInputFilter {
      public static IInputFilter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IInputFilter";
      }
      
      public void install(IInputFilterHost param2IInputFilterHost) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IInputFilter");
          if (param2IInputFilterHost != null) {
            iBinder = param2IInputFilterHost.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInputFilter.Stub.getDefaultImpl() != null) {
            IInputFilter.Stub.getDefaultImpl().install(param2IInputFilterHost);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void uninstall() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IInputFilter");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInputFilter.Stub.getDefaultImpl() != null) {
            IInputFilter.Stub.getDefaultImpl().uninstall();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void filterInputEvent(InputEvent param2InputEvent, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IInputFilter");
          if (param2InputEvent != null) {
            parcel.writeInt(1);
            param2InputEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IInputFilter.Stub.getDefaultImpl() != null) {
            IInputFilter.Stub.getDefaultImpl().filterInputEvent(param2InputEvent, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputFilter param1IInputFilter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputFilter != null) {
          Proxy.sDefaultImpl = param1IInputFilter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputFilter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
