package android.view;

import android.os.CancellationSignal;
import android.view.animation.Interpolator;
import java.util.ArrayList;

public class PendingInsetsController implements WindowInsetsController {
  private final ArrayList<PendingRequest> mRequests = new ArrayList<>();
  
  private int mBehavior = -1;
  
  private final InsetsState mDummyState = new InsetsState();
  
  private ArrayList<WindowInsetsController.OnControllableInsetsChangedListener> mControllableInsetsChangedListeners = new ArrayList<>();
  
  private int mCaptionInsetsHeight = 0;
  
  private static final int KEEP_BEHAVIOR = -1;
  
  private boolean mAnimationsDisabled;
  
  private int mAppearance;
  
  private int mAppearanceMask;
  
  private InsetsController mReplayedInsetsController;
  
  public void show(int paramInt) {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null) {
      insetsController.show(paramInt);
    } else {
      this.mRequests.add(new ShowRequest(paramInt));
    } 
  }
  
  public void hide(int paramInt) {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null) {
      insetsController.hide(paramInt);
    } else {
      this.mRequests.add(new HideRequest(paramInt));
    } 
  }
  
  public void setSystemBarsAppearance(int paramInt1, int paramInt2) {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null) {
      insetsController.setSystemBarsAppearance(paramInt1, paramInt2);
    } else {
      this.mAppearance = this.mAppearance & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2;
      this.mAppearanceMask |= paramInt2;
    } 
  }
  
  public int getSystemBarsAppearance() {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null)
      return insetsController.getSystemBarsAppearance(); 
    return this.mAppearance;
  }
  
  public void setCaptionInsetsHeight(int paramInt) {
    this.mCaptionInsetsHeight = paramInt;
  }
  
  public void setSystemBarsBehavior(int paramInt) {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null) {
      insetsController.setSystemBarsBehavior(paramInt);
    } else {
      this.mBehavior = paramInt;
    } 
  }
  
  public int getSystemBarsBehavior() {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null)
      return insetsController.getSystemBarsBehavior(); 
    return this.mBehavior;
  }
  
  public void setAnimationsDisabled(boolean paramBoolean) {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null) {
      insetsController.setAnimationsDisabled(paramBoolean);
    } else {
      this.mAnimationsDisabled = paramBoolean;
    } 
  }
  
  public InsetsState getState() {
    return this.mDummyState;
  }
  
  public boolean isRequestedVisible(int paramInt) {
    return InsetsState.getDefaultVisibility(paramInt);
  }
  
  public void addOnControllableInsetsChangedListener(WindowInsetsController.OnControllableInsetsChangedListener paramOnControllableInsetsChangedListener) {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null) {
      insetsController.addOnControllableInsetsChangedListener(paramOnControllableInsetsChangedListener);
    } else {
      this.mControllableInsetsChangedListeners.add(paramOnControllableInsetsChangedListener);
      paramOnControllableInsetsChangedListener.onControllableInsetsChanged(this, 0);
    } 
  }
  
  public void removeOnControllableInsetsChangedListener(WindowInsetsController.OnControllableInsetsChangedListener paramOnControllableInsetsChangedListener) {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null) {
      insetsController.removeOnControllableInsetsChangedListener(paramOnControllableInsetsChangedListener);
    } else {
      this.mControllableInsetsChangedListeners.remove(paramOnControllableInsetsChangedListener);
    } 
  }
  
  public void replayAndAttach(InsetsController paramInsetsController) {
    int i = this.mBehavior;
    if (i != -1)
      paramInsetsController.setSystemBarsBehavior(i); 
    i = this.mAppearanceMask;
    if (i != 0)
      paramInsetsController.setSystemBarsAppearance(this.mAppearance, i); 
    i = this.mCaptionInsetsHeight;
    if (i != 0)
      paramInsetsController.setCaptionInsetsHeight(i); 
    if (this.mAnimationsDisabled)
      paramInsetsController.setAnimationsDisabled(true); 
    int j = this.mRequests.size();
    for (i = 0; i < j; i++)
      ((PendingRequest)this.mRequests.get(i)).replay(paramInsetsController); 
    j = this.mControllableInsetsChangedListeners.size();
    for (i = 0; i < j; i++) {
      ArrayList<WindowInsetsController.OnControllableInsetsChangedListener> arrayList = this.mControllableInsetsChangedListeners;
      WindowInsetsController.OnControllableInsetsChangedListener onControllableInsetsChangedListener = arrayList.get(i);
      paramInsetsController.addOnControllableInsetsChangedListener(onControllableInsetsChangedListener);
    } 
    this.mRequests.clear();
    this.mControllableInsetsChangedListeners.clear();
    this.mBehavior = -1;
    this.mAppearance = 0;
    this.mAppearanceMask = 0;
    this.mAnimationsDisabled = false;
    this.mReplayedInsetsController = paramInsetsController;
  }
  
  public void detach() {
    this.mReplayedInsetsController = null;
  }
  
  public void controlWindowInsetsAnimation(int paramInt, long paramLong, Interpolator paramInterpolator, CancellationSignal paramCancellationSignal, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener) {
    InsetsController insetsController = this.mReplayedInsetsController;
    if (insetsController != null) {
      insetsController.controlWindowInsetsAnimation(paramInt, paramLong, paramInterpolator, paramCancellationSignal, paramWindowInsetsAnimationControlListener);
    } else {
      paramWindowInsetsAnimationControlListener.onCancelled(null);
    } 
  }
  
  private static class ShowRequest implements PendingRequest {
    private final int mTypes;
    
    public ShowRequest(int param1Int) {
      this.mTypes = param1Int;
    }
    
    public void replay(InsetsController param1InsetsController) {
      param1InsetsController.show(this.mTypes);
    }
  }
  
  class PendingRequest {
    public abstract void replay(InsetsController param1InsetsController);
  }
  
  private static class HideRequest implements PendingRequest {
    private final int mTypes;
    
    public HideRequest(int param1Int) {
      this.mTypes = param1Int;
    }
    
    public void replay(InsetsController param1InsetsController) {
      param1InsetsController.hide(this.mTypes);
    }
  }
}
