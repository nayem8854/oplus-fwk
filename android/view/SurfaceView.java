package android.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.RenderNode;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.AttributeSet;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.IAccessibilityEmbeddedConnection;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class SurfaceView extends View implements ViewRootImpl.SurfaceChangedCallback {
  static {
    boolean bool = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    DEBUG_POSITION = bool;
  }
  
  final ArrayList<SurfaceHolder.Callback> mCallbacks = new ArrayList<>();
  
  final int[] mLocation = new int[2];
  
  final ReentrantLock mSurfaceLock = new ReentrantLock(true);
  
  final Surface mSurface = new Surface();
  
  boolean mDrawingStopped = true;
  
  boolean mDrawFinished = false;
  
  final Rect mScreenRect = new Rect();
  
  private final SurfaceSession mSurfaceSession = new SurfaceSession();
  
  private boolean mDisableBackgroundLayer = false;
  
  final Object mSurfaceControlLock = new Object();
  
  final Rect mTmpRect = new Rect();
  
  int mSubLayer = -2;
  
  boolean mIsCreating = false;
  
  private volatile boolean mRtHandlingPositionUpdates = false;
  
  private volatile boolean mRtReleaseSurfaces = false;
  
  private final ViewTreeObserver.OnScrollChangedListener mScrollChangedListener = new _$$Lambda$PYGleuqIeCxjTD1pJqqx1opFv1g(this);
  
  private final ViewTreeObserver.OnPreDrawListener mDrawListener = new _$$Lambda$SurfaceView$w68OV7dB_zKVNsA_r0IrAUtyWas(this);
  
  boolean mRequestedVisible = false;
  
  boolean mWindowVisibility = false;
  
  boolean mLastWindowVisibility = false;
  
  boolean mViewVisibility = false;
  
  boolean mWindowStopped = false;
  
  int mRequestedWidth = -1;
  
  int mRequestedHeight = -1;
  
  int mRequestedFormat = 4;
  
  boolean mUseAlpha = false;
  
  float mSurfaceAlpha = 1.0F;
  
  int mBackgroundColor = -16777216;
  
  boolean mHaveFrame = false;
  
  boolean mSurfaceCreated = false;
  
  long mLastLockTime = 0L;
  
  boolean mVisible = false;
  
  int mWindowSpaceLeft = -1;
  
  int mWindowSpaceTop = -1;
  
  int mSurfaceWidth = -1;
  
  int mSurfaceHeight = -1;
  
  int mFormat = -1;
  
  final Rect mSurfaceFrame = new Rect();
  
  int mLastSurfaceHeight = -1;
  
  int mLastSurfaceWidth = -1;
  
  private int mSurfaceFlags = 4;
  
  private SurfaceControl.Transaction mRtTransaction = new SurfaceControl.Transaction();
  
  private SurfaceControl.Transaction mTmpTransaction = new SurfaceControl.Transaction();
  
  private final Matrix mScreenMatrixForEmbeddedHierarchy = new Matrix();
  
  private final Matrix mTmpMatrix = new Matrix();
  
  private final float[] mMatrixValues = new float[9];
  
  private static final boolean DEBUG;
  
  private static final boolean DEBUG_POSITION;
  
  private static final String TAG = "SurfaceView";
  
  private final String BROADCAST_NAME;
  
  private boolean HQV_ENABLE;
  
  private volatile boolean isRegistered;
  
  private boolean mAttachedToWindow;
  
  SurfaceControl mBackgroundControl;
  
  boolean mClipSurfaceToBounds;
  
  float mCornerRadius;
  
  SurfaceControl mDeferredDestroySurfaceControl;
  
  private boolean mGlobalListenersAdded;
  
  private int mParentSurfaceGenerationId;
  
  private int mPendingReportDraws;
  
  private RenderNode.PositionUpdateListener mPositionListener;
  
  private Rect mRTLastReportedPosition;
  
  private RemoteAccessibilityEmbeddedConnection mRemoteAccessibilityEmbeddedConnection;
  
  Paint mRoundedViewportPaint;
  
  private BroadcastReceiver mStatisticsReceiver;
  
  SurfaceControl mSurfaceControl;
  
  private final SurfaceHolder mSurfaceHolder;
  
  SurfaceControlViewHost.SurfacePackage mSurfacePackage;
  
  public SurfaceView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public SurfaceView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public SurfaceView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SurfaceView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    this(paramContext, paramAttributeSet, paramInt1, paramInt2, false);
  }
  
  public SurfaceView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2, boolean paramBoolean) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.isRegistered = false;
    this.HQV_ENABLE = this.mContext.getPackageManager().hasSystemFeature("oppo.game.color.plus.support");
    this.BROADCAST_NAME = "coloros.intent.action.hqvchanged";
    this.mStatisticsReceiver = (BroadcastReceiver)new Object(this);
    this.mRTLastReportedPosition = new Rect();
    this.mPositionListener = (RenderNode.PositionUpdateListener)new Object(this);
    this.mSurfaceHolder = (SurfaceHolder)new Object(this);
    this.mRenderNode.addPositionUpdateListener(this.mPositionListener);
    setWillNotDraw(true);
    registerHqvListen();
    this.mDisableBackgroundLayer = paramBoolean;
  }
  
  private void registerHqvListen() {
    if (!this.isRegistered && this.HQV_ENABLE) {
      if (DEBUG)
        Log.d("SurfaceView", "hqv registerHqvListen"); 
      IntentFilter intentFilter = new IntentFilter();
      intentFilter.addAction("coloros.intent.action.hqvchanged");
      this.mContext.registerReceiver(this.mStatisticsReceiver, intentFilter, null, null);
      this.isRegistered = true;
    } 
  }
  
  private void unRegisterHqvListen() {
    try {
      if (this.isRegistered && this.HQV_ENABLE) {
        if (DEBUG)
          Log.d("SurfaceView", "hqv unRegisterHqvListen"); 
        this.mContext.unregisterReceiver(this.mStatisticsReceiver);
        this.isRegistered = false;
      } 
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.d("SurfaceView", "SurfaceView Unregister has exception!");
    } 
  }
  
  public SurfaceHolder getHolder() {
    return this.mSurfaceHolder;
  }
  
  private void updateRequestedVisibility() {
    boolean bool;
    if (this.mViewVisibility && this.mWindowVisibility && !this.mWindowStopped) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mRequestedVisible = bool;
  }
  
  private void setWindowStopped(boolean paramBoolean) {
    this.mWindowStopped = paramBoolean;
    updateRequestedVisibility();
    updateSurface();
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    getViewRootImpl().addSurfaceChangedCallback(this);
    boolean bool = false;
    this.mWindowStopped = false;
    if (getVisibility() == 0)
      bool = true; 
    this.mViewVisibility = bool;
    updateRequestedVisibility();
    this.mAttachedToWindow = true;
    this.mParent.requestTransparentRegion(this);
    if (!this.mGlobalListenersAdded) {
      ViewTreeObserver viewTreeObserver = getViewTreeObserver();
      viewTreeObserver.addOnScrollChangedListener(this.mScrollChangedListener);
      viewTreeObserver.addOnPreDrawListener(this.mDrawListener);
      this.mGlobalListenersAdded = true;
    } 
  }
  
  protected void onWindowVisibilityChanged(int paramInt) {
    boolean bool;
    super.onWindowVisibilityChanged(paramInt);
    if (paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mWindowVisibility = bool;
    updateRequestedVisibility();
    updateSurface();
  }
  
  public void setVisibility(int paramInt) {
    boolean bool2;
    super.setVisibility(paramInt);
    boolean bool1 = true;
    if (paramInt == 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mViewVisibility = bool2;
    if (this.mWindowVisibility && bool2 && !this.mWindowStopped) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    if (bool2 != this.mRequestedVisible)
      requestLayout(); 
    this.mRequestedVisible = bool2;
    updateSurface();
  }
  
  public void setUseAlpha() {
    if (!this.mUseAlpha) {
      this.mUseAlpha = true;
      updateSurfaceAlpha();
    } 
  }
  
  public void setAlpha(float paramFloat) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(System.identityHashCode(this));
      stringBuilder.append(" setAlpha: mUseAlpha = ");
      stringBuilder.append(this.mUseAlpha);
      stringBuilder.append(" alpha=");
      stringBuilder.append(paramFloat);
      Log.d("SurfaceView", stringBuilder.toString());
    } 
    super.setAlpha(paramFloat);
    updateSurfaceAlpha();
  }
  
  private float getFixedAlpha() {
    float f = getAlpha();
    if (!this.mUseAlpha || (this.mSubLayer <= 0 && f != 0.0F))
      f = 1.0F; 
    return f;
  }
  
  private void updateSurfaceAlpha() {
    StringBuilder stringBuilder;
    if (!this.mUseAlpha) {
      if (DEBUG) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(System.identityHashCode(this));
        stringBuilder.append(" updateSurfaceAlpha: setUseAlpha() is not called, ignored.");
        Log.d("SurfaceView", stringBuilder.toString());
      } 
      return;
    } 
    float f = getAlpha();
    if (this.mSubLayer < 0 && 0.0F < f && f < 1.0F) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(System.identityHashCode(this));
      stringBuilder.append(" updateSurfaceAlpha: translucent color is not supported for a surface placed z-below.");
      Log.w("SurfaceView", stringBuilder.toString());
    } 
    if (!this.mHaveFrame) {
      if (DEBUG) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(System.identityHashCode(this));
        stringBuilder.append(" updateSurfaceAlpha: has no surface.");
        Log.d("SurfaceView", stringBuilder.toString());
      } 
      return;
    } 
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl == null) {
      if (DEBUG) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(System.identityHashCode(this));
        stringBuilder.append(" updateSurfaceAlpha: ViewRootImpl not available.");
        Log.d("SurfaceView", stringBuilder.toString());
      } 
      return;
    } 
    if (this.mSurfaceControl == null) {
      if (DEBUG) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(System.identityHashCode(this));
        stringBuilder.append("updateSurfaceAlpha: surface is not yet created, or already released.");
        Log.d("SurfaceView", stringBuilder.toString());
      } 
      return;
    } 
    Surface surface = ((ViewRootImpl)stringBuilder).mSurface;
    if (surface == null || !surface.isValid()) {
      if (DEBUG) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(System.identityHashCode(this));
        stringBuilder.append(" updateSurfaceAlpha: ViewRootImpl has no valid surface");
        Log.d("SurfaceView", stringBuilder.toString());
      } 
      return;
    } 
    f = getFixedAlpha();
    if (f != this.mSurfaceAlpha) {
      if (isHardwareAccelerated()) {
        boolean bool = stringBuilder.useBLAST();
        stringBuilder.registerRtFrameCallback(new _$$Lambda$SurfaceView$DKSyxzWn62XKbC15Dh1hMSfxKQg(this, bool, (ViewRootImpl)stringBuilder, surface, f));
        damageInParent();
      } else {
        if (DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append(System.identityHashCode(this));
          stringBuilder.append(" updateSurfaceAlpha: set alpha=");
          stringBuilder.append(f);
          Log.d("SurfaceView", stringBuilder.toString());
        } 
        this.mTmpTransaction.setAlpha(this.mSurfaceControl, f).apply();
      } 
      this.mSurfaceAlpha = f;
    } 
  }
  
  private void performDrawFinished() {
    if (this.mDeferredDestroySurfaceControl != null)
      synchronized (this.mSurfaceControlLock) {
        this.mTmpTransaction.remove(this.mDeferredDestroySurfaceControl).apply();
        this.mDeferredDestroySurfaceControl = null;
      }  
    if (this.mPendingReportDraws > 0) {
      this.mDrawFinished = true;
      if (this.mAttachedToWindow) {
        this.mParent.requestTransparentRegion(this);
        notifyDrawFinished();
        invalidate();
      } 
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(System.identityHashCode(this));
      stringBuilder.append("finished drawing but no pending report draw (extra call to draw completion runnable?)");
      Log.e("SurfaceView", stringBuilder.toString());
    } 
  }
  
  void notifyDrawFinished() {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl != null)
      viewRootImpl.pendingDrawFinished(); 
    this.mPendingReportDraws--;
  }
  
  protected void onDetachedFromWindow() {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl != null)
      viewRootImpl.removeSurfaceChangedCallback(this); 
    this.mAttachedToWindow = false;
    if (this.mGlobalListenersAdded) {
      ViewTreeObserver viewTreeObserver = getViewTreeObserver();
      viewTreeObserver.removeOnScrollChangedListener(this.mScrollChangedListener);
      viewTreeObserver.removeOnPreDrawListener(this.mDrawListener);
      this.mGlobalListenersAdded = false;
    } 
    while (this.mPendingReportDraws > 0)
      notifyDrawFinished(); 
    this.mRequestedVisible = false;
    updateSurface();
    releaseSurfaces();
    SurfaceControlViewHost.SurfacePackage surfacePackage = this.mSurfacePackage;
    if (surfacePackage != null) {
      surfacePackage.release();
      this.mSurfacePackage = null;
    } 
    this.mHaveFrame = false;
    unRegisterHqvListen();
    super.onDetachedFromWindow();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = this.mRequestedWidth;
    if (i >= 0) {
      paramInt1 = resolveSizeAndState(i, paramInt1, 0);
    } else {
      paramInt1 = getDefaultSize(0, paramInt1);
    } 
    i = this.mRequestedHeight;
    if (i >= 0) {
      paramInt2 = resolveSizeAndState(i, paramInt2, 0);
    } else {
      paramInt2 = getDefaultSize(0, paramInt2);
    } 
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
    updateSurface();
    return bool;
  }
  
  public boolean gatherTransparentRegion(Region paramRegion) {
    boolean bool2;
    if (isAboveParent() || !this.mDrawFinished)
      return super.gatherTransparentRegion(paramRegion); 
    boolean bool1 = true;
    if ((this.mPrivateFlags & 0x80) == 0) {
      bool2 = super.gatherTransparentRegion(paramRegion);
    } else {
      bool2 = bool1;
      if (paramRegion != null) {
        int i = getWidth();
        int j = getHeight();
        bool2 = bool1;
        if (i > 0) {
          bool2 = bool1;
          if (j > 0) {
            getLocationInWindow(this.mLocation);
            int arrayOfInt[] = this.mLocation, k = arrayOfInt[0];
            int m = arrayOfInt[1];
            paramRegion.op(k, m, k + i, m + j, Region.Op.UNION);
            bool2 = bool1;
          } 
        } 
      } 
    } 
    if (PixelFormat.formatHasAlpha(this.mRequestedFormat))
      bool2 = false; 
    return bool2;
  }
  
  public void draw(Canvas paramCanvas) {
    if (this.mDrawFinished && !isAboveParent())
      if ((this.mPrivateFlags & 0x80) == 0)
        clearSurfaceViewPort(paramCanvas);  
    super.draw(paramCanvas);
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    if (this.mDrawFinished && !isAboveParent())
      if ((this.mPrivateFlags & 0x80) == 128)
        clearSurfaceViewPort(paramCanvas);  
    super.dispatchDraw(paramCanvas);
  }
  
  public void setEnableSurfaceClipping(boolean paramBoolean) {
    this.mClipSurfaceToBounds = paramBoolean;
    invalidate();
  }
  
  public void setClipBounds(Rect paramRect) {
    super.setClipBounds(paramRect);
    if (!this.mClipSurfaceToBounds)
      return; 
    if (this.mCornerRadius > 0.0F && !isAboveParent())
      invalidate(); 
    if (this.mSurfaceControl != null) {
      if (this.mClipBounds != null) {
        this.mTmpRect.set(this.mClipBounds);
      } else {
        this.mTmpRect.set(0, 0, this.mSurfaceWidth, this.mSurfaceHeight);
      } 
      SyncRtSurfaceTransactionApplier syncRtSurfaceTransactionApplier = new SyncRtSurfaceTransactionApplier(this);
      SyncRtSurfaceTransactionApplier.SurfaceParams.Builder builder1 = new SyncRtSurfaceTransactionApplier.SurfaceParams.Builder(this.mSurfaceControl);
      Rect rect = this.mTmpRect;
      SyncRtSurfaceTransactionApplier.SurfaceParams.Builder builder2 = builder1.withWindowCrop(rect);
      SyncRtSurfaceTransactionApplier.SurfaceParams surfaceParams = builder2.build();
      syncRtSurfaceTransactionApplier.scheduleApply(new SyncRtSurfaceTransactionApplier.SurfaceParams[] { surfaceParams });
    } 
  }
  
  private void clearSurfaceViewPort(Canvas paramCanvas) {
    if (this.mCornerRadius > 0.0F) {
      paramCanvas.getClipBounds(this.mTmpRect);
      if (this.mClipSurfaceToBounds && this.mClipBounds != null)
        this.mTmpRect.intersect(this.mClipBounds); 
      float f1 = this.mTmpRect.left, f2 = this.mTmpRect.top, f3 = this.mTmpRect.right, f4 = this.mTmpRect.bottom, f5 = this.mCornerRadius;
      paramCanvas.drawRoundRect(f1, f2, f3, f4, f5, f5, this.mRoundedViewportPaint);
    } else {
      paramCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
    } 
  }
  
  public void setCornerRadius(float paramFloat) {
    this.mCornerRadius = paramFloat;
    if (paramFloat > 0.0F && this.mRoundedViewportPaint == null) {
      Paint paint = new Paint(1);
      paint.setBlendMode(BlendMode.CLEAR);
      this.mRoundedViewportPaint.setColor(0);
    } 
    invalidate();
  }
  
  public float getCornerRadius() {
    return this.mCornerRadius;
  }
  
  public void setZOrderMediaOverlay(boolean paramBoolean) {
    byte b;
    if (paramBoolean) {
      b = -1;
    } else {
      b = -2;
    } 
    this.mSubLayer = b;
  }
  
  public void setZOrderOnTop(boolean paramBoolean) {
    boolean bool;
    if ((getContext().getApplicationInfo()).targetSdkVersion > 29) {
      bool = true;
    } else {
      bool = false;
    } 
    setZOrderedOnTop(paramBoolean, bool);
  }
  
  public boolean isZOrderedOnTop() {
    boolean bool;
    if (this.mSubLayer > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean setZOrderedOnTop(boolean paramBoolean1, boolean paramBoolean2) {
    byte b;
    if (paramBoolean1) {
      b = 1;
    } else {
      b = -2;
    } 
    if (this.mSubLayer == b)
      return false; 
    this.mSubLayer = b;
    if (!paramBoolean2)
      return false; 
    if (this.mSurfaceControl == null)
      return true; 
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl == null)
      return true; 
    Surface surface = viewRootImpl.mSurface;
    if (surface == null || !surface.isValid())
      return true; 
    paramBoolean1 = viewRootImpl.useBLAST();
    viewRootImpl.registerRtFrameCallback(new _$$Lambda$SurfaceView$LV_kXr5_jITlrRhkV6FsfMSbnO8(this, paramBoolean1, viewRootImpl, surface));
    invalidate();
    return true;
  }
  
  public void setSecure(boolean paramBoolean) {
    if (paramBoolean) {
      this.mSurfaceFlags |= 0x80;
    } else {
      this.mSurfaceFlags &= 0xFFFFFF7F;
    } 
  }
  
  public void setProtected(boolean paramBoolean) {
    if (paramBoolean) {
      this.mSurfaceFlags |= 0x800;
    } else {
      this.mSurfaceFlags &= 0xFFFFF7FF;
    } 
  }
  
  private void updateOpaqueFlag() {
    if (!PixelFormat.formatHasAlpha(this.mRequestedFormat)) {
      this.mSurfaceFlags |= 0x400;
    } else {
      this.mSurfaceFlags &= 0xFFFFFBFF;
    } 
  }
  
  private void updateBackgroundVisibility(SurfaceControl.Transaction paramTransaction) {
    SurfaceControl surfaceControl = this.mBackgroundControl;
    if (surfaceControl == null)
      return; 
    if (this.mSubLayer < 0 && (this.mSurfaceFlags & 0x400) != 0 && !this.mDisableBackgroundLayer) {
      paramTransaction.show(surfaceControl);
    } else {
      ViewRootImpl viewRootImpl = getViewRootImpl();
      String str = viewRootImpl.getTitle().toString();
      if (str != null && str.contains("com.tencent.qqlive") && this.mSubLayer == -2) {
        paramTransaction.show(this.mBackgroundControl);
      } else {
        paramTransaction.hide(this.mBackgroundControl);
      } 
    } 
  }
  
  private SurfaceControl.Transaction updateBackgroundColor(SurfaceControl.Transaction paramTransaction) {
    float f1 = Color.red(this.mBackgroundColor) / 255.0F;
    int i = this.mBackgroundColor;
    float f2 = Color.green(i) / 255.0F, f3 = Color.blue(this.mBackgroundColor) / 255.0F;
    paramTransaction.setColor(this.mBackgroundControl, new float[] { f1, f2, f3 });
    return paramTransaction;
  }
  
  private void releaseSurfaces() {
    this.mSurfaceAlpha = 1.0F;
    synchronized (this.mSurfaceControlLock) {
      this.mSurface.release();
      if (this.mRtHandlingPositionUpdates) {
        this.mRtReleaseSurfaces = true;
        return;
      } 
      if (this.mSurfaceControl != null) {
        this.mTmpTransaction.remove(this.mSurfaceControl);
        this.mSurfaceControl = null;
      } 
      if (this.mBackgroundControl != null) {
        this.mTmpTransaction.remove(this.mBackgroundControl);
        this.mBackgroundControl = null;
      } 
      this.mTmpTransaction.apply();
      return;
    } 
  }
  
  protected void updateSurface() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mHaveFrame : Z
    //   4: ifne -> 8
    //   7: return
    //   8: aload_0
    //   9: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   12: astore_1
    //   13: aload_1
    //   14: ifnonnull -> 18
    //   17: return
    //   18: aload_1
    //   19: getfield mSurface : Landroid/view/Surface;
    //   22: ifnull -> 2884
    //   25: aload_1
    //   26: getfield mSurface : Landroid/view/Surface;
    //   29: invokevirtual isValid : ()Z
    //   32: ifne -> 38
    //   35: goto -> 2884
    //   38: aload_1
    //   39: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   42: astore_2
    //   43: aload_2
    //   44: ifnull -> 55
    //   47: aload_0
    //   48: getfield mSurface : Landroid/view/Surface;
    //   51: aload_2
    //   52: invokevirtual setCompatibilityTranslator : (Landroid/content/res/CompatibilityInfo$Translator;)V
    //   55: aload_0
    //   56: getfield mRequestedWidth : I
    //   59: istore_3
    //   60: iload_3
    //   61: istore #4
    //   63: iload_3
    //   64: ifgt -> 73
    //   67: aload_0
    //   68: invokevirtual getWidth : ()I
    //   71: istore #4
    //   73: aload_0
    //   74: getfield mRequestedHeight : I
    //   77: istore #5
    //   79: iload #5
    //   81: istore_3
    //   82: iload #5
    //   84: ifgt -> 92
    //   87: aload_0
    //   88: invokevirtual getHeight : ()I
    //   91: istore_3
    //   92: aload_0
    //   93: invokespecial getFixedAlpha : ()F
    //   96: fstore #6
    //   98: aload_0
    //   99: getfield mFormat : I
    //   102: aload_0
    //   103: getfield mRequestedFormat : I
    //   106: if_icmpeq -> 115
    //   109: iconst_1
    //   110: istore #7
    //   112: goto -> 118
    //   115: iconst_0
    //   116: istore #7
    //   118: aload_0
    //   119: getfield mVisible : Z
    //   122: aload_0
    //   123: getfield mRequestedVisible : Z
    //   126: if_icmpeq -> 135
    //   129: iconst_1
    //   130: istore #8
    //   132: goto -> 138
    //   135: iconst_0
    //   136: istore #8
    //   138: aload_0
    //   139: getfield mSurfaceAlpha : F
    //   142: fload #6
    //   144: fcmpl
    //   145: ifeq -> 154
    //   148: iconst_1
    //   149: istore #9
    //   151: goto -> 157
    //   154: iconst_0
    //   155: istore #9
    //   157: aload_0
    //   158: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   161: ifnull -> 174
    //   164: iload #7
    //   166: ifne -> 174
    //   169: iload #8
    //   171: ifeq -> 187
    //   174: aload_0
    //   175: getfield mRequestedVisible : Z
    //   178: ifeq -> 187
    //   181: iconst_1
    //   182: istore #10
    //   184: goto -> 190
    //   187: iconst_0
    //   188: istore #10
    //   190: aload_0
    //   191: getfield mSurfaceWidth : I
    //   194: iload #4
    //   196: if_icmpne -> 216
    //   199: aload_0
    //   200: getfield mSurfaceHeight : I
    //   203: iload_3
    //   204: if_icmpeq -> 210
    //   207: goto -> 216
    //   210: iconst_0
    //   211: istore #11
    //   213: goto -> 219
    //   216: iconst_1
    //   217: istore #11
    //   219: aload_0
    //   220: getfield mWindowVisibility : Z
    //   223: aload_0
    //   224: getfield mLastWindowVisibility : Z
    //   227: if_icmpeq -> 236
    //   230: iconst_1
    //   231: istore #12
    //   233: goto -> 239
    //   236: iconst_0
    //   237: istore #12
    //   239: iconst_0
    //   240: istore #13
    //   242: aload_0
    //   243: aload_0
    //   244: getfield mLocation : [I
    //   247: invokevirtual getLocationInSurface : ([I)V
    //   250: aload_0
    //   251: getfield mWindowSpaceLeft : I
    //   254: istore #5
    //   256: aload_0
    //   257: getfield mLocation : [I
    //   260: astore #14
    //   262: iload #5
    //   264: aload #14
    //   266: iconst_0
    //   267: iaload
    //   268: if_icmpne -> 291
    //   271: aload_0
    //   272: getfield mWindowSpaceTop : I
    //   275: aload #14
    //   277: iconst_1
    //   278: iaload
    //   279: if_icmpeq -> 285
    //   282: goto -> 291
    //   285: iconst_0
    //   286: istore #5
    //   288: goto -> 294
    //   291: iconst_1
    //   292: istore #5
    //   294: aload_0
    //   295: invokevirtual getWidth : ()I
    //   298: aload_0
    //   299: getfield mScreenRect : Landroid/graphics/Rect;
    //   302: invokevirtual width : ()I
    //   305: if_icmpne -> 331
    //   308: aload_0
    //   309: invokevirtual getHeight : ()I
    //   312: aload_0
    //   313: getfield mScreenRect : Landroid/graphics/Rect;
    //   316: invokevirtual height : ()I
    //   319: if_icmpeq -> 325
    //   322: goto -> 331
    //   325: iconst_0
    //   326: istore #15
    //   328: goto -> 334
    //   331: iconst_1
    //   332: istore #15
    //   334: iload #10
    //   336: ifne -> 387
    //   339: iload #7
    //   341: ifne -> 387
    //   344: iload #11
    //   346: ifne -> 387
    //   349: iload #8
    //   351: ifne -> 387
    //   354: aload_0
    //   355: getfield mUseAlpha : Z
    //   358: ifeq -> 366
    //   361: iload #9
    //   363: ifne -> 387
    //   366: iload #12
    //   368: ifne -> 387
    //   371: iload #5
    //   373: ifne -> 387
    //   376: iload #15
    //   378: ifeq -> 384
    //   381: goto -> 387
    //   384: goto -> 2883
    //   387: aload_0
    //   388: aload_0
    //   389: getfield mLocation : [I
    //   392: invokevirtual getLocationInWindow : ([I)V
    //   395: getstatic android/view/SurfaceView.DEBUG : Z
    //   398: ifeq -> 630
    //   401: new java/lang/StringBuilder
    //   404: dup
    //   405: invokespecial <init> : ()V
    //   408: astore #14
    //   410: aload #14
    //   412: aload_0
    //   413: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   416: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   419: pop
    //   420: aload #14
    //   422: ldc_w ' Changes: creating='
    //   425: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   428: pop
    //   429: aload #14
    //   431: iload #10
    //   433: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   436: pop
    //   437: aload #14
    //   439: ldc_w ' format='
    //   442: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   445: pop
    //   446: aload #14
    //   448: iload #7
    //   450: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   453: pop
    //   454: aload #14
    //   456: ldc_w ' size='
    //   459: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   462: pop
    //   463: aload #14
    //   465: iload #11
    //   467: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   470: pop
    //   471: aload #14
    //   473: ldc_w ' visible='
    //   476: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   479: pop
    //   480: aload #14
    //   482: iload #8
    //   484: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   487: pop
    //   488: aload #14
    //   490: ldc_w ' alpha='
    //   493: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   496: pop
    //   497: aload #14
    //   499: iload #9
    //   501: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   504: pop
    //   505: aload #14
    //   507: ldc_w ' mUseAlpha='
    //   510: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   513: pop
    //   514: aload #14
    //   516: aload_0
    //   517: getfield mUseAlpha : Z
    //   520: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   523: pop
    //   524: aload #14
    //   526: ldc_w ' visible='
    //   529: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   532: pop
    //   533: aload #14
    //   535: iload #8
    //   537: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   540: pop
    //   541: aload #14
    //   543: ldc_w ' left='
    //   546: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   549: pop
    //   550: aload_0
    //   551: getfield mWindowSpaceLeft : I
    //   554: aload_0
    //   555: getfield mLocation : [I
    //   558: iconst_0
    //   559: iaload
    //   560: if_icmpeq -> 569
    //   563: iconst_1
    //   564: istore #9
    //   566: goto -> 572
    //   569: iconst_0
    //   570: istore #9
    //   572: aload #14
    //   574: iload #9
    //   576: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   579: pop
    //   580: aload #14
    //   582: ldc_w ' top='
    //   585: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   588: pop
    //   589: aload_0
    //   590: getfield mWindowSpaceTop : I
    //   593: aload_0
    //   594: getfield mLocation : [I
    //   597: iconst_1
    //   598: iaload
    //   599: if_icmpeq -> 608
    //   602: iconst_1
    //   603: istore #9
    //   605: goto -> 611
    //   608: iconst_0
    //   609: istore #9
    //   611: aload #14
    //   613: iload #9
    //   615: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   618: pop
    //   619: ldc 'SurfaceView'
    //   621: aload #14
    //   623: invokevirtual toString : ()Ljava/lang/String;
    //   626: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   629: pop
    //   630: aload_0
    //   631: getfield mRequestedVisible : Z
    //   634: istore #16
    //   636: aload_0
    //   637: iload #16
    //   639: putfield mVisible : Z
    //   642: aload_0
    //   643: getfield mLocation : [I
    //   646: iconst_0
    //   647: iaload
    //   648: istore #12
    //   650: aload_0
    //   651: iload #12
    //   653: putfield mWindowSpaceLeft : I
    //   656: aload_0
    //   657: aload_0
    //   658: getfield mLocation : [I
    //   661: iconst_1
    //   662: iaload
    //   663: putfield mWindowSpaceTop : I
    //   666: aload_0
    //   667: iload #4
    //   669: putfield mSurfaceWidth : I
    //   672: aload_0
    //   673: iload_3
    //   674: putfield mSurfaceHeight : I
    //   677: aload_0
    //   678: aload_0
    //   679: getfield mRequestedFormat : I
    //   682: putfield mFormat : I
    //   685: aload_0
    //   686: aload_0
    //   687: getfield mWindowVisibility : Z
    //   690: putfield mLastWindowVisibility : Z
    //   693: aload_0
    //   694: getfield mScreenRect : Landroid/graphics/Rect;
    //   697: iload #12
    //   699: putfield left : I
    //   702: aload_0
    //   703: getfield mScreenRect : Landroid/graphics/Rect;
    //   706: aload_0
    //   707: getfield mWindowSpaceTop : I
    //   710: putfield top : I
    //   713: aload_0
    //   714: getfield mScreenRect : Landroid/graphics/Rect;
    //   717: aload_0
    //   718: getfield mWindowSpaceLeft : I
    //   721: aload_0
    //   722: invokevirtual getWidth : ()I
    //   725: iadd
    //   726: putfield right : I
    //   729: aload_0
    //   730: getfield mScreenRect : Landroid/graphics/Rect;
    //   733: aload_0
    //   734: getfield mWindowSpaceTop : I
    //   737: aload_0
    //   738: invokevirtual getHeight : ()I
    //   741: iadd
    //   742: putfield bottom : I
    //   745: aload_2
    //   746: ifnull -> 764
    //   749: aload_2
    //   750: aload_0
    //   751: getfield mScreenRect : Landroid/graphics/Rect;
    //   754: invokevirtual translateRectInAppWindowToScreen : (Landroid/graphics/Rect;)V
    //   757: goto -> 764
    //   760: astore_2
    //   761: goto -> 2746
    //   764: aload_1
    //   765: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   768: getfield surfaceInsets : Landroid/graphics/Rect;
    //   771: astore #14
    //   773: aload_0
    //   774: getfield mScreenRect : Landroid/graphics/Rect;
    //   777: aload #14
    //   779: getfield left : I
    //   782: aload #14
    //   784: getfield top : I
    //   787: invokevirtual offset : (II)V
    //   790: iload #10
    //   792: ifeq -> 1108
    //   795: aload_0
    //   796: aload_0
    //   797: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   800: putfield mDeferredDestroySurfaceControl : Landroid/view/SurfaceControl;
    //   803: aload_0
    //   804: invokespecial updateOpaqueFlag : ()V
    //   807: new java/lang/StringBuilder
    //   810: astore #14
    //   812: aload #14
    //   814: invokespecial <init> : ()V
    //   817: aload #14
    //   819: ldc_w 'SurfaceView - '
    //   822: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   825: pop
    //   826: aload #14
    //   828: aload_1
    //   829: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   832: invokeinterface toString : ()Ljava/lang/String;
    //   837: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   840: pop
    //   841: aload #14
    //   843: invokevirtual toString : ()Ljava/lang/String;
    //   846: astore #14
    //   848: new android/view/SurfaceControl$Builder
    //   851: astore #17
    //   853: aload #17
    //   855: aload_0
    //   856: getfield mSurfaceSession : Landroid/view/SurfaceSession;
    //   859: invokespecial <init> : (Landroid/view/SurfaceSession;)V
    //   862: aload #17
    //   864: aload #14
    //   866: invokevirtual setName : (Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
    //   869: astore #17
    //   871: aload #17
    //   873: aload_0
    //   874: invokevirtual setLocalOwnerView : (Landroid/view/View;)Landroid/view/SurfaceControl$Builder;
    //   877: astore #17
    //   879: aload_0
    //   880: getfield mSurfaceFlags : I
    //   883: sipush #1024
    //   886: iand
    //   887: ifeq -> 896
    //   890: iconst_1
    //   891: istore #9
    //   893: goto -> 899
    //   896: iconst_0
    //   897: istore #9
    //   899: aload #17
    //   901: iload #9
    //   903: invokevirtual setOpaque : (Z)Landroid/view/SurfaceControl$Builder;
    //   906: astore #17
    //   908: aload_0
    //   909: getfield mSurfaceWidth : I
    //   912: istore #18
    //   914: aload_0
    //   915: getfield mSurfaceHeight : I
    //   918: istore #12
    //   920: aload #17
    //   922: iload #18
    //   924: iload #12
    //   926: invokevirtual setBufferSize : (II)Landroid/view/SurfaceControl$Builder;
    //   929: astore #17
    //   931: aload_0
    //   932: getfield mFormat : I
    //   935: istore #12
    //   937: aload #17
    //   939: iload #12
    //   941: invokevirtual setFormat : (I)Landroid/view/SurfaceControl$Builder;
    //   944: astore #17
    //   946: aload #17
    //   948: aload_1
    //   949: invokevirtual getBoundsLayer : ()Landroid/view/SurfaceControl;
    //   952: invokevirtual setParent : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;
    //   955: astore #17
    //   957: aload_0
    //   958: getfield mSurfaceFlags : I
    //   961: istore #12
    //   963: aload #17
    //   965: iload #12
    //   967: invokevirtual setFlags : (I)Landroid/view/SurfaceControl$Builder;
    //   970: astore #17
    //   972: aload #17
    //   974: ldc_w 'SurfaceView.updateSurface'
    //   977: invokevirtual setCallsite : (Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
    //   980: astore #17
    //   982: aload_0
    //   983: aload #17
    //   985: invokevirtual build : ()Landroid/view/SurfaceControl;
    //   988: putfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   991: new android/view/SurfaceControl$Builder
    //   994: astore #17
    //   996: aload #17
    //   998: aload_0
    //   999: getfield mSurfaceSession : Landroid/view/SurfaceSession;
    //   1002: invokespecial <init> : (Landroid/view/SurfaceSession;)V
    //   1005: new java/lang/StringBuilder
    //   1008: astore #19
    //   1010: aload #19
    //   1012: invokespecial <init> : ()V
    //   1015: aload #19
    //   1017: ldc_w 'Background for -'
    //   1020: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1023: pop
    //   1024: aload #19
    //   1026: aload #14
    //   1028: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1031: pop
    //   1032: aload #19
    //   1034: invokevirtual toString : ()Ljava/lang/String;
    //   1037: astore #14
    //   1039: aload #17
    //   1041: aload #14
    //   1043: invokevirtual setName : (Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
    //   1046: astore #14
    //   1048: aload #14
    //   1050: aload_0
    //   1051: invokevirtual setLocalOwnerView : (Landroid/view/View;)Landroid/view/SurfaceControl$Builder;
    //   1054: astore #14
    //   1056: aload #14
    //   1058: iconst_1
    //   1059: invokevirtual setOpaque : (Z)Landroid/view/SurfaceControl$Builder;
    //   1062: astore #14
    //   1064: aload #14
    //   1066: invokevirtual setColorLayer : ()Landroid/view/SurfaceControl$Builder;
    //   1069: astore #17
    //   1071: aload_0
    //   1072: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1075: astore #14
    //   1077: aload #17
    //   1079: aload #14
    //   1081: invokevirtual setParent : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;
    //   1084: astore #14
    //   1086: aload #14
    //   1088: ldc_w 'SurfaceView.updateSurface'
    //   1091: invokevirtual setCallsite : (Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
    //   1094: astore #14
    //   1096: aload_0
    //   1097: aload #14
    //   1099: invokevirtual build : ()Landroid/view/SurfaceControl;
    //   1102: putfield mBackgroundControl : Landroid/view/SurfaceControl;
    //   1105: goto -> 1116
    //   1108: aload_0
    //   1109: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1112: ifnonnull -> 1116
    //   1115: return
    //   1116: aload_0
    //   1117: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
    //   1120: invokevirtual lock : ()V
    //   1123: iload #16
    //   1125: ifne -> 1134
    //   1128: iconst_1
    //   1129: istore #9
    //   1131: goto -> 1137
    //   1134: iconst_0
    //   1135: istore #9
    //   1137: aload_0
    //   1138: iload #9
    //   1140: putfield mDrawingStopped : Z
    //   1143: getstatic android/view/SurfaceView.DEBUG : Z
    //   1146: istore #9
    //   1148: iload #9
    //   1150: ifeq -> 1210
    //   1153: new java/lang/StringBuilder
    //   1156: astore #14
    //   1158: aload #14
    //   1160: invokespecial <init> : ()V
    //   1163: aload #14
    //   1165: aload_0
    //   1166: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   1169: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1172: pop
    //   1173: aload #14
    //   1175: ldc_w ' Cur surface: '
    //   1178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1181: pop
    //   1182: aload #14
    //   1184: aload_0
    //   1185: getfield mSurface : Landroid/view/Surface;
    //   1188: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1191: pop
    //   1192: ldc 'SurfaceView'
    //   1194: aload #14
    //   1196: invokevirtual toString : ()Ljava/lang/String;
    //   1199: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   1202: pop
    //   1203: goto -> 1210
    //   1206: astore_2
    //   1207: goto -> 2732
    //   1210: iload #10
    //   1212: ifne -> 1241
    //   1215: aload_0
    //   1216: getfield mParentSurfaceGenerationId : I
    //   1219: istore #18
    //   1221: aload_1
    //   1222: getfield mSurface : Landroid/view/Surface;
    //   1225: astore #14
    //   1227: aload #14
    //   1229: invokevirtual getGenerationId : ()I
    //   1232: istore #12
    //   1234: iload #18
    //   1236: iload #12
    //   1238: if_icmpne -> 1249
    //   1241: aload_0
    //   1242: aload_0
    //   1243: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1246: invokespecial updateRelativeZ : (Landroid/view/SurfaceControl$Transaction;)V
    //   1249: aload_0
    //   1250: aload_1
    //   1251: getfield mSurface : Landroid/view/Surface;
    //   1254: invokevirtual getGenerationId : ()I
    //   1257: putfield mParentSurfaceGenerationId : I
    //   1260: aload_0
    //   1261: getfield mViewVisibility : Z
    //   1264: istore #9
    //   1266: iload #9
    //   1268: ifeq -> 1286
    //   1271: aload_0
    //   1272: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1275: aload_0
    //   1276: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1279: invokevirtual show : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    //   1282: pop
    //   1283: goto -> 1298
    //   1286: aload_0
    //   1287: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1290: aload_0
    //   1291: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1294: invokevirtual hide : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    //   1297: pop
    //   1298: aload_0
    //   1299: getfield mSurfacePackage : Landroid/view/SurfaceControlViewHost$SurfacePackage;
    //   1302: astore #14
    //   1304: aload #14
    //   1306: ifnull -> 1321
    //   1309: aload_0
    //   1310: aload_0
    //   1311: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1314: aload_0
    //   1315: getfield mSurfacePackage : Landroid/view/SurfaceControlViewHost$SurfacePackage;
    //   1318: invokespecial reparentSurfacePackage : (Landroid/view/SurfaceControl$Transaction;Landroid/view/SurfaceControlViewHost$SurfacePackage;)V
    //   1321: aload_0
    //   1322: aload_0
    //   1323: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1326: invokespecial updateBackgroundVisibility : (Landroid/view/SurfaceControl$Transaction;)V
    //   1329: aload_0
    //   1330: aload_0
    //   1331: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1334: invokespecial updateBackgroundColor : (Landroid/view/SurfaceControl$Transaction;)Landroid/view/SurfaceControl$Transaction;
    //   1337: pop
    //   1338: aload_0
    //   1339: getfield mUseAlpha : Z
    //   1342: istore #9
    //   1344: iload #9
    //   1346: ifeq -> 1369
    //   1349: aload_0
    //   1350: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1353: aload_0
    //   1354: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1357: fload #6
    //   1359: invokevirtual setAlpha : (Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
    //   1362: pop
    //   1363: aload_0
    //   1364: fload #6
    //   1366: putfield mSurfaceAlpha : F
    //   1369: iload #11
    //   1371: ifne -> 1427
    //   1374: iload #10
    //   1376: ifne -> 1427
    //   1379: aload_0
    //   1380: getfield mRtHandlingPositionUpdates : Z
    //   1383: ifne -> 1389
    //   1386: goto -> 1427
    //   1389: iload #15
    //   1391: ifne -> 1410
    //   1394: iload #5
    //   1396: ifne -> 1410
    //   1399: iload #8
    //   1401: ifeq -> 1407
    //   1404: goto -> 1410
    //   1407: goto -> 1582
    //   1410: aload_1
    //   1411: invokevirtual useBLAST : ()Z
    //   1414: ifeq -> 1424
    //   1417: aload_1
    //   1418: invokevirtual setUseBLASTSyncTransaction : ()V
    //   1421: goto -> 1582
    //   1424: goto -> 1582
    //   1427: aload_0
    //   1428: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1431: astore #17
    //   1433: aload_0
    //   1434: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1437: astore #14
    //   1439: aload_0
    //   1440: getfield mScreenRect : Landroid/graphics/Rect;
    //   1443: getfield left : I
    //   1446: istore #15
    //   1448: aload_0
    //   1449: getfield mScreenRect : Landroid/graphics/Rect;
    //   1452: getfield top : I
    //   1455: istore #5
    //   1457: aload_0
    //   1458: getfield mScreenRect : Landroid/graphics/Rect;
    //   1461: astore #19
    //   1463: aload #19
    //   1465: invokevirtual width : ()I
    //   1468: istore #12
    //   1470: iload #12
    //   1472: i2f
    //   1473: fstore #6
    //   1475: fload #6
    //   1477: aload_0
    //   1478: getfield mSurfaceWidth : I
    //   1481: i2f
    //   1482: fdiv
    //   1483: fstore #6
    //   1485: aload_0
    //   1486: getfield mScreenRect : Landroid/graphics/Rect;
    //   1489: astore #19
    //   1491: aload #19
    //   1493: invokevirtual height : ()I
    //   1496: i2f
    //   1497: fstore #20
    //   1499: aload_0
    //   1500: getfield mSurfaceHeight : I
    //   1503: istore #12
    //   1505: fload #20
    //   1507: iload #12
    //   1509: i2f
    //   1510: fdiv
    //   1511: fstore #20
    //   1513: aload_0
    //   1514: aload #17
    //   1516: aload #14
    //   1518: iload #15
    //   1520: iload #5
    //   1522: fload #6
    //   1524: fload #20
    //   1526: invokevirtual onSetSurfacePositionAndScaleRT : (Landroid/view/SurfaceControl$Transaction;Landroid/view/SurfaceControl;IIFF)V
    //   1529: aload_0
    //   1530: getfield mClipSurfaceToBounds : Z
    //   1533: ifeq -> 1562
    //   1536: aload_0
    //   1537: getfield mClipBounds : Landroid/graphics/Rect;
    //   1540: ifnull -> 1562
    //   1543: aload_0
    //   1544: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1547: aload_0
    //   1548: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1551: aload_0
    //   1552: getfield mClipBounds : Landroid/graphics/Rect;
    //   1555: invokevirtual setWindowCrop : (Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
    //   1558: pop
    //   1559: goto -> 1582
    //   1562: aload_0
    //   1563: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1566: aload_0
    //   1567: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1570: aload_0
    //   1571: getfield mSurfaceWidth : I
    //   1574: aload_0
    //   1575: getfield mSurfaceHeight : I
    //   1578: invokevirtual setWindowCrop : (Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;
    //   1581: pop
    //   1582: aload_0
    //   1583: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1586: aload_0
    //   1587: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1590: aload_0
    //   1591: getfield mCornerRadius : F
    //   1594: invokevirtual setCornerRadius : (Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
    //   1597: pop
    //   1598: iload #11
    //   1600: ifeq -> 1628
    //   1603: iload #10
    //   1605: ifne -> 1628
    //   1608: aload_0
    //   1609: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1612: aload_0
    //   1613: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1616: aload_0
    //   1617: getfield mSurfaceWidth : I
    //   1620: aload_0
    //   1621: getfield mSurfaceHeight : I
    //   1624: invokevirtual setBufferSize : (Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;
    //   1627: pop
    //   1628: aload_0
    //   1629: getfield mTmpTransaction : Landroid/view/SurfaceControl$Transaction;
    //   1632: invokevirtual apply : ()V
    //   1635: aload_0
    //   1636: invokespecial updateScreenMatrixForEmbeddedHierarchy : ()V
    //   1639: iload #11
    //   1641: ifne -> 1653
    //   1644: iload #13
    //   1646: istore #5
    //   1648: iload #10
    //   1650: ifeq -> 1656
    //   1653: iconst_1
    //   1654: istore #5
    //   1656: aload_0
    //   1657: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   1660: iconst_0
    //   1661: putfield left : I
    //   1664: aload_0
    //   1665: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   1668: iconst_0
    //   1669: putfield top : I
    //   1672: aload_2
    //   1673: ifnonnull -> 1701
    //   1676: aload_0
    //   1677: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   1680: aload_0
    //   1681: getfield mSurfaceWidth : I
    //   1684: putfield right : I
    //   1687: aload_0
    //   1688: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   1691: aload_0
    //   1692: getfield mSurfaceHeight : I
    //   1695: putfield bottom : I
    //   1698: goto -> 1747
    //   1701: aload_2
    //   1702: getfield applicationInvertedScale : F
    //   1705: fstore #6
    //   1707: aload_0
    //   1708: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   1711: aload_0
    //   1712: getfield mSurfaceWidth : I
    //   1715: i2f
    //   1716: fload #6
    //   1718: fmul
    //   1719: ldc_w 0.5
    //   1722: fadd
    //   1723: f2i
    //   1724: putfield right : I
    //   1727: aload_0
    //   1728: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   1731: aload_0
    //   1732: getfield mSurfaceHeight : I
    //   1735: i2f
    //   1736: fload #6
    //   1738: fmul
    //   1739: ldc_w 0.5
    //   1742: fadd
    //   1743: f2i
    //   1744: putfield bottom : I
    //   1747: aload_0
    //   1748: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   1751: getfield right : I
    //   1754: istore #13
    //   1756: aload_0
    //   1757: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   1760: getfield bottom : I
    //   1763: istore #12
    //   1765: aload_0
    //   1766: getfield mLastSurfaceWidth : I
    //   1769: iload #13
    //   1771: if_icmpne -> 1792
    //   1774: aload_0
    //   1775: getfield mLastSurfaceHeight : I
    //   1778: iload #12
    //   1780: if_icmpeq -> 1786
    //   1783: goto -> 1792
    //   1786: iconst_0
    //   1787: istore #15
    //   1789: goto -> 1795
    //   1792: iconst_1
    //   1793: istore #15
    //   1795: aload_0
    //   1796: iload #13
    //   1798: putfield mLastSurfaceWidth : I
    //   1801: aload_0
    //   1802: iload #12
    //   1804: putfield mLastSurfaceHeight : I
    //   1807: aload_0
    //   1808: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
    //   1811: invokevirtual unlock : ()V
    //   1814: iload #16
    //   1816: ifeq -> 1837
    //   1819: aload_0
    //   1820: getfield mDrawFinished : Z
    //   1823: ifne -> 1837
    //   1826: iconst_1
    //   1827: istore #12
    //   1829: goto -> 1840
    //   1832: astore #14
    //   1834: goto -> 2545
    //   1837: iconst_0
    //   1838: istore #12
    //   1840: aconst_null
    //   1841: astore #17
    //   1843: aconst_null
    //   1844: astore #14
    //   1846: aload_0
    //   1847: getfield mSurfaceCreated : Z
    //   1850: ifeq -> 1891
    //   1853: iload #10
    //   1855: ifne -> 1868
    //   1858: iload #16
    //   1860: ifne -> 1891
    //   1863: iload #8
    //   1865: ifeq -> 1891
    //   1868: aload_0
    //   1869: iconst_0
    //   1870: putfield mSurfaceCreated : Z
    //   1873: aload_0
    //   1874: getfield mSurface : Landroid/view/Surface;
    //   1877: invokevirtual isValid : ()Z
    //   1880: ifeq -> 1887
    //   1883: aload_0
    //   1884: invokespecial unRegisterHqvListen : ()V
    //   1887: aload_0
    //   1888: invokespecial notifySurfaceDestroyed : ()V
    //   1891: iload #10
    //   1893: ifeq -> 1907
    //   1896: aload_0
    //   1897: getfield mSurface : Landroid/view/Surface;
    //   1900: aload_0
    //   1901: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1904: invokevirtual copyFrom : (Landroid/view/SurfaceControl;)V
    //   1907: iload #11
    //   1909: ifeq -> 1938
    //   1912: aload_0
    //   1913: invokevirtual getContext : ()Landroid/content/Context;
    //   1916: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   1919: getfield targetSdkVersion : I
    //   1922: bipush #26
    //   1924: if_icmpge -> 1938
    //   1927: aload_0
    //   1928: getfield mSurface : Landroid/view/Surface;
    //   1931: aload_0
    //   1932: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   1935: invokevirtual createFrom : (Landroid/view/SurfaceControl;)V
    //   1938: iload #16
    //   1940: ifeq -> 2368
    //   1943: aload_0
    //   1944: getfield mSurface : Landroid/view/Surface;
    //   1947: invokevirtual isValid : ()Z
    //   1950: ifeq -> 2368
    //   1953: aload #17
    //   1955: astore_2
    //   1956: aload_0
    //   1957: getfield mSurfaceCreated : Z
    //   1960: ifne -> 2081
    //   1963: iload #10
    //   1965: ifne -> 1976
    //   1968: aload #17
    //   1970: astore_2
    //   1971: iload #8
    //   1973: ifeq -> 2081
    //   1976: aload_0
    //   1977: iconst_1
    //   1978: putfield mSurfaceCreated : Z
    //   1981: aload_0
    //   1982: iconst_1
    //   1983: putfield mIsCreating : Z
    //   1986: getstatic android/view/SurfaceView.DEBUG : Z
    //   1989: ifeq -> 2027
    //   1992: new java/lang/StringBuilder
    //   1995: astore_2
    //   1996: aload_2
    //   1997: invokespecial <init> : ()V
    //   2000: aload_2
    //   2001: aload_0
    //   2002: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   2005: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2008: pop
    //   2009: aload_2
    //   2010: ldc_w ' visibleChanged -- surfaceCreated'
    //   2013: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2016: pop
    //   2017: ldc 'SurfaceView'
    //   2019: aload_2
    //   2020: invokevirtual toString : ()Ljava/lang/String;
    //   2023: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   2026: pop
    //   2027: aload_0
    //   2028: invokespecial registerHqvListen : ()V
    //   2031: iconst_0
    //   2032: ifne -> 2041
    //   2035: aload_0
    //   2036: invokespecial getSurfaceCallbacks : ()[Landroid/view/SurfaceHolder$Callback;
    //   2039: astore #14
    //   2041: aload #14
    //   2043: arraylength
    //   2044: istore #18
    //   2046: iconst_0
    //   2047: istore #13
    //   2049: aload #14
    //   2051: astore_2
    //   2052: iload #13
    //   2054: iload #18
    //   2056: if_icmpge -> 2081
    //   2059: aload #14
    //   2061: iload #13
    //   2063: aaload
    //   2064: astore_2
    //   2065: aload_2
    //   2066: aload_0
    //   2067: getfield mSurfaceHolder : Landroid/view/SurfaceHolder;
    //   2070: invokeinterface surfaceCreated : (Landroid/view/SurfaceHolder;)V
    //   2075: iinc #13, 1
    //   2078: goto -> 2049
    //   2081: iload #10
    //   2083: ifne -> 2109
    //   2086: iload #7
    //   2088: ifne -> 2109
    //   2091: iload #11
    //   2093: ifne -> 2109
    //   2096: iload #8
    //   2098: ifne -> 2109
    //   2101: aload_2
    //   2102: astore #14
    //   2104: iload #15
    //   2106: ifeq -> 2259
    //   2109: getstatic android/view/SurfaceView.DEBUG : Z
    //   2112: ifeq -> 2198
    //   2115: new java/lang/StringBuilder
    //   2118: astore #14
    //   2120: aload #14
    //   2122: invokespecial <init> : ()V
    //   2125: aload #14
    //   2127: aload_0
    //   2128: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   2131: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2134: pop
    //   2135: aload #14
    //   2137: ldc_w ' surfaceChanged -- format='
    //   2140: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2143: pop
    //   2144: aload #14
    //   2146: aload_0
    //   2147: getfield mFormat : I
    //   2150: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2153: pop
    //   2154: aload #14
    //   2156: ldc_w ' w='
    //   2159: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2162: pop
    //   2163: aload #14
    //   2165: iload #4
    //   2167: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2170: pop
    //   2171: aload #14
    //   2173: ldc_w ' h='
    //   2176: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2179: pop
    //   2180: aload #14
    //   2182: iload_3
    //   2183: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2186: pop
    //   2187: ldc 'SurfaceView'
    //   2189: aload #14
    //   2191: invokevirtual toString : ()Ljava/lang/String;
    //   2194: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   2197: pop
    //   2198: aload_2
    //   2199: astore #17
    //   2201: aload_2
    //   2202: ifnonnull -> 2211
    //   2205: aload_0
    //   2206: invokespecial getSurfaceCallbacks : ()[Landroid/view/SurfaceHolder$Callback;
    //   2209: astore #17
    //   2211: aload #17
    //   2213: arraylength
    //   2214: istore #13
    //   2216: iconst_0
    //   2217: istore #15
    //   2219: aload #17
    //   2221: astore #14
    //   2223: iload #15
    //   2225: iload #13
    //   2227: if_icmpge -> 2259
    //   2230: aload #17
    //   2232: iload #15
    //   2234: aaload
    //   2235: astore_2
    //   2236: aload_2
    //   2237: aload_0
    //   2238: getfield mSurfaceHolder : Landroid/view/SurfaceHolder;
    //   2241: aload_0
    //   2242: getfield mFormat : I
    //   2245: iload #4
    //   2247: iload_3
    //   2248: invokeinterface surfaceChanged : (Landroid/view/SurfaceHolder;III)V
    //   2253: iinc #15, 1
    //   2256: goto -> 2219
    //   2259: iload #5
    //   2261: iload #12
    //   2263: ior
    //   2264: ifeq -> 2368
    //   2267: getstatic android/view/SurfaceView.DEBUG : Z
    //   2270: ifeq -> 2308
    //   2273: new java/lang/StringBuilder
    //   2276: astore_2
    //   2277: aload_2
    //   2278: invokespecial <init> : ()V
    //   2281: aload_2
    //   2282: aload_0
    //   2283: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   2286: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2289: pop
    //   2290: aload_2
    //   2291: ldc_w ' surfaceRedrawNeeded'
    //   2294: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2297: pop
    //   2298: ldc 'SurfaceView'
    //   2300: aload_2
    //   2301: invokevirtual toString : ()Ljava/lang/String;
    //   2304: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   2307: pop
    //   2308: aload #14
    //   2310: astore_2
    //   2311: aload #14
    //   2313: ifnonnull -> 2321
    //   2316: aload_0
    //   2317: invokespecial getSurfaceCallbacks : ()[Landroid/view/SurfaceHolder$Callback;
    //   2320: astore_2
    //   2321: aload_0
    //   2322: aload_0
    //   2323: getfield mPendingReportDraws : I
    //   2326: iconst_1
    //   2327: iadd
    //   2328: putfield mPendingReportDraws : I
    //   2331: aload_1
    //   2332: invokevirtual drawPending : ()V
    //   2335: new com/android/internal/view/SurfaceCallbackHelper
    //   2338: astore #17
    //   2340: new android/view/_$$Lambda$SurfaceView$SyyzxOgxKwZMRgiiTGcRYbOU5JY
    //   2343: astore #14
    //   2345: aload #14
    //   2347: aload_0
    //   2348: invokespecial <init> : (Landroid/view/SurfaceView;)V
    //   2351: aload #17
    //   2353: aload #14
    //   2355: invokespecial <init> : (Ljava/lang/Runnable;)V
    //   2358: aload #17
    //   2360: aload_0
    //   2361: getfield mSurfaceHolder : Landroid/view/SurfaceHolder;
    //   2364: aload_2
    //   2365: invokevirtual dispatchSurfaceRedrawNeededAsync : (Landroid/view/SurfaceHolder;[Landroid/view/SurfaceHolder$Callback;)V
    //   2368: aload_0
    //   2369: iconst_0
    //   2370: putfield mIsCreating : Z
    //   2373: aload_0
    //   2374: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   2377: ifnull -> 2542
    //   2380: aload_0
    //   2381: getfield mSurfaceCreated : Z
    //   2384: ifne -> 2542
    //   2387: aload_1
    //   2388: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   2391: ifnonnull -> 2399
    //   2394: aconst_null
    //   2395: astore_2
    //   2396: goto -> 2409
    //   2399: aload_1
    //   2400: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   2403: invokeinterface toString : ()Ljava/lang/String;
    //   2408: astore_2
    //   2409: aload_2
    //   2410: ifnull -> 2538
    //   2413: aload_2
    //   2414: ldc_w 'com.oppo.camera'
    //   2417: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2420: ifne -> 2493
    //   2423: aload_2
    //   2424: ldc_w 'com.coloros.compass'
    //   2427: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2430: ifne -> 2493
    //   2433: aload_2
    //   2434: ldc_w 'com.coloros.gallery3d'
    //   2437: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2440: ifne -> 2493
    //   2443: aload_2
    //   2444: ldc_w 'com.heytap.speechassist'
    //   2447: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2450: ifne -> 2493
    //   2453: aload_2
    //   2454: ldc_w 'com.tencent.qqlive'
    //   2457: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2460: ifne -> 2493
    //   2463: aload_2
    //   2464: ldc_w 'com.coloros.weather2'
    //   2467: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2470: ifne -> 2493
    //   2473: aload_2
    //   2474: ldc_w 'com.tencent.tmgp.sgame'
    //   2477: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2480: ifne -> 2493
    //   2483: aload_2
    //   2484: ldc_w 'com.tencent.mm.plugin.voip.ui.VideoActivity'
    //   2487: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2490: ifeq -> 2538
    //   2493: aload_0
    //   2494: fconst_1
    //   2495: putfield mSurfaceAlpha : F
    //   2498: aload_0
    //   2499: getfield mSurfaceControlLock : Ljava/lang/Object;
    //   2502: astore #14
    //   2504: aload #14
    //   2506: monitorenter
    //   2507: aload_0
    //   2508: getfield mSurface : Landroid/view/Surface;
    //   2511: invokevirtual release : ()V
    //   2514: aload_0
    //   2515: getfield mRtHandlingPositionUpdates : Z
    //   2518: ifeq -> 2526
    //   2521: aload_0
    //   2522: iconst_1
    //   2523: putfield mRtReleaseSurfaces : Z
    //   2526: aload #14
    //   2528: monitorexit
    //   2529: goto -> 2542
    //   2532: astore_2
    //   2533: aload #14
    //   2535: monitorexit
    //   2536: aload_2
    //   2537: athrow
    //   2538: aload_0
    //   2539: invokespecial releaseSurfaces : ()V
    //   2542: goto -> 2756
    //   2545: aload_0
    //   2546: iconst_0
    //   2547: putfield mIsCreating : Z
    //   2550: aload_0
    //   2551: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   2554: ifnull -> 2720
    //   2557: aload_0
    //   2558: getfield mSurfaceCreated : Z
    //   2561: ifne -> 2720
    //   2564: aload_1
    //   2565: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   2568: ifnonnull -> 2576
    //   2571: aconst_null
    //   2572: astore_2
    //   2573: goto -> 2586
    //   2576: aload_1
    //   2577: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   2580: invokeinterface toString : ()Ljava/lang/String;
    //   2585: astore_2
    //   2586: aload_2
    //   2587: ifnull -> 2716
    //   2590: aload_2
    //   2591: ldc_w 'com.oppo.camera'
    //   2594: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2597: ifne -> 2670
    //   2600: aload_2
    //   2601: ldc_w 'com.coloros.compass'
    //   2604: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2607: ifne -> 2670
    //   2610: aload_2
    //   2611: ldc_w 'com.coloros.gallery3d'
    //   2614: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2617: ifne -> 2670
    //   2620: aload_2
    //   2621: ldc_w 'com.heytap.speechassist'
    //   2624: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2627: ifne -> 2670
    //   2630: aload_2
    //   2631: ldc_w 'com.tencent.qqlive'
    //   2634: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2637: ifne -> 2670
    //   2640: aload_2
    //   2641: ldc_w 'com.coloros.weather2'
    //   2644: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2647: ifne -> 2670
    //   2650: aload_2
    //   2651: ldc_w 'com.tencent.tmgp.sgame'
    //   2654: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2657: ifne -> 2670
    //   2660: aload_2
    //   2661: ldc_w 'com.tencent.mm.plugin.voip.ui.VideoActivity'
    //   2664: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   2667: ifeq -> 2716
    //   2670: aload_0
    //   2671: fconst_1
    //   2672: putfield mSurfaceAlpha : F
    //   2675: aload_0
    //   2676: getfield mSurfaceControlLock : Ljava/lang/Object;
    //   2679: astore #17
    //   2681: aload #17
    //   2683: monitorenter
    //   2684: aload_0
    //   2685: getfield mSurface : Landroid/view/Surface;
    //   2688: invokevirtual release : ()V
    //   2691: aload_0
    //   2692: getfield mRtHandlingPositionUpdates : Z
    //   2695: ifeq -> 2703
    //   2698: aload_0
    //   2699: iconst_1
    //   2700: putfield mRtReleaseSurfaces : Z
    //   2703: aload #17
    //   2705: monitorexit
    //   2706: goto -> 2720
    //   2709: astore_2
    //   2710: aload #17
    //   2712: monitorexit
    //   2713: goto -> 2536
    //   2716: aload_0
    //   2717: invokespecial releaseSurfaces : ()V
    //   2720: aload #14
    //   2722: athrow
    //   2723: astore_2
    //   2724: goto -> 2732
    //   2727: astore_2
    //   2728: goto -> 2732
    //   2731: astore_2
    //   2732: aload_0
    //   2733: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
    //   2736: invokevirtual unlock : ()V
    //   2739: aload_2
    //   2740: athrow
    //   2741: astore_2
    //   2742: goto -> 2746
    //   2745: astore_2
    //   2746: ldc 'SurfaceView'
    //   2748: ldc_w 'Exception configuring surface'
    //   2751: aload_2
    //   2752: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   2755: pop
    //   2756: getstatic android/view/SurfaceView.DEBUG : Z
    //   2759: ifeq -> 2883
    //   2762: new java/lang/StringBuilder
    //   2765: dup
    //   2766: invokespecial <init> : ()V
    //   2769: astore_2
    //   2770: aload_2
    //   2771: ldc_w 'Layout: x='
    //   2774: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2777: pop
    //   2778: aload_2
    //   2779: aload_0
    //   2780: getfield mScreenRect : Landroid/graphics/Rect;
    //   2783: getfield left : I
    //   2786: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2789: pop
    //   2790: aload_2
    //   2791: ldc_w ' y='
    //   2794: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2797: pop
    //   2798: aload_2
    //   2799: aload_0
    //   2800: getfield mScreenRect : Landroid/graphics/Rect;
    //   2803: getfield top : I
    //   2806: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2809: pop
    //   2810: aload_2
    //   2811: ldc_w ' w='
    //   2814: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2817: pop
    //   2818: aload_0
    //   2819: getfield mScreenRect : Landroid/graphics/Rect;
    //   2822: astore #14
    //   2824: aload_2
    //   2825: aload #14
    //   2827: invokevirtual width : ()I
    //   2830: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2833: pop
    //   2834: aload_2
    //   2835: ldc_w ' h='
    //   2838: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2841: pop
    //   2842: aload_2
    //   2843: aload_0
    //   2844: getfield mScreenRect : Landroid/graphics/Rect;
    //   2847: invokevirtual height : ()I
    //   2850: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2853: pop
    //   2854: aload_2
    //   2855: ldc_w ', frame='
    //   2858: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2861: pop
    //   2862: aload_2
    //   2863: aload_0
    //   2864: getfield mSurfaceFrame : Landroid/graphics/Rect;
    //   2867: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2870: pop
    //   2871: aload_2
    //   2872: invokevirtual toString : ()Ljava/lang/String;
    //   2875: astore_2
    //   2876: ldc 'SurfaceView'
    //   2878: aload_2
    //   2879: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   2882: pop
    //   2883: return
    //   2884: aload_0
    //   2885: invokespecial notifySurfaceDestroyed : ()V
    //   2888: aload_0
    //   2889: invokespecial releaseSurfaces : ()V
    //   2892: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1009	-> 0
    //   #1016	-> 7
    //   #1018	-> 8
    //   #1020	-> 13
    //   #1021	-> 17
    //   #1024	-> 18
    //   #1030	-> 38
    //   #1031	-> 43
    //   #1032	-> 47
    //   #1035	-> 55
    //   #1036	-> 60
    //   #1037	-> 73
    //   #1038	-> 79
    //   #1040	-> 92
    //   #1041	-> 98
    //   #1042	-> 118
    //   #1043	-> 138
    //   #1044	-> 157
    //   #1046	-> 190
    //   #1047	-> 219
    //   #1048	-> 239
    //   #1049	-> 242
    //   #1050	-> 250
    //   #1052	-> 294
    //   #1053	-> 308
    //   #1056	-> 334
    //   #1059	-> 387
    //   #1061	-> 395
    //   #1071	-> 630
    //   #1072	-> 642
    //   #1073	-> 656
    //   #1074	-> 666
    //   #1075	-> 672
    //   #1076	-> 677
    //   #1077	-> 685
    //   #1079	-> 693
    //   #1080	-> 702
    //   #1081	-> 713
    //   #1082	-> 729
    //   #1083	-> 745
    //   #1084	-> 749
    //   #1338	-> 760
    //   #1087	-> 764
    //   #1088	-> 773
    //   #1090	-> 790
    //   #1091	-> 795
    //   #1093	-> 803
    //   #1109	-> 807
    //   #1111	-> 848
    //   #1112	-> 862
    //   #1113	-> 871
    //   #1114	-> 899
    //   #1115	-> 920
    //   #1116	-> 937
    //   #1117	-> 946
    //   #1118	-> 963
    //   #1119	-> 972
    //   #1120	-> 982
    //   #1121	-> 991
    //   #1122	-> 1039
    //   #1123	-> 1048
    //   #1124	-> 1056
    //   #1125	-> 1064
    //   #1126	-> 1077
    //   #1127	-> 1086
    //   #1128	-> 1096
    //   #1130	-> 1108
    //   #1131	-> 1115
    //   #1130	-> 1116
    //   #1134	-> 1116
    //   #1136	-> 1116
    //   #1138	-> 1123
    //   #1140	-> 1143
    //   #1231	-> 1206
    //   #1148	-> 1210
    //   #1149	-> 1227
    //   #1150	-> 1241
    //   #1152	-> 1249
    //   #1154	-> 1260
    //   #1155	-> 1271
    //   #1157	-> 1286
    //   #1160	-> 1298
    //   #1161	-> 1309
    //   #1164	-> 1321
    //   #1165	-> 1329
    //   #1166	-> 1338
    //   #1167	-> 1349
    //   #1168	-> 1363
    //   #1179	-> 1369
    //   #1196	-> 1389
    //   #1197	-> 1410
    //   #1198	-> 1417
    //   #1197	-> 1424
    //   #1180	-> 1427
    //   #1183	-> 1463
    //   #1184	-> 1491
    //   #1180	-> 1513
    //   #1190	-> 1529
    //   #1191	-> 1543
    //   #1193	-> 1562
    //   #1200	-> 1582
    //   #1201	-> 1598
    //   #1202	-> 1608
    //   #1206	-> 1628
    //   #1207	-> 1635
    //   #1209	-> 1639
    //   #1210	-> 1653
    //   #1213	-> 1656
    //   #1214	-> 1664
    //   #1215	-> 1672
    //   #1216	-> 1676
    //   #1217	-> 1687
    //   #1219	-> 1701
    //   #1220	-> 1707
    //   #1221	-> 1727
    //   #1224	-> 1747
    //   #1225	-> 1756
    //   #1226	-> 1765
    //   #1228	-> 1795
    //   #1229	-> 1801
    //   #1231	-> 1807
    //   #1232	-> 1814
    //   #1235	-> 1814
    //   #1309	-> 1832
    //   #1235	-> 1837
    //   #1237	-> 1840
    //   #1239	-> 1846
    //   #1240	-> 1846
    //   #1241	-> 1868
    //   #1244	-> 1873
    //   #1245	-> 1883
    //   #1248	-> 1887
    //   #1251	-> 1891
    //   #1252	-> 1896
    //   #1255	-> 1907
    //   #1262	-> 1927
    //   #1265	-> 1938
    //   #1266	-> 1953
    //   #1267	-> 1976
    //   #1268	-> 1981
    //   #1269	-> 1986
    //   #1273	-> 2027
    //   #1275	-> 2031
    //   #1276	-> 2035
    //   #1278	-> 2041
    //   #1279	-> 2065
    //   #1278	-> 2075
    //   #1282	-> 2081
    //   #1284	-> 2109
    //   #1287	-> 2198
    //   #1288	-> 2205
    //   #1290	-> 2211
    //   #1291	-> 2236
    //   #1290	-> 2253
    //   #1294	-> 2259
    //   #1295	-> 2267
    //   #1297	-> 2308
    //   #1298	-> 2316
    //   #1301	-> 2321
    //   #1302	-> 2331
    //   #1303	-> 2335
    //   #1305	-> 2358
    //   #1309	-> 2368
    //   #1310	-> 2373
    //   #1315	-> 2387
    //   #1316	-> 2409
    //   #1317	-> 2413
    //   #1318	-> 2423
    //   #1319	-> 2433
    //   #1320	-> 2443
    //   #1321	-> 2453
    //   #1322	-> 2463
    //   #1323	-> 2473
    //   #1324	-> 2483
    //   #1325	-> 2493
    //   #1326	-> 2498
    //   #1327	-> 2507
    //   #1328	-> 2514
    //   #1329	-> 2521
    //   #1331	-> 2526
    //   #1333	-> 2538
    //   #1340	-> 2542
    //   #1309	-> 2545
    //   #1310	-> 2550
    //   #1315	-> 2564
    //   #1316	-> 2586
    //   #1317	-> 2590
    //   #1318	-> 2600
    //   #1319	-> 2610
    //   #1320	-> 2620
    //   #1321	-> 2630
    //   #1322	-> 2640
    //   #1323	-> 2650
    //   #1324	-> 2660
    //   #1325	-> 2670
    //   #1326	-> 2675
    //   #1327	-> 2684
    //   #1328	-> 2691
    //   #1329	-> 2698
    //   #1331	-> 2703
    //   #1333	-> 2716
    //   #1337	-> 2720
    //   #1231	-> 2723
    //   #1232	-> 2739
    //   #1338	-> 2741
    //   #1339	-> 2746
    //   #1341	-> 2756
    //   #1343	-> 2824
    //   #1341	-> 2876
    //   #1346	-> 2883
    //   #1025	-> 2884
    //   #1026	-> 2888
    //   #1027	-> 2892
    // Exception table:
    //   from	to	target	type
    //   630	642	2745	java/lang/Exception
    //   642	656	2745	java/lang/Exception
    //   656	666	2745	java/lang/Exception
    //   666	672	2745	java/lang/Exception
    //   672	677	2745	java/lang/Exception
    //   677	685	2745	java/lang/Exception
    //   685	693	2745	java/lang/Exception
    //   693	702	2745	java/lang/Exception
    //   702	713	2745	java/lang/Exception
    //   713	729	2745	java/lang/Exception
    //   729	745	2745	java/lang/Exception
    //   749	757	760	java/lang/Exception
    //   764	773	2745	java/lang/Exception
    //   773	790	2745	java/lang/Exception
    //   795	803	760	java/lang/Exception
    //   803	807	760	java/lang/Exception
    //   807	848	760	java/lang/Exception
    //   848	862	760	java/lang/Exception
    //   862	871	760	java/lang/Exception
    //   871	890	760	java/lang/Exception
    //   899	920	760	java/lang/Exception
    //   920	937	760	java/lang/Exception
    //   937	946	760	java/lang/Exception
    //   946	963	760	java/lang/Exception
    //   963	972	760	java/lang/Exception
    //   972	982	760	java/lang/Exception
    //   982	991	760	java/lang/Exception
    //   991	1039	760	java/lang/Exception
    //   1039	1048	760	java/lang/Exception
    //   1048	1056	760	java/lang/Exception
    //   1056	1064	760	java/lang/Exception
    //   1064	1077	760	java/lang/Exception
    //   1077	1086	760	java/lang/Exception
    //   1086	1096	760	java/lang/Exception
    //   1096	1105	760	java/lang/Exception
    //   1108	1115	2745	java/lang/Exception
    //   1116	1123	2745	java/lang/Exception
    //   1137	1143	2731	finally
    //   1143	1148	2731	finally
    //   1153	1203	1206	finally
    //   1215	1227	1206	finally
    //   1227	1234	1206	finally
    //   1241	1249	2731	finally
    //   1249	1260	2731	finally
    //   1260	1266	2731	finally
    //   1271	1283	1206	finally
    //   1286	1298	2731	finally
    //   1298	1304	2731	finally
    //   1309	1321	1206	finally
    //   1321	1329	2731	finally
    //   1329	1338	2731	finally
    //   1338	1344	2731	finally
    //   1349	1363	1206	finally
    //   1363	1369	1206	finally
    //   1379	1386	1206	finally
    //   1410	1417	1206	finally
    //   1417	1421	1206	finally
    //   1427	1463	2731	finally
    //   1463	1470	2731	finally
    //   1475	1491	2727	finally
    //   1491	1505	2727	finally
    //   1513	1529	2723	finally
    //   1529	1543	2723	finally
    //   1543	1559	2723	finally
    //   1562	1582	2723	finally
    //   1582	1598	2723	finally
    //   1608	1628	2723	finally
    //   1628	1635	2723	finally
    //   1635	1639	2723	finally
    //   1656	1664	2723	finally
    //   1664	1672	2723	finally
    //   1676	1687	2723	finally
    //   1687	1698	2723	finally
    //   1701	1707	2723	finally
    //   1707	1727	2723	finally
    //   1727	1747	2723	finally
    //   1747	1756	2723	finally
    //   1756	1765	2723	finally
    //   1765	1783	2723	finally
    //   1795	1801	2723	finally
    //   1801	1807	2723	finally
    //   1807	1814	2741	java/lang/Exception
    //   1819	1826	1832	finally
    //   1846	1853	1832	finally
    //   1868	1873	1832	finally
    //   1873	1883	1832	finally
    //   1883	1887	1832	finally
    //   1887	1891	1832	finally
    //   1896	1907	1832	finally
    //   1912	1927	1832	finally
    //   1927	1938	1832	finally
    //   1943	1953	1832	finally
    //   1956	1963	1832	finally
    //   1976	1981	1832	finally
    //   1981	1986	1832	finally
    //   1986	2027	1832	finally
    //   2027	2031	1832	finally
    //   2035	2041	1832	finally
    //   2041	2046	1832	finally
    //   2065	2075	1832	finally
    //   2109	2198	1832	finally
    //   2205	2211	1832	finally
    //   2211	2216	1832	finally
    //   2236	2253	1832	finally
    //   2267	2308	1832	finally
    //   2316	2321	1832	finally
    //   2321	2331	1832	finally
    //   2331	2335	1832	finally
    //   2335	2358	1832	finally
    //   2358	2368	1832	finally
    //   2368	2373	2741	java/lang/Exception
    //   2373	2387	2741	java/lang/Exception
    //   2387	2394	2741	java/lang/Exception
    //   2399	2409	2741	java/lang/Exception
    //   2413	2423	2741	java/lang/Exception
    //   2423	2433	2741	java/lang/Exception
    //   2433	2443	2741	java/lang/Exception
    //   2443	2453	2741	java/lang/Exception
    //   2453	2463	2741	java/lang/Exception
    //   2463	2473	2741	java/lang/Exception
    //   2473	2483	2741	java/lang/Exception
    //   2483	2493	2741	java/lang/Exception
    //   2493	2498	2741	java/lang/Exception
    //   2498	2507	2741	java/lang/Exception
    //   2507	2514	2532	finally
    //   2514	2521	2532	finally
    //   2521	2526	2532	finally
    //   2526	2529	2532	finally
    //   2533	2536	2532	finally
    //   2536	2538	2741	java/lang/Exception
    //   2538	2542	2741	java/lang/Exception
    //   2545	2550	2741	java/lang/Exception
    //   2550	2564	2741	java/lang/Exception
    //   2564	2571	2741	java/lang/Exception
    //   2576	2586	2741	java/lang/Exception
    //   2590	2600	2741	java/lang/Exception
    //   2600	2610	2741	java/lang/Exception
    //   2610	2620	2741	java/lang/Exception
    //   2620	2630	2741	java/lang/Exception
    //   2630	2640	2741	java/lang/Exception
    //   2640	2650	2741	java/lang/Exception
    //   2650	2660	2741	java/lang/Exception
    //   2660	2670	2741	java/lang/Exception
    //   2670	2675	2741	java/lang/Exception
    //   2675	2684	2741	java/lang/Exception
    //   2684	2691	2709	finally
    //   2691	2698	2709	finally
    //   2698	2703	2709	finally
    //   2703	2706	2709	finally
    //   2710	2713	2709	finally
    //   2716	2720	2741	java/lang/Exception
    //   2720	2723	2741	java/lang/Exception
    //   2732	2739	2741	java/lang/Exception
    //   2739	2741	2741	java/lang/Exception
  }
  
  private void onDrawFinished() {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(System.identityHashCode(this));
      stringBuilder.append(" finishedDrawing");
      Log.i("SurfaceView", stringBuilder.toString());
    } 
    runOnUiThread(new _$$Lambda$SurfaceView$TWz4D2u33ZlAmRtgKzbqqDue3iM(this));
  }
  
  protected void applyChildSurfaceTransaction_renderWorker(SurfaceControl.Transaction paramTransaction, Surface paramSurface, long paramLong) {}
  
  protected void onSetSurfacePositionAndScaleRT(SurfaceControl.Transaction paramTransaction, SurfaceControl paramSurfaceControl, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2) {
    paramTransaction.setPosition(paramSurfaceControl, paramInt1, paramInt2);
    paramTransaction.setMatrix(paramSurfaceControl, paramFloat1, 0.0F, 0.0F, paramFloat2);
  }
  
  public void requestUpdateSurfacePositionAndScale() {
    SurfaceControl surfaceControl = this.mSurfaceControl;
    if (surfaceControl == null)
      return; 
    SurfaceControl.Transaction transaction = this.mTmpTransaction;
    int i = this.mScreenRect.left, j = this.mScreenRect.top;
    Rect rect = this.mScreenRect;
    float f1 = rect.width() / this.mSurfaceWidth;
    rect = this.mScreenRect;
    float f2 = rect.height() / this.mSurfaceHeight;
    onSetSurfacePositionAndScaleRT(transaction, surfaceControl, i, j, f1, f2);
    this.mTmpTransaction.apply();
  }
  
  private void applySurfaceTransforms(SurfaceControl paramSurfaceControl, SurfaceControl.Transaction paramTransaction, Rect paramRect, long paramLong) {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (paramLong > 0L && viewRootImpl != null && !viewRootImpl.isDrawingToBLASTTransaction())
      paramTransaction.deferTransactionUntil(paramSurfaceControl, viewRootImpl.getRenderSurfaceControl(), paramLong); 
    int i = paramRect.left, j = paramRect.top;
    float f1 = paramRect.width() / this.mSurfaceWidth;
    float f2 = paramRect.height() / this.mSurfaceHeight;
    onSetSurfacePositionAndScaleRT(paramTransaction, paramSurfaceControl, i, j, f1, f2);
    if (this.mViewVisibility)
      paramTransaction.show(paramSurfaceControl); 
  }
  
  public Rect getSurfaceRenderPosition() {
    return this.mRTLastReportedPosition;
  }
  
  private void setParentSpaceRectangle(Rect paramRect, long paramLong) {
    SurfaceControl.Transaction transaction;
    ViewRootImpl viewRootImpl = getViewRootImpl();
    boolean bool = viewRootImpl.isDrawingToBLASTTransaction();
    if (bool) {
      transaction = viewRootImpl.getBLASTSyncTransaction();
    } else {
      transaction = this.mRtTransaction;
    } 
    applySurfaceTransforms(this.mSurfaceControl, transaction, paramRect, paramLong);
    applyChildSurfaceTransaction_renderWorker(transaction, viewRootImpl.mSurface, paramLong);
    if (!bool)
      transaction.apply(); 
  }
  
  private SurfaceHolder.Callback[] getSurfaceCallbacks() {
    synchronized (this.mCallbacks) {
      SurfaceHolder.Callback[] arrayOfCallback = new SurfaceHolder.Callback[this.mCallbacks.size()];
      this.mCallbacks.toArray(arrayOfCallback);
      return arrayOfCallback;
    } 
  }
  
  private void runOnUiThread(Runnable paramRunnable) {
    Handler handler = getHandler();
    if (handler != null && handler.getLooper() != Looper.myLooper()) {
      handler.post(paramRunnable);
    } else {
      paramRunnable.run();
    } 
  }
  
  public boolean isFixedSize() {
    return (this.mRequestedWidth != -1 || this.mRequestedHeight != -1);
  }
  
  private boolean isAboveParent() {
    boolean bool;
    if (this.mSubLayer >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setResizeBackgroundColor(int paramInt) {
    if (this.mBackgroundControl == null)
      return; 
    this.mBackgroundColor = paramInt;
    updateBackgroundColor(this.mTmpTransaction).apply();
  }
  
  public SurfaceControl getSurfaceControl() {
    return this.mSurfaceControl;
  }
  
  public IBinder getHostToken() {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl == null)
      return null; 
    return viewRootImpl.getInputToken();
  }
  
  public void surfaceCreated(SurfaceControl.Transaction paramTransaction) {
    setWindowStopped(false);
  }
  
  public void surfaceDestroyed() {
    setWindowStopped(true);
    setRemoteAccessibilityEmbeddedConnection((IAccessibilityEmbeddedConnection)null, (IBinder)null);
  }
  
  public void surfaceReplaced(SurfaceControl.Transaction paramTransaction) {
    if (this.mSurfaceControl != null && this.mBackgroundControl != null)
      updateRelativeZ(paramTransaction); 
  }
  
  private void updateRelativeZ(SurfaceControl.Transaction paramTransaction) {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl == null)
      return; 
    SurfaceControl surfaceControl = viewRootImpl.getSurfaceControl();
    paramTransaction.setRelativeLayer(this.mBackgroundControl, surfaceControl, -2147483648);
    paramTransaction.setRelativeLayer(this.mSurfaceControl, surfaceControl, this.mSubLayer);
  }
  
  public void setChildSurfacePackage(SurfaceControlViewHost.SurfacePackage paramSurfacePackage) {
    if (paramSurfacePackage != null)
      paramSurfacePackage.getSurfaceControl(); 
    SurfaceControlViewHost.SurfacePackage surfacePackage = this.mSurfacePackage;
    if (surfacePackage != null) {
      SurfaceControl surfaceControl = surfacePackage.getSurfaceControl();
    } else {
      surfacePackage = null;
    } 
    if (this.mSurfaceControl != null && surfacePackage != null) {
      this.mTmpTransaction.reparent((SurfaceControl)surfacePackage, null).apply();
      this.mSurfacePackage.release();
    } else if (this.mSurfaceControl != null) {
      reparentSurfacePackage(this.mTmpTransaction, paramSurfacePackage);
      this.mTmpTransaction.apply();
    } 
    this.mSurfacePackage = paramSurfacePackage;
  }
  
  private void reparentSurfacePackage(SurfaceControl.Transaction paramTransaction, SurfaceControlViewHost.SurfacePackage paramSurfacePackage) {
    initEmbeddedHierarchyForAccessibility(paramSurfacePackage);
    SurfaceControl surfaceControl = paramSurfacePackage.getSurfaceControl();
    paramTransaction.reparent(surfaceControl, this.mSurfaceControl).show(surfaceControl);
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    RemoteAccessibilityEmbeddedConnection remoteAccessibilityEmbeddedConnection = getRemoteAccessibilityEmbeddedConnection();
    if (remoteAccessibilityEmbeddedConnection == null)
      return; 
    paramAccessibilityNodeInfo.addChild(remoteAccessibilityEmbeddedConnection.getLeashToken());
  }
  
  public int getImportantForAccessibility() {
    int i = super.getImportantForAccessibility();
    if (this.mRemoteAccessibilityEmbeddedConnection == null || i != 0)
      return i; 
    return 1;
  }
  
  private void initEmbeddedHierarchyForAccessibility(SurfaceControlViewHost.SurfacePackage paramSurfacePackage) {
    IAccessibilityEmbeddedConnection iAccessibilityEmbeddedConnection = paramSurfacePackage.getAccessibilityEmbeddedConnection();
    RemoteAccessibilityEmbeddedConnection remoteAccessibilityEmbeddedConnection = getRemoteAccessibilityEmbeddedConnection();
    if (remoteAccessibilityEmbeddedConnection != null && remoteAccessibilityEmbeddedConnection.getConnection().equals(iAccessibilityEmbeddedConnection))
      return; 
    setRemoteAccessibilityEmbeddedConnection((IAccessibilityEmbeddedConnection)null, (IBinder)null);
    try {
      IBinder iBinder = (getViewRootImpl()).mLeashToken;
      int i = getAccessibilityViewId();
      iBinder = iAccessibilityEmbeddedConnection.associateEmbeddedHierarchy(iBinder, i);
      setRemoteAccessibilityEmbeddedConnection(iAccessibilityEmbeddedConnection, iBinder);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error while associateEmbeddedHierarchy ");
      stringBuilder.append(remoteException);
      Log.d("SurfaceView", stringBuilder.toString());
    } 
    updateScreenMatrixForEmbeddedHierarchy();
  }
  
  private void setRemoteAccessibilityEmbeddedConnection(IAccessibilityEmbeddedConnection paramIAccessibilityEmbeddedConnection, IBinder paramIBinder) {
    try {
      if (this.mRemoteAccessibilityEmbeddedConnection != null) {
        IAccessibilityEmbeddedConnection iAccessibilityEmbeddedConnection = this.mRemoteAccessibilityEmbeddedConnection.getConnection();
        iAccessibilityEmbeddedConnection.disassociateEmbeddedHierarchy();
        this.mRemoteAccessibilityEmbeddedConnection.unlinkToDeath();
        this.mRemoteAccessibilityEmbeddedConnection = null;
      } 
      if (paramIAccessibilityEmbeddedConnection != null && paramIBinder != null) {
        RemoteAccessibilityEmbeddedConnection remoteAccessibilityEmbeddedConnection = new RemoteAccessibilityEmbeddedConnection();
        this(this, paramIAccessibilityEmbeddedConnection, paramIBinder);
        this.mRemoteAccessibilityEmbeddedConnection = remoteAccessibilityEmbeddedConnection;
        remoteAccessibilityEmbeddedConnection.linkToDeath();
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error while setRemoteEmbeddedConnection ");
      stringBuilder.append(remoteException);
      Log.d("SurfaceView", stringBuilder.toString());
    } 
  }
  
  private RemoteAccessibilityEmbeddedConnection getRemoteAccessibilityEmbeddedConnection() {
    return this.mRemoteAccessibilityEmbeddedConnection;
  }
  
  private void updateScreenMatrixForEmbeddedHierarchy() {
    getBoundsOnScreen(this.mTmpRect);
    this.mTmpMatrix.reset();
    this.mTmpMatrix.setTranslate(this.mTmpRect.left, this.mTmpRect.top);
    Matrix matrix = this.mTmpMatrix;
    float f1 = this.mScreenRect.width() / this.mSurfaceWidth;
    Rect rect = this.mScreenRect;
    float f2 = rect.height() / this.mSurfaceHeight;
    matrix.postScale(f1, f2);
    if (this.mTmpMatrix.isIdentity() || this.mTmpMatrix.equals(this.mScreenMatrixForEmbeddedHierarchy))
      return; 
    try {
      RemoteAccessibilityEmbeddedConnection remoteAccessibilityEmbeddedConnection = getRemoteAccessibilityEmbeddedConnection();
      if (remoteAccessibilityEmbeddedConnection == null)
        return; 
      this.mTmpMatrix.getValues(this.mMatrixValues);
      remoteAccessibilityEmbeddedConnection.getConnection().setScreenMatrix(this.mMatrixValues);
      this.mScreenMatrixForEmbeddedHierarchy.set(this.mTmpMatrix);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error while setScreenMatrix ");
      stringBuilder.append(remoteException);
      Log.d("SurfaceView", stringBuilder.toString());
    } 
  }
  
  private void notifySurfaceDestroyed() {
    if (this.mSurface.isValid()) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(System.identityHashCode(this));
        stringBuilder.append(" surfaceDestroyed");
        Log.i("SurfaceView", stringBuilder.toString());
      } 
      for (SurfaceHolder.Callback callback : getSurfaceCallbacks())
        callback.surfaceDestroyed(this.mSurfaceHolder); 
      if (this.mSurface.isValid())
        this.mSurface.forceScopedDisconnect(); 
    } 
  }
  
  class RemoteAccessibilityEmbeddedConnection implements IBinder.DeathRecipient {
    private final IAccessibilityEmbeddedConnection mConnection;
    
    private final IBinder mLeashToken;
    
    final SurfaceView this$0;
    
    RemoteAccessibilityEmbeddedConnection(IAccessibilityEmbeddedConnection param1IAccessibilityEmbeddedConnection, IBinder param1IBinder) {
      this.mConnection = param1IAccessibilityEmbeddedConnection;
      this.mLeashToken = param1IBinder;
    }
    
    IAccessibilityEmbeddedConnection getConnection() {
      return this.mConnection;
    }
    
    IBinder getLeashToken() {
      return this.mLeashToken;
    }
    
    void linkToDeath() throws RemoteException {
      this.mConnection.asBinder().linkToDeath(this, 0);
    }
    
    void unlinkToDeath() {
      this.mConnection.asBinder().unlinkToDeath(this, 0);
    }
    
    public void binderDied() {
      unlinkToDeath();
      SurfaceView.this.runOnUiThread(new _$$Lambda$SurfaceView$RemoteAccessibilityEmbeddedConnection$MJCC8_qn1j4IJab7lJYkrpYVv74(this));
    }
  }
}
