package android.view;

import android.app.AppGlobals;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;

public class ViewConfiguration {
  private static final int A11Y_SHORTCUT_KEY_TIMEOUT = 3000;
  
  private static final int A11Y_SHORTCUT_KEY_TIMEOUT_AFTER_CONFIRMATION = 1000;
  
  private static final long ACTION_MODE_HIDE_DURATION_DEFAULT = 2000L;
  
  private static final float AMBIGUOUS_GESTURE_MULTIPLIER = 2.0F;
  
  public static final int DEFAULT_LONG_PRESS_TIMEOUT = 400;
  
  private static final int DEFAULT_MULTI_PRESS_TIMEOUT = 300;
  
  private static final int DOUBLE_TAP_MIN_TIME = 40;
  
  private static final int DOUBLE_TAP_SLOP = 100;
  
  private static final int DOUBLE_TAP_TIMEOUT = 300;
  
  private static final int DOUBLE_TAP_TOUCH_SLOP = 8;
  
  private static final int EDGE_SLOP = 12;
  
  private static final int FADING_EDGE_LENGTH = 12;
  
  private static final int GLOBAL_ACTIONS_KEY_TIMEOUT = 500;
  
  private static final int HAS_PERMANENT_MENU_KEY_AUTODETECT = 0;
  
  private static final int HAS_PERMANENT_MENU_KEY_FALSE = 2;
  
  private static final int HAS_PERMANENT_MENU_KEY_TRUE = 1;
  
  private static final float HORIZONTAL_SCROLL_FACTOR = 64.0F;
  
  private static final int HOVER_TAP_SLOP = 20;
  
  private static final int HOVER_TAP_TIMEOUT = 150;
  
  private static final int HOVER_TOOLTIP_HIDE_SHORT_TIMEOUT = 3000;
  
  private static final int HOVER_TOOLTIP_HIDE_TIMEOUT = 15000;
  
  private static final int HOVER_TOOLTIP_SHOW_TIMEOUT = 500;
  
  private static final int JUMP_TAP_TIMEOUT = 500;
  
  private static final int KEY_REPEAT_DELAY = 50;
  
  private static final int LONG_PRESS_TOOLTIP_HIDE_TIMEOUT = 1500;
  
  @Deprecated
  private static final int MAXIMUM_DRAWING_CACHE_SIZE = 1536000;
  
  private static final int MAXIMUM_FLING_VELOCITY = 8000;
  
  private static final int MINIMUM_FLING_VELOCITY = 50;
  
  private static final int MIN_SCROLLBAR_TOUCH_TARGET = 48;
  
  private static final int OVERFLING_DISTANCE = 6;
  
  private static final int OVERSCROLL_DISTANCE = 0;
  
  private static final int PAGING_TOUCH_SLOP = 16;
  
  private static final int PRESSED_STATE_DURATION = 64;
  
  private static final int SCREENSHOT_CHORD_KEY_TIMEOUT = 500;
  
  private static final int SCROLL_BAR_DEFAULT_DELAY = 300;
  
  private static final int SCROLL_BAR_FADE_DURATION = 250;
  
  private static final int SCROLL_BAR_SIZE = 4;
  
  private static final float SCROLL_FRICTION = 0.015F;
  
  private static final long SEND_RECURRING_ACCESSIBILITY_EVENTS_INTERVAL_MILLIS = 100L;
  
  private static final String TAG = "ViewConfiguration";
  
  private static final int TAP_TIMEOUT = 100;
  
  private static final int TOUCH_SLOP = 8;
  
  private static final float VERTICAL_SCROLL_FACTOR = 64.0F;
  
  private static final int WINDOW_TOUCH_SLOP = 16;
  
  private static final int ZOOM_CONTROLS_TIMEOUT = 3000;
  
  static final SparseArray<ViewConfiguration> sConfigurations = new SparseArray<>(2);
  
  private final float mAmbiguousGestureMultiplier;
  
  private final boolean mConstructedWithContext;
  
  private final int mDoubleTapSlop;
  
  private final int mDoubleTapTouchSlop;
  
  private final int mEdgeSlop;
  
  private final int mFadingEdgeLength;
  
  private final boolean mFadingMarqueeEnabled;
  
  private final long mGlobalActionsKeyTimeout;
  
  private final float mHorizontalScrollFactor;
  
  private final int mHoverSlop;
  
  private final int mMaximumDrawingCacheSize;
  
  private final int mMaximumFlingVelocity;
  
  private final int mMinScalingSpan;
  
  private final int mMinScrollbarTouchTarget;
  
  private final int mMinimumFlingVelocity;
  
  private final int mOverflingDistance;
  
  private final int mOverscrollDistance;
  
  private final int mPagingTouchSlop;
  
  private final long mScreenshotChordKeyTimeout;
  
  private final int mScrollbarSize;
  
  private final boolean mShowMenuShortcutsWhenKeyboardPresent;
  
  private final int mTouchSlop;
  
  private final float mVerticalScrollFactor;
  
  private final int mWindowTouchSlop;
  
  private boolean sHasPermanentMenuKey;
  
  private boolean sHasPermanentMenuKeySet;
  
  @Deprecated
  public ViewConfiguration() {
    this.mConstructedWithContext = false;
    this.mEdgeSlop = 12;
    this.mFadingEdgeLength = 12;
    this.mMinimumFlingVelocity = 50;
    this.mMaximumFlingVelocity = 8000;
    this.mScrollbarSize = 4;
    this.mTouchSlop = 8;
    this.mHoverSlop = 4;
    this.mMinScrollbarTouchTarget = 48;
    this.mDoubleTapTouchSlop = 8;
    this.mPagingTouchSlop = 16;
    this.mDoubleTapSlop = 100;
    this.mWindowTouchSlop = 16;
    this.mAmbiguousGestureMultiplier = 2.0F;
    this.mMaximumDrawingCacheSize = 1536000;
    this.mOverscrollDistance = 0;
    this.mOverflingDistance = 6;
    this.mFadingMarqueeEnabled = true;
    this.mGlobalActionsKeyTimeout = 500L;
    this.mHorizontalScrollFactor = 64.0F;
    this.mVerticalScrollFactor = 64.0F;
    this.mShowMenuShortcutsWhenKeyboardPresent = false;
    this.mScreenshotChordKeyTimeout = 500L;
    this.mMinScalingSpan = 0;
  }
  
  private ViewConfiguration(Context paramContext) {
    this.mConstructedWithContext = true;
    Resources resources = paramContext.getResources();
    DisplayMetrics displayMetrics = resources.getDisplayMetrics();
    Configuration configuration = resources.getConfiguration();
    float f = displayMetrics.density;
    if (configuration.isLayoutSizeAtLeast(4))
      f = 1.5F * f; 
    this.mEdgeSlop = (int)(f * 12.0F + 0.5F);
    this.mFadingEdgeLength = (int)(12.0F * f + 0.5F);
    this.mScrollbarSize = resources.getDimensionPixelSize(17105088);
    this.mDoubleTapSlop = (int)(100.0F * f + 0.5F);
    this.mWindowTouchSlop = (int)(16.0F * f + 0.5F);
    TypedValue typedValue = new TypedValue();
    resources.getValue(17105049, typedValue, true);
    this.mAmbiguousGestureMultiplier = Math.max(1.0F, typedValue.getFloat());
    WindowManager windowManager = (WindowManager)paramContext.getSystemService(WindowManager.class);
    Rect rect = windowManager.getMaximumWindowMetrics().getBounds();
    this.mMaximumDrawingCacheSize = rect.width() * 4 * rect.height();
    this.mOverscrollDistance = (int)(0.0F * f + 0.5F);
    this.mOverflingDistance = (int)(6.0F * f + 0.5F);
    if (!this.sHasPermanentMenuKeySet) {
      int j = resources.getInteger(17694876);
      if (j != 1) {
        if (j != 2) {
          IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
          try {
            boolean bool;
            if (!iWindowManager.hasNavigationBar(paramContext.getDisplayId())) {
              bool = true;
            } else {
              bool = false;
            } 
            this.sHasPermanentMenuKey = bool;
            this.sHasPermanentMenuKeySet = true;
          } catch (RemoteException remoteException) {
            this.sHasPermanentMenuKey = false;
          } 
        } else {
          this.sHasPermanentMenuKey = false;
          this.sHasPermanentMenuKeySet = true;
        } 
      } else {
        this.sHasPermanentMenuKey = true;
        this.sHasPermanentMenuKeySet = true;
      } 
    } 
    this.mFadingMarqueeEnabled = resources.getBoolean(17891565);
    this.mTouchSlop = resources.getDimensionPixelSize(17105093);
    this.mHoverSlop = resources.getDimensionPixelSize(17105092);
    this.mMinScrollbarTouchTarget = resources.getDimensionPixelSize(17105067);
    int i = this.mTouchSlop;
    this.mPagingTouchSlop = i * 2;
    this.mDoubleTapTouchSlop = i;
    this.mMinimumFlingVelocity = resources.getDimensionPixelSize(17105095);
    this.mMaximumFlingVelocity = resources.getDimensionPixelSize(17105094);
    this.mGlobalActionsKeyTimeout = resources.getInteger(17694815);
    this.mHorizontalScrollFactor = resources.getDimensionPixelSize(17105061);
    this.mVerticalScrollFactor = resources.getDimensionPixelSize(17105091);
    this.mShowMenuShortcutsWhenKeyboardPresent = resources.getBoolean(17891524);
    this.mMinScalingSpan = resources.getDimensionPixelSize(17105065);
    this.mScreenshotChordKeyTimeout = resources.getInteger(17694900);
  }
  
  public static ViewConfiguration get(Context paramContext) {
    if (!paramContext.isUiContext() && StrictMode.vmIncorrectContextUseEnabled()) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Tried to access UI constants from a non-visual Context:");
      stringBuilder1.append(paramContext);
      String str = stringBuilder1.toString();
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException(str);
      StrictMode.onIncorrectContextUsed("UI constants, such as display metrics or window metrics, must be accessed from Activity or other visual Context. Use an Activity or a Context created with Context#createWindowContext(int, Bundle), which are adjusted to the configuration and visual bounds of an area on screen", illegalArgumentException);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(str);
      stringBuilder2.append("UI constants, such as display metrics or window metrics, must be accessed from Activity or other visual Context. Use an Activity or a Context created with Context#createWindowContext(int, Bundle), which are adjusted to the configuration and visual bounds of an area on screen");
      Log.e("ViewConfiguration", stringBuilder2.toString(), illegalArgumentException);
    } 
    DisplayMetrics displayMetrics = paramContext.getResources().getDisplayMetrics();
    int i = (int)(displayMetrics.density * 100.0F);
    ViewConfiguration viewConfiguration2 = sConfigurations.get(i);
    ViewConfiguration viewConfiguration1 = viewConfiguration2;
    if (viewConfiguration2 == null) {
      viewConfiguration1 = new ViewConfiguration(paramContext);
      sConfigurations.put(i, viewConfiguration1);
    } 
    return viewConfiguration1;
  }
  
  @Deprecated
  public static int getScrollBarSize() {
    return 4;
  }
  
  public int getScaledScrollBarSize() {
    return this.mScrollbarSize;
  }
  
  public int getScaledMinScrollbarTouchTarget() {
    return this.mMinScrollbarTouchTarget;
  }
  
  public static int getScrollBarFadeDuration() {
    return 250;
  }
  
  public static int getScrollDefaultDelay() {
    return 300;
  }
  
  @Deprecated
  public static int getFadingEdgeLength() {
    return 12;
  }
  
  public int getScaledFadingEdgeLength() {
    return this.mFadingEdgeLength;
  }
  
  public static int getPressedStateDuration() {
    return 64;
  }
  
  public static int getLongPressTimeout() {
    return AppGlobals.getIntCoreSetting("long_press_timeout", 400);
  }
  
  public static int getMultiPressTimeout() {
    return AppGlobals.getIntCoreSetting("multi_press_timeout", 300);
  }
  
  public static int getKeyRepeatTimeout() {
    return getLongPressTimeout();
  }
  
  public static int getKeyRepeatDelay() {
    return 50;
  }
  
  public static int getTapTimeout() {
    return 100;
  }
  
  public static int getJumpTapTimeout() {
    return 500;
  }
  
  public static int getDoubleTapTimeout() {
    return 300;
  }
  
  public static int getDoubleTapMinTime() {
    return 40;
  }
  
  public static int getHoverTapTimeout() {
    return 150;
  }
  
  public static int getHoverTapSlop() {
    return 20;
  }
  
  @Deprecated
  public static int getEdgeSlop() {
    return 12;
  }
  
  public int getScaledEdgeSlop() {
    return this.mEdgeSlop;
  }
  
  @Deprecated
  public static int getTouchSlop() {
    return 8;
  }
  
  public int getScaledTouchSlop() {
    return this.mTouchSlop;
  }
  
  public int getScaledHoverSlop() {
    return this.mHoverSlop;
  }
  
  public int getScaledDoubleTapTouchSlop() {
    return this.mDoubleTapTouchSlop;
  }
  
  public int getScaledPagingTouchSlop() {
    return this.mPagingTouchSlop;
  }
  
  @Deprecated
  public static int getDoubleTapSlop() {
    return 100;
  }
  
  public int getScaledDoubleTapSlop() {
    return this.mDoubleTapSlop;
  }
  
  public static long getSendRecurringAccessibilityEventsInterval() {
    return 100L;
  }
  
  @Deprecated
  public static int getWindowTouchSlop() {
    return 16;
  }
  
  public int getScaledWindowTouchSlop() {
    return this.mWindowTouchSlop;
  }
  
  @Deprecated
  public static int getMinimumFlingVelocity() {
    return 50;
  }
  
  public int getScaledMinimumFlingVelocity() {
    return this.mMinimumFlingVelocity;
  }
  
  @Deprecated
  public static int getMaximumFlingVelocity() {
    return 8000;
  }
  
  public int getScaledMaximumFlingVelocity() {
    return this.mMaximumFlingVelocity;
  }
  
  public int getScaledScrollFactor() {
    return (int)this.mVerticalScrollFactor;
  }
  
  public float getScaledHorizontalScrollFactor() {
    return this.mHorizontalScrollFactor;
  }
  
  public float getScaledVerticalScrollFactor() {
    return this.mVerticalScrollFactor;
  }
  
  @Deprecated
  public static int getMaximumDrawingCacheSize() {
    return 1536000;
  }
  
  public int getScaledMaximumDrawingCacheSize() {
    return this.mMaximumDrawingCacheSize;
  }
  
  public int getScaledOverscrollDistance() {
    return this.mOverscrollDistance;
  }
  
  public int getScaledOverflingDistance() {
    return this.mOverflingDistance;
  }
  
  public static long getZoomControlsTimeout() {
    return 3000L;
  }
  
  @Deprecated
  public static long getGlobalActionKeyTimeout() {
    return 500L;
  }
  
  public long getDeviceGlobalActionKeyTimeout() {
    return this.mGlobalActionsKeyTimeout;
  }
  
  public long getScreenshotChordKeyTimeout() {
    return this.mScreenshotChordKeyTimeout;
  }
  
  public long getAccessibilityShortcutKeyTimeout() {
    return 3000L;
  }
  
  public long getAccessibilityShortcutKeyTimeoutAfterConfirmation() {
    return 1000L;
  }
  
  public static float getScrollFriction() {
    return 0.015F;
  }
  
  public static long getDefaultActionModeHideDuration() {
    return 2000L;
  }
  
  @Deprecated
  public static float getAmbiguousGestureMultiplier() {
    return 2.0F;
  }
  
  public float getScaledAmbiguousGestureMultiplier() {
    return this.mAmbiguousGestureMultiplier;
  }
  
  public boolean hasPermanentMenuKey() {
    return this.sHasPermanentMenuKey;
  }
  
  public boolean shouldShowMenuShortcutsWhenKeyboardPresent() {
    return this.mShowMenuShortcutsWhenKeyboardPresent;
  }
  
  public int getScaledMinimumScalingSpan() {
    if (this.mConstructedWithContext)
      return this.mMinScalingSpan; 
    throw new IllegalStateException("Min scaling span cannot be determined when this method is called on a ViewConfiguration that was instantiated using a constructor with no Context parameter");
  }
  
  public boolean isFadingMarqueeEnabled() {
    return this.mFadingMarqueeEnabled;
  }
  
  public static int getLongPressTooltipHideTimeout() {
    return 1500;
  }
  
  public static int getHoverTooltipShowTimeout() {
    return 500;
  }
  
  public static int getHoverTooltipHideTimeout() {
    return 15000;
  }
  
  public static int getHoverTooltipHideShortTimeout() {
    return 3000;
  }
}
