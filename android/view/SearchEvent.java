package android.view;

public class SearchEvent {
  private InputDevice mInputDevice;
  
  public SearchEvent(InputDevice paramInputDevice) {
    this.mInputDevice = paramInputDevice;
  }
  
  public InputDevice getInputDevice() {
    return this.mInputDevice;
  }
}
