package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.OplusBaseBaseCanvas;
import android.graphics.Rect;
import android.widget.IOplusTextViewRTLUtilForUG;
import com.oplus.favorite.IOplusFavoriteManager;
import com.oplus.screenshot.IOplusLongshotController;
import com.oplus.screenshot.OplusLongshotViewController;
import com.oplus.screenshot.OplusLongshotViewInfo;
import com.oplus.util.OplusContextUtil;
import com.oplus.util.OplusTypeCastingHelper;
import com.oplus.view.IOplusScrollBarEffect;
import com.oplus.view.OplusScrollBarEffect;

public class OplusViewHooksImp implements IOplusViewHooks {
  private OplusContextUtil mContextUtil = null;
  
  private final IOplusLongshotController mLongshotController;
  
  private final IOplusScrollBarEffect mScrollBarEffect;
  
  private final View mView;
  
  public OplusViewHooksImp(View paramView, Resources paramResources) {
    this.mView = paramView;
    OplusBaseView oplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, paramView);
    this.mLongshotController = (IOplusLongshotController)new OplusLongshotViewController(oplusBaseView);
    this.mScrollBarEffect = createScrollBarEffect(paramResources);
    ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).initRtlParameter(paramResources);
  }
  
  public boolean awakenScrollBars() {
    return this.mView.awakenScrollBars();
  }
  
  public boolean isLayoutRtl() {
    return this.mView.isLayoutRtl();
  }
  
  public IOplusLongshotController getLongshotController() {
    return this.mLongshotController;
  }
  
  public IOplusScrollBarEffect getScrollBarEffect() {
    return this.mScrollBarEffect;
  }
  
  public boolean isOplusStyle() {
    if (this.mContextUtil == null)
      this.mContextUtil = new OplusContextUtil(this.mView.getContext()); 
    return this.mContextUtil.isOplusStyle();
  }
  
  public boolean isOplusOSStyle() {
    if (this.mContextUtil == null)
      this.mContextUtil = new OplusContextUtil(this.mView.getContext()); 
    return this.mContextUtil.isOplusOSStyle();
  }
  
  public int getOverScrollMode(int paramInt) {
    return getLongshotController().getOverScrollMode(paramInt);
  }
  
  public boolean findViewsLongshotInfo(OplusLongshotViewInfo paramOplusLongshotViewInfo) {
    return getLongshotController().findInfo(paramOplusLongshotViewInfo);
  }
  
  public boolean isLongshotConnected() {
    return getLongshotController().isLongshotConnected();
  }
  
  public boolean overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean1, int paramInt9, boolean paramBoolean2) {
    return getLongshotController().overScrollBy(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBoolean1, paramInt9, paramBoolean2);
  }
  
  public void performClick() {
    IOplusFavoriteManager iOplusFavoriteManager = (IOplusFavoriteManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFavoriteManager.DEFAULT, new Object[0]);
    iOplusFavoriteManager.processClick(this.mView, null);
    iOplusFavoriteManager.logViewInfo(this.mView);
  }
  
  public Bitmap getOplusCustomDrawingCache(Rect paramRect, int paramInt1, int paramInt2) {
    int i = this.mView.getDrawingCacheBackgroundColor();
    try {
      Canvas canvas;
      int j = this.mView.getWidth();
      int k = this.mView.getHeight();
      if (j <= 0 || k <= 0)
        return null; 
      Bitmap.Config config = Bitmap.Config.ARGB_8888;
      Bitmap bitmap = Bitmap.createBitmap(this.mView.getResources().getDisplayMetrics(), j, k, config);
      bitmap.setDensity((this.mView.getResources().getDisplayMetrics()).densityDpi);
      if (bitmap == null)
        return null; 
      if (i != 0) {
        j = 1;
      } else {
        j = 0;
      } 
      View.AttachInfo attachInfo = this.mView.mAttachInfo;
      if (attachInfo != null) {
        Canvas canvas1 = attachInfo.mCanvas;
        canvas = canvas1;
        if (canvas1 == null)
          canvas = new Canvas(); 
        canvas.setBitmap(bitmap);
        attachInfo.mCanvas = null;
      } else {
        canvas = new Canvas(bitmap);
      } 
      if (paramRect != null && !paramRect.isEmpty()) {
        OplusBaseBaseCanvas oplusBaseBaseCanvas1 = (OplusBaseBaseCanvas)OplusTypeCastingHelper.typeCasting(OplusBaseBaseCanvas.class, canvas);
        oplusBaseBaseCanvas1.setClipChildRect(paramRect);
        if (paramRect.top > paramInt1)
          canvas.clipRect(paramRect.left, paramRect.top - paramInt1, paramRect.right, paramRect.bottom - paramInt1); 
      } 
      if (j != 0)
        bitmap.eraseColor(i); 
      paramInt1 = canvas.save();
      if ((paramInt2 & 0x80) == 128) {
        this.mView.dispatchDraw(canvas);
        if (this.mView.getOverlay() != null && !this.mView.getOverlay().isEmpty())
          this.mView.getOverlay().getOverlayView().draw(canvas); 
      } else {
        this.mView.draw(canvas);
      } 
      canvas.restoreToCount(paramInt1);
      OplusBaseBaseCanvas oplusBaseBaseCanvas = (OplusBaseBaseCanvas)OplusTypeCastingHelper.typeCasting(OplusBaseBaseCanvas.class, canvas);
      oplusBaseBaseCanvas.setClipChildRect(null);
      canvas.setBitmap(null);
      return bitmap;
    } catch (OutOfMemoryError outOfMemoryError) {
      return null;
    } 
  }
  
  private IOplusScrollBarEffect createScrollBarEffect(Resources paramResources) {
    if (paramResources == null)
      return OplusScrollBarEffect.NO_EFFECT; 
    return (IOplusScrollBarEffect)new OplusScrollBarEffect(paramResources, (IOplusScrollBarEffect.ViewCallback)this);
  }
  
  public IOplusViewConfigHelper getOplusViewConfigHelper(Context paramContext) {
    return new OplusViewConfigHelper(paramContext);
  }
  
  public boolean needHook() {
    return true;
  }
}
