package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGraphicsStatsCallback extends IInterface {
  void onRotateGraphicsStatsBuffer() throws RemoteException;
  
  class Default implements IGraphicsStatsCallback {
    public void onRotateGraphicsStatsBuffer() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGraphicsStatsCallback {
    private static final String DESCRIPTOR = "android.view.IGraphicsStatsCallback";
    
    static final int TRANSACTION_onRotateGraphicsStatsBuffer = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IGraphicsStatsCallback");
    }
    
    public static IGraphicsStatsCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IGraphicsStatsCallback");
      if (iInterface != null && iInterface instanceof IGraphicsStatsCallback)
        return (IGraphicsStatsCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onRotateGraphicsStatsBuffer";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IGraphicsStatsCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IGraphicsStatsCallback");
      onRotateGraphicsStatsBuffer();
      return true;
    }
    
    private static class Proxy implements IGraphicsStatsCallback {
      public static IGraphicsStatsCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IGraphicsStatsCallback";
      }
      
      public void onRotateGraphicsStatsBuffer() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IGraphicsStatsCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IGraphicsStatsCallback.Stub.getDefaultImpl() != null) {
            IGraphicsStatsCallback.Stub.getDefaultImpl().onRotateGraphicsStatsBuffer();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGraphicsStatsCallback param1IGraphicsStatsCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGraphicsStatsCallback != null) {
          Proxy.sDefaultImpl = param1IGraphicsStatsCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGraphicsStatsCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
