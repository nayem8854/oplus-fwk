package android.view;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class VerifiedMotionEvent extends VerifiedInputEvent implements Parcelable {
  public Boolean getFlag(int paramInt) {
    boolean bool = true;
    if (paramInt != 1 && paramInt != 2)
      return null; 
    if ((this.mFlags & paramInt) == 0)
      bool = false; 
    return Boolean.valueOf(bool);
  }
  
  public VerifiedMotionEvent(int paramInt1, long paramLong1, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, int paramInt4, long paramLong2, int paramInt5, int paramInt6, int paramInt7) {
    super(2, paramInt1, paramLong1, paramInt2, paramInt3);
    this.mRawX = paramFloat1;
    this.mRawY = paramFloat2;
    this.mActionMasked = paramInt4;
    AnnotationValidations.validate(MotionEventAction.class, null, paramInt4);
    this.mDownTimeNanos = paramLong2;
    AnnotationValidations.validate(SuppressLint.class, null, Long.valueOf(paramLong2), new Object[] { "value", "MethodNameUnits" });
    this.mFlags = paramInt5;
    this.mMetaState = paramInt6;
    this.mButtonState = paramInt7;
  }
  
  public float getRawX() {
    return this.mRawX;
  }
  
  public float getRawY() {
    return this.mRawY;
  }
  
  public int getActionMasked() {
    return this.mActionMasked;
  }
  
  public long getDownTimeNanos() {
    return this.mDownTimeNanos;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public int getMetaState() {
    return this.mMetaState;
  }
  
  public int getButtonState() {
    return this.mButtonState;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (getDeviceId() != paramObject.getDeviceId() || 
      getEventTimeNanos() != paramObject.getEventTimeNanos() || 
      getSource() != paramObject.getSource() || 
      getDisplayId() != paramObject.getDisplayId() || this.mRawX != ((VerifiedMotionEvent)paramObject).mRawX || this.mRawY != ((VerifiedMotionEvent)paramObject).mRawY || this.mActionMasked != ((VerifiedMotionEvent)paramObject).mActionMasked || this.mDownTimeNanos != ((VerifiedMotionEvent)paramObject).mDownTimeNanos || this.mFlags != ((VerifiedMotionEvent)paramObject).mFlags || this.mMetaState != ((VerifiedMotionEvent)paramObject).mMetaState || this.mButtonState != ((VerifiedMotionEvent)paramObject).mButtonState)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    int i = getDeviceId();
    int j = Long.hashCode(getEventTimeNanos());
    int k = getSource();
    int m = getDisplayId();
    int n = Float.hashCode(this.mRawX);
    int i1 = Float.hashCode(this.mRawY);
    int i2 = this.mActionMasked;
    int i3 = Long.hashCode(this.mDownTimeNanos);
    int i4 = this.mFlags;
    int i5 = this.mMetaState;
    int i6 = this.mButtonState;
    return ((((((((((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeFloat(this.mRawX);
    paramParcel.writeFloat(this.mRawY);
    paramParcel.writeInt(this.mActionMasked);
    paramParcel.writeLong(this.mDownTimeNanos);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeInt(this.mMetaState);
    paramParcel.writeInt(this.mButtonState);
  }
  
  public int describeContents() {
    return 0;
  }
  
  VerifiedMotionEvent(Parcel paramParcel) {
    super(paramParcel, 2);
    float f1 = paramParcel.readFloat();
    float f2 = paramParcel.readFloat();
    int i = paramParcel.readInt();
    long l = paramParcel.readLong();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    int m = paramParcel.readInt();
    this.mRawX = f1;
    this.mRawY = f2;
    this.mActionMasked = i;
    AnnotationValidations.validate(MotionEventAction.class, null, i);
    this.mDownTimeNanos = l;
    AnnotationValidations.validate(SuppressLint.class, null, Long.valueOf(l), new Object[] { "value", "MethodNameUnits" });
    this.mFlags = j;
    this.mMetaState = k;
    this.mButtonState = m;
  }
  
  public static final Parcelable.Creator<VerifiedMotionEvent> CREATOR = (Parcelable.Creator<VerifiedMotionEvent>)new Object();
  
  private static final String TAG = "VerifiedMotionEvent";
  
  private int mActionMasked;
  
  private int mButtonState;
  
  private long mDownTimeNanos;
  
  private int mFlags;
  
  private int mMetaState;
  
  private float mRawX;
  
  private float mRawY;
  
  @Deprecated
  private void __metadata() {}
  
  @Retention(RetentionPolicy.SOURCE)
  class MotionEventAction implements Annotation {}
}
