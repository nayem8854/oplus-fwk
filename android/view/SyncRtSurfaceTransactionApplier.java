package android.view;

import android.graphics.Matrix;
import android.graphics.Rect;
import java.util.function.Consumer;

public class SyncRtSurfaceTransactionApplier {
  public static final int FLAG_ALL = -1;
  
  public static final int FLAG_ALPHA = 1;
  
  public static final int FLAG_BACKGROUND_BLUR_RADIUS = 32;
  
  public static final int FLAG_CORNER_RADIUS = 16;
  
  public static final int FLAG_LAYER = 8;
  
  public static final int FLAG_MATRIX = 2;
  
  public static final int FLAG_VISIBILITY = 64;
  
  public static final int FLAG_WINDOW_CROP = 4;
  
  private SurfaceControl mTargetSc;
  
  private final ViewRootImpl mTargetViewRootImpl;
  
  private final float[] mTmpFloat9 = new float[9];
  
  public SyncRtSurfaceTransactionApplier(View paramView) {
    if (paramView != null) {
      ViewRootImpl viewRootImpl = paramView.getViewRootImpl();
    } else {
      paramView = null;
    } 
    this.mTargetViewRootImpl = (ViewRootImpl)paramView;
  }
  
  public void scheduleApply(SurfaceParams... paramVarArgs) {
    ViewRootImpl viewRootImpl = this.mTargetViewRootImpl;
    if (viewRootImpl == null)
      return; 
    this.mTargetSc = viewRootImpl.getRenderSurfaceControl();
    this.mTargetViewRootImpl.registerRtFrameCallback(new _$$Lambda$SyncRtSurfaceTransactionApplier$ttntIVYYZl7t890CcQHVoB3U1nQ(this, paramVarArgs));
    this.mTargetViewRootImpl.getView().invalidate();
  }
  
  void applyParams(SurfaceControl.Transaction paramTransaction, long paramLong, SurfaceParams... paramVarArgs) {
    for (int i = paramVarArgs.length - 1; i >= 0; i--) {
      SurfaceParams surfaceParams = paramVarArgs[i];
      SurfaceControl surfaceControl = surfaceParams.surface;
      if (paramLong > 0L)
        paramTransaction.deferTransactionUntil(surfaceControl, this.mTargetSc, paramLong); 
      applyParams(paramTransaction, surfaceParams, this.mTmpFloat9);
    } 
    paramTransaction.apply();
  }
  
  public static void applyParams(SurfaceControl.Transaction paramTransaction, SurfaceParams paramSurfaceParams, float[] paramArrayOffloat) {
    if ((paramSurfaceParams.flags & 0x2) != 0)
      paramTransaction.setMatrix(paramSurfaceParams.surface, paramSurfaceParams.matrix, paramArrayOffloat); 
    if ((paramSurfaceParams.flags & 0x4) != 0)
      paramTransaction.setWindowCrop(paramSurfaceParams.surface, paramSurfaceParams.windowCrop); 
    if ((paramSurfaceParams.flags & 0x1) != 0)
      paramTransaction.setAlpha(paramSurfaceParams.surface, paramSurfaceParams.alpha); 
    if ((paramSurfaceParams.flags & 0x8) != 0)
      paramTransaction.setLayer(paramSurfaceParams.surface, paramSurfaceParams.layer); 
    if ((paramSurfaceParams.flags & 0x10) != 0)
      paramTransaction.setCornerRadius(paramSurfaceParams.surface, paramSurfaceParams.cornerRadius); 
    if ((paramSurfaceParams.flags & 0x20) != 0)
      paramTransaction.setBackgroundBlurRadius(paramSurfaceParams.surface, paramSurfaceParams.backgroundBlurRadius); 
    if ((paramSurfaceParams.flags & 0x40) != 0)
      if (paramSurfaceParams.visible) {
        paramTransaction.show(paramSurfaceParams.surface);
      } else {
        paramTransaction.hide(paramSurfaceParams.surface);
      }  
  }
  
  public static void create(View paramView, Consumer<SyncRtSurfaceTransactionApplier> paramConsumer) {
    if (paramView == null) {
      paramConsumer.accept(null);
    } else if (paramView.getViewRootImpl() != null) {
      paramConsumer.accept(new SyncRtSurfaceTransactionApplier(paramView));
    } else {
      paramView.addOnAttachStateChangeListener((View.OnAttachStateChangeListener)new Object(paramView, paramConsumer));
    } 
  }
  
  public static class SurfaceParams {
    public final float alpha;
    
    public final int backgroundBlurRadius;
    
    public final float cornerRadius;
    
    private final int flags;
    
    public final int layer;
    
    public final Matrix matrix;
    
    public final SurfaceControl surface;
    
    public final boolean visible;
    
    public final Rect windowCrop;
    
    public static class Builder {
      float alpha;
      
      int backgroundBlurRadius;
      
      float cornerRadius;
      
      int flags;
      
      int layer;
      
      Matrix matrix;
      
      final SurfaceControl surface;
      
      boolean visible;
      
      Rect windowCrop;
      
      public Builder(SurfaceControl param2SurfaceControl) {
        this.surface = param2SurfaceControl;
      }
      
      public Builder withAlpha(float param2Float) {
        this.alpha = param2Float;
        this.flags |= 0x1;
        return this;
      }
      
      public Builder withMatrix(Matrix param2Matrix) {
        this.matrix = param2Matrix;
        this.flags |= 0x2;
        return this;
      }
      
      public Builder withWindowCrop(Rect param2Rect) {
        this.windowCrop = param2Rect;
        this.flags |= 0x4;
        return this;
      }
      
      public Builder withLayer(int param2Int) {
        this.layer = param2Int;
        this.flags |= 0x8;
        return this;
      }
      
      public Builder withCornerRadius(float param2Float) {
        this.cornerRadius = param2Float;
        this.flags |= 0x10;
        return this;
      }
      
      public Builder withBackgroundBlur(int param2Int) {
        this.backgroundBlurRadius = param2Int;
        this.flags |= 0x20;
        return this;
      }
      
      public Builder withVisibility(boolean param2Boolean) {
        this.visible = param2Boolean;
        this.flags |= 0x40;
        return this;
      }
      
      public SyncRtSurfaceTransactionApplier.SurfaceParams build() {
        return new SyncRtSurfaceTransactionApplier.SurfaceParams(this.surface, this.flags, this.alpha, this.matrix, this.windowCrop, this.layer, this.cornerRadius, this.backgroundBlurRadius, this.visible);
      }
    }
    
    private SurfaceParams(SurfaceControl param1SurfaceControl, int param1Int1, float param1Float1, Matrix param1Matrix, Rect param1Rect, int param1Int2, float param1Float2, int param1Int3, boolean param1Boolean) {
      this.flags = param1Int1;
      this.surface = param1SurfaceControl;
      this.alpha = param1Float1;
      this.matrix = new Matrix(param1Matrix);
      this.windowCrop = new Rect(param1Rect);
      this.layer = param1Int2;
      this.cornerRadius = param1Float2;
      this.backgroundBlurRadius = param1Int3;
      this.visible = param1Boolean;
    }
  }
  
  public static class Builder {
    float alpha;
    
    int backgroundBlurRadius;
    
    float cornerRadius;
    
    int flags;
    
    int layer;
    
    Matrix matrix;
    
    final SurfaceControl surface;
    
    boolean visible;
    
    Rect windowCrop;
    
    public Builder(SurfaceControl param1SurfaceControl) {
      this.surface = param1SurfaceControl;
    }
    
    public Builder withAlpha(float param1Float) {
      this.alpha = param1Float;
      this.flags |= 0x1;
      return this;
    }
    
    public Builder withMatrix(Matrix param1Matrix) {
      this.matrix = param1Matrix;
      this.flags |= 0x2;
      return this;
    }
    
    public Builder withWindowCrop(Rect param1Rect) {
      this.windowCrop = param1Rect;
      this.flags |= 0x4;
      return this;
    }
    
    public Builder withLayer(int param1Int) {
      this.layer = param1Int;
      this.flags |= 0x8;
      return this;
    }
    
    public Builder withCornerRadius(float param1Float) {
      this.cornerRadius = param1Float;
      this.flags |= 0x10;
      return this;
    }
    
    public Builder withBackgroundBlur(int param1Int) {
      this.backgroundBlurRadius = param1Int;
      this.flags |= 0x20;
      return this;
    }
    
    public Builder withVisibility(boolean param1Boolean) {
      this.visible = param1Boolean;
      this.flags |= 0x40;
      return this;
    }
    
    public SyncRtSurfaceTransactionApplier.SurfaceParams build() {
      return new SyncRtSurfaceTransactionApplier.SurfaceParams(this.surface, this.flags, this.alpha, this.matrix, this.windowCrop, this.layer, this.cornerRadius, this.backgroundBlurRadius, this.visible);
    }
  }
}
