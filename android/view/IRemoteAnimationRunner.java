package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface IRemoteAnimationRunner extends IInterface {
  void onAnimationCancelled() throws RemoteException;
  
  void onAnimationStart(RemoteAnimationTarget[] paramArrayOfRemoteAnimationTarget1, RemoteAnimationTarget[] paramArrayOfRemoteAnimationTarget2, IRemoteAnimationFinishedCallback paramIRemoteAnimationFinishedCallback) throws RemoteException;
  
  class Default implements IRemoteAnimationRunner {
    public void onAnimationStart(RemoteAnimationTarget[] param1ArrayOfRemoteAnimationTarget1, RemoteAnimationTarget[] param1ArrayOfRemoteAnimationTarget2, IRemoteAnimationFinishedCallback param1IRemoteAnimationFinishedCallback) throws RemoteException {}
    
    public void onAnimationCancelled() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteAnimationRunner {
    private static final String DESCRIPTOR = "android.view.IRemoteAnimationRunner";
    
    static final int TRANSACTION_onAnimationCancelled = 2;
    
    static final int TRANSACTION_onAnimationStart = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IRemoteAnimationRunner");
    }
    
    public static IRemoteAnimationRunner asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IRemoteAnimationRunner");
      if (iInterface != null && iInterface instanceof IRemoteAnimationRunner)
        return (IRemoteAnimationRunner)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onAnimationCancelled";
      } 
      return "onAnimationStart";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.view.IRemoteAnimationRunner");
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IRemoteAnimationRunner");
        onAnimationCancelled();
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IRemoteAnimationRunner");
      RemoteAnimationTarget[] arrayOfRemoteAnimationTarget2 = (RemoteAnimationTarget[])param1Parcel1.createTypedArray(RemoteAnimationTarget.CREATOR);
      RemoteAnimationTarget[] arrayOfRemoteAnimationTarget1 = (RemoteAnimationTarget[])param1Parcel1.createTypedArray(RemoteAnimationTarget.CREATOR);
      IRemoteAnimationFinishedCallback iRemoteAnimationFinishedCallback = IRemoteAnimationFinishedCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      onAnimationStart(arrayOfRemoteAnimationTarget2, arrayOfRemoteAnimationTarget1, iRemoteAnimationFinishedCallback);
      return true;
    }
    
    private static class Proxy implements IRemoteAnimationRunner {
      public static IRemoteAnimationRunner sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IRemoteAnimationRunner";
      }
      
      public void onAnimationStart(RemoteAnimationTarget[] param2ArrayOfRemoteAnimationTarget1, RemoteAnimationTarget[] param2ArrayOfRemoteAnimationTarget2, IRemoteAnimationFinishedCallback param2IRemoteAnimationFinishedCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IRemoteAnimationRunner");
          parcel.writeTypedArray((Parcelable[])param2ArrayOfRemoteAnimationTarget1, 0);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfRemoteAnimationTarget2, 0);
          if (param2IRemoteAnimationFinishedCallback != null) {
            iBinder = param2IRemoteAnimationFinishedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IRemoteAnimationRunner.Stub.getDefaultImpl() != null) {
            IRemoteAnimationRunner.Stub.getDefaultImpl().onAnimationStart(param2ArrayOfRemoteAnimationTarget1, param2ArrayOfRemoteAnimationTarget2, param2IRemoteAnimationFinishedCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAnimationCancelled() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IRemoteAnimationRunner");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IRemoteAnimationRunner.Stub.getDefaultImpl() != null) {
            IRemoteAnimationRunner.Stub.getDefaultImpl().onAnimationCancelled();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteAnimationRunner param1IRemoteAnimationRunner) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteAnimationRunner != null) {
          Proxy.sDefaultImpl = param1IRemoteAnimationRunner;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteAnimationRunner getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
