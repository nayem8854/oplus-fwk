package android.view;

import android.content.Context;

public class ViewGroupOverlay extends ViewOverlay {
  ViewGroupOverlay(Context paramContext, View paramView) {
    super(paramContext, paramView);
  }
  
  public void add(View paramView) {
    this.mOverlayViewGroup.add(paramView);
  }
  
  public void remove(View paramView) {
    this.mOverlayViewGroup.remove(paramView);
  }
}
