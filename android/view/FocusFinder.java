package android.view;

import android.graphics.Rect;
import android.util.ArrayMap;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class FocusFinder {
  private static final ThreadLocal<FocusFinder> tlFocusFinder = new ThreadLocal<FocusFinder>() {
      protected FocusFinder initialValue() {
        return new FocusFinder();
      }
    };
  
  public static FocusFinder getInstance() {
    return tlFocusFinder.get();
  }
  
  final Rect mFocusedRect = new Rect();
  
  final Rect mOtherRect = new Rect();
  
  final Rect mBestCandidateRect = new Rect();
  
  private final UserSpecifiedFocusComparator mUserSpecifiedFocusComparator = new UserSpecifiedFocusComparator((UserSpecifiedFocusComparator.NextFocusGetter)_$$Lambda$FocusFinder$Pgx6IETuqCkrhJYdiBes48tolG4.INSTANCE);
  
  private final UserSpecifiedFocusComparator mUserSpecifiedClusterComparator = new UserSpecifiedFocusComparator((UserSpecifiedFocusComparator.NextFocusGetter)_$$Lambda$FocusFinder$P8rLvOJhymJH5ALAgUjGaM5gxKA.INSTANCE);
  
  private final FocusSorter mFocusSorter = new FocusSorter();
  
  private final ArrayList<View> mTempList = new ArrayList<>();
  
  public final View findNextFocus(ViewGroup paramViewGroup, View paramView, int paramInt) {
    return findNextFocus(paramViewGroup, paramView, null, paramInt);
  }
  
  public View findNextFocusFromRect(ViewGroup paramViewGroup, Rect paramRect, int paramInt) {
    this.mFocusedRect.set(paramRect);
    return findNextFocus(paramViewGroup, null, this.mFocusedRect, paramInt);
  }
  
  private View findNextFocus(ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt) {
    ViewGroup viewGroup1 = null;
    ViewGroup viewGroup2 = getEffectiveRoot(paramViewGroup, paramView);
    paramViewGroup = viewGroup1;
    if (paramView != null)
      null = findNextUserSpecifiedFocus(viewGroup2, paramView, paramInt); 
    if (null != null)
      return null; 
    ArrayList<View> arrayList = this.mTempList;
    try {
      arrayList.clear();
      viewGroup2.addFocusables(arrayList, paramInt);
      if (!arrayList.isEmpty())
        null = findNextFocus(viewGroup2, paramView, paramRect, paramInt, arrayList); 
      return null;
    } finally {
      arrayList.clear();
    } 
  }
  
  private ViewGroup getEffectiveRoot(ViewGroup paramViewGroup, View paramView) {
    if (paramView == null || paramView == paramViewGroup)
      return paramViewGroup; 
    ViewGroup viewGroup = null;
    ViewParent viewParent = paramView.getParent();
    while (true) {
      if (viewParent == paramViewGroup) {
        if (viewGroup == null)
          viewGroup = paramViewGroup; 
        return viewGroup;
      } 
      ViewGroup viewGroup1 = (ViewGroup)viewParent;
      ViewGroup viewGroup2 = viewGroup;
      if (viewGroup1.getTouchscreenBlocksFocus()) {
        viewGroup2 = viewGroup;
        if (paramView.getContext().getPackageManager().hasSystemFeature("android.hardware.touchscreen")) {
          viewGroup2 = viewGroup;
          if (viewGroup1.isKeyboardNavigationCluster())
            viewGroup2 = viewGroup1; 
        } 
      } 
      ViewParent viewParent1 = viewParent.getParent();
      viewGroup = viewGroup2;
      viewParent = viewParent1;
      if (!(viewParent1 instanceof ViewGroup))
        return paramViewGroup; 
    } 
  }
  
  public View findNextKeyboardNavigationCluster(View paramView1, View paramView2, int paramInt) {
    View view = null;
    if (paramView2 != null) {
      View view1 = findNextUserSpecifiedKeyboardNavigationCluster(paramView1, paramView2, paramInt);
      view = view1;
      if (view1 != null)
        return view1; 
    } 
    ArrayList<View> arrayList = this.mTempList;
    try {
      arrayList.clear();
      paramView1.addKeyboardNavigationClusters(arrayList, paramInt);
      if (!arrayList.isEmpty())
        view = findNextKeyboardNavigationCluster(paramView1, paramView2, arrayList, paramInt); 
      return view;
    } finally {
      arrayList.clear();
    } 
  }
  
  private View findNextUserSpecifiedKeyboardNavigationCluster(View paramView1, View paramView2, int paramInt) {
    paramView1 = paramView2.findUserSetNextKeyboardNavigationCluster(paramView1, paramInt);
    if (paramView1 != null && paramView1.hasFocusable())
      return paramView1; 
    return null;
  }
  
  private View findNextUserSpecifiedFocus(ViewGroup paramViewGroup, View paramView, int paramInt) {
    View view = paramView.findUserSetNextFocus(paramViewGroup, paramInt);
    paramView = view;
    boolean bool = true;
    while (view != null) {
      boolean bool1;
      if (view.isFocusable() && 
        view.getVisibility() == 0 && (
        !view.isInTouchMode() || 
        view.isFocusableInTouchMode()))
        return view; 
      View view1 = view.findUserSetNextFocus(paramViewGroup, paramInt);
      if (!bool) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      boolean bool2 = bool1;
      view = view1;
      bool = bool2;
      if (bool1) {
        View view2 = paramView.findUserSetNextFocus(paramViewGroup, paramInt);
        view = view1;
        paramView = view2;
        bool = bool2;
        if (view2 == view1)
          break; 
      } 
    } 
    return null;
  }
  
  private View findNextFocus(ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt, ArrayList<View> paramArrayList) {
    StringBuilder stringBuilder;
    if (paramView != null) {
      if (paramRect == null)
        paramRect = this.mFocusedRect; 
      paramView.getFocusedRect(paramRect);
      paramViewGroup.offsetDescendantRectToMyCoords(paramView, paramRect);
    } else if (paramRect == null) {
      paramRect = this.mFocusedRect;
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 17 && paramInt != 33) {
            if (paramInt == 66 || paramInt == 130)
              setFocusTopLeft(paramViewGroup, paramRect); 
          } else {
            setFocusBottomRight(paramViewGroup, paramRect);
          } 
        } else if (paramViewGroup.isLayoutRtl()) {
          setFocusBottomRight(paramViewGroup, paramRect);
        } else {
          setFocusTopLeft(paramViewGroup, paramRect);
        } 
      } else if (paramViewGroup.isLayoutRtl()) {
        setFocusTopLeft(paramViewGroup, paramRect);
      } else {
        setFocusBottomRight(paramViewGroup, paramRect);
      } 
    } 
    if (paramInt != 1 && paramInt != 2) {
      if (paramInt == 17 || paramInt == 33 || paramInt == 66 || paramInt == 130)
        return findNextFocusInAbsoluteDirection(paramArrayList, paramViewGroup, paramView, paramRect, paramInt); 
      stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown direction: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return findNextFocusInRelativeDirection(paramArrayList, (ViewGroup)stringBuilder, paramView, paramRect, paramInt);
  }
  
  private View findNextKeyboardNavigationCluster(View paramView1, View paramView2, List<View> paramList, int paramInt) {
    try {
      this.mUserSpecifiedClusterComparator.setFocusables(paramList, paramView1);
      Collections.sort(paramList, this.mUserSpecifiedClusterComparator);
      this.mUserSpecifiedClusterComparator.recycle();
      int i = paramList.size();
      return getPreviousKeyboardNavigationCluster((View)stringBuilder, paramView2, paramList, i);
    } finally {
      this.mUserSpecifiedClusterComparator.recycle();
    } 
  }
  
  private View findNextFocusInRelativeDirection(ArrayList<View> paramArrayList, ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt) {
    try {
      this.mUserSpecifiedFocusComparator.setFocusables(paramArrayList, paramViewGroup);
      Collections.sort(paramArrayList, this.mUserSpecifiedFocusComparator);
      this.mUserSpecifiedFocusComparator.recycle();
      int i = paramArrayList.size();
      return getPreviousFocusable(paramView, paramArrayList, i);
    } finally {
      this.mUserSpecifiedFocusComparator.recycle();
    } 
  }
  
  private void setFocusBottomRight(ViewGroup paramViewGroup, Rect paramRect) {
    int i = paramViewGroup.getScrollY() + paramViewGroup.getHeight();
    int j = paramViewGroup.getScrollX() + paramViewGroup.getWidth();
    paramRect.set(j, i, j, i);
  }
  
  private void setFocusTopLeft(ViewGroup paramViewGroup, Rect paramRect) {
    int i = paramViewGroup.getScrollY();
    int j = paramViewGroup.getScrollX();
    paramRect.set(j, i, j, i);
  }
  
  View findNextFocusInAbsoluteDirection(ArrayList<View> paramArrayList, ViewGroup paramViewGroup, View paramView, Rect paramRect, int paramInt) {
    this.mBestCandidateRect.set(paramRect);
    if (paramInt != 17) {
      if (paramInt != 33) {
        if (paramInt != 66) {
          if (paramInt == 130)
            this.mBestCandidateRect.offset(0, -(paramRect.height() + 1)); 
        } else {
          this.mBestCandidateRect.offset(-(paramRect.width() + 1), 0);
        } 
      } else {
        this.mBestCandidateRect.offset(0, paramRect.height() + 1);
      } 
    } else {
      this.mBestCandidateRect.offset(paramRect.width() + 1, 0);
    } 
    View view = null;
    int i = paramArrayList.size();
    for (byte b = 0; b < i; b++, view = view2) {
      View view1 = paramArrayList.get(b);
      View view2 = view;
      if (view1 != paramView)
        if (view1 == paramViewGroup) {
          view2 = view;
        } else {
          view1.getFocusedRect(this.mOtherRect);
          paramViewGroup.offsetDescendantRectToMyCoords(view1, this.mOtherRect);
          view2 = view;
          if (isBetterCandidate(paramInt, paramRect, this.mOtherRect, this.mBestCandidateRect)) {
            this.mBestCandidateRect.set(this.mOtherRect);
            view2 = view1;
          } 
        }  
    } 
    return view;
  }
  
  private static View getNextFocusable(View paramView, ArrayList<View> paramArrayList, int paramInt) {
    if (paramView != null) {
      int i = paramArrayList.lastIndexOf(paramView);
      if (i >= 0 && i + 1 < paramInt)
        return paramArrayList.get(i + 1); 
    } 
    if (!paramArrayList.isEmpty())
      return paramArrayList.get(0); 
    return null;
  }
  
  private static View getPreviousFocusable(View paramView, ArrayList<View> paramArrayList, int paramInt) {
    if (paramView != null) {
      int i = paramArrayList.indexOf(paramView);
      if (i > 0)
        return paramArrayList.get(i - 1); 
    } 
    if (!paramArrayList.isEmpty())
      return paramArrayList.get(paramInt - 1); 
    return null;
  }
  
  private static View getNextKeyboardNavigationCluster(View paramView1, View paramView2, List<View> paramList, int paramInt) {
    if (paramView2 == null)
      return paramList.get(0); 
    int i = paramList.lastIndexOf(paramView2);
    if (i >= 0 && i + 1 < paramInt)
      return paramList.get(i + 1); 
    return paramView1;
  }
  
  private static View getPreviousKeyboardNavigationCluster(View paramView1, View paramView2, List<View> paramList, int paramInt) {
    if (paramView2 == null)
      return paramList.get(paramInt - 1); 
    paramInt = paramList.indexOf(paramView2);
    if (paramInt > 0)
      return paramList.get(paramInt - 1); 
    return paramView1;
  }
  
  boolean isBetterCandidate(int paramInt, Rect paramRect1, Rect paramRect2, Rect paramRect3) {
    boolean bool = isCandidate(paramRect1, paramRect2, paramInt);
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (!isCandidate(paramRect1, paramRect3, paramInt))
      return true; 
    if (beamBeats(paramInt, paramRect1, paramRect2, paramRect3))
      return true; 
    if (beamBeats(paramInt, paramRect1, paramRect3, paramRect2))
      return false; 
    long l1 = majorAxisDistance(paramInt, paramRect1, paramRect2);
    long l2 = minorAxisDistance(paramInt, paramRect1, paramRect2);
    l1 = getWeightedDistanceFor(l1, l2);
    long l3 = majorAxisDistance(paramInt, paramRect1, paramRect3);
    l2 = minorAxisDistance(paramInt, paramRect1, paramRect3);
    if (l1 < getWeightedDistanceFor(l3, l2))
      bool1 = true; 
    return bool1;
  }
  
  boolean beamBeats(int paramInt, Rect paramRect1, Rect paramRect2, Rect paramRect3) {
    boolean bool1 = beamsOverlap(paramInt, paramRect1, paramRect2);
    boolean bool2 = beamsOverlap(paramInt, paramRect1, paramRect3);
    boolean bool = false;
    if (bool2 || !bool1)
      return false; 
    if (!isToDirectionOf(paramInt, paramRect1, paramRect3))
      return true; 
    if (paramInt == 17 || paramInt == 66)
      return true; 
    int i = majorAxisDistance(paramInt, paramRect1, paramRect2);
    if (i < majorAxisDistanceToFarEdge(paramInt, paramRect1, paramRect3))
      bool = true; 
    return bool;
  }
  
  long getWeightedDistanceFor(long paramLong1, long paramLong2) {
    return 13L * paramLong1 * paramLong1 + paramLong2 * paramLong2;
  }
  
  boolean isCandidate(Rect paramRect1, Rect paramRect2, int paramInt) {
    boolean bool1 = true, bool2 = true, bool3 = true, bool4 = true;
    if (paramInt != 17) {
      if (paramInt != 33) {
        if (paramInt != 66) {
          if (paramInt == 130) {
            if ((paramRect1.top < paramRect2.top || paramRect1.bottom <= paramRect2.top) && paramRect1.bottom < paramRect2.bottom) {
              bool1 = bool4;
            } else {
              bool1 = false;
            } 
            return bool1;
          } 
          throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        } 
        if ((paramRect1.left >= paramRect2.left && paramRect1.right > paramRect2.left) || paramRect1.right >= paramRect2.right)
          bool1 = false; 
        return bool1;
      } 
      if ((paramRect1.bottom > paramRect2.bottom || paramRect1.top >= paramRect2.bottom) && paramRect1.top > paramRect2.top) {
        bool1 = bool2;
      } else {
        bool1 = false;
      } 
      return bool1;
    } 
    if ((paramRect1.right > paramRect2.right || paramRect1.left >= paramRect2.right) && paramRect1.left > paramRect2.left) {
      bool1 = bool3;
    } else {
      bool1 = false;
    } 
    return bool1;
  }
  
  boolean beamsOverlap(int paramInt, Rect paramRect1, Rect paramRect2) {
    // Byte code:
    //   0: iconst_1
    //   1: istore #4
    //   3: iconst_1
    //   4: istore #5
    //   6: iload_1
    //   7: bipush #17
    //   9: if_icmpeq -> 80
    //   12: iload_1
    //   13: bipush #33
    //   15: if_icmpeq -> 45
    //   18: iload_1
    //   19: bipush #66
    //   21: if_icmpeq -> 80
    //   24: iload_1
    //   25: sipush #130
    //   28: if_icmpne -> 34
    //   31: goto -> 45
    //   34: new java/lang/IllegalArgumentException
    //   37: dup
    //   38: ldc_w 'direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.'
    //   41: invokespecial <init> : (Ljava/lang/String;)V
    //   44: athrow
    //   45: aload_3
    //   46: getfield right : I
    //   49: aload_2
    //   50: getfield left : I
    //   53: if_icmple -> 74
    //   56: aload_3
    //   57: getfield left : I
    //   60: aload_2
    //   61: getfield right : I
    //   64: if_icmpge -> 74
    //   67: iload #5
    //   69: istore #4
    //   71: goto -> 77
    //   74: iconst_0
    //   75: istore #4
    //   77: iload #4
    //   79: ireturn
    //   80: aload_3
    //   81: getfield bottom : I
    //   84: aload_2
    //   85: getfield top : I
    //   88: if_icmple -> 105
    //   91: aload_3
    //   92: getfield top : I
    //   95: aload_2
    //   96: getfield bottom : I
    //   99: if_icmpge -> 105
    //   102: goto -> 108
    //   105: iconst_0
    //   106: istore #4
    //   108: iload #4
    //   110: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #574	-> 0
    //   #582	-> 34
    //   #580	-> 45
    //   #577	-> 80
  }
  
  boolean isToDirectionOf(int paramInt, Rect paramRect1, Rect paramRect2) {
    boolean bool1 = true, bool2 = true, bool3 = true, bool4 = true;
    if (paramInt != 17) {
      if (paramInt != 33) {
        if (paramInt != 66) {
          if (paramInt == 130) {
            if (paramRect1.bottom <= paramRect2.top) {
              bool3 = bool4;
            } else {
              bool3 = false;
            } 
            return bool3;
          } 
          throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        } 
        if (paramRect1.right <= paramRect2.left) {
          bool3 = bool1;
        } else {
          bool3 = false;
        } 
        return bool3;
      } 
      if (paramRect1.top >= paramRect2.bottom) {
        bool3 = bool2;
      } else {
        bool3 = false;
      } 
      return bool3;
    } 
    if (paramRect1.left < paramRect2.right)
      bool3 = false; 
    return bool3;
  }
  
  static int majorAxisDistance(int paramInt, Rect paramRect1, Rect paramRect2) {
    return Math.max(0, majorAxisDistanceRaw(paramInt, paramRect1, paramRect2));
  }
  
  static int majorAxisDistanceRaw(int paramInt, Rect paramRect1, Rect paramRect2) {
    if (paramInt != 17) {
      if (paramInt != 33) {
        if (paramInt != 66) {
          if (paramInt == 130)
            return paramRect2.top - paramRect1.bottom; 
          throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        } 
        return paramRect2.left - paramRect1.right;
      } 
      return paramRect1.top - paramRect2.bottom;
    } 
    return paramRect1.left - paramRect2.right;
  }
  
  static int majorAxisDistanceToFarEdge(int paramInt, Rect paramRect1, Rect paramRect2) {
    return Math.max(1, majorAxisDistanceToFarEdgeRaw(paramInt, paramRect1, paramRect2));
  }
  
  static int majorAxisDistanceToFarEdgeRaw(int paramInt, Rect paramRect1, Rect paramRect2) {
    if (paramInt != 17) {
      if (paramInt != 33) {
        if (paramInt != 66) {
          if (paramInt == 130)
            return paramRect2.bottom - paramRect1.bottom; 
          throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        } 
        return paramRect2.right - paramRect1.right;
      } 
      return paramRect1.top - paramRect2.top;
    } 
    return paramRect1.left - paramRect2.left;
  }
  
  static int minorAxisDistance(int paramInt, Rect paramRect1, Rect paramRect2) {
    // Byte code:
    //   0: iload_0
    //   1: bipush #17
    //   3: if_icmpeq -> 78
    //   6: iload_0
    //   7: bipush #33
    //   9: if_icmpeq -> 39
    //   12: iload_0
    //   13: bipush #66
    //   15: if_icmpeq -> 78
    //   18: iload_0
    //   19: sipush #130
    //   22: if_icmpne -> 28
    //   25: goto -> 39
    //   28: new java/lang/IllegalArgumentException
    //   31: dup
    //   32: ldc_w 'direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.'
    //   35: invokespecial <init> : (Ljava/lang/String;)V
    //   38: athrow
    //   39: aload_1
    //   40: getfield left : I
    //   43: istore_0
    //   44: aload_1
    //   45: invokevirtual width : ()I
    //   48: iconst_2
    //   49: idiv
    //   50: istore_3
    //   51: aload_2
    //   52: getfield left : I
    //   55: istore #4
    //   57: aload_2
    //   58: invokevirtual width : ()I
    //   61: iconst_2
    //   62: idiv
    //   63: istore #5
    //   65: iload_0
    //   66: iload_3
    //   67: iadd
    //   68: iload #4
    //   70: iload #5
    //   72: iadd
    //   73: isub
    //   74: invokestatic abs : (I)I
    //   77: ireturn
    //   78: aload_1
    //   79: getfield top : I
    //   82: istore_0
    //   83: aload_1
    //   84: invokevirtual height : ()I
    //   87: iconst_2
    //   88: idiv
    //   89: istore #4
    //   91: aload_2
    //   92: getfield top : I
    //   95: istore #5
    //   97: aload_2
    //   98: invokevirtual height : ()I
    //   101: iconst_2
    //   102: idiv
    //   103: istore_3
    //   104: iload_0
    //   105: iload #4
    //   107: iadd
    //   108: iload #5
    //   110: iload_3
    //   111: iadd
    //   112: isub
    //   113: invokestatic abs : (I)I
    //   116: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #662	-> 0
    //   #676	-> 28
    //   #672	-> 39
    //   #673	-> 44
    //   #674	-> 57
    //   #672	-> 65
    //   #666	-> 78
    //   #667	-> 83
    //   #668	-> 97
    //   #666	-> 104
  }
  
  public View findNearestTouchable(ViewGroup paramViewGroup, int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getTouchables : ()Ljava/util/ArrayList;
    //   4: astore #6
    //   6: ldc_w 2147483647
    //   9: istore #7
    //   11: aconst_null
    //   12: astore #8
    //   14: aload #6
    //   16: invokevirtual size : ()I
    //   19: istore #9
    //   21: aload_1
    //   22: getfield mContext : Landroid/content/Context;
    //   25: invokestatic get : (Landroid/content/Context;)Landroid/view/ViewConfiguration;
    //   28: invokevirtual getScaledEdgeSlop : ()I
    //   31: istore #10
    //   33: new android/graphics/Rect
    //   36: dup
    //   37: invokespecial <init> : ()V
    //   40: astore #11
    //   42: aload_0
    //   43: getfield mOtherRect : Landroid/graphics/Rect;
    //   46: astore #12
    //   48: iconst_0
    //   49: istore #13
    //   51: iload #13
    //   53: iload #9
    //   55: if_icmpge -> 352
    //   58: aload #6
    //   60: iload #13
    //   62: invokevirtual get : (I)Ljava/lang/Object;
    //   65: checkcast android/view/View
    //   68: astore #14
    //   70: aload #14
    //   72: aload #12
    //   74: invokevirtual getDrawingRect : (Landroid/graphics/Rect;)V
    //   77: aload_1
    //   78: aload #14
    //   80: aload #12
    //   82: iconst_1
    //   83: iconst_1
    //   84: invokevirtual offsetRectBetweenParentAndChild : (Landroid/view/View;Landroid/graphics/Rect;ZZ)V
    //   87: aload_0
    //   88: iload_2
    //   89: iload_3
    //   90: aload #12
    //   92: iload #4
    //   94: invokespecial isTouchCandidate : (IILandroid/graphics/Rect;I)Z
    //   97: ifne -> 111
    //   100: iload #7
    //   102: istore #15
    //   104: aload #8
    //   106: astore #16
    //   108: goto -> 338
    //   111: ldc_w 2147483647
    //   114: istore #17
    //   116: iload #4
    //   118: bipush #17
    //   120: if_icmpeq -> 182
    //   123: iload #4
    //   125: bipush #33
    //   127: if_icmpeq -> 168
    //   130: iload #4
    //   132: bipush #66
    //   134: if_icmpeq -> 158
    //   137: iload #4
    //   139: sipush #130
    //   142: if_icmpeq -> 148
    //   145: goto -> 193
    //   148: aload #12
    //   150: getfield top : I
    //   153: istore #17
    //   155: goto -> 193
    //   158: aload #12
    //   160: getfield left : I
    //   163: istore #17
    //   165: goto -> 193
    //   168: iload_3
    //   169: aload #12
    //   171: getfield bottom : I
    //   174: isub
    //   175: iconst_1
    //   176: iadd
    //   177: istore #17
    //   179: goto -> 193
    //   182: iload_2
    //   183: aload #12
    //   185: getfield right : I
    //   188: isub
    //   189: iconst_1
    //   190: iadd
    //   191: istore #17
    //   193: iload #7
    //   195: istore #15
    //   197: aload #8
    //   199: astore #16
    //   201: iload #17
    //   203: iload #10
    //   205: if_icmpge -> 338
    //   208: aload #8
    //   210: ifnull -> 256
    //   213: aload #11
    //   215: aload #12
    //   217: invokevirtual contains : (Landroid/graphics/Rect;)Z
    //   220: ifne -> 256
    //   223: iload #7
    //   225: istore #15
    //   227: aload #8
    //   229: astore #16
    //   231: aload #12
    //   233: aload #11
    //   235: invokevirtual contains : (Landroid/graphics/Rect;)Z
    //   238: ifne -> 338
    //   241: iload #7
    //   243: istore #15
    //   245: aload #8
    //   247: astore #16
    //   249: iload #17
    //   251: iload #7
    //   253: if_icmpge -> 338
    //   256: iload #17
    //   258: istore #15
    //   260: aload #14
    //   262: astore #16
    //   264: aload #11
    //   266: aload #12
    //   268: invokevirtual set : (Landroid/graphics/Rect;)V
    //   271: iload #4
    //   273: bipush #17
    //   275: if_icmpeq -> 331
    //   278: iload #4
    //   280: bipush #33
    //   282: if_icmpeq -> 321
    //   285: iload #4
    //   287: bipush #66
    //   289: if_icmpeq -> 312
    //   292: iload #4
    //   294: sipush #130
    //   297: if_icmpeq -> 303
    //   300: goto -> 338
    //   303: aload #5
    //   305: iconst_1
    //   306: iload #17
    //   308: iastore
    //   309: goto -> 338
    //   312: aload #5
    //   314: iconst_0
    //   315: iload #17
    //   317: iastore
    //   318: goto -> 338
    //   321: aload #5
    //   323: iconst_1
    //   324: iload #17
    //   326: ineg
    //   327: iastore
    //   328: goto -> 338
    //   331: aload #5
    //   333: iconst_0
    //   334: iload #17
    //   336: ineg
    //   337: iastore
    //   338: iinc #13, 1
    //   341: iload #15
    //   343: istore #7
    //   345: aload #16
    //   347: astore #8
    //   349: goto -> 51
    //   352: aload #8
    //   354: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #692	-> 0
    //   #693	-> 6
    //   #694	-> 11
    //   #696	-> 14
    //   #698	-> 21
    //   #700	-> 33
    //   #701	-> 42
    //   #703	-> 48
    //   #704	-> 58
    //   #707	-> 70
    //   #709	-> 77
    //   #711	-> 87
    //   #712	-> 100
    //   #715	-> 111
    //   #717	-> 116
    //   #728	-> 148
    //   #722	-> 158
    //   #723	-> 165
    //   #725	-> 168
    //   #726	-> 179
    //   #719	-> 182
    //   #720	-> 193
    //   #732	-> 193
    //   #734	-> 208
    //   #735	-> 213
    //   #736	-> 223
    //   #737	-> 256
    //   #738	-> 260
    //   #739	-> 264
    //   #740	-> 271
    //   #751	-> 303
    //   #745	-> 312
    //   #746	-> 318
    //   #748	-> 321
    //   #749	-> 328
    //   #742	-> 331
    //   #703	-> 338
    //   #757	-> 352
  }
  
  private boolean isTouchCandidate(int paramInt1, int paramInt2, Rect paramRect, int paramInt3) {
    boolean bool1 = true, bool2 = true, bool3 = true, bool4 = true;
    if (paramInt3 != 17) {
      if (paramInt3 != 33) {
        if (paramInt3 != 66) {
          if (paramInt3 == 130) {
            if (paramRect.top >= paramInt2 && paramRect.left <= paramInt1 && paramInt1 <= paramRect.right) {
              bool1 = bool4;
            } else {
              bool1 = false;
            } 
            return bool1;
          } 
          throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        } 
        if (paramRect.left < paramInt1 || paramRect.top > paramInt2 || paramInt2 > paramRect.bottom)
          bool1 = false; 
        return bool1;
      } 
      if (paramRect.top <= paramInt2 && paramRect.left <= paramInt1 && paramInt1 <= paramRect.right) {
        bool1 = bool2;
      } else {
        bool1 = false;
      } 
      return bool1;
    } 
    if (paramRect.left <= paramInt1 && paramRect.top <= paramInt2 && paramInt2 <= paramRect.bottom) {
      bool1 = bool3;
    } else {
      bool1 = false;
    } 
    return bool1;
  }
  
  private static final boolean isValidId(int paramInt) {
    boolean bool;
    if (paramInt != 0 && paramInt != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  static final class FocusSorter {
    private int mLastPoolRect;
    
    private HashMap<View, Rect> mRectByView;
    
    private ArrayList<Rect> mRectPool;
    
    private int mRtlMult;
    
    private Comparator<View> mSidesComparator;
    
    private Comparator<View> mTopsComparator;
    
    FocusSorter() {
      this.mRectPool = new ArrayList<>();
      this.mRectByView = null;
      this.mTopsComparator = new _$$Lambda$FocusFinder$FocusSorter$kW7K1t9q7Y62V38r_7g6xRzqqq8(this);
      this.mSidesComparator = new _$$Lambda$FocusFinder$FocusSorter$h0f2ZYL6peSaaEeCCkAoYs_YZvU(this);
    }
    
    public void sort(View[] param1ArrayOfView, int param1Int1, int param1Int2, ViewGroup param1ViewGroup, boolean param1Boolean) {
      int i = param1Int2 - param1Int1;
      if (i < 2)
        return; 
      if (this.mRectByView == null)
        this.mRectByView = new HashMap<>(); 
      if (param1Boolean) {
        j = -1;
      } else {
        j = 1;
      } 
      this.mRtlMult = j;
      int j;
      for (j = this.mRectPool.size(); j < i; j++)
        this.mRectPool.add(new Rect()); 
      for (j = param1Int1; j < param1Int2; j++) {
        ArrayList<Rect> arrayList = this.mRectPool;
        int m = this.mLastPoolRect;
        this.mLastPoolRect = m + 1;
        Rect rect = arrayList.get(m);
        param1ArrayOfView[j].getDrawingRect(rect);
        param1ViewGroup.offsetDescendantRectToMyCoords(param1ArrayOfView[j], rect);
        this.mRectByView.put(param1ArrayOfView[j], rect);
      } 
      Arrays.sort(param1ArrayOfView, param1Int1, i, this.mTopsComparator);
      int k = ((Rect)this.mRectByView.get(param1ArrayOfView[param1Int1])).bottom;
      j = param1Int1;
      param1Int1++;
      for (; param1Int1 < param1Int2; param1Int1++, k = j, j = i) {
        Rect rect = this.mRectByView.get(param1ArrayOfView[param1Int1]);
        if (rect.top >= k) {
          if (param1Int1 - j > 1)
            Arrays.sort(param1ArrayOfView, j, param1Int1, this.mSidesComparator); 
          j = rect.bottom;
          i = param1Int1;
        } else {
          k = Math.max(k, rect.bottom);
          i = j;
          j = k;
        } 
      } 
      if (param1Int1 - j > 1)
        Arrays.sort(param1ArrayOfView, j, param1Int1, this.mSidesComparator); 
      this.mLastPoolRect = 0;
      this.mRectByView.clear();
    }
  }
  
  public static void sort(View[] paramArrayOfView, int paramInt1, int paramInt2, ViewGroup paramViewGroup, boolean paramBoolean) {
    (getInstance()).mFocusSorter.sort(paramArrayOfView, paramInt1, paramInt2, paramViewGroup, paramBoolean);
  }
  
  private FocusFinder() {}
  
  private static final class UserSpecifiedFocusComparator implements Comparator<View> {
    private final ArrayMap<View, View> mNextFoci = new ArrayMap<>();
    
    private final ArraySet<View> mIsConnectedTo = new ArraySet<>();
    
    private final ArrayMap<View, View> mHeadsOfChains = new ArrayMap<>();
    
    private final ArrayMap<View, Integer> mOriginalOrdinal = new ArrayMap<>();
    
    private final NextFocusGetter mNextFocusGetter;
    
    private View mRoot;
    
    UserSpecifiedFocusComparator(NextFocusGetter param1NextFocusGetter) {
      this.mNextFocusGetter = param1NextFocusGetter;
    }
    
    public void recycle() {
      this.mRoot = null;
      this.mHeadsOfChains.clear();
      this.mIsConnectedTo.clear();
      this.mOriginalOrdinal.clear();
      this.mNextFoci.clear();
    }
    
    public void setFocusables(List<View> param1List, View param1View) {
      this.mRoot = param1View;
      int i;
      for (i = 0; i < param1List.size(); i++)
        this.mOriginalOrdinal.put(param1List.get(i), Integer.valueOf(i)); 
      for (i = param1List.size() - 1; i >= 0; i--) {
        param1View = param1List.get(i);
        View view = this.mNextFocusGetter.get(this.mRoot, param1View);
        if (view != null && this.mOriginalOrdinal.containsKey(view)) {
          this.mNextFoci.put(param1View, view);
          this.mIsConnectedTo.add(view);
        } 
      } 
      for (i = param1List.size() - 1; i >= 0; i--) {
        param1View = param1List.get(i);
        View view = this.mNextFoci.get(param1View);
        if (view != null && !this.mIsConnectedTo.contains(param1View))
          setHeadOfChain(param1View); 
      } 
    }
    
    private void setHeadOfChain(View param1View) {
      for (View view = param1View; view != null; view = this.mNextFoci.get(view), param1View = view2) {
        View view1 = this.mHeadsOfChains.get(view);
        View view2 = param1View;
        if (view1 != null) {
          if (view1 == param1View)
            return; 
          view2 = view1;
          view = param1View;
        } 
        this.mHeadsOfChains.put(view, view2);
      } 
    }
    
    public int compare(View param1View1, View param1View2) {
      if (param1View1 == param1View2)
        return 0; 
      View view1 = this.mHeadsOfChains.get(param1View1);
      View view2 = this.mHeadsOfChains.get(param1View2);
      byte b1 = 1;
      if (view1 == view2 && view1 != null) {
        if (param1View1 == view1)
          return -1; 
        if (param1View2 == view1)
          return 1; 
        if (this.mNextFoci.get(param1View1) != null)
          return -1; 
        return 1;
      } 
      byte b2 = 0;
      if (view1 != null) {
        param1View1 = view1;
        b2 = 1;
      } 
      if (view2 != null) {
        param1View2 = view2;
        b2 = 1;
      } 
      if (b2) {
        b2 = b1;
        if (((Integer)this.mOriginalOrdinal.get(param1View1)).intValue() < ((Integer)this.mOriginalOrdinal.get(param1View2)).intValue())
          b2 = -1; 
        return b2;
      } 
      return 0;
    }
    
    public static interface NextFocusGetter {
      View get(View param2View1, View param2View2);
    }
  }
  
  public static interface NextFocusGetter {
    View get(View param1View1, View param1View2);
  }
}
