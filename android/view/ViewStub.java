package android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RemoteViews.RemoteView;
import com.android.internal.R;
import java.lang.ref.WeakReference;

@RemoteView
public final class ViewStub extends View {
  private OnInflateListener mInflateListener;
  
  private int mInflatedId;
  
  private WeakReference<View> mInflatedViewRef;
  
  private LayoutInflater mInflater;
  
  private int mLayoutResource;
  
  public ViewStub(Context paramContext) {
    this(paramContext, 0);
  }
  
  public ViewStub(Context paramContext, int paramInt) {
    this(paramContext, (AttributeSet)null);
    this.mLayoutResource = paramInt;
  }
  
  public ViewStub(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ViewStub(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ViewStub(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewStub, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ViewStub, paramAttributeSet, typedArray, paramInt1, paramInt2);
    this.mInflatedId = typedArray.getResourceId(2, -1);
    this.mLayoutResource = typedArray.getResourceId(1, 0);
    this.mID = typedArray.getResourceId(0, -1);
    typedArray.recycle();
    setVisibility(8);
    setWillNotDraw(true);
  }
  
  public int getInflatedId() {
    return this.mInflatedId;
  }
  
  @RemotableViewMethod(asyncImpl = "setInflatedIdAsync")
  public void setInflatedId(int paramInt) {
    this.mInflatedId = paramInt;
  }
  
  public Runnable setInflatedIdAsync(int paramInt) {
    this.mInflatedId = paramInt;
    return null;
  }
  
  public int getLayoutResource() {
    return this.mLayoutResource;
  }
  
  @RemotableViewMethod(asyncImpl = "setLayoutResourceAsync")
  public void setLayoutResource(int paramInt) {
    this.mLayoutResource = paramInt;
  }
  
  public Runnable setLayoutResourceAsync(int paramInt) {
    this.mLayoutResource = paramInt;
    return null;
  }
  
  public void setLayoutInflater(LayoutInflater paramLayoutInflater) {
    this.mInflater = paramLayoutInflater;
  }
  
  public LayoutInflater getLayoutInflater() {
    return this.mInflater;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    setMeasuredDimension(0, 0);
  }
  
  public void draw(Canvas paramCanvas) {}
  
  protected void dispatchDraw(Canvas paramCanvas) {}
  
  @RemotableViewMethod(asyncImpl = "setVisibilityAsync")
  public void setVisibility(int paramInt) {
    WeakReference<View> weakReference = this.mInflatedViewRef;
    if (weakReference != null) {
      View view = weakReference.get();
      if (view != null) {
        view.setVisibility(paramInt);
      } else {
        throw new IllegalStateException("setVisibility called on un-referenced view");
      } 
    } else {
      super.setVisibility(paramInt);
      if (paramInt == 0 || paramInt == 4)
        inflate(); 
    } 
  }
  
  public Runnable setVisibilityAsync(int paramInt) {
    if (paramInt == 0 || paramInt == 4) {
      ViewGroup viewGroup = (ViewGroup)getParent();
      return new ViewReplaceRunnable(inflateViewNoAdd(viewGroup));
    } 
    return null;
  }
  
  private View inflateViewNoAdd(ViewGroup paramViewGroup) {
    LayoutInflater layoutInflater;
    if (this.mInflater != null) {
      layoutInflater = this.mInflater;
    } else {
      layoutInflater = LayoutInflater.from(this.mContext);
    } 
    View view = layoutInflater.inflate(this.mLayoutResource, paramViewGroup, false);
    int i = this.mInflatedId;
    if (i != -1)
      view.setId(i); 
    return view;
  }
  
  private void replaceSelfWithView(View paramView, ViewGroup paramViewGroup) {
    int i = paramViewGroup.indexOfChild(this);
    paramViewGroup.removeViewInLayout(this);
    ViewGroup.LayoutParams layoutParams = getLayoutParams();
    if (layoutParams != null) {
      paramViewGroup.addView(paramView, i, layoutParams);
    } else {
      paramViewGroup.addView(paramView, i);
    } 
  }
  
  public View inflate() {
    ViewParent viewParent = getParent();
    if (viewParent != null && viewParent instanceof ViewGroup) {
      if (this.mLayoutResource != 0) {
        ViewGroup viewGroup = (ViewGroup)viewParent;
        View view = inflateViewNoAdd(viewGroup);
        replaceSelfWithView(view, viewGroup);
        this.mInflatedViewRef = new WeakReference<>(view);
        OnInflateListener onInflateListener = this.mInflateListener;
        if (onInflateListener != null)
          onInflateListener.onInflate(this, view); 
        return view;
      } 
      throw new IllegalArgumentException("ViewStub must have a valid layoutResource");
    } 
    throw new IllegalStateException("ViewStub must have a non-null ViewGroup viewParent");
  }
  
  public void setOnInflateListener(OnInflateListener paramOnInflateListener) {
    this.mInflateListener = paramOnInflateListener;
  }
  
  class OnInflateListener {
    public abstract void onInflate(ViewStub param1ViewStub, View param1View);
  }
  
  class ViewReplaceRunnable implements Runnable {
    final ViewStub this$0;
    
    public final View view;
    
    ViewReplaceRunnable(View param1View) {
      this.view = param1View;
    }
    
    public void run() {
      ViewStub viewStub = ViewStub.this;
      viewStub.replaceSelfWithView(this.view, (ViewGroup)viewStub.getParent());
    }
  }
}
