package android.view;

public interface FallbackEventHandler {
  boolean dispatchKeyEvent(KeyEvent paramKeyEvent);
  
  void preDispatchKeyEvent(KeyEvent paramKeyEvent);
  
  void setView(View paramView);
}
