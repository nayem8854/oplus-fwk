package android.view;

import android.graphics.drawable.Drawable;

public interface ContextMenu extends Menu {
  void clearHeader();
  
  ContextMenu setHeaderIcon(int paramInt);
  
  ContextMenu setHeaderIcon(Drawable paramDrawable);
  
  ContextMenu setHeaderTitle(int paramInt);
  
  ContextMenu setHeaderTitle(CharSequence paramCharSequence);
  
  ContextMenu setHeaderView(View paramView);
  
  class ContextMenuInfo {}
}
