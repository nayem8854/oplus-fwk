package android.view;

import android.graphics.Region;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISystemGestureExclusionListener extends IInterface {
  void onSystemGestureExclusionChanged(int paramInt, Region paramRegion1, Region paramRegion2) throws RemoteException;
  
  class Default implements ISystemGestureExclusionListener {
    public void onSystemGestureExclusionChanged(int param1Int, Region param1Region1, Region param1Region2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISystemGestureExclusionListener {
    private static final String DESCRIPTOR = "android.view.ISystemGestureExclusionListener";
    
    static final int TRANSACTION_onSystemGestureExclusionChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.view.ISystemGestureExclusionListener");
    }
    
    public static ISystemGestureExclusionListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.ISystemGestureExclusionListener");
      if (iInterface != null && iInterface instanceof ISystemGestureExclusionListener)
        return (ISystemGestureExclusionListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onSystemGestureExclusionChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.ISystemGestureExclusionListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.ISystemGestureExclusionListener");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        Region region = (Region)Region.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        Region region = (Region)Region.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onSystemGestureExclusionChanged(param1Int1, (Region)param1Parcel2, (Region)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ISystemGestureExclusionListener {
      public static ISystemGestureExclusionListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.ISystemGestureExclusionListener";
      }
      
      public void onSystemGestureExclusionChanged(int param2Int, Region param2Region1, Region param2Region2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.ISystemGestureExclusionListener");
          parcel.writeInt(param2Int);
          if (param2Region1 != null) {
            parcel.writeInt(1);
            param2Region1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Region2 != null) {
            parcel.writeInt(1);
            param2Region2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISystemGestureExclusionListener.Stub.getDefaultImpl() != null) {
            ISystemGestureExclusionListener.Stub.getDefaultImpl().onSystemGestureExclusionChanged(param2Int, param2Region1, param2Region2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISystemGestureExclusionListener param1ISystemGestureExclusionListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISystemGestureExclusionListener != null) {
          Proxy.sDefaultImpl = param1ISystemGestureExclusionListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISystemGestureExclusionListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
