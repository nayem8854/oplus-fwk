package android.view;

import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import java.io.PrintStream;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class ScrollCaptureTargetResolver {
  private static final boolean DEBUG = true;
  
  private static final String TAG = "ScrollCaptureTargetRes";
  
  private long mDeadlineMillis;
  
  private boolean mFinished;
  
  private Handler mHandler;
  
  private final Object mLock = new Object();
  
  private int mPendingBoundsRequests;
  
  private ScrollCaptureTarget mResult;
  
  private boolean mStarted;
  
  private final Queue<ScrollCaptureTarget> mTargets;
  
  private long mTimeLimitMillis;
  
  private final Runnable mTimeoutRunnable;
  
  private Consumer<ScrollCaptureTarget> mWhenComplete;
  
  private static int area(Rect paramRect) {
    return paramRect.width() * paramRect.height();
  }
  
  private static boolean nullOrEmpty(Rect paramRect) {
    return (paramRect == null || paramRect.isEmpty());
  }
  
  private static ScrollCaptureTarget chooseTarget(ScrollCaptureTarget paramScrollCaptureTarget1, ScrollCaptureTarget paramScrollCaptureTarget2) {
    StringBuilder stringBuilder2;
    ScrollCaptureTarget scrollCaptureTarget;
    StringBuilder stringBuilder1, stringBuilder4 = new StringBuilder();
    stringBuilder4.append("chooseTarget: ");
    stringBuilder4.append(paramScrollCaptureTarget1);
    stringBuilder4.append(" or ");
    stringBuilder4.append(paramScrollCaptureTarget2);
    Log.d("ScrollCaptureTargetRes", stringBuilder4.toString());
    if (paramScrollCaptureTarget1 == null && paramScrollCaptureTarget2 == null) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("chooseTarget: (both null) return ");
      stringBuilder2.append((Object)null);
      Log.d("ScrollCaptureTargetRes", stringBuilder2.toString());
      return null;
    } 
    if (stringBuilder2 == null || paramScrollCaptureTarget2 == null) {
      if (stringBuilder2 == null)
        scrollCaptureTarget = paramScrollCaptureTarget2; 
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append("chooseTarget: (other is null) return ");
      stringBuilder3.append(scrollCaptureTarget);
      Log.d("ScrollCaptureTargetRes", stringBuilder3.toString());
      return scrollCaptureTarget;
    } 
    boolean bool1 = nullOrEmpty(scrollCaptureTarget.getScrollBounds());
    boolean bool2 = nullOrEmpty(stringBuilder3.getScrollBounds());
    if (bool1 || bool2) {
      if (bool1 && bool2) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("chooseTarget: (both have empty or null bounds) return ");
        stringBuilder1.append((Object)null);
        Log.d("ScrollCaptureTargetRes", stringBuilder1.toString());
        return null;
      } 
      if (bool1) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("chooseTarget: (a has empty or null bounds) return ");
        stringBuilder1.append(stringBuilder3);
        Log.d("ScrollCaptureTargetRes", stringBuilder1.toString());
        return (ScrollCaptureTarget)stringBuilder3;
      } 
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append("chooseTarget: (b has empty or null bounds) return ");
      stringBuilder3.append(stringBuilder1);
      Log.d("ScrollCaptureTargetRes", stringBuilder3.toString());
      return (ScrollCaptureTarget)stringBuilder1;
    } 
    View view2 = stringBuilder1.getContainingView();
    View view1 = stringBuilder3.getContainingView();
    bool2 = hasIncludeHint(view2);
    bool1 = hasIncludeHint(view1);
    if (bool2 != bool1) {
      if (!bool2)
        stringBuilder1 = stringBuilder3; 
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append("chooseTarget: (has hint=INCLUDE) return ");
      stringBuilder3.append(stringBuilder1);
      Log.d("ScrollCaptureTargetRes", stringBuilder3.toString());
      return (ScrollCaptureTarget)stringBuilder1;
    } 
    if (isDescendant(view2, view1)) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("chooseTarget: (b is descendant of a) return ");
      stringBuilder1.append(stringBuilder3);
      Log.d("ScrollCaptureTargetRes", stringBuilder1.toString());
      return (ScrollCaptureTarget)stringBuilder3;
    } 
    if (isDescendant(view1, view2)) {
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append("chooseTarget: (a is descendant of b) return ");
      stringBuilder3.append(stringBuilder1);
      Log.d("ScrollCaptureTargetRes", stringBuilder3.toString());
      return (ScrollCaptureTarget)stringBuilder1;
    } 
    int i = area(stringBuilder1.getScrollBounds());
    int j = area(stringBuilder3.getScrollBounds());
    if (i < j)
      stringBuilder1 = stringBuilder3; 
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("chooseTarget: return ");
    stringBuilder3.append(stringBuilder1);
    Log.d("ScrollCaptureTargetRes", stringBuilder3.toString());
    return (ScrollCaptureTarget)stringBuilder1;
  }
  
  void checkThread() {
    if (this.mHandler.getLooper() == Looper.myLooper())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Called from wrong thread! (");
    stringBuilder.append(Thread.currentThread().getName());
    stringBuilder.append(")");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public ScrollCaptureTarget waitForResult() throws InterruptedException {
    synchronized (this.mLock) {
      while (!this.mFinished)
        this.mLock.wait(); 
      return this.mResult;
    } 
  }
  
  private void supplyResult(ScrollCaptureTarget paramScrollCaptureTarget) {
    boolean bool;
    checkThread();
    if (this.mFinished)
      return; 
    this.mResult = chooseTarget(this.mResult, paramScrollCaptureTarget);
    if (this.mPendingBoundsRequests == 0 || 
      SystemClock.elapsedRealtime() >= this.mDeadlineMillis) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      System.err.println("We think we're done, or timed out");
      this.mPendingBoundsRequests = 0;
      this.mWhenComplete.accept(this.mResult);
      synchronized (this.mLock) {
        this.mFinished = true;
        this.mLock.notify();
        this.mWhenComplete = null;
      } 
    } 
  }
  
  public void start(Handler paramHandler, long paramLong, Consumer<ScrollCaptureTarget> paramConsumer) {
    synchronized (this.mLock) {
      if (!this.mStarted) {
        if (paramLong >= 0L) {
          _$$Lambda$ScrollCaptureTargetResolver$EzFCJjUw_13D2CDqIgLoFuJ5qdY _$$Lambda$ScrollCaptureTargetResolver$EzFCJjUw_13D2CDqIgLoFuJ5qdY;
          Handler handler;
          this.mHandler = paramHandler;
          this.mTimeLimitMillis = paramLong;
          this.mWhenComplete = paramConsumer;
          if (this.mTargets.isEmpty()) {
            handler = this.mHandler;
            _$$Lambda$ScrollCaptureTargetResolver$EzFCJjUw_13D2CDqIgLoFuJ5qdY = new _$$Lambda$ScrollCaptureTargetResolver$EzFCJjUw_13D2CDqIgLoFuJ5qdY();
            this(this);
            handler.post(_$$Lambda$ScrollCaptureTargetResolver$EzFCJjUw_13D2CDqIgLoFuJ5qdY);
            return;
          } 
          this.mStarted = true;
          _$$Lambda$ScrollCaptureTargetResolver$yVQQiWyGWiKm9_d01s2pn78mS0k _$$Lambda$ScrollCaptureTargetResolver$yVQQiWyGWiKm9_d01s2pn78mS0k = new _$$Lambda$ScrollCaptureTargetResolver$yVQQiWyGWiKm9_d01s2pn78mS0k();
          this(this, paramLong, (Consumer)handler);
          _$$Lambda$ScrollCaptureTargetResolver$EzFCJjUw_13D2CDqIgLoFuJ5qdY.post(_$$Lambda$ScrollCaptureTargetResolver$yVQQiWyGWiKm9_d01s2pn78mS0k);
          return;
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("Time limit must be positive");
        throw illegalArgumentException;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("already started!");
      throw illegalStateException;
    } 
  }
  
  private void run(long paramLong, Consumer<ScrollCaptureTarget> paramConsumer) {
    checkThread();
    this.mPendingBoundsRequests = this.mTargets.size();
    for (ScrollCaptureTarget scrollCaptureTarget : this.mTargets)
      queryTarget(scrollCaptureTarget); 
    this.mDeadlineMillis = paramLong = SystemClock.elapsedRealtime() + this.mTimeLimitMillis;
    this.mHandler.postAtTime(this.mTimeoutRunnable, paramLong);
  }
  
  public ScrollCaptureTargetResolver(Queue<ScrollCaptureTarget> paramQueue) {
    this.mTimeoutRunnable = new Runnable() {
        final ScrollCaptureTargetResolver this$0;
        
        public void run() {
          ScrollCaptureTargetResolver.this.checkThread();
          ScrollCaptureTargetResolver.this.supplyResult(null);
        }
      };
    this.mTargets = paramQueue;
  }
  
  private void queryTarget(ScrollCaptureTarget paramScrollCaptureTarget) {
    checkThread();
    ScrollCaptureCallback scrollCaptureCallback = paramScrollCaptureTarget.getCallback();
    scrollCaptureCallback.onScrollCaptureSearch(new SingletonConsumer<>(new _$$Lambda$ScrollCaptureTargetResolver$J9O9ShCuuF3gTNBsMj2uaRnxEtQ(this, paramScrollCaptureTarget)));
  }
  
  private void onScrollBoundsProvided(ScrollCaptureTarget paramScrollCaptureTarget, Rect paramRect) {
    checkThread();
    if (this.mFinished)
      return; 
    int i = this.mPendingBoundsRequests;
    boolean bool = true;
    this.mPendingBoundsRequests = i - 1;
    this.mHandler.removeCallbacks(this.mTimeoutRunnable);
    if (this.mPendingBoundsRequests != 0 && 
      SystemClock.elapsedRealtime() < this.mDeadlineMillis)
      bool = false; 
    View view = paramScrollCaptureTarget.getContainingView();
    if (!nullOrEmpty(paramRect) && view.isAggregatedVisible()) {
      paramScrollCaptureTarget.updatePositionInWindow();
      paramScrollCaptureTarget.setScrollBounds(paramRect);
      supplyResult(paramScrollCaptureTarget);
    } 
    PrintStream printStream2 = System.err;
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("mPendingBoundsRequests: ");
    stringBuilder3.append(this.mPendingBoundsRequests);
    printStream2.println(stringBuilder3.toString());
    PrintStream printStream3 = System.err;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("mDeadlineMillis: ");
    stringBuilder1.append(this.mDeadlineMillis);
    printStream3.println(stringBuilder1.toString());
    PrintStream printStream1 = System.err;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("SystemClock.elapsedRealtime(): ");
    stringBuilder2.append(SystemClock.elapsedRealtime());
    printStream1.println(stringBuilder2.toString());
    if (!this.mFinished) {
      PrintStream printStream = System.err;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("We think we're NOT done yet and will check back at ");
      stringBuilder.append(this.mDeadlineMillis);
      printStream.println(stringBuilder.toString());
      this.mHandler.postAtTime(this.mTimeoutRunnable, this.mDeadlineMillis);
    } 
  }
  
  private static boolean hasIncludeHint(View paramView) {
    boolean bool;
    if ((paramView.getScrollCaptureHint() & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean isDescendant(View paramView1, View paramView2) {
    boolean bool = false;
    if (paramView1 == paramView2)
      return false; 
    ViewParent viewParent = paramView2.getParent();
    while (viewParent != paramView1 && viewParent != null)
      viewParent = viewParent.getParent(); 
    if (viewParent == paramView1)
      bool = true; 
    return bool;
  }
  
  private static int findRelation(View paramView1, View paramView2) {
    if (paramView1 == paramView2)
      return 0; 
    ViewParent viewParent1 = paramView1.getParent();
    ViewParent viewParent2 = paramView2.getParent();
    while (true) {
      if (viewParent1 != null || viewParent2 != null) {
        if (viewParent1 == viewParent2)
          return 0; 
        if (viewParent1 == paramView2)
          return 1; 
        if (viewParent2 == paramView1)
          return -1; 
        ViewParent viewParent = viewParent1;
        if (viewParent1 != null)
          viewParent = viewParent1.getParent(); 
        if (viewParent2 != null) {
          viewParent2 = viewParent2.getParent();
          viewParent1 = viewParent;
          continue;
        } 
        viewParent1 = viewParent;
        continue;
      } 
      return 0;
    } 
  }
  
  static class SingletonConsumer<T> implements Consumer<T> {
    final AtomicReference<Consumer<T>> mAtomicRef;
    
    SingletonConsumer(Consumer<T> param1Consumer) {
      this.mAtomicRef = new AtomicReference<>(param1Consumer);
    }
    
    public void accept(T param1T) {
      Consumer<T> consumer = this.mAtomicRef.getAndSet(null);
      if (consumer != null)
        consumer.accept(param1T); 
    }
  }
}
