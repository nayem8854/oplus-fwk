package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWindowId extends IInterface {
  boolean isFocused() throws RemoteException;
  
  void registerFocusObserver(IWindowFocusObserver paramIWindowFocusObserver) throws RemoteException;
  
  void unregisterFocusObserver(IWindowFocusObserver paramIWindowFocusObserver) throws RemoteException;
  
  class Default implements IWindowId {
    public void registerFocusObserver(IWindowFocusObserver param1IWindowFocusObserver) throws RemoteException {}
    
    public void unregisterFocusObserver(IWindowFocusObserver param1IWindowFocusObserver) throws RemoteException {}
    
    public boolean isFocused() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowId {
    private static final String DESCRIPTOR = "android.view.IWindowId";
    
    static final int TRANSACTION_isFocused = 3;
    
    static final int TRANSACTION_registerFocusObserver = 1;
    
    static final int TRANSACTION_unregisterFocusObserver = 2;
    
    public Stub() {
      attachInterface(this, "android.view.IWindowId");
    }
    
    public static IWindowId asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IWindowId");
      if (iInterface != null && iInterface instanceof IWindowId)
        return (IWindowId)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "isFocused";
        } 
        return "unregisterFocusObserver";
      } 
      return "registerFocusObserver";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.view.IWindowId");
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.IWindowId");
          boolean bool = isFocused();
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool);
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IWindowId");
        iWindowFocusObserver = IWindowFocusObserver.Stub.asInterface(param1Parcel1.readStrongBinder());
        unregisterFocusObserver(iWindowFocusObserver);
        param1Parcel2.writeNoException();
        return true;
      } 
      iWindowFocusObserver.enforceInterface("android.view.IWindowId");
      IWindowFocusObserver iWindowFocusObserver = IWindowFocusObserver.Stub.asInterface(iWindowFocusObserver.readStrongBinder());
      registerFocusObserver(iWindowFocusObserver);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IWindowId {
      public static IWindowId sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IWindowId";
      }
      
      public void registerFocusObserver(IWindowFocusObserver param2IWindowFocusObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowId");
          if (param2IWindowFocusObserver != null) {
            iBinder = param2IWindowFocusObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWindowId.Stub.getDefaultImpl() != null) {
            IWindowId.Stub.getDefaultImpl().registerFocusObserver(param2IWindowFocusObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterFocusObserver(IWindowFocusObserver param2IWindowFocusObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowId");
          if (param2IWindowFocusObserver != null) {
            iBinder = param2IWindowFocusObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IWindowId.Stub.getDefaultImpl() != null) {
            IWindowId.Stub.getDefaultImpl().unregisterFocusObserver(param2IWindowFocusObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isFocused() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowId");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IWindowId.Stub.getDefaultImpl() != null) {
            bool1 = IWindowId.Stub.getDefaultImpl().isFocused();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowId param1IWindowId) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowId != null) {
          Proxy.sDefaultImpl = param1IWindowId;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowId getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
