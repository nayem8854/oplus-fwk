package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWallpaperVisibilityListener extends IInterface {
  void onWallpaperVisibilityChanged(boolean paramBoolean, int paramInt) throws RemoteException;
  
  class Default implements IWallpaperVisibilityListener {
    public void onWallpaperVisibilityChanged(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWallpaperVisibilityListener {
    private static final String DESCRIPTOR = "android.view.IWallpaperVisibilityListener";
    
    static final int TRANSACTION_onWallpaperVisibilityChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IWallpaperVisibilityListener");
    }
    
    public static IWallpaperVisibilityListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IWallpaperVisibilityListener");
      if (iInterface != null && iInterface instanceof IWallpaperVisibilityListener)
        return (IWallpaperVisibilityListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onWallpaperVisibilityChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IWallpaperVisibilityListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IWallpaperVisibilityListener");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      param1Int1 = param1Parcel1.readInt();
      onWallpaperVisibilityChanged(bool, param1Int1);
      return true;
    }
    
    private static class Proxy implements IWallpaperVisibilityListener {
      public static IWallpaperVisibilityListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IWallpaperVisibilityListener";
      }
      
      public void onWallpaperVisibilityChanged(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWallpaperVisibilityListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IWallpaperVisibilityListener.Stub.getDefaultImpl() != null) {
            IWallpaperVisibilityListener.Stub.getDefaultImpl().onWallpaperVisibilityChanged(param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWallpaperVisibilityListener param1IWallpaperVisibilityListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWallpaperVisibilityListener != null) {
          Proxy.sDefaultImpl = param1IWallpaperVisibilityListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWallpaperVisibilityListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
