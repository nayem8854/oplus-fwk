package android.view;

import android.os.Looper;
import android.os.MessageQueue;
import android.util.Log;
import dalvik.annotation.optimization.FastNative;
import dalvik.system.CloseGuard;
import java.lang.ref.WeakReference;

public abstract class DisplayEventReceiver {
  public static final int CONFIG_CHANGED_EVENT_DISPATCH = 1;
  
  public static final int CONFIG_CHANGED_EVENT_SUPPRESS = 0;
  
  private static final String TAG = "DisplayEventReceiver";
  
  public static final int VSYNC_SOURCE_APP = 0;
  
  public static final int VSYNC_SOURCE_SURFACE_FLINGER = 1;
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private MessageQueue mMessageQueue;
  
  private long mReceiverPtr;
  
  public DisplayEventReceiver(Looper paramLooper) {
    this(paramLooper, 0, 0);
  }
  
  public DisplayEventReceiver(Looper paramLooper, int paramInt1, int paramInt2) {
    if (paramLooper != null) {
      this.mMessageQueue = paramLooper.getQueue();
      this.mReceiverPtr = nativeInit(new WeakReference<>(this), this.mMessageQueue, paramInt1, paramInt2);
      this.mCloseGuard.open("dispose");
      return;
    } 
    throw new IllegalArgumentException("looper must not be null");
  }
  
  protected void finalize() throws Throwable {
    try {
      dispose(true);
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void dispose() {
    dispose(false);
  }
  
  private void dispose(boolean paramBoolean) {
    CloseGuard closeGuard = this.mCloseGuard;
    if (closeGuard != null) {
      if (paramBoolean)
        closeGuard.warnIfOpen(); 
      this.mCloseGuard.close();
    } 
    long l = this.mReceiverPtr;
    if (l != 0L) {
      nativeDispose(l);
      this.mReceiverPtr = 0L;
    } 
    this.mMessageQueue = null;
  }
  
  public void onVsync(long paramLong1, long paramLong2, int paramInt) {}
  
  public void onHotplug(long paramLong1, long paramLong2, boolean paramBoolean) {}
  
  public void onConfigChanged(long paramLong1, long paramLong2, int paramInt) {}
  
  public void scheduleVsync() {
    long l = this.mReceiverPtr;
    if (l == 0L) {
      Log.w("DisplayEventReceiver", "Attempted to schedule a vertical sync pulse but the display event receiver has already been disposed.");
    } else {
      nativeScheduleVsync(l);
    } 
  }
  
  private void dispatchVsync(long paramLong1, long paramLong2, int paramInt) {
    onVsync(paramLong1, paramLong2, paramInt);
  }
  
  private void dispatchHotplug(long paramLong1, long paramLong2, boolean paramBoolean) {
    onHotplug(paramLong1, paramLong2, paramBoolean);
  }
  
  private void dispatchConfigChanged(long paramLong1, long paramLong2, int paramInt) {
    onConfigChanged(paramLong1, paramLong2, paramInt);
  }
  
  private static native void nativeDispose(long paramLong);
  
  private static native long nativeInit(WeakReference<DisplayEventReceiver> paramWeakReference, MessageQueue paramMessageQueue, int paramInt1, int paramInt2);
  
  @FastNative
  private static native void nativeScheduleVsync(long paramLong);
}
