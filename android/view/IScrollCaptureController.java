package android.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IScrollCaptureController extends IInterface {
  void onCaptureBufferSent(long paramLong, Rect paramRect) throws RemoteException;
  
  void onCaptureStarted() throws RemoteException;
  
  void onClientConnected(IScrollCaptureClient paramIScrollCaptureClient, Rect paramRect, Point paramPoint) throws RemoteException;
  
  void onClientUnavailable() throws RemoteException;
  
  void onConnectionClosed() throws RemoteException;
  
  class Default implements IScrollCaptureController {
    public void onClientConnected(IScrollCaptureClient param1IScrollCaptureClient, Rect param1Rect, Point param1Point) throws RemoteException {}
    
    public void onClientUnavailable() throws RemoteException {}
    
    public void onCaptureStarted() throws RemoteException {}
    
    public void onCaptureBufferSent(long param1Long, Rect param1Rect) throws RemoteException {}
    
    public void onConnectionClosed() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IScrollCaptureController {
    private static final String DESCRIPTOR = "android.view.IScrollCaptureController";
    
    static final int TRANSACTION_onCaptureBufferSent = 4;
    
    static final int TRANSACTION_onCaptureStarted = 3;
    
    static final int TRANSACTION_onClientConnected = 1;
    
    static final int TRANSACTION_onClientUnavailable = 2;
    
    static final int TRANSACTION_onConnectionClosed = 5;
    
    public Stub() {
      attachInterface(this, "android.view.IScrollCaptureController");
    }
    
    public static IScrollCaptureController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IScrollCaptureController");
      if (iInterface != null && iInterface instanceof IScrollCaptureController)
        return (IScrollCaptureController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onConnectionClosed";
            } 
            return "onCaptureBufferSent";
          } 
          return "onCaptureStarted";
        } 
        return "onClientUnavailable";
      } 
      return "onClientConnected";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.view.IScrollCaptureController");
                return true;
              } 
              param1Parcel1.enforceInterface("android.view.IScrollCaptureController");
              onConnectionClosed();
              return true;
            } 
            param1Parcel1.enforceInterface("android.view.IScrollCaptureController");
            long l = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onCaptureBufferSent(l, (Rect)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.IScrollCaptureController");
          onCaptureStarted();
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IScrollCaptureController");
        onClientUnavailable();
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IScrollCaptureController");
      IScrollCaptureClient iScrollCaptureClient = IScrollCaptureClient.Stub.asInterface(param1Parcel1.readStrongBinder());
      if (param1Parcel1.readInt() != 0) {
        Rect rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        Point point = (Point)Point.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onClientConnected(iScrollCaptureClient, (Rect)param1Parcel2, (Point)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IScrollCaptureController {
      public static IScrollCaptureController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IScrollCaptureController";
      }
      
      public void onClientConnected(IScrollCaptureClient param2IScrollCaptureClient, Rect param2Rect, Point param2Point) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IScrollCaptureController");
          if (param2IScrollCaptureClient != null) {
            iBinder = param2IScrollCaptureClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Point != null) {
            parcel.writeInt(1);
            param2Point.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IScrollCaptureController.Stub.getDefaultImpl() != null) {
            IScrollCaptureController.Stub.getDefaultImpl().onClientConnected(param2IScrollCaptureClient, param2Rect, param2Point);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onClientUnavailable() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IScrollCaptureController");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IScrollCaptureController.Stub.getDefaultImpl() != null) {
            IScrollCaptureController.Stub.getDefaultImpl().onClientUnavailable();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCaptureStarted() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IScrollCaptureController");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IScrollCaptureController.Stub.getDefaultImpl() != null) {
            IScrollCaptureController.Stub.getDefaultImpl().onCaptureStarted();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCaptureBufferSent(long param2Long, Rect param2Rect) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IScrollCaptureController");
          parcel.writeLong(param2Long);
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IScrollCaptureController.Stub.getDefaultImpl() != null) {
            IScrollCaptureController.Stub.getDefaultImpl().onCaptureBufferSent(param2Long, param2Rect);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConnectionClosed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IScrollCaptureController");
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IScrollCaptureController.Stub.getDefaultImpl() != null) {
            IScrollCaptureController.Stub.getDefaultImpl().onConnectionClosed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IScrollCaptureController param1IScrollCaptureController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IScrollCaptureController != null) {
          Proxy.sDefaultImpl = param1IScrollCaptureController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IScrollCaptureController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
