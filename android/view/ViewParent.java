package android.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Bundle;
import android.view.accessibility.AccessibilityEvent;

public interface ViewParent {
  default void onDescendantInvalidated(View paramView1, View paramView2) {
    if (getParent() != null)
      getParent().onDescendantInvalidated(paramView1, paramView2); 
  }
  
  default void subtractObscuredTouchableRegion(Region paramRegion, View paramView) {}
  
  default void onDescendantUnbufferedRequested() {
    if (getParent() != null)
      getParent().onDescendantUnbufferedRequested(); 
  }
  
  void bringChildToFront(View paramView);
  
  boolean canResolveLayoutDirection();
  
  boolean canResolveTextAlignment();
  
  boolean canResolveTextDirection();
  
  void childDrawableStateChanged(View paramView);
  
  void childHasTransientStateChanged(View paramView, boolean paramBoolean);
  
  void clearChildFocus(View paramView);
  
  void createContextMenu(ContextMenu paramContextMenu);
  
  View focusSearch(View paramView, int paramInt);
  
  void focusableViewAvailable(View paramView);
  
  boolean getChildVisibleRect(View paramView, Rect paramRect, Point paramPoint);
  
  int getLayoutDirection();
  
  ViewParent getParent();
  
  ViewParent getParentForAccessibility();
  
  int getTextAlignment();
  
  int getTextDirection();
  
  @Deprecated
  void invalidateChild(View paramView, Rect paramRect);
  
  @Deprecated
  ViewParent invalidateChildInParent(int[] paramArrayOfint, Rect paramRect);
  
  boolean isLayoutDirectionResolved();
  
  boolean isLayoutRequested();
  
  boolean isTextAlignmentResolved();
  
  boolean isTextDirectionResolved();
  
  View keyboardNavigationClusterSearch(View paramView, int paramInt);
  
  void notifySubtreeAccessibilityStateChanged(View paramView1, View paramView2, int paramInt);
  
  boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean);
  
  boolean onNestedPreFling(View paramView, float paramFloat1, float paramFloat2);
  
  boolean onNestedPrePerformAccessibilityAction(View paramView, int paramInt, Bundle paramBundle);
  
  void onNestedPreScroll(View paramView, int paramInt1, int paramInt2, int[] paramArrayOfint);
  
  void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt);
  
  boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt);
  
  void onStopNestedScroll(View paramView);
  
  void recomputeViewAttributes(View paramView);
  
  void requestChildFocus(View paramView1, View paramView2);
  
  boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean);
  
  void requestDisallowInterceptTouchEvent(boolean paramBoolean);
  
  void requestFitSystemWindows();
  
  void requestLayout();
  
  boolean requestSendAccessibilityEvent(View paramView, AccessibilityEvent paramAccessibilityEvent);
  
  void requestTransparentRegion(View paramView);
  
  boolean showContextMenuForChild(View paramView);
  
  boolean showContextMenuForChild(View paramView, float paramFloat1, float paramFloat2);
  
  ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback);
  
  ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback, int paramInt);
}
