package android.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.R;
import com.android.internal.util.XmlUtils;
import org.xmlpull.v1.XmlPullParser;

public final class PointerIcon implements Parcelable {
  public static final Parcelable.Creator<PointerIcon> CREATOR;
  
  private static final String TAG = "PointerIcon";
  
  public static final int TYPE_ALIAS = 1010;
  
  public static final int TYPE_ALL_SCROLL = 1013;
  
  public static final int TYPE_ARROW = 1000;
  
  public static final int TYPE_CELL = 1006;
  
  public static final int TYPE_CONTEXT_MENU = 1001;
  
  public static final int TYPE_COPY = 1011;
  
  public static final int TYPE_CROSSHAIR = 1007;
  
  public static final int TYPE_CUSTOM = -1;
  
  public static final int TYPE_DEFAULT = 1000;
  
  public static final int TYPE_GRAB = 1020;
  
  public static final int TYPE_GRABBING = 1021;
  
  public static final int TYPE_HAND = 1002;
  
  public static final int TYPE_HELP = 1003;
  
  public static final int TYPE_HORIZONTAL_DOUBLE_ARROW = 1014;
  
  public static final int TYPE_NOT_SPECIFIED = 1;
  
  public static final int TYPE_NO_DROP = 1012;
  
  public static final int TYPE_NULL = 0;
  
  private static final int TYPE_OEM_FIRST = 10000;
  
  public static final int TYPE_SPOT_ANCHOR = 2002;
  
  public static final int TYPE_SPOT_HOVER = 2000;
  
  public static final int TYPE_SPOT_TOUCH = 2001;
  
  public static final int TYPE_TEXT = 1008;
  
  public static final int TYPE_TOP_LEFT_DIAGONAL_DOUBLE_ARROW = 1017;
  
  public static final int TYPE_TOP_RIGHT_DIAGONAL_DOUBLE_ARROW = 1016;
  
  public static final int TYPE_VERTICAL_DOUBLE_ARROW = 1015;
  
  public static final int TYPE_VERTICAL_TEXT = 1009;
  
  public static final int TYPE_WAIT = 1004;
  
  public static final int TYPE_ZOOM_IN = 1018;
  
  public static final int TYPE_ZOOM_OUT = 1019;
  
  private static final PointerIcon gNullIcon = new PointerIcon(0);
  
  private static final SparseArray<SparseArray<PointerIcon>> gSystemIconsByDisplay = new SparseArray<>();
  
  private static DisplayManager.DisplayListener sDisplayListener;
  
  private static boolean sUseLargeIcons = false;
  
  private Bitmap mBitmap;
  
  private Bitmap[] mBitmapFrames;
  
  private int mDurationPerFrame;
  
  private float mHotSpotX;
  
  private float mHotSpotY;
  
  private int mSystemIconResourceId;
  
  private final int mType;
  
  private PointerIcon(int paramInt) {
    this.mType = paramInt;
  }
  
  public static PointerIcon getNullIcon() {
    return gNullIcon;
  }
  
  public static PointerIcon getDefaultIcon(Context paramContext) {
    return getSystemIcon(paramContext, 1000);
  }
  
  public static PointerIcon getSystemIcon(Context paramContext, int paramInt) {
    if (paramContext != null) {
      PointerIcon pointerIcon1;
      StringBuilder stringBuilder;
      if (paramInt == 0)
        return gNullIcon; 
      if (sDisplayListener == null)
        registerDisplayListener(paramContext); 
      int i = paramContext.getDisplayId();
      SparseArray<PointerIcon> sparseArray1 = gSystemIconsByDisplay.get(i);
      SparseArray<PointerIcon> sparseArray2 = sparseArray1;
      if (sparseArray1 == null) {
        sparseArray2 = new SparseArray();
        gSystemIconsByDisplay.put(i, sparseArray2);
      } 
      PointerIcon pointerIcon3 = sparseArray2.get(paramInt);
      if (pointerIcon3 != null)
        return pointerIcon3; 
      int j = getSystemIconTypeIndex(paramInt);
      i = j;
      if (j == 0)
        i = getSystemIconTypeIndex(1000); 
      if (sUseLargeIcons) {
        j = 16974639;
      } else {
        j = 16974647;
      } 
      TypedArray typedArray = paramContext.obtainStyledAttributes(null, R.styleable.Pointer, 0, j);
      i = typedArray.getResourceId(i, -1);
      typedArray.recycle();
      if (i == -1) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Missing theme resources for pointer icon type ");
        stringBuilder.append(paramInt);
        Log.w("PointerIcon", stringBuilder.toString());
        if (paramInt == 1000) {
          pointerIcon1 = gNullIcon;
        } else {
          pointerIcon1 = getSystemIcon((Context)pointerIcon1, 1000);
        } 
        return pointerIcon1;
      } 
      PointerIcon pointerIcon2 = new PointerIcon(paramInt);
      if ((0xFF000000 & i) == 16777216) {
        pointerIcon2.mSystemIconResourceId = i;
      } else {
        pointerIcon2.loadResource((Context)pointerIcon1, pointerIcon1.getResources(), i);
      } 
      stringBuilder.append(paramInt, pointerIcon2);
      return pointerIcon2;
    } 
    throw new IllegalArgumentException("context must not be null");
  }
  
  public static void setUseLargeIcons(boolean paramBoolean) {
    sUseLargeIcons = paramBoolean;
    gSystemIconsByDisplay.clear();
  }
  
  public static PointerIcon create(Bitmap paramBitmap, float paramFloat1, float paramFloat2) {
    if (paramBitmap != null) {
      validateHotSpot(paramBitmap, paramFloat1, paramFloat2);
      PointerIcon pointerIcon = new PointerIcon(-1);
      pointerIcon.mBitmap = paramBitmap;
      pointerIcon.mHotSpotX = paramFloat1;
      pointerIcon.mHotSpotY = paramFloat2;
      return pointerIcon;
    } 
    throw new IllegalArgumentException("bitmap must not be null");
  }
  
  public static PointerIcon load(Resources paramResources, int paramInt) {
    if (paramResources != null) {
      PointerIcon pointerIcon = new PointerIcon(-1);
      pointerIcon.loadResource(null, paramResources, paramInt);
      return pointerIcon;
    } 
    throw new IllegalArgumentException("resources must not be null");
  }
  
  public PointerIcon load(Context paramContext) {
    if (paramContext != null) {
      if (this.mSystemIconResourceId == 0 || this.mBitmap != null)
        return this; 
      PointerIcon pointerIcon = new PointerIcon(this.mType);
      pointerIcon.mSystemIconResourceId = this.mSystemIconResourceId;
      pointerIcon.loadResource(paramContext, paramContext.getResources(), this.mSystemIconResourceId);
      return pointerIcon;
    } 
    throw new IllegalArgumentException("context must not be null");
  }
  
  public int getType() {
    return this.mType;
  }
  
  static {
    CREATOR = new Parcelable.Creator<PointerIcon>() {
        public PointerIcon createFromParcel(Parcel param1Parcel) {
          PointerIcon pointerIcon;
          int i = param1Parcel.readInt();
          if (i == 0)
            return PointerIcon.getNullIcon(); 
          int j = param1Parcel.readInt();
          if (j != 0) {
            pointerIcon = new PointerIcon(i);
            PointerIcon.access$102(pointerIcon, j);
            return pointerIcon;
          } 
          Bitmap bitmap = (Bitmap)Bitmap.CREATOR.createFromParcel((Parcel)pointerIcon);
          float f1 = pointerIcon.readFloat();
          float f2 = pointerIcon.readFloat();
          return PointerIcon.create(bitmap, f1, f2);
        }
        
        public PointerIcon[] newArray(int param1Int) {
          return new PointerIcon[param1Int];
        }
      };
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    if (this.mType != 0) {
      paramParcel.writeInt(this.mSystemIconResourceId);
      if (this.mSystemIconResourceId == 0) {
        this.mBitmap.writeToParcel(paramParcel, paramInt);
        paramParcel.writeFloat(this.mHotSpotX);
        paramParcel.writeFloat(this.mHotSpotY);
      } 
    } 
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || !(paramObject instanceof PointerIcon))
      return false; 
    paramObject = paramObject;
    if (this.mType == ((PointerIcon)paramObject).mType) {
      int i = this.mSystemIconResourceId;
      if (i == ((PointerIcon)paramObject).mSystemIconResourceId) {
        if (i == 0 && (this.mBitmap != ((PointerIcon)paramObject).mBitmap || this.mHotSpotX != ((PointerIcon)paramObject).mHotSpotX || this.mHotSpotY != ((PointerIcon)paramObject).mHotSpotY))
          return false; 
        return true;
      } 
    } 
    return false;
  }
  
  private Bitmap getBitmapFromDrawable(BitmapDrawable paramBitmapDrawable) {
    Bitmap bitmap1 = paramBitmapDrawable.getBitmap();
    int i = paramBitmapDrawable.getIntrinsicWidth();
    int j = paramBitmapDrawable.getIntrinsicHeight();
    if (i == bitmap1.getWidth() && j == bitmap1.getHeight())
      return bitmap1; 
    Rect rect = new Rect(0, 0, bitmap1.getWidth(), bitmap1.getHeight());
    RectF rectF = new RectF(0.0F, 0.0F, i, j);
    Bitmap bitmap2 = Bitmap.createBitmap(i, j, bitmap1.getConfig());
    Canvas canvas = new Canvas(bitmap2);
    Paint paint = new Paint();
    paint.setFilterBitmap(true);
    canvas.drawBitmap(bitmap1, rect, rectF, paint);
    return bitmap2;
  }
  
  private void loadResource(Context paramContext, Resources paramResources, int paramInt) {
    BitmapDrawable bitmapDrawable;
    XmlResourceParser xmlResourceParser = paramResources.getXml(paramInt);
    try {
      XmlUtils.beginDocument((XmlPullParser)xmlResourceParser, "pointer-icon");
      TypedArray typedArray = paramResources.obtainAttributes((AttributeSet)xmlResourceParser, R.styleable.PointerIcon);
      paramInt = typedArray.getResourceId(0, 0);
      float f1 = typedArray.getDimension(1, 0.0F);
      float f2 = typedArray.getDimension(2, 0.0F);
      typedArray.recycle();
      xmlResourceParser.close();
      if (paramInt != 0) {
        Drawable drawable1;
        if (paramContext == null) {
          drawable1 = paramResources.getDrawable(paramInt);
        } else {
          drawable1 = drawable1.getDrawable(paramInt);
        } 
        Drawable drawable2 = drawable1;
        if (drawable1 instanceof AnimationDrawable) {
          AnimationDrawable animationDrawable = (AnimationDrawable)drawable1;
          int i = animationDrawable.getNumberOfFrames();
          drawable2 = animationDrawable.getFrame(0);
          if (i == 1) {
            Log.w("PointerIcon", "Animation icon with single frame -- simply treating the first frame as a normal bitmap icon.");
          } else {
            this.mDurationPerFrame = animationDrawable.getDuration(0);
            this.mBitmapFrames = new Bitmap[i - 1];
            int j = drawable2.getIntrinsicWidth();
            int k = drawable2.getIntrinsicHeight();
            for (paramInt = 1; paramInt < i; ) {
              Drawable drawable = animationDrawable.getFrame(paramInt);
              if (drawable instanceof BitmapDrawable) {
                if (drawable.getIntrinsicWidth() == j && 
                  drawable.getIntrinsicHeight() == k) {
                  bitmapDrawable = (BitmapDrawable)drawable;
                  this.mBitmapFrames[paramInt - 1] = getBitmapFromDrawable(bitmapDrawable);
                  paramInt++;
                } 
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("The bitmap size of ");
                stringBuilder.append(paramInt);
                stringBuilder.append("-th frame is different. All frames should have the exact same size and share the same hotspot.");
                throw new IllegalArgumentException(stringBuilder.toString());
              } 
              throw new IllegalArgumentException("Frame of an animated pointer icon must refer to a bitmap drawable.");
            } 
          } 
        } 
        if (drawable2 instanceof BitmapDrawable) {
          BitmapDrawable bitmapDrawable1 = (BitmapDrawable)drawable2;
          Bitmap bitmap = getBitmapFromDrawable(bitmapDrawable1);
          validateHotSpot(bitmap, f1, f2);
          this.mBitmap = bitmap;
          this.mHotSpotX = f1;
          this.mHotSpotY = f2;
          return;
        } 
        throw new IllegalArgumentException("<pointer-icon> bitmap attribute must refer to a bitmap drawable.");
      } 
      throw new IllegalArgumentException("<pointer-icon> is missing bitmap attribute.");
    } catch (Exception exception) {
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("Exception parsing pointer icon resource.", exception);
      throw illegalArgumentException;
    } finally {}
    bitmapDrawable.close();
    throw paramContext;
  }
  
  private static void validateHotSpot(Bitmap paramBitmap, float paramFloat1, float paramFloat2) {
    if (paramFloat1 >= 0.0F && paramFloat1 < paramBitmap.getWidth()) {
      if (paramFloat2 >= 0.0F && paramFloat2 < paramBitmap.getHeight())
        return; 
      throw new IllegalArgumentException("y hotspot lies outside of the bitmap area");
    } 
    throw new IllegalArgumentException("x hotspot lies outside of the bitmap area");
  }
  
  private static int getSystemIconTypeIndex(int paramInt) {
    switch (paramInt) {
      default:
        switch (paramInt) {
          default:
            switch (paramInt) {
              default:
                switch (paramInt) {
                  default:
                    return 0;
                  case 2002:
                    return 13;
                  case 2001:
                    return 15;
                  case 2000:
                    break;
                } 
                return 14;
              case 1021:
                return 8;
              case 1020:
                return 7;
              case 1019:
                return 23;
              case 1018:
                return 22;
              case 1017:
                return 17;
              case 1016:
                return 18;
              case 1015:
                return 19;
              case 1014:
                return 11;
              case 1013:
                return 1;
              case 1012:
                return 12;
              case 1011:
                break;
            } 
            return 5;
          case 1009:
            return 20;
          case 1008:
            return 16;
          case 1007:
            return 6;
          case 1006:
            break;
        } 
        return 3;
      case 1004:
        return 21;
      case 1003:
        return 10;
      case 1002:
        return 9;
      case 1001:
        return 4;
      case 1000:
        break;
    } 
    return 2;
  }
  
  private static void registerDisplayListener(Context paramContext) {
    sDisplayListener = new DisplayManager.DisplayListener() {
        public void onDisplayAdded(int param1Int) {}
        
        public void onDisplayRemoved(int param1Int) {
          PointerIcon.gSystemIconsByDisplay.remove(param1Int);
        }
        
        public void onDisplayChanged(int param1Int) {
          PointerIcon.gSystemIconsByDisplay.remove(param1Int);
        }
      };
    DisplayManager displayManager = (DisplayManager)paramContext.getSystemService(DisplayManager.class);
    displayManager.registerDisplayListener(sDisplayListener, null);
  }
}
