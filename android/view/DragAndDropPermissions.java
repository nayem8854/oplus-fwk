package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.android.internal.view.IDragAndDropPermissions;

public final class DragAndDropPermissions implements Parcelable {
  public static DragAndDropPermissions obtain(DragEvent paramDragEvent) {
    if (paramDragEvent.getDragAndDropPermissions() == null)
      return null; 
    return new DragAndDropPermissions(paramDragEvent.getDragAndDropPermissions());
  }
  
  private DragAndDropPermissions(IDragAndDropPermissions paramIDragAndDropPermissions) {
    this.mDragAndDropPermissions = paramIDragAndDropPermissions;
  }
  
  public boolean take(IBinder paramIBinder) {
    try {
      this.mDragAndDropPermissions.take(paramIBinder);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean takeTransient() {
    try {
      Binder binder = new Binder();
      this();
      this.mTransientToken = (IBinder)binder;
      this.mDragAndDropPermissions.takeTransient((IBinder)binder);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void release() {
    try {
      this.mDragAndDropPermissions.release();
      this.mTransientToken = null;
    } catch (RemoteException remoteException) {}
  }
  
  public static final Parcelable.Creator<DragAndDropPermissions> CREATOR = new Parcelable.Creator<DragAndDropPermissions>() {
      public DragAndDropPermissions createFromParcel(Parcel param1Parcel) {
        return new DragAndDropPermissions(param1Parcel);
      }
      
      public DragAndDropPermissions[] newArray(int param1Int) {
        return new DragAndDropPermissions[param1Int];
      }
    };
  
  private final IDragAndDropPermissions mDragAndDropPermissions;
  
  private IBinder mTransientToken;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongInterface((IInterface)this.mDragAndDropPermissions);
    paramParcel.writeStrongBinder(this.mTransientToken);
  }
  
  private DragAndDropPermissions(Parcel paramParcel) {
    this.mDragAndDropPermissions = IDragAndDropPermissions.Stub.asInterface(paramParcel.readStrongBinder());
    this.mTransientToken = paramParcel.readStrongBinder();
  }
}
