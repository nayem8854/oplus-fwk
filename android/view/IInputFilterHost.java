package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IInputFilterHost extends IInterface {
  void sendInputEvent(InputEvent paramInputEvent, int paramInt) throws RemoteException;
  
  class Default implements IInputFilterHost {
    public void sendInputEvent(InputEvent param1InputEvent, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputFilterHost {
    private static final String DESCRIPTOR = "android.view.IInputFilterHost";
    
    static final int TRANSACTION_sendInputEvent = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IInputFilterHost");
    }
    
    public static IInputFilterHost asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IInputFilterHost");
      if (iInterface != null && iInterface instanceof IInputFilterHost)
        return (IInputFilterHost)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "sendInputEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IInputFilterHost");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IInputFilterHost");
      if (param1Parcel1.readInt() != 0) {
        InputEvent inputEvent = (InputEvent)InputEvent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = param1Parcel1.readInt();
      sendInputEvent((InputEvent)param1Parcel2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IInputFilterHost {
      public static IInputFilterHost sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IInputFilterHost";
      }
      
      public void sendInputEvent(InputEvent param2InputEvent, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IInputFilterHost");
          if (param2InputEvent != null) {
            parcel.writeInt(1);
            param2InputEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInputFilterHost.Stub.getDefaultImpl() != null) {
            IInputFilterHost.Stub.getDefaultImpl().sendInputEvent(param2InputEvent, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputFilterHost param1IInputFilterHost) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputFilterHost != null) {
          Proxy.sDefaultImpl = param1IInputFilterHost;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputFilterHost getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
