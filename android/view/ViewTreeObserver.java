package android.view;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.Region;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public final class ViewTreeObserver {
  private static boolean sIllegalOnDrawModificationIsFatal;
  
  private boolean mAlive;
  
  private CopyOnWriteArray<Consumer<List<Rect>>> mGestureExclusionListeners;
  
  private boolean mInDispatchOnDraw;
  
  private CopyOnWriteArray<OnComputeInternalInsetsListener> mOnComputeInternalInsetsListeners;
  
  private ArrayList<OnDrawListener> mOnDrawListeners;
  
  private CopyOnWriteArrayList<OnEnterAnimationCompleteListener> mOnEnterAnimationCompleteListeners;
  
  private ArrayList<Runnable> mOnFrameCommitListeners;
  
  private CopyOnWriteArrayList<OnGlobalFocusChangeListener> mOnGlobalFocusListeners;
  
  private CopyOnWriteArray<OnGlobalLayoutListener> mOnGlobalLayoutListeners;
  
  private CopyOnWriteArray<OnPreDrawListener> mOnPreDrawListeners;
  
  private CopyOnWriteArray<OnScrollChangedListener> mOnScrollChangedListeners;
  
  private CopyOnWriteArrayList<OnTouchModeChangeListener> mOnTouchModeChangeListeners;
  
  private CopyOnWriteArrayList<OnWindowAttachListener> mOnWindowAttachListeners;
  
  private CopyOnWriteArrayList<OnWindowFocusChangeListener> mOnWindowFocusListeners;
  
  private CopyOnWriteArray<OnWindowShownListener> mOnWindowShownListeners;
  
  private boolean mWindowShown;
  
  public static interface OnWindowShownListener {
    void onWindowShown();
  }
  
  public static interface OnWindowFocusChangeListener {
    void onWindowFocusChanged(boolean param1Boolean);
  }
  
  public static interface OnWindowAttachListener {
    void onWindowAttached();
    
    void onWindowDetached();
  }
  
  public static interface OnTouchModeChangeListener {
    void onTouchModeChanged(boolean param1Boolean);
  }
  
  class OnScrollChangedListener {
    public abstract void onScrollChanged();
  }
  
  class OnPreDrawListener {
    public abstract boolean onPreDraw();
  }
  
  class OnGlobalLayoutListener {
    public abstract void onGlobalLayout();
  }
  
  class OnGlobalFocusChangeListener {
    public abstract void onGlobalFocusChanged(View param1View1, View param1View2);
  }
  
  class OnEnterAnimationCompleteListener {
    public abstract void onEnterAnimationComplete();
  }
  
  class OnDrawListener {
    public abstract void onDraw();
  }
  
  class OnComputeInternalInsetsListener {
    public abstract void onComputeInternalInsets(ViewTreeObserver.InternalInsetsInfo param1InternalInsetsInfo);
  }
  
  class InternalInsetsInfo {
    public final Rect contentInsets = new Rect();
    
    public final Rect visibleInsets = new Rect();
    
    public final Region touchableRegion = new Region();
    
    public static final int TOUCHABLE_INSETS_CONTENT = 1;
    
    public static final int TOUCHABLE_INSETS_FRAME = 0;
    
    public static final int TOUCHABLE_INSETS_REGION = 3;
    
    public static final int TOUCHABLE_INSETS_VISIBLE = 2;
    
    int mTouchableInsets;
    
    public void setTouchableInsets(int param1Int) {
      this.mTouchableInsets = param1Int;
    }
    
    void reset() {
      this.contentInsets.setEmpty();
      this.visibleInsets.setEmpty();
      this.touchableRegion.setEmpty();
      this.mTouchableInsets = 0;
    }
    
    boolean isEmpty() {
      if (this.contentInsets.isEmpty()) {
        Rect rect = this.visibleInsets;
        if (rect.isEmpty()) {
          Region region = this.touchableRegion;
          if (region.isEmpty() && this.mTouchableInsets == 0)
            return true; 
        } 
      } 
      return false;
    }
    
    public int hashCode() {
      int i = this.contentInsets.hashCode();
      int j = this.visibleInsets.hashCode();
      int k = this.touchableRegion.hashCode();
      int m = this.mTouchableInsets;
      return ((i * 31 + j) * 31 + k) * 31 + m;
    }
    
    public boolean equals(Object param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mTouchableInsets == ((InternalInsetsInfo)param1Object).mTouchableInsets) {
        Rect rect1 = this.contentInsets, rect2 = ((InternalInsetsInfo)param1Object).contentInsets;
        if (rect1.equals(rect2)) {
          rect1 = this.visibleInsets;
          rect2 = ((InternalInsetsInfo)param1Object).visibleInsets;
          if (rect1.equals(rect2)) {
            Region region = this.touchableRegion;
            param1Object = ((InternalInsetsInfo)param1Object).touchableRegion;
            if (region.equals(param1Object))
              return null; 
          } 
        } 
      } 
      return false;
    }
    
    void set(InternalInsetsInfo param1InternalInsetsInfo) {
      this.contentInsets.set(param1InternalInsetsInfo.contentInsets);
      this.visibleInsets.set(param1InternalInsetsInfo.visibleInsets);
      this.touchableRegion.set(param1InternalInsetsInfo.touchableRegion);
      this.mTouchableInsets = param1InternalInsetsInfo.mTouchableInsets;
    }
  }
  
  ViewTreeObserver(Context paramContext) {
    boolean bool = true;
    this.mAlive = true;
    if ((paramContext.getApplicationInfo()).targetSdkVersion < 26)
      bool = false; 
    sIllegalOnDrawModificationIsFatal = bool;
  }
  
  void merge(ViewTreeObserver paramViewTreeObserver) {
    CopyOnWriteArrayList<OnWindowAttachListener> copyOnWriteArrayList2 = paramViewTreeObserver.mOnWindowAttachListeners;
    if (copyOnWriteArrayList2 != null) {
      CopyOnWriteArrayList<OnWindowAttachListener> copyOnWriteArrayList4 = this.mOnWindowAttachListeners;
      if (copyOnWriteArrayList4 != null) {
        copyOnWriteArrayList4.addAll(copyOnWriteArrayList2);
      } else {
        this.mOnWindowAttachListeners = copyOnWriteArrayList2;
      } 
    } 
    CopyOnWriteArrayList<OnWindowFocusChangeListener> copyOnWriteArrayList1 = paramViewTreeObserver.mOnWindowFocusListeners;
    if (copyOnWriteArrayList1 != null) {
      CopyOnWriteArrayList<OnWindowFocusChangeListener> copyOnWriteArrayList4 = this.mOnWindowFocusListeners;
      if (copyOnWriteArrayList4 != null) {
        copyOnWriteArrayList4.addAll(copyOnWriteArrayList1);
      } else {
        this.mOnWindowFocusListeners = copyOnWriteArrayList1;
      } 
    } 
    CopyOnWriteArrayList<OnGlobalFocusChangeListener> copyOnWriteArrayList3 = paramViewTreeObserver.mOnGlobalFocusListeners;
    if (copyOnWriteArrayList3 != null) {
      CopyOnWriteArrayList<OnGlobalFocusChangeListener> copyOnWriteArrayList4 = this.mOnGlobalFocusListeners;
      if (copyOnWriteArrayList4 != null) {
        copyOnWriteArrayList4.addAll(copyOnWriteArrayList3);
      } else {
        this.mOnGlobalFocusListeners = copyOnWriteArrayList3;
      } 
    } 
    CopyOnWriteArray<OnGlobalLayoutListener> copyOnWriteArray3 = paramViewTreeObserver.mOnGlobalLayoutListeners;
    if (copyOnWriteArray3 != null) {
      CopyOnWriteArray<OnGlobalLayoutListener> copyOnWriteArray6 = this.mOnGlobalLayoutListeners;
      if (copyOnWriteArray6 != null) {
        copyOnWriteArray6.addAll(copyOnWriteArray3);
      } else {
        this.mOnGlobalLayoutListeners = copyOnWriteArray3;
      } 
    } 
    CopyOnWriteArray<OnPreDrawListener> copyOnWriteArray2 = paramViewTreeObserver.mOnPreDrawListeners;
    if (copyOnWriteArray2 != null) {
      CopyOnWriteArray<OnPreDrawListener> copyOnWriteArray6 = this.mOnPreDrawListeners;
      if (copyOnWriteArray6 != null) {
        copyOnWriteArray6.addAll(copyOnWriteArray2);
      } else {
        this.mOnPreDrawListeners = copyOnWriteArray2;
      } 
    } 
    ArrayList<OnDrawListener> arrayList = paramViewTreeObserver.mOnDrawListeners;
    if (arrayList != null) {
      ArrayList<OnDrawListener> arrayList1 = this.mOnDrawListeners;
      if (arrayList1 != null) {
        arrayList1.addAll(arrayList);
      } else {
        this.mOnDrawListeners = arrayList;
      } 
    } 
    if (paramViewTreeObserver.mOnFrameCommitListeners != null) {
      ArrayList<Runnable> arrayList1 = this.mOnFrameCommitListeners;
      if (arrayList1 != null) {
        arrayList1.addAll(paramViewTreeObserver.captureFrameCommitCallbacks());
      } else {
        this.mOnFrameCommitListeners = paramViewTreeObserver.captureFrameCommitCallbacks();
      } 
    } 
    CopyOnWriteArrayList<OnTouchModeChangeListener> copyOnWriteArrayList = paramViewTreeObserver.mOnTouchModeChangeListeners;
    if (copyOnWriteArrayList != null) {
      CopyOnWriteArrayList<OnTouchModeChangeListener> copyOnWriteArrayList4 = this.mOnTouchModeChangeListeners;
      if (copyOnWriteArrayList4 != null) {
        copyOnWriteArrayList4.addAll(copyOnWriteArrayList);
      } else {
        this.mOnTouchModeChangeListeners = copyOnWriteArrayList;
      } 
    } 
    CopyOnWriteArray<OnComputeInternalInsetsListener> copyOnWriteArray1 = paramViewTreeObserver.mOnComputeInternalInsetsListeners;
    if (copyOnWriteArray1 != null) {
      CopyOnWriteArray<OnComputeInternalInsetsListener> copyOnWriteArray6 = this.mOnComputeInternalInsetsListeners;
      if (copyOnWriteArray6 != null) {
        copyOnWriteArray6.addAll(copyOnWriteArray1);
      } else {
        this.mOnComputeInternalInsetsListeners = copyOnWriteArray1;
      } 
    } 
    CopyOnWriteArray<OnScrollChangedListener> copyOnWriteArray5 = paramViewTreeObserver.mOnScrollChangedListeners;
    if (copyOnWriteArray5 != null) {
      CopyOnWriteArray<OnScrollChangedListener> copyOnWriteArray6 = this.mOnScrollChangedListeners;
      if (copyOnWriteArray6 != null) {
        copyOnWriteArray6.addAll(copyOnWriteArray5);
      } else {
        this.mOnScrollChangedListeners = copyOnWriteArray5;
      } 
    } 
    CopyOnWriteArray<OnWindowShownListener> copyOnWriteArray = paramViewTreeObserver.mOnWindowShownListeners;
    if (copyOnWriteArray != null) {
      CopyOnWriteArray<OnWindowShownListener> copyOnWriteArray6 = this.mOnWindowShownListeners;
      if (copyOnWriteArray6 != null) {
        copyOnWriteArray6.addAll(copyOnWriteArray);
      } else {
        this.mOnWindowShownListeners = copyOnWriteArray;
      } 
    } 
    CopyOnWriteArray<Consumer<List<Rect>>> copyOnWriteArray4 = paramViewTreeObserver.mGestureExclusionListeners;
    if (copyOnWriteArray4 != null) {
      CopyOnWriteArray<Consumer<List<Rect>>> copyOnWriteArray6 = this.mGestureExclusionListeners;
      if (copyOnWriteArray6 != null) {
        copyOnWriteArray6.addAll(copyOnWriteArray4);
      } else {
        this.mGestureExclusionListeners = copyOnWriteArray4;
      } 
    } 
    paramViewTreeObserver.kill();
  }
  
  public void addOnWindowAttachListener(OnWindowAttachListener paramOnWindowAttachListener) {
    checkIsAlive();
    if (this.mOnWindowAttachListeners == null)
      this.mOnWindowAttachListeners = new CopyOnWriteArrayList<>(); 
    this.mOnWindowAttachListeners.add(paramOnWindowAttachListener);
  }
  
  public void removeOnWindowAttachListener(OnWindowAttachListener paramOnWindowAttachListener) {
    checkIsAlive();
    CopyOnWriteArrayList<OnWindowAttachListener> copyOnWriteArrayList = this.mOnWindowAttachListeners;
    if (copyOnWriteArrayList == null)
      return; 
    copyOnWriteArrayList.remove(paramOnWindowAttachListener);
  }
  
  public void addOnWindowFocusChangeListener(OnWindowFocusChangeListener paramOnWindowFocusChangeListener) {
    checkIsAlive();
    if (this.mOnWindowFocusListeners == null)
      this.mOnWindowFocusListeners = new CopyOnWriteArrayList<>(); 
    this.mOnWindowFocusListeners.add(paramOnWindowFocusChangeListener);
  }
  
  public void removeOnWindowFocusChangeListener(OnWindowFocusChangeListener paramOnWindowFocusChangeListener) {
    checkIsAlive();
    CopyOnWriteArrayList<OnWindowFocusChangeListener> copyOnWriteArrayList = this.mOnWindowFocusListeners;
    if (copyOnWriteArrayList == null)
      return; 
    copyOnWriteArrayList.remove(paramOnWindowFocusChangeListener);
  }
  
  public void addOnGlobalFocusChangeListener(OnGlobalFocusChangeListener paramOnGlobalFocusChangeListener) {
    checkIsAlive();
    if (this.mOnGlobalFocusListeners == null)
      this.mOnGlobalFocusListeners = new CopyOnWriteArrayList<>(); 
    this.mOnGlobalFocusListeners.add(paramOnGlobalFocusChangeListener);
  }
  
  public void removeOnGlobalFocusChangeListener(OnGlobalFocusChangeListener paramOnGlobalFocusChangeListener) {
    checkIsAlive();
    CopyOnWriteArrayList<OnGlobalFocusChangeListener> copyOnWriteArrayList = this.mOnGlobalFocusListeners;
    if (copyOnWriteArrayList == null)
      return; 
    copyOnWriteArrayList.remove(paramOnGlobalFocusChangeListener);
  }
  
  public void addOnGlobalLayoutListener(OnGlobalLayoutListener paramOnGlobalLayoutListener) {
    checkIsAlive();
    if (this.mOnGlobalLayoutListeners == null)
      this.mOnGlobalLayoutListeners = new CopyOnWriteArray<>(); 
    this.mOnGlobalLayoutListeners.add(paramOnGlobalLayoutListener);
  }
  
  @Deprecated
  public void removeGlobalOnLayoutListener(OnGlobalLayoutListener paramOnGlobalLayoutListener) {
    removeOnGlobalLayoutListener(paramOnGlobalLayoutListener);
  }
  
  public void removeOnGlobalLayoutListener(OnGlobalLayoutListener paramOnGlobalLayoutListener) {
    checkIsAlive();
    CopyOnWriteArray<OnGlobalLayoutListener> copyOnWriteArray = this.mOnGlobalLayoutListeners;
    if (copyOnWriteArray == null)
      return; 
    copyOnWriteArray.remove(paramOnGlobalLayoutListener);
  }
  
  public void addOnPreDrawListener(OnPreDrawListener paramOnPreDrawListener) {
    checkIsAlive();
    if (this.mOnPreDrawListeners == null)
      this.mOnPreDrawListeners = new CopyOnWriteArray<>(); 
    this.mOnPreDrawListeners.add(paramOnPreDrawListener);
  }
  
  public void removeOnPreDrawListener(OnPreDrawListener paramOnPreDrawListener) {
    checkIsAlive();
    CopyOnWriteArray<OnPreDrawListener> copyOnWriteArray = this.mOnPreDrawListeners;
    if (copyOnWriteArray == null)
      return; 
    copyOnWriteArray.remove(paramOnPreDrawListener);
  }
  
  public void addOnWindowShownListener(OnWindowShownListener paramOnWindowShownListener) {
    checkIsAlive();
    if (this.mOnWindowShownListeners == null)
      this.mOnWindowShownListeners = new CopyOnWriteArray<>(); 
    this.mOnWindowShownListeners.add(paramOnWindowShownListener);
    if (this.mWindowShown)
      paramOnWindowShownListener.onWindowShown(); 
  }
  
  public void removeOnWindowShownListener(OnWindowShownListener paramOnWindowShownListener) {
    checkIsAlive();
    CopyOnWriteArray<OnWindowShownListener> copyOnWriteArray = this.mOnWindowShownListeners;
    if (copyOnWriteArray == null)
      return; 
    copyOnWriteArray.remove(paramOnWindowShownListener);
  }
  
  public void addOnDrawListener(OnDrawListener paramOnDrawListener) {
    checkIsAlive();
    if (this.mOnDrawListeners == null)
      this.mOnDrawListeners = new ArrayList<>(); 
    if (this.mInDispatchOnDraw) {
      IllegalStateException illegalStateException = new IllegalStateException("Cannot call addOnDrawListener inside of onDraw");
      if (!sIllegalOnDrawModificationIsFatal) {
        Log.e("ViewTreeObserver", illegalStateException.getMessage(), illegalStateException);
      } else {
        throw illegalStateException;
      } 
    } 
    this.mOnDrawListeners.add(paramOnDrawListener);
  }
  
  public void removeOnDrawListener(OnDrawListener paramOnDrawListener) {
    checkIsAlive();
    if (this.mOnDrawListeners == null)
      return; 
    if (this.mInDispatchOnDraw) {
      IllegalStateException illegalStateException = new IllegalStateException("Cannot call removeOnDrawListener inside of onDraw");
      if (!sIllegalOnDrawModificationIsFatal) {
        Log.e("ViewTreeObserver", illegalStateException.getMessage(), illegalStateException);
      } else {
        throw illegalStateException;
      } 
    } 
    this.mOnDrawListeners.remove(paramOnDrawListener);
  }
  
  public void registerFrameCommitCallback(Runnable paramRunnable) {
    checkIsAlive();
    if (this.mOnFrameCommitListeners == null)
      this.mOnFrameCommitListeners = new ArrayList<>(); 
    this.mOnFrameCommitListeners.add(paramRunnable);
  }
  
  ArrayList<Runnable> captureFrameCommitCallbacks() {
    ArrayList<Runnable> arrayList = this.mOnFrameCommitListeners;
    this.mOnFrameCommitListeners = null;
    return arrayList;
  }
  
  public boolean unregisterFrameCommitCallback(Runnable paramRunnable) {
    checkIsAlive();
    ArrayList<Runnable> arrayList = this.mOnFrameCommitListeners;
    if (arrayList == null)
      return false; 
    return arrayList.remove(paramRunnable);
  }
  
  public void addOnScrollChangedListener(OnScrollChangedListener paramOnScrollChangedListener) {
    checkIsAlive();
    if (this.mOnScrollChangedListeners == null)
      this.mOnScrollChangedListeners = new CopyOnWriteArray<>(); 
    this.mOnScrollChangedListeners.add(paramOnScrollChangedListener);
  }
  
  public void removeOnScrollChangedListener(OnScrollChangedListener paramOnScrollChangedListener) {
    checkIsAlive();
    CopyOnWriteArray<OnScrollChangedListener> copyOnWriteArray = this.mOnScrollChangedListeners;
    if (copyOnWriteArray == null)
      return; 
    copyOnWriteArray.remove(paramOnScrollChangedListener);
  }
  
  public void addOnTouchModeChangeListener(OnTouchModeChangeListener paramOnTouchModeChangeListener) {
    checkIsAlive();
    if (this.mOnTouchModeChangeListeners == null)
      this.mOnTouchModeChangeListeners = new CopyOnWriteArrayList<>(); 
    this.mOnTouchModeChangeListeners.add(paramOnTouchModeChangeListener);
  }
  
  public void removeOnTouchModeChangeListener(OnTouchModeChangeListener paramOnTouchModeChangeListener) {
    checkIsAlive();
    CopyOnWriteArrayList<OnTouchModeChangeListener> copyOnWriteArrayList = this.mOnTouchModeChangeListeners;
    if (copyOnWriteArrayList == null)
      return; 
    copyOnWriteArrayList.remove(paramOnTouchModeChangeListener);
  }
  
  public void addOnComputeInternalInsetsListener(OnComputeInternalInsetsListener paramOnComputeInternalInsetsListener) {
    checkIsAlive();
    if (this.mOnComputeInternalInsetsListeners == null)
      this.mOnComputeInternalInsetsListeners = new CopyOnWriteArray<>(); 
    this.mOnComputeInternalInsetsListeners.add(paramOnComputeInternalInsetsListener);
  }
  
  public void removeOnComputeInternalInsetsListener(OnComputeInternalInsetsListener paramOnComputeInternalInsetsListener) {
    checkIsAlive();
    CopyOnWriteArray<OnComputeInternalInsetsListener> copyOnWriteArray = this.mOnComputeInternalInsetsListeners;
    if (copyOnWriteArray == null)
      return; 
    copyOnWriteArray.remove(paramOnComputeInternalInsetsListener);
  }
  
  public void addOnEnterAnimationCompleteListener(OnEnterAnimationCompleteListener paramOnEnterAnimationCompleteListener) {
    checkIsAlive();
    if (this.mOnEnterAnimationCompleteListeners == null)
      this.mOnEnterAnimationCompleteListeners = new CopyOnWriteArrayList<>(); 
    this.mOnEnterAnimationCompleteListeners.add(paramOnEnterAnimationCompleteListener);
  }
  
  public void removeOnEnterAnimationCompleteListener(OnEnterAnimationCompleteListener paramOnEnterAnimationCompleteListener) {
    checkIsAlive();
    CopyOnWriteArrayList<OnEnterAnimationCompleteListener> copyOnWriteArrayList = this.mOnEnterAnimationCompleteListeners;
    if (copyOnWriteArrayList == null)
      return; 
    copyOnWriteArrayList.remove(paramOnEnterAnimationCompleteListener);
  }
  
  public void addOnSystemGestureExclusionRectsChangedListener(Consumer<List<Rect>> paramConsumer) {
    checkIsAlive();
    if (this.mGestureExclusionListeners == null)
      this.mGestureExclusionListeners = new CopyOnWriteArray<>(); 
    this.mGestureExclusionListeners.add(paramConsumer);
  }
  
  public void removeOnSystemGestureExclusionRectsChangedListener(Consumer<List<Rect>> paramConsumer) {
    checkIsAlive();
    CopyOnWriteArray<Consumer<List<Rect>>> copyOnWriteArray = this.mGestureExclusionListeners;
    if (copyOnWriteArray == null)
      return; 
    copyOnWriteArray.remove(paramConsumer);
  }
  
  private void checkIsAlive() {
    if (this.mAlive)
      return; 
    throw new IllegalStateException("This ViewTreeObserver is not alive, call getViewTreeObserver() again");
  }
  
  public boolean isAlive() {
    return this.mAlive;
  }
  
  private void kill() {
    this.mAlive = false;
  }
  
  final void dispatchOnWindowAttachedChange(boolean paramBoolean) {
    CopyOnWriteArrayList<OnWindowAttachListener> copyOnWriteArrayList = this.mOnWindowAttachListeners;
    if (copyOnWriteArrayList != null && copyOnWriteArrayList.size() > 0)
      for (OnWindowAttachListener onWindowAttachListener : copyOnWriteArrayList) {
        if (paramBoolean) {
          onWindowAttachListener.onWindowAttached();
          continue;
        } 
        onWindowAttachListener.onWindowDetached();
      }  
  }
  
  final void dispatchOnWindowFocusChange(boolean paramBoolean) {
    CopyOnWriteArrayList<OnWindowFocusChangeListener> copyOnWriteArrayList = this.mOnWindowFocusListeners;
    if (copyOnWriteArrayList != null && copyOnWriteArrayList.size() > 0)
      for (OnWindowFocusChangeListener onWindowFocusChangeListener : copyOnWriteArrayList)
        onWindowFocusChangeListener.onWindowFocusChanged(paramBoolean);  
  }
  
  final void dispatchOnGlobalFocusChange(View paramView1, View paramView2) {
    CopyOnWriteArrayList<OnGlobalFocusChangeListener> copyOnWriteArrayList = this.mOnGlobalFocusListeners;
    if (copyOnWriteArrayList != null && copyOnWriteArrayList.size() > 0)
      for (OnGlobalFocusChangeListener onGlobalFocusChangeListener : copyOnWriteArrayList)
        onGlobalFocusChangeListener.onGlobalFocusChanged(paramView1, paramView2);  
  }
  
  public final void dispatchOnGlobalLayout() {
    CopyOnWriteArray<OnGlobalLayoutListener> copyOnWriteArray = this.mOnGlobalLayoutListeners;
    if (copyOnWriteArray != null && copyOnWriteArray.size() > 0) {
      null = copyOnWriteArray.start();
      try {
        int i = null.size();
        for (byte b = 0; b < i; b++)
          ((OnGlobalLayoutListener)null.get(b)).onGlobalLayout(); 
      } finally {
        copyOnWriteArray.end();
      } 
    } 
  }
  
  final boolean hasOnPreDrawListeners() {
    boolean bool;
    CopyOnWriteArray<OnPreDrawListener> copyOnWriteArray = this.mOnPreDrawListeners;
    if (copyOnWriteArray != null && copyOnWriteArray.size() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean dispatchOnPreDraw() {
    int i;
    boolean bool1 = false, bool2 = false;
    CopyOnWriteArray<OnPreDrawListener> copyOnWriteArray = this.mOnPreDrawListeners;
    boolean bool3 = bool1;
    if (copyOnWriteArray != null) {
      bool3 = bool1;
      if (copyOnWriteArray.size() > 0) {
        null = copyOnWriteArray.start();
        try {
          int j = null.size();
          for (byte b = 0; b < j; b++) {
            bool1 = ((OnPreDrawListener)null.get(b)).onPreDraw();
            i = bool3 | bool1 ^ true;
          } 
        } finally {
          copyOnWriteArray.end();
        } 
      } 
    } 
    return i;
  }
  
  public final void dispatchOnWindowShown() {
    this.mWindowShown = true;
    CopyOnWriteArray<OnWindowShownListener> copyOnWriteArray = this.mOnWindowShownListeners;
    if (copyOnWriteArray != null && copyOnWriteArray.size() > 0) {
      null = copyOnWriteArray.start();
      try {
        int i = null.size();
        for (byte b = 0; b < i; b++)
          ((OnWindowShownListener)null.get(b)).onWindowShown(); 
      } finally {
        copyOnWriteArray.end();
      } 
    } 
  }
  
  public final void dispatchOnDraw() {
    if (this.mOnDrawListeners != null) {
      this.mInDispatchOnDraw = true;
      ArrayList<OnDrawListener> arrayList = this.mOnDrawListeners;
      int i = arrayList.size();
      for (byte b = 0; b < i; b++)
        ((OnDrawListener)arrayList.get(b)).onDraw(); 
      this.mInDispatchOnDraw = false;
    } 
  }
  
  final void dispatchOnTouchModeChanged(boolean paramBoolean) {
    CopyOnWriteArrayList<OnTouchModeChangeListener> copyOnWriteArrayList = this.mOnTouchModeChangeListeners;
    if (copyOnWriteArrayList != null && copyOnWriteArrayList.size() > 0)
      for (OnTouchModeChangeListener onTouchModeChangeListener : copyOnWriteArrayList)
        onTouchModeChangeListener.onTouchModeChanged(paramBoolean);  
  }
  
  final void dispatchOnScrollChanged() {
    CopyOnWriteArray<OnScrollChangedListener> copyOnWriteArray = this.mOnScrollChangedListeners;
    if (copyOnWriteArray != null && copyOnWriteArray.size() > 0) {
      null = copyOnWriteArray.start();
      try {
        int i = null.size();
        for (byte b = 0; b < i; b++)
          ((OnScrollChangedListener)null.get(b)).onScrollChanged(); 
      } finally {
        copyOnWriteArray.end();
      } 
    } 
  }
  
  final boolean hasComputeInternalInsetsListeners() {
    boolean bool;
    CopyOnWriteArray<OnComputeInternalInsetsListener> copyOnWriteArray = this.mOnComputeInternalInsetsListeners;
    if (copyOnWriteArray != null && copyOnWriteArray.size() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  final void dispatchOnComputeInternalInsets(InternalInsetsInfo paramInternalInsetsInfo) {
    CopyOnWriteArray<OnComputeInternalInsetsListener> copyOnWriteArray = this.mOnComputeInternalInsetsListeners;
    if (copyOnWriteArray != null && copyOnWriteArray.size() > 0) {
      CopyOnWriteArray.Access<OnComputeInternalInsetsListener> access = copyOnWriteArray.start();
      try {
        int i = access.size();
        for (byte b = 0; b < i; b++)
          ((OnComputeInternalInsetsListener)access.get(b)).onComputeInternalInsets(paramInternalInsetsInfo); 
      } finally {
        copyOnWriteArray.end();
      } 
    } 
  }
  
  public final void dispatchOnEnterAnimationComplete() {
    CopyOnWriteArrayList<OnEnterAnimationCompleteListener> copyOnWriteArrayList = this.mOnEnterAnimationCompleteListeners;
    if (copyOnWriteArrayList != null && !copyOnWriteArrayList.isEmpty())
      for (OnEnterAnimationCompleteListener onEnterAnimationCompleteListener : copyOnWriteArrayList)
        onEnterAnimationCompleteListener.onEnterAnimationComplete();  
  }
  
  void dispatchOnSystemGestureExclusionRectsChanged(List<Rect> paramList) {
    CopyOnWriteArray<Consumer<List<Rect>>> copyOnWriteArray = this.mGestureExclusionListeners;
    if (copyOnWriteArray != null && copyOnWriteArray.size() > 0) {
      CopyOnWriteArray.Access<Consumer<List<Rect>>> access = copyOnWriteArray.start();
      try {
        int i = access.size();
        for (byte b = 0; b < i; b++)
          ((Consumer<List<Rect>>)access.get(b)).accept(paramList); 
      } finally {
        copyOnWriteArray.end();
      } 
    } 
  }
  
  class CopyOnWriteArray<T> {
    private boolean mStart;
    
    private ArrayList<T> mDataCopy;
    
    private ArrayList<T> mData = new ArrayList<>();
    
    private final Access<T> mAccess = new Access<>();
    
    static class Access<T> {
      private ArrayList<T> mData;
      
      private int mSize;
      
      T get(int param2Int) {
        return this.mData.get(param2Int);
      }
      
      int size() {
        return this.mSize;
      }
    }
    
    private ArrayList<T> getArray() {
      if (this.mStart) {
        if (this.mDataCopy == null)
          this.mDataCopy = new ArrayList<>(this.mData); 
        return this.mDataCopy;
      } 
      return this.mData;
    }
    
    Access<T> start() {
      if (!this.mStart) {
        this.mStart = true;
        this.mDataCopy = null;
        Access.access$002(this.mAccess, this.mData);
        Access.access$102(this.mAccess, this.mData.size());
        return this.mAccess;
      } 
      throw new IllegalStateException("Iteration already started");
    }
    
    void end() {
      if (this.mStart) {
        this.mStart = false;
        ArrayList<T> arrayList = this.mDataCopy;
        if (arrayList != null) {
          this.mData = arrayList;
          this.mAccess.mData.clear();
          Access.access$102(this.mAccess, 0);
        } 
        this.mDataCopy = null;
        return;
      } 
      throw new IllegalStateException("Iteration not started");
    }
    
    int size() {
      return getArray().size();
    }
    
    void add(T param1T) {
      getArray().add(param1T);
    }
    
    void addAll(CopyOnWriteArray<T> param1CopyOnWriteArray) {
      getArray().addAll(param1CopyOnWriteArray.mData);
    }
    
    void remove(T param1T) {
      getArray().remove(param1T);
    }
    
    void clear() {
      getArray().clear();
    }
  }
}
