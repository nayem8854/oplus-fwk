package android.view;

public class OplusKeyEvent extends KeyEvent {
  public static final int KEYCODE_GIMBAL_POWER = 717;
  
  public static final int KEYCODE_GIMBAL_SWITCH_CAMERA = 706;
  
  private OplusKeyEvent(int paramInt1, int paramInt2) {
    super(paramInt1, paramInt2);
  }
}
