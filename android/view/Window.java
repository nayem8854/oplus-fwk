package android.view;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Insets;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.session.MediaController;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Pair;
import android.view.accessibility.AccessibilityEvent;
import java.util.Collections;
import java.util.List;

public abstract class Window extends OplusBaseWindow {
  private boolean mIsActive = false;
  
  private boolean mHasChildren = false;
  
  private boolean mCloseOnTouchOutside = false;
  
  private boolean mSetCloseOnTouchOutside = false;
  
  private int mForcedWindowFlags = 0;
  
  private boolean mHaveWindowFormat = false;
  
  private boolean mHaveDimAmount = false;
  
  private int mDefaultWindowFormat = -1;
  
  private boolean mHasSoftInputMode = false;
  
  private boolean mOverlayWithDecorCaptionEnabled = true;
  
  private boolean mCloseOnSwipeEnabled = false;
  
  private final WindowManager.LayoutParams mWindowAttributes = new WindowManager.LayoutParams();
  
  public static final int DECOR_CAPTION_SHADE_AUTO = 0;
  
  public static final int DECOR_CAPTION_SHADE_DARK = 2;
  
  public static final int DECOR_CAPTION_SHADE_LIGHT = 1;
  
  @Deprecated
  protected static final int DEFAULT_FEATURES = 65;
  
  public static final int FEATURE_ACTION_BAR = 8;
  
  public static final int FEATURE_ACTION_BAR_OVERLAY = 9;
  
  public static final int FEATURE_ACTION_MODE_OVERLAY = 10;
  
  public static final int FEATURE_ACTIVITY_TRANSITIONS = 13;
  
  public static final int FEATURE_CONTENT_TRANSITIONS = 12;
  
  public static final int FEATURE_CONTEXT_MENU = 6;
  
  public static final int FEATURE_CUSTOM_TITLE = 7;
  
  @Deprecated
  public static final int FEATURE_INDETERMINATE_PROGRESS = 5;
  
  public static final int FEATURE_LEFT_ICON = 3;
  
  public static final int FEATURE_MAX = 13;
  
  public static final int FEATURE_NO_TITLE = 1;
  
  public static final int FEATURE_OPTIONS_PANEL = 0;
  
  @Deprecated
  public static final int FEATURE_PROGRESS = 2;
  
  public static final int FEATURE_RIGHT_ICON = 4;
  
  @Deprecated
  public static final int FEATURE_SWIPE_TO_DISMISS = 11;
  
  public static final int ID_ANDROID_CONTENT = 16908290;
  
  public static final String NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME = "android:navigation:background";
  
  @Deprecated
  public static final int PROGRESS_END = 10000;
  
  @Deprecated
  public static final int PROGRESS_INDETERMINATE_OFF = -4;
  
  @Deprecated
  public static final int PROGRESS_INDETERMINATE_ON = -3;
  
  @Deprecated
  public static final int PROGRESS_SECONDARY_END = 30000;
  
  @Deprecated
  public static final int PROGRESS_SECONDARY_START = 20000;
  
  @Deprecated
  public static final int PROGRESS_START = 0;
  
  @Deprecated
  public static final int PROGRESS_VISIBILITY_OFF = -2;
  
  @Deprecated
  public static final int PROGRESS_VISIBILITY_ON = -1;
  
  public static final String STATUS_BAR_BACKGROUND_TRANSITION_NAME = "android:status:background";
  
  private Window mActiveChild;
  
  private String mAppName;
  
  private IBinder mAppToken;
  
  private Callback mCallback;
  
  private Window mContainer;
  
  private final Context mContext;
  
  private boolean mDestroyed;
  
  private int mFeatures;
  
  private boolean mHardwareAccelerated;
  
  private int mLocalFeatures;
  
  private OnRestrictedCaptionAreaChangedListener mOnRestrictedCaptionAreaChangedListener;
  
  private OnWindowDismissedCallback mOnWindowDismissedCallback;
  
  private OnWindowSwipeDismissedCallback mOnWindowSwipeDismissedCallback;
  
  private Rect mRestrictedCaptionAreaRect;
  
  private WindowControllerCallback mWindowControllerCallback;
  
  private WindowManager mWindowManager;
  
  private TypedArray mWindowStyle;
  
  class Callback {
    public abstract ActionMode onWindowStartingActionMode(ActionMode.Callback param1Callback, int param1Int);
    
    public abstract ActionMode onWindowStartingActionMode(ActionMode.Callback param1Callback);
    
    public abstract void onWindowFocusChanged(boolean param1Boolean);
    
    public abstract void onWindowAttributesChanged(WindowManager.LayoutParams param1LayoutParams);
    
    public abstract boolean onSearchRequested(SearchEvent param1SearchEvent);
    
    public abstract boolean onSearchRequested();
    
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> param1List, Menu param1Menu, int param1Int) {}
    
    public abstract boolean onPreparePanel(int param1Int, View param1View, Menu param1Menu);
    
    public void onPointerCaptureChanged(boolean param1Boolean) {}
    
    public abstract void onPanelClosed(int param1Int, Menu param1Menu);
    
    public abstract boolean onMenuOpened(int param1Int, Menu param1Menu);
    
    public abstract boolean onMenuItemSelected(int param1Int, MenuItem param1MenuItem);
    
    public abstract void onDetachedFromWindow();
    
    public abstract View onCreatePanelView(int param1Int);
    
    public abstract boolean onCreatePanelMenu(int param1Int, Menu param1Menu);
    
    public abstract void onContentChanged();
    
    public abstract void onAttachedToWindow();
    
    public abstract void onActionModeStarted(ActionMode param1ActionMode);
    
    public abstract void onActionModeFinished(ActionMode param1ActionMode);
    
    public abstract boolean dispatchTrackballEvent(MotionEvent param1MotionEvent);
    
    public abstract boolean dispatchTouchEvent(MotionEvent param1MotionEvent);
    
    public abstract boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent);
    
    public abstract boolean dispatchKeyShortcutEvent(KeyEvent param1KeyEvent);
    
    public abstract boolean dispatchKeyEvent(KeyEvent param1KeyEvent);
    
    public abstract boolean dispatchGenericMotionEvent(MotionEvent param1MotionEvent);
  }
  
  public Window(Context paramContext) {
    this.mContext = paramContext;
    int i = getDefaultFeatures(paramContext);
    this.mFeatures = i;
  }
  
  public final Context getContext() {
    return this.mContext;
  }
  
  public final TypedArray getWindowStyle() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mWindowStyle : Landroid/content/res/TypedArray;
    //   6: ifnonnull -> 23
    //   9: aload_0
    //   10: aload_0
    //   11: getfield mContext : Landroid/content/Context;
    //   14: getstatic com/android/internal/R$styleable.Window : [I
    //   17: invokevirtual obtainStyledAttributes : ([I)Landroid/content/res/TypedArray;
    //   20: putfield mWindowStyle : Landroid/content/res/TypedArray;
    //   23: aload_0
    //   24: getfield mWindowStyle : Landroid/content/res/TypedArray;
    //   27: astore_1
    //   28: aload_0
    //   29: monitorexit
    //   30: aload_1
    //   31: areturn
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #751	-> 0
    //   #752	-> 2
    //   #753	-> 9
    //   #756	-> 23
    //   #757	-> 32
    // Exception table:
    //   from	to	target	type
    //   2	9	32	finally
    //   9	23	32	finally
    //   23	30	32	finally
    //   33	35	32	finally
  }
  
  public void setContainer(Window paramWindow) {
    this.mContainer = paramWindow;
    if (paramWindow != null) {
      this.mFeatures |= 0x2;
      this.mLocalFeatures |= 0x2;
      paramWindow.mHasChildren = true;
    } 
  }
  
  public final Window getContainer() {
    return this.mContainer;
  }
  
  public final boolean hasChildren() {
    return this.mHasChildren;
  }
  
  public final void destroy() {
    this.mDestroyed = true;
  }
  
  public final boolean isDestroyed() {
    return this.mDestroyed;
  }
  
  public void setWindowManager(WindowManager paramWindowManager, IBinder paramIBinder, String paramString) {
    setWindowManager(paramWindowManager, paramIBinder, paramString, false);
  }
  
  public void setWindowManager(WindowManager paramWindowManager, IBinder paramIBinder, String paramString, boolean paramBoolean) {
    this.mAppToken = paramIBinder;
    this.mAppName = paramString;
    this.mHardwareAccelerated = paramBoolean;
    WindowManager windowManager = paramWindowManager;
    if (paramWindowManager == null)
      windowManager = (WindowManager)this.mContext.getSystemService("window"); 
    this.mWindowManager = ((WindowManagerImpl)windowManager).createLocalWindowManager(this);
  }
  
  void adjustLayoutParamsForSubWindow(WindowManager.LayoutParams paramLayoutParams) {
    CharSequence charSequence = paramLayoutParams.getTitle();
    if (paramLayoutParams.type >= 1000 && paramLayoutParams.type <= 1999) {
      if (paramLayoutParams.token == null) {
        View view = peekDecorView();
        if (view != null)
          paramLayoutParams.token = view.getWindowToken(); 
      } 
      if (charSequence == null || charSequence.length() == 0) {
        StringBuilder stringBuilder = new StringBuilder(32);
        if (paramLayoutParams.type == 1001) {
          stringBuilder.append("Media");
        } else if (paramLayoutParams.type == 1004) {
          stringBuilder.append("MediaOvr");
        } else if (paramLayoutParams.type == 1000) {
          stringBuilder.append("Panel");
        } else if (paramLayoutParams.type == 1002) {
          stringBuilder.append("SubPanel");
        } else if (paramLayoutParams.type == 1005) {
          stringBuilder.append("AboveSubPanel");
        } else if (paramLayoutParams.type == 1003) {
          stringBuilder.append("AtchDlg");
        } else {
          stringBuilder.append(paramLayoutParams.type);
        } 
        if (this.mAppName != null) {
          stringBuilder.append(":");
          stringBuilder.append(this.mAppName);
        } 
        paramLayoutParams.setTitle(stringBuilder);
      } 
    } else if (paramLayoutParams.type >= 2000 && paramLayoutParams.type <= 2999) {
      if (charSequence == null || charSequence.length() == 0) {
        StringBuilder stringBuilder = new StringBuilder(32);
        stringBuilder.append("Sys");
        stringBuilder.append(paramLayoutParams.type);
        if (this.mAppName != null) {
          stringBuilder.append(":");
          stringBuilder.append(this.mAppName);
        } 
        paramLayoutParams.setTitle(stringBuilder);
      } 
    } else {
      if (paramLayoutParams.token == null) {
        IBinder iBinder;
        Window window = this.mContainer;
        if (window == null) {
          iBinder = this.mAppToken;
        } else {
          iBinder = ((Window)iBinder).mAppToken;
        } 
        paramLayoutParams.token = iBinder;
      } 
      if (charSequence == null || charSequence.length() == 0) {
        String str = this.mAppName;
        if (str != null)
          paramLayoutParams.setTitle(str); 
      } 
    } 
    if (paramLayoutParams.packageName == null)
      paramLayoutParams.packageName = this.mContext.getPackageName(); 
    if (this.mHardwareAccelerated || (this.mWindowAttributes.flags & 0x1000000) != 0)
      paramLayoutParams.flags |= 0x1000000; 
  }
  
  public WindowManager getWindowManager() {
    return this.mWindowManager;
  }
  
  public void setCallback(Callback paramCallback) {
    this.mCallback = paramCallback;
  }
  
  public final Callback getCallback() {
    return this.mCallback;
  }
  
  public final void addOnFrameMetricsAvailableListener(OnFrameMetricsAvailableListener paramOnFrameMetricsAvailableListener, Handler paramHandler) {
    View view = getDecorView();
    if (view != null) {
      if (paramOnFrameMetricsAvailableListener != null) {
        view.addFrameMetricsListener(this, paramOnFrameMetricsAvailableListener, paramHandler);
        return;
      } 
      throw new NullPointerException("listener cannot be null");
    } 
    throw new IllegalStateException("can't observe a Window without an attached view");
  }
  
  public final void removeOnFrameMetricsAvailableListener(OnFrameMetricsAvailableListener paramOnFrameMetricsAvailableListener) {
    View view = getDecorView();
    if (view != null)
      getDecorView().removeFrameMetricsListener(paramOnFrameMetricsAvailableListener); 
  }
  
  public final void setOnWindowDismissedCallback(OnWindowDismissedCallback paramOnWindowDismissedCallback) {
    this.mOnWindowDismissedCallback = paramOnWindowDismissedCallback;
  }
  
  public final void dispatchOnWindowDismissed(boolean paramBoolean1, boolean paramBoolean2) {
    OnWindowDismissedCallback onWindowDismissedCallback = this.mOnWindowDismissedCallback;
    if (onWindowDismissedCallback != null)
      onWindowDismissedCallback.onWindowDismissed(paramBoolean1, paramBoolean2); 
  }
  
  public final void setOnWindowSwipeDismissedCallback(OnWindowSwipeDismissedCallback paramOnWindowSwipeDismissedCallback) {
    this.mOnWindowSwipeDismissedCallback = paramOnWindowSwipeDismissedCallback;
  }
  
  public final void dispatchOnWindowSwipeDismissed() {
    OnWindowSwipeDismissedCallback onWindowSwipeDismissedCallback = this.mOnWindowSwipeDismissedCallback;
    if (onWindowSwipeDismissedCallback != null)
      onWindowSwipeDismissedCallback.onWindowSwipeDismissed(); 
  }
  
  public final void setWindowControllerCallback(WindowControllerCallback paramWindowControllerCallback) {
    this.mWindowControllerCallback = paramWindowControllerCallback;
  }
  
  public final WindowControllerCallback getWindowControllerCallback() {
    return this.mWindowControllerCallback;
  }
  
  public final void setRestrictedCaptionAreaListener(OnRestrictedCaptionAreaChangedListener paramOnRestrictedCaptionAreaChangedListener) {
    this.mOnRestrictedCaptionAreaChangedListener = paramOnRestrictedCaptionAreaChangedListener;
    if (paramOnRestrictedCaptionAreaChangedListener != null) {
      Rect rect = new Rect();
    } else {
      paramOnRestrictedCaptionAreaChangedListener = null;
    } 
    this.mRestrictedCaptionAreaRect = (Rect)paramOnRestrictedCaptionAreaChangedListener;
  }
  
  public void setLayout(int paramInt1, int paramInt2) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    layoutParams.width = paramInt1;
    layoutParams.height = paramInt2;
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public void setGravity(int paramInt) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    layoutParams.gravity = paramInt;
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public void setType(int paramInt) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    layoutParams.type = paramInt;
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public void setFormat(int paramInt) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    if (paramInt != 0) {
      layoutParams.format = paramInt;
      this.mHaveWindowFormat = true;
    } else {
      layoutParams.format = this.mDefaultWindowFormat;
      this.mHaveWindowFormat = false;
    } 
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public void setWindowAnimations(int paramInt) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    layoutParams.windowAnimations = paramInt;
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public void setSoftInputMode(int paramInt) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    if (paramInt != 0) {
      layoutParams.softInputMode = paramInt;
      this.mHasSoftInputMode = true;
    } else {
      this.mHasSoftInputMode = false;
    } 
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public void addFlags(int paramInt) {
    setFlags(paramInt, paramInt);
  }
  
  public void addPrivateFlags(int paramInt) {
    setPrivateFlags(paramInt, paramInt);
  }
  
  @SystemApi
  public void addSystemFlags(int paramInt) {
    addPrivateFlags(paramInt);
  }
  
  public void clearFlags(int paramInt) {
    setFlags(0, paramInt);
  }
  
  public void setFlags(int paramInt1, int paramInt2) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    layoutParams.flags = layoutParams.flags & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2;
    this.mForcedWindowFlags |= paramInt2;
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  private void setPrivateFlags(int paramInt1, int paramInt2) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    layoutParams.privateFlags = layoutParams.privateFlags & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2;
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  protected void dispatchWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams) {
    Callback callback = this.mCallback;
    if (callback != null)
      callback.onWindowAttributesChanged(paramLayoutParams); 
  }
  
  public void setColorMode(int paramInt) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    layoutParams.setColorMode(paramInt);
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public void setPreferMinimalPostProcessing(boolean paramBoolean) {
    this.mWindowAttributes.preferMinimalPostProcessing = paramBoolean;
    dispatchWindowAttributesChanged(this.mWindowAttributes);
  }
  
  public int getColorMode() {
    return getAttributes().getColorMode();
  }
  
  public boolean isWideColorGamut() {
    int i = getColorMode();
    boolean bool = true;
    if (i != 1 || 
      !getContext().getResources().getConfiguration().isScreenWideColorGamut())
      bool = false; 
    return bool;
  }
  
  public void setDimAmount(float paramFloat) {
    WindowManager.LayoutParams layoutParams = getAttributes();
    layoutParams.dimAmount = paramFloat;
    this.mHaveDimAmount = true;
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public void setDecorFitsSystemWindows(boolean paramBoolean) {}
  
  public void setAttributes(WindowManager.LayoutParams paramLayoutParams) {
    this.mWindowAttributes.copyFrom(paramLayoutParams);
    dispatchWindowAttributesChanged(this.mWindowAttributes);
  }
  
  public final WindowManager.LayoutParams getAttributes() {
    return this.mWindowAttributes;
  }
  
  protected final int getForcedWindowFlags() {
    return this.mForcedWindowFlags;
  }
  
  protected final boolean hasSoftInputMode() {
    return this.mHasSoftInputMode;
  }
  
  public void setCloseOnTouchOutside(boolean paramBoolean) {
    this.mCloseOnTouchOutside = paramBoolean;
    this.mSetCloseOnTouchOutside = true;
  }
  
  public void setCloseOnTouchOutsideIfNotSet(boolean paramBoolean) {
    if (!this.mSetCloseOnTouchOutside) {
      this.mCloseOnTouchOutside = paramBoolean;
      this.mSetCloseOnTouchOutside = true;
    } 
  }
  
  public boolean shouldCloseOnTouch(Context paramContext, MotionEvent paramMotionEvent) {
    boolean bool;
    if ((paramMotionEvent.getAction() == 1 && isOutOfBounds(paramContext, paramMotionEvent)) || 
      paramMotionEvent.getAction() == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mCloseOnTouchOutside && peekDecorView() != null && bool)
      return true; 
    return false;
  }
  
  public void setSustainedPerformanceMode(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setPrivateFlags(bool, 262144);
  }
  
  private boolean isOutOfBounds(Context paramContext, MotionEvent paramMotionEvent) {
    int i = (int)paramMotionEvent.getX();
    int j = (int)paramMotionEvent.getY();
    int k = ViewConfiguration.get(paramContext).getScaledWindowTouchSlop();
    View view = getDecorView();
    return (i < -k || j < -k || 
      i > view.getWidth() + k || 
      j > view.getHeight() + k);
  }
  
  public boolean requestFeature(int paramInt) {
    boolean bool = true;
    int i = 1 << paramInt;
    this.mFeatures |= i;
    int j = this.mLocalFeatures;
    Window window = this.mContainer;
    if (window != null) {
      paramInt = (window.mFeatures ^ 0xFFFFFFFF) & i;
    } else {
      paramInt = i;
    } 
    this.mLocalFeatures = j | paramInt;
    if ((this.mFeatures & i) == 0)
      bool = false; 
    return bool;
  }
  
  protected void removeFeature(int paramInt) {
    paramInt = 1 << paramInt;
    this.mFeatures &= paramInt ^ 0xFFFFFFFF;
    int i = this.mLocalFeatures;
    Window window = this.mContainer;
    if (window != null)
      paramInt = (window.mFeatures ^ 0xFFFFFFFF) & paramInt; 
    this.mLocalFeatures = i & (paramInt ^ 0xFFFFFFFF);
  }
  
  public final void makeActive() {
    Window window = this.mContainer;
    if (window != null) {
      window = window.mActiveChild;
      if (window != null)
        window.mIsActive = false; 
      this.mContainer.mActiveChild = this;
    } 
    this.mIsActive = true;
    onActive();
  }
  
  public final boolean isActive() {
    return this.mIsActive;
  }
  
  public <T extends View> T findViewById(int paramInt) {
    return getDecorView().findViewById(paramInt);
  }
  
  public final <T extends View> T requireViewById(int paramInt) {
    T t = (T)findViewById(paramInt);
    if (t != null)
      return t; 
    throw new IllegalArgumentException("ID does not reference a View inside this Window");
  }
  
  public void setElevation(float paramFloat) {}
  
  public float getElevation() {
    return 0.0F;
  }
  
  public void setClipToOutline(boolean paramBoolean) {}
  
  public void setBackgroundDrawableResource(int paramInt) {
    setBackgroundDrawable(this.mContext.getDrawable(paramInt));
  }
  
  public View getStatusBarBackgroundView() {
    return null;
  }
  
  public View getNavigationBarBackgroundView() {
    return null;
  }
  
  protected final int getFeatures() {
    return this.mFeatures;
  }
  
  public static int getDefaultFeatures(Context paramContext) {
    int i = 0;
    Resources resources = paramContext.getResources();
    if (resources.getBoolean(17891405))
      i = false | true; 
    int j = i;
    if (resources.getBoolean(17891404))
      j = i | 0x40; 
    return j;
  }
  
  public boolean hasFeature(int paramInt) {
    int i = getFeatures();
    boolean bool = true;
    if ((i & 1 << paramInt) == 0)
      bool = false; 
    return bool;
  }
  
  protected final int getLocalFeatures() {
    return this.mLocalFeatures;
  }
  
  protected void setDefaultWindowFormat(int paramInt) {
    this.mDefaultWindowFormat = paramInt;
    if (!this.mHaveWindowFormat) {
      WindowManager.LayoutParams layoutParams = getAttributes();
      layoutParams.format = paramInt;
      dispatchWindowAttributesChanged(layoutParams);
    } 
  }
  
  protected boolean haveDimAmount() {
    return this.mHaveDimAmount;
  }
  
  public void setMediaController(MediaController paramMediaController) {}
  
  public MediaController getMediaController() {
    return null;
  }
  
  public void setUiOptions(int paramInt) {}
  
  public void setUiOptions(int paramInt1, int paramInt2) {}
  
  public void setIcon(int paramInt) {}
  
  public void setDefaultIcon(int paramInt) {}
  
  public void setLogo(int paramInt) {}
  
  public void setDefaultLogo(int paramInt) {}
  
  public void setLocalFocus(boolean paramBoolean1, boolean paramBoolean2) {}
  
  public void injectInputEvent(InputEvent paramInputEvent) {}
  
  public TransitionManager getTransitionManager() {
    return null;
  }
  
  public void setTransitionManager(TransitionManager paramTransitionManager) {
    throw new UnsupportedOperationException();
  }
  
  public Scene getContentScene() {
    return null;
  }
  
  public void setEnterTransition(Transition paramTransition) {}
  
  public void setReturnTransition(Transition paramTransition) {}
  
  public void setExitTransition(Transition paramTransition) {}
  
  public void setReenterTransition(Transition paramTransition) {}
  
  public Transition getEnterTransition() {
    return null;
  }
  
  public Transition getReturnTransition() {
    return null;
  }
  
  public Transition getExitTransition() {
    return null;
  }
  
  public Transition getReenterTransition() {
    return null;
  }
  
  public void setSharedElementEnterTransition(Transition paramTransition) {}
  
  public void setSharedElementReturnTransition(Transition paramTransition) {}
  
  public Transition getSharedElementEnterTransition() {
    return null;
  }
  
  public Transition getSharedElementReturnTransition() {
    return null;
  }
  
  public void setSharedElementExitTransition(Transition paramTransition) {}
  
  public void setSharedElementReenterTransition(Transition paramTransition) {}
  
  public Transition getSharedElementExitTransition() {
    return null;
  }
  
  public Transition getSharedElementReenterTransition() {
    return null;
  }
  
  public void setAllowEnterTransitionOverlap(boolean paramBoolean) {}
  
  public boolean getAllowEnterTransitionOverlap() {
    return true;
  }
  
  public void setAllowReturnTransitionOverlap(boolean paramBoolean) {}
  
  public boolean getAllowReturnTransitionOverlap() {
    return true;
  }
  
  public long getTransitionBackgroundFadeDuration() {
    return 0L;
  }
  
  public void setTransitionBackgroundFadeDuration(long paramLong) {}
  
  public boolean getSharedElementsUseOverlay() {
    return true;
  }
  
  public void setSharedElementsUseOverlay(boolean paramBoolean) {}
  
  public void setNavigationBarDividerColor(int paramInt) {}
  
  public int getNavigationBarDividerColor() {
    return 0;
  }
  
  public void setStatusBarContrastEnforced(boolean paramBoolean) {}
  
  public boolean isStatusBarContrastEnforced() {
    return false;
  }
  
  public void setNavigationBarContrastEnforced(boolean paramBoolean) {}
  
  public boolean isNavigationBarContrastEnforced() {
    return false;
  }
  
  public void setSystemGestureExclusionRects(List<Rect> paramList) {
    throw new UnsupportedOperationException("window does not support gesture exclusion rects");
  }
  
  public List<Rect> getSystemGestureExclusionRects() {
    return Collections.emptyList();
  }
  
  public void requestScrollCapture(IScrollCaptureController paramIScrollCaptureController) {}
  
  public void addScrollCaptureCallback(ScrollCaptureCallback paramScrollCaptureCallback) {}
  
  public void removeScrollCaptureCallback(ScrollCaptureCallback paramScrollCaptureCallback) {}
  
  public void setTheme(int paramInt) {}
  
  public void setOverlayWithDecorCaptionEnabled(boolean paramBoolean) {
    this.mOverlayWithDecorCaptionEnabled = paramBoolean;
  }
  
  public boolean isOverlayWithDecorCaptionEnabled() {
    return this.mOverlayWithDecorCaptionEnabled;
  }
  
  public void notifyRestrictedCaptionAreaCallback(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mOnRestrictedCaptionAreaChangedListener != null) {
      this.mRestrictedCaptionAreaRect.set(paramInt1, paramInt2, paramInt3, paramInt4);
      this.mOnRestrictedCaptionAreaChangedListener.onRestrictedCaptionAreaChanged(this.mRestrictedCaptionAreaRect);
    } 
  }
  
  public WindowInsetsController getInsetsController() {
    return null;
  }
  
  public abstract void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams);
  
  public abstract void alwaysReadCloseOnTouchAttr();
  
  public abstract void clearContentView();
  
  public abstract void closeAllPanels();
  
  public abstract void closePanel(int paramInt);
  
  public abstract View getCurrentFocus();
  
  public abstract View getDecorView();
  
  public abstract LayoutInflater getLayoutInflater();
  
  public abstract int getNavigationBarColor();
  
  public abstract int getStatusBarColor();
  
  public abstract int getVolumeControlStream();
  
  public abstract void invalidatePanelMenu(int paramInt);
  
  public abstract boolean isFloating();
  
  public abstract boolean isShortcutKey(int paramInt, KeyEvent paramKeyEvent);
  
  protected abstract void onActive();
  
  public abstract void onConfigurationChanged(Configuration paramConfiguration);
  
  public abstract void onMultiWindowModeChanged();
  
  public abstract void onPictureInPictureModeChanged(boolean paramBoolean);
  
  public abstract void openPanel(int paramInt, KeyEvent paramKeyEvent);
  
  public abstract View peekDecorView();
  
  public abstract boolean performContextMenuIdentifierAction(int paramInt1, int paramInt2);
  
  public abstract boolean performPanelIdentifierAction(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract boolean performPanelShortcut(int paramInt1, int paramInt2, KeyEvent paramKeyEvent, int paramInt3);
  
  public abstract void reportActivityRelaunched();
  
  public abstract void restoreHierarchyState(Bundle paramBundle);
  
  public abstract Bundle saveHierarchyState();
  
  public abstract void setBackgroundDrawable(Drawable paramDrawable);
  
  public abstract void setChildDrawable(int paramInt, Drawable paramDrawable);
  
  public abstract void setChildInt(int paramInt1, int paramInt2);
  
  public abstract void setContentView(int paramInt);
  
  public abstract void setContentView(View paramView);
  
  public abstract void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams);
  
  public abstract void setDecorCaptionShade(int paramInt);
  
  public abstract void setFeatureDrawable(int paramInt, Drawable paramDrawable);
  
  public abstract void setFeatureDrawableAlpha(int paramInt1, int paramInt2);
  
  public abstract void setFeatureDrawableResource(int paramInt1, int paramInt2);
  
  public abstract void setFeatureDrawableUri(int paramInt, Uri paramUri);
  
  public abstract void setFeatureInt(int paramInt1, int paramInt2);
  
  public abstract void setNavigationBarColor(int paramInt);
  
  public abstract void setResizingCaptionDrawable(Drawable paramDrawable);
  
  public abstract void setStatusBarColor(int paramInt);
  
  public abstract void setTitle(CharSequence paramCharSequence);
  
  @Deprecated
  public abstract void setTitleColor(int paramInt);
  
  public abstract void setVolumeControlStream(int paramInt);
  
  public abstract boolean superDispatchGenericMotionEvent(MotionEvent paramMotionEvent);
  
  public abstract boolean superDispatchKeyEvent(KeyEvent paramKeyEvent);
  
  public abstract boolean superDispatchKeyShortcutEvent(KeyEvent paramKeyEvent);
  
  public abstract boolean superDispatchTouchEvent(MotionEvent paramMotionEvent);
  
  public abstract boolean superDispatchTrackballEvent(MotionEvent paramMotionEvent);
  
  public abstract void takeInputQueue(InputQueue.Callback paramCallback);
  
  public abstract void takeKeyEvents(boolean paramBoolean);
  
  public abstract void takeSurface(SurfaceHolder.Callback2 paramCallback2);
  
  public abstract void togglePanel(int paramInt, KeyEvent paramKeyEvent);
  
  class OnContentApplyWindowInsetsListener {
    public abstract Pair<Insets, WindowInsets> onContentApplyWindowInsets(View param1View, WindowInsets param1WindowInsets);
  }
  
  class OnFrameMetricsAvailableListener {
    public abstract void onFrameMetricsAvailable(Window param1Window, FrameMetrics param1FrameMetrics, int param1Int);
  }
  
  class OnRestrictedCaptionAreaChangedListener {
    public abstract void onRestrictedCaptionAreaChanged(Rect param1Rect);
  }
  
  class OnWindowDismissedCallback {
    public abstract void onWindowDismissed(boolean param1Boolean1, boolean param1Boolean2);
  }
  
  class OnWindowSwipeDismissedCallback {
    public abstract void onWindowSwipeDismissed();
  }
  
  class WindowControllerCallback {
    public abstract void enterPictureInPictureModeIfPossible();
    
    public abstract boolean isTaskRoot();
    
    public abstract void toggleFreeformWindowingMode() throws RemoteException;
    
    public abstract void updateNavigationBarColor(int param1Int);
    
    public abstract void updateStatusBarColor(int param1Int);
  }
}
