package android.view;

import android.os.IBinder;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import java.util.function.Supplier;

public final class ImeInsetsSourceConsumer extends InsetsSourceConsumer {
  private EditorInfo mFocusedEditor;
  
  private boolean mIsRequestedVisibleAwaitingControl;
  
  private EditorInfo mPreRenderedEditor;
  
  private boolean mShowOnNextImeRender;
  
  public ImeInsetsSourceConsumer(InsetsState paramInsetsState, Supplier<SurfaceControl.Transaction> paramSupplier, InsetsController paramInsetsController) {
    super(13, paramInsetsState, paramSupplier, paramInsetsController);
  }
  
  public void onPreRendered(EditorInfo paramEditorInfo) {
    this.mPreRenderedEditor = paramEditorInfo;
    if (this.mShowOnNextImeRender) {
      this.mShowOnNextImeRender = false;
      if (isServedEditorRendered())
        applyImeVisibility(true); 
    } 
  }
  
  public void onServedEditorChanged(EditorInfo paramEditorInfo) {
    if (isDummyOrEmptyEditor(paramEditorInfo))
      this.mShowOnNextImeRender = false; 
    this.mFocusedEditor = paramEditorInfo;
  }
  
  public void applyImeVisibility(boolean paramBoolean) {
    this.mController.applyImeVisibility(paramBoolean);
  }
  
  public void onWindowFocusGained() {
    super.onWindowFocusGained();
    getImm().registerImeConsumer(this);
  }
  
  public void onWindowFocusLost() {
    super.onWindowFocusLost();
    getImm().unregisterImeConsumer(this);
    this.mIsRequestedVisibleAwaitingControl = false;
  }
  
  void hide(boolean paramBoolean, int paramInt) {
    hide();
    if (paramBoolean) {
      notifyHidden();
      removeSurface();
    } 
  }
  
  public int requestShow(boolean paramBoolean) {
    InsetsSourceControl insetsSourceControl = getControl();
    byte b = 1;
    if (insetsSourceControl == null)
      this.mIsRequestedVisibleAwaitingControl = true; 
    if (paramBoolean || (this.mState.getSource(getType()).isVisible() && getControl() != null))
      return 0; 
    if (!getImm().requestImeShow(this.mController.getHost().getWindowToken()))
      b = 2; 
    return b;
  }
  
  void notifyHidden() {
    getImm().notifyImeHidden(this.mController.getHost().getWindowToken());
  }
  
  public void removeSurface() {
    IBinder iBinder = this.mController.getHost().getWindowToken();
    if (iBinder != null)
      getImm().removeImeSurface(iBinder); 
  }
  
  public void setControl(InsetsSourceControl paramInsetsSourceControl, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    super.setControl(paramInsetsSourceControl, paramArrayOfint1, paramArrayOfint2);
    if (paramInsetsSourceControl == null && !this.mIsRequestedVisibleAwaitingControl) {
      hide();
      removeSurface();
    } 
  }
  
  protected boolean isRequestedVisibleAwaitingControl() {
    return (this.mIsRequestedVisibleAwaitingControl || isRequestedVisible());
  }
  
  public void onPerceptible(boolean paramBoolean) {
    super.onPerceptible(paramBoolean);
    IBinder iBinder = this.mController.getHost().getWindowToken();
    if (iBinder != null)
      getImm().reportPerceptible(iBinder, paramBoolean); 
  }
  
  private boolean isDummyOrEmptyEditor(EditorInfo paramEditorInfo) {
    return (paramEditorInfo == null || (paramEditorInfo.fieldId <= 0 && paramEditorInfo.inputType <= 0));
  }
  
  private boolean isServedEditorRendered() {
    EditorInfo editorInfo = this.mFocusedEditor;
    if (editorInfo != null && this.mPreRenderedEditor != null && 
      !isDummyOrEmptyEditor(editorInfo)) {
      editorInfo = this.mPreRenderedEditor;
      if (!isDummyOrEmptyEditor(editorInfo))
        return areEditorsSimilar(this.mFocusedEditor, this.mPreRenderedEditor); 
    } 
    return false;
  }
  
  public static boolean areEditorsSimilar(EditorInfo paramEditorInfo1, EditorInfo paramEditorInfo2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield imeOptions : I
    //   4: aload_1
    //   5: getfield imeOptions : I
    //   8: if_icmpne -> 46
    //   11: aload_0
    //   12: getfield inputType : I
    //   15: aload_1
    //   16: getfield inputType : I
    //   19: if_icmpne -> 46
    //   22: aload_0
    //   23: getfield packageName : Ljava/lang/String;
    //   26: astore_2
    //   27: aload_1
    //   28: getfield packageName : Ljava/lang/String;
    //   31: astore_3
    //   32: aload_2
    //   33: aload_3
    //   34: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   37: ifeq -> 46
    //   40: iconst_1
    //   41: istore #4
    //   43: goto -> 49
    //   46: iconst_0
    //   47: istore #4
    //   49: aload_0
    //   50: getfield privateImeOptions : Ljava/lang/String;
    //   53: ifnull -> 72
    //   56: aload_0
    //   57: getfield privateImeOptions : Ljava/lang/String;
    //   60: aload_1
    //   61: getfield privateImeOptions : Ljava/lang/String;
    //   64: invokevirtual equals : (Ljava/lang/Object;)Z
    //   67: istore #5
    //   69: goto -> 75
    //   72: iconst_1
    //   73: istore #5
    //   75: iload #4
    //   77: iload #5
    //   79: iand
    //   80: ifne -> 85
    //   83: iconst_0
    //   84: ireturn
    //   85: aload_0
    //   86: getfield extras : Landroid/os/Bundle;
    //   89: ifnonnull -> 99
    //   92: aload_1
    //   93: getfield extras : Landroid/os/Bundle;
    //   96: ifnull -> 110
    //   99: aload_0
    //   100: getfield extras : Landroid/os/Bundle;
    //   103: aload_1
    //   104: getfield extras : Landroid/os/Bundle;
    //   107: if_acmpne -> 112
    //   110: iconst_1
    //   111: ireturn
    //   112: aload_0
    //   113: getfield extras : Landroid/os/Bundle;
    //   116: ifnonnull -> 126
    //   119: aload_1
    //   120: getfield extras : Landroid/os/Bundle;
    //   123: ifnonnull -> 140
    //   126: aload_0
    //   127: getfield extras : Landroid/os/Bundle;
    //   130: ifnonnull -> 142
    //   133: aload_1
    //   134: getfield extras : Landroid/os/Bundle;
    //   137: ifnull -> 142
    //   140: iconst_0
    //   141: ireturn
    //   142: aload_0
    //   143: getfield extras : Landroid/os/Bundle;
    //   146: invokevirtual hashCode : ()I
    //   149: aload_1
    //   150: getfield extras : Landroid/os/Bundle;
    //   153: invokevirtual hashCode : ()I
    //   156: if_icmpeq -> 264
    //   159: aload_0
    //   160: getfield extras : Landroid/os/Bundle;
    //   163: astore_2
    //   164: aload_2
    //   165: aload_0
    //   166: invokevirtual equals : (Ljava/lang/Object;)Z
    //   169: ifeq -> 175
    //   172: goto -> 264
    //   175: aload_0
    //   176: getfield extras : Landroid/os/Bundle;
    //   179: invokevirtual size : ()I
    //   182: aload_1
    //   183: getfield extras : Landroid/os/Bundle;
    //   186: invokevirtual size : ()I
    //   189: if_icmpeq -> 194
    //   192: iconst_0
    //   193: ireturn
    //   194: aload_0
    //   195: getfield extras : Landroid/os/Bundle;
    //   198: invokevirtual toString : ()Ljava/lang/String;
    //   201: aload_1
    //   202: getfield extras : Landroid/os/Bundle;
    //   205: invokevirtual toString : ()Ljava/lang/String;
    //   208: invokevirtual equals : (Ljava/lang/Object;)Z
    //   211: ifeq -> 216
    //   214: iconst_1
    //   215: ireturn
    //   216: invokestatic obtain : ()Landroid/os/Parcel;
    //   219: astore_2
    //   220: aload_0
    //   221: getfield extras : Landroid/os/Bundle;
    //   224: aload_2
    //   225: iconst_0
    //   226: invokevirtual writeToParcel : (Landroid/os/Parcel;I)V
    //   229: aload_2
    //   230: iconst_0
    //   231: invokevirtual setDataPosition : (I)V
    //   234: invokestatic obtain : ()Landroid/os/Parcel;
    //   237: astore_0
    //   238: aload_1
    //   239: getfield extras : Landroid/os/Bundle;
    //   242: aload_0
    //   243: iconst_0
    //   244: invokevirtual writeToParcel : (Landroid/os/Parcel;I)V
    //   247: aload_0
    //   248: iconst_0
    //   249: invokevirtual setDataPosition : (I)V
    //   252: aload_2
    //   253: invokevirtual createByteArray : ()[B
    //   256: aload_0
    //   257: invokevirtual createByteArray : ()[B
    //   260: invokestatic equals : ([B[B)Z
    //   263: ireturn
    //   264: iconst_1
    //   265: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #189	-> 0
    //   #192	-> 32
    //   #193	-> 49
    //   #194	-> 56
    //   #196	-> 75
    //   #197	-> 83
    //   #201	-> 85
    //   #202	-> 110
    //   #204	-> 112
    //   #206	-> 140
    //   #208	-> 142
    //   #209	-> 164
    //   #212	-> 175
    //   #213	-> 192
    //   #215	-> 194
    //   #216	-> 214
    //   #220	-> 216
    //   #221	-> 220
    //   #222	-> 229
    //   #223	-> 234
    //   #224	-> 238
    //   #225	-> 247
    //   #227	-> 252
    //   #210	-> 264
  }
  
  private InputMethodManager getImm() {
    return this.mController.getHost().getInputMethodManager();
  }
}
