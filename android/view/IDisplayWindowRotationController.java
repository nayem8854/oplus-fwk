package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDisplayWindowRotationController extends IInterface {
  void onRotateDisplay(int paramInt1, int paramInt2, int paramInt3, IDisplayWindowRotationCallback paramIDisplayWindowRotationCallback) throws RemoteException;
  
  class Default implements IDisplayWindowRotationController {
    public void onRotateDisplay(int param1Int1, int param1Int2, int param1Int3, IDisplayWindowRotationCallback param1IDisplayWindowRotationCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDisplayWindowRotationController {
    private static final String DESCRIPTOR = "android.view.IDisplayWindowRotationController";
    
    static final int TRANSACTION_onRotateDisplay = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IDisplayWindowRotationController");
    }
    
    public static IDisplayWindowRotationController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IDisplayWindowRotationController");
      if (iInterface != null && iInterface instanceof IDisplayWindowRotationController)
        return (IDisplayWindowRotationController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onRotateDisplay";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IDisplayWindowRotationController");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IDisplayWindowRotationController");
      int i = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      IDisplayWindowRotationCallback iDisplayWindowRotationCallback = IDisplayWindowRotationCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      onRotateDisplay(i, param1Int1, param1Int2, iDisplayWindowRotationCallback);
      return true;
    }
    
    private static class Proxy implements IDisplayWindowRotationController {
      public static IDisplayWindowRotationController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IDisplayWindowRotationController";
      }
      
      public void onRotateDisplay(int param2Int1, int param2Int2, int param2Int3, IDisplayWindowRotationCallback param2IDisplayWindowRotationCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IDisplayWindowRotationController");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          if (param2IDisplayWindowRotationCallback != null) {
            iBinder = param2IDisplayWindowRotationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IDisplayWindowRotationController.Stub.getDefaultImpl() != null) {
            IDisplayWindowRotationController.Stub.getDefaultImpl().onRotateDisplay(param2Int1, param2Int2, param2Int3, param2IDisplayWindowRotationCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDisplayWindowRotationController param1IDisplayWindowRotationController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDisplayWindowRotationController != null) {
          Proxy.sDefaultImpl = param1IDisplayWindowRotationController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDisplayWindowRotationController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
