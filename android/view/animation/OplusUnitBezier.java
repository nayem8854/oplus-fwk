package android.view.animation;

public class OplusUnitBezier {
  private double ax;
  
  private double ay;
  
  private double bx;
  
  private double by;
  
  private double cx;
  
  private double cy;
  
  public OplusUnitBezier(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4) {
    double d = paramDouble1 * 3.0D;
    this.bx = paramDouble1 = (paramDouble3 - paramDouble1) * 3.0D - d;
    this.ax = 1.0D - d - paramDouble1;
    this.cy = paramDouble1 = paramDouble2 * 3.0D;
    this.by = paramDouble2 = (paramDouble4 - paramDouble2) * 3.0D - paramDouble1;
    this.ay = 1.0D - paramDouble1 - paramDouble2;
  }
  
  public double sampleCurveX(double paramDouble) {
    return ((this.ax * paramDouble + this.bx) * paramDouble + this.cx) * paramDouble;
  }
  
  public double sampleCurveY(double paramDouble) {
    return ((this.ay * paramDouble + this.by) * paramDouble + this.cy) * paramDouble;
  }
  
  public double sampleCurveDerivativeX(double paramDouble) {
    return (this.ax * 3.0D * paramDouble + this.bx * 2.0D) * paramDouble + this.cx;
  }
  
  public double solveCurveX(double paramDouble1, double paramDouble2) {
    double d1;
    byte b;
    for (d1 = paramDouble1, b = 0; b < 8; b++) {
      double d5 = sampleCurveX(d1) - paramDouble1;
      if (Math.abs(d5) < paramDouble2)
        return d1; 
      double d6 = sampleCurveDerivativeX(d1);
      if (Math.abs(d6) < 1.0E-6D)
        break; 
      d1 -= d5 / d6;
    } 
    double d2 = 0.0D;
    double d3 = 1.0D;
    double d4 = paramDouble1;
    if (d4 < 0.0D)
      return 0.0D; 
    d1 = d4;
    if (d4 > 1.0D)
      return 1.0D; 
    while (d2 < d3) {
      d4 = sampleCurveX(d1);
      if (Math.abs(d4 - paramDouble1) < paramDouble2)
        return d1; 
      if (paramDouble1 > d4) {
        d2 = d1;
      } else {
        d3 = d1;
      } 
      d1 = (d3 - d2) * 0.5D + d2;
    } 
    return d1;
  }
  
  double solve(double paramDouble1, double paramDouble2) {
    return sampleCurveY(solveCurveX(paramDouble1, paramDouble2));
  }
}
