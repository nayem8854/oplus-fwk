package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Xml;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimationUtils {
  private static final int SEQUENTIALLY = 1;
  
  private static final int TOGETHER = 0;
  
  private static class AnimationState {
    boolean animationClockLocked;
    
    long currentVsyncTimeMillis;
    
    long lastReportedTimeMillis;
    
    private AnimationState() {}
  }
  
  private static ThreadLocal<AnimationState> sAnimationState = new ThreadLocal<AnimationState>() {
      protected AnimationUtils.AnimationState initialValue() {
        return new AnimationUtils.AnimationState();
      }
    };
  
  public static void lockAnimationClock(long paramLong) {
    AnimationState animationState = sAnimationState.get();
    animationState.animationClockLocked = true;
    animationState.currentVsyncTimeMillis = paramLong;
  }
  
  public static void unlockAnimationClock() {
    ((AnimationState)sAnimationState.get()).animationClockLocked = false;
  }
  
  public static long currentAnimationTimeMillis() {
    AnimationState animationState = sAnimationState.get();
    if (animationState.animationClockLocked)
      return Math.max(animationState.currentVsyncTimeMillis, animationState.lastReportedTimeMillis); 
    animationState.lastReportedTimeMillis = SystemClock.uptimeMillis();
    return animationState.lastReportedTimeMillis;
  }
  
  public static Animation loadAnimation(Context paramContext, int paramInt) throws Resources.NotFoundException {
    XmlPullParserException xmlPullParserException2;
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null, xmlResourceParser3 = null;
    try {
      XmlResourceParser xmlResourceParser = paramContext.getResources().getAnimation(paramInt);
      xmlResourceParser3 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      Animation animation = createAnimationFromXml(paramContext, (XmlPullParser)xmlResourceParser);
      if (xmlResourceParser != null)
        xmlResourceParser.close(); 
      return animation;
    } catch (XmlPullParserException xmlPullParserException1) {
      xmlResourceParser3 = xmlResourceParser2;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      xmlResourceParser3 = xmlResourceParser2;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser3 = xmlResourceParser2;
      this();
      xmlResourceParser3 = xmlResourceParser2;
      stringBuilder.append("Can't load animation resource ID #0x");
      xmlResourceParser3 = xmlResourceParser2;
      stringBuilder.append(Integer.toHexString(paramInt));
      xmlResourceParser3 = xmlResourceParser2;
      this(stringBuilder.toString());
      xmlResourceParser3 = xmlResourceParser2;
      notFoundException.initCause((Throwable)xmlPullParserException1);
      xmlResourceParser3 = xmlResourceParser2;
      throw notFoundException;
    } catch (IOException iOException) {
      xmlPullParserException2 = xmlPullParserException1;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      xmlPullParserException2 = xmlPullParserException1;
      StringBuilder stringBuilder = new StringBuilder();
      xmlPullParserException2 = xmlPullParserException1;
      this();
      xmlPullParserException2 = xmlPullParserException1;
      stringBuilder.append("Can't load animation resource ID #0x");
      xmlPullParserException2 = xmlPullParserException1;
      stringBuilder.append(Integer.toHexString(paramInt));
      xmlPullParserException2 = xmlPullParserException1;
      this(stringBuilder.toString());
      xmlPullParserException2 = xmlPullParserException1;
      notFoundException.initCause(iOException);
      xmlPullParserException2 = xmlPullParserException1;
      throw notFoundException;
    } finally {}
    if (xmlPullParserException2 != null)
      xmlPullParserException2.close(); 
    throw paramContext;
  }
  
  private static Animation createAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    return createAnimationFromXml(paramContext, paramXmlPullParser, null, Xml.asAttributeSet(paramXmlPullParser));
  }
  
  private static Animation createAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser, AnimationSet paramAnimationSet, AttributeSet paramAttributeSet) throws XmlPullParserException, IOException {
    ClipRectAnimation clipRectAnimation;
    String str = null;
    int i = paramXmlPullParser.getDepth();
    while (true) {
      int j = paramXmlPullParser.next();
      if ((j != 3 || paramXmlPullParser.getDepth() > i) && j != 1) {
        AnimationSet animationSet;
        if (j != 2)
          continue; 
        str = paramXmlPullParser.getName();
        if (str.equals("set")) {
          animationSet = new AnimationSet(paramContext, paramAttributeSet);
          createAnimationFromXml(paramContext, paramXmlPullParser, animationSet, paramAttributeSet);
        } else {
          AlphaAnimation alphaAnimation;
          if (animationSet.equals("alpha")) {
            alphaAnimation = new AlphaAnimation(paramContext, paramAttributeSet);
          } else {
            ScaleAnimation scaleAnimation;
            if (alphaAnimation.equals("scale")) {
              scaleAnimation = new ScaleAnimation(paramContext, paramAttributeSet);
            } else {
              RotateAnimation rotateAnimation;
              if (scaleAnimation.equals("rotate")) {
                rotateAnimation = new RotateAnimation(paramContext, paramAttributeSet);
              } else {
                TranslateAnimation translateAnimation;
                if (rotateAnimation.equals("translate")) {
                  translateAnimation = new TranslateAnimation(paramContext, paramAttributeSet);
                } else if (translateAnimation.equals("cliprect")) {
                  clipRectAnimation = new ClipRectAnimation(paramContext, paramAttributeSet);
                } else {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("Unknown animation name: ");
                  stringBuilder.append(paramXmlPullParser.getName());
                  throw new RuntimeException(stringBuilder.toString());
                } 
              } 
            } 
          } 
        } 
        if (paramAnimationSet != null)
          paramAnimationSet.addAnimation(clipRectAnimation); 
        continue;
      } 
      break;
    } 
    return clipRectAnimation;
  }
  
  public static LayoutAnimationController loadLayoutAnimation(Context paramContext, int paramInt) throws Resources.NotFoundException {
    XmlPullParserException xmlPullParserException2;
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null, xmlResourceParser3 = null;
    try {
      XmlResourceParser xmlResourceParser = paramContext.getResources().getAnimation(paramInt);
      xmlResourceParser3 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      LayoutAnimationController layoutAnimationController = createLayoutAnimationFromXml(paramContext, (XmlPullParser)xmlResourceParser);
      if (xmlResourceParser != null)
        xmlResourceParser.close(); 
      return layoutAnimationController;
    } catch (XmlPullParserException xmlPullParserException1) {
      xmlResourceParser3 = xmlResourceParser2;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      xmlResourceParser3 = xmlResourceParser2;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser3 = xmlResourceParser2;
      this();
      xmlResourceParser3 = xmlResourceParser2;
      stringBuilder.append("Can't load animation resource ID #0x");
      xmlResourceParser3 = xmlResourceParser2;
      stringBuilder.append(Integer.toHexString(paramInt));
      xmlResourceParser3 = xmlResourceParser2;
      this(stringBuilder.toString());
      xmlResourceParser3 = xmlResourceParser2;
      notFoundException.initCause((Throwable)xmlPullParserException1);
      xmlResourceParser3 = xmlResourceParser2;
      throw notFoundException;
    } catch (IOException iOException) {
      xmlPullParserException2 = xmlPullParserException1;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      xmlPullParserException2 = xmlPullParserException1;
      StringBuilder stringBuilder = new StringBuilder();
      xmlPullParserException2 = xmlPullParserException1;
      this();
      xmlPullParserException2 = xmlPullParserException1;
      stringBuilder.append("Can't load animation resource ID #0x");
      xmlPullParserException2 = xmlPullParserException1;
      stringBuilder.append(Integer.toHexString(paramInt));
      xmlPullParserException2 = xmlPullParserException1;
      this(stringBuilder.toString());
      xmlPullParserException2 = xmlPullParserException1;
      notFoundException.initCause(iOException);
      xmlPullParserException2 = xmlPullParserException1;
      throw notFoundException;
    } finally {}
    if (xmlPullParserException2 != null)
      xmlPullParserException2.close(); 
    throw paramContext;
  }
  
  private static LayoutAnimationController createLayoutAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    return createLayoutAnimationFromXml(paramContext, paramXmlPullParser, Xml.asAttributeSet(paramXmlPullParser));
  }
  
  private static LayoutAnimationController createLayoutAnimationFromXml(Context paramContext, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet) throws XmlPullParserException, IOException {
    LayoutAnimationController layoutAnimationController;
    String str = null;
    int i = paramXmlPullParser.getDepth();
    while (true) {
      int j = paramXmlPullParser.next();
      if ((j != 3 || paramXmlPullParser.getDepth() > i) && j != 1) {
        if (j != 2)
          continue; 
        str = paramXmlPullParser.getName();
        if ("layoutAnimation".equals(str)) {
          layoutAnimationController = new LayoutAnimationController(paramContext, paramAttributeSet);
          continue;
        } 
        if ("gridLayoutAnimation".equals(layoutAnimationController)) {
          layoutAnimationController = new GridLayoutAnimationController(paramContext, paramAttributeSet);
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown layout animation name: ");
        stringBuilder.append((String)layoutAnimationController);
        throw new RuntimeException(stringBuilder.toString());
      } 
      break;
    } 
    return layoutAnimationController;
  }
  
  public static Animation makeInAnimation(Context paramContext, boolean paramBoolean) {
    Animation animation;
    if (paramBoolean) {
      animation = loadAnimation(paramContext, 17432578);
    } else {
      animation = loadAnimation((Context)animation, 17432741);
    } 
    animation.setInterpolator(new DecelerateInterpolator());
    animation.setStartTime(currentAnimationTimeMillis());
    return animation;
  }
  
  public static Animation makeOutAnimation(Context paramContext, boolean paramBoolean) {
    Animation animation;
    if (paramBoolean) {
      animation = loadAnimation(paramContext, 17432579);
    } else {
      animation = loadAnimation((Context)animation, 17432744);
    } 
    animation.setInterpolator(new AccelerateInterpolator());
    animation.setStartTime(currentAnimationTimeMillis());
    return animation;
  }
  
  public static Animation makeInChildBottomAnimation(Context paramContext) {
    Animation animation = loadAnimation(paramContext, 17432738);
    animation.setInterpolator(new AccelerateInterpolator());
    animation.setStartTime(currentAnimationTimeMillis());
    return animation;
  }
  
  public static Interpolator loadInterpolator(Context paramContext, int paramInt) throws Resources.NotFoundException {
    Resources.NotFoundException notFoundException1, notFoundException2;
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null, xmlResourceParser3 = null;
    try {
      XmlResourceParser xmlResourceParser = paramContext.getResources().getAnimation(paramInt);
      xmlResourceParser3 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      Interpolator interpolator = createInterpolatorFromXml(paramContext.getResources(), paramContext.getTheme(), (XmlPullParser)xmlResourceParser);
      if (xmlResourceParser != null)
        xmlResourceParser.close(); 
      return interpolator;
    } catch (XmlPullParserException xmlPullParserException) {
      xmlResourceParser3 = xmlResourceParser2;
      notFoundException1 = new Resources.NotFoundException();
      xmlResourceParser3 = xmlResourceParser2;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser3 = xmlResourceParser2;
      this();
      xmlResourceParser3 = xmlResourceParser2;
      stringBuilder.append("Can't load animation resource ID #0x");
      xmlResourceParser3 = xmlResourceParser2;
      stringBuilder.append(Integer.toHexString(paramInt));
      xmlResourceParser3 = xmlResourceParser2;
      this(stringBuilder.toString());
      xmlResourceParser3 = xmlResourceParser2;
      notFoundException1.initCause((Throwable)xmlPullParserException);
      xmlResourceParser3 = xmlResourceParser2;
      throw notFoundException1;
    } catch (IOException iOException) {
      notFoundException2 = notFoundException1;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      notFoundException2 = notFoundException1;
      StringBuilder stringBuilder = new StringBuilder();
      notFoundException2 = notFoundException1;
      this();
      notFoundException2 = notFoundException1;
      stringBuilder.append("Can't load animation resource ID #0x");
      notFoundException2 = notFoundException1;
      stringBuilder.append(Integer.toHexString(paramInt));
      notFoundException2 = notFoundException1;
      this(stringBuilder.toString());
      notFoundException2 = notFoundException1;
      notFoundException.initCause(iOException);
      notFoundException2 = notFoundException1;
      throw notFoundException;
    } finally {}
    if (notFoundException2 != null)
      notFoundException2.close(); 
    throw paramContext;
  }
  
  public static Interpolator loadInterpolator(Resources paramResources, Resources.Theme paramTheme, int paramInt) throws Resources.NotFoundException {
    XmlPullParserException xmlPullParserException2;
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null, xmlResourceParser3 = null;
    try {
      XmlResourceParser xmlResourceParser = paramResources.getAnimation(paramInt);
      xmlResourceParser3 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      Interpolator interpolator = createInterpolatorFromXml(paramResources, paramTheme, (XmlPullParser)xmlResourceParser);
      if (xmlResourceParser != null)
        xmlResourceParser.close(); 
      return interpolator;
    } catch (XmlPullParserException xmlPullParserException1) {
      xmlResourceParser3 = xmlResourceParser2;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      xmlResourceParser3 = xmlResourceParser2;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser3 = xmlResourceParser2;
      this();
      xmlResourceParser3 = xmlResourceParser2;
      stringBuilder.append("Can't load animation resource ID #0x");
      xmlResourceParser3 = xmlResourceParser2;
      stringBuilder.append(Integer.toHexString(paramInt));
      xmlResourceParser3 = xmlResourceParser2;
      this(stringBuilder.toString());
      xmlResourceParser3 = xmlResourceParser2;
      notFoundException.initCause((Throwable)xmlPullParserException1);
      xmlResourceParser3 = xmlResourceParser2;
      throw notFoundException;
    } catch (IOException iOException) {
      xmlPullParserException2 = xmlPullParserException1;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      xmlPullParserException2 = xmlPullParserException1;
      StringBuilder stringBuilder = new StringBuilder();
      xmlPullParserException2 = xmlPullParserException1;
      this();
      xmlPullParserException2 = xmlPullParserException1;
      stringBuilder.append("Can't load animation resource ID #0x");
      xmlPullParserException2 = xmlPullParserException1;
      stringBuilder.append(Integer.toHexString(paramInt));
      xmlPullParserException2 = xmlPullParserException1;
      this(stringBuilder.toString());
      xmlPullParserException2 = xmlPullParserException1;
      notFoundException.initCause(iOException);
      xmlPullParserException2 = xmlPullParserException1;
      throw notFoundException;
    } finally {}
    if (xmlPullParserException2 != null)
      xmlPullParserException2.close(); 
    throw paramResources;
  }
  
  private static Interpolator createInterpolatorFromXml(Resources paramResources, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    BaseInterpolator baseInterpolator = null;
    int i = paramXmlPullParser.getDepth();
    while (true) {
      int j = paramXmlPullParser.next();
      if ((j != 3 || paramXmlPullParser.getDepth() > i) && j != 1) {
        if (j != 2)
          continue; 
        AttributeSet attributeSet = Xml.asAttributeSet(paramXmlPullParser);
        String str = paramXmlPullParser.getName();
        baseInterpolator = OplusAnimationUtils.createInterpolatorFromXml(str, paramResources, paramTheme, attributeSet);
        if (baseInterpolator != null)
          return baseInterpolator; 
        if (str.equals("linearInterpolator")) {
          baseInterpolator = new LinearInterpolator();
          continue;
        } 
        if (str.equals("accelerateInterpolator")) {
          baseInterpolator = new AccelerateInterpolator(paramResources, paramTheme, attributeSet);
          continue;
        } 
        if (str.equals("decelerateInterpolator")) {
          baseInterpolator = new DecelerateInterpolator(paramResources, paramTheme, attributeSet);
          continue;
        } 
        if (str.equals("accelerateDecelerateInterpolator")) {
          baseInterpolator = new AccelerateDecelerateInterpolator();
          continue;
        } 
        if (str.equals("cycleInterpolator")) {
          baseInterpolator = new CycleInterpolator(paramResources, paramTheme, attributeSet);
          continue;
        } 
        if (str.equals("anticipateInterpolator")) {
          baseInterpolator = new AnticipateInterpolator(paramResources, paramTheme, attributeSet);
          continue;
        } 
        if (str.equals("overshootInterpolator")) {
          baseInterpolator = new OvershootInterpolator(paramResources, paramTheme, attributeSet);
          continue;
        } 
        if (str.equals("anticipateOvershootInterpolator")) {
          baseInterpolator = new AnticipateOvershootInterpolator(paramResources, paramTheme, attributeSet);
          continue;
        } 
        if (str.equals("bounceInterpolator")) {
          baseInterpolator = new BounceInterpolator();
          continue;
        } 
        if (str.equals("pathInterpolator")) {
          baseInterpolator = new PathInterpolator(paramResources, paramTheme, attributeSet);
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown interpolator name: ");
        stringBuilder.append(paramXmlPullParser.getName());
        throw new RuntimeException(stringBuilder.toString());
      } 
      break;
    } 
    return baseInterpolator;
  }
}
