package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R;

public class TranslateAnimation extends Animation {
  private int mFromXType = 0;
  
  private int mToXType = 0;
  
  private int mFromYType = 0;
  
  private int mToYType = 0;
  
  protected float mFromXValue = 0.0F;
  
  protected float mToXValue = 0.0F;
  
  protected float mFromYValue = 0.0F;
  
  protected float mToYValue = 0.0F;
  
  protected float mFromXDelta;
  
  protected float mFromYDelta;
  
  protected float mToXDelta;
  
  protected float mToYDelta;
  
  public TranslateAnimation(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TranslateAnimation);
    Animation.Description description = Animation.Description.parseValue(typedArray.peekValue(0));
    this.mFromXType = description.type;
    this.mFromXValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(1));
    this.mToXType = description.type;
    this.mToXValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(2));
    this.mFromYType = description.type;
    this.mFromYValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(3));
    this.mToYType = description.type;
    this.mToYValue = description.value;
    typedArray.recycle();
  }
  
  public TranslateAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.mFromXValue = paramFloat1;
    this.mToXValue = paramFloat2;
    this.mFromYValue = paramFloat3;
    this.mToYValue = paramFloat4;
    this.mFromXType = 0;
    this.mToXType = 0;
    this.mFromYType = 0;
    this.mToYType = 0;
  }
  
  public TranslateAnimation(int paramInt1, float paramFloat1, int paramInt2, float paramFloat2, int paramInt3, float paramFloat3, int paramInt4, float paramFloat4) {
    this.mFromXValue = paramFloat1;
    this.mToXValue = paramFloat2;
    this.mFromYValue = paramFloat3;
    this.mToYValue = paramFloat4;
    this.mFromXType = paramInt1;
    this.mToXType = paramInt2;
    this.mFromYType = paramInt3;
    this.mToYType = paramInt4;
  }
  
  protected void applyTransformation(float paramFloat, Transformation paramTransformation) {
    float f1 = this.mFromXDelta;
    float f2 = this.mFromYDelta;
    float f3 = this.mFromXDelta, f4 = this.mToXDelta;
    if (f3 != f4)
      f1 = f3 + (f4 - f3) * paramFloat; 
    f4 = this.mFromYDelta;
    f3 = this.mToYDelta;
    if (f4 != f3)
      f2 = f4 + (f3 - f4) * paramFloat; 
    paramTransformation.getMatrix().setTranslate(f1, f2);
  }
  
  public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mFromXDelta = resolveSize(this.mFromXType, this.mFromXValue, paramInt1, paramInt3);
    this.mToXDelta = resolveSize(this.mToXType, this.mToXValue, paramInt1, paramInt3);
    this.mFromYDelta = resolveSize(this.mFromYType, this.mFromYValue, paramInt2, paramInt4);
    this.mToYDelta = resolveSize(this.mToYType, this.mToYValue, paramInt2, paramInt4);
  }
  
  public float getFromXValue() {
    return this.mFromXValue;
  }
  
  public void setFromXValue(float paramFloat) {
    this.mFromXValue = paramFloat;
  }
  
  public float getToXValue() {
    return this.mToXValue;
  }
}
