package android.view.animation;

import android.content.Context;
import android.util.AttributeSet;

public class OplusAccelerateDecelerateInterpolator extends BaseInterpolator {
  private static final boolean DEBUG = false;
  
  private static final String LOG_TAG = "OplusAccelerateDecelerateInterpolator";
  
  public OplusAccelerateDecelerateInterpolator() {}
  
  public OplusAccelerateDecelerateInterpolator(Context paramContext, AttributeSet paramAttributeSet) {}
  
  public float getInterpolation(float paramFloat) {
    if (paramFloat < 0.5F) {
      paramFloat = (float)((Math.cos((Math.sin(paramFloat * Math.PI / 2.0D) + 1.0D) * Math.PI) + 1.0D) / 2.0D);
    } else {
      paramFloat = (float)((Math.cos((Math.sqrt(paramFloat) + 1.0D) * Math.PI) + 1.0D) / 2.0D);
    } 
    return paramFloat;
  }
}
