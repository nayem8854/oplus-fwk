package android.view.animation;

import android.content.Context;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;

@HasNativeInterpolator
public class AccelerateDecelerateInterpolator extends BaseInterpolator implements NativeInterpolator {
  public AccelerateDecelerateInterpolator() {}
  
  public AccelerateDecelerateInterpolator(Context paramContext, AttributeSet paramAttributeSet) {}
  
  public float getInterpolation(float paramFloat) {
    return (float)(Math.cos((1.0F + paramFloat) * Math.PI) / 2.0D) + 0.5F;
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createAccelerateDecelerateInterpolator();
  }
}
