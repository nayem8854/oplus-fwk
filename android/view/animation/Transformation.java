package android.view.animation;

import android.graphics.Matrix;
import android.graphics.Rect;
import java.io.PrintWriter;

public class Transformation {
  public static final int TYPE_ALPHA = 1;
  
  public static final int TYPE_BOTH = 3;
  
  public static final int TYPE_IDENTITY = 0;
  
  public static final int TYPE_MATRIX = 2;
  
  protected float mAlpha;
  
  private Rect mClipRect = new Rect();
  
  private boolean mHasClipRect;
  
  protected Matrix mMatrix;
  
  protected int mTransformationType;
  
  public Transformation() {
    clear();
  }
  
  public void clear() {
    Matrix matrix = this.mMatrix;
    if (matrix == null) {
      this.mMatrix = new Matrix();
    } else {
      matrix.reset();
    } 
    this.mClipRect.setEmpty();
    this.mHasClipRect = false;
    this.mAlpha = 1.0F;
    this.mTransformationType = 3;
  }
  
  public int getTransformationType() {
    return this.mTransformationType;
  }
  
  public void setTransformationType(int paramInt) {
    this.mTransformationType = paramInt;
  }
  
  public void set(Transformation paramTransformation) {
    this.mAlpha = paramTransformation.getAlpha();
    this.mMatrix.set(paramTransformation.getMatrix());
    if (paramTransformation.mHasClipRect) {
      setClipRect(paramTransformation.getClipRect());
    } else {
      this.mHasClipRect = false;
      this.mClipRect.setEmpty();
    } 
    this.mTransformationType = paramTransformation.getTransformationType();
  }
  
  public void compose(Transformation paramTransformation) {
    this.mAlpha *= paramTransformation.getAlpha();
    this.mMatrix.preConcat(paramTransformation.getMatrix());
    if (paramTransformation.mHasClipRect) {
      Rect rect = paramTransformation.getClipRect();
      if (this.mHasClipRect) {
        setClipRect(this.mClipRect.left + rect.left, this.mClipRect.top + rect.top, this.mClipRect.right + rect.right, this.mClipRect.bottom + rect.bottom);
      } else {
        setClipRect(rect);
      } 
    } 
  }
  
  public void postCompose(Transformation paramTransformation) {
    this.mAlpha *= paramTransformation.getAlpha();
    this.mMatrix.postConcat(paramTransformation.getMatrix());
    if (paramTransformation.mHasClipRect) {
      Rect rect = paramTransformation.getClipRect();
      if (this.mHasClipRect) {
        setClipRect(this.mClipRect.left + rect.left, this.mClipRect.top + rect.top, this.mClipRect.right + rect.right, this.mClipRect.bottom + rect.bottom);
      } else {
        setClipRect(rect);
      } 
    } 
  }
  
  public Matrix getMatrix() {
    return this.mMatrix;
  }
  
  public void setAlpha(float paramFloat) {
    this.mAlpha = paramFloat;
  }
  
  public void setClipRect(Rect paramRect) {
    setClipRect(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public void setClipRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mClipRect.set(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mHasClipRect = true;
  }
  
  public Rect getClipRect() {
    return this.mClipRect;
  }
  
  public boolean hasClipRect() {
    return this.mHasClipRect;
  }
  
  public float getAlpha() {
    return this.mAlpha;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(64);
    stringBuilder.append("Transformation");
    toShortString(stringBuilder);
    return stringBuilder.toString();
  }
  
  public String toShortString() {
    StringBuilder stringBuilder = new StringBuilder(64);
    toShortString(stringBuilder);
    return stringBuilder.toString();
  }
  
  public void toShortString(StringBuilder paramStringBuilder) {
    paramStringBuilder.append("{alpha=");
    paramStringBuilder.append(this.mAlpha);
    paramStringBuilder.append(" matrix=");
    this.mMatrix.toShortString(paramStringBuilder);
    paramStringBuilder.append('}');
  }
  
  public void printShortString(PrintWriter paramPrintWriter) {
    paramPrintWriter.print("{alpha=");
    paramPrintWriter.print(this.mAlpha);
    paramPrintWriter.print(" matrix=");
    this.mMatrix.printShortString(paramPrintWriter);
    paramPrintWriter.print('}');
  }
}
