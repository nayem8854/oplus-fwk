package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.android.internal.R;

public class ScaleAnimation extends Animation {
  private int mFromXType = 0;
  
  private int mToXType = 0;
  
  private int mFromYType = 0;
  
  private int mToYType = 0;
  
  private int mFromXData = 0;
  
  private int mToXData = 0;
  
  private int mFromYData = 0;
  
  private int mToYData = 0;
  
  private int mPivotXType = 0;
  
  private int mPivotYType = 0;
  
  private float mPivotXValue = 0.0F;
  
  private float mPivotYValue = 0.0F;
  
  private float mFromX;
  
  private float mFromY;
  
  private float mPivotX;
  
  private float mPivotY;
  
  private final Resources mResources;
  
  private float mToX;
  
  private float mToY;
  
  public ScaleAnimation(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    this.mResources = paramContext.getResources();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ScaleAnimation);
    TypedValue typedValue = typedArray.peekValue(2);
    this.mFromX = 0.0F;
    if (typedValue != null)
      if (typedValue.type == 4) {
        this.mFromX = typedValue.getFloat();
      } else {
        this.mFromXType = typedValue.type;
        this.mFromXData = typedValue.data;
      }  
    typedValue = typedArray.peekValue(3);
    this.mToX = 0.0F;
    if (typedValue != null)
      if (typedValue.type == 4) {
        this.mToX = typedValue.getFloat();
      } else {
        this.mToXType = typedValue.type;
        this.mToXData = typedValue.data;
      }  
    typedValue = typedArray.peekValue(4);
    this.mFromY = 0.0F;
    if (typedValue != null)
      if (typedValue.type == 4) {
        this.mFromY = typedValue.getFloat();
      } else {
        this.mFromYType = typedValue.type;
        this.mFromYData = typedValue.data;
      }  
    typedValue = typedArray.peekValue(5);
    this.mToY = 0.0F;
    if (typedValue != null)
      if (typedValue.type == 4) {
        this.mToY = typedValue.getFloat();
      } else {
        this.mToYType = typedValue.type;
        this.mToYData = typedValue.data;
      }  
    Animation.Description description = Animation.Description.parseValue(typedArray.peekValue(0));
    this.mPivotXType = description.type;
    this.mPivotXValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(1));
    this.mPivotYType = description.type;
    this.mPivotYValue = description.value;
    typedArray.recycle();
    initializePivotPoint();
  }
  
  public ScaleAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.mResources = null;
    this.mFromX = paramFloat1;
    this.mToX = paramFloat2;
    this.mFromY = paramFloat3;
    this.mToY = paramFloat4;
    this.mPivotX = 0.0F;
    this.mPivotY = 0.0F;
  }
  
  public ScaleAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
    this.mResources = null;
    this.mFromX = paramFloat1;
    this.mToX = paramFloat2;
    this.mFromY = paramFloat3;
    this.mToY = paramFloat4;
    this.mPivotXType = 0;
    this.mPivotYType = 0;
    this.mPivotXValue = paramFloat5;
    this.mPivotYValue = paramFloat6;
    initializePivotPoint();
  }
  
  public ScaleAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt1, float paramFloat5, int paramInt2, float paramFloat6) {
    this.mResources = null;
    this.mFromX = paramFloat1;
    this.mToX = paramFloat2;
    this.mFromY = paramFloat3;
    this.mToY = paramFloat4;
    this.mPivotXValue = paramFloat5;
    this.mPivotXType = paramInt1;
    this.mPivotYValue = paramFloat6;
    this.mPivotYType = paramInt2;
    initializePivotPoint();
  }
  
  private void initializePivotPoint() {
    if (this.mPivotXType == 0)
      this.mPivotX = this.mPivotXValue; 
    if (this.mPivotYType == 0)
      this.mPivotY = this.mPivotYValue; 
  }
  
  protected void applyTransformation(float paramFloat, Transformation paramTransformation) {
    float f1 = 1.0F;
    float f2 = 1.0F;
    float f3 = getScaleFactor();
    if (this.mFromX != 1.0F || this.mToX != 1.0F) {
      f1 = this.mFromX;
      f1 += (this.mToX - f1) * paramFloat;
    } 
    if (this.mFromY != 1.0F || this.mToY != 1.0F) {
      f2 = this.mFromY;
      f2 += (this.mToY - f2) * paramFloat;
    } 
    if (this.mPivotX == 0.0F && this.mPivotY == 0.0F) {
      paramTransformation.getMatrix().setScale(f1, f2);
    } else {
      paramTransformation.getMatrix().setScale(f1, f2, this.mPivotX * f3, this.mPivotY * f3);
    } 
  }
  
  float resolveScale(float paramFloat, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt1 == 6) {
      paramFloat = TypedValue.complexToFraction(paramInt2, paramInt3, paramInt4);
    } else if (paramInt1 == 5) {
      paramFloat = TypedValue.complexToDimension(paramInt2, this.mResources.getDisplayMetrics());
    } else {
      return paramFloat;
    } 
    if (paramInt3 == 0)
      return 1.0F; 
    return paramFloat / paramInt3;
  }
  
  public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mFromX = resolveScale(this.mFromX, this.mFromXType, this.mFromXData, paramInt1, paramInt3);
    this.mToX = resolveScale(this.mToX, this.mToXType, this.mToXData, paramInt1, paramInt3);
    this.mFromY = resolveScale(this.mFromY, this.mFromYType, this.mFromYData, paramInt2, paramInt4);
    this.mToY = resolveScale(this.mToY, this.mToYType, this.mToYData, paramInt2, paramInt4);
    this.mPivotX = resolveSize(this.mPivotXType, this.mPivotXValue, paramInt1, paramInt3);
    this.mPivotY = resolveSize(this.mPivotYType, this.mPivotYValue, paramInt2, paramInt4);
  }
}
