package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R;

public class AlphaAnimation extends Animation {
  private float mFromAlpha;
  
  private float mToAlpha;
  
  public AlphaAnimation(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    int[] arrayOfInt = R.styleable.AlphaAnimation;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt);
    this.mFromAlpha = typedArray.getFloat(0, 1.0F);
    this.mToAlpha = typedArray.getFloat(1, 1.0F);
    typedArray.recycle();
  }
  
  public AlphaAnimation(float paramFloat1, float paramFloat2) {
    this.mFromAlpha = paramFloat1;
    this.mToAlpha = paramFloat2;
  }
  
  protected void applyTransformation(float paramFloat, Transformation paramTransformation) {
    float f = this.mFromAlpha;
    paramTransformation.setAlpha((this.mToAlpha - f) * paramFloat + f);
  }
  
  public boolean willChangeTransformationMatrix() {
    return false;
  }
  
  public boolean willChangeBounds() {
    return false;
  }
  
  public boolean hasAlpha() {
    return true;
  }
  
  public float getFromAlpha() {
    return this.mFromAlpha;
  }
  
  public void setFromAlpha(float paramFloat) {
    this.mFromAlpha = paramFloat;
  }
  
  public float getToAlpha() {
    return this.mToAlpha;
  }
}
