package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;

public class OplusBezierInterpolator extends BaseInterpolator {
  private final String TAG = "OplusBezierInterpolator";
  
  private final boolean DEBUG = false;
  
  private final double EPSILON = 6.25E-5D;
  
  private final float ABOVE_ONE = 1.0F;
  
  private final float BELOW_ONE = 0.9999F;
  
  private final float ABOVE_ZERO = 1.0E-4F;
  
  private boolean mAbove = false;
  
  private boolean mLimit = false;
  
  private OplusUnitBezier mOplusUnitBezier;
  
  public OplusBezierInterpolator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext.getResources(), paramContext.getTheme(), paramAttributeSet);
  }
  
  public OplusBezierInterpolator(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, boolean paramBoolean) {
    this.mLimit = paramBoolean;
    this.mOplusUnitBezier = new OplusUnitBezier(paramDouble1, paramDouble2, paramDouble3, paramDouble4);
  }
  
  public float getInterpolation(float paramFloat) {
    double d1 = this.mOplusUnitBezier.solve(paramFloat, 6.25E-5D);
    double d2 = d1;
    if (this.mLimit) {
      if (paramFloat < 1.0E-4F || paramFloat > 0.9999F)
        this.mAbove = false; 
      d2 = d1;
      if (d1 > 1.0D) {
        d2 = d1;
        if (!this.mAbove) {
          d2 = 1.0D;
          this.mAbove = true;
        } 
      } 
      if (this.mAbove)
        d2 = 1.0D; 
    } 
    return (float)d2;
  }
  
  public OplusBezierInterpolator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {}
}
