package android.view.animation;

import android.content.res.Resources;
import android.util.AttributeSet;

public class OplusAnimationUtils {
  public static BaseInterpolator createInterpolatorFromXml(String paramString, Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    OplusBezierInterpolator oplusBezierInterpolator;
    OplusDecelerateInterpolator oplusDecelerateInterpolator = null;
    if (paramString.equals("oplusDecelerateInterpolator")) {
      oplusDecelerateInterpolator = new OplusDecelerateInterpolator();
    } else if (paramString.equals("oplusAccelerateDecelerateInterpolator")) {
      OplusAccelerateDecelerateInterpolator oplusAccelerateDecelerateInterpolator = new OplusAccelerateDecelerateInterpolator();
    } else if (paramString.equals("oplusBezierInterpolator")) {
      oplusBezierInterpolator = new OplusBezierInterpolator(paramResources, paramTheme, paramAttributeSet);
    } 
    return oplusBezierInterpolator;
  }
}
