package android.view.animation;

import android.content.Context;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;

@HasNativeInterpolator
public class BounceInterpolator extends BaseInterpolator implements NativeInterpolator {
  public BounceInterpolator() {}
  
  public BounceInterpolator(Context paramContext, AttributeSet paramAttributeSet) {}
  
  private static float bounce(float paramFloat) {
    return paramFloat * paramFloat * 8.0F;
  }
  
  public float getInterpolation(float paramFloat) {
    paramFloat *= 1.1226F;
    if (paramFloat < 0.3535F)
      return bounce(paramFloat); 
    if (paramFloat < 0.7408F)
      return bounce(paramFloat - 0.54719F) + 0.7F; 
    if (paramFloat < 0.9644F)
      return bounce(paramFloat - 0.8526F) + 0.9F; 
    return bounce(paramFloat - 1.0435F) + 0.95F;
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createBounceInterpolator();
  }
}
