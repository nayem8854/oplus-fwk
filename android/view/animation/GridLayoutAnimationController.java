package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.Random;

public class GridLayoutAnimationController extends LayoutAnimationController {
  public static final int DIRECTION_BOTTOM_TO_TOP = 2;
  
  public static final int DIRECTION_HORIZONTAL_MASK = 1;
  
  public static final int DIRECTION_LEFT_TO_RIGHT = 0;
  
  public static final int DIRECTION_RIGHT_TO_LEFT = 1;
  
  public static final int DIRECTION_TOP_TO_BOTTOM = 0;
  
  public static final int DIRECTION_VERTICAL_MASK = 2;
  
  public static final int PRIORITY_COLUMN = 1;
  
  public static final int PRIORITY_NONE = 0;
  
  public static final int PRIORITY_ROW = 2;
  
  private float mColumnDelay;
  
  private int mDirection;
  
  private int mDirectionPriority;
  
  private float mRowDelay;
  
  public GridLayoutAnimationController(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.GridLayoutAnimation);
    TypedValue typedValue2 = typedArray.peekValue(0);
    Animation.Description description2 = Animation.Description.parseValue(typedValue2);
    this.mColumnDelay = description2.value;
    TypedValue typedValue1 = typedArray.peekValue(1);
    Animation.Description description1 = Animation.Description.parseValue(typedValue1);
    this.mRowDelay = description1.value;
    this.mDirection = typedArray.getInt(2, 0);
    this.mDirectionPriority = typedArray.getInt(3, 0);
    typedArray.recycle();
  }
  
  public GridLayoutAnimationController(Animation paramAnimation) {
    this(paramAnimation, 0.5F, 0.5F);
  }
  
  public GridLayoutAnimationController(Animation paramAnimation, float paramFloat1, float paramFloat2) {
    super(paramAnimation);
    this.mColumnDelay = paramFloat1;
    this.mRowDelay = paramFloat2;
  }
  
  public float getColumnDelay() {
    return this.mColumnDelay;
  }
  
  public void setColumnDelay(float paramFloat) {
    this.mColumnDelay = paramFloat;
  }
  
  public float getRowDelay() {
    return this.mRowDelay;
  }
  
  public void setRowDelay(float paramFloat) {
    this.mRowDelay = paramFloat;
  }
  
  public int getDirection() {
    return this.mDirection;
  }
  
  public void setDirection(int paramInt) {
    this.mDirection = paramInt;
  }
  
  public int getDirectionPriority() {
    return this.mDirectionPriority;
  }
  
  public void setDirectionPriority(int paramInt) {
    this.mDirectionPriority = paramInt;
  }
  
  public boolean willOverlap() {
    return (this.mColumnDelay < 1.0F || this.mRowDelay < 1.0F);
  }
  
  protected long getDelayForView(View paramView) {
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    AnimationParameters animationParameters = (AnimationParameters)layoutParams.layoutAnimationParameters;
    if (animationParameters == null)
      return 0L; 
    int i = getTransformedColumnIndex(animationParameters);
    int j = getTransformedRowIndex(animationParameters);
    int k = animationParameters.rowsCount;
    int m = animationParameters.columnsCount;
    long l = this.mAnimation.getDuration();
    float f1 = this.mColumnDelay * (float)l;
    float f2 = this.mRowDelay * (float)l;
    if (this.mInterpolator == null)
      this.mInterpolator = new LinearInterpolator(); 
    int n = this.mDirectionPriority;
    if (n != 1) {
      if (n != 2) {
        l = (long)(i * f1 + j * f2);
        f1 = m * f1 + k * f2;
      } else {
        l = (long)(i * f1 + (j * m) * f1);
        f1 = m * f1 + (k * m) * f1;
      } 
    } else {
      l = (long)(j * f2 + (i * k) * f2);
      f1 = k * f2 + (m * k) * f2;
    } 
    f2 = (float)l / f1;
    f2 = this.mInterpolator.getInterpolation(f2);
    return (long)(f2 * f1);
  }
  
  private int getTransformedColumnIndex(AnimationParameters paramAnimationParameters) {
    int i = getOrder();
    if (i != 1) {
      if (i != 2) {
        i = paramAnimationParameters.column;
      } else {
        if (this.mRandomizer == null)
          this.mRandomizer = new Random(); 
        i = (int)(paramAnimationParameters.columnsCount * this.mRandomizer.nextFloat());
      } 
    } else {
      i = paramAnimationParameters.columnsCount - 1 - paramAnimationParameters.column;
    } 
    int j = this.mDirection;
    int k = i;
    if ((j & 0x1) == 1)
      k = paramAnimationParameters.columnsCount - 1 - i; 
    return k;
  }
  
  private int getTransformedRowIndex(AnimationParameters paramAnimationParameters) {
    int i = getOrder();
    if (i != 1) {
      if (i != 2) {
        i = paramAnimationParameters.row;
      } else {
        if (this.mRandomizer == null)
          this.mRandomizer = new Random(); 
        i = (int)(paramAnimationParameters.rowsCount * this.mRandomizer.nextFloat());
      } 
    } else {
      i = paramAnimationParameters.rowsCount - 1 - paramAnimationParameters.row;
    } 
    int j = this.mDirection;
    int k = i;
    if ((j & 0x2) == 2)
      k = paramAnimationParameters.rowsCount - 1 - i; 
    return k;
  }
  
  public static class AnimationParameters extends LayoutAnimationController.AnimationParameters {
    public int column;
    
    public int columnsCount;
    
    public int row;
    
    public int rowsCount;
  }
}
