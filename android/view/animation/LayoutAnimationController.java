package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.Random;

public class LayoutAnimationController {
  public static final int ORDER_NORMAL = 0;
  
  public static final int ORDER_RANDOM = 2;
  
  public static final int ORDER_REVERSE = 1;
  
  protected Animation mAnimation;
  
  private float mDelay;
  
  private long mDuration;
  
  protected Interpolator mInterpolator;
  
  private long mMaxDelay;
  
  private int mOrder;
  
  protected Random mRandomizer;
  
  public LayoutAnimationController(Context paramContext, AttributeSet paramAttributeSet) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LayoutAnimation);
    TypedValue typedValue = typedArray.peekValue(1);
    Animation.Description description = Animation.Description.parseValue(typedValue);
    this.mDelay = description.value;
    this.mOrder = typedArray.getInt(3, 0);
    int i = typedArray.getResourceId(2, 0);
    if (i > 0)
      setAnimation(paramContext, i); 
    i = typedArray.getResourceId(0, 0);
    if (i > 0)
      setInterpolator(paramContext, i); 
    typedArray.recycle();
  }
  
  public LayoutAnimationController(Animation paramAnimation) {
    this(paramAnimation, 0.5F);
  }
  
  public LayoutAnimationController(Animation paramAnimation, float paramFloat) {
    this.mDelay = paramFloat;
    setAnimation(paramAnimation);
  }
  
  public int getOrder() {
    return this.mOrder;
  }
  
  public void setOrder(int paramInt) {
    this.mOrder = paramInt;
  }
  
  public void setAnimation(Context paramContext, int paramInt) {
    setAnimation(AnimationUtils.loadAnimation(paramContext, paramInt));
  }
  
  public void setAnimation(Animation paramAnimation) {
    this.mAnimation = paramAnimation;
    paramAnimation.setFillBefore(true);
  }
  
  public Animation getAnimation() {
    return this.mAnimation;
  }
  
  public void setInterpolator(Context paramContext, int paramInt) {
    setInterpolator(AnimationUtils.loadInterpolator(paramContext, paramInt));
  }
  
  public void setInterpolator(Interpolator paramInterpolator) {
    this.mInterpolator = paramInterpolator;
  }
  
  public Interpolator getInterpolator() {
    return this.mInterpolator;
  }
  
  public float getDelay() {
    return this.mDelay;
  }
  
  public void setDelay(float paramFloat) {
    this.mDelay = paramFloat;
  }
  
  public boolean willOverlap() {
    boolean bool;
    if (this.mDelay < 1.0F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void start() {
    this.mDuration = this.mAnimation.getDuration();
    this.mMaxDelay = Long.MIN_VALUE;
    this.mAnimation.setStartTime(-1L);
  }
  
  public final Animation getAnimationForView(View paramView) {
    long l = getDelayForView(paramView) + this.mAnimation.getStartOffset();
    this.mMaxDelay = Math.max(this.mMaxDelay, l);
    try {
      Animation animation = this.mAnimation.clone();
      animation.setStartOffset(l);
      return animation;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      return null;
    } 
  }
  
  public boolean isDone() {
    boolean bool;
    long l = AnimationUtils.currentAnimationTimeMillis();
    Animation animation = this.mAnimation;
    if (l > animation.getStartTime() + this.mMaxDelay + this.mDuration) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected long getDelayForView(View paramView) {
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    AnimationParameters animationParameters = layoutParams.layoutAnimationParameters;
    if (animationParameters == null)
      return 0L; 
    float f1 = this.mDelay * (float)this.mAnimation.getDuration();
    long l = (long)(getTransformedIndex(animationParameters) * f1);
    f1 = animationParameters.count * f1;
    if (this.mInterpolator == null)
      this.mInterpolator = new LinearInterpolator(); 
    float f2 = (float)l / f1;
    f2 = this.mInterpolator.getInterpolation(f2);
    return (long)(f2 * f1);
  }
  
  protected int getTransformedIndex(AnimationParameters paramAnimationParameters) {
    int i = getOrder();
    if (i != 1) {
      if (i != 2)
        return paramAnimationParameters.index; 
      if (this.mRandomizer == null)
        this.mRandomizer = new Random(); 
      return (int)(paramAnimationParameters.count * this.mRandomizer.nextFloat());
    } 
    return paramAnimationParameters.count - 1 - paramAnimationParameters.index;
  }
  
  public static class AnimationParameters {
    public int count;
    
    public int index;
  }
}
