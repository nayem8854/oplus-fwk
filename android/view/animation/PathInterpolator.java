package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Path;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;
import android.util.PathParser;
import android.view.InflateException;
import com.android.internal.R;

@HasNativeInterpolator
public class PathInterpolator extends BaseInterpolator implements NativeInterpolator {
  private static final float PRECISION = 0.002F;
  
  private float[] mX;
  
  private float[] mY;
  
  public PathInterpolator(Path paramPath) {
    initPath(paramPath);
  }
  
  public PathInterpolator(float paramFloat1, float paramFloat2) {
    initQuad(paramFloat1, paramFloat2);
  }
  
  public PathInterpolator(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    initCubic(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
  }
  
  public PathInterpolator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext.getResources(), paramContext.getTheme(), paramAttributeSet);
  }
  
  public PathInterpolator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    TypedArray typedArray;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.PathInterpolator, 0, 0);
    } else {
      typedArray = typedArray.obtainAttributes(paramAttributeSet, R.styleable.PathInterpolator);
    } 
    parseInterpolatorFromTypeArray(typedArray);
    setChangingConfiguration(typedArray.getChangingConfigurations());
    typedArray.recycle();
  }
  
  private void parseInterpolatorFromTypeArray(TypedArray paramTypedArray) {
    String str;
    if (paramTypedArray.hasValue(4)) {
      str = paramTypedArray.getString(4);
      Path path = PathParser.createPathFromPathData(str);
      if (path != null) {
        initPath(path);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("The path is null, which is created from ");
        stringBuilder.append(str);
        throw new InflateException(stringBuilder.toString());
      } 
    } else {
      if (str.hasValue(0)) {
        if (str.hasValue(1)) {
          float f1 = str.getFloat(0, 0.0F);
          float f2 = str.getFloat(1, 0.0F);
          boolean bool1 = str.hasValue(2);
          boolean bool2 = str.hasValue(3);
          if (bool1 == bool2) {
            if (!bool1) {
              initQuad(f1, f2);
            } else {
              float f3 = str.getFloat(2, 0.0F);
              float f4 = str.getFloat(3, 0.0F);
              initCubic(f1, f2, f3, f4);
            } 
            return;
          } 
          throw new InflateException("pathInterpolator requires both controlX2 and controlY2 for cubic Beziers.");
        } 
        throw new InflateException("pathInterpolator requires the controlY1 attribute");
      } 
      throw new InflateException("pathInterpolator requires the controlX1 attribute");
    } 
  }
  
  private void initQuad(float paramFloat1, float paramFloat2) {
    Path path = new Path();
    path.moveTo(0.0F, 0.0F);
    path.quadTo(paramFloat1, paramFloat2, 1.0F, 1.0F);
    initPath(path);
  }
  
  private void initCubic(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    Path path = new Path();
    path.moveTo(0.0F, 0.0F);
    path.cubicTo(paramFloat1, paramFloat2, paramFloat3, paramFloat4, 1.0F, 1.0F);
    initPath(path);
  }
  
  private void initPath(Path paramPath) {
    float[] arrayOfFloat = paramPath.approximate(0.002F);
    int i = arrayOfFloat.length / 3;
    if (arrayOfFloat[1] == 0.0F && arrayOfFloat[2] == 0.0F && arrayOfFloat[arrayOfFloat.length - 2] == 1.0F && arrayOfFloat[arrayOfFloat.length - 1] == 1.0F) {
      this.mX = new float[i];
      this.mY = new float[i];
      float f1 = 0.0F;
      float f2 = 0.0F;
      int j = 0;
      for (byte b = 0; b < i; ) {
        int k = j + 1;
        float f3 = arrayOfFloat[j];
        j = k + 1;
        float f4 = arrayOfFloat[k];
        float f5 = arrayOfFloat[j];
        if (f3 != f2 || f4 == f1) {
          if (f4 >= f1) {
            this.mX[b] = f4;
            this.mY[b] = f5;
            f1 = f4;
            f2 = f3;
            b++;
            j++;
          } 
          throw new IllegalArgumentException("The Path cannot loop back on itself.");
        } 
        throw new IllegalArgumentException("The Path cannot have discontinuity in the X axis.");
      } 
      return;
    } 
    throw new IllegalArgumentException("The Path must start at (0,0) and end at (1,1)");
  }
  
  public float getInterpolation(float paramFloat) {
    if (paramFloat <= 0.0F)
      return 0.0F; 
    if (paramFloat >= 1.0F)
      return 1.0F; 
    int i = 0;
    int j = this.mX.length - 1;
    while (j - i > 1) {
      int k = (i + j) / 2;
      if (paramFloat < this.mX[k]) {
        j = k;
        continue;
      } 
      i = k;
    } 
    float arrayOfFloat[] = this.mX, f1 = arrayOfFloat[j] - arrayOfFloat[i];
    if (f1 == 0.0F)
      return this.mY[i]; 
    float f2 = arrayOfFloat[i];
    f1 = (paramFloat - f2) / f1;
    arrayOfFloat = this.mY;
    paramFloat = arrayOfFloat[i];
    f2 = arrayOfFloat[j];
    return (f2 - paramFloat) * f1 + paramFloat;
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createPathInterpolator(this.mX, this.mY);
  }
}
