package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;
import com.android.internal.R;

@HasNativeInterpolator
public class AnticipateOvershootInterpolator extends BaseInterpolator implements NativeInterpolator {
  private final float mTension;
  
  public AnticipateOvershootInterpolator() {
    this.mTension = 3.0F;
  }
  
  public AnticipateOvershootInterpolator(float paramFloat) {
    this.mTension = 1.5F * paramFloat;
  }
  
  public AnticipateOvershootInterpolator(float paramFloat1, float paramFloat2) {
    this.mTension = paramFloat1 * paramFloat2;
  }
  
  public AnticipateOvershootInterpolator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext.getResources(), paramContext.getTheme(), paramAttributeSet);
  }
  
  public AnticipateOvershootInterpolator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    TypedArray typedArray;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.AnticipateOvershootInterpolator, 0, 0);
    } else {
      typedArray = typedArray.obtainAttributes(paramAttributeSet, R.styleable.AnticipateOvershootInterpolator);
    } 
    float f = typedArray.getFloat(0, 2.0F);
    this.mTension = f * typedArray.getFloat(1, 1.5F);
    setChangingConfiguration(typedArray.getChangingConfigurations());
    typedArray.recycle();
  }
  
  private static float a(float paramFloat1, float paramFloat2) {
    return paramFloat1 * paramFloat1 * ((1.0F + paramFloat2) * paramFloat1 - paramFloat2);
  }
  
  private static float o(float paramFloat1, float paramFloat2) {
    return paramFloat1 * paramFloat1 * ((1.0F + paramFloat2) * paramFloat1 + paramFloat2);
  }
  
  public float getInterpolation(float paramFloat) {
    if (paramFloat < 0.5F)
      return a(2.0F * paramFloat, this.mTension) * 0.5F; 
    return (o(paramFloat * 2.0F - 2.0F, this.mTension) + 2.0F) * 0.5F;
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createAnticipateOvershootInterpolator(this.mTension);
  }
}
