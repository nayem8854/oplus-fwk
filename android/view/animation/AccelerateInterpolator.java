package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;
import com.android.internal.R;

@HasNativeInterpolator
public class AccelerateInterpolator extends BaseInterpolator implements NativeInterpolator {
  private final double mDoubleFactor;
  
  private final float mFactor;
  
  public AccelerateInterpolator() {
    this.mFactor = 1.0F;
    this.mDoubleFactor = 2.0D;
  }
  
  public AccelerateInterpolator(float paramFloat) {
    this.mFactor = paramFloat;
    this.mDoubleFactor = (2.0F * paramFloat);
  }
  
  public AccelerateInterpolator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext.getResources(), paramContext.getTheme(), paramAttributeSet);
  }
  
  public AccelerateInterpolator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    TypedArray typedArray;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.AccelerateInterpolator, 0, 0);
    } else {
      typedArray = typedArray.obtainAttributes(paramAttributeSet, R.styleable.AccelerateInterpolator);
    } 
    float f = typedArray.getFloat(0, 1.0F);
    this.mDoubleFactor = (f * 2.0F);
    setChangingConfiguration(typedArray.getChangingConfigurations());
    typedArray.recycle();
  }
  
  public float getInterpolation(float paramFloat) {
    if (this.mFactor == 1.0F)
      return paramFloat * paramFloat; 
    return (float)Math.pow(paramFloat, this.mDoubleFactor);
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createAccelerateInterpolator(this.mFactor);
  }
}
