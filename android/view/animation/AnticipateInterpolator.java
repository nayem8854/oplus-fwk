package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;
import com.android.internal.R;

@HasNativeInterpolator
public class AnticipateInterpolator extends BaseInterpolator implements NativeInterpolator {
  private final float mTension;
  
  public AnticipateInterpolator() {
    this.mTension = 2.0F;
  }
  
  public AnticipateInterpolator(float paramFloat) {
    this.mTension = paramFloat;
  }
  
  public AnticipateInterpolator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext.getResources(), paramContext.getTheme(), paramAttributeSet);
  }
  
  public AnticipateInterpolator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    TypedArray typedArray;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.AnticipateInterpolator, 0, 0);
    } else {
      typedArray = typedArray.obtainAttributes(paramAttributeSet, R.styleable.AnticipateInterpolator);
    } 
    this.mTension = typedArray.getFloat(0, 2.0F);
    setChangingConfiguration(typedArray.getChangingConfigurations());
    typedArray.recycle();
  }
  
  public float getInterpolation(float paramFloat) {
    float f = this.mTension;
    return paramFloat * paramFloat * ((1.0F + f) * paramFloat - f);
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createAnticipateInterpolator(this.mTension);
  }
}
