package android.view.animation;

import android.graphics.Matrix;

public class TranslateYAnimation extends TranslateAnimation {
  float[] mTmpValues = new float[9];
  
  public TranslateYAnimation(float paramFloat1, float paramFloat2) {
    super(0.0F, 0.0F, paramFloat1, paramFloat2);
  }
  
  public TranslateYAnimation(int paramInt1, float paramFloat1, int paramInt2, float paramFloat2) {
    super(0, 0.0F, 0, 0.0F, paramInt1, paramFloat1, paramInt2, paramFloat2);
  }
  
  protected void applyTransformation(float paramFloat, Transformation paramTransformation) {
    Matrix matrix = paramTransformation.getMatrix();
    matrix.getValues(this.mTmpValues);
    float f1 = this.mFromYDelta, f2 = this.mToYDelta, f3 = this.mFromYDelta;
    paramTransformation.getMatrix().setTranslate(this.mTmpValues[2], f1 + (f2 - f3) * paramFloat);
  }
}
