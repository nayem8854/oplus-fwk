package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.android.internal.R;

public class ClipRectAnimation extends Animation {
  protected final Rect mFromRect = new Rect();
  
  protected final Rect mToRect = new Rect();
  
  private int mFromLeftType = 0;
  
  private int mFromTopType = 0;
  
  private int mFromRightType = 0;
  
  private int mFromBottomType = 0;
  
  private int mToLeftType = 0;
  
  private int mToTopType = 0;
  
  private int mToRightType = 0;
  
  private int mToBottomType = 0;
  
  private float mFromBottomValue;
  
  private float mFromLeftValue;
  
  private float mFromRightValue;
  
  private float mFromTopValue;
  
  private float mToBottomValue;
  
  private float mToLeftValue;
  
  private float mToRightValue;
  
  private float mToTopValue;
  
  public ClipRectAnimation(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ClipRectAnimation);
    Animation.Description description = Animation.Description.parseValue(typedArray.peekValue(1));
    this.mFromLeftType = description.type;
    this.mFromLeftValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(3));
    this.mFromTopType = description.type;
    this.mFromTopValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(2));
    this.mFromRightType = description.type;
    this.mFromRightValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(0));
    this.mFromBottomType = description.type;
    this.mFromBottomValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(5));
    this.mToLeftType = description.type;
    this.mToLeftValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(7));
    this.mToTopType = description.type;
    this.mToTopValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(6));
    this.mToRightType = description.type;
    this.mToRightValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(4));
    this.mToBottomType = description.type;
    this.mToBottomValue = description.value;
    typedArray.recycle();
  }
  
  public ClipRectAnimation(Rect paramRect1, Rect paramRect2) {
    if (paramRect1 != null && paramRect2 != null) {
      this.mFromLeftValue = paramRect1.left;
      this.mFromTopValue = paramRect1.top;
      this.mFromRightValue = paramRect1.right;
      this.mFromBottomValue = paramRect1.bottom;
      this.mToLeftValue = paramRect2.left;
      this.mToTopValue = paramRect2.top;
      this.mToRightValue = paramRect2.right;
      this.mToBottomValue = paramRect2.bottom;
      return;
    } 
    throw new RuntimeException("Expected non-null animation clip rects");
  }
  
  public ClipRectAnimation(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    this(new Rect(paramInt1, paramInt2, paramInt3, paramInt4), new Rect(paramInt5, paramInt6, paramInt7, paramInt8));
  }
  
  protected void applyTransformation(float paramFloat, Transformation paramTransformation) {
    int i = this.mFromRect.left, j = (int)((this.mToRect.left - this.mFromRect.left) * paramFloat);
    int k = this.mFromRect.top, m = (int)((this.mToRect.top - this.mFromRect.top) * paramFloat);
    int n = this.mFromRect.right, i1 = (int)((this.mToRect.right - this.mFromRect.right) * paramFloat);
    int i2 = this.mFromRect.bottom, i3 = (int)((this.mToRect.bottom - this.mFromRect.bottom) * paramFloat);
    paramTransformation.setClipRect(i + j, k + m, n + i1, i2 + i3);
  }
  
  public boolean willChangeTransformationMatrix() {
    return false;
  }
  
  public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
    Rect rect = this.mFromRect;
    int i = (int)resolveSize(this.mFromLeftType, this.mFromLeftValue, paramInt1, paramInt3), j = this.mFromTopType;
    float f = this.mFromTopValue;
    j = (int)resolveSize(j, f, paramInt2, paramInt4);
    int k = this.mFromRightType;
    f = this.mFromRightValue;
    k = (int)resolveSize(k, f, paramInt1, paramInt3);
    int m = this.mFromBottomType;
    f = this.mFromBottomValue;
    m = (int)resolveSize(m, f, paramInt2, paramInt4);
    rect.set(i, j, k, m);
    rect = this.mToRect;
    i = (int)resolveSize(this.mToLeftType, this.mToLeftValue, paramInt1, paramInt3);
    j = this.mToTopType;
    f = this.mToTopValue;
    j = (int)resolveSize(j, f, paramInt2, paramInt4);
    k = this.mToRightType;
    f = this.mToRightValue;
    paramInt1 = (int)resolveSize(k, f, paramInt1, paramInt3);
    paramInt3 = this.mToBottomType;
    f = this.mToBottomValue;
    paramInt2 = (int)resolveSize(paramInt3, f, paramInt2, paramInt4);
    rect.set(i, j, paramInt1, paramInt2);
  }
}
