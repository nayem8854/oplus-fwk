package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.List;

public class AnimationSet extends Animation {
  private int mFlags = 0;
  
  private ArrayList<Animation> mAnimations = new ArrayList<>();
  
  private Transformation mTempTransformation = new Transformation();
  
  private static final int PROPERTY_CHANGE_BOUNDS_MASK = 128;
  
  private static final int PROPERTY_DURATION_MASK = 32;
  
  private static final int PROPERTY_FILL_AFTER_MASK = 1;
  
  private static final int PROPERTY_FILL_BEFORE_MASK = 2;
  
  private static final int PROPERTY_MORPH_MATRIX_MASK = 64;
  
  private static final int PROPERTY_REPEAT_MODE_MASK = 4;
  
  private static final int PROPERTY_SHARE_INTERPOLATOR_MASK = 16;
  
  private static final int PROPERTY_START_OFFSET_MASK = 8;
  
  private boolean mDirty;
  
  private boolean mHasAlpha;
  
  private long mLastEnd;
  
  private long[] mStoredOffsets;
  
  public AnimationSet(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    int[] arrayOfInt = R.styleable.AnimationSet;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt);
    boolean bool = typedArray.getBoolean(1, true);
    setFlag(16, bool);
    init();
    if ((paramContext.getApplicationInfo()).targetSdkVersion >= 14) {
      if (typedArray.hasValue(0))
        this.mFlags |= 0x20; 
      if (typedArray.hasValue(2))
        this.mFlags = 0x2 | this.mFlags; 
      if (typedArray.hasValue(3))
        this.mFlags |= 0x1; 
      if (typedArray.hasValue(5))
        this.mFlags |= 0x4; 
      if (typedArray.hasValue(4))
        this.mFlags |= 0x8; 
    } 
    typedArray.recycle();
  }
  
  public AnimationSet(boolean paramBoolean) {
    setFlag(16, paramBoolean);
    init();
  }
  
  protected AnimationSet clone() throws CloneNotSupportedException {
    AnimationSet animationSet = (AnimationSet)super.clone();
    animationSet.mTempTransformation = new Transformation();
    animationSet.mAnimations = new ArrayList<>();
    int i = this.mAnimations.size();
    ArrayList<Animation> arrayList = this.mAnimations;
    for (byte b = 0; b < i; b++)
      animationSet.mAnimations.add(((Animation)arrayList.get(b)).clone()); 
    return animationSet;
  }
  
  private void setFlag(int paramInt, boolean paramBoolean) {
    if (paramBoolean) {
      this.mFlags |= paramInt;
    } else {
      this.mFlags &= paramInt ^ 0xFFFFFFFF;
    } 
  }
  
  private void init() {
    this.mStartTime = 0L;
  }
  
  public void setFillAfter(boolean paramBoolean) {
    this.mFlags |= 0x1;
    super.setFillAfter(paramBoolean);
  }
  
  public void setFillBefore(boolean paramBoolean) {
    this.mFlags |= 0x2;
    super.setFillBefore(paramBoolean);
  }
  
  public void setRepeatMode(int paramInt) {
    this.mFlags |= 0x4;
    super.setRepeatMode(paramInt);
  }
  
  public void setStartOffset(long paramLong) {
    this.mFlags |= 0x8;
    super.setStartOffset(paramLong);
  }
  
  public boolean hasAlpha() {
    if (this.mDirty) {
      this.mHasAlpha = false;
      this.mDirty = false;
      int i = this.mAnimations.size();
      ArrayList<Animation> arrayList = this.mAnimations;
      for (byte b = 0; b < i; b++) {
        if (((Animation)arrayList.get(b)).hasAlpha()) {
          this.mHasAlpha = true;
          break;
        } 
      } 
    } 
    return this.mHasAlpha;
  }
  
  public void setDuration(long paramLong) {
    this.mFlags |= 0x20;
    super.setDuration(paramLong);
    this.mLastEnd = this.mStartOffset + this.mDuration;
  }
  
  public void addAnimation(Animation paramAnimation) {
    this.mAnimations.add(paramAnimation);
    int i = this.mFlags;
    boolean bool = false;
    if ((i & 0x40) == 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i != 0 && paramAnimation.willChangeTransformationMatrix())
      this.mFlags |= 0x40; 
    i = bool;
    if ((this.mFlags & 0x80) == 0)
      i = 1; 
    if (i != 0 && paramAnimation.willChangeBounds())
      this.mFlags |= 0x80; 
    if ((this.mFlags & 0x20) == 32) {
      this.mLastEnd = this.mStartOffset + this.mDuration;
    } else if (this.mAnimations.size() == 1) {
      this.mDuration = paramAnimation.getStartOffset() + paramAnimation.getDuration();
      this.mLastEnd = this.mStartOffset + this.mDuration;
    } else {
      long l = Math.max(this.mLastEnd, this.mStartOffset + paramAnimation.getStartOffset() + paramAnimation.getDuration());
      this.mDuration = l - this.mStartOffset;
    } 
    this.mDirty = true;
  }
  
  public void setStartTime(long paramLong) {
    super.setStartTime(paramLong);
    int i = this.mAnimations.size();
    ArrayList<Animation> arrayList = this.mAnimations;
    for (byte b = 0; b < i; b++) {
      Animation animation = arrayList.get(b);
      animation.setStartTime(paramLong);
    } 
  }
  
  public long getStartTime() {
    long l = Long.MAX_VALUE;
    int i = this.mAnimations.size();
    ArrayList<Animation> arrayList = this.mAnimations;
    for (byte b = 0; b < i; b++) {
      Animation animation = arrayList.get(b);
      l = Math.min(l, animation.getStartTime());
    } 
    return l;
  }
  
  public void restrictDuration(long paramLong) {
    super.restrictDuration(paramLong);
    ArrayList<Animation> arrayList = this.mAnimations;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++)
      ((Animation)arrayList.get(b)).restrictDuration(paramLong); 
  }
  
  public long getDuration() {
    byte b;
    long l2;
    ArrayList<Animation> arrayList = this.mAnimations;
    int i = arrayList.size();
    long l1 = 0L;
    if ((this.mFlags & 0x20) == 32) {
      b = 1;
    } else {
      b = 0;
    } 
    if (b) {
      l2 = this.mDuration;
    } else {
      b = 0;
      while (true) {
        l2 = l1;
        if (b < i) {
          l1 = Math.max(l1, ((Animation)arrayList.get(b)).getDuration());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return l2;
  }
  
  public long computeDurationHint() {
    long l = 0L;
    int i = this.mAnimations.size();
    ArrayList<Animation> arrayList = this.mAnimations;
    for (; --i >= 0; i--, l = l2) {
      long l1 = ((Animation)arrayList.get(i)).computeDurationHint();
      long l2 = l;
      if (l1 > l)
        l2 = l1; 
    } 
    return l;
  }
  
  public void initializeInvalidateRegion(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    RectF rectF = this.mPreviousRegion;
    rectF.set(paramInt1, paramInt2, paramInt3, paramInt4);
    rectF.inset(-1.0F, -1.0F);
    if (this.mFillBefore) {
      paramInt1 = this.mAnimations.size();
      ArrayList<Animation> arrayList = this.mAnimations;
      Transformation transformation1 = this.mTempTransformation;
      Transformation transformation2 = this.mPreviousTransformation;
      for (; --paramInt1 >= 0; paramInt1--) {
        Animation animation = arrayList.get(paramInt1);
        if (!animation.isFillEnabled() || animation.getFillBefore() || animation.getStartOffset() == 0L) {
          transformation1.clear();
          Interpolator interpolator = animation.mInterpolator;
          float f = 0.0F;
          if (interpolator != null)
            f = interpolator.getInterpolation(0.0F); 
          animation.applyTransformation(f, transformation1);
          transformation2.compose(transformation1);
        } 
      } 
    } 
  }
  
  public boolean getTransformation(long paramLong, Transformation paramTransformation) {
    int i = this.mAnimations.size();
    ArrayList<Animation> arrayList = this.mAnimations;
    Transformation transformation = this.mTempTransformation;
    boolean bool1 = false;
    boolean bool = false;
    boolean bool2 = true;
    paramTransformation.clear();
    i--;
    while (true) {
      boolean bool3 = true;
      if (i >= 0) {
        boolean bool4;
        Animation animation = arrayList.get(i);
        transformation.clear();
        if (animation.getTransformation(paramLong, transformation, getScaleFactor()) || bool1) {
          bool4 = true;
        } else {
          bool4 = false;
        } 
        bool1 = bool4;
        paramTransformation.compose(transformation);
        if (bool || animation.hasStarted()) {
          bool = true;
        } else {
          bool = false;
        } 
        if (animation.hasEnded() && bool2) {
          bool4 = bool3;
        } else {
          bool4 = false;
        } 
        i--;
        bool2 = bool4;
        continue;
      } 
      break;
    } 
    if (bool && !this.mStarted) {
      dispatchAnimationStart();
      this.mStarted = true;
    } 
    if (bool2 != this.mEnded) {
      dispatchAnimationEnd();
      this.mEnded = bool2;
    } 
    return bool1;
  }
  
  public void scaleCurrentDuration(float paramFloat) {
    ArrayList<Animation> arrayList = this.mAnimations;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++)
      ((Animation)arrayList.get(b)).scaleCurrentDuration(paramFloat); 
  }
  
  public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: iload_2
    //   3: iload_3
    //   4: iload #4
    //   6: invokespecial initialize : (IIII)V
    //   9: aload_0
    //   10: getfield mFlags : I
    //   13: istore #5
    //   15: iconst_0
    //   16: istore #6
    //   18: iload #5
    //   20: bipush #32
    //   22: iand
    //   23: bipush #32
    //   25: if_icmpne -> 34
    //   28: iconst_1
    //   29: istore #5
    //   31: goto -> 37
    //   34: iconst_0
    //   35: istore #5
    //   37: aload_0
    //   38: getfield mFlags : I
    //   41: iconst_1
    //   42: iand
    //   43: iconst_1
    //   44: if_icmpne -> 53
    //   47: iconst_1
    //   48: istore #7
    //   50: goto -> 56
    //   53: iconst_0
    //   54: istore #7
    //   56: aload_0
    //   57: getfield mFlags : I
    //   60: iconst_2
    //   61: iand
    //   62: iconst_2
    //   63: if_icmpne -> 72
    //   66: iconst_1
    //   67: istore #8
    //   69: goto -> 75
    //   72: iconst_0
    //   73: istore #8
    //   75: aload_0
    //   76: getfield mFlags : I
    //   79: iconst_4
    //   80: iand
    //   81: iconst_4
    //   82: if_icmpne -> 91
    //   85: iconst_1
    //   86: istore #9
    //   88: goto -> 94
    //   91: iconst_0
    //   92: istore #9
    //   94: aload_0
    //   95: getfield mFlags : I
    //   98: bipush #16
    //   100: iand
    //   101: bipush #16
    //   103: if_icmpne -> 112
    //   106: iconst_1
    //   107: istore #10
    //   109: goto -> 115
    //   112: iconst_0
    //   113: istore #10
    //   115: aload_0
    //   116: getfield mFlags : I
    //   119: bipush #8
    //   121: iand
    //   122: bipush #8
    //   124: if_icmpne -> 130
    //   127: iconst_1
    //   128: istore #6
    //   130: iload #10
    //   132: ifeq -> 139
    //   135: aload_0
    //   136: invokevirtual ensureInterpolator : ()V
    //   139: aload_0
    //   140: getfield mAnimations : Ljava/util/ArrayList;
    //   143: astore #11
    //   145: aload #11
    //   147: invokevirtual size : ()I
    //   150: istore #12
    //   152: aload_0
    //   153: getfield mDuration : J
    //   156: lstore #13
    //   158: aload_0
    //   159: getfield mFillAfter : Z
    //   162: istore #15
    //   164: aload_0
    //   165: getfield mFillBefore : Z
    //   168: istore #16
    //   170: aload_0
    //   171: getfield mRepeatMode : I
    //   174: istore #17
    //   176: aload_0
    //   177: getfield mInterpolator : Landroid/view/animation/Interpolator;
    //   180: astore #18
    //   182: aload_0
    //   183: getfield mStartOffset : J
    //   186: lstore #19
    //   188: aload_0
    //   189: getfield mStoredOffsets : [J
    //   192: astore #21
    //   194: iload #6
    //   196: ifeq -> 231
    //   199: aload #21
    //   201: ifnull -> 216
    //   204: aload #21
    //   206: astore #22
    //   208: aload #21
    //   210: arraylength
    //   211: iload #12
    //   213: if_icmpeq -> 248
    //   216: iload #12
    //   218: newarray long
    //   220: astore #22
    //   222: aload_0
    //   223: aload #22
    //   225: putfield mStoredOffsets : [J
    //   228: goto -> 248
    //   231: aload #21
    //   233: astore #22
    //   235: aload #21
    //   237: ifnull -> 248
    //   240: aload_0
    //   241: aconst_null
    //   242: putfield mStoredOffsets : [J
    //   245: aconst_null
    //   246: astore #22
    //   248: iconst_0
    //   249: istore #23
    //   251: iload #23
    //   253: iload #12
    //   255: if_icmpge -> 381
    //   258: aload #11
    //   260: iload #23
    //   262: invokevirtual get : (I)Ljava/lang/Object;
    //   265: checkcast android/view/animation/Animation
    //   268: astore #21
    //   270: iload #5
    //   272: ifeq -> 282
    //   275: aload #21
    //   277: lload #13
    //   279: invokevirtual setDuration : (J)V
    //   282: iload #7
    //   284: ifeq -> 294
    //   287: aload #21
    //   289: iload #15
    //   291: invokevirtual setFillAfter : (Z)V
    //   294: iload #8
    //   296: ifeq -> 306
    //   299: aload #21
    //   301: iload #16
    //   303: invokevirtual setFillBefore : (Z)V
    //   306: iload #9
    //   308: ifeq -> 318
    //   311: aload #21
    //   313: iload #17
    //   315: invokevirtual setRepeatMode : (I)V
    //   318: iload #10
    //   320: ifeq -> 333
    //   323: aload #21
    //   325: aload #18
    //   327: invokevirtual setInterpolator : (Landroid/view/animation/Interpolator;)V
    //   330: goto -> 333
    //   333: iload #6
    //   335: ifeq -> 365
    //   338: aload #21
    //   340: invokevirtual getStartOffset : ()J
    //   343: lstore #24
    //   345: aload #21
    //   347: lload #24
    //   349: lload #19
    //   351: ladd
    //   352: invokevirtual setStartOffset : (J)V
    //   355: aload #22
    //   357: iload #23
    //   359: lload #24
    //   361: lastore
    //   362: goto -> 365
    //   365: aload #21
    //   367: iload_1
    //   368: iload_2
    //   369: iload_3
    //   370: iload #4
    //   372: invokevirtual initialize : (IIII)V
    //   375: iinc #23, 1
    //   378: goto -> 251
    //   381: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #421	-> 0
    //   #423	-> 9
    //   #424	-> 37
    //   #425	-> 56
    //   #426	-> 75
    //   #427	-> 94
    //   #429	-> 115
    //   #432	-> 130
    //   #433	-> 135
    //   #436	-> 139
    //   #437	-> 145
    //   #439	-> 152
    //   #440	-> 158
    //   #441	-> 164
    //   #442	-> 170
    //   #443	-> 176
    //   #444	-> 182
    //   #447	-> 188
    //   #448	-> 194
    //   #449	-> 199
    //   #450	-> 216
    //   #452	-> 231
    //   #453	-> 240
    //   #456	-> 248
    //   #457	-> 258
    //   #458	-> 270
    //   #459	-> 275
    //   #461	-> 282
    //   #462	-> 287
    //   #464	-> 294
    //   #465	-> 299
    //   #467	-> 306
    //   #468	-> 311
    //   #470	-> 318
    //   #471	-> 323
    //   #470	-> 333
    //   #473	-> 333
    //   #474	-> 338
    //   #475	-> 345
    //   #476	-> 355
    //   #473	-> 365
    //   #478	-> 365
    //   #456	-> 375
    //   #480	-> 381
  }
  
  public void reset() {
    super.reset();
    restoreChildrenStartOffset();
  }
  
  void restoreChildrenStartOffset() {
    long[] arrayOfLong = this.mStoredOffsets;
    if (arrayOfLong == null)
      return; 
    ArrayList<Animation> arrayList = this.mAnimations;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++)
      ((Animation)arrayList.get(b)).setStartOffset(arrayOfLong[b]); 
  }
  
  public List<Animation> getAnimations() {
    return this.mAnimations;
  }
  
  public boolean willChangeTransformationMatrix() {
    boolean bool;
    if ((this.mFlags & 0x40) == 64) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean willChangeBounds() {
    boolean bool;
    if ((this.mFlags & 0x80) == 128) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
