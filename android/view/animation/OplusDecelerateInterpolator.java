package android.view.animation;

import android.content.Context;
import android.util.AttributeSet;

public class OplusDecelerateInterpolator extends BaseInterpolator {
  private static final boolean DEBUG = false;
  
  private static final String LOG_TAG = "OplusDecelerateInterpolator";
  
  public OplusDecelerateInterpolator() {}
  
  public OplusDecelerateInterpolator(Context paramContext, AttributeSet paramAttributeSet) {}
  
  public float getInterpolation(float paramFloat) {
    paramFloat = (float)(1.0199999809265137D - 1.0199999809265137D / (Math.pow(paramFloat, 2.0D) * 50.0D + 1.0D));
    return paramFloat;
  }
}
