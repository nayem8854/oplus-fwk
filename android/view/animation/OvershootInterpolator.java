package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;
import com.android.internal.R;

@HasNativeInterpolator
public class OvershootInterpolator extends BaseInterpolator implements NativeInterpolator {
  private final float mTension;
  
  public OvershootInterpolator() {
    this.mTension = 2.0F;
  }
  
  public OvershootInterpolator(float paramFloat) {
    this.mTension = paramFloat;
  }
  
  public OvershootInterpolator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext.getResources(), paramContext.getTheme(), paramAttributeSet);
  }
  
  public OvershootInterpolator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    TypedArray typedArray;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.OvershootInterpolator, 0, 0);
    } else {
      typedArray = typedArray.obtainAttributes(paramAttributeSet, R.styleable.OvershootInterpolator);
    } 
    this.mTension = typedArray.getFloat(0, 2.0F);
    setChangingConfiguration(typedArray.getChangingConfigurations());
    typedArray.recycle();
  }
  
  public float getInterpolation(float paramFloat) {
    paramFloat--;
    float f = this.mTension;
    return paramFloat * paramFloat * ((f + 1.0F) * paramFloat + f) + 1.0F;
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createOvershootInterpolator(this.mTension);
  }
}
