package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;
import com.android.internal.R;

@HasNativeInterpolator
public class CycleInterpolator extends BaseInterpolator implements NativeInterpolator {
  private float mCycles;
  
  public CycleInterpolator(float paramFloat) {
    this.mCycles = paramFloat;
  }
  
  public CycleInterpolator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext.getResources(), paramContext.getTheme(), paramAttributeSet);
  }
  
  public CycleInterpolator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    TypedArray typedArray;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.CycleInterpolator, 0, 0);
    } else {
      typedArray = typedArray.obtainAttributes(paramAttributeSet, R.styleable.CycleInterpolator);
    } 
    this.mCycles = typedArray.getFloat(0, 1.0F);
    setChangingConfiguration(typedArray.getChangingConfigurations());
    typedArray.recycle();
  }
  
  public float getInterpolation(float paramFloat) {
    return (float)Math.sin((this.mCycles * 2.0F) * Math.PI * paramFloat);
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createCycleInterpolator(this.mCycles);
  }
}
