package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.os.Handler;
import android.os.SystemProperties;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.android.internal.R;
import dalvik.system.CloseGuard;

public abstract class Animation extends OplusBaseAnimation implements Cloneable {
  public boolean mSpeeduped = false;
  
  class NoImagePreloadHolder {
    public static final boolean USE_CLOSEGUARD = SystemProperties.getBoolean("log.closeguard.Animation", false);
  }
  
  boolean mEnded = false;
  
  boolean mStarted = false;
  
  boolean mCycleFlip = false;
  
  boolean mInitialized = false;
  
  boolean mFillBefore = true;
  
  boolean mFillAfter = false;
  
  boolean mFillEnabled = false;
  
  long mStartTime = -1L;
  
  int mRepeatCount = 0;
  
  int mRepeated = 0;
  
  int mRepeatMode = 1;
  
  private float mScaleFactor = 1.0F;
  
  private boolean mMore = true;
  
  private boolean mOneMoreTime = true;
  
  RectF mPreviousRegion = new RectF();
  
  RectF mRegion = new RectF();
  
  Transformation mTransformation = new Transformation();
  
  Transformation mPreviousTransformation = new Transformation();
  
  private final CloseGuard guard = CloseGuard.get();
  
  public static final int ABSOLUTE = 0;
  
  public static final int INFINITE = -1;
  
  public static final int RELATIVE_TO_PARENT = 2;
  
  public static final int RELATIVE_TO_SELF = 1;
  
  public static final int RESTART = 1;
  
  public static final int REVERSE = 2;
  
  public static final int START_ON_FIRST_FRAME = -1;
  
  public static final int ZORDER_BOTTOM = -1;
  
  public static final int ZORDER_NORMAL = 0;
  
  public static final int ZORDER_TOP = 1;
  
  private int mBackgroundColor;
  
  long mDuration;
  
  private boolean mHasRoundedCorners;
  
  Interpolator mInterpolator;
  
  private AnimationListener mListener;
  
  private Handler mListenerHandler;
  
  private Runnable mOnEnd;
  
  private Runnable mOnRepeat;
  
  private Runnable mOnStart;
  
  private boolean mShowWallpaper;
  
  long mStartOffset;
  
  private int mZAdjustment;
  
  public Animation() {
    ensureInterpolator();
  }
  
  public Animation(Context paramContext, AttributeSet paramAttributeSet) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Animation);
    setDuration(typedArray.getInt(2, 0));
    setStartOffset(typedArray.getInt(5, 0));
    setFillEnabled(typedArray.getBoolean(9, this.mFillEnabled));
    setFillBefore(typedArray.getBoolean(3, this.mFillBefore));
    setFillAfter(typedArray.getBoolean(4, this.mFillAfter));
    setRepeatCount(typedArray.getInt(6, this.mRepeatCount));
    setRepeatMode(typedArray.getInt(7, 1));
    setZAdjustment(typedArray.getInt(8, 0));
    setBackgroundColor(typedArray.getInt(0, 0));
    boolean bool = typedArray.getBoolean(10, false);
    setDetachWallpaper(bool);
    bool = typedArray.getBoolean(12, false);
    setShowWallpaper(bool);
    bool = typedArray.getBoolean(11, false);
    setHasRoundedCorners(bool);
    int i = typedArray.getResourceId(1, 0);
    typedArray.recycle();
    if (i > 0)
      setInterpolator(paramContext, i); 
    ensureInterpolator();
  }
  
  protected Animation clone() throws CloneNotSupportedException {
    Animation animation = (Animation)super.clone();
    animation.mPreviousRegion = new RectF();
    animation.mRegion = new RectF();
    animation.mTransformation = new Transformation();
    animation.mPreviousTransformation = new Transformation();
    return animation;
  }
  
  public void reset() {
    this.mPreviousRegion.setEmpty();
    this.mPreviousTransformation.clear();
    this.mInitialized = false;
    this.mCycleFlip = false;
    this.mRepeated = 0;
    this.mMore = true;
    this.mOneMoreTime = true;
    this.mListenerHandler = null;
  }
  
  public void cancel() {
    if (this.mStarted && !this.mEnded) {
      fireAnimationEnd();
      this.mEnded = true;
      this.guard.close();
    } 
    this.mStartTime = Long.MIN_VALUE;
    this.mOneMoreTime = false;
    this.mMore = false;
  }
  
  public void detach() {
    if (this.mStarted && !this.mEnded) {
      this.mEnded = true;
      this.guard.close();
      fireAnimationEnd();
    } 
  }
  
  public boolean isInitialized() {
    return this.mInitialized;
  }
  
  public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    reset();
    this.mInitialized = true;
  }
  
  public void setListenerHandler(Handler paramHandler) {
    if (this.mListenerHandler == null) {
      this.mOnStart = (Runnable)new Object(this);
      this.mOnRepeat = (Runnable)new Object(this);
      this.mOnEnd = (Runnable)new Object(this);
    } 
    this.mListenerHandler = paramHandler;
  }
  
  public void setInterpolator(Context paramContext, int paramInt) {
    setInterpolator(AnimationUtils.loadInterpolator(paramContext, paramInt));
  }
  
  public void setInterpolator(Interpolator paramInterpolator) {
    this.mInterpolator = paramInterpolator;
  }
  
  public void setStartOffset(long paramLong) {
    this.mStartOffset = paramLong;
  }
  
  public void setDuration(long paramLong) {
    if (paramLong >= 0L) {
      this.mDuration = paramLong;
      return;
    } 
    throw new IllegalArgumentException("Animation duration cannot be negative");
  }
  
  public void restrictDuration(long paramLong) {
    long l1 = this.mStartOffset;
    if (l1 > paramLong) {
      this.mStartOffset = paramLong;
      this.mDuration = 0L;
      this.mRepeatCount = 0;
      return;
    } 
    long l2 = this.mDuration + l1;
    long l3 = l2;
    if (l2 > paramLong) {
      this.mDuration = paramLong - l1;
      l3 = paramLong;
    } 
    if (this.mDuration <= 0L) {
      this.mDuration = 0L;
      this.mRepeatCount = 0;
      return;
    } 
    int i = this.mRepeatCount;
    if (i < 0 || i > paramLong || i * l3 > paramLong) {
      this.mRepeatCount = i = (int)(paramLong / l3) - 1;
      if (i < 0)
        this.mRepeatCount = 0; 
    } 
  }
  
  public void scaleCurrentDuration(float paramFloat) {
    this.mDuration = (long)((float)this.mDuration * paramFloat);
    this.mStartOffset = (long)((float)this.mStartOffset * paramFloat);
  }
  
  public void setStartTime(long paramLong) {
    this.mStartTime = paramLong;
    this.mEnded = false;
    this.mStarted = false;
    this.mCycleFlip = false;
    this.mRepeated = 0;
    this.mMore = true;
  }
  
  public void start() {
    setStartTime(-1L);
  }
  
  public void startNow() {
    setStartTime(AnimationUtils.currentAnimationTimeMillis());
  }
  
  public void setRepeatMode(int paramInt) {
    this.mRepeatMode = paramInt;
  }
  
  public void setRepeatCount(int paramInt) {
    int i = paramInt;
    if (paramInt < 0)
      i = -1; 
    this.mRepeatCount = i;
  }
  
  public boolean isFillEnabled() {
    return this.mFillEnabled;
  }
  
  public void setFillEnabled(boolean paramBoolean) {
    this.mFillEnabled = paramBoolean;
  }
  
  public void setFillBefore(boolean paramBoolean) {
    this.mFillBefore = paramBoolean;
  }
  
  public void setFillAfter(boolean paramBoolean) {
    this.mFillAfter = paramBoolean;
  }
  
  public void setZAdjustment(int paramInt) {
    this.mZAdjustment = paramInt;
  }
  
  @Deprecated
  public void setBackgroundColor(int paramInt) {}
  
  protected float getScaleFactor() {
    return this.mScaleFactor;
  }
  
  @Deprecated
  public void setDetachWallpaper(boolean paramBoolean) {}
  
  public void setShowWallpaper(boolean paramBoolean) {
    this.mShowWallpaper = paramBoolean;
  }
  
  public void setHasRoundedCorners(boolean paramBoolean) {
    this.mHasRoundedCorners = paramBoolean;
  }
  
  public Interpolator getInterpolator() {
    return this.mInterpolator;
  }
  
  public long getStartTime() {
    return this.mStartTime;
  }
  
  public long getDuration() {
    return this.mDuration;
  }
  
  public long getStartOffset() {
    return this.mStartOffset;
  }
  
  public int getRepeatMode() {
    return this.mRepeatMode;
  }
  
  public int getRepeatCount() {
    return this.mRepeatCount;
  }
  
  public boolean getFillBefore() {
    return this.mFillBefore;
  }
  
  public boolean getFillAfter() {
    return this.mFillAfter;
  }
  
  public int getZAdjustment() {
    return this.mZAdjustment;
  }
  
  @Deprecated
  public int getBackgroundColor() {
    return 0;
  }
  
  @Deprecated
  public boolean getDetachWallpaper() {
    return true;
  }
  
  public boolean getShowWallpaper() {
    return this.mShowWallpaper;
  }
  
  public boolean hasRoundedCorners() {
    return this.mHasRoundedCorners;
  }
  
  public boolean willChangeTransformationMatrix() {
    return true;
  }
  
  public boolean willChangeBounds() {
    return true;
  }
  
  private boolean hasAnimationListener() {
    boolean bool;
    if (this.mListener != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setAnimationListener(AnimationListener paramAnimationListener) {
    this.mListener = paramAnimationListener;
  }
  
  protected void ensureInterpolator() {
    if (this.mInterpolator == null)
      this.mInterpolator = new AccelerateDecelerateInterpolator(); 
  }
  
  public long computeDurationHint() {
    return (getStartOffset() + getDuration()) * (getRepeatCount() + 1);
  }
  
  public boolean getTransformation(long paramLong, Transformation paramTransformation) {
    float f1;
    boolean bool1, bool2;
    if (this.mStartTime == -1L)
      this.mStartTime = paramLong; 
    long l1 = getStartOffset();
    long l2 = this.mDuration;
    if (l2 != 0L) {
      f1 = (float)(paramLong - this.mStartTime + l1) / (float)l2;
    } else if (paramLong < this.mStartTime) {
      f1 = 0.0F;
    } else {
      f1 = 1.0F;
    } 
    if (f1 >= 1.0F || isCanceled()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (!bool1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mMore = bool2;
    float f2 = f1;
    if (!this.mFillEnabled)
      f2 = Math.max(Math.min(f1, 1.0F), 0.0F); 
    if ((f2 >= 0.0F || this.mFillBefore) && (f2 <= 1.0F || this.mFillAfter)) {
      if (!this.mStarted) {
        fireAnimationStart();
        this.mStarted = true;
        if (NoImagePreloadHolder.USE_CLOSEGUARD)
          this.guard.open("cancel or detach or getTransformation"); 
      } 
      f1 = f2;
      if (this.mFillEnabled)
        f1 = Math.max(Math.min(f2, 1.0F), 0.0F); 
      f2 = f1;
      if (this.mCycleFlip)
        f2 = 1.0F - f1; 
      f1 = this.mInterpolator.getInterpolation(f2);
      applyTransformation(f1, paramTransformation);
    } 
    if (bool1)
      if (this.mRepeatCount == this.mRepeated || isCanceled()) {
        if (!this.mEnded) {
          this.mEnded = true;
          this.guard.close();
          fireAnimationEnd();
        } 
      } else {
        if (this.mRepeatCount > 0)
          this.mRepeated++; 
        if (this.mRepeatMode == 2)
          this.mCycleFlip ^= 0x1; 
        this.mStartTime = -1L;
        this.mMore = true;
        fireAnimationRepeat();
      }  
    if (!this.mMore && this.mOneMoreTime) {
      this.mOneMoreTime = false;
      return true;
    } 
    return this.mMore;
  }
  
  private boolean isCanceled() {
    boolean bool;
    if (this.mStartTime == Long.MIN_VALUE) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void fireAnimationStart() {
    if (hasAnimationListener()) {
      Handler handler = this.mListenerHandler;
      if (handler == null) {
        dispatchAnimationStart();
      } else {
        handler.postAtFrontOfQueue(this.mOnStart);
      } 
    } 
  }
  
  private void fireAnimationRepeat() {
    if (hasAnimationListener()) {
      Handler handler = this.mListenerHandler;
      if (handler == null) {
        dispatchAnimationRepeat();
      } else {
        handler.postAtFrontOfQueue(this.mOnRepeat);
      } 
    } 
  }
  
  private void fireAnimationEnd() {
    if (hasAnimationListener()) {
      Handler handler = this.mListenerHandler;
      if (handler == null) {
        dispatchAnimationEnd();
      } else {
        handler.postAtFrontOfQueue(this.mOnEnd);
      } 
    } 
  }
  
  void dispatchAnimationStart() {
    AnimationListener animationListener = this.mListener;
    if (animationListener != null)
      animationListener.onAnimationStart(this); 
  }
  
  void dispatchAnimationRepeat() {
    AnimationListener animationListener = this.mListener;
    if (animationListener != null)
      animationListener.onAnimationRepeat(this); 
  }
  
  void dispatchAnimationEnd() {
    AnimationListener animationListener = this.mListener;
    if (animationListener != null)
      animationListener.onAnimationEnd(this); 
  }
  
  public boolean getTransformation(long paramLong, Transformation paramTransformation, float paramFloat) {
    this.mScaleFactor = paramFloat;
    return getTransformation(paramLong, paramTransformation);
  }
  
  public boolean hasStarted() {
    return this.mStarted;
  }
  
  public boolean hasEnded() {
    return this.mEnded;
  }
  
  protected void applyTransformation(float paramFloat, Transformation paramTransformation) {}
  
  protected float resolveSize(int paramInt1, float paramFloat, int paramInt2, int paramInt3) {
    if (paramInt1 != 1) {
      if (paramInt1 != 2)
        return paramFloat; 
      return paramInt3 * paramFloat;
    } 
    return paramInt2 * paramFloat;
  }
  
  public void getInvalidateRegion(int paramInt1, int paramInt2, int paramInt3, int paramInt4, RectF paramRectF, Transformation paramTransformation) {
    RectF rectF1 = this.mRegion;
    RectF rectF2 = this.mPreviousRegion;
    paramRectF.set(paramInt1, paramInt2, paramInt3, paramInt4);
    paramTransformation.getMatrix().mapRect(paramRectF);
    paramRectF.inset(-1.0F, -1.0F);
    rectF1.set(paramRectF);
    paramRectF.union(rectF2);
    rectF2.set(rectF1);
    Transformation transformation2 = this.mTransformation;
    Transformation transformation1 = this.mPreviousTransformation;
    transformation2.set(paramTransformation);
    paramTransformation.set(transformation1);
    transformation1.set(transformation2);
  }
  
  public void initializeInvalidateRegion(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    RectF rectF = this.mPreviousRegion;
    rectF.set(paramInt1, paramInt2, paramInt3, paramInt4);
    rectF.inset(-1.0F, -1.0F);
    if (this.mFillBefore) {
      Transformation transformation = this.mPreviousTransformation;
      applyTransformation(this.mInterpolator.getInterpolation(0.0F), transformation);
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.guard != null)
        this.guard.warnIfOpen(); 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public boolean hasAlpha() {
    return false;
  }
  
  class Description {
    public int type;
    
    public float value;
    
    static Description parseValue(TypedValue param1TypedValue) {
      Description description = new Description();
      if (param1TypedValue == null) {
        description.type = 0;
        description.value = 0.0F;
      } else {
        if (param1TypedValue.type == 6) {
          int i = param1TypedValue.data;
          byte b = 1;
          if ((i & 0xF) == 1)
            b = 2; 
          description.type = b;
          description.value = TypedValue.complexToFloat(param1TypedValue.data);
          return description;
        } 
        if (param1TypedValue.type == 4) {
          description.type = 0;
          description.value = param1TypedValue.getFloat();
          return description;
        } 
        if (param1TypedValue.type >= 16 && param1TypedValue.type <= 31) {
          description.type = 0;
          description.value = param1TypedValue.data;
          return description;
        } 
      } 
      description.type = 0;
      description.value = 0.0F;
      return description;
    }
  }
  
  class AnimationListener {
    public abstract void onAnimationEnd(Animation param1Animation);
    
    public abstract void onAnimationRepeat(Animation param1Animation);
    
    public abstract void onAnimationStart(Animation param1Animation);
  }
}
