package android.view.animation;

public abstract class OplusBaseAnimation {
  public float getFromXValue() {
    return -1.0F;
  }
  
  public void setFromXValue(float paramFloat) {}
  
  public float getToXValue() {
    return -1.0F;
  }
  
  public float getFromAlpha() {
    return -1.0F;
  }
  
  public void setFromAlpha(float paramFloat) {}
  
  public float getToAlpha() {
    return -1.0F;
  }
}
