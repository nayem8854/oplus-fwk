package android.view.animation;

import android.content.Context;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;

@HasNativeInterpolator
public class LinearInterpolator extends BaseInterpolator implements NativeInterpolator {
  public LinearInterpolator() {}
  
  public LinearInterpolator(Context paramContext, AttributeSet paramAttributeSet) {}
  
  public float getInterpolation(float paramFloat) {
    return paramFloat;
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createLinearInterpolator();
  }
}
