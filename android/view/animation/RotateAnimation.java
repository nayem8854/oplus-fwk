package android.view.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R;

public class RotateAnimation extends Animation {
  private int mPivotXType = 0;
  
  private int mPivotYType = 0;
  
  private float mPivotXValue = 0.0F;
  
  private float mPivotYValue = 0.0F;
  
  private float mFromDegrees;
  
  private float mPivotX;
  
  private float mPivotY;
  
  private float mToDegrees;
  
  public RotateAnimation(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RotateAnimation);
    this.mFromDegrees = typedArray.getFloat(0, 0.0F);
    this.mToDegrees = typedArray.getFloat(1, 0.0F);
    Animation.Description description = Animation.Description.parseValue(typedArray.peekValue(2));
    this.mPivotXType = description.type;
    this.mPivotXValue = description.value;
    description = Animation.Description.parseValue(typedArray.peekValue(3));
    this.mPivotYType = description.type;
    this.mPivotYValue = description.value;
    typedArray.recycle();
    initializePivotPoint();
  }
  
  public RotateAnimation(float paramFloat1, float paramFloat2) {
    this.mFromDegrees = paramFloat1;
    this.mToDegrees = paramFloat2;
    this.mPivotX = 0.0F;
    this.mPivotY = 0.0F;
  }
  
  public RotateAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.mFromDegrees = paramFloat1;
    this.mToDegrees = paramFloat2;
    this.mPivotXType = 0;
    this.mPivotYType = 0;
    this.mPivotXValue = paramFloat3;
    this.mPivotYValue = paramFloat4;
    initializePivotPoint();
  }
  
  public RotateAnimation(float paramFloat1, float paramFloat2, int paramInt1, float paramFloat3, int paramInt2, float paramFloat4) {
    this.mFromDegrees = paramFloat1;
    this.mToDegrees = paramFloat2;
    this.mPivotXValue = paramFloat3;
    this.mPivotXType = paramInt1;
    this.mPivotYValue = paramFloat4;
    this.mPivotYType = paramInt2;
    initializePivotPoint();
  }
  
  private void initializePivotPoint() {
    if (this.mPivotXType == 0)
      this.mPivotX = this.mPivotXValue; 
    if (this.mPivotYType == 0)
      this.mPivotY = this.mPivotYValue; 
  }
  
  protected void applyTransformation(float paramFloat, Transformation paramTransformation) {
    float f = this.mFromDegrees;
    paramFloat = f + (this.mToDegrees - f) * paramFloat;
    f = getScaleFactor();
    if (this.mPivotX == 0.0F && this.mPivotY == 0.0F) {
      paramTransformation.getMatrix().setRotate(paramFloat);
    } else {
      paramTransformation.getMatrix().setRotate(paramFloat, this.mPivotX * f, this.mPivotY * f);
    } 
  }
  
  public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mPivotX = resolveSize(this.mPivotXType, this.mPivotXValue, paramInt1, paramInt3);
    this.mPivotY = resolveSize(this.mPivotYType, this.mPivotYValue, paramInt2, paramInt4);
  }
}
