package android.view.animation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.animation.HasNativeInterpolator;
import android.graphics.animation.NativeInterpolator;
import android.graphics.animation.NativeInterpolatorFactory;
import android.util.AttributeSet;
import com.android.internal.R;

@HasNativeInterpolator
public class DecelerateInterpolator extends BaseInterpolator implements NativeInterpolator {
  private float mFactor;
  
  public DecelerateInterpolator() {
    this.mFactor = 1.0F;
  }
  
  public DecelerateInterpolator(float paramFloat) {
    this.mFactor = 1.0F;
    this.mFactor = paramFloat;
  }
  
  public DecelerateInterpolator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext.getResources(), paramContext.getTheme(), paramAttributeSet);
  }
  
  public DecelerateInterpolator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    TypedArray typedArray;
    this.mFactor = 1.0F;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.DecelerateInterpolator, 0, 0);
    } else {
      typedArray = typedArray.obtainAttributes(paramAttributeSet, R.styleable.DecelerateInterpolator);
    } 
    this.mFactor = typedArray.getFloat(0, 1.0F);
    setChangingConfiguration(typedArray.getChangingConfigurations());
    typedArray.recycle();
  }
  
  public float getInterpolation(float paramFloat) {
    float f = this.mFactor;
    if (f == 1.0F) {
      paramFloat = 1.0F - (1.0F - paramFloat) * (1.0F - paramFloat);
    } else {
      paramFloat = (float)(1.0D - Math.pow((1.0F - paramFloat), (f * 2.0F)));
    } 
    return paramFloat;
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createDecelerateInterpolator(this.mFactor);
  }
}
