package android.view;

import android.content.ComponentName;
import android.content.pm.ParceledListSlice;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPinnedStackListener extends IInterface {
  void onActionsChanged(ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void onActivityHidden(ComponentName paramComponentName) throws RemoteException;
  
  void onAspectRatioChanged(float paramFloat) throws RemoteException;
  
  void onConfigurationChanged() throws RemoteException;
  
  void onDisplayInfoChanged(DisplayInfo paramDisplayInfo) throws RemoteException;
  
  void onImeVisibilityChanged(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void onListenerRegistered(IPinnedStackController paramIPinnedStackController) throws RemoteException;
  
  void onMovementBoundsChanged(boolean paramBoolean) throws RemoteException;
  
  class Default implements IPinnedStackListener {
    public void onListenerRegistered(IPinnedStackController param1IPinnedStackController) throws RemoteException {}
    
    public void onMovementBoundsChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onImeVisibilityChanged(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void onActionsChanged(ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void onActivityHidden(ComponentName param1ComponentName) throws RemoteException {}
    
    public void onDisplayInfoChanged(DisplayInfo param1DisplayInfo) throws RemoteException {}
    
    public void onConfigurationChanged() throws RemoteException {}
    
    public void onAspectRatioChanged(float param1Float) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPinnedStackListener {
    private static final String DESCRIPTOR = "android.view.IPinnedStackListener";
    
    static final int TRANSACTION_onActionsChanged = 4;
    
    static final int TRANSACTION_onActivityHidden = 5;
    
    static final int TRANSACTION_onAspectRatioChanged = 8;
    
    static final int TRANSACTION_onConfigurationChanged = 7;
    
    static final int TRANSACTION_onDisplayInfoChanged = 6;
    
    static final int TRANSACTION_onImeVisibilityChanged = 3;
    
    static final int TRANSACTION_onListenerRegistered = 1;
    
    static final int TRANSACTION_onMovementBoundsChanged = 2;
    
    public Stub() {
      attachInterface(this, "android.view.IPinnedStackListener");
    }
    
    public static IPinnedStackListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IPinnedStackListener");
      if (iInterface != null && iInterface instanceof IPinnedStackListener)
        return (IPinnedStackListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "onAspectRatioChanged";
        case 7:
          return "onConfigurationChanged";
        case 6:
          return "onDisplayInfoChanged";
        case 5:
          return "onActivityHidden";
        case 4:
          return "onActionsChanged";
        case 3:
          return "onImeVisibilityChanged";
        case 2:
          return "onMovementBoundsChanged";
        case 1:
          break;
      } 
      return "onListenerRegistered";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        float f;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.view.IPinnedStackListener");
            f = param1Parcel1.readFloat();
            onAspectRatioChanged(f);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.view.IPinnedStackListener");
            onConfigurationChanged();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.view.IPinnedStackListener");
            if (param1Parcel1.readInt() != 0) {
              DisplayInfo displayInfo = (DisplayInfo)DisplayInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onDisplayInfoChanged((DisplayInfo)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.view.IPinnedStackListener");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onActivityHidden((ComponentName)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.view.IPinnedStackListener");
            if (param1Parcel1.readInt() != 0) {
              ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onActionsChanged((ParceledListSlice)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.view.IPinnedStackListener");
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            param1Int1 = param1Parcel1.readInt();
            onImeVisibilityChanged(bool1, param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.view.IPinnedStackListener");
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            onMovementBoundsChanged(bool1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.view.IPinnedStackListener");
        IPinnedStackController iPinnedStackController = IPinnedStackController.Stub.asInterface(param1Parcel1.readStrongBinder());
        onListenerRegistered(iPinnedStackController);
        return true;
      } 
      param1Parcel2.writeString("android.view.IPinnedStackListener");
      return true;
    }
    
    private static class Proxy implements IPinnedStackListener {
      public static IPinnedStackListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IPinnedStackListener";
      }
      
      public void onListenerRegistered(IPinnedStackController param2IPinnedStackController) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IPinnedStackListener");
          if (param2IPinnedStackController != null) {
            iBinder = param2IPinnedStackController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IPinnedStackListener.Stub.getDefaultImpl() != null) {
            IPinnedStackListener.Stub.getDefaultImpl().onListenerRegistered(param2IPinnedStackController);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMovementBoundsChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IPinnedStackListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, null, 1);
          if (!bool1 && IPinnedStackListener.Stub.getDefaultImpl() != null) {
            IPinnedStackListener.Stub.getDefaultImpl().onMovementBoundsChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onImeVisibilityChanged(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IPinnedStackListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IPinnedStackListener.Stub.getDefaultImpl() != null) {
            IPinnedStackListener.Stub.getDefaultImpl().onImeVisibilityChanged(param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onActionsChanged(ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IPinnedStackListener");
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IPinnedStackListener.Stub.getDefaultImpl() != null) {
            IPinnedStackListener.Stub.getDefaultImpl().onActionsChanged(param2ParceledListSlice);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onActivityHidden(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IPinnedStackListener");
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IPinnedStackListener.Stub.getDefaultImpl() != null) {
            IPinnedStackListener.Stub.getDefaultImpl().onActivityHidden(param2ComponentName);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisplayInfoChanged(DisplayInfo param2DisplayInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IPinnedStackListener");
          if (param2DisplayInfo != null) {
            parcel.writeInt(1);
            param2DisplayInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IPinnedStackListener.Stub.getDefaultImpl() != null) {
            IPinnedStackListener.Stub.getDefaultImpl().onDisplayInfoChanged(param2DisplayInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConfigurationChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IPinnedStackListener");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IPinnedStackListener.Stub.getDefaultImpl() != null) {
            IPinnedStackListener.Stub.getDefaultImpl().onConfigurationChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAspectRatioChanged(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IPinnedStackListener");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IPinnedStackListener.Stub.getDefaultImpl() != null) {
            IPinnedStackListener.Stub.getDefaultImpl().onAspectRatioChanged(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPinnedStackListener param1IPinnedStackListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPinnedStackListener != null) {
          Proxy.sDefaultImpl = param1IPinnedStackListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPinnedStackListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
