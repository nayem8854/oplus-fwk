package android.view;

import android.os.Parcel;
import com.oplus.view.OplusLayoutParams;

public class OplusBaseLayoutParams extends ViewGroup.LayoutParams {
  public int ignoreHomeMenuKey = 0;
  
  public int isDisableStatusBar = 0;
  
  public int navigationBarVisibility = 0;
  
  public int navigationBarColor = 0;
  
  public int oplusFlags = 0;
  
  public final OplusLayoutParams mOplusLayoutParams = new OplusLayoutParams();
  
  public static final int DEFAULT_STATUS_BAR = 0;
  
  public static final int DISABLE_STATUS_BAR = 1;
  
  public static final int ENABLE_STATUS_BAR = 2;
  
  public static final int FLAG_DISABLE_STATUS_BAR = 1048576;
  
  public static final int FLAG_ENABLE_STATUS_BAR = 2097152;
  
  public static final int FLAG_IGNORE_HOME_KEY = 33554432;
  
  public static final int FLAG_IGNORE_HOME_MENU = 16777216;
  
  public static final int FLAG_IMENU_KEY = 67108864;
  
  public static final int IGNORE_HOME_KEY = 2;
  
  public static final int IGNORE_HOME_MENU_KEY = 1;
  
  public static final int IGNORE_MENU_KEY = 3;
  
  public static final int OPLUS_FLAG_WINDOW_DEFAULT = 0;
  
  public static final int OPLUS_FLAG_WINDOW_USER_DEFINED_TOAST = 1;
  
  public static final int PRIVATE_FLAG_BOTTOM_ALERT_DIALOG = 16777216;
  
  public static final int PRIVATE_FLAG_NAVIGATION_BAR_LIGHT = -2147483648;
  
  public static final int TYPE_DRAG_SCREEN_BACKGROUND = 2301;
  
  public static final int TYPE_DRAG_SCREEN_FOREGROUND = 2302;
  
  public static final int UNSET_ANY_KEY = 0;
  
  public OplusBaseLayoutParams(int paramInt1, int paramInt2) {
    super(paramInt1, paramInt2);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.ignoreHomeMenuKey);
    paramParcel.writeInt(this.isDisableStatusBar);
    this.mOplusLayoutParams.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.oplusFlags);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.ignoreHomeMenuKey = paramParcel.readInt();
    this.isDisableStatusBar = paramParcel.readInt();
    this.mOplusLayoutParams.readFromParcel(paramParcel);
    this.oplusFlags = paramParcel.readInt();
  }
  
  public final int copyFrom(OplusBaseLayoutParams paramOplusBaseLayoutParams) {
    int i = 0;
    int j = this.ignoreHomeMenuKey, k = paramOplusBaseLayoutParams.ignoreHomeMenuKey;
    if (j != k)
      this.ignoreHomeMenuKey = k; 
    j = this.isDisableStatusBar;
    k = paramOplusBaseLayoutParams.isDisableStatusBar;
    if (j != k)
      this.isDisableStatusBar = k; 
    if (!this.mOplusLayoutParams.equals(paramOplusBaseLayoutParams.mOplusLayoutParams)) {
      this.mOplusLayoutParams.set(paramOplusBaseLayoutParams.mOplusLayoutParams);
      i = Character.MIN_VALUE | 0x4000;
    } 
    k = this.oplusFlags;
    j = paramOplusBaseLayoutParams.oplusFlags;
    if (k != j)
      this.oplusFlags = j; 
    return i;
  }
  
  public String toString(String paramString) {
    StringBuilder stringBuilder = new StringBuilder(128);
    if (this.isDisableStatusBar != 0) {
      stringBuilder.append(" isDisableStatusBar=");
      stringBuilder.append(this.isDisableStatusBar);
    } 
    stringBuilder.append(this.mOplusLayoutParams);
    if (this.oplusFlags != 0) {
      stringBuilder.append(" oplusFlags=");
      stringBuilder.append(this.oplusFlags);
    } 
    return stringBuilder.toString();
  }
  
  public OplusBaseLayoutParams() {}
}
