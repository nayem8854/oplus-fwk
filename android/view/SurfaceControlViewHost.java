package android.view;

import android.content.Context;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.accessibility.IAccessibilityEmbeddedConnection;
import java.util.Objects;

public class SurfaceControlViewHost {
  private IAccessibilityEmbeddedConnection mAccessibilityEmbeddedConnection;
  
  private SurfaceControl mSurfaceControl;
  
  private final ViewRootImpl mViewRoot;
  
  private WindowlessWindowManager mWm;
  
  class SurfacePackage implements Parcelable {
    SurfacePackage(IAccessibilityEmbeddedConnection param1IAccessibilityEmbeddedConnection) {
      this.mSurfaceControl = (SurfaceControl)this$0;
      this.mAccessibilityEmbeddedConnection = param1IAccessibilityEmbeddedConnection;
    }
    
    private SurfacePackage(SurfaceControlViewHost this$0) {
      SurfaceControl surfaceControl = new SurfaceControl();
      surfaceControl.readFromParcel((Parcel)this$0);
      IBinder iBinder = this$0.readStrongBinder();
      this.mAccessibilityEmbeddedConnection = IAccessibilityEmbeddedConnection.Stub.asInterface(iBinder);
    }
    
    public SurfaceControl getSurfaceControl() {
      return this.mSurfaceControl;
    }
    
    public IAccessibilityEmbeddedConnection getAccessibilityEmbeddedConnection() {
      return this.mAccessibilityEmbeddedConnection;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      this.mSurfaceControl.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeStrongBinder(this.mAccessibilityEmbeddedConnection.asBinder());
    }
    
    public void release() {
      SurfaceControl surfaceControl = this.mSurfaceControl;
      if (surfaceControl != null)
        surfaceControl.release(); 
      this.mSurfaceControl = null;
    }
    
    public static final Parcelable.Creator<SurfacePackage> CREATOR = new Parcelable.Creator<SurfacePackage>() {
        public SurfaceControlViewHost.SurfacePackage createFromParcel(Parcel param2Parcel) {
          return new SurfaceControlViewHost.SurfacePackage();
        }
        
        public SurfaceControlViewHost.SurfacePackage[] newArray(int param2Int) {
          return new SurfaceControlViewHost.SurfacePackage[param2Int];
        }
      };
    
    private final IAccessibilityEmbeddedConnection mAccessibilityEmbeddedConnection;
    
    private SurfaceControl mSurfaceControl;
  }
  
  public SurfaceControlViewHost(Context paramContext, Display paramDisplay, WindowlessWindowManager paramWindowlessWindowManager) {
    this(paramContext, paramDisplay, paramWindowlessWindowManager, false);
  }
  
  public SurfaceControlViewHost(Context paramContext, Display paramDisplay, WindowlessWindowManager paramWindowlessWindowManager, boolean paramBoolean) {
    this.mWm = paramWindowlessWindowManager;
    ViewRootImpl viewRootImpl = new ViewRootImpl(paramContext, paramDisplay, this.mWm, paramBoolean);
    viewRootImpl.forceDisableBLAST();
    this.mAccessibilityEmbeddedConnection = this.mViewRoot.getAccessibilityEmbeddedConnection();
  }
  
  public SurfaceControlViewHost(Context paramContext, Display paramDisplay, IBinder paramIBinder) {
    SurfaceControl.Builder builder = new SurfaceControl.Builder();
    builder = builder.setContainerLayer();
    builder = builder.setName("SurfaceControlViewHost");
    builder = builder.setCallsite("SurfaceControlViewHost");
    this.mSurfaceControl = builder.build();
    this.mWm = new WindowlessWindowManager(paramContext.getResources().getConfiguration(), this.mSurfaceControl, paramIBinder);
    ViewRootImpl viewRootImpl = new ViewRootImpl(paramContext, paramDisplay, this.mWm);
    viewRootImpl.forceDisableBLAST();
    this.mAccessibilityEmbeddedConnection = this.mViewRoot.getAccessibilityEmbeddedConnection();
  }
  
  protected void finalize() throws Throwable {
    this.mViewRoot.die(false);
  }
  
  public SurfacePackage getSurfacePackage() {
    if (this.mSurfaceControl != null && this.mAccessibilityEmbeddedConnection != null)
      return new SurfacePackage(this.mAccessibilityEmbeddedConnection); 
    return null;
  }
  
  public void setView(View paramView, WindowManager.LayoutParams paramLayoutParams) {
    Objects.requireNonNull(paramView);
    this.mViewRoot.setView(paramView, paramLayoutParams, null);
  }
  
  public void setView(View paramView, int paramInt1, int paramInt2) {
    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(paramInt1, paramInt2, 2, 0, -2);
    layoutParams.flags |= 0x1000000;
    setView(paramView, layoutParams);
  }
  
  public View getView() {
    return this.mViewRoot.getView();
  }
  
  public IWindow getWindowToken() {
    return this.mViewRoot.mWindow;
  }
  
  public WindowlessWindowManager getWindowlessWM() {
    return this.mWm;
  }
  
  public void relayout(WindowManager.LayoutParams paramLayoutParams) {
    this.mViewRoot.setLayoutParams(paramLayoutParams, false);
    this.mViewRoot.setReportNextDraw();
    this.mWm.setCompletionCallback(this.mViewRoot.mWindow.asBinder(), (WindowlessWindowManager.ResizeCompleteCallback)_$$Lambda$SurfaceControlViewHost$eK8W6Rz7iopatcJwPdlT0MmkE40.INSTANCE);
  }
  
  public void relayout(int paramInt1, int paramInt2) {
    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(paramInt1, paramInt2, 2, 0, -2);
    relayout(layoutParams);
  }
  
  public void release() {
    this.mViewRoot.die(true);
  }
}
