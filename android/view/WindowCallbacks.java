package android.view;

import android.graphics.RecordingCanvas;
import android.graphics.Rect;

public interface WindowCallbacks {
  public static final int RESIZE_MODE_DOCKED_DIVIDER = 1;
  
  public static final int RESIZE_MODE_FREEFORM = 0;
  
  public static final int RESIZE_MODE_INVALID = -1;
  
  boolean onContentDrawn(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  void onPostDraw(RecordingCanvas paramRecordingCanvas);
  
  void onRequestDraw(boolean paramBoolean);
  
  void onWindowDragResizeEnd();
  
  void onWindowDragResizeStart(Rect paramRect1, boolean paramBoolean, Rect paramRect2, Rect paramRect3, int paramInt);
  
  void onWindowSizeIsChanging(Rect paramRect1, boolean paramBoolean, Rect paramRect2, Rect paramRect3);
}
