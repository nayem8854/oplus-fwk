package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteAnimationFinishedCallback extends IInterface {
  void onAnimationFinished() throws RemoteException;
  
  class Default implements IRemoteAnimationFinishedCallback {
    public void onAnimationFinished() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteAnimationFinishedCallback {
    private static final String DESCRIPTOR = "android.view.IRemoteAnimationFinishedCallback";
    
    static final int TRANSACTION_onAnimationFinished = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IRemoteAnimationFinishedCallback");
    }
    
    public static IRemoteAnimationFinishedCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IRemoteAnimationFinishedCallback");
      if (iInterface != null && iInterface instanceof IRemoteAnimationFinishedCallback)
        return (IRemoteAnimationFinishedCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onAnimationFinished";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IRemoteAnimationFinishedCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IRemoteAnimationFinishedCallback");
      onAnimationFinished();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IRemoteAnimationFinishedCallback {
      public static IRemoteAnimationFinishedCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IRemoteAnimationFinishedCallback";
      }
      
      public void onAnimationFinished() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IRemoteAnimationFinishedCallback");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IRemoteAnimationFinishedCallback.Stub.getDefaultImpl() != null) {
            IRemoteAnimationFinishedCallback.Stub.getDefaultImpl().onAnimationFinished();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteAnimationFinishedCallback param1IRemoteAnimationFinishedCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteAnimationFinishedCallback != null) {
          Proxy.sDefaultImpl = param1IRemoteAnimationFinishedCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteAnimationFinishedCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
