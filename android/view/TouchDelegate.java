package android.view;

import android.graphics.Rect;
import android.graphics.Region;
import android.util.ArrayMap;
import android.view.accessibility.AccessibilityNodeInfo;
import java.util.Map;

public class TouchDelegate {
  public static final int ABOVE = 1;
  
  public static final int BELOW = 2;
  
  public static final int TO_LEFT = 4;
  
  public static final int TO_RIGHT = 8;
  
  private Rect mBounds;
  
  private boolean mDelegateTargeted;
  
  private View mDelegateView;
  
  private int mSlop;
  
  private Rect mSlopBounds;
  
  private AccessibilityNodeInfo.TouchDelegateInfo mTouchDelegateInfo;
  
  public TouchDelegate(Rect paramRect, View paramView) {
    this.mBounds = paramRect;
    this.mSlop = ViewConfiguration.get(paramView.getContext()).getScaledTouchSlop();
    this.mSlopBounds = paramRect = new Rect(paramRect);
    int i = this.mSlop;
    paramRect.inset(-i, -i);
    this.mDelegateView = paramView;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getX : ()F
    //   4: f2i
    //   5: istore_2
    //   6: aload_1
    //   7: invokevirtual getY : ()F
    //   10: f2i
    //   11: istore_3
    //   12: iconst_0
    //   13: istore #4
    //   15: iconst_1
    //   16: istore #5
    //   18: iconst_1
    //   19: istore #6
    //   21: iconst_0
    //   22: istore #7
    //   24: aload_1
    //   25: invokevirtual getActionMasked : ()I
    //   28: istore #8
    //   30: iload #8
    //   32: ifeq -> 128
    //   35: iload #8
    //   37: iconst_1
    //   38: if_icmpeq -> 83
    //   41: iload #8
    //   43: iconst_2
    //   44: if_icmpeq -> 83
    //   47: iload #8
    //   49: iconst_3
    //   50: if_icmpeq -> 69
    //   53: iload #8
    //   55: iconst_5
    //   56: if_icmpeq -> 83
    //   59: iload #8
    //   61: bipush #6
    //   63: if_icmpeq -> 83
    //   66: goto -> 147
    //   69: aload_0
    //   70: getfield mDelegateTargeted : Z
    //   73: istore #4
    //   75: aload_0
    //   76: iconst_0
    //   77: putfield mDelegateTargeted : Z
    //   80: goto -> 147
    //   83: aload_0
    //   84: getfield mDelegateTargeted : Z
    //   87: istore #9
    //   89: iload #9
    //   91: istore #4
    //   93: iload #9
    //   95: ifeq -> 147
    //   98: aload_0
    //   99: getfield mSlopBounds : Landroid/graphics/Rect;
    //   102: astore #10
    //   104: iload #6
    //   106: istore #5
    //   108: aload #10
    //   110: iload_2
    //   111: iload_3
    //   112: invokevirtual contains : (II)Z
    //   115: ifne -> 121
    //   118: iconst_0
    //   119: istore #5
    //   121: iload #9
    //   123: istore #4
    //   125: goto -> 147
    //   128: aload_0
    //   129: aload_0
    //   130: getfield mBounds : Landroid/graphics/Rect;
    //   133: iload_2
    //   134: iload_3
    //   135: invokevirtual contains : (II)Z
    //   138: putfield mDelegateTargeted : Z
    //   141: aload_0
    //   142: getfield mDelegateTargeted : Z
    //   145: istore #4
    //   147: iload #4
    //   149: ifeq -> 216
    //   152: iload #5
    //   154: ifeq -> 184
    //   157: aload_1
    //   158: aload_0
    //   159: getfield mDelegateView : Landroid/view/View;
    //   162: invokevirtual getWidth : ()I
    //   165: iconst_2
    //   166: idiv
    //   167: i2f
    //   168: aload_0
    //   169: getfield mDelegateView : Landroid/view/View;
    //   172: invokevirtual getHeight : ()I
    //   175: iconst_2
    //   176: idiv
    //   177: i2f
    //   178: invokevirtual setLocation : (FF)V
    //   181: goto -> 206
    //   184: aload_0
    //   185: getfield mSlop : I
    //   188: istore #5
    //   190: aload_1
    //   191: iload #5
    //   193: iconst_2
    //   194: imul
    //   195: ineg
    //   196: i2f
    //   197: iload #5
    //   199: iconst_2
    //   200: imul
    //   201: ineg
    //   202: i2f
    //   203: invokevirtual setLocation : (FF)V
    //   206: aload_0
    //   207: getfield mDelegateView : Landroid/view/View;
    //   210: aload_1
    //   211: invokevirtual dispatchTouchEvent : (Landroid/view/MotionEvent;)Z
    //   214: istore #7
    //   216: iload #7
    //   218: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #113	-> 0
    //   #114	-> 6
    //   #115	-> 12
    //   #116	-> 15
    //   #117	-> 21
    //   #119	-> 24
    //   #137	-> 69
    //   #138	-> 75
    //   #128	-> 83
    //   #129	-> 89
    //   #130	-> 98
    //   #131	-> 104
    //   #132	-> 118
    //   #134	-> 121
    //   #121	-> 128
    //   #122	-> 141
    //   #123	-> 147
    //   #141	-> 147
    //   #142	-> 152
    //   #144	-> 157
    //   #148	-> 184
    //   #149	-> 190
    //   #151	-> 206
    //   #153	-> 216
  }
  
  public boolean onTouchExplorationHoverEvent(MotionEvent paramMotionEvent) {
    if (this.mBounds == null)
      return false; 
    int i = (int)paramMotionEvent.getX();
    int j = (int)paramMotionEvent.getY();
    boolean bool = true;
    boolean bool1 = false;
    boolean bool2 = this.mBounds.contains(i, j);
    int k = paramMotionEvent.getActionMasked();
    if (k != 7) {
      if (k != 9) {
        if (k != 10) {
          k = bool;
        } else {
          this.mDelegateTargeted = true;
          k = bool;
        } 
      } else {
        this.mDelegateTargeted = bool2;
        k = bool;
      } 
    } else if (bool2) {
      this.mDelegateTargeted = true;
      k = bool;
    } else {
      k = bool;
      if (this.mDelegateTargeted) {
        k = bool;
        if (!this.mSlopBounds.contains(i, j))
          k = 0; 
      } 
    } 
    if (this.mDelegateTargeted) {
      if (k != 0) {
        paramMotionEvent.setLocation((this.mDelegateView.getWidth() / 2), (this.mDelegateView.getHeight() / 2));
      } else {
        this.mDelegateTargeted = false;
      } 
      bool1 = this.mDelegateView.dispatchHoverEvent(paramMotionEvent);
    } 
    return bool1;
  }
  
  public AccessibilityNodeInfo.TouchDelegateInfo getTouchDelegateInfo() {
    if (this.mTouchDelegateInfo == null) {
      ArrayMap<Object, Object> arrayMap = new ArrayMap<>(1);
      Rect rect1 = this.mBounds;
      Rect rect2 = rect1;
      if (rect1 == null)
        rect2 = new Rect(); 
      arrayMap.put(new Region(rect2), this.mDelegateView);
      this.mTouchDelegateInfo = new AccessibilityNodeInfo.TouchDelegateInfo((Map)arrayMap);
    } 
    return this.mTouchDelegateInfo;
  }
}
