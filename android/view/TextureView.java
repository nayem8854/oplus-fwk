package android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;

public class TextureView extends OplusBaseTextureView {
  private boolean mOpaque = true;
  
  private final Matrix mMatrix = new Matrix();
  
  private final Object[] mLock = new Object[0];
  
  private final Object[] mNativeWindowLock = new Object[0];
  
  private static final String LOG_TAG = "TextureView";
  
  private Canvas mCanvas;
  
  private boolean mHadSurface;
  
  private TextureLayer mLayer;
  
  private SurfaceTextureListener mListener;
  
  private boolean mMatrixChanged;
  
  private long mNativeWindow;
  
  private int mSaveCount;
  
  private SurfaceTexture mSurface;
  
  private boolean mUpdateLayer;
  
  private final SurfaceTexture.OnFrameAvailableListener mUpdateListener;
  
  private boolean mUpdateSurface;
  
  public TextureView(Context paramContext) {
    super(paramContext);
    this.mUpdateListener = new _$$Lambda$TextureView$WAq1rgfoZeDSt6cBQga7iQDymYk(this);
  }
  
  public TextureView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    this.mUpdateListener = new _$$Lambda$TextureView$WAq1rgfoZeDSt6cBQga7iQDymYk(this);
  }
  
  public TextureView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    this.mUpdateListener = new _$$Lambda$TextureView$WAq1rgfoZeDSt6cBQga7iQDymYk(this);
  }
  
  public TextureView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mUpdateListener = new _$$Lambda$TextureView$WAq1rgfoZeDSt6cBQga7iQDymYk(this);
  }
  
  public boolean isOpaque() {
    return this.mOpaque;
  }
  
  public void setOpaque(boolean paramBoolean) {
    if (paramBoolean != this.mOpaque) {
      this.mOpaque = paramBoolean;
      if (this.mLayer != null)
        updateLayerAndInvalidate(); 
    } 
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (!isHardwareAccelerated())
      Log.w("TextureView", "A TextureView or a subclass can only be used with hardware acceleration enabled."); 
    if (this.mHadSurface) {
      invalidate(true);
      this.mHadSurface = false;
    } 
  }
  
  protected void onDetachedFromWindowInternal() {
    destroyHardwareLayer();
    releaseSurfaceTexture();
    super.onDetachedFromWindowInternal();
  }
  
  protected void destroyHardwareResources() {
    super.destroyHardwareResources();
    destroyHardwareLayer();
  }
  
  private void destroyHardwareLayer() {
    TextureLayer textureLayer = this.mLayer;
    if (textureLayer != null) {
      textureLayer.detachSurfaceTexture();
      this.mLayer.destroy();
      this.mLayer = null;
      this.mMatrixChanged = true;
    } 
  }
  
  private void releaseSurfaceTexture() {
    SurfaceTexture surfaceTexture = this.mSurface;
    if (surfaceTexture != null) {
      boolean bool = true;
      SurfaceTextureListener surfaceTextureListener = this.mListener;
      if (surfaceTextureListener != null)
        bool = surfaceTextureListener.onSurfaceTextureDestroyed(surfaceTexture); 
      synchronized (this.mNativeWindowLock) {
        nDestroyNativeWindow();
        if (bool)
          this.mSurface.release(); 
        this.mSurface = null;
        this.mHadSurface = true;
      } 
    } 
  }
  
  public void setLayerType(int paramInt, Paint paramPaint) {
    setLayerPaint(paramPaint);
  }
  
  public void setLayerPaint(Paint paramPaint) {
    if (paramPaint != this.mLayerPaint) {
      this.mLayerPaint = paramPaint;
      invalidate();
    } 
  }
  
  public int getLayerType() {
    return 2;
  }
  
  public void buildLayer() {}
  
  public void setForeground(Drawable paramDrawable) {
    if (paramDrawable == null || sTextureViewIgnoresDrawableSetters)
      return; 
    throw new UnsupportedOperationException("TextureView doesn't support displaying a foreground drawable");
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable) {
    if (paramDrawable == null || sTextureViewIgnoresDrawableSetters)
      return; 
    throw new UnsupportedOperationException("TextureView doesn't support displaying a background drawable");
  }
  
  public final void draw(Canvas paramCanvas) {
    this.mPrivateFlags = this.mPrivateFlags & 0xFFDFFFFF | 0x20;
    if (paramCanvas.isHardwareAccelerated()) {
      RecordingCanvas recordingCanvas = (RecordingCanvas)paramCanvas;
      TextureLayer textureLayer = getTextureLayer();
      if (textureLayer != null) {
        applyUpdate();
        applyTransformMatrix();
        this.mLayer.setLayerPaint(this.mLayerPaint);
        recordingCanvas.drawTextureLayer(textureLayer);
      } 
    } 
  }
  
  protected final void onDraw(Canvas paramCanvas) {}
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    SurfaceTexture surfaceTexture = this.mSurface;
    if (surfaceTexture != null) {
      surfaceTexture.setDefaultBufferSize(getWidth(), getHeight());
      updateLayer();
      SurfaceTextureListener surfaceTextureListener = this.mListener;
      if (surfaceTextureListener != null)
        surfaceTextureListener.onSurfaceTextureSizeChanged(this.mSurface, getWidth(), getHeight()); 
    } 
  }
  
  TextureLayer getTextureLayer() {
    if (this.mLayer == null) {
      boolean bool;
      if (this.mAttachInfo == null || this.mAttachInfo.mThreadedRenderer == null)
        return null; 
      this.mLayer = this.mAttachInfo.mThreadedRenderer.createTextureLayer();
      if (this.mSurface == null) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        SurfaceTexture surfaceTexture = new SurfaceTexture(false);
        nCreateNativeWindow(surfaceTexture);
      } 
      this.mLayer.setSurfaceTexture(this.mSurface);
      this.mSurface.setDefaultBufferSize(getWidth(), getHeight());
      this.mSurface.setOnFrameAvailableListener(this.mUpdateListener, this.mAttachInfo.mHandler);
      SurfaceTextureListener surfaceTextureListener = this.mListener;
      if (surfaceTextureListener != null && bool)
        surfaceTextureListener.onSurfaceTextureAvailable(this.mSurface, getWidth(), getHeight()); 
      this.mLayer.setLayerPaint(this.mLayerPaint);
    } 
    if (this.mUpdateSurface) {
      this.mUpdateSurface = false;
      updateLayer();
      this.mMatrixChanged = true;
      this.mLayer.setSurfaceTexture(this.mSurface);
      this.mSurface.setDefaultBufferSize(getWidth(), getHeight());
    } 
    return this.mLayer;
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt) {
    super.onVisibilityChanged(paramView, paramInt);
    SurfaceTexture surfaceTexture = this.mSurface;
    if (surfaceTexture != null)
      if (paramInt == 0) {
        if (this.mLayer != null)
          surfaceTexture.setOnFrameAvailableListener(this.mUpdateListener, this.mAttachInfo.mHandler); 
        updateLayerAndInvalidate();
      } else {
        surfaceTexture.setOnFrameAvailableListener(null);
      }  
  }
  
  private void updateLayer() {
    synchronized (this.mLock) {
      this.mUpdateLayer = true;
      return;
    } 
  }
  
  private void updateLayerAndInvalidate() {
    synchronized (this.mLock) {
      this.mUpdateLayer = true;
      invalidate();
      return;
    } 
  }
  
  private void applyUpdate() {
    if (this.mLayer == null)
      return; 
    synchronized (this.mLock) {
      if (this.mUpdateLayer) {
        this.mUpdateLayer = false;
        this.mLayer.prepare(getWidth(), getHeight(), this.mOpaque);
        this.mLayer.updateSurfaceTexture();
        SurfaceTextureListener surfaceTextureListener = this.mListener;
        if (surfaceTextureListener != null)
          surfaceTextureListener.onSurfaceTextureUpdated(this.mSurface); 
        return;
      } 
      return;
    } 
  }
  
  public void setTransform(Matrix paramMatrix) {
    this.mMatrix.set(paramMatrix);
    this.mMatrixChanged = true;
    invalidateParentIfNeeded();
  }
  
  public Matrix getTransform(Matrix paramMatrix) {
    Matrix matrix = paramMatrix;
    if (paramMatrix == null)
      matrix = new Matrix(); 
    matrix.set(this.mMatrix);
    return matrix;
  }
  
  private void applyTransformMatrix() {
    if (this.mMatrixChanged) {
      TextureLayer textureLayer = this.mLayer;
      if (textureLayer != null) {
        textureLayer.setTransform(this.mMatrix);
        this.mMatrixChanged = false;
      } 
    } 
  }
  
  public Bitmap getBitmap() {
    return getBitmap(getWidth(), getHeight());
  }
  
  public Bitmap getBitmap(int paramInt1, int paramInt2) {
    if (isAvailable() && paramInt1 > 0 && paramInt2 > 0)
      return getBitmap(Bitmap.createBitmap(getResources().getDisplayMetrics(), paramInt1, paramInt2, Bitmap.Config.ARGB_8888)); 
    return null;
  }
  
  public Bitmap getBitmap(Bitmap paramBitmap) {
    if (paramBitmap != null && isAvailable()) {
      applyUpdate();
      applyTransformMatrix();
      if (this.mLayer == null && this.mUpdateSurface)
        getTextureLayer(); 
      TextureLayer textureLayer = this.mLayer;
      if (textureLayer != null)
        textureLayer.copyInto(paramBitmap); 
    } 
    return paramBitmap;
  }
  
  public boolean isAvailable() {
    boolean bool;
    if (this.mSurface != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Canvas lockCanvas() {
    return lockCanvas((Rect)null);
  }
  
  public Canvas lockCanvas(Rect paramRect) {
    if (!isAvailable())
      return null; 
    if (this.mCanvas == null)
      this.mCanvas = new Canvas(); 
    synchronized (this.mNativeWindowLock) {
      if (!nLockCanvas(this.mNativeWindow, this.mCanvas, paramRect))
        return null; 
      this.mSaveCount = this.mCanvas.save();
      return this.mCanvas;
    } 
  }
  
  public void unlockCanvasAndPost(Canvas paramCanvas) {
    Canvas canvas = this.mCanvas;
    if (canvas != null && paramCanvas == canvas) {
      paramCanvas.restoreToCount(this.mSaveCount);
      this.mSaveCount = 0;
      synchronized (this.mNativeWindowLock) {
        nUnlockCanvasAndPost(this.mNativeWindow, this.mCanvas);
      } 
    } 
  }
  
  public SurfaceTexture getSurfaceTexture() {
    return this.mSurface;
  }
  
  public void setSurfaceTexture(SurfaceTexture paramSurfaceTexture) {
    if (paramSurfaceTexture != null) {
      if (paramSurfaceTexture != this.mSurface) {
        if (!paramSurfaceTexture.isReleased()) {
          if (this.mSurface != null) {
            nDestroyNativeWindow();
            this.mSurface.release();
          } 
          this.mSurface = paramSurfaceTexture;
          nCreateNativeWindow(paramSurfaceTexture);
          if ((this.mViewFlags & 0xC) == 0 && this.mLayer != null)
            this.mSurface.setOnFrameAvailableListener(this.mUpdateListener, this.mAttachInfo.mHandler); 
          this.mUpdateSurface = true;
          invalidateParentIfNeeded();
          return;
        } 
        throw new IllegalArgumentException("Cannot setSurfaceTexture to a released SurfaceTexture");
      } 
      throw new IllegalArgumentException("Trying to setSurfaceTexture to the same SurfaceTexture that's already set.");
    } 
    throw new NullPointerException("surfaceTexture must not be null");
  }
  
  public SurfaceTextureListener getSurfaceTextureListener() {
    return this.mListener;
  }
  
  public void setSurfaceTextureListener(SurfaceTextureListener paramSurfaceTextureListener) {
    this.mListener = paramSurfaceTextureListener;
  }
  
  private native void nCreateNativeWindow(SurfaceTexture paramSurfaceTexture);
  
  private native void nDestroyNativeWindow();
  
  private static native boolean nLockCanvas(long paramLong, Canvas paramCanvas, Rect paramRect);
  
  private static native void nUnlockCanvasAndPost(long paramLong, Canvas paramCanvas);
  
  class SurfaceTextureListener {
    public abstract void onSurfaceTextureAvailable(SurfaceTexture param1SurfaceTexture, int param1Int1, int param1Int2);
    
    public abstract boolean onSurfaceTextureDestroyed(SurfaceTexture param1SurfaceTexture);
    
    public abstract void onSurfaceTextureSizeChanged(SurfaceTexture param1SurfaceTexture, int param1Int1, int param1Int2);
    
    public abstract void onSurfaceTextureUpdated(SurfaceTexture param1SurfaceTexture);
  }
}
