package android.view;

import android.graphics.Rect;

public class OplusLongshotViewContent {
  private final OplusLongshotViewContent mParent;
  
  private final Rect mRect;
  
  private final View mView;
  
  public OplusLongshotViewContent(View paramView, Rect paramRect, OplusLongshotViewContent paramOplusLongshotViewContent) {
    Rect rect = new Rect();
    this.mView = paramView;
    rect.set(paramRect);
    this.mParent = paramOplusLongshotViewContent;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{Parent=");
    stringBuilder.append(this.mParent);
    stringBuilder.append(":");
    stringBuilder.append(this.mRect);
    stringBuilder.append(":");
    stringBuilder.append(this.mView);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public OplusLongshotViewContent getParent() {
    return this.mParent;
  }
  
  public View getView() {
    return this.mView;
  }
  
  public Rect getRect() {
    return this.mRect;
  }
}
