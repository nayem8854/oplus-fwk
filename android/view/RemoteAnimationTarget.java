package android.view;

import android.app.WindowConfiguration;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.proto.ProtoOutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class RemoteAnimationTarget implements Parcelable {
  public RemoteAnimationTarget(int paramInt1, int paramInt2, SurfaceControl paramSurfaceControl1, boolean paramBoolean1, Rect paramRect1, Rect paramRect2, int paramInt3, Point paramPoint, Rect paramRect3, Rect paramRect4, WindowConfiguration paramWindowConfiguration, boolean paramBoolean2, SurfaceControl paramSurfaceControl2, Rect paramRect5) {
    Rect rect;
    this.mode = paramInt2;
    this.taskId = paramInt1;
    this.leash = paramSurfaceControl1;
    this.isTranslucent = paramBoolean1;
    this.clipRect = new Rect(paramRect1);
    this.contentInsets = new Rect(paramRect2);
    this.prefixOrderIndex = paramInt3;
    this.position = new Point(paramPoint);
    this.localBounds = new Rect(paramRect3);
    this.sourceContainerBounds = new Rect(paramRect4);
    this.screenSpaceBounds = new Rect(paramRect4);
    this.windowConfiguration = paramWindowConfiguration;
    this.isNotInRecents = paramBoolean2;
    this.startLeash = paramSurfaceControl2;
    if (paramRect5 == null) {
      paramSurfaceControl1 = null;
    } else {
      rect = new Rect(paramRect5);
    } 
    this.startBounds = rect;
  }
  
  public RemoteAnimationTarget(Parcel paramParcel) {
    this.taskId = paramParcel.readInt();
    this.mode = paramParcel.readInt();
    this.leash = (SurfaceControl)paramParcel.readParcelable(null);
    this.isTranslucent = paramParcel.readBoolean();
    this.clipRect = (Rect)paramParcel.readParcelable(null);
    this.contentInsets = (Rect)paramParcel.readParcelable(null);
    this.prefixOrderIndex = paramParcel.readInt();
    this.position = (Point)paramParcel.readParcelable(null);
    this.localBounds = (Rect)paramParcel.readParcelable(null);
    this.sourceContainerBounds = (Rect)paramParcel.readParcelable(null);
    this.screenSpaceBounds = (Rect)paramParcel.readParcelable(null);
    this.windowConfiguration = (WindowConfiguration)paramParcel.readParcelable(null);
    this.isNotInRecents = paramParcel.readBoolean();
    this.startLeash = (SurfaceControl)paramParcel.readParcelable(null);
    this.startBounds = (Rect)paramParcel.readParcelable(null);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.taskId);
    paramParcel.writeInt(this.mode);
    paramParcel.writeParcelable(this.leash, 0);
    paramParcel.writeBoolean(this.isTranslucent);
    paramParcel.writeParcelable((Parcelable)this.clipRect, 0);
    paramParcel.writeParcelable((Parcelable)this.contentInsets, 0);
    paramParcel.writeInt(this.prefixOrderIndex);
    paramParcel.writeParcelable((Parcelable)this.position, 0);
    paramParcel.writeParcelable((Parcelable)this.localBounds, 0);
    paramParcel.writeParcelable((Parcelable)this.sourceContainerBounds, 0);
    paramParcel.writeParcelable((Parcelable)this.screenSpaceBounds, 0);
    paramParcel.writeParcelable((Parcelable)this.windowConfiguration, 0);
    paramParcel.writeBoolean(this.isNotInRecents);
    paramParcel.writeParcelable(this.startLeash, 0);
    paramParcel.writeParcelable((Parcelable)this.startBounds, 0);
  }
  
  public void dump(PrintWriter paramPrintWriter, String paramString) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mode=");
    paramPrintWriter.print(this.mode);
    paramPrintWriter.print(" taskId=");
    paramPrintWriter.print(this.taskId);
    paramPrintWriter.print(" isTranslucent=");
    paramPrintWriter.print(this.isTranslucent);
    paramPrintWriter.print(" clipRect=");
    this.clipRect.printShortString(paramPrintWriter);
    paramPrintWriter.print(" contentInsets=");
    this.contentInsets.printShortString(paramPrintWriter);
    paramPrintWriter.print(" prefixOrderIndex=");
    paramPrintWriter.print(this.prefixOrderIndex);
    paramPrintWriter.print(" position=");
    this.position.printShortString(paramPrintWriter);
    paramPrintWriter.print(" sourceContainerBounds=");
    this.sourceContainerBounds.printShortString(paramPrintWriter);
    paramPrintWriter.print(" screenSpaceBounds=");
    this.screenSpaceBounds.printShortString(paramPrintWriter);
    paramPrintWriter.print(" localBounds=");
    this.localBounds.printShortString(paramPrintWriter);
    paramPrintWriter.println();
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("windowConfiguration=");
    paramPrintWriter.println(this.windowConfiguration);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("leash=");
    paramPrintWriter.println(this.leash);
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1120986464257L, this.taskId);
    paramProtoOutputStream.write(1120986464258L, this.mode);
    this.leash.dumpDebug(paramProtoOutputStream, 1146756268035L);
    paramProtoOutputStream.write(1133871366148L, this.isTranslucent);
    this.clipRect.dumpDebug(paramProtoOutputStream, 1146756268037L);
    this.contentInsets.dumpDebug(paramProtoOutputStream, 1146756268038L);
    paramProtoOutputStream.write(1120986464263L, this.prefixOrderIndex);
    this.position.dumpDebug(paramProtoOutputStream, 1146756268040L);
    this.sourceContainerBounds.dumpDebug(paramProtoOutputStream, 1146756268041L);
    this.screenSpaceBounds.dumpDebug(paramProtoOutputStream, 1146756268046L);
    this.localBounds.dumpDebug(paramProtoOutputStream, 1146756268045L);
    this.windowConfiguration.dumpDebug(paramProtoOutputStream, 1146756268042L);
    SurfaceControl surfaceControl = this.startLeash;
    if (surfaceControl != null)
      surfaceControl.dumpDebug(paramProtoOutputStream, 1146756268043L); 
    Rect rect = this.startBounds;
    if (rect != null)
      rect.dumpDebug(paramProtoOutputStream, 1146756268044L); 
    paramProtoOutputStream.end(paramLong);
  }
  
  public static final Parcelable.Creator<RemoteAnimationTarget> CREATOR = new Parcelable.Creator<RemoteAnimationTarget>() {
      public RemoteAnimationTarget createFromParcel(Parcel param1Parcel) {
        return new RemoteAnimationTarget(param1Parcel);
      }
      
      public RemoteAnimationTarget[] newArray(int param1Int) {
        return new RemoteAnimationTarget[param1Int];
      }
    };
  
  public static final int MODE_CHANGING = 2;
  
  public static final int MODE_CLOSING = 1;
  
  public static final int MODE_OPENING = 0;
  
  public final Rect clipRect;
  
  public final Rect contentInsets;
  
  public boolean isNotInRecents;
  
  public final boolean isTranslucent;
  
  public final SurfaceControl leash;
  
  public final Rect localBounds;
  
  public final int mode;
  
  @Deprecated
  public final Point position;
  
  public final int prefixOrderIndex;
  
  public final Rect screenSpaceBounds;
  
  @Deprecated
  public final Rect sourceContainerBounds;
  
  public final Rect startBounds;
  
  public final SurfaceControl startLeash;
  
  public final int taskId;
  
  public final WindowConfiguration windowConfiguration;
  
  @Retention(RetentionPolicy.SOURCE)
  class Mode implements Annotation {}
}
