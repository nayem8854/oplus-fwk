package android.view;

import android.content.ClipData;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.MergedConfiguration;
import java.util.ArrayList;
import java.util.List;

public interface IWindowSession extends IInterface {
  int addToDisplay(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, Rect paramRect3, DisplayCutout.ParcelableWrapper paramParcelableWrapper, InputChannel paramInputChannel, InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl) throws RemoteException;
  
  int addToDisplayAsUser(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, int paramInt4, Rect paramRect1, Rect paramRect2, Rect paramRect3, DisplayCutout.ParcelableWrapper paramParcelableWrapper, InputChannel paramInputChannel, InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl) throws RemoteException;
  
  int addToDisplayWithoutInputChannel(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, InsetsState paramInsetsState) throws RemoteException;
  
  void cancelDragAndDrop(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void dragRecipientEntered(IWindow paramIWindow) throws RemoteException;
  
  void dragRecipientExited(IWindow paramIWindow) throws RemoteException;
  
  void finishDrawing(IWindow paramIWindow, SurfaceControl.Transaction paramTransaction) throws RemoteException;
  
  void finishMovingTask(IWindow paramIWindow) throws RemoteException;
  
  void getDisplayFrame(IWindow paramIWindow, Rect paramRect) throws RemoteException;
  
  boolean getInTouchMode() throws RemoteException;
  
  void getVisibleDisplayFrame(IWindow paramIWindow, Rect paramRect) throws RemoteException;
  
  IWindowId getWindowId(IBinder paramIBinder) throws RemoteException;
  
  void grantInputChannel(int paramInt1, SurfaceControl paramSurfaceControl, IWindow paramIWindow, IBinder paramIBinder, int paramInt2, int paramInt3, InputChannel paramInputChannel) throws RemoteException;
  
  void insetsModified(IWindow paramIWindow, InsetsState paramInsetsState) throws RemoteException;
  
  void onRectangleOnScreenRequested(IBinder paramIBinder, Rect paramRect) throws RemoteException;
  
  boolean outOfMemory(IWindow paramIWindow) throws RemoteException;
  
  IBinder performDrag(IWindow paramIWindow, int paramInt1, SurfaceControl paramSurfaceControl, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, ClipData paramClipData) throws RemoteException;
  
  boolean performHapticFeedback(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void pokeDrawLock(IBinder paramIBinder) throws RemoteException;
  
  void prepareToReplaceWindows(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  int relayout(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, int paramInt4, int paramInt5, long paramLong, Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4, Rect paramRect5, DisplayCutout.ParcelableWrapper paramParcelableWrapper, MergedConfiguration paramMergedConfiguration, SurfaceControl paramSurfaceControl1, InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl, Point paramPoint, SurfaceControl paramSurfaceControl2) throws RemoteException;
  
  void remove(IWindow paramIWindow) throws RemoteException;
  
  void reparentDisplayContent(IWindow paramIWindow, SurfaceControl paramSurfaceControl, int paramInt) throws RemoteException;
  
  void reportDropResult(IWindow paramIWindow, boolean paramBoolean) throws RemoteException;
  
  void reportSystemGestureExclusionChanged(IWindow paramIWindow, List<Rect> paramList) throws RemoteException;
  
  Bundle sendWallpaperCommand(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean) throws RemoteException;
  
  void setInTouchMode(boolean paramBoolean) throws RemoteException;
  
  void setInsets(IWindow paramIWindow, int paramInt, Rect paramRect1, Rect paramRect2, Region paramRegion) throws RemoteException;
  
  void setShouldZoomOutWallpaper(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setTransparentRegion(IWindow paramIWindow, Region paramRegion) throws RemoteException;
  
  void setWallpaperDisplayOffset(IBinder paramIBinder, int paramInt1, int paramInt2) throws RemoteException;
  
  void setWallpaperPosition(IBinder paramIBinder, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) throws RemoteException;
  
  void setWallpaperZoomOut(IBinder paramIBinder, float paramFloat) throws RemoteException;
  
  boolean startMovingTask(IWindow paramIWindow, float paramFloat1, float paramFloat2) throws RemoteException;
  
  void updateDisplayContentLocation(IWindow paramIWindow, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void updateInputChannel(IBinder paramIBinder, int paramInt1, SurfaceControl paramSurfaceControl, int paramInt2, Region paramRegion) throws RemoteException;
  
  void updatePointerIcon(IWindow paramIWindow) throws RemoteException;
  
  void updateTapExcludeRegion(IWindow paramIWindow, Region paramRegion) throws RemoteException;
  
  void wallpaperCommandComplete(IBinder paramIBinder, Bundle paramBundle) throws RemoteException;
  
  void wallpaperOffsetsComplete(IBinder paramIBinder) throws RemoteException;
  
  class Default implements IWindowSession {
    public int addToDisplay(IWindow param1IWindow, int param1Int1, WindowManager.LayoutParams param1LayoutParams, int param1Int2, int param1Int3, Rect param1Rect1, Rect param1Rect2, Rect param1Rect3, DisplayCutout.ParcelableWrapper param1ParcelableWrapper, InputChannel param1InputChannel, InsetsState param1InsetsState, InsetsSourceControl[] param1ArrayOfInsetsSourceControl) throws RemoteException {
      return 0;
    }
    
    public int addToDisplayAsUser(IWindow param1IWindow, int param1Int1, WindowManager.LayoutParams param1LayoutParams, int param1Int2, int param1Int3, int param1Int4, Rect param1Rect1, Rect param1Rect2, Rect param1Rect3, DisplayCutout.ParcelableWrapper param1ParcelableWrapper, InputChannel param1InputChannel, InsetsState param1InsetsState, InsetsSourceControl[] param1ArrayOfInsetsSourceControl) throws RemoteException {
      return 0;
    }
    
    public int addToDisplayWithoutInputChannel(IWindow param1IWindow, int param1Int1, WindowManager.LayoutParams param1LayoutParams, int param1Int2, int param1Int3, Rect param1Rect1, Rect param1Rect2, InsetsState param1InsetsState) throws RemoteException {
      return 0;
    }
    
    public void remove(IWindow param1IWindow) throws RemoteException {}
    
    public int relayout(IWindow param1IWindow, int param1Int1, WindowManager.LayoutParams param1LayoutParams, int param1Int2, int param1Int3, int param1Int4, int param1Int5, long param1Long, Rect param1Rect1, Rect param1Rect2, Rect param1Rect3, Rect param1Rect4, Rect param1Rect5, DisplayCutout.ParcelableWrapper param1ParcelableWrapper, MergedConfiguration param1MergedConfiguration, SurfaceControl param1SurfaceControl1, InsetsState param1InsetsState, InsetsSourceControl[] param1ArrayOfInsetsSourceControl, Point param1Point, SurfaceControl param1SurfaceControl2) throws RemoteException {
      return 0;
    }
    
    public void prepareToReplaceWindows(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public boolean outOfMemory(IWindow param1IWindow) throws RemoteException {
      return false;
    }
    
    public void setTransparentRegion(IWindow param1IWindow, Region param1Region) throws RemoteException {}
    
    public void setInsets(IWindow param1IWindow, int param1Int, Rect param1Rect1, Rect param1Rect2, Region param1Region) throws RemoteException {}
    
    public void getDisplayFrame(IWindow param1IWindow, Rect param1Rect) throws RemoteException {}
    
    public void getVisibleDisplayFrame(IWindow param1IWindow, Rect param1Rect) throws RemoteException {}
    
    public void finishDrawing(IWindow param1IWindow, SurfaceControl.Transaction param1Transaction) throws RemoteException {}
    
    public void setInTouchMode(boolean param1Boolean) throws RemoteException {}
    
    public boolean getInTouchMode() throws RemoteException {
      return false;
    }
    
    public boolean performHapticFeedback(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public IBinder performDrag(IWindow param1IWindow, int param1Int1, SurfaceControl param1SurfaceControl, int param1Int2, float param1Float1, float param1Float2, float param1Float3, float param1Float4, ClipData param1ClipData) throws RemoteException {
      return null;
    }
    
    public void reportDropResult(IWindow param1IWindow, boolean param1Boolean) throws RemoteException {}
    
    public void cancelDragAndDrop(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void dragRecipientEntered(IWindow param1IWindow) throws RemoteException {}
    
    public void dragRecipientExited(IWindow param1IWindow) throws RemoteException {}
    
    public void setWallpaperPosition(IBinder param1IBinder, float param1Float1, float param1Float2, float param1Float3, float param1Float4) throws RemoteException {}
    
    public void setWallpaperZoomOut(IBinder param1IBinder, float param1Float) throws RemoteException {}
    
    public void setShouldZoomOutWallpaper(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void wallpaperOffsetsComplete(IBinder param1IBinder) throws RemoteException {}
    
    public void setWallpaperDisplayOffset(IBinder param1IBinder, int param1Int1, int param1Int2) throws RemoteException {}
    
    public Bundle sendWallpaperCommand(IBinder param1IBinder, String param1String, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void wallpaperCommandComplete(IBinder param1IBinder, Bundle param1Bundle) throws RemoteException {}
    
    public void onRectangleOnScreenRequested(IBinder param1IBinder, Rect param1Rect) throws RemoteException {}
    
    public IWindowId getWindowId(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void pokeDrawLock(IBinder param1IBinder) throws RemoteException {}
    
    public boolean startMovingTask(IWindow param1IWindow, float param1Float1, float param1Float2) throws RemoteException {
      return false;
    }
    
    public void finishMovingTask(IWindow param1IWindow) throws RemoteException {}
    
    public void updatePointerIcon(IWindow param1IWindow) throws RemoteException {}
    
    public void reparentDisplayContent(IWindow param1IWindow, SurfaceControl param1SurfaceControl, int param1Int) throws RemoteException {}
    
    public void updateDisplayContentLocation(IWindow param1IWindow, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void updateTapExcludeRegion(IWindow param1IWindow, Region param1Region) throws RemoteException {}
    
    public void insetsModified(IWindow param1IWindow, InsetsState param1InsetsState) throws RemoteException {}
    
    public void reportSystemGestureExclusionChanged(IWindow param1IWindow, List<Rect> param1List) throws RemoteException {}
    
    public void grantInputChannel(int param1Int1, SurfaceControl param1SurfaceControl, IWindow param1IWindow, IBinder param1IBinder, int param1Int2, int param1Int3, InputChannel param1InputChannel) throws RemoteException {}
    
    public void updateInputChannel(IBinder param1IBinder, int param1Int1, SurfaceControl param1SurfaceControl, int param1Int2, Region param1Region) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowSession {
    private static final String DESCRIPTOR = "android.view.IWindowSession";
    
    static final int TRANSACTION_addToDisplay = 1;
    
    static final int TRANSACTION_addToDisplayAsUser = 2;
    
    static final int TRANSACTION_addToDisplayWithoutInputChannel = 3;
    
    static final int TRANSACTION_cancelDragAndDrop = 18;
    
    static final int TRANSACTION_dragRecipientEntered = 19;
    
    static final int TRANSACTION_dragRecipientExited = 20;
    
    static final int TRANSACTION_finishDrawing = 12;
    
    static final int TRANSACTION_finishMovingTask = 32;
    
    static final int TRANSACTION_getDisplayFrame = 10;
    
    static final int TRANSACTION_getInTouchMode = 14;
    
    static final int TRANSACTION_getVisibleDisplayFrame = 11;
    
    static final int TRANSACTION_getWindowId = 29;
    
    static final int TRANSACTION_grantInputChannel = 39;
    
    static final int TRANSACTION_insetsModified = 37;
    
    static final int TRANSACTION_onRectangleOnScreenRequested = 28;
    
    static final int TRANSACTION_outOfMemory = 7;
    
    static final int TRANSACTION_performDrag = 16;
    
    static final int TRANSACTION_performHapticFeedback = 15;
    
    static final int TRANSACTION_pokeDrawLock = 30;
    
    static final int TRANSACTION_prepareToReplaceWindows = 6;
    
    static final int TRANSACTION_relayout = 5;
    
    static final int TRANSACTION_remove = 4;
    
    static final int TRANSACTION_reparentDisplayContent = 34;
    
    static final int TRANSACTION_reportDropResult = 17;
    
    static final int TRANSACTION_reportSystemGestureExclusionChanged = 38;
    
    static final int TRANSACTION_sendWallpaperCommand = 26;
    
    static final int TRANSACTION_setInTouchMode = 13;
    
    static final int TRANSACTION_setInsets = 9;
    
    static final int TRANSACTION_setShouldZoomOutWallpaper = 23;
    
    static final int TRANSACTION_setTransparentRegion = 8;
    
    static final int TRANSACTION_setWallpaperDisplayOffset = 25;
    
    static final int TRANSACTION_setWallpaperPosition = 21;
    
    static final int TRANSACTION_setWallpaperZoomOut = 22;
    
    static final int TRANSACTION_startMovingTask = 31;
    
    static final int TRANSACTION_updateDisplayContentLocation = 35;
    
    static final int TRANSACTION_updateInputChannel = 40;
    
    static final int TRANSACTION_updatePointerIcon = 33;
    
    static final int TRANSACTION_updateTapExcludeRegion = 36;
    
    static final int TRANSACTION_wallpaperCommandComplete = 27;
    
    static final int TRANSACTION_wallpaperOffsetsComplete = 24;
    
    public Stub() {
      attachInterface(this, "android.view.IWindowSession");
    }
    
    public static IWindowSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IWindowSession");
      if (iInterface != null && iInterface instanceof IWindowSession)
        return (IWindowSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 40:
          return "updateInputChannel";
        case 39:
          return "grantInputChannel";
        case 38:
          return "reportSystemGestureExclusionChanged";
        case 37:
          return "insetsModified";
        case 36:
          return "updateTapExcludeRegion";
        case 35:
          return "updateDisplayContentLocation";
        case 34:
          return "reparentDisplayContent";
        case 33:
          return "updatePointerIcon";
        case 32:
          return "finishMovingTask";
        case 31:
          return "startMovingTask";
        case 30:
          return "pokeDrawLock";
        case 29:
          return "getWindowId";
        case 28:
          return "onRectangleOnScreenRequested";
        case 27:
          return "wallpaperCommandComplete";
        case 26:
          return "sendWallpaperCommand";
        case 25:
          return "setWallpaperDisplayOffset";
        case 24:
          return "wallpaperOffsetsComplete";
        case 23:
          return "setShouldZoomOutWallpaper";
        case 22:
          return "setWallpaperZoomOut";
        case 21:
          return "setWallpaperPosition";
        case 20:
          return "dragRecipientExited";
        case 19:
          return "dragRecipientEntered";
        case 18:
          return "cancelDragAndDrop";
        case 17:
          return "reportDropResult";
        case 16:
          return "performDrag";
        case 15:
          return "performHapticFeedback";
        case 14:
          return "getInTouchMode";
        case 13:
          return "setInTouchMode";
        case 12:
          return "finishDrawing";
        case 11:
          return "getVisibleDisplayFrame";
        case 10:
          return "getDisplayFrame";
        case 9:
          return "setInsets";
        case 8:
          return "setTransparentRegion";
        case 7:
          return "outOfMemory";
        case 6:
          return "prepareToReplaceWindows";
        case 5:
          return "relayout";
        case 4:
          return "remove";
        case 3:
          return "addToDisplayWithoutInputChannel";
        case 2:
          return "addToDisplayAsUser";
        case 1:
          break;
      } 
      return "addToDisplay";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        InputChannel inputChannel1;
        ArrayList<Rect> arrayList;
        IWindow iWindow4;
        IBinder iBinder3;
        IWindowId iWindowId;
        Bundle bundle;
        IBinder iBinder2;
        IWindow iWindow3;
        IBinder iBinder1;
        Rect rect2;
        IWindow iWindow2;
        InsetsSourceControl[] arrayOfInsetsSourceControl2;
        IWindow iWindow1;
        Rect rect1;
        InsetsSourceControl[] arrayOfInsetsSourceControl1;
        IWindow iWindow5;
        IBinder iBinder4;
        IWindow iWindow7;
        String str;
        SurfaceControl surfaceControl1;
        IWindow iWindow10;
        IBinder iBinder6;
        IWindow iWindow9;
        Rect rect3;
        IWindow iWindow8;
        IBinder iBinder5, iBinder7;
        IWindow iWindow11;
        SurfaceControl surfaceControl2;
        float f1, f2, f3, f4;
        int n, i1;
        long l;
        Rect rect5;
        InsetsState insetsState1;
        Rect rect6, rect8;
        InsetsState insetsState2;
        Rect rect10;
        DisplayCutout.ParcelableWrapper parcelableWrapper1;
        Rect rect11;
        InputChannel inputChannel3;
        DisplayCutout.ParcelableWrapper parcelableWrapper3;
        MergedConfiguration mergedConfiguration;
        InsetsState insetsState4;
        Rect rect12;
        Point point;
        SurfaceControl surfaceControl3;
        boolean bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 40:
            param1Parcel1.enforceInterface("android.view.IWindowSession");
            iBinder4 = param1Parcel1.readStrongBinder();
            param1Int2 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              surfaceControl1 = (SurfaceControl)SurfaceControl.CREATOR.createFromParcel(param1Parcel1);
            } else {
              surfaceControl1 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Region region = (Region)Region.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            updateInputChannel(iBinder4, param1Int2, surfaceControl1, param1Int1, (Region)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            param1Parcel1.enforceInterface("android.view.IWindowSession");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              surfaceControl1 = (SurfaceControl)SurfaceControl.CREATOR.createFromParcel(param1Parcel1);
            } else {
              surfaceControl1 = null;
            } 
            iWindow7 = IWindow.Stub.asInterface(param1Parcel1.readStrongBinder());
            iBinder7 = param1Parcel1.readStrongBinder();
            param1Int2 = param1Parcel1.readInt();
            m = param1Parcel1.readInt();
            inputChannel1 = new InputChannel();
            grantInputChannel(param1Int1, surfaceControl1, iWindow7, iBinder7, param1Int2, m, inputChannel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(1);
            inputChannel1.writeToParcel(param1Parcel2, 1);
            return true;
          case 38:
            inputChannel1.enforceInterface("android.view.IWindowSession");
            iWindow5 = IWindow.Stub.asInterface(inputChannel1.readStrongBinder());
            arrayList = inputChannel1.createTypedArrayList(Rect.CREATOR);
            reportSystemGestureExclusionChanged(iWindow5, arrayList);
            return true;
          case 37:
            arrayList.enforceInterface("android.view.IWindowSession");
            iWindow5 = IWindow.Stub.asInterface(arrayList.readStrongBinder());
            if (arrayList.readInt() != 0) {
              InsetsState insetsState = (InsetsState)InsetsState.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            insetsModified(iWindow5, (InsetsState)arrayList);
            return true;
          case 36:
            arrayList.enforceInterface("android.view.IWindowSession");
            iWindow10 = IWindow.Stub.asInterface(arrayList.readStrongBinder());
            if (arrayList.readInt() != 0) {
              Region region = (Region)Region.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            updateTapExcludeRegion(iWindow10, (Region)arrayList);
            iWindow5.writeNoException();
            return true;
          case 35:
            arrayList.enforceInterface("android.view.IWindowSession");
            iWindow10 = IWindow.Stub.asInterface(arrayList.readStrongBinder());
            param1Int2 = arrayList.readInt();
            m = arrayList.readInt();
            param1Int1 = arrayList.readInt();
            updateDisplayContentLocation(iWindow10, param1Int2, m, param1Int1);
            iWindow5.writeNoException();
            return true;
          case 34:
            arrayList.enforceInterface("android.view.IWindowSession");
            iWindow7 = IWindow.Stub.asInterface(arrayList.readStrongBinder());
            if (arrayList.readInt() != 0) {
              SurfaceControl surfaceControl = (SurfaceControl)SurfaceControl.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              iWindow10 = null;
            } 
            param1Int1 = arrayList.readInt();
            reparentDisplayContent(iWindow7, (SurfaceControl)iWindow10, param1Int1);
            iWindow5.writeNoException();
            return true;
          case 33:
            arrayList.enforceInterface("android.view.IWindowSession");
            iWindow4 = IWindow.Stub.asInterface(arrayList.readStrongBinder());
            updatePointerIcon(iWindow4);
            iWindow5.writeNoException();
            return true;
          case 32:
            iWindow4.enforceInterface("android.view.IWindowSession");
            iWindow4 = IWindow.Stub.asInterface(iWindow4.readStrongBinder());
            finishMovingTask(iWindow4);
            iWindow5.writeNoException();
            return true;
          case 31:
            iWindow4.enforceInterface("android.view.IWindowSession");
            iWindow10 = IWindow.Stub.asInterface(iWindow4.readStrongBinder());
            f1 = iWindow4.readFloat();
            f2 = iWindow4.readFloat();
            bool3 = startMovingTask(iWindow10, f1, f2);
            iWindow5.writeNoException();
            iWindow5.writeInt(bool3);
            return true;
          case 30:
            iWindow4.enforceInterface("android.view.IWindowSession");
            iBinder3 = iWindow4.readStrongBinder();
            pokeDrawLock(iBinder3);
            iWindow5.writeNoException();
            return true;
          case 29:
            iBinder3.enforceInterface("android.view.IWindowSession");
            iBinder3 = iBinder3.readStrongBinder();
            iWindowId = getWindowId(iBinder3);
            iWindow5.writeNoException();
            if (iWindowId != null) {
              IBinder iBinder8 = iWindowId.asBinder();
            } else {
              iWindowId = null;
            } 
            iWindow5.writeStrongBinder((IBinder)iWindowId);
            return true;
          case 28:
            iWindowId.enforceInterface("android.view.IWindowSession");
            iBinder6 = iWindowId.readStrongBinder();
            if (iWindowId.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iWindowId);
            } else {
              iWindowId = null;
            } 
            onRectangleOnScreenRequested(iBinder6, (Rect)iWindowId);
            iWindow5.writeNoException();
            return true;
          case 27:
            iWindowId.enforceInterface("android.view.IWindowSession");
            iBinder6 = iWindowId.readStrongBinder();
            if (iWindowId.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iWindowId);
            } else {
              iWindowId = null;
            } 
            wallpaperCommandComplete(iBinder6, (Bundle)iWindowId);
            iWindow5.writeNoException();
            return true;
          case 26:
            iWindowId.enforceInterface("android.view.IWindowSession");
            iBinder7 = iWindowId.readStrongBinder();
            str = iWindowId.readString();
            m = iWindowId.readInt();
            k = iWindowId.readInt();
            param1Int2 = iWindowId.readInt();
            if (iWindowId.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iWindowId);
            } else {
              iBinder6 = null;
            } 
            if (iWindowId.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            bundle = sendWallpaperCommand(iBinder7, str, m, k, param1Int2, (Bundle)iBinder6, bool4);
            iWindow5.writeNoException();
            if (bundle != null) {
              iWindow5.writeInt(1);
              bundle.writeToParcel((Parcel)iWindow5, 1);
            } else {
              iWindow5.writeInt(0);
            } 
            return true;
          case 25:
            bundle.enforceInterface("android.view.IWindowSession");
            iBinder6 = bundle.readStrongBinder();
            param1Int2 = bundle.readInt();
            k = bundle.readInt();
            setWallpaperDisplayOffset(iBinder6, param1Int2, k);
            iWindow5.writeNoException();
            return true;
          case 24:
            bundle.enforceInterface("android.view.IWindowSession");
            iBinder2 = bundle.readStrongBinder();
            wallpaperOffsetsComplete(iBinder2);
            iWindow5.writeNoException();
            return true;
          case 23:
            iBinder2.enforceInterface("android.view.IWindowSession");
            iBinder6 = iBinder2.readStrongBinder();
            bool4 = bool9;
            if (iBinder2.readInt() != 0)
              bool4 = true; 
            setShouldZoomOutWallpaper(iBinder6, bool4);
            iWindow5.writeNoException();
            return true;
          case 22:
            iBinder2.enforceInterface("android.view.IWindowSession");
            iBinder = iBinder2.readStrongBinder();
            f2 = iBinder2.readFloat();
            setWallpaperZoomOut(iBinder, f2);
            return true;
          case 21:
            iBinder2.enforceInterface("android.view.IWindowSession");
            iBinder = iBinder2.readStrongBinder();
            f2 = iBinder2.readFloat();
            f3 = iBinder2.readFloat();
            f1 = iBinder2.readFloat();
            f4 = iBinder2.readFloat();
            setWallpaperPosition(iBinder, f2, f3, f1, f4);
            return true;
          case 20:
            iBinder2.enforceInterface("android.view.IWindowSession");
            iWindow3 = IWindow.Stub.asInterface(iBinder2.readStrongBinder());
            dragRecipientExited(iWindow3);
            iBinder.writeNoException();
            return true;
          case 19:
            iWindow3.enforceInterface("android.view.IWindowSession");
            iWindow3 = IWindow.Stub.asInterface(iWindow3.readStrongBinder());
            dragRecipientEntered(iWindow3);
            iBinder.writeNoException();
            return true;
          case 18:
            iWindow3.enforceInterface("android.view.IWindowSession");
            iBinder6 = iWindow3.readStrongBinder();
            if (iWindow3.readInt() != 0)
              bool4 = true; 
            cancelDragAndDrop(iBinder6, bool4);
            iBinder.writeNoException();
            return true;
          case 17:
            iWindow3.enforceInterface("android.view.IWindowSession");
            iWindow9 = IWindow.Stub.asInterface(iWindow3.readStrongBinder());
            bool4 = bool5;
            if (iWindow3.readInt() != 0)
              bool4 = true; 
            reportDropResult(iWindow9, bool4);
            iBinder.writeNoException();
            return true;
          case 16:
            iWindow3.enforceInterface("android.view.IWindowSession");
            iWindow6 = IWindow.Stub.asInterface(iWindow3.readStrongBinder());
            k = iWindow3.readInt();
            if (iWindow3.readInt() != 0) {
              SurfaceControl surfaceControl = (SurfaceControl)SurfaceControl.CREATOR.createFromParcel((Parcel)iWindow3);
            } else {
              iWindow9 = null;
            } 
            param1Int2 = iWindow3.readInt();
            f4 = iWindow3.readFloat();
            f3 = iWindow3.readFloat();
            f1 = iWindow3.readFloat();
            f2 = iWindow3.readFloat();
            if (iWindow3.readInt() != 0) {
              ClipData clipData = (ClipData)ClipData.CREATOR.createFromParcel((Parcel)iWindow3);
            } else {
              iWindow3 = null;
            } 
            iBinder1 = performDrag(iWindow6, k, (SurfaceControl)iWindow9, param1Int2, f4, f3, f1, f2, (ClipData)iWindow3);
            iBinder.writeNoException();
            iBinder.writeStrongBinder(iBinder1);
            return true;
          case 15:
            iBinder1.enforceInterface("android.view.IWindowSession");
            k = iBinder1.readInt();
            bool4 = bool6;
            if (iBinder1.readInt() != 0)
              bool4 = true; 
            bool2 = performHapticFeedback(k, bool4);
            iBinder.writeNoException();
            iBinder.writeInt(bool2);
            return true;
          case 14:
            iBinder1.enforceInterface("android.view.IWindowSession");
            bool2 = getInTouchMode();
            iBinder.writeNoException();
            iBinder.writeInt(bool2);
            return true;
          case 13:
            iBinder1.enforceInterface("android.view.IWindowSession");
            bool4 = bool7;
            if (iBinder1.readInt() != 0)
              bool4 = true; 
            setInTouchMode(bool4);
            iBinder.writeNoException();
            return true;
          case 12:
            iBinder1.enforceInterface("android.view.IWindowSession");
            iWindow9 = IWindow.Stub.asInterface(iBinder1.readStrongBinder());
            if (iBinder1.readInt() != 0) {
              SurfaceControl.Transaction transaction = (SurfaceControl.Transaction)SurfaceControl.Transaction.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder1 = null;
            } 
            finishDrawing(iWindow9, (SurfaceControl.Transaction)iBinder1);
            iBinder.writeNoException();
            return true;
          case 11:
            iBinder1.enforceInterface("android.view.IWindowSession");
            iWindow9 = IWindow.Stub.asInterface(iBinder1.readStrongBinder());
            rect2 = new Rect();
            getVisibleDisplayFrame(iWindow9, rect2);
            iBinder.writeNoException();
            iBinder.writeInt(1);
            rect2.writeToParcel((Parcel)iBinder, 1);
            return true;
          case 10:
            rect2.enforceInterface("android.view.IWindowSession");
            iWindow2 = IWindow.Stub.asInterface(rect2.readStrongBinder());
            rect3 = new Rect();
            getDisplayFrame(iWindow2, rect3);
            iBinder.writeNoException();
            iBinder.writeInt(1);
            rect3.writeToParcel((Parcel)iBinder, 1);
            return true;
          case 9:
            iWindow2.enforceInterface("android.view.IWindowSession");
            iWindow11 = IWindow.Stub.asInterface(iWindow2.readStrongBinder());
            j = iWindow2.readInt();
            if (iWindow2.readInt() != 0) {
              rect3 = (Rect)Rect.CREATOR.createFromParcel((Parcel)iWindow2);
            } else {
              rect3 = null;
            } 
            if (iWindow2.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iWindow2);
            } else {
              iWindow6 = null;
            } 
            if (iWindow2.readInt() != 0) {
              Region region = (Region)Region.CREATOR.createFromParcel((Parcel)iWindow2);
            } else {
              iWindow2 = null;
            } 
            setInsets(iWindow11, j, rect3, (Rect)iWindow6, (Region)iWindow2);
            iBinder.writeNoException();
            return true;
          case 8:
            iWindow2.enforceInterface("android.view.IWindowSession");
            iWindow8 = IWindow.Stub.asInterface(iWindow2.readStrongBinder());
            if (iWindow2.readInt() != 0) {
              Region region = (Region)Region.CREATOR.createFromParcel((Parcel)iWindow2);
            } else {
              iWindow2 = null;
            } 
            setTransparentRegion(iWindow8, (Region)iWindow2);
            iBinder.writeNoException();
            return true;
          case 7:
            iWindow2.enforceInterface("android.view.IWindowSession");
            iWindow2 = IWindow.Stub.asInterface(iWindow2.readStrongBinder());
            bool1 = outOfMemory(iWindow2);
            iBinder.writeNoException();
            iBinder.writeInt(bool1);
            return true;
          case 6:
            iWindow2.enforceInterface("android.view.IWindowSession");
            iBinder5 = iWindow2.readStrongBinder();
            bool4 = bool8;
            if (iWindow2.readInt() != 0)
              bool4 = true; 
            prepareToReplaceWindows(iBinder5, bool4);
            iBinder.writeNoException();
            return true;
          case 5:
            iWindow2.enforceInterface("android.view.IWindowSession");
            iWindow6 = IWindow.Stub.asInterface(iWindow2.readStrongBinder());
            n = iWindow2.readInt();
            if (iWindow2.readInt() != 0) {
              WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel((Parcel)iWindow2);
            } else {
              iBinder5 = null;
            } 
            i = iWindow2.readInt();
            m = iWindow2.readInt();
            i1 = iWindow2.readInt();
            i2 = iWindow2.readInt();
            l = iWindow2.readLong();
            rect5 = new Rect();
            rect6 = new Rect();
            rect8 = new Rect();
            rect10 = new Rect();
            rect11 = new Rect();
            parcelableWrapper3 = new DisplayCutout.ParcelableWrapper();
            mergedConfiguration = new MergedConfiguration();
            surfaceControl2 = new SurfaceControl();
            insetsState4 = new InsetsState();
            param1Int2 = iWindow2.readInt();
            if (param1Int2 < 0) {
              iWindow2 = null;
            } else {
              arrayOfInsetsSourceControl2 = new InsetsSourceControl[param1Int2];
            } 
            point = new Point();
            surfaceControl3 = new SurfaceControl();
            i = relayout(iWindow6, n, (WindowManager.LayoutParams)iBinder5, i, m, i1, i2, l, rect5, rect6, rect8, rect10, rect11, parcelableWrapper3, mergedConfiguration, surfaceControl2, insetsState4, arrayOfInsetsSourceControl2, point, surfaceControl3);
            iBinder.writeNoException();
            iBinder.writeInt(i);
            iBinder.writeInt(1);
            rect5.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            rect6.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            rect8.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            rect10.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            rect11.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            parcelableWrapper3.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            mergedConfiguration.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            surfaceControl2.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            insetsState4.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeTypedArray((Parcelable[])arrayOfInsetsSourceControl2, 1);
            iBinder.writeInt(1);
            point.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            surfaceControl3.writeToParcel((Parcel)iBinder, 1);
            return true;
          case 4:
            arrayOfInsetsSourceControl2.enforceInterface("android.view.IWindowSession");
            iWindow1 = IWindow.Stub.asInterface(arrayOfInsetsSourceControl2.readStrongBinder());
            remove(iWindow1);
            iBinder.writeNoException();
            return true;
          case 3:
            iWindow1.enforceInterface("android.view.IWindowSession");
            iWindow6 = IWindow.Stub.asInterface(iWindow1.readStrongBinder());
            i = iWindow1.readInt();
            if (iWindow1.readInt() != 0) {
              WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel((Parcel)iWindow1);
            } else {
              iBinder5 = null;
            } 
            m = iWindow1.readInt();
            param1Int2 = iWindow1.readInt();
            rect4 = new Rect();
            rect1 = new Rect();
            insetsState2 = new InsetsState();
            i = addToDisplayWithoutInputChannel(iWindow6, i, (WindowManager.LayoutParams)iBinder5, m, param1Int2, rect4, rect1, insetsState2);
            iBinder.writeNoException();
            iBinder.writeInt(i);
            iBinder.writeInt(1);
            rect4.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            rect1.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            insetsState2.writeToParcel((Parcel)iBinder, 1);
            return true;
          case 2:
            rect1.enforceInterface("android.view.IWindowSession");
            iWindow6 = IWindow.Stub.asInterface(rect1.readStrongBinder());
            param1Int2 = rect1.readInt();
            if (rect1.readInt() != 0) {
              WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel((Parcel)rect1);
            } else {
              iBinder5 = null;
            } 
            i = rect1.readInt();
            i1 = rect1.readInt();
            i2 = rect1.readInt();
            rect7 = new Rect();
            rect4 = new Rect();
            rect12 = new Rect();
            parcelableWrapper1 = new DisplayCutout.ParcelableWrapper();
            inputChannel3 = new InputChannel();
            insetsState1 = new InsetsState();
            m = rect1.readInt();
            if (m < 0) {
              rect1 = null;
            } else {
              arrayOfInsetsSourceControl1 = new InsetsSourceControl[m];
            } 
            i = addToDisplayAsUser(iWindow6, param1Int2, (WindowManager.LayoutParams)iBinder5, i, i1, i2, rect7, rect4, rect12, parcelableWrapper1, inputChannel3, insetsState1, arrayOfInsetsSourceControl1);
            iBinder.writeNoException();
            iBinder.writeInt(i);
            iBinder.writeInt(1);
            rect7.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            rect4.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            rect12.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            parcelableWrapper1.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            inputChannel3.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeInt(1);
            insetsState1.writeToParcel((Parcel)iBinder, 1);
            iBinder.writeTypedArray((Parcelable[])arrayOfInsetsSourceControl1, 1);
            return true;
          case 1:
            break;
        } 
        arrayOfInsetsSourceControl1.enforceInterface("android.view.IWindowSession");
        IWindow iWindow6 = IWindow.Stub.asInterface(arrayOfInsetsSourceControl1.readStrongBinder());
        int i2 = arrayOfInsetsSourceControl1.readInt();
        if (arrayOfInsetsSourceControl1.readInt() != 0) {
          WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl1);
        } else {
          iBinder5 = null;
        } 
        param1Int2 = arrayOfInsetsSourceControl1.readInt();
        int m = arrayOfInsetsSourceControl1.readInt();
        Rect rect7 = new Rect();
        Rect rect9 = new Rect();
        Rect rect4 = new Rect();
        DisplayCutout.ParcelableWrapper parcelableWrapper2 = new DisplayCutout.ParcelableWrapper();
        InputChannel inputChannel2 = new InputChannel();
        InsetsState insetsState3 = new InsetsState();
        int i = arrayOfInsetsSourceControl1.readInt();
        if (i < 0) {
          arrayOfInsetsSourceControl1 = null;
        } else {
          arrayOfInsetsSourceControl1 = new InsetsSourceControl[i];
        } 
        i = addToDisplay(iWindow6, i2, (WindowManager.LayoutParams)iBinder5, param1Int2, m, rect7, rect9, rect4, parcelableWrapper2, inputChannel2, insetsState3, arrayOfInsetsSourceControl1);
        iBinder.writeNoException();
        iBinder.writeInt(i);
        iBinder.writeInt(1);
        rect7.writeToParcel((Parcel)iBinder, 1);
        iBinder.writeInt(1);
        rect9.writeToParcel((Parcel)iBinder, 1);
        iBinder.writeInt(1);
        rect4.writeToParcel((Parcel)iBinder, 1);
        iBinder.writeInt(1);
        parcelableWrapper2.writeToParcel((Parcel)iBinder, 1);
        iBinder.writeInt(1);
        inputChannel2.writeToParcel((Parcel)iBinder, 1);
        iBinder.writeInt(1);
        insetsState3.writeToParcel((Parcel)iBinder, 1);
        iBinder.writeTypedArray((Parcelable[])arrayOfInsetsSourceControl1, 1);
        return true;
      } 
      iBinder.writeString("android.view.IWindowSession");
      return true;
    }
    
    private static class Proxy implements IWindowSession {
      public static IWindowSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IWindowSession";
      }
      
      public int addToDisplay(IWindow param2IWindow, int param2Int1, WindowManager.LayoutParams param2LayoutParams, int param2Int2, int param2Int3, Rect param2Rect1, Rect param2Rect2, Rect param2Rect3, DisplayCutout.ParcelableWrapper param2ParcelableWrapper, InputChannel param2InputChannel, InsetsState param2InsetsState, InsetsSourceControl[] param2ArrayOfInsetsSourceControl) throws RemoteException {
        // Byte code:
        //   0: invokestatic obtain : ()Landroid/os/Parcel;
        //   3: astore #13
        //   5: invokestatic obtain : ()Landroid/os/Parcel;
        //   8: astore #14
        //   10: aload #13
        //   12: ldc 'android.view.IWindowSession'
        //   14: invokevirtual writeInterfaceToken : (Ljava/lang/String;)V
        //   17: aload_1
        //   18: ifnull -> 36
        //   21: aload_1
        //   22: invokeinterface asBinder : ()Landroid/os/IBinder;
        //   27: astore #15
        //   29: goto -> 39
        //   32: astore_1
        //   33: goto -> 390
        //   36: aconst_null
        //   37: astore #15
        //   39: aload #13
        //   41: aload #15
        //   43: invokevirtual writeStrongBinder : (Landroid/os/IBinder;)V
        //   46: aload #13
        //   48: iload_2
        //   49: invokevirtual writeInt : (I)V
        //   52: aload_3
        //   53: ifnull -> 72
        //   56: aload #13
        //   58: iconst_1
        //   59: invokevirtual writeInt : (I)V
        //   62: aload_3
        //   63: aload #13
        //   65: iconst_0
        //   66: invokevirtual writeToParcel : (Landroid/os/Parcel;I)V
        //   69: goto -> 78
        //   72: aload #13
        //   74: iconst_0
        //   75: invokevirtual writeInt : (I)V
        //   78: aload #13
        //   80: iload #4
        //   82: invokevirtual writeInt : (I)V
        //   85: aload #13
        //   87: iload #5
        //   89: invokevirtual writeInt : (I)V
        //   92: aload #12
        //   94: ifnonnull -> 106
        //   97: aload #13
        //   99: iconst_m1
        //   100: invokevirtual writeInt : (I)V
        //   103: goto -> 114
        //   106: aload #13
        //   108: aload #12
        //   110: arraylength
        //   111: invokevirtual writeInt : (I)V
        //   114: aload_0
        //   115: getfield mRemote : Landroid/os/IBinder;
        //   118: iconst_1
        //   119: aload #13
        //   121: aload #14
        //   123: iconst_0
        //   124: invokeinterface transact : (ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
        //   129: istore #16
        //   131: iload #16
        //   133: ifne -> 196
        //   136: invokestatic getDefaultImpl : ()Landroid/view/IWindowSession;
        //   139: ifnull -> 196
        //   142: invokestatic getDefaultImpl : ()Landroid/view/IWindowSession;
        //   145: astore #15
        //   147: aload #15
        //   149: aload_1
        //   150: iload_2
        //   151: aload_3
        //   152: iload #4
        //   154: iload #5
        //   156: aload #6
        //   158: aload #7
        //   160: aload #8
        //   162: aload #9
        //   164: aload #10
        //   166: aload #11
        //   168: aload #12
        //   170: invokeinterface addToDisplay : (Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/DisplayCutout$ParcelableWrapper;Landroid/view/InputChannel;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;)I
        //   175: istore_2
        //   176: aload #14
        //   178: invokevirtual recycle : ()V
        //   181: aload #13
        //   183: invokevirtual recycle : ()V
        //   186: iload_2
        //   187: ireturn
        //   188: astore_1
        //   189: goto -> 390
        //   192: astore_1
        //   193: goto -> 390
        //   196: aload #14
        //   198: invokevirtual readException : ()V
        //   201: aload #14
        //   203: invokevirtual readInt : ()I
        //   206: istore_2
        //   207: aload #14
        //   209: invokevirtual readInt : ()I
        //   212: istore #4
        //   214: iload #4
        //   216: ifeq -> 229
        //   219: aload #6
        //   221: aload #14
        //   223: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   226: goto -> 229
        //   229: aload #14
        //   231: invokevirtual readInt : ()I
        //   234: istore #4
        //   236: iload #4
        //   238: ifeq -> 251
        //   241: aload #7
        //   243: aload #14
        //   245: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   248: goto -> 251
        //   251: aload #14
        //   253: invokevirtual readInt : ()I
        //   256: istore #4
        //   258: iload #4
        //   260: ifeq -> 273
        //   263: aload #8
        //   265: aload #14
        //   267: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   270: goto -> 273
        //   273: aload #14
        //   275: invokevirtual readInt : ()I
        //   278: istore #4
        //   280: iload #4
        //   282: ifeq -> 295
        //   285: aload #9
        //   287: aload #14
        //   289: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   292: goto -> 295
        //   295: aload #14
        //   297: invokevirtual readInt : ()I
        //   300: istore #4
        //   302: iload #4
        //   304: ifeq -> 317
        //   307: aload #10
        //   309: aload #14
        //   311: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   314: goto -> 317
        //   317: aload #14
        //   319: invokevirtual readInt : ()I
        //   322: istore #4
        //   324: iload #4
        //   326: ifeq -> 339
        //   329: aload #11
        //   331: aload #14
        //   333: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   336: goto -> 339
        //   339: aload #14
        //   341: aload #12
        //   343: getstatic android/view/InsetsSourceControl.CREATOR : Landroid/os/Parcelable$Creator;
        //   346: invokevirtual readTypedArray : ([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V
        //   349: aload #14
        //   351: invokevirtual recycle : ()V
        //   354: aload #13
        //   356: invokevirtual recycle : ()V
        //   359: iload_2
        //   360: ireturn
        //   361: astore_1
        //   362: goto -> 390
        //   365: astore_1
        //   366: goto -> 382
        //   369: astore_1
        //   370: goto -> 382
        //   373: astore_1
        //   374: goto -> 382
        //   377: astore_1
        //   378: goto -> 382
        //   381: astore_1
        //   382: goto -> 390
        //   385: astore_1
        //   386: goto -> 390
        //   389: astore_1
        //   390: aload #14
        //   392: invokevirtual recycle : ()V
        //   395: aload #13
        //   397: invokevirtual recycle : ()V
        //   400: aload_1
        //   401: athrow
        // Line number table:
        //   Java source line number -> byte code offset
        //   #1492	-> 0
        //   #1493	-> 5
        //   #1496	-> 10
        //   #1497	-> 17
        //   #1541	-> 32
        //   #1497	-> 36
        //   #1498	-> 46
        //   #1499	-> 52
        //   #1500	-> 56
        //   #1501	-> 62
        //   #1504	-> 72
        //   #1506	-> 78
        //   #1507	-> 85
        //   #1508	-> 92
        //   #1509	-> 97
        //   #1512	-> 106
        //   #1514	-> 114
        //   #1515	-> 131
        //   #1516	-> 142
        //   #1541	-> 176
        //   #1542	-> 181
        //   #1516	-> 186
        //   #1541	-> 188
        //   #1515	-> 196
        //   #1518	-> 196
        //   #1519	-> 201
        //   #1520	-> 207
        //   #1521	-> 219
        //   #1520	-> 229
        //   #1523	-> 229
        //   #1524	-> 241
        //   #1523	-> 251
        //   #1526	-> 251
        //   #1527	-> 263
        //   #1526	-> 273
        //   #1529	-> 273
        //   #1530	-> 285
        //   #1529	-> 295
        //   #1532	-> 295
        //   #1533	-> 307
        //   #1532	-> 317
        //   #1535	-> 317
        //   #1536	-> 329
        //   #1535	-> 339
        //   #1538	-> 339
        //   #1541	-> 349
        //   #1542	-> 354
        //   #1543	-> 359
        //   #1544	-> 359
        //   #1541	-> 361
        //   #1542	-> 395
        //   #1543	-> 400
        // Exception table:
        //   from	to	target	type
        //   10	17	389	finally
        //   21	29	32	finally
        //   39	46	389	finally
        //   46	52	389	finally
        //   56	62	32	finally
        //   62	69	32	finally
        //   72	78	389	finally
        //   78	85	389	finally
        //   85	92	389	finally
        //   97	103	32	finally
        //   106	114	389	finally
        //   114	131	389	finally
        //   136	142	192	finally
        //   142	147	192	finally
        //   147	176	188	finally
        //   196	201	385	finally
        //   201	207	385	finally
        //   207	214	385	finally
        //   219	226	381	finally
        //   229	236	381	finally
        //   241	248	377	finally
        //   251	258	377	finally
        //   263	270	373	finally
        //   273	280	373	finally
        //   285	292	369	finally
        //   295	302	369	finally
        //   307	314	365	finally
        //   317	324	365	finally
        //   329	336	361	finally
        //   339	349	361	finally
      }
      
      public int addToDisplayAsUser(IWindow param2IWindow, int param2Int1, WindowManager.LayoutParams param2LayoutParams, int param2Int2, int param2Int3, int param2Int4, Rect param2Rect1, Rect param2Rect2, Rect param2Rect3, DisplayCutout.ParcelableWrapper param2ParcelableWrapper, InputChannel param2InputChannel, InsetsState param2InsetsState, InsetsSourceControl[] param2ArrayOfInsetsSourceControl) throws RemoteException {
        // Byte code:
        //   0: invokestatic obtain : ()Landroid/os/Parcel;
        //   3: astore #14
        //   5: invokestatic obtain : ()Landroid/os/Parcel;
        //   8: astore #15
        //   10: aload #14
        //   12: ldc 'android.view.IWindowSession'
        //   14: invokevirtual writeInterfaceToken : (Ljava/lang/String;)V
        //   17: aload_1
        //   18: ifnull -> 36
        //   21: aload_1
        //   22: invokeinterface asBinder : ()Landroid/os/IBinder;
        //   27: astore #16
        //   29: goto -> 39
        //   32: astore_1
        //   33: goto -> 399
        //   36: aconst_null
        //   37: astore #16
        //   39: aload #14
        //   41: aload #16
        //   43: invokevirtual writeStrongBinder : (Landroid/os/IBinder;)V
        //   46: aload #14
        //   48: iload_2
        //   49: invokevirtual writeInt : (I)V
        //   52: aload_3
        //   53: ifnull -> 72
        //   56: aload #14
        //   58: iconst_1
        //   59: invokevirtual writeInt : (I)V
        //   62: aload_3
        //   63: aload #14
        //   65: iconst_0
        //   66: invokevirtual writeToParcel : (Landroid/os/Parcel;I)V
        //   69: goto -> 78
        //   72: aload #14
        //   74: iconst_0
        //   75: invokevirtual writeInt : (I)V
        //   78: aload #14
        //   80: iload #4
        //   82: invokevirtual writeInt : (I)V
        //   85: aload #14
        //   87: iload #5
        //   89: invokevirtual writeInt : (I)V
        //   92: aload #14
        //   94: iload #6
        //   96: invokevirtual writeInt : (I)V
        //   99: aload #13
        //   101: ifnonnull -> 113
        //   104: aload #14
        //   106: iconst_m1
        //   107: invokevirtual writeInt : (I)V
        //   110: goto -> 121
        //   113: aload #14
        //   115: aload #13
        //   117: arraylength
        //   118: invokevirtual writeInt : (I)V
        //   121: aload_0
        //   122: getfield mRemote : Landroid/os/IBinder;
        //   125: iconst_2
        //   126: aload #14
        //   128: aload #15
        //   130: iconst_0
        //   131: invokeinterface transact : (ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
        //   136: istore #17
        //   138: iload #17
        //   140: ifne -> 205
        //   143: invokestatic getDefaultImpl : ()Landroid/view/IWindowSession;
        //   146: ifnull -> 205
        //   149: invokestatic getDefaultImpl : ()Landroid/view/IWindowSession;
        //   152: astore #16
        //   154: aload #16
        //   156: aload_1
        //   157: iload_2
        //   158: aload_3
        //   159: iload #4
        //   161: iload #5
        //   163: iload #6
        //   165: aload #7
        //   167: aload #8
        //   169: aload #9
        //   171: aload #10
        //   173: aload #11
        //   175: aload #12
        //   177: aload #13
        //   179: invokeinterface addToDisplayAsUser : (Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/DisplayCutout$ParcelableWrapper;Landroid/view/InputChannel;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;)I
        //   184: istore_2
        //   185: aload #15
        //   187: invokevirtual recycle : ()V
        //   190: aload #14
        //   192: invokevirtual recycle : ()V
        //   195: iload_2
        //   196: ireturn
        //   197: astore_1
        //   198: goto -> 399
        //   201: astore_1
        //   202: goto -> 399
        //   205: aload #15
        //   207: invokevirtual readException : ()V
        //   210: aload #15
        //   212: invokevirtual readInt : ()I
        //   215: istore_2
        //   216: aload #15
        //   218: invokevirtual readInt : ()I
        //   221: istore #4
        //   223: iload #4
        //   225: ifeq -> 238
        //   228: aload #7
        //   230: aload #15
        //   232: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   235: goto -> 238
        //   238: aload #15
        //   240: invokevirtual readInt : ()I
        //   243: istore #4
        //   245: iload #4
        //   247: ifeq -> 260
        //   250: aload #8
        //   252: aload #15
        //   254: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   257: goto -> 260
        //   260: aload #15
        //   262: invokevirtual readInt : ()I
        //   265: istore #4
        //   267: iload #4
        //   269: ifeq -> 282
        //   272: aload #9
        //   274: aload #15
        //   276: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   279: goto -> 282
        //   282: aload #15
        //   284: invokevirtual readInt : ()I
        //   287: istore #4
        //   289: iload #4
        //   291: ifeq -> 304
        //   294: aload #10
        //   296: aload #15
        //   298: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   301: goto -> 304
        //   304: aload #15
        //   306: invokevirtual readInt : ()I
        //   309: istore #4
        //   311: iload #4
        //   313: ifeq -> 326
        //   316: aload #11
        //   318: aload #15
        //   320: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   323: goto -> 326
        //   326: aload #15
        //   328: invokevirtual readInt : ()I
        //   331: istore #4
        //   333: iload #4
        //   335: ifeq -> 348
        //   338: aload #12
        //   340: aload #15
        //   342: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   345: goto -> 348
        //   348: aload #15
        //   350: aload #13
        //   352: getstatic android/view/InsetsSourceControl.CREATOR : Landroid/os/Parcelable$Creator;
        //   355: invokevirtual readTypedArray : ([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V
        //   358: aload #15
        //   360: invokevirtual recycle : ()V
        //   363: aload #14
        //   365: invokevirtual recycle : ()V
        //   368: iload_2
        //   369: ireturn
        //   370: astore_1
        //   371: goto -> 399
        //   374: astore_1
        //   375: goto -> 391
        //   378: astore_1
        //   379: goto -> 391
        //   382: astore_1
        //   383: goto -> 391
        //   386: astore_1
        //   387: goto -> 391
        //   390: astore_1
        //   391: goto -> 399
        //   394: astore_1
        //   395: goto -> 399
        //   398: astore_1
        //   399: aload #15
        //   401: invokevirtual recycle : ()V
        //   404: aload #14
        //   406: invokevirtual recycle : ()V
        //   409: aload_1
        //   410: athrow
        // Line number table:
        //   Java source line number -> byte code offset
        //   #1548	-> 0
        //   #1549	-> 5
        //   #1552	-> 10
        //   #1553	-> 17
        //   #1598	-> 32
        //   #1553	-> 36
        //   #1554	-> 46
        //   #1555	-> 52
        //   #1556	-> 56
        //   #1557	-> 62
        //   #1560	-> 72
        //   #1562	-> 78
        //   #1563	-> 85
        //   #1564	-> 92
        //   #1565	-> 99
        //   #1566	-> 104
        //   #1569	-> 113
        //   #1571	-> 121
        //   #1572	-> 138
        //   #1573	-> 149
        //   #1598	-> 185
        //   #1599	-> 190
        //   #1573	-> 195
        //   #1598	-> 197
        //   #1572	-> 205
        //   #1575	-> 205
        //   #1576	-> 210
        //   #1577	-> 216
        //   #1578	-> 228
        //   #1577	-> 238
        //   #1580	-> 238
        //   #1581	-> 250
        //   #1580	-> 260
        //   #1583	-> 260
        //   #1584	-> 272
        //   #1583	-> 282
        //   #1586	-> 282
        //   #1587	-> 294
        //   #1586	-> 304
        //   #1589	-> 304
        //   #1590	-> 316
        //   #1589	-> 326
        //   #1592	-> 326
        //   #1593	-> 338
        //   #1592	-> 348
        //   #1595	-> 348
        //   #1598	-> 358
        //   #1599	-> 363
        //   #1600	-> 368
        //   #1601	-> 368
        //   #1598	-> 370
        //   #1599	-> 404
        //   #1600	-> 409
        // Exception table:
        //   from	to	target	type
        //   10	17	398	finally
        //   21	29	32	finally
        //   39	46	398	finally
        //   46	52	398	finally
        //   56	62	32	finally
        //   62	69	32	finally
        //   72	78	398	finally
        //   78	85	398	finally
        //   85	92	398	finally
        //   92	99	398	finally
        //   104	110	32	finally
        //   113	121	398	finally
        //   121	138	398	finally
        //   143	149	201	finally
        //   149	154	201	finally
        //   154	185	197	finally
        //   205	210	394	finally
        //   210	216	394	finally
        //   216	223	394	finally
        //   228	235	390	finally
        //   238	245	390	finally
        //   250	257	386	finally
        //   260	267	386	finally
        //   272	279	382	finally
        //   282	289	382	finally
        //   294	301	378	finally
        //   304	311	378	finally
        //   316	323	374	finally
        //   326	333	374	finally
        //   338	345	370	finally
        //   348	358	370	finally
      }
      
      public int addToDisplayWithoutInputChannel(IWindow param2IWindow, int param2Int1, WindowManager.LayoutParams param2LayoutParams, int param2Int2, int param2Int3, Rect param2Rect1, Rect param2Rect2, InsetsState param2InsetsState) throws RemoteException {
        // Byte code:
        //   0: invokestatic obtain : ()Landroid/os/Parcel;
        //   3: astore #9
        //   5: invokestatic obtain : ()Landroid/os/Parcel;
        //   8: astore #10
        //   10: aload #9
        //   12: ldc 'android.view.IWindowSession'
        //   14: invokevirtual writeInterfaceToken : (Ljava/lang/String;)V
        //   17: aload_1
        //   18: ifnull -> 32
        //   21: aload_1
        //   22: invokeinterface asBinder : ()Landroid/os/IBinder;
        //   27: astore #11
        //   29: goto -> 35
        //   32: aconst_null
        //   33: astore #11
        //   35: aload #9
        //   37: aload #11
        //   39: invokevirtual writeStrongBinder : (Landroid/os/IBinder;)V
        //   42: aload #9
        //   44: iload_2
        //   45: invokevirtual writeInt : (I)V
        //   48: aload_3
        //   49: ifnull -> 68
        //   52: aload #9
        //   54: iconst_1
        //   55: invokevirtual writeInt : (I)V
        //   58: aload_3
        //   59: aload #9
        //   61: iconst_0
        //   62: invokevirtual writeToParcel : (Landroid/os/Parcel;I)V
        //   65: goto -> 74
        //   68: aload #9
        //   70: iconst_0
        //   71: invokevirtual writeInt : (I)V
        //   74: aload #9
        //   76: iload #4
        //   78: invokevirtual writeInt : (I)V
        //   81: aload #9
        //   83: iload #5
        //   85: invokevirtual writeInt : (I)V
        //   88: aload_0
        //   89: getfield mRemote : Landroid/os/IBinder;
        //   92: iconst_3
        //   93: aload #9
        //   95: aload #10
        //   97: iconst_0
        //   98: invokeinterface transact : (ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
        //   103: istore #12
        //   105: iload #12
        //   107: ifne -> 150
        //   110: invokestatic getDefaultImpl : ()Landroid/view/IWindowSession;
        //   113: ifnull -> 150
        //   116: invokestatic getDefaultImpl : ()Landroid/view/IWindowSession;
        //   119: aload_1
        //   120: iload_2
        //   121: aload_3
        //   122: iload #4
        //   124: iload #5
        //   126: aload #6
        //   128: aload #7
        //   130: aload #8
        //   132: invokeinterface addToDisplayWithoutInputChannel : (Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/InsetsState;)I
        //   137: istore_2
        //   138: aload #10
        //   140: invokevirtual recycle : ()V
        //   143: aload #9
        //   145: invokevirtual recycle : ()V
        //   148: iload_2
        //   149: ireturn
        //   150: aload #10
        //   152: invokevirtual readException : ()V
        //   155: aload #10
        //   157: invokevirtual readInt : ()I
        //   160: istore_2
        //   161: aload #10
        //   163: invokevirtual readInt : ()I
        //   166: istore #4
        //   168: iload #4
        //   170: ifeq -> 183
        //   173: aload #6
        //   175: aload #10
        //   177: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   180: goto -> 183
        //   183: aload #10
        //   185: invokevirtual readInt : ()I
        //   188: istore #4
        //   190: iload #4
        //   192: ifeq -> 205
        //   195: aload #7
        //   197: aload #10
        //   199: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   202: goto -> 205
        //   205: aload #10
        //   207: invokevirtual readInt : ()I
        //   210: istore #4
        //   212: iload #4
        //   214: ifeq -> 231
        //   217: aload #8
        //   219: aload #10
        //   221: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   224: goto -> 231
        //   227: astore_1
        //   228: goto -> 264
        //   231: aload #10
        //   233: invokevirtual recycle : ()V
        //   236: aload #9
        //   238: invokevirtual recycle : ()V
        //   241: iload_2
        //   242: ireturn
        //   243: astore_1
        //   244: goto -> 264
        //   247: astore_1
        //   248: goto -> 264
        //   251: astore_1
        //   252: goto -> 264
        //   255: astore_1
        //   256: goto -> 264
        //   259: astore_1
        //   260: goto -> 264
        //   263: astore_1
        //   264: aload #10
        //   266: invokevirtual recycle : ()V
        //   269: aload #9
        //   271: invokevirtual recycle : ()V
        //   274: aload_1
        //   275: athrow
        // Line number table:
        //   Java source line number -> byte code offset
        //   #1605	-> 0
        //   #1606	-> 5
        //   #1609	-> 10
        //   #1610	-> 17
        //   #1611	-> 42
        //   #1612	-> 48
        //   #1613	-> 52
        //   #1614	-> 58
        //   #1617	-> 68
        //   #1619	-> 74
        //   #1620	-> 81
        //   #1621	-> 88
        //   #1622	-> 105
        //   #1623	-> 116
        //   #1638	-> 138
        //   #1639	-> 143
        //   #1623	-> 148
        //   #1625	-> 150
        //   #1626	-> 155
        //   #1627	-> 161
        //   #1628	-> 173
        //   #1627	-> 183
        //   #1630	-> 183
        //   #1631	-> 195
        //   #1630	-> 205
        //   #1633	-> 205
        //   #1634	-> 217
        //   #1638	-> 227
        //   #1633	-> 231
        //   #1638	-> 231
        //   #1639	-> 236
        //   #1640	-> 241
        //   #1641	-> 241
        //   #1638	-> 243
        //   #1639	-> 269
        //   #1640	-> 274
        // Exception table:
        //   from	to	target	type
        //   10	17	263	finally
        //   21	29	263	finally
        //   35	42	263	finally
        //   42	48	259	finally
        //   52	58	259	finally
        //   58	65	259	finally
        //   68	74	259	finally
        //   74	81	255	finally
        //   81	88	251	finally
        //   88	105	251	finally
        //   110	116	251	finally
        //   116	138	251	finally
        //   150	155	251	finally
        //   155	161	251	finally
        //   161	168	251	finally
        //   173	180	247	finally
        //   183	190	247	finally
        //   195	202	243	finally
        //   205	212	243	finally
        //   217	224	227	finally
      }
      
      public void remove(IWindow param2IWindow) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().remove(param2IWindow);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int relayout(IWindow param2IWindow, int param2Int1, WindowManager.LayoutParams param2LayoutParams, int param2Int2, int param2Int3, int param2Int4, int param2Int5, long param2Long, Rect param2Rect1, Rect param2Rect2, Rect param2Rect3, Rect param2Rect4, Rect param2Rect5, DisplayCutout.ParcelableWrapper param2ParcelableWrapper, MergedConfiguration param2MergedConfiguration, SurfaceControl param2SurfaceControl1, InsetsState param2InsetsState, InsetsSourceControl[] param2ArrayOfInsetsSourceControl, Point param2Point, SurfaceControl param2SurfaceControl2) throws RemoteException {
        // Byte code:
        //   0: invokestatic obtain : ()Landroid/os/Parcel;
        //   3: astore #22
        //   5: invokestatic obtain : ()Landroid/os/Parcel;
        //   8: astore #23
        //   10: aload #22
        //   12: ldc 'android.view.IWindowSession'
        //   14: invokevirtual writeInterfaceToken : (Ljava/lang/String;)V
        //   17: aload_1
        //   18: ifnull -> 36
        //   21: aload_1
        //   22: invokeinterface asBinder : ()Landroid/os/IBinder;
        //   27: astore #24
        //   29: goto -> 39
        //   32: astore_1
        //   33: goto -> 563
        //   36: aconst_null
        //   37: astore #24
        //   39: aload #22
        //   41: aload #24
        //   43: invokevirtual writeStrongBinder : (Landroid/os/IBinder;)V
        //   46: aload #22
        //   48: iload_2
        //   49: invokevirtual writeInt : (I)V
        //   52: aload_3
        //   53: ifnull -> 72
        //   56: aload #22
        //   58: iconst_1
        //   59: invokevirtual writeInt : (I)V
        //   62: aload_3
        //   63: aload #22
        //   65: iconst_0
        //   66: invokevirtual writeToParcel : (Landroid/os/Parcel;I)V
        //   69: goto -> 78
        //   72: aload #22
        //   74: iconst_0
        //   75: invokevirtual writeInt : (I)V
        //   78: aload #22
        //   80: iload #4
        //   82: invokevirtual writeInt : (I)V
        //   85: aload #22
        //   87: iload #5
        //   89: invokevirtual writeInt : (I)V
        //   92: aload #22
        //   94: iload #6
        //   96: invokevirtual writeInt : (I)V
        //   99: aload #22
        //   101: iload #7
        //   103: invokevirtual writeInt : (I)V
        //   106: aload #22
        //   108: lload #8
        //   110: invokevirtual writeLong : (J)V
        //   113: aload #19
        //   115: ifnonnull -> 127
        //   118: aload #22
        //   120: iconst_m1
        //   121: invokevirtual writeInt : (I)V
        //   124: goto -> 135
        //   127: aload #22
        //   129: aload #19
        //   131: arraylength
        //   132: invokevirtual writeInt : (I)V
        //   135: aload_0
        //   136: getfield mRemote : Landroid/os/IBinder;
        //   139: iconst_5
        //   140: aload #22
        //   142: aload #23
        //   144: iconst_0
        //   145: invokeinterface transact : (ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
        //   150: istore #25
        //   152: iload #25
        //   154: ifne -> 233
        //   157: invokestatic getDefaultImpl : ()Landroid/view/IWindowSession;
        //   160: ifnull -> 233
        //   163: invokestatic getDefaultImpl : ()Landroid/view/IWindowSession;
        //   166: astore #24
        //   168: aload #24
        //   170: aload_1
        //   171: iload_2
        //   172: aload_3
        //   173: iload #4
        //   175: iload #5
        //   177: iload #6
        //   179: iload #7
        //   181: lload #8
        //   183: aload #10
        //   185: aload #11
        //   187: aload #12
        //   189: aload #13
        //   191: aload #14
        //   193: aload #15
        //   195: aload #16
        //   197: aload #17
        //   199: aload #18
        //   201: aload #19
        //   203: aload #20
        //   205: aload #21
        //   207: invokeinterface relayout : (Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIIJLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/DisplayCutout$ParcelableWrapper;Landroid/util/MergedConfiguration;Landroid/view/SurfaceControl;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;Landroid/graphics/Point;Landroid/view/SurfaceControl;)I
        //   212: istore_2
        //   213: aload #23
        //   215: invokevirtual recycle : ()V
        //   218: aload #22
        //   220: invokevirtual recycle : ()V
        //   223: iload_2
        //   224: ireturn
        //   225: astore_1
        //   226: goto -> 563
        //   229: astore_1
        //   230: goto -> 563
        //   233: aload #23
        //   235: invokevirtual readException : ()V
        //   238: aload #23
        //   240: invokevirtual readInt : ()I
        //   243: istore_2
        //   244: aload #23
        //   246: invokevirtual readInt : ()I
        //   249: istore #4
        //   251: iload #4
        //   253: ifeq -> 266
        //   256: aload #10
        //   258: aload #23
        //   260: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   263: goto -> 266
        //   266: aload #23
        //   268: invokevirtual readInt : ()I
        //   271: istore #4
        //   273: iload #4
        //   275: ifeq -> 288
        //   278: aload #11
        //   280: aload #23
        //   282: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   285: goto -> 288
        //   288: aload #23
        //   290: invokevirtual readInt : ()I
        //   293: istore #4
        //   295: iload #4
        //   297: ifeq -> 310
        //   300: aload #12
        //   302: aload #23
        //   304: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   307: goto -> 310
        //   310: aload #23
        //   312: invokevirtual readInt : ()I
        //   315: istore #4
        //   317: iload #4
        //   319: ifeq -> 332
        //   322: aload #13
        //   324: aload #23
        //   326: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   329: goto -> 332
        //   332: aload #23
        //   334: invokevirtual readInt : ()I
        //   337: istore #4
        //   339: iload #4
        //   341: ifeq -> 354
        //   344: aload #14
        //   346: aload #23
        //   348: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   351: goto -> 354
        //   354: aload #23
        //   356: invokevirtual readInt : ()I
        //   359: istore #4
        //   361: iload #4
        //   363: ifeq -> 376
        //   366: aload #15
        //   368: aload #23
        //   370: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   373: goto -> 376
        //   376: aload #23
        //   378: invokevirtual readInt : ()I
        //   381: istore #4
        //   383: iload #4
        //   385: ifeq -> 398
        //   388: aload #16
        //   390: aload #23
        //   392: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   395: goto -> 398
        //   398: aload #23
        //   400: invokevirtual readInt : ()I
        //   403: istore #4
        //   405: iload #4
        //   407: ifeq -> 420
        //   410: aload #17
        //   412: aload #23
        //   414: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   417: goto -> 420
        //   420: aload #23
        //   422: invokevirtual readInt : ()I
        //   425: istore #4
        //   427: iload #4
        //   429: ifeq -> 442
        //   432: aload #18
        //   434: aload #23
        //   436: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   439: goto -> 442
        //   442: getstatic android/view/InsetsSourceControl.CREATOR : Landroid/os/Parcelable$Creator;
        //   445: astore_1
        //   446: aload #23
        //   448: aload #19
        //   450: aload_1
        //   451: invokevirtual readTypedArray : ([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V
        //   454: aload #23
        //   456: invokevirtual readInt : ()I
        //   459: istore #4
        //   461: iload #4
        //   463: ifeq -> 476
        //   466: aload #20
        //   468: aload #23
        //   470: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   473: goto -> 476
        //   476: aload #23
        //   478: invokevirtual readInt : ()I
        //   481: istore #4
        //   483: iload #4
        //   485: ifeq -> 502
        //   488: aload #21
        //   490: aload #23
        //   492: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   495: goto -> 502
        //   498: astore_1
        //   499: goto -> 563
        //   502: aload #23
        //   504: invokevirtual recycle : ()V
        //   507: aload #22
        //   509: invokevirtual recycle : ()V
        //   512: iload_2
        //   513: ireturn
        //   514: astore_1
        //   515: goto -> 563
        //   518: astore_1
        //   519: goto -> 563
        //   522: astore_1
        //   523: goto -> 563
        //   526: astore_1
        //   527: goto -> 563
        //   530: astore_1
        //   531: goto -> 563
        //   534: astore_1
        //   535: goto -> 563
        //   538: astore_1
        //   539: goto -> 563
        //   542: astore_1
        //   543: goto -> 563
        //   546: astore_1
        //   547: goto -> 563
        //   550: astore_1
        //   551: goto -> 563
        //   554: astore_1
        //   555: goto -> 563
        //   558: astore_1
        //   559: goto -> 563
        //   562: astore_1
        //   563: aload #23
        //   565: invokevirtual recycle : ()V
        //   568: aload #22
        //   570: invokevirtual recycle : ()V
        //   573: aload_1
        //   574: athrow
        // Line number table:
        //   Java source line number -> byte code offset
        //   #1710	-> 0
        //   #1711	-> 5
        //   #1714	-> 10
        //   #1715	-> 17
        //   #1777	-> 32
        //   #1715	-> 36
        //   #1716	-> 46
        //   #1717	-> 52
        //   #1718	-> 56
        //   #1719	-> 62
        //   #1722	-> 72
        //   #1724	-> 78
        //   #1725	-> 85
        //   #1726	-> 92
        //   #1727	-> 99
        //   #1728	-> 106
        //   #1729	-> 113
        //   #1730	-> 118
        //   #1733	-> 127
        //   #1735	-> 135
        //   #1736	-> 152
        //   #1737	-> 163
        //   #1777	-> 213
        //   #1778	-> 218
        //   #1737	-> 223
        //   #1777	-> 225
        //   #1736	-> 233
        //   #1739	-> 233
        //   #1740	-> 238
        //   #1741	-> 244
        //   #1742	-> 256
        //   #1741	-> 266
        //   #1744	-> 266
        //   #1745	-> 278
        //   #1744	-> 288
        //   #1747	-> 288
        //   #1748	-> 300
        //   #1747	-> 310
        //   #1750	-> 310
        //   #1751	-> 322
        //   #1750	-> 332
        //   #1753	-> 332
        //   #1754	-> 344
        //   #1753	-> 354
        //   #1756	-> 354
        //   #1757	-> 366
        //   #1756	-> 376
        //   #1759	-> 376
        //   #1760	-> 388
        //   #1759	-> 398
        //   #1762	-> 398
        //   #1763	-> 410
        //   #1762	-> 420
        //   #1765	-> 420
        //   #1766	-> 432
        //   #1765	-> 442
        //   #1768	-> 442
        //   #1769	-> 454
        //   #1770	-> 466
        //   #1769	-> 476
        //   #1772	-> 476
        //   #1773	-> 488
        //   #1777	-> 498
        //   #1772	-> 502
        //   #1777	-> 502
        //   #1778	-> 507
        //   #1779	-> 512
        //   #1780	-> 512
        //   #1777	-> 514
        //   #1778	-> 568
        //   #1779	-> 573
        // Exception table:
        //   from	to	target	type
        //   10	17	562	finally
        //   21	29	32	finally
        //   39	46	562	finally
        //   46	52	562	finally
        //   56	62	32	finally
        //   62	69	32	finally
        //   72	78	562	finally
        //   78	85	562	finally
        //   85	92	562	finally
        //   92	99	562	finally
        //   99	106	562	finally
        //   106	113	562	finally
        //   118	124	32	finally
        //   127	135	562	finally
        //   135	152	562	finally
        //   157	163	229	finally
        //   163	168	229	finally
        //   168	213	225	finally
        //   233	238	558	finally
        //   238	244	558	finally
        //   244	251	558	finally
        //   256	263	554	finally
        //   266	273	554	finally
        //   278	285	550	finally
        //   288	295	550	finally
        //   300	307	546	finally
        //   310	317	546	finally
        //   322	329	542	finally
        //   332	339	542	finally
        //   344	351	538	finally
        //   354	361	538	finally
        //   366	373	534	finally
        //   376	383	534	finally
        //   388	395	530	finally
        //   398	405	530	finally
        //   410	417	526	finally
        //   420	427	526	finally
        //   432	439	522	finally
        //   442	446	522	finally
        //   446	454	518	finally
        //   454	461	518	finally
        //   466	473	514	finally
        //   476	483	514	finally
        //   488	495	498	finally
      }
      
      public void prepareToReplaceWindows(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().prepareToReplaceWindows(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean outOfMemory(IWindow param2IWindow) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IWindowSession.Stub.getDefaultImpl() != null) {
            bool1 = IWindowSession.Stub.getDefaultImpl().outOfMemory(param2IWindow);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTransparentRegion(IWindow param2IWindow, Region param2Region) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Region != null) {
            parcel1.writeInt(1);
            param2Region.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().setTransparentRegion(param2IWindow, param2Region);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInsets(IWindow param2IWindow, int param2Int, Rect param2Rect1, Rect param2Rect2, Region param2Region) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          if (param2Rect1 != null) {
            parcel1.writeInt(1);
            param2Rect1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Rect2 != null) {
            parcel1.writeInt(1);
            param2Rect2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Region != null) {
            parcel1.writeInt(1);
            param2Region.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().setInsets(param2IWindow, param2Int, param2Rect1, param2Rect2, param2Region);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getDisplayFrame(IWindow param2IWindow, Rect param2Rect) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().getDisplayFrame(param2IWindow, param2Rect);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2Rect.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getVisibleDisplayFrame(IWindow param2IWindow, Rect param2Rect) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().getVisibleDisplayFrame(param2IWindow, param2Rect);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2Rect.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishDrawing(IWindow param2IWindow, SurfaceControl.Transaction param2Transaction) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Transaction != null) {
            parcel1.writeInt(1);
            param2Transaction.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().finishDrawing(param2IWindow, param2Transaction);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInTouchMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool1 && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().setInTouchMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getInTouchMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IWindowSession.Stub.getDefaultImpl() != null) {
            bool1 = IWindowSession.Stub.getDefaultImpl().getInTouchMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean performHapticFeedback(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            param2Boolean = IWindowSession.Stub.getDefaultImpl().performHapticFeedback(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBinder performDrag(IWindow param2IWindow, int param2Int1, SurfaceControl param2SurfaceControl, int param2Int2, float param2Float1, float param2Float2, float param2Float3, float param2Float4, ClipData param2ClipData) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeInt(param2Int1);
            if (param2SurfaceControl != null) {
              parcel1.writeInt(1);
              param2SurfaceControl.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            parcel1.writeInt(param2Int2);
            parcel1.writeFloat(param2Float1);
            parcel1.writeFloat(param2Float2);
            parcel1.writeFloat(param2Float3);
            parcel1.writeFloat(param2Float4);
            if (param2ClipData != null) {
              parcel1.writeInt(1);
              param2ClipData.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
            if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
              IBinder iBinder2 = IWindowSession.Stub.getDefaultImpl().performDrag(param2IWindow, param2Int1, param2SurfaceControl, param2Int2, param2Float1, param2Float2, param2Float3, param2Float4, param2ClipData);
              parcel2.recycle();
              parcel1.recycle();
              return iBinder2;
            } 
            parcel2.readException();
            IBinder iBinder1 = parcel2.readStrongBinder();
            parcel2.recycle();
            parcel1.recycle();
            return iBinder1;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IWindow;
      }
      
      public void reportDropResult(IWindow param2IWindow, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool1 && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().reportDropResult(param2IWindow, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelDragAndDrop(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().cancelDragAndDrop(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dragRecipientEntered(IWindow param2IWindow) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().dragRecipientEntered(param2IWindow);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dragRecipientExited(IWindow param2IWindow) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().dragRecipientExited(param2IWindow);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setWallpaperPosition(IBinder param2IBinder, float param2Float1, float param2Float2, float param2Float3, float param2Float4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindowSession");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeFloat(param2Float1);
          parcel.writeFloat(param2Float2);
          parcel.writeFloat(param2Float3);
          parcel.writeFloat(param2Float4);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().setWallpaperPosition(param2IBinder, param2Float1, param2Float2, param2Float3, param2Float4);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setWallpaperZoomOut(IBinder param2IBinder, float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindowSession");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().setWallpaperZoomOut(param2IBinder, param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setShouldZoomOutWallpaper(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool1 && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().setShouldZoomOutWallpaper(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void wallpaperOffsetsComplete(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().wallpaperOffsetsComplete(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setWallpaperDisplayOffset(IBinder param2IBinder, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().setWallpaperDisplayOffset(param2IBinder, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle sendWallpaperCommand(IBinder param2IBinder, String param2String, int param2Int1, int param2Int2, int param2Int3, Bundle param2Bundle, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  parcel1.writeInt(param2Int3);
                  boolean bool = true;
                  if (param2Bundle != null) {
                    parcel1.writeInt(1);
                    param2Bundle.writeToParcel(parcel1, 0);
                  } else {
                    parcel1.writeInt(0);
                  } 
                  if (!param2Boolean)
                    bool = false; 
                  parcel1.writeInt(bool);
                  boolean bool1 = this.mRemote.transact(26, parcel1, parcel2, 0);
                  if (!bool1 && IWindowSession.Stub.getDefaultImpl() != null) {
                    Bundle bundle = IWindowSession.Stub.getDefaultImpl().sendWallpaperCommand(param2IBinder, param2String, param2Int1, param2Int2, param2Int3, param2Bundle, param2Boolean);
                    parcel2.recycle();
                    parcel1.recycle();
                    return bundle;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2IBinder = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (Bundle)param2IBinder;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void wallpaperCommandComplete(IBinder param2IBinder, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().wallpaperCommandComplete(param2IBinder, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onRectangleOnScreenRequested(IBinder param2IBinder, Rect param2Rect) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().onRectangleOnScreenRequested(param2IBinder, param2Rect);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IWindowId getWindowId(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null)
            return IWindowSession.Stub.getDefaultImpl().getWindowId(param2IBinder); 
          parcel2.readException();
          return IWindowId.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pokeDrawLock(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().pokeDrawLock(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startMovingTask(IWindow param2IWindow, float param2Float1, float param2Float2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeFloat(param2Float1);
          parcel1.writeFloat(param2Float2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(31, parcel1, parcel2, 0);
          if (!bool2 && IWindowSession.Stub.getDefaultImpl() != null) {
            bool1 = IWindowSession.Stub.getDefaultImpl().startMovingTask(param2IWindow, param2Float1, param2Float2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishMovingTask(IWindow param2IWindow) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().finishMovingTask(param2IWindow);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updatePointerIcon(IWindow param2IWindow) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().updatePointerIcon(param2IWindow);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reparentDisplayContent(IWindow param2IWindow, SurfaceControl param2SurfaceControl, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2SurfaceControl != null) {
            parcel1.writeInt(1);
            param2SurfaceControl.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().reparentDisplayContent(param2IWindow, param2SurfaceControl, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateDisplayContentLocation(IWindow param2IWindow, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().updateDisplayContentLocation(param2IWindow, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateTapExcludeRegion(IWindow param2IWindow, Region param2Region) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Region != null) {
            parcel1.writeInt(1);
            param2Region.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().updateTapExcludeRegion(param2IWindow, param2Region);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void insetsModified(IWindow param2IWindow, InsetsState param2InsetsState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2InsetsState != null) {
            parcel.writeInt(1);
            param2InsetsState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(37, parcel, null, 1);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().insetsModified(param2IWindow, param2InsetsState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reportSystemGestureExclusionChanged(IWindow param2IWindow, List<Rect> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IWindowSession");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(38, parcel, null, 1);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().reportSystemGestureExclusionChanged(param2IWindow, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void grantInputChannel(int param2Int1, SurfaceControl param2SurfaceControl, IWindow param2IWindow, IBinder param2IBinder, int param2Int2, int param2Int3, InputChannel param2InputChannel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          try {
            IBinder iBinder;
            parcel1.writeInt(param2Int1);
            if (param2SurfaceControl != null) {
              parcel1.writeInt(1);
              param2SurfaceControl.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2IWindow != null) {
              iBinder = param2IWindow.asBinder();
            } else {
              iBinder = null;
            } 
            parcel1.writeStrongBinder(iBinder);
            try {
              parcel1.writeStrongBinder(param2IBinder);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
                  if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
                    IWindowSession.Stub.getDefaultImpl().grantInputChannel(param2Int1, param2SurfaceControl, param2IWindow, param2IBinder, param2Int2, param2Int3, param2InputChannel);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  if (param2Int1 != 0)
                    try {
                      param2InputChannel.readFromParcel(parcel2);
                    } finally {} 
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2SurfaceControl;
      }
      
      public void updateInputChannel(IBinder param2IBinder, int param2Int1, SurfaceControl param2SurfaceControl, int param2Int2, Region param2Region) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowSession");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          if (param2SurfaceControl != null) {
            parcel1.writeInt(1);
            param2SurfaceControl.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          if (param2Region != null) {
            parcel1.writeInt(1);
            param2Region.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IWindowSession.Stub.getDefaultImpl() != null) {
            IWindowSession.Stub.getDefaultImpl().updateInputChannel(param2IBinder, param2Int1, param2SurfaceControl, param2Int2, param2Region);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowSession param1IWindowSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowSession != null) {
          Proxy.sDefaultImpl = param1IWindowSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
