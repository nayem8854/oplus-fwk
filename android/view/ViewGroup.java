package android.view;

import android.animation.LayoutTransition;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Insets;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pools;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.Transformation;
import android.view.autofill.Helper;
import com.android.internal.R;
import com.oplus.darkmode.IOplusDarkModeManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.function.Predicate;

public abstract class ViewGroup extends View implements ViewParent, ViewManager {
  @ExportedProperty(category = "events")
  private int mLastTouchDownIndex = -1;
  
  private int mLayoutMode = -1;
  
  private static final int[] DESCENDANT_FOCUSABILITY_FLAGS = new int[] { 131072, 262144, 393216 };
  
  public static int LAYOUT_MODE_DEFAULT = 0;
  
  boolean mSuppressLayout = false;
  
  private boolean mLayoutCalledWhileSuppressed = false;
  
  @ExportedProperty(category = "layout")
  private int mChildCountWithTransientState = 0;
  
  private List<Integer> mTransientIndices = null;
  
  private List<View> mTransientViews = null;
  
  int mChildUnhandledKeyListeners = 0;
  
  private int mInsetsAnimationDispatchMode = 1;
  
  private static final ActionMode SENTINEL_ACTION_MODE = (ActionMode)new Object();
  
  private static final int ARRAY_CAPACITY_INCREMENT = 12;
  
  private static final int ARRAY_INITIAL_CAPACITY = 12;
  
  private static final int CHILD_LEFT_INDEX = 0;
  
  private static final int CHILD_TOP_INDEX = 1;
  
  protected static final int CLIP_TO_PADDING_MASK = 34;
  
  private static final boolean DBG = false;
  
  private static final int FLAG_ADD_STATES_FROM_CHILDREN = 8192;
  
  @Deprecated
  private static final int FLAG_ALWAYS_DRAWN_WITH_CACHE = 16384;
  
  @Deprecated
  private static final int FLAG_ANIMATION_CACHE = 64;
  
  static final int FLAG_ANIMATION_DONE = 16;
  
  @Deprecated
  private static final int FLAG_CHILDREN_DRAWN_WITH_CACHE = 32768;
  
  static final int FLAG_CLEAR_TRANSFORMATION = 256;
  
  static final int FLAG_CLIP_CHILDREN = 1;
  
  private static final int FLAG_CLIP_TO_PADDING = 2;
  
  protected static final int FLAG_DISALLOW_INTERCEPT = 524288;
  
  static final int FLAG_INVALIDATE_REQUIRED = 4;
  
  static final int FLAG_IS_TRANSITION_GROUP = 16777216;
  
  static final int FLAG_IS_TRANSITION_GROUP_SET = 33554432;
  
  private static final int FLAG_LAYOUT_MODE_WAS_EXPLICITLY_SET = 8388608;
  
  private static final int FLAG_MASK_FOCUSABILITY = 393216;
  
  private static final int FLAG_NOTIFY_ANIMATION_LISTENER = 512;
  
  private static final int FLAG_NOTIFY_CHILDREN_ON_DRAWABLE_STATE_CHANGE = 65536;
  
  static final int FLAG_OPTIMIZE_INVALIDATE = 128;
  
  private static final int FLAG_PADDING_NOT_NULL = 32;
  
  private static final int FLAG_PREVENT_DISPATCH_ATTACHED_TO_WINDOW = 4194304;
  
  private static final int FLAG_RUN_ANIMATION = 8;
  
  private static final int FLAG_SHOW_CONTEXT_MENU_WITH_COORDS = 536870912;
  
  private static final int FLAG_SPLIT_MOTION_EVENTS = 2097152;
  
  private static final int FLAG_START_ACTION_MODE_FOR_CHILD_IS_NOT_TYPED = 268435456;
  
  private static final int FLAG_START_ACTION_MODE_FOR_CHILD_IS_TYPED = 134217728;
  
  protected static final int FLAG_SUPPORT_STATIC_TRANSFORMATIONS = 2048;
  
  static final int FLAG_TOUCHSCREEN_BLOCKS_FOCUS = 67108864;
  
  protected static final int FLAG_USE_CHILD_DRAWING_ORDER = 1024;
  
  public static final int FOCUS_AFTER_DESCENDANTS = 262144;
  
  public static final int FOCUS_BEFORE_DESCENDANTS = 131072;
  
  public static final int FOCUS_BLOCK_DESCENDANTS = 393216;
  
  public static final int LAYOUT_MODE_CLIP_BOUNDS = 0;
  
  public static final int LAYOUT_MODE_OPTICAL_BOUNDS = 1;
  
  private static final int LAYOUT_MODE_UNDEFINED = -1;
  
  @Deprecated
  public static final int PERSISTENT_ALL_CACHES = 3;
  
  @Deprecated
  public static final int PERSISTENT_ANIMATION_CACHE = 1;
  
  @Deprecated
  public static final int PERSISTENT_NO_CACHE = 0;
  
  @Deprecated
  public static final int PERSISTENT_SCROLLING_CACHE = 2;
  
  private static final String TAG = "ViewGroup";
  
  private static float[] sDebugLines;
  
  private Animation.AnimationListener mAnimationListener;
  
  Paint mCachePaint;
  
  private Transformation mChildTransformation;
  
  private View[] mChildren;
  
  private int mChildrenCount;
  
  private HashSet<View> mChildrenInterestedInDrag;
  
  private View mCurrentDragChild;
  
  private DragEvent mCurrentDragStartEvent;
  
  private View mDefaultFocus;
  
  protected ArrayList<View> mDisappearingChildren;
  
  private HoverTarget mFirstHoverTarget;
  
  private TouchTarget mFirstTouchTarget;
  
  private View mFocused;
  
  View mFocusedInCluster;
  
  @ExportedProperty(flagMapping = {@FlagToString(equals = 1, mask = 1, name = "CLIP_CHILDREN"), @FlagToString(equals = 2, mask = 2, name = "CLIP_TO_PADDING"), @FlagToString(equals = 32, mask = 32, name = "PADDING_NOT_NULL")}, formatToHexString = true)
  protected int mGroupFlags;
  
  private boolean mHoveredSelf;
  
  RectF mInvalidateRegion;
  
  Transformation mInvalidationTransformation;
  
  private boolean mIsInterestedInDrag;
  
  @ExportedProperty(category = "events")
  private long mLastTouchDownTime;
  
  @ExportedProperty(category = "events")
  private float mLastTouchDownX;
  
  @ExportedProperty(category = "events")
  private float mLastTouchDownY;
  
  private LayoutAnimationController mLayoutAnimationController;
  
  private LayoutTransition.TransitionListener mLayoutTransitionListener;
  
  private PointF mLocalPoint;
  
  private int mNestedScrollAxes;
  
  protected OnHierarchyChangeListener mOnHierarchyChangeListener;
  
  protected int mPersistentDrawingCache;
  
  private ArrayList<View> mPreSortedChildren;
  
  private int[] mTempLocation;
  
  private Point mTempPoint;
  
  private float[] mTempPosition;
  
  private Rect mTempRect;
  
  private View mTooltipHoverTarget;
  
  private boolean mTooltipHoveredSelf;
  
  private LayoutTransition mTransition;
  
  private ArrayList<View> mTransitioningViews;
  
  private ArrayList<View> mVisibilityChangingChildren;
  
  public ViewGroup(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ViewGroup(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ViewGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ViewGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mLayoutTransitionListener = (LayoutTransition.TransitionListener)new Object(this);
    initViewGroup();
    initFromAttributes(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  private void initViewGroup() {
    if (!isShowingLayoutBounds())
      setFlags(128, 128); 
    int i = this.mGroupFlags | 0x1;
    this.mGroupFlags = i |= 0x2;
    this.mGroupFlags = i |= 0x10;
    this.mGroupFlags = i |= 0x40;
    this.mGroupFlags = i | 0x4000;
    if ((this.mContext.getApplicationInfo()).targetSdkVersion >= 11)
      this.mGroupFlags |= 0x200000; 
    setDescendantFocusability(131072);
    this.mChildren = new View[12];
    this.mChildrenCount = 0;
    this.mPersistentDrawingCache = 2;
  }
  
  private void initFromAttributes(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewGroup, paramInt1, paramInt2);
    saveAttributeDataForStyleable(paramContext, R.styleable.ViewGroup, paramAttributeSet, typedArray, paramInt1, paramInt2);
    paramInt2 = typedArray.getIndexCount();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      boolean bool;
      int i = typedArray.getIndex(paramInt1);
      switch (i) {
        case 12:
          setTouchscreenBlocksFocus(typedArray.getBoolean(i, false));
          break;
        case 11:
          setTransitionGroup(typedArray.getBoolean(i, false));
          break;
        case 10:
          setLayoutMode(typedArray.getInt(i, -1));
          break;
        case 9:
          bool = typedArray.getBoolean(i, false);
          if (bool)
            setLayoutTransition(new LayoutTransition()); 
          break;
        case 8:
          setMotionEventSplittingEnabled(typedArray.getBoolean(i, false));
          break;
        case 7:
          setDescendantFocusability(DESCENDANT_FOCUSABILITY_FLAGS[typedArray.getInt(i, 0)]);
          break;
        case 6:
          setAddStatesFromChildren(typedArray.getBoolean(i, false));
          break;
        case 5:
          setAlwaysDrawnWithCacheEnabled(typedArray.getBoolean(i, true));
          break;
        case 4:
          setPersistentDrawingCache(typedArray.getInt(i, 2));
          break;
        case 3:
          setAnimationCacheEnabled(typedArray.getBoolean(i, true));
          break;
        case 2:
          i = typedArray.getResourceId(i, -1);
          if (i > 0)
            setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this.mContext, i)); 
          break;
        case 1:
          setClipToPadding(typedArray.getBoolean(i, true));
          break;
        case 0:
          setClipChildren(typedArray.getBoolean(i, true));
          break;
      } 
    } 
    typedArray.recycle();
  }
  
  @ExportedProperty(category = "focus", mapping = {@IntToString(from = 131072, to = "FOCUS_BEFORE_DESCENDANTS"), @IntToString(from = 262144, to = "FOCUS_AFTER_DESCENDANTS"), @IntToString(from = 393216, to = "FOCUS_BLOCK_DESCENDANTS")})
  public int getDescendantFocusability() {
    return this.mGroupFlags & 0x60000;
  }
  
  public void setDescendantFocusability(int paramInt) {
    if (paramInt == 131072 || paramInt == 262144 || paramInt == 393216) {
      int i = this.mGroupFlags & 0xFFF9FFFF;
      this.mGroupFlags = i | 0x60000 & paramInt;
      return;
    } 
    throw new IllegalArgumentException("must be one of FOCUS_BEFORE_DESCENDANTS, FOCUS_AFTER_DESCENDANTS, FOCUS_BLOCK_DESCENDANTS");
  }
  
  void handleFocusGainInternal(int paramInt, Rect paramRect) {
    View view = this.mFocused;
    if (view != null) {
      view.unFocus(this);
      this.mFocused = null;
      this.mFocusedInCluster = null;
    } 
    super.handleFocusGainInternal(paramInt, paramRect);
  }
  
  public void requestChildFocus(View paramView1, View paramView2) {
    if (getDescendantFocusability() == 393216)
      return; 
    super.unFocus(paramView2);
    View view = this.mFocused;
    if (view != paramView1) {
      if (view != null)
        view.unFocus(paramView2); 
      this.mFocused = paramView1;
    } 
    if (this.mParent != null)
      this.mParent.requestChildFocus(this, paramView2); 
  }
  
  void setDefaultFocus(View paramView) {
    View view = this.mDefaultFocus;
    if (view != null && view.isFocusedByDefault())
      return; 
    this.mDefaultFocus = paramView;
    if (this.mParent instanceof ViewGroup)
      ((ViewGroup)this.mParent).setDefaultFocus(this); 
  }
  
  void clearDefaultFocus(View paramView) {
    View view = this.mDefaultFocus;
    if (view != paramView && view != null && view.isFocusedByDefault())
      return; 
    this.mDefaultFocus = null;
    for (byte b = 0; b < this.mChildrenCount; b++) {
      paramView = this.mChildren[b];
      if (paramView.isFocusedByDefault()) {
        this.mDefaultFocus = paramView;
        return;
      } 
      if (this.mDefaultFocus == null && paramView.hasDefaultFocus())
        this.mDefaultFocus = paramView; 
    } 
    if (this.mParent instanceof ViewGroup)
      ((ViewGroup)this.mParent).clearDefaultFocus(this); 
  }
  
  boolean hasDefaultFocus() {
    return (this.mDefaultFocus != null || super.hasDefaultFocus());
  }
  
  void clearFocusedInCluster(View paramView) {
    if (this.mFocusedInCluster != paramView)
      return; 
    clearFocusedInCluster();
  }
  
  void clearFocusedInCluster() {
    ViewParent viewParent;
    View view = findKeyboardNavigationCluster();
    ViewGroup viewGroup = this;
    do {
      viewGroup.mFocusedInCluster = null;
      if (viewGroup == view)
        break; 
      viewParent = viewGroup.getParent();
      ViewParent viewParent1 = viewParent;
    } while (viewParent instanceof ViewGroup);
  }
  
  public void focusableViewAvailable(View paramView) {
    if (this.mParent != null)
      if (getDescendantFocusability() != 393216 && (this.mViewFlags & 0xC) == 0)
        if (isFocusableInTouchMode() || !shouldBlockFocusForTouchscreen())
          if (!isFocused() || getDescendantFocusability() == 262144)
            this.mParent.focusableViewAvailable(paramView);    
  }
  
  public boolean showContextMenuForChild(View paramView) {
    boolean bool = isShowingContextMenuWithCoords();
    boolean bool1 = false;
    if (bool)
      return false; 
    bool = bool1;
    if (this.mParent != null) {
      bool = bool1;
      if (this.mParent.showContextMenuForChild(paramView))
        bool = true; 
    } 
    return bool;
  }
  
  public final boolean isShowingContextMenuWithCoords() {
    boolean bool;
    if ((this.mGroupFlags & 0x20000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean showContextMenuForChild(View paramView, float paramFloat1, float paramFloat2) {
    try {
      this.mGroupFlags |= 0x20000000;
      boolean bool = showContextMenuForChild(paramView);
      boolean bool1 = true;
      if (bool)
        return true; 
      this.mGroupFlags = 0xDFFFFFFF & this.mGroupFlags;
      return bool1;
    } finally {
      this.mGroupFlags = 0xDFFFFFFF & this.mGroupFlags;
    } 
  }
  
  public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback) {
    int i = this.mGroupFlags;
    if ((0x8000000 & i) == 0)
      try {
        this.mGroupFlags = i | 0x10000000;
        return startActionModeForChild(paramView, paramCallback, 0);
      } finally {
        this.mGroupFlags = 0xEFFFFFFF & this.mGroupFlags;
      }  
    return SENTINEL_ACTION_MODE;
  }
  
  public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback, int paramInt) {
    int i = this.mGroupFlags;
    if ((0x10000000 & i) == 0 && paramInt == 0)
      try {
        this.mGroupFlags = i | 0x8000000;
        ActionMode actionMode = startActionModeForChild(paramView, paramCallback);
        this.mGroupFlags = 0xF7FFFFFF & this.mGroupFlags;
      } finally {
        this.mGroupFlags = 0xF7FFFFFF & this.mGroupFlags;
      }  
    if (this.mParent != null)
      try {
        return this.mParent.startActionModeForChild(paramView, paramCallback, paramInt);
      } catch (AbstractMethodError abstractMethodError) {
        return this.mParent.startActionModeForChild(paramView, paramCallback);
      }  
    return null;
  }
  
  public boolean dispatchActivityResult(String paramString, int paramInt1, int paramInt2, Intent paramIntent) {
    if (super.dispatchActivityResult(paramString, paramInt1, paramInt2, paramIntent))
      return true; 
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.dispatchActivityResult(paramString, paramInt1, paramInt2, paramIntent))
        return true; 
    } 
    return false;
  }
  
  public View focusSearch(View paramView, int paramInt) {
    if (isRootNamespace())
      return FocusFinder.getInstance().findNextFocus(this, paramView, paramInt); 
    if (this.mParent != null)
      return this.mParent.focusSearch(paramView, paramInt); 
    return null;
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean) {
    return false;
  }
  
  public boolean requestSendAccessibilityEvent(View paramView, AccessibilityEvent paramAccessibilityEvent) {
    ViewParent viewParent = this.mParent;
    if (viewParent == null)
      return false; 
    boolean bool = onRequestSendAccessibilityEvent(paramView, paramAccessibilityEvent);
    if (!bool)
      return false; 
    return viewParent.requestSendAccessibilityEvent(this, paramAccessibilityEvent);
  }
  
  public boolean onRequestSendAccessibilityEvent(View paramView, AccessibilityEvent paramAccessibilityEvent) {
    if (this.mAccessibilityDelegate != null)
      return this.mAccessibilityDelegate.onRequestSendAccessibilityEvent(this, paramView, paramAccessibilityEvent); 
    return onRequestSendAccessibilityEventInternal(paramView, paramAccessibilityEvent);
  }
  
  public boolean onRequestSendAccessibilityEventInternal(View paramView, AccessibilityEvent paramAccessibilityEvent) {
    return true;
  }
  
  public void childHasTransientStateChanged(View paramView, boolean paramBoolean) {
    boolean bool = hasTransientState();
    if (paramBoolean) {
      this.mChildCountWithTransientState++;
    } else {
      this.mChildCountWithTransientState--;
    } 
    paramBoolean = hasTransientState();
    if (this.mParent != null && bool != paramBoolean)
      try {
        this.mParent.childHasTransientStateChanged(this, paramBoolean);
      } catch (AbstractMethodError abstractMethodError) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mParent.getClass().getSimpleName());
        stringBuilder.append(" does not fully implement ViewParent");
        Log.e("ViewGroup", stringBuilder.toString(), abstractMethodError);
      }  
  }
  
  public boolean hasTransientState() {
    return (this.mChildCountWithTransientState > 0 || super.hasTransientState());
  }
  
  public boolean dispatchUnhandledMove(View paramView, int paramInt) {
    boolean bool;
    View view = this.mFocused;
    if (view != null && view.dispatchUnhandledMove(paramView, paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void clearChildFocus(View paramView) {
    this.mFocused = null;
    if (this.mParent != null)
      this.mParent.clearChildFocus(this); 
  }
  
  public void clearFocus() {
    if (this.mFocused == null) {
      super.clearFocus();
    } else {
      View view = this.mFocused;
      this.mFocused = null;
      view.clearFocus();
    } 
  }
  
  void unFocus(View paramView) {
    View view = this.mFocused;
    if (view == null) {
      super.unFocus(paramView);
    } else {
      view.unFocus(paramView);
      this.mFocused = null;
    } 
  }
  
  public View getFocusedChild() {
    return this.mFocused;
  }
  
  View getDeepestFocusedChild() {
    ViewGroup viewGroup = this;
    while (true) {
      View view = null;
      if (viewGroup != null) {
        if (viewGroup.isFocused())
          return viewGroup; 
        if (viewGroup instanceof ViewGroup)
          view = viewGroup.getFocusedChild(); 
        View view1 = view;
        continue;
      } 
      break;
    } 
    return null;
  }
  
  public boolean hasFocus() {
    return ((this.mPrivateFlags & 0x2) != 0 || this.mFocused != null);
  }
  
  public View findFocus() {
    if (isFocused())
      return this; 
    View view = this.mFocused;
    if (view != null)
      return view.findFocus(); 
    return null;
  }
  
  boolean hasFocusable(boolean paramBoolean1, boolean paramBoolean2) {
    if ((this.mViewFlags & 0xC) != 0)
      return false; 
    if ((paramBoolean1 || getFocusable() != 16) && isFocusable())
      return true; 
    int i = getDescendantFocusability();
    if (i != 393216)
      return hasFocusableChild(paramBoolean2); 
    return false;
  }
  
  boolean hasFocusableChild(boolean paramBoolean) {
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if ((paramBoolean && view.hasExplicitFocusable()) || (!paramBoolean && view.hasFocusable()))
        return true; 
    } 
    return false;
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2) {
    boolean bool1;
    int i = paramArrayList.size();
    int j = getDescendantFocusability();
    boolean bool = shouldBlockFocusForTouchscreen();
    if (isFocusableInTouchMode() || !bool) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (j == 393216) {
      if (bool1)
        super.addFocusables(paramArrayList, paramInt1, paramInt2); 
      return;
    } 
    int k = paramInt2;
    if (bool)
      k = paramInt2 | 0x1; 
    if (j == 131072 && bool1)
      super.addFocusables(paramArrayList, paramInt1, k); 
    paramInt2 = 0;
    View[] arrayOfView = new View[this.mChildrenCount];
    byte b;
    for (b = 0; b < this.mChildrenCount; b++, paramInt2 = m) {
      View view = this.mChildren[b];
      int m = paramInt2;
      if ((view.mViewFlags & 0xC) == 0) {
        arrayOfView[paramInt2] = view;
        m = paramInt2 + 1;
      } 
    } 
    FocusFinder.sort(arrayOfView, 0, paramInt2, this, isLayoutRtl());
    for (b = 0; b < paramInt2; b++)
      arrayOfView[b].addFocusables(paramArrayList, paramInt1, k); 
    if (j == 262144 && bool1 && i == paramArrayList.size())
      super.addFocusables(paramArrayList, paramInt1, k); 
  }
  
  public void addKeyboardNavigationClusters(Collection<View> paramCollection, int paramInt) {
    int i = paramCollection.size();
    if (isKeyboardNavigationCluster()) {
      boolean bool = getTouchscreenBlocksFocus();
      try {
        setTouchscreenBlocksFocusNoRefocus(false);
        super.addKeyboardNavigationClusters(paramCollection, paramInt);
      } finally {
        setTouchscreenBlocksFocusNoRefocus(bool);
      } 
    } else {
      super.addKeyboardNavigationClusters(paramCollection, paramInt);
    } 
    if (i != paramCollection.size())
      return; 
    if (getDescendantFocusability() == 393216)
      return; 
    i = 0;
    View[] arrayOfView = new View[this.mChildrenCount];
    byte b;
    for (b = 0; b < this.mChildrenCount; b++, i = j) {
      View view = this.mChildren[b];
      int j = i;
      if ((view.mViewFlags & 0xC) == 0) {
        arrayOfView[i] = view;
        j = i + 1;
      } 
    } 
    FocusFinder.sort(arrayOfView, 0, i, this, isLayoutRtl());
    for (b = 0; b < i; b++)
      arrayOfView[b].addKeyboardNavigationClusters(paramCollection, paramInt); 
  }
  
  public void setTouchscreenBlocksFocus(boolean paramBoolean) {
    if (paramBoolean) {
      this.mGroupFlags |= 0x4000000;
      if (hasFocus() && !isKeyboardNavigationCluster()) {
        View view = getDeepestFocusedChild();
        if (!view.isFocusableInTouchMode()) {
          view = focusSearch(2);
          if (view != null)
            view.requestFocus(); 
        } 
      } 
    } else {
      this.mGroupFlags &= 0xFBFFFFFF;
    } 
  }
  
  private void setTouchscreenBlocksFocusNoRefocus(boolean paramBoolean) {
    if (paramBoolean) {
      this.mGroupFlags |= 0x4000000;
    } else {
      this.mGroupFlags &= 0xFBFFFFFF;
    } 
  }
  
  @ExportedProperty(category = "focus")
  public boolean getTouchscreenBlocksFocus() {
    boolean bool;
    if ((this.mGroupFlags & 0x4000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean shouldBlockFocusForTouchscreen() {
    if (getTouchscreenBlocksFocus()) {
      Context context = this.mContext;
      if (context.getPackageManager().hasSystemFeature("android.hardware.touchscreen") && (!isKeyboardNavigationCluster() || (!hasFocus() && findKeyboardNavigationCluster() == this)))
        return true; 
    } 
    return false;
  }
  
  public void findViewsWithText(ArrayList<View> paramArrayList, CharSequence paramCharSequence, int paramInt) {
    super.findViewsWithText(paramArrayList, paramCharSequence, paramInt);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if ((view.mViewFlags & 0xC) == 0 && (view.mPrivateFlags & 0x8) == 0)
        view.findViewsWithText(paramArrayList, paramCharSequence, paramInt); 
    } 
  }
  
  public View findViewByAccessibilityIdTraversal(int paramInt) {
    View view = (View)super.findViewByAccessibilityIdTraversal(paramInt);
    if (view != null)
      return view; 
    if (getAccessibilityNodeProvider() != null)
      return null; 
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view1 = arrayOfView[b];
      view1 = view1.findViewByAccessibilityIdTraversal(paramInt);
      if (view1 != null)
        return view1; 
    } 
    return null;
  }
  
  public View findViewByAutofillIdTraversal(int paramInt) {
    View view = (View)super.findViewByAutofillIdTraversal(paramInt);
    if (view != null)
      return view; 
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view1 = arrayOfView[b];
      view1 = view1.findViewByAutofillIdTraversal(paramInt);
      if (view1 != null)
        return view1; 
    } 
    return null;
  }
  
  public void dispatchWindowFocusChanged(boolean paramBoolean) {
    super.dispatchWindowFocusChanged(paramBoolean);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      if (arrayOfView[b] != null) {
        arrayOfView[b].dispatchWindowFocusChanged(paramBoolean);
      } else {
        Log.e("ViewGroup", "none child,ignore focus change!");
      } 
    } 
  }
  
  public void addTouchables(ArrayList<View> paramArrayList) {
    super.addTouchables(paramArrayList);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if ((view.mViewFlags & 0xC) == 0)
        view.addTouchables(paramArrayList); 
    } 
  }
  
  public void makeOptionalFitsSystemWindows() {
    super.makeOptionalFitsSystemWindows();
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].makeOptionalFitsSystemWindows(); 
  }
  
  public void makeFrameworkOptionalFitsSystemWindows() {
    super.makeFrameworkOptionalFitsSystemWindows();
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].makeFrameworkOptionalFitsSystemWindows(); 
  }
  
  public void dispatchDisplayHint(int paramInt) {
    super.dispatchDisplayHint(paramInt);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchDisplayHint(paramInt); 
  }
  
  protected void onChildVisibilityChanged(View paramView, int paramInt1, int paramInt2) {
    LayoutTransition layoutTransition = this.mTransition;
    if (layoutTransition != null)
      if (paramInt2 == 0) {
        layoutTransition.showChild(this, paramView, paramInt1);
      } else {
        layoutTransition.hideChild(this, paramView, paramInt2);
        ArrayList<View> arrayList = this.mTransitioningViews;
        if (arrayList != null && arrayList.contains(paramView)) {
          if (this.mVisibilityChangingChildren == null)
            this.mVisibilityChangingChildren = new ArrayList<>(); 
          this.mVisibilityChangingChildren.add(paramView);
          addDisappearingView(paramView);
        } 
      }  
    if (paramInt2 == 0 && this.mCurrentDragStartEvent != null && !this.mChildrenInterestedInDrag.contains(paramView))
      notifyChildOfDragStart(paramView); 
  }
  
  protected void dispatchVisibilityChanged(View paramView, int paramInt) {
    super.dispatchVisibilityChanged(paramView, paramInt);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchVisibilityChanged(paramView, paramInt); 
  }
  
  public void dispatchWindowVisibilityChanged(int paramInt) {
    super.dispatchWindowVisibilityChanged(paramInt);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchWindowVisibilityChanged(paramInt); 
  }
  
  boolean dispatchVisibilityAggregated(boolean paramBoolean) {
    paramBoolean = super.dispatchVisibilityAggregated(paramBoolean);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      if (arrayOfView[b].getVisibility() == 0)
        arrayOfView[b].dispatchVisibilityAggregated(paramBoolean); 
    } 
    return paramBoolean;
  }
  
  public void dispatchConfigurationChanged(Configuration paramConfiguration) {
    super.dispatchConfigurationChanged(paramConfiguration);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchConfigurationChanged(paramConfiguration); 
  }
  
  public void recomputeViewAttributes(View paramView) {
    if (this.mAttachInfo != null && !this.mAttachInfo.mRecomputeGlobalAttributes) {
      ViewParent viewParent = this.mParent;
      if (viewParent != null)
        viewParent.recomputeViewAttributes(this); 
    } 
  }
  
  void dispatchCollectViewAttributes(View.AttachInfo paramAttachInfo, int paramInt) {
    if ((paramInt & 0xC) == 0) {
      super.dispatchCollectViewAttributes(paramAttachInfo, paramInt);
      int i = this.mChildrenCount;
      View[] arrayOfView = this.mChildren;
      for (byte b = 0; b < i; b++) {
        View view = arrayOfView[b];
        view.dispatchCollectViewAttributes(paramAttachInfo, view.mViewFlags & 0xC | paramInt);
      } 
    } 
  }
  
  public void bringChildToFront(View paramView) {
    int i = indexOfChild(paramView);
    if (i >= 0) {
      removeFromArray(i);
      addInArray(paramView, this.mChildrenCount);
      paramView.mParent = this;
      requestLayout();
      invalidate();
    } 
  }
  
  private PointF getLocalPoint() {
    if (this.mLocalPoint == null)
      this.mLocalPoint = new PointF(); 
    return this.mLocalPoint;
  }
  
  boolean dispatchDragEnterExitInPreN(DragEvent paramDragEvent) {
    boolean bool;
    if (paramDragEvent.mAction == 6) {
      View view = this.mCurrentDragChild;
      if (view != null) {
        view.dispatchDragEnterExitInPreN(paramDragEvent);
        this.mCurrentDragChild = null;
      } 
    } 
    if (this.mIsInterestedInDrag && super.dispatchDragEnterExitInPreN(paramDragEvent)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean dispatchDragEvent(DragEvent paramDragEvent) {
    boolean bool1 = false;
    boolean bool = false;
    boolean bool2 = false, bool3 = false;
    float f1 = paramDragEvent.mX;
    float f2 = paramDragEvent.mY;
    ClipData clipData = paramDragEvent.mClipData;
    PointF pointF = getLocalPoint();
    int i = paramDragEvent.mAction;
    if (i != 1) {
      DragEvent dragEvent;
      HashSet<View> hashSet;
      if (i != 2 && i != 3) {
        if (i != 4) {
          bool1 = bool2;
        } else {
          hashSet = this.mChildrenInterestedInDrag;
          if (hashSet != null) {
            for (Iterator<View> iterator = hashSet.iterator(); iterator.hasNext(); ) {
              View view = iterator.next();
              if (view.dispatchDragEvent(paramDragEvent))
                bool1 = true; 
            } 
            hashSet.clear();
          } 
          dragEvent = this.mCurrentDragStartEvent;
          if (dragEvent != null) {
            dragEvent.recycle();
            this.mCurrentDragStartEvent = null;
          } 
          bool3 = bool1;
          if (this.mIsInterestedInDrag) {
            if (super.dispatchDragEvent(paramDragEvent))
              bool1 = true; 
            this.mIsInterestedInDrag = false;
            bool3 = bool1;
          } 
          bool1 = bool3;
        } 
      } else {
        View view2 = findFrontmostDroppableChildAt(paramDragEvent.mX, paramDragEvent.mY, (PointF)hashSet);
        if (view2 != this.mCurrentDragChild) {
          if (sCascadedDragDrop) {
            i = paramDragEvent.mAction;
            paramDragEvent.mX = 0.0F;
            paramDragEvent.mY = 0.0F;
            paramDragEvent.mClipData = null;
            if (this.mCurrentDragChild != null) {
              paramDragEvent.mAction = 6;
              this.mCurrentDragChild.dispatchDragEnterExitInPreN(paramDragEvent);
            } 
            if (view2 != null) {
              paramDragEvent.mAction = 5;
              view2.dispatchDragEnterExitInPreN(paramDragEvent);
            } 
            paramDragEvent.mAction = i;
            paramDragEvent.mX = f1;
            paramDragEvent.mY = f2;
            paramDragEvent.mClipData = (ClipData)dragEvent;
          } 
          this.mCurrentDragChild = view2;
        } 
        View view1 = view2;
        if (view2 == null) {
          view1 = view2;
          if (this.mIsInterestedInDrag)
            view1 = this; 
        } 
        bool1 = bool2;
        if (view1 != null)
          if (view1 != this) {
            paramDragEvent.mX = ((PointF)hashSet).x;
            paramDragEvent.mY = ((PointF)hashSet).y;
            bool3 = view1.dispatchDragEvent(paramDragEvent);
            paramDragEvent.mX = f1;
            paramDragEvent.mY = f2;
            bool1 = bool3;
            if (this.mIsInterestedInDrag) {
              if (sCascadedDragDrop) {
                bool2 = bool3;
              } else {
                bool2 = paramDragEvent.mEventHandlerWasCalled;
              } 
              bool1 = bool3;
              if (!bool2)
                bool1 = super.dispatchDragEvent(paramDragEvent); 
            } 
          } else {
            bool1 = super.dispatchDragEvent(paramDragEvent);
          }  
      } 
    } else {
      this.mCurrentDragChild = null;
      this.mCurrentDragStartEvent = DragEvent.obtain(paramDragEvent);
      HashSet<View> hashSet = this.mChildrenInterestedInDrag;
      if (hashSet == null) {
        this.mChildrenInterestedInDrag = new HashSet<>();
      } else {
        hashSet.clear();
      } 
      int j = this.mChildrenCount;
      View[] arrayOfView = this.mChildren;
      for (i = 0, bool1 = bool; i < j; i++, bool1 = bool3) {
        View view = arrayOfView[i];
        view.mPrivateFlags2 &= 0xFFFFFFFC;
        bool3 = bool1;
        if (view.getVisibility() == 0) {
          bool3 = bool1;
          if (notifyChildOfDragStart(arrayOfView[i]))
            bool3 = true; 
        } 
      } 
      this.mIsInterestedInDrag = bool3 = super.dispatchDragEvent(paramDragEvent);
      if (bool3)
        bool1 = true; 
      if (!bool1) {
        this.mCurrentDragStartEvent.recycle();
        this.mCurrentDragStartEvent = null;
      } 
    } 
    return bool1;
  }
  
  View findFrontmostDroppableChildAt(float paramFloat1, float paramFloat2, PointF paramPointF) {
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (; --i >= 0; i--) {
      View view = arrayOfView[i];
      if (view.canAcceptDrag())
        if (isTransformedTouchPointInView(paramFloat1, paramFloat2, view, paramPointF))
          return view;  
    } 
    return null;
  }
  
  boolean notifyChildOfDragStart(View paramView) {
    float f1 = this.mCurrentDragStartEvent.mX;
    float f2 = this.mCurrentDragStartEvent.mY;
    float[] arrayOfFloat = getTempLocationF();
    arrayOfFloat[0] = f1;
    arrayOfFloat[1] = f2;
    transformPointToViewLocal(arrayOfFloat, paramView);
    this.mCurrentDragStartEvent.mX = arrayOfFloat[0];
    this.mCurrentDragStartEvent.mY = arrayOfFloat[1];
    boolean bool = paramView.dispatchDragEvent(this.mCurrentDragStartEvent);
    this.mCurrentDragStartEvent.mX = f1;
    this.mCurrentDragStartEvent.mY = f2;
    this.mCurrentDragStartEvent.mEventHandlerWasCalled = false;
    if (bool) {
      this.mChildrenInterestedInDrag.add(paramView);
      if (!paramView.canAcceptDrag()) {
        paramView.mPrivateFlags2 |= 0x1;
        paramView.refreshDrawableState();
      } 
    } 
    return bool;
  }
  
  @Deprecated
  public void dispatchWindowSystemUiVisiblityChanged(int paramInt) {
    super.dispatchWindowSystemUiVisiblityChanged(paramInt);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      view.dispatchWindowSystemUiVisiblityChanged(paramInt);
    } 
  }
  
  @Deprecated
  public void dispatchSystemUiVisibilityChanged(int paramInt) {
    super.dispatchSystemUiVisibilityChanged(paramInt);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      view.dispatchSystemUiVisibilityChanged(paramInt);
    } 
  }
  
  boolean updateLocalSystemUiVisibility(int paramInt1, int paramInt2) {
    boolean bool = super.updateLocalSystemUiVisibility(paramInt1, paramInt2);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      bool |= view.updateLocalSystemUiVisibility(paramInt1, paramInt2);
    } 
    return bool;
  }
  
  public boolean dispatchKeyEventPreIme(KeyEvent paramKeyEvent) {
    if ((this.mPrivateFlags & 0x12) == 18)
      return super.dispatchKeyEventPreIme(paramKeyEvent); 
    View view = this.mFocused;
    if (view != null && (view.mPrivateFlags & 0x10) == 16)
      return this.mFocused.dispatchKeyEventPreIme(paramKeyEvent); 
    return false;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    if (this.mInputEventConsistencyVerifier != null)
      this.mInputEventConsistencyVerifier.onKeyEvent(paramKeyEvent, 1); 
    if ((this.mPrivateFlags & 0x12) == 18) {
      if (super.dispatchKeyEvent(paramKeyEvent))
        return true; 
    } else {
      View view = this.mFocused;
      if (view != null && (view.mPrivateFlags & 0x10) == 16)
        if (this.mFocused.dispatchKeyEvent(paramKeyEvent))
          return true;  
    } 
    if (this.mInputEventConsistencyVerifier != null)
      this.mInputEventConsistencyVerifier.onUnhandledEvent(paramKeyEvent, 1); 
    return false;
  }
  
  public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent) {
    if ((this.mPrivateFlags & 0x12) == 18)
      return super.dispatchKeyShortcutEvent(paramKeyEvent); 
    View view = this.mFocused;
    if (view != null && (view.mPrivateFlags & 0x10) == 16)
      return this.mFocused.dispatchKeyShortcutEvent(paramKeyEvent); 
    return false;
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent) {
    if (this.mInputEventConsistencyVerifier != null)
      this.mInputEventConsistencyVerifier.onTrackballEvent(paramMotionEvent, 1); 
    if ((this.mPrivateFlags & 0x12) == 18) {
      if (super.dispatchTrackballEvent(paramMotionEvent))
        return true; 
    } else {
      View view = this.mFocused;
      if (view != null && (view.mPrivateFlags & 0x10) == 16)
        if (this.mFocused.dispatchTrackballEvent(paramMotionEvent))
          return true;  
    } 
    if (this.mInputEventConsistencyVerifier != null)
      this.mInputEventConsistencyVerifier.onUnhandledEvent(paramMotionEvent, 1); 
    return false;
  }
  
  public boolean dispatchCapturedPointerEvent(MotionEvent paramMotionEvent) {
    if ((this.mPrivateFlags & 0x12) == 18) {
      if (super.dispatchCapturedPointerEvent(paramMotionEvent))
        return true; 
    } else {
      View view = this.mFocused;
      if (view != null && (view.mPrivateFlags & 0x10) == 16)
        if (this.mFocused.dispatchCapturedPointerEvent(paramMotionEvent))
          return true;  
    } 
    return false;
  }
  
  public void dispatchPointerCaptureChanged(boolean paramBoolean) {
    exitHoverTargets();
    super.dispatchPointerCaptureChanged(paramBoolean);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchPointerCaptureChanged(paramBoolean); 
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    float f1 = paramMotionEvent.getX(paramInt);
    float f2 = paramMotionEvent.getY(paramInt);
    if (isOnScrollbarThumb(f1, f2) || isDraggingScrollBar())
      return PointerIcon.getSystemIcon(this.mContext, 1000); 
    int i = this.mChildrenCount;
    if (i != 0) {
      boolean bool;
      ArrayList<View> arrayList = buildOrderedChildList();
      if (arrayList == null && isChildrenDrawingOrderEnabled()) {
        bool = true;
      } else {
        bool = false;
      } 
      View[] arrayOfView = this.mChildren;
      for (int j = i - 1; j >= 0; j--) {
        View view2;
        int k = getAndVerifyPreorderedIndex(i, j, bool);
        View view1 = getAndVerifyPreorderedView(arrayList, arrayOfView, k);
        if (paramMotionEvent.isTargetAccessibilityFocus()) {
          view2 = findChildWithAccessibilityFocus();
        } else {
          view2 = null;
        } 
        if (!view1.canReceivePointerEvents() || !isTransformedTouchPointInView(f1, f2, view1, (PointF)null)) {
          k = j;
          if (view2 != null) {
            if (view2 != view1)
              continue; 
            k = i - 1;
          } 
          paramMotionEvent.setTargetAccessibilityFocus(false);
          j = k;
          continue;
        } 
        PointerIcon pointerIcon = dispatchResolvePointerIcon(paramMotionEvent, paramInt, view1);
        if (pointerIcon != null) {
          if (arrayList != null)
            arrayList.clear(); 
          return pointerIcon;
        } 
        continue;
      } 
      if (arrayList != null)
        arrayList.clear(); 
    } 
    return super.onResolvePointerIcon(paramMotionEvent, paramInt);
  }
  
  private PointerIcon dispatchResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt, View paramView) {
    PointerIcon pointerIcon;
    if (!paramView.hasIdentityMatrix()) {
      MotionEvent motionEvent = getTransformedMotionEvent(paramMotionEvent, paramView);
      pointerIcon = paramView.onResolvePointerIcon(motionEvent, paramInt);
      motionEvent.recycle();
    } else {
      float f1 = (this.mScrollX - paramView.mLeft);
      float f2 = (this.mScrollY - paramView.mTop);
      pointerIcon.offsetLocation(f1, f2);
      PointerIcon pointerIcon1 = paramView.onResolvePointerIcon((MotionEvent)pointerIcon, paramInt);
      pointerIcon.offsetLocation(-f1, -f2);
      pointerIcon = pointerIcon1;
    } 
    return pointerIcon;
  }
  
  private int getAndVerifyPreorderedIndex(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (paramBoolean) {
      paramInt2 = getChildDrawingOrder(paramInt1, paramInt2);
      if (paramInt2 < paramInt1) {
        paramInt1 = paramInt2;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getChildDrawingOrder() returned invalid index ");
        stringBuilder.append(paramInt2);
        stringBuilder.append(" (child count is ");
        stringBuilder.append(paramInt1);
        stringBuilder.append(")");
        throw new IndexOutOfBoundsException(stringBuilder.toString());
      } 
    } else {
      paramInt1 = paramInt2;
    } 
    return paramInt1;
  }
  
  protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    int j;
    MotionEvent motionEvent2, motionEvent3;
    int i = paramMotionEvent.getAction();
    boolean bool1 = onInterceptHoverEvent(paramMotionEvent);
    paramMotionEvent.setAction(i);
    MotionEvent motionEvent1 = paramMotionEvent;
    boolean bool2 = false, bool3 = false;
    HoverTarget hoverTarget = this.mFirstHoverTarget;
    this.mFirstHoverTarget = null;
    if (!bool1 && i != 10) {
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      int k = this.mChildrenCount;
      if (k != 0) {
        ArrayList<View> arrayList = buildOrderedChildList();
        if (arrayList == null && isChildrenDrawingOrderEnabled()) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        View[] arrayOfView = this.mChildren;
        HoverTarget hoverTarget1 = null;
        int m = k - 1;
        HoverTarget hoverTarget2 = hoverTarget;
        motionEvent2 = motionEvent1;
        label107: while (true) {
          if (m >= 0) {
            boolean bool = bool2;
            int n = getAndVerifyPreorderedIndex(k, m, bool);
            View view = getAndVerifyPreorderedView(arrayList, arrayOfView, n);
            if (!view.canReceivePointerEvents() || !isTransformedTouchPointInView(f1, f2, view, (PointF)null)) {
              bool = bool2;
              HoverTarget hoverTarget6 = hoverTarget1;
              hoverTarget1 = hoverTarget2;
              m--;
              bool2 = bool;
              hoverTarget2 = hoverTarget1;
              hoverTarget1 = hoverTarget6;
            } 
            HoverTarget hoverTarget4 = hoverTarget2;
            HoverTarget hoverTarget3 = hoverTarget2, hoverTarget5 = null;
            bool2 = bool;
            hoverTarget2 = hoverTarget4;
            hoverTarget4 = hoverTarget5;
            while (true) {
              MotionEvent motionEvent;
              if (hoverTarget2 == null) {
                hoverTarget4 = HoverTarget.obtain(view);
                hoverTarget2 = hoverTarget3;
                n = 0;
                hoverTarget3 = hoverTarget4;
              } else if (hoverTarget2.child == view) {
                if (hoverTarget4 != null) {
                  hoverTarget4.next = hoverTarget2.next;
                } else {
                  hoverTarget3 = hoverTarget2.next;
                } 
                hoverTarget2.next = null;
                n = 1;
                hoverTarget4 = hoverTarget3;
                hoverTarget3 = hoverTarget2;
                hoverTarget2 = hoverTarget4;
              } else {
                hoverTarget4 = hoverTarget2;
                hoverTarget2 = hoverTarget2.next;
                continue;
              } 
              bool = bool2;
              if (hoverTarget1 != null) {
                hoverTarget1.next = hoverTarget3;
              } else {
                this.mFirstHoverTarget = hoverTarget3;
              } 
              hoverTarget4 = hoverTarget3;
              if (i == 9) {
                motionEvent = motionEvent2;
                bool2 = bool3;
                if (n == 0) {
                  bool2 = bool3 | dispatchTransformedGenericPointerEvent(paramMotionEvent, view);
                  motionEvent = motionEvent2;
                } 
              } else {
                motionEvent = motionEvent2;
                bool2 = bool3;
                if (i == 7)
                  if (n == 0) {
                    motionEvent = obtainMotionEventNoHistoryOrSelf(motionEvent2);
                    motionEvent.setAction(9);
                    bool2 = dispatchTransformedGenericPointerEvent(motionEvent, view);
                    motionEvent.setAction(i);
                    int i1 = bool3 | bool2 | dispatchTransformedGenericPointerEvent(motionEvent, view);
                  } else {
                    bool2 = bool3 | dispatchTransformedGenericPointerEvent(paramMotionEvent, view);
                    motionEvent = motionEvent2;
                  }  
              } 
              motionEvent2 = motionEvent;
              bool3 = bool2;
              hoverTarget1 = hoverTarget2;
              if (bool2) {
                bool3 = bool2;
                hoverTarget1 = hoverTarget2;
                break;
              } 
              continue label107;
            } 
            break;
          } 
          hoverTarget1 = hoverTarget2;
          motionEvent1 = motionEvent2;
          break;
        } 
        motionEvent3 = motionEvent1;
        bool2 = bool3;
        hoverTarget = hoverTarget1;
        if (arrayList != null) {
          arrayList.clear();
          motionEvent3 = motionEvent1;
          bool2 = bool3;
          hoverTarget = hoverTarget1;
        } 
      } else {
        motionEvent3 = motionEvent1;
      } 
    } else {
      motionEvent3 = motionEvent1;
    } 
    while (hoverTarget != null) {
      View view = hoverTarget.child;
      if (i == 10) {
        bool2 |= dispatchTransformedGenericPointerEvent(paramMotionEvent, view);
      } else {
        if (i == 7) {
          bool3 = paramMotionEvent.isHoverExitPending();
          paramMotionEvent.setHoverExitPending(true);
          dispatchTransformedGenericPointerEvent(paramMotionEvent, view);
          paramMotionEvent.setHoverExitPending(bool3);
        } 
        motionEvent3 = obtainMotionEventNoHistoryOrSelf(motionEvent3);
        motionEvent3.setAction(10);
        dispatchTransformedGenericPointerEvent(motionEvent3, view);
        motionEvent3.setAction(i);
      } 
      HoverTarget hoverTarget1 = hoverTarget.next;
      hoverTarget.recycle();
      hoverTarget = hoverTarget1;
    } 
    if (!bool2 && i != 10 && !paramMotionEvent.isHoverExitPending()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    bool3 = this.mHoveredSelf;
    if (bool1 == bool3) {
      motionEvent2 = motionEvent3;
      bool3 = bool2;
      if (bool1) {
        bool3 = bool2 | super.dispatchHoverEvent(paramMotionEvent);
        motionEvent2 = motionEvent3;
      } 
    } else {
      motionEvent1 = motionEvent3;
      boolean bool = bool2;
      if (bool3) {
        if (i == 10) {
          bool2 |= super.dispatchHoverEvent(paramMotionEvent);
        } else {
          if (i == 7)
            super.dispatchHoverEvent(paramMotionEvent); 
          motionEvent3 = obtainMotionEventNoHistoryOrSelf(motionEvent3);
          motionEvent3.setAction(10);
          super.dispatchHoverEvent(motionEvent3);
          motionEvent3.setAction(i);
        } 
        this.mHoveredSelf = false;
        bool = bool2;
        motionEvent1 = motionEvent3;
      } 
      motionEvent2 = motionEvent1;
      bool3 = bool;
      if (bool1)
        if (i == 9) {
          bool3 = bool | super.dispatchHoverEvent(paramMotionEvent);
          this.mHoveredSelf = true;
          motionEvent2 = motionEvent1;
        } else {
          motionEvent2 = motionEvent1;
          bool3 = bool;
          if (i == 7) {
            motionEvent2 = obtainMotionEventNoHistoryOrSelf(motionEvent1);
            motionEvent2.setAction(9);
            bool2 = super.dispatchHoverEvent(motionEvent2);
            motionEvent2.setAction(i);
            j = bool | bool2 | super.dispatchHoverEvent(motionEvent2);
            this.mHoveredSelf = true;
          } 
        }  
    } 
    if (motionEvent2 != paramMotionEvent)
      motionEvent2.recycle(); 
    return j;
  }
  
  private void exitHoverTargets() {
    if (this.mHoveredSelf || this.mFirstHoverTarget != null) {
      long l = SystemClock.uptimeMillis();
      MotionEvent motionEvent = MotionEvent.obtain(l, l, 10, 0.0F, 0.0F, 0);
      motionEvent.setSource(4098);
      dispatchHoverEvent(motionEvent);
      motionEvent.recycle();
    } 
  }
  
  private void cancelHoverTarget(View paramView) {
    MotionEvent motionEvent = null;
    HoverTarget hoverTarget = this.mFirstHoverTarget;
    while (hoverTarget != null) {
      MotionEvent motionEvent1;
      HoverTarget hoverTarget2 = hoverTarget.next;
      if (hoverTarget.child == paramView) {
        if (motionEvent == null) {
          this.mFirstHoverTarget = hoverTarget2;
        } else {
          ((HoverTarget)motionEvent).next = hoverTarget2;
        } 
        hoverTarget.recycle();
        long l = SystemClock.uptimeMillis();
        motionEvent1 = MotionEvent.obtain(l, l, 10, 0.0F, 0.0F, 0);
        motionEvent1.setSource(4098);
        paramView.dispatchHoverEvent(motionEvent1);
        motionEvent1.recycle();
        return;
      } 
      motionEvent = motionEvent1;
      HoverTarget hoverTarget1 = hoverTarget2;
    } 
  }
  
  boolean dispatchTooltipHoverEvent(MotionEvent paramMotionEvent) {
    View view2;
    int i = paramMotionEvent.getAction();
    if (i != 7) {
      if (i == 10) {
        View view = this.mTooltipHoverTarget;
        if (view != null) {
          view.dispatchTooltipHoverEvent(paramMotionEvent);
          this.mTooltipHoverTarget = null;
        } else if (this.mTooltipHoveredSelf) {
          super.dispatchTooltipHoverEvent(paramMotionEvent);
          this.mTooltipHoveredSelf = false;
        } 
      } 
      return false;
    } 
    View arrayOfView[] = null, view3 = null;
    int j = this.mChildrenCount;
    if (j != 0) {
      View view;
      boolean bool1;
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      ArrayList<View> arrayList = buildOrderedChildList();
      if (arrayList == null && isChildrenDrawingOrderEnabled()) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      arrayOfView = this.mChildren;
      int k = j - 1;
      while (true) {
        view = view3;
        if (k >= 0) {
          int m = getAndVerifyPreorderedIndex(j, k, bool1);
          view = getAndVerifyPreorderedView(arrayList, arrayOfView, m);
          if (view.canReceivePointerEvents() && isTransformedTouchPointInView(f1, f2, view, (PointF)null))
            if (dispatchTooltipHoverEvent(paramMotionEvent, view))
              break;  
          k--;
          continue;
        } 
        break;
      } 
      view2 = view;
      if (arrayList != null) {
        arrayList.clear();
        view2 = view;
      } 
    } 
    View view1 = this.mTooltipHoverTarget;
    if (view1 != view2) {
      if (view1 != null) {
        paramMotionEvent.setAction(10);
        this.mTooltipHoverTarget.dispatchTooltipHoverEvent(paramMotionEvent);
        paramMotionEvent.setAction(i);
      } 
      this.mTooltipHoverTarget = view2;
    } 
    if (this.mTooltipHoverTarget != null) {
      if (this.mTooltipHoveredSelf) {
        this.mTooltipHoveredSelf = false;
        paramMotionEvent.setAction(10);
        super.dispatchTooltipHoverEvent(paramMotionEvent);
        paramMotionEvent.setAction(i);
      } 
      return true;
    } 
    boolean bool = super.dispatchTooltipHoverEvent(paramMotionEvent);
    return bool;
  }
  
  private boolean dispatchTooltipHoverEvent(MotionEvent paramMotionEvent, View paramView) {
    boolean bool;
    if (!paramView.hasIdentityMatrix()) {
      paramMotionEvent = getTransformedMotionEvent(paramMotionEvent, paramView);
      bool = paramView.dispatchTooltipHoverEvent(paramMotionEvent);
      paramMotionEvent.recycle();
    } else {
      float f1 = (this.mScrollX - paramView.mLeft);
      float f2 = (this.mScrollY - paramView.mTop);
      paramMotionEvent.offsetLocation(f1, f2);
      bool = paramView.dispatchTooltipHoverEvent(paramMotionEvent);
      paramMotionEvent.offsetLocation(-f1, -f2);
    } 
    return bool;
  }
  
  private void exitTooltipHoverTargets() {
    if (this.mTooltipHoveredSelf || this.mTooltipHoverTarget != null) {
      long l = SystemClock.uptimeMillis();
      MotionEvent motionEvent = MotionEvent.obtain(l, l, 10, 0.0F, 0.0F, 0);
      motionEvent.setSource(4098);
      dispatchTooltipHoverEvent(motionEvent);
      motionEvent.recycle();
    } 
  }
  
  protected boolean hasHoveredChild() {
    boolean bool;
    if (this.mFirstHoverTarget != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected boolean pointInHoveredChild(MotionEvent paramMotionEvent) {
    if (this.mFirstHoverTarget != null)
      return isTransformedTouchPointInView(paramMotionEvent.getX(), paramMotionEvent.getY(), this.mFirstHoverTarget.child, (PointF)null); 
    return false;
  }
  
  public void addChildrenForAccessibility(ArrayList<View> paramArrayList) {
    if (getAccessibilityNodeProvider() != null)
      return; 
    ChildListForAccessibility childListForAccessibility = ChildListForAccessibility.obtain(this, true);
    try {
      int i = childListForAccessibility.getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = childListForAccessibility.getChildAt(b);
        if ((view.mViewFlags & 0xC) == 0)
          if (view.includeForAccessibility()) {
            paramArrayList.add(view);
          } else {
            view.addChildrenForAccessibility(paramArrayList);
          }  
      } 
      return;
    } finally {
      childListForAccessibility.recycle();
    } 
  }
  
  public boolean onInterceptHoverEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.isFromSource(8194)) {
      int i = paramMotionEvent.getAction();
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      if ((i == 7 || i == 9) && isOnScrollbar(f1, f2))
        return true; 
    } 
    return false;
  }
  
  private static MotionEvent obtainMotionEventNoHistoryOrSelf(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.getHistorySize() == 0)
      return paramMotionEvent; 
    return MotionEvent.obtainNoHistory(paramMotionEvent);
  }
  
  protected boolean dispatchGenericPointerEvent(MotionEvent paramMotionEvent) {
    int i = this.mChildrenCount;
    if (i != 0) {
      boolean bool;
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      ArrayList<View> arrayList = buildOrderedChildList();
      if (arrayList == null && isChildrenDrawingOrderEnabled()) {
        bool = true;
      } else {
        bool = false;
      } 
      View[] arrayOfView = this.mChildren;
      for (int j = i - 1; j >= 0; j--) {
        int k = getAndVerifyPreorderedIndex(i, j, bool);
        View view = getAndVerifyPreorderedView(arrayList, arrayOfView, k);
        if (view.canReceivePointerEvents() && isTransformedTouchPointInView(f1, f2, view, (PointF)null))
          if (dispatchTransformedGenericPointerEvent(paramMotionEvent, view)) {
            if (arrayList != null)
              arrayList.clear(); 
            return true;
          }  
      } 
      if (arrayList != null)
        arrayList.clear(); 
    } 
    return super.dispatchGenericPointerEvent(paramMotionEvent);
  }
  
  protected boolean dispatchGenericFocusedEvent(MotionEvent paramMotionEvent) {
    if ((this.mPrivateFlags & 0x12) == 18)
      return super.dispatchGenericFocusedEvent(paramMotionEvent); 
    View view = this.mFocused;
    if (view != null && (view.mPrivateFlags & 0x10) == 16)
      return this.mFocused.dispatchGenericMotionEvent(paramMotionEvent); 
    return false;
  }
  
  private boolean dispatchTransformedGenericPointerEvent(MotionEvent paramMotionEvent, View paramView) {
    boolean bool;
    if (!paramView.hasIdentityMatrix()) {
      paramMotionEvent = getTransformedMotionEvent(paramMotionEvent, paramView);
      bool = paramView.dispatchGenericMotionEvent(paramMotionEvent);
      paramMotionEvent.recycle();
    } else {
      float f1 = (this.mScrollX - paramView.mLeft);
      float f2 = (this.mScrollY - paramView.mTop);
      paramMotionEvent.offsetLocation(f1, f2);
      bool = paramView.dispatchGenericMotionEvent(paramMotionEvent);
      paramMotionEvent.offsetLocation(-f1, -f2);
    } 
    return bool;
  }
  
  private MotionEvent getTransformedMotionEvent(MotionEvent paramMotionEvent, View paramView) {
    float f1 = (this.mScrollX - paramView.mLeft);
    float f2 = (this.mScrollY - paramView.mTop);
    paramMotionEvent = MotionEvent.obtain(paramMotionEvent);
    paramMotionEvent.offsetLocation(f1, f2);
    if (!paramView.hasIdentityMatrix())
      paramMotionEvent.transform(paramView.getInverseMatrix()); 
    return paramMotionEvent;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mViewHooks : Landroid/view/IOplusViewHooks;
    //   4: invokeinterface getScrollBarEffect : ()Lcom/oplus/view/IOplusScrollBarEffect;
    //   9: aload_1
    //   10: invokeinterface onTouchEvent : (Landroid/view/MotionEvent;)V
    //   15: aload_0
    //   16: getfield mInputEventConsistencyVerifier : Landroid/view/InputEventConsistencyVerifier;
    //   19: ifnull -> 31
    //   22: aload_0
    //   23: getfield mInputEventConsistencyVerifier : Landroid/view/InputEventConsistencyVerifier;
    //   26: aload_1
    //   27: iconst_1
    //   28: invokevirtual onTouchEvent : (Landroid/view/MotionEvent;I)V
    //   31: aload_1
    //   32: invokevirtual isTargetAccessibilityFocus : ()Z
    //   35: ifeq -> 50
    //   38: aload_0
    //   39: invokevirtual isAccessibilityFocusedViewOrHost : ()Z
    //   42: ifeq -> 50
    //   45: aload_1
    //   46: iconst_0
    //   47: invokevirtual setTargetAccessibilityFocus : (Z)V
    //   50: iconst_0
    //   51: istore_2
    //   52: aload_0
    //   53: aload_1
    //   54: invokevirtual onFilterTouchEventForSecurity : (Landroid/view/MotionEvent;)Z
    //   57: ifeq -> 1008
    //   60: aload_1
    //   61: invokevirtual getAction : ()I
    //   64: istore_3
    //   65: iload_3
    //   66: sipush #255
    //   69: iand
    //   70: istore #4
    //   72: iload #4
    //   74: ifne -> 86
    //   77: aload_0
    //   78: aload_1
    //   79: invokespecial cancelAndClearTouchTargets : (Landroid/view/MotionEvent;)V
    //   82: aload_0
    //   83: invokespecial resetTouchState : ()V
    //   86: iload #4
    //   88: ifeq -> 107
    //   91: aload_0
    //   92: getfield mFirstTouchTarget : Landroid/view/ViewGroup$TouchTarget;
    //   95: ifnull -> 101
    //   98: goto -> 107
    //   101: iconst_1
    //   102: istore #5
    //   104: goto -> 153
    //   107: aload_0
    //   108: getfield mGroupFlags : I
    //   111: ldc 524288
    //   113: iand
    //   114: ifeq -> 123
    //   117: iconst_1
    //   118: istore #6
    //   120: goto -> 126
    //   123: iconst_0
    //   124: istore #6
    //   126: iload #6
    //   128: ifne -> 146
    //   131: aload_0
    //   132: aload_1
    //   133: invokevirtual onInterceptTouchEvent : (Landroid/view/MotionEvent;)Z
    //   136: istore #7
    //   138: aload_1
    //   139: iload_3
    //   140: invokevirtual setAction : (I)V
    //   143: goto -> 149
    //   146: iconst_0
    //   147: istore #7
    //   149: iload #7
    //   151: istore #5
    //   153: iload #5
    //   155: ifne -> 165
    //   158: aload_0
    //   159: getfield mFirstTouchTarget : Landroid/view/ViewGroup$TouchTarget;
    //   162: ifnull -> 170
    //   165: aload_1
    //   166: iconst_0
    //   167: invokevirtual setTargetAccessibilityFocus : (Z)V
    //   170: aload_0
    //   171: invokestatic resetCancelNextUpFlag : (Landroid/view/View;)Z
    //   174: ifne -> 192
    //   177: iload #4
    //   179: iconst_3
    //   180: if_icmpne -> 186
    //   183: goto -> 192
    //   186: iconst_0
    //   187: istore #8
    //   189: goto -> 195
    //   192: iconst_1
    //   193: istore #8
    //   195: aload_1
    //   196: invokevirtual getSource : ()I
    //   199: sipush #8194
    //   202: if_icmpne -> 211
    //   205: iconst_1
    //   206: istore #9
    //   208: goto -> 214
    //   211: iconst_0
    //   212: istore #9
    //   214: aload_0
    //   215: getfield mGroupFlags : I
    //   218: ldc 2097152
    //   220: iand
    //   221: ifeq -> 235
    //   224: iload #9
    //   226: ifne -> 235
    //   229: iconst_1
    //   230: istore #10
    //   232: goto -> 238
    //   235: iconst_0
    //   236: istore #10
    //   238: aconst_null
    //   239: astore #11
    //   241: aconst_null
    //   242: astore #12
    //   244: aconst_null
    //   245: astore #13
    //   247: iconst_0
    //   248: istore #6
    //   250: iload #8
    //   252: ifne -> 760
    //   255: iload #5
    //   257: ifne -> 760
    //   260: aload_1
    //   261: invokevirtual isTargetAccessibilityFocus : ()Z
    //   264: ifeq -> 275
    //   267: aload_0
    //   268: invokespecial findChildWithAccessibilityFocus : ()Landroid/view/View;
    //   271: pop
    //   272: goto -> 275
    //   275: iload #4
    //   277: ifeq -> 304
    //   280: iload #10
    //   282: ifeq -> 291
    //   285: iload #4
    //   287: iconst_5
    //   288: if_icmpeq -> 304
    //   291: iload #4
    //   293: bipush #7
    //   295: if_icmpne -> 301
    //   298: goto -> 304
    //   301: goto -> 760
    //   304: aload_1
    //   305: invokevirtual getActionIndex : ()I
    //   308: istore #14
    //   310: iload #10
    //   312: ifeq -> 328
    //   315: iconst_1
    //   316: aload_1
    //   317: iload #14
    //   319: invokevirtual getPointerId : (I)I
    //   322: ishl
    //   323: istore #15
    //   325: goto -> 331
    //   328: iconst_m1
    //   329: istore #15
    //   331: aload_0
    //   332: iload #15
    //   334: invokespecial removePointersFromTouchTargets : (I)V
    //   337: aload_0
    //   338: getfield mChildrenCount : I
    //   341: istore #16
    //   343: iconst_0
    //   344: ifne -> 685
    //   347: iload #16
    //   349: ifeq -> 685
    //   352: iload #9
    //   354: ifeq -> 366
    //   357: aload_1
    //   358: invokevirtual getXCursorPosition : ()F
    //   361: fstore #17
    //   363: goto -> 374
    //   366: aload_1
    //   367: iload #14
    //   369: invokevirtual getX : (I)F
    //   372: fstore #17
    //   374: fload #17
    //   376: fstore #18
    //   378: iload #9
    //   380: ifeq -> 392
    //   383: aload_1
    //   384: invokevirtual getYCursorPosition : ()F
    //   387: fstore #17
    //   389: goto -> 400
    //   392: aload_1
    //   393: iload #14
    //   395: invokevirtual getY : (I)F
    //   398: fstore #17
    //   400: aload_0
    //   401: invokevirtual buildTouchDispatchChildList : ()Ljava/util/ArrayList;
    //   404: astore #12
    //   406: aload #12
    //   408: ifnonnull -> 424
    //   411: aload_0
    //   412: invokevirtual isChildrenDrawingOrderEnabled : ()Z
    //   415: ifeq -> 424
    //   418: iconst_1
    //   419: istore #7
    //   421: goto -> 427
    //   424: iconst_0
    //   425: istore #7
    //   427: aload_0
    //   428: getfield mChildren : [Landroid/view/View;
    //   431: astore #11
    //   433: iload #16
    //   435: iconst_1
    //   436: isub
    //   437: istore #19
    //   439: iload #9
    //   441: istore #20
    //   443: iload #16
    //   445: istore #9
    //   447: iload #19
    //   449: iflt -> 656
    //   452: aload_0
    //   453: iload #9
    //   455: iload #19
    //   457: iload #7
    //   459: invokespecial getAndVerifyPreorderedIndex : (IIZ)I
    //   462: istore #16
    //   464: aload #12
    //   466: aload #11
    //   468: iload #16
    //   470: invokestatic getAndVerifyPreorderedView : (Ljava/util/ArrayList;[Landroid/view/View;I)Landroid/view/View;
    //   473: astore #21
    //   475: aload #21
    //   477: invokevirtual canReceivePointerEvents : ()Z
    //   480: ifeq -> 650
    //   483: aload_0
    //   484: fload #18
    //   486: fload #17
    //   488: aload #21
    //   490: aconst_null
    //   491: invokevirtual isTransformedTouchPointInView : (FFLandroid/view/View;Landroid/graphics/PointF;)Z
    //   494: ifne -> 500
    //   497: goto -> 650
    //   500: aload_0
    //   501: aload #21
    //   503: invokespecial getTouchTarget : (Landroid/view/View;)Landroid/view/ViewGroup$TouchTarget;
    //   506: astore #13
    //   508: aload #13
    //   510: ifnull -> 529
    //   513: aload #13
    //   515: aload #13
    //   517: getfield pointerIdBits : I
    //   520: iload #15
    //   522: ior
    //   523: putfield pointerIdBits : I
    //   526: goto -> 656
    //   529: aload #21
    //   531: invokestatic resetCancelNextUpFlag : (Landroid/view/View;)Z
    //   534: pop
    //   535: aload_0
    //   536: aload_1
    //   537: iconst_0
    //   538: aload #21
    //   540: iload #15
    //   542: invokespecial dispatchTransformedTouchEvent : (Landroid/view/MotionEvent;ZLandroid/view/View;I)Z
    //   545: ifeq -> 642
    //   548: aload_0
    //   549: aload_1
    //   550: invokevirtual getDownTime : ()J
    //   553: putfield mLastTouchDownTime : J
    //   556: aload #12
    //   558: ifnull -> 604
    //   561: iconst_0
    //   562: istore #6
    //   564: iload #6
    //   566: iload #9
    //   568: if_icmpge -> 601
    //   571: aload #11
    //   573: iload #16
    //   575: aaload
    //   576: aload_0
    //   577: getfield mChildren : [Landroid/view/View;
    //   580: iload #6
    //   582: aaload
    //   583: if_acmpne -> 595
    //   586: aload_0
    //   587: iload #6
    //   589: putfield mLastTouchDownIndex : I
    //   592: goto -> 601
    //   595: iinc #6, 1
    //   598: goto -> 564
    //   601: goto -> 610
    //   604: aload_0
    //   605: iload #16
    //   607: putfield mLastTouchDownIndex : I
    //   610: aload_0
    //   611: aload_1
    //   612: invokevirtual getX : ()F
    //   615: putfield mLastTouchDownX : F
    //   618: aload_0
    //   619: aload_1
    //   620: invokevirtual getY : ()F
    //   623: putfield mLastTouchDownY : F
    //   626: aload_0
    //   627: aload #21
    //   629: iload #15
    //   631: invokespecial addTouchTarget : (Landroid/view/View;I)Landroid/view/ViewGroup$TouchTarget;
    //   634: astore #13
    //   636: iconst_1
    //   637: istore #6
    //   639: goto -> 656
    //   642: aload_1
    //   643: iconst_0
    //   644: invokevirtual setTargetAccessibilityFocus : (Z)V
    //   647: goto -> 650
    //   650: iinc #19, -1
    //   653: goto -> 447
    //   656: aload #13
    //   658: astore #11
    //   660: iload #6
    //   662: istore #9
    //   664: aload #12
    //   666: ifnull -> 688
    //   669: aload #12
    //   671: invokevirtual clear : ()V
    //   674: aload #13
    //   676: astore #11
    //   678: iload #6
    //   680: istore #9
    //   682: goto -> 688
    //   685: iconst_0
    //   686: istore #9
    //   688: aload #11
    //   690: astore #12
    //   692: iload #9
    //   694: istore #6
    //   696: aload #11
    //   698: ifnonnull -> 763
    //   701: aload #11
    //   703: astore #12
    //   705: iload #9
    //   707: istore #6
    //   709: aload_0
    //   710: getfield mFirstTouchTarget : Landroid/view/ViewGroup$TouchTarget;
    //   713: ifnull -> 763
    //   716: aload_0
    //   717: getfield mFirstTouchTarget : Landroid/view/ViewGroup$TouchTarget;
    //   720: astore #12
    //   722: aload #12
    //   724: getfield next : Landroid/view/ViewGroup$TouchTarget;
    //   727: ifnull -> 740
    //   730: aload #12
    //   732: getfield next : Landroid/view/ViewGroup$TouchTarget;
    //   735: astore #12
    //   737: goto -> 722
    //   740: aload #12
    //   742: aload #12
    //   744: getfield pointerIdBits : I
    //   747: iload #15
    //   749: ior
    //   750: putfield pointerIdBits : I
    //   753: iload #9
    //   755: istore #6
    //   757: goto -> 763
    //   760: iconst_0
    //   761: istore #6
    //   763: iconst_0
    //   764: istore #7
    //   766: aload_0
    //   767: getfield mFirstTouchTarget : Landroid/view/ViewGroup$TouchTarget;
    //   770: ifnonnull -> 787
    //   773: aload_0
    //   774: aload_1
    //   775: iload #8
    //   777: aconst_null
    //   778: iconst_m1
    //   779: invokespecial dispatchTransformedTouchEvent : (Landroid/view/MotionEvent;ZLandroid/view/View;I)Z
    //   782: istore #7
    //   784: goto -> 931
    //   787: aconst_null
    //   788: astore #11
    //   790: aload_0
    //   791: getfield mFirstTouchTarget : Landroid/view/ViewGroup$TouchTarget;
    //   794: astore #13
    //   796: aload #13
    //   798: ifnull -> 931
    //   801: aload #13
    //   803: getfield next : Landroid/view/ViewGroup$TouchTarget;
    //   806: astore #21
    //   808: iload #6
    //   810: ifeq -> 825
    //   813: aload #13
    //   815: aload #12
    //   817: if_acmpne -> 825
    //   820: iconst_1
    //   821: istore_2
    //   822: goto -> 917
    //   825: aload #13
    //   827: getfield child : Landroid/view/View;
    //   830: invokestatic resetCancelNextUpFlag : (Landroid/view/View;)Z
    //   833: ifne -> 850
    //   836: iload #5
    //   838: ifeq -> 844
    //   841: goto -> 850
    //   844: iconst_0
    //   845: istore #22
    //   847: goto -> 853
    //   850: iconst_1
    //   851: istore #22
    //   853: aload_0
    //   854: aload_1
    //   855: iload #22
    //   857: aload #13
    //   859: getfield child : Landroid/view/View;
    //   862: aload #13
    //   864: getfield pointerIdBits : I
    //   867: invokespecial dispatchTransformedTouchEvent : (Landroid/view/MotionEvent;ZLandroid/view/View;I)Z
    //   870: ifeq -> 876
    //   873: iconst_1
    //   874: istore #7
    //   876: iload #7
    //   878: istore_2
    //   879: iload #22
    //   881: ifeq -> 917
    //   884: aload #11
    //   886: ifnonnull -> 898
    //   889: aload_0
    //   890: aload #21
    //   892: putfield mFirstTouchTarget : Landroid/view/ViewGroup$TouchTarget;
    //   895: goto -> 905
    //   898: aload #11
    //   900: aload #21
    //   902: putfield next : Landroid/view/ViewGroup$TouchTarget;
    //   905: aload #13
    //   907: invokevirtual recycle : ()V
    //   910: aload #21
    //   912: astore #13
    //   914: goto -> 796
    //   917: aload #13
    //   919: astore #11
    //   921: aload #21
    //   923: astore #13
    //   925: iload_2
    //   926: istore #7
    //   928: goto -> 796
    //   931: iload #8
    //   933: ifne -> 998
    //   936: iload #4
    //   938: iconst_1
    //   939: if_icmpeq -> 998
    //   942: iload #4
    //   944: bipush #7
    //   946: if_icmpne -> 952
    //   949: goto -> 998
    //   952: iload #7
    //   954: istore_2
    //   955: iload #10
    //   957: ifeq -> 1008
    //   960: iload #7
    //   962: istore_2
    //   963: iload #4
    //   965: bipush #6
    //   967: if_icmpne -> 1008
    //   970: aload_1
    //   971: invokevirtual getActionIndex : ()I
    //   974: istore #6
    //   976: aload_1
    //   977: iload #6
    //   979: invokevirtual getPointerId : (I)I
    //   982: istore #6
    //   984: aload_0
    //   985: iconst_1
    //   986: iload #6
    //   988: ishl
    //   989: invokespecial removePointersFromTouchTargets : (I)V
    //   992: iload #7
    //   994: istore_2
    //   995: goto -> 1008
    //   998: aload_0
    //   999: invokespecial resetTouchState : ()V
    //   1002: iload #7
    //   1004: istore_2
    //   1005: goto -> 1008
    //   1008: iload_2
    //   1009: ifne -> 1028
    //   1012: aload_0
    //   1013: getfield mInputEventConsistencyVerifier : Landroid/view/InputEventConsistencyVerifier;
    //   1016: ifnull -> 1028
    //   1019: aload_0
    //   1020: getfield mInputEventConsistencyVerifier : Landroid/view/InputEventConsistencyVerifier;
    //   1023: aload_1
    //   1024: iconst_1
    //   1025: invokevirtual onUnhandledEvent : (Landroid/view/InputEvent;I)V
    //   1028: iload_2
    //   1029: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2647	-> 0
    //   #2649	-> 15
    //   #2650	-> 22
    //   #2655	-> 31
    //   #2656	-> 45
    //   #2659	-> 50
    //   #2660	-> 52
    //   #2661	-> 60
    //   #2662	-> 65
    //   #2665	-> 72
    //   #2669	-> 77
    //   #2670	-> 82
    //   #2675	-> 86
    //   #2687	-> 101
    //   #2677	-> 107
    //   #2678	-> 126
    //   #2679	-> 131
    //   #2680	-> 138
    //   #2682	-> 146
    //   #2684	-> 149
    //   #2692	-> 153
    //   #2693	-> 165
    //   #2697	-> 170
    //   #2701	-> 195
    //   #2702	-> 214
    //   #2704	-> 238
    //   #2705	-> 247
    //   #2706	-> 250
    //   #2712	-> 260
    //   #2713	-> 267
    //   #2715	-> 275
    //   #2718	-> 304
    //   #2719	-> 310
    //   #2720	-> 328
    //   #2724	-> 331
    //   #2726	-> 337
    //   #2727	-> 343
    //   #2729	-> 352
    //   #2731	-> 378
    //   #2734	-> 400
    //   #2735	-> 406
    //   #2736	-> 411
    //   #2737	-> 427
    //   #2738	-> 433
    //   #2739	-> 452
    //   #2741	-> 464
    //   #2743	-> 475
    //   #2744	-> 483
    //   #2745	-> 497
    //   #2748	-> 500
    //   #2749	-> 508
    //   #2752	-> 513
    //   #2753	-> 526
    //   #2756	-> 529
    //   #2757	-> 535
    //   #2759	-> 548
    //   #2760	-> 556
    //   #2762	-> 561
    //   #2763	-> 571
    //   #2764	-> 586
    //   #2765	-> 592
    //   #2762	-> 595
    //   #2769	-> 604
    //   #2771	-> 610
    //   #2772	-> 618
    //   #2773	-> 626
    //   #2774	-> 636
    //   #2775	-> 639
    //   #2780	-> 642
    //   #2743	-> 650
    //   #2738	-> 650
    //   #2782	-> 656
    //   #2727	-> 685
    //   #2785	-> 685
    //   #2788	-> 716
    //   #2789	-> 722
    //   #2790	-> 730
    //   #2792	-> 740
    //   #2706	-> 760
    //   #2798	-> 760
    //   #2800	-> 773
    //   #2805	-> 787
    //   #2806	-> 790
    //   #2807	-> 796
    //   #2808	-> 801
    //   #2809	-> 808
    //   #2810	-> 820
    //   #2812	-> 825
    //   #2814	-> 853
    //   #2816	-> 873
    //   #2818	-> 876
    //   #2819	-> 884
    //   #2820	-> 889
    //   #2822	-> 898
    //   #2824	-> 905
    //   #2825	-> 910
    //   #2826	-> 914
    //   #2829	-> 917
    //   #2830	-> 917
    //   #2831	-> 917
    //   #2807	-> 931
    //   #2835	-> 931
    //   #2839	-> 952
    //   #2840	-> 970
    //   #2841	-> 976
    //   #2842	-> 984
    //   #2838	-> 998
    //   #2660	-> 1008
    //   #2846	-> 1008
    //   #2847	-> 1019
    //   #2849	-> 1028
  }
  
  public ArrayList<View> buildTouchDispatchChildList() {
    return buildOrderedChildList();
  }
  
  private View findChildWithAccessibilityFocus() {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl == null)
      return null; 
    View view = viewRootImpl.getAccessibilityFocusedHost();
    if (view == null)
      return null; 
    ViewParent viewParent = view.getParent();
    while (viewParent instanceof View) {
      if (viewParent == this)
        return view; 
      view = (View)viewParent;
      viewParent = view.getParent();
    } 
    return null;
  }
  
  private void resetTouchState() {
    clearTouchTargets();
    resetCancelNextUpFlag(this);
    this.mGroupFlags &= 0xFFF7FFFF;
    this.mNestedScrollAxes = 0;
  }
  
  private static boolean resetCancelNextUpFlag(View paramView) {
    if (paramView != null && (paramView.mPrivateFlags & 0x4000000) != 0) {
      paramView.mPrivateFlags &= 0xFBFFFFFF;
      return true;
    } 
    return false;
  }
  
  private void clearTouchTargets() {
    TouchTarget touchTarget = this.mFirstTouchTarget;
    if (touchTarget != null)
      while (true) {
        TouchTarget touchTarget1 = touchTarget.next;
        touchTarget.recycle();
        touchTarget = touchTarget1;
        if (touchTarget1 == null) {
          this.mFirstTouchTarget = null;
          break;
        } 
      }  
  }
  
  private void cancelAndClearTouchTargets(MotionEvent paramMotionEvent) {
    if (this.mFirstTouchTarget != null) {
      boolean bool = false;
      MotionEvent motionEvent = paramMotionEvent;
      if (paramMotionEvent == null) {
        long l = SystemClock.uptimeMillis();
        motionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
        motionEvent.setSource(4098);
        bool = true;
      } 
      for (TouchTarget touchTarget = this.mFirstTouchTarget; touchTarget != null; touchTarget = touchTarget.next) {
        resetCancelNextUpFlag(touchTarget.child);
        dispatchTransformedTouchEvent(motionEvent, true, touchTarget.child, touchTarget.pointerIdBits);
      } 
      clearTouchTargets();
      if (bool)
        motionEvent.recycle(); 
    } 
  }
  
  private TouchTarget getTouchTarget(View paramView) {
    for (TouchTarget touchTarget = this.mFirstTouchTarget; touchTarget != null; touchTarget = touchTarget.next) {
      if (touchTarget.child == paramView)
        return touchTarget; 
    } 
    return null;
  }
  
  private TouchTarget addTouchTarget(View paramView, int paramInt) {
    TouchTarget touchTarget = TouchTarget.obtain(paramView, paramInt);
    touchTarget.next = this.mFirstTouchTarget;
    this.mFirstTouchTarget = touchTarget;
    return touchTarget;
  }
  
  private void removePointersFromTouchTargets(int paramInt) {
    TouchTarget touchTarget1 = null;
    TouchTarget touchTarget2 = this.mFirstTouchTarget;
    while (touchTarget2 != null) {
      TouchTarget touchTarget = touchTarget2.next;
      if ((touchTarget2.pointerIdBits & paramInt) != 0) {
        touchTarget2.pointerIdBits &= paramInt ^ 0xFFFFFFFF;
        if (touchTarget2.pointerIdBits == 0) {
          if (touchTarget1 == null) {
            this.mFirstTouchTarget = touchTarget;
          } else {
            touchTarget1.next = touchTarget;
          } 
          touchTarget2.recycle();
          touchTarget2 = touchTarget;
          continue;
        } 
      } 
      touchTarget1 = touchTarget2;
      touchTarget2 = touchTarget;
    } 
  }
  
  private void cancelTouchTarget(View paramView) {
    MotionEvent motionEvent = null;
    TouchTarget touchTarget = this.mFirstTouchTarget;
    while (touchTarget != null) {
      MotionEvent motionEvent1;
      TouchTarget touchTarget2 = touchTarget.next;
      if (touchTarget.child == paramView) {
        if (motionEvent == null) {
          this.mFirstTouchTarget = touchTarget2;
        } else {
          ((TouchTarget)motionEvent).next = touchTarget2;
        } 
        touchTarget.recycle();
        long l = SystemClock.uptimeMillis();
        motionEvent1 = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
        motionEvent1.setSource(4098);
        paramView.dispatchTouchEvent(motionEvent1);
        motionEvent1.recycle();
        return;
      } 
      motionEvent = motionEvent1;
      TouchTarget touchTarget1 = touchTarget2;
    } 
  }
  
  private Rect getTempRect() {
    if (this.mTempRect == null)
      this.mTempRect = new Rect(); 
    return this.mTempRect;
  }
  
  private float[] getTempLocationF() {
    if (this.mTempPosition == null)
      this.mTempPosition = new float[2]; 
    return this.mTempPosition;
  }
  
  private Point getTempPoint() {
    if (this.mTempPoint == null)
      this.mTempPoint = new Point(); 
    return this.mTempPoint;
  }
  
  protected boolean isTransformedTouchPointInView(float paramFloat1, float paramFloat2, View paramView, PointF paramPointF) {
    float[] arrayOfFloat = getTempLocationF();
    arrayOfFloat[0] = paramFloat1;
    arrayOfFloat[1] = paramFloat2;
    transformPointToViewLocal(arrayOfFloat, paramView);
    boolean bool = paramView.pointInView(arrayOfFloat[0], arrayOfFloat[1]);
    if (bool && paramPointF != null)
      paramPointF.set(arrayOfFloat[0], arrayOfFloat[1]); 
    return bool;
  }
  
  public void transformPointToViewLocal(float[] paramArrayOffloat, View paramView) {
    paramArrayOffloat[0] = paramArrayOffloat[0] + (this.mScrollX - paramView.mLeft);
    paramArrayOffloat[1] = paramArrayOffloat[1] + (this.mScrollY - paramView.mTop);
    if (!paramView.hasIdentityMatrix())
      paramView.getInverseMatrix().mapPoints(paramArrayOffloat); 
  }
  
  private boolean dispatchTransformedTouchEvent(MotionEvent paramMotionEvent, boolean paramBoolean, View paramView, int paramInt) {
    int i = paramMotionEvent.getAction();
    if (paramBoolean || i == 3) {
      paramMotionEvent.setAction(3);
      if (paramView == null) {
        paramBoolean = super.dispatchTouchEvent(paramMotionEvent);
      } else {
        paramBoolean = paramView.dispatchTouchEvent(paramMotionEvent);
      } 
      paramMotionEvent.setAction(i);
      return paramBoolean;
    } 
    i = paramMotionEvent.getPointerIdBits();
    paramInt = i & paramInt;
    if (paramInt == 0)
      return false; 
    if (paramInt == i) {
      if (paramView == null || paramView.hasIdentityMatrix()) {
        if (paramView == null) {
          paramBoolean = super.dispatchTouchEvent(paramMotionEvent);
        } else {
          float f1 = (this.mScrollX - paramView.mLeft);
          float f2 = (this.mScrollY - paramView.mTop);
          paramMotionEvent.offsetLocation(f1, f2);
          paramBoolean = paramView.dispatchTouchEvent(paramMotionEvent);
          paramMotionEvent.offsetLocation(-f1, -f2);
        } 
        return paramBoolean;
      } 
      paramMotionEvent = MotionEvent.obtain(paramMotionEvent);
    } else {
      paramMotionEvent = paramMotionEvent.split(paramInt);
    } 
    if (paramView == null) {
      paramBoolean = super.dispatchTouchEvent(paramMotionEvent);
    } else {
      float f1 = (this.mScrollX - paramView.mLeft);
      float f2 = (this.mScrollY - paramView.mTop);
      paramMotionEvent.offsetLocation(f1, f2);
      if (!paramView.hasIdentityMatrix())
        paramMotionEvent.transform(paramView.getInverseMatrix()); 
      paramBoolean = paramView.dispatchTouchEvent(paramMotionEvent);
    } 
    paramMotionEvent.recycle();
    return paramBoolean;
  }
  
  public void setMotionEventSplittingEnabled(boolean paramBoolean) {
    if (paramBoolean) {
      this.mGroupFlags |= 0x200000;
    } else {
      this.mGroupFlags &= 0xFFDFFFFF;
    } 
  }
  
  public boolean isMotionEventSplittingEnabled() {
    boolean bool;
    if ((this.mGroupFlags & 0x200000) == 2097152) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTransitionGroup() {
    int i = this.mGroupFlags;
    boolean bool = false;
    null = false;
    if ((0x2000000 & i) != 0) {
      if ((i & 0x1000000) != 0)
        null = true; 
      return null;
    } 
    ViewOutlineProvider viewOutlineProvider = getOutlineProvider();
    if (getBackground() == null && getTransitionName() == null) {
      null = bool;
      if (viewOutlineProvider != null) {
        null = bool;
        if (viewOutlineProvider != ViewOutlineProvider.BACKGROUND)
          return true; 
      } 
      return null;
    } 
    return true;
  }
  
  public void setTransitionGroup(boolean paramBoolean) {
    int i = this.mGroupFlags | 0x2000000;
    if (paramBoolean) {
      this.mGroupFlags = i | 0x1000000;
    } else {
      this.mGroupFlags = i & 0xFEFFFFFF;
    } 
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean) {
    boolean bool;
    if ((this.mGroupFlags & 0x80000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean == bool)
      return; 
    if (paramBoolean) {
      this.mGroupFlags |= 0x80000;
    } else {
      this.mGroupFlags &= 0xFFF7FFFF;
    } 
    if (this.mParent != null)
      this.mParent.requestDisallowInterceptTouchEvent(paramBoolean); 
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.isFromSource(8194) && paramMotionEvent.getAction() == 0 && paramMotionEvent.isButtonPressed(1) && isOnScrollbarThumb(paramMotionEvent.getX(), paramMotionEvent.getY()))
      return true; 
    return false;
  }
  
  public boolean requestFocus(int paramInt, Rect paramRect) {
    StringBuilder stringBuilder;
    boolean bool;
    int i = getDescendantFocusability();
    if (i != 131072) {
      if (i != 262144) {
        if (i == 393216) {
          bool = super.requestFocus(paramInt, paramRect);
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("descendant focusability must be one of FOCUS_BEFORE_DESCENDANTS, FOCUS_AFTER_DESCENDANTS, FOCUS_BLOCK_DESCENDANTS but is ");
          stringBuilder.append(i);
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } else {
        bool = onRequestFocusInDescendants(paramInt, (Rect)stringBuilder);
        if (!bool)
          bool = super.requestFocus(paramInt, (Rect)stringBuilder); 
      } 
    } else {
      bool = super.requestFocus(paramInt, (Rect)stringBuilder);
      if (!bool)
        bool = onRequestFocusInDescendants(paramInt, (Rect)stringBuilder); 
    } 
    if (bool && !isLayoutValid() && (this.mPrivateFlags & 0x1) == 0)
      this.mPrivateFlags |= 0x1; 
    return bool;
  }
  
  protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect) {
    int j;
    byte b;
    int i = this.mChildrenCount;
    if ((paramInt & 0x2) != 0) {
      j = 0;
      b = 1;
    } else {
      j = i - 1;
      b = -1;
      i = -1;
    } 
    View[] arrayOfView = this.mChildren;
    for (; j != i; j += b) {
      View view = arrayOfView[j];
      if ((view.mViewFlags & 0xC) == 0 && view.requestFocus(paramInt, paramRect))
        return true; 
    } 
    return false;
  }
  
  public boolean restoreDefaultFocus() {
    if (this.mDefaultFocus != null && getDescendantFocusability() != 393216 && (this.mDefaultFocus.mViewFlags & 0xC) == 0) {
      View view = this.mDefaultFocus;
      if (view.restoreDefaultFocus())
        return true; 
    } 
    return super.restoreDefaultFocus();
  }
  
  public boolean restoreFocusInCluster(int paramInt) {
    if (isKeyboardNavigationCluster()) {
      boolean bool = getTouchscreenBlocksFocus();
      try {
        setTouchscreenBlocksFocusNoRefocus(false);
        return restoreFocusInClusterInternal(paramInt);
      } finally {
        setTouchscreenBlocksFocusNoRefocus(bool);
      } 
    } 
    return restoreFocusInClusterInternal(paramInt);
  }
  
  private boolean restoreFocusInClusterInternal(int paramInt) {
    if (this.mFocusedInCluster != null && getDescendantFocusability() != 393216 && (this.mFocusedInCluster.mViewFlags & 0xC) == 0) {
      View view = this.mFocusedInCluster;
      if (view.restoreFocusInCluster(paramInt))
        return true; 
    } 
    return super.restoreFocusInCluster(paramInt);
  }
  
  public boolean restoreFocusNotInCluster() {
    if (this.mFocusedInCluster != null)
      return restoreFocusInCluster(130); 
    if (isKeyboardNavigationCluster() || (this.mViewFlags & 0xC) != 0)
      return false; 
    int i = getDescendantFocusability();
    if (i == 393216)
      return super.requestFocus(130, null); 
    if (i == 131072 && super.requestFocus(130, null))
      return true; 
    for (byte b = 0; b < this.mChildrenCount; b++) {
      View view = this.mChildren[b];
      if (!view.isKeyboardNavigationCluster() && view.restoreFocusNotInCluster())
        return true; 
    } 
    if (i == 262144 && !hasFocusableChild(false))
      return super.requestFocus(130, null); 
    return false;
  }
  
  public void dispatchStartTemporaryDetach() {
    super.dispatchStartTemporaryDetach();
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchStartTemporaryDetach(); 
  }
  
  public void dispatchFinishTemporaryDetach() {
    super.dispatchFinishTemporaryDetach();
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchFinishTemporaryDetach(); 
  }
  
  void dispatchAttachedToWindow(View.AttachInfo paramAttachInfo, int paramInt) {
    this.mGroupFlags |= 0x400000;
    super.dispatchAttachedToWindow(paramAttachInfo, paramInt);
    this.mGroupFlags &= 0xFFBFFFFF;
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    int j;
    for (j = 0; j < i; j++) {
      View view = arrayOfView[j];
      int k = combineVisibility(paramInt, view.getVisibility());
      view.dispatchAttachedToWindow(paramAttachInfo, k);
    } 
    List<Integer> list = this.mTransientIndices;
    if (list == null) {
      j = 0;
    } else {
      j = list.size();
    } 
    for (i = 0; i < j; i++) {
      View view = this.mTransientViews.get(i);
      int k = combineVisibility(paramInt, view.getVisibility());
      view.dispatchAttachedToWindow(paramAttachInfo, k);
    } 
  }
  
  void dispatchScreenStateChanged(int paramInt) {
    super.dispatchScreenStateChanged(paramInt);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchScreenStateChanged(paramInt); 
  }
  
  void dispatchMovedToDisplay(Display paramDisplay, Configuration paramConfiguration) {
    super.dispatchMovedToDisplay(paramDisplay, paramConfiguration);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchMovedToDisplay(paramDisplay, paramConfiguration); 
  }
  
  public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    if (includeForAccessibility()) {
      boolean bool = super.dispatchPopulateAccessibilityEventInternal(paramAccessibilityEvent);
      if (bool)
        return bool; 
    } 
    ChildListForAccessibility childListForAccessibility = ChildListForAccessibility.obtain(this, true);
    try {
      int i = childListForAccessibility.getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = childListForAccessibility.getChildAt(b);
        if ((view.mViewFlags & 0xC) == 0) {
          boolean bool = view.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent);
          if (bool)
            return bool; 
        } 
      } 
      return false;
    } finally {
      childListForAccessibility.recycle();
    } 
  }
  
  public void dispatchProvideStructure(ViewStructure paramViewStructure) {
    String str;
    boolean bool;
    super.dispatchProvideStructure(paramViewStructure);
    if (isAssistBlocked() || paramViewStructure.getChildCount() != 0)
      return; 
    int i = this.mChildrenCount;
    if (i <= 0)
      return; 
    if (!isLaidOut()) {
      if (Helper.sVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("dispatchProvideStructure(): not laid out, ignoring ");
        stringBuilder.append(i);
        stringBuilder.append(" children of ");
        stringBuilder.append(getAccessibilityViewId());
        str = stringBuilder.toString();
        Log.v("View", str);
      } 
      return;
    } 
    str.setChildCount(i);
    ArrayList<View> arrayList = buildOrderedChildList();
    if (arrayList == null && isChildrenDrawingOrderEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    for (byte b = 0; b < i; b++) {
      byte b1;
      try {
        b1 = getAndVerifyPreorderedIndex(i, b, bool);
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        int[] arrayOfInt;
        if ((this.mContext.getApplicationInfo()).targetSdkVersion < 23) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Bad getChildDrawingOrder while collecting assist @ ");
          stringBuilder.append(b);
          stringBuilder.append(" of ");
          stringBuilder.append(i);
          Log.w("ViewGroup", stringBuilder.toString(), indexOutOfBoundsException);
          bool = false;
          if (b > 0) {
            arrayOfInt = new int[i];
            SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
            for (b1 = 0; b1 < b; b1++) {
              arrayOfInt[b1] = getChildDrawingOrder(i, b1);
              sparseBooleanArray.put(arrayOfInt[b1], true);
            } 
            b1 = 0;
            int j;
            for (j = b; j < i; j++) {
              while (sparseBooleanArray.get(b1, false))
                b1++; 
              arrayOfInt[j] = b1;
              b1++;
            } 
            arrayList = new ArrayList(i);
            for (b1 = 0; b1 < i; b1++) {
              j = arrayOfInt[b1];
              View view1 = this.mChildren[j];
              arrayList.add(view1);
            } 
            b1 = b;
          } else {
            b1 = b;
          } 
        } else {
          throw arrayOfInt;
        } 
      } 
      View view = getAndVerifyPreorderedView(arrayList, this.mChildren, b1);
      ViewStructure viewStructure = str.newChild(b);
      view.dispatchProvideStructure(viewStructure);
    } 
    if (arrayList != null)
      arrayList.clear(); 
  }
  
  public void dispatchProvideAutofillStructure(ViewStructure paramViewStructure, int paramInt) {
    String str;
    super.dispatchProvideAutofillStructure(paramViewStructure, paramInt);
    if (paramViewStructure.getChildCount() != 0)
      return; 
    if (!isLaidOut()) {
      if (Helper.sVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("dispatchProvideAutofillStructure(): not laid out, ignoring ");
        stringBuilder.append(this.mChildrenCount);
        stringBuilder.append(" children of ");
        stringBuilder.append(getAutofillId());
        str = stringBuilder.toString();
        Log.v("View", str);
      } 
      return;
    } 
    ChildListForAutoFillOrContentCapture childListForAutoFillOrContentCapture = getChildrenForAutofill(paramInt);
    int i = childListForAutoFillOrContentCapture.size();
    str.setChildCount(i);
    for (byte b = 0; b < i; b++) {
      View view = childListForAutoFillOrContentCapture.get(b);
      ViewStructure viewStructure = str.newChild(b);
      view.dispatchProvideAutofillStructure(viewStructure, paramInt);
    } 
    childListForAutoFillOrContentCapture.recycle();
  }
  
  public void dispatchProvideContentCaptureStructure() {
    super.dispatchProvideContentCaptureStructure();
    if (!isLaidOut())
      return; 
    ChildListForAutoFillOrContentCapture childListForAutoFillOrContentCapture = getChildrenForContentCapture();
    int i = childListForAutoFillOrContentCapture.size();
    for (byte b = 0; b < i; b++) {
      View view = childListForAutoFillOrContentCapture.get(b);
      view.dispatchProvideContentCaptureStructure();
    } 
    childListForAutoFillOrContentCapture.recycle();
  }
  
  private ChildListForAutoFillOrContentCapture getChildrenForAutofill(int paramInt) {
    ChildListForAutoFillOrContentCapture childListForAutoFillOrContentCapture = ChildListForAutoFillOrContentCapture.obtain();
    populateChildrenForAutofill(childListForAutoFillOrContentCapture, paramInt);
    return childListForAutoFillOrContentCapture;
  }
  
  private void populateChildrenForAutofill(ArrayList<View> paramArrayList, int paramInt) {
    boolean bool;
    int i = this.mChildrenCount;
    if (i <= 0)
      return; 
    ArrayList<View> arrayList = buildOrderedChildList();
    if (arrayList == null && isChildrenDrawingOrderEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    for (byte b = 0; b < i; b++) {
      View view;
      int j = getAndVerifyPreorderedIndex(i, b, bool);
      if (arrayList == null) {
        view = this.mChildren[j];
      } else {
        view = arrayList.get(j);
      } 
      if ((paramInt & 0x1) != 0 || view.isImportantForAutofill()) {
        paramArrayList.add(view);
      } else if (view instanceof ViewGroup) {
        ((ViewGroup)view).populateChildrenForAutofill(paramArrayList, paramInt);
      } 
    } 
  }
  
  private ChildListForAutoFillOrContentCapture getChildrenForContentCapture() {
    ChildListForAutoFillOrContentCapture childListForAutoFillOrContentCapture = ChildListForAutoFillOrContentCapture.obtain();
    populateChildrenForContentCapture(childListForAutoFillOrContentCapture);
    return childListForAutoFillOrContentCapture;
  }
  
  private void populateChildrenForContentCapture(ArrayList<View> paramArrayList) {
    boolean bool;
    int i = this.mChildrenCount;
    if (i <= 0)
      return; 
    ArrayList<View> arrayList = buildOrderedChildList();
    if (arrayList == null && isChildrenDrawingOrderEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    for (byte b = 0; b < i; b++) {
      View view;
      int j = getAndVerifyPreorderedIndex(i, b, bool);
      if (arrayList == null) {
        view = this.mChildren[j];
      } else {
        view = arrayList.get(j);
      } 
      if (view.isImportantForContentCapture()) {
        paramArrayList.add(view);
      } else if (view instanceof ViewGroup) {
        ((ViewGroup)view).populateChildrenForContentCapture(paramArrayList);
      } 
    } 
  }
  
  private static View getAndVerifyPreorderedView(ArrayList<View> paramArrayList, View[] paramArrayOfView, int paramInt) {
    View view;
    if (paramArrayList != null) {
      view = paramArrayList.get(paramInt);
      if (view == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid preorderedList contained null child at index ");
        stringBuilder.append(paramInt);
        throw new RuntimeException(stringBuilder.toString());
      } 
    } else {
      view = paramArrayOfView[paramInt];
    } 
    return view;
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (getAccessibilityNodeProvider() != null)
      return; 
    if (this.mAttachInfo != null) {
      ArrayList<View> arrayList = this.mAttachInfo.mTempArrayList;
      arrayList.clear();
      addChildrenForAccessibility(arrayList);
      int i = arrayList.size();
      for (byte b = 0; b < i; b++) {
        View view = arrayList.get(b);
        paramAccessibilityNodeInfo.addChildUnchecked(view);
      } 
      arrayList.clear();
    } 
    paramAccessibilityNodeInfo.setAvailableExtraData(Collections.singletonList("android.view.accessibility.extra.DATA_RENDERING_INFO_KEY"));
  }
  
  public void addExtraDataToAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo, String paramString, Bundle paramBundle) {
    if (paramString.equals("android.view.accessibility.extra.DATA_RENDERING_INFO_KEY")) {
      AccessibilityNodeInfo.ExtraRenderingInfo extraRenderingInfo = AccessibilityNodeInfo.ExtraRenderingInfo.obtain();
      extraRenderingInfo.setLayoutSize((getLayoutParams()).width, (getLayoutParams()).height);
      paramAccessibilityNodeInfo.setExtraRenderingInfo(extraRenderingInfo);
    } 
  }
  
  public CharSequence getAccessibilityClassName() {
    return ViewGroup.class.getName();
  }
  
  public void notifySubtreeAccessibilityStateChanged(View paramView1, View paramView2, int paramInt) {
    if (getAccessibilityLiveRegion() != 0) {
      notifyViewAccessibilityStateChangedIfNeeded(1);
    } else if (this.mParent != null) {
      try {
        this.mParent.notifySubtreeAccessibilityStateChanged(this, paramView2, paramInt);
      } catch (AbstractMethodError abstractMethodError) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mParent.getClass().getSimpleName());
        stringBuilder.append(" does not fully implement ViewParent");
        Log.e("View", stringBuilder.toString(), abstractMethodError);
      } 
    } 
  }
  
  public void notifySubtreeAccessibilityStateChangedIfNeeded() {
    if (!AccessibilityManager.getInstance(this.mContext).isEnabled() || this.mAttachInfo == null)
      return; 
    if (getImportantForAccessibility() != 4 && !isImportantForAccessibility() && getChildCount() > 0) {
      ViewParent viewParent = getParentForAccessibility();
      if (viewParent instanceof View) {
        ((View)viewParent).notifySubtreeAccessibilityStateChangedIfNeeded();
        return;
      } 
    } 
    super.notifySubtreeAccessibilityStateChangedIfNeeded();
  }
  
  void resetSubtreeAccessibilityStateChanged() {
    super.resetSubtreeAccessibilityStateChanged();
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if (view != null)
        view.resetSubtreeAccessibilityStateChanged(); 
    } 
  }
  
  int getNumChildrenForAccessibility() {
    int i = 0;
    for (byte b = 0; b < getChildCount(); b++, i = j) {
      int j;
      View view = getChildAt(b);
      if (view.includeForAccessibility()) {
        j = i + 1;
      } else {
        j = i;
        if (view instanceof ViewGroup) {
          view = view;
          j = i + view.getNumChildrenForAccessibility();
        } 
      } 
    } 
    return i;
  }
  
  public boolean onNestedPrePerformAccessibilityAction(View paramView, int paramInt, Bundle paramBundle) {
    return false;
  }
  
  void dispatchDetachedFromWindow() {
    cancelAndClearTouchTargets((MotionEvent)null);
    exitHoverTargets();
    exitTooltipHoverTargets();
    byte b = 0;
    this.mLayoutCalledWhileSuppressed = false;
    this.mChildrenInterestedInDrag = null;
    this.mIsInterestedInDrag = false;
    DragEvent dragEvent = this.mCurrentDragStartEvent;
    if (dragEvent != null) {
      dragEvent.recycle();
      this.mCurrentDragStartEvent = null;
    } 
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    int j;
    for (j = 0; j < i; j++)
      arrayOfView[j].dispatchDetachedFromWindow(); 
    clearDisappearingChildren();
    if (this.mTransientViews == null) {
      j = b;
    } else {
      j = this.mTransientIndices.size();
    } 
    for (b = 0; b < j; b++) {
      View view = this.mTransientViews.get(b);
      view.dispatchDetachedFromWindow();
    } 
    super.dispatchDetachedFromWindow();
  }
  
  protected void internalSetPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.internalSetPadding(paramInt1, paramInt2, paramInt3, paramInt4);
    if ((this.mPaddingLeft | this.mPaddingTop | this.mPaddingRight | this.mPaddingBottom) != 0) {
      this.mGroupFlags |= 0x20;
    } else {
      this.mGroupFlags &= 0xFFFFFFDF;
    } 
  }
  
  protected void dispatchSaveInstanceState(SparseArray<Parcelable> paramSparseArray) {
    super.dispatchSaveInstanceState(paramSparseArray);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if ((view.mViewFlags & 0x20000000) != 536870912)
        view.dispatchSaveInstanceState(paramSparseArray); 
    } 
  }
  
  protected void dispatchFreezeSelfOnly(SparseArray<Parcelable> paramSparseArray) {
    super.dispatchSaveInstanceState(paramSparseArray);
  }
  
  protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray) {
    super.dispatchRestoreInstanceState(paramSparseArray);
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if ((view.mViewFlags & 0x20000000) != 536870912)
        view.dispatchRestoreInstanceState(paramSparseArray); 
    } 
  }
  
  protected void dispatchThawSelfOnly(SparseArray<Parcelable> paramSparseArray) {
    super.dispatchRestoreInstanceState(paramSparseArray);
  }
  
  @Deprecated
  protected void setChildrenDrawingCacheEnabled(boolean paramBoolean) {
    if (paramBoolean || (this.mPersistentDrawingCache & 0x3) != 3) {
      View[] arrayOfView = this.mChildren;
      int i = this.mChildrenCount;
      for (byte b = 0; b < i; b++)
        arrayOfView[b].setDrawingCacheEnabled(paramBoolean); 
    } 
  }
  
  public Bitmap createSnapshot(ViewDebug.CanvasProvider paramCanvasProvider, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mChildrenCount : I
    //   4: istore_3
    //   5: aconst_null
    //   6: astore #4
    //   8: iload_2
    //   9: ifeq -> 77
    //   12: iload_3
    //   13: newarray int
    //   15: astore #5
    //   17: iconst_0
    //   18: istore #6
    //   20: aload #5
    //   22: astore #4
    //   24: iload #6
    //   26: iload_3
    //   27: if_icmpge -> 77
    //   30: aload_0
    //   31: iload #6
    //   33: invokevirtual getChildAt : (I)Landroid/view/View;
    //   36: astore #4
    //   38: aload #5
    //   40: iload #6
    //   42: aload #4
    //   44: invokevirtual getVisibility : ()I
    //   47: iastore
    //   48: aload #5
    //   50: iload #6
    //   52: iaload
    //   53: ifne -> 71
    //   56: aload #4
    //   58: aload #4
    //   60: getfield mViewFlags : I
    //   63: bipush #-13
    //   65: iand
    //   66: iconst_4
    //   67: ior
    //   68: putfield mViewFlags : I
    //   71: iinc #6, 1
    //   74: goto -> 20
    //   77: aload_0
    //   78: aload_1
    //   79: iload_2
    //   80: invokespecial createSnapshot : (Landroid/view/ViewDebug$CanvasProvider;Z)Landroid/graphics/Bitmap;
    //   83: astore #5
    //   85: iload_2
    //   86: ifeq -> 131
    //   89: iconst_0
    //   90: istore #6
    //   92: iload #6
    //   94: iload_3
    //   95: if_icmpge -> 131
    //   98: aload_0
    //   99: iload #6
    //   101: invokevirtual getChildAt : (I)Landroid/view/View;
    //   104: astore_1
    //   105: aload_1
    //   106: aload_1
    //   107: getfield mViewFlags : I
    //   110: bipush #-13
    //   112: iand
    //   113: aload #4
    //   115: iload #6
    //   117: iaload
    //   118: bipush #12
    //   120: iand
    //   121: ior
    //   122: putfield mViewFlags : I
    //   125: iinc #6, 1
    //   128: goto -> 92
    //   131: aload #5
    //   133: areturn
    //   134: astore_1
    //   135: iload_2
    //   136: ifeq -> 184
    //   139: iconst_0
    //   140: istore #6
    //   142: iload #6
    //   144: iload_3
    //   145: if_icmpge -> 184
    //   148: aload_0
    //   149: iload #6
    //   151: invokevirtual getChildAt : (I)Landroid/view/View;
    //   154: astore #5
    //   156: aload #5
    //   158: aload #5
    //   160: getfield mViewFlags : I
    //   163: bipush #-13
    //   165: iand
    //   166: aload #4
    //   168: iload #6
    //   170: iaload
    //   171: bipush #12
    //   173: iand
    //   174: ior
    //   175: putfield mViewFlags : I
    //   178: iinc #6, 1
    //   181: goto -> 142
    //   184: aload_1
    //   185: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4072	-> 0
    //   #4073	-> 5
    //   #4075	-> 8
    //   #4076	-> 12
    //   #4077	-> 17
    //   #4078	-> 30
    //   #4079	-> 38
    //   #4080	-> 48
    //   #4081	-> 56
    //   #4077	-> 71
    //   #4088	-> 77
    //   #4090	-> 85
    //   #4091	-> 89
    //   #4092	-> 98
    //   #4093	-> 105
    //   #4091	-> 125
    //   #4088	-> 131
    //   #4090	-> 134
    //   #4091	-> 139
    //   #4092	-> 148
    //   #4093	-> 156
    //   #4091	-> 178
    //   #4097	-> 184
    // Exception table:
    //   from	to	target	type
    //   77	85	134	finally
  }
  
  boolean isLayoutModeOptical() {
    int i = this.mLayoutMode;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  Insets computeOpticalInsets() {
    if (isLayoutModeOptical()) {
      int i = 0;
      int j = 0;
      int k = 0;
      int m = 0;
      for (byte b = 0; b < this.mChildrenCount; b++, i = n, j = i1, k = i2, m = i3) {
        View view = getChildAt(b);
        int n = i, i1 = j, i2 = k, i3 = m;
        if (view.getVisibility() == 0) {
          Insets insets = view.getOpticalInsets();
          n = Math.max(i, insets.left);
          i1 = Math.max(j, insets.top);
          i2 = Math.max(k, insets.right);
          i3 = Math.max(m, insets.bottom);
        } 
      } 
      return Insets.of(i, j, k, m);
    } 
    return Insets.NONE;
  }
  
  private static void fillRect(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt1 != paramInt3 && paramInt2 != paramInt4) {
      int i = paramInt1, j = paramInt3;
      if (paramInt1 > paramInt3) {
        j = paramInt1;
        i = paramInt3;
      } 
      paramInt3 = paramInt2;
      paramInt1 = paramInt4;
      if (paramInt2 > paramInt4) {
        paramInt3 = paramInt4;
        paramInt1 = paramInt2;
      } 
      paramCanvas.drawRect(i, paramInt3, j, paramInt1, paramPaint);
    } 
  }
  
  private static int sign(int paramInt) {
    if (paramInt >= 0) {
      paramInt = 1;
    } else {
      paramInt = -1;
    } 
    return paramInt;
  }
  
  private static void drawCorner(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    fillRect(paramCanvas, paramPaint, paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + sign(paramInt4) * paramInt5);
    fillRect(paramCanvas, paramPaint, paramInt1, paramInt2, paramInt1 + sign(paramInt3) * paramInt5, paramInt2 + paramInt4);
  }
  
  private static void drawRectCorners(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Paint paramPaint, int paramInt5, int paramInt6) {
    drawCorner(paramCanvas, paramPaint, paramInt1, paramInt2, paramInt5, paramInt5, paramInt6);
    drawCorner(paramCanvas, paramPaint, paramInt1, paramInt4, paramInt5, -paramInt5, paramInt6);
    drawCorner(paramCanvas, paramPaint, paramInt3, paramInt2, -paramInt5, paramInt5, paramInt6);
    drawCorner(paramCanvas, paramPaint, paramInt3, paramInt4, -paramInt5, -paramInt5, paramInt6);
  }
  
  private static void fillDifference(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Paint paramPaint) {
    paramInt5 = paramInt1 - paramInt5;
    paramInt7 = paramInt3 + paramInt7;
    fillRect(paramCanvas, paramPaint, paramInt5, paramInt2 - paramInt6, paramInt7, paramInt2);
    fillRect(paramCanvas, paramPaint, paramInt5, paramInt2, paramInt1, paramInt4);
    fillRect(paramCanvas, paramPaint, paramInt3, paramInt2, paramInt7, paramInt4);
    fillRect(paramCanvas, paramPaint, paramInt5, paramInt4, paramInt7, paramInt4 + paramInt8);
  }
  
  protected void onDebugDrawMargins(Canvas paramCanvas, Paint paramPaint) {
    for (byte b = 0; b < getChildCount(); b++) {
      View view = getChildAt(b);
      view.getLayoutParams().onDebugDraw(view, paramCanvas, paramPaint);
    } 
  }
  
  protected void onDebugDraw(Canvas paramCanvas) {
    Paint paint = getDebugPaint();
    paint.setColor(-65536);
    paint.setStyle(Paint.Style.STROKE);
    byte b;
    for (b = 0; b < getChildCount(); b++) {
      View view = getChildAt(b);
      if (view.getVisibility() != 8) {
        Insets insets = view.getOpticalInsets();
        int k = view.getLeft(), m = insets.left;
        int n = view.getTop(), i1 = insets.top;
        int i2 = view.getRight(), i3 = insets.right;
        int i4 = view.getBottom(), i5 = insets.bottom;
        drawRect(paramCanvas, paint, m + k, i1 + n, i2 - i3 - 1, i4 - i5 - 1);
      } 
    } 
    paint.setColor(Color.argb(63, 255, 0, 255));
    paint.setStyle(Paint.Style.FILL);
    onDebugDrawMargins(paramCanvas, paint);
    paint.setColor(DEBUG_CORNERS_COLOR);
    paint.setStyle(Paint.Style.FILL);
    int j = dipsToPixels(8);
    int i = dipsToPixels(1);
    for (b = 0; b < getChildCount(); b++) {
      View view = getChildAt(b);
      if (view.getVisibility() != 8)
        drawRectCorners(paramCanvas, view.getLeft(), view.getTop(), view.getRight(), view.getBottom(), paint, j, i); 
    } 
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_1
    //   1: aload_0
    //   2: getfield mRenderNode : Landroid/graphics/RenderNode;
    //   5: invokevirtual isRecordingFor : (Ljava/lang/Object;)Z
    //   8: istore_2
    //   9: aload_0
    //   10: getfield mChildrenCount : I
    //   13: istore_3
    //   14: aload_0
    //   15: getfield mChildren : [Landroid/view/View;
    //   18: astore #4
    //   20: aload_0
    //   21: getfield mGroupFlags : I
    //   24: istore #5
    //   26: iload #5
    //   28: bipush #8
    //   30: iand
    //   31: ifeq -> 186
    //   34: aload_0
    //   35: invokevirtual canAnimate : ()Z
    //   38: ifeq -> 186
    //   41: aload_0
    //   42: invokevirtual isHardwareAccelerated : ()Z
    //   45: pop
    //   46: iconst_0
    //   47: istore #6
    //   49: iload #6
    //   51: iload_3
    //   52: if_icmpge -> 108
    //   55: aload #4
    //   57: iload #6
    //   59: aaload
    //   60: astore #7
    //   62: aload #7
    //   64: ifnull -> 102
    //   67: aload #7
    //   69: getfield mViewFlags : I
    //   72: bipush #12
    //   74: iand
    //   75: ifne -> 102
    //   78: aload #7
    //   80: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   83: astore #8
    //   85: aload_0
    //   86: aload #7
    //   88: aload #8
    //   90: iload #6
    //   92: iload_3
    //   93: invokevirtual attachLayoutAnimationParameters : (Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;II)V
    //   96: aload_0
    //   97: aload #7
    //   99: invokespecial bindLayoutAnimation : (Landroid/view/View;)V
    //   102: iinc #6, 1
    //   105: goto -> 49
    //   108: aload_0
    //   109: getfield mLayoutAnimationController : Landroid/view/animation/LayoutAnimationController;
    //   112: astore #7
    //   114: aload #7
    //   116: invokevirtual willOverlap : ()Z
    //   119: ifeq -> 134
    //   122: aload_0
    //   123: aload_0
    //   124: getfield mGroupFlags : I
    //   127: sipush #128
    //   130: ior
    //   131: putfield mGroupFlags : I
    //   134: aload #7
    //   136: invokevirtual start : ()V
    //   139: aload_0
    //   140: getfield mGroupFlags : I
    //   143: bipush #-9
    //   145: iand
    //   146: istore #6
    //   148: aload_0
    //   149: iload #6
    //   151: putfield mGroupFlags : I
    //   154: aload_0
    //   155: iload #6
    //   157: bipush #-17
    //   159: iand
    //   160: putfield mGroupFlags : I
    //   163: aload_0
    //   164: getfield mAnimationListener : Landroid/view/animation/Animation$AnimationListener;
    //   167: astore #8
    //   169: aload #8
    //   171: ifnull -> 186
    //   174: aload #8
    //   176: aload #7
    //   178: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   181: invokeinterface onAnimationStart : (Landroid/view/animation/Animation;)V
    //   186: iconst_0
    //   187: istore #9
    //   189: iload #5
    //   191: bipush #34
    //   193: iand
    //   194: bipush #34
    //   196: if_icmpne -> 205
    //   199: iconst_1
    //   200: istore #10
    //   202: goto -> 208
    //   205: iconst_0
    //   206: istore #10
    //   208: iload #10
    //   210: ifeq -> 281
    //   213: aload_1
    //   214: iconst_2
    //   215: invokevirtual save : (I)I
    //   218: istore #9
    //   220: aload_1
    //   221: aload_0
    //   222: getfield mScrollX : I
    //   225: aload_0
    //   226: getfield mPaddingLeft : I
    //   229: iadd
    //   230: aload_0
    //   231: getfield mScrollY : I
    //   234: aload_0
    //   235: getfield mPaddingTop : I
    //   238: iadd
    //   239: aload_0
    //   240: getfield mScrollX : I
    //   243: aload_0
    //   244: getfield mRight : I
    //   247: iadd
    //   248: aload_0
    //   249: getfield mLeft : I
    //   252: isub
    //   253: aload_0
    //   254: getfield mPaddingRight : I
    //   257: isub
    //   258: aload_0
    //   259: getfield mScrollY : I
    //   262: aload_0
    //   263: getfield mBottom : I
    //   266: iadd
    //   267: aload_0
    //   268: getfield mTop : I
    //   271: isub
    //   272: aload_0
    //   273: getfield mPaddingBottom : I
    //   276: isub
    //   277: invokevirtual clipRect : (IIII)Z
    //   280: pop
    //   281: aload_0
    //   282: aload_0
    //   283: getfield mPrivateFlags : I
    //   286: bipush #-65
    //   288: iand
    //   289: putfield mPrivateFlags : I
    //   292: aload_0
    //   293: aload_0
    //   294: getfield mGroupFlags : I
    //   297: bipush #-5
    //   299: iand
    //   300: putfield mGroupFlags : I
    //   303: iconst_0
    //   304: istore #6
    //   306: aload_0
    //   307: invokevirtual getDrawingTime : ()J
    //   310: lstore #11
    //   312: iload_2
    //   313: ifeq -> 320
    //   316: aload_1
    //   317: invokevirtual insertReorderBarrier : ()V
    //   320: aload_0
    //   321: getfield mTransientIndices : Ljava/util/List;
    //   324: astore #7
    //   326: aload #7
    //   328: ifnonnull -> 337
    //   331: iconst_0
    //   332: istore #13
    //   334: goto -> 346
    //   337: aload #7
    //   339: invokeinterface size : ()I
    //   344: istore #13
    //   346: iload #13
    //   348: ifeq -> 357
    //   351: iconst_0
    //   352: istore #14
    //   354: goto -> 360
    //   357: iconst_m1
    //   358: istore #14
    //   360: iload_2
    //   361: ifeq -> 370
    //   364: aconst_null
    //   365: astore #7
    //   367: goto -> 376
    //   370: aload_0
    //   371: invokevirtual buildOrderedChildList : ()Ljava/util/ArrayList;
    //   374: astore #7
    //   376: aload #7
    //   378: ifnonnull -> 394
    //   381: aload_0
    //   382: invokevirtual isChildrenDrawingOrderEnabled : ()Z
    //   385: ifeq -> 394
    //   388: iconst_1
    //   389: istore #15
    //   391: goto -> 397
    //   394: iconst_0
    //   395: istore #15
    //   397: getstatic com/oplus/darkmode/IOplusDarkModeManager.DEFAULT : Lcom/oplus/darkmode/IOplusDarkModeManager;
    //   400: iconst_0
    //   401: anewarray java/lang/Object
    //   404: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   407: checkcast com/oplus/darkmode/IOplusDarkModeManager
    //   410: aload_0
    //   411: aload_1
    //   412: invokeinterface markDispatchDraw : (Landroid/view/ViewGroup;Landroid/graphics/Canvas;)V
    //   417: iconst_0
    //   418: istore #16
    //   420: iload #16
    //   422: iload_3
    //   423: if_icmpge -> 679
    //   426: iload #14
    //   428: istore #17
    //   430: iload #6
    //   432: istore #14
    //   434: iload #17
    //   436: iflt -> 545
    //   439: aload_0
    //   440: getfield mTransientIndices : Ljava/util/List;
    //   443: iload #17
    //   445: invokeinterface get : (I)Ljava/lang/Object;
    //   450: checkcast java/lang/Integer
    //   453: invokevirtual intValue : ()I
    //   456: iload #16
    //   458: if_icmpne -> 545
    //   461: aload_0
    //   462: getfield mTransientViews : Ljava/util/List;
    //   465: iload #17
    //   467: invokeinterface get : (I)Ljava/lang/Object;
    //   472: checkcast android/view/View
    //   475: astore #8
    //   477: aload #8
    //   479: getfield mViewFlags : I
    //   482: bipush #12
    //   484: iand
    //   485: ifeq -> 500
    //   488: iload #14
    //   490: istore #18
    //   492: aload #8
    //   494: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   497: ifnull -> 514
    //   500: aload_0
    //   501: aload_1
    //   502: aload #8
    //   504: lload #11
    //   506: invokevirtual drawChild : (Landroid/graphics/Canvas;Landroid/view/View;J)Z
    //   509: iload #14
    //   511: ior
    //   512: istore #18
    //   514: iload #17
    //   516: iconst_1
    //   517: iadd
    //   518: istore #14
    //   520: iload #14
    //   522: istore #6
    //   524: iload #14
    //   526: iload #13
    //   528: if_icmplt -> 534
    //   531: iconst_m1
    //   532: istore #6
    //   534: iload #18
    //   536: istore #14
    //   538: iload #6
    //   540: istore #17
    //   542: goto -> 434
    //   545: aload_0
    //   546: iload_3
    //   547: iload #16
    //   549: iload #15
    //   551: invokespecial getAndVerifyPreorderedIndex : (IIZ)I
    //   554: istore #6
    //   556: aload #7
    //   558: aload #4
    //   560: iload #6
    //   562: invokestatic getAndVerifyPreorderedView : (Ljava/util/ArrayList;[Landroid/view/View;I)Landroid/view/View;
    //   565: astore #19
    //   567: aload #19
    //   569: getfield mViewFlags : I
    //   572: bipush #12
    //   574: iand
    //   575: ifeq -> 596
    //   578: aload #19
    //   580: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   583: ifnull -> 589
    //   586: goto -> 596
    //   589: iload #14
    //   591: istore #6
    //   593: goto -> 669
    //   596: aload_1
    //   597: invokevirtual getClipChildRect : ()Landroid/graphics/Rect;
    //   600: astore #8
    //   602: aload #8
    //   604: ifnonnull -> 624
    //   607: iload #14
    //   609: aload_0
    //   610: aload_1
    //   611: aload #19
    //   613: lload #11
    //   615: invokevirtual drawChild : (Landroid/graphics/Canvas;Landroid/view/View;J)Z
    //   618: ior
    //   619: istore #6
    //   621: goto -> 669
    //   624: new android/graphics/Rect
    //   627: dup
    //   628: invokespecial <init> : ()V
    //   631: astore #20
    //   633: aload #19
    //   635: aload #20
    //   637: iconst_1
    //   638: invokevirtual getBoundsOnScreen : (Landroid/graphics/Rect;Z)V
    //   641: iload #14
    //   643: istore #6
    //   645: aload #8
    //   647: aload #20
    //   649: invokestatic intersects : (Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    //   652: ifeq -> 669
    //   655: iload #14
    //   657: aload_0
    //   658: aload_1
    //   659: aload #19
    //   661: lload #11
    //   663: invokevirtual drawChild : (Landroid/graphics/Canvas;Landroid/view/View;J)Z
    //   666: ior
    //   667: istore #6
    //   669: iinc #16, 1
    //   672: iload #17
    //   674: istore #14
    //   676: goto -> 420
    //   679: iload #6
    //   681: istore #5
    //   683: iload #5
    //   685: istore #6
    //   687: iload #14
    //   689: iflt -> 824
    //   692: aload_0
    //   693: getfield mTransientViews : Ljava/util/List;
    //   696: iload #14
    //   698: invokeinterface get : (I)Ljava/lang/Object;
    //   703: checkcast android/view/View
    //   706: astore #20
    //   708: aload #20
    //   710: getfield mViewFlags : I
    //   713: bipush #12
    //   715: iand
    //   716: ifeq -> 731
    //   719: iload #5
    //   721: istore #6
    //   723: aload #20
    //   725: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   728: ifnull -> 804
    //   731: aload_1
    //   732: invokevirtual getClipChildRect : ()Landroid/graphics/Rect;
    //   735: astore #4
    //   737: aload #4
    //   739: ifnonnull -> 759
    //   742: aload_0
    //   743: aload_1
    //   744: aload #20
    //   746: lload #11
    //   748: invokevirtual drawChild : (Landroid/graphics/Canvas;Landroid/view/View;J)Z
    //   751: iload #5
    //   753: ior
    //   754: istore #6
    //   756: goto -> 804
    //   759: new android/graphics/Rect
    //   762: dup
    //   763: invokespecial <init> : ()V
    //   766: astore #8
    //   768: aload #20
    //   770: aload #8
    //   772: iconst_1
    //   773: invokevirtual getBoundsOnScreen : (Landroid/graphics/Rect;Z)V
    //   776: iload #5
    //   778: istore #6
    //   780: aload #4
    //   782: aload #8
    //   784: invokestatic intersects : (Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    //   787: ifeq -> 804
    //   790: aload_0
    //   791: aload_1
    //   792: aload #20
    //   794: lload #11
    //   796: invokevirtual drawChild : (Landroid/graphics/Canvas;Landroid/view/View;J)Z
    //   799: iload #5
    //   801: ior
    //   802: istore #6
    //   804: iinc #14, 1
    //   807: iload #14
    //   809: iload #13
    //   811: if_icmplt -> 817
    //   814: goto -> 824
    //   817: iload #6
    //   819: istore #5
    //   821: goto -> 683
    //   824: aload #7
    //   826: ifnull -> 834
    //   829: aload #7
    //   831: invokevirtual clear : ()V
    //   834: iload #6
    //   836: istore #5
    //   838: aload_0
    //   839: getfield mDisappearingChildren : Ljava/util/ArrayList;
    //   842: ifnull -> 905
    //   845: aload_0
    //   846: getfield mDisappearingChildren : Ljava/util/ArrayList;
    //   849: astore #4
    //   851: aload #4
    //   853: invokevirtual size : ()I
    //   856: istore #5
    //   858: iload #5
    //   860: iconst_1
    //   861: isub
    //   862: istore #14
    //   864: iload #6
    //   866: istore #5
    //   868: iload #14
    //   870: iflt -> 905
    //   873: aload #4
    //   875: iload #14
    //   877: invokevirtual get : (I)Ljava/lang/Object;
    //   880: checkcast android/view/View
    //   883: astore #7
    //   885: iload #6
    //   887: aload_0
    //   888: aload_1
    //   889: aload #7
    //   891: lload #11
    //   893: invokevirtual drawChild : (Landroid/graphics/Canvas;Landroid/view/View;J)Z
    //   896: ior
    //   897: istore #6
    //   899: iinc #14, -1
    //   902: goto -> 864
    //   905: iload_2
    //   906: ifeq -> 913
    //   909: aload_1
    //   910: invokevirtual insertInorderBarrier : ()V
    //   913: aload_0
    //   914: invokevirtual isShowingLayoutBounds : ()Z
    //   917: ifeq -> 925
    //   920: aload_0
    //   921: aload_1
    //   922: invokevirtual onDebugDraw : (Landroid/graphics/Canvas;)V
    //   925: iload #10
    //   927: ifeq -> 936
    //   930: aload_1
    //   931: iload #9
    //   933: invokevirtual restoreToCount : (I)V
    //   936: aload_0
    //   937: getfield mGroupFlags : I
    //   940: istore #6
    //   942: iload #6
    //   944: iconst_4
    //   945: iand
    //   946: iconst_4
    //   947: if_icmpne -> 955
    //   950: aload_0
    //   951: iconst_1
    //   952: invokevirtual invalidate : (Z)V
    //   955: iload #6
    //   957: bipush #16
    //   959: iand
    //   960: ifne -> 1016
    //   963: iload #6
    //   965: sipush #512
    //   968: iand
    //   969: ifne -> 1016
    //   972: aload_0
    //   973: getfield mLayoutAnimationController : Landroid/view/animation/LayoutAnimationController;
    //   976: astore_1
    //   977: aload_1
    //   978: invokevirtual isDone : ()Z
    //   981: ifeq -> 1016
    //   984: iload #5
    //   986: ifne -> 1016
    //   989: aload_0
    //   990: aload_0
    //   991: getfield mGroupFlags : I
    //   994: sipush #512
    //   997: ior
    //   998: putfield mGroupFlags : I
    //   1001: new android/view/ViewGroup$2
    //   1004: dup
    //   1005: aload_0
    //   1006: invokespecial <init> : (Landroid/view/ViewGroup;)V
    //   1009: astore_1
    //   1010: aload_0
    //   1011: aload_1
    //   1012: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   1015: pop
    //   1016: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4234	-> 0
    //   #4235	-> 9
    //   #4236	-> 14
    //   #4237	-> 20
    //   #4239	-> 26
    //   #4240	-> 41
    //   #4241	-> 46
    //   #4242	-> 55
    //   #4248	-> 62
    //   #4250	-> 78
    //   #4251	-> 85
    //   #4252	-> 96
    //   #4241	-> 102
    //   #4256	-> 108
    //   #4257	-> 114
    //   #4258	-> 122
    //   #4261	-> 134
    //   #4263	-> 139
    //   #4264	-> 154
    //   #4266	-> 163
    //   #4267	-> 174
    //   #4271	-> 186
    //   #4272	-> 189
    //   #4273	-> 208
    //   #4274	-> 213
    //   #4275	-> 220
    //   #4281	-> 281
    //   #4282	-> 292
    //   #4284	-> 303
    //   #4285	-> 306
    //   #4287	-> 312
    //   #4288	-> 320
    //   #4289	-> 346
    //   #4292	-> 360
    //   #4293	-> 364
    //   #4294	-> 376
    //   #4295	-> 381
    //   #4298	-> 397
    //   #4300	-> 417
    //   #4301	-> 434
    //   #4302	-> 461
    //   #4303	-> 477
    //   #4304	-> 488
    //   #4305	-> 500
    //   #4307	-> 514
    //   #4308	-> 520
    //   #4309	-> 531
    //   #4311	-> 534
    //   #4301	-> 545
    //   #4313	-> 545
    //   #4314	-> 556
    //   #4315	-> 567
    //   #4321	-> 596
    //   #4322	-> 602
    //   #4323	-> 607
    //   #4325	-> 624
    //   #4326	-> 633
    //   #4327	-> 641
    //   #4328	-> 655
    //   #4300	-> 669
    //   #4334	-> 683
    //   #4336	-> 692
    //   #4337	-> 708
    //   #4338	-> 719
    //   #4344	-> 731
    //   #4345	-> 737
    //   #4346	-> 742
    //   #4348	-> 759
    //   #4349	-> 768
    //   #4350	-> 776
    //   #4351	-> 790
    //   #4356	-> 804
    //   #4357	-> 807
    //   #4358	-> 814
    //   #4360	-> 817
    //   #4361	-> 824
    //   #4364	-> 834
    //   #4365	-> 845
    //   #4366	-> 851
    //   #4368	-> 858
    //   #4369	-> 873
    //   #4370	-> 885
    //   #4368	-> 899
    //   #4373	-> 905
    //   #4375	-> 913
    //   #4376	-> 920
    //   #4379	-> 925
    //   #4380	-> 930
    //   #4384	-> 936
    //   #4386	-> 942
    //   #4387	-> 950
    //   #4390	-> 955
    //   #4391	-> 977
    //   #4395	-> 989
    //   #4396	-> 1001
    //   #4402	-> 1010
    //   #4404	-> 1016
  }
  
  public ViewGroupOverlay getOverlay() {
    if (this.mOverlay == null)
      this.mOverlay = new ViewGroupOverlay(this.mContext, this); 
    return (ViewGroupOverlay)this.mOverlay;
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2) {
    return paramInt2;
  }
  
  public final int getChildDrawingOrder(int paramInt) {
    return getChildDrawingOrder(getChildCount(), paramInt);
  }
  
  private boolean hasChildWithZ() {
    for (byte b = 0; b < this.mChildrenCount; b++) {
      if (this.mChildren[b].getZ() != 0.0F)
        return true; 
    } 
    return false;
  }
  
  ArrayList<View> buildOrderedChildList() {
    int i = this.mChildrenCount;
    if (i <= 1 || !hasChildWithZ())
      return null; 
    ArrayList<View> arrayList = this.mPreSortedChildren;
    if (arrayList == null) {
      this.mPreSortedChildren = new ArrayList<>(i);
    } else {
      arrayList.clear();
      this.mPreSortedChildren.ensureCapacity(i);
    } 
    boolean bool = isChildrenDrawingOrderEnabled();
    for (byte b = 0; b < i; b++) {
      int j = getAndVerifyPreorderedIndex(i, b, bool);
      View view = this.mChildren[j];
      float f = view.getZ();
      j = b;
      while (j > 0 && ((View)this.mPreSortedChildren.get(j - 1)).getZ() > f)
        j--; 
      this.mPreSortedChildren.add(j, view);
    } 
    return this.mPreSortedChildren;
  }
  
  private void notifyAnimationListener() {
    int i = this.mGroupFlags & 0xFFFFFDFF;
    this.mGroupFlags = i | 0x10;
    if (this.mAnimationListener != null) {
      Object object = new Object(this);
      post((Runnable)object);
    } 
    invalidate(true);
  }
  
  protected void dispatchGetDisplayList() {
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    int j;
    for (j = 0; j < i; j++) {
      View view = arrayOfView[j];
      if ((view.mViewFlags & 0xC) == 0 || view.getAnimation() != null)
        recreateChildDisplayList(view); 
    } 
    if (this.mTransientViews == null) {
      j = 0;
    } else {
      j = this.mTransientIndices.size();
    } 
    for (i = 0; i < j; i++) {
      View view = this.mTransientViews.get(i);
      if ((view.mViewFlags & 0xC) == 0 || view.getAnimation() != null)
        recreateChildDisplayList(view); 
    } 
    if (this.mOverlay != null) {
      ViewGroup viewGroup = this.mOverlay.getOverlayView();
      recreateChildDisplayList(viewGroup);
    } 
    if (this.mDisappearingChildren != null) {
      ArrayList<View> arrayList = this.mDisappearingChildren;
      i = arrayList.size();
      for (j = 0; j < i; j++) {
        View view = arrayList.get(j);
        recreateChildDisplayList(view);
      } 
    } 
  }
  
  private void recreateChildDisplayList(View paramView) {
    boolean bool;
    if ((paramView.mPrivateFlags & Integer.MIN_VALUE) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    paramView.mRecreateDisplayList = bool;
    paramView.mPrivateFlags &= Integer.MAX_VALUE;
    paramView.updateDisplayListIfDirty();
    paramView.mRecreateDisplayList = false;
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong) {
    ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).markDrawChild(this, paramView, paramCanvas);
    return paramView.draw(paramCanvas, this, paramLong);
  }
  
  void getScrollIndicatorBounds(Rect paramRect) {
    boolean bool;
    super.getScrollIndicatorBounds(paramRect);
    if ((this.mGroupFlags & 0x22) == 34) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      paramRect.left += this.mPaddingLeft;
      paramRect.right -= this.mPaddingRight;
      paramRect.top += this.mPaddingTop;
      paramRect.bottom -= this.mPaddingBottom;
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public boolean getClipChildren() {
    int i = this.mGroupFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public void setClipChildren(boolean paramBoolean) {
    boolean bool;
    if ((this.mGroupFlags & 0x1) == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean != bool) {
      setBooleanFlag(1, paramBoolean);
      for (byte b = 0; b < this.mChildrenCount; b++) {
        View view = getChildAt(b);
        if (view.mRenderNode != null)
          view.mRenderNode.setClipToBounds(paramBoolean); 
      } 
      invalidate(true);
    } 
  }
  
  public void setClipToPadding(boolean paramBoolean) {
    if (hasBooleanFlag(2) != paramBoolean) {
      setBooleanFlag(2, paramBoolean);
      invalidate(true);
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public boolean getClipToPadding() {
    return hasBooleanFlag(2);
  }
  
  public void dispatchSetSelected(boolean paramBoolean) {
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].setSelected(paramBoolean); 
  }
  
  public void dispatchSetActivated(boolean paramBoolean) {
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].setActivated(paramBoolean); 
  }
  
  protected void dispatchSetPressed(boolean paramBoolean) {
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if (!paramBoolean || (!view.isClickable() && !view.isLongClickable()))
        view.setPressed(paramBoolean); 
    } 
  }
  
  public void dispatchDrawableHotspotChanged(float paramFloat1, float paramFloat2) {
    int i = this.mChildrenCount;
    if (i == 0)
      return; 
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      boolean bool1, bool2;
      View view = arrayOfView[b];
      if (!view.isClickable() && !view.isLongClickable()) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if ((view.mViewFlags & 0x400000) != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool1 || bool2) {
        float[] arrayOfFloat = getTempLocationF();
        arrayOfFloat[0] = paramFloat1;
        arrayOfFloat[1] = paramFloat2;
        transformPointToViewLocal(arrayOfFloat, view);
        view.drawableHotspotChanged(arrayOfFloat[0], arrayOfFloat[1]);
      } 
    } 
  }
  
  void dispatchCancelPendingInputEvents() {
    super.dispatchCancelPendingInputEvents();
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].dispatchCancelPendingInputEvents(); 
  }
  
  protected void setStaticTransformationsEnabled(boolean paramBoolean) {
    setBooleanFlag(2048, paramBoolean);
  }
  
  protected boolean getChildStaticTransformation(View paramView, Transformation paramTransformation) {
    return false;
  }
  
  Transformation getChildTransformation() {
    if (this.mChildTransformation == null)
      this.mChildTransformation = new Transformation(); 
    return this.mChildTransformation;
  }
  
  protected <T extends View> T findViewTraversal(int paramInt) {
    if (paramInt == this.mID)
      return (T)this; 
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if ((view.mPrivateFlags & 0x8) == 0) {
        view = view.findViewById(paramInt);
        if (view != null)
          return (T)view; 
      } 
    } 
    return null;
  }
  
  protected <T extends View> T findViewWithTagTraversal(Object paramObject) {
    if (paramObject != null && paramObject.equals(this.mTag))
      return (T)this; 
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if ((view.mPrivateFlags & 0x8) == 0) {
        view = view.findViewWithTag(paramObject);
        if (view != null)
          return (T)view; 
      } 
    } 
    return null;
  }
  
  protected <T extends View> T findViewByPredicateTraversal(Predicate<View> paramPredicate, View paramView) {
    if (paramPredicate.test(this))
      return (T)this; 
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if (view != paramView && (view.mPrivateFlags & 0x8) == 0) {
        view = view.findViewByPredicate(paramPredicate);
        if (view != null)
          return (T)view; 
      } 
    } 
    return null;
  }
  
  public void addTransientView(View paramView, int paramInt) {
    if (paramInt < 0 || paramView == null)
      return; 
    if (paramView.mParent == null) {
      if (this.mTransientIndices == null) {
        this.mTransientIndices = new ArrayList<>();
        this.mTransientViews = new ArrayList<>();
      } 
      int i = this.mTransientIndices.size();
      if (i > 0) {
        byte b;
        for (b = 0; b < i && paramInt >= ((Integer)this.mTransientIndices.get(b)).intValue(); b++);
        this.mTransientIndices.add(b, Integer.valueOf(paramInt));
        this.mTransientViews.add(b, paramView);
      } else {
        this.mTransientIndices.add(Integer.valueOf(paramInt));
        this.mTransientViews.add(paramView);
      } 
      paramView.mParent = this;
      if (this.mAttachInfo != null)
        paramView.dispatchAttachedToWindow(this.mAttachInfo, this.mViewFlags & 0xC); 
      invalidate(true);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("The specified view already has a parent ");
    stringBuilder.append(paramView.mParent);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void removeTransientView(View paramView) {
    List<View> list = this.mTransientViews;
    if (list == null)
      return; 
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      if (paramView == this.mTransientViews.get(b)) {
        this.mTransientViews.remove(b);
        this.mTransientIndices.remove(b);
        paramView.mParent = null;
        if (paramView.mAttachInfo != null)
          paramView.dispatchDetachedFromWindow(); 
        invalidate(true);
        return;
      } 
    } 
  }
  
  public int getTransientViewCount() {
    int i;
    List<Integer> list = this.mTransientIndices;
    if (list == null) {
      i = 0;
    } else {
      i = list.size();
    } 
    return i;
  }
  
  public int getTransientViewIndex(int paramInt) {
    if (paramInt >= 0) {
      List<Integer> list = this.mTransientIndices;
      if (list != null && paramInt < list.size())
        return ((Integer)this.mTransientIndices.get(paramInt)).intValue(); 
    } 
    return -1;
  }
  
  public View getTransientView(int paramInt) {
    List<View> list = this.mTransientViews;
    if (list == null || paramInt >= list.size())
      return null; 
    return this.mTransientViews.get(paramInt);
  }
  
  public void addView(View paramView) {
    addView(paramView, -1);
  }
  
  public void addView(View paramView, int paramInt) {
    if (paramView != null) {
      LayoutParams layoutParams1 = paramView.getLayoutParams();
      LayoutParams layoutParams2 = layoutParams1;
      if (layoutParams1 == null) {
        layoutParams2 = generateDefaultLayoutParams();
        if (layoutParams2 == null)
          throw new IllegalArgumentException("generateDefaultLayoutParams() cannot return null"); 
      } 
      addView(paramView, paramInt, layoutParams2);
      return;
    } 
    throw new IllegalArgumentException("Cannot add a null child view to a ViewGroup");
  }
  
  public void addView(View paramView, int paramInt1, int paramInt2) {
    LayoutParams layoutParams = generateDefaultLayoutParams();
    layoutParams.width = paramInt1;
    layoutParams.height = paramInt2;
    addView(paramView, -1, layoutParams);
  }
  
  public void addView(View paramView, LayoutParams paramLayoutParams) {
    addView(paramView, -1, paramLayoutParams);
  }
  
  public void addView(View paramView, int paramInt, LayoutParams paramLayoutParams) {
    if (paramView != null) {
      requestLayout();
      invalidate(true);
      addViewInner(paramView, paramInt, paramLayoutParams, false);
      return;
    } 
    throw new IllegalArgumentException("Cannot add a null child view to a ViewGroup");
  }
  
  public void updateViewLayout(View paramView, LayoutParams paramLayoutParams) {
    if (checkLayoutParams(paramLayoutParams)) {
      if (paramView.mParent == this) {
        paramView.setLayoutParams(paramLayoutParams);
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Given view not a child of ");
      stringBuilder1.append(this);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid LayoutParams supplied to ");
    stringBuilder.append(this);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  protected boolean checkLayoutParams(LayoutParams paramLayoutParams) {
    boolean bool;
    if (paramLayoutParams != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setOnHierarchyChangeListener(OnHierarchyChangeListener paramOnHierarchyChangeListener) {
    this.mOnHierarchyChangeListener = paramOnHierarchyChangeListener;
  }
  
  void dispatchViewAdded(View paramView) {
    onViewAdded(paramView);
    OnHierarchyChangeListener onHierarchyChangeListener = this.mOnHierarchyChangeListener;
    if (onHierarchyChangeListener != null)
      onHierarchyChangeListener.onChildViewAdded(this, paramView); 
  }
  
  public void onViewAdded(View paramView) {}
  
  void dispatchViewRemoved(View paramView) {
    onViewRemoved(paramView);
    OnHierarchyChangeListener onHierarchyChangeListener = this.mOnHierarchyChangeListener;
    if (onHierarchyChangeListener != null)
      onHierarchyChangeListener.onChildViewRemoved(this, paramView); 
  }
  
  public void onViewRemoved(View paramView) {}
  
  private void clearCachedLayoutMode() {
    if (!hasBooleanFlag(8388608))
      this.mLayoutMode = -1; 
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    clearCachedLayoutMode();
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    clearCachedLayoutMode();
  }
  
  protected void destroyHardwareResources() {
    super.destroyHardwareResources();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view != null)
        view.destroyHardwareResources(); 
    } 
  }
  
  protected boolean addViewInLayout(View paramView, int paramInt, LayoutParams paramLayoutParams) {
    return addViewInLayout(paramView, paramInt, paramLayoutParams, false);
  }
  
  protected boolean addViewInLayout(View paramView, int paramInt, LayoutParams paramLayoutParams, boolean paramBoolean) {
    if (paramView != null) {
      paramView.mParent = null;
      addViewInner(paramView, paramInt, paramLayoutParams, paramBoolean);
      paramView.mPrivateFlags = paramView.mPrivateFlags & 0xFFDFFFFF | 0x20;
      return true;
    } 
    throw new IllegalArgumentException("Cannot add a null child view to a ViewGroup");
  }
  
  protected void cleanupLayoutState(View paramView) {
    paramView.mPrivateFlags &= 0xFFFFEFFF;
  }
  
  private void addViewInner(View paramView, int paramInt, LayoutParams paramLayoutParams, boolean paramBoolean) {
    LayoutTransition layoutTransition = this.mTransition;
    if (layoutTransition != null)
      layoutTransition.cancel(3); 
    if (paramView.getParent() == null) {
      layoutTransition = this.mTransition;
      if (layoutTransition != null)
        layoutTransition.addChild(this, paramView); 
      LayoutParams layoutParams = paramLayoutParams;
      if (!checkLayoutParams(paramLayoutParams))
        layoutParams = generateLayoutParams(paramLayoutParams); 
      if (paramBoolean) {
        paramView.mLayoutParams = layoutParams;
      } else {
        paramView.setLayoutParams(layoutParams);
      } 
      int i = paramInt;
      if (paramInt < 0)
        i = this.mChildrenCount; 
      addInArray(paramView, i);
      if (paramBoolean) {
        paramView.assignParent(this);
      } else {
        paramView.mParent = this;
      } 
      if (paramView.hasUnhandledKeyListener())
        incrementChildUnhandledKeyListeners(); 
      paramBoolean = paramView.hasFocus();
      if (paramBoolean)
        requestChildFocus(paramView, paramView.findFocus()); 
      View.AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null && (this.mGroupFlags & 0x400000) == 0) {
        paramBoolean = attachInfo.mKeepScreenOn;
        attachInfo.mKeepScreenOn = false;
        paramView.dispatchAttachedToWindow(this.mAttachInfo, this.mViewFlags & 0xC);
        if (attachInfo.mKeepScreenOn)
          needGlobalAttributesUpdate(true); 
        attachInfo.mKeepScreenOn = paramBoolean;
      } 
      if (paramView.isLayoutDirectionInherited())
        paramView.resetRtlProperties(); 
      dispatchViewAdded(paramView);
      if ((paramView.mViewFlags & 0x400000) == 4194304)
        this.mGroupFlags |= 0x10000; 
      if (paramView.hasTransientState())
        childHasTransientStateChanged(paramView, true); 
      if (paramView.getVisibility() != 8)
        notifySubtreeAccessibilityStateChangedIfNeeded(); 
      List<Integer> list = this.mTransientIndices;
      if (list != null) {
        int j = list.size();
        for (paramInt = 0; paramInt < j; paramInt++) {
          int k = ((Integer)this.mTransientIndices.get(paramInt)).intValue();
          if (i <= k)
            this.mTransientIndices.set(paramInt, Integer.valueOf(k + 1)); 
        } 
      } 
      if (this.mCurrentDragStartEvent != null && paramView.getVisibility() == 0)
        notifyChildOfDragStart(paramView); 
      if (paramView.hasDefaultFocus())
        setDefaultFocus(paramView); 
      touchAccessibilityNodeProviderIfNeeded(paramView);
      return;
    } 
    throw new IllegalStateException("The specified child already has a parent. You must call removeView() on the child's parent first.");
  }
  
  private void touchAccessibilityNodeProviderIfNeeded(View paramView) {
    if (this.mContext.isAutofillCompatibilityEnabled())
      paramView.getAccessibilityNodeProvider(); 
  }
  
  private void addInArray(View paramView, int paramInt) {
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    int j = arrayOfView.length;
    if (paramInt == i) {
      View[] arrayOfView1 = arrayOfView;
      if (j == i) {
        this.mChildren = arrayOfView1 = new View[j + 12];
        System.arraycopy(arrayOfView, 0, arrayOfView1, 0, j);
        arrayOfView1 = this.mChildren;
      } 
      paramInt = this.mChildrenCount;
      this.mChildrenCount = paramInt + 1;
      arrayOfView1[paramInt] = paramView;
    } else {
      if (paramInt < i) {
        if (j == i) {
          View[] arrayOfView1 = new View[j + 12];
          System.arraycopy(arrayOfView, 0, arrayOfView1, 0, paramInt);
          System.arraycopy(arrayOfView, paramInt, this.mChildren, paramInt + 1, i - paramInt);
          arrayOfView = this.mChildren;
        } else {
          System.arraycopy(arrayOfView, paramInt, arrayOfView, paramInt + 1, i - paramInt);
        } 
        arrayOfView[paramInt] = paramView;
        this.mChildrenCount++;
        i = this.mLastTouchDownIndex;
        if (i >= paramInt)
          this.mLastTouchDownIndex = i + 1; 
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("index=");
      stringBuilder.append(paramInt);
      stringBuilder.append(" count=");
      stringBuilder.append(i);
      throw new IndexOutOfBoundsException(stringBuilder.toString());
    } 
  }
  
  private void removeFromArray(int paramInt) {
    View[] arrayOfView = this.mChildren;
    ArrayList<View> arrayList = this.mTransitioningViews;
    if (arrayList == null || !arrayList.contains(arrayOfView[paramInt]))
      (arrayOfView[paramInt]).mParent = null; 
    int i = this.mChildrenCount;
    if (paramInt == i - 1) {
      this.mChildrenCount = i = this.mChildrenCount - 1;
      arrayOfView[i] = null;
    } else if (paramInt >= 0 && paramInt < i) {
      System.arraycopy(arrayOfView, paramInt + 1, arrayOfView, paramInt, i - paramInt - 1);
      this.mChildrenCount = i = this.mChildrenCount - 1;
      arrayOfView[i] = null;
    } else {
      throw new IndexOutOfBoundsException();
    } 
    i = this.mLastTouchDownIndex;
    if (i == paramInt) {
      this.mLastTouchDownTime = 0L;
      this.mLastTouchDownIndex = -1;
    } else if (i > paramInt) {
      this.mLastTouchDownIndex = i - 1;
    } 
  }
  
  private void removeFromArray(int paramInt1, int paramInt2) {
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    paramInt1 = Math.max(0, paramInt1);
    int j = Math.min(i, paramInt1 + paramInt2);
    if (paramInt1 == j)
      return; 
    if (j == i) {
      for (paramInt2 = paramInt1; paramInt2 < j; paramInt2++) {
        (arrayOfView[paramInt2]).mParent = null;
        arrayOfView[paramInt2] = null;
      } 
    } else {
      for (paramInt2 = paramInt1; paramInt2 < j; paramInt2++)
        (arrayOfView[paramInt2]).mParent = null; 
      System.arraycopy(arrayOfView, j, arrayOfView, paramInt1, i - j);
      for (paramInt2 = i - j - paramInt1; paramInt2 < i; paramInt2++)
        arrayOfView[paramInt2] = null; 
    } 
    this.mChildrenCount -= j - paramInt1;
  }
  
  private void bindLayoutAnimation(View paramView) {
    Animation animation = this.mLayoutAnimationController.getAnimationForView(paramView);
    paramView.setAnimation(animation);
  }
  
  protected void attachLayoutAnimationParameters(View paramView, LayoutParams paramLayoutParams, int paramInt1, int paramInt2) {
    LayoutAnimationController.AnimationParameters animationParameters2 = paramLayoutParams.layoutAnimationParameters;
    LayoutAnimationController.AnimationParameters animationParameters1 = animationParameters2;
    if (animationParameters2 == null) {
      animationParameters1 = new LayoutAnimationController.AnimationParameters();
      paramLayoutParams.layoutAnimationParameters = animationParameters1;
    } 
    animationParameters1.count = paramInt2;
    animationParameters1.index = paramInt1;
  }
  
  public void removeView(View paramView) {
    if (removeViewInternal(paramView)) {
      requestLayout();
      invalidate(true);
    } 
  }
  
  public void removeViewInLayout(View paramView) {
    removeViewInternal(paramView);
  }
  
  public void removeViewsInLayout(int paramInt1, int paramInt2) {
    removeViewsInternal(paramInt1, paramInt2);
  }
  
  public void removeViewAt(int paramInt) {
    removeViewInternal(paramInt, getChildAt(paramInt));
    requestLayout();
    invalidate(true);
  }
  
  public void removeViews(int paramInt1, int paramInt2) {
    removeViewsInternal(paramInt1, paramInt2);
    requestLayout();
    invalidate(true);
  }
  
  private boolean removeViewInternal(View paramView) {
    int i = indexOfChild(paramView);
    if (i >= 0) {
      removeViewInternal(i, paramView);
      return true;
    } 
    return false;
  }
  
  private void removeViewInternal(int paramInt, View paramView) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTransition : Landroid/animation/LayoutTransition;
    //   4: astore_3
    //   5: aload_3
    //   6: ifnull -> 15
    //   9: aload_3
    //   10: aload_0
    //   11: aload_2
    //   12: invokevirtual removeChild : (Landroid/view/ViewGroup;Landroid/view/View;)V
    //   15: iconst_0
    //   16: istore #4
    //   18: aload_2
    //   19: aload_0
    //   20: getfield mFocused : Landroid/view/View;
    //   23: if_acmpne -> 34
    //   26: aload_2
    //   27: aconst_null
    //   28: invokevirtual unFocus : (Landroid/view/View;)V
    //   31: iconst_1
    //   32: istore #4
    //   34: aload_2
    //   35: aload_0
    //   36: getfield mFocusedInCluster : Landroid/view/View;
    //   39: if_acmpne -> 47
    //   42: aload_0
    //   43: aload_2
    //   44: invokevirtual clearFocusedInCluster : (Landroid/view/View;)V
    //   47: aload_2
    //   48: invokevirtual clearAccessibilityFocus : ()V
    //   51: aload_0
    //   52: aload_2
    //   53: invokespecial cancelTouchTarget : (Landroid/view/View;)V
    //   56: aload_0
    //   57: aload_2
    //   58: invokespecial cancelHoverTarget : (Landroid/view/View;)V
    //   61: aload_2
    //   62: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   65: ifnonnull -> 102
    //   68: aload_0
    //   69: getfield mTransitioningViews : Ljava/util/ArrayList;
    //   72: astore_3
    //   73: aload_3
    //   74: ifnull -> 88
    //   77: aload_3
    //   78: aload_2
    //   79: invokevirtual contains : (Ljava/lang/Object;)Z
    //   82: ifeq -> 88
    //   85: goto -> 102
    //   88: aload_2
    //   89: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   92: ifnull -> 107
    //   95: aload_2
    //   96: invokevirtual dispatchDetachedFromWindow : ()V
    //   99: goto -> 107
    //   102: aload_0
    //   103: aload_2
    //   104: invokespecial addDisappearingView : (Landroid/view/View;)V
    //   107: aload_2
    //   108: invokevirtual hasTransientState : ()Z
    //   111: istore #5
    //   113: iconst_0
    //   114: istore #6
    //   116: iload #5
    //   118: ifeq -> 127
    //   121: aload_0
    //   122: aload_2
    //   123: iconst_0
    //   124: invokevirtual childHasTransientStateChanged : (Landroid/view/View;Z)V
    //   127: aload_0
    //   128: iconst_0
    //   129: invokevirtual needGlobalAttributesUpdate : (Z)V
    //   132: aload_0
    //   133: iload_1
    //   134: invokespecial removeFromArray : (I)V
    //   137: aload_2
    //   138: invokevirtual hasUnhandledKeyListener : ()Z
    //   141: ifeq -> 148
    //   144: aload_0
    //   145: invokevirtual decrementChildUnhandledKeyListeners : ()V
    //   148: aload_2
    //   149: aload_0
    //   150: getfield mDefaultFocus : Landroid/view/View;
    //   153: if_acmpne -> 161
    //   156: aload_0
    //   157: aload_2
    //   158: invokevirtual clearDefaultFocus : (Landroid/view/View;)V
    //   161: iload #4
    //   163: ifeq -> 183
    //   166: aload_0
    //   167: aload_2
    //   168: invokevirtual clearChildFocus : (Landroid/view/View;)V
    //   171: aload_0
    //   172: invokevirtual rootViewRequestFocus : ()Z
    //   175: ifne -> 183
    //   178: aload_0
    //   179: aload_0
    //   180: invokevirtual notifyGlobalFocusCleared : (Landroid/view/View;)V
    //   183: aload_0
    //   184: aload_2
    //   185: invokevirtual dispatchViewRemoved : (Landroid/view/View;)V
    //   188: aload_2
    //   189: invokevirtual getVisibility : ()I
    //   192: bipush #8
    //   194: if_icmpeq -> 201
    //   197: aload_0
    //   198: invokevirtual notifySubtreeAccessibilityStateChangedIfNeeded : ()V
    //   201: aload_0
    //   202: getfield mTransientIndices : Ljava/util/List;
    //   205: astore_3
    //   206: aload_3
    //   207: ifnonnull -> 217
    //   210: iload #6
    //   212: istore #4
    //   214: goto -> 225
    //   217: aload_3
    //   218: invokeinterface size : ()I
    //   223: istore #4
    //   225: iconst_0
    //   226: istore #6
    //   228: iload #6
    //   230: iload #4
    //   232: if_icmpge -> 285
    //   235: aload_0
    //   236: getfield mTransientIndices : Ljava/util/List;
    //   239: iload #6
    //   241: invokeinterface get : (I)Ljava/lang/Object;
    //   246: checkcast java/lang/Integer
    //   249: invokevirtual intValue : ()I
    //   252: istore #7
    //   254: iload_1
    //   255: iload #7
    //   257: if_icmpge -> 279
    //   260: aload_0
    //   261: getfield mTransientIndices : Ljava/util/List;
    //   264: iload #6
    //   266: iload #7
    //   268: iconst_1
    //   269: isub
    //   270: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   273: invokeinterface set : (ILjava/lang/Object;)Ljava/lang/Object;
    //   278: pop
    //   279: iinc #6, 1
    //   282: goto -> 228
    //   285: aload_0
    //   286: getfield mCurrentDragStartEvent : Landroid/view/DragEvent;
    //   289: ifnull -> 301
    //   292: aload_0
    //   293: getfield mChildrenInterestedInDrag : Ljava/util/HashSet;
    //   296: aload_2
    //   297: invokevirtual remove : (Ljava/lang/Object;)Z
    //   300: pop
    //   301: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5619	-> 0
    //   #5620	-> 9
    //   #5623	-> 15
    //   #5624	-> 18
    //   #5625	-> 26
    //   #5626	-> 31
    //   #5628	-> 34
    //   #5629	-> 42
    //   #5632	-> 47
    //   #5634	-> 51
    //   #5635	-> 56
    //   #5637	-> 61
    //   #5638	-> 77
    //   #5640	-> 88
    //   #5641	-> 95
    //   #5639	-> 102
    //   #5644	-> 107
    //   #5645	-> 121
    //   #5648	-> 127
    //   #5650	-> 132
    //   #5652	-> 137
    //   #5653	-> 144
    //   #5656	-> 148
    //   #5657	-> 156
    //   #5659	-> 161
    //   #5660	-> 166
    //   #5661	-> 171
    //   #5662	-> 178
    //   #5666	-> 183
    //   #5668	-> 188
    //   #5669	-> 197
    //   #5672	-> 201
    //   #5673	-> 225
    //   #5674	-> 235
    //   #5675	-> 254
    //   #5676	-> 260
    //   #5673	-> 279
    //   #5680	-> 285
    //   #5681	-> 292
    //   #5683	-> 301
  }
  
  public void setLayoutTransition(LayoutTransition paramLayoutTransition) {
    if (this.mTransition != null) {
      LayoutTransition layoutTransition = this.mTransition;
      layoutTransition.cancel();
      layoutTransition.removeTransitionListener(this.mLayoutTransitionListener);
    } 
    this.mTransition = paramLayoutTransition;
    if (paramLayoutTransition != null)
      paramLayoutTransition.addTransitionListener(this.mLayoutTransitionListener); 
  }
  
  public LayoutTransition getLayoutTransition() {
    return this.mTransition;
  }
  
  private void removeViewsInternal(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: iload_1
    //   1: iload_2
    //   2: iadd
    //   3: istore_3
    //   4: iload_1
    //   5: iflt -> 275
    //   8: iload_2
    //   9: iflt -> 275
    //   12: iload_3
    //   13: aload_0
    //   14: getfield mChildrenCount : I
    //   17: if_icmpgt -> 275
    //   20: aload_0
    //   21: getfield mFocused : Landroid/view/View;
    //   24: astore #4
    //   26: aload_0
    //   27: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   30: ifnull -> 39
    //   33: iconst_1
    //   34: istore #5
    //   36: goto -> 42
    //   39: iconst_0
    //   40: istore #5
    //   42: iconst_0
    //   43: istore #6
    //   45: aconst_null
    //   46: astore #7
    //   48: aload_0
    //   49: getfield mChildren : [Landroid/view/View;
    //   52: astore #8
    //   54: iload_1
    //   55: istore #9
    //   57: iload #9
    //   59: iload_3
    //   60: if_icmpge -> 233
    //   63: aload #8
    //   65: iload #9
    //   67: aaload
    //   68: astore #10
    //   70: aload_0
    //   71: getfield mTransition : Landroid/animation/LayoutTransition;
    //   74: astore #11
    //   76: aload #11
    //   78: ifnull -> 89
    //   81: aload #11
    //   83: aload_0
    //   84: aload #10
    //   86: invokevirtual removeChild : (Landroid/view/ViewGroup;Landroid/view/View;)V
    //   89: aload #10
    //   91: aload #4
    //   93: if_acmpne -> 105
    //   96: aload #10
    //   98: aconst_null
    //   99: invokevirtual unFocus : (Landroid/view/View;)V
    //   102: iconst_1
    //   103: istore #6
    //   105: aload #10
    //   107: aload_0
    //   108: getfield mDefaultFocus : Landroid/view/View;
    //   111: if_acmpne -> 118
    //   114: aload #10
    //   116: astore #7
    //   118: aload #10
    //   120: aload_0
    //   121: getfield mFocusedInCluster : Landroid/view/View;
    //   124: if_acmpne -> 133
    //   127: aload_0
    //   128: aload #10
    //   130: invokevirtual clearFocusedInCluster : (Landroid/view/View;)V
    //   133: aload #10
    //   135: invokevirtual clearAccessibilityFocus : ()V
    //   138: aload_0
    //   139: aload #10
    //   141: invokespecial cancelTouchTarget : (Landroid/view/View;)V
    //   144: aload_0
    //   145: aload #10
    //   147: invokespecial cancelHoverTarget : (Landroid/view/View;)V
    //   150: aload #10
    //   152: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   155: ifnonnull -> 195
    //   158: aload_0
    //   159: getfield mTransitioningViews : Ljava/util/ArrayList;
    //   162: astore #11
    //   164: aload #11
    //   166: ifnull -> 182
    //   169: aload #11
    //   171: aload #10
    //   173: invokevirtual contains : (Ljava/lang/Object;)Z
    //   176: ifeq -> 182
    //   179: goto -> 195
    //   182: iload #5
    //   184: ifeq -> 201
    //   187: aload #10
    //   189: invokevirtual dispatchDetachedFromWindow : ()V
    //   192: goto -> 201
    //   195: aload_0
    //   196: aload #10
    //   198: invokespecial addDisappearingView : (Landroid/view/View;)V
    //   201: aload #10
    //   203: invokevirtual hasTransientState : ()Z
    //   206: ifeq -> 216
    //   209: aload_0
    //   210: aload #10
    //   212: iconst_0
    //   213: invokevirtual childHasTransientStateChanged : (Landroid/view/View;Z)V
    //   216: aload_0
    //   217: iconst_0
    //   218: invokevirtual needGlobalAttributesUpdate : (Z)V
    //   221: aload_0
    //   222: aload #10
    //   224: invokevirtual dispatchViewRemoved : (Landroid/view/View;)V
    //   227: iinc #9, 1
    //   230: goto -> 57
    //   233: aload_0
    //   234: iload_1
    //   235: iload_2
    //   236: invokespecial removeFromArray : (II)V
    //   239: aload #7
    //   241: ifnull -> 250
    //   244: aload_0
    //   245: aload #7
    //   247: invokevirtual clearDefaultFocus : (Landroid/view/View;)V
    //   250: iload #6
    //   252: ifeq -> 274
    //   255: aload_0
    //   256: aload #4
    //   258: invokevirtual clearChildFocus : (Landroid/view/View;)V
    //   261: aload_0
    //   262: invokevirtual rootViewRequestFocus : ()Z
    //   265: ifne -> 274
    //   268: aload_0
    //   269: aload #4
    //   271: invokevirtual notifyGlobalFocusCleared : (Landroid/view/View;)V
    //   274: return
    //   275: new java/lang/IndexOutOfBoundsException
    //   278: dup
    //   279: invokespecial <init> : ()V
    //   282: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5725	-> 0
    //   #5727	-> 4
    //   #5731	-> 20
    //   #5732	-> 26
    //   #5733	-> 42
    //   #5734	-> 45
    //   #5736	-> 48
    //   #5738	-> 54
    //   #5739	-> 63
    //   #5741	-> 70
    //   #5742	-> 81
    //   #5745	-> 89
    //   #5746	-> 96
    //   #5747	-> 102
    //   #5749	-> 105
    //   #5750	-> 114
    //   #5752	-> 118
    //   #5753	-> 127
    //   #5756	-> 133
    //   #5758	-> 138
    //   #5759	-> 144
    //   #5761	-> 150
    //   #5762	-> 169
    //   #5764	-> 182
    //   #5765	-> 187
    //   #5763	-> 195
    //   #5768	-> 201
    //   #5769	-> 209
    //   #5772	-> 216
    //   #5774	-> 221
    //   #5738	-> 227
    //   #5777	-> 233
    //   #5779	-> 239
    //   #5780	-> 244
    //   #5782	-> 250
    //   #5783	-> 255
    //   #5784	-> 261
    //   #5785	-> 268
    //   #5788	-> 274
    //   #5728	-> 275
  }
  
  public void removeAllViews() {
    removeAllViewsInLayout();
    requestLayout();
    invalidate(true);
  }
  
  public void removeAllViewsInLayout() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mChildrenCount : I
    //   4: istore_1
    //   5: iload_1
    //   6: ifgt -> 10
    //   9: return
    //   10: aload_0
    //   11: getfield mChildren : [Landroid/view/View;
    //   14: astore_2
    //   15: aload_0
    //   16: iconst_0
    //   17: putfield mChildrenCount : I
    //   20: aload_0
    //   21: getfield mFocused : Landroid/view/View;
    //   24: astore_3
    //   25: aload_0
    //   26: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   29: ifnull -> 38
    //   32: iconst_1
    //   33: istore #4
    //   35: goto -> 41
    //   38: iconst_0
    //   39: istore #4
    //   41: iconst_0
    //   42: istore #5
    //   44: aload_0
    //   45: iconst_0
    //   46: invokevirtual needGlobalAttributesUpdate : (Z)V
    //   49: iinc #1, -1
    //   52: iload_1
    //   53: iflt -> 200
    //   56: aload_2
    //   57: iload_1
    //   58: aaload
    //   59: astore #6
    //   61: aload_0
    //   62: getfield mTransition : Landroid/animation/LayoutTransition;
    //   65: astore #7
    //   67: aload #7
    //   69: ifnull -> 80
    //   72: aload #7
    //   74: aload_0
    //   75: aload #6
    //   77: invokevirtual removeChild : (Landroid/view/ViewGroup;Landroid/view/View;)V
    //   80: aload #6
    //   82: aload_3
    //   83: if_acmpne -> 95
    //   86: aload #6
    //   88: aconst_null
    //   89: invokevirtual unFocus : (Landroid/view/View;)V
    //   92: iconst_1
    //   93: istore #5
    //   95: aload #6
    //   97: invokevirtual clearAccessibilityFocus : ()V
    //   100: aload_0
    //   101: aload #6
    //   103: invokespecial cancelTouchTarget : (Landroid/view/View;)V
    //   106: aload_0
    //   107: aload #6
    //   109: invokespecial cancelHoverTarget : (Landroid/view/View;)V
    //   112: aload #6
    //   114: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   117: ifnonnull -> 157
    //   120: aload_0
    //   121: getfield mTransitioningViews : Ljava/util/ArrayList;
    //   124: astore #7
    //   126: aload #7
    //   128: ifnull -> 144
    //   131: aload #7
    //   133: aload #6
    //   135: invokevirtual contains : (Ljava/lang/Object;)Z
    //   138: ifeq -> 144
    //   141: goto -> 157
    //   144: iload #4
    //   146: ifeq -> 163
    //   149: aload #6
    //   151: invokevirtual dispatchDetachedFromWindow : ()V
    //   154: goto -> 163
    //   157: aload_0
    //   158: aload #6
    //   160: invokespecial addDisappearingView : (Landroid/view/View;)V
    //   163: aload #6
    //   165: invokevirtual hasTransientState : ()Z
    //   168: ifeq -> 178
    //   171: aload_0
    //   172: aload #6
    //   174: iconst_0
    //   175: invokevirtual childHasTransientStateChanged : (Landroid/view/View;Z)V
    //   178: aload_0
    //   179: aload #6
    //   181: invokevirtual dispatchViewRemoved : (Landroid/view/View;)V
    //   184: aload #6
    //   186: aconst_null
    //   187: putfield mParent : Landroid/view/ViewParent;
    //   190: aload_2
    //   191: iload_1
    //   192: aconst_null
    //   193: aastore
    //   194: iinc #1, -1
    //   197: goto -> 52
    //   200: aload_0
    //   201: getfield mDefaultFocus : Landroid/view/View;
    //   204: astore_2
    //   205: aload_2
    //   206: ifnull -> 214
    //   209: aload_0
    //   210: aload_2
    //   211: invokevirtual clearDefaultFocus : (Landroid/view/View;)V
    //   214: aload_0
    //   215: getfield mFocusedInCluster : Landroid/view/View;
    //   218: astore_2
    //   219: aload_2
    //   220: ifnull -> 228
    //   223: aload_0
    //   224: aload_2
    //   225: invokevirtual clearFocusedInCluster : (Landroid/view/View;)V
    //   228: iload #5
    //   230: ifeq -> 250
    //   233: aload_0
    //   234: aload_3
    //   235: invokevirtual clearChildFocus : (Landroid/view/View;)V
    //   238: aload_0
    //   239: invokevirtual rootViewRequestFocus : ()Z
    //   242: ifne -> 250
    //   245: aload_0
    //   246: aload_3
    //   247: invokevirtual notifyGlobalFocusCleared : (Landroid/view/View;)V
    //   250: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5818	-> 0
    //   #5819	-> 5
    //   #5820	-> 9
    //   #5823	-> 10
    //   #5824	-> 15
    //   #5826	-> 20
    //   #5827	-> 25
    //   #5828	-> 41
    //   #5830	-> 44
    //   #5832	-> 49
    //   #5833	-> 56
    //   #5835	-> 61
    //   #5836	-> 72
    //   #5839	-> 80
    //   #5840	-> 86
    //   #5841	-> 92
    //   #5844	-> 95
    //   #5846	-> 100
    //   #5847	-> 106
    //   #5849	-> 112
    //   #5850	-> 131
    //   #5852	-> 144
    //   #5853	-> 149
    //   #5851	-> 157
    //   #5856	-> 163
    //   #5857	-> 171
    //   #5860	-> 178
    //   #5862	-> 184
    //   #5863	-> 190
    //   #5832	-> 194
    //   #5866	-> 200
    //   #5867	-> 209
    //   #5869	-> 214
    //   #5870	-> 223
    //   #5872	-> 228
    //   #5873	-> 233
    //   #5874	-> 238
    //   #5875	-> 245
    //   #5878	-> 250
  }
  
  protected void removeDetachedView(View paramView, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTransition : Landroid/animation/LayoutTransition;
    //   4: astore_3
    //   5: aload_3
    //   6: ifnull -> 15
    //   9: aload_3
    //   10: aload_0
    //   11: aload_1
    //   12: invokevirtual removeChild : (Landroid/view/ViewGroup;Landroid/view/View;)V
    //   15: aload_1
    //   16: aload_0
    //   17: getfield mFocused : Landroid/view/View;
    //   20: if_acmpne -> 27
    //   23: aload_1
    //   24: invokevirtual clearFocus : ()V
    //   27: aload_1
    //   28: aload_0
    //   29: getfield mDefaultFocus : Landroid/view/View;
    //   32: if_acmpne -> 40
    //   35: aload_0
    //   36: aload_1
    //   37: invokevirtual clearDefaultFocus : (Landroid/view/View;)V
    //   40: aload_1
    //   41: aload_0
    //   42: getfield mFocusedInCluster : Landroid/view/View;
    //   45: if_acmpne -> 53
    //   48: aload_0
    //   49: aload_1
    //   50: invokevirtual clearFocusedInCluster : (Landroid/view/View;)V
    //   53: aload_1
    //   54: invokevirtual clearAccessibilityFocus : ()V
    //   57: aload_0
    //   58: aload_1
    //   59: invokespecial cancelTouchTarget : (Landroid/view/View;)V
    //   62: aload_0
    //   63: aload_1
    //   64: invokespecial cancelHoverTarget : (Landroid/view/View;)V
    //   67: iload_2
    //   68: ifeq -> 78
    //   71: aload_1
    //   72: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   75: ifnonnull -> 95
    //   78: aload_0
    //   79: getfield mTransitioningViews : Ljava/util/ArrayList;
    //   82: astore_3
    //   83: aload_3
    //   84: ifnull -> 103
    //   87: aload_3
    //   88: aload_1
    //   89: invokevirtual contains : (Ljava/lang/Object;)Z
    //   92: ifeq -> 103
    //   95: aload_0
    //   96: aload_1
    //   97: invokespecial addDisappearingView : (Landroid/view/View;)V
    //   100: goto -> 114
    //   103: aload_1
    //   104: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   107: ifnull -> 114
    //   110: aload_1
    //   111: invokevirtual dispatchDetachedFromWindow : ()V
    //   114: aload_1
    //   115: invokevirtual hasTransientState : ()Z
    //   118: ifeq -> 127
    //   121: aload_0
    //   122: aload_1
    //   123: iconst_0
    //   124: invokevirtual childHasTransientStateChanged : (Landroid/view/View;Z)V
    //   127: aload_0
    //   128: aload_1
    //   129: invokevirtual dispatchViewRemoved : (Landroid/view/View;)V
    //   132: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5901	-> 0
    //   #5902	-> 9
    //   #5905	-> 15
    //   #5906	-> 23
    //   #5908	-> 27
    //   #5909	-> 35
    //   #5911	-> 40
    //   #5912	-> 48
    //   #5915	-> 53
    //   #5917	-> 57
    //   #5918	-> 62
    //   #5920	-> 67
    //   #5921	-> 87
    //   #5922	-> 95
    //   #5923	-> 103
    //   #5924	-> 110
    //   #5927	-> 114
    //   #5928	-> 121
    //   #5931	-> 127
    //   #5932	-> 132
  }
  
  protected void attachViewToParent(View paramView, int paramInt, LayoutParams paramLayoutParams) {
    boolean bool;
    paramView.mLayoutParams = paramLayoutParams;
    int i = paramInt;
    if (paramInt < 0)
      i = this.mChildrenCount; 
    addInArray(paramView, i);
    paramView.mParent = this;
    paramView.mPrivateFlags = paramView.mPrivateFlags & 0xFFDFFFFF & 0xFFFF7FFF | 0x20 | Integer.MIN_VALUE;
    this.mPrivateFlags |= Integer.MIN_VALUE;
    if (paramView.hasFocus())
      requestChildFocus(paramView, paramView.findFocus()); 
    if (isAttachedToWindow() && getWindowVisibility() == 0 && isShown()) {
      bool = true;
    } else {
      bool = false;
    } 
    dispatchVisibilityAggregated(bool);
    notifySubtreeAccessibilityStateChangedIfNeeded();
  }
  
  protected void detachViewFromParent(View paramView) {
    removeFromArray(indexOfChild(paramView));
  }
  
  protected void detachViewFromParent(int paramInt) {
    removeFromArray(paramInt);
  }
  
  protected void detachViewsFromParent(int paramInt1, int paramInt2) {
    removeFromArray(paramInt1, paramInt2);
  }
  
  protected void detachAllViewsFromParent() {
    int i = this.mChildrenCount;
    if (i <= 0)
      return; 
    View[] arrayOfView = this.mChildren;
    this.mChildrenCount = 0;
    for (; --i >= 0; i--) {
      (arrayOfView[i]).mParent = null;
      arrayOfView[i] = null;
    } 
  }
  
  public void onDescendantInvalidated(View paramView1, View paramView2) {
    this.mPrivateFlags |= paramView2.mPrivateFlags & 0x40;
    if ((paramView2.mPrivateFlags & 0xFFDFFFFF) != 0) {
      this.mPrivateFlags = this.mPrivateFlags & 0xFFDFFFFF | 0x200000;
      this.mPrivateFlags &= 0xFFFF7FFF;
    } 
    paramView1 = paramView2;
    if (this.mLayerType == 1) {
      this.mPrivateFlags |= 0x80200000;
      paramView1 = this;
    } 
    if (this.mParent != null)
      this.mParent.onDescendantInvalidated(this, paramView1); 
  }
  
  @Deprecated
  public final void invalidateChild(View paramView, Rect paramRect) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   4: astore_3
    //   5: aload_3
    //   6: ifnull -> 30
    //   9: aload_3
    //   10: getfield mHardwareAccelerated : Z
    //   13: ifeq -> 30
    //   16: aload_0
    //   17: getfield mIgnoreOnDescendantInvalidated : Z
    //   20: ifne -> 30
    //   23: aload_0
    //   24: aload_1
    //   25: aload_1
    //   26: invokevirtual onDescendantInvalidated : (Landroid/view/View;Landroid/view/View;)V
    //   29: return
    //   30: aload_0
    //   31: astore #4
    //   33: aload_3
    //   34: ifnull -> 541
    //   37: aload_1
    //   38: getfield mPrivateFlags : I
    //   41: bipush #64
    //   43: iand
    //   44: ifeq -> 53
    //   47: iconst_1
    //   48: istore #5
    //   50: goto -> 56
    //   53: iconst_0
    //   54: istore #5
    //   56: aload_1
    //   57: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   60: astore #6
    //   62: aload_1
    //   63: getfield mLayerType : I
    //   66: ifeq -> 93
    //   69: aload_0
    //   70: aload_0
    //   71: getfield mPrivateFlags : I
    //   74: ldc_w -2147483648
    //   77: ior
    //   78: putfield mPrivateFlags : I
    //   81: aload_0
    //   82: aload_0
    //   83: getfield mPrivateFlags : I
    //   86: ldc_w -32769
    //   89: iand
    //   90: putfield mPrivateFlags : I
    //   93: aload_3
    //   94: getfield mInvalidateChildLocation : [I
    //   97: astore #7
    //   99: aload #7
    //   101: iconst_0
    //   102: aload_1
    //   103: getfield mLeft : I
    //   106: iastore
    //   107: aload #7
    //   109: iconst_1
    //   110: aload_1
    //   111: getfield mTop : I
    //   114: iastore
    //   115: aload #6
    //   117: invokevirtual isIdentity : ()Z
    //   120: ifeq -> 142
    //   123: aload #4
    //   125: astore #8
    //   127: aload #6
    //   129: astore #9
    //   131: aload_0
    //   132: getfield mGroupFlags : I
    //   135: sipush #2048
    //   138: iand
    //   139: ifeq -> 322
    //   142: aload_3
    //   143: getfield mTmpTransformRect : Landroid/graphics/RectF;
    //   146: astore #8
    //   148: aload #8
    //   150: aload_2
    //   151: invokevirtual set : (Landroid/graphics/Rect;)V
    //   154: aload_0
    //   155: getfield mGroupFlags : I
    //   158: sipush #2048
    //   161: iand
    //   162: ifeq -> 232
    //   165: aload_3
    //   166: getfield mTmpTransformation : Landroid/view/animation/Transformation;
    //   169: astore #10
    //   171: aload_0
    //   172: aload_1
    //   173: aload #10
    //   175: invokevirtual getChildStaticTransformation : (Landroid/view/View;Landroid/view/animation/Transformation;)Z
    //   178: istore #11
    //   180: iload #11
    //   182: ifeq -> 226
    //   185: aload_3
    //   186: getfield mTmpMatrix : Landroid/graphics/Matrix;
    //   189: astore #9
    //   191: aload #9
    //   193: aload #10
    //   195: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   198: invokevirtual set : (Landroid/graphics/Matrix;)V
    //   201: aload #9
    //   203: astore_1
    //   204: aload #6
    //   206: invokevirtual isIdentity : ()Z
    //   209: ifne -> 229
    //   212: aload #9
    //   214: aload #6
    //   216: invokevirtual preConcat : (Landroid/graphics/Matrix;)Z
    //   219: pop
    //   220: aload #9
    //   222: astore_1
    //   223: goto -> 229
    //   226: aload #6
    //   228: astore_1
    //   229: goto -> 235
    //   232: aload #6
    //   234: astore_1
    //   235: aload_1
    //   236: aload #8
    //   238: invokevirtual mapRect : (Landroid/graphics/RectF;)Z
    //   241: pop
    //   242: aload #8
    //   244: getfield left : F
    //   247: f2d
    //   248: invokestatic floor : (D)D
    //   251: d2i
    //   252: istore #12
    //   254: aload #8
    //   256: getfield top : F
    //   259: f2d
    //   260: dstore #13
    //   262: dload #13
    //   264: invokestatic floor : (D)D
    //   267: d2i
    //   268: istore #15
    //   270: aload #8
    //   272: getfield right : F
    //   275: f2d
    //   276: dstore #13
    //   278: dload #13
    //   280: invokestatic ceil : (D)D
    //   283: d2i
    //   284: istore #16
    //   286: aload #8
    //   288: getfield bottom : F
    //   291: f2d
    //   292: dstore #13
    //   294: dload #13
    //   296: invokestatic ceil : (D)D
    //   299: d2i
    //   300: istore #17
    //   302: aload_2
    //   303: iload #12
    //   305: iload #15
    //   307: iload #16
    //   309: iload #17
    //   311: invokevirtual set : (IIII)V
    //   314: aload #6
    //   316: astore #9
    //   318: aload #4
    //   320: astore #8
    //   322: aconst_null
    //   323: astore_1
    //   324: aload #8
    //   326: instanceof android/view/View
    //   329: ifeq -> 338
    //   332: aload #8
    //   334: checkcast android/view/View
    //   337: astore_1
    //   338: iload #5
    //   340: ifeq -> 378
    //   343: aload_1
    //   344: ifnull -> 361
    //   347: aload_1
    //   348: aload_1
    //   349: getfield mPrivateFlags : I
    //   352: bipush #64
    //   354: ior
    //   355: putfield mPrivateFlags : I
    //   358: goto -> 378
    //   361: aload #8
    //   363: instanceof android/view/ViewRootImpl
    //   366: ifeq -> 378
    //   369: aload #8
    //   371: checkcast android/view/ViewRootImpl
    //   374: iconst_1
    //   375: putfield mIsAnimating : Z
    //   378: aload_1
    //   379: ifnull -> 409
    //   382: aload_1
    //   383: getfield mPrivateFlags : I
    //   386: ldc 2097152
    //   388: iand
    //   389: ldc 2097152
    //   391: if_icmpeq -> 409
    //   394: aload_1
    //   395: aload_1
    //   396: getfield mPrivateFlags : I
    //   399: ldc_w -2097153
    //   402: iand
    //   403: ldc 2097152
    //   405: ior
    //   406: putfield mPrivateFlags : I
    //   409: aload #8
    //   411: aload #7
    //   413: aload_2
    //   414: invokeinterface invalidateChildInParent : ([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    //   419: astore #8
    //   421: aload_1
    //   422: ifnull -> 530
    //   425: aload_1
    //   426: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   429: astore #6
    //   431: aload #6
    //   433: invokevirtual isIdentity : ()Z
    //   436: ifne -> 527
    //   439: aload_3
    //   440: getfield mTmpTransformRect : Landroid/graphics/RectF;
    //   443: astore_1
    //   444: aload_1
    //   445: aload_2
    //   446: invokevirtual set : (Landroid/graphics/Rect;)V
    //   449: aload #6
    //   451: aload_1
    //   452: invokevirtual mapRect : (Landroid/graphics/RectF;)Z
    //   455: pop
    //   456: aload_1
    //   457: getfield left : F
    //   460: f2d
    //   461: invokestatic floor : (D)D
    //   464: d2i
    //   465: istore #16
    //   467: aload_1
    //   468: getfield top : F
    //   471: f2d
    //   472: dstore #13
    //   474: dload #13
    //   476: invokestatic floor : (D)D
    //   479: d2i
    //   480: istore #12
    //   482: aload_1
    //   483: getfield right : F
    //   486: f2d
    //   487: dstore #13
    //   489: dload #13
    //   491: invokestatic ceil : (D)D
    //   494: d2i
    //   495: istore #15
    //   497: aload_1
    //   498: getfield bottom : F
    //   501: f2d
    //   502: dstore #13
    //   504: dload #13
    //   506: invokestatic ceil : (D)D
    //   509: d2i
    //   510: istore #17
    //   512: aload_2
    //   513: iload #16
    //   515: iload #12
    //   517: iload #15
    //   519: iload #17
    //   521: invokevirtual set : (IIII)V
    //   524: goto -> 530
    //   527: goto -> 530
    //   530: aload #8
    //   532: ifnonnull -> 538
    //   535: goto -> 541
    //   538: goto -> 322
    //   541: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #6120	-> 0
    //   #6125	-> 5
    //   #6128	-> 23
    //   #6129	-> 29
    //   #6132	-> 30
    //   #6133	-> 33
    //   #6137	-> 37
    //   #6142	-> 56
    //   #6146	-> 62
    //   #6147	-> 69
    //   #6148	-> 81
    //   #6151	-> 93
    //   #6152	-> 99
    //   #6153	-> 107
    //   #6154	-> 115
    //   #6156	-> 142
    //   #6157	-> 148
    //   #6159	-> 154
    //   #6160	-> 165
    //   #6161	-> 171
    //   #6162	-> 180
    //   #6163	-> 185
    //   #6164	-> 191
    //   #6165	-> 201
    //   #6166	-> 212
    //   #6169	-> 226
    //   #6171	-> 229
    //   #6172	-> 232
    //   #6174	-> 235
    //   #6175	-> 242
    //   #6176	-> 262
    //   #6177	-> 278
    //   #6178	-> 294
    //   #6175	-> 302
    //   #6182	-> 322
    //   #6183	-> 324
    //   #6184	-> 332
    //   #6187	-> 338
    //   #6188	-> 343
    //   #6189	-> 347
    //   #6190	-> 361
    //   #6191	-> 369
    //   #6197	-> 378
    //   #6198	-> 382
    //   #6199	-> 394
    //   #6203	-> 409
    //   #6204	-> 421
    //   #6206	-> 425
    //   #6207	-> 431
    //   #6208	-> 439
    //   #6209	-> 444
    //   #6210	-> 449
    //   #6211	-> 456
    //   #6212	-> 474
    //   #6213	-> 489
    //   #6214	-> 504
    //   #6211	-> 512
    //   #6207	-> 527
    //   #6204	-> 530
    //   #6217	-> 530
    //   #6219	-> 541
  }
  
  @Deprecated
  public ViewParent invalidateChildInParent(int[] paramArrayOfint, Rect paramRect) {
    if ((this.mPrivateFlags & 0x8020) != 0) {
      int i = this.mGroupFlags;
      if ((i & 0x90) != 128) {
        paramRect.offset(paramArrayOfint[0] - this.mScrollX, paramArrayOfint[1] - this.mScrollY);
        if ((this.mGroupFlags & 0x1) == 0)
          paramRect.union(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop); 
        i = this.mLeft;
        int j = this.mTop;
        if ((this.mGroupFlags & 0x1) == 1 && !paramRect.intersect(0, 0, this.mRight - i, this.mBottom - j))
          paramRect.setEmpty(); 
        paramArrayOfint[0] = i;
        paramArrayOfint[1] = j;
      } else {
        if ((i & 0x1) == 1) {
          paramRect.set(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop);
        } else {
          paramRect.union(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop);
        } 
        paramArrayOfint[0] = this.mLeft;
        paramArrayOfint[1] = this.mTop;
        this.mPrivateFlags &= 0xFFFFFFDF;
      } 
      this.mPrivateFlags &= 0xFFFF7FFF;
      if (this.mLayerType != 0)
        this.mPrivateFlags |= Integer.MIN_VALUE; 
      return this.mParent;
    } 
    return null;
  }
  
  public final void offsetDescendantRectToMyCoords(View paramView, Rect paramRect) {
    offsetRectBetweenParentAndChild(paramView, paramRect, true, false);
  }
  
  public final void offsetRectIntoDescendantCoords(View paramView, Rect paramRect) {
    offsetRectBetweenParentAndChild(paramView, paramRect, false, false);
  }
  
  void offsetRectBetweenParentAndChild(View paramView, Rect paramRect, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramView == this)
      return; 
    ViewParent viewParent2 = paramView.mParent;
    View view = paramView;
    ViewParent viewParent1 = viewParent2;
    while (viewParent1 != null && viewParent1 instanceof View && viewParent1 != this) {
      if (paramBoolean1) {
        paramRect.offset(view.mLeft - view.mScrollX, view.mTop - view.mScrollY);
        if (paramBoolean2) {
          view = (View)viewParent1;
          boolean bool = paramRect.intersect(0, 0, view.mRight - view.mLeft, view.mBottom - view.mTop);
          if (!bool)
            paramRect.setEmpty(); 
        } 
      } else {
        if (paramBoolean2) {
          View view1 = (View)viewParent1;
          boolean bool = paramRect.intersect(0, 0, view1.mRight - view1.mLeft, view1.mBottom - view1.mTop);
          if (!bool)
            paramRect.setEmpty(); 
        } 
        paramRect.offset(view.mScrollX - view.mLeft, view.mScrollY - view.mTop);
      } 
      view = (View)viewParent1;
      viewParent1 = view.mParent;
    } 
    if (viewParent1 == this) {
      if (paramBoolean1) {
        paramRect.offset(view.mLeft - view.mScrollX, view.mTop - view.mScrollY);
      } else {
        paramRect.offset(view.mScrollX - view.mLeft, view.mScrollY - view.mTop);
      } 
      return;
    } 
    throw new IllegalArgumentException("parameter must be a descendant of this view");
  }
  
  public void offsetChildrenTopAndBottom(int paramInt) {
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    boolean bool = false;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      view.mTop += paramInt;
      view.mBottom += paramInt;
      if (view.mRenderNode != null) {
        bool = true;
        view.mRenderNode.offsetTopAndBottom(paramInt);
      } 
    } 
    if (bool)
      invalidateViewProperty(false, false); 
    notifySubtreeAccessibilityStateChangedIfNeeded();
  }
  
  public boolean getChildVisibleRect(View paramView, Rect paramRect, Point paramPoint) {
    return getChildVisibleRect(paramView, paramRect, paramPoint, false);
  }
  
  public boolean getChildVisibleRect(View paramView, Rect paramRect, Point paramPoint, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   4: ifnull -> 19
    //   7: aload_0
    //   8: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   11: getfield mTmpTransformRect : Landroid/graphics/RectF;
    //   14: astore #5
    //   16: goto -> 28
    //   19: new android/graphics/RectF
    //   22: dup
    //   23: invokespecial <init> : ()V
    //   26: astore #5
    //   28: aload #5
    //   30: aload_2
    //   31: invokevirtual set : (Landroid/graphics/Rect;)V
    //   34: aload_1
    //   35: invokevirtual hasIdentityMatrix : ()Z
    //   38: ifne -> 51
    //   41: aload_1
    //   42: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   45: aload #5
    //   47: invokevirtual mapRect : (Landroid/graphics/RectF;)Z
    //   50: pop
    //   51: aload_1
    //   52: getfield mLeft : I
    //   55: aload_0
    //   56: getfield mScrollX : I
    //   59: isub
    //   60: istore #6
    //   62: aload_1
    //   63: getfield mTop : I
    //   66: aload_0
    //   67: getfield mScrollY : I
    //   70: isub
    //   71: istore #7
    //   73: aload #5
    //   75: iload #6
    //   77: i2f
    //   78: iload #7
    //   80: i2f
    //   81: invokevirtual offset : (FF)V
    //   84: aload_3
    //   85: ifnull -> 190
    //   88: aload_1
    //   89: invokevirtual hasIdentityMatrix : ()Z
    //   92: ifne -> 168
    //   95: aload_0
    //   96: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   99: ifnull -> 114
    //   102: aload_0
    //   103: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   106: getfield mTmpTransformLocation : [F
    //   109: astore #8
    //   111: goto -> 119
    //   114: iconst_2
    //   115: newarray float
    //   117: astore #8
    //   119: aload #8
    //   121: iconst_0
    //   122: aload_3
    //   123: getfield x : I
    //   126: i2f
    //   127: fastore
    //   128: aload #8
    //   130: iconst_1
    //   131: aload_3
    //   132: getfield y : I
    //   135: i2f
    //   136: fastore
    //   137: aload_1
    //   138: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   141: aload #8
    //   143: invokevirtual mapPoints : ([F)V
    //   146: aload_3
    //   147: aload #8
    //   149: iconst_0
    //   150: faload
    //   151: invokestatic round : (F)I
    //   154: putfield x : I
    //   157: aload_3
    //   158: aload #8
    //   160: iconst_1
    //   161: faload
    //   162: invokestatic round : (F)I
    //   165: putfield y : I
    //   168: aload_3
    //   169: aload_3
    //   170: getfield x : I
    //   173: iload #6
    //   175: iadd
    //   176: putfield x : I
    //   179: aload_3
    //   180: aload_3
    //   181: getfield y : I
    //   184: iload #7
    //   186: iadd
    //   187: putfield y : I
    //   190: aload_0
    //   191: getfield mRight : I
    //   194: aload_0
    //   195: getfield mLeft : I
    //   198: isub
    //   199: istore #7
    //   201: aload_0
    //   202: getfield mBottom : I
    //   205: aload_0
    //   206: getfield mTop : I
    //   209: isub
    //   210: istore #6
    //   212: iconst_1
    //   213: istore #9
    //   215: aload_0
    //   216: getfield mParent : Landroid/view/ViewParent;
    //   219: ifnull -> 255
    //   222: iload #9
    //   224: istore #10
    //   226: aload_0
    //   227: getfield mParent : Landroid/view/ViewParent;
    //   230: instanceof android/view/ViewGroup
    //   233: ifeq -> 270
    //   236: aload_0
    //   237: getfield mParent : Landroid/view/ViewParent;
    //   240: checkcast android/view/ViewGroup
    //   243: astore_1
    //   244: iload #9
    //   246: istore #10
    //   248: aload_1
    //   249: invokevirtual getClipChildren : ()Z
    //   252: ifeq -> 270
    //   255: aload #5
    //   257: fconst_0
    //   258: fconst_0
    //   259: iload #7
    //   261: i2f
    //   262: iload #6
    //   264: i2f
    //   265: invokevirtual intersect : (FFFF)Z
    //   268: istore #10
    //   270: iload #4
    //   272: ifne -> 284
    //   275: iload #10
    //   277: istore #9
    //   279: iload #10
    //   281: ifeq -> 333
    //   284: iload #10
    //   286: istore #9
    //   288: aload_0
    //   289: getfield mGroupFlags : I
    //   292: bipush #34
    //   294: iand
    //   295: bipush #34
    //   297: if_icmpne -> 333
    //   300: aload #5
    //   302: aload_0
    //   303: getfield mPaddingLeft : I
    //   306: i2f
    //   307: aload_0
    //   308: getfield mPaddingTop : I
    //   311: i2f
    //   312: iload #7
    //   314: aload_0
    //   315: getfield mPaddingRight : I
    //   318: isub
    //   319: i2f
    //   320: iload #6
    //   322: aload_0
    //   323: getfield mPaddingBottom : I
    //   326: isub
    //   327: i2f
    //   328: invokevirtual intersect : (FFFF)Z
    //   331: istore #9
    //   333: iload #4
    //   335: ifne -> 347
    //   338: iload #9
    //   340: istore #10
    //   342: iload #9
    //   344: ifeq -> 397
    //   347: iload #9
    //   349: istore #10
    //   351: aload_0
    //   352: getfield mClipBounds : Landroid/graphics/Rect;
    //   355: ifnull -> 397
    //   358: aload #5
    //   360: aload_0
    //   361: getfield mClipBounds : Landroid/graphics/Rect;
    //   364: getfield left : I
    //   367: i2f
    //   368: aload_0
    //   369: getfield mClipBounds : Landroid/graphics/Rect;
    //   372: getfield top : I
    //   375: i2f
    //   376: aload_0
    //   377: getfield mClipBounds : Landroid/graphics/Rect;
    //   380: getfield right : I
    //   383: i2f
    //   384: aload_0
    //   385: getfield mClipBounds : Landroid/graphics/Rect;
    //   388: getfield bottom : I
    //   391: i2f
    //   392: invokevirtual intersect : (FFFF)Z
    //   395: istore #10
    //   397: aload #5
    //   399: getfield left : F
    //   402: f2d
    //   403: invokestatic floor : (D)D
    //   406: d2i
    //   407: istore #11
    //   409: aload #5
    //   411: getfield top : F
    //   414: f2d
    //   415: invokestatic floor : (D)D
    //   418: d2i
    //   419: istore #7
    //   421: aload #5
    //   423: getfield right : F
    //   426: f2d
    //   427: dstore #12
    //   429: dload #12
    //   431: invokestatic ceil : (D)D
    //   434: d2i
    //   435: istore #6
    //   437: aload #5
    //   439: getfield bottom : F
    //   442: f2d
    //   443: invokestatic ceil : (D)D
    //   446: d2i
    //   447: istore #14
    //   449: aload_2
    //   450: iload #11
    //   452: iload #7
    //   454: iload #6
    //   456: iload #14
    //   458: invokevirtual set : (IIII)V
    //   461: iload #4
    //   463: ifne -> 475
    //   466: iload #10
    //   468: istore #9
    //   470: iload #10
    //   472: ifeq -> 532
    //   475: iload #10
    //   477: istore #9
    //   479: aload_0
    //   480: getfield mParent : Landroid/view/ViewParent;
    //   483: ifnull -> 532
    //   486: aload_0
    //   487: getfield mParent : Landroid/view/ViewParent;
    //   490: instanceof android/view/ViewGroup
    //   493: ifeq -> 518
    //   496: aload_0
    //   497: getfield mParent : Landroid/view/ViewParent;
    //   500: checkcast android/view/ViewGroup
    //   503: astore_1
    //   504: aload_1
    //   505: aload_0
    //   506: aload_2
    //   507: aload_3
    //   508: iload #4
    //   510: invokevirtual getChildVisibleRect : (Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;Z)Z
    //   513: istore #9
    //   515: goto -> 532
    //   518: aload_0
    //   519: getfield mParent : Landroid/view/ViewParent;
    //   522: aload_0
    //   523: aload_2
    //   524: aload_3
    //   525: invokeinterface getChildVisibleRect : (Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z
    //   530: istore #9
    //   532: iload #9
    //   534: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #6407	-> 0
    //   #6408	-> 28
    //   #6410	-> 34
    //   #6411	-> 41
    //   #6414	-> 51
    //   #6415	-> 62
    //   #6417	-> 73
    //   #6419	-> 84
    //   #6420	-> 88
    //   #6421	-> 95
    //   #6422	-> 114
    //   #6423	-> 119
    //   #6424	-> 128
    //   #6425	-> 137
    //   #6426	-> 146
    //   #6427	-> 157
    //   #6429	-> 168
    //   #6430	-> 179
    //   #6433	-> 190
    //   #6434	-> 201
    //   #6436	-> 212
    //   #6437	-> 215
    //   #6438	-> 244
    //   #6440	-> 255
    //   #6443	-> 270
    //   #6446	-> 300
    //   #6450	-> 333
    //   #6452	-> 358
    //   #6455	-> 397
    //   #6456	-> 429
    //   #6455	-> 449
    //   #6458	-> 461
    //   #6459	-> 486
    //   #6460	-> 496
    //   #6461	-> 504
    //   #6463	-> 518
    //   #6466	-> 532
  }
  
  public final void layout(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!this.mSuppressLayout) {
      LayoutTransition layoutTransition = this.mTransition;
      if (layoutTransition == null || !layoutTransition.isChangingLayout()) {
        layoutTransition = this.mTransition;
        if (layoutTransition != null)
          layoutTransition.layoutChange(this); 
        super.layout(paramInt1, paramInt2, paramInt3, paramInt4);
        return;
      } 
    } 
    this.mLayoutCalledWhileSuppressed = true;
  }
  
  protected boolean canAnimate() {
    boolean bool;
    if (this.mLayoutAnimationController != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void startLayoutAnimation() {
    if (this.mLayoutAnimationController != null) {
      this.mGroupFlags |= 0x8;
      requestLayout();
    } 
  }
  
  public void scheduleLayoutAnimation() {
    this.mGroupFlags |= 0x8;
  }
  
  public void setLayoutAnimation(LayoutAnimationController paramLayoutAnimationController) {
    this.mLayoutAnimationController = paramLayoutAnimationController;
    if (paramLayoutAnimationController != null)
      this.mGroupFlags |= 0x8; 
  }
  
  public LayoutAnimationController getLayoutAnimation() {
    return this.mLayoutAnimationController;
  }
  
  @Deprecated
  public boolean isAnimationCacheEnabled() {
    boolean bool;
    if ((this.mGroupFlags & 0x40) == 64) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public void setAnimationCacheEnabled(boolean paramBoolean) {
    setBooleanFlag(64, paramBoolean);
  }
  
  @Deprecated
  public boolean isAlwaysDrawnWithCacheEnabled() {
    boolean bool;
    if ((this.mGroupFlags & 0x4000) == 16384) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public void setAlwaysDrawnWithCacheEnabled(boolean paramBoolean) {
    setBooleanFlag(16384, paramBoolean);
  }
  
  @Deprecated
  protected boolean isChildrenDrawnWithCacheEnabled() {
    boolean bool;
    if ((this.mGroupFlags & 0x8000) == 32768) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  protected void setChildrenDrawnWithCacheEnabled(boolean paramBoolean) {
    setBooleanFlag(32768, paramBoolean);
  }
  
  @ExportedProperty(category = "drawing")
  protected boolean isChildrenDrawingOrderEnabled() {
    boolean bool;
    if ((this.mGroupFlags & 0x400) == 1024) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void setChildrenDrawingOrderEnabled(boolean paramBoolean) {
    setBooleanFlag(1024, paramBoolean);
  }
  
  private boolean hasBooleanFlag(int paramInt) {
    boolean bool;
    if ((this.mGroupFlags & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setBooleanFlag(int paramInt, boolean paramBoolean) {
    if (paramBoolean) {
      this.mGroupFlags |= paramInt;
    } else {
      this.mGroupFlags &= paramInt ^ 0xFFFFFFFF;
    } 
  }
  
  @ExportedProperty(category = "drawing", mapping = {@IntToString(from = 0, to = "NONE"), @IntToString(from = 1, to = "ANIMATION"), @IntToString(from = 2, to = "SCROLLING"), @IntToString(from = 3, to = "ALL")})
  @Deprecated
  public int getPersistentDrawingCache() {
    return this.mPersistentDrawingCache;
  }
  
  @Deprecated
  public void setPersistentDrawingCache(int paramInt) {
    this.mPersistentDrawingCache = paramInt & 0x3;
  }
  
  private void setLayoutMode(int paramInt, boolean paramBoolean) {
    this.mLayoutMode = paramInt;
    setBooleanFlag(8388608, paramBoolean);
  }
  
  void invalidateInheritedLayoutMode(int paramInt) {
    int i = this.mLayoutMode;
    if (i != -1 && i != paramInt)
      if (!hasBooleanFlag(8388608)) {
        setLayoutMode(-1, false);
        int j;
        for (i = 0, j = getChildCount(); i < j; i++)
          getChildAt(i).invalidateInheritedLayoutMode(paramInt); 
        return;
      }  
  }
  
  public int getLayoutMode() {
    if (this.mLayoutMode == -1) {
      int i;
      if (this.mParent instanceof ViewGroup) {
        i = ((ViewGroup)this.mParent).getLayoutMode();
      } else {
        i = LAYOUT_MODE_DEFAULT;
      } 
      setLayoutMode(i, false);
    } 
    return this.mLayoutMode;
  }
  
  public void setLayoutMode(int paramInt) {
    if (this.mLayoutMode != paramInt) {
      boolean bool;
      invalidateInheritedLayoutMode(paramInt);
      if (paramInt != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      setLayoutMode(paramInt, bool);
      requestLayout();
    } 
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams generateLayoutParams(LayoutParams paramLayoutParams) {
    return paramLayoutParams;
  }
  
  protected LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-2, -2);
  }
  
  protected void debug(int paramInt) {
    super.debug(paramInt);
    if (this.mFocused != null) {
      String str = debugIndent(paramInt);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append("mFocused");
      str = stringBuilder.toString();
      Log.d("View", str);
      this.mFocused.debug(paramInt + 1);
    } 
    if (this.mDefaultFocus != null) {
      String str2 = debugIndent(paramInt);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append("mDefaultFocus");
      String str1 = stringBuilder.toString();
      Log.d("View", str1);
      this.mDefaultFocus.debug(paramInt + 1);
    } 
    if (this.mFocusedInCluster != null) {
      String str2 = debugIndent(paramInt);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append("mFocusedInCluster");
      String str1 = stringBuilder.toString();
      Log.d("View", str1);
      this.mFocusedInCluster.debug(paramInt + 1);
    } 
    if (this.mChildrenCount != 0) {
      String str2 = debugIndent(paramInt);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append("{");
      String str1 = stringBuilder.toString();
      Log.d("View", str1);
    } 
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++) {
      View view = this.mChildren[b];
      view.debug(paramInt + 1);
    } 
    if (this.mChildrenCount != 0) {
      String str2 = debugIndent(paramInt);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append("}");
      String str1 = stringBuilder.toString();
      Log.d("View", str1);
    } 
  }
  
  public int indexOfChild(View paramView) {
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      if (arrayOfView[b] == paramView)
        return b; 
    } 
    return -1;
  }
  
  public int getChildCount() {
    return this.mChildrenCount;
  }
  
  public View getChildAt(int paramInt) {
    if (paramInt < 0 || paramInt >= this.mChildrenCount)
      return null; 
    return this.mChildren[paramInt];
  }
  
  protected void measureChildren(int paramInt1, int paramInt2) {
    int i = this.mChildrenCount;
    View[] arrayOfView = this.mChildren;
    for (byte b = 0; b < i; b++) {
      View view = arrayOfView[b];
      if ((view.mViewFlags & 0xC) != 8)
        measureChild(view, paramInt1, paramInt2); 
    } 
  }
  
  protected void measureChild(View paramView, int paramInt1, int paramInt2) {
    LayoutParams layoutParams = paramView.getLayoutParams();
    paramInt1 = getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight, layoutParams.width);
    paramInt2 = getChildMeasureSpec(paramInt2, this.mPaddingTop + this.mPaddingBottom, layoutParams.height);
    paramView.measure(paramInt1, paramInt2);
  }
  
  protected void measureChildWithMargins(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    MarginLayoutParams marginLayoutParams = (MarginLayoutParams)paramView.getLayoutParams();
    paramInt1 = getChildMeasureSpec(paramInt1, this.mPaddingLeft + this.mPaddingRight + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + paramInt2, marginLayoutParams.width);
    paramInt2 = getChildMeasureSpec(paramInt3, this.mPaddingTop + this.mPaddingBottom + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + paramInt4, marginLayoutParams.height);
    paramView.measure(paramInt1, paramInt2);
  }
  
  public static int getChildMeasureSpec(int paramInt1, int paramInt2, int paramInt3) {
    int i = View.MeasureSpec.getMode(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt1);
    boolean bool1 = false;
    paramInt1 = 0;
    j = Math.max(0, j - paramInt2);
    boolean bool2 = false;
    paramInt2 = 0;
    if (i != Integer.MIN_VALUE) {
      if (i != 0) {
        if (i != 1073741824) {
          paramInt1 = bool2;
        } else if (paramInt3 >= 0) {
          paramInt1 = paramInt3;
          paramInt2 = 1073741824;
        } else if (paramInt3 == -1) {
          paramInt1 = j;
          paramInt2 = 1073741824;
        } else {
          paramInt1 = bool2;
          if (paramInt3 == -2) {
            paramInt1 = j;
            paramInt2 = Integer.MIN_VALUE;
          } 
        } 
      } else if (paramInt3 >= 0) {
        paramInt1 = paramInt3;
        paramInt2 = 1073741824;
      } else if (paramInt3 == -1) {
        if (!View.sUseZeroUnspecifiedMeasureSpec)
          paramInt1 = j; 
        paramInt2 = 0;
      } else {
        paramInt1 = bool2;
        if (paramInt3 == -2) {
          if (View.sUseZeroUnspecifiedMeasureSpec) {
            paramInt1 = bool1;
          } else {
            paramInt1 = j;
          } 
          paramInt2 = 0;
        } 
      } 
    } else if (paramInt3 >= 0) {
      paramInt1 = paramInt3;
      paramInt2 = 1073741824;
    } else if (paramInt3 == -1) {
      paramInt1 = j;
      paramInt2 = Integer.MIN_VALUE;
    } else {
      paramInt1 = bool2;
      if (paramInt3 == -2) {
        paramInt1 = j;
        paramInt2 = Integer.MIN_VALUE;
      } 
    } 
    return View.MeasureSpec.makeMeasureSpec(paramInt1, paramInt2);
  }
  
  public void clearDisappearingChildren() {
    ArrayList<View> arrayList = this.mDisappearingChildren;
    if (arrayList != null) {
      int i = arrayList.size();
      for (byte b = 0; b < i; b++) {
        View view = arrayList.get(b);
        if (view.mAttachInfo != null)
          view.dispatchDetachedFromWindow(); 
        view.clearAnimation();
      } 
      arrayList.clear();
      invalidate();
    } 
  }
  
  private void addDisappearingView(View paramView) {
    ArrayList<View> arrayList1 = this.mDisappearingChildren;
    ArrayList<View> arrayList2 = arrayList1;
    if (arrayList1 == null)
      this.mDisappearingChildren = arrayList2 = new ArrayList<>(); 
    arrayList2.add(paramView);
  }
  
  void finishAnimatingView(View paramView, Animation paramAnimation) {
    ArrayList<View> arrayList = this.mDisappearingChildren;
    if (arrayList != null && arrayList.contains(paramView)) {
      arrayList.remove(paramView);
      if (paramView.mAttachInfo != null)
        paramView.dispatchDetachedFromWindow(); 
      paramView.clearAnimation();
      this.mGroupFlags |= 0x4;
    } 
    if (paramAnimation != null && !paramAnimation.getFillAfter())
      paramView.clearAnimation(); 
    if ((paramView.mPrivateFlags & 0x10000) == 65536) {
      paramView.onAnimationEnd();
      paramView.mPrivateFlags &= 0xFFFEFFFF;
      this.mGroupFlags |= 0x4;
    } 
  }
  
  boolean isViewTransitioning(View paramView) {
    boolean bool;
    ArrayList<View> arrayList = this.mTransitioningViews;
    if (arrayList != null && arrayList.contains(paramView)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void startViewTransition(View paramView) {
    if (paramView.mParent == this) {
      if (this.mTransitioningViews == null)
        this.mTransitioningViews = new ArrayList<>(); 
      this.mTransitioningViews.add(paramView);
    } 
  }
  
  public void endViewTransition(View paramView) {
    ArrayList<View> arrayList = this.mTransitioningViews;
    if (arrayList != null) {
      arrayList.remove(paramView);
      arrayList = this.mDisappearingChildren;
      if (arrayList != null && arrayList.contains(paramView)) {
        arrayList.remove(paramView);
        arrayList = this.mVisibilityChangingChildren;
        if (arrayList != null && arrayList.contains(paramView)) {
          this.mVisibilityChangingChildren.remove(paramView);
        } else {
          if (paramView.mAttachInfo != null)
            paramView.dispatchDetachedFromWindow(); 
          if (paramView.mParent != null)
            paramView.mParent = null; 
        } 
        invalidate();
      } 
    } 
  }
  
  public void suppressLayout(boolean paramBoolean) {
    this.mSuppressLayout = paramBoolean;
    if (!paramBoolean && 
      this.mLayoutCalledWhileSuppressed) {
      requestLayout();
      this.mLayoutCalledWhileSuppressed = false;
    } 
  }
  
  public boolean isLayoutSuppressed() {
    return this.mSuppressLayout;
  }
  
  public boolean gatherTransparentRegion(Region paramRegion) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPrivateFlags : I
    //   4: istore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: iload_2
    //   8: sipush #512
    //   11: iand
    //   12: ifne -> 21
    //   15: iconst_1
    //   16: istore #4
    //   18: goto -> 24
    //   21: iconst_0
    //   22: istore #4
    //   24: iload #4
    //   26: ifeq -> 35
    //   29: aload_1
    //   30: ifnonnull -> 35
    //   33: iconst_1
    //   34: ireturn
    //   35: aload_0
    //   36: aload_1
    //   37: invokespecial gatherTransparentRegion : (Landroid/graphics/Region;)Z
    //   40: pop
    //   41: aload_0
    //   42: getfield mChildrenCount : I
    //   45: istore #5
    //   47: iconst_1
    //   48: istore #6
    //   50: iconst_1
    //   51: istore_2
    //   52: iload #5
    //   54: ifle -> 185
    //   57: aload_0
    //   58: invokevirtual buildOrderedChildList : ()Ljava/util/ArrayList;
    //   61: astore #7
    //   63: aload #7
    //   65: ifnonnull -> 81
    //   68: aload_0
    //   69: invokevirtual isChildrenDrawingOrderEnabled : ()Z
    //   72: ifeq -> 81
    //   75: iconst_1
    //   76: istore #8
    //   78: goto -> 84
    //   81: iconst_0
    //   82: istore #8
    //   84: aload_0
    //   85: getfield mChildren : [Landroid/view/View;
    //   88: astore #9
    //   90: iconst_0
    //   91: istore #6
    //   93: iload #6
    //   95: iload #5
    //   97: if_icmpge -> 169
    //   100: aload_0
    //   101: iload #5
    //   103: iload #6
    //   105: iload #8
    //   107: invokespecial getAndVerifyPreorderedIndex : (IIZ)I
    //   110: istore #10
    //   112: aload #7
    //   114: aload #9
    //   116: iload #10
    //   118: invokestatic getAndVerifyPreorderedView : (Ljava/util/ArrayList;[Landroid/view/View;I)Landroid/view/View;
    //   121: astore #11
    //   123: aload #11
    //   125: getfield mViewFlags : I
    //   128: bipush #12
    //   130: iand
    //   131: ifeq -> 145
    //   134: iload_2
    //   135: istore #10
    //   137: aload #11
    //   139: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   142: ifnull -> 160
    //   145: iload_2
    //   146: istore #10
    //   148: aload #11
    //   150: aload_1
    //   151: invokevirtual gatherTransparentRegion : (Landroid/graphics/Region;)Z
    //   154: ifne -> 160
    //   157: iconst_0
    //   158: istore #10
    //   160: iinc #6, 1
    //   163: iload #10
    //   165: istore_2
    //   166: goto -> 93
    //   169: iload_2
    //   170: istore #6
    //   172: aload #7
    //   174: ifnull -> 185
    //   177: aload #7
    //   179: invokevirtual clear : ()V
    //   182: iload_2
    //   183: istore #6
    //   185: iload #4
    //   187: ifne -> 198
    //   190: iload_3
    //   191: istore #8
    //   193: iload #6
    //   195: ifeq -> 201
    //   198: iconst_1
    //   199: istore #8
    //   201: iload #8
    //   203: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #7322	-> 0
    //   #7323	-> 24
    //   #7325	-> 33
    //   #7327	-> 35
    //   #7333	-> 41
    //   #7334	-> 47
    //   #7335	-> 52
    //   #7336	-> 57
    //   #7337	-> 63
    //   #7338	-> 68
    //   #7339	-> 84
    //   #7340	-> 90
    //   #7341	-> 100
    //   #7342	-> 112
    //   #7343	-> 123
    //   #7344	-> 145
    //   #7345	-> 157
    //   #7340	-> 160
    //   #7349	-> 169
    //   #7351	-> 185
  }
  
  public void requestTransparentRegion(View paramView) {
    if (paramView != null) {
      paramView.mPrivateFlags |= 0x200;
      if (this.mParent != null)
        this.mParent.requestTransparentRegion(this); 
    } 
  }
  
  public void subtractObscuredTouchableRegion(Region paramRegion, View paramView) {
    boolean bool;
    int i = this.mChildrenCount;
    ArrayList<View> arrayList = buildTouchDispatchChildList();
    if (arrayList == null && isChildrenDrawingOrderEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    View[] arrayOfView = this.mChildren;
    for (int j = i - 1; j >= 0; j--) {
      int k = getAndVerifyPreorderedIndex(i, j, bool);
      View view = getAndVerifyPreorderedView(arrayList, arrayOfView, k);
      if (view == paramView)
        break; 
      if (view.canReceivePointerEvents())
        applyOpToRegionByBounds(paramRegion, view, Region.Op.DIFFERENCE); 
    } 
    applyOpToRegionByBounds(paramRegion, this, Region.Op.INTERSECT);
    ViewParent viewParent = getParent();
    if (viewParent != null)
      viewParent.subtractObscuredTouchableRegion(paramRegion, this); 
  }
  
  private static void applyOpToRegionByBounds(Region paramRegion, View paramView, Region.Op paramOp) {
    int[] arrayOfInt = new int[2];
    paramView.getLocationInWindow(arrayOfInt);
    int i = arrayOfInt[0];
    int j = arrayOfInt[1];
    paramRegion.op(i, j, i + paramView.getWidth(), j + paramView.getHeight(), paramOp);
  }
  
  public WindowInsets dispatchApplyWindowInsets(WindowInsets paramWindowInsets) {
    paramWindowInsets = super.dispatchApplyWindowInsets(paramWindowInsets);
    if (paramWindowInsets.isConsumed())
      return paramWindowInsets; 
    if (View.sBrokenInsetsDispatch)
      return brokenDispatchApplyWindowInsets(paramWindowInsets); 
    return newDispatchApplyWindowInsets(paramWindowInsets);
  }
  
  private WindowInsets brokenDispatchApplyWindowInsets(WindowInsets paramWindowInsets) {
    WindowInsets windowInsets;
    int i = getChildCount();
    byte b = 0;
    while (true) {
      windowInsets = paramWindowInsets;
      if (b < i) {
        paramWindowInsets = getChildAt(b).dispatchApplyWindowInsets(paramWindowInsets);
        if (paramWindowInsets.isConsumed()) {
          windowInsets = paramWindowInsets;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    return windowInsets;
  }
  
  private WindowInsets newDispatchApplyWindowInsets(WindowInsets paramWindowInsets) {
    int i = getChildCount();
    for (byte b = 0; b < i; b++)
      getChildAt(b).dispatchApplyWindowInsets(paramWindowInsets); 
    return paramWindowInsets;
  }
  
  public void setWindowInsetsAnimationCallback(WindowInsetsAnimation.Callback paramCallback) {
    boolean bool;
    super.setWindowInsetsAnimationCallback(paramCallback);
    if (paramCallback != null) {
      bool = paramCallback.getDispatchMode();
    } else {
      bool = true;
    } 
    this.mInsetsAnimationDispatchMode = bool;
  }
  
  public boolean hasWindowInsetsAnimationCallback() {
    if (super.hasWindowInsetsAnimationCallback())
      return true; 
    if ((this.mViewFlags & 0x800) != 0 || 
      isFrameworkOptionalFitsSystemWindows()) {
      b = 1;
    } else {
      b = 0;
    } 
    if (b && this.mAttachInfo != null && this.mAttachInfo.mContentOnApplyWindowInsetsListener != null)
      if ((getWindowSystemUiVisibility() & 0x600) == 0)
        return false;  
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      if (getChildAt(b).hasWindowInsetsAnimationCallback())
        return true; 
    } 
    return false;
  }
  
  public void dispatchWindowInsetsAnimationPrepare(WindowInsetsAnimation paramWindowInsetsAnimation) {
    super.dispatchWindowInsetsAnimationPrepare(paramWindowInsetsAnimation);
    if ((this.mViewFlags & 0x800) != 0 || 
      isFrameworkOptionalFitsSystemWindows()) {
      b = 1;
    } else {
      b = 0;
    } 
    if (b && this.mAttachInfo != null && 
      (getListenerInfo()).mWindowInsetsAnimationCallback == null && this.mAttachInfo.mContentOnApplyWindowInsetsListener != null)
      if ((getWindowSystemUiVisibility() & 0x600) == 0) {
        this.mInsetsAnimationDispatchMode = 0;
        return;
      }  
    if (this.mInsetsAnimationDispatchMode == 0)
      return; 
    int i = getChildCount();
    for (byte b = 0; b < i; b++)
      getChildAt(b).dispatchWindowInsetsAnimationPrepare(paramWindowInsetsAnimation); 
  }
  
  public WindowInsetsAnimation.Bounds dispatchWindowInsetsAnimationStart(WindowInsetsAnimation paramWindowInsetsAnimation, WindowInsetsAnimation.Bounds paramBounds) {
    paramBounds = super.dispatchWindowInsetsAnimationStart(paramWindowInsetsAnimation, paramBounds);
    if (this.mInsetsAnimationDispatchMode == 0)
      return paramBounds; 
    int i = getChildCount();
    for (byte b = 0; b < i; b++)
      getChildAt(b).dispatchWindowInsetsAnimationStart(paramWindowInsetsAnimation, paramBounds); 
    return paramBounds;
  }
  
  public WindowInsets dispatchWindowInsetsAnimationProgress(WindowInsets paramWindowInsets, List<WindowInsetsAnimation> paramList) {
    paramWindowInsets = super.dispatchWindowInsetsAnimationProgress(paramWindowInsets, paramList);
    if (this.mInsetsAnimationDispatchMode == 0)
      return paramWindowInsets; 
    int i = getChildCount();
    for (byte b = 0; b < i; b++)
      getChildAt(b).dispatchWindowInsetsAnimationProgress(paramWindowInsets, paramList); 
    return paramWindowInsets;
  }
  
  public void dispatchWindowInsetsAnimationEnd(WindowInsetsAnimation paramWindowInsetsAnimation) {
    super.dispatchWindowInsetsAnimationEnd(paramWindowInsetsAnimation);
    if (this.mInsetsAnimationDispatchMode == 0)
      return; 
    int i = getChildCount();
    for (byte b = 0; b < i; b++)
      getChildAt(b).dispatchWindowInsetsAnimationEnd(paramWindowInsetsAnimation); 
  }
  
  private void dispatchTransformedScrollCaptureSearch(View paramView, Rect paramRect, Point paramPoint, Queue<ScrollCaptureTarget> paramQueue) {
    Rect rect = getTempRect();
    rect.set(paramRect);
    Point point = getTempPoint();
    point.set(paramPoint.x, paramPoint.y);
    int i = paramView.mLeft - this.mScrollX;
    int j = paramView.mTop - this.mScrollY;
    rect.offset(-i, -j);
    point.offset(i, j);
    boolean bool1 = true;
    i = this.mRight;
    i = this.mLeft;
    i = this.mBottom;
    i = this.mTop;
    if (getClipChildren())
      bool1 = rect.intersect(0, 0, paramView.getWidth(), paramView.getHeight()); 
    boolean bool2 = bool1;
    if (bool1) {
      bool2 = bool1;
      if (paramView instanceof ViewGroup) {
        ViewGroup viewGroup = (ViewGroup)paramView;
        bool2 = bool1;
        if (viewGroup.getClipToPadding()) {
          int k = paramView.mPaddingLeft;
          i = paramView.mPaddingTop;
          int m = paramView.getWidth();
          j = paramView.mPaddingRight;
          int n = paramView.getHeight(), i1 = paramView.mPaddingBottom;
          bool2 = rect.intersect(k, i, m - j, n - i1);
        } 
      } 
    } 
    bool1 = bool2;
    if (bool2) {
      bool1 = bool2;
      if (paramView.mClipBounds != null)
        bool1 = rect.intersect(paramView.mClipBounds); 
    } 
    if (bool1)
      paramView.dispatchScrollCaptureSearch(rect, point, paramQueue); 
  }
  
  public void dispatchScrollCaptureSearch(Rect paramRect, Point paramPoint, Queue<ScrollCaptureTarget> paramQueue) {
    super.dispatchScrollCaptureSearch(paramRect, paramPoint, paramQueue);
    if ((getScrollCaptureHint() & 0x4) == 0) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        if (view.getVisibility() == 0)
          dispatchTransformedScrollCaptureSearch(view, paramRect, paramPoint, paramQueue); 
      } 
    } 
  }
  
  public Animation.AnimationListener getLayoutAnimationListener() {
    return this.mAnimationListener;
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    int i = this.mGroupFlags;
    if ((0x10000 & i) != 0)
      if ((i & 0x2000) == 0) {
        View[] arrayOfView = this.mChildren;
        int j = this.mChildrenCount;
        for (i = 0; i < j; i++) {
          View view = arrayOfView[i];
          if ((view.mViewFlags & 0x400000) != 0)
            view.refreshDrawableState(); 
        } 
      } else {
        throw new IllegalStateException("addStateFromChildren cannot be enabled if a child has duplicateParentState set to true");
      }  
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    View[] arrayOfView = this.mChildren;
    int i = this.mChildrenCount;
    for (byte b = 0; b < i; b++)
      arrayOfView[b].jumpDrawablesToCurrentState(); 
  }
  
  protected int[] onCreateDrawableState(int paramInt) {
    if ((this.mGroupFlags & 0x2000) == 0)
      return super.onCreateDrawableState(paramInt); 
    int i = 0;
    int j = getChildCount();
    for (byte b = 0; b < j; b++, i = k) {
      int[] arrayOfInt1 = getChildAt(b).getDrawableState();
      int k = i;
      if (arrayOfInt1 != null)
        k = i + arrayOfInt1.length; 
    } 
    int[] arrayOfInt = super.onCreateDrawableState(paramInt + i);
    for (paramInt = 0; paramInt < j; paramInt++, arrayOfInt = arrayOfInt2) {
      int[] arrayOfInt1 = getChildAt(paramInt).getDrawableState();
      int[] arrayOfInt2 = arrayOfInt;
      if (arrayOfInt1 != null)
        arrayOfInt2 = mergeDrawableStates(arrayOfInt, arrayOfInt1); 
    } 
    return arrayOfInt;
  }
  
  public void setAddStatesFromChildren(boolean paramBoolean) {
    if (paramBoolean) {
      this.mGroupFlags |= 0x2000;
    } else {
      this.mGroupFlags &= 0xFFFFDFFF;
    } 
    refreshDrawableState();
  }
  
  public boolean addStatesFromChildren() {
    boolean bool;
    if ((this.mGroupFlags & 0x2000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void childDrawableStateChanged(View paramView) {
    if ((this.mGroupFlags & 0x2000) != 0)
      refreshDrawableState(); 
  }
  
  public void setLayoutAnimationListener(Animation.AnimationListener paramAnimationListener) {
    this.mAnimationListener = paramAnimationListener;
  }
  
  public void requestTransitionStart(LayoutTransition paramLayoutTransition) {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl != null)
      viewRootImpl.requestTransitionStart(paramLayoutTransition); 
  }
  
  public boolean resolveRtlPropertiesIfNeeded() {
    boolean bool = super.resolveRtlPropertiesIfNeeded();
    if (bool) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        if (view.isLayoutDirectionInherited())
          view.resolveRtlPropertiesIfNeeded(); 
      } 
    } 
    return bool;
  }
  
  public boolean resolveLayoutDirection() {
    boolean bool = super.resolveLayoutDirection();
    if (bool) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        if (view.isLayoutDirectionInherited())
          view.resolveLayoutDirection(); 
      } 
    } 
    return bool;
  }
  
  public boolean resolveTextDirection() {
    boolean bool = super.resolveTextDirection();
    if (bool) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        if (view.isTextDirectionInherited())
          view.resolveTextDirection(); 
      } 
    } 
    return bool;
  }
  
  public boolean resolveTextAlignment() {
    boolean bool = super.resolveTextAlignment();
    if (bool) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        if (view.isTextAlignmentInherited())
          view.resolveTextAlignment(); 
      } 
    } 
    return bool;
  }
  
  public void resolvePadding() {
    super.resolvePadding();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.isLayoutDirectionInherited() && !view.isPaddingResolved())
        view.resolvePadding(); 
    } 
  }
  
  protected void resolveDrawables() {
    super.resolveDrawables();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.isLayoutDirectionInherited() && !view.areDrawablesResolved())
        view.resolveDrawables(); 
    } 
  }
  
  public void resolveLayoutParams() {
    super.resolveLayoutParams();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view != null)
        view.resolveLayoutParams(); 
    } 
  }
  
  public void resetResolvedLayoutDirection() {
    super.resetResolvedLayoutDirection();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.isLayoutDirectionInherited())
        view.resetResolvedLayoutDirection(); 
    } 
  }
  
  public void resetResolvedTextDirection() {
    super.resetResolvedTextDirection();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.isTextDirectionInherited())
        view.resetResolvedTextDirection(); 
    } 
  }
  
  public void resetResolvedTextAlignment() {
    super.resetResolvedTextAlignment();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.isTextAlignmentInherited())
        view.resetResolvedTextAlignment(); 
    } 
  }
  
  public void resetResolvedPadding() {
    super.resetResolvedPadding();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.isLayoutDirectionInherited())
        view.resetResolvedPadding(); 
    } 
  }
  
  protected void resetResolvedDrawables() {
    super.resetResolvedDrawables();
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.isLayoutDirectionInherited())
        view.resetResolvedDrawables(); 
    } 
  }
  
  public boolean shouldDelayChildPressedState() {
    return true;
  }
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt) {
    return false;
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt) {
    this.mNestedScrollAxes = paramInt;
  }
  
  public void onStopNestedScroll(View paramView) {
    stopNestedScroll();
    this.mNestedScrollAxes = 0;
  }
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    dispatchNestedScroll(paramInt1, paramInt2, paramInt3, paramInt4, null);
  }
  
  public void onNestedPreScroll(View paramView, int paramInt1, int paramInt2, int[] paramArrayOfint) {
    dispatchNestedPreScroll(paramInt1, paramInt2, paramArrayOfint, null);
  }
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    return dispatchNestedFling(paramFloat1, paramFloat2, paramBoolean);
  }
  
  public boolean onNestedPreFling(View paramView, float paramFloat1, float paramFloat2) {
    return dispatchNestedPreFling(paramFloat1, paramFloat2);
  }
  
  public int getNestedScrollAxes() {
    return this.mNestedScrollAxes;
  }
  
  protected void onSetLayoutParams(View paramView, LayoutParams paramLayoutParams) {
    requestLayout();
  }
  
  public void captureTransitioningViews(List<View> paramList) {
    if (getVisibility() != 0)
      return; 
    if (isTransitionGroup()) {
      paramList.add(this);
    } else {
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        view.captureTransitioningViews(paramList);
      } 
    } 
  }
  
  public void findNamedViews(Map<String, View> paramMap) {
    if (getVisibility() != 0 && this.mGhostView == null)
      return; 
    super.findNamedViews(paramMap);
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      view.findNamedViews(paramMap);
    } 
  }
  
  boolean hasUnhandledKeyListener() {
    return (this.mChildUnhandledKeyListeners > 0 || super.hasUnhandledKeyListener());
  }
  
  void incrementChildUnhandledKeyListeners() {
    int i = this.mChildUnhandledKeyListeners + 1;
    if (i == 1 && 
      this.mParent instanceof ViewGroup)
      ((ViewGroup)this.mParent).incrementChildUnhandledKeyListeners(); 
  }
  
  void decrementChildUnhandledKeyListeners() {
    int i = this.mChildUnhandledKeyListeners - 1;
    if (i == 0 && 
      this.mParent instanceof ViewGroup)
      ((ViewGroup)this.mParent).decrementChildUnhandledKeyListeners(); 
  }
  
  View dispatchUnhandledKeyEvent(KeyEvent paramKeyEvent) {
    if (!hasUnhandledKeyListener())
      return null; 
    ArrayList<View> arrayList = buildOrderedChildList();
    if (arrayList != null) {
      try {
        for (int i = arrayList.size() - 1; i >= 0; i--) {
          View view = arrayList.get(i);
          view = view.dispatchUnhandledKeyEvent(paramKeyEvent);
          if (view != null)
            return view; 
        } 
      } finally {
        arrayList.clear();
      } 
    } else {
      for (int i = getChildCount() - 1; i >= 0; i--) {
        View view = getChildAt(i);
        view = view.dispatchUnhandledKeyEvent(paramKeyEvent);
        if (view != null)
          return view; 
      } 
    } 
    if (onUnhandledKeyEvent(paramKeyEvent))
      return this; 
    return null;
  }
  
  class LayoutParams {
    @Deprecated
    public static final int FILL_PARENT = -1;
    
    public static final int MATCH_PARENT = -1;
    
    public static final int WRAP_CONTENT = -2;
    
    @ExportedProperty(category = "layout", mapping = {@IntToString(from = -1, to = "MATCH_PARENT"), @IntToString(from = -2, to = "WRAP_CONTENT")})
    public int height;
    
    public LayoutAnimationController.AnimationParameters layoutAnimationParameters;
    
    @ExportedProperty(category = "layout", mapping = {@IntToString(from = -1, to = "MATCH_PARENT"), @IntToString(from = -2, to = "WRAP_CONTENT")})
    public int width;
    
    public LayoutParams(ViewGroup this$0, AttributeSet param1AttributeSet) {
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, R.styleable.ViewGroup_Layout);
      setBaseAttributes(typedArray, 0, 1);
      typedArray.recycle();
    }
    
    public LayoutParams(ViewGroup this$0, int param1Int1) {
      this.width = this$0;
      this.height = param1Int1;
    }
    
    public LayoutParams(ViewGroup this$0) {
      this.width = ((LayoutParams)this$0).width;
      this.height = ((LayoutParams)this$0).height;
    }
    
    LayoutParams() {}
    
    protected void setBaseAttributes(TypedArray param1TypedArray, int param1Int1, int param1Int2) {
      this.width = param1TypedArray.getLayoutDimension(param1Int1, "layout_width");
      this.height = param1TypedArray.getLayoutDimension(param1Int2, "layout_height");
    }
    
    public void resolveLayoutDirection(int param1Int) {}
    
    public String debug(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append("ViewGroup.LayoutParams={ width=");
      int i = this.width;
      stringBuilder.append(sizeToString(i));
      stringBuilder.append(", height=");
      stringBuilder.append(sizeToString(this.height));
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    public void onDebugDraw(View param1View, Canvas param1Canvas, Paint param1Paint) {}
    
    protected static String sizeToString(int param1Int) {
      if (param1Int == -2)
        return "wrap-content"; 
      if (param1Int == -1)
        return "match-parent"; 
      return String.valueOf(param1Int);
    }
    
    void encode(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      param1ViewHierarchyEncoder.beginObject(this);
      encodeProperties(param1ViewHierarchyEncoder);
      param1ViewHierarchyEncoder.endObject();
    }
    
    protected void encodeProperties(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      param1ViewHierarchyEncoder.addProperty("width", this.width);
      param1ViewHierarchyEncoder.addProperty("height", this.height);
    }
  }
  
  class MarginLayoutParams extends LayoutParams {
    @ExportedProperty(category = "layout")
    public int topMargin;
    
    @ExportedProperty(category = "layout")
    private int startMargin = Integer.MIN_VALUE;
    
    @ExportedProperty(category = "layout")
    public int rightMargin;
    
    @ExportedProperty(category = "layout", flagMapping = {@FlagToString(equals = 3, mask = 3, name = "LAYOUT_DIRECTION"), @FlagToString(equals = 4, mask = 4, name = "LEFT_MARGIN_UNDEFINED_MASK"), @FlagToString(equals = 8, mask = 8, name = "RIGHT_MARGIN_UNDEFINED_MASK"), @FlagToString(equals = 16, mask = 16, name = "RTL_COMPATIBILITY_MODE_MASK"), @FlagToString(equals = 32, mask = 32, name = "NEED_RESOLUTION_MASK")}, formatToHexString = true)
    byte mMarginFlags;
    
    @ExportedProperty(category = "layout")
    public int leftMargin;
    
    @ExportedProperty(category = "layout")
    private int endMargin = Integer.MIN_VALUE;
    
    @ExportedProperty(category = "layout")
    public int bottomMargin;
    
    private static final int UNDEFINED_MARGIN = -2147483648;
    
    private static final int RTL_COMPATIBILITY_MODE_MASK = 16;
    
    private static final int RIGHT_MARGIN_UNDEFINED_MASK = 8;
    
    private static final int NEED_RESOLUTION_MASK = 32;
    
    private static final int LEFT_MARGIN_UNDEFINED_MASK = 4;
    
    private static final int LAYOUT_DIRECTION_MASK = 3;
    
    private static final int DEFAULT_MARGIN_RESOLVED = 0;
    
    public static final int DEFAULT_MARGIN_RELATIVE = -2147483648;
    
    public MarginLayoutParams(ViewGroup this$0, AttributeSet param1AttributeSet) {
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, R.styleable.ViewGroup_MarginLayout);
      setBaseAttributes(typedArray, 0, 1);
      int i = typedArray.getDimensionPixelSize(2, -1);
      if (i >= 0) {
        this.leftMargin = i;
        this.topMargin = i;
        this.rightMargin = i;
        this.bottomMargin = i;
      } else {
        int j = typedArray.getDimensionPixelSize(9, -1);
        i = typedArray.getDimensionPixelSize(10, -1);
        if (j >= 0) {
          this.leftMargin = j;
          this.rightMargin = j;
        } else {
          this.leftMargin = j = typedArray.getDimensionPixelSize(3, -2147483648);
          if (j == Integer.MIN_VALUE) {
            this.mMarginFlags = (byte)(this.mMarginFlags | 0x4);
            this.leftMargin = 0;
          } 
          this.rightMargin = j = typedArray.getDimensionPixelSize(5, -2147483648);
          if (j == Integer.MIN_VALUE) {
            this.mMarginFlags = (byte)(this.mMarginFlags | 0x8);
            this.rightMargin = 0;
          } 
        } 
        this.startMargin = typedArray.getDimensionPixelSize(7, -2147483648);
        this.endMargin = typedArray.getDimensionPixelSize(8, -2147483648);
        if (i >= 0) {
          this.topMargin = i;
          this.bottomMargin = i;
        } else {
          this.topMargin = typedArray.getDimensionPixelSize(4, 0);
          this.bottomMargin = typedArray.getDimensionPixelSize(6, 0);
        } 
        if (isMarginRelative())
          this.mMarginFlags = (byte)(this.mMarginFlags | 0x20); 
      } 
      boolean bool = this$0.getApplicationInfo().hasRtlSupport();
      i = (this$0.getApplicationInfo()).targetSdkVersion;
      if (i < 17 || !bool)
        this.mMarginFlags = (byte)(this.mMarginFlags | 0x10); 
      this.mMarginFlags = (byte)(0x0 | this.mMarginFlags);
      typedArray.recycle();
    }
    
    public MarginLayoutParams(ViewGroup this$0, int param1Int1) {
      super(this$0, param1Int1);
      byte b = (byte)(this.mMarginFlags | 0x4);
      this.mMarginFlags = b = (byte)(b | 0x8);
      this.mMarginFlags = b = (byte)(b & 0xFFFFFFDF);
      this.mMarginFlags = (byte)(b & 0xFFFFFFEF);
    }
    
    public MarginLayoutParams(ViewGroup this$0) {
      this.width = ((MarginLayoutParams)this$0).width;
      this.height = ((MarginLayoutParams)this$0).height;
      this.leftMargin = ((MarginLayoutParams)this$0).leftMargin;
      this.topMargin = ((MarginLayoutParams)this$0).topMargin;
      this.rightMargin = ((MarginLayoutParams)this$0).rightMargin;
      this.bottomMargin = ((MarginLayoutParams)this$0).bottomMargin;
      this.startMargin = ((MarginLayoutParams)this$0).startMargin;
      this.endMargin = ((MarginLayoutParams)this$0).endMargin;
      this.mMarginFlags = ((MarginLayoutParams)this$0).mMarginFlags;
    }
    
    public MarginLayoutParams(ViewGroup this$0) {
      super((ViewGroup.LayoutParams)this$0);
      byte b = (byte)(this.mMarginFlags | 0x4);
      this.mMarginFlags = b = (byte)(b | 0x8);
      this.mMarginFlags = b = (byte)(b & 0xFFFFFFDF);
      this.mMarginFlags = (byte)(b & 0xFFFFFFEF);
    }
    
    public final void copyMarginsFrom(MarginLayoutParams param1MarginLayoutParams) {
      this.leftMargin = param1MarginLayoutParams.leftMargin;
      this.topMargin = param1MarginLayoutParams.topMargin;
      this.rightMargin = param1MarginLayoutParams.rightMargin;
      this.bottomMargin = param1MarginLayoutParams.bottomMargin;
      this.startMargin = param1MarginLayoutParams.startMargin;
      this.endMargin = param1MarginLayoutParams.endMargin;
      this.mMarginFlags = param1MarginLayoutParams.mMarginFlags;
    }
    
    public void setMargins(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.leftMargin = param1Int1;
      this.topMargin = param1Int2;
      this.rightMargin = param1Int3;
      this.bottomMargin = param1Int4;
      byte b = (byte)(this.mMarginFlags & 0xFFFFFFFB);
      this.mMarginFlags = (byte)(b & 0xFFFFFFF7);
      if (isMarginRelative()) {
        this.mMarginFlags = (byte)(this.mMarginFlags | 0x20);
      } else {
        this.mMarginFlags = (byte)(this.mMarginFlags & 0xFFFFFFDF);
      } 
    }
    
    public void setMarginsRelative(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.startMargin = param1Int1;
      this.topMargin = param1Int2;
      this.endMargin = param1Int3;
      this.bottomMargin = param1Int4;
      this.mMarginFlags = (byte)(this.mMarginFlags | 0x20);
    }
    
    public void setMarginStart(int param1Int) {
      this.startMargin = param1Int;
      this.mMarginFlags = (byte)(this.mMarginFlags | 0x20);
    }
    
    public int getMarginStart() {
      int i = this.startMargin;
      if (i != Integer.MIN_VALUE)
        return i; 
      if ((this.mMarginFlags & 0x20) == 32)
        doResolveMargins(); 
      if ((this.mMarginFlags & 0x3) != 1)
        return this.leftMargin; 
      return this.rightMargin;
    }
    
    public void setMarginEnd(int param1Int) {
      this.endMargin = param1Int;
      this.mMarginFlags = (byte)(this.mMarginFlags | 0x20);
    }
    
    public int getMarginEnd() {
      int i = this.endMargin;
      if (i != Integer.MIN_VALUE)
        return i; 
      if ((this.mMarginFlags & 0x20) == 32)
        doResolveMargins(); 
      if ((this.mMarginFlags & 0x3) != 1)
        return this.rightMargin; 
      return this.leftMargin;
    }
    
    public boolean isMarginRelative() {
      return (this.startMargin != Integer.MIN_VALUE || this.endMargin != Integer.MIN_VALUE);
    }
    
    public void setLayoutDirection(int param1Int) {
      if (param1Int != 0 && param1Int != 1)
        return; 
      byte b = this.mMarginFlags;
      if (param1Int != (b & 0x3)) {
        byte b1 = (byte)(b & 0xFFFFFFFC);
        this.mMarginFlags = (byte)(b1 | param1Int & 0x3);
        if (isMarginRelative()) {
          this.mMarginFlags = (byte)(this.mMarginFlags | 0x20);
        } else {
          this.mMarginFlags = (byte)(this.mMarginFlags & 0xFFFFFFDF);
        } 
      } 
    }
    
    public int getLayoutDirection() {
      return this.mMarginFlags & 0x3;
    }
    
    public void resolveLayoutDirection(int param1Int) {
      setLayoutDirection(param1Int);
      if (!isMarginRelative() || (this.mMarginFlags & 0x20) != 32)
        return; 
      doResolveMargins();
    }
    
    private void doResolveMargins() {
      int i = this.mMarginFlags;
      if ((i & 0x10) == 16) {
        if ((i & 0x4) == 4) {
          int j = this.startMargin;
          if (j > Integer.MIN_VALUE)
            this.leftMargin = j; 
        } 
        if ((this.mMarginFlags & 0x8) == 8) {
          i = this.endMargin;
          if (i > Integer.MIN_VALUE)
            this.rightMargin = i; 
        } 
      } else {
        boolean bool1 = false, bool2 = false;
        if ((i & 0x3) != 1) {
          i = this.startMargin;
          if (i <= Integer.MIN_VALUE)
            i = 0; 
          this.leftMargin = i;
          i = this.endMargin;
          if (i <= Integer.MIN_VALUE)
            i = bool2; 
          this.rightMargin = i;
        } else {
          i = this.endMargin;
          if (i <= Integer.MIN_VALUE)
            i = 0; 
          this.leftMargin = i;
          i = this.startMargin;
          if (i <= Integer.MIN_VALUE)
            i = bool1; 
          this.rightMargin = i;
        } 
      } 
      this.mMarginFlags = (byte)(this.mMarginFlags & 0xFFFFFFDF);
    }
    
    public boolean isLayoutRtl() {
      byte b = this.mMarginFlags;
      boolean bool = true;
      if ((b & 0x3) != 1)
        bool = false; 
      return bool;
    }
    
    public void onDebugDraw(View param1View, Canvas param1Canvas, Paint param1Paint) {
      Insets insets;
      if (View.isLayoutModeOptical(param1View.mParent)) {
        insets = param1View.getOpticalInsets();
      } else {
        insets = Insets.NONE;
      } 
      int i = param1View.getLeft(), j = insets.left;
      int k = param1View.getTop(), m = insets.top;
      int n = param1View.getRight(), i1 = insets.right;
      int i2 = param1View.getBottom(), i3 = insets.bottom, i4 = this.leftMargin, i5 = this.topMargin, i6 = this.rightMargin, i7 = this.bottomMargin;
      ViewGroup.fillDifference(param1Canvas, i + j, k + m, n - i1, i2 - i3, i4, i5, i6, i7, param1Paint);
    }
    
    protected void encodeProperties(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      super.encodeProperties(param1ViewHierarchyEncoder);
      param1ViewHierarchyEncoder.addProperty("leftMargin", this.leftMargin);
      param1ViewHierarchyEncoder.addProperty("topMargin", this.topMargin);
      param1ViewHierarchyEncoder.addProperty("rightMargin", this.rightMargin);
      param1ViewHierarchyEncoder.addProperty("bottomMargin", this.bottomMargin);
      param1ViewHierarchyEncoder.addProperty("startMargin", this.startMargin);
      param1ViewHierarchyEncoder.addProperty("endMargin", this.endMargin);
    }
  }
  
  class TouchTarget {
    public static final int ALL_POINTER_IDS = -1;
    
    private static final int MAX_RECYCLED = 32;
    
    private static TouchTarget sRecycleBin;
    
    private static final Object sRecycleLock = new Object[0];
    
    private static int sRecycledCount;
    
    public View child;
    
    public TouchTarget next;
    
    public int pointerIdBits;
    
    public static TouchTarget obtain(View param1View, int param1Int) {
      if (param1View != null)
        synchronized (sRecycleLock) {
          TouchTarget touchTarget;
          if (sRecycleBin == null) {
            touchTarget = new TouchTarget();
            this();
          } else {
            touchTarget = sRecycleBin;
            sRecycleBin = touchTarget.next;
            sRecycledCount--;
            touchTarget.next = null;
          } 
          touchTarget.child = param1View;
          touchTarget.pointerIdBits = param1Int;
          return touchTarget;
        }  
      throw new IllegalArgumentException("child must be non-null");
    }
    
    public void recycle() {
      if (this.child != null)
        synchronized (sRecycleLock) {
          if (sRecycledCount < 32) {
            this.next = sRecycleBin;
            sRecycleBin = this;
            sRecycledCount++;
          } else {
            this.next = null;
          } 
          this.child = null;
          return;
        }  
      throw new IllegalStateException("already recycled once");
    }
  }
  
  class HoverTarget {
    private static final int MAX_RECYCLED = 32;
    
    private static HoverTarget sRecycleBin;
    
    private static final Object sRecycleLock = new Object[0];
    
    private static int sRecycledCount;
    
    public View child;
    
    public HoverTarget next;
    
    public static HoverTarget obtain(View param1View) {
      if (param1View != null)
        synchronized (sRecycleLock) {
          HoverTarget hoverTarget;
          if (sRecycleBin == null) {
            hoverTarget = new HoverTarget();
            this();
          } else {
            hoverTarget = sRecycleBin;
            sRecycleBin = hoverTarget.next;
            sRecycledCount--;
            hoverTarget.next = null;
          } 
          hoverTarget.child = param1View;
          return hoverTarget;
        }  
      throw new IllegalArgumentException("child must be non-null");
    }
    
    public void recycle() {
      if (this.child != null)
        synchronized (sRecycleLock) {
          if (sRecycledCount < 32) {
            this.next = sRecycleBin;
            sRecycleBin = this;
            sRecycledCount++;
          } else {
            this.next = null;
          } 
          this.child = null;
          return;
        }  
      throw new IllegalStateException("already recycled once");
    }
  }
  
  class ChildListForAutoFillOrContentCapture extends ArrayList<View> {
    private static final int MAX_POOL_SIZE = 32;
    
    private static final Pools.SimplePool<ChildListForAutoFillOrContentCapture> sPool = new Pools.SimplePool<>(32);
    
    public static ChildListForAutoFillOrContentCapture obtain() {
      ChildListForAutoFillOrContentCapture childListForAutoFillOrContentCapture1 = sPool.acquire();
      ChildListForAutoFillOrContentCapture childListForAutoFillOrContentCapture2 = childListForAutoFillOrContentCapture1;
      if (childListForAutoFillOrContentCapture1 == null)
        childListForAutoFillOrContentCapture2 = new ChildListForAutoFillOrContentCapture(); 
      return childListForAutoFillOrContentCapture2;
    }
    
    public void recycle() {
      clear();
      sPool.release(this);
    }
  }
  
  class ChildListForAccessibility {
    private static final int MAX_POOL_SIZE = 32;
    
    private static final Pools.SynchronizedPool<ChildListForAccessibility> sPool = new Pools.SynchronizedPool<>(32);
    
    private final ArrayList<View> mChildren = new ArrayList<>();
    
    private final ArrayList<ViewGroup.ViewLocationHolder> mHolders = new ArrayList<>();
    
    public static ChildListForAccessibility obtain(ViewGroup param1ViewGroup, boolean param1Boolean) {
      ChildListForAccessibility childListForAccessibility1 = sPool.acquire();
      ChildListForAccessibility childListForAccessibility2 = childListForAccessibility1;
      if (childListForAccessibility1 == null)
        childListForAccessibility2 = new ChildListForAccessibility(); 
      childListForAccessibility2.init(param1ViewGroup, param1Boolean);
      return childListForAccessibility2;
    }
    
    public void recycle() {
      clear();
      sPool.release(this);
    }
    
    public int getChildCount() {
      return this.mChildren.size();
    }
    
    public View getChildAt(int param1Int) {
      return this.mChildren.get(param1Int);
    }
    
    private void init(ViewGroup param1ViewGroup, boolean param1Boolean) {
      ArrayList<View> arrayList = this.mChildren;
      int i = param1ViewGroup.getChildCount();
      byte b;
      for (b = 0; b < i; b++) {
        View view = param1ViewGroup.getChildAt(b);
        arrayList.add(view);
      } 
      if (param1Boolean) {
        ArrayList<ViewGroup.ViewLocationHolder> arrayList1 = this.mHolders;
        for (b = 0; b < i; b++) {
          View view = arrayList.get(b);
          ViewGroup.ViewLocationHolder viewLocationHolder = ViewGroup.ViewLocationHolder.obtain(param1ViewGroup, view);
          arrayList1.add(viewLocationHolder);
        } 
        sort(arrayList1);
        for (b = 0; b < i; b++) {
          ViewGroup.ViewLocationHolder viewLocationHolder = arrayList1.get(b);
          arrayList.set(b, viewLocationHolder.mView);
          viewLocationHolder.recycle();
        } 
        arrayList1.clear();
      } 
    }
    
    private void sort(ArrayList<ViewGroup.ViewLocationHolder> param1ArrayList) {
      try {
        ViewGroup.ViewLocationHolder.setComparisonStrategy(1);
        Collections.sort(param1ArrayList);
      } catch (IllegalArgumentException illegalArgumentException) {
        ViewGroup.ViewLocationHolder.setComparisonStrategy(2);
        Collections.sort(param1ArrayList);
      } 
    }
    
    private void clear() {
      this.mChildren.clear();
    }
  }
  
  class ViewLocationHolder implements Comparable<ViewLocationHolder> {
    public static final int COMPARISON_STRATEGY_LOCATION = 2;
    
    public static final int COMPARISON_STRATEGY_STRIPE = 1;
    
    private static final int MAX_POOL_SIZE = 32;
    
    private static int sComparisonStrategy = 1;
    
    ViewLocationHolder() {
      this.mLocation = new Rect();
    }
    
    private static final Pools.SynchronizedPool<ViewLocationHolder> sPool = new Pools.SynchronizedPool<>(32);
    
    private int mLayoutDirection;
    
    private final Rect mLocation;
    
    private ViewGroup mRoot;
    
    public View mView;
    
    static {
    
    }
    
    public static ViewLocationHolder obtain(ViewGroup param1ViewGroup, View param1View) {
      ViewLocationHolder viewLocationHolder1 = sPool.acquire();
      ViewLocationHolder viewLocationHolder2 = viewLocationHolder1;
      if (viewLocationHolder1 == null)
        viewLocationHolder2 = new ViewLocationHolder(); 
      viewLocationHolder2.init(param1ViewGroup, param1View);
      return viewLocationHolder2;
    }
    
    public static void setComparisonStrategy(int param1Int) {
      sComparisonStrategy = param1Int;
    }
    
    public void recycle() {
      clear();
      sPool.release(this);
    }
    
    public int compareTo(ViewLocationHolder param1ViewLocationHolder) {
      if (param1ViewLocationHolder == null)
        return 1; 
      int i = compareBoundsOfTree(this, param1ViewLocationHolder);
      if (i != 0)
        return i; 
      return this.mView.getAccessibilityViewId() - param1ViewLocationHolder.mView.getAccessibilityViewId();
    }
    
    private static int compareBoundsOfTree(ViewLocationHolder param1ViewLocationHolder1, ViewLocationHolder param1ViewLocationHolder2) {
      ViewLocationHolder viewLocationHolder;
      if (sComparisonStrategy == 1) {
        if (param1ViewLocationHolder1.mLocation.bottom - param1ViewLocationHolder2.mLocation.top <= 0)
          return -1; 
        if (param1ViewLocationHolder1.mLocation.top - param1ViewLocationHolder2.mLocation.bottom >= 0)
          return 1; 
      } 
      if (param1ViewLocationHolder1.mLayoutDirection == 0) {
        int j = param1ViewLocationHolder1.mLocation.left - param1ViewLocationHolder2.mLocation.left;
        if (j != 0)
          return j; 
      } else {
        int j = param1ViewLocationHolder1.mLocation.right - param1ViewLocationHolder2.mLocation.right;
        if (j != 0)
          return -j; 
      } 
      int i = param1ViewLocationHolder1.mLocation.top - param1ViewLocationHolder2.mLocation.top;
      if (i != 0)
        return i; 
      i = param1ViewLocationHolder1.mLocation.height() - param1ViewLocationHolder2.mLocation.height();
      if (i != 0)
        return -i; 
      i = param1ViewLocationHolder1.mLocation.width() - param1ViewLocationHolder2.mLocation.width();
      if (i != 0)
        return -i; 
      Rect rect1 = new Rect();
      Rect rect2 = new Rect();
      Rect rect3 = new Rect();
      param1ViewLocationHolder1.mView.getBoundsOnScreen(rect1, true);
      param1ViewLocationHolder2.mView.getBoundsOnScreen(rect2, true);
      rect1 = param1ViewLocationHolder1.mView.findViewByPredicateTraversal(new _$$Lambda$ViewGroup$ViewLocationHolder$QbO7cM0ULKe25a7bfXG3VH6DB0c(rect3, rect1), null);
      param1ViewLocationHolder2 = param1ViewLocationHolder2.mView.findViewByPredicateTraversal(new _$$Lambda$ViewGroup$ViewLocationHolder$AjKvqdj7SGGIzA5qrlZUuu71jl8(rect3, rect2), null);
      if (rect1 != null && param1ViewLocationHolder2 != null) {
        ViewGroup viewGroup2 = param1ViewLocationHolder1.mRoot;
        viewLocationHolder = obtain(viewGroup2, (View)rect1);
        ViewGroup viewGroup1 = param1ViewLocationHolder1.mRoot;
        ViewLocationHolder viewLocationHolder1 = obtain(viewGroup1, (View)param1ViewLocationHolder2);
        return compareBoundsOfTree(viewLocationHolder, viewLocationHolder1);
      } 
      if (viewLocationHolder != null)
        return 1; 
      if (param1ViewLocationHolder2 != null)
        return -1; 
      return 0;
    }
    
    private void init(ViewGroup param1ViewGroup, View param1View) {
      Rect rect = this.mLocation;
      param1View.getDrawingRect(rect);
      param1ViewGroup.offsetDescendantRectToMyCoords(param1View, rect);
      this.mView = param1View;
      this.mRoot = param1ViewGroup;
      this.mLayoutDirection = param1ViewGroup.getLayoutDirection();
    }
    
    private void clear() {
      this.mView = null;
      this.mRoot = null;
      this.mLocation.set(0, 0, 0, 0);
    }
  }
  
  private static void drawRect(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (sDebugLines == null)
      sDebugLines = new float[16]; 
    float[] arrayOfFloat = sDebugLines;
    arrayOfFloat[0] = paramInt1;
    arrayOfFloat[1] = paramInt2;
    arrayOfFloat[2] = paramInt3;
    arrayOfFloat[3] = paramInt2;
    arrayOfFloat[4] = paramInt3;
    arrayOfFloat[5] = paramInt2;
    arrayOfFloat[6] = paramInt3;
    arrayOfFloat[7] = paramInt4;
    arrayOfFloat[8] = paramInt3;
    arrayOfFloat[9] = paramInt4;
    arrayOfFloat[10] = paramInt1;
    arrayOfFloat[11] = paramInt4;
    arrayOfFloat[12] = paramInt1;
    arrayOfFloat[13] = paramInt4;
    arrayOfFloat[14] = paramInt1;
    arrayOfFloat[15] = paramInt2;
    paramCanvas.drawLines(arrayOfFloat, paramPaint);
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.addProperty("focus:descendantFocusability", getDescendantFocusability());
    paramViewHierarchyEncoder.addProperty("drawing:clipChildren", getClipChildren());
    paramViewHierarchyEncoder.addProperty("drawing:clipToPadding", getClipToPadding());
    paramViewHierarchyEncoder.addProperty("drawing:childrenDrawingOrderEnabled", isChildrenDrawingOrderEnabled());
    paramViewHierarchyEncoder.addProperty("drawing:persistentDrawingCache", getPersistentDrawingCache());
    int i = getChildCount();
    paramViewHierarchyEncoder.addProperty("meta:__childCount__", (short)i);
    for (byte b = 0; b < i; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("meta:__child__");
      stringBuilder.append(b);
      paramViewHierarchyEncoder.addPropertyKey(stringBuilder.toString());
      getChildAt(b).encode(paramViewHierarchyEncoder);
    } 
  }
  
  public final void onDescendantUnbufferedRequested() {
    int i = 0;
    View view = this.mFocused;
    if (view != null)
      i = view.mUnbufferedInputSource & 0xFFFFFFFD; 
    this.mUnbufferedInputSource = i;
    for (i = 0; i < this.mChildrenCount; i++) {
      view = this.mChildren[i];
      if ((view.mUnbufferedInputSource & 0x2) != 0) {
        this.mUnbufferedInputSource |= 0x2;
        break;
      } 
    } 
    if (this.mParent != null)
      this.mParent.onDescendantUnbufferedRequested(); 
  }
  
  protected abstract void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  class OnHierarchyChangeListener {
    public abstract void onChildViewAdded(View param1View1, View param1View2);
    
    public abstract void onChildViewRemoved(View param1View1, View param1View2);
  }
}
