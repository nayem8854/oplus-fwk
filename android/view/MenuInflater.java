package android.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.BlendMode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.android.internal.R;
import com.android.internal.view.menu.MenuItemImpl;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class MenuInflater {
  private static final Class<?>[] ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE;
  
  private static final Class<?>[] ACTION_VIEW_CONSTRUCTOR_SIGNATURE;
  
  private static final String LOG_TAG = "MenuInflater";
  
  private static final int NO_ID = 0;
  
  private static final String XML_GROUP = "group";
  
  private static final String XML_ITEM = "item";
  
  private static final String XML_MENU = "menu";
  
  private final Object[] mActionProviderConstructorArguments;
  
  private final Object[] mActionViewConstructorArguments;
  
  private Context mContext;
  
  private Object mRealOwner;
  
  static {
    Class[] arrayOfClass = new Class[1];
    arrayOfClass[0] = Context.class;
    ACTION_VIEW_CONSTRUCTOR_SIGNATURE = arrayOfClass;
    ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE = arrayOfClass;
  }
  
  public MenuInflater(Context paramContext) {
    this.mContext = paramContext;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramContext;
    this.mActionViewConstructorArguments = arrayOfObject;
    this.mActionProviderConstructorArguments = arrayOfObject;
  }
  
  public MenuInflater(Context paramContext, Object paramObject) {
    this.mContext = paramContext;
    this.mRealOwner = paramObject;
    paramObject = new Object[1];
    paramObject[0] = paramContext;
    this.mActionViewConstructorArguments = (Object[])paramObject;
    this.mActionProviderConstructorArguments = (Object[])paramObject;
  }
  
  public void inflate(int paramInt, Menu paramMenu) {
    XmlPullParserException xmlPullParserException2;
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null, xmlResourceParser3 = null;
    try {
      XmlResourceParser xmlResourceParser = this.mContext.getResources().getLayout(paramInt);
      xmlResourceParser3 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xmlResourceParser);
      xmlResourceParser3 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      parseMenu((XmlPullParser)xmlResourceParser, attributeSet, paramMenu);
      if (xmlResourceParser != null)
        xmlResourceParser.close(); 
      return;
    } catch (XmlPullParserException xmlPullParserException1) {
      xmlResourceParser3 = xmlResourceParser2;
      InflateException inflateException = new InflateException();
      xmlResourceParser3 = xmlResourceParser2;
      this("Error inflating menu XML", (Throwable)xmlPullParserException1);
      xmlResourceParser3 = xmlResourceParser2;
      throw inflateException;
    } catch (IOException iOException) {
      xmlPullParserException2 = xmlPullParserException1;
      InflateException inflateException = new InflateException();
      xmlPullParserException2 = xmlPullParserException1;
      this("Error inflating menu XML", iOException);
      xmlPullParserException2 = xmlPullParserException1;
      throw inflateException;
    } finally {}
    if (xmlPullParserException2 != null)
      xmlPullParserException2.close(); 
    throw paramMenu;
  }
  
  private void parseMenu(XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Menu paramMenu) throws XmlPullParserException, IOException {
    StringBuilder stringBuilder;
    MenuState menuState = new MenuState(paramMenu);
    int i = paramXmlPullParser.getEventType();
    int j = 0;
    Menu menu = null;
    while (true) {
      if (i == 2) {
        String str = paramXmlPullParser.getName();
        if (str.equals("menu")) {
          i = paramXmlPullParser.next();
          break;
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Expecting menu, got ");
        stringBuilder.append(str);
        throw new RuntimeException(stringBuilder.toString());
      } 
      int m = stringBuilder.next();
      i = m;
      if (m == 1) {
        i = m;
        break;
      } 
    } 
    boolean bool = false;
    int k = i;
    while (!bool) {
      if (k != 1) {
        Menu menu1;
        boolean bool1;
        if (k != 2) {
          if (k != 3) {
            i = j;
            paramMenu = menu;
            bool1 = bool;
          } else {
            String str = stringBuilder.getName();
            if (j && str.equals(menu)) {
              i = 0;
              paramMenu = null;
              bool1 = bool;
            } else if (str.equals("group")) {
              menuState.resetGroup();
              i = j;
              paramMenu = menu;
              bool1 = bool;
            } else if (str.equals("item")) {
              i = j;
              paramMenu = menu;
              bool1 = bool;
              if (!menuState.hasAddedItem())
                if (menuState.itemActionProvider != null && 
                  menuState.itemActionProvider.hasSubMenu()) {
                  registerMenu(menuState.addSubMenuItem(), paramAttributeSet);
                  i = j;
                  paramMenu = menu;
                  bool1 = bool;
                } else {
                  registerMenu(menuState.addItem(), paramAttributeSet);
                  i = j;
                  paramMenu = menu;
                  bool1 = bool;
                }  
            } else {
              i = j;
              paramMenu = menu;
              bool1 = bool;
              if (str.equals("menu")) {
                bool1 = true;
                i = j;
                paramMenu = menu;
              } 
            } 
          } 
        } else if (j) {
          i = j;
          paramMenu = menu;
          bool1 = bool;
        } else {
          String str = stringBuilder.getName();
          if (str.equals("group")) {
            menuState.readGroup(paramAttributeSet);
            i = j;
            menu1 = menu;
            bool1 = bool;
          } else if (menu1.equals("item")) {
            menuState.readItem(paramAttributeSet);
            i = j;
            menu1 = menu;
            bool1 = bool;
          } else if (menu1.equals("menu")) {
            menu1 = menuState.addSubMenuItem();
            registerMenu((SubMenu)menu1, paramAttributeSet);
            parseMenu((XmlPullParser)stringBuilder, paramAttributeSet, menu1);
            i = j;
            menu1 = menu;
            bool1 = bool;
          } else {
            i = 1;
            bool1 = bool;
          } 
        } 
        k = stringBuilder.next();
        j = i;
        menu = menu1;
        bool = bool1;
        continue;
      } 
      throw new RuntimeException("Unexpected end of document");
    } 
  }
  
  private void registerMenu(MenuItem paramMenuItem, AttributeSet paramAttributeSet) {}
  
  private void registerMenu(SubMenu paramSubMenu, AttributeSet paramAttributeSet) {}
  
  Context getContext() {
    return this.mContext;
  }
  
  class InflatedOnMenuItemClickListener implements MenuItem.OnMenuItemClickListener {
    private static final Class<?>[] PARAM_TYPES = new Class[] { MenuItem.class };
    
    private Method mMethod;
    
    private Object mRealOwner;
    
    public InflatedOnMenuItemClickListener(MenuInflater this$0, String param1String) {
      this.mRealOwner = this$0;
      Class<?> clazz = this$0.getClass();
      try {
        this.mMethod = clazz.getMethod(param1String, PARAM_TYPES);
        return;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Couldn't resolve menu item onClick handler ");
        stringBuilder.append(param1String);
        stringBuilder.append(" in class ");
        stringBuilder.append(clazz.getName());
        InflateException inflateException = new InflateException(stringBuilder.toString());
        inflateException.initCause(exception);
        throw inflateException;
      } 
    }
    
    public boolean onMenuItemClick(MenuItem param1MenuItem) {
      try {
        if (this.mMethod.getReturnType() == boolean.class)
          return ((Boolean)this.mMethod.invoke(this.mRealOwner, new Object[] { param1MenuItem })).booleanValue(); 
        this.mMethod.invoke(this.mRealOwner, new Object[] { param1MenuItem });
        return true;
      } catch (Exception exception) {
        throw new RuntimeException(exception);
      } 
    }
  }
  
  private Object getRealOwner() {
    if (this.mRealOwner == null)
      this.mRealOwner = findRealOwner(this.mContext); 
    return this.mRealOwner;
  }
  
  private Object findRealOwner(Object paramObject) {
    if (paramObject instanceof android.app.Activity)
      return paramObject; 
    if (paramObject instanceof ContextWrapper)
      return findRealOwner(((ContextWrapper)paramObject).getBaseContext()); 
    return paramObject;
  }
  
  private class MenuState {
    private static final int defaultGroupId = 0;
    
    private static final int defaultItemCategory = 0;
    
    private static final int defaultItemCheckable = 0;
    
    private static final boolean defaultItemChecked = false;
    
    private static final boolean defaultItemEnabled = true;
    
    private static final int defaultItemId = 0;
    
    private static final int defaultItemOrder = 0;
    
    private static final boolean defaultItemVisible = true;
    
    private int groupCategory;
    
    private int groupCheckable;
    
    private boolean groupEnabled;
    
    private int groupId;
    
    private int groupOrder;
    
    private boolean groupVisible;
    
    private ActionProvider itemActionProvider;
    
    private String itemActionProviderClassName;
    
    private String itemActionViewClassName;
    
    private int itemActionViewLayout;
    
    private boolean itemAdded;
    
    private int itemAlphabeticModifiers;
    
    private char itemAlphabeticShortcut;
    
    private int itemCategoryOrder;
    
    private int itemCheckable;
    
    private boolean itemChecked;
    
    private CharSequence itemContentDescription;
    
    private boolean itemEnabled;
    
    private int itemIconResId;
    
    private ColorStateList itemIconTintList = null;
    
    private int itemId;
    
    private String itemListenerMethodName;
    
    private int itemNumericModifiers;
    
    private char itemNumericShortcut;
    
    private int itemShowAsAction;
    
    private CharSequence itemTitle;
    
    private CharSequence itemTitleCondensed;
    
    private CharSequence itemTooltipText;
    
    private boolean itemVisible;
    
    private BlendMode mItemIconBlendMode = null;
    
    private Menu menu;
    
    final MenuInflater this$0;
    
    public MenuState(Menu param1Menu) {
      this.menu = param1Menu;
      resetGroup();
    }
    
    public void resetGroup() {
      this.groupId = 0;
      this.groupCategory = 0;
      this.groupOrder = 0;
      this.groupCheckable = 0;
      this.groupVisible = true;
      this.groupEnabled = true;
    }
    
    public void readGroup(AttributeSet param1AttributeSet) {
      TypedArray typedArray = MenuInflater.this.mContext.obtainStyledAttributes(param1AttributeSet, R.styleable.MenuGroup);
      this.groupId = typedArray.getResourceId(1, 0);
      this.groupCategory = typedArray.getInt(3, 0);
      this.groupOrder = typedArray.getInt(4, 0);
      this.groupCheckable = typedArray.getInt(5, 0);
      this.groupVisible = typedArray.getBoolean(2, true);
      this.groupEnabled = typedArray.getBoolean(0, true);
      typedArray.recycle();
    }
    
    public void readItem(AttributeSet param1AttributeSet) {
      TypedArray typedArray = MenuInflater.this.mContext.obtainStyledAttributes(param1AttributeSet, R.styleable.MenuItem);
      this.itemId = typedArray.getResourceId(2, 0);
      int i = typedArray.getInt(5, this.groupCategory);
      int j = typedArray.getInt(6, this.groupOrder);
      this.itemCategoryOrder = 0xFFFF0000 & i | 0xFFFF & j;
      this.itemTitle = typedArray.getText(7);
      this.itemTitleCondensed = typedArray.getText(8);
      this.itemIconResId = typedArray.getResourceId(0, 0);
      if (typedArray.hasValue(22)) {
        this.mItemIconBlendMode = Drawable.parseBlendMode(typedArray.getInt(22, -1), this.mItemIconBlendMode);
      } else {
        this.mItemIconBlendMode = null;
      } 
      if (typedArray.hasValue(21)) {
        this.itemIconTintList = typedArray.getColorStateList(21);
      } else {
        this.itemIconTintList = null;
      } 
      this.itemAlphabeticShortcut = getShortcut(typedArray.getString(9));
      this.itemAlphabeticModifiers = typedArray.getInt(19, 4096);
      this.itemNumericShortcut = getShortcut(typedArray.getString(10));
      this.itemNumericModifiers = typedArray.getInt(20, 4096);
      if (typedArray.hasValue(11)) {
        this.itemCheckable = typedArray.getBoolean(11, false);
      } else {
        this.itemCheckable = this.groupCheckable;
      } 
      this.itemChecked = typedArray.getBoolean(3, false);
      this.itemVisible = typedArray.getBoolean(4, this.groupVisible);
      boolean bool = this.groupEnabled;
      j = 1;
      this.itemEnabled = typedArray.getBoolean(1, bool);
      this.itemShowAsAction = typedArray.getInt(14, -1);
      this.itemListenerMethodName = typedArray.getString(12);
      this.itemActionViewLayout = typedArray.getResourceId(15, 0);
      this.itemActionViewClassName = typedArray.getString(16);
      String str = typedArray.getString(17);
      if (str == null)
        j = 0; 
      if (j != 0 && this.itemActionViewLayout == 0 && this.itemActionViewClassName == null) {
        str = this.itemActionProviderClassName;
        Class[] arrayOfClass = MenuInflater.ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE;
        MenuInflater menuInflater = MenuInflater.this;
        Object[] arrayOfObject = menuInflater.mActionProviderConstructorArguments;
        this.itemActionProvider = newInstance(str, arrayOfClass, arrayOfObject);
      } else {
        if (j != 0)
          Log.w("MenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified."); 
        this.itemActionProvider = null;
      } 
      this.itemContentDescription = typedArray.getText(13);
      this.itemTooltipText = typedArray.getText(18);
      typedArray.recycle();
      this.itemAdded = false;
    }
    
    private char getShortcut(String param1String) {
      if (param1String == null)
        return Character.MIN_VALUE; 
      return param1String.charAt(0);
    }
    
    private void setItem(MenuItem param1MenuItem) {
      MenuItem menuItem2 = param1MenuItem.setChecked(this.itemChecked);
      boolean bool = this.itemVisible;
      menuItem2 = menuItem2.setVisible(bool);
      bool = this.itemEnabled;
      menuItem2 = menuItem2.setEnabled(bool);
      if (this.itemCheckable >= 1) {
        bool = true;
      } else {
        bool = false;
      } 
      MenuItem menuItem3 = menuItem2.setCheckable(bool);
      CharSequence charSequence = this.itemTitleCondensed;
      MenuItem menuItem1 = menuItem3.setTitleCondensed(charSequence);
      int i = this.itemIconResId;
      menuItem1 = menuItem1.setIcon(i);
      char c = this.itemAlphabeticShortcut;
      i = this.itemAlphabeticModifiers;
      menuItem1 = menuItem1.setAlphabeticShortcut(c, i);
      c = this.itemNumericShortcut;
      i = this.itemNumericModifiers;
      menuItem1.setNumericShortcut(c, i);
      i = this.itemShowAsAction;
      if (i >= 0)
        param1MenuItem.setShowAsAction(i); 
      BlendMode blendMode = this.mItemIconBlendMode;
      if (blendMode != null)
        param1MenuItem.setIconTintBlendMode(blendMode); 
      ColorStateList colorStateList = this.itemIconTintList;
      if (colorStateList != null)
        param1MenuItem.setIconTintList(colorStateList); 
      if (this.itemListenerMethodName != null)
        if (!MenuInflater.this.mContext.isRestricted()) {
          MenuInflater menuInflater = MenuInflater.this;
          MenuInflater.InflatedOnMenuItemClickListener inflatedOnMenuItemClickListener = new MenuInflater.InflatedOnMenuItemClickListener(menuInflater.getRealOwner(), this.itemListenerMethodName);
          param1MenuItem.setOnMenuItemClickListener(inflatedOnMenuItemClickListener);
        } else {
          throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
        }  
      if (param1MenuItem instanceof MenuItemImpl) {
        MenuItemImpl menuItemImpl = (MenuItemImpl)param1MenuItem;
        if (this.itemCheckable >= 2)
          menuItemImpl.setExclusiveCheckable(true); 
      } 
      i = 0;
      String str = this.itemActionViewClassName;
      if (str != null) {
        Class[] arrayOfClass = MenuInflater.ACTION_VIEW_CONSTRUCTOR_SIGNATURE;
        Object[] arrayOfObject = MenuInflater.this.mActionViewConstructorArguments;
        View view = newInstance(str, arrayOfClass, arrayOfObject);
        param1MenuItem.setActionView(view);
        i = 1;
      } 
      int j = this.itemActionViewLayout;
      if (j > 0)
        if (i == 0) {
          param1MenuItem.setActionView(j);
        } else {
          Log.w("MenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
        }  
      ActionProvider actionProvider = this.itemActionProvider;
      if (actionProvider != null)
        param1MenuItem.setActionProvider(actionProvider); 
      param1MenuItem.setContentDescription(this.itemContentDescription);
      param1MenuItem.setTooltipText(this.itemTooltipText);
    }
    
    public MenuItem addItem() {
      this.itemAdded = true;
      MenuItem menuItem = this.menu.add(this.groupId, this.itemId, this.itemCategoryOrder, this.itemTitle);
      setItem(menuItem);
      return menuItem;
    }
    
    public SubMenu addSubMenuItem() {
      this.itemAdded = true;
      SubMenu subMenu = this.menu.addSubMenu(this.groupId, this.itemId, this.itemCategoryOrder, this.itemTitle);
      setItem(subMenu.getItem());
      return subMenu;
    }
    
    public boolean hasAddedItem() {
      return this.itemAdded;
    }
    
    private <T> T newInstance(String param1String, Class<?>[] param1ArrayOfClass, Object[] param1ArrayOfObject) {
      try {
        Class<?> clazz = MenuInflater.this.mContext.getClassLoader().loadClass(param1String);
        null = clazz.getConstructor(param1ArrayOfClass);
        null.setAccessible(true);
        return (T)null.newInstance(param1ArrayOfObject);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot instantiate class: ");
        stringBuilder.append(param1String);
        Log.w("MenuInflater", stringBuilder.toString(), exception);
        return null;
      } 
    }
  }
}
