package android.view;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class VerifiedKeyEvent extends VerifiedInputEvent implements Parcelable {
  public Boolean getFlag(int paramInt) {
    boolean bool;
    if (paramInt != 32)
      return null; 
    if ((this.mFlags & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return Boolean.valueOf(bool);
  }
  
  public VerifiedKeyEvent(int paramInt1, long paramLong1, int paramInt2, int paramInt3, int paramInt4, long paramLong2, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9) {
    super(1, paramInt1, paramLong1, paramInt2, paramInt3);
    this.mAction = paramInt4;
    AnnotationValidations.validate(KeyEventAction.class, null, paramInt4);
    this.mDownTimeNanos = paramLong2;
    AnnotationValidations.validate(SuppressLint.class, null, Long.valueOf(paramLong2), new Object[] { "value", "MethodNameUnits" });
    this.mFlags = paramInt5;
    this.mKeyCode = paramInt6;
    this.mScanCode = paramInt7;
    this.mMetaState = paramInt8;
    this.mRepeatCount = paramInt9;
  }
  
  public int getAction() {
    return this.mAction;
  }
  
  public long getDownTimeNanos() {
    return this.mDownTimeNanos;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public int getKeyCode() {
    return this.mKeyCode;
  }
  
  public int getScanCode() {
    return this.mScanCode;
  }
  
  public int getMetaState() {
    return this.mMetaState;
  }
  
  public int getRepeatCount() {
    return this.mRepeatCount;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (getDeviceId() != paramObject.getDeviceId() || 
      getEventTimeNanos() != paramObject.getEventTimeNanos() || 
      getSource() != paramObject.getSource() || 
      getDisplayId() != paramObject.getDisplayId() || this.mAction != ((VerifiedKeyEvent)paramObject).mAction || this.mDownTimeNanos != ((VerifiedKeyEvent)paramObject).mDownTimeNanos || this.mFlags != ((VerifiedKeyEvent)paramObject).mFlags || this.mKeyCode != ((VerifiedKeyEvent)paramObject).mKeyCode || this.mScanCode != ((VerifiedKeyEvent)paramObject).mScanCode || this.mMetaState != ((VerifiedKeyEvent)paramObject).mMetaState || this.mRepeatCount != ((VerifiedKeyEvent)paramObject).mRepeatCount)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    int i = getDeviceId();
    int j = Long.hashCode(getEventTimeNanos());
    int k = getSource();
    int m = getDisplayId();
    int n = this.mAction;
    int i1 = Long.hashCode(this.mDownTimeNanos);
    int i2 = this.mFlags;
    int i3 = this.mKeyCode;
    int i4 = this.mScanCode;
    int i5 = this.mMetaState;
    int i6 = this.mRepeatCount;
    return ((((((((((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.mAction);
    paramParcel.writeLong(this.mDownTimeNanos);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeInt(this.mKeyCode);
    paramParcel.writeInt(this.mScanCode);
    paramParcel.writeInt(this.mMetaState);
    paramParcel.writeInt(this.mRepeatCount);
  }
  
  public int describeContents() {
    return 0;
  }
  
  VerifiedKeyEvent(Parcel paramParcel) {
    super(paramParcel, 1);
    int i = paramParcel.readInt();
    long l = paramParcel.readLong();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    int m = paramParcel.readInt();
    int n = paramParcel.readInt();
    int i1 = paramParcel.readInt();
    this.mAction = i;
    AnnotationValidations.validate(KeyEventAction.class, null, i);
    this.mDownTimeNanos = l;
    AnnotationValidations.validate(SuppressLint.class, null, Long.valueOf(l), new Object[] { "value", "MethodNameUnits" });
    this.mFlags = j;
    this.mKeyCode = k;
    this.mScanCode = m;
    this.mMetaState = n;
    this.mRepeatCount = i1;
  }
  
  public static final Parcelable.Creator<VerifiedKeyEvent> CREATOR = (Parcelable.Creator<VerifiedKeyEvent>)new Object();
  
  private static final String TAG = "VerifiedKeyEvent";
  
  private int mAction;
  
  private long mDownTimeNanos;
  
  private int mFlags;
  
  private int mKeyCode;
  
  private int mMetaState;
  
  private int mRepeatCount;
  
  private int mScanCode;
  
  @Deprecated
  private void __metadata() {}
  
  @Retention(RetentionPolicy.SOURCE)
  class KeyEventAction implements Annotation {}
}
