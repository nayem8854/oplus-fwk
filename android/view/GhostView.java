package android.view;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RecordingCanvas;
import android.graphics.RenderNode;
import android.widget.FrameLayout;
import java.util.ArrayList;

public class GhostView extends View {
  private boolean mBeingMoved;
  
  private int mReferences;
  
  private final View mView;
  
  private GhostView(View paramView) {
    super(paramView.getContext());
    this.mView = paramView;
    paramView.mGhostView = this;
    paramView = (ViewGroup)this.mView.getParent();
    this.mView.setTransitionVisibility(4);
    paramView.invalidate();
  }
  
  protected void onDraw(Canvas paramCanvas) {
    if (paramCanvas instanceof RecordingCanvas) {
      RecordingCanvas recordingCanvas = (RecordingCanvas)paramCanvas;
      this.mView.mRecreateDisplayList = true;
      RenderNode renderNode = this.mView.updateDisplayListIfDirty();
      if (renderNode.hasDisplayList()) {
        recordingCanvas.insertReorderBarrier();
        recordingCanvas.drawRenderNode(renderNode);
        recordingCanvas.insertInorderBarrier();
      } 
    } 
  }
  
  public void setMatrix(Matrix paramMatrix) {
    this.mRenderNode.setAnimationMatrix(paramMatrix);
  }
  
  public void setVisibility(int paramInt) {
    super.setVisibility(paramInt);
    if (this.mView.mGhostView == this) {
      if (paramInt == 0) {
        paramInt = 4;
      } else {
        paramInt = 0;
      } 
      this.mView.setTransitionVisibility(paramInt);
    } 
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (!this.mBeingMoved) {
      this.mView.setTransitionVisibility(0);
      this.mView.mGhostView = null;
      ViewGroup viewGroup = (ViewGroup)this.mView.getParent();
      if (viewGroup != null)
        viewGroup.invalidate(); 
    } 
  }
  
  public static void calculateMatrix(View paramView, ViewGroup paramViewGroup, Matrix paramMatrix) {
    paramView = (ViewGroup)paramView.getParent();
    paramMatrix.reset();
    paramView.transformMatrixToGlobal(paramMatrix);
    paramMatrix.preTranslate(-paramView.getScrollX(), -paramView.getScrollY());
    paramViewGroup.transformMatrixToLocal(paramMatrix);
  }
  
  public static GhostView addGhost(View paramView, ViewGroup paramViewGroup, Matrix paramMatrix) {
    if (paramView.getParent() instanceof ViewGroup) {
      Matrix matrix1;
      GhostView ghostView1;
      Matrix matrix2;
      ViewGroupOverlay viewGroupOverlay = paramViewGroup.getOverlay();
      ViewOverlay.OverlayViewGroup overlayViewGroup = viewGroupOverlay.mOverlayViewGroup;
      GhostView ghostView2 = paramView.mGhostView;
      int i = 0;
      GhostView ghostView3 = ghostView2;
      int j = i;
      if (ghostView2 != null) {
        View view = (View)ghostView2.getParent();
        ViewGroup viewGroup = (ViewGroup)view.getParent();
        ghostView3 = ghostView2;
        j = i;
        if (viewGroup != overlayViewGroup) {
          j = ghostView2.mReferences;
          viewGroup.removeView(view);
          ghostView3 = null;
        } 
      } 
      if (ghostView3 == null) {
        matrix2 = paramMatrix;
        if (paramMatrix == null) {
          matrix2 = new Matrix();
          calculateMatrix(paramView, paramViewGroup, matrix2);
        } 
        ghostView1 = new GhostView(paramView);
        ghostView1.setMatrix(matrix2);
        paramView = new FrameLayout(paramView.getContext());
        paramView.setClipChildren(false);
        copySize(paramViewGroup, paramView);
        copySize(paramViewGroup, ghostView1);
        paramView.addView(ghostView1);
        ArrayList<View> arrayList = new ArrayList();
        i = moveGhostViewsToTop(viewGroupOverlay.mOverlayViewGroup, arrayList);
        insertIntoOverlay(viewGroupOverlay.mOverlayViewGroup, (ViewGroup)paramView, ghostView1, arrayList, i);
        ghostView1.mReferences = j;
        paramView = ghostView1;
      } else {
        matrix1 = matrix2;
        if (ghostView1 != null) {
          matrix2.setMatrix((Matrix)ghostView1);
          matrix1 = matrix2;
        } 
      } 
      ((GhostView)matrix1).mReferences++;
      return (GhostView)matrix1;
    } 
    throw new IllegalArgumentException("Ghosted views must be parented by a ViewGroup");
  }
  
  public static GhostView addGhost(View paramView, ViewGroup paramViewGroup) {
    return addGhost(paramView, paramViewGroup, (Matrix)null);
  }
  
  public static void removeGhost(View paramView) {
    paramView = paramView.mGhostView;
    if (paramView != null) {
      int i = ((GhostView)paramView).mReferences - 1;
      if (i == 0) {
        ViewGroup viewGroup = (ViewGroup)paramView.getParent();
        paramView = (ViewGroup)viewGroup.getParent();
        paramView.removeView(viewGroup);
      } 
    } 
  }
  
  public static GhostView getGhost(View paramView) {
    return paramView.mGhostView;
  }
  
  private static void copySize(View paramView1, View paramView2) {
    paramView2.setLeft(0);
    paramView2.setTop(0);
    paramView2.setRight(paramView1.getWidth());
    paramView2.setBottom(paramView1.getHeight());
  }
  
  private static int moveGhostViewsToTop(ViewGroup paramViewGroup, ArrayList<View> paramArrayList) {
    int i = paramViewGroup.getChildCount();
    if (i == 0)
      return -1; 
    if (isGhostWrapper(paramViewGroup.getChildAt(i - 1))) {
      int j = i - 1;
      for (i -= 2; i >= 0 && 
        isGhostWrapper(paramViewGroup.getChildAt(i)); i--)
        j = i; 
      return j;
    } 
    for (i -= 2; i >= 0; i--) {
      View view = paramViewGroup.getChildAt(i);
      if (isGhostWrapper(view)) {
        paramArrayList.add(view);
        view = ((ViewGroup)view).getChildAt(0);
        ((GhostView)view).mBeingMoved = true;
        paramViewGroup.removeViewAt(i);
        ((GhostView)view).mBeingMoved = false;
      } 
    } 
    if (paramArrayList.isEmpty()) {
      i = -1;
    } else {
      int j = paramViewGroup.getChildCount();
      for (i = paramArrayList.size() - 1; i >= 0; i--)
        paramViewGroup.addView(paramArrayList.get(i)); 
      paramArrayList.clear();
      i = j;
    } 
    return i;
  }
  
  private static void insertIntoOverlay(ViewGroup paramViewGroup1, ViewGroup paramViewGroup2, GhostView paramGhostView, ArrayList<View> paramArrayList, int paramInt) {
    if (paramInt == -1) {
      paramViewGroup1.addView(paramViewGroup2);
    } else {
      ArrayList<View> arrayList = new ArrayList();
      getParents(paramGhostView.mView, arrayList);
      paramInt = getInsertIndex(paramViewGroup1, arrayList, paramArrayList, paramInt);
      if (paramInt < 0 || paramInt >= paramViewGroup1.getChildCount()) {
        paramViewGroup1.addView(paramViewGroup2);
        return;
      } 
      paramViewGroup1.addView(paramViewGroup2, paramInt);
    } 
  }
  
  private static int getInsertIndex(ViewGroup paramViewGroup, ArrayList<View> paramArrayList1, ArrayList<View> paramArrayList2, int paramInt) {
    int i = paramInt;
    paramInt = paramViewGroup.getChildCount() - 1;
    while (i <= paramInt) {
      int j = (i + paramInt) / 2;
      ViewGroup viewGroup = (ViewGroup)paramViewGroup.getChildAt(j);
      GhostView ghostView = (GhostView)viewGroup.getChildAt(0);
      getParents(ghostView.mView, paramArrayList2);
      if (isOnTop(paramArrayList1, paramArrayList2)) {
        i = j + 1;
      } else {
        paramInt = j - 1;
      } 
      paramArrayList2.clear();
    } 
    return i;
  }
  
  private static boolean isGhostWrapper(View paramView) {
    if (paramView instanceof FrameLayout) {
      paramView = paramView;
      if (paramView.getChildCount() == 1) {
        paramView = paramView.getChildAt(0);
        return paramView instanceof GhostView;
      } 
    } 
    return false;
  }
  
  private static boolean isOnTop(ArrayList<View> paramArrayList1, ArrayList<View> paramArrayList2) {
    boolean bool = paramArrayList1.isEmpty();
    boolean bool1 = true;
    if (bool || paramArrayList2.isEmpty() || 
      paramArrayList1.get(0) != paramArrayList2.get(0))
      return true; 
    int i = Math.min(paramArrayList1.size(), paramArrayList2.size());
    for (byte b = 1; b < i; b++) {
      View view1 = paramArrayList1.get(b);
      View view2 = paramArrayList2.get(b);
      if (view1 != view2)
        return isOnTop(view1, view2); 
    } 
    if (paramArrayList2.size() != i)
      bool1 = false; 
    return bool1;
  }
  
  private static void getParents(View paramView, ArrayList<View> paramArrayList) {
    ViewParent viewParent = paramView.getParent();
    if (viewParent != null && viewParent instanceof ViewGroup)
      getParents((View)viewParent, paramArrayList); 
    paramArrayList.add(paramView);
  }
  
  private static boolean isOnTop(View paramView1, View paramView2) {
    boolean bool1, bool3;
    ViewGroup viewGroup = (ViewGroup)paramView1.getParent();
    int i = viewGroup.getChildCount();
    ArrayList<View> arrayList = viewGroup.buildOrderedChildList();
    if (arrayList == null && 
      viewGroup.isChildrenDrawingOrderEnabled()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    boolean bool2 = true;
    byte b = 0;
    while (true) {
      bool3 = bool2;
      if (b < i) {
        byte b1;
        View view;
        if (bool1) {
          b1 = viewGroup.getChildDrawingOrder(i, b);
        } else {
          b1 = b;
        } 
        if (arrayList == null) {
          view = viewGroup.getChildAt(b1);
        } else {
          view = arrayList.get(b1);
        } 
        if (view == paramView1) {
          bool3 = false;
          break;
        } 
        if (view == paramView2) {
          bool3 = true;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    if (arrayList != null)
      arrayList.clear(); 
    return bool3;
  }
}
