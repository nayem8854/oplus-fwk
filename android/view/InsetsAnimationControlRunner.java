package android.view;

public interface InsetsAnimationControlRunner {
  void cancel();
  
  default boolean controlsInternalType(int paramInt) {
    return InsetsState.toInternalType(getTypes()).contains(Integer.valueOf(paramInt));
  }
  
  WindowInsetsAnimation getAnimation();
  
  int getAnimationType();
  
  int getTypes();
}
