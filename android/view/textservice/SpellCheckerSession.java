package android.view.textservice;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.android.internal.textservice.ISpellCheckerSession;
import com.android.internal.textservice.ISpellCheckerSessionListener;
import com.android.internal.textservice.ITextServicesSessionListener;
import dalvik.system.CloseGuard;
import java.util.LinkedList;
import java.util.Queue;

public class SpellCheckerSession {
  private static final boolean DBG = false;
  
  private static final int MSG_ON_GET_SUGGESTION_MULTIPLE = 1;
  
  private static final int MSG_ON_GET_SUGGESTION_MULTIPLE_FOR_SENTENCE = 2;
  
  public static final String SERVICE_META_DATA = "android.view.textservice.scs";
  
  private static final String TAG = SpellCheckerSession.class.getSimpleName();
  
  private final CloseGuard mGuard = CloseGuard.get();
  
  private final Handler mHandler = (Handler)new Object(this);
  
  private final InternalListener mInternalListener;
  
  private final SpellCheckerInfo mSpellCheckerInfo;
  
  private final SpellCheckerSessionListener mSpellCheckerSessionListener;
  
  private final SpellCheckerSessionListenerImpl mSpellCheckerSessionListenerImpl;
  
  private final TextServicesManager mTextServicesManager;
  
  public SpellCheckerSession(SpellCheckerInfo paramSpellCheckerInfo, TextServicesManager paramTextServicesManager, SpellCheckerSessionListener paramSpellCheckerSessionListener) {
    if (paramSpellCheckerInfo != null && paramSpellCheckerSessionListener != null && paramTextServicesManager != null) {
      this.mSpellCheckerInfo = paramSpellCheckerInfo;
      this.mSpellCheckerSessionListenerImpl = new SpellCheckerSessionListenerImpl(this.mHandler);
      this.mInternalListener = new InternalListener(this.mSpellCheckerSessionListenerImpl);
      this.mTextServicesManager = paramTextServicesManager;
      this.mSpellCheckerSessionListener = paramSpellCheckerSessionListener;
      this.mGuard.open("finishSession");
      return;
    } 
    throw null;
  }
  
  public boolean isSessionDisconnected() {
    return this.mSpellCheckerSessionListenerImpl.isDisconnected();
  }
  
  public SpellCheckerInfo getSpellChecker() {
    return this.mSpellCheckerInfo;
  }
  
  public void cancel() {
    this.mSpellCheckerSessionListenerImpl.cancel();
  }
  
  public void close() {
    this.mGuard.close();
    this.mSpellCheckerSessionListenerImpl.close();
    this.mTextServicesManager.finishSpellCheckerService((ISpellCheckerSessionListener)this.mSpellCheckerSessionListenerImpl);
  }
  
  public void getSentenceSuggestions(TextInfo[] paramArrayOfTextInfo, int paramInt) {
    this.mSpellCheckerSessionListenerImpl.getSentenceSuggestionsMultiple(paramArrayOfTextInfo, paramInt);
  }
  
  @Deprecated
  public void getSuggestions(TextInfo paramTextInfo, int paramInt) {
    getSuggestions(new TextInfo[] { paramTextInfo }, paramInt, false);
  }
  
  @Deprecated
  public void getSuggestions(TextInfo[] paramArrayOfTextInfo, int paramInt, boolean paramBoolean) {
    this.mSpellCheckerSessionListenerImpl.getSuggestionsMultiple(paramArrayOfTextInfo, paramInt, paramBoolean);
  }
  
  private void handleOnGetSuggestionsMultiple(SuggestionsInfo[] paramArrayOfSuggestionsInfo) {
    this.mSpellCheckerSessionListener.onGetSuggestions(paramArrayOfSuggestionsInfo);
  }
  
  private void handleOnGetSentenceSuggestionsMultiple(SentenceSuggestionsInfo[] paramArrayOfSentenceSuggestionsInfo) {
    this.mSpellCheckerSessionListener.onGetSentenceSuggestions(paramArrayOfSentenceSuggestionsInfo);
  }
  
  class SpellCheckerSessionListenerImpl extends ISpellCheckerSessionListener.Stub {
    private static final int STATE_CLOSED_AFTER_CONNECTION = 2;
    
    private static final int STATE_CLOSED_BEFORE_CONNECTION = 3;
    
    private static final int STATE_CONNECTED = 1;
    
    private static final int STATE_WAIT_CONNECTION = 0;
    
    private static final int TASK_CANCEL = 1;
    
    private static final int TASK_CLOSE = 3;
    
    private static final int TASK_GET_SUGGESTIONS_MULTIPLE = 2;
    
    private static final int TASK_GET_SUGGESTIONS_MULTIPLE_FOR_SENTENCE = 4;
    
    private Handler mAsyncHandler;
    
    private Handler mHandler;
    
    private ISpellCheckerSession mISpellCheckerSession;
    
    private static String taskToString(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unexpected task=");
              stringBuilder.append(param1Int);
              return stringBuilder.toString();
            } 
            return "TASK_GET_SUGGESTIONS_MULTIPLE_FOR_SENTENCE";
          } 
          return "TASK_CLOSE";
        } 
        return "TASK_GET_SUGGESTIONS_MULTIPLE";
      } 
      return "TASK_CANCEL";
    }
    
    private final Queue<SpellCheckerParams> mPendingTasks = new LinkedList<>();
    
    private static String stateToString(int param1Int) {
      if (param1Int != 0) {
        if (param1Int != 1) {
          if (param1Int != 2) {
            if (param1Int != 3) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unexpected state=");
              stringBuilder.append(param1Int);
              return stringBuilder.toString();
            } 
            return "STATE_CLOSED_BEFORE_CONNECTION";
          } 
          return "STATE_CLOSED_AFTER_CONNECTION";
        } 
        return "STATE_CONNECTED";
      } 
      return "STATE_WAIT_CONNECTION";
    }
    
    private int mState = 0;
    
    private HandlerThread mThread;
    
    public SpellCheckerSessionListenerImpl(SpellCheckerSession this$0) {
      this.mHandler = (Handler)this$0;
    }
    
    class SpellCheckerParams {
      public final boolean mSequentialWords;
      
      public ISpellCheckerSession mSession;
      
      public final int mSuggestionsLimit;
      
      public final TextInfo[] mTextInfos;
      
      public final int mWhat;
      
      public SpellCheckerParams(SpellCheckerSession.SpellCheckerSessionListenerImpl this$0, TextInfo[] param2ArrayOfTextInfo, int param2Int1, boolean param2Boolean) {
        this.mWhat = this$0;
        this.mTextInfos = param2ArrayOfTextInfo;
        this.mSuggestionsLimit = param2Int1;
        this.mSequentialWords = param2Boolean;
      }
    }
    
    private void processTask(ISpellCheckerSession param1ISpellCheckerSession, SpellCheckerParams param1SpellCheckerParams, boolean param1Boolean) {
      // Byte code:
      //   0: iload_3
      //   1: ifne -> 38
      //   4: aload_0
      //   5: getfield mAsyncHandler : Landroid/os/Handler;
      //   8: ifnonnull -> 14
      //   11: goto -> 38
      //   14: aload_2
      //   15: aload_1
      //   16: putfield mSession : Lcom/android/internal/textservice/ISpellCheckerSession;
      //   19: aload_0
      //   20: getfield mAsyncHandler : Landroid/os/Handler;
      //   23: astore_1
      //   24: aload_1
      //   25: aload_1
      //   26: iconst_1
      //   27: aload_2
      //   28: invokestatic obtain : (Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
      //   31: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   34: pop
      //   35: goto -> 300
      //   38: aload_2
      //   39: getfield mWhat : I
      //   42: istore #4
      //   44: iload #4
      //   46: iconst_1
      //   47: if_icmpeq -> 250
      //   50: iload #4
      //   52: iconst_2
      //   53: if_icmpeq -> 185
      //   56: iload #4
      //   58: iconst_3
      //   59: if_icmpeq -> 132
      //   62: iload #4
      //   64: iconst_4
      //   65: if_icmpeq -> 71
      //   68: goto -> 300
      //   71: aload_1
      //   72: aload_2
      //   73: getfield mTextInfos : [Landroid/view/textservice/TextInfo;
      //   76: aload_2
      //   77: getfield mSuggestionsLimit : I
      //   80: invokeinterface onGetSentenceSuggestionsMultiple : ([Landroid/view/textservice/TextInfo;I)V
      //   85: goto -> 300
      //   88: astore_1
      //   89: invokestatic access$200 : ()Ljava/lang/String;
      //   92: astore #5
      //   94: new java/lang/StringBuilder
      //   97: dup
      //   98: invokespecial <init> : ()V
      //   101: astore #6
      //   103: aload #6
      //   105: ldc 'Failed to get suggestions '
      //   107: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   110: pop
      //   111: aload #6
      //   113: aload_1
      //   114: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   117: pop
      //   118: aload #5
      //   120: aload #6
      //   122: invokevirtual toString : ()Ljava/lang/String;
      //   125: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   128: pop
      //   129: goto -> 300
      //   132: aload_1
      //   133: invokeinterface onClose : ()V
      //   138: goto -> 300
      //   141: astore_1
      //   142: invokestatic access$200 : ()Ljava/lang/String;
      //   145: astore #5
      //   147: new java/lang/StringBuilder
      //   150: dup
      //   151: invokespecial <init> : ()V
      //   154: astore #6
      //   156: aload #6
      //   158: ldc 'Failed to close '
      //   160: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   163: pop
      //   164: aload #6
      //   166: aload_1
      //   167: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   170: pop
      //   171: aload #5
      //   173: aload #6
      //   175: invokevirtual toString : ()Ljava/lang/String;
      //   178: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   181: pop
      //   182: goto -> 300
      //   185: aload_1
      //   186: aload_2
      //   187: getfield mTextInfos : [Landroid/view/textservice/TextInfo;
      //   190: aload_2
      //   191: getfield mSuggestionsLimit : I
      //   194: aload_2
      //   195: getfield mSequentialWords : Z
      //   198: invokeinterface onGetSuggestionsMultiple : ([Landroid/view/textservice/TextInfo;IZ)V
      //   203: goto -> 300
      //   206: astore #6
      //   208: invokestatic access$200 : ()Ljava/lang/String;
      //   211: astore_1
      //   212: new java/lang/StringBuilder
      //   215: dup
      //   216: invokespecial <init> : ()V
      //   219: astore #5
      //   221: aload #5
      //   223: ldc 'Failed to get suggestions '
      //   225: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   228: pop
      //   229: aload #5
      //   231: aload #6
      //   233: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   236: pop
      //   237: aload_1
      //   238: aload #5
      //   240: invokevirtual toString : ()Ljava/lang/String;
      //   243: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   246: pop
      //   247: goto -> 300
      //   250: aload_1
      //   251: invokeinterface onCancel : ()V
      //   256: goto -> 300
      //   259: astore #5
      //   261: invokestatic access$200 : ()Ljava/lang/String;
      //   264: astore_1
      //   265: new java/lang/StringBuilder
      //   268: dup
      //   269: invokespecial <init> : ()V
      //   272: astore #6
      //   274: aload #6
      //   276: ldc 'Failed to cancel '
      //   278: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   281: pop
      //   282: aload #6
      //   284: aload #5
      //   286: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   289: pop
      //   290: aload_1
      //   291: aload #6
      //   293: invokevirtual toString : ()Ljava/lang/String;
      //   296: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   299: pop
      //   300: aload_2
      //   301: getfield mWhat : I
      //   304: iconst_3
      //   305: if_icmpne -> 324
      //   308: aload_0
      //   309: monitorenter
      //   310: aload_0
      //   311: invokespecial processCloseLocked : ()V
      //   314: aload_0
      //   315: monitorexit
      //   316: goto -> 324
      //   319: astore_1
      //   320: aload_0
      //   321: monitorexit
      //   322: aload_1
      //   323: athrow
      //   324: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #291	-> 0
      //   #327	-> 14
      //   #328	-> 19
      //   #292	-> 38
      //   #310	-> 71
      //   #314	-> 85
      //   #312	-> 88
      //   #313	-> 89
      //   #315	-> 129
      //   #318	-> 132
      //   #321	-> 138
      //   #319	-> 141
      //   #320	-> 142
      //   #302	-> 185
      //   #306	-> 203
      //   #304	-> 206
      //   #305	-> 208
      //   #307	-> 247
      //   #295	-> 250
      //   #298	-> 256
      //   #296	-> 259
      //   #297	-> 261
      //   #299	-> 300
      //   #322	-> 300
      //   #331	-> 300
      //   #334	-> 308
      //   #335	-> 310
      //   #336	-> 314
      //   #338	-> 324
      // Exception table:
      //   from	to	target	type
      //   71	85	88	android/os/RemoteException
      //   132	138	141	android/os/RemoteException
      //   185	203	206	android/os/RemoteException
      //   250	256	259	android/os/RemoteException
      //   310	314	319	finally
      //   314	316	319	finally
      //   320	322	319	finally
    }
    
    private void processCloseLocked() {
      this.mISpellCheckerSession = null;
      HandlerThread handlerThread = this.mThread;
      if (handlerThread != null)
        handlerThread.quit(); 
      this.mHandler = null;
      this.mPendingTasks.clear();
      this.mThread = null;
      this.mAsyncHandler = null;
      int i = this.mState;
      if (i != 0) {
        if (i != 1) {
          String str1 = SpellCheckerSession.TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("processCloseLocked is called unexpectedly. mState=");
          i = this.mState;
          stringBuilder.append(stateToString(i));
          String str2 = stringBuilder.toString();
          Log.e(str1, str2);
        } else {
          this.mState = 2;
        } 
      } else {
        this.mState = 3;
      } 
    }
    
    public void onServiceConnected(ISpellCheckerSession param1ISpellCheckerSession) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mState : I
      //   6: istore_2
      //   7: iload_2
      //   8: ifeq -> 66
      //   11: iload_2
      //   12: iconst_3
      //   13: if_icmpeq -> 63
      //   16: invokestatic access$200 : ()Ljava/lang/String;
      //   19: astore_1
      //   20: new java/lang/StringBuilder
      //   23: astore_3
      //   24: aload_3
      //   25: invokespecial <init> : ()V
      //   28: aload_3
      //   29: ldc 'ignoring onServiceConnected due to unexpected mState='
      //   31: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   34: pop
      //   35: aload_0
      //   36: getfield mState : I
      //   39: istore_2
      //   40: aload_3
      //   41: iload_2
      //   42: invokestatic stateToString : (I)Ljava/lang/String;
      //   45: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   48: pop
      //   49: aload_3
      //   50: invokevirtual toString : ()Ljava/lang/String;
      //   53: astore_3
      //   54: aload_1
      //   55: aload_3
      //   56: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   59: pop
      //   60: aload_0
      //   61: monitorexit
      //   62: return
      //   63: aload_0
      //   64: monitorexit
      //   65: return
      //   66: aload_1
      //   67: ifnonnull -> 82
      //   70: invokestatic access$200 : ()Ljava/lang/String;
      //   73: ldc 'ignoring onServiceConnected due to session=null'
      //   75: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   78: pop
      //   79: aload_0
      //   80: monitorexit
      //   81: return
      //   82: aload_0
      //   83: aload_1
      //   84: putfield mISpellCheckerSession : Lcom/android/internal/textservice/ISpellCheckerSession;
      //   87: aload_1
      //   88: invokeinterface asBinder : ()Landroid/os/IBinder;
      //   93: instanceof android/os/Binder
      //   96: ifeq -> 148
      //   99: aload_0
      //   100: getfield mThread : Landroid/os/HandlerThread;
      //   103: ifnonnull -> 148
      //   106: new android/os/HandlerThread
      //   109: astore_3
      //   110: aload_3
      //   111: ldc 'SpellCheckerSession'
      //   113: bipush #10
      //   115: invokespecial <init> : (Ljava/lang/String;I)V
      //   118: aload_0
      //   119: aload_3
      //   120: putfield mThread : Landroid/os/HandlerThread;
      //   123: aload_3
      //   124: invokevirtual start : ()V
      //   127: new android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$1
      //   130: astore_3
      //   131: aload_3
      //   132: aload_0
      //   133: aload_0
      //   134: getfield mThread : Landroid/os/HandlerThread;
      //   137: invokevirtual getLooper : ()Landroid/os/Looper;
      //   140: invokespecial <init> : (Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl;Landroid/os/Looper;)V
      //   143: aload_0
      //   144: aload_3
      //   145: putfield mAsyncHandler : Landroid/os/Handler;
      //   148: aload_0
      //   149: iconst_1
      //   150: putfield mState : I
      //   153: aload_0
      //   154: getfield mPendingTasks : Ljava/util/Queue;
      //   157: invokeinterface isEmpty : ()Z
      //   162: ifne -> 186
      //   165: aload_0
      //   166: aload_1
      //   167: aload_0
      //   168: getfield mPendingTasks : Ljava/util/Queue;
      //   171: invokeinterface poll : ()Ljava/lang/Object;
      //   176: checkcast android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$SpellCheckerParams
      //   179: iconst_0
      //   180: invokespecial processTask : (Lcom/android/internal/textservice/ISpellCheckerSession;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$SpellCheckerParams;Z)V
      //   183: goto -> 153
      //   186: aload_0
      //   187: monitorexit
      //   188: return
      //   189: astore_1
      //   190: aload_0
      //   191: monitorexit
      //   192: aload_1
      //   193: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #368	-> 0
      //   #369	-> 2
      //   #380	-> 16
      //   #381	-> 40
      //   #380	-> 54
      //   #382	-> 60
      //   #378	-> 63
      //   #372	-> 66
      //   #384	-> 66
      //   #385	-> 70
      //   #386	-> 79
      //   #388	-> 82
      //   #389	-> 87
      //   #393	-> 106
      //   #395	-> 123
      //   #396	-> 127
      //   #403	-> 148
      //   #409	-> 153
      //   #410	-> 165
      //   #412	-> 186
      //   #413	-> 188
      //   #412	-> 189
      // Exception table:
      //   from	to	target	type
      //   2	7	189	finally
      //   16	40	189	finally
      //   40	54	189	finally
      //   54	60	189	finally
      //   60	62	189	finally
      //   63	65	189	finally
      //   70	79	189	finally
      //   79	81	189	finally
      //   82	87	189	finally
      //   87	106	189	finally
      //   106	123	189	finally
      //   123	127	189	finally
      //   127	148	189	finally
      //   148	153	189	finally
      //   153	165	189	finally
      //   165	183	189	finally
      //   186	188	189	finally
      //   190	192	189	finally
    }
    
    public void cancel() {
      processOrEnqueueTask(new SpellCheckerParams(1, null, 0, false));
    }
    
    public void getSuggestionsMultiple(TextInfo[] param1ArrayOfTextInfo, int param1Int, boolean param1Boolean) {
      processOrEnqueueTask(new SpellCheckerParams(2, param1ArrayOfTextInfo, param1Int, param1Boolean));
    }
    
    public void getSentenceSuggestionsMultiple(TextInfo[] param1ArrayOfTextInfo, int param1Int) {
      processOrEnqueueTask(new SpellCheckerParams(4, param1ArrayOfTextInfo, param1Int, false));
    }
    
    public void close() {
      processOrEnqueueTask(new SpellCheckerParams(3, null, 0, false));
    }
    
    public boolean isDisconnected() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mState : I
      //   6: istore_1
      //   7: iconst_1
      //   8: istore_2
      //   9: iload_1
      //   10: iconst_1
      //   11: if_icmpeq -> 17
      //   14: goto -> 19
      //   17: iconst_0
      //   18: istore_2
      //   19: aload_0
      //   20: monitorexit
      //   21: iload_2
      //   22: ireturn
      //   23: astore_3
      //   24: aload_0
      //   25: monitorexit
      //   26: aload_3
      //   27: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #437	-> 0
      //   #438	-> 2
      //   #439	-> 23
      // Exception table:
      //   from	to	target	type
      //   2	7	23	finally
      //   19	21	23	finally
      //   24	26	23	finally
    }
    
    private void processOrEnqueueTask(SpellCheckerParams param1SpellCheckerParams) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_1
      //   3: getfield mWhat : I
      //   6: iconst_3
      //   7: if_icmpne -> 29
      //   10: aload_0
      //   11: getfield mState : I
      //   14: iconst_2
      //   15: if_icmpeq -> 26
      //   18: aload_0
      //   19: getfield mState : I
      //   22: iconst_3
      //   23: if_icmpne -> 29
      //   26: aload_0
      //   27: monitorexit
      //   28: return
      //   29: aload_0
      //   30: getfield mState : I
      //   33: ifeq -> 116
      //   36: aload_0
      //   37: getfield mState : I
      //   40: iconst_1
      //   41: if_icmpeq -> 116
      //   44: invokestatic access$200 : ()Ljava/lang/String;
      //   47: astore_2
      //   48: new java/lang/StringBuilder
      //   51: astore_3
      //   52: aload_3
      //   53: invokespecial <init> : ()V
      //   56: aload_3
      //   57: ldc 'ignoring processOrEnqueueTask due to unexpected mState='
      //   59: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   62: pop
      //   63: aload_0
      //   64: getfield mState : I
      //   67: istore #4
      //   69: aload_3
      //   70: iload #4
      //   72: invokestatic stateToString : (I)Ljava/lang/String;
      //   75: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   78: pop
      //   79: aload_3
      //   80: ldc ' scp.mWhat='
      //   82: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   85: pop
      //   86: aload_1
      //   87: getfield mWhat : I
      //   90: istore #4
      //   92: aload_3
      //   93: iload #4
      //   95: invokestatic taskToString : (I)Ljava/lang/String;
      //   98: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   101: pop
      //   102: aload_3
      //   103: invokevirtual toString : ()Ljava/lang/String;
      //   106: astore_1
      //   107: aload_2
      //   108: aload_1
      //   109: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   112: pop
      //   113: aload_0
      //   114: monitorexit
      //   115: return
      //   116: aload_0
      //   117: getfield mState : I
      //   120: ifne -> 219
      //   123: aload_1
      //   124: getfield mWhat : I
      //   127: iconst_3
      //   128: if_icmpne -> 138
      //   131: aload_0
      //   132: invokespecial processCloseLocked : ()V
      //   135: aload_0
      //   136: monitorexit
      //   137: return
      //   138: aconst_null
      //   139: astore_3
      //   140: aconst_null
      //   141: astore_2
      //   142: aload_1
      //   143: getfield mWhat : I
      //   146: iconst_1
      //   147: if_icmpne -> 190
      //   150: aload_2
      //   151: astore_3
      //   152: aload_0
      //   153: getfield mPendingTasks : Ljava/util/Queue;
      //   156: invokeinterface isEmpty : ()Z
      //   161: ifne -> 190
      //   164: aload_0
      //   165: getfield mPendingTasks : Ljava/util/Queue;
      //   168: invokeinterface poll : ()Ljava/lang/Object;
      //   173: checkcast android/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$SpellCheckerParams
      //   176: astore_3
      //   177: aload_3
      //   178: getfield mWhat : I
      //   181: iconst_3
      //   182: if_icmpne -> 187
      //   185: aload_3
      //   186: astore_2
      //   187: goto -> 150
      //   190: aload_0
      //   191: getfield mPendingTasks : Ljava/util/Queue;
      //   194: aload_1
      //   195: invokeinterface offer : (Ljava/lang/Object;)Z
      //   200: pop
      //   201: aload_3
      //   202: ifnull -> 216
      //   205: aload_0
      //   206: getfield mPendingTasks : Ljava/util/Queue;
      //   209: aload_3
      //   210: invokeinterface offer : (Ljava/lang/Object;)Z
      //   215: pop
      //   216: aload_0
      //   217: monitorexit
      //   218: return
      //   219: aload_0
      //   220: getfield mISpellCheckerSession : Lcom/android/internal/textservice/ISpellCheckerSession;
      //   223: astore_2
      //   224: aload_0
      //   225: monitorexit
      //   226: aload_0
      //   227: aload_2
      //   228: aload_1
      //   229: iconst_0
      //   230: invokespecial processTask : (Lcom/android/internal/textservice/ISpellCheckerSession;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListenerImpl$SpellCheckerParams;Z)V
      //   233: return
      //   234: astore_1
      //   235: aload_0
      //   236: monitorexit
      //   237: aload_1
      //   238: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #444	-> 0
      //   #445	-> 2
      //   #449	-> 26
      //   #451	-> 29
      //   #452	-> 44
      //   #453	-> 69
      //   #454	-> 92
      //   #452	-> 107
      //   #455	-> 113
      //   #458	-> 116
      //   #460	-> 123
      //   #461	-> 131
      //   #462	-> 135
      //   #465	-> 138
      //   #466	-> 142
      //   #468	-> 150
      //   #469	-> 164
      //   #470	-> 177
      //   #473	-> 185
      //   #475	-> 187
      //   #477	-> 190
      //   #478	-> 201
      //   #479	-> 205
      //   #484	-> 216
      //   #487	-> 219
      //   #488	-> 224
      //   #490	-> 226
      //   #491	-> 233
      //   #488	-> 234
      // Exception table:
      //   from	to	target	type
      //   2	26	234	finally
      //   26	28	234	finally
      //   29	44	234	finally
      //   44	69	234	finally
      //   69	92	234	finally
      //   92	107	234	finally
      //   107	113	234	finally
      //   113	115	234	finally
      //   116	123	234	finally
      //   123	131	234	finally
      //   131	135	234	finally
      //   135	137	234	finally
      //   142	150	234	finally
      //   152	164	234	finally
      //   164	177	234	finally
      //   177	185	234	finally
      //   190	201	234	finally
      //   205	216	234	finally
      //   216	218	234	finally
      //   219	224	234	finally
      //   224	226	234	finally
      //   235	237	234	finally
    }
    
    public void onGetSuggestions(SuggestionsInfo[] param1ArrayOfSuggestionsInfo) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mHandler : Landroid/os/Handler;
      //   6: ifnull -> 26
      //   9: aload_0
      //   10: getfield mHandler : Landroid/os/Handler;
      //   13: aload_0
      //   14: getfield mHandler : Landroid/os/Handler;
      //   17: iconst_1
      //   18: aload_1
      //   19: invokestatic obtain : (Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
      //   22: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   25: pop
      //   26: aload_0
      //   27: monitorexit
      //   28: return
      //   29: astore_1
      //   30: aload_0
      //   31: monitorexit
      //   32: aload_1
      //   33: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #495	-> 0
      //   #496	-> 2
      //   #497	-> 9
      //   #500	-> 26
      //   #501	-> 28
      //   #500	-> 29
      // Exception table:
      //   from	to	target	type
      //   2	9	29	finally
      //   9	26	29	finally
      //   26	28	29	finally
      //   30	32	29	finally
    }
    
    public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] param1ArrayOfSentenceSuggestionsInfo) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mHandler : Landroid/os/Handler;
      //   6: ifnull -> 26
      //   9: aload_0
      //   10: getfield mHandler : Landroid/os/Handler;
      //   13: aload_0
      //   14: getfield mHandler : Landroid/os/Handler;
      //   17: iconst_2
      //   18: aload_1
      //   19: invokestatic obtain : (Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
      //   22: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   25: pop
      //   26: aload_0
      //   27: monitorexit
      //   28: return
      //   29: astore_1
      //   30: aload_0
      //   31: monitorexit
      //   32: aload_1
      //   33: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #505	-> 0
      //   #506	-> 2
      //   #507	-> 9
      //   #510	-> 26
      //   #511	-> 28
      //   #510	-> 29
      // Exception table:
      //   from	to	target	type
      //   2	9	29	finally
      //   9	26	29	finally
      //   26	28	29	finally
      //   30	32	29	finally
    }
  }
  
  private static class SpellCheckerParams {
    public final boolean mSequentialWords;
    
    public ISpellCheckerSession mSession;
    
    public final int mSuggestionsLimit;
    
    public final TextInfo[] mTextInfos;
    
    public final int mWhat;
    
    public SpellCheckerParams(int param1Int1, TextInfo[] param1ArrayOfTextInfo, int param1Int2, boolean param1Boolean) {
      this.mWhat = param1Int1;
      this.mTextInfos = param1ArrayOfTextInfo;
      this.mSuggestionsLimit = param1Int2;
      this.mSequentialWords = param1Boolean;
    }
  }
  
  class InternalListener extends ITextServicesSessionListener.Stub {
    private final SpellCheckerSession.SpellCheckerSessionListenerImpl mParentSpellCheckerSessionListenerImpl;
    
    public InternalListener(SpellCheckerSession this$0) {
      this.mParentSpellCheckerSessionListenerImpl = (SpellCheckerSession.SpellCheckerSessionListenerImpl)this$0;
    }
    
    public void onServiceConnected(ISpellCheckerSession param1ISpellCheckerSession) {
      this.mParentSpellCheckerSessionListenerImpl.onServiceConnected(param1ISpellCheckerSession);
    }
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mGuard != null) {
        this.mGuard.warnIfOpen();
        close();
      } 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public ITextServicesSessionListener getTextServicesSessionListener() {
    return (ITextServicesSessionListener)this.mInternalListener;
  }
  
  public ISpellCheckerSessionListener getSpellCheckerSessionListener() {
    return (ISpellCheckerSessionListener)this.mSpellCheckerSessionListenerImpl;
  }
  
  public static interface SpellCheckerSessionListener {
    void onGetSentenceSuggestions(SentenceSuggestionsInfo[] param1ArrayOfSentenceSuggestionsInfo);
    
    void onGetSuggestions(SuggestionsInfo[] param1ArrayOfSuggestionsInfo);
  }
}
