package android.view.textservice;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public final class SentenceSuggestionsInfo implements Parcelable {
  public SentenceSuggestionsInfo(SuggestionsInfo[] paramArrayOfSuggestionsInfo, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    if (paramArrayOfSuggestionsInfo != null && paramArrayOfint1 != null && paramArrayOfint2 != null) {
      if (paramArrayOfSuggestionsInfo.length == paramArrayOfint1.length && paramArrayOfint1.length == paramArrayOfint2.length) {
        int i = paramArrayOfSuggestionsInfo.length;
        this.mSuggestionsInfos = Arrays.<SuggestionsInfo>copyOf(paramArrayOfSuggestionsInfo, i);
        this.mOffsets = Arrays.copyOf(paramArrayOfint1, i);
        this.mLengths = Arrays.copyOf(paramArrayOfint2, i);
        return;
      } 
      throw new IllegalArgumentException();
    } 
    throw null;
  }
  
  public SentenceSuggestionsInfo(Parcel paramParcel) {
    int i = paramParcel.readInt();
    SuggestionsInfo[] arrayOfSuggestionsInfo = new SuggestionsInfo[i];
    paramParcel.readTypedArray((Object[])arrayOfSuggestionsInfo, SuggestionsInfo.CREATOR);
    int[] arrayOfInt = new int[this.mSuggestionsInfos.length];
    paramParcel.readIntArray(arrayOfInt);
    this.mLengths = arrayOfInt = new int[this.mSuggestionsInfos.length];
    paramParcel.readIntArray(arrayOfInt);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = this.mSuggestionsInfos.length;
    paramParcel.writeInt(paramInt);
    paramParcel.writeTypedArray((Parcelable[])this.mSuggestionsInfos, 0);
    paramParcel.writeIntArray(this.mOffsets);
    paramParcel.writeIntArray(this.mLengths);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int getSuggestionsCount() {
    return this.mSuggestionsInfos.length;
  }
  
  public SuggestionsInfo getSuggestionsInfoAt(int paramInt) {
    if (paramInt >= 0) {
      SuggestionsInfo[] arrayOfSuggestionsInfo = this.mSuggestionsInfos;
      if (paramInt < arrayOfSuggestionsInfo.length)
        return arrayOfSuggestionsInfo[paramInt]; 
    } 
    return null;
  }
  
  public int getOffsetAt(int paramInt) {
    if (paramInt >= 0) {
      int[] arrayOfInt = this.mOffsets;
      if (paramInt < arrayOfInt.length)
        return arrayOfInt[paramInt]; 
    } 
    return -1;
  }
  
  public int getLengthAt(int paramInt) {
    if (paramInt >= 0) {
      int[] arrayOfInt = this.mLengths;
      if (paramInt < arrayOfInt.length)
        return arrayOfInt[paramInt]; 
    } 
    return -1;
  }
  
  public static final Parcelable.Creator<SentenceSuggestionsInfo> CREATOR = new Parcelable.Creator<SentenceSuggestionsInfo>() {
      public SentenceSuggestionsInfo createFromParcel(Parcel param1Parcel) {
        return new SentenceSuggestionsInfo(param1Parcel);
      }
      
      public SentenceSuggestionsInfo[] newArray(int param1Int) {
        return new SentenceSuggestionsInfo[param1Int];
      }
    };
  
  private final int[] mLengths;
  
  private final int[] mOffsets;
  
  private final SuggestionsInfo[] mSuggestionsInfos;
}
