package android.view.textservice;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.SpellCheckSpan;

public final class TextInfo implements Parcelable {
  public TextInfo(String paramString) {
    this(paramString, 0, getStringLengthOrZero(paramString), 0, 0);
  }
  
  public TextInfo(String paramString, int paramInt1, int paramInt2) {
    this(paramString, 0, getStringLengthOrZero(paramString), paramInt1, paramInt2);
  }
  
  private static int getStringLengthOrZero(String paramString) {
    int i;
    if (TextUtils.isEmpty(paramString)) {
      i = 0;
    } else {
      i = paramString.length();
    } 
    return i;
  }
  
  public TextInfo(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (!TextUtils.isEmpty(paramCharSequence)) {
      paramCharSequence = new SpannableStringBuilder(paramCharSequence, paramInt1, paramInt2);
      SpellCheckSpan[] arrayOfSpellCheckSpan = paramCharSequence.<SpellCheckSpan>getSpans(0, paramCharSequence.length(), SpellCheckSpan.class);
      for (paramInt1 = 0; paramInt1 < arrayOfSpellCheckSpan.length; paramInt1++)
        paramCharSequence.removeSpan(arrayOfSpellCheckSpan[paramInt1]); 
      this.mCharSequence = paramCharSequence;
      this.mCookie = paramInt3;
      this.mSequenceNumber = paramInt4;
      return;
    } 
    throw new IllegalArgumentException("charSequence is empty");
  }
  
  public TextInfo(Parcel paramParcel) {
    this.mCharSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mCookie = paramParcel.readInt();
    this.mSequenceNumber = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    TextUtils.writeToParcel(this.mCharSequence, paramParcel, paramInt);
    paramParcel.writeInt(this.mCookie);
    paramParcel.writeInt(this.mSequenceNumber);
  }
  
  public String getText() {
    CharSequence charSequence = this.mCharSequence;
    if (charSequence == null)
      return null; 
    return charSequence.toString();
  }
  
  public CharSequence getCharSequence() {
    return this.mCharSequence;
  }
  
  public int getCookie() {
    return this.mCookie;
  }
  
  public int getSequence() {
    return this.mSequenceNumber;
  }
  
  public static final Parcelable.Creator<TextInfo> CREATOR = new Parcelable.Creator<TextInfo>() {
      public TextInfo createFromParcel(Parcel param1Parcel) {
        return new TextInfo(param1Parcel);
      }
      
      public TextInfo[] newArray(int param1Int) {
        return new TextInfo[param1Int];
      }
    };
  
  private static final int DEFAULT_COOKIE = 0;
  
  private static final int DEFAULT_SEQUENCE_NUMBER = 0;
  
  private final CharSequence mCharSequence;
  
  private final int mCookie;
  
  private final int mSequenceNumber;
  
  public int describeContents() {
    return 0;
  }
}
