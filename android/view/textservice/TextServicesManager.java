package android.view.textservice;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.internal.textservice.ISpellCheckerSessionListener;
import com.android.internal.textservice.ITextServicesManager;
import com.android.internal.textservice.ITextServicesSessionListener;
import java.util.Locale;

public final class TextServicesManager {
  private static final boolean DBG = false;
  
  private static final String TAG = TextServicesManager.class.getSimpleName();
  
  @Deprecated
  private static TextServicesManager sInstance;
  
  private final ITextServicesManager mService;
  
  private final int mUserId;
  
  private TextServicesManager(int paramInt) throws ServiceManager.ServiceNotFoundException {
    IBinder iBinder = ServiceManager.getServiceOrThrow("textservices");
    this.mService = ITextServicesManager.Stub.asInterface(iBinder);
    this.mUserId = paramInt;
  }
  
  public static TextServicesManager createInstance(Context paramContext) throws ServiceManager.ServiceNotFoundException {
    return new TextServicesManager(paramContext.getUserId());
  }
  
  public static TextServicesManager getInstance() {
    // Byte code:
    //   0: ldc android/view/textservice/TextServicesManager
    //   2: monitorenter
    //   3: getstatic android/view/textservice/TextServicesManager.sInstance : Landroid/view/textservice/TextServicesManager;
    //   6: astore_0
    //   7: aload_0
    //   8: ifnonnull -> 41
    //   11: new android/view/textservice/TextServicesManager
    //   14: astore_0
    //   15: aload_0
    //   16: invokestatic myUserId : ()I
    //   19: invokespecial <init> : (I)V
    //   22: aload_0
    //   23: putstatic android/view/textservice/TextServicesManager.sInstance : Landroid/view/textservice/TextServicesManager;
    //   26: goto -> 41
    //   29: astore_0
    //   30: new java/lang/IllegalStateException
    //   33: astore_1
    //   34: aload_1
    //   35: aload_0
    //   36: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   39: aload_1
    //   40: athrow
    //   41: getstatic android/view/textservice/TextServicesManager.sInstance : Landroid/view/textservice/TextServicesManager;
    //   44: astore_0
    //   45: ldc android/view/textservice/TextServicesManager
    //   47: monitorexit
    //   48: aload_0
    //   49: areturn
    //   50: astore_0
    //   51: ldc android/view/textservice/TextServicesManager
    //   53: monitorexit
    //   54: aload_0
    //   55: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #113	-> 0
    //   #114	-> 3
    //   #116	-> 11
    //   #119	-> 26
    //   #117	-> 29
    //   #118	-> 30
    //   #121	-> 41
    //   #122	-> 50
    // Exception table:
    //   from	to	target	type
    //   3	7	50	finally
    //   11	26	29	android/os/ServiceManager$ServiceNotFoundException
    //   11	26	50	finally
    //   30	41	50	finally
    //   41	48	50	finally
    //   51	54	50	finally
  }
  
  private static String parseLanguageFromLocaleString(String paramString) {
    int i = paramString.indexOf('_');
    if (i < 0)
      return paramString; 
    return paramString.substring(0, i);
  }
  
  public SpellCheckerSession newSpellCheckerSession(Bundle paramBundle, Locale paramLocale, SpellCheckerSession.SpellCheckerSessionListener paramSpellCheckerSessionListener, boolean paramBoolean) {
    if (paramSpellCheckerSessionListener != null) {
      if (paramBoolean || paramLocale != null) {
        if (paramBoolean && !isSpellCheckerEnabled())
          return null; 
        try {
          SpellCheckerSubtype spellCheckerSubtype2;
          SpellCheckerInfo spellCheckerInfo = this.mService.getCurrentSpellChecker(this.mUserId, null);
          if (spellCheckerInfo == null)
            return null; 
          SpellCheckerSubtype spellCheckerSubtype1 = null;
          if (paramBoolean) {
            spellCheckerSubtype1 = getCurrentSpellCheckerSubtype(true);
            if (spellCheckerSubtype1 == null)
              return null; 
            spellCheckerSubtype2 = spellCheckerSubtype1;
            if (paramLocale != null) {
              String str = spellCheckerSubtype1.getLocale();
              str = parseLanguageFromLocaleString(str);
              if (str.length() < 2 || !paramLocale.getLanguage().equals(str))
                return null; 
              SpellCheckerSubtype spellCheckerSubtype = spellCheckerSubtype1;
            } 
          } else {
            String str = paramLocale.toString();
            byte b = 0;
            while (true) {
              spellCheckerSubtype2 = spellCheckerSubtype1;
              if (b < spellCheckerInfo.getSubtypeCount()) {
                spellCheckerSubtype2 = spellCheckerInfo.getSubtypeAt(b);
                String str1 = spellCheckerSubtype2.getLocale();
                String str2 = parseLanguageFromLocaleString(str1);
                if (str1.equals(str))
                  break; 
                SpellCheckerSubtype spellCheckerSubtype = spellCheckerSubtype1;
                if (str2.length() >= 2) {
                  spellCheckerSubtype = spellCheckerSubtype1;
                  if (paramLocale.getLanguage().equals(str2))
                    spellCheckerSubtype = spellCheckerSubtype2; 
                } 
                b++;
                spellCheckerSubtype1 = spellCheckerSubtype;
                continue;
              } 
              break;
            } 
          } 
          if (spellCheckerSubtype2 == null)
            return null; 
          SpellCheckerSession spellCheckerSession = new SpellCheckerSession(spellCheckerInfo, this, paramSpellCheckerSessionListener);
          try {
            ITextServicesManager iTextServicesManager = this.mService;
            int i = this.mUserId;
            String str2 = spellCheckerInfo.getId(), str1 = spellCheckerSubtype2.getLocale();
            ITextServicesSessionListener iTextServicesSessionListener = spellCheckerSession.getTextServicesSessionListener();
            ISpellCheckerSessionListener iSpellCheckerSessionListener = spellCheckerSession.getSpellCheckerSessionListener();
            iTextServicesManager.getSpellCheckerService(i, str2, str1, iTextServicesSessionListener, iSpellCheckerSessionListener, paramBundle);
            return spellCheckerSession;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          } 
        } catch (RemoteException remoteException) {
          return null;
        } 
      } 
      throw new IllegalArgumentException("Locale should not be null if you don't refer settings.");
    } 
    throw null;
  }
  
  public SpellCheckerInfo[] getEnabledSpellCheckers() {
    try {
      return this.mService.getEnabledSpellCheckers(this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public SpellCheckerInfo getCurrentSpellChecker() {
    try {
      return this.mService.getCurrentSpellChecker(this.mUserId, null);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public SpellCheckerSubtype getCurrentSpellCheckerSubtype(boolean paramBoolean) {
    try {
      return this.mService.getCurrentSpellCheckerSubtype(this.mUserId, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isSpellCheckerEnabled() {
    try {
      return this.mService.isSpellCheckerEnabled(this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  void finishSpellCheckerService(ISpellCheckerSessionListener paramISpellCheckerSessionListener) {
    try {
      this.mService.finishSpellCheckerService(this.mUserId, paramISpellCheckerSessionListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
