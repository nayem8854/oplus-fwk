package android.view.textservice;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.PrintWriterPrinter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;

public final class SpellCheckerInfo implements Parcelable {
  public static final Parcelable.Creator<SpellCheckerInfo> CREATOR;
  
  private static final String TAG = SpellCheckerInfo.class.getSimpleName();
  
  private final String mId;
  
  private final int mLabel;
  
  private final ResolveInfo mService;
  
  private final String mSettingsActivityName;
  
  private final ArrayList<SpellCheckerSubtype> mSubtypes = new ArrayList<>();
  
  public SpellCheckerInfo(Context paramContext, ResolveInfo paramResolveInfo) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_0
    //   5: new java/util/ArrayList
    //   8: dup
    //   9: invokespecial <init> : ()V
    //   12: putfield mSubtypes : Ljava/util/ArrayList;
    //   15: aload_0
    //   16: aload_2
    //   17: putfield mService : Landroid/content/pm/ResolveInfo;
    //   20: aload_2
    //   21: getfield serviceInfo : Landroid/content/pm/ServiceInfo;
    //   24: astore_3
    //   25: aload_0
    //   26: new android/content/ComponentName
    //   29: dup
    //   30: aload_3
    //   31: getfield packageName : Ljava/lang/String;
    //   34: aload_3
    //   35: getfield name : Ljava/lang/String;
    //   38: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   41: invokevirtual flattenToShortString : ()Ljava/lang/String;
    //   44: putfield mId : Ljava/lang/String;
    //   47: aload_1
    //   48: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   51: astore #4
    //   53: aconst_null
    //   54: astore_2
    //   55: aconst_null
    //   56: astore_1
    //   57: aload_3
    //   58: aload #4
    //   60: ldc 'android.view.textservice.scs'
    //   62: invokevirtual loadXmlMetaData : (Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    //   65: astore #5
    //   67: aload #5
    //   69: ifnull -> 543
    //   72: aload #5
    //   74: astore_1
    //   75: aload #5
    //   77: astore_2
    //   78: aload #4
    //   80: aload_3
    //   81: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   84: invokevirtual getResourcesForApplication : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    //   87: astore #4
    //   89: aload #5
    //   91: astore_1
    //   92: aload #5
    //   94: astore_2
    //   95: aload #5
    //   97: invokestatic asAttributeSet : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    //   100: astore #6
    //   102: aload #5
    //   104: astore_1
    //   105: aload #5
    //   107: astore_2
    //   108: aload #5
    //   110: invokeinterface next : ()I
    //   115: istore #7
    //   117: iload #7
    //   119: iconst_1
    //   120: if_icmpeq -> 132
    //   123: iload #7
    //   125: iconst_2
    //   126: if_icmpeq -> 132
    //   129: goto -> 102
    //   132: aload #5
    //   134: astore_1
    //   135: aload #5
    //   137: astore_2
    //   138: aload #5
    //   140: invokeinterface getName : ()Ljava/lang/String;
    //   145: astore #8
    //   147: aload #5
    //   149: astore_1
    //   150: aload #5
    //   152: astore_2
    //   153: ldc 'spell-checker'
    //   155: aload #8
    //   157: invokevirtual equals : (Ljava/lang/Object;)Z
    //   160: ifeq -> 510
    //   163: aload #5
    //   165: astore_1
    //   166: aload #5
    //   168: astore_2
    //   169: aload #4
    //   171: aload #6
    //   173: getstatic com/android/internal/R$styleable.SpellChecker : [I
    //   176: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   179: astore #9
    //   181: aload #5
    //   183: astore_1
    //   184: aload #5
    //   186: astore_2
    //   187: aload #9
    //   189: iconst_0
    //   190: iconst_0
    //   191: invokevirtual getResourceId : (II)I
    //   194: istore #10
    //   196: aload #5
    //   198: astore_1
    //   199: aload #5
    //   201: astore_2
    //   202: aload #9
    //   204: iconst_1
    //   205: invokevirtual getString : (I)Ljava/lang/String;
    //   208: astore #8
    //   210: aload #5
    //   212: astore_1
    //   213: aload #5
    //   215: astore_2
    //   216: aload #9
    //   218: invokevirtual recycle : ()V
    //   221: aload #5
    //   223: astore_1
    //   224: aload #5
    //   226: astore_2
    //   227: aload #5
    //   229: invokeinterface getDepth : ()I
    //   234: istore #7
    //   236: aload #5
    //   238: astore_1
    //   239: aload #5
    //   241: astore_2
    //   242: aload #5
    //   244: invokeinterface next : ()I
    //   249: istore #11
    //   251: iload #11
    //   253: iconst_3
    //   254: if_icmpne -> 275
    //   257: aload #5
    //   259: astore_1
    //   260: aload #5
    //   262: astore_2
    //   263: aload #5
    //   265: invokeinterface getDepth : ()I
    //   270: iload #7
    //   272: if_icmple -> 485
    //   275: iload #11
    //   277: iconst_1
    //   278: if_icmpeq -> 485
    //   281: iload #11
    //   283: iconst_2
    //   284: if_icmpne -> 482
    //   287: aload #5
    //   289: astore_1
    //   290: aload #5
    //   292: astore_2
    //   293: aload #5
    //   295: invokeinterface getName : ()Ljava/lang/String;
    //   300: astore #9
    //   302: aload #5
    //   304: astore_1
    //   305: aload #5
    //   307: astore_2
    //   308: ldc 'subtype'
    //   310: aload #9
    //   312: invokevirtual equals : (Ljava/lang/Object;)Z
    //   315: ifeq -> 449
    //   318: aload #5
    //   320: astore_1
    //   321: aload #5
    //   323: astore_2
    //   324: aload #4
    //   326: aload #6
    //   328: getstatic com/android/internal/R$styleable.SpellChecker_Subtype : [I
    //   331: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   334: astore #9
    //   336: aload #5
    //   338: astore_1
    //   339: aload #5
    //   341: astore_2
    //   342: new android/view/textservice/SpellCheckerSubtype
    //   345: astore #12
    //   347: aload #5
    //   349: astore_1
    //   350: aload #5
    //   352: astore_2
    //   353: aload #9
    //   355: iconst_0
    //   356: iconst_0
    //   357: invokevirtual getResourceId : (II)I
    //   360: istore #11
    //   362: aload #5
    //   364: astore_1
    //   365: aload #5
    //   367: astore_2
    //   368: aload #9
    //   370: iconst_1
    //   371: invokevirtual getString : (I)Ljava/lang/String;
    //   374: astore #13
    //   376: aload #5
    //   378: astore_1
    //   379: aload #5
    //   381: astore_2
    //   382: aload #9
    //   384: iconst_4
    //   385: invokevirtual getString : (I)Ljava/lang/String;
    //   388: astore #14
    //   390: aload #5
    //   392: astore_1
    //   393: aload #5
    //   395: astore_2
    //   396: aload #9
    //   398: iconst_2
    //   399: invokevirtual getString : (I)Ljava/lang/String;
    //   402: astore #15
    //   404: aload #5
    //   406: astore_1
    //   407: aload #5
    //   409: astore_2
    //   410: aload #12
    //   412: iload #11
    //   414: aload #13
    //   416: aload #14
    //   418: aload #15
    //   420: aload #9
    //   422: iconst_3
    //   423: iconst_0
    //   424: invokevirtual getInt : (II)I
    //   427: invokespecial <init> : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    //   430: aload #5
    //   432: astore_1
    //   433: aload #5
    //   435: astore_2
    //   436: aload_0
    //   437: getfield mSubtypes : Ljava/util/ArrayList;
    //   440: aload #12
    //   442: invokevirtual add : (Ljava/lang/Object;)Z
    //   445: pop
    //   446: goto -> 482
    //   449: aload #5
    //   451: astore_1
    //   452: aload #5
    //   454: astore_2
    //   455: new org/xmlpull/v1/XmlPullParserException
    //   458: astore #4
    //   460: aload #5
    //   462: astore_1
    //   463: aload #5
    //   465: astore_2
    //   466: aload #4
    //   468: ldc 'Meta-data in spell-checker does not start with subtype tag'
    //   470: invokespecial <init> : (Ljava/lang/String;)V
    //   473: aload #5
    //   475: astore_1
    //   476: aload #5
    //   478: astore_2
    //   479: aload #4
    //   481: athrow
    //   482: goto -> 236
    //   485: aload #5
    //   487: ifnull -> 497
    //   490: aload #5
    //   492: invokeinterface close : ()V
    //   497: aload_0
    //   498: iload #10
    //   500: putfield mLabel : I
    //   503: aload_0
    //   504: aload #8
    //   506: putfield mSettingsActivityName : Ljava/lang/String;
    //   509: return
    //   510: aload #5
    //   512: astore_1
    //   513: aload #5
    //   515: astore_2
    //   516: new org/xmlpull/v1/XmlPullParserException
    //   519: astore #4
    //   521: aload #5
    //   523: astore_1
    //   524: aload #5
    //   526: astore_2
    //   527: aload #4
    //   529: ldc 'Meta-data does not start with spell-checker tag'
    //   531: invokespecial <init> : (Ljava/lang/String;)V
    //   534: aload #5
    //   536: astore_1
    //   537: aload #5
    //   539: astore_2
    //   540: aload #4
    //   542: athrow
    //   543: aload #5
    //   545: astore_1
    //   546: aload #5
    //   548: astore_2
    //   549: new org/xmlpull/v1/XmlPullParserException
    //   552: astore #4
    //   554: aload #5
    //   556: astore_1
    //   557: aload #5
    //   559: astore_2
    //   560: aload #4
    //   562: ldc 'No android.view.textservice.scs meta-data'
    //   564: invokespecial <init> : (Ljava/lang/String;)V
    //   567: aload #5
    //   569: astore_1
    //   570: aload #5
    //   572: astore_2
    //   573: aload #4
    //   575: athrow
    //   576: astore_2
    //   577: goto -> 696
    //   580: astore #6
    //   582: aload_2
    //   583: astore_1
    //   584: getstatic android/view/textservice/SpellCheckerInfo.TAG : Ljava/lang/String;
    //   587: astore #4
    //   589: aload_2
    //   590: astore_1
    //   591: new java/lang/StringBuilder
    //   594: astore #5
    //   596: aload_2
    //   597: astore_1
    //   598: aload #5
    //   600: invokespecial <init> : ()V
    //   603: aload_2
    //   604: astore_1
    //   605: aload #5
    //   607: ldc 'Caught exception: '
    //   609: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   612: pop
    //   613: aload_2
    //   614: astore_1
    //   615: aload #5
    //   617: aload #6
    //   619: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   622: pop
    //   623: aload_2
    //   624: astore_1
    //   625: aload #4
    //   627: aload #5
    //   629: invokevirtual toString : ()Ljava/lang/String;
    //   632: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   635: pop
    //   636: aload_2
    //   637: astore_1
    //   638: new org/xmlpull/v1/XmlPullParserException
    //   641: astore #5
    //   643: aload_2
    //   644: astore_1
    //   645: new java/lang/StringBuilder
    //   648: astore #4
    //   650: aload_2
    //   651: astore_1
    //   652: aload #4
    //   654: invokespecial <init> : ()V
    //   657: aload_2
    //   658: astore_1
    //   659: aload #4
    //   661: ldc 'Unable to create context for: '
    //   663: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   666: pop
    //   667: aload_2
    //   668: astore_1
    //   669: aload #4
    //   671: aload_3
    //   672: getfield packageName : Ljava/lang/String;
    //   675: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   678: pop
    //   679: aload_2
    //   680: astore_1
    //   681: aload #5
    //   683: aload #4
    //   685: invokevirtual toString : ()Ljava/lang/String;
    //   688: invokespecial <init> : (Ljava/lang/String;)V
    //   691: aload_2
    //   692: astore_1
    //   693: aload #5
    //   695: athrow
    //   696: aload_1
    //   697: ifnull -> 706
    //   700: aload_1
    //   701: invokeinterface close : ()V
    //   706: aload_2
    //   707: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #67	-> 0
    //   #60	-> 4
    //   #68	-> 15
    //   #69	-> 20
    //   #70	-> 25
    //   #72	-> 47
    //   #73	-> 53
    //   #74	-> 53
    //   #76	-> 53
    //   #78	-> 57
    //   #79	-> 67
    //   #84	-> 72
    //   #85	-> 89
    //   #87	-> 102
    //   #91	-> 132
    //   #92	-> 147
    //   #97	-> 163
    //   #99	-> 181
    //   #100	-> 196
    //   #102	-> 210
    //   #104	-> 221
    //   #106	-> 236
    //   #108	-> 281
    //   #109	-> 287
    //   #110	-> 302
    //   #114	-> 318
    //   #116	-> 336
    //   #117	-> 347
    //   #119	-> 362
    //   #121	-> 376
    //   #123	-> 390
    //   #125	-> 404
    //   #127	-> 430
    //   #128	-> 446
    //   #111	-> 449
    //   #108	-> 482
    //   #106	-> 482
    //   #135	-> 485
    //   #137	-> 497
    //   #138	-> 503
    //   #139	-> 509
    //   #93	-> 510
    //   #80	-> 543
    //   #135	-> 576
    //   #130	-> 580
    //   #131	-> 582
    //   #132	-> 636
    //   #135	-> 696
    //   #136	-> 706
    // Exception table:
    //   from	to	target	type
    //   57	67	580	java/lang/Exception
    //   57	67	576	finally
    //   78	89	580	java/lang/Exception
    //   78	89	576	finally
    //   95	102	580	java/lang/Exception
    //   95	102	576	finally
    //   108	117	580	java/lang/Exception
    //   108	117	576	finally
    //   138	147	580	java/lang/Exception
    //   138	147	576	finally
    //   153	163	580	java/lang/Exception
    //   153	163	576	finally
    //   169	181	580	java/lang/Exception
    //   169	181	576	finally
    //   187	196	580	java/lang/Exception
    //   187	196	576	finally
    //   202	210	580	java/lang/Exception
    //   202	210	576	finally
    //   216	221	580	java/lang/Exception
    //   216	221	576	finally
    //   227	236	580	java/lang/Exception
    //   227	236	576	finally
    //   242	251	580	java/lang/Exception
    //   242	251	576	finally
    //   263	275	580	java/lang/Exception
    //   263	275	576	finally
    //   293	302	580	java/lang/Exception
    //   293	302	576	finally
    //   308	318	580	java/lang/Exception
    //   308	318	576	finally
    //   324	336	580	java/lang/Exception
    //   324	336	576	finally
    //   342	347	580	java/lang/Exception
    //   342	347	576	finally
    //   353	362	580	java/lang/Exception
    //   353	362	576	finally
    //   368	376	580	java/lang/Exception
    //   368	376	576	finally
    //   382	390	580	java/lang/Exception
    //   382	390	576	finally
    //   396	404	580	java/lang/Exception
    //   396	404	576	finally
    //   410	430	580	java/lang/Exception
    //   410	430	576	finally
    //   436	446	580	java/lang/Exception
    //   436	446	576	finally
    //   455	460	580	java/lang/Exception
    //   455	460	576	finally
    //   466	473	580	java/lang/Exception
    //   466	473	576	finally
    //   479	482	580	java/lang/Exception
    //   479	482	576	finally
    //   516	521	580	java/lang/Exception
    //   516	521	576	finally
    //   527	534	580	java/lang/Exception
    //   527	534	576	finally
    //   540	543	580	java/lang/Exception
    //   540	543	576	finally
    //   549	554	580	java/lang/Exception
    //   549	554	576	finally
    //   560	567	580	java/lang/Exception
    //   560	567	576	finally
    //   573	576	580	java/lang/Exception
    //   573	576	576	finally
    //   584	589	576	finally
    //   591	596	576	finally
    //   598	603	576	finally
    //   605	613	576	finally
    //   615	623	576	finally
    //   625	636	576	finally
    //   638	643	576	finally
    //   645	650	576	finally
    //   652	657	576	finally
    //   659	667	576	finally
    //   669	679	576	finally
    //   681	691	576	finally
    //   693	696	576	finally
  }
  
  public SpellCheckerInfo(Parcel paramParcel) {
    this.mLabel = paramParcel.readInt();
    this.mId = paramParcel.readString();
    this.mSettingsActivityName = paramParcel.readString();
    this.mService = (ResolveInfo)ResolveInfo.CREATOR.createFromParcel(paramParcel);
    paramParcel.readTypedList(this.mSubtypes, SpellCheckerSubtype.CREATOR);
  }
  
  public String getId() {
    return this.mId;
  }
  
  public ComponentName getComponent() {
    return new ComponentName(this.mService.serviceInfo.packageName, this.mService.serviceInfo.name);
  }
  
  public String getPackageName() {
    return this.mService.serviceInfo.packageName;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mLabel);
    paramParcel.writeString(this.mId);
    paramParcel.writeString(this.mSettingsActivityName);
    this.mService.writeToParcel(paramParcel, paramInt);
    paramParcel.writeTypedList(this.mSubtypes);
  }
  
  static {
    CREATOR = new Parcelable.Creator<SpellCheckerInfo>() {
        public SpellCheckerInfo createFromParcel(Parcel param1Parcel) {
          return new SpellCheckerInfo(param1Parcel);
        }
        
        public SpellCheckerInfo[] newArray(int param1Int) {
          return new SpellCheckerInfo[param1Int];
        }
      };
  }
  
  public CharSequence loadLabel(PackageManager paramPackageManager) {
    if (this.mLabel == 0 || paramPackageManager == null)
      return ""; 
    return paramPackageManager.getText(getPackageName(), this.mLabel, this.mService.serviceInfo.applicationInfo);
  }
  
  public Drawable loadIcon(PackageManager paramPackageManager) {
    return this.mService.loadIcon(paramPackageManager);
  }
  
  public ServiceInfo getServiceInfo() {
    return this.mService.serviceInfo;
  }
  
  public String getSettingsActivity() {
    return this.mSettingsActivityName;
  }
  
  public int getSubtypeCount() {
    return this.mSubtypes.size();
  }
  
  public SpellCheckerSubtype getSubtypeAt(int paramInt) {
    return this.mSubtypes.get(paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void dump(PrintWriter paramPrintWriter, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("mId=");
    stringBuilder.append(this.mId);
    paramPrintWriter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("mSettingsActivityName=");
    stringBuilder.append(this.mSettingsActivityName);
    paramPrintWriter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("Service:");
    paramPrintWriter.println(stringBuilder.toString());
    ResolveInfo resolveInfo = this.mService;
    PrintWriterPrinter printWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
    stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("  ");
    resolveInfo.dump(printWriterPrinter, stringBuilder.toString());
    int i = getSubtypeCount();
    for (byte b = 0; b < i; b++) {
      SpellCheckerSubtype spellCheckerSubtype = getSubtypeAt(b);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(paramString);
      stringBuilder2.append("  Subtype #");
      stringBuilder2.append(b);
      stringBuilder2.append(":");
      paramPrintWriter.println(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(paramString);
      stringBuilder2.append("    locale=");
      stringBuilder2.append(spellCheckerSubtype.getLocale());
      stringBuilder2.append(" languageTag=");
      stringBuilder2.append(spellCheckerSubtype.getLanguageTag());
      String str = stringBuilder2.toString();
      paramPrintWriter.println(str);
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("    extraValue=");
      stringBuilder1.append(spellCheckerSubtype.getExtraValue());
      paramPrintWriter.println(stringBuilder1.toString());
    } 
  }
}
