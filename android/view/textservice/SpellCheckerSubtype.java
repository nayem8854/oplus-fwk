package android.view.textservice;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Slog;
import com.android.internal.inputmethod.SubtypeLocaleUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public final class SpellCheckerSubtype implements Parcelable {
  public static final Parcelable.Creator<SpellCheckerSubtype> CREATOR;
  
  private static final String EXTRA_VALUE_KEY_VALUE_SEPARATOR = "=";
  
  private static final String EXTRA_VALUE_PAIR_SEPARATOR = ",";
  
  public static final int SUBTYPE_ID_NONE = 0;
  
  private static final String SUBTYPE_LANGUAGE_TAG_NONE = "";
  
  private static final String TAG = SpellCheckerSubtype.class.getSimpleName();
  
  private HashMap<String, String> mExtraValueHashMapCache;
  
  private final String mSubtypeExtraValue;
  
  private final int mSubtypeHashCode;
  
  private final int mSubtypeId;
  
  private final String mSubtypeLanguageTag;
  
  private final String mSubtypeLocale;
  
  private final int mSubtypeNameResId;
  
  public SpellCheckerSubtype(int paramInt1, String paramString1, String paramString2, String paramString3, int paramInt2) {
    this.mSubtypeNameResId = paramInt1;
    String str = "";
    if (paramString1 == null)
      paramString1 = ""; 
    this.mSubtypeLocale = paramString1;
    if (paramString2 != null) {
      paramString1 = paramString2;
    } else {
      paramString1 = "";
    } 
    this.mSubtypeLanguageTag = paramString1;
    paramString1 = str;
    if (paramString3 != null)
      paramString1 = paramString3; 
    this.mSubtypeExtraValue = paramString1;
    this.mSubtypeId = paramInt2;
    if (paramInt2 == 0)
      paramInt2 = hashCodeInternal(this.mSubtypeLocale, paramString1); 
    this.mSubtypeHashCode = paramInt2;
  }
  
  @Deprecated
  public SpellCheckerSubtype(int paramInt, String paramString1, String paramString2) {
    this(paramInt, paramString1, "", paramString2, 0);
  }
  
  SpellCheckerSubtype(Parcel paramParcel) {
    this.mSubtypeNameResId = paramParcel.readInt();
    String str1 = paramParcel.readString();
    String str2 = "";
    if (str1 == null)
      str1 = ""; 
    this.mSubtypeLocale = str1;
    str1 = paramParcel.readString();
    if (str1 == null)
      str1 = ""; 
    this.mSubtypeLanguageTag = str1;
    String str3 = paramParcel.readString();
    str1 = str2;
    if (str3 != null)
      str1 = str3; 
    this.mSubtypeExtraValue = str1;
    int i = paramParcel.readInt();
    if (i == 0)
      i = hashCodeInternal(this.mSubtypeLocale, this.mSubtypeExtraValue); 
    this.mSubtypeHashCode = i;
  }
  
  public int getNameResId() {
    return this.mSubtypeNameResId;
  }
  
  @Deprecated
  public String getLocale() {
    return this.mSubtypeLocale;
  }
  
  public String getLanguageTag() {
    return this.mSubtypeLanguageTag;
  }
  
  public String getExtraValue() {
    return this.mSubtypeExtraValue;
  }
  
  private HashMap<String, String> getExtraValueHashMap() {
    if (this.mExtraValueHashMapCache == null) {
      this.mExtraValueHashMapCache = new HashMap<>();
      String[] arrayOfString = this.mSubtypeExtraValue.split(",");
      int i = arrayOfString.length;
      for (byte b = 0; b < i; b++) {
        String[] arrayOfString1 = arrayOfString[b].split("=");
        if (arrayOfString1.length == 1) {
          this.mExtraValueHashMapCache.put(arrayOfString1[0], null);
        } else if (arrayOfString1.length > 1) {
          if (arrayOfString1.length > 2)
            Slog.w(TAG, "ExtraValue has two or more '='s"); 
          this.mExtraValueHashMapCache.put(arrayOfString1[0], arrayOfString1[1]);
        } 
      } 
    } 
    return this.mExtraValueHashMapCache;
  }
  
  public boolean containsExtraValueKey(String paramString) {
    return getExtraValueHashMap().containsKey(paramString);
  }
  
  public String getExtraValueOf(String paramString) {
    return getExtraValueHashMap().get(paramString);
  }
  
  public int hashCode() {
    return this.mSubtypeHashCode;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof SpellCheckerSubtype;
    boolean bool1 = false, bool2 = false;
    if (bool) {
      paramObject = paramObject;
      if (((SpellCheckerSubtype)paramObject).mSubtypeId != 0 || this.mSubtypeId != 0) {
        if (paramObject.hashCode() == hashCode())
          bool1 = true; 
        return bool1;
      } 
      if (paramObject.hashCode() == hashCode() && 
        paramObject.getNameResId() == getNameResId() && 
        paramObject.getLocale().equals(getLocale()) && 
        paramObject.getLanguageTag().equals(getLanguageTag()) && 
        paramObject.getExtraValue().equals(getExtraValue())) {
        bool1 = true;
      } else {
        bool1 = bool2;
      } 
      return bool1;
    } 
    return false;
  }
  
  public Locale getLocaleObject() {
    if (!TextUtils.isEmpty(this.mSubtypeLanguageTag))
      return Locale.forLanguageTag(this.mSubtypeLanguageTag); 
    return SubtypeLocaleUtils.constructLocaleFromString(this.mSubtypeLocale);
  }
  
  public CharSequence getDisplayName(Context paramContext, String paramString, ApplicationInfo paramApplicationInfo) {
    String str;
    Locale locale = getLocaleObject();
    if (locale != null) {
      str = locale.getDisplayName();
    } else {
      str = this.mSubtypeLocale;
    } 
    if (this.mSubtypeNameResId == 0)
      return str; 
    CharSequence charSequence = paramContext.getPackageManager().getText(paramString, this.mSubtypeNameResId, paramApplicationInfo);
    if (!TextUtils.isEmpty(charSequence))
      return String.format(charSequence.toString(), new Object[] { str }); 
    return str;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSubtypeNameResId);
    paramParcel.writeString(this.mSubtypeLocale);
    paramParcel.writeString(this.mSubtypeLanguageTag);
    paramParcel.writeString(this.mSubtypeExtraValue);
    paramParcel.writeInt(this.mSubtypeId);
  }
  
  static {
    CREATOR = new Parcelable.Creator<SpellCheckerSubtype>() {
        public SpellCheckerSubtype createFromParcel(Parcel param1Parcel) {
          return new SpellCheckerSubtype(param1Parcel);
        }
        
        public SpellCheckerSubtype[] newArray(int param1Int) {
          return new SpellCheckerSubtype[param1Int];
        }
      };
  }
  
  private static int hashCodeInternal(String paramString1, String paramString2) {
    return Arrays.hashCode(new Object[] { paramString1, paramString2 });
  }
  
  public static List<SpellCheckerSubtype> sort(Context paramContext, int paramInt, SpellCheckerInfo paramSpellCheckerInfo, List<SpellCheckerSubtype> paramList) {
    if (paramSpellCheckerInfo == null)
      return paramList; 
    HashSet<SpellCheckerSubtype> hashSet = new HashSet<>(paramList);
    ArrayList<SpellCheckerSubtype> arrayList = new ArrayList();
    int i = paramSpellCheckerInfo.getSubtypeCount();
    for (paramInt = 0; paramInt < i; paramInt++) {
      SpellCheckerSubtype spellCheckerSubtype = paramSpellCheckerInfo.getSubtypeAt(paramInt);
      if (hashSet.contains(spellCheckerSubtype)) {
        arrayList.add(spellCheckerSubtype);
        hashSet.remove(spellCheckerSubtype);
      } 
    } 
    for (SpellCheckerSubtype spellCheckerSubtype : hashSet)
      arrayList.add(spellCheckerSubtype); 
    return arrayList;
  }
}
