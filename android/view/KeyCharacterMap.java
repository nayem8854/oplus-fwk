package android.view;

import android.hardware.input.InputManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AndroidRuntimeException;
import android.util.SparseIntArray;
import java.text.Normalizer;

public class KeyCharacterMap implements Parcelable {
  private static final int ACCENT_ACUTE = 180;
  
  private static final int ACCENT_BREVE = 728;
  
  private static final int ACCENT_CARON = 711;
  
  private static final int ACCENT_CEDILLA = 184;
  
  private static final int ACCENT_CIRCUMFLEX = 710;
  
  private static final int ACCENT_CIRCUMFLEX_LEGACY = 94;
  
  private static final int ACCENT_COMMA_ABOVE = 8125;
  
  private static final int ACCENT_COMMA_ABOVE_RIGHT = 700;
  
  private static final int ACCENT_DOT_ABOVE = 729;
  
  private static final int ACCENT_DOT_BELOW = 46;
  
  private static final int ACCENT_DOUBLE_ACUTE = 733;
  
  private static final int ACCENT_GRAVE = 715;
  
  private static final int ACCENT_GRAVE_LEGACY = 96;
  
  private static final int ACCENT_HOOK_ABOVE = 704;
  
  private static final int ACCENT_HORN = 39;
  
  private static final int ACCENT_MACRON = 175;
  
  private static final int ACCENT_MACRON_BELOW = 717;
  
  private static final int ACCENT_OGONEK = 731;
  
  private static final int ACCENT_REVERSED_COMMA_ABOVE = 701;
  
  private static final int ACCENT_RING_ABOVE = 730;
  
  private static final int ACCENT_STROKE = 45;
  
  private static final int ACCENT_TILDE = 732;
  
  private static final int ACCENT_TILDE_LEGACY = 126;
  
  private static final int ACCENT_TURNED_COMMA_ABOVE = 699;
  
  private static final int ACCENT_UMLAUT = 168;
  
  private static final int ACCENT_VERTICAL_LINE_ABOVE = 712;
  
  private static final int ACCENT_VERTICAL_LINE_BELOW = 716;
  
  public static final int ALPHA = 3;
  
  @Deprecated
  public static final int BUILT_IN_KEYBOARD = 0;
  
  private static final int CHAR_SPACE = 32;
  
  public static final int COMBINING_ACCENT = -2147483648;
  
  public static final int COMBINING_ACCENT_MASK = 2147483647;
  
  public static final Parcelable.Creator<KeyCharacterMap> CREATOR;
  
  public static final int FULL = 4;
  
  public static final char HEX_INPUT = '';
  
  public static final int MODIFIER_BEHAVIOR_CHORDED = 0;
  
  public static final int MODIFIER_BEHAVIOR_CHORDED_OR_TOGGLED = 1;
  
  public static final int NUMERIC = 1;
  
  public static final char PICKER_DIALOG_INPUT = '';
  
  public static final int PREDICTIVE = 2;
  
  public static final int SPECIAL_FUNCTION = 5;
  
  public static final int VIRTUAL_KEYBOARD = -1;
  
  private static final SparseIntArray sAccentToCombining;
  
  private static final SparseIntArray sCombiningToAccent = new SparseIntArray();
  
  private static final StringBuilder sDeadKeyBuilder;
  
  private static final SparseIntArray sDeadKeyCache;
  
  private long mPtr;
  
  static {
    sAccentToCombining = new SparseIntArray();
    addCombining(768, 715);
    addCombining(769, 180);
    addCombining(770, 710);
    addCombining(771, 732);
    addCombining(772, 175);
    addCombining(774, 728);
    addCombining(775, 729);
    addCombining(776, 168);
    addCombining(777, 704);
    addCombining(778, 730);
    addCombining(779, 733);
    addCombining(780, 711);
    addCombining(781, 712);
    addCombining(786, 699);
    addCombining(787, 8125);
    addCombining(788, 701);
    addCombining(789, 700);
    addCombining(795, 39);
    addCombining(803, 46);
    addCombining(807, 184);
    addCombining(808, 731);
    addCombining(809, 716);
    addCombining(817, 717);
    addCombining(821, 45);
    sCombiningToAccent.append(832, 715);
    sCombiningToAccent.append(833, 180);
    sCombiningToAccent.append(835, 8125);
    sAccentToCombining.append(96, 768);
    sAccentToCombining.append(94, 770);
    sAccentToCombining.append(126, 771);
    sDeadKeyCache = new SparseIntArray();
    sDeadKeyBuilder = new StringBuilder();
    addDeadKey(45, 68, 272);
    addDeadKey(45, 71, 484);
    addDeadKey(45, 72, 294);
    addDeadKey(45, 73, 407);
    addDeadKey(45, 76, 321);
    addDeadKey(45, 79, 216);
    addDeadKey(45, 84, 358);
    addDeadKey(45, 100, 273);
    addDeadKey(45, 103, 485);
    addDeadKey(45, 104, 295);
    addDeadKey(45, 105, 616);
    addDeadKey(45, 108, 322);
    addDeadKey(45, 111, 248);
    addDeadKey(45, 116, 359);
    CREATOR = new Parcelable.Creator<KeyCharacterMap>() {
        public KeyCharacterMap createFromParcel(Parcel param1Parcel) {
          return new KeyCharacterMap(param1Parcel);
        }
        
        public KeyCharacterMap[] newArray(int param1Int) {
          return new KeyCharacterMap[param1Int];
        }
      };
  }
  
  private static void addCombining(int paramInt1, int paramInt2) {
    sCombiningToAccent.append(paramInt1, paramInt2);
    sAccentToCombining.append(paramInt2, paramInt1);
  }
  
  private static void addDeadKey(int paramInt1, int paramInt2, int paramInt3) {
    paramInt1 = sAccentToCombining.get(paramInt1);
    if (paramInt1 != 0) {
      sDeadKeyCache.put(paramInt1 << 16 | paramInt2, paramInt3);
      return;
    } 
    throw new IllegalStateException("Invalid dead key declaration.");
  }
  
  private KeyCharacterMap(Parcel paramParcel) {
    if (paramParcel != null) {
      long l = nativeReadFromParcel(paramParcel);
      if (l != 0L)
        return; 
      throw new RuntimeException("Could not read KeyCharacterMap from parcel.");
    } 
    throw new IllegalArgumentException("parcel must not be null");
  }
  
  private KeyCharacterMap(long paramLong) {
    this.mPtr = paramLong;
  }
  
  protected void finalize() throws Throwable {
    long l = this.mPtr;
    if (l != 0L) {
      nativeDispose(l);
      this.mPtr = 0L;
    } 
  }
  
  public static KeyCharacterMap load(int paramInt) {
    StringBuilder stringBuilder;
    InputManager inputManager = InputManager.getInstance();
    InputDevice inputDevice1 = inputManager.getInputDevice(paramInt);
    InputDevice inputDevice2 = inputDevice1;
    if (inputDevice1 == null) {
      inputDevice2 = inputManager.getInputDevice(-1);
      if (inputDevice2 == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Could not load key character map for device ");
        stringBuilder.append(paramInt);
        throw new UnavailableException(stringBuilder.toString());
      } 
    } 
    return stringBuilder.getKeyCharacterMap();
  }
  
  public int get(int paramInt1, int paramInt2) {
    paramInt2 = KeyEvent.normalizeMetaState(paramInt2);
    paramInt1 = nativeGetCharacter(this.mPtr, paramInt1, paramInt2);
    paramInt2 = sCombiningToAccent.get(paramInt1);
    if (paramInt2 != 0)
      return Integer.MIN_VALUE | paramInt2; 
    return paramInt1;
  }
  
  public FallbackAction getFallbackAction(int paramInt1, int paramInt2) {
    FallbackAction fallbackAction = FallbackAction.obtain();
    paramInt2 = KeyEvent.normalizeMetaState(paramInt2);
    if (nativeGetFallbackAction(this.mPtr, paramInt1, paramInt2, fallbackAction)) {
      fallbackAction.metaState = KeyEvent.normalizeMetaState(fallbackAction.metaState);
      return fallbackAction;
    } 
    fallbackAction.recycle();
    return null;
  }
  
  public char getNumber(int paramInt) {
    return nativeGetNumber(this.mPtr, paramInt);
  }
  
  public char getMatch(int paramInt, char[] paramArrayOfchar) {
    return getMatch(paramInt, paramArrayOfchar, 0);
  }
  
  public char getMatch(int paramInt1, char[] paramArrayOfchar, int paramInt2) {
    if (paramArrayOfchar != null) {
      paramInt2 = KeyEvent.normalizeMetaState(paramInt2);
      return nativeGetMatch(this.mPtr, paramInt1, paramArrayOfchar, paramInt2);
    } 
    throw new IllegalArgumentException("chars must not be null.");
  }
  
  public char getDisplayLabel(int paramInt) {
    return nativeGetDisplayLabel(this.mPtr, paramInt);
  }
  
  public static int getDeadChar(int paramInt1, int paramInt2) {
    if (paramInt2 == paramInt1 || 32 == paramInt2)
      return paramInt1; 
    int i = sAccentToCombining.get(paramInt1);
    boolean bool = false;
    if (i == 0)
      return 0; 
    int j = i << 16 | paramInt2;
    synchronized (sDeadKeyCache) {
      int k = sDeadKeyCache.get(j, -1);
      paramInt1 = k;
      if (k == -1) {
        sDeadKeyBuilder.setLength(0);
        sDeadKeyBuilder.append((char)paramInt2);
        sDeadKeyBuilder.append((char)i);
        String str = Normalizer.normalize(sDeadKeyBuilder, Normalizer.Form.NFC);
        if (str.codePointCount(0, str.length()) == 1) {
          paramInt1 = str.codePointAt(0);
        } else {
          paramInt1 = bool;
        } 
        sDeadKeyCache.put(j, paramInt1);
      } 
      return paramInt1;
    } 
  }
  
  @Deprecated
  class KeyData {
    public static final int META_LENGTH = 4;
    
    public char displayLabel;
    
    public char[] meta = new char[4];
    
    public char number;
  }
  
  @Deprecated
  public boolean getKeyData(int paramInt, KeyData paramKeyData) {
    if (paramKeyData.meta.length >= 4) {
      char c = nativeGetDisplayLabel(this.mPtr, paramInt);
      if (c == '\000')
        return false; 
      paramKeyData.displayLabel = c;
      paramKeyData.number = nativeGetNumber(this.mPtr, paramInt);
      paramKeyData.meta[0] = nativeGetCharacter(this.mPtr, paramInt, 0);
      paramKeyData.meta[1] = nativeGetCharacter(this.mPtr, paramInt, 1);
      paramKeyData.meta[2] = nativeGetCharacter(this.mPtr, paramInt, 2);
      paramKeyData.meta[3] = nativeGetCharacter(this.mPtr, paramInt, 3);
      return true;
    } 
    throw new IndexOutOfBoundsException("results.meta.length must be >= 4");
  }
  
  public KeyEvent[] getEvents(char[] paramArrayOfchar) {
    if (paramArrayOfchar != null)
      return nativeGetEvents(this.mPtr, paramArrayOfchar); 
    throw new IllegalArgumentException("chars must not be null.");
  }
  
  public boolean isPrintingKey(int paramInt) {
    paramInt = Character.getType(nativeGetDisplayLabel(this.mPtr, paramInt));
    switch (paramInt) {
      default:
        return true;
      case 12:
      case 13:
      case 14:
      case 15:
      case 16:
        break;
    } 
    return false;
  }
  
  public int getKeyboardType() {
    return nativeGetKeyboardType(this.mPtr);
  }
  
  public int getModifierBehavior() {
    int i = getKeyboardType();
    if (i != 4 && i != 5)
      return 1; 
    return 0;
  }
  
  public static boolean deviceHasKey(int paramInt) {
    return InputManager.getInstance().deviceHasKeys(new int[] { paramInt })[0];
  }
  
  public static boolean[] deviceHasKeys(int[] paramArrayOfint) {
    return InputManager.getInstance().deviceHasKeys(paramArrayOfint);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (paramParcel != null) {
      nativeWriteToParcel(this.mPtr, paramParcel);
      return;
    } 
    throw new IllegalArgumentException("parcel must not be null");
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static native void nativeDispose(long paramLong);
  
  private static native char nativeGetCharacter(long paramLong, int paramInt1, int paramInt2);
  
  private static native char nativeGetDisplayLabel(long paramLong, int paramInt);
  
  private static native KeyEvent[] nativeGetEvents(long paramLong, char[] paramArrayOfchar);
  
  private static native boolean nativeGetFallbackAction(long paramLong, int paramInt1, int paramInt2, FallbackAction paramFallbackAction);
  
  private static native int nativeGetKeyboardType(long paramLong);
  
  private static native char nativeGetMatch(long paramLong, int paramInt1, char[] paramArrayOfchar, int paramInt2);
  
  private static native char nativeGetNumber(long paramLong, int paramInt);
  
  private static native long nativeReadFromParcel(Parcel paramParcel);
  
  private static native void nativeWriteToParcel(long paramLong, Parcel paramParcel);
  
  public static class UnavailableException extends AndroidRuntimeException {
    public UnavailableException(String param1String) {
      super(param1String);
    }
  }
  
  class FallbackAction {
    private static final int MAX_RECYCLED = 10;
    
    private static FallbackAction sRecycleBin;
    
    private static final Object sRecycleLock = new Object();
    
    private static int sRecycledCount;
    
    public int keyCode;
    
    public int metaState;
    
    private FallbackAction next;
    
    public static FallbackAction obtain() {
      synchronized (sRecycleLock) {
        FallbackAction fallbackAction;
        if (sRecycleBin == null) {
          fallbackAction = new FallbackAction();
          this();
        } else {
          fallbackAction = sRecycleBin;
          sRecycleBin = fallbackAction.next;
          sRecycledCount--;
          fallbackAction.next = null;
        } 
        return fallbackAction;
      } 
    }
    
    public void recycle() {
      synchronized (sRecycleLock) {
        if (sRecycledCount < 10) {
          this.next = sRecycleBin;
          sRecycleBin = this;
          sRecycledCount++;
        } else {
          this.next = null;
        } 
        return;
      } 
    }
  }
}
