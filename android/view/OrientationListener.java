package android.view;

import android.content.Context;
import android.hardware.SensorListener;

@Deprecated
public abstract class OrientationListener implements SensorListener {
  public static final int ORIENTATION_UNKNOWN = -1;
  
  private OrientationEventListener mOrientationEventLis;
  
  public OrientationListener(Context paramContext) {
    this.mOrientationEventLis = new OrientationEventListenerInternal(paramContext);
  }
  
  public OrientationListener(Context paramContext, int paramInt) {
    this.mOrientationEventLis = new OrientationEventListenerInternal(paramContext, paramInt);
  }
  
  class OrientationEventListenerInternal extends OrientationEventListener {
    final OrientationListener this$0;
    
    OrientationEventListenerInternal(Context param1Context) {
      super(param1Context);
    }
    
    OrientationEventListenerInternal(Context param1Context, int param1Int) {
      super(param1Context, param1Int);
      registerListener(OrientationListener.this);
    }
    
    public void onOrientationChanged(int param1Int) {
      OrientationListener.this.onOrientationChanged(param1Int);
    }
  }
  
  public void enable() {
    this.mOrientationEventLis.enable();
  }
  
  public void disable() {
    this.mOrientationEventLis.disable();
  }
  
  public void onAccuracyChanged(int paramInt1, int paramInt2) {}
  
  public void onSensorChanged(int paramInt, float[] paramArrayOffloat) {}
  
  public abstract void onOrientationChanged(int paramInt);
}
