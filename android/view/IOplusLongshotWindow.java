package android.view;

import android.os.RemoteException;
import com.oplus.view.analysis.OplusWindowNode;
import java.io.FileDescriptor;
import java.util.List;

public interface IOplusLongshotWindow {
  OplusWindowNode longshotCollectWindow(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void longshotDump(FileDescriptor paramFileDescriptor, List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) throws RemoteException;
  
  void longshotInjectInput(InputEvent paramInputEvent, int paramInt) throws RemoteException;
  
  void longshotInjectInputBegin() throws RemoteException;
  
  void longshotInjectInputEnd() throws RemoteException;
  
  void longshotNotifyConnected(boolean paramBoolean) throws RemoteException;
  
  void screenshotDump(FileDescriptor paramFileDescriptor) throws RemoteException;
}
