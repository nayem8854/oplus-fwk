package android.view;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public abstract class OrientationEventListener {
  private SensorManager mSensorManager;
  
  private SensorEventListener mSensorEventListener;
  
  private Sensor mSensor;
  
  private int mRate;
  
  private int mOrientation = -1;
  
  private OrientationListener mOldListener;
  
  private boolean mEnabled = false;
  
  private static final boolean localLOGV = false;
  
  private static final String TAG = "OrientationEventListener";
  
  public static final int ORIENTATION_UNKNOWN = -1;
  
  private static final boolean DEBUG = false;
  
  public OrientationEventListener(Context paramContext) {
    this(paramContext, 3);
  }
  
  public OrientationEventListener(Context paramContext, int paramInt) {
    SensorManager sensorManager = (SensorManager)paramContext.getSystemService("sensor");
    this.mRate = paramInt;
    Sensor sensor = sensorManager.getDefaultSensor(1);
    if (sensor != null)
      this.mSensorEventListener = new SensorEventListenerImpl(); 
  }
  
  void registerListener(OrientationListener paramOrientationListener) {
    this.mOldListener = paramOrientationListener;
  }
  
  public void enable() {
    Sensor sensor = this.mSensor;
    if (sensor == null) {
      Log.w("OrientationEventListener", "Cannot detect sensors. Not enabled");
      return;
    } 
    if (!this.mEnabled) {
      this.mSensorManager.registerListener(this.mSensorEventListener, sensor, this.mRate);
      this.mEnabled = true;
    } 
  }
  
  public void disable() {
    if (this.mSensor == null) {
      Log.w("OrientationEventListener", "Cannot detect sensors. Invalid disable");
      return;
    } 
    if (this.mEnabled == true) {
      this.mSensorManager.unregisterListener(this.mSensorEventListener);
      this.mEnabled = false;
    } 
  }
  
  class SensorEventListenerImpl implements SensorEventListener {
    private static final int _DATA_X = 0;
    
    private static final int _DATA_Y = 1;
    
    private static final int _DATA_Z = 2;
    
    final OrientationEventListener this$0;
    
    public void onSensorChanged(SensorEvent param1SensorEvent) {
      float[] arrayOfFloat = param1SensorEvent.values;
      int i = -1;
      float f1 = -arrayOfFloat[0];
      float f2 = -arrayOfFloat[1];
      float f3 = -arrayOfFloat[2];
      if (4.0F * (f1 * f1 + f2 * f2) >= f3 * f3) {
        int j;
        f2 = (float)Math.atan2(-f2, f1);
        i = 90 - Math.round(f2 * 57.29578F);
        while (true) {
          j = i;
          if (i >= 360) {
            i -= 360;
            continue;
          } 
          break;
        } 
        while (true) {
          i = j;
          if (j < 0) {
            j += 360;
            continue;
          } 
          break;
        } 
      } 
      if (OrientationEventListener.this.mOldListener != null)
        OrientationEventListener.this.mOldListener.onSensorChanged(1, param1SensorEvent.values); 
      if (i != OrientationEventListener.this.mOrientation) {
        OrientationEventListener.access$102(OrientationEventListener.this, i);
        OrientationEventListener.this.onOrientationChanged(i);
      } 
    }
    
    public void onAccuracyChanged(Sensor param1Sensor, int param1Int) {}
  }
  
  public boolean canDetectOrientation() {
    boolean bool;
    if (this.mSensor != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public abstract void onOrientationChanged(int paramInt);
}
