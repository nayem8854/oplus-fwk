package android.view;

import android.os.SystemProperties;
import android.util.Log;

public class OplusScreenShotUtil {
  private static boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String PERSIST_KEY = "sys.oppo.screenshot";
  
  private static final String TAG = "OplusScreenShotUtil";
  
  public static void pauseDeliverPointerEvent() {
    SystemProperties.set("sys.oppo.screenshot", "1");
  }
  
  public static void resumeDeliverPointerEvent() {
    SystemProperties.set("sys.oppo.screenshot", "0");
  }
  
  public static boolean checkPauseDeliverPointer() {
    return SystemProperties.getBoolean("sys.oppo.screenshot", false);
  }
  
  public static void dealPausedDeliverPointer(MotionEvent paramMotionEvent, View paramView) {
    if (DEBUG)
      Log.d("OplusScreenShotUtil", "dealPausedDeliverPointer ------------------------"); 
    if (paramView != null) {
      paramMotionEvent.setAction(3);
      paramView.dispatchPointerEvent(paramMotionEvent);
    } 
  }
}
