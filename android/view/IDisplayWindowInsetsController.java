package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface IDisplayWindowInsetsController extends IInterface {
  void hideInsets(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void insetsChanged(InsetsState paramInsetsState) throws RemoteException;
  
  void insetsControlChanged(InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl) throws RemoteException;
  
  void showInsets(int paramInt, boolean paramBoolean) throws RemoteException;
  
  class Default implements IDisplayWindowInsetsController {
    public void insetsChanged(InsetsState param1InsetsState) throws RemoteException {}
    
    public void insetsControlChanged(InsetsState param1InsetsState, InsetsSourceControl[] param1ArrayOfInsetsSourceControl) throws RemoteException {}
    
    public void showInsets(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void hideInsets(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDisplayWindowInsetsController {
    private static final String DESCRIPTOR = "android.view.IDisplayWindowInsetsController";
    
    static final int TRANSACTION_hideInsets = 4;
    
    static final int TRANSACTION_insetsChanged = 1;
    
    static final int TRANSACTION_insetsControlChanged = 2;
    
    static final int TRANSACTION_showInsets = 3;
    
    public Stub() {
      attachInterface(this, "android.view.IDisplayWindowInsetsController");
    }
    
    public static IDisplayWindowInsetsController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IDisplayWindowInsetsController");
      if (iInterface != null && iInterface instanceof IDisplayWindowInsetsController)
        return (IDisplayWindowInsetsController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "hideInsets";
          } 
          return "showInsets";
        } 
        return "insetsControlChanged";
      } 
      return "insetsChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      InsetsSourceControl[] arrayOfInsetsSourceControl;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          boolean bool1 = false, bool2 = false;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.view.IDisplayWindowInsetsController");
              return true;
            } 
            param1Parcel1.enforceInterface("android.view.IDisplayWindowInsetsController");
            param1Int1 = param1Parcel1.readInt();
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            hideInsets(param1Int1, bool1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.IDisplayWindowInsetsController");
          param1Int1 = param1Parcel1.readInt();
          if (param1Parcel1.readInt() != 0)
            bool1 = true; 
          showInsets(param1Int1, bool1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IDisplayWindowInsetsController");
        if (param1Parcel1.readInt() != 0) {
          InsetsState insetsState = (InsetsState)InsetsState.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel2 = null;
        } 
        arrayOfInsetsSourceControl = (InsetsSourceControl[])param1Parcel1.createTypedArray(InsetsSourceControl.CREATOR);
        insetsControlChanged((InsetsState)param1Parcel2, arrayOfInsetsSourceControl);
        return true;
      } 
      arrayOfInsetsSourceControl.enforceInterface("android.view.IDisplayWindowInsetsController");
      if (arrayOfInsetsSourceControl.readInt() != 0) {
        InsetsState insetsState = (InsetsState)InsetsState.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
      } else {
        arrayOfInsetsSourceControl = null;
      } 
      insetsChanged((InsetsState)arrayOfInsetsSourceControl);
      return true;
    }
    
    private static class Proxy implements IDisplayWindowInsetsController {
      public static IDisplayWindowInsetsController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IDisplayWindowInsetsController";
      }
      
      public void insetsChanged(InsetsState param2InsetsState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDisplayWindowInsetsController");
          if (param2InsetsState != null) {
            parcel.writeInt(1);
            param2InsetsState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IDisplayWindowInsetsController.Stub.getDefaultImpl() != null) {
            IDisplayWindowInsetsController.Stub.getDefaultImpl().insetsChanged(param2InsetsState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void insetsControlChanged(InsetsState param2InsetsState, InsetsSourceControl[] param2ArrayOfInsetsSourceControl) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDisplayWindowInsetsController");
          if (param2InsetsState != null) {
            parcel.writeInt(1);
            param2InsetsState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeTypedArray((Parcelable[])param2ArrayOfInsetsSourceControl, 0);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IDisplayWindowInsetsController.Stub.getDefaultImpl() != null) {
            IDisplayWindowInsetsController.Stub.getDefaultImpl().insetsControlChanged(param2InsetsState, param2ArrayOfInsetsSourceControl);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showInsets(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IDisplayWindowInsetsController");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IDisplayWindowInsetsController.Stub.getDefaultImpl() != null) {
            IDisplayWindowInsetsController.Stub.getDefaultImpl().showInsets(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hideInsets(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IDisplayWindowInsetsController");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IDisplayWindowInsetsController.Stub.getDefaultImpl() != null) {
            IDisplayWindowInsetsController.Stub.getDefaultImpl().hideInsets(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDisplayWindowInsetsController param1IDisplayWindowInsetsController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDisplayWindowInsetsController != null) {
          Proxy.sDefaultImpl = param1IDisplayWindowInsetsController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDisplayWindowInsetsController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
