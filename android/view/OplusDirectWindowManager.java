package android.view;

import android.os.Parcel;
import android.os.RemoteException;
import com.oplus.direct.OplusDirectFindCmd;

public class OplusDirectWindowManager extends OplusBaseWindowManager implements IOplusDirectWindowManager {
  private static final String TAG = "OplusDirectWindowManager";
  
  public void directFindCmd(OplusDirectFindCmd paramOplusDirectFindCmd) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindowManager");
      if (paramOplusDirectFindCmd != null) {
        parcel.writeInt(1);
        paramOplusDirectFindCmd.writeToParcel(parcel, 0);
      } else {
        parcel.writeInt(0);
      } 
      this.mRemote.transact(10402, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
}
