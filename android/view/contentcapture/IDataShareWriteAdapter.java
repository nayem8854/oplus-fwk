package android.view.contentcapture;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IDataShareWriteAdapter extends IInterface {
  void error(int paramInt) throws RemoteException;
  
  void finish() throws RemoteException;
  
  void rejected() throws RemoteException;
  
  void write(ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  class Default implements IDataShareWriteAdapter {
    public void write(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {}
    
    public void error(int param1Int) throws RemoteException {}
    
    public void rejected() throws RemoteException {}
    
    public void finish() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDataShareWriteAdapter {
    private static final String DESCRIPTOR = "android.view.contentcapture.IDataShareWriteAdapter";
    
    static final int TRANSACTION_error = 2;
    
    static final int TRANSACTION_finish = 4;
    
    static final int TRANSACTION_rejected = 3;
    
    static final int TRANSACTION_write = 1;
    
    public Stub() {
      attachInterface(this, "android.view.contentcapture.IDataShareWriteAdapter");
    }
    
    public static IDataShareWriteAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.contentcapture.IDataShareWriteAdapter");
      if (iInterface != null && iInterface instanceof IDataShareWriteAdapter)
        return (IDataShareWriteAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "finish";
          } 
          return "rejected";
        } 
        return "error";
      } 
      return "write";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.view.contentcapture.IDataShareWriteAdapter");
              return true;
            } 
            param1Parcel1.enforceInterface("android.view.contentcapture.IDataShareWriteAdapter");
            finish();
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.contentcapture.IDataShareWriteAdapter");
          rejected();
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.contentcapture.IDataShareWriteAdapter");
        param1Int1 = param1Parcel1.readInt();
        error(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.contentcapture.IDataShareWriteAdapter");
      if (param1Parcel1.readInt() != 0) {
        ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      write((ParcelFileDescriptor)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IDataShareWriteAdapter {
      public static IDataShareWriteAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.contentcapture.IDataShareWriteAdapter";
      }
      
      public void write(ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.contentcapture.IDataShareWriteAdapter");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IDataShareWriteAdapter.Stub.getDefaultImpl() != null) {
            IDataShareWriteAdapter.Stub.getDefaultImpl().write(param2ParcelFileDescriptor);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void error(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.contentcapture.IDataShareWriteAdapter");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IDataShareWriteAdapter.Stub.getDefaultImpl() != null) {
            IDataShareWriteAdapter.Stub.getDefaultImpl().error(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void rejected() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.contentcapture.IDataShareWriteAdapter");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IDataShareWriteAdapter.Stub.getDefaultImpl() != null) {
            IDataShareWriteAdapter.Stub.getDefaultImpl().rejected();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void finish() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.contentcapture.IDataShareWriteAdapter");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IDataShareWriteAdapter.Stub.getDefaultImpl() != null) {
            IDataShareWriteAdapter.Stub.getDefaultImpl().finish();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDataShareWriteAdapter param1IDataShareWriteAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDataShareWriteAdapter != null) {
          Proxy.sDefaultImpl = param1IDataShareWriteAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDataShareWriteAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
