package android.view.contentcapture;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.internal.os.IResultReceiver;

public interface IContentCaptureManager extends IInterface {
  void finishSession(int paramInt) throws RemoteException;
  
  void getContentCaptureConditions(String paramString, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void getServiceComponentName(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void getServiceSettingsActivity(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void isContentCaptureFeatureEnabled(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void removeData(DataRemovalRequest paramDataRemovalRequest) throws RemoteException;
  
  void shareData(DataShareRequest paramDataShareRequest, IDataShareWriteAdapter paramIDataShareWriteAdapter) throws RemoteException;
  
  void startSession(IBinder paramIBinder, ComponentName paramComponentName, int paramInt1, int paramInt2, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  class Default implements IContentCaptureManager {
    public void startSession(IBinder param1IBinder, ComponentName param1ComponentName, int param1Int1, int param1Int2, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void finishSession(int param1Int) throws RemoteException {}
    
    public void getServiceComponentName(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void removeData(DataRemovalRequest param1DataRemovalRequest) throws RemoteException {}
    
    public void shareData(DataShareRequest param1DataShareRequest, IDataShareWriteAdapter param1IDataShareWriteAdapter) throws RemoteException {}
    
    public void isContentCaptureFeatureEnabled(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void getServiceSettingsActivity(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void getContentCaptureConditions(String param1String, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IContentCaptureManager {
    private static final String DESCRIPTOR = "android.view.contentcapture.IContentCaptureManager";
    
    static final int TRANSACTION_finishSession = 2;
    
    static final int TRANSACTION_getContentCaptureConditions = 8;
    
    static final int TRANSACTION_getServiceComponentName = 3;
    
    static final int TRANSACTION_getServiceSettingsActivity = 7;
    
    static final int TRANSACTION_isContentCaptureFeatureEnabled = 6;
    
    static final int TRANSACTION_removeData = 4;
    
    static final int TRANSACTION_shareData = 5;
    
    static final int TRANSACTION_startSession = 1;
    
    public Stub() {
      attachInterface(this, "android.view.contentcapture.IContentCaptureManager");
    }
    
    public static IContentCaptureManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.contentcapture.IContentCaptureManager");
      if (iInterface != null && iInterface instanceof IContentCaptureManager)
        return (IContentCaptureManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "getContentCaptureConditions";
        case 7:
          return "getServiceSettingsActivity";
        case 6:
          return "isContentCaptureFeatureEnabled";
        case 5:
          return "shareData";
        case 4:
          return "removeData";
        case 3:
          return "getServiceComponentName";
        case 2:
          return "finishSession";
        case 1:
          break;
      } 
      return "startSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        IResultReceiver iResultReceiver2;
        IDataShareWriteAdapter iDataShareWriteAdapter;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.view.contentcapture.IContentCaptureManager");
            str = param1Parcel1.readString();
            iResultReceiver2 = IResultReceiver.Stub.asInterface(param1Parcel1.readStrongBinder());
            getContentCaptureConditions(str, iResultReceiver2);
            return true;
          case 7:
            iResultReceiver2.enforceInterface("android.view.contentcapture.IContentCaptureManager");
            iResultReceiver2 = IResultReceiver.Stub.asInterface(iResultReceiver2.readStrongBinder());
            getServiceSettingsActivity(iResultReceiver2);
            return true;
          case 6:
            iResultReceiver2.enforceInterface("android.view.contentcapture.IContentCaptureManager");
            iResultReceiver2 = IResultReceiver.Stub.asInterface(iResultReceiver2.readStrongBinder());
            isContentCaptureFeatureEnabled(iResultReceiver2);
            return true;
          case 5:
            iResultReceiver2.enforceInterface("android.view.contentcapture.IContentCaptureManager");
            if (iResultReceiver2.readInt() != 0) {
              DataShareRequest dataShareRequest = (DataShareRequest)DataShareRequest.CREATOR.createFromParcel((Parcel)iResultReceiver2);
            } else {
              str = null;
            } 
            iDataShareWriteAdapter = IDataShareWriteAdapter.Stub.asInterface(iResultReceiver2.readStrongBinder());
            shareData((DataShareRequest)str, iDataShareWriteAdapter);
            return true;
          case 4:
            iDataShareWriteAdapter.enforceInterface("android.view.contentcapture.IContentCaptureManager");
            if (iDataShareWriteAdapter.readInt() != 0) {
              DataRemovalRequest dataRemovalRequest = (DataRemovalRequest)DataRemovalRequest.CREATOR.createFromParcel((Parcel)iDataShareWriteAdapter);
            } else {
              iDataShareWriteAdapter = null;
            } 
            removeData((DataRemovalRequest)iDataShareWriteAdapter);
            return true;
          case 3:
            iDataShareWriteAdapter.enforceInterface("android.view.contentcapture.IContentCaptureManager");
            iResultReceiver1 = IResultReceiver.Stub.asInterface(iDataShareWriteAdapter.readStrongBinder());
            getServiceComponentName(iResultReceiver1);
            return true;
          case 2:
            iResultReceiver1.enforceInterface("android.view.contentcapture.IContentCaptureManager");
            param1Int1 = iResultReceiver1.readInt();
            finishSession(param1Int1);
            return true;
          case 1:
            break;
        } 
        iResultReceiver1.enforceInterface("android.view.contentcapture.IContentCaptureManager");
        IBinder iBinder = iResultReceiver1.readStrongBinder();
        if (iResultReceiver1.readInt() != 0) {
          ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iResultReceiver1);
        } else {
          str = null;
        } 
        param1Int1 = iResultReceiver1.readInt();
        param1Int2 = iResultReceiver1.readInt();
        IResultReceiver iResultReceiver1 = IResultReceiver.Stub.asInterface(iResultReceiver1.readStrongBinder());
        startSession(iBinder, (ComponentName)str, param1Int1, param1Int2, iResultReceiver1);
        return true;
      } 
      str.writeString("android.view.contentcapture.IContentCaptureManager");
      return true;
    }
    
    private static class Proxy implements IContentCaptureManager {
      public static IContentCaptureManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.contentcapture.IContentCaptureManager";
      }
      
      public void startSession(IBinder param2IBinder, ComponentName param2ComponentName, int param2Int1, int param2Int2, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureManager");
          parcel.writeStrongBinder(param2IBinder);
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IContentCaptureManager.Stub.getDefaultImpl() != null) {
            IContentCaptureManager.Stub.getDefaultImpl().startSession(param2IBinder, param2ComponentName, param2Int1, param2Int2, param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void finishSession(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureManager");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IContentCaptureManager.Stub.getDefaultImpl() != null) {
            IContentCaptureManager.Stub.getDefaultImpl().finishSession(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getServiceComponentName(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IContentCaptureManager.Stub.getDefaultImpl() != null) {
            IContentCaptureManager.Stub.getDefaultImpl().getServiceComponentName(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeData(DataRemovalRequest param2DataRemovalRequest) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureManager");
          if (param2DataRemovalRequest != null) {
            parcel.writeInt(1);
            param2DataRemovalRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IContentCaptureManager.Stub.getDefaultImpl() != null) {
            IContentCaptureManager.Stub.getDefaultImpl().removeData(param2DataRemovalRequest);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void shareData(DataShareRequest param2DataShareRequest, IDataShareWriteAdapter param2IDataShareWriteAdapter) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureManager");
          if (param2DataShareRequest != null) {
            parcel.writeInt(1);
            param2DataShareRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IDataShareWriteAdapter != null) {
            iBinder = param2IDataShareWriteAdapter.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IContentCaptureManager.Stub.getDefaultImpl() != null) {
            IContentCaptureManager.Stub.getDefaultImpl().shareData(param2DataShareRequest, param2IDataShareWriteAdapter);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void isContentCaptureFeatureEnabled(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IContentCaptureManager.Stub.getDefaultImpl() != null) {
            IContentCaptureManager.Stub.getDefaultImpl().isContentCaptureFeatureEnabled(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getServiceSettingsActivity(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IContentCaptureManager.Stub.getDefaultImpl() != null) {
            IContentCaptureManager.Stub.getDefaultImpl().getServiceSettingsActivity(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getContentCaptureConditions(String param2String, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureManager");
          parcel.writeString(param2String);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IContentCaptureManager.Stub.getDefaultImpl() != null) {
            IContentCaptureManager.Stub.getDefaultImpl().getContentCaptureConditions(param2String, param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IContentCaptureManager param1IContentCaptureManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IContentCaptureManager != null) {
          Proxy.sDefaultImpl = param1IContentCaptureManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IContentCaptureManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
