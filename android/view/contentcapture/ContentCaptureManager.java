package android.view.contentcapture;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.ContentCaptureOptions;
import android.content.Context;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.util.Slog;
import android.view.WindowManager;
import com.android.internal.os.IResultReceiver;
import com.android.internal.util.Preconditions;
import com.android.internal.util.SyncResultReceiver;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public final class ContentCaptureManager {
  public static final int DATA_SHARE_ERROR_CONCURRENT_REQUEST = 2;
  
  public static final int DATA_SHARE_ERROR_TIMEOUT_INTERRUPTED = 3;
  
  public static final int DATA_SHARE_ERROR_UNKNOWN = 1;
  
  public static final int DEFAULT_IDLE_FLUSHING_FREQUENCY_MS = 5000;
  
  public static final int DEFAULT_LOG_HISTORY_SIZE = 10;
  
  public static final int DEFAULT_MAX_BUFFER_SIZE = 500;
  
  public static final int DEFAULT_TEXT_CHANGE_FLUSHING_FREQUENCY_MS = 1000;
  
  public static final String DEVICE_CONFIG_PROPERTY_IDLE_FLUSH_FREQUENCY = "idle_flush_frequency";
  
  public static final String DEVICE_CONFIG_PROPERTY_IDLE_UNBIND_TIMEOUT = "idle_unbind_timeout";
  
  public static final String DEVICE_CONFIG_PROPERTY_LOGGING_LEVEL = "logging_level";
  
  public static final String DEVICE_CONFIG_PROPERTY_LOG_HISTORY_SIZE = "log_history_size";
  
  public static final String DEVICE_CONFIG_PROPERTY_MAX_BUFFER_SIZE = "max_buffer_size";
  
  public static final String DEVICE_CONFIG_PROPERTY_SERVICE_EXPLICITLY_ENABLED = "service_explicitly_enabled";
  
  public static final String DEVICE_CONFIG_PROPERTY_TEXT_CHANGE_FLUSH_FREQUENCY = "text_change_flush_frequency";
  
  public static final int LOGGING_LEVEL_DEBUG = 1;
  
  public static final int LOGGING_LEVEL_OFF = 0;
  
  public static final int LOGGING_LEVEL_VERBOSE = 2;
  
  @SystemApi
  public static final int NO_SESSION_ID = 0;
  
  public static final int RESULT_CODE_FALSE = 2;
  
  public static final int RESULT_CODE_OK = 0;
  
  public static final int RESULT_CODE_SECURITY_EXCEPTION = -1;
  
  public static final int RESULT_CODE_TRUE = 1;
  
  private static final int SYNC_CALLS_TIMEOUT_MS = 5000;
  
  private static final String TAG = ContentCaptureManager.class.getSimpleName();
  
  private final Context mContext;
  
  private final LocalDataShareAdapterResourceManager mDataShareAdapterResourceManager;
  
  private int mFlags;
  
  private final Handler mHandler;
  
  private final Object mLock = new Object();
  
  private MainContentCaptureSession mMainSession;
  
  final ContentCaptureOptions mOptions;
  
  private final IContentCaptureManager mService;
  
  public ContentCaptureManager(Context paramContext, IContentCaptureManager paramIContentCaptureManager, ContentCaptureOptions paramContentCaptureOptions) {
    this.mContext = (Context)Preconditions.checkNotNull(paramContext, "context cannot be null");
    this.mService = (IContentCaptureManager)Preconditions.checkNotNull(paramIContentCaptureManager, "service cannot be null");
    ContentCaptureOptions contentCaptureOptions = (ContentCaptureOptions)Preconditions.checkNotNull(paramContentCaptureOptions, "options cannot be null");
    ContentCaptureHelper.setLoggingLevel(contentCaptureOptions.loggingLevel);
    if (ContentCaptureHelper.sVerbose) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Constructor for ");
      stringBuilder.append(paramContext.getPackageName());
      Log.v(str, stringBuilder.toString());
    } 
    this.mHandler = Handler.createAsync(Looper.getMainLooper());
    this.mDataShareAdapterResourceManager = new LocalDataShareAdapterResourceManager();
  }
  
  public MainContentCaptureSession getMainContentCaptureSession() {
    synchronized (this.mLock) {
      if (this.mMainSession == null) {
        MainContentCaptureSession mainContentCaptureSession = new MainContentCaptureSession();
        this(this.mContext, this, this.mHandler, this.mService);
        this.mMainSession = mainContentCaptureSession;
        if (ContentCaptureHelper.sVerbose) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("getMainContentCaptureSession(): created ");
          stringBuilder.append(this.mMainSession);
          Log.v(str, stringBuilder.toString());
        } 
      } 
      return this.mMainSession;
    } 
  }
  
  public void onActivityCreated(IBinder paramIBinder, ComponentName paramComponentName) {
    if (this.mOptions.lite)
      return; 
    synchronized (this.mLock) {
      getMainContentCaptureSession().start(paramIBinder, paramComponentName, this.mFlags);
      return;
    } 
  }
  
  public void onActivityResumed() {
    if (this.mOptions.lite)
      return; 
    getMainContentCaptureSession().notifySessionResumed();
  }
  
  public void onActivityPaused() {
    if (this.mOptions.lite)
      return; 
    getMainContentCaptureSession().notifySessionPaused();
  }
  
  public void onActivityDestroyed() {
    if (this.mOptions.lite)
      return; 
    getMainContentCaptureSession().destroy();
  }
  
  public void flush(int paramInt) {
    if (this.mOptions.lite)
      return; 
    getMainContentCaptureSession().flush(paramInt);
  }
  
  public ComponentName getServiceComponentName() {
    if (!isContentCaptureEnabled() && !this.mOptions.lite)
      return null; 
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      this.mService.getServiceComponentName((IResultReceiver)syncResultReceiver);
      return (ComponentName)syncResultReceiver.getParcelableResult();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get service componentName.");
    } 
  }
  
  public static ComponentName getServiceSettingsComponentName() {
    IBinder iBinder = ServiceManager.checkService("content_capture");
    if (iBinder == null)
      return null; 
    IContentCaptureManager iContentCaptureManager = IContentCaptureManager.Stub.asInterface(iBinder);
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      iContentCaptureManager.getServiceSettingsActivity((IResultReceiver)syncResultReceiver);
      int i = syncResultReceiver.getIntResult();
      if (i != -1)
        return (ComponentName)syncResultReceiver.getParcelableResult(); 
      SecurityException securityException = new SecurityException();
      this(syncResultReceiver.getStringResult());
      throw securityException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fail to get service settings componentName: ");
      stringBuilder.append(timeoutException);
      Log.e(str, stringBuilder.toString());
      return null;
    } 
  }
  
  public boolean isContentCaptureEnabled() {
    if (this.mOptions.lite)
      return false; 
    synchronized (this.mLock) {
      MainContentCaptureSession mainContentCaptureSession = this.mMainSession;
      if (mainContentCaptureSession != null && mainContentCaptureSession.isDisabled())
        return false; 
      return true;
    } 
  }
  
  public Set<ContentCaptureCondition> getContentCaptureConditions() {
    if (!isContentCaptureEnabled() && !this.mOptions.lite)
      return null; 
    SyncResultReceiver syncResultReceiver = syncRun(new _$$Lambda$ContentCaptureManager$F5a5O5ubPHwlndmmnmOInl75_sQ(this));
    try {
      ArrayList<?> arrayList = syncResultReceiver.getParcelableListResult();
      return (Set)ContentCaptureHelper.toSet(arrayList);
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get content capture conditions.");
    } 
  }
  
  public void setContentCaptureEnabled(boolean paramBoolean) {
    // Byte code:
    //   0: getstatic android/view/contentcapture/ContentCaptureHelper.sDebug : Z
    //   3: ifeq -> 58
    //   6: getstatic android/view/contentcapture/ContentCaptureManager.TAG : Ljava/lang/String;
    //   9: astore_2
    //   10: new java/lang/StringBuilder
    //   13: dup
    //   14: invokespecial <init> : ()V
    //   17: astore_3
    //   18: aload_3
    //   19: ldc_w 'setContentCaptureEnabled(): setting to '
    //   22: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   25: pop
    //   26: aload_3
    //   27: iload_1
    //   28: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload_3
    //   33: ldc_w ' for '
    //   36: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload_3
    //   41: aload_0
    //   42: getfield mContext : Landroid/content/Context;
    //   45: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   48: pop
    //   49: aload_2
    //   50: aload_3
    //   51: invokevirtual toString : ()Ljava/lang/String;
    //   54: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   57: pop
    //   58: aload_0
    //   59: getfield mLock : Ljava/lang/Object;
    //   62: astore_2
    //   63: aload_2
    //   64: monitorenter
    //   65: iload_1
    //   66: ifeq -> 83
    //   69: aload_0
    //   70: aload_0
    //   71: getfield mFlags : I
    //   74: bipush #-2
    //   76: iand
    //   77: putfield mFlags : I
    //   80: goto -> 93
    //   83: aload_0
    //   84: aload_0
    //   85: getfield mFlags : I
    //   88: iconst_1
    //   89: ior
    //   90: putfield mFlags : I
    //   93: aload_0
    //   94: getfield mMainSession : Landroid/view/contentcapture/MainContentCaptureSession;
    //   97: astore_3
    //   98: aload_2
    //   99: monitorexit
    //   100: aload_3
    //   101: ifnull -> 112
    //   104: aload_3
    //   105: iload_1
    //   106: iconst_1
    //   107: ixor
    //   108: invokevirtual setDisabled : (Z)Z
    //   111: pop
    //   112: return
    //   113: astore_3
    //   114: aload_2
    //   115: monitorexit
    //   116: aload_3
    //   117: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #591	-> 0
    //   #592	-> 6
    //   #596	-> 58
    //   #597	-> 65
    //   #598	-> 69
    //   #600	-> 83
    //   #602	-> 93
    //   #603	-> 98
    //   #604	-> 100
    //   #605	-> 104
    //   #607	-> 112
    //   #603	-> 113
    // Exception table:
    //   from	to	target	type
    //   69	80	113	finally
    //   83	93	113	finally
    //   93	98	113	finally
    //   98	100	113	finally
    //   114	116	113	finally
  }
  
  public void updateWindowAttributes(WindowManager.LayoutParams paramLayoutParams) {
    // Byte code:
    //   0: getstatic android/view/contentcapture/ContentCaptureHelper.sDebug : Z
    //   3: ifeq -> 44
    //   6: getstatic android/view/contentcapture/ContentCaptureManager.TAG : Ljava/lang/String;
    //   9: astore_2
    //   10: new java/lang/StringBuilder
    //   13: dup
    //   14: invokespecial <init> : ()V
    //   17: astore_3
    //   18: aload_3
    //   19: ldc_w 'updateWindowAttributes(): window flags='
    //   22: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   25: pop
    //   26: aload_3
    //   27: aload_1
    //   28: getfield flags : I
    //   31: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload_2
    //   36: aload_3
    //   37: invokevirtual toString : ()Ljava/lang/String;
    //   40: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   43: pop
    //   44: aload_1
    //   45: getfield flags : I
    //   48: sipush #8192
    //   51: iand
    //   52: ifeq -> 61
    //   55: iconst_1
    //   56: istore #4
    //   58: goto -> 64
    //   61: iconst_0
    //   62: istore #4
    //   64: aload_0
    //   65: getfield mLock : Ljava/lang/Object;
    //   68: astore_1
    //   69: aload_1
    //   70: monitorenter
    //   71: iload #4
    //   73: ifeq -> 89
    //   76: aload_0
    //   77: aload_0
    //   78: getfield mFlags : I
    //   81: iconst_2
    //   82: ior
    //   83: putfield mFlags : I
    //   86: goto -> 100
    //   89: aload_0
    //   90: aload_0
    //   91: getfield mFlags : I
    //   94: bipush #-3
    //   96: iand
    //   97: putfield mFlags : I
    //   100: aload_0
    //   101: getfield mMainSession : Landroid/view/contentcapture/MainContentCaptureSession;
    //   104: astore_2
    //   105: aload_1
    //   106: monitorexit
    //   107: aload_2
    //   108: ifnull -> 118
    //   111: aload_2
    //   112: iload #4
    //   114: invokevirtual setDisabled : (Z)Z
    //   117: pop
    //   118: return
    //   119: astore_2
    //   120: aload_1
    //   121: monitorexit
    //   122: aload_2
    //   123: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #615	-> 0
    //   #616	-> 6
    //   #618	-> 44
    //   #622	-> 64
    //   #623	-> 71
    //   #624	-> 76
    //   #626	-> 89
    //   #628	-> 100
    //   #629	-> 105
    //   #630	-> 107
    //   #631	-> 111
    //   #633	-> 118
    //   #629	-> 119
    // Exception table:
    //   from	to	target	type
    //   76	86	119	finally
    //   89	100	119	finally
    //   100	105	119	finally
    //   105	107	119	finally
    //   120	122	119	finally
  }
  
  @SystemApi
  public boolean isContentCaptureFeatureEnabled() {
    SyncResultReceiver syncResultReceiver = syncRun(new _$$Lambda$ContentCaptureManager$uvjEvSXcmP7_uA6i89N3m1TrKCk(this));
    try {
      int i = syncResultReceiver.getIntResult();
      if (i != 1) {
        if (i != 2) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("received invalid result: ");
          stringBuilder.append(i);
          Log.wtf(str, stringBuilder.toString());
          return false;
        } 
        return false;
      } 
      return true;
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fail to get content capture feature enable status: ");
      stringBuilder.append(timeoutException);
      Log.e(str, stringBuilder.toString());
      return false;
    } 
  }
  
  public void removeData(DataRemovalRequest paramDataRemovalRequest) {
    Preconditions.checkNotNull(paramDataRemovalRequest);
    try {
      this.mService.removeData(paramDataRemovalRequest);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void shareData(DataShareRequest paramDataShareRequest, Executor paramExecutor, DataShareWriteAdapter paramDataShareWriteAdapter) {
    Preconditions.checkNotNull(paramDataShareRequest);
    Preconditions.checkNotNull(paramDataShareWriteAdapter);
    Preconditions.checkNotNull(paramExecutor);
    try {
      IContentCaptureManager iContentCaptureManager = this.mService;
      DataShareAdapterDelegate dataShareAdapterDelegate = new DataShareAdapterDelegate();
      this(paramExecutor, paramDataShareWriteAdapter, this.mDataShareAdapterResourceManager);
      iContentCaptureManager.shareData(paramDataShareRequest, dataShareAdapterDelegate);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private SyncResultReceiver syncRun(MyRunnable paramMyRunnable) {
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      paramMyRunnable.run(syncResultReceiver);
      int i = syncResultReceiver.getIntResult();
      if (i != -1)
        return syncResultReceiver; 
      SecurityException securityException = new SecurityException();
      this(syncResultReceiver.getStringResult());
      throw securityException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get syn run result from SyncResultReceiver.");
    } 
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.println("ContentCaptureManager");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("  ");
    String str = stringBuilder.toString();
    synchronized (this.mLock) {
      paramPrintWriter.print(str);
      paramPrintWriter.print("isContentCaptureEnabled(): ");
      paramPrintWriter.println(isContentCaptureEnabled());
      paramPrintWriter.print(str);
      paramPrintWriter.print("Debug: ");
      paramPrintWriter.print(ContentCaptureHelper.sDebug);
      paramPrintWriter.print(" Verbose: ");
      paramPrintWriter.println(ContentCaptureHelper.sVerbose);
      paramPrintWriter.print(str);
      paramPrintWriter.print("Context: ");
      paramPrintWriter.println(this.mContext);
      paramPrintWriter.print(str);
      paramPrintWriter.print("User: ");
      paramPrintWriter.println(this.mContext.getUserId());
      paramPrintWriter.print(str);
      paramPrintWriter.print("Service: ");
      paramPrintWriter.println(this.mService);
      paramPrintWriter.print(str);
      paramPrintWriter.print("Flags: ");
      paramPrintWriter.println(this.mFlags);
      paramPrintWriter.print(str);
      paramPrintWriter.print("Options: ");
      this.mOptions.dumpShort(paramPrintWriter);
      paramPrintWriter.println();
      if (this.mMainSession != null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append(str);
        stringBuilder1.append("  ");
        String str1 = stringBuilder1.toString();
        paramPrintWriter.print(str);
        paramPrintWriter.println("Main session:");
        this.mMainSession.dump(str1, paramPrintWriter);
      } else {
        paramPrintWriter.print(str);
        paramPrintWriter.println("No sessions");
      } 
      return;
    } 
  }
  
  public static interface ContentCaptureClient {
    ComponentName contentCaptureClientGetComponentName();
  }
  
  class DataShareAdapterDelegate extends IDataShareWriteAdapter.Stub {
    private final WeakReference<ContentCaptureManager.LocalDataShareAdapterResourceManager> mResourceManagerReference;
    
    private DataShareAdapterDelegate(ContentCaptureManager this$0, DataShareWriteAdapter param1DataShareWriteAdapter, ContentCaptureManager.LocalDataShareAdapterResourceManager param1LocalDataShareAdapterResourceManager) {
      Preconditions.checkNotNull(this$0);
      Preconditions.checkNotNull(param1DataShareWriteAdapter);
      Preconditions.checkNotNull(param1LocalDataShareAdapterResourceManager);
      param1LocalDataShareAdapterResourceManager.initializeForDelegate(this, param1DataShareWriteAdapter, (Executor)this$0);
      this.mResourceManagerReference = new WeakReference<>(param1LocalDataShareAdapterResourceManager);
    }
    
    public void write(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {
      executeAdapterMethodLocked(new _$$Lambda$ContentCaptureManager$DataShareAdapterDelegate$q3ba3mvm7p4FcLLvlJAkgyzoYHc(param1ParcelFileDescriptor), "onWrite");
    }
    
    public void error(int param1Int) throws RemoteException {
      executeAdapterMethodLocked(new _$$Lambda$ContentCaptureManager$DataShareAdapterDelegate$Zpq4r008FlTn1ia7xzkN4NTgJrU(param1Int), "onError");
      clearHardReferences();
    }
    
    public void rejected() throws RemoteException {
      executeAdapterMethodLocked((Consumer<DataShareWriteAdapter>)_$$Lambda$Hf5vPTf9zSkFpeqlKIJymvlJvy8.INSTANCE, "onRejected");
      clearHardReferences();
    }
    
    public void finish() throws RemoteException {
      clearHardReferences();
    }
    
    private void executeAdapterMethodLocked(Consumer<DataShareWriteAdapter> param1Consumer, String param1String) {
      StringBuilder stringBuilder;
      ContentCaptureManager.LocalDataShareAdapterResourceManager localDataShareAdapterResourceManager = this.mResourceManagerReference.get();
      if (localDataShareAdapterResourceManager == null) {
        String str = ContentCaptureManager.TAG;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Can't execute ");
        stringBuilder1.append(param1String);
        stringBuilder1.append("(), resource manager has been GC'ed");
        Slog.w(str, stringBuilder1.toString());
        return;
      } 
      DataShareWriteAdapter dataShareWriteAdapter = localDataShareAdapterResourceManager.getAdapter(this);
      Executor executor = localDataShareAdapterResourceManager.getExecutor(this);
      if (dataShareWriteAdapter == null || executor == null) {
        null = ContentCaptureManager.TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("Can't execute ");
        stringBuilder.append(param1String);
        stringBuilder.append("(), references are null");
        Slog.w(null, stringBuilder.toString());
        return;
      } 
      long l = Binder.clearCallingIdentity();
      try {
        _$$Lambda$ContentCaptureManager$DataShareAdapterDelegate$47CPH_dzxQYL6qTCBFWSq11DNAU _$$Lambda$ContentCaptureManager$DataShareAdapterDelegate$47CPH_dzxQYL6qTCBFWSq11DNAU = new _$$Lambda$ContentCaptureManager$DataShareAdapterDelegate$47CPH_dzxQYL6qTCBFWSq11DNAU();
        this((Consumer)null, (DataShareWriteAdapter)stringBuilder);
        executor.execute(_$$Lambda$ContentCaptureManager$DataShareAdapterDelegate$47CPH_dzxQYL6qTCBFWSq11DNAU);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
    
    private void clearHardReferences() {
      ContentCaptureManager.LocalDataShareAdapterResourceManager localDataShareAdapterResourceManager = this.mResourceManagerReference.get();
      if (localDataShareAdapterResourceManager == null) {
        Slog.w(ContentCaptureManager.TAG, "Can't clear references, resource manager has been GC'ed");
        return;
      } 
      localDataShareAdapterResourceManager.clearHardReferences(this);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DataShareError {}
  
  private static class LocalDataShareAdapterResourceManager {
    private Map<ContentCaptureManager.DataShareAdapterDelegate, Executor> mExecutorHardReferences;
    
    private Map<ContentCaptureManager.DataShareAdapterDelegate, DataShareWriteAdapter> mWriteAdapterHardReferences;
    
    private LocalDataShareAdapterResourceManager() {
      this.mWriteAdapterHardReferences = new HashMap<>();
      this.mExecutorHardReferences = new HashMap<>();
    }
    
    void initializeForDelegate(ContentCaptureManager.DataShareAdapterDelegate param1DataShareAdapterDelegate, DataShareWriteAdapter param1DataShareWriteAdapter, Executor param1Executor) {
      this.mWriteAdapterHardReferences.put(param1DataShareAdapterDelegate, param1DataShareWriteAdapter);
      this.mExecutorHardReferences.put(param1DataShareAdapterDelegate, param1Executor);
    }
    
    Executor getExecutor(ContentCaptureManager.DataShareAdapterDelegate param1DataShareAdapterDelegate) {
      return this.mExecutorHardReferences.get(param1DataShareAdapterDelegate);
    }
    
    DataShareWriteAdapter getAdapter(ContentCaptureManager.DataShareAdapterDelegate param1DataShareAdapterDelegate) {
      return this.mWriteAdapterHardReferences.get(param1DataShareAdapterDelegate);
    }
    
    void clearHardReferences(ContentCaptureManager.DataShareAdapterDelegate param1DataShareAdapterDelegate) {
      this.mWriteAdapterHardReferences.remove(param1DataShareAdapterDelegate);
      this.mExecutorHardReferences.remove(param1DataShareAdapterDelegate);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface LoggingLevel {}
  
  private static interface MyRunnable {
    void run(SyncResultReceiver param1SyncResultReceiver) throws RemoteException;
  }
}
