package android.view.contentcapture;

import android.graphics.Insets;
import android.util.DebugUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewStructure;
import android.view.autofill.AutofillId;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.SecureRandom;
import java.util.ArrayList;

public abstract class ContentCaptureSession implements AutoCloseable {
  public static final int FLUSH_REASON_FULL = 1;
  
  public static final int FLUSH_REASON_IDLE_TIMEOUT = 5;
  
  public static final int FLUSH_REASON_SESSION_CONNECTED = 7;
  
  public static final int FLUSH_REASON_SESSION_FINISHED = 4;
  
  public static final int FLUSH_REASON_SESSION_STARTED = 3;
  
  public static final int FLUSH_REASON_TEXT_CHANGE_TIMEOUT = 6;
  
  public static final int FLUSH_REASON_VIEW_ROOT_ENTERED = 2;
  
  private static final SecureRandom ID_GENERATOR;
  
  private static final int INITIAL_CHILDREN_CAPACITY = 5;
  
  public static final int STATE_ACTIVE = 2;
  
  public static final int STATE_BY_APP = 64;
  
  public static final int STATE_DISABLED = 4;
  
  public static final int STATE_DUPLICATED_ID = 8;
  
  public static final int STATE_FLAG_SECURE = 32;
  
  public static final int STATE_INTERNAL_ERROR = 256;
  
  public static final int STATE_NOT_WHITELISTED = 512;
  
  public static final int STATE_NO_RESPONSE = 128;
  
  public static final int STATE_NO_SERVICE = 16;
  
  public static final int STATE_SERVICE_DIED = 1024;
  
  public static final int STATE_SERVICE_RESURRECTED = 4096;
  
  public static final int STATE_SERVICE_UPDATING = 2048;
  
  public static final int STATE_WAITING_FOR_SERVER = 1;
  
  private static final String TAG = ContentCaptureSession.class.getSimpleName();
  
  public static final int UNKNOWN_STATE = 0;
  
  private ArrayList<ContentCaptureSession> mChildren;
  
  private ContentCaptureContext mClientContext;
  
  private ContentCaptureSessionId mContentCaptureSessionId;
  
  private boolean mDestroyed;
  
  protected final int mId;
  
  static {
    ID_GENERATOR = new SecureRandom();
  }
  
  private final Object mLock = new Object();
  
  private int mState;
  
  public ContentCaptureSession(int paramInt) {
    boolean bool = false;
    this.mState = 0;
    if (paramInt != 0)
      bool = true; 
    Preconditions.checkArgument(bool);
    this.mId = paramInt;
  }
  
  protected ContentCaptureSession() {
    this(getRandomSessionId());
  }
  
  ContentCaptureSession(ContentCaptureContext paramContentCaptureContext) {
    this();
    this.mClientContext = (ContentCaptureContext)Preconditions.checkNotNull(paramContentCaptureContext);
  }
  
  public final ContentCaptureSessionId getContentCaptureSessionId() {
    if (this.mContentCaptureSessionId == null)
      this.mContentCaptureSessionId = new ContentCaptureSessionId(this.mId); 
    return this.mContentCaptureSessionId;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public final ContentCaptureSession createContentCaptureSession(ContentCaptureContext paramContentCaptureContext) {
    null = newChild(paramContentCaptureContext);
    if (ContentCaptureHelper.sDebug) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("createContentCaptureSession(");
      stringBuilder.append(paramContentCaptureContext);
      stringBuilder.append(": parent=");
      stringBuilder.append(this.mId);
      stringBuilder.append(", child=");
      stringBuilder.append(null.mId);
      Log.d(str, stringBuilder.toString());
    } 
    synchronized (this.mLock) {
      if (this.mChildren == null) {
        ArrayList<ContentCaptureSession> arrayList = new ArrayList();
        this(5);
        this.mChildren = arrayList;
      } 
      this.mChildren.add(null);
      return null;
    } 
  }
  
  public final void setContentCaptureContext(ContentCaptureContext paramContentCaptureContext) {
    this.mClientContext = paramContentCaptureContext;
    updateContentCaptureContext(paramContentCaptureContext);
  }
  
  public final ContentCaptureContext getContentCaptureContext() {
    return this.mClientContext;
  }
  
  public final void destroy() {
    synchronized (this.mLock) {
      if (this.mDestroyed) {
        if (ContentCaptureHelper.sDebug) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("destroy(");
          stringBuilder.append(this.mId);
          stringBuilder.append("): already destroyed");
          Log.d(str, stringBuilder.toString());
        } 
        return;
      } 
      this.mDestroyed = true;
      if (ContentCaptureHelper.sVerbose) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("destroy(): state=");
        stringBuilder.append(getStateAsString(this.mState));
        stringBuilder.append(", mId=");
        stringBuilder.append(this.mId);
        Log.v(str, stringBuilder.toString());
      } 
      if (this.mChildren != null) {
        int i = this.mChildren.size();
        if (ContentCaptureHelper.sVerbose) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Destroying ");
          stringBuilder.append(i);
          stringBuilder.append(" children first");
          Log.v(str, stringBuilder.toString());
        } 
        for (byte b = 0; b < i; b++) {
          ContentCaptureSession contentCaptureSession = this.mChildren.get(b);
          try {
            contentCaptureSession.destroy();
          } catch (Exception exception) {
            String str = TAG;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("exception destroying child session #");
            stringBuilder.append(b);
            stringBuilder.append(": ");
            stringBuilder.append(exception);
            Log.w(str, stringBuilder.toString());
          } 
        } 
      } 
      try {
        flush(4);
        return;
      } finally {
        onDestroy();
      } 
    } 
  }
  
  public void close() {
    destroy();
  }
  
  public final void notifyViewAppeared(ViewStructure paramViewStructure) {
    Preconditions.checkNotNull(paramViewStructure);
    if (!isContentCaptureEnabled())
      return; 
    if (paramViewStructure instanceof ViewNode.ViewStructureImpl) {
      internalNotifyViewAppeared((ViewNode.ViewStructureImpl)paramViewStructure);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid node class: ");
    stringBuilder.append(paramViewStructure.getClass());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public final void notifyViewDisappeared(AutofillId paramAutofillId) {
    Preconditions.checkNotNull(paramAutofillId);
    if (!isContentCaptureEnabled())
      return; 
    internalNotifyViewDisappeared(paramAutofillId);
  }
  
  public final void notifyViewsDisappeared(AutofillId paramAutofillId, long[] paramArrayOflong) {
    boolean bool = paramAutofillId.isNonVirtual();
    byte b = 0;
    Preconditions.checkArgument(bool, "hostId cannot be virtual: %s", new Object[] { paramAutofillId });
    Preconditions.checkArgument(ArrayUtils.isEmpty(paramArrayOflong) ^ true, "virtual ids cannot be empty");
    if (!isContentCaptureEnabled())
      return; 
    for (int i = paramArrayOflong.length; b < i; ) {
      long l = paramArrayOflong[b];
      internalNotifyViewDisappeared(new AutofillId(paramAutofillId, l, this.mId));
      b++;
    } 
  }
  
  public final void notifyViewTextChanged(AutofillId paramAutofillId, CharSequence paramCharSequence) {
    Preconditions.checkNotNull(paramAutofillId);
    if (!isContentCaptureEnabled())
      return; 
    internalNotifyViewTextChanged(paramAutofillId, paramCharSequence);
  }
  
  public final void notifyViewInsetsChanged(Insets paramInsets) {
    Preconditions.checkNotNull(paramInsets);
    if (!isContentCaptureEnabled())
      return; 
    internalNotifyViewInsetsChanged(paramInsets);
  }
  
  public final void notifySessionResumed() {
    if (!isContentCaptureEnabled())
      return; 
    internalNotifySessionResumed();
  }
  
  public final void notifySessionPaused() {
    if (!isContentCaptureEnabled())
      return; 
    internalNotifySessionPaused();
  }
  
  public final ViewStructure newViewStructure(View paramView) {
    return new ViewNode.ViewStructureImpl(paramView);
  }
  
  public AutofillId newAutofillId(AutofillId paramAutofillId, long paramLong) {
    Preconditions.checkNotNull(paramAutofillId);
    Preconditions.checkArgument(paramAutofillId.isNonVirtual(), "hostId cannot be virtual: %s", new Object[] { paramAutofillId });
    return new AutofillId(paramAutofillId, paramLong, this.mId);
  }
  
  public final ViewStructure newVirtualViewStructure(AutofillId paramAutofillId, long paramLong) {
    return new ViewNode.ViewStructureImpl(paramAutofillId, paramLong, this.mId);
  }
  
  boolean isContentCaptureEnabled() {
    synchronized (this.mLock) {
      boolean bool;
      if (!this.mDestroyed) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("id: ");
    paramPrintWriter.println(this.mId);
    if (this.mClientContext != null) {
      paramPrintWriter.print(paramString);
      this.mClientContext.dump(paramPrintWriter);
      paramPrintWriter.println();
    } 
    synchronized (this.mLock) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("destroyed: ");
      paramPrintWriter.println(this.mDestroyed);
      if (this.mChildren != null && !this.mChildren.isEmpty()) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(paramString);
        stringBuilder.append("  ");
        String str = stringBuilder.toString();
        int i = this.mChildren.size();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("number children: ");
        paramPrintWriter.println(i);
        for (byte b = 0; b < i; b++) {
          ContentCaptureSession contentCaptureSession = this.mChildren.get(b);
          paramPrintWriter.print(paramString);
          paramPrintWriter.print(b);
          paramPrintWriter.println(": ");
          contentCaptureSession.dump(str, paramPrintWriter);
        } 
      } 
      return;
    } 
  }
  
  public String toString() {
    return Integer.toString(this.mId);
  }
  
  protected static String getStateAsString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder.append(" (");
    if (paramInt == 0) {
      null = "UNKNOWN";
    } else {
      null = DebugUtils.flagsToString(ContentCaptureSession.class, "STATE_", paramInt);
    } 
    stringBuilder.append(null);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  public static String getFlushReasonAsString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("UNKOWN-");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 7:
        return "CONNECTED";
      case 6:
        return "TEXT_CHANGE";
      case 5:
        return "IDLE";
      case 4:
        return "FINISHED";
      case 3:
        return "STARTED";
      case 2:
        return "VIEW_ROOT";
      case 1:
        break;
    } 
    return "FULL";
  }
  
  private static int getRandomSessionId() {
    while (true) {
      int i = ID_GENERATOR.nextInt();
      if (i != 0)
        return i; 
    } 
  }
  
  abstract void flush(int paramInt);
  
  abstract MainContentCaptureSession getMainCaptureSession();
  
  abstract void internalNotifySessionPaused();
  
  abstract void internalNotifySessionResumed();
  
  abstract void internalNotifyViewAppeared(ViewNode.ViewStructureImpl paramViewStructureImpl);
  
  abstract void internalNotifyViewDisappeared(AutofillId paramAutofillId);
  
  abstract void internalNotifyViewInsetsChanged(Insets paramInsets);
  
  abstract void internalNotifyViewTextChanged(AutofillId paramAutofillId, CharSequence paramCharSequence);
  
  public abstract void internalNotifyViewTreeEvent(boolean paramBoolean);
  
  abstract ContentCaptureSession newChild(ContentCaptureContext paramContentCaptureContext);
  
  abstract void onDestroy();
  
  abstract void updateContentCaptureContext(ContentCaptureContext paramContentCaptureContext);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FlushReason {}
}
