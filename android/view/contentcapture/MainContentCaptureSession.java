package android.view.contentcapture;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ParceledListSlice;
import android.graphics.Insets;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.LocalLog;
import android.util.Log;
import android.util.TimeUtils;
import android.view.autofill.AutofillId;
import com.android.internal.os.IResultReceiver;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public final class MainContentCaptureSession extends ContentCaptureSession {
  private static final String TAG = MainContentCaptureSession.class.getSimpleName();
  
  private final AtomicBoolean mDisabled = new AtomicBoolean(false);
  
  private int mState = 0;
  
  private boolean mNextFlushForTextChanged = false;
  
  public static final String EXTRA_BINDER = "binder";
  
  public static final String EXTRA_ENABLED_STATE = "enabled";
  
  private static final boolean FORCE_FLUSH = true;
  
  private static final int MSG_FLUSH = 1;
  
  private IBinder mApplicationToken;
  
  private ComponentName mComponentName;
  
  private final Context mContext;
  
  private IContentCaptureDirectManager mDirectServiceInterface;
  
  private IBinder.DeathRecipient mDirectServiceVulture;
  
  private ArrayList<ContentCaptureEvent> mEvents;
  
  private final LocalLog mFlushHistory;
  
  private final Handler mHandler;
  
  private final ContentCaptureManager mManager;
  
  private long mNextFlush;
  
  private final SessionStateReceiver mSessionStateReceiver;
  
  private final IContentCaptureManager mSystemServerInterface;
  
  class SessionStateReceiver extends IResultReceiver.Stub {
    private final WeakReference<MainContentCaptureSession> mMainSession;
    
    SessionStateReceiver(MainContentCaptureSession this$0) {
      this.mMainSession = new WeakReference<>(this$0);
    }
    
    public void send(int param1Int, Bundle param1Bundle) {
      MainContentCaptureSession mainContentCaptureSession = this.mMainSession.get();
      if (mainContentCaptureSession == null) {
        Log.w(MainContentCaptureSession.TAG, "received result after mina session released");
        return;
      } 
      if (param1Bundle != null) {
        boolean bool = param1Bundle.getBoolean("enabled");
        if (bool) {
          if (param1Int == 2) {
            bool = true;
          } else {
            bool = false;
          } 
          mainContentCaptureSession.mDisabled.set(bool);
          return;
        } 
        IBinder iBinder = param1Bundle.getBinder("binder");
        if (iBinder == null) {
          Log.wtf(MainContentCaptureSession.TAG, "No binder extra result");
          mainContentCaptureSession.mHandler.post(new _$$Lambda$MainContentCaptureSession$SessionStateReceiver$8hITRJtT52FGVzLySKUnda7QvUU(mainContentCaptureSession));
          return;
        } 
      } else {
        param1Bundle = null;
      } 
      mainContentCaptureSession.mHandler.post(new _$$Lambda$MainContentCaptureSession$SessionStateReceiver$1XAByNlZB50Bl0h_alx4PJFpMsU(mainContentCaptureSession, param1Int, (IBinder)param1Bundle));
    }
  }
  
  protected MainContentCaptureSession(Context paramContext, ContentCaptureManager paramContentCaptureManager, Handler paramHandler, IContentCaptureManager paramIContentCaptureManager) {
    this.mContext = paramContext;
    this.mManager = paramContentCaptureManager;
    this.mHandler = paramHandler;
    this.mSystemServerInterface = paramIContentCaptureManager;
    int i = paramContentCaptureManager.mOptions.logHistorySize;
    if (i > 0) {
      LocalLog localLog = new LocalLog(i);
    } else {
      paramContext = null;
    } 
    this.mFlushHistory = (LocalLog)paramContext;
    this.mSessionStateReceiver = new SessionStateReceiver(this);
  }
  
  MainContentCaptureSession getMainCaptureSession() {
    return this;
  }
  
  ContentCaptureSession newChild(ContentCaptureContext paramContentCaptureContext) {
    ChildContentCaptureSession childContentCaptureSession = new ChildContentCaptureSession(this, paramContentCaptureContext);
    notifyChildSessionStarted(this.mId, childContentCaptureSession.mId, paramContentCaptureContext);
    return childContentCaptureSession;
  }
  
  void start(IBinder paramIBinder, ComponentName paramComponentName, int paramInt) {
    String str;
    if (!isContentCaptureEnabled())
      return; 
    if (ContentCaptureHelper.sVerbose) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("start(): token=");
      stringBuilder.append(paramIBinder);
      stringBuilder.append(", comp=");
      stringBuilder.append(ComponentName.flattenToShortString(paramComponentName));
      String str2 = stringBuilder.toString();
      Log.v(str1, str2);
    } 
    if (hasStarted()) {
      if (ContentCaptureHelper.sDebug) {
        String str1 = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ignoring handleStartSession(");
        stringBuilder.append(paramIBinder);
        stringBuilder.append("/");
        stringBuilder.append(ComponentName.flattenToShortString(paramComponentName));
        stringBuilder.append(" while on state ");
        paramInt = this.mState;
        stringBuilder.append(getStateAsString(paramInt));
        str = stringBuilder.toString();
        Log.d(str1, str);
      } 
      return;
    } 
    this.mState = 1;
    this.mApplicationToken = (IBinder)str;
    this.mComponentName = paramComponentName;
    if (ContentCaptureHelper.sVerbose) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleStartSession(): token=");
      stringBuilder.append(str);
      stringBuilder.append(", act=");
      stringBuilder.append(getDebugState());
      stringBuilder.append(", id=");
      stringBuilder.append(this.mId);
      str = stringBuilder.toString();
      Log.v(str1, str);
    } 
    try {
      this.mSystemServerInterface.startSession(this.mApplicationToken, paramComponentName, this.mId, paramInt, this.mSessionStateReceiver);
    } catch (RemoteException remoteException) {
      str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error starting session for ");
      stringBuilder.append(paramComponentName.flattenToShortString());
      stringBuilder.append(": ");
      stringBuilder.append(remoteException);
      Log.w(str, stringBuilder.toString());
    } 
  }
  
  void onDestroy() {
    this.mHandler.removeMessages(1);
    this.mHandler.post(new _$$Lambda$MainContentCaptureSession$HTmdDf687TPcaTnLyPp3wo0gI60(this));
  }
  
  private void onSessionStarted(int paramInt, IBinder paramIBinder) {
    boolean bool = false;
    if (paramIBinder != null) {
      this.mDirectServiceInterface = IContentCaptureDirectManager.Stub.asInterface(paramIBinder);
      _$$Lambda$MainContentCaptureSession$UWslDbWedtPhv49PtRsvG4TlYWw _$$Lambda$MainContentCaptureSession$UWslDbWedtPhv49PtRsvG4TlYWw = new _$$Lambda$MainContentCaptureSession$UWslDbWedtPhv49PtRsvG4TlYWw(this);
      try {
        paramIBinder.linkToDeath(_$$Lambda$MainContentCaptureSession$UWslDbWedtPhv49PtRsvG4TlYWw, 0);
      } catch (RemoteException remoteException) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to link to death on ");
        stringBuilder.append(paramIBinder);
        stringBuilder.append(": ");
        stringBuilder.append(remoteException);
        Log.w(str, stringBuilder.toString());
      } 
    } 
    if ((paramInt & 0x4) != 0) {
      resetSession(paramInt);
    } else {
      this.mState = paramInt;
      this.mDisabled.set(false);
      flushIfNeeded(7);
    } 
    if (ContentCaptureHelper.sVerbose) {
      String str2 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleSessionStarted() result: id=");
      stringBuilder.append(this.mId);
      stringBuilder.append(" resultCode=");
      stringBuilder.append(paramInt);
      stringBuilder.append(", state=");
      paramInt = this.mState;
      stringBuilder.append(getStateAsString(paramInt));
      stringBuilder.append(", disabled=");
      stringBuilder.append(this.mDisabled.get());
      stringBuilder.append(", binder=");
      stringBuilder.append(paramIBinder);
      stringBuilder.append(", events=");
      ArrayList<ContentCaptureEvent> arrayList = this.mEvents;
      if (arrayList == null) {
        paramInt = bool;
      } else {
        paramInt = arrayList.size();
      } 
      stringBuilder.append(paramInt);
      String str1 = stringBuilder.toString();
      Log.v(str2, str1);
    } 
  }
  
  private void sendEvent(ContentCaptureEvent paramContentCaptureEvent) {
    sendEvent(paramContentCaptureEvent, false);
  }
  
  private void sendEvent(ContentCaptureEvent paramContentCaptureEvent, boolean paramBoolean) {
    String str;
    int i = paramContentCaptureEvent.getType();
    if (ContentCaptureHelper.sVerbose) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleSendEvent(");
      stringBuilder.append(getDebugState());
      stringBuilder.append("): ");
      stringBuilder.append(paramContentCaptureEvent);
      Log.v(str1, stringBuilder.toString());
    } 
    if (!hasStarted() && i != -1 && i != 6) {
      str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleSendEvent(");
      stringBuilder.append(getDebugState());
      stringBuilder.append(", ");
      stringBuilder.append(ContentCaptureEvent.getTypeAsString(i));
      stringBuilder.append("): dropping because session not started yet");
      String str1 = stringBuilder.toString();
      Log.v(str, str1);
      return;
    } 
    if (this.mDisabled.get()) {
      if (ContentCaptureHelper.sVerbose)
        Log.v(TAG, "handleSendEvent(): ignoring when disabled"); 
      return;
    } 
    int j = this.mManager.mOptions.maxBufferSize;
    if (this.mEvents == null) {
      if (ContentCaptureHelper.sVerbose) {
        String str1 = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handleSendEvent(): creating buffer for ");
        stringBuilder.append(j);
        stringBuilder.append(" events");
        Log.v(str1, stringBuilder.toString());
      } 
      this.mEvents = new ArrayList<>(j);
    } 
    int k = 1;
    int m = k;
    if (!this.mEvents.isEmpty()) {
      m = k;
      if (i == 3) {
        ArrayList<ContentCaptureEvent> arrayList = this.mEvents;
        ContentCaptureEvent contentCaptureEvent = arrayList.get(arrayList.size() - 1);
        m = k;
        if (contentCaptureEvent.getType() == 3) {
          m = k;
          if (contentCaptureEvent.getId().equals(str.getId())) {
            if (ContentCaptureHelper.sVerbose) {
              String str1 = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Buffering VIEW_TEXT_CHANGED event, updated text=");
              stringBuilder.append(ContentCaptureHelper.getSanitizedString(str.getText()));
              String str2 = stringBuilder.toString();
              Log.v(str1, str2);
            } 
            contentCaptureEvent.mergeEvent((ContentCaptureEvent)str);
            m = 0;
          } 
        } 
      } 
    } 
    k = m;
    if (!this.mEvents.isEmpty()) {
      k = m;
      if (i == 2) {
        ArrayList<ContentCaptureEvent> arrayList = this.mEvents;
        ContentCaptureEvent contentCaptureEvent = arrayList.get(arrayList.size() - 1);
        k = m;
        if (contentCaptureEvent.getType() == 2) {
          k = m;
          if (str.getSessionId() == contentCaptureEvent.getSessionId()) {
            if (ContentCaptureHelper.sVerbose) {
              String str1 = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Buffering TYPE_VIEW_DISAPPEARED events for session ");
              stringBuilder.append(contentCaptureEvent.getSessionId());
              String str2 = stringBuilder.toString();
              Log.v(str1, str2);
            } 
            contentCaptureEvent.mergeEvent((ContentCaptureEvent)str);
            k = 0;
          } 
        } 
      } 
    } 
    if (k)
      this.mEvents.add(str); 
    k = this.mEvents.size();
    if (k < j) {
      m = 1;
    } else {
      m = 0;
    } 
    if (m != 0 && !paramBoolean) {
      if (i == 3) {
        this.mNextFlushForTextChanged = true;
        m = 6;
      } else {
        if (this.mNextFlushForTextChanged) {
          if (ContentCaptureHelper.sVerbose)
            Log.i(TAG, "Not scheduling flush because next flush is for text changed"); 
          return;
        } 
        m = 5;
      } 
      scheduleFlush(m, true);
      return;
    } 
    if (this.mState != 2 && k >= j) {
      if (ContentCaptureHelper.sDebug) {
        str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Closing session for ");
        stringBuilder.append(getDebugState());
        stringBuilder.append(" after ");
        stringBuilder.append(k);
        stringBuilder.append(" delayed events");
        Log.d(str, stringBuilder.toString());
      } 
      resetSession(132);
      return;
    } 
    if (i != -2) {
      if (i != -1) {
        m = 1;
      } else {
        m = 3;
      } 
    } else {
      m = 4;
    } 
    flush(m);
  }
  
  private boolean hasStarted() {
    boolean bool;
    if (this.mState != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void scheduleFlush(int paramInt, boolean paramBoolean) {
    int i;
    if (ContentCaptureHelper.sVerbose) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleScheduleFlush(");
      stringBuilder.append(getDebugState(paramInt));
      stringBuilder.append(", checkExisting=");
      stringBuilder.append(paramBoolean);
      Log.v(str, stringBuilder.toString());
    } 
    if (!hasStarted()) {
      if (ContentCaptureHelper.sVerbose)
        Log.v(TAG, "handleScheduleFlush(): session not started yet"); 
      return;
    } 
    if (this.mDisabled.get()) {
      Integer integer;
      String str2 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleScheduleFlush(");
      stringBuilder.append(getDebugState(paramInt));
      stringBuilder.append("): should not be called when disabled. events=");
      ArrayList<ContentCaptureEvent> arrayList = this.mEvents;
      if (arrayList == null) {
        arrayList = null;
      } else {
        integer = Integer.valueOf(arrayList.size());
      } 
      stringBuilder.append(integer);
      String str1 = stringBuilder.toString();
      Log.e(str2, str1);
      return;
    } 
    if (paramBoolean && this.mHandler.hasMessages(1))
      this.mHandler.removeMessages(1); 
    if (paramInt == 6) {
      i = this.mManager.mOptions.textChangeFlushingFrequencyMs;
    } else {
      if (paramInt != 5 && 
        ContentCaptureHelper.sDebug) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handleScheduleFlush(");
        stringBuilder.append(getDebugState(paramInt));
        stringBuilder.append("): not a timeout reason because mDirectServiceInterface is not ready yet");
        Log.d(str, stringBuilder.toString());
      } 
      i = this.mManager.mOptions.idleFlushingFrequencyMs;
    } 
    this.mNextFlush = System.currentTimeMillis() + i;
    if (ContentCaptureHelper.sVerbose) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleScheduleFlush(): scheduled to flush in ");
      stringBuilder.append(i);
      stringBuilder.append("ms: ");
      long l = this.mNextFlush;
      stringBuilder.append(TimeUtils.logTimeOfDay(l));
      String str2 = stringBuilder.toString();
      Log.v(str1, str2);
    } 
    this.mHandler.postDelayed(new _$$Lambda$MainContentCaptureSession$49zT7C2BXrEdkyggyGk1Qs4d46k(this, paramInt), 1, i);
  }
  
  private void flushIfNeeded(int paramInt) {
    ArrayList<ContentCaptureEvent> arrayList = this.mEvents;
    if (arrayList == null || arrayList.isEmpty()) {
      if (ContentCaptureHelper.sVerbose)
        Log.v(TAG, "Nothing to flush"); 
      return;
    } 
    flush(paramInt);
  }
  
  void flush(int paramInt) {
    if (this.mEvents == null)
      return; 
    if (this.mDisabled.get()) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleForceFlush(");
      stringBuilder.append(getDebugState(paramInt));
      stringBuilder.append("): should not be when disabled");
      Log.e(str1, stringBuilder.toString());
      return;
    } 
    if (this.mDirectServiceInterface == null) {
      if (ContentCaptureHelper.sVerbose) {
        String str1 = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handleForceFlush(");
        stringBuilder.append(getDebugState(paramInt));
        stringBuilder.append("): hold your horses, client not ready: ");
        stringBuilder.append(this.mEvents);
        Log.v(str1, stringBuilder.toString());
      } 
      if (!this.mHandler.hasMessages(1))
        scheduleFlush(paramInt, false); 
      return;
    } 
    this.mNextFlushForTextChanged = false;
    int i = this.mEvents.size();
    String str = getFlushReasonAsString(paramInt);
    if (ContentCaptureHelper.sDebug) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Flushing ");
      stringBuilder.append(i);
      stringBuilder.append(" event(s) for ");
      stringBuilder.append(getDebugState(paramInt));
      Log.d(str1, stringBuilder.toString());
    } 
    if (this.mFlushHistory != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("r=");
      stringBuilder.append(str);
      stringBuilder.append(" s=");
      stringBuilder.append(i);
      stringBuilder.append(" m=");
      stringBuilder.append(this.mManager.mOptions.maxBufferSize);
      stringBuilder.append(" i=");
      stringBuilder.append(this.mManager.mOptions.idleFlushingFrequencyMs);
      str = stringBuilder.toString();
      this.mFlushHistory.log(str);
    } 
    try {
      this.mHandler.removeMessages(1);
      ParceledListSlice<ContentCaptureEvent> parceledListSlice = clearEvents();
      this.mDirectServiceInterface.sendEvents(parceledListSlice, paramInt, this.mManager.mOptions);
    } catch (RemoteException remoteException) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error sending ");
      stringBuilder.append(i);
      stringBuilder.append(" for ");
      stringBuilder.append(getDebugState());
      stringBuilder.append(": ");
      stringBuilder.append(remoteException);
      Log.w(str1, stringBuilder.toString());
    } 
  }
  
  public void updateContentCaptureContext(ContentCaptureContext paramContentCaptureContext) {
    notifyContextUpdated(this.mId, paramContentCaptureContext);
  }
  
  private ParceledListSlice<ContentCaptureEvent> clearEvents() {
    List<?> list = this.mEvents;
    if (list == null)
      list = Collections.emptyList(); 
    this.mEvents = null;
    return new ParceledListSlice(list);
  }
  
  private void destroySession() {
    if (ContentCaptureHelper.sDebug) {
      int i;
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Destroying session (ctx=");
      stringBuilder.append(this.mContext);
      stringBuilder.append(", id=");
      stringBuilder.append(this.mId);
      stringBuilder.append(") with ");
      ArrayList<ContentCaptureEvent> arrayList = this.mEvents;
      if (arrayList == null) {
        i = 0;
      } else {
        i = arrayList.size();
      } 
      stringBuilder.append(i);
      stringBuilder.append(" event(s) for ");
      stringBuilder.append(getDebugState());
      String str2 = stringBuilder.toString();
      Log.d(str1, str2);
    } 
    try {
      this.mSystemServerInterface.finishSession(this.mId);
    } catch (RemoteException remoteException) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error destroying system-service session ");
      stringBuilder.append(this.mId);
      stringBuilder.append(" for ");
      stringBuilder.append(getDebugState());
      stringBuilder.append(": ");
      stringBuilder.append(remoteException);
      String str2 = stringBuilder.toString();
      Log.e(str1, str2);
    } 
    IContentCaptureDirectManager iContentCaptureDirectManager = this.mDirectServiceInterface;
    if (iContentCaptureDirectManager != null)
      iContentCaptureDirectManager.asBinder().unlinkToDeath(this.mDirectServiceVulture, 0); 
    this.mDirectServiceInterface = null;
  }
  
  private void resetSession(int paramInt) {
    boolean bool;
    if (ContentCaptureHelper.sVerbose) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleResetSession(");
      stringBuilder.append(getActivityName());
      stringBuilder.append("): from ");
      int i = this.mState;
      stringBuilder.append(getStateAsString(i));
      stringBuilder.append(" to ");
      stringBuilder.append(getStateAsString(paramInt));
      String str2 = stringBuilder.toString();
      Log.v(str1, str2);
    } 
    this.mState = paramInt;
    AtomicBoolean atomicBoolean = this.mDisabled;
    if ((paramInt & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    atomicBoolean.set(bool);
    this.mApplicationToken = null;
    this.mComponentName = null;
    this.mEvents = null;
    IContentCaptureDirectManager iContentCaptureDirectManager = this.mDirectServiceInterface;
    if (iContentCaptureDirectManager != null)
      iContentCaptureDirectManager.asBinder().unlinkToDeath(this.mDirectServiceVulture, 0); 
    this.mDirectServiceInterface = null;
    this.mHandler.removeMessages(1);
  }
  
  void internalNotifyViewAppeared(ViewNode.ViewStructureImpl paramViewStructureImpl) {
    notifyViewAppeared(this.mId, paramViewStructureImpl);
  }
  
  void internalNotifyViewDisappeared(AutofillId paramAutofillId) {
    notifyViewDisappeared(this.mId, paramAutofillId);
  }
  
  void internalNotifyViewTextChanged(AutofillId paramAutofillId, CharSequence paramCharSequence) {
    notifyViewTextChanged(this.mId, paramAutofillId, paramCharSequence);
  }
  
  void internalNotifyViewInsetsChanged(Insets paramInsets) {
    notifyViewInsetsChanged(this.mId, paramInsets);
  }
  
  public void internalNotifyViewTreeEvent(boolean paramBoolean) {
    notifyViewTreeEvent(this.mId, paramBoolean);
  }
  
  public void internalNotifySessionResumed() {
    notifySessionResumed(this.mId);
  }
  
  public void internalNotifySessionPaused() {
    notifySessionPaused(this.mId);
  }
  
  boolean isContentCaptureEnabled() {
    boolean bool;
    if (super.isContentCaptureEnabled() && this.mManager.isContentCaptureEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean isDisabled() {
    return this.mDisabled.get();
  }
  
  boolean setDisabled(boolean paramBoolean) {
    return this.mDisabled.compareAndSet(paramBoolean ^ true, paramBoolean);
  }
  
  void notifyChildSessionStarted(int paramInt1, int paramInt2, ContentCaptureContext paramContentCaptureContext) {
    ContentCaptureEvent contentCaptureEvent2 = new ContentCaptureEvent(paramInt2, -1);
    ContentCaptureEvent contentCaptureEvent1 = contentCaptureEvent2.setParentSessionId(paramInt1).setClientContext(paramContentCaptureContext);
    sendEvent(contentCaptureEvent1, true);
  }
  
  void notifyChildSessionFinished(int paramInt1, int paramInt2) {
    ContentCaptureEvent contentCaptureEvent = new ContentCaptureEvent(paramInt2, -2);
    contentCaptureEvent = contentCaptureEvent.setParentSessionId(paramInt1);
    sendEvent(contentCaptureEvent, true);
  }
  
  void notifyViewAppeared(int paramInt, ViewNode.ViewStructureImpl paramViewStructureImpl) {
    ContentCaptureEvent contentCaptureEvent2 = new ContentCaptureEvent(paramInt, 1);
    ViewNode viewNode = paramViewStructureImpl.mNode;
    ContentCaptureEvent contentCaptureEvent1 = contentCaptureEvent2.setViewNode(viewNode);
    sendEvent(contentCaptureEvent1);
  }
  
  public void notifyViewDisappeared(int paramInt, AutofillId paramAutofillId) {
    sendEvent((new ContentCaptureEvent(paramInt, 2)).setAutofillId(paramAutofillId));
  }
  
  void notifyViewTextChanged(int paramInt, AutofillId paramAutofillId, CharSequence paramCharSequence) {
    ContentCaptureEvent contentCaptureEvent = (new ContentCaptureEvent(paramInt, 3)).setAutofillId(paramAutofillId);
    contentCaptureEvent = contentCaptureEvent.setText(paramCharSequence);
    sendEvent(contentCaptureEvent);
  }
  
  public void notifyViewInsetsChanged(int paramInt, Insets paramInsets) {
    ContentCaptureEvent contentCaptureEvent2 = new ContentCaptureEvent(paramInt, 9);
    ContentCaptureEvent contentCaptureEvent1 = contentCaptureEvent2.setInsets(paramInsets);
    sendEvent(contentCaptureEvent1);
  }
  
  public void notifyViewTreeEvent(int paramInt, boolean paramBoolean) {
    byte b;
    if (paramBoolean) {
      b = 4;
    } else {
      b = 5;
    } 
    sendEvent(new ContentCaptureEvent(paramInt, b), true);
  }
  
  void notifySessionResumed(int paramInt) {
    sendEvent(new ContentCaptureEvent(paramInt, 7), true);
  }
  
  void notifySessionPaused(int paramInt) {
    sendEvent(new ContentCaptureEvent(paramInt, 8), true);
  }
  
  void notifyContextUpdated(int paramInt, ContentCaptureContext paramContentCaptureContext) {
    ContentCaptureEvent contentCaptureEvent2 = new ContentCaptureEvent(paramInt, 6);
    ContentCaptureEvent contentCaptureEvent1 = contentCaptureEvent2.setClientContext(paramContentCaptureContext);
    sendEvent(contentCaptureEvent1);
  }
  
  void dump(String paramString, PrintWriter paramPrintWriter) {
    super.dump(paramString, paramPrintWriter);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mContext: ");
    paramPrintWriter.println(this.mContext);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("user: ");
    paramPrintWriter.println(this.mContext.getUserId());
    if (this.mDirectServiceInterface != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mDirectServiceInterface: ");
      paramPrintWriter.println(this.mDirectServiceInterface);
    } 
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mDisabled: ");
    paramPrintWriter.println(this.mDisabled.get());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("isEnabled(): ");
    paramPrintWriter.println(isContentCaptureEnabled());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("state: ");
    paramPrintWriter.println(getStateAsString(this.mState));
    if (this.mApplicationToken != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("app token: ");
      paramPrintWriter.println(this.mApplicationToken);
    } 
    if (this.mComponentName != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("component name: ");
      paramPrintWriter.println(this.mComponentName.flattenToShortString());
    } 
    ArrayList<ContentCaptureEvent> arrayList = this.mEvents;
    if (arrayList != null && !arrayList.isEmpty()) {
      int i = this.mEvents.size();
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("buffered events: ");
      paramPrintWriter.print(i);
      paramPrintWriter.print('/');
      paramPrintWriter.println(this.mManager.mOptions.maxBufferSize);
      if (ContentCaptureHelper.sVerbose && i > 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        stringBuilder.append("  ");
        String str = stringBuilder.toString();
        for (byte b = 0; b < i; b++) {
          ContentCaptureEvent contentCaptureEvent = this.mEvents.get(b);
          paramPrintWriter.print(str);
          paramPrintWriter.print(b);
          paramPrintWriter.print(": ");
          contentCaptureEvent.dump(paramPrintWriter);
          paramPrintWriter.println();
        } 
      } 
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mNextFlushForTextChanged: ");
      paramPrintWriter.println(this.mNextFlushForTextChanged);
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("flush frequency: ");
      if (this.mNextFlushForTextChanged) {
        paramPrintWriter.println(this.mManager.mOptions.textChangeFlushingFrequencyMs);
      } else {
        paramPrintWriter.println(this.mManager.mOptions.idleFlushingFrequencyMs);
      } 
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("next flush: ");
      TimeUtils.formatDuration(this.mNextFlush - System.currentTimeMillis(), paramPrintWriter);
      paramPrintWriter.print(" (");
      paramPrintWriter.print(TimeUtils.logTimeOfDay(this.mNextFlush));
      paramPrintWriter.println(")");
    } 
    if (this.mFlushHistory != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("flush history:");
      this.mFlushHistory.reverseDump(null, paramPrintWriter, null);
      paramPrintWriter.println();
    } else {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("not logging flush history");
    } 
    super.dump(paramString, paramPrintWriter);
  }
  
  private String getActivityName() {
    String str;
    if (this.mComponentName == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("pkg:");
      stringBuilder.append(this.mContext.getPackageName());
      str = stringBuilder.toString();
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("act:");
      stringBuilder.append(this.mComponentName.flattenToShortString());
      str = stringBuilder.toString();
    } 
    return str;
  }
  
  private String getDebugState() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getActivityName());
    stringBuilder.append(" [state=");
    stringBuilder.append(getStateAsString(this.mState));
    stringBuilder.append(", disabled=");
    AtomicBoolean atomicBoolean = this.mDisabled;
    stringBuilder.append(atomicBoolean.get());
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private String getDebugState(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getDebugState());
    stringBuilder.append(", reason=");
    stringBuilder.append(getFlushReasonAsString(paramInt));
    return stringBuilder.toString();
  }
}
