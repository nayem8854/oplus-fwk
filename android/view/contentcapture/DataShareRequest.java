package android.view.contentcapture;

import android.annotation.NonNull;
import android.app.ActivityThread;
import android.content.LocusId;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;
import com.android.internal.util.Preconditions;
import java.util.Objects;

public final class DataShareRequest implements Parcelable {
  public DataShareRequest(LocusId paramLocusId, String paramString) {
    Preconditions.checkNotNull(paramString);
    this.mPackageName = ActivityThread.currentActivityThread().getApplication().getPackageName();
    this.mLocusId = paramLocusId;
    this.mMimeType = paramString;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public LocusId getLocusId() {
    return this.mLocusId;
  }
  
  public String getMimeType() {
    return this.mMimeType;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DataShareRequest { packageName = ");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append(", locusId = ");
    stringBuilder.append(this.mLocusId);
    stringBuilder.append(", mimeType = ");
    stringBuilder.append(this.mMimeType);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    String str1 = this.mPackageName, str2 = ((DataShareRequest)paramObject).mPackageName;
    if (Objects.equals(str1, str2)) {
      LocusId locusId1 = this.mLocusId, locusId2 = ((DataShareRequest)paramObject).mLocusId;
      if (Objects.equals(locusId1, locusId2)) {
        String str = this.mMimeType;
        paramObject = ((DataShareRequest)paramObject).mMimeType;
        if (Objects.equals(str, paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mPackageName);
    int j = Objects.hashCode(this.mLocusId);
    int k = Objects.hashCode(this.mMimeType);
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b = 0;
    if (this.mLocusId != null)
      b = (byte)(0x0 | 0x2); 
    paramParcel.writeByte(b);
    paramParcel.writeString(this.mPackageName);
    LocusId locusId = this.mLocusId;
    if (locusId != null)
      paramParcel.writeTypedObject((Parcelable)locusId, paramInt); 
    paramParcel.writeString(this.mMimeType);
  }
  
  public int describeContents() {
    return 0;
  }
  
  DataShareRequest(Parcel paramParcel) {
    LocusId locusId;
    byte b = paramParcel.readByte();
    String str2 = paramParcel.readString();
    if ((b & 0x2) == 0) {
      locusId = null;
    } else {
      locusId = (LocusId)paramParcel.readTypedObject(LocusId.CREATOR);
    } 
    String str1 = paramParcel.readString();
    this.mPackageName = str2;
    AnnotationValidations.validate(NonNull.class, null, str2);
    this.mLocusId = locusId;
    this.mMimeType = str1;
    AnnotationValidations.validate(NonNull.class, null, str1);
  }
  
  public static final Parcelable.Creator<DataShareRequest> CREATOR = new Parcelable.Creator<DataShareRequest>() {
      public DataShareRequest[] newArray(int param1Int) {
        return new DataShareRequest[param1Int];
      }
      
      public DataShareRequest createFromParcel(Parcel param1Parcel) {
        return new DataShareRequest(param1Parcel);
      }
    };
  
  private final LocusId mLocusId;
  
  private final String mMimeType;
  
  private final String mPackageName;
  
  @Deprecated
  private void __metadata() {}
}
