package android.view.contentcapture;

import android.annotation.SystemApi;
import android.graphics.Insets;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.autofill.AutofillId;
import com.android.internal.util.Preconditions;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

@SystemApi
public final class ContentCaptureEvent implements Parcelable {
  public static final Parcelable.Creator<ContentCaptureEvent> CREATOR;
  
  private static final String TAG = ContentCaptureEvent.class.getSimpleName();
  
  public static final int TYPE_CONTEXT_UPDATED = 6;
  
  public static final int TYPE_SESSION_FINISHED = -2;
  
  public static final int TYPE_SESSION_PAUSED = 8;
  
  public static final int TYPE_SESSION_RESUMED = 7;
  
  public static final int TYPE_SESSION_STARTED = -1;
  
  public static final int TYPE_VIEW_APPEARED = 1;
  
  public static final int TYPE_VIEW_DISAPPEARED = 2;
  
  public static final int TYPE_VIEW_INSETS_CHANGED = 9;
  
  public static final int TYPE_VIEW_TEXT_CHANGED = 3;
  
  public static final int TYPE_VIEW_TREE_APPEARED = 5;
  
  public static final int TYPE_VIEW_TREE_APPEARING = 4;
  
  private ContentCaptureContext mClientContext;
  
  private final long mEventTime;
  
  private AutofillId mId;
  
  private ArrayList<AutofillId> mIds;
  
  private Insets mInsets;
  
  private ViewNode mNode;
  
  private int mParentSessionId = 0;
  
  private final int mSessionId;
  
  private CharSequence mText;
  
  private final int mType;
  
  public ContentCaptureEvent(int paramInt1, int paramInt2, long paramLong) {
    this.mSessionId = paramInt1;
    this.mType = paramInt2;
    this.mEventTime = paramLong;
  }
  
  public ContentCaptureEvent(int paramInt1, int paramInt2) {
    this(paramInt1, paramInt2, System.currentTimeMillis());
  }
  
  public ContentCaptureEvent setAutofillId(AutofillId paramAutofillId) {
    this.mId = (AutofillId)Preconditions.checkNotNull(paramAutofillId);
    return this;
  }
  
  public ContentCaptureEvent setAutofillIds(ArrayList<AutofillId> paramArrayList) {
    this.mIds = (ArrayList<AutofillId>)Preconditions.checkNotNull(paramArrayList);
    return this;
  }
  
  public ContentCaptureEvent addAutofillId(AutofillId paramAutofillId) {
    Preconditions.checkNotNull(paramAutofillId);
    if (this.mIds == null) {
      String str;
      StringBuilder stringBuilder;
      ArrayList<AutofillId> arrayList = new ArrayList();
      AutofillId autofillId = this.mId;
      if (autofillId == null) {
        str = TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("addAutofillId(");
        stringBuilder.append(paramAutofillId);
        stringBuilder.append(") called without an initial id");
        Log.w(str, stringBuilder.toString());
      } else {
        str.add(stringBuilder);
        this.mId = null;
      } 
    } 
    this.mIds.add(paramAutofillId);
    return this;
  }
  
  public ContentCaptureEvent setParentSessionId(int paramInt) {
    this.mParentSessionId = paramInt;
    return this;
  }
  
  public ContentCaptureEvent setClientContext(ContentCaptureContext paramContentCaptureContext) {
    this.mClientContext = paramContentCaptureContext;
    return this;
  }
  
  public int getSessionId() {
    return this.mSessionId;
  }
  
  public int getParentSessionId() {
    return this.mParentSessionId;
  }
  
  public ContentCaptureContext getContentCaptureContext() {
    return this.mClientContext;
  }
  
  public ContentCaptureEvent setViewNode(ViewNode paramViewNode) {
    this.mNode = (ViewNode)Preconditions.checkNotNull(paramViewNode);
    return this;
  }
  
  public ContentCaptureEvent setText(CharSequence paramCharSequence) {
    this.mText = paramCharSequence;
    return this;
  }
  
  public ContentCaptureEvent setInsets(Insets paramInsets) {
    this.mInsets = paramInsets;
    return this;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public long getEventTime() {
    return this.mEventTime;
  }
  
  public ViewNode getViewNode() {
    return this.mNode;
  }
  
  public AutofillId getId() {
    return this.mId;
  }
  
  public List<AutofillId> getIds() {
    return this.mIds;
  }
  
  public CharSequence getText() {
    return this.mText;
  }
  
  public Insets getInsets() {
    return this.mInsets;
  }
  
  public void mergeEvent(ContentCaptureEvent paramContentCaptureEvent) {
    String str;
    Preconditions.checkNotNull(paramContentCaptureEvent);
    int i = paramContentCaptureEvent.getType();
    if (this.mType != i) {
      str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mergeEvent(");
      stringBuilder.append(getTypeAsString(i));
      stringBuilder.append(") cannot be merged with different eventType=");
      i = this.mType;
      stringBuilder.append(getTypeAsString(i));
      String str1 = stringBuilder.toString();
      Log.e(str, str1);
      return;
    } 
    if (i == 2) {
      String str1;
      List<AutofillId> list = str.getIds();
      AutofillId autofillId = str.getId();
      if (list != null) {
        if (autofillId != null) {
          str1 = TAG;
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("got TYPE_VIEW_DISAPPEARED event with both id and ids: ");
          stringBuilder1.append(str);
          Log.w(str1, stringBuilder1.toString());
        } 
        for (i = 0; i < list.size(); i++)
          addAutofillId(list.get(i)); 
        return;
      } 
      if (str1 != null) {
        addAutofillId((AutofillId)str1);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mergeEvent(): got TYPE_VIEW_DISAPPEARED event with neither id or ids: ");
      stringBuilder.append(str);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    if (i == 3) {
      setText(str.getText());
    } else {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mergeEvent(");
      stringBuilder.append(getTypeAsString(i));
      stringBuilder.append(") does not support this event type.");
      Log.e(str1, stringBuilder.toString());
    } 
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    paramPrintWriter.print("type=");
    paramPrintWriter.print(getTypeAsString(this.mType));
    paramPrintWriter.print(", time=");
    paramPrintWriter.print(this.mEventTime);
    if (this.mId != null) {
      paramPrintWriter.print(", id=");
      paramPrintWriter.print(this.mId);
    } 
    if (this.mIds != null) {
      paramPrintWriter.print(", ids=");
      paramPrintWriter.print(this.mIds);
    } 
    if (this.mNode != null) {
      paramPrintWriter.print(", mNode.id=");
      paramPrintWriter.print(this.mNode.getAutofillId());
    } 
    if (this.mSessionId != 0) {
      paramPrintWriter.print(", sessionId=");
      paramPrintWriter.print(this.mSessionId);
    } 
    if (this.mParentSessionId != 0) {
      paramPrintWriter.print(", parentSessionId=");
      paramPrintWriter.print(this.mParentSessionId);
    } 
    if (this.mText != null) {
      paramPrintWriter.print(", text=");
      paramPrintWriter.println(ContentCaptureHelper.getSanitizedString(this.mText));
    } 
    if (this.mClientContext != null) {
      paramPrintWriter.print(", context=");
      this.mClientContext.dump(paramPrintWriter);
      paramPrintWriter.println();
    } 
    if (this.mInsets != null) {
      paramPrintWriter.print(", insets=");
      paramPrintWriter.println(this.mInsets);
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("ContentCaptureEvent[type=");
    int i = this.mType;
    stringBuilder = stringBuilder.append(getTypeAsString(i));
    stringBuilder.append(", session=");
    stringBuilder.append(this.mSessionId);
    if (this.mType == -1 && this.mParentSessionId != 0) {
      stringBuilder.append(", parent=");
      stringBuilder.append(this.mParentSessionId);
    } 
    if (this.mId != null) {
      stringBuilder.append(", id=");
      stringBuilder.append(this.mId);
    } 
    if (this.mIds != null) {
      stringBuilder.append(", ids=");
      stringBuilder.append(this.mIds);
    } 
    ViewNode viewNode = this.mNode;
    if (viewNode != null) {
      String str = viewNode.getClassName();
      if (this.mNode != null) {
        stringBuilder.append(", class=");
        stringBuilder.append(str);
      } 
      stringBuilder.append(", id=");
      stringBuilder.append(this.mNode.getAutofillId());
    } 
    if (this.mText != null) {
      stringBuilder.append(", text=");
      stringBuilder.append(ContentCaptureHelper.getSanitizedString(this.mText));
    } 
    if (this.mClientContext != null) {
      stringBuilder.append(", context=");
      stringBuilder.append(this.mClientContext);
    } 
    if (this.mInsets != null) {
      stringBuilder.append(", insets=");
      stringBuilder.append(this.mInsets);
    } 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSessionId);
    paramParcel.writeInt(this.mType);
    paramParcel.writeLong(this.mEventTime);
    paramParcel.writeParcelable(this.mId, paramInt);
    paramParcel.writeTypedList(this.mIds);
    ViewNode.writeToParcel(paramParcel, this.mNode, paramInt);
    paramParcel.writeCharSequence(this.mText);
    int i = this.mType;
    if (i == -1 || i == -2)
      paramParcel.writeInt(this.mParentSessionId); 
    i = this.mType;
    if (i == -1 || i == 6)
      paramParcel.writeParcelable(this.mClientContext, paramInt); 
    if (this.mType == 9)
      paramParcel.writeParcelable((Parcelable)this.mInsets, paramInt); 
  }
  
  static {
    CREATOR = new Parcelable.Creator<ContentCaptureEvent>() {
        public ContentCaptureEvent createFromParcel(Parcel param1Parcel) {
          int i = param1Parcel.readInt();
          int j = param1Parcel.readInt();
          long l = param1Parcel.readLong();
          ContentCaptureEvent contentCaptureEvent = new ContentCaptureEvent(i, j, l);
          AutofillId autofillId = (AutofillId)param1Parcel.readParcelable(null);
          if (autofillId != null)
            contentCaptureEvent.setAutofillId(autofillId); 
          ArrayList<AutofillId> arrayList = param1Parcel.createTypedArrayList(AutofillId.CREATOR);
          if (arrayList != null)
            contentCaptureEvent.setAutofillIds(arrayList); 
          ViewNode viewNode = ViewNode.readFromParcel(param1Parcel);
          if (viewNode != null)
            contentCaptureEvent.setViewNode(viewNode); 
          contentCaptureEvent.setText(param1Parcel.readCharSequence());
          if (j == -1 || j == -2)
            contentCaptureEvent.setParentSessionId(param1Parcel.readInt()); 
          if (j == -1 || j == 6)
            contentCaptureEvent.setClientContext((ContentCaptureContext)param1Parcel.readParcelable(null)); 
          if (j == 9)
            contentCaptureEvent.setInsets((Insets)param1Parcel.readParcelable(null)); 
          return contentCaptureEvent;
        }
        
        public ContentCaptureEvent[] newArray(int param1Int) {
          return new ContentCaptureEvent[param1Int];
        }
      };
  }
  
  public static String getTypeAsString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("UKNOWN_TYPE: ");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 9:
        return "VIEW_INSETS_CHANGED";
      case 8:
        return "SESSION_PAUSED";
      case 7:
        return "SESSION_RESUMED";
      case 6:
        return "CONTEXT_UPDATED";
      case 5:
        return "VIEW_TREE_APPEARED";
      case 4:
        return "VIEW_TREE_APPEARING";
      case 3:
        return "VIEW_TEXT_CHANGED";
      case 2:
        return "VIEW_DISAPPEARED";
      case 1:
        return "VIEW_APPEARED";
      case -1:
        return "SESSION_STARTED";
      case -2:
        break;
    } 
    return "SESSION_FINISHED";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class EventType implements Annotation {}
}
