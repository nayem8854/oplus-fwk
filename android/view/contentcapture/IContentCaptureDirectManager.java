package android.view.contentcapture;

import android.content.ContentCaptureOptions;
import android.content.pm.ParceledListSlice;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IContentCaptureDirectManager extends IInterface {
  void sendEvents(ParceledListSlice paramParceledListSlice, int paramInt, ContentCaptureOptions paramContentCaptureOptions) throws RemoteException;
  
  class Default implements IContentCaptureDirectManager {
    public void sendEvents(ParceledListSlice param1ParceledListSlice, int param1Int, ContentCaptureOptions param1ContentCaptureOptions) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IContentCaptureDirectManager {
    private static final String DESCRIPTOR = "android.view.contentcapture.IContentCaptureDirectManager";
    
    static final int TRANSACTION_sendEvents = 1;
    
    public Stub() {
      attachInterface(this, "android.view.contentcapture.IContentCaptureDirectManager");
    }
    
    public static IContentCaptureDirectManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.contentcapture.IContentCaptureDirectManager");
      if (iInterface != null && iInterface instanceof IContentCaptureDirectManager)
        return (IContentCaptureDirectManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "sendEvents";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.contentcapture.IContentCaptureDirectManager");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.contentcapture.IContentCaptureDirectManager");
      if (param1Parcel1.readInt() != 0) {
        ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        ContentCaptureOptions contentCaptureOptions = (ContentCaptureOptions)ContentCaptureOptions.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      sendEvents((ParceledListSlice)param1Parcel2, param1Int1, (ContentCaptureOptions)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IContentCaptureDirectManager {
      public static IContentCaptureDirectManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.contentcapture.IContentCaptureDirectManager";
      }
      
      public void sendEvents(ParceledListSlice param2ParceledListSlice, int param2Int, ContentCaptureOptions param2ContentCaptureOptions) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.contentcapture.IContentCaptureDirectManager");
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2ContentCaptureOptions != null) {
            parcel.writeInt(1);
            param2ContentCaptureOptions.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IContentCaptureDirectManager.Stub.getDefaultImpl() != null) {
            IContentCaptureDirectManager.Stub.getDefaultImpl().sendEvents(param2ParceledListSlice, param2Int, param2ContentCaptureOptions);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IContentCaptureDirectManager param1IContentCaptureDirectManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IContentCaptureDirectManager != null) {
          Proxy.sDefaultImpl = param1IContentCaptureDirectManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IContentCaptureDirectManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
