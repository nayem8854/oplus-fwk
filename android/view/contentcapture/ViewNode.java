package android.view.contentcapture;

import android.annotation.SystemApi;
import android.app.assist.AssistStructure;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.Parcel;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.view.ViewStructure;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import com.android.internal.util.Preconditions;

@SystemApi
public final class ViewNode extends AssistStructure.ViewNode {
  private static final String TAG = ViewNode.class.getSimpleName();
  
  private int mId = -1;
  
  private int mMinEms = -1;
  
  private int mMaxEms = -1;
  
  private int mMaxLength = -1;
  
  private static final long FLAGS_ACCESSIBILITY_FOCUSED = 131072L;
  
  private static final long FLAGS_ACTIVATED = 2097152L;
  
  private static final long FLAGS_ASSIST_BLOCKED = 1024L;
  
  private static final long FLAGS_CHECKABLE = 262144L;
  
  private static final long FLAGS_CHECKED = 524288L;
  
  private static final long FLAGS_CLICKABLE = 4096L;
  
  private static final long FLAGS_CONTEXT_CLICKABLE = 16384L;
  
  private static final long FLAGS_DISABLED = 2048L;
  
  private static final long FLAGS_FOCUSABLE = 32768L;
  
  private static final long FLAGS_FOCUSED = 65536L;
  
  private static final long FLAGS_HAS_AUTOFILL_HINTS = 8589934592L;
  
  private static final long FLAGS_HAS_AUTOFILL_ID = 32L;
  
  private static final long FLAGS_HAS_AUTOFILL_OPTIONS = 17179869184L;
  
  private static final long FLAGS_HAS_AUTOFILL_PARENT_ID = 64L;
  
  private static final long FLAGS_HAS_AUTOFILL_TYPE = 2147483648L;
  
  private static final long FLAGS_HAS_AUTOFILL_VALUE = 4294967296L;
  
  private static final long FLAGS_HAS_CLASSNAME = 16L;
  
  private static final long FLAGS_HAS_COMPLEX_TEXT = 2L;
  
  private static final long FLAGS_HAS_CONTENT_DESCRIPTION = 8388608L;
  
  private static final long FLAGS_HAS_EXTRAS = 16777216L;
  
  private static final long FLAGS_HAS_HINT_ID_ENTRY = 34359738368L;
  
  private static final long FLAGS_HAS_ID = 128L;
  
  private static final long FLAGS_HAS_INPUT_TYPE = 67108864L;
  
  private static final long FLAGS_HAS_LARGE_COORDS = 256L;
  
  private static final long FLAGS_HAS_LOCALE_LIST = 33554432L;
  
  private static final long FLAGS_HAS_MAX_TEXT_EMS = 268435456L;
  
  private static final long FLAGS_HAS_MAX_TEXT_LENGTH = 536870912L;
  
  private static final long FLAGS_HAS_MIN_TEXT_EMS = 134217728L;
  
  private static final long FLAGS_HAS_SCROLL = 512L;
  
  private static final long FLAGS_HAS_TEXT = 1L;
  
  private static final long FLAGS_HAS_TEXT_ID_ENTRY = 1073741824L;
  
  private static final long FLAGS_LONG_CLICKABLE = 8192L;
  
  private static final long FLAGS_OPAQUE = 4194304L;
  
  private static final long FLAGS_SELECTED = 1048576L;
  
  private static final long FLAGS_VISIBILITY_MASK = 12L;
  
  private String[] mAutofillHints;
  
  private AutofillId mAutofillId;
  
  private CharSequence[] mAutofillOptions;
  
  private int mAutofillType;
  
  private AutofillValue mAutofillValue;
  
  private String mClassName;
  
  private CharSequence mContentDescription;
  
  private Bundle mExtras;
  
  private long mFlags;
  
  private int mHeight;
  
  private String mHintIdEntry;
  
  private String mIdEntry;
  
  private String mIdPackage;
  
  private String mIdType;
  
  private int mInputType;
  
  private LocaleList mLocaleList;
  
  private AutofillId mParentAutofillId;
  
  private int mScrollX;
  
  private int mScrollY;
  
  private ViewNodeText mText;
  
  private String mTextIdEntry;
  
  private int mWidth;
  
  private int mX;
  
  private int mY;
  
  public ViewNode() {
    this.mAutofillType = 0;
  }
  
  private ViewNode(long paramLong, Parcel paramParcel) {
    boolean bool = false;
    this.mAutofillType = 0;
    this.mFlags = paramLong;
    if ((0x20L & paramLong) != 0L)
      this.mAutofillId = (AutofillId)paramParcel.readParcelable(null); 
    if ((0x40L & paramLong) != 0L)
      this.mParentAutofillId = (AutofillId)paramParcel.readParcelable(null); 
    if ((0x1L & paramLong) != 0L) {
      if ((0x2L & paramLong) == 0L)
        bool = true; 
      this.mText = new ViewNodeText(paramParcel, bool);
    } 
    if ((0x10L & paramLong) != 0L)
      this.mClassName = paramParcel.readString(); 
    if ((0x80L & paramLong) != 0L) {
      int i = paramParcel.readInt();
      if (i != -1) {
        String str = paramParcel.readString();
        if (str != null) {
          this.mIdType = paramParcel.readString();
          this.mIdPackage = paramParcel.readString();
        } 
      } 
    } 
    if ((0x100L & paramLong) != 0L) {
      this.mX = paramParcel.readInt();
      this.mY = paramParcel.readInt();
      this.mWidth = paramParcel.readInt();
      this.mHeight = paramParcel.readInt();
    } else {
      int i = paramParcel.readInt();
      this.mX = i & 0x7FFF;
      this.mY = i >> 16 & 0x7FFF;
      i = paramParcel.readInt();
      this.mWidth = i & 0x7FFF;
      this.mHeight = i >> 16 & 0x7FFF;
    } 
    if ((0x200L & paramLong) != 0L) {
      this.mScrollX = paramParcel.readInt();
      this.mScrollY = paramParcel.readInt();
    } 
    if ((0x800000L & paramLong) != 0L)
      this.mContentDescription = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel); 
    if ((0x1000000L & paramLong) != 0L)
      this.mExtras = paramParcel.readBundle(); 
    if ((0x2000000L & paramLong) != 0L)
      this.mLocaleList = (LocaleList)paramParcel.readParcelable(null); 
    if ((0x4000000L & paramLong) != 0L)
      this.mInputType = paramParcel.readInt(); 
    if ((0x8000000L & paramLong) != 0L)
      this.mMinEms = paramParcel.readInt(); 
    if ((0x10000000L & paramLong) != 0L)
      this.mMaxEms = paramParcel.readInt(); 
    if ((0x20000000L & paramLong) != 0L)
      this.mMaxLength = paramParcel.readInt(); 
    if ((0x40000000L & paramLong) != 0L)
      this.mTextIdEntry = paramParcel.readString(); 
    if ((0x80000000L & paramLong) != 0L)
      this.mAutofillType = paramParcel.readInt(); 
    if ((0x200000000L & paramLong) != 0L)
      this.mAutofillHints = paramParcel.readStringArray(); 
    if ((0x100000000L & paramLong) != 0L)
      this.mAutofillValue = (AutofillValue)paramParcel.readParcelable(null); 
    if ((0x400000000L & paramLong) != 0L)
      this.mAutofillOptions = paramParcel.readCharSequenceArray(); 
    if ((0x800000000L & paramLong) != 0L)
      this.mHintIdEntry = paramParcel.readString(); 
  }
  
  public AutofillId getParentAutofillId() {
    return this.mParentAutofillId;
  }
  
  public AutofillId getAutofillId() {
    return this.mAutofillId;
  }
  
  public CharSequence getText() {
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      CharSequence charSequence = viewNodeText.mText;
    } else {
      viewNodeText = null;
    } 
    return (CharSequence)viewNodeText;
  }
  
  public String getClassName() {
    return this.mClassName;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public String getIdPackage() {
    return this.mIdPackage;
  }
  
  public String getIdType() {
    return this.mIdType;
  }
  
  public String getIdEntry() {
    return this.mIdEntry;
  }
  
  public int getLeft() {
    return this.mX;
  }
  
  public int getTop() {
    return this.mY;
  }
  
  public int getScrollX() {
    return this.mScrollX;
  }
  
  public int getScrollY() {
    return this.mScrollY;
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  public boolean isAssistBlocked() {
    boolean bool;
    if ((this.mFlags & 0x400L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isEnabled() {
    boolean bool;
    if ((this.mFlags & 0x800L) == 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isClickable() {
    boolean bool;
    if ((this.mFlags & 0x1000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isLongClickable() {
    boolean bool;
    if ((this.mFlags & 0x2000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isContextClickable() {
    boolean bool;
    if ((this.mFlags & 0x4000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isFocusable() {
    boolean bool;
    if ((this.mFlags & 0x8000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isFocused() {
    boolean bool;
    if ((this.mFlags & 0x10000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isAccessibilityFocused() {
    boolean bool;
    if ((this.mFlags & 0x20000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCheckable() {
    boolean bool;
    if ((this.mFlags & 0x40000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isChecked() {
    boolean bool;
    if ((this.mFlags & 0x80000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isSelected() {
    boolean bool;
    if ((this.mFlags & 0x100000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isActivated() {
    boolean bool;
    if ((this.mFlags & 0x200000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isOpaque() {
    boolean bool;
    if ((this.mFlags & 0x400000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public CharSequence getContentDescription() {
    return this.mContentDescription;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public String getHint() {
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      String str = viewNodeText.mHint;
    } else {
      viewNodeText = null;
    } 
    return (String)viewNodeText;
  }
  
  public String getHintIdEntry() {
    return this.mHintIdEntry;
  }
  
  public int getTextSelectionStart() {
    byte b;
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      b = viewNodeText.mTextSelectionStart;
    } else {
      b = -1;
    } 
    return b;
  }
  
  public int getTextSelectionEnd() {
    byte b;
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      b = viewNodeText.mTextSelectionEnd;
    } else {
      b = -1;
    } 
    return b;
  }
  
  public int getTextColor() {
    boolean bool;
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      bool = viewNodeText.mTextColor;
    } else {
      bool = true;
    } 
    return bool;
  }
  
  public int getTextBackgroundColor() {
    boolean bool;
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      bool = viewNodeText.mTextBackgroundColor;
    } else {
      bool = true;
    } 
    return bool;
  }
  
  public float getTextSize() {
    float f;
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      f = viewNodeText.mTextSize;
    } else {
      f = 0.0F;
    } 
    return f;
  }
  
  public int getTextStyle() {
    boolean bool;
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      bool = viewNodeText.mTextStyle;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int[] getTextLineCharOffsets() {
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      int[] arrayOfInt = viewNodeText.mLineCharOffsets;
    } else {
      viewNodeText = null;
    } 
    return (int[])viewNodeText;
  }
  
  public int[] getTextLineBaselines() {
    ViewNodeText viewNodeText = this.mText;
    if (viewNodeText != null) {
      int[] arrayOfInt = viewNodeText.mLineBaselines;
    } else {
      viewNodeText = null;
    } 
    return (int[])viewNodeText;
  }
  
  public int getVisibility() {
    return (int)(this.mFlags & 0xCL);
  }
  
  public int getInputType() {
    return this.mInputType;
  }
  
  public int getMinTextEms() {
    return this.mMinEms;
  }
  
  public int getMaxTextEms() {
    return this.mMaxEms;
  }
  
  public int getMaxTextLength() {
    return this.mMaxLength;
  }
  
  public String getTextIdEntry() {
    return this.mTextIdEntry;
  }
  
  public int getAutofillType() {
    return this.mAutofillType;
  }
  
  public String[] getAutofillHints() {
    return this.mAutofillHints;
  }
  
  public AutofillValue getAutofillValue() {
    return this.mAutofillValue;
  }
  
  public CharSequence[] getAutofillOptions() {
    return this.mAutofillOptions;
  }
  
  public LocaleList getLocaleList() {
    return this.mLocaleList;
  }
  
  private void writeSelfToParcel(Parcel paramParcel, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mFlags : J
    //   4: lstore_3
    //   5: lload_3
    //   6: lstore #5
    //   8: aload_0
    //   9: getfield mAutofillId : Landroid/view/autofill/AutofillId;
    //   12: ifnull -> 22
    //   15: lload_3
    //   16: ldc2_w 32
    //   19: lor
    //   20: lstore #5
    //   22: lload #5
    //   24: lstore_3
    //   25: aload_0
    //   26: getfield mParentAutofillId : Landroid/view/autofill/AutofillId;
    //   29: ifnull -> 39
    //   32: lload #5
    //   34: ldc2_w 64
    //   37: lor
    //   38: lstore_3
    //   39: aload_0
    //   40: getfield mText : Landroid/view/contentcapture/ViewNode$ViewNodeText;
    //   43: astore #7
    //   45: lload_3
    //   46: lstore #5
    //   48: aload #7
    //   50: ifnull -> 75
    //   53: lload_3
    //   54: lconst_1
    //   55: lor
    //   56: lstore_3
    //   57: lload_3
    //   58: lstore #5
    //   60: aload #7
    //   62: invokevirtual isSimple : ()Z
    //   65: ifne -> 75
    //   68: lload_3
    //   69: ldc2_w 2
    //   72: lor
    //   73: lstore #5
    //   75: lload #5
    //   77: lstore_3
    //   78: aload_0
    //   79: getfield mClassName : Ljava/lang/String;
    //   82: ifnull -> 92
    //   85: lload #5
    //   87: ldc2_w 16
    //   90: lor
    //   91: lstore_3
    //   92: lload_3
    //   93: lstore #5
    //   95: aload_0
    //   96: getfield mId : I
    //   99: iconst_m1
    //   100: if_icmpeq -> 110
    //   103: lload_3
    //   104: ldc2_w 128
    //   107: lor
    //   108: lstore #5
    //   110: aload_0
    //   111: getfield mX : I
    //   114: sipush #-32768
    //   117: iand
    //   118: ifne -> 183
    //   121: aload_0
    //   122: getfield mY : I
    //   125: sipush #-32768
    //   128: iand
    //   129: ifne -> 183
    //   132: aload_0
    //   133: getfield mWidth : I
    //   136: sipush #-32768
    //   139: iand
    //   140: ifeq -> 149
    //   143: iconst_1
    //   144: istore #8
    //   146: goto -> 152
    //   149: iconst_0
    //   150: istore #8
    //   152: aload_0
    //   153: getfield mHeight : I
    //   156: sipush #-32768
    //   159: iand
    //   160: ifeq -> 169
    //   163: iconst_1
    //   164: istore #9
    //   166: goto -> 172
    //   169: iconst_0
    //   170: istore #9
    //   172: lload #5
    //   174: lstore_3
    //   175: iload #8
    //   177: iload #9
    //   179: ior
    //   180: ifeq -> 190
    //   183: lload #5
    //   185: ldc2_w 256
    //   188: lor
    //   189: lstore_3
    //   190: aload_0
    //   191: getfield mScrollX : I
    //   194: ifne -> 207
    //   197: lload_3
    //   198: lstore #5
    //   200: aload_0
    //   201: getfield mScrollY : I
    //   204: ifeq -> 214
    //   207: lload_3
    //   208: ldc2_w 512
    //   211: lor
    //   212: lstore #5
    //   214: lload #5
    //   216: lstore_3
    //   217: aload_0
    //   218: getfield mContentDescription : Ljava/lang/CharSequence;
    //   221: ifnull -> 231
    //   224: lload #5
    //   226: ldc2_w 8388608
    //   229: lor
    //   230: lstore_3
    //   231: lload_3
    //   232: lstore #5
    //   234: aload_0
    //   235: getfield mExtras : Landroid/os/Bundle;
    //   238: ifnull -> 248
    //   241: lload_3
    //   242: ldc2_w 16777216
    //   245: lor
    //   246: lstore #5
    //   248: lload #5
    //   250: lstore_3
    //   251: aload_0
    //   252: getfield mLocaleList : Landroid/os/LocaleList;
    //   255: ifnull -> 265
    //   258: lload #5
    //   260: ldc2_w 33554432
    //   263: lor
    //   264: lstore_3
    //   265: lload_3
    //   266: lstore #10
    //   268: aload_0
    //   269: getfield mInputType : I
    //   272: ifeq -> 282
    //   275: lload_3
    //   276: ldc2_w 67108864
    //   279: lor
    //   280: lstore #10
    //   282: lload #10
    //   284: lstore #5
    //   286: aload_0
    //   287: getfield mMinEms : I
    //   290: iconst_m1
    //   291: if_icmple -> 302
    //   294: lload #10
    //   296: ldc2_w 134217728
    //   299: lor
    //   300: lstore #5
    //   302: lload #5
    //   304: lstore_3
    //   305: aload_0
    //   306: getfield mMaxEms : I
    //   309: iconst_m1
    //   310: if_icmple -> 320
    //   313: lload #5
    //   315: ldc2_w 268435456
    //   318: lor
    //   319: lstore_3
    //   320: lload_3
    //   321: lstore #5
    //   323: aload_0
    //   324: getfield mMaxLength : I
    //   327: iconst_m1
    //   328: if_icmple -> 338
    //   331: lload_3
    //   332: ldc2_w 536870912
    //   335: lor
    //   336: lstore #5
    //   338: lload #5
    //   340: lstore #10
    //   342: aload_0
    //   343: getfield mTextIdEntry : Ljava/lang/String;
    //   346: ifnull -> 357
    //   349: lload #5
    //   351: ldc2_w 1073741824
    //   354: lor
    //   355: lstore #10
    //   357: lload #10
    //   359: lstore_3
    //   360: aload_0
    //   361: getfield mAutofillValue : Landroid/view/autofill/AutofillValue;
    //   364: ifnull -> 374
    //   367: lload #10
    //   369: ldc2_w 4294967296
    //   372: lor
    //   373: lstore_3
    //   374: lload_3
    //   375: lstore #5
    //   377: aload_0
    //   378: getfield mAutofillType : I
    //   381: ifeq -> 391
    //   384: lload_3
    //   385: ldc2_w 2147483648
    //   388: lor
    //   389: lstore #5
    //   391: lload #5
    //   393: lstore_3
    //   394: aload_0
    //   395: getfield mAutofillHints : [Ljava/lang/String;
    //   398: ifnull -> 408
    //   401: lload #5
    //   403: ldc2_w 8589934592
    //   406: lor
    //   407: lstore_3
    //   408: lload_3
    //   409: lstore #5
    //   411: aload_0
    //   412: getfield mAutofillOptions : [Ljava/lang/CharSequence;
    //   415: ifnull -> 425
    //   418: lload_3
    //   419: ldc2_w 17179869184
    //   422: lor
    //   423: lstore #5
    //   425: lload #5
    //   427: lstore_3
    //   428: aload_0
    //   429: getfield mHintIdEntry : Ljava/lang/String;
    //   432: ifnull -> 442
    //   435: lload #5
    //   437: ldc2_w 34359738368
    //   440: lor
    //   441: lstore_3
    //   442: aload_1
    //   443: lload_3
    //   444: invokevirtual writeLong : (J)V
    //   447: lload_3
    //   448: ldc2_w 32
    //   451: land
    //   452: lconst_0
    //   453: lcmp
    //   454: ifeq -> 466
    //   457: aload_1
    //   458: aload_0
    //   459: getfield mAutofillId : Landroid/view/autofill/AutofillId;
    //   462: iload_2
    //   463: invokevirtual writeParcelable : (Landroid/os/Parcelable;I)V
    //   466: lload_3
    //   467: ldc2_w 64
    //   470: land
    //   471: lconst_0
    //   472: lcmp
    //   473: ifeq -> 485
    //   476: aload_1
    //   477: aload_0
    //   478: getfield mParentAutofillId : Landroid/view/autofill/AutofillId;
    //   481: iload_2
    //   482: invokevirtual writeParcelable : (Landroid/os/Parcelable;I)V
    //   485: lload_3
    //   486: lconst_1
    //   487: land
    //   488: lconst_0
    //   489: lcmp
    //   490: ifeq -> 526
    //   493: aload_0
    //   494: getfield mText : Landroid/view/contentcapture/ViewNode$ViewNodeText;
    //   497: astore #7
    //   499: lload_3
    //   500: ldc2_w 2
    //   503: land
    //   504: lconst_0
    //   505: lcmp
    //   506: ifne -> 515
    //   509: iconst_1
    //   510: istore #12
    //   512: goto -> 518
    //   515: iconst_0
    //   516: istore #12
    //   518: aload #7
    //   520: aload_1
    //   521: iload #12
    //   523: invokevirtual writeToParcel : (Landroid/os/Parcel;Z)V
    //   526: ldc2_w 16
    //   529: lload_3
    //   530: land
    //   531: lconst_0
    //   532: lcmp
    //   533: ifeq -> 544
    //   536: aload_1
    //   537: aload_0
    //   538: getfield mClassName : Ljava/lang/String;
    //   541: invokevirtual writeString : (Ljava/lang/String;)V
    //   544: lload_3
    //   545: ldc2_w 128
    //   548: land
    //   549: lconst_0
    //   550: lcmp
    //   551: ifeq -> 601
    //   554: aload_1
    //   555: aload_0
    //   556: getfield mId : I
    //   559: invokevirtual writeInt : (I)V
    //   562: aload_0
    //   563: getfield mId : I
    //   566: iconst_m1
    //   567: if_icmpeq -> 601
    //   570: aload_1
    //   571: aload_0
    //   572: getfield mIdEntry : Ljava/lang/String;
    //   575: invokevirtual writeString : (Ljava/lang/String;)V
    //   578: aload_0
    //   579: getfield mIdEntry : Ljava/lang/String;
    //   582: ifnull -> 601
    //   585: aload_1
    //   586: aload_0
    //   587: getfield mIdType : Ljava/lang/String;
    //   590: invokevirtual writeString : (Ljava/lang/String;)V
    //   593: aload_1
    //   594: aload_0
    //   595: getfield mIdPackage : Ljava/lang/String;
    //   598: invokevirtual writeString : (Ljava/lang/String;)V
    //   601: lload_3
    //   602: ldc2_w 256
    //   605: land
    //   606: lconst_0
    //   607: lcmp
    //   608: ifeq -> 646
    //   611: aload_1
    //   612: aload_0
    //   613: getfield mX : I
    //   616: invokevirtual writeInt : (I)V
    //   619: aload_1
    //   620: aload_0
    //   621: getfield mY : I
    //   624: invokevirtual writeInt : (I)V
    //   627: aload_1
    //   628: aload_0
    //   629: getfield mWidth : I
    //   632: invokevirtual writeInt : (I)V
    //   635: aload_1
    //   636: aload_0
    //   637: getfield mHeight : I
    //   640: invokevirtual writeInt : (I)V
    //   643: goto -> 678
    //   646: aload_1
    //   647: aload_0
    //   648: getfield mY : I
    //   651: bipush #16
    //   653: ishl
    //   654: aload_0
    //   655: getfield mX : I
    //   658: ior
    //   659: invokevirtual writeInt : (I)V
    //   662: aload_1
    //   663: aload_0
    //   664: getfield mHeight : I
    //   667: bipush #16
    //   669: ishl
    //   670: aload_0
    //   671: getfield mWidth : I
    //   674: ior
    //   675: invokevirtual writeInt : (I)V
    //   678: lload_3
    //   679: ldc2_w 512
    //   682: land
    //   683: lconst_0
    //   684: lcmp
    //   685: ifeq -> 704
    //   688: aload_1
    //   689: aload_0
    //   690: getfield mScrollX : I
    //   693: invokevirtual writeInt : (I)V
    //   696: aload_1
    //   697: aload_0
    //   698: getfield mScrollY : I
    //   701: invokevirtual writeInt : (I)V
    //   704: lload_3
    //   705: ldc2_w 8388608
    //   708: land
    //   709: lconst_0
    //   710: lcmp
    //   711: ifeq -> 723
    //   714: aload_0
    //   715: getfield mContentDescription : Ljava/lang/CharSequence;
    //   718: aload_1
    //   719: iconst_0
    //   720: invokestatic writeToParcel : (Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
    //   723: lload_3
    //   724: ldc2_w 16777216
    //   727: land
    //   728: lconst_0
    //   729: lcmp
    //   730: ifeq -> 741
    //   733: aload_1
    //   734: aload_0
    //   735: getfield mExtras : Landroid/os/Bundle;
    //   738: invokevirtual writeBundle : (Landroid/os/Bundle;)V
    //   741: lload_3
    //   742: ldc2_w 33554432
    //   745: land
    //   746: lconst_0
    //   747: lcmp
    //   748: ifeq -> 760
    //   751: aload_1
    //   752: aload_0
    //   753: getfield mLocaleList : Landroid/os/LocaleList;
    //   756: iconst_0
    //   757: invokevirtual writeParcelable : (Landroid/os/Parcelable;I)V
    //   760: lload_3
    //   761: ldc2_w 67108864
    //   764: land
    //   765: lconst_0
    //   766: lcmp
    //   767: ifeq -> 778
    //   770: aload_1
    //   771: aload_0
    //   772: getfield mInputType : I
    //   775: invokevirtual writeInt : (I)V
    //   778: lload_3
    //   779: ldc2_w 134217728
    //   782: land
    //   783: lconst_0
    //   784: lcmp
    //   785: ifeq -> 796
    //   788: aload_1
    //   789: aload_0
    //   790: getfield mMinEms : I
    //   793: invokevirtual writeInt : (I)V
    //   796: lload_3
    //   797: ldc2_w 268435456
    //   800: land
    //   801: lconst_0
    //   802: lcmp
    //   803: ifeq -> 814
    //   806: aload_1
    //   807: aload_0
    //   808: getfield mMaxEms : I
    //   811: invokevirtual writeInt : (I)V
    //   814: lload_3
    //   815: ldc2_w 536870912
    //   818: land
    //   819: lconst_0
    //   820: lcmp
    //   821: ifeq -> 832
    //   824: aload_1
    //   825: aload_0
    //   826: getfield mMaxLength : I
    //   829: invokevirtual writeInt : (I)V
    //   832: lload_3
    //   833: ldc2_w 1073741824
    //   836: land
    //   837: lconst_0
    //   838: lcmp
    //   839: ifeq -> 850
    //   842: aload_1
    //   843: aload_0
    //   844: getfield mTextIdEntry : Ljava/lang/String;
    //   847: invokevirtual writeString : (Ljava/lang/String;)V
    //   850: ldc2_w 2147483648
    //   853: lload_3
    //   854: land
    //   855: lconst_0
    //   856: lcmp
    //   857: ifeq -> 868
    //   860: aload_1
    //   861: aload_0
    //   862: getfield mAutofillType : I
    //   865: invokevirtual writeInt : (I)V
    //   868: ldc2_w 8589934592
    //   871: lload_3
    //   872: land
    //   873: lconst_0
    //   874: lcmp
    //   875: ifeq -> 886
    //   878: aload_1
    //   879: aload_0
    //   880: getfield mAutofillHints : [Ljava/lang/String;
    //   883: invokevirtual writeStringArray : ([Ljava/lang/String;)V
    //   886: lload_3
    //   887: ldc2_w 4294967296
    //   890: land
    //   891: lconst_0
    //   892: lcmp
    //   893: ifeq -> 905
    //   896: aload_1
    //   897: aload_0
    //   898: getfield mAutofillValue : Landroid/view/autofill/AutofillValue;
    //   901: iconst_0
    //   902: invokevirtual writeParcelable : (Landroid/os/Parcelable;I)V
    //   905: ldc2_w 17179869184
    //   908: lload_3
    //   909: land
    //   910: lconst_0
    //   911: lcmp
    //   912: ifeq -> 923
    //   915: aload_1
    //   916: aload_0
    //   917: getfield mAutofillOptions : [Ljava/lang/CharSequence;
    //   920: invokevirtual writeCharSequenceArray : ([Ljava/lang/CharSequence;)V
    //   923: ldc2_w 34359738368
    //   926: lload_3
    //   927: land
    //   928: lconst_0
    //   929: lcmp
    //   930: ifeq -> 941
    //   933: aload_1
    //   934: aload_0
    //   935: getfield mHintIdEntry : Ljava/lang/String;
    //   938: invokevirtual writeString : (Ljava/lang/String;)V
    //   941: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #474	-> 0
    //   #476	-> 5
    //   #477	-> 15
    //   #480	-> 22
    //   #481	-> 32
    //   #484	-> 39
    //   #485	-> 53
    //   #486	-> 57
    //   #487	-> 68
    //   #490	-> 75
    //   #491	-> 85
    //   #493	-> 92
    //   #494	-> 103
    //   #496	-> 110
    //   #498	-> 183
    //   #500	-> 190
    //   #501	-> 207
    //   #503	-> 214
    //   #504	-> 224
    //   #506	-> 231
    //   #507	-> 241
    //   #509	-> 248
    //   #510	-> 258
    //   #512	-> 265
    //   #513	-> 275
    //   #515	-> 282
    //   #516	-> 294
    //   #518	-> 302
    //   #519	-> 313
    //   #521	-> 320
    //   #522	-> 331
    //   #524	-> 338
    //   #525	-> 349
    //   #527	-> 357
    //   #528	-> 367
    //   #530	-> 374
    //   #531	-> 384
    //   #533	-> 391
    //   #534	-> 401
    //   #536	-> 408
    //   #537	-> 418
    //   #539	-> 425
    //   #540	-> 435
    //   #542	-> 442
    //   #544	-> 447
    //   #545	-> 457
    //   #547	-> 466
    //   #548	-> 476
    //   #550	-> 485
    //   #551	-> 493
    //   #553	-> 526
    //   #554	-> 536
    //   #556	-> 544
    //   #557	-> 554
    //   #558	-> 562
    //   #559	-> 570
    //   #560	-> 578
    //   #561	-> 585
    //   #562	-> 593
    //   #566	-> 601
    //   #567	-> 611
    //   #568	-> 619
    //   #569	-> 627
    //   #570	-> 635
    //   #572	-> 646
    //   #573	-> 662
    //   #575	-> 678
    //   #576	-> 688
    //   #577	-> 696
    //   #579	-> 704
    //   #580	-> 714
    //   #582	-> 723
    //   #583	-> 733
    //   #585	-> 741
    //   #586	-> 751
    //   #588	-> 760
    //   #589	-> 770
    //   #591	-> 778
    //   #592	-> 788
    //   #594	-> 796
    //   #595	-> 806
    //   #597	-> 814
    //   #598	-> 824
    //   #600	-> 832
    //   #601	-> 842
    //   #603	-> 850
    //   #604	-> 860
    //   #606	-> 868
    //   #607	-> 878
    //   #609	-> 886
    //   #610	-> 896
    //   #612	-> 905
    //   #613	-> 915
    //   #615	-> 923
    //   #616	-> 933
    //   #618	-> 941
  }
  
  public static void writeToParcel(Parcel paramParcel, ViewNode paramViewNode, int paramInt) {
    if (paramViewNode == null) {
      paramParcel.writeLong(0L);
    } else {
      paramViewNode.writeSelfToParcel(paramParcel, paramInt);
    } 
  }
  
  public static ViewNode readFromParcel(Parcel paramParcel) {
    ViewNode viewNode;
    long l = paramParcel.readLong();
    if (l == 0L) {
      paramParcel = null;
    } else {
      viewNode = new ViewNode(l, paramParcel);
    } 
    return viewNode;
  }
  
  public static final class ViewStructureImpl extends ViewStructure {
    final ViewNode mNode;
    
    public ViewStructureImpl(View param1View) {
      ViewNode viewNode = new ViewNode();
      ViewNode.access$002(viewNode, ((View)Preconditions.checkNotNull(param1View)).getAutofillId());
      ViewParent viewParent = param1View.getParent();
      if (viewParent instanceof View)
        ViewNode.access$102(this.mNode, ((View)viewParent).getAutofillId()); 
    }
    
    public ViewStructureImpl(AutofillId param1AutofillId, long param1Long, int param1Int) {
      ViewNode viewNode = new ViewNode();
      ViewNode.access$102(viewNode, (AutofillId)Preconditions.checkNotNull(param1AutofillId));
      ViewNode.access$002(this.mNode, new AutofillId(param1AutofillId, param1Long, param1Int));
    }
    
    public ViewNode getNode() {
      return this.mNode;
    }
    
    public void setId(int param1Int, String param1String1, String param1String2, String param1String3) {
      ViewNode.access$202(this.mNode, param1Int);
      ViewNode.access$302(this.mNode, param1String1);
      ViewNode.access$402(this.mNode, param1String2);
      ViewNode.access$502(this.mNode, param1String3);
    }
    
    public void setDimens(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      ViewNode.access$602(this.mNode, param1Int1);
      ViewNode.access$702(this.mNode, param1Int2);
      ViewNode.access$802(this.mNode, param1Int3);
      ViewNode.access$902(this.mNode, param1Int4);
      ViewNode.access$1002(this.mNode, param1Int5);
      ViewNode.access$1102(this.mNode, param1Int6);
    }
    
    public void setTransformation(Matrix param1Matrix) {
      Log.w(ViewNode.TAG, "setTransformation() is not supported");
    }
    
    public void setElevation(float param1Float) {
      Log.w(ViewNode.TAG, "setElevation() is not supported");
    }
    
    public void setAlpha(float param1Float) {
      Log.w(ViewNode.TAG, "setAlpha() is not supported");
    }
    
    public void setVisibility(int param1Int) {
      ViewNode viewNode = this.mNode;
      ViewNode.access$1302(viewNode, viewNode.mFlags & 0xFFFFFFFFFFFFFFF3L | param1Int & 0xCL);
    }
    
    public void setAssistBlocked(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 1024L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFFFBFFL | l2);
    }
    
    public void setEnabled(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 0L;
      } else {
        l2 = 2048L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFFF7FFL | l2);
    }
    
    public void setClickable(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 4096L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFFEFFFL | l2);
    }
    
    public void setLongClickable(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 8192L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFFDFFFL | l2);
    }
    
    public void setContextClickable(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 16384L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFFBFFFL | l2);
    }
    
    public void setFocusable(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 32768L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFF7FFFL | l2);
    }
    
    public void setFocused(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 65536L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFEFFFFL | l2);
    }
    
    public void setAccessibilityFocused(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 131072L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFDFFFFL | l2);
    }
    
    public void setCheckable(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 262144L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFFBFFFFL | l2);
    }
    
    public void setChecked(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 524288L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFF7FFFFL | l2);
    }
    
    public void setSelected(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 1048576L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFEFFFFFL | l2);
    }
    
    public void setActivated(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 2097152L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFDFFFFFL | l2);
    }
    
    public void setOpaque(boolean param1Boolean) {
      long l2;
      ViewNode viewNode = this.mNode;
      long l1 = viewNode.mFlags;
      if (param1Boolean) {
        l2 = 4194304L;
      } else {
        l2 = 0L;
      } 
      ViewNode.access$1302(viewNode, l1 & 0xFFFFFFFFFFBFFFFFL | l2);
    }
    
    public void setClassName(String param1String) {
      ViewNode.access$1402(this.mNode, param1String);
    }
    
    public void setContentDescription(CharSequence param1CharSequence) {
      ViewNode.access$1502(this.mNode, param1CharSequence);
    }
    
    public void setText(CharSequence param1CharSequence) {
      ViewNode.ViewNodeText viewNodeText = getNodeText();
      viewNodeText.mText = TextUtils.trimNoCopySpans(param1CharSequence);
      viewNodeText.mTextSelectionEnd = -1;
      viewNodeText.mTextSelectionStart = -1;
    }
    
    public void setText(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      ViewNode.ViewNodeText viewNodeText = getNodeText();
      viewNodeText.mText = TextUtils.trimNoCopySpans(param1CharSequence);
      viewNodeText.mTextSelectionStart = param1Int1;
      viewNodeText.mTextSelectionEnd = param1Int2;
    }
    
    public void setTextStyle(float param1Float, int param1Int1, int param1Int2, int param1Int3) {
      ViewNode.ViewNodeText viewNodeText = getNodeText();
      viewNodeText.mTextColor = param1Int1;
      viewNodeText.mTextBackgroundColor = param1Int2;
      viewNodeText.mTextSize = param1Float;
      viewNodeText.mTextStyle = param1Int3;
    }
    
    public void setTextLines(int[] param1ArrayOfint1, int[] param1ArrayOfint2) {
      ViewNode.ViewNodeText viewNodeText = getNodeText();
      viewNodeText.mLineCharOffsets = param1ArrayOfint1;
      viewNodeText.mLineBaselines = param1ArrayOfint2;
    }
    
    public void setTextIdEntry(String param1String) {
      ViewNode.access$1602(this.mNode, (String)Preconditions.checkNotNull(param1String));
    }
    
    public void setHint(CharSequence param1CharSequence) {
      ViewNode.ViewNodeText viewNodeText = getNodeText();
      if (param1CharSequence != null) {
        param1CharSequence = param1CharSequence.toString();
      } else {
        param1CharSequence = null;
      } 
      viewNodeText.mHint = (String)param1CharSequence;
    }
    
    public void setHintIdEntry(String param1String) {
      ViewNode.access$1702(this.mNode, (String)Preconditions.checkNotNull(param1String));
    }
    
    public CharSequence getText() {
      return this.mNode.getText();
    }
    
    public int getTextSelectionStart() {
      return this.mNode.getTextSelectionStart();
    }
    
    public int getTextSelectionEnd() {
      return this.mNode.getTextSelectionEnd();
    }
    
    public CharSequence getHint() {
      return this.mNode.getHint();
    }
    
    public Bundle getExtras() {
      if (this.mNode.mExtras != null)
        return this.mNode.mExtras; 
      ViewNode.access$1802(this.mNode, new Bundle());
      return this.mNode.mExtras;
    }
    
    public boolean hasExtras() {
      boolean bool;
      if (this.mNode.mExtras != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void setChildCount(int param1Int) {
      Log.w(ViewNode.TAG, "setChildCount() is not supported");
    }
    
    public int addChildCount(int param1Int) {
      Log.w(ViewNode.TAG, "addChildCount() is not supported");
      return 0;
    }
    
    public int getChildCount() {
      Log.w(ViewNode.TAG, "getChildCount() is not supported");
      return 0;
    }
    
    public ViewStructure newChild(int param1Int) {
      Log.w(ViewNode.TAG, "newChild() is not supported");
      return null;
    }
    
    public ViewStructure asyncNewChild(int param1Int) {
      Log.w(ViewNode.TAG, "asyncNewChild() is not supported");
      return null;
    }
    
    public AutofillId getAutofillId() {
      return this.mNode.mAutofillId;
    }
    
    public void setAutofillId(AutofillId param1AutofillId) {
      ViewNode.access$002(this.mNode, (AutofillId)Preconditions.checkNotNull(param1AutofillId));
    }
    
    public void setAutofillId(AutofillId param1AutofillId, int param1Int) {
      ViewNode.access$102(this.mNode, (AutofillId)Preconditions.checkNotNull(param1AutofillId));
      ViewNode.access$002(this.mNode, new AutofillId(param1AutofillId, param1Int));
    }
    
    public void setAutofillType(int param1Int) {
      ViewNode.access$1902(this.mNode, param1Int);
    }
    
    public void setAutofillHints(String[] param1ArrayOfString) {
      ViewNode.access$2002(this.mNode, param1ArrayOfString);
    }
    
    public void setAutofillValue(AutofillValue param1AutofillValue) {
      ViewNode.access$2102(this.mNode, param1AutofillValue);
    }
    
    public void setAutofillOptions(CharSequence[] param1ArrayOfCharSequence) {
      ViewNode.access$2202(this.mNode, param1ArrayOfCharSequence);
    }
    
    public void setInputType(int param1Int) {
      ViewNode.access$2302(this.mNode, param1Int);
    }
    
    public void setMinTextEms(int param1Int) {
      ViewNode.access$2402(this.mNode, param1Int);
    }
    
    public void setMaxTextEms(int param1Int) {
      ViewNode.access$2502(this.mNode, param1Int);
    }
    
    public void setMaxTextLength(int param1Int) {
      ViewNode.access$2602(this.mNode, param1Int);
    }
    
    public void setDataIsSensitive(boolean param1Boolean) {
      Log.w(ViewNode.TAG, "setDataIsSensitive() is not supported");
    }
    
    public void asyncCommit() {
      Log.w(ViewNode.TAG, "asyncCommit() is not supported");
    }
    
    public Rect getTempRect() {
      Log.w(ViewNode.TAG, "getTempRect() is not supported");
      return null;
    }
    
    public void setWebDomain(String param1String) {
      Log.w(ViewNode.TAG, "setWebDomain() is not supported");
    }
    
    public void setLocaleList(LocaleList param1LocaleList) {
      ViewNode.access$2702(this.mNode, param1LocaleList);
    }
    
    public ViewStructure.HtmlInfo.Builder newHtmlInfoBuilder(String param1String) {
      Log.w(ViewNode.TAG, "newHtmlInfoBuilder() is not supported");
      return null;
    }
    
    public void setHtmlInfo(ViewStructure.HtmlInfo param1HtmlInfo) {
      Log.w(ViewNode.TAG, "setHtmlInfo() is not supported");
    }
    
    private ViewNode.ViewNodeText getNodeText() {
      if (this.mNode.mText != null)
        return this.mNode.mText; 
      ViewNode.access$2802(this.mNode, new ViewNode.ViewNodeText());
      return this.mNode.mText;
    }
  }
  
  class ViewNodeText {
    int mTextStyle;
    
    float mTextSize;
    
    int mTextSelectionStart;
    
    int mTextSelectionEnd;
    
    int mTextColor = 1;
    
    int mTextBackgroundColor = 1;
    
    CharSequence mText;
    
    int[] mLineCharOffsets;
    
    int[] mLineBaselines;
    
    String mHint;
    
    boolean isSimple() {
      int i = this.mTextBackgroundColor;
      boolean bool = true;
      if (i != 1 || this.mTextSelectionStart != 0 || this.mTextSelectionEnd != 0 || this.mLineCharOffsets != null || this.mLineBaselines != null || this.mHint != null)
        bool = false; 
      return bool;
    }
    
    ViewNodeText(ViewNode this$0, boolean param1Boolean) {
      this.mText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)this$0);
      this.mTextSize = this$0.readFloat();
      this.mTextStyle = this$0.readInt();
      this.mTextColor = this$0.readInt();
      if (!param1Boolean) {
        this.mTextBackgroundColor = this$0.readInt();
        this.mTextSelectionStart = this$0.readInt();
        this.mTextSelectionEnd = this$0.readInt();
        this.mLineCharOffsets = this$0.createIntArray();
        this.mLineBaselines = this$0.createIntArray();
        this.mHint = this$0.readString();
      } 
    }
    
    void writeToParcel(Parcel param1Parcel, boolean param1Boolean) {
      TextUtils.writeToParcel(this.mText, param1Parcel, 0);
      param1Parcel.writeFloat(this.mTextSize);
      param1Parcel.writeInt(this.mTextStyle);
      param1Parcel.writeInt(this.mTextColor);
      if (!param1Boolean) {
        param1Parcel.writeInt(this.mTextBackgroundColor);
        param1Parcel.writeInt(this.mTextSelectionStart);
        param1Parcel.writeInt(this.mTextSelectionEnd);
        param1Parcel.writeIntArray(this.mLineCharOffsets);
        param1Parcel.writeIntArray(this.mLineBaselines);
        param1Parcel.writeString(this.mHint);
      } 
    }
    
    ViewNodeText() {}
  }
}
