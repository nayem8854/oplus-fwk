package android.view.contentcapture;

import android.graphics.Insets;
import android.view.autofill.AutofillId;

final class ChildContentCaptureSession extends ContentCaptureSession {
  private final ContentCaptureSession mParent;
  
  protected ChildContentCaptureSession(ContentCaptureSession paramContentCaptureSession, ContentCaptureContext paramContentCaptureContext) {
    super(paramContentCaptureContext);
    this.mParent = paramContentCaptureSession;
  }
  
  MainContentCaptureSession getMainCaptureSession() {
    ContentCaptureSession contentCaptureSession = this.mParent;
    if (contentCaptureSession instanceof MainContentCaptureSession)
      return (MainContentCaptureSession)contentCaptureSession; 
    return contentCaptureSession.getMainCaptureSession();
  }
  
  ContentCaptureSession newChild(ContentCaptureContext paramContentCaptureContext) {
    ChildContentCaptureSession childContentCaptureSession = new ChildContentCaptureSession(this, paramContentCaptureContext);
    getMainCaptureSession().notifyChildSessionStarted(this.mId, childContentCaptureSession.mId, paramContentCaptureContext);
    return childContentCaptureSession;
  }
  
  void flush(int paramInt) {
    this.mParent.flush(paramInt);
  }
  
  public void updateContentCaptureContext(ContentCaptureContext paramContentCaptureContext) {
    getMainCaptureSession().notifyContextUpdated(this.mId, paramContentCaptureContext);
  }
  
  void onDestroy() {
    getMainCaptureSession().notifyChildSessionFinished(this.mParent.mId, this.mId);
  }
  
  void internalNotifyViewAppeared(ViewNode.ViewStructureImpl paramViewStructureImpl) {
    getMainCaptureSession().notifyViewAppeared(this.mId, paramViewStructureImpl);
  }
  
  void internalNotifyViewDisappeared(AutofillId paramAutofillId) {
    getMainCaptureSession().notifyViewDisappeared(this.mId, paramAutofillId);
  }
  
  void internalNotifyViewTextChanged(AutofillId paramAutofillId, CharSequence paramCharSequence) {
    getMainCaptureSession().notifyViewTextChanged(this.mId, paramAutofillId, paramCharSequence);
  }
  
  void internalNotifyViewInsetsChanged(Insets paramInsets) {
    getMainCaptureSession().notifyViewInsetsChanged(this.mId, paramInsets);
  }
  
  public void internalNotifyViewTreeEvent(boolean paramBoolean) {
    getMainCaptureSession().notifyViewTreeEvent(this.mId, paramBoolean);
  }
  
  void internalNotifySessionResumed() {
    getMainCaptureSession().notifySessionResumed();
  }
  
  void internalNotifySessionPaused() {
    getMainCaptureSession().notifySessionPaused();
  }
  
  boolean isContentCaptureEnabled() {
    return getMainCaptureSession().isContentCaptureEnabled();
  }
}
