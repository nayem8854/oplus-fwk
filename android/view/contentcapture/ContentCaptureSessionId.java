package android.view.contentcapture;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.PrintWriter;

public final class ContentCaptureSessionId implements Parcelable {
  public ContentCaptureSessionId(int paramInt) {
    this.mValue = paramInt;
  }
  
  public int getValue() {
    return this.mValue;
  }
  
  public int hashCode() {
    int i = this.mValue;
    return 1 * 31 + i;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mValue != ((ContentCaptureSessionId)paramObject).mValue)
      return false; 
    return true;
  }
  
  public String toString() {
    return Integer.toString(this.mValue);
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    paramPrintWriter.print(this.mValue);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mValue);
  }
  
  public static final Parcelable.Creator<ContentCaptureSessionId> CREATOR = new Parcelable.Creator<ContentCaptureSessionId>() {
      public ContentCaptureSessionId createFromParcel(Parcel param1Parcel) {
        return new ContentCaptureSessionId(param1Parcel.readInt());
      }
      
      public ContentCaptureSessionId[] newArray(int param1Int) {
        return new ContentCaptureSessionId[param1Int];
      }
    };
  
  private final int mValue;
}
