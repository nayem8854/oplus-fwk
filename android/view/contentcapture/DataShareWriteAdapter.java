package android.view.contentcapture;

import android.os.ParcelFileDescriptor;

public interface DataShareWriteAdapter {
  default void onError(int paramInt) {}
  
  void onRejected();
  
  void onWrite(ParcelFileDescriptor paramParcelFileDescriptor);
}
