package android.view.contentcapture;

import android.content.LocusId;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DebugUtils;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class ContentCaptureCondition implements Parcelable {
  public ContentCaptureCondition(LocusId paramLocusId, int paramInt) {
    this.mLocusId = (LocusId)Preconditions.checkNotNull(paramLocusId);
    this.mFlags = paramInt;
  }
  
  public LocusId getLocusId() {
    return this.mLocusId;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public int hashCode() {
    int j, i = this.mFlags;
    LocusId locusId = this.mLocusId;
    if (locusId == null) {
      j = 0;
    } else {
      j = locusId.hashCode();
    } 
    return (1 * 31 + i) * 31 + j;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    ContentCaptureCondition contentCaptureCondition = (ContentCaptureCondition)paramObject;
    if (this.mFlags != contentCaptureCondition.mFlags)
      return false; 
    paramObject = this.mLocusId;
    if (paramObject == null) {
      if (contentCaptureCondition.mLocusId != null)
        return false; 
    } else if (!paramObject.equals(contentCaptureCondition.mLocusId)) {
      return false;
    } 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(this.mLocusId.toString());
    if (this.mFlags != 0) {
      stringBuilder.append(" (");
      int i = this.mFlags;
      stringBuilder.append(DebugUtils.flagsToString(ContentCaptureCondition.class, "FLAG_", i));
      stringBuilder.append(')');
    } 
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mLocusId, paramInt);
    paramParcel.writeInt(this.mFlags);
  }
  
  public static final Parcelable.Creator<ContentCaptureCondition> CREATOR = new Parcelable.Creator<ContentCaptureCondition>() {
      public ContentCaptureCondition createFromParcel(Parcel param1Parcel) {
        LocusId locusId = (LocusId)param1Parcel.readParcelable(null);
        return 
          new ContentCaptureCondition(locusId, param1Parcel.readInt());
      }
      
      public ContentCaptureCondition[] newArray(int param1Int) {
        return new ContentCaptureCondition[param1Int];
      }
    };
  
  public static final int FLAG_IS_REGEX = 2;
  
  private final int mFlags;
  
  private final LocusId mLocusId;
  
  @Retention(RetentionPolicy.SOURCE)
  class Flags implements Annotation {}
}
