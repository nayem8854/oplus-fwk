package android.view.contentcapture;

import android.app.ActivityThread;
import android.content.LocusId;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.IntArray;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public final class DataRemovalRequest implements Parcelable {
  private DataRemovalRequest(Builder paramBuilder) {
    this.mPackageName = ActivityThread.currentActivityThread().getApplication().getPackageName();
    this.mForEverything = paramBuilder.mForEverything;
    if (paramBuilder.mLocusIds != null) {
      int i = paramBuilder.mLocusIds.size();
      this.mLocusIdRequests = new ArrayList<>(i);
      for (byte b = 0; b < i; b++) {
        ArrayList<LocusIdRequest> arrayList = this.mLocusIdRequests;
        LocusId locusId = paramBuilder.mLocusIds.get(b);
        LocusIdRequest locusIdRequest = new LocusIdRequest(locusId, paramBuilder.mFlags.get(b));
        arrayList.add(locusIdRequest);
      } 
    } 
  }
  
  private DataRemovalRequest(Parcel paramParcel) {
    this.mPackageName = paramParcel.readString();
    boolean bool = paramParcel.readBoolean();
    if (!bool) {
      int i = paramParcel.readInt();
      this.mLocusIdRequests = new ArrayList<>(i);
      for (byte b = 0; b < i; b++) {
        ArrayList<LocusIdRequest> arrayList = this.mLocusIdRequests;
        LocusId locusId = (LocusId)paramParcel.readValue(null);
        LocusIdRequest locusIdRequest = new LocusIdRequest(locusId, paramParcel.readInt());
        arrayList.add(locusIdRequest);
      } 
    } 
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public boolean isForEverything() {
    return this.mForEverything;
  }
  
  public List<LocusIdRequest> getLocusIdRequests() {
    return this.mLocusIdRequests;
  }
  
  class Builder {
    private boolean mDestroyed;
    
    private IntArray mFlags;
    
    private boolean mForEverything;
    
    private ArrayList<LocusId> mLocusIds;
    
    public Builder forEverything() {
      boolean bool;
      throwIfDestroyed();
      if (this.mLocusIds == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Already added LocusIds");
      this.mForEverything = true;
      return this;
    }
    
    public Builder addLocusId(LocusId param1LocusId, int param1Int) {
      throwIfDestroyed();
      Preconditions.checkState(this.mForEverything ^ true, "Already is for everything");
      Preconditions.checkNotNull(param1LocusId);
      if (this.mLocusIds == null) {
        this.mLocusIds = new ArrayList<>();
        this.mFlags = new IntArray();
      } 
      this.mLocusIds.add(param1LocusId);
      this.mFlags.add(param1Int);
      return this;
    }
    
    public DataRemovalRequest build() {
      throwIfDestroyed();
      if (this.mForEverything || this.mLocusIds != null) {
        boolean bool1 = true;
        Preconditions.checkState(bool1, "must call either #forEverything() or add one #addLocusId()");
        this.mDestroyed = true;
        return new DataRemovalRequest(this);
      } 
      boolean bool = false;
      Preconditions.checkState(bool, "must call either #forEverything() or add one #addLocusId()");
      this.mDestroyed = true;
      return new DataRemovalRequest(this);
    }
    
    private void throwIfDestroyed() {
      Preconditions.checkState(this.mDestroyed ^ true, "Already destroyed!");
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeBoolean(this.mForEverything);
    if (!this.mForEverything) {
      int i = this.mLocusIdRequests.size();
      paramParcel.writeInt(i);
      for (paramInt = 0; paramInt < i; paramInt++) {
        LocusIdRequest locusIdRequest = this.mLocusIdRequests.get(paramInt);
        paramParcel.writeValue(locusIdRequest.getLocusId());
        paramParcel.writeInt(locusIdRequest.getFlags());
      } 
    } 
  }
  
  public static final Parcelable.Creator<DataRemovalRequest> CREATOR = new Parcelable.Creator<DataRemovalRequest>() {
      public DataRemovalRequest createFromParcel(Parcel param1Parcel) {
        return new DataRemovalRequest(param1Parcel);
      }
      
      public DataRemovalRequest[] newArray(int param1Int) {
        return new DataRemovalRequest[param1Int];
      }
    };
  
  public static final int FLAG_IS_PREFIX = 1;
  
  private final boolean mForEverything;
  
  private ArrayList<LocusIdRequest> mLocusIdRequests;
  
  private final String mPackageName;
  
  class LocusIdRequest {
    private final int mFlags;
    
    private final LocusId mLocusId;
    
    final DataRemovalRequest this$0;
    
    private LocusIdRequest(LocusId param1LocusId, int param1Int) {
      this.mLocusId = param1LocusId;
      this.mFlags = param1Int;
    }
    
    public LocusId getLocusId() {
      return this.mLocusId;
    }
    
    public int getFlags() {
      return this.mFlags;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Flags implements Annotation {}
}
