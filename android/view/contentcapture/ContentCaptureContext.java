package android.view.contentcapture;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.LocusId;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class ContentCaptureContext implements Parcelable {
  private final int mTaskId;
  
  private int mParentSessionId = 0;
  
  private final LocusId mId;
  
  private final boolean mHasClientContext;
  
  private final int mFlags;
  
  private final Bundle mExtras;
  
  private final int mDisplayId;
  
  private final ComponentName mComponentName;
  
  @SystemApi
  public static final int FLAG_RECONNECTED = 4;
  
  @SystemApi
  public static final int FLAG_DISABLED_BY_FLAG_SECURE = 2;
  
  @SystemApi
  public static final int FLAG_DISABLED_BY_APP = 1;
  
  public ContentCaptureContext(ContentCaptureContext paramContentCaptureContext, ComponentName paramComponentName, int paramInt1, int paramInt2, int paramInt3) {
    if (paramContentCaptureContext != null) {
      this.mHasClientContext = true;
      this.mExtras = paramContentCaptureContext.mExtras;
      this.mId = paramContentCaptureContext.mId;
    } else {
      this.mHasClientContext = false;
      this.mExtras = null;
      this.mId = null;
    } 
    this.mComponentName = (ComponentName)Preconditions.checkNotNull(paramComponentName);
    this.mTaskId = paramInt1;
    this.mDisplayId = paramInt2;
    this.mFlags = paramInt3;
  }
  
  private ContentCaptureContext(Builder paramBuilder) {
    this.mHasClientContext = true;
    this.mExtras = paramBuilder.mExtras;
    this.mId = paramBuilder.mId;
    this.mComponentName = null;
    this.mFlags = 0;
    this.mTaskId = 0;
    this.mDisplayId = -1;
  }
  
  public ContentCaptureContext(ContentCaptureContext paramContentCaptureContext, int paramInt) {
    this.mHasClientContext = paramContentCaptureContext.mHasClientContext;
    this.mExtras = paramContentCaptureContext.mExtras;
    this.mId = paramContentCaptureContext.mId;
    this.mComponentName = paramContentCaptureContext.mComponentName;
    this.mTaskId = paramContentCaptureContext.mTaskId;
    paramContentCaptureContext.mFlags |= paramInt;
    this.mDisplayId = paramContentCaptureContext.mDisplayId;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public LocusId getLocusId() {
    return this.mId;
  }
  
  @SystemApi
  public int getTaskId() {
    return this.mTaskId;
  }
  
  @SystemApi
  public ComponentName getActivityComponent() {
    return this.mComponentName;
  }
  
  @SystemApi
  public ContentCaptureSessionId getParentSessionId() {
    ContentCaptureSessionId contentCaptureSessionId;
    if (this.mParentSessionId == 0) {
      contentCaptureSessionId = null;
    } else {
      contentCaptureSessionId = new ContentCaptureSessionId(this.mParentSessionId);
    } 
    return contentCaptureSessionId;
  }
  
  public void setParentSessionId(int paramInt) {
    this.mParentSessionId = paramInt;
  }
  
  @SystemApi
  public int getDisplayId() {
    return this.mDisplayId;
  }
  
  @SystemApi
  public int getFlags() {
    return this.mFlags;
  }
  
  public static ContentCaptureContext forLocusId(String paramString) {
    return (new Builder(new LocusId(paramString))).build();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ContextCreationFlags implements Annotation {}
  
  class Builder {
    private boolean mDestroyed;
    
    private Bundle mExtras;
    
    private final LocusId mId;
    
    public Builder(ContentCaptureContext this$0) {
      this.mId = (LocusId)Preconditions.checkNotNull(this$0);
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = (Bundle)Preconditions.checkNotNull(param1Bundle);
      throwIfDestroyed();
      return this;
    }
    
    public ContentCaptureContext build() {
      throwIfDestroyed();
      this.mDestroyed = true;
      return new ContentCaptureContext(this);
    }
    
    private void throwIfDestroyed() {
      Preconditions.checkState(this.mDestroyed ^ true, "Already called #build()");
    }
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    if (this.mComponentName != null) {
      paramPrintWriter.print("activity=");
      paramPrintWriter.print(this.mComponentName.flattenToShortString());
    } 
    if (this.mId != null) {
      paramPrintWriter.print(", id=");
      this.mId.dump(paramPrintWriter);
    } 
    paramPrintWriter.print(", taskId=");
    paramPrintWriter.print(this.mTaskId);
    paramPrintWriter.print(", displayId=");
    paramPrintWriter.print(this.mDisplayId);
    if (this.mParentSessionId != 0) {
      paramPrintWriter.print(", parentId=");
      paramPrintWriter.print(this.mParentSessionId);
    } 
    if (this.mFlags > 0) {
      paramPrintWriter.print(", flags=");
      paramPrintWriter.print(this.mFlags);
    } 
    if (this.mExtras != null)
      paramPrintWriter.print(", hasExtras"); 
  }
  
  private boolean fromServer() {
    boolean bool;
    if (this.mComponentName != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("Context[");
    if (fromServer()) {
      stringBuilder.append("act=");
      stringBuilder.append(ComponentName.flattenToShortString(this.mComponentName));
      stringBuilder.append(", taskId=");
      stringBuilder.append(this.mTaskId);
      stringBuilder.append(", displayId=");
      stringBuilder.append(this.mDisplayId);
      stringBuilder.append(", flags=");
      stringBuilder.append(this.mFlags);
    } else {
      stringBuilder.append("id=");
      stringBuilder.append(this.mId);
      if (this.mExtras != null)
        stringBuilder.append(", hasExtras"); 
    } 
    if (this.mParentSessionId != 0) {
      stringBuilder.append(", parentId=");
      stringBuilder.append(this.mParentSessionId);
    } 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mHasClientContext);
    if (this.mHasClientContext) {
      paramParcel.writeParcelable((Parcelable)this.mId, paramInt);
      paramParcel.writeBundle(this.mExtras);
    } 
    paramParcel.writeParcelable((Parcelable)this.mComponentName, paramInt);
    if (fromServer()) {
      paramParcel.writeInt(this.mTaskId);
      paramParcel.writeInt(this.mDisplayId);
      paramParcel.writeInt(this.mFlags);
    } 
  }
  
  public static final Parcelable.Creator<ContentCaptureContext> CREATOR = new Parcelable.Creator<ContentCaptureContext>() {
      public ContentCaptureContext createFromParcel(Parcel param1Parcel) {
        ContentCaptureContext contentCaptureContext;
        int i = param1Parcel.readInt(), j = 1;
        if (i != 1)
          j = 0; 
        if (j) {
          LocusId locusId = (LocusId)param1Parcel.readParcelable(null);
          Bundle bundle = param1Parcel.readBundle();
          ContentCaptureContext.Builder builder = new ContentCaptureContext.Builder(locusId);
          if (bundle != null)
            builder.setExtras(bundle); 
          contentCaptureContext = new ContentCaptureContext(builder);
        } else {
          contentCaptureContext = null;
        } 
        ComponentName componentName = (ComponentName)param1Parcel.readParcelable(null);
        if (componentName == null)
          return contentCaptureContext; 
        i = param1Parcel.readInt();
        j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        return new ContentCaptureContext(contentCaptureContext, componentName, i, j, k);
      }
      
      public ContentCaptureContext[] newArray(int param1Int) {
        return new ContentCaptureContext[param1Int];
      }
    };
}
