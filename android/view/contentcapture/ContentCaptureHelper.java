package android.view.contentcapture;

import android.os.Build;
import android.provider.DeviceConfig;
import android.util.ArraySet;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class ContentCaptureHelper {
  private static final String TAG = ContentCaptureHelper.class.getSimpleName();
  
  public static boolean sDebug;
  
  public static boolean sVerbose = false;
  
  static {
    sDebug = true;
  }
  
  public static String getSanitizedString(CharSequence paramCharSequence) {
    if (paramCharSequence == null) {
      paramCharSequence = null;
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramCharSequence.length());
      stringBuilder.append("_chars");
      paramCharSequence = stringBuilder.toString();
    } 
    return (String)paramCharSequence;
  }
  
  public static int getDefaultLoggingLevel() {
    return Build.IS_DEBUGGABLE;
  }
  
  public static void setLoggingLevel() {
    int i = getDefaultLoggingLevel();
    i = DeviceConfig.getInt("content_capture", "logging_level", i);
    setLoggingLevel(i);
  }
  
  public static void setLoggingLevel(int paramInt) {
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Setting logging level to ");
    stringBuilder.append(getLoggingLevelAsString(paramInt));
    Log.i(str, stringBuilder.toString());
    sDebug = false;
    sVerbose = false;
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          str = TAG;
          stringBuilder = new StringBuilder();
          stringBuilder.append("setLoggingLevel(): invalud level: ");
          stringBuilder.append(paramInt);
          Log.w(str, stringBuilder.toString());
          return;
        } 
        sVerbose = true;
      } 
      sDebug = true;
      return;
    } 
  }
  
  public static String getLoggingLevelAsString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("UNKNOWN-");
          stringBuilder.append(paramInt);
          return stringBuilder.toString();
        } 
        return "VERBOSE";
      } 
      return "DEBUG";
    } 
    return "OFF";
  }
  
  public static <T> ArrayList<T> toList(Set<T> paramSet) {
    ArrayList<T> arrayList;
    if (paramSet == null) {
      paramSet = null;
    } else {
      arrayList = new ArrayList<>(paramSet);
    } 
    return arrayList;
  }
  
  public static <T> ArraySet<T> toSet(List<T> paramList) {
    ArraySet<T> arraySet;
    if (paramList == null) {
      paramList = null;
    } else {
      arraySet = new ArraySet<>(paramList);
    } 
    return arraySet;
  }
  
  private ContentCaptureHelper() {
    throw new UnsupportedOperationException("contains only static methods");
  }
}
