package android.view.accessibility;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAccessibilityEmbeddedConnection extends IInterface {
  IBinder associateEmbeddedHierarchy(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void disassociateEmbeddedHierarchy() throws RemoteException;
  
  void setScreenMatrix(float[] paramArrayOffloat) throws RemoteException;
  
  class Default implements IAccessibilityEmbeddedConnection {
    public IBinder associateEmbeddedHierarchy(IBinder param1IBinder, int param1Int) throws RemoteException {
      return null;
    }
    
    public void disassociateEmbeddedHierarchy() throws RemoteException {}
    
    public void setScreenMatrix(float[] param1ArrayOffloat) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAccessibilityEmbeddedConnection {
    private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityEmbeddedConnection";
    
    static final int TRANSACTION_associateEmbeddedHierarchy = 1;
    
    static final int TRANSACTION_disassociateEmbeddedHierarchy = 2;
    
    static final int TRANSACTION_setScreenMatrix = 3;
    
    public Stub() {
      attachInterface(this, "android.view.accessibility.IAccessibilityEmbeddedConnection");
    }
    
    public static IAccessibilityEmbeddedConnection asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.accessibility.IAccessibilityEmbeddedConnection");
      if (iInterface != null && iInterface instanceof IAccessibilityEmbeddedConnection)
        return (IAccessibilityEmbeddedConnection)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "setScreenMatrix";
        } 
        return "disassociateEmbeddedHierarchy";
      } 
      return "associateEmbeddedHierarchy";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      float[] arrayOfFloat;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.view.accessibility.IAccessibilityEmbeddedConnection");
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityEmbeddedConnection");
          arrayOfFloat = param1Parcel1.createFloatArray();
          setScreenMatrix(arrayOfFloat);
          return true;
        } 
        arrayOfFloat.enforceInterface("android.view.accessibility.IAccessibilityEmbeddedConnection");
        disassociateEmbeddedHierarchy();
        param1Parcel2.writeNoException();
        return true;
      } 
      arrayOfFloat.enforceInterface("android.view.accessibility.IAccessibilityEmbeddedConnection");
      IBinder iBinder2 = arrayOfFloat.readStrongBinder();
      param1Int1 = arrayOfFloat.readInt();
      IBinder iBinder1 = associateEmbeddedHierarchy(iBinder2, param1Int1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeStrongBinder(iBinder1);
      return true;
    }
    
    private static class Proxy implements IAccessibilityEmbeddedConnection {
      public static IAccessibilityEmbeddedConnection sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.accessibility.IAccessibilityEmbeddedConnection";
      }
      
      public IBinder associateEmbeddedHierarchy(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityEmbeddedConnection");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAccessibilityEmbeddedConnection.Stub.getDefaultImpl() != null) {
            param2IBinder = IAccessibilityEmbeddedConnection.Stub.getDefaultImpl().associateEmbeddedHierarchy(param2IBinder, param2Int);
            return param2IBinder;
          } 
          parcel2.readException();
          param2IBinder = parcel2.readStrongBinder();
          return param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disassociateEmbeddedHierarchy() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityEmbeddedConnection");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAccessibilityEmbeddedConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityEmbeddedConnection.Stub.getDefaultImpl().disassociateEmbeddedHierarchy();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setScreenMatrix(float[] param2ArrayOffloat) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityEmbeddedConnection");
          parcel.writeFloatArray(param2ArrayOffloat);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IAccessibilityEmbeddedConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityEmbeddedConnection.Stub.getDefaultImpl().setScreenMatrix(param2ArrayOffloat);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAccessibilityEmbeddedConnection param1IAccessibilityEmbeddedConnection) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAccessibilityEmbeddedConnection != null) {
          Proxy.sDefaultImpl = param1IAccessibilityEmbeddedConnection;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAccessibilityEmbeddedConnection getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
