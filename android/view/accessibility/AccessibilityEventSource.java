package android.view.accessibility;

public interface AccessibilityEventSource {
  void sendAccessibilityEvent(int paramInt);
  
  void sendAccessibilityEventUnchecked(AccessibilityEvent paramAccessibilityEvent);
}
