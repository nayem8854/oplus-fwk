package android.view.accessibility;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.AccessibilityShortcutInfo;
import android.annotation.SystemApi;
import android.app.RemoteAction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseArray;
import android.view.IWindow;
import com.android.internal.util.IntPair;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

public final class AccessibilityManager {
  static final Object sInstanceSync = new Object();
  
  private final Object mLock = new Object();
  
  int mRelevantEventTypes = -1;
  
  private final ArrayMap<AccessibilityStateChangeListener, Handler> mAccessibilityStateChangeListeners = new ArrayMap<>();
  
  private final ArrayMap<TouchExplorationStateChangeListener, Handler> mTouchExplorationStateChangeListeners = new ArrayMap<>();
  
  private final ArrayMap<HighTextContrastChangeListener, Handler> mHighTextContrastStateChangeListeners = new ArrayMap<>();
  
  private final ArrayMap<AccessibilityServicesStateChangeListener, Handler> mServicesStateChangeListeners = new ArrayMap<>();
  
  private final IAccessibilityManagerClient.Stub mClient = (IAccessibilityManagerClient.Stub)new Object(this);
  
  public static final int ACCESSIBILITY_BUTTON = 0;
  
  public static final int ACCESSIBILITY_SHORTCUT_KEY = 1;
  
  public static final String ACTION_CHOOSE_ACCESSIBILITY_BUTTON = "com.android.internal.intent.action.CHOOSE_ACCESSIBILITY_BUTTON";
  
  public static final int AUTOCLICK_DELAY_DEFAULT = 600;
  
  public static final int COLOR_STATE_DIRECT_ENABLED = 128;
  
  public static final int DALTONIZER_CORRECT_DEUTERANOMALY = 12;
  
  public static final int DALTONIZER_DISABLED = -1;
  
  public static final int DALTONIZER_SIMULATE_MONOCHROMACY = 0;
  
  private static final boolean DEBUG = false;
  
  public static final int FLAG_CONTENT_CONTROLS = 4;
  
  public static final int FLAG_CONTENT_ICONS = 1;
  
  public static final int FLAG_CONTENT_TEXT = 2;
  
  private static final String LOG_TAG = "AccessibilityManager";
  
  public static final int STATE_FLAG_ACCESSIBILITY_ENABLED = 1;
  
  public static final int STATE_FLAG_DISPATCH_DOUBLE_TAP = 8;
  
  public static final int STATE_FLAG_HIGH_TEXT_CONTRAST_ENABLED = 4;
  
  public static final int STATE_FLAG_REQUEST_MULTI_FINGER_GESTURES = 16;
  
  public static final int STATE_FLAG_TOUCH_EXPLORATION_ENABLED = 2;
  
  private static AccessibilityManager sInstance;
  
  AccessibilityPolicy mAccessibilityPolicy;
  
  final Handler.Callback mCallback;
  
  final Handler mHandler;
  
  int mInteractiveUiTimeout;
  
  boolean mIsDirectEnabled;
  
  boolean mIsEnabled;
  
  boolean mIsHighTextContrastEnabled;
  
  boolean mIsTouchExplorationEnabled;
  
  int mNonInteractiveUiTimeout;
  
  private SparseArray<List<AccessibilityRequestPreparer>> mRequestPreparerLists;
  
  private IAccessibilityManager mService;
  
  final int mUserId;
  
  public static AccessibilityManager getInstance(Context paramContext) {
    // Byte code:
    //   0: getstatic android/view/accessibility/AccessibilityManager.sInstanceSync : Ljava/lang/Object;
    //   3: astore_1
    //   4: aload_1
    //   5: monitorenter
    //   6: getstatic android/view/accessibility/AccessibilityManager.sInstance : Landroid/view/accessibility/AccessibilityManager;
    //   9: ifnonnull -> 68
    //   12: invokestatic getCallingUid : ()I
    //   15: sipush #1000
    //   18: if_icmpeq -> 50
    //   21: aload_0
    //   22: ldc 'android.permission.INTERACT_ACROSS_USERS'
    //   24: invokevirtual checkCallingOrSelfPermission : (Ljava/lang/String;)I
    //   27: ifeq -> 50
    //   30: aload_0
    //   31: ldc 'android.permission.INTERACT_ACROSS_USERS_FULL'
    //   33: invokevirtual checkCallingOrSelfPermission : (Ljava/lang/String;)I
    //   36: ifne -> 42
    //   39: goto -> 50
    //   42: aload_0
    //   43: invokevirtual getUserId : ()I
    //   46: istore_2
    //   47: goto -> 53
    //   50: bipush #-2
    //   52: istore_2
    //   53: new android/view/accessibility/AccessibilityManager
    //   56: astore_3
    //   57: aload_3
    //   58: aload_0
    //   59: aconst_null
    //   60: iload_2
    //   61: invokespecial <init> : (Landroid/content/Context;Landroid/view/accessibility/IAccessibilityManager;I)V
    //   64: aload_3
    //   65: putstatic android/view/accessibility/AccessibilityManager.sInstance : Landroid/view/accessibility/AccessibilityManager;
    //   68: aload_1
    //   69: monitorexit
    //   70: getstatic android/view/accessibility/AccessibilityManager.sInstance : Landroid/view/accessibility/AccessibilityManager;
    //   73: areturn
    //   74: astore_0
    //   75: aload_1
    //   76: monitorexit
    //   77: aload_0
    //   78: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #422	-> 0
    //   #423	-> 6
    //   #425	-> 12
    //   #426	-> 21
    //   #429	-> 30
    //   #434	-> 42
    //   #432	-> 50
    //   #436	-> 53
    //   #438	-> 68
    //   #439	-> 70
    //   #438	-> 74
    // Exception table:
    //   from	to	target	type
    //   6	12	74	finally
    //   12	21	74	finally
    //   21	30	74	finally
    //   30	39	74	finally
    //   42	47	74	finally
    //   53	68	74	finally
    //   68	70	74	finally
    //   75	77	74	finally
  }
  
  public AccessibilityManager(Context paramContext, IAccessibilityManager paramIAccessibilityManager, int paramInt) {
    this.mCallback = new MyCallback();
    this.mHandler = new Handler(paramContext.getMainLooper(), this.mCallback);
    this.mUserId = paramInt;
    synchronized (this.mLock) {
      tryConnectToServiceLocked(paramIAccessibilityManager);
      return;
    } 
  }
  
  public AccessibilityManager(Handler paramHandler, IAccessibilityManager paramIAccessibilityManager, int paramInt) {
    this.mCallback = new MyCallback();
    this.mHandler = paramHandler;
    this.mUserId = paramInt;
    synchronized (this.mLock) {
      tryConnectToServiceLocked(paramIAccessibilityManager);
      return;
    } 
  }
  
  public IAccessibilityManagerClient getClient() {
    return this.mClient;
  }
  
  public Handler.Callback getCallback() {
    return this.mCallback;
  }
  
  public boolean isEnabled() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_1
    //   5: aload_1
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mIsEnabled : Z
    //   11: ifne -> 49
    //   14: aload_0
    //   15: getfield mAccessibilityPolicy : Landroid/view/accessibility/AccessibilityManager$AccessibilityPolicy;
    //   18: ifnull -> 44
    //   21: aload_0
    //   22: getfield mAccessibilityPolicy : Landroid/view/accessibility/AccessibilityManager$AccessibilityPolicy;
    //   25: astore_2
    //   26: aload_0
    //   27: getfield mIsEnabled : Z
    //   30: istore_3
    //   31: aload_2
    //   32: iload_3
    //   33: invokeinterface isEnabled : (Z)Z
    //   38: ifeq -> 44
    //   41: goto -> 49
    //   44: iconst_0
    //   45: istore_3
    //   46: goto -> 51
    //   49: iconst_1
    //   50: istore_3
    //   51: aload_1
    //   52: monitorexit
    //   53: iload_3
    //   54: ireturn
    //   55: astore_2
    //   56: aload_1
    //   57: monitorexit
    //   58: aload_2
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #501	-> 0
    //   #502	-> 7
    //   #503	-> 31
    //   #502	-> 53
    //   #504	-> 55
    // Exception table:
    //   from	to	target	type
    //   7	31	55	finally
    //   31	41	55	finally
    //   51	53	55	finally
    //   56	58	55	finally
  }
  
  public boolean isTouchExplorationEnabled() {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return false; 
      return this.mIsTouchExplorationEnabled;
    } 
  }
  
  public boolean isHighTextContrastEnabled() {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return false; 
      return this.mIsHighTextContrastEnabled;
    } 
  }
  
  public void sendAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: invokespecial getServiceLocked : ()Landroid/view/accessibility/IAccessibilityManager;
    //   11: astore_3
    //   12: aload_3
    //   13: ifnonnull -> 19
    //   16: aload_2
    //   17: monitorexit
    //   18: return
    //   19: aload_1
    //   20: invokestatic uptimeMillis : ()J
    //   23: invokevirtual setEventTime : (J)V
    //   26: aload_0
    //   27: getfield mAccessibilityPolicy : Landroid/view/accessibility/AccessibilityManager$AccessibilityPolicy;
    //   30: ifnull -> 65
    //   33: aload_0
    //   34: getfield mAccessibilityPolicy : Landroid/view/accessibility/AccessibilityManager$AccessibilityPolicy;
    //   37: aload_1
    //   38: aload_0
    //   39: getfield mIsEnabled : Z
    //   42: aload_0
    //   43: getfield mRelevantEventTypes : I
    //   46: invokeinterface onAccessibilityEvent : (Landroid/view/accessibility/AccessibilityEvent;ZI)Landroid/view/accessibility/AccessibilityEvent;
    //   51: astore #4
    //   53: aload #4
    //   55: astore #5
    //   57: aload #4
    //   59: ifnonnull -> 68
    //   62: aload_2
    //   63: monitorexit
    //   64: return
    //   65: aload_1
    //   66: astore #5
    //   68: aload_0
    //   69: invokevirtual isEnabled : ()Z
    //   72: ifne -> 111
    //   75: invokestatic myLooper : ()Landroid/os/Looper;
    //   78: astore_1
    //   79: aload_1
    //   80: invokestatic getMainLooper : ()Landroid/os/Looper;
    //   83: if_acmpeq -> 98
    //   86: ldc 'AccessibilityManager'
    //   88: ldc_w 'AccessibilityEvent sent with accessibility disabled'
    //   91: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   94: pop
    //   95: aload_2
    //   96: monitorexit
    //   97: return
    //   98: new java/lang/IllegalStateException
    //   101: astore_1
    //   102: aload_1
    //   103: ldc_w 'Accessibility off. Did you forget to check that?'
    //   106: invokespecial <init> : (Ljava/lang/String;)V
    //   109: aload_1
    //   110: athrow
    //   111: aload #5
    //   113: invokevirtual getEventType : ()I
    //   116: aload_0
    //   117: getfield mRelevantEventTypes : I
    //   120: iand
    //   121: ifne -> 127
    //   124: aload_2
    //   125: monitorexit
    //   126: return
    //   127: aload_0
    //   128: getfield mUserId : I
    //   131: istore #6
    //   133: aload_2
    //   134: monitorexit
    //   135: invokestatic clearCallingIdentity : ()J
    //   138: lstore #7
    //   140: aload_3
    //   141: aload #5
    //   143: iload #6
    //   145: invokeinterface sendAccessibilityEvent : (Landroid/view/accessibility/AccessibilityEvent;I)V
    //   150: lload #7
    //   152: invokestatic restoreCallingIdentity : (J)V
    //   155: aload_1
    //   156: aload #5
    //   158: if_acmpeq -> 165
    //   161: aload_1
    //   162: invokevirtual recycle : ()V
    //   165: aload #5
    //   167: invokevirtual recycle : ()V
    //   170: goto -> 242
    //   173: astore #4
    //   175: lload #7
    //   177: invokestatic restoreCallingIdentity : (J)V
    //   180: aload #4
    //   182: athrow
    //   183: astore #4
    //   185: goto -> 243
    //   188: astore #4
    //   190: new java/lang/StringBuilder
    //   193: astore_2
    //   194: aload_2
    //   195: invokespecial <init> : ()V
    //   198: aload_2
    //   199: ldc_w 'Error during sending '
    //   202: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   205: pop
    //   206: aload_2
    //   207: aload #5
    //   209: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   212: pop
    //   213: aload_2
    //   214: ldc_w ' '
    //   217: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   220: pop
    //   221: ldc 'AccessibilityManager'
    //   223: aload_2
    //   224: invokevirtual toString : ()Ljava/lang/String;
    //   227: aload #4
    //   229: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   232: pop
    //   233: aload_1
    //   234: aload #5
    //   236: if_acmpeq -> 165
    //   239: goto -> 161
    //   242: return
    //   243: aload_1
    //   244: aload #5
    //   246: if_acmpeq -> 253
    //   249: aload_1
    //   250: invokevirtual recycle : ()V
    //   253: aload #5
    //   255: invokevirtual recycle : ()V
    //   258: aload #4
    //   260: athrow
    //   261: astore_1
    //   262: aload_2
    //   263: monitorexit
    //   264: aload_1
    //   265: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #561	-> 0
    //   #562	-> 7
    //   #563	-> 12
    //   #564	-> 16
    //   #566	-> 19
    //   #567	-> 26
    //   #568	-> 33
    //   #570	-> 53
    //   #571	-> 62
    //   #574	-> 65
    //   #576	-> 68
    //   #577	-> 75
    //   #578	-> 79
    //   #586	-> 86
    //   #587	-> 95
    //   #579	-> 98
    //   #590	-> 111
    //   #596	-> 124
    //   #598	-> 127
    //   #599	-> 133
    //   #604	-> 135
    //   #606	-> 140
    //   #608	-> 150
    //   #609	-> 155
    //   #616	-> 155
    //   #617	-> 161
    //   #619	-> 165
    //   #620	-> 170
    //   #608	-> 173
    //   #609	-> 180
    //   #616	-> 183
    //   #613	-> 188
    //   #614	-> 190
    //   #616	-> 233
    //   #617	-> 239
    //   #621	-> 242
    //   #616	-> 243
    //   #617	-> 249
    //   #619	-> 253
    //   #620	-> 258
    //   #599	-> 261
    // Exception table:
    //   from	to	target	type
    //   7	12	261	finally
    //   16	18	261	finally
    //   19	26	261	finally
    //   26	33	261	finally
    //   33	53	261	finally
    //   62	64	261	finally
    //   68	75	261	finally
    //   75	79	261	finally
    //   79	86	261	finally
    //   86	95	261	finally
    //   95	97	261	finally
    //   98	111	261	finally
    //   111	124	261	finally
    //   124	126	261	finally
    //   127	133	261	finally
    //   133	135	261	finally
    //   135	140	188	android/os/RemoteException
    //   135	140	183	finally
    //   140	150	173	finally
    //   150	155	188	android/os/RemoteException
    //   150	155	183	finally
    //   175	180	188	android/os/RemoteException
    //   175	180	183	finally
    //   180	183	188	android/os/RemoteException
    //   180	183	183	finally
    //   190	233	183	finally
    //   262	264	261	finally
  }
  
  public void interrupt() {
    synchronized (this.mLock) {
      IllegalStateException illegalStateException;
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      if (!isEnabled()) {
        Looper looper = Looper.myLooper();
        if (looper != Looper.getMainLooper()) {
          Log.e("AccessibilityManager", "Interrupt called with accessibility disabled");
          return;
        } 
        illegalStateException = new IllegalStateException();
        this("Accessibility off. Did you forget to check that?");
        throw illegalStateException;
      } 
      int i = this.mUserId;
      try {
        illegalStateException.interrupt(i);
      } catch (RemoteException null) {
        Log.e("AccessibilityManager", "Error while requesting interrupt from all services. ", (Throwable)null);
      } 
      return;
    } 
  }
  
  @Deprecated
  public List<ServiceInfo> getAccessibilityServiceList() {
    List<AccessibilityServiceInfo> list = getInstalledAccessibilityServiceList();
    ArrayList<ServiceInfo> arrayList = new ArrayList();
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      AccessibilityServiceInfo accessibilityServiceInfo = list.get(b);
      arrayList.add((accessibilityServiceInfo.getResolveInfo()).serviceInfo);
    } 
    return Collections.unmodifiableList(arrayList);
  }
  
  public List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList() {
    synchronized (this.mLock) {
      List<?> list;
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null) {
        list = Collections.emptyList();
        return (List)list;
      } 
      int i = this.mUserId;
      null = null;
      try {
        null = (Object<?>)(list = list.getInstalledAccessibilityServiceList(i));
      } catch (RemoteException remoteException) {
        Log.e("AccessibilityManager", "Error while obtaining the installed AccessibilityServices. ", (Throwable)remoteException);
      } 
      AccessibilityPolicy accessibilityPolicy = this.mAccessibilityPolicy;
      Object<?> object = null;
      if (accessibilityPolicy != null)
        object = (Object<?>)accessibilityPolicy.getInstalledAccessibilityServiceList((List)null); 
      if (object != null)
        return (List)Collections.unmodifiableList((List<?>)object); 
      return Collections.emptyList();
    } 
  }
  
  public List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int paramInt) {
    synchronized (this.mLock) {
      List<?> list;
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null) {
        list = Collections.emptyList();
        return (List)list;
      } 
      int i = this.mUserId;
      null = null;
      try {
        null = (Object<?>)(list = list.getEnabledAccessibilityServiceList(paramInt, i));
      } catch (RemoteException remoteException) {
        Log.e("AccessibilityManager", "Error while obtaining the enabled AccessibilityServices. ", (Throwable)remoteException);
      } 
      AccessibilityPolicy accessibilityPolicy = this.mAccessibilityPolicy;
      Object<?> object = null;
      if (accessibilityPolicy != null)
        object = (Object<?>)accessibilityPolicy.getEnabledAccessibilityServiceList(paramInt, (List)null); 
      if (object != null)
        return (List)Collections.unmodifiableList((List<?>)object); 
      return Collections.emptyList();
    } 
  }
  
  public boolean addAccessibilityStateChangeListener(AccessibilityStateChangeListener paramAccessibilityStateChangeListener) {
    addAccessibilityStateChangeListener(paramAccessibilityStateChangeListener, null);
    return true;
  }
  
  public void addAccessibilityStateChangeListener(AccessibilityStateChangeListener paramAccessibilityStateChangeListener, Handler paramHandler) {
    synchronized (this.mLock) {
      ArrayMap<AccessibilityStateChangeListener, Handler> arrayMap = this.mAccessibilityStateChangeListeners;
      if (paramHandler == null)
        paramHandler = this.mHandler; 
      arrayMap.put(paramAccessibilityStateChangeListener, paramHandler);
      return;
    } 
  }
  
  public boolean removeAccessibilityStateChangeListener(AccessibilityStateChangeListener paramAccessibilityStateChangeListener) {
    synchronized (this.mLock) {
      boolean bool;
      int i = this.mAccessibilityStateChangeListeners.indexOfKey(paramAccessibilityStateChangeListener);
      this.mAccessibilityStateChangeListeners.remove(paramAccessibilityStateChangeListener);
      if (i >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  public boolean addTouchExplorationStateChangeListener(TouchExplorationStateChangeListener paramTouchExplorationStateChangeListener) {
    addTouchExplorationStateChangeListener(paramTouchExplorationStateChangeListener, null);
    return true;
  }
  
  public void addTouchExplorationStateChangeListener(TouchExplorationStateChangeListener paramTouchExplorationStateChangeListener, Handler paramHandler) {
    synchronized (this.mLock) {
      ArrayMap<TouchExplorationStateChangeListener, Handler> arrayMap = this.mTouchExplorationStateChangeListeners;
      if (paramHandler == null)
        paramHandler = this.mHandler; 
      arrayMap.put(paramTouchExplorationStateChangeListener, paramHandler);
      return;
    } 
  }
  
  public boolean removeTouchExplorationStateChangeListener(TouchExplorationStateChangeListener paramTouchExplorationStateChangeListener) {
    synchronized (this.mLock) {
      boolean bool;
      int i = this.mTouchExplorationStateChangeListeners.indexOfKey(paramTouchExplorationStateChangeListener);
      this.mTouchExplorationStateChangeListeners.remove(paramTouchExplorationStateChangeListener);
      if (i >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  public void addAccessibilityServicesStateChangeListener(AccessibilityServicesStateChangeListener paramAccessibilityServicesStateChangeListener, Handler paramHandler) {
    synchronized (this.mLock) {
      ArrayMap<AccessibilityServicesStateChangeListener, Handler> arrayMap = this.mServicesStateChangeListeners;
      if (paramHandler == null)
        paramHandler = this.mHandler; 
      arrayMap.put(paramAccessibilityServicesStateChangeListener, paramHandler);
      return;
    } 
  }
  
  public void removeAccessibilityServicesStateChangeListener(AccessibilityServicesStateChangeListener paramAccessibilityServicesStateChangeListener) {
    synchronized (this.mLock) {
      this.mServicesStateChangeListeners.remove(paramAccessibilityServicesStateChangeListener);
      return;
    } 
  }
  
  public void addAccessibilityRequestPreparer(AccessibilityRequestPreparer paramAccessibilityRequestPreparer) {
    if (this.mRequestPreparerLists == null)
      this.mRequestPreparerLists = new SparseArray<>(1); 
    int i = paramAccessibilityRequestPreparer.getAccessibilityViewId();
    List<AccessibilityRequestPreparer> list1 = this.mRequestPreparerLists.get(i);
    List<AccessibilityRequestPreparer> list2 = list1;
    if (list1 == null) {
      list2 = new ArrayList(1);
      this.mRequestPreparerLists.put(i, list2);
    } 
    list2.add(paramAccessibilityRequestPreparer);
  }
  
  public void removeAccessibilityRequestPreparer(AccessibilityRequestPreparer paramAccessibilityRequestPreparer) {
    if (this.mRequestPreparerLists == null)
      return; 
    int i = paramAccessibilityRequestPreparer.getAccessibilityViewId();
    List list = this.mRequestPreparerLists.get(i);
    if (list != null) {
      list.remove(paramAccessibilityRequestPreparer);
      if (list.isEmpty())
        this.mRequestPreparerLists.remove(i); 
    } 
  }
  
  public int getRecommendedTimeoutMillis(int paramInt1, int paramInt2) {
    boolean bool2, bool1 = false;
    if ((paramInt2 & 0x4) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((paramInt2 & 0x1) != 0 || (paramInt2 & 0x2) != 0)
      bool1 = true; 
    paramInt2 = paramInt1;
    paramInt1 = paramInt2;
    if (bool2)
      paramInt1 = Math.max(paramInt2, this.mInteractiveUiTimeout); 
    paramInt2 = paramInt1;
    if (bool1)
      paramInt2 = Math.max(paramInt1, this.mNonInteractiveUiTimeout); 
    return paramInt2;
  }
  
  public List<AccessibilityRequestPreparer> getRequestPreparersForAccessibilityId(int paramInt) {
    SparseArray<List<AccessibilityRequestPreparer>> sparseArray = this.mRequestPreparerLists;
    if (sparseArray == null)
      return null; 
    return sparseArray.get(paramInt);
  }
  
  public void addHighTextContrastStateChangeListener(HighTextContrastChangeListener paramHighTextContrastChangeListener, Handler paramHandler) {
    synchronized (this.mLock) {
      ArrayMap<HighTextContrastChangeListener, Handler> arrayMap = this.mHighTextContrastStateChangeListeners;
      if (paramHandler == null)
        paramHandler = this.mHandler; 
      arrayMap.put(paramHighTextContrastChangeListener, paramHandler);
      return;
    } 
  }
  
  public void removeHighTextContrastStateChangeListener(HighTextContrastChangeListener paramHighTextContrastChangeListener) {
    synchronized (this.mLock) {
      this.mHighTextContrastStateChangeListeners.remove(paramHighTextContrastChangeListener);
      return;
    } 
  }
  
  public void setAccessibilityPolicy(AccessibilityPolicy paramAccessibilityPolicy) {
    synchronized (this.mLock) {
      this.mAccessibilityPolicy = paramAccessibilityPolicy;
      return;
    } 
  }
  
  public boolean isAccessibilityVolumeStreamActive() {
    List<AccessibilityServiceInfo> list = getEnabledAccessibilityServiceList(-1);
    for (byte b = 0; b < list.size(); b++) {
      if ((((AccessibilityServiceInfo)list.get(b)).flags & 0x80) != 0)
        return true; 
    } 
    return false;
  }
  
  public boolean sendFingerprintGesture(int paramInt) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return false; 
      try {
        return iAccessibilityManager.sendFingerprintGesture(paramInt);
      } catch (RemoteException null) {
        return false;
      } 
    } 
  }
  
  @SystemApi
  public int getAccessibilityWindowId(IBinder paramIBinder) {
    if (paramIBinder == null)
      return -1; 
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return -1; 
      try {
        return iAccessibilityManager.getAccessibilityWindowId(paramIBinder);
      } catch (RemoteException remoteException) {
        return -1;
      } 
    } 
  }
  
  public void associateEmbeddedHierarchy(IBinder paramIBinder1, IBinder paramIBinder2) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.associateEmbeddedHierarchy(paramIBinder1, paramIBinder2);
        return;
      } catch (RemoteException remoteException) {
        return;
      } 
    } 
  }
  
  public void disassociateEmbeddedHierarchy(IBinder paramIBinder) {
    if (paramIBinder == null)
      return; 
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.disassociateEmbeddedHierarchy(paramIBinder);
        return;
      } catch (RemoteException remoteException) {
        return;
      } 
    } 
  }
  
  private void setStateLocked(int paramInt) {
    boolean bool2;
    boolean bool3, bool4;
    boolean bool1 = false;
    if ((paramInt & 0x1) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((paramInt & 0x2) != 0) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if ((paramInt & 0x4) != 0) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    boolean bool5 = isEnabled();
    boolean bool6 = this.mIsTouchExplorationEnabled;
    boolean bool7 = this.mIsHighTextContrastEnabled;
    if ((paramInt & 0x80) != 0)
      bool1 = true; 
    this.mIsDirectEnabled = bool1;
    this.mIsEnabled = bool2;
    this.mIsTouchExplorationEnabled = bool3;
    this.mIsHighTextContrastEnabled = bool4;
    if (bool5 != isEnabled())
      notifyAccessibilityStateChanged(); 
    if (bool6 != bool3)
      notifyTouchExplorationStateChanged(); 
    if (bool7 != bool4)
      notifyHighTextContrastStateChanged(); 
  }
  
  public AccessibilityServiceInfo getInstalledServiceInfoWithComponentName(ComponentName paramComponentName) {
    List<AccessibilityServiceInfo> list = getInstalledAccessibilityServiceList();
    if (list == null || paramComponentName == null)
      return null; 
    for (byte b = 0; b < list.size(); b++) {
      if (paramComponentName.equals(((AccessibilityServiceInfo)list.get(b)).getComponentName()))
        return list.get(b); 
    } 
    return null;
  }
  
  public int addAccessibilityInteractionConnection(IWindow paramIWindow, IBinder paramIBinder, String paramString, IAccessibilityInteractionConnection paramIAccessibilityInteractionConnection) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return -1; 
      int i = this.mUserId;
      try {
        return iAccessibilityManager.addAccessibilityInteractionConnection(paramIWindow, paramIBinder, paramIAccessibilityInteractionConnection, paramString, i);
      } catch (RemoteException remoteException) {
        Log.e("AccessibilityManager", "Error while adding an accessibility interaction connection. ", (Throwable)remoteException);
        return -1;
      } 
    } 
  }
  
  public void removeAccessibilityInteractionConnection(IWindow paramIWindow) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.removeAccessibilityInteractionConnection(paramIWindow);
      } catch (RemoteException remoteException) {
        Log.e("AccessibilityManager", "Error while removing an accessibility interaction connection. ", (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  @SystemApi
  public void performAccessibilityShortcut() {
    performAccessibilityShortcut(null);
  }
  
  public void performAccessibilityShortcut(String paramString) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.performAccessibilityShortcut(paramString);
      } catch (RemoteException remoteException) {
        Log.e("AccessibilityManager", "Error performing accessibility shortcut. ", (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  @SystemApi
  public void registerSystemAction(RemoteAction paramRemoteAction, int paramInt) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.registerSystemAction(paramRemoteAction, paramInt);
      } catch (RemoteException remoteException) {
        null = new StringBuilder();
        null.append("Error registering system action ");
        null.append(paramRemoteAction.getTitle());
        null.append(" ");
        Log.e("AccessibilityManager", null.toString(), (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  @SystemApi
  public void unregisterSystemAction(int paramInt) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.unregisterSystemAction(paramInt);
      } catch (RemoteException null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error unregistering system action with actionId ");
        stringBuilder.append(paramInt);
        stringBuilder.append(" ");
        Log.e("AccessibilityManager", stringBuilder.toString(), (Throwable)null);
      } 
      return;
    } 
  }
  
  public void notifyAccessibilityButtonClicked(int paramInt) {
    notifyAccessibilityButtonClicked(paramInt, null);
  }
  
  public void notifyAccessibilityButtonClicked(int paramInt, String paramString) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.notifyAccessibilityButtonClicked(paramInt, paramString);
      } catch (RemoteException remoteException) {
        Log.e("AccessibilityManager", "Error while dispatching accessibility button click", (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  public void notifyAccessibilityButtonVisibilityChanged(boolean paramBoolean) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.notifyAccessibilityButtonVisibilityChanged(paramBoolean);
      } catch (RemoteException null) {
        Log.e("AccessibilityManager", "Error while dispatching accessibility button visibility change", (Throwable)null);
      } 
      return;
    } 
  }
  
  public void setPictureInPictureActionReplacingConnection(IAccessibilityInteractionConnection paramIAccessibilityInteractionConnection) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.setPictureInPictureActionReplacingConnection(paramIAccessibilityInteractionConnection);
      } catch (RemoteException remoteException) {
        Log.e("AccessibilityManager", "Error setting picture in picture action replacement", (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  public List<String> getAccessibilityShortcutTargets(int paramInt) {
    Object<String> object;
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager != null)
        try {
          return iAccessibilityManager.getAccessibilityShortcutTargets(paramInt);
        } catch (RemoteException remoteException) {
          remoteException.rethrowFromSystemServer();
        }  
      return Collections.emptyList();
    } 
  }
  
  public List<AccessibilityShortcutInfo> getInstalledAccessibilityShortcutListAsUser(Context paramContext, int paramInt) {
    ArrayList<AccessibilityShortcutInfo> arrayList = new ArrayList();
    Intent intent = new Intent("android.intent.action.MAIN");
    intent.addCategory("android.intent.category.ACCESSIBILITY_SHORTCUT_TARGET");
    PackageManager packageManager = paramContext.getPackageManager();
    List<ResolveInfo> list = packageManager.queryIntentActivitiesAsUser(intent, 819329, paramInt);
    for (paramInt = 0; paramInt < list.size(); paramInt++) {
      AccessibilityShortcutInfo accessibilityShortcutInfo = getShortcutInfo(paramContext, list.get(paramInt));
      if (accessibilityShortcutInfo != null)
        arrayList.add(accessibilityShortcutInfo); 
    } 
    return arrayList;
  }
  
  private AccessibilityShortcutInfo getShortcutInfo(Context paramContext, ResolveInfo paramResolveInfo) {
    ActivityInfo activityInfo = paramResolveInfo.activityInfo;
    if (activityInfo != null && activityInfo.metaData != null) {
      Bundle bundle = activityInfo.metaData;
      if (bundle.getInt("android.accessibilityshortcut.target") != 0)
        try {
          return new AccessibilityShortcutInfo(paramContext, activityInfo);
        } catch (XmlPullParserException|java.io.IOException xmlPullParserException) {
          Log.e("AccessibilityManager", "Error while initializing AccessibilityShortcutInfo", (Throwable)xmlPullParserException);
          return null;
        }  
    } 
    return null;
  }
  
  public void setWindowMagnificationConnection(IWindowMagnificationConnection paramIWindowMagnificationConnection) {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return; 
      try {
        iAccessibilityManager.setWindowMagnificationConnection(paramIWindowMagnificationConnection);
      } catch (RemoteException remoteException) {
        Log.e("AccessibilityManager", "Error setting window magnfication connection", (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  private IAccessibilityManager getServiceLocked() {
    if (this.mService == null)
      tryConnectToServiceLocked(null); 
    return this.mService;
  }
  
  private void tryConnectToServiceLocked(IAccessibilityManager paramIAccessibilityManager) {
    IAccessibilityManager iAccessibilityManager = paramIAccessibilityManager;
    if (paramIAccessibilityManager == null) {
      IBinder iBinder = ServiceManager.getService("accessibility");
      if (iBinder == null)
        return; 
      iAccessibilityManager = IAccessibilityManager.Stub.asInterface(iBinder);
    } 
    try {
      long l = iAccessibilityManager.addClient(this.mClient, this.mUserId);
      setStateLocked(IntPair.first(l));
      this.mRelevantEventTypes = IntPair.second(l);
      updateUiTimeout(iAccessibilityManager.getRecommendedTimeoutMillis());
      this.mService = iAccessibilityManager;
    } catch (RemoteException remoteException) {
      Log.e("AccessibilityManager", "AccessibilityManagerService is dead", (Throwable)remoteException);
    } 
  }
  
  private void notifyAccessibilityStateChanged() {
    synchronized (this.mLock) {
      if (this.mAccessibilityStateChangeListeners.isEmpty())
        return; 
      boolean bool = isEnabled();
      ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
      this((ArrayMap)this.mAccessibilityStateChangeListeners);
      int i = arrayMap.size();
      for (byte b = 0; b < i; b++) {
        null = arrayMap.keyAt(b);
        ((Handler)arrayMap.valueAt(b)).post(new _$$Lambda$AccessibilityManager$yzw5NYY7_MfAQ9gLy3mVllchaXo((AccessibilityStateChangeListener)null, bool));
      } 
      return;
    } 
  }
  
  private void notifyTouchExplorationStateChanged() {
    synchronized (this.mLock) {
      if (this.mTouchExplorationStateChangeListeners.isEmpty())
        return; 
      boolean bool = this.mIsTouchExplorationEnabled;
      ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
      this((ArrayMap)this.mTouchExplorationStateChangeListeners);
      int i = arrayMap.size();
      for (byte b = 0; b < i; b++) {
        null = arrayMap.keyAt(b);
        ((Handler)arrayMap.valueAt(b)).post(new _$$Lambda$AccessibilityManager$a0OtrjOl35tiW2vwyvAmY6_LiLI((TouchExplorationStateChangeListener)null, bool));
      } 
      return;
    } 
  }
  
  private void notifyHighTextContrastStateChanged() {
    synchronized (this.mLock) {
      if (this.mHighTextContrastStateChangeListeners.isEmpty())
        return; 
      boolean bool = this.mIsHighTextContrastEnabled;
      ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
      this((ArrayMap)this.mHighTextContrastStateChangeListeners);
      int i = arrayMap.size();
      for (byte b = 0; b < i; b++) {
        null = arrayMap.keyAt(b);
        ((Handler)arrayMap.valueAt(b)).post(new _$$Lambda$AccessibilityManager$4M6GrmFiqsRwVzn352N10DcU6RM((HighTextContrastChangeListener)null, bool));
      } 
      return;
    } 
  }
  
  private void updateUiTimeout(long paramLong) {
    this.mInteractiveUiTimeout = IntPair.first(paramLong);
    this.mNonInteractiveUiTimeout = IntPair.second(paramLong);
  }
  
  public static boolean isAccessibilityButtonSupported() {
    Resources resources = Resources.getSystem();
    return resources.getBoolean(17891525);
  }
  
  public static interface AccessibilityPolicy {
    List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int param1Int, List<AccessibilityServiceInfo> param1List);
    
    List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList(List<AccessibilityServiceInfo> param1List);
    
    int getRelevantEventTypes(int param1Int);
    
    boolean isEnabled(boolean param1Boolean);
    
    AccessibilityEvent onAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent, boolean param1Boolean, int param1Int);
  }
  
  public static interface AccessibilityServicesStateChangeListener {
    void onAccessibilityServicesStateChanged(AccessibilityManager param1AccessibilityManager);
  }
  
  public static interface AccessibilityStateChangeListener {
    void onAccessibilityStateChanged(boolean param1Boolean);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ContentFlag {}
  
  public static interface HighTextContrastChangeListener {
    void onHighTextContrastStateChanged(boolean param1Boolean);
  }
  
  class MyCallback implements Handler.Callback {
    public static final int MSG_SET_STATE = 1;
    
    final AccessibilityManager this$0;
    
    private MyCallback() {}
    
    public boolean handleMessage(Message param1Message) {
      if (param1Message.what == 1) {
        int i = param1Message.arg1;
        synchronized (AccessibilityManager.this.mLock) {
          AccessibilityManager.this.setStateLocked(i);
          return true;
        } 
      } 
      return true;
    }
  }
  
  public boolean isOppoTouchExplorationEnabled() {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return false; 
      return this.mIsTouchExplorationEnabled;
    } 
  }
  
  public boolean isColorDirectEnabled() {
    synchronized (this.mLock) {
      IAccessibilityManager iAccessibilityManager = getServiceLocked();
      if (iAccessibilityManager == null)
        return false; 
      return this.mIsDirectEnabled;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ShortcutType {}
  
  public static interface TouchExplorationStateChangeListener {
    void onTouchExplorationStateChanged(boolean param1Boolean);
  }
}
