package android.view.accessibility;

import android.os.Build;
import android.util.ArraySet;
import android.util.Log;
import android.util.LongArray;
import android.util.LongSparseArray;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;

public class AccessibilityCache {
  static {
    boolean bool = Log.isLoggable("AccessibilityCache", 3);
    boolean bool1 = true;
    if (bool && Build.IS_DEBUGGABLE) {
      bool = true;
    } else {
      bool = false;
    } 
    DEBUG = bool;
    if (Log.isLoggable("AccessibilityCache", 2) && Build.IS_DEBUGGABLE) {
      bool = bool1;
    } else {
      bool = false;
    } 
    VERBOSE = bool;
    CHECK_INTEGRITY = Build.IS_ENG;
  }
  
  private final Object mLock = new Object();
  
  private long mAccessibilityFocus = 2147483647L;
  
  private long mInputFocus = 2147483647L;
  
  private int mAccessibilityFocusedWindow = -1;
  
  private final SparseArray<SparseArray<AccessibilityWindowInfo>> mWindowCacheByDisplay = new SparseArray<>();
  
  private final SparseArray<LongSparseArray<AccessibilityNodeInfo>> mNodeCache = new SparseArray<>();
  
  private final SparseArray<AccessibilityWindowInfo> mTempWindowArray = new SparseArray<>();
  
  public static final int CACHE_CRITICAL_EVENTS_MASK = 4307005;
  
  private static final boolean CHECK_INTEGRITY;
  
  private static final boolean DEBUG;
  
  private static final String LOG_TAG = "AccessibilityCache";
  
  private static final boolean VERBOSE;
  
  private final AccessibilityNodeRefresher mAccessibilityNodeRefresher;
  
  private boolean mIsAllWindowsCached;
  
  public AccessibilityCache(AccessibilityNodeRefresher paramAccessibilityNodeRefresher) {
    this.mAccessibilityNodeRefresher = paramAccessibilityNodeRefresher;
  }
  
  public void setWindowsOnAllDisplays(SparseArray<List<AccessibilityWindowInfo>> paramSparseArray) {
    synchronized (this.mLock) {
      if (DEBUG)
        Log.i("AccessibilityCache", "Set windows"); 
      clearWindowCacheLocked();
      if (paramSparseArray == null)
        return; 
      int i = paramSparseArray.size();
      for (byte b = 0; b < i; b++) {
        List<AccessibilityWindowInfo> list = paramSparseArray.valueAt(b);
        if (list != null) {
          int j = paramSparseArray.keyAt(b);
          int k = list.size();
          for (byte b1 = 0; b1 < k; b1++)
            addWindowByDisplayLocked(j, list.get(b1)); 
        } 
      } 
      this.mIsAllWindowsCached = true;
      return;
    } 
  }
  
  public void addWindow(AccessibilityWindowInfo paramAccessibilityWindowInfo) {
    synchronized (this.mLock) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Caching window: ");
        stringBuilder.append(paramAccessibilityWindowInfo.getId());
        stringBuilder.append(" at display Id [ ");
        stringBuilder.append(paramAccessibilityWindowInfo.getDisplayId());
        stringBuilder.append(" ]");
        String str = stringBuilder.toString();
        Log.i("AccessibilityCache", str);
      } 
      addWindowByDisplayLocked(paramAccessibilityWindowInfo.getDisplayId(), paramAccessibilityWindowInfo);
      return;
    } 
  }
  
  private void addWindowByDisplayLocked(int paramInt, AccessibilityWindowInfo paramAccessibilityWindowInfo) {
    SparseArray<AccessibilityWindowInfo> sparseArray1 = this.mWindowCacheByDisplay.get(paramInt);
    SparseArray<AccessibilityWindowInfo> sparseArray2 = sparseArray1;
    if (sparseArray1 == null) {
      sparseArray2 = new SparseArray();
      this.mWindowCacheByDisplay.put(paramInt, sparseArray2);
    } 
    paramInt = paramAccessibilityWindowInfo.getId();
    sparseArray2.put(paramInt, new AccessibilityWindowInfo(paramAccessibilityWindowInfo));
  }
  
  public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    synchronized (this.mLock) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("onAccessibilityEvent(");
        stringBuilder.append(paramAccessibilityEvent);
        stringBuilder.append(")");
        Log.i("AccessibilityCache", stringBuilder.toString());
      } 
      int i = paramAccessibilityEvent.getEventType();
      switch (i) {
        case 4194304:
          if (paramAccessibilityEvent.getWindowChanges() == 128) {
            refreshCachedWindowLocked(paramAccessibilityEvent.getWindowId());
            break;
          } 
        case 65536:
          if (this.mAccessibilityFocus == paramAccessibilityEvent.getSourceNodeId()) {
            i = this.mAccessibilityFocusedWindow;
            if (i == paramAccessibilityEvent.getWindowId()) {
              refreshCachedNodeLocked(this.mAccessibilityFocusedWindow, this.mAccessibilityFocus);
              this.mAccessibilityFocus = 2147483647L;
              this.mAccessibilityFocusedWindow = -1;
            } 
          } 
          break;
        case 32768:
          if (this.mAccessibilityFocus != 2147483647L)
            refreshCachedNodeLocked(this.mAccessibilityFocusedWindow, this.mAccessibilityFocus); 
          this.mAccessibilityFocus = paramAccessibilityEvent.getSourceNodeId();
          this.mAccessibilityFocusedWindow = i = paramAccessibilityEvent.getWindowId();
          refreshCachedNodeLocked(i, this.mAccessibilityFocus);
          break;
        case 4096:
          clearSubTreeLocked(paramAccessibilityEvent.getWindowId(), paramAccessibilityEvent.getSourceNodeId());
          break;
        case 2048:
          synchronized (this.mLock) {
            i = paramAccessibilityEvent.getWindowId();
            long l = paramAccessibilityEvent.getSourceNodeId();
            if ((paramAccessibilityEvent.getContentChangeTypes() & 0x1) != 0) {
              clearSubTreeLocked(i, l);
            } else {
              refreshCachedNodeLocked(i, l);
            } 
          } 
          break;
        case 32:
          clear();
          break;
        case 8:
          if (this.mInputFocus != 2147483647L)
            refreshCachedNodeLocked(paramAccessibilityEvent.getWindowId(), this.mInputFocus); 
          this.mInputFocus = paramAccessibilityEvent.getSourceNodeId();
          refreshCachedNodeLocked(paramAccessibilityEvent.getWindowId(), this.mInputFocus);
          break;
        case 1:
        case 4:
        case 16:
        case 8192:
          refreshCachedNodeLocked(paramAccessibilityEvent.getWindowId(), paramAccessibilityEvent.getSourceNodeId());
          break;
      } 
      if (CHECK_INTEGRITY)
        checkIntegrity(); 
      return;
    } 
  }
  
  private void refreshCachedNodeLocked(int paramInt, long paramLong) {
    if (DEBUG)
      Log.i("AccessibilityCache", "Refreshing cached node."); 
    LongSparseArray<AccessibilityNodeInfo> longSparseArray = this.mNodeCache.get(paramInt);
    if (longSparseArray == null)
      return; 
    AccessibilityNodeInfo accessibilityNodeInfo = longSparseArray.get(paramLong);
    if (accessibilityNodeInfo == null)
      return; 
    if (this.mAccessibilityNodeRefresher.refreshNode(accessibilityNodeInfo, true))
      return; 
    clearSubTreeLocked(paramInt, paramLong);
  }
  
  private void refreshCachedWindowLocked(int paramInt) {
    if (DEBUG)
      Log.i("AccessibilityCache", "Refreshing cached window."); 
    if (paramInt == -1)
      return; 
    int i = this.mWindowCacheByDisplay.size();
    for (byte b = 0; b < i; b++) {
      SparseArray<SparseArray<AccessibilityWindowInfo>> sparseArray = this.mWindowCacheByDisplay;
      sparseArray = (SparseArray<SparseArray<AccessibilityWindowInfo>>)sparseArray.valueAt(b);
      if (sparseArray != null) {
        AccessibilityWindowInfo accessibilityWindowInfo = (AccessibilityWindowInfo)sparseArray.get(paramInt);
        if (accessibilityWindowInfo != null) {
          if (!this.mAccessibilityNodeRefresher.refreshWindow(accessibilityWindowInfo))
            clearWindowCacheLocked(); 
          return;
        } 
      } 
    } 
  }
  
  public AccessibilityNodeInfo getNode(int paramInt, long paramLong) {
    synchronized (this.mLock) {
      LongSparseArray<AccessibilityNodeInfo> longSparseArray = this.mNodeCache.get(paramInt);
      if (longSparseArray == null)
        return null; 
      AccessibilityNodeInfo accessibilityNodeInfo2 = longSparseArray.get(paramLong);
      AccessibilityNodeInfo accessibilityNodeInfo1 = accessibilityNodeInfo2;
      if (accessibilityNodeInfo2 != null) {
        accessibilityNodeInfo1 = new AccessibilityNodeInfo();
        this(accessibilityNodeInfo2);
      } 
      if (VERBOSE) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("get(0x");
        stringBuilder.append(Long.toHexString(paramLong));
        stringBuilder.append(") = ");
        stringBuilder.append(accessibilityNodeInfo1);
        Log.i("AccessibilityCache", stringBuilder.toString());
      } 
      return accessibilityNodeInfo1;
    } 
  }
  
  public SparseArray<List<AccessibilityWindowInfo>> getWindowsOnAllDisplays() {
    synchronized (this.mLock) {
      if (!this.mIsAllWindowsCached)
        return null; 
      SparseArray<ArrayList<AccessibilityWindowInfo>> sparseArray = new SparseArray();
      this();
      int i = this.mWindowCacheByDisplay.size();
      if (i > 0) {
        for (byte b = 0; b < i; b++) {
          int j = this.mWindowCacheByDisplay.keyAt(b);
          SparseArray<SparseArray<AccessibilityWindowInfo>> sparseArray1 = this.mWindowCacheByDisplay;
          SparseArray<AccessibilityWindowInfo> sparseArray2 = sparseArray1.valueAt(b);
          if (sparseArray2 != null) {
            int k = sparseArray2.size();
            if (k > 0) {
              SparseArray<AccessibilityWindowInfo> sparseArray3 = this.mTempWindowArray;
              sparseArray3.clear();
              int m;
              for (m = 0; m < k; m++) {
                AccessibilityWindowInfo accessibilityWindowInfo = sparseArray2.valueAt(m);
                sparseArray3.put(accessibilityWindowInfo.getLayer(), accessibilityWindowInfo);
              } 
              m = sparseArray3.size();
              ArrayList<AccessibilityWindowInfo> arrayList = new ArrayList();
              this(m);
              for (; --m >= 0; m--) {
                AccessibilityWindowInfo accessibilityWindowInfo2 = sparseArray3.valueAt(m);
                AccessibilityWindowInfo accessibilityWindowInfo1 = new AccessibilityWindowInfo();
                this(accessibilityWindowInfo2);
                arrayList.add(accessibilityWindowInfo1);
                sparseArray3.removeAt(m);
              } 
              sparseArray.put(j, arrayList);
            } 
          } 
        } 
        return (SparseArray)sparseArray;
      } 
      return null;
    } 
  }
  
  public AccessibilityWindowInfo getWindow(int paramInt) {
    synchronized (this.mLock) {
      int i = this.mWindowCacheByDisplay.size();
      for (byte b = 0; b < i; b++) {
        SparseArray<SparseArray<AccessibilityWindowInfo>> sparseArray = this.mWindowCacheByDisplay;
        sparseArray = (SparseArray<SparseArray<AccessibilityWindowInfo>>)sparseArray.valueAt(b);
        if (sparseArray != null) {
          AccessibilityWindowInfo accessibilityWindowInfo = (AccessibilityWindowInfo)sparseArray.get(paramInt);
          if (accessibilityWindowInfo != null) {
            AccessibilityWindowInfo accessibilityWindowInfo1 = new AccessibilityWindowInfo();
            this(accessibilityWindowInfo);
            return accessibilityWindowInfo1;
          } 
        } 
      } 
      return null;
    } 
  }
  
  public void add(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    synchronized (this.mLock) {
      if (VERBOSE) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("add(");
        stringBuilder.append(paramAccessibilityNodeInfo);
        stringBuilder.append(")");
        Log.i("AccessibilityCache", stringBuilder.toString());
      } 
      int i = paramAccessibilityNodeInfo.getWindowId();
      LongSparseArray<AccessibilityNodeInfo> longSparseArray2 = this.mNodeCache.get(i);
      LongSparseArray<AccessibilityNodeInfo> longSparseArray1 = longSparseArray2;
      if (longSparseArray2 == null) {
        longSparseArray1 = new LongSparseArray();
        this();
        this.mNodeCache.put(i, longSparseArray1);
      } 
      long l = paramAccessibilityNodeInfo.getSourceNodeId();
      AccessibilityNodeInfo accessibilityNodeInfo2 = longSparseArray1.get(l);
      if (accessibilityNodeInfo2 != null) {
        LongArray longArray = paramAccessibilityNodeInfo.getChildNodeIds();
        int j = accessibilityNodeInfo2.getChildCount();
        for (byte b = 0; b < j; b++) {
          long l2 = accessibilityNodeInfo2.getChildId(b);
          if (longArray == null || longArray.indexOf(l2) < 0)
            clearSubTreeLocked(i, l2); 
          if (longSparseArray1.get(l) == null) {
            clearNodesForWindowLocked(i);
            return;
          } 
        } 
        long l1 = accessibilityNodeInfo2.getParentNodeId();
        if (paramAccessibilityNodeInfo.getParentNodeId() != l1)
          clearSubTreeLocked(i, l1); 
      } 
      AccessibilityNodeInfo accessibilityNodeInfo1 = new AccessibilityNodeInfo();
      this(paramAccessibilityNodeInfo);
      longSparseArray1.put(l, accessibilityNodeInfo1);
      if (accessibilityNodeInfo1.isAccessibilityFocused()) {
        if (this.mAccessibilityFocus != 2147483647L && this.mAccessibilityFocus != l)
          refreshCachedNodeLocked(i, this.mAccessibilityFocus); 
        this.mAccessibilityFocus = l;
        this.mAccessibilityFocusedWindow = i;
      } else if (this.mAccessibilityFocus == l) {
        this.mAccessibilityFocus = 2147483647L;
        this.mAccessibilityFocusedWindow = -1;
      } 
      if (accessibilityNodeInfo1.isFocused())
        this.mInputFocus = l; 
      return;
    } 
  }
  
  public void clear() {
    synchronized (this.mLock) {
      if (DEBUG)
        Log.i("AccessibilityCache", "clear()"); 
      clearWindowCacheLocked();
      int i = this.mNodeCache.size();
      for (; --i >= 0; i--) {
        int j = this.mNodeCache.keyAt(i);
        clearNodesForWindowLocked(j);
      } 
      this.mAccessibilityFocus = 2147483647L;
      this.mInputFocus = 2147483647L;
      this.mAccessibilityFocusedWindow = -1;
      return;
    } 
  }
  
  private void clearWindowCacheLocked() {
    if (DEBUG)
      Log.i("AccessibilityCache", "clearWindowCacheLocked"); 
    int i = this.mWindowCacheByDisplay.size();
    if (i > 0)
      for (; --i >= 0; i--) {
        int j = this.mWindowCacheByDisplay.keyAt(i);
        SparseArray<SparseArray<AccessibilityWindowInfo>> sparseArray = this.mWindowCacheByDisplay;
        sparseArray = (SparseArray<SparseArray<AccessibilityWindowInfo>>)sparseArray.get(j);
        if (sparseArray != null)
          sparseArray.clear(); 
        this.mWindowCacheByDisplay.remove(j);
      }  
    this.mIsAllWindowsCached = false;
  }
  
  private void clearNodesForWindowLocked(int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("clearNodesForWindowLocked(");
      stringBuilder.append(paramInt);
      stringBuilder.append(")");
      Log.i("AccessibilityCache", stringBuilder.toString());
    } 
    LongSparseArray longSparseArray = this.mNodeCache.get(paramInt);
    if (longSparseArray == null)
      return; 
    this.mNodeCache.remove(paramInt);
  }
  
  private void clearSubTreeLocked(int paramInt, long paramLong) {
    if (DEBUG)
      Log.i("AccessibilityCache", "Clearing cached subtree."); 
    LongSparseArray<AccessibilityNodeInfo> longSparseArray = this.mNodeCache.get(paramInt);
    if (longSparseArray != null)
      clearSubTreeRecursiveLocked(longSparseArray, paramLong); 
  }
  
  private boolean clearSubTreeRecursiveLocked(LongSparseArray<AccessibilityNodeInfo> paramLongSparseArray, long paramLong) {
    AccessibilityNodeInfo accessibilityNodeInfo = paramLongSparseArray.get(paramLong);
    if (accessibilityNodeInfo == null) {
      clear();
      return true;
    } 
    paramLongSparseArray.remove(paramLong);
    int i = accessibilityNodeInfo.getChildCount();
    for (byte b = 0; b < i; b++) {
      paramLong = accessibilityNodeInfo.getChildId(b);
      if (clearSubTreeRecursiveLocked(paramLongSparseArray, paramLong))
        return true; 
    } 
    return false;
  }
  
  public void checkIntegrity() {
    synchronized (this.mLock) {
      if (this.mWindowCacheByDisplay.size() <= 0 && this.mNodeCache.size() == 0)
        return; 
      SparseArray<SparseArray<AccessibilityWindowInfo>> sparseArray = null;
      StringBuilder stringBuilder1 = null;
      int i = this.mWindowCacheByDisplay.size();
      int j;
      for (j = 0; j < i; j++, accessibilityWindowInfo1 = accessibilityWindowInfo3, accessibilityWindowInfo2 = accessibilityWindowInfo4) {
        AccessibilityWindowInfo accessibilityWindowInfo1, accessibilityWindowInfo2, accessibilityWindowInfo3, accessibilityWindowInfo4;
        SparseArray<SparseArray<AccessibilityWindowInfo>> sparseArray1 = this.mWindowCacheByDisplay;
        SparseArray<AccessibilityWindowInfo> sparseArray2 = sparseArray1.valueAt(j);
        if (sparseArray2 == null) {
          sparseArray1 = sparseArray;
          StringBuilder stringBuilder = stringBuilder1;
        } else {
          int m = sparseArray2.size();
          byte b1 = 0;
          while (true) {
            sparseArray1 = sparseArray;
            StringBuilder stringBuilder = stringBuilder1;
            if (b1 < m) {
              accessibilityWindowInfo3 = sparseArray2.valueAt(b1);
              stringBuilder = stringBuilder1;
              if (accessibilityWindowInfo3.isActive())
                if (stringBuilder1 != null) {
                  stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("Duplicate active window:");
                  stringBuilder.append(accessibilityWindowInfo3);
                  Log.e("AccessibilityCache", stringBuilder.toString());
                  stringBuilder = stringBuilder1;
                } else {
                  accessibilityWindowInfo4 = accessibilityWindowInfo3;
                }  
              SparseArray<SparseArray<AccessibilityWindowInfo>> sparseArray3 = sparseArray;
              if (accessibilityWindowInfo3.isFocused())
                if (sparseArray != null) {
                  StringBuilder stringBuilder4 = new StringBuilder();
                  this();
                  stringBuilder4.append("Duplicate focused window:");
                  stringBuilder4.append(accessibilityWindowInfo3);
                  Log.e("AccessibilityCache", stringBuilder4.toString());
                  SparseArray<SparseArray<AccessibilityWindowInfo>> sparseArray4 = sparseArray;
                } else {
                  accessibilityWindowInfo6 = accessibilityWindowInfo3;
                }  
              b1++;
              AccessibilityWindowInfo accessibilityWindowInfo5 = accessibilityWindowInfo6, accessibilityWindowInfo6 = accessibilityWindowInfo4;
              continue;
            } 
            break;
          } 
        } 
      } 
      StringBuilder stringBuilder3 = null;
      StringBuilder stringBuilder2 = null;
      int k = this.mNodeCache.size();
      for (byte b = 0; b < k; b++) {
        LongSparseArray<AccessibilityNodeInfo> longSparseArray = this.mNodeCache.valueAt(b);
        if (longSparseArray.size() > 0) {
          ArraySet<AccessibilityNodeInfo> arraySet = new ArraySet();
          this();
          int m = this.mNodeCache.keyAt(b);
          int n = longSparseArray.size();
          for (i = 0; i < n; i++) {
            AccessibilityNodeInfo accessibilityNodeInfo = longSparseArray.valueAt(i);
            if (!arraySet.add(accessibilityNodeInfo)) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Duplicate node: ");
              stringBuilder.append(accessibilityNodeInfo);
              stringBuilder.append(" in window:");
              stringBuilder.append(m);
              Log.e("AccessibilityCache", stringBuilder.toString());
            } else {
              AccessibilityNodeInfo accessibilityNodeInfo2;
              StringBuilder stringBuilder = stringBuilder3;
              if (accessibilityNodeInfo.isAccessibilityFocused())
                if (stringBuilder3 != null) {
                  stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("Duplicate accessibility focus:");
                  stringBuilder.append(accessibilityNodeInfo);
                  stringBuilder.append(" in window:");
                  stringBuilder.append(m);
                  Log.e("AccessibilityCache", stringBuilder.toString());
                  stringBuilder = stringBuilder3;
                } else {
                  accessibilityNodeInfo2 = accessibilityNodeInfo;
                }  
              stringBuilder3 = stringBuilder2;
              if (accessibilityNodeInfo.isFocused())
                if (stringBuilder2 != null) {
                  stringBuilder3 = new StringBuilder();
                  this();
                  stringBuilder3.append("Duplicate input focus: ");
                  stringBuilder3.append(accessibilityNodeInfo);
                  stringBuilder3.append(" in window:");
                  stringBuilder3.append(m);
                  Log.e("AccessibilityCache", stringBuilder3.toString());
                  stringBuilder3 = stringBuilder2;
                } else {
                  accessibilityNodeInfo3 = accessibilityNodeInfo;
                }  
              AccessibilityNodeInfo accessibilityNodeInfo1 = longSparseArray.get(accessibilityNodeInfo.getParentNodeId());
              if (accessibilityNodeInfo1 != null) {
                int i2 = accessibilityNodeInfo1.getChildCount();
                byte b2 = 0;
                while (true) {
                  if (b2 < i2) {
                    AccessibilityNodeInfo accessibilityNodeInfo4 = longSparseArray.get(accessibilityNodeInfo1.getChildId(b2));
                    if (accessibilityNodeInfo4 == accessibilityNodeInfo) {
                      b2 = 1;
                      break;
                    } 
                    b2++;
                    continue;
                  } 
                  b2 = 0;
                  break;
                } 
                if (b2 == 0) {
                  StringBuilder stringBuilder4 = new StringBuilder();
                  this();
                  stringBuilder4.append("Invalid parent-child relation between parent: ");
                  stringBuilder4.append(accessibilityNodeInfo1);
                  stringBuilder4.append(" and child: ");
                  stringBuilder4.append(accessibilityNodeInfo);
                  Log.e("AccessibilityCache", stringBuilder4.toString());
                } 
              } 
              int i1 = accessibilityNodeInfo.getChildCount();
              for (byte b1 = 0; b1 < i1; b1++) {
                AccessibilityNodeInfo accessibilityNodeInfo4 = longSparseArray.get(accessibilityNodeInfo.getChildId(b1));
                if (accessibilityNodeInfo4 != null) {
                  accessibilityNodeInfo4 = longSparseArray.get(accessibilityNodeInfo4.getParentNodeId());
                  if (accessibilityNodeInfo4 != accessibilityNodeInfo) {
                    StringBuilder stringBuilder4 = new StringBuilder();
                    this();
                    stringBuilder4.append("Invalid child-parent relation between child: ");
                    stringBuilder4.append(accessibilityNodeInfo);
                    stringBuilder4.append(" and parent: ");
                    stringBuilder4.append(accessibilityNodeInfo1);
                    Log.e("AccessibilityCache", stringBuilder4.toString());
                  } 
                } 
              } 
              accessibilityNodeInfo1 = accessibilityNodeInfo3;
              AccessibilityNodeInfo accessibilityNodeInfo3 = accessibilityNodeInfo2;
            } 
          } 
        } 
      } 
      return;
    } 
  }
  
  public static class AccessibilityNodeRefresher {
    public boolean refreshNode(AccessibilityNodeInfo param1AccessibilityNodeInfo, boolean param1Boolean) {
      return param1AccessibilityNodeInfo.refresh(null, param1Boolean);
    }
    
    public boolean refreshWindow(AccessibilityWindowInfo param1AccessibilityWindowInfo) {
      return param1AccessibilityWindowInfo.refresh();
    }
  }
}
