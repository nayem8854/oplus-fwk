package android.view.accessibility;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pools;
import com.android.internal.util.BitUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.function.IntFunction;

public final class AccessibilityEvent extends AccessibilityRecord implements Parcelable {
  public static final int CONTENT_CHANGE_TYPE_CONTENT_DESCRIPTION = 4;
  
  public static final int CONTENT_CHANGE_TYPE_PANE_APPEARED = 16;
  
  public static final int CONTENT_CHANGE_TYPE_PANE_DISAPPEARED = 32;
  
  public static final int CONTENT_CHANGE_TYPE_PANE_TITLE = 8;
  
  public static final int CONTENT_CHANGE_TYPE_STATE_DESCRIPTION = 64;
  
  public static final int CONTENT_CHANGE_TYPE_SUBTREE = 1;
  
  public static final int CONTENT_CHANGE_TYPE_TEXT = 2;
  
  public static final int CONTENT_CHANGE_TYPE_UNDEFINED = 0;
  
  public static final Parcelable.Creator<AccessibilityEvent> CREATOR;
  
  private static final boolean DEBUG;
  
  public static final boolean DEBUG_ORIGIN = false;
  
  public static final int INVALID_POSITION = -1;
  
  private static final String LOG_TAG = "AccessibilityEvent";
  
  private static final int MAX_POOL_SIZE = 10;
  
  @Deprecated
  public static final int MAX_TEXT_LENGTH = 500;
  
  public static final int TYPES_ALL_MASK = -1;
  
  public static final int TYPE_ANNOUNCEMENT = 16384;
  
  public static final int TYPE_ASSIST_READING_CONTEXT = 16777216;
  
  public static final int TYPE_GESTURE_DETECTION_END = 524288;
  
  public static final int TYPE_GESTURE_DETECTION_START = 262144;
  
  public static final int TYPE_NOTIFICATION_STATE_CHANGED = 64;
  
  public static final int TYPE_TOUCH_EXPLORATION_GESTURE_END = 1024;
  
  public static final int TYPE_TOUCH_EXPLORATION_GESTURE_START = 512;
  
  public static final int TYPE_TOUCH_INTERACTION_END = 2097152;
  
  public static final int TYPE_TOUCH_INTERACTION_START = 1048576;
  
  public static final int TYPE_VIEW_ACCESSIBILITY_FOCUSED = 32768;
  
  public static final int TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED = 65536;
  
  public static final int TYPE_VIEW_CLICKED = 1;
  
  public static final int TYPE_VIEW_CONTEXT_CLICKED = 8388608;
  
  public static final int TYPE_VIEW_FOCUSED = 8;
  
  public static final int TYPE_VIEW_HOVER_ENTER = 128;
  
  public static final int TYPE_VIEW_HOVER_EXIT = 256;
  
  public static final int TYPE_VIEW_LONG_CLICKED = 2;
  
  public static final int TYPE_VIEW_SCROLLED = 4096;
  
  public static final int TYPE_VIEW_SELECTED = 4;
  
  public static final int TYPE_VIEW_TEXT_CHANGED = 16;
  
  public static final int TYPE_VIEW_TEXT_SELECTION_CHANGED = 8192;
  
  public static final int TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY = 131072;
  
  public static final int TYPE_WINDOWS_CHANGED = 4194304;
  
  public static final int TYPE_WINDOW_CONTENT_CHANGED = 2048;
  
  public static final int TYPE_WINDOW_STATE_CHANGED = 32;
  
  public static final int WINDOWS_CHANGE_ACCESSIBILITY_FOCUSED = 128;
  
  public static final int WINDOWS_CHANGE_ACTIVE = 32;
  
  public static final int WINDOWS_CHANGE_ADDED = 1;
  
  public static final int WINDOWS_CHANGE_BOUNDS = 8;
  
  public static final int WINDOWS_CHANGE_CHILDREN = 512;
  
  public static final int WINDOWS_CHANGE_FOCUSED = 64;
  
  public static final int WINDOWS_CHANGE_LAYER = 16;
  
  public static final int WINDOWS_CHANGE_PARENT = 256;
  
  public static final int WINDOWS_CHANGE_PIP = 1024;
  
  public static final int WINDOWS_CHANGE_REMOVED = 2;
  
  public static final int WINDOWS_CHANGE_TITLE = 4;
  
  static {
    boolean bool;
    if (Log.isLoggable("AccessibilityEvent", 3) && Build.IS_DEBUGGABLE) {
      bool = true;
    } else {
      bool = false;
    } 
    DEBUG = bool;
  }
  
  private static final Pools.SynchronizedPool<AccessibilityEvent> sPool = new Pools.SynchronizedPool<>(10);
  
  int mAction;
  
  int mContentChangeTypes;
  
  private long mEventTime;
  
  private int mEventType;
  
  int mMovementGranularity;
  
  private CharSequence mPackageName;
  
  private ArrayList<AccessibilityRecord> mRecords;
  
  int mWindowChangeTypes;
  
  public StackTraceElement[] originStackTrace = null;
  
  public AccessibilityEvent(int paramInt) {
    this.mEventType = paramInt;
  }
  
  public AccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    init(paramAccessibilityEvent);
  }
  
  void init(AccessibilityEvent paramAccessibilityEvent) {
    init(paramAccessibilityEvent);
    this.mEventType = paramAccessibilityEvent.mEventType;
    this.mMovementGranularity = paramAccessibilityEvent.mMovementGranularity;
    this.mAction = paramAccessibilityEvent.mAction;
    this.mContentChangeTypes = paramAccessibilityEvent.mContentChangeTypes;
    this.mWindowChangeTypes = paramAccessibilityEvent.mWindowChangeTypes;
    this.mEventTime = paramAccessibilityEvent.mEventTime;
    this.mPackageName = paramAccessibilityEvent.mPackageName;
    ArrayList<AccessibilityRecord> arrayList = paramAccessibilityEvent.mRecords;
    if (arrayList != null) {
      int i = arrayList.size();
      this.mRecords = new ArrayList<>(i);
      for (byte b = 0; b < i; b++) {
        AccessibilityRecord accessibilityRecord = paramAccessibilityEvent.mRecords.get(b);
        accessibilityRecord = new AccessibilityRecord(accessibilityRecord);
        this.mRecords.add(accessibilityRecord);
      } 
    } 
  }
  
  public void setSealed(boolean paramBoolean) {
    super.setSealed(paramBoolean);
    ArrayList<AccessibilityRecord> arrayList = this.mRecords;
    if (arrayList != null) {
      int i = arrayList.size();
      for (byte b = 0; b < i; b++) {
        AccessibilityRecord accessibilityRecord = arrayList.get(b);
        accessibilityRecord.setSealed(paramBoolean);
      } 
    } 
  }
  
  public int getRecordCount() {
    int i;
    ArrayList<AccessibilityRecord> arrayList = this.mRecords;
    if (arrayList == null) {
      i = 0;
    } else {
      i = arrayList.size();
    } 
    return i;
  }
  
  public void appendRecord(AccessibilityRecord paramAccessibilityRecord) {
    enforceNotSealed();
    if (this.mRecords == null)
      this.mRecords = new ArrayList<>(); 
    this.mRecords.add(paramAccessibilityRecord);
  }
  
  public AccessibilityRecord getRecord(int paramInt) {
    ArrayList<AccessibilityRecord> arrayList = this.mRecords;
    if (arrayList != null)
      return arrayList.get(paramInt); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid index ");
    stringBuilder.append(paramInt);
    stringBuilder.append(", size is 0");
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int getEventType() {
    return this.mEventType;
  }
  
  public int getContentChangeTypes() {
    return this.mContentChangeTypes;
  }
  
  private static String contentChangeTypesToString(int paramInt) {
    return BitUtils.flagsToString(paramInt, (IntFunction)_$$Lambda$AccessibilityEvent$gjyLj65KEDUo5PJZiVYxPrd2Vug.INSTANCE);
  }
  
  private static String singleContentChangeTypeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 4) {
            if (paramInt != 8) {
              if (paramInt != 16) {
                if (paramInt != 32) {
                  if (paramInt != 64)
                    return Integer.toHexString(paramInt); 
                  return "CONTENT_CHANGE_TYPE_STATE_DESCRIPTION";
                } 
                return "CONTENT_CHANGE_TYPE_PANE_DISAPPEARED";
              } 
              return "CONTENT_CHANGE_TYPE_PANE_APPEARED";
            } 
            return "CONTENT_CHANGE_TYPE_PANE_TITLE";
          } 
          return "CONTENT_CHANGE_TYPE_CONTENT_DESCRIPTION";
        } 
        return "CONTENT_CHANGE_TYPE_TEXT";
      } 
      return "CONTENT_CHANGE_TYPE_SUBTREE";
    } 
    return "CONTENT_CHANGE_TYPE_UNDEFINED";
  }
  
  public void setContentChangeTypes(int paramInt) {
    enforceNotSealed();
    this.mContentChangeTypes = paramInt;
  }
  
  public int getWindowChanges() {
    return this.mWindowChangeTypes;
  }
  
  public void setWindowChanges(int paramInt) {
    this.mWindowChangeTypes = paramInt;
  }
  
  private static String windowChangeTypesToString(int paramInt) {
    return BitUtils.flagsToString(paramInt, (IntFunction)_$$Lambda$AccessibilityEvent$c6ikd5OkCnJv2aVsheVXIxBvSTk.INSTANCE);
  }
  
  private static String singleWindowChangeTypeToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt != 64) {
                  if (paramInt != 128) {
                    if (paramInt != 256) {
                      if (paramInt != 512) {
                        if (paramInt != 1024)
                          return Integer.toHexString(paramInt); 
                        return "WINDOWS_CHANGE_PIP";
                      } 
                      return "WINDOWS_CHANGE_CHILDREN";
                    } 
                    return "WINDOWS_CHANGE_PARENT";
                  } 
                  return "WINDOWS_CHANGE_ACCESSIBILITY_FOCUSED";
                } 
                return "WINDOWS_CHANGE_FOCUSED";
              } 
              return "WINDOWS_CHANGE_ACTIVE";
            } 
            return "WINDOWS_CHANGE_LAYER";
          } 
          return "WINDOWS_CHANGE_BOUNDS";
        } 
        return "WINDOWS_CHANGE_TITLE";
      } 
      return "WINDOWS_CHANGE_REMOVED";
    } 
    return "WINDOWS_CHANGE_ADDED";
  }
  
  public void setEventType(int paramInt) {
    enforceNotSealed();
    this.mEventType = paramInt;
  }
  
  public long getEventTime() {
    return this.mEventTime;
  }
  
  public void setEventTime(long paramLong) {
    enforceNotSealed();
    this.mEventTime = paramLong;
  }
  
  public CharSequence getPackageName() {
    return this.mPackageName;
  }
  
  public void setPackageName(CharSequence paramCharSequence) {
    enforceNotSealed();
    this.mPackageName = paramCharSequence;
  }
  
  public void setMovementGranularity(int paramInt) {
    enforceNotSealed();
    this.mMovementGranularity = paramInt;
  }
  
  public int getMovementGranularity() {
    return this.mMovementGranularity;
  }
  
  public void setAction(int paramInt) {
    enforceNotSealed();
    this.mAction = paramInt;
  }
  
  public int getAction() {
    return this.mAction;
  }
  
  public static AccessibilityEvent obtainWindowsChangedEvent(int paramInt1, int paramInt2) {
    AccessibilityEvent accessibilityEvent = obtain(4194304);
    accessibilityEvent.setWindowId(paramInt1);
    accessibilityEvent.setWindowChanges(paramInt2);
    accessibilityEvent.setImportantForAccessibility(true);
    return accessibilityEvent;
  }
  
  public static AccessibilityEvent obtain(int paramInt) {
    AccessibilityEvent accessibilityEvent = obtain();
    accessibilityEvent.setEventType(paramInt);
    return accessibilityEvent;
  }
  
  public static AccessibilityEvent obtain(AccessibilityEvent paramAccessibilityEvent) {
    AccessibilityEvent accessibilityEvent = obtain();
    accessibilityEvent.init(paramAccessibilityEvent);
    return accessibilityEvent;
  }
  
  public static AccessibilityEvent obtain() {
    AccessibilityEvent accessibilityEvent1 = sPool.acquire();
    AccessibilityEvent accessibilityEvent2 = accessibilityEvent1;
    if (accessibilityEvent1 == null)
      accessibilityEvent2 = new AccessibilityEvent(); 
    return accessibilityEvent2;
  }
  
  public void recycle() {
    clear();
    sPool.release(this);
  }
  
  protected void clear() {
    super.clear();
    this.mEventType = 0;
    this.mMovementGranularity = 0;
    this.mAction = 0;
    this.mContentChangeTypes = 0;
    this.mWindowChangeTypes = 0;
    this.mPackageName = null;
    this.mEventTime = 0L;
    if (this.mRecords != null)
      while (!this.mRecords.isEmpty()) {
        AccessibilityRecord accessibilityRecord = this.mRecords.remove(0);
        accessibilityRecord.recycle();
      }  
  }
  
  public void initFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    this.mSealed = bool;
    this.mEventType = paramParcel.readInt();
    this.mMovementGranularity = paramParcel.readInt();
    this.mAction = paramParcel.readInt();
    this.mContentChangeTypes = paramParcel.readInt();
    this.mWindowChangeTypes = paramParcel.readInt();
    this.mPackageName = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mEventTime = paramParcel.readLong();
    this.mConnectionId = paramParcel.readInt();
    readAccessibilityRecordFromParcel(this, paramParcel);
    int j = paramParcel.readInt();
    if (j > 0) {
      this.mRecords = new ArrayList<>(j);
      for (i = 0; i < j; i++) {
        AccessibilityRecord accessibilityRecord = AccessibilityRecord.obtain();
        readAccessibilityRecordFromParcel(accessibilityRecord, paramParcel);
        accessibilityRecord.mConnectionId = this.mConnectionId;
        this.mRecords.add(accessibilityRecord);
      } 
    } 
  }
  
  private void readAccessibilityRecordFromParcel(AccessibilityRecord paramAccessibilityRecord, Parcel paramParcel) {
    paramAccessibilityRecord.mBooleanProperties = paramParcel.readInt();
    paramAccessibilityRecord.mCurrentItemIndex = paramParcel.readInt();
    paramAccessibilityRecord.mItemCount = paramParcel.readInt();
    paramAccessibilityRecord.mFromIndex = paramParcel.readInt();
    paramAccessibilityRecord.mToIndex = paramParcel.readInt();
    paramAccessibilityRecord.mScrollX = paramParcel.readInt();
    paramAccessibilityRecord.mScrollY = paramParcel.readInt();
    paramAccessibilityRecord.mScrollDeltaX = paramParcel.readInt();
    paramAccessibilityRecord.mScrollDeltaY = paramParcel.readInt();
    paramAccessibilityRecord.mMaxScrollX = paramParcel.readInt();
    paramAccessibilityRecord.mMaxScrollY = paramParcel.readInt();
    paramAccessibilityRecord.mAddedCount = paramParcel.readInt();
    paramAccessibilityRecord.mRemovedCount = paramParcel.readInt();
    paramAccessibilityRecord.mClassName = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    paramAccessibilityRecord.mContentDescription = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    paramAccessibilityRecord.mBeforeText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    paramAccessibilityRecord.mParcelableData = paramParcel.readParcelable(null);
    paramParcel.readList(paramAccessibilityRecord.mText, null);
    paramAccessibilityRecord.mSourceWindowId = paramParcel.readInt();
    paramAccessibilityRecord.mSourceNodeId = paramParcel.readLong();
    int i = paramParcel.readInt();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    paramAccessibilityRecord.mSealed = bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(isSealed());
    paramParcel.writeInt(this.mEventType);
    paramParcel.writeInt(this.mMovementGranularity);
    paramParcel.writeInt(this.mAction);
    paramParcel.writeInt(this.mContentChangeTypes);
    paramParcel.writeInt(this.mWindowChangeTypes);
    TextUtils.writeToParcel(this.mPackageName, paramParcel, 0);
    paramParcel.writeLong(this.mEventTime);
    paramParcel.writeInt(this.mConnectionId);
    writeAccessibilityRecordToParcel(this, paramParcel, paramInt);
    int i = getRecordCount();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++) {
      AccessibilityRecord accessibilityRecord = this.mRecords.get(b);
      writeAccessibilityRecordToParcel(accessibilityRecord, paramParcel, paramInt);
    } 
  }
  
  private void writeAccessibilityRecordToParcel(AccessibilityRecord paramAccessibilityRecord, Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(paramAccessibilityRecord.mBooleanProperties);
    paramParcel.writeInt(paramAccessibilityRecord.mCurrentItemIndex);
    paramParcel.writeInt(paramAccessibilityRecord.mItemCount);
    paramParcel.writeInt(paramAccessibilityRecord.mFromIndex);
    paramParcel.writeInt(paramAccessibilityRecord.mToIndex);
    paramParcel.writeInt(paramAccessibilityRecord.mScrollX);
    paramParcel.writeInt(paramAccessibilityRecord.mScrollY);
    paramParcel.writeInt(paramAccessibilityRecord.mScrollDeltaX);
    paramParcel.writeInt(paramAccessibilityRecord.mScrollDeltaY);
    paramParcel.writeInt(paramAccessibilityRecord.mMaxScrollX);
    paramParcel.writeInt(paramAccessibilityRecord.mMaxScrollY);
    paramParcel.writeInt(paramAccessibilityRecord.mAddedCount);
    paramParcel.writeInt(paramAccessibilityRecord.mRemovedCount);
    TextUtils.writeToParcel(paramAccessibilityRecord.mClassName, paramParcel, paramInt);
    TextUtils.writeToParcel(paramAccessibilityRecord.mContentDescription, paramParcel, paramInt);
    TextUtils.writeToParcel(paramAccessibilityRecord.mBeforeText, paramParcel, paramInt);
    paramParcel.writeParcelable(paramAccessibilityRecord.mParcelableData, paramInt);
    paramParcel.writeList(paramAccessibilityRecord.mText);
    paramParcel.writeInt(paramAccessibilityRecord.mSourceWindowId);
    paramParcel.writeLong(paramAccessibilityRecord.mSourceNodeId);
    paramParcel.writeInt(paramAccessibilityRecord.mSealed);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("EventType: ");
    stringBuilder.append(eventTypeToString(this.mEventType));
    stringBuilder.append("; EventTime: ");
    stringBuilder.append(this.mEventTime);
    stringBuilder.append("; PackageName: ");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append("; MovementGranularity: ");
    stringBuilder.append(this.mMovementGranularity);
    stringBuilder.append("; Action: ");
    stringBuilder.append(this.mAction);
    stringBuilder.append("; ContentChangeTypes: ");
    int i = this.mContentChangeTypes;
    String str = contentChangeTypesToString(i);
    stringBuilder.append(str);
    stringBuilder.append("; WindowChangeTypes: ");
    i = this.mWindowChangeTypes;
    str = windowChangeTypesToString(i);
    stringBuilder.append(str);
    appendTo(stringBuilder);
    if (!DEBUG) {
      stringBuilder.append("; recordCount: ");
      stringBuilder.append(getRecordCount());
    } else {
      stringBuilder.append("\n");
      if (DEBUG) {
        stringBuilder.append("; SourceWindowId: 0x");
        stringBuilder.append(Long.toHexString(this.mSourceWindowId));
        stringBuilder.append("; SourceNodeId: 0x");
        stringBuilder.append(Long.toHexString(this.mSourceNodeId));
      } 
      for (i = 0; i < getRecordCount(); i++) {
        stringBuilder.append("  Record ");
        stringBuilder.append(i);
        stringBuilder.append(":");
        getRecord(i).appendTo(stringBuilder).append("\n");
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static String eventTypeToString(int paramInt) {
    if (paramInt == -1)
      return "TYPES_ALL_MASK"; 
    StringBuilder stringBuilder = new StringBuilder();
    byte b = 0;
    while (paramInt != 0) {
      int i = 1 << Integer.numberOfTrailingZeros(paramInt);
      paramInt &= i ^ 0xFFFFFFFF;
      if (b)
        stringBuilder.append(", "); 
      stringBuilder.append(singleEventTypeToString(i));
      b++;
    } 
    if (b > 1) {
      stringBuilder.insert(0, '[');
      stringBuilder.append(']');
    } 
    return stringBuilder.toString();
  }
  
  private static String singleEventTypeToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        switch (paramInt) {
          default:
            return Integer.toHexString(paramInt);
          case 16777216:
            return "TYPE_ASSIST_READING_CONTEXT";
          case 8388608:
            return "TYPE_VIEW_CONTEXT_CLICKED";
          case 4194304:
            return "TYPE_WINDOWS_CHANGED";
          case 2097152:
            return "TYPE_TOUCH_INTERACTION_END";
          case 1048576:
            return "TYPE_TOUCH_INTERACTION_START";
          case 524288:
            return "TYPE_GESTURE_DETECTION_END";
          case 262144:
            return "TYPE_GESTURE_DETECTION_START";
          case 131072:
            return "TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY";
          case 65536:
            return "TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED";
          case 32768:
            return "TYPE_VIEW_ACCESSIBILITY_FOCUSED";
          case 16384:
            return "TYPE_ANNOUNCEMENT";
          case 8192:
            return "TYPE_VIEW_TEXT_SELECTION_CHANGED";
          case 4096:
            return "TYPE_VIEW_SCROLLED";
          case 2048:
            return "TYPE_WINDOW_CONTENT_CHANGED";
          case 1024:
            return "TYPE_TOUCH_EXPLORATION_GESTURE_END";
          case 512:
            return "TYPE_TOUCH_EXPLORATION_GESTURE_START";
          case 256:
            return "TYPE_VIEW_HOVER_EXIT";
          case 128:
            return "TYPE_VIEW_HOVER_ENTER";
          case 64:
            return "TYPE_NOTIFICATION_STATE_CHANGED";
          case 32:
            return "TYPE_WINDOW_STATE_CHANGED";
          case 16:
            return "TYPE_VIEW_TEXT_CHANGED";
          case 8:
            return "TYPE_VIEW_FOCUSED";
          case 4:
            break;
        } 
        return "TYPE_VIEW_SELECTED";
      } 
      return "TYPE_VIEW_LONG_CLICKED";
    } 
    return "TYPE_VIEW_CLICKED";
  }
  
  static {
    CREATOR = (Parcelable.Creator<AccessibilityEvent>)new Object();
  }
  
  public AccessibilityEvent() {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ContentChangeTypes implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class EventType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class WindowsChangeTypes implements Annotation {}
}
