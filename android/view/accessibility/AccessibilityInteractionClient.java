package android.view.accessibility;

import android.accessibilityservice.IAccessibilityServiceConnection;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.SparseArray;
import android.util.SparseLongArray;
import android.view.ViewConfiguration;
import com.android.internal.util.ArrayUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class AccessibilityInteractionClient extends IAccessibilityInteractionConnectionCallback.Stub {
  private static final long DISABLE_PREFETCHING_FOR_SCROLLING_MILLIS = (long)(ViewConfiguration.getSendRecurringAccessibilityEventsInterval() * 1.5D);
  
  private static final Object sStaticLock = new Object();
  
  static {
    sClients = new LongSparseArray<>();
    sConnectionCache = new SparseArray<>();
    sScrollingWindows = new SparseLongArray();
    sAccessibilityCache = new AccessibilityCache(new AccessibilityCache.AccessibilityNodeRefresher());
  }
  
  private final AtomicInteger mInteractionIdCounter = new AtomicInteger();
  
  private final Object mInstanceLock = new Object();
  
  private volatile int mInteractionId = -1;
  
  private static final boolean CHECK_INTEGRITY = true;
  
  private static final boolean DEBUG = false;
  
  private static final String LOG_TAG = "AccessibilityInteractionClient";
  
  public static final int NO_ID = -1;
  
  private static final long TIMEOUT_INTERACTION_MILLIS = 5000L;
  
  private static AccessibilityCache sAccessibilityCache;
  
  private static final LongSparseArray<AccessibilityInteractionClient> sClients;
  
  private static final SparseArray<IAccessibilityServiceConnection> sConnectionCache;
  
  private static final SparseLongArray sScrollingWindows;
  
  private AccessibilityNodeInfo mFindAccessibilityNodeInfoResult;
  
  private List<AccessibilityNodeInfo> mFindAccessibilityNodeInfosResult;
  
  private boolean mPerformAccessibilityActionResult;
  
  private Message mSameThreadMessage;
  
  public static AccessibilityInteractionClient getInstance() {
    long l = Thread.currentThread().getId();
    return getInstanceForThread(l);
  }
  
  public static AccessibilityInteractionClient getInstanceForThread(long paramLong) {
    synchronized (sStaticLock) {
      AccessibilityInteractionClient accessibilityInteractionClient1 = sClients.get(paramLong);
      AccessibilityInteractionClient accessibilityInteractionClient2 = accessibilityInteractionClient1;
      if (accessibilityInteractionClient1 == null) {
        accessibilityInteractionClient2 = new AccessibilityInteractionClient();
        this();
        sClients.put(paramLong, accessibilityInteractionClient2);
      } 
      return accessibilityInteractionClient2;
    } 
  }
  
  public static IAccessibilityServiceConnection getConnection(int paramInt) {
    synchronized (sConnectionCache) {
      return sConnectionCache.get(paramInt);
    } 
  }
  
  public static void addConnection(int paramInt, IAccessibilityServiceConnection paramIAccessibilityServiceConnection) {
    synchronized (sConnectionCache) {
      sConnectionCache.put(paramInt, paramIAccessibilityServiceConnection);
      return;
    } 
  }
  
  public static void removeConnection(int paramInt) {
    synchronized (sConnectionCache) {
      sConnectionCache.remove(paramInt);
      return;
    } 
  }
  
  public static void setCache(AccessibilityCache paramAccessibilityCache) {
    sAccessibilityCache = paramAccessibilityCache;
  }
  
  public void setSameThreadMessage(Message paramMessage) {
    synchronized (this.mInstanceLock) {
      this.mSameThreadMessage = paramMessage;
      this.mInstanceLock.notifyAll();
      return;
    } 
  }
  
  public AccessibilityNodeInfo getRootInActiveWindow(int paramInt) {
    return findAccessibilityNodeInfoByAccessibilityId(paramInt, 2147483647, AccessibilityNodeInfo.ROOT_NODE_ID, false, 4, (Bundle)null);
  }
  
  public AccessibilityWindowInfo getWindow(int paramInt1, int paramInt2) {
    return getWindow(paramInt1, paramInt2, false);
  }
  
  public AccessibilityWindowInfo getWindow(int paramInt1, int paramInt2, boolean paramBoolean) {
    try {
      IAccessibilityServiceConnection iAccessibilityServiceConnection = getConnection(paramInt1);
      if (iAccessibilityServiceConnection != null) {
        if (!paramBoolean) {
          AccessibilityWindowInfo accessibilityWindowInfo = sAccessibilityCache.getWindow(paramInt2);
          if (accessibilityWindowInfo != null)
            return accessibilityWindowInfo; 
        } 
        long l = Binder.clearCallingIdentity();
        try {
          AccessibilityWindowInfo accessibilityWindowInfo = iAccessibilityServiceConnection.getWindow(paramInt2);
          Binder.restoreCallingIdentity(l);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
    } catch (RemoteException remoteException) {
      Log.e("AccessibilityInteractionClient", "Error while calling remote getWindow", (Throwable)remoteException);
    } 
    return null;
  }
  
  public List<AccessibilityWindowInfo> getWindows(int paramInt) {
    SparseArray<List<AccessibilityWindowInfo>> sparseArray = getWindowsOnAllDisplays(paramInt);
    if (sparseArray.size() > 0)
      return sparseArray.valueAt(0); 
    return Collections.emptyList();
  }
  
  public SparseArray<List<AccessibilityWindowInfo>> getWindowsOnAllDisplays(int paramInt) {
    try {
      IAccessibilityServiceConnection iAccessibilityServiceConnection = getConnection(paramInt);
      if (iAccessibilityServiceConnection != null) {
        AccessibilityCache accessibilityCache = sAccessibilityCache;
        SparseArray<List<AccessibilityWindowInfo>> sparseArray = accessibilityCache.getWindowsOnAllDisplays();
        if (sparseArray != null)
          return sparseArray; 
        long l = Binder.clearCallingIdentity();
        try {
          AccessibilityWindowInfo.WindowListSparseArray windowListSparseArray = iAccessibilityServiceConnection.getWindows();
          Binder.restoreCallingIdentity(l);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
    } catch (RemoteException remoteException) {
      Log.e("AccessibilityInteractionClient", "Error while calling remote getWindowsOnAllDisplays", (Throwable)remoteException);
    } 
    return new SparseArray();
  }
  
  public AccessibilityNodeInfo findAccessibilityNodeInfoByAccessibilityId(int paramInt1, IBinder paramIBinder, long paramLong, boolean paramBoolean, int paramInt2, Bundle paramBundle) {
    byte b2;
    if (paramIBinder == null)
      return null; 
    byte b1 = -1;
    try {
      IAccessibilityServiceConnection iAccessibilityServiceConnection = getConnection(paramInt1);
      b2 = b1;
      if (iAccessibilityServiceConnection != null)
        b2 = iAccessibilityServiceConnection.getWindowIdForLeashToken(paramIBinder); 
    } catch (RemoteException remoteException) {
      Log.e("AccessibilityInteractionClient", "Error while calling remote getWindowIdForLeashToken", (Throwable)remoteException);
      b2 = b1;
    } 
    if (b2 == -1)
      return null; 
    return findAccessibilityNodeInfoByAccessibilityId(paramInt1, b2, paramLong, paramBoolean, paramInt2, paramBundle);
  }
  
  public AccessibilityNodeInfo findAccessibilityNodeInfoByAccessibilityId(int paramInt1, int paramInt2, long paramLong, boolean paramBoolean, int paramInt3, Bundle paramBundle) {
    // Byte code:
    //   0: iload #6
    //   2: iconst_2
    //   3: iand
    //   4: ifeq -> 28
    //   7: iload #6
    //   9: iconst_1
    //   10: iand
    //   11: ifeq -> 17
    //   14: goto -> 28
    //   17: new java/lang/IllegalArgumentException
    //   20: dup
    //   21: ldc_w 'FLAG_PREFETCH_SIBLINGS requires FLAG_PREFETCH_PREDECESSORS'
    //   24: invokespecial <init> : (Ljava/lang/String;)V
    //   27: athrow
    //   28: iload_1
    //   29: invokestatic getConnection : (I)Landroid/accessibilityservice/IAccessibilityServiceConnection;
    //   32: astore #8
    //   34: aload #8
    //   36: ifnull -> 336
    //   39: iload #5
    //   41: ifne -> 74
    //   44: getstatic android/view/accessibility/AccessibilityInteractionClient.sAccessibilityCache : Landroid/view/accessibility/AccessibilityCache;
    //   47: astore #9
    //   49: aload #9
    //   51: iload_2
    //   52: lload_3
    //   53: invokevirtual getNode : (IJ)Landroid/view/accessibility/AccessibilityNodeInfo;
    //   56: astore #9
    //   58: aload #9
    //   60: ifnull -> 66
    //   63: aload #9
    //   65: areturn
    //   66: goto -> 81
    //   69: astore #7
    //   71: goto -> 341
    //   74: iload #6
    //   76: bipush #-8
    //   78: iand
    //   79: istore #6
    //   81: iload #6
    //   83: bipush #7
    //   85: iand
    //   86: ifeq -> 116
    //   89: aload_0
    //   90: iload_2
    //   91: invokespecial isWindowScrolling : (I)Z
    //   94: istore #10
    //   96: iload #10
    //   98: ifeq -> 116
    //   101: iload #6
    //   103: bipush #-8
    //   105: iand
    //   106: istore #6
    //   108: goto -> 116
    //   111: astore #7
    //   113: goto -> 341
    //   116: aload_0
    //   117: getfield mInteractionIdCounter : Ljava/util/concurrent/atomic/AtomicInteger;
    //   120: invokevirtual getAndIncrement : ()I
    //   123: istore #11
    //   125: invokestatic clearCallingIdentity : ()J
    //   128: lstore #12
    //   130: aconst_null
    //   131: astore #9
    //   133: invokestatic currentThread : ()Ljava/lang/Thread;
    //   136: invokevirtual getId : ()J
    //   139: lstore #14
    //   141: aload #8
    //   143: iload_2
    //   144: lload_3
    //   145: iload #11
    //   147: aload_0
    //   148: iload #6
    //   150: lload #14
    //   152: aload #7
    //   154: invokeinterface findAccessibilityNodeInfoByAccessibilityId : (IJILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IJLandroid/os/Bundle;)[Ljava/lang/String;
    //   159: astore #7
    //   161: lload #12
    //   163: invokestatic restoreCallingIdentity : (J)V
    //   166: goto -> 231
    //   169: astore #7
    //   171: goto -> 318
    //   174: astore #7
    //   176: goto -> 186
    //   179: astore #7
    //   181: goto -> 318
    //   184: astore #7
    //   186: new java/lang/StringBuilder
    //   189: astore #8
    //   191: aload #8
    //   193: invokespecial <init> : ()V
    //   196: aload #8
    //   198: ldc_w 'Error findAccessibilityNodeInfoByAccessibilityId'
    //   201: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   204: pop
    //   205: aload #8
    //   207: aload #7
    //   209: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   212: pop
    //   213: ldc 'AccessibilityInteractionClient'
    //   215: aload #8
    //   217: invokevirtual toString : ()Ljava/lang/String;
    //   220: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   223: pop
    //   224: aload #9
    //   226: astore #7
    //   228: goto -> 161
    //   231: aload #7
    //   233: ifnull -> 315
    //   236: aload_0
    //   237: iload #11
    //   239: invokespecial getFindAccessibilityNodeInfosResultAndClear : (I)Ljava/util/List;
    //   242: astore #9
    //   244: aload_0
    //   245: aload #9
    //   247: iload_1
    //   248: iload #5
    //   250: aload #7
    //   252: invokespecial finalizeAndCacheAccessibilityNodeInfos : (Ljava/util/List;IZ[Ljava/lang/String;)V
    //   255: aload #9
    //   257: ifnull -> 336
    //   260: aload #9
    //   262: invokeinterface isEmpty : ()Z
    //   267: ifne -> 336
    //   270: iconst_1
    //   271: istore_1
    //   272: iload_1
    //   273: aload #9
    //   275: invokeinterface size : ()I
    //   280: if_icmpge -> 303
    //   283: aload #9
    //   285: iload_1
    //   286: invokeinterface get : (I)Ljava/lang/Object;
    //   291: checkcast android/view/accessibility/AccessibilityNodeInfo
    //   294: invokevirtual recycle : ()V
    //   297: iinc #1, 1
    //   300: goto -> 272
    //   303: aload #9
    //   305: iconst_0
    //   306: invokeinterface get : (I)Ljava/lang/Object;
    //   311: checkcast android/view/accessibility/AccessibilityNodeInfo
    //   314: areturn
    //   315: goto -> 336
    //   318: lload #12
    //   320: invokestatic restoreCallingIdentity : (J)V
    //   323: aload #7
    //   325: athrow
    //   326: astore #7
    //   328: goto -> 341
    //   331: astore #7
    //   333: goto -> 341
    //   336: goto -> 352
    //   339: astore #7
    //   341: ldc 'AccessibilityInteractionClient'
    //   343: ldc_w 'Error while calling remote findAccessibilityNodeInfoByAccessibilityId'
    //   346: aload #7
    //   348: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   351: pop
    //   352: aconst_null
    //   353: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #412	-> 0
    //   #414	-> 17
    //   #418	-> 28
    //   #419	-> 34
    //   #420	-> 39
    //   #421	-> 44
    //   #423	-> 58
    //   #428	-> 63
    //   #434	-> 66
    //   #485	-> 69
    //   #436	-> 74
    //   #439	-> 81
    //   #440	-> 89
    //   #441	-> 101
    //   #485	-> 111
    //   #443	-> 116
    //   #444	-> 125
    //   #453	-> 130
    //   #455	-> 133
    //   #457	-> 133
    //   #455	-> 141
    //   #462	-> 161
    //   #463	-> 166
    //   #462	-> 169
    //   #458	-> 174
    //   #462	-> 179
    //   #458	-> 184
    //   #459	-> 186
    //   #466	-> 231
    //   #468	-> 231
    //   #469	-> 236
    //   #471	-> 244
    //   #473	-> 255
    //   #474	-> 270
    //   #475	-> 283
    //   #474	-> 297
    //   #477	-> 303
    //   #468	-> 315
    //   #462	-> 318
    //   #463	-> 323
    //   #485	-> 326
    //   #419	-> 336
    //   #488	-> 336
    //   #485	-> 339
    //   #486	-> 341
    //   #489	-> 352
    // Exception table:
    //   from	to	target	type
    //   28	34	339	android/os/RemoteException
    //   44	49	69	android/os/RemoteException
    //   49	58	339	android/os/RemoteException
    //   89	96	111	android/os/RemoteException
    //   116	125	331	android/os/RemoteException
    //   125	130	331	android/os/RemoteException
    //   133	141	184	java/lang/NullPointerException
    //   133	141	179	finally
    //   141	161	174	java/lang/NullPointerException
    //   141	161	169	finally
    //   161	166	331	android/os/RemoteException
    //   186	224	169	finally
    //   236	244	331	android/os/RemoteException
    //   244	255	326	android/os/RemoteException
    //   260	270	326	android/os/RemoteException
    //   272	283	326	android/os/RemoteException
    //   283	297	326	android/os/RemoteException
    //   303	315	326	android/os/RemoteException
    //   318	323	326	android/os/RemoteException
    //   323	326	326	android/os/RemoteException
  }
  
  private static String idToString(int paramInt, long paramLong) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder.append("/");
    stringBuilder.append(AccessibilityNodeInfo.idToString(paramLong));
    return stringBuilder.toString();
  }
  
  public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByViewId(int paramInt1, int paramInt2, long paramLong, String paramString) {
    try {
      IAccessibilityServiceConnection iAccessibilityServiceConnection = getConnection(paramInt1);
      if (iAccessibilityServiceConnection != null) {
        int i = this.mInteractionIdCounter.getAndIncrement();
        long l = Binder.clearCallingIdentity();
        try {
          long l1 = Thread.currentThread().getId();
          String[] arrayOfString = iAccessibilityServiceConnection.findAccessibilityNodeInfosByViewId(paramInt2, paramLong, paramString, i, this, l1);
          Binder.restoreCallingIdentity(l);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } else {
        return Collections.emptyList();
      } 
    } catch (RemoteException remoteException) {}
    Log.w("AccessibilityInteractionClient", "Error while calling remote findAccessibilityNodeInfoByViewIdInActiveWindow", (Throwable)remoteException);
    return Collections.emptyList();
  }
  
  public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(int paramInt1, int paramInt2, long paramLong, String paramString) {
    try {
      IAccessibilityServiceConnection iAccessibilityServiceConnection = getConnection(paramInt1);
      if (iAccessibilityServiceConnection != null) {
        int i = this.mInteractionIdCounter.getAndIncrement();
        long l = Binder.clearCallingIdentity();
        try {
          long l1 = Thread.currentThread().getId();
          String[] arrayOfString = iAccessibilityServiceConnection.findAccessibilityNodeInfosByText(paramInt2, paramLong, paramString, i, this, l1);
          Binder.restoreCallingIdentity(l);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } else {
        return Collections.emptyList();
      } 
    } catch (RemoteException remoteException) {}
    Log.w("AccessibilityInteractionClient", "Error while calling remote findAccessibilityNodeInfosByViewText", (Throwable)remoteException);
    return Collections.emptyList();
  }
  
  public AccessibilityNodeInfo findFocus(int paramInt1, int paramInt2, long paramLong, int paramInt3) {
    try {
      IAccessibilityServiceConnection iAccessibilityServiceConnection = getConnection(paramInt1);
      if (iAccessibilityServiceConnection != null) {
        int i = this.mInteractionIdCounter.getAndIncrement();
        long l = Binder.clearCallingIdentity();
        try {
          long l1 = Thread.currentThread().getId();
          String[] arrayOfString = iAccessibilityServiceConnection.findFocus(paramInt2, paramLong, paramInt3, i, this, l1);
          Binder.restoreCallingIdentity(l);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } else {
        return null;
      } 
    } catch (RemoteException remoteException) {}
    Log.w("AccessibilityInteractionClient", "Error while calling remote findFocus", (Throwable)remoteException);
    return null;
  }
  
  public AccessibilityNodeInfo focusSearch(int paramInt1, int paramInt2, long paramLong, int paramInt3) {
    try {
      IAccessibilityServiceConnection iAccessibilityServiceConnection = getConnection(paramInt1);
      if (iAccessibilityServiceConnection != null) {
        int i = this.mInteractionIdCounter.getAndIncrement();
        long l = Binder.clearCallingIdentity();
        try {
          long l1 = Thread.currentThread().getId();
          String[] arrayOfString = iAccessibilityServiceConnection.focusSearch(paramInt2, paramLong, paramInt3, i, this, l1);
          Binder.restoreCallingIdentity(l);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } else {
        return null;
      } 
    } catch (RemoteException remoteException) {}
    Log.w("AccessibilityInteractionClient", "Error while calling remote accessibilityFocusSearch", (Throwable)remoteException);
    return null;
  }
  
  public boolean performAccessibilityAction(int paramInt1, int paramInt2, long paramLong, int paramInt3, Bundle paramBundle) {
    try {
      IAccessibilityServiceConnection iAccessibilityServiceConnection = getConnection(paramInt1);
      if (iAccessibilityServiceConnection != null) {
        paramInt1 = this.mInteractionIdCounter.getAndIncrement();
        long l = Binder.clearCallingIdentity();
        try {
          long l1 = Thread.currentThread().getId();
          boolean bool = iAccessibilityServiceConnection.performAccessibilityAction(paramInt2, paramLong, paramInt3, paramBundle, paramInt1, this, l1);
          Binder.restoreCallingIdentity(l);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
    } catch (RemoteException remoteException) {
      Log.w("AccessibilityInteractionClient", "Error while calling remote performAccessibilityAction", (Throwable)remoteException);
    } 
    return false;
  }
  
  public void clearCache() {
    sAccessibilityCache.clear();
  }
  
  public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    int i = paramAccessibilityEvent.getEventType();
    if (i != 4096) {
      if (i == 4194304)
        if (paramAccessibilityEvent.getWindowChanges() == 2)
          deleteScrollingWindow(paramAccessibilityEvent.getWindowId());  
    } else {
      updateScrollingWindow(paramAccessibilityEvent.getWindowId(), SystemClock.uptimeMillis());
    } 
    sAccessibilityCache.onAccessibilityEvent(paramAccessibilityEvent);
  }
  
  private AccessibilityNodeInfo getFindAccessibilityNodeInfoResultAndClear(int paramInt) {
    synchronized (this.mInstanceLock) {
      AccessibilityNodeInfo accessibilityNodeInfo;
      boolean bool = waitForResultTimedLocked(paramInt);
      if (bool) {
        accessibilityNodeInfo = this.mFindAccessibilityNodeInfoResult;
      } else {
        accessibilityNodeInfo = null;
      } 
      clearResultLocked();
      return accessibilityNodeInfo;
    } 
  }
  
  public void setFindAccessibilityNodeInfoResult(AccessibilityNodeInfo paramAccessibilityNodeInfo, int paramInt) {
    synchronized (this.mInstanceLock) {
      if (paramInt > this.mInteractionId) {
        this.mFindAccessibilityNodeInfoResult = paramAccessibilityNodeInfo;
        this.mInteractionId = paramInt;
      } 
      this.mInstanceLock.notifyAll();
      return;
    } 
  }
  
  private List<AccessibilityNodeInfo> getFindAccessibilityNodeInfosResultAndClear(int paramInt) {
    synchronized (this.mInstanceLock) {
      List<?> list;
      boolean bool = waitForResultTimedLocked(paramInt);
      if (bool) {
        list = this.mFindAccessibilityNodeInfosResult;
      } else {
        list = Collections.emptyList();
      } 
      clearResultLocked();
      if (Build.IS_DEBUGGABLE)
        checkFindAccessibilityNodeInfoResultIntegrity((List)list); 
      return (List)list;
    } 
  }
  
  public void setFindAccessibilityNodeInfosResult(List<AccessibilityNodeInfo> paramList, int paramInt) {
    synchronized (this.mInstanceLock) {
      if (paramInt > this.mInteractionId) {
        if (paramList != null) {
          boolean bool;
          if (Binder.getCallingPid() != Process.myPid()) {
            bool = true;
          } else {
            bool = false;
          } 
          if (!bool) {
            ArrayList<AccessibilityNodeInfo> arrayList = new ArrayList();
            this((Collection)paramList);
            this.mFindAccessibilityNodeInfosResult = arrayList;
          } else {
            this.mFindAccessibilityNodeInfosResult = paramList;
          } 
        } else {
          this.mFindAccessibilityNodeInfosResult = Collections.emptyList();
        } 
        this.mInteractionId = paramInt;
      } 
      this.mInstanceLock.notifyAll();
      return;
    } 
  }
  
  private boolean getPerformAccessibilityActionResultAndClear(int paramInt) {
    synchronized (this.mInstanceLock) {
      boolean bool = waitForResultTimedLocked(paramInt);
      if (bool) {
        bool = this.mPerformAccessibilityActionResult;
      } else {
        bool = false;
      } 
      clearResultLocked();
      return bool;
    } 
  }
  
  public void setPerformAccessibilityActionResult(boolean paramBoolean, int paramInt) {
    synchronized (this.mInstanceLock) {
      if (paramInt > this.mInteractionId) {
        this.mPerformAccessibilityActionResult = paramBoolean;
        this.mInteractionId = paramInt;
      } 
      this.mInstanceLock.notifyAll();
      return;
    } 
  }
  
  private void clearResultLocked() {
    this.mInteractionId = -1;
    this.mFindAccessibilityNodeInfoResult = null;
    this.mFindAccessibilityNodeInfosResult = null;
    this.mPerformAccessibilityActionResult = false;
  }
  
  private boolean waitForResultTimedLocked(int paramInt) {
    long l = SystemClock.uptimeMillis();
    while (true) {
      try {
        Message message = getSameProcessMessageAndClear();
        if (message != null)
          message.getTarget().handleMessage(message); 
        if (this.mInteractionId == paramInt)
          return true; 
        if (this.mInteractionId > paramInt)
          return false; 
        long l1 = SystemClock.uptimeMillis();
        l1 = 5000L - l1 - l;
        if (l1 <= 0L)
          return false; 
        this.mInstanceLock.wait(l1);
      } catch (InterruptedException interruptedException) {}
    } 
  }
  
  private void finalizeAndCacheAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo, int paramInt, boolean paramBoolean, String[] paramArrayOfString) {
    if (paramAccessibilityNodeInfo != null) {
      paramAccessibilityNodeInfo.setConnectionId(paramInt);
      if (!ArrayUtils.isEmpty((Object[])paramArrayOfString)) {
        CharSequence charSequence = paramAccessibilityNodeInfo.getPackageName();
        if (charSequence == null || 
          !ArrayUtils.contains((Object[])paramArrayOfString, charSequence.toString()))
          paramAccessibilityNodeInfo.setPackageName(paramArrayOfString[0]); 
      } 
      paramAccessibilityNodeInfo.setSealed(true);
      if (!paramBoolean)
        sAccessibilityCache.add(paramAccessibilityNodeInfo); 
    } 
  }
  
  private void finalizeAndCacheAccessibilityNodeInfos(List<AccessibilityNodeInfo> paramList, int paramInt, boolean paramBoolean, String[] paramArrayOfString) {
    if (paramList != null) {
      int i = paramList.size();
      for (byte b = 0; b < i; b++) {
        AccessibilityNodeInfo accessibilityNodeInfo = paramList.get(b);
        finalizeAndCacheAccessibilityNodeInfo(accessibilityNodeInfo, paramInt, paramBoolean, paramArrayOfString);
      } 
    } 
  }
  
  private Message getSameProcessMessageAndClear() {
    synchronized (this.mInstanceLock) {
      Message message = this.mSameThreadMessage;
      this.mSameThreadMessage = null;
      return message;
    } 
  }
  
  private void checkFindAccessibilityNodeInfoResultIntegrity(List<AccessibilityNodeInfo> paramList) {
    if (paramList.size() == 0)
      return; 
    AccessibilityNodeInfo accessibilityNodeInfo = paramList.get(0);
    int i = paramList.size();
    int j;
    for (j = 1; j < i; j++, accessibilityNodeInfo = accessibilityNodeInfo1) {
      AccessibilityNodeInfo accessibilityNodeInfo1;
      int k = j;
      while (true) {
        accessibilityNodeInfo1 = accessibilityNodeInfo;
        if (k < i) {
          accessibilityNodeInfo1 = paramList.get(k);
          if (accessibilityNodeInfo.getParentNodeId() == accessibilityNodeInfo1.getSourceNodeId())
            break; 
          k++;
          continue;
        } 
        break;
      } 
    } 
    if (accessibilityNodeInfo == null)
      Log.e("AccessibilityInteractionClient", "No root."); 
    HashSet<AccessibilityNodeInfo> hashSet = new HashSet();
    LinkedList<AccessibilityNodeInfo> linkedList = new LinkedList();
    linkedList.add(accessibilityNodeInfo);
    while (!linkedList.isEmpty()) {
      AccessibilityNodeInfo accessibilityNodeInfo1 = linkedList.poll();
      if (!hashSet.add(accessibilityNodeInfo1)) {
        Log.e("AccessibilityInteractionClient", "Duplicate node.");
        return;
      } 
      int k = accessibilityNodeInfo1.getChildCount();
      for (j = 0; j < k; j++) {
        long l = accessibilityNodeInfo1.getChildId(j);
        for (byte b = 0; b < i; b++) {
          accessibilityNodeInfo = paramList.get(b);
          if (accessibilityNodeInfo.getSourceNodeId() == l)
            linkedList.add(accessibilityNodeInfo); 
        } 
      } 
    } 
    j = paramList.size() - hashSet.size();
    if (j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(j);
      stringBuilder.append(" Disconnected nodes.");
      Log.e("AccessibilityInteractionClient", stringBuilder.toString());
    } 
  }
  
  private void updateScrollingWindow(int paramInt, long paramLong) {
    synchronized (sScrollingWindows) {
      sScrollingWindows.put(paramInt, paramLong);
      return;
    } 
  }
  
  private void deleteScrollingWindow(int paramInt) {
    synchronized (sScrollingWindows) {
      sScrollingWindows.delete(paramInt);
      return;
    } 
  }
  
  private boolean isWindowScrolling(int paramInt) {
    synchronized (sScrollingWindows) {
      long l1 = sScrollingWindows.get(paramInt);
      if (l1 == 0L)
        return false; 
      long l2 = SystemClock.uptimeMillis();
      if (l2 > DISABLE_PREFETCHING_FOR_SCROLLING_MILLIS + l1) {
        sScrollingWindows.delete(paramInt);
        return false;
      } 
      return true;
    } 
  }
}
