package android.view.accessibility;

import android.graphics.Region;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.MagnificationSpec;

public interface IAccessibilityInteractionConnection extends IInterface {
  void clearAccessibilityFocus() throws RemoteException;
  
  void findAccessibilityNodeInfoByAccessibilityId(long paramLong1, Region paramRegion, int paramInt1, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt2, int paramInt3, long paramLong2, MagnificationSpec paramMagnificationSpec, Bundle paramBundle) throws RemoteException;
  
  void findAccessibilityNodeInfosByText(long paramLong1, String paramString, Region paramRegion, int paramInt1, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt2, int paramInt3, long paramLong2, MagnificationSpec paramMagnificationSpec) throws RemoteException;
  
  void findAccessibilityNodeInfosByViewId(long paramLong1, String paramString, Region paramRegion, int paramInt1, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt2, int paramInt3, long paramLong2, MagnificationSpec paramMagnificationSpec) throws RemoteException;
  
  void findFocus(long paramLong1, int paramInt1, Region paramRegion, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2, MagnificationSpec paramMagnificationSpec) throws RemoteException;
  
  void focusSearch(long paramLong1, int paramInt1, Region paramRegion, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2, MagnificationSpec paramMagnificationSpec) throws RemoteException;
  
  void notifyOutsideTouch() throws RemoteException;
  
  void performAccessibilityAction(long paramLong1, int paramInt1, Bundle paramBundle, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2) throws RemoteException;
  
  class Default implements IAccessibilityInteractionConnection {
    public void findAccessibilityNodeInfoByAccessibilityId(long param1Long1, Region param1Region, int param1Int1, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int2, int param1Int3, long param1Long2, MagnificationSpec param1MagnificationSpec, Bundle param1Bundle) throws RemoteException {}
    
    public void findAccessibilityNodeInfosByViewId(long param1Long1, String param1String, Region param1Region, int param1Int1, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int2, int param1Int3, long param1Long2, MagnificationSpec param1MagnificationSpec) throws RemoteException {}
    
    public void findAccessibilityNodeInfosByText(long param1Long1, String param1String, Region param1Region, int param1Int1, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int2, int param1Int3, long param1Long2, MagnificationSpec param1MagnificationSpec) throws RemoteException {}
    
    public void findFocus(long param1Long1, int param1Int1, Region param1Region, int param1Int2, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int3, int param1Int4, long param1Long2, MagnificationSpec param1MagnificationSpec) throws RemoteException {}
    
    public void focusSearch(long param1Long1, int param1Int1, Region param1Region, int param1Int2, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int3, int param1Int4, long param1Long2, MagnificationSpec param1MagnificationSpec) throws RemoteException {}
    
    public void performAccessibilityAction(long param1Long1, int param1Int1, Bundle param1Bundle, int param1Int2, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int3, int param1Int4, long param1Long2) throws RemoteException {}
    
    public void clearAccessibilityFocus() throws RemoteException {}
    
    public void notifyOutsideTouch() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAccessibilityInteractionConnection {
    private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityInteractionConnection";
    
    static final int TRANSACTION_clearAccessibilityFocus = 7;
    
    static final int TRANSACTION_findAccessibilityNodeInfoByAccessibilityId = 1;
    
    static final int TRANSACTION_findAccessibilityNodeInfosByText = 3;
    
    static final int TRANSACTION_findAccessibilityNodeInfosByViewId = 2;
    
    static final int TRANSACTION_findFocus = 4;
    
    static final int TRANSACTION_focusSearch = 5;
    
    static final int TRANSACTION_notifyOutsideTouch = 8;
    
    static final int TRANSACTION_performAccessibilityAction = 6;
    
    public Stub() {
      attachInterface(this, "android.view.accessibility.IAccessibilityInteractionConnection");
    }
    
    public static IAccessibilityInteractionConnection asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.accessibility.IAccessibilityInteractionConnection");
      if (iInterface != null && iInterface instanceof IAccessibilityInteractionConnection)
        return (IAccessibilityInteractionConnection)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "notifyOutsideTouch";
        case 7:
          return "clearAccessibilityFocus";
        case 6:
          return "performAccessibilityAction";
        case 5:
          return "focusSearch";
        case 4:
          return "findFocus";
        case 3:
          return "findAccessibilityNodeInfosByText";
        case 2:
          return "findAccessibilityNodeInfosByViewId";
        case 1:
          break;
      } 
      return "findAccessibilityNodeInfoByAccessibilityId";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IAccessibilityInteractionConnectionCallback iAccessibilityInteractionConnectionCallback1;
        String str;
        int j;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
            notifyOutsideTouch();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
            clearAccessibilityFocus();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
            l1 = param1Parcel1.readLong();
            i = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            iAccessibilityInteractionConnectionCallback1 = IAccessibilityInteractionConnectionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            j = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            l2 = param1Parcel1.readLong();
            performAccessibilityAction(l1, i, (Bundle)param1Parcel2, param1Int1, iAccessibilityInteractionConnectionCallback1, j, param1Int2, l2);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
            l2 = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Region region = (Region)Region.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            i = param1Parcel1.readInt();
            iAccessibilityInteractionConnectionCallback1 = IAccessibilityInteractionConnectionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            param1Int2 = param1Parcel1.readInt();
            j = param1Parcel1.readInt();
            l1 = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              MagnificationSpec magnificationSpec = (MagnificationSpec)MagnificationSpec.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            focusSearch(l2, param1Int1, (Region)param1Parcel2, i, iAccessibilityInteractionConnectionCallback1, param1Int2, j, l1, (MagnificationSpec)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
            l1 = param1Parcel1.readLong();
            i = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Region region = (Region)Region.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int2 = param1Parcel1.readInt();
            iAccessibilityInteractionConnectionCallback1 = IAccessibilityInteractionConnectionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            param1Int1 = param1Parcel1.readInt();
            j = param1Parcel1.readInt();
            l2 = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              MagnificationSpec magnificationSpec = (MagnificationSpec)MagnificationSpec.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            findFocus(l1, i, (Region)param1Parcel2, param1Int2, iAccessibilityInteractionConnectionCallback1, param1Int1, j, l2, (MagnificationSpec)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
            l2 = param1Parcel1.readLong();
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Region region = (Region)Region.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int2 = param1Parcel1.readInt();
            iAccessibilityInteractionConnectionCallback2 = IAccessibilityInteractionConnectionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            i = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            l1 = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              MagnificationSpec magnificationSpec = (MagnificationSpec)MagnificationSpec.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            findAccessibilityNodeInfosByText(l2, str, (Region)param1Parcel2, param1Int2, iAccessibilityInteractionConnectionCallback2, i, param1Int1, l1, (MagnificationSpec)param1Parcel1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
            l2 = param1Parcel1.readLong();
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Region region = (Region)Region.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int2 = param1Parcel1.readInt();
            iAccessibilityInteractionConnectionCallback2 = IAccessibilityInteractionConnectionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            i = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            l1 = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              MagnificationSpec magnificationSpec = (MagnificationSpec)MagnificationSpec.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            findAccessibilityNodeInfosByViewId(l2, str, (Region)param1Parcel2, param1Int2, iAccessibilityInteractionConnectionCallback2, i, param1Int1, l1, (MagnificationSpec)param1Parcel1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnection");
        long l1 = param1Parcel1.readLong();
        if (param1Parcel1.readInt() != 0) {
          Region region = (Region)Region.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel2 = null;
        } 
        param1Int2 = param1Parcel1.readInt();
        IAccessibilityInteractionConnectionCallback iAccessibilityInteractionConnectionCallback2 = IAccessibilityInteractionConnectionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
        param1Int1 = param1Parcel1.readInt();
        int i = param1Parcel1.readInt();
        long l2 = param1Parcel1.readLong();
        if (param1Parcel1.readInt() != 0) {
          MagnificationSpec magnificationSpec = (MagnificationSpec)MagnificationSpec.CREATOR.createFromParcel(param1Parcel1);
        } else {
          str = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        findAccessibilityNodeInfoByAccessibilityId(l1, (Region)param1Parcel2, param1Int2, iAccessibilityInteractionConnectionCallback2, param1Int1, i, l2, (MagnificationSpec)str, (Bundle)param1Parcel1);
        return true;
      } 
      param1Parcel2.writeString("android.view.accessibility.IAccessibilityInteractionConnection");
      return true;
    }
    
    private static class Proxy implements IAccessibilityInteractionConnection {
      public static IAccessibilityInteractionConnection sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.accessibility.IAccessibilityInteractionConnection";
      }
      
      public void findAccessibilityNodeInfoByAccessibilityId(long param2Long1, Region param2Region, int param2Int1, IAccessibilityInteractionConnectionCallback param2IAccessibilityInteractionConnectionCallback, int param2Int2, int param2Int3, long param2Long2, MagnificationSpec param2MagnificationSpec, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
          parcel.writeLong(param2Long1);
          if (param2Region != null) {
            try {
              parcel.writeInt(1);
              param2Region.writeToParcel(parcel, 0);
            } finally {}
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          if (param2IAccessibilityInteractionConnectionCallback != null) {
            iBinder = param2IAccessibilityInteractionConnectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeLong(param2Long2);
          if (param2MagnificationSpec != null) {
            parcel.writeInt(1);
            param2MagnificationSpec.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnection iAccessibilityInteractionConnection = IAccessibilityInteractionConnection.Stub.getDefaultImpl();
            try {
              iAccessibilityInteractionConnection.findAccessibilityNodeInfoByAccessibilityId(param2Long1, param2Region, param2Int1, param2IAccessibilityInteractionConnectionCallback, param2Int2, param2Int3, param2Long2, param2MagnificationSpec, param2Bundle);
              parcel.recycle();
              return;
            } finally {}
          } else {
            parcel.recycle();
            return;
          } 
        } finally {}
        parcel.recycle();
        throw param2Region;
      }
      
      public void findAccessibilityNodeInfosByViewId(long param2Long1, String param2String, Region param2Region, int param2Int1, IAccessibilityInteractionConnectionCallback param2IAccessibilityInteractionConnectionCallback, int param2Int2, int param2Int3, long param2Long2, MagnificationSpec param2MagnificationSpec) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
          parcel.writeLong(param2Long1);
          parcel.writeString(param2String);
          if (param2Region != null) {
            parcel.writeInt(1);
            param2Region.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          if (param2IAccessibilityInteractionConnectionCallback != null) {
            iBinder = param2IAccessibilityInteractionConnectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeLong(param2Long2);
          if (param2MagnificationSpec != null) {
            parcel.writeInt(1);
            param2MagnificationSpec.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnection.Stub.getDefaultImpl().findAccessibilityNodeInfosByViewId(param2Long1, param2String, param2Region, param2Int1, param2IAccessibilityInteractionConnectionCallback, param2Int2, param2Int3, param2Long2, param2MagnificationSpec);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void findAccessibilityNodeInfosByText(long param2Long1, String param2String, Region param2Region, int param2Int1, IAccessibilityInteractionConnectionCallback param2IAccessibilityInteractionConnectionCallback, int param2Int2, int param2Int3, long param2Long2, MagnificationSpec param2MagnificationSpec) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
          parcel.writeLong(param2Long1);
          parcel.writeString(param2String);
          if (param2Region != null) {
            parcel.writeInt(1);
            param2Region.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          if (param2IAccessibilityInteractionConnectionCallback != null) {
            iBinder = param2IAccessibilityInteractionConnectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeLong(param2Long2);
          if (param2MagnificationSpec != null) {
            parcel.writeInt(1);
            param2MagnificationSpec.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnection.Stub.getDefaultImpl().findAccessibilityNodeInfosByText(param2Long1, param2String, param2Region, param2Int1, param2IAccessibilityInteractionConnectionCallback, param2Int2, param2Int3, param2Long2, param2MagnificationSpec);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void findFocus(long param2Long1, int param2Int1, Region param2Region, int param2Int2, IAccessibilityInteractionConnectionCallback param2IAccessibilityInteractionConnectionCallback, int param2Int3, int param2Int4, long param2Long2, MagnificationSpec param2MagnificationSpec) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
          parcel.writeLong(param2Long1);
          parcel.writeInt(param2Int1);
          if (param2Region != null) {
            parcel.writeInt(1);
            param2Region.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int2);
          if (param2IAccessibilityInteractionConnectionCallback != null) {
            iBinder = param2IAccessibilityInteractionConnectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          parcel.writeLong(param2Long2);
          if (param2MagnificationSpec != null) {
            parcel.writeInt(1);
            param2MagnificationSpec.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnection.Stub.getDefaultImpl().findFocus(param2Long1, param2Int1, param2Region, param2Int2, param2IAccessibilityInteractionConnectionCallback, param2Int3, param2Int4, param2Long2, param2MagnificationSpec);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void focusSearch(long param2Long1, int param2Int1, Region param2Region, int param2Int2, IAccessibilityInteractionConnectionCallback param2IAccessibilityInteractionConnectionCallback, int param2Int3, int param2Int4, long param2Long2, MagnificationSpec param2MagnificationSpec) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
          parcel.writeLong(param2Long1);
          parcel.writeInt(param2Int1);
          if (param2Region != null) {
            parcel.writeInt(1);
            param2Region.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int2);
          if (param2IAccessibilityInteractionConnectionCallback != null) {
            iBinder = param2IAccessibilityInteractionConnectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          parcel.writeLong(param2Long2);
          if (param2MagnificationSpec != null) {
            parcel.writeInt(1);
            param2MagnificationSpec.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnection.Stub.getDefaultImpl().focusSearch(param2Long1, param2Int1, param2Region, param2Int2, param2IAccessibilityInteractionConnectionCallback, param2Int3, param2Int4, param2Long2, param2MagnificationSpec);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void performAccessibilityAction(long param2Long1, int param2Int1, Bundle param2Bundle, int param2Int2, IAccessibilityInteractionConnectionCallback param2IAccessibilityInteractionConnectionCallback, int param2Int3, int param2Int4, long param2Long2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
          try {
            IBinder iBinder;
            parcel.writeLong(param2Long1);
            parcel.writeInt(param2Int1);
            if (param2Bundle != null) {
              parcel.writeInt(1);
              param2Bundle.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            parcel.writeInt(param2Int2);
            if (param2IAccessibilityInteractionConnectionCallback != null) {
              iBinder = param2IAccessibilityInteractionConnectionCallback.asBinder();
            } else {
              iBinder = null;
            } 
            parcel.writeStrongBinder(iBinder);
            parcel.writeInt(param2Int3);
            parcel.writeInt(param2Int4);
            parcel.writeLong(param2Long2);
            boolean bool = this.mRemote.transact(6, parcel, null, 1);
            if (!bool && IAccessibilityInteractionConnection.Stub.getDefaultImpl() != null) {
              IAccessibilityInteractionConnection.Stub.getDefaultImpl().performAccessibilityAction(param2Long1, param2Int1, param2Bundle, param2Int2, param2IAccessibilityInteractionConnectionCallback, param2Int3, param2Int4, param2Long2);
              parcel.recycle();
              return;
            } 
            parcel.recycle();
            return;
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2Bundle;
      }
      
      public void clearAccessibilityFocus() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnection.Stub.getDefaultImpl().clearAccessibilityFocus();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyOutsideTouch() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnection");
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnection.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnection.Stub.getDefaultImpl().notifyOutsideTouch();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAccessibilityInteractionConnection param1IAccessibilityInteractionConnection) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAccessibilityInteractionConnection != null) {
          Proxy.sDefaultImpl = param1IAccessibilityInteractionConnection;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAccessibilityInteractionConnection getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
