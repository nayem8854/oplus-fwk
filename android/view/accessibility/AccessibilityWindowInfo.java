package android.view.accessibility;

import android.graphics.Rect;
import android.graphics.Region;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.LongArray;
import android.util.Pools;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public final class AccessibilityWindowInfo implements Parcelable {
  private static final Pools.SynchronizedPool<AccessibilityWindowInfo> sPool = new Pools.SynchronizedPool<>(10);
  
  private int mDisplayId = -1;
  
  private int mType = -1;
  
  private int mLayer = -1;
  
  private int mId = -1;
  
  private int mParentId = -1;
  
  private Region mRegionInScreen = new Region();
  
  private long mAnchorId = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
  
  private int mConnectionId = -1;
  
  public static final int ACTIVE_WINDOW_ID = 2147483647;
  
  public static final int ANY_WINDOW_ID = -2;
  
  private static final int BOOLEAN_PROPERTY_ACCESSIBILITY_FOCUSED = 4;
  
  private static final int BOOLEAN_PROPERTY_ACTIVE = 1;
  
  private static final int BOOLEAN_PROPERTY_FOCUSED = 2;
  
  private static final int BOOLEAN_PROPERTY_PICTURE_IN_PICTURE = 8;
  
  public static final Parcelable.Creator<AccessibilityWindowInfo> CREATOR;
  
  private static final boolean DEBUG = false;
  
  private static final int MAX_POOL_SIZE = 10;
  
  public static final int PICTURE_IN_PICTURE_ACTION_REPLACER_WINDOW_ID = -3;
  
  public static final int TYPE_ACCESSIBILITY_OVERLAY = 4;
  
  public static final int TYPE_APPLICATION = 1;
  
  public static final int TYPE_INPUT_METHOD = 2;
  
  public static final int TYPE_SPLIT_SCREEN_DIVIDER = 5;
  
  public static final int TYPE_SYSTEM = 3;
  
  public static final int UNDEFINED_CONNECTION_ID = -1;
  
  public static final int UNDEFINED_WINDOW_ID = -1;
  
  private static AtomicInteger sNumInstancesInUse;
  
  private int mBooleanProperties;
  
  private LongArray mChildIds;
  
  private CharSequence mTitle;
  
  public AccessibilityWindowInfo(AccessibilityWindowInfo paramAccessibilityWindowInfo) {
    init(paramAccessibilityWindowInfo);
  }
  
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mTitle = paramCharSequence;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public void setType(int paramInt) {
    this.mType = paramInt;
  }
  
  public int getLayer() {
    return this.mLayer;
  }
  
  public void setLayer(int paramInt) {
    this.mLayer = paramInt;
  }
  
  public AccessibilityNodeInfo getRoot() {
    if (this.mConnectionId == -1)
      return null; 
    AccessibilityInteractionClient accessibilityInteractionClient = AccessibilityInteractionClient.getInstance();
    return accessibilityInteractionClient.findAccessibilityNodeInfoByAccessibilityId(this.mConnectionId, this.mId, AccessibilityNodeInfo.ROOT_NODE_ID, true, 4, (Bundle)null);
  }
  
  public void setAnchorId(long paramLong) {
    this.mAnchorId = paramLong;
  }
  
  public AccessibilityNodeInfo getAnchor() {
    if (this.mConnectionId == -1 || this.mAnchorId == AccessibilityNodeInfo.UNDEFINED_NODE_ID || this.mParentId == -1)
      return null; 
    AccessibilityInteractionClient accessibilityInteractionClient = AccessibilityInteractionClient.getInstance();
    return accessibilityInteractionClient.findAccessibilityNodeInfoByAccessibilityId(this.mConnectionId, this.mParentId, this.mAnchorId, true, 0, (Bundle)null);
  }
  
  public void setPictureInPicture(boolean paramBoolean) {
    setBooleanProperty(8, paramBoolean);
  }
  
  public boolean isInPictureInPictureMode() {
    return getBooleanProperty(8);
  }
  
  public AccessibilityWindowInfo getParent() {
    if (this.mConnectionId == -1 || this.mParentId == -1)
      return null; 
    AccessibilityInteractionClient accessibilityInteractionClient = AccessibilityInteractionClient.getInstance();
    return accessibilityInteractionClient.getWindow(this.mConnectionId, this.mParentId);
  }
  
  public void setParentId(int paramInt) {
    this.mParentId = paramInt;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public void setId(int paramInt) {
    this.mId = paramInt;
  }
  
  public void setConnectionId(int paramInt) {
    this.mConnectionId = paramInt;
  }
  
  public void getRegionInScreen(Region paramRegion) {
    paramRegion.set(this.mRegionInScreen);
  }
  
  public void setRegionInScreen(Region paramRegion) {
    this.mRegionInScreen.set(paramRegion);
  }
  
  public void getBoundsInScreen(Rect paramRect) {
    paramRect.set(this.mRegionInScreen.getBounds());
  }
  
  public boolean isActive() {
    return getBooleanProperty(1);
  }
  
  public void setActive(boolean paramBoolean) {
    setBooleanProperty(1, paramBoolean);
  }
  
  public boolean isFocused() {
    return getBooleanProperty(2);
  }
  
  public void setFocused(boolean paramBoolean) {
    setBooleanProperty(2, paramBoolean);
  }
  
  public boolean isAccessibilityFocused() {
    return getBooleanProperty(4);
  }
  
  public void setAccessibilityFocused(boolean paramBoolean) {
    setBooleanProperty(4, paramBoolean);
  }
  
  public int getChildCount() {
    boolean bool;
    LongArray longArray = this.mChildIds;
    if (longArray != null) {
      bool = longArray.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public AccessibilityWindowInfo getChild(int paramInt) {
    LongArray longArray = this.mChildIds;
    if (longArray != null) {
      if (this.mConnectionId == -1)
        return null; 
      paramInt = (int)longArray.get(paramInt);
      AccessibilityInteractionClient accessibilityInteractionClient = AccessibilityInteractionClient.getInstance();
      return accessibilityInteractionClient.getWindow(this.mConnectionId, paramInt);
    } 
    throw new IndexOutOfBoundsException();
  }
  
  public void addChild(int paramInt) {
    if (this.mChildIds == null)
      this.mChildIds = new LongArray(); 
    this.mChildIds.add(paramInt);
  }
  
  public void setDisplayId(int paramInt) {
    this.mDisplayId = paramInt;
  }
  
  public int getDisplayId() {
    return this.mDisplayId;
  }
  
  public static AccessibilityWindowInfo obtain() {
    AccessibilityWindowInfo accessibilityWindowInfo1 = sPool.acquire();
    AccessibilityWindowInfo accessibilityWindowInfo2 = accessibilityWindowInfo1;
    if (accessibilityWindowInfo1 == null)
      accessibilityWindowInfo2 = new AccessibilityWindowInfo(); 
    AtomicInteger atomicInteger = sNumInstancesInUse;
    if (atomicInteger != null)
      atomicInteger.incrementAndGet(); 
    return accessibilityWindowInfo2;
  }
  
  public static AccessibilityWindowInfo obtain(AccessibilityWindowInfo paramAccessibilityWindowInfo) {
    AccessibilityWindowInfo accessibilityWindowInfo = obtain();
    accessibilityWindowInfo.init(paramAccessibilityWindowInfo);
    return accessibilityWindowInfo;
  }
  
  public static void setNumInstancesInUseCounter(AtomicInteger paramAtomicInteger) {
    if (sNumInstancesInUse != null)
      sNumInstancesInUse = paramAtomicInteger; 
  }
  
  public void recycle() {
    clear();
    sPool.release(this);
    AtomicInteger atomicInteger = sNumInstancesInUse;
    if (atomicInteger != null)
      atomicInteger.decrementAndGet(); 
  }
  
  public boolean refresh() {
    if (this.mConnectionId == -1 || this.mId == -1)
      return false; 
    AccessibilityInteractionClient accessibilityInteractionClient = AccessibilityInteractionClient.getInstance();
    AccessibilityWindowInfo accessibilityWindowInfo = accessibilityInteractionClient.getWindow(this.mConnectionId, this.mId, true);
    if (accessibilityWindowInfo == null)
      return false; 
    init(accessibilityWindowInfo);
    accessibilityWindowInfo.recycle();
    return true;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mDisplayId);
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mLayer);
    paramParcel.writeInt(this.mBooleanProperties);
    paramParcel.writeInt(this.mId);
    paramParcel.writeInt(this.mParentId);
    this.mRegionInScreen.writeToParcel(paramParcel, paramInt);
    paramParcel.writeCharSequence(this.mTitle);
    paramParcel.writeLong(this.mAnchorId);
    LongArray longArray = this.mChildIds;
    if (longArray == null) {
      paramParcel.writeInt(0);
    } else {
      int i = longArray.size();
      paramParcel.writeInt(i);
      for (paramInt = 0; paramInt < i; paramInt++)
        paramParcel.writeInt((int)longArray.get(paramInt)); 
    } 
    paramParcel.writeInt(this.mConnectionId);
  }
  
  private void init(AccessibilityWindowInfo paramAccessibilityWindowInfo) {
    this.mDisplayId = paramAccessibilityWindowInfo.mDisplayId;
    this.mType = paramAccessibilityWindowInfo.mType;
    this.mLayer = paramAccessibilityWindowInfo.mLayer;
    this.mBooleanProperties = paramAccessibilityWindowInfo.mBooleanProperties;
    this.mId = paramAccessibilityWindowInfo.mId;
    this.mParentId = paramAccessibilityWindowInfo.mParentId;
    this.mRegionInScreen.set(paramAccessibilityWindowInfo.mRegionInScreen);
    this.mTitle = paramAccessibilityWindowInfo.mTitle;
    this.mAnchorId = paramAccessibilityWindowInfo.mAnchorId;
    LongArray longArray = this.mChildIds;
    if (longArray != null)
      longArray.clear(); 
    longArray = paramAccessibilityWindowInfo.mChildIds;
    if (longArray != null && longArray.size() > 0) {
      longArray = this.mChildIds;
      if (longArray == null) {
        this.mChildIds = paramAccessibilityWindowInfo.mChildIds.clone();
      } else {
        longArray.addAll(paramAccessibilityWindowInfo.mChildIds);
      } 
    } 
    this.mConnectionId = paramAccessibilityWindowInfo.mConnectionId;
  }
  
  private void initFromParcel(Parcel paramParcel) {
    this.mDisplayId = paramParcel.readInt();
    this.mType = paramParcel.readInt();
    this.mLayer = paramParcel.readInt();
    this.mBooleanProperties = paramParcel.readInt();
    this.mId = paramParcel.readInt();
    this.mParentId = paramParcel.readInt();
    this.mRegionInScreen = (Region)Region.CREATOR.createFromParcel(paramParcel);
    this.mTitle = paramParcel.readCharSequence();
    this.mAnchorId = paramParcel.readLong();
    int i = paramParcel.readInt();
    if (i > 0) {
      if (this.mChildIds == null)
        this.mChildIds = new LongArray(i); 
      for (byte b = 0; b < i; b++) {
        int j = paramParcel.readInt();
        this.mChildIds.add(j);
      } 
    } 
    this.mConnectionId = paramParcel.readInt();
  }
  
  public int hashCode() {
    return this.mId;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mId != ((AccessibilityWindowInfo)paramObject).mId)
      bool = false; 
    return bool;
  }
  
  public String toString() {
    boolean bool2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AccessibilityWindowInfo[");
    stringBuilder.append("title=");
    stringBuilder.append(this.mTitle);
    stringBuilder.append(", displayId=");
    stringBuilder.append(this.mDisplayId);
    stringBuilder.append(", id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(", type=");
    stringBuilder.append(typeToString(this.mType));
    stringBuilder.append(", layer=");
    stringBuilder.append(this.mLayer);
    stringBuilder.append(", region=");
    stringBuilder.append(this.mRegionInScreen);
    stringBuilder.append(", bounds=");
    stringBuilder.append(this.mRegionInScreen.getBounds());
    stringBuilder.append(", focused=");
    stringBuilder.append(isFocused());
    stringBuilder.append(", active=");
    stringBuilder.append(isActive());
    stringBuilder.append(", pictureInPicture=");
    stringBuilder.append(isInPictureInPictureMode());
    stringBuilder.append(", hasParent=");
    int i = this.mParentId;
    boolean bool1 = true;
    if (i != -1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    stringBuilder.append(bool2);
    stringBuilder.append(", isAnchored=");
    if (this.mAnchorId != AccessibilityNodeInfo.UNDEFINED_NODE_ID) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    stringBuilder.append(bool2);
    stringBuilder.append(", hasChildren=");
    LongArray longArray = this.mChildIds;
    if (longArray != null && 
      longArray.size() > 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    stringBuilder.append(bool2);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  private void clear() {
    this.mDisplayId = -1;
    this.mType = -1;
    this.mLayer = -1;
    this.mBooleanProperties = 0;
    this.mId = -1;
    this.mParentId = -1;
    this.mRegionInScreen.setEmpty();
    this.mChildIds = null;
    this.mConnectionId = -1;
    this.mAnchorId = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
    this.mTitle = null;
  }
  
  private boolean getBooleanProperty(int paramInt) {
    boolean bool;
    if ((this.mBooleanProperties & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setBooleanProperty(int paramInt, boolean paramBoolean) {
    if (paramBoolean) {
      this.mBooleanProperties |= paramInt;
    } else {
      this.mBooleanProperties &= paramInt ^ 0xFFFFFFFF;
    } 
  }
  
  public static String typeToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4) {
            if (paramInt != 5) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("<UNKNOWN:");
              stringBuilder.append(paramInt);
              stringBuilder.append(">");
              return stringBuilder.toString();
            } 
            return "TYPE_SPLIT_SCREEN_DIVIDER";
          } 
          return "TYPE_ACCESSIBILITY_OVERLAY";
        } 
        return "TYPE_SYSTEM";
      } 
      return "TYPE_INPUT_METHOD";
    } 
    return "TYPE_APPLICATION";
  }
  
  public int differenceFrom(AccessibilityWindowInfo paramAccessibilityWindowInfo) {
    if (paramAccessibilityWindowInfo.mId == this.mId) {
      if (paramAccessibilityWindowInfo.mType == this.mType) {
        int i = 0;
        if (!TextUtils.equals(this.mTitle, paramAccessibilityWindowInfo.mTitle))
          i = 0x0 | 0x4; 
        int j = i;
        if (!this.mRegionInScreen.equals(paramAccessibilityWindowInfo.mRegionInScreen))
          j = i | 0x8; 
        i = j;
        if (this.mLayer != paramAccessibilityWindowInfo.mLayer)
          i = j | 0x10; 
        boolean bool = getBooleanProperty(1);
        j = i;
        if (bool != paramAccessibilityWindowInfo.getBooleanProperty(1))
          j = i | 0x20; 
        bool = getBooleanProperty(2);
        i = j;
        if (bool != paramAccessibilityWindowInfo.getBooleanProperty(2))
          i = j | 0x40; 
        bool = getBooleanProperty(4);
        j = i;
        if (bool != paramAccessibilityWindowInfo.getBooleanProperty(4))
          j = i | 0x80; 
        bool = getBooleanProperty(8);
        i = j;
        if (bool != paramAccessibilityWindowInfo.getBooleanProperty(8))
          i = j | 0x400; 
        j = i;
        if (this.mParentId != paramAccessibilityWindowInfo.mParentId)
          j = i | 0x100; 
        i = j;
        if (!Objects.equals(this.mChildIds, paramAccessibilityWindowInfo.mChildIds))
          i = j | 0x200; 
        return i;
      } 
      throw new IllegalArgumentException("Not same type.");
    } 
    throw new IllegalArgumentException("Not same window.");
  }
  
  static {
    CREATOR = new Parcelable.Creator<AccessibilityWindowInfo>() {
        public AccessibilityWindowInfo createFromParcel(Parcel param1Parcel) {
          AccessibilityWindowInfo accessibilityWindowInfo = AccessibilityWindowInfo.obtain();
          accessibilityWindowInfo.initFromParcel(param1Parcel);
          return accessibilityWindowInfo;
        }
        
        public AccessibilityWindowInfo[] newArray(int param1Int) {
          return new AccessibilityWindowInfo[param1Int];
        }
      };
  }
  
  public AccessibilityWindowInfo() {}
  
  public static final class WindowListSparseArray extends SparseArray<List<AccessibilityWindowInfo>> implements Parcelable {
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      int i = size();
      param1Parcel.writeInt(i);
      for (param1Int = 0; param1Int < i; param1Int++) {
        param1Parcel.writeParcelableList(valueAt(param1Int), 0);
        param1Parcel.writeInt(keyAt(param1Int));
      } 
    }
    
    public static final Parcelable.Creator<WindowListSparseArray> CREATOR = new Parcelable.Creator<WindowListSparseArray>() {
        public AccessibilityWindowInfo.WindowListSparseArray createFromParcel(Parcel param2Parcel) {
          AccessibilityWindowInfo.WindowListSparseArray windowListSparseArray = new AccessibilityWindowInfo.WindowListSparseArray();
          ClassLoader classLoader = windowListSparseArray.getClass().getClassLoader();
          int i = param2Parcel.readInt();
          for (byte b = 0; b < i; b++) {
            ArrayList<AccessibilityWindowInfo> arrayList = new ArrayList();
            param2Parcel.readParcelableList(arrayList, classLoader);
            windowListSparseArray.put(param2Parcel.readInt(), arrayList);
          } 
          return windowListSparseArray;
        }
        
        public AccessibilityWindowInfo.WindowListSparseArray[] newArray(int param2Int) {
          return new AccessibilityWindowInfo.WindowListSparseArray[param2Int];
        }
      };
  }
  
  class null implements Parcelable.Creator<WindowListSparseArray> {
    public AccessibilityWindowInfo.WindowListSparseArray createFromParcel(Parcel param1Parcel) {
      AccessibilityWindowInfo.WindowListSparseArray windowListSparseArray = new AccessibilityWindowInfo.WindowListSparseArray();
      ClassLoader classLoader = windowListSparseArray.getClass().getClassLoader();
      int i = param1Parcel.readInt();
      for (byte b = 0; b < i; b++) {
        ArrayList<AccessibilityWindowInfo> arrayList = new ArrayList();
        param1Parcel.readParcelableList(arrayList, classLoader);
        windowListSparseArray.put(param1Parcel.readInt(), arrayList);
      } 
      return windowListSparseArray;
    }
    
    public AccessibilityWindowInfo.WindowListSparseArray[] newArray(int param1Int) {
      return new AccessibilityWindowInfo.WindowListSparseArray[param1Int];
    }
  }
}
