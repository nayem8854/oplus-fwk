package android.view.accessibility;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;

public abstract class AccessibilityRequestPreparer {
  public static final int REQUEST_TYPE_EXTRA_DATA = 1;
  
  private final int mAccessibilityViewId;
  
  private final int mRequestTypes;
  
  private final WeakReference<View> mViewRef;
  
  public AccessibilityRequestPreparer(View paramView, int paramInt) {
    if (paramView.isAttachedToWindow()) {
      this.mViewRef = new WeakReference<>(paramView);
      this.mAccessibilityViewId = paramView.getAccessibilityViewId();
      this.mRequestTypes = paramInt;
      paramView.addOnAttachStateChangeListener(new ViewAttachStateListener());
      return;
    } 
    throw new IllegalStateException("View must be attached to a window");
  }
  
  public View getView() {
    return this.mViewRef.get();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RequestTypes {}
  
  class ViewAttachStateListener implements View.OnAttachStateChangeListener {
    final AccessibilityRequestPreparer this$0;
    
    private ViewAttachStateListener() {}
    
    public void onViewAttachedToWindow(View param1View) {}
    
    public void onViewDetachedFromWindow(View param1View) {
      Context context = param1View.getContext();
      if (context != null) {
        AccessibilityManager accessibilityManager = (AccessibilityManager)context.getSystemService(AccessibilityManager.class);
        AccessibilityRequestPreparer accessibilityRequestPreparer = AccessibilityRequestPreparer.this;
        accessibilityManager.removeAccessibilityRequestPreparer(accessibilityRequestPreparer);
      } 
      param1View.removeOnAttachStateChangeListener(this);
    }
  }
  
  int getAccessibilityViewId() {
    return this.mAccessibilityViewId;
  }
  
  public abstract void onPrepareExtraData(int paramInt, String paramString, Bundle paramBundle, Message paramMessage);
}
