package android.view.accessibility;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAccessibilityManagerClient extends IInterface {
  void notifyServicesStateChanged(long paramLong) throws RemoteException;
  
  void setRelevantEventTypes(int paramInt) throws RemoteException;
  
  void setState(int paramInt) throws RemoteException;
  
  class Default implements IAccessibilityManagerClient {
    public void setState(int param1Int) throws RemoteException {}
    
    public void notifyServicesStateChanged(long param1Long) throws RemoteException {}
    
    public void setRelevantEventTypes(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAccessibilityManagerClient {
    private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityManagerClient";
    
    static final int TRANSACTION_notifyServicesStateChanged = 2;
    
    static final int TRANSACTION_setRelevantEventTypes = 3;
    
    static final int TRANSACTION_setState = 1;
    
    public Stub() {
      attachInterface(this, "android.view.accessibility.IAccessibilityManagerClient");
    }
    
    public static IAccessibilityManagerClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.accessibility.IAccessibilityManagerClient");
      if (iInterface != null && iInterface instanceof IAccessibilityManagerClient)
        return (IAccessibilityManagerClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "setRelevantEventTypes";
        } 
        return "notifyServicesStateChanged";
      } 
      return "setState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.view.accessibility.IAccessibilityManagerClient");
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityManagerClient");
          param1Int1 = param1Parcel1.readInt();
          setRelevantEventTypes(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityManagerClient");
        long l = param1Parcel1.readLong();
        notifyServicesStateChanged(l);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityManagerClient");
      param1Int1 = param1Parcel1.readInt();
      setState(param1Int1);
      return true;
    }
    
    private static class Proxy implements IAccessibilityManagerClient {
      public static IAccessibilityManagerClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.accessibility.IAccessibilityManagerClient";
      }
      
      public void setState(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManagerClient");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAccessibilityManagerClient.Stub.getDefaultImpl() != null) {
            IAccessibilityManagerClient.Stub.getDefaultImpl().setState(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyServicesStateChanged(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManagerClient");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAccessibilityManagerClient.Stub.getDefaultImpl() != null) {
            IAccessibilityManagerClient.Stub.getDefaultImpl().notifyServicesStateChanged(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRelevantEventTypes(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManagerClient");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IAccessibilityManagerClient.Stub.getDefaultImpl() != null) {
            IAccessibilityManagerClient.Stub.getDefaultImpl().setRelevantEventTypes(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAccessibilityManagerClient param1IAccessibilityManagerClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAccessibilityManagerClient != null) {
          Proxy.sDefaultImpl = param1IAccessibilityManagerClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAccessibilityManagerClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
