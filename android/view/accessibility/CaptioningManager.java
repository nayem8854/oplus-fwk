package android.view.accessibility;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Locale;

public class CaptioningManager {
  private static final int DEFAULT_ENABLED = 0;
  
  private static final float DEFAULT_FONT_SCALE = 1.0F;
  
  private static final int DEFAULT_PRESET = 0;
  
  private final ContentObserver mContentObserver;
  
  private final ContentResolver mContentResolver;
  
  private final ArrayList<CaptioningChangeListener> mListeners = new ArrayList<>();
  
  private final Runnable mStyleChangedRunnable;
  
  public final boolean isEnabled() {
    ContentResolver contentResolver = this.mContentResolver;
    boolean bool = false;
    if (Settings.Secure.getInt(contentResolver, "accessibility_captioning_enabled", 0) == 1)
      bool = true; 
    return bool;
  }
  
  public final String getRawLocale() {
    return Settings.Secure.getString(this.mContentResolver, "accessibility_captioning_locale");
  }
  
  public final Locale getLocale() {
    String str = getRawLocale();
    if (!TextUtils.isEmpty(str)) {
      String[] arrayOfString = str.split("_");
      int i = arrayOfString.length;
      if (i != 1) {
        if (i != 2) {
          if (i == 3)
            return new Locale(arrayOfString[0], arrayOfString[1], arrayOfString[2]); 
        } else {
          return new Locale(arrayOfString[0], arrayOfString[1]);
        } 
      } else {
        return new Locale(arrayOfString[0]);
      } 
    } 
    return null;
  }
  
  public final float getFontScale() {
    return Settings.Secure.getFloat(this.mContentResolver, "accessibility_captioning_font_scale", 1.0F);
  }
  
  public int getRawUserStyle() {
    return Settings.Secure.getInt(this.mContentResolver, "accessibility_captioning_preset", 0);
  }
  
  public CaptionStyle getUserStyle() {
    int i = getRawUserStyle();
    if (i == -1)
      return CaptionStyle.getCustomStyle(this.mContentResolver); 
    return CaptionStyle.PRESETS[i];
  }
  
  public void addCaptioningChangeListener(CaptioningChangeListener paramCaptioningChangeListener) {
    synchronized (this.mListeners) {
      if (this.mListeners.isEmpty()) {
        registerObserver("accessibility_captioning_enabled");
        registerObserver("accessibility_captioning_foreground_color");
        registerObserver("accessibility_captioning_background_color");
        registerObserver("accessibility_captioning_window_color");
        registerObserver("accessibility_captioning_edge_type");
        registerObserver("accessibility_captioning_edge_color");
        registerObserver("accessibility_captioning_typeface");
        registerObserver("accessibility_captioning_font_scale");
        registerObserver("accessibility_captioning_locale");
        registerObserver("accessibility_captioning_preset");
      } 
      this.mListeners.add(paramCaptioningChangeListener);
      return;
    } 
  }
  
  private void registerObserver(String paramString) {
    this.mContentResolver.registerContentObserver(Settings.Secure.getUriFor(paramString), false, this.mContentObserver);
  }
  
  public void removeCaptioningChangeListener(CaptioningChangeListener paramCaptioningChangeListener) {
    synchronized (this.mListeners) {
      this.mListeners.remove(paramCaptioningChangeListener);
      if (this.mListeners.isEmpty())
        this.mContentResolver.unregisterContentObserver(this.mContentObserver); 
      return;
    } 
  }
  
  private void notifyEnabledChanged() {
    boolean bool = isEnabled();
    synchronized (this.mListeners) {
      for (CaptioningChangeListener captioningChangeListener : this.mListeners)
        captioningChangeListener.onEnabledChanged(bool); 
      return;
    } 
  }
  
  private void notifyUserStyleChanged() {
    CaptionStyle captionStyle = getUserStyle();
    synchronized (this.mListeners) {
      for (CaptioningChangeListener captioningChangeListener : this.mListeners)
        captioningChangeListener.onUserStyleChanged(captionStyle); 
      return;
    } 
  }
  
  private void notifyLocaleChanged() {
    Locale locale = getLocale();
    synchronized (this.mListeners) {
      for (CaptioningChangeListener captioningChangeListener : this.mListeners)
        captioningChangeListener.onLocaleChanged(locale); 
      return;
    } 
  }
  
  private void notifyFontScaleChanged() {
    float f = getFontScale();
    synchronized (this.mListeners) {
      for (CaptioningChangeListener captioningChangeListener : this.mListeners)
        captioningChangeListener.onFontScaleChanged(f); 
      return;
    } 
  }
  
  class MyContentObserver extends ContentObserver {
    private final Handler mHandler;
    
    final CaptioningManager this$0;
    
    public MyContentObserver(Handler param1Handler) {
      super(param1Handler);
      this.mHandler = param1Handler;
    }
    
    public void onChange(boolean param1Boolean, Uri param1Uri) {
      String str = param1Uri.getPath();
      str = str.substring(str.lastIndexOf('/') + 1);
      if ("accessibility_captioning_enabled".equals(str)) {
        CaptioningManager.this.notifyEnabledChanged();
      } else if ("accessibility_captioning_locale".equals(str)) {
        CaptioningManager.this.notifyLocaleChanged();
      } else if ("accessibility_captioning_font_scale".equals(str)) {
        CaptioningManager.this.notifyFontScaleChanged();
      } else {
        this.mHandler.removeCallbacks(CaptioningManager.this.mStyleChangedRunnable);
        this.mHandler.post(CaptioningManager.this.mStyleChangedRunnable);
      } 
    }
  }
  
  public CaptioningManager(Context paramContext) {
    this.mStyleChangedRunnable = new Runnable() {
        final CaptioningManager this$0;
        
        public void run() {
          CaptioningManager.this.notifyUserStyleChanged();
        }
      };
    this.mContentResolver = paramContext.getContentResolver();
    Handler handler = new Handler(paramContext.getMainLooper());
    this.mContentObserver = new MyContentObserver(handler);
  }
  
  public static final class CaptionStyle {
    private static final CaptionStyle BLACK_ON_WHITE = new CaptionStyle(-16777216, -1, 0, -16777216, 255, null);
    
    private static final int COLOR_NONE_OPAQUE = 255;
    
    public static final int COLOR_UNSPECIFIED = 16777215;
    
    public static final CaptionStyle DEFAULT;
    
    private static final CaptionStyle DEFAULT_CUSTOM;
    
    public static final int EDGE_TYPE_DEPRESSED = 4;
    
    public static final int EDGE_TYPE_DROP_SHADOW = 2;
    
    public static final int EDGE_TYPE_NONE = 0;
    
    public static final int EDGE_TYPE_OUTLINE = 1;
    
    public static final int EDGE_TYPE_RAISED = 3;
    
    public static final int EDGE_TYPE_UNSPECIFIED = -1;
    
    public static final CaptionStyle[] PRESETS;
    
    public static final int PRESET_CUSTOM = -1;
    
    private static final CaptionStyle UNSPECIFIED;
    
    private CaptionStyle(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, String param1String) {
      this.mHasForegroundColor = hasColor(param1Int1);
      this.mHasBackgroundColor = hasColor(param1Int2);
      boolean bool = false;
      int i = -1;
      if (param1Int3 != -1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      this.mHasEdgeType = bool1;
      this.mHasEdgeColor = hasColor(param1Int4);
      this.mHasWindowColor = hasColor(param1Int5);
      if (this.mHasForegroundColor)
        i = param1Int1; 
      this.foregroundColor = i;
      boolean bool1 = this.mHasBackgroundColor;
      i = -16777216;
      if (!bool1)
        param1Int2 = -16777216; 
      this.backgroundColor = param1Int2;
      param1Int1 = bool;
      if (this.mHasEdgeType)
        param1Int1 = param1Int3; 
      this.edgeType = param1Int1;
      param1Int1 = i;
      if (this.mHasEdgeColor)
        param1Int1 = param1Int4; 
      this.edgeColor = param1Int1;
      if (!this.mHasWindowColor)
        param1Int5 = 255; 
      this.windowColor = param1Int5;
      this.mRawTypeface = param1String;
    }
    
    public static boolean hasColor(int param1Int) {
      return (param1Int >>> 24 != 0 || (0xFFFF00 & param1Int) == 0);
    }
    
    public CaptionStyle applyStyle(CaptionStyle param1CaptionStyle) {
      int i, j, k, m, n;
      if (param1CaptionStyle.hasForegroundColor()) {
        i = param1CaptionStyle.foregroundColor;
      } else {
        i = this.foregroundColor;
      } 
      if (param1CaptionStyle.hasBackgroundColor()) {
        j = param1CaptionStyle.backgroundColor;
      } else {
        j = this.backgroundColor;
      } 
      if (param1CaptionStyle.hasEdgeType()) {
        k = param1CaptionStyle.edgeType;
      } else {
        k = this.edgeType;
      } 
      if (param1CaptionStyle.hasEdgeColor()) {
        m = param1CaptionStyle.edgeColor;
      } else {
        m = this.edgeColor;
      } 
      if (param1CaptionStyle.hasWindowColor()) {
        n = param1CaptionStyle.windowColor;
      } else {
        n = this.windowColor;
      } 
      String str = param1CaptionStyle.mRawTypeface;
      if (str == null)
        str = this.mRawTypeface; 
      return new CaptionStyle(i, j, k, m, n, str);
    }
    
    public boolean hasBackgroundColor() {
      return this.mHasBackgroundColor;
    }
    
    public boolean hasForegroundColor() {
      return this.mHasForegroundColor;
    }
    
    public boolean hasEdgeType() {
      return this.mHasEdgeType;
    }
    
    public boolean hasEdgeColor() {
      return this.mHasEdgeColor;
    }
    
    public boolean hasWindowColor() {
      return this.mHasWindowColor;
    }
    
    public Typeface getTypeface() {
      if (this.mParsedTypeface == null && !TextUtils.isEmpty(this.mRawTypeface))
        this.mParsedTypeface = Typeface.create(this.mRawTypeface, 0); 
      return this.mParsedTypeface;
    }
    
    public static CaptionStyle getCustomStyle(ContentResolver param1ContentResolver) {
      CaptionStyle captionStyle = DEFAULT_CUSTOM;
      int i = Settings.Secure.getInt(param1ContentResolver, "accessibility_captioning_foreground_color", captionStyle.foregroundColor);
      int j = Settings.Secure.getInt(param1ContentResolver, "accessibility_captioning_background_color", captionStyle.backgroundColor);
      int k = Settings.Secure.getInt(param1ContentResolver, "accessibility_captioning_edge_type", captionStyle.edgeType);
      int m = Settings.Secure.getInt(param1ContentResolver, "accessibility_captioning_edge_color", captionStyle.edgeColor);
      int n = Settings.Secure.getInt(param1ContentResolver, "accessibility_captioning_window_color", captionStyle.windowColor);
      String str = Settings.Secure.getString(param1ContentResolver, "accessibility_captioning_typeface");
      if (str == null)
        str = captionStyle.mRawTypeface; 
      return new CaptionStyle(i, j, k, m, n, str);
    }
    
    private static final CaptionStyle WHITE_ON_BLACK = new CaptionStyle(-1, -16777216, 0, -16777216, 255, null);
    
    private static final CaptionStyle YELLOW_ON_BLACK = new CaptionStyle(-256, -16777216, 0, -16777216, 255, null);
    
    private static final CaptionStyle YELLOW_ON_BLUE = new CaptionStyle(-256, -16776961, 0, -16777216, 255, null);
    
    public final int backgroundColor;
    
    public final int edgeColor;
    
    public final int edgeType;
    
    public final int foregroundColor;
    
    private final boolean mHasBackgroundColor;
    
    private final boolean mHasEdgeColor;
    
    private final boolean mHasEdgeType;
    
    private final boolean mHasForegroundColor;
    
    private final boolean mHasWindowColor;
    
    private Typeface mParsedTypeface;
    
    public final String mRawTypeface;
    
    public final int windowColor;
    
    static {
      CaptionStyle captionStyle1 = new CaptionStyle(16777215, 16777215, -1, 16777215, 16777215, null);
      CaptionStyle captionStyle2 = WHITE_ON_BLACK;
      PRESETS = new CaptionStyle[] { captionStyle2, BLACK_ON_WHITE, YELLOW_ON_BLACK, YELLOW_ON_BLUE, captionStyle1 };
      DEFAULT_CUSTOM = captionStyle2;
      DEFAULT = captionStyle2;
    }
  }
  
  public static abstract class CaptioningChangeListener {
    public void onEnabledChanged(boolean param1Boolean) {}
    
    public void onUserStyleChanged(CaptioningManager.CaptionStyle param1CaptionStyle) {}
    
    public void onLocaleChanged(Locale param1Locale) {}
    
    public void onFontScaleChanged(float param1Float) {}
  }
}
