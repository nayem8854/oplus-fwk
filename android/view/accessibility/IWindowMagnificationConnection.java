package android.view.accessibility;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWindowMagnificationConnection extends IInterface {
  void disableWindowMagnification(int paramInt) throws RemoteException;
  
  void enableWindowMagnification(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3) throws RemoteException;
  
  void moveWindowMagnifier(int paramInt, float paramFloat1, float paramFloat2) throws RemoteException;
  
  void setConnectionCallback(IWindowMagnificationConnectionCallback paramIWindowMagnificationConnectionCallback) throws RemoteException;
  
  void setScale(int paramInt, float paramFloat) throws RemoteException;
  
  class Default implements IWindowMagnificationConnection {
    public void enableWindowMagnification(int param1Int, float param1Float1, float param1Float2, float param1Float3) throws RemoteException {}
    
    public void setScale(int param1Int, float param1Float) throws RemoteException {}
    
    public void disableWindowMagnification(int param1Int) throws RemoteException {}
    
    public void moveWindowMagnifier(int param1Int, float param1Float1, float param1Float2) throws RemoteException {}
    
    public void setConnectionCallback(IWindowMagnificationConnectionCallback param1IWindowMagnificationConnectionCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowMagnificationConnection {
    private static final String DESCRIPTOR = "android.view.accessibility.IWindowMagnificationConnection";
    
    static final int TRANSACTION_disableWindowMagnification = 3;
    
    static final int TRANSACTION_enableWindowMagnification = 1;
    
    static final int TRANSACTION_moveWindowMagnifier = 4;
    
    static final int TRANSACTION_setConnectionCallback = 5;
    
    static final int TRANSACTION_setScale = 2;
    
    public Stub() {
      attachInterface(this, "android.view.accessibility.IWindowMagnificationConnection");
    }
    
    public static IWindowMagnificationConnection asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.accessibility.IWindowMagnificationConnection");
      if (iInterface != null && iInterface instanceof IWindowMagnificationConnection)
        return (IWindowMagnificationConnection)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "setConnectionCallback";
            } 
            return "moveWindowMagnifier";
          } 
          return "disableWindowMagnification";
        } 
        return "setScale";
      } 
      return "enableWindowMagnification";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IWindowMagnificationConnectionCallback iWindowMagnificationConnectionCallback;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.view.accessibility.IWindowMagnificationConnection");
                return true;
              } 
              param1Parcel1.enforceInterface("android.view.accessibility.IWindowMagnificationConnection");
              iWindowMagnificationConnectionCallback = IWindowMagnificationConnectionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
              setConnectionCallback(iWindowMagnificationConnectionCallback);
              return true;
            } 
            iWindowMagnificationConnectionCallback.enforceInterface("android.view.accessibility.IWindowMagnificationConnection");
            param1Int1 = iWindowMagnificationConnectionCallback.readInt();
            float f4 = iWindowMagnificationConnectionCallback.readFloat();
            float f5 = iWindowMagnificationConnectionCallback.readFloat();
            moveWindowMagnifier(param1Int1, f4, f5);
            return true;
          } 
          iWindowMagnificationConnectionCallback.enforceInterface("android.view.accessibility.IWindowMagnificationConnection");
          param1Int1 = iWindowMagnificationConnectionCallback.readInt();
          disableWindowMagnification(param1Int1);
          return true;
        } 
        iWindowMagnificationConnectionCallback.enforceInterface("android.view.accessibility.IWindowMagnificationConnection");
        param1Int1 = iWindowMagnificationConnectionCallback.readInt();
        float f = iWindowMagnificationConnectionCallback.readFloat();
        setScale(param1Int1, f);
        return true;
      } 
      iWindowMagnificationConnectionCallback.enforceInterface("android.view.accessibility.IWindowMagnificationConnection");
      param1Int1 = iWindowMagnificationConnectionCallback.readInt();
      float f1 = iWindowMagnificationConnectionCallback.readFloat();
      float f2 = iWindowMagnificationConnectionCallback.readFloat();
      float f3 = iWindowMagnificationConnectionCallback.readFloat();
      enableWindowMagnification(param1Int1, f1, f2, f3);
      return true;
    }
    
    private static class Proxy implements IWindowMagnificationConnection {
      public static IWindowMagnificationConnection sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.accessibility.IWindowMagnificationConnection";
      }
      
      public void enableWindowMagnification(int param2Int, float param2Float1, float param2Float2, float param2Float3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IWindowMagnificationConnection");
          parcel.writeInt(param2Int);
          parcel.writeFloat(param2Float1);
          parcel.writeFloat(param2Float2);
          parcel.writeFloat(param2Float3);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IWindowMagnificationConnection.Stub.getDefaultImpl() != null) {
            IWindowMagnificationConnection.Stub.getDefaultImpl().enableWindowMagnification(param2Int, param2Float1, param2Float2, param2Float3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setScale(int param2Int, float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IWindowMagnificationConnection");
          parcel.writeInt(param2Int);
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IWindowMagnificationConnection.Stub.getDefaultImpl() != null) {
            IWindowMagnificationConnection.Stub.getDefaultImpl().setScale(param2Int, param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disableWindowMagnification(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IWindowMagnificationConnection");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IWindowMagnificationConnection.Stub.getDefaultImpl() != null) {
            IWindowMagnificationConnection.Stub.getDefaultImpl().disableWindowMagnification(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void moveWindowMagnifier(int param2Int, float param2Float1, float param2Float2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IWindowMagnificationConnection");
          parcel.writeInt(param2Int);
          parcel.writeFloat(param2Float1);
          parcel.writeFloat(param2Float2);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IWindowMagnificationConnection.Stub.getDefaultImpl() != null) {
            IWindowMagnificationConnection.Stub.getDefaultImpl().moveWindowMagnifier(param2Int, param2Float1, param2Float2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setConnectionCallback(IWindowMagnificationConnectionCallback param2IWindowMagnificationConnectionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.accessibility.IWindowMagnificationConnection");
          if (param2IWindowMagnificationConnectionCallback != null) {
            iBinder = param2IWindowMagnificationConnectionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IWindowMagnificationConnection.Stub.getDefaultImpl() != null) {
            IWindowMagnificationConnection.Stub.getDefaultImpl().setConnectionCallback(param2IWindowMagnificationConnectionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowMagnificationConnection param1IWindowMagnificationConnection) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowMagnificationConnection != null) {
          Proxy.sDefaultImpl = param1IWindowMagnificationConnection;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowMagnificationConnection getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
