package android.view.accessibility;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IAccessibilityInteractionConnectionCallback extends IInterface {
  void setFindAccessibilityNodeInfoResult(AccessibilityNodeInfo paramAccessibilityNodeInfo, int paramInt) throws RemoteException;
  
  void setFindAccessibilityNodeInfosResult(List<AccessibilityNodeInfo> paramList, int paramInt) throws RemoteException;
  
  void setPerformAccessibilityActionResult(boolean paramBoolean, int paramInt) throws RemoteException;
  
  class Default implements IAccessibilityInteractionConnectionCallback {
    public void setFindAccessibilityNodeInfoResult(AccessibilityNodeInfo param1AccessibilityNodeInfo, int param1Int) throws RemoteException {}
    
    public void setFindAccessibilityNodeInfosResult(List<AccessibilityNodeInfo> param1List, int param1Int) throws RemoteException {}
    
    public void setPerformAccessibilityActionResult(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAccessibilityInteractionConnectionCallback {
    private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityInteractionConnectionCallback";
    
    static final int TRANSACTION_setFindAccessibilityNodeInfoResult = 1;
    
    static final int TRANSACTION_setFindAccessibilityNodeInfosResult = 2;
    
    static final int TRANSACTION_setPerformAccessibilityActionResult = 3;
    
    public Stub() {
      attachInterface(this, "android.view.accessibility.IAccessibilityInteractionConnectionCallback");
    }
    
    public static IAccessibilityInteractionConnectionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
      if (iInterface != null && iInterface instanceof IAccessibilityInteractionConnectionCallback)
        return (IAccessibilityInteractionConnectionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "setPerformAccessibilityActionResult";
        } 
        return "setFindAccessibilityNodeInfosResult";
      } 
      return "setFindAccessibilityNodeInfoResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          boolean bool;
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
          if (param1Parcel1.readInt() != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          param1Int1 = param1Parcel1.readInt();
          setPerformAccessibilityActionResult(bool, param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
        ArrayList<AccessibilityNodeInfo> arrayList = param1Parcel1.createTypedArrayList(AccessibilityNodeInfo.CREATOR);
        param1Int1 = param1Parcel1.readInt();
        setFindAccessibilityNodeInfosResult(arrayList, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
      if (param1Parcel1.readInt() != 0) {
        AccessibilityNodeInfo accessibilityNodeInfo = (AccessibilityNodeInfo)AccessibilityNodeInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = param1Parcel1.readInt();
      setFindAccessibilityNodeInfoResult((AccessibilityNodeInfo)param1Parcel2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IAccessibilityInteractionConnectionCallback {
      public static IAccessibilityInteractionConnectionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.accessibility.IAccessibilityInteractionConnectionCallback";
      }
      
      public void setFindAccessibilityNodeInfoResult(AccessibilityNodeInfo param2AccessibilityNodeInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
          if (param2AccessibilityNodeInfo != null) {
            parcel.writeInt(1);
            param2AccessibilityNodeInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnectionCallback.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnectionCallback.Stub.getDefaultImpl().setFindAccessibilityNodeInfoResult(param2AccessibilityNodeInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setFindAccessibilityNodeInfosResult(List<AccessibilityNodeInfo> param2List, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
          parcel.writeTypedList(param2List);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAccessibilityInteractionConnectionCallback.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnectionCallback.Stub.getDefaultImpl().setFindAccessibilityNodeInfosResult(param2List, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPerformAccessibilityActionResult(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityInteractionConnectionCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IAccessibilityInteractionConnectionCallback.Stub.getDefaultImpl() != null) {
            IAccessibilityInteractionConnectionCallback.Stub.getDefaultImpl().setPerformAccessibilityActionResult(param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAccessibilityInteractionConnectionCallback != null) {
          Proxy.sDefaultImpl = param1IAccessibilityInteractionConnectionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAccessibilityInteractionConnectionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
