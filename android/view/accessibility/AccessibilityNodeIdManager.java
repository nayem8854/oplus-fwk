package android.view.accessibility;

import android.view.View;

public final class AccessibilityNodeIdManager {
  private static AccessibilityNodeIdManager sIdManager;
  
  private WeakSparseArray<View> mIdsToViews = new WeakSparseArray<>();
  
  public static AccessibilityNodeIdManager getInstance() {
    // Byte code:
    //   0: ldc android/view/accessibility/AccessibilityNodeIdManager
    //   2: monitorenter
    //   3: getstatic android/view/accessibility/AccessibilityNodeIdManager.sIdManager : Landroid/view/accessibility/AccessibilityNodeIdManager;
    //   6: ifnonnull -> 21
    //   9: new android/view/accessibility/AccessibilityNodeIdManager
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/view/accessibility/AccessibilityNodeIdManager.sIdManager : Landroid/view/accessibility/AccessibilityNodeIdManager;
    //   21: getstatic android/view/accessibility/AccessibilityNodeIdManager.sIdManager : Landroid/view/accessibility/AccessibilityNodeIdManager;
    //   24: astore_0
    //   25: ldc android/view/accessibility/AccessibilityNodeIdManager
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/view/accessibility/AccessibilityNodeIdManager
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #31	-> 3
    //   #32	-> 9
    //   #34	-> 21
    //   #30	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  public void registerViewWithId(View paramView, int paramInt) {
    synchronized (this.mIdsToViews) {
      this.mIdsToViews.append(paramInt, paramView);
      return;
    } 
  }
  
  public void unregisterViewWithId(int paramInt) {
    synchronized (this.mIdsToViews) {
      this.mIdsToViews.remove(paramInt);
      return;
    } 
  }
  
  public View findView(int paramInt) {
    synchronized (this.mIdsToViews) {
      View view = this.mIdsToViews.get(paramInt);
      if (view == null || !view.includeForAccessibility())
        view = null; 
      return view;
    } 
  }
}
