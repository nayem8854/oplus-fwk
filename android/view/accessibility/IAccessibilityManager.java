package android.view.accessibility;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.IAccessibilityServiceClient;
import android.app.RemoteAction;
import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.IWindow;
import java.util.List;

public interface IAccessibilityManager extends IInterface {
  int addAccessibilityInteractionConnection(IWindow paramIWindow, IBinder paramIBinder, IAccessibilityInteractionConnection paramIAccessibilityInteractionConnection, String paramString, int paramInt) throws RemoteException;
  
  long addClient(IAccessibilityManagerClient paramIAccessibilityManagerClient, int paramInt) throws RemoteException;
  
  void associateEmbeddedHierarchy(IBinder paramIBinder1, IBinder paramIBinder2) throws RemoteException;
  
  void disassociateEmbeddedHierarchy(IBinder paramIBinder) throws RemoteException;
  
  List<String> getAccessibilityShortcutTargets(int paramInt) throws RemoteException;
  
  int getAccessibilityWindowId(IBinder paramIBinder) throws RemoteException;
  
  List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int paramInt1, int paramInt2) throws RemoteException;
  
  List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList(int paramInt) throws RemoteException;
  
  long getRecommendedTimeoutMillis() throws RemoteException;
  
  IBinder getWindowToken(int paramInt1, int paramInt2) throws RemoteException;
  
  void interrupt(int paramInt) throws RemoteException;
  
  void notifyAccessibilityButtonClicked(int paramInt, String paramString) throws RemoteException;
  
  void notifyAccessibilityButtonVisibilityChanged(boolean paramBoolean) throws RemoteException;
  
  void performAccessibilityShortcut(String paramString) throws RemoteException;
  
  void registerSystemAction(RemoteAction paramRemoteAction, int paramInt) throws RemoteException;
  
  void registerUiTestAutomationService(IBinder paramIBinder, IAccessibilityServiceClient paramIAccessibilityServiceClient, AccessibilityServiceInfo paramAccessibilityServiceInfo, int paramInt) throws RemoteException;
  
  void removeAccessibilityInteractionConnection(IWindow paramIWindow) throws RemoteException;
  
  void sendAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent, int paramInt) throws RemoteException;
  
  boolean sendFingerprintGesture(int paramInt) throws RemoteException;
  
  void setPictureInPictureActionReplacingConnection(IAccessibilityInteractionConnection paramIAccessibilityInteractionConnection) throws RemoteException;
  
  void setWindowMagnificationConnection(IWindowMagnificationConnection paramIWindowMagnificationConnection) throws RemoteException;
  
  void temporaryEnableAccessibilityStateUntilKeyguardRemoved(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void unregisterSystemAction(int paramInt) throws RemoteException;
  
  void unregisterUiTestAutomationService(IAccessibilityServiceClient paramIAccessibilityServiceClient) throws RemoteException;
  
  class Default implements IAccessibilityManager {
    public void interrupt(int param1Int) throws RemoteException {}
    
    public void sendAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent, int param1Int) throws RemoteException {}
    
    public long addClient(IAccessibilityManagerClient param1IAccessibilityManagerClient, int param1Int) throws RemoteException {
      return 0L;
    }
    
    public List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public int addAccessibilityInteractionConnection(IWindow param1IWindow, IBinder param1IBinder, IAccessibilityInteractionConnection param1IAccessibilityInteractionConnection, String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void removeAccessibilityInteractionConnection(IWindow param1IWindow) throws RemoteException {}
    
    public void setPictureInPictureActionReplacingConnection(IAccessibilityInteractionConnection param1IAccessibilityInteractionConnection) throws RemoteException {}
    
    public void registerUiTestAutomationService(IBinder param1IBinder, IAccessibilityServiceClient param1IAccessibilityServiceClient, AccessibilityServiceInfo param1AccessibilityServiceInfo, int param1Int) throws RemoteException {}
    
    public void unregisterUiTestAutomationService(IAccessibilityServiceClient param1IAccessibilityServiceClient) throws RemoteException {}
    
    public void temporaryEnableAccessibilityStateUntilKeyguardRemoved(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public IBinder getWindowToken(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void notifyAccessibilityButtonClicked(int param1Int, String param1String) throws RemoteException {}
    
    public void notifyAccessibilityButtonVisibilityChanged(boolean param1Boolean) throws RemoteException {}
    
    public void performAccessibilityShortcut(String param1String) throws RemoteException {}
    
    public List<String> getAccessibilityShortcutTargets(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean sendFingerprintGesture(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getAccessibilityWindowId(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public long getRecommendedTimeoutMillis() throws RemoteException {
      return 0L;
    }
    
    public void registerSystemAction(RemoteAction param1RemoteAction, int param1Int) throws RemoteException {}
    
    public void unregisterSystemAction(int param1Int) throws RemoteException {}
    
    public void setWindowMagnificationConnection(IWindowMagnificationConnection param1IWindowMagnificationConnection) throws RemoteException {}
    
    public void associateEmbeddedHierarchy(IBinder param1IBinder1, IBinder param1IBinder2) throws RemoteException {}
    
    public void disassociateEmbeddedHierarchy(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAccessibilityManager {
    private static final String DESCRIPTOR = "android.view.accessibility.IAccessibilityManager";
    
    static final int TRANSACTION_addAccessibilityInteractionConnection = 6;
    
    static final int TRANSACTION_addClient = 3;
    
    static final int TRANSACTION_associateEmbeddedHierarchy = 23;
    
    static final int TRANSACTION_disassociateEmbeddedHierarchy = 24;
    
    static final int TRANSACTION_getAccessibilityShortcutTargets = 16;
    
    static final int TRANSACTION_getAccessibilityWindowId = 18;
    
    static final int TRANSACTION_getEnabledAccessibilityServiceList = 5;
    
    static final int TRANSACTION_getInstalledAccessibilityServiceList = 4;
    
    static final int TRANSACTION_getRecommendedTimeoutMillis = 19;
    
    static final int TRANSACTION_getWindowToken = 12;
    
    static final int TRANSACTION_interrupt = 1;
    
    static final int TRANSACTION_notifyAccessibilityButtonClicked = 13;
    
    static final int TRANSACTION_notifyAccessibilityButtonVisibilityChanged = 14;
    
    static final int TRANSACTION_performAccessibilityShortcut = 15;
    
    static final int TRANSACTION_registerSystemAction = 20;
    
    static final int TRANSACTION_registerUiTestAutomationService = 9;
    
    static final int TRANSACTION_removeAccessibilityInteractionConnection = 7;
    
    static final int TRANSACTION_sendAccessibilityEvent = 2;
    
    static final int TRANSACTION_sendFingerprintGesture = 17;
    
    static final int TRANSACTION_setPictureInPictureActionReplacingConnection = 8;
    
    static final int TRANSACTION_setWindowMagnificationConnection = 22;
    
    static final int TRANSACTION_temporaryEnableAccessibilityStateUntilKeyguardRemoved = 11;
    
    static final int TRANSACTION_unregisterSystemAction = 21;
    
    static final int TRANSACTION_unregisterUiTestAutomationService = 10;
    
    public Stub() {
      attachInterface(this, "android.view.accessibility.IAccessibilityManager");
    }
    
    public static IAccessibilityManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.accessibility.IAccessibilityManager");
      if (iInterface != null && iInterface instanceof IAccessibilityManager)
        return (IAccessibilityManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 24:
          return "disassociateEmbeddedHierarchy";
        case 23:
          return "associateEmbeddedHierarchy";
        case 22:
          return "setWindowMagnificationConnection";
        case 21:
          return "unregisterSystemAction";
        case 20:
          return "registerSystemAction";
        case 19:
          return "getRecommendedTimeoutMillis";
        case 18:
          return "getAccessibilityWindowId";
        case 17:
          return "sendFingerprintGesture";
        case 16:
          return "getAccessibilityShortcutTargets";
        case 15:
          return "performAccessibilityShortcut";
        case 14:
          return "notifyAccessibilityButtonVisibilityChanged";
        case 13:
          return "notifyAccessibilityButtonClicked";
        case 12:
          return "getWindowToken";
        case 11:
          return "temporaryEnableAccessibilityStateUntilKeyguardRemoved";
        case 10:
          return "unregisterUiTestAutomationService";
        case 9:
          return "registerUiTestAutomationService";
        case 8:
          return "setPictureInPictureActionReplacingConnection";
        case 7:
          return "removeAccessibilityInteractionConnection";
        case 6:
          return "addAccessibilityInteractionConnection";
        case 5:
          return "getEnabledAccessibilityServiceList";
        case 4:
          return "getInstalledAccessibilityServiceList";
        case 3:
          return "addClient";
        case 2:
          return "sendAccessibilityEvent";
        case 1:
          break;
      } 
      return "interrupt";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        IBinder iBinder3;
        IWindowMagnificationConnection iWindowMagnificationConnection;
        IBinder iBinder2;
        List<String> list1;
        String str1;
        IBinder iBinder1;
        IAccessibilityServiceClient iAccessibilityServiceClient1;
        IAccessibilityInteractionConnection iAccessibilityInteractionConnection1;
        IWindow iWindow1;
        List<AccessibilityServiceInfo> list;
        IBinder iBinder4;
        IWindow iWindow2;
        IAccessibilityManagerClient iAccessibilityManagerClient;
        long l;
        IBinder iBinder5;
        IAccessibilityServiceClient iAccessibilityServiceClient2;
        IAccessibilityInteractionConnection iAccessibilityInteractionConnection2;
        String str2;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 24:
            param1Parcel1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iBinder3 = param1Parcel1.readStrongBinder();
            disassociateEmbeddedHierarchy(iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            iBinder3.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iBinder4 = iBinder3.readStrongBinder();
            iBinder3 = iBinder3.readStrongBinder();
            associateEmbeddedHierarchy(iBinder4, iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            iBinder3.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iWindowMagnificationConnection = IWindowMagnificationConnection.Stub.asInterface(iBinder3.readStrongBinder());
            setWindowMagnificationConnection(iWindowMagnificationConnection);
            return true;
          case 21:
            iWindowMagnificationConnection.enforceInterface("android.view.accessibility.IAccessibilityManager");
            param1Int1 = iWindowMagnificationConnection.readInt();
            unregisterSystemAction(param1Int1);
            return true;
          case 20:
            iWindowMagnificationConnection.enforceInterface("android.view.accessibility.IAccessibilityManager");
            if (iWindowMagnificationConnection.readInt() != 0) {
              RemoteAction remoteAction = (RemoteAction)RemoteAction.CREATOR.createFromParcel((Parcel)iWindowMagnificationConnection);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = iWindowMagnificationConnection.readInt();
            registerSystemAction((RemoteAction)param1Parcel2, param1Int1);
            return true;
          case 19:
            iWindowMagnificationConnection.enforceInterface("android.view.accessibility.IAccessibilityManager");
            l = getRecommendedTimeoutMillis();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 18:
            iWindowMagnificationConnection.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iBinder2 = iWindowMagnificationConnection.readStrongBinder();
            param1Int1 = getAccessibilityWindowId(iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 17:
            iBinder2.enforceInterface("android.view.accessibility.IAccessibilityManager");
            param1Int1 = iBinder2.readInt();
            bool = sendFingerprintGesture(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 16:
            iBinder2.enforceInterface("android.view.accessibility.IAccessibilityManager");
            i = iBinder2.readInt();
            list1 = getAccessibilityShortcutTargets(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 15:
            list1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            str1 = list1.readString();
            performAccessibilityShortcut(str1);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            if (str1.readInt() != 0)
              bool2 = true; 
            notifyAccessibilityButtonVisibilityChanged(bool2);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            i = str1.readInt();
            str1 = str1.readString();
            notifyAccessibilityButtonClicked(i, str1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            param1Int2 = str1.readInt();
            i = str1.readInt();
            iBinder1 = getWindowToken(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 11:
            iBinder1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            if (iBinder1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder4 = null;
            } 
            bool2 = bool1;
            if (iBinder1.readInt() != 0)
              bool2 = true; 
            temporaryEnableAccessibilityStateUntilKeyguardRemoved((ComponentName)iBinder4, bool2);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            iBinder1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iAccessibilityServiceClient1 = IAccessibilityServiceClient.Stub.asInterface(iBinder1.readStrongBinder());
            unregisterUiTestAutomationService(iAccessibilityServiceClient1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iAccessibilityServiceClient1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iBinder5 = iAccessibilityServiceClient1.readStrongBinder();
            iAccessibilityServiceClient2 = IAccessibilityServiceClient.Stub.asInterface(iAccessibilityServiceClient1.readStrongBinder());
            if (iAccessibilityServiceClient1.readInt() != 0) {
              AccessibilityServiceInfo accessibilityServiceInfo = (AccessibilityServiceInfo)AccessibilityServiceInfo.CREATOR.createFromParcel((Parcel)iAccessibilityServiceClient1);
            } else {
              iBinder4 = null;
            } 
            i = iAccessibilityServiceClient1.readInt();
            registerUiTestAutomationService(iBinder5, iAccessibilityServiceClient2, (AccessibilityServiceInfo)iBinder4, i);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iAccessibilityServiceClient1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iAccessibilityInteractionConnection1 = IAccessibilityInteractionConnection.Stub.asInterface(iAccessibilityServiceClient1.readStrongBinder());
            setPictureInPictureActionReplacingConnection(iAccessibilityInteractionConnection1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iAccessibilityInteractionConnection1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iWindow1 = IWindow.Stub.asInterface(iAccessibilityInteractionConnection1.readStrongBinder());
            removeAccessibilityInteractionConnection(iWindow1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iWindow1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iWindow2 = IWindow.Stub.asInterface(iWindow1.readStrongBinder());
            iBinder5 = iWindow1.readStrongBinder();
            iAccessibilityInteractionConnection2 = IAccessibilityInteractionConnection.Stub.asInterface(iWindow1.readStrongBinder());
            str2 = iWindow1.readString();
            i = iWindow1.readInt();
            i = addAccessibilityInteractionConnection(iWindow2, iBinder5, iAccessibilityInteractionConnection2, str2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 5:
            iWindow1.enforceInterface("android.view.accessibility.IAccessibilityManager");
            param1Int2 = iWindow1.readInt();
            i = iWindow1.readInt();
            list = getEnabledAccessibilityServiceList(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 4:
            list.enforceInterface("android.view.accessibility.IAccessibilityManager");
            i = list.readInt();
            list = getInstalledAccessibilityServiceList(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 3:
            list.enforceInterface("android.view.accessibility.IAccessibilityManager");
            iAccessibilityManagerClient = IAccessibilityManagerClient.Stub.asInterface(list.readStrongBinder());
            i = list.readInt();
            l = addClient(iAccessibilityManagerClient, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 2:
            list.enforceInterface("android.view.accessibility.IAccessibilityManager");
            if (list.readInt() != 0) {
              AccessibilityEvent accessibilityEvent = (AccessibilityEvent)AccessibilityEvent.CREATOR.createFromParcel((Parcel)list);
            } else {
              param1Parcel2 = null;
            } 
            i = list.readInt();
            sendAccessibilityEvent((AccessibilityEvent)param1Parcel2, i);
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.view.accessibility.IAccessibilityManager");
        int i = list.readInt();
        interrupt(i);
        return true;
      } 
      param1Parcel2.writeString("android.view.accessibility.IAccessibilityManager");
      return true;
    }
    
    private static class Proxy implements IAccessibilityManager {
      public static IAccessibilityManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.accessibility.IAccessibilityManager";
      }
      
      public void interrupt(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().interrupt(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendAccessibilityEvent(AccessibilityEvent param2AccessibilityEvent, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          if (param2AccessibilityEvent != null) {
            parcel.writeInt(1);
            param2AccessibilityEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().sendAccessibilityEvent(param2AccessibilityEvent, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public long addClient(IAccessibilityManagerClient param2IAccessibilityManagerClient, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          if (param2IAccessibilityManagerClient != null) {
            iBinder = param2IAccessibilityManagerClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null)
            return IAccessibilityManager.Stub.getDefaultImpl().addClient(param2IAccessibilityManagerClient, param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null)
            return IAccessibilityManager.Stub.getDefaultImpl().getInstalledAccessibilityServiceList(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AccessibilityServiceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null)
            return IAccessibilityManager.Stub.getDefaultImpl().getEnabledAccessibilityServiceList(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AccessibilityServiceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addAccessibilityInteractionConnection(IWindow param2IWindow, IBinder param2IBinder, IAccessibilityInteractionConnection param2IAccessibilityInteractionConnection, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          IBinder iBinder1 = null;
          if (param2IWindow != null) {
            iBinder2 = param2IWindow.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder2 = iBinder1;
          if (param2IAccessibilityInteractionConnection != null)
            iBinder2 = param2IAccessibilityInteractionConnection.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            param2Int = IAccessibilityManager.Stub.getDefaultImpl().addAccessibilityInteractionConnection(param2IWindow, param2IBinder, param2IAccessibilityInteractionConnection, param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAccessibilityInteractionConnection(IWindow param2IWindow) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().removeAccessibilityInteractionConnection(param2IWindow);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPictureInPictureActionReplacingConnection(IAccessibilityInteractionConnection param2IAccessibilityInteractionConnection) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          if (param2IAccessibilityInteractionConnection != null) {
            iBinder = param2IAccessibilityInteractionConnection.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().setPictureInPictureActionReplacingConnection(param2IAccessibilityInteractionConnection);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerUiTestAutomationService(IBinder param2IBinder, IAccessibilityServiceClient param2IAccessibilityServiceClient, AccessibilityServiceInfo param2AccessibilityServiceInfo, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2IAccessibilityServiceClient != null) {
            iBinder = param2IAccessibilityServiceClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2AccessibilityServiceInfo != null) {
            parcel1.writeInt(1);
            param2AccessibilityServiceInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().registerUiTestAutomationService(param2IBinder, param2IAccessibilityServiceClient, param2AccessibilityServiceInfo, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterUiTestAutomationService(IAccessibilityServiceClient param2IAccessibilityServiceClient) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          if (param2IAccessibilityServiceClient != null) {
            iBinder = param2IAccessibilityServiceClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().unregisterUiTestAutomationService(param2IAccessibilityServiceClient);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void temporaryEnableAccessibilityStateUntilKeyguardRemoved(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().temporaryEnableAccessibilityStateUntilKeyguardRemoved(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBinder getWindowToken(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null)
            return IAccessibilityManager.Stub.getDefaultImpl().getWindowToken(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyAccessibilityButtonClicked(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().notifyAccessibilityButtonClicked(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyAccessibilityButtonVisibilityChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().notifyAccessibilityButtonVisibilityChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void performAccessibilityShortcut(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().performAccessibilityShortcut(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAccessibilityShortcutTargets(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null)
            return IAccessibilityManager.Stub.getDefaultImpl().getAccessibilityShortcutTargets(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean sendFingerprintGesture(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            bool1 = IAccessibilityManager.Stub.getDefaultImpl().sendFingerprintGesture(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAccessibilityWindowId(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null)
            return IAccessibilityManager.Stub.getDefaultImpl().getAccessibilityWindowId(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getRecommendedTimeoutMillis() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null)
            return IAccessibilityManager.Stub.getDefaultImpl().getRecommendedTimeoutMillis(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerSystemAction(RemoteAction param2RemoteAction, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          if (param2RemoteAction != null) {
            parcel.writeInt(1);
            param2RemoteAction.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().registerSystemAction(param2RemoteAction, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregisterSystemAction(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().unregisterSystemAction(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setWindowMagnificationConnection(IWindowMagnificationConnection param2IWindowMagnificationConnection) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          if (param2IWindowMagnificationConnection != null) {
            iBinder = param2IWindowMagnificationConnection.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().setWindowMagnificationConnection(param2IWindowMagnificationConnection);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void associateEmbeddedHierarchy(IBinder param2IBinder1, IBinder param2IBinder2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeStrongBinder(param2IBinder1);
          parcel1.writeStrongBinder(param2IBinder2);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().associateEmbeddedHierarchy(param2IBinder1, param2IBinder2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disassociateEmbeddedHierarchy(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.accessibility.IAccessibilityManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IAccessibilityManager.Stub.getDefaultImpl() != null) {
            IAccessibilityManager.Stub.getDefaultImpl().disassociateEmbeddedHierarchy(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAccessibilityManager param1IAccessibilityManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAccessibilityManager != null) {
          Proxy.sDefaultImpl = param1IAccessibilityManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAccessibilityManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
