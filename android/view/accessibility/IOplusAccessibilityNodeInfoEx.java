package android.view.accessibility;

public interface IOplusAccessibilityNodeInfoEx {
  CharSequence getRealClassName();
}
