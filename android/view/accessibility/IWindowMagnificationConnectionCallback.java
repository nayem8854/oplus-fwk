package android.view.accessibility;

import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWindowMagnificationConnectionCallback extends IInterface {
  void onChangeMagnificationMode(int paramInt1, int paramInt2) throws RemoteException;
  
  void onWindowMagnifierBoundsChanged(int paramInt, Rect paramRect) throws RemoteException;
  
  class Default implements IWindowMagnificationConnectionCallback {
    public void onWindowMagnifierBoundsChanged(int param1Int, Rect param1Rect) throws RemoteException {}
    
    public void onChangeMagnificationMode(int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowMagnificationConnectionCallback {
    private static final String DESCRIPTOR = "android.view.accessibility.IWindowMagnificationConnectionCallback";
    
    static final int TRANSACTION_onChangeMagnificationMode = 2;
    
    static final int TRANSACTION_onWindowMagnifierBoundsChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.view.accessibility.IWindowMagnificationConnectionCallback");
    }
    
    public static IWindowMagnificationConnectionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.accessibility.IWindowMagnificationConnectionCallback");
      if (iInterface != null && iInterface instanceof IWindowMagnificationConnectionCallback)
        return (IWindowMagnificationConnectionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onChangeMagnificationMode";
      } 
      return "onWindowMagnifierBoundsChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.view.accessibility.IWindowMagnificationConnectionCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.accessibility.IWindowMagnificationConnectionCallback");
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        onChangeMagnificationMode(param1Int2, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.accessibility.IWindowMagnificationConnectionCallback");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        Rect rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onWindowMagnifierBoundsChanged(param1Int1, (Rect)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IWindowMagnificationConnectionCallback {
      public static IWindowMagnificationConnectionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.accessibility.IWindowMagnificationConnectionCallback";
      }
      
      public void onWindowMagnifierBoundsChanged(int param2Int, Rect param2Rect) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IWindowMagnificationConnectionCallback");
          parcel.writeInt(param2Int);
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IWindowMagnificationConnectionCallback.Stub.getDefaultImpl() != null) {
            IWindowMagnificationConnectionCallback.Stub.getDefaultImpl().onWindowMagnifierBoundsChanged(param2Int, param2Rect);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onChangeMagnificationMode(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.accessibility.IWindowMagnificationConnectionCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IWindowMagnificationConnectionCallback.Stub.getDefaultImpl() != null) {
            IWindowMagnificationConnectionCallback.Stub.getDefaultImpl().onChangeMagnificationMode(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowMagnificationConnectionCallback param1IWindowMagnificationConnectionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowMagnificationConnectionCallback != null) {
          Proxy.sDefaultImpl = param1IWindowMagnificationConnectionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowMagnificationConnectionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
