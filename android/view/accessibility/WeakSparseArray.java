package android.view.accessibility;

import android.util.SparseArray;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

final class WeakSparseArray<E> {
  private final ReferenceQueue<E> mRefQueue = new ReferenceQueue<>();
  
  private final SparseArray<WeakReferenceWithId<E>> mSparseArray = new SparseArray<>();
  
  public void append(int paramInt, E paramE) {
    removeUnreachableValues();
    this.mSparseArray.append(paramInt, new WeakReferenceWithId<>(paramE, this.mRefQueue, paramInt));
  }
  
  public void remove(int paramInt) {
    removeUnreachableValues();
    this.mSparseArray.remove(paramInt);
  }
  
  public E get(int paramInt) {
    removeUnreachableValues();
    WeakReferenceWithId<WeakReferenceWithId> weakReferenceWithId = (WeakReferenceWithId)this.mSparseArray.get(paramInt);
    if (weakReferenceWithId != null) {
      weakReferenceWithId = weakReferenceWithId.get();
    } else {
      weakReferenceWithId = null;
    } 
    return (E)weakReferenceWithId;
  }
  
  private void removeUnreachableValues() {
    for (Reference<? extends E> reference = this.mRefQueue.poll(); reference != null; reference = this.mRefQueue.poll())
      this.mSparseArray.remove(((WeakReferenceWithId)reference).mId); 
  }
  
  private static class WeakReferenceWithId<E> extends WeakReference<E> {
    final int mId;
    
    WeakReferenceWithId(E param1E, ReferenceQueue<? super E> param1ReferenceQueue, int param1Int) {
      super(param1E, param1ReferenceQueue);
      this.mId = param1Int;
    }
  }
}
