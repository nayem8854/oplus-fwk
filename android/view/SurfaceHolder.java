package android.view;

import android.graphics.Canvas;
import android.graphics.Rect;

public interface SurfaceHolder {
  @Deprecated
  public static final int SURFACE_TYPE_GPU = 2;
  
  @Deprecated
  public static final int SURFACE_TYPE_HARDWARE = 1;
  
  @Deprecated
  public static final int SURFACE_TYPE_NORMAL = 0;
  
  @Deprecated
  public static final int SURFACE_TYPE_PUSH_BUFFERS = 3;
  
  void addCallback(Callback paramCallback);
  
  Surface getSurface();
  
  Rect getSurfaceFrame();
  
  boolean isCreating();
  
  Canvas lockCanvas();
  
  Canvas lockCanvas(Rect paramRect);
  
  public static class BadSurfaceTypeException extends RuntimeException {
    public BadSurfaceTypeException() {}
    
    public BadSurfaceTypeException(String param1String) {
      super(param1String);
    }
  }
  
  public static interface Callback {
    void surfaceChanged(SurfaceHolder param1SurfaceHolder, int param1Int1, int param1Int2, int param1Int3);
    
    void surfaceCreated(SurfaceHolder param1SurfaceHolder);
    
    void surfaceDestroyed(SurfaceHolder param1SurfaceHolder);
  }
  
  class Callback2 implements Callback {
    public abstract void surfaceRedrawNeeded(SurfaceHolder param1SurfaceHolder);
    
    public void surfaceRedrawNeededAsync(SurfaceHolder param1SurfaceHolder, Runnable param1Runnable) {
      surfaceRedrawNeeded(param1SurfaceHolder);
      param1Runnable.run();
    }
  }
  
  default Canvas lockHardwareCanvas() {
    throw new IllegalStateException("This SurfaceHolder doesn't support lockHardwareCanvas");
  }
  
  void removeCallback(Callback paramCallback);
  
  void setFixedSize(int paramInt1, int paramInt2);
  
  void setFormat(int paramInt);
  
  void setKeepScreenOn(boolean paramBoolean);
  
  void setSizeFromLayout();
  
  @Deprecated
  void setType(int paramInt);
  
  void unlockCanvasAndPost(Canvas paramCanvas);
}
