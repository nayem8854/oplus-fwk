package android.view;

public interface IOplusMotionEventInterface {
  float etXOffset();
  
  float getX();
  
  float getY();
  
  float getYOffset();
}
