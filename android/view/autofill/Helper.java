package android.view.autofill;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public final class Helper {
  public static boolean sDebug = false;
  
  public static boolean sVerbose = false;
  
  public static void appendRedacted(StringBuilder paramStringBuilder, CharSequence paramCharSequence) {
    paramStringBuilder.append(getRedacted(paramCharSequence));
  }
  
  public static String getRedacted(CharSequence paramCharSequence) {
    if (paramCharSequence == null) {
      paramCharSequence = "null";
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramCharSequence.length());
      stringBuilder.append("_chars");
      paramCharSequence = stringBuilder.toString();
    } 
    return (String)paramCharSequence;
  }
  
  public static void appendRedacted(StringBuilder paramStringBuilder, String[] paramArrayOfString) {
    if (paramArrayOfString == null) {
      paramStringBuilder.append("N/A");
      return;
    } 
    paramStringBuilder.append("[");
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      paramStringBuilder.append(" '");
      appendRedacted(paramStringBuilder, str);
      paramStringBuilder.append("'");
      b++;
    } 
    paramStringBuilder.append(" ]");
  }
  
  public static AutofillId[] toArray(Collection<AutofillId> paramCollection) {
    if (paramCollection == null)
      return new AutofillId[0]; 
    AutofillId[] arrayOfAutofillId = new AutofillId[paramCollection.size()];
    paramCollection.toArray(arrayOfAutofillId);
    return arrayOfAutofillId;
  }
  
  public static <T> ArrayList<T> toList(Set<T> paramSet) {
    ArrayList<T> arrayList;
    if (paramSet == null) {
      paramSet = null;
    } else {
      arrayList = new ArrayList<>(paramSet);
    } 
    return arrayList;
  }
  
  private Helper() {
    throw new UnsupportedOperationException("contains static members only");
  }
}
