package android.view.autofill;

import android.content.AutofillOptions;

public abstract class AutofillManagerInternal {
  public abstract AutofillOptions getAutofillOptions(String paramString, long paramLong, int paramInt);
  
  public abstract boolean isAugmentedAutofillServiceForUser(int paramInt1, int paramInt2);
  
  public abstract void onBackKeyPressed();
}
