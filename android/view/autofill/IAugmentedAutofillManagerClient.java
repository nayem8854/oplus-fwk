package android.view.autofill;

import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IAugmentedAutofillManagerClient extends IInterface {
  void autofill(int paramInt, List<AutofillId> paramList, List<AutofillValue> paramList1, boolean paramBoolean) throws RemoteException;
  
  Rect getViewCoordinates(AutofillId paramAutofillId) throws RemoteException;
  
  boolean requestAutofill(int paramInt, AutofillId paramAutofillId) throws RemoteException;
  
  void requestHideFillUi(int paramInt, AutofillId paramAutofillId) throws RemoteException;
  
  void requestShowFillUi(int paramInt1, AutofillId paramAutofillId, int paramInt2, int paramInt3, Rect paramRect, IAutofillWindowPresenter paramIAutofillWindowPresenter) throws RemoteException;
  
  class Default implements IAugmentedAutofillManagerClient {
    public Rect getViewCoordinates(AutofillId param1AutofillId) throws RemoteException {
      return null;
    }
    
    public void autofill(int param1Int, List<AutofillId> param1List, List<AutofillValue> param1List1, boolean param1Boolean) throws RemoteException {}
    
    public void requestShowFillUi(int param1Int1, AutofillId param1AutofillId, int param1Int2, int param1Int3, Rect param1Rect, IAutofillWindowPresenter param1IAutofillWindowPresenter) throws RemoteException {}
    
    public void requestHideFillUi(int param1Int, AutofillId param1AutofillId) throws RemoteException {}
    
    public boolean requestAutofill(int param1Int, AutofillId param1AutofillId) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAugmentedAutofillManagerClient {
    private static final String DESCRIPTOR = "android.view.autofill.IAugmentedAutofillManagerClient";
    
    static final int TRANSACTION_autofill = 2;
    
    static final int TRANSACTION_getViewCoordinates = 1;
    
    static final int TRANSACTION_requestAutofill = 5;
    
    static final int TRANSACTION_requestHideFillUi = 4;
    
    static final int TRANSACTION_requestShowFillUi = 3;
    
    public Stub() {
      attachInterface(this, "android.view.autofill.IAugmentedAutofillManagerClient");
    }
    
    public static IAugmentedAutofillManagerClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.autofill.IAugmentedAutofillManagerClient");
      if (iInterface != null && iInterface instanceof IAugmentedAutofillManagerClient)
        return (IAugmentedAutofillManagerClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "requestAutofill";
            } 
            return "requestHideFillUi";
          } 
          return "requestShowFillUi";
        } 
        return "autofill";
      } 
      return "getViewCoordinates";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IAutofillWindowPresenter iAutofillWindowPresenter;
      boolean bool = false;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          AutofillId autofillId;
          Rect rect1;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.view.autofill.IAugmentedAutofillManagerClient");
                return true;
              } 
              param1Parcel1.enforceInterface("android.view.autofill.IAugmentedAutofillManagerClient");
              param1Int1 = param1Parcel1.readInt();
              if (param1Parcel1.readInt() != 0) {
                AutofillId autofillId1 = (AutofillId)AutofillId.CREATOR.createFromParcel(param1Parcel1);
              } else {
                param1Parcel1 = null;
              } 
              boolean bool1 = requestAutofill(param1Int1, (AutofillId)param1Parcel1);
              param1Parcel2.writeNoException();
              param1Parcel2.writeInt(bool1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.view.autofill.IAugmentedAutofillManagerClient");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              AutofillId autofillId1 = (AutofillId)AutofillId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            requestHideFillUi(param1Int1, (AutofillId)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.autofill.IAugmentedAutofillManagerClient");
          int i = param1Parcel1.readInt();
          if (param1Parcel1.readInt() != 0) {
            autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel(param1Parcel1);
          } else {
            autofillId = null;
          } 
          param1Int1 = param1Parcel1.readInt();
          param1Int2 = param1Parcel1.readInt();
          if (param1Parcel1.readInt() != 0) {
            rect1 = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
          } else {
            rect1 = null;
          } 
          iAutofillWindowPresenter = IAutofillWindowPresenter.Stub.asInterface(param1Parcel1.readStrongBinder());
          requestShowFillUi(i, autofillId, param1Int1, param1Int2, rect1, iAutofillWindowPresenter);
          param1Parcel2.writeNoException();
          return true;
        } 
        iAutofillWindowPresenter.enforceInterface("android.view.autofill.IAugmentedAutofillManagerClient");
        param1Int1 = iAutofillWindowPresenter.readInt();
        ArrayList<AutofillId> arrayList = iAutofillWindowPresenter.createTypedArrayList(AutofillId.CREATOR);
        ArrayList<AutofillValue> arrayList1 = iAutofillWindowPresenter.createTypedArrayList(AutofillValue.CREATOR);
        if (iAutofillWindowPresenter.readInt() != 0)
          bool = true; 
        autofill(param1Int1, arrayList, arrayList1, bool);
        param1Parcel2.writeNoException();
        return true;
      } 
      iAutofillWindowPresenter.enforceInterface("android.view.autofill.IAugmentedAutofillManagerClient");
      if (iAutofillWindowPresenter.readInt() != 0) {
        AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)iAutofillWindowPresenter);
      } else {
        iAutofillWindowPresenter = null;
      } 
      Rect rect = getViewCoordinates((AutofillId)iAutofillWindowPresenter);
      param1Parcel2.writeNoException();
      if (rect != null) {
        param1Parcel2.writeInt(1);
        rect.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IAugmentedAutofillManagerClient {
      public static IAugmentedAutofillManagerClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.autofill.IAugmentedAutofillManagerClient";
      }
      
      public Rect getViewCoordinates(AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.autofill.IAugmentedAutofillManagerClient");
          if (param2AutofillId != null) {
            parcel1.writeInt(1);
            param2AutofillId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAugmentedAutofillManagerClient.Stub.getDefaultImpl() != null)
            return IAugmentedAutofillManagerClient.Stub.getDefaultImpl().getViewCoordinates(param2AutofillId); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Rect rect = (Rect)Rect.CREATOR.createFromParcel(parcel2);
          } else {
            param2AutofillId = null;
          } 
          return (Rect)param2AutofillId;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void autofill(int param2Int, List<AutofillId> param2List, List<AutofillValue> param2List1, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.autofill.IAugmentedAutofillManagerClient");
          parcel1.writeInt(param2Int);
          parcel1.writeTypedList(param2List);
          parcel1.writeTypedList(param2List1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IAugmentedAutofillManagerClient.Stub.getDefaultImpl() != null) {
            IAugmentedAutofillManagerClient.Stub.getDefaultImpl().autofill(param2Int, param2List, param2List1, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestShowFillUi(int param2Int1, AutofillId param2AutofillId, int param2Int2, int param2Int3, Rect param2Rect, IAutofillWindowPresenter param2IAutofillWindowPresenter) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.autofill.IAugmentedAutofillManagerClient");
          try {
            parcel1.writeInt(param2Int1);
            if (param2AutofillId != null) {
              parcel1.writeInt(1);
              param2AutofillId.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeInt(param2Int2);
              try {
                IBinder iBinder;
                parcel1.writeInt(param2Int3);
                if (param2Rect != null) {
                  parcel1.writeInt(1);
                  param2Rect.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                if (param2IAutofillWindowPresenter != null) {
                  iBinder = param2IAutofillWindowPresenter.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                  if (!bool && IAugmentedAutofillManagerClient.Stub.getDefaultImpl() != null) {
                    IAugmentedAutofillManagerClient.Stub.getDefaultImpl().requestShowFillUi(param2Int1, param2AutofillId, param2Int2, param2Int3, param2Rect, param2IAutofillWindowPresenter);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2AutofillId;
      }
      
      public void requestHideFillUi(int param2Int, AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.autofill.IAugmentedAutofillManagerClient");
          parcel1.writeInt(param2Int);
          if (param2AutofillId != null) {
            parcel1.writeInt(1);
            param2AutofillId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IAugmentedAutofillManagerClient.Stub.getDefaultImpl() != null) {
            IAugmentedAutofillManagerClient.Stub.getDefaultImpl().requestHideFillUi(param2Int, param2AutofillId);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestAutofill(int param2Int, AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.autofill.IAugmentedAutofillManagerClient");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2AutofillId != null) {
            parcel1.writeInt(1);
            param2AutofillId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IAugmentedAutofillManagerClient.Stub.getDefaultImpl() != null) {
            bool1 = IAugmentedAutofillManagerClient.Stub.getDefaultImpl().requestAutofill(param2Int, param2AutofillId);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAugmentedAutofillManagerClient param1IAugmentedAutofillManagerClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAugmentedAutofillManagerClient != null) {
          Proxy.sDefaultImpl = param1IAugmentedAutofillManagerClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAugmentedAutofillManagerClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
