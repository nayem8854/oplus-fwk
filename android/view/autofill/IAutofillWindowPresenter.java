package android.view.autofill;

import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.WindowManager;

public interface IAutofillWindowPresenter extends IInterface {
  void hide(Rect paramRect) throws RemoteException;
  
  void show(WindowManager.LayoutParams paramLayoutParams, Rect paramRect, boolean paramBoolean, int paramInt) throws RemoteException;
  
  class Default implements IAutofillWindowPresenter {
    public void show(WindowManager.LayoutParams param1LayoutParams, Rect param1Rect, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void hide(Rect param1Rect) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAutofillWindowPresenter {
    private static final String DESCRIPTOR = "android.view.autofill.IAutofillWindowPresenter";
    
    static final int TRANSACTION_hide = 2;
    
    static final int TRANSACTION_show = 1;
    
    public Stub() {
      attachInterface(this, "android.view.autofill.IAutofillWindowPresenter");
    }
    
    public static IAutofillWindowPresenter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.autofill.IAutofillWindowPresenter");
      if (iInterface != null && iInterface instanceof IAutofillWindowPresenter)
        return (IAutofillWindowPresenter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "hide";
      } 
      return "show";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      Rect rect;
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.view.autofill.IAutofillWindowPresenter");
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.autofill.IAutofillWindowPresenter");
        if (param1Parcel1.readInt() != 0) {
          Rect rect1 = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        hide((Rect)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.autofill.IAutofillWindowPresenter");
      if (param1Parcel1.readInt() != 0) {
        WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
      } else {
        rect = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      param1Int1 = param1Parcel1.readInt();
      show((WindowManager.LayoutParams)param1Parcel2, rect, bool, param1Int1);
      return true;
    }
    
    private static class Proxy implements IAutofillWindowPresenter {
      public static IAutofillWindowPresenter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.autofill.IAutofillWindowPresenter";
      }
      
      public void show(WindowManager.LayoutParams param2LayoutParams, Rect param2Rect, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutofillWindowPresenter");
          boolean bool = false;
          if (param2LayoutParams != null) {
            parcel.writeInt(1);
            param2LayoutParams.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IAutofillWindowPresenter.Stub.getDefaultImpl() != null) {
            IAutofillWindowPresenter.Stub.getDefaultImpl().show(param2LayoutParams, param2Rect, param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hide(Rect param2Rect) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutofillWindowPresenter");
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAutofillWindowPresenter.Stub.getDefaultImpl() != null) {
            IAutofillWindowPresenter.Stub.getDefaultImpl().hide(param2Rect);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAutofillWindowPresenter param1IAutofillWindowPresenter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAutofillWindowPresenter != null) {
          Proxy.sDefaultImpl = param1IAutofillWindowPresenter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAutofillWindowPresenter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
