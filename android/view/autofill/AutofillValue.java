package android.view.autofill;

import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.Objects;

public final class AutofillValue implements Parcelable {
  private AutofillValue(int paramInt, Object paramObject) {
    this.mType = paramInt;
    this.mValue = paramObject;
  }
  
  public CharSequence getTextValue() {
    boolean bool = isText();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("value must be a text value, not type=");
    stringBuilder.append(this.mType);
    Preconditions.checkState(bool, stringBuilder.toString());
    return (CharSequence)this.mValue;
  }
  
  public boolean isText() {
    int i = this.mType;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  public boolean getToggleValue() {
    boolean bool = isToggle();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("value must be a toggle value, not type=");
    stringBuilder.append(this.mType);
    Preconditions.checkState(bool, stringBuilder.toString());
    return ((Boolean)this.mValue).booleanValue();
  }
  
  public boolean isToggle() {
    boolean bool;
    if (this.mType == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getListValue() {
    boolean bool = isList();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("value must be a list value, not type=");
    stringBuilder.append(this.mType);
    Preconditions.checkState(bool, stringBuilder.toString());
    return ((Integer)this.mValue).intValue();
  }
  
  public boolean isList() {
    boolean bool;
    if (this.mType == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public long getDateValue() {
    boolean bool = isDate();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("value must be a date value, not type=");
    stringBuilder.append(this.mType);
    Preconditions.checkState(bool, stringBuilder.toString());
    return ((Long)this.mValue).longValue();
  }
  
  public boolean isDate() {
    boolean bool;
    if (this.mType == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (isText() && ((CharSequence)this.mValue).length() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int hashCode() {
    return this.mType + this.mValue.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mType != ((AutofillValue)paramObject).mType)
      return false; 
    if (isText())
      return this.mValue.toString().equals(((AutofillValue)paramObject).mValue.toString()); 
    return Objects.equals(this.mValue, ((AutofillValue)paramObject).mValue);
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[type=");
    stringBuilder.append(this.mType);
    stringBuilder = stringBuilder.append(", value=");
    if (isText()) {
      Helper.appendRedacted(stringBuilder, (CharSequence)this.mValue);
    } else {
      stringBuilder.append(this.mValue);
    } 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramInt = this.mType;
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt == 4)
            paramParcel.writeLong(((Long)this.mValue).longValue()); 
        } else {
          paramParcel.writeInt(((Integer)this.mValue).intValue());
        } 
      } else {
        paramParcel.writeInt(((Boolean)this.mValue).booleanValue());
      } 
    } else {
      paramParcel.writeCharSequence((CharSequence)this.mValue);
    } 
  }
  
  private AutofillValue(Parcel paramParcel) {
    StringBuilder stringBuilder;
    int i = paramParcel.readInt();
    boolean bool = true;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i == 4) {
            this.mValue = Long.valueOf(paramParcel.readLong());
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("type=");
            stringBuilder.append(this.mType);
            stringBuilder.append(" not valid");
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
        } else {
          this.mValue = Integer.valueOf(stringBuilder.readInt());
        } 
      } else {
        i = stringBuilder.readInt();
        if (i == 0)
          bool = false; 
        this.mValue = Boolean.valueOf(bool);
      } 
    } else {
      this.mValue = stringBuilder.readCharSequence();
    } 
  }
  
  public static final Parcelable.Creator<AutofillValue> CREATOR = new Parcelable.Creator<AutofillValue>() {
      public AutofillValue createFromParcel(Parcel param1Parcel) {
        return new AutofillValue(param1Parcel);
      }
      
      public AutofillValue[] newArray(int param1Int) {
        return new AutofillValue[param1Int];
      }
    };
  
  private static final String TAG = "AutofillValue";
  
  private final int mType;
  
  private final Object mValue;
  
  public static AutofillValue forText(CharSequence paramCharSequence) {
    AutofillValue autofillValue;
    if (Helper.sVerbose && !Looper.getMainLooper().isCurrentThread()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("forText() not called on main thread: ");
      stringBuilder.append(Thread.currentThread());
      Log.v("AutofillValue", stringBuilder.toString());
    } 
    if (paramCharSequence == null) {
      paramCharSequence = null;
    } else {
      autofillValue = new AutofillValue(1, TextUtils.trimNoCopySpans(paramCharSequence));
    } 
    return autofillValue;
  }
  
  public static AutofillValue forToggle(boolean paramBoolean) {
    return new AutofillValue(2, Boolean.valueOf(paramBoolean));
  }
  
  public static AutofillValue forList(int paramInt) {
    return new AutofillValue(3, Integer.valueOf(paramInt));
  }
  
  public static AutofillValue forDate(long paramLong) {
    return new AutofillValue(4, Long.valueOf(paramLong));
  }
}
