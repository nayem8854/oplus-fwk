package android.view.autofill;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.os.RemoteException;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowMetrics;
import android.widget.PopupWindow;
import android.window.WindowMetricsHelper;

public class AutofillPopupWindow extends PopupWindow {
  private static final String TAG = "AutofillPopupWindow";
  
  private boolean mFullScreen;
  
  private final View.OnAttachStateChangeListener mOnAttachStateChangeListener = new View.OnAttachStateChangeListener() {
      final AutofillPopupWindow this$0;
      
      public void onViewAttachedToWindow(View param1View) {}
      
      public void onViewDetachedFromWindow(View param1View) {
        AutofillPopupWindow.this.dismiss();
      }
    };
  
  private WindowManager.LayoutParams mWindowLayoutParams;
  
  private final WindowPresenter mWindowPresenter;
  
  public AutofillPopupWindow(IAutofillWindowPresenter paramIAutofillWindowPresenter) {
    this.mWindowPresenter = new WindowPresenter(paramIAutofillWindowPresenter);
    setTouchModal(false);
    setOutsideTouchable(true);
    setInputMethodMode(2);
    setFocusable(true);
  }
  
  protected boolean hasContentView() {
    return true;
  }
  
  protected boolean hasDecorView() {
    return true;
  }
  
  protected WindowManager.LayoutParams getDecorViewLayoutParams() {
    return this.mWindowLayoutParams;
  }
  
  public void update(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Rect paramRect) {
    Object object;
    Rect rect;
    boolean bool;
    int i;
    if (paramInt3 == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mFullScreen = bool;
    if (bool) {
      i = 2008;
    } else {
      i = 1005;
    } 
    setWindowLayoutType(i);
    if (this.mFullScreen) {
      i = 0;
      paramInt1 = 0;
      Context context = paramView.getContext();
      WindowManager windowManager = (WindowManager)context.getSystemService(WindowManager.class);
      WindowMetrics windowMetrics = windowManager.getCurrentWindowMetrics();
      rect = WindowMetricsHelper.getBoundsExcludingNavigationBarAndCutout(windowMetrics);
      paramInt3 = rect.width();
      if (paramInt4 != -1)
        paramInt1 = rect.height() - paramInt4; 
      paramInt2 = paramInt1;
    } else if (rect != null) {
      int[] arrayOfInt = new int[2];
      arrayOfInt[0] = rect.left;
      arrayOfInt[1] = rect.top;
      Object object1 = new Object(this, paramView.getContext(), arrayOfInt, paramView);
      object1.setLeftTopRightBottom(rect.left, rect.top, rect.right, rect.bottom);
      object1.setScrollX(paramView.getScrollX());
      object1.setScrollY(paramView.getScrollY());
      paramView.setOnScrollChangeListener(new _$$Lambda$AutofillPopupWindow$DnLs9aVkSgQ89oSTe4P9EweBBks(arrayOfInt));
      object1.setWillNotDraw(true);
      object = object1;
      i = paramInt1;
    } else {
      i = paramInt1;
    } 
    if (!this.mFullScreen) {
      setAnimationStyle(-1);
    } else if (paramInt4 == -1) {
      setAnimationStyle(0);
    } else {
      setAnimationStyle(16974609);
    } 
    if (!isShowing()) {
      setWidth(paramInt3);
      setHeight(paramInt4);
      showAsDropDown((View)object, i, paramInt2);
    } else {
      update((View)object, i, paramInt2, paramInt3, paramInt4);
    } 
  }
  
  protected void update(View paramView, WindowManager.LayoutParams paramLayoutParams) {
    byte b;
    if (paramView != null) {
      b = paramView.getLayoutDirection();
    } else {
      b = 3;
    } 
    this.mWindowPresenter.show(paramLayoutParams, getTransitionEpicenter(), isLayoutInsetDecor(), b);
  }
  
  protected boolean findDropDownPosition(View paramView, WindowManager.LayoutParams paramLayoutParams, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean) {
    if (this.mFullScreen) {
      paramLayoutParams.x = paramInt1;
      paramLayoutParams.y = paramInt2;
      paramLayoutParams.width = paramInt3;
      paramLayoutParams.height = paramInt4;
      paramLayoutParams.gravity = paramInt5;
      return false;
    } 
    return super.findDropDownPosition(paramView, paramLayoutParams, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramBoolean);
  }
  
  public void showAsDropDown(View paramView, int paramInt1, int paramInt2, int paramInt3) {
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("showAsDropDown(): anchor=");
      stringBuilder.append(paramView);
      stringBuilder.append(", xoff=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", yoff=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(", isShowing(): ");
      stringBuilder.append(isShowing());
      String str = stringBuilder.toString();
      Log.v("AutofillPopupWindow", str);
    } 
    if (isShowing())
      return; 
    setShowing(true);
    setDropDown(true);
    attachToAnchor(paramView, paramInt1, paramInt2, paramInt3);
    IBinder iBinder = paramView.getWindowToken();
    WindowManager.LayoutParams layoutParams = createPopupLayoutParams(iBinder);
    int i = layoutParams.width, j = layoutParams.height;
    boolean bool = getAllowScrollingAnchorParent();
    bool = findDropDownPosition(paramView, layoutParams, paramInt1, paramInt2, i, j, paramInt3, bool);
    updateAboveAnchor(bool);
    layoutParams.accessibilityIdOfAnchor = paramView.getAccessibilityViewId();
    layoutParams.packageName = paramView.getContext().getPackageName();
    WindowPresenter windowPresenter = this.mWindowPresenter;
    Rect rect = getTransitionEpicenter();
    bool = isLayoutInsetDecor();
    paramInt1 = paramView.getLayoutDirection();
    windowPresenter.show(layoutParams, rect, bool, paramInt1);
  }
  
  protected void attachToAnchor(View paramView, int paramInt1, int paramInt2, int paramInt3) {
    super.attachToAnchor(paramView, paramInt1, paramInt2, paramInt3);
    paramView.addOnAttachStateChangeListener(this.mOnAttachStateChangeListener);
  }
  
  protected void detachFromAnchor() {
    View view = getAnchor();
    if (view != null)
      view.removeOnAttachStateChangeListener(this.mOnAttachStateChangeListener); 
    super.detachFromAnchor();
  }
  
  public void dismiss() {
    if (!isShowing() || isTransitioningToDismiss())
      return; 
    setShowing(false);
    setTransitioningToDismiss(true);
    this.mWindowPresenter.hide(getTransitionEpicenter());
    detachFromAnchor();
    if (getOnDismissListener() != null)
      getOnDismissListener().onDismiss(); 
  }
  
  public int getAnimationStyle() {
    throw new IllegalStateException("You can't call this!");
  }
  
  public Drawable getBackground() {
    throw new IllegalStateException("You can't call this!");
  }
  
  public View getContentView() {
    throw new IllegalStateException("You can't call this!");
  }
  
  public float getElevation() {
    throw new IllegalStateException("You can't call this!");
  }
  
  public Transition getEnterTransition() {
    throw new IllegalStateException("You can't call this!");
  }
  
  public Transition getExitTransition() {
    throw new IllegalStateException("You can't call this!");
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable) {
    throw new IllegalStateException("You can't call this!");
  }
  
  public void setContentView(View paramView) {
    if (paramView == null)
      return; 
    throw new IllegalStateException("You can't call this!");
  }
  
  public void setElevation(float paramFloat) {
    throw new IllegalStateException("You can't call this!");
  }
  
  public void setEnterTransition(Transition paramTransition) {
    throw new IllegalStateException("You can't call this!");
  }
  
  public void setExitTransition(Transition paramTransition) {
    throw new IllegalStateException("You can't call this!");
  }
  
  public void setTouchInterceptor(View.OnTouchListener paramOnTouchListener) {
    throw new IllegalStateException("You can't call this!");
  }
  
  class WindowPresenter {
    final IAutofillWindowPresenter mPresenter;
    
    final AutofillPopupWindow this$0;
    
    WindowPresenter(IAutofillWindowPresenter param1IAutofillWindowPresenter) {
      this.mPresenter = param1IAutofillWindowPresenter;
    }
    
    void show(WindowManager.LayoutParams param1LayoutParams, Rect param1Rect, boolean param1Boolean, int param1Int) {
      try {
        this.mPresenter.show(param1LayoutParams, param1Rect, param1Boolean, param1Int);
      } catch (RemoteException remoteException) {
        Log.w("AutofillPopupWindow", "Error showing fill window", (Throwable)remoteException);
        remoteException.rethrowFromSystemServer();
      } 
    }
    
    void hide(Rect param1Rect) {
      try {
        this.mPresenter.hide(param1Rect);
      } catch (RemoteException remoteException) {
        Log.w("AutofillPopupWindow", "Error hiding fill window", (Throwable)remoteException);
        remoteException.rethrowFromSystemServer();
      } 
    }
  }
}
