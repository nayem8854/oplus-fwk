package android.view.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;

class ParcelableMap extends HashMap<AutofillId, AutofillValue> implements Parcelable {
  ParcelableMap(int paramInt) {
    super(paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(size());
    for (Map.Entry<AutofillId, AutofillValue> entry : entrySet()) {
      paramParcel.writeParcelable((Parcelable)entry.getKey(), 0);
      paramParcel.writeParcelable((Parcelable)entry.getValue(), 0);
    } 
  }
  
  public static final Parcelable.Creator<ParcelableMap> CREATOR = new Parcelable.Creator<ParcelableMap>() {
      public ParcelableMap createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        ParcelableMap parcelableMap = new ParcelableMap(i);
        for (byte b = 0; b < i; b++) {
          AutofillId autofillId = (AutofillId)param1Parcel.readParcelable(null);
          AutofillValue autofillValue = (AutofillValue)param1Parcel.readParcelable(null);
          parcelableMap.put(autofillId, autofillValue);
        } 
        return parcelableMap;
      }
      
      public ParcelableMap[] newArray(int param1Int) {
        return new ParcelableMap[param1Int];
      }
    };
}
