package android.view.autofill;

import android.os.Parcel;
import android.os.Parcelable;

public final class AutofillId implements Parcelable {
  public AutofillId(int paramInt) {
    this(0, paramInt, -1L, 0);
  }
  
  public AutofillId(AutofillId paramAutofillId, int paramInt) {
    this(1, paramAutofillId.mViewId, paramInt, 0);
  }
  
  public AutofillId(int paramInt1, int paramInt2) {
    this(1, paramInt1, paramInt2, 0);
  }
  
  public AutofillId(AutofillId paramAutofillId, long paramLong, int paramInt) {
    this(6, paramAutofillId.mViewId, paramLong, paramInt);
  }
  
  private AutofillId(int paramInt1, int paramInt2, long paramLong, int paramInt3) {
    this.mFlags = paramInt1;
    this.mViewId = paramInt2;
    if ((paramInt1 & 0x1) != 0) {
      paramInt2 = (int)paramLong;
    } else {
      paramInt2 = -1;
    } 
    this.mVirtualIntId = paramInt2;
    if ((paramInt1 & 0x2) == 0)
      paramLong = -1L; 
    this.mVirtualLongId = paramLong;
    this.mSessionId = paramInt3;
  }
  
  public static AutofillId withoutSession(AutofillId paramAutofillId) {
    long l;
    int i = paramAutofillId.mFlags;
    if ((i & 0x2) != 0) {
      l = paramAutofillId.mVirtualLongId;
    } else {
      l = paramAutofillId.mVirtualIntId;
    } 
    return new AutofillId(i & 0xFFFFFFFB, paramAutofillId.mViewId, l, 0);
  }
  
  public int getViewId() {
    return this.mViewId;
  }
  
  public int getVirtualChildIntId() {
    return this.mVirtualIntId;
  }
  
  public long getVirtualChildLongId() {
    return this.mVirtualLongId;
  }
  
  public boolean isVirtualInt() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isVirtualLong() {
    boolean bool;
    if ((this.mFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isNonVirtual() {
    boolean bool;
    if (!isVirtualInt() && !isVirtualLong()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasSession() {
    boolean bool;
    if ((this.mFlags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getSessionId() {
    return this.mSessionId;
  }
  
  public void setSessionId(int paramInt) {
    this.mFlags |= 0x4;
    this.mSessionId = paramInt;
  }
  
  public void resetSessionId() {
    this.mFlags &= 0xFFFFFFFB;
    this.mSessionId = 0;
  }
  
  public int hashCode() {
    int i = this.mViewId;
    int j = this.mVirtualIntId;
    long l = this.mVirtualLongId;
    int k = (int)(l ^ l >>> 32L);
    int m = this.mSessionId;
    return (((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mViewId != ((AutofillId)paramObject).mViewId)
      return false; 
    if (this.mVirtualIntId != ((AutofillId)paramObject).mVirtualIntId)
      return false; 
    if (this.mVirtualLongId != ((AutofillId)paramObject).mVirtualLongId)
      return false; 
    if (this.mSessionId != ((AutofillId)paramObject).mSessionId)
      return false; 
    return true;
  }
  
  public boolean equalsIgnoreSession(AutofillId paramAutofillId) {
    if (this == paramAutofillId)
      return true; 
    if (paramAutofillId == null)
      return false; 
    if (this.mViewId != paramAutofillId.mViewId)
      return false; 
    if (this.mVirtualIntId != paramAutofillId.mVirtualIntId)
      return false; 
    if (this.mVirtualLongId != paramAutofillId.mVirtualLongId)
      return false; 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = (new StringBuilder()).append(this.mViewId);
    if (isVirtualInt()) {
      stringBuilder.append(':');
      stringBuilder.append(this.mVirtualIntId);
    } else if (isVirtualLong()) {
      stringBuilder.append(':');
      stringBuilder.append(this.mVirtualLongId);
    } 
    if (hasSession()) {
      stringBuilder.append('@');
      stringBuilder.append(this.mSessionId);
    } 
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mViewId);
    paramParcel.writeInt(this.mFlags);
    if (hasSession())
      paramParcel.writeInt(this.mSessionId); 
    if (isVirtualInt()) {
      paramParcel.writeInt(this.mVirtualIntId);
    } else if (isVirtualLong()) {
      paramParcel.writeLong(this.mVirtualLongId);
    } 
  }
  
  public static final Parcelable.Creator<AutofillId> CREATOR = new Parcelable.Creator<AutofillId>() {
      public AutofillId createFromParcel(Parcel param1Parcel) {
        boolean bool;
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        if ((j & 0x4) != 0) {
          bool = param1Parcel.readInt();
        } else {
          bool = false;
        } 
        if ((j & 0x1) != 0)
          return new AutofillId(j, i, param1Parcel.readInt(), bool); 
        if ((j & 0x2) != 0)
          return new AutofillId(j, i, param1Parcel.readLong(), bool); 
        return new AutofillId(j, i, -1L, bool);
      }
      
      public AutofillId[] newArray(int param1Int) {
        return new AutofillId[param1Int];
      }
    };
  
  private static final int FLAG_HAS_SESSION = 4;
  
  private static final int FLAG_IS_VIRTUAL_INT = 1;
  
  private static final int FLAG_IS_VIRTUAL_LONG = 2;
  
  public static final int NO_SESSION = 0;
  
  private int mFlags;
  
  private int mSessionId;
  
  private final int mViewId;
  
  private final int mVirtualIntId;
  
  private final long mVirtualLongId;
}
