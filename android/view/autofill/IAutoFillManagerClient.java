package android.view.autofill;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.KeyEvent;
import com.android.internal.os.IResultReceiver;
import java.util.ArrayList;
import java.util.List;

public interface IAutoFillManagerClient extends IInterface {
  void authenticate(int paramInt1, int paramInt2, IntentSender paramIntentSender, Intent paramIntent, boolean paramBoolean) throws RemoteException;
  
  void autofill(int paramInt, List<AutofillId> paramList, List<AutofillValue> paramList1, boolean paramBoolean) throws RemoteException;
  
  void dispatchUnhandledKey(int paramInt, AutofillId paramAutofillId, KeyEvent paramKeyEvent) throws RemoteException;
  
  void getAugmentedAutofillClient(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void notifyDisableAutofill(long paramLong, ComponentName paramComponentName) throws RemoteException;
  
  void notifyFillUiHidden(int paramInt, AutofillId paramAutofillId) throws RemoteException;
  
  void notifyFillUiShown(int paramInt, AutofillId paramAutofillId) throws RemoteException;
  
  void notifyNoFillUi(int paramInt1, AutofillId paramAutofillId, int paramInt2) throws RemoteException;
  
  void requestHideFillUi(int paramInt, AutofillId paramAutofillId) throws RemoteException;
  
  void requestShowFillUi(int paramInt1, AutofillId paramAutofillId, int paramInt2, int paramInt3, Rect paramRect, IAutofillWindowPresenter paramIAutofillWindowPresenter) throws RemoteException;
  
  void requestShowSoftInput(AutofillId paramAutofillId) throws RemoteException;
  
  void setSaveUiState(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setSessionFinished(int paramInt, List<AutofillId> paramList) throws RemoteException;
  
  void setState(int paramInt) throws RemoteException;
  
  void setTrackedViews(int paramInt, AutofillId[] paramArrayOfAutofillId1, boolean paramBoolean1, boolean paramBoolean2, AutofillId[] paramArrayOfAutofillId2, AutofillId paramAutofillId) throws RemoteException;
  
  void startIntentSender(IntentSender paramIntentSender, Intent paramIntent) throws RemoteException;
  
  class Default implements IAutoFillManagerClient {
    public void setState(int param1Int) throws RemoteException {}
    
    public void autofill(int param1Int, List<AutofillId> param1List, List<AutofillValue> param1List1, boolean param1Boolean) throws RemoteException {}
    
    public void authenticate(int param1Int1, int param1Int2, IntentSender param1IntentSender, Intent param1Intent, boolean param1Boolean) throws RemoteException {}
    
    public void setTrackedViews(int param1Int, AutofillId[] param1ArrayOfAutofillId1, boolean param1Boolean1, boolean param1Boolean2, AutofillId[] param1ArrayOfAutofillId2, AutofillId param1AutofillId) throws RemoteException {}
    
    public void requestShowFillUi(int param1Int1, AutofillId param1AutofillId, int param1Int2, int param1Int3, Rect param1Rect, IAutofillWindowPresenter param1IAutofillWindowPresenter) throws RemoteException {}
    
    public void requestHideFillUi(int param1Int, AutofillId param1AutofillId) throws RemoteException {}
    
    public void notifyNoFillUi(int param1Int1, AutofillId param1AutofillId, int param1Int2) throws RemoteException {}
    
    public void notifyFillUiShown(int param1Int, AutofillId param1AutofillId) throws RemoteException {}
    
    public void notifyFillUiHidden(int param1Int, AutofillId param1AutofillId) throws RemoteException {}
    
    public void dispatchUnhandledKey(int param1Int, AutofillId param1AutofillId, KeyEvent param1KeyEvent) throws RemoteException {}
    
    public void startIntentSender(IntentSender param1IntentSender, Intent param1Intent) throws RemoteException {}
    
    public void setSaveUiState(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setSessionFinished(int param1Int, List<AutofillId> param1List) throws RemoteException {}
    
    public void getAugmentedAutofillClient(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void notifyDisableAutofill(long param1Long, ComponentName param1ComponentName) throws RemoteException {}
    
    public void requestShowSoftInput(AutofillId param1AutofillId) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAutoFillManagerClient {
    private static final String DESCRIPTOR = "android.view.autofill.IAutoFillManagerClient";
    
    static final int TRANSACTION_authenticate = 3;
    
    static final int TRANSACTION_autofill = 2;
    
    static final int TRANSACTION_dispatchUnhandledKey = 10;
    
    static final int TRANSACTION_getAugmentedAutofillClient = 14;
    
    static final int TRANSACTION_notifyDisableAutofill = 15;
    
    static final int TRANSACTION_notifyFillUiHidden = 9;
    
    static final int TRANSACTION_notifyFillUiShown = 8;
    
    static final int TRANSACTION_notifyNoFillUi = 7;
    
    static final int TRANSACTION_requestHideFillUi = 6;
    
    static final int TRANSACTION_requestShowFillUi = 5;
    
    static final int TRANSACTION_requestShowSoftInput = 16;
    
    static final int TRANSACTION_setSaveUiState = 12;
    
    static final int TRANSACTION_setSessionFinished = 13;
    
    static final int TRANSACTION_setState = 1;
    
    static final int TRANSACTION_setTrackedViews = 4;
    
    static final int TRANSACTION_startIntentSender = 11;
    
    public Stub() {
      attachInterface(this, "android.view.autofill.IAutoFillManagerClient");
    }
    
    public static IAutoFillManagerClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.autofill.IAutoFillManagerClient");
      if (iInterface != null && iInterface instanceof IAutoFillManagerClient)
        return (IAutoFillManagerClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 16:
          return "requestShowSoftInput";
        case 15:
          return "notifyDisableAutofill";
        case 14:
          return "getAugmentedAutofillClient";
        case 13:
          return "setSessionFinished";
        case 12:
          return "setSaveUiState";
        case 11:
          return "startIntentSender";
        case 10:
          return "dispatchUnhandledKey";
        case 9:
          return "notifyFillUiHidden";
        case 8:
          return "notifyFillUiShown";
        case 7:
          return "notifyNoFillUi";
        case 6:
          return "requestHideFillUi";
        case 5:
          return "requestShowFillUi";
        case 4:
          return "setTrackedViews";
        case 3:
          return "authenticate";
        case 2:
          return "autofill";
        case 1:
          break;
      } 
      return "setState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<AutofillValue> arrayList;
      if (param1Int1 != 1598968902) {
        IResultReceiver iResultReceiver;
        ArrayList<AutofillId> arrayList1;
        IAutofillWindowPresenter iAutofillWindowPresenter;
        AutofillId[] arrayOfAutofillId1;
        long l;
        int i;
        Rect rect;
        AutofillId[] arrayOfAutofillId2;
        ArrayList<AutofillId> arrayList2;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 16:
            param1Parcel1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            if (param1Parcel1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            requestShowSoftInput((AutofillId)param1Parcel1);
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            l = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyDisableAutofill(l, (ComponentName)param1Parcel1);
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            iResultReceiver = IResultReceiver.Stub.asInterface(param1Parcel1.readStrongBinder());
            getAugmentedAutofillClient(iResultReceiver);
            return true;
          case 13:
            iResultReceiver.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int1 = iResultReceiver.readInt();
            arrayList1 = iResultReceiver.createTypedArrayList(AutofillId.CREATOR);
            setSessionFinished(param1Int1, arrayList1);
            return true;
          case 12:
            arrayList1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int1 = arrayList1.readInt();
            bool1 = bool2;
            if (arrayList1.readInt() != 0)
              bool1 = true; 
            setSaveUiState(param1Int1, bool1);
            return true;
          case 11:
            arrayList1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            if (arrayList1.readInt() != 0) {
              IntentSender intentSender = (IntentSender)IntentSender.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              param1Parcel2 = null;
            } 
            if (arrayList1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              arrayList1 = null;
            } 
            startIntentSender((IntentSender)param1Parcel2, (Intent)arrayList1);
            return true;
          case 10:
            arrayList1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int1 = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              param1Parcel2 = null;
            } 
            if (arrayList1.readInt() != 0) {
              KeyEvent keyEvent = (KeyEvent)KeyEvent.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              arrayList1 = null;
            } 
            dispatchUnhandledKey(param1Int1, (AutofillId)param1Parcel2, (KeyEvent)arrayList1);
            return true;
          case 9:
            arrayList1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int1 = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              arrayList1 = null;
            } 
            notifyFillUiHidden(param1Int1, (AutofillId)arrayList1);
            return true;
          case 8:
            arrayList1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int1 = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              arrayList1 = null;
            } 
            notifyFillUiShown(param1Int1, (AutofillId)arrayList1);
            return true;
          case 7:
            arrayList1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int2 = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = arrayList1.readInt();
            notifyNoFillUi(param1Int2, (AutofillId)param1Parcel2, param1Int1);
            return true;
          case 6:
            arrayList1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int1 = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              arrayList1 = null;
            } 
            requestHideFillUi(param1Int1, (AutofillId)arrayList1);
            return true;
          case 5:
            arrayList1.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            i = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int2 = arrayList1.readInt();
            param1Int1 = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              rect = null;
            } 
            iAutofillWindowPresenter = IAutofillWindowPresenter.Stub.asInterface(arrayList1.readStrongBinder());
            requestShowFillUi(i, (AutofillId)param1Parcel2, param1Int2, param1Int1, rect, iAutofillWindowPresenter);
            return true;
          case 4:
            iAutofillWindowPresenter.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int1 = iAutofillWindowPresenter.readInt();
            arrayOfAutofillId1 = (AutofillId[])iAutofillWindowPresenter.createTypedArray(AutofillId.CREATOR);
            if (iAutofillWindowPresenter.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (iAutofillWindowPresenter.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            arrayOfAutofillId2 = (AutofillId[])iAutofillWindowPresenter.createTypedArray(AutofillId.CREATOR);
            if (iAutofillWindowPresenter.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)iAutofillWindowPresenter);
            } else {
              iAutofillWindowPresenter = null;
            } 
            setTrackedViews(param1Int1, arrayOfAutofillId1, bool1, bool2, arrayOfAutofillId2, (AutofillId)iAutofillWindowPresenter);
            return true;
          case 3:
            iAutofillWindowPresenter.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int2 = iAutofillWindowPresenter.readInt();
            param1Int1 = iAutofillWindowPresenter.readInt();
            if (iAutofillWindowPresenter.readInt() != 0) {
              IntentSender intentSender = (IntentSender)IntentSender.CREATOR.createFromParcel((Parcel)iAutofillWindowPresenter);
            } else {
              arrayOfAutofillId1 = null;
            } 
            if (iAutofillWindowPresenter.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iAutofillWindowPresenter);
            } else {
              arrayOfAutofillId2 = null;
            } 
            if (iAutofillWindowPresenter.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            authenticate(param1Int2, param1Int1, (IntentSender)arrayOfAutofillId1, (Intent)arrayOfAutofillId2, bool1);
            return true;
          case 2:
            iAutofillWindowPresenter.enforceInterface("android.view.autofill.IAutoFillManagerClient");
            param1Int1 = iAutofillWindowPresenter.readInt();
            arrayList2 = iAutofillWindowPresenter.createTypedArrayList(AutofillId.CREATOR);
            arrayList = iAutofillWindowPresenter.createTypedArrayList(AutofillValue.CREATOR);
            if (iAutofillWindowPresenter.readInt() != 0)
              bool1 = true; 
            autofill(param1Int1, arrayList2, arrayList, bool1);
            return true;
          case 1:
            break;
        } 
        iAutofillWindowPresenter.enforceInterface("android.view.autofill.IAutoFillManagerClient");
        param1Int1 = iAutofillWindowPresenter.readInt();
        setState(param1Int1);
        return true;
      } 
      arrayList.writeString("android.view.autofill.IAutoFillManagerClient");
      return true;
    }
    
    private static class Proxy implements IAutoFillManagerClient {
      public static IAutoFillManagerClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.autofill.IAutoFillManagerClient";
      }
      
      public void setState(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().setState(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void autofill(int param2Int, List<AutofillId> param2List, List<AutofillValue> param2List1, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int);
          parcel.writeTypedList(param2List);
          parcel.writeTypedList(param2List1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, null, 1);
          if (!bool1 && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().autofill(param2Int, param2List, param2List1, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void authenticate(int param2Int1, int param2Int2, IntentSender param2IntentSender, Intent param2Intent, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = false;
          if (param2IntentSender != null) {
            parcel.writeInt(1);
            param2IntentSender.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().authenticate(param2Int1, param2Int2, param2IntentSender, param2Intent, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setTrackedViews(int param2Int, AutofillId[] param2ArrayOfAutofillId1, boolean param2Boolean1, boolean param2Boolean2, AutofillId[] param2ArrayOfAutofillId2, AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          try {
            parcel.writeInt(param2Int);
            try {
              boolean bool;
              parcel.writeTypedArray((Parcelable[])param2ArrayOfAutofillId1, 0);
              if (param2Boolean1) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel.writeInt(bool);
              if (param2Boolean2) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel.writeInt(bool);
              try {
                parcel.writeTypedArray((Parcelable[])param2ArrayOfAutofillId2, 0);
                if (param2AutofillId != null) {
                  parcel.writeInt(1);
                  param2AutofillId.writeToParcel(parcel, 0);
                } else {
                  parcel.writeInt(0);
                } 
                try {
                  boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
                  if (!bool1 && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
                    IAutoFillManagerClient.Stub.getDefaultImpl().setTrackedViews(param2Int, param2ArrayOfAutofillId1, param2Boolean1, param2Boolean2, param2ArrayOfAutofillId2, param2AutofillId);
                    parcel.recycle();
                    return;
                  } 
                  parcel.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2ArrayOfAutofillId1;
      }
      
      public void requestShowFillUi(int param2Int1, AutofillId param2AutofillId, int param2Int2, int param2Int3, Rect param2Rect, IAutofillWindowPresenter param2IAutofillWindowPresenter) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          try {
            parcel.writeInt(param2Int1);
            if (param2AutofillId != null) {
              parcel.writeInt(1);
              param2AutofillId.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            try {
              parcel.writeInt(param2Int2);
              try {
                IBinder iBinder;
                parcel.writeInt(param2Int3);
                if (param2Rect != null) {
                  parcel.writeInt(1);
                  param2Rect.writeToParcel(parcel, 0);
                } else {
                  parcel.writeInt(0);
                } 
                if (param2IAutofillWindowPresenter != null) {
                  iBinder = param2IAutofillWindowPresenter.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel.writeStrongBinder(iBinder);
                try {
                  boolean bool = this.mRemote.transact(5, parcel, null, 1);
                  if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
                    IAutoFillManagerClient.Stub.getDefaultImpl().requestShowFillUi(param2Int1, param2AutofillId, param2Int2, param2Int3, param2Rect, param2IAutofillWindowPresenter);
                    parcel.recycle();
                    return;
                  } 
                  parcel.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2AutofillId;
      }
      
      public void requestHideFillUi(int param2Int, AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int);
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().requestHideFillUi(param2Int, param2AutofillId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyNoFillUi(int param2Int1, AutofillId param2AutofillId, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int1);
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().notifyNoFillUi(param2Int1, param2AutofillId, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyFillUiShown(int param2Int, AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int);
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().notifyFillUiShown(param2Int, param2AutofillId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyFillUiHidden(int param2Int, AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int);
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().notifyFillUiHidden(param2Int, param2AutofillId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchUnhandledKey(int param2Int, AutofillId param2AutofillId, KeyEvent param2KeyEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int);
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2KeyEvent != null) {
            parcel.writeInt(1);
            param2KeyEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().dispatchUnhandledKey(param2Int, param2AutofillId, param2KeyEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startIntentSender(IntentSender param2IntentSender, Intent param2Intent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          if (param2IntentSender != null) {
            parcel.writeInt(1);
            param2IntentSender.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().startIntentSender(param2IntentSender, param2Intent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSaveUiState(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel, null, 1);
          if (!bool1 && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().setSaveUiState(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSessionFinished(int param2Int, List<AutofillId> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeInt(param2Int);
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().setSessionFinished(param2Int, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getAugmentedAutofillClient(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().getAugmentedAutofillClient(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyDisableAutofill(long param2Long, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          parcel.writeLong(param2Long);
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().notifyDisableAutofill(param2Long, param2ComponentName);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestShowSoftInput(AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManagerClient");
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IAutoFillManagerClient.Stub.getDefaultImpl() != null) {
            IAutoFillManagerClient.Stub.getDefaultImpl().requestShowSoftInput(param2AutofillId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAutoFillManagerClient param1IAutoFillManagerClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAutoFillManagerClient != null) {
          Proxy.sDefaultImpl = param1IAutoFillManagerClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAutoFillManagerClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
