package android.view.autofill;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.annotation.SystemApi;
import android.content.AutofillOptions;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ResolveInfo;
import android.graphics.Rect;
import android.metrics.LogMaker;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.SystemClock;
import android.service.autofill.FillEventHistory;
import android.service.autofill.UserData;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;
import android.util.Slog;
import android.view.Choreographer;
import android.view.KeyEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.os.IResultReceiver;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import com.android.internal.util.SyncResultReceiver;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.xmlpull.v1.XmlPullParserException;
import sun.misc.Cleaner;

public final class AutofillManager {
  static {
    boolean bool;
    if (Build.IS_DEBUGGABLE) {
      bool = true;
    } else {
      bool = false;
    } 
    DEFAULT_LOGGING_LEVEL = bool;
  }
  
  public static int makeAuthenticationId(int paramInt1, int paramInt2) {
    return paramInt1 << 16 | 0xFFFF & paramInt2;
  }
  
  public static int getRequestIdFromAuthenticationId(int paramInt) {
    return paramInt >> 16;
  }
  
  public static int getDatasetIdFromAuthenticationId(int paramInt) {
    return 0xFFFF & paramInt;
  }
  
  private final MetricsLogger mMetricsLogger = new MetricsLogger();
  
  private final Object mLock = new Object();
  
  private int mSessionId = Integer.MAX_VALUE;
  
  public static final int ACTION_RESPONSE_EXPIRED = 5;
  
  public static final int ACTION_START_SESSION = 1;
  
  public static final int ACTION_VALUE_CHANGED = 4;
  
  public static final int ACTION_VIEW_ENTERED = 2;
  
  public static final int ACTION_VIEW_EXITED = 3;
  
  private static final int AUTHENTICATION_ID_DATASET_ID_MASK = 65535;
  
  private static final int AUTHENTICATION_ID_DATASET_ID_SHIFT = 16;
  
  public static final int AUTHENTICATION_ID_DATASET_ID_UNDEFINED = 65535;
  
  public static final int DEFAULT_LOGGING_LEVEL;
  
  public static final int DEFAULT_MAX_PARTITIONS_SIZE = 10;
  
  public static final String DEVICE_CONFIG_AUGMENTED_SERVICE_IDLE_UNBIND_TIMEOUT = "augmented_service_idle_unbind_timeout";
  
  public static final String DEVICE_CONFIG_AUGMENTED_SERVICE_REQUEST_TIMEOUT = "augmented_service_request_timeout";
  
  public static final String DEVICE_CONFIG_AUTOFILL_SMART_SUGGESTION_SUPPORTED_MODES = "smart_suggestion_supported_modes";
  
  public static final String EXTRA_ASSIST_STRUCTURE = "android.view.autofill.extra.ASSIST_STRUCTURE";
  
  public static final String EXTRA_AUGMENTED_AUTOFILL_CLIENT = "android.view.autofill.extra.AUGMENTED_AUTOFILL_CLIENT";
  
  public static final String EXTRA_AUTHENTICATION_RESULT = "android.view.autofill.extra.AUTHENTICATION_RESULT";
  
  public static final String EXTRA_CLIENT_STATE = "android.view.autofill.extra.CLIENT_STATE";
  
  public static final String EXTRA_RESTORE_CROSS_ACTIVITY = "android.view.autofill.extra.RESTORE_CROSS_ACTIVITY";
  
  public static final String EXTRA_RESTORE_SESSION_TOKEN = "android.view.autofill.extra.RESTORE_SESSION_TOKEN";
  
  public static final int FC_SERVICE_TIMEOUT = 5000;
  
  public static final int FLAG_ADD_CLIENT_DEBUG = 2;
  
  public static final int FLAG_ADD_CLIENT_ENABLED = 1;
  
  public static final int FLAG_ADD_CLIENT_ENABLED_FOR_AUGMENTED_AUTOFILL_ONLY = 8;
  
  public static final int FLAG_ADD_CLIENT_VERBOSE = 4;
  
  public static final int FLAG_SMART_SUGGESTION_OFF = 0;
  
  public static final int FLAG_SMART_SUGGESTION_SYSTEM = 1;
  
  private static final String LAST_AUTOFILLED_DATA_TAG = "android:lastAutoFilledData";
  
  public static final int MAX_TEMP_AUGMENTED_SERVICE_DURATION_MS = 120000;
  
  public static final int NO_LOGGING = 0;
  
  public static final int NO_SESSION = 2147483647;
  
  public static final int PENDING_UI_OPERATION_CANCEL = 1;
  
  public static final int PENDING_UI_OPERATION_RESTORE = 2;
  
  public static final int RECEIVER_FLAG_SESSION_FOR_AUGMENTED_AUTOFILL_ONLY = 1;
  
  public static final int RESULT_CODE_NOT_SERVICE = -1;
  
  public static final int RESULT_OK = 0;
  
  private static final String SESSION_ID_TAG = "android:sessionId";
  
  public static final int SET_STATE_FLAG_DEBUG = 8;
  
  public static final int SET_STATE_FLAG_ENABLED = 1;
  
  public static final int SET_STATE_FLAG_FOR_AUTOFILL_ONLY = 32;
  
  public static final int SET_STATE_FLAG_RESET_CLIENT = 4;
  
  public static final int SET_STATE_FLAG_RESET_SESSION = 2;
  
  public static final int SET_STATE_FLAG_VERBOSE = 16;
  
  public static final int STATE_ACTIVE = 1;
  
  public static final int STATE_DISABLED_BY_SERVICE = 4;
  
  public static final int STATE_FINISHED = 2;
  
  public static final int STATE_SHOWING_SAVE_UI = 3;
  
  private static final String STATE_TAG = "android:state";
  
  public static final int STATE_UNKNOWN = 0;
  
  public static final int STATE_UNKNOWN_COMPAT_MODE = 5;
  
  public static final int STATE_UNKNOWN_FAILED = 6;
  
  private static final int SYNC_CALLS_TIMEOUT_MS = 5000;
  
  private static final String TAG = "AutofillManager";
  
  private IAugmentedAutofillManagerClient mAugmentedAutofillServiceClient;
  
  private AutofillCallback mCallback;
  
  private CompatibilityBridge mCompatibilityBridge;
  
  private final Context mContext;
  
  private boolean mEnabled;
  
  private boolean mEnabledForAugmentedAutofillOnly;
  
  private Set<AutofillId> mEnteredForAugmentedAutofillIds;
  
  private ArraySet<AutofillId> mEnteredIds;
  
  private ArraySet<AutofillId> mFillableIds;
  
  private boolean mForAugmentedAutofillOnly;
  
  private AutofillId mIdShownFillUi;
  
  private ParcelableMap mLastAutofilledData;
  
  private boolean mOnInvisibleCalled;
  
  private final AutofillOptions mOptions;
  
  private boolean mSaveOnFinish;
  
  private AutofillId mSaveTriggerId;
  
  private final IAutoFillManager mService;
  
  private IAutoFillManagerClient mServiceClient;
  
  private Cleaner mServiceClientCleaner;
  
  private int mState;
  
  private TrackedViews mTrackedViews;
  
  public AutofillManager(Context paramContext, IAutoFillManager paramIAutoFillManager) {
    boolean bool = false;
    this.mState = 0;
    this.mContext = (Context)Preconditions.checkNotNull(paramContext, "context cannot be null");
    this.mService = paramIAutoFillManager;
    AutofillOptions autofillOptions = paramContext.getAutofillOptions();
    if (autofillOptions != null) {
      if ((autofillOptions.loggingLevel & 0x2) != 0) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      Helper.sDebug = bool1;
      boolean bool1 = bool;
      if ((this.mOptions.loggingLevel & 0x4) != 0)
        bool1 = true; 
      Helper.sVerbose = bool1;
    } 
  }
  
  public void enableCompatibilityMode() {
    synchronized (this.mLock) {
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("creating CompatibilityBridge for ");
        stringBuilder.append(this.mContext);
        Slog.d("AutofillManager", stringBuilder.toString());
      } 
      CompatibilityBridge compatibilityBridge = new CompatibilityBridge();
      this(this);
      this.mCompatibilityBridge = compatibilityBridge;
      return;
    } 
  }
  
  public void onCreate(Bundle paramBundle) {
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      this.mLastAutofilledData = (ParcelableMap)paramBundle.getParcelable("android:lastAutoFilledData");
      if (isActiveLocked()) {
        Log.w("AutofillManager", "New session was started before onCreate()");
        return;
      } 
      this.mSessionId = paramBundle.getInt("android:sessionId", 2147483647);
      this.mState = paramBundle.getInt("android:state", 0);
      if (this.mSessionId != Integer.MAX_VALUE) {
        ensureServiceClientAddedIfNeededLocked();
        AutofillClient autofillClient = getClient();
        if (autofillClient != null) {
          SyncResultReceiver syncResultReceiver = new SyncResultReceiver();
          this(5000);
          try {
            StringBuilder stringBuilder;
            IAutoFillManager iAutoFillManager = this.mService;
            int i = this.mSessionId;
            IBinder iBinder1 = autofillClient.autofillClientGetActivityToken();
            IAutoFillManagerClient iAutoFillManagerClient = this.mServiceClient;
            IBinder iBinder2 = iAutoFillManagerClient.asBinder();
            iAutoFillManager.restoreSession(i, iBinder1, iBinder2, (IResultReceiver)syncResultReceiver);
            int j = syncResultReceiver.getIntResult();
            i = 1;
            if (j != 1)
              i = 0; 
            if (i == 0) {
              stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Session ");
              stringBuilder.append(this.mSessionId);
              stringBuilder.append(" could not be restored");
              Log.w("AutofillManager", stringBuilder.toString());
              this.mSessionId = Integer.MAX_VALUE;
              this.mState = 0;
            } else {
              if (Helper.sDebug) {
                StringBuilder stringBuilder1 = new StringBuilder();
                this();
                stringBuilder1.append("session ");
                stringBuilder1.append(this.mSessionId);
                stringBuilder1.append(" was restored");
                Log.d("AutofillManager", stringBuilder1.toString());
              } 
              stringBuilder.autofillClientResetableStateAvailable();
            } 
          } catch (RemoteException remoteException) {
            Log.e("AutofillManager", "Could not figure out if there was an autofill session", (Throwable)remoteException);
          } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Fail to get session restore status: ");
            stringBuilder.append(timeoutException);
            Log.e("AutofillManager", stringBuilder.toString());
          } 
        } 
      } 
      return;
    } 
  }
  
  public void onVisibleForAutofill() {
    Choreographer.getInstance().postCallback(4, new _$$Lambda$AutofillManager$YfpJNFodEuj5lbXfPlc77fsEvC8(this), null);
  }
  
  public void onInvisibleForAutofill(boolean paramBoolean) {
    synchronized (this.mLock) {
      this.mOnInvisibleCalled = true;
      if (paramBoolean)
        updateSessionLocked(null, null, null, 5, 0); 
      return;
    } 
  }
  
  public void onSaveInstanceState(Bundle paramBundle) {
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      if (this.mSessionId != Integer.MAX_VALUE)
        paramBundle.putInt("android:sessionId", this.mSessionId); 
      if (this.mState != 0)
        paramBundle.putInt("android:state", this.mState); 
      if (this.mLastAutofilledData != null)
        paramBundle.putParcelable("android:lastAutoFilledData", this.mLastAutofilledData); 
      return;
    } 
  }
  
  public boolean isCompatibilityModeEnabledLocked() {
    boolean bool;
    if (this.mCompatibilityBridge != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isEnabled() {
    if (!hasAutofillFeature())
      return false; 
    synchronized (this.mLock) {
      if (isDisabledByServiceLocked())
        return false; 
      ensureServiceClientAddedIfNeededLocked();
      return this.mEnabled;
    } 
  }
  
  public FillEventHistory getFillEventHistory() {
    try {
      SyncResultReceiver syncResultReceiver = new SyncResultReceiver();
      this(5000);
      this.mService.getFillEventHistory((IResultReceiver)syncResultReceiver);
      return (FillEventHistory)syncResultReceiver.getParcelableResult();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fail to get fill event history: ");
      stringBuilder.append(timeoutException);
      Log.e("AutofillManager", stringBuilder.toString());
      return null;
    } 
  }
  
  public void requestAutofill(View paramView) {
    int i = 1;
    if (!paramView.isFocused())
      i = 0x1 | 0x10; 
    notifyViewEntered(paramView, i);
  }
  
  void requestAutofillFromNewSession(View paramView) {
    cancel();
    notifyViewEntered(paramView);
  }
  
  public void requestAutofill(View paramView, int paramInt, Rect paramRect) {
    int i = 1;
    if (!paramView.isFocused())
      i = 0x1 | 0x10; 
    notifyViewEntered(paramView, paramInt, paramRect, i);
  }
  
  public void notifyViewEntered(View paramView) {
    notifyViewEntered(paramView, 0);
  }
  
  private boolean shouldIgnoreViewEnteredLocked(AutofillId paramAutofillId, int paramInt) {
    String str;
    if (isDisabledByServiceLocked()) {
      if (Helper.sVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ignoring notifyViewEntered(flags=");
        stringBuilder.append(paramInt);
        stringBuilder.append(", view=");
        stringBuilder.append(paramAutofillId);
        stringBuilder.append(") on state ");
        stringBuilder.append(getStateAsStringLocked());
        stringBuilder.append(" because disabled by svc");
        str = stringBuilder.toString();
        Log.v("AutofillManager", str);
      } 
      return true;
    } 
    if (isFinishedLocked())
      if ((paramInt & 0x1) == 0) {
        ArraySet<AutofillId> arraySet = this.mEnteredIds;
        if (arraySet != null && 
          arraySet.contains(str)) {
          if (Helper.sVerbose) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ignoring notifyViewEntered(flags=");
            stringBuilder.append(paramInt);
            stringBuilder.append(", view=");
            stringBuilder.append(str);
            stringBuilder.append(") on state ");
            stringBuilder.append(getStateAsStringLocked());
            stringBuilder.append(" because view was already entered: ");
            stringBuilder.append(this.mEnteredIds);
            str = stringBuilder.toString();
            Log.v("AutofillManager", str);
          } 
          return true;
        } 
      }  
    return false;
  }
  
  private boolean isClientVisibleForAutofillLocked() {
    boolean bool;
    AutofillClient autofillClient = getClient();
    if (autofillClient != null && autofillClient.autofillClientIsVisibleForAutofill()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isClientDisablingEnterExitEvent() {
    boolean bool;
    AutofillClient autofillClient = getClient();
    if (autofillClient != null && autofillClient.isDisablingEnterExitEventForAutofill()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void notifyViewEntered(View paramView, int paramInt) {
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      AutofillCallback autofillCallback = notifyViewEnteredLocked(paramView, paramInt);
      if (autofillCallback != null)
        this.mCallback.onAutofillEvent(paramView, 3); 
      return;
    } 
  }
  
  private AutofillCallback notifyViewEnteredLocked(View paramView, int paramInt) {
    StringBuilder stringBuilder;
    AutofillCallback autofillCallback2;
    AutofillId autofillId = paramView.getAutofillId();
    if (shouldIgnoreViewEnteredLocked(autofillId, paramInt))
      return null; 
    AutofillCallback autofillCallback1 = null;
    ensureServiceClientAddedIfNeededLocked();
    if (!this.mEnabled && !this.mEnabledForAugmentedAutofillOnly) {
      if (Helper.sVerbose) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("ignoring notifyViewEntered(");
        stringBuilder.append(autofillId);
        stringBuilder.append("): disabled");
        Log.v("AutofillManager", stringBuilder.toString());
      } 
      autofillCallback2 = autofillCallback1;
      if (this.mCallback != null)
        autofillCallback2 = this.mCallback; 
    } else {
      autofillCallback2 = autofillCallback1;
      if (!isClientDisablingEnterExitEvent()) {
        AutofillValue autofillValue = stringBuilder.getAutofillValue();
        int i = paramInt;
        if (stringBuilder instanceof TextView) {
          i = paramInt;
          if (((TextView)stringBuilder).isAnyPasswordInputType())
            i = paramInt | 0x4; 
        } 
        if (!isActiveLocked()) {
          startSessionLocked(autofillId, null, autofillValue, i);
        } else {
          if (this.mForAugmentedAutofillOnly && (i & 0x1) != 0) {
            if (Helper.sDebug) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("notifyViewEntered(");
              stringBuilder.append(autofillId);
              stringBuilder.append("): resetting mForAugmentedAutofillOnly on manual request");
              Log.d("AutofillManager", stringBuilder.toString());
            } 
            this.mForAugmentedAutofillOnly = false;
          } 
          updateSessionLocked(autofillId, null, autofillValue, 2, i);
        } 
        addEnteredIdLocked(autofillId);
        autofillCallback2 = autofillCallback1;
      } 
    } 
    return autofillCallback2;
  }
  
  public void notifyViewExited(View paramView) {
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      notifyViewExitedLocked(paramView);
      return;
    } 
  }
  
  void notifyViewExitedLocked(View paramView) {
    ensureServiceClientAddedIfNeededLocked();
    if ((this.mEnabled || this.mEnabledForAugmentedAutofillOnly) && isActiveLocked())
      if (!isClientDisablingEnterExitEvent()) {
        AutofillId autofillId = paramView.getAutofillId();
        updateSessionLocked(autofillId, null, null, 3, 0);
      }  
  }
  
  public void notifyViewVisibilityChanged(View paramView, boolean paramBoolean) {
    notifyViewVisibilityChangedInternal(paramView, 0, paramBoolean, false);
  }
  
  public void notifyViewVisibilityChanged(View paramView, int paramInt, boolean paramBoolean) {
    notifyViewVisibilityChangedInternal(paramView, paramInt, paramBoolean, true);
  }
  
  private void notifyViewVisibilityChangedInternal(View paramView, int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    synchronized (this.mLock) {
      StringBuilder stringBuilder;
      if (this.mForAugmentedAutofillOnly) {
        if (Helper.sVerbose)
          Log.v("AutofillManager", "notifyViewVisibilityChanged(): ignoring on augmented only mode"); 
        return;
      } 
      if (this.mEnabled && isActiveLocked()) {
        AutofillId autofillId;
        if (paramBoolean2) {
          autofillId = getAutofillId(paramView, paramInt);
        } else {
          autofillId = paramView.getAutofillId();
        } 
        if (Helper.sVerbose) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("visibility changed for ");
          stringBuilder1.append(autofillId);
          stringBuilder1.append(": ");
          stringBuilder1.append(paramBoolean1);
          Log.v("AutofillManager", stringBuilder1.toString());
        } 
        if (!paramBoolean1 && this.mFillableIds != null && 
          this.mFillableIds.contains(autofillId)) {
          if (Helper.sDebug) {
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("Hidding UI when view ");
            stringBuilder1.append(autofillId);
            stringBuilder1.append(" became invisible");
            Log.d("AutofillManager", stringBuilder1.toString());
          } 
          requestHideFillUi(autofillId, paramView);
        } 
        if (this.mTrackedViews != null) {
          this.mTrackedViews.notifyViewVisibilityChangedLocked(autofillId, paramBoolean1);
        } else if (Helper.sVerbose) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Ignoring visibility change on ");
          stringBuilder.append(autofillId);
          stringBuilder.append(": no tracked views");
          Log.v("AutofillManager", stringBuilder.toString());
        } 
      } else if (!paramBoolean2 && paramBoolean1) {
        startAutofillIfNeededLocked((View)stringBuilder);
      } 
      return;
    } 
  }
  
  public void notifyViewEntered(View paramView, int paramInt, Rect paramRect) {
    notifyViewEntered(paramView, paramInt, paramRect, 0);
  }
  
  private void notifyViewEntered(View paramView, int paramInt1, Rect paramRect, int paramInt2) {
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      AutofillCallback autofillCallback = notifyViewEnteredLocked(paramView, paramInt1, paramRect, paramInt2);
      if (autofillCallback != null)
        autofillCallback.onAutofillEvent(paramView, paramInt1, 3); 
      return;
    } 
  }
  
  private AutofillCallback notifyViewEnteredLocked(View paramView, int paramInt1, Rect paramRect, int paramInt2) {
    StringBuilder stringBuilder;
    AutofillCallback autofillCallback2;
    AutofillId autofillId = getAutofillId(paramView, paramInt1);
    AutofillCallback autofillCallback1 = null;
    if (shouldIgnoreViewEnteredLocked(autofillId, paramInt2))
      return null; 
    ensureServiceClientAddedIfNeededLocked();
    if (!this.mEnabled && !this.mEnabledForAugmentedAutofillOnly) {
      if (Helper.sVerbose) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("ignoring notifyViewEntered(");
        stringBuilder.append(autofillId);
        stringBuilder.append("): disabled");
        Log.v("AutofillManager", stringBuilder.toString());
      } 
      autofillCallback2 = autofillCallback1;
      if (this.mCallback != null)
        autofillCallback2 = this.mCallback; 
    } else {
      autofillCallback2 = autofillCallback1;
      if (!isClientDisablingEnterExitEvent()) {
        paramInt1 = paramInt2;
        if (stringBuilder instanceof TextView) {
          paramInt1 = paramInt2;
          if (((TextView)stringBuilder).isAnyPasswordInputType())
            paramInt1 = paramInt2 | 0x4; 
        } 
        if (!isActiveLocked()) {
          startSessionLocked(autofillId, paramRect, null, paramInt1);
        } else {
          if (this.mForAugmentedAutofillOnly && (paramInt1 & 0x1) != 0) {
            if (Helper.sDebug) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("notifyViewEntered(");
              stringBuilder.append(autofillId);
              stringBuilder.append("): resetting mForAugmentedAutofillOnly on manual request");
              Log.d("AutofillManager", stringBuilder.toString());
            } 
            this.mForAugmentedAutofillOnly = false;
          } 
          updateSessionLocked(autofillId, paramRect, null, 2, paramInt1);
        } 
        addEnteredIdLocked(autofillId);
        autofillCallback2 = autofillCallback1;
      } 
    } 
    return autofillCallback2;
  }
  
  private void addEnteredIdLocked(AutofillId paramAutofillId) {
    if (this.mEnteredIds == null)
      this.mEnteredIds = new ArraySet<>(1); 
    paramAutofillId.resetSessionId();
    this.mEnteredIds.add(paramAutofillId);
  }
  
  public void notifyViewExited(View paramView, int paramInt) {
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifyViewExited(");
      stringBuilder.append(paramView.getAutofillId());
      stringBuilder.append(", ");
      stringBuilder.append(paramInt);
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      notifyViewExitedLocked(paramView, paramInt);
      return;
    } 
  }
  
  private void notifyViewExitedLocked(View paramView, int paramInt) {
    ensureServiceClientAddedIfNeededLocked();
    if ((this.mEnabled || this.mEnabledForAugmentedAutofillOnly) && isActiveLocked())
      if (!isClientDisablingEnterExitEvent()) {
        AutofillId autofillId = getAutofillId(paramView, paramInt);
        updateSessionLocked(autofillId, null, null, 3, 0);
      }  
  }
  
  public void notifyValueChanged(View paramView) {
    if (!hasAutofillFeature())
      return; 
    AutofillId autofillId = null;
    boolean bool = false;
    AutofillValue autofillValue = null;
    synchronized (this.mLock) {
      String str;
      if (this.mLastAutofilledData == null) {
        paramView.setAutofilled(false, false);
      } else {
        autofillId = paramView.getAutofillId();
        if (this.mLastAutofilledData.containsKey(autofillId)) {
          boolean bool1;
          autofillValue = paramView.getAutofillValue();
          bool = true;
          if (this.mLastAutofilledData.keySet().size() == 1) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          if (Objects.equals(this.mLastAutofilledData.get(autofillId), autofillValue)) {
            paramView.setAutofilled(true, bool1);
          } else {
            paramView.setAutofilled(false, false);
            this.mLastAutofilledData.remove(autofillId);
          } 
        } else {
          paramView.setAutofilled(false, false);
        } 
      } 
      if (!this.mEnabled || !isActiveLocked()) {
        if (!startAutofillIfNeededLocked(paramView) && 
          Helper.sVerbose) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("notifyValueChanged(");
          stringBuilder.append(paramView.getAutofillId());
          stringBuilder.append("): ignoring on state ");
          stringBuilder.append(getStateAsStringLocked());
          str = stringBuilder.toString();
          Log.v("AutofillManager", str);
        } 
        return;
      } 
      AutofillId autofillId1 = autofillId;
      if (autofillId == null)
        autofillId1 = str.getAutofillId(); 
      if (!bool)
        autofillValue = str.getAutofillValue(); 
      updateSessionLocked(autofillId1, null, autofillValue, 4, 0);
      return;
    } 
  }
  
  public void notifyValueChanged(View paramView, int paramInt, AutofillValue paramAutofillValue) {
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      String str;
      StringBuilder stringBuilder;
      if (!this.mEnabled || !isActiveLocked()) {
        if (Helper.sVerbose) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("notifyValueChanged(");
          stringBuilder.append(paramView.getAutofillId());
          stringBuilder.append(":");
          stringBuilder.append(paramInt);
          stringBuilder.append("): ignoring on state ");
          stringBuilder.append(getStateAsStringLocked());
          str = stringBuilder.toString();
          Log.v("AutofillManager", str);
        } 
        return;
      } 
      AutofillId autofillId = getAutofillId((View)str, paramInt);
      updateSessionLocked(autofillId, null, (AutofillValue)stringBuilder, 4, 0);
      return;
    } 
  }
  
  public void notifyViewClicked(View paramView) {
    notifyViewClicked(paramView.getAutofillId());
  }
  
  public void notifyViewClicked(View paramView, int paramInt) {
    notifyViewClicked(getAutofillId(paramView, paramInt));
  }
  
  private void notifyViewClicked(AutofillId paramAutofillId) {
    if (!hasAutofillFeature())
      return; 
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifyViewClicked(): id=");
      stringBuilder.append(paramAutofillId);
      stringBuilder.append(", trigger=");
      stringBuilder.append(this.mSaveTriggerId);
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    synchronized (this.mLock) {
      if (!this.mEnabled || !isActiveLocked())
        return; 
      if (this.mSaveTriggerId != null && this.mSaveTriggerId.equals(paramAutofillId)) {
        if (Helper.sDebug) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("triggering commit by click of ");
          stringBuilder.append(paramAutofillId);
          Log.d("AutofillManager", stringBuilder.toString());
        } 
        commitLocked();
        this.mMetricsLogger.write(newLog(1229));
      } 
      return;
    } 
  }
  
  public void onActivityFinishing() {
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      if (this.mSaveOnFinish) {
        if (Helper.sDebug)
          Log.d("AutofillManager", "onActivityFinishing(): calling commitLocked()"); 
        commitLocked();
      } else {
        if (Helper.sDebug)
          Log.d("AutofillManager", "onActivityFinishing(): calling cancelLocked()"); 
        cancelLocked();
      } 
      return;
    } 
  }
  
  public void commit() {
    if (!hasAutofillFeature())
      return; 
    if (Helper.sVerbose)
      Log.v("AutofillManager", "commit() called by app"); 
    synchronized (this.mLock) {
      commitLocked();
      return;
    } 
  }
  
  private void commitLocked() {
    if (!this.mEnabled && !isActiveLocked())
      return; 
    finishSessionLocked();
  }
  
  public void cancel() {
    if (Helper.sVerbose)
      Log.v("AutofillManager", "cancel() called by app or augmented autofill service"); 
    if (!hasAutofillFeature())
      return; 
    synchronized (this.mLock) {
      cancelLocked();
      return;
    } 
  }
  
  private void cancelLocked() {
    if (!this.mEnabled && !isActiveLocked())
      return; 
    cancelSessionLocked();
  }
  
  public void disableOwnedAutofillServices() {
    disableAutofillServices();
  }
  
  public void disableAutofillServices() {
    if (!hasAutofillFeature())
      return; 
    try {
      this.mService.disableOwnedAutofillServices(this.mContext.getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasEnabledAutofillServices() {
    IAutoFillManager iAutoFillManager = this.mService;
    boolean bool = false;
    if (iAutoFillManager == null)
      return false; 
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      this.mService.isServiceEnabled(this.mContext.getUserId(), this.mContext.getPackageName(), (IResultReceiver)syncResultReceiver);
      int i = syncResultReceiver.getIntResult();
      if (i == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get enabled autofill services status.");
    } 
  }
  
  public ComponentName getAutofillServiceComponentName() {
    if (this.mService == null)
      return null; 
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      this.mService.getAutofillServiceComponentName((IResultReceiver)syncResultReceiver);
      return (ComponentName)syncResultReceiver.getParcelableResult();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get autofill services component name.");
    } 
  }
  
  public String getUserDataId() {
    try {
      SyncResultReceiver syncResultReceiver = new SyncResultReceiver();
      this(5000);
      this.mService.getUserDataId((IResultReceiver)syncResultReceiver);
      return syncResultReceiver.getStringResult();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get user data id for field classification.");
    } 
  }
  
  public UserData getUserData() {
    try {
      SyncResultReceiver syncResultReceiver = new SyncResultReceiver();
      this(5000);
      this.mService.getUserData((IResultReceiver)syncResultReceiver);
      return (UserData)syncResultReceiver.getParcelableResult();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get user data for field classification.");
    } 
  }
  
  public void setUserData(UserData paramUserData) {
    try {
      this.mService.setUserData(paramUserData);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isFieldClassificationEnabled() {
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      this.mService.isFieldClassificationEnabled((IResultReceiver)syncResultReceiver);
      int i = syncResultReceiver.getIntResult();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get field classification enabled status.");
    } 
  }
  
  public String getDefaultFieldClassificationAlgorithm() {
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      this.mService.getDefaultFieldClassificationAlgorithm((IResultReceiver)syncResultReceiver);
      return syncResultReceiver.getStringResult();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get default field classification algorithm.");
    } 
  }
  
  public List<String> getAvailableFieldClassificationAlgorithms() {
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      List<?> list;
      this.mService.getAvailableFieldClassificationAlgorithms((IResultReceiver)syncResultReceiver);
      String[] arrayOfString = syncResultReceiver.getStringArrayResult();
      if (arrayOfString != null) {
        list = Arrays.asList(arrayOfString);
      } else {
        list = Collections.emptyList();
      } 
      return (List)list;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get available field classification algorithms.");
    } 
  }
  
  public boolean isAutofillSupported() {
    IAutoFillManager iAutoFillManager = this.mService;
    boolean bool = false;
    if (iAutoFillManager == null)
      return false; 
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      this.mService.isServiceSupported(this.mContext.getUserId(), (IResultReceiver)syncResultReceiver);
      int i = syncResultReceiver.getIntResult();
      if (i == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      throw new RuntimeException("Fail to get autofill supported status.");
    } 
  }
  
  private AutofillClient getClient() {
    AutofillClient autofillClient = this.mContext.getAutofillClient();
    if (autofillClient == null && Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No AutofillClient for ");
      stringBuilder.append(this.mContext.getPackageName());
      stringBuilder.append(" on context ");
      stringBuilder.append(this.mContext);
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    return autofillClient;
  }
  
  public boolean isAutofillUiShowing() {
    boolean bool;
    AutofillClient autofillClient = this.mContext.getAutofillClient();
    if (autofillClient != null && autofillClient.autofillClientIsFillUiShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onAuthenticationResult(int paramInt, Intent paramIntent, View paramView) {
    if (!hasAutofillFeature())
      return; 
    if (Helper.sDebug) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onAuthenticationResult(): id= ");
      stringBuilder.append(paramInt);
      stringBuilder.append(", data=");
      stringBuilder.append(paramIntent);
      Log.d("AutofillManager", stringBuilder.toString());
    } 
    synchronized (this.mLock) {
      if (!isActiveLocked())
        return; 
      if (!this.mOnInvisibleCalled && paramView != null && 
        paramView.canNotifyAutofillEnterExitEvent()) {
        notifyViewExitedLocked(paramView);
        notifyViewEnteredLocked(paramView, 0);
      } 
      if (paramIntent == null)
        return; 
      Parcelable parcelable = paramIntent.getParcelableExtra("android.view.autofill.extra.AUTHENTICATION_RESULT");
      Bundle bundle2 = new Bundle();
      this();
      bundle2.putParcelable("android.view.autofill.extra.AUTHENTICATION_RESULT", parcelable);
      Bundle bundle1 = paramIntent.getBundleExtra("android.view.autofill.extra.CLIENT_STATE");
      if (bundle1 != null)
        bundle2.putBundle("android.view.autofill.extra.CLIENT_STATE", bundle1); 
      try {
        IAutoFillManager iAutoFillManager = this.mService;
        int i = this.mSessionId;
        Context context = this.mContext;
        int j = context.getUserId();
        iAutoFillManager.setAuthenticationResult(bundle2, i, paramInt, j);
      } catch (RemoteException remoteException) {
        Log.e("AutofillManager", "Error delivering authentication result", (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  public AutofillId getNextAutofillId() {
    AutofillClient autofillClient = getClient();
    if (autofillClient == null)
      return null; 
    AutofillId autofillId = autofillClient.autofillClientGetNextAutofillId();
    if (autofillId == null && Helper.sDebug) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getNextAutofillId(): client ");
      stringBuilder.append(autofillClient);
      stringBuilder.append(" returned null");
      Log.d("AutofillManager", stringBuilder.toString());
    } 
    return autofillId;
  }
  
  private static AutofillId getAutofillId(View paramView, int paramInt) {
    return new AutofillId(paramView.getAutofillViewId(), paramInt);
  }
  
  private void startSessionLocked(AutofillId paramAutofillId, Rect paramRect, AutofillValue paramAutofillValue, int paramInt) {
    StringBuilder stringBuilder;
    Set<AutofillId> set = this.mEnteredForAugmentedAutofillIds;
    if ((set != null && 
      set.contains(paramAutofillId)) || this.mEnabledForAugmentedAutofillOnly) {
      if (Helper.sVerbose) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Starting session for augmented autofill on ");
        stringBuilder1.append(paramAutofillId);
        Log.v("AutofillManager", stringBuilder1.toString());
      } 
      paramInt |= 0x8;
    } 
    if (Helper.sVerbose) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("startSessionLocked(): id=");
      stringBuilder1.append(paramAutofillId);
      stringBuilder1.append(", bounds=");
      stringBuilder1.append(paramRect);
      stringBuilder1.append(", value=");
      stringBuilder1.append(paramAutofillValue);
      stringBuilder1.append(", flags=");
      stringBuilder1.append(paramInt);
      stringBuilder1.append(", state=");
      stringBuilder1.append(getStateAsStringLocked());
      stringBuilder1.append(", compatMode=");
      stringBuilder1.append(isCompatibilityModeEnabledLocked());
      stringBuilder1.append(", augmentedOnly=");
      stringBuilder1.append(this.mForAugmentedAutofillOnly);
      stringBuilder1.append(", enabledAugmentedOnly=");
      stringBuilder1.append(this.mEnabledForAugmentedAutofillOnly);
      stringBuilder1.append(", enteredIds=");
      stringBuilder1.append(this.mEnteredIds);
      String str = stringBuilder1.toString();
      Log.v("AutofillManager", str);
    } 
    if (this.mForAugmentedAutofillOnly && !this.mEnabledForAugmentedAutofillOnly && (paramInt & 0x1) != 0) {
      if (Helper.sVerbose)
        Log.v("AutofillManager", "resetting mForAugmentedAutofillOnly on manual autofill request"); 
      this.mForAugmentedAutofillOnly = false;
    } 
    if (this.mState != 0 && !isFinishedLocked() && (paramInt & 0x1) == 0) {
      if (Helper.sVerbose) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("not automatically starting session for ");
        stringBuilder.append(paramAutofillId);
        stringBuilder.append(" on state ");
        stringBuilder.append(getStateAsStringLocked());
        stringBuilder.append(" and flags ");
        stringBuilder.append(paramInt);
        String str = stringBuilder.toString();
        Log.v("AutofillManager", str);
      } 
      return;
    } 
    try {
      StringBuilder stringBuilder1;
      AutofillClient autofillClient = getClient();
      if (autofillClient == null)
        return; 
      SyncResultReceiver syncResultReceiver = new SyncResultReceiver();
      this(5000);
      ComponentName componentName = autofillClient.autofillClientGetComponentName();
      boolean bool = this.mEnabledForAugmentedAutofillOnly;
      if (!bool && this.mOptions != null) {
        AutofillOptions autofillOptions = this.mOptions;
        if (autofillOptions.isAutofillDisabledLocked(componentName))
          if (this.mOptions.isAugmentedAutofillEnabled(this.mContext)) {
            if (Helper.sDebug) {
              StringBuilder stringBuilder2 = new StringBuilder();
              this();
              stringBuilder2.append("startSession(");
              stringBuilder2.append(componentName);
              stringBuilder2.append("): disabled by service but whitelisted for augmented autofill");
              Log.d("AutofillManager", stringBuilder2.toString());
              paramInt |= 0x8;
            } 
          } else {
            if (Helper.sDebug) {
              stringBuilder1 = new StringBuilder();
              this();
              stringBuilder1.append("startSession(");
              stringBuilder1.append(componentName);
              stringBuilder1.append("): ignored because disabled by service and not whitelisted for augmented autofill");
              Log.d("AutofillManager", stringBuilder1.toString());
            } 
            setSessionFinished(4, null);
            autofillClient.autofillClientResetableStateAvailable();
            return;
          }  
      } 
      try {
        IAutoFillManager iAutoFillManager = this.mService;
        IBinder iBinder1 = autofillClient.autofillClientGetActivityToken();
        IAutoFillManagerClient iAutoFillManagerClient = this.mServiceClient;
        IBinder iBinder2 = iAutoFillManagerClient.asBinder();
        int i = this.mContext.getUserId();
        if (this.mCallback != null) {
          bool = true;
        } else {
          bool = false;
        } 
        boolean bool1 = isCompatibilityModeEnabledLocked();
        iAutoFillManager.startSession(iBinder1, iBinder2, (AutofillId)stringBuilder1, (Rect)stringBuilder, paramAutofillValue, i, bool, paramInt, componentName, bool1, (IResultReceiver)syncResultReceiver);
        this.mSessionId = i = syncResultReceiver.getIntResult();
        if (i != Integer.MAX_VALUE)
          this.mState = 1; 
        i = syncResultReceiver.getOptionalExtraIntResult(0);
        if ((i & 0x1) != 0) {
          if (Helper.sDebug) {
            stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("startSession(");
            stringBuilder1.append(componentName);
            stringBuilder1.append("): for augmented only");
            Log.d("AutofillManager", stringBuilder1.toString());
          } 
          this.mForAugmentedAutofillOnly = true;
        } 
        autofillClient.autofillClientResetableStateAvailable();
      } catch (RemoteException remoteException) {
      
      } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {}
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Exception getting result from SyncResultReceiver: ");
      stringBuilder.append(timeoutException);
      Log.w("AutofillManager", stringBuilder.toString());
      return;
    } 
  }
  
  private void finishSessionLocked() {
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("finishSessionLocked(): ");
      stringBuilder.append(getStateAsStringLocked());
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    if (!isActiveLocked())
      return; 
    try {
      this.mService.finishSession(this.mSessionId, this.mContext.getUserId());
      resetSessionLocked(true);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void cancelSessionLocked() {
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("cancelSessionLocked(): ");
      stringBuilder.append(getStateAsStringLocked());
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    if (!isActiveLocked())
      return; 
    try {
      this.mService.cancelSession(this.mSessionId, this.mContext.getUserId());
      resetSessionLocked(true);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void resetSessionLocked(boolean paramBoolean) {
    this.mSessionId = Integer.MAX_VALUE;
    this.mState = 0;
    this.mTrackedViews = null;
    this.mFillableIds = null;
    this.mSaveTriggerId = null;
    this.mIdShownFillUi = null;
    if (paramBoolean)
      this.mEnteredIds = null; 
  }
  
  private void updateSessionLocked(AutofillId paramAutofillId, Rect paramRect, AutofillValue paramAutofillValue, int paramInt1, int paramInt2) {
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateSessionLocked(): id=");
      stringBuilder.append(paramAutofillId);
      stringBuilder.append(", bounds=");
      stringBuilder.append(paramRect);
      stringBuilder.append(", value=");
      stringBuilder.append(paramAutofillValue);
      stringBuilder.append(", action=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", flags=");
      stringBuilder.append(paramInt2);
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    try {
      IAutoFillManager iAutoFillManager = this.mService;
      int i = this.mSessionId;
      Context context = this.mContext;
      int j = context.getUserId();
      iAutoFillManager.updateSession(i, paramAutofillId, paramRect, paramAutofillValue, paramInt1, paramInt2, j);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void ensureServiceClientAddedIfNeededLocked() {
    AutofillClient autofillClient = getClient();
    if (autofillClient == null)
      return; 
    if (this.mServiceClient == null) {
      this.mServiceClient = new AutofillManagerClient();
      try {
        int i = this.mContext.getUserId();
        SyncResultReceiver syncResultReceiver = new SyncResultReceiver();
        this(5000);
        this.mService.addClient(this.mServiceClient, autofillClient.autofillClientGetComponentName(), i, (IResultReceiver)syncResultReceiver);
        try {
          int j = syncResultReceiver.getIntResult();
          boolean bool1 = false;
          if ((j & 0x1) != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          this.mEnabled = bool2;
          if ((j & 0x2) != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          Helper.sDebug = bool2;
          if ((j & 0x4) != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          Helper.sVerbose = bool2;
          boolean bool2 = bool1;
          if ((j & 0x8) != 0)
            bool2 = true; 
          this.mEnabledForAugmentedAutofillOnly = bool2;
          if (Helper.sVerbose) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("receiver results: flags=");
            stringBuilder.append(j);
            stringBuilder.append(" enabled=");
            stringBuilder.append(this.mEnabled);
            stringBuilder.append(", enabledForAugmentedOnly: ");
            stringBuilder.append(this.mEnabledForAugmentedAutofillOnly);
            Log.v("AutofillManager", stringBuilder.toString());
          } 
          IAutoFillManager iAutoFillManager = this.mService;
          IAutoFillManagerClient iAutoFillManagerClient = this.mServiceClient;
          _$$Lambda$AutofillManager$V76JiQu509LCUz3_ckpb_nB3JhA _$$Lambda$AutofillManager$V76JiQu509LCUz3_ckpb_nB3JhA = new _$$Lambda$AutofillManager$V76JiQu509LCUz3_ckpb_nB3JhA();
          this(iAutoFillManager, iAutoFillManagerClient, i);
          this.mServiceClientCleaner = Cleaner.create(this, _$$Lambda$AutofillManager$V76JiQu509LCUz3_ckpb_nB3JhA);
        } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Failed to initialize autofill: ");
          stringBuilder.append(timeoutException);
          Log.w("AutofillManager", stringBuilder.toString());
          return;
        } 
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  private boolean startAutofillIfNeededLocked(View paramView) {
    if (this.mState == 0 && this.mSessionId == Integer.MAX_VALUE && paramView instanceof EditText) {
      EditText editText = (EditText)paramView;
      if (!TextUtils.isEmpty(editText.getText()) && 
        !paramView.isFocused() && 
        paramView.isImportantForAutofill() && 
        paramView.isLaidOut() && 
        paramView.isVisibleToUser()) {
        ensureServiceClientAddedIfNeededLocked();
        if (Helper.sVerbose) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("startAutofillIfNeededLocked(): enabled=");
          stringBuilder.append(this.mEnabled);
          Log.v("AutofillManager", stringBuilder.toString());
        } 
        if (this.mEnabled && !isClientDisablingEnterExitEvent()) {
          AutofillId autofillId = paramView.getAutofillId();
          AutofillValue autofillValue = paramView.getAutofillValue();
          startSessionLocked(autofillId, null, null, 0);
          updateSessionLocked(autofillId, null, autofillValue, 4, 0);
          addEnteredIdLocked(autofillId);
          return true;
        } 
      } 
    } 
    return false;
  }
  
  public void registerCallback(AutofillCallback paramAutofillCallback) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual hasAutofillFeature : ()Z
    //   4: ifne -> 8
    //   7: return
    //   8: aload_0
    //   9: getfield mLock : Ljava/lang/Object;
    //   12: astore_2
    //   13: aload_2
    //   14: monitorenter
    //   15: aload_1
    //   16: ifnonnull -> 22
    //   19: aload_2
    //   20: monitorexit
    //   21: return
    //   22: aload_0
    //   23: getfield mCallback : Landroid/view/autofill/AutofillManager$AutofillCallback;
    //   26: ifnull -> 34
    //   29: iconst_1
    //   30: istore_3
    //   31: goto -> 36
    //   34: iconst_0
    //   35: istore_3
    //   36: aload_0
    //   37: aload_1
    //   38: putfield mCallback : Landroid/view/autofill/AutofillManager$AutofillCallback;
    //   41: iload_3
    //   42: ifne -> 75
    //   45: aload_0
    //   46: getfield mService : Landroid/view/autofill/IAutoFillManager;
    //   49: aload_0
    //   50: getfield mSessionId : I
    //   53: aload_0
    //   54: getfield mContext : Landroid/content/Context;
    //   57: invokevirtual getUserId : ()I
    //   60: iconst_1
    //   61: invokeinterface setHasCallback : (IIZ)V
    //   66: goto -> 75
    //   69: astore_1
    //   70: aload_1
    //   71: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   74: athrow
    //   75: aload_2
    //   76: monitorexit
    //   77: return
    //   78: astore_1
    //   79: aload_2
    //   80: monitorexit
    //   81: aload_1
    //   82: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1991	-> 0
    //   #1992	-> 7
    //   #1994	-> 8
    //   #1995	-> 15
    //   #1997	-> 22
    //   #1998	-> 36
    //   #2000	-> 41
    //   #2002	-> 45
    //   #2005	-> 66
    //   #2003	-> 69
    //   #2004	-> 70
    //   #2007	-> 75
    //   #2008	-> 77
    //   #2007	-> 78
    // Exception table:
    //   from	to	target	type
    //   19	21	78	finally
    //   22	29	78	finally
    //   36	41	78	finally
    //   45	66	69	android/os/RemoteException
    //   45	66	78	finally
    //   70	75	78	finally
    //   75	77	78	finally
    //   79	81	78	finally
  }
  
  public void unregisterCallback(AutofillCallback paramAutofillCallback) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual hasAutofillFeature : ()Z
    //   4: ifne -> 8
    //   7: return
    //   8: aload_0
    //   9: getfield mLock : Ljava/lang/Object;
    //   12: astore_2
    //   13: aload_2
    //   14: monitorenter
    //   15: aload_1
    //   16: ifnull -> 72
    //   19: aload_0
    //   20: getfield mCallback : Landroid/view/autofill/AutofillManager$AutofillCallback;
    //   23: ifnull -> 72
    //   26: aload_1
    //   27: aload_0
    //   28: getfield mCallback : Landroid/view/autofill/AutofillManager$AutofillCallback;
    //   31: if_acmpeq -> 37
    //   34: goto -> 72
    //   37: aload_0
    //   38: aconst_null
    //   39: putfield mCallback : Landroid/view/autofill/AutofillManager$AutofillCallback;
    //   42: aload_0
    //   43: getfield mService : Landroid/view/autofill/IAutoFillManager;
    //   46: aload_0
    //   47: getfield mSessionId : I
    //   50: aload_0
    //   51: getfield mContext : Landroid/content/Context;
    //   54: invokevirtual getUserId : ()I
    //   57: iconst_0
    //   58: invokeinterface setHasCallback : (IIZ)V
    //   63: aload_2
    //   64: monitorexit
    //   65: return
    //   66: astore_1
    //   67: aload_1
    //   68: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   71: athrow
    //   72: aload_2
    //   73: monitorexit
    //   74: return
    //   75: astore_1
    //   76: aload_2
    //   77: monitorexit
    //   78: aload_1
    //   79: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2016	-> 0
    //   #2017	-> 7
    //   #2019	-> 8
    //   #2020	-> 15
    //   #2022	-> 37
    //   #2025	-> 42
    //   #2028	-> 63
    //   #2029	-> 63
    //   #2030	-> 65
    //   #2026	-> 66
    //   #2027	-> 67
    //   #2020	-> 72
    //   #2029	-> 75
    // Exception table:
    //   from	to	target	type
    //   19	34	75	finally
    //   37	42	75	finally
    //   42	63	66	android/os/RemoteException
    //   42	63	75	finally
    //   63	65	75	finally
    //   67	72	75	finally
    //   72	74	75	finally
    //   76	78	75	finally
  }
  
  @SystemApi
  public void setAugmentedAutofillWhitelist(Set<String> paramSet, Set<ComponentName> paramSet1) {
    if (!hasAutofillFeature())
      return; 
    SyncResultReceiver syncResultReceiver = new SyncResultReceiver(5000);
    try {
      this.mService.setAugmentedAutofillWhitelist(Helper.toList(paramSet), Helper.toList(paramSet1), (IResultReceiver)syncResultReceiver);
      int i = syncResultReceiver.getIntResult();
      if (i != -1) {
        if (i != 0) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("setAugmentedAutofillWhitelist(): received invalid result: ");
          stringBuilder.append(i);
          Log.wtf("AutofillManager", stringBuilder.toString());
          return;
        } 
        return;
      } 
      throw new SecurityException("caller is not user's Augmented Autofill Service");
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (com.android.internal.util.SyncResultReceiver.TimeoutException timeoutException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fail to get the result of set AugmentedAutofill whitelist. ");
      stringBuilder.append(timeoutException);
      Log.e("AutofillManager", stringBuilder.toString());
      return;
    } 
  }
  
  public void notifyViewEnteredForAugmentedAutofill(View paramView) {
    AutofillId autofillId = paramView.getAutofillId();
    synchronized (this.mLock) {
      if (this.mEnteredForAugmentedAutofillIds == null) {
        ArraySet<AutofillId> arraySet = new ArraySet();
        this(1);
        this.mEnteredForAugmentedAutofillIds = arraySet;
      } 
      this.mEnteredForAugmentedAutofillIds.add(autofillId);
      return;
    } 
  }
  
  private void requestShowFillUi(int paramInt1, AutofillId paramAutofillId, int paramInt2, int paramInt3, Rect paramRect, IAutofillWindowPresenter paramIAutofillWindowPresenter) {
    // Byte code:
    //   0: aload_0
    //   1: aload_2
    //   2: invokespecial findView : (Landroid/view/autofill/AutofillId;)Landroid/view/View;
    //   5: astore #7
    //   7: aload #7
    //   9: ifnonnull -> 13
    //   12: return
    //   13: aconst_null
    //   14: astore #8
    //   16: aload_0
    //   17: getfield mLock : Ljava/lang/Object;
    //   20: astore #9
    //   22: aload #9
    //   24: monitorenter
    //   25: aload_0
    //   26: getfield mSessionId : I
    //   29: istore #10
    //   31: aload #8
    //   33: astore #11
    //   35: iload #10
    //   37: iload_1
    //   38: if_icmpne -> 90
    //   41: aload_0
    //   42: invokespecial getClient : ()Landroid/view/autofill/AutofillManager$AutofillClient;
    //   45: astore #12
    //   47: aload #8
    //   49: astore #11
    //   51: aload #12
    //   53: ifnull -> 90
    //   56: aload #8
    //   58: astore #11
    //   60: aload #12
    //   62: aload #7
    //   64: iload_3
    //   65: iload #4
    //   67: aload #5
    //   69: aload #6
    //   71: invokeinterface autofillClientRequestShowFillUi : (Landroid/view/View;IILandroid/graphics/Rect;Landroid/view/autofill/IAutofillWindowPresenter;)Z
    //   76: ifeq -> 90
    //   79: aload_0
    //   80: getfield mCallback : Landroid/view/autofill/AutofillManager$AutofillCallback;
    //   83: astore #11
    //   85: aload_0
    //   86: aload_2
    //   87: putfield mIdShownFillUi : Landroid/view/autofill/AutofillId;
    //   90: aload #9
    //   92: monitorexit
    //   93: aload #11
    //   95: ifnull -> 128
    //   98: aload_2
    //   99: invokevirtual isVirtualInt : ()Z
    //   102: ifeq -> 120
    //   105: aload #11
    //   107: aload #7
    //   109: aload_2
    //   110: invokevirtual getVirtualChildIntId : ()I
    //   113: iconst_1
    //   114: invokevirtual onAutofillEvent : (Landroid/view/View;II)V
    //   117: goto -> 128
    //   120: aload #11
    //   122: aload #7
    //   124: iconst_1
    //   125: invokevirtual onAutofillEvent : (Landroid/view/View;I)V
    //   128: return
    //   129: astore_2
    //   130: aload #9
    //   132: monitorexit
    //   133: aload_2
    //   134: athrow
    //   135: astore_2
    //   136: goto -> 130
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2102	-> 0
    //   #2103	-> 7
    //   #2104	-> 12
    //   #2107	-> 13
    //   #2108	-> 16
    //   #2109	-> 25
    //   #2110	-> 41
    //   #2112	-> 47
    //   #2113	-> 56
    //   #2115	-> 79
    //   #2116	-> 85
    //   #2120	-> 90
    //   #2122	-> 93
    //   #2123	-> 98
    //   #2124	-> 105
    //   #2127	-> 120
    //   #2130	-> 128
    //   #2120	-> 129
    // Exception table:
    //   from	to	target	type
    //   25	31	129	finally
    //   41	47	135	finally
    //   60	79	135	finally
    //   79	85	135	finally
    //   85	90	135	finally
    //   90	93	135	finally
    //   130	133	135	finally
  }
  
  private void authenticate(int paramInt1, int paramInt2, IntentSender paramIntentSender, Intent paramIntent, boolean paramBoolean) {
    synchronized (this.mLock) {
      if (paramInt1 == this.mSessionId) {
        AutofillClient autofillClient = getClient();
        if (autofillClient != null) {
          this.mOnInvisibleCalled = false;
          autofillClient.autofillClientAuthenticate(paramInt2, paramIntentSender, paramIntent, paramBoolean);
        } 
      } 
      return;
    } 
  }
  
  private void dispatchUnhandledKey(int paramInt, AutofillId paramAutofillId, KeyEvent paramKeyEvent) {
    View view = findView(paramAutofillId);
    if (view == null)
      return; 
    synchronized (this.mLock) {
      if (this.mSessionId == paramInt) {
        AutofillClient autofillClient = getClient();
        if (autofillClient != null)
          autofillClient.autofillClientDispatchUnhandledKey(view, paramKeyEvent); 
      } 
      return;
    } 
  }
  
  private void setState(int paramInt) {
    // Byte code:
    //   0: getstatic android/view/autofill/Helper.sVerbose : Z
    //   3: ifeq -> 68
    //   6: new java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore_2
    //   14: aload_2
    //   15: ldc_w 'setState('
    //   18: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   21: pop
    //   22: aload_2
    //   23: iload_1
    //   24: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   27: pop
    //   28: aload_2
    //   29: ldc_w ': '
    //   32: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: aload_2
    //   37: ldc android/view/autofill/AutofillManager
    //   39: ldc_w 'SET_STATE_FLAG_'
    //   42: iload_1
    //   43: invokestatic flagsToString : (Ljava/lang/Class;Ljava/lang/String;I)Ljava/lang/String;
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_2
    //   51: ldc_w ')'
    //   54: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: pop
    //   58: ldc 'AutofillManager'
    //   60: aload_2
    //   61: invokevirtual toString : ()Ljava/lang/String;
    //   64: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   67: pop
    //   68: aload_0
    //   69: getfield mLock : Ljava/lang/Object;
    //   72: astore_2
    //   73: aload_2
    //   74: monitorenter
    //   75: iconst_1
    //   76: istore_3
    //   77: iload_1
    //   78: bipush #32
    //   80: iand
    //   81: ifeq -> 97
    //   84: aload_0
    //   85: iconst_1
    //   86: putfield mForAugmentedAutofillOnly : Z
    //   89: aload_2
    //   90: monitorexit
    //   91: return
    //   92: astore #4
    //   94: goto -> 218
    //   97: iload_1
    //   98: iconst_1
    //   99: iand
    //   100: ifeq -> 109
    //   103: iconst_1
    //   104: istore #5
    //   106: goto -> 112
    //   109: iconst_0
    //   110: istore #5
    //   112: aload_0
    //   113: iload #5
    //   115: putfield mEnabled : Z
    //   118: iload #5
    //   120: ifeq -> 129
    //   123: iload_1
    //   124: iconst_2
    //   125: iand
    //   126: ifeq -> 134
    //   129: aload_0
    //   130: iconst_1
    //   131: invokespecial resetSessionLocked : (Z)V
    //   134: iload_1
    //   135: iconst_4
    //   136: iand
    //   137: ifeq -> 173
    //   140: aload_0
    //   141: aconst_null
    //   142: putfield mServiceClient : Landroid/view/autofill/IAutoFillManagerClient;
    //   145: aload_0
    //   146: aconst_null
    //   147: putfield mAugmentedAutofillServiceClient : Landroid/view/autofill/IAugmentedAutofillManagerClient;
    //   150: aload_0
    //   151: getfield mServiceClientCleaner : Lsun/misc/Cleaner;
    //   154: ifnull -> 169
    //   157: aload_0
    //   158: getfield mServiceClientCleaner : Lsun/misc/Cleaner;
    //   161: invokevirtual clean : ()V
    //   164: aload_0
    //   165: aconst_null
    //   166: putfield mServiceClientCleaner : Lsun/misc/Cleaner;
    //   169: aload_0
    //   170: invokevirtual notifyReenableAutofill : ()V
    //   173: aload_2
    //   174: monitorexit
    //   175: iload_1
    //   176: bipush #8
    //   178: iand
    //   179: ifeq -> 188
    //   182: iconst_1
    //   183: istore #5
    //   185: goto -> 191
    //   188: iconst_0
    //   189: istore #5
    //   191: iload #5
    //   193: putstatic android/view/autofill/Helper.sDebug : Z
    //   196: iload_1
    //   197: bipush #16
    //   199: iand
    //   200: ifeq -> 209
    //   203: iload_3
    //   204: istore #5
    //   206: goto -> 212
    //   209: iconst_0
    //   210: istore #5
    //   212: iload #5
    //   214: putstatic android/view/autofill/Helper.sVerbose : Z
    //   217: return
    //   218: aload_2
    //   219: monitorexit
    //   220: aload #4
    //   222: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2179	-> 0
    //   #2180	-> 6
    //   #2183	-> 68
    //   #2184	-> 75
    //   #2185	-> 84
    //   #2187	-> 89
    //   #2204	-> 92
    //   #2189	-> 97
    //   #2190	-> 118
    //   #2192	-> 129
    //   #2194	-> 134
    //   #2196	-> 140
    //   #2197	-> 145
    //   #2198	-> 150
    //   #2199	-> 157
    //   #2200	-> 164
    //   #2202	-> 169
    //   #2204	-> 173
    //   #2205	-> 175
    //   #2206	-> 196
    //   #2207	-> 217
    //   #2204	-> 218
    // Exception table:
    //   from	to	target	type
    //   84	89	92	finally
    //   89	91	92	finally
    //   112	118	92	finally
    //   129	134	92	finally
    //   140	145	92	finally
    //   145	150	92	finally
    //   150	157	92	finally
    //   157	164	92	finally
    //   164	169	92	finally
    //   169	173	92	finally
    //   173	175	92	finally
    //   218	220	92	finally
  }
  
  private void setAutofilledIfValuesIs(View paramView, AutofillValue paramAutofillValue, boolean paramBoolean) {
    AutofillValue autofillValue = paramView.getAutofillValue();
    if (Objects.equals(autofillValue, paramAutofillValue))
      synchronized (this.mLock) {
        if (this.mLastAutofilledData == null) {
          ParcelableMap parcelableMap = new ParcelableMap();
          this(1);
          this.mLastAutofilledData = parcelableMap;
        } 
        this.mLastAutofilledData.put(paramView.getAutofillId(), paramAutofillValue);
        paramView.setAutofilled(true, paramBoolean);
      }  
  }
  
  private void autofill(int paramInt, List<AutofillId> paramList, List<AutofillValue> paramList1, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore #5
    //   6: aload #5
    //   8: monitorenter
    //   9: iload_1
    //   10: aload_0
    //   11: getfield mSessionId : I
    //   14: if_icmpeq -> 21
    //   17: aload #5
    //   19: monitorexit
    //   20: return
    //   21: aload_0
    //   22: invokespecial getClient : ()Landroid/view/autofill/AutofillManager$AutofillClient;
    //   25: astore #6
    //   27: aload #6
    //   29: ifnonnull -> 36
    //   32: aload #5
    //   34: monitorexit
    //   35: return
    //   36: aload_2
    //   37: invokeinterface size : ()I
    //   42: istore #7
    //   44: aload_2
    //   45: invokestatic toArray : (Ljava/util/Collection;)[Landroid/view/autofill/AutofillId;
    //   48: astore #8
    //   50: aload #6
    //   52: aload #8
    //   54: invokeinterface autofillClientFindViewsByAutofillIdTraversal : ([Landroid/view/autofill/AutofillId;)[Landroid/view/View;
    //   59: astore #9
    //   61: iconst_0
    //   62: istore_1
    //   63: iconst_0
    //   64: istore #10
    //   66: aconst_null
    //   67: astore #8
    //   69: aconst_null
    //   70: astore #11
    //   72: iload #10
    //   74: iload #7
    //   76: if_icmpge -> 352
    //   79: aload_2
    //   80: iload #10
    //   82: invokeinterface get : (I)Ljava/lang/Object;
    //   87: checkcast android/view/autofill/AutofillId
    //   90: astore #12
    //   92: aload_3
    //   93: iload #10
    //   95: invokeinterface get : (I)Ljava/lang/Object;
    //   100: checkcast android/view/autofill/AutofillValue
    //   103: astore #13
    //   105: aload #9
    //   107: iload #10
    //   109: aaload
    //   110: astore #14
    //   112: aload #14
    //   114: ifnonnull -> 189
    //   117: new java/lang/StringBuilder
    //   120: astore #15
    //   122: aload #15
    //   124: invokespecial <init> : ()V
    //   127: aload #15
    //   129: ldc_w 'autofill(): no View with id '
    //   132: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: pop
    //   136: aload #15
    //   138: aload #12
    //   140: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   143: pop
    //   144: ldc 'AutofillManager'
    //   146: aload #15
    //   148: invokevirtual toString : ()Ljava/lang/String;
    //   151: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   154: pop
    //   155: aload #8
    //   157: astore #15
    //   159: aload #8
    //   161: ifnonnull -> 174
    //   164: new java/util/ArrayList
    //   167: astore #15
    //   169: aload #15
    //   171: invokespecial <init> : ()V
    //   174: aload #15
    //   176: aload #12
    //   178: invokevirtual add : (Ljava/lang/Object;)Z
    //   181: pop
    //   182: aload #15
    //   184: astore #8
    //   186: goto -> 338
    //   189: aload #12
    //   191: invokevirtual isVirtualInt : ()Z
    //   194: ifeq -> 278
    //   197: aload #11
    //   199: astore #15
    //   201: aload #11
    //   203: ifnonnull -> 217
    //   206: new android/util/ArrayMap
    //   209: astore #15
    //   211: aload #15
    //   213: iconst_1
    //   214: invokespecial <init> : (I)V
    //   217: aload #15
    //   219: aload #14
    //   221: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   224: checkcast android/util/SparseArray
    //   227: astore #16
    //   229: aload #16
    //   231: astore #11
    //   233: aload #16
    //   235: ifnonnull -> 259
    //   238: new android/util/SparseArray
    //   241: astore #11
    //   243: aload #11
    //   245: iconst_5
    //   246: invokespecial <init> : (I)V
    //   249: aload #15
    //   251: aload #14
    //   253: aload #11
    //   255: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   258: pop
    //   259: aload #11
    //   261: aload #12
    //   263: invokevirtual getVirtualChildIntId : ()I
    //   266: aload #13
    //   268: invokevirtual put : (ILjava/lang/Object;)V
    //   271: aload #15
    //   273: astore #11
    //   275: goto -> 338
    //   278: aload_0
    //   279: getfield mLastAutofilledData : Landroid/view/autofill/ParcelableMap;
    //   282: ifnonnull -> 306
    //   285: new android/view/autofill/ParcelableMap
    //   288: astore #15
    //   290: aload #15
    //   292: iload #7
    //   294: iload #10
    //   296: isub
    //   297: invokespecial <init> : (I)V
    //   300: aload_0
    //   301: aload #15
    //   303: putfield mLastAutofilledData : Landroid/view/autofill/ParcelableMap;
    //   306: aload_0
    //   307: getfield mLastAutofilledData : Landroid/view/autofill/ParcelableMap;
    //   310: aload #12
    //   312: aload #13
    //   314: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   317: pop
    //   318: aload #14
    //   320: aload #13
    //   322: invokevirtual autofill : (Landroid/view/autofill/AutofillValue;)V
    //   325: aload_0
    //   326: aload #14
    //   328: aload #13
    //   330: iload #4
    //   332: invokespecial setAutofilledIfValuesIs : (Landroid/view/View;Landroid/view/autofill/AutofillValue;Z)V
    //   335: iinc #1, 1
    //   338: iinc #10, 1
    //   341: goto -> 72
    //   344: astore_2
    //   345: goto -> 540
    //   348: astore_2
    //   349: goto -> 540
    //   352: aload #8
    //   354: ifnull -> 427
    //   357: getstatic android/view/autofill/Helper.sVerbose : Z
    //   360: ifeq -> 396
    //   363: new java/lang/StringBuilder
    //   366: astore_2
    //   367: aload_2
    //   368: invokespecial <init> : ()V
    //   371: aload_2
    //   372: ldc_w 'autofill(): total failed views: '
    //   375: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   378: pop
    //   379: aload_2
    //   380: aload #8
    //   382: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   385: pop
    //   386: ldc 'AutofillManager'
    //   388: aload_2
    //   389: invokevirtual toString : ()Ljava/lang/String;
    //   392: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   395: pop
    //   396: aload_0
    //   397: getfield mService : Landroid/view/autofill/IAutoFillManager;
    //   400: aload_0
    //   401: getfield mSessionId : I
    //   404: aload #8
    //   406: aload_0
    //   407: getfield mContext : Landroid/content/Context;
    //   410: invokevirtual getUserId : ()I
    //   413: invokeinterface setAutofillFailure : (ILjava/util/List;I)V
    //   418: goto -> 427
    //   421: astore_2
    //   422: aload_2
    //   423: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   426: athrow
    //   427: iload_1
    //   428: istore #17
    //   430: aload #11
    //   432: ifnull -> 491
    //   435: iconst_0
    //   436: istore #10
    //   438: iload_1
    //   439: istore #17
    //   441: iload #10
    //   443: aload #11
    //   445: invokevirtual size : ()I
    //   448: if_icmpge -> 491
    //   451: aload #11
    //   453: iload #10
    //   455: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   458: checkcast android/view/View
    //   461: astore_2
    //   462: aload #11
    //   464: iload #10
    //   466: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   469: checkcast android/util/SparseArray
    //   472: astore_3
    //   473: aload_2
    //   474: aload_3
    //   475: invokevirtual autofill : (Landroid/util/SparseArray;)V
    //   478: iload_1
    //   479: aload_3
    //   480: invokevirtual size : ()I
    //   483: iadd
    //   484: istore_1
    //   485: iinc #10, 1
    //   488: goto -> 438
    //   491: aload_0
    //   492: getfield mMetricsLogger : Lcom/android/internal/logging/MetricsLogger;
    //   495: astore_2
    //   496: aload_0
    //   497: sipush #913
    //   500: invokespecial newLog : (I)Landroid/metrics/LogMaker;
    //   503: astore_3
    //   504: aload_3
    //   505: sipush #914
    //   508: iload #7
    //   510: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   513: invokevirtual addTaggedData : (ILjava/lang/Object;)Landroid/metrics/LogMaker;
    //   516: astore_3
    //   517: aload_3
    //   518: sipush #915
    //   521: iload #17
    //   523: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   526: invokevirtual addTaggedData : (ILjava/lang/Object;)Landroid/metrics/LogMaker;
    //   529: astore_3
    //   530: aload_2
    //   531: aload_3
    //   532: invokevirtual write : (Landroid/metrics/LogMaker;)V
    //   535: aload #5
    //   537: monitorexit
    //   538: return
    //   539: astore_2
    //   540: aload #5
    //   542: monitorexit
    //   543: aload_2
    //   544: athrow
    //   545: astore_2
    //   546: goto -> 540
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2231	-> 0
    //   #2232	-> 9
    //   #2233	-> 17
    //   #2236	-> 21
    //   #2237	-> 27
    //   #2238	-> 32
    //   #2241	-> 36
    //   #2242	-> 44
    //   #2243	-> 44
    //   #2244	-> 44
    //   #2245	-> 44
    //   #2244	-> 50
    //   #2247	-> 61
    //   #2249	-> 61
    //   #2250	-> 79
    //   #2251	-> 92
    //   #2252	-> 105
    //   #2253	-> 112
    //   #2257	-> 117
    //   #2258	-> 155
    //   #2259	-> 164
    //   #2261	-> 174
    //   #2262	-> 182
    //   #2264	-> 189
    //   #2265	-> 197
    //   #2267	-> 206
    //   #2269	-> 217
    //   #2270	-> 229
    //   #2272	-> 238
    //   #2273	-> 249
    //   #2275	-> 259
    //   #2276	-> 271
    //   #2278	-> 278
    //   #2279	-> 285
    //   #2281	-> 306
    //   #2283	-> 318
    //   #2289	-> 325
    //   #2291	-> 335
    //   #2249	-> 338
    //   #2323	-> 344
    //   #2249	-> 352
    //   #2295	-> 352
    //   #2296	-> 357
    //   #2297	-> 363
    //   #2300	-> 396
    //   #2306	-> 418
    //   #2301	-> 421
    //   #2305	-> 422
    //   #2309	-> 427
    //   #2310	-> 435
    //   #2311	-> 451
    //   #2312	-> 462
    //   #2313	-> 473
    //   #2314	-> 478
    //   #2310	-> 485
    //   #2320	-> 491
    //   #2321	-> 504
    //   #2322	-> 517
    //   #2320	-> 530
    //   #2323	-> 535
    //   #2324	-> 538
    //   #2323	-> 539
    // Exception table:
    //   from	to	target	type
    //   9	17	539	finally
    //   17	20	539	finally
    //   21	27	539	finally
    //   32	35	539	finally
    //   36	44	539	finally
    //   44	50	539	finally
    //   50	61	539	finally
    //   79	92	348	finally
    //   92	105	344	finally
    //   117	155	344	finally
    //   164	174	344	finally
    //   174	182	344	finally
    //   189	197	344	finally
    //   206	217	344	finally
    //   217	229	344	finally
    //   238	249	344	finally
    //   249	259	344	finally
    //   259	271	344	finally
    //   278	285	344	finally
    //   285	306	344	finally
    //   306	318	344	finally
    //   318	325	344	finally
    //   325	335	545	finally
    //   357	363	545	finally
    //   363	396	545	finally
    //   396	418	421	android/os/RemoteException
    //   396	418	545	finally
    //   422	427	545	finally
    //   441	451	545	finally
    //   451	462	545	finally
    //   462	473	545	finally
    //   473	478	545	finally
    //   478	485	545	finally
    //   491	504	545	finally
    //   504	517	545	finally
    //   517	530	545	finally
    //   530	535	545	finally
    //   535	538	545	finally
    //   540	543	545	finally
  }
  
  private LogMaker newLog(int paramInt) {
    LogMaker logMaker = new LogMaker(paramInt);
    paramInt = this.mSessionId;
    logMaker = logMaker.addTaggedData(1456, Integer.valueOf(paramInt));
    if (isCompatibilityModeEnabledLocked())
      logMaker.addTaggedData(1414, Integer.valueOf(1)); 
    AutofillClient autofillClient = getClient();
    if (autofillClient == null) {
      logMaker.setPackageName(this.mContext.getPackageName());
    } else {
      logMaker.setComponentName(autofillClient.autofillClientGetComponentName());
    } 
    return logMaker;
  }
  
  private void setTrackedViews(int paramInt, AutofillId[] paramArrayOfAutofillId1, boolean paramBoolean1, boolean paramBoolean2, AutofillId[] paramArrayOfAutofillId2, AutofillId paramAutofillId) {
    if (paramAutofillId != null)
      paramAutofillId.resetSessionId(); 
    synchronized (this.mLock) {
      if (Helper.sVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setTrackedViews(): sessionId=");
        stringBuilder.append(paramInt);
        stringBuilder.append(", trackedIds=");
        stringBuilder.append(Arrays.toString((Object[])paramArrayOfAutofillId1));
        stringBuilder.append(", saveOnAllViewsInvisible=");
        stringBuilder.append(paramBoolean1);
        stringBuilder.append(", saveOnFinish=");
        stringBuilder.append(paramBoolean2);
        stringBuilder.append(", fillableIds=");
        stringBuilder.append(Arrays.toString((Object[])paramArrayOfAutofillId2));
        stringBuilder.append(", saveTrigerId=");
        stringBuilder.append(paramAutofillId);
        stringBuilder.append(", mFillableIds=");
        stringBuilder.append(this.mFillableIds);
        stringBuilder.append(", mEnabled=");
        stringBuilder.append(this.mEnabled);
        stringBuilder.append(", mSessionId=");
        stringBuilder.append(this.mSessionId);
        String str = stringBuilder.toString();
        Log.v("AutofillManager", str);
      } 
      if (this.mEnabled && this.mSessionId == paramInt) {
        if (paramBoolean1) {
          TrackedViews trackedViews = new TrackedViews();
          this(this, paramArrayOfAutofillId1);
          this.mTrackedViews = trackedViews;
        } else {
          this.mTrackedViews = null;
        } 
        this.mSaveOnFinish = paramBoolean2;
        if (paramArrayOfAutofillId2 != null) {
          if (this.mFillableIds == null) {
            ArraySet<AutofillId> arraySet = new ArraySet();
            this(paramArrayOfAutofillId2.length);
            this.mFillableIds = arraySet;
          } 
          for (int i = paramArrayOfAutofillId2.length; paramInt < i; ) {
            AutofillId autofillId = paramArrayOfAutofillId2[paramInt];
            autofillId.resetSessionId();
            this.mFillableIds.add(autofillId);
            paramInt++;
          } 
        } 
        if (this.mSaveTriggerId != null && !this.mSaveTriggerId.equals(paramAutofillId))
          setNotifyOnClickLocked(this.mSaveTriggerId, false); 
        if (paramAutofillId != null && !paramAutofillId.equals(this.mSaveTriggerId)) {
          this.mSaveTriggerId = paramAutofillId;
          setNotifyOnClickLocked(paramAutofillId, true);
        } 
      } 
      return;
    } 
  }
  
  private void setNotifyOnClickLocked(AutofillId paramAutofillId, boolean paramBoolean) {
    StringBuilder stringBuilder;
    View view = findView(paramAutofillId);
    if (view == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("setNotifyOnClick(): invalid id: ");
      stringBuilder.append(paramAutofillId);
      Log.w("AutofillManager", stringBuilder.toString());
      return;
    } 
    stringBuilder.setNotifyAutofillManagerOnClick(paramBoolean);
  }
  
  private void setSaveUiState(int paramInt, boolean paramBoolean) {
    if (Helper.sDebug) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSaveUiState(");
      stringBuilder.append(paramInt);
      stringBuilder.append("): ");
      stringBuilder.append(paramBoolean);
      Log.d("AutofillManager", stringBuilder.toString());
    } 
    synchronized (this.mLock) {
      if (this.mSessionId != Integer.MAX_VALUE) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setSaveUiState(");
        stringBuilder.append(paramInt);
        stringBuilder.append(", ");
        stringBuilder.append(paramBoolean);
        stringBuilder.append(") called on existing session ");
        stringBuilder.append(this.mSessionId);
        stringBuilder.append("; cancelling it");
        Log.w("AutofillManager", stringBuilder.toString());
        cancelSessionLocked();
      } 
      if (paramBoolean) {
        this.mSessionId = paramInt;
        this.mState = 3;
      } else {
        this.mSessionId = Integer.MAX_VALUE;
        this.mState = 0;
      } 
      return;
    } 
  }
  
  private void setSessionFinished(int paramInt, List<AutofillId> paramList) {
    if (paramList != null)
      for (byte b = 0; b < paramList.size(); b++)
        ((AutofillId)paramList.get(b)).resetSessionId();  
    synchronized (this.mLock) {
      if (Helper.sVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setSessionFinished(): from ");
        stringBuilder.append(getStateAsStringLocked());
        stringBuilder.append(" to ");
        stringBuilder.append(getStateAsString(paramInt));
        stringBuilder.append("; autofillableIds=");
        stringBuilder.append(paramList);
        String str = stringBuilder.toString();
        Log.v("AutofillManager", str);
      } 
      if (paramList != null) {
        ArraySet<AutofillId> arraySet = new ArraySet();
        this((Collection)paramList);
        this.mEnteredIds = arraySet;
      } 
      if (paramInt == 5 || paramInt == 6) {
        resetSessionLocked(true);
        this.mState = 0;
      } else {
        resetSessionLocked(false);
        this.mState = paramInt;
      } 
      return;
    } 
  }
  
  private void getAugmentedAutofillClient(IResultReceiver paramIResultReceiver) {
    synchronized (this.mLock) {
      if (this.mAugmentedAutofillServiceClient == null) {
        AugmentedAutofillManagerClient augmentedAutofillManagerClient = new AugmentedAutofillManagerClient();
        this(this);
        this.mAugmentedAutofillServiceClient = augmentedAutofillManagerClient;
      } 
      Bundle bundle = new Bundle();
      this();
      IAugmentedAutofillManagerClient iAugmentedAutofillManagerClient = this.mAugmentedAutofillServiceClient;
      IBinder iBinder = iAugmentedAutofillManagerClient.asBinder();
      bundle.putBinder("android.view.autofill.extra.AUGMENTED_AUTOFILL_CLIENT", iBinder);
      try {
        paramIResultReceiver.send(0, bundle);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Could not send AugmentedAutofillClient back: ");
        stringBuilder.append(remoteException);
        Log.w("AutofillManager", stringBuilder.toString());
      } 
      return;
    } 
  }
  
  private void requestShowSoftInput(AutofillId paramAutofillId) {
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("requestShowSoftInput(");
      stringBuilder.append(paramAutofillId);
      stringBuilder.append(")");
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    AutofillClient autofillClient = getClient();
    if (autofillClient == null)
      return; 
    View view = autofillClient.autofillClientFindViewByAutofillIdTraversal(paramAutofillId);
    if (view == null) {
      if (Helper.sVerbose)
        Log.v("AutofillManager", "View is not found"); 
      return;
    } 
    Handler handler = view.getHandler();
    if (handler == null) {
      if (Helper.sVerbose)
        Log.v("AutofillManager", "Ignoring requestShowSoftInput due to no handler in view"); 
      return;
    } 
    if (handler.getLooper() != Looper.myLooper()) {
      if (Helper.sVerbose)
        Log.v("AutofillManager", "Scheduling showSoftInput() on the view UI thread"); 
      handler.post(new _$$Lambda$AutofillManager$yUWPXQETqEjLvLaUNJn6ewweoC8(view));
    } else {
      requestShowSoftInputInViewThread(view);
    } 
  }
  
  private static void requestShowSoftInputInViewThread(View paramView) {
    if (!paramView.isFocused()) {
      Log.w("AutofillManager", "Ignoring requestShowSoftInput() due to non-focused view");
      return;
    } 
    InputMethodManager inputMethodManager = (InputMethodManager)paramView.getContext().getSystemService(InputMethodManager.class);
    boolean bool = inputMethodManager.showSoftInput(paramView, 0);
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" InputMethodManager.showSoftInput returns ");
      stringBuilder.append(bool);
      Log.v("AutofillManager", stringBuilder.toString());
    } 
  }
  
  public void requestHideFillUi() {
    requestHideFillUi(this.mIdShownFillUi, true);
  }
  
  private void requestHideFillUi(AutofillId paramAutofillId, boolean paramBoolean) {
    AutofillClient autofillClient;
    View view;
    if (paramAutofillId == null) {
      view = null;
    } else {
      view = findView(paramAutofillId);
    } 
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("requestHideFillUi(");
      stringBuilder.append(paramAutofillId);
      stringBuilder.append("): anchor = ");
      stringBuilder.append(view);
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    if (view == null) {
      if (paramBoolean) {
        autofillClient = getClient();
        if (autofillClient != null)
          autofillClient.autofillClientRequestHideFillUi(); 
      } 
      return;
    } 
    requestHideFillUi((AutofillId)autofillClient, view);
  }
  
  private void requestHideFillUi(AutofillId paramAutofillId, View paramView) {
    AutofillCallback autofillCallback = null;
    synchronized (this.mLock) {
      AutofillClient autofillClient = getClient();
      AutofillCallback autofillCallback1 = autofillCallback;
      if (autofillClient != null) {
        autofillCallback1 = autofillCallback;
        if (autofillClient.autofillClientRequestHideFillUi()) {
          this.mIdShownFillUi = null;
          autofillCallback1 = this.mCallback;
        } 
      } 
      if (autofillCallback1 != null)
        if (paramAutofillId.isVirtualInt()) {
          autofillCallback1.onAutofillEvent(paramView, paramAutofillId.getVirtualChildIntId(), 2);
        } else {
          autofillCallback1.onAutofillEvent(paramView, 2);
        }  
      return;
    } 
  }
  
  private void notifyDisableAutofill(long paramLong, ComponentName paramComponentName) {
    synchronized (this.mLock) {
      if (this.mOptions == null)
        return; 
      long l = SystemClock.elapsedRealtime() + paramLong;
      paramLong = l;
      if (l < 0L)
        paramLong = Long.MAX_VALUE; 
      if (paramComponentName != null) {
        if (this.mOptions.disabledActivities == null) {
          AutofillOptions autofillOptions = this.mOptions;
          ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
          this();
          autofillOptions.disabledActivities = arrayMap;
        } 
        this.mOptions.disabledActivities.put(paramComponentName.flattenToString(), Long.valueOf(paramLong));
      } else {
        this.mOptions.appDisabledExpiration = paramLong;
      } 
      return;
    } 
  }
  
  void notifyReenableAutofill() {
    synchronized (this.mLock) {
      if (this.mOptions == null)
        return; 
      this.mOptions.appDisabledExpiration = 0L;
      this.mOptions.disabledActivities = null;
      return;
    } 
  }
  
  private void notifyNoFillUi(int paramInt1, AutofillId paramAutofillId, int paramInt2) {
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifyNoFillUi(): sessionFinishedState=");
      stringBuilder.append(paramInt2);
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    View view = findView(paramAutofillId);
    if (view == null)
      return; 
    notifyCallback(paramInt1, paramAutofillId, 3);
    if (paramInt2 != 0)
      setSessionFinished(paramInt2, null); 
  }
  
  private void notifyCallback(int paramInt1, AutofillId paramAutofillId, int paramInt2) {
    // Byte code:
    //   0: getstatic android/view/autofill/Helper.sVerbose : Z
    //   3: ifeq -> 74
    //   6: new java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore #4
    //   15: aload #4
    //   17: ldc_w 'notifyCallback(): sessionId='
    //   20: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: pop
    //   24: aload #4
    //   26: iload_1
    //   27: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #4
    //   33: ldc_w ', autofillId='
    //   36: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload #4
    //   42: aload_2
    //   43: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: aload #4
    //   49: ldc_w ', event='
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload #4
    //   58: iload_3
    //   59: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   62: pop
    //   63: ldc 'AutofillManager'
    //   65: aload #4
    //   67: invokevirtual toString : ()Ljava/lang/String;
    //   70: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   73: pop
    //   74: aload_0
    //   75: aload_2
    //   76: invokespecial findView : (Landroid/view/autofill/AutofillId;)Landroid/view/View;
    //   79: astore #5
    //   81: aload #5
    //   83: ifnonnull -> 87
    //   86: return
    //   87: aconst_null
    //   88: astore #6
    //   90: aload_0
    //   91: getfield mLock : Ljava/lang/Object;
    //   94: astore #7
    //   96: aload #7
    //   98: monitorenter
    //   99: aload #6
    //   101: astore #4
    //   103: aload_0
    //   104: getfield mSessionId : I
    //   107: iload_1
    //   108: if_icmpne -> 128
    //   111: aload #6
    //   113: astore #4
    //   115: aload_0
    //   116: invokespecial getClient : ()Landroid/view/autofill/AutofillManager$AutofillClient;
    //   119: ifnull -> 128
    //   122: aload_0
    //   123: getfield mCallback : Landroid/view/autofill/AutofillManager$AutofillCallback;
    //   126: astore #4
    //   128: aload #7
    //   130: monitorexit
    //   131: aload #4
    //   133: ifnull -> 168
    //   136: aload_2
    //   137: invokevirtual isVirtualInt : ()Z
    //   140: ifeq -> 160
    //   143: aload_2
    //   144: invokevirtual getVirtualChildIntId : ()I
    //   147: istore_1
    //   148: aload #4
    //   150: aload #5
    //   152: iload_1
    //   153: iload_3
    //   154: invokevirtual onAutofillEvent : (Landroid/view/View;II)V
    //   157: goto -> 168
    //   160: aload #4
    //   162: aload #5
    //   164: iload_3
    //   165: invokevirtual onAutofillEvent : (Landroid/view/View;I)V
    //   168: return
    //   169: astore_2
    //   170: aload #7
    //   172: monitorexit
    //   173: aload_2
    //   174: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2629	-> 0
    //   #2630	-> 6
    //   #2633	-> 74
    //   #2634	-> 81
    //   #2635	-> 86
    //   #2638	-> 87
    //   #2639	-> 90
    //   #2640	-> 99
    //   #2641	-> 122
    //   #2643	-> 128
    //   #2645	-> 131
    //   #2646	-> 136
    //   #2647	-> 143
    //   #2648	-> 143
    //   #2647	-> 148
    //   #2650	-> 160
    //   #2653	-> 168
    //   #2643	-> 169
    // Exception table:
    //   from	to	target	type
    //   103	111	169	finally
    //   115	122	169	finally
    //   122	128	169	finally
    //   128	131	169	finally
    //   170	173	169	finally
  }
  
  private View findView(AutofillId paramAutofillId) {
    AutofillClient autofillClient = getClient();
    if (autofillClient != null)
      return autofillClient.autofillClientFindViewByAutofillIdTraversal(paramAutofillId); 
    return null;
  }
  
  public boolean hasAutofillFeature() {
    boolean bool;
    if (this.mService != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onPendingSaveUi(int paramInt, IBinder paramIBinder) {
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onPendingSaveUi(");
      stringBuilder.append(paramInt);
      stringBuilder.append("): ");
      stringBuilder.append(paramIBinder);
      Log.v("AutofillManager", stringBuilder.toString());
    } 
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      this.mService.onPendingSaveUi(paramInt, paramIBinder);
    } catch (RemoteException remoteException) {
      Log.e("AutofillManager", "Error in onPendingSaveUi: ", (Throwable)remoteException);
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    boolean bool2;
    paramPrintWriter.print(paramString);
    paramPrintWriter.println("AutofillManager:");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("  ");
    paramString = stringBuilder.toString();
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("sessionId: ");
    paramPrintWriter.println(this.mSessionId);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("state: ");
    paramPrintWriter.println(getStateAsStringLocked());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("context: ");
    paramPrintWriter.println(this.mContext);
    AutofillClient autofillClient = getClient();
    if (autofillClient != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("client: ");
      paramPrintWriter.print(autofillClient);
      paramPrintWriter.print(" (");
      paramPrintWriter.print(autofillClient.autofillClientGetActivityToken());
      paramPrintWriter.println(')');
    } 
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("enabled: ");
    paramPrintWriter.println(this.mEnabled);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("enabledAugmentedOnly: ");
    paramPrintWriter.println(this.mForAugmentedAutofillOnly);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("hasService: ");
    IAutoFillManager iAutoFillManager = this.mService;
    boolean bool1 = true;
    if (iAutoFillManager != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramPrintWriter.println(bool2);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("hasCallback: ");
    if (this.mCallback != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    paramPrintWriter.println(bool2);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("onInvisibleCalled ");
    paramPrintWriter.println(this.mOnInvisibleCalled);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("last autofilled data: ");
    paramPrintWriter.println(this.mLastAutofilledData);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("id of last fill UI shown: ");
    paramPrintWriter.println(this.mIdShownFillUi);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("tracked views: ");
    if (this.mTrackedViews == null) {
      paramPrintWriter.println("null");
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  ");
      String str = stringBuilder1.toString();
      paramPrintWriter.println();
      paramPrintWriter.print(str);
      paramPrintWriter.print("visible:");
      paramPrintWriter.println(this.mTrackedViews.mVisibleTrackedIds);
      paramPrintWriter.print(str);
      paramPrintWriter.print("invisible:");
      paramPrintWriter.println(this.mTrackedViews.mInvisibleTrackedIds);
    } 
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("fillable ids: ");
    paramPrintWriter.println(this.mFillableIds);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("entered ids: ");
    paramPrintWriter.println(this.mEnteredIds);
    if (this.mEnteredForAugmentedAutofillIds != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("entered ids for augmented autofill: ");
      paramPrintWriter.println(this.mEnteredForAugmentedAutofillIds);
    } 
    if (this.mForAugmentedAutofillOnly) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("For Augmented Autofill Only");
    } 
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("save trigger id: ");
    paramPrintWriter.println(this.mSaveTriggerId);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("save on finish(): ");
    paramPrintWriter.println(this.mSaveOnFinish);
    if (this.mOptions != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("options: ");
      this.mOptions.dumpShort(paramPrintWriter);
      paramPrintWriter.println();
    } 
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("compat mode enabled: ");
    synchronized (this.mLock) {
      if (this.mCompatibilityBridge != null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append(paramString);
        stringBuilder1.append("  ");
        String str = stringBuilder1.toString();
        paramPrintWriter.println("true");
        paramPrintWriter.print(str);
        paramPrintWriter.print("windowId: ");
        paramPrintWriter.println(this.mCompatibilityBridge.mFocusedWindowId);
        paramPrintWriter.print(str);
        paramPrintWriter.print("nodeId: ");
        paramPrintWriter.println(this.mCompatibilityBridge.mFocusedNodeId);
        paramPrintWriter.print(str);
        paramPrintWriter.print("virtualId: ");
        CompatibilityBridge compatibilityBridge = this.mCompatibilityBridge;
        int i = AccessibilityNodeInfo.getVirtualDescendantId(compatibilityBridge.mFocusedNodeId);
        paramPrintWriter.println(i);
        paramPrintWriter.print(str);
        paramPrintWriter.print("focusedBounds: ");
        paramPrintWriter.println(this.mCompatibilityBridge.mFocusedBounds);
      } else {
        paramPrintWriter.println("false");
      } 
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("debug: ");
      paramPrintWriter.print(Helper.sDebug);
      paramPrintWriter.print(" verbose: ");
      paramPrintWriter.println(Helper.sVerbose);
      return;
    } 
  }
  
  private String getStateAsStringLocked() {
    return getStateAsString(this.mState);
  }
  
  private static String getStateAsString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("INVALID:");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 6:
        return "UNKNOWN_FAILED";
      case 5:
        return "UNKNOWN_COMPAT_MODE";
      case 4:
        return "DISABLED_BY_SERVICE";
      case 3:
        return "SHOWING_SAVE_UI";
      case 2:
        return "FINISHED";
      case 1:
        return "ACTIVE";
      case 0:
        break;
    } 
    return "UNKNOWN";
  }
  
  public static String getSmartSuggestionModeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INVALID:");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      } 
      return "SYSTEM";
    } 
    return "OFF";
  }
  
  private boolean isActiveLocked() {
    int i = this.mState;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  private boolean isDisabledByServiceLocked() {
    boolean bool;
    if (this.mState == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isFinishedLocked() {
    boolean bool;
    if (this.mState == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void post(Runnable paramRunnable) {
    AutofillClient autofillClient = getClient();
    if (autofillClient == null) {
      if (Helper.sVerbose)
        Log.v("AutofillManager", "ignoring post() because client is null"); 
      return;
    } 
    autofillClient.autofillClientRunOnUiThread(paramRunnable);
  }
  
  class CompatibilityBridge implements AccessibilityManager.AccessibilityPolicy {
    private final Rect mFocusedBounds = new Rect();
    
    private final Rect mTempBounds = new Rect();
    
    private int mFocusedWindowId = -1;
    
    private long mFocusedNodeId = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
    
    AccessibilityServiceInfo mCompatServiceInfo;
    
    final AutofillManager this$0;
    
    CompatibilityBridge() {
      AccessibilityManager accessibilityManager = AccessibilityManager.getInstance(AutofillManager.this.mContext);
      accessibilityManager.setAccessibilityPolicy(this);
    }
    
    private AccessibilityServiceInfo getCompatServiceInfo() {
      synchronized (AutofillManager.this.mLock) {
        if (this.mCompatServiceInfo != null)
          return this.mCompatServiceInfo; 
        Intent intent = new Intent();
        this();
        ComponentName componentName = new ComponentName();
        this("android", "com.android.server.autofill.AutofillCompatAccessibilityService");
        intent.setComponent(componentName);
        ResolveInfo resolveInfo = AutofillManager.this.mContext.getPackageManager().resolveService(intent, 1048704);
        try {
          AccessibilityServiceInfo accessibilityServiceInfo = new AccessibilityServiceInfo();
          this(resolveInfo, AutofillManager.this.mContext);
          this.mCompatServiceInfo = accessibilityServiceInfo;
          return accessibilityServiceInfo;
        } catch (XmlPullParserException|java.io.IOException xmlPullParserException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Cannot find compat autofill service:");
          stringBuilder.append(intent);
          Log.e("AutofillManager", stringBuilder.toString());
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Cannot find compat autofill service");
          throw illegalStateException;
        } 
      } 
    }
    
    public boolean isEnabled(boolean param1Boolean) {
      return true;
    }
    
    public int getRelevantEventTypes(int param1Int) {
      return param1Int | 0x8 | 0x10 | 0x1 | 0x800;
    }
    
    public List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList(List<AccessibilityServiceInfo> param1List) {
      List<AccessibilityServiceInfo> list = param1List;
      if (param1List == null)
        list = new ArrayList<>(); 
      list.add(getCompatServiceInfo());
      return list;
    }
    
    public List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int param1Int, List<AccessibilityServiceInfo> param1List) {
      List<AccessibilityServiceInfo> list = param1List;
      if (param1List == null)
        list = new ArrayList<>(); 
      list.add(getCompatServiceInfo());
      return list;
    }
    
    public AccessibilityEvent onAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent, boolean param1Boolean, int param1Int) {
      param1Int = param1AccessibilityEvent.getEventType();
      if (Helper.sVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onAccessibilityEvent(");
        stringBuilder.append(AccessibilityEvent.eventTypeToString(param1Int));
        stringBuilder.append("): virtualId=");
        stringBuilder.append(AccessibilityNodeInfo.getVirtualDescendantId(param1AccessibilityEvent.getSourceNodeId()));
        stringBuilder.append(", client=");
        AutofillManager autofillManager = AutofillManager.this;
        stringBuilder.append(autofillManager.getClient());
        String str = stringBuilder.toString();
        Log.v("AutofillManager", str);
      } 
      if (param1Int != 1) {
        if (param1Int != 8) {
          if (param1Int != 16) {
            if (param1Int == 2048) {
              AutofillManager.AutofillClient autofillClient = AutofillManager.this.getClient();
              if (autofillClient != null)
                synchronized (AutofillManager.this.mLock) {
                  if (autofillClient.autofillClientIsFillUiShowing())
                    notifyViewEntered(this.mFocusedWindowId, this.mFocusedNodeId, this.mFocusedBounds); 
                  updateTrackedViewsLocked();
                }  
            } 
          } else {
            synchronized (AutofillManager.this.mLock) {
              if (this.mFocusedWindowId == param1AccessibilityEvent.getWindowId()) {
                long l = this.mFocusedNodeId;
                if (l == param1AccessibilityEvent.getSourceNodeId())
                  notifyValueChanged(param1AccessibilityEvent.getWindowId(), param1AccessibilityEvent.getSourceNodeId()); 
              } 
            } 
          } 
        } else {
          synchronized (AutofillManager.this.mLock) {
            if (this.mFocusedWindowId == param1AccessibilityEvent.getWindowId()) {
              long l1 = this.mFocusedNodeId;
              if (l1 == param1AccessibilityEvent.getSourceNodeId())
                return param1AccessibilityEvent; 
            } 
            if (this.mFocusedWindowId != -1 && this.mFocusedNodeId != AccessibilityNodeInfo.UNDEFINED_NODE_ID) {
              notifyViewExited(this.mFocusedWindowId, this.mFocusedNodeId);
              this.mFocusedWindowId = -1;
              this.mFocusedNodeId = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
              this.mFocusedBounds.set(0, 0, 0, 0);
            } 
            param1Int = param1AccessibilityEvent.getWindowId();
            long l = param1AccessibilityEvent.getSourceNodeId();
            if (notifyViewEntered(param1Int, l, this.mFocusedBounds)) {
              this.mFocusedWindowId = param1Int;
              this.mFocusedNodeId = l;
            } 
          } 
        } 
      } else {
        synchronized (AutofillManager.this.mLock) {
          notifyViewClicked(param1AccessibilityEvent.getWindowId(), param1AccessibilityEvent.getSourceNodeId());
          if (!param1Boolean)
            param1AccessibilityEvent = null; 
          return param1AccessibilityEvent;
        } 
      } 
      if (!param1Boolean)
        param1AccessibilityEvent = null; 
      return param1AccessibilityEvent;
    }
    
    private boolean notifyViewEntered(int param1Int, long param1Long, Rect param1Rect) {
      int i = AccessibilityNodeInfo.getVirtualDescendantId(param1Long);
      if (!isVirtualNode(i))
        return false; 
      View view = findViewByAccessibilityId(param1Int, param1Long);
      if (view == null)
        return false; 
      AccessibilityNodeInfo accessibilityNodeInfo = findVirtualNodeByAccessibilityId(view, i);
      if (accessibilityNodeInfo == null)
        return false; 
      if (!accessibilityNodeInfo.isEditable())
        return false; 
      Rect rect = this.mTempBounds;
      accessibilityNodeInfo.getBoundsInScreen(rect);
      if (rect.equals(param1Rect))
        return false; 
      param1Rect.set(rect);
      AutofillManager.this.notifyViewEntered(view, i, rect);
      return true;
    }
    
    private void notifyViewExited(int param1Int, long param1Long) {
      int i = AccessibilityNodeInfo.getVirtualDescendantId(param1Long);
      if (!isVirtualNode(i))
        return; 
      View view = findViewByAccessibilityId(param1Int, param1Long);
      if (view == null)
        return; 
      AutofillManager.this.notifyViewExited(view, i);
    }
    
    private void notifyValueChanged(int param1Int, long param1Long) {
      int i = AccessibilityNodeInfo.getVirtualDescendantId(param1Long);
      if (!isVirtualNode(i))
        return; 
      View view = findViewByAccessibilityId(param1Int, param1Long);
      if (view == null)
        return; 
      AccessibilityNodeInfo accessibilityNodeInfo = findVirtualNodeByAccessibilityId(view, i);
      if (accessibilityNodeInfo == null)
        return; 
      AutofillManager autofillManager = AutofillManager.this;
      AutofillValue autofillValue = AutofillValue.forText(accessibilityNodeInfo.getText());
      autofillManager.notifyValueChanged(view, i, autofillValue);
    }
    
    private void notifyViewClicked(int param1Int, long param1Long) {
      int i = AccessibilityNodeInfo.getVirtualDescendantId(param1Long);
      if (!isVirtualNode(i))
        return; 
      View view = findViewByAccessibilityId(param1Int, param1Long);
      if (view == null)
        return; 
      AccessibilityNodeInfo accessibilityNodeInfo = findVirtualNodeByAccessibilityId(view, i);
      if (accessibilityNodeInfo == null)
        return; 
      AutofillManager.this.notifyViewClicked(view, i);
    }
    
    private void updateTrackedViewsLocked() {
      if (AutofillManager.this.mTrackedViews != null)
        AutofillManager.this.mTrackedViews.onVisibleForAutofillChangedLocked(); 
    }
    
    private View findViewByAccessibilityId(int param1Int, long param1Long) {
      AutofillManager.AutofillClient autofillClient = AutofillManager.this.getClient();
      if (autofillClient == null)
        return null; 
      int i = AccessibilityNodeInfo.getAccessibilityViewId(param1Long);
      return autofillClient.autofillClientFindViewByAccessibilityIdTraversal(i, param1Int);
    }
    
    private AccessibilityNodeInfo findVirtualNodeByAccessibilityId(View param1View, int param1Int) {
      AccessibilityNodeProvider accessibilityNodeProvider = param1View.getAccessibilityNodeProvider();
      if (accessibilityNodeProvider == null)
        return null; 
      return accessibilityNodeProvider.createAccessibilityNodeInfo(param1Int);
    }
    
    private boolean isVirtualNode(int param1Int) {
      boolean bool;
      if (param1Int != -1 && param1Int != Integer.MAX_VALUE) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  private class TrackedViews {
    private ArraySet<AutofillId> mInvisibleTrackedIds;
    
    private ArraySet<AutofillId> mVisibleTrackedIds;
    
    final AutofillManager this$0;
    
    private <T> boolean isInSet(ArraySet<T> param1ArraySet, T param1T) {
      boolean bool;
      if (param1ArraySet != null && param1ArraySet.contains(param1T)) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private <T> ArraySet<T> addToSet(ArraySet<T> param1ArraySet, T param1T) {
      ArraySet<T> arraySet = param1ArraySet;
      if (param1ArraySet == null)
        arraySet = new ArraySet<>(1); 
      arraySet.add(param1T);
      return arraySet;
    }
    
    private <T> ArraySet<T> removeFromSet(ArraySet<T> param1ArraySet, T param1T) {
      if (param1ArraySet == null)
        return null; 
      param1ArraySet.remove(param1T);
      if (param1ArraySet.isEmpty())
        return null; 
      return param1ArraySet;
    }
    
    TrackedViews(AutofillId[] param1ArrayOfAutofillId) {
      AutofillManager.AutofillClient autofillClient = AutofillManager.this.getClient();
      if (!ArrayUtils.isEmpty((Object[])param1ArrayOfAutofillId) && autofillClient != null) {
        boolean[] arrayOfBoolean;
        if (autofillClient.autofillClientIsVisibleForAutofill()) {
          if (Helper.sVerbose)
            Log.v("AutofillManager", "client is visible, check tracked ids"); 
          arrayOfBoolean = autofillClient.autofillClientGetViewVisibility(param1ArrayOfAutofillId);
        } else {
          arrayOfBoolean = new boolean[param1ArrayOfAutofillId.length];
        } 
        int i = param1ArrayOfAutofillId.length;
        for (byte b = 0; b < i; b++) {
          AutofillId autofillId = param1ArrayOfAutofillId[b];
          autofillId.resetSessionId();
          if (arrayOfBoolean[b]) {
            this.mVisibleTrackedIds = addToSet(this.mVisibleTrackedIds, autofillId);
          } else {
            this.mInvisibleTrackedIds = addToSet(this.mInvisibleTrackedIds, autofillId);
          } 
        } 
      } 
      if (Helper.sVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TrackedViews(trackedIds=");
        stringBuilder.append(Arrays.toString((Object[])param1ArrayOfAutofillId));
        stringBuilder.append("):  mVisibleTrackedIds=");
        stringBuilder.append(this.mVisibleTrackedIds);
        stringBuilder.append(" mInvisibleTrackedIds=");
        stringBuilder.append(this.mInvisibleTrackedIds);
        Log.v("AutofillManager", stringBuilder.toString());
      } 
      if (this.mVisibleTrackedIds == null)
        AutofillManager.this.finishSessionLocked(); 
    }
    
    void notifyViewVisibilityChangedLocked(AutofillId param1AutofillId, boolean param1Boolean) {
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("notifyViewVisibilityChangedLocked(): id=");
        stringBuilder.append(param1AutofillId);
        stringBuilder.append(" isVisible=");
        stringBuilder.append(param1Boolean);
        Log.d("AutofillManager", stringBuilder.toString());
      } 
      if (AutofillManager.this.isClientVisibleForAutofillLocked())
        if (param1Boolean) {
          if (isInSet(this.mInvisibleTrackedIds, param1AutofillId)) {
            this.mInvisibleTrackedIds = removeFromSet(this.mInvisibleTrackedIds, param1AutofillId);
            this.mVisibleTrackedIds = addToSet(this.mVisibleTrackedIds, param1AutofillId);
          } 
        } else if (isInSet(this.mVisibleTrackedIds, param1AutofillId)) {
          this.mVisibleTrackedIds = removeFromSet(this.mVisibleTrackedIds, param1AutofillId);
          this.mInvisibleTrackedIds = addToSet(this.mInvisibleTrackedIds, param1AutofillId);
        }  
      if (this.mVisibleTrackedIds == null) {
        if (Helper.sVerbose) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("No more visible ids. Invisibile = ");
          stringBuilder.append(this.mInvisibleTrackedIds);
          Log.v("AutofillManager", stringBuilder.toString());
        } 
        AutofillManager.this.finishSessionLocked();
      } 
    }
    
    void onVisibleForAutofillChangedLocked() {
      AutofillManager.AutofillClient autofillClient = AutofillManager.this.getClient();
      AutofillId[] arrayOfAutofillId1 = null, arrayOfAutofillId2 = null;
      ArraySet<AutofillId> arraySet1 = null, arraySet2 = null;
      if (autofillClient != null) {
        ArraySet<AutofillId> arraySet3, arraySet5;
        if (Helper.sVerbose) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onVisibleForAutofillChangedLocked(): inv= ");
          stringBuilder.append(this.mInvisibleTrackedIds);
          stringBuilder.append(" vis=");
          stringBuilder.append(this.mVisibleTrackedIds);
          Log.v("AutofillManager", stringBuilder.toString());
        } 
        if (this.mInvisibleTrackedIds != null) {
          ArrayList<AutofillId> arrayList = new ArrayList<>(this.mInvisibleTrackedIds);
          arrayOfAutofillId1 = Helper.toArray(arrayList);
          boolean[] arrayOfBoolean = autofillClient.autofillClientGetViewVisibility(arrayOfAutofillId1);
          int i = arrayList.size();
          byte b = 0;
          while (true) {
            arrayOfAutofillId1 = arrayOfAutofillId2;
            arraySet1 = arraySet2;
            if (b < i) {
              AutofillId autofillId = arrayList.get(b);
              if (arrayOfBoolean[b]) {
                arraySet1 = addToSet((ArraySet<AutofillId>)arrayOfAutofillId2, autofillId);
                ArraySet<AutofillId> arraySet = arraySet1;
                arraySet3 = arraySet2;
                if (Helper.sDebug) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("onVisibleForAutofill() ");
                  stringBuilder.append(autofillId);
                  stringBuilder.append(" became visible");
                  Log.d("AutofillManager", stringBuilder.toString());
                  arraySet = arraySet1;
                  ArraySet<AutofillId> arraySet6 = arraySet2;
                } 
              } else {
                arraySet3 = addToSet(arraySet2, autofillId);
              } 
              b++;
              arraySet2 = arraySet3;
              continue;
            } 
            break;
          } 
        } 
        ArraySet<AutofillId> arraySet4 = arraySet3;
        arraySet2 = arraySet1;
        if (this.mVisibleTrackedIds != null) {
          ArrayList<AutofillId> arrayList = new ArrayList<>(this.mVisibleTrackedIds);
          AutofillId[] arrayOfAutofillId = Helper.toArray(arrayList);
          boolean[] arrayOfBoolean = autofillClient.autofillClientGetViewVisibility(arrayOfAutofillId);
          int i = arrayList.size();
          byte b = 0;
          while (true) {
            arraySet4 = arraySet3;
            arraySet5 = arraySet1;
            if (b < i) {
              AutofillId autofillId = arrayList.get(b);
              if (arrayOfBoolean[b]) {
                arraySet5 = addToSet(arraySet3, autofillId);
              } else {
                arraySet4 = addToSet(arraySet1, autofillId);
                arraySet5 = arraySet3;
                arraySet1 = arraySet4;
                if (Helper.sDebug) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("onVisibleForAutofill() ");
                  stringBuilder.append(autofillId);
                  stringBuilder.append(" became invisible");
                  Log.d("AutofillManager", stringBuilder.toString());
                  ArraySet<AutofillId> arraySet = arraySet4;
                  arraySet5 = arraySet3;
                } 
              } 
              b++;
              arraySet3 = arraySet5;
              continue;
            } 
            break;
          } 
        } 
        this.mInvisibleTrackedIds = arraySet5;
        this.mVisibleTrackedIds = arraySet4;
      } 
      if (this.mVisibleTrackedIds == null) {
        if (Helper.sVerbose)
          Log.v("AutofillManager", "onVisibleForAutofillChangedLocked(): no more visible ids"); 
        AutofillManager.this.finishSessionLocked();
      } 
    }
  }
  
  public static abstract class AutofillCallback {
    public static final int EVENT_INPUT_HIDDEN = 2;
    
    public static final int EVENT_INPUT_SHOWN = 1;
    
    public static final int EVENT_INPUT_UNAVAILABLE = 3;
    
    public void onAutofillEvent(View param1View, int param1Int) {}
    
    public void onAutofillEvent(View param1View, int param1Int1, int param1Int2) {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface AutofillEventType {}
  }
  
  class AutofillManagerClient extends IAutoFillManagerClient.Stub {
    private final WeakReference<AutofillManager> mAfm = new WeakReference<>(AutofillManager.this);
    
    public void setState(int param1Int) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$qH36EJk2Hkdja9ZZmTxqYPyr0YA(autofillManager, param1Int)); 
    }
    
    public void autofill(int param1Int, List<AutofillId> param1List, List<AutofillValue> param1List1, boolean param1Boolean) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$M5cIHWp4VjSrFSjWPhPkW66WcgE(autofillManager, param1Int, param1List, param1List1, param1Boolean)); 
    }
    
    public void authenticate(int param1Int1, int param1Int2, IntentSender param1IntentSender, Intent param1Intent, boolean param1Boolean) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$24XrRrXer3uVV1bTpl9qUIIHmJM(autofillManager, param1Int1, param1Int2, param1IntentSender, param1Intent, param1Boolean)); 
    }
    
    public void requestShowFillUi(int param1Int1, AutofillId param1AutofillId, int param1Int2, int param1Int3, Rect param1Rect, IAutofillWindowPresenter param1IAutofillWindowPresenter) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$kRL9XILLc2XNr90gxVDACLzcyqc(autofillManager, param1Int1, param1AutofillId, param1Int2, param1Int3, param1Rect, param1IAutofillWindowPresenter)); 
    }
    
    public void requestHideFillUi(int param1Int, AutofillId param1AutofillId) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$dCTetwfU0gT1ZrSzZGZiGStXlOY(autofillManager, param1AutofillId)); 
    }
    
    public void notifyNoFillUi(int param1Int1, AutofillId param1AutofillId, int param1Int2) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$K79QnIPRaZuikYDQdsLcIUBhqiI(autofillManager, param1Int1, param1AutofillId, param1Int2)); 
    }
    
    public void notifyFillUiShown(int param1Int, AutofillId param1AutofillId) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$C_N0TMarq3R8L6VkaEm7H_n1T7k(autofillManager, param1Int, param1AutofillId)); 
    }
    
    public void notifyFillUiHidden(int param1Int, AutofillId param1AutofillId) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$tXABDazS_gnl4cSTqRq7xAnrZwo(autofillManager, param1Int, param1AutofillId)); 
    }
    
    public void notifyDisableAutofill(long param1Long, ComponentName param1ComponentName) throws RemoteException {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$nGHoEL_C5tEDrLAbg_3JSwDnikk(autofillManager, param1Long, param1ComponentName)); 
    }
    
    public void dispatchUnhandledKey(int param1Int, AutofillId param1AutofillId, KeyEvent param1KeyEvent) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$diRhCbPquG9lbMilczZW3FIsRVQ(autofillManager, param1Int, param1AutofillId, param1KeyEvent)); 
    }
    
    public void startIntentSender(IntentSender param1IntentSender, Intent param1Intent) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$5gqq5d05q4z8TAod0qB3s7DbGQ0(autofillManager, param1IntentSender, param1Intent)); 
    }
    
    public void setTrackedViews(int param1Int, AutofillId[] param1ArrayOfAutofillId1, boolean param1Boolean1, boolean param1Boolean2, AutofillId[] param1ArrayOfAutofillId2, AutofillId param1AutofillId) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$h3jwxJ8O0NJVjgcEdK_Tct9rSeg(autofillManager, param1Int, param1ArrayOfAutofillId1, param1Boolean1, param1Boolean2, param1ArrayOfAutofillId2, param1AutofillId)); 
    }
    
    public void setSaveUiState(int param1Int, boolean param1Boolean) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$APk49Wn0CZsFOdF9dTuuaJaRhy8(autofillManager, param1Int, param1Boolean)); 
    }
    
    public void setSessionFinished(int param1Int, List<AutofillId> param1List) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$6YlLUns4Jn3OXEyrRSzxW7pT0cE(autofillManager, param1Int, param1List)); 
    }
    
    public void getAugmentedAutofillClient(IResultReceiver param1IResultReceiver) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$hCkZWIIgnjguHBxIc5d7zi3IzQY(autofillManager, param1IResultReceiver)); 
    }
    
    public void requestShowSoftInput(AutofillId param1AutofillId) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AutofillManagerClient$5AR3XMrrRE8rxyNGRepInd_vCsE(autofillManager, param1AutofillId)); 
    }
    
    private AutofillManagerClient(AutofillManager this$0) {}
  }
  
  class AugmentedAutofillManagerClient extends IAugmentedAutofillManagerClient.Stub {
    private final WeakReference<AutofillManager> mAfm = new WeakReference<>(AutofillManager.this);
    
    public Rect getViewCoordinates(AutofillId param1AutofillId) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager == null)
        return null; 
      View view = getView(autofillManager, param1AutofillId);
      if (view == null)
        return null; 
      Rect rect1 = new Rect();
      view.getWindowVisibleDisplayFrame(rect1);
      int[] arrayOfInt = new int[2];
      view.getLocationOnScreen(arrayOfInt);
      int i = arrayOfInt[0], j = arrayOfInt[1], k = rect1.top, m = arrayOfInt[0];
      int n = view.getWidth(), i1 = arrayOfInt[1], i2 = rect1.top;
      Rect rect2 = new Rect(i, j - k, m + n, i1 - i2 + view.getHeight());
      if (Helper.sVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Coordinates for ");
        stringBuilder.append(param1AutofillId);
        stringBuilder.append(": ");
        stringBuilder.append(rect2);
        Log.v("AutofillManager", stringBuilder.toString());
      } 
      return rect2;
    }
    
    public void autofill(int param1Int, List<AutofillId> param1List, List<AutofillValue> param1List1, boolean param1Boolean) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AugmentedAutofillManagerClient$4xiNVTyrzh8FT7hUgeGwxhOR4TY(autofillManager, param1Int, param1List, param1List1, param1Boolean)); 
    }
    
    public void requestShowFillUi(int param1Int1, AutofillId param1AutofillId, int param1Int2, int param1Int3, Rect param1Rect, IAutofillWindowPresenter param1IAutofillWindowPresenter) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AugmentedAutofillManagerClient$OrAY5q15e0VwuCSYnsGgs6GcY1U(autofillManager, param1Int1, param1AutofillId, param1Int2, param1Int3, param1Rect, param1IAutofillWindowPresenter)); 
    }
    
    public void requestHideFillUi(int param1Int, AutofillId param1AutofillId) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager != null)
        autofillManager.post(new _$$Lambda$AutofillManager$AugmentedAutofillManagerClient$tbNtqpHgXnRdc3JO5HaBlxclFg0(autofillManager, param1AutofillId)); 
    }
    
    public boolean requestAutofill(int param1Int, AutofillId param1AutofillId) {
      AutofillManager autofillManager = this.mAfm.get();
      if (autofillManager == null || autofillManager.mSessionId != param1Int) {
        if (Helper.sDebug)
          Slog.d("AutofillManager", "Autofill not available or sessionId doesn't match"); 
        return false;
      } 
      View view = getView(autofillManager, param1AutofillId);
      if (view == null || !view.isFocused()) {
        if (Helper.sDebug)
          Slog.d("AutofillManager", "View not available or is not on focus"); 
        return false;
      } 
      if (Helper.sVerbose)
        Log.v("AutofillManager", "requestAutofill() by AugmentedAutofillService."); 
      autofillManager.post(new _$$Lambda$AutofillManager$AugmentedAutofillManagerClient$IV7bcpTcFDYfdRsdqPS_5PQAkX0(autofillManager, view));
      return true;
    }
    
    private View getView(AutofillManager param1AutofillManager, AutofillId param1AutofillId) {
      StringBuilder stringBuilder;
      AutofillManager.AutofillClient autofillClient = param1AutofillManager.getClient();
      if (autofillClient == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("getView(");
        stringBuilder.append(param1AutofillId);
        stringBuilder.append("): no autofill client");
        Log.w("AutofillManager", stringBuilder.toString());
        return null;
      } 
      View view = stringBuilder.autofillClientFindViewByAutofillIdTraversal(param1AutofillId);
      if (view == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("getView(");
        stringBuilder.append(param1AutofillId);
        stringBuilder.append("): could not find view");
        Log.w("AutofillManager", stringBuilder.toString());
      } 
      return view;
    }
    
    private AugmentedAutofillManagerClient(AutofillManager this$0) {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface AutofillEventType {}
  
  public static interface AutofillClient {
    void autofillClientAuthenticate(int param1Int, IntentSender param1IntentSender, Intent param1Intent, boolean param1Boolean);
    
    void autofillClientDispatchUnhandledKey(View param1View, KeyEvent param1KeyEvent);
    
    View autofillClientFindViewByAccessibilityIdTraversal(int param1Int1, int param1Int2);
    
    View autofillClientFindViewByAutofillIdTraversal(AutofillId param1AutofillId);
    
    View[] autofillClientFindViewsByAutofillIdTraversal(AutofillId[] param1ArrayOfAutofillId);
    
    IBinder autofillClientGetActivityToken();
    
    ComponentName autofillClientGetComponentName();
    
    AutofillId autofillClientGetNextAutofillId();
    
    boolean[] autofillClientGetViewVisibility(AutofillId[] param1ArrayOfAutofillId);
    
    boolean autofillClientIsCompatibilityModeEnabled();
    
    boolean autofillClientIsFillUiShowing();
    
    boolean autofillClientIsVisibleForAutofill();
    
    boolean autofillClientRequestHideFillUi();
    
    boolean autofillClientRequestShowFillUi(View param1View, int param1Int1, int param1Int2, Rect param1Rect, IAutofillWindowPresenter param1IAutofillWindowPresenter);
    
    void autofillClientResetableStateAvailable();
    
    void autofillClientRunOnUiThread(Runnable param1Runnable);
    
    boolean isDisablingEnterExitEventForAutofill();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SmartSuggestionMode {}
}
