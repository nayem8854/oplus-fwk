package android.view.autofill;

import android.content.ComponentName;
import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.service.autofill.UserData;
import com.android.internal.os.IResultReceiver;
import java.util.ArrayList;
import java.util.List;

public interface IAutoFillManager extends IInterface {
  void addClient(IAutoFillManagerClient paramIAutoFillManagerClient, ComponentName paramComponentName, int paramInt, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void cancelSession(int paramInt1, int paramInt2) throws RemoteException;
  
  void disableOwnedAutofillServices(int paramInt) throws RemoteException;
  
  void finishSession(int paramInt1, int paramInt2) throws RemoteException;
  
  void getAutofillServiceComponentName(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void getAvailableFieldClassificationAlgorithms(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void getDefaultFieldClassificationAlgorithm(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void getFillEventHistory(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void getUserData(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void getUserDataId(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void isFieldClassificationEnabled(IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void isServiceEnabled(int paramInt, String paramString, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void isServiceSupported(int paramInt, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void onPendingSaveUi(int paramInt, IBinder paramIBinder) throws RemoteException;
  
  void removeClient(IAutoFillManagerClient paramIAutoFillManagerClient, int paramInt) throws RemoteException;
  
  void restoreSession(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void setAugmentedAutofillWhitelist(List<String> paramList, List<ComponentName> paramList1, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void setAuthenticationResult(Bundle paramBundle, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setAutofillFailure(int paramInt1, List<AutofillId> paramList, int paramInt2) throws RemoteException;
  
  void setHasCallback(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void setUserData(UserData paramUserData) throws RemoteException;
  
  void startSession(IBinder paramIBinder1, IBinder paramIBinder2, AutofillId paramAutofillId, Rect paramRect, AutofillValue paramAutofillValue, int paramInt1, boolean paramBoolean1, int paramInt2, ComponentName paramComponentName, boolean paramBoolean2, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void updateSession(int paramInt1, AutofillId paramAutofillId, Rect paramRect, AutofillValue paramAutofillValue, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  class Default implements IAutoFillManager {
    public void addClient(IAutoFillManagerClient param1IAutoFillManagerClient, ComponentName param1ComponentName, int param1Int, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void removeClient(IAutoFillManagerClient param1IAutoFillManagerClient, int param1Int) throws RemoteException {}
    
    public void startSession(IBinder param1IBinder1, IBinder param1IBinder2, AutofillId param1AutofillId, Rect param1Rect, AutofillValue param1AutofillValue, int param1Int1, boolean param1Boolean1, int param1Int2, ComponentName param1ComponentName, boolean param1Boolean2, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void getFillEventHistory(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void restoreSession(int param1Int, IBinder param1IBinder1, IBinder param1IBinder2, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void updateSession(int param1Int1, AutofillId param1AutofillId, Rect param1Rect, AutofillValue param1AutofillValue, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void setAutofillFailure(int param1Int1, List<AutofillId> param1List, int param1Int2) throws RemoteException {}
    
    public void finishSession(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void cancelSession(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setAuthenticationResult(Bundle param1Bundle, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void setHasCallback(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void disableOwnedAutofillServices(int param1Int) throws RemoteException {}
    
    public void isServiceSupported(int param1Int, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void isServiceEnabled(int param1Int, String param1String, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void onPendingSaveUi(int param1Int, IBinder param1IBinder) throws RemoteException {}
    
    public void getUserData(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void getUserDataId(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void setUserData(UserData param1UserData) throws RemoteException {}
    
    public void isFieldClassificationEnabled(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void getAutofillServiceComponentName(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void getAvailableFieldClassificationAlgorithms(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void getDefaultFieldClassificationAlgorithm(IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void setAugmentedAutofillWhitelist(List<String> param1List, List<ComponentName> param1List1, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAutoFillManager {
    private static final String DESCRIPTOR = "android.view.autofill.IAutoFillManager";
    
    static final int TRANSACTION_addClient = 1;
    
    static final int TRANSACTION_cancelSession = 9;
    
    static final int TRANSACTION_disableOwnedAutofillServices = 12;
    
    static final int TRANSACTION_finishSession = 8;
    
    static final int TRANSACTION_getAutofillServiceComponentName = 20;
    
    static final int TRANSACTION_getAvailableFieldClassificationAlgorithms = 21;
    
    static final int TRANSACTION_getDefaultFieldClassificationAlgorithm = 22;
    
    static final int TRANSACTION_getFillEventHistory = 4;
    
    static final int TRANSACTION_getUserData = 16;
    
    static final int TRANSACTION_getUserDataId = 17;
    
    static final int TRANSACTION_isFieldClassificationEnabled = 19;
    
    static final int TRANSACTION_isServiceEnabled = 14;
    
    static final int TRANSACTION_isServiceSupported = 13;
    
    static final int TRANSACTION_onPendingSaveUi = 15;
    
    static final int TRANSACTION_removeClient = 2;
    
    static final int TRANSACTION_restoreSession = 5;
    
    static final int TRANSACTION_setAugmentedAutofillWhitelist = 23;
    
    static final int TRANSACTION_setAuthenticationResult = 10;
    
    static final int TRANSACTION_setAutofillFailure = 7;
    
    static final int TRANSACTION_setHasCallback = 11;
    
    static final int TRANSACTION_setUserData = 18;
    
    static final int TRANSACTION_startSession = 3;
    
    static final int TRANSACTION_updateSession = 6;
    
    public Stub() {
      attachInterface(this, "android.view.autofill.IAutoFillManager");
    }
    
    public static IAutoFillManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.autofill.IAutoFillManager");
      if (iInterface != null && iInterface instanceof IAutoFillManager)
        return (IAutoFillManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 23:
          return "setAugmentedAutofillWhitelist";
        case 22:
          return "getDefaultFieldClassificationAlgorithm";
        case 21:
          return "getAvailableFieldClassificationAlgorithms";
        case 20:
          return "getAutofillServiceComponentName";
        case 19:
          return "isFieldClassificationEnabled";
        case 18:
          return "setUserData";
        case 17:
          return "getUserDataId";
        case 16:
          return "getUserData";
        case 15:
          return "onPendingSaveUi";
        case 14:
          return "isServiceEnabled";
        case 13:
          return "isServiceSupported";
        case 12:
          return "disableOwnedAutofillServices";
        case 11:
          return "setHasCallback";
        case 10:
          return "setAuthenticationResult";
        case 9:
          return "cancelSession";
        case 8:
          return "finishSession";
        case 7:
          return "setAutofillFailure";
        case 6:
          return "updateSession";
        case 5:
          return "restoreSession";
        case 4:
          return "getFillEventHistory";
        case 3:
          return "startSession";
        case 2:
          return "removeClient";
        case 1:
          break;
      } 
      return "addClient";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IAutoFillManagerClient iAutoFillManagerClient;
      if (param1Int1 != 1598968902) {
        IResultReceiver iResultReceiver2;
        IBinder iBinder1;
        ArrayList<String> arrayList1;
        String str;
        ArrayList<AutofillId> arrayList;
        IBinder iBinder2;
        ArrayList<ComponentName> arrayList2;
        IBinder iBinder3;
        int i;
        AutofillValue autofillValue;
        int j;
        IBinder iBinder4, iBinder5;
        ComponentName componentName;
        boolean bool2, bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 23:
            param1Parcel1.enforceInterface("android.view.autofill.IAutoFillManager");
            arrayList1 = param1Parcel1.createStringArrayList();
            arrayList2 = param1Parcel1.createTypedArrayList(ComponentName.CREATOR);
            iResultReceiver2 = IResultReceiver.Stub.asInterface(param1Parcel1.readStrongBinder());
            setAugmentedAutofillWhitelist(arrayList1, arrayList2, iResultReceiver2);
            return true;
          case 22:
            iResultReceiver2.enforceInterface("android.view.autofill.IAutoFillManager");
            iResultReceiver2 = IResultReceiver.Stub.asInterface(iResultReceiver2.readStrongBinder());
            getDefaultFieldClassificationAlgorithm(iResultReceiver2);
            return true;
          case 21:
            iResultReceiver2.enforceInterface("android.view.autofill.IAutoFillManager");
            iResultReceiver2 = IResultReceiver.Stub.asInterface(iResultReceiver2.readStrongBinder());
            getAvailableFieldClassificationAlgorithms(iResultReceiver2);
            return true;
          case 20:
            iResultReceiver2.enforceInterface("android.view.autofill.IAutoFillManager");
            iResultReceiver2 = IResultReceiver.Stub.asInterface(iResultReceiver2.readStrongBinder());
            getAutofillServiceComponentName(iResultReceiver2);
            return true;
          case 19:
            iResultReceiver2.enforceInterface("android.view.autofill.IAutoFillManager");
            iResultReceiver2 = IResultReceiver.Stub.asInterface(iResultReceiver2.readStrongBinder());
            isFieldClassificationEnabled(iResultReceiver2);
            return true;
          case 18:
            iResultReceiver2.enforceInterface("android.view.autofill.IAutoFillManager");
            if (iResultReceiver2.readInt() != 0) {
              UserData userData = (UserData)UserData.CREATOR.createFromParcel((Parcel)iResultReceiver2);
            } else {
              iResultReceiver2 = null;
            } 
            setUserData((UserData)iResultReceiver2);
            return true;
          case 17:
            iResultReceiver2.enforceInterface("android.view.autofill.IAutoFillManager");
            iResultReceiver2 = IResultReceiver.Stub.asInterface(iResultReceiver2.readStrongBinder());
            getUserDataId(iResultReceiver2);
            return true;
          case 16:
            iResultReceiver2.enforceInterface("android.view.autofill.IAutoFillManager");
            iResultReceiver2 = IResultReceiver.Stub.asInterface(iResultReceiver2.readStrongBinder());
            getUserData(iResultReceiver2);
            return true;
          case 15:
            iResultReceiver2.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int1 = iResultReceiver2.readInt();
            iBinder1 = iResultReceiver2.readStrongBinder();
            onPendingSaveUi(param1Int1, iBinder1);
            return true;
          case 14:
            iBinder1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int1 = iBinder1.readInt();
            str = iBinder1.readString();
            iResultReceiver1 = IResultReceiver.Stub.asInterface(iBinder1.readStrongBinder());
            isServiceEnabled(param1Int1, str, iResultReceiver1);
            return true;
          case 13:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int1 = iResultReceiver1.readInt();
            iResultReceiver1 = IResultReceiver.Stub.asInterface(iResultReceiver1.readStrongBinder());
            isServiceSupported(param1Int1, iResultReceiver1);
            return true;
          case 12:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int1 = iResultReceiver1.readInt();
            disableOwnedAutofillServices(param1Int1);
            return true;
          case 11:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int2 = iResultReceiver1.readInt();
            param1Int1 = iResultReceiver1.readInt();
            if (iResultReceiver1.readInt() != 0)
              bool1 = true; 
            setHasCallback(param1Int2, param1Int1, bool1);
            return true;
          case 10:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            if (iResultReceiver1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iResultReceiver1);
            } else {
              str = null;
            } 
            param1Int1 = iResultReceiver1.readInt();
            param1Int2 = iResultReceiver1.readInt();
            i = iResultReceiver1.readInt();
            setAuthenticationResult((Bundle)str, param1Int1, param1Int2, i);
            return true;
          case 9:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int2 = iResultReceiver1.readInt();
            param1Int1 = iResultReceiver1.readInt();
            cancelSession(param1Int2, param1Int1);
            return true;
          case 8:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int2 = iResultReceiver1.readInt();
            param1Int1 = iResultReceiver1.readInt();
            finishSession(param1Int2, param1Int1);
            return true;
          case 7:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int2 = iResultReceiver1.readInt();
            arrayList = iResultReceiver1.createTypedArrayList(AutofillId.CREATOR);
            param1Int1 = iResultReceiver1.readInt();
            setAutofillFailure(param1Int2, arrayList, param1Int1);
            return true;
          case 6:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int2 = iResultReceiver1.readInt();
            if (iResultReceiver1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)iResultReceiver1);
            } else {
              arrayList = null;
            } 
            if (iResultReceiver1.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iResultReceiver1);
            } else {
              arrayList2 = null;
            } 
            if (iResultReceiver1.readInt() != 0) {
              autofillValue = (AutofillValue)AutofillValue.CREATOR.createFromParcel((Parcel)iResultReceiver1);
            } else {
              autofillValue = null;
            } 
            i = iResultReceiver1.readInt();
            j = iResultReceiver1.readInt();
            param1Int1 = iResultReceiver1.readInt();
            updateSession(param1Int2, (AutofillId)arrayList, (Rect)arrayList2, autofillValue, i, j, param1Int1);
            return true;
          case 5:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            param1Int1 = iResultReceiver1.readInt();
            iBinder2 = iResultReceiver1.readStrongBinder();
            iBinder3 = iResultReceiver1.readStrongBinder();
            iResultReceiver1 = IResultReceiver.Stub.asInterface(iResultReceiver1.readStrongBinder());
            restoreSession(param1Int1, iBinder2, iBinder3, iResultReceiver1);
            return true;
          case 4:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            iResultReceiver1 = IResultReceiver.Stub.asInterface(iResultReceiver1.readStrongBinder());
            getFillEventHistory(iResultReceiver1);
            return true;
          case 3:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            iBinder4 = iResultReceiver1.readStrongBinder();
            iBinder5 = iResultReceiver1.readStrongBinder();
            if (iResultReceiver1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel((Parcel)iResultReceiver1);
            } else {
              iBinder2 = null;
            } 
            if (iResultReceiver1.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iResultReceiver1);
            } else {
              iBinder3 = null;
            } 
            if (iResultReceiver1.readInt() != 0) {
              autofillValue = (AutofillValue)AutofillValue.CREATOR.createFromParcel((Parcel)iResultReceiver1);
            } else {
              autofillValue = null;
            } 
            param1Int2 = iResultReceiver1.readInt();
            if (iResultReceiver1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            param1Int1 = iResultReceiver1.readInt();
            if (iResultReceiver1.readInt() != 0) {
              componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iResultReceiver1);
            } else {
              componentName = null;
            } 
            if (iResultReceiver1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            iResultReceiver1 = IResultReceiver.Stub.asInterface(iResultReceiver1.readStrongBinder());
            startSession(iBinder4, iBinder5, (AutofillId)iBinder2, (Rect)iBinder3, autofillValue, param1Int2, bool1, param1Int1, componentName, bool2, iResultReceiver1);
            return true;
          case 2:
            iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
            iAutoFillManagerClient = IAutoFillManagerClient.Stub.asInterface(iResultReceiver1.readStrongBinder());
            param1Int1 = iResultReceiver1.readInt();
            removeClient(iAutoFillManagerClient, param1Int1);
            return true;
          case 1:
            break;
        } 
        iResultReceiver1.enforceInterface("android.view.autofill.IAutoFillManager");
        IAutoFillManagerClient iAutoFillManagerClient1 = IAutoFillManagerClient.Stub.asInterface(iResultReceiver1.readStrongBinder());
        if (iResultReceiver1.readInt() != 0) {
          ComponentName componentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iResultReceiver1);
        } else {
          iAutoFillManagerClient = null;
        } 
        param1Int1 = iResultReceiver1.readInt();
        IResultReceiver iResultReceiver1 = IResultReceiver.Stub.asInterface(iResultReceiver1.readStrongBinder());
        addClient(iAutoFillManagerClient1, (ComponentName)iAutoFillManagerClient, param1Int1, iResultReceiver1);
        return true;
      } 
      iAutoFillManagerClient.writeString("android.view.autofill.IAutoFillManager");
      return true;
    }
    
    private static class Proxy implements IAutoFillManager {
      public static IAutoFillManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.autofill.IAutoFillManager";
      }
      
      public void addClient(IAutoFillManagerClient param2IAutoFillManagerClient, ComponentName param2ComponentName, int param2Int, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IAutoFillManagerClient != null) {
            iBinder = param2IAutoFillManagerClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().addClient(param2IAutoFillManagerClient, param2ComponentName, param2Int, param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeClient(IAutoFillManagerClient param2IAutoFillManagerClient, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IAutoFillManagerClient != null) {
            iBinder = param2IAutoFillManagerClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().removeClient(param2IAutoFillManagerClient, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startSession(IBinder param2IBinder1, IBinder param2IBinder2, AutofillId param2AutofillId, Rect param2Rect, AutofillValue param2AutofillValue, int param2Int1, boolean param2Boolean1, int param2Int2, ComponentName param2ComponentName, boolean param2Boolean2, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeStrongBinder(param2IBinder1);
          parcel.writeStrongBinder(param2IBinder2);
          boolean bool1 = false;
          if (param2AutofillId != null) {
            try {
              parcel.writeInt(1);
              param2AutofillId.writeToParcel(parcel, 0);
            } finally {}
          } else {
            parcel.writeInt(0);
          } 
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2AutofillValue != null) {
            parcel.writeInt(1);
            param2AutofillValue.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          parcel.writeInt(param2Int2);
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager iAutoFillManager = IAutoFillManager.Stub.getDefaultImpl();
            try {
              iAutoFillManager.startSession(param2IBinder1, param2IBinder2, param2AutofillId, param2Rect, param2AutofillValue, param2Int1, param2Boolean1, param2Int2, param2ComponentName, param2Boolean2, param2IResultReceiver);
              parcel.recycle();
              return;
            } finally {}
          } else {
            parcel.recycle();
            return;
          } 
        } finally {}
        parcel.recycle();
        throw param2IBinder1;
      }
      
      public void getFillEventHistory(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().getFillEventHistory(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void restoreSession(int param2Int, IBinder param2IBinder1, IBinder param2IBinder2, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int);
          parcel.writeStrongBinder(param2IBinder1);
          parcel.writeStrongBinder(param2IBinder2);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().restoreSession(param2Int, param2IBinder1, param2IBinder2, param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateSession(int param2Int1, AutofillId param2AutofillId, Rect param2Rect, AutofillValue param2AutofillValue, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          try {
            parcel.writeInt(param2Int1);
            if (param2AutofillId != null) {
              parcel.writeInt(1);
              param2AutofillId.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2Rect != null) {
              parcel.writeInt(1);
              param2Rect.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2AutofillValue != null) {
              parcel.writeInt(1);
              param2AutofillValue.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            try {
              parcel.writeInt(param2Int2);
              try {
                parcel.writeInt(param2Int3);
                parcel.writeInt(param2Int4);
                boolean bool = this.mRemote.transact(6, parcel, null, 1);
                if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
                  IAutoFillManager.Stub.getDefaultImpl().updateSession(param2Int1, param2AutofillId, param2Rect, param2AutofillValue, param2Int2, param2Int3, param2Int4);
                  parcel.recycle();
                  return;
                } 
                parcel.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2AutofillId;
      }
      
      public void setAutofillFailure(int param2Int1, List<AutofillId> param2List, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int1);
          parcel.writeTypedList(param2List);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().setAutofillFailure(param2Int1, param2List, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void finishSession(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().finishSession(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void cancelSession(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().cancelSession(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setAuthenticationResult(Bundle param2Bundle, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().setAuthenticationResult(param2Bundle, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setHasCallback(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel, null, 1);
          if (!bool1 && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().setHasCallback(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disableOwnedAutofillServices(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().disableOwnedAutofillServices(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void isServiceSupported(int param2Int, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().isServiceSupported(param2Int, param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void isServiceEnabled(int param2Int, String param2String, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().isServiceEnabled(param2Int, param2String, param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPendingSaveUi(int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeInt(param2Int);
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().onPendingSaveUi(param2Int, param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getUserData(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().getUserData(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getUserDataId(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().getUserDataId(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setUserData(UserData param2UserData) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2UserData != null) {
            parcel.writeInt(1);
            param2UserData.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().setUserData(param2UserData);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void isFieldClassificationEnabled(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().isFieldClassificationEnabled(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getAutofillServiceComponentName(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().getAutofillServiceComponentName(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getAvailableFieldClassificationAlgorithms(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().getAvailableFieldClassificationAlgorithms(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getDefaultFieldClassificationAlgorithm(IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().getDefaultFieldClassificationAlgorithm(param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setAugmentedAutofillWhitelist(List<String> param2List, List<ComponentName> param2List1, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.autofill.IAutoFillManager");
          parcel.writeStringList(param2List);
          parcel.writeTypedList(param2List1);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IAutoFillManager.Stub.getDefaultImpl() != null) {
            IAutoFillManager.Stub.getDefaultImpl().setAugmentedAutofillWhitelist(param2List, param2List1, param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAutoFillManager param1IAutoFillManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAutoFillManager != null) {
          Proxy.sDefaultImpl = param1IAutoFillManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAutoFillManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
