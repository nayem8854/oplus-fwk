package android.view;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class InputChannel implements Parcelable {
  public static final Parcelable.Creator<InputChannel> CREATOR = new Parcelable.Creator<InputChannel>() {
      public InputChannel createFromParcel(Parcel param1Parcel) {
        InputChannel inputChannel = new InputChannel();
        inputChannel.readFromParcel(param1Parcel);
        return inputChannel;
      }
      
      public InputChannel[] newArray(int param1Int) {
        return new InputChannel[param1Int];
      }
    };
  
  private static final boolean DEBUG = false;
  
  private static final String TAG = "InputChannel";
  
  private long mPtr;
  
  protected void finalize() throws Throwable {
    try {
      nativeDispose(true);
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public static InputChannel[] openInputChannelPair(String paramString) {
    if (paramString != null)
      return nativeOpenInputChannelPair(paramString); 
    throw new IllegalArgumentException("name must not be null");
  }
  
  public String getName() {
    String str = nativeGetName();
    if (str == null)
      str = "uninitialized"; 
    return str;
  }
  
  public void dispose() {
    nativeDispose(false);
  }
  
  public void release() {
    nativeRelease();
  }
  
  public void transferTo(InputChannel paramInputChannel) {
    if (paramInputChannel != null) {
      nativeTransferTo(paramInputChannel);
      return;
    } 
    throw new IllegalArgumentException("outParameter must not be null");
  }
  
  public InputChannel dup() {
    InputChannel inputChannel = new InputChannel();
    nativeDup(inputChannel);
    return inputChannel;
  }
  
  public int describeContents() {
    return 1;
  }
  
  public void readFromParcel(Parcel paramParcel) {
    if (paramParcel != null) {
      nativeReadFromParcel(paramParcel);
      return;
    } 
    throw new IllegalArgumentException("in must not be null");
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (paramParcel != null) {
      nativeWriteToParcel(paramParcel);
      if ((paramInt & 0x1) != 0)
        dispose(); 
      return;
    } 
    throw new IllegalArgumentException("out must not be null");
  }
  
  public String toString() {
    return getName();
  }
  
  public IBinder getToken() {
    return nativeGetToken();
  }
  
  private native void nativeDispose(boolean paramBoolean);
  
  private native void nativeDup(InputChannel paramInputChannel);
  
  private native String nativeGetName();
  
  private native IBinder nativeGetToken();
  
  private static native InputChannel[] nativeOpenInputChannelPair(String paramString);
  
  private native void nativeReadFromParcel(Parcel paramParcel);
  
  private native void nativeRelease();
  
  private native void nativeTransferTo(InputChannel paramInputChannel);
  
  private native void nativeWriteToParcel(Parcel paramParcel);
}
