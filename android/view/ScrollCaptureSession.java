package android.view;

import android.graphics.Point;
import android.graphics.Rect;

public class ScrollCaptureSession {
  private ScrollCaptureClient mClient;
  
  private final Point mPositionInWindow;
  
  private final Rect mScrollBounds;
  
  private final Surface mSurface;
  
  public ScrollCaptureSession(Surface paramSurface, Rect paramRect, Point paramPoint, ScrollCaptureClient paramScrollCaptureClient) {
    this.mSurface = paramSurface;
    this.mScrollBounds = paramRect;
    this.mPositionInWindow = paramPoint;
    this.mClient = paramScrollCaptureClient;
  }
  
  public Surface getSurface() {
    return this.mSurface;
  }
  
  public Rect getScrollBounds() {
    return this.mScrollBounds;
  }
  
  public Point getPositionInWindow() {
    return this.mPositionInWindow;
  }
  
  public void notifyBufferSent(long paramLong, Rect paramRect) {
    ScrollCaptureClient scrollCaptureClient = this.mClient;
    if (scrollCaptureClient != null)
      scrollCaptureClient.onRequestImageCompleted(paramLong, paramRect); 
  }
  
  public void disconnect() {
    this.mClient = null;
    if (this.mSurface.isValid())
      this.mSurface.release(); 
  }
}
