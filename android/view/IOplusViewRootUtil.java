package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import java.lang.ref.WeakReference;

public interface IOplusViewRootUtil extends IOplusCommonFeature {
  public static final IOplusViewRootUtil DEFAULT = (IOplusViewRootUtil)new Object();
  
  public static final String NAME = "OplusViewRootUtil";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusViewRootUtil;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default void initSwipState(Display paramDisplay, Context paramContext) {}
  
  default void initSwipState(Display paramDisplay, Context paramContext, boolean paramBoolean) {}
  
  default boolean needScale(int paramInt1, int paramInt2, Display paramDisplay) {
    return false;
  }
  
  default boolean swipeFromBottom(MotionEvent paramMotionEvent, int paramInt1, int paramInt2, Display paramDisplay) {
    return false;
  }
  
  default float getCompactScale() {
    return 1.0F;
  }
  
  default int getScreenHeight() {
    return 1;
  }
  
  default int getScreenWidth() {
    return 1;
  }
  
  default void checkGestureConfig(Context paramContext) {}
  
  default DisplayInfo getDisplayInfo() {
    return null;
  }
  
  default IOplusLongshotViewHelper getOplusLongshotViewHelper(WeakReference<ViewRootImpl> paramWeakReference) {
    return null;
  }
}
