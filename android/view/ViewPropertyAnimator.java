package android.view;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.graphics.RenderNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class ViewPropertyAnimator {
  private boolean mDurationSet = false;
  
  private long mStartDelay = 0L;
  
  private boolean mStartDelaySet = false;
  
  private boolean mInterpolatorSet = false;
  
  private Animator.AnimatorListener mListener = null;
  
  private ValueAnimator.AnimatorUpdateListener mUpdateListener = null;
  
  private AnimatorEventListener mAnimatorEventListener = new AnimatorEventListener();
  
  ArrayList<NameValuesHolder> mPendingAnimations = new ArrayList<>();
  
  private Runnable mAnimationStarter = new Runnable() {
      final ViewPropertyAnimator this$0;
      
      public void run() {
        ViewPropertyAnimator.this.startAnimation();
      }
    };
  
  private static class PropertyBundle {
    ArrayList<ViewPropertyAnimator.NameValuesHolder> mNameValuesHolder;
    
    int mPropertyMask;
    
    PropertyBundle(int param1Int, ArrayList<ViewPropertyAnimator.NameValuesHolder> param1ArrayList) {
      this.mPropertyMask = param1Int;
      this.mNameValuesHolder = param1ArrayList;
    }
    
    boolean cancel(int param1Int) {
      if ((this.mPropertyMask & param1Int) != 0) {
        ArrayList<ViewPropertyAnimator.NameValuesHolder> arrayList = this.mNameValuesHolder;
        if (arrayList != null) {
          int i = arrayList.size();
          for (byte b = 0; b < i; b++) {
            ViewPropertyAnimator.NameValuesHolder nameValuesHolder = this.mNameValuesHolder.get(b);
            if (nameValuesHolder.mNameConstant == param1Int) {
              this.mNameValuesHolder.remove(b);
              this.mPropertyMask &= param1Int ^ 0xFFFFFFFF;
              return true;
            } 
          } 
        } 
      } 
      return false;
    }
  }
  
  private HashMap<Animator, PropertyBundle> mAnimatorMap = new HashMap<>();
  
  static final int ALPHA = 2048;
  
  static final int NONE = 0;
  
  static final int ROTATION = 32;
  
  static final int ROTATION_X = 64;
  
  static final int ROTATION_Y = 128;
  
  static final int SCALE_X = 8;
  
  static final int SCALE_Y = 16;
  
  private static final int TRANSFORM_MASK = 2047;
  
  static final int TRANSLATION_X = 1;
  
  static final int TRANSLATION_Y = 2;
  
  static final int TRANSLATION_Z = 4;
  
  static final int X = 256;
  
  static final int Y = 512;
  
  static final int Z = 1024;
  
  private HashMap<Animator, Runnable> mAnimatorCleanupMap;
  
  private HashMap<Animator, Runnable> mAnimatorOnEndMap;
  
  private HashMap<Animator, Runnable> mAnimatorOnStartMap;
  
  private HashMap<Animator, Runnable> mAnimatorSetupMap;
  
  private long mDuration;
  
  private TimeInterpolator mInterpolator;
  
  private Runnable mPendingCleanupAction;
  
  private Runnable mPendingOnEndAction;
  
  private Runnable mPendingOnStartAction;
  
  private Runnable mPendingSetupAction;
  
  private ValueAnimator mTempValueAnimator;
  
  final View mView;
  
  static class NameValuesHolder {
    float mDeltaValue;
    
    float mFromValue;
    
    int mNameConstant;
    
    NameValuesHolder(int param1Int, float param1Float1, float param1Float2) {
      this.mNameConstant = param1Int;
      this.mFromValue = param1Float1;
      this.mDeltaValue = param1Float2;
    }
  }
  
  ViewPropertyAnimator(View paramView) {
    this.mView = paramView;
    paramView.ensureTransformationInfo();
  }
  
  public ViewPropertyAnimator setDuration(long paramLong) {
    if (paramLong >= 0L) {
      this.mDurationSet = true;
      this.mDuration = paramLong;
      return this;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Animators cannot have negative duration: ");
    stringBuilder.append(paramLong);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public long getDuration() {
    if (this.mDurationSet)
      return this.mDuration; 
    if (this.mTempValueAnimator == null)
      this.mTempValueAnimator = new ValueAnimator(); 
    return this.mTempValueAnimator.getDuration();
  }
  
  public long getStartDelay() {
    if (this.mStartDelaySet)
      return this.mStartDelay; 
    return 0L;
  }
  
  public ViewPropertyAnimator setStartDelay(long paramLong) {
    if (paramLong >= 0L) {
      this.mStartDelaySet = true;
      this.mStartDelay = paramLong;
      return this;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Animators cannot have negative start delay: ");
    stringBuilder.append(paramLong);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public ViewPropertyAnimator setInterpolator(TimeInterpolator paramTimeInterpolator) {
    this.mInterpolatorSet = true;
    this.mInterpolator = paramTimeInterpolator;
    return this;
  }
  
  public TimeInterpolator getInterpolator() {
    if (this.mInterpolatorSet)
      return this.mInterpolator; 
    if (this.mTempValueAnimator == null)
      this.mTempValueAnimator = new ValueAnimator(); 
    return this.mTempValueAnimator.getInterpolator();
  }
  
  public ViewPropertyAnimator setListener(Animator.AnimatorListener paramAnimatorListener) {
    this.mListener = paramAnimatorListener;
    return this;
  }
  
  Animator.AnimatorListener getListener() {
    return this.mListener;
  }
  
  public ViewPropertyAnimator setUpdateListener(ValueAnimator.AnimatorUpdateListener paramAnimatorUpdateListener) {
    this.mUpdateListener = paramAnimatorUpdateListener;
    return this;
  }
  
  ValueAnimator.AnimatorUpdateListener getUpdateListener() {
    return this.mUpdateListener;
  }
  
  public void start() {
    this.mView.removeCallbacks(this.mAnimationStarter);
    startAnimation();
  }
  
  public void cancel() {
    if (this.mAnimatorMap.size() > 0) {
      HashMap<Animator, PropertyBundle> hashMap = this.mAnimatorMap;
      hashMap = (HashMap<Animator, PropertyBundle>)hashMap.clone();
      Set<Animator> set = hashMap.keySet();
      for (Animator animator : set)
        animator.cancel(); 
    } 
    this.mPendingAnimations.clear();
    this.mPendingSetupAction = null;
    this.mPendingCleanupAction = null;
    this.mPendingOnStartAction = null;
    this.mPendingOnEndAction = null;
    this.mView.removeCallbacks(this.mAnimationStarter);
  }
  
  public ViewPropertyAnimator x(float paramFloat) {
    animateProperty(256, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator xBy(float paramFloat) {
    animatePropertyBy(256, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator y(float paramFloat) {
    animateProperty(512, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator yBy(float paramFloat) {
    animatePropertyBy(512, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator z(float paramFloat) {
    animateProperty(1024, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator zBy(float paramFloat) {
    animatePropertyBy(1024, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator rotation(float paramFloat) {
    animateProperty(32, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator rotationBy(float paramFloat) {
    animatePropertyBy(32, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator rotationX(float paramFloat) {
    animateProperty(64, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator rotationXBy(float paramFloat) {
    animatePropertyBy(64, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator rotationY(float paramFloat) {
    animateProperty(128, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator rotationYBy(float paramFloat) {
    animatePropertyBy(128, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator translationX(float paramFloat) {
    animateProperty(1, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator translationXBy(float paramFloat) {
    animatePropertyBy(1, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator translationY(float paramFloat) {
    animateProperty(2, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator translationYBy(float paramFloat) {
    animatePropertyBy(2, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator translationZ(float paramFloat) {
    animateProperty(4, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator translationZBy(float paramFloat) {
    animatePropertyBy(4, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator scaleX(float paramFloat) {
    animateProperty(8, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator scaleXBy(float paramFloat) {
    animatePropertyBy(8, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator scaleY(float paramFloat) {
    animateProperty(16, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator scaleYBy(float paramFloat) {
    animatePropertyBy(16, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator alpha(float paramFloat) {
    animateProperty(2048, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator alphaBy(float paramFloat) {
    animatePropertyBy(2048, paramFloat);
    return this;
  }
  
  public ViewPropertyAnimator withLayer() {
    this.mPendingSetupAction = new Runnable() {
        final ViewPropertyAnimator this$0;
        
        public void run() {
          ViewPropertyAnimator.this.mView.setLayerType(2, null);
          if (ViewPropertyAnimator.this.mView.isAttachedToWindow())
            ViewPropertyAnimator.this.mView.buildLayer(); 
        }
      };
    final int currentLayerType = this.mView.getLayerType();
    this.mPendingCleanupAction = new Runnable() {
        final ViewPropertyAnimator this$0;
        
        final int val$currentLayerType;
        
        public void run() {
          ViewPropertyAnimator.this.mView.setLayerType(currentLayerType, null);
        }
      };
    if (this.mAnimatorSetupMap == null)
      this.mAnimatorSetupMap = new HashMap<>(); 
    if (this.mAnimatorCleanupMap == null)
      this.mAnimatorCleanupMap = new HashMap<>(); 
    return this;
  }
  
  public ViewPropertyAnimator withStartAction(Runnable paramRunnable) {
    this.mPendingOnStartAction = paramRunnable;
    if (paramRunnable != null && this.mAnimatorOnStartMap == null)
      this.mAnimatorOnStartMap = new HashMap<>(); 
    return this;
  }
  
  public ViewPropertyAnimator withEndAction(Runnable paramRunnable) {
    this.mPendingOnEndAction = paramRunnable;
    if (paramRunnable != null && this.mAnimatorOnEndMap == null)
      this.mAnimatorOnEndMap = new HashMap<>(); 
    return this;
  }
  
  boolean hasActions() {
    return (this.mPendingSetupAction != null || this.mPendingCleanupAction != null || this.mPendingOnStartAction != null || this.mPendingOnEndAction != null);
  }
  
  private void startAnimation() {
    this.mView.setHasTransientState(true);
    ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { 1.0F });
    ArrayList<NameValuesHolder> arrayList1 = this.mPendingAnimations;
    ArrayList<NameValuesHolder> arrayList2 = (ArrayList)arrayList1.clone();
    this.mPendingAnimations.clear();
    int i = 0;
    int j = arrayList2.size();
    for (byte b = 0; b < j; b++) {
      NameValuesHolder nameValuesHolder = arrayList2.get(b);
      i |= nameValuesHolder.mNameConstant;
    } 
    this.mAnimatorMap.put(valueAnimator, new PropertyBundle(i, arrayList2));
    Runnable runnable = this.mPendingSetupAction;
    if (runnable != null) {
      this.mAnimatorSetupMap.put(valueAnimator, runnable);
      this.mPendingSetupAction = null;
    } 
    runnable = this.mPendingCleanupAction;
    if (runnable != null) {
      this.mAnimatorCleanupMap.put(valueAnimator, runnable);
      this.mPendingCleanupAction = null;
    } 
    runnable = this.mPendingOnStartAction;
    if (runnable != null) {
      this.mAnimatorOnStartMap.put(valueAnimator, runnable);
      this.mPendingOnStartAction = null;
    } 
    runnable = this.mPendingOnEndAction;
    if (runnable != null) {
      this.mAnimatorOnEndMap.put(valueAnimator, runnable);
      this.mPendingOnEndAction = null;
    } 
    valueAnimator.addUpdateListener(this.mAnimatorEventListener);
    valueAnimator.addListener(this.mAnimatorEventListener);
    if (this.mStartDelaySet)
      valueAnimator.setStartDelay(this.mStartDelay); 
    if (this.mDurationSet)
      valueAnimator.setDuration(this.mDuration); 
    if (this.mInterpolatorSet)
      valueAnimator.setInterpolator(this.mInterpolator); 
    valueAnimator.start();
  }
  
  private void animateProperty(int paramInt, float paramFloat) {
    float f = getValue(paramInt);
    animatePropertyBy(paramInt, f, paramFloat - f);
  }
  
  private void animatePropertyBy(int paramInt, float paramFloat) {
    float f = getValue(paramInt);
    animatePropertyBy(paramInt, f, paramFloat);
  }
  
  private void animatePropertyBy(int paramInt, float paramFloat1, float paramFloat2) {
    if (this.mAnimatorMap.size() > 0) {
      Animator animator;
      Set set = null;
      Set<Animator> set1 = this.mAnimatorMap.keySet();
      Iterator<Animator> iterator = set1.iterator();
      while (true) {
        set1 = set;
        if (iterator.hasNext()) {
          animator = iterator.next();
          PropertyBundle propertyBundle = this.mAnimatorMap.get(animator);
          if (propertyBundle.cancel(paramInt))
            if (propertyBundle.mPropertyMask == 0)
              break;  
          continue;
        } 
        break;
      } 
      if (animator != null)
        animator.cancel(); 
    } 
    NameValuesHolder nameValuesHolder = new NameValuesHolder(paramInt, paramFloat1, paramFloat2);
    this.mPendingAnimations.add(nameValuesHolder);
    this.mView.removeCallbacks(this.mAnimationStarter);
    this.mView.postOnAnimation(this.mAnimationStarter);
  }
  
  private void setValue(int paramInt, float paramFloat) {
    RenderNode renderNode = this.mView.mRenderNode;
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt != 64) {
                  if (paramInt != 128) {
                    if (paramInt != 256) {
                      if (paramInt != 512) {
                        if (paramInt != 1024) {
                          if (paramInt == 2048) {
                            this.mView.setAlphaInternal(paramFloat);
                            renderNode.setAlpha(paramFloat);
                          } 
                        } else {
                          renderNode.setTranslationZ(paramFloat - renderNode.getElevation());
                        } 
                      } else {
                        renderNode.setTranslationY(paramFloat - this.mView.mTop);
                      } 
                    } else {
                      renderNode.setTranslationX(paramFloat - this.mView.mLeft);
                    } 
                  } else {
                    renderNode.setRotationY(paramFloat);
                  } 
                } else {
                  renderNode.setRotationX(paramFloat);
                } 
              } else {
                renderNode.setRotationZ(paramFloat);
              } 
            } else {
              renderNode.setScaleY(paramFloat);
            } 
          } else {
            renderNode.setScaleX(paramFloat);
          } 
        } else {
          renderNode.setTranslationZ(paramFloat);
        } 
      } else {
        renderNode.setTranslationY(paramFloat);
      } 
    } else {
      renderNode.setTranslationX(paramFloat);
    } 
  }
  
  private float getValue(int paramInt) {
    RenderNode renderNode = this.mView.mRenderNode;
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt != 64) {
                  if (paramInt != 128) {
                    if (paramInt != 256) {
                      if (paramInt != 512) {
                        if (paramInt != 1024) {
                          if (paramInt != 2048)
                            return 0.0F; 
                          return this.mView.getAlpha();
                        } 
                        return renderNode.getElevation() + renderNode.getTranslationZ();
                      } 
                      return this.mView.mTop + renderNode.getTranslationY();
                    } 
                    return this.mView.mLeft + renderNode.getTranslationX();
                  } 
                  return renderNode.getRotationY();
                } 
                return renderNode.getRotationX();
              } 
              return renderNode.getRotationZ();
            } 
            return renderNode.getScaleY();
          } 
          return renderNode.getScaleX();
        } 
        return renderNode.getTranslationZ();
      } 
      return renderNode.getTranslationY();
    } 
    return renderNode.getTranslationX();
  }
  
  class AnimatorEventListener implements Animator.AnimatorListener, ValueAnimator.AnimatorUpdateListener {
    final ViewPropertyAnimator this$0;
    
    private AnimatorEventListener() {}
    
    public void onAnimationStart(Animator param1Animator) {
      if (ViewPropertyAnimator.this.mAnimatorSetupMap != null) {
        Runnable runnable = (Runnable)ViewPropertyAnimator.this.mAnimatorSetupMap.get(param1Animator);
        if (runnable != null)
          runnable.run(); 
        ViewPropertyAnimator.this.mAnimatorSetupMap.remove(param1Animator);
      } 
      if (ViewPropertyAnimator.this.mAnimatorOnStartMap != null) {
        Runnable runnable = (Runnable)ViewPropertyAnimator.this.mAnimatorOnStartMap.get(param1Animator);
        if (runnable != null)
          runnable.run(); 
        ViewPropertyAnimator.this.mAnimatorOnStartMap.remove(param1Animator);
      } 
      if (ViewPropertyAnimator.this.mListener != null)
        ViewPropertyAnimator.this.mListener.onAnimationStart(param1Animator); 
    }
    
    public void onAnimationCancel(Animator param1Animator) {
      if (ViewPropertyAnimator.this.mListener != null)
        ViewPropertyAnimator.this.mListener.onAnimationCancel(param1Animator); 
      if (ViewPropertyAnimator.this.mAnimatorOnEndMap != null)
        ViewPropertyAnimator.this.mAnimatorOnEndMap.remove(param1Animator); 
    }
    
    public void onAnimationRepeat(Animator param1Animator) {
      if (ViewPropertyAnimator.this.mListener != null)
        ViewPropertyAnimator.this.mListener.onAnimationRepeat(param1Animator); 
    }
    
    public void onAnimationEnd(Animator param1Animator) {
      ViewPropertyAnimator.this.mView.setHasTransientState(false);
      if (ViewPropertyAnimator.this.mAnimatorCleanupMap != null) {
        Runnable runnable = (Runnable)ViewPropertyAnimator.this.mAnimatorCleanupMap.get(param1Animator);
        if (runnable != null)
          runnable.run(); 
        ViewPropertyAnimator.this.mAnimatorCleanupMap.remove(param1Animator);
      } 
      if (ViewPropertyAnimator.this.mListener != null)
        ViewPropertyAnimator.this.mListener.onAnimationEnd(param1Animator); 
      if (ViewPropertyAnimator.this.mAnimatorOnEndMap != null) {
        Runnable runnable = (Runnable)ViewPropertyAnimator.this.mAnimatorOnEndMap.get(param1Animator);
        if (runnable != null)
          runnable.run(); 
        ViewPropertyAnimator.this.mAnimatorOnEndMap.remove(param1Animator);
      } 
      ViewPropertyAnimator.this.mAnimatorMap.remove(param1Animator);
    }
    
    public void onAnimationUpdate(ValueAnimator param1ValueAnimator) {
      ViewPropertyAnimator.PropertyBundle propertyBundle = (ViewPropertyAnimator.PropertyBundle)ViewPropertyAnimator.this.mAnimatorMap.get(param1ValueAnimator);
      if (propertyBundle == null)
        return; 
      boolean bool1 = ViewPropertyAnimator.this.mView.isHardwareAccelerated();
      boolean bool2 = false, bool3 = false;
      if (!bool1)
        ViewPropertyAnimator.this.mView.invalidateParentCaches(); 
      float f = param1ValueAnimator.getAnimatedFraction();
      int i = propertyBundle.mPropertyMask;
      if ((i & 0x7FF) != 0)
        ViewPropertyAnimator.this.mView.invalidateViewProperty(bool1, false); 
      ArrayList<ViewPropertyAnimator.NameValuesHolder> arrayList = propertyBundle.mNameValuesHolder;
      if (arrayList != null) {
        int j = arrayList.size();
        byte b = 0;
        while (true) {
          bool2 = bool3;
          if (b < j) {
            ViewPropertyAnimator.NameValuesHolder nameValuesHolder = arrayList.get(b);
            float f1 = nameValuesHolder.mFromValue + nameValuesHolder.mDeltaValue * f;
            if (nameValuesHolder.mNameConstant == 2048) {
              bool3 = ViewPropertyAnimator.this.mView.setAlphaNoInvalidation(f1);
            } else {
              ViewPropertyAnimator.this.setValue(nameValuesHolder.mNameConstant, f1);
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
      if ((i & 0x7FF) != 0 && 
        !bool1) {
        View view = ViewPropertyAnimator.this.mView;
        view.mPrivateFlags |= 0x20;
      } 
      if (bool2) {
        ViewPropertyAnimator.this.mView.invalidate(true);
      } else {
        ViewPropertyAnimator.this.mView.invalidateViewProperty(false, false);
      } 
      if (ViewPropertyAnimator.this.mUpdateListener != null)
        ViewPropertyAnimator.this.mUpdateListener.onAnimationUpdate(param1ValueAnimator); 
    }
  }
}
