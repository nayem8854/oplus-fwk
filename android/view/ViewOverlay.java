package android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;

public class ViewOverlay {
  OverlayViewGroup mOverlayViewGroup;
  
  ViewOverlay(Context paramContext, View paramView) {
    this.mOverlayViewGroup = new OverlayViewGroup(paramContext, paramView);
  }
  
  ViewGroup getOverlayView() {
    return this.mOverlayViewGroup;
  }
  
  public void add(Drawable paramDrawable) {
    this.mOverlayViewGroup.add(paramDrawable);
  }
  
  public void remove(Drawable paramDrawable) {
    this.mOverlayViewGroup.remove(paramDrawable);
  }
  
  public void clear() {
    this.mOverlayViewGroup.clear();
  }
  
  boolean isEmpty() {
    return this.mOverlayViewGroup.isEmpty();
  }
  
  class OverlayViewGroup extends ViewGroup {
    ArrayList<Drawable> mDrawables = null;
    
    final View mHostView;
    
    OverlayViewGroup(ViewOverlay this$0, View param1View) {
      super((Context)this$0);
      this.mHostView = param1View;
      this.mAttachInfo = param1View.mAttachInfo;
      this.mRight = param1View.getWidth();
      this.mBottom = param1View.getHeight();
      this.mRenderNode.setLeftTopRightBottom(0, 0, this.mRight, this.mBottom);
    }
    
    public void add(Drawable param1Drawable) {
      if (param1Drawable != null) {
        if (this.mDrawables == null)
          this.mDrawables = new ArrayList<>(); 
        if (!this.mDrawables.contains(param1Drawable)) {
          this.mDrawables.add(param1Drawable);
          invalidate(param1Drawable.getBounds());
          param1Drawable.setCallback(this);
        } 
        return;
      } 
      throw new IllegalArgumentException("drawable must be non-null");
    }
    
    public void remove(Drawable param1Drawable) {
      if (param1Drawable != null) {
        ArrayList<Drawable> arrayList = this.mDrawables;
        if (arrayList != null) {
          arrayList.remove(param1Drawable);
          invalidate(param1Drawable.getBounds());
          param1Drawable.setCallback(null);
        } 
        return;
      } 
      throw new IllegalArgumentException("drawable must be non-null");
    }
    
    protected boolean verifyDrawable(Drawable param1Drawable) {
      if (!super.verifyDrawable(param1Drawable)) {
        ArrayList<Drawable> arrayList = this.mDrawables;
        return (arrayList != null && arrayList.contains(param1Drawable));
      } 
      return true;
    }
    
    public void add(View param1View) {
      if (param1View != null) {
        if (param1View.getParent() instanceof ViewGroup) {
          ViewGroup viewGroup = (ViewGroup)param1View.getParent();
          if (viewGroup != this.mHostView && viewGroup.getParent() != null && viewGroup.mAttachInfo != null) {
            int[] arrayOfInt1 = new int[2];
            int[] arrayOfInt2 = new int[2];
            viewGroup.getLocationOnScreen(arrayOfInt1);
            this.mHostView.getLocationOnScreen(arrayOfInt2);
            param1View.offsetLeftAndRight(arrayOfInt1[0] - arrayOfInt2[0]);
            param1View.offsetTopAndBottom(arrayOfInt1[1] - arrayOfInt2[1]);
          } 
          viewGroup.removeView(param1View);
          if (viewGroup.getLayoutTransition() != null)
            viewGroup.getLayoutTransition().cancel(3); 
          if (param1View.getParent() != null)
            param1View.mParent = null; 
        } 
        addView(param1View);
        return;
      } 
      throw new IllegalArgumentException("view must be non-null");
    }
    
    public void remove(View param1View) {
      if (param1View != null) {
        removeView(param1View);
        return;
      } 
      throw new IllegalArgumentException("view must be non-null");
    }
    
    public void clear() {
      removeAllViews();
      ArrayList<Drawable> arrayList = this.mDrawables;
      if (arrayList != null) {
        for (Drawable drawable : arrayList)
          drawable.setCallback(null); 
        this.mDrawables.clear();
      } 
    }
    
    boolean isEmpty() {
      if (getChildCount() == 0) {
        ArrayList<Drawable> arrayList = this.mDrawables;
        if (arrayList == null || 
          arrayList.size() == 0)
          return true; 
      } 
      return false;
    }
    
    public void invalidateDrawable(Drawable param1Drawable) {
      invalidate(param1Drawable.getBounds());
    }
    
    protected void dispatchDraw(Canvas param1Canvas) {
      int i;
      param1Canvas.insertReorderBarrier();
      super.dispatchDraw(param1Canvas);
      param1Canvas.insertInorderBarrier();
      ArrayList<Drawable> arrayList = this.mDrawables;
      if (arrayList == null) {
        i = 0;
      } else {
        i = arrayList.size();
      } 
      for (byte b = 0; b < i; b++)
        ((Drawable)this.mDrawables.get(b)).draw(param1Canvas); 
    }
    
    protected void onLayout(boolean param1Boolean, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {}
    
    public void invalidate(Rect param1Rect) {
      super.invalidate(param1Rect);
      View view = this.mHostView;
      if (view != null)
        view.invalidate(param1Rect); 
    }
    
    public void invalidate(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      super.invalidate(param1Int1, param1Int2, param1Int3, param1Int4);
      View view = this.mHostView;
      if (view != null)
        view.invalidate(param1Int1, param1Int2, param1Int3, param1Int4); 
    }
    
    public void invalidate() {
      super.invalidate();
      View view = this.mHostView;
      if (view != null)
        view.invalidate(); 
    }
    
    public void invalidate(boolean param1Boolean) {
      super.invalidate(param1Boolean);
      View view = this.mHostView;
      if (view != null)
        view.invalidate(param1Boolean); 
    }
    
    void invalidateViewProperty(boolean param1Boolean1, boolean param1Boolean2) {
      super.invalidateViewProperty(param1Boolean1, param1Boolean2);
      View view = this.mHostView;
      if (view != null)
        view.invalidateViewProperty(param1Boolean1, param1Boolean2); 
    }
    
    protected void invalidateParentCaches() {
      super.invalidateParentCaches();
      View view = this.mHostView;
      if (view != null)
        view.invalidateParentCaches(); 
    }
    
    protected void invalidateParentIfNeeded() {
      super.invalidateParentIfNeeded();
      View view = this.mHostView;
      if (view != null)
        view.invalidateParentIfNeeded(); 
    }
    
    public void onDescendantInvalidated(View param1View1, View param1View2) {
      View view = this.mHostView;
      if (view != null)
        if (view instanceof ViewGroup) {
          ((ViewGroup)view).onDescendantInvalidated(view, param1View2);
          super.onDescendantInvalidated(param1View1, param1View2);
        } else {
          invalidate();
        }  
    }
    
    public ViewParent invalidateChildInParent(int[] param1ArrayOfint, Rect param1Rect) {
      if (this.mHostView != null) {
        param1Rect.offset(param1ArrayOfint[0], param1ArrayOfint[1]);
        if (this.mHostView instanceof ViewGroup) {
          param1ArrayOfint[0] = 0;
          param1ArrayOfint[1] = 0;
          super.invalidateChildInParent(param1ArrayOfint, param1Rect);
          return ((ViewGroup)this.mHostView).invalidateChildInParent(param1ArrayOfint, param1Rect);
        } 
        invalidate(param1Rect);
      } 
      return null;
    }
  }
}
