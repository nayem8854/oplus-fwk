package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.os.Parcel;

public interface IOplusDirectViewHelper extends IOplusDirectWindow {
  public static final IOplusDirectViewHelper DEFAULT = (IOplusDirectViewHelper)new Object();
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusDirectViewHelper;
  }
  
  default IOplusDirectViewHelper getDefault() {
    return DEFAULT;
  }
  
  default boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) {
    return false;
  }
}
