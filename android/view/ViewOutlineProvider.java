package android.view;

import android.graphics.Outline;

public abstract class ViewOutlineProvider {
  public static final ViewOutlineProvider BACKGROUND = (ViewOutlineProvider)new Object();
  
  public static final ViewOutlineProvider BOUNDS = (ViewOutlineProvider)new Object();
  
  public static final ViewOutlineProvider PADDED_BOUNDS = (ViewOutlineProvider)new Object();
  
  public abstract void getOutline(View paramView, Outline paramOutline);
}
