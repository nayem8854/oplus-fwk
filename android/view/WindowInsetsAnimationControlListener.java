package android.view;

public interface WindowInsetsAnimationControlListener {
  void onCancelled(WindowInsetsAnimationController paramWindowInsetsAnimationController);
  
  void onFinished(WindowInsetsAnimationController paramWindowInsetsAnimationController);
  
  void onReady(WindowInsetsAnimationController paramWindowInsetsAnimationController, int paramInt);
}
