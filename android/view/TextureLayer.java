package android.view;

import android.graphics.Bitmap;
import android.graphics.HardwareRenderer;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import com.android.internal.util.VirtualRefBasePtr;

public final class TextureLayer {
  private VirtualRefBasePtr mFinalizer;
  
  private HardwareRenderer mRenderer;
  
  private TextureLayer(HardwareRenderer paramHardwareRenderer, long paramLong) {
    if (paramHardwareRenderer != null && paramLong != 0L) {
      this.mRenderer = paramHardwareRenderer;
      this.mFinalizer = new VirtualRefBasePtr(paramLong);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Either hardware renderer: ");
    stringBuilder.append(paramHardwareRenderer);
    stringBuilder.append(" or deferredUpdater: ");
    stringBuilder.append(paramLong);
    stringBuilder.append(" is invalid");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setLayerPaint(Paint paramPaint) {
    long l2, l1 = this.mFinalizer.get();
    if (paramPaint != null) {
      l2 = paramPaint.getNativeInstance();
    } else {
      l2 = 0L;
    } 
    nSetLayerPaint(l1, l2);
    this.mRenderer.pushLayerUpdate(this);
  }
  
  public boolean isValid() {
    boolean bool;
    VirtualRefBasePtr virtualRefBasePtr = this.mFinalizer;
    if (virtualRefBasePtr != null && virtualRefBasePtr.get() != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void destroy() {
    if (!isValid())
      return; 
    this.mRenderer.onLayerDestroyed(this);
    this.mRenderer = null;
    this.mFinalizer.release();
    this.mFinalizer = null;
  }
  
  public long getDeferredLayerUpdater() {
    return this.mFinalizer.get();
  }
  
  public boolean copyInto(Bitmap paramBitmap) {
    return this.mRenderer.copyLayerInto(this, paramBitmap);
  }
  
  public boolean prepare(int paramInt1, int paramInt2, boolean paramBoolean) {
    return nPrepare(this.mFinalizer.get(), paramInt1, paramInt2, paramBoolean);
  }
  
  public void setTransform(Matrix paramMatrix) {
    nSetTransform(this.mFinalizer.get(), paramMatrix.native_instance);
    this.mRenderer.pushLayerUpdate(this);
  }
  
  public void detachSurfaceTexture() {
    this.mRenderer.detachSurfaceTexture(this.mFinalizer.get());
  }
  
  public long getLayerHandle() {
    return this.mFinalizer.get();
  }
  
  public void setSurfaceTexture(SurfaceTexture paramSurfaceTexture) {
    nSetSurfaceTexture(this.mFinalizer.get(), paramSurfaceTexture);
    this.mRenderer.pushLayerUpdate(this);
  }
  
  public void updateSurfaceTexture() {
    nUpdateSurfaceTexture(this.mFinalizer.get());
    this.mRenderer.pushLayerUpdate(this);
  }
  
  public static TextureLayer adoptTextureLayer(HardwareRenderer paramHardwareRenderer, long paramLong) {
    return new TextureLayer(paramHardwareRenderer, paramLong);
  }
  
  private static native boolean nPrepare(long paramLong, int paramInt1, int paramInt2, boolean paramBoolean);
  
  private static native void nSetLayerPaint(long paramLong1, long paramLong2);
  
  private static native void nSetSurfaceTexture(long paramLong, SurfaceTexture paramSurfaceTexture);
  
  private static native void nSetTransform(long paramLong1, long paramLong2);
  
  private static native void nUpdateSurfaceTexture(long paramLong);
}
