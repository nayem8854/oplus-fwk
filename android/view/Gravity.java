package android.view;

import android.graphics.Rect;

public class Gravity {
  public static final int AXIS_CLIP = 8;
  
  public static final int AXIS_PULL_AFTER = 4;
  
  public static final int AXIS_PULL_BEFORE = 2;
  
  public static final int AXIS_SPECIFIED = 1;
  
  public static final int AXIS_X_SHIFT = 0;
  
  public static final int AXIS_Y_SHIFT = 4;
  
  public static final int BOTTOM = 80;
  
  public static final int CENTER = 17;
  
  public static final int CENTER_HORIZONTAL = 1;
  
  public static final int CENTER_VERTICAL = 16;
  
  public static final int CLIP_HORIZONTAL = 8;
  
  public static final int CLIP_VERTICAL = 128;
  
  public static final int DISPLAY_CLIP_HORIZONTAL = 16777216;
  
  public static final int DISPLAY_CLIP_VERTICAL = 268435456;
  
  public static final int END = 8388613;
  
  public static final int FILL = 119;
  
  public static final int FILL_HORIZONTAL = 7;
  
  public static final int FILL_VERTICAL = 112;
  
  public static final int HORIZONTAL_GRAVITY_MASK = 7;
  
  public static final int LEFT = 3;
  
  public static final int NO_GRAVITY = 0;
  
  public static final int RELATIVE_HORIZONTAL_GRAVITY_MASK = 8388615;
  
  public static final int RELATIVE_LAYOUT_DIRECTION = 8388608;
  
  public static final int RIGHT = 5;
  
  public static final int START = 8388611;
  
  public static final int TOP = 48;
  
  public static final int VERTICAL_GRAVITY_MASK = 112;
  
  public static void apply(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2) {
    apply(paramInt1, paramInt2, paramInt3, paramRect1, 0, 0, paramRect2);
  }
  
  public static void apply(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, int paramInt4) {
    paramInt1 = getAbsoluteGravity(paramInt1, paramInt4);
    apply(paramInt1, paramInt2, paramInt3, paramRect1, 0, 0, paramRect2);
  }
  
  public static void apply(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, int paramInt4, int paramInt5, Rect paramRect2) {
    int i = paramInt1 & 0x6;
    if (i != 0) {
      if (i != 2) {
        if (i != 4) {
          paramRect1.left += paramInt4;
          paramRect1.right += paramInt4;
        } else {
          paramRect1.right -= paramInt4;
          paramRect2.left = paramRect2.right - paramInt2;
          if ((paramInt1 & 0x8) == 8)
            if (paramRect2.left < paramRect1.left)
              paramRect2.left = paramRect1.left;  
        } 
      } else {
        paramRect1.left += paramInt4;
        paramRect2.right = paramRect2.left + paramInt2;
        if ((paramInt1 & 0x8) == 8)
          if (paramRect2.right > paramRect1.right)
            paramRect2.right = paramRect1.right;  
      } 
    } else {
      paramRect2.left = paramRect1.left + (paramRect1.right - paramRect1.left - paramInt2) / 2 + paramInt4;
      paramRect2.right = paramRect2.left + paramInt2;
      if ((paramInt1 & 0x8) == 8) {
        if (paramRect2.left < paramRect1.left)
          paramRect2.left = paramRect1.left; 
        if (paramRect2.right > paramRect1.right)
          paramRect2.right = paramRect1.right; 
      } 
    } 
    paramInt2 = paramInt1 & 0x60;
    if (paramInt2 != 0) {
      if (paramInt2 != 32) {
        if (paramInt2 != 64) {
          paramRect1.top += paramInt5;
          paramRect1.bottom += paramInt5;
        } else {
          paramRect1.bottom -= paramInt5;
          paramRect2.top = paramRect2.bottom - paramInt3;
          if ((paramInt1 & 0x80) == 128)
            if (paramRect2.top < paramRect1.top)
              paramRect2.top = paramRect1.top;  
        } 
      } else {
        paramRect1.top += paramInt5;
        paramRect2.bottom = paramRect2.top + paramInt3;
        if ((paramInt1 & 0x80) == 128)
          if (paramRect2.bottom > paramRect1.bottom)
            paramRect2.bottom = paramRect1.bottom;  
      } 
    } else {
      paramRect2.top = paramRect1.top + (paramRect1.bottom - paramRect1.top - paramInt3) / 2 + paramInt5;
      paramRect2.bottom = paramRect2.top + paramInt3;
      if ((paramInt1 & 0x80) == 128) {
        if (paramRect2.top < paramRect1.top)
          paramRect2.top = paramRect1.top; 
        if (paramRect2.bottom > paramRect1.bottom)
          paramRect2.bottom = paramRect1.bottom; 
      } 
    } 
  }
  
  public static void apply(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, int paramInt4, int paramInt5, Rect paramRect2, int paramInt6) {
    paramInt1 = getAbsoluteGravity(paramInt1, paramInt6);
    apply(paramInt1, paramInt2, paramInt3, paramRect1, paramInt4, paramInt5, paramRect2);
  }
  
  public static void applyDisplay(int paramInt, Rect paramRect1, Rect paramRect2) {
    if ((0x10000000 & paramInt) != 0) {
      if (paramRect2.top < paramRect1.top)
        paramRect2.top = paramRect1.top; 
      if (paramRect2.bottom > paramRect1.bottom)
        paramRect2.bottom = paramRect1.bottom; 
    } else {
      int i = 0;
      if (paramRect2.top < paramRect1.top) {
        i = paramRect1.top - paramRect2.top;
      } else if (paramRect2.bottom > paramRect1.bottom) {
        i = paramRect1.bottom - paramRect2.bottom;
      } 
      if (i != 0)
        if (paramRect2.height() > paramRect1.bottom - paramRect1.top) {
          paramRect2.top = paramRect1.top;
          paramRect2.bottom = paramRect1.bottom;
        } else {
          paramRect2.top += i;
          paramRect2.bottom += i;
        }  
    } 
    if ((0x1000000 & paramInt) != 0) {
      if (paramRect2.left < paramRect1.left)
        paramRect2.left = paramRect1.left; 
      if (paramRect2.right > paramRect1.right)
        paramRect2.right = paramRect1.right; 
    } else {
      paramInt = 0;
      if (paramRect2.left < paramRect1.left) {
        paramInt = paramRect1.left - paramRect2.left;
      } else if (paramRect2.right > paramRect1.right) {
        paramInt = paramRect1.right - paramRect2.right;
      } 
      if (paramInt != 0)
        if (paramRect2.width() > paramRect1.right - paramRect1.left) {
          paramRect2.left = paramRect1.left;
          paramRect2.right = paramRect1.right;
        } else {
          paramRect2.left += paramInt;
          paramRect2.right += paramInt;
        }  
    } 
  }
  
  public static void applyDisplay(int paramInt1, Rect paramRect1, Rect paramRect2, int paramInt2) {
    paramInt1 = getAbsoluteGravity(paramInt1, paramInt2);
    applyDisplay(paramInt1, paramRect1, paramRect2);
  }
  
  public static boolean isVertical(int paramInt) {
    boolean bool;
    if (paramInt > 0 && (paramInt & 0x70) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isHorizontal(int paramInt) {
    boolean bool;
    if (paramInt > 0 && (0x800007 & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int getAbsoluteGravity(int paramInt1, int paramInt2) {
    int i = paramInt1;
    paramInt1 = i;
    if ((0x800000 & i) > 0) {
      if ((i & 0x800003) == 8388611) {
        paramInt1 = i & 0xFF7FFFFC;
        if (paramInt2 == 1) {
          paramInt1 |= 0x5;
        } else {
          paramInt1 |= 0x3;
        } 
      } else {
        paramInt1 = i;
        if ((i & 0x800005) == 8388613) {
          paramInt1 = i & 0xFF7FFFFA;
          if (paramInt2 == 1) {
            paramInt1 |= 0x3;
          } else {
            paramInt1 |= 0x5;
          } 
        } 
      } 
      paramInt1 &= 0xFF7FFFFF;
    } 
    return paramInt1;
  }
  
  public static String toString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    if ((paramInt & 0x77) == 119) {
      stringBuilder.append("FILL");
      stringBuilder.append(' ');
    } else {
      if ((paramInt & 0x70) == 112) {
        stringBuilder.append("FILL_VERTICAL");
        stringBuilder.append(' ');
      } else {
        if ((paramInt & 0x30) == 48) {
          stringBuilder.append("TOP");
          stringBuilder.append(' ');
        } 
        if ((paramInt & 0x50) == 80) {
          stringBuilder.append("BOTTOM");
          stringBuilder.append(' ');
        } 
      } 
      if ((paramInt & 0x7) == 7) {
        stringBuilder.append("FILL_HORIZONTAL");
        stringBuilder.append(' ');
      } else {
        if ((paramInt & 0x800003) == 8388611) {
          stringBuilder.append("START");
          stringBuilder.append(' ');
        } else if ((paramInt & 0x3) == 3) {
          stringBuilder.append("LEFT");
          stringBuilder.append(' ');
        } 
        if ((paramInt & 0x800005) == 8388613) {
          stringBuilder.append("END");
          stringBuilder.append(' ');
        } else if ((paramInt & 0x5) == 5) {
          stringBuilder.append("RIGHT");
          stringBuilder.append(' ');
        } 
      } 
    } 
    if ((paramInt & 0x11) == 17) {
      stringBuilder.append("CENTER");
      stringBuilder.append(' ');
    } else {
      if ((paramInt & 0x10) == 16) {
        stringBuilder.append("CENTER_VERTICAL");
        stringBuilder.append(' ');
      } 
      if ((paramInt & 0x1) == 1) {
        stringBuilder.append("CENTER_HORIZONTAL");
        stringBuilder.append(' ');
      } 
    } 
    if (stringBuilder.length() == 0) {
      stringBuilder.append("NO GRAVITY");
      stringBuilder.append(' ');
    } 
    if ((paramInt & 0x10000000) == 268435456) {
      stringBuilder.append("DISPLAY_CLIP_VERTICAL");
      stringBuilder.append(' ');
    } 
    if ((paramInt & 0x1000000) == 16777216) {
      stringBuilder.append("DISPLAY_CLIP_HORIZONTAL");
      stringBuilder.append(' ');
    } 
    stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    return stringBuilder.toString();
  }
}
