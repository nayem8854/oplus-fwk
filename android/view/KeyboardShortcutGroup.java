package android.view;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class KeyboardShortcutGroup implements Parcelable {
  public KeyboardShortcutGroup(CharSequence paramCharSequence, List<KeyboardShortcutInfo> paramList) {
    this.mLabel = paramCharSequence;
    this.mItems = new ArrayList<>((Collection<? extends KeyboardShortcutInfo>)Preconditions.checkNotNull(paramList));
  }
  
  public KeyboardShortcutGroup(CharSequence paramCharSequence) {
    this(paramCharSequence, Collections.emptyList());
  }
  
  public KeyboardShortcutGroup(CharSequence paramCharSequence, List<KeyboardShortcutInfo> paramList, boolean paramBoolean) {
    this.mLabel = paramCharSequence;
    this.mItems = new ArrayList<>((Collection<? extends KeyboardShortcutInfo>)Preconditions.checkNotNull(paramList));
    this.mSystemGroup = paramBoolean;
  }
  
  public KeyboardShortcutGroup(CharSequence paramCharSequence, boolean paramBoolean) {
    this(paramCharSequence, Collections.emptyList(), paramBoolean);
  }
  
  private KeyboardShortcutGroup(Parcel paramParcel) {
    this.mItems = new ArrayList<>();
    this.mLabel = paramParcel.readCharSequence();
    paramParcel.readTypedList(this.mItems, KeyboardShortcutInfo.CREATOR);
    int i = paramParcel.readInt();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    this.mSystemGroup = bool;
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public List<KeyboardShortcutInfo> getItems() {
    return this.mItems;
  }
  
  public boolean isSystemGroup() {
    return this.mSystemGroup;
  }
  
  public void addItem(KeyboardShortcutInfo paramKeyboardShortcutInfo) {
    this.mItems.add(paramKeyboardShortcutInfo);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mLabel);
    paramParcel.writeTypedList(this.mItems);
    paramParcel.writeInt(this.mSystemGroup);
  }
  
  public static final Parcelable.Creator<KeyboardShortcutGroup> CREATOR = new Parcelable.Creator<KeyboardShortcutGroup>() {
      public KeyboardShortcutGroup createFromParcel(Parcel param1Parcel) {
        return new KeyboardShortcutGroup(param1Parcel);
      }
      
      public KeyboardShortcutGroup[] newArray(int param1Int) {
        return new KeyboardShortcutGroup[param1Int];
      }
    };
  
  private final List<KeyboardShortcutInfo> mItems;
  
  private final CharSequence mLabel;
  
  private boolean mSystemGroup;
}
