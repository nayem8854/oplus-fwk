package android.view;

import android.content.Context;
import android.util.AttributeSet;

public class OplusBaseTextureView extends View {
  protected boolean mReleaseTextureWhenDestory = false;
  
  protected boolean mCallBackSizeChangeWhenLayerUpdate = false;
  
  private static final String LOCAL_TAG = "OplusBaseTextureView";
  
  private static final boolean DEBUG = true;
  
  public void setReleaseTextureWhenDestory(boolean paramBoolean) {
    this.mReleaseTextureWhenDestory = paramBoolean;
  }
  
  public OplusBaseTextureView(Context paramContext) {
    super(paramContext);
  }
  
  public OplusBaseTextureView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public OplusBaseTextureView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public OplusBaseTextureView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public boolean isReleaseTextureWhenDestory() {
    return this.mReleaseTextureWhenDestory;
  }
  
  public void setCallBackSizeChangeWhenLayerUpdate(boolean paramBoolean) {
    this.mCallBackSizeChangeWhenLayerUpdate = paramBoolean;
  }
  
  public boolean isCallBackSizeChangeWhenLayerUpdate() {
    return this.mCallBackSizeChangeWhenLayerUpdate;
  }
  
  protected void onSetRefreshRate(int paramInt) {}
}
