package android.view;

import android.os.IBinder;

public final class InputApplicationHandle {
  public final long dispatchingTimeoutNanos;
  
  public final String name;
  
  private long ptr;
  
  public final IBinder token;
  
  public InputApplicationHandle(IBinder paramIBinder, String paramString, long paramLong) {
    this.token = paramIBinder;
    this.name = paramString;
    this.dispatchingTimeoutNanos = paramLong;
  }
  
  public InputApplicationHandle(InputApplicationHandle paramInputApplicationHandle) {
    this.token = paramInputApplicationHandle.token;
    this.dispatchingTimeoutNanos = paramInputApplicationHandle.dispatchingTimeoutNanos;
    this.name = paramInputApplicationHandle.name;
  }
  
  private native void nativeDispose();
  
  protected void finalize() throws Throwable {
    try {
      nativeDispose();
      return;
    } finally {
      super.finalize();
    } 
  }
}
