package android.view.inputmethod;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.util.Log;
import com.android.internal.inputmethod.IInputMethodPrivilegedOperations;
import com.android.internal.view.IInlineSuggestionsRequestCallback;
import com.android.internal.view.InlineSuggestionsRequestInfo;

public interface InputMethod {
  public static final String SERVICE_INTERFACE = "android.view.InputMethod";
  
  public static final String SERVICE_META_DATA = "android.view.im";
  
  public static final int SHOW_EXPLICIT = 1;
  
  public static final int SHOW_FORCED = 2;
  
  public static final String TAG = "InputMethod";
  
  default void initializeInternal(IBinder paramIBinder, int paramInt, IInputMethodPrivilegedOperations paramIInputMethodPrivilegedOperations) {
    updateInputMethodDisplay(paramInt);
    attachToken(paramIBinder);
  }
  
  default void onCreateInlineSuggestionsRequest(InlineSuggestionsRequestInfo paramInlineSuggestionsRequestInfo, IInlineSuggestionsRequestCallback paramIInlineSuggestionsRequestCallback) {
    try {
      paramIInlineSuggestionsRequestCallback.onInlineSuggestionsUnsupported();
    } catch (RemoteException remoteException) {
      Log.w("InputMethod", "Failed to call onInlineSuggestionsUnsupported.", (Throwable)remoteException);
    } 
  }
  
  default void updateInputMethodDisplay(int paramInt) {}
  
  default void dispatchStartInputWithToken(InputConnection paramInputConnection, EditorInfo paramEditorInfo, boolean paramBoolean1, IBinder paramIBinder, boolean paramBoolean2) {
    if (paramBoolean1) {
      restartInput(paramInputConnection, paramEditorInfo);
    } else {
      startInput(paramInputConnection, paramEditorInfo);
    } 
  }
  
  default void showSoftInputWithToken(int paramInt, ResultReceiver paramResultReceiver, IBinder paramIBinder) {
    showSoftInput(paramInt, paramResultReceiver);
  }
  
  void attachToken(IBinder paramIBinder);
  
  void bindInput(InputBinding paramInputBinding);
  
  void changeInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype);
  
  void createSession(SessionCallback paramSessionCallback);
  
  void hideSoftInput(int paramInt, ResultReceiver paramResultReceiver);
  
  void hideSoftInputWithToken(int paramInt, ResultReceiver paramResultReceiver, IBinder paramIBinder);
  
  void restartInput(InputConnection paramInputConnection, EditorInfo paramEditorInfo);
  
  void revokeSession(InputMethodSession paramInputMethodSession);
  
  void setCurrentHideInputToken(IBinder paramIBinder);
  
  void setCurrentShowInputToken(IBinder paramIBinder);
  
  void setSessionEnabled(InputMethodSession paramInputMethodSession, boolean paramBoolean);
  
  void showSoftInput(int paramInt, ResultReceiver paramResultReceiver);
  
  void startInput(InputConnection paramInputConnection, EditorInfo paramEditorInfo);
  
  void unbindInput();
  
  public static interface SessionCallback {
    void sessionCreated(InputMethodSession param1InputMethodSession);
  }
}
