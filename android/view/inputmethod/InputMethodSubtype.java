package android.view.inputmethod;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.icu.text.DisplayContext;
import android.icu.text.LocaleDisplayNames;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Slog;
import com.android.internal.inputmethod.SubtypeLocaleUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Locale;

public final class InputMethodSubtype implements Parcelable {
  public static final Parcelable.Creator<InputMethodSubtype> CREATOR;
  
  private static final String EXTRA_KEY_UNTRANSLATABLE_STRING_IN_SUBTYPE_NAME = "UntranslatableReplacementStringInSubtypeName";
  
  private static final String EXTRA_VALUE_KEY_VALUE_SEPARATOR = "=";
  
  private static final String EXTRA_VALUE_PAIR_SEPARATOR = ",";
  
  private static final String LANGUAGE_TAG_NONE = "";
  
  private static final int SUBTYPE_ID_NONE = 0;
  
  private static final String TAG = InputMethodSubtype.class.getSimpleName();
  
  private volatile Locale mCachedLocaleObj;
  
  private volatile HashMap<String, String> mExtraValueHashMapCache;
  
  private final boolean mIsAsciiCapable;
  
  private final boolean mIsAuxiliary;
  
  private final Object mLock = new Object();
  
  private final boolean mOverridesImplicitlyEnabledSubtype;
  
  private final String mSubtypeExtraValue;
  
  private final int mSubtypeHashCode;
  
  private final int mSubtypeIconResId;
  
  private final int mSubtypeId;
  
  private final String mSubtypeLanguageTag;
  
  private final String mSubtypeLocale;
  
  private final String mSubtypeMode;
  
  private final int mSubtypeNameResId;
  
  class InputMethodSubtypeBuilder {
    public InputMethodSubtypeBuilder setIsAuxiliary(boolean param1Boolean) {
      this.mIsAuxiliary = param1Boolean;
      return this;
    }
    
    private boolean mIsAuxiliary = false;
    
    public InputMethodSubtypeBuilder setOverridesImplicitlyEnabledSubtype(boolean param1Boolean) {
      this.mOverridesImplicitlyEnabledSubtype = param1Boolean;
      return this;
    }
    
    private boolean mOverridesImplicitlyEnabledSubtype = false;
    
    public InputMethodSubtypeBuilder setIsAsciiCapable(boolean param1Boolean) {
      this.mIsAsciiCapable = param1Boolean;
      return this;
    }
    
    private boolean mIsAsciiCapable = false;
    
    public InputMethodSubtypeBuilder setSubtypeIconResId(int param1Int) {
      this.mSubtypeIconResId = param1Int;
      return this;
    }
    
    private int mSubtypeIconResId = 0;
    
    public InputMethodSubtypeBuilder setSubtypeNameResId(int param1Int) {
      this.mSubtypeNameResId = param1Int;
      return this;
    }
    
    private int mSubtypeNameResId = 0;
    
    public InputMethodSubtypeBuilder setSubtypeId(int param1Int) {
      this.mSubtypeId = param1Int;
      return this;
    }
    
    private int mSubtypeId = 0;
    
    public InputMethodSubtypeBuilder setSubtypeLocale(String param1String) {
      if (param1String == null)
        param1String = ""; 
      this.mSubtypeLocale = param1String;
      return this;
    }
    
    private String mSubtypeLocale = "";
    
    public InputMethodSubtypeBuilder setLanguageTag(String param1String) {
      if (param1String == null)
        param1String = ""; 
      this.mSubtypeLanguageTag = param1String;
      return this;
    }
    
    private String mSubtypeLanguageTag = "";
    
    public InputMethodSubtypeBuilder setSubtypeMode(String param1String) {
      if (param1String == null)
        param1String = ""; 
      this.mSubtypeMode = param1String;
      return this;
    }
    
    private String mSubtypeMode = "";
    
    public InputMethodSubtypeBuilder setSubtypeExtraValue(String param1String) {
      if (param1String == null)
        param1String = ""; 
      this.mSubtypeExtraValue = param1String;
      return this;
    }
    
    private String mSubtypeExtraValue = "";
    
    public InputMethodSubtype build() {
      return new InputMethodSubtype(this);
    }
  }
  
  private static InputMethodSubtypeBuilder getBuilder(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, int paramInt3, boolean paramBoolean3) {
    InputMethodSubtypeBuilder inputMethodSubtypeBuilder = new InputMethodSubtypeBuilder();
    InputMethodSubtypeBuilder.access$102(inputMethodSubtypeBuilder, paramInt1);
    InputMethodSubtypeBuilder.access$202(inputMethodSubtypeBuilder, paramInt2);
    InputMethodSubtypeBuilder.access$302(inputMethodSubtypeBuilder, paramString1);
    InputMethodSubtypeBuilder.access$402(inputMethodSubtypeBuilder, paramString2);
    InputMethodSubtypeBuilder.access$502(inputMethodSubtypeBuilder, paramString3);
    InputMethodSubtypeBuilder.access$602(inputMethodSubtypeBuilder, paramBoolean1);
    InputMethodSubtypeBuilder.access$702(inputMethodSubtypeBuilder, paramBoolean2);
    InputMethodSubtypeBuilder.access$802(inputMethodSubtypeBuilder, paramInt3);
    InputMethodSubtypeBuilder.access$902(inputMethodSubtypeBuilder, paramBoolean3);
    return inputMethodSubtypeBuilder;
  }
  
  @Deprecated
  public InputMethodSubtype(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2) {
    this(paramInt1, paramInt2, paramString1, paramString2, paramString3, paramBoolean1, paramBoolean2, 0);
  }
  
  @Deprecated
  public InputMethodSubtype(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, int paramInt3) {
    this(getBuilder(paramInt1, paramInt2, paramString1, paramString2, paramString3, paramBoolean1, paramBoolean2, paramInt3, false));
  }
  
  private InputMethodSubtype(InputMethodSubtypeBuilder paramInputMethodSubtypeBuilder) {
    this.mSubtypeNameResId = paramInputMethodSubtypeBuilder.mSubtypeNameResId;
    this.mSubtypeIconResId = paramInputMethodSubtypeBuilder.mSubtypeIconResId;
    this.mSubtypeLocale = paramInputMethodSubtypeBuilder.mSubtypeLocale;
    this.mSubtypeLanguageTag = paramInputMethodSubtypeBuilder.mSubtypeLanguageTag;
    this.mSubtypeMode = paramInputMethodSubtypeBuilder.mSubtypeMode;
    this.mSubtypeExtraValue = paramInputMethodSubtypeBuilder.mSubtypeExtraValue;
    this.mIsAuxiliary = paramInputMethodSubtypeBuilder.mIsAuxiliary;
    this.mOverridesImplicitlyEnabledSubtype = paramInputMethodSubtypeBuilder.mOverridesImplicitlyEnabledSubtype;
    this.mSubtypeId = paramInputMethodSubtypeBuilder.mSubtypeId;
    boolean bool = paramInputMethodSubtypeBuilder.mIsAsciiCapable;
    int i = this.mSubtypeId;
    if (i != 0) {
      this.mSubtypeHashCode = i;
    } else {
      this.mSubtypeHashCode = hashCodeInternal(this.mSubtypeLocale, this.mSubtypeMode, this.mSubtypeExtraValue, this.mIsAuxiliary, this.mOverridesImplicitlyEnabledSubtype, bool);
    } 
  }
  
  InputMethodSubtype(Parcel paramParcel) {
    this.mSubtypeNameResId = paramParcel.readInt();
    this.mSubtypeIconResId = paramParcel.readInt();
    String str1 = paramParcel.readString();
    String str2 = "";
    if (str1 == null)
      str1 = ""; 
    this.mSubtypeLocale = str1;
    str1 = paramParcel.readString();
    if (str1 == null)
      str1 = ""; 
    this.mSubtypeLanguageTag = str1;
    str1 = paramParcel.readString();
    if (str1 == null)
      str1 = ""; 
    this.mSubtypeMode = str1;
    String str3 = paramParcel.readString();
    str1 = str2;
    if (str3 != null)
      str1 = str3; 
    this.mSubtypeExtraValue = str1;
    int i = paramParcel.readInt();
    boolean bool1 = false;
    if (i == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsAuxiliary = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mOverridesImplicitlyEnabledSubtype = bool2;
    this.mSubtypeHashCode = paramParcel.readInt();
    this.mSubtypeId = paramParcel.readInt();
    boolean bool2 = bool1;
    if (paramParcel.readInt() == 1)
      bool2 = true; 
    this.mIsAsciiCapable = bool2;
  }
  
  public int getNameResId() {
    return this.mSubtypeNameResId;
  }
  
  public int getIconResId() {
    return this.mSubtypeIconResId;
  }
  
  @Deprecated
  public String getLocale() {
    return this.mSubtypeLocale;
  }
  
  public String getLanguageTag() {
    return this.mSubtypeLanguageTag;
  }
  
  public Locale getLocaleObject() {
    if (this.mCachedLocaleObj != null)
      return this.mCachedLocaleObj; 
    synchronized (this.mLock) {
      if (this.mCachedLocaleObj != null)
        return this.mCachedLocaleObj; 
      if (!TextUtils.isEmpty(this.mSubtypeLanguageTag)) {
        this.mCachedLocaleObj = Locale.forLanguageTag(this.mSubtypeLanguageTag);
      } else {
        this.mCachedLocaleObj = SubtypeLocaleUtils.constructLocaleFromString(this.mSubtypeLocale);
      } 
      return this.mCachedLocaleObj;
    } 
  }
  
  public String getMode() {
    return this.mSubtypeMode;
  }
  
  public String getExtraValue() {
    return this.mSubtypeExtraValue;
  }
  
  public boolean isAuxiliary() {
    return this.mIsAuxiliary;
  }
  
  public boolean overridesImplicitlyEnabledSubtype() {
    return this.mOverridesImplicitlyEnabledSubtype;
  }
  
  public boolean isAsciiCapable() {
    return this.mIsAsciiCapable;
  }
  
  public CharSequence getDisplayName(Context paramContext, String paramString, ApplicationInfo paramApplicationInfo) {
    String str1;
    if (this.mSubtypeNameResId == 0)
      return getLocaleDisplayName(getLocaleFromContext(paramContext), getLocaleObject(), DisplayContext.CAPITALIZATION_FOR_UI_LIST_OR_MENU); 
    CharSequence charSequence = paramContext.getPackageManager().getText(paramString, this.mSubtypeNameResId, paramApplicationInfo);
    if (TextUtils.isEmpty(charSequence))
      return ""; 
    String str2 = charSequence.toString();
    if (containsExtraValueKey("UntranslatableReplacementStringInSubtypeName")) {
      str1 = getExtraValueOf("UntranslatableReplacementStringInSubtypeName");
    } else {
      DisplayContext displayContext;
      if (TextUtils.equals(str2, "%s")) {
        displayContext = DisplayContext.CAPITALIZATION_FOR_UI_LIST_OR_MENU;
      } else if (str2.startsWith("%s")) {
        displayContext = DisplayContext.CAPITALIZATION_FOR_BEGINNING_OF_SENTENCE;
      } else {
        displayContext = DisplayContext.CAPITALIZATION_FOR_MIDDLE_OF_SENTENCE;
      } 
      Locale locale2 = getLocaleFromContext((Context)str1);
      Locale locale1 = getLocaleObject();
      str1 = getLocaleDisplayName(locale2, locale1, displayContext);
    } 
    paramString = str1;
    if (str1 == null)
      paramString = ""; 
    try {
      return String.format(str2, new Object[] { paramString });
    } catch (IllegalFormatException illegalFormatException) {
      paramString = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Found illegal format in subtype name(");
      stringBuilder.append(charSequence);
      stringBuilder.append("): ");
      stringBuilder.append(illegalFormatException);
      Slog.w(paramString, stringBuilder.toString());
      return "";
    } 
  }
  
  private static Locale getLocaleFromContext(Context paramContext) {
    if (paramContext == null)
      return null; 
    if (paramContext.getResources() == null)
      return null; 
    Configuration configuration = paramContext.getResources().getConfiguration();
    if (configuration == null)
      return null; 
    return configuration.getLocales().get(0);
  }
  
  private static String getLocaleDisplayName(Locale paramLocale1, Locale paramLocale2, DisplayContext paramDisplayContext) {
    if (paramLocale2 == null)
      return ""; 
    if (paramLocale1 == null)
      paramLocale1 = Locale.getDefault(); 
    LocaleDisplayNames localeDisplayNames = LocaleDisplayNames.getInstance(paramLocale1, new DisplayContext[] { paramDisplayContext });
    return localeDisplayNames.localeDisplayName(paramLocale2);
  }
  
  private HashMap<String, String> getExtraValueHashMap() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mExtraValueHashMapCache : Ljava/util/HashMap;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull -> 15
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: areturn
    //   15: new java/util/HashMap
    //   18: astore_2
    //   19: aload_2
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: getfield mSubtypeExtraValue : Ljava/lang/String;
    //   27: ldc ','
    //   29: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   32: astore_3
    //   33: iconst_0
    //   34: istore #4
    //   36: iload #4
    //   38: aload_3
    //   39: arraylength
    //   40: if_icmpge -> 109
    //   43: aload_3
    //   44: iload #4
    //   46: aaload
    //   47: ldc '='
    //   49: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   52: astore_1
    //   53: aload_1
    //   54: arraylength
    //   55: iconst_1
    //   56: if_icmpne -> 71
    //   59: aload_2
    //   60: aload_1
    //   61: iconst_0
    //   62: aaload
    //   63: aconst_null
    //   64: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   67: pop
    //   68: goto -> 103
    //   71: aload_1
    //   72: arraylength
    //   73: iconst_1
    //   74: if_icmple -> 103
    //   77: aload_1
    //   78: arraylength
    //   79: iconst_2
    //   80: if_icmple -> 92
    //   83: getstatic android/view/inputmethod/InputMethodSubtype.TAG : Ljava/lang/String;
    //   86: ldc 'ExtraValue has two or more '='s'
    //   88: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   91: pop
    //   92: aload_2
    //   93: aload_1
    //   94: iconst_0
    //   95: aaload
    //   96: aload_1
    //   97: iconst_1
    //   98: aaload
    //   99: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   102: pop
    //   103: iinc #4, 1
    //   106: goto -> 36
    //   109: aload_0
    //   110: aload_2
    //   111: putfield mExtraValueHashMapCache : Ljava/util/HashMap;
    //   114: aload_0
    //   115: monitorexit
    //   116: aload_2
    //   117: areturn
    //   118: astore_1
    //   119: aload_0
    //   120: monitorexit
    //   121: aload_1
    //   122: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #535	-> 0
    //   #536	-> 2
    //   #537	-> 7
    //   #538	-> 11
    //   #540	-> 15
    //   #541	-> 23
    //   #542	-> 33
    //   #543	-> 43
    //   #544	-> 53
    //   #545	-> 59
    //   #546	-> 71
    //   #547	-> 77
    //   #548	-> 83
    //   #550	-> 92
    //   #542	-> 103
    //   #553	-> 109
    //   #554	-> 114
    //   #555	-> 118
    // Exception table:
    //   from	to	target	type
    //   2	7	118	finally
    //   11	13	118	finally
    //   15	23	118	finally
    //   23	33	118	finally
    //   36	43	118	finally
    //   43	53	118	finally
    //   53	59	118	finally
    //   59	68	118	finally
    //   71	77	118	finally
    //   77	83	118	finally
    //   83	92	118	finally
    //   92	103	118	finally
    //   109	114	118	finally
    //   114	116	118	finally
    //   119	121	118	finally
  }
  
  public boolean containsExtraValueKey(String paramString) {
    return getExtraValueHashMap().containsKey(paramString);
  }
  
  public String getExtraValueOf(String paramString) {
    return getExtraValueHashMap().get(paramString);
  }
  
  public int hashCode() {
    return this.mSubtypeHashCode;
  }
  
  public final boolean hasSubtypeId() {
    boolean bool;
    if (this.mSubtypeId != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final int getSubtypeId() {
    return this.mSubtypeId;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = paramObject instanceof InputMethodSubtype, bool2 = false, bool3 = false;
    if (bool1) {
      paramObject = paramObject;
      if (((InputMethodSubtype)paramObject).mSubtypeId != 0 || this.mSubtypeId != 0) {
        bool3 = bool2;
        if (paramObject.hashCode() == hashCode())
          bool3 = true; 
        return bool3;
      } 
      if (paramObject.hashCode() == hashCode() && 
        paramObject.getLocale().equals(getLocale()) && 
        paramObject.getLanguageTag().equals(getLanguageTag()) && 
        paramObject.getMode().equals(getMode()) && 
        paramObject.getExtraValue().equals(getExtraValue()) && 
        paramObject.isAuxiliary() == isAuxiliary()) {
        bool2 = paramObject.overridesImplicitlyEnabledSubtype();
        if (bool2 == overridesImplicitlyEnabledSubtype() && 
          paramObject.isAsciiCapable() == isAsciiCapable())
          bool3 = true; 
      } 
      return bool3;
    } 
    return false;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSubtypeNameResId);
    paramParcel.writeInt(this.mSubtypeIconResId);
    paramParcel.writeString(this.mSubtypeLocale);
    paramParcel.writeString(this.mSubtypeLanguageTag);
    paramParcel.writeString(this.mSubtypeMode);
    paramParcel.writeString(this.mSubtypeExtraValue);
    paramParcel.writeInt(this.mIsAuxiliary);
    paramParcel.writeInt(this.mOverridesImplicitlyEnabledSubtype);
    paramParcel.writeInt(this.mSubtypeHashCode);
    paramParcel.writeInt(this.mSubtypeId);
    paramParcel.writeInt(this.mIsAsciiCapable);
  }
  
  static {
    CREATOR = (Parcelable.Creator<InputMethodSubtype>)new Object();
  }
  
  private static int hashCodeInternal(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    if ((paramBoolean3 ^ true) != 0)
      return Arrays.hashCode(new Object[] { paramString1, paramString2, paramString3, Boolean.valueOf(paramBoolean1), Boolean.valueOf(paramBoolean2) }); 
    return Arrays.hashCode(new Object[] { paramString1, paramString2, paramString3, Boolean.valueOf(paramBoolean1), Boolean.valueOf(paramBoolean2), Boolean.valueOf(paramBoolean3) });
  }
  
  public static List<InputMethodSubtype> sort(Context paramContext, int paramInt, InputMethodInfo paramInputMethodInfo, List<InputMethodSubtype> paramList) {
    if (paramInputMethodInfo == null)
      return paramList; 
    HashSet<InputMethodSubtype> hashSet = new HashSet<>(paramList);
    ArrayList<InputMethodSubtype> arrayList = new ArrayList();
    int i = paramInputMethodInfo.getSubtypeCount();
    for (paramInt = 0; paramInt < i; paramInt++) {
      InputMethodSubtype inputMethodSubtype = paramInputMethodInfo.getSubtypeAt(paramInt);
      if (hashSet.contains(inputMethodSubtype)) {
        arrayList.add(inputMethodSubtype);
        hashSet.remove(inputMethodSubtype);
      } 
    } 
    for (InputMethodSubtype inputMethodSubtype : hashSet)
      arrayList.add(inputMethodSubtype); 
    return arrayList;
  }
}
