package android.view.inputmethod;

import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public final class SparseRectFArray implements Parcelable {
  public SparseRectFArray(Parcel paramParcel) {
    this.mKeys = paramParcel.createIntArray();
    this.mCoordinates = paramParcel.createFloatArray();
    this.mFlagsArray = paramParcel.createIntArray();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeIntArray(this.mKeys);
    paramParcel.writeFloatArray(this.mCoordinates);
    paramParcel.writeIntArray(this.mFlagsArray);
  }
  
  public int hashCode() {
    int[] arrayOfInt = this.mKeys;
    if (arrayOfInt == null || arrayOfInt.length == 0)
      return 0; 
    int i = arrayOfInt.length;
    int j;
    for (j = 0; j < 4; j++)
      i = (int)((i * 31) + this.mCoordinates[j]); 
    j = this.mFlagsArray[0];
    return i * 31 + j;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof SparseRectFArray))
      return false; 
    SparseRectFArray sparseRectFArray = (SparseRectFArray)paramObject;
    if (Arrays.equals(this.mKeys, sparseRectFArray.mKeys) && Arrays.equals(this.mCoordinates, sparseRectFArray.mCoordinates)) {
      paramObject = this.mFlagsArray;
      int[] arrayOfInt = sparseRectFArray.mFlagsArray;
      if (Arrays.equals((int[])paramObject, arrayOfInt))
        bool = true; 
    } 
    return bool;
  }
  
  public String toString() {
    if (this.mKeys == null || this.mCoordinates == null || this.mFlagsArray == null)
      return "SparseRectFArray{}"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SparseRectFArray{");
    for (byte b = 0; b < this.mKeys.length; b++) {
      if (b != 0)
        stringBuilder.append(", "); 
      int i = b * 4;
      stringBuilder.append(this.mKeys[b]);
      stringBuilder.append(":[");
      stringBuilder.append(this.mCoordinates[i + 0]);
      stringBuilder.append(",");
      stringBuilder.append(this.mCoordinates[i + 1]);
      stringBuilder.append("],[");
      stringBuilder.append(this.mCoordinates[i + 2]);
      stringBuilder.append(",");
      stringBuilder.append(this.mCoordinates[i + 3]);
      stringBuilder.append("]:flagsArray=");
      stringBuilder.append(this.mFlagsArray[b]);
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  class SparseRectFArrayBuilder {
    private void checkIndex(int param1Int) {
      int i = this.mCount;
      if (i == 0)
        return; 
      if (this.mKeys[i - 1] < param1Int)
        return; 
      throw new IllegalArgumentException("key must be greater than all existing keys.");
    }
    
    private void ensureBufferSize() {
      if (this.mKeys == null)
        this.mKeys = new int[INITIAL_SIZE]; 
      if (this.mCoordinates == null)
        this.mCoordinates = new float[INITIAL_SIZE * 4]; 
      if (this.mFlagsArray == null)
        this.mFlagsArray = new int[INITIAL_SIZE]; 
      int i = this.mCount, j = i + 1;
      int[] arrayOfInt1 = this.mKeys;
      if (arrayOfInt1.length <= j) {
        int[] arrayOfInt = new int[j * 2];
        System.arraycopy(arrayOfInt1, 0, arrayOfInt, 0, i);
        this.mKeys = arrayOfInt;
      } 
      int k = this.mCount;
      i = (k + 1) * 4;
      float[] arrayOfFloat = this.mCoordinates;
      if (arrayOfFloat.length <= i) {
        float[] arrayOfFloat1 = new float[i * 2];
        System.arraycopy(arrayOfFloat, 0, arrayOfFloat1, 0, k * 4);
        this.mCoordinates = arrayOfFloat1;
      } 
      int[] arrayOfInt2 = this.mFlagsArray;
      if (arrayOfInt2.length <= j) {
        arrayOfInt1 = new int[j * 2];
        System.arraycopy(arrayOfInt2, 0, arrayOfInt1, 0, this.mCount);
        this.mFlagsArray = arrayOfInt1;
      } 
    }
    
    public SparseRectFArrayBuilder append(int param1Int1, float param1Float1, float param1Float2, float param1Float3, float param1Float4, int param1Int2) {
      checkIndex(param1Int1);
      ensureBufferSize();
      int i = this.mCount, j = i * 4;
      float[] arrayOfFloat = this.mCoordinates;
      arrayOfFloat[j + 0] = param1Float1;
      arrayOfFloat[j + 1] = param1Float2;
      arrayOfFloat[j + 2] = param1Float3;
      arrayOfFloat[j + 3] = param1Float4;
      j = this.mCount;
      this.mFlagsArray[j] = param1Int2;
      this.mKeys[i] = param1Int1;
      this.mCount = i + 1;
      return this;
    }
    
    private int mCount = 0;
    
    private int[] mKeys = null;
    
    private float[] mCoordinates = null;
    
    private int[] mFlagsArray = null;
    
    private static int INITIAL_SIZE = 16;
    
    public boolean isEmpty() {
      boolean bool;
      if (this.mCount <= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public SparseRectFArray build() {
      return new SparseRectFArray(this);
    }
    
    public void reset() {
      if (this.mCount == 0) {
        this.mKeys = null;
        this.mCoordinates = null;
        this.mFlagsArray = null;
      } 
      this.mCount = 0;
    }
  }
  
  private SparseRectFArray(SparseRectFArrayBuilder paramSparseRectFArrayBuilder) {
    if (paramSparseRectFArrayBuilder.mCount == 0) {
      this.mKeys = null;
      this.mCoordinates = null;
      this.mFlagsArray = null;
    } else {
      this.mKeys = new int[paramSparseRectFArrayBuilder.mCount];
      this.mCoordinates = new float[paramSparseRectFArrayBuilder.mCount * 4];
      this.mFlagsArray = new int[paramSparseRectFArrayBuilder.mCount];
      System.arraycopy(paramSparseRectFArrayBuilder.mKeys, 0, this.mKeys, 0, paramSparseRectFArrayBuilder.mCount);
      System.arraycopy(paramSparseRectFArrayBuilder.mCoordinates, 0, this.mCoordinates, 0, paramSparseRectFArrayBuilder.mCount * 4);
      System.arraycopy(paramSparseRectFArrayBuilder.mFlagsArray, 0, this.mFlagsArray, 0, paramSparseRectFArrayBuilder.mCount);
    } 
  }
  
  public RectF get(int paramInt) {
    int[] arrayOfInt = this.mKeys;
    if (arrayOfInt == null)
      return null; 
    if (paramInt < 0)
      return null; 
    paramInt = Arrays.binarySearch(arrayOfInt, paramInt);
    if (paramInt < 0)
      return null; 
    paramInt *= 4;
    float[] arrayOfFloat = this.mCoordinates;
    return new RectF(arrayOfFloat[paramInt], arrayOfFloat[paramInt + 1], arrayOfFloat[paramInt + 2], arrayOfFloat[paramInt + 3]);
  }
  
  public int getFlags(int paramInt1, int paramInt2) {
    int[] arrayOfInt = this.mKeys;
    if (arrayOfInt == null)
      return paramInt2; 
    if (paramInt1 < 0)
      return paramInt2; 
    paramInt1 = Arrays.binarySearch(arrayOfInt, paramInt1);
    if (paramInt1 < 0)
      return paramInt2; 
    return this.mFlagsArray[paramInt1];
  }
  
  public static final Parcelable.Creator<SparseRectFArray> CREATOR = new Parcelable.Creator<SparseRectFArray>() {
      public SparseRectFArray createFromParcel(Parcel param1Parcel) {
        return new SparseRectFArray(param1Parcel);
      }
      
      public SparseRectFArray[] newArray(int param1Int) {
        return new SparseRectFArray[param1Int];
      }
    };
  
  private final float[] mCoordinates;
  
  private final int[] mFlagsArray;
  
  private final int[] mKeys;
  
  public int describeContents() {
    return 0;
  }
}
