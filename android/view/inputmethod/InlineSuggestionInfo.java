package android.view.inputmethod;

import android.annotation.NonNull;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.inline.InlinePresentationSpec;
import com.android.internal.util.AnnotationValidations;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;

public final class InlineSuggestionInfo implements Parcelable {
  public static InlineSuggestionInfo newInlineSuggestionInfo(InlinePresentationSpec paramInlinePresentationSpec, String paramString1, String[] paramArrayOfString, String paramString2, boolean paramBoolean) {
    return new InlineSuggestionInfo(paramInlinePresentationSpec, paramString1, paramArrayOfString, paramString2, paramBoolean);
  }
  
  public InlineSuggestionInfo(InlinePresentationSpec paramInlinePresentationSpec, String paramString1, String[] paramArrayOfString, String paramString2, boolean paramBoolean) {
    this.mInlinePresentationSpec = paramInlinePresentationSpec;
    AnnotationValidations.validate(NonNull.class, null, paramInlinePresentationSpec);
    this.mSource = paramString1;
    if (!Objects.equals(paramString1, "android:autofill")) {
      String str = this.mSource;
      if (!Objects.equals(str, "android:platform")) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("source was ");
        stringBuilder.append(this.mSource);
        stringBuilder.append(" but must be one of: SOURCE_AUTOFILL(");
        stringBuilder.append("android:autofill");
        stringBuilder.append("), SOURCE_PLATFORM(");
        stringBuilder.append("android:platform");
        stringBuilder.append(")");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
    AnnotationValidations.validate(NonNull.class, null, this.mSource);
    this.mAutofillHints = paramArrayOfString;
    this.mType = paramString2;
    if (!Objects.equals(paramString2, "android:autofill:suggestion")) {
      String str = this.mType;
      if (!Objects.equals(str, "android:autofill:action")) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("type was ");
        stringBuilder.append(this.mType);
        stringBuilder.append(" but must be one of: TYPE_SUGGESTION(");
        stringBuilder.append("android:autofill:suggestion");
        stringBuilder.append("), TYPE_ACTION(");
        stringBuilder.append("android:autofill:action");
        stringBuilder.append(")");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
    AnnotationValidations.validate(NonNull.class, null, this.mType);
    this.mPinned = paramBoolean;
  }
  
  public InlinePresentationSpec getInlinePresentationSpec() {
    return this.mInlinePresentationSpec;
  }
  
  public String getSource() {
    return this.mSource;
  }
  
  public String[] getAutofillHints() {
    return this.mAutofillHints;
  }
  
  public String getType() {
    return this.mType;
  }
  
  public boolean isPinned() {
    return this.mPinned;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InlineSuggestionInfo { inlinePresentationSpec = ");
    stringBuilder.append(this.mInlinePresentationSpec);
    stringBuilder.append(", source = ");
    stringBuilder.append(this.mSource);
    stringBuilder.append(", autofillHints = ");
    String[] arrayOfString = this.mAutofillHints;
    stringBuilder.append(Arrays.toString((Object[])arrayOfString));
    stringBuilder.append(", type = ");
    stringBuilder.append(this.mType);
    stringBuilder.append(", pinned = ");
    stringBuilder.append(this.mPinned);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    InlinePresentationSpec inlinePresentationSpec1 = this.mInlinePresentationSpec, inlinePresentationSpec2 = ((InlineSuggestionInfo)paramObject).mInlinePresentationSpec;
    if (Objects.equals(inlinePresentationSpec1, inlinePresentationSpec2)) {
      String str2 = this.mSource, str1 = ((InlineSuggestionInfo)paramObject).mSource;
      if (Objects.equals(str2, str1)) {
        String[] arrayOfString2 = this.mAutofillHints, arrayOfString1 = ((InlineSuggestionInfo)paramObject).mAutofillHints;
        if (Arrays.equals((Object[])arrayOfString2, (Object[])arrayOfString1)) {
          String str3 = this.mType, str4 = ((InlineSuggestionInfo)paramObject).mType;
          if (Objects.equals(str3, str4) && this.mPinned == ((InlineSuggestionInfo)paramObject).mPinned)
            return null; 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mInlinePresentationSpec);
    int j = Objects.hashCode(this.mSource);
    int k = Arrays.hashCode((Object[])this.mAutofillHints);
    int m = Objects.hashCode(this.mType);
    int n = Boolean.hashCode(this.mPinned);
    return ((((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b1 = 0;
    if (this.mPinned)
      b1 = (byte)(0x0 | 0x10); 
    byte b2 = b1;
    if (this.mAutofillHints != null)
      b2 = (byte)(b1 | 0x4); 
    paramParcel.writeByte(b2);
    paramParcel.writeTypedObject(this.mInlinePresentationSpec, paramInt);
    paramParcel.writeString(this.mSource);
    String[] arrayOfString = this.mAutofillHints;
    if (arrayOfString != null)
      paramParcel.writeStringArray(arrayOfString); 
    paramParcel.writeString(this.mType);
  }
  
  public int describeContents() {
    return 0;
  }
  
  InlineSuggestionInfo(Parcel paramParcel) {
    StringBuilder stringBuilder;
    boolean bool;
    String[] arrayOfString;
    byte b = paramParcel.readByte();
    if ((b & 0x10) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    InlinePresentationSpec inlinePresentationSpec = (InlinePresentationSpec)paramParcel.readTypedObject(InlinePresentationSpec.CREATOR);
    String str2 = paramParcel.readString();
    if ((b & 0x4) == 0) {
      arrayOfString = null;
    } else {
      arrayOfString = paramParcel.createStringArray();
    } 
    String str1 = paramParcel.readString();
    this.mInlinePresentationSpec = inlinePresentationSpec;
    AnnotationValidations.validate(NonNull.class, null, inlinePresentationSpec);
    this.mSource = str2;
    if (!Objects.equals(str2, "android:autofill")) {
      str2 = this.mSource;
      if (!Objects.equals(str2, "android:platform")) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("source was ");
        stringBuilder.append(this.mSource);
        stringBuilder.append(" but must be one of: SOURCE_AUTOFILL(");
        stringBuilder.append("android:autofill");
        stringBuilder.append("), SOURCE_PLATFORM(");
        stringBuilder.append("android:platform");
        stringBuilder.append(")");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
    AnnotationValidations.validate(NonNull.class, null, this.mSource);
    this.mAutofillHints = arrayOfString;
    this.mType = (String)stringBuilder;
    if (!Objects.equals(stringBuilder, "android:autofill:suggestion")) {
      String str = this.mType;
      if (!Objects.equals(str, "android:autofill:action")) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("type was ");
        stringBuilder1.append(this.mType);
        stringBuilder1.append(" but must be one of: TYPE_SUGGESTION(");
        stringBuilder1.append("android:autofill:suggestion");
        stringBuilder1.append("), TYPE_ACTION(");
        stringBuilder1.append("android:autofill:action");
        stringBuilder1.append(")");
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
    } 
    AnnotationValidations.validate(NonNull.class, null, this.mType);
    this.mPinned = bool;
  }
  
  public static final Parcelable.Creator<InlineSuggestionInfo> CREATOR = new Parcelable.Creator<InlineSuggestionInfo>() {
      public InlineSuggestionInfo[] newArray(int param1Int) {
        return new InlineSuggestionInfo[param1Int];
      }
      
      public InlineSuggestionInfo createFromParcel(Parcel param1Parcel) {
        return new InlineSuggestionInfo(param1Parcel);
      }
    };
  
  public static final String SOURCE_AUTOFILL = "android:autofill";
  
  public static final String SOURCE_PLATFORM = "android:platform";
  
  public static final String TYPE_ACTION = "android:autofill:action";
  
  public static final String TYPE_SUGGESTION = "android:autofill:suggestion";
  
  private final String[] mAutofillHints;
  
  private final InlinePresentationSpec mInlinePresentationSpec;
  
  private final boolean mPinned;
  
  private final String mSource;
  
  private final String mType;
  
  @Deprecated
  private void __metadata() {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Source implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Type implements Annotation {}
}
