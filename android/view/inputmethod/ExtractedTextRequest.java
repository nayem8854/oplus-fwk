package android.view.inputmethod;

import android.os.Parcel;
import android.os.Parcelable;

public class ExtractedTextRequest implements Parcelable {
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.token);
    paramParcel.writeInt(this.flags);
    paramParcel.writeInt(this.hintMaxLines);
    paramParcel.writeInt(this.hintMaxChars);
  }
  
  public static final Parcelable.Creator<ExtractedTextRequest> CREATOR = new Parcelable.Creator<ExtractedTextRequest>() {
      public ExtractedTextRequest createFromParcel(Parcel param1Parcel) {
        ExtractedTextRequest extractedTextRequest = new ExtractedTextRequest();
        extractedTextRequest.token = param1Parcel.readInt();
        extractedTextRequest.flags = param1Parcel.readInt();
        extractedTextRequest.hintMaxLines = param1Parcel.readInt();
        extractedTextRequest.hintMaxChars = param1Parcel.readInt();
        return extractedTextRequest;
      }
      
      public ExtractedTextRequest[] newArray(int param1Int) {
        return new ExtractedTextRequest[param1Int];
      }
    };
  
  public int flags;
  
  public int hintMaxChars;
  
  public int hintMaxLines;
  
  public int token;
  
  public int describeContents() {
    return 0;
  }
}
