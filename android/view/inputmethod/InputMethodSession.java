package android.view.inputmethod;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;

public interface InputMethodSession {
  void appPrivateCommand(String paramString, Bundle paramBundle);
  
  void dispatchGenericMotionEvent(int paramInt, MotionEvent paramMotionEvent, EventCallback paramEventCallback);
  
  void dispatchKeyEvent(int paramInt, KeyEvent paramKeyEvent, EventCallback paramEventCallback);
  
  void dispatchTrackballEvent(int paramInt, MotionEvent paramMotionEvent, EventCallback paramEventCallback);
  
  void displayCompletions(CompletionInfo[] paramArrayOfCompletionInfo);
  
  void finishInput();
  
  void notifyImeHidden();
  
  void removeImeSurface();
  
  void toggleSoftInput(int paramInt1, int paramInt2);
  
  void updateCursor(Rect paramRect);
  
  void updateCursorAnchorInfo(CursorAnchorInfo paramCursorAnchorInfo);
  
  void updateExtractedText(int paramInt, ExtractedText paramExtractedText);
  
  void updateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);
  
  void viewClicked(boolean paramBoolean);
  
  public static interface EventCallback {
    void finishedEvent(int param1Int, boolean param1Boolean);
  }
}
