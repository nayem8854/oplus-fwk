package android.view.inputmethod;

import android.annotation.NonNull;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class InlineSuggestionsResponse implements Parcelable {
  public static InlineSuggestionsResponse newInlineSuggestionsResponse(List<InlineSuggestion> paramList) {
    return new InlineSuggestionsResponse(paramList);
  }
  
  public InlineSuggestionsResponse(List<InlineSuggestion> paramList) {
    this.mInlineSuggestions = paramList;
    AnnotationValidations.validate(NonNull.class, null, paramList);
  }
  
  public List<InlineSuggestion> getInlineSuggestions() {
    return this.mInlineSuggestions;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InlineSuggestionsResponse { inlineSuggestions = ");
    stringBuilder.append(this.mInlineSuggestions);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object<InlineSuggestion> paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    InlineSuggestionsResponse inlineSuggestionsResponse = (InlineSuggestionsResponse)paramObject;
    paramObject = (Object<InlineSuggestion>)this.mInlineSuggestions;
    List<InlineSuggestion> list = inlineSuggestionsResponse.mInlineSuggestions;
    return 
      Objects.equals(paramObject, list);
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mInlineSuggestions);
    return 1 * 31 + i;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelableList(this.mInlineSuggestions, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  InlineSuggestionsResponse(Parcel paramParcel) {
    ArrayList<InlineSuggestion> arrayList = new ArrayList();
    paramParcel.readParcelableList(arrayList, InlineSuggestion.class.getClassLoader());
    this.mInlineSuggestions = arrayList;
    AnnotationValidations.validate(NonNull.class, null, arrayList);
  }
  
  public static final Parcelable.Creator<InlineSuggestionsResponse> CREATOR = new Parcelable.Creator<InlineSuggestionsResponse>() {
      public InlineSuggestionsResponse[] newArray(int param1Int) {
        return new InlineSuggestionsResponse[param1Int];
      }
      
      public InlineSuggestionsResponse createFromParcel(Parcel param1Parcel) {
        return new InlineSuggestionsResponse(param1Parcel);
      }
    };
  
  private final List<InlineSuggestion> mInlineSuggestions;
  
  @Deprecated
  private void __metadata() {}
}
