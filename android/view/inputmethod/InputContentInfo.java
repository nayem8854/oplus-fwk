package android.view.inputmethod;

import android.content.ClipDescription;
import android.content.ContentProvider;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.UserHandle;
import com.android.internal.inputmethod.IInputContentUriToken;
import java.security.InvalidParameterException;

public final class InputContentInfo implements Parcelable {
  public InputContentInfo(Uri paramUri, ClipDescription paramClipDescription) {
    this(paramUri, paramClipDescription, null);
  }
  
  public InputContentInfo(Uri paramUri1, ClipDescription paramClipDescription, Uri paramUri2) {
    validateInternal(paramUri1, paramClipDescription, paramUri2, true);
    this.mContentUri = paramUri1;
    this.mContentUriOwnerUserId = ContentProvider.getUserIdFromUri(paramUri1, UserHandle.myUserId());
    this.mDescription = paramClipDescription;
    this.mLinkUri = paramUri2;
  }
  
  public boolean validate() {
    return validateInternal(this.mContentUri, this.mDescription, this.mLinkUri, false);
  }
  
  private static boolean validateInternal(Uri paramUri1, ClipDescription paramClipDescription, Uri paramUri2, boolean paramBoolean) {
    if (paramUri1 == null) {
      if (!paramBoolean)
        return false; 
      throw new NullPointerException("contentUri");
    } 
    if (paramClipDescription == null) {
      if (!paramBoolean)
        return false; 
      throw new NullPointerException("description");
    } 
    String str = paramUri1.getScheme();
    if (!"content".equals(str)) {
      if (!paramBoolean)
        return false; 
      throw new InvalidParameterException("contentUri must have content scheme");
    } 
    if (paramUri2 != null) {
      str = paramUri2.getScheme();
      if (str == null || (
        !str.equalsIgnoreCase("http") && !str.equalsIgnoreCase("https"))) {
        if (!paramBoolean)
          return false; 
        throw new InvalidParameterException("linkUri must have either http or https scheme");
      } 
    } 
    return true;
  }
  
  public Uri getContentUri() {
    if (this.mContentUriOwnerUserId != UserHandle.myUserId())
      return ContentProvider.maybeAddUserId(this.mContentUri, this.mContentUriOwnerUserId); 
    return this.mContentUri;
  }
  
  public ClipDescription getDescription() {
    return this.mDescription;
  }
  
  public Uri getLinkUri() {
    return this.mLinkUri;
  }
  
  public void setUriToken(IInputContentUriToken paramIInputContentUriToken) {
    if (this.mUriToken == null) {
      this.mUriToken = paramIInputContentUriToken;
      return;
    } 
    throw new IllegalStateException("URI token is already set");
  }
  
  public void requestPermission() {
    IInputContentUriToken iInputContentUriToken = this.mUriToken;
    if (iInputContentUriToken == null)
      return; 
    try {
      iInputContentUriToken.take();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void releasePermission() {
    IInputContentUriToken iInputContentUriToken = this.mUriToken;
    if (iInputContentUriToken == null)
      return; 
    try {
      iInputContentUriToken.release();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    Uri.writeToParcel(paramParcel, this.mContentUri);
    paramParcel.writeInt(this.mContentUriOwnerUserId);
    this.mDescription.writeToParcel(paramParcel, paramInt);
    Uri.writeToParcel(paramParcel, this.mLinkUri);
    if (this.mUriToken != null) {
      paramParcel.writeInt(1);
      paramParcel.writeStrongBinder(this.mUriToken.asBinder());
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  private InputContentInfo(Parcel paramParcel) {
    this.mContentUri = (Uri)Uri.CREATOR.createFromParcel(paramParcel);
    this.mContentUriOwnerUserId = paramParcel.readInt();
    this.mDescription = (ClipDescription)ClipDescription.CREATOR.createFromParcel(paramParcel);
    this.mLinkUri = (Uri)Uri.CREATOR.createFromParcel(paramParcel);
    if (paramParcel.readInt() == 1) {
      this.mUriToken = IInputContentUriToken.Stub.asInterface(paramParcel.readStrongBinder());
    } else {
      this.mUriToken = null;
    } 
  }
  
  public static final Parcelable.Creator<InputContentInfo> CREATOR = new Parcelable.Creator<InputContentInfo>() {
      public InputContentInfo createFromParcel(Parcel param1Parcel) {
        return new InputContentInfo(param1Parcel);
      }
      
      public InputContentInfo[] newArray(int param1Int) {
        return new InputContentInfo[param1Int];
      }
    };
  
  private final Uri mContentUri;
  
  private final int mContentUriOwnerUserId;
  
  private final ClipDescription mDescription;
  
  private final Uri mLinkUri;
  
  private IInputContentUriToken mUriToken;
  
  public int describeContents() {
    return 0;
  }
}
