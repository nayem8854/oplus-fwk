package android.view.inputmethod;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class CompletionInfo implements Parcelable {
  public CompletionInfo(long paramLong, int paramInt, CharSequence paramCharSequence) {
    this.mId = paramLong;
    this.mPosition = paramInt;
    this.mText = paramCharSequence;
    this.mLabel = null;
  }
  
  public CompletionInfo(long paramLong, int paramInt, CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    this.mId = paramLong;
    this.mPosition = paramInt;
    this.mText = paramCharSequence1;
    this.mLabel = paramCharSequence2;
  }
  
  private CompletionInfo(Parcel paramParcel) {
    this.mId = paramParcel.readLong();
    this.mPosition = paramParcel.readInt();
    this.mText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mLabel = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
  }
  
  public long getId() {
    return this.mId;
  }
  
  public int getPosition() {
    return this.mPosition;
  }
  
  public CharSequence getText() {
    return this.mText;
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CompletionInfo{#");
    stringBuilder.append(this.mPosition);
    stringBuilder.append(" \"");
    stringBuilder.append(this.mText);
    stringBuilder.append("\" id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(" label=");
    stringBuilder.append(this.mLabel);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mId);
    paramParcel.writeInt(this.mPosition);
    TextUtils.writeToParcel(this.mText, paramParcel, paramInt);
    TextUtils.writeToParcel(this.mLabel, paramParcel, paramInt);
  }
  
  public static final Parcelable.Creator<CompletionInfo> CREATOR = new Parcelable.Creator<CompletionInfo>() {
      public CompletionInfo createFromParcel(Parcel param1Parcel) {
        return new CompletionInfo(param1Parcel);
      }
      
      public CompletionInfo[] newArray(int param1Int) {
        return new CompletionInfo[param1Int];
      }
    };
  
  private final long mId;
  
  private final CharSequence mLabel;
  
  private final int mPosition;
  
  private final CharSequence mText;
  
  public int describeContents() {
    return 0;
  }
}
