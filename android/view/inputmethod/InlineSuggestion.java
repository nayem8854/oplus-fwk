package android.view.inputmethod;

import android.annotation.NonNull;
import android.content.Context;
import android.os.Handler;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Size;
import android.util.Slog;
import android.view.SurfaceControlViewHost;
import android.view.ViewGroup;
import android.widget.inline.InlineContentView;
import com.android.internal.util.AnnotationValidations;
import com.android.internal.util.Parcelling;
import com.android.internal.view.inline.IInlineContentCallback;
import com.android.internal.view.inline.IInlineContentProvider;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public final class InlineSuggestion implements Parcelable {
  public static InlineSuggestion newInlineSuggestion(InlineSuggestionInfo paramInlineSuggestionInfo) {
    return new InlineSuggestion(paramInlineSuggestionInfo, null, null);
  }
  
  public InlineSuggestion(InlineSuggestionInfo paramInlineSuggestionInfo, IInlineContentProvider paramIInlineContentProvider) {
    this(paramInlineSuggestionInfo, paramIInlineContentProvider, null);
  }
  
  public void inflate(Context paramContext, Size paramSize, Executor paramExecutor, Consumer<InlineContentView> paramConsumer) {
    Size size1 = this.mInfo.getInlinePresentationSpec().getMinSize();
    Size size2 = this.mInfo.getInlinePresentationSpec().getMaxSize();
    if (isValid(paramSize.getWidth(), size1.getWidth(), size2.getWidth()) && 
      isValid(paramSize.getHeight(), size1.getHeight(), size2.getHeight())) {
      this.mInlineContentCallback = getInlineContentCallback(paramContext, paramExecutor, paramConsumer);
      IInlineContentProvider iInlineContentProvider = this.mContentProvider;
      if (iInlineContentProvider == null) {
        paramExecutor.execute(new _$$Lambda$InlineSuggestion$1Z84E2nGcL7UkGjb1l1YHUY09Qo(paramConsumer));
        return;
      } 
      try {
        int i = paramSize.getWidth(), j = paramSize.getHeight();
        InlineContentCallbackWrapper inlineContentCallbackWrapper = new InlineContentCallbackWrapper();
        this(this.mInlineContentCallback);
        iInlineContentProvider.provideContent(i, j, (IInlineContentCallback)inlineContentCallbackWrapper);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Error creating suggestion content surface: ");
        stringBuilder1.append(remoteException);
        Slog.w("InlineSuggestion", stringBuilder1.toString());
        paramExecutor.execute(new _$$Lambda$InlineSuggestion$Svlod0cVfjyzxKJchAioH7zzqJQ(paramConsumer));
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("size is neither between min:");
    stringBuilder.append(size1);
    stringBuilder.append(" and max:");
    stringBuilder.append(size2);
    stringBuilder.append(", nor wrap_content");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static boolean isValid(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = true;
    if (paramInt1 == -2)
      return true; 
    if (paramInt1 < paramInt2 || paramInt1 > paramInt3)
      bool = false; 
    return bool;
  }
  
  private InlineContentCallbackImpl getInlineContentCallback(Context paramContext, Executor paramExecutor, Consumer<InlineContentView> paramConsumer) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mInlineContentCallback : Landroid/view/inputmethod/InlineSuggestion$InlineContentCallbackImpl;
    //   6: ifnonnull -> 28
    //   9: new android/view/inputmethod/InlineSuggestion$InlineContentCallbackImpl
    //   12: dup
    //   13: aload_1
    //   14: aload_0
    //   15: getfield mContentProvider : Lcom/android/internal/view/inline/IInlineContentProvider;
    //   18: aload_2
    //   19: aload_3
    //   20: invokespecial <init> : (Landroid/content/Context;Lcom/android/internal/view/inline/IInlineContentProvider;Ljava/util/concurrent/Executor;Ljava/util/function/Consumer;)V
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: areturn
    //   28: new java/lang/IllegalStateException
    //   31: astore_1
    //   32: aload_1
    //   33: ldc 'Already called #inflate()'
    //   35: invokespecial <init> : (Ljava/lang/String;)V
    //   38: aload_1
    //   39: athrow
    //   40: astore_1
    //   41: aload_0
    //   42: monitorexit
    //   43: aload_1
    //   44: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #166	-> 2
    //   #169	-> 9
    //   #167	-> 28
    //   #165	-> 40
    // Exception table:
    //   from	to	target	type
    //   2	9	40	finally
    //   9	24	40	finally
    //   28	40	40	finally
  }
  
  class InlineContentCallbackWrapper extends IInlineContentCallback.Stub {
    private final WeakReference<InlineSuggestion.InlineContentCallbackImpl> mCallbackImpl;
    
    InlineContentCallbackWrapper(InlineSuggestion this$0) {
      this.mCallbackImpl = new WeakReference(this$0);
    }
    
    public void onContent(SurfaceControlViewHost.SurfacePackage param1SurfacePackage, int param1Int1, int param1Int2) {
      InlineSuggestion.InlineContentCallbackImpl inlineContentCallbackImpl = this.mCallbackImpl.get();
      if (inlineContentCallbackImpl != null)
        inlineContentCallbackImpl.onContent(param1SurfacePackage, param1Int1, param1Int2); 
    }
    
    public void onClick() {
      InlineSuggestion.InlineContentCallbackImpl inlineContentCallbackImpl = this.mCallbackImpl.get();
      if (inlineContentCallbackImpl != null)
        inlineContentCallbackImpl.onClick(); 
    }
    
    public void onLongClick() {
      InlineSuggestion.InlineContentCallbackImpl inlineContentCallbackImpl = this.mCallbackImpl.get();
      if (inlineContentCallbackImpl != null)
        inlineContentCallbackImpl.onLongClick(); 
    }
  }
  
  class InlineContentCallbackImpl {
    private InlineContentView mView;
    
    private Consumer<SurfaceControlViewHost.SurfacePackage> mSurfacePackageConsumer;
    
    private SurfaceControlViewHost.SurfacePackage mSurfacePackage;
    
    private final Handler mMainHandler = new Handler(Looper.getMainLooper());
    
    private final IInlineContentProvider mInlineContentProvider;
    
    private boolean mFirstContentReceived = false;
    
    private final Context mContext;
    
    private final Executor mCallbackExecutor;
    
    private final Consumer<InlineContentView> mCallback;
    
    InlineContentCallbackImpl(InlineSuggestion this$0, IInlineContentProvider param1IInlineContentProvider, Executor param1Executor, Consumer<InlineContentView> param1Consumer) {
      this.mContext = (Context)this$0;
      this.mInlineContentProvider = param1IInlineContentProvider;
      this.mCallbackExecutor = param1Executor;
      this.mCallback = param1Consumer;
    }
    
    public void onContent(SurfaceControlViewHost.SurfacePackage param1SurfacePackage, int param1Int1, int param1Int2) {
      this.mMainHandler.post(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$1iQeV802Da0EJqlELwXcCuQIVdM(this, param1SurfacePackage, param1Int1, param1Int2));
    }
    
    private void handleOnContent(SurfaceControlViewHost.SurfacePackage param1SurfacePackage, int param1Int1, int param1Int2) {
      if (!this.mFirstContentReceived) {
        handleOnFirstContentReceived(param1SurfacePackage, param1Int1, param1Int2);
        this.mFirstContentReceived = true;
      } else {
        handleOnSurfacePackage(param1SurfacePackage);
      } 
    }
    
    private void handleOnFirstContentReceived(SurfaceControlViewHost.SurfacePackage param1SurfacePackage, int param1Int1, int param1Int2) {
      this.mSurfacePackage = param1SurfacePackage;
      if (param1SurfacePackage == null) {
        this.mCallbackExecutor.execute(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$n8U3GjMW_nvrfe7Z6T3PZXOJb5o(this));
      } else {
        InlineContentView inlineContentView = new InlineContentView(this.mContext);
        inlineContentView.setLayoutParams(new ViewGroup.LayoutParams(param1Int1, param1Int2));
        this.mView.setChildSurfacePackageUpdater(getSurfacePackageUpdater());
        this.mCallbackExecutor.execute(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$uxQy_Xvm9tEtpDfDJOobVwjMLjY(this));
      } 
    }
    
    private void handleOnSurfacePackage(SurfaceControlViewHost.SurfacePackage param1SurfacePackage) {
      if (param1SurfacePackage == null)
        return; 
      if (this.mSurfacePackage == null) {
        Consumer<SurfaceControlViewHost.SurfacePackage> consumer = this.mSurfacePackageConsumer;
        if (consumer != null) {
          this.mSurfacePackage = param1SurfacePackage;
          if (param1SurfacePackage == null)
            return; 
          if (consumer != null) {
            consumer.accept(param1SurfacePackage);
            this.mSurfacePackageConsumer = null;
          } 
          return;
        } 
      } 
      param1SurfacePackage.release();
      try {
        this.mInlineContentProvider.onSurfacePackageReleased();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error calling onSurfacePackageReleased(): ");
        stringBuilder.append(remoteException);
        Slog.w("InlineSuggestion", stringBuilder.toString());
      } 
    }
    
    private void handleOnSurfacePackageReleased() {
      if (this.mSurfacePackage != null) {
        try {
          this.mInlineContentProvider.onSurfacePackageReleased();
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error calling onSurfacePackageReleased(): ");
          stringBuilder.append(remoteException);
          Slog.w("InlineSuggestion", stringBuilder.toString());
        } 
        this.mSurfacePackage = null;
      } 
      this.mSurfacePackageConsumer = null;
    }
    
    private void handleGetSurfacePackage(Consumer<SurfaceControlViewHost.SurfacePackage> param1Consumer) {
      SurfaceControlViewHost.SurfacePackage surfacePackage = this.mSurfacePackage;
      if (surfacePackage != null) {
        param1Consumer.accept(surfacePackage);
      } else {
        this.mSurfacePackageConsumer = param1Consumer;
        try {
          this.mInlineContentProvider.requestSurfacePackage();
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error calling getSurfacePackage(): ");
          stringBuilder.append(remoteException);
          Slog.w("InlineSuggestion", stringBuilder.toString());
          param1Consumer.accept(null);
          this.mSurfacePackageConsumer = null;
        } 
      } 
    }
    
    private InlineContentView.SurfacePackageUpdater getSurfacePackageUpdater() {
      return new InlineContentView.SurfacePackageUpdater() {
          final InlineSuggestion.InlineContentCallbackImpl this$0;
          
          public void onSurfacePackageReleased() {
            this.this$0.mMainHandler.post(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$1$D6eD5QR1yay7_fJkCbPrrjOmAUs(this));
          }
          
          public void getSurfacePackage(Consumer<SurfaceControlViewHost.SurfacePackage> param1Consumer) {
            this.this$0.mMainHandler.post(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$1$kqBwKkTdRUmxIUsJkLYtI_7F9u0(this, param1Consumer));
          }
        };
    }
    
    public void onClick() {
      this.mMainHandler.post(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$eBD70lfriFltBhoclpzNcJGZRdA(this));
    }
    
    public void onLongClick() {
      this.mMainHandler.post(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$kJ55cHQ8DrRkgYoKfUMf2DIhrBo(this));
    }
  }
  
  class null implements InlineContentView.SurfacePackageUpdater {
    final InlineSuggestion.InlineContentCallbackImpl this$0;
    
    public void onSurfacePackageReleased() {
      this.this$0.mMainHandler.post(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$1$D6eD5QR1yay7_fJkCbPrrjOmAUs(this));
    }
    
    public void getSurfacePackage(Consumer<SurfaceControlViewHost.SurfacePackage> param1Consumer) {
      this.this$0.mMainHandler.post(new _$$Lambda$InlineSuggestion$InlineContentCallbackImpl$1$kqBwKkTdRUmxIUsJkLYtI_7F9u0(this, param1Consumer));
    }
  }
  
  private static class InlineContentCallbackImplParceling implements Parcelling<InlineContentCallbackImpl> {
    private InlineContentCallbackImplParceling() {}
    
    public void parcel(InlineSuggestion.InlineContentCallbackImpl param1InlineContentCallbackImpl, Parcel param1Parcel, int param1Int) {}
    
    public InlineSuggestion.InlineContentCallbackImpl unparcel(Parcel param1Parcel) {
      return null;
    }
  }
  
  public InlineSuggestion(InlineSuggestionInfo paramInlineSuggestionInfo, IInlineContentProvider paramIInlineContentProvider, InlineContentCallbackImpl paramInlineContentCallbackImpl) {
    this.mInfo = paramInlineSuggestionInfo;
    AnnotationValidations.validate(NonNull.class, null, paramInlineSuggestionInfo);
    this.mContentProvider = paramIInlineContentProvider;
    this.mInlineContentCallback = paramInlineContentCallbackImpl;
  }
  
  public InlineSuggestionInfo getInfo() {
    return this.mInfo;
  }
  
  public IInlineContentProvider getContentProvider() {
    return this.mContentProvider;
  }
  
  public InlineContentCallbackImpl getInlineContentCallback() {
    return this.mInlineContentCallback;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InlineSuggestion { info = ");
    stringBuilder.append(this.mInfo);
    stringBuilder.append(", contentProvider = ");
    stringBuilder.append(this.mContentProvider);
    stringBuilder.append(", inlineContentCallback = ");
    stringBuilder.append(this.mInlineContentCallback);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    InlineSuggestionInfo inlineSuggestionInfo1 = this.mInfo, inlineSuggestionInfo2 = ((InlineSuggestion)paramObject).mInfo;
    if (Objects.equals(inlineSuggestionInfo1, inlineSuggestionInfo2)) {
      IInlineContentProvider iInlineContentProvider1 = this.mContentProvider, iInlineContentProvider2 = ((InlineSuggestion)paramObject).mContentProvider;
      if (Objects.equals(iInlineContentProvider1, iInlineContentProvider2)) {
        InlineContentCallbackImpl inlineContentCallbackImpl = this.mInlineContentCallback;
        paramObject = ((InlineSuggestion)paramObject).mInlineContentCallback;
        if (Objects.equals(inlineContentCallbackImpl, paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mInfo);
    int j = Objects.hashCode(this.mContentProvider);
    int k = Objects.hashCode(this.mInlineContentCallback);
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  static {
    Parcelling<InlineContentCallbackImpl> parcelling = Parcelling.Cache.get(InlineContentCallbackImplParceling.class);
    if (parcelling == null)
      sParcellingForInlineContentCallback = Parcelling.Cache.put(new InlineContentCallbackImplParceling()); 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b1 = 0;
    if (this.mContentProvider != null)
      b1 = (byte)(0x0 | 0x2); 
    byte b2 = b1;
    if (this.mInlineContentCallback != null)
      b2 = (byte)(b1 | 0x4); 
    paramParcel.writeByte(b2);
    paramParcel.writeTypedObject(this.mInfo, paramInt);
    IInlineContentProvider iInlineContentProvider = this.mContentProvider;
    if (iInlineContentProvider != null)
      paramParcel.writeStrongInterface((IInterface)iInlineContentProvider); 
    sParcellingForInlineContentCallback.parcel(this.mInlineContentCallback, paramParcel, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  InlineSuggestion(Parcel paramParcel) {
    IInlineContentProvider iInlineContentProvider;
    byte b = paramParcel.readByte();
    InlineSuggestionInfo inlineSuggestionInfo = (InlineSuggestionInfo)paramParcel.readTypedObject(InlineSuggestionInfo.CREATOR);
    if ((b & 0x2) == 0) {
      iInlineContentProvider = null;
    } else {
      iInlineContentProvider = IInlineContentProvider.Stub.asInterface(paramParcel.readStrongBinder());
    } 
    InlineContentCallbackImpl inlineContentCallbackImpl = (InlineContentCallbackImpl)sParcellingForInlineContentCallback.unparcel(paramParcel);
    this.mInfo = inlineSuggestionInfo;
    AnnotationValidations.validate(NonNull.class, null, inlineSuggestionInfo);
    this.mContentProvider = iInlineContentProvider;
    this.mInlineContentCallback = inlineContentCallbackImpl;
  }
  
  public static final Parcelable.Creator<InlineSuggestion> CREATOR = new Parcelable.Creator<InlineSuggestion>() {
      public InlineSuggestion[] newArray(int param1Int) {
        return new InlineSuggestion[param1Int];
      }
      
      public InlineSuggestion createFromParcel(Parcel param1Parcel) {
        return new InlineSuggestion(param1Parcel);
      }
    };
  
  private static final String TAG = "InlineSuggestion";
  
  static Parcelling<InlineContentCallbackImpl> sParcellingForInlineContentCallback;
  
  private final IInlineContentProvider mContentProvider;
  
  private final InlineSuggestionInfo mInfo;
  
  private InlineContentCallbackImpl mInlineContentCallback;
  
  @Deprecated
  private void __metadata() {}
}
