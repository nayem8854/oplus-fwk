package android.view.inputmethod;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class ExtractedText implements Parcelable {
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    TextUtils.writeToParcel(this.text, paramParcel, paramInt);
    paramParcel.writeInt(this.startOffset);
    paramParcel.writeInt(this.partialStartOffset);
    paramParcel.writeInt(this.partialEndOffset);
    paramParcel.writeInt(this.selectionStart);
    paramParcel.writeInt(this.selectionEnd);
    paramParcel.writeInt(this.flags);
    TextUtils.writeToParcel(this.hint, paramParcel, paramInt);
  }
  
  public static final Parcelable.Creator<ExtractedText> CREATOR = new Parcelable.Creator<ExtractedText>() {
      public ExtractedText createFromParcel(Parcel param1Parcel) {
        ExtractedText extractedText = new ExtractedText();
        extractedText.text = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        extractedText.startOffset = param1Parcel.readInt();
        extractedText.partialStartOffset = param1Parcel.readInt();
        extractedText.partialEndOffset = param1Parcel.readInt();
        extractedText.selectionStart = param1Parcel.readInt();
        extractedText.selectionEnd = param1Parcel.readInt();
        extractedText.flags = param1Parcel.readInt();
        extractedText.hint = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        return extractedText;
      }
      
      public ExtractedText[] newArray(int param1Int) {
        return new ExtractedText[param1Int];
      }
    };
  
  public static final int FLAG_SELECTING = 2;
  
  public static final int FLAG_SINGLE_LINE = 1;
  
  public int flags;
  
  public CharSequence hint;
  
  public int partialEndOffset;
  
  public int partialStartOffset;
  
  public int selectionEnd;
  
  public int selectionStart;
  
  public int startOffset;
  
  public CharSequence text;
  
  public int describeContents() {
    return 0;
  }
}
