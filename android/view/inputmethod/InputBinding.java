package android.view.inputmethod;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class InputBinding implements Parcelable {
  public InputBinding(InputConnection paramInputConnection, IBinder paramIBinder, int paramInt1, int paramInt2) {
    this.mConnection = paramInputConnection;
    this.mConnectionToken = paramIBinder;
    this.mUid = paramInt1;
    this.mPid = paramInt2;
  }
  
  public InputBinding(InputConnection paramInputConnection, InputBinding paramInputBinding) {
    this.mConnection = paramInputConnection;
    this.mConnectionToken = paramInputBinding.getConnectionToken();
    this.mUid = paramInputBinding.getUid();
    this.mPid = paramInputBinding.getPid();
  }
  
  InputBinding(Parcel paramParcel) {
    this.mConnection = null;
    this.mConnectionToken = paramParcel.readStrongBinder();
    this.mUid = paramParcel.readInt();
    this.mPid = paramParcel.readInt();
  }
  
  public InputConnection getConnection() {
    return this.mConnection;
  }
  
  public IBinder getConnectionToken() {
    return this.mConnectionToken;
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public int getPid() {
    return this.mPid;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InputBinding{");
    stringBuilder.append(this.mConnectionToken);
    stringBuilder.append(" / uid ");
    stringBuilder.append(this.mUid);
    stringBuilder.append(" / pid ");
    stringBuilder.append(this.mPid);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.mConnectionToken);
    paramParcel.writeInt(this.mUid);
    paramParcel.writeInt(this.mPid);
  }
  
  public static final Parcelable.Creator<InputBinding> CREATOR = new Parcelable.Creator<InputBinding>() {
      public InputBinding createFromParcel(Parcel param1Parcel) {
        return new InputBinding(param1Parcel);
      }
      
      public InputBinding[] newArray(int param1Int) {
        return new InputBinding[param1Int];
      }
    };
  
  static final String TAG = "InputBinding";
  
  final InputConnection mConnection;
  
  final IBinder mConnectionToken;
  
  final int mPid;
  
  final int mUid;
  
  public int describeContents() {
    return 0;
  }
}
