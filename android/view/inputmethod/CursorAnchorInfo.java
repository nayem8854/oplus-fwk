package android.view.inputmethod;

import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannedString;
import android.text.TextUtils;
import java.util.Arrays;
import java.util.Objects;

public final class CursorAnchorInfo implements Parcelable {
  public CursorAnchorInfo(Parcel paramParcel) {
    this.mHashCode = paramParcel.readInt();
    this.mSelectionStart = paramParcel.readInt();
    this.mSelectionEnd = paramParcel.readInt();
    this.mComposingTextStart = paramParcel.readInt();
    this.mComposingText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mInsertionMarkerFlags = paramParcel.readInt();
    this.mInsertionMarkerHorizontal = paramParcel.readFloat();
    this.mInsertionMarkerTop = paramParcel.readFloat();
    this.mInsertionMarkerBaseline = paramParcel.readFloat();
    this.mInsertionMarkerBottom = paramParcel.readFloat();
    this.mCharacterBoundsArray = (SparseRectFArray)paramParcel.readParcelable(SparseRectFArray.class.getClassLoader());
    this.mMatrixValues = paramParcel.createFloatArray();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mHashCode);
    paramParcel.writeInt(this.mSelectionStart);
    paramParcel.writeInt(this.mSelectionEnd);
    paramParcel.writeInt(this.mComposingTextStart);
    TextUtils.writeToParcel(this.mComposingText, paramParcel, paramInt);
    paramParcel.writeInt(this.mInsertionMarkerFlags);
    paramParcel.writeFloat(this.mInsertionMarkerHorizontal);
    paramParcel.writeFloat(this.mInsertionMarkerTop);
    paramParcel.writeFloat(this.mInsertionMarkerBaseline);
    paramParcel.writeFloat(this.mInsertionMarkerBottom);
    paramParcel.writeParcelable(this.mCharacterBoundsArray, paramInt);
    paramParcel.writeFloatArray(this.mMatrixValues);
  }
  
  public int hashCode() {
    return this.mHashCode;
  }
  
  private static boolean areSameFloatImpl(float paramFloat1, float paramFloat2) {
    boolean bool = Float.isNaN(paramFloat1);
    boolean bool1 = true;
    if (bool && Float.isNaN(paramFloat2))
      return true; 
    if (paramFloat1 != paramFloat2)
      bool1 = false; 
    return bool1;
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CursorAnchorInfo))
      return false; 
    paramObject = paramObject;
    if (hashCode() != paramObject.hashCode())
      return false; 
    if (this.mSelectionStart != ((CursorAnchorInfo)paramObject).mSelectionStart || this.mSelectionEnd != ((CursorAnchorInfo)paramObject).mSelectionEnd)
      return false; 
    if (this.mInsertionMarkerFlags == ((CursorAnchorInfo)paramObject).mInsertionMarkerFlags) {
      float f1 = this.mInsertionMarkerHorizontal, f2 = ((CursorAnchorInfo)paramObject).mInsertionMarkerHorizontal;
      if (areSameFloatImpl(f1, f2)) {
        f1 = this.mInsertionMarkerTop;
        f2 = ((CursorAnchorInfo)paramObject).mInsertionMarkerTop;
        if (areSameFloatImpl(f1, f2)) {
          f2 = this.mInsertionMarkerBaseline;
          f1 = ((CursorAnchorInfo)paramObject).mInsertionMarkerBaseline;
          if (areSameFloatImpl(f2, f1)) {
            f2 = this.mInsertionMarkerBottom;
            f1 = ((CursorAnchorInfo)paramObject).mInsertionMarkerBottom;
            if (areSameFloatImpl(f2, f1)) {
              if (!Objects.equals(this.mCharacterBoundsArray, ((CursorAnchorInfo)paramObject).mCharacterBoundsArray))
                return false; 
              if (this.mComposingTextStart == ((CursorAnchorInfo)paramObject).mComposingTextStart) {
                CharSequence charSequence1 = this.mComposingText, charSequence2 = ((CursorAnchorInfo)paramObject).mComposingText;
                if (Objects.equals(charSequence1, charSequence2)) {
                  if (this.mMatrixValues.length != ((CursorAnchorInfo)paramObject).mMatrixValues.length)
                    return false; 
                  byte b = 0;
                  while (true) {
                    float[] arrayOfFloat = this.mMatrixValues;
                    if (b < arrayOfFloat.length) {
                      if (arrayOfFloat[b] != ((CursorAnchorInfo)paramObject).mMatrixValues[b])
                        return false; 
                      b++;
                      continue;
                    } 
                    break;
                  } 
                  return true;
                } 
              } 
              return false;
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CursorAnchorInfo{mHashCode=");
    stringBuilder.append(this.mHashCode);
    stringBuilder.append(" mSelection=");
    stringBuilder.append(this.mSelectionStart);
    stringBuilder.append(",");
    stringBuilder.append(this.mSelectionEnd);
    stringBuilder.append(" mComposingTextStart=");
    stringBuilder.append(this.mComposingTextStart);
    stringBuilder.append(" mComposingText=");
    CharSequence charSequence = this.mComposingText;
    stringBuilder.append(Objects.toString(charSequence));
    stringBuilder.append(" mInsertionMarkerFlags=");
    stringBuilder.append(this.mInsertionMarkerFlags);
    stringBuilder.append(" mInsertionMarkerHorizontal=");
    stringBuilder.append(this.mInsertionMarkerHorizontal);
    stringBuilder.append(" mInsertionMarkerTop=");
    stringBuilder.append(this.mInsertionMarkerTop);
    stringBuilder.append(" mInsertionMarkerBaseline=");
    stringBuilder.append(this.mInsertionMarkerBaseline);
    stringBuilder.append(" mInsertionMarkerBottom=");
    stringBuilder.append(this.mInsertionMarkerBottom);
    stringBuilder.append(" mCharacterBoundsArray=");
    SparseRectFArray sparseRectFArray = this.mCharacterBoundsArray;
    stringBuilder.append(Objects.toString(sparseRectFArray));
    stringBuilder.append(" mMatrix=");
    float[] arrayOfFloat = this.mMatrixValues;
    stringBuilder.append(Arrays.toString(arrayOfFloat));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  class Builder {
    private int mSelectionStart = -1;
    
    private int mSelectionEnd = -1;
    
    private int mComposingTextStart = -1;
    
    private CharSequence mComposingText = null;
    
    private float mInsertionMarkerHorizontal = Float.NaN;
    
    private float mInsertionMarkerTop = Float.NaN;
    
    private float mInsertionMarkerBaseline = Float.NaN;
    
    private float mInsertionMarkerBottom = Float.NaN;
    
    private int mInsertionMarkerFlags = 0;
    
    private SparseRectFArray.SparseRectFArrayBuilder mCharacterBoundsArrayBuilder = null;
    
    private float[] mMatrixValues = null;
    
    private boolean mMatrixInitialized = false;
    
    public Builder setSelectionRange(int param1Int1, int param1Int2) {
      this.mSelectionStart = param1Int1;
      this.mSelectionEnd = param1Int2;
      return this;
    }
    
    public Builder setComposingText(int param1Int, CharSequence param1CharSequence) {
      this.mComposingTextStart = param1Int;
      if (param1CharSequence == null) {
        this.mComposingText = null;
      } else {
        this.mComposingText = new SpannedString(param1CharSequence);
      } 
      return this;
    }
    
    public Builder setInsertionMarkerLocation(float param1Float1, float param1Float2, float param1Float3, float param1Float4, int param1Int) {
      this.mInsertionMarkerHorizontal = param1Float1;
      this.mInsertionMarkerTop = param1Float2;
      this.mInsertionMarkerBaseline = param1Float3;
      this.mInsertionMarkerBottom = param1Float4;
      this.mInsertionMarkerFlags = param1Int;
      return this;
    }
    
    public Builder addCharacterBounds(int param1Int1, float param1Float1, float param1Float2, float param1Float3, float param1Float4, int param1Int2) {
      if (param1Int1 >= 0) {
        if (this.mCharacterBoundsArrayBuilder == null)
          this.mCharacterBoundsArrayBuilder = new SparseRectFArray.SparseRectFArrayBuilder(); 
        this.mCharacterBoundsArrayBuilder.append(param1Int1, param1Float1, param1Float2, param1Float3, param1Float4, param1Int2);
        return this;
      } 
      throw new IllegalArgumentException("index must not be a negative integer.");
    }
    
    public Builder setMatrix(Matrix param1Matrix) {
      if (this.mMatrixValues == null)
        this.mMatrixValues = new float[9]; 
      if (param1Matrix == null)
        param1Matrix = Matrix.IDENTITY_MATRIX; 
      param1Matrix.getValues(this.mMatrixValues);
      this.mMatrixInitialized = true;
      return this;
    }
    
    public CursorAnchorInfo build() {
      if (!this.mMatrixInitialized) {
        boolean bool;
        SparseRectFArray.SparseRectFArrayBuilder sparseRectFArrayBuilder = this.mCharacterBoundsArrayBuilder;
        if (sparseRectFArrayBuilder != null && 
          !sparseRectFArrayBuilder.isEmpty()) {
          bool = true;
        } else {
          bool = false;
        } 
        if (!bool) {
          float f = this.mInsertionMarkerHorizontal;
          if (Float.isNaN(f)) {
            f = this.mInsertionMarkerTop;
            if (Float.isNaN(f)) {
              f = this.mInsertionMarkerBaseline;
              if (Float.isNaN(f)) {
                f = this.mInsertionMarkerBottom;
                if (Float.isNaN(f))
                  return CursorAnchorInfo.create(this); 
              } 
            } 
          } 
        } 
        throw new IllegalArgumentException("Coordinate transformation matrix is required when positional parameters are specified.");
      } 
      return CursorAnchorInfo.create(this);
    }
    
    public void reset() {
      this.mSelectionStart = -1;
      this.mSelectionEnd = -1;
      this.mComposingTextStart = -1;
      this.mComposingText = null;
      this.mInsertionMarkerFlags = 0;
      this.mInsertionMarkerHorizontal = Float.NaN;
      this.mInsertionMarkerTop = Float.NaN;
      this.mInsertionMarkerBaseline = Float.NaN;
      this.mInsertionMarkerBottom = Float.NaN;
      this.mMatrixInitialized = false;
      SparseRectFArray.SparseRectFArrayBuilder sparseRectFArrayBuilder = this.mCharacterBoundsArrayBuilder;
      if (sparseRectFArrayBuilder != null)
        sparseRectFArrayBuilder.reset(); 
    }
  }
  
  private static CursorAnchorInfo create(Builder paramBuilder) {
    SparseRectFArray sparseRectFArray;
    if (paramBuilder.mCharacterBoundsArrayBuilder != null) {
      sparseRectFArray = paramBuilder.mCharacterBoundsArrayBuilder.build();
    } else {
      sparseRectFArray = null;
    } 
    float[] arrayOfFloat = new float[9];
    if (paramBuilder.mMatrixInitialized) {
      System.arraycopy(paramBuilder.mMatrixValues, 0, arrayOfFloat, 0, 9);
    } else {
      Matrix.IDENTITY_MATRIX.getValues(arrayOfFloat);
    } 
    int i = paramBuilder.mSelectionStart, j = paramBuilder.mSelectionEnd;
    int k = paramBuilder.mComposingTextStart;
    CharSequence charSequence = paramBuilder.mComposingText;
    int m = paramBuilder.mInsertionMarkerFlags;
    float f1 = paramBuilder.mInsertionMarkerHorizontal, f2 = paramBuilder.mInsertionMarkerTop;
    return new CursorAnchorInfo(i, j, k, charSequence, m, f1, f2, paramBuilder.mInsertionMarkerBaseline, paramBuilder.mInsertionMarkerBottom, sparseRectFArray, arrayOfFloat);
  }
  
  private CursorAnchorInfo(int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence, int paramInt4, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, SparseRectFArray paramSparseRectFArray, float[] paramArrayOffloat) {
    this.mSelectionStart = paramInt1;
    this.mSelectionEnd = paramInt2;
    this.mComposingTextStart = paramInt3;
    this.mComposingText = paramCharSequence;
    this.mInsertionMarkerFlags = paramInt4;
    this.mInsertionMarkerHorizontal = paramFloat1;
    this.mInsertionMarkerTop = paramFloat2;
    this.mInsertionMarkerBaseline = paramFloat3;
    this.mInsertionMarkerBottom = paramFloat4;
    this.mCharacterBoundsArray = paramSparseRectFArray;
    this.mMatrixValues = paramArrayOffloat;
    paramInt1 = Objects.hashCode(paramCharSequence);
    paramInt2 = Arrays.hashCode(paramArrayOffloat);
    this.mHashCode = paramInt1 * 31 + paramInt2;
  }
  
  public static CursorAnchorInfo createForAdditionalParentMatrix(CursorAnchorInfo paramCursorAnchorInfo, Matrix paramMatrix) {
    int i = paramCursorAnchorInfo.mSelectionStart, j = paramCursorAnchorInfo.mSelectionEnd, k = paramCursorAnchorInfo.mComposingTextStart;
    CharSequence charSequence = paramCursorAnchorInfo.mComposingText;
    int m = paramCursorAnchorInfo.mInsertionMarkerFlags;
    float f1 = paramCursorAnchorInfo.mInsertionMarkerHorizontal, f2 = paramCursorAnchorInfo.mInsertionMarkerTop, f3 = paramCursorAnchorInfo.mInsertionMarkerBaseline, f4 = paramCursorAnchorInfo.mInsertionMarkerBottom;
    SparseRectFArray sparseRectFArray = paramCursorAnchorInfo.mCharacterBoundsArray;
    return 



      
      new CursorAnchorInfo(i, j, k, charSequence, m, f1, f2, f3, f4, sparseRectFArray, computeMatrixValues(paramMatrix, paramCursorAnchorInfo));
  }
  
  private static float[] computeMatrixValues(Matrix paramMatrix, CursorAnchorInfo paramCursorAnchorInfo) {
    if (paramMatrix.isIdentity())
      return paramCursorAnchorInfo.mMatrixValues; 
    Matrix matrix = new Matrix();
    matrix.setValues(paramCursorAnchorInfo.mMatrixValues);
    matrix.postConcat(paramMatrix);
    float[] arrayOfFloat = new float[9];
    matrix.getValues(arrayOfFloat);
    return arrayOfFloat;
  }
  
  public int getSelectionStart() {
    return this.mSelectionStart;
  }
  
  public int getSelectionEnd() {
    return this.mSelectionEnd;
  }
  
  public int getComposingTextStart() {
    return this.mComposingTextStart;
  }
  
  public CharSequence getComposingText() {
    return this.mComposingText;
  }
  
  public int getInsertionMarkerFlags() {
    return this.mInsertionMarkerFlags;
  }
  
  public float getInsertionMarkerHorizontal() {
    return this.mInsertionMarkerHorizontal;
  }
  
  public float getInsertionMarkerTop() {
    return this.mInsertionMarkerTop;
  }
  
  public float getInsertionMarkerBaseline() {
    return this.mInsertionMarkerBaseline;
  }
  
  public float getInsertionMarkerBottom() {
    return this.mInsertionMarkerBottom;
  }
  
  public RectF getCharacterBounds(int paramInt) {
    SparseRectFArray sparseRectFArray = this.mCharacterBoundsArray;
    if (sparseRectFArray == null)
      return null; 
    return sparseRectFArray.get(paramInt);
  }
  
  public int getCharacterBoundsFlags(int paramInt) {
    SparseRectFArray sparseRectFArray = this.mCharacterBoundsArray;
    if (sparseRectFArray == null)
      return 0; 
    return sparseRectFArray.getFlags(paramInt, 0);
  }
  
  public Matrix getMatrix() {
    Matrix matrix = new Matrix();
    matrix.setValues(this.mMatrixValues);
    return matrix;
  }
  
  public static final Parcelable.Creator<CursorAnchorInfo> CREATOR = new Parcelable.Creator<CursorAnchorInfo>() {
      public CursorAnchorInfo createFromParcel(Parcel param1Parcel) {
        return new CursorAnchorInfo(param1Parcel);
      }
      
      public CursorAnchorInfo[] newArray(int param1Int) {
        return new CursorAnchorInfo[param1Int];
      }
    };
  
  public static final int FLAG_HAS_INVISIBLE_REGION = 2;
  
  public static final int FLAG_HAS_VISIBLE_REGION = 1;
  
  public static final int FLAG_IS_RTL = 4;
  
  private final SparseRectFArray mCharacterBoundsArray;
  
  private final CharSequence mComposingText;
  
  private final int mComposingTextStart;
  
  private final int mHashCode;
  
  private final float mInsertionMarkerBaseline;
  
  private final float mInsertionMarkerBottom;
  
  private final int mInsertionMarkerFlags;
  
  private final float mInsertionMarkerHorizontal;
  
  private final float mInsertionMarkerTop;
  
  private final float[] mMatrixValues;
  
  private final int mSelectionEnd;
  
  private final int mSelectionStart;
  
  public int describeContents() {
    return 0;
  }
}
