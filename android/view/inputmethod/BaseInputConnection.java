package android.view.inputmethod;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.MetaKeyKeyListener;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;

public class BaseInputConnection implements InputConnection {
  static final Object COMPOSING = new ComposingText();
  
  private static final boolean DEBUG = false;
  
  BaseInputConnection(InputMethodManager paramInputMethodManager, boolean paramBoolean) {
    this.mIMM = paramInputMethodManager;
    this.mTargetView = null;
    this.mDummyMode = paramBoolean ^ true;
  }
  
  public BaseInputConnection(View paramView, boolean paramBoolean) {
    this.mIMM = (InputMethodManager)paramView.getContext().getSystemService("input_method");
    this.mTargetView = paramView;
    this.mDummyMode = paramBoolean ^ true;
  }
  
  public static final void removeComposingSpans(Spannable paramSpannable) {
    paramSpannable.removeSpan(COMPOSING);
    Object[] arrayOfObject = paramSpannable.getSpans(0, paramSpannable.length(), Object.class);
    if (arrayOfObject != null)
      for (int i = arrayOfObject.length - 1; i >= 0; i--) {
        Object object = arrayOfObject[i];
        if ((paramSpannable.getSpanFlags(object) & 0x100) != 0)
          paramSpannable.removeSpan(object); 
      }  
  }
  
  public static void setComposingSpans(Spannable paramSpannable) {
    setComposingSpans(paramSpannable, 0, paramSpannable.length());
  }
  
  public static void setComposingSpans(Spannable paramSpannable, int paramInt1, int paramInt2) {
    Object[] arrayOfObject = paramSpannable.getSpans(paramInt1, paramInt2, Object.class);
    if (arrayOfObject != null)
      for (int i = arrayOfObject.length - 1; i >= 0; i--) {
        Object object = arrayOfObject[i];
        if (object == COMPOSING) {
          paramSpannable.removeSpan(object);
        } else {
          int j = paramSpannable.getSpanFlags(object);
          if ((j & 0x133) != 289)
            paramSpannable.setSpan(object, paramSpannable.getSpanStart(object), paramSpannable.getSpanEnd(object), j & 0xFFFFFFCC | 0x100 | 0x21); 
        } 
      }  
    paramSpannable.setSpan(COMPOSING, paramInt1, paramInt2, 289);
  }
  
  public static int getComposingSpanStart(Spannable paramSpannable) {
    return paramSpannable.getSpanStart(COMPOSING);
  }
  
  public static int getComposingSpanEnd(Spannable paramSpannable) {
    return paramSpannable.getSpanEnd(COMPOSING);
  }
  
  public Editable getEditable() {
    if (this.mEditable == null) {
      Editable editable = Editable.Factory.getInstance().newEditable("");
      Selection.setSelection(editable, 0);
    } 
    return this.mEditable;
  }
  
  public boolean beginBatchEdit() {
    return false;
  }
  
  public boolean endBatchEdit() {
    return false;
  }
  
  public void closeConnection() {
    finishComposingText();
  }
  
  public boolean clearMetaKeyStates(int paramInt) {
    Editable editable = getEditable();
    if (editable == null)
      return false; 
    MetaKeyKeyListener.clearMetaKeyState(editable, paramInt);
    return true;
  }
  
  public boolean commitCompletion(CompletionInfo paramCompletionInfo) {
    return false;
  }
  
  public boolean commitCorrection(CorrectionInfo paramCorrectionInfo) {
    return false;
  }
  
  public boolean commitText(CharSequence paramCharSequence, int paramInt) {
    replaceText(paramCharSequence, paramInt, false);
    sendCurrentText();
    return true;
  }
  
  public boolean deleteSurroundingText(int paramInt1, int paramInt2) {
    Editable editable = getEditable();
    if (editable == null)
      return false; 
    beginBatchEdit();
    int i = Selection.getSelectionStart(editable);
    int j = Selection.getSelectionEnd(editable);
    int k = i, m = j;
    if (i > j) {
      k = j;
      m = i;
    } 
    if (k == -1 || m == -1) {
      endBatchEdit();
      return false;
    } 
    int n = getComposingSpanStart(editable);
    int i1 = getComposingSpanEnd(editable);
    j = n;
    i = i1;
    if (i1 < n) {
      j = i1;
      i = n;
    } 
    int i2 = k;
    i1 = m;
    if (j != -1) {
      i2 = k;
      i1 = m;
      if (i != -1) {
        n = k;
        if (j < k)
          n = j; 
        i2 = n;
        i1 = m;
        if (i > m) {
          i1 = i;
          i2 = n;
        } 
      } 
    } 
    m = 0;
    k = m;
    if (paramInt1 > 0) {
      k = i2 - paramInt1;
      paramInt1 = k;
      if (k < 0)
        paramInt1 = 0; 
      i = i2 - paramInt1;
      k = m;
      if (i2 >= 0) {
        k = m;
        if (i > 0) {
          editable.delete(paramInt1, i2);
          k = i;
        } 
      } 
    } 
    if (paramInt2 > 0) {
      k = i1 - k;
      paramInt2 = k + paramInt2;
      paramInt1 = paramInt2;
      if (paramInt2 > editable.length())
        paramInt1 = editable.length(); 
      if (k >= 0 && paramInt1 - k > 0)
        editable.delete(k, paramInt1); 
    } 
    endBatchEdit();
    return true;
  }
  
  private static int INVALID_INDEX = -1;
  
  private static final String TAG = "BaseInputConnection";
  
  private Object[] mDefaultComposingSpans;
  
  final boolean mDummyMode;
  
  Editable mEditable;
  
  protected final InputMethodManager mIMM;
  
  KeyCharacterMap mKeyCharacterMap;
  
  final View mTargetView;
  
  private static int findIndexBackward(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    int i = paramInt1;
    boolean bool = false;
    paramInt1 = paramCharSequence.length();
    if (i < 0 || paramInt1 < i)
      return INVALID_INDEX; 
    if (paramInt2 < 0)
      return INVALID_INDEX; 
    paramInt1 = paramInt2;
    paramInt2 = bool;
    while (true) {
      if (paramInt1 == 0)
        return i; 
      i--;
      if (i < 0) {
        if (paramInt2 != 0)
          return INVALID_INDEX; 
        return 0;
      } 
      char c = paramCharSequence.charAt(i);
      if (paramInt2 != 0) {
        if (!Character.isHighSurrogate(c))
          return INVALID_INDEX; 
        paramInt2 = 0;
        paramInt1--;
        continue;
      } 
      if (!Character.isSurrogate(c)) {
        paramInt1--;
        continue;
      } 
      if (Character.isHighSurrogate(c))
        return INVALID_INDEX; 
      paramInt2 = 1;
    } 
  }
  
  private static int findIndexForward(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    boolean bool = false;
    int i = paramCharSequence.length();
    if (paramInt1 < 0 || i < paramInt1)
      return INVALID_INDEX; 
    if (paramInt2 < 0)
      return INVALID_INDEX; 
    while (true) {
      if (paramInt2 == 0)
        return paramInt1; 
      if (paramInt1 >= i) {
        if (bool)
          return INVALID_INDEX; 
        return i;
      } 
      char c = paramCharSequence.charAt(paramInt1);
      if (bool) {
        if (!Character.isLowSurrogate(c))
          return INVALID_INDEX; 
        paramInt2--;
        bool = false;
        paramInt1++;
        continue;
      } 
      if (!Character.isSurrogate(c)) {
        paramInt2--;
        paramInt1++;
        continue;
      } 
      if (Character.isLowSurrogate(c))
        return INVALID_INDEX; 
      bool = true;
      paramInt1++;
    } 
  }
  
  public boolean deleteSurroundingTextInCodePoints(int paramInt1, int paramInt2) {
    Editable editable = getEditable();
    if (editable == null)
      return false; 
    beginBatchEdit();
    int i = Selection.getSelectionStart(editable);
    int j = Selection.getSelectionEnd(editable);
    int k = i, m = j;
    if (i > j) {
      k = j;
      m = i;
    } 
    int n = getComposingSpanStart(editable);
    int i1 = getComposingSpanEnd(editable);
    j = n;
    i = i1;
    if (i1 < n) {
      j = i1;
      i = n;
    } 
    i1 = k;
    int i2 = m;
    if (j != -1) {
      i1 = k;
      i2 = m;
      if (i != -1) {
        n = k;
        if (j < k)
          n = j; 
        i1 = n;
        i2 = m;
        if (i > m) {
          i2 = i;
          i1 = n;
        } 
      } 
    } 
    if (i1 >= 0 && i2 >= 0) {
      paramInt1 = findIndexBackward(editable, i1, Math.max(paramInt1, 0));
      if (paramInt1 != INVALID_INDEX) {
        paramInt2 = findIndexForward(editable, i2, Math.max(paramInt2, 0));
        if (paramInt2 != INVALID_INDEX) {
          k = i1 - paramInt1;
          if (k > 0)
            editable.delete(paramInt1, i1); 
          if (paramInt2 - i2 > 0)
            editable.delete(i2 - k, paramInt2 - k); 
        } 
      } 
    } 
    endBatchEdit();
    return true;
  }
  
  public boolean finishComposingText() {
    Editable editable = getEditable();
    if (editable != null) {
      beginBatchEdit();
      removeComposingSpans(editable);
      sendCurrentText();
      endBatchEdit();
    } 
    return true;
  }
  
  public int getCursorCapsMode(int paramInt) {
    if (this.mDummyMode)
      return 0; 
    Editable editable = getEditable();
    if (editable == null)
      return 0; 
    int i = Selection.getSelectionStart(editable);
    int j = Selection.getSelectionEnd(editable);
    int k = i;
    if (i > j)
      k = j; 
    return TextUtils.getCapsMode(editable, k, paramInt);
  }
  
  public ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt) {
    return null;
  }
  
  public CharSequence getTextBeforeCursor(int paramInt1, int paramInt2) {
    Editable editable = getEditable();
    if (editable == null)
      return null; 
    int i = Selection.getSelectionStart(editable);
    int j = Selection.getSelectionEnd(editable);
    int k = i;
    if (i > j)
      k = j; 
    if (k <= 0)
      return ""; 
    i = paramInt1;
    if (paramInt1 > k)
      i = k; 
    if ((paramInt2 & 0x1) != 0)
      return editable.subSequence(k - i, k); 
    return TextUtils.substring(editable, k - i, k);
  }
  
  public CharSequence getSelectedText(int paramInt) {
    Editable editable = getEditable();
    if (editable == null)
      return null; 
    int i = Selection.getSelectionStart(editable);
    int j = Selection.getSelectionEnd(editable);
    int k = i, m = j;
    if (i > j) {
      k = j;
      m = i;
    } 
    if (k == m || k < 0)
      return null; 
    if ((paramInt & 0x1) != 0)
      return editable.subSequence(k, m); 
    return TextUtils.substring(editable, k, m);
  }
  
  public CharSequence getTextAfterCursor(int paramInt1, int paramInt2) {
    Editable editable = getEditable();
    if (editable == null)
      return null; 
    int i = Selection.getSelectionStart(editable);
    int j = Selection.getSelectionEnd(editable);
    int k = j;
    if (i > j)
      k = i; 
    i = k;
    if (k < 0)
      i = 0; 
    k = paramInt1;
    if (i + paramInt1 > editable.length())
      k = editable.length() - i; 
    if ((paramInt2 & 0x1) != 0)
      return editable.subSequence(i, i + k); 
    return TextUtils.substring(editable, i, i + k);
  }
  
  public boolean performEditorAction(int paramInt) {
    long l = SystemClock.uptimeMillis();
    sendKeyEvent(new KeyEvent(l, l, 0, 66, 0, 0, -1, 0, 22));
    sendKeyEvent(new KeyEvent(SystemClock.uptimeMillis(), l, 1, 66, 0, 0, -1, 0, 22));
    return true;
  }
  
  public boolean performContextMenuAction(int paramInt) {
    return false;
  }
  
  public boolean performPrivateCommand(String paramString, Bundle paramBundle) {
    return false;
  }
  
  public boolean requestCursorUpdates(int paramInt) {
    return false;
  }
  
  public Handler getHandler() {
    return null;
  }
  
  public boolean setComposingText(CharSequence paramCharSequence, int paramInt) {
    replaceText(paramCharSequence, paramInt, true);
    return true;
  }
  
  public boolean setComposingRegion(int paramInt1, int paramInt2) {
    Editable editable = getEditable();
    if (editable != null) {
      beginBatchEdit();
      removeComposingSpans(editable);
      int i = paramInt1;
      int j = paramInt2;
      paramInt2 = i;
      paramInt1 = j;
      if (i > j) {
        paramInt2 = j;
        paramInt1 = i;
      } 
      j = editable.length();
      i = paramInt2;
      if (paramInt2 < 0)
        i = 0; 
      paramInt2 = paramInt1;
      if (paramInt1 < 0)
        paramInt2 = 0; 
      paramInt1 = i;
      if (i > j)
        paramInt1 = j; 
      i = paramInt2;
      if (paramInt2 > j)
        i = j; 
      ensureDefaultComposingSpans();
      if (this.mDefaultComposingSpans != null) {
        paramInt2 = 0;
        while (true) {
          Object[] arrayOfObject = this.mDefaultComposingSpans;
          if (paramInt2 < arrayOfObject.length) {
            editable.setSpan(arrayOfObject[paramInt2], paramInt1, i, 289);
            paramInt2++;
            continue;
          } 
          break;
        } 
      } 
      editable.setSpan(COMPOSING, paramInt1, i, 289);
      sendCurrentText();
      endBatchEdit();
    } 
    return true;
  }
  
  public boolean setSelection(int paramInt1, int paramInt2) {
    Editable editable = getEditable();
    if (editable == null)
      return false; 
    int i = editable.length();
    if (paramInt1 > i || paramInt2 > i || paramInt1 < 0 || paramInt2 < 0)
      return true; 
    if (paramInt1 == paramInt2 && MetaKeyKeyListener.getMetaState(editable, 2048) != 0) {
      Selection.extendSelection(editable, paramInt1);
    } else {
      Selection.setSelection(editable, paramInt1, paramInt2);
    } 
    return true;
  }
  
  public boolean sendKeyEvent(KeyEvent paramKeyEvent) {
    this.mIMM.dispatchKeyEventFromInputMethod(this.mTargetView, paramKeyEvent);
    return false;
  }
  
  public boolean reportFullscreenMode(boolean paramBoolean) {
    return true;
  }
  
  private void sendCurrentText() {
    if (!this.mDummyMode)
      return; 
    Editable editable = getEditable();
    if (editable != null) {
      int i = editable.length();
      if (i == 0)
        return; 
      if (i == 1) {
        if (this.mKeyCharacterMap == null)
          this.mKeyCharacterMap = KeyCharacterMap.load(-1); 
        char[] arrayOfChar = new char[1];
        editable.getChars(0, 1, arrayOfChar, 0);
        KeyEvent[] arrayOfKeyEvent = this.mKeyCharacterMap.getEvents(arrayOfChar);
        if (arrayOfKeyEvent != null) {
          for (i = 0; i < arrayOfKeyEvent.length; i++)
            sendKeyEvent(arrayOfKeyEvent[i]); 
          editable.clear();
          return;
        } 
      } 
      long l = SystemClock.uptimeMillis();
      KeyEvent keyEvent = new KeyEvent(l, editable.toString(), -1, 0);
      sendKeyEvent(keyEvent);
      editable.clear();
    } 
  }
  
  private void ensureDefaultComposingSpans() {
    if (this.mDefaultComposingSpans == null) {
      View view = this.mTargetView;
      if (view != null) {
        Context context = view.getContext();
      } else if (this.mIMM.mCurRootView != null) {
        view = this.mIMM.mCurRootView.getImeFocusController().getServedView();
        if (view != null) {
          Context context = view.getContext();
        } else {
          view = null;
        } 
      } else {
        view = null;
      } 
      if (view != null) {
        Resources.Theme theme = view.getTheme();
        TypedArray typedArray = theme.obtainStyledAttributes(new int[] { 16843312 });
        CharSequence charSequence = typedArray.getText(0);
        typedArray.recycle();
        if (charSequence != null && charSequence instanceof Spanned) {
          Spanned spanned = (Spanned)charSequence;
          int i = charSequence.length();
          this.mDefaultComposingSpans = spanned.getSpans(0, i, Object.class);
        } 
      } 
    } 
  }
  
  private void replaceText(CharSequence paramCharSequence, int paramInt, boolean paramBoolean) {
    Editable editable = getEditable();
    if (editable == null)
      return; 
    if (paramCharSequence == null)
      return; 
    beginBatchEdit();
    int i = getComposingSpanStart(editable);
    int j = getComposingSpanEnd(editable);
    int k = i, m = j;
    if (j < i) {
      m = i;
      k = j;
    } 
    if (k != -1 && m != -1) {
      removeComposingSpans(editable);
    } else {
      j = Selection.getSelectionStart(editable);
      k = Selection.getSelectionEnd(editable);
      i = j;
      if (j < 0)
        i = 0; 
      j = k;
      if (k < 0)
        j = 0; 
      k = i;
      m = j;
      if (j < i) {
        m = i;
        k = j;
      } 
    } 
    CharSequence charSequence = paramCharSequence;
    if (paramBoolean) {
      CharSequence charSequence1;
      Spannable spannable;
      if (!(paramCharSequence instanceof Spannable)) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(paramCharSequence);
        charSequence = spannableStringBuilder;
        ensureDefaultComposingSpans();
        spannable = spannableStringBuilder;
        paramCharSequence = charSequence;
        if (this.mDefaultComposingSpans != null) {
          i = 0;
          while (true) {
            Object[] arrayOfObject = this.mDefaultComposingSpans;
            if (i < arrayOfObject.length) {
              spannableStringBuilder.setSpan(arrayOfObject[i], 0, spannableStringBuilder.length(), 289);
              i++;
              continue;
            } 
            spannable = spannableStringBuilder;
            charSequence1 = charSequence;
            break;
          } 
        } 
      } else {
        spannable = (Spannable)charSequence1;
      } 
      setComposingSpans(spannable);
      charSequence = charSequence1;
    } 
    if (paramInt > 0) {
      i = paramInt + m - 1;
    } else {
      i = paramInt + k;
    } 
    paramInt = i;
    if (i < 0)
      paramInt = 0; 
    i = paramInt;
    if (paramInt > editable.length())
      i = editable.length(); 
    Selection.setSelection(editable, i);
    editable.replace(k, m, charSequence);
    endBatchEdit();
  }
  
  public boolean commitContent(InputContentInfo paramInputContentInfo, int paramInt, Bundle paramBundle) {
    return false;
  }
}
