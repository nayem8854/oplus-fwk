package android.view.inputmethod;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class CorrectionInfo implements Parcelable {
  public CorrectionInfo(int paramInt, CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    this.mOffset = paramInt;
    this.mOldText = paramCharSequence1;
    this.mNewText = paramCharSequence2;
  }
  
  private CorrectionInfo(Parcel paramParcel) {
    this.mOffset = paramParcel.readInt();
    this.mOldText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mNewText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
  }
  
  public int getOffset() {
    return this.mOffset;
  }
  
  public CharSequence getOldText() {
    return this.mOldText;
  }
  
  public CharSequence getNewText() {
    return this.mNewText;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CorrectionInfo{#");
    stringBuilder.append(this.mOffset);
    stringBuilder.append(" \"");
    stringBuilder.append(this.mOldText);
    stringBuilder.append("\" -> \"");
    stringBuilder.append(this.mNewText);
    stringBuilder.append("\"}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mOffset);
    TextUtils.writeToParcel(this.mOldText, paramParcel, paramInt);
    TextUtils.writeToParcel(this.mNewText, paramParcel, paramInt);
  }
  
  public static final Parcelable.Creator<CorrectionInfo> CREATOR = new Parcelable.Creator<CorrectionInfo>() {
      public CorrectionInfo createFromParcel(Parcel param1Parcel) {
        return new CorrectionInfo(param1Parcel);
      }
      
      public CorrectionInfo[] newArray(int param1Int) {
        return new CorrectionInfo[param1Int];
      }
    };
  
  private final CharSequence mNewText;
  
  private final int mOffset;
  
  private final CharSequence mOldText;
  
  public int describeContents() {
    return 0;
  }
}
