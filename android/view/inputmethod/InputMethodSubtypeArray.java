package android.view.inputmethod;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Slog;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class InputMethodSubtypeArray {
  private static final String TAG = "InputMethodSubtypeArray";
  
  private volatile byte[] mCompressedData;
  
  private final int mCount;
  
  private volatile int mDecompressedSize;
  
  private volatile InputMethodSubtype[] mInstance;
  
  public InputMethodSubtypeArray(List<InputMethodSubtype> paramList) {
    if (paramList == null) {
      this.mCount = 0;
      return;
    } 
    int i = paramList.size();
    this.mInstance = paramList.<InputMethodSubtype>toArray(new InputMethodSubtype[i]);
  }
  
  public InputMethodSubtypeArray(Parcel paramParcel) {
    int i = paramParcel.readInt();
    if (i > 0) {
      this.mDecompressedSize = paramParcel.readInt();
      this.mCompressedData = paramParcel.createByteArray();
    } 
  }
  
  public void writeToParcel(Parcel paramParcel) {
    int i = this.mCount;
    if (i == 0) {
      paramParcel.writeInt(i);
      return;
    } 
    byte[] arrayOfByte1 = this.mCompressedData;
    int j = this.mDecompressedSize;
    byte[] arrayOfByte2 = arrayOfByte1;
    i = j;
    if (arrayOfByte1 == null) {
      arrayOfByte2 = arrayOfByte1;
      i = j;
      if (j == 0)
        synchronized (this.mLockObject) {
          arrayOfByte1 = this.mCompressedData;
          j = this.mDecompressedSize;
          arrayOfByte2 = arrayOfByte1;
          i = j;
          if (arrayOfByte1 == null) {
            arrayOfByte2 = arrayOfByte1;
            i = j;
            if (j == 0) {
              arrayOfByte1 = marshall(this.mInstance);
              arrayOfByte2 = compress(arrayOfByte1);
              if (arrayOfByte2 == null) {
                i = -1;
                Slog.i("InputMethodSubtypeArray", "Failed to compress data.");
              } else {
                i = arrayOfByte1.length;
              } 
              this.mDecompressedSize = i;
              this.mCompressedData = arrayOfByte2;
            } 
          } 
        }  
    } 
    if (arrayOfByte2 != null && i > 0) {
      paramParcel.writeInt(this.mCount);
      paramParcel.writeInt(i);
      paramParcel.writeByteArray(arrayOfByte2);
    } else {
      Slog.i("InputMethodSubtypeArray", "Unexpected state. Behaving as an empty array.");
      paramParcel.writeInt(0);
    } 
  }
  
  public InputMethodSubtype get(int paramInt) {
    if (paramInt >= 0 && this.mCount > paramInt) {
      InputMethodSubtype[] arrayOfInputMethodSubtype1 = this.mInstance;
      InputMethodSubtype[] arrayOfInputMethodSubtype2 = arrayOfInputMethodSubtype1;
      if (arrayOfInputMethodSubtype1 == null)
        synchronized (this.mLockObject) {
          arrayOfInputMethodSubtype1 = this.mInstance;
          arrayOfInputMethodSubtype2 = arrayOfInputMethodSubtype1;
          if (arrayOfInputMethodSubtype1 == null) {
            InputMethodSubtype[] arrayOfInputMethodSubtype;
            byte[] arrayOfByte = this.mCompressedData;
            int i = this.mDecompressedSize;
            arrayOfByte = decompress(arrayOfByte, i);
            this.mCompressedData = null;
            this.mDecompressedSize = 0;
            if (arrayOfByte != null) {
              arrayOfInputMethodSubtype = unmarshall(arrayOfByte);
            } else {
              Slog.e("InputMethodSubtypeArray", "Failed to decompress data. Returns null as fallback.");
              arrayOfInputMethodSubtype = new InputMethodSubtype[this.mCount];
            } 
            this.mInstance = arrayOfInputMethodSubtype;
          } 
        }  
      return arrayOfInputMethodSubtype2[paramInt];
    } 
    throw new ArrayIndexOutOfBoundsException();
  }
  
  public int getCount() {
    return this.mCount;
  }
  
  private final Object mLockObject = new Object();
  
  private static byte[] marshall(InputMethodSubtype[] paramArrayOfInputMethodSubtype) {
    Parcel parcel = null;
    try {
      Parcel parcel1 = Parcel.obtain();
      parcel = parcel1;
      parcel1.writeTypedArray((Parcelable[])paramArrayOfInputMethodSubtype, 0);
      parcel = parcel1;
      return parcel1.marshall();
    } finally {
      if (parcel != null)
        parcel.recycle(); 
    } 
  }
  
  private static InputMethodSubtype[] unmarshall(byte[] paramArrayOfbyte) {
    Parcel parcel = null;
    try {
      Parcel parcel1 = Parcel.obtain();
      parcel = parcel1;
      parcel1.unmarshall(paramArrayOfbyte, 0, paramArrayOfbyte.length);
      parcel = parcel1;
      parcel1.setDataPosition(0);
      parcel = parcel1;
      return (InputMethodSubtype[])parcel1.createTypedArray(InputMethodSubtype.CREATOR);
    } finally {
      if (parcel != null)
        parcel.recycle(); 
    } 
  }
  
  private static byte[] compress(byte[] paramArrayOfbyte) {
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this();
      try {
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream();
        this(byteArrayOutputStream);
      } finally {
        try {
          byteArrayOutputStream.close();
        } finally {
          byteArrayOutputStream = null;
        } 
      } 
    } catch (Exception exception) {
      Slog.e("InputMethodSubtypeArray", "Failed to compress the data.", exception);
      return null;
    } 
  }
  
  private static byte[] decompress(byte[] paramArrayOfbyte, int paramInt) {
    try {
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
      this(paramArrayOfbyte);
      try {
        GZIPInputStream gZIPInputStream = new GZIPInputStream();
        this(byteArrayInputStream);
      } finally {
        try {
          byteArrayInputStream.close();
        } finally {
          byteArrayInputStream = null;
        } 
      } 
    } catch (Exception exception) {
      Slog.e("InputMethodSubtypeArray", "Failed to decompress the data.", exception);
      return null;
    } 
  }
}
