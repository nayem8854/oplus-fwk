package android.view.inputmethod;

import android.app.ActivityThread;
import android.app.Application;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.os.Trace;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.style.SuggestionSpan;
import android.util.Log;
import android.util.Pools;
import android.util.PrintWriterPrinter;
import android.util.SparseArray;
import android.view.ImeFocusController;
import android.view.ImeInsetsSourceConsumer;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventSender;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewRootImpl;
import android.view.autofill.AutofillManager;
import com.android.internal.inputmethod.InputMethodDebug;
import com.android.internal.inputmethod.InputMethodPrivilegedOperations;
import com.android.internal.inputmethod.InputMethodPrivilegedOperationsRegistry;
import com.android.internal.os.SomeArgs;
import com.android.internal.view.IInputConnectionWrapper;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodManager;
import com.android.internal.view.IInputMethodSession;
import com.android.internal.view.InputBindResult;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadFactory;
import java.util.function.ToIntFunction;

public final class InputMethodManager extends OplusInputMethodManager {
  public static void ensureDefaultInstanceForDefaultDisplayIfNecessary() {
    forContextInternal(0, Looper.getMainLooper());
  }
  
  private static final Object sLock = new Object();
  
  static {
    sInstanceMap = new SparseArray<>();
  }
  
  boolean mActive = false;
  
  private boolean mRestartOnNextWindowFocus = true;
  
  Rect mTmpCursorRect = new Rect();
  
  Rect mCursorRect = new Rect();
  
  private CursorAnchorInfo mCursorAnchorInfo = null;
  
  private Matrix mActivityViewToScreenMatrix = null;
  
  int mBindSequence = -1;
  
  private int mRequestUpdateCursorAnchorInfoMonitorMode = 0;
  
  final Pools.Pool<PendingEvent> mPendingEventPool = new Pools.SimplePool<>(20);
  
  final SparseArray<PendingEvent> mPendingEvents = new SparseArray<>(20);
  
  final DelegateImpl mDelegate = new DelegateImpl();
  
  private static boolean isAutofillUIShowing(View paramView) {
    boolean bool;
    AutofillManager autofillManager = (AutofillManager)paramView.getContext().getSystemService(AutofillManager.class);
    if (autofillManager != null && autofillManager.isAutofillUiShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private InputMethodManager getFallbackInputMethodManagerIfNecessary(View paramView) {
    StringBuilder stringBuilder2;
    if (paramView == null)
      return null; 
    ViewRootImpl viewRootImpl = paramView.getViewRootImpl();
    if (viewRootImpl == null)
      return null; 
    int i = viewRootImpl.getDisplayId();
    if (i == this.mDisplayId)
      return null; 
    Context context = viewRootImpl.mContext;
    InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(InputMethodManager.class);
    if (inputMethodManager == null) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("b/117267690: Failed to get non-null fallback IMM. view=");
      stringBuilder2.append(paramView);
      Log.v("InputMethodManager", stringBuilder2.toString());
      return null;
    } 
    if (((InputMethodManager)stringBuilder2).mDisplayId != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("b/117267690: Failed to get fallback IMM with expected displayId=");
      stringBuilder.append(i);
      stringBuilder.append(" actual IMM#displayId=");
      stringBuilder.append(((InputMethodManager)stringBuilder2).mDisplayId);
      stringBuilder.append(" view=");
      stringBuilder.append(paramView);
      Log.v("InputMethodManager", stringBuilder.toString());
      return null;
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("b/117267690: Display ID mismatch found. ViewRootImpl displayId=");
    stringBuilder1.append(i);
    stringBuilder1.append(" InputMethodManager displayId=");
    stringBuilder1.append(this.mDisplayId);
    stringBuilder1.append(". Use the right InputMethodManager instance to avoid performance overhead.");
    Log.v("InputMethodManager", stringBuilder1.toString(), new Throwable());
    return (InputMethodManager)stringBuilder2;
  }
  
  private static boolean canStartInput(View paramView) {
    return (paramView.hasWindowFocus() || isAutofillUIShowing(paramView));
  }
  
  public void reportPerceptible(IBinder paramIBinder, boolean paramBoolean) {
    try {
      this.mService.reportPerceptible(paramIBinder, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private final class DelegateImpl implements ImeFocusController.InputMethodManagerDelegate {
    final InputMethodManager this$0;
    
    private DelegateImpl() {}
    
    public boolean startInput(int param1Int1, View param1View, int param1Int2, int param1Int3, int param1Int4) {
      InputMethodManager.H h;
      InputMethodManager inputMethodManager;
      synchronized (InputMethodManager.this.mH) {
        InputMethodManager inputMethodManager1 = InputMethodManager.this;
        IBinder iBinder = null;
        inputMethodManager1.mCurrentTextBoxAttribute = null;
        InputMethodManager.this.mCompletions = null;
        InputMethodManager.this.mServedConnecting = true;
        View view = InputMethodManager.this.getServedViewLocked();
        if (view != null && view.getHandler() != null)
          view.getHandler().post(new _$$Lambda$InputMethodManager$DelegateImpl$r2X8PLo_YIORJTYJGDfinf_IvK4(this, view)); 
        inputMethodManager = InputMethodManager.this;
        if (param1View != null)
          iBinder = param1View.getWindowToken(); 
        return inputMethodManager.startInputInner(param1Int1, iBinder, param1Int2, param1Int3, param1Int4);
      } 
    }
    
    public void finishInput() {
      synchronized (InputMethodManager.this.mH) {
        InputMethodManager.this.finishInputLocked();
        return;
      } 
    }
    
    public void closeCurrentIme() {
      InputMethodManager.this.closeCurrentInput();
    }
    
    public void startInputAsyncOnWindowFocusGain(View param1View, int param1Int1, int param1Int2, boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/view/inputmethod/InputMethodManager;
      //   4: astore #5
      //   6: iconst_0
      //   7: istore #6
      //   9: aload #5
      //   11: aload_1
      //   12: iconst_0
      //   13: invokestatic access$200 : (Landroid/view/inputmethod/InputMethodManager;Landroid/view/View;I)I
      //   16: istore #7
      //   18: aload_0
      //   19: getfield this$0 : Landroid/view/inputmethod/InputMethodManager;
      //   22: invokestatic access$300 : (Landroid/view/inputmethod/InputMethodManager;)Landroid/view/ImeFocusController;
      //   25: astore #8
      //   27: aload #8
      //   29: ifnonnull -> 33
      //   32: return
      //   33: aload #8
      //   35: iload #4
      //   37: iconst_0
      //   38: invokevirtual checkFocus : (ZZ)Z
      //   41: ifeq -> 58
      //   44: aload_0
      //   45: iconst_1
      //   46: aload_1
      //   47: iload #7
      //   49: iload_2
      //   50: iload_3
      //   51: invokevirtual startInput : (ILandroid/view/View;III)Z
      //   54: ifeq -> 58
      //   57: return
      //   58: aload_0
      //   59: getfield this$0 : Landroid/view/inputmethod/InputMethodManager;
      //   62: getfield mH : Landroid/view/inputmethod/InputMethodManager$H;
      //   65: astore #5
      //   67: aload #5
      //   69: monitorenter
      //   70: aload #8
      //   72: invokevirtual getServedView : ()Landroid/view/View;
      //   75: astore #8
      //   77: aload #8
      //   79: ifnull -> 114
      //   82: aload #8
      //   84: aload_1
      //   85: if_acmpne -> 114
      //   88: aload_0
      //   89: aload_1
      //   90: invokevirtual hasActiveConnection : (Landroid/view/View;)Z
      //   93: istore #4
      //   95: iload #4
      //   97: ifeq -> 114
      //   100: iconst_1
      //   101: istore #4
      //   103: goto -> 118
      //   106: astore_1
      //   107: goto -> 265
      //   110: astore_1
      //   111: goto -> 259
      //   114: iload #6
      //   116: istore #4
      //   118: getstatic android/view/inputmethod/OplusInputMethodManager.DEBUG : Z
      //   121: istore #6
      //   123: iload #6
      //   125: ifeq -> 165
      //   128: new java/lang/StringBuilder
      //   131: astore #8
      //   133: aload #8
      //   135: invokespecial <init> : ()V
      //   138: aload #8
      //   140: ldc 'Reporting focus gain, without startInput, nextFocusIsServedView='
      //   142: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   145: pop
      //   146: aload #8
      //   148: iload #4
      //   150: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   153: pop
      //   154: ldc 'InputMethodManager'
      //   156: aload #8
      //   158: invokevirtual toString : ()Ljava/lang/String;
      //   161: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   164: pop
      //   165: iload #4
      //   167: ifeq -> 176
      //   170: iconst_2
      //   171: istore #9
      //   173: goto -> 179
      //   176: iconst_3
      //   177: istore #9
      //   179: aload_0
      //   180: getfield this$0 : Landroid/view/inputmethod/InputMethodManager;
      //   183: getfield mService : Lcom/android/internal/view/IInputMethodManager;
      //   186: astore #8
      //   188: aload_0
      //   189: getfield this$0 : Landroid/view/inputmethod/InputMethodManager;
      //   192: getfield mClient : Lcom/android/internal/view/IInputMethodClient$Stub;
      //   195: astore #10
      //   197: aload_1
      //   198: invokevirtual getWindowToken : ()Landroid/os/IBinder;
      //   201: astore_1
      //   202: aload_0
      //   203: getfield this$0 : Landroid/view/inputmethod/InputMethodManager;
      //   206: getfield mCurRootView : Landroid/view/ViewRootImpl;
      //   209: getfield mContext : Landroid/content/Context;
      //   212: astore #11
      //   214: aload #11
      //   216: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
      //   219: getfield targetSdkVersion : I
      //   222: istore #12
      //   224: aload #8
      //   226: iload #9
      //   228: aload #10
      //   230: aload_1
      //   231: iload #7
      //   233: iload_2
      //   234: iload_3
      //   235: aconst_null
      //   236: aconst_null
      //   237: iconst_0
      //   238: iload #12
      //   240: invokeinterface startInputOrWindowGainedFocus : (ILcom/android/internal/view/IInputMethodClient;Landroid/os/IBinder;IIILandroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;II)Lcom/android/internal/view/InputBindResult;
      //   245: pop
      //   246: aload #5
      //   248: monitorexit
      //   249: return
      //   250: astore_1
      //   251: goto -> 259
      //   254: astore_1
      //   255: goto -> 265
      //   258: astore_1
      //   259: aload_1
      //   260: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
      //   263: athrow
      //   264: astore_1
      //   265: aload #5
      //   267: monitorexit
      //   268: aload_1
      //   269: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #620	-> 0
      //   #622	-> 18
      //   #623	-> 27
      //   #624	-> 32
      //   #626	-> 33
      //   #631	-> 44
      //   #633	-> 57
      //   #637	-> 58
      //   #641	-> 70
      //   #642	-> 77
      //   #643	-> 88
      //   #662	-> 106
      //   #659	-> 110
      //   #643	-> 114
      //   #644	-> 118
      //   #645	-> 128
      //   #649	-> 165
      //   #650	-> 176
      //   #651	-> 179
      //   #653	-> 197
      //   #658	-> 214
      //   #651	-> 224
      //   #661	-> 246
      //   #662	-> 246
      //   #663	-> 249
      //   #659	-> 250
      //   #662	-> 254
      //   #659	-> 258
      //   #660	-> 259
      //   #662	-> 264
      // Exception table:
      //   from	to	target	type
      //   70	77	258	android/os/RemoteException
      //   70	77	254	finally
      //   88	95	110	android/os/RemoteException
      //   88	95	106	finally
      //   118	123	258	android/os/RemoteException
      //   118	123	254	finally
      //   128	165	110	android/os/RemoteException
      //   128	165	106	finally
      //   179	197	258	android/os/RemoteException
      //   179	197	254	finally
      //   197	214	258	android/os/RemoteException
      //   197	214	254	finally
      //   214	224	258	android/os/RemoteException
      //   214	224	254	finally
      //   224	246	250	android/os/RemoteException
      //   224	246	264	finally
      //   246	249	264	finally
      //   259	264	264	finally
      //   265	268	264	finally
    }
    
    public void finishComposingText() {
      if (InputMethodManager.this.mServedInputConnectionWrapper != null)
        InputMethodManager.this.mServedInputConnectionWrapper.finishComposingText(); 
    }
    
    public void setCurrentRootView(ViewRootImpl param1ViewRootImpl) {
      synchronized (InputMethodManager.this.mH) {
        InputMethodManager.this.mCurRootView = param1ViewRootImpl;
        return;
      } 
    }
    
    public boolean isCurrentRootView(ViewRootImpl param1ViewRootImpl) {
      synchronized (InputMethodManager.this.mH) {
        boolean bool;
        if (InputMethodManager.this.mCurRootView == param1ViewRootImpl) {
          bool = true;
        } else {
          bool = false;
        } 
        return bool;
      } 
    }
    
    public boolean isRestartOnNextWindowFocus(boolean param1Boolean) {
      boolean bool = InputMethodManager.this.mRestartOnNextWindowFocus;
      if (param1Boolean)
        InputMethodManager.access$402(InputMethodManager.this, false); 
      return bool;
    }
    
    public boolean hasActiveConnection(View param1View) {
      synchronized (InputMethodManager.this.mH) {
        boolean bool = InputMethodManager.this.hasServedByInputMethodLocked(param1View);
        boolean bool1 = false;
        if (!bool)
          return false; 
        if (InputMethodManager.this.mServedInputConnectionWrapper != null) {
          InputMethodManager.ControlledInputConnectionWrapper controlledInputConnectionWrapper = InputMethodManager.this.mServedInputConnectionWrapper;
          if (controlledInputConnectionWrapper.isActive()) {
            controlledInputConnectionWrapper = InputMethodManager.this.mServedInputConnectionWrapper;
            if (controlledInputConnectionWrapper.mServedView.get() == param1View)
              bool1 = true; 
          } 
        } 
        return bool1;
      } 
    }
  }
  
  public DelegateImpl getDelegate() {
    return this.mDelegate;
  }
  
  private View getServedViewLocked() {
    ViewRootImpl viewRootImpl = this.mCurRootView;
    if (viewRootImpl != null) {
      View view = viewRootImpl.getImeFocusController().getServedView();
    } else {
      viewRootImpl = null;
    } 
    return (View)viewRootImpl;
  }
  
  private View getNextServedViewLocked() {
    ViewRootImpl viewRootImpl = this.mCurRootView;
    if (viewRootImpl != null) {
      View view = viewRootImpl.getImeFocusController().getNextServedView();
    } else {
      viewRootImpl = null;
    } 
    return (View)viewRootImpl;
  }
  
  private void setServedViewLocked(View paramView) {
    ViewRootImpl viewRootImpl = this.mCurRootView;
    if (viewRootImpl != null)
      viewRootImpl.getImeFocusController().setServedView(paramView); 
  }
  
  private void setNextServedViewLocked(View paramView) {
    ViewRootImpl viewRootImpl = this.mCurRootView;
    if (viewRootImpl != null)
      viewRootImpl.getImeFocusController().setNextServedView(paramView); 
  }
  
  private ImeFocusController getFocusController() {
    synchronized (this.mH) {
      if (this.mCurRootView != null)
        return this.mCurRootView.getImeFocusController(); 
      return null;
    } 
  }
  
  private boolean hasServedByInputMethodLocked(View paramView) {
    View view = getServedViewLocked();
    return (view == paramView || (view != null && 
      view.checkInputConnectionProxy(paramView)));
  }
  
  class H extends Handler {
    final InputMethodManager this$0;
    
    H(Looper param1Looper) {
      super(param1Looper, null, true);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      boolean bool = true;
      boolean bool1 = true;
      int j = 1;
      if (i != 10) {
        if (i != 15) {
          if (i != 20) {
            if (i != 30) {
              switch (i) {
                default:
                  return;
                case 7:
                  InputMethodManager.this.finishedInputEvent(param1Message.arg1, false, false);
                  return;
                case 6:
                  InputMethodManager.this.finishedInputEvent(param1Message.arg1, false, true);
                  return;
                case 5:
                  InputMethodManager.this.sendInputEventAndReportResultOnMainLooper((InputMethodManager.PendingEvent)param1Message.obj);
                  return;
                case 4:
                  if (param1Message.arg1 != 0) {
                    bool1 = true;
                  } else {
                    bool1 = false;
                  } 
                  if (param1Message.arg2 != 0) {
                    bool = true;
                  } else {
                    bool = false;
                  } 
                  if (OplusInputMethodManager.DEBUG) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("handleMessage: MSG_SET_ACTIVE ");
                    stringBuilder.append(bool1);
                    stringBuilder.append(", was ");
                    stringBuilder.append(InputMethodManager.this.mActive);
                    Log.i("InputMethodManager", stringBuilder.toString());
                  } 
                  synchronized (InputMethodManager.this.mH) {
                    InputMethodManager.this.mActive = bool1;
                    InputMethodManager.this.mFullscreenMode = bool;
                    if (!bool1) {
                      InputMethodManager.access$402(InputMethodManager.this, true);
                      try {
                        InputMethodManager.this.mIInputContext.finishComposingText();
                      } catch (RemoteException remoteException) {}
                    } 
                    View view = InputMethodManager.this.getServedViewLocked();
                    if (view != null && InputMethodManager.canStartInput(view) && InputMethodManager.this.mCurRootView != null) {
                      ImeFocusController imeFocusController = InputMethodManager.this.mCurRootView.getImeFocusController();
                      InputMethodManager inputMethodManager = InputMethodManager.this;
                      if (imeFocusController.checkFocus(inputMethodManager.mRestartOnNextWindowFocus, false)) {
                        if (bool1) {
                          j = 8;
                        } else {
                          j = 9;
                        } 
                        InputMethodManager.this.mDelegate.startInput(j, null, 0, 0, 0);
                      } 
                    } 
                    return;
                  } 
                case 3:
                  j = param1Message.arg1;
                  i = param1Message.arg2;
                  if (OplusInputMethodManager.DEBUG) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("handleMessage: MSG_UNBIND ");
                    stringBuilder.append(j);
                    stringBuilder.append(" reason=");
                    stringBuilder.append(InputMethodDebug.unbindReasonToString(i));
                    String str = stringBuilder.toString();
                    Log.i("InputMethodManager", str);
                  } 
                  synchronized (InputMethodManager.this.mH) {
                    if (InputMethodManager.this.mBindSequence != j)
                      return; 
                    InputMethodManager.this.clearBindingLocked();
                    View view = InputMethodManager.this.getServedViewLocked();
                    if (view != null && view.isFocused())
                      InputMethodManager.this.mServedConnecting = true; 
                    bool1 = InputMethodManager.this.mActive;
                    if (bool1)
                      InputMethodManager.this.startInputInner(7, (IBinder)null, 0, 0, 0); 
                    return;
                  } 
                case 2:
                  null = (InputBindResult)param1Message.obj;
                  if (OplusInputMethodManager.DEBUG) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("handleMessage: MSG_BIND ");
                    stringBuilder.append(null.sequence);
                    stringBuilder.append(",");
                    stringBuilder.append(null.id);
                    Log.i("InputMethodManager", stringBuilder.toString());
                  } 
                  synchronized (InputMethodManager.this.mH) {
                    if (InputMethodManager.this.mBindSequence < 0 || InputMethodManager.this.mBindSequence != null.sequence) {
                      StringBuilder stringBuilder = new StringBuilder();
                      this();
                      stringBuilder.append("Ignoring onBind: cur seq=");
                      stringBuilder.append(InputMethodManager.this.mBindSequence);
                      stringBuilder.append(", given seq=");
                      stringBuilder.append(null.sequence);
                      Log.w("InputMethodManager", stringBuilder.toString());
                      if (null.channel != null && null.channel != InputMethodManager.this.mCurChannel)
                        null.channel.dispose(); 
                      return;
                    } 
                    InputMethodManager.access$802(InputMethodManager.this, 0);
                    InputMethodManager.this.setInputChannelLocked(null.channel);
                    InputMethodManager.this.mCurMethod = null.method;
                    InputMethodManager.this.mCurId = null.id;
                    InputMethodManager.this.mBindSequence = null.sequence;
                    InputMethodManager.access$902(InputMethodManager.this, null.getActivityViewToScreenMatrix());
                    InputMethodManager.this.startInputInner(6, (IBinder)null, 0, 0, 0);
                    return;
                  } 
                case 1:
                  break;
              } 
              null = (SomeArgs)param1Message.obj;
              try {
                InputMethodManager.this.doDump((FileDescriptor)null.arg1, (PrintWriter)null.arg2, (String[])null.arg3);
              } catch (RuntimeException runtimeException) {
                PrintWriter printWriter = (PrintWriter)null.arg2;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Exception: ");
                stringBuilder.append(runtimeException);
                printWriter.println(stringBuilder.toString());
              } 
              synchronized (null.arg4) {
                ((CountDownLatch)null.arg4).countDown();
                null.recycle();
                return;
              } 
            } 
            null = (float[])param1Message.obj;
            i = param1Message.arg1;
            synchronized (InputMethodManager.this.mH) {
              if (InputMethodManager.this.mBindSequence != i)
                return; 
              if (null == null || InputMethodManager.this.mActivityViewToScreenMatrix == null) {
                InputMethodManager.access$902(InputMethodManager.this, (Matrix)null);
                return;
              } 
              float[] arrayOfFloat = new float[9];
              InputMethodManager.this.mActivityViewToScreenMatrix.getValues(arrayOfFloat);
              if (Arrays.equals(arrayOfFloat, null))
                return; 
              InputMethodManager.this.mActivityViewToScreenMatrix.setValues(null);
              if (InputMethodManager.this.mCursorAnchorInfo == null || InputMethodManager.this.mCurMethod == null || InputMethodManager.this.mServedInputConnectionWrapper == null)
                return; 
              if ((InputMethodManager.this.mRequestUpdateCursorAnchorInfoMonitorMode & 0x2) == 0)
                j = 0; 
              if (j == 0)
                return; 
              try {
                IInputMethodSession iInputMethodSession = InputMethodManager.this.mCurMethod;
                InputMethodManager inputMethodManager = InputMethodManager.this;
                CursorAnchorInfo cursorAnchorInfo2 = inputMethodManager.mCursorAnchorInfo;
                Matrix matrix = InputMethodManager.this.mActivityViewToScreenMatrix;
                CursorAnchorInfo cursorAnchorInfo1 = CursorAnchorInfo.createForAdditionalParentMatrix(cursorAnchorInfo2, matrix);
                iInputMethodSession.updateCursorAnchorInfo(cursorAnchorInfo1);
              } catch (RemoteException remoteException) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("IME died: ");
                stringBuilder.append(InputMethodManager.this.mCurId);
                Log.w("InputMethodManager", stringBuilder.toString(), (Throwable)remoteException);
              } 
              return;
            } 
          } 
          synchronized (InputMethodManager.this.mH) {
            if (InputMethodManager.this.mImeInsetsConsumer != null) {
              ImeInsetsSourceConsumer imeInsetsSourceConsumer = InputMethodManager.this.mImeInsetsConsumer;
              if (param1Message.arg1 != 0) {
                bool1 = bool;
              } else {
                bool1 = false;
              } 
              imeInsetsSourceConsumer.applyImeVisibility(bool1);
            } 
            return;
          } 
        } 
        synchronized (InputMethodManager.this.mH) {
          if (InputMethodManager.this.mImeInsetsConsumer != null)
            InputMethodManager.this.mImeInsetsConsumer.onPreRendered((EditorInfo)param1Message.obj); 
          return;
        } 
      } 
      if (param1Message.arg1 == 0)
        bool1 = false; 
      param1Message = null;
      synchronized (InputMethodManager.this.mH) {
        InputConnection inputConnection;
        InputMethodManager.this.mFullscreenMode = bool1;
        if (InputMethodManager.this.mServedInputConnectionWrapper != null)
          inputConnection = InputMethodManager.this.mServedInputConnectionWrapper.getInputConnection(); 
        if (inputConnection != null)
          inputConnection.reportFullscreenMode(bool1); 
        return;
      } 
    }
  }
  
  class ControlledInputConnectionWrapper extends IInputConnectionWrapper {
    private final InputMethodManager mParentInputMethodManager;
    
    private final WeakReference<View> mServedView;
    
    ControlledInputConnectionWrapper(InputMethodManager this$0, InputConnection param1InputConnection, InputMethodManager param1InputMethodManager, View param1View) {
      super((Looper)this$0, param1InputConnection);
      this.mParentInputMethodManager = param1InputMethodManager;
      this.mServedView = new WeakReference<>(param1View);
    }
    
    public boolean isActive() {
      boolean bool;
      if (this.mParentInputMethodManager.mActive && !isFinished()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void deactivate() {
      if (isFinished())
        return; 
      closeConnection();
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ControlledInputConnectionWrapper{connection=");
      stringBuilder.append(getInputConnection());
      stringBuilder.append(" finished=");
      stringBuilder.append(isFinished());
      stringBuilder.append(" mParentInputMethodManager.mActive=");
      stringBuilder.append(this.mParentInputMethodManager.mActive);
      stringBuilder.append(" mServedView=");
      WeakReference<View> weakReference = this.mServedView;
      stringBuilder.append(weakReference.get());
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class ImeThreadFactory implements ThreadFactory {
    private final String mThreadName;
    
    ImeThreadFactory(InputMethodManager this$0) {
      this.mThreadName = (String)this$0;
    }
    
    public Thread newThread(Runnable param1Runnable) {
      return new Thread(param1Runnable, this.mThreadName);
    }
  }
  
  final IInputMethodClient.Stub mClient = (IInputMethodClient.Stub)new Object(this);
  
  final InputConnection mDummyInputConnection = new BaseInputConnection(this, false);
  
  public static final int DISPATCH_HANDLED = 1;
  
  public static final int DISPATCH_IN_PROGRESS = -1;
  
  public static final int DISPATCH_NOT_HANDLED = 0;
  
  public static final int HIDE_IMPLICIT_ONLY = 1;
  
  public static final int HIDE_NOT_ALWAYS = 2;
  
  static final long INPUT_METHOD_NOT_RESPONDING_TIMEOUT = 2500L;
  
  static final int MSG_APPLY_IME_VISIBILITY = 20;
  
  static final int MSG_BIND = 2;
  
  static final int MSG_DUMP = 1;
  
  static final int MSG_FLUSH_INPUT_EVENT = 7;
  
  static final int MSG_REPORT_FULLSCREEN_MODE = 10;
  
  static final int MSG_REPORT_PRE_RENDERED = 15;
  
  static final int MSG_SEND_INPUT_EVENT = 5;
  
  static final int MSG_SET_ACTIVE = 4;
  
  static final int MSG_TIMEOUT_INPUT_EVENT = 6;
  
  static final int MSG_UNBIND = 3;
  
  static final int MSG_UPDATE_ACTIVITY_VIEW_TO_SCREEN_MATRIX = 30;
  
  private static final int NOT_A_SUBTYPE_ID = -1;
  
  static final String PENDING_EVENT_COUNTER = "aq:imm";
  
  private static final int REQUEST_UPDATE_CURSOR_ANCHOR_INFO_NONE = 0;
  
  public static final int RESULT_HIDDEN = 3;
  
  public static final int RESULT_SHOWN = 2;
  
  public static final int RESULT_UNCHANGED_HIDDEN = 1;
  
  public static final int RESULT_UNCHANGED_SHOWN = 0;
  
  public static final int SHOW_FORCED = 2;
  
  public static final int SHOW_IMPLICIT = 1;
  
  public static final int SHOW_IM_PICKER_MODE_AUTO = 0;
  
  public static final int SHOW_IM_PICKER_MODE_EXCLUDE_AUXILIARY_SUBTYPES = 2;
  
  public static final int SHOW_IM_PICKER_MODE_INCLUDE_AUXILIARY_SUBTYPES = 1;
  
  private static final String SUBTYPE_MODE_VOICE = "voice";
  
  static final String TAG = "InputMethodManager";
  
  @Deprecated
  static InputMethodManager sInstance;
  
  private static final SparseArray<InputMethodManager> sInstanceMap;
  
  CompletionInfo[] mCompletions;
  
  InputChannel mCurChannel;
  
  String mCurId;
  
  IInputMethodSession mCurMethod;
  
  ViewRootImpl mCurRootView;
  
  ImeInputEventSender mCurSender;
  
  EditorInfo mCurrentTextBoxAttribute;
  
  int mCursorCandEnd;
  
  int mCursorCandStart;
  
  int mCursorSelEnd;
  
  int mCursorSelStart;
  
  private final int mDisplayId;
  
  boolean mFullscreenMode;
  
  final H mH;
  
  final IInputContext mIInputContext;
  
  private ImeInsetsSourceConsumer mImeInsetsConsumer;
  
  final Looper mMainLooper;
  
  boolean mServedConnecting;
  
  ControlledInputConnectionWrapper mServedInputConnectionWrapper;
  
  final IInputMethodManager mService;
  
  static void tearDownEditMode() {
    if (isInEditMode())
      synchronized (sLock) {
        sInstance = null;
        return;
      }  
    throw new UnsupportedOperationException("This method must be called only from layoutlib");
  }
  
  private static boolean isInEditMode() {
    return false;
  }
  
  private static InputMethodManager createInstance(int paramInt, Looper paramLooper) {
    InputMethodManager inputMethodManager;
    if (isInEditMode()) {
      inputMethodManager = createStubInstance(paramInt, paramLooper);
    } else {
      inputMethodManager = createRealInstance(paramInt, (Looper)inputMethodManager);
    } 
    return inputMethodManager;
  }
  
  private static InputMethodManager createRealInstance(int paramInt, Looper paramLooper) {
    try {
      IBinder iBinder = ServiceManager.getServiceOrThrow("input_method");
      IInputMethodManager iInputMethodManager = IInputMethodManager.Stub.asInterface(iBinder);
      InputMethodManager inputMethodManager = new InputMethodManager(iInputMethodManager, paramInt, paramLooper);
      long l = Binder.clearCallingIdentity();
      try {
        iInputMethodManager.addClient((IInputMethodClient)inputMethodManager.mClient, inputMethodManager.mIInputContext, paramInt);
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      } finally {}
      Binder.restoreCallingIdentity(l);
      return inputMethodManager;
    } catch (android.os.ServiceManager.ServiceNotFoundException serviceNotFoundException) {
      throw new IllegalStateException(serviceNotFoundException);
    } 
  }
  
  private static InputMethodManager createStubInstance(int paramInt, Looper paramLooper) {
    ClassLoader classLoader = IInputMethodManager.class.getClassLoader();
    -$.Lambda.InputMethodManager.iDWn3IGSUFqIcs8Py42UhfrshxI iDWn3IGSUFqIcs8Py42UhfrshxI = _$$Lambda$InputMethodManager$iDWn3IGSUFqIcs8Py42UhfrshxI.INSTANCE;
    IInputMethodManager iInputMethodManager = (IInputMethodManager)Proxy.newProxyInstance(classLoader, new Class[] { IInputMethodManager.class }, (InvocationHandler)iDWn3IGSUFqIcs8Py42UhfrshxI);
    return new InputMethodManager(iInputMethodManager, paramInt, paramLooper);
  }
  
  private InputMethodManager(IInputMethodManager paramIInputMethodManager, int paramInt, Looper paramLooper) {
    this.mService = paramIInputMethodManager;
    this.mMainLooper = paramLooper;
    this.mH = new H(paramLooper);
    this.mDisplayId = paramInt;
    this.mIInputContext = (IInputContext)new ControlledInputConnectionWrapper(paramLooper, this.mDummyInputConnection, this, null);
  }
  
  public static InputMethodManager forContext(Context paramContext) {
    Looper looper;
    int i = paramContext.getDisplayId();
    if (i == 0) {
      looper = Looper.getMainLooper();
    } else {
      looper = looper.getMainLooper();
    } 
    return forContextInternal(i, looper);
  }
  
  private static InputMethodManager forContextInternal(int paramInt, Looper paramLooper) {
    boolean bool;
    if (paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    synchronized (sLock) {
      InputMethodManager inputMethodManager2 = sInstanceMap.get(paramInt);
      if (inputMethodManager2 != null)
        return inputMethodManager2; 
      InputMethodManager inputMethodManager1 = createInstance(paramInt, paramLooper);
      if (sInstance == null && bool)
        sInstance = inputMethodManager1; 
      sInstanceMap.put(paramInt, inputMethodManager1);
      return inputMethodManager1;
    } 
  }
  
  @Deprecated
  public static InputMethodManager getInstance() {
    Log.w("InputMethodManager", "InputMethodManager.getInstance() is deprecated because it cannot be compatible with multi-display. Use context.getSystemService(InputMethodManager.class) instead.", new Throwable());
    ensureDefaultInstanceForDefaultDisplayIfNecessary();
    return peekInstance();
  }
  
  @Deprecated
  public static InputMethodManager peekInstance() {
    Log.w("InputMethodManager", "InputMethodManager.peekInstance() is deprecated because it cannot be compatible with multi-display. Use context.getSystemService(InputMethodManager.class) instead.", new Throwable());
    getDebugFlag();
    synchronized (sLock) {
      return sInstance;
    } 
  }
  
  public IInputMethodClient getClient() {
    return (IInputMethodClient)this.mClient;
  }
  
  public IInputContext getInputContext() {
    return this.mIInputContext;
  }
  
  public List<InputMethodInfo> getInputMethodList() {
    try {
      return this.mService.getInputMethodList(UserHandle.myUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<InputMethodInfo> getInputMethodListAsUser(int paramInt) {
    try {
      return this.mService.getInputMethodList(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<InputMethodInfo> getEnabledInputMethodList() {
    try {
      return this.mService.getEnabledInputMethodList(UserHandle.myUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<InputMethodInfo> getEnabledInputMethodListAsUser(int paramInt) {
    try {
      return this.mService.getEnabledInputMethodList(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<InputMethodSubtype> getEnabledInputMethodSubtypeList(InputMethodInfo paramInputMethodInfo, boolean paramBoolean) {
    try {
      String str;
      IInputMethodManager iInputMethodManager = this.mService;
      if (paramInputMethodInfo == null) {
        paramInputMethodInfo = null;
      } else {
        str = paramInputMethodInfo.getId();
      } 
      return iInputMethodManager.getEnabledInputMethodSubtypeList(str, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void showStatusIcon(IBinder paramIBinder, String paramString, int paramInt) {
    InputMethodPrivilegedOperationsRegistry.get(paramIBinder).updateStatusIcon(paramString, paramInt);
  }
  
  @Deprecated
  public void hideStatusIcon(IBinder paramIBinder) {
    InputMethodPrivilegedOperationsRegistry.get(paramIBinder).updateStatusIcon(null, 0);
  }
  
  @Deprecated
  public void registerSuggestionSpansForNotification(SuggestionSpan[] paramArrayOfSuggestionSpan) {
    Log.w("InputMethodManager", "registerSuggestionSpansForNotification() is deprecated.  Does nothing.");
  }
  
  @Deprecated
  public void notifySuggestionPicked(SuggestionSpan paramSuggestionSpan, String paramString, int paramInt) {
    Log.w("InputMethodManager", "notifySuggestionPicked() is deprecated.  Does nothing.");
  }
  
  public boolean isFullscreenMode() {
    synchronized (this.mH) {
      return this.mFullscreenMode;
    } 
  }
  
  public boolean isActive(View paramView) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null)
      return inputMethodManager.isActive(paramView); 
    checkFocus();
    synchronized (this.mH) {
      boolean bool;
      if (hasServedByInputMethodLocked(paramView) && this.mCurrentTextBoxAttribute != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  public boolean isActive() {
    checkFocus();
    synchronized (this.mH) {
      boolean bool;
      if (getServedViewLocked() != null && this.mCurrentTextBoxAttribute != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  public boolean isAcceptingText() {
    boolean bool;
    checkFocus();
    ControlledInputConnectionWrapper controlledInputConnectionWrapper = this.mServedInputConnectionWrapper;
    if (controlledInputConnectionWrapper != null && 
      controlledInputConnectionWrapper.getInputConnection() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void clearBindingLocked() {
    if (DEBUG)
      Log.v("InputMethodManager", "Clearing binding!"); 
    clearConnectionLocked();
    setInputChannelLocked((InputChannel)null);
    this.mBindSequence = -1;
    this.mCurId = null;
    this.mCurMethod = null;
  }
  
  void setInputChannelLocked(InputChannel paramInputChannel) {
    if (this.mCurChannel != paramInputChannel) {
      if (this.mCurSender != null) {
        flushPendingEventsLocked();
        this.mCurSender.dispose();
        this.mCurSender = null;
      } 
      InputChannel inputChannel = this.mCurChannel;
      if (inputChannel != null)
        inputChannel.dispose(); 
      this.mCurChannel = paramInputChannel;
    } 
  }
  
  void clearConnectionLocked() {
    this.mCurrentTextBoxAttribute = null;
    ControlledInputConnectionWrapper controlledInputConnectionWrapper = this.mServedInputConnectionWrapper;
    if (controlledInputConnectionWrapper != null) {
      controlledInputConnectionWrapper.deactivate();
      this.mServedInputConnectionWrapper = null;
    } 
  }
  
  void finishInputLocked() {
    this.mActivityViewToScreenMatrix = null;
    setNextServedViewLocked((View)null);
    if (getServedViewLocked() != null) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("FINISH INPUT: mServedView=");
        stringBuilder.append(dumpViewInfo(getServedViewLocked()));
        String str = stringBuilder.toString();
        Log.v("InputMethodManager", str);
      } 
      setServedViewLocked((View)null);
      this.mCompletions = null;
      this.mServedConnecting = false;
      clearConnectionLocked();
    } 
  }
  
  public void displayCompletions(View paramView, CompletionInfo[] paramArrayOfCompletionInfo) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.displayCompletions(paramView, paramArrayOfCompletionInfo);
      return;
    } 
    checkFocus();
    synchronized (this.mH) {
      if (!hasServedByInputMethodLocked(paramView))
        return; 
      this.mCompletions = paramArrayOfCompletionInfo;
      IInputMethodSession iInputMethodSession = this.mCurMethod;
      if (iInputMethodSession != null)
        try {
          this.mCurMethod.displayCompletions(paramArrayOfCompletionInfo);
        } catch (RemoteException remoteException) {} 
      return;
    } 
  }
  
  public void updateExtractedText(View paramView, int paramInt, ExtractedText paramExtractedText) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.updateExtractedText(paramView, paramInt, paramExtractedText);
      return;
    } 
    checkFocus();
    synchronized (this.mH) {
      if (!hasServedByInputMethodLocked(paramView))
        return; 
      IInputMethodSession iInputMethodSession = this.mCurMethod;
      if (iInputMethodSession != null)
        try {
          this.mCurMethod.updateExtractedText(paramInt, paramExtractedText);
        } catch (RemoteException remoteException) {} 
      return;
    } 
  }
  
  public boolean showSoftInput(View paramView, int paramInt) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null)
      return inputMethodManager.showSoftInput(paramView, paramInt); 
    return showSoftInput(paramView, paramInt, (ResultReceiver)null);
  }
  
  public boolean showSoftInput(View paramView, int paramInt, ResultReceiver paramResultReceiver) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null)
      return inputMethodManager.showSoftInput(paramView, paramInt, paramResultReceiver); 
    checkFocus();
    synchronized (this.mH) {
      if (!hasServedByInputMethodLocked(paramView))
        return false; 
      try {
        paramInt = adjustFlag(paramInt, 2);
        setHypnusManager();
        if (paramView != null && paramView.getContext() != null && paramView.getContext().toString().contains("com.tencent.mm")) {
          (paramView.getViewRootImpl()).mIsWeixinLauncherUI = false;
          if (DEBUG_TOGGLE_SOFT)
            Log.d("InputMethodManager", " mIsWeixinLauncherUI set false"); 
        } 
        IInputMethodManager iInputMethodManager = this.mService;
        IInputMethodClient.Stub stub = this.mClient;
        IBinder iBinder = paramView.getWindowToken();
        return iInputMethodManager.showSoftInput((IInputMethodClient)stub, iBinder, paramInt, paramResultReceiver);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  @Deprecated
  public void showSoftInputUnchecked(int paramInt, ResultReceiver paramResultReceiver) {
    H h = this.mH;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
    try {
      printCallPidAndUid("showSoftInputUncheck");
      Log.w("InputMethodManager", "showSoftInputUnchecked() is a hidden method, which will be removed soon. If you are using android.support.v7.widget.SearchView, please update to version 26.0 or newer version.");
      if (this.mCurRootView == null || this.mCurRootView.getView() == null) {
        Log.w("InputMethodManager", "No current root view, ignoring showSoftInputUnchecked()");
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
        return;
      } 
      IInputMethodManager iInputMethodManager = this.mService;
      IInputMethodClient.Stub stub = this.mClient;
      ViewRootImpl viewRootImpl = this.mCurRootView;
      IBinder iBinder = viewRootImpl.getView().getWindowToken();
      iInputMethodManager.showSoftInput((IInputMethodClient)stub, iBinder, paramInt, paramResultReceiver);
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
    throw paramResultReceiver;
  }
  
  public boolean hideSoftInputFromWindow(IBinder paramIBinder, int paramInt) {
    return hideSoftInputFromWindow(paramIBinder, paramInt, (ResultReceiver)null);
  }
  
  public boolean hideSoftInputFromWindow(IBinder paramIBinder, int paramInt, ResultReceiver paramResultReceiver) {
    checkFocus();
    synchronized (this.mH) {
      View view = getServedViewLocked();
      if (view != null) {
        IBinder iBinder = view.getWindowToken();
        if (iBinder == paramIBinder)
          try {
            printCallPidAndUid("hideSoftInput FromWindow");
            if (view.getContext().toString().contains("com.tencent.mm")) {
              (view.getViewRootImpl()).mIsWeixinLauncherUI = true;
              if (DEBUG_TOGGLE_SOFT)
                Log.d("InputMethodManager", " mIsWeixinLauncherUI set true"); 
            } 
            return this.mService.hideSoftInput((IInputMethodClient)this.mClient, paramIBinder, paramInt, paramResultReceiver);
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          }  
      } 
      return false;
    } 
  }
  
  public void toggleSoftInputFromWindow(IBinder paramIBinder, int paramInt1, int paramInt2) {
    synchronized (this.mH) {
      View view = getServedViewLocked();
      if (view == null || view.getWindowToken() != paramIBinder)
        return; 
      if (this.mCurMethod != null) {
        if (DEBUG_TOGGLE_SOFT)
          printCallPidAndUid("toggleSoftInputFromWindow", paramInt1, paramInt2); 
        try {
          this.mCurMethod.toggleSoftInput(paramInt1, paramInt2);
        } catch (RemoteException remoteException) {}
      } 
      return;
    } 
  }
  
  public void toggleSoftInput(int paramInt1, int paramInt2) {
    if (this.mCurMethod != null) {
      if (DEBUG_TOGGLE_SOFT)
        printCallPidAndUid("toggleSoftInput", paramInt1, paramInt2); 
      paramInt1 = adjustFlag(paramInt1, 2);
      try {
        this.mCurMethod.toggleSoftInput(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {}
    } 
  }
  
  public void restartInput(View paramView) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.restartInput(paramView);
      return;
    } 
    checkFocus();
    synchronized (this.mH) {
      if (!hasServedByInputMethodLocked(paramView))
        return; 
      this.mServedConnecting = true;
      startInputInner(4, (IBinder)null, 0, 0, 0);
      return;
    } 
  }
  
  boolean startInputInner(int paramInt1, IBinder paramIBinder, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mH : Landroid/view/inputmethod/InputMethodManager$H;
    //   4: astore #6
    //   6: aload #6
    //   8: monitorenter
    //   9: aload_0
    //   10: invokespecial getServedViewLocked : ()Landroid/view/View;
    //   13: astore #7
    //   15: getstatic android/view/inputmethod/InputMethodManager.DEBUG : Z
    //   18: ifeq -> 85
    //   21: new java/lang/StringBuilder
    //   24: astore #8
    //   26: aload #8
    //   28: invokespecial <init> : ()V
    //   31: aload #8
    //   33: ldc_w 'Starting input: view='
    //   36: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload #8
    //   42: aload #7
    //   44: invokestatic dumpViewInfo : (Landroid/view/View;)Ljava/lang/String;
    //   47: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload #8
    //   53: ldc_w ' reason='
    //   56: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload #8
    //   62: iload_1
    //   63: invokestatic startInputReasonToString : (I)Ljava/lang/String;
    //   66: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: pop
    //   70: aload #8
    //   72: invokevirtual toString : ()Ljava/lang/String;
    //   75: astore #8
    //   77: ldc 'InputMethodManager'
    //   79: aload #8
    //   81: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   84: pop
    //   85: aload #7
    //   87: ifnonnull -> 110
    //   90: getstatic android/view/inputmethod/InputMethodManager.DEBUG : Z
    //   93: ifeq -> 105
    //   96: ldc 'InputMethodManager'
    //   98: ldc_w 'ABORT input: no served view!'
    //   101: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   104: pop
    //   105: aload #6
    //   107: monitorexit
    //   108: iconst_0
    //   109: ireturn
    //   110: aload #6
    //   112: monitorexit
    //   113: aload_2
    //   114: ifnonnull -> 177
    //   117: aload #7
    //   119: invokevirtual getWindowToken : ()Landroid/os/IBinder;
    //   122: astore #8
    //   124: aload #8
    //   126: ifnonnull -> 140
    //   129: ldc 'InputMethodManager'
    //   131: ldc_w 'ABORT input: ServedView must be attached to a Window'
    //   134: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   137: pop
    //   138: iconst_0
    //   139: ireturn
    //   140: aload_0
    //   141: aload #7
    //   143: iload_3
    //   144: invokespecial getStartInputFlags : (Landroid/view/View;I)I
    //   147: istore_3
    //   148: aload #7
    //   150: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   153: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   156: getfield softInputMode : I
    //   159: istore #5
    //   161: aload #7
    //   163: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   166: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   169: getfield flags : I
    //   172: istore #4
    //   174: goto -> 192
    //   177: iload #4
    //   179: istore #9
    //   181: iload #5
    //   183: istore #4
    //   185: iload #9
    //   187: istore #5
    //   189: aload_2
    //   190: astore #8
    //   192: aload #7
    //   194: invokevirtual getHandler : ()Landroid/os/Handler;
    //   197: astore #10
    //   199: aload #10
    //   201: ifnonnull -> 225
    //   204: getstatic android/view/inputmethod/InputMethodManager.DEBUG : Z
    //   207: ifeq -> 219
    //   210: ldc 'InputMethodManager'
    //   212: ldc_w 'ABORT input: no handler for view! Close current input.'
    //   215: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   218: pop
    //   219: aload_0
    //   220: invokevirtual closeCurrentInput : ()V
    //   223: iconst_0
    //   224: ireturn
    //   225: aload #10
    //   227: invokevirtual getLooper : ()Landroid/os/Looper;
    //   230: invokestatic myLooper : ()Landroid/os/Looper;
    //   233: if_acmpeq -> 268
    //   236: getstatic android/view/inputmethod/InputMethodManager.DEBUG : Z
    //   239: ifeq -> 251
    //   242: ldc 'InputMethodManager'
    //   244: ldc_w 'Starting input: reschedule to view thread'
    //   247: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   250: pop
    //   251: aload #10
    //   253: new android/view/inputmethod/_$$Lambda$InputMethodManager$dfnCauFoZCf_HfXs1QavrkwWDf0
    //   256: dup
    //   257: aload_0
    //   258: iload_1
    //   259: invokespecial <init> : (Landroid/view/inputmethod/InputMethodManager;I)V
    //   262: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   265: pop
    //   266: iconst_0
    //   267: ireturn
    //   268: new android/view/inputmethod/EditorInfo
    //   271: dup
    //   272: invokespecial <init> : ()V
    //   275: astore #11
    //   277: aload #11
    //   279: aload #7
    //   281: invokevirtual getContext : ()Landroid/content/Context;
    //   284: invokevirtual getOpPackageName : ()Ljava/lang/String;
    //   287: putfield packageName : Ljava/lang/String;
    //   290: aload #11
    //   292: aload #7
    //   294: invokevirtual getAutofillId : ()Landroid/view/autofill/AutofillId;
    //   297: putfield autofillId : Landroid/view/autofill/AutofillId;
    //   300: aload #11
    //   302: aload #7
    //   304: invokevirtual getId : ()I
    //   307: putfield fieldId : I
    //   310: aload #7
    //   312: aload #11
    //   314: invokevirtual onCreateInputConnection : (Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    //   317: astore #12
    //   319: getstatic android/view/inputmethod/InputMethodManager.DEBUG : Z
    //   322: ifeq -> 373
    //   325: new java/lang/StringBuilder
    //   328: dup
    //   329: invokespecial <init> : ()V
    //   332: astore_2
    //   333: aload_2
    //   334: ldc_w 'Starting input: tba='
    //   337: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: aload_2
    //   342: aload #11
    //   344: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   347: pop
    //   348: aload_2
    //   349: ldc_w ' ic='
    //   352: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   355: pop
    //   356: aload_2
    //   357: aload #12
    //   359: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   362: pop
    //   363: ldc 'InputMethodManager'
    //   365: aload_2
    //   366: invokevirtual toString : ()Ljava/lang/String;
    //   369: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   372: pop
    //   373: aload_0
    //   374: getfield mH : Landroid/view/inputmethod/InputMethodManager$H;
    //   377: astore_2
    //   378: aload_2
    //   379: monitorenter
    //   380: aload_0
    //   381: invokespecial getServedViewLocked : ()Landroid/view/View;
    //   384: astore #13
    //   386: aload #13
    //   388: aload #7
    //   390: if_acmpne -> 1247
    //   393: aload_0
    //   394: getfield mServedConnecting : Z
    //   397: ifne -> 403
    //   400: goto -> 1247
    //   403: iload_3
    //   404: istore #9
    //   406: aload_0
    //   407: getfield mCurrentTextBoxAttribute : Landroid/view/inputmethod/EditorInfo;
    //   410: ifnonnull -> 418
    //   413: iload_3
    //   414: iconst_4
    //   415: ior
    //   416: istore #9
    //   418: aload_0
    //   419: aload #11
    //   421: putfield mCurrentTextBoxAttribute : Landroid/view/inputmethod/EditorInfo;
    //   424: aload_0
    //   425: aload #11
    //   427: invokespecial maybeCallServedViewChangedLocked : (Landroid/view/inputmethod/EditorInfo;)V
    //   430: aload_0
    //   431: iconst_0
    //   432: putfield mServedConnecting : Z
    //   435: aload_0
    //   436: getfield mServedInputConnectionWrapper : Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
    //   439: astore #6
    //   441: aload #6
    //   443: ifnull -> 466
    //   446: aload_0
    //   447: getfield mServedInputConnectionWrapper : Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
    //   450: invokevirtual deactivate : ()V
    //   453: aload_0
    //   454: aconst_null
    //   455: putfield mServedInputConnectionWrapper : Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
    //   458: goto -> 466
    //   461: astore #8
    //   463: goto -> 1379
    //   466: aload #12
    //   468: ifnull -> 581
    //   471: aload_0
    //   472: aload #11
    //   474: getfield initialSelStart : I
    //   477: putfield mCursorSelStart : I
    //   480: aload_0
    //   481: aload #11
    //   483: getfield initialSelEnd : I
    //   486: putfield mCursorSelEnd : I
    //   489: aload_0
    //   490: iconst_m1
    //   491: putfield mCursorCandStart : I
    //   494: aload_0
    //   495: iconst_m1
    //   496: putfield mCursorCandEnd : I
    //   499: aload_0
    //   500: getfield mCursorRect : Landroid/graphics/Rect;
    //   503: invokevirtual setEmpty : ()V
    //   506: aload_0
    //   507: aconst_null
    //   508: putfield mCursorAnchorInfo : Landroid/view/inputmethod/CursorAnchorInfo;
    //   511: aload #12
    //   513: invokestatic getMissingMethodFlags : (Landroid/view/inputmethod/InputConnection;)I
    //   516: istore_3
    //   517: iload_3
    //   518: bipush #32
    //   520: iand
    //   521: ifeq -> 530
    //   524: aconst_null
    //   525: astore #6
    //   527: goto -> 539
    //   530: aload #12
    //   532: invokeinterface getHandler : ()Landroid/os/Handler;
    //   537: astore #6
    //   539: new android/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper
    //   542: astore #13
    //   544: aload #6
    //   546: ifnull -> 559
    //   549: aload #6
    //   551: invokevirtual getLooper : ()Landroid/os/Looper;
    //   554: astore #6
    //   556: goto -> 566
    //   559: aload #10
    //   561: invokevirtual getLooper : ()Landroid/os/Looper;
    //   564: astore #6
    //   566: aload #13
    //   568: aload #6
    //   570: aload #12
    //   572: aload_0
    //   573: aload #7
    //   575: invokespecial <init> : (Landroid/os/Looper;Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/InputMethodManager;Landroid/view/View;)V
    //   578: goto -> 586
    //   581: iconst_0
    //   582: istore_3
    //   583: aconst_null
    //   584: astore #13
    //   586: aload_0
    //   587: aload #13
    //   589: putfield mServedInputConnectionWrapper : Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
    //   592: getstatic android/view/inputmethod/InputMethodManager.DEBUG : Z
    //   595: istore #14
    //   597: iload #14
    //   599: ifeq -> 709
    //   602: new java/lang/StringBuilder
    //   605: astore #6
    //   607: aload #6
    //   609: invokespecial <init> : ()V
    //   612: aload #6
    //   614: ldc_w 'START INPUT: view='
    //   617: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   620: pop
    //   621: aload #6
    //   623: aload #7
    //   625: invokestatic dumpViewInfo : (Landroid/view/View;)Ljava/lang/String;
    //   628: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   631: pop
    //   632: aload #6
    //   634: ldc_w ' ic='
    //   637: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   640: pop
    //   641: aload #6
    //   643: aload #12
    //   645: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   648: pop
    //   649: aload #6
    //   651: ldc_w ' tba='
    //   654: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   657: pop
    //   658: aload #6
    //   660: aload #11
    //   662: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   665: pop
    //   666: aload #6
    //   668: ldc_w ' startInputFlags='
    //   671: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   674: pop
    //   675: aload #6
    //   677: iload #9
    //   679: invokestatic startInputFlagsToString : (I)Ljava/lang/String;
    //   682: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   685: pop
    //   686: aload #6
    //   688: invokevirtual toString : ()Ljava/lang/String;
    //   691: astore #6
    //   693: ldc 'InputMethodManager'
    //   695: aload #6
    //   697: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   700: pop
    //   701: goto -> 709
    //   704: astore #8
    //   706: goto -> 1175
    //   709: aload_0
    //   710: getfield mService : Lcom/android/internal/view/IInputMethodManager;
    //   713: astore #12
    //   715: aload_0
    //   716: getfield mClient : Lcom/android/internal/view/IInputMethodClient$Stub;
    //   719: astore #10
    //   721: aload #7
    //   723: invokevirtual getContext : ()Landroid/content/Context;
    //   726: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   729: getfield targetSdkVersion : I
    //   732: istore #15
    //   734: aload_2
    //   735: astore #7
    //   737: aload #7
    //   739: astore #6
    //   741: aload #12
    //   743: iload_1
    //   744: aload #10
    //   746: aload #8
    //   748: iload #9
    //   750: iload #5
    //   752: iload #4
    //   754: aload #11
    //   756: aload #13
    //   758: iload_3
    //   759: iload #15
    //   761: invokeinterface startInputOrWindowGainedFocus : (ILcom/android/internal/view/IInputMethodClient;Landroid/os/IBinder;IIILandroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;II)Lcom/android/internal/view/InputBindResult;
    //   766: astore #13
    //   768: aload #7
    //   770: astore #6
    //   772: getstatic android/view/inputmethod/InputMethodManager.DEBUG : Z
    //   775: ifeq -> 836
    //   778: aload #7
    //   780: astore #6
    //   782: new java/lang/StringBuilder
    //   785: astore #8
    //   787: aload #7
    //   789: astore #6
    //   791: aload #8
    //   793: invokespecial <init> : ()V
    //   796: aload #7
    //   798: astore #6
    //   800: aload #8
    //   802: ldc_w 'Starting input: Bind result='
    //   805: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   808: pop
    //   809: aload #7
    //   811: astore #6
    //   813: aload #8
    //   815: aload #13
    //   817: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   820: pop
    //   821: aload #7
    //   823: astore #6
    //   825: ldc 'InputMethodManager'
    //   827: aload #8
    //   829: invokevirtual toString : ()Ljava/lang/String;
    //   832: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   835: pop
    //   836: aload #13
    //   838: ifnonnull -> 971
    //   841: aload #7
    //   843: astore #6
    //   845: new java/lang/StringBuilder
    //   848: astore #8
    //   850: aload #7
    //   852: astore #6
    //   854: aload #8
    //   856: invokespecial <init> : ()V
    //   859: aload #7
    //   861: astore #6
    //   863: aload #8
    //   865: ldc_w 'startInputOrWindowGainedFocus must not return null. startInputReason='
    //   868: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   871: pop
    //   872: aload #7
    //   874: astore #6
    //   876: aload #8
    //   878: iload_1
    //   879: invokestatic startInputReasonToString : (I)Ljava/lang/String;
    //   882: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   885: pop
    //   886: aload #7
    //   888: astore #6
    //   890: aload #8
    //   892: ldc_w ' editorInfo='
    //   895: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   898: pop
    //   899: aload #7
    //   901: astore #6
    //   903: aload #8
    //   905: aload #11
    //   907: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   910: pop
    //   911: aload #7
    //   913: astore #6
    //   915: aload #8
    //   917: ldc_w ' startInputFlags='
    //   920: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   923: pop
    //   924: aload #7
    //   926: astore #6
    //   928: aload #8
    //   930: iload #9
    //   932: invokestatic startInputFlagsToString : (I)Ljava/lang/String;
    //   935: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   938: pop
    //   939: aload #7
    //   941: astore #6
    //   943: aload #8
    //   945: invokevirtual toString : ()Ljava/lang/String;
    //   948: astore #8
    //   950: aload #7
    //   952: astore #6
    //   954: ldc 'InputMethodManager'
    //   956: aload #8
    //   958: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   961: pop
    //   962: aload #7
    //   964: astore #6
    //   966: aload #7
    //   968: monitorexit
    //   969: iconst_0
    //   970: ireturn
    //   971: aload #7
    //   973: astore #6
    //   975: aload_0
    //   976: aload #13
    //   978: invokevirtual getActivityViewToScreenMatrix : ()Landroid/graphics/Matrix;
    //   981: putfield mActivityViewToScreenMatrix : Landroid/graphics/Matrix;
    //   984: aload #7
    //   986: astore #6
    //   988: aload #13
    //   990: getfield id : Ljava/lang/String;
    //   993: ifnull -> 1051
    //   996: aload #7
    //   998: astore #6
    //   1000: aload_0
    //   1001: aload #13
    //   1003: getfield channel : Landroid/view/InputChannel;
    //   1006: invokevirtual setInputChannelLocked : (Landroid/view/InputChannel;)V
    //   1009: aload #7
    //   1011: astore #6
    //   1013: aload_0
    //   1014: aload #13
    //   1016: getfield sequence : I
    //   1019: putfield mBindSequence : I
    //   1022: aload #7
    //   1024: astore #6
    //   1026: aload_0
    //   1027: aload #13
    //   1029: getfield method : Lcom/android/internal/view/IInputMethodSession;
    //   1032: putfield mCurMethod : Lcom/android/internal/view/IInputMethodSession;
    //   1035: aload #7
    //   1037: astore #6
    //   1039: aload_0
    //   1040: aload #13
    //   1042: getfield id : Ljava/lang/String;
    //   1045: putfield mCurId : Ljava/lang/String;
    //   1048: goto -> 1091
    //   1051: aload #7
    //   1053: astore #6
    //   1055: aload #13
    //   1057: getfield channel : Landroid/view/InputChannel;
    //   1060: ifnull -> 1091
    //   1063: aload #7
    //   1065: astore #6
    //   1067: aload #13
    //   1069: getfield channel : Landroid/view/InputChannel;
    //   1072: aload_0
    //   1073: getfield mCurChannel : Landroid/view/InputChannel;
    //   1076: if_acmpeq -> 1091
    //   1079: aload #7
    //   1081: astore #6
    //   1083: aload #13
    //   1085: getfield channel : Landroid/view/InputChannel;
    //   1088: invokevirtual dispose : ()V
    //   1091: aload #7
    //   1093: astore #6
    //   1095: aload #13
    //   1097: getfield result : I
    //   1100: bipush #12
    //   1102: if_icmpeq -> 1108
    //   1105: goto -> 1117
    //   1108: aload #7
    //   1110: astore #6
    //   1112: aload_0
    //   1113: iconst_1
    //   1114: putfield mRestartOnNextWindowFocus : Z
    //   1117: aload #7
    //   1119: astore #6
    //   1121: aload_0
    //   1122: getfield mCurMethod : Lcom/android/internal/view/IInputMethodSession;
    //   1125: ifnull -> 1165
    //   1128: aload #7
    //   1130: astore #6
    //   1132: aload_0
    //   1133: getfield mCompletions : [Landroid/view/inputmethod/CompletionInfo;
    //   1136: astore #8
    //   1138: aload #8
    //   1140: ifnull -> 1165
    //   1143: aload #7
    //   1145: astore #6
    //   1147: aload_0
    //   1148: getfield mCurMethod : Lcom/android/internal/view/IInputMethodSession;
    //   1151: aload_0
    //   1152: getfield mCompletions : [Landroid/view/inputmethod/CompletionInfo;
    //   1155: invokeinterface displayCompletions : ([Landroid/view/inputmethod/CompletionInfo;)V
    //   1160: goto -> 1165
    //   1163: astore #6
    //   1165: goto -> 1240
    //   1168: astore #8
    //   1170: goto -> 1175
    //   1173: astore #8
    //   1175: aload_2
    //   1176: astore #7
    //   1178: aload #7
    //   1180: astore #6
    //   1182: new java/lang/StringBuilder
    //   1185: astore #13
    //   1187: aload #7
    //   1189: astore #6
    //   1191: aload #13
    //   1193: invokespecial <init> : ()V
    //   1196: aload #7
    //   1198: astore #6
    //   1200: aload #13
    //   1202: ldc_w 'IME died: '
    //   1205: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1208: pop
    //   1209: aload #7
    //   1211: astore #6
    //   1213: aload #13
    //   1215: aload_0
    //   1216: getfield mCurId : Ljava/lang/String;
    //   1219: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1222: pop
    //   1223: aload #7
    //   1225: astore #6
    //   1227: ldc 'InputMethodManager'
    //   1229: aload #13
    //   1231: invokevirtual toString : ()Ljava/lang/String;
    //   1234: aload #8
    //   1236: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1239: pop
    //   1240: aload_2
    //   1241: astore #6
    //   1243: aload_2
    //   1244: monitorexit
    //   1245: iconst_1
    //   1246: ireturn
    //   1247: aload_2
    //   1248: astore #6
    //   1250: getstatic android/view/inputmethod/InputMethodManager.DEBUG : Z
    //   1253: ifeq -> 1370
    //   1256: aload_2
    //   1257: astore #6
    //   1259: new java/lang/StringBuilder
    //   1262: astore #8
    //   1264: aload_2
    //   1265: astore #6
    //   1267: aload #8
    //   1269: invokespecial <init> : ()V
    //   1272: aload_2
    //   1273: astore #6
    //   1275: aload #8
    //   1277: ldc_w 'Starting input: finished by someone else. view='
    //   1280: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1283: pop
    //   1284: aload_2
    //   1285: astore #6
    //   1287: aload #8
    //   1289: aload #7
    //   1291: invokestatic dumpViewInfo : (Landroid/view/View;)Ljava/lang/String;
    //   1294: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1297: pop
    //   1298: aload_2
    //   1299: astore #6
    //   1301: aload #8
    //   1303: ldc_w ' servedView='
    //   1306: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1309: pop
    //   1310: aload_2
    //   1311: astore #6
    //   1313: aload #8
    //   1315: aload #13
    //   1317: invokestatic dumpViewInfo : (Landroid/view/View;)Ljava/lang/String;
    //   1320: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1323: pop
    //   1324: aload_2
    //   1325: astore #6
    //   1327: aload #8
    //   1329: ldc_w ' mServedConnecting='
    //   1332: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1335: pop
    //   1336: aload_2
    //   1337: astore #6
    //   1339: aload #8
    //   1341: aload_0
    //   1342: getfield mServedConnecting : Z
    //   1345: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   1348: pop
    //   1349: aload_2
    //   1350: astore #6
    //   1352: aload #8
    //   1354: invokevirtual toString : ()Ljava/lang/String;
    //   1357: astore #8
    //   1359: aload_2
    //   1360: astore #6
    //   1362: ldc 'InputMethodManager'
    //   1364: aload #8
    //   1366: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   1369: pop
    //   1370: aload_2
    //   1371: astore #6
    //   1373: aload_2
    //   1374: monitorexit
    //   1375: iconst_0
    //   1376: ireturn
    //   1377: astore #8
    //   1379: aload_2
    //   1380: astore #6
    //   1382: aload_2
    //   1383: monitorexit
    //   1384: aload #8
    //   1386: athrow
    //   1387: astore #8
    //   1389: aload #6
    //   1391: astore_2
    //   1392: goto -> 1379
    //   1395: astore_2
    //   1396: aload #6
    //   1398: monitorexit
    //   1399: aload_2
    //   1400: athrow
    //   1401: astore_2
    //   1402: goto -> 1396
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1909	-> 0
    //   #1910	-> 9
    //   #1913	-> 15
    //   #1914	-> 21
    //   #1915	-> 60
    //   #1914	-> 77
    //   #1917	-> 85
    //   #1918	-> 90
    //   #1919	-> 105
    //   #1921	-> 110
    //   #1923	-> 113
    //   #1924	-> 117
    //   #1925	-> 124
    //   #1926	-> 129
    //   #1927	-> 138
    //   #1929	-> 140
    //   #1930	-> 148
    //   #1931	-> 161
    //   #1923	-> 177
    //   #1938	-> 192
    //   #1939	-> 199
    //   #1944	-> 204
    //   #1945	-> 219
    //   #1946	-> 223
    //   #1948	-> 225
    //   #1951	-> 236
    //   #1952	-> 251
    //   #1953	-> 266
    //   #1959	-> 268
    //   #1963	-> 277
    //   #1964	-> 290
    //   #1965	-> 300
    //   #1966	-> 310
    //   #1967	-> 319
    //   #1969	-> 373
    //   #1972	-> 380
    //   #1973	-> 386
    //   #1984	-> 403
    //   #1985	-> 413
    //   #1989	-> 418
    //   #1990	-> 424
    //   #1991	-> 430
    //   #1992	-> 435
    //   #1993	-> 446
    //   #1994	-> 453
    //   #2063	-> 461
    //   #1998	-> 466
    //   #1999	-> 471
    //   #2000	-> 480
    //   #2001	-> 489
    //   #2002	-> 494
    //   #2003	-> 499
    //   #2004	-> 506
    //   #2006	-> 511
    //   #2007	-> 517
    //   #2010	-> 524
    //   #2012	-> 530
    //   #2014	-> 539
    //   #2015	-> 544
    //   #2016	-> 578
    //   #2017	-> 581
    //   #2018	-> 581
    //   #2020	-> 586
    //   #2023	-> 592
    //   #2025	-> 675
    //   #2023	-> 693
    //   #2060	-> 704
    //   #2026	-> 709
    //   #2029	-> 721
    //   #2026	-> 734
    //   #2030	-> 768
    //   #2031	-> 836
    //   #2032	-> 841
    //   #2034	-> 872
    //   #2037	-> 924
    //   #2032	-> 950
    //   #2038	-> 962
    //   #2040	-> 971
    //   #2041	-> 984
    //   #2042	-> 996
    //   #2043	-> 1009
    //   #2044	-> 1022
    //   #2045	-> 1035
    //   #2046	-> 1051
    //   #2047	-> 1079
    //   #2049	-> 1091
    //   #2051	-> 1108
    //   #2054	-> 1117
    //   #2056	-> 1143
    //   #2058	-> 1160
    //   #2057	-> 1163
    //   #2062	-> 1165
    //   #2060	-> 1168
    //   #2061	-> 1175
    //   #2063	-> 1240
    //   #2065	-> 1245
    //   #1973	-> 1247
    //   #1975	-> 1247
    //   #1976	-> 1284
    //   #1977	-> 1310
    //   #1975	-> 1359
    //   #1979	-> 1370
    //   #2063	-> 1377
    //   #1921	-> 1395
    // Exception table:
    //   from	to	target	type
    //   9	15	1395	finally
    //   15	21	1395	finally
    //   21	60	1395	finally
    //   60	77	1395	finally
    //   77	85	1395	finally
    //   90	105	1395	finally
    //   105	108	1395	finally
    //   110	113	1395	finally
    //   380	386	1377	finally
    //   393	400	1377	finally
    //   406	413	1377	finally
    //   418	424	1377	finally
    //   424	430	1377	finally
    //   430	435	1377	finally
    //   435	441	1377	finally
    //   446	453	461	finally
    //   453	458	461	finally
    //   471	480	461	finally
    //   480	489	461	finally
    //   489	494	461	finally
    //   494	499	461	finally
    //   499	506	461	finally
    //   506	511	461	finally
    //   511	517	461	finally
    //   530	539	461	finally
    //   539	544	461	finally
    //   549	556	461	finally
    //   559	566	461	finally
    //   566	578	461	finally
    //   586	592	1377	finally
    //   592	597	1173	android/os/RemoteException
    //   592	597	1377	finally
    //   602	675	704	android/os/RemoteException
    //   602	675	461	finally
    //   675	693	704	android/os/RemoteException
    //   675	693	461	finally
    //   693	701	704	android/os/RemoteException
    //   693	701	461	finally
    //   709	721	1173	android/os/RemoteException
    //   709	721	1377	finally
    //   721	734	1173	android/os/RemoteException
    //   721	734	1377	finally
    //   741	768	1168	android/os/RemoteException
    //   741	768	1387	finally
    //   772	778	1168	android/os/RemoteException
    //   772	778	1387	finally
    //   782	787	1168	android/os/RemoteException
    //   782	787	1387	finally
    //   791	796	1168	android/os/RemoteException
    //   791	796	1387	finally
    //   800	809	1168	android/os/RemoteException
    //   800	809	1387	finally
    //   813	821	1168	android/os/RemoteException
    //   813	821	1387	finally
    //   825	836	1168	android/os/RemoteException
    //   825	836	1387	finally
    //   845	850	1168	android/os/RemoteException
    //   845	850	1387	finally
    //   854	859	1168	android/os/RemoteException
    //   854	859	1387	finally
    //   863	872	1168	android/os/RemoteException
    //   863	872	1387	finally
    //   876	886	1168	android/os/RemoteException
    //   876	886	1387	finally
    //   890	899	1168	android/os/RemoteException
    //   890	899	1387	finally
    //   903	911	1168	android/os/RemoteException
    //   903	911	1387	finally
    //   915	924	1168	android/os/RemoteException
    //   915	924	1387	finally
    //   928	939	1168	android/os/RemoteException
    //   928	939	1387	finally
    //   943	950	1168	android/os/RemoteException
    //   943	950	1387	finally
    //   954	962	1168	android/os/RemoteException
    //   954	962	1387	finally
    //   966	969	1387	finally
    //   975	984	1168	android/os/RemoteException
    //   975	984	1387	finally
    //   988	996	1168	android/os/RemoteException
    //   988	996	1387	finally
    //   1000	1009	1168	android/os/RemoteException
    //   1000	1009	1387	finally
    //   1013	1022	1168	android/os/RemoteException
    //   1013	1022	1387	finally
    //   1026	1035	1168	android/os/RemoteException
    //   1026	1035	1387	finally
    //   1039	1048	1168	android/os/RemoteException
    //   1039	1048	1387	finally
    //   1055	1063	1168	android/os/RemoteException
    //   1055	1063	1387	finally
    //   1067	1079	1168	android/os/RemoteException
    //   1067	1079	1387	finally
    //   1083	1091	1168	android/os/RemoteException
    //   1083	1091	1387	finally
    //   1095	1105	1168	android/os/RemoteException
    //   1095	1105	1387	finally
    //   1112	1117	1168	android/os/RemoteException
    //   1112	1117	1387	finally
    //   1121	1128	1168	android/os/RemoteException
    //   1121	1128	1387	finally
    //   1132	1138	1168	android/os/RemoteException
    //   1132	1138	1387	finally
    //   1147	1160	1163	android/os/RemoteException
    //   1147	1160	1387	finally
    //   1182	1187	1387	finally
    //   1191	1196	1387	finally
    //   1200	1209	1387	finally
    //   1213	1223	1387	finally
    //   1227	1240	1387	finally
    //   1243	1245	1387	finally
    //   1250	1256	1387	finally
    //   1259	1264	1387	finally
    //   1267	1272	1387	finally
    //   1275	1284	1387	finally
    //   1287	1298	1387	finally
    //   1301	1310	1387	finally
    //   1313	1324	1387	finally
    //   1327	1336	1387	finally
    //   1339	1349	1387	finally
    //   1352	1359	1387	finally
    //   1362	1370	1387	finally
    //   1373	1375	1387	finally
    //   1382	1384	1387	finally
    //   1396	1399	1401	finally
  }
  
  @Deprecated
  public void windowDismissed(IBinder paramIBinder) {}
  
  private int getStartInputFlags(View paramView, int paramInt) {
    int i = paramInt | 0x1;
    paramInt = i;
    if (paramView.onCheckIsTextEditor())
      paramInt = i | 0x2; 
    return paramInt;
  }
  
  public void checkFocus() {
    ImeFocusController imeFocusController = getFocusController();
    if (imeFocusController != null)
      imeFocusController.checkFocus(false, true); 
  }
  
  void closeCurrentInput() {
    synchronized (this.mH) {
      if (this.mCurRootView != null) {
        View view = this.mCurRootView.getView();
        if (view != null)
          try {
            printCallPidAndUid("closeCurrentInput hideSoftInput");
            IInputMethodManager iInputMethodManager = this.mService;
            IInputMethodClient.Stub stub = this.mClient;
            ViewRootImpl viewRootImpl = this.mCurRootView;
            IBinder iBinder = viewRootImpl.getView().getWindowToken();
            iInputMethodManager.hideSoftInput((IInputMethodClient)stub, iBinder, 2, null);
            return;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          }  
      } 
      Log.w("InputMethodManager", "No current root view, ignoring closeCurrentInput()");
      return;
    } 
  }
  
  public void registerImeConsumer(ImeInsetsSourceConsumer paramImeInsetsSourceConsumer) {
    if (paramImeInsetsSourceConsumer != null)
      synchronized (this.mH) {
        this.mImeInsetsConsumer = paramImeInsetsSourceConsumer;
        return;
      }  
    throw new IllegalStateException("ImeInsetsSourceConsumer cannot be null.");
  }
  
  public void unregisterImeConsumer(ImeInsetsSourceConsumer paramImeInsetsSourceConsumer) {
    if (paramImeInsetsSourceConsumer != null)
      synchronized (this.mH) {
        if (this.mImeInsetsConsumer == paramImeInsetsSourceConsumer)
          this.mImeInsetsConsumer = null; 
        return;
      }  
    throw new IllegalStateException("ImeInsetsSourceConsumer cannot be null.");
  }
  
  public boolean requestImeShow(IBinder paramIBinder) {
    synchronized (this.mH) {
      View view = getServedViewLocked();
      if (view == null || view.getWindowToken() != paramIBinder)
        return false; 
      showSoftInput(view, 0, (ResultReceiver)null);
      return true;
    } 
  }
  
  public void notifyImeHidden(IBinder paramIBinder) {
    H h = this.mH;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
    try {
      if (this.mCurMethod != null && this.mCurRootView != null) {
        ViewRootImpl viewRootImpl = this.mCurRootView;
        if (viewRootImpl.getWindowToken() == paramIBinder)
          this.mCurMethod.notifyImeHidden(); 
      } 
    } catch (RemoteException remoteException) {
    
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
  }
  
  public void removeImeSurface(IBinder paramIBinder) {
    H h = this.mH;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
    try {
      this.mService.removeImeSurfaceFromWindow(paramIBinder);
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
    throw paramIBinder;
  }
  
  public void updateSelection(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.updateSelection(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    } 
    checkFocus();
    synchronized (this.mH) {
      if (!hasServedByInputMethodLocked(paramView) || this.mCurrentTextBoxAttribute == null || this.mCurMethod == null)
        return; 
      if (this.mCursorSelStart != paramInt1 || this.mCursorSelEnd != paramInt2 || this.mCursorCandStart != paramInt3 || this.mCursorCandEnd != paramInt4) {
        if (DEBUG)
          Log.d("InputMethodManager", "updateSelection"); 
        try {
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("SELECTION CHANGE: ");
            stringBuilder.append(this.mCurMethod);
            Log.v("InputMethodManager", stringBuilder.toString());
          } 
          int i = this.mCursorSelStart;
          int j = this.mCursorSelEnd;
          this.mCursorSelStart = paramInt1;
          this.mCursorSelEnd = paramInt2;
          this.mCursorCandStart = paramInt3;
          this.mCursorCandEnd = paramInt4;
          this.mCurMethod.updateSelection(i, j, paramInt1, paramInt2, paramInt3, paramInt4);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("IME died: ");
          stringBuilder.append(this.mCurId);
          Log.w("InputMethodManager", stringBuilder.toString(), (Throwable)remoteException);
        } 
      } 
      return;
    } 
  }
  
  @Deprecated
  public void viewClicked(View paramView) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.viewClicked(paramView);
      return;
    } 
    synchronized (this.mH) {
      boolean bool;
      View view1 = getServedViewLocked();
      View view2 = getNextServedViewLocked();
      if (view1 != view2) {
        bool = true;
      } else {
        bool = false;
      } 
      checkFocus();
      synchronized (this.mH) {
        if (hasServedByInputMethodLocked(paramView) && this.mCurrentTextBoxAttribute != null) {
          IInputMethodSession iInputMethodSession = this.mCurMethod;
          if (iInputMethodSession != null) {
            try {
              if (DEBUG) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("onViewClicked: ");
                stringBuilder.append(bool);
                Log.v("InputMethodManager", stringBuilder.toString());
              } 
              this.mCurMethod.viewClicked(bool);
            } catch (RemoteException remoteException) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("IME died: ");
              stringBuilder.append(this.mCurId);
              Log.w("InputMethodManager", stringBuilder.toString(), (Throwable)remoteException);
            } 
            return;
          } 
        } 
        return;
      } 
    } 
  }
  
  @Deprecated
  public boolean isWatchingCursor(View paramView) {
    return false;
  }
  
  public boolean isCursorAnchorInfoEnabled() {
    synchronized (this.mH) {
      boolean bool2;
      int i = this.mRequestUpdateCursorAnchorInfoMonitorMode;
      boolean bool1 = true;
      if ((i & 0x1) != 0) {
        i = 1;
      } else {
        i = 0;
      } 
      if ((this.mRequestUpdateCursorAnchorInfoMonitorMode & 0x2) != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      boolean bool3 = bool1;
      if (i == 0)
        if (bool2) {
          bool3 = bool1;
        } else {
          bool3 = false;
        }  
      return bool3;
    } 
  }
  
  public void setUpdateCursorAnchorInfoMode(int paramInt) {
    synchronized (this.mH) {
      this.mRequestUpdateCursorAnchorInfoMonitorMode = paramInt;
      return;
    } 
  }
  
  @Deprecated
  public void updateCursor(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.updateCursor(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    } 
    checkFocus();
    synchronized (this.mH) {
      if (!hasServedByInputMethodLocked(paramView) || this.mCurrentTextBoxAttribute == null || this.mCurMethod == null)
        return; 
      this.mTmpCursorRect.set(paramInt1, paramInt2, paramInt3, paramInt4);
      if (!this.mCursorRect.equals(this.mTmpCursorRect)) {
        if (DEBUG)
          Log.d("InputMethodManager", "updateCursor"); 
        try {
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("CURSOR CHANGE: ");
            stringBuilder.append(this.mCurMethod);
            Log.v("InputMethodManager", stringBuilder.toString());
          } 
          this.mCurMethod.updateCursor(this.mTmpCursorRect);
          this.mCursorRect.set(this.mTmpCursorRect);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("IME died: ");
          stringBuilder.append(this.mCurId);
          Log.w("InputMethodManager", stringBuilder.toString(), (Throwable)remoteException);
        } 
      } 
      return;
    } 
  }
  
  public void updateCursorAnchorInfo(View paramView, CursorAnchorInfo paramCursorAnchorInfo) {
    if (paramView == null || paramCursorAnchorInfo == null)
      return; 
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.updateCursorAnchorInfo(paramView, paramCursorAnchorInfo);
      return;
    } 
    checkFocus();
    synchronized (this.mH) {
      if (!hasServedByInputMethodLocked(paramView) || this.mCurrentTextBoxAttribute == null || this.mCurMethod == null)
        return; 
      int i = this.mRequestUpdateCursorAnchorInfoMonitorMode;
      boolean bool = true;
      if ((i & 0x1) == 0)
        bool = false; 
      if (!bool && Objects.equals(this.mCursorAnchorInfo, paramCursorAnchorInfo)) {
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Ignoring redundant updateCursorAnchorInfo: info=");
          stringBuilder.append(paramCursorAnchorInfo);
          Log.w("InputMethodManager", stringBuilder.toString());
        } 
        return;
      } 
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("updateCursorAnchorInfo: ");
        stringBuilder.append(paramCursorAnchorInfo);
        Log.v("InputMethodManager", stringBuilder.toString());
      } 
      try {
        if (this.mActivityViewToScreenMatrix != null) {
          IInputMethodSession iInputMethodSession = this.mCurMethod;
          Matrix matrix = this.mActivityViewToScreenMatrix;
          CursorAnchorInfo cursorAnchorInfo = CursorAnchorInfo.createForAdditionalParentMatrix(paramCursorAnchorInfo, matrix);
          iInputMethodSession.updateCursorAnchorInfo(cursorAnchorInfo);
        } else {
          this.mCurMethod.updateCursorAnchorInfo(paramCursorAnchorInfo);
        } 
        this.mCursorAnchorInfo = paramCursorAnchorInfo;
        this.mRequestUpdateCursorAnchorInfoMonitorMode &= 0xFFFFFFFE;
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("IME died: ");
        stringBuilder.append(this.mCurId);
        Log.w("InputMethodManager", stringBuilder.toString(), (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  public void sendAppPrivateCommand(View paramView, String paramString, Bundle paramBundle) {
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.sendAppPrivateCommand(paramView, paramString, paramBundle);
      return;
    } 
    checkFocus();
    synchronized (this.mH) {
      if (hasServedByInputMethodLocked(paramView) && this.mCurrentTextBoxAttribute != null) {
        IInputMethodSession iInputMethodSession = this.mCurMethod;
        if (iInputMethodSession != null) {
          try {
            if (DEBUG) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("APP PRIVATE COMMAND ");
              stringBuilder.append(paramString);
              stringBuilder.append(": ");
              stringBuilder.append(paramBundle);
              Log.v("InputMethodManager", stringBuilder.toString());
            } 
            this.mCurMethod.appPrivateCommand(paramString, paramBundle);
          } catch (RemoteException remoteException) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("IME died: ");
            stringBuilder.append(this.mCurId);
            Log.w("InputMethodManager", stringBuilder.toString(), (Throwable)remoteException);
          } 
          return;
        } 
      } 
      return;
    } 
  }
  
  @Deprecated
  public void setInputMethod(IBinder paramIBinder, String paramString) {
    ContentResolver contentResolver;
    if (paramIBinder == null) {
      StringBuilder stringBuilder;
      boolean bool2;
      if (paramString == null)
        return; 
      if (Process.myUid() == 1000) {
        Log.w("InputMethodManager", "System process should not be calling setInputMethod() because almost always it is a bug under multi-user / multi-profile environment. Consider interacting with InputMethodManagerService directly via LocalServices.");
        return;
      } 
      Application application = ActivityThread.currentApplication();
      if (application == null)
        return; 
      if (application.checkSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0)
        return; 
      List<InputMethodInfo> list = getEnabledInputMethodList();
      int i = list.size();
      boolean bool1 = false;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < i) {
          InputMethodInfo inputMethodInfo = list.get(b);
          if (paramString.equals(inputMethodInfo.getId())) {
            bool2 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (!bool2) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Ignoring setInputMethod(null, ");
        stringBuilder.append(paramString);
        stringBuilder.append(") because the specified id not found in enabled IMEs.");
        Log.e("InputMethodManager", stringBuilder.toString());
        return;
      } 
      Log.w("InputMethodManager", "The undocumented behavior that setInputMethod() accepts null token when the caller has WRITE_SECURE_SETTINGS is deprecated. This behavior may be completely removed in a future version.  Update secure settings directly instead.");
      contentResolver = stringBuilder.getContentResolver();
      Settings.Secure.putInt(contentResolver, "selected_input_method_subtype", -1);
      Settings.Secure.putString(contentResolver, "default_input_method", paramString);
      return;
    } 
    InputMethodPrivilegedOperationsRegistry.get((IBinder)contentResolver).setInputMethod(paramString);
  }
  
  @Deprecated
  public void setInputMethodAndSubtype(IBinder paramIBinder, String paramString, InputMethodSubtype paramInputMethodSubtype) {
    if (paramIBinder == null) {
      Log.e("InputMethodManager", "setInputMethodAndSubtype() does not accept null token on Android Q and later.");
      return;
    } 
    InputMethodPrivilegedOperationsRegistry.get(paramIBinder).setInputMethodAndSubtype(paramString, paramInputMethodSubtype);
  }
  
  @Deprecated
  public void hideSoftInputFromInputMethod(IBinder paramIBinder, int paramInt) {
    printCallPidAndUid("hideSoftInputFromInputMethod");
    InputMethodPrivilegedOperationsRegistry.get(paramIBinder).hideMySoftInput(paramInt);
  }
  
  @Deprecated
  public void showSoftInputFromInputMethod(IBinder paramIBinder, int paramInt) {
    printCallPidAndUid("showSoftInputFromInputMethod");
    InputMethodPrivilegedOperationsRegistry.get(paramIBinder).showMySoftInput(paramInt);
  }
  
  public int dispatchInputEvent(InputEvent paramInputEvent, Object paramObject, FinishedInputEventCallback paramFinishedInputEventCallback, Handler paramHandler) {
    synchronized (this.mH) {
      if (this.mCurMethod != null) {
        if (paramInputEvent instanceof KeyEvent) {
          KeyEvent keyEvent = (KeyEvent)paramInputEvent;
          if (keyEvent.getAction() == 0 && 
            keyEvent.getKeyCode() == 63 && 
            keyEvent.getRepeatCount() == 0) {
            showInputMethodPickerLocked();
            return 1;
          } 
        } 
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("DISPATCH INPUT EVENT: ");
          stringBuilder.append(this.mCurMethod);
          Log.v("InputMethodManager", stringBuilder.toString());
        } 
        PendingEvent pendingEvent = obtainPendingEventLocked(paramInputEvent, paramObject, this.mCurId, paramFinishedInputEventCallback, paramHandler);
        if (this.mMainLooper.isCurrentThread())
          return sendInputEventOnMainLooperLocked(pendingEvent); 
        Message message = this.mH.obtainMessage(5, pendingEvent);
        message.setAsynchronous(true);
        this.mH.sendMessage(message);
        return -1;
      } 
      return 0;
    } 
  }
  
  public void dispatchKeyEventFromInputMethod(View paramView, KeyEvent paramKeyEvent) {
    ViewRootImpl viewRootImpl;
    InputMethodManager inputMethodManager = getFallbackInputMethodManagerIfNecessary(paramView);
    if (inputMethodManager != null) {
      inputMethodManager.dispatchKeyEventFromInputMethod(paramView, paramKeyEvent);
      return;
    } 
    H h = this.mH;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
    if (paramView != null) {
      try {
        ViewRootImpl viewRootImpl1 = paramView.getViewRootImpl();
      } finally {}
    } else {
      paramView = null;
    } 
    View view = paramView;
    if (paramView == null) {
      View view1 = getServedViewLocked();
      view = paramView;
      if (view1 != null)
        viewRootImpl = view1.getViewRootImpl(); 
    } 
    if (viewRootImpl != null)
      viewRootImpl.dispatchKeyFromIme(paramKeyEvent); 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/view/inputmethod/InputMethodManager}.Landroid/view/inputmethod/InputMethodManager$H;}, name=null} */
  }
  
  void sendInputEventAndReportResultOnMainLooper(PendingEvent paramPendingEvent) {
    synchronized (this.mH) {
      int i = sendInputEventOnMainLooperLocked(paramPendingEvent);
      if (i == -1)
        return; 
      boolean bool = true;
      if (i != 1)
        bool = false; 
      invokeFinishedInputEventCallback(paramPendingEvent, bool);
      return;
    } 
  }
  
  int sendInputEventOnMainLooperLocked(PendingEvent paramPendingEvent) {
    InputChannel inputChannel = this.mCurChannel;
    if (inputChannel != null) {
      SparseArray<PendingEvent> sparseArray;
      if (this.mCurSender == null)
        this.mCurSender = new ImeInputEventSender(inputChannel, this.mH.getLooper()); 
      InputEvent inputEvent = paramPendingEvent.mEvent;
      int i = inputEvent.getSequenceNumber();
      if (this.mCurSender.sendInputEvent(i, inputEvent)) {
        this.mPendingEvents.put(i, paramPendingEvent);
        sparseArray = this.mPendingEvents;
        int j = sparseArray.size();
        Trace.traceCounter(4L, "aq:imm", j);
        Message message = this.mH.obtainMessage(6, i, 0, paramPendingEvent);
        message.setAsynchronous(true);
        this.mH.sendMessageDelayed(message, 2500L);
        return -1;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to send input event to IME: ");
      stringBuilder.append(this.mCurId);
      stringBuilder.append(" dropping: ");
      stringBuilder.append(sparseArray);
      Log.w("InputMethodManager", stringBuilder.toString());
    } 
    return 0;
  }
  
  void finishedInputEvent(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    synchronized (this.mH) {
      paramInt = this.mPendingEvents.indexOfKey(paramInt);
      if (paramInt < 0)
        return; 
      PendingEvent pendingEvent = this.mPendingEvents.valueAt(paramInt);
      this.mPendingEvents.removeAt(paramInt);
      Trace.traceCounter(4L, "aq:imm", this.mPendingEvents.size());
      if (paramBoolean2) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Timeout waiting for IME to handle input event after 2500 ms: ");
        stringBuilder.append(pendingEvent.mInputMethodId);
        Log.w("InputMethodManager", stringBuilder.toString());
      } else {
        this.mH.removeMessages(6, pendingEvent);
      } 
      invokeFinishedInputEventCallback(pendingEvent, paramBoolean1);
      return;
    } 
  }
  
  void invokeFinishedInputEventCallback(PendingEvent paramPendingEvent, boolean paramBoolean) {
    paramPendingEvent.mHandled = paramBoolean;
    if (paramPendingEvent.mHandler.getLooper().isCurrentThread()) {
      paramPendingEvent.run();
    } else {
      Message message = Message.obtain(paramPendingEvent.mHandler, paramPendingEvent);
      message.setAsynchronous(true);
      message.sendToTarget();
    } 
  }
  
  private void flushPendingEventsLocked() {
    this.mH.removeMessages(7);
    int i = this.mPendingEvents.size();
    for (byte b = 0; b < i; b++) {
      int j = this.mPendingEvents.keyAt(b);
      Message message = this.mH.obtainMessage(7, j, 0);
      message.setAsynchronous(true);
      message.sendToTarget();
    } 
  }
  
  private PendingEvent obtainPendingEventLocked(InputEvent paramInputEvent, Object paramObject, String paramString, FinishedInputEventCallback paramFinishedInputEventCallback, Handler paramHandler) {
    PendingEvent pendingEvent1 = this.mPendingEventPool.acquire();
    PendingEvent pendingEvent2 = pendingEvent1;
    if (pendingEvent1 == null)
      pendingEvent2 = new PendingEvent(); 
    pendingEvent2.mEvent = paramInputEvent;
    pendingEvent2.mToken = paramObject;
    pendingEvent2.mInputMethodId = paramString;
    pendingEvent2.mCallback = paramFinishedInputEventCallback;
    pendingEvent2.mHandler = paramHandler;
    return pendingEvent2;
  }
  
  private void recyclePendingEventLocked(PendingEvent paramPendingEvent) {
    paramPendingEvent.recycle();
    this.mPendingEventPool.release(paramPendingEvent);
  }
  
  public void showInputMethodPicker() {
    synchronized (this.mH) {
      showInputMethodPickerLocked();
      return;
    } 
  }
  
  public void showInputMethodPickerFromSystem(boolean paramBoolean, int paramInt) {
    byte b;
    if (paramBoolean) {
      b = 1;
    } else {
      b = 2;
    } 
    try {
      this.mService.showInputMethodPickerFromSystem((IInputMethodClient)this.mClient, b, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void showInputMethodPickerLocked() {
    try {
      this.mService.showInputMethodPickerFromClient((IInputMethodClient)this.mClient, 0);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isInputMethodPickerShown() {
    try {
      return this.mService.isInputMethodPickerShownForTest();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void showInputMethodAndSubtypeEnabler(String paramString) {
    try {
      this.mService.showInputMethodAndSubtypeEnablerFromClient((IInputMethodClient)this.mClient, paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public InputMethodSubtype getCurrentInputMethodSubtype() {
    try {
      return this.mService.getCurrentInputMethodSubtype();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public boolean setCurrentInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype) {
    if (Process.myUid() == 1000) {
      Log.w("InputMethodManager", "System process should not call setCurrentInputMethodSubtype() because almost always it is a bug under multi-user / multi-profile environment. Consider directly interacting with InputMethodManagerService via LocalServices.");
      return false;
    } 
    if (paramInputMethodSubtype == null)
      return false; 
    Application application = ActivityThread.currentApplication();
    if (application == null)
      return false; 
    if (application.checkSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0)
      return false; 
    ContentResolver contentResolver = application.getContentResolver();
    String str = Settings.Secure.getString(contentResolver, "default_input_method");
    if (ComponentName.unflattenFromString(str) == null)
      return false; 
    try {
      List<InputMethodSubtype> list = this.mService.getEnabledInputMethodSubtypeList(str, true);
      int i = list.size();
      for (int j = 0; j < i; j++) {
        InputMethodSubtype inputMethodSubtype = list.get(j);
        if (inputMethodSubtype.equals(paramInputMethodSubtype)) {
          j = inputMethodSubtype.hashCode();
          Settings.Secure.putInt(contentResolver, "selected_input_method_subtype", j);
          return true;
        } 
      } 
      return false;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  @Deprecated
  public void notifyUserAction() {
    Log.w("InputMethodManager", "notifyUserAction() is a hidden method, which is now just a stub method that does nothing.  Leave comments in b.android.com/114740982 if your  application still depends on the previous behavior of this method.");
  }
  
  public Map<InputMethodInfo, List<InputMethodSubtype>> getShortcutInputMethodsAndSubtypes() {
    List<InputMethodInfo> list = getEnabledInputMethodList();
    list.sort(Comparator.comparingInt((ToIntFunction<? super InputMethodInfo>)_$$Lambda$InputMethodManager$pvWYFFVbHzZCDCCTiZVM09Xls4w.INSTANCE));
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      InputMethodInfo inputMethodInfo = list.get(b);
      List<InputMethodSubtype> list1 = getEnabledInputMethodSubtypeList(inputMethodInfo, true);
      int j = list1.size();
      for (byte b1 = 0; b1 < j; b1++) {
        InputMethodSubtype inputMethodSubtype = inputMethodInfo.getSubtypeAt(b1);
        if ("voice".equals(inputMethodSubtype.getMode()))
          return Collections.singletonMap(inputMethodInfo, Collections.singletonList(inputMethodSubtype)); 
      } 
    } 
    return Collections.emptyMap();
  }
  
  public int getInputMethodWindowVisibleHeight() {
    try {
      return this.mService.getInputMethodWindowVisibleHeight();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportActivityView(int paramInt, Matrix paramMatrix) {
    if (paramMatrix == null) {
      paramMatrix = null;
    } else {
      try {
        float[] arrayOfFloat2 = new float[9];
        paramMatrix.getValues(arrayOfFloat2);
        float[] arrayOfFloat1 = arrayOfFloat2;
        this.mService.reportActivityView((IInputMethodClient)this.mClient, paramInt, arrayOfFloat1);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    this.mService.reportActivityView((IInputMethodClient)this.mClient, paramInt, (float[])remoteException);
  }
  
  @Deprecated
  public boolean switchToLastInputMethod(IBinder paramIBinder) {
    return InputMethodPrivilegedOperationsRegistry.get(paramIBinder).switchToPreviousInputMethod();
  }
  
  @Deprecated
  public boolean switchToNextInputMethod(IBinder paramIBinder, boolean paramBoolean) {
    InputMethodPrivilegedOperations inputMethodPrivilegedOperations = InputMethodPrivilegedOperationsRegistry.get(paramIBinder);
    return 
      inputMethodPrivilegedOperations.switchToNextInputMethod(paramBoolean);
  }
  
  @Deprecated
  public boolean shouldOfferSwitchingToNextInputMethod(IBinder paramIBinder) {
    InputMethodPrivilegedOperations inputMethodPrivilegedOperations = InputMethodPrivilegedOperationsRegistry.get(paramIBinder);
    return 
      inputMethodPrivilegedOperations.shouldOfferSwitchingToNextInputMethod();
  }
  
  @Deprecated
  public void setAdditionalInputMethodSubtypes(String paramString, InputMethodSubtype[] paramArrayOfInputMethodSubtype) {
    try {
      this.mService.setAdditionalInputMethodSubtypes(paramString, paramArrayOfInputMethodSubtype);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public InputMethodSubtype getLastInputMethodSubtype() {
    try {
      return this.mService.getLastInputMethodSubtype();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void maybeCallServedViewChangedLocked(EditorInfo paramEditorInfo) {
    ImeInsetsSourceConsumer imeInsetsSourceConsumer = this.mImeInsetsConsumer;
    if (imeInsetsSourceConsumer != null)
      imeInsetsSourceConsumer.onServedEditorChanged(paramEditorInfo); 
  }
  
  public int getDisplayId() {
    return this.mDisplayId;
  }
  
  void doDump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    PrintWriterPrinter printWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
    if (dynamicallyConfigImsLogTag(printWriterPrinter, paramArrayOfString))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Input method client state for ");
    stringBuilder.append(this);
    stringBuilder.append(":");
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mService=");
    stringBuilder.append(this.mService);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mMainLooper=");
    stringBuilder.append(this.mMainLooper);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mIInputContext=");
    stringBuilder.append(this.mIInputContext);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mActive=");
    stringBuilder.append(this.mActive);
    stringBuilder.append(" mRestartOnNextWindowFocus=");
    stringBuilder.append(this.mRestartOnNextWindowFocus);
    stringBuilder.append(" mBindSequence=");
    stringBuilder.append(this.mBindSequence);
    stringBuilder.append(" mCurId=");
    stringBuilder.append(this.mCurId);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mFullscreenMode=");
    stringBuilder.append(this.mFullscreenMode);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mCurMethod=");
    stringBuilder.append(this.mCurMethod);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mCurRootView=");
    stringBuilder.append(this.mCurRootView);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mServedView=");
    stringBuilder.append(getServedViewLocked());
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mNextServedView=");
    stringBuilder.append(getNextServedViewLocked());
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mServedConnecting=");
    stringBuilder.append(this.mServedConnecting);
    printWriterPrinter.println(stringBuilder.toString());
    if (this.mCurrentTextBoxAttribute != null) {
      printWriterPrinter.println("  mCurrentTextBoxAttribute:");
      this.mCurrentTextBoxAttribute.dump(printWriterPrinter, "    ");
    } else {
      printWriterPrinter.println("  mCurrentTextBoxAttribute: null");
    } 
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mServedInputConnectionWrapper=");
    stringBuilder.append(this.mServedInputConnectionWrapper);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mCompletions=");
    stringBuilder.append(Arrays.toString((Object[])this.mCompletions));
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mCursorRect=");
    stringBuilder.append(this.mCursorRect);
    printWriterPrinter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mCursorSelStart=");
    stringBuilder.append(this.mCursorSelStart);
    stringBuilder.append(" mCursorSelEnd=");
    stringBuilder.append(this.mCursorSelEnd);
    stringBuilder.append(" mCursorCandStart=");
    stringBuilder.append(this.mCursorCandStart);
    stringBuilder.append(" mCursorCandEnd=");
    stringBuilder.append(this.mCursorCandEnd);
    printWriterPrinter.println(stringBuilder.toString());
  }
  
  private final class ImeInputEventSender extends InputEventSender {
    final InputMethodManager this$0;
    
    public ImeInputEventSender(InputChannel param1InputChannel, Looper param1Looper) {
      super(param1InputChannel, param1Looper);
    }
    
    public void onInputEventFinished(int param1Int, boolean param1Boolean) {
      InputMethodManager.this.finishedInputEvent(param1Int, param1Boolean, false);
    }
  }
  
  class PendingEvent implements Runnable {
    public InputMethodManager.FinishedInputEventCallback mCallback;
    
    public InputEvent mEvent;
    
    public boolean mHandled;
    
    public Handler mHandler;
    
    public String mInputMethodId;
    
    public Object mToken;
    
    final InputMethodManager this$0;
    
    private PendingEvent() {}
    
    public void recycle() {
      this.mEvent = null;
      this.mToken = null;
      this.mInputMethodId = null;
      this.mCallback = null;
      this.mHandler = null;
      this.mHandled = false;
    }
    
    public void run() {
      this.mCallback.onFinishedInputEvent(this.mToken, this.mHandled);
      synchronized (InputMethodManager.this.mH) {
        InputMethodManager.this.recyclePendingEventLocked(this);
        return;
      } 
    }
  }
  
  private static String dumpViewInfo(View paramView) {
    if (paramView == null)
      return "null"; 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramView);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(",focus=");
    stringBuilder2.append(paramView.hasFocus());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(",windowFocus=");
    stringBuilder2.append(paramView.hasWindowFocus());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(",autofillUiShowing=");
    stringBuilder2.append(isAutofillUIShowing(paramView));
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(",window=");
    stringBuilder2.append(paramView.getWindowToken());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(",displayId=");
    stringBuilder2.append(paramView.getContext().getDisplayId());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(",temporaryDetach=");
    stringBuilder2.append(paramView.isTemporarilyDetached());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(",hasImeFocus=");
    stringBuilder2.append(paramView.hasImeFocus());
    stringBuilder1.append(stringBuilder2.toString());
    return stringBuilder1.toString();
  }
  
  class FinishedInputEventCallback {
    public abstract void onFinishedInputEvent(Object param1Object, boolean param1Boolean);
  }
}
