package android.view.inputmethod;

import android.os.Bundle;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

public final class InputConnectionInspector {
  private static final Map<Class, Integer> sMissingMethodsMap = Collections.synchronizedMap(new WeakHashMap<>());
  
  public static int getMissingMethodFlags(InputConnection paramInputConnection) {
    if (paramInputConnection == null)
      return 0; 
    if (paramInputConnection instanceof BaseInputConnection)
      return 0; 
    if (paramInputConnection instanceof InputConnectionWrapper)
      return ((InputConnectionWrapper)paramInputConnection).getMissingMethodFlags(); 
    return getMissingMethodFlagsInternal(paramInputConnection.getClass());
  }
  
  public static int getMissingMethodFlagsInternal(Class paramClass) {
    Integer integer = sMissingMethodsMap.get(paramClass);
    if (integer != null)
      return integer.intValue(); 
    int i = 0;
    if (!hasGetSelectedText(paramClass))
      i = false | true; 
    int j = i;
    if (!hasSetComposingRegion(paramClass))
      j = i | 0x2; 
    i = j;
    if (!hasCommitCorrection(paramClass))
      i = j | 0x4; 
    j = i;
    if (!hasRequestCursorUpdate(paramClass))
      j = i | 0x8; 
    int k = j;
    if (!hasDeleteSurroundingTextInCodePoints(paramClass))
      k = j | 0x10; 
    i = k;
    if (!hasGetHandler(paramClass))
      i = k | 0x20; 
    j = i;
    if (!hasCloseConnection(paramClass))
      j = i | 0x40; 
    i = j;
    if (!hasCommitContent(paramClass))
      i = j | 0x80; 
    sMissingMethodsMap.put(paramClass, Integer.valueOf(i));
    return i;
  }
  
  private static boolean hasGetSelectedText(Class paramClass) {
    try {
      Method method = paramClass.getMethod("getSelectedText", new Class[] { int.class });
      boolean bool = Modifier.isAbstract(method.getModifiers());
      return bool ^ true;
    } catch (NoSuchMethodException noSuchMethodException) {
      return false;
    } 
  }
  
  private static boolean hasSetComposingRegion(Class paramClass) {
    try {
      Method method = paramClass.getMethod("setComposingRegion", new Class[] { int.class, int.class });
      boolean bool = Modifier.isAbstract(method.getModifiers());
      return bool ^ true;
    } catch (NoSuchMethodException noSuchMethodException) {
      return false;
    } 
  }
  
  private static boolean hasCommitCorrection(Class paramClass) {
    try {
      Method method = paramClass.getMethod("commitCorrection", new Class[] { CorrectionInfo.class });
      boolean bool = Modifier.isAbstract(method.getModifiers());
      return bool ^ true;
    } catch (NoSuchMethodException noSuchMethodException) {
      return false;
    } 
  }
  
  private static boolean hasRequestCursorUpdate(Class paramClass) {
    try {
      Method method = paramClass.getMethod("requestCursorUpdates", new Class[] { int.class });
      boolean bool = Modifier.isAbstract(method.getModifiers());
      return bool ^ true;
    } catch (NoSuchMethodException noSuchMethodException) {
      return false;
    } 
  }
  
  private static boolean hasDeleteSurroundingTextInCodePoints(Class paramClass) {
    try {
      Method method = paramClass.getMethod("deleteSurroundingTextInCodePoints", new Class[] { int.class, int.class });
      boolean bool = Modifier.isAbstract(method.getModifiers());
      return bool ^ true;
    } catch (NoSuchMethodException noSuchMethodException) {
      return false;
    } 
  }
  
  private static boolean hasGetHandler(Class paramClass) {
    try {
      Method method = paramClass.getMethod("getHandler", new Class[0]);
      boolean bool = Modifier.isAbstract(method.getModifiers());
      return bool ^ true;
    } catch (NoSuchMethodException noSuchMethodException) {
      return false;
    } 
  }
  
  private static boolean hasCloseConnection(Class paramClass) {
    try {
      Method method = paramClass.getMethod("closeConnection", new Class[0]);
      boolean bool = Modifier.isAbstract(method.getModifiers());
      return bool ^ true;
    } catch (NoSuchMethodException noSuchMethodException) {
      return false;
    } 
  }
  
  private static boolean hasCommitContent(Class paramClass) {
    try {
      Method method = paramClass.getMethod("commitContent", new Class[] { InputContentInfo.class, int.class, Bundle.class });
      boolean bool = Modifier.isAbstract(method.getModifiers());
      return bool ^ true;
    } catch (NoSuchMethodException noSuchMethodException) {
      return false;
    } 
  }
  
  public static String getMissingMethodFlagsAsString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    boolean bool1 = true;
    if ((paramInt & 0x1) != 0) {
      stringBuilder.append("getSelectedText(int)");
      bool1 = false;
    } 
    boolean bool2 = bool1;
    if ((paramInt & 0x2) != 0) {
      if (!bool1)
        stringBuilder.append(","); 
      stringBuilder.append("setComposingRegion(int, int)");
      bool2 = false;
    } 
    bool1 = bool2;
    if ((paramInt & 0x4) != 0) {
      if (!bool2)
        stringBuilder.append(","); 
      stringBuilder.append("commitCorrection(CorrectionInfo)");
      bool1 = false;
    } 
    bool2 = bool1;
    if ((paramInt & 0x8) != 0) {
      if (!bool1)
        stringBuilder.append(","); 
      stringBuilder.append("requestCursorUpdate(int)");
      bool2 = false;
    } 
    bool1 = bool2;
    if ((paramInt & 0x10) != 0) {
      if (!bool2)
        stringBuilder.append(","); 
      stringBuilder.append("deleteSurroundingTextInCodePoints(int, int)");
      bool1 = false;
    } 
    if ((paramInt & 0x20) != 0) {
      if (!bool1)
        stringBuilder.append(","); 
      stringBuilder.append("getHandler()");
    } 
    if ((paramInt & 0x40) != 0) {
      if (!bool1)
        stringBuilder.append(","); 
      stringBuilder.append("closeConnection()");
    } 
    if ((paramInt & 0x80) != 0) {
      if (!bool1)
        stringBuilder.append(","); 
      stringBuilder.append("commitContent(InputContentInfo, Bundle)");
    } 
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface MissingMethodFlags {
    public static final int CLOSE_CONNECTION = 64;
    
    public static final int COMMIT_CONTENT = 128;
    
    public static final int COMMIT_CORRECTION = 4;
    
    public static final int DELETE_SURROUNDING_TEXT_IN_CODE_POINTS = 16;
    
    public static final int GET_HANDLER = 32;
    
    public static final int GET_SELECTED_TEXT = 1;
    
    public static final int REQUEST_CURSOR_UPDATES = 8;
    
    public static final int SET_COMPOSING_REGION = 2;
  }
}
