package android.view.inputmethod;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;

public class InputConnectionWrapper implements InputConnection {
  private int mMissingMethodFlags;
  
  final boolean mMutable;
  
  private InputConnection mTarget;
  
  public InputConnectionWrapper(InputConnection paramInputConnection, boolean paramBoolean) {
    this.mMutable = paramBoolean;
    this.mTarget = paramInputConnection;
    this.mMissingMethodFlags = InputConnectionInspector.getMissingMethodFlags(paramInputConnection);
  }
  
  public void setTarget(InputConnection paramInputConnection) {
    if (this.mTarget == null || this.mMutable) {
      this.mTarget = paramInputConnection;
      this.mMissingMethodFlags = InputConnectionInspector.getMissingMethodFlags(paramInputConnection);
      return;
    } 
    throw new SecurityException("not mutable");
  }
  
  public int getMissingMethodFlags() {
    return this.mMissingMethodFlags;
  }
  
  public CharSequence getTextBeforeCursor(int paramInt1, int paramInt2) {
    return this.mTarget.getTextBeforeCursor(paramInt1, paramInt2);
  }
  
  public CharSequence getTextAfterCursor(int paramInt1, int paramInt2) {
    return this.mTarget.getTextAfterCursor(paramInt1, paramInt2);
  }
  
  public CharSequence getSelectedText(int paramInt) {
    return this.mTarget.getSelectedText(paramInt);
  }
  
  public int getCursorCapsMode(int paramInt) {
    return this.mTarget.getCursorCapsMode(paramInt);
  }
  
  public ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt) {
    return this.mTarget.getExtractedText(paramExtractedTextRequest, paramInt);
  }
  
  public boolean deleteSurroundingTextInCodePoints(int paramInt1, int paramInt2) {
    return this.mTarget.deleteSurroundingTextInCodePoints(paramInt1, paramInt2);
  }
  
  public boolean deleteSurroundingText(int paramInt1, int paramInt2) {
    return this.mTarget.deleteSurroundingText(paramInt1, paramInt2);
  }
  
  public boolean setComposingText(CharSequence paramCharSequence, int paramInt) {
    return this.mTarget.setComposingText(paramCharSequence, paramInt);
  }
  
  public boolean setComposingRegion(int paramInt1, int paramInt2) {
    return this.mTarget.setComposingRegion(paramInt1, paramInt2);
  }
  
  public boolean finishComposingText() {
    return this.mTarget.finishComposingText();
  }
  
  public boolean commitText(CharSequence paramCharSequence, int paramInt) {
    return this.mTarget.commitText(paramCharSequence, paramInt);
  }
  
  public boolean commitCompletion(CompletionInfo paramCompletionInfo) {
    return this.mTarget.commitCompletion(paramCompletionInfo);
  }
  
  public boolean commitCorrection(CorrectionInfo paramCorrectionInfo) {
    return this.mTarget.commitCorrection(paramCorrectionInfo);
  }
  
  public boolean setSelection(int paramInt1, int paramInt2) {
    return this.mTarget.setSelection(paramInt1, paramInt2);
  }
  
  public boolean performEditorAction(int paramInt) {
    return this.mTarget.performEditorAction(paramInt);
  }
  
  public boolean performContextMenuAction(int paramInt) {
    return this.mTarget.performContextMenuAction(paramInt);
  }
  
  public boolean beginBatchEdit() {
    return this.mTarget.beginBatchEdit();
  }
  
  public boolean endBatchEdit() {
    return this.mTarget.endBatchEdit();
  }
  
  public boolean sendKeyEvent(KeyEvent paramKeyEvent) {
    return this.mTarget.sendKeyEvent(paramKeyEvent);
  }
  
  public boolean clearMetaKeyStates(int paramInt) {
    return this.mTarget.clearMetaKeyStates(paramInt);
  }
  
  public boolean reportFullscreenMode(boolean paramBoolean) {
    return this.mTarget.reportFullscreenMode(paramBoolean);
  }
  
  public boolean performPrivateCommand(String paramString, Bundle paramBundle) {
    return this.mTarget.performPrivateCommand(paramString, paramBundle);
  }
  
  public boolean requestCursorUpdates(int paramInt) {
    return this.mTarget.requestCursorUpdates(paramInt);
  }
  
  public Handler getHandler() {
    InputConnection inputConnection = this.mTarget;
    if (inputConnection == null) {
      Log.w("InputConnectionWrapper", "getHandler:target inputconcection not init.");
      return null;
    } 
    return inputConnection.getHandler();
  }
  
  public void closeConnection() {
    InputConnection inputConnection = this.mTarget;
    if (inputConnection == null) {
      Log.w("InputConnectionWrapper", "closeConnection:target inputconcection not init.");
      return;
    } 
    inputConnection.closeConnection();
  }
  
  public boolean commitContent(InputContentInfo paramInputContentInfo, int paramInt, Bundle paramBundle) {
    return this.mTarget.commitContent(paramInputContentInfo, paramInt, paramBundle);
  }
}
