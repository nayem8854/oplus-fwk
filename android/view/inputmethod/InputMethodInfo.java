package android.view.inputmethod;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Printer;
import java.io.IOException;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

public final class InputMethodInfo implements Parcelable {
  public static String computeId(ResolveInfo paramResolveInfo) {
    ServiceInfo serviceInfo = paramResolveInfo.serviceInfo;
    return (new ComponentName(serviceInfo.packageName, serviceInfo.name)).flattenToShortString();
  }
  
  public InputMethodInfo(Context paramContext, ResolveInfo paramResolveInfo) throws XmlPullParserException, IOException {
    this(paramContext, paramResolveInfo, null);
  }
  
  public InputMethodInfo(Context paramContext, ResolveInfo paramResolveInfo, List<InputMethodSubtype> paramList) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_0
    //   5: aload_2
    //   6: putfield mService : Landroid/content/pm/ResolveInfo;
    //   9: aload_2
    //   10: getfield serviceInfo : Landroid/content/pm/ServiceInfo;
    //   13: astore #4
    //   15: aload_0
    //   16: aload_2
    //   17: invokestatic computeId : (Landroid/content/pm/ResolveInfo;)Ljava/lang/String;
    //   20: putfield mId : Ljava/lang/String;
    //   23: aload_0
    //   24: iconst_0
    //   25: putfield mForceDefault : Z
    //   28: aload_1
    //   29: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   32: astore_1
    //   33: aconst_null
    //   34: astore #5
    //   36: aconst_null
    //   37: astore #6
    //   39: new java/util/ArrayList
    //   42: dup
    //   43: invokespecial <init> : ()V
    //   46: astore #7
    //   48: aload #4
    //   50: aload_1
    //   51: ldc 'android.view.im'
    //   53: invokevirtual loadXmlMetaData : (Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    //   56: astore_2
    //   57: aload_2
    //   58: ifnull -> 1070
    //   61: aload_2
    //   62: astore #6
    //   64: aload_2
    //   65: astore #5
    //   67: aload_1
    //   68: aload #4
    //   70: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   73: invokevirtual getResourcesForApplication : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    //   76: astore #8
    //   78: aload_2
    //   79: astore #6
    //   81: aload_2
    //   82: astore #5
    //   84: aload_2
    //   85: invokestatic asAttributeSet : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    //   88: astore #9
    //   90: aload_2
    //   91: astore #6
    //   93: aload_2
    //   94: astore #5
    //   96: aload_2
    //   97: invokeinterface next : ()I
    //   102: istore #10
    //   104: iload #10
    //   106: iconst_1
    //   107: if_icmpeq -> 119
    //   110: iload #10
    //   112: iconst_2
    //   113: if_icmpeq -> 119
    //   116: goto -> 90
    //   119: aload_2
    //   120: astore #6
    //   122: aload_2
    //   123: astore #5
    //   125: aload_2
    //   126: invokeinterface getName : ()Ljava/lang/String;
    //   131: astore #11
    //   133: aload_2
    //   134: astore #6
    //   136: aload_2
    //   137: astore #5
    //   139: ldc 'input-method'
    //   141: aload #11
    //   143: invokevirtual equals : (Ljava/lang/Object;)Z
    //   146: ifeq -> 1027
    //   149: aload_2
    //   150: astore #6
    //   152: aload_2
    //   153: astore #5
    //   155: aload #8
    //   157: aload #9
    //   159: getstatic com/android/internal/R$styleable.InputMethod : [I
    //   162: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   165: astore #12
    //   167: aload_2
    //   168: astore #6
    //   170: aload_2
    //   171: astore #5
    //   173: aload #12
    //   175: iconst_1
    //   176: invokevirtual getString : (I)Ljava/lang/String;
    //   179: astore #13
    //   181: iconst_1
    //   182: istore #14
    //   184: iconst_1
    //   185: istore #15
    //   187: iconst_1
    //   188: istore #16
    //   190: aload_1
    //   191: astore #11
    //   193: iload #14
    //   195: istore #17
    //   197: aload_1
    //   198: astore #11
    //   200: iload #15
    //   202: istore #17
    //   204: aload #12
    //   206: iconst_3
    //   207: iconst_0
    //   208: invokevirtual getBoolean : (IZ)Z
    //   211: istore #18
    //   213: aload_1
    //   214: astore #11
    //   216: iload #14
    //   218: istore #17
    //   220: aload_1
    //   221: astore #11
    //   223: iload #15
    //   225: istore #17
    //   227: aload #12
    //   229: iconst_0
    //   230: iconst_0
    //   231: invokevirtual getResourceId : (II)I
    //   234: istore #19
    //   236: aload_1
    //   237: astore #11
    //   239: iload #14
    //   241: istore #17
    //   243: aload_1
    //   244: astore #11
    //   246: iload #15
    //   248: istore #17
    //   250: aload #12
    //   252: iconst_2
    //   253: iconst_0
    //   254: invokevirtual getBoolean : (IZ)Z
    //   257: istore #20
    //   259: aload_1
    //   260: astore #11
    //   262: iload #14
    //   264: istore #17
    //   266: aload_1
    //   267: astore #11
    //   269: iload #15
    //   271: istore #17
    //   273: aload #12
    //   275: iconst_4
    //   276: iconst_0
    //   277: invokevirtual getBoolean : (IZ)Z
    //   280: istore #21
    //   282: aload_1
    //   283: astore #11
    //   285: iload #14
    //   287: istore #17
    //   289: aload_1
    //   290: astore #11
    //   292: iload #15
    //   294: istore #17
    //   296: aload #12
    //   298: invokevirtual recycle : ()V
    //   301: aload_1
    //   302: astore #11
    //   304: iload #14
    //   306: istore #17
    //   308: aload_1
    //   309: astore #11
    //   311: iload #15
    //   313: istore #17
    //   315: aload_2
    //   316: invokeinterface getDepth : ()I
    //   321: istore #10
    //   323: iload #16
    //   325: istore #14
    //   327: aload #8
    //   329: astore #5
    //   331: aload #12
    //   333: astore #6
    //   335: aload_1
    //   336: astore #11
    //   338: iload #14
    //   340: istore #17
    //   342: aload_1
    //   343: astore #11
    //   345: iload #14
    //   347: istore #17
    //   349: aload_2
    //   350: invokeinterface next : ()I
    //   355: istore #22
    //   357: iload #22
    //   359: iconst_3
    //   360: if_icmpne -> 392
    //   363: aload_2
    //   364: invokeinterface getDepth : ()I
    //   369: istore #23
    //   371: iload #23
    //   373: iload #10
    //   375: if_icmple -> 381
    //   378: goto -> 392
    //   381: goto -> 834
    //   384: astore_1
    //   385: goto -> 1173
    //   388: astore_1
    //   389: goto -> 1132
    //   392: iload #22
    //   394: iconst_1
    //   395: if_icmpeq -> 834
    //   398: iload #22
    //   400: iconst_2
    //   401: if_icmpne -> 831
    //   404: aload_1
    //   405: astore #11
    //   407: iload #14
    //   409: istore #17
    //   411: aload_1
    //   412: astore #11
    //   414: iload #14
    //   416: istore #17
    //   418: aload_2
    //   419: invokeinterface getName : ()Ljava/lang/String;
    //   424: astore #12
    //   426: aload_1
    //   427: astore #11
    //   429: iload #14
    //   431: istore #17
    //   433: aload_1
    //   434: astore #11
    //   436: iload #14
    //   438: istore #17
    //   440: ldc 'subtype'
    //   442: aload #12
    //   444: invokevirtual equals : (Ljava/lang/Object;)Z
    //   447: ifeq -> 795
    //   450: aload_1
    //   451: astore #11
    //   453: iload #14
    //   455: istore #17
    //   457: aload_1
    //   458: astore #11
    //   460: iload #14
    //   462: istore #17
    //   464: aload #5
    //   466: aload #9
    //   468: getstatic com/android/internal/R$styleable.InputMethod_Subtype : [I
    //   471: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   474: astore #12
    //   476: aload_1
    //   477: astore #11
    //   479: iload #14
    //   481: istore #17
    //   483: aload_1
    //   484: astore #11
    //   486: iload #14
    //   488: istore #17
    //   490: new android/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder
    //   493: astore #8
    //   495: aload_1
    //   496: astore #11
    //   498: iload #14
    //   500: istore #17
    //   502: aload_1
    //   503: astore #11
    //   505: iload #14
    //   507: istore #17
    //   509: aload #8
    //   511: invokespecial <init> : ()V
    //   514: iload #14
    //   516: istore #17
    //   518: iload #14
    //   520: istore #17
    //   522: aload #8
    //   524: aload #12
    //   526: iconst_0
    //   527: iconst_0
    //   528: invokevirtual getResourceId : (II)I
    //   531: invokevirtual setSubtypeNameResId : (I)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   534: astore #11
    //   536: iload #14
    //   538: istore #17
    //   540: iload #14
    //   542: istore #17
    //   544: aload #11
    //   546: aload #12
    //   548: iconst_1
    //   549: iconst_0
    //   550: invokevirtual getResourceId : (II)I
    //   553: invokevirtual setSubtypeIconResId : (I)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   556: astore #11
    //   558: iload #14
    //   560: istore #17
    //   562: iload #14
    //   564: istore #17
    //   566: aload #11
    //   568: aload #12
    //   570: bipush #9
    //   572: invokevirtual getString : (I)Ljava/lang/String;
    //   575: invokevirtual setLanguageTag : (Ljava/lang/String;)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   578: astore #11
    //   580: iload #14
    //   582: istore #17
    //   584: iload #14
    //   586: istore #17
    //   588: aload #11
    //   590: aload #12
    //   592: iconst_2
    //   593: invokevirtual getString : (I)Ljava/lang/String;
    //   596: invokevirtual setSubtypeLocale : (Ljava/lang/String;)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   599: astore #11
    //   601: iload #14
    //   603: istore #17
    //   605: iload #14
    //   607: istore #17
    //   609: aload #11
    //   611: aload #12
    //   613: iconst_3
    //   614: invokevirtual getString : (I)Ljava/lang/String;
    //   617: invokevirtual setSubtypeMode : (Ljava/lang/String;)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   620: astore #11
    //   622: iload #14
    //   624: istore #17
    //   626: iload #14
    //   628: istore #17
    //   630: aload #11
    //   632: aload #12
    //   634: iconst_4
    //   635: invokevirtual getString : (I)Ljava/lang/String;
    //   638: invokevirtual setSubtypeExtraValue : (Ljava/lang/String;)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   641: astore #11
    //   643: iload #14
    //   645: istore #17
    //   647: iload #14
    //   649: istore #17
    //   651: aload #11
    //   653: aload #12
    //   655: iconst_5
    //   656: iconst_0
    //   657: invokevirtual getBoolean : (IZ)Z
    //   660: invokevirtual setIsAuxiliary : (Z)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   663: astore #11
    //   665: iload #14
    //   667: istore #17
    //   669: iload #14
    //   671: istore #17
    //   673: aload #11
    //   675: aload #12
    //   677: bipush #6
    //   679: iconst_0
    //   680: invokevirtual getBoolean : (IZ)Z
    //   683: invokevirtual setOverridesImplicitlyEnabledSubtype : (Z)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   686: astore #11
    //   688: iload #14
    //   690: istore #17
    //   692: iload #14
    //   694: istore #17
    //   696: aload #11
    //   698: aload #12
    //   700: bipush #7
    //   702: iconst_0
    //   703: invokevirtual getInt : (II)I
    //   706: invokevirtual setSubtypeId : (I)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   709: astore #11
    //   711: iload #14
    //   713: istore #17
    //   715: iload #14
    //   717: istore #17
    //   719: aload #11
    //   721: aload #12
    //   723: bipush #8
    //   725: iconst_0
    //   726: invokevirtual getBoolean : (IZ)Z
    //   729: invokevirtual setIsAsciiCapable : (Z)Landroid/view/inputmethod/InputMethodSubtype$InputMethodSubtypeBuilder;
    //   732: astore #11
    //   734: iload #14
    //   736: istore #17
    //   738: iload #14
    //   740: istore #17
    //   742: aload #11
    //   744: invokevirtual build : ()Landroid/view/inputmethod/InputMethodSubtype;
    //   747: astore #11
    //   749: iload #14
    //   751: istore #16
    //   753: iload #14
    //   755: istore #17
    //   757: iload #14
    //   759: istore #17
    //   761: aload #11
    //   763: invokevirtual isAuxiliary : ()Z
    //   766: ifne -> 772
    //   769: iconst_0
    //   770: istore #16
    //   772: iload #16
    //   774: istore #17
    //   776: iload #16
    //   778: istore #17
    //   780: aload #7
    //   782: aload #11
    //   784: invokevirtual add : (Ljava/lang/Object;)Z
    //   787: pop
    //   788: iload #16
    //   790: istore #14
    //   792: goto -> 335
    //   795: iload #14
    //   797: istore #17
    //   799: iload #14
    //   801: istore #17
    //   803: new org/xmlpull/v1/XmlPullParserException
    //   806: astore_1
    //   807: iload #14
    //   809: istore #17
    //   811: iload #14
    //   813: istore #17
    //   815: aload_1
    //   816: ldc 'Meta-data in input-method does not start with subtype tag'
    //   818: invokespecial <init> : (Ljava/lang/String;)V
    //   821: iload #14
    //   823: istore #17
    //   825: iload #14
    //   827: istore #17
    //   829: aload_1
    //   830: athrow
    //   831: goto -> 335
    //   834: aload_2
    //   835: ifnull -> 844
    //   838: aload_2
    //   839: invokeinterface close : ()V
    //   844: aload #7
    //   846: invokevirtual size : ()I
    //   849: ifne -> 858
    //   852: iconst_0
    //   853: istore #14
    //   855: goto -> 858
    //   858: aload_3
    //   859: ifnull -> 969
    //   862: aload_3
    //   863: invokeinterface size : ()I
    //   868: istore #23
    //   870: iconst_0
    //   871: istore #10
    //   873: iload #10
    //   875: iload #23
    //   877: if_icmpge -> 969
    //   880: aload_3
    //   881: iload #10
    //   883: invokeinterface get : (I)Ljava/lang/Object;
    //   888: checkcast android/view/inputmethod/InputMethodSubtype
    //   891: astore_2
    //   892: aload #7
    //   894: aload_2
    //   895: invokevirtual contains : (Ljava/lang/Object;)Z
    //   898: ifne -> 911
    //   901: aload #7
    //   903: aload_2
    //   904: invokevirtual add : (Ljava/lang/Object;)Z
    //   907: pop
    //   908: goto -> 963
    //   911: new java/lang/StringBuilder
    //   914: dup
    //   915: invokespecial <init> : ()V
    //   918: astore_1
    //   919: aload_1
    //   920: ldc 'Duplicated subtype definition found: '
    //   922: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   925: pop
    //   926: aload_1
    //   927: aload_2
    //   928: invokevirtual getLocale : ()Ljava/lang/String;
    //   931: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   934: pop
    //   935: aload_1
    //   936: ldc ', '
    //   938: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   941: pop
    //   942: aload_1
    //   943: aload_2
    //   944: invokevirtual getMode : ()Ljava/lang/String;
    //   947: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   950: pop
    //   951: aload_1
    //   952: invokevirtual toString : ()Ljava/lang/String;
    //   955: astore_1
    //   956: ldc 'InputMethodInfo'
    //   958: aload_1
    //   959: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   962: pop
    //   963: iinc #10, 1
    //   966: goto -> 873
    //   969: aload_0
    //   970: new android/view/inputmethod/InputMethodSubtypeArray
    //   973: dup
    //   974: aload #7
    //   976: invokespecial <init> : (Ljava/util/List;)V
    //   979: putfield mSubtypes : Landroid/view/inputmethod/InputMethodSubtypeArray;
    //   982: aload_0
    //   983: aload #13
    //   985: putfield mSettingsActivityName : Ljava/lang/String;
    //   988: aload_0
    //   989: iload #19
    //   991: putfield mIsDefaultResId : I
    //   994: aload_0
    //   995: iload #14
    //   997: putfield mIsAuxIme : Z
    //   1000: aload_0
    //   1001: iload #20
    //   1003: putfield mSupportsSwitchingToNextInputMethod : Z
    //   1006: aload_0
    //   1007: iload #21
    //   1009: putfield mInlineSuggestionsEnabled : Z
    //   1012: aload_0
    //   1013: iload #18
    //   1015: putfield mIsVrOnly : Z
    //   1018: return
    //   1019: astore_1
    //   1020: goto -> 1173
    //   1023: astore_1
    //   1024: goto -> 1132
    //   1027: iconst_1
    //   1028: istore #14
    //   1030: iconst_1
    //   1031: istore #16
    //   1033: iload #16
    //   1035: istore #17
    //   1037: iload #14
    //   1039: istore #17
    //   1041: new org/xmlpull/v1/XmlPullParserException
    //   1044: astore_1
    //   1045: iload #16
    //   1047: istore #17
    //   1049: iload #14
    //   1051: istore #17
    //   1053: aload_1
    //   1054: ldc_w 'Meta-data does not start with input-method tag'
    //   1057: invokespecial <init> : (Ljava/lang/String;)V
    //   1060: iload #16
    //   1062: istore #17
    //   1064: iload #14
    //   1066: istore #17
    //   1068: aload_1
    //   1069: athrow
    //   1070: iconst_1
    //   1071: istore #14
    //   1073: iconst_1
    //   1074: istore #16
    //   1076: iload #16
    //   1078: istore #17
    //   1080: iload #14
    //   1082: istore #17
    //   1084: new org/xmlpull/v1/XmlPullParserException
    //   1087: astore_1
    //   1088: iload #16
    //   1090: istore #17
    //   1092: iload #14
    //   1094: istore #17
    //   1096: aload_1
    //   1097: ldc_w 'No android.view.im meta-data'
    //   1100: invokespecial <init> : (Ljava/lang/String;)V
    //   1103: iload #16
    //   1105: istore #17
    //   1107: iload #14
    //   1109: istore #17
    //   1111: aload_1
    //   1112: athrow
    //   1113: astore_1
    //   1114: goto -> 1173
    //   1117: astore_1
    //   1118: goto -> 1132
    //   1121: astore_1
    //   1122: aload #6
    //   1124: astore_2
    //   1125: goto -> 1173
    //   1128: astore_1
    //   1129: aload #5
    //   1131: astore_2
    //   1132: new org/xmlpull/v1/XmlPullParserException
    //   1135: astore_1
    //   1136: new java/lang/StringBuilder
    //   1139: astore_3
    //   1140: aload_3
    //   1141: invokespecial <init> : ()V
    //   1144: aload_3
    //   1145: ldc_w 'Unable to create context for: '
    //   1148: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1151: pop
    //   1152: aload_3
    //   1153: aload #4
    //   1155: getfield packageName : Ljava/lang/String;
    //   1158: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1161: pop
    //   1162: aload_1
    //   1163: aload_3
    //   1164: invokevirtual toString : ()Ljava/lang/String;
    //   1167: invokespecial <init> : (Ljava/lang/String;)V
    //   1170: aload_1
    //   1171: athrow
    //   1172: astore_1
    //   1173: aload_2
    //   1174: ifnull -> 1183
    //   1177: aload_2
    //   1178: invokeinterface close : ()V
    //   1183: aload_1
    //   1184: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #155	-> 0
    //   #156	-> 4
    //   #157	-> 9
    //   #158	-> 15
    //   #159	-> 23
    //   #160	-> 23
    //   #161	-> 23
    //   #162	-> 23
    //   #164	-> 28
    //   #165	-> 33
    //   #167	-> 33
    //   #169	-> 33
    //   #170	-> 39
    //   #172	-> 48
    //   #173	-> 57
    //   #178	-> 61
    //   #180	-> 78
    //   #183	-> 90
    //   #187	-> 119
    //   #188	-> 133
    //   #193	-> 149
    //   #195	-> 167
    //   #197	-> 181
    //   #198	-> 213
    //   #200	-> 236
    //   #203	-> 259
    //   #205	-> 282
    //   #207	-> 301
    //   #209	-> 335
    //   #251	-> 384
    //   #247	-> 388
    //   #209	-> 392
    //   #211	-> 398
    //   #212	-> 404
    //   #213	-> 426
    //   #217	-> 450
    //   #219	-> 476
    //   #220	-> 514
    //   #222	-> 536
    //   #224	-> 558
    //   #226	-> 580
    //   #228	-> 601
    //   #230	-> 622
    //   #232	-> 643
    //   #234	-> 665
    //   #237	-> 688
    //   #239	-> 711
    //   #240	-> 734
    //   #241	-> 749
    //   #242	-> 769
    //   #244	-> 772
    //   #245	-> 788
    //   #214	-> 795
    //   #211	-> 831
    //   #209	-> 834
    //   #251	-> 834
    //   #254	-> 844
    //   #255	-> 852
    //   #254	-> 858
    //   #258	-> 858
    //   #259	-> 862
    //   #260	-> 870
    //   #261	-> 880
    //   #262	-> 892
    //   #263	-> 901
    //   #265	-> 911
    //   #266	-> 926
    //   #265	-> 956
    //   #260	-> 963
    //   #270	-> 969
    //   #271	-> 982
    //   #272	-> 988
    //   #273	-> 994
    //   #274	-> 1000
    //   #275	-> 1006
    //   #276	-> 1012
    //   #277	-> 1018
    //   #251	-> 1019
    //   #247	-> 1023
    //   #189	-> 1027
    //   #174	-> 1070
    //   #251	-> 1113
    //   #247	-> 1117
    //   #251	-> 1121
    //   #247	-> 1128
    //   #248	-> 1132
    //   #251	-> 1172
    //   #252	-> 1183
    // Exception table:
    //   from	to	target	type
    //   48	57	1128	android/content/pm/PackageManager$NameNotFoundException
    //   48	57	1128	java/lang/IndexOutOfBoundsException
    //   48	57	1128	java/lang/NumberFormatException
    //   48	57	1121	finally
    //   67	78	1128	android/content/pm/PackageManager$NameNotFoundException
    //   67	78	1128	java/lang/IndexOutOfBoundsException
    //   67	78	1128	java/lang/NumberFormatException
    //   67	78	1121	finally
    //   84	90	1128	android/content/pm/PackageManager$NameNotFoundException
    //   84	90	1128	java/lang/IndexOutOfBoundsException
    //   84	90	1128	java/lang/NumberFormatException
    //   84	90	1121	finally
    //   96	104	1128	android/content/pm/PackageManager$NameNotFoundException
    //   96	104	1128	java/lang/IndexOutOfBoundsException
    //   96	104	1128	java/lang/NumberFormatException
    //   96	104	1121	finally
    //   125	133	1128	android/content/pm/PackageManager$NameNotFoundException
    //   125	133	1128	java/lang/IndexOutOfBoundsException
    //   125	133	1128	java/lang/NumberFormatException
    //   125	133	1121	finally
    //   139	149	1128	android/content/pm/PackageManager$NameNotFoundException
    //   139	149	1128	java/lang/IndexOutOfBoundsException
    //   139	149	1128	java/lang/NumberFormatException
    //   139	149	1121	finally
    //   155	167	1128	android/content/pm/PackageManager$NameNotFoundException
    //   155	167	1128	java/lang/IndexOutOfBoundsException
    //   155	167	1128	java/lang/NumberFormatException
    //   155	167	1121	finally
    //   173	181	1128	android/content/pm/PackageManager$NameNotFoundException
    //   173	181	1128	java/lang/IndexOutOfBoundsException
    //   173	181	1128	java/lang/NumberFormatException
    //   173	181	1121	finally
    //   204	213	1023	android/content/pm/PackageManager$NameNotFoundException
    //   204	213	1023	java/lang/IndexOutOfBoundsException
    //   204	213	1023	java/lang/NumberFormatException
    //   204	213	1019	finally
    //   227	236	1023	android/content/pm/PackageManager$NameNotFoundException
    //   227	236	1023	java/lang/IndexOutOfBoundsException
    //   227	236	1023	java/lang/NumberFormatException
    //   227	236	1019	finally
    //   250	259	1023	android/content/pm/PackageManager$NameNotFoundException
    //   250	259	1023	java/lang/IndexOutOfBoundsException
    //   250	259	1023	java/lang/NumberFormatException
    //   250	259	1019	finally
    //   273	282	1023	android/content/pm/PackageManager$NameNotFoundException
    //   273	282	1023	java/lang/IndexOutOfBoundsException
    //   273	282	1023	java/lang/NumberFormatException
    //   273	282	1019	finally
    //   296	301	1023	android/content/pm/PackageManager$NameNotFoundException
    //   296	301	1023	java/lang/IndexOutOfBoundsException
    //   296	301	1023	java/lang/NumberFormatException
    //   296	301	1019	finally
    //   315	323	1023	android/content/pm/PackageManager$NameNotFoundException
    //   315	323	1023	java/lang/IndexOutOfBoundsException
    //   315	323	1023	java/lang/NumberFormatException
    //   315	323	1019	finally
    //   349	357	1023	android/content/pm/PackageManager$NameNotFoundException
    //   349	357	1023	java/lang/IndexOutOfBoundsException
    //   349	357	1023	java/lang/NumberFormatException
    //   349	357	1019	finally
    //   363	371	388	android/content/pm/PackageManager$NameNotFoundException
    //   363	371	388	java/lang/IndexOutOfBoundsException
    //   363	371	388	java/lang/NumberFormatException
    //   363	371	384	finally
    //   418	426	1023	android/content/pm/PackageManager$NameNotFoundException
    //   418	426	1023	java/lang/IndexOutOfBoundsException
    //   418	426	1023	java/lang/NumberFormatException
    //   418	426	1019	finally
    //   440	450	1023	android/content/pm/PackageManager$NameNotFoundException
    //   440	450	1023	java/lang/IndexOutOfBoundsException
    //   440	450	1023	java/lang/NumberFormatException
    //   440	450	1019	finally
    //   464	476	1023	android/content/pm/PackageManager$NameNotFoundException
    //   464	476	1023	java/lang/IndexOutOfBoundsException
    //   464	476	1023	java/lang/NumberFormatException
    //   464	476	1019	finally
    //   490	495	1023	android/content/pm/PackageManager$NameNotFoundException
    //   490	495	1023	java/lang/IndexOutOfBoundsException
    //   490	495	1023	java/lang/NumberFormatException
    //   490	495	1019	finally
    //   509	514	1023	android/content/pm/PackageManager$NameNotFoundException
    //   509	514	1023	java/lang/IndexOutOfBoundsException
    //   509	514	1023	java/lang/NumberFormatException
    //   509	514	1019	finally
    //   522	536	1117	android/content/pm/PackageManager$NameNotFoundException
    //   522	536	1117	java/lang/IndexOutOfBoundsException
    //   522	536	1117	java/lang/NumberFormatException
    //   522	536	1113	finally
    //   544	558	1117	android/content/pm/PackageManager$NameNotFoundException
    //   544	558	1117	java/lang/IndexOutOfBoundsException
    //   544	558	1117	java/lang/NumberFormatException
    //   544	558	1113	finally
    //   566	580	1117	android/content/pm/PackageManager$NameNotFoundException
    //   566	580	1117	java/lang/IndexOutOfBoundsException
    //   566	580	1117	java/lang/NumberFormatException
    //   566	580	1113	finally
    //   588	601	1117	android/content/pm/PackageManager$NameNotFoundException
    //   588	601	1117	java/lang/IndexOutOfBoundsException
    //   588	601	1117	java/lang/NumberFormatException
    //   588	601	1113	finally
    //   609	622	1117	android/content/pm/PackageManager$NameNotFoundException
    //   609	622	1117	java/lang/IndexOutOfBoundsException
    //   609	622	1117	java/lang/NumberFormatException
    //   609	622	1113	finally
    //   630	643	1117	android/content/pm/PackageManager$NameNotFoundException
    //   630	643	1117	java/lang/IndexOutOfBoundsException
    //   630	643	1117	java/lang/NumberFormatException
    //   630	643	1113	finally
    //   651	665	1117	android/content/pm/PackageManager$NameNotFoundException
    //   651	665	1117	java/lang/IndexOutOfBoundsException
    //   651	665	1117	java/lang/NumberFormatException
    //   651	665	1113	finally
    //   673	688	1117	android/content/pm/PackageManager$NameNotFoundException
    //   673	688	1117	java/lang/IndexOutOfBoundsException
    //   673	688	1117	java/lang/NumberFormatException
    //   673	688	1113	finally
    //   696	711	1117	android/content/pm/PackageManager$NameNotFoundException
    //   696	711	1117	java/lang/IndexOutOfBoundsException
    //   696	711	1117	java/lang/NumberFormatException
    //   696	711	1113	finally
    //   719	734	1117	android/content/pm/PackageManager$NameNotFoundException
    //   719	734	1117	java/lang/IndexOutOfBoundsException
    //   719	734	1117	java/lang/NumberFormatException
    //   719	734	1113	finally
    //   742	749	1117	android/content/pm/PackageManager$NameNotFoundException
    //   742	749	1117	java/lang/IndexOutOfBoundsException
    //   742	749	1117	java/lang/NumberFormatException
    //   742	749	1113	finally
    //   761	769	1117	android/content/pm/PackageManager$NameNotFoundException
    //   761	769	1117	java/lang/IndexOutOfBoundsException
    //   761	769	1117	java/lang/NumberFormatException
    //   761	769	1113	finally
    //   780	788	1117	android/content/pm/PackageManager$NameNotFoundException
    //   780	788	1117	java/lang/IndexOutOfBoundsException
    //   780	788	1117	java/lang/NumberFormatException
    //   780	788	1113	finally
    //   803	807	1117	android/content/pm/PackageManager$NameNotFoundException
    //   803	807	1117	java/lang/IndexOutOfBoundsException
    //   803	807	1117	java/lang/NumberFormatException
    //   803	807	1113	finally
    //   815	821	1117	android/content/pm/PackageManager$NameNotFoundException
    //   815	821	1117	java/lang/IndexOutOfBoundsException
    //   815	821	1117	java/lang/NumberFormatException
    //   815	821	1113	finally
    //   829	831	1117	android/content/pm/PackageManager$NameNotFoundException
    //   829	831	1117	java/lang/IndexOutOfBoundsException
    //   829	831	1117	java/lang/NumberFormatException
    //   829	831	1113	finally
    //   1041	1045	1117	android/content/pm/PackageManager$NameNotFoundException
    //   1041	1045	1117	java/lang/IndexOutOfBoundsException
    //   1041	1045	1117	java/lang/NumberFormatException
    //   1041	1045	1113	finally
    //   1053	1060	1117	android/content/pm/PackageManager$NameNotFoundException
    //   1053	1060	1117	java/lang/IndexOutOfBoundsException
    //   1053	1060	1117	java/lang/NumberFormatException
    //   1053	1060	1113	finally
    //   1068	1070	1117	android/content/pm/PackageManager$NameNotFoundException
    //   1068	1070	1117	java/lang/IndexOutOfBoundsException
    //   1068	1070	1117	java/lang/NumberFormatException
    //   1068	1070	1113	finally
    //   1084	1088	1117	android/content/pm/PackageManager$NameNotFoundException
    //   1084	1088	1117	java/lang/IndexOutOfBoundsException
    //   1084	1088	1117	java/lang/NumberFormatException
    //   1084	1088	1113	finally
    //   1096	1103	1117	android/content/pm/PackageManager$NameNotFoundException
    //   1096	1103	1117	java/lang/IndexOutOfBoundsException
    //   1096	1103	1117	java/lang/NumberFormatException
    //   1096	1103	1113	finally
    //   1111	1113	1117	android/content/pm/PackageManager$NameNotFoundException
    //   1111	1113	1117	java/lang/IndexOutOfBoundsException
    //   1111	1113	1117	java/lang/NumberFormatException
    //   1111	1113	1113	finally
    //   1132	1172	1172	finally
  }
  
  InputMethodInfo(Parcel paramParcel) {
    boolean bool2;
    this.mId = paramParcel.readString();
    this.mSettingsActivityName = paramParcel.readString();
    this.mIsDefaultResId = paramParcel.readInt();
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsAuxIme = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mSupportsSwitchingToNextInputMethod = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mInlineSuggestionsEnabled = bool2;
    this.mIsVrOnly = paramParcel.readBoolean();
    this.mService = (ResolveInfo)ResolveInfo.CREATOR.createFromParcel(paramParcel);
    this.mSubtypes = new InputMethodSubtypeArray(paramParcel);
    this.mForceDefault = false;
  }
  
  public InputMethodInfo(String paramString1, String paramString2, CharSequence paramCharSequence, String paramString3) {
    this(buildDummyResolveInfo(paramString1, paramString2, paramCharSequence), false, paramString3, null, 0, false, true, false, false);
  }
  
  public InputMethodInfo(ResolveInfo paramResolveInfo, boolean paramBoolean1, String paramString, List<InputMethodSubtype> paramList, int paramInt, boolean paramBoolean2) {
    this(paramResolveInfo, paramBoolean1, paramString, paramList, paramInt, paramBoolean2, true, false, false);
  }
  
  public InputMethodInfo(ResolveInfo paramResolveInfo, boolean paramBoolean1, String paramString, List<InputMethodSubtype> paramList, int paramInt, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4) {
    this(paramResolveInfo, paramBoolean1, paramString, paramList, paramInt, paramBoolean2, paramBoolean3, false, paramBoolean4);
  }
  
  public InputMethodInfo(ResolveInfo paramResolveInfo, boolean paramBoolean1, String paramString, List<InputMethodSubtype> paramList, int paramInt, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5) {
    ServiceInfo serviceInfo = paramResolveInfo.serviceInfo;
    this.mService = paramResolveInfo;
    this.mId = (new ComponentName(serviceInfo.packageName, serviceInfo.name)).flattenToShortString();
    this.mSettingsActivityName = paramString;
    this.mIsDefaultResId = paramInt;
    this.mIsAuxIme = paramBoolean1;
    this.mSubtypes = new InputMethodSubtypeArray(paramList);
    this.mForceDefault = paramBoolean2;
    this.mSupportsSwitchingToNextInputMethod = paramBoolean3;
    this.mInlineSuggestionsEnabled = paramBoolean4;
    this.mIsVrOnly = paramBoolean5;
  }
  
  private static ResolveInfo buildDummyResolveInfo(String paramString1, String paramString2, CharSequence paramCharSequence) {
    ResolveInfo resolveInfo = new ResolveInfo();
    ServiceInfo serviceInfo = new ServiceInfo();
    ApplicationInfo applicationInfo = new ApplicationInfo();
    applicationInfo.packageName = paramString1;
    applicationInfo.enabled = true;
    serviceInfo.applicationInfo = applicationInfo;
    serviceInfo.enabled = true;
    serviceInfo.packageName = paramString1;
    serviceInfo.name = paramString2;
    serviceInfo.exported = true;
    serviceInfo.nonLocalizedLabel = paramCharSequence;
    resolveInfo.serviceInfo = serviceInfo;
    return resolveInfo;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public String getPackageName() {
    return this.mService.serviceInfo.packageName;
  }
  
  public String getServiceName() {
    return this.mService.serviceInfo.name;
  }
  
  public ServiceInfo getServiceInfo() {
    return this.mService.serviceInfo;
  }
  
  public ComponentName getComponent() {
    return new ComponentName(this.mService.serviceInfo.packageName, this.mService.serviceInfo.name);
  }
  
  public CharSequence loadLabel(PackageManager paramPackageManager) {
    return this.mService.loadLabel(paramPackageManager);
  }
  
  public Drawable loadIcon(PackageManager paramPackageManager) {
    return this.mService.loadIcon(paramPackageManager);
  }
  
  public String getSettingsActivity() {
    return this.mSettingsActivityName;
  }
  
  public boolean isVrOnly() {
    return this.mIsVrOnly;
  }
  
  public int getSubtypeCount() {
    return this.mSubtypes.getCount();
  }
  
  public InputMethodSubtype getSubtypeAt(int paramInt) {
    return this.mSubtypes.get(paramInt);
  }
  
  public int getIsDefaultResourceId() {
    return this.mIsDefaultResId;
  }
  
  public boolean isDefault(Context paramContext) {
    if (this.mForceDefault)
      return true; 
    try {
      if (getIsDefaultResourceId() == 0)
        return false; 
      Resources resources = paramContext.createPackageContext(getPackageName(), 0).getResources();
      return resources.getBoolean(getIsDefaultResourceId());
    } catch (android.content.pm.PackageManager.NameNotFoundException|android.content.res.Resources.NotFoundException nameNotFoundException) {
      return false;
    } 
  }
  
  public void dump(Printer paramPrinter, String paramString) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString);
    stringBuilder2.append("mId=");
    stringBuilder2.append(this.mId);
    stringBuilder2.append(" mSettingsActivityName=");
    stringBuilder2.append(this.mSettingsActivityName);
    stringBuilder2.append(" mIsVrOnly=");
    stringBuilder2.append(this.mIsVrOnly);
    stringBuilder2.append(" mSupportsSwitchingToNextInputMethod=");
    stringBuilder2.append(this.mSupportsSwitchingToNextInputMethod);
    stringBuilder2.append(" mInlineSuggestionsEnabled=");
    stringBuilder2.append(this.mInlineSuggestionsEnabled);
    paramPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString);
    stringBuilder2.append("mIsDefaultResId=0x");
    int i = this.mIsDefaultResId;
    stringBuilder2.append(Integer.toHexString(i));
    String str = stringBuilder2.toString();
    paramPrinter.println(str);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString);
    stringBuilder1.append("Service:");
    paramPrinter.println(stringBuilder1.toString());
    ResolveInfo resolveInfo = this.mService;
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString);
    stringBuilder1.append("  ");
    resolveInfo.dump(paramPrinter, stringBuilder1.toString());
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InputMethodInfo{");
    stringBuilder.append(this.mId);
    stringBuilder.append(", settings: ");
    stringBuilder.append(this.mSettingsActivityName);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == this)
      return true; 
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof InputMethodInfo))
      return false; 
    paramObject = paramObject;
    return this.mId.equals(((InputMethodInfo)paramObject).mId);
  }
  
  public int hashCode() {
    return this.mId.hashCode();
  }
  
  public boolean isSystem() {
    int i = this.mService.serviceInfo.applicationInfo.flags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isAuxiliaryIme() {
    return this.mIsAuxIme;
  }
  
  public boolean supportsSwitchingToNextInputMethod() {
    return this.mSupportsSwitchingToNextInputMethod;
  }
  
  public boolean isInlineSuggestionsEnabled() {
    return this.mInlineSuggestionsEnabled;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    paramParcel.writeString(this.mSettingsActivityName);
    paramParcel.writeInt(this.mIsDefaultResId);
    paramParcel.writeInt(this.mIsAuxIme);
    paramParcel.writeInt(this.mSupportsSwitchingToNextInputMethod);
    paramParcel.writeInt(this.mInlineSuggestionsEnabled);
    paramParcel.writeBoolean(this.mIsVrOnly);
    this.mService.writeToParcel(paramParcel, paramInt);
    this.mSubtypes.writeToParcel(paramParcel);
  }
  
  public static final Parcelable.Creator<InputMethodInfo> CREATOR = new Parcelable.Creator<InputMethodInfo>() {
      public InputMethodInfo createFromParcel(Parcel param1Parcel) {
        return new InputMethodInfo(param1Parcel);
      }
      
      public InputMethodInfo[] newArray(int param1Int) {
        return new InputMethodInfo[param1Int];
      }
    };
  
  static final String TAG = "InputMethodInfo";
  
  private final boolean mForceDefault;
  
  final String mId;
  
  private final boolean mInlineSuggestionsEnabled;
  
  private final boolean mIsAuxIme;
  
  final int mIsDefaultResId;
  
  final boolean mIsVrOnly;
  
  final ResolveInfo mService;
  
  final String mSettingsActivityName;
  
  private final InputMethodSubtypeArray mSubtypes;
  
  private final boolean mSupportsSwitchingToNextInputMethod;
  
  public int describeContents() {
    return 0;
  }
}
