package android.view.inputmethod;

import android.app.AppGlobals;
import android.content.pm.OplusPackageManager;
import android.os.Binder;
import android.os.Debug;
import android.os.Process;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Printer;
import android.view.View;
import com.oplus.orms.OplusResourceManager;

public class OplusInputMethodManager {
  static {
    boolean bool = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    boolean bool1 = true;
    if (!bool) {
      bool = false;
    } else {
      bool = true;
    } 
    DEBUG_IME_ACTIVE = bool;
    bool = bool1;
    if (!DEBUG)
      bool = false; 
    DEBUG_TOGGLE_SOFT = bool;
    DEBUG_PANIC = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    mOrmsManager = null;
  }
  
  protected int mCurPid = 0;
  
  protected int mCurUid = 0;
  
  protected boolean mInitCompatibilityFlag = false;
  
  protected boolean mApplyCompatibilityPatch = false;
  
  public static boolean DEBUG = false;
  
  public static boolean DEBUG_IME_ACTIVE = false;
  
  static boolean DEBUG_PANIC = false;
  
  public static boolean DEBUG_TOGGLE_SOFT = false;
  
  protected static final int IME_SKIP_TMP_DETACH = 686;
  
  static final String TAG = "InputMethodManager";
  
  private static OplusResourceManager mOrmsManager;
  
  public OplusInputMethodManager() {
    getDebugFlag();
    this.mCurPid = Process.myPid();
    this.mCurUid = Process.myUid();
  }
  
  public static boolean getDebugFlag() {
    boolean bool = DEBUG;
    boolean bool1 = false;
    if (bool || SystemProperties.getBoolean("persist.sys.assert.imelog", false))
      bool1 = true; 
    DEBUG = bool1;
    return bool1;
  }
  
  public void printCallPidAndUid(String paramString) {
    if (DEBUG_PANIC) {
      int i = Binder.getCallingUid();
      int j = Binder.getCallingPid();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("call from uid:");
      stringBuilder.append(i);
      stringBuilder.append(", pid:");
      stringBuilder.append(j);
      stringBuilder.append(" caller:");
      stringBuilder.append(Debug.getCallers(8));
      String str = stringBuilder.toString();
      Log.d("InputMethodManager", str);
    } 
  }
  
  public void printCallPidAndUid(String paramString, int paramInt1, int paramInt2) {
    if (DEBUG_PANIC) {
      int i = Binder.getCallingUid();
      int j = Binder.getCallingPid();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("call from uid:");
      stringBuilder.append(i);
      stringBuilder.append(", pid:");
      stringBuilder.append(j);
      stringBuilder.append(", showFlags:");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", hideFlags:");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" caller:");
      stringBuilder.append(Debug.getCallers(8));
      String str = stringBuilder.toString();
      Log.d("InputMethodManager", str);
    } 
  }
  
  public int adjustFlag(int paramInt1, int paramInt2) {
    OplusPackageManager oplusPackageManager = new OplusPackageManager();
    boolean bool = oplusPackageManager.inCptWhiteList(701, AppGlobals.getInitialPackage());
    if (paramInt1 == paramInt2 && bool)
      return 0; 
    return paramInt1;
  }
  
  public void setHypnusManager() {
    // Byte code:
    //   0: getstatic android/view/inputmethod/OplusInputMethodManager.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   3: ifnonnull -> 20
    //   6: ldc android/view/inputmethod/OplusInputMethodManager
    //   8: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   11: astore_1
    //   12: aload_1
    //   13: putstatic android/view/inputmethod/OplusInputMethodManager.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   16: aload_1
    //   17: ifnull -> 41
    //   20: getstatic android/view/inputmethod/OplusInputMethodManager.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   23: new com/oplus/orms/info/OrmsSaParam
    //   26: dup
    //   27: ldc ''
    //   29: ldc 'ORMS_ACTION_INPUT_METHOD_BOOST'
    //   31: sipush #400
    //   34: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   37: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   40: pop2
    //   41: getstatic android/view/inputmethod/OplusInputMethodManager.DEBUG_PANIC : Z
    //   44: ifeq -> 53
    //   47: aload_0
    //   48: ldc 'showSoftInput'
    //   50: invokevirtual printCallPidAndUid : (Ljava/lang/String;)V
    //   53: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #109	-> 0
    //   #110	-> 20
    //   #114	-> 41
    //   #115	-> 47
    //   #117	-> 53
  }
  
  public void extendInputMethodCompatible(View paramView) {}
  
  public boolean dynamicallyConfigImsLogTag(Printer paramPrinter, String[] paramArrayOfString) {
    if (paramArrayOfString.length != 3)
      return false; 
    String str = paramArrayOfString[0];
    if (!"log".equals(str))
      return false; 
    str = paramArrayOfString[1];
    boolean bool = "1".equals(paramArrayOfString[2]);
    if ("all".equals(str) || "client".equals(str))
      DEBUG = bool; 
    return true;
  }
}
