package android.view.inputmethod;

import android.annotation.NonNull;
import android.app.ActivityThread;
import android.os.Bundle;
import android.os.IBinder;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.inline.InlinePresentationSpec;
import com.android.internal.util.AnnotationValidations;
import com.android.internal.util.Preconditions;
import com.android.internal.widget.InlinePresentationStyleUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class InlineSuggestionsRequest implements Parcelable {
  public void setHostInputToken(IBinder paramIBinder) {
    this.mHostInputToken = paramIBinder;
  }
  
  private boolean extrasEquals(Bundle paramBundle) {
    return InlinePresentationStyleUtils.bundleEquals(this.mExtras, paramBundle);
  }
  
  private void parcelHostInputToken(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.mHostInputToken);
  }
  
  private IBinder unparcelHostInputToken(Parcel paramParcel) {
    return paramParcel.readStrongBinder();
  }
  
  public void setHostDisplayId(int paramInt) {
    this.mHostDisplayId = paramInt;
  }
  
  private void onConstructed() {
    boolean bool = this.mInlinePresentationSpecs.isEmpty();
    boolean bool1 = true;
    Preconditions.checkState(bool ^ true);
    if (this.mMaxSuggestionCount < this.mInlinePresentationSpecs.size())
      bool1 = false; 
    Preconditions.checkState(bool1);
  }
  
  public void filterContentTypes() {
    InlinePresentationStyleUtils.filterContentTypes(this.mExtras);
    for (byte b = 0; b < this.mInlinePresentationSpecs.size(); b++)
      ((InlinePresentationSpec)this.mInlinePresentationSpecs.get(b)).filterContentTypes(); 
  }
  
  private static int defaultMaxSuggestionCount() {
    return Integer.MAX_VALUE;
  }
  
  private static String defaultHostPackageName() {
    return ActivityThread.currentPackageName();
  }
  
  private static LocaleList defaultSupportedLocales() {
    return LocaleList.getDefault();
  }
  
  private static IBinder defaultHostInputToken() {
    return null;
  }
  
  private static int defaultHostDisplayId() {
    return -1;
  }
  
  private static Bundle defaultExtras() {
    return Bundle.EMPTY;
  }
  
  class BaseBuilder {
    abstract InlineSuggestionsRequest.Builder setHostDisplayId(int param1Int);
    
    abstract InlineSuggestionsRequest.Builder setHostInputToken(IBinder param1IBinder);
    
    abstract InlineSuggestionsRequest.Builder setHostPackageName(String param1String);
    
    abstract InlineSuggestionsRequest.Builder setInlinePresentationSpecs(List<InlinePresentationSpec> param1List);
  }
  
  InlineSuggestionsRequest(int paramInt1, List<InlinePresentationSpec> paramList, String paramString, LocaleList paramLocaleList, Bundle paramBundle, IBinder paramIBinder, int paramInt2) {
    this.mMaxSuggestionCount = paramInt1;
    this.mInlinePresentationSpecs = paramList;
    AnnotationValidations.validate(NonNull.class, null, paramList);
    this.mHostPackageName = paramString;
    AnnotationValidations.validate(NonNull.class, null, paramString);
    this.mSupportedLocales = paramLocaleList;
    AnnotationValidations.validate(NonNull.class, null, paramLocaleList);
    this.mExtras = paramBundle;
    AnnotationValidations.validate(NonNull.class, null, paramBundle);
    this.mHostInputToken = paramIBinder;
    this.mHostDisplayId = paramInt2;
    onConstructed();
  }
  
  public int getMaxSuggestionCount() {
    return this.mMaxSuggestionCount;
  }
  
  public List<InlinePresentationSpec> getInlinePresentationSpecs() {
    return this.mInlinePresentationSpecs;
  }
  
  public String getHostPackageName() {
    return this.mHostPackageName;
  }
  
  public LocaleList getSupportedLocales() {
    return this.mSupportedLocales;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public IBinder getHostInputToken() {
    return this.mHostInputToken;
  }
  
  public int getHostDisplayId() {
    return this.mHostDisplayId;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InlineSuggestionsRequest { maxSuggestionCount = ");
    stringBuilder.append(this.mMaxSuggestionCount);
    stringBuilder.append(", inlinePresentationSpecs = ");
    stringBuilder.append(this.mInlinePresentationSpecs);
    stringBuilder.append(", hostPackageName = ");
    stringBuilder.append(this.mHostPackageName);
    stringBuilder.append(", supportedLocales = ");
    stringBuilder.append(this.mSupportedLocales);
    stringBuilder.append(", extras = ");
    stringBuilder.append(this.mExtras);
    stringBuilder.append(", hostInputToken = ");
    stringBuilder.append(this.mHostInputToken);
    stringBuilder.append(", hostDisplayId = ");
    stringBuilder.append(this.mHostDisplayId);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mMaxSuggestionCount == ((InlineSuggestionsRequest)paramObject).mMaxSuggestionCount) {
      List<InlinePresentationSpec> list1 = this.mInlinePresentationSpecs, list2 = ((InlineSuggestionsRequest)paramObject).mInlinePresentationSpecs;
      if (Objects.equals(list1, list2)) {
        String str1 = this.mHostPackageName, str2 = ((InlineSuggestionsRequest)paramObject).mHostPackageName;
        if (Objects.equals(str1, str2)) {
          LocaleList localeList2 = this.mSupportedLocales, localeList1 = ((InlineSuggestionsRequest)paramObject).mSupportedLocales;
          if (Objects.equals(localeList2, localeList1)) {
            Bundle bundle = ((InlineSuggestionsRequest)paramObject).mExtras;
            if (extrasEquals(bundle)) {
              IBinder iBinder2 = this.mHostInputToken, iBinder1 = ((InlineSuggestionsRequest)paramObject).mHostInputToken;
              if (Objects.equals(iBinder2, iBinder1) && this.mHostDisplayId == ((InlineSuggestionsRequest)paramObject).mHostDisplayId)
                return null; 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mMaxSuggestionCount;
    int j = Objects.hashCode(this.mInlinePresentationSpecs);
    int k = Objects.hashCode(this.mHostPackageName);
    int m = Objects.hashCode(this.mSupportedLocales);
    int n = Objects.hashCode(this.mExtras);
    int i1 = Objects.hashCode(this.mHostInputToken);
    int i2 = this.mHostDisplayId;
    return ((((((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b = 0;
    if (this.mHostInputToken != null)
      b = (byte)(0x0 | 0x20); 
    paramParcel.writeByte(b);
    paramParcel.writeInt(this.mMaxSuggestionCount);
    paramParcel.writeParcelableList(this.mInlinePresentationSpecs, paramInt);
    paramParcel.writeString(this.mHostPackageName);
    paramParcel.writeTypedObject((Parcelable)this.mSupportedLocales, paramInt);
    paramParcel.writeBundle(this.mExtras);
    parcelHostInputToken(paramParcel, paramInt);
    paramParcel.writeInt(this.mHostDisplayId);
  }
  
  public int describeContents() {
    return 0;
  }
  
  InlineSuggestionsRequest(Parcel paramParcel) {
    paramParcel.readByte();
    int i = paramParcel.readInt();
    ArrayList<InlinePresentationSpec> arrayList = new ArrayList();
    paramParcel.readParcelableList(arrayList, InlinePresentationSpec.class.getClassLoader());
    String str = paramParcel.readString();
    LocaleList localeList = (LocaleList)paramParcel.readTypedObject(LocaleList.CREATOR);
    Bundle bundle = paramParcel.readBundle();
    IBinder iBinder = unparcelHostInputToken(paramParcel);
    int j = paramParcel.readInt();
    this.mMaxSuggestionCount = i;
    this.mInlinePresentationSpecs = arrayList;
    AnnotationValidations.validate(NonNull.class, null, arrayList);
    this.mHostPackageName = str;
    AnnotationValidations.validate(NonNull.class, null, str);
    this.mSupportedLocales = localeList;
    AnnotationValidations.validate(NonNull.class, null, localeList);
    this.mExtras = bundle;
    AnnotationValidations.validate(NonNull.class, null, bundle);
    this.mHostInputToken = iBinder;
    this.mHostDisplayId = j;
    onConstructed();
  }
  
  public static final Parcelable.Creator<InlineSuggestionsRequest> CREATOR = new Parcelable.Creator<InlineSuggestionsRequest>() {
      public InlineSuggestionsRequest[] newArray(int param1Int) {
        return new InlineSuggestionsRequest[param1Int];
      }
      
      public InlineSuggestionsRequest createFromParcel(Parcel param1Parcel) {
        return new InlineSuggestionsRequest(param1Parcel);
      }
    };
  
  public static final int SUGGESTION_COUNT_UNLIMITED = 2147483647;
  
  private Bundle mExtras;
  
  private int mHostDisplayId;
  
  private IBinder mHostInputToken;
  
  private String mHostPackageName;
  
  private final List<InlinePresentationSpec> mInlinePresentationSpecs;
  
  private final int mMaxSuggestionCount;
  
  private LocaleList mSupportedLocales;
  
  public static final class Builder extends BaseBuilder {
    private long mBuilderFieldsSet = 0L;
    
    private Bundle mExtras;
    
    private int mHostDisplayId;
    
    private IBinder mHostInputToken;
    
    private String mHostPackageName;
    
    private List<InlinePresentationSpec> mInlinePresentationSpecs;
    
    private int mMaxSuggestionCount;
    
    private LocaleList mSupportedLocales;
    
    public Builder(List<InlinePresentationSpec> param1List) {
      this.mInlinePresentationSpecs = param1List;
      AnnotationValidations.validate(NonNull.class, null, param1List);
    }
    
    public Builder setMaxSuggestionCount(int param1Int) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x1L;
      this.mMaxSuggestionCount = param1Int;
      return this;
    }
    
    public Builder setInlinePresentationSpecs(List<InlinePresentationSpec> param1List) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x2L;
      this.mInlinePresentationSpecs = param1List;
      return this;
    }
    
    public Builder addInlinePresentationSpecs(InlinePresentationSpec param1InlinePresentationSpec) {
      if (this.mInlinePresentationSpecs == null)
        setInlinePresentationSpecs(new ArrayList<>()); 
      this.mInlinePresentationSpecs.add(param1InlinePresentationSpec);
      return this;
    }
    
    Builder setHostPackageName(String param1String) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x4L;
      this.mHostPackageName = param1String;
      return this;
    }
    
    public Builder setSupportedLocales(LocaleList param1LocaleList) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x8L;
      this.mSupportedLocales = param1LocaleList;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x10L;
      this.mExtras = param1Bundle;
      return this;
    }
    
    Builder setHostInputToken(IBinder param1IBinder) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x20L;
      this.mHostInputToken = param1IBinder;
      return this;
    }
    
    Builder setHostDisplayId(int param1Int) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x40L;
      this.mHostDisplayId = param1Int;
      return this;
    }
    
    public InlineSuggestionsRequest build() {
      checkNotUsed();
      long l = this.mBuilderFieldsSet | 0x80L;
      if ((l & 0x1L) == 0L)
        this.mMaxSuggestionCount = InlineSuggestionsRequest.defaultMaxSuggestionCount(); 
      if ((this.mBuilderFieldsSet & 0x4L) == 0L)
        this.mHostPackageName = InlineSuggestionsRequest.defaultHostPackageName(); 
      if ((this.mBuilderFieldsSet & 0x8L) == 0L)
        this.mSupportedLocales = InlineSuggestionsRequest.defaultSupportedLocales(); 
      if ((this.mBuilderFieldsSet & 0x10L) == 0L)
        this.mExtras = InlineSuggestionsRequest.defaultExtras(); 
      if ((this.mBuilderFieldsSet & 0x20L) == 0L)
        this.mHostInputToken = InlineSuggestionsRequest.defaultHostInputToken(); 
      if ((this.mBuilderFieldsSet & 0x40L) == 0L)
        this.mHostDisplayId = InlineSuggestionsRequest.defaultHostDisplayId(); 
      return new InlineSuggestionsRequest(this.mMaxSuggestionCount, this.mInlinePresentationSpecs, this.mHostPackageName, this.mSupportedLocales, this.mExtras, this.mHostInputToken, this.mHostDisplayId);
    }
    
    private void checkNotUsed() {
      if ((this.mBuilderFieldsSet & 0x80L) == 0L)
        return; 
      throw new IllegalStateException("This Builder should not be reused. Use a new Builder instance instead");
    }
  }
  
  @Deprecated
  private void __metadata() {}
}
