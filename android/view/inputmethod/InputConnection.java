package android.view.inputmethod;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

public interface InputConnection {
  public static final int CURSOR_UPDATE_IMMEDIATE = 1;
  
  public static final int CURSOR_UPDATE_MONITOR = 2;
  
  public static final int GET_EXTRACTED_TEXT_MONITOR = 1;
  
  public static final int GET_TEXT_WITH_STYLES = 1;
  
  public static final int INPUT_CONTENT_GRANT_READ_URI_PERMISSION = 1;
  
  boolean beginBatchEdit();
  
  boolean clearMetaKeyStates(int paramInt);
  
  void closeConnection();
  
  boolean commitCompletion(CompletionInfo paramCompletionInfo);
  
  boolean commitContent(InputContentInfo paramInputContentInfo, int paramInt, Bundle paramBundle);
  
  boolean commitCorrection(CorrectionInfo paramCorrectionInfo);
  
  boolean commitText(CharSequence paramCharSequence, int paramInt);
  
  boolean deleteSurroundingText(int paramInt1, int paramInt2);
  
  boolean deleteSurroundingTextInCodePoints(int paramInt1, int paramInt2);
  
  boolean endBatchEdit();
  
  boolean finishComposingText();
  
  int getCursorCapsMode(int paramInt);
  
  ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt);
  
  Handler getHandler();
  
  CharSequence getSelectedText(int paramInt);
  
  CharSequence getTextAfterCursor(int paramInt1, int paramInt2);
  
  CharSequence getTextBeforeCursor(int paramInt1, int paramInt2);
  
  boolean performContextMenuAction(int paramInt);
  
  boolean performEditorAction(int paramInt);
  
  boolean performPrivateCommand(String paramString, Bundle paramBundle);
  
  boolean reportFullscreenMode(boolean paramBoolean);
  
  boolean requestCursorUpdates(int paramInt);
  
  boolean sendKeyEvent(KeyEvent paramKeyEvent);
  
  boolean setComposingRegion(int paramInt1, int paramInt2);
  
  boolean setComposingText(CharSequence paramCharSequence, int paramInt);
  
  boolean setSelection(int paramInt1, int paramInt2);
}
