package android.view.inputmethod;

import android.os.Bundle;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Printer;
import android.view.autofill.AutofillId;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;

public class EditorInfo implements InputType, Parcelable {
  public int inputType = 0;
  
  public int imeOptions = 0;
  
  public String privateImeOptions = null;
  
  public CharSequence actionLabel = null;
  
  public int actionId = 0;
  
  public int initialSelStart = -1;
  
  public int initialSelEnd = -1;
  
  public int initialCapsMode = 0;
  
  public LocaleList hintLocales = null;
  
  public String[] contentMimeTypes = null;
  
  public UserHandle targetInputMethodUser = null;
  
  private InitialSurroundingText mInitialSurroundingText = new InitialSurroundingText();
  
  public void setInitialSurroundingText(CharSequence paramCharSequence) {
    setInitialSurroundingSubText(paramCharSequence, 0);
  }
  
  public void setInitialSurroundingSubText(CharSequence paramCharSequence, int paramInt) {
    paramCharSequence = Editable.Factory.getInstance().newEditable(paramCharSequence);
    Objects.requireNonNull(paramCharSequence);
    int i = this.initialSelStart, j = this.initialSelEnd;
    if (i > j) {
      i = j - paramInt;
    } else {
      i -= paramInt;
    } 
    j = this.initialSelStart;
    int k = this.initialSelEnd;
    if (j > k) {
      j -= paramInt;
    } else {
      j = k - paramInt;
    } 
    k = paramCharSequence.length();
    if (paramInt < 0 || i < 0 || j > k) {
      this.mInitialSurroundingText = new InitialSurroundingText();
      return;
    } 
    if (isPasswordInputType(this.inputType)) {
      this.mInitialSurroundingText = new InitialSurroundingText();
      return;
    } 
    if (k <= 2048) {
      this.mInitialSurroundingText = new InitialSurroundingText(paramCharSequence, i, j);
      return;
    } 
    trimLongSurroundingText(paramCharSequence, i, j);
  }
  
  private void trimLongSurroundingText(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    int j, i = paramInt2 - paramInt1;
    if (i > 1024) {
      j = 0;
    } else {
      j = i;
    } 
    int k = paramCharSequence.length();
    int m = 2048 - j;
    int n = (int)(m * 0.8D);
    n = Math.min(paramInt1, n);
    n = Math.min(k - paramInt2, m - n);
    int i1 = Math.min(paramInt1, m - n);
    int i2 = paramInt1 - i1;
    k = i1;
    m = i2;
    if (isCutOnSurrogate(paramCharSequence, paramInt1 - i1, 0)) {
      m = i2 + 1;
      k = i1 - 1;
    } 
    paramInt1 = n;
    if (isCutOnSurrogate(paramCharSequence, paramInt2 + n - 1, 1))
      paramInt1 = n - 1; 
    if (j != i) {
      CharSequence charSequence = paramCharSequence.subSequence(m, m + k);
      paramCharSequence = paramCharSequence.subSequence(paramInt2, paramInt2 + paramInt1);
      paramCharSequence = TextUtils.concat(new CharSequence[] { charSequence, paramCharSequence });
    } else {
      paramCharSequence = paramCharSequence.subSequence(m, m + k + j + paramInt1);
    } 
    paramInt1 = 0 + k;
    this.mInitialSurroundingText = new InitialSurroundingText(paramCharSequence, paramInt1, paramInt1 + j);
  }
  
  public CharSequence getInitialTextBeforeCursor(int paramInt1, int paramInt2) {
    return this.mInitialSurroundingText.getInitialTextBeforeCursor(paramInt1, paramInt2);
  }
  
  public CharSequence getInitialSelectedText(int paramInt) {
    int i = this.initialSelStart, j = this.initialSelEnd;
    if (i <= j)
      j = i; 
    i = this.initialSelStart;
    int k = this.initialSelEnd;
    if (i <= k)
      i = k; 
    if (this.initialSelStart >= 0 && this.initialSelEnd >= 0) {
      InitialSurroundingText initialSurroundingText = this.mInitialSurroundingText;
      if (initialSurroundingText.getSelectionLength() == i - j)
        return this.mInitialSurroundingText.getInitialSelectedText(paramInt); 
    } 
    return null;
  }
  
  public CharSequence getInitialTextAfterCursor(int paramInt1, int paramInt2) {
    return this.mInitialSurroundingText.getInitialTextAfterCursor(paramInt1, paramInt2);
  }
  
  private static boolean isCutOnSurrogate(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      if (paramInt2 != 1)
        return false; 
      return Character.isHighSurrogate(paramCharSequence.charAt(paramInt1));
    } 
    return Character.isLowSurrogate(paramCharSequence.charAt(paramInt1));
  }
  
  private static boolean isPasswordInputType(int paramInt) {
    paramInt &= 0xFFF;
    return (paramInt == 129 || paramInt == 225 || paramInt == 18);
  }
  
  public final void makeCompatible(int paramInt) {
    if (paramInt < 11) {
      paramInt = this.inputType;
      int i = paramInt & 0xFFF;
      if (i != 2 && i != 18) {
        if (i != 209) {
          if (i == 225)
            this.inputType = paramInt & 0xFFF000 | 0x81; 
        } else {
          this.inputType = paramInt & 0xFFF000 | 0x21;
        } 
      } else {
        this.inputType = this.inputType & 0xFFF000 | 0x2;
      } 
    } 
  }
  
  public void dump(Printer paramPrinter, String paramString) {
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(paramString);
    stringBuilder3.append("inputType=0x");
    stringBuilder3.append(Integer.toHexString(this.inputType));
    stringBuilder3.append(" imeOptions=0x");
    int i = this.imeOptions;
    stringBuilder3.append(Integer.toHexString(i));
    stringBuilder3.append(" privateImeOptions=");
    stringBuilder3.append(this.privateImeOptions);
    String str2 = stringBuilder3.toString();
    paramPrinter.println(str2);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString);
    stringBuilder2.append("actionLabel=");
    stringBuilder2.append(this.actionLabel);
    stringBuilder2.append(" actionId=");
    stringBuilder2.append(this.actionId);
    paramPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString);
    stringBuilder2.append("initialSelStart=");
    stringBuilder2.append(this.initialSelStart);
    stringBuilder2.append(" initialSelEnd=");
    stringBuilder2.append(this.initialSelEnd);
    stringBuilder2.append(" initialCapsMode=0x");
    i = this.initialCapsMode;
    stringBuilder2.append(Integer.toHexString(i));
    String str1 = stringBuilder2.toString();
    paramPrinter.println(str1);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString);
    stringBuilder1.append("hintText=");
    stringBuilder1.append(this.hintText);
    stringBuilder1.append(" label=");
    stringBuilder1.append(this.label);
    paramPrinter.println(stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString);
    stringBuilder1.append("packageName=");
    stringBuilder1.append(this.packageName);
    stringBuilder1.append(" autofillId=");
    stringBuilder1.append(this.autofillId);
    stringBuilder1.append(" fieldId=");
    stringBuilder1.append(this.fieldId);
    stringBuilder1.append(" fieldName=");
    stringBuilder1.append(this.fieldName);
    paramPrinter.println(stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString);
    stringBuilder1.append("extras=");
    stringBuilder1.append(this.extras);
    paramPrinter.println(stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString);
    stringBuilder1.append("hintLocales=");
    stringBuilder1.append(this.hintLocales);
    paramPrinter.println(stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString);
    stringBuilder1.append("contentMimeTypes=");
    stringBuilder1.append(Arrays.toString((Object[])this.contentMimeTypes));
    paramPrinter.println(stringBuilder1.toString());
    if (this.targetInputMethodUser != null) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("targetInputMethodUserId=");
      stringBuilder1.append(this.targetInputMethodUser.getIdentifier());
      paramPrinter.println(stringBuilder1.toString());
    } 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.inputType);
    paramParcel.writeInt(this.imeOptions);
    paramParcel.writeString(this.privateImeOptions);
    TextUtils.writeToParcel(this.actionLabel, paramParcel, paramInt);
    paramParcel.writeInt(this.actionId);
    paramParcel.writeInt(this.initialSelStart);
    paramParcel.writeInt(this.initialSelEnd);
    paramParcel.writeInt(this.initialCapsMode);
    TextUtils.writeToParcel(this.hintText, paramParcel, paramInt);
    TextUtils.writeToParcel(this.label, paramParcel, paramInt);
    paramParcel.writeString(this.packageName);
    paramParcel.writeParcelable(this.autofillId, paramInt);
    paramParcel.writeInt(this.fieldId);
    paramParcel.writeString(this.fieldName);
    paramParcel.writeBundle(this.extras);
    this.mInitialSurroundingText.writeToParcel(paramParcel, paramInt);
    LocaleList localeList = this.hintLocales;
    if (localeList != null) {
      localeList.writeToParcel(paramParcel, paramInt);
    } else {
      LocaleList.getEmptyLocaleList().writeToParcel(paramParcel, paramInt);
    } 
    paramParcel.writeStringArray(this.contentMimeTypes);
    UserHandle.writeToParcel(this.targetInputMethodUser, paramParcel);
  }
  
  public static final Parcelable.Creator<EditorInfo> CREATOR = new Parcelable.Creator<EditorInfo>() {
      public EditorInfo createFromParcel(Parcel param1Parcel) {
        EditorInfo editorInfo = new EditorInfo();
        editorInfo.inputType = param1Parcel.readInt();
        editorInfo.imeOptions = param1Parcel.readInt();
        editorInfo.privateImeOptions = param1Parcel.readString();
        editorInfo.actionLabel = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        editorInfo.actionId = param1Parcel.readInt();
        editorInfo.initialSelStart = param1Parcel.readInt();
        editorInfo.initialSelEnd = param1Parcel.readInt();
        editorInfo.initialCapsMode = param1Parcel.readInt();
        editorInfo.hintText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        editorInfo.label = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        editorInfo.packageName = param1Parcel.readString();
        editorInfo.autofillId = (AutofillId)param1Parcel.readParcelable(AutofillId.class.getClassLoader());
        editorInfo.fieldId = param1Parcel.readInt();
        editorInfo.fieldName = param1Parcel.readString();
        editorInfo.extras = param1Parcel.readBundle();
        Parcelable.Creator<EditorInfo.InitialSurroundingText> creator = EditorInfo.InitialSurroundingText.CREATOR;
        EditorInfo.InitialSurroundingText initialSurroundingText = (EditorInfo.InitialSurroundingText)creator.createFromParcel(param1Parcel);
        EditorInfo.access$402(editorInfo, initialSurroundingText);
        LocaleList localeList = (LocaleList)LocaleList.CREATOR.createFromParcel(param1Parcel);
        if (localeList.isEmpty())
          localeList = null; 
        editorInfo.hintLocales = localeList;
        editorInfo.contentMimeTypes = param1Parcel.readStringArray();
        editorInfo.targetInputMethodUser = UserHandle.readFromParcel(param1Parcel);
        return editorInfo;
      }
      
      public EditorInfo[] newArray(int param1Int) {
        return new EditorInfo[param1Int];
      }
    };
  
  public static final int IME_ACTION_DONE = 6;
  
  public static final int IME_ACTION_GO = 2;
  
  public static final int IME_ACTION_NEXT = 5;
  
  public static final int IME_ACTION_NONE = 1;
  
  public static final int IME_ACTION_PREVIOUS = 7;
  
  public static final int IME_ACTION_SEARCH = 3;
  
  public static final int IME_ACTION_SEND = 4;
  
  public static final int IME_ACTION_UNSPECIFIED = 0;
  
  public static final int IME_FLAG_FORCE_ASCII = -2147483648;
  
  public static final int IME_FLAG_NAVIGATE_NEXT = 134217728;
  
  public static final int IME_FLAG_NAVIGATE_PREVIOUS = 67108864;
  
  public static final int IME_FLAG_NO_ACCESSORY_ACTION = 536870912;
  
  public static final int IME_FLAG_NO_ENTER_ACTION = 1073741824;
  
  public static final int IME_FLAG_NO_EXTRACT_UI = 268435456;
  
  public static final int IME_FLAG_NO_FULLSCREEN = 33554432;
  
  public static final int IME_FLAG_NO_PERSONALIZED_LEARNING = 16777216;
  
  public static final int IME_MASK_ACTION = 255;
  
  public static final int IME_NULL = 0;
  
  static final int MAX_INITIAL_SELECTION_LENGTH = 1024;
  
  static final int MEMORY_EFFICIENT_TEXT_LENGTH = 2048;
  
  public AutofillId autofillId;
  
  public Bundle extras;
  
  public int fieldId;
  
  public String fieldName;
  
  public CharSequence hintText;
  
  public CharSequence label;
  
  public String packageName;
  
  public int describeContents() {
    return 0;
  }
  
  static final class InitialSurroundingText implements Parcelable {
    InitialSurroundingText() {
      this.mSurroundingText = null;
      this.mSelectionHead = 0;
      this.mSelectionEnd = 0;
    }
    
    InitialSurroundingText(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      this.mSurroundingText = param1CharSequence;
      this.mSelectionHead = param1Int1;
      this.mSelectionEnd = param1Int2;
    }
    
    private CharSequence getInitialTextBeforeCursor(int param1Int1, int param1Int2) {
      CharSequence charSequence;
      if (this.mSurroundingText == null)
        return null; 
      param1Int1 = Math.min(param1Int1, this.mSelectionHead);
      if ((param1Int2 & 0x1) != 0) {
        charSequence = this.mSurroundingText;
        param1Int2 = this.mSelectionHead;
        charSequence = charSequence.subSequence(param1Int2 - param1Int1, param1Int2);
      } else {
        charSequence = this.mSurroundingText;
        param1Int2 = this.mSelectionHead;
        charSequence = TextUtils.substring(charSequence, param1Int2 - param1Int1, param1Int2);
      } 
      return charSequence;
    }
    
    private CharSequence getInitialSelectedText(int param1Int) {
      CharSequence charSequence = this.mSurroundingText;
      if (charSequence == null)
        return null; 
      if ((param1Int & 0x1) != 0) {
        charSequence = charSequence.subSequence(this.mSelectionHead, this.mSelectionEnd);
      } else {
        charSequence = TextUtils.substring(charSequence, this.mSelectionHead, this.mSelectionEnd);
      } 
      return charSequence;
    }
    
    private CharSequence getInitialTextAfterCursor(int param1Int1, int param1Int2) {
      CharSequence charSequence = this.mSurroundingText;
      if (charSequence == null)
        return null; 
      param1Int1 = Math.min(param1Int1, charSequence.length() - this.mSelectionEnd);
      if ((param1Int2 & 0x1) != 0) {
        charSequence = this.mSurroundingText;
        param1Int2 = this.mSelectionEnd;
        charSequence = charSequence.subSequence(param1Int2, param1Int2 + param1Int1);
      } else {
        charSequence = this.mSurroundingText;
        param1Int2 = this.mSelectionEnd;
        charSequence = TextUtils.substring(charSequence, param1Int2, param1Int2 + param1Int1);
      } 
      return charSequence;
    }
    
    private int getSelectionLength() {
      return this.mSelectionEnd - this.mSelectionHead;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      TextUtils.writeToParcel(this.mSurroundingText, param1Parcel, param1Int);
      param1Parcel.writeInt(this.mSelectionHead);
      param1Parcel.writeInt(this.mSelectionEnd);
    }
    
    public static final Parcelable.Creator<InitialSurroundingText> CREATOR = new Parcelable.Creator<InitialSurroundingText>() {
        public EditorInfo.InitialSurroundingText createFromParcel(Parcel param2Parcel) {
          Parcelable.Creator<CharSequence> creator = TextUtils.CHAR_SEQUENCE_CREATOR;
          CharSequence charSequence = (CharSequence)creator.createFromParcel(param2Parcel);
          int i = param2Parcel.readInt();
          int j = param2Parcel.readInt();
          return new EditorInfo.InitialSurroundingText(charSequence, i, j);
        }
        
        public EditorInfo.InitialSurroundingText[] newArray(int param2Int) {
          return new EditorInfo.InitialSurroundingText[param2Int];
        }
      };
    
    final int mSelectionEnd;
    
    final int mSelectionHead;
    
    final CharSequence mSurroundingText;
  }
  
  class null implements Parcelable.Creator<InitialSurroundingText> {
    public EditorInfo.InitialSurroundingText createFromParcel(Parcel param1Parcel) {
      Parcelable.Creator<CharSequence> creator = TextUtils.CHAR_SEQUENCE_CREATOR;
      CharSequence charSequence = (CharSequence)creator.createFromParcel(param1Parcel);
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      return new EditorInfo.InitialSurroundingText(charSequence, i, j);
    }
    
    public EditorInfo.InitialSurroundingText[] newArray(int param1Int) {
      return new EditorInfo.InitialSurroundingText[param1Int];
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class TrimPolicy implements Annotation {
    public static final int HEAD = 0;
    
    public static final int TAIL = 1;
  }
}
