package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.res.Configuration;

public interface IOplusBurmeseZgHooks extends IOplusCommonFeature {
  public static final IOplusBurmeseZgHooks DEFAULT = (IOplusBurmeseZgHooks)new Object();
  
  default IOplusBurmeseZgHooks getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusBurmeseZgHooks;
  }
  
  default void initBurmeseZgFlag(Context paramContext) {}
  
  default void updateBurmeseZgFlag(Context paramContext) {}
  
  default boolean getZgFlag() {
    return false;
  }
  
  default void updateBurmeseEncodingForUser(Context paramContext, Configuration paramConfiguration, int paramInt) {}
}
