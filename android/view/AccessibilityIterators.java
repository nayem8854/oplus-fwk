package android.view;

import android.content.res.Configuration;
import java.text.BreakIterator;
import java.util.Locale;

public final class AccessibilityIterators {
  class AbstractTextSegmentIterator implements TextSegmentIterator {
    private final int[] mSegment = new int[2];
    
    protected String mText;
    
    public void initialize(String param1String) {
      this.mText = param1String;
    }
    
    protected int[] getRange(int param1Int1, int param1Int2) {
      if (param1Int1 < 0 || param1Int2 < 0 || param1Int1 == param1Int2)
        return null; 
      int[] arrayOfInt = this.mSegment;
      arrayOfInt[0] = param1Int1;
      arrayOfInt[1] = param1Int2;
      return arrayOfInt;
    }
  }
  
  class CharacterTextSegmentIterator extends AbstractTextSegmentIterator implements ViewRootImpl.ConfigChangedCallback {
    private static CharacterTextSegmentIterator sInstance;
    
    protected BreakIterator mImpl;
    
    private Locale mLocale;
    
    public static CharacterTextSegmentIterator getInstance(Locale param1Locale) {
      if (sInstance == null)
        sInstance = new CharacterTextSegmentIterator(); 
      return sInstance;
    }
    
    private CharacterTextSegmentIterator(AccessibilityIterators this$0) {
      this.mLocale = (Locale)this$0;
      onLocaleChanged((Locale)this$0);
      ViewRootImpl.addConfigCallback(this);
    }
    
    public void initialize(String param1String) {
      super.initialize(param1String);
      this.mImpl.setText(param1String);
    }
    
    public int[] following(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int >= i)
        return null; 
      i = param1Int;
      param1Int = i;
      if (i < 0)
        param1Int = 0; 
      while (!this.mImpl.isBoundary(param1Int)) {
        i = this.mImpl.following(param1Int);
        param1Int = i;
        if (i == -1)
          return null; 
      } 
      i = this.mImpl.following(param1Int);
      if (i == -1)
        return null; 
      return getRange(param1Int, i);
    }
    
    public int[] preceding(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int <= 0)
        return null; 
      int j = param1Int;
      param1Int = j;
      if (j > i)
        param1Int = i; 
      while (!this.mImpl.isBoundary(param1Int)) {
        j = this.mImpl.preceding(param1Int);
        param1Int = j;
        if (j == -1)
          return null; 
      } 
      j = this.mImpl.preceding(param1Int);
      if (j == -1)
        return null; 
      return getRange(j, param1Int);
    }
    
    public void onConfigurationChanged(Configuration param1Configuration) {
      Locale locale = param1Configuration.getLocales().get(0);
      if (locale == null)
        return; 
      if (!this.mLocale.equals(locale)) {
        this.mLocale = locale;
        onLocaleChanged(locale);
      } 
    }
    
    protected void onLocaleChanged(Locale param1Locale) {
      this.mImpl = BreakIterator.getCharacterInstance(param1Locale);
    }
  }
  
  class WordTextSegmentIterator extends CharacterTextSegmentIterator {
    private static WordTextSegmentIterator sInstance;
    
    public static WordTextSegmentIterator getInstance(Locale param1Locale) {
      if (sInstance == null)
        sInstance = new WordTextSegmentIterator(param1Locale); 
      return sInstance;
    }
    
    private WordTextSegmentIterator(AccessibilityIterators this$0) {
      super((Locale)this$0);
    }
    
    protected void onLocaleChanged(Locale param1Locale) {
      this.mImpl = BreakIterator.getWordInstance(param1Locale);
    }
    
    public int[] following(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int >= this.mText.length())
        return null; 
      i = param1Int;
      param1Int = i;
      if (i < 0)
        param1Int = 0; 
      while (!isLetterOrDigit(param1Int) && !isStartBoundary(param1Int)) {
        i = this.mImpl.following(param1Int);
        param1Int = i;
        if (i == -1)
          return null; 
      } 
      i = this.mImpl.following(param1Int);
      if (i == -1 || !isEndBoundary(i))
        return null; 
      return getRange(param1Int, i);
    }
    
    public int[] preceding(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int <= 0)
        return null; 
      int j = param1Int;
      param1Int = j;
      if (j > i)
        param1Int = i; 
      while (param1Int > 0 && !isLetterOrDigit(param1Int - 1) && !isEndBoundary(param1Int)) {
        j = this.mImpl.preceding(param1Int);
        param1Int = j;
        if (j == -1)
          return null; 
      } 
      j = this.mImpl.preceding(param1Int);
      if (j == -1 || !isStartBoundary(j))
        return null; 
      return getRange(j, param1Int);
    }
    
    private boolean isStartBoundary(int param1Int) {
      boolean bool;
      if (isLetterOrDigit(param1Int) && (param1Int == 0 || 
        !isLetterOrDigit(param1Int - 1))) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean isEndBoundary(int param1Int) {
      if (param1Int > 0 && isLetterOrDigit(param1Int - 1)) {
        String str = this.mText;
        if (param1Int == str.length() || !isLetterOrDigit(param1Int))
          return true; 
      } 
      return false;
    }
    
    private boolean isLetterOrDigit(int param1Int) {
      if (param1Int >= 0 && param1Int < this.mText.length()) {
        param1Int = this.mText.codePointAt(param1Int);
        return Character.isLetterOrDigit(param1Int);
      } 
      return false;
    }
  }
  
  class ParagraphTextSegmentIterator extends AbstractTextSegmentIterator {
    private static ParagraphTextSegmentIterator sInstance;
    
    public static ParagraphTextSegmentIterator getInstance() {
      if (sInstance == null)
        sInstance = new ParagraphTextSegmentIterator(); 
      return sInstance;
    }
    
    public int[] following(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int >= i)
        return null; 
      int j = param1Int;
      param1Int = j;
      if (j < 0)
        param1Int = 0; 
      while (param1Int < i && this.mText.charAt(param1Int) == '\n' && 
        !isStartBoundary(param1Int))
        param1Int++; 
      if (param1Int >= i)
        return null; 
      j = param1Int + 1;
      while (j < i && !isEndBoundary(j))
        j++; 
      return getRange(param1Int, j);
    }
    
    public int[] preceding(int param1Int) {
      int i = this.mText.length();
      if (i <= 0)
        return null; 
      if (param1Int <= 0)
        return null; 
      int j = param1Int;
      param1Int = j;
      if (j > i)
        param1Int = i; 
      while (param1Int > 0 && this.mText.charAt(param1Int - 1) == '\n' && !isEndBoundary(param1Int))
        param1Int--; 
      if (param1Int <= 0)
        return null; 
      j = param1Int - 1;
      while (j > 0 && !isStartBoundary(j))
        j--; 
      return getRange(j, param1Int);
    }
    
    private boolean isStartBoundary(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mText : Ljava/lang/String;
      //   4: iload_1
      //   5: invokevirtual charAt : (I)C
      //   8: bipush #10
      //   10: if_icmpeq -> 39
      //   13: iload_1
      //   14: ifeq -> 34
      //   17: aload_0
      //   18: getfield mText : Ljava/lang/String;
      //   21: astore_2
      //   22: aload_2
      //   23: iload_1
      //   24: iconst_1
      //   25: isub
      //   26: invokevirtual charAt : (I)C
      //   29: bipush #10
      //   31: if_icmpne -> 39
      //   34: iconst_1
      //   35: istore_3
      //   36: goto -> 41
      //   39: iconst_0
      //   40: istore_3
      //   41: iload_3
      //   42: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #322	-> 0
      //   #323	-> 22
      //   #322	-> 41
    }
    
    private boolean isEndBoundary(int param1Int) {
      if (param1Int > 0 && this.mText.charAt(param1Int - 1) != '\n') {
        String str = this.mText;
        if (param1Int == str.length() || this.mText.charAt(param1Int) == '\n')
          return true; 
      } 
      return false;
    }
  }
  
  public static interface TextSegmentIterator {
    int[] following(int param1Int);
    
    int[] preceding(int param1Int);
  }
}
