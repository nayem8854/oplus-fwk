package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPinnedStackController extends IInterface {
  int getDisplayRotation() throws RemoteException;
  
  class Default implements IPinnedStackController {
    public int getDisplayRotation() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPinnedStackController {
    private static final String DESCRIPTOR = "android.view.IPinnedStackController";
    
    static final int TRANSACTION_getDisplayRotation = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IPinnedStackController");
    }
    
    public static IPinnedStackController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IPinnedStackController");
      if (iInterface != null && iInterface instanceof IPinnedStackController)
        return (IPinnedStackController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getDisplayRotation";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IPinnedStackController");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IPinnedStackController");
      param1Int1 = getDisplayRotation();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IPinnedStackController {
      public static IPinnedStackController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IPinnedStackController";
      }
      
      public int getDisplayRotation() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IPinnedStackController");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPinnedStackController.Stub.getDefaultImpl() != null)
            return IPinnedStackController.Stub.getDefaultImpl().getDisplayRotation(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPinnedStackController param1IPinnedStackController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPinnedStackController != null) {
          Proxy.sDefaultImpl = param1IPinnedStackController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPinnedStackController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
