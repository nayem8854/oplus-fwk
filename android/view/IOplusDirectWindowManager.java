package android.view;

import android.os.RemoteException;
import com.oplus.direct.OplusDirectFindCmd;

public interface IOplusDirectWindowManager extends IOplusBaseWindowManager {
  public static final int DIRECT_FIND_CMD = 10402;
  
  public static final int IOPLUSDIRECTWINDOWMANAGER_INDEX = 10401;
  
  void directFindCmd(OplusDirectFindCmd paramOplusDirectFindCmd) throws RemoteException;
}
