package android.view;

import com.oplus.screenshot.OplusLongshotViewRoot;

public interface IOplusBaseViewRoot {
  OplusLongshotViewRoot getLongshotViewRoot();
  
  default OplusViewRootImplHooks getOplusViewRootImplHooks() {
    return null;
  }
}
