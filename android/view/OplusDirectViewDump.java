package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.widget.TextView;
import com.oplus.direct.IOplusDirectFindCallback;
import com.oplus.direct.OplusDirectFindCmd;
import com.oplus.direct.OplusDirectFindCmds;
import com.oplus.direct.OplusDirectFindResult;
import com.oplus.direct.OplusDirectUtils;
import com.oplus.favorite.IOplusFavoriteConstans;
import com.oplus.favorite.IOplusFavoriteManager;
import com.oplus.favorite.OplusFavoriteCallback;
import com.oplus.favorite.OplusFavoriteData;
import com.oplus.favorite.OplusFavoriteHelper;
import com.oplus.favorite.OplusFavoriteResult;
import com.oplus.util.OplusLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class OplusDirectViewDump implements IOplusFavoriteConstans {
  private static final boolean DBG = OplusDirectUtils.DBG;
  
  private static final String TAG = "DirectService";
  
  public void findCmd(ViewRootImpl paramViewRootImpl, OplusDirectFindCmd paramOplusDirectFindCmd) {
    OplusDirectFindCmds oplusDirectFindCmds = paramOplusDirectFindCmd.getCommand();
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("findCmd : ");
    stringBuilder.append(oplusDirectFindCmds);
    OplusLog.d(bool, "DirectService", stringBuilder.toString());
    int i = null.$SwitchMap$com$oplus$direct$OplusDirectFindCmds[oplusDirectFindCmds.ordinal()];
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          OplusDirectUtils.onFindFailed(paramOplusDirectFindCmd.getCallback(), "unknown_cmd");
        } else {
          saveFavorite(paramViewRootImpl, paramOplusDirectFindCmd.getCallback());
        } 
      } else {
        findFavorite(paramViewRootImpl, paramOplusDirectFindCmd.getCallback());
      } 
    } else {
      Bundle bundle = paramOplusDirectFindCmd.getBundle();
      ArrayList<String> arrayList = bundle.getStringArrayList("id_names");
      findText(paramViewRootImpl, paramOplusDirectFindCmd.getCallback(), arrayList);
    } 
  }
  
  private String getPackageName(String paramString1, String paramString2) {
    String[] arrayOfString = paramString1.toString().split(":");
    if (arrayOfString != null && arrayOfString.length == 2)
      return arrayOfString[0]; 
    return paramString2;
  }
  
  private String getIdName(String paramString) {
    String[] arrayOfString = paramString.toString().split(":");
    if (arrayOfString != null && arrayOfString.length == 2)
      return arrayOfString[1].replace("id/", ""); 
    return paramString;
  }
  
  private CharSequence getTextFromView(ViewRootImpl paramViewRootImpl, Resources paramResources, String paramString1, String paramString2) {
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getTextFromView : [");
    stringBuilder.append(paramString2);
    stringBuilder.append(":id/");
    stringBuilder.append(paramString1);
    stringBuilder.append("]");
    OplusLog.d(bool, "DirectService", stringBuilder.toString());
    View view = paramViewRootImpl.getView();
    if (view == null)
      return null; 
    int i = paramResources.getIdentifier(paramString1, "id", paramString2);
    if (i <= 0)
      return null; 
    view = view.findViewById(i);
    if (view == null)
      return null; 
    if (view instanceof TextView) {
      TextView textView = (TextView)view;
      return textView.getText();
    } 
    return null;
  }
  
  private void findText(ViewRootImpl paramViewRootImpl, IOplusDirectFindCallback paramIOplusDirectFindCallback, ArrayList<String> paramArrayList) {
    if (paramArrayList != null && paramArrayList.size() > 0) {
      OplusLog.d(DBG, "DirectService", "findText");
      Context context = paramViewRootImpl.mContext;
      Resources resources = context.getResources();
      String str = context.getPackageName();
      for (String str1 : paramArrayList) {
        String str2 = getPackageName(str1, str);
        str1 = getIdName(str1);
        CharSequence charSequence = getTextFromView(paramViewRootImpl, resources, str1, str2);
        if (TextUtils.isEmpty(charSequence))
          continue; 
        boolean bool = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("findText : text=");
        stringBuilder.append(charSequence);
        OplusLog.d(bool, "DirectService", stringBuilder.toString());
        Bundle bundle = new Bundle();
        bundle.putCharSequence("result_text", charSequence);
        OplusDirectUtils.onFindSuccess(paramIOplusDirectFindCallback, bundle);
        return;
      } 
      OplusDirectUtils.onFindFailed(paramIOplusDirectFindCallback, "no_text");
    } else {
      OplusDirectUtils.onFindFailed(paramIOplusDirectFindCallback, "no_idnames");
    } 
  }
  
  private void findFavorite(ViewRootImpl paramViewRootImpl, IOplusDirectFindCallback paramIOplusDirectFindCallback) {
    OplusLog.d(DBG, "DirectService", "findFavorite");
    IOplusFavoriteManager iOplusFavoriteManager = (IOplusFavoriteManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFavoriteManager.DEFAULT, new Object[0]);
    iOplusFavoriteManager.processCrawl(paramViewRootImpl.getView(), new FavoriteCallback(paramIOplusDirectFindCallback));
  }
  
  private void saveFavorite(ViewRootImpl paramViewRootImpl, IOplusDirectFindCallback paramIOplusDirectFindCallback) {
    OplusLog.d(DBG, "DirectService", "saveFavorite");
    IOplusFavoriteManager iOplusFavoriteManager = (IOplusFavoriteManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFavoriteManager.DEFAULT, new Object[0]);
    iOplusFavoriteManager.processSave(paramViewRootImpl.getView(), new FavoriteCallback(paramIOplusDirectFindCallback));
  }
  
  private static class FavoriteCallback extends OplusFavoriteCallback {
    private final WeakReference<IOplusDirectFindCallback> mCallback;
    
    public FavoriteCallback(IOplusDirectFindCallback param1IOplusDirectFindCallback) {
      this.mCallback = new WeakReference<>(param1IOplusDirectFindCallback);
    }
    
    public boolean isSettingOn(Context param1Context) {
      return OplusFavoriteHelper.isSettingOn(param1Context);
    }
    
    public void onFavoriteStart(Context param1Context, OplusFavoriteResult param1OplusFavoriteResult) {
      IOplusDirectFindCallback iOplusDirectFindCallback = this.mCallback.get();
      if (iOplusDirectFindCallback != null)
        try {
          OplusDirectFindResult oplusDirectFindResult = new OplusDirectFindResult();
          this();
          Bundle bundle = oplusDirectFindResult.getBundle();
          String str2 = param1OplusFavoriteResult.getPackageName();
          String str1 = this.TAG;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("onFavoriteStart : packageName=");
          stringBuilder.append(str2);
          OplusLog.d(false, str1, stringBuilder.toString());
          if (!TextUtils.isEmpty(str2))
            bundle.putString("package_name", str2); 
          iOplusDirectFindCallback.onDirectInfoFound(oplusDirectFindResult);
        } catch (RemoteException remoteException) {
          OplusLog.e(this.TAG, remoteException.toString());
        } catch (Exception exception) {
          OplusLog.e(this.TAG, exception.toString());
        }  
    }
    
    public void onFavoriteFinished(Context param1Context, OplusFavoriteResult param1OplusFavoriteResult) {
      IOplusDirectFindCallback iOplusDirectFindCallback = this.mCallback.get();
      if (iOplusDirectFindCallback != null)
        try {
          OplusDirectFindResult oplusDirectFindResult = new OplusDirectFindResult();
          this();
          Bundle bundle = oplusDirectFindResult.getBundle();
          ArrayList<OplusFavoriteData> arrayList = param1OplusFavoriteResult.getData();
          if (!arrayList.isEmpty()) {
            String str1 = this.TAG;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("onFavoriteFinished : data=");
            stringBuilder.append(arrayList.size());
            stringBuilder.append(":");
            stringBuilder.append(arrayList.get(0));
            OplusLog.d(false, str1, stringBuilder.toString());
            ArrayList<Bundle> arrayList1 = extractData(arrayList);
            bundle.putParcelableArrayList("result_data", arrayList1);
            arrayList = (ArrayList)extractTitles(arrayList);
            bundle.putStringArrayList("result_titles", arrayList);
          } 
          String str = param1OplusFavoriteResult.getError();
          if (!TextUtils.isEmpty(str)) {
            String str1 = this.TAG;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("onFavoriteFinished : error=");
            stringBuilder.append(str);
            OplusLog.d(false, str1, stringBuilder.toString());
            bundle.putString("result_error", str);
          } 
          iOplusDirectFindCallback.onDirectInfoFound(oplusDirectFindResult);
        } catch (RemoteException remoteException) {
          OplusLog.e(this.TAG, remoteException.toString());
        } catch (Exception exception) {
          OplusLog.e(this.TAG, exception.toString());
        }  
    }
    
    private ArrayList<Bundle> extractData(ArrayList<OplusFavoriteData> param1ArrayList) {
      synchronized (new ArrayList()) {
        for (OplusFavoriteData oplusFavoriteData : param1ArrayList) {
          Bundle bundle = new Bundle();
          this();
          bundle.putString("data_title", oplusFavoriteData.getTitle());
          bundle.putString("data_url", oplusFavoriteData.getUrl());
          null.add(bundle);
        } 
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/ArrayList<ObjectType{com/oplus/favorite/OplusFavoriteData}>}, name=param1ArrayList} */
        return null;
      } 
    }
    
    private ArrayList<String> extractTitles(ArrayList<OplusFavoriteData> param1ArrayList) {
      synchronized (new ArrayList()) {
        for (OplusFavoriteData oplusFavoriteData : param1ArrayList)
          null.add(oplusFavoriteData.getTitle()); 
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/ArrayList<ObjectType{com/oplus/favorite/OplusFavoriteData}>}, name=param1ArrayList} */
        return null;
      } 
    }
  }
}
