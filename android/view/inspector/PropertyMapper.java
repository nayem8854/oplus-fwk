package android.view.inspector;

import java.util.Set;
import java.util.function.IntFunction;

public interface PropertyMapper {
  int mapBoolean(String paramString, int paramInt);
  
  int mapByte(String paramString, int paramInt);
  
  int mapChar(String paramString, int paramInt);
  
  int mapColor(String paramString, int paramInt);
  
  int mapDouble(String paramString, int paramInt);
  
  int mapFloat(String paramString, int paramInt);
  
  int mapGravity(String paramString, int paramInt);
  
  int mapInt(String paramString, int paramInt);
  
  int mapIntEnum(String paramString, int paramInt, IntFunction<String> paramIntFunction);
  
  int mapIntFlag(String paramString, int paramInt, IntFunction<Set<String>> paramIntFunction);
  
  int mapLong(String paramString, int paramInt);
  
  int mapObject(String paramString, int paramInt);
  
  int mapResourceId(String paramString, int paramInt);
  
  int mapShort(String paramString, int paramInt);
  
  public static class PropertyConflictException extends RuntimeException {
    public PropertyConflictException(String param1String1, String param1String2, String param1String3) {
      super(String.format("Attempted to map property \"%s\" as type %s, but it is already mapped as %s.", new Object[] { param1String1, param1String2, param1String3 }));
    }
  }
}
