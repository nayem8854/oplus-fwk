package android.view.inspector;

public class StaticInspectionCompanionProvider implements InspectionCompanionProvider {
  private static final String COMPANION_SUFFIX = "$InspectionCompanion";
  
  public <T> InspectionCompanion<T> provide(Class<T> paramClass) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramClass.getName());
    stringBuilder.append("$InspectionCompanion");
    String str = stringBuilder.toString();
    try {
      paramClass = (Class)paramClass.getClassLoader().loadClass(str);
      if (InspectionCompanion.class.isAssignableFrom(paramClass))
        return (InspectionCompanion)paramClass.newInstance(); 
      return null;
    } catch (ClassNotFoundException classNotFoundException) {
      return null;
    } catch (IllegalAccessException illegalAccessException) {
      throw new RuntimeException(illegalAccessException);
    } catch (InstantiationException instantiationException) {
      Throwable throwable = instantiationException.getCause();
      if (!(throwable instanceof RuntimeException)) {
        if (throwable instanceof Error)
          throw (Error)throwable; 
        throw new RuntimeException(throwable);
      } 
      throw (RuntimeException)throwable;
    } 
  }
}
