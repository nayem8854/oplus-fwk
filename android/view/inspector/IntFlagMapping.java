package android.view.inspector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public final class IntFlagMapping {
  private final List<Flag> mFlags = new ArrayList<>();
  
  public Set<String> get(int paramInt) {
    HashSet<String> hashSet = new HashSet(this.mFlags.size());
    for (Flag flag : this.mFlags) {
      if (flag.isEnabledFor(paramInt))
        hashSet.add(flag.mName); 
    } 
    return Collections.unmodifiableSet(hashSet);
  }
  
  public void add(int paramInt1, int paramInt2, String paramString) {
    this.mFlags.add(new Flag(paramInt1, paramInt2, paramString));
  }
  
  private static final class Flag {
    private final int mMask;
    
    private final String mName;
    
    private final int mTarget;
    
    private Flag(int param1Int1, int param1Int2, String param1String) {
      this.mTarget = param1Int2;
      this.mMask = param1Int1;
      Objects.requireNonNull(param1String);
      this.mName = param1String;
    }
    
    private boolean isEnabledFor(int param1Int) {
      boolean bool;
      if ((this.mMask & param1Int) == this.mTarget) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
}
