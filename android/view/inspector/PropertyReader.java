package android.view.inspector;

import android.graphics.Color;

public interface PropertyReader {
  void readBoolean(int paramInt, boolean paramBoolean);
  
  void readByte(int paramInt, byte paramByte);
  
  void readChar(int paramInt, char paramChar);
  
  void readColor(int paramInt1, int paramInt2);
  
  void readColor(int paramInt, long paramLong);
  
  void readColor(int paramInt, Color paramColor);
  
  void readDouble(int paramInt, double paramDouble);
  
  void readFloat(int paramInt, float paramFloat);
  
  void readGravity(int paramInt1, int paramInt2);
  
  void readInt(int paramInt1, int paramInt2);
  
  void readIntEnum(int paramInt1, int paramInt2);
  
  void readIntFlag(int paramInt1, int paramInt2);
  
  void readLong(int paramInt, long paramLong);
  
  void readObject(int paramInt, Object paramObject);
  
  void readResourceId(int paramInt1, int paramInt2);
  
  void readShort(int paramInt, short paramShort);
  
  public static class PropertyTypeMismatchException extends RuntimeException {
    public PropertyTypeMismatchException(int param1Int, String param1String1, String param1String2, String param1String3) {
      super(formatMessage(param1Int, param1String1, param1String2, param1String3));
    }
    
    public PropertyTypeMismatchException(int param1Int, String param1String1, String param1String2) {
      super(formatMessage(param1Int, param1String1, param1String2, null));
    }
    
    private static String formatMessage(int param1Int, String param1String1, String param1String2, String param1String3) {
      if (param1String3 == null)
        return String.format("Attempted to read property with ID 0x%08X as type %s, but the ID is of type %s.", new Object[] { Integer.valueOf(param1Int), param1String1, param1String2 }); 
      return String.format("Attempted to read property \"%s\" with ID 0x%08X as type %s, but the ID is of type %s.", new Object[] { param1String3, Integer.valueOf(param1Int), param1String1, param1String2 });
    }
  }
}
