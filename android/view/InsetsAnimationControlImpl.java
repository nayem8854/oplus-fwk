package android.view;

import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.util.ArraySet;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.SparseSetArray;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Objects;

public class InsetsAnimationControlImpl implements WindowInsetsAnimationController, InsetsAnimationControlRunner {
  private final Rect mTmpFrame = new Rect();
  
  private final SparseIntArray mTypeSideMap = new SparseIntArray();
  
  private final SparseSetArray<InsetsSourceControl> mSideSourceMap = new SparseSetArray<>();
  
  private final Matrix mTmpMatrix = new Matrix();
  
  private float mCurrentAlpha = 1.0F;
  
  private float mPendingAlpha = 1.0F;
  
  private static final String TAG = "InsetsAnimationCtrlImpl";
  
  private final WindowInsetsAnimation mAnimation;
  
  private final int mAnimationType;
  
  private boolean mCancelled;
  
  private final InsetsAnimationControlCallbacks mController;
  
  private final SparseArray<InsetsSourceControl> mControls;
  
  private Insets mCurrentInsets;
  
  private boolean mFinished;
  
  private final boolean mHasZeroInsetsIme;
  
  private final Insets mHiddenInsets;
  
  private final InsetsState mInitialInsetsState;
  
  private final WindowInsetsAnimationControlListener mListener;
  
  private float mPendingFraction;
  
  private Insets mPendingInsets;
  
  private Boolean mPerceptible;
  
  public boolean mReadyDispatched;
  
  private final Insets mShownInsets;
  
  private boolean mShownOnFinish;
  
  private final int mTypes;
  
  public InsetsAnimationControlImpl(SparseArray<InsetsSourceControl> paramSparseArray, Rect paramRect, InsetsState paramInsetsState, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener, int paramInt1, InsetsAnimationControlCallbacks paramInsetsAnimationControlCallbacks, long paramLong, Interpolator paramInterpolator, int paramInt2) {
    this.mControls = paramSparseArray;
    this.mListener = paramWindowInsetsAnimationControlListener;
    this.mTypes = paramInt1;
    this.mController = paramInsetsAnimationControlCallbacks;
    boolean bool = true;
    this.mInitialInsetsState = paramInsetsState = new InsetsState(paramInsetsState, true, true);
    Insets insets2 = getInsetsFromState(paramInsetsState, paramRect, null);
    this.mPendingInsets = insets2;
    this.mHiddenInsets = calculateInsets(this.mInitialInsetsState, paramRect, paramSparseArray, false, null);
    Insets insets1 = calculateInsets(this.mInitialInsetsState, paramRect, paramSparseArray, true, this.mTypeSideMap);
    if (insets1.bottom != 0 || !controlsInternalType(13))
      bool = false; 
    this.mHasZeroInsetsIme = bool;
    if (bool)
      this.mTypeSideMap.put(13, 3); 
    buildTypeSourcesMap(this.mTypeSideMap, this.mSideSourceMap, this.mControls);
    WindowInsetsAnimation windowInsetsAnimation = new WindowInsetsAnimation(this.mTypes, paramInterpolator, paramLong);
    windowInsetsAnimation.setAlpha(getCurrentAlpha());
    this.mAnimationType = paramInt2;
    this.mController.startAnimation(this, paramWindowInsetsAnimationControlListener, paramInt1, this.mAnimation, new WindowInsetsAnimation.Bounds(this.mHiddenInsets, this.mShownInsets));
  }
  
  private boolean calculatePerceptible(Insets paramInsets, float paramFloat) {
    boolean bool;
    if (paramInsets.left * 100 >= (this.mShownInsets.left - this.mHiddenInsets.left) * 5 && paramInsets.top * 100 >= (this.mShownInsets.top - this.mHiddenInsets.top) * 5 && paramInsets.right * 100 >= (this.mShownInsets.right - this.mHiddenInsets.right) * 5 && paramInsets.bottom * 100 >= (this.mShownInsets.bottom - this.mHiddenInsets.bottom) * 5 && paramFloat >= 0.5F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasZeroInsetsIme() {
    return this.mHasZeroInsetsIme;
  }
  
  public Insets getHiddenStateInsets() {
    return this.mHiddenInsets;
  }
  
  public Insets getShownStateInsets() {
    return this.mShownInsets;
  }
  
  public Insets getCurrentInsets() {
    return this.mCurrentInsets;
  }
  
  public float getCurrentAlpha() {
    return this.mCurrentAlpha;
  }
  
  public int getTypes() {
    return this.mTypes;
  }
  
  public int getAnimationType() {
    return this.mAnimationType;
  }
  
  public void setInsetsAndAlpha(Insets paramInsets, float paramFloat1, float paramFloat2) {
    setInsetsAndAlpha(paramInsets, paramFloat1, paramFloat2, false);
  }
  
  private void setInsetsAndAlpha(Insets paramInsets, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    if (paramBoolean || !this.mFinished) {
      if (!this.mCancelled) {
        this.mPendingFraction = sanitize(paramFloat2);
        this.mPendingInsets = sanitize(paramInsets);
        this.mPendingAlpha = sanitize(paramFloat1);
        this.mController.scheduleApplyChangeInsets(this);
        paramBoolean = calculatePerceptible(this.mPendingInsets, this.mPendingAlpha);
        Boolean bool = this.mPerceptible;
        if (bool == null || paramBoolean != bool.booleanValue()) {
          this.mController.reportPerceptible(this.mTypes, paramBoolean);
          this.mPerceptible = Boolean.valueOf(paramBoolean);
        } 
        return;
      } 
      throw new IllegalStateException("Can't change insets on an animation that is cancelled.");
    } 
    throw new IllegalStateException("Can't change insets on an animation that is finished.");
  }
  
  void applyInsetsAndAlphaNoCheck(boolean paramBoolean, float paramFloat, InsetsState paramInsetsState) {
    Insets insets1;
    if (paramBoolean) {
      insets1 = this.mShownInsets;
    } else {
      insets1 = this.mHiddenInsets;
    } 
    Insets insets2 = sanitize(insets1);
    Insets insets3 = Insets.subtract(this.mShownInsets, insets2);
    ArrayList<SyncRtSurfaceTransactionApplier.SurfaceParams> arrayList = new ArrayList();
    int i = insets3.left, j = this.mShownInsets.left, k = insets2.left;
    updateLeashesForSide(0, i, j, k, arrayList, paramInsetsState, Float.valueOf(paramFloat));
    k = insets3.top;
    j = this.mShownInsets.top;
    i = insets2.top;
    updateLeashesForSide(1, k, j, i, arrayList, paramInsetsState, Float.valueOf(paramFloat));
    k = insets3.right;
    j = this.mShownInsets.right;
    i = insets2.right;
    updateLeashesForSide(2, k, j, i, arrayList, paramInsetsState, Float.valueOf(paramFloat));
    k = insets3.bottom;
    j = this.mShownInsets.bottom;
    i = insets2.bottom;
    updateLeashesForSide(3, k, j, i, arrayList, paramInsetsState, Float.valueOf(paramFloat));
    this.mController.applySurfaceParams(arrayList.<SyncRtSurfaceTransactionApplier.SurfaceParams>toArray(new SyncRtSurfaceTransactionApplier.SurfaceParams[arrayList.size()]));
  }
  
  public boolean applyChangeInsets(InsetsState paramInsetsState) {
    if (this.mCancelled) {
      if (InsetsController.DEBUG)
        Log.d("InsetsAnimationCtrlImpl", "applyChangeInsets canceled"); 
      return false;
    } 
    Insets insets = Insets.subtract(this.mShownInsets, this.mPendingInsets);
    ArrayList<SyncRtSurfaceTransactionApplier.SurfaceParams> arrayList = new ArrayList();
    int i = insets.left, j = this.mShownInsets.left, k = this.mPendingInsets.left;
    float f = this.mPendingAlpha;
    updateLeashesForSide(0, i, j, k, arrayList, paramInsetsState, Float.valueOf(f));
    j = insets.top;
    i = this.mShownInsets.top;
    k = this.mPendingInsets.top;
    f = this.mPendingAlpha;
    updateLeashesForSide(1, j, i, k, arrayList, paramInsetsState, Float.valueOf(f));
    k = insets.right;
    i = this.mShownInsets.right;
    j = this.mPendingInsets.right;
    f = this.mPendingAlpha;
    updateLeashesForSide(2, k, i, j, arrayList, paramInsetsState, Float.valueOf(f));
    k = insets.bottom;
    j = this.mShownInsets.bottom;
    i = this.mPendingInsets.bottom;
    f = this.mPendingAlpha;
    updateLeashesForSide(3, k, j, i, arrayList, paramInsetsState, Float.valueOf(f));
    this.mController.applySurfaceParams(arrayList.<SyncRtSurfaceTransactionApplier.SurfaceParams>toArray(new SyncRtSurfaceTransactionApplier.SurfaceParams[arrayList.size()]));
    this.mCurrentInsets = this.mPendingInsets;
    this.mAnimation.setFraction(this.mPendingFraction);
    this.mCurrentAlpha = f = this.mPendingAlpha;
    this.mAnimation.setAlpha(f);
    if (this.mFinished) {
      if (InsetsController.DEBUG) {
        boolean bool = this.mShownOnFinish;
        f = this.mCurrentAlpha;
        Insets insets1 = this.mCurrentInsets;
        Log.d("InsetsAnimationCtrlImpl", String.format("notifyFinished shown: %s, currentAlpha: %f, currentInsets: %s", new Object[] { Boolean.valueOf(bool), Float.valueOf(f), insets1 }));
      } 
      this.mController.notifyFinished(this, this.mShownOnFinish);
      releaseLeashes();
    } 
    if (InsetsController.DEBUG)
      Log.d("InsetsAnimationCtrlImpl", "Animation finished abruptly."); 
    return this.mFinished;
  }
  
  private void releaseLeashes() {
    for (int i = this.mControls.size() - 1; i >= 0; i--) {
      InsetsSourceControl insetsSourceControl = this.mControls.valueAt(i);
      if (insetsSourceControl != null) {
        InsetsAnimationControlCallbacks insetsAnimationControlCallbacks = this.mController;
        Objects.requireNonNull(insetsAnimationControlCallbacks);
        insetsSourceControl.release(new _$$Lambda$yePAgdxpSSjmKnpPAp6YHM4lpEQ(insetsAnimationControlCallbacks));
      } 
    } 
  }
  
  public void finish(boolean paramBoolean) {
    Insets insets;
    if (this.mCancelled || this.mFinished) {
      if (InsetsController.DEBUG)
        Log.d("InsetsAnimationCtrlImpl", "Animation already canceled or finished, not notifying."); 
      return;
    } 
    this.mShownOnFinish = paramBoolean;
    this.mFinished = true;
    if (paramBoolean) {
      insets = this.mShownInsets;
    } else {
      insets = this.mHiddenInsets;
    } 
    setInsetsAndAlpha(insets, 1.0F, 1.0F, true);
    if (InsetsController.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notify control request finished for types: ");
      stringBuilder.append(this.mTypes);
      Log.d("InsetsAnimationCtrlImpl", stringBuilder.toString());
    } 
    this.mListener.onFinished(this);
  }
  
  public float getCurrentFraction() {
    return this.mAnimation.getFraction();
  }
  
  public void cancel() {
    WindowInsetsAnimationController windowInsetsAnimationController;
    if (this.mFinished)
      return; 
    this.mCancelled = true;
    WindowInsetsAnimationControlListener windowInsetsAnimationControlListener = this.mListener;
    if (this.mReadyDispatched) {
      windowInsetsAnimationController = this;
    } else {
      windowInsetsAnimationController = null;
    } 
    windowInsetsAnimationControlListener.onCancelled(windowInsetsAnimationController);
    if (InsetsController.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notify Control request cancelled for types: ");
      stringBuilder.append(this.mTypes);
      Log.d("InsetsAnimationCtrlImpl", stringBuilder.toString());
    } 
    releaseLeashes();
  }
  
  public boolean isFinished() {
    return this.mFinished;
  }
  
  public boolean isCancelled() {
    return this.mCancelled;
  }
  
  public WindowInsetsAnimation getAnimation() {
    return this.mAnimation;
  }
  
  WindowInsetsAnimationControlListener getListener() {
    return this.mListener;
  }
  
  SparseArray<InsetsSourceControl> getControls() {
    return this.mControls;
  }
  
  private Insets calculateInsets(InsetsState paramInsetsState, Rect paramRect, SparseArray<InsetsSourceControl> paramSparseArray, boolean paramBoolean, SparseIntArray paramSparseIntArray) {
    for (int i = paramSparseArray.size() - 1; i >= 0; i--) {
      if (paramSparseArray.valueAt(i) != null)
        paramInsetsState.getSource(((InsetsSourceControl)paramSparseArray.valueAt(i)).getType()).setVisible(paramBoolean); 
    } 
    return getInsetsFromState(paramInsetsState, paramRect, paramSparseIntArray);
  }
  
  private Insets getInsetsFromState(InsetsState paramInsetsState, Rect paramRect, SparseIntArray paramSparseIntArray) {
    WindowInsets windowInsets = paramInsetsState.calculateInsets(paramRect, null, false, false, null, 16, 0, 0, paramSparseIntArray);
    int i = this.mTypes;
    return 



      
      windowInsets.getInsets(i);
  }
  
  private Insets sanitize(Insets paramInsets) {
    Insets insets = paramInsets;
    if (paramInsets == null)
      insets = getCurrentInsets(); 
    if (hasZeroInsetsIme())
      return insets; 
    return Insets.max(Insets.min(insets, this.mShownInsets), this.mHiddenInsets);
  }
  
  private static float sanitize(float paramFloat) {
    float f = 1.0F;
    if (paramFloat >= 1.0F) {
      paramFloat = f;
    } else if (paramFloat <= 0.0F) {
      paramFloat = 0.0F;
    } 
    return paramFloat;
  }
  
  private void updateLeashesForSide(int paramInt1, int paramInt2, int paramInt3, int paramInt4, ArrayList<SyncRtSurfaceTransactionApplier.SurfaceParams> paramArrayList, InsetsState paramInsetsState, Float paramFloat) {
    ArraySet<InsetsSourceControl> arraySet = this.mSideSourceMap.get(paramInt1);
    if (arraySet == null)
      return; 
    for (paramInt3 = arraySet.size() - 1; paramInt3 >= 0; paramInt3--) {
      InsetsSourceControl insetsSourceControl = arraySet.valueAt(paramInt3);
      InsetsSource insetsSource = this.mInitialInsetsState.getSource(insetsSourceControl.getType());
      SurfaceControl surfaceControl = insetsSourceControl.getLeash();
      this.mTmpMatrix.setTranslate((insetsSourceControl.getSurfacePosition()).x, (insetsSourceControl.getSurfacePosition()).y);
      this.mTmpFrame.set(insetsSource.getFrame());
      addTranslationToMatrix(paramInt1, paramInt2, this.mTmpMatrix, this.mTmpFrame);
      boolean bool = this.mHasZeroInsetsIme;
      boolean bool1 = false;
      if (bool && paramInt1 == 3) {
        if (this.mAnimationType == 0 || !this.mFinished)
          bool1 = true; 
      } else if (paramInt4 != 0) {
        bool1 = true;
      } 
      paramInsetsState.getSource(insetsSource.getType()).setVisible(bool1);
      paramInsetsState.getSource(insetsSource.getType()).setFrame(this.mTmpFrame);
      if (surfaceControl != null) {
        SyncRtSurfaceTransactionApplier.SurfaceParams.Builder builder2 = new SyncRtSurfaceTransactionApplier.SurfaceParams.Builder(surfaceControl);
        SyncRtSurfaceTransactionApplier.SurfaceParams.Builder builder3 = builder2.withAlpha(paramFloat.floatValue());
        Matrix matrix = this.mTmpMatrix;
        SyncRtSurfaceTransactionApplier.SurfaceParams.Builder builder1 = builder3.withMatrix(matrix);
        builder1 = builder1.withVisibility(bool1);
        SyncRtSurfaceTransactionApplier.SurfaceParams surfaceParams = builder1.build();
        paramArrayList.add(surfaceParams);
      } 
    } 
  }
  
  private void addTranslationToMatrix(int paramInt1, int paramInt2, Matrix paramMatrix, Rect paramRect) {
    if (paramInt1 != 0) {
      if (paramInt1 != 1) {
        if (paramInt1 != 2) {
          if (paramInt1 == 3) {
            paramMatrix.postTranslate(0.0F, paramInt2);
            paramRect.offset(0, paramInt2);
          } 
        } else {
          paramMatrix.postTranslate(paramInt2, 0.0F);
          paramRect.offset(paramInt2, 0);
        } 
      } else {
        paramMatrix.postTranslate(0.0F, -paramInt2);
        paramRect.offset(0, -paramInt2);
      } 
    } else {
      paramMatrix.postTranslate(-paramInt2, 0.0F);
      paramRect.offset(-paramInt2, 0);
    } 
  }
  
  private static void buildTypeSourcesMap(SparseIntArray paramSparseIntArray, SparseSetArray<InsetsSourceControl> paramSparseSetArray, SparseArray<InsetsSourceControl> paramSparseArray) {
    for (int i = paramSparseIntArray.size() - 1; i >= 0; i--) {
      int j = paramSparseIntArray.keyAt(i);
      int k = paramSparseIntArray.valueAt(i);
      InsetsSourceControl insetsSourceControl = paramSparseArray.get(j);
      if (insetsSourceControl != null)
        paramSparseSetArray.add(k, insetsSourceControl); 
    } 
  }
}
