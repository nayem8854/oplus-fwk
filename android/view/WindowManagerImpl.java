package android.view;

import android.app.ResourcesManager;
import android.content.Context;
import android.graphics.Insets;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.SeempLog;
import com.android.internal.os.IResultReceiver;

public final class WindowManagerImpl implements WindowManager {
  public final Context mContext;
  
  private IBinder mDefaultToken;
  
  private final WindowManagerGlobal mGlobal = WindowManagerGlobal.getInstance();
  
  private final Window mParentWindow;
  
  public WindowManagerImpl(Context paramContext) {
    this(paramContext, null);
  }
  
  private WindowManagerImpl(Context paramContext, Window paramWindow) {
    this.mContext = paramContext;
    this.mParentWindow = paramWindow;
  }
  
  public WindowManagerImpl createLocalWindowManager(Window paramWindow) {
    return new WindowManagerImpl(this.mContext, paramWindow);
  }
  
  public WindowManagerImpl createPresentationWindowManager(Context paramContext) {
    return new WindowManagerImpl(paramContext, this.mParentWindow);
  }
  
  public void setDefaultToken(IBinder paramIBinder) {
    this.mDefaultToken = paramIBinder;
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    SeempLog.record_vg_layout(383, paramLayoutParams);
    applyDefaultToken(paramLayoutParams);
    WindowManagerGlobal windowManagerGlobal = this.mGlobal;
    Display display = this.mContext.getDisplayNoVerify();
    Window window = this.mParentWindow;
    Context context = this.mContext;
    int i = context.getUserId();
    windowManagerGlobal.addView(paramView, paramLayoutParams, display, window, i);
  }
  
  public void updateViewLayout(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    SeempLog.record_vg_layout(384, paramLayoutParams);
    applyDefaultToken(paramLayoutParams);
    this.mGlobal.updateViewLayout(paramView, paramLayoutParams);
  }
  
  private void applyDefaultToken(ViewGroup.LayoutParams paramLayoutParams) {
    if (this.mDefaultToken != null && this.mParentWindow == null)
      if (paramLayoutParams instanceof WindowManager.LayoutParams) {
        paramLayoutParams = paramLayoutParams;
        if (((WindowManager.LayoutParams)paramLayoutParams).token == null)
          ((WindowManager.LayoutParams)paramLayoutParams).token = this.mDefaultToken; 
      } else {
        throw new IllegalArgumentException("Params must be WindowManager.LayoutParams");
      }  
  }
  
  public void removeView(View paramView) {
    this.mGlobal.removeView(paramView, false);
  }
  
  public void removeViewImmediate(View paramView) {
    this.mGlobal.removeView(paramView, true);
  }
  
  public void requestAppKeyboardShortcuts(WindowManager.KeyboardShortcutsReceiver paramKeyboardShortcutsReceiver, int paramInt) {
    Object object = new Object(this, paramKeyboardShortcutsReceiver);
    try {
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      iWindowManager.requestAppKeyboardShortcuts((IResultReceiver)object, paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public Display getDefaultDisplay() {
    return this.mContext.getDisplayNoVerify();
  }
  
  public Region getCurrentImeTouchRegion() {
    try {
      return WindowManagerGlobal.getWindowManagerService().getCurrentImeTouchRegion();
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public void setShouldShowWithInsecureKeyguard(int paramInt, boolean paramBoolean) {
    try {
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      iWindowManager.setShouldShowWithInsecureKeyguard(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void setShouldShowSystemDecors(int paramInt, boolean paramBoolean) {
    try {
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      iWindowManager.setShouldShowSystemDecors(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public boolean shouldShowSystemDecors(int paramInt) {
    try {
      return WindowManagerGlobal.getWindowManagerService().shouldShowSystemDecors(paramInt);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void setShouldShowIme(int paramInt, boolean paramBoolean) {
    try {
      WindowManagerGlobal.getWindowManagerService().setShouldShowIme(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public boolean shouldShowIme(int paramInt) {
    try {
      return WindowManagerGlobal.getWindowManagerService().shouldShowIme(paramInt);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public WindowMetrics getCurrentWindowMetrics() {
    Context context;
    Window window = this.mParentWindow;
    if (window != null) {
      context = window.getContext();
    } else {
      context = this.mContext;
    } 
    Rect rect = getCurrentBounds(context);
    return new WindowMetrics(rect, computeWindowInsets(rect));
  }
  
  private static Rect getCurrentBounds(Context paramContext) {
    synchronized (ResourcesManager.getInstance()) {
      return (paramContext.getResources().getConfiguration()).windowConfiguration.getBounds();
    } 
  }
  
  public WindowMetrics getMaximumWindowMetrics() {
    Rect rect = getMaximumBounds();
    return new WindowMetrics(rect, computeWindowInsets(rect));
  }
  
  private Rect getMaximumBounds() {
    Display display = this.mContext.getDisplayNoVerify();
    Point point = new Point();
    display.getRealSize(point);
    return new Rect(0, 0, point.x, point.y);
  }
  
  private WindowInsets computeWindowInsets(Rect paramRect) {
    IBinder iBinder;
    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
    layoutParams.flags = 65792;
    Window window = this.mParentWindow;
    if (window != null) {
      iBinder = window.getContext().getActivityToken();
    } else {
      iBinder = this.mContext.getActivityToken();
    } 
    layoutParams.token = iBinder;
    layoutParams.systemUiVisibility = 1536;
    layoutParams.setFitInsetsTypes(0);
    layoutParams.setFitInsetsSides(0);
    return getWindowInsetsFromServer(layoutParams, paramRect);
  }
  
  private WindowInsets getWindowInsetsFromServer(WindowManager.LayoutParams paramLayoutParams, Rect paramRect) {
    try {
      DisplayCutout displayCutout;
      Rect rect1 = new Rect();
      this();
      Rect rect2 = new Rect();
      this();
      DisplayCutout.ParcelableWrapper parcelableWrapper = new DisplayCutout.ParcelableWrapper();
      this();
      InsetsState insetsState = new InsetsState();
      this();
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      Context context2 = this.mContext;
      boolean bool1 = iWindowManager.getWindowInsets(paramLayoutParams, context2.getDisplayId(), rect1, rect2, parcelableWrapper, insetsState);
      Context context1 = this.mContext;
      boolean bool2 = context1.getResources().getConfiguration().isScreenRound();
      if (ViewRootImpl.sNewInsetsMode == 2) {
        displayCutout = parcelableWrapper.get();
        try {
          int i = paramLayoutParams.flags;
          return insetsState.calculateInsets(paramRect, null, bool2, bool1, displayCutout, 48, i, 0, null);
        } catch (RemoteException null) {}
      } else {
        WindowInsets.Builder builder = new WindowInsets.Builder();
        this();
        builder = builder.setAlwaysConsumeSystemBars(bool1);
        builder = builder.setRound(bool2);
        builder = builder.setSystemWindowInsets(Insets.of(rect1));
        builder = builder.setStableInsets(Insets.of(rect2));
        return builder.setDisplayCutout(displayCutout.get()).build();
      } 
    } catch (RemoteException remoteException) {}
    throw remoteException.rethrowFromSystemServer();
  }
}
