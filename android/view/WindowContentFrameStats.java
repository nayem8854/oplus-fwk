package android.view;

import android.os.Parcel;
import android.os.Parcelable;

public final class WindowContentFrameStats extends FrameStats implements Parcelable {
  public WindowContentFrameStats() {}
  
  public void init(long paramLong, long[] paramArrayOflong1, long[] paramArrayOflong2, long[] paramArrayOflong3) {
    this.mRefreshPeriodNano = paramLong;
    this.mFramesPostedTimeNano = paramArrayOflong1;
    this.mFramesPresentedTimeNano = paramArrayOflong2;
    this.mFramesReadyTimeNano = paramArrayOflong3;
  }
  
  private WindowContentFrameStats(Parcel paramParcel) {
    this.mRefreshPeriodNano = paramParcel.readLong();
    this.mFramesPostedTimeNano = paramParcel.createLongArray();
    this.mFramesPresentedTimeNano = paramParcel.createLongArray();
    this.mFramesReadyTimeNano = paramParcel.createLongArray();
  }
  
  public long getFramePostedTimeNano(int paramInt) {
    long[] arrayOfLong = this.mFramesPostedTimeNano;
    if (arrayOfLong != null)
      return arrayOfLong[paramInt]; 
    throw new IndexOutOfBoundsException();
  }
  
  public long getFrameReadyTimeNano(int paramInt) {
    long[] arrayOfLong = this.mFramesReadyTimeNano;
    if (arrayOfLong != null)
      return arrayOfLong[paramInt]; 
    throw new IndexOutOfBoundsException();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mRefreshPeriodNano);
    paramParcel.writeLongArray(this.mFramesPostedTimeNano);
    paramParcel.writeLongArray(this.mFramesPresentedTimeNano);
    paramParcel.writeLongArray(this.mFramesReadyTimeNano);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("WindowContentFrameStats[");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("frameCount:");
    stringBuilder2.append(getFrameCount());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", fromTimeNano:");
    stringBuilder2.append(getStartTimeNano());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", toTimeNano:");
    stringBuilder2.append(getEndTimeNano());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append(']');
    return stringBuilder1.toString();
  }
  
  public static final Parcelable.Creator<WindowContentFrameStats> CREATOR = new Parcelable.Creator<WindowContentFrameStats>() {
      public WindowContentFrameStats createFromParcel(Parcel param1Parcel) {
        return new WindowContentFrameStats(param1Parcel);
      }
      
      public WindowContentFrameStats[] newArray(int param1Int) {
        return new WindowContentFrameStats[param1Int];
      }
    };
  
  private long[] mFramesPostedTimeNano;
  
  private long[] mFramesReadyTimeNano;
}
