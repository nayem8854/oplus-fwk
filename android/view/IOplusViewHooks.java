package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import com.oplus.screenshot.IOplusLongshotController;
import com.oplus.screenshot.OplusLongshotViewInfo;
import com.oplus.view.IOplusScrollBarEffect;
import com.oplus.view.OplusScrollBarEffect;

public interface IOplusViewHooks extends IOplusScrollBarEffect.ViewCallback, IOplusCommonFeature {
  public static final IOplusViewHooks DEFAULT = (IOplusViewHooks)new Object();
  
  default IOplusViewHooks getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusViewHooks;
  }
  
  default boolean awakenScrollBars() {
    return false;
  }
  
  default boolean isLayoutRtl() {
    return false;
  }
  
  default boolean isLongshotConnected() {
    return false;
  }
  
  default boolean isOplusOSStyle() {
    return false;
  }
  
  default boolean isOplusStyle() {
    return false;
  }
  
  default void performClick() {}
  
  default int getOverScrollMode(int paramInt) {
    return paramInt;
  }
  
  default IOplusScrollBarEffect getScrollBarEffect() {
    return OplusScrollBarEffect.NO_EFFECT;
  }
  
  default boolean findViewsLongshotInfo(OplusLongshotViewInfo paramOplusLongshotViewInfo) {
    return false;
  }
  
  default IOplusLongshotController getLongshotController() {
    return null;
  }
  
  default boolean overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean1, int paramInt9, boolean paramBoolean2) {
    return false;
  }
  
  default IOplusViewConfigHelper getOplusViewConfigHelper(Context paramContext) {
    return IOplusViewConfigHelper.DEFAULT;
  }
  
  default Bitmap getOplusCustomDrawingCache(Rect paramRect, int paramInt1, int paramInt2) {
    return null;
  }
  
  default boolean needHook() {
    return false;
  }
}
