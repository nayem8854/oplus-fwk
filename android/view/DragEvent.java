package android.view;

import android.content.ClipData;
import android.content.ClipDescription;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.view.IDragAndDropPermissions;

public class DragEvent implements Parcelable {
  public static final int ACTION_DRAG_ENDED = 4;
  
  public static final int ACTION_DRAG_ENTERED = 5;
  
  public static final int ACTION_DRAG_EXITED = 6;
  
  public static final int ACTION_DRAG_LOCATION = 2;
  
  public static final int ACTION_DRAG_STARTED = 1;
  
  public static final int ACTION_DROP = 3;
  
  public static final Parcelable.Creator<DragEvent> CREATOR;
  
  private static final int MAX_RECYCLED = 10;
  
  private static final boolean TRACK_RECYCLED_LOCATION = false;
  
  private static final Object gRecyclerLock = new Object();
  
  private static DragEvent gRecyclerTop;
  
  private static int gRecyclerUsed = 0;
  
  int mAction;
  
  ClipData mClipData;
  
  ClipDescription mClipDescription;
  
  IDragAndDropPermissions mDragAndDropPermissions;
  
  boolean mDragResult;
  
  boolean mEventHandlerWasCalled;
  
  Object mLocalState;
  
  private DragEvent mNext;
  
  private boolean mRecycled;
  
  private RuntimeException mRecycledLocation;
  
  float mX;
  
  float mY;
  
  static {
    gRecyclerTop = null;
    CREATOR = new Parcelable.Creator<DragEvent>() {
        public DragEvent createFromParcel(Parcel param1Parcel) {
          boolean bool;
          DragEvent dragEvent = DragEvent.obtain();
          dragEvent.mAction = param1Parcel.readInt();
          dragEvent.mX = param1Parcel.readFloat();
          dragEvent.mY = param1Parcel.readFloat();
          if (param1Parcel.readInt() != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          dragEvent.mDragResult = bool;
          if (param1Parcel.readInt() != 0)
            dragEvent.mClipData = (ClipData)ClipData.CREATOR.createFromParcel(param1Parcel); 
          if (param1Parcel.readInt() != 0)
            dragEvent.mClipDescription = (ClipDescription)ClipDescription.CREATOR.createFromParcel(param1Parcel); 
          if (param1Parcel.readInt() != 0)
            dragEvent.mDragAndDropPermissions = IDragAndDropPermissions.Stub.asInterface(param1Parcel.readStrongBinder()); 
          return dragEvent;
        }
        
        public DragEvent[] newArray(int param1Int) {
          return new DragEvent[param1Int];
        }
      };
  }
  
  private void init(int paramInt, float paramFloat1, float paramFloat2, ClipDescription paramClipDescription, ClipData paramClipData, IDragAndDropPermissions paramIDragAndDropPermissions, Object paramObject, boolean paramBoolean) {
    this.mAction = paramInt;
    this.mX = paramFloat1;
    this.mY = paramFloat2;
    this.mClipDescription = paramClipDescription;
    this.mClipData = paramClipData;
    this.mDragAndDropPermissions = paramIDragAndDropPermissions;
    this.mLocalState = paramObject;
    this.mDragResult = paramBoolean;
  }
  
  static DragEvent obtain() {
    return obtain(0, 0.0F, 0.0F, null, null, null, null, false);
  }
  
  public static DragEvent obtain(int paramInt, float paramFloat1, float paramFloat2, Object paramObject, ClipDescription paramClipDescription, ClipData paramClipData, IDragAndDropPermissions paramIDragAndDropPermissions, boolean paramBoolean) {
    synchronized (gRecyclerLock) {
      if (gRecyclerTop == null) {
        DragEvent dragEvent1 = new DragEvent();
        this();
        dragEvent1.init(paramInt, paramFloat1, paramFloat2, paramClipDescription, paramClipData, paramIDragAndDropPermissions, paramObject, paramBoolean);
        return dragEvent1;
      } 
      DragEvent dragEvent = gRecyclerTop;
      gRecyclerTop = dragEvent.mNext;
      gRecyclerUsed--;
      dragEvent.mRecycledLocation = null;
      dragEvent.mRecycled = false;
      dragEvent.mNext = null;
      dragEvent.init(paramInt, paramFloat1, paramFloat2, paramClipDescription, paramClipData, paramIDragAndDropPermissions, paramObject, paramBoolean);
      return dragEvent;
    } 
  }
  
  public static DragEvent obtain(DragEvent paramDragEvent) {
    return obtain(paramDragEvent.mAction, paramDragEvent.mX, paramDragEvent.mY, paramDragEvent.mLocalState, paramDragEvent.mClipDescription, paramDragEvent.mClipData, paramDragEvent.mDragAndDropPermissions, paramDragEvent.mDragResult);
  }
  
  public int getAction() {
    return this.mAction;
  }
  
  public float getX() {
    return this.mX;
  }
  
  public float getY() {
    return this.mY;
  }
  
  public ClipData getClipData() {
    return this.mClipData;
  }
  
  public ClipDescription getClipDescription() {
    return this.mClipDescription;
  }
  
  public IDragAndDropPermissions getDragAndDropPermissions() {
    return this.mDragAndDropPermissions;
  }
  
  public Object getLocalState() {
    return this.mLocalState;
  }
  
  public boolean getResult() {
    return this.mDragResult;
  }
  
  public final void recycle() {
    if (!this.mRecycled) {
      this.mRecycled = true;
      this.mClipData = null;
      this.mClipDescription = null;
      this.mLocalState = null;
      this.mEventHandlerWasCalled = false;
      synchronized (gRecyclerLock) {
        if (gRecyclerUsed < 10) {
          gRecyclerUsed++;
          this.mNext = gRecyclerTop;
          gRecyclerTop = this;
        } 
        return;
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(toString());
    stringBuilder.append(" recycled twice!");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DragEvent{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(" action=");
    stringBuilder.append(this.mAction);
    stringBuilder.append(" @ (");
    stringBuilder.append(this.mX);
    stringBuilder.append(", ");
    stringBuilder.append(this.mY);
    stringBuilder.append(") desc=");
    stringBuilder.append(this.mClipDescription);
    stringBuilder.append(" data=");
    stringBuilder.append(this.mClipData);
    stringBuilder.append(" local=");
    stringBuilder.append(this.mLocalState);
    stringBuilder.append(" result=");
    stringBuilder.append(this.mDragResult);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mAction);
    paramParcel.writeFloat(this.mX);
    paramParcel.writeFloat(this.mY);
    paramParcel.writeInt(this.mDragResult);
    if (this.mClipData == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      this.mClipData.writeToParcel(paramParcel, paramInt);
    } 
    if (this.mClipDescription == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      this.mClipDescription.writeToParcel(paramParcel, paramInt);
    } 
    if (this.mDragAndDropPermissions == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      paramParcel.writeStrongBinder(this.mDragAndDropPermissions.asBinder());
    } 
  }
}
