package android.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews.RemoteView;
import com.android.internal.widget.CachingIconView;
import com.android.internal.widget.NotificationExpandButton;
import java.util.ArrayList;

@RemoteView
public class NotificationHeaderView extends ViewGroup {
  private LinearLayout mTransferChip;
  
  private HeaderTouchListener mTouchListener = new HeaderTouchListener();
  
  private int mTotalWidth;
  
  private boolean mShowWorkBadgeAtEnd;
  
  private boolean mShowExpandButtonAtEnd;
  
  private View mSecondaryHeaderText;
  
  ViewOutlineProvider mProvider = (ViewOutlineProvider)new Object(this);
  
  private View mProfileBadge;
  
  private CachingIconView mIcon;
  
  private int mHeaderTextMarginEnd;
  
  private View mHeaderText;
  
  private final int mGravity;
  
  private boolean mExpanded;
  
  private boolean mExpandOnlyOnButton;
  
  private View.OnClickListener mExpandClickListener;
  
  private NotificationExpandButton mExpandButton;
  
  private boolean mEntireHeaderClickable;
  
  private final int mContentEndMargin;
  
  private final int mChildMinWidth;
  
  private Drawable mBackground;
  
  private View.OnClickListener mAppOpsListener;
  
  private View mAppOps;
  
  private View mAppName;
  
  private boolean mAcceptAllTouches;
  
  public static final int NO_COLOR = 1;
  
  public NotificationHeaderView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public NotificationHeaderView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NotificationHeaderView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public NotificationHeaderView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    Resources resources = getResources();
    this.mChildMinWidth = resources.getDimensionPixelSize(17105365);
    this.mContentEndMargin = resources.getDimensionPixelSize(17105345);
    this.mEntireHeaderClickable = resources.getBoolean(17891497);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, new int[] { 16842927 }, paramInt1, paramInt2);
    this.mGravity = typedArray.getInt(0, 0);
    typedArray.recycle();
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mAppName = findViewById(16908761);
    this.mHeaderText = findViewById(16909034);
    this.mSecondaryHeaderText = findViewById(16909036);
    this.mTransferChip = findViewById(16909160);
    this.mExpandButton = findViewById(16908950);
    this.mIcon = findViewById(16908294);
    this.mProfileBadge = findViewById(16909320);
    this.mAppOps = findViewById(16908762);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt2);
    int k = View.MeasureSpec.makeMeasureSpec(i, -2147483648);
    int m = View.MeasureSpec.makeMeasureSpec(j, -2147483648);
    paramInt1 = getPaddingStart();
    int n = getPaddingEnd();
    for (paramInt2 = 0; paramInt2 < getChildCount(); paramInt2++) {
      View view = getChildAt(paramInt2);
      if (view.getVisibility() != 8) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int i1 = getChildMeasureSpec(k, marginLayoutParams.leftMargin + marginLayoutParams.rightMargin, marginLayoutParams.width);
        int i2 = getChildMeasureSpec(m, marginLayoutParams.topMargin + marginLayoutParams.bottomMargin, marginLayoutParams.height);
        view.measure(i1, i2);
        if ((view == this.mExpandButton && this.mShowExpandButtonAtEnd) || view == this.mProfileBadge || view == this.mAppOps || view == this.mTransferChip) {
          n += marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + view.getMeasuredWidth();
        } else {
          paramInt1 += marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + view.getMeasuredWidth();
        } 
      } 
    } 
    paramInt2 = Math.max(this.mHeaderTextMarginEnd, n);
    if (paramInt1 > i - paramInt2) {
      paramInt2 = shrinkViewForOverflow(m, paramInt1 - i + paramInt2, this.mAppName, this.mChildMinWidth);
      paramInt2 = shrinkViewForOverflow(m, paramInt2, this.mHeaderText, 0);
      shrinkViewForOverflow(m, paramInt2, this.mSecondaryHeaderText, 0);
    } 
    paramInt2 = getPaddingEnd();
    this.mTotalWidth = Math.min(paramInt1 + paramInt2, i);
    setMeasuredDimension(i, j);
  }
  
  private int shrinkViewForOverflow(int paramInt1, int paramInt2, View paramView, int paramInt3) {
    int i = paramView.getMeasuredWidth();
    int j = paramInt2;
    if (paramInt2 > 0) {
      j = paramInt2;
      if (paramView.getVisibility() != 8) {
        j = paramInt2;
        if (i > paramInt3) {
          j = Math.max(paramInt3, i - paramInt2);
          paramInt3 = View.MeasureSpec.makeMeasureSpec(j, -2147483648);
          paramView.measure(paramInt3, paramInt1);
          j = paramInt2 - i - j;
        } 
      } 
    } 
    return j;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt4 = getPaddingStart();
    paramInt3 = getMeasuredWidth();
    if ((this.mGravity & 0x1) != 0) {
      paramInt2 = 1;
    } else {
      paramInt2 = 0;
    } 
    paramInt1 = paramInt4;
    if (paramInt2 != 0)
      paramInt1 = paramInt4 + getMeasuredWidth() / 2 - this.mTotalWidth / 2; 
    int i = getChildCount();
    int j = getMeasuredHeight(), k = getPaddingTop(), m = getPaddingBottom();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.getVisibility() != 8) {
        int n = view.getMeasuredHeight();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int i1 = (int)(getPaddingTop() + (j - k - m - n) / 2.0F);
        if ((view == this.mExpandButton && this.mShowExpandButtonAtEnd) || view == this.mProfileBadge || view == this.mAppOps || view == this.mTransferChip) {
          if (paramInt2 == getMeasuredWidth()) {
            paramInt4 = paramInt2 - this.mContentEndMargin;
          } else {
            paramInt4 = paramInt2 - marginLayoutParams.getMarginEnd();
          } 
          paramInt3 = paramInt4 - view.getMeasuredWidth();
          paramInt2 = paramInt3 - marginLayoutParams.getMarginStart();
        } else {
          paramInt3 = paramInt1 + marginLayoutParams.getMarginStart();
          paramInt1 = view.getMeasuredWidth() + paramInt3;
          paramInt4 = paramInt1;
          paramInt1 += marginLayoutParams.getMarginEnd();
        } 
        int i2 = paramInt3, i3 = paramInt4;
        if (getLayoutDirection() == 1) {
          i2 = getWidth() - paramInt4;
          i3 = getWidth() - paramInt3;
        } 
        view.layout(i2, i1, i3, i1 + n);
      } 
    } 
    updateTouchListener();
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new ViewGroup.MarginLayoutParams(getContext(), paramAttributeSet);
  }
  
  public void setHeaderBackgroundDrawable(Drawable paramDrawable) {
    if (paramDrawable != null) {
      setWillNotDraw(false);
      this.mBackground = paramDrawable;
      paramDrawable.setCallback(this);
      setOutlineProvider(this.mProvider);
    } else {
      setWillNotDraw(true);
      this.mBackground = null;
      setOutlineProvider(null);
    } 
    invalidate();
  }
  
  protected void onDraw(Canvas paramCanvas) {
    Drawable drawable = this.mBackground;
    if (drawable != null) {
      drawable.setBounds(0, 0, getWidth(), getHeight());
      this.mBackground.draw(paramCanvas);
    } 
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (super.verifyDrawable(paramDrawable) || paramDrawable == this.mBackground);
  }
  
  protected void drawableStateChanged() {
    Drawable drawable = this.mBackground;
    if (drawable != null && drawable.isStateful())
      this.mBackground.setState(getDrawableState()); 
  }
  
  private void updateTouchListener() {
    if (this.mExpandClickListener == null && this.mAppOpsListener == null) {
      setOnTouchListener(null);
      return;
    } 
    setOnTouchListener(this.mTouchListener);
    this.mTouchListener.bindTouchRects();
  }
  
  public void setAppOpsOnClickListener(View.OnClickListener paramOnClickListener) {
    this.mAppOpsListener = paramOnClickListener;
    updateTouchListener();
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener) {
    this.mExpandClickListener = paramOnClickListener;
    this.mExpandButton.setOnClickListener(paramOnClickListener);
    updateTouchListener();
  }
  
  public int getOriginalIconColor() {
    return this.mIcon.getOriginalIconColor();
  }
  
  public int getOriginalNotificationColor() {
    return this.mExpandButton.getOriginalNotificationColor();
  }
  
  @RemotableViewMethod
  public void setExpanded(boolean paramBoolean) {
    this.mExpanded = paramBoolean;
    updateExpandButton();
  }
  
  private void updateExpandButton() {
    int i, j;
    if (this.mExpanded) {
      i = 17302369;
      j = 17040127;
    } else {
      i = 17302428;
      j = 17040126;
    } 
    this.mExpandButton.setImageDrawable(getContext().getDrawable(i));
    this.mExpandButton.setColorFilter(getOriginalNotificationColor());
    this.mExpandButton.setContentDescription(this.mContext.getText(j));
  }
  
  public void setShowWorkBadgeAtEnd(boolean paramBoolean) {
    if (paramBoolean != this.mShowWorkBadgeAtEnd) {
      setClipToPadding(paramBoolean ^ true);
      this.mShowWorkBadgeAtEnd = paramBoolean;
    } 
  }
  
  public void setShowExpandButtonAtEnd(boolean paramBoolean) {
    if (paramBoolean != this.mShowExpandButtonAtEnd) {
      setClipToPadding(paramBoolean ^ true);
      this.mShowExpandButtonAtEnd = paramBoolean;
    } 
  }
  
  public View getWorkProfileIcon() {
    return this.mProfileBadge;
  }
  
  public CachingIconView getIcon() {
    return this.mIcon;
  }
  
  @RemotableViewMethod
  public void setHeaderTextMarginEnd(int paramInt) {
    if (this.mHeaderTextMarginEnd != paramInt) {
      this.mHeaderTextMarginEnd = paramInt;
      requestLayout();
    } 
  }
  
  public int getHeaderTextMarginEnd() {
    return this.mHeaderTextMarginEnd;
  }
  
  class HeaderTouchListener implements View.OnTouchListener {
    private Rect mAppOpsRect;
    
    private float mDownX;
    
    private float mDownY;
    
    private Rect mExpandButtonRect;
    
    private final ArrayList<Rect> mTouchRects = new ArrayList<>();
    
    private int mTouchSlop;
    
    private boolean mTrackGesture;
    
    final NotificationHeaderView this$0;
    
    public void bindTouchRects() {
      this.mTouchRects.clear();
      addRectAroundView((View)NotificationHeaderView.this.mIcon);
      this.mExpandButtonRect = addRectAroundView((View)NotificationHeaderView.this.mExpandButton);
      Rect rect = addRectAroundView(NotificationHeaderView.this.mAppOps);
      NotificationHeaderView notificationHeaderView = NotificationHeaderView.this;
      notificationHeaderView.setTouchDelegate(new TouchDelegate(rect, notificationHeaderView.mAppOps));
      addWidthRect();
      this.mTouchSlop = ViewConfiguration.get(NotificationHeaderView.this.getContext()).getScaledTouchSlop();
    }
    
    private void addWidthRect() {
      Rect rect = new Rect();
      rect.top = 0;
      rect.bottom = (int)((NotificationHeaderView.this.getResources().getDisplayMetrics()).density * 32.0F);
      rect.left = 0;
      rect.right = NotificationHeaderView.this.getWidth();
      this.mTouchRects.add(rect);
    }
    
    private Rect addRectAroundView(View param1View) {
      Rect rect = getRectAroundView(param1View);
      this.mTouchRects.add(rect);
      return rect;
    }
    
    private Rect getRectAroundView(View param1View) {
      float f1 = (NotificationHeaderView.this.getResources().getDisplayMetrics()).density * 48.0F;
      float f2 = Math.max(f1, param1View.getWidth());
      f1 = Math.max(f1, param1View.getHeight());
      Rect rect = new Rect();
      if (param1View.getVisibility() == 8) {
        param1View = NotificationHeaderView.this.getFirstChildNotGone();
        rect.left = (int)(param1View.getLeft() - f2 / 2.0F);
      } else {
        rect.left = (int)((param1View.getLeft() + param1View.getRight()) / 2.0F - f2 / 2.0F);
      } 
      rect.top = (int)((param1View.getTop() + param1View.getBottom()) / 2.0F - f1 / 2.0F);
      rect.bottom = (int)(rect.top + f1);
      rect.right = (int)(rect.left + f2);
      return rect;
    }
    
    public boolean onTouch(View param1View, MotionEvent param1MotionEvent) {
      float f1 = param1MotionEvent.getX();
      float f2 = param1MotionEvent.getY();
      int i = param1MotionEvent.getActionMasked() & 0xFF;
      if (i != 0) {
        if (i != 1) {
          if (i != 2)
            return this.mTrackGesture; 
          if (this.mTrackGesture) {
            if (Math.abs(this.mDownX - f1) <= this.mTouchSlop) {
              f1 = this.mDownY;
              if (Math.abs(f1 - f2) > this.mTouchSlop) {
                this.mTrackGesture = false;
                return this.mTrackGesture;
              } 
              return this.mTrackGesture;
            } 
          } else {
            return this.mTrackGesture;
          } 
        } else {
          if (this.mTrackGesture) {
            if (NotificationHeaderView.this.mAppOps.isVisibleToUser())
              if (!this.mAppOpsRect.contains((int)f1, (int)f2)) {
                Rect rect = this.mAppOpsRect;
                int j = (int)this.mDownX;
                i = (int)this.mDownY;
                if (rect.contains(j, i)) {
                  NotificationHeaderView.this.mAppOps.performClick();
                  return true;
                } 
              } else {
                NotificationHeaderView.this.mAppOps.performClick();
                return true;
              }  
            NotificationHeaderView.this.mExpandButton.performClick();
          } 
          return this.mTrackGesture;
        } 
      } else {
        this.mTrackGesture = false;
        if (isInside(f1, f2)) {
          this.mDownX = f1;
          this.mDownY = f2;
          this.mTrackGesture = true;
          return true;
        } 
        return this.mTrackGesture;
      } 
      this.mTrackGesture = false;
      return this.mTrackGesture;
    }
    
    private boolean isInside(float param1Float1, float param1Float2) {
      if (NotificationHeaderView.this.mAcceptAllTouches)
        return true; 
      if (NotificationHeaderView.this.mExpandOnlyOnButton)
        return this.mExpandButtonRect.contains((int)param1Float1, (int)param1Float2); 
      for (byte b = 0; b < this.mTouchRects.size(); b++) {
        Rect rect = this.mTouchRects.get(b);
        if (rect.contains((int)param1Float1, (int)param1Float2))
          return true; 
      } 
      return false;
    }
  }
  
  private View getFirstChildNotGone() {
    for (byte b = 0; b < getChildCount(); b++) {
      View view = getChildAt(b);
      if (view.getVisibility() != 8)
        return view; 
    } 
    return this;
  }
  
  public ImageView getExpandButton() {
    return (ImageView)this.mExpandButton;
  }
  
  public boolean hasOverlappingRendering() {
    return false;
  }
  
  public boolean isInTouchRect(float paramFloat1, float paramFloat2) {
    if (this.mExpandClickListener == null)
      return false; 
    return this.mTouchListener.isInside(paramFloat1, paramFloat2);
  }
  
  @RemotableViewMethod
  public void setAcceptAllTouches(boolean paramBoolean) {
    if (this.mEntireHeaderClickable || paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    this.mAcceptAllTouches = paramBoolean;
  }
  
  @RemotableViewMethod
  public void setExpandOnlyOnButton(boolean paramBoolean) {
    this.mExpandOnlyOnButton = paramBoolean;
  }
}
