package android.view;

import oplus.app.IOplusCommonManager;

public interface IOplusBaseWindowManager extends IOplusCommonManager {
  public static final String DESCRIPTOR = "android.view.IWindowManager";
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
}
