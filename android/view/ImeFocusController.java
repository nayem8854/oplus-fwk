package android.view;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.OplusInputMethodManager;
import com.oplus.util.OplusTypeCastingHelper;

public final class ImeFocusController {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "ImeFocusController";
  
  private InputMethodManagerDelegate mDelegate;
  
  private boolean mHasImeFocus = false;
  
  private View mNextServedView;
  
  private View mServedView;
  
  private final ViewRootImpl mViewRootImpl;
  
  ImeFocusController(ViewRootImpl paramViewRootImpl) {
    this.mViewRootImpl = paramViewRootImpl;
  }
  
  private InputMethodManagerDelegate getImmDelegate() {
    InputMethodManagerDelegate inputMethodManagerDelegate = this.mDelegate;
    if (inputMethodManagerDelegate != null)
      return inputMethodManagerDelegate; 
    inputMethodManagerDelegate = ((InputMethodManager)this.mViewRootImpl.mContext.getSystemService(InputMethodManager.class)).getDelegate();
    this.mDelegate = inputMethodManagerDelegate;
    return inputMethodManagerDelegate;
  }
  
  void onMovedToDisplay() {
    this.mDelegate = null;
  }
  
  void onTraversal(boolean paramBoolean, WindowManager.LayoutParams paramLayoutParams) {
    boolean bool = updateImeFocusable(paramLayoutParams, false);
    if (!paramBoolean || isInLocalFocusMode(paramLayoutParams))
      return; 
    if (bool == this.mHasImeFocus)
      return; 
    this.mHasImeFocus = bool;
    if (bool) {
      onPreWindowFocus(true, paramLayoutParams);
      onPostWindowFocus(this.mViewRootImpl.mView.findFocus(), true, paramLayoutParams);
    } 
  }
  
  void onPreWindowFocus(boolean paramBoolean, WindowManager.LayoutParams paramLayoutParams) {
    if (!this.mHasImeFocus || isInLocalFocusMode(paramLayoutParams))
      return; 
    if (paramBoolean)
      getImmDelegate().setCurrentRootView(this.mViewRootImpl); 
  }
  
  boolean updateImeFocusable(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    boolean bool = WindowManager.LayoutParams.mayUseInputMethod(paramLayoutParams.flags);
    if (paramBoolean)
      this.mHasImeFocus = bool; 
    return bool;
  }
  
  void onPostWindowFocus(View paramView, boolean paramBoolean, WindowManager.LayoutParams paramLayoutParams) {
    if (!paramBoolean || !this.mHasImeFocus || isInLocalFocusMode(paramLayoutParams))
      return; 
    paramBoolean = false;
    InputMethodManagerDelegate inputMethodManagerDelegate = getImmDelegate();
    boolean bool = true;
    if (inputMethodManagerDelegate.isRestartOnNextWindowFocus(true))
      paramBoolean = true; 
    if (paramView == null)
      paramView = this.mViewRootImpl.mView; 
    onViewFocusChanged(paramView, true);
    if (this.mServedView != paramView)
      bool = false; 
    boolean bool1 = paramBoolean;
    if (bool) {
      bool1 = paramBoolean;
      if (!inputMethodManagerDelegate.hasActiveConnection(paramView))
        bool1 = true; 
    } 
    inputMethodManagerDelegate.startInputAsyncOnWindowFocusGain(paramView, paramLayoutParams.softInputMode, paramLayoutParams.flags, bool1);
  }
  
  public boolean checkFocus(boolean paramBoolean1, boolean paramBoolean2) {
    InputMethodManagerDelegate inputMethodManagerDelegate = getImmDelegate();
    if (!inputMethodManagerDelegate.isCurrentRootView(this.mViewRootImpl) || (this.mServedView == this.mNextServedView && !paramBoolean1))
      return false; 
    View view = this.mNextServedView;
    if (view == null) {
      inputMethodManagerDelegate.finishInput();
      inputMethodManagerDelegate.closeCurrentIme();
      return false;
    } 
    this.mServedView = view;
    inputMethodManagerDelegate.finishComposingText();
    if (paramBoolean2)
      inputMethodManagerDelegate.startInput(5, null, 0, 0, 0); 
    return true;
  }
  
  void onViewFocusChanged(View paramView, boolean paramBoolean) {
    if (paramView == null || paramView.isTemporarilyDetached())
      return; 
    if (!getImmDelegate().isCurrentRootView(paramView.getViewRootImpl()))
      return; 
    if (!paramView.hasImeFocus() || !paramView.hasWindowFocus())
      return; 
    if (paramBoolean) {
      InputMethodManager inputMethodManager1 = null;
      ViewRootImpl viewRootImpl = this.mViewRootImpl;
      InputMethodManager inputMethodManager2 = inputMethodManager1;
      if (viewRootImpl != null) {
        inputMethodManager2 = inputMethodManager1;
        if (viewRootImpl.mContext != null)
          inputMethodManager2 = (InputMethodManager)this.mViewRootImpl.mContext.getSystemService(InputMethodManager.class); 
      } 
      OplusInputMethodManager oplusInputMethodManager = (OplusInputMethodManager)OplusTypeCastingHelper.typeCasting(OplusInputMethodManager.class, inputMethodManager2);
      if (oplusInputMethodManager != null)
        oplusInputMethodManager.extendInputMethodCompatible(paramView); 
      this.mNextServedView = paramView;
    } 
    this.mViewRootImpl.dispatchCheckFocus();
  }
  
  void onViewDetachedFromWindow(View paramView) {
    if (!getImmDelegate().isCurrentRootView(paramView.getViewRootImpl()))
      return; 
    if (this.mServedView == paramView) {
      this.mNextServedView = null;
      this.mViewRootImpl.dispatchCheckFocus();
    } 
  }
  
  void onWindowDismissed() {
    InputMethodManagerDelegate inputMethodManagerDelegate = getImmDelegate();
    if (!inputMethodManagerDelegate.isCurrentRootView(this.mViewRootImpl))
      return; 
    if (this.mServedView != null)
      inputMethodManagerDelegate.finishInput(); 
    inputMethodManagerDelegate.setCurrentRootView(null);
    this.mHasImeFocus = false;
  }
  
  private static boolean isInLocalFocusMode(WindowManager.LayoutParams paramLayoutParams) {
    boolean bool;
    if ((paramLayoutParams.flags & 0x10000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  int onProcessImeInputStage(Object paramObject, InputEvent paramInputEvent, WindowManager.LayoutParams paramLayoutParams, InputMethodManager.FinishedInputEventCallback paramFinishedInputEventCallback) {
    if (!this.mHasImeFocus || isInLocalFocusMode(paramLayoutParams))
      return 0; 
    Context context = this.mViewRootImpl.mContext;
    InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService(InputMethodManager.class);
    if (inputMethodManager == null)
      return 0; 
    return inputMethodManager.dispatchInputEvent(paramInputEvent, paramObject, paramFinishedInputEventCallback, this.mViewRootImpl.mHandler);
  }
  
  public View getServedView() {
    return this.mServedView;
  }
  
  public View getNextServedView() {
    return this.mNextServedView;
  }
  
  public void setServedView(View paramView) {
    this.mServedView = paramView;
  }
  
  public void setNextServedView(View paramView) {
    this.mNextServedView = paramView;
  }
  
  boolean hasImeFocus() {
    return this.mHasImeFocus;
  }
  
  public static interface InputMethodManagerDelegate {
    void closeCurrentIme();
    
    void finishComposingText();
    
    void finishInput();
    
    boolean hasActiveConnection(View param1View);
    
    boolean isCurrentRootView(ViewRootImpl param1ViewRootImpl);
    
    boolean isRestartOnNextWindowFocus(boolean param1Boolean);
    
    void setCurrentRootView(ViewRootImpl param1ViewRootImpl);
    
    boolean startInput(int param1Int1, View param1View, int param1Int2, int param1Int3, int param1Int4);
    
    void startInputAsyncOnWindowFocusGain(View param1View, int param1Int1, int param1Int2, boolean param1Boolean);
  }
}
