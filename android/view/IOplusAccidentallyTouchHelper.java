package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.res.Configuration;
import com.oplus.util.OplusAccidentallyTouchData;

public interface IOplusAccidentallyTouchHelper extends IOplusCommonFeature {
  public static final IOplusAccidentallyTouchHelper DEFAULT = (IOplusAccidentallyTouchHelper)new Object();
  
  default IOplusAccidentallyTouchHelper getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusAccidentallyTouchHelper;
  }
  
  default void initOnAmsReady() {}
  
  default OplusAccidentallyTouchData getAccidentallyTouchData() {
    return new OplusAccidentallyTouchData();
  }
  
  default MotionEvent updatePointerEvent(MotionEvent paramMotionEvent, View paramView, Configuration paramConfiguration) {
    return paramMotionEvent;
  }
  
  default void updataeAccidentPreventionState(Context paramContext, boolean paramBoolean, int paramInt) {}
  
  default void initData(Context paramContext) {}
  
  default boolean getEdgeEnable() {
    return false;
  }
  
  default int getEdgeT1() {
    return 10;
  }
  
  default int getEdgeT2() {
    return 30;
  }
  
  default int getOriEdgeT1() {
    return 10;
  }
}
