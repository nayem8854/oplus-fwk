package android.view;

import android.graphics.Rect;
import java.util.function.Consumer;

public interface ScrollCaptureCallback {
  void onScrollCaptureEnd(Runnable paramRunnable);
  
  void onScrollCaptureImageRequest(ScrollCaptureSession paramScrollCaptureSession, Rect paramRect);
  
  void onScrollCaptureSearch(Consumer<Rect> paramConsumer);
  
  void onScrollCaptureStart(ScrollCaptureSession paramScrollCaptureSession, Runnable paramRunnable);
}
