package android.view;

import android.annotation.NonNull;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.android.internal.util.AnnotationValidations;

public final class InputMonitor implements Parcelable {
  public void pilferPointers() {
    try {
      this.mHost.pilferPointers();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void dispose() {
    this.mInputChannel.dispose();
    try {
      this.mHost.dispose();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public InputMonitor(InputChannel paramInputChannel, IInputMonitorHost paramIInputMonitorHost) {
    this.mInputChannel = paramInputChannel;
    AnnotationValidations.validate(NonNull.class, null, paramInputChannel);
    this.mHost = paramIInputMonitorHost;
    AnnotationValidations.validate(NonNull.class, null, paramIInputMonitorHost);
  }
  
  public InputChannel getInputChannel() {
    return this.mInputChannel;
  }
  
  public IInputMonitorHost getHost() {
    return this.mHost;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InputMonitor { inputChannel = ");
    stringBuilder.append(this.mInputChannel);
    stringBuilder.append(", host = ");
    stringBuilder.append(this.mHost);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedObject(this.mInputChannel, paramInt);
    paramParcel.writeStrongInterface(this.mHost);
  }
  
  public int describeContents() {
    return 0;
  }
  
  InputMonitor(Parcel paramParcel) {
    InputChannel inputChannel = (InputChannel)paramParcel.readTypedObject(InputChannel.CREATOR);
    IInputMonitorHost iInputMonitorHost = IInputMonitorHost.Stub.asInterface(paramParcel.readStrongBinder());
    this.mInputChannel = inputChannel;
    AnnotationValidations.validate(NonNull.class, null, inputChannel);
    this.mHost = iInputMonitorHost;
    AnnotationValidations.validate(NonNull.class, null, iInputMonitorHost);
  }
  
  public static final Parcelable.Creator<InputMonitor> CREATOR = new Parcelable.Creator<InputMonitor>() {
      public InputMonitor[] newArray(int param1Int) {
        return new InputMonitor[param1Int];
      }
      
      public InputMonitor createFromParcel(Parcel param1Parcel) {
        return new InputMonitor(param1Parcel);
      }
    };
  
  private static final boolean DEBUG = false;
  
  private static final String TAG = "InputMonitor";
  
  private final IInputMonitorHost mHost;
  
  private final InputChannel mInputChannel;
  
  @Deprecated
  private void __metadata() {}
}
