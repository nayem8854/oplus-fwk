package android.view;

import oplus.app.OplusCommonManager;

public abstract class OplusBaseWindowManager extends OplusCommonManager {
  public OplusBaseWindowManager() {
    super("window");
  }
}
