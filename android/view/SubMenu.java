package android.view;

import android.graphics.drawable.Drawable;

public interface SubMenu extends Menu {
  void clearHeader();
  
  MenuItem getItem();
  
  SubMenu setHeaderIcon(int paramInt);
  
  SubMenu setHeaderIcon(Drawable paramDrawable);
  
  SubMenu setHeaderTitle(int paramInt);
  
  SubMenu setHeaderTitle(CharSequence paramCharSequence);
  
  SubMenu setHeaderView(View paramView);
  
  SubMenu setIcon(int paramInt);
  
  SubMenu setIcon(Drawable paramDrawable);
}
