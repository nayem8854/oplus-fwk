package android.view;

public class InsetsFlags {
  @ExportedProperty(flagMapping = {@FlagToString(equals = 1, mask = 1, name = "OPAQUE_STATUS_BARS"), @FlagToString(equals = 2, mask = 2, name = "OPAQUE_NAVIGATION_BARS"), @FlagToString(equals = 4, mask = 4, name = "LOW_PROFILE_BARS"), @FlagToString(equals = 8, mask = 8, name = "LIGHT_STATUS_BARS"), @FlagToString(equals = 16, mask = 16, name = "LIGHT_NAVIGATION_BARS")})
  public int appearance;
  
  @ExportedProperty(flagMapping = {@FlagToString(equals = 1, mask = 1, name = "SHOW_BARS_BY_SWIPE"), @FlagToString(equals = 2, mask = 2, name = "SHOW_TRANSIENT_BARS_BY_SWIPE")})
  public int behavior;
  
  public static int getAppearance(int paramInt) {
    int i = convertFlag(paramInt, 1, 4);
    int j = convertFlag(paramInt, 8192, 8);
    int k = convertFlag(paramInt, 16, 16);
    int m = convertNoFlag(paramInt, 1073741832, 1);
    paramInt = convertNoFlag(paramInt, -2147450880, 2);
    return 0x0 | i | j | k | m | paramInt;
  }
  
  private static int convertFlag(int paramInt1, int paramInt2, int paramInt3) {
    if ((paramInt1 & paramInt2) != 0) {
      paramInt1 = paramInt3;
    } else {
      paramInt1 = 0;
    } 
    return paramInt1;
  }
  
  private static int convertNoFlag(int paramInt1, int paramInt2, int paramInt3) {
    if ((paramInt1 & paramInt2) != 0)
      paramInt3 = 0; 
    return paramInt3;
  }
}
