package android.view;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.ArraySet;
import android.util.Slog;
import android.util.SparseArray;

public class RemoteAnimationDefinition implements Parcelable {
  public RemoteAnimationDefinition() {
    this.mTransitionAnimationMap = new SparseArray<>();
  }
  
  public void addRemoteAnimation(int paramInt1, int paramInt2, RemoteAnimationAdapter paramRemoteAnimationAdapter) {
    this.mTransitionAnimationMap.put(paramInt1, new RemoteAnimationAdapterEntry(paramRemoteAnimationAdapter, paramInt2));
  }
  
  public void addRemoteAnimation(int paramInt, RemoteAnimationAdapter paramRemoteAnimationAdapter) {
    addRemoteAnimation(paramInt, 0, paramRemoteAnimationAdapter);
  }
  
  public boolean hasTransition(int paramInt, ArraySet<Integer> paramArraySet) {
    boolean bool;
    if (getAdapter(paramInt, paramArraySet) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public RemoteAnimationAdapter getAdapter(int paramInt, ArraySet<Integer> paramArraySet) {
    RemoteAnimationAdapterEntry remoteAnimationAdapterEntry = this.mTransitionAnimationMap.get(paramInt);
    if (remoteAnimationAdapterEntry == null)
      return null; 
    if (remoteAnimationAdapterEntry.activityTypeFilter != 0) {
      paramInt = remoteAnimationAdapterEntry.activityTypeFilter;
      if (!paramArraySet.contains(Integer.valueOf(paramInt)))
        return null; 
    } 
    return remoteAnimationAdapterEntry.adapter;
  }
  
  public RemoteAnimationDefinition(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.mTransitionAnimationMap = new SparseArray<>(i);
    for (byte b = 0; b < i; b++) {
      int j = paramParcel.readInt();
      Parcelable.Creator creator = RemoteAnimationAdapterEntry.CREATOR;
      RemoteAnimationAdapterEntry remoteAnimationAdapterEntry = (RemoteAnimationAdapterEntry)paramParcel.readTypedObject(creator);
      this.mTransitionAnimationMap.put(j, remoteAnimationAdapterEntry);
    } 
  }
  
  public void setCallingPidUid(int paramInt1, int paramInt2) {
    for (int i = this.mTransitionAnimationMap.size() - 1; i >= 0; i--)
      ((RemoteAnimationAdapterEntry)this.mTransitionAnimationMap.valueAt(i)).adapter.setCallingPidUid(paramInt1, paramInt2); 
  }
  
  public void linkToDeath(IBinder.DeathRecipient paramDeathRecipient) {
    byte b = 0;
    try {
      for (; b < this.mTransitionAnimationMap.size(); b++) {
        IBinder iBinder = ((RemoteAnimationAdapterEntry)this.mTransitionAnimationMap.valueAt(b)).adapter.getRunner().asBinder();
        iBinder.linkToDeath(paramDeathRecipient, 0);
      } 
    } catch (RemoteException remoteException) {
      Slog.e("RemoteAnimationDefinition", "Failed to link to death recipient");
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = this.mTransitionAnimationMap.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++) {
      paramParcel.writeInt(this.mTransitionAnimationMap.keyAt(b));
      paramParcel.writeTypedObject(this.mTransitionAnimationMap.valueAt(b), paramInt);
    } 
  }
  
  public static final Parcelable.Creator<RemoteAnimationDefinition> CREATOR = new Parcelable.Creator<RemoteAnimationDefinition>() {
      public RemoteAnimationDefinition createFromParcel(Parcel param1Parcel) {
        return new RemoteAnimationDefinition(param1Parcel);
      }
      
      public RemoteAnimationDefinition[] newArray(int param1Int) {
        return new RemoteAnimationDefinition[param1Int];
      }
    };
  
  private final SparseArray<RemoteAnimationAdapterEntry> mTransitionAnimationMap;
  
  private static class RemoteAnimationAdapterEntry implements Parcelable {
    RemoteAnimationAdapterEntry(RemoteAnimationAdapter param1RemoteAnimationAdapter, int param1Int) {
      this.adapter = param1RemoteAnimationAdapter;
      this.activityTypeFilter = param1Int;
    }
    
    private RemoteAnimationAdapterEntry(Parcel param1Parcel) {
      this.adapter = (RemoteAnimationAdapter)param1Parcel.readParcelable(RemoteAnimationAdapter.class.getClassLoader());
      this.activityTypeFilter = param1Parcel.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeParcelable(this.adapter, param1Int);
      param1Parcel.writeInt(this.activityTypeFilter);
    }
    
    public int describeContents() {
      return 0;
    }
    
    private static final Parcelable.Creator<RemoteAnimationAdapterEntry> CREATOR = new Parcelable.Creator<RemoteAnimationAdapterEntry>() {
        public RemoteAnimationDefinition.RemoteAnimationAdapterEntry createFromParcel(Parcel param2Parcel) {
          return new RemoteAnimationDefinition.RemoteAnimationAdapterEntry(param2Parcel);
        }
        
        public RemoteAnimationDefinition.RemoteAnimationAdapterEntry[] newArray(int param2Int) {
          return new RemoteAnimationDefinition.RemoteAnimationAdapterEntry[param2Int];
        }
      };
    
    final int activityTypeFilter;
    
    final RemoteAnimationAdapter adapter;
  }
  
  class null implements Parcelable.Creator<RemoteAnimationAdapterEntry> {
    public RemoteAnimationDefinition.RemoteAnimationAdapterEntry createFromParcel(Parcel param1Parcel) {
      return new RemoteAnimationDefinition.RemoteAnimationAdapterEntry(param1Parcel);
    }
    
    public RemoteAnimationDefinition.RemoteAnimationAdapterEntry[] newArray(int param1Int) {
      return new RemoteAnimationDefinition.RemoteAnimationAdapterEntry[param1Int];
    }
  }
}
