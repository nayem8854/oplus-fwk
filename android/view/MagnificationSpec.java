package android.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pools;

public class MagnificationSpec implements Parcelable {
  public static final Parcelable.Creator<MagnificationSpec> CREATOR;
  
  private static final int MAX_POOL_SIZE = 20;
  
  private static final Pools.SynchronizedPool<MagnificationSpec> sPool = new Pools.SynchronizedPool<>(20);
  
  public float offsetX;
  
  public float offsetY;
  
  public float scale = 1.0F;
  
  public void initialize(float paramFloat1, float paramFloat2, float paramFloat3) {
    this.scale = paramFloat1;
    this.offsetX = paramFloat2;
    this.offsetY = paramFloat3;
  }
  
  public boolean isNop() {
    boolean bool;
    if (this.scale == 1.0F && this.offsetX == 0.0F && this.offsetY == 0.0F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static MagnificationSpec obtain(MagnificationSpec paramMagnificationSpec) {
    MagnificationSpec magnificationSpec = obtain();
    magnificationSpec.scale = paramMagnificationSpec.scale;
    magnificationSpec.offsetX = paramMagnificationSpec.offsetX;
    magnificationSpec.offsetY = paramMagnificationSpec.offsetY;
    return magnificationSpec;
  }
  
  public static MagnificationSpec obtain() {
    MagnificationSpec magnificationSpec = sPool.acquire();
    if (magnificationSpec == null)
      magnificationSpec = new MagnificationSpec(); 
    return magnificationSpec;
  }
  
  public void recycle() {
    clear();
    sPool.release(this);
  }
  
  public void clear() {
    this.scale = 1.0F;
    this.offsetX = 0.0F;
    this.offsetY = 0.0F;
  }
  
  public void setTo(MagnificationSpec paramMagnificationSpec) {
    this.scale = paramMagnificationSpec.scale;
    this.offsetX = paramMagnificationSpec.offsetX;
    this.offsetY = paramMagnificationSpec.offsetY;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloat(this.scale);
    paramParcel.writeFloat(this.offsetX);
    paramParcel.writeFloat(this.offsetY);
    recycle();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.scale != ((MagnificationSpec)paramObject).scale || this.offsetX != ((MagnificationSpec)paramObject).offsetX || this.offsetY != ((MagnificationSpec)paramObject).offsetY)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    byte b1, b2;
    float f = this.scale;
    int i = 0;
    if (f != 0.0F) {
      b1 = Float.floatToIntBits(f);
    } else {
      b1 = 0;
    } 
    f = this.offsetX;
    if (f != 0.0F) {
      b2 = Float.floatToIntBits(f);
    } else {
      b2 = 0;
    } 
    f = this.offsetY;
    if (f != 0.0F)
      i = Float.floatToIntBits(f); 
    return (b1 * 31 + b2) * 31 + i;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<scale:");
    stringBuilder.append(Float.toString(this.scale));
    stringBuilder.append(",offsetX:");
    stringBuilder.append(Float.toString(this.offsetX));
    stringBuilder.append(",offsetY:");
    stringBuilder.append(Float.toString(this.offsetY));
    stringBuilder.append(">");
    return stringBuilder.toString();
  }
  
  private void initFromParcel(Parcel paramParcel) {
    this.scale = paramParcel.readFloat();
    this.offsetX = paramParcel.readFloat();
    this.offsetY = paramParcel.readFloat();
  }
  
  static {
    CREATOR = new Parcelable.Creator<MagnificationSpec>() {
        public MagnificationSpec[] newArray(int param1Int) {
          return new MagnificationSpec[param1Int];
        }
        
        public MagnificationSpec createFromParcel(Parcel param1Parcel) {
          MagnificationSpec magnificationSpec = MagnificationSpec.obtain();
          magnificationSpec.initFromParcel(param1Parcel);
          return magnificationSpec;
        }
      };
  }
}
