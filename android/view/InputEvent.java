package android.view;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class InputEvent implements Parcelable {
  public static final Parcelable.Creator<InputEvent> CREATOR;
  
  protected static final int PARCEL_TOKEN_KEY_EVENT = 2;
  
  protected static final int PARCEL_TOKEN_MOTION_EVENT = 1;
  
  private static final boolean TRACK_RECYCLED_LOCATION = false;
  
  private static final AtomicInteger mNextSeq = new AtomicInteger();
  
  protected boolean mRecycled;
  
  private RuntimeException mRecycledLocation;
  
  protected int mSeq = mNextSeq.getAndIncrement();
  
  public final InputDevice getDevice() {
    return InputDevice.getDevice(getDeviceId());
  }
  
  public boolean isFromSource(int paramInt) {
    boolean bool;
    if ((getSource() & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void recycle() {
    if (!this.mRecycled) {
      this.mRecycled = true;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(toString());
    stringBuilder.append(" recycled twice!");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public void recycleIfNeededAfterDispatch() {
    recycle();
  }
  
  protected void prepareForReuse() {
    this.mRecycled = false;
    this.mRecycledLocation = null;
    this.mSeq = mNextSeq.getAndIncrement();
  }
  
  public int getSequenceNumber() {
    return this.mSeq;
  }
  
  public int describeContents() {
    return 0;
  }
  
  static {
    CREATOR = new Parcelable.Creator<InputEvent>() {
        public InputEvent createFromParcel(Parcel param1Parcel) {
          int i = param1Parcel.readInt();
          if (i == 2)
            return KeyEvent.createFromParcelBody(param1Parcel); 
          if (i == 1)
            return MotionEvent.createFromParcelBody(param1Parcel); 
          throw new IllegalStateException("Unexpected input event type token in parcel.");
        }
        
        public InputEvent[] newArray(int param1Int) {
          return new InputEvent[param1Int];
        }
      };
  }
  
  public abstract void cancel();
  
  public abstract InputEvent copy();
  
  public abstract int getDeviceId();
  
  public abstract int getDisplayId();
  
  public abstract long getEventTime();
  
  public abstract long getEventTimeNano();
  
  public abstract int getId();
  
  public abstract int getSource();
  
  public abstract boolean isTainted();
  
  public abstract void setDisplayId(int paramInt);
  
  public abstract void setSource(int paramInt);
  
  public abstract void setTainted(boolean paramBoolean);
}
