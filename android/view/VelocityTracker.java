package android.view;

import android.util.Pools;

public final class VelocityTracker {
  private static final int ACTIVE_POINTER_ID = -1;
  
  private static final Pools.SynchronizedPool<VelocityTracker> sPool = new Pools.SynchronizedPool<>(2);
  
  private long mPtr;
  
  private final String mStrategy;
  
  public static VelocityTracker obtain() {
    VelocityTracker velocityTracker = sPool.acquire();
    if (velocityTracker == null)
      velocityTracker = new VelocityTracker(null); 
    return velocityTracker;
  }
  
  public static VelocityTracker obtain(String paramString) {
    if (paramString == null)
      return obtain(); 
    return new VelocityTracker(paramString);
  }
  
  public void recycle() {
    if (this.mStrategy == null) {
      clear();
      sPool.release(this);
    } 
  }
  
  private VelocityTracker(String paramString) {
    this.mPtr = nativeInitialize(paramString);
    this.mStrategy = paramString;
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mPtr != 0L) {
        nativeDispose(this.mPtr);
        this.mPtr = 0L;
      } 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void clear() {
    nativeClear(this.mPtr);
  }
  
  public void addMovement(MotionEvent paramMotionEvent) {
    if (paramMotionEvent != null) {
      nativeAddMovement(this.mPtr, paramMotionEvent);
      return;
    } 
    throw new IllegalArgumentException("event must not be null");
  }
  
  public void computeCurrentVelocity(int paramInt) {
    nativeComputeCurrentVelocity(this.mPtr, paramInt, Float.MAX_VALUE);
  }
  
  public void computeCurrentVelocity(int paramInt, float paramFloat) {
    nativeComputeCurrentVelocity(this.mPtr, paramInt, paramFloat);
  }
  
  public float getXVelocity() {
    return nativeGetXVelocity(this.mPtr, -1);
  }
  
  public float getYVelocity() {
    return nativeGetYVelocity(this.mPtr, -1);
  }
  
  public float getXVelocity(int paramInt) {
    return nativeGetXVelocity(this.mPtr, paramInt);
  }
  
  public float getYVelocity(int paramInt) {
    return nativeGetYVelocity(this.mPtr, paramInt);
  }
  
  public boolean getEstimator(int paramInt, Estimator paramEstimator) {
    if (paramEstimator != null)
      return nativeGetEstimator(this.mPtr, paramInt, paramEstimator); 
    throw new IllegalArgumentException("outEstimator must not be null");
  }
  
  private static native void nativeAddMovement(long paramLong, MotionEvent paramMotionEvent);
  
  private static native void nativeClear(long paramLong);
  
  private static native void nativeComputeCurrentVelocity(long paramLong, int paramInt, float paramFloat);
  
  private static native void nativeDispose(long paramLong);
  
  private static native boolean nativeGetEstimator(long paramLong, int paramInt, Estimator paramEstimator);
  
  private static native float nativeGetXVelocity(long paramLong, int paramInt);
  
  private static native float nativeGetYVelocity(long paramLong, int paramInt);
  
  private static native long nativeInitialize(String paramString);
  
  public static final class Estimator {
    private static final int MAX_DEGREE = 4;
    
    public float confidence;
    
    public int degree;
    
    public final float[] xCoeff = new float[5];
    
    public final float[] yCoeff = new float[5];
    
    public float estimateX(float param1Float) {
      return estimate(param1Float, this.xCoeff);
    }
    
    public float estimateY(float param1Float) {
      return estimate(param1Float, this.yCoeff);
    }
    
    public float getXCoeff(int param1Int) {
      float f;
      if (param1Int <= this.degree) {
        f = this.xCoeff[param1Int];
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public float getYCoeff(int param1Int) {
      float f;
      if (param1Int <= this.degree) {
        f = this.yCoeff[param1Int];
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    private float estimate(float param1Float, float[] param1ArrayOffloat) {
      float f1 = 0.0F;
      float f2 = 1.0F;
      for (byte b = 0; b <= this.degree; b++) {
        f1 += param1ArrayOffloat[b] * f2;
        f2 *= param1Float;
      } 
      return f1;
    }
  }
}
