package android.view;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.SystemProperties;
import android.util.ArraySet;
import android.util.Log;
import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.Collection;

public final class WindowManagerGlobal {
  private static boolean sUseBLASTAdapter = false;
  
  private final Object mLock = new Object();
  
  private final ArrayList<View> mViews = new ArrayList<>();
  
  private final ArrayList<ViewRootImpl> mRoots = new ArrayList<>();
  
  private final ArrayList<WindowManager.LayoutParams> mParams = new ArrayList<>();
  
  private final ArraySet<View> mDyingViews = new ArraySet<>();
  
  public static final int ADD_APP_EXITING = -4;
  
  public static final int ADD_BAD_APP_TOKEN = -1;
  
  public static final int ADD_BAD_SUBWINDOW_TOKEN = -2;
  
  public static final int ADD_DUPLICATE_ADD = -5;
  
  public static final int ADD_FLAG_ALWAYS_CONSUME_SYSTEM_BARS = 4;
  
  public static final int ADD_FLAG_APP_VISIBLE = 2;
  
  public static final int ADD_FLAG_IN_TOUCH_MODE = 1;
  
  public static final int ADD_FLAG_USE_BLAST = 8;
  
  public static final int ADD_FLAG_USE_TRIPLE_BUFFERING = 4;
  
  public static final int ADD_INVALID_DISPLAY = -9;
  
  public static final int ADD_INVALID_TYPE = -10;
  
  public static final int ADD_INVALID_USER = -11;
  
  public static final int ADD_MULTIPLE_SINGLETON = -7;
  
  public static final int ADD_NOT_APP_TOKEN = -3;
  
  public static final int ADD_OKAY = 0;
  
  public static final int ADD_PERMISSION_DENIED = -8;
  
  public static final int ADD_STARTING_NOT_NEEDED = -6;
  
  public static final int ADD_TOO_MANY_TOKENS = -12;
  
  public static final int RELAYOUT_DEFER_SURFACE_DESTROY = 2;
  
  public static final int RELAYOUT_INSETS_PENDING = 1;
  
  public static final int RELAYOUT_RES_BLAST_SYNC = 128;
  
  public static final int RELAYOUT_RES_CONSUME_ALWAYS_SYSTEM_BARS = 64;
  
  public static final int RELAYOUT_RES_DRAG_RESIZING_DOCKED = 8;
  
  public static final int RELAYOUT_RES_DRAG_RESIZING_FREEFORM = 16;
  
  public static final int RELAYOUT_RES_FIRST_TIME = 2;
  
  public static final int RELAYOUT_RES_IN_TOUCH_MODE = 1;
  
  public static final int RELAYOUT_RES_SURFACE_CHANGED = 4;
  
  public static final int RELAYOUT_RES_SURFACE_RESIZED = 32;
  
  private static final String TAG = "WindowManager";
  
  private static WindowManagerGlobal sDefaultWindowManager;
  
  private static IWindowManager sWindowManagerService;
  
  private static IWindowSession sWindowSession;
  
  private Runnable mSystemPropertyUpdater;
  
  public static void initialize() {
    getWindowManagerService();
  }
  
  public static WindowManagerGlobal getInstance() {
    // Byte code:
    //   0: ldc android/view/WindowManagerGlobal
    //   2: monitorenter
    //   3: getstatic android/view/WindowManagerGlobal.sDefaultWindowManager : Landroid/view/WindowManagerGlobal;
    //   6: ifnonnull -> 21
    //   9: new android/view/WindowManagerGlobal
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/view/WindowManagerGlobal.sDefaultWindowManager : Landroid/view/WindowManagerGlobal;
    //   21: getstatic android/view/WindowManagerGlobal.sDefaultWindowManager : Landroid/view/WindowManagerGlobal;
    //   24: astore_0
    //   25: ldc android/view/WindowManagerGlobal
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/view/WindowManagerGlobal
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #183	-> 0
    //   #184	-> 3
    //   #185	-> 9
    //   #187	-> 21
    //   #188	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	28	30	finally
    //   31	34	30	finally
  }
  
  public static IWindowManager getWindowManagerService() {
    // Byte code:
    //   0: ldc android/view/WindowManagerGlobal
    //   2: monitorenter
    //   3: getstatic android/view/WindowManagerGlobal.sWindowManagerService : Landroid/view/IWindowManager;
    //   6: ifnonnull -> 59
    //   9: ldc 'window'
    //   11: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   14: astore_0
    //   15: aload_0
    //   16: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/view/IWindowManager;
    //   19: astore_0
    //   20: aload_0
    //   21: putstatic android/view/WindowManagerGlobal.sWindowManagerService : Landroid/view/IWindowManager;
    //   24: aload_0
    //   25: ifnull -> 59
    //   28: aload_0
    //   29: invokeinterface getCurrentAnimatorScale : ()F
    //   34: fstore_1
    //   35: fload_1
    //   36: invokestatic setDurationScale : (F)V
    //   39: getstatic android/view/WindowManagerGlobal.sWindowManagerService : Landroid/view/IWindowManager;
    //   42: invokeinterface useBLAST : ()Z
    //   47: putstatic android/view/WindowManagerGlobal.sUseBLASTAdapter : Z
    //   50: goto -> 59
    //   53: astore_0
    //   54: aload_0
    //   55: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   58: athrow
    //   59: getstatic android/view/WindowManagerGlobal.sWindowManagerService : Landroid/view/IWindowManager;
    //   62: astore_0
    //   63: ldc android/view/WindowManagerGlobal
    //   65: monitorexit
    //   66: aload_0
    //   67: areturn
    //   68: astore_0
    //   69: ldc android/view/WindowManagerGlobal
    //   71: monitorexit
    //   72: aload_0
    //   73: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #193	-> 0
    //   #194	-> 3
    //   #195	-> 9
    //   #196	-> 9
    //   #195	-> 15
    //   #198	-> 24
    //   #199	-> 28
    //   #200	-> 28
    //   #199	-> 35
    //   #201	-> 39
    //   #203	-> 53
    //   #204	-> 54
    //   #207	-> 59
    //   #208	-> 68
    // Exception table:
    //   from	to	target	type
    //   3	9	68	finally
    //   9	15	68	finally
    //   15	24	68	finally
    //   28	35	53	android/os/RemoteException
    //   28	35	68	finally
    //   35	39	53	android/os/RemoteException
    //   35	39	68	finally
    //   39	50	53	android/os/RemoteException
    //   39	50	68	finally
    //   54	59	68	finally
    //   59	66	68	finally
    //   69	72	68	finally
  }
  
  public static IWindowSession getWindowSession() {
    // Byte code:
    //   0: ldc android/view/WindowManagerGlobal
    //   2: monitorenter
    //   3: getstatic android/view/WindowManagerGlobal.sWindowSession : Landroid/view/IWindowSession;
    //   6: astore_0
    //   7: aload_0
    //   8: ifnonnull -> 45
    //   11: invokestatic ensureDefaultInstanceForDefaultDisplayIfNecessary : ()V
    //   14: invokestatic getWindowManagerService : ()Landroid/view/IWindowManager;
    //   17: astore_1
    //   18: new android/view/WindowManagerGlobal$1
    //   21: astore_0
    //   22: aload_0
    //   23: invokespecial <init> : ()V
    //   26: aload_1
    //   27: aload_0
    //   28: invokeinterface openSession : (Landroid/view/IWindowSessionCallback;)Landroid/view/IWindowSession;
    //   33: putstatic android/view/WindowManagerGlobal.sWindowSession : Landroid/view/IWindowSession;
    //   36: goto -> 45
    //   39: astore_0
    //   40: aload_0
    //   41: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   44: athrow
    //   45: getstatic android/view/WindowManagerGlobal.sWindowSession : Landroid/view/IWindowSession;
    //   48: astore_0
    //   49: ldc android/view/WindowManagerGlobal
    //   51: monitorexit
    //   52: aload_0
    //   53: areturn
    //   54: astore_0
    //   55: ldc android/view/WindowManagerGlobal
    //   57: monitorexit
    //   58: aload_0
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #213	-> 0
    //   #214	-> 3
    //   #219	-> 11
    //   #220	-> 14
    //   #221	-> 18
    //   #230	-> 36
    //   #228	-> 39
    //   #229	-> 40
    //   #232	-> 45
    //   #233	-> 54
    // Exception table:
    //   from	to	target	type
    //   3	7	54	finally
    //   11	14	39	android/os/RemoteException
    //   11	14	54	finally
    //   14	18	39	android/os/RemoteException
    //   14	18	54	finally
    //   18	36	39	android/os/RemoteException
    //   18	36	54	finally
    //   40	45	54	finally
    //   45	52	54	finally
    //   55	58	54	finally
  }
  
  public static IWindowSession peekWindowSession() {
    // Byte code:
    //   0: ldc android/view/WindowManagerGlobal
    //   2: monitorenter
    //   3: getstatic android/view/WindowManagerGlobal.sWindowSession : Landroid/view/IWindowSession;
    //   6: astore_0
    //   7: ldc android/view/WindowManagerGlobal
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc android/view/WindowManagerGlobal
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #238	-> 0
    //   #239	-> 3
    //   #240	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	10	12	finally
    //   13	16	12	finally
  }
  
  public static boolean useBLAST() {
    return sUseBLASTAdapter;
  }
  
  public String[] getViewRootNames() {
    synchronized (this.mLock) {
      int i = this.mRoots.size();
      String[] arrayOfString = new String[i];
      for (byte b = 0; b < i; b++)
        arrayOfString[b] = getWindowName(this.mRoots.get(b)); 
      return arrayOfString;
    } 
  }
  
  public ArrayList<ViewRootImpl> getRootViews(IBinder paramIBinder) {
    ArrayList<ViewRootImpl> arrayList = new ArrayList();
    synchronized (this.mLock) {
      int i = this.mRoots.size();
      for (byte b = 0; b < i; b++) {
        WindowManager.LayoutParams layoutParams = this.mParams.get(b);
        if (layoutParams.token == null)
          continue; 
        if (layoutParams.token != paramIBinder) {
          boolean bool1 = false;
          boolean bool2 = bool1;
          if (layoutParams.type >= 1000) {
            bool2 = bool1;
            if (layoutParams.type <= 1999) {
              byte b1 = 0;
              while (true) {
                bool2 = bool1;
                if (b1 < i) {
                  View view = this.mViews.get(b1);
                  WindowManager.LayoutParams layoutParams1 = this.mParams.get(b1);
                  if (layoutParams.token == view.getWindowToken() && layoutParams1.token == paramIBinder) {
                    bool2 = true;
                    break;
                  } 
                  b1++;
                  continue;
                } 
                break;
              } 
            } 
          } 
          if (!bool2)
            continue; 
        } 
        arrayList.add(this.mRoots.get(b));
        continue;
      } 
      return arrayList;
    } 
  }
  
  public ArrayList<View> getWindowViews() {
    synchronized (this.mLock) {
      ArrayList<View> arrayList = new ArrayList();
      this((Collection)this.mViews);
      return arrayList;
    } 
  }
  
  public View getWindowView(IBinder paramIBinder) {
    synchronized (this.mLock) {
      int i = this.mViews.size();
      for (byte b = 0; b < i; b++) {
        View view = this.mViews.get(b);
        if (view.getWindowToken() == paramIBinder)
          return view; 
      } 
      return null;
    } 
  }
  
  public View getRootView(String paramString) {
    synchronized (this.mLock) {
      for (int i = this.mRoots.size() - 1; i >= 0; i--) {
        ViewRootImpl viewRootImpl = this.mRoots.get(i);
        if (paramString.equals(getWindowName(viewRootImpl)))
          return viewRootImpl.getView(); 
      } 
      return null;
    } 
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams, Display paramDisplay, Window paramWindow, int paramInt) {
    if (paramView != null) {
      if (paramDisplay != null) {
        if (paramLayoutParams instanceof WindowManager.LayoutParams) {
          WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)paramLayoutParams;
          if (paramWindow != null) {
            paramWindow.adjustLayoutParamsForSubWindow(layoutParams);
          } else {
            Context context = paramView.getContext();
            if (context != null && (
              (context.getApplicationInfo()).flags & 0x20000000) != 0)
              layoutParams.flags |= 0x1000000; 
          } 
          Window window = null;
          paramLayoutParams = null;
          synchronized (this.mLock) {
            StringBuilder stringBuilder1;
            IllegalStateException illegalStateException;
            StringBuilder stringBuilder3;
            if (this.mSystemPropertyUpdater == null) {
              Runnable runnable = new Runnable() {
                  final WindowManagerGlobal this$0;
                  
                  public void run() {
                    synchronized (WindowManagerGlobal.this.mLock) {
                      for (int i = WindowManagerGlobal.this.mRoots.size() - 1; i >= 0; i--)
                        ((ViewRootImpl)WindowManagerGlobal.this.mRoots.get(i)).loadSystemProperties(); 
                      return;
                    } 
                  }
                };
              super(this);
              this.mSystemPropertyUpdater = runnable;
              SystemProperties.addChangeCallback(runnable);
            } 
            int i = findViewLocked(paramView, false);
            if (i >= 0)
              if (this.mDyingViews.contains(paramView)) {
                ((ViewRootImpl)this.mRoots.get(i)).doDie();
              } else {
                illegalStateException = new IllegalStateException();
                stringBuilder1 = new StringBuilder();
                this();
                stringBuilder1.append("View ");
                stringBuilder1.append(paramView);
                stringBuilder1.append(" has already been added to the window manager.");
                this(stringBuilder1.toString());
                throw illegalStateException;
              }  
            paramWindow = window;
            if (layoutParams.type >= 1000) {
              paramWindow = window;
              if (layoutParams.type <= 1999) {
                int j = this.mViews.size();
                byte b = 0;
                while (true) {
                  stringBuilder3 = stringBuilder1;
                  if (b < j) {
                    if (((ViewRootImpl)this.mRoots.get(b)).mWindow.asBinder() == layoutParams.token)
                      View view = this.mViews.get(b); 
                    b++;
                    continue;
                  } 
                  break;
                } 
              } 
            } 
            ViewRootImpl viewRootImpl = new ViewRootImpl();
            this(paramView.getContext(), (Display)illegalStateException);
            paramView.setLayoutParams(layoutParams);
            this.mViews.add(paramView);
            this.mRoots.add(viewRootImpl);
            this.mParams.add(layoutParams);
            StringBuilder stringBuilder2 = new StringBuilder();
            this();
            stringBuilder2.append("Add to mViews: ");
            stringBuilder2.append(paramView);
            stringBuilder2.append(",pkg= ");
            stringBuilder2.append(layoutParams.packageName);
            Log.d("WindowManager", stringBuilder2.toString());
            try {
              viewRootImpl.setView(paramView, layoutParams, (View)stringBuilder3, paramInt);
              return;
            } catch (RuntimeException runtimeException) {
              if (i >= 0)
                removeViewLocked(i, true); 
              throw runtimeException;
            } 
          } 
        } 
        throw new IllegalArgumentException("Params must be WindowManager.LayoutParams");
      } 
      throw new IllegalArgumentException("display must not be null");
    } 
    throw new IllegalArgumentException("view must not be null");
  }
  
  public void updateViewLayout(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    if (paramView != null) {
      if (paramLayoutParams instanceof WindowManager.LayoutParams) {
        WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)paramLayoutParams;
        paramView.setLayoutParams(layoutParams);
        synchronized (this.mLock) {
          int i = findViewLocked(paramView, true);
          ViewRootImpl viewRootImpl = this.mRoots.get(i);
          this.mParams.remove(i);
          this.mParams.add(i, layoutParams);
          viewRootImpl.setLayoutParams(layoutParams, false);
          return;
        } 
      } 
      throw new IllegalArgumentException("Params must be WindowManager.LayoutParams");
    } 
    throw new IllegalArgumentException("view must not be null");
  }
  
  public void removeView(View paramView, boolean paramBoolean) {
    if (paramView != null)
      synchronized (this.mLock) {
        int i = findViewLocked(paramView, true);
        View view = ((ViewRootImpl)this.mRoots.get(i)).getView();
        removeViewLocked(i, paramBoolean);
        if (view == paramView)
          return; 
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Calling with view ");
        stringBuilder.append(paramView);
        stringBuilder.append(" but the ViewAncestor is attached to ");
        stringBuilder.append(view);
        this(stringBuilder.toString());
        throw illegalStateException;
      }  
    throw new IllegalArgumentException("view must not be null");
  }
  
  public void closeAll(IBinder paramIBinder, String paramString1, String paramString2) {
    closeAllExceptView(paramIBinder, null, paramString1, paramString2);
  }
  
  public void closeAllExceptView(IBinder paramIBinder, View paramView, String paramString1, String paramString2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore #5
    //   6: aload #5
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mViews : Ljava/util/ArrayList;
    //   13: invokevirtual size : ()I
    //   16: istore #6
    //   18: iconst_0
    //   19: istore #7
    //   21: iload #7
    //   23: iload #6
    //   25: if_icmpge -> 205
    //   28: aload_2
    //   29: ifnull -> 45
    //   32: aload_0
    //   33: getfield mViews : Ljava/util/ArrayList;
    //   36: iload #7
    //   38: invokevirtual get : (I)Ljava/lang/Object;
    //   41: aload_2
    //   42: if_acmpeq -> 199
    //   45: aload_1
    //   46: ifnull -> 72
    //   49: aload_0
    //   50: getfield mParams : Ljava/util/ArrayList;
    //   53: astore #8
    //   55: aload #8
    //   57: iload #7
    //   59: invokevirtual get : (I)Ljava/lang/Object;
    //   62: checkcast android/view/WindowManager$LayoutParams
    //   65: getfield token : Landroid/os/IBinder;
    //   68: aload_1
    //   69: if_acmpne -> 199
    //   72: aload_0
    //   73: getfield mRoots : Ljava/util/ArrayList;
    //   76: iload #7
    //   78: invokevirtual get : (I)Ljava/lang/Object;
    //   81: checkcast android/view/ViewRootImpl
    //   84: astore #9
    //   86: aload_3
    //   87: ifnull -> 192
    //   90: new android/view/WindowLeaked
    //   93: astore #8
    //   95: new java/lang/StringBuilder
    //   98: astore #10
    //   100: aload #10
    //   102: invokespecial <init> : ()V
    //   105: aload #10
    //   107: aload #4
    //   109: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload #10
    //   115: ldc_w ' '
    //   118: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   121: pop
    //   122: aload #10
    //   124: aload_3
    //   125: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   128: pop
    //   129: aload #10
    //   131: ldc_w ' has leaked window '
    //   134: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   137: pop
    //   138: aload #10
    //   140: aload #9
    //   142: invokevirtual getView : ()Landroid/view/View;
    //   145: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   148: pop
    //   149: aload #10
    //   151: ldc_w ' that was originally added here'
    //   154: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   157: pop
    //   158: aload #8
    //   160: aload #10
    //   162: invokevirtual toString : ()Ljava/lang/String;
    //   165: invokespecial <init> : (Ljava/lang/String;)V
    //   168: aload #8
    //   170: aload #9
    //   172: invokevirtual getLocation : ()Landroid/view/WindowLeaked;
    //   175: invokevirtual getStackTrace : ()[Ljava/lang/StackTraceElement;
    //   178: invokevirtual setStackTrace : ([Ljava/lang/StackTraceElement;)V
    //   181: ldc 'WindowManager'
    //   183: ldc_w ''
    //   186: aload #8
    //   188: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   191: pop
    //   192: aload_0
    //   193: iload #7
    //   195: iconst_0
    //   196: invokespecial removeViewLocked : (IZ)V
    //   199: iinc #7, 1
    //   202: goto -> 21
    //   205: aload #5
    //   207: monitorexit
    //   208: return
    //   209: astore_1
    //   210: aload #5
    //   212: monitorexit
    //   213: aload_1
    //   214: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #485	-> 0
    //   #486	-> 9
    //   #487	-> 18
    //   #488	-> 28
    //   #489	-> 55
    //   #490	-> 72
    //   #492	-> 86
    //   #493	-> 90
    //   #495	-> 138
    //   #496	-> 168
    //   #497	-> 181
    //   #500	-> 192
    //   #487	-> 199
    //   #503	-> 205
    //   #504	-> 208
    //   #503	-> 209
    // Exception table:
    //   from	to	target	type
    //   9	18	209	finally
    //   32	45	209	finally
    //   49	55	209	finally
    //   55	72	209	finally
    //   72	86	209	finally
    //   90	138	209	finally
    //   138	168	209	finally
    //   168	181	209	finally
    //   181	192	209	finally
    //   192	199	209	finally
    //   205	208	209	finally
    //   210	213	209	finally
  }
  
  private void removeViewLocked(int paramInt, boolean paramBoolean) {
    ViewRootImpl viewRootImpl = this.mRoots.get(paramInt);
    View view = viewRootImpl.getView();
    if (viewRootImpl != null)
      viewRootImpl.getImeFocusController().onWindowDismissed(); 
    paramBoolean = viewRootImpl.die(paramBoolean);
    if (view != null) {
      view.assignParent(null);
      if (paramBoolean)
        this.mDyingViews.add(view); 
    } 
  }
  
  void doRemoveView(ViewRootImpl paramViewRootImpl) {
    synchronized (this.mLock) {
      int i = this.mRoots.indexOf(paramViewRootImpl);
      if (i >= 0) {
        this.mRoots.remove(i);
        this.mParams.remove(i);
        View view = this.mViews.remove(i);
        this.mDyingViews.remove(view);
      } 
      boolean bool = this.mRoots.isEmpty();
      if (ThreadedRenderer.sTrimForeground && ThreadedRenderer.isAvailable())
        doTrimForeground(); 
      if (bool)
        InsetsAnimationThread.release(); 
      return;
    } 
  }
  
  private int findViewLocked(View paramView, boolean paramBoolean) {
    int i = this.mViews.indexOf(paramView);
    if (!paramBoolean || i >= 0)
      return i; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("View=");
    stringBuilder.append(paramView);
    stringBuilder.append(" not attached to window manager");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static boolean shouldDestroyEglContext(int paramInt) {
    if (paramInt >= 80)
      return true; 
    if (paramInt >= 60 && 
      !ActivityManager.isHighEndGfx())
      return true; 
    return false;
  }
  
  public void trimMemory(int paramInt) {
    if (ThreadedRenderer.isAvailable()) {
      int i = paramInt;
      if (shouldDestroyEglContext(paramInt))
        synchronized (this.mLock) {
          for (i = this.mRoots.size() - 1; i >= 0; i--)
            ((ViewRootImpl)this.mRoots.get(i)).destroyHardwareResources(); 
          i = paramInt;
          if (paramInt != 1000)
            i = 80; 
        }  
      ThreadedRenderer.trimMemory(i);
      if (ThreadedRenderer.sTrimForeground)
        doTrimForeground(); 
    } 
  }
  
  public static void trimForeground() {
    if (ThreadedRenderer.sTrimForeground && ThreadedRenderer.isAvailable()) {
      WindowManagerGlobal windowManagerGlobal = getInstance();
      windowManagerGlobal.doTrimForeground();
    } 
  }
  
  private void doTrimForeground() {
    boolean bool = false;
    synchronized (this.mLock) {
      for (int i = this.mRoots.size() - 1; i >= 0; i--) {
        ViewRootImpl viewRootImpl = this.mRoots.get(i);
        if (viewRootImpl.mView != null && viewRootImpl.getHostVisibility() == 0 && viewRootImpl.mAttachInfo.mThreadedRenderer != null) {
          bool = true;
        } else {
          viewRootImpl.destroyHardwareResources();
        } 
      } 
      if (!bool)
        ThreadedRenderer.trimMemory(80); 
      return;
    } 
  }
  
  public void dumpGfxInfo(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) {
    // Byte code:
    //   0: new java/io/FileOutputStream
    //   3: dup
    //   4: aload_1
    //   5: invokespecial <init> : (Ljava/io/FileDescriptor;)V
    //   8: astore_3
    //   9: new com/android/internal/util/FastPrintWriter
    //   12: dup
    //   13: aload_3
    //   14: invokespecial <init> : (Ljava/io/OutputStream;)V
    //   17: astore_3
    //   18: aload_0
    //   19: getfield mLock : Ljava/lang/Object;
    //   22: astore #4
    //   24: aload #4
    //   26: monitorenter
    //   27: aload_0
    //   28: getfield mViews : Ljava/util/ArrayList;
    //   31: invokevirtual size : ()I
    //   34: istore #5
    //   36: aload_3
    //   37: ldc_w 'Profile data in ms:'
    //   40: invokevirtual println : (Ljava/lang/String;)V
    //   43: iconst_0
    //   44: istore #6
    //   46: iload #6
    //   48: iload #5
    //   50: if_icmpge -> 137
    //   53: aload_0
    //   54: getfield mRoots : Ljava/util/ArrayList;
    //   57: iload #6
    //   59: invokevirtual get : (I)Ljava/lang/Object;
    //   62: checkcast android/view/ViewRootImpl
    //   65: astore #7
    //   67: aload #7
    //   69: invokestatic getWindowName : (Landroid/view/ViewRootImpl;)Ljava/lang/String;
    //   72: astore #8
    //   74: aload_3
    //   75: ldc_w '\\n\\t%s (visibility=%d)'
    //   78: iconst_2
    //   79: anewarray java/lang/Object
    //   82: dup
    //   83: iconst_0
    //   84: aload #8
    //   86: aastore
    //   87: dup
    //   88: iconst_1
    //   89: aload #7
    //   91: invokevirtual getHostVisibility : ()I
    //   94: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   97: aastore
    //   98: invokevirtual printf : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    //   101: pop
    //   102: aload #7
    //   104: invokevirtual getView : ()Landroid/view/View;
    //   107: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   110: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   113: astore #8
    //   115: aload #8
    //   117: ifnull -> 131
    //   120: aload #8
    //   122: aload_3
    //   123: aload_1
    //   124: aload_2
    //   125: invokevirtual dumpGfxInfo : (Ljava/io/PrintWriter;Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    //   128: goto -> 131
    //   131: iinc #6, 1
    //   134: goto -> 46
    //   137: aload_3
    //   138: ldc_w '\\nView hierarchy:\\n'
    //   141: invokevirtual println : (Ljava/lang/String;)V
    //   144: new android/view/ViewRootImpl$GfxInfo
    //   147: astore_1
    //   148: aload_1
    //   149: invokespecial <init> : ()V
    //   152: iconst_0
    //   153: istore #6
    //   155: iload #6
    //   157: iload #5
    //   159: if_icmpge -> 262
    //   162: aload_0
    //   163: getfield mRoots : Ljava/util/ArrayList;
    //   166: iload #6
    //   168: invokevirtual get : (I)Ljava/lang/Object;
    //   171: checkcast android/view/ViewRootImpl
    //   174: astore #8
    //   176: aload #8
    //   178: invokevirtual getGfxInfo : ()Landroid/view/ViewRootImpl$GfxInfo;
    //   181: astore_2
    //   182: aload_1
    //   183: aload_2
    //   184: invokevirtual add : (Landroid/view/ViewRootImpl$GfxInfo;)V
    //   187: aload #8
    //   189: invokestatic getWindowName : (Landroid/view/ViewRootImpl;)Ljava/lang/String;
    //   192: astore #8
    //   194: aload_2
    //   195: getfield viewCount : I
    //   198: istore #9
    //   200: aload_2
    //   201: getfield renderNodeMemoryUsage : J
    //   204: l2f
    //   205: ldc_w 1024.0
    //   208: fdiv
    //   209: fstore #10
    //   211: aload_3
    //   212: ldc_w '  %s\\n  %d views, %.2f kB of render nodes'
    //   215: iconst_3
    //   216: anewarray java/lang/Object
    //   219: dup
    //   220: iconst_0
    //   221: aload #8
    //   223: aastore
    //   224: dup
    //   225: iconst_1
    //   226: iload #9
    //   228: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   231: aastore
    //   232: dup
    //   233: iconst_2
    //   234: fload #10
    //   236: invokestatic valueOf : (F)Ljava/lang/Float;
    //   239: aastore
    //   240: invokevirtual printf : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    //   243: pop
    //   244: aload_3
    //   245: ldc_w '\\n\\n'
    //   248: iconst_0
    //   249: anewarray java/lang/Object
    //   252: invokevirtual printf : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    //   255: pop
    //   256: iinc #6, 1
    //   259: goto -> 155
    //   262: aload_3
    //   263: ldc_w '\\nTotal %-15s: %d\\n'
    //   266: iconst_2
    //   267: anewarray java/lang/Object
    //   270: dup
    //   271: iconst_0
    //   272: ldc_w 'ViewRootImpl'
    //   275: aastore
    //   276: dup
    //   277: iconst_1
    //   278: iload #5
    //   280: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   283: aastore
    //   284: invokevirtual printf : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    //   287: pop
    //   288: aload_3
    //   289: ldc_w 'Total %-15s: %d\\n'
    //   292: iconst_2
    //   293: anewarray java/lang/Object
    //   296: dup
    //   297: iconst_0
    //   298: ldc_w 'attached Views'
    //   301: aastore
    //   302: dup
    //   303: iconst_1
    //   304: aload_1
    //   305: getfield viewCount : I
    //   308: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   311: aastore
    //   312: invokevirtual printf : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    //   315: pop
    //   316: aload_1
    //   317: getfield renderNodeMemoryUsage : J
    //   320: l2f
    //   321: ldc_w 1024.0
    //   324: fdiv
    //   325: fstore #10
    //   327: aload_1
    //   328: getfield renderNodeMemoryAllocated : J
    //   331: l2f
    //   332: ldc_w 1024.0
    //   335: fdiv
    //   336: fstore #11
    //   338: aload_3
    //   339: ldc_w 'Total %-15s: %.2f kB (used) / %.2f kB (capacity)\\n\\n'
    //   342: iconst_3
    //   343: anewarray java/lang/Object
    //   346: dup
    //   347: iconst_0
    //   348: ldc_w 'RenderNode'
    //   351: aastore
    //   352: dup
    //   353: iconst_1
    //   354: fload #10
    //   356: invokestatic valueOf : (F)Ljava/lang/Float;
    //   359: aastore
    //   360: dup
    //   361: iconst_2
    //   362: fload #11
    //   364: invokestatic valueOf : (F)Ljava/lang/Float;
    //   367: aastore
    //   368: invokevirtual printf : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    //   371: pop
    //   372: aload #4
    //   374: monitorexit
    //   375: aload_3
    //   376: invokevirtual flush : ()V
    //   379: return
    //   380: astore_1
    //   381: aload #4
    //   383: monitorexit
    //   384: aload_1
    //   385: athrow
    //   386: astore_1
    //   387: goto -> 395
    //   390: astore_1
    //   391: goto -> 381
    //   394: astore_1
    //   395: aload_3
    //   396: invokevirtual flush : ()V
    //   399: aload_1
    //   400: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #623	-> 0
    //   #624	-> 9
    //   #626	-> 18
    //   #627	-> 27
    //   #629	-> 36
    //   #631	-> 43
    //   #632	-> 53
    //   #633	-> 67
    //   #634	-> 74
    //   #636	-> 102
    //   #637	-> 102
    //   #638	-> 115
    //   #639	-> 120
    //   #638	-> 131
    //   #631	-> 131
    //   #643	-> 137
    //   #645	-> 144
    //   #647	-> 152
    //   #648	-> 162
    //   #649	-> 176
    //   #650	-> 182
    //   #652	-> 187
    //   #653	-> 194
    //   #654	-> 200
    //   #653	-> 211
    //   #655	-> 244
    //   #647	-> 256
    //   #658	-> 262
    //   #659	-> 288
    //   #660	-> 316
    //   #661	-> 327
    //   #662	-> 338
    //   #660	-> 338
    //   #663	-> 372
    //   #665	-> 375
    //   #666	-> 379
    //   #667	-> 379
    //   #663	-> 380
    //   #665	-> 386
    //   #663	-> 390
    //   #665	-> 394
    //   #666	-> 399
    // Exception table:
    //   from	to	target	type
    //   18	27	394	finally
    //   27	36	380	finally
    //   36	43	380	finally
    //   53	67	380	finally
    //   67	74	380	finally
    //   74	102	380	finally
    //   102	115	380	finally
    //   120	128	390	finally
    //   137	144	390	finally
    //   144	152	390	finally
    //   162	176	390	finally
    //   176	182	390	finally
    //   182	187	390	finally
    //   187	194	390	finally
    //   194	200	390	finally
    //   200	211	390	finally
    //   211	244	390	finally
    //   244	256	390	finally
    //   262	288	390	finally
    //   288	316	390	finally
    //   316	327	390	finally
    //   327	338	390	finally
    //   338	372	390	finally
    //   372	375	390	finally
    //   381	384	390	finally
    //   384	386	386	finally
  }
  
  private static String getWindowName(ViewRootImpl paramViewRootImpl) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramViewRootImpl.mWindowAttributes.getTitle());
    stringBuilder.append("/");
    stringBuilder.append(paramViewRootImpl.getClass().getName());
    stringBuilder.append('@');
    stringBuilder.append(Integer.toHexString(paramViewRootImpl.hashCode()));
    return stringBuilder.toString();
  }
  
  public void setStoppedState(IBinder paramIBinder, boolean paramBoolean) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_0
    //   3: getfield mLock : Ljava/lang/Object;
    //   6: astore #4
    //   8: aload #4
    //   10: monitorenter
    //   11: aload_0
    //   12: getfield mViews : Ljava/util/ArrayList;
    //   15: invokevirtual size : ()I
    //   18: istore #5
    //   20: iinc #5, -1
    //   23: iload #5
    //   25: iflt -> 141
    //   28: aload_1
    //   29: ifnull -> 54
    //   32: aload_3
    //   33: astore #6
    //   35: aload_0
    //   36: getfield mParams : Ljava/util/ArrayList;
    //   39: iload #5
    //   41: invokevirtual get : (I)Ljava/lang/Object;
    //   44: checkcast android/view/WindowManager$LayoutParams
    //   47: getfield token : Landroid/os/IBinder;
    //   50: aload_1
    //   51: if_acmpne -> 132
    //   54: aload_0
    //   55: getfield mRoots : Ljava/util/ArrayList;
    //   58: iload #5
    //   60: invokevirtual get : (I)Ljava/lang/Object;
    //   63: checkcast android/view/ViewRootImpl
    //   66: astore #7
    //   68: aload #7
    //   70: getfield mThread : Ljava/lang/Thread;
    //   73: invokestatic currentThread : ()Ljava/lang/Thread;
    //   76: if_acmpne -> 88
    //   79: aload #7
    //   81: iload_2
    //   82: invokevirtual setWindowStopped : (Z)V
    //   85: goto -> 116
    //   88: aload_3
    //   89: astore #6
    //   91: aload_3
    //   92: ifnonnull -> 105
    //   95: new java/util/ArrayList
    //   98: astore #6
    //   100: aload #6
    //   102: invokespecial <init> : ()V
    //   105: aload #6
    //   107: aload #7
    //   109: invokevirtual add : (Ljava/lang/Object;)Z
    //   112: pop
    //   113: aload #6
    //   115: astore_3
    //   116: aload_0
    //   117: aload #7
    //   119: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   122: getfield mWindowToken : Landroid/os/IBinder;
    //   125: iload_2
    //   126: invokevirtual setStoppedState : (Landroid/os/IBinder;Z)V
    //   129: aload_3
    //   130: astore #6
    //   132: iinc #5, -1
    //   135: aload #6
    //   137: astore_3
    //   138: goto -> 23
    //   141: aload #4
    //   143: monitorexit
    //   144: aload_3
    //   145: ifnull -> 195
    //   148: aload_3
    //   149: invokevirtual size : ()I
    //   152: iconst_1
    //   153: isub
    //   154: istore #5
    //   156: iload #5
    //   158: iflt -> 195
    //   161: aload_3
    //   162: iload #5
    //   164: invokevirtual get : (I)Ljava/lang/Object;
    //   167: checkcast android/view/ViewRootImpl
    //   170: astore_1
    //   171: aload_1
    //   172: getfield mHandler : Landroid/view/ViewRootImpl$ViewRootHandler;
    //   175: new android/view/_$$Lambda$WindowManagerGlobal$2bR3FsEm4EdRwuXfttH0wA2xOW4
    //   178: dup
    //   179: aload_1
    //   180: iload_2
    //   181: invokespecial <init> : (Landroid/view/ViewRootImpl;Z)V
    //   184: lconst_0
    //   185: invokevirtual runWithScissors : (Ljava/lang/Runnable;J)Z
    //   188: pop
    //   189: iinc #5, -1
    //   192: goto -> 156
    //   195: return
    //   196: astore_1
    //   197: aload #4
    //   199: monitorexit
    //   200: aload_1
    //   201: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #675	-> 0
    //   #676	-> 2
    //   #677	-> 11
    //   #678	-> 20
    //   #679	-> 28
    //   #680	-> 54
    //   #682	-> 68
    //   #683	-> 79
    //   #685	-> 88
    //   #686	-> 95
    //   #688	-> 105
    //   #693	-> 116
    //   #678	-> 132
    //   #696	-> 141
    //   #701	-> 144
    //   #702	-> 148
    //   #703	-> 161
    //   #704	-> 171
    //   #702	-> 189
    //   #707	-> 195
    //   #696	-> 196
    // Exception table:
    //   from	to	target	type
    //   11	20	196	finally
    //   35	54	196	finally
    //   54	68	196	finally
    //   68	79	196	finally
    //   79	85	196	finally
    //   95	105	196	finally
    //   105	113	196	finally
    //   116	129	196	finally
    //   141	144	196	finally
    //   197	200	196	finally
  }
  
  public void reportNewConfiguration(Configuration paramConfiguration) {
    synchronized (this.mLock) {
      int i = this.mViews.size();
      Configuration configuration = new Configuration();
      this(paramConfiguration);
      for (byte b = 0; b < i; b++) {
        ViewRootImpl viewRootImpl = this.mRoots.get(b);
        viewRootImpl.requestUpdateConfiguration(configuration);
      } 
      return;
    } 
  }
  
  public void changeCanvasOpacity(IBinder paramIBinder, boolean paramBoolean) {
    if (paramIBinder == null)
      return; 
    synchronized (this.mLock) {
      for (int i = this.mParams.size() - 1; i >= 0; i--) {
        if (((WindowManager.LayoutParams)this.mParams.get(i)).token == paramIBinder) {
          ((ViewRootImpl)this.mRoots.get(i)).changeCanvasOpacity(paramBoolean);
          return;
        } 
      } 
      return;
    } 
  }
}
