package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWindowSessionCallback extends IInterface {
  void onAnimatorScaleChanged(float paramFloat) throws RemoteException;
  
  class Default implements IWindowSessionCallback {
    public void onAnimatorScaleChanged(float param1Float) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowSessionCallback {
    private static final String DESCRIPTOR = "android.view.IWindowSessionCallback";
    
    static final int TRANSACTION_onAnimatorScaleChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IWindowSessionCallback");
    }
    
    public static IWindowSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IWindowSessionCallback");
      if (iInterface != null && iInterface instanceof IWindowSessionCallback)
        return (IWindowSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onAnimatorScaleChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IWindowSessionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IWindowSessionCallback");
      float f = param1Parcel1.readFloat();
      onAnimatorScaleChanged(f);
      return true;
    }
    
    private static class Proxy implements IWindowSessionCallback {
      public static IWindowSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IWindowSessionCallback";
      }
      
      public void onAnimatorScaleChanged(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindowSessionCallback");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IWindowSessionCallback.Stub.getDefaultImpl() != null) {
            IWindowSessionCallback.Stub.getDefaultImpl().onAnimatorScaleChanged(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowSessionCallback param1IWindowSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowSessionCallback != null) {
          Proxy.sDefaultImpl = param1IWindowSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
