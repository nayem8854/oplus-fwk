package android.view;

import android.graphics.Rect;
import com.android.internal.util.Preconditions;
import java.util.concurrent.Executor;

public abstract class CompositionSamplingListener {
  private final Executor mExecutor;
  
  private long mNativeListener;
  
  public CompositionSamplingListener(Executor paramExecutor) {
    this.mExecutor = paramExecutor;
    this.mNativeListener = nativeCreate(this);
  }
  
  public void destroy() {
    if (this.mNativeListener == 0L)
      return; 
    unregister(this);
    nativeDestroy(this.mNativeListener);
    this.mNativeListener = 0L;
  }
  
  protected void finalize() throws Throwable {
    try {
      destroy();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public static void register(CompositionSamplingListener paramCompositionSamplingListener, int paramInt, SurfaceControl paramSurfaceControl, Rect paramRect) {
    boolean bool;
    long l1 = paramCompositionSamplingListener.mNativeListener, l2 = 0L;
    if (l1 == 0L)
      return; 
    if (paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "default display only for now");
    if (paramSurfaceControl != null)
      l2 = paramSurfaceControl.mNativeObject; 
    nativeRegister(paramCompositionSamplingListener.mNativeListener, l2, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public static void unregister(CompositionSamplingListener paramCompositionSamplingListener) {
    long l = paramCompositionSamplingListener.mNativeListener;
    if (l == 0L)
      return; 
    nativeUnregister(l);
  }
  
  private static void dispatchOnSampleCollected(CompositionSamplingListener paramCompositionSamplingListener, float paramFloat) {
    paramCompositionSamplingListener.mExecutor.execute(new _$$Lambda$CompositionSamplingListener$hrbPutjnKRv7VkkiY9eg32N6QA8(paramCompositionSamplingListener, paramFloat));
  }
  
  private static native long nativeCreate(CompositionSamplingListener paramCompositionSamplingListener);
  
  private static native void nativeDestroy(long paramLong);
  
  private static native void nativeRegister(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  private static native void nativeUnregister(long paramLong);
  
  public abstract void onSampleCollected(float paramFloat);
}
