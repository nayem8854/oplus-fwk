package android.view;

import android.os.Parcel;
import android.os.RemoteException;
import com.oplus.direct.OplusDirectFindCmd;
import java.lang.ref.WeakReference;

public class OplusDummyDirectViewHelper implements IOplusDirectViewHelper {
  public OplusDummyDirectViewHelper(WeakReference<ViewRootImpl> paramWeakReference) {}
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) {
    return false;
  }
  
  public void directFindCmd(OplusDirectFindCmd paramOplusDirectFindCmd) throws RemoteException {}
}
