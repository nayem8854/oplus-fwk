package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDisplayFoldListener extends IInterface {
  void onDisplayFoldChanged(int paramInt, boolean paramBoolean) throws RemoteException;
  
  class Default implements IDisplayFoldListener {
    public void onDisplayFoldChanged(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDisplayFoldListener {
    private static final String DESCRIPTOR = "android.view.IDisplayFoldListener";
    
    static final int TRANSACTION_onDisplayFoldChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IDisplayFoldListener");
    }
    
    public static IDisplayFoldListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IDisplayFoldListener");
      if (iInterface != null && iInterface instanceof IDisplayFoldListener)
        return (IDisplayFoldListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onDisplayFoldChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IDisplayFoldListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IDisplayFoldListener");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onDisplayFoldChanged(param1Int1, bool);
      return true;
    }
    
    private static class Proxy implements IDisplayFoldListener {
      public static IDisplayFoldListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IDisplayFoldListener";
      }
      
      public void onDisplayFoldChanged(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IDisplayFoldListener");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IDisplayFoldListener.Stub.getDefaultImpl() != null) {
            IDisplayFoldListener.Stub.getDefaultImpl().onDisplayFoldChanged(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDisplayFoldListener param1IDisplayFoldListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDisplayFoldListener != null) {
          Proxy.sDefaultImpl = param1IDisplayFoldListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDisplayFoldListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
