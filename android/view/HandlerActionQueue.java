package android.view;

import android.os.Handler;

public class HandlerActionQueue {
  private HandlerAction[] mActions;
  
  private int mCount;
  
  public void post(Runnable paramRunnable) {
    postDelayed(paramRunnable, 0L);
  }
  
  public void postDelayed(Runnable paramRunnable, long paramLong) {
    // Byte code:
    //   0: new android/view/HandlerActionQueue$HandlerAction
    //   3: dup
    //   4: aload_1
    //   5: lload_2
    //   6: invokespecial <init> : (Ljava/lang/Runnable;J)V
    //   9: astore_1
    //   10: aload_0
    //   11: monitorenter
    //   12: aload_0
    //   13: getfield mActions : [Landroid/view/HandlerActionQueue$HandlerAction;
    //   16: ifnonnull -> 27
    //   19: aload_0
    //   20: iconst_4
    //   21: anewarray android/view/HandlerActionQueue$HandlerAction
    //   24: putfield mActions : [Landroid/view/HandlerActionQueue$HandlerAction;
    //   27: aload_0
    //   28: aload_0
    //   29: getfield mActions : [Landroid/view/HandlerActionQueue$HandlerAction;
    //   32: aload_0
    //   33: getfield mCount : I
    //   36: aload_1
    //   37: invokestatic append : ([Ljava/lang/Object;ILjava/lang/Object;)[Ljava/lang/Object;
    //   40: checkcast [Landroid/view/HandlerActionQueue$HandlerAction;
    //   43: putfield mActions : [Landroid/view/HandlerActionQueue$HandlerAction;
    //   46: aload_0
    //   47: aload_0
    //   48: getfield mCount : I
    //   51: iconst_1
    //   52: iadd
    //   53: putfield mCount : I
    //   56: aload_0
    //   57: monitorexit
    //   58: return
    //   59: astore_1
    //   60: aload_0
    //   61: monitorexit
    //   62: aload_1
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #37	-> 0
    //   #39	-> 10
    //   #40	-> 12
    //   #41	-> 19
    //   #43	-> 27
    //   #44	-> 46
    //   #45	-> 56
    //   #46	-> 58
    //   #45	-> 59
    // Exception table:
    //   from	to	target	type
    //   12	19	59	finally
    //   19	27	59	finally
    //   27	46	59	finally
    //   46	56	59	finally
    //   56	58	59	finally
    //   60	62	59	finally
  }
  
  public void removeCallbacks(Runnable paramRunnable) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCount : I
    //   6: istore_2
    //   7: iconst_0
    //   8: istore_3
    //   9: aload_0
    //   10: getfield mActions : [Landroid/view/HandlerActionQueue$HandlerAction;
    //   13: astore #4
    //   15: iconst_0
    //   16: istore #5
    //   18: iload #5
    //   20: iload_2
    //   21: if_icmpge -> 63
    //   24: aload #4
    //   26: iload #5
    //   28: aaload
    //   29: aload_1
    //   30: invokevirtual matches : (Ljava/lang/Runnable;)Z
    //   33: ifeq -> 39
    //   36: goto -> 57
    //   39: iload_3
    //   40: iload #5
    //   42: if_icmpeq -> 54
    //   45: aload #4
    //   47: iload_3
    //   48: aload #4
    //   50: iload #5
    //   52: aaload
    //   53: aastore
    //   54: iinc #3, 1
    //   57: iinc #5, 1
    //   60: goto -> 18
    //   63: aload_0
    //   64: iload_3
    //   65: putfield mCount : I
    //   68: iload_3
    //   69: iload_2
    //   70: if_icmpge -> 84
    //   73: aload #4
    //   75: iload_3
    //   76: aconst_null
    //   77: aastore
    //   78: iinc #3, 1
    //   81: goto -> 68
    //   84: aload_0
    //   85: monitorexit
    //   86: return
    //   87: astore_1
    //   88: aload_0
    //   89: monitorexit
    //   90: aload_1
    //   91: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #49	-> 0
    //   #50	-> 2
    //   #51	-> 7
    //   #53	-> 9
    //   #54	-> 15
    //   #55	-> 24
    //   #58	-> 36
    //   #61	-> 39
    //   #64	-> 45
    //   #67	-> 54
    //   #54	-> 57
    //   #71	-> 63
    //   #74	-> 68
    //   #75	-> 73
    //   #74	-> 78
    //   #77	-> 84
    //   #78	-> 86
    //   #77	-> 87
    // Exception table:
    //   from	to	target	type
    //   2	7	87	finally
    //   9	15	87	finally
    //   24	36	87	finally
    //   63	68	87	finally
    //   84	86	87	finally
    //   88	90	87	finally
  }
  
  public void executeActions(Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActions : [Landroid/view/HandlerActionQueue$HandlerAction;
    //   6: astore_2
    //   7: iconst_0
    //   8: istore_3
    //   9: aload_0
    //   10: getfield mCount : I
    //   13: istore #4
    //   15: iload_3
    //   16: iload #4
    //   18: if_icmpge -> 47
    //   21: aload_2
    //   22: iload_3
    //   23: aaload
    //   24: astore #5
    //   26: aload_1
    //   27: aload #5
    //   29: getfield action : Ljava/lang/Runnable;
    //   32: aload #5
    //   34: getfield delay : J
    //   37: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   40: pop
    //   41: iinc #3, 1
    //   44: goto -> 15
    //   47: aload_0
    //   48: aconst_null
    //   49: putfield mActions : [Landroid/view/HandlerActionQueue$HandlerAction;
    //   52: aload_0
    //   53: iconst_0
    //   54: putfield mCount : I
    //   57: aload_0
    //   58: monitorexit
    //   59: return
    //   60: astore_1
    //   61: aload_0
    //   62: monitorexit
    //   63: aload_1
    //   64: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #81	-> 0
    //   #82	-> 2
    //   #83	-> 7
    //   #84	-> 21
    //   #85	-> 26
    //   #83	-> 41
    //   #88	-> 47
    //   #89	-> 52
    //   #90	-> 57
    //   #91	-> 59
    //   #90	-> 60
    // Exception table:
    //   from	to	target	type
    //   2	7	60	finally
    //   9	15	60	finally
    //   26	41	60	finally
    //   47	52	60	finally
    //   52	57	60	finally
    //   57	59	60	finally
    //   61	63	60	finally
  }
  
  public int size() {
    return this.mCount;
  }
  
  public Runnable getRunnable(int paramInt) {
    if (paramInt < this.mCount)
      return (this.mActions[paramInt]).action; 
    throw new IndexOutOfBoundsException();
  }
  
  public long getDelay(int paramInt) {
    if (paramInt < this.mCount)
      return (this.mActions[paramInt]).delay; 
    throw new IndexOutOfBoundsException();
  }
  
  private static class HandlerAction {
    final Runnable action;
    
    final long delay;
    
    public HandlerAction(Runnable param1Runnable, long param1Long) {
      this.action = param1Runnable;
      this.delay = param1Long;
    }
    
    public boolean matches(Runnable param1Runnable) {
      if (param1Runnable != null || this.action != null) {
        Runnable runnable = this.action;
        return (runnable != null && 
          runnable.equals(param1Runnable));
      } 
      return true;
    }
  }
}
