package android.view;

import android.graphics.RenderNode;

public class ViewAnimationHostBridge implements RenderNode.AnimationHost {
  private final View mView;
  
  public ViewAnimationHostBridge(View paramView) {
    this.mView = paramView;
  }
  
  public void registerAnimatingRenderNode(RenderNode paramRenderNode) {
    this.mView.mAttachInfo.mViewRootImpl.registerAnimatingRenderNode(paramRenderNode);
  }
  
  public void registerVectorDrawableAnimator(NativeVectorDrawableAnimator paramNativeVectorDrawableAnimator) {
    this.mView.mAttachInfo.mViewRootImpl.registerVectorDrawableAnimator(paramNativeVectorDrawableAnimator);
  }
  
  public boolean isAttached() {
    boolean bool;
    if (this.mView.mAttachInfo != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
