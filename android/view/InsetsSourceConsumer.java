package android.view;

import android.graphics.Rect;
import android.os.Debug;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.function.Supplier;

public class InsetsSourceConsumer {
  private static final String TAG = "InsetsSourceConsumer";
  
  protected final InsetsController mController;
  
  private boolean mHasWindowFocus;
  
  private boolean mIsAnimationPending;
  
  private Rect mPendingFrame;
  
  private Rect mPendingVisibleFrame;
  
  protected boolean mRequestedVisible;
  
  private InsetsSourceControl mSourceControl;
  
  protected final InsetsState mState;
  
  private final Supplier<SurfaceControl.Transaction> mTransactionSupplier;
  
  protected final int mType;
  
  public InsetsSourceConsumer(int paramInt, InsetsState paramInsetsState, Supplier<SurfaceControl.Transaction> paramSupplier, InsetsController paramInsetsController) {
    this.mType = paramInt;
    this.mState = paramInsetsState;
    this.mTransactionSupplier = paramSupplier;
    this.mController = paramInsetsController;
    this.mRequestedVisible = InsetsState.getDefaultVisibility(paramInt);
  }
  
  public void setControl(InsetsSourceControl paramInsetsSourceControl, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSourceControl : Landroid/view/InsetsSourceControl;
    //   4: astore #4
    //   6: aload #4
    //   8: aload_1
    //   9: if_acmpne -> 13
    //   12: return
    //   13: aload #4
    //   15: ifnull -> 28
    //   18: aload #4
    //   20: invokevirtual getLeash : ()Landroid/view/SurfaceControl;
    //   23: astore #4
    //   25: goto -> 31
    //   28: aconst_null
    //   29: astore #4
    //   31: aload_0
    //   32: getfield mSourceControl : Landroid/view/InsetsSourceControl;
    //   35: astore #5
    //   37: aload_0
    //   38: aload_1
    //   39: putfield mSourceControl : Landroid/view/InsetsSourceControl;
    //   42: aload_1
    //   43: ifnull -> 146
    //   46: getstatic android/view/InsetsController.DEBUG : Z
    //   49: ifeq -> 146
    //   52: new java/lang/StringBuilder
    //   55: dup
    //   56: invokespecial <init> : ()V
    //   59: astore #6
    //   61: aload_1
    //   62: invokevirtual getType : ()I
    //   65: invokestatic typeToString : (I)Ljava/lang/String;
    //   68: astore #7
    //   70: aload_0
    //   71: getfield mController : Landroid/view/InsetsController;
    //   74: astore #8
    //   76: aload #8
    //   78: invokevirtual getHost : ()Landroid/view/InsetsController$Host;
    //   81: invokeinterface getRootViewTitle : ()Ljava/lang/String;
    //   86: astore #8
    //   88: aload #6
    //   90: ldc 'setControl -> %s on %s'
    //   92: iconst_2
    //   93: anewarray java/lang/Object
    //   96: dup
    //   97: iconst_0
    //   98: aload #7
    //   100: aastore
    //   101: dup
    //   102: iconst_1
    //   103: aload #8
    //   105: aastore
    //   106: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   109: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload #6
    //   115: ldc ' caller '
    //   117: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload #6
    //   123: iconst_5
    //   124: invokestatic getCallers : (I)Ljava/lang/String;
    //   127: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: pop
    //   131: aload #6
    //   133: invokevirtual toString : ()Ljava/lang/String;
    //   136: astore #7
    //   138: ldc 'InsetsSourceConsumer'
    //   140: aload #7
    //   142: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   145: pop
    //   146: aload_0
    //   147: getfield mSourceControl : Landroid/view/InsetsSourceControl;
    //   150: ifnonnull -> 320
    //   153: aload_0
    //   154: getfield mController : Landroid/view/InsetsController;
    //   157: aload_0
    //   158: invokevirtual notifyControlRevoked : (Landroid/view/InsetsSourceConsumer;)V
    //   161: aload_0
    //   162: getfield mState : Landroid/view/InsetsState;
    //   165: aload_0
    //   166: invokevirtual getType : ()I
    //   169: invokevirtual getSource : (I)Landroid/view/InsetsSource;
    //   172: astore_1
    //   173: aload_0
    //   174: getfield mController : Landroid/view/InsetsController;
    //   177: astore_2
    //   178: aload_2
    //   179: invokevirtual getLastDispatchedState : ()Landroid/view/InsetsState;
    //   182: aload_0
    //   183: invokevirtual getType : ()I
    //   186: invokevirtual getSource : (I)Landroid/view/InsetsSource;
    //   189: invokevirtual isVisible : ()Z
    //   192: istore #9
    //   194: aload_1
    //   195: iload #9
    //   197: invokevirtual setVisible : (Z)V
    //   200: getstatic android/view/InsetsController.DEBUG : Z
    //   203: ifeq -> 312
    //   206: new java/lang/StringBuilder
    //   209: dup
    //   210: invokespecial <init> : ()V
    //   213: astore_1
    //   214: aload_1
    //   215: ldc 'notifyControlRevoked: '
    //   217: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   220: pop
    //   221: aload_0
    //   222: getfield mController : Landroid/view/InsetsController;
    //   225: astore_2
    //   226: aload_1
    //   227: aload_2
    //   228: invokevirtual getHost : ()Landroid/view/InsetsController$Host;
    //   231: invokeinterface getRootViewTitle : ()Ljava/lang/String;
    //   236: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   239: pop
    //   240: aload_1
    //   241: ldc ', restoreVisible '
    //   243: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   246: pop
    //   247: aload_0
    //   248: getfield mState : Landroid/view/InsetsState;
    //   251: astore_2
    //   252: aload_1
    //   253: aload_2
    //   254: aload_0
    //   255: invokevirtual getType : ()I
    //   258: invokevirtual getSource : (I)Landroid/view/InsetsSource;
    //   261: invokevirtual isVisible : ()Z
    //   264: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   267: pop
    //   268: aload_1
    //   269: ldc ', type '
    //   271: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   274: pop
    //   275: aload_1
    //   276: aload_0
    //   277: invokevirtual getType : ()I
    //   280: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   283: pop
    //   284: aload_1
    //   285: ldc ', caller '
    //   287: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   290: pop
    //   291: aload_1
    //   292: iconst_5
    //   293: invokestatic getCallers : (I)Ljava/lang/String;
    //   296: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   299: pop
    //   300: aload_1
    //   301: invokevirtual toString : ()Ljava/lang/String;
    //   304: astore_1
    //   305: ldc 'InsetsSourceConsumer'
    //   307: aload_1
    //   308: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   311: pop
    //   312: aload_0
    //   313: invokevirtual applyLocalVisibilityOverride : ()Z
    //   316: pop
    //   317: goto -> 550
    //   320: aload_0
    //   321: invokevirtual isRequestedVisibleAwaitingControl : ()Z
    //   324: istore #9
    //   326: iload #9
    //   328: aload_0
    //   329: getfield mState : Landroid/view/InsetsState;
    //   332: aload_0
    //   333: getfield mType : I
    //   336: invokevirtual getSource : (I)Landroid/view/InsetsSource;
    //   339: invokevirtual isVisible : ()Z
    //   342: if_icmpeq -> 351
    //   345: iconst_1
    //   346: istore #10
    //   348: goto -> 354
    //   351: iconst_0
    //   352: istore #10
    //   354: aload_1
    //   355: invokevirtual getLeash : ()Landroid/view/SurfaceControl;
    //   358: ifnull -> 465
    //   361: iload #10
    //   363: ifne -> 373
    //   366: aload_0
    //   367: getfield mIsAnimationPending : Z
    //   370: ifeq -> 465
    //   373: getstatic android/view/InsetsController.DEBUG : Z
    //   376: ifeq -> 421
    //   379: aload_0
    //   380: getfield mController : Landroid/view/InsetsController;
    //   383: astore_1
    //   384: aload_1
    //   385: invokevirtual getHost : ()Landroid/view/InsetsController$Host;
    //   388: invokeinterface getRootViewTitle : ()Ljava/lang/String;
    //   393: astore_1
    //   394: ldc 'InsetsSourceConsumer'
    //   396: ldc 'Gaining control in %s, requestedVisible: %b'
    //   398: iconst_2
    //   399: anewarray java/lang/Object
    //   402: dup
    //   403: iconst_0
    //   404: aload_1
    //   405: aastore
    //   406: dup
    //   407: iconst_1
    //   408: iload #9
    //   410: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   413: aastore
    //   414: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   417: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   420: pop
    //   421: iload #9
    //   423: ifeq -> 443
    //   426: aload_2
    //   427: iconst_0
    //   428: aload_2
    //   429: iconst_0
    //   430: iaload
    //   431: aload_0
    //   432: invokevirtual getType : ()I
    //   435: invokestatic toPublicType : (I)I
    //   438: ior
    //   439: iastore
    //   440: goto -> 457
    //   443: aload_3
    //   444: iconst_0
    //   445: aload_3
    //   446: iconst_0
    //   447: iaload
    //   448: aload_0
    //   449: invokevirtual getType : ()I
    //   452: invokestatic toPublicType : (I)I
    //   455: ior
    //   456: iastore
    //   457: aload_0
    //   458: iconst_0
    //   459: putfield mIsAnimationPending : Z
    //   462: goto -> 550
    //   465: iload #10
    //   467: ifeq -> 475
    //   470: aload_0
    //   471: iconst_1
    //   472: putfield mIsAnimationPending : Z
    //   475: aload_0
    //   476: invokevirtual applyLocalVisibilityOverride : ()Z
    //   479: ifeq -> 489
    //   482: aload_0
    //   483: getfield mController : Landroid/view/InsetsController;
    //   486: invokevirtual notifyVisibilityChanged : ()V
    //   489: aload_0
    //   490: getfield mSourceControl : Landroid/view/InsetsSourceControl;
    //   493: invokevirtual getLeash : ()Landroid/view/SurfaceControl;
    //   496: astore_1
    //   497: aload #4
    //   499: ifnull -> 515
    //   502: aload_1
    //   503: ifnull -> 515
    //   506: aload #4
    //   508: aload_1
    //   509: invokevirtual isSameSurface : (Landroid/view/SurfaceControl;)Z
    //   512: ifne -> 534
    //   515: aload_0
    //   516: invokespecial applyHiddenToControl : ()V
    //   519: goto -> 534
    //   522: astore_1
    //   523: ldc_w 'InsetsSC'
    //   526: ldc_w 'applyHiddenToControl ex '
    //   529: aload_1
    //   530: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   533: pop
    //   534: iload #9
    //   536: ifne -> 550
    //   539: aload_0
    //   540: getfield mIsAnimationPending : Z
    //   543: ifne -> 550
    //   546: aload_0
    //   547: invokevirtual removeSurface : ()V
    //   550: aload #5
    //   552: ifnull -> 563
    //   555: aload #5
    //   557: getstatic android/view/_$$Lambda$Rl1VZmNJ0VZDLK0BAbaVGis0rrA.INSTANCE : Landroid/view/-$$Lambda$Rl1VZmNJ0VZDLK0BAbaVGis0rrA;
    //   560: invokevirtual release : (Ljava/util/function/Consumer;)V
    //   563: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #107	-> 0
    //   #108	-> 12
    //   #110	-> 13
    //   #112	-> 31
    //   #113	-> 37
    //   #114	-> 42
    //   #121	-> 46
    //   #122	-> 61
    //   #123	-> 76
    //   #121	-> 88
    //   #124	-> 121
    //   #121	-> 138
    //   #128	-> 146
    //   #129	-> 153
    //   #132	-> 161
    //   #133	-> 178
    //   #132	-> 194
    //   #136	-> 200
    //   #137	-> 226
    //   #138	-> 252
    //   #139	-> 275
    //   #140	-> 291
    //   #136	-> 305
    //   #142	-> 312
    //   #146	-> 320
    //   #147	-> 326
    //   #148	-> 354
    //   #149	-> 373
    //   #150	-> 384
    //   #149	-> 394
    //   #151	-> 421
    //   #152	-> 426
    //   #154	-> 443
    //   #156	-> 457
    //   #158	-> 465
    //   #161	-> 470
    //   #165	-> 475
    //   #166	-> 482
    //   #171	-> 489
    //   #172	-> 497
    //   #178	-> 515
    //   #181	-> 519
    //   #179	-> 522
    //   #180	-> 523
    //   #184	-> 534
    //   #185	-> 546
    //   #189	-> 550
    //   #190	-> 555
    //   #192	-> 563
    // Exception table:
    //   from	to	target	type
    //   515	519	522	java/lang/Exception
  }
  
  public InsetsSourceControl getControl() {
    return this.mSourceControl;
  }
  
  protected boolean isRequestedVisibleAwaitingControl() {
    return isRequestedVisible();
  }
  
  int getType() {
    return this.mType;
  }
  
  public void show(boolean paramBoolean) {
    if (InsetsController.DEBUG) {
      int i = this.mType;
      String str = InsetsState.typeToString(i);
      Log.d("InsetsSourceConsumer", String.format("Call show() for type: %s fromIme: %b ", new Object[] { str, Boolean.valueOf(paramBoolean) }));
    } 
    setRequestedVisible(true);
  }
  
  public void hide() {
    if (InsetsController.DEBUG) {
      int i = this.mType;
      String str1 = InsetsState.typeToString(i), str2 = this.mController.getHost().getRootViewTitle();
      Log.d("InsetsSourceConsumer", String.format("Call hide for %s on %s", new Object[] { str1, str2 }));
    } 
    setRequestedVisible(false);
  }
  
  void hide(boolean paramBoolean, int paramInt) {
    hide();
  }
  
  public void onWindowFocusGained() {
    this.mHasWindowFocus = true;
  }
  
  public void onWindowFocusLost() {
    this.mHasWindowFocus = false;
  }
  
  boolean hasWindowFocus() {
    return this.mHasWindowFocus;
  }
  
  boolean applyLocalVisibilityOverride() {
    boolean bool1;
    boolean bool;
    boolean bool2;
    InsetsSource insetsSource = this.mState.peekSource(this.mType);
    if (insetsSource != null) {
      bool1 = insetsSource.isVisible();
    } else {
      bool1 = InsetsState.getDefaultVisibility(this.mType);
    } 
    if (this.mSourceControl != null) {
      bool = true;
    } else {
      bool = false;
    } 
    InsetsController insetsController = this.mController;
    int i = this.mType;
    if (bool || insetsSource == null) {
      bool2 = this.mRequestedVisible;
    } else {
      bool2 = bool1;
    } 
    insetsController.updateCompatSysUiVisibility(i, bool2, bool);
    if (!bool) {
      if (InsetsController.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("applyLocalVisibilityOverride: No control in ");
        insetsController = this.mController;
        stringBuilder.append(insetsController.getHost().getRootViewTitle());
        stringBuilder.append(" requestedVisible ");
        stringBuilder.append(this.mRequestedVisible);
        stringBuilder.append(" type ");
        stringBuilder.append(getType());
        String str = stringBuilder.toString();
        Log.d("InsetsSourceConsumer", str);
      } 
      return false;
    } 
    if (bool1 == this.mRequestedVisible)
      return false; 
    if (InsetsController.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      InsetsController insetsController1 = this.mController;
      String str1 = insetsController1.getHost().getRootViewTitle();
      bool1 = this.mRequestedVisible;
      stringBuilder.append(String.format("applyLocalVisibilityOverride: %s requestedVisible: %b", new Object[] { str1, Boolean.valueOf(bool1) }));
      stringBuilder.append(" type ");
      stringBuilder.append(getType());
      String str2 = stringBuilder.toString();
      Log.d("InsetsSourceConsumer", str2);
    } 
    this.mState.getSource(this.mType).setVisible(this.mRequestedVisible);
    return true;
  }
  
  public boolean isRequestedVisible() {
    return this.mRequestedVisible;
  }
  
  public int requestShow(boolean paramBoolean) {
    return 0;
  }
  
  public void onPerceptible(boolean paramBoolean) {}
  
  void notifyHidden() {}
  
  public void removeSurface() {}
  
  public void updateSource(InsetsSource paramInsetsSource, int paramInt) {
    InsetsSource insetsSource1 = this.mState.peekSource(this.mType);
    InsetsSource insetsSource2 = null;
    if (insetsSource1 == null || paramInt == -1 || 
      insetsSource1.getFrame().equals(paramInsetsSource.getFrame())) {
      this.mPendingFrame = null;
      this.mPendingVisibleFrame = null;
      this.mState.addSource(paramInsetsSource);
      return;
    } 
    InsetsSource insetsSource3 = new InsetsSource(paramInsetsSource);
    this.mPendingFrame = new Rect(insetsSource3.getFrame());
    if (insetsSource3.getVisibleFrame() != null) {
      Rect rect = new Rect(insetsSource3.getVisibleFrame());
    } else {
      paramInsetsSource = insetsSource2;
    } 
    this.mPendingVisibleFrame = (Rect)paramInsetsSource;
    insetsSource3.setFrame(insetsSource1.getFrame());
    insetsSource3.setVisibleFrame(insetsSource1.getVisibleFrame());
    this.mState.addSource(insetsSource3);
    if (InsetsController.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateSource: ");
      stringBuilder.append(insetsSource3);
      Log.d("InsetsSourceConsumer", stringBuilder.toString());
    } 
  }
  
  public boolean notifyAnimationFinished() {
    if (this.mPendingFrame != null) {
      InsetsSource insetsSource = this.mState.getSource(this.mType);
      insetsSource.setFrame(this.mPendingFrame);
      insetsSource.setVisibleFrame(this.mPendingVisibleFrame);
      this.mPendingFrame = null;
      this.mPendingVisibleFrame = null;
      return true;
    } 
    return false;
  }
  
  protected void setRequestedVisible(boolean paramBoolean) {
    if (this.mRequestedVisible != paramBoolean) {
      this.mRequestedVisible = paramBoolean;
      this.mIsAnimationPending = false;
      if (InsetsController.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setRequestedVisible: ");
        stringBuilder.append(paramBoolean);
        stringBuilder.append(", type ");
        stringBuilder.append(getType());
        stringBuilder.append(",");
        stringBuilder.append(Debug.getCallers(5));
        String str = stringBuilder.toString();
        Log.d("InsetsSourceConsumer", str);
      } 
    } 
    if (applyLocalVisibilityOverride())
      this.mController.notifyVisibilityChanged(); 
  }
  
  private void applyHiddenToControl() {
    InsetsSourceControl insetsSourceControl = this.mSourceControl;
    if (insetsSourceControl == null || insetsSourceControl.getLeash() == null)
      return; 
    SurfaceControl.Transaction transaction = this.mTransactionSupplier.get();
    if (InsetsController.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("applyHiddenToControl: ");
      stringBuilder.append(this.mRequestedVisible);
      Log.d("InsetsSourceConsumer", stringBuilder.toString());
    } 
    if (this.mRequestedVisible) {
      transaction.show(this.mSourceControl.getLeash());
      transaction.setAlpha(this.mSourceControl.getLeash(), 1.0F);
    } else {
      transaction.hide(this.mSourceControl.getLeash());
    } 
    transaction.apply();
    onPerceptible(this.mRequestedVisible);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  static @interface ShowResult {
    public static final int IME_SHOW_DELAYED = 1;
    
    public static final int IME_SHOW_FAILED = 2;
    
    public static final int SHOW_IMMEDIATELY = 0;
  }
}
