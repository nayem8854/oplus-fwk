package android.view;

import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefLong;
import com.oplus.reflect.RefStaticMethod;

public class OplusMirrirMotionEvent {
  public static Class<?> TYPE = RefClass.load(OplusMirrirMotionEvent.class, MotionEvent.class);
  
  public static RefLong mNativePtr;
  
  @MethodParams({long.class})
  public static RefStaticMethod<Float> nativeGetXOffset;
  
  @MethodParams({long.class})
  public static RefStaticMethod<Float> nativeGetYOffset;
}
