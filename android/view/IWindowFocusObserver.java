package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWindowFocusObserver extends IInterface {
  void focusGained(IBinder paramIBinder) throws RemoteException;
  
  void focusLost(IBinder paramIBinder) throws RemoteException;
  
  class Default implements IWindowFocusObserver {
    public void focusGained(IBinder param1IBinder) throws RemoteException {}
    
    public void focusLost(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowFocusObserver {
    private static final String DESCRIPTOR = "android.view.IWindowFocusObserver";
    
    static final int TRANSACTION_focusGained = 1;
    
    static final int TRANSACTION_focusLost = 2;
    
    public Stub() {
      attachInterface(this, "android.view.IWindowFocusObserver");
    }
    
    public static IWindowFocusObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IWindowFocusObserver");
      if (iInterface != null && iInterface instanceof IWindowFocusObserver)
        return (IWindowFocusObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "focusLost";
      } 
      return "focusGained";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.view.IWindowFocusObserver");
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IWindowFocusObserver");
        iBinder = param1Parcel1.readStrongBinder();
        focusLost(iBinder);
        param1Parcel2.writeNoException();
        return true;
      } 
      iBinder.enforceInterface("android.view.IWindowFocusObserver");
      IBinder iBinder = iBinder.readStrongBinder();
      focusGained(iBinder);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IWindowFocusObserver {
      public static IWindowFocusObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IWindowFocusObserver";
      }
      
      public void focusGained(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowFocusObserver");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWindowFocusObserver.Stub.getDefaultImpl() != null) {
            IWindowFocusObserver.Stub.getDefaultImpl().focusGained(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void focusLost(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowFocusObserver");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IWindowFocusObserver.Stub.getDefaultImpl() != null) {
            IWindowFocusObserver.Stub.getDefaultImpl().focusLost(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowFocusObserver param1IWindowFocusObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowFocusObserver != null) {
          Proxy.sDefaultImpl = param1IWindowFocusObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowFocusObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
