package android.view;

import android.graphics.Rect;
import android.os.RemoteException;

public final class OplusFreeformManager {
  private static final String TAG = "OplusFreeformManager";
  
  private static OplusFreeformManager sDefaultFreeforManager;
  
  private static IWindowManager sWindowManagerService;
  
  private OplusWindowManager mOWm;
  
  private OplusFreeformManager() {
    getWindowManagerService();
    this.mOWm = new OplusWindowManager();
  }
  
  public static OplusFreeformManager getInstance() {
    // Byte code:
    //   0: ldc android/view/OplusFreeformManager
    //   2: monitorenter
    //   3: getstatic android/view/OplusFreeformManager.sDefaultFreeforManager : Landroid/view/OplusFreeformManager;
    //   6: ifnonnull -> 21
    //   9: new android/view/OplusFreeformManager
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/view/OplusFreeformManager.sDefaultFreeforManager : Landroid/view/OplusFreeformManager;
    //   21: getstatic android/view/OplusFreeformManager.sDefaultFreeforManager : Landroid/view/OplusFreeformManager;
    //   24: astore_0
    //   25: ldc android/view/OplusFreeformManager
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/view/OplusFreeformManager
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #40	-> 0
    //   #41	-> 3
    //   #42	-> 9
    //   #44	-> 21
    //   #45	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	28	30	finally
    //   31	34	30	finally
  }
  
  private static IWindowManager getWindowManagerService() {
    // Byte code:
    //   0: ldc android/view/OplusFreeformManager
    //   2: monitorenter
    //   3: getstatic android/view/OplusFreeformManager.sWindowManagerService : Landroid/view/IWindowManager;
    //   6: ifnonnull -> 22
    //   9: ldc 'window'
    //   11: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   14: astore_0
    //   15: aload_0
    //   16: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/view/IWindowManager;
    //   19: putstatic android/view/OplusFreeformManager.sWindowManagerService : Landroid/view/IWindowManager;
    //   22: getstatic android/view/OplusFreeformManager.sWindowManagerService : Landroid/view/IWindowManager;
    //   25: astore_0
    //   26: ldc android/view/OplusFreeformManager
    //   28: monitorexit
    //   29: aload_0
    //   30: areturn
    //   31: astore_0
    //   32: ldc android/view/OplusFreeformManager
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #49	-> 0
    //   #50	-> 3
    //   #51	-> 9
    //   #52	-> 9
    //   #51	-> 15
    //   #54	-> 22
    //   #55	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	15	31	finally
    //   15	22	31	finally
    //   22	29	31	finally
    //   32	35	31	finally
  }
  
  public boolean isInFreeformMode() {
    try {
      return this.mOWm.isInFreeformMode();
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public void getFreeformStackBounds(Rect paramRect) {
    try {
      this.mOWm.getFreeformStackBounds(paramRect);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
  }
}
