package android.view;

import android.graphics.Matrix;
import android.os.IBinder;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.IAccessibilityEmbeddedConnection;
import java.lang.ref.WeakReference;

final class AccessibilityEmbeddedConnection extends IAccessibilityEmbeddedConnection.Stub {
  private final Matrix mTmpScreenMatrix = new Matrix();
  
  private final WeakReference<ViewRootImpl> mViewRootImpl;
  
  AccessibilityEmbeddedConnection(ViewRootImpl paramViewRootImpl) {
    this.mViewRootImpl = new WeakReference<>(paramViewRootImpl);
  }
  
  public IBinder associateEmbeddedHierarchy(IBinder paramIBinder, int paramInt) {
    ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
    if (viewRootImpl != null) {
      AccessibilityManager accessibilityManager = AccessibilityManager.getInstance(viewRootImpl.mContext);
      viewRootImpl.mAttachInfo.mLeashedParentToken = paramIBinder;
      viewRootImpl.mAttachInfo.mLeashedParentAccessibilityViewId = paramInt;
      if (accessibilityManager.isEnabled())
        accessibilityManager.associateEmbeddedHierarchy(paramIBinder, viewRootImpl.mLeashToken); 
      return viewRootImpl.mLeashToken;
    } 
    return null;
  }
  
  public void disassociateEmbeddedHierarchy() {
    ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
    if (viewRootImpl != null) {
      AccessibilityManager accessibilityManager = AccessibilityManager.getInstance(viewRootImpl.mContext);
      viewRootImpl.mAttachInfo.mLeashedParentToken = null;
      viewRootImpl.mAttachInfo.mLeashedParentAccessibilityViewId = -1;
      viewRootImpl.mAttachInfo.mLocationInParentDisplay.set(0, 0);
      if (accessibilityManager.isEnabled())
        accessibilityManager.disassociateEmbeddedHierarchy(viewRootImpl.mLeashToken); 
    } 
  }
  
  public void setScreenMatrix(float[] paramArrayOffloat) {
    ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
    if (viewRootImpl != null) {
      this.mTmpScreenMatrix.setValues(paramArrayOffloat);
      if (viewRootImpl.mAttachInfo.mScreenMatrixInEmbeddedHierarchy == null)
        viewRootImpl.mAttachInfo.mScreenMatrixInEmbeddedHierarchy = new Matrix(); 
      viewRootImpl.mAttachInfo.mScreenMatrixInEmbeddedHierarchy.set(this.mTmpScreenMatrix);
    } 
  }
}
