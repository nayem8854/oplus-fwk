package android.view;

import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import java.util.List;

public class ViewRootInsetsControllerHost implements InsetsController.Host {
  private final String TAG = "VRInsetsControllerHost";
  
  private SyncRtSurfaceTransactionApplier mApplier;
  
  private final ViewRootImpl mViewRoot;
  
  public ViewRootInsetsControllerHost(ViewRootImpl paramViewRootImpl) {
    this.mViewRoot = paramViewRootImpl;
  }
  
  public Handler getHandler() {
    return this.mViewRoot.mHandler;
  }
  
  public void notifyInsetsChanged() {
    this.mViewRoot.notifyInsetsChanged();
  }
  
  public void addOnPreDrawRunnable(final Runnable r) {
    if (this.mViewRoot.mView == null)
      return; 
    this.mViewRoot.mView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
          final ViewRootInsetsControllerHost this$0;
          
          final Runnable val$r;
          
          public boolean onPreDraw() {
            ViewRootInsetsControllerHost.this.mViewRoot.mView.getViewTreeObserver().removeOnPreDrawListener(this);
            r.run();
            return true;
          }
        });
    this.mViewRoot.mView.invalidate();
  }
  
  public void dispatchWindowInsetsAnimationPrepare(WindowInsetsAnimation paramWindowInsetsAnimation) {
    if (this.mViewRoot.mView == null)
      return; 
    this.mViewRoot.mView.dispatchWindowInsetsAnimationPrepare(paramWindowInsetsAnimation);
  }
  
  public WindowInsetsAnimation.Bounds dispatchWindowInsetsAnimationStart(WindowInsetsAnimation paramWindowInsetsAnimation, WindowInsetsAnimation.Bounds paramBounds) {
    if (this.mViewRoot.mView == null)
      return null; 
    if (InsetsController.DEBUG)
      Log.d("VRInsetsControllerHost", "windowInsetsAnimation started"); 
    return this.mViewRoot.mView.dispatchWindowInsetsAnimationStart(paramWindowInsetsAnimation, paramBounds);
  }
  
  public WindowInsets dispatchWindowInsetsAnimationProgress(WindowInsets paramWindowInsets, List<WindowInsetsAnimation> paramList) {
    if (this.mViewRoot.mView == null)
      return null; 
    if (InsetsController.DEBUG)
      for (WindowInsetsAnimation windowInsetsAnimation : paramList) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("windowInsetsAnimation progress: ");
        stringBuilder.append(windowInsetsAnimation.getInterpolatedFraction());
        String str = stringBuilder.toString();
        Log.d("VRInsetsControllerHost", str);
      }  
    return this.mViewRoot.mView.dispatchWindowInsetsAnimationProgress(paramWindowInsets, paramList);
  }
  
  public void dispatchWindowInsetsAnimationEnd(WindowInsetsAnimation paramWindowInsetsAnimation) {
    if (InsetsController.DEBUG)
      Log.d("VRInsetsControllerHost", "windowInsetsAnimation ended"); 
    this.mViewRoot.mView.dispatchWindowInsetsAnimationEnd(paramWindowInsetsAnimation);
  }
  
  public void applySurfaceParams(SyncRtSurfaceTransactionApplier.SurfaceParams... paramVarArgs) {
    if (this.mViewRoot.mView != null) {
      if (this.mApplier == null)
        this.mApplier = new SyncRtSurfaceTransactionApplier(this.mViewRoot.mView); 
      if (this.mViewRoot.mView.isHardwareAccelerated()) {
        this.mApplier.scheduleApply(paramVarArgs);
      } else {
        this.mApplier.applyParams(new SurfaceControl.Transaction(), -1L, paramVarArgs);
      } 
      return;
    } 
    throw new IllegalStateException("View of the ViewRootImpl is not initiated.");
  }
  
  public void postInsetsAnimationCallback(Runnable paramRunnable) {
    this.mViewRoot.mChoreographer.postCallback(2, paramRunnable, null);
  }
  
  public void updateCompatSysUiVisibility(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    this.mViewRoot.updateCompatSysUiVisibility(paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void onInsetsModified(InsetsState paramInsetsState) {
    try {
      this.mViewRoot.mWindowSession.insetsModified(this.mViewRoot.mWindow, paramInsetsState);
    } catch (RemoteException remoteException) {
      Log.e("VRInsetsControllerHost", "Failed to call insetsModified", (Throwable)remoteException);
    } 
  }
  
  public boolean hasAnimationCallbacks() {
    if (this.mViewRoot.mView == null)
      return false; 
    return this.mViewRoot.mView.hasWindowInsetsAnimationCallback();
  }
  
  public void setSystemBarsAppearance(int paramInt1, int paramInt2) {
    WindowManager.LayoutParams layoutParams = this.mViewRoot.mWindowAttributes;
    layoutParams.privateFlags |= 0x4000000;
    InsetsFlags insetsFlags = this.mViewRoot.mWindowAttributes.insetsFlags;
    if (insetsFlags.appearance != paramInt1) {
      insetsFlags.appearance = insetsFlags.appearance & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2;
      this.mViewRoot.mWindowAttributesChanged = true;
      this.mViewRoot.scheduleTraversals();
    } 
  }
  
  public int getSystemBarsAppearance() {
    if ((this.mViewRoot.mWindowAttributes.privateFlags & 0x4000000) == 0)
      return 0; 
    return this.mViewRoot.mWindowAttributes.insetsFlags.appearance;
  }
  
  public void setSystemBarsBehavior(int paramInt) {
    WindowManager.LayoutParams layoutParams = this.mViewRoot.mWindowAttributes;
    layoutParams.privateFlags |= 0x8000000;
    if (this.mViewRoot.mWindowAttributes.insetsFlags.behavior != paramInt) {
      this.mViewRoot.mWindowAttributes.insetsFlags.behavior = paramInt;
      this.mViewRoot.mWindowAttributesChanged = true;
      this.mViewRoot.scheduleTraversals();
    } 
  }
  
  public int getSystemBarsBehavior() {
    if ((this.mViewRoot.mWindowAttributes.privateFlags & 0x8000000) == 0)
      return 0; 
    return this.mViewRoot.mWindowAttributes.insetsFlags.behavior;
  }
  
  public void releaseSurfaceControlFromRt(SurfaceControl paramSurfaceControl) {
    if (this.mViewRoot.mView != null && this.mViewRoot.mView.isHardwareAccelerated()) {
      this.mViewRoot.registerRtFrameCallback(new _$$Lambda$ViewRootInsetsControllerHost$3v_KSpxQXUSxSM3D_S6T5_aZKoQ(paramSurfaceControl));
      this.mViewRoot.mView.invalidate();
    } else {
      paramSurfaceControl.release();
    } 
  }
  
  public InputMethodManager getInputMethodManager() {
    return (InputMethodManager)this.mViewRoot.mContext.getSystemService(InputMethodManager.class);
  }
  
  public String getRootViewTitle() {
    ViewRootImpl viewRootImpl = this.mViewRoot;
    if (viewRootImpl == null)
      return null; 
    return viewRootImpl.getTitle().toString();
  }
  
  public int dipToPx(int paramInt) {
    ViewRootImpl viewRootImpl = this.mViewRoot;
    if (viewRootImpl != null)
      return viewRootImpl.dipToPx(paramInt); 
    return 0;
  }
  
  public IBinder getWindowToken() {
    ViewRootImpl viewRootImpl = this.mViewRoot;
    if (viewRootImpl == null)
      return null; 
    View view = viewRootImpl.getView();
    if (view == null)
      return null; 
    return view.getWindowToken();
  }
}
