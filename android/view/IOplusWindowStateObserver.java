package android.view;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusWindowStateObserver extends IInterface {
  void onWindowStateChange(Bundle paramBundle) throws RemoteException;
  
  class Default implements IOplusWindowStateObserver {
    public void onWindowStateChange(Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusWindowStateObserver {
    private static final String DESCRIPTOR = "android.view.IOplusWindowStateObserver";
    
    static final int TRANSACTION_onWindowStateChange = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IOplusWindowStateObserver");
    }
    
    public static IOplusWindowStateObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IOplusWindowStateObserver");
      if (iInterface != null && iInterface instanceof IOplusWindowStateObserver)
        return (IOplusWindowStateObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onWindowStateChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IOplusWindowStateObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IOplusWindowStateObserver");
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onWindowStateChange((Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusWindowStateObserver {
      public static IOplusWindowStateObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IOplusWindowStateObserver";
      }
      
      public void onWindowStateChange(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IOplusWindowStateObserver");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusWindowStateObserver.Stub.getDefaultImpl() != null) {
            IOplusWindowStateObserver.Stub.getDefaultImpl().onWindowStateChange(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusWindowStateObserver param1IOplusWindowStateObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusWindowStateObserver != null) {
          Proxy.sDefaultImpl = param1IOplusWindowStateObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusWindowStateObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
