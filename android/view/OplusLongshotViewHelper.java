package android.view;

import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import com.oplus.screenshot.OplusLongshotDump;
import com.oplus.screenshot.OplusLongshotViewRoot;
import com.oplus.util.OplusLog;
import com.oplus.util.OplusTypeCastingHelper;
import com.oplus.view.analysis.OplusWindowNode;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class OplusLongshotViewHelper implements IOplusLongshotViewHelper {
  private static final boolean DBG = OplusLongshotDump.DBG;
  
  private static final String TAG = "LongshotDump";
  
  private final OplusLongshotViewDump mDump;
  
  private final H mHandler;
  
  private final WeakReference<ViewRootImpl> mViewAncestor;
  
  public OplusLongshotViewHelper(WeakReference<ViewRootImpl> paramWeakReference) {
    this.mViewAncestor = paramWeakReference;
    this.mDump = new OplusLongshotViewDump();
    this.mHandler = new H(this.mViewAncestor, this.mDump);
  }
  
  public void longshotNotifyConnected(boolean paramBoolean) {
    ViewRootImpl viewRootImpl = this.mViewAncestor.get();
    if (viewRootImpl != null && viewRootImpl.mView != null) {
      IOplusBaseViewRoot iOplusBaseViewRoot = (IOplusBaseViewRoot)OplusTypeCastingHelper.typeCasting(IOplusBaseViewRoot.class, viewRootImpl);
      OplusLongshotViewRoot oplusLongshotViewRoot = iOplusBaseViewRoot.getOplusViewRootImplHooks().getLongshotViewRoot();
      oplusLongshotViewRoot.setConnected(paramBoolean);
      if (!paramBoolean)
        this.mDump.reset(); 
    } 
  }
  
  public void longshotDump(FileDescriptor paramFileDescriptor, List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) {
    try {
      DumpInfoData dumpInfoData = new DumpInfoData();
      this(ParcelFileDescriptor.dup(paramFileDescriptor), paramList1, paramList2, true);
      sendMessage(1, dumpInfoData, 0, 0, true);
    } catch (IOException iOException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("longshotDump failed : ");
      stringBuilder.append(iOException);
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } 
  }
  
  public OplusWindowNode longshotCollectWindow(boolean paramBoolean1, boolean paramBoolean2) {
    ViewRootImpl viewRootImpl = this.mViewAncestor.get();
    if (viewRootImpl != null && viewRootImpl.mView != null)
      return this.mDump.collectWindow(viewRootImpl.mView, paramBoolean1, paramBoolean2); 
    return null;
  }
  
  public void longshotInjectInput(InputEvent paramInputEvent, int paramInt) {
    ViewRootImpl viewRootImpl = this.mViewAncestor.get();
    if (viewRootImpl != null && viewRootImpl.mView != null)
      viewRootImpl.dispatchInputEvent(paramInputEvent, (InputEventReceiver)viewRootImpl.mInputEventReceiver); 
  }
  
  public void longshotInjectInputBegin() {
    ViewRootImpl viewRootImpl = this.mViewAncestor.get();
    if (viewRootImpl != null && viewRootImpl.mView != null)
      this.mDump.injectInputBegin(); 
  }
  
  public void longshotInjectInputEnd() {
    ViewRootImpl viewRootImpl = this.mViewAncestor.get();
    if (viewRootImpl != null && viewRootImpl.mView != null)
      this.mDump.injectInputEnd(); 
  }
  
  public void screenshotDump(FileDescriptor paramFileDescriptor) {
    try {
      DumpInfoData dumpInfoData = new DumpInfoData();
      this(ParcelFileDescriptor.dup(paramFileDescriptor), null, null, false);
      sendMessage(1, dumpInfoData, 0, 0, true);
    } catch (IOException iOException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("screenshotDump failed : ");
      stringBuilder.append(iOException);
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } 
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) {
    ParcelFileDescriptor parcelFileDescriptor1;
    OplusWindowNode oplusWindowNode;
    boolean bool2;
    ParcelFileDescriptor parcelFileDescriptor2;
    boolean bool1 = false;
    switch (paramInt1) {
      default:
        return false;
      case 10009:
        paramParcel1.enforceInterface("android.view.IWindow");
        parcelFileDescriptor1 = paramParcel1.readFileDescriptor();
        if (parcelFileDescriptor1 != null) {
          screenshotDump(parcelFileDescriptor1.getFileDescriptor());
          try {
            parcelFileDescriptor1.close();
          } catch (IOException iOException2) {}
        } 
        return true;
      case 10007:
        iOException2.enforceInterface("android.view.IWindow");
        longshotInjectInputEnd();
        return true;
      case 10006:
        iOException2.enforceInterface("android.view.IWindow");
        longshotInjectInputBegin();
        return true;
      case 10005:
        iOException2.enforceInterface("android.view.IWindow");
        if (iOException2.readInt() != 0) {
          InputEvent inputEvent = (InputEvent)InputEvent.CREATOR.createFromParcel((Parcel)iOException2);
        } else {
          paramParcel2 = null;
        } 
        paramInt1 = iOException2.readInt();
        longshotInjectInput((InputEvent)paramParcel2, paramInt1);
        return true;
      case 10004:
        iOException2.enforceInterface("android.view.IWindow");
        if (1 == iOException2.readInt()) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (1 == iOException2.readInt()) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        oplusWindowNode = longshotCollectWindow(bool1, bool2);
        paramParcel2.writeNoException();
        if (oplusWindowNode != null) {
          paramParcel2.writeInt(1);
          oplusWindowNode.writeToParcel(paramParcel2, 1);
        } else {
          paramParcel2.writeInt(0);
        } 
        return true;
      case 10003:
        oplusWindowNode.enforceInterface("android.view.IWindow");
        parcelFileDescriptor2 = oplusWindowNode.readFileDescriptor();
        if (parcelFileDescriptor2 != null) {
          if (1 == oplusWindowNode.readInt()) {
            ArrayList arrayList = oplusWindowNode.createTypedArrayList(OplusWindowNode.CREATOR);
          } else {
            paramParcel2 = null;
          } 
          if (1 == oplusWindowNode.readInt()) {
            ArrayList arrayList = oplusWindowNode.createTypedArrayList(OplusWindowNode.CREATOR);
          } else {
            oplusWindowNode = null;
          } 
          longshotDump(parcelFileDescriptor2.getFileDescriptor(), (List<OplusWindowNode>)paramParcel2, (List<OplusWindowNode>)oplusWindowNode);
          try {
            parcelFileDescriptor2.close();
          } catch (IOException iOException1) {}
        } 
        return true;
      case 10002:
        break;
    } 
    iOException1.enforceInterface("android.view.IWindow");
    if (1 == iOException1.readInt())
      bool1 = true; 
    longshotNotifyConnected(bool1);
    return true;
  }
  
  private void sendMessage(int paramInt1, Object paramObject, int paramInt2, int paramInt3, boolean paramBoolean) {
    Message message = this.mHandler.obtainMessage(paramInt1);
    message.what = paramInt1;
    message.obj = paramObject;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    if (paramBoolean)
      message.setAsynchronous(true); 
    this.mHandler.sendMessage(message);
  }
  
  private static final class DumpInfoData {
    private final List<OplusWindowNode> mFloatWindows;
    
    private final boolean mIsLongshot;
    
    private final ParcelFileDescriptor mParcelFileDescriptor;
    
    private final List<OplusWindowNode> mSystemWindows;
    
    public DumpInfoData(ParcelFileDescriptor param1ParcelFileDescriptor, List<OplusWindowNode> param1List1, List<OplusWindowNode> param1List2, boolean param1Boolean) {
      this.mParcelFileDescriptor = param1ParcelFileDescriptor;
      this.mSystemWindows = param1List1;
      this.mFloatWindows = param1List2;
      this.mIsLongshot = param1Boolean;
    }
    
    public ParcelFileDescriptor getParcelFileDescriptor() {
      return this.mParcelFileDescriptor;
    }
    
    public List<OplusWindowNode> getSystemWindows() {
      return this.mSystemWindows;
    }
    
    public List<OplusWindowNode> getFloatWindows() {
      return this.mFloatWindows;
    }
    
    public boolean isLongshot() {
      return this.mIsLongshot;
    }
  }
  
  private static class H extends Handler {
    public static final int MSG_DUMP_VIEW_HIERARCHY = 1;
    
    private final WeakReference<OplusLongshotViewDump> mDump;
    
    private final WeakReference<ViewRootImpl> mViewAncestor;
    
    public H(WeakReference<ViewRootImpl> param1WeakReference, OplusLongshotViewDump param1OplusLongshotViewDump) {
      this.mViewAncestor = param1WeakReference;
      this.mDump = new WeakReference<>(param1OplusLongshotViewDump);
    }
    
    public void handleMessage(Message param1Message) {
      if (param1Message.what == 1) {
        ViewRootImpl viewRootImpl = this.mViewAncestor.get();
        OplusLongshotViewDump oplusLongshotViewDump = this.mDump.get();
        if (viewRootImpl != null && oplusLongshotViewDump != null) {
          if (viewRootImpl.mView != null) {
            OplusLongshotViewHelper.DumpInfoData dumpInfoData = (OplusLongshotViewHelper.DumpInfoData)param1Message.obj;
            ParcelFileDescriptor parcelFileDescriptor = dumpInfoData.getParcelFileDescriptor();
            List<OplusWindowNode> list1 = dumpInfoData.getSystemWindows();
            List<OplusWindowNode> list2 = dumpInfoData.getFloatWindows();
            oplusLongshotViewDump.dumpViewRoot(viewRootImpl, parcelFileDescriptor, list1, list2, dumpInfoData.isLongshot());
          } else {
            OplusLog.e(OplusLongshotViewHelper.DBG, "LongshotDump", "longshotDump : viewAncestor.mView is null");
          } 
        } else {
          OplusLog.e(OplusLongshotViewHelper.DBG, "LongshotDump", "longshotDump : viewAncestor is null");
        } 
      } 
    }
  }
}
