package android.view;

import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IScrollCaptureClient extends IInterface {
  void endCapture() throws RemoteException;
  
  void requestImage(Rect paramRect) throws RemoteException;
  
  void startCapture(Surface paramSurface) throws RemoteException;
  
  class Default implements IScrollCaptureClient {
    public void startCapture(Surface param1Surface) throws RemoteException {}
    
    public void requestImage(Rect param1Rect) throws RemoteException {}
    
    public void endCapture() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IScrollCaptureClient {
    private static final String DESCRIPTOR = "android.view.IScrollCaptureClient";
    
    static final int TRANSACTION_endCapture = 3;
    
    static final int TRANSACTION_requestImage = 2;
    
    static final int TRANSACTION_startCapture = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IScrollCaptureClient");
    }
    
    public static IScrollCaptureClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IScrollCaptureClient");
      if (iInterface != null && iInterface instanceof IScrollCaptureClient)
        return (IScrollCaptureClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "endCapture";
        } 
        return "requestImage";
      } 
      return "startCapture";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.view.IScrollCaptureClient");
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.IScrollCaptureClient");
          endCapture();
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IScrollCaptureClient");
        if (param1Parcel1.readInt() != 0) {
          Rect rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        requestImage((Rect)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IScrollCaptureClient");
      if (param1Parcel1.readInt() != 0) {
        Surface surface = (Surface)Surface.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      startCapture((Surface)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IScrollCaptureClient {
      public static IScrollCaptureClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IScrollCaptureClient";
      }
      
      public void startCapture(Surface param2Surface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IScrollCaptureClient");
          if (param2Surface != null) {
            parcel.writeInt(1);
            param2Surface.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IScrollCaptureClient.Stub.getDefaultImpl() != null) {
            IScrollCaptureClient.Stub.getDefaultImpl().startCapture(param2Surface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestImage(Rect param2Rect) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IScrollCaptureClient");
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IScrollCaptureClient.Stub.getDefaultImpl() != null) {
            IScrollCaptureClient.Stub.getDefaultImpl().requestImage(param2Rect);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void endCapture() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IScrollCaptureClient");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IScrollCaptureClient.Stub.getDefaultImpl() != null) {
            IScrollCaptureClient.Stub.getDefaultImpl().endCapture();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IScrollCaptureClient param1IScrollCaptureClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IScrollCaptureClient != null) {
          Proxy.sDefaultImpl = param1IScrollCaptureClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IScrollCaptureClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
