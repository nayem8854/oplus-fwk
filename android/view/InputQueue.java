package android.view;

import android.os.Looper;
import android.os.MessageQueue;
import android.util.LongSparseArray;
import android.util.Pools;
import dalvik.system.CloseGuard;
import java.lang.ref.WeakReference;

public final class InputQueue {
  private final LongSparseArray<ActiveInputEvent> mActiveEventArray = new LongSparseArray<>(20);
  
  private final Pools.Pool<ActiveInputEvent> mActiveInputEventPool = new Pools.SimplePool<>(20);
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private long mPtr;
  
  public InputQueue() {
    this.mPtr = nativeInit(new WeakReference<>(this), Looper.myQueue());
    this.mCloseGuard.open("dispose");
  }
  
  protected void finalize() throws Throwable {
    try {
      dispose(true);
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void dispose() {
    dispose(false);
  }
  
  public void dispose(boolean paramBoolean) {
    CloseGuard closeGuard = this.mCloseGuard;
    if (closeGuard != null) {
      if (paramBoolean)
        closeGuard.warnIfOpen(); 
      this.mCloseGuard.close();
    } 
    long l = this.mPtr;
    if (l != 0L) {
      nativeDispose(l);
      this.mPtr = 0L;
    } 
  }
  
  public long getNativePtr() {
    return this.mPtr;
  }
  
  public void sendInputEvent(InputEvent paramInputEvent, Object paramObject, boolean paramBoolean, FinishedInputEventCallback paramFinishedInputEventCallback) {
    long l;
    paramObject = obtainActiveInputEvent(paramObject, paramFinishedInputEventCallback);
    if (paramInputEvent instanceof KeyEvent) {
      l = nativeSendKeyEvent(this.mPtr, (KeyEvent)paramInputEvent, paramBoolean);
    } else {
      l = nativeSendMotionEvent(this.mPtr, (MotionEvent)paramInputEvent);
    } 
    this.mActiveEventArray.put(l, paramObject);
  }
  
  private void finishInputEvent(long paramLong, boolean paramBoolean) {
    int i = this.mActiveEventArray.indexOfKey(paramLong);
    if (i >= 0) {
      ActiveInputEvent activeInputEvent = this.mActiveEventArray.valueAt(i);
      this.mActiveEventArray.removeAt(i);
      activeInputEvent.mCallback.onFinishedInputEvent(activeInputEvent.mToken, paramBoolean);
      recycleActiveInputEvent(activeInputEvent);
    } 
  }
  
  private ActiveInputEvent obtainActiveInputEvent(Object paramObject, FinishedInputEventCallback paramFinishedInputEventCallback) {
    ActiveInputEvent activeInputEvent1 = this.mActiveInputEventPool.acquire();
    ActiveInputEvent activeInputEvent2 = activeInputEvent1;
    if (activeInputEvent1 == null)
      activeInputEvent2 = new ActiveInputEvent(); 
    activeInputEvent2.mToken = paramObject;
    activeInputEvent2.mCallback = paramFinishedInputEventCallback;
    return activeInputEvent2;
  }
  
  private void recycleActiveInputEvent(ActiveInputEvent paramActiveInputEvent) {
    paramActiveInputEvent.recycle();
    this.mActiveInputEventPool.release(paramActiveInputEvent);
  }
  
  private static native void nativeDispose(long paramLong);
  
  private static native long nativeInit(WeakReference<InputQueue> paramWeakReference, MessageQueue paramMessageQueue);
  
  private static native long nativeSendKeyEvent(long paramLong, KeyEvent paramKeyEvent, boolean paramBoolean);
  
  private static native long nativeSendMotionEvent(long paramLong, MotionEvent paramMotionEvent);
  
  private final class ActiveInputEvent {
    public InputQueue.FinishedInputEventCallback mCallback;
    
    public Object mToken;
    
    final InputQueue this$0;
    
    private ActiveInputEvent() {}
    
    public void recycle() {
      this.mToken = null;
      this.mCallback = null;
    }
  }
  
  public static interface Callback {
    void onInputQueueCreated(InputQueue param1InputQueue);
    
    void onInputQueueDestroyed(InputQueue param1InputQueue);
  }
  
  public static interface FinishedInputEventCallback {
    void onFinishedInputEvent(Object param1Object, boolean param1Boolean);
  }
}
