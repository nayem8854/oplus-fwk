package android.view.textclassifier;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Locale;
import java.util.Objects;

public final class TextClassificationContext implements Parcelable {
  private TextClassificationContext(String paramString1, String paramString2, String paramString3) {
    Objects.requireNonNull(paramString1);
    this.mPackageName = paramString1;
    Objects.requireNonNull(paramString2);
    this.mWidgetType = paramString2;
    this.mWidgetVersion = paramString3;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  void setSystemTextClassifierMetadata(SystemTextClassifierMetadata paramSystemTextClassifierMetadata) {
    this.mSystemTcMetadata = paramSystemTextClassifierMetadata;
  }
  
  public SystemTextClassifierMetadata getSystemTextClassifierMetadata() {
    return this.mSystemTcMetadata;
  }
  
  public String getWidgetType() {
    return this.mWidgetType;
  }
  
  public String getWidgetVersion() {
    return this.mWidgetVersion;
  }
  
  public String toString() {
    return String.format(Locale.US, "TextClassificationContext{packageName=%s, widgetType=%s, widgetVersion=%s, systemTcMetadata=%s}", new Object[] { this.mPackageName, this.mWidgetType, this.mWidgetVersion, this.mSystemTcMetadata });
  }
  
  class Builder {
    private final String mPackageName;
    
    private final String mWidgetType;
    
    private String mWidgetVersion;
    
    public Builder(TextClassificationContext this$0, String param1String1) {
      Objects.requireNonNull(this$0);
      this.mPackageName = (String)this$0;
      Objects.requireNonNull(param1String1);
      this.mWidgetType = param1String1;
    }
    
    public Builder setWidgetVersion(String param1String) {
      this.mWidgetVersion = param1String;
      return this;
    }
    
    public TextClassificationContext build() {
      return new TextClassificationContext(this.mPackageName, this.mWidgetType, this.mWidgetVersion);
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeString(this.mWidgetType);
    paramParcel.writeString(this.mWidgetVersion);
    paramParcel.writeParcelable(this.mSystemTcMetadata, paramInt);
  }
  
  private TextClassificationContext(Parcel paramParcel) {
    this.mPackageName = paramParcel.readString();
    this.mWidgetType = paramParcel.readString();
    this.mWidgetVersion = paramParcel.readString();
    this.mSystemTcMetadata = (SystemTextClassifierMetadata)paramParcel.readParcelable(null);
  }
  
  public static final Parcelable.Creator<TextClassificationContext> CREATOR = new Parcelable.Creator<TextClassificationContext>() {
      public TextClassificationContext createFromParcel(Parcel param1Parcel) {
        return new TextClassificationContext(param1Parcel);
      }
      
      public TextClassificationContext[] newArray(int param1Int) {
        return new TextClassificationContext[param1Int];
      }
    };
  
  private String mPackageName;
  
  private SystemTextClassifierMetadata mSystemTcMetadata;
  
  private final String mWidgetType;
  
  private final String mWidgetVersion;
}
