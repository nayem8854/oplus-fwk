package android.view.textclassifier;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Locale;
import java.util.Objects;

public final class SelectionEvent implements Parcelable {
  public static final int ACTION_ABANDON = 107;
  
  public static final int ACTION_COPY = 101;
  
  public static final int ACTION_CUT = 103;
  
  public static final int ACTION_DRAG = 106;
  
  public static final int ACTION_OTHER = 108;
  
  public static final int ACTION_OVERTYPE = 100;
  
  public static final int ACTION_PASTE = 102;
  
  public static final int ACTION_RESET = 201;
  
  public static final int ACTION_SELECT_ALL = 200;
  
  public static final int ACTION_SHARE = 104;
  
  public static final int ACTION_SMART_SHARE = 105;
  
  SelectionEvent(int paramInt1, int paramInt2, int paramInt3, String paramString1, int paramInt4, String paramString2) {
    boolean bool;
    this.mPackageName = "";
    this.mWidgetType = "unknown";
    if (paramInt2 >= paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "end cannot be less than start");
    this.mAbsoluteStart = paramInt1;
    this.mAbsoluteEnd = paramInt2;
    this.mEventType = paramInt3;
    Objects.requireNonNull(paramString1);
    this.mEntityType = paramString1;
    this.mResultId = paramString2;
    this.mInvocationMethod = paramInt4;
  }
  
  private SelectionEvent(Parcel paramParcel) {
    String str;
    this.mPackageName = "";
    this.mWidgetType = "unknown";
    this.mAbsoluteStart = paramParcel.readInt();
    this.mAbsoluteEnd = paramParcel.readInt();
    this.mEventType = paramParcel.readInt();
    this.mEntityType = paramParcel.readString();
    if (paramParcel.readInt() > 0) {
      str = paramParcel.readString();
    } else {
      str = null;
    } 
    this.mWidgetVersion = str;
    this.mPackageName = paramParcel.readString();
    this.mWidgetType = paramParcel.readString();
    this.mInvocationMethod = paramParcel.readInt();
    this.mResultId = paramParcel.readString();
    this.mEventTime = paramParcel.readLong();
    this.mDurationSinceSessionStart = paramParcel.readLong();
    this.mDurationSincePreviousEvent = paramParcel.readLong();
    this.mEventIndex = paramParcel.readInt();
    if (paramParcel.readInt() > 0) {
      TextClassificationSessionId textClassificationSessionId = (TextClassificationSessionId)TextClassificationSessionId.CREATOR.createFromParcel(paramParcel);
    } else {
      str = null;
    } 
    this.mSessionId = (TextClassificationSessionId)str;
    this.mStart = paramParcel.readInt();
    this.mEnd = paramParcel.readInt();
    this.mSmartStart = paramParcel.readInt();
    this.mSmartEnd = paramParcel.readInt();
    this.mSystemTcMetadata = (SystemTextClassifierMetadata)paramParcel.readParcelable(null);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    boolean bool2;
    paramParcel.writeInt(this.mAbsoluteStart);
    paramParcel.writeInt(this.mAbsoluteEnd);
    paramParcel.writeInt(this.mEventType);
    paramParcel.writeString(this.mEntityType);
    String str = this.mWidgetVersion;
    boolean bool1 = true;
    if (str != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramParcel.writeInt(bool2);
    str = this.mWidgetVersion;
    if (str != null)
      paramParcel.writeString(str); 
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeString(this.mWidgetType);
    paramParcel.writeInt(this.mInvocationMethod);
    paramParcel.writeString(this.mResultId);
    paramParcel.writeLong(this.mEventTime);
    paramParcel.writeLong(this.mDurationSinceSessionStart);
    paramParcel.writeLong(this.mDurationSincePreviousEvent);
    paramParcel.writeInt(this.mEventIndex);
    if (this.mSessionId != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    paramParcel.writeInt(bool2);
    TextClassificationSessionId textClassificationSessionId = this.mSessionId;
    if (textClassificationSessionId != null)
      textClassificationSessionId.writeToParcel(paramParcel, paramInt); 
    paramParcel.writeInt(this.mStart);
    paramParcel.writeInt(this.mEnd);
    paramParcel.writeInt(this.mSmartStart);
    paramParcel.writeInt(this.mSmartEnd);
    paramParcel.writeParcelable(this.mSystemTcMetadata, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static SelectionEvent createSelectionStartedEvent(int paramInt1, int paramInt2) {
    return new SelectionEvent(paramInt2, paramInt2 + 1, 1, "", paramInt1, "");
  }
  
  public static SelectionEvent createSelectionModifiedEvent(int paramInt1, int paramInt2) {
    boolean bool;
    if (paramInt2 >= paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "end cannot be less than start");
    return new SelectionEvent(paramInt1, paramInt2, 2, "", 0, "");
  }
  
  public static SelectionEvent createSelectionModifiedEvent(int paramInt1, int paramInt2, TextClassification paramTextClassification) {
    boolean bool;
    String str;
    if (paramInt2 >= paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "end cannot be less than start");
    Objects.requireNonNull(paramTextClassification);
    if (paramTextClassification.getEntityCount() > 0) {
      str = paramTextClassification.getEntity(0);
    } else {
      str = "";
    } 
    return 
      
      new SelectionEvent(paramInt1, paramInt2, 2, str, 0, paramTextClassification.getId());
  }
  
  public static SelectionEvent createSelectionModifiedEvent(int paramInt1, int paramInt2, TextSelection paramTextSelection) {
    boolean bool;
    String str;
    if (paramInt2 >= paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "end cannot be less than start");
    Objects.requireNonNull(paramTextSelection);
    if (paramTextSelection.getEntityCount() > 0) {
      str = paramTextSelection.getEntity(0);
    } else {
      str = "";
    } 
    return 
      
      new SelectionEvent(paramInt1, paramInt2, 5, str, 0, paramTextSelection.getId());
  }
  
  public static SelectionEvent createSelectionActionEvent(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool;
    if (paramInt2 >= paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "end cannot be less than start");
    checkActionType(paramInt3);
    return new SelectionEvent(paramInt1, paramInt2, paramInt3, "", 0, "");
  }
  
  public static SelectionEvent createSelectionActionEvent(int paramInt1, int paramInt2, int paramInt3, TextClassification paramTextClassification) {
    boolean bool;
    String str;
    if (paramInt2 >= paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "end cannot be less than start");
    Objects.requireNonNull(paramTextClassification);
    checkActionType(paramInt3);
    if (paramTextClassification.getEntityCount() > 0) {
      str = paramTextClassification.getEntity(0);
    } else {
      str = "";
    } 
    return 
      new SelectionEvent(paramInt1, paramInt2, paramInt3, str, 0, paramTextClassification.getId());
  }
  
  private static void checkActionType(int paramInt) throws IllegalArgumentException {
    if (paramInt != 200 && paramInt != 201) {
      Locale locale;
      switch (paramInt) {
        default:
          locale = Locale.US;
          throw new IllegalArgumentException(String.format(locale, "%d is not an eventType", new Object[] { Integer.valueOf(paramInt) }));
        case 100:
        case 101:
        case 102:
        case 103:
        case 104:
        case 105:
        case 106:
        case 107:
        case 108:
          break;
      } 
    } 
  }
  
  int getAbsoluteStart() {
    return this.mAbsoluteStart;
  }
  
  int getAbsoluteEnd() {
    return this.mAbsoluteEnd;
  }
  
  public int getEventType() {
    return this.mEventType;
  }
  
  public void setEventType(int paramInt) {
    this.mEventType = paramInt;
  }
  
  public String getEntityType() {
    return this.mEntityType;
  }
  
  void setEntityType(String paramString) {
    Objects.requireNonNull(paramString);
    this.mEntityType = paramString;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  void setSystemTextClassifierMetadata(SystemTextClassifierMetadata paramSystemTextClassifierMetadata) {
    this.mSystemTcMetadata = paramSystemTextClassifierMetadata;
  }
  
  public SystemTextClassifierMetadata getSystemTextClassifierMetadata() {
    return this.mSystemTcMetadata;
  }
  
  public String getWidgetType() {
    return this.mWidgetType;
  }
  
  public String getWidgetVersion() {
    return this.mWidgetVersion;
  }
  
  public void setTextClassificationSessionContext(TextClassificationContext paramTextClassificationContext) {
    this.mPackageName = paramTextClassificationContext.getPackageName();
    this.mWidgetType = paramTextClassificationContext.getWidgetType();
    this.mWidgetVersion = paramTextClassificationContext.getWidgetVersion();
    this.mSystemTcMetadata = paramTextClassificationContext.getSystemTextClassifierMetadata();
  }
  
  public int getInvocationMethod() {
    return this.mInvocationMethod;
  }
  
  public void setInvocationMethod(int paramInt) {
    this.mInvocationMethod = paramInt;
  }
  
  public String getResultId() {
    return this.mResultId;
  }
  
  SelectionEvent setResultId(String paramString) {
    this.mResultId = paramString;
    return this;
  }
  
  public long getEventTime() {
    return this.mEventTime;
  }
  
  SelectionEvent setEventTime(long paramLong) {
    this.mEventTime = paramLong;
    return this;
  }
  
  public long getDurationSinceSessionStart() {
    return this.mDurationSinceSessionStart;
  }
  
  SelectionEvent setDurationSinceSessionStart(long paramLong) {
    this.mDurationSinceSessionStart = paramLong;
    return this;
  }
  
  public long getDurationSincePreviousEvent() {
    return this.mDurationSincePreviousEvent;
  }
  
  SelectionEvent setDurationSincePreviousEvent(long paramLong) {
    this.mDurationSincePreviousEvent = paramLong;
    return this;
  }
  
  public int getEventIndex() {
    return this.mEventIndex;
  }
  
  public SelectionEvent setEventIndex(int paramInt) {
    this.mEventIndex = paramInt;
    return this;
  }
  
  public TextClassificationSessionId getSessionId() {
    return this.mSessionId;
  }
  
  public SelectionEvent setSessionId(TextClassificationSessionId paramTextClassificationSessionId) {
    this.mSessionId = paramTextClassificationSessionId;
    return this;
  }
  
  public int getStart() {
    return this.mStart;
  }
  
  public SelectionEvent setStart(int paramInt) {
    this.mStart = paramInt;
    return this;
  }
  
  public int getEnd() {
    return this.mEnd;
  }
  
  public SelectionEvent setEnd(int paramInt) {
    this.mEnd = paramInt;
    return this;
  }
  
  public int getSmartStart() {
    return this.mSmartStart;
  }
  
  public SelectionEvent setSmartStart(int paramInt) {
    this.mSmartStart = paramInt;
    return this;
  }
  
  public int getSmartEnd() {
    return this.mSmartEnd;
  }
  
  public SelectionEvent setSmartEnd(int paramInt) {
    this.mSmartEnd = paramInt;
    return this;
  }
  
  boolean isTerminal() {
    return isTerminal(this.mEventType);
  }
  
  public static boolean isTerminal(int paramInt) {
    switch (paramInt) {
      default:
        return false;
      case 100:
      case 101:
      case 102:
      case 103:
      case 104:
      case 105:
      case 106:
      case 107:
      case 108:
        break;
    } 
    return true;
  }
  
  public int hashCode() {
    int i = this.mAbsoluteStart, j = this.mAbsoluteEnd, k = this.mEventType;
    String str1 = this.mEntityType, str2 = this.mWidgetVersion, str3 = this.mPackageName, str4 = this.mWidgetType;
    int m = this.mInvocationMethod;
    String str5 = this.mResultId;
    long l1 = this.mEventTime;
    long l2 = this.mDurationSinceSessionStart, l3 = this.mDurationSincePreviousEvent;
    int n = this.mEventIndex;
    TextClassificationSessionId textClassificationSessionId = this.mSessionId;
    int i1 = this.mStart, i2 = this.mEnd, i3 = this.mSmartStart, i4 = this.mSmartEnd;
    SystemTextClassifierMetadata systemTextClassifierMetadata = this.mSystemTcMetadata;
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), str1, str2, str3, str4, Integer.valueOf(m), str5, Long.valueOf(l1), 
          Long.valueOf(l2), Long.valueOf(l3), Integer.valueOf(n), textClassificationSessionId, Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), systemTextClassifierMetadata });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof SelectionEvent))
      return false; 
    paramObject = paramObject;
    if (this.mAbsoluteStart == ((SelectionEvent)paramObject).mAbsoluteStart && this.mAbsoluteEnd == ((SelectionEvent)paramObject).mAbsoluteEnd && this.mEventType == ((SelectionEvent)paramObject).mEventType) {
      String str1 = this.mEntityType, str2 = ((SelectionEvent)paramObject).mEntityType;
      if (Objects.equals(str1, str2)) {
        str2 = this.mWidgetVersion;
        str1 = ((SelectionEvent)paramObject).mWidgetVersion;
        if (Objects.equals(str2, str1)) {
          str1 = this.mPackageName;
          str2 = ((SelectionEvent)paramObject).mPackageName;
          if (Objects.equals(str1, str2)) {
            str1 = this.mWidgetType;
            str2 = ((SelectionEvent)paramObject).mWidgetType;
            if (Objects.equals(str1, str2) && this.mInvocationMethod == ((SelectionEvent)paramObject).mInvocationMethod) {
              str2 = this.mResultId;
              str1 = ((SelectionEvent)paramObject).mResultId;
              if (Objects.equals(str2, str1) && this.mEventTime == ((SelectionEvent)paramObject).mEventTime && this.mDurationSinceSessionStart == ((SelectionEvent)paramObject).mDurationSinceSessionStart && this.mDurationSincePreviousEvent == ((SelectionEvent)paramObject).mDurationSincePreviousEvent && this.mEventIndex == ((SelectionEvent)paramObject).mEventIndex) {
                TextClassificationSessionId textClassificationSessionId2 = this.mSessionId, textClassificationSessionId1 = ((SelectionEvent)paramObject).mSessionId;
                if (Objects.equals(textClassificationSessionId2, textClassificationSessionId1) && this.mStart == ((SelectionEvent)paramObject).mStart && this.mEnd == ((SelectionEvent)paramObject).mEnd && this.mSmartStart == ((SelectionEvent)paramObject).mSmartStart && this.mSmartEnd == ((SelectionEvent)paramObject).mSmartEnd && this.mSystemTcMetadata == ((SelectionEvent)paramObject).mSystemTcMetadata)
                  return null; 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public String toString() {
    Locale locale = Locale.US;
    int i = this.mAbsoluteStart;
    int j = this.mAbsoluteEnd, k = this.mEventType;
    String str1 = this.mEntityType, str2 = this.mWidgetVersion, str3 = this.mPackageName, str4 = this.mWidgetType;
    int m = this.mInvocationMethod;
    String str5 = this.mResultId;
    long l1 = this.mEventTime;
    long l2 = this.mDurationSinceSessionStart, l3 = this.mDurationSincePreviousEvent;
    int n = this.mEventIndex;
    TextClassificationSessionId textClassificationSessionId = this.mSessionId;
    int i1 = this.mStart, i2 = this.mEnd, i3 = this.mSmartStart, i4 = this.mSmartEnd;
    SystemTextClassifierMetadata systemTextClassifierMetadata = this.mSystemTcMetadata;
    return String.format(locale, "SelectionEvent {absoluteStart=%d, absoluteEnd=%d, eventType=%d, entityType=%s, widgetVersion=%s, packageName=%s, widgetType=%s, invocationMethod=%s, resultId=%s, eventTime=%d, durationSinceSessionStart=%d, durationSincePreviousEvent=%d, eventIndex=%d,sessionId=%s, start=%d, end=%d, smartStart=%d, smartEnd=%d, systemTcMetadata=%s}", new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), str1, str2, str3, str4, Integer.valueOf(m), str5, Long.valueOf(l1), 
          Long.valueOf(l2), Long.valueOf(l3), Integer.valueOf(n), textClassificationSessionId, Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), systemTextClassifierMetadata });
  }
  
  public static final Parcelable.Creator<SelectionEvent> CREATOR = new Parcelable.Creator<SelectionEvent>() {
      public SelectionEvent createFromParcel(Parcel param1Parcel) {
        return new SelectionEvent(param1Parcel);
      }
      
      public SelectionEvent[] newArray(int param1Int) {
        return new SelectionEvent[param1Int];
      }
    };
  
  public static final int EVENT_AUTO_SELECTION = 5;
  
  public static final int EVENT_SELECTION_MODIFIED = 2;
  
  public static final int EVENT_SELECTION_STARTED = 1;
  
  public static final int EVENT_SMART_SELECTION_MULTI = 4;
  
  public static final int EVENT_SMART_SELECTION_SINGLE = 3;
  
  public static final int INVOCATION_LINK = 2;
  
  public static final int INVOCATION_MANUAL = 1;
  
  public static final int INVOCATION_UNKNOWN = 0;
  
  static final String NO_SIGNATURE = "";
  
  private final int mAbsoluteEnd;
  
  private final int mAbsoluteStart;
  
  private long mDurationSincePreviousEvent;
  
  private long mDurationSinceSessionStart;
  
  private int mEnd;
  
  private String mEntityType;
  
  private int mEventIndex;
  
  private long mEventTime;
  
  private int mEventType;
  
  private int mInvocationMethod;
  
  private String mPackageName;
  
  private String mResultId;
  
  private TextClassificationSessionId mSessionId;
  
  private int mSmartEnd;
  
  private int mSmartStart;
  
  private int mStart;
  
  private SystemTextClassifierMetadata mSystemTcMetadata;
  
  private String mWidgetType;
  
  private String mWidgetVersion;
  
  @Retention(RetentionPolicy.SOURCE)
  class ActionType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class EventType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class InvocationMethod implements Annotation {}
}
