package android.view.textclassifier;

public final class SelectionSessionLogger {
  private static final String CLASSIFIER_ID = "androidtc";
  
  static boolean isPlatformLocalTextClassifierSmartSelection(String paramString) {
    paramString = SignatureParser.getClassifierId(paramString);
    return "androidtc".equals(paramString);
  }
  
  public static final class SignatureParser {
    static String getClassifierId(String param1String) {
      if (param1String == null)
        return ""; 
      int i = param1String.indexOf("|");
      if (i >= 0)
        return param1String.substring(0, i); 
      return "";
    }
  }
}
