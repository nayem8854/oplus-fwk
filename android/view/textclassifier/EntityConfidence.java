package android.view.textclassifier;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

final class EntityConfidence implements Parcelable {
  private final ArrayMap<String, Float> mEntityConfidence = new ArrayMap<>();
  
  private final ArrayList<String> mSortedEntities = new ArrayList<>();
  
  EntityConfidence(EntityConfidence paramEntityConfidence) {
    Objects.requireNonNull(paramEntityConfidence);
    this.mEntityConfidence.putAll(paramEntityConfidence.mEntityConfidence);
    this.mSortedEntities.addAll(paramEntityConfidence.mSortedEntities);
  }
  
  EntityConfidence(Map<String, Float> paramMap) {
    Objects.requireNonNull(paramMap);
    this.mEntityConfidence.ensureCapacity(paramMap.size());
    for (Map.Entry<String, Float> entry : paramMap.entrySet()) {
      if (((Float)entry.getValue()).floatValue() <= 0.0F)
        continue; 
      this.mEntityConfidence.put((String)entry.getKey(), Float.valueOf(Math.min(1.0F, ((Float)entry.getValue()).floatValue())));
    } 
    resetSortedEntitiesFromMap();
  }
  
  public List<String> getEntities() {
    return Collections.unmodifiableList(this.mSortedEntities);
  }
  
  public float getConfidenceScore(String paramString) {
    if (this.mEntityConfidence.containsKey(paramString))
      return ((Float)this.mEntityConfidence.get(paramString)).floatValue(); 
    return 0.0F;
  }
  
  public Map<String, Float> toMap() {
    return new ArrayMap<>(this.mEntityConfidence);
  }
  
  public String toString() {
    return this.mEntityConfidence.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mEntityConfidence.size());
    for (Map.Entry<String, Float> entry : this.mEntityConfidence.entrySet()) {
      paramParcel.writeString((String)entry.getKey());
      paramParcel.writeFloat(((Float)entry.getValue()).floatValue());
    } 
  }
  
  public static final Parcelable.Creator<EntityConfidence> CREATOR = new Parcelable.Creator<EntityConfidence>() {
      public EntityConfidence createFromParcel(Parcel param1Parcel) {
        return new EntityConfidence(param1Parcel);
      }
      
      public EntityConfidence[] newArray(int param1Int) {
        return new EntityConfidence[param1Int];
      }
    };
  
  private EntityConfidence(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.mEntityConfidence.ensureCapacity(i);
    for (byte b = 0; b < i; b++)
      this.mEntityConfidence.put(paramParcel.readString(), Float.valueOf(paramParcel.readFloat())); 
    resetSortedEntitiesFromMap();
  }
  
  private void resetSortedEntitiesFromMap() {
    this.mSortedEntities.clear();
    this.mSortedEntities.ensureCapacity(this.mEntityConfidence.size());
    this.mSortedEntities.addAll(this.mEntityConfidence.keySet());
    this.mSortedEntities.sort(new _$$Lambda$EntityConfidence$YPh8hwgSYYK8OyQ1kFlQngc71Q0(this));
  }
  
  EntityConfidence() {}
}
