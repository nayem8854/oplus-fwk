package android.view.textclassifier;

import android.icu.util.ULocale;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import com.android.internal.util.Preconditions;
import java.util.Locale;
import java.util.Map;

public final class TextLanguage implements Parcelable {
  public static final Parcelable.Creator<TextLanguage> CREATOR = new Parcelable.Creator<TextLanguage>() {
      public TextLanguage createFromParcel(Parcel param1Parcel) {
        return TextLanguage.readFromParcel(param1Parcel);
      }
      
      public TextLanguage[] newArray(int param1Int) {
        return new TextLanguage[param1Int];
      }
    };
  
  static final TextLanguage EMPTY = (new Builder()).build();
  
  private final Bundle mBundle;
  
  private final EntityConfidence mEntityConfidence;
  
  private final String mId;
  
  private TextLanguage(String paramString, EntityConfidence paramEntityConfidence, Bundle paramBundle) {
    this.mId = paramString;
    this.mEntityConfidence = paramEntityConfidence;
    this.mBundle = paramBundle;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public int getLocaleHypothesisCount() {
    return this.mEntityConfidence.getEntities().size();
  }
  
  public ULocale getLocale(int paramInt) {
    return ULocale.forLanguageTag(this.mEntityConfidence.getEntities().get(paramInt));
  }
  
  public float getConfidenceScore(ULocale paramULocale) {
    return this.mEntityConfidence.getConfidenceScore(paramULocale.toLanguageTag());
  }
  
  public Bundle getExtras() {
    return this.mBundle;
  }
  
  public String toString() {
    return String.format(Locale.US, "TextLanguage {id=%s, locales=%s, bundle=%s}", new Object[] { this.mId, this.mEntityConfidence, this.mBundle });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    this.mEntityConfidence.writeToParcel(paramParcel, paramInt);
    paramParcel.writeBundle(this.mBundle);
  }
  
  private static TextLanguage readFromParcel(Parcel paramParcel) {
    String str = paramParcel.readString();
    Parcelable.Creator<EntityConfidence> creator = EntityConfidence.CREATOR;
    EntityConfidence entityConfidence = (EntityConfidence)creator.createFromParcel(paramParcel);
    return new TextLanguage(str, entityConfidence, paramParcel.readBundle());
  }
  
  class Builder {
    private Bundle mBundle;
    
    private final Map<String, Float> mEntityConfidenceMap;
    
    private String mId;
    
    public Builder() {
      this.mEntityConfidenceMap = new ArrayMap<>();
    }
    
    public Builder putLocale(ULocale param1ULocale, float param1Float) {
      Preconditions.checkNotNull(param1ULocale);
      this.mEntityConfidenceMap.put(param1ULocale.toLanguageTag(), Float.valueOf(param1Float));
      return this;
    }
    
    public Builder setId(String param1String) {
      this.mId = param1String;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mBundle = (Bundle)Preconditions.checkNotNull(param1Bundle);
      return this;
    }
    
    public TextLanguage build() {
      Bundle bundle1 = this.mBundle, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      this.mBundle = bundle2;
      return new TextLanguage(this.mId, new EntityConfidence(this.mEntityConfidenceMap), this.mBundle);
    }
  }
  
  public static final class Request implements Parcelable {
    public static final Parcelable.Creator<Request> CREATOR = new Parcelable.Creator<Request>() {
        public TextLanguage.Request createFromParcel(Parcel param2Parcel) {
          return TextLanguage.Request.readFromParcel(param2Parcel);
        }
        
        public TextLanguage.Request[] newArray(int param2Int) {
          return new TextLanguage.Request[param2Int];
        }
      };
    
    private final Bundle mExtra;
    
    private SystemTextClassifierMetadata mSystemTcMetadata;
    
    private final CharSequence mText;
    
    private Request(CharSequence param1CharSequence, Bundle param1Bundle) {
      this.mText = param1CharSequence;
      this.mExtra = param1Bundle;
    }
    
    public CharSequence getText() {
      return this.mText;
    }
    
    public String getCallingPackageName() {
      SystemTextClassifierMetadata systemTextClassifierMetadata = this.mSystemTcMetadata;
      if (systemTextClassifierMetadata != null) {
        String str = systemTextClassifierMetadata.getCallingPackageName();
      } else {
        systemTextClassifierMetadata = null;
      } 
      return (String)systemTextClassifierMetadata;
    }
    
    public void setSystemTextClassifierMetadata(SystemTextClassifierMetadata param1SystemTextClassifierMetadata) {
      this.mSystemTcMetadata = param1SystemTextClassifierMetadata;
    }
    
    public SystemTextClassifierMetadata getSystemTextClassifierMetadata() {
      return this.mSystemTcMetadata;
    }
    
    public Bundle getExtras() {
      return this.mExtra;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeCharSequence(this.mText);
      param1Parcel.writeBundle(this.mExtra);
      param1Parcel.writeParcelable(this.mSystemTcMetadata, param1Int);
    }
    
    private static Request readFromParcel(Parcel param1Parcel) {
      CharSequence charSequence = param1Parcel.readCharSequence();
      Bundle bundle = param1Parcel.readBundle();
      SystemTextClassifierMetadata systemTextClassifierMetadata = (SystemTextClassifierMetadata)param1Parcel.readParcelable(null);
      Request request = new Request(charSequence, bundle);
      request.setSystemTextClassifierMetadata(systemTextClassifierMetadata);
      return request;
    }
    
    class Builder {
      private Bundle mBundle;
      
      private final CharSequence mText;
      
      public Builder(TextLanguage.Request this$0) {
        this.mText = (CharSequence)Preconditions.checkNotNull(this$0);
      }
      
      public Builder setExtras(Bundle param2Bundle) {
        this.mBundle = (Bundle)Preconditions.checkNotNull(param2Bundle);
        return this;
      }
      
      public TextLanguage.Request build() {
        String str = this.mText.toString();
        Bundle bundle1 = this.mBundle, bundle2 = bundle1;
        if (bundle1 == null)
          bundle2 = Bundle.EMPTY; 
        return new TextLanguage.Request(str, bundle2);
      }
    }
  }
  
  class null implements Parcelable.Creator<Request> {
    public TextLanguage.Request createFromParcel(Parcel param1Parcel) {
      return TextLanguage.Request.readFromParcel(param1Parcel);
    }
    
    public TextLanguage.Request[] newArray(int param1Int) {
      return new TextLanguage.Request[param1Int];
    }
  }
  
  class Builder {
    private Bundle mBundle;
    
    private final CharSequence mText;
    
    public Builder(TextLanguage this$0) {
      this.mText = (CharSequence)Preconditions.checkNotNull(this$0);
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mBundle = (Bundle)Preconditions.checkNotNull(param1Bundle);
      return this;
    }
    
    public TextLanguage.Request build() {
      String str = this.mText.toString();
      Bundle bundle1 = this.mBundle, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new TextLanguage.Request(str, bundle2);
    }
  }
}
