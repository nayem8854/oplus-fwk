package android.view.textclassifier;

import android.provider.DeviceConfig;
import com.android.internal.util.IndentingPrintWriter;

public final class TextClassificationConstants {
  private static final String DEFAULT_TEXT_CLASSIFIER_SERVICE_PACKAGE_OVERRIDE = null;
  
  static final String GENERATE_LINKS_MAX_TEXT_LENGTH = "generate_links_max_text_length";
  
  private static final int GENERATE_LINKS_MAX_TEXT_LENGTH_DEFAULT = 100000;
  
  static final String LOCAL_TEXT_CLASSIFIER_ENABLED = "local_textclassifier_enabled";
  
  private static final boolean LOCAL_TEXT_CLASSIFIER_ENABLED_DEFAULT = true;
  
  private static final String MODEL_DARK_LAUNCH_ENABLED = "model_dark_launch_enabled";
  
  private static final boolean MODEL_DARK_LAUNCH_ENABLED_DEFAULT = false;
  
  private static final String SMART_LINKIFY_ENABLED = "smart_linkify_enabled";
  
  private static final boolean SMART_LINKIFY_ENABLED_DEFAULT = true;
  
  private static final String SMART_SELECTION_ENABLED = "smart_selection_enabled";
  
  private static final boolean SMART_SELECTION_ENABLED_DEFAULT = true;
  
  private static final String SMART_SELECT_ANIMATION_ENABLED = "smart_select_animation_enabled";
  
  private static final boolean SMART_SELECT_ANIMATION_ENABLED_DEFAULT = true;
  
  private static final String SMART_TEXT_SHARE_ENABLED = "smart_text_share_enabled";
  
  private static final boolean SMART_TEXT_SHARE_ENABLED_DEFAULT = true;
  
  static final String SYSTEM_TEXT_CLASSIFIER_ENABLED = "system_textclassifier_enabled";
  
  private static final boolean SYSTEM_TEXT_CLASSIFIER_ENABLED_DEFAULT = true;
  
  static final String TEXT_CLASSIFIER_SERVICE_PACKAGE_OVERRIDE = "textclassifier_service_package_override";
  
  public String getTextClassifierServicePackageOverride() {
    return DeviceConfig.getString("textclassifier", "textclassifier_service_package_override", DEFAULT_TEXT_CLASSIFIER_SERVICE_PACKAGE_OVERRIDE);
  }
  
  public boolean isLocalTextClassifierEnabled() {
    return DeviceConfig.getBoolean("textclassifier", "local_textclassifier_enabled", true);
  }
  
  public boolean isSystemTextClassifierEnabled() {
    return DeviceConfig.getBoolean("textclassifier", "system_textclassifier_enabled", true);
  }
  
  public boolean isModelDarkLaunchEnabled() {
    return DeviceConfig.getBoolean("textclassifier", "model_dark_launch_enabled", false);
  }
  
  public boolean isSmartSelectionEnabled() {
    return DeviceConfig.getBoolean("textclassifier", "smart_selection_enabled", true);
  }
  
  public boolean isSmartTextShareEnabled() {
    return DeviceConfig.getBoolean("textclassifier", "smart_text_share_enabled", true);
  }
  
  public boolean isSmartLinkifyEnabled() {
    return DeviceConfig.getBoolean("textclassifier", "smart_linkify_enabled", true);
  }
  
  public boolean isSmartSelectionAnimationEnabled() {
    return DeviceConfig.getBoolean("textclassifier", "smart_select_animation_enabled", true);
  }
  
  public int getGenerateLinksMaxTextLength() {
    return DeviceConfig.getInt("textclassifier", "generate_links_max_text_length", 100000);
  }
  
  void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    paramIndentingPrintWriter.println("TextClassificationConstants:");
    paramIndentingPrintWriter.increaseIndent();
    IndentingPrintWriter indentingPrintWriter2 = paramIndentingPrintWriter.printPair("generate_links_max_text_length", Integer.valueOf(getGenerateLinksMaxTextLength()));
    indentingPrintWriter2.println();
    indentingPrintWriter2 = paramIndentingPrintWriter.printPair("local_textclassifier_enabled", Boolean.valueOf(isLocalTextClassifierEnabled()));
    indentingPrintWriter2.println();
    indentingPrintWriter2 = paramIndentingPrintWriter.printPair("model_dark_launch_enabled", Boolean.valueOf(isModelDarkLaunchEnabled()));
    indentingPrintWriter2.println();
    indentingPrintWriter2 = paramIndentingPrintWriter.printPair("smart_linkify_enabled", Boolean.valueOf(isSmartLinkifyEnabled()));
    indentingPrintWriter2.println();
    indentingPrintWriter2 = paramIndentingPrintWriter.printPair("smart_select_animation_enabled", Boolean.valueOf(isSmartSelectionAnimationEnabled()));
    indentingPrintWriter2.println();
    indentingPrintWriter2 = paramIndentingPrintWriter.printPair("smart_selection_enabled", Boolean.valueOf(isSmartSelectionEnabled()));
    indentingPrintWriter2.println();
    indentingPrintWriter2 = paramIndentingPrintWriter.printPair("smart_text_share_enabled", Boolean.valueOf(isSmartTextShareEnabled()));
    indentingPrintWriter2.println();
    indentingPrintWriter2 = paramIndentingPrintWriter.printPair("system_textclassifier_enabled", Boolean.valueOf(isSystemTextClassifierEnabled()));
    indentingPrintWriter2.println();
    String str = getTextClassifierServicePackageOverride();
    IndentingPrintWriter indentingPrintWriter1 = paramIndentingPrintWriter.printPair("textclassifier_service_package_override", str);
    indentingPrintWriter1.println();
    paramIndentingPrintWriter.decreaseIndent();
  }
}
