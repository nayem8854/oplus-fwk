package android.view.textclassifier;

import android.text.Spannable;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.util.EventLog;
import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Function;

public final class TextLinksParams {
  private static final Function<TextLinks.TextLink, TextLinks.TextLinkSpan> DEFAULT_SPAN_FACTORY = (Function<TextLinks.TextLink, TextLinks.TextLinkSpan>)_$$Lambda$TextLinksParams$km8pN8nazHT6NQiHykIrRALWbkE.INSTANCE;
  
  private final int mApplyStrategy;
  
  private final TextClassifier.EntityConfig mEntityConfig;
  
  private final Function<TextLinks.TextLink, TextLinks.TextLinkSpan> mSpanFactory;
  
  private TextLinksParams(int paramInt, Function<TextLinks.TextLink, TextLinks.TextLinkSpan> paramFunction) {
    this.mApplyStrategy = paramInt;
    this.mSpanFactory = paramFunction;
    this.mEntityConfig = TextClassifier.EntityConfig.createWithHints(null);
  }
  
  public static TextLinksParams fromLinkMask(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    if ((paramInt & 0x1) != 0)
      arrayList.add("url"); 
    if ((paramInt & 0x2) != 0)
      arrayList.add("email"); 
    if ((paramInt & 0x4) != 0)
      arrayList.add("phone"); 
    if ((paramInt & 0x8) != 0)
      arrayList.add("address"); 
    Builder builder = new Builder();
    TextClassifier.EntityConfig entityConfig = TextClassifier.EntityConfig.createWithExplicitEntityList(arrayList);
    builder = builder.setEntityConfig(entityConfig);
    return builder.build();
  }
  
  public TextClassifier.EntityConfig getEntityConfig() {
    return this.mEntityConfig;
  }
  
  public int apply(Spannable paramSpannable, TextLinks paramTextLinks) {
    Objects.requireNonNull(paramSpannable);
    Objects.requireNonNull(paramTextLinks);
    String str = paramSpannable.toString();
    if (Linkify.containsUnsupportedCharacters(str)) {
      EventLog.writeEvent(1397638484, new Object[] { "116321860", Integer.valueOf(-1), "" });
      return 4;
    } 
    if (!str.startsWith(paramTextLinks.getText().toString()))
      return 3; 
    if (paramTextLinks.getLinks().isEmpty())
      return 1; 
    int i = 0;
    for (TextLinks.TextLink textLink : paramTextLinks.getLinks()) {
      TextLinks.TextLinkSpan textLinkSpan = this.mSpanFactory.apply(textLink);
      int j = i;
      if (textLinkSpan != null) {
        int k = textLink.getStart();
        j = textLink.getEnd();
        ClickableSpan[] arrayOfClickableSpan = paramSpannable.<ClickableSpan>getSpans(k, j, ClickableSpan.class);
        if (arrayOfClickableSpan.length > 0) {
          j = i;
          if (this.mApplyStrategy == 1) {
            for (k = arrayOfClickableSpan.length, j = 0; j < k; ) {
              ClickableSpan clickableSpan = arrayOfClickableSpan[j];
              paramSpannable.removeSpan(clickableSpan);
              j++;
            } 
            paramSpannable.setSpan(textLinkSpan, textLink.getStart(), textLink.getEnd(), 33);
            j = i + 1;
          } 
        } else {
          paramSpannable.setSpan(textLinkSpan, textLink.getStart(), textLink.getEnd(), 33);
          j = i + 1;
        } 
      } 
      i = j;
    } 
    if (i == 0)
      return 2; 
    return 0;
  }
  
  public static final class Builder {
    private int mApplyStrategy = 0;
    
    private Function<TextLinks.TextLink, TextLinks.TextLinkSpan> mSpanFactory = TextLinksParams.DEFAULT_SPAN_FACTORY;
    
    public Builder setApplyStrategy(int param1Int) {
      this.mApplyStrategy = TextLinksParams.checkApplyStrategy(param1Int);
      return this;
    }
    
    public Builder setSpanFactory(Function<TextLinks.TextLink, TextLinks.TextLinkSpan> param1Function) {
      if (param1Function == null)
        param1Function = TextLinksParams.DEFAULT_SPAN_FACTORY; 
      this.mSpanFactory = param1Function;
      return this;
    }
    
    public Builder setEntityConfig(TextClassifier.EntityConfig param1EntityConfig) {
      return this;
    }
    
    public TextLinksParams build() {
      return new TextLinksParams(this.mApplyStrategy, this.mSpanFactory);
    }
  }
  
  private static int checkApplyStrategy(int paramInt) {
    if (paramInt == 0 || paramInt == 1)
      return paramInt; 
    throw new IllegalArgumentException("Invalid apply strategy. See TextLinksParams.ApplyStrategy for options.");
  }
}
