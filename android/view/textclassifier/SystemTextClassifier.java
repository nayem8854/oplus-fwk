package android.view.textclassifier;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.service.textclassifier.ITextClassifierCallback;
import android.service.textclassifier.ITextClassifierService;
import android.service.textclassifier.TextClassifierService;
import com.android.internal.util.IndentingPrintWriter;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public final class SystemTextClassifier implements TextClassifier {
  private static final String LOG_TAG = "androidtc";
  
  private final TextClassifier mFallback;
  
  private final ITextClassifierService mManagerService;
  
  private TextClassificationSessionId mSessionId;
  
  private final TextClassificationConstants mSettings;
  
  private final SystemTextClassifierMetadata mSystemTcMetadata;
  
  public SystemTextClassifier(Context paramContext, TextClassificationConstants paramTextClassificationConstants, boolean paramBoolean) throws ServiceManager.ServiceNotFoundException {
    IBinder iBinder = ServiceManager.getServiceOrThrow("textclassification");
    this.mManagerService = ITextClassifierService.Stub.asInterface(iBinder);
    Objects.requireNonNull(paramTextClassificationConstants);
    this.mSettings = paramTextClassificationConstants;
    this.mFallback = TextClassifier.NO_OP;
    String str = paramContext.getOpPackageName();
    Objects.requireNonNull(str);
    this.mSystemTcMetadata = new SystemTextClassifierMetadata(str, paramContext.getUserId(), paramBoolean);
  }
  
  public TextSelection suggestSelection(TextSelection.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    TextClassifier.Utils.checkMainThread();
    try {
      paramRequest.setSystemTextClassifierMetadata(this.mSystemTcMetadata);
      BlockingCallback<Parcelable> blockingCallback = new BlockingCallback<>();
      this("textselection");
      this.mManagerService.onSuggestSelection(this.mSessionId, paramRequest, (ITextClassifierCallback)blockingCallback);
      TextSelection textSelection = (TextSelection)blockingCallback.get();
      if (textSelection != null)
        return textSelection; 
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error suggesting selection for text. Using fallback.", (Throwable)remoteException);
    } 
    return this.mFallback.suggestSelection(paramRequest);
  }
  
  public TextClassification classifyText(TextClassification.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    TextClassifier.Utils.checkMainThread();
    try {
      paramRequest.setSystemTextClassifierMetadata(this.mSystemTcMetadata);
      BlockingCallback<Parcelable> blockingCallback = new BlockingCallback<>();
      this("textclassification");
      this.mManagerService.onClassifyText(this.mSessionId, paramRequest, (ITextClassifierCallback)blockingCallback);
      TextClassification textClassification = (TextClassification)blockingCallback.get();
      if (textClassification != null)
        return textClassification; 
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error classifying text. Using fallback.", (Throwable)remoteException);
    } 
    return this.mFallback.classifyText(paramRequest);
  }
  
  public TextLinks generateLinks(TextLinks.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    TextClassifier.Utils.checkMainThread();
    if (!TextClassifier.Utils.checkTextLength(paramRequest.getText(), getMaxGenerateLinksTextLength()))
      return this.mFallback.generateLinks(paramRequest); 
    if (!this.mSettings.isSmartLinkifyEnabled() && paramRequest.isLegacyFallback())
      return TextClassifier.Utils.generateLegacyLinks(paramRequest); 
    try {
      paramRequest.setSystemTextClassifierMetadata(this.mSystemTcMetadata);
      BlockingCallback<Parcelable> blockingCallback = new BlockingCallback<>();
      this("textlinks");
      this.mManagerService.onGenerateLinks(this.mSessionId, paramRequest, (ITextClassifierCallback)blockingCallback);
      TextLinks textLinks = (TextLinks)blockingCallback.get();
      if (textLinks != null)
        return textLinks; 
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error generating links. Using fallback.", (Throwable)remoteException);
    } 
    return this.mFallback.generateLinks(paramRequest);
  }
  
  public void onSelectionEvent(SelectionEvent paramSelectionEvent) {
    Objects.requireNonNull(paramSelectionEvent);
    TextClassifier.Utils.checkMainThread();
    try {
      paramSelectionEvent.setSystemTextClassifierMetadata(this.mSystemTcMetadata);
      this.mManagerService.onSelectionEvent(this.mSessionId, paramSelectionEvent);
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error reporting selection event.", (Throwable)remoteException);
    } 
  }
  
  public void onTextClassifierEvent(TextClassifierEvent paramTextClassifierEvent) {
    Objects.requireNonNull(paramTextClassifierEvent);
    TextClassifier.Utils.checkMainThread();
    try {
      TextClassificationContext textClassificationContext;
      if (paramTextClassifierEvent.getEventContext() == null) {
        TextClassificationContext.Builder builder = new TextClassificationContext.Builder();
        this(this.mSystemTcMetadata.getCallingPackageName(), "unknown");
        textClassificationContext = builder.build();
      } else {
        textClassificationContext = paramTextClassifierEvent.getEventContext();
      } 
      textClassificationContext.setSystemTextClassifierMetadata(this.mSystemTcMetadata);
      paramTextClassifierEvent.setEventContext(textClassificationContext);
      this.mManagerService.onTextClassifierEvent(this.mSessionId, paramTextClassifierEvent);
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error reporting textclassifier event.", (Throwable)remoteException);
    } 
  }
  
  public TextLanguage detectLanguage(TextLanguage.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    TextClassifier.Utils.checkMainThread();
    try {
      paramRequest.setSystemTextClassifierMetadata(this.mSystemTcMetadata);
      BlockingCallback<Parcelable> blockingCallback = new BlockingCallback<>();
      this("textlanguage");
      this.mManagerService.onDetectLanguage(this.mSessionId, paramRequest, (ITextClassifierCallback)blockingCallback);
      TextLanguage textLanguage = (TextLanguage)blockingCallback.get();
      if (textLanguage != null)
        return textLanguage; 
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error detecting language.", (Throwable)remoteException);
    } 
    return this.mFallback.detectLanguage(paramRequest);
  }
  
  public ConversationActions suggestConversationActions(ConversationActions.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    TextClassifier.Utils.checkMainThread();
    try {
      paramRequest.setSystemTextClassifierMetadata(this.mSystemTcMetadata);
      BlockingCallback<Parcelable> blockingCallback = new BlockingCallback<>();
      this("conversation-actions");
      this.mManagerService.onSuggestConversationActions(this.mSessionId, paramRequest, (ITextClassifierCallback)blockingCallback);
      ConversationActions conversationActions = (ConversationActions)blockingCallback.get();
      if (conversationActions != null)
        return conversationActions; 
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error reporting selection event.", (Throwable)remoteException);
    } 
    return this.mFallback.suggestConversationActions(paramRequest);
  }
  
  public int getMaxGenerateLinksTextLength() {
    return this.mSettings.getGenerateLinksMaxTextLength();
  }
  
  public void destroy() {
    try {
      if (this.mSessionId != null)
        this.mManagerService.onDestroyTextClassificationSession(this.mSessionId); 
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error destroying classification session.", (Throwable)remoteException);
    } 
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    paramIndentingPrintWriter.println("SystemTextClassifier:");
    paramIndentingPrintWriter.increaseIndent();
    paramIndentingPrintWriter.printPair("mFallback", this.mFallback);
    paramIndentingPrintWriter.printPair("mSessionId", this.mSessionId);
    paramIndentingPrintWriter.printPair("mSystemTcMetadata", this.mSystemTcMetadata);
    paramIndentingPrintWriter.decreaseIndent();
    paramIndentingPrintWriter.println();
  }
  
  void initializeRemoteSession(TextClassificationContext paramTextClassificationContext, TextClassificationSessionId paramTextClassificationSessionId) {
    Objects.requireNonNull(paramTextClassificationSessionId);
    this.mSessionId = paramTextClassificationSessionId;
    try {
      paramTextClassificationContext.setSystemTextClassifierMetadata(this.mSystemTcMetadata);
      this.mManagerService.onCreateTextClassificationSession(paramTextClassificationContext, this.mSessionId);
    } catch (RemoteException remoteException) {
      Log.e("androidtc", "Error starting a new classification session.", (Throwable)remoteException);
    } 
  }
  
  class BlockingCallback<T extends Parcelable> extends ITextClassifierCallback.Stub {
    private final SystemTextClassifier.ResponseReceiver<T> mReceiver = new SystemTextClassifier.ResponseReceiver<>();
    
    public void onSuccess(Bundle param1Bundle) {
      this.mReceiver.onSuccess((T)TextClassifierService.getResponse(param1Bundle));
    }
    
    public void onFailure() {
      this.mReceiver.onFailure();
    }
    
    public T get() {
      return this.mReceiver.get();
    }
    
    BlockingCallback(SystemTextClassifier this$0) {}
  }
  
  class ResponseReceiver<T> {
    private final CountDownLatch mLatch = new CountDownLatch(1);
    
    private final String mName;
    
    private T mResponse;
    
    private ResponseReceiver(SystemTextClassifier this$0) {
      this.mName = (String)this$0;
    }
    
    public void onSuccess(T param1T) {
      this.mResponse = param1T;
      this.mLatch.countDown();
    }
    
    public void onFailure() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Request failed at ");
      stringBuilder.append(this.mName);
      Log.e("androidtc", stringBuilder.toString(), null);
      this.mLatch.countDown();
    }
    
    public T get() {
      if (Looper.myLooper() != Looper.getMainLooper())
        try {
          boolean bool = this.mLatch.await(2L, TimeUnit.SECONDS);
          if (!bool) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Timeout in ResponseReceiver.get(): ");
            stringBuilder.append(this.mName);
            Log.w("androidtc", stringBuilder.toString());
          } 
        } catch (InterruptedException interruptedException) {
          Thread.currentThread().interrupt();
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Interrupted during ResponseReceiver.get(): ");
          stringBuilder.append(this.mName);
          Log.e("androidtc", stringBuilder.toString(), interruptedException);
        }  
      return this.mResponse;
    }
  }
}
