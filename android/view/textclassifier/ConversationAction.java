package android.view.textclassifier;

import android.app.RemoteAction;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class ConversationAction implements Parcelable {
  public static final Parcelable.Creator<ConversationAction> CREATOR = new Parcelable.Creator<ConversationAction>() {
      public ConversationAction createFromParcel(Parcel param1Parcel) {
        return new ConversationAction(param1Parcel);
      }
      
      public ConversationAction[] newArray(int param1Int) {
        return new ConversationAction[param1Int];
      }
    };
  
  public static final String TYPE_ADD_CONTACT = "add_contact";
  
  public static final String TYPE_CALL_PHONE = "call_phone";
  
  public static final String TYPE_COPY = "copy";
  
  public static final String TYPE_CREATE_REMINDER = "create_reminder";
  
  public static final String TYPE_OPEN_URL = "open_url";
  
  public static final String TYPE_SEND_EMAIL = "send_email";
  
  public static final String TYPE_SEND_SMS = "send_sms";
  
  public static final String TYPE_SHARE_LOCATION = "share_location";
  
  public static final String TYPE_TEXT_REPLY = "text_reply";
  
  public static final String TYPE_TRACK_FLIGHT = "track_flight";
  
  public static final String TYPE_VIEW_CALENDAR = "view_calendar";
  
  public static final String TYPE_VIEW_MAP = "view_map";
  
  private final RemoteAction mAction;
  
  private final Bundle mExtras;
  
  private final float mScore;
  
  private final CharSequence mTextReply;
  
  private final String mType;
  
  private ConversationAction(String paramString, RemoteAction paramRemoteAction, CharSequence paramCharSequence, float paramFloat, Bundle paramBundle) {
    Objects.requireNonNull(paramString);
    this.mType = paramString;
    this.mAction = paramRemoteAction;
    this.mTextReply = paramCharSequence;
    this.mScore = paramFloat;
    Objects.requireNonNull(paramBundle);
    this.mExtras = paramBundle;
  }
  
  private ConversationAction(Parcel paramParcel) {
    this.mType = paramParcel.readString();
    this.mAction = (RemoteAction)paramParcel.readParcelable(null);
    this.mTextReply = paramParcel.readCharSequence();
    this.mScore = paramParcel.readFloat();
    this.mExtras = paramParcel.readBundle();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mType);
    paramParcel.writeParcelable((Parcelable)this.mAction, paramInt);
    paramParcel.writeCharSequence(this.mTextReply);
    paramParcel.writeFloat(this.mScore);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String getType() {
    return this.mType;
  }
  
  public RemoteAction getAction() {
    return this.mAction;
  }
  
  public float getConfidenceScore() {
    return this.mScore;
  }
  
  public CharSequence getTextReply() {
    return this.mTextReply;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public Builder toBuilder() {
    Builder builder2 = new Builder(this.mType);
    CharSequence charSequence = this.mTextReply;
    Builder builder3 = builder2.setTextReply(charSequence);
    RemoteAction remoteAction = this.mAction;
    Builder builder1 = builder3.setAction(remoteAction);
    float f = this.mScore;
    builder3 = builder1.setConfidenceScore(f);
    Bundle bundle = this.mExtras;
    return builder3.setExtras(bundle);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ActionType implements Annotation {}
  
  class Builder {
    private RemoteAction mAction;
    
    private Bundle mExtras;
    
    private float mScore;
    
    private CharSequence mTextReply;
    
    private String mType;
    
    public Builder(ConversationAction this$0) {
      Objects.requireNonNull(this$0);
      this.mType = (String)this$0;
    }
    
    public Builder setAction(RemoteAction param1RemoteAction) {
      this.mAction = param1RemoteAction;
      return this;
    }
    
    public Builder setTextReply(CharSequence param1CharSequence) {
      this.mTextReply = param1CharSequence;
      return this;
    }
    
    public Builder setConfidenceScore(float param1Float) {
      this.mScore = param1Float;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public ConversationAction build() {
      String str = this.mType;
      RemoteAction remoteAction = this.mAction;
      CharSequence charSequence = this.mTextReply;
      float f = this.mScore;
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new ConversationAction(str, remoteAction, charSequence, f, bundle2);
    }
  }
}
