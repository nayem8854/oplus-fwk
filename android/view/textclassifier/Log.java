package android.view.textclassifier;

public final class Log {
  static final boolean ENABLE_FULL_LOGGING = android.util.Log.isLoggable("androidtc", 2);
  
  public static void v(String paramString1, String paramString2) {
    if (ENABLE_FULL_LOGGING)
      android.util.Log.v(paramString1, paramString2); 
  }
  
  public static void d(String paramString1, String paramString2) {
    android.util.Log.d(paramString1, paramString2);
  }
  
  public static void w(String paramString1, String paramString2) {
    android.util.Log.w(paramString1, paramString2);
  }
  
  public static void e(String paramString1, String paramString2, Throwable paramThrowable) {
    if (ENABLE_FULL_LOGGING) {
      android.util.Log.e(paramString1, paramString2, paramThrowable);
    } else {
      String str;
      if (paramThrowable != null) {
        str = paramThrowable.getClass().getSimpleName();
      } else {
        str = "??";
      } 
      android.util.Log.d(paramString1, String.format("%s (%s)", new Object[] { paramString2, str }));
    } 
  }
}
