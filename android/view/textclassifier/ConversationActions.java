package android.view.textclassifier;

import android.app.Person;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannedString;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class ConversationActions implements Parcelable {
  public static final Parcelable.Creator<ConversationActions> CREATOR = new Parcelable.Creator<ConversationActions>() {
      public ConversationActions createFromParcel(Parcel param1Parcel) {
        return new ConversationActions(param1Parcel);
      }
      
      public ConversationActions[] newArray(int param1Int) {
        return new ConversationActions[param1Int];
      }
    };
  
  private final List<ConversationAction> mConversationActions;
  
  private final String mId;
  
  public ConversationActions(List<ConversationAction> paramList, String paramString) {
    Objects.requireNonNull(paramList);
    this.mConversationActions = Collections.unmodifiableList(paramList);
    this.mId = paramString;
  }
  
  private ConversationActions(Parcel paramParcel) {
    Parcelable.Creator<ConversationAction> creator = ConversationAction.CREATOR;
    this.mConversationActions = Collections.unmodifiableList(paramParcel.createTypedArrayList(creator));
    this.mId = paramParcel.readString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.mConversationActions);
    paramParcel.writeString(this.mId);
  }
  
  public List<ConversationAction> getConversationActions() {
    return this.mConversationActions;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public static final class Message implements Parcelable {
    public static final Parcelable.Creator<Message> CREATOR;
    
    public static final Person PERSON_USER_OTHERS;
    
    public static final Person PERSON_USER_SELF;
    
    private final Person mAuthor;
    
    private final Bundle mExtras;
    
    private final ZonedDateTime mReferenceTime;
    
    private final CharSequence mText;
    
    static {
      Person.Builder builder = new Person.Builder();
      builder = builder.setKey("text-classifier-conversation-actions-user-self");
      PERSON_USER_SELF = builder.build();
      builder = new Person.Builder();
      builder = builder.setKey("text-classifier-conversation-actions-user-others");
      PERSON_USER_OTHERS = builder.build();
      CREATOR = new Parcelable.Creator<Message>() {
          public ConversationActions.Message createFromParcel(Parcel param2Parcel) {
            return new ConversationActions.Message(param2Parcel);
          }
          
          public ConversationActions.Message[] newArray(int param2Int) {
            return new ConversationActions.Message[param2Int];
          }
        };
    }
    
    private Message(Person param1Person, ZonedDateTime param1ZonedDateTime, CharSequence param1CharSequence, Bundle param1Bundle) {
      this.mAuthor = param1Person;
      this.mReferenceTime = param1ZonedDateTime;
      this.mText = param1CharSequence;
      Objects.requireNonNull(param1Bundle);
      this.mExtras = param1Bundle;
    }
    
    private Message(Parcel param1Parcel) {
      ZonedDateTime zonedDateTime;
      String str = null;
      this.mAuthor = (Person)param1Parcel.readParcelable(null);
      if (param1Parcel.readInt() != 0) {
        str = param1Parcel.readString();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;
        zonedDateTime = ZonedDateTime.parse(str, dateTimeFormatter);
      } 
      this.mReferenceTime = zonedDateTime;
      this.mText = param1Parcel.readCharSequence();
      this.mExtras = param1Parcel.readBundle();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeParcelable((Parcelable)this.mAuthor, param1Int);
      if (this.mReferenceTime != null) {
        param1Int = 1;
      } else {
        param1Int = 0;
      } 
      param1Parcel.writeInt(param1Int);
      ZonedDateTime zonedDateTime = this.mReferenceTime;
      if (zonedDateTime != null)
        param1Parcel.writeString(zonedDateTime.format(DateTimeFormatter.ISO_ZONED_DATE_TIME)); 
      param1Parcel.writeCharSequence(this.mText);
      param1Parcel.writeBundle(this.mExtras);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public Person getAuthor() {
      return this.mAuthor;
    }
    
    public ZonedDateTime getReferenceTime() {
      return this.mReferenceTime;
    }
    
    public CharSequence getText() {
      return this.mText;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    class Builder {
      private Person mAuthor;
      
      private Bundle mExtras;
      
      private ZonedDateTime mReferenceTime;
      
      private CharSequence mText;
      
      public Builder(ConversationActions.Message this$0) {
        Objects.requireNonNull(this$0);
        this.mAuthor = (Person)this$0;
      }
      
      public Builder setText(CharSequence param2CharSequence) {
        this.mText = param2CharSequence;
        return this;
      }
      
      public Builder setReferenceTime(ZonedDateTime param2ZonedDateTime) {
        this.mReferenceTime = param2ZonedDateTime;
        return this;
      }
      
      public Builder setExtras(Bundle param2Bundle) {
        this.mExtras = param2Bundle;
        return this;
      }
      
      public ConversationActions.Message build() {
        SpannedString spannedString;
        Person person = this.mAuthor;
        ZonedDateTime zonedDateTime = this.mReferenceTime;
        if (this.mText == null) {
          spannedString = null;
        } else {
          spannedString = new SpannedString(this.mText);
        } 
        Bundle bundle1 = this.mExtras, bundle2 = bundle1;
        if (bundle1 == null)
          bundle2 = Bundle.EMPTY; 
        return new ConversationActions.Message(person, zonedDateTime, spannedString, bundle2);
      }
    }
  }
  
  class null implements Parcelable.Creator<Message> {
    public ConversationActions.Message createFromParcel(Parcel param1Parcel) {
      return new ConversationActions.Message(param1Parcel);
    }
    
    public ConversationActions.Message[] newArray(int param1Int) {
      return new ConversationActions.Message[param1Int];
    }
  }
  
  class Builder {
    private Person mAuthor;
    
    private Bundle mExtras;
    
    private ZonedDateTime mReferenceTime;
    
    private CharSequence mText;
    
    public Builder(ConversationActions this$0) {
      Objects.requireNonNull(this$0);
      this.mAuthor = (Person)this$0;
    }
    
    public Builder setText(CharSequence param1CharSequence) {
      this.mText = param1CharSequence;
      return this;
    }
    
    public Builder setReferenceTime(ZonedDateTime param1ZonedDateTime) {
      this.mReferenceTime = param1ZonedDateTime;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public ConversationActions.Message build() {
      SpannedString spannedString;
      Person person = this.mAuthor;
      ZonedDateTime zonedDateTime = this.mReferenceTime;
      if (this.mText == null) {
        spannedString = null;
      } else {
        spannedString = new SpannedString(this.mText);
      } 
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new ConversationActions.Message(person, zonedDateTime, spannedString, bundle2);
    }
  }
  
  public static final class Request implements Parcelable {
    private Request(List<ConversationActions.Message> param1List, TextClassifier.EntityConfig param1EntityConfig, int param1Int, List<String> param1List1, Bundle param1Bundle) {
      Objects.requireNonNull(param1List);
      this.mConversation = param1List;
      Objects.requireNonNull(param1EntityConfig);
      this.mTypeConfig = param1EntityConfig;
      this.mMaxSuggestions = param1Int;
      this.mHints = param1List1;
      this.mExtras = param1Bundle;
    }
    
    private static Request readFromParcel(Parcel param1Parcel) {
      ArrayList<ConversationActions.Message> arrayList = new ArrayList();
      param1Parcel.readParcelableList(arrayList, null);
      TextClassifier.EntityConfig entityConfig = (TextClassifier.EntityConfig)param1Parcel.readParcelable(null);
      int i = param1Parcel.readInt();
      ArrayList<String> arrayList1 = new ArrayList();
      param1Parcel.readStringList(arrayList1);
      Bundle bundle = param1Parcel.readBundle();
      SystemTextClassifierMetadata systemTextClassifierMetadata = (SystemTextClassifierMetadata)param1Parcel.readParcelable(null);
      Request request = new Request(arrayList, entityConfig, i, arrayList1, bundle);
      request.setSystemTextClassifierMetadata(systemTextClassifierMetadata);
      return request;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeParcelableList(this.mConversation, param1Int);
      param1Parcel.writeParcelable(this.mTypeConfig, param1Int);
      param1Parcel.writeInt(this.mMaxSuggestions);
      param1Parcel.writeStringList(this.mHints);
      param1Parcel.writeBundle(this.mExtras);
      param1Parcel.writeParcelable(this.mSystemTcMetadata, param1Int);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<Request> CREATOR = new Parcelable.Creator<Request>() {
        public ConversationActions.Request createFromParcel(Parcel param2Parcel) {
          return ConversationActions.Request.readFromParcel(param2Parcel);
        }
        
        public ConversationActions.Request[] newArray(int param2Int) {
          return new ConversationActions.Request[param2Int];
        }
      };
    
    public static final String HINT_FOR_IN_APP = "in_app";
    
    public static final String HINT_FOR_NOTIFICATION = "notification";
    
    private final List<ConversationActions.Message> mConversation;
    
    private Bundle mExtras;
    
    private final List<String> mHints;
    
    private final int mMaxSuggestions;
    
    private SystemTextClassifierMetadata mSystemTcMetadata;
    
    private final TextClassifier.EntityConfig mTypeConfig;
    
    public TextClassifier.EntityConfig getTypeConfig() {
      return this.mTypeConfig;
    }
    
    public List<ConversationActions.Message> getConversation() {
      return this.mConversation;
    }
    
    public int getMaxSuggestions() {
      return this.mMaxSuggestions;
    }
    
    public List<String> getHints() {
      return this.mHints;
    }
    
    public String getCallingPackageName() {
      SystemTextClassifierMetadata systemTextClassifierMetadata = this.mSystemTcMetadata;
      if (systemTextClassifierMetadata != null) {
        String str = systemTextClassifierMetadata.getCallingPackageName();
      } else {
        systemTextClassifierMetadata = null;
      } 
      return (String)systemTextClassifierMetadata;
    }
    
    void setSystemTextClassifierMetadata(SystemTextClassifierMetadata param1SystemTextClassifierMetadata) {
      this.mSystemTcMetadata = param1SystemTextClassifierMetadata;
    }
    
    public SystemTextClassifierMetadata getSystemTextClassifierMetadata() {
      return this.mSystemTcMetadata;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    class Builder {
      private List<ConversationActions.Message> mConversation;
      
      private Bundle mExtras;
      
      private List<String> mHints;
      
      private int mMaxSuggestions = -1;
      
      private TextClassifier.EntityConfig mTypeConfig;
      
      public Builder(ConversationActions.Request this$0) {
        Objects.requireNonNull(this$0);
        this.mConversation = (List<ConversationActions.Message>)this$0;
      }
      
      public Builder setHints(List<String> param2List) {
        this.mHints = param2List;
        return this;
      }
      
      public Builder setTypeConfig(TextClassifier.EntityConfig param2EntityConfig) {
        this.mTypeConfig = param2EntityConfig;
        return this;
      }
      
      public Builder setMaxSuggestions(int param2Int) {
        if (param2Int >= -1) {
          this.mMaxSuggestions = param2Int;
          return this;
        } 
        throw new IllegalArgumentException("maxSuggestions has to be greater than or equal to -1.");
      }
      
      public Builder setExtras(Bundle param2Bundle) {
        this.mExtras = param2Bundle;
        return this;
      }
      
      public ConversationActions.Request build() {
        List<ConversationActions.Message> list1 = this.mConversation;
        List<ConversationActions.Message> list2 = Collections.unmodifiableList(list1);
        TextClassifier.EntityConfig entityConfig = this.mTypeConfig;
        if (entityConfig == null)
          entityConfig = (new TextClassifier.EntityConfig.Builder()).build(); 
        int i = this.mMaxSuggestions;
        List<String> list = this.mHints;
        if (list == null) {
          list = Collections.emptyList();
        } else {
          list = Collections.unmodifiableList(list);
        } 
        Bundle bundle1 = this.mExtras, bundle2 = bundle1;
        if (bundle1 == null)
          bundle2 = Bundle.EMPTY; 
        return new ConversationActions.Request(list2, entityConfig, i, list, bundle2);
      }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class Hint implements Annotation {}
  }
  
  class null implements Parcelable.Creator<Request> {
    public ConversationActions.Request createFromParcel(Parcel param1Parcel) {
      return ConversationActions.Request.readFromParcel(param1Parcel);
    }
    
    public ConversationActions.Request[] newArray(int param1Int) {
      return new ConversationActions.Request[param1Int];
    }
  }
  
  class Builder {
    private List<ConversationActions.Message> mConversation;
    
    private Bundle mExtras;
    
    private List<String> mHints;
    
    private int mMaxSuggestions = -1;
    
    private TextClassifier.EntityConfig mTypeConfig;
    
    public Builder(ConversationActions this$0) {
      Objects.requireNonNull(this$0);
      this.mConversation = (List<ConversationActions.Message>)this$0;
    }
    
    public Builder setHints(List<String> param1List) {
      this.mHints = param1List;
      return this;
    }
    
    public Builder setTypeConfig(TextClassifier.EntityConfig param1EntityConfig) {
      this.mTypeConfig = param1EntityConfig;
      return this;
    }
    
    public Builder setMaxSuggestions(int param1Int) {
      if (param1Int >= -1) {
        this.mMaxSuggestions = param1Int;
        return this;
      } 
      throw new IllegalArgumentException("maxSuggestions has to be greater than or equal to -1.");
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public ConversationActions.Request build() {
      List<ConversationActions.Message> list1 = this.mConversation;
      List<ConversationActions.Message> list2 = Collections.unmodifiableList(list1);
      TextClassifier.EntityConfig entityConfig = this.mTypeConfig;
      if (entityConfig == null)
        entityConfig = (new TextClassifier.EntityConfig.Builder()).build(); 
      int i = this.mMaxSuggestions;
      List<String> list = this.mHints;
      if (list == null) {
        list = Collections.emptyList();
      } else {
        list = Collections.unmodifiableList(list);
      } 
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new ConversationActions.Request(list2, entityConfig, i, list, bundle2);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Hint implements Annotation {}
}
