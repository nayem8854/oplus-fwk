package android.view.textclassifier;

import android.content.Context;
import android.os.ServiceManager;
import com.android.internal.util.IndentingPrintWriter;
import java.util.Objects;

public final class TextClassificationManager {
  private static final TextClassificationConstants sDefaultSettings = new TextClassificationConstants();
  
  private final Object mLock = new Object();
  
  private final TextClassificationSessionFactory mDefaultSessionFactory = new _$$Lambda$TextClassificationManager$JIaezIJbMig__kVzN6oArzkTsJE(this);
  
  private static final String LOG_TAG = "androidtc";
  
  private final Context mContext;
  
  private TextClassifier mCustomTextClassifier;
  
  private TextClassificationSessionFactory mSessionFactory;
  
  private TextClassificationConstants mSettings;
  
  public TextClassificationManager(Context paramContext) {
    Objects.requireNonNull(paramContext);
    this.mContext = paramContext;
    this.mSessionFactory = this.mDefaultSessionFactory;
  }
  
  public TextClassifier getTextClassifier() {
    synchronized (this.mLock) {
      if (this.mCustomTextClassifier != null)
        return this.mCustomTextClassifier; 
      if (getSettings().isSystemTextClassifierEnabled())
        return getSystemTextClassifier(1); 
      return getLocalTextClassifier();
    } 
  }
  
  public void setTextClassifier(TextClassifier paramTextClassifier) {
    synchronized (this.mLock) {
      this.mCustomTextClassifier = paramTextClassifier;
      return;
    } 
  }
  
  public TextClassifier getTextClassifier(int paramInt) {
    if (paramInt != 0)
      return getSystemTextClassifier(paramInt); 
    return getLocalTextClassifier();
  }
  
  private TextClassificationConstants getSettings() {
    synchronized (this.mLock) {
      if (this.mSettings == null) {
        TextClassificationConstants textClassificationConstants = new TextClassificationConstants();
        this();
        this.mSettings = textClassificationConstants;
      } 
      return this.mSettings;
    } 
  }
  
  public TextClassifier createTextClassificationSession(TextClassificationContext paramTextClassificationContext) {
    Objects.requireNonNull(paramTextClassificationContext);
    TextClassificationSessionFactory textClassificationSessionFactory = this.mSessionFactory;
    TextClassifier textClassifier = textClassificationSessionFactory.createTextClassificationSession(paramTextClassificationContext);
    Objects.requireNonNull(textClassifier, "Session Factory should never return null");
    return textClassifier;
  }
  
  public TextClassifier createTextClassificationSession(TextClassificationContext paramTextClassificationContext, TextClassifier paramTextClassifier) {
    Objects.requireNonNull(paramTextClassificationContext);
    Objects.requireNonNull(paramTextClassifier);
    return new TextClassificationSession(paramTextClassificationContext, paramTextClassifier);
  }
  
  public void setTextClassificationSessionFactory(TextClassificationSessionFactory paramTextClassificationSessionFactory) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 19
    //   11: aload_0
    //   12: aload_1
    //   13: putfield mSessionFactory : Landroid/view/textclassifier/TextClassificationSessionFactory;
    //   16: goto -> 27
    //   19: aload_0
    //   20: aload_0
    //   21: getfield mDefaultSessionFactory : Landroid/view/textclassifier/TextClassificationSessionFactory;
    //   24: putfield mSessionFactory : Landroid/view/textclassifier/TextClassificationSessionFactory;
    //   27: aload_2
    //   28: monitorexit
    //   29: return
    //   30: astore_1
    //   31: aload_2
    //   32: monitorexit
    //   33: aload_1
    //   34: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #172	-> 0
    //   #173	-> 7
    //   #174	-> 11
    //   #176	-> 19
    //   #178	-> 27
    //   #179	-> 29
    //   #178	-> 30
    // Exception table:
    //   from	to	target	type
    //   11	16	30	finally
    //   19	27	30	finally
    //   27	29	30	finally
    //   31	33	30	finally
  }
  
  private TextClassifier getSystemTextClassifier(int paramInt) {
    synchronized (this.mLock) {
      boolean bool = getSettings().isSystemTextClassifierEnabled();
      if (bool)
        try {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Initializing SystemTextClassifier, type = ");
          stringBuilder.append(TextClassifier.typeToString(paramInt));
          String str = stringBuilder.toString();
          Log.d("androidtc", str);
          SystemTextClassifier systemTextClassifier = new SystemTextClassifier();
          Context context = this.mContext;
          TextClassificationConstants textClassificationConstants = getSettings();
          if (paramInt == 2) {
            bool = true;
          } else {
            bool = false;
          } 
          this(context, textClassificationConstants, bool);
          return systemTextClassifier;
        } catch (android.os.ServiceManager.ServiceNotFoundException serviceNotFoundException) {
          Log.e("androidtc", "Could not initialize SystemTextClassifier", (Throwable)serviceNotFoundException);
        }  
      return TextClassifier.NO_OP;
    } 
  }
  
  private TextClassifier getLocalTextClassifier() {
    Log.d("androidtc", "Local text-classifier not supported. Returning a no-op text-classifier.");
    return TextClassifier.NO_OP;
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    getSystemTextClassifier(2).dump(paramIndentingPrintWriter);
    getSystemTextClassifier(1).dump(paramIndentingPrintWriter);
    getSettings().dump(paramIndentingPrintWriter);
  }
  
  public static TextClassificationConstants getSettings(Context paramContext) {
    Objects.requireNonNull(paramContext);
    TextClassificationManager textClassificationManager = (TextClassificationManager)paramContext.getSystemService(TextClassificationManager.class);
    if (textClassificationManager != null)
      return textClassificationManager.getSettings(); 
    return sDefaultSettings;
  }
}
