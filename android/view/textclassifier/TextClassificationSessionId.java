package android.view.textclassifier;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

public final class TextClassificationSessionId implements Parcelable {
  public TextClassificationSessionId() {
    this(UUID.randomUUID().toString(), (IBinder)new Binder());
  }
  
  public TextClassificationSessionId(String paramString, IBinder paramIBinder) {
    Objects.requireNonNull(paramString);
    this.mValue = paramString;
    Objects.requireNonNull(paramIBinder);
    this.mToken = paramIBinder;
  }
  
  public IBinder getToken() {
    return this.mToken;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!Objects.equals(this.mValue, ((TextClassificationSessionId)paramObject).mValue) || !Objects.equals(this.mToken, ((TextClassificationSessionId)paramObject).mToken))
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mValue, this.mToken });
  }
  
  public String toString() {
    return String.format(Locale.US, "TextClassificationSessionId {%s}", new Object[] { this.mValue });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mValue);
    paramParcel.writeStrongBinder(this.mToken);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String getValue() {
    return this.mValue;
  }
  
  public static final Parcelable.Creator<TextClassificationSessionId> CREATOR = new Parcelable.Creator<TextClassificationSessionId>() {
      public TextClassificationSessionId createFromParcel(Parcel param1Parcel) {
        return 
          new TextClassificationSessionId(param1Parcel.readString(), param1Parcel.readStrongBinder());
      }
      
      public TextClassificationSessionId[] newArray(int param1Int) {
        return new TextClassificationSessionId[param1Int];
      }
    };
  
  private final IBinder mToken;
  
  private final String mValue;
}
