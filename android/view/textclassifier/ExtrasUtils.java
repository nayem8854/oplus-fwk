package android.view.textclassifier;

import android.app.RemoteAction;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;

public final class ExtrasUtils {
  private static final String ACTIONS_INTENTS = "actions-intents";
  
  private static final String ACTION_INTENT = "action-intent";
  
  private static final String ENTITY_TYPE = "entity-type";
  
  private static final String FOREIGN_LANGUAGE = "foreign-language";
  
  private static final String MODEL_NAME = "model-name";
  
  private static final String SCORE = "score";
  
  public static Bundle getForeignLanguageExtra(TextClassification paramTextClassification) {
    if (paramTextClassification == null)
      return null; 
    return paramTextClassification.getExtras().getBundle("foreign-language");
  }
  
  public static Intent getActionIntent(Bundle paramBundle) {
    return (Intent)paramBundle.getParcelable("action-intent");
  }
  
  public static ArrayList<Intent> getActionsIntents(TextClassification paramTextClassification) {
    if (paramTextClassification == null)
      return null; 
    return paramTextClassification.getExtras().getParcelableArrayList("actions-intents");
  }
  
  private static RemoteAction findAction(TextClassification paramTextClassification, String paramString) {
    if (paramTextClassification == null || paramString == null)
      return null; 
    ArrayList<Intent> arrayList = getActionsIntents(paramTextClassification);
    if (arrayList != null) {
      int i = arrayList.size();
      for (byte b = 0; b < i; b++) {
        Intent intent = arrayList.get(b);
        if (intent != null && paramString.equals(intent.getAction()))
          return paramTextClassification.getActions().get(b); 
      } 
    } 
    return null;
  }
  
  public static RemoteAction findTranslateAction(TextClassification paramTextClassification) {
    return findAction(paramTextClassification, "android.intent.action.TRANSLATE");
  }
  
  public static String getEntityType(Bundle paramBundle) {
    if (paramBundle == null)
      return null; 
    return paramBundle.getString("entity-type");
  }
  
  public static float getScore(Bundle paramBundle) {
    if (paramBundle == null)
      return -1.0F; 
    return paramBundle.getFloat("score", -1.0F);
  }
  
  public static String getModelName(Bundle paramBundle) {
    if (paramBundle == null)
      return null; 
    return paramBundle.getString("model-name");
  }
}
