package android.view.textclassifier;

import com.android.internal.util.Preconditions;
import java.util.Objects;
import sun.misc.Cleaner;

final class TextClassificationSession implements TextClassifier {
  private static final String LOG_TAG = "TextClassificationSession";
  
  private final TextClassificationContext mClassificationContext;
  
  private final Cleaner mCleaner;
  
  private final TextClassifier mDelegate;
  
  private boolean mDestroyed;
  
  private final SelectionEventHelper mEventHelper;
  
  private final TextClassificationSessionId mSessionId;
  
  TextClassificationSession(TextClassificationContext paramTextClassificationContext, TextClassifier paramTextClassifier) {
    Objects.requireNonNull(paramTextClassificationContext);
    this.mClassificationContext = paramTextClassificationContext;
    Objects.requireNonNull(paramTextClassifier);
    this.mDelegate = paramTextClassifier;
    TextClassificationSessionId textClassificationSessionId = new TextClassificationSessionId();
    this.mEventHelper = new SelectionEventHelper(textClassificationSessionId, this.mClassificationContext);
    initializeRemoteSession();
    this.mCleaner = Cleaner.create(this, new CleanerRunnable(this.mEventHelper, this.mDelegate));
  }
  
  public TextSelection suggestSelection(TextSelection.Request paramRequest) {
    checkDestroyed();
    return this.mDelegate.suggestSelection(paramRequest);
  }
  
  private void initializeRemoteSession() {
    TextClassifier textClassifier = this.mDelegate;
    if (textClassifier instanceof SystemTextClassifier)
      ((SystemTextClassifier)textClassifier).initializeRemoteSession(this.mClassificationContext, this.mSessionId); 
  }
  
  public TextClassification classifyText(TextClassification.Request paramRequest) {
    checkDestroyed();
    return this.mDelegate.classifyText(paramRequest);
  }
  
  public TextLinks generateLinks(TextLinks.Request paramRequest) {
    checkDestroyed();
    return this.mDelegate.generateLinks(paramRequest);
  }
  
  public ConversationActions suggestConversationActions(ConversationActions.Request paramRequest) {
    checkDestroyed();
    return this.mDelegate.suggestConversationActions(paramRequest);
  }
  
  public TextLanguage detectLanguage(TextLanguage.Request paramRequest) {
    checkDestroyed();
    return this.mDelegate.detectLanguage(paramRequest);
  }
  
  public int getMaxGenerateLinksTextLength() {
    checkDestroyed();
    return this.mDelegate.getMaxGenerateLinksTextLength();
  }
  
  public void onSelectionEvent(SelectionEvent paramSelectionEvent) {
    try {
      if (this.mEventHelper.sanitizeEvent(paramSelectionEvent))
        this.mDelegate.onSelectionEvent(paramSelectionEvent); 
    } catch (Exception exception) {
      Log.e("TextClassificationSession", "Error reporting text classifier selection event", exception);
    } 
  }
  
  public void onTextClassifierEvent(TextClassifierEvent paramTextClassifierEvent) {
    try {
      paramTextClassifierEvent.mHiddenTempSessionId = this.mSessionId;
      this.mDelegate.onTextClassifierEvent(paramTextClassifierEvent);
    } catch (Exception exception) {
      Log.e("TextClassificationSession", "Error reporting text classifier event", exception);
    } 
  }
  
  public void destroy() {
    this.mCleaner.clean();
    this.mDestroyed = true;
  }
  
  public boolean isDestroyed() {
    return this.mDestroyed;
  }
  
  private void checkDestroyed() {
    if (!this.mDestroyed)
      return; 
    throw new IllegalStateException("This TextClassification session has been destroyed");
  }
  
  class SelectionEventHelper {
    private final TextClassificationContext mContext;
    
    private int mInvocationMethod = 0;
    
    private SelectionEvent mPrevEvent;
    
    private final TextClassificationSessionId mSessionId;
    
    private SelectionEvent mSmartEvent;
    
    private SelectionEvent mStartEvent;
    
    SelectionEventHelper(TextClassificationSession this$0, TextClassificationContext param1TextClassificationContext) {
      Objects.requireNonNull(this$0);
      this.mSessionId = (TextClassificationSessionId)this$0;
      Objects.requireNonNull(param1TextClassificationContext);
      this.mContext = param1TextClassificationContext;
    }
    
    boolean sanitizeEvent(SelectionEvent param1SelectionEvent) {
      updateInvocationMethod(param1SelectionEvent);
      modifyAutoSelectionEventType(param1SelectionEvent);
      int i = param1SelectionEvent.getEventType();
      boolean bool = false;
      if (i != 1 && this.mStartEvent == null) {
        Log.d("TextClassificationSession", "Selection session not yet started. Ignoring event");
        return false;
      } 
      long l = System.currentTimeMillis();
      i = param1SelectionEvent.getEventType();
      if (i != 1) {
        if (i != 2) {
          if (i != 3 && i != 4 && i != 5) {
            if (i == 100 || i == 107) {
              SelectionEvent selectionEvent1 = this.mPrevEvent;
              if (selectionEvent1 != null)
                param1SelectionEvent.setEntityType(selectionEvent1.getEntityType()); 
            } 
          } else {
            this.mSmartEvent = param1SelectionEvent;
          } 
        } else {
          SelectionEvent selectionEvent1 = this.mPrevEvent;
          if (selectionEvent1 != null && 
            selectionEvent1.getAbsoluteStart() == param1SelectionEvent.getAbsoluteStart()) {
            selectionEvent1 = this.mPrevEvent;
            if (selectionEvent1.getAbsoluteEnd() == param1SelectionEvent.getAbsoluteEnd())
              return false; 
          } 
        } 
      } else {
        if (param1SelectionEvent.getAbsoluteEnd() == param1SelectionEvent.getAbsoluteStart() + 1)
          bool = true; 
        Preconditions.checkArgument(bool);
        param1SelectionEvent.setSessionId(this.mSessionId);
        this.mStartEvent = param1SelectionEvent;
      } 
      param1SelectionEvent.setEventTime(l);
      SelectionEvent selectionEvent = this.mStartEvent;
      if (selectionEvent != null) {
        SelectionEvent selectionEvent1 = param1SelectionEvent.setSessionId(selectionEvent.getSessionId());
        selectionEvent = this.mStartEvent;
        selectionEvent = selectionEvent1.setDurationSinceSessionStart(l - selectionEvent.getEventTime());
        selectionEvent = selectionEvent.setStart(param1SelectionEvent.getAbsoluteStart() - this.mStartEvent.getAbsoluteStart());
        selectionEvent.setEnd(param1SelectionEvent.getAbsoluteEnd() - this.mStartEvent.getAbsoluteStart());
      } 
      selectionEvent = this.mSmartEvent;
      if (selectionEvent != null) {
        SelectionEvent selectionEvent1 = param1SelectionEvent.setResultId(selectionEvent.getResultId());
        selectionEvent = this.mSmartEvent;
        int j = selectionEvent.getAbsoluteStart();
        i = this.mStartEvent.getAbsoluteStart();
        selectionEvent = selectionEvent1.setSmartStart(j - i);
        selectionEvent1 = this.mSmartEvent;
        selectionEvent.setSmartEnd(selectionEvent1.getAbsoluteEnd() - this.mStartEvent.getAbsoluteStart());
      } 
      selectionEvent = this.mPrevEvent;
      if (selectionEvent != null) {
        selectionEvent = param1SelectionEvent.setDurationSincePreviousEvent(l - selectionEvent.getEventTime());
        SelectionEvent selectionEvent1 = this.mPrevEvent;
        selectionEvent.setEventIndex(selectionEvent1.getEventIndex() + 1);
      } 
      this.mPrevEvent = param1SelectionEvent;
      return true;
    }
    
    void endSession() {
      this.mPrevEvent = null;
      this.mSmartEvent = null;
      this.mStartEvent = null;
    }
    
    private void updateInvocationMethod(SelectionEvent param1SelectionEvent) {
      param1SelectionEvent.setTextClassificationSessionContext(this.mContext);
      if (param1SelectionEvent.getInvocationMethod() == 0) {
        param1SelectionEvent.setInvocationMethod(this.mInvocationMethod);
      } else {
        this.mInvocationMethod = param1SelectionEvent.getInvocationMethod();
      } 
    }
    
    private void modifyAutoSelectionEventType(SelectionEvent param1SelectionEvent) {
      int i = param1SelectionEvent.getEventType();
      if (i != 3 && i != 4 && i != 5)
        return; 
      String str = param1SelectionEvent.getResultId();
      if (SelectionSessionLogger.isPlatformLocalTextClassifierSmartSelection(str)) {
        if (param1SelectionEvent.getAbsoluteEnd() - param1SelectionEvent.getAbsoluteStart() > 1) {
          param1SelectionEvent.setEventType(4);
        } else {
          param1SelectionEvent.setEventType(3);
        } 
      } else {
        param1SelectionEvent.setEventType(5);
      } 
    }
  }
  
  class CleanerRunnable implements Runnable {
    private final TextClassifier mDelegate;
    
    private final TextClassificationSession.SelectionEventHelper mEventHelper;
    
    CleanerRunnable(TextClassificationSession this$0, TextClassifier param1TextClassifier) {
      Objects.requireNonNull(this$0);
      this.mEventHelper = (TextClassificationSession.SelectionEventHelper)this$0;
      Objects.requireNonNull(param1TextClassifier);
      this.mDelegate = param1TextClassifier;
    }
    
    public void run() {
      this.mEventHelper.endSession();
      this.mDelegate.destroy();
    }
  }
}
