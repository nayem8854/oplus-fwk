package android.view.textclassifier;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Locale;
import java.util.Objects;

public final class SystemTextClassifierMetadata implements Parcelable {
  public SystemTextClassifierMetadata(String paramString, int paramInt, boolean paramBoolean) {
    Objects.requireNonNull(paramString);
    this.mCallingPackageName = paramString;
    this.mUserId = paramInt;
    this.mUseDefaultTextClassifier = paramBoolean;
  }
  
  public int getUserId() {
    return this.mUserId;
  }
  
  public String getCallingPackageName() {
    return this.mCallingPackageName;
  }
  
  public boolean useDefaultTextClassifier() {
    return this.mUseDefaultTextClassifier;
  }
  
  public String toString() {
    Locale locale = Locale.US;
    String str = this.mCallingPackageName;
    int i = this.mUserId;
    boolean bool = this.mUseDefaultTextClassifier;
    return String.format(locale, "SystemTextClassifierMetadata {callingPackageName=%s, userId=%d, useDefaultTextClassifier=%b}", new Object[] { str, Integer.valueOf(i), Boolean.valueOf(bool) });
  }
  
  private static SystemTextClassifierMetadata readFromParcel(Parcel paramParcel) {
    String str = paramParcel.readString();
    int i = paramParcel.readInt();
    boolean bool = paramParcel.readBoolean();
    return new SystemTextClassifierMetadata(str, i, bool);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mCallingPackageName);
    paramParcel.writeInt(this.mUserId);
    paramParcel.writeBoolean(this.mUseDefaultTextClassifier);
  }
  
  public static final Parcelable.Creator<SystemTextClassifierMetadata> CREATOR = new Parcelable.Creator<SystemTextClassifierMetadata>() {
      public SystemTextClassifierMetadata createFromParcel(Parcel param1Parcel) {
        return SystemTextClassifierMetadata.readFromParcel(param1Parcel);
      }
      
      public SystemTextClassifierMetadata[] newArray(int param1Int) {
        return new SystemTextClassifierMetadata[param1Int];
      }
    };
  
  private final String mCallingPackageName;
  
  private final boolean mUseDefaultTextClassifier;
  
  private final int mUserId;
}
