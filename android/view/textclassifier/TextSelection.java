package android.view.textclassifier;

import android.os.Bundle;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannedString;
import android.util.ArrayMap;
import com.android.internal.util.Preconditions;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public final class TextSelection implements Parcelable {
  private TextSelection(int paramInt1, int paramInt2, Map<String, Float> paramMap, String paramString, Bundle paramBundle) {
    this.mStartIndex = paramInt1;
    this.mEndIndex = paramInt2;
    this.mEntityConfidence = new EntityConfidence(paramMap);
    this.mId = paramString;
    this.mExtras = paramBundle;
  }
  
  public int getSelectionStartIndex() {
    return this.mStartIndex;
  }
  
  public int getSelectionEndIndex() {
    return this.mEndIndex;
  }
  
  public int getEntityCount() {
    return this.mEntityConfidence.getEntities().size();
  }
  
  public String getEntity(int paramInt) {
    return this.mEntityConfidence.getEntities().get(paramInt);
  }
  
  public float getConfidenceScore(String paramString) {
    return this.mEntityConfidence.getConfidenceScore(paramString);
  }
  
  public String getId() {
    return this.mId;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public String toString() {
    Locale locale = Locale.US;
    String str = this.mId;
    int i = this.mStartIndex;
    int j = this.mEndIndex;
    EntityConfidence entityConfidence = this.mEntityConfidence;
    return String.format(locale, "TextSelection {id=%s, startIndex=%d, endIndex=%d, entities=%s}", new Object[] { str, Integer.valueOf(i), Integer.valueOf(j), entityConfidence });
  }
  
  class Builder {
    private final int mEndIndex;
    
    private final Map<String, Float> mEntityConfidence;
    
    private Bundle mExtras;
    
    private String mId;
    
    private final int mStartIndex;
    
    public Builder(TextSelection this$0, int param1Int1) {
      boolean bool2;
      this.mEntityConfidence = new ArrayMap<>();
      boolean bool1 = true;
      if (this$0 >= null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      if (param1Int1 > this$0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      this.mStartIndex = this$0;
      this.mEndIndex = param1Int1;
    }
    
    public Builder setEntityType(String param1String, float param1Float) {
      Objects.requireNonNull(param1String);
      this.mEntityConfidence.put(param1String, Float.valueOf(param1Float));
      return this;
    }
    
    public Builder setId(String param1String) {
      this.mId = param1String;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public TextSelection build() {
      int i = this.mStartIndex, j = this.mEndIndex;
      Map<String, Float> map = this.mEntityConfidence;
      String str = this.mId;
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new TextSelection(i, j, map, str, bundle2);
    }
  }
  
  class Builder {
    private boolean mDarkLaunchAllowed;
    
    private LocaleList mDefaultLocales;
    
    private final int mEndIndex;
    
    private Bundle mExtras;
    
    private final int mStartIndex;
    
    private final CharSequence mText;
    
    public Builder(TextSelection this$0, int param1Int1, int param1Int2) {
      TextClassifier.Utils.checkArgument((CharSequence)this$0, param1Int1, param1Int2);
      this.mText = (CharSequence)this$0;
      this.mStartIndex = param1Int1;
      this.mEndIndex = param1Int2;
    }
    
    public Builder setDefaultLocales(LocaleList param1LocaleList) {
      this.mDefaultLocales = param1LocaleList;
      return this;
    }
    
    public Builder setDarkLaunchAllowed(boolean param1Boolean) {
      this.mDarkLaunchAllowed = param1Boolean;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public TextSelection.Request build() {
      SpannedString spannedString = new SpannedString(this.mText);
      int i = this.mStartIndex, j = this.mEndIndex;
      LocaleList localeList = this.mDefaultLocales;
      boolean bool = this.mDarkLaunchAllowed;
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new TextSelection.Request(spannedString, i, j, localeList, bool, bundle2);
    }
  }
  
  public static final class Request implements Parcelable {
    private Request(CharSequence param1CharSequence, int param1Int1, int param1Int2, LocaleList param1LocaleList, boolean param1Boolean, Bundle param1Bundle) {
      this.mText = param1CharSequence;
      this.mStartIndex = param1Int1;
      this.mEndIndex = param1Int2;
      this.mDefaultLocales = param1LocaleList;
      this.mDarkLaunchAllowed = param1Boolean;
      this.mExtras = param1Bundle;
    }
    
    public CharSequence getText() {
      return this.mText;
    }
    
    public int getStartIndex() {
      return this.mStartIndex;
    }
    
    public int getEndIndex() {
      return this.mEndIndex;
    }
    
    public boolean isDarkLaunchAllowed() {
      return this.mDarkLaunchAllowed;
    }
    
    public LocaleList getDefaultLocales() {
      return this.mDefaultLocales;
    }
    
    public String getCallingPackageName() {
      SystemTextClassifierMetadata systemTextClassifierMetadata = this.mSystemTcMetadata;
      if (systemTextClassifierMetadata != null) {
        String str = systemTextClassifierMetadata.getCallingPackageName();
      } else {
        systemTextClassifierMetadata = null;
      } 
      return (String)systemTextClassifierMetadata;
    }
    
    public void setSystemTextClassifierMetadata(SystemTextClassifierMetadata param1SystemTextClassifierMetadata) {
      this.mSystemTcMetadata = param1SystemTextClassifierMetadata;
    }
    
    public SystemTextClassifierMetadata getSystemTextClassifierMetadata() {
      return this.mSystemTcMetadata;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    class Builder {
      private boolean mDarkLaunchAllowed;
      
      private LocaleList mDefaultLocales;
      
      private final int mEndIndex;
      
      private Bundle mExtras;
      
      private final int mStartIndex;
      
      private final CharSequence mText;
      
      public Builder(TextSelection.Request this$0, int param2Int1, int param2Int2) {
        TextClassifier.Utils.checkArgument((CharSequence)this$0, param2Int1, param2Int2);
        this.mText = (CharSequence)this$0;
        this.mStartIndex = param2Int1;
        this.mEndIndex = param2Int2;
      }
      
      public Builder setDefaultLocales(LocaleList param2LocaleList) {
        this.mDefaultLocales = param2LocaleList;
        return this;
      }
      
      public Builder setDarkLaunchAllowed(boolean param2Boolean) {
        this.mDarkLaunchAllowed = param2Boolean;
        return this;
      }
      
      public Builder setExtras(Bundle param2Bundle) {
        this.mExtras = param2Bundle;
        return this;
      }
      
      public TextSelection.Request build() {
        SpannedString spannedString = new SpannedString(this.mText);
        int i = this.mStartIndex, j = this.mEndIndex;
        LocaleList localeList = this.mDefaultLocales;
        boolean bool = this.mDarkLaunchAllowed;
        Bundle bundle1 = this.mExtras, bundle2 = bundle1;
        if (bundle1 == null)
          bundle2 = Bundle.EMPTY; 
        return new TextSelection.Request(spannedString, i, j, localeList, bool, bundle2);
      }
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeCharSequence(this.mText);
      param1Parcel.writeInt(this.mStartIndex);
      param1Parcel.writeInt(this.mEndIndex);
      param1Parcel.writeParcelable((Parcelable)this.mDefaultLocales, param1Int);
      param1Parcel.writeBundle(this.mExtras);
      param1Parcel.writeParcelable(this.mSystemTcMetadata, param1Int);
    }
    
    private static Request readFromParcel(Parcel param1Parcel) {
      CharSequence charSequence = param1Parcel.readCharSequence();
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      LocaleList localeList = (LocaleList)param1Parcel.readParcelable(null);
      Bundle bundle = param1Parcel.readBundle();
      SystemTextClassifierMetadata systemTextClassifierMetadata = (SystemTextClassifierMetadata)param1Parcel.readParcelable(null);
      Request request = new Request(charSequence, i, j, localeList, false, bundle);
      request.setSystemTextClassifierMetadata(systemTextClassifierMetadata);
      return request;
    }
    
    public static final Parcelable.Creator<Request> CREATOR = new Parcelable.Creator<Request>() {
        public TextSelection.Request createFromParcel(Parcel param2Parcel) {
          return TextSelection.Request.readFromParcel(param2Parcel);
        }
        
        public TextSelection.Request[] newArray(int param2Int) {
          return new TextSelection.Request[param2Int];
        }
      };
    
    private final boolean mDarkLaunchAllowed;
    
    private final LocaleList mDefaultLocales;
    
    private final int mEndIndex;
    
    private final Bundle mExtras;
    
    private final int mStartIndex;
    
    private SystemTextClassifierMetadata mSystemTcMetadata;
    
    private final CharSequence mText;
  }
  
  class null implements Parcelable.Creator<Request> {
    public TextSelection.Request createFromParcel(Parcel param1Parcel) {
      return TextSelection.Request.readFromParcel(param1Parcel);
    }
    
    public TextSelection.Request[] newArray(int param1Int) {
      return new TextSelection.Request[param1Int];
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mStartIndex);
    paramParcel.writeInt(this.mEndIndex);
    this.mEntityConfidence.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.mId);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public static final Parcelable.Creator<TextSelection> CREATOR = new Parcelable.Creator<TextSelection>() {
      public TextSelection createFromParcel(Parcel param1Parcel) {
        return new TextSelection(param1Parcel);
      }
      
      public TextSelection[] newArray(int param1Int) {
        return new TextSelection[param1Int];
      }
    };
  
  private final int mEndIndex;
  
  private final EntityConfidence mEntityConfidence;
  
  private final Bundle mExtras;
  
  private final String mId;
  
  private final int mStartIndex;
  
  private TextSelection(Parcel paramParcel) {
    this.mStartIndex = paramParcel.readInt();
    this.mEndIndex = paramParcel.readInt();
    this.mEntityConfidence = (EntityConfidence)EntityConfidence.CREATOR.createFromParcel(paramParcel);
    this.mId = paramParcel.readString();
    this.mExtras = paramParcel.readBundle();
  }
}
