package android.view.textclassifier;

import android.content.Context;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Spannable;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

public final class TextLinks implements Parcelable {
  public static final int APPLY_STRATEGY_IGNORE = 0;
  
  public static final int APPLY_STRATEGY_REPLACE = 1;
  
  private TextLinks(String paramString, ArrayList<TextLink> paramArrayList, Bundle paramBundle) {
    this.mFullText = paramString;
    this.mLinks = Collections.unmodifiableList(paramArrayList);
    this.mExtras = paramBundle;
  }
  
  public CharSequence getText() {
    return this.mFullText;
  }
  
  public Collection<TextLink> getLinks() {
    return this.mLinks;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public int apply(Spannable paramSpannable, int paramInt, Function<TextLink, TextLinkSpan> paramFunction) {
    Objects.requireNonNull(paramSpannable);
    TextLinksParams.Builder builder2 = new TextLinksParams.Builder();
    builder2 = builder2.setApplyStrategy(paramInt);
    TextLinksParams.Builder builder1 = builder2.setSpanFactory(paramFunction);
    TextLinksParams textLinksParams = builder1.build();
    return textLinksParams.apply(paramSpannable, this);
  }
  
  public String toString() {
    return String.format(Locale.US, "TextLinks{fullText=%s, links=%s}", new Object[] { this.mFullText, this.mLinks });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mFullText);
    paramParcel.writeTypedList(this.mLinks);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public static final Parcelable.Creator<TextLinks> CREATOR = new Parcelable.Creator<TextLinks>() {
      public TextLinks createFromParcel(Parcel param1Parcel) {
        return new TextLinks(param1Parcel);
      }
      
      public TextLinks[] newArray(int param1Int) {
        return new TextLinks[param1Int];
      }
    };
  
  public static final int STATUS_DIFFERENT_TEXT = 3;
  
  public static final int STATUS_LINKS_APPLIED = 0;
  
  public static final int STATUS_NO_LINKS_APPLIED = 2;
  
  public static final int STATUS_NO_LINKS_FOUND = 1;
  
  public static final int STATUS_UNSUPPORTED_CHARACTER = 4;
  
  private final Bundle mExtras;
  
  private final String mFullText;
  
  private final List<TextLink> mLinks;
  
  private TextLinks(Parcel paramParcel) {
    this.mFullText = paramParcel.readString();
    this.mLinks = paramParcel.createTypedArrayList(TextLink.CREATOR);
    this.mExtras = paramParcel.readBundle();
  }
  
  public static final class TextLink implements Parcelable {
    private TextLink(int param1Int1, int param1Int2, EntityConfidence param1EntityConfidence, Bundle param1Bundle, URLSpan param1URLSpan) {
      Objects.requireNonNull(param1EntityConfidence);
      boolean bool = param1EntityConfidence.getEntities().isEmpty();
      boolean bool1 = true;
      Preconditions.checkArgument(bool ^ true);
      if (param1Int1 > param1Int2)
        bool1 = false; 
      Preconditions.checkArgument(bool1);
      Objects.requireNonNull(param1Bundle);
      this.mStart = param1Int1;
      this.mEnd = param1Int2;
      this.mEntityScores = param1EntityConfidence;
      this.mUrlSpan = param1URLSpan;
      this.mExtras = param1Bundle;
    }
    
    public int getStart() {
      return this.mStart;
    }
    
    public int getEnd() {
      return this.mEnd;
    }
    
    public int getEntityCount() {
      return this.mEntityScores.getEntities().size();
    }
    
    public String getEntity(int param1Int) {
      return this.mEntityScores.getEntities().get(param1Int);
    }
    
    public float getConfidenceScore(String param1String) {
      return this.mEntityScores.getConfidenceScore(param1String);
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    public String toString() {
      Locale locale = Locale.US;
      int i = this.mStart;
      int j = this.mEnd;
      EntityConfidence entityConfidence = this.mEntityScores;
      URLSpan uRLSpan = this.mUrlSpan;
      return String.format(locale, "TextLink{start=%s, end=%s, entityScores=%s, urlSpan=%s}", new Object[] { Integer.valueOf(i), Integer.valueOf(j), entityConfidence, uRLSpan });
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      this.mEntityScores.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.mStart);
      param1Parcel.writeInt(this.mEnd);
      param1Parcel.writeBundle(this.mExtras);
    }
    
    private static TextLink readFromParcel(Parcel param1Parcel) {
      EntityConfidence entityConfidence = (EntityConfidence)EntityConfidence.CREATOR.createFromParcel(param1Parcel);
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      Bundle bundle = param1Parcel.readBundle();
      return new TextLink(i, j, entityConfidence, bundle, null);
    }
    
    public static final Parcelable.Creator<TextLink> CREATOR = new Parcelable.Creator<TextLink>() {
        public TextLinks.TextLink createFromParcel(Parcel param2Parcel) {
          return TextLinks.TextLink.readFromParcel(param2Parcel);
        }
        
        public TextLinks.TextLink[] newArray(int param2Int) {
          return new TextLinks.TextLink[param2Int];
        }
      };
    
    private final int mEnd;
    
    private final EntityConfidence mEntityScores;
    
    private final Bundle mExtras;
    
    private final int mStart;
    
    private final URLSpan mUrlSpan;
  }
  
  class null implements Parcelable.Creator<TextLink> {
    public TextLinks.TextLink createFromParcel(Parcel param1Parcel) {
      return TextLinks.TextLink.readFromParcel(param1Parcel);
    }
    
    public TextLinks.TextLink[] newArray(int param1Int) {
      return new TextLinks.TextLink[param1Int];
    }
  }
  
  class Builder {
    private LocaleList mDefaultLocales;
    
    private TextClassifier.EntityConfig mEntityConfig;
    
    private Bundle mExtras;
    
    private boolean mLegacyFallback = true;
    
    private ZonedDateTime mReferenceTime;
    
    private final CharSequence mText;
    
    public Builder(TextLinks this$0) {
      Objects.requireNonNull(this$0);
      this.mText = (CharSequence)this$0;
    }
    
    public Builder setDefaultLocales(LocaleList param1LocaleList) {
      this.mDefaultLocales = param1LocaleList;
      return this;
    }
    
    public Builder setEntityConfig(TextClassifier.EntityConfig param1EntityConfig) {
      this.mEntityConfig = param1EntityConfig;
      return this;
    }
    
    public Builder setLegacyFallback(boolean param1Boolean) {
      this.mLegacyFallback = param1Boolean;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public Builder setReferenceTime(ZonedDateTime param1ZonedDateTime) {
      this.mReferenceTime = param1ZonedDateTime;
      return this;
    }
    
    public TextLinks.Request build() {
      CharSequence charSequence = this.mText;
      LocaleList localeList = this.mDefaultLocales;
      TextClassifier.EntityConfig entityConfig = this.mEntityConfig;
      boolean bool = this.mLegacyFallback;
      ZonedDateTime zonedDateTime = this.mReferenceTime;
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new TextLinks.Request(charSequence, localeList, entityConfig, bool, zonedDateTime, bundle2);
    }
  }
  
  public static final class Request implements Parcelable {
    private Request(CharSequence param1CharSequence, LocaleList param1LocaleList, TextClassifier.EntityConfig param1EntityConfig, boolean param1Boolean, ZonedDateTime param1ZonedDateTime, Bundle param1Bundle) {
      this.mText = param1CharSequence;
      this.mDefaultLocales = param1LocaleList;
      this.mEntityConfig = param1EntityConfig;
      this.mLegacyFallback = param1Boolean;
      this.mReferenceTime = param1ZonedDateTime;
      this.mExtras = param1Bundle;
    }
    
    public CharSequence getText() {
      return this.mText;
    }
    
    public LocaleList getDefaultLocales() {
      return this.mDefaultLocales;
    }
    
    public TextClassifier.EntityConfig getEntityConfig() {
      return this.mEntityConfig;
    }
    
    public boolean isLegacyFallback() {
      return this.mLegacyFallback;
    }
    
    public ZonedDateTime getReferenceTime() {
      return this.mReferenceTime;
    }
    
    public String getCallingPackageName() {
      SystemTextClassifierMetadata systemTextClassifierMetadata = this.mSystemTcMetadata;
      if (systemTextClassifierMetadata != null) {
        String str = systemTextClassifierMetadata.getCallingPackageName();
      } else {
        systemTextClassifierMetadata = null;
      } 
      return (String)systemTextClassifierMetadata;
    }
    
    public void setSystemTextClassifierMetadata(SystemTextClassifierMetadata param1SystemTextClassifierMetadata) {
      this.mSystemTcMetadata = param1SystemTextClassifierMetadata;
    }
    
    public SystemTextClassifierMetadata getSystemTextClassifierMetadata() {
      return this.mSystemTcMetadata;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    class Builder {
      private LocaleList mDefaultLocales;
      
      private TextClassifier.EntityConfig mEntityConfig;
      
      private Bundle mExtras;
      
      private boolean mLegacyFallback = true;
      
      private ZonedDateTime mReferenceTime;
      
      private final CharSequence mText;
      
      public Builder(TextLinks.Request this$0) {
        Objects.requireNonNull(this$0);
        this.mText = (CharSequence)this$0;
      }
      
      public Builder setDefaultLocales(LocaleList param2LocaleList) {
        this.mDefaultLocales = param2LocaleList;
        return this;
      }
      
      public Builder setEntityConfig(TextClassifier.EntityConfig param2EntityConfig) {
        this.mEntityConfig = param2EntityConfig;
        return this;
      }
      
      public Builder setLegacyFallback(boolean param2Boolean) {
        this.mLegacyFallback = param2Boolean;
        return this;
      }
      
      public Builder setExtras(Bundle param2Bundle) {
        this.mExtras = param2Bundle;
        return this;
      }
      
      public Builder setReferenceTime(ZonedDateTime param2ZonedDateTime) {
        this.mReferenceTime = param2ZonedDateTime;
        return this;
      }
      
      public TextLinks.Request build() {
        CharSequence charSequence = this.mText;
        LocaleList localeList = this.mDefaultLocales;
        TextClassifier.EntityConfig entityConfig = this.mEntityConfig;
        boolean bool = this.mLegacyFallback;
        ZonedDateTime zonedDateTime = this.mReferenceTime;
        Bundle bundle1 = this.mExtras, bundle2 = bundle1;
        if (bundle1 == null)
          bundle2 = Bundle.EMPTY; 
        return new TextLinks.Request(charSequence, localeList, entityConfig, bool, zonedDateTime, bundle2);
      }
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      String str;
      param1Parcel.writeString(this.mText.toString());
      param1Parcel.writeParcelable((Parcelable)this.mDefaultLocales, param1Int);
      param1Parcel.writeParcelable(this.mEntityConfig, param1Int);
      param1Parcel.writeBundle(this.mExtras);
      ZonedDateTime zonedDateTime = this.mReferenceTime;
      if (zonedDateTime == null) {
        zonedDateTime = null;
      } else {
        str = zonedDateTime.toString();
      } 
      param1Parcel.writeString(str);
      param1Parcel.writeParcelable(this.mSystemTcMetadata, param1Int);
    }
    
    private static Request readFromParcel(Parcel param1Parcel) {
      ZonedDateTime zonedDateTime;
      String str1 = param1Parcel.readString();
      LocaleList localeList = (LocaleList)param1Parcel.readParcelable(null);
      TextClassifier.EntityConfig entityConfig = (TextClassifier.EntityConfig)param1Parcel.readParcelable(null);
      Bundle bundle = param1Parcel.readBundle();
      String str2 = param1Parcel.readString();
      if (str2 == null) {
        str2 = null;
      } else {
        zonedDateTime = ZonedDateTime.parse(str2);
      } 
      SystemTextClassifierMetadata systemTextClassifierMetadata = (SystemTextClassifierMetadata)param1Parcel.readParcelable(null);
      Request request = new Request(str1, localeList, entityConfig, true, zonedDateTime, bundle);
      request.setSystemTextClassifierMetadata(systemTextClassifierMetadata);
      return request;
    }
    
    public static final Parcelable.Creator<Request> CREATOR = new Parcelable.Creator<Request>() {
        public TextLinks.Request createFromParcel(Parcel param2Parcel) {
          return TextLinks.Request.readFromParcel(param2Parcel);
        }
        
        public TextLinks.Request[] newArray(int param2Int) {
          return new TextLinks.Request[param2Int];
        }
      };
    
    private final LocaleList mDefaultLocales;
    
    private final TextClassifier.EntityConfig mEntityConfig;
    
    private final Bundle mExtras;
    
    private final boolean mLegacyFallback;
    
    private final ZonedDateTime mReferenceTime;
    
    private SystemTextClassifierMetadata mSystemTcMetadata;
    
    private final CharSequence mText;
  }
  
  class null implements Parcelable.Creator<Request> {
    public TextLinks.Request createFromParcel(Parcel param1Parcel) {
      return TextLinks.Request.readFromParcel(param1Parcel);
    }
    
    public TextLinks.Request[] newArray(int param1Int) {
      return new TextLinks.Request[param1Int];
    }
  }
  
  class TextLinkSpan extends ClickableSpan {
    public static final int INVOCATION_METHOD_KEYBOARD = 1;
    
    public static final int INVOCATION_METHOD_TOUCH = 0;
    
    public static final int INVOCATION_METHOD_UNSPECIFIED = -1;
    
    private final TextLinks.TextLink mTextLink;
    
    public TextLinkSpan(TextLinks this$0) {
      this.mTextLink = (TextLinks.TextLink)this$0;
    }
    
    public void onClick(View param1View) {
      onClick(param1View, -1);
    }
    
    public final void onClick(View param1View, int param1Int) {
      if (param1View instanceof android.widget.TextView) {
        param1View = param1View;
        Context context = param1View.getContext();
        if (TextClassificationManager.getSettings(context).isSmartLinkifyEnabled()) {
          if (param1Int != 0) {
            param1View.handleClick(this);
          } else {
            param1View.requestActionMode(this);
          } 
        } else if (this.mTextLink.mUrlSpan != null) {
          this.mTextLink.mUrlSpan.onClick(param1View);
        } else {
          param1View.handleClick(this);
        } 
      } 
    }
    
    public final TextLinks.TextLink getTextLink() {
      return this.mTextLink;
    }
    
    public final String getUrl() {
      if (this.mTextLink.mUrlSpan != null)
        return this.mTextLink.mUrlSpan.getURL(); 
      return null;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class InvocationMethod implements Annotation {}
  }
  
  class Builder {
    private Bundle mExtras;
    
    private final String mFullText;
    
    private final ArrayList<TextLinks.TextLink> mLinks;
    
    public Builder(TextLinks this$0) {
      Objects.requireNonNull(this$0);
      this.mFullText = (String)this$0;
      this.mLinks = new ArrayList<>();
    }
    
    public Builder addLink(int param1Int1, int param1Int2, Map<String, Float> param1Map) {
      return addLink(param1Int1, param1Int2, param1Map, Bundle.EMPTY, null);
    }
    
    public Builder addLink(int param1Int1, int param1Int2, Map<String, Float> param1Map, Bundle param1Bundle) {
      return addLink(param1Int1, param1Int2, param1Map, param1Bundle, null);
    }
    
    Builder addLink(int param1Int1, int param1Int2, Map<String, Float> param1Map, URLSpan param1URLSpan) {
      return addLink(param1Int1, param1Int2, param1Map, Bundle.EMPTY, param1URLSpan);
    }
    
    private Builder addLink(int param1Int1, int param1Int2, Map<String, Float> param1Map, Bundle param1Bundle, URLSpan param1URLSpan) {
      this.mLinks.add(new TextLinks.TextLink(param1Int1, param1Int2, new EntityConfidence(param1Map), param1Bundle, param1URLSpan));
      return this;
    }
    
    public Builder clearTextLinks() {
      this.mLinks.clear();
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public TextLinks build() {
      String str = this.mFullText;
      ArrayList<TextLinks.TextLink> arrayList = this.mLinks;
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new TextLinks(str, arrayList, bundle2);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ApplyStrategy implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Status implements Annotation {}
}
