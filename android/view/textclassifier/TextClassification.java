package android.view.textclassifier;

import android.app.PendingIntent;
import android.app.RemoteAction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannedString;
import android.util.ArrayMap;
import android.view.View;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public final class TextClassification implements Parcelable {
  public static final Parcelable.Creator<TextClassification> CREATOR;
  
  public static final TextClassification EMPTY = (new Builder()).build();
  
  private static final String LOG_TAG = "TextClassification";
  
  private static final int MAX_LEGACY_ICON_SIZE = 192;
  
  private final List<RemoteAction> mActions;
  
  private final EntityConfidence mEntityConfidence;
  
  private final Bundle mExtras;
  
  private final String mId;
  
  private final Drawable mLegacyIcon;
  
  private final Intent mLegacyIntent;
  
  private final String mLegacyLabel;
  
  private final View.OnClickListener mLegacyOnClickListener;
  
  private final String mText;
  
  private TextClassification(String paramString1, Drawable paramDrawable, String paramString2, Intent paramIntent, View.OnClickListener paramOnClickListener, List<RemoteAction> paramList, EntityConfidence paramEntityConfidence, String paramString3, Bundle paramBundle) {
    this.mText = paramString1;
    this.mLegacyIcon = paramDrawable;
    this.mLegacyLabel = paramString2;
    this.mLegacyIntent = paramIntent;
    this.mLegacyOnClickListener = paramOnClickListener;
    this.mActions = Collections.unmodifiableList(paramList);
    Objects.requireNonNull(paramEntityConfidence);
    this.mEntityConfidence = paramEntityConfidence;
    this.mId = paramString3;
    this.mExtras = paramBundle;
  }
  
  public String getText() {
    return this.mText;
  }
  
  public int getEntityCount() {
    return this.mEntityConfidence.getEntities().size();
  }
  
  public String getEntity(int paramInt) {
    return this.mEntityConfidence.getEntities().get(paramInt);
  }
  
  public float getConfidenceScore(String paramString) {
    return this.mEntityConfidence.getConfidenceScore(paramString);
  }
  
  public List<RemoteAction> getActions() {
    return this.mActions;
  }
  
  @Deprecated
  public Drawable getIcon() {
    return this.mLegacyIcon;
  }
  
  @Deprecated
  public CharSequence getLabel() {
    return this.mLegacyLabel;
  }
  
  @Deprecated
  public Intent getIntent() {
    return this.mLegacyIntent;
  }
  
  public View.OnClickListener getOnClickListener() {
    return this.mLegacyOnClickListener;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public Builder toBuilder() {
    Builder builder2 = new Builder();
    String str2 = this.mId;
    builder2 = builder2.setId(str2);
    str2 = this.mText;
    builder2 = builder2.setText(str2);
    List<RemoteAction> list = this.mActions;
    builder2 = builder2.addActions(list);
    EntityConfidence entityConfidence = this.mEntityConfidence;
    builder2 = builder2.setEntityConfidence(entityConfidence);
    Drawable drawable = this.mLegacyIcon;
    Builder builder4 = builder2.setIcon(drawable);
    String str1 = this.mLegacyLabel;
    builder4 = builder4.setLabel(str1);
    Intent intent = this.mLegacyIntent;
    Builder builder1 = builder4.setIntent(intent);
    View.OnClickListener onClickListener = this.mLegacyOnClickListener;
    Builder builder3 = builder1.setOnClickListener(onClickListener);
    Bundle bundle = this.mExtras;
    return builder3.setExtras(bundle);
  }
  
  public String toString() {
    return String.format(Locale.US, "TextClassification {text=%s, entities=%s, actions=%s, id=%s, extras=%s}", new Object[] { this.mText, this.mEntityConfidence, this.mActions, this.mId, this.mExtras });
  }
  
  public static View.OnClickListener createIntentOnClickListener(PendingIntent paramPendingIntent) {
    Objects.requireNonNull(paramPendingIntent);
    return new _$$Lambda$TextClassification$ysasaE5ZkXkkzjVWIJ06GTV92_g(paramPendingIntent);
  }
  
  public static PendingIntent createPendingIntent(Context paramContext, Intent paramIntent, int paramInt) {
    return PendingIntent.getActivity(paramContext, paramInt, paramIntent, 134217728);
  }
  
  class Builder {
    private final List<RemoteAction> mActions = new ArrayList<>();
    
    private Bundle mExtras;
    
    private String mId;
    
    private Drawable mLegacyIcon;
    
    private Intent mLegacyIntent;
    
    private String mLegacyLabel;
    
    private View.OnClickListener mLegacyOnClickListener;
    
    private String mText;
    
    private final Map<String, Float> mTypeScoreMap = new ArrayMap<>();
    
    public Builder setText(String param1String) {
      this.mText = param1String;
      return this;
    }
    
    public Builder setEntityType(String param1String, float param1Float) {
      this.mTypeScoreMap.put(param1String, Float.valueOf(param1Float));
      return this;
    }
    
    Builder setEntityConfidence(EntityConfidence param1EntityConfidence) {
      this.mTypeScoreMap.clear();
      this.mTypeScoreMap.putAll(param1EntityConfidence.toMap());
      return this;
    }
    
    public Builder clearEntityTypes() {
      this.mTypeScoreMap.clear();
      return this;
    }
    
    public Builder addAction(RemoteAction param1RemoteAction) {
      boolean bool;
      if (param1RemoteAction != null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      this.mActions.add(param1RemoteAction);
      return this;
    }
    
    public Builder addActions(Collection<RemoteAction> param1Collection) {
      Objects.requireNonNull(param1Collection);
      this.mActions.addAll(param1Collection);
      return this;
    }
    
    public Builder clearActions() {
      this.mActions.clear();
      return this;
    }
    
    @Deprecated
    public Builder setIcon(Drawable param1Drawable) {
      this.mLegacyIcon = param1Drawable;
      return this;
    }
    
    @Deprecated
    public Builder setLabel(String param1String) {
      this.mLegacyLabel = param1String;
      return this;
    }
    
    @Deprecated
    public Builder setIntent(Intent param1Intent) {
      this.mLegacyIntent = param1Intent;
      return this;
    }
    
    @Deprecated
    public Builder setOnClickListener(View.OnClickListener param1OnClickListener) {
      this.mLegacyOnClickListener = param1OnClickListener;
      return this;
    }
    
    public Builder setId(String param1String) {
      this.mId = param1String;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public TextClassification build() {
      EntityConfidence entityConfidence = new EntityConfidence(this.mTypeScoreMap);
      String str1 = this.mText;
      Drawable drawable = this.mLegacyIcon;
      String str2 = this.mLegacyLabel;
      Intent intent = this.mLegacyIntent;
      View.OnClickListener onClickListener = this.mLegacyOnClickListener;
      List<RemoteAction> list = this.mActions;
      String str3 = this.mId;
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new TextClassification(str1, drawable, str2, intent, onClickListener, list, entityConfidence, str3, bundle2);
    }
  }
  
  class Builder {
    private LocaleList mDefaultLocales;
    
    private final int mEndIndex;
    
    private Bundle mExtras;
    
    private ZonedDateTime mReferenceTime;
    
    private final int mStartIndex;
    
    private final CharSequence mText;
    
    public Builder(TextClassification this$0, int param1Int1, int param1Int2) {
      TextClassifier.Utils.checkArgument((CharSequence)this$0, param1Int1, param1Int2);
      this.mText = (CharSequence)this$0;
      this.mStartIndex = param1Int1;
      this.mEndIndex = param1Int2;
    }
    
    public Builder setDefaultLocales(LocaleList param1LocaleList) {
      this.mDefaultLocales = param1LocaleList;
      return this;
    }
    
    public Builder setReferenceTime(ZonedDateTime param1ZonedDateTime) {
      this.mReferenceTime = param1ZonedDateTime;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public TextClassification.Request build() {
      SpannedString spannedString = new SpannedString(this.mText);
      int i = this.mStartIndex, j = this.mEndIndex;
      LocaleList localeList = this.mDefaultLocales;
      ZonedDateTime zonedDateTime = this.mReferenceTime;
      Bundle bundle1 = this.mExtras, bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = Bundle.EMPTY; 
      return new TextClassification.Request(spannedString, i, j, localeList, zonedDateTime, bundle2);
    }
  }
  
  public static final class Request implements Parcelable {
    private Request(CharSequence param1CharSequence, int param1Int1, int param1Int2, LocaleList param1LocaleList, ZonedDateTime param1ZonedDateTime, Bundle param1Bundle) {
      this.mText = param1CharSequence;
      this.mStartIndex = param1Int1;
      this.mEndIndex = param1Int2;
      this.mDefaultLocales = param1LocaleList;
      this.mReferenceTime = param1ZonedDateTime;
      this.mExtras = param1Bundle;
    }
    
    public CharSequence getText() {
      return this.mText;
    }
    
    public int getStartIndex() {
      return this.mStartIndex;
    }
    
    public int getEndIndex() {
      return this.mEndIndex;
    }
    
    public LocaleList getDefaultLocales() {
      return this.mDefaultLocales;
    }
    
    public ZonedDateTime getReferenceTime() {
      return this.mReferenceTime;
    }
    
    public String getCallingPackageName() {
      SystemTextClassifierMetadata systemTextClassifierMetadata = this.mSystemTcMetadata;
      if (systemTextClassifierMetadata != null) {
        String str = systemTextClassifierMetadata.getCallingPackageName();
      } else {
        systemTextClassifierMetadata = null;
      } 
      return (String)systemTextClassifierMetadata;
    }
    
    public void setSystemTextClassifierMetadata(SystemTextClassifierMetadata param1SystemTextClassifierMetadata) {
      this.mSystemTcMetadata = param1SystemTextClassifierMetadata;
    }
    
    public SystemTextClassifierMetadata getSystemTextClassifierMetadata() {
      return this.mSystemTcMetadata;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    class Builder {
      private LocaleList mDefaultLocales;
      
      private final int mEndIndex;
      
      private Bundle mExtras;
      
      private ZonedDateTime mReferenceTime;
      
      private final int mStartIndex;
      
      private final CharSequence mText;
      
      public Builder(TextClassification.Request this$0, int param2Int1, int param2Int2) {
        TextClassifier.Utils.checkArgument((CharSequence)this$0, param2Int1, param2Int2);
        this.mText = (CharSequence)this$0;
        this.mStartIndex = param2Int1;
        this.mEndIndex = param2Int2;
      }
      
      public Builder setDefaultLocales(LocaleList param2LocaleList) {
        this.mDefaultLocales = param2LocaleList;
        return this;
      }
      
      public Builder setReferenceTime(ZonedDateTime param2ZonedDateTime) {
        this.mReferenceTime = param2ZonedDateTime;
        return this;
      }
      
      public Builder setExtras(Bundle param2Bundle) {
        this.mExtras = param2Bundle;
        return this;
      }
      
      public TextClassification.Request build() {
        SpannedString spannedString = new SpannedString(this.mText);
        int i = this.mStartIndex, j = this.mEndIndex;
        LocaleList localeList = this.mDefaultLocales;
        ZonedDateTime zonedDateTime = this.mReferenceTime;
        Bundle bundle1 = this.mExtras, bundle2 = bundle1;
        if (bundle1 == null)
          bundle2 = Bundle.EMPTY; 
        return new TextClassification.Request(spannedString, i, j, localeList, zonedDateTime, bundle2);
      }
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      String str;
      param1Parcel.writeCharSequence(this.mText);
      param1Parcel.writeInt(this.mStartIndex);
      param1Parcel.writeInt(this.mEndIndex);
      param1Parcel.writeParcelable((Parcelable)this.mDefaultLocales, param1Int);
      ZonedDateTime zonedDateTime = this.mReferenceTime;
      if (zonedDateTime == null) {
        zonedDateTime = null;
      } else {
        str = zonedDateTime.toString();
      } 
      param1Parcel.writeString(str);
      param1Parcel.writeBundle(this.mExtras);
      param1Parcel.writeParcelable(this.mSystemTcMetadata, param1Int);
    }
    
    private static Request readFromParcel(Parcel param1Parcel) {
      ZonedDateTime zonedDateTime;
      CharSequence charSequence = param1Parcel.readCharSequence();
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      LocaleList localeList = (LocaleList)param1Parcel.readParcelable(null);
      String str = param1Parcel.readString();
      if (str == null) {
        str = null;
      } else {
        zonedDateTime = ZonedDateTime.parse(str);
      } 
      Bundle bundle = param1Parcel.readBundle();
      SystemTextClassifierMetadata systemTextClassifierMetadata = (SystemTextClassifierMetadata)param1Parcel.readParcelable(null);
      Request request = new Request(charSequence, i, j, localeList, zonedDateTime, bundle);
      request.setSystemTextClassifierMetadata(systemTextClassifierMetadata);
      return request;
    }
    
    public static final Parcelable.Creator<Request> CREATOR = new Parcelable.Creator<Request>() {
        public TextClassification.Request createFromParcel(Parcel param2Parcel) {
          return TextClassification.Request.readFromParcel(param2Parcel);
        }
        
        public TextClassification.Request[] newArray(int param2Int) {
          return new TextClassification.Request[param2Int];
        }
      };
    
    private final LocaleList mDefaultLocales;
    
    private final int mEndIndex;
    
    private final Bundle mExtras;
    
    private final ZonedDateTime mReferenceTime;
    
    private final int mStartIndex;
    
    private SystemTextClassifierMetadata mSystemTcMetadata;
    
    private final CharSequence mText;
  }
  
  class null implements Parcelable.Creator<Request> {
    public TextClassification.Request createFromParcel(Parcel param1Parcel) {
      return TextClassification.Request.readFromParcel(param1Parcel);
    }
    
    public TextClassification.Request[] newArray(int param1Int) {
      return new TextClassification.Request[param1Int];
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mText);
    paramParcel.writeTypedList(this.mActions);
    this.mEntityConfidence.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.mId);
    paramParcel.writeBundle(this.mExtras);
  }
  
  static {
    CREATOR = new Parcelable.Creator<TextClassification>() {
        public TextClassification createFromParcel(Parcel param1Parcel) {
          return new TextClassification(param1Parcel);
        }
        
        public TextClassification[] newArray(int param1Int) {
          return new TextClassification[param1Int];
        }
      };
  }
  
  private TextClassification(Parcel paramParcel) {
    this.mText = paramParcel.readString();
    ArrayList<RemoteAction> arrayList = paramParcel.createTypedArrayList(RemoteAction.CREATOR);
    if (!arrayList.isEmpty()) {
      RemoteAction remoteAction = this.mActions.get(0);
      this.mLegacyIcon = maybeLoadDrawable(remoteAction.getIcon());
      this.mLegacyLabel = remoteAction.getTitle().toString();
      this.mLegacyOnClickListener = createIntentOnClickListener(((RemoteAction)this.mActions.get(0)).getActionIntent());
    } else {
      this.mLegacyIcon = null;
      this.mLegacyLabel = null;
      this.mLegacyOnClickListener = null;
    } 
    this.mLegacyIntent = null;
    this.mEntityConfidence = (EntityConfidence)EntityConfidence.CREATOR.createFromParcel(paramParcel);
    this.mId = paramParcel.readString();
    this.mExtras = paramParcel.readBundle();
  }
  
  private static Drawable maybeLoadDrawable(Icon paramIcon) {
    if (paramIcon == null)
      return null; 
    int i = paramIcon.getType();
    if (i != 1) {
      if (i != 3) {
        if (i != 5)
          return null; 
        return (Drawable)new AdaptiveIconDrawable(null, (Drawable)new BitmapDrawable(Resources.getSystem(), paramIcon.getBitmap()));
      } 
      Resources resources = Resources.getSystem();
      byte[] arrayOfByte = paramIcon.getDataBytes();
      int j = paramIcon.getDataOffset();
      i = paramIcon.getDataLength();
      return (Drawable)new BitmapDrawable(resources, BitmapFactory.decodeByteArray(arrayOfByte, j, i));
    } 
    return (Drawable)new BitmapDrawable(Resources.getSystem(), paramIcon.getBitmap());
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class IntentType implements Annotation {
    public static final int ACTIVITY = 0;
    
    public static final int SERVICE = 1;
    
    public static final int UNSUPPORTED = -1;
  }
}
