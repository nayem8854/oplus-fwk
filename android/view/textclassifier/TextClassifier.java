package android.view.textclassifier;

import android.os.LocaleList;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableString;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.ArrayMap;
import com.android.internal.util.IndentingPrintWriter;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public interface TextClassifier {
  public static final int DEFAULT_SYSTEM = 2;
  
  public static final String EXTRA_FROM_TEXT_CLASSIFIER = "android.view.textclassifier.extra.FROM_TEXT_CLASSIFIER";
  
  public static final String HINT_TEXT_IS_EDITABLE = "android.text_is_editable";
  
  public static final String HINT_TEXT_IS_NOT_EDITABLE = "android.text_is_not_editable";
  
  public static final int LOCAL = 0;
  
  public static final String LOG_TAG = "androidtc";
  
  static String typeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2)
          return "Unknown"; 
        return "Default system";
      } 
      return "System";
    } 
    return "Local";
  }
  
  public static final TextClassifier NO_OP = (TextClassifier)new Object();
  
  public static final int SYSTEM = 1;
  
  public static final String TYPE_ADDRESS = "address";
  
  public static final String TYPE_DATE = "date";
  
  public static final String TYPE_DATE_TIME = "datetime";
  
  public static final String TYPE_DICTIONARY = "dictionary";
  
  public static final String TYPE_EMAIL = "email";
  
  public static final String TYPE_FLIGHT_NUMBER = "flight";
  
  public static final String TYPE_OTHER = "other";
  
  public static final String TYPE_PHONE = "phone";
  
  public static final String TYPE_UNKNOWN = "";
  
  public static final String TYPE_URL = "url";
  
  public static final String WIDGET_TYPE_CUSTOM_EDITTEXT = "customedit";
  
  public static final String WIDGET_TYPE_CUSTOM_TEXTVIEW = "customview";
  
  public static final String WIDGET_TYPE_CUSTOM_UNSELECTABLE_TEXTVIEW = "nosel-customview";
  
  public static final String WIDGET_TYPE_EDITTEXT = "edittext";
  
  public static final String WIDGET_TYPE_EDIT_WEBVIEW = "edit-webview";
  
  public static final String WIDGET_TYPE_NOTIFICATION = "notification";
  
  public static final String WIDGET_TYPE_TEXTVIEW = "textview";
  
  public static final String WIDGET_TYPE_UNKNOWN = "unknown";
  
  public static final String WIDGET_TYPE_UNSELECTABLE_TEXTVIEW = "nosel-textview";
  
  public static final String WIDGET_TYPE_WEBVIEW = "webview";
  
  default TextSelection suggestSelection(TextSelection.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    Utils.checkMainThread();
    return (new TextSelection.Builder(paramRequest.getStartIndex(), paramRequest.getEndIndex())).build();
  }
  
  default TextSelection suggestSelection(CharSequence paramCharSequence, int paramInt1, int paramInt2, LocaleList paramLocaleList) {
    TextSelection.Request.Builder builder = new TextSelection.Request.Builder(paramCharSequence, paramInt1, paramInt2);
    builder = builder.setDefaultLocales(paramLocaleList);
    TextSelection.Request request = builder.build();
    return suggestSelection(request);
  }
  
  default TextClassification classifyText(TextClassification.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    Utils.checkMainThread();
    return TextClassification.EMPTY;
  }
  
  default TextClassification classifyText(CharSequence paramCharSequence, int paramInt1, int paramInt2, LocaleList paramLocaleList) {
    TextClassification.Request.Builder builder = new TextClassification.Request.Builder(paramCharSequence, paramInt1, paramInt2);
    builder = builder.setDefaultLocales(paramLocaleList);
    TextClassification.Request request = builder.build();
    return classifyText(request);
  }
  
  default TextLinks generateLinks(TextLinks.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    Utils.checkMainThread();
    return (new TextLinks.Builder(paramRequest.getText().toString())).build();
  }
  
  default int getMaxGenerateLinksTextLength() {
    return Integer.MAX_VALUE;
  }
  
  default TextLanguage detectLanguage(TextLanguage.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    Utils.checkMainThread();
    return TextLanguage.EMPTY;
  }
  
  default ConversationActions suggestConversationActions(ConversationActions.Request paramRequest) {
    Objects.requireNonNull(paramRequest);
    Utils.checkMainThread();
    return new ConversationActions(Collections.emptyList(), null);
  }
  
  default void onSelectionEvent(SelectionEvent paramSelectionEvent) {}
  
  default void onTextClassifierEvent(TextClassifierEvent paramTextClassifierEvent) {}
  
  default void destroy() {}
  
  default boolean isDestroyed() {
    return false;
  }
  
  default void dump(IndentingPrintWriter paramIndentingPrintWriter) {}
  
  class EntityConfig implements Parcelable {
    private EntityConfig(List<String> param1List1, List<String> param1List2, boolean param1Boolean) {
      Objects.requireNonNull(this$0);
      this.mIncludedTypes = (List<String>)this$0;
      Objects.requireNonNull(param1List1);
      this.mExcludedTypes = param1List1;
      Objects.requireNonNull(param1List2);
      this.mHints = param1List2;
      this.mIncludeTypesFromTextClassifier = param1Boolean;
    }
    
    private EntityConfig(TextClassifier this$0) {
      boolean bool;
      ArrayList<String> arrayList = new ArrayList();
      this$0.readStringList(arrayList);
      this.mExcludedTypes = arrayList = new ArrayList<>();
      this$0.readStringList(arrayList);
      arrayList = new ArrayList<>();
      this$0.readStringList(arrayList);
      this.mHints = Collections.unmodifiableList(arrayList);
      if (this$0.readByte() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mIncludeTypesFromTextClassifier = bool;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeStringList(this.mIncludedTypes);
      param1Parcel.writeStringList(this.mExcludedTypes);
      param1Parcel.writeStringList(this.mHints);
      param1Parcel.writeByte((byte)this.mIncludeTypesFromTextClassifier);
    }
    
    @Deprecated
    public static EntityConfig createWithHints(Collection<String> param1Collection) {
      Builder builder2 = new Builder();
      builder2 = builder2.includeTypesFromTextClassifier(true);
      Builder builder1 = builder2.setHints(param1Collection);
      return builder1.build();
    }
    
    @Deprecated
    public static EntityConfig create(Collection<String> param1Collection1, Collection<String> param1Collection2, Collection<String> param1Collection3) {
      Builder builder3 = new Builder();
      Builder builder2 = builder3.setIncludedTypes(param1Collection2);
      builder2 = builder2.setExcludedTypes(param1Collection3);
      Builder builder1 = builder2.setHints(param1Collection1);
      builder1 = builder1.includeTypesFromTextClassifier(true);
      return builder1.build();
    }
    
    @Deprecated
    public static EntityConfig createWithExplicitEntityList(Collection<String> param1Collection) {
      Builder builder2 = new Builder();
      Builder builder1 = builder2.setIncludedTypes(param1Collection);
      builder1 = builder1.includeTypesFromTextClassifier(false);
      return builder1.build();
    }
    
    public Collection<String> resolveEntityListModifications(Collection<String> param1Collection) {
      HashSet<String> hashSet = new HashSet();
      if (this.mIncludeTypesFromTextClassifier)
        hashSet.addAll(param1Collection); 
      hashSet.addAll(this.mIncludedTypes);
      hashSet.removeAll(this.mExcludedTypes);
      return hashSet;
    }
    
    public Collection<String> getHints() {
      return this.mHints;
    }
    
    public boolean shouldIncludeTypesFromTextClassifier() {
      return this.mIncludeTypesFromTextClassifier;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<EntityConfig> CREATOR = new Parcelable.Creator<EntityConfig>() {
        public TextClassifier.EntityConfig createFromParcel(Parcel param2Parcel) {
          return new TextClassifier.EntityConfig();
        }
        
        public TextClassifier.EntityConfig[] newArray(int param2Int) {
          return new TextClassifier.EntityConfig[param2Int];
        }
      };
    
    private final List<String> mExcludedTypes;
    
    private final List<String> mHints;
    
    private final boolean mIncludeTypesFromTextClassifier;
    
    private final List<String> mIncludedTypes;
    
    class Builder {
      private Collection<String> mExcludedTypes;
      
      private Collection<String> mHints;
      
      private boolean mIncludeTypesFromTextClassifier;
      
      private Collection<String> mIncludedTypes;
      
      public Builder() {
        this.mIncludeTypesFromTextClassifier = true;
      }
      
      public Builder setIncludedTypes(Collection<String> param2Collection) {
        this.mIncludedTypes = param2Collection;
        return this;
      }
      
      public Builder setExcludedTypes(Collection<String> param2Collection) {
        this.mExcludedTypes = param2Collection;
        return this;
      }
      
      public Builder includeTypesFromTextClassifier(boolean param2Boolean) {
        this.mIncludeTypesFromTextClassifier = param2Boolean;
        return this;
      }
      
      public Builder setHints(Collection<String> param2Collection) {
        this.mHints = param2Collection;
        return this;
      }
      
      public TextClassifier.EntityConfig build() {
        List<?> list1;
        List<?> list2;
        List<?> list3;
        if (this.mIncludedTypes == null) {
          list1 = Collections.emptyList();
        } else {
          list1 = new ArrayList<>(this.mIncludedTypes);
        } 
        if (this.mExcludedTypes == null) {
          list2 = Collections.emptyList();
        } else {
          list2 = new ArrayList<>(this.mExcludedTypes);
        } 
        if (this.mHints == null) {
          list3 = Collections.emptyList();
        } else {
          list3 = Collections.unmodifiableList(new ArrayList(this.mHints));
        } 
        return new TextClassifier.EntityConfig(list2, list3, this.mIncludeTypesFromTextClassifier);
      }
    }
  }
  
  public static final class Builder {
    private Collection<String> mExcludedTypes;
    
    private Collection<String> mHints;
    
    private boolean mIncludeTypesFromTextClassifier;
    
    private Collection<String> mIncludedTypes;
    
    public Builder() {
      this.mIncludeTypesFromTextClassifier = true;
    }
    
    public Builder setIncludedTypes(Collection<String> param1Collection) {
      this.mIncludedTypes = param1Collection;
      return this;
    }
    
    public Builder setExcludedTypes(Collection<String> param1Collection) {
      this.mExcludedTypes = param1Collection;
      return this;
    }
    
    public Builder includeTypesFromTextClassifier(boolean param1Boolean) {
      this.mIncludeTypesFromTextClassifier = param1Boolean;
      return this;
    }
    
    public Builder setHints(Collection<String> param1Collection) {
      this.mHints = param1Collection;
      return this;
    }
    
    public TextClassifier.EntityConfig build() {
      List<?> list1;
      List<?> list2;
      List<?> list3;
      if (this.mIncludedTypes == null) {
        list1 = Collections.emptyList();
      } else {
        list1 = new ArrayList<>(this.mIncludedTypes);
      } 
      if (this.mExcludedTypes == null) {
        list2 = Collections.emptyList();
      } else {
        list2 = new ArrayList<>(this.mExcludedTypes);
      } 
      if (this.mHints == null) {
        list3 = Collections.emptyList();
      } else {
        list3 = Collections.unmodifiableList(new ArrayList(this.mHints));
      } 
      return new TextClassifier.EntityConfig(list2, list3, this.mIncludeTypesFromTextClassifier);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface EntityType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Hints {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TextClassifierType {}
  
  public static final class Utils {
    private static final BreakIterator WORD_ITERATOR = BreakIterator.getWordInstance();
    
    static void checkArgument(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      boolean bool2, bool1 = true;
      if (param1CharSequence != null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      if (param1Int1 >= 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      if (param1Int2 <= param1CharSequence.length()) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      if (param1Int2 > param1Int1) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
    }
    
    static boolean checkTextLength(CharSequence param1CharSequence, int param1Int) {
      boolean bool;
      int i = param1CharSequence.length();
      if (i >= 0 && i <= param1Int) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public static String getSubString(String param1String, int param1Int1, int param1Int2, int param1Int3) {
      boolean bool2, bool1 = true;
      if (param1Int1 >= 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      if (param1Int2 <= param1String.length()) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      if (param1Int1 <= param1Int2) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      if (param1String.length() < param1Int3)
        return param1String; 
      int i = param1Int2 - param1Int1;
      if (i >= param1Int3)
        return param1String.substring(param1Int1, param1Int2); 
      param1Int2 = (param1Int3 - i) / 2;
      param1Int1 = Math.max(0, Math.min(param1Int1 - param1Int2, param1String.length() - param1Int3));
      param1Int2 = Math.min(param1String.length(), param1Int1 + param1Int3);
      synchronized (WORD_ITERATOR) {
        WORD_ITERATOR.setText(param1String);
        if (!WORD_ITERATOR.isBoundary(param1Int1))
          param1Int1 = Math.max(0, WORD_ITERATOR.preceding(param1Int1)); 
        if (!WORD_ITERATOR.isBoundary(param1Int2))
          param1Int2 = Math.max(param1Int2, WORD_ITERATOR.following(param1Int2)); 
        WORD_ITERATOR.setText("");
        param1String = param1String.substring(param1Int1, param1Int2);
        return param1String;
      } 
    }
    
    public static TextLinks generateLegacyLinks(TextLinks.Request param1Request) {
      String str = param1Request.getText().toString();
      TextLinks.Builder builder = new TextLinks.Builder(str);
      TextClassifier.EntityConfig entityConfig = param1Request.getEntityConfig();
      Collection<String> collection = entityConfig.resolveEntityListModifications(Collections.emptyList());
      if (collection.contains("url"))
        addLinks(builder, str, "url"); 
      if (collection.contains("phone"))
        addLinks(builder, str, "phone"); 
      if (collection.contains("email"))
        addLinks(builder, str, "email"); 
      return builder.build();
    }
    
    private static void addLinks(TextLinks.Builder param1Builder, String param1String1, String param1String2) {
      SpannableString spannableString = new SpannableString(param1String1);
      if (Linkify.addLinks(spannableString, linkMask(param1String2))) {
        int i = spannableString.length();
        byte b = 0;
        URLSpan[] arrayOfURLSpan = spannableString.<URLSpan>getSpans(0, i, URLSpan.class);
        for (i = arrayOfURLSpan.length; b < i; ) {
          URLSpan uRLSpan = arrayOfURLSpan[b];
          int j = spannableString.getSpanStart(uRLSpan);
          int k = spannableString.getSpanEnd(uRLSpan);
          Map<String, Float> map = entityScores(param1String2);
          param1Builder.addLink(j, k, map, uRLSpan);
          b++;
        } 
      } 
    }
    
    private static int linkMask(String param1String) {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual hashCode : ()I
      //   4: istore_1
      //   5: iload_1
      //   6: ldc 116079
      //   8: if_icmpeq -> 54
      //   11: iload_1
      //   12: ldc 96619420
      //   14: if_icmpeq -> 40
      //   17: iload_1
      //   18: ldc 106642798
      //   20: if_icmpeq -> 26
      //   23: goto -> 68
      //   26: aload_0
      //   27: ldc 'phone'
      //   29: invokevirtual equals : (Ljava/lang/Object;)Z
      //   32: ifeq -> 23
      //   35: iconst_1
      //   36: istore_1
      //   37: goto -> 70
      //   40: aload_0
      //   41: ldc 'email'
      //   43: invokevirtual equals : (Ljava/lang/Object;)Z
      //   46: ifeq -> 23
      //   49: iconst_2
      //   50: istore_1
      //   51: goto -> 70
      //   54: aload_0
      //   55: ldc 'url'
      //   57: invokevirtual equals : (Ljava/lang/Object;)Z
      //   60: ifeq -> 23
      //   63: iconst_0
      //   64: istore_1
      //   65: goto -> 70
      //   68: iconst_m1
      //   69: istore_1
      //   70: iload_1
      //   71: ifeq -> 90
      //   74: iload_1
      //   75: iconst_1
      //   76: if_icmpeq -> 88
      //   79: iload_1
      //   80: iconst_2
      //   81: if_icmpeq -> 86
      //   84: iconst_0
      //   85: ireturn
      //   86: iconst_2
      //   87: ireturn
      //   88: iconst_4
      //   89: ireturn
      //   90: iconst_1
      //   91: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #771	-> 0
      //   #780	-> 84
      //   #777	-> 86
      //   #775	-> 88
      //   #773	-> 90
    }
    
    private static Map<String, Float> entityScores(String param1String) {
      ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
      arrayMap.put(param1String, Float.valueOf(1.0F));
      return (Map)arrayMap;
    }
    
    static void checkMainThread() {
      if (Looper.myLooper() == Looper.getMainLooper())
        Log.w("androidtc", "TextClassifier called on main thread"); 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface WidgetType {}
}
