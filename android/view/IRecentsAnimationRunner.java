package android.view;

import android.app.ActivityManager;
import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface IRecentsAnimationRunner extends IInterface {
  void onAnimationCanceled(ActivityManager.TaskSnapshot paramTaskSnapshot) throws RemoteException;
  
  void onAnimationStart(IRecentsAnimationController paramIRecentsAnimationController, RemoteAnimationTarget[] paramArrayOfRemoteAnimationTarget1, RemoteAnimationTarget[] paramArrayOfRemoteAnimationTarget2, Rect paramRect1, Rect paramRect2) throws RemoteException;
  
  void onTaskAppeared(RemoteAnimationTarget paramRemoteAnimationTarget) throws RemoteException;
  
  class Default implements IRecentsAnimationRunner {
    public void onAnimationCanceled(ActivityManager.TaskSnapshot param1TaskSnapshot) throws RemoteException {}
    
    public void onAnimationStart(IRecentsAnimationController param1IRecentsAnimationController, RemoteAnimationTarget[] param1ArrayOfRemoteAnimationTarget1, RemoteAnimationTarget[] param1ArrayOfRemoteAnimationTarget2, Rect param1Rect1, Rect param1Rect2) throws RemoteException {}
    
    public void onTaskAppeared(RemoteAnimationTarget param1RemoteAnimationTarget) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecentsAnimationRunner {
    private static final String DESCRIPTOR = "android.view.IRecentsAnimationRunner";
    
    static final int TRANSACTION_onAnimationCanceled = 2;
    
    static final int TRANSACTION_onAnimationStart = 3;
    
    static final int TRANSACTION_onTaskAppeared = 4;
    
    public Stub() {
      attachInterface(this, "android.view.IRecentsAnimationRunner");
    }
    
    public static IRecentsAnimationRunner asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IRecentsAnimationRunner");
      if (iInterface != null && iInterface instanceof IRecentsAnimationRunner)
        return (IRecentsAnimationRunner)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 2) {
        if (param1Int != 3) {
          if (param1Int != 4)
            return null; 
          return "onTaskAppeared";
        } 
        return "onAnimationStart";
      } 
      return "onAnimationCanceled";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 2) {
        if (param1Int1 != 3) {
          if (param1Int1 != 4) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.view.IRecentsAnimationRunner");
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.IRecentsAnimationRunner");
          if (param1Parcel1.readInt() != 0) {
            RemoteAnimationTarget remoteAnimationTarget = (RemoteAnimationTarget)RemoteAnimationTarget.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onTaskAppeared((RemoteAnimationTarget)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IRecentsAnimationRunner");
        IRecentsAnimationController iRecentsAnimationController = IRecentsAnimationController.Stub.asInterface(param1Parcel1.readStrongBinder());
        RemoteAnimationTarget[] arrayOfRemoteAnimationTarget1 = (RemoteAnimationTarget[])param1Parcel1.createTypedArray(RemoteAnimationTarget.CREATOR);
        RemoteAnimationTarget[] arrayOfRemoteAnimationTarget2 = (RemoteAnimationTarget[])param1Parcel1.createTypedArray(RemoteAnimationTarget.CREATOR);
        if (param1Parcel1.readInt() != 0) {
          Rect rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel2 = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          Rect rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onAnimationStart(iRecentsAnimationController, arrayOfRemoteAnimationTarget1, arrayOfRemoteAnimationTarget2, (Rect)param1Parcel2, (Rect)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IRecentsAnimationRunner");
      if (param1Parcel1.readInt() != 0) {
        ActivityManager.TaskSnapshot taskSnapshot = (ActivityManager.TaskSnapshot)ActivityManager.TaskSnapshot.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onAnimationCanceled((ActivityManager.TaskSnapshot)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IRecentsAnimationRunner {
      public static IRecentsAnimationRunner sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IRecentsAnimationRunner";
      }
      
      public void onAnimationCanceled(ActivityManager.TaskSnapshot param2TaskSnapshot) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IRecentsAnimationRunner");
          if (param2TaskSnapshot != null) {
            parcel.writeInt(1);
            param2TaskSnapshot.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IRecentsAnimationRunner.Stub.getDefaultImpl() != null) {
            IRecentsAnimationRunner.Stub.getDefaultImpl().onAnimationCanceled(param2TaskSnapshot);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAnimationStart(IRecentsAnimationController param2IRecentsAnimationController, RemoteAnimationTarget[] param2ArrayOfRemoteAnimationTarget1, RemoteAnimationTarget[] param2ArrayOfRemoteAnimationTarget2, Rect param2Rect1, Rect param2Rect2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IRecentsAnimationRunner");
          if (param2IRecentsAnimationController != null) {
            iBinder = param2IRecentsAnimationController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfRemoteAnimationTarget1, 0);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfRemoteAnimationTarget2, 0);
          if (param2Rect1 != null) {
            parcel.writeInt(1);
            param2Rect1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Rect2 != null) {
            parcel.writeInt(1);
            param2Rect2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IRecentsAnimationRunner.Stub.getDefaultImpl() != null) {
            IRecentsAnimationRunner.Stub.getDefaultImpl().onAnimationStart(param2IRecentsAnimationController, param2ArrayOfRemoteAnimationTarget1, param2ArrayOfRemoteAnimationTarget2, param2Rect1, param2Rect2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTaskAppeared(RemoteAnimationTarget param2RemoteAnimationTarget) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IRecentsAnimationRunner");
          if (param2RemoteAnimationTarget != null) {
            parcel.writeInt(1);
            param2RemoteAnimationTarget.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IRecentsAnimationRunner.Stub.getDefaultImpl() != null) {
            IRecentsAnimationRunner.Stub.getDefaultImpl().onTaskAppeared(param2RemoteAnimationTarget);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRecentsAnimationRunner param1IRecentsAnimationRunner) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecentsAnimationRunner != null) {
          Proxy.sDefaultImpl = param1IRecentsAnimationRunner;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecentsAnimationRunner getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
