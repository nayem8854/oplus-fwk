package android.view;

import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;
import com.oplus.util.OplusAccidentallyTouchData;
import com.oplus.util.OplusAccidentallyTouchUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class OplusAccidentallyTouchHelper implements IOplusAccidentallyTouchHelper {
  static final String TAG_WM = "ColorAccidentallyManager";
  
  private static volatile OplusAccidentallyTouchHelper sInstance = null;
  
  public static OplusAccidentallyTouchHelper getInstance() {
    // Byte code:
    //   0: getstatic android/view/OplusAccidentallyTouchHelper.sInstance : Landroid/view/OplusAccidentallyTouchHelper;
    //   3: ifnonnull -> 39
    //   6: ldc android/view/OplusAccidentallyTouchHelper
    //   8: monitorenter
    //   9: getstatic android/view/OplusAccidentallyTouchHelper.sInstance : Landroid/view/OplusAccidentallyTouchHelper;
    //   12: ifnonnull -> 27
    //   15: new android/view/OplusAccidentallyTouchHelper
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/view/OplusAccidentallyTouchHelper.sInstance : Landroid/view/OplusAccidentallyTouchHelper;
    //   27: ldc android/view/OplusAccidentallyTouchHelper
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/view/OplusAccidentallyTouchHelper
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/view/OplusAccidentallyTouchHelper.sInstance : Landroid/view/OplusAccidentallyTouchHelper;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #39	-> 0
    //   #40	-> 6
    //   #41	-> 9
    //   #42	-> 15
    //   #44	-> 27
    //   #46	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public MotionEvent updatePointerEvent(MotionEvent paramMotionEvent, View paramView, Configuration paramConfiguration) {
    return OplusAccidentallyTouchUtils.getInstance().updatePointerEvent(paramMotionEvent, paramView, paramConfiguration);
  }
  
  public void initOnAmsReady() {
    OplusAccidentallyTouchUtils.getInstance().init();
  }
  
  public void initData(Context paramContext) {
    OplusAccidentallyTouchUtils.getInstance().initData(paramContext);
  }
  
  public OplusAccidentallyTouchData getAccidentallyTouchData() {
    return OplusAccidentallyTouchUtils.getInstance().getTouchData();
  }
  
  public void updataeAccidentPreventionState(Context paramContext, boolean paramBoolean, final int rotation) {
    final String whiteList;
    if (paramBoolean) {
      str = "1";
    } else {
      str = "0";
    } 
    Thread thread = new Thread() {
        final OplusAccidentallyTouchHelper this$0;
        
        final int val$rotation;
        
        final String val$whiteList;
        
        public void run() {
          int i = rotation;
          if (i != 0) {
            if (i != 1) {
              if (i != 2) {
                if (i == 3)
                  OplusAccidentallyTouchHelper.this.updateTouchPanelInfo("0", "2", whiteList); 
              } else {
                OplusAccidentallyTouchHelper.this.updateTouchPanelInfo("1", "0", whiteList);
              } 
            } else {
              OplusAccidentallyTouchHelper.this.updateTouchPanelInfo("0", "1", whiteList);
            } 
          } else {
            OplusAccidentallyTouchHelper.this.updateTouchPanelInfo("1", "0", whiteList);
          } 
        }
      };
    thread.start();
  }
  
  private void updateTouchPanelInfo(String paramString1, String paramString2, String paramString3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("accidentPrevention >>> updateTouchPanelInfo: ");
    stringBuilder.append(paramString3);
    stringBuilder.append(",");
    stringBuilder.append(paramString2);
    stringBuilder.append(",");
    stringBuilder.append(paramString1);
    Log.d("ColorAccidentallyManager", stringBuilder.toString());
    File file1 = new File("/proc/touchpanel/oplus_tp_limit_whitelist");
    File file2 = new File("/proc/touchpanel/oplus_tp_direction");
    File file3 = new File("/proc/touchpanel/oplus_tp_limit_enable");
    writeTouchPanelFile(file1, paramString3);
    writeTouchPanelFile(file2, paramString2);
    writeTouchPanelFile(file3, paramString1);
  }
  
  private void writeTouchPanelFile(File paramFile, String paramString) {
    StringBuilder stringBuilder1;
    if (!paramFile.exists()) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("accidentPrevention >>> not exists: ");
      stringBuilder1.append(paramFile);
      Log.d("ColorAccidentallyManager", stringBuilder1.toString());
      return;
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("accidentPrevention >>> writeNarrowFile: ");
    stringBuilder2.append(paramFile);
    stringBuilder2.append(", ");
    stringBuilder2.append((String)stringBuilder1);
    Log.d("ColorAccidentallyManager", stringBuilder2.toString());
    FileOutputStream fileOutputStream1 = null, fileOutputStream2 = null;
    StringBuilder stringBuilder3 = null;
    stringBuilder2 = stringBuilder3;
    FileOutputStream fileOutputStream3 = fileOutputStream1, fileOutputStream4 = fileOutputStream2;
    try {
      FileOutputStream fileOutputStream7 = new FileOutputStream();
      stringBuilder2 = stringBuilder3;
      fileOutputStream3 = fileOutputStream1;
      fileOutputStream4 = fileOutputStream2;
      this(paramFile);
      FileOutputStream fileOutputStream5 = fileOutputStream7;
      FileOutputStream fileOutputStream6 = fileOutputStream5;
      fileOutputStream3 = fileOutputStream5;
      fileOutputStream4 = fileOutputStream5;
      fileOutputStream5.write(stringBuilder1.getBytes());
      try {
        fileOutputStream5.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (IOException iOException) {
      FileOutputStream fileOutputStream = fileOutputStream4;
      iOException.printStackTrace();
      if (fileOutputStream4 != null)
        fileOutputStream4.close(); 
    } catch (Exception exception) {
      FileOutputStream fileOutputStream = fileOutputStream3;
      exception.printStackTrace();
      if (fileOutputStream3 != null)
        fileOutputStream3.close(); 
    } finally {}
  }
  
  public int getOriEdgeT1() {
    return OplusAccidentallyTouchUtils.getInstance().getOriEdgeT1();
  }
  
  public int getEdgeT2() {
    return OplusAccidentallyTouchUtils.getInstance().getEdgeT2();
  }
  
  public boolean getEdgeEnable() {
    return OplusAccidentallyTouchUtils.getInstance().getEdgeEnable();
  }
  
  public int getEdgeT1() {
    return OplusAccidentallyTouchUtils.getInstance().getEdgeT1();
  }
}
