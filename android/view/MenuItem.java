package android.view;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.BlendMode;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

public interface MenuItem {
  public static final int SHOW_AS_ACTION_ALWAYS = 2;
  
  public static final int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;
  
  public static final int SHOW_AS_ACTION_IF_ROOM = 1;
  
  public static final int SHOW_AS_ACTION_NEVER = 0;
  
  public static final int SHOW_AS_ACTION_WITH_TEXT = 4;
  
  default MenuItem setIconTintList(ColorStateList paramColorStateList) {
    return this;
  }
  
  default ColorStateList getIconTintList() {
    return null;
  }
  
  default MenuItem setIconTintMode(PorterDuff.Mode paramMode) {
    return this;
  }
  
  default MenuItem setIconTintBlendMode(BlendMode paramBlendMode) {
    PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(paramBlendMode);
    if (mode != null)
      return setIconTintMode(mode); 
    return this;
  }
  
  default PorterDuff.Mode getIconTintMode() {
    return null;
  }
  
  default BlendMode getIconTintBlendMode() {
    PorterDuff.Mode mode = getIconTintMode();
    if (mode != null)
      return BlendMode.fromValue(mode.nativeInt); 
    return null;
  }
  
  default MenuItem setShortcut(char paramChar1, char paramChar2, int paramInt1, int paramInt2) {
    if ((paramInt2 & 0x1100F) == 4096 && (0x1100F & paramInt1) == 4096)
      return setShortcut(paramChar1, paramChar2); 
    return this;
  }
  
  default MenuItem setNumericShortcut(char paramChar, int paramInt) {
    if ((0x1100F & paramInt) == 4096)
      return setNumericShortcut(paramChar); 
    return this;
  }
  
  default int getNumericModifiers() {
    return 4096;
  }
  
  default MenuItem setAlphabeticShortcut(char paramChar, int paramInt) {
    if ((0x1100F & paramInt) == 4096)
      return setAlphabeticShortcut(paramChar); 
    return this;
  }
  
  default int getAlphabeticModifiers() {
    return 4096;
  }
  
  default MenuItem setContentDescription(CharSequence paramCharSequence) {
    return this;
  }
  
  default CharSequence getContentDescription() {
    return null;
  }
  
  default MenuItem setTooltipText(CharSequence paramCharSequence) {
    return this;
  }
  
  default CharSequence getTooltipText() {
    return null;
  }
  
  default boolean requiresActionButton() {
    return false;
  }
  
  default boolean requiresOverflow() {
    return true;
  }
  
  boolean collapseActionView();
  
  boolean expandActionView();
  
  ActionProvider getActionProvider();
  
  View getActionView();
  
  char getAlphabeticShortcut();
  
  int getGroupId();
  
  Drawable getIcon();
  
  Intent getIntent();
  
  int getItemId();
  
  ContextMenu.ContextMenuInfo getMenuInfo();
  
  char getNumericShortcut();
  
  int getOrder();
  
  SubMenu getSubMenu();
  
  CharSequence getTitle();
  
  CharSequence getTitleCondensed();
  
  boolean hasSubMenu();
  
  boolean isActionViewExpanded();
  
  boolean isCheckable();
  
  boolean isChecked();
  
  boolean isEnabled();
  
  boolean isVisible();
  
  MenuItem setActionProvider(ActionProvider paramActionProvider);
  
  MenuItem setActionView(int paramInt);
  
  MenuItem setActionView(View paramView);
  
  MenuItem setAlphabeticShortcut(char paramChar);
  
  MenuItem setCheckable(boolean paramBoolean);
  
  MenuItem setChecked(boolean paramBoolean);
  
  MenuItem setEnabled(boolean paramBoolean);
  
  MenuItem setIcon(int paramInt);
  
  MenuItem setIcon(Drawable paramDrawable);
  
  MenuItem setIntent(Intent paramIntent);
  
  MenuItem setNumericShortcut(char paramChar);
  
  MenuItem setOnActionExpandListener(OnActionExpandListener paramOnActionExpandListener);
  
  MenuItem setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener);
  
  MenuItem setShortcut(char paramChar1, char paramChar2);
  
  void setShowAsAction(int paramInt);
  
  MenuItem setShowAsActionFlags(int paramInt);
  
  MenuItem setTitle(int paramInt);
  
  MenuItem setTitle(CharSequence paramCharSequence);
  
  MenuItem setTitleCondensed(CharSequence paramCharSequence);
  
  MenuItem setVisible(boolean paramBoolean);
  
  public static interface OnActionExpandListener {
    boolean onMenuItemActionCollapse(MenuItem param1MenuItem);
    
    boolean onMenuItemActionExpand(MenuItem param1MenuItem);
  }
  
  public static interface OnMenuItemClickListener {
    boolean onMenuItemClick(MenuItem param1MenuItem);
  }
}
