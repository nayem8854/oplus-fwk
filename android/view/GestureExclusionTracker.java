package android.view;

import android.graphics.Rect;
import com.android.internal.util.Preconditions;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class GestureExclusionTracker {
  private boolean mGestureExclusionViewsChanged = false;
  
  private boolean mRootGestureExclusionRectsChanged = false;
  
  private List<Rect> mRootGestureExclusionRects = Collections.emptyList();
  
  private List<GestureExclusionViewInfo> mGestureExclusionViewInfos = new ArrayList<>();
  
  private List<Rect> mGestureExclusionRects = Collections.emptyList();
  
  public void updateRectsForView(View paramView) {
    boolean bool2, bool1 = false;
    Iterator<GestureExclusionViewInfo> iterator = this.mGestureExclusionViewInfos.iterator();
    while (true) {
      bool2 = bool1;
      if (iterator.hasNext()) {
        GestureExclusionViewInfo gestureExclusionViewInfo = iterator.next();
        View view = gestureExclusionViewInfo.getView();
        if (view == null || !view.isAttachedToWindow() || !view.isAggregatedVisible()) {
          this.mGestureExclusionViewsChanged = true;
          iterator.remove();
          continue;
        } 
        if (view == paramView) {
          bool2 = true;
          gestureExclusionViewInfo.mDirty = true;
          break;
        } 
        continue;
      } 
      break;
    } 
    if (!bool2 && paramView.isAttachedToWindow()) {
      this.mGestureExclusionViewInfos.add(new GestureExclusionViewInfo(paramView));
      this.mGestureExclusionViewsChanged = true;
    } 
  }
  
  public List<Rect> computeChangedRects() {
    boolean bool = this.mRootGestureExclusionRectsChanged;
    Iterator<GestureExclusionViewInfo> iterator = this.mGestureExclusionViewInfos.iterator();
    ArrayList<Rect> arrayList = new ArrayList<>(this.mRootGestureExclusionRects);
    while (iterator.hasNext()) {
      boolean bool1;
      GestureExclusionViewInfo gestureExclusionViewInfo = iterator.next();
      int i = gestureExclusionViewInfo.update();
      if (i != 0) {
        bool1 = bool;
        if (i != 1) {
          if (i != 2)
            continue; 
          this.mGestureExclusionViewsChanged = true;
          iterator.remove();
          continue;
        } 
      } else {
        bool1 = true;
      } 
      arrayList.addAll(gestureExclusionViewInfo.mExclusionRects);
      bool = bool1;
    } 
    if (bool || this.mGestureExclusionViewsChanged) {
      this.mGestureExclusionViewsChanged = false;
      this.mRootGestureExclusionRectsChanged = false;
      if (!this.mGestureExclusionRects.equals(arrayList)) {
        this.mGestureExclusionRects = arrayList;
        return arrayList;
      } 
    } 
    return null;
  }
  
  public void setRootSystemGestureExclusionRects(List<Rect> paramList) {
    Preconditions.checkNotNull(paramList, "rects must not be null");
    this.mRootGestureExclusionRects = paramList;
    this.mRootGestureExclusionRectsChanged = true;
  }
  
  public List<Rect> getRootSystemGestureExclusionRects() {
    return this.mRootGestureExclusionRects;
  }
  
  private static class GestureExclusionViewInfo {
    public static final int CHANGED = 0;
    
    public static final int GONE = 2;
    
    public static final int UNCHANGED = 1;
    
    boolean mDirty = true;
    
    List<Rect> mExclusionRects = Collections.emptyList();
    
    private final WeakReference<View> mView;
    
    GestureExclusionViewInfo(View param1View) {
      this.mView = new WeakReference<>(param1View);
    }
    
    public View getView() {
      return this.mView.get();
    }
    
    public int update() {
      View view = getView();
      if (view == null || !view.isAttachedToWindow() || 
        !view.isAggregatedVisible())
        return 2; 
      List<Rect> list = view.getSystemGestureExclusionRects();
      ArrayList<Rect> arrayList = new ArrayList(list.size());
      for (Rect rect : list) {
        rect = new Rect(rect);
        ViewParent viewParent = view.getParent();
        if (viewParent != null && viewParent.getChildVisibleRect(view, rect, null))
          arrayList.add(rect); 
      } 
      if (this.mExclusionRects.equals(list))
        return 1; 
      this.mExclusionRects = arrayList;
      return 0;
    }
  }
}
