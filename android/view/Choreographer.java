package android.view;

import android.graphics.FrameInfo;
import android.hardware.display.DisplayManagerGlobal;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Trace;
import android.util.BoostFramework;
import android.util.Log;
import android.util.TimeUtils;
import android.view.animation.AnimationUtils;
import java.io.PrintWriter;

public final class Choreographer {
  private static boolean OPTS_INPUT = false;
  
  private static volatile long sFrameDelay = 10L;
  
  private int sIsSFChoregrapher = 0;
  
  private static final ThreadLocal<Choreographer> sThreadInstance = new ThreadLocal<Choreographer>() {
      protected Choreographer initialValue() {
        Looper looper = Looper.myLooper();
        if (looper != null) {
          Choreographer choreographer = new Choreographer(looper, 0);
          if (looper == Looper.getMainLooper())
            Choreographer.access$102(choreographer); 
          return choreographer;
        } 
        throw new IllegalStateException("The current thread must have a looper!");
      }
    };
  
  static {
    sSfThreadInstance = new ThreadLocal<Choreographer>() {
        protected Choreographer initialValue() {
          Looper looper = Looper.myLooper();
          if (looper != null)
            return new Choreographer(looper, 1); 
          throw new IllegalStateException("The current thread must have a looper!");
        }
      };
    USE_VSYNC = SystemProperties.getBoolean("debug.choreographer.vsync", true);
    USE_FRAME_TIME = SystemProperties.getBoolean("debug.choreographer.frametime", true);
    SKIPPED_FRAME_WARNING_LIMIT = SystemProperties.getInt("debug.choreographer.skipwarning", 30);
    SKIPPED_FRAME_THRESHOLD = SystemProperties.getInt("debug.skip_frame_threshold", 1);
    FRAME_CALLBACK_TOKEN = new Object() {
        public String toString() {
          return "FRAME_CALLBACK_TOKEN";
        }
      };
    CALLBACK_TRACE_TITLES = new String[] { "input", "animation", "insets_animation", "traversal", "commit" };
  }
  
  private final Object mLock = new Object();
  
  private int mFPSDivisor = 1;
  
  private int mTouchMoveNum = -1;
  
  private int mMotionEventType = -1;
  
  private boolean mConsumedMove = false;
  
  private boolean mConsumedDown = false;
  
  private boolean mIsVsyncScheduled = false;
  
  private long mLastTouchOptTimeNanos = 0L;
  
  private boolean mIsDoFrameProcessing = false;
  
  private long mTraceMoreFrames = 0L;
  
  FrameInfo mFrameInfo = new FrameInfo();
  
  public static final int CALLBACK_ANIMATION = 1;
  
  public static final int CALLBACK_COMMIT = 4;
  
  public static final int CALLBACK_INPUT = 0;
  
  public static final int CALLBACK_INSETS_ANIMATION = 2;
  
  private static final int CALLBACK_LAST = 4;
  
  private static final String[] CALLBACK_TRACE_TITLES;
  
  public static final int CALLBACK_TRAVERSAL = 3;
  
  private static final boolean DEBUG_FRAMES = false;
  
  private static final boolean DEBUG_JANK = false;
  
  private static final long DEFAULT_FRAME_DELAY = 10L;
  
  private static final Object FRAME_CALLBACK_TOKEN;
  
  private static final int MOTION_EVENT_ACTION_CANCEL = 3;
  
  private static final int MOTION_EVENT_ACTION_DOWN = 0;
  
  private static final int MOTION_EVENT_ACTION_MOVE = 2;
  
  private static final int MOTION_EVENT_ACTION_UP = 1;
  
  private static final int MSG_DO_FRAME = 0;
  
  private static final int MSG_DO_SCHEDULE_CALLBACK = 2;
  
  private static final int MSG_DO_SCHEDULE_VSYNC = 1;
  
  private static final int SKIPPED_FRAME_THRESHOLD;
  
  private static final int SKIPPED_FRAME_WARNING_LIMIT;
  
  private static final String TAG = "Choreographer";
  
  private static final boolean USE_FRAME_TIME;
  
  private static final boolean USE_VSYNC;
  
  private static volatile Choreographer mMainInstance;
  
  private static final ThreadLocal<Choreographer> sSfThreadInstance;
  
  private CallbackRecord mCallbackPool;
  
  private final CallbackQueue[] mCallbackQueues;
  
  private boolean mCallbacksRunning;
  
  private boolean mDebugPrintNextFrameTimeDelta;
  
  private final FrameDisplayEventReceiver mDisplayEventReceiver;
  
  private long mFrameIntervalNanos;
  
  private boolean mFrameScheduled;
  
  private final FrameHandler mHandler;
  
  private long mLastFrameTimeNanos;
  
  private final Looper mLooper;
  
  private Choreographer(Looper paramLooper, int paramInt) {
    this.mLooper = paramLooper;
    this.mHandler = new FrameHandler(paramLooper);
    if (USE_VSYNC) {
      FrameDisplayEventReceiver frameDisplayEventReceiver = new FrameDisplayEventReceiver(paramLooper, paramInt);
    } else {
      paramLooper = null;
    } 
    this.mDisplayEventReceiver = (FrameDisplayEventReceiver)paramLooper;
    this.mLastFrameTimeNanos = Long.MIN_VALUE;
    long l = (long)(1.0E9F / getRefreshRate());
    BoostFramework.ScrollOptimizer.setFrameInterval(l);
    this.mCallbackQueues = new CallbackQueue[5];
    for (paramInt = 0; paramInt <= 4; paramInt++)
      this.mCallbackQueues[paramInt] = new CallbackQueue(); 
    setFPSDivisor(SystemProperties.getInt("debug.hwui.fps_divisor", 1));
  }
  
  private static float getRefreshRate() {
    DisplayInfo displayInfo = DisplayManagerGlobal.getInstance().getDisplayInfo(0);
    return displayInfo.getMode().getRefreshRate();
  }
  
  public static Choreographer getInstance() {
    return sThreadInstance.get();
  }
  
  public static Choreographer getSfInstance() {
    return sSfThreadInstance.get();
  }
  
  public void setMotionEventInfo(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_2
    //   4: putfield mTouchMoveNum : I
    //   7: aload_0
    //   8: iload_1
    //   9: putfield mMotionEventType : I
    //   12: iload_1
    //   13: invokestatic setMotionType : (I)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #335	-> 0
    //   #336	-> 2
    //   #337	-> 7
    //   #338	-> 12
    //   #339	-> 16
    //   #340	-> 18
    //   #339	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	7	19	finally
    //   7	12	19	finally
    //   12	16	19	finally
    //   16	18	19	finally
    //   20	22	19	finally
  }
  
  public static Choreographer getMainThreadInstance() {
    return mMainInstance;
  }
  
  public static void releaseInstance() {
    Choreographer choreographer = sThreadInstance.get();
    sThreadInstance.remove();
    choreographer.dispose();
  }
  
  private void dispose() {
    this.mDisplayEventReceiver.dispose();
  }
  
  public static long getFrameDelay() {
    return sFrameDelay;
  }
  
  public static void setFrameDelay(long paramLong) {
    sFrameDelay = paramLong;
  }
  
  public static long subtractFrameDelay(long paramLong) {
    long l = sFrameDelay;
    if (paramLong <= l) {
      paramLong = 0L;
    } else {
      paramLong -= l;
    } 
    return paramLong;
  }
  
  public long getFrameIntervalNanos() {
    return this.mFrameIntervalNanos;
  }
  
  void dump(String paramString, PrintWriter paramPrintWriter) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("  ");
    String str = stringBuilder.toString();
    paramPrintWriter.print(paramString);
    paramPrintWriter.println("Choreographer:");
    paramPrintWriter.print(str);
    paramPrintWriter.print("mFrameScheduled=");
    paramPrintWriter.println(this.mFrameScheduled);
    paramPrintWriter.print(str);
    paramPrintWriter.print("mLastFrameTime=");
    paramPrintWriter.println(TimeUtils.formatUptime(this.mLastFrameTimeNanos / 1000000L));
  }
  
  public void postCallback(int paramInt, Runnable paramRunnable, Object paramObject) {
    postCallbackDelayed(paramInt, paramRunnable, paramObject, 0L);
  }
  
  public void postCallbackDelayed(int paramInt, Runnable paramRunnable, Object paramObject, long paramLong) {
    if (paramRunnable != null) {
      if (paramInt >= 0 && paramInt <= 4) {
        postCallbackDelayedInternal(paramInt, paramRunnable, paramObject, paramLong);
        return;
      } 
      throw new IllegalArgumentException("callbackType is invalid");
    } 
    throw new IllegalArgumentException("action must not be null");
  }
  
  private void postCallbackDelayedInternal(int paramInt, Object paramObject1, Object paramObject2, long paramLong) {
    synchronized (this.mLock) {
      long l = SystemClock.uptimeMillis();
      paramLong = l + paramLong;
      this.mCallbackQueues[paramInt].addCallbackLocked(paramLong, paramObject1, paramObject2);
      if (paramLong <= l) {
        scheduleFrameLocked(l);
      } else {
        paramObject1 = this.mHandler.obtainMessage(2, paramObject1);
        ((Message)paramObject1).arg1 = paramInt;
        paramObject1.setAsynchronous(true);
        this.mHandler.sendMessageAtTime((Message)paramObject1, paramLong);
      } 
      return;
    } 
  }
  
  public void doFrameImmediately() {
    synchronized (this.mLock) {
      Message message = this.mHandler.obtainMessage(0);
      message.setAsynchronous(true);
      this.mHandler.sendMessageAtFrontOfQueue(message);
      return;
    } 
  }
  
  public void postCallbackImmediately(int paramInt, Object paramObject1, Object paramObject2, long paramLong) {
    if (paramObject1 != null) {
      if (paramInt >= 0 && paramInt <= 4)
        synchronized (this.mLock) {
          long l = SystemClock.uptimeMillis();
          this.mCallbackQueues[paramInt].addCallbackLocked(l + paramLong, paramObject1, paramObject2);
          return;
        }  
      throw new IllegalArgumentException("callbackType is invalid");
    } 
    throw new IllegalArgumentException("action must not be null");
  }
  
  public void removeCallbacks(int paramInt, Runnable paramRunnable, Object paramObject) {
    if (paramInt >= 0 && paramInt <= 4) {
      removeCallbacksInternal(paramInt, paramRunnable, paramObject);
      return;
    } 
    throw new IllegalArgumentException("callbackType is invalid");
  }
  
  private void removeCallbacksInternal(int paramInt, Object paramObject1, Object paramObject2) {
    synchronized (this.mLock) {
      this.mCallbackQueues[paramInt].removeCallbacksLocked(paramObject1, paramObject2);
      if (paramObject1 != null && paramObject2 == null)
        this.mHandler.removeMessages(2, paramObject1); 
      return;
    } 
  }
  
  public void postFrameCallback(FrameCallback paramFrameCallback) {
    postFrameCallbackDelayed(paramFrameCallback, 0L);
  }
  
  public void postFrameCallbackDelayed(FrameCallback paramFrameCallback, long paramLong) {
    if (paramFrameCallback != null) {
      postCallbackDelayedInternal(1, paramFrameCallback, FRAME_CALLBACK_TOKEN, paramLong);
      return;
    } 
    throw new IllegalArgumentException("callback must not be null");
  }
  
  public void removeFrameCallback(FrameCallback paramFrameCallback) {
    if (paramFrameCallback != null) {
      removeCallbacksInternal(1, paramFrameCallback, FRAME_CALLBACK_TOKEN);
      return;
    } 
    throw new IllegalArgumentException("callback must not be null");
  }
  
  public long getFrameTime() {
    return getFrameTimeNanos() / 1000000L;
  }
  
  public long getFrameTimeNanos() {
    synchronized (this.mLock) {
      if (this.mCallbacksRunning) {
        long l;
        if (USE_FRAME_TIME) {
          l = this.mLastFrameTimeNanos;
        } else {
          l = System.nanoTime();
        } 
        return l;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("This method must only be called as part of a callback while a frame is in progress.");
      throw illegalStateException;
    } 
  }
  
  public long getLastFrameTimeNanos() {
    synchronized (this.mLock) {
      long l;
      if (USE_FRAME_TIME) {
        l = this.mLastFrameTimeNanos;
      } else {
        l = System.nanoTime();
      } 
      return l;
    } 
  }
  
  private void scheduleFrameLocked(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mFrameScheduled : Z
    //   4: ifne -> 509
    //   7: aload_0
    //   8: iconst_1
    //   9: putfield mFrameScheduled : Z
    //   12: getstatic android/view/Choreographer.OPTS_INPUT : Z
    //   15: ifeq -> 400
    //   18: aload_0
    //   19: getfield mIsVsyncScheduled : Z
    //   22: ifne -> 400
    //   25: invokestatic nanoTime : ()J
    //   28: lstore_3
    //   29: lload_3
    //   30: aload_0
    //   31: getfield mLastTouchOptTimeNanos : J
    //   34: lsub
    //   35: aload_0
    //   36: getfield mFrameIntervalNanos : J
    //   39: lcmp
    //   40: ifge -> 49
    //   43: iconst_1
    //   44: istore #5
    //   46: goto -> 52
    //   49: iconst_0
    //   50: istore #5
    //   52: new java/lang/StringBuilder
    //   55: dup
    //   56: invokespecial <init> : ()V
    //   59: astore #6
    //   61: aload #6
    //   63: ldc_w 'scheduleFrameLocked-mMotionEventType:'
    //   66: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: pop
    //   70: aload #6
    //   72: aload_0
    //   73: getfield mMotionEventType : I
    //   76: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload #6
    //   82: ldc_w ' mTouchMoveNum:'
    //   85: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: pop
    //   89: aload #6
    //   91: aload_0
    //   92: getfield mTouchMoveNum : I
    //   95: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   98: pop
    //   99: aload #6
    //   101: ldc_w ' mConsumedDown:'
    //   104: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   107: pop
    //   108: aload #6
    //   110: aload_0
    //   111: getfield mConsumedDown : Z
    //   114: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   117: pop
    //   118: aload #6
    //   120: ldc_w ' mConsumedMove:'
    //   123: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload #6
    //   129: aload_0
    //   130: getfield mConsumedMove : Z
    //   133: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   136: pop
    //   137: aload #6
    //   139: ldc_w ' mIsDoFrameProcessing:'
    //   142: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload #6
    //   148: aload_0
    //   149: getfield mIsDoFrameProcessing : Z
    //   152: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload #6
    //   158: ldc_w ' skip:'
    //   161: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: aload #6
    //   167: iload #5
    //   169: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload #6
    //   175: ldc_w ' diff:'
    //   178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: aload #6
    //   184: lload_3
    //   185: aload_0
    //   186: getfield mLastTouchOptTimeNanos : J
    //   189: lsub
    //   190: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   193: pop
    //   194: ldc2_w 8
    //   197: aload #6
    //   199: invokevirtual toString : ()Ljava/lang/String;
    //   202: invokestatic traceBegin : (JLjava/lang/String;)V
    //   205: ldc2_w 8
    //   208: invokestatic traceEnd : (J)V
    //   211: aload_0
    //   212: monitorenter
    //   213: aload_0
    //   214: getfield mMotionEventType : I
    //   217: istore #7
    //   219: iload #7
    //   221: ifeq -> 323
    //   224: iload #7
    //   226: iconst_1
    //   227: if_icmpeq -> 310
    //   230: iload #7
    //   232: iconst_2
    //   233: if_icmpeq -> 245
    //   236: iload #7
    //   238: iconst_3
    //   239: if_icmpeq -> 310
    //   242: goto -> 388
    //   245: aload_0
    //   246: iconst_0
    //   247: putfield mConsumedDown : Z
    //   250: aload_0
    //   251: getfield mConsumedMove : Z
    //   254: ifne -> 388
    //   257: iload #5
    //   259: ifne -> 388
    //   262: aload_0
    //   263: getfield mIsDoFrameProcessing : Z
    //   266: ifne -> 388
    //   269: aload_0
    //   270: getfield mHandler : Landroid/view/Choreographer$FrameHandler;
    //   273: iconst_0
    //   274: invokevirtual obtainMessage : (I)Landroid/os/Message;
    //   277: astore #6
    //   279: aload #6
    //   281: iconst_1
    //   282: invokevirtual setAsynchronous : (Z)V
    //   285: aload_0
    //   286: getfield mHandler : Landroid/view/Choreographer$FrameHandler;
    //   289: aload #6
    //   291: invokevirtual sendMessageAtFrontOfQueue : (Landroid/os/Message;)Z
    //   294: pop
    //   295: aload_0
    //   296: invokestatic nanoTime : ()J
    //   299: putfield mLastTouchOptTimeNanos : J
    //   302: aload_0
    //   303: iconst_1
    //   304: putfield mConsumedMove : Z
    //   307: aload_0
    //   308: monitorexit
    //   309: return
    //   310: aload_0
    //   311: iconst_0
    //   312: putfield mConsumedMove : Z
    //   315: aload_0
    //   316: iconst_0
    //   317: putfield mConsumedDown : Z
    //   320: goto -> 388
    //   323: aload_0
    //   324: iconst_0
    //   325: putfield mConsumedMove : Z
    //   328: aload_0
    //   329: getfield mConsumedDown : Z
    //   332: ifne -> 388
    //   335: iload #5
    //   337: ifne -> 388
    //   340: aload_0
    //   341: getfield mIsDoFrameProcessing : Z
    //   344: ifne -> 388
    //   347: aload_0
    //   348: getfield mHandler : Landroid/view/Choreographer$FrameHandler;
    //   351: iconst_0
    //   352: invokevirtual obtainMessage : (I)Landroid/os/Message;
    //   355: astore #6
    //   357: aload #6
    //   359: iconst_1
    //   360: invokevirtual setAsynchronous : (Z)V
    //   363: aload_0
    //   364: getfield mHandler : Landroid/view/Choreographer$FrameHandler;
    //   367: aload #6
    //   369: invokevirtual sendMessageAtFrontOfQueue : (Landroid/os/Message;)Z
    //   372: pop
    //   373: aload_0
    //   374: invokestatic nanoTime : ()J
    //   377: putfield mLastTouchOptTimeNanos : J
    //   380: aload_0
    //   381: iconst_1
    //   382: putfield mConsumedDown : Z
    //   385: aload_0
    //   386: monitorexit
    //   387: return
    //   388: aload_0
    //   389: monitorexit
    //   390: goto -> 400
    //   393: astore #6
    //   395: aload_0
    //   396: monitorexit
    //   397: aload #6
    //   399: athrow
    //   400: getstatic android/view/Choreographer.USE_VSYNC : Z
    //   403: invokestatic shouldUseVsync : (Z)Z
    //   406: ifeq -> 452
    //   409: aload_0
    //   410: invokespecial isRunningOnLooperThreadLocked : ()Z
    //   413: ifeq -> 423
    //   416: aload_0
    //   417: invokespecial scheduleVsyncLocked : ()V
    //   420: goto -> 509
    //   423: aload_0
    //   424: getfield mHandler : Landroid/view/Choreographer$FrameHandler;
    //   427: iconst_1
    //   428: invokevirtual obtainMessage : (I)Landroid/os/Message;
    //   431: astore #6
    //   433: aload #6
    //   435: iconst_1
    //   436: invokevirtual setAsynchronous : (Z)V
    //   439: aload_0
    //   440: getfield mHandler : Landroid/view/Choreographer$FrameHandler;
    //   443: aload #6
    //   445: invokevirtual sendMessageAtFrontOfQueue : (Landroid/os/Message;)Z
    //   448: pop
    //   449: goto -> 509
    //   452: getstatic android/view/Choreographer.sFrameDelay : J
    //   455: aload_0
    //   456: getfield mLastFrameTimeNanos : J
    //   459: invokestatic getFrameDelay : (JJ)J
    //   462: putstatic android/view/Choreographer.sFrameDelay : J
    //   465: aload_0
    //   466: getfield mLastFrameTimeNanos : J
    //   469: ldc2_w 1000000
    //   472: ldiv
    //   473: getstatic android/view/Choreographer.sFrameDelay : J
    //   476: ladd
    //   477: lload_1
    //   478: invokestatic max : (JJ)J
    //   481: lstore_1
    //   482: aload_0
    //   483: getfield mHandler : Landroid/view/Choreographer$FrameHandler;
    //   486: iconst_0
    //   487: invokevirtual obtainMessage : (I)Landroid/os/Message;
    //   490: astore #6
    //   492: aload #6
    //   494: iconst_1
    //   495: invokevirtual setAsynchronous : (Z)V
    //   498: aload_0
    //   499: getfield mHandler : Landroid/view/Choreographer$FrameHandler;
    //   502: aload #6
    //   504: lload_1
    //   505: invokevirtual sendMessageAtTime : (Landroid/os/Message;J)Z
    //   508: pop
    //   509: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #699	-> 0
    //   #700	-> 7
    //   #701	-> 12
    //   #702	-> 18
    //   #703	-> 25
    //   #704	-> 29
    //   #705	-> 52
    //   #712	-> 205
    //   #713	-> 211
    //   #714	-> 213
    //   #727	-> 245
    //   #729	-> 250
    //   #730	-> 269
    //   #731	-> 279
    //   #732	-> 285
    //   #733	-> 295
    //   #734	-> 302
    //   #735	-> 307
    //   #740	-> 310
    //   #741	-> 315
    //   #742	-> 320
    //   #716	-> 323
    //   #717	-> 328
    //   #718	-> 347
    //   #719	-> 357
    //   #720	-> 363
    //   #721	-> 373
    //   #722	-> 380
    //   #723	-> 385
    //   #746	-> 388
    //   #749	-> 400
    //   #757	-> 409
    //   #758	-> 416
    //   #760	-> 423
    //   #761	-> 433
    //   #762	-> 439
    //   #763	-> 449
    //   #765	-> 452
    //   #767	-> 465
    //   #772	-> 482
    //   #773	-> 492
    //   #774	-> 498
    //   #777	-> 509
    // Exception table:
    //   from	to	target	type
    //   213	219	393	finally
    //   245	250	393	finally
    //   250	257	393	finally
    //   262	269	393	finally
    //   269	279	393	finally
    //   279	285	393	finally
    //   285	295	393	finally
    //   295	302	393	finally
    //   302	307	393	finally
    //   307	309	393	finally
    //   310	315	393	finally
    //   315	320	393	finally
    //   323	328	393	finally
    //   328	335	393	finally
    //   340	347	393	finally
    //   347	357	393	finally
    //   357	363	393	finally
    //   363	373	393	finally
    //   373	380	393	finally
    //   380	385	393	finally
    //   385	387	393	finally
    //   388	390	393	finally
    //   395	397	393	finally
  }
  
  void setFPSDivisor(int paramInt) {
    int i = paramInt;
    if (paramInt <= 0)
      i = 1; 
    this.mFPSDivisor = i;
    ThreadedRenderer.setFPSDivisor(i);
  }
  
  void doFrame(long paramLong, int paramInt) {
    Exception exception;
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      this.mIsVsyncScheduled = false;
      if (!this.mFrameScheduled) {
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return;
      } 
      long l1 = System.nanoTime();
      long l2 = l1 - paramLong;
      if (l2 >= this.mFrameIntervalNanos) {
        long l = l2 / this.mFrameIntervalNanos;
        if (l >= SKIPPED_FRAME_WARNING_LIMIT) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Skipped ");
          stringBuilder.append(l);
          stringBuilder.append(" frames!  The application may be doing too much work on its main thread.");
          Log.i("Choreographer", stringBuilder.toString());
        } 
        if (l >= SKIPPED_FRAME_THRESHOLD) {
          boolean bool;
          if (this.mCallbackQueues[1].hasCallbacks() && !this.mCallbackQueues[3].hasCallbacks()) {
            bool = true;
          } else {
            bool = false;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Skipped: ");
          stringBuilder.append(bool);
          stringBuilder.append(" ");
          stringBuilder.append(l);
          Log.p("Quality", stringBuilder.toString());
          this.mTraceMoreFrames = l;
          boolean bool1 = SystemProperties.getBoolean("debug.track_frame_skip", false);
          if (bool1) {
            if (bool) {
              paramInt = SystemProperties.getInt("debug.track_frame_skip.threshold_anim", 4);
            } else {
              paramInt = SystemProperties.getInt("debug.track_frame_skip.threshold", 15);
            } 
            if (l >= paramInt && !FrameSkipReporter.checkDuplicate(l1, l2))
              FrameSkipReporter.report(bool, l); 
          } 
        } 
        l = this.mFrameIntervalNanos;
        l2 = l1 - l2 % l;
      } else {
        l2 = paramLong;
      } 
      try {
        if (l2 < this.mLastFrameTimeNanos) {
          scheduleVsyncLocked();
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return;
        } 
        paramInt = this.mFPSDivisor;
        if (paramInt > 1)
          try {
            long l = l2 - this.mLastFrameTimeNanos;
            if (l < this.mFrameIntervalNanos * this.mFPSDivisor && l > 0L) {
              scheduleVsyncLocked();
              /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
              return;
            } 
          } finally {} 
        this.mFrameInfo.setVsync(paramLong, l2);
        this.mFrameScheduled = false;
        this.mLastFrameTimeNanos = l2;
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        try {
          String str;
          this.mIsDoFrameProcessing = true;
          object = new StringBuilder();
          super();
          object.append("Choreographer#doFrame");
          if (this.mTraceMoreFrames > 0L) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append(" ");
            stringBuilder.append(this.mTraceMoreFrames);
            str = stringBuilder.toString();
          } else {
            str = "";
          } 
          object.append(str);
          Trace.traceBegin(8L, object.toString());
          this.mTraceMoreFrames = 0L;
          BoostFramework.ScrollOptimizer.setUITaskStatus(true);
          paramLong = BoostFramework.ScrollOptimizer.getAdjustedAnimationClock(l2);
          AnimationUtils.lockAnimationClock(paramLong / 1000000L);
          this.mFrameInfo.markInputHandlingStart();
          doCallbacks(0, l2);
          this.mFrameInfo.markAnimationsStart();
          doCallbacks(1, l2);
          doCallbacks(2, l2);
          this.mFrameInfo.markPerformTraversalsStart();
          doCallbacks(3, l2);
          doCallbacks(4, l2);
          BoostFramework.ScrollOptimizer.setUITaskStatus(false);
          AnimationUtils.unlockAnimationClock();
          Trace.traceEnd(8L);
          return;
        } finally {
          AnimationUtils.unlockAnimationClock();
          Trace.traceEnd(8L);
        } 
      } finally {}
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    throw exception;
  }
  
  void doCallbacks(int paramInt, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore #4
    //   6: aload #4
    //   8: monitorenter
    //   9: invokestatic nanoTime : ()J
    //   12: lstore #5
    //   14: aload_0
    //   15: getfield mCallbackQueues : [Landroid/view/Choreographer$CallbackQueue;
    //   18: iload_1
    //   19: aaload
    //   20: lload #5
    //   22: ldc2_w 1000000
    //   25: ldiv
    //   26: invokevirtual extractDueCallbacksLocked : (J)Landroid/view/Choreographer$CallbackRecord;
    //   29: astore #7
    //   31: aload #7
    //   33: ifnonnull -> 40
    //   36: aload #4
    //   38: monitorexit
    //   39: return
    //   40: aload_0
    //   41: iconst_1
    //   42: putfield mCallbacksRunning : Z
    //   45: iload_1
    //   46: iconst_4
    //   47: if_icmpne -> 112
    //   50: lload #5
    //   52: lload_2
    //   53: lsub
    //   54: lstore #8
    //   56: ldc2_w 8
    //   59: ldc_w 'jitterNanos'
    //   62: lload #8
    //   64: l2i
    //   65: invokestatic traceCounter : (JLjava/lang/String;I)V
    //   68: lload #8
    //   70: aload_0
    //   71: getfield mFrameIntervalNanos : J
    //   74: ldc2_w 2
    //   77: lmul
    //   78: lcmp
    //   79: iflt -> 112
    //   82: aload_0
    //   83: getfield mFrameIntervalNanos : J
    //   86: lstore #10
    //   88: aload_0
    //   89: getfield mFrameIntervalNanos : J
    //   92: lstore_2
    //   93: lload #5
    //   95: lload #8
    //   97: lload #10
    //   99: lrem
    //   100: lload_2
    //   101: ladd
    //   102: lsub
    //   103: lstore_2
    //   104: aload_0
    //   105: lload_2
    //   106: putfield mLastFrameTimeNanos : J
    //   109: goto -> 112
    //   112: aload #4
    //   114: monitorexit
    //   115: ldc2_w 8
    //   118: getstatic android/view/Choreographer.CALLBACK_TRACE_TITLES : [Ljava/lang/String;
    //   121: iload_1
    //   122: aaload
    //   123: invokestatic traceBegin : (JLjava/lang/String;)V
    //   126: aload #7
    //   128: astore #4
    //   130: aload #4
    //   132: ifnull -> 151
    //   135: aload #4
    //   137: lload_2
    //   138: invokevirtual run : (J)V
    //   141: aload #4
    //   143: getfield next : Landroid/view/Choreographer$CallbackRecord;
    //   146: astore #4
    //   148: goto -> 130
    //   151: aload_0
    //   152: getfield mLock : Ljava/lang/Object;
    //   155: astore #12
    //   157: aload #12
    //   159: monitorenter
    //   160: aload_0
    //   161: iconst_0
    //   162: putfield mCallbacksRunning : Z
    //   165: aload #7
    //   167: getfield next : Landroid/view/Choreographer$CallbackRecord;
    //   170: astore #4
    //   172: aload_0
    //   173: aload #7
    //   175: invokespecial recycleCallbackLocked : (Landroid/view/Choreographer$CallbackRecord;)V
    //   178: aload #4
    //   180: astore #7
    //   182: aload #4
    //   184: ifnonnull -> 165
    //   187: aload #12
    //   189: monitorexit
    //   190: ldc2_w 8
    //   193: invokestatic traceEnd : (J)V
    //   196: return
    //   197: astore #7
    //   199: aload #12
    //   201: monitorexit
    //   202: aload #7
    //   204: athrow
    //   205: astore #13
    //   207: aload_0
    //   208: getfield mLock : Ljava/lang/Object;
    //   211: astore #12
    //   213: aload #12
    //   215: monitorenter
    //   216: aload_0
    //   217: iconst_0
    //   218: putfield mCallbacksRunning : Z
    //   221: aload #7
    //   223: getfield next : Landroid/view/Choreographer$CallbackRecord;
    //   226: astore #4
    //   228: aload_0
    //   229: aload #7
    //   231: invokespecial recycleCallbackLocked : (Landroid/view/Choreographer$CallbackRecord;)V
    //   234: aload #4
    //   236: astore #7
    //   238: aload #7
    //   240: ifnull -> 246
    //   243: goto -> 221
    //   246: aload #12
    //   248: monitorexit
    //   249: ldc2_w 8
    //   252: invokestatic traceEnd : (J)V
    //   255: aload #13
    //   257: athrow
    //   258: astore #7
    //   260: aload #12
    //   262: monitorexit
    //   263: aload #7
    //   265: athrow
    //   266: astore #7
    //   268: aload #4
    //   270: monitorexit
    //   271: aload #7
    //   273: athrow
    //   274: astore #7
    //   276: goto -> 268
    // Line number table:
    //   Java source line number -> byte code offset
    //   #912	-> 0
    //   #916	-> 9
    //   #917	-> 14
    //   #919	-> 31
    //   #920	-> 36
    //   #922	-> 40
    //   #932	-> 45
    //   #933	-> 50
    //   #934	-> 56
    //   #935	-> 68
    //   #936	-> 82
    //   #946	-> 93
    //   #947	-> 104
    //   #950	-> 112
    //   #952	-> 115
    //   #953	-> 126
    //   #959	-> 135
    //   #953	-> 141
    //   #962	-> 151
    //   #963	-> 160
    //   #965	-> 165
    //   #966	-> 172
    //   #967	-> 178
    //   #968	-> 178
    //   #969	-> 187
    //   #970	-> 190
    //   #971	-> 196
    //   #972	-> 196
    //   #969	-> 197
    //   #962	-> 205
    //   #963	-> 216
    //   #965	-> 221
    //   #966	-> 228
    //   #967	-> 234
    //   #968	-> 238
    //   #969	-> 246
    //   #970	-> 249
    //   #971	-> 255
    //   #969	-> 258
    //   #950	-> 266
    // Exception table:
    //   from	to	target	type
    //   9	14	266	finally
    //   14	31	266	finally
    //   36	39	266	finally
    //   40	45	266	finally
    //   56	68	266	finally
    //   68	82	266	finally
    //   82	93	266	finally
    //   104	109	274	finally
    //   112	115	274	finally
    //   115	126	205	finally
    //   135	141	205	finally
    //   141	148	205	finally
    //   160	165	197	finally
    //   165	172	197	finally
    //   172	178	197	finally
    //   187	190	197	finally
    //   199	202	197	finally
    //   216	221	258	finally
    //   221	228	258	finally
    //   228	234	258	finally
    //   246	249	258	finally
    //   260	263	258	finally
    //   268	271	274	finally
  }
  
  void doScheduleVsync() {
    synchronized (this.mLock) {
      if (this.mFrameScheduled)
        scheduleVsyncLocked(); 
      return;
    } 
  }
  
  void doScheduleCallback(int paramInt) {
    synchronized (this.mLock) {
      if (!this.mFrameScheduled) {
        long l = SystemClock.uptimeMillis();
        if (this.mCallbackQueues[paramInt].hasDueCallbacksLocked(l))
          scheduleFrameLocked(l); 
      } 
      return;
    } 
  }
  
  private void scheduleVsyncLocked() {
    this.mDisplayEventReceiver.scheduleVsync();
    this.mIsVsyncScheduled = true;
  }
  
  private boolean isRunningOnLooperThreadLocked() {
    boolean bool;
    if (Looper.myLooper() == this.mLooper) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private CallbackRecord obtainCallbackLocked(long paramLong, Object paramObject1, Object paramObject2) {
    CallbackRecord callbackRecord = this.mCallbackPool;
    if (callbackRecord == null) {
      callbackRecord = new CallbackRecord();
    } else {
      this.mCallbackPool = callbackRecord.next;
      callbackRecord.next = null;
    } 
    callbackRecord.dueTime = paramLong;
    callbackRecord.action = paramObject1;
    callbackRecord.token = paramObject2;
    return callbackRecord;
  }
  
  private void recycleCallbackLocked(CallbackRecord paramCallbackRecord) {
    paramCallbackRecord.action = null;
    paramCallbackRecord.token = null;
    paramCallbackRecord.next = this.mCallbackPool;
    this.mCallbackPool = paramCallbackRecord;
  }
  
  class FrameHandler extends Handler {
    final Choreographer this$0;
    
    public FrameHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 0) {
        if (i != 1) {
          if (i == 2)
            Choreographer.this.doScheduleCallback(param1Message.arg1); 
        } else {
          Choreographer.this.doScheduleVsync();
        } 
      } else {
        Choreographer.this.doFrame(System.nanoTime(), 0);
      } 
    }
  }
  
  class FrameDisplayEventReceiver extends DisplayEventReceiver implements Runnable {
    private int mFrame;
    
    private boolean mHavePendingVsync;
    
    private long mTimestampNanos;
    
    final Choreographer this$0;
    
    public FrameDisplayEventReceiver(Looper param1Looper, int param1Int) {
      super(param1Looper, param1Int, 0);
    }
    
    public void onVsync(long param1Long1, long param1Long2, int param1Int) {
      long l = System.nanoTime();
      param1Long2 = param1Long1;
      if (param1Long1 > l) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Frame time is ");
        stringBuilder.append((float)(param1Long1 - l) * 1.0E-6F);
        stringBuilder.append(" ms in the future!  Check that graphics HAL is generating vsync timestamps using the correct timebase.");
        Log.w("Choreographer", stringBuilder.toString());
        param1Long2 = l;
      } 
      if (this.mHavePendingVsync) {
        Log.w("Choreographer", "Already have a pending vsync event.  There should only be one at a time.");
      } else {
        this.mHavePendingVsync = true;
      } 
      this.mTimestampNanos = param1Long2;
      this.mFrame = param1Int;
      BoostFramework.ScrollOptimizer.setVsyncTime(param1Long2);
      Message message = Message.obtain(Choreographer.this.mHandler, this);
      message.setAsynchronous(true);
      Choreographer.this.mHandler.sendMessageAtTime(message, param1Long2 / 1000000L);
    }
    
    public void run() {
      this.mHavePendingVsync = false;
      Choreographer.this.doFrame(this.mTimestampNanos, this.mFrame);
    }
  }
  
  private static final class CallbackRecord {
    public Object action;
    
    public long dueTime;
    
    public CallbackRecord next;
    
    public Object token;
    
    private CallbackRecord() {}
    
    public void run(long param1Long) {
      if (this.token == Choreographer.FRAME_CALLBACK_TOKEN) {
        ((Choreographer.FrameCallback)this.action).doFrame(param1Long);
      } else {
        ((Runnable)this.action).run();
      } 
    }
  }
  
  private final class CallbackQueue {
    private Choreographer.CallbackRecord mHead;
    
    final Choreographer this$0;
    
    private CallbackQueue() {}
    
    public boolean hasCallbacks() {
      boolean bool;
      if (this.mHead != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean hasDueCallbacksLocked(long param1Long) {
      boolean bool;
      Choreographer.CallbackRecord callbackRecord = this.mHead;
      if (callbackRecord != null && callbackRecord.dueTime <= param1Long) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public Choreographer.CallbackRecord extractDueCallbacksLocked(long param1Long) {
      Choreographer.CallbackRecord callbackRecord1 = this.mHead;
      if (callbackRecord1 == null || callbackRecord1.dueTime > param1Long)
        return null; 
      Choreographer.CallbackRecord callbackRecord2 = callbackRecord1;
      Choreographer.CallbackRecord callbackRecord3 = callbackRecord2.next;
      while (callbackRecord3 != null) {
        if (callbackRecord3.dueTime > param1Long) {
          callbackRecord2.next = null;
          break;
        } 
        callbackRecord2 = callbackRecord3;
        callbackRecord3 = callbackRecord3.next;
      } 
      this.mHead = callbackRecord3;
      return callbackRecord1;
    }
    
    public void addCallbackLocked(long param1Long, Object param1Object1, Object param1Object2) {
      Choreographer.CallbackRecord callbackRecord = Choreographer.this.obtainCallbackLocked(param1Long, param1Object1, param1Object2);
      param1Object2 = this.mHead;
      if (param1Object2 == null) {
        this.mHead = callbackRecord;
        return;
      } 
      param1Object1 = param1Object2;
      if (param1Long < ((Choreographer.CallbackRecord)param1Object2).dueTime) {
        callbackRecord.next = (Choreographer.CallbackRecord)param1Object2;
        this.mHead = callbackRecord;
        return;
      } 
      while (((Choreographer.CallbackRecord)param1Object1).next != null) {
        if (param1Long < ((Choreographer.CallbackRecord)param1Object1).next.dueTime) {
          callbackRecord.next = ((Choreographer.CallbackRecord)param1Object1).next;
          break;
        } 
        param1Object1 = ((Choreographer.CallbackRecord)param1Object1).next;
      } 
      ((Choreographer.CallbackRecord)param1Object1).next = callbackRecord;
    }
    
    public void removeCallbacksLocked(Object param1Object1, Object param1Object2) {
      Choreographer.CallbackRecord callbackRecord1 = null;
      for (Choreographer.CallbackRecord callbackRecord2 = this.mHead; callbackRecord2 != null; ) {
        Choreographer.CallbackRecord callbackRecord = callbackRecord2.next;
        if ((param1Object1 == null || callbackRecord2.action == param1Object1) && (param1Object2 == null || callbackRecord2.token == param1Object2)) {
          if (callbackRecord1 != null) {
            callbackRecord1.next = callbackRecord;
          } else {
            this.mHead = callbackRecord;
          } 
          Choreographer.this.recycleCallbackLocked(callbackRecord2);
        } else {
          callbackRecord1 = callbackRecord2;
        } 
        callbackRecord2 = callbackRecord;
      } 
    }
  }
  
  public void setIsSFChoregrapher(boolean paramBoolean) {
    this.sIsSFChoregrapher = paramBoolean;
  }
  
  public static interface FrameCallback {
    void doFrame(long param1Long);
  }
}
