package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.window.WindowContainerTransaction;

public interface IDisplayWindowRotationCallback extends IInterface {
  void continueRotateDisplay(int paramInt, WindowContainerTransaction paramWindowContainerTransaction) throws RemoteException;
  
  class Default implements IDisplayWindowRotationCallback {
    public void continueRotateDisplay(int param1Int, WindowContainerTransaction param1WindowContainerTransaction) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDisplayWindowRotationCallback {
    private static final String DESCRIPTOR = "android.view.IDisplayWindowRotationCallback";
    
    static final int TRANSACTION_continueRotateDisplay = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IDisplayWindowRotationCallback");
    }
    
    public static IDisplayWindowRotationCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IDisplayWindowRotationCallback");
      if (iInterface != null && iInterface instanceof IDisplayWindowRotationCallback)
        return (IDisplayWindowRotationCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "continueRotateDisplay";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IDisplayWindowRotationCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IDisplayWindowRotationCallback");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        WindowContainerTransaction windowContainerTransaction = (WindowContainerTransaction)WindowContainerTransaction.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      continueRotateDisplay(param1Int1, (WindowContainerTransaction)param1Parcel1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IDisplayWindowRotationCallback {
      public static IDisplayWindowRotationCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IDisplayWindowRotationCallback";
      }
      
      public void continueRotateDisplay(int param2Int, WindowContainerTransaction param2WindowContainerTransaction) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IDisplayWindowRotationCallback");
          parcel1.writeInt(param2Int);
          if (param2WindowContainerTransaction != null) {
            parcel1.writeInt(1);
            param2WindowContainerTransaction.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDisplayWindowRotationCallback.Stub.getDefaultImpl() != null) {
            IDisplayWindowRotationCallback.Stub.getDefaultImpl().continueRotateDisplay(param2Int, param2WindowContainerTransaction);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDisplayWindowRotationCallback param1IDisplayWindowRotationCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDisplayWindowRotationCallback != null) {
          Proxy.sDefaultImpl = param1IDisplayWindowRotationCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDisplayWindowRotationCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
