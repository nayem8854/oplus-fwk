package android.view;

import android.graphics.CanvasProperty;
import android.graphics.Paint;
import android.graphics.animation.RenderNodeAnimator;

public class RenderNodeAnimator extends RenderNodeAnimator implements RenderNodeAnimator.ViewListener {
  private View mViewTarget;
  
  public RenderNodeAnimator(int paramInt, float paramFloat) {
    super(paramInt, paramFloat);
  }
  
  public RenderNodeAnimator(CanvasProperty<Float> paramCanvasProperty, float paramFloat) {
    super(paramCanvasProperty, paramFloat);
  }
  
  public RenderNodeAnimator(CanvasProperty<Paint> paramCanvasProperty, int paramInt, float paramFloat) {
    super(paramCanvasProperty, paramInt, paramFloat);
  }
  
  public RenderNodeAnimator(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2) {
    super(paramInt1, paramInt2, paramFloat1, paramFloat2);
  }
  
  public void onAlphaAnimationStart(float paramFloat) {
    this.mViewTarget.ensureTransformationInfo();
    this.mViewTarget.setAlphaInternal(paramFloat);
  }
  
  public void invalidateParent(boolean paramBoolean) {
    this.mViewTarget.invalidateViewProperty(true, false);
  }
  
  public void setTarget(View paramView) {
    this.mViewTarget = paramView;
    setViewListener(this);
    setTarget(this.mViewTarget.mRenderNode);
  }
}
