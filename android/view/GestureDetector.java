package android.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.Log;
import com.android.internal.util.FrameworkStatsLog;

public class GestureDetector {
  private static final int DOUBLE_TAP_MIN_TIME;
  
  private static final int DOUBLE_TAP_TIMEOUT;
  
  private static final int LONGPRESS_TIMEOUT;
  
  private static final int LONG_PRESS = 2;
  
  private static final int SHOW_PRESS = 1;
  
  class SimpleOnGestureListener implements OnGestureListener, OnDoubleTapListener, OnContextClickListener {
    public boolean onSingleTapUp(MotionEvent param1MotionEvent) {
      return false;
    }
    
    public void onLongPress(MotionEvent param1MotionEvent) {}
    
    public boolean onScroll(MotionEvent param1MotionEvent1, MotionEvent param1MotionEvent2, float param1Float1, float param1Float2) {
      return false;
    }
    
    public boolean onFling(MotionEvent param1MotionEvent1, MotionEvent param1MotionEvent2, float param1Float1, float param1Float2) {
      return false;
    }
    
    public void onShowPress(MotionEvent param1MotionEvent) {}
    
    public boolean onDown(MotionEvent param1MotionEvent) {
      return false;
    }
    
    public boolean onDoubleTap(MotionEvent param1MotionEvent) {
      return false;
    }
    
    public boolean onDoubleTapEvent(MotionEvent param1MotionEvent) {
      return false;
    }
    
    public boolean onSingleTapConfirmed(MotionEvent param1MotionEvent) {
      return false;
    }
    
    public boolean onContextClick(MotionEvent param1MotionEvent) {
      return false;
    }
  }
  
  private static final String TAG = GestureDetector.class.getSimpleName();
  
  private static final int TAP = 3;
  
  private static final int TAP_TIMEOUT;
  
  private boolean mAlwaysInBiggerTapRegion;
  
  private boolean mAlwaysInTapRegion;
  
  private float mAmbiguousGestureMultiplier;
  
  private OnContextClickListener mContextClickListener;
  
  private MotionEvent mCurrentDownEvent;
  
  private MotionEvent mCurrentMotionEvent;
  
  private boolean mDeferConfirmSingleTap;
  
  private OnDoubleTapListener mDoubleTapListener;
  
  private int mDoubleTapSlopSquare;
  
  private int mDoubleTapTouchSlopSquare;
  
  private float mDownFocusX;
  
  private float mDownFocusY;
  
  private final Handler mHandler;
  
  private boolean mHasRecordedClassification;
  
  private boolean mIgnoreNextUpEvent;
  
  private boolean mInContextClick;
  
  private boolean mInLongPress;
  
  private final InputEventConsistencyVerifier mInputEventConsistencyVerifier;
  
  private boolean mIsDoubleTapping;
  
  private boolean mIsLongpressEnabled;
  
  private float mLastFocusX;
  
  private float mLastFocusY;
  
  private final OnGestureListener mListener;
  
  private int mMaximumFlingVelocity;
  
  private int mMinimumFlingVelocity;
  
  private MotionEvent mPreviousUpEvent;
  
  private boolean mStillDown;
  
  private int mTouchSlopSquare;
  
  private VelocityTracker mVelocityTracker;
  
  static {
    LONGPRESS_TIMEOUT = ViewConfiguration.getLongPressTimeout();
    TAP_TIMEOUT = ViewConfiguration.getTapTimeout();
    DOUBLE_TAP_TIMEOUT = ViewConfiguration.getDoubleTapTimeout();
    DOUBLE_TAP_MIN_TIME = ViewConfiguration.getDoubleTapMinTime();
  }
  
  public static interface OnGestureListener {
    boolean onDown(MotionEvent param1MotionEvent);
    
    boolean onFling(MotionEvent param1MotionEvent1, MotionEvent param1MotionEvent2, float param1Float1, float param1Float2);
    
    void onLongPress(MotionEvent param1MotionEvent);
    
    boolean onScroll(MotionEvent param1MotionEvent1, MotionEvent param1MotionEvent2, float param1Float1, float param1Float2);
    
    void onShowPress(MotionEvent param1MotionEvent);
    
    boolean onSingleTapUp(MotionEvent param1MotionEvent);
  }
  
  public static interface OnDoubleTapListener {
    boolean onDoubleTap(MotionEvent param1MotionEvent);
    
    boolean onDoubleTapEvent(MotionEvent param1MotionEvent);
    
    boolean onSingleTapConfirmed(MotionEvent param1MotionEvent);
  }
  
  public static interface OnContextClickListener {
    boolean onContextClick(MotionEvent param1MotionEvent);
  }
  
  class GestureHandler extends Handler {
    final GestureDetector this$0;
    
    GestureHandler() {}
    
    GestureHandler(Handler param1Handler) {
      super(param1Handler.getLooper());
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        if (i != 2) {
          if (i == 3) {
            if (GestureDetector.this.mDoubleTapListener != null)
              if (!GestureDetector.this.mStillDown) {
                GestureDetector.this.recordGestureClassification(1);
                GestureDetector.this.mDoubleTapListener.onSingleTapConfirmed(GestureDetector.this.mCurrentDownEvent);
              } else {
                GestureDetector.access$602(GestureDetector.this, true);
              }  
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown message ");
            stringBuilder.append(param1Message);
            throw new RuntimeException(stringBuilder.toString());
          } 
        } else {
          GestureDetector.this.recordGestureClassification(param1Message.arg1);
          GestureDetector.this.dispatchLongPress();
        } 
      } else {
        GestureDetector.this.mListener.onShowPress(GestureDetector.this.mCurrentDownEvent);
      } 
    }
  }
  
  @Deprecated
  public GestureDetector(OnGestureListener paramOnGestureListener, Handler paramHandler) {
    this(null, paramOnGestureListener, paramHandler);
  }
  
  @Deprecated
  public GestureDetector(OnGestureListener paramOnGestureListener) {
    this(null, paramOnGestureListener, null);
  }
  
  public GestureDetector(Context paramContext, OnGestureListener paramOnGestureListener) {
    this(paramContext, paramOnGestureListener, null);
  }
  
  public GestureDetector(Context paramContext, OnGestureListener paramOnGestureListener, Handler paramHandler) {
    InputEventConsistencyVerifier inputEventConsistencyVerifier;
    if (InputEventConsistencyVerifier.isInstrumentationEnabled()) {
      inputEventConsistencyVerifier = new InputEventConsistencyVerifier(this, 0);
    } else {
      inputEventConsistencyVerifier = null;
    } 
    this.mInputEventConsistencyVerifier = inputEventConsistencyVerifier;
    if (paramHandler != null) {
      this.mHandler = new GestureHandler(paramHandler);
    } else {
      this.mHandler = new GestureHandler();
    } 
    this.mListener = paramOnGestureListener;
    if (paramOnGestureListener instanceof OnDoubleTapListener)
      setOnDoubleTapListener((OnDoubleTapListener)paramOnGestureListener); 
    if (paramOnGestureListener instanceof OnContextClickListener)
      setContextClickListener((OnContextClickListener)paramOnGestureListener); 
    init(paramContext);
  }
  
  public GestureDetector(Context paramContext, OnGestureListener paramOnGestureListener, Handler paramHandler, boolean paramBoolean) {
    this(paramContext, paramOnGestureListener, paramHandler);
  }
  
  private void init(Context paramContext) {
    if (this.mListener != null) {
      int i, j, k;
      this.mIsLongpressEnabled = true;
      if (paramContext == null) {
        i = ViewConfiguration.getTouchSlop();
        j = i;
        k = ViewConfiguration.getDoubleTapSlop();
        this.mMinimumFlingVelocity = ViewConfiguration.getMinimumFlingVelocity();
        this.mMaximumFlingVelocity = ViewConfiguration.getMaximumFlingVelocity();
        this.mAmbiguousGestureMultiplier = ViewConfiguration.getAmbiguousGestureMultiplier();
      } else {
        if (!paramContext.isUiContext() && StrictMode.vmIncorrectContextUseEnabled()) {
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Tried to access UI constants from a non-visual Context.");
          StrictMode.onIncorrectContextUsed("GestureDetector must be accessed from Activity or other visual Context. Use an Activity or a Context created with Context#createWindowContext(int, Bundle), which are adjusted to the configuration and visual bounds of an area on screen.", illegalArgumentException);
          Log.e(TAG, "Tried to access UI constants from a non-visual Context.GestureDetector must be accessed from Activity or other visual Context. Use an Activity or a Context created with Context#createWindowContext(int, Bundle), which are adjusted to the configuration and visual bounds of an area on screen.", illegalArgumentException);
        } 
        ViewConfiguration viewConfiguration = ViewConfiguration.get(paramContext);
        i = viewConfiguration.getScaledTouchSlop();
        j = viewConfiguration.getScaledDoubleTapTouchSlop();
        k = viewConfiguration.getScaledDoubleTapSlop();
        this.mMinimumFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaximumFlingVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
        this.mAmbiguousGestureMultiplier = viewConfiguration.getScaledAmbiguousGestureMultiplier();
      } 
      this.mTouchSlopSquare = i * i;
      this.mDoubleTapTouchSlopSquare = j * j;
      this.mDoubleTapSlopSquare = k * k;
      return;
    } 
    throw new NullPointerException("OnGestureListener must not be null");
  }
  
  public void setOnDoubleTapListener(OnDoubleTapListener paramOnDoubleTapListener) {
    this.mDoubleTapListener = paramOnDoubleTapListener;
  }
  
  public void setContextClickListener(OnContextClickListener paramOnContextClickListener) {
    this.mContextClickListener = paramOnContextClickListener;
  }
  
  public void setIsLongpressEnabled(boolean paramBoolean) {
    this.mIsLongpressEnabled = paramBoolean;
  }
  
  public boolean isLongpressEnabled() {
    return this.mIsLongpressEnabled;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInputEventConsistencyVerifier : Landroid/view/InputEventConsistencyVerifier;
    //   4: astore_2
    //   5: aload_2
    //   6: ifnull -> 15
    //   9: aload_2
    //   10: aload_1
    //   11: iconst_0
    //   12: invokevirtual onTouchEvent : (Landroid/view/MotionEvent;I)V
    //   15: aload_1
    //   16: invokevirtual getAction : ()I
    //   19: istore_3
    //   20: aload_0
    //   21: getfield mCurrentMotionEvent : Landroid/view/MotionEvent;
    //   24: astore_2
    //   25: aload_2
    //   26: ifnull -> 33
    //   29: aload_2
    //   30: invokevirtual recycle : ()V
    //   33: aload_0
    //   34: aload_1
    //   35: invokestatic obtain : (Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    //   38: putfield mCurrentMotionEvent : Landroid/view/MotionEvent;
    //   41: aload_0
    //   42: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   45: ifnonnull -> 55
    //   48: aload_0
    //   49: invokestatic obtain : ()Landroid/view/VelocityTracker;
    //   52: putfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   55: aload_0
    //   56: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   59: aload_1
    //   60: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   63: iload_3
    //   64: sipush #255
    //   67: iand
    //   68: bipush #6
    //   70: if_icmpne -> 79
    //   73: iconst_1
    //   74: istore #4
    //   76: goto -> 82
    //   79: iconst_0
    //   80: istore #4
    //   82: iload #4
    //   84: ifeq -> 96
    //   87: aload_1
    //   88: invokevirtual getActionIndex : ()I
    //   91: istore #5
    //   93: goto -> 99
    //   96: iconst_m1
    //   97: istore #5
    //   99: aload_1
    //   100: invokevirtual getFlags : ()I
    //   103: bipush #8
    //   105: iand
    //   106: ifeq -> 115
    //   109: iconst_1
    //   110: istore #6
    //   112: goto -> 118
    //   115: iconst_0
    //   116: istore #6
    //   118: fconst_0
    //   119: fstore #7
    //   121: fconst_0
    //   122: fstore #8
    //   124: aload_1
    //   125: invokevirtual getPointerCount : ()I
    //   128: istore #9
    //   130: iconst_0
    //   131: istore #10
    //   133: iload #10
    //   135: iload #9
    //   137: if_icmpge -> 178
    //   140: iload #5
    //   142: iload #10
    //   144: if_icmpne -> 150
    //   147: goto -> 172
    //   150: fload #7
    //   152: aload_1
    //   153: iload #10
    //   155: invokevirtual getX : (I)F
    //   158: fadd
    //   159: fstore #7
    //   161: fload #8
    //   163: aload_1
    //   164: iload #10
    //   166: invokevirtual getY : (I)F
    //   169: fadd
    //   170: fstore #8
    //   172: iinc #10, 1
    //   175: goto -> 133
    //   178: iload #4
    //   180: ifeq -> 192
    //   183: iload #9
    //   185: iconst_1
    //   186: isub
    //   187: istore #4
    //   189: goto -> 196
    //   192: iload #9
    //   194: istore #4
    //   196: fload #7
    //   198: iload #4
    //   200: i2f
    //   201: fdiv
    //   202: fstore #7
    //   204: fload #8
    //   206: iload #4
    //   208: i2f
    //   209: fdiv
    //   210: fstore #11
    //   212: iload_3
    //   213: sipush #255
    //   216: iand
    //   217: istore #4
    //   219: iload #4
    //   221: ifeq -> 1294
    //   224: iload #4
    //   226: iconst_1
    //   227: if_icmpeq -> 977
    //   230: iload #4
    //   232: iconst_2
    //   233: if_icmpeq -> 456
    //   236: iload #4
    //   238: iconst_3
    //   239: if_icmpeq -> 449
    //   242: iload #4
    //   244: iconst_5
    //   245: if_icmpeq -> 418
    //   248: iload #4
    //   250: bipush #6
    //   252: if_icmpeq -> 258
    //   255: goto -> 971
    //   258: aload_0
    //   259: fload #7
    //   261: putfield mLastFocusX : F
    //   264: aload_0
    //   265: fload #7
    //   267: putfield mDownFocusX : F
    //   270: aload_0
    //   271: fload #11
    //   273: putfield mLastFocusY : F
    //   276: aload_0
    //   277: fload #11
    //   279: putfield mDownFocusY : F
    //   282: aload_0
    //   283: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   286: sipush #1000
    //   289: aload_0
    //   290: getfield mMaximumFlingVelocity : I
    //   293: i2f
    //   294: invokevirtual computeCurrentVelocity : (IF)V
    //   297: aload_1
    //   298: invokevirtual getActionIndex : ()I
    //   301: istore #6
    //   303: aload_1
    //   304: iload #6
    //   306: invokevirtual getPointerId : (I)I
    //   309: istore #4
    //   311: aload_0
    //   312: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   315: iload #4
    //   317: invokevirtual getXVelocity : (I)F
    //   320: fstore #8
    //   322: aload_0
    //   323: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   326: iload #4
    //   328: invokevirtual getYVelocity : (I)F
    //   331: fstore #7
    //   333: iconst_0
    //   334: istore #5
    //   336: iload #5
    //   338: iload #9
    //   340: if_icmpge -> 415
    //   343: iload #5
    //   345: iload #6
    //   347: if_icmpne -> 353
    //   350: goto -> 409
    //   353: aload_1
    //   354: iload #5
    //   356: invokevirtual getPointerId : (I)I
    //   359: istore #10
    //   361: aload_0
    //   362: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   365: iload #10
    //   367: invokevirtual getXVelocity : (I)F
    //   370: fstore #12
    //   372: aload_0
    //   373: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   376: iload #10
    //   378: invokevirtual getYVelocity : (I)F
    //   381: fstore #13
    //   383: fload #12
    //   385: fload #8
    //   387: fmul
    //   388: fload #13
    //   390: fload #7
    //   392: fmul
    //   393: fadd
    //   394: fconst_0
    //   395: fcmpg
    //   396: ifge -> 409
    //   399: aload_0
    //   400: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   403: invokevirtual clear : ()V
    //   406: goto -> 415
    //   409: iinc #5, 1
    //   412: goto -> 336
    //   415: goto -> 971
    //   418: aload_0
    //   419: fload #7
    //   421: putfield mLastFocusX : F
    //   424: aload_0
    //   425: fload #7
    //   427: putfield mDownFocusX : F
    //   430: aload_0
    //   431: fload #11
    //   433: putfield mLastFocusY : F
    //   436: aload_0
    //   437: fload #11
    //   439: putfield mDownFocusY : F
    //   442: aload_0
    //   443: invokespecial cancelTaps : ()V
    //   446: goto -> 971
    //   449: aload_0
    //   450: invokespecial cancel : ()V
    //   453: goto -> 971
    //   456: aload_0
    //   457: getfield mInLongPress : Z
    //   460: ifne -> 971
    //   463: aload_0
    //   464: getfield mInContextClick : Z
    //   467: ifeq -> 473
    //   470: goto -> 971
    //   473: aload_1
    //   474: invokevirtual getClassification : ()I
    //   477: istore #10
    //   479: aload_0
    //   480: getfield mHandler : Landroid/os/Handler;
    //   483: iconst_2
    //   484: invokevirtual hasMessages : (I)Z
    //   487: istore #14
    //   489: aload_0
    //   490: getfield mLastFocusX : F
    //   493: fload #7
    //   495: fsub
    //   496: fstore #12
    //   498: aload_0
    //   499: getfield mLastFocusY : F
    //   502: fload #11
    //   504: fsub
    //   505: fstore #13
    //   507: aload_0
    //   508: getfield mIsDoubleTapping : Z
    //   511: ifeq -> 536
    //   514: aload_0
    //   515: iconst_2
    //   516: invokespecial recordGestureClassification : (I)V
    //   519: aload_0
    //   520: getfield mDoubleTapListener : Landroid/view/GestureDetector$OnDoubleTapListener;
    //   523: aload_1
    //   524: invokeinterface onDoubleTapEvent : (Landroid/view/MotionEvent;)Z
    //   529: iconst_0
    //   530: ior
    //   531: istore #15
    //   533: goto -> 902
    //   536: aload_0
    //   537: getfield mAlwaysInTapRegion : Z
    //   540: ifeq -> 836
    //   543: fload #7
    //   545: aload_0
    //   546: getfield mDownFocusX : F
    //   549: fsub
    //   550: f2i
    //   551: istore #5
    //   553: fload #11
    //   555: aload_0
    //   556: getfield mDownFocusY : F
    //   559: fsub
    //   560: f2i
    //   561: istore #4
    //   563: iload #5
    //   565: iload #5
    //   567: imul
    //   568: iload #4
    //   570: iload #4
    //   572: imul
    //   573: iadd
    //   574: istore #9
    //   576: iload #6
    //   578: ifeq -> 587
    //   581: iconst_0
    //   582: istore #4
    //   584: goto -> 593
    //   587: aload_0
    //   588: getfield mTouchSlopSquare : I
    //   591: istore #4
    //   593: iload #10
    //   595: iconst_1
    //   596: if_icmpne -> 605
    //   599: iconst_1
    //   600: istore #5
    //   602: goto -> 608
    //   605: iconst_0
    //   606: istore #5
    //   608: iload #14
    //   610: ifeq -> 624
    //   613: iload #5
    //   615: ifeq -> 624
    //   618: iconst_1
    //   619: istore #5
    //   621: goto -> 627
    //   624: iconst_0
    //   625: istore #5
    //   627: iload #5
    //   629: ifeq -> 725
    //   632: iload #9
    //   634: iload #4
    //   636: if_icmple -> 700
    //   639: aload_0
    //   640: getfield mHandler : Landroid/os/Handler;
    //   643: iconst_2
    //   644: invokevirtual removeMessages : (I)V
    //   647: invokestatic getLongPressTimeout : ()I
    //   650: i2l
    //   651: lstore #16
    //   653: aload_0
    //   654: getfield mHandler : Landroid/os/Handler;
    //   657: astore #18
    //   659: aload #18
    //   661: iconst_2
    //   662: iconst_3
    //   663: iconst_0
    //   664: invokevirtual obtainMessage : (III)Landroid/os/Message;
    //   667: astore_2
    //   668: aload_1
    //   669: invokevirtual getDownTime : ()J
    //   672: lstore #19
    //   674: lload #16
    //   676: l2f
    //   677: aload_0
    //   678: getfield mAmbiguousGestureMultiplier : F
    //   681: fmul
    //   682: f2l
    //   683: lstore #16
    //   685: aload #18
    //   687: aload_2
    //   688: lload #19
    //   690: lload #16
    //   692: ladd
    //   693: invokevirtual sendMessageAtTime : (Landroid/os/Message;J)Z
    //   696: pop
    //   697: goto -> 700
    //   700: iload #4
    //   702: i2f
    //   703: fstore #21
    //   705: aload_0
    //   706: getfield mAmbiguousGestureMultiplier : F
    //   709: fstore #8
    //   711: fload #21
    //   713: fload #8
    //   715: fload #8
    //   717: fmul
    //   718: fmul
    //   719: f2i
    //   720: istore #4
    //   722: goto -> 725
    //   725: iload #9
    //   727: iload #4
    //   729: if_icmple -> 801
    //   732: aload_0
    //   733: iconst_5
    //   734: invokespecial recordGestureClassification : (I)V
    //   737: aload_0
    //   738: getfield mListener : Landroid/view/GestureDetector$OnGestureListener;
    //   741: aload_0
    //   742: getfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   745: aload_1
    //   746: fload #12
    //   748: fload #13
    //   750: invokeinterface onScroll : (Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    //   755: istore #15
    //   757: aload_0
    //   758: fload #7
    //   760: putfield mLastFocusX : F
    //   763: aload_0
    //   764: fload #11
    //   766: putfield mLastFocusY : F
    //   769: aload_0
    //   770: iconst_0
    //   771: putfield mAlwaysInTapRegion : Z
    //   774: aload_0
    //   775: getfield mHandler : Landroid/os/Handler;
    //   778: iconst_3
    //   779: invokevirtual removeMessages : (I)V
    //   782: aload_0
    //   783: getfield mHandler : Landroid/os/Handler;
    //   786: iconst_1
    //   787: invokevirtual removeMessages : (I)V
    //   790: aload_0
    //   791: getfield mHandler : Landroid/os/Handler;
    //   794: iconst_2
    //   795: invokevirtual removeMessages : (I)V
    //   798: goto -> 804
    //   801: iconst_0
    //   802: istore #15
    //   804: iload #6
    //   806: ifeq -> 815
    //   809: iconst_0
    //   810: istore #4
    //   812: goto -> 821
    //   815: aload_0
    //   816: getfield mDoubleTapTouchSlopSquare : I
    //   819: istore #4
    //   821: iload #9
    //   823: iload #4
    //   825: if_icmple -> 833
    //   828: aload_0
    //   829: iconst_0
    //   830: putfield mAlwaysInBiggerTapRegion : Z
    //   833: goto -> 902
    //   836: fload #12
    //   838: invokestatic abs : (F)F
    //   841: fconst_1
    //   842: fcmpl
    //   843: ifge -> 865
    //   846: fload #13
    //   848: invokestatic abs : (F)F
    //   851: fconst_1
    //   852: fcmpl
    //   853: iflt -> 859
    //   856: goto -> 865
    //   859: iconst_0
    //   860: istore #15
    //   862: goto -> 902
    //   865: aload_0
    //   866: iconst_5
    //   867: invokespecial recordGestureClassification : (I)V
    //   870: aload_0
    //   871: getfield mListener : Landroid/view/GestureDetector$OnGestureListener;
    //   874: aload_0
    //   875: getfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   878: aload_1
    //   879: fload #12
    //   881: fload #13
    //   883: invokeinterface onScroll : (Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    //   888: istore #15
    //   890: aload_0
    //   891: fload #7
    //   893: putfield mLastFocusX : F
    //   896: aload_0
    //   897: fload #11
    //   899: putfield mLastFocusY : F
    //   902: iload #10
    //   904: iconst_2
    //   905: if_icmpne -> 914
    //   908: iconst_1
    //   909: istore #4
    //   911: goto -> 917
    //   914: iconst_0
    //   915: istore #4
    //   917: iload #15
    //   919: istore #22
    //   921: iload #4
    //   923: ifeq -> 1607
    //   926: iload #15
    //   928: istore #22
    //   930: iload #14
    //   932: ifeq -> 1607
    //   935: aload_0
    //   936: getfield mHandler : Landroid/os/Handler;
    //   939: iconst_2
    //   940: invokevirtual removeMessages : (I)V
    //   943: aload_0
    //   944: getfield mHandler : Landroid/os/Handler;
    //   947: astore_2
    //   948: aload_2
    //   949: iconst_2
    //   950: iconst_4
    //   951: iconst_0
    //   952: invokevirtual obtainMessage : (III)Landroid/os/Message;
    //   955: astore #18
    //   957: aload_2
    //   958: aload #18
    //   960: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   963: pop
    //   964: iload #15
    //   966: istore #22
    //   968: goto -> 1607
    //   971: iconst_0
    //   972: istore #22
    //   974: goto -> 1607
    //   977: aload_0
    //   978: iconst_0
    //   979: putfield mStillDown : Z
    //   982: aload_1
    //   983: invokestatic obtain : (Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    //   986: astore_2
    //   987: aload_0
    //   988: getfield mIsDoubleTapping : Z
    //   991: ifeq -> 1016
    //   994: aload_0
    //   995: iconst_2
    //   996: invokespecial recordGestureClassification : (I)V
    //   999: iconst_0
    //   1000: aload_0
    //   1001: getfield mDoubleTapListener : Landroid/view/GestureDetector$OnDoubleTapListener;
    //   1004: aload_1
    //   1005: invokeinterface onDoubleTapEvent : (Landroid/view/MotionEvent;)Z
    //   1010: ior
    //   1011: istore #15
    //   1013: goto -> 1217
    //   1016: aload_0
    //   1017: getfield mInLongPress : Z
    //   1020: ifeq -> 1039
    //   1023: aload_0
    //   1024: getfield mHandler : Landroid/os/Handler;
    //   1027: iconst_3
    //   1028: invokevirtual removeMessages : (I)V
    //   1031: aload_0
    //   1032: iconst_0
    //   1033: putfield mInLongPress : Z
    //   1036: goto -> 1214
    //   1039: aload_0
    //   1040: getfield mAlwaysInTapRegion : Z
    //   1043: ifeq -> 1112
    //   1046: aload_0
    //   1047: getfield mIgnoreNextUpEvent : Z
    //   1050: ifne -> 1112
    //   1053: aload_0
    //   1054: iconst_1
    //   1055: invokespecial recordGestureClassification : (I)V
    //   1058: aload_0
    //   1059: getfield mListener : Landroid/view/GestureDetector$OnGestureListener;
    //   1062: aload_1
    //   1063: invokeinterface onSingleTapUp : (Landroid/view/MotionEvent;)Z
    //   1068: istore #22
    //   1070: iload #22
    //   1072: istore #15
    //   1074: aload_0
    //   1075: getfield mDeferConfirmSingleTap : Z
    //   1078: ifeq -> 1217
    //   1081: aload_0
    //   1082: getfield mDoubleTapListener : Landroid/view/GestureDetector$OnDoubleTapListener;
    //   1085: astore #18
    //   1087: iload #22
    //   1089: istore #15
    //   1091: aload #18
    //   1093: ifnull -> 1217
    //   1096: aload #18
    //   1098: aload_1
    //   1099: invokeinterface onSingleTapConfirmed : (Landroid/view/MotionEvent;)Z
    //   1104: pop
    //   1105: iload #22
    //   1107: istore #15
    //   1109: goto -> 1217
    //   1112: aload_0
    //   1113: getfield mIgnoreNextUpEvent : Z
    //   1116: ifne -> 1214
    //   1119: aload_0
    //   1120: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   1123: astore #18
    //   1125: aload_1
    //   1126: iconst_0
    //   1127: invokevirtual getPointerId : (I)I
    //   1130: istore #4
    //   1132: aload #18
    //   1134: sipush #1000
    //   1137: aload_0
    //   1138: getfield mMaximumFlingVelocity : I
    //   1141: i2f
    //   1142: invokevirtual computeCurrentVelocity : (IF)V
    //   1145: aload #18
    //   1147: iload #4
    //   1149: invokevirtual getYVelocity : (I)F
    //   1152: fstore #7
    //   1154: aload #18
    //   1156: iload #4
    //   1158: invokevirtual getXVelocity : (I)F
    //   1161: fstore #8
    //   1163: fload #7
    //   1165: invokestatic abs : (F)F
    //   1168: aload_0
    //   1169: getfield mMinimumFlingVelocity : I
    //   1172: i2f
    //   1173: fcmpl
    //   1174: ifgt -> 1191
    //   1177: fload #8
    //   1179: invokestatic abs : (F)F
    //   1182: aload_0
    //   1183: getfield mMinimumFlingVelocity : I
    //   1186: i2f
    //   1187: fcmpl
    //   1188: ifle -> 1214
    //   1191: aload_0
    //   1192: getfield mListener : Landroid/view/GestureDetector$OnGestureListener;
    //   1195: aload_0
    //   1196: getfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   1199: aload_1
    //   1200: fload #8
    //   1202: fload #7
    //   1204: invokeinterface onFling : (Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    //   1209: istore #15
    //   1211: goto -> 1217
    //   1214: iconst_0
    //   1215: istore #15
    //   1217: aload_0
    //   1218: getfield mPreviousUpEvent : Landroid/view/MotionEvent;
    //   1221: astore #18
    //   1223: aload #18
    //   1225: ifnull -> 1233
    //   1228: aload #18
    //   1230: invokevirtual recycle : ()V
    //   1233: aload_0
    //   1234: aload_2
    //   1235: putfield mPreviousUpEvent : Landroid/view/MotionEvent;
    //   1238: aload_0
    //   1239: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   1242: astore_2
    //   1243: aload_2
    //   1244: ifnull -> 1256
    //   1247: aload_2
    //   1248: invokevirtual recycle : ()V
    //   1251: aload_0
    //   1252: aconst_null
    //   1253: putfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   1256: aload_0
    //   1257: iconst_0
    //   1258: putfield mIsDoubleTapping : Z
    //   1261: aload_0
    //   1262: iconst_0
    //   1263: putfield mDeferConfirmSingleTap : Z
    //   1266: aload_0
    //   1267: iconst_0
    //   1268: putfield mIgnoreNextUpEvent : Z
    //   1271: aload_0
    //   1272: getfield mHandler : Landroid/os/Handler;
    //   1275: iconst_1
    //   1276: invokevirtual removeMessages : (I)V
    //   1279: aload_0
    //   1280: getfield mHandler : Landroid/os/Handler;
    //   1283: iconst_2
    //   1284: invokevirtual removeMessages : (I)V
    //   1287: iload #15
    //   1289: istore #22
    //   1291: goto -> 1607
    //   1294: aload_0
    //   1295: getfield mDoubleTapListener : Landroid/view/GestureDetector$OnDoubleTapListener;
    //   1298: ifnull -> 1418
    //   1301: aload_0
    //   1302: getfield mHandler : Landroid/os/Handler;
    //   1305: iconst_3
    //   1306: invokevirtual hasMessages : (I)Z
    //   1309: istore #15
    //   1311: iload #15
    //   1313: ifeq -> 1324
    //   1316: aload_0
    //   1317: getfield mHandler : Landroid/os/Handler;
    //   1320: iconst_3
    //   1321: invokevirtual removeMessages : (I)V
    //   1324: aload_0
    //   1325: getfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   1328: astore #18
    //   1330: aload #18
    //   1332: ifnull -> 1405
    //   1335: aload_0
    //   1336: getfield mPreviousUpEvent : Landroid/view/MotionEvent;
    //   1339: astore_2
    //   1340: aload_2
    //   1341: ifnull -> 1405
    //   1344: iload #15
    //   1346: ifeq -> 1405
    //   1349: aload_0
    //   1350: aload #18
    //   1352: aload_2
    //   1353: aload_1
    //   1354: invokespecial isConsideredDoubleTap : (Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    //   1357: ifeq -> 1405
    //   1360: aload_0
    //   1361: iconst_1
    //   1362: putfield mIsDoubleTapping : Z
    //   1365: aload_0
    //   1366: iconst_2
    //   1367: invokespecial recordGestureClassification : (I)V
    //   1370: aload_0
    //   1371: getfield mDoubleTapListener : Landroid/view/GestureDetector$OnDoubleTapListener;
    //   1374: aload_0
    //   1375: getfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   1378: invokeinterface onDoubleTap : (Landroid/view/MotionEvent;)Z
    //   1383: istore #15
    //   1385: iconst_0
    //   1386: iload #15
    //   1388: ior
    //   1389: aload_0
    //   1390: getfield mDoubleTapListener : Landroid/view/GestureDetector$OnDoubleTapListener;
    //   1393: aload_1
    //   1394: invokeinterface onDoubleTapEvent : (Landroid/view/MotionEvent;)Z
    //   1399: ior
    //   1400: istore #4
    //   1402: goto -> 1421
    //   1405: aload_0
    //   1406: getfield mHandler : Landroid/os/Handler;
    //   1409: iconst_3
    //   1410: getstatic android/view/GestureDetector.DOUBLE_TAP_TIMEOUT : I
    //   1413: i2l
    //   1414: invokevirtual sendEmptyMessageDelayed : (IJ)Z
    //   1417: pop
    //   1418: iconst_0
    //   1419: istore #4
    //   1421: aload_0
    //   1422: fload #7
    //   1424: putfield mLastFocusX : F
    //   1427: aload_0
    //   1428: fload #7
    //   1430: putfield mDownFocusX : F
    //   1433: aload_0
    //   1434: fload #11
    //   1436: putfield mLastFocusY : F
    //   1439: aload_0
    //   1440: fload #11
    //   1442: putfield mDownFocusY : F
    //   1445: aload_0
    //   1446: getfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   1449: astore_2
    //   1450: aload_2
    //   1451: ifnull -> 1458
    //   1454: aload_2
    //   1455: invokevirtual recycle : ()V
    //   1458: aload_0
    //   1459: aload_1
    //   1460: invokestatic obtain : (Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    //   1463: putfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   1466: aload_0
    //   1467: iconst_1
    //   1468: putfield mAlwaysInTapRegion : Z
    //   1471: aload_0
    //   1472: iconst_1
    //   1473: putfield mAlwaysInBiggerTapRegion : Z
    //   1476: aload_0
    //   1477: iconst_1
    //   1478: putfield mStillDown : Z
    //   1481: aload_0
    //   1482: iconst_0
    //   1483: putfield mInLongPress : Z
    //   1486: aload_0
    //   1487: iconst_0
    //   1488: putfield mDeferConfirmSingleTap : Z
    //   1491: aload_0
    //   1492: iconst_0
    //   1493: putfield mHasRecordedClassification : Z
    //   1496: aload_0
    //   1497: getfield mIsLongpressEnabled : Z
    //   1500: ifeq -> 1557
    //   1503: aload_0
    //   1504: getfield mHandler : Landroid/os/Handler;
    //   1507: iconst_2
    //   1508: invokevirtual removeMessages : (I)V
    //   1511: aload_0
    //   1512: getfield mHandler : Landroid/os/Handler;
    //   1515: astore #23
    //   1517: aload #23
    //   1519: iconst_2
    //   1520: iconst_3
    //   1521: iconst_0
    //   1522: invokevirtual obtainMessage : (III)Landroid/os/Message;
    //   1525: astore #18
    //   1527: aload_0
    //   1528: getfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   1531: astore_2
    //   1532: aload_2
    //   1533: invokevirtual getDownTime : ()J
    //   1536: lstore #16
    //   1538: invokestatic getLongPressTimeout : ()I
    //   1541: i2l
    //   1542: lstore #19
    //   1544: aload #23
    //   1546: aload #18
    //   1548: lload #16
    //   1550: lload #19
    //   1552: ladd
    //   1553: invokevirtual sendMessageAtTime : (Landroid/os/Message;J)Z
    //   1556: pop
    //   1557: aload_0
    //   1558: getfield mHandler : Landroid/os/Handler;
    //   1561: astore_2
    //   1562: aload_0
    //   1563: getfield mCurrentDownEvent : Landroid/view/MotionEvent;
    //   1566: astore #18
    //   1568: aload #18
    //   1570: invokevirtual getDownTime : ()J
    //   1573: lstore #16
    //   1575: getstatic android/view/GestureDetector.TAP_TIMEOUT : I
    //   1578: i2l
    //   1579: lstore #19
    //   1581: aload_2
    //   1582: iconst_1
    //   1583: lload #16
    //   1585: lload #19
    //   1587: ladd
    //   1588: invokevirtual sendEmptyMessageAtTime : (IJ)Z
    //   1591: pop
    //   1592: iload #4
    //   1594: aload_0
    //   1595: getfield mListener : Landroid/view/GestureDetector$OnGestureListener;
    //   1598: aload_1
    //   1599: invokeinterface onDown : (Landroid/view/MotionEvent;)Z
    //   1604: ior
    //   1605: istore #22
    //   1607: iload #22
    //   1609: ifne -> 1627
    //   1612: aload_0
    //   1613: getfield mInputEventConsistencyVerifier : Landroid/view/InputEventConsistencyVerifier;
    //   1616: astore_2
    //   1617: aload_2
    //   1618: ifnull -> 1627
    //   1621: aload_2
    //   1622: aload_1
    //   1623: iconst_0
    //   1624: invokevirtual onUnhandledEvent : (Landroid/view/InputEvent;I)V
    //   1627: iload #22
    //   1629: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #542	-> 0
    //   #543	-> 9
    //   #546	-> 15
    //   #548	-> 20
    //   #549	-> 29
    //   #551	-> 33
    //   #553	-> 41
    //   #554	-> 48
    //   #556	-> 55
    //   #558	-> 63
    //   #560	-> 82
    //   #561	-> 99
    //   #562	-> 99
    //   #565	-> 118
    //   #566	-> 124
    //   #567	-> 130
    //   #568	-> 140
    //   #569	-> 150
    //   #570	-> 161
    //   #567	-> 172
    //   #572	-> 178
    //   #573	-> 196
    //   #574	-> 204
    //   #576	-> 212
    //   #578	-> 212
    //   #587	-> 258
    //   #588	-> 270
    //   #592	-> 282
    //   #593	-> 297
    //   #594	-> 303
    //   #595	-> 311
    //   #596	-> 322
    //   #597	-> 333
    //   #598	-> 343
    //   #600	-> 353
    //   #601	-> 361
    //   #602	-> 372
    //   #604	-> 383
    //   #605	-> 383
    //   #606	-> 399
    //   #607	-> 406
    //   #605	-> 409
    //   #597	-> 409
    //   #610	-> 415
    //   #580	-> 418
    //   #581	-> 430
    //   #583	-> 442
    //   #584	-> 446
    //   #795	-> 449
    //   #662	-> 456
    //   #663	-> 470
    //   #666	-> 473
    //   #667	-> 479
    //   #669	-> 489
    //   #670	-> 498
    //   #671	-> 507
    //   #673	-> 514
    //   #675	-> 519
    //   #676	-> 536
    //   #677	-> 543
    //   #678	-> 553
    //   #679	-> 563
    //   #680	-> 576
    //   #682	-> 593
    //   #684	-> 608
    //   #686	-> 627
    //   #688	-> 632
    //   #694	-> 639
    //   #695	-> 647
    //   #696	-> 653
    //   #697	-> 659
    //   #701	-> 668
    //   #696	-> 685
    //   #688	-> 700
    //   #708	-> 700
    //   #686	-> 725
    //   #711	-> 725
    //   #712	-> 732
    //   #714	-> 737
    //   #715	-> 757
    //   #716	-> 763
    //   #717	-> 769
    //   #718	-> 774
    //   #719	-> 782
    //   #720	-> 790
    //   #711	-> 801
    //   #722	-> 804
    //   #723	-> 821
    //   #724	-> 828
    //   #726	-> 833
    //   #727	-> 865
    //   #728	-> 870
    //   #729	-> 890
    //   #730	-> 896
    //   #732	-> 902
    //   #734	-> 917
    //   #735	-> 935
    //   #736	-> 943
    //   #737	-> 948
    //   #736	-> 957
    //   #662	-> 971
    //   #799	-> 971
    //   #745	-> 977
    //   #746	-> 982
    //   #747	-> 987
    //   #749	-> 994
    //   #751	-> 999
    //   #752	-> 1016
    //   #753	-> 1023
    //   #754	-> 1031
    //   #755	-> 1039
    //   #756	-> 1053
    //   #758	-> 1058
    //   #759	-> 1070
    //   #760	-> 1096
    //   #762	-> 1112
    //   #765	-> 1119
    //   #766	-> 1125
    //   #767	-> 1132
    //   #768	-> 1145
    //   #769	-> 1154
    //   #771	-> 1163
    //   #772	-> 1177
    //   #773	-> 1191
    //   #776	-> 1214
    //   #777	-> 1228
    //   #780	-> 1233
    //   #781	-> 1238
    //   #784	-> 1247
    //   #785	-> 1251
    //   #787	-> 1256
    //   #788	-> 1261
    //   #789	-> 1266
    //   #790	-> 1271
    //   #791	-> 1279
    //   #792	-> 1287
    //   #613	-> 1294
    //   #614	-> 1301
    //   #615	-> 1311
    //   #616	-> 1324
    //   #618	-> 1349
    //   #620	-> 1360
    //   #621	-> 1365
    //   #624	-> 1370
    //   #626	-> 1385
    //   #629	-> 1405
    //   #633	-> 1418
    //   #634	-> 1433
    //   #635	-> 1445
    //   #636	-> 1454
    //   #638	-> 1458
    //   #639	-> 1466
    //   #640	-> 1471
    //   #641	-> 1476
    //   #642	-> 1481
    //   #643	-> 1486
    //   #644	-> 1491
    //   #646	-> 1496
    //   #647	-> 1503
    //   #648	-> 1511
    //   #649	-> 1517
    //   #653	-> 1532
    //   #654	-> 1538
    //   #648	-> 1544
    //   #656	-> 1557
    //   #657	-> 1568
    //   #656	-> 1581
    //   #658	-> 1592
    //   #659	-> 1607
    //   #799	-> 1607
    //   #800	-> 1621
    //   #802	-> 1627
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    InputEventConsistencyVerifier inputEventConsistencyVerifier = this.mInputEventConsistencyVerifier;
    if (inputEventConsistencyVerifier != null)
      inputEventConsistencyVerifier.onGenericMotionEvent(paramMotionEvent, 0); 
    int i = paramMotionEvent.getActionButton();
    int j = paramMotionEvent.getActionMasked();
    if (j != 11) {
      if (j == 12)
        if (this.mInContextClick && (i == 32 || i == 2)) {
          this.mInContextClick = false;
          this.mIgnoreNextUpEvent = true;
        }  
    } else if (this.mContextClickListener != null && !this.mInContextClick && !this.mInLongPress && (i == 32 || i == 2)) {
      if (this.mContextClickListener.onContextClick(paramMotionEvent)) {
        this.mInContextClick = true;
        this.mHandler.removeMessages(2);
        this.mHandler.removeMessages(3);
        return true;
      } 
    } 
    return false;
  }
  
  private void cancel() {
    this.mHandler.removeMessages(1);
    this.mHandler.removeMessages(2);
    this.mHandler.removeMessages(3);
    this.mVelocityTracker.recycle();
    this.mVelocityTracker = null;
    this.mIsDoubleTapping = false;
    this.mStillDown = false;
    this.mAlwaysInTapRegion = false;
    this.mAlwaysInBiggerTapRegion = false;
    this.mDeferConfirmSingleTap = false;
    this.mInLongPress = false;
    this.mInContextClick = false;
    this.mIgnoreNextUpEvent = false;
  }
  
  private void cancelTaps() {
    this.mHandler.removeMessages(1);
    this.mHandler.removeMessages(2);
    this.mHandler.removeMessages(3);
    this.mIsDoubleTapping = false;
    this.mAlwaysInTapRegion = false;
    this.mAlwaysInBiggerTapRegion = false;
    this.mDeferConfirmSingleTap = false;
    this.mInLongPress = false;
    this.mInContextClick = false;
    this.mIgnoreNextUpEvent = false;
  }
  
  private boolean isConsideredDoubleTap(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, MotionEvent paramMotionEvent3) {
    int k;
    boolean bool = this.mAlwaysInBiggerTapRegion;
    boolean bool1 = false;
    if (!bool)
      return false; 
    long l = paramMotionEvent3.getEventTime() - paramMotionEvent2.getEventTime();
    if (l > DOUBLE_TAP_TIMEOUT || l < DOUBLE_TAP_MIN_TIME)
      return false; 
    int i = (int)paramMotionEvent1.getX() - (int)paramMotionEvent3.getX();
    int j = (int)paramMotionEvent1.getY() - (int)paramMotionEvent3.getY();
    if ((paramMotionEvent1.getFlags() & 0x8) != 0) {
      k = 1;
    } else {
      k = 0;
    } 
    if (k) {
      k = 0;
    } else {
      k = this.mDoubleTapSlopSquare;
    } 
    if (i * i + j * j < k)
      bool1 = true; 
    return bool1;
  }
  
  private void dispatchLongPress() {
    this.mHandler.removeMessages(3);
    this.mDeferConfirmSingleTap = false;
    this.mInLongPress = true;
    this.mListener.onLongPress(this.mCurrentDownEvent);
  }
  
  private void recordGestureClassification(int paramInt) {
    if (this.mHasRecordedClassification || paramInt == 0)
      return; 
    if (this.mCurrentDownEvent == null || this.mCurrentMotionEvent == null) {
      this.mHasRecordedClassification = true;
      return;
    } 
    String str = getClass().getName();
    int i = (int)(SystemClock.uptimeMillis() - this.mCurrentMotionEvent.getDownTime());
    MotionEvent motionEvent = this.mCurrentMotionEvent;
    double d1 = (motionEvent.getRawX() - this.mCurrentDownEvent.getRawX());
    motionEvent = this.mCurrentMotionEvent;
    double d2 = (motionEvent.getRawY() - this.mCurrentDownEvent.getRawY());
    float f = (float)Math.hypot(d1, d2);
    FrameworkStatsLog.write(177, str, paramInt, i, f);
    this.mHasRecordedClassification = true;
  }
}
