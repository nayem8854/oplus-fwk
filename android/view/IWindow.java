package android.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.MergedConfiguration;
import com.android.internal.os.IResultReceiver;

public interface IWindow extends IInterface {
  void closeSystemDialogs(String paramString) throws RemoteException;
  
  void dispatchAppVisibility(boolean paramBoolean) throws RemoteException;
  
  void dispatchDragEvent(DragEvent paramDragEvent) throws RemoteException;
  
  void dispatchGetNewSurface() throws RemoteException;
  
  void dispatchPointerCaptureChanged(boolean paramBoolean) throws RemoteException;
  
  void dispatchSystemUiVisibilityChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean) throws RemoteException;
  
  void dispatchWallpaperOffsets(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, boolean paramBoolean) throws RemoteException;
  
  void dispatchWindowShown() throws RemoteException;
  
  void executeCommand(String paramString1, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  void hideInsets(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void insetsChanged(InsetsState paramInsetsState) throws RemoteException;
  
  void insetsControlChanged(InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl) throws RemoteException;
  
  void locationInParentDisplayChanged(Point paramPoint) throws RemoteException;
  
  void moved(int paramInt1, int paramInt2) throws RemoteException;
  
  void requestAppKeyboardShortcuts(IResultReceiver paramIResultReceiver, int paramInt) throws RemoteException;
  
  void requestScrollCapture(IScrollCaptureController paramIScrollCaptureController) throws RemoteException;
  
  void resized(Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4, boolean paramBoolean1, MergedConfiguration paramMergedConfiguration, Rect paramRect5, boolean paramBoolean2, boolean paramBoolean3, int paramInt, DisplayCutout.ParcelableWrapper paramParcelableWrapper) throws RemoteException;
  
  void showInsets(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void updatePointerIcon(float paramFloat1, float paramFloat2) throws RemoteException;
  
  void windowFocusChanged(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  class Default implements IWindow {
    public void executeCommand(String param1String1, String param1String2, ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {}
    
    public void resized(Rect param1Rect1, Rect param1Rect2, Rect param1Rect3, Rect param1Rect4, boolean param1Boolean1, MergedConfiguration param1MergedConfiguration, Rect param1Rect5, boolean param1Boolean2, boolean param1Boolean3, int param1Int, DisplayCutout.ParcelableWrapper param1ParcelableWrapper) throws RemoteException {}
    
    public void locationInParentDisplayChanged(Point param1Point) throws RemoteException {}
    
    public void insetsChanged(InsetsState param1InsetsState) throws RemoteException {}
    
    public void insetsControlChanged(InsetsState param1InsetsState, InsetsSourceControl[] param1ArrayOfInsetsSourceControl) throws RemoteException {}
    
    public void showInsets(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void hideInsets(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void moved(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void dispatchAppVisibility(boolean param1Boolean) throws RemoteException {}
    
    public void dispatchGetNewSurface() throws RemoteException {}
    
    public void windowFocusChanged(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void closeSystemDialogs(String param1String) throws RemoteException {}
    
    public void dispatchWallpaperOffsets(float param1Float1, float param1Float2, float param1Float3, float param1Float4, float param1Float5, boolean param1Boolean) throws RemoteException {}
    
    public void dispatchWallpaperCommand(String param1String, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle, boolean param1Boolean) throws RemoteException {}
    
    public void dispatchDragEvent(DragEvent param1DragEvent) throws RemoteException {}
    
    public void updatePointerIcon(float param1Float1, float param1Float2) throws RemoteException {}
    
    public void dispatchSystemUiVisibilityChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void dispatchWindowShown() throws RemoteException {}
    
    public void requestAppKeyboardShortcuts(IResultReceiver param1IResultReceiver, int param1Int) throws RemoteException {}
    
    public void dispatchPointerCaptureChanged(boolean param1Boolean) throws RemoteException {}
    
    public void requestScrollCapture(IScrollCaptureController param1IScrollCaptureController) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindow {
    private static final String DESCRIPTOR = "android.view.IWindow";
    
    static final int TRANSACTION_closeSystemDialogs = 12;
    
    static final int TRANSACTION_dispatchAppVisibility = 9;
    
    static final int TRANSACTION_dispatchDragEvent = 15;
    
    static final int TRANSACTION_dispatchGetNewSurface = 10;
    
    static final int TRANSACTION_dispatchPointerCaptureChanged = 20;
    
    static final int TRANSACTION_dispatchSystemUiVisibilityChanged = 17;
    
    static final int TRANSACTION_dispatchWallpaperCommand = 14;
    
    static final int TRANSACTION_dispatchWallpaperOffsets = 13;
    
    static final int TRANSACTION_dispatchWindowShown = 18;
    
    static final int TRANSACTION_executeCommand = 1;
    
    static final int TRANSACTION_hideInsets = 7;
    
    static final int TRANSACTION_insetsChanged = 4;
    
    static final int TRANSACTION_insetsControlChanged = 5;
    
    static final int TRANSACTION_locationInParentDisplayChanged = 3;
    
    static final int TRANSACTION_moved = 8;
    
    static final int TRANSACTION_requestAppKeyboardShortcuts = 19;
    
    static final int TRANSACTION_requestScrollCapture = 21;
    
    static final int TRANSACTION_resized = 2;
    
    static final int TRANSACTION_showInsets = 6;
    
    static final int TRANSACTION_updatePointerIcon = 16;
    
    static final int TRANSACTION_windowFocusChanged = 11;
    
    public Stub() {
      attachInterface(this, "android.view.IWindow");
    }
    
    public static IWindow asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IWindow");
      if (iInterface != null && iInterface instanceof IWindow)
        return (IWindow)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 21:
          return "requestScrollCapture";
        case 20:
          return "dispatchPointerCaptureChanged";
        case 19:
          return "requestAppKeyboardShortcuts";
        case 18:
          return "dispatchWindowShown";
        case 17:
          return "dispatchSystemUiVisibilityChanged";
        case 16:
          return "updatePointerIcon";
        case 15:
          return "dispatchDragEvent";
        case 14:
          return "dispatchWallpaperCommand";
        case 13:
          return "dispatchWallpaperOffsets";
        case 12:
          return "closeSystemDialogs";
        case 11:
          return "windowFocusChanged";
        case 10:
          return "dispatchGetNewSurface";
        case 9:
          return "dispatchAppVisibility";
        case 8:
          return "moved";
        case 7:
          return "hideInsets";
        case 6:
          return "showInsets";
        case 5:
          return "insetsControlChanged";
        case 4:
          return "insetsChanged";
        case 3:
          return "locationInParentDisplayChanged";
        case 2:
          return "resized";
        case 1:
          break;
      } 
      return "executeCommand";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        IScrollCaptureController iScrollCaptureController;
        String str1;
        InsetsSourceControl[] arrayOfInsetsSourceControl;
        IResultReceiver iResultReceiver;
        int i, j;
        float f1, f2, f3, f4, f5;
        Rect rect1, rect2;
        MergedConfiguration mergedConfiguration;
        Rect rect3;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 21:
            param1Parcel1.enforceInterface("android.view.IWindow");
            iScrollCaptureController = IScrollCaptureController.Stub.asInterface(param1Parcel1.readStrongBinder());
            requestScrollCapture(iScrollCaptureController);
            return true;
          case 20:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            bool2 = bool5;
            if (iScrollCaptureController.readInt() != 0)
              bool2 = true; 
            dispatchPointerCaptureChanged(bool2);
            return true;
          case 19:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            iResultReceiver = IResultReceiver.Stub.asInterface(iScrollCaptureController.readStrongBinder());
            param1Int1 = iScrollCaptureController.readInt();
            requestAppKeyboardShortcuts(iResultReceiver, param1Int1);
            return true;
          case 18:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            dispatchWindowShown();
            return true;
          case 17:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            param1Int1 = iScrollCaptureController.readInt();
            i = iScrollCaptureController.readInt();
            j = iScrollCaptureController.readInt();
            param1Int2 = iScrollCaptureController.readInt();
            dispatchSystemUiVisibilityChanged(param1Int1, i, j, param1Int2);
            return true;
          case 16:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            f1 = iScrollCaptureController.readFloat();
            f2 = iScrollCaptureController.readFloat();
            updatePointerIcon(f1, f2);
            return true;
          case 15:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            if (iScrollCaptureController.readInt() != 0) {
              DragEvent dragEvent = (DragEvent)DragEvent.CREATOR.createFromParcel((Parcel)iScrollCaptureController);
            } else {
              iScrollCaptureController = null;
            } 
            dispatchDragEvent((DragEvent)iScrollCaptureController);
            return true;
          case 14:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            str2 = iScrollCaptureController.readString();
            param1Int2 = iScrollCaptureController.readInt();
            i = iScrollCaptureController.readInt();
            param1Int1 = iScrollCaptureController.readInt();
            if (iScrollCaptureController.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iScrollCaptureController);
            } else {
              iResultReceiver = null;
            } 
            if (iScrollCaptureController.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            dispatchWallpaperCommand(str2, param1Int2, i, param1Int1, (Bundle)iResultReceiver, bool2);
            return true;
          case 13:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            f1 = iScrollCaptureController.readFloat();
            f3 = iScrollCaptureController.readFloat();
            f4 = iScrollCaptureController.readFloat();
            f2 = iScrollCaptureController.readFloat();
            f5 = iScrollCaptureController.readFloat();
            if (iScrollCaptureController.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            dispatchWallpaperOffsets(f1, f3, f4, f2, f5, bool2);
            return true;
          case 12:
            iScrollCaptureController.enforceInterface("android.view.IWindow");
            str1 = iScrollCaptureController.readString();
            closeSystemDialogs(str1);
            return true;
          case 11:
            str1.enforceInterface("android.view.IWindow");
            if (str1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (str1.readInt() != 0)
              bool1 = true; 
            windowFocusChanged(bool2, bool1);
            return true;
          case 10:
            str1.enforceInterface("android.view.IWindow");
            dispatchGetNewSurface();
            return true;
          case 9:
            str1.enforceInterface("android.view.IWindow");
            if (str1.readInt() != 0)
              bool2 = true; 
            dispatchAppVisibility(bool2);
            return true;
          case 8:
            str1.enforceInterface("android.view.IWindow");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            moved(param1Int1, param1Int2);
            return true;
          case 7:
            str1.enforceInterface("android.view.IWindow");
            param1Int1 = str1.readInt();
            bool2 = bool3;
            if (str1.readInt() != 0)
              bool2 = true; 
            hideInsets(param1Int1, bool2);
            return true;
          case 6:
            str1.enforceInterface("android.view.IWindow");
            param1Int1 = str1.readInt();
            bool2 = bool4;
            if (str1.readInt() != 0)
              bool2 = true; 
            showInsets(param1Int1, bool2);
            return true;
          case 5:
            str1.enforceInterface("android.view.IWindow");
            if (str1.readInt() != 0) {
              InsetsState insetsState = (InsetsState)InsetsState.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iResultReceiver = null;
            } 
            arrayOfInsetsSourceControl = (InsetsSourceControl[])str1.createTypedArray(InsetsSourceControl.CREATOR);
            insetsControlChanged((InsetsState)iResultReceiver, arrayOfInsetsSourceControl);
            return true;
          case 4:
            arrayOfInsetsSourceControl.enforceInterface("android.view.IWindow");
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              InsetsState insetsState = (InsetsState)InsetsState.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              arrayOfInsetsSourceControl = null;
            } 
            insetsChanged((InsetsState)arrayOfInsetsSourceControl);
            return true;
          case 3:
            arrayOfInsetsSourceControl.enforceInterface("android.view.IWindow");
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              Point point = (Point)Point.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              arrayOfInsetsSourceControl = null;
            } 
            locationInParentDisplayChanged((Point)arrayOfInsetsSourceControl);
            return true;
          case 2:
            arrayOfInsetsSourceControl.enforceInterface("android.view.IWindow");
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              iResultReceiver = null;
            } 
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              str2 = null;
            } 
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              rect1 = (Rect)Rect.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              rect1 = null;
            } 
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              rect2 = (Rect)Rect.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              rect2 = null;
            } 
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              mergedConfiguration = (MergedConfiguration)MergedConfiguration.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              mergedConfiguration = null;
            } 
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              rect3 = (Rect)Rect.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              rect3 = null;
            } 
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            param1Int1 = arrayOfInsetsSourceControl.readInt();
            if (arrayOfInsetsSourceControl.readInt() != 0) {
              DisplayCutout.ParcelableWrapper parcelableWrapper = (DisplayCutout.ParcelableWrapper)DisplayCutout.ParcelableWrapper.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
            } else {
              arrayOfInsetsSourceControl = null;
            } 
            resized((Rect)iResultReceiver, (Rect)str2, rect1, rect2, bool2, mergedConfiguration, rect3, bool1, bool3, param1Int1, (DisplayCutout.ParcelableWrapper)arrayOfInsetsSourceControl);
            return true;
          case 1:
            break;
        } 
        arrayOfInsetsSourceControl.enforceInterface("android.view.IWindow");
        str = arrayOfInsetsSourceControl.readString();
        String str2 = arrayOfInsetsSourceControl.readString();
        if (arrayOfInsetsSourceControl.readInt() != 0) {
          ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfInsetsSourceControl);
        } else {
          arrayOfInsetsSourceControl = null;
        } 
        executeCommand(str, str2, (ParcelFileDescriptor)arrayOfInsetsSourceControl);
        return true;
      } 
      str.writeString("android.view.IWindow");
      return true;
    }
    
    private static class Proxy implements IWindow {
      public static IWindow sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IWindow";
      }
      
      public void executeCommand(String param2String1, String param2String2, ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().executeCommand(param2String1, param2String2, param2ParcelFileDescriptor);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void resized(Rect param2Rect1, Rect param2Rect2, Rect param2Rect3, Rect param2Rect4, boolean param2Boolean1, MergedConfiguration param2MergedConfiguration, Rect param2Rect5, boolean param2Boolean2, boolean param2Boolean3, int param2Int, DisplayCutout.ParcelableWrapper param2ParcelableWrapper) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2Rect1 != null) {
            try {
              parcel.writeInt(1);
              param2Rect1.writeToParcel(parcel, 0);
            } finally {}
          } else {
            parcel.writeInt(0);
          } 
          if (param2Rect2 != null) {
            parcel.writeInt(1);
            param2Rect2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Rect3 != null) {
            parcel.writeInt(1);
            param2Rect3.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Rect4 != null) {
            parcel.writeInt(1);
            param2Rect4.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean1) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2MergedConfiguration != null) {
            parcel.writeInt(1);
            param2MergedConfiguration.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Rect5 != null) {
            parcel.writeInt(1);
            param2Rect5.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean2) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Boolean3) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          if (param2ParcelableWrapper != null) {
            parcel.writeInt(1);
            param2ParcelableWrapper.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(2, parcel, null, 1);
          if (!bool1 && IWindow.Stub.getDefaultImpl() != null) {
            IWindow iWindow = IWindow.Stub.getDefaultImpl();
            try {
              iWindow.resized(param2Rect1, param2Rect2, param2Rect3, param2Rect4, param2Boolean1, param2MergedConfiguration, param2Rect5, param2Boolean2, param2Boolean3, param2Int, param2ParcelableWrapper);
              parcel.recycle();
              return;
            } finally {}
          } else {
            parcel.recycle();
            return;
          } 
        } finally {}
        parcel.recycle();
        throw param2Rect1;
      }
      
      public void locationInParentDisplayChanged(Point param2Point) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2Point != null) {
            parcel.writeInt(1);
            param2Point.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().locationInParentDisplayChanged(param2Point);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void insetsChanged(InsetsState param2InsetsState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2InsetsState != null) {
            parcel.writeInt(1);
            param2InsetsState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().insetsChanged(param2InsetsState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void insetsControlChanged(InsetsState param2InsetsState, InsetsSourceControl[] param2ArrayOfInsetsSourceControl) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2InsetsState != null) {
            parcel.writeInt(1);
            param2InsetsState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeTypedArray((Parcelable[])param2ArrayOfInsetsSourceControl, 0);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().insetsControlChanged(param2InsetsState, param2ArrayOfInsetsSourceControl);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showInsets(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWindow");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel, null, 1);
          if (!bool1 && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().showInsets(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hideInsets(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWindow");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel, null, 1);
          if (!bool1 && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().hideInsets(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void moved(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().moved(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchAppVisibility(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(9, parcel, null, 1);
          if (!bool1 && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().dispatchAppVisibility(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchGetNewSurface() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().dispatchGetNewSurface();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void windowFocusChanged(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().windowFocusChanged(param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void closeSystemDialogs(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().closeSystemDialogs(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchWallpaperOffsets(float param2Float1, float param2Float2, float param2Float3, float param2Float4, float param2Float5, boolean param2Boolean) throws RemoteException {
        Exception exception;
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          try {
            parcel.writeFloat(param2Float1);
            try {
              parcel.writeFloat(param2Float2);
              try {
                parcel.writeFloat(param2Float3);
                try {
                  parcel.writeFloat(param2Float4);
                  try {
                    boolean bool;
                    parcel.writeFloat(param2Float5);
                    if (param2Boolean) {
                      bool = true;
                    } else {
                      bool = false;
                    } 
                    parcel.writeInt(bool);
                    try {
                      boolean bool1 = this.mRemote.transact(13, parcel, null, 1);
                      if (!bool1 && IWindow.Stub.getDefaultImpl() != null) {
                        IWindow.Stub.getDefaultImpl().dispatchWallpaperOffsets(param2Float1, param2Float2, param2Float3, param2Float4, param2Float5, param2Boolean);
                        parcel.recycle();
                        return;
                      } 
                      parcel.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw exception;
      }
      
      public void dispatchWallpaperCommand(String param2String, int param2Int1, int param2Int2, int param2Int3, Bundle param2Bundle, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          try {
            parcel.writeString(param2String);
            try {
              parcel.writeInt(param2Int1);
              try {
                parcel.writeInt(param2Int2);
                try {
                  parcel.writeInt(param2Int3);
                  boolean bool = false;
                  if (param2Bundle != null) {
                    parcel.writeInt(1);
                    param2Bundle.writeToParcel(parcel, 0);
                  } else {
                    parcel.writeInt(0);
                  } 
                  if (param2Boolean)
                    bool = true; 
                  parcel.writeInt(bool);
                  try {
                    boolean bool1 = this.mRemote.transact(14, parcel, null, 1);
                    if (!bool1 && IWindow.Stub.getDefaultImpl() != null) {
                      IWindow.Stub.getDefaultImpl().dispatchWallpaperCommand(param2String, param2Int1, param2Int2, param2Int3, param2Bundle, param2Boolean);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String;
      }
      
      public void dispatchDragEvent(DragEvent param2DragEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2DragEvent != null) {
            parcel.writeInt(1);
            param2DragEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().dispatchDragEvent(param2DragEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updatePointerIcon(float param2Float1, float param2Float2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          parcel.writeFloat(param2Float1);
          parcel.writeFloat(param2Float2);
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().updatePointerIcon(param2Float1, param2Float2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchSystemUiVisibilityChanged(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().dispatchSystemUiVisibilityChanged(param2Int1, param2Int2, param2Int3, param2Int4);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchWindowShown() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindow");
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().dispatchWindowShown();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestAppKeyboardShortcuts(IResultReceiver param2IResultReceiver, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().requestAppKeyboardShortcuts(param2IResultReceiver, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchPointerCaptureChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(20, parcel, null, 1);
          if (!bool1 && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().dispatchPointerCaptureChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestScrollCapture(IScrollCaptureController param2IScrollCaptureController) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.view.IWindow");
          if (param2IScrollCaptureController != null) {
            iBinder = param2IScrollCaptureController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IWindow.Stub.getDefaultImpl() != null) {
            IWindow.Stub.getDefaultImpl().requestScrollCapture(param2IScrollCaptureController);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindow param1IWindow) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindow != null) {
          Proxy.sDefaultImpl = param1IWindow;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindow getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
