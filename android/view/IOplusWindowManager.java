package android.view;

import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.oplus.darkmode.IOplusDarkModeListener;
import java.util.List;

public interface IOplusWindowManager extends IOplusBaseWindowManager {
  public static final int GET_APK_UNLOCK_WINDOW = 10004;
  
  public static final int GET_CURRENT_FOCUS_WINDOW = 10025;
  
  public static final int GET_FLOATWINDOW_RECT = 10030;
  
  public static final int GET_FOCUSED_WINDOW_IGNORE_HOME_MENU_KEY = 10056;
  
  public static final int GET_FREEFORM_STACK_BOUNDS = 10050;
  
  public static final int GET_IMEBG_OPLUS_FROM_ADAPTATION = 10054;
  
  public static final int GET_NAVBAR_OPLUS_FROM_ADAPTATION = 10052;
  
  public static final int GET_STATUSBAR_OPLUS_FROM_ADAPTATION = 10053;
  
  public static final int GET_TYPED_WINDOW_LAYER = 10055;
  
  public static final int IOPLUSWINDOWMANAGER_INDEX = 10001;
  
  public static final int IS_ACTIVITY_NEED_PALETTE = 10051;
  
  public static final int IS_FLOAT_WINDOW_FORBIDDEN = 10014;
  
  public static final int IS_FULL_SCREEN = 10011;
  
  public static final int IS_INPUT_SHOW = 10010;
  
  public static final int IS_IN_FREEFORM_MODE = 10049;
  
  public static final int IS_LOCK_ON_SHOW = 10006;
  
  public static final int IS_LOCK_WNDSHOW = 10002;
  
  public static final int IS_ROTATING = 10013;
  
  public static final int IS_SIM_UNLOCK_RUNNING = 10007;
  
  public static final int IS_STATUSBAR_VISIBLE = 10012;
  
  public static final int IS_WINDOW_SHOWN_FOR_UID = 10018;
  
  public static final int KEYGUARD_SET_APK_LOCKSCREEN_SHOWING = 10003;
  
  public static final int KEYGUARD_SHOE_SECURE_APKLOCK = 10005;
  
  public static final int OPEN_KEYGUARD_SESSION = 10020;
  
  public static final int REGISTAER_UIMODE_CHANGE_LISTENER = 10057;
  
  public static final int REGISTER_OPLUS_WINDOW_STATE_OBSERVER = 10047;
  
  public static final int REQUEST_DISMISS_KEYGUARD = 10017;
  
  public static final int REQUEST_KEYGUARD = 10019;
  
  public static final int REQUEST_REMOVE_WINDOW_ON_KEYGUARD = 10022;
  
  public static final int SET_BOOTANIM_ROTATION_LOCK = 10059;
  
  public static final int SET_GESTURE_FOLLOW_ANIMATION = 10042;
  
  public static final int SET_JOYSTICK_CONFIG = 10061;
  
  public static final int SET_JOYSTICK_CONFIGSTATUS = 10062;
  
  public static final int SET_JOYSTICK_SWITCHSTATUS = 10063;
  
  public static final int SET_MAGNIFICATION_SPEC_EX = 10015;
  
  public static final int SET_SPLIT_TIMEOUT = 10036;
  
  public static final int START_OPLUS_DRAG_WINDOW = 10046;
  
  public static final int UNREGISTAER_UIMODE_CHANGE_LISTENER = 10058;
  
  public static final int UNREGISTER_OPLUS_WINDOW_STATE_OBSERVER = 10048;
  
  public static final int UPDATE_INVALID_REGION = 10060;
  
  boolean checkIsFloatWindowForbidden(String paramString, int paramInt) throws RemoteException;
  
  IBinder getApkUnlockWindow() throws RemoteException;
  
  String getCurrentFocus() throws RemoteException;
  
  Rect getFloatWindowRect(int paramInt) throws RemoteException;
  
  int getFocusedWindowIgnoreHomeMenuKey() throws RemoteException;
  
  void getFreeformStackBounds(Rect paramRect) throws RemoteException;
  
  int getImeBgOplusFromAdaptation(String paramString) throws RemoteException;
  
  int getNavBarOplusFromAdaptation(String paramString1, String paramString2) throws RemoteException;
  
  int getStatusBarOplusFromAdaptation(String paramString1, String paramString2) throws RemoteException;
  
  int getTypedWindowLayer(int paramInt) throws RemoteException;
  
  boolean isActivityNeedPalette(String paramString1, String paramString2) throws RemoteException;
  
  boolean isFullScreen() throws RemoteException;
  
  boolean isInFreeformMode() throws RemoteException;
  
  boolean isInputShow() throws RemoteException;
  
  boolean isLockOnShow() throws RemoteException;
  
  boolean isLockWndShow() throws RemoteException;
  
  boolean isRotatingLw() throws RemoteException;
  
  boolean isSIMUnlockRunning() throws RemoteException;
  
  boolean isStatusBarVisible() throws RemoteException;
  
  boolean isWindowShownForUid(int paramInt) throws RemoteException;
  
  void keyguardSetApkLockScreenShowing(boolean paramBoolean) throws RemoteException;
  
  void keyguardShowSecureApkLock(boolean paramBoolean) throws RemoteException;
  
  void registerOnUiModeConfigurationChangeFinishListener(IOplusDarkModeListener paramIOplusDarkModeListener) throws RemoteException;
  
  void registerOplusWindowStateObserver(IOplusWindowStateObserver paramIOplusWindowStateObserver) throws RemoteException;
  
  void removeWindowShownOnKeyguard() throws RemoteException;
  
  void requestDismissKeyguard() throws RemoteException;
  
  void requestKeyguard(String paramString) throws RemoteException;
  
  void setBootAnimationRotationLock(boolean paramBoolean) throws RemoteException;
  
  void setGestureFollowAnimation(boolean paramBoolean) throws RemoteException;
  
  boolean setJoyStickConfig(int paramInt, String paramString) throws RemoteException;
  
  boolean setJoyStickStatus(int paramInt) throws RemoteException;
  
  boolean setJoyStickSwitch(int paramInt) throws RemoteException;
  
  void setMagnification(Bundle paramBundle) throws RemoteException;
  
  void setMagnificationSpecEx(MagnificationSpec paramMagnificationSpec) throws RemoteException;
  
  void setSplitTimeout(int paramInt) throws RemoteException;
  
  void startOplusDragWindow(String paramString, int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  void unregisterOnUiModeConfigurationChangeFinishListener(IOplusDarkModeListener paramIOplusDarkModeListener) throws RemoteException;
  
  void unregisterOplusWindowStateObserver(IOplusWindowStateObserver paramIOplusWindowStateObserver) throws RemoteException;
  
  boolean updateInvalidRegion(String paramString, List<RectF> paramList, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle) throws RemoteException;
}
