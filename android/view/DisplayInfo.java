package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.hardware.display.DeviceProductInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.util.ArraySet;
import android.util.DisplayMetrics;
import android.util.proto.ProtoOutputStream;
import com.oplus.screenmode.IOplusScreenModeFeature;
import java.util.Arrays;
import java.util.Objects;

public final class DisplayInfo implements Parcelable {
  public String uniqueId;
  
  public int type;
  
  public Display.Mode[] supportedModes = Display.Mode.EMPTY_ARRAY;
  
  public int[] supportedColorModes = new int[] { 0 };
  
  public int state;
  
  public int smallestNominalAppWidth;
  
  public int smallestNominalAppHeight;
  
  public int rotation;
  
  public int removeMode = 0;
  
  public long presentationDeadlineNanos;
  
  public float physicalYDpi;
  
  public float physicalXDpi;
  
  public int ownerUid;
  
  public String ownerPackageName;
  
  public String name;
  
  public int modeId;
  
  public boolean minimalPostProcessingSupported;
  
  public int logicalWidth;
  
  public int logicalHeight;
  
  public int logicalDensityDpi;
  
  public int layerStack;
  
  public int largestNominalAppWidth;
  
  public int largestNominalAppHeight;
  
  public Display.HdrCapabilities hdrCapabilities;
  
  public int flags;
  
  public int displayId;
  
  public DisplayCutout displayCutout;
  
  public DeviceProductInfo deviceProductInfo;
  
  public int defaultModeId;
  
  public int colorMode;
  
  public int appWidth;
  
  public long appVsyncOffsetNanos;
  
  public int appHeight;
  
  public DisplayAddress address;
  
  public static final Parcelable.Creator<DisplayInfo> CREATOR = new Parcelable.Creator<DisplayInfo>() {
      public DisplayInfo createFromParcel(Parcel param1Parcel) {
        return new DisplayInfo(param1Parcel);
      }
      
      public DisplayInfo[] newArray(int param1Int) {
        return new DisplayInfo[param1Int];
      }
    };
  
  public DisplayInfo(DisplayInfo paramDisplayInfo) {
    copyFrom(paramDisplayInfo);
  }
  
  private DisplayInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool;
    if (paramObject instanceof DisplayInfo && equals((DisplayInfo)paramObject)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean equals(DisplayInfo paramDisplayInfo) {
    if (paramDisplayInfo != null && this.layerStack == paramDisplayInfo.layerStack && this.flags == paramDisplayInfo.flags && this.type == paramDisplayInfo.type && this.displayId == paramDisplayInfo.displayId) {
      DisplayAddress displayAddress1 = this.address, displayAddress2 = paramDisplayInfo.address;
      if (Objects.equals(displayAddress1, displayAddress2)) {
        DeviceProductInfo deviceProductInfo1 = this.deviceProductInfo, deviceProductInfo2 = paramDisplayInfo.deviceProductInfo;
        if (Objects.equals(deviceProductInfo1, deviceProductInfo2)) {
          String str2 = this.uniqueId, str1 = paramDisplayInfo.uniqueId;
          if (Objects.equals(str2, str1) && this.appWidth == paramDisplayInfo.appWidth && this.appHeight == paramDisplayInfo.appHeight && this.smallestNominalAppWidth == paramDisplayInfo.smallestNominalAppWidth && this.smallestNominalAppHeight == paramDisplayInfo.smallestNominalAppHeight && this.largestNominalAppWidth == paramDisplayInfo.largestNominalAppWidth && this.largestNominalAppHeight == paramDisplayInfo.largestNominalAppHeight && this.logicalWidth == paramDisplayInfo.logicalWidth && this.logicalHeight == paramDisplayInfo.logicalHeight) {
            DisplayCutout displayCutout2 = this.displayCutout, displayCutout1 = paramDisplayInfo.displayCutout;
            if (Objects.equals(displayCutout2, displayCutout1) && this.rotation == paramDisplayInfo.rotation && this.modeId == paramDisplayInfo.modeId && this.defaultModeId == paramDisplayInfo.defaultModeId && this.colorMode == paramDisplayInfo.colorMode) {
              int[] arrayOfInt2 = this.supportedColorModes, arrayOfInt1 = paramDisplayInfo.supportedColorModes;
              if (Arrays.equals(arrayOfInt2, arrayOfInt1)) {
                Display.HdrCapabilities hdrCapabilities1 = this.hdrCapabilities, hdrCapabilities2 = paramDisplayInfo.hdrCapabilities;
                if (Objects.equals(hdrCapabilities1, hdrCapabilities2) && this.minimalPostProcessingSupported == paramDisplayInfo.minimalPostProcessingSupported && this.logicalDensityDpi == paramDisplayInfo.logicalDensityDpi && this.physicalXDpi == paramDisplayInfo.physicalXDpi && this.physicalYDpi == paramDisplayInfo.physicalYDpi && this.appVsyncOffsetNanos == paramDisplayInfo.appVsyncOffsetNanos && this.presentationDeadlineNanos == paramDisplayInfo.presentationDeadlineNanos && this.state == paramDisplayInfo.state && this.ownerUid == paramDisplayInfo.ownerUid) {
                  String str3 = this.ownerPackageName, str4 = paramDisplayInfo.ownerPackageName;
                  if (Objects.equals(str3, str4) && this.removeMode == paramDisplayInfo.removeMode)
                    return true; 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return 0;
  }
  
  public void copyFrom(DisplayInfo paramDisplayInfo) {
    this.layerStack = paramDisplayInfo.layerStack;
    this.flags = paramDisplayInfo.flags;
    this.type = paramDisplayInfo.type;
    this.displayId = paramDisplayInfo.displayId;
    this.address = paramDisplayInfo.address;
    this.deviceProductInfo = paramDisplayInfo.deviceProductInfo;
    this.name = paramDisplayInfo.name;
    this.uniqueId = paramDisplayInfo.uniqueId;
    this.appWidth = paramDisplayInfo.appWidth;
    this.appHeight = paramDisplayInfo.appHeight;
    this.smallestNominalAppWidth = paramDisplayInfo.smallestNominalAppWidth;
    this.smallestNominalAppHeight = paramDisplayInfo.smallestNominalAppHeight;
    this.largestNominalAppWidth = paramDisplayInfo.largestNominalAppWidth;
    this.largestNominalAppHeight = paramDisplayInfo.largestNominalAppHeight;
    this.logicalWidth = paramDisplayInfo.logicalWidth;
    this.logicalHeight = paramDisplayInfo.logicalHeight;
    this.displayCutout = paramDisplayInfo.displayCutout;
    this.rotation = paramDisplayInfo.rotation;
    this.modeId = paramDisplayInfo.modeId;
    this.defaultModeId = paramDisplayInfo.defaultModeId;
    Display.Mode[] arrayOfMode = paramDisplayInfo.supportedModes;
    this.supportedModes = Arrays.<Display.Mode>copyOf(arrayOfMode, arrayOfMode.length);
    this.colorMode = paramDisplayInfo.colorMode;
    int[] arrayOfInt = paramDisplayInfo.supportedColorModes;
    this.supportedColorModes = Arrays.copyOf(arrayOfInt, arrayOfInt.length);
    this.hdrCapabilities = paramDisplayInfo.hdrCapabilities;
    this.minimalPostProcessingSupported = paramDisplayInfo.minimalPostProcessingSupported;
    this.logicalDensityDpi = paramDisplayInfo.logicalDensityDpi;
    this.physicalXDpi = paramDisplayInfo.physicalXDpi;
    this.physicalYDpi = paramDisplayInfo.physicalYDpi;
    this.appVsyncOffsetNanos = paramDisplayInfo.appVsyncOffsetNanos;
    this.presentationDeadlineNanos = paramDisplayInfo.presentationDeadlineNanos;
    this.state = paramDisplayInfo.state;
    this.ownerUid = paramDisplayInfo.ownerUid;
    this.ownerPackageName = paramDisplayInfo.ownerPackageName;
    this.removeMode = paramDisplayInfo.removeMode;
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.layerStack = paramParcel.readInt();
    this.flags = paramParcel.readInt();
    this.type = paramParcel.readInt();
    this.displayId = paramParcel.readInt();
    this.address = (DisplayAddress)paramParcel.readParcelable(null);
    this.deviceProductInfo = (DeviceProductInfo)paramParcel.readParcelable(null);
    this.name = paramParcel.readString8();
    this.appWidth = paramParcel.readInt();
    this.appHeight = paramParcel.readInt();
    this.smallestNominalAppWidth = paramParcel.readInt();
    this.smallestNominalAppHeight = paramParcel.readInt();
    this.largestNominalAppWidth = paramParcel.readInt();
    this.largestNominalAppHeight = paramParcel.readInt();
    this.logicalWidth = paramParcel.readInt();
    this.logicalHeight = paramParcel.readInt();
    this.displayCutout = DisplayCutout.ParcelableWrapper.readCutoutFromParcel(paramParcel);
    this.rotation = paramParcel.readInt();
    this.modeId = paramParcel.readInt();
    this.defaultModeId = paramParcel.readInt();
    int i = paramParcel.readInt();
    this.supportedModes = new Display.Mode[i];
    byte b;
    for (b = 0; b < i; b++)
      this.supportedModes[b] = (Display.Mode)Display.Mode.CREATOR.createFromParcel(paramParcel); 
    this.colorMode = paramParcel.readInt();
    i = paramParcel.readInt();
    this.supportedColorModes = new int[i];
    for (b = 0; b < i; b++)
      this.supportedColorModes[b] = paramParcel.readInt(); 
    this.hdrCapabilities = (Display.HdrCapabilities)paramParcel.readParcelable(null);
    this.minimalPostProcessingSupported = paramParcel.readBoolean();
    this.logicalDensityDpi = paramParcel.readInt();
    this.physicalXDpi = paramParcel.readFloat();
    this.physicalYDpi = paramParcel.readFloat();
    this.appVsyncOffsetNanos = paramParcel.readLong();
    this.presentationDeadlineNanos = paramParcel.readLong();
    this.state = paramParcel.readInt();
    this.ownerUid = paramParcel.readInt();
    this.ownerPackageName = paramParcel.readString8();
    this.uniqueId = paramParcel.readString8();
    this.removeMode = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.layerStack);
    paramParcel.writeInt(this.flags);
    paramParcel.writeInt(this.type);
    paramParcel.writeInt(this.displayId);
    paramParcel.writeParcelable(this.address, paramInt);
    paramParcel.writeParcelable((Parcelable)this.deviceProductInfo, paramInt);
    paramParcel.writeString8(this.name);
    paramParcel.writeInt(this.appWidth);
    paramParcel.writeInt(this.appHeight);
    paramParcel.writeInt(this.smallestNominalAppWidth);
    paramParcel.writeInt(this.smallestNominalAppHeight);
    paramParcel.writeInt(this.largestNominalAppWidth);
    paramParcel.writeInt(this.largestNominalAppHeight);
    paramParcel.writeInt(this.logicalWidth);
    paramParcel.writeInt(this.logicalHeight);
    DisplayCutout.ParcelableWrapper.writeCutoutToParcel(this.displayCutout, paramParcel, paramInt);
    paramParcel.writeInt(this.rotation);
    paramParcel.writeInt(this.modeId);
    paramParcel.writeInt(this.defaultModeId);
    paramParcel.writeInt(this.supportedModes.length);
    byte b = 0;
    while (true) {
      Display.Mode[] arrayOfMode = this.supportedModes;
      if (b < arrayOfMode.length) {
        arrayOfMode[b].writeToParcel(paramParcel, paramInt);
        b++;
        continue;
      } 
      break;
    } 
    paramParcel.writeInt(this.colorMode);
    paramParcel.writeInt(this.supportedColorModes.length);
    b = 0;
    while (true) {
      int[] arrayOfInt = this.supportedColorModes;
      if (b < arrayOfInt.length) {
        paramParcel.writeInt(arrayOfInt[b]);
        b++;
        continue;
      } 
      break;
    } 
    paramParcel.writeParcelable(this.hdrCapabilities, paramInt);
    paramParcel.writeBoolean(this.minimalPostProcessingSupported);
    paramParcel.writeInt(this.logicalDensityDpi);
    paramParcel.writeFloat(this.physicalXDpi);
    paramParcel.writeFloat(this.physicalYDpi);
    paramParcel.writeLong(this.appVsyncOffsetNanos);
    paramParcel.writeLong(this.presentationDeadlineNanos);
    paramParcel.writeInt(this.state);
    paramParcel.writeInt(this.ownerUid);
    paramParcel.writeString8(this.ownerPackageName);
    paramParcel.writeString8(this.uniqueId);
    paramParcel.writeInt(this.removeMode);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public Display.Mode getMode() {
    return findMode(this.modeId);
  }
  
  public Display.Mode getDefaultMode() {
    return findMode(this.defaultModeId);
  }
  
  private Display.Mode findMode(int paramInt) {
    byte b = 0;
    while (true) {
      Display.Mode[] arrayOfMode = this.supportedModes;
      if (b < arrayOfMode.length) {
        if (arrayOfMode[b].getModeId() == paramInt)
          return this.supportedModes[b]; 
        b++;
        continue;
      } 
      break;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to locate mode ");
    stringBuilder.append(paramInt);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public int findDefaultModeByRefreshRate(float paramFloat) {
    Display.Mode[] arrayOfMode = this.supportedModes;
    Display.Mode mode = getDefaultMode();
    for (byte b = 0; b < arrayOfMode.length; b++) {
      Display.Mode mode1 = arrayOfMode[b];
      int i = mode.getPhysicalWidth(), j = mode.getPhysicalHeight();
      if (mode1.matches(i, j, paramFloat))
        return arrayOfMode[b].getModeId(); 
    } 
    return 0;
  }
  
  public float[] getDefaultRefreshRates() {
    Display.Mode[] arrayOfMode = this.supportedModes;
    ArraySet<Float> arraySet = new ArraySet();
    Display.Mode mode = getDefaultMode();
    byte b;
    for (b = 0; b < arrayOfMode.length; b++) {
      Display.Mode mode1 = arrayOfMode[b];
      if (mode1.getPhysicalWidth() == mode.getPhysicalWidth() && 
        mode1.getPhysicalHeight() == mode.getPhysicalHeight())
        arraySet.add(Float.valueOf(mode1.getRefreshRate())); 
    } 
    float[] arrayOfFloat = new float[arraySet.size()];
    b = 0;
    for (Float float_ : arraySet) {
      arrayOfFloat[b] = float_.floatValue();
      b++;
    } 
    return arrayOfFloat;
  }
  
  public void getAppMetrics(DisplayMetrics paramDisplayMetrics) {
    getAppMetrics(paramDisplayMetrics, CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO, null);
  }
  
  public void getAppMetrics(DisplayMetrics paramDisplayMetrics, DisplayAdjustments paramDisplayAdjustments) {
    CompatibilityInfo compatibilityInfo = paramDisplayAdjustments.getCompatibilityInfo();
    Configuration configuration = paramDisplayAdjustments.getConfiguration();
    int i = this.appWidth, j = this.appHeight;
    getMetricsWithSize(paramDisplayMetrics, compatibilityInfo, configuration, i, j);
  }
  
  public void getAppMetrics(DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo, Configuration paramConfiguration) {
    getMetricsWithSize(paramDisplayMetrics, paramCompatibilityInfo, paramConfiguration, this.appWidth, this.appHeight);
  }
  
  public void getLogicalMetrics(DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo, Configuration paramConfiguration) {
    getMetricsWithSize(paramDisplayMetrics, paramCompatibilityInfo, paramConfiguration, this.logicalWidth, this.logicalHeight);
    IOplusScreenModeFeature iOplusScreenModeFeature = (IOplusScreenModeFeature)OplusFeatureCache.get((IOplusCommonFeature)IOplusScreenModeFeature.DEFAULT);
    if (iOplusScreenModeFeature != null && iOplusScreenModeFeature.supportDisplayCompat())
      iOplusScreenModeFeature.applyCompatInfo(paramCompatibilityInfo, paramDisplayMetrics); 
  }
  
  public int getNaturalWidth() {
    null = this.rotation;
    return (null == 0 || null == 2) ? 
      this.logicalWidth : this.logicalHeight;
  }
  
  public int getNaturalHeight() {
    null = this.rotation;
    return (null == 0 || null == 2) ? 
      this.logicalHeight : this.logicalWidth;
  }
  
  public boolean isHdr() {
    boolean bool;
    Display.HdrCapabilities hdrCapabilities = this.hdrCapabilities;
    if (hdrCapabilities != null) {
      int[] arrayOfInt = hdrCapabilities.getSupportedHdrTypes();
    } else {
      hdrCapabilities = null;
    } 
    if (hdrCapabilities != null && hdrCapabilities.length > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isWideColorGamut() {
    for (int i : this.supportedColorModes) {
      if (i == 6 || i > 7)
        return true; 
    } 
    return false;
  }
  
  public boolean hasAccess(int paramInt) {
    return Display.hasAccess(paramInt, this.flags, this.ownerUid, this.displayId);
  }
  
  private void getMetricsWithSize(DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo, Configuration paramConfiguration, int paramInt1, int paramInt2) {
    int i = this.logicalDensityDpi;
    paramDisplayMetrics.densityDpi = i;
    float f = this.logicalDensityDpi * 0.00625F;
    paramDisplayMetrics.density = f;
    paramDisplayMetrics.noncompatScaledDensity = f = paramDisplayMetrics.density;
    paramDisplayMetrics.scaledDensity = f;
    paramDisplayMetrics.noncompatXdpi = f = this.physicalXDpi;
    paramDisplayMetrics.xdpi = f;
    paramDisplayMetrics.noncompatYdpi = f = this.physicalYDpi;
    paramDisplayMetrics.ydpi = f;
    if (paramConfiguration != null) {
      Rect rect = paramConfiguration.windowConfiguration.getAppBounds();
    } else {
      paramConfiguration = null;
    } 
    if (paramConfiguration != null)
      paramInt1 = paramConfiguration.width(); 
    if (paramConfiguration != null)
      paramInt2 = paramConfiguration.height(); 
    paramDisplayMetrics.widthPixels = paramInt1;
    paramDisplayMetrics.noncompatWidthPixels = paramInt1;
    paramDisplayMetrics.heightPixels = paramInt2;
    paramDisplayMetrics.noncompatHeightPixels = paramInt2;
    IOplusScreenModeFeature iOplusScreenModeFeature = (IOplusScreenModeFeature)OplusFeatureCache.get((IOplusCommonFeature)IOplusScreenModeFeature.DEFAULT);
    paramInt2 = 0;
    paramInt1 = paramInt2;
    if (iOplusScreenModeFeature != null) {
      paramInt1 = paramInt2;
      if (iOplusScreenModeFeature.supportDisplayCompat())
        paramInt1 = 1; 
    } 
    if (!paramCompatibilityInfo.equals(CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO)) {
      paramCompatibilityInfo.applyToDisplayMetrics(paramDisplayMetrics);
    } else if (paramInt1 != 0) {
      iOplusScreenModeFeature.applyCompatInfo(paramCompatibilityInfo, paramDisplayMetrics);
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DisplayInfo{\"");
    stringBuilder.append(this.name);
    stringBuilder.append("\", displayId ");
    stringBuilder.append(this.displayId);
    stringBuilder.append(flagsToString(this.flags));
    stringBuilder.append(", real ");
    stringBuilder.append(this.logicalWidth);
    stringBuilder.append(" x ");
    stringBuilder.append(this.logicalHeight);
    stringBuilder.append(", largest app ");
    stringBuilder.append(this.largestNominalAppWidth);
    stringBuilder.append(" x ");
    stringBuilder.append(this.largestNominalAppHeight);
    stringBuilder.append(", smallest app ");
    stringBuilder.append(this.smallestNominalAppWidth);
    stringBuilder.append(" x ");
    stringBuilder.append(this.smallestNominalAppHeight);
    stringBuilder.append(", appVsyncOff ");
    stringBuilder.append(this.appVsyncOffsetNanos);
    stringBuilder.append(", presDeadline ");
    stringBuilder.append(this.presentationDeadlineNanos);
    stringBuilder.append(", mode ");
    stringBuilder.append(this.modeId);
    stringBuilder.append(", defaultMode ");
    stringBuilder.append(this.defaultModeId);
    stringBuilder.append(", modes ");
    stringBuilder.append(Arrays.toString((Object[])this.supportedModes));
    stringBuilder.append(", hdrCapabilities ");
    stringBuilder.append(this.hdrCapabilities);
    stringBuilder.append(", minimalPostProcessingSupported ");
    stringBuilder.append(this.minimalPostProcessingSupported);
    stringBuilder.append(", rotation ");
    stringBuilder.append(this.rotation);
    stringBuilder.append(", state ");
    stringBuilder.append(Display.stateToString(this.state));
    if (Process.myUid() != 1000) {
      stringBuilder.append("}");
      return stringBuilder.toString();
    } 
    stringBuilder.append(", type ");
    stringBuilder.append(Display.typeToString(this.type));
    stringBuilder.append(", uniqueId \"");
    stringBuilder.append(this.uniqueId);
    stringBuilder.append("\", app ");
    stringBuilder.append(this.appWidth);
    stringBuilder.append(" x ");
    stringBuilder.append(this.appHeight);
    stringBuilder.append(", density ");
    stringBuilder.append(this.logicalDensityDpi);
    stringBuilder.append(" (");
    stringBuilder.append(this.physicalXDpi);
    stringBuilder.append(" x ");
    stringBuilder.append(this.physicalYDpi);
    stringBuilder.append(") dpi, layerStack ");
    stringBuilder.append(this.layerStack);
    stringBuilder.append(", colorMode ");
    stringBuilder.append(this.colorMode);
    stringBuilder.append(", supportedColorModes ");
    stringBuilder.append(Arrays.toString(this.supportedColorModes));
    if (this.address != null) {
      stringBuilder.append(", address ");
      stringBuilder.append(this.address);
    } 
    stringBuilder.append(", deviceProductInfo ");
    stringBuilder.append(this.deviceProductInfo);
    if (this.ownerUid != 0 || this.ownerPackageName != null) {
      stringBuilder.append(", owner ");
      stringBuilder.append(this.ownerPackageName);
      stringBuilder.append(" (uid ");
      stringBuilder.append(this.ownerUid);
      stringBuilder.append(")");
    } 
    stringBuilder.append(", removeMode ");
    stringBuilder.append(this.removeMode);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1120986464257L, this.logicalWidth);
    paramProtoOutputStream.write(1120986464258L, this.logicalHeight);
    paramProtoOutputStream.write(1120986464259L, this.appWidth);
    paramProtoOutputStream.write(1120986464260L, this.appHeight);
    paramProtoOutputStream.write(1138166333445L, this.name);
    paramProtoOutputStream.write(1120986464262L, this.flags);
    paramProtoOutputStream.end(paramLong);
  }
  
  private static String flagsToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    if ((paramInt & 0x2) != 0)
      stringBuilder.append(", FLAG_SECURE"); 
    if ((paramInt & 0x1) != 0)
      stringBuilder.append(", FLAG_SUPPORTS_PROTECTED_BUFFERS"); 
    if ((paramInt & 0x4) != 0)
      stringBuilder.append(", FLAG_PRIVATE"); 
    if ((paramInt & 0x8) != 0)
      stringBuilder.append(", FLAG_PRESENTATION"); 
    if ((0x40000000 & paramInt) != 0)
      stringBuilder.append(", FLAG_SCALING_DISABLED"); 
    if ((paramInt & 0x10) != 0)
      stringBuilder.append(", FLAG_ROUND"); 
    if ((paramInt & 0x20) != 0)
      stringBuilder.append(", FLAG_CAN_SHOW_WITH_INSECURE_KEYGUARD"); 
    if ((paramInt & 0x40) != 0)
      stringBuilder.append(", FLAG_SHOULD_SHOW_SYSTEM_DECORATIONS"); 
    if ((paramInt & 0x80) != 0)
      stringBuilder.append(", FLAG_TRUSTED"); 
    return stringBuilder.toString();
  }
  
  public DisplayInfo() {}
}
