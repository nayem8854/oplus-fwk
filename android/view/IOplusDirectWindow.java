package android.view;

import android.common.IOplusCommonFeature;
import android.os.RemoteException;
import com.oplus.direct.OplusDirectFindCmd;

public interface IOplusDirectWindow extends IOplusCommonFeature {
  default void directFindCmd(OplusDirectFindCmd paramOplusDirectFindCmd) throws RemoteException {}
}
