package android.view;

import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorTransaction {
  public static Class<?> TYPE = RefClass.load(OplusMirrorTransaction.class, "android.view.SurfaceControl$Transaction");
  
  @MethodParams({SurfaceControl.class, int.class})
  public static RefMethod<SurfaceControl.Transaction> setBackgroundBlurRadiusForOplus;
  
  @MethodParams({SurfaceControl.class, int.class})
  public static RefMethod<Void> setCastFlags;
}
