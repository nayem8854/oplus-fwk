package android.view;

import android.content.Context;
import android.os.Handler;

public class ScaleGestureDetector {
  private static final int ANCHORED_SCALE_MODE_DOUBLE_TAP = 1;
  
  private static final int ANCHORED_SCALE_MODE_NONE = 0;
  
  private static final int ANCHORED_SCALE_MODE_STYLUS = 2;
  
  private static final float SCALE_FACTOR = 0.5F;
  
  private static final String TAG = "ScaleGestureDetector";
  
  private static final long TOUCH_STABILIZE_TIME = 128L;
  
  private int mAnchoredScaleMode;
  
  private float mAnchoredScaleStartX;
  
  private float mAnchoredScaleStartY;
  
  private final Context mContext;
  
  private float mCurrSpan;
  
  private float mCurrSpanX;
  
  private float mCurrSpanY;
  
  private long mCurrTime;
  
  private boolean mEventBeforeOrAboveStartingGestureEvent;
  
  private float mFocusX;
  
  private float mFocusY;
  
  private GestureDetector mGestureDetector;
  
  private final Handler mHandler;
  
  private boolean mInProgress;
  
  private float mInitialSpan;
  
  private final InputEventConsistencyVerifier mInputEventConsistencyVerifier;
  
  private final OnScaleGestureListener mListener;
  
  private int mMinSpan;
  
  private float mPrevSpan;
  
  private float mPrevSpanX;
  
  private float mPrevSpanY;
  
  private long mPrevTime;
  
  private boolean mQuickScaleEnabled;
  
  private int mSpanSlop;
  
  private boolean mStylusScaleEnabled;
  
  public static interface OnScaleGestureListener {
    boolean onScale(ScaleGestureDetector param1ScaleGestureDetector);
    
    boolean onScaleBegin(ScaleGestureDetector param1ScaleGestureDetector);
    
    void onScaleEnd(ScaleGestureDetector param1ScaleGestureDetector);
  }
  
  class SimpleOnScaleGestureListener implements OnScaleGestureListener {
    public boolean onScale(ScaleGestureDetector param1ScaleGestureDetector) {
      return false;
    }
    
    public boolean onScaleBegin(ScaleGestureDetector param1ScaleGestureDetector) {
      return true;
    }
    
    public void onScaleEnd(ScaleGestureDetector param1ScaleGestureDetector) {}
  }
  
  public ScaleGestureDetector(Context paramContext, OnScaleGestureListener paramOnScaleGestureListener) {
    this(paramContext, paramOnScaleGestureListener, null);
  }
  
  public ScaleGestureDetector(Context paramContext, OnScaleGestureListener paramOnScaleGestureListener, Handler paramHandler) {
    InputEventConsistencyVerifier inputEventConsistencyVerifier;
    this.mAnchoredScaleMode = 0;
    if (InputEventConsistencyVerifier.isInstrumentationEnabled()) {
      inputEventConsistencyVerifier = new InputEventConsistencyVerifier(this, 0);
    } else {
      inputEventConsistencyVerifier = null;
    } 
    this.mInputEventConsistencyVerifier = inputEventConsistencyVerifier;
    this.mContext = paramContext;
    this.mListener = paramOnScaleGestureListener;
    ViewConfiguration viewConfiguration = ViewConfiguration.get(paramContext);
    this.mSpanSlop = viewConfiguration.getScaledTouchSlop() * 2;
    this.mMinSpan = viewConfiguration.getScaledMinimumScalingSpan();
    this.mHandler = paramHandler;
    int i = (paramContext.getApplicationInfo()).targetSdkVersion;
    if (i > 18)
      setQuickScaleEnabled(true); 
    if (i > 22)
      setStylusScaleEnabled(true); 
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInputEventConsistencyVerifier : Landroid/view/InputEventConsistencyVerifier;
    //   4: astore_2
    //   5: aload_2
    //   6: ifnull -> 15
    //   9: aload_2
    //   10: aload_1
    //   11: iconst_0
    //   12: invokevirtual onTouchEvent : (Landroid/view/MotionEvent;I)V
    //   15: aload_0
    //   16: aload_1
    //   17: invokevirtual getEventTime : ()J
    //   20: putfield mCurrTime : J
    //   23: aload_1
    //   24: invokevirtual getActionMasked : ()I
    //   27: istore_3
    //   28: aload_0
    //   29: getfield mQuickScaleEnabled : Z
    //   32: ifeq -> 44
    //   35: aload_0
    //   36: getfield mGestureDetector : Landroid/view/GestureDetector;
    //   39: aload_1
    //   40: invokevirtual onTouchEvent : (Landroid/view/MotionEvent;)Z
    //   43: pop
    //   44: aload_1
    //   45: invokevirtual getPointerCount : ()I
    //   48: istore #4
    //   50: aload_1
    //   51: invokevirtual getButtonState : ()I
    //   54: bipush #32
    //   56: iand
    //   57: ifeq -> 66
    //   60: iconst_1
    //   61: istore #5
    //   63: goto -> 69
    //   66: iconst_0
    //   67: istore #5
    //   69: aload_0
    //   70: getfield mAnchoredScaleMode : I
    //   73: iconst_2
    //   74: if_icmpne -> 88
    //   77: iload #5
    //   79: ifne -> 88
    //   82: iconst_1
    //   83: istore #6
    //   85: goto -> 91
    //   88: iconst_0
    //   89: istore #6
    //   91: iload_3
    //   92: iconst_1
    //   93: if_icmpeq -> 115
    //   96: iload_3
    //   97: iconst_3
    //   98: if_icmpeq -> 115
    //   101: iload #6
    //   103: ifeq -> 109
    //   106: goto -> 115
    //   109: iconst_0
    //   110: istore #7
    //   112: goto -> 118
    //   115: iconst_1
    //   116: istore #7
    //   118: iload_3
    //   119: ifeq -> 127
    //   122: iload #7
    //   124: ifeq -> 196
    //   127: aload_0
    //   128: getfield mInProgress : Z
    //   131: ifeq -> 162
    //   134: aload_0
    //   135: getfield mListener : Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    //   138: aload_0
    //   139: invokeinterface onScaleEnd : (Landroid/view/ScaleGestureDetector;)V
    //   144: aload_0
    //   145: iconst_0
    //   146: putfield mInProgress : Z
    //   149: aload_0
    //   150: fconst_0
    //   151: putfield mInitialSpan : F
    //   154: aload_0
    //   155: iconst_0
    //   156: putfield mAnchoredScaleMode : I
    //   159: goto -> 189
    //   162: aload_0
    //   163: invokespecial inAnchoredScaleMode : ()Z
    //   166: ifeq -> 189
    //   169: iload #7
    //   171: ifeq -> 189
    //   174: aload_0
    //   175: iconst_0
    //   176: putfield mInProgress : Z
    //   179: aload_0
    //   180: fconst_0
    //   181: putfield mInitialSpan : F
    //   184: aload_0
    //   185: iconst_0
    //   186: putfield mAnchoredScaleMode : I
    //   189: iload #7
    //   191: ifeq -> 196
    //   194: iconst_1
    //   195: ireturn
    //   196: aload_0
    //   197: getfield mInProgress : Z
    //   200: ifne -> 253
    //   203: aload_0
    //   204: getfield mStylusScaleEnabled : Z
    //   207: ifeq -> 253
    //   210: aload_0
    //   211: invokespecial inAnchoredScaleMode : ()Z
    //   214: ifne -> 253
    //   217: iload #7
    //   219: ifne -> 253
    //   222: iload #5
    //   224: ifeq -> 253
    //   227: aload_0
    //   228: aload_1
    //   229: invokevirtual getX : ()F
    //   232: putfield mAnchoredScaleStartX : F
    //   235: aload_0
    //   236: aload_1
    //   237: invokevirtual getY : ()F
    //   240: putfield mAnchoredScaleStartY : F
    //   243: aload_0
    //   244: iconst_2
    //   245: putfield mAnchoredScaleMode : I
    //   248: aload_0
    //   249: fconst_0
    //   250: putfield mInitialSpan : F
    //   253: iload_3
    //   254: ifeq -> 282
    //   257: iload_3
    //   258: bipush #6
    //   260: if_icmpeq -> 282
    //   263: iload_3
    //   264: iconst_5
    //   265: if_icmpeq -> 282
    //   268: iload #6
    //   270: ifeq -> 276
    //   273: goto -> 282
    //   276: iconst_0
    //   277: istore #5
    //   279: goto -> 285
    //   282: iconst_1
    //   283: istore #5
    //   285: iload_3
    //   286: bipush #6
    //   288: if_icmpne -> 297
    //   291: iconst_1
    //   292: istore #7
    //   294: goto -> 300
    //   297: iconst_0
    //   298: istore #7
    //   300: iload #7
    //   302: ifeq -> 314
    //   305: aload_1
    //   306: invokevirtual getActionIndex : ()I
    //   309: istore #6
    //   311: goto -> 317
    //   314: iconst_m1
    //   315: istore #6
    //   317: fconst_0
    //   318: fstore #8
    //   320: fconst_0
    //   321: fstore #9
    //   323: iload #7
    //   325: ifeq -> 337
    //   328: iload #4
    //   330: iconst_1
    //   331: isub
    //   332: istore #7
    //   334: goto -> 341
    //   337: iload #4
    //   339: istore #7
    //   341: aload_0
    //   342: invokespecial inAnchoredScaleMode : ()Z
    //   345: ifeq -> 386
    //   348: aload_0
    //   349: getfield mAnchoredScaleStartX : F
    //   352: fstore #8
    //   354: aload_0
    //   355: getfield mAnchoredScaleStartY : F
    //   358: fstore #9
    //   360: aload_1
    //   361: invokevirtual getY : ()F
    //   364: fload #9
    //   366: fcmpg
    //   367: ifge -> 378
    //   370: aload_0
    //   371: iconst_1
    //   372: putfield mEventBeforeOrAboveStartingGestureEvent : Z
    //   375: goto -> 450
    //   378: aload_0
    //   379: iconst_0
    //   380: putfield mEventBeforeOrAboveStartingGestureEvent : Z
    //   383: goto -> 450
    //   386: iconst_0
    //   387: istore #10
    //   389: iload #10
    //   391: iload #4
    //   393: if_icmpge -> 434
    //   396: iload #6
    //   398: iload #10
    //   400: if_icmpne -> 406
    //   403: goto -> 428
    //   406: fload #8
    //   408: aload_1
    //   409: iload #10
    //   411: invokevirtual getX : (I)F
    //   414: fadd
    //   415: fstore #8
    //   417: fload #9
    //   419: aload_1
    //   420: iload #10
    //   422: invokevirtual getY : (I)F
    //   425: fadd
    //   426: fstore #9
    //   428: iinc #10, 1
    //   431: goto -> 389
    //   434: fload #8
    //   436: iload #7
    //   438: i2f
    //   439: fdiv
    //   440: fstore #8
    //   442: fload #9
    //   444: iload #7
    //   446: i2f
    //   447: fdiv
    //   448: fstore #9
    //   450: fconst_0
    //   451: fstore #11
    //   453: fconst_0
    //   454: fstore #12
    //   456: iconst_0
    //   457: istore #10
    //   459: iload #10
    //   461: iload #4
    //   463: if_icmpge -> 516
    //   466: iload #6
    //   468: iload #10
    //   470: if_icmpne -> 476
    //   473: goto -> 510
    //   476: fload #11
    //   478: aload_1
    //   479: iload #10
    //   481: invokevirtual getX : (I)F
    //   484: fload #8
    //   486: fsub
    //   487: invokestatic abs : (F)F
    //   490: fadd
    //   491: fstore #11
    //   493: fload #12
    //   495: aload_1
    //   496: iload #10
    //   498: invokevirtual getY : (I)F
    //   501: fload #9
    //   503: fsub
    //   504: invokestatic abs : (F)F
    //   507: fadd
    //   508: fstore #12
    //   510: iinc #10, 1
    //   513: goto -> 459
    //   516: fload #11
    //   518: iload #7
    //   520: i2f
    //   521: fdiv
    //   522: fstore #11
    //   524: fload #12
    //   526: iload #7
    //   528: i2f
    //   529: fdiv
    //   530: fstore #12
    //   532: fload #11
    //   534: fconst_2
    //   535: fmul
    //   536: fstore #13
    //   538: fload #12
    //   540: fconst_2
    //   541: fmul
    //   542: fstore #11
    //   544: aload_0
    //   545: invokespecial inAnchoredScaleMode : ()Z
    //   548: ifeq -> 558
    //   551: fload #11
    //   553: fstore #12
    //   555: goto -> 570
    //   558: fload #13
    //   560: f2d
    //   561: fload #11
    //   563: f2d
    //   564: invokestatic hypot : (DD)D
    //   567: d2f
    //   568: fstore #12
    //   570: aload_0
    //   571: getfield mInProgress : Z
    //   574: istore #14
    //   576: aload_0
    //   577: fload #8
    //   579: putfield mFocusX : F
    //   582: aload_0
    //   583: fload #9
    //   585: putfield mFocusY : F
    //   588: aload_0
    //   589: invokespecial inAnchoredScaleMode : ()Z
    //   592: ifne -> 639
    //   595: aload_0
    //   596: getfield mInProgress : Z
    //   599: ifeq -> 639
    //   602: fload #12
    //   604: aload_0
    //   605: getfield mMinSpan : I
    //   608: i2f
    //   609: fcmpg
    //   610: iflt -> 618
    //   613: iload #5
    //   615: ifeq -> 639
    //   618: aload_0
    //   619: getfield mListener : Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    //   622: aload_0
    //   623: invokeinterface onScaleEnd : (Landroid/view/ScaleGestureDetector;)V
    //   628: aload_0
    //   629: iconst_0
    //   630: putfield mInProgress : Z
    //   633: aload_0
    //   634: fload #12
    //   636: putfield mInitialSpan : F
    //   639: iload #5
    //   641: ifeq -> 686
    //   644: aload_0
    //   645: fload #13
    //   647: putfield mCurrSpanX : F
    //   650: aload_0
    //   651: fload #13
    //   653: putfield mPrevSpanX : F
    //   656: aload_0
    //   657: fload #11
    //   659: putfield mCurrSpanY : F
    //   662: aload_0
    //   663: fload #11
    //   665: putfield mPrevSpanY : F
    //   668: aload_0
    //   669: fload #12
    //   671: putfield mCurrSpan : F
    //   674: aload_0
    //   675: fload #12
    //   677: putfield mPrevSpan : F
    //   680: aload_0
    //   681: fload #12
    //   683: putfield mInitialSpan : F
    //   686: aload_0
    //   687: invokespecial inAnchoredScaleMode : ()Z
    //   690: ifeq -> 702
    //   693: aload_0
    //   694: getfield mSpanSlop : I
    //   697: istore #5
    //   699: goto -> 708
    //   702: aload_0
    //   703: getfield mMinSpan : I
    //   706: istore #5
    //   708: aload_0
    //   709: getfield mInProgress : Z
    //   712: ifne -> 819
    //   715: fload #12
    //   717: iload #5
    //   719: i2f
    //   720: fcmpl
    //   721: iflt -> 819
    //   724: iload #14
    //   726: ifne -> 758
    //   729: aload_0
    //   730: getfield mInitialSpan : F
    //   733: fstore #9
    //   735: fload #12
    //   737: fload #9
    //   739: fsub
    //   740: invokestatic abs : (F)F
    //   743: aload_0
    //   744: getfield mSpanSlop : I
    //   747: i2f
    //   748: fcmpl
    //   749: ifle -> 755
    //   752: goto -> 758
    //   755: goto -> 819
    //   758: aload_0
    //   759: fload #13
    //   761: putfield mCurrSpanX : F
    //   764: aload_0
    //   765: fload #13
    //   767: putfield mPrevSpanX : F
    //   770: aload_0
    //   771: fload #11
    //   773: putfield mCurrSpanY : F
    //   776: aload_0
    //   777: fload #11
    //   779: putfield mPrevSpanY : F
    //   782: aload_0
    //   783: fload #12
    //   785: putfield mCurrSpan : F
    //   788: aload_0
    //   789: fload #12
    //   791: putfield mPrevSpan : F
    //   794: aload_0
    //   795: aload_0
    //   796: getfield mCurrTime : J
    //   799: putfield mPrevTime : J
    //   802: aload_0
    //   803: aload_0
    //   804: getfield mListener : Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    //   807: aload_0
    //   808: invokeinterface onScaleBegin : (Landroid/view/ScaleGestureDetector;)Z
    //   813: putfield mInProgress : Z
    //   816: goto -> 819
    //   819: iload_3
    //   820: iconst_2
    //   821: if_icmpne -> 907
    //   824: aload_0
    //   825: fload #13
    //   827: putfield mCurrSpanX : F
    //   830: aload_0
    //   831: fload #11
    //   833: putfield mCurrSpanY : F
    //   836: aload_0
    //   837: fload #12
    //   839: putfield mCurrSpan : F
    //   842: iconst_1
    //   843: istore #14
    //   845: aload_0
    //   846: getfield mInProgress : Z
    //   849: ifeq -> 864
    //   852: aload_0
    //   853: getfield mListener : Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    //   856: aload_0
    //   857: invokeinterface onScale : (Landroid/view/ScaleGestureDetector;)Z
    //   862: istore #14
    //   864: iload #14
    //   866: ifeq -> 904
    //   869: aload_0
    //   870: aload_0
    //   871: getfield mCurrSpanX : F
    //   874: putfield mPrevSpanX : F
    //   877: aload_0
    //   878: aload_0
    //   879: getfield mCurrSpanY : F
    //   882: putfield mPrevSpanY : F
    //   885: aload_0
    //   886: aload_0
    //   887: getfield mCurrSpan : F
    //   890: putfield mPrevSpan : F
    //   893: aload_0
    //   894: aload_0
    //   895: getfield mCurrTime : J
    //   898: putfield mPrevTime : J
    //   901: goto -> 907
    //   904: goto -> 907
    //   907: iconst_1
    //   908: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #230	-> 0
    //   #231	-> 9
    //   #234	-> 15
    //   #236	-> 23
    //   #239	-> 28
    //   #240	-> 35
    //   #243	-> 44
    //   #244	-> 50
    //   #245	-> 50
    //   #247	-> 69
    //   #249	-> 91
    //   #252	-> 118
    //   #256	-> 127
    //   #257	-> 134
    //   #258	-> 144
    //   #259	-> 149
    //   #260	-> 154
    //   #261	-> 162
    //   #262	-> 174
    //   #263	-> 179
    //   #264	-> 184
    //   #267	-> 189
    //   #268	-> 194
    //   #272	-> 196
    //   #275	-> 227
    //   #276	-> 235
    //   #277	-> 243
    //   #278	-> 248
    //   #281	-> 253
    //   #285	-> 285
    //   #286	-> 300
    //   #289	-> 317
    //   #290	-> 323
    //   #293	-> 341
    //   #296	-> 348
    //   #297	-> 354
    //   #298	-> 360
    //   #299	-> 370
    //   #301	-> 378
    //   #304	-> 386
    //   #305	-> 396
    //   #306	-> 406
    //   #307	-> 417
    //   #304	-> 428
    //   #310	-> 434
    //   #311	-> 442
    //   #315	-> 450
    //   #316	-> 456
    //   #317	-> 466
    //   #320	-> 476
    //   #321	-> 493
    //   #316	-> 510
    //   #323	-> 516
    //   #324	-> 524
    //   #329	-> 532
    //   #330	-> 538
    //   #332	-> 544
    //   #333	-> 551
    //   #335	-> 558
    //   #341	-> 570
    //   #342	-> 576
    //   #343	-> 582
    //   #344	-> 588
    //   #345	-> 618
    //   #346	-> 628
    //   #347	-> 633
    //   #349	-> 639
    //   #350	-> 644
    //   #351	-> 656
    //   #352	-> 668
    //   #355	-> 686
    //   #356	-> 708
    //   #357	-> 735
    //   #356	-> 758
    //   #358	-> 758
    //   #359	-> 770
    //   #360	-> 782
    //   #361	-> 794
    //   #362	-> 802
    //   #356	-> 819
    //   #366	-> 819
    //   #367	-> 824
    //   #368	-> 830
    //   #369	-> 836
    //   #371	-> 842
    //   #373	-> 845
    //   #374	-> 852
    //   #377	-> 864
    //   #378	-> 869
    //   #379	-> 877
    //   #380	-> 885
    //   #381	-> 893
    //   #377	-> 904
    //   #366	-> 907
    //   #385	-> 907
  }
  
  private boolean inAnchoredScaleMode() {
    boolean bool;
    if (this.mAnchoredScaleMode != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setQuickScaleEnabled(boolean paramBoolean) {
    this.mQuickScaleEnabled = paramBoolean;
    if (paramBoolean && this.mGestureDetector == null) {
      Object object = new Object(this);
      this.mGestureDetector = new GestureDetector(this.mContext, (GestureDetector.OnGestureListener)object, this.mHandler);
    } 
  }
  
  public boolean isQuickScaleEnabled() {
    return this.mQuickScaleEnabled;
  }
  
  public void setStylusScaleEnabled(boolean paramBoolean) {
    this.mStylusScaleEnabled = paramBoolean;
  }
  
  public boolean isStylusScaleEnabled() {
    return this.mStylusScaleEnabled;
  }
  
  public boolean isInProgress() {
    return this.mInProgress;
  }
  
  public float getFocusX() {
    return this.mFocusX;
  }
  
  public float getFocusY() {
    return this.mFocusY;
  }
  
  public float getCurrentSpan() {
    return this.mCurrSpan;
  }
  
  public float getCurrentSpanX() {
    return this.mCurrSpanX;
  }
  
  public float getCurrentSpanY() {
    return this.mCurrSpanY;
  }
  
  public float getPreviousSpan() {
    return this.mPrevSpan;
  }
  
  public float getPreviousSpanX() {
    return this.mPrevSpanX;
  }
  
  public float getPreviousSpanY() {
    return this.mPrevSpanY;
  }
  
  public float getScaleFactor() {
    boolean bool = inAnchoredScaleMode();
    float f1 = 1.0F;
    if (bool) {
      boolean bool1;
      if ((this.mEventBeforeOrAboveStartingGestureEvent && this.mCurrSpan < this.mPrevSpan) || (!this.mEventBeforeOrAboveStartingGestureEvent && this.mCurrSpan > this.mPrevSpan)) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      float f = Math.abs(1.0F - this.mCurrSpan / this.mPrevSpan) * 0.5F;
      if (this.mPrevSpan > this.mSpanSlop)
        if (bool1) {
          f1 = 1.0F + f;
        } else {
          f1 = 1.0F - f;
        }  
      return f1;
    } 
    float f2 = this.mPrevSpan;
    if (f2 > 0.0F)
      f1 = this.mCurrSpan / f2; 
    return f1;
  }
  
  public long getTimeDelta() {
    return this.mCurrTime - this.mPrevTime;
  }
  
  public long getEventTime() {
    return this.mCurrTime;
  }
}
