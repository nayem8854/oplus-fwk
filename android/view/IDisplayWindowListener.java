package android.view;

import android.content.res.Configuration;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDisplayWindowListener extends IInterface {
  void onDisplayAdded(int paramInt) throws RemoteException;
  
  void onDisplayConfigurationChanged(int paramInt, Configuration paramConfiguration) throws RemoteException;
  
  void onDisplayRemoved(int paramInt) throws RemoteException;
  
  void onFixedRotationFinished(int paramInt) throws RemoteException;
  
  void onFixedRotationStarted(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IDisplayWindowListener {
    public void onDisplayAdded(int param1Int) throws RemoteException {}
    
    public void onDisplayConfigurationChanged(int param1Int, Configuration param1Configuration) throws RemoteException {}
    
    public void onDisplayRemoved(int param1Int) throws RemoteException {}
    
    public void onFixedRotationStarted(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onFixedRotationFinished(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDisplayWindowListener {
    private static final String DESCRIPTOR = "android.view.IDisplayWindowListener";
    
    static final int TRANSACTION_onDisplayAdded = 1;
    
    static final int TRANSACTION_onDisplayConfigurationChanged = 2;
    
    static final int TRANSACTION_onDisplayRemoved = 3;
    
    static final int TRANSACTION_onFixedRotationFinished = 5;
    
    static final int TRANSACTION_onFixedRotationStarted = 4;
    
    public Stub() {
      attachInterface(this, "android.view.IDisplayWindowListener");
    }
    
    public static IDisplayWindowListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IDisplayWindowListener");
      if (iInterface != null && iInterface instanceof IDisplayWindowListener)
        return (IDisplayWindowListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onFixedRotationFinished";
            } 
            return "onFixedRotationStarted";
          } 
          return "onDisplayRemoved";
        } 
        return "onDisplayConfigurationChanged";
      } 
      return "onDisplayAdded";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.view.IDisplayWindowListener");
                return true;
              } 
              param1Parcel1.enforceInterface("android.view.IDisplayWindowListener");
              param1Int1 = param1Parcel1.readInt();
              onFixedRotationFinished(param1Int1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.view.IDisplayWindowListener");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onFixedRotationStarted(param1Int1, param1Int2);
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.IDisplayWindowListener");
          param1Int1 = param1Parcel1.readInt();
          onDisplayRemoved(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IDisplayWindowListener");
        param1Int1 = param1Parcel1.readInt();
        if (param1Parcel1.readInt() != 0) {
          Configuration configuration = (Configuration)Configuration.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onDisplayConfigurationChanged(param1Int1, (Configuration)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IDisplayWindowListener");
      param1Int1 = param1Parcel1.readInt();
      onDisplayAdded(param1Int1);
      return true;
    }
    
    private static class Proxy implements IDisplayWindowListener {
      public static IDisplayWindowListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IDisplayWindowListener";
      }
      
      public void onDisplayAdded(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDisplayWindowListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IDisplayWindowListener.Stub.getDefaultImpl() != null) {
            IDisplayWindowListener.Stub.getDefaultImpl().onDisplayAdded(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisplayConfigurationChanged(int param2Int, Configuration param2Configuration) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDisplayWindowListener");
          parcel.writeInt(param2Int);
          if (param2Configuration != null) {
            parcel.writeInt(1);
            param2Configuration.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IDisplayWindowListener.Stub.getDefaultImpl() != null) {
            IDisplayWindowListener.Stub.getDefaultImpl().onDisplayConfigurationChanged(param2Int, param2Configuration);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisplayRemoved(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDisplayWindowListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IDisplayWindowListener.Stub.getDefaultImpl() != null) {
            IDisplayWindowListener.Stub.getDefaultImpl().onDisplayRemoved(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFixedRotationStarted(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDisplayWindowListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IDisplayWindowListener.Stub.getDefaultImpl() != null) {
            IDisplayWindowListener.Stub.getDefaultImpl().onFixedRotationStarted(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFixedRotationFinished(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDisplayWindowListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IDisplayWindowListener.Stub.getDefaultImpl() != null) {
            IDisplayWindowListener.Stub.getDefaultImpl().onFixedRotationFinished(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDisplayWindowListener param1IDisplayWindowListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDisplayWindowListener != null) {
          Proxy.sDefaultImpl = param1IDisplayWindowListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDisplayWindowListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
