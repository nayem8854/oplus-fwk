package android.view;

import android.app.IAssistDataReceiver;
import android.graphics.Bitmap;
import android.graphics.Insets;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.IRemoteCallback;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;
import com.android.internal.os.IResultReceiver;
import com.android.internal.policy.IKeyguardDismissCallback;
import com.android.internal.policy.IShortcutService;

public interface IWindowManager extends IInterface {
  public static final int FIXED_TO_USER_ROTATION_DEFAULT = 0;
  
  public static final int FIXED_TO_USER_ROTATION_DISABLED = 1;
  
  public static final int FIXED_TO_USER_ROTATION_ENABLED = 2;
  
  SurfaceControl addShellRoot(int paramInt1, IWindow paramIWindow, int paramInt2) throws RemoteException;
  
  void addWindowToken(IBinder paramIBinder, int paramInt1, int paramInt2) throws RemoteException;
  
  int addWindowTokenWithOptions(IBinder paramIBinder, int paramInt1, int paramInt2, Bundle paramBundle, String paramString) throws RemoteException;
  
  void clearForcedDisplayDensityForUser(int paramInt1, int paramInt2) throws RemoteException;
  
  void clearForcedDisplaySize(int paramInt) throws RemoteException;
  
  boolean clearWindowContentFrameStats(IBinder paramIBinder) throws RemoteException;
  
  void closeSystemDialogs(String paramString) throws RemoteException;
  
  void createInputConsumer(IBinder paramIBinder, String paramString, int paramInt, InputChannel paramInputChannel) throws RemoteException;
  
  boolean destroyInputConsumer(String paramString, int paramInt) throws RemoteException;
  
  void disableKeyguard(IBinder paramIBinder, String paramString, int paramInt) throws RemoteException;
  
  void dismissKeyguard(IKeyguardDismissCallback paramIKeyguardDismissCallback, CharSequence paramCharSequence) throws RemoteException;
  
  void dontOverrideDisplayInfo(int paramInt) throws RemoteException;
  
  void enableScreenIfNeeded() throws RemoteException;
  
  void endProlongedAnimations() throws RemoteException;
  
  void executeAppTransition() throws RemoteException;
  
  void exitKeyguardSecurely(IOnKeyguardExitResult paramIOnKeyguardExitResult) throws RemoteException;
  
  void freezeDisplayRotation(int paramInt1, int paramInt2) throws RemoteException;
  
  void freezeRotation(int paramInt) throws RemoteException;
  
  float getAnimationScale(int paramInt) throws RemoteException;
  
  float[] getAnimationScales() throws RemoteException;
  
  int getBaseDisplayDensity(int paramInt) throws RemoteException;
  
  void getBaseDisplaySize(int paramInt, Point paramPoint) throws RemoteException;
  
  float getCurrentAnimatorScale() throws RemoteException;
  
  Region getCurrentImeTouchRegion() throws RemoteException;
  
  int getDefaultDisplayRotation() throws RemoteException;
  
  int getDockedStackSide() throws RemoteException;
  
  int getFocusedWindowIgnoreHomeMenuKey() throws RemoteException;
  
  int getImeBgColorFromAdaptation(String paramString) throws RemoteException;
  
  int getInitialDisplayDensity(int paramInt) throws RemoteException;
  
  void getInitialDisplaySize(int paramInt, Point paramPoint) throws RemoteException;
  
  int getNavBarColorFromAdaptation(String paramString1, String paramString2) throws RemoteException;
  
  int getNavBarPosition(int paramInt) throws RemoteException;
  
  int getPreferredOptionsPanelGravity(int paramInt) throws RemoteException;
  
  int getRemoveContentMode(int paramInt) throws RemoteException;
  
  void getStableInsets(int paramInt, Rect paramRect) throws RemoteException;
  
  int getStatusBarColorFromAdaptation(String paramString1, String paramString2) throws RemoteException;
  
  int getTypedWindowLayer(int paramInt) throws RemoteException;
  
  WindowContentFrameStats getWindowContentFrameStats(IBinder paramIBinder) throws RemoteException;
  
  boolean getWindowInsets(WindowManager.LayoutParams paramLayoutParams, int paramInt, Rect paramRect1, Rect paramRect2, DisplayCutout.ParcelableWrapper paramParcelableWrapper, InsetsState paramInsetsState) throws RemoteException;
  
  int getWindowingMode(int paramInt) throws RemoteException;
  
  boolean hasNavigationBar(int paramInt) throws RemoteException;
  
  void hideTransientBars(int paramInt) throws RemoteException;
  
  boolean injectInputAfterTransactionsApplied(InputEvent paramInputEvent, int paramInt) throws RemoteException;
  
  boolean isActivityNeedPalette(String paramString1, String paramString2) throws RemoteException;
  
  boolean isDisplayRotationFrozen(int paramInt) throws RemoteException;
  
  boolean isKeyguardLocked() throws RemoteException;
  
  boolean isKeyguardSecure(int paramInt) throws RemoteException;
  
  boolean isLayerTracing() throws RemoteException;
  
  boolean isRotationFrozen() throws RemoteException;
  
  boolean isSafeModeEnabled() throws RemoteException;
  
  boolean isViewServerRunning() throws RemoteException;
  
  boolean isWindowToken(IBinder paramIBinder) throws RemoteException;
  
  boolean isWindowTraceEnabled() throws RemoteException;
  
  void lockNow(Bundle paramBundle) throws RemoteException;
  
  boolean mirrorDisplay(int paramInt, SurfaceControl paramSurfaceControl) throws RemoteException;
  
  void modifyDisplayWindowInsets(int paramInt, InsetsState paramInsetsState) throws RemoteException;
  
  IWindowSession openSession(IWindowSessionCallback paramIWindowSessionCallback) throws RemoteException;
  
  void overridePendingAppTransitionMultiThumbFuture(IAppTransitionAnimationSpecsFuture paramIAppTransitionAnimationSpecsFuture, IRemoteCallback paramIRemoteCallback, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void overridePendingAppTransitionRemote(RemoteAnimationAdapter paramRemoteAnimationAdapter, int paramInt) throws RemoteException;
  
  void prepareAppTransition(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void reenableKeyguard(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void refreshScreenCaptureDisabled(int paramInt) throws RemoteException;
  
  void registerDisplayFoldListener(IDisplayFoldListener paramIDisplayFoldListener) throws RemoteException;
  
  void registerDisplayWindowListener(IDisplayWindowListener paramIDisplayWindowListener) throws RemoteException;
  
  void registerOppoWindowStateObserver(IOplusWindowStateObserver paramIOplusWindowStateObserver) throws RemoteException;
  
  void registerPinnedStackListener(int paramInt, IPinnedStackListener paramIPinnedStackListener) throws RemoteException;
  
  void registerShortcutKey(long paramLong, IShortcutService paramIShortcutService) throws RemoteException;
  
  void registerSystemGestureExclusionListener(ISystemGestureExclusionListener paramISystemGestureExclusionListener, int paramInt) throws RemoteException;
  
  boolean registerWallpaperVisibilityListener(IWallpaperVisibilityListener paramIWallpaperVisibilityListener, int paramInt) throws RemoteException;
  
  void removeRotationWatcher(IRotationWatcher paramIRotationWatcher) throws RemoteException;
  
  void removeWindowToken(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void requestAppKeyboardShortcuts(IResultReceiver paramIResultReceiver, int paramInt) throws RemoteException;
  
  boolean requestAssistScreenshot(IAssistDataReceiver paramIAssistDataReceiver) throws RemoteException;
  
  void requestScrollCapture(int paramInt1, IBinder paramIBinder, int paramInt2, IScrollCaptureController paramIScrollCaptureController) throws RemoteException;
  
  Bitmap screenshotWallpaper() throws RemoteException;
  
  void sendWakeUpReasonToKeyguard(String paramString) throws RemoteException;
  
  void setAnimationScale(int paramInt, float paramFloat) throws RemoteException;
  
  void setAnimationScales(float[] paramArrayOffloat) throws RemoteException;
  
  void setDisplayWindowInsetsController(int paramInt, IDisplayWindowInsetsController paramIDisplayWindowInsetsController) throws RemoteException;
  
  void setDisplayWindowRotationController(IDisplayWindowRotationController paramIDisplayWindowRotationController) throws RemoteException;
  
  void setDockedStackDividerTouchRegion(Rect paramRect) throws RemoteException;
  
  void setEventDispatching(boolean paramBoolean) throws RemoteException;
  
  void setFixedToUserRotation(int paramInt1, int paramInt2) throws RemoteException;
  
  void setForceShowSystemBars(boolean paramBoolean) throws RemoteException;
  
  void setForcedDisplayDensityForUser(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setForcedDisplayScalingMode(int paramInt1, int paramInt2) throws RemoteException;
  
  void setForcedDisplaySize(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setForwardedInsets(int paramInt, Insets paramInsets) throws RemoteException;
  
  void setInTouchMode(boolean paramBoolean) throws RemoteException;
  
  void setLayerTracing(boolean paramBoolean) throws RemoteException;
  
  void setLayerTracingFlags(int paramInt) throws RemoteException;
  
  void setNavBarVirtualKeyHapticFeedbackEnabled(boolean paramBoolean) throws RemoteException;
  
  void setPipVisibility(boolean paramBoolean) throws RemoteException;
  
  void setRecentsVisibility(boolean paramBoolean) throws RemoteException;
  
  void setRemoveContentMode(int paramInt1, int paramInt2) throws RemoteException;
  
  void setShellRootAccessibilityWindow(int paramInt1, int paramInt2, IWindow paramIWindow) throws RemoteException;
  
  void setShouldShowIme(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setShouldShowSystemDecors(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setShouldShowWithInsecureKeyguard(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setStrictModeVisualIndicatorPreference(String paramString) throws RemoteException;
  
  void setSwitchingUser(boolean paramBoolean) throws RemoteException;
  
  void setWindowingMode(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean shouldShowIme(int paramInt) throws RemoteException;
  
  boolean shouldShowSystemDecors(int paramInt) throws RemoteException;
  
  boolean shouldShowWithInsecureKeyguard(int paramInt) throws RemoteException;
  
  void showGlobalActions() throws RemoteException;
  
  void showStrictModeViolation(boolean paramBoolean) throws RemoteException;
  
  void startFreezingScreen(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean startViewServer(int paramInt) throws RemoteException;
  
  void startWindowTrace() throws RemoteException;
  
  void statusBarVisibilityChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void stopFreezingScreen() throws RemoteException;
  
  boolean stopViewServer() throws RemoteException;
  
  void stopWindowTrace() throws RemoteException;
  
  void syncInputTransactions() throws RemoteException;
  
  void thawDisplayRotation(int paramInt) throws RemoteException;
  
  void thawRotation() throws RemoteException;
  
  void unregisterDisplayFoldListener(IDisplayFoldListener paramIDisplayFoldListener) throws RemoteException;
  
  void unregisterDisplayWindowListener(IDisplayWindowListener paramIDisplayWindowListener) throws RemoteException;
  
  void unregisterOppoWindowStateObserver(IOplusWindowStateObserver paramIOplusWindowStateObserver) throws RemoteException;
  
  void unregisterSystemGestureExclusionListener(ISystemGestureExclusionListener paramISystemGestureExclusionListener, int paramInt) throws RemoteException;
  
  void unregisterWallpaperVisibilityListener(IWallpaperVisibilityListener paramIWallpaperVisibilityListener, int paramInt) throws RemoteException;
  
  void updateRotation(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  boolean useBLAST() throws RemoteException;
  
  int watchRotation(IRotationWatcher paramIRotationWatcher, int paramInt) throws RemoteException;
  
  class Default implements IWindowManager {
    public boolean startViewServer(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean stopViewServer() throws RemoteException {
      return false;
    }
    
    public boolean isViewServerRunning() throws RemoteException {
      return false;
    }
    
    public IWindowSession openSession(IWindowSessionCallback param1IWindowSessionCallback) throws RemoteException {
      return null;
    }
    
    public boolean useBLAST() throws RemoteException {
      return false;
    }
    
    public void getInitialDisplaySize(int param1Int, Point param1Point) throws RemoteException {}
    
    public void getBaseDisplaySize(int param1Int, Point param1Point) throws RemoteException {}
    
    public void setForcedDisplaySize(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void clearForcedDisplaySize(int param1Int) throws RemoteException {}
    
    public int getInitialDisplayDensity(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getBaseDisplayDensity(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setForcedDisplayDensityForUser(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void clearForcedDisplayDensityForUser(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setForcedDisplayScalingMode(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setEventDispatching(boolean param1Boolean) throws RemoteException {}
    
    public boolean isWindowToken(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public int addWindowTokenWithOptions(IBinder param1IBinder, int param1Int1, int param1Int2, Bundle param1Bundle, String param1String) throws RemoteException {
      return 0;
    }
    
    public void addWindowToken(IBinder param1IBinder, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void removeWindowToken(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void prepareAppTransition(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setDisplayWindowRotationController(IDisplayWindowRotationController param1IDisplayWindowRotationController) throws RemoteException {}
    
    public SurfaceControl addShellRoot(int param1Int1, IWindow param1IWindow, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void setShellRootAccessibilityWindow(int param1Int1, int param1Int2, IWindow param1IWindow) throws RemoteException {}
    
    public void overridePendingAppTransitionMultiThumbFuture(IAppTransitionAnimationSpecsFuture param1IAppTransitionAnimationSpecsFuture, IRemoteCallback param1IRemoteCallback, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void overridePendingAppTransitionRemote(RemoteAnimationAdapter param1RemoteAnimationAdapter, int param1Int) throws RemoteException {}
    
    public void executeAppTransition() throws RemoteException {}
    
    public void endProlongedAnimations() throws RemoteException {}
    
    public void startFreezingScreen(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void stopFreezingScreen() throws RemoteException {}
    
    public void disableKeyguard(IBinder param1IBinder, String param1String, int param1Int) throws RemoteException {}
    
    public void reenableKeyguard(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void exitKeyguardSecurely(IOnKeyguardExitResult param1IOnKeyguardExitResult) throws RemoteException {}
    
    public boolean isKeyguardLocked() throws RemoteException {
      return false;
    }
    
    public boolean isKeyguardSecure(int param1Int) throws RemoteException {
      return false;
    }
    
    public void dismissKeyguard(IKeyguardDismissCallback param1IKeyguardDismissCallback, CharSequence param1CharSequence) throws RemoteException {}
    
    public void setSwitchingUser(boolean param1Boolean) throws RemoteException {}
    
    public void closeSystemDialogs(String param1String) throws RemoteException {}
    
    public float getAnimationScale(int param1Int) throws RemoteException {
      return 0.0F;
    }
    
    public float[] getAnimationScales() throws RemoteException {
      return null;
    }
    
    public void setAnimationScale(int param1Int, float param1Float) throws RemoteException {}
    
    public void setAnimationScales(float[] param1ArrayOffloat) throws RemoteException {}
    
    public float getCurrentAnimatorScale() throws RemoteException {
      return 0.0F;
    }
    
    public void setInTouchMode(boolean param1Boolean) throws RemoteException {}
    
    public void showStrictModeViolation(boolean param1Boolean) throws RemoteException {}
    
    public void setStrictModeVisualIndicatorPreference(String param1String) throws RemoteException {}
    
    public void refreshScreenCaptureDisabled(int param1Int) throws RemoteException {}
    
    public void updateRotation(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public int getDefaultDisplayRotation() throws RemoteException {
      return 0;
    }
    
    public int watchRotation(IRotationWatcher param1IRotationWatcher, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void removeRotationWatcher(IRotationWatcher param1IRotationWatcher) throws RemoteException {}
    
    public int getPreferredOptionsPanelGravity(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void freezeRotation(int param1Int) throws RemoteException {}
    
    public void thawRotation() throws RemoteException {}
    
    public boolean isRotationFrozen() throws RemoteException {
      return false;
    }
    
    public void freezeDisplayRotation(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void thawDisplayRotation(int param1Int) throws RemoteException {}
    
    public boolean isDisplayRotationFrozen(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setFixedToUserRotation(int param1Int1, int param1Int2) throws RemoteException {}
    
    public Bitmap screenshotWallpaper() throws RemoteException {
      return null;
    }
    
    public boolean registerWallpaperVisibilityListener(IWallpaperVisibilityListener param1IWallpaperVisibilityListener, int param1Int) throws RemoteException {
      return false;
    }
    
    public void unregisterWallpaperVisibilityListener(IWallpaperVisibilityListener param1IWallpaperVisibilityListener, int param1Int) throws RemoteException {}
    
    public void registerSystemGestureExclusionListener(ISystemGestureExclusionListener param1ISystemGestureExclusionListener, int param1Int) throws RemoteException {}
    
    public void unregisterSystemGestureExclusionListener(ISystemGestureExclusionListener param1ISystemGestureExclusionListener, int param1Int) throws RemoteException {}
    
    public boolean requestAssistScreenshot(IAssistDataReceiver param1IAssistDataReceiver) throws RemoteException {
      return false;
    }
    
    public void statusBarVisibilityChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void hideTransientBars(int param1Int) throws RemoteException {}
    
    public void setForceShowSystemBars(boolean param1Boolean) throws RemoteException {}
    
    public void setRecentsVisibility(boolean param1Boolean) throws RemoteException {}
    
    public void setPipVisibility(boolean param1Boolean) throws RemoteException {}
    
    public void setNavBarVirtualKeyHapticFeedbackEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean hasNavigationBar(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getNavBarPosition(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void lockNow(Bundle param1Bundle) throws RemoteException {}
    
    public boolean isSafeModeEnabled() throws RemoteException {
      return false;
    }
    
    public void enableScreenIfNeeded() throws RemoteException {}
    
    public boolean clearWindowContentFrameStats(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public WindowContentFrameStats getWindowContentFrameStats(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public int getDockedStackSide() throws RemoteException {
      return 0;
    }
    
    public void setDockedStackDividerTouchRegion(Rect param1Rect) throws RemoteException {}
    
    public void registerPinnedStackListener(int param1Int, IPinnedStackListener param1IPinnedStackListener) throws RemoteException {}
    
    public void requestAppKeyboardShortcuts(IResultReceiver param1IResultReceiver, int param1Int) throws RemoteException {}
    
    public void getStableInsets(int param1Int, Rect param1Rect) throws RemoteException {}
    
    public void setForwardedInsets(int param1Int, Insets param1Insets) throws RemoteException {}
    
    public void registerShortcutKey(long param1Long, IShortcutService param1IShortcutService) throws RemoteException {}
    
    public void createInputConsumer(IBinder param1IBinder, String param1String, int param1Int, InputChannel param1InputChannel) throws RemoteException {}
    
    public boolean destroyInputConsumer(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public Region getCurrentImeTouchRegion() throws RemoteException {
      return null;
    }
    
    public void registerDisplayFoldListener(IDisplayFoldListener param1IDisplayFoldListener) throws RemoteException {}
    
    public void unregisterDisplayFoldListener(IDisplayFoldListener param1IDisplayFoldListener) throws RemoteException {}
    
    public void registerDisplayWindowListener(IDisplayWindowListener param1IDisplayWindowListener) throws RemoteException {}
    
    public void unregisterDisplayWindowListener(IDisplayWindowListener param1IDisplayWindowListener) throws RemoteException {}
    
    public void startWindowTrace() throws RemoteException {}
    
    public void stopWindowTrace() throws RemoteException {}
    
    public boolean isWindowTraceEnabled() throws RemoteException {
      return false;
    }
    
    public void dontOverrideDisplayInfo(int param1Int) throws RemoteException {}
    
    public void registerOppoWindowStateObserver(IOplusWindowStateObserver param1IOplusWindowStateObserver) throws RemoteException {}
    
    public void unregisterOppoWindowStateObserver(IOplusWindowStateObserver param1IOplusWindowStateObserver) throws RemoteException {}
    
    public int getWindowingMode(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setWindowingMode(int param1Int1, int param1Int2) throws RemoteException {}
    
    public int getRemoveContentMode(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setRemoveContentMode(int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean shouldShowWithInsecureKeyguard(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setShouldShowWithInsecureKeyguard(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean shouldShowSystemDecors(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setShouldShowSystemDecors(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean shouldShowIme(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setShouldShowIme(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean injectInputAfterTransactionsApplied(InputEvent param1InputEvent, int param1Int) throws RemoteException {
      return false;
    }
    
    public void syncInputTransactions() throws RemoteException {}
    
    public boolean isLayerTracing() throws RemoteException {
      return false;
    }
    
    public void setLayerTracing(boolean param1Boolean) throws RemoteException {}
    
    public boolean mirrorDisplay(int param1Int, SurfaceControl param1SurfaceControl) throws RemoteException {
      return false;
    }
    
    public void setDisplayWindowInsetsController(int param1Int, IDisplayWindowInsetsController param1IDisplayWindowInsetsController) throws RemoteException {}
    
    public void modifyDisplayWindowInsets(int param1Int, InsetsState param1InsetsState) throws RemoteException {}
    
    public boolean getWindowInsets(WindowManager.LayoutParams param1LayoutParams, int param1Int, Rect param1Rect1, Rect param1Rect2, DisplayCutout.ParcelableWrapper param1ParcelableWrapper, InsetsState param1InsetsState) throws RemoteException {
      return false;
    }
    
    public void showGlobalActions() throws RemoteException {}
    
    public boolean isActivityNeedPalette(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public int getNavBarColorFromAdaptation(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getStatusBarColorFromAdaptation(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getImeBgColorFromAdaptation(String param1String) throws RemoteException {
      return 0;
    }
    
    public int getTypedWindowLayer(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getFocusedWindowIgnoreHomeMenuKey() throws RemoteException {
      return 0;
    }
    
    public void setLayerTracingFlags(int param1Int) throws RemoteException {}
    
    public void requestScrollCapture(int param1Int1, IBinder param1IBinder, int param1Int2, IScrollCaptureController param1IScrollCaptureController) throws RemoteException {}
    
    public void sendWakeUpReasonToKeyguard(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowManager {
    private static final String DESCRIPTOR = "android.view.IWindowManager";
    
    static final int TRANSACTION_addShellRoot = 22;
    
    static final int TRANSACTION_addWindowToken = 18;
    
    static final int TRANSACTION_addWindowTokenWithOptions = 17;
    
    static final int TRANSACTION_clearForcedDisplayDensityForUser = 13;
    
    static final int TRANSACTION_clearForcedDisplaySize = 9;
    
    static final int TRANSACTION_clearWindowContentFrameStats = 76;
    
    static final int TRANSACTION_closeSystemDialogs = 37;
    
    static final int TRANSACTION_createInputConsumer = 85;
    
    static final int TRANSACTION_destroyInputConsumer = 86;
    
    static final int TRANSACTION_disableKeyguard = 30;
    
    static final int TRANSACTION_dismissKeyguard = 35;
    
    static final int TRANSACTION_dontOverrideDisplayInfo = 95;
    
    static final int TRANSACTION_enableScreenIfNeeded = 75;
    
    static final int TRANSACTION_endProlongedAnimations = 27;
    
    static final int TRANSACTION_executeAppTransition = 26;
    
    static final int TRANSACTION_exitKeyguardSecurely = 32;
    
    static final int TRANSACTION_freezeDisplayRotation = 55;
    
    static final int TRANSACTION_freezeRotation = 52;
    
    static final int TRANSACTION_getAnimationScale = 38;
    
    static final int TRANSACTION_getAnimationScales = 39;
    
    static final int TRANSACTION_getBaseDisplayDensity = 11;
    
    static final int TRANSACTION_getBaseDisplaySize = 7;
    
    static final int TRANSACTION_getCurrentAnimatorScale = 42;
    
    static final int TRANSACTION_getCurrentImeTouchRegion = 87;
    
    static final int TRANSACTION_getDefaultDisplayRotation = 48;
    
    static final int TRANSACTION_getDockedStackSide = 78;
    
    static final int TRANSACTION_getFocusedWindowIgnoreHomeMenuKey = 122;
    
    static final int TRANSACTION_getImeBgColorFromAdaptation = 120;
    
    static final int TRANSACTION_getInitialDisplayDensity = 10;
    
    static final int TRANSACTION_getInitialDisplaySize = 6;
    
    static final int TRANSACTION_getNavBarColorFromAdaptation = 118;
    
    static final int TRANSACTION_getNavBarPosition = 72;
    
    static final int TRANSACTION_getPreferredOptionsPanelGravity = 51;
    
    static final int TRANSACTION_getRemoveContentMode = 100;
    
    static final int TRANSACTION_getStableInsets = 82;
    
    static final int TRANSACTION_getStatusBarColorFromAdaptation = 119;
    
    static final int TRANSACTION_getTypedWindowLayer = 121;
    
    static final int TRANSACTION_getWindowContentFrameStats = 77;
    
    static final int TRANSACTION_getWindowInsets = 115;
    
    static final int TRANSACTION_getWindowingMode = 98;
    
    static final int TRANSACTION_hasNavigationBar = 71;
    
    static final int TRANSACTION_hideTransientBars = 66;
    
    static final int TRANSACTION_injectInputAfterTransactionsApplied = 108;
    
    static final int TRANSACTION_isActivityNeedPalette = 117;
    
    static final int TRANSACTION_isDisplayRotationFrozen = 57;
    
    static final int TRANSACTION_isKeyguardLocked = 33;
    
    static final int TRANSACTION_isKeyguardSecure = 34;
    
    static final int TRANSACTION_isLayerTracing = 110;
    
    static final int TRANSACTION_isRotationFrozen = 54;
    
    static final int TRANSACTION_isSafeModeEnabled = 74;
    
    static final int TRANSACTION_isViewServerRunning = 3;
    
    static final int TRANSACTION_isWindowToken = 16;
    
    static final int TRANSACTION_isWindowTraceEnabled = 94;
    
    static final int TRANSACTION_lockNow = 73;
    
    static final int TRANSACTION_mirrorDisplay = 112;
    
    static final int TRANSACTION_modifyDisplayWindowInsets = 114;
    
    static final int TRANSACTION_openSession = 4;
    
    static final int TRANSACTION_overridePendingAppTransitionMultiThumbFuture = 24;
    
    static final int TRANSACTION_overridePendingAppTransitionRemote = 25;
    
    static final int TRANSACTION_prepareAppTransition = 20;
    
    static final int TRANSACTION_reenableKeyguard = 31;
    
    static final int TRANSACTION_refreshScreenCaptureDisabled = 46;
    
    static final int TRANSACTION_registerDisplayFoldListener = 88;
    
    static final int TRANSACTION_registerDisplayWindowListener = 90;
    
    static final int TRANSACTION_registerOppoWindowStateObserver = 96;
    
    static final int TRANSACTION_registerPinnedStackListener = 80;
    
    static final int TRANSACTION_registerShortcutKey = 84;
    
    static final int TRANSACTION_registerSystemGestureExclusionListener = 62;
    
    static final int TRANSACTION_registerWallpaperVisibilityListener = 60;
    
    static final int TRANSACTION_removeRotationWatcher = 50;
    
    static final int TRANSACTION_removeWindowToken = 19;
    
    static final int TRANSACTION_requestAppKeyboardShortcuts = 81;
    
    static final int TRANSACTION_requestAssistScreenshot = 64;
    
    static final int TRANSACTION_requestScrollCapture = 124;
    
    static final int TRANSACTION_screenshotWallpaper = 59;
    
    static final int TRANSACTION_sendWakeUpReasonToKeyguard = 125;
    
    static final int TRANSACTION_setAnimationScale = 40;
    
    static final int TRANSACTION_setAnimationScales = 41;
    
    static final int TRANSACTION_setDisplayWindowInsetsController = 113;
    
    static final int TRANSACTION_setDisplayWindowRotationController = 21;
    
    static final int TRANSACTION_setDockedStackDividerTouchRegion = 79;
    
    static final int TRANSACTION_setEventDispatching = 15;
    
    static final int TRANSACTION_setFixedToUserRotation = 58;
    
    static final int TRANSACTION_setForceShowSystemBars = 67;
    
    static final int TRANSACTION_setForcedDisplayDensityForUser = 12;
    
    static final int TRANSACTION_setForcedDisplayScalingMode = 14;
    
    static final int TRANSACTION_setForcedDisplaySize = 8;
    
    static final int TRANSACTION_setForwardedInsets = 83;
    
    static final int TRANSACTION_setInTouchMode = 43;
    
    static final int TRANSACTION_setLayerTracing = 111;
    
    static final int TRANSACTION_setLayerTracingFlags = 123;
    
    static final int TRANSACTION_setNavBarVirtualKeyHapticFeedbackEnabled = 70;
    
    static final int TRANSACTION_setPipVisibility = 69;
    
    static final int TRANSACTION_setRecentsVisibility = 68;
    
    static final int TRANSACTION_setRemoveContentMode = 101;
    
    static final int TRANSACTION_setShellRootAccessibilityWindow = 23;
    
    static final int TRANSACTION_setShouldShowIme = 107;
    
    static final int TRANSACTION_setShouldShowSystemDecors = 105;
    
    static final int TRANSACTION_setShouldShowWithInsecureKeyguard = 103;
    
    static final int TRANSACTION_setStrictModeVisualIndicatorPreference = 45;
    
    static final int TRANSACTION_setSwitchingUser = 36;
    
    static final int TRANSACTION_setWindowingMode = 99;
    
    static final int TRANSACTION_shouldShowIme = 106;
    
    static final int TRANSACTION_shouldShowSystemDecors = 104;
    
    static final int TRANSACTION_shouldShowWithInsecureKeyguard = 102;
    
    static final int TRANSACTION_showGlobalActions = 116;
    
    static final int TRANSACTION_showStrictModeViolation = 44;
    
    static final int TRANSACTION_startFreezingScreen = 28;
    
    static final int TRANSACTION_startViewServer = 1;
    
    static final int TRANSACTION_startWindowTrace = 92;
    
    static final int TRANSACTION_statusBarVisibilityChanged = 65;
    
    static final int TRANSACTION_stopFreezingScreen = 29;
    
    static final int TRANSACTION_stopViewServer = 2;
    
    static final int TRANSACTION_stopWindowTrace = 93;
    
    static final int TRANSACTION_syncInputTransactions = 109;
    
    static final int TRANSACTION_thawDisplayRotation = 56;
    
    static final int TRANSACTION_thawRotation = 53;
    
    static final int TRANSACTION_unregisterDisplayFoldListener = 89;
    
    static final int TRANSACTION_unregisterDisplayWindowListener = 91;
    
    static final int TRANSACTION_unregisterOppoWindowStateObserver = 97;
    
    static final int TRANSACTION_unregisterSystemGestureExclusionListener = 63;
    
    static final int TRANSACTION_unregisterWallpaperVisibilityListener = 61;
    
    static final int TRANSACTION_updateRotation = 47;
    
    static final int TRANSACTION_useBLAST = 5;
    
    static final int TRANSACTION_watchRotation = 49;
    
    public Stub() {
      attachInterface(this, "android.view.IWindowManager");
    }
    
    public static IWindowManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IWindowManager");
      if (iInterface != null && iInterface instanceof IWindowManager)
        return (IWindowManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 125:
          return "sendWakeUpReasonToKeyguard";
        case 124:
          return "requestScrollCapture";
        case 123:
          return "setLayerTracingFlags";
        case 122:
          return "getFocusedWindowIgnoreHomeMenuKey";
        case 121:
          return "getTypedWindowLayer";
        case 120:
          return "getImeBgColorFromAdaptation";
        case 119:
          return "getStatusBarColorFromAdaptation";
        case 118:
          return "getNavBarColorFromAdaptation";
        case 117:
          return "isActivityNeedPalette";
        case 116:
          return "showGlobalActions";
        case 115:
          return "getWindowInsets";
        case 114:
          return "modifyDisplayWindowInsets";
        case 113:
          return "setDisplayWindowInsetsController";
        case 112:
          return "mirrorDisplay";
        case 111:
          return "setLayerTracing";
        case 110:
          return "isLayerTracing";
        case 109:
          return "syncInputTransactions";
        case 108:
          return "injectInputAfterTransactionsApplied";
        case 107:
          return "setShouldShowIme";
        case 106:
          return "shouldShowIme";
        case 105:
          return "setShouldShowSystemDecors";
        case 104:
          return "shouldShowSystemDecors";
        case 103:
          return "setShouldShowWithInsecureKeyguard";
        case 102:
          return "shouldShowWithInsecureKeyguard";
        case 101:
          return "setRemoveContentMode";
        case 100:
          return "getRemoveContentMode";
        case 99:
          return "setWindowingMode";
        case 98:
          return "getWindowingMode";
        case 97:
          return "unregisterOppoWindowStateObserver";
        case 96:
          return "registerOppoWindowStateObserver";
        case 95:
          return "dontOverrideDisplayInfo";
        case 94:
          return "isWindowTraceEnabled";
        case 93:
          return "stopWindowTrace";
        case 92:
          return "startWindowTrace";
        case 91:
          return "unregisterDisplayWindowListener";
        case 90:
          return "registerDisplayWindowListener";
        case 89:
          return "unregisterDisplayFoldListener";
        case 88:
          return "registerDisplayFoldListener";
        case 87:
          return "getCurrentImeTouchRegion";
        case 86:
          return "destroyInputConsumer";
        case 85:
          return "createInputConsumer";
        case 84:
          return "registerShortcutKey";
        case 83:
          return "setForwardedInsets";
        case 82:
          return "getStableInsets";
        case 81:
          return "requestAppKeyboardShortcuts";
        case 80:
          return "registerPinnedStackListener";
        case 79:
          return "setDockedStackDividerTouchRegion";
        case 78:
          return "getDockedStackSide";
        case 77:
          return "getWindowContentFrameStats";
        case 76:
          return "clearWindowContentFrameStats";
        case 75:
          return "enableScreenIfNeeded";
        case 74:
          return "isSafeModeEnabled";
        case 73:
          return "lockNow";
        case 72:
          return "getNavBarPosition";
        case 71:
          return "hasNavigationBar";
        case 70:
          return "setNavBarVirtualKeyHapticFeedbackEnabled";
        case 69:
          return "setPipVisibility";
        case 68:
          return "setRecentsVisibility";
        case 67:
          return "setForceShowSystemBars";
        case 66:
          return "hideTransientBars";
        case 65:
          return "statusBarVisibilityChanged";
        case 64:
          return "requestAssistScreenshot";
        case 63:
          return "unregisterSystemGestureExclusionListener";
        case 62:
          return "registerSystemGestureExclusionListener";
        case 61:
          return "unregisterWallpaperVisibilityListener";
        case 60:
          return "registerWallpaperVisibilityListener";
        case 59:
          return "screenshotWallpaper";
        case 58:
          return "setFixedToUserRotation";
        case 57:
          return "isDisplayRotationFrozen";
        case 56:
          return "thawDisplayRotation";
        case 55:
          return "freezeDisplayRotation";
        case 54:
          return "isRotationFrozen";
        case 53:
          return "thawRotation";
        case 52:
          return "freezeRotation";
        case 51:
          return "getPreferredOptionsPanelGravity";
        case 50:
          return "removeRotationWatcher";
        case 49:
          return "watchRotation";
        case 48:
          return "getDefaultDisplayRotation";
        case 47:
          return "updateRotation";
        case 46:
          return "refreshScreenCaptureDisabled";
        case 45:
          return "setStrictModeVisualIndicatorPreference";
        case 44:
          return "showStrictModeViolation";
        case 43:
          return "setInTouchMode";
        case 42:
          return "getCurrentAnimatorScale";
        case 41:
          return "setAnimationScales";
        case 40:
          return "setAnimationScale";
        case 39:
          return "getAnimationScales";
        case 38:
          return "getAnimationScale";
        case 37:
          return "closeSystemDialogs";
        case 36:
          return "setSwitchingUser";
        case 35:
          return "dismissKeyguard";
        case 34:
          return "isKeyguardSecure";
        case 33:
          return "isKeyguardLocked";
        case 32:
          return "exitKeyguardSecurely";
        case 31:
          return "reenableKeyguard";
        case 30:
          return "disableKeyguard";
        case 29:
          return "stopFreezingScreen";
        case 28:
          return "startFreezingScreen";
        case 27:
          return "endProlongedAnimations";
        case 26:
          return "executeAppTransition";
        case 25:
          return "overridePendingAppTransitionRemote";
        case 24:
          return "overridePendingAppTransitionMultiThumbFuture";
        case 23:
          return "setShellRootAccessibilityWindow";
        case 22:
          return "addShellRoot";
        case 21:
          return "setDisplayWindowRotationController";
        case 20:
          return "prepareAppTransition";
        case 19:
          return "removeWindowToken";
        case 18:
          return "addWindowToken";
        case 17:
          return "addWindowTokenWithOptions";
        case 16:
          return "isWindowToken";
        case 15:
          return "setEventDispatching";
        case 14:
          return "setForcedDisplayScalingMode";
        case 13:
          return "clearForcedDisplayDensityForUser";
        case 12:
          return "setForcedDisplayDensityForUser";
        case 11:
          return "getBaseDisplayDensity";
        case 10:
          return "getInitialDisplayDensity";
        case 9:
          return "clearForcedDisplaySize";
        case 8:
          return "setForcedDisplaySize";
        case 7:
          return "getBaseDisplaySize";
        case 6:
          return "getInitialDisplaySize";
        case 5:
          return "useBLAST";
        case 4:
          return "openSession";
        case 3:
          return "isViewServerRunning";
        case 2:
          return "stopViewServer";
        case 1:
          break;
      } 
      return "startViewServer";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool19;
        int i13;
        boolean bool18;
        int i12;
        boolean bool17;
        int i11;
        boolean bool16;
        int i10;
        boolean bool15;
        int i9;
        boolean bool14;
        int i8;
        boolean bool13;
        int i7;
        boolean bool12;
        int i6;
        boolean bool11;
        int i5;
        boolean bool10;
        int i4;
        boolean bool9;
        int i3;
        boolean bool8;
        int i2;
        boolean bool7;
        int i1;
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        String str5;
        IScrollCaptureController iScrollCaptureController;
        String str4;
        Rect rect2;
        IDisplayWindowInsetsController iDisplayWindowInsetsController;
        SurfaceControl surfaceControl2;
        IOplusWindowStateObserver iOplusWindowStateObserver;
        IDisplayWindowListener iDisplayWindowListener;
        IDisplayFoldListener iDisplayFoldListener;
        Region region;
        InputChannel inputChannel;
        IShortcutService iShortcutService;
        Rect rect1;
        IPinnedStackListener iPinnedStackListener;
        IBinder iBinder3;
        WindowContentFrameStats windowContentFrameStats;
        IBinder iBinder2;
        IAssistDataReceiver iAssistDataReceiver;
        Bitmap bitmap;
        IRotationWatcher iRotationWatcher1;
        String str3;
        float[] arrayOfFloat;
        String str2;
        IOnKeyguardExitResult iOnKeyguardExitResult;
        IWindow iWindow1;
        SurfaceControl surfaceControl1;
        IDisplayWindowRotationController iDisplayWindowRotationController;
        String str1;
        IBinder iBinder1;
        Point point;
        IWindowSessionCallback iWindowSessionCallback;
        IWindowSession iWindowSession;
        IBinder iBinder7;
        String str7;
        IBinder iBinder6;
        IResultReceiver iResultReceiver;
        ISystemGestureExclusionListener iSystemGestureExclusionListener;
        IWallpaperVisibilityListener iWallpaperVisibilityListener;
        IRotationWatcher iRotationWatcher2;
        IKeyguardDismissCallback iKeyguardDismissCallback;
        IBinder iBinder5;
        String str6;
        IAppTransitionAnimationSpecsFuture iAppTransitionAnimationSpecsFuture;
        IWindow iWindow2;
        IBinder iBinder4;
        Rect rect3;
        DisplayCutout.ParcelableWrapper parcelableWrapper;
        InsetsState insetsState;
        String str8;
        IBinder iBinder9;
        IRemoteCallback iRemoteCallback;
        IBinder iBinder8;
        long l;
        float f;
        int i14;
        boolean bool20 = false, bool21 = false, bool22 = false, bool23 = false, bool24 = false, bool25 = false, bool26 = false, bool27 = false, bool28 = false, bool29 = false, bool30 = false, bool31 = false, bool32 = false, bool33 = false, bool34 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 125:
            param1Parcel1.enforceInterface("android.view.IWindowManager");
            str5 = param1Parcel1.readString();
            sendWakeUpReasonToKeyguard(str5);
            param1Parcel2.writeNoException();
            return true;
          case 124:
            str5.enforceInterface("android.view.IWindowManager");
            param1Int1 = str5.readInt();
            iBinder7 = str5.readStrongBinder();
            param1Int2 = str5.readInt();
            iScrollCaptureController = IScrollCaptureController.Stub.asInterface(str5.readStrongBinder());
            requestScrollCapture(param1Int1, iBinder7, param1Int2, iScrollCaptureController);
            param1Parcel2.writeNoException();
            return true;
          case 123:
            iScrollCaptureController.enforceInterface("android.view.IWindowManager");
            param1Int1 = iScrollCaptureController.readInt();
            setLayerTracingFlags(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 122:
            iScrollCaptureController.enforceInterface("android.view.IWindowManager");
            param1Int1 = getFocusedWindowIgnoreHomeMenuKey();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 121:
            iScrollCaptureController.enforceInterface("android.view.IWindowManager");
            param1Int1 = iScrollCaptureController.readInt();
            param1Int1 = getTypedWindowLayer(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 120:
            iScrollCaptureController.enforceInterface("android.view.IWindowManager");
            str4 = iScrollCaptureController.readString();
            param1Int1 = getImeBgColorFromAdaptation(str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 119:
            str4.enforceInterface("android.view.IWindowManager");
            str7 = str4.readString();
            str4 = str4.readString();
            param1Int1 = getStatusBarColorFromAdaptation(str7, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 118:
            str4.enforceInterface("android.view.IWindowManager");
            str7 = str4.readString();
            str4 = str4.readString();
            param1Int1 = getNavBarColorFromAdaptation(str7, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 117:
            str4.enforceInterface("android.view.IWindowManager");
            str7 = str4.readString();
            str4 = str4.readString();
            bool19 = isActivityNeedPalette(str7, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 116:
            str4.enforceInterface("android.view.IWindowManager");
            showGlobalActions();
            param1Parcel2.writeNoException();
            return true;
          case 115:
            str4.enforceInterface("android.view.IWindowManager");
            if (str4.readInt() != 0) {
              WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams)WindowManager.LayoutParams.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str7 = null;
            } 
            i13 = str4.readInt();
            rect2 = new Rect();
            rect3 = new Rect();
            parcelableWrapper = new DisplayCutout.ParcelableWrapper();
            insetsState = new InsetsState();
            bool18 = getWindowInsets((WindowManager.LayoutParams)str7, i13, rect2, rect3, parcelableWrapper, insetsState);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            param1Parcel2.writeInt(1);
            rect2.writeToParcel(param1Parcel2, 1);
            param1Parcel2.writeInt(1);
            rect3.writeToParcel(param1Parcel2, 1);
            param1Parcel2.writeInt(1);
            parcelableWrapper.writeToParcel(param1Parcel2, 1);
            param1Parcel2.writeInt(1);
            insetsState.writeToParcel(param1Parcel2, 1);
            return true;
          case 114:
            rect2.enforceInterface("android.view.IWindowManager");
            i12 = rect2.readInt();
            if (rect2.readInt() != 0) {
              InsetsState insetsState1 = (InsetsState)InsetsState.CREATOR.createFromParcel((Parcel)rect2);
            } else {
              rect2 = null;
            } 
            modifyDisplayWindowInsets(i12, (InsetsState)rect2);
            param1Parcel2.writeNoException();
            return true;
          case 113:
            rect2.enforceInterface("android.view.IWindowManager");
            i12 = rect2.readInt();
            iDisplayWindowInsetsController = IDisplayWindowInsetsController.Stub.asInterface(rect2.readStrongBinder());
            setDisplayWindowInsetsController(i12, iDisplayWindowInsetsController);
            param1Parcel2.writeNoException();
            return true;
          case 112:
            iDisplayWindowInsetsController.enforceInterface("android.view.IWindowManager");
            i12 = iDisplayWindowInsetsController.readInt();
            surfaceControl2 = new SurfaceControl();
            bool17 = mirrorDisplay(i12, surfaceControl2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            param1Parcel2.writeInt(1);
            surfaceControl2.writeToParcel(param1Parcel2, 1);
            return true;
          case 111:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            bool31 = bool34;
            if (surfaceControl2.readInt() != 0)
              bool31 = true; 
            setLayerTracing(bool31);
            param1Parcel2.writeNoException();
            return true;
          case 110:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            bool17 = isLayerTracing();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 109:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            syncInputTransactions();
            param1Parcel2.writeNoException();
            return true;
          case 108:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            if (surfaceControl2.readInt() != 0) {
              InputEvent inputEvent = (InputEvent)InputEvent.CREATOR.createFromParcel((Parcel)surfaceControl2);
            } else {
              str7 = null;
            } 
            i11 = surfaceControl2.readInt();
            bool16 = injectInputAfterTransactionsApplied((InputEvent)str7, i11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 107:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i10 = surfaceControl2.readInt();
            bool31 = bool20;
            if (surfaceControl2.readInt() != 0)
              bool31 = true; 
            setShouldShowIme(i10, bool31);
            param1Parcel2.writeNoException();
            return true;
          case 106:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i10 = surfaceControl2.readInt();
            bool15 = shouldShowIme(i10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 105:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i9 = surfaceControl2.readInt();
            bool31 = bool21;
            if (surfaceControl2.readInt() != 0)
              bool31 = true; 
            setShouldShowSystemDecors(i9, bool31);
            param1Parcel2.writeNoException();
            return true;
          case 104:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i9 = surfaceControl2.readInt();
            bool14 = shouldShowSystemDecors(i9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 103:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i8 = surfaceControl2.readInt();
            bool31 = bool22;
            if (surfaceControl2.readInt() != 0)
              bool31 = true; 
            setShouldShowWithInsecureKeyguard(i8, bool31);
            param1Parcel2.writeNoException();
            return true;
          case 102:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i8 = surfaceControl2.readInt();
            bool13 = shouldShowWithInsecureKeyguard(i8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 101:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            param1Int2 = surfaceControl2.readInt();
            i7 = surfaceControl2.readInt();
            setRemoveContentMode(param1Int2, i7);
            param1Parcel2.writeNoException();
            return true;
          case 100:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i7 = surfaceControl2.readInt();
            i7 = getRemoveContentMode(i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 99:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i7 = surfaceControl2.readInt();
            param1Int2 = surfaceControl2.readInt();
            setWindowingMode(i7, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 98:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            i7 = surfaceControl2.readInt();
            i7 = getWindowingMode(i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 97:
            surfaceControl2.enforceInterface("android.view.IWindowManager");
            iOplusWindowStateObserver = IOplusWindowStateObserver.Stub.asInterface(surfaceControl2.readStrongBinder());
            unregisterOppoWindowStateObserver(iOplusWindowStateObserver);
            param1Parcel2.writeNoException();
            return true;
          case 96:
            iOplusWindowStateObserver.enforceInterface("android.view.IWindowManager");
            iOplusWindowStateObserver = IOplusWindowStateObserver.Stub.asInterface(iOplusWindowStateObserver.readStrongBinder());
            registerOppoWindowStateObserver(iOplusWindowStateObserver);
            param1Parcel2.writeNoException();
            return true;
          case 95:
            iOplusWindowStateObserver.enforceInterface("android.view.IWindowManager");
            i7 = iOplusWindowStateObserver.readInt();
            dontOverrideDisplayInfo(i7);
            param1Parcel2.writeNoException();
            return true;
          case 94:
            iOplusWindowStateObserver.enforceInterface("android.view.IWindowManager");
            bool12 = isWindowTraceEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 93:
            iOplusWindowStateObserver.enforceInterface("android.view.IWindowManager");
            stopWindowTrace();
            param1Parcel2.writeNoException();
            return true;
          case 92:
            iOplusWindowStateObserver.enforceInterface("android.view.IWindowManager");
            startWindowTrace();
            param1Parcel2.writeNoException();
            return true;
          case 91:
            iOplusWindowStateObserver.enforceInterface("android.view.IWindowManager");
            iDisplayWindowListener = IDisplayWindowListener.Stub.asInterface(iOplusWindowStateObserver.readStrongBinder());
            unregisterDisplayWindowListener(iDisplayWindowListener);
            param1Parcel2.writeNoException();
            return true;
          case 90:
            iDisplayWindowListener.enforceInterface("android.view.IWindowManager");
            iDisplayWindowListener = IDisplayWindowListener.Stub.asInterface(iDisplayWindowListener.readStrongBinder());
            registerDisplayWindowListener(iDisplayWindowListener);
            param1Parcel2.writeNoException();
            return true;
          case 89:
            iDisplayWindowListener.enforceInterface("android.view.IWindowManager");
            iDisplayFoldListener = IDisplayFoldListener.Stub.asInterface(iDisplayWindowListener.readStrongBinder());
            unregisterDisplayFoldListener(iDisplayFoldListener);
            param1Parcel2.writeNoException();
            return true;
          case 88:
            iDisplayFoldListener.enforceInterface("android.view.IWindowManager");
            iDisplayFoldListener = IDisplayFoldListener.Stub.asInterface(iDisplayFoldListener.readStrongBinder());
            registerDisplayFoldListener(iDisplayFoldListener);
            param1Parcel2.writeNoException();
            return true;
          case 87:
            iDisplayFoldListener.enforceInterface("android.view.IWindowManager");
            region = getCurrentImeTouchRegion();
            param1Parcel2.writeNoException();
            if (region != null) {
              param1Parcel2.writeInt(1);
              region.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 86:
            region.enforceInterface("android.view.IWindowManager");
            str7 = region.readString();
            i6 = region.readInt();
            bool11 = destroyInputConsumer(str7, i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 85:
            region.enforceInterface("android.view.IWindowManager");
            iBinder6 = region.readStrongBinder();
            str8 = region.readString();
            i5 = region.readInt();
            inputChannel = new InputChannel();
            createInputConsumer(iBinder6, str8, i5, inputChannel);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(1);
            inputChannel.writeToParcel(param1Parcel2, 1);
            return true;
          case 84:
            inputChannel.enforceInterface("android.view.IWindowManager");
            l = inputChannel.readLong();
            iShortcutService = IShortcutService.Stub.asInterface(inputChannel.readStrongBinder());
            registerShortcutKey(l, iShortcutService);
            param1Parcel2.writeNoException();
            return true;
          case 83:
            iShortcutService.enforceInterface("android.view.IWindowManager");
            i5 = iShortcutService.readInt();
            if (iShortcutService.readInt() != 0) {
              Insets insets = (Insets)Insets.CREATOR.createFromParcel((Parcel)iShortcutService);
            } else {
              iShortcutService = null;
            } 
            setForwardedInsets(i5, (Insets)iShortcutService);
            param1Parcel2.writeNoException();
            return true;
          case 82:
            iShortcutService.enforceInterface("android.view.IWindowManager");
            i5 = iShortcutService.readInt();
            rect1 = new Rect();
            getStableInsets(i5, rect1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(1);
            rect1.writeToParcel(param1Parcel2, 1);
            return true;
          case 81:
            rect1.enforceInterface("android.view.IWindowManager");
            iResultReceiver = IResultReceiver.Stub.asInterface(rect1.readStrongBinder());
            i5 = rect1.readInt();
            requestAppKeyboardShortcuts(iResultReceiver, i5);
            param1Parcel2.writeNoException();
            return true;
          case 80:
            rect1.enforceInterface("android.view.IWindowManager");
            i5 = rect1.readInt();
            iPinnedStackListener = IPinnedStackListener.Stub.asInterface(rect1.readStrongBinder());
            registerPinnedStackListener(i5, iPinnedStackListener);
            param1Parcel2.writeNoException();
            return true;
          case 79:
            iPinnedStackListener.enforceInterface("android.view.IWindowManager");
            if (iPinnedStackListener.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iPinnedStackListener);
            } else {
              iPinnedStackListener = null;
            } 
            setDockedStackDividerTouchRegion((Rect)iPinnedStackListener);
            param1Parcel2.writeNoException();
            return true;
          case 78:
            iPinnedStackListener.enforceInterface("android.view.IWindowManager");
            i5 = getDockedStackSide();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 77:
            iPinnedStackListener.enforceInterface("android.view.IWindowManager");
            iBinder3 = iPinnedStackListener.readStrongBinder();
            windowContentFrameStats = getWindowContentFrameStats(iBinder3);
            param1Parcel2.writeNoException();
            if (windowContentFrameStats != null) {
              param1Parcel2.writeInt(1);
              windowContentFrameStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 76:
            windowContentFrameStats.enforceInterface("android.view.IWindowManager");
            iBinder2 = windowContentFrameStats.readStrongBinder();
            bool10 = clearWindowContentFrameStats(iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 75:
            iBinder2.enforceInterface("android.view.IWindowManager");
            enableScreenIfNeeded();
            param1Parcel2.writeNoException();
            return true;
          case 74:
            iBinder2.enforceInterface("android.view.IWindowManager");
            bool10 = isSafeModeEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 73:
            iBinder2.enforceInterface("android.view.IWindowManager");
            if (iBinder2.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iBinder2 = null;
            } 
            lockNow((Bundle)iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 72:
            iBinder2.enforceInterface("android.view.IWindowManager");
            i4 = iBinder2.readInt();
            i4 = getNavBarPosition(i4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 71:
            iBinder2.enforceInterface("android.view.IWindowManager");
            i4 = iBinder2.readInt();
            bool9 = hasNavigationBar(i4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 70:
            iBinder2.enforceInterface("android.view.IWindowManager");
            bool31 = bool23;
            if (iBinder2.readInt() != 0)
              bool31 = true; 
            setNavBarVirtualKeyHapticFeedbackEnabled(bool31);
            param1Parcel2.writeNoException();
            return true;
          case 69:
            iBinder2.enforceInterface("android.view.IWindowManager");
            bool31 = bool24;
            if (iBinder2.readInt() != 0)
              bool31 = true; 
            setPipVisibility(bool31);
            return true;
          case 68:
            iBinder2.enforceInterface("android.view.IWindowManager");
            bool31 = bool25;
            if (iBinder2.readInt() != 0)
              bool31 = true; 
            setRecentsVisibility(bool31);
            return true;
          case 67:
            iBinder2.enforceInterface("android.view.IWindowManager");
            bool31 = bool26;
            if (iBinder2.readInt() != 0)
              bool31 = true; 
            setForceShowSystemBars(bool31);
            return true;
          case 66:
            iBinder2.enforceInterface("android.view.IWindowManager");
            i3 = iBinder2.readInt();
            hideTransientBars(i3);
            return true;
          case 65:
            iBinder2.enforceInterface("android.view.IWindowManager");
            param1Int2 = iBinder2.readInt();
            i3 = iBinder2.readInt();
            statusBarVisibilityChanged(param1Int2, i3);
            return true;
          case 64:
            iBinder2.enforceInterface("android.view.IWindowManager");
            iAssistDataReceiver = IAssistDataReceiver.Stub.asInterface(iBinder2.readStrongBinder());
            bool8 = requestAssistScreenshot(iAssistDataReceiver);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 63:
            iAssistDataReceiver.enforceInterface("android.view.IWindowManager");
            iSystemGestureExclusionListener = ISystemGestureExclusionListener.Stub.asInterface(iAssistDataReceiver.readStrongBinder());
            i2 = iAssistDataReceiver.readInt();
            unregisterSystemGestureExclusionListener(iSystemGestureExclusionListener, i2);
            param1Parcel2.writeNoException();
            return true;
          case 62:
            iAssistDataReceiver.enforceInterface("android.view.IWindowManager");
            iSystemGestureExclusionListener = ISystemGestureExclusionListener.Stub.asInterface(iAssistDataReceiver.readStrongBinder());
            i2 = iAssistDataReceiver.readInt();
            registerSystemGestureExclusionListener(iSystemGestureExclusionListener, i2);
            param1Parcel2.writeNoException();
            return true;
          case 61:
            iAssistDataReceiver.enforceInterface("android.view.IWindowManager");
            iWallpaperVisibilityListener = IWallpaperVisibilityListener.Stub.asInterface(iAssistDataReceiver.readStrongBinder());
            i2 = iAssistDataReceiver.readInt();
            unregisterWallpaperVisibilityListener(iWallpaperVisibilityListener, i2);
            param1Parcel2.writeNoException();
            return true;
          case 60:
            iAssistDataReceiver.enforceInterface("android.view.IWindowManager");
            iWallpaperVisibilityListener = IWallpaperVisibilityListener.Stub.asInterface(iAssistDataReceiver.readStrongBinder());
            i2 = iAssistDataReceiver.readInt();
            bool7 = registerWallpaperVisibilityListener(iWallpaperVisibilityListener, i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 59:
            iAssistDataReceiver.enforceInterface("android.view.IWindowManager");
            bitmap = screenshotWallpaper();
            param1Parcel2.writeNoException();
            if (bitmap != null) {
              param1Parcel2.writeInt(1);
              bitmap.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 58:
            bitmap.enforceInterface("android.view.IWindowManager");
            i1 = bitmap.readInt();
            param1Int2 = bitmap.readInt();
            setFixedToUserRotation(i1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 57:
            bitmap.enforceInterface("android.view.IWindowManager");
            i1 = bitmap.readInt();
            bool6 = isDisplayRotationFrozen(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 56:
            bitmap.enforceInterface("android.view.IWindowManager");
            n = bitmap.readInt();
            thawDisplayRotation(n);
            param1Parcel2.writeNoException();
            return true;
          case 55:
            bitmap.enforceInterface("android.view.IWindowManager");
            param1Int2 = bitmap.readInt();
            n = bitmap.readInt();
            freezeDisplayRotation(param1Int2, n);
            param1Parcel2.writeNoException();
            return true;
          case 54:
            bitmap.enforceInterface("android.view.IWindowManager");
            bool5 = isRotationFrozen();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 53:
            bitmap.enforceInterface("android.view.IWindowManager");
            thawRotation();
            param1Parcel2.writeNoException();
            return true;
          case 52:
            bitmap.enforceInterface("android.view.IWindowManager");
            m = bitmap.readInt();
            freezeRotation(m);
            param1Parcel2.writeNoException();
            return true;
          case 51:
            bitmap.enforceInterface("android.view.IWindowManager");
            m = bitmap.readInt();
            m = getPreferredOptionsPanelGravity(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 50:
            bitmap.enforceInterface("android.view.IWindowManager");
            iRotationWatcher1 = IRotationWatcher.Stub.asInterface(bitmap.readStrongBinder());
            removeRotationWatcher(iRotationWatcher1);
            param1Parcel2.writeNoException();
            return true;
          case 49:
            iRotationWatcher1.enforceInterface("android.view.IWindowManager");
            iRotationWatcher2 = IRotationWatcher.Stub.asInterface(iRotationWatcher1.readStrongBinder());
            m = iRotationWatcher1.readInt();
            m = watchRotation(iRotationWatcher2, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 48:
            iRotationWatcher1.enforceInterface("android.view.IWindowManager");
            m = getDefaultDisplayRotation();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 47:
            iRotationWatcher1.enforceInterface("android.view.IWindowManager");
            if (iRotationWatcher1.readInt() != 0) {
              bool31 = true;
            } else {
              bool31 = false;
            } 
            if (iRotationWatcher1.readInt() != 0)
              bool27 = true; 
            updateRotation(bool31, bool27);
            param1Parcel2.writeNoException();
            return true;
          case 46:
            iRotationWatcher1.enforceInterface("android.view.IWindowManager");
            m = iRotationWatcher1.readInt();
            refreshScreenCaptureDisabled(m);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            iRotationWatcher1.enforceInterface("android.view.IWindowManager");
            str3 = iRotationWatcher1.readString();
            setStrictModeVisualIndicatorPreference(str3);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            str3.enforceInterface("android.view.IWindowManager");
            bool31 = bool28;
            if (str3.readInt() != 0)
              bool31 = true; 
            showStrictModeViolation(bool31);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            str3.enforceInterface("android.view.IWindowManager");
            bool31 = bool29;
            if (str3.readInt() != 0)
              bool31 = true; 
            setInTouchMode(bool31);
            param1Parcel2.writeNoException();
            return true;
          case 42:
            str3.enforceInterface("android.view.IWindowManager");
            f = getCurrentAnimatorScale();
            param1Parcel2.writeNoException();
            param1Parcel2.writeFloat(f);
            return true;
          case 41:
            str3.enforceInterface("android.view.IWindowManager");
            arrayOfFloat = str3.createFloatArray();
            setAnimationScales(arrayOfFloat);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            arrayOfFloat.enforceInterface("android.view.IWindowManager");
            m = arrayOfFloat.readInt();
            f = arrayOfFloat.readFloat();
            setAnimationScale(m, f);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            arrayOfFloat.enforceInterface("android.view.IWindowManager");
            arrayOfFloat = getAnimationScales();
            param1Parcel2.writeNoException();
            param1Parcel2.writeFloatArray(arrayOfFloat);
            return true;
          case 38:
            arrayOfFloat.enforceInterface("android.view.IWindowManager");
            m = arrayOfFloat.readInt();
            f = getAnimationScale(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeFloat(f);
            return true;
          case 37:
            arrayOfFloat.enforceInterface("android.view.IWindowManager");
            str2 = arrayOfFloat.readString();
            closeSystemDialogs(str2);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str2.enforceInterface("android.view.IWindowManager");
            bool31 = bool30;
            if (str2.readInt() != 0)
              bool31 = true; 
            setSwitchingUser(bool31);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            str2.enforceInterface("android.view.IWindowManager");
            iKeyguardDismissCallback = IKeyguardDismissCallback.Stub.asInterface(str2.readStrongBinder());
            if (str2.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            dismissKeyguard(iKeyguardDismissCallback, str2);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            str2.enforceInterface("android.view.IWindowManager");
            m = str2.readInt();
            bool4 = isKeyguardSecure(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 33:
            str2.enforceInterface("android.view.IWindowManager");
            bool4 = isKeyguardLocked();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 32:
            str2.enforceInterface("android.view.IWindowManager");
            iOnKeyguardExitResult = IOnKeyguardExitResult.Stub.asInterface(str2.readStrongBinder());
            exitKeyguardSecurely(iOnKeyguardExitResult);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            iBinder5 = iOnKeyguardExitResult.readStrongBinder();
            k = iOnKeyguardExitResult.readInt();
            reenableKeyguard(iBinder5, k);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            iBinder9 = iOnKeyguardExitResult.readStrongBinder();
            str6 = iOnKeyguardExitResult.readString();
            k = iOnKeyguardExitResult.readInt();
            disableKeyguard(iBinder9, str6, k);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            stopFreezingScreen();
            param1Parcel2.writeNoException();
            return true;
          case 28:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            k = iOnKeyguardExitResult.readInt();
            param1Int2 = iOnKeyguardExitResult.readInt();
            startFreezingScreen(k, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            endProlongedAnimations();
            param1Parcel2.writeNoException();
            return true;
          case 26:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            executeAppTransition();
            param1Parcel2.writeNoException();
            return true;
          case 25:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            if (iOnKeyguardExitResult.readInt() != 0) {
              RemoteAnimationAdapter remoteAnimationAdapter = (RemoteAnimationAdapter)RemoteAnimationAdapter.CREATOR.createFromParcel((Parcel)iOnKeyguardExitResult);
            } else {
              str6 = null;
            } 
            k = iOnKeyguardExitResult.readInt();
            overridePendingAppTransitionRemote((RemoteAnimationAdapter)str6, k);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            iAppTransitionAnimationSpecsFuture = IAppTransitionAnimationSpecsFuture.Stub.asInterface(iOnKeyguardExitResult.readStrongBinder());
            iRemoteCallback = IRemoteCallback.Stub.asInterface(iOnKeyguardExitResult.readStrongBinder());
            if (iOnKeyguardExitResult.readInt() != 0)
              bool31 = true; 
            k = iOnKeyguardExitResult.readInt();
            overridePendingAppTransitionMultiThumbFuture(iAppTransitionAnimationSpecsFuture, iRemoteCallback, bool31, k);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            iOnKeyguardExitResult.enforceInterface("android.view.IWindowManager");
            param1Int2 = iOnKeyguardExitResult.readInt();
            k = iOnKeyguardExitResult.readInt();
            iWindow1 = IWindow.Stub.asInterface(iOnKeyguardExitResult.readStrongBinder());
            setShellRootAccessibilityWindow(param1Int2, k, iWindow1);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            iWindow1.enforceInterface("android.view.IWindowManager");
            k = iWindow1.readInt();
            iWindow2 = IWindow.Stub.asInterface(iWindow1.readStrongBinder());
            param1Int2 = iWindow1.readInt();
            surfaceControl1 = addShellRoot(k, iWindow2, param1Int2);
            param1Parcel2.writeNoException();
            if (surfaceControl1 != null) {
              param1Parcel2.writeInt(1);
              surfaceControl1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 21:
            surfaceControl1.enforceInterface("android.view.IWindowManager");
            iDisplayWindowRotationController = IDisplayWindowRotationController.Stub.asInterface(surfaceControl1.readStrongBinder());
            setDisplayWindowRotationController(iDisplayWindowRotationController);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            iDisplayWindowRotationController.enforceInterface("android.view.IWindowManager");
            k = iDisplayWindowRotationController.readInt();
            bool31 = bool32;
            if (iDisplayWindowRotationController.readInt() != 0)
              bool31 = true; 
            prepareAppTransition(k, bool31);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            iDisplayWindowRotationController.enforceInterface("android.view.IWindowManager");
            iBinder4 = iDisplayWindowRotationController.readStrongBinder();
            k = iDisplayWindowRotationController.readInt();
            removeWindowToken(iBinder4, k);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            iDisplayWindowRotationController.enforceInterface("android.view.IWindowManager");
            iBinder4 = iDisplayWindowRotationController.readStrongBinder();
            k = iDisplayWindowRotationController.readInt();
            param1Int2 = iDisplayWindowRotationController.readInt();
            addWindowToken(iBinder4, k, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            iDisplayWindowRotationController.enforceInterface("android.view.IWindowManager");
            iBinder8 = iDisplayWindowRotationController.readStrongBinder();
            k = iDisplayWindowRotationController.readInt();
            param1Int2 = iDisplayWindowRotationController.readInt();
            if (iDisplayWindowRotationController.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iDisplayWindowRotationController);
            } else {
              iBinder4 = null;
            } 
            str1 = iDisplayWindowRotationController.readString();
            k = addWindowTokenWithOptions(iBinder8, k, param1Int2, (Bundle)iBinder4, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 16:
            str1.enforceInterface("android.view.IWindowManager");
            iBinder1 = str1.readStrongBinder();
            bool3 = isWindowToken(iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 15:
            iBinder1.enforceInterface("android.view.IWindowManager");
            bool31 = bool33;
            if (iBinder1.readInt() != 0)
              bool31 = true; 
            setEventDispatching(bool31);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iBinder1.enforceInterface("android.view.IWindowManager");
            param1Int2 = iBinder1.readInt();
            j = iBinder1.readInt();
            setForcedDisplayScalingMode(param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            iBinder1.enforceInterface("android.view.IWindowManager");
            param1Int2 = iBinder1.readInt();
            j = iBinder1.readInt();
            clearForcedDisplayDensityForUser(param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            iBinder1.enforceInterface("android.view.IWindowManager");
            i14 = iBinder1.readInt();
            param1Int2 = iBinder1.readInt();
            j = iBinder1.readInt();
            setForcedDisplayDensityForUser(i14, param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            iBinder1.enforceInterface("android.view.IWindowManager");
            j = iBinder1.readInt();
            j = getBaseDisplayDensity(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 10:
            iBinder1.enforceInterface("android.view.IWindowManager");
            j = iBinder1.readInt();
            j = getInitialDisplayDensity(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 9:
            iBinder1.enforceInterface("android.view.IWindowManager");
            j = iBinder1.readInt();
            clearForcedDisplaySize(j);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iBinder1.enforceInterface("android.view.IWindowManager");
            param1Int2 = iBinder1.readInt();
            j = iBinder1.readInt();
            i14 = iBinder1.readInt();
            setForcedDisplaySize(param1Int2, j, i14);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iBinder1.enforceInterface("android.view.IWindowManager");
            j = iBinder1.readInt();
            point = new Point();
            getBaseDisplaySize(j, point);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(1);
            point.writeToParcel(param1Parcel2, 1);
            return true;
          case 6:
            point.enforceInterface("android.view.IWindowManager");
            j = point.readInt();
            point = new Point();
            getInitialDisplaySize(j, point);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(1);
            point.writeToParcel(param1Parcel2, 1);
            return true;
          case 5:
            point.enforceInterface("android.view.IWindowManager");
            bool2 = useBLAST();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 4:
            point.enforceInterface("android.view.IWindowManager");
            iWindowSessionCallback = IWindowSessionCallback.Stub.asInterface(point.readStrongBinder());
            iWindowSession = openSession(iWindowSessionCallback);
            param1Parcel2.writeNoException();
            if (iWindowSession != null) {
              IBinder iBinder = iWindowSession.asBinder();
            } else {
              iWindowSession = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iWindowSession);
            return true;
          case 3:
            iWindowSession.enforceInterface("android.view.IWindowManager");
            bool2 = isViewServerRunning();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            iWindowSession.enforceInterface("android.view.IWindowManager");
            bool2 = stopViewServer();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 1:
            break;
        } 
        iWindowSession.enforceInterface("android.view.IWindowManager");
        int i = iWindowSession.readInt();
        boolean bool1 = startViewServer(i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.view.IWindowManager");
      return true;
    }
    
    private static class Proxy implements IWindowManager {
      public static IWindowManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IWindowManager";
      }
      
      public boolean startViewServer(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().startViewServer(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean stopViewServer() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().stopViewServer();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isViewServerRunning() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isViewServerRunning();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IWindowSession openSession(IWindowSessionCallback param2IWindowSessionCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IWindowSessionCallback != null) {
            iBinder = param2IWindowSessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().openSession(param2IWindowSessionCallback); 
          parcel2.readException();
          return IWindowSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean useBLAST() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().useBLAST();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getInitialDisplaySize(int param2Int, Point param2Point) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().getInitialDisplaySize(param2Int, param2Point);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2Point.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getBaseDisplaySize(int param2Int, Point param2Point) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().getBaseDisplaySize(param2Int, param2Point);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2Point.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setForcedDisplaySize(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setForcedDisplaySize(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearForcedDisplaySize(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().clearForcedDisplaySize(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getInitialDisplayDensity(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int = IWindowManager.Stub.getDefaultImpl().getInitialDisplayDensity(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBaseDisplayDensity(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int = IWindowManager.Stub.getDefaultImpl().getBaseDisplayDensity(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setForcedDisplayDensityForUser(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setForcedDisplayDensityForUser(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearForcedDisplayDensityForUser(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().clearForcedDisplayDensityForUser(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setForcedDisplayScalingMode(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setForcedDisplayScalingMode(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setEventDispatching(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setEventDispatching(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWindowToken(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isWindowToken(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addWindowTokenWithOptions(IBinder param2IBinder, int param2Int1, int param2Int2, Bundle param2Bundle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int1 = IWindowManager.Stub.getDefaultImpl().addWindowTokenWithOptions(param2IBinder, param2Int1, param2Int2, param2Bundle, param2String);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addWindowToken(IBinder param2IBinder, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().addWindowToken(param2IBinder, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeWindowToken(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().removeWindowToken(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepareAppTransition(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().prepareAppTransition(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDisplayWindowRotationController(IDisplayWindowRotationController param2IDisplayWindowRotationController) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IDisplayWindowRotationController != null) {
            iBinder = param2IDisplayWindowRotationController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setDisplayWindowRotationController(param2IDisplayWindowRotationController);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SurfaceControl addShellRoot(int param2Int1, IWindow param2IWindow, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().addShellRoot(param2Int1, param2IWindow, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SurfaceControl surfaceControl = (SurfaceControl)SurfaceControl.CREATOR.createFromParcel(parcel2);
          } else {
            param2IWindow = null;
          } 
          return (SurfaceControl)param2IWindow;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setShellRootAccessibilityWindow(int param2Int1, int param2Int2, IWindow param2IWindow) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IWindow != null) {
            iBinder = param2IWindow.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setShellRootAccessibilityWindow(param2Int1, param2Int2, param2IWindow);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void overridePendingAppTransitionMultiThumbFuture(IAppTransitionAnimationSpecsFuture param2IAppTransitionAnimationSpecsFuture, IRemoteCallback param2IRemoteCallback, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder1 = null;
          if (param2IAppTransitionAnimationSpecsFuture != null) {
            iBinder2 = param2IAppTransitionAnimationSpecsFuture.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IRemoteCallback != null)
            iBinder2 = param2IRemoteCallback.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().overridePendingAppTransitionMultiThumbFuture(param2IAppTransitionAnimationSpecsFuture, param2IRemoteCallback, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void overridePendingAppTransitionRemote(RemoteAnimationAdapter param2RemoteAnimationAdapter, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2RemoteAnimationAdapter != null) {
            parcel1.writeInt(1);
            param2RemoteAnimationAdapter.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().overridePendingAppTransitionRemote(param2RemoteAnimationAdapter, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void executeAppTransition() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().executeAppTransition();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void endProlongedAnimations() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().endProlongedAnimations();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startFreezingScreen(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().startFreezingScreen(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopFreezingScreen() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().stopFreezingScreen();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableKeyguard(IBinder param2IBinder, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().disableKeyguard(param2IBinder, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reenableKeyguard(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().reenableKeyguard(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void exitKeyguardSecurely(IOnKeyguardExitResult param2IOnKeyguardExitResult) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IOnKeyguardExitResult != null) {
            iBinder = param2IOnKeyguardExitResult.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().exitKeyguardSecurely(param2IOnKeyguardExitResult);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isKeyguardLocked() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(33, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isKeyguardLocked();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isKeyguardSecure(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(34, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isKeyguardSecure(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dismissKeyguard(IKeyguardDismissCallback param2IKeyguardDismissCallback, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IKeyguardDismissCallback != null) {
            iBinder = param2IKeyguardDismissCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().dismissKeyguard(param2IKeyguardDismissCallback, param2CharSequence);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSwitchingUser(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setSwitchingUser(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeSystemDialogs(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().closeSystemDialogs(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float getAnimationScale(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getAnimationScale(param2Int); 
          parcel2.readException();
          return parcel2.readFloat();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float[] getAnimationScales() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getAnimationScales(); 
          parcel2.readException();
          return parcel2.createFloatArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAnimationScale(int param2Int, float param2Float) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          parcel1.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setAnimationScale(param2Int, param2Float);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAnimationScales(float[] param2ArrayOffloat) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeFloatArray(param2ArrayOffloat);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setAnimationScales(param2ArrayOffloat);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float getCurrentAnimatorScale() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getCurrentAnimatorScale(); 
          parcel2.readException();
          return parcel2.readFloat();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInTouchMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setInTouchMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showStrictModeViolation(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().showStrictModeViolation(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setStrictModeVisualIndicatorPreference(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setStrictModeVisualIndicatorPreference(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void refreshScreenCaptureDisabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().refreshScreenCaptureDisabled(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateRotation(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().updateRotation(param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDefaultDisplayRotation() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getDefaultDisplayRotation(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int watchRotation(IRotationWatcher param2IRotationWatcher, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IRotationWatcher != null) {
            iBinder = param2IRotationWatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int = IWindowManager.Stub.getDefaultImpl().watchRotation(param2IRotationWatcher, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeRotationWatcher(IRotationWatcher param2IRotationWatcher) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IRotationWatcher != null) {
            iBinder = param2IRotationWatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().removeRotationWatcher(param2IRotationWatcher);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPreferredOptionsPanelGravity(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int = IWindowManager.Stub.getDefaultImpl().getPreferredOptionsPanelGravity(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void freezeRotation(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().freezeRotation(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void thawRotation() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().thawRotation();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRotationFrozen() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(54, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isRotationFrozen();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void freezeDisplayRotation(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().freezeDisplayRotation(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void thawDisplayRotation(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().thawDisplayRotation(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDisplayRotationFrozen(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(57, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isDisplayRotationFrozen(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFixedToUserRotation(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setFixedToUserRotation(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bitmap screenshotWallpaper() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bitmap bitmap;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            bitmap = IWindowManager.Stub.getDefaultImpl().screenshotWallpaper();
            return bitmap;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(parcel2);
          } else {
            bitmap = null;
          } 
          return bitmap;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerWallpaperVisibilityListener(IWallpaperVisibilityListener param2IWallpaperVisibilityListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IWallpaperVisibilityListener != null) {
            iBinder = param2IWallpaperVisibilityListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(60, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().registerWallpaperVisibilityListener(param2IWallpaperVisibilityListener, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterWallpaperVisibilityListener(IWallpaperVisibilityListener param2IWallpaperVisibilityListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IWallpaperVisibilityListener != null) {
            iBinder = param2IWallpaperVisibilityListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().unregisterWallpaperVisibilityListener(param2IWallpaperVisibilityListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerSystemGestureExclusionListener(ISystemGestureExclusionListener param2ISystemGestureExclusionListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2ISystemGestureExclusionListener != null) {
            iBinder = param2ISystemGestureExclusionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().registerSystemGestureExclusionListener(param2ISystemGestureExclusionListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterSystemGestureExclusionListener(ISystemGestureExclusionListener param2ISystemGestureExclusionListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2ISystemGestureExclusionListener != null) {
            iBinder = param2ISystemGestureExclusionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().unregisterSystemGestureExclusionListener(param2ISystemGestureExclusionListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestAssistScreenshot(IAssistDataReceiver param2IAssistDataReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IAssistDataReceiver != null) {
            iBinder = param2IAssistDataReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(64, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().requestAssistScreenshot(param2IAssistDataReceiver);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void statusBarVisibilityChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindowManager");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(65, parcel, null, 1);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().statusBarVisibilityChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hideTransientBars(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IWindowManager");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(66, parcel, null, 1);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().hideTransientBars(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setForceShowSystemBars(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(67, parcel, null, 1);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setForceShowSystemBars(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRecentsVisibility(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(68, parcel, null, 1);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setRecentsVisibility(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPipVisibility(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(69, parcel, null, 1);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setPipVisibility(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setNavBarVirtualKeyHapticFeedbackEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setNavBarVirtualKeyHapticFeedbackEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasNavigationBar(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(71, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().hasNavigationBar(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNavBarPosition(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int = IWindowManager.Stub.getDefaultImpl().getNavBarPosition(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void lockNow(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().lockNow(param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSafeModeEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(74, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isSafeModeEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableScreenIfNeeded() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().enableScreenIfNeeded();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearWindowContentFrameStats(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(76, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().clearWindowContentFrameStats(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WindowContentFrameStats getWindowContentFrameStats(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getWindowContentFrameStats(param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            WindowContentFrameStats windowContentFrameStats = (WindowContentFrameStats)WindowContentFrameStats.CREATOR.createFromParcel(parcel2);
          } else {
            param2IBinder = null;
          } 
          return (WindowContentFrameStats)param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDockedStackSide() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getDockedStackSide(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDockedStackDividerTouchRegion(Rect param2Rect) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setDockedStackDividerTouchRegion(param2Rect);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerPinnedStackListener(int param2Int, IPinnedStackListener param2IPinnedStackListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          if (param2IPinnedStackListener != null) {
            iBinder = param2IPinnedStackListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().registerPinnedStackListener(param2Int, param2IPinnedStackListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestAppKeyboardShortcuts(IResultReceiver param2IResultReceiver, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().requestAppKeyboardShortcuts(param2IResultReceiver, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getStableInsets(int param2Int, Rect param2Rect) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().getStableInsets(param2Int, param2Rect);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2Rect.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setForwardedInsets(int param2Int, Insets param2Insets) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          if (param2Insets != null) {
            parcel1.writeInt(1);
            param2Insets.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setForwardedInsets(param2Int, param2Insets);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerShortcutKey(long param2Long, IShortcutService param2IShortcutService) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeLong(param2Long);
          if (param2IShortcutService != null) {
            iBinder = param2IShortcutService.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().registerShortcutKey(param2Long, param2IShortcutService);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createInputConsumer(IBinder param2IBinder, String param2String, int param2Int, InputChannel param2InputChannel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(85, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().createInputConsumer(param2IBinder, param2String, param2Int, param2InputChannel);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2InputChannel.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean destroyInputConsumer(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(86, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().destroyInputConsumer(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Region getCurrentImeTouchRegion() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Region region;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(87, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            region = IWindowManager.Stub.getDefaultImpl().getCurrentImeTouchRegion();
            return region;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            region = (Region)Region.CREATOR.createFromParcel(parcel2);
          } else {
            region = null;
          } 
          return region;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerDisplayFoldListener(IDisplayFoldListener param2IDisplayFoldListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IDisplayFoldListener != null) {
            iBinder = param2IDisplayFoldListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(88, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().registerDisplayFoldListener(param2IDisplayFoldListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterDisplayFoldListener(IDisplayFoldListener param2IDisplayFoldListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IDisplayFoldListener != null) {
            iBinder = param2IDisplayFoldListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().unregisterDisplayFoldListener(param2IDisplayFoldListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerDisplayWindowListener(IDisplayWindowListener param2IDisplayWindowListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IDisplayWindowListener != null) {
            iBinder = param2IDisplayWindowListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(90, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().registerDisplayWindowListener(param2IDisplayWindowListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterDisplayWindowListener(IDisplayWindowListener param2IDisplayWindowListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IDisplayWindowListener != null) {
            iBinder = param2IDisplayWindowListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().unregisterDisplayWindowListener(param2IDisplayWindowListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startWindowTrace() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(92, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().startWindowTrace();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopWindowTrace() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(93, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().stopWindowTrace();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWindowTraceEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(94, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isWindowTraceEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dontOverrideDisplayInfo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(95, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().dontOverrideDisplayInfo(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerOppoWindowStateObserver(IOplusWindowStateObserver param2IOplusWindowStateObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IOplusWindowStateObserver != null) {
            iBinder = param2IOplusWindowStateObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(96, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().registerOppoWindowStateObserver(param2IOplusWindowStateObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterOppoWindowStateObserver(IOplusWindowStateObserver param2IOplusWindowStateObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2IOplusWindowStateObserver != null) {
            iBinder = param2IOplusWindowStateObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().unregisterOppoWindowStateObserver(param2IOplusWindowStateObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getWindowingMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(98, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int = IWindowManager.Stub.getDefaultImpl().getWindowingMode(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setWindowingMode(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(99, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setWindowingMode(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRemoveContentMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(100, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int = IWindowManager.Stub.getDefaultImpl().getRemoveContentMode(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRemoveContentMode(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(101, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setRemoveContentMode(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldShowWithInsecureKeyguard(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(102, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().shouldShowWithInsecureKeyguard(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setShouldShowWithInsecureKeyguard(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(103, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setShouldShowWithInsecureKeyguard(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldShowSystemDecors(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(104, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().shouldShowSystemDecors(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setShouldShowSystemDecors(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(105, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setShouldShowSystemDecors(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldShowIme(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(106, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().shouldShowIme(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setShouldShowIme(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(107, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setShouldShowIme(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean injectInputAfterTransactionsApplied(InputEvent param2InputEvent, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool1 = true;
          if (param2InputEvent != null) {
            parcel1.writeInt(1);
            param2InputEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(108, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().injectInputAfterTransactionsApplied(param2InputEvent, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void syncInputTransactions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(109, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().syncInputTransactions();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLayerTracing() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(110, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isLayerTracing();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLayerTracing(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(111, parcel1, parcel2, 0);
          if (!bool1 && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setLayerTracing(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean mirrorDisplay(int param2Int, SurfaceControl param2SurfaceControl) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(112, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().mirrorDisplay(param2Int, param2SurfaceControl);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            bool1 = true; 
          if (parcel2.readInt() != 0)
            param2SurfaceControl.readFromParcel(parcel2); 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDisplayWindowInsetsController(int param2Int, IDisplayWindowInsetsController param2IDisplayWindowInsetsController) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          if (param2IDisplayWindowInsetsController != null) {
            iBinder = param2IDisplayWindowInsetsController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(113, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setDisplayWindowInsetsController(param2Int, param2IDisplayWindowInsetsController);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void modifyDisplayWindowInsets(int param2Int, InsetsState param2InsetsState) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          if (param2InsetsState != null) {
            parcel1.writeInt(1);
            param2InsetsState.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(114, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().modifyDisplayWindowInsets(param2Int, param2InsetsState);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getWindowInsets(WindowManager.LayoutParams param2LayoutParams, int param2Int, Rect param2Rect1, Rect param2Rect2, DisplayCutout.ParcelableWrapper param2ParcelableWrapper, InsetsState param2InsetsState) throws RemoteException {
        // Byte code:
        //   0: invokestatic obtain : ()Landroid/os/Parcel;
        //   3: astore #7
        //   5: invokestatic obtain : ()Landroid/os/Parcel;
        //   8: astore #8
        //   10: aload #7
        //   12: ldc 'android.view.IWindowManager'
        //   14: invokevirtual writeInterfaceToken : (Ljava/lang/String;)V
        //   17: iconst_1
        //   18: istore #9
        //   20: aload_1
        //   21: ifnull -> 40
        //   24: aload #7
        //   26: iconst_1
        //   27: invokevirtual writeInt : (I)V
        //   30: aload_1
        //   31: aload #7
        //   33: iconst_0
        //   34: invokevirtual writeToParcel : (Landroid/os/Parcel;I)V
        //   37: goto -> 46
        //   40: aload #7
        //   42: iconst_0
        //   43: invokevirtual writeInt : (I)V
        //   46: aload #7
        //   48: iload_2
        //   49: invokevirtual writeInt : (I)V
        //   52: aload_0
        //   53: getfield mRemote : Landroid/os/IBinder;
        //   56: bipush #115
        //   58: aload #7
        //   60: aload #8
        //   62: iconst_0
        //   63: invokeinterface transact : (ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
        //   68: istore #10
        //   70: iload #10
        //   72: ifne -> 113
        //   75: invokestatic getDefaultImpl : ()Landroid/view/IWindowManager;
        //   78: ifnull -> 113
        //   81: invokestatic getDefaultImpl : ()Landroid/view/IWindowManager;
        //   84: aload_1
        //   85: iload_2
        //   86: aload_3
        //   87: aload #4
        //   89: aload #5
        //   91: aload #6
        //   93: invokeinterface getWindowInsets : (Landroid/view/WindowManager$LayoutParams;ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/DisplayCutout$ParcelableWrapper;Landroid/view/InsetsState;)Z
        //   98: istore #9
        //   100: aload #8
        //   102: invokevirtual recycle : ()V
        //   105: aload #7
        //   107: invokevirtual recycle : ()V
        //   110: iload #9
        //   112: ireturn
        //   113: aload #8
        //   115: invokevirtual readException : ()V
        //   118: aload #8
        //   120: invokevirtual readInt : ()I
        //   123: ifeq -> 129
        //   126: goto -> 132
        //   129: iconst_0
        //   130: istore #9
        //   132: aload #8
        //   134: invokevirtual readInt : ()I
        //   137: istore_2
        //   138: iload_2
        //   139: ifeq -> 151
        //   142: aload_3
        //   143: aload #8
        //   145: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   148: goto -> 151
        //   151: aload #8
        //   153: invokevirtual readInt : ()I
        //   156: istore_2
        //   157: iload_2
        //   158: ifeq -> 171
        //   161: aload #4
        //   163: aload #8
        //   165: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   168: goto -> 171
        //   171: aload #8
        //   173: invokevirtual readInt : ()I
        //   176: istore_2
        //   177: iload_2
        //   178: ifeq -> 191
        //   181: aload #5
        //   183: aload #8
        //   185: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   188: goto -> 191
        //   191: aload #8
        //   193: invokevirtual readInt : ()I
        //   196: istore_2
        //   197: iload_2
        //   198: ifeq -> 215
        //   201: aload #6
        //   203: aload #8
        //   205: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
        //   208: goto -> 215
        //   211: astore_1
        //   212: goto -> 249
        //   215: aload #8
        //   217: invokevirtual recycle : ()V
        //   220: aload #7
        //   222: invokevirtual recycle : ()V
        //   225: iload #9
        //   227: ireturn
        //   228: astore_1
        //   229: goto -> 249
        //   232: astore_1
        //   233: goto -> 249
        //   236: astore_1
        //   237: goto -> 249
        //   240: astore_1
        //   241: goto -> 249
        //   244: astore_1
        //   245: goto -> 249
        //   248: astore_1
        //   249: aload #8
        //   251: invokevirtual recycle : ()V
        //   254: aload #7
        //   256: invokevirtual recycle : ()V
        //   259: aload_1
        //   260: athrow
        // Line number table:
        //   Java source line number -> byte code offset
        //   #5655	-> 0
        //   #5656	-> 5
        //   #5659	-> 10
        //   #5660	-> 17
        //   #5661	-> 24
        //   #5662	-> 30
        //   #5665	-> 40
        //   #5667	-> 46
        //   #5668	-> 52
        //   #5669	-> 70
        //   #5670	-> 81
        //   #5688	-> 100
        //   #5689	-> 105
        //   #5670	-> 110
        //   #5672	-> 113
        //   #5673	-> 118
        //   #5674	-> 132
        //   #5675	-> 142
        //   #5674	-> 151
        //   #5677	-> 151
        //   #5678	-> 161
        //   #5677	-> 171
        //   #5680	-> 171
        //   #5681	-> 181
        //   #5680	-> 191
        //   #5683	-> 191
        //   #5684	-> 201
        //   #5688	-> 211
        //   #5683	-> 215
        //   #5688	-> 215
        //   #5689	-> 220
        //   #5690	-> 225
        //   #5691	-> 225
        //   #5688	-> 228
        //   #5689	-> 254
        //   #5690	-> 259
        // Exception table:
        //   from	to	target	type
        //   10	17	248	finally
        //   24	30	248	finally
        //   30	37	248	finally
        //   40	46	248	finally
        //   46	52	244	finally
        //   52	70	240	finally
        //   75	81	240	finally
        //   81	100	240	finally
        //   113	118	240	finally
        //   118	126	240	finally
        //   132	138	240	finally
        //   142	148	236	finally
        //   151	157	236	finally
        //   161	168	232	finally
        //   171	177	232	finally
        //   181	188	228	finally
        //   191	197	228	finally
        //   201	208	211	finally
      }
      
      public void showGlobalActions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(116, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().showGlobalActions();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isActivityNeedPalette(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(117, parcel1, parcel2, 0);
          if (!bool2 && IWindowManager.Stub.getDefaultImpl() != null) {
            bool1 = IWindowManager.Stub.getDefaultImpl().isActivityNeedPalette(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNavBarColorFromAdaptation(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(118, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getNavBarColorFromAdaptation(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getStatusBarColorFromAdaptation(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(119, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getStatusBarColorFromAdaptation(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getImeBgColorFromAdaptation(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(120, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getImeBgColorFromAdaptation(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getTypedWindowLayer(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(121, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            param2Int = IWindowManager.Stub.getDefaultImpl().getTypedWindowLayer(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFocusedWindowIgnoreHomeMenuKey() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          boolean bool = this.mRemote.transact(122, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null)
            return IWindowManager.Stub.getDefaultImpl().getFocusedWindowIgnoreHomeMenuKey(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLayerTracingFlags(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(123, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().setLayerTracingFlags(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestScrollCapture(int param2Int1, IBinder param2IBinder, int param2Int2, IScrollCaptureController param2IScrollCaptureController) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int2);
          if (param2IScrollCaptureController != null) {
            iBinder = param2IScrollCaptureController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(124, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().requestScrollCapture(param2Int1, param2IBinder, param2Int2, param2IScrollCaptureController);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendWakeUpReasonToKeyguard(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IWindowManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(125, parcel1, parcel2, 0);
          if (!bool && IWindowManager.Stub.getDefaultImpl() != null) {
            IWindowManager.Stub.getDefaultImpl().sendWakeUpReasonToKeyguard(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowManager param1IWindowManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowManager != null) {
          Proxy.sDefaultImpl = param1IWindowManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
