package android.view;

public interface ViewManager {
  void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams);
  
  void removeView(View paramView);
  
  void updateViewLayout(View paramView, ViewGroup.LayoutParams paramLayoutParams);
}
