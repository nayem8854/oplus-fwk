package android.view;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.GraphicBuffer;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.hardware.display.DeviceProductInfo;
import android.hardware.display.DisplayedContentSample;
import android.hardware.display.DisplayedContentSamplingAttributes;
import android.os.Debug;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemProperties;
import android.os.customize.util.OplusCustomizeScreenCaptureHelper;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseIntArray;
import android.util.proto.ProtoOutputStream;
import dalvik.system.CloseGuard;
import java.io.Closeable;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Objects;
import libcore.util.NativeAllocationRegistry;

public final class SurfaceControl implements Parcelable {
  public static final int CAST_FLAGS = 64;
  
  public static final Parcelable.Creator<SurfaceControl> CREATOR;
  
  public static final int CURSOR_WINDOW = 8192;
  
  private static final boolean DEBUG_ALL;
  
  private static final int DEBUG_DEPTH;
  
  private static boolean DEBUG_SFC = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final int FX_SURFACE_BLAST = 262144;
  
  public static final int FX_SURFACE_CONTAINER = 524288;
  
  public static final int FX_SURFACE_EFFECT = 131072;
  
  public static final int FX_SURFACE_MASK = 983040;
  
  public static final int FX_SURFACE_NORMAL = 0;
  
  public static final int HIDDEN = 4;
  
  private static final int INTERNAL_DATASPACE_DISPLAY_P3 = 143261696;
  
  private static final int INTERNAL_DATASPACE_SCRGB = 411107328;
  
  private static final int INTERNAL_DATASPACE_SRGB = 142671872;
  
  public static final int METADATA_ACCESSIBILITY_ID = 5;
  
  public static final int METADATA_MOUSE_CURSOR = 4;
  
  public static final int METADATA_OWNER_UID = 1;
  
  public static final int METADATA_TASK_ID = 3;
  
  public static final int METADATA_WINDOW_TYPE = 2;
  
  public static final int NON_PREMULTIPLIED = 256;
  
  public static final int NO_COLOR_FILL = 16384;
  
  public static final int OPAQUE = 1024;
  
  public static final int POWER_MODE_DOZE = 1;
  
  public static final int POWER_MODE_DOZE_SUSPEND = 3;
  
  public static final int POWER_MODE_NORMAL = 2;
  
  public static final int POWER_MODE_OFF = 0;
  
  public static final int POWER_MODE_ON_SUSPEND = 4;
  
  public static final int PROTECTED_APP = 2048;
  
  public static final int SECURE = 128;
  
  private static final int SURFACE_HIDDEN = 1;
  
  private static final int SURFACE_OPAQUE = 2;
  
  private static final String TAG = "SurfaceControl";
  
  public static final int WINDOW_TYPE_DONT_SCREENSHOT = 441731;
  
  static Transaction sGlobalTransaction;
  
  static long sTransactionNestCount;
  
  static {
    DEBUG_ALL = SystemProperties.getBoolean("debug.surfacectrl", false);
    DEBUG_DEPTH = SystemProperties.getInt("debug.surfacectrl.depth", 5);
    sTransactionNestCount = 0L;
    CREATOR = new Parcelable.Creator<SurfaceControl>() {
        public SurfaceControl createFromParcel(Parcel param1Parcel) {
          return new SurfaceControl(param1Parcel);
        }
        
        public SurfaceControl[] newArray(int param1Int) {
          return new SurfaceControl[param1Int];
        }
      };
  }
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private int mHeight;
  
  private WeakReference<View> mLocalOwnerView;
  
  private final Object mLock = new Object();
  
  private String mName;
  
  private long mNativeHandle;
  
  public long mNativeObject;
  
  private ArrayList<OnReparentListener> mReparentListeners;
  
  private int mWidth;
  
  public boolean addOnReparentListener(OnReparentListener paramOnReparentListener) {
    synchronized (this.mLock) {
      if (this.mReparentListeners == null) {
        ArrayList<OnReparentListener> arrayList = new ArrayList();
        this(1);
        this.mReparentListeners = arrayList;
      } 
      return this.mReparentListeners.add(paramOnReparentListener);
    } 
  }
  
  public boolean removeOnReparentListener(OnReparentListener paramOnReparentListener) {
    synchronized (this.mLock) {
      boolean bool = this.mReparentListeners.remove(paramOnReparentListener);
      if (this.mReparentListeners.isEmpty())
        this.mReparentListeners = null; 
      return bool;
    } 
  }
  
  private void assignNativeObject(long paramLong, String paramString) {
    long l1 = this.mNativeObject, l2 = 0L;
    if (l1 != 0L)
      release(); 
    if (paramLong != 0L)
      this.mCloseGuard.openWithCallSite("release", paramString); 
    this.mNativeObject = paramLong;
    if (paramLong != 0L)
      l2 = nativeGetHandle(paramLong); 
    this.mNativeHandle = l2;
  }
  
  public void copyFrom(SurfaceControl paramSurfaceControl, String paramString) {
    this.mName = paramSurfaceControl.mName;
    this.mWidth = paramSurfaceControl.mWidth;
    this.mHeight = paramSurfaceControl.mHeight;
    this.mLocalOwnerView = paramSurfaceControl.mLocalOwnerView;
    assignNativeObject(nativeCopyFromSurfaceControl(paramSurfaceControl.mNativeObject), paramString);
  }
  
  class ScreenshotGraphicBuffer {
    private final ColorSpace mColorSpace;
    
    private final boolean mContainsSecureLayers;
    
    private final GraphicBuffer mGraphicBuffer;
    
    public ScreenshotGraphicBuffer(SurfaceControl this$0, ColorSpace param1ColorSpace, boolean param1Boolean) {
      this.mGraphicBuffer = (GraphicBuffer)this$0;
      this.mColorSpace = param1ColorSpace;
      this.mContainsSecureLayers = param1Boolean;
    }
    
    private static ScreenshotGraphicBuffer createFromNative(int param1Int1, int param1Int2, int param1Int3, int param1Int4, long param1Long, int param1Int5, boolean param1Boolean) {
      GraphicBuffer graphicBuffer = GraphicBuffer.createFromExisting(param1Int1, param1Int2, param1Int3, param1Int4, param1Long);
      ColorSpace colorSpace = ColorSpace.get(ColorSpace.Named.values()[param1Int5]);
      return new ScreenshotGraphicBuffer(graphicBuffer, colorSpace, param1Boolean);
    }
    
    public ColorSpace getColorSpace() {
      return this.mColorSpace;
    }
    
    public GraphicBuffer getGraphicBuffer() {
      return this.mGraphicBuffer;
    }
    
    public boolean containsSecureLayers() {
      return this.mContainsSecureLayers;
    }
  }
  
  class Builder {
    private int mFlags = 4;
    
    private int mFormat = -1;
    
    private String mCallsite = "SurfaceControl.Builder";
    
    private int mHeight;
    
    private WeakReference<View> mLocalOwnerView;
    
    private SparseIntArray mMetadata;
    
    private String mName;
    
    private SurfaceControl mParent;
    
    private SurfaceSession mSession;
    
    private int mWidth;
    
    public Builder(SurfaceControl this$0) {
      this.mSession = (SurfaceSession)this$0;
    }
    
    public SurfaceControl build() {
      int i = this.mWidth;
      if (i >= 0) {
        int j = this.mHeight;
        if (j >= 0) {
          if ((i <= 0 && j <= 0) || (!isEffectLayer() && !isContainerLayer()))
            return new SurfaceControl(this.mSession, this.mName, this.mWidth, this.mHeight, this.mFormat, this.mFlags, this.mParent, this.mMetadata, this.mLocalOwnerView, this.mCallsite); 
          throw new IllegalStateException("Only buffer layers can set a valid buffer size.");
        } 
      } 
      throw new IllegalStateException("width and height must be positive or unset");
    }
    
    public Builder setName(String param1String) {
      this.mName = param1String;
      return this;
    }
    
    public Builder setLocalOwnerView(View param1View) {
      this.mLocalOwnerView = new WeakReference<>(param1View);
      return this;
    }
    
    public Builder setBufferSize(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0 && param1Int2 >= 0) {
        this.mWidth = param1Int1;
        this.mHeight = param1Int2;
        return setFlags(0, 983040);
      } 
      throw new IllegalArgumentException("width and height must be positive");
    }
    
    private void unsetBufferSize() {
      this.mWidth = 0;
      this.mHeight = 0;
    }
    
    public Builder setFormat(int param1Int) {
      this.mFormat = param1Int;
      return this;
    }
    
    public Builder setProtected(boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= 0x800;
      } else {
        this.mFlags &= 0xFFFFF7FF;
      } 
      return this;
    }
    
    public Builder setSecure(boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= 0x80;
      } else {
        this.mFlags &= 0xFFFFFF7F;
      } 
      return this;
    }
    
    public Builder setOpaque(boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= 0x400;
      } else {
        this.mFlags &= 0xFFFFFBFF;
      } 
      return this;
    }
    
    public Builder setHidden(boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= 0x4;
      } else {
        this.mFlags &= 0xFFFFFFFB;
      } 
      return this;
    }
    
    public Builder setParent(SurfaceControl param1SurfaceControl) {
      this.mParent = param1SurfaceControl;
      return this;
    }
    
    public Builder setMetadata(int param1Int1, int param1Int2) {
      if (this.mMetadata == null)
        this.mMetadata = new SparseIntArray(); 
      this.mMetadata.put(param1Int1, param1Int2);
      return this;
    }
    
    public Builder setEffectLayer() {
      this.mFlags |= 0x4000;
      unsetBufferSize();
      return setFlags(131072, 983040);
    }
    
    public Builder setColorLayer() {
      unsetBufferSize();
      return setFlags(131072, 983040);
    }
    
    private boolean isEffectLayer() {
      boolean bool;
      if ((this.mFlags & 0x20000) == 131072) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public Builder setBLASTLayer() {
      unsetBufferSize();
      return setFlags(262144, 983040);
    }
    
    public Builder setContainerLayer() {
      unsetBufferSize();
      return setFlags(524288, 983040);
    }
    
    private boolean isContainerLayer() {
      boolean bool;
      if ((this.mFlags & 0x80000) == 524288) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public Builder setFlags(int param1Int) {
      this.mFlags = param1Int;
      return this;
    }
    
    public Builder setCallsite(String param1String) {
      this.mCallsite = param1String;
      return this;
    }
    
    private Builder setFlags(int param1Int1, int param1Int2) {
      this.mFlags = this.mFlags & (param1Int2 ^ 0xFFFFFFFF) | param1Int1;
      return this;
    }
    
    public Builder() {}
  }
  
  private SurfaceControl(SurfaceSession paramSurfaceSession, String paramString1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, SurfaceControl paramSurfaceControl, SparseIntArray paramSparseIntArray, WeakReference<View> paramWeakReference, String paramString2) throws Surface.OutOfResourcesException, IllegalArgumentException {
    if (paramString1 != null) {
      long l;
      this.mName = paramString1;
      this.mWidth = paramInt1;
      this.mHeight = paramInt2;
      this.mLocalOwnerView = paramWeakReference;
      Parcel parcel = Parcel.obtain();
      if (paramSparseIntArray != null)
        try {
          if (paramSparseIntArray.size() > 0) {
            parcel.writeInt(paramSparseIntArray.size());
            for (byte b = 0; b < paramSparseIntArray.size(); b++) {
              parcel.writeInt(paramSparseIntArray.keyAt(b));
              ByteBuffer byteBuffer = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder());
              byte[] arrayOfByte = byteBuffer.putInt(paramSparseIntArray.valueAt(b)).array();
              parcel.writeByteArray(arrayOfByte);
            } 
            parcel.setDataPosition(0);
          } 
        } finally {} 
      if (paramSurfaceControl != null) {
        l = paramSurfaceControl.mNativeObject;
      } else {
        l = 0L;
      } 
      try {
        this.mNativeObject = nativeCreate(paramSurfaceSession, paramString1, paramInt1, paramInt2, paramInt3, paramInt4, l, parcel);
        parcel.recycle();
        l = this.mNativeObject;
        if (l != 0L) {
          this.mNativeHandle = nativeGetHandle(l);
          this.mCloseGuard.openWithCallSite("release", paramString2);
          return;
        } 
        throw new Surface.OutOfResourcesException("Couldn't allocate SurfaceControl native object");
      } finally {}
      parcel.recycle();
      throw paramSurfaceSession;
    } 
    throw new IllegalArgumentException("name must not be null");
  }
  
  public SurfaceControl(SurfaceControl paramSurfaceControl, String paramString) {
    copyFrom(paramSurfaceControl, paramString);
  }
  
  private SurfaceControl(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    if (paramParcel != null) {
      this.mName = paramParcel.readString8();
      this.mWidth = paramParcel.readInt();
      this.mHeight = paramParcel.readInt();
      long l = 0L;
      if (paramParcel.readInt() != 0)
        l = nativeReadFromParcel(paramParcel); 
      assignNativeObject(l, "readFromParcel");
      return;
    } 
    throw new IllegalArgumentException("source must not be null");
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString8(this.mName);
    paramParcel.writeInt(this.mWidth);
    paramParcel.writeInt(this.mHeight);
    if (this.mNativeObject == 0L) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
    } 
    nativeWriteToParcel(this.mNativeObject, paramParcel);
    if ((paramInt & 0x1) != 0)
      release(); 
  }
  
  public boolean isSameSurface(SurfaceControl paramSurfaceControl) {
    boolean bool;
    if (paramSurfaceControl.mNativeHandle == this.mNativeHandle) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1120986464257L, System.identityHashCode(this));
    paramProtoOutputStream.write(1138166333442L, this.mName);
    paramProtoOutputStream.end(paramLong);
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      if (this.mNativeObject != 0L) {
        if (DEBUG_SFC) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("SurfaceControl finalize: ");
          stringBuilder.append(System.identityHashCode(this));
          stringBuilder.append(" this ");
          stringBuilder.append(this);
          stringBuilder.append(" caller=");
          stringBuilder.append(Debug.getCallers(5));
          String str = stringBuilder.toString();
          Log.v("SurfaceControl", str);
        } 
        nativeRelease(this.mNativeObject);
      } 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void release() {
    if (this.mNativeObject != 0L) {
      if (DEBUG_SFC) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SurfaceControl release: ");
        stringBuilder.append(System.identityHashCode(this));
        stringBuilder.append(" this ");
        stringBuilder.append(this);
        stringBuilder.append(" caller=");
        stringBuilder.append(Debug.getCallers(5));
        String str = stringBuilder.toString();
        Log.v("SurfaceControl", str);
      } 
      nativeRelease(this.mNativeObject);
      this.mNativeObject = 0L;
      this.mNativeHandle = 0L;
      this.mCloseGuard.close();
    } 
  }
  
  public void disconnect() {
    long l = this.mNativeObject;
    if (l != 0L)
      nativeDisconnect(l); 
  }
  
  private void checkNotReleased() {
    if (this.mNativeObject != 0L)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid ");
    stringBuilder.append(this);
    stringBuilder.append(", mNativeObject is null. Have you called release() already?");
    throw new NullPointerException(stringBuilder.toString());
  }
  
  public boolean isValid() {
    boolean bool;
    if (this.mNativeObject != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void openTransaction() {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   6: ifnonnull -> 21
    //   9: new android/view/SurfaceControl$Transaction
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   21: ldc android/view/SurfaceControl
    //   23: monitorenter
    //   24: getstatic android/view/SurfaceControl.sTransactionNestCount : J
    //   27: lconst_1
    //   28: ladd
    //   29: putstatic android/view/SurfaceControl.sTransactionNestCount : J
    //   32: ldc android/view/SurfaceControl
    //   34: monitorexit
    //   35: ldc android/view/SurfaceControl
    //   37: monitorexit
    //   38: return
    //   39: astore_0
    //   40: ldc android/view/SurfaceControl
    //   42: monitorexit
    //   43: aload_0
    //   44: athrow
    //   45: astore_0
    //   46: ldc android/view/SurfaceControl
    //   48: monitorexit
    //   49: aload_0
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1209	-> 0
    //   #1210	-> 3
    //   #1211	-> 9
    //   #1213	-> 21
    //   #1214	-> 24
    //   #1215	-> 32
    //   #1216	-> 35
    //   #1217	-> 38
    //   #1215	-> 39
    //   #1216	-> 45
    // Exception table:
    //   from	to	target	type
    //   3	9	45	finally
    //   9	21	45	finally
    //   21	24	45	finally
    //   24	32	39	finally
    //   32	35	39	finally
    //   35	38	45	finally
    //   40	43	39	finally
    //   43	45	45	finally
    //   46	49	45	finally
  }
  
  @Deprecated
  public static void mergeToGlobalTransaction(Transaction paramTransaction) {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   6: aload_0
    //   7: invokevirtual merge : (Landroid/view/SurfaceControl$Transaction;)Landroid/view/SurfaceControl$Transaction;
    //   10: pop
    //   11: ldc android/view/SurfaceControl
    //   13: monitorexit
    //   14: return
    //   15: astore_0
    //   16: ldc android/view/SurfaceControl
    //   18: monitorexit
    //   19: aload_0
    //   20: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1228	-> 0
    //   #1229	-> 3
    //   #1230	-> 11
    //   #1231	-> 14
    //   #1230	-> 15
    // Exception table:
    //   from	to	target	type
    //   3	11	15	finally
    //   11	14	15	finally
    //   16	19	15	finally
  }
  
  public static void closeTransaction() {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sTransactionNestCount : J
    //   6: lconst_0
    //   7: lcmp
    //   8: ifne -> 23
    //   11: ldc 'SurfaceControl'
    //   13: ldc_w 'Call to SurfaceControl.closeTransaction without matching openTransaction'
    //   16: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   19: pop
    //   20: goto -> 43
    //   23: getstatic android/view/SurfaceControl.sTransactionNestCount : J
    //   26: lconst_1
    //   27: lsub
    //   28: lstore_0
    //   29: lload_0
    //   30: putstatic android/view/SurfaceControl.sTransactionNestCount : J
    //   33: lload_0
    //   34: lconst_0
    //   35: lcmp
    //   36: ifle -> 43
    //   39: ldc android/view/SurfaceControl
    //   41: monitorexit
    //   42: return
    //   43: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   46: invokevirtual apply : ()V
    //   49: ldc android/view/SurfaceControl
    //   51: monitorexit
    //   52: return
    //   53: astore_2
    //   54: ldc android/view/SurfaceControl
    //   56: monitorexit
    //   57: aload_2
    //   58: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1238	-> 0
    //   #1239	-> 3
    //   #1240	-> 11
    //   #1242	-> 23
    //   #1243	-> 39
    //   #1245	-> 43
    //   #1246	-> 49
    //   #1247	-> 52
    //   #1246	-> 53
    // Exception table:
    //   from	to	target	type
    //   3	11	53	finally
    //   11	20	53	finally
    //   23	33	53	finally
    //   39	42	53	finally
    //   43	49	53	finally
    //   49	52	53	finally
    //   54	57	53	finally
  }
  
  public void deferTransactionUntil(SurfaceControl paramSurfaceControl, long paramLong) {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   6: aload_0
    //   7: aload_1
    //   8: lload_2
    //   9: invokevirtual deferTransactionUntil : (Landroid/view/SurfaceControl;Landroid/view/SurfaceControl;J)Landroid/view/SurfaceControl$Transaction;
    //   12: pop
    //   13: ldc android/view/SurfaceControl
    //   15: monitorexit
    //   16: return
    //   17: astore_1
    //   18: ldc android/view/SurfaceControl
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1253	-> 0
    //   #1254	-> 3
    //   #1255	-> 13
    //   #1256	-> 16
    //   #1255	-> 17
    // Exception table:
    //   from	to	target	type
    //   3	13	17	finally
    //   13	16	17	finally
    //   18	21	17	finally
  }
  
  public void reparentChildren(SurfaceControl paramSurfaceControl) {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   6: aload_0
    //   7: aload_1
    //   8: invokevirtual reparentChildren : (Landroid/view/SurfaceControl;Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    //   11: pop
    //   12: ldc android/view/SurfaceControl
    //   14: monitorexit
    //   15: return
    //   16: astore_1
    //   17: ldc android/view/SurfaceControl
    //   19: monitorexit
    //   20: aload_1
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1262	-> 0
    //   #1263	-> 3
    //   #1264	-> 12
    //   #1265	-> 15
    //   #1264	-> 16
    // Exception table:
    //   from	to	target	type
    //   3	12	16	finally
    //   12	15	16	finally
    //   17	20	16	finally
  }
  
  public void detachChildren() {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: aload_0
    //   4: getfield mName : Ljava/lang/String;
    //   7: ifnull -> 179
    //   10: aload_0
    //   11: getfield mName : Ljava/lang/String;
    //   14: ldc_w 'com.oppo.camera'
    //   17: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   20: ifne -> 143
    //   23: aload_0
    //   24: getfield mName : Ljava/lang/String;
    //   27: astore_1
    //   28: aload_1
    //   29: ldc_w 'com.youku.phone'
    //   32: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   35: ifne -> 143
    //   38: aload_0
    //   39: getfield mName : Ljava/lang/String;
    //   42: astore_1
    //   43: aload_1
    //   44: ldc_w 'com.coloros.compass'
    //   47: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   50: ifne -> 143
    //   53: aload_0
    //   54: getfield mName : Ljava/lang/String;
    //   57: astore_1
    //   58: aload_1
    //   59: ldc_w 'com.coloros.gallery3d'
    //   62: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   65: ifne -> 143
    //   68: aload_0
    //   69: getfield mName : Ljava/lang/String;
    //   72: astore_1
    //   73: aload_1
    //   74: ldc_w 'com.coloros.speechassist'
    //   77: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   80: ifne -> 143
    //   83: aload_0
    //   84: getfield mName : Ljava/lang/String;
    //   87: astore_1
    //   88: aload_1
    //   89: ldc_w 'com.tencent.qqlive'
    //   92: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   95: ifne -> 143
    //   98: aload_0
    //   99: getfield mName : Ljava/lang/String;
    //   102: astore_1
    //   103: aload_1
    //   104: ldc_w 'com.coloros.weather2'
    //   107: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   110: ifne -> 143
    //   113: aload_0
    //   114: getfield mName : Ljava/lang/String;
    //   117: astore_1
    //   118: aload_1
    //   119: ldc_w 'com.tencent.tmgp.sgame'
    //   122: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   125: ifne -> 143
    //   128: aload_0
    //   129: getfield mName : Ljava/lang/String;
    //   132: astore_1
    //   133: aload_1
    //   134: ldc_w 'com.tencent.mm.plugin.voip.ui.VideoActivity'
    //   137: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   140: ifeq -> 179
    //   143: new java/lang/StringBuilder
    //   146: astore_1
    //   147: aload_1
    //   148: invokespecial <init> : ()V
    //   151: aload_1
    //   152: ldc_w 'Not detachChildren, return '
    //   155: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   158: pop
    //   159: aload_1
    //   160: aload_0
    //   161: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: ldc 'SurfaceControl'
    //   167: aload_1
    //   168: invokevirtual toString : ()Ljava/lang/String;
    //   171: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   174: pop
    //   175: ldc android/view/SurfaceControl
    //   177: monitorexit
    //   178: return
    //   179: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   182: aload_0
    //   183: invokevirtual detachChildren : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    //   186: pop
    //   187: ldc android/view/SurfaceControl
    //   189: monitorexit
    //   190: return
    //   191: astore_1
    //   192: ldc android/view/SurfaceControl
    //   194: monitorexit
    //   195: aload_1
    //   196: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1271	-> 0
    //   #1274	-> 3
    //   #1275	-> 28
    //   #1276	-> 43
    //   #1277	-> 58
    //   #1278	-> 73
    //   #1279	-> 88
    //   #1280	-> 103
    //   #1281	-> 118
    //   #1282	-> 133
    //   #1283	-> 143
    //   #1284	-> 175
    //   #1287	-> 179
    //   #1288	-> 187
    //   #1289	-> 190
    //   #1288	-> 191
    // Exception table:
    //   from	to	target	type
    //   3	28	191	finally
    //   28	43	191	finally
    //   43	58	191	finally
    //   58	73	191	finally
    //   73	88	191	finally
    //   88	103	191	finally
    //   103	118	191	finally
    //   118	133	191	finally
    //   133	143	191	finally
    //   143	175	191	finally
    //   175	178	191	finally
    //   179	187	191	finally
    //   187	190	191	finally
    //   192	195	191	finally
  }
  
  public void setOverrideScalingMode(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: iload_1
    //   12: invokevirtual setOverrideScalingMode : (Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
    //   15: pop
    //   16: ldc android/view/SurfaceControl
    //   18: monitorexit
    //   19: return
    //   20: astore_2
    //   21: ldc android/view/SurfaceControl
    //   23: monitorexit
    //   24: aload_2
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1295	-> 0
    //   #1296	-> 4
    //   #1297	-> 7
    //   #1298	-> 16
    //   #1299	-> 19
    //   #1298	-> 20
    // Exception table:
    //   from	to	target	type
    //   7	16	20	finally
    //   16	19	20	finally
    //   21	24	20	finally
  }
  
  public void setLayer(int paramInt) {
    // Byte code:
    //   0: getstatic android/view/SurfaceControl.DEBUG_SFC : Z
    //   3: ifeq -> 63
    //   6: new java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore_2
    //   14: aload_2
    //   15: ldc_w 'setLayer zorder '
    //   18: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   21: pop
    //   22: aload_2
    //   23: iload_1
    //   24: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   27: pop
    //   28: aload_2
    //   29: aload_0
    //   30: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   33: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   36: pop
    //   37: aload_2
    //   38: ldc_w ' this '
    //   41: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: pop
    //   45: aload_2
    //   46: aload_0
    //   47: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload_2
    //   52: invokevirtual toString : ()Ljava/lang/String;
    //   55: astore_2
    //   56: ldc 'SurfaceControl'
    //   58: aload_2
    //   59: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   62: pop
    //   63: aload_0
    //   64: invokespecial checkNotReleased : ()V
    //   67: ldc android/view/SurfaceControl
    //   69: monitorenter
    //   70: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   73: aload_0
    //   74: iload_1
    //   75: invokevirtual setLayer : (Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
    //   78: pop
    //   79: ldc android/view/SurfaceControl
    //   81: monitorexit
    //   82: return
    //   83: astore_2
    //   84: ldc android/view/SurfaceControl
    //   86: monitorexit
    //   87: aload_2
    //   88: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1308	-> 0
    //   #1309	-> 6
    //   #1310	-> 28
    //   #1309	-> 56
    //   #1313	-> 63
    //   #1314	-> 67
    //   #1315	-> 70
    //   #1316	-> 79
    //   #1317	-> 82
    //   #1316	-> 83
    // Exception table:
    //   from	to	target	type
    //   70	79	83	finally
    //   79	82	83	finally
    //   84	87	83	finally
  }
  
  public void setPosition(float paramFloat1, float paramFloat2) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: fload_1
    //   12: fload_2
    //   13: invokevirtual setPosition : (Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
    //   16: pop
    //   17: ldc android/view/SurfaceControl
    //   19: monitorexit
    //   20: return
    //   21: astore_3
    //   22: ldc android/view/SurfaceControl
    //   24: monitorexit
    //   25: aload_3
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1324	-> 0
    //   #1325	-> 4
    //   #1326	-> 7
    //   #1327	-> 17
    //   #1328	-> 20
    //   #1327	-> 21
    // Exception table:
    //   from	to	target	type
    //   7	17	21	finally
    //   17	20	21	finally
    //   22	25	21	finally
  }
  
  public void setBufferSize(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: getstatic android/view/SurfaceControl.DEBUG_SFC : Z
    //   3: ifeq -> 85
    //   6: new java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore_3
    //   14: aload_3
    //   15: ldc_w 'SurfaceControl setBufferSize: w '
    //   18: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   21: pop
    //   22: aload_3
    //   23: iload_1
    //   24: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   27: pop
    //   28: aload_3
    //   29: ldc_w ' h '
    //   32: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: aload_3
    //   37: iload_2
    //   38: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   41: pop
    //   42: aload_3
    //   43: ldc_w ' '
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_3
    //   51: aload_0
    //   52: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   55: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   58: pop
    //   59: aload_3
    //   60: ldc_w ' this '
    //   63: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload_3
    //   68: aload_0
    //   69: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload_3
    //   74: invokevirtual toString : ()Ljava/lang/String;
    //   77: astore_3
    //   78: ldc 'SurfaceControl'
    //   80: aload_3
    //   81: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   84: pop
    //   85: aload_0
    //   86: invokespecial checkNotReleased : ()V
    //   89: ldc android/view/SurfaceControl
    //   91: monitorenter
    //   92: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   95: aload_0
    //   96: iload_1
    //   97: iload_2
    //   98: invokevirtual setBufferSize : (Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;
    //   101: pop
    //   102: ldc android/view/SurfaceControl
    //   104: monitorexit
    //   105: return
    //   106: astore_3
    //   107: ldc android/view/SurfaceControl
    //   109: monitorexit
    //   110: aload_3
    //   111: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1336	-> 0
    //   #1337	-> 6
    //   #1338	-> 50
    //   #1337	-> 78
    //   #1341	-> 85
    //   #1342	-> 89
    //   #1343	-> 92
    //   #1344	-> 102
    //   #1345	-> 105
    //   #1344	-> 106
    // Exception table:
    //   from	to	target	type
    //   92	102	106	finally
    //   102	105	106	finally
    //   107	110	106	finally
  }
  
  public void hide() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: invokevirtual hide : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    //   14: pop
    //   15: ldc android/view/SurfaceControl
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: ldc android/view/SurfaceControl
    //   22: monitorexit
    //   23: aload_1
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1352	-> 0
    //   #1353	-> 4
    //   #1354	-> 7
    //   #1355	-> 15
    //   #1356	-> 18
    //   #1355	-> 19
    // Exception table:
    //   from	to	target	type
    //   7	15	19	finally
    //   15	18	19	finally
    //   20	23	19	finally
  }
  
  public void show() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: invokevirtual show : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    //   14: pop
    //   15: ldc android/view/SurfaceControl
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: ldc android/view/SurfaceControl
    //   22: monitorexit
    //   23: aload_1
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1363	-> 0
    //   #1364	-> 4
    //   #1365	-> 7
    //   #1366	-> 15
    //   #1367	-> 18
    //   #1366	-> 19
    // Exception table:
    //   from	to	target	type
    //   7	15	19	finally
    //   15	18	19	finally
    //   20	23	19	finally
  }
  
  public void setTransparentRegionHint(Region paramRegion) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: aload_1
    //   12: invokevirtual setTransparentRegionHint : (Landroid/view/SurfaceControl;Landroid/graphics/Region;)Landroid/view/SurfaceControl$Transaction;
    //   15: pop
    //   16: ldc android/view/SurfaceControl
    //   18: monitorexit
    //   19: return
    //   20: astore_1
    //   21: ldc android/view/SurfaceControl
    //   23: monitorexit
    //   24: aload_1
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1373	-> 0
    //   #1374	-> 4
    //   #1375	-> 7
    //   #1376	-> 16
    //   #1377	-> 19
    //   #1376	-> 20
    // Exception table:
    //   from	to	target	type
    //   7	16	20	finally
    //   16	19	20	finally
    //   21	24	20	finally
  }
  
  public boolean clearContentFrameStats() {
    checkNotReleased();
    return nativeClearContentFrameStats(this.mNativeObject);
  }
  
  public boolean getContentFrameStats(WindowContentFrameStats paramWindowContentFrameStats) {
    checkNotReleased();
    return nativeGetContentFrameStats(this.mNativeObject, paramWindowContentFrameStats);
  }
  
  public static boolean clearAnimationFrameStats() {
    return nativeClearAnimationFrameStats();
  }
  
  public static boolean getAnimationFrameStats(WindowAnimationFrameStats paramWindowAnimationFrameStats) {
    return nativeGetAnimationFrameStats(paramWindowAnimationFrameStats);
  }
  
  public void setAlpha(float paramFloat) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: fload_1
    //   12: invokevirtual setAlpha : (Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
    //   15: pop
    //   16: ldc android/view/SurfaceControl
    //   18: monitorexit
    //   19: return
    //   20: astore_2
    //   21: ldc android/view/SurfaceControl
    //   23: monitorexit
    //   24: aload_2
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1413	-> 0
    //   #1414	-> 4
    //   #1415	-> 7
    //   #1416	-> 16
    //   #1417	-> 19
    //   #1416	-> 20
    // Exception table:
    //   from	to	target	type
    //   7	16	20	finally
    //   16	19	20	finally
    //   21	24	20	finally
  }
  
  public void setMatrix(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: fload_1
    //   12: fload_2
    //   13: fload_3
    //   14: fload #4
    //   16: invokevirtual setMatrix : (Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;
    //   19: pop
    //   20: ldc android/view/SurfaceControl
    //   22: monitorexit
    //   23: return
    //   24: astore #5
    //   26: ldc android/view/SurfaceControl
    //   28: monitorexit
    //   29: aload #5
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1423	-> 0
    //   #1424	-> 4
    //   #1425	-> 7
    //   #1426	-> 20
    //   #1427	-> 23
    //   #1426	-> 24
    // Exception table:
    //   from	to	target	type
    //   7	20	24	finally
    //   20	23	24	finally
    //   26	29	24	finally
  }
  
  public void setColorSpaceAgnostic(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: iload_1
    //   12: invokevirtual setColorSpaceAgnostic : (Landroid/view/SurfaceControl;Z)Landroid/view/SurfaceControl$Transaction;
    //   15: pop
    //   16: ldc android/view/SurfaceControl
    //   18: monitorexit
    //   19: return
    //   20: astore_2
    //   21: ldc android/view/SurfaceControl
    //   23: monitorexit
    //   24: aload_2
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1436	-> 0
    //   #1437	-> 4
    //   #1438	-> 7
    //   #1439	-> 16
    //   #1440	-> 19
    //   #1439	-> 20
    // Exception table:
    //   from	to	target	type
    //   7	16	20	finally
    //   16	19	20	finally
    //   21	24	20	finally
  }
  
  public void setWindowCrop(Rect paramRect) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: aload_1
    //   12: invokevirtual setWindowCrop : (Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
    //   15: pop
    //   16: ldc android/view/SurfaceControl
    //   18: monitorexit
    //   19: return
    //   20: astore_1
    //   21: ldc android/view/SurfaceControl
    //   23: monitorexit
    //   24: aload_1
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1452	-> 0
    //   #1453	-> 4
    //   #1454	-> 7
    //   #1455	-> 16
    //   #1456	-> 19
    //   #1455	-> 20
    // Exception table:
    //   from	to	target	type
    //   7	16	20	finally
    //   16	19	20	finally
    //   21	24	20	finally
  }
  
  public void setOpaque(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: iload_1
    //   12: invokevirtual setOpaque : (Landroid/view/SurfaceControl;Z)Landroid/view/SurfaceControl$Transaction;
    //   15: pop
    //   16: ldc android/view/SurfaceControl
    //   18: monitorexit
    //   19: return
    //   20: astore_2
    //   21: ldc android/view/SurfaceControl
    //   23: monitorexit
    //   24: aload_2
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1462	-> 0
    //   #1464	-> 4
    //   #1465	-> 7
    //   #1466	-> 16
    //   #1467	-> 19
    //   #1466	-> 20
    // Exception table:
    //   from	to	target	type
    //   7	16	20	finally
    //   16	19	20	finally
    //   21	24	20	finally
  }
  
  public void setSecure(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkNotReleased : ()V
    //   4: ldc android/view/SurfaceControl
    //   6: monitorenter
    //   7: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   10: aload_0
    //   11: iload_1
    //   12: invokevirtual setSecure : (Landroid/view/SurfaceControl;Z)Landroid/view/SurfaceControl$Transaction;
    //   15: pop
    //   16: ldc android/view/SurfaceControl
    //   18: monitorexit
    //   19: return
    //   20: astore_2
    //   21: ldc android/view/SurfaceControl
    //   23: monitorexit
    //   24: aload_2
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1473	-> 0
    //   #1475	-> 4
    //   #1476	-> 7
    //   #1477	-> 16
    //   #1478	-> 19
    //   #1477	-> 20
    // Exception table:
    //   from	to	target	type
    //   7	16	20	finally
    //   16	19	20	finally
    //   21	24	20	finally
  }
  
  public int getWidth() {
    synchronized (this.mLock) {
      return this.mWidth;
    } 
  }
  
  public int getHeight() {
    synchronized (this.mLock) {
      return this.mHeight;
    } 
  }
  
  public View getLocalOwnerView() {
    WeakReference<View> weakReference = this.mLocalOwnerView;
    if (weakReference != null) {
      View view = weakReference.get();
    } else {
      weakReference = null;
    } 
    return (View)weakReference;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Surface(name=");
    stringBuilder.append(this.mName);
    stringBuilder.append(")/@0x");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    return stringBuilder.toString();
  }
  
  class DisplayInfo {
    public float density;
    
    public DeviceProductInfo deviceProductInfo;
    
    public boolean isInternal;
    
    public boolean secure;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DisplayInfo{isInternal=");
      stringBuilder.append(this.isInternal);
      stringBuilder.append(", density=");
      stringBuilder.append(this.density);
      stringBuilder.append(", secure=");
      stringBuilder.append(this.secure);
      stringBuilder.append(", deviceProductInfo=");
      stringBuilder.append(this.deviceProductInfo);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class DisplayConfig {
    public static final int INVALID_DISPLAY_CONFIG_ID = -1;
    
    public long appVsyncOffsetNanos;
    
    public int configGroup;
    
    public int height;
    
    public long presentationDeadlineNanos;
    
    public float refreshRate;
    
    public int width;
    
    public float xDpi;
    
    public float yDpi;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DisplayConfig{width=");
      stringBuilder.append(this.width);
      stringBuilder.append(", height=");
      stringBuilder.append(this.height);
      stringBuilder.append(", xDpi=");
      stringBuilder.append(this.xDpi);
      stringBuilder.append(", yDpi=");
      stringBuilder.append(this.yDpi);
      stringBuilder.append(", refreshRate=");
      stringBuilder.append(this.refreshRate);
      stringBuilder.append(", appVsyncOffsetNanos=");
      stringBuilder.append(this.appVsyncOffsetNanos);
      stringBuilder.append(", presentationDeadlineNanos=");
      stringBuilder.append(this.presentationDeadlineNanos);
      stringBuilder.append(", configGroup=");
      stringBuilder.append(this.configGroup);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  public static void setDisplayPowerMode(IBinder paramIBinder, int paramInt) {
    if (paramIBinder != null) {
      nativeSetDisplayPowerMode(paramIBinder, paramInt);
      return;
    } 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static DisplayInfo getDisplayInfo(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetDisplayInfo(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static DisplayConfig[] getDisplayConfigs(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetDisplayConfigs(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static int getActiveConfig(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetActiveConfig(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static DisplayedContentSamplingAttributes getDisplayedContentSamplingAttributes(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetDisplayedContentSamplingAttributes(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static boolean setDisplayedContentSamplingEnabled(IBinder paramIBinder, boolean paramBoolean, int paramInt1, int paramInt2) {
    if (paramIBinder != null) {
      if (paramInt1 >> 4 == 0)
        return nativeSetDisplayedContentSamplingEnabled(paramIBinder, paramBoolean, paramInt1, paramInt2); 
      throw new IllegalArgumentException("invalid componentMask when enabling sampling");
    } 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static DisplayedContentSample getDisplayedContentSample(IBinder paramIBinder, long paramLong1, long paramLong2) {
    if (paramIBinder != null)
      return nativeGetDisplayedContentSample(paramIBinder, paramLong1, paramLong2); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  class DesiredDisplayConfigSpecs {
    public float appRequestRefreshRateMax;
    
    public float appRequestRefreshRateMin;
    
    public int defaultConfig;
    
    public float primaryRefreshRateMax;
    
    public float primaryRefreshRateMin;
    
    public DesiredDisplayConfigSpecs() {}
    
    public DesiredDisplayConfigSpecs(SurfaceControl this$0) {
      copyFrom((DesiredDisplayConfigSpecs)this$0);
    }
    
    public DesiredDisplayConfigSpecs(SurfaceControl this$0, float param1Float1, float param1Float2, float param1Float3, float param1Float4) {
      this.defaultConfig = this$0;
      this.primaryRefreshRateMin = param1Float1;
      this.primaryRefreshRateMax = param1Float2;
      this.appRequestRefreshRateMin = param1Float3;
      this.appRequestRefreshRateMax = param1Float4;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool;
      if (param1Object instanceof DesiredDisplayConfigSpecs && equals((DesiredDisplayConfigSpecs)param1Object)) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean equals(DesiredDisplayConfigSpecs param1DesiredDisplayConfigSpecs) {
      boolean bool;
      if (param1DesiredDisplayConfigSpecs != null && this.defaultConfig == param1DesiredDisplayConfigSpecs.defaultConfig && this.primaryRefreshRateMin == param1DesiredDisplayConfigSpecs.primaryRefreshRateMin && this.primaryRefreshRateMax == param1DesiredDisplayConfigSpecs.primaryRefreshRateMax && this.appRequestRefreshRateMin == param1DesiredDisplayConfigSpecs.appRequestRefreshRateMin && this.appRequestRefreshRateMax == param1DesiredDisplayConfigSpecs.appRequestRefreshRateMax) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int hashCode() {
      return 0;
    }
    
    public void copyFrom(DesiredDisplayConfigSpecs param1DesiredDisplayConfigSpecs) {
      this.defaultConfig = param1DesiredDisplayConfigSpecs.defaultConfig;
      this.primaryRefreshRateMin = param1DesiredDisplayConfigSpecs.primaryRefreshRateMin;
      this.primaryRefreshRateMax = param1DesiredDisplayConfigSpecs.primaryRefreshRateMax;
      this.appRequestRefreshRateMin = param1DesiredDisplayConfigSpecs.appRequestRefreshRateMin;
      this.appRequestRefreshRateMax = param1DesiredDisplayConfigSpecs.appRequestRefreshRateMax;
    }
    
    public String toString() {
      int i = this.defaultConfig;
      float f1 = this.primaryRefreshRateMin, f2 = this.primaryRefreshRateMax, f3 = this.appRequestRefreshRateMin;
      float f4 = this.appRequestRefreshRateMax;
      return String.format("defaultConfig=%d primaryRefreshRateRange=[%.0f %.0f] appRequestRefreshRateRange=[%.0f %.0f]", new Object[] { Integer.valueOf(i), Float.valueOf(f1), Float.valueOf(f2), Float.valueOf(f3), Float.valueOf(f4) });
    }
  }
  
  public static boolean setDesiredDisplayConfigSpecs(IBinder paramIBinder, DesiredDisplayConfigSpecs paramDesiredDisplayConfigSpecs) {
    if (paramIBinder != null) {
      if (DEBUG_SFC) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setDesiredDisplayConfigSpecs displayToken ");
        stringBuilder.append(paramIBinder);
        stringBuilder.append(" ,desiredDisplayConfigSpecs=");
        stringBuilder.append(paramDesiredDisplayConfigSpecs);
        Log.v("SurfaceControl", stringBuilder.toString());
      } 
      return nativeSetDesiredDisplayConfigSpecs(paramIBinder, paramDesiredDisplayConfigSpecs);
    } 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static DesiredDisplayConfigSpecs getDesiredDisplayConfigSpecs(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetDesiredDisplayConfigSpecs(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static int[] getDisplayColorModes(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetDisplayColorModes(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  class CieXyz {
    public float X;
    
    public float Y;
    
    public float Z;
  }
  
  class DisplayPrimaries {
    public SurfaceControl.CieXyz blue;
    
    public SurfaceControl.CieXyz green;
    
    public SurfaceControl.CieXyz red;
    
    public SurfaceControl.CieXyz white;
  }
  
  public static DisplayPrimaries getDisplayNativePrimaries(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetDisplayNativePrimaries(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static int getActiveColorMode(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetActiveColorMode(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static boolean setActiveColorMode(IBinder paramIBinder, int paramInt) {
    if (paramIBinder != null)
      return nativeSetActiveColorMode(paramIBinder, paramInt); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static ColorSpace[] getCompositionColorSpaces() {
    int[] arrayOfInt = nativeGetCompositionDataspaces();
    ColorSpace colorSpace = ColorSpace.get(ColorSpace.Named.SRGB);
    ColorSpace[] arrayOfColorSpace = new ColorSpace[2];
    arrayOfColorSpace[0] = colorSpace;
    arrayOfColorSpace[1] = colorSpace;
    if (arrayOfInt.length == 2)
      for (byte b = 0; b < 2; b++) {
        int i = arrayOfInt[b];
        if (i != 143261696) {
          if (i == 411107328)
            arrayOfColorSpace[b] = ColorSpace.get(ColorSpace.Named.EXTENDED_SRGB); 
        } else {
          arrayOfColorSpace[b] = ColorSpace.get(ColorSpace.Named.DISPLAY_P3);
        } 
      }  
    return arrayOfColorSpace;
  }
  
  public static void setAutoLowLatencyMode(IBinder paramIBinder, boolean paramBoolean) {
    if (paramIBinder != null) {
      nativeSetAutoLowLatencyMode(paramIBinder, paramBoolean);
      return;
    } 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static void setGameContentType(IBinder paramIBinder, boolean paramBoolean) {
    if (paramIBinder != null) {
      nativeSetGameContentType(paramIBinder, paramBoolean);
      return;
    } 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static void setDisplayProjection(IBinder paramIBinder, int paramInt, Rect paramRect1, Rect paramRect2) {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   6: aload_0
    //   7: iload_1
    //   8: aload_2
    //   9: aload_3
    //   10: invokevirtual setDisplayProjection : (Landroid/os/IBinder;ILandroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
    //   13: pop
    //   14: ldc android/view/SurfaceControl
    //   16: monitorexit
    //   17: return
    //   18: astore_0
    //   19: ldc android/view/SurfaceControl
    //   21: monitorexit
    //   22: aload_0
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1921	-> 0
    //   #1922	-> 3
    //   #1924	-> 14
    //   #1925	-> 17
    //   #1924	-> 18
    // Exception table:
    //   from	to	target	type
    //   3	14	18	finally
    //   14	17	18	finally
    //   19	22	18	finally
  }
  
  public static void setDisplayLayerStack(IBinder paramIBinder, int paramInt) {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   6: aload_0
    //   7: iload_1
    //   8: invokevirtual setDisplayLayerStack : (Landroid/os/IBinder;I)Landroid/view/SurfaceControl$Transaction;
    //   11: pop
    //   12: ldc android/view/SurfaceControl
    //   14: monitorexit
    //   15: return
    //   16: astore_0
    //   17: ldc android/view/SurfaceControl
    //   19: monitorexit
    //   20: aload_0
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1932	-> 0
    //   #1933	-> 3
    //   #1934	-> 12
    //   #1935	-> 15
    //   #1934	-> 16
    // Exception table:
    //   from	to	target	type
    //   3	12	16	finally
    //   12	15	16	finally
    //   17	20	16	finally
  }
  
  public static void setDisplaySurface(IBinder paramIBinder, Surface paramSurface) {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   6: aload_0
    //   7: aload_1
    //   8: invokevirtual setDisplaySurface : (Landroid/os/IBinder;Landroid/view/Surface;)Landroid/view/SurfaceControl$Transaction;
    //   11: pop
    //   12: ldc android/view/SurfaceControl
    //   14: monitorexit
    //   15: return
    //   16: astore_0
    //   17: ldc android/view/SurfaceControl
    //   19: monitorexit
    //   20: aload_0
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1942	-> 0
    //   #1943	-> 3
    //   #1944	-> 12
    //   #1945	-> 15
    //   #1944	-> 16
    // Exception table:
    //   from	to	target	type
    //   3	12	16	finally
    //   12	15	16	finally
    //   17	20	16	finally
  }
  
  public static void setDisplaySize(IBinder paramIBinder, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: ldc android/view/SurfaceControl
    //   2: monitorenter
    //   3: getstatic android/view/SurfaceControl.sGlobalTransaction : Landroid/view/SurfaceControl$Transaction;
    //   6: aload_0
    //   7: iload_1
    //   8: iload_2
    //   9: invokevirtual setDisplaySize : (Landroid/os/IBinder;II)Landroid/view/SurfaceControl$Transaction;
    //   12: pop
    //   13: ldc android/view/SurfaceControl
    //   15: monitorexit
    //   16: return
    //   17: astore_0
    //   18: ldc android/view/SurfaceControl
    //   20: monitorexit
    //   21: aload_0
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1951	-> 0
    //   #1952	-> 3
    //   #1953	-> 13
    //   #1954	-> 16
    //   #1953	-> 17
    // Exception table:
    //   from	to	target	type
    //   3	13	17	finally
    //   13	16	17	finally
    //   18	21	17	finally
  }
  
  public static Display.HdrCapabilities getHdrCapabilities(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetHdrCapabilities(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static boolean getAutoLowLatencyModeSupport(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetAutoLowLatencyModeSupport(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static boolean getGameContentTypeSupport(IBinder paramIBinder) {
    if (paramIBinder != null)
      return nativeGetGameContentTypeSupport(paramIBinder); 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static IBinder createDisplay(String paramString, boolean paramBoolean) {
    if (paramString != null)
      return nativeCreateDisplay(paramString, paramBoolean); 
    throw new IllegalArgumentException("name must not be null");
  }
  
  public static void destroyDisplay(IBinder paramIBinder) {
    if (paramIBinder != null) {
      nativeDestroyDisplay(paramIBinder);
      return;
    } 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static long[] getPhysicalDisplayIds() {
    return nativeGetPhysicalDisplayIds();
  }
  
  public static IBinder getPhysicalDisplayToken(long paramLong) {
    return nativeGetPhysicalDisplayToken(paramLong);
  }
  
  public static IBinder getInternalDisplayToken() {
    long[] arrayOfLong = getPhysicalDisplayIds();
    if (arrayOfLong.length == 0)
      return null; 
    return getPhysicalDisplayToken(arrayOfLong[0]);
  }
  
  public static void screenshot(IBinder paramIBinder, Surface paramSurface) {
    screenshot(paramIBinder, paramSurface, new Rect(), 0, 0, false, 0);
  }
  
  public static void screenshot(IBinder paramIBinder, Surface paramSurface, Rect paramRect, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    if (paramSurface != null) {
      ScreenshotGraphicBuffer screenshotGraphicBuffer = screenshotToBuffer(paramIBinder, paramRect, paramInt1, paramInt2, paramBoolean, paramInt3);
      if (screenshotGraphicBuffer == null)
        return; 
      try {
        GraphicBuffer graphicBuffer = screenshotGraphicBuffer.getGraphicBuffer();
        ColorSpace colorSpace = screenshotGraphicBuffer.getColorSpace();
        paramSurface.attachAndQueueBufferWithColorSpace(graphicBuffer, colorSpace);
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to take screenshot - ");
        stringBuilder.append(runtimeException.getMessage());
        Log.w("SurfaceControl", stringBuilder.toString());
      } 
      return;
    } 
    throw new IllegalArgumentException("consumer must not be null");
  }
  
  public static Bitmap screenshot(Rect paramRect, int paramInt1, int paramInt2, int paramInt3) {
    return screenshot(paramRect, paramInt1, paramInt2, false, paramInt3);
  }
  
  public static Bitmap screenshot(Rect paramRect, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    // Byte code:
    //   0: invokestatic getInternalDisplayToken : ()Landroid/os/IBinder;
    //   3: astore #5
    //   5: aload #5
    //   7: ifnonnull -> 21
    //   10: ldc 'SurfaceControl'
    //   12: ldc_w 'Failed to take screenshot because internal display is disconnected'
    //   15: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   18: pop
    //   19: aconst_null
    //   20: areturn
    //   21: iconst_3
    //   22: istore #6
    //   24: iload #4
    //   26: iconst_1
    //   27: if_icmpeq -> 40
    //   30: iload #4
    //   32: istore #7
    //   34: iload #4
    //   36: iconst_3
    //   37: if_icmpne -> 60
    //   40: iload #4
    //   42: iconst_1
    //   43: if_icmpne -> 53
    //   46: iload #6
    //   48: istore #4
    //   50: goto -> 56
    //   53: iconst_1
    //   54: istore #4
    //   56: iload #4
    //   58: istore #7
    //   60: aload_0
    //   61: iload #7
    //   63: invokestatic rotateCropForSF : (Landroid/graphics/Rect;I)V
    //   66: aload #5
    //   68: aload_0
    //   69: iload_1
    //   70: iload_2
    //   71: iload_3
    //   72: iload #7
    //   74: invokestatic screenshotToBuffer : (Landroid/os/IBinder;Landroid/graphics/Rect;IIZI)Landroid/view/SurfaceControl$ScreenshotGraphicBuffer;
    //   77: astore_0
    //   78: aload_0
    //   79: ifnonnull -> 93
    //   82: ldc 'SurfaceControl'
    //   84: ldc_w 'Failed to take screenshot'
    //   87: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   90: pop
    //   91: aconst_null
    //   92: areturn
    //   93: aload_0
    //   94: invokevirtual getGraphicBuffer : ()Landroid/graphics/GraphicBuffer;
    //   97: aload_0
    //   98: invokevirtual getColorSpace : ()Landroid/graphics/ColorSpace;
    //   101: invokestatic wrapHardwareBuffer : (Landroid/graphics/GraphicBuffer;Landroid/graphics/ColorSpace;)Landroid/graphics/Bitmap;
    //   104: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2101	-> 0
    //   #2102	-> 5
    //   #2103	-> 10
    //   #2104	-> 19
    //   #2107	-> 21
    //   #2108	-> 40
    //   #2111	-> 60
    //   #2112	-> 66
    //   #2115	-> 78
    //   #2116	-> 82
    //   #2117	-> 91
    //   #2119	-> 93
  }
  
  public static ScreenshotGraphicBuffer screenshotToBuffer(IBinder paramIBinder, Rect paramRect, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    if (paramIBinder != null) {
      if (OplusCustomizeScreenCaptureHelper.isForbidCaptureScreen())
        return null; 
      OplusScreenDragUtil.scaleScreenshotIfNeeded(paramRect);
      return nativeScreenshot(paramIBinder, paramRect, paramInt1, paramInt2, paramBoolean, paramInt3, false);
    } 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  public static ScreenshotGraphicBuffer screenshotToBufferWithSecureLayersUnsafe(IBinder paramIBinder, Rect paramRect, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    if (paramIBinder != null) {
      if (OplusCustomizeScreenCaptureHelper.isForbidCaptureScreen())
        return null; 
      return nativeScreenshot(paramIBinder, paramRect, paramInt1, paramInt2, paramBoolean, paramInt3, true);
    } 
    throw new IllegalArgumentException("displayToken must not be null");
  }
  
  private static void rotateCropForSF(Rect paramRect, int paramInt) {
    if (paramInt == 1 || paramInt == 3) {
      paramInt = paramRect.top;
      paramRect.top = paramRect.left;
      paramRect.left = paramInt;
      paramInt = paramRect.right;
      paramRect.right = paramRect.bottom;
      paramRect.bottom = paramInt;
    } 
  }
  
  public static ScreenshotGraphicBuffer captureLayers(SurfaceControl paramSurfaceControl, Rect paramRect, float paramFloat) {
    return captureLayers(paramSurfaceControl, paramRect, paramFloat, 1);
  }
  
  public static ScreenshotGraphicBuffer captureLayers(SurfaceControl paramSurfaceControl, Rect paramRect, float paramFloat, int paramInt) {
    if (OplusCustomizeScreenCaptureHelper.isForbidCaptureScreen())
      return null; 
    IBinder iBinder = getInternalDisplayToken();
    return nativeCaptureLayers(iBinder, paramSurfaceControl.mNativeObject, paramRect, paramFloat, null, paramInt);
  }
  
  public static ScreenshotGraphicBuffer captureLayersExcluding(SurfaceControl paramSurfaceControl, Rect paramRect, float paramFloat, int paramInt, SurfaceControl[] paramArrayOfSurfaceControl) {
    if (OplusCustomizeScreenCaptureHelper.isForbidCaptureScreen())
      return null; 
    IBinder iBinder = getInternalDisplayToken();
    long[] arrayOfLong = new long[paramArrayOfSurfaceControl.length];
    for (paramInt = 0; paramInt < paramArrayOfSurfaceControl.length; paramInt++)
      arrayOfLong[paramInt] = (paramArrayOfSurfaceControl[paramInt]).mNativeObject; 
    return nativeCaptureLayers(iBinder, paramSurfaceControl.mNativeObject, paramRect, paramFloat, arrayOfLong, 1);
  }
  
  public static boolean getProtectedContentSupport() {
    return nativeGetProtectedContentSupport();
  }
  
  public static boolean getDisplayBrightnessSupport(IBinder paramIBinder) {
    return nativeGetDisplayBrightnessSupport(paramIBinder);
  }
  
  public static boolean setDisplayBrightness(IBinder paramIBinder, float paramFloat) {
    Objects.requireNonNull(paramIBinder);
    if (!Float.isNaN(paramFloat) && paramFloat <= 1.0F && (paramFloat >= 0.0F || paramFloat == -1.0F))
      return nativeSetDisplayBrightness(paramIBinder, paramFloat); 
    throw new IllegalArgumentException("brightness must be a number between 0.0f and 1.0f, or -1 to turn the backlight off.");
  }
  
  public static SurfaceControl mirrorSurface(SurfaceControl paramSurfaceControl) {
    long l = nativeMirrorSurface(paramSurfaceControl.mNativeObject);
    paramSurfaceControl = new SurfaceControl();
    paramSurfaceControl.assignNativeObject(l, "mirrorSurface");
    return paramSurfaceControl;
  }
  
  private static void validateColorArg(float[] paramArrayOffloat) {
    if (paramArrayOffloat.length == 4) {
      int i;
      byte b;
      for (i = paramArrayOffloat.length, b = 0; b < i; ) {
        float f = paramArrayOffloat[b];
        if (f >= 0.0F && f <= 1.0F) {
          b++;
          continue;
        } 
        throw new IllegalArgumentException("Color must be specified as a float array with four values to represent r, g, b, a in range [0..1]");
      } 
      return;
    } 
    throw new IllegalArgumentException("Color must be specified as a float array with four values to represent r, g, b, a in range [0..1]");
  }
  
  public static void setGlobalShadowSettings(float[] paramArrayOffloat1, float[] paramArrayOffloat2, float paramFloat1, float paramFloat2, float paramFloat3) {
    validateColorArg(paramArrayOffloat1);
    validateColorArg(paramArrayOffloat2);
    nativeSetGlobalShadowSettings(paramArrayOffloat1, paramArrayOffloat2, paramFloat1, paramFloat2, paramFloat3);
  }
  
  public static class Transaction implements Closeable, Parcelable {
    static {
    
    }
    
    private final ArrayMap<SurfaceControl, Point> mResizedSurfaces = new ArrayMap<>();
    
    private final ArrayMap<SurfaceControl, SurfaceControl> mReparentedSurfaces = new ArrayMap<>();
    
    public long mNativeObject;
    
    Runnable mFreeNativeResources;
    
    public static final NativeAllocationRegistry sRegistry;
    
    private static final float[] INVALID_COLOR = new float[] { -1.0F, -1.0F, -1.0F };
    
    public static final Parcelable.Creator<Transaction> CREATOR = new Parcelable.Creator<Transaction>() {
        public SurfaceControl.Transaction createFromParcel(Parcel param2Parcel) {
          return new SurfaceControl.Transaction(param2Parcel);
        }
        
        public SurfaceControl.Transaction[] newArray(int param2Int) {
          return new SurfaceControl.Transaction[param2Int];
        }
      };
    
    protected void checkPreconditions(SurfaceControl param1SurfaceControl) {
      param1SurfaceControl.checkNotReleased();
    }
    
    public Transaction() {
      long l = SurfaceControl.nativeCreateTransaction();
      NativeAllocationRegistry nativeAllocationRegistry = sRegistry;
      this.mFreeNativeResources = nativeAllocationRegistry.registerNativeAllocation(this, l);
    }
    
    private Transaction(Parcel param1Parcel) {
      readFromParcel(param1Parcel);
    }
    
    public void apply() {
      apply(false);
    }
    
    public void close() {
      this.mResizedSurfaces.clear();
      this.mReparentedSurfaces.clear();
      this.mFreeNativeResources.run();
      this.mNativeObject = 0L;
    }
    
    public void apply(boolean param1Boolean) {
      applyResizedSurfaces();
      notifyReparentedSurfaces();
      SurfaceControl.nativeApplyTransaction(this.mNativeObject, param1Boolean);
    }
    
    private void applyResizedSurfaces() {
      for (int i = this.mResizedSurfaces.size() - 1; i >= 0; ) {
        Point point = this.mResizedSurfaces.valueAt(i);
        null = this.mResizedSurfaces.keyAt(i);
        synchronized (null.mLock) {
          SurfaceControl.access$702(null, point.x);
          SurfaceControl.access$802(null, point.y);
          i--;
        } 
      } 
      this.mResizedSurfaces.clear();
    }
    
    private void notifyReparentedSurfaces() {
      int i = this.mReparentedSurfaces.size();
      while (--i >= 0) {
        SurfaceControl surfaceControl = this.mReparentedSurfaces.keyAt(i);
        synchronized (surfaceControl.mLock) {
          byte b1;
          if (surfaceControl.mReparentListeners != null) {
            b1 = surfaceControl.mReparentListeners.size();
          } else {
            b1 = 0;
          } 
          for (byte b2 = 0; b2 < b1; b2++) {
            SurfaceControl.OnReparentListener onReparentListener = surfaceControl.mReparentListeners.get(b2);
            onReparentListener.onReparent(this, this.mReparentedSurfaces.valueAt(i));
          } 
          this.mReparentedSurfaces.removeAt(i);
          i--;
        } 
      } 
    }
    
    public Transaction setVisibility(SurfaceControl param1SurfaceControl, boolean param1Boolean) {
      checkPreconditions(param1SurfaceControl);
      if (param1Boolean)
        return show(param1SurfaceControl); 
      return hide(param1SurfaceControl);
    }
    
    public Transaction setFrameRateSelectionPriority(SurfaceControl param1SurfaceControl, int param1Int) {
      param1SurfaceControl.checkNotReleased();
      SurfaceControl.nativeSetFrameRateSelectionPriority(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int);
      return this;
    }
    
    public Transaction show(SurfaceControl param1SurfaceControl) {
      if (SurfaceControl.DEBUG_SFC || SurfaceControl.DEBUG_ALL) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SurfaceControl show : sc ");
        stringBuilder.append(param1SurfaceControl);
        Log.v("SurfaceControl", stringBuilder.toString());
      } 
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetFlags(this.mNativeObject, param1SurfaceControl.mNativeObject, 0, 1);
      return this;
    }
    
    public Transaction hide(SurfaceControl param1SurfaceControl) {
      if (SurfaceControl.DEBUG_SFC || SurfaceControl.DEBUG_ALL) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SurfaceControl hide: sc ");
        stringBuilder.append(param1SurfaceControl);
        Log.v("SurfaceControl", stringBuilder.toString());
      } 
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetFlags(this.mNativeObject, param1SurfaceControl.mNativeObject, 1, 1);
      return this;
    }
    
    public Transaction setPosition(SurfaceControl param1SurfaceControl, float param1Float1, float param1Float2) {
      if (SurfaceControl.DEBUG_SFC || SurfaceControl.DEBUG_ALL) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SurfaceControl setPosition:  sc ");
        stringBuilder.append(param1SurfaceControl);
        stringBuilder.append(" x=");
        stringBuilder.append(param1Float1);
        stringBuilder.append(" y=");
        stringBuilder.append(param1Float2);
        stringBuilder.append(" caller=");
        stringBuilder.append(Debug.getCallers(SurfaceControl.DEBUG_DEPTH));
        String str = stringBuilder.toString();
        Log.v("SurfaceControl", str);
      } 
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetPosition(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Float1, param1Float2);
      return this;
    }
    
    public Transaction setCastFlags(SurfaceControl param1SurfaceControl, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetFlags(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int, 64);
      return this;
    }
    
    public Transaction setBufferSize(SurfaceControl param1SurfaceControl, int param1Int1, int param1Int2) {
      checkPreconditions(param1SurfaceControl);
      this.mResizedSurfaces.put(param1SurfaceControl, new Point(param1Int1, param1Int2));
      SurfaceControl.nativeSetSize(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int1, param1Int2);
      return this;
    }
    
    public Transaction setFixedTransformHint(SurfaceControl param1SurfaceControl, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetFixedTransformHint(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int);
      return this;
    }
    
    public Transaction unsetFixedTransformHint(SurfaceControl param1SurfaceControl) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetFixedTransformHint(this.mNativeObject, param1SurfaceControl.mNativeObject, -1);
      return this;
    }
    
    public Transaction setLayer(SurfaceControl param1SurfaceControl, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetLayer(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int);
      return this;
    }
    
    public Transaction setRelativeLayer(SurfaceControl param1SurfaceControl1, SurfaceControl param1SurfaceControl2, int param1Int) {
      checkPreconditions(param1SurfaceControl1);
      SurfaceControl.nativeSetRelativeLayer(this.mNativeObject, param1SurfaceControl1.mNativeObject, param1SurfaceControl2.mNativeObject, param1Int);
      return this;
    }
    
    public Transaction setTransparentRegionHint(SurfaceControl param1SurfaceControl, Region param1Region) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetTransparentRegionHint(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Region);
      return this;
    }
    
    public Transaction setAlpha(SurfaceControl param1SurfaceControl, float param1Float) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetAlpha(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Float);
      return this;
    }
    
    public Transaction setInputWindowInfo(SurfaceControl param1SurfaceControl, InputWindowHandle param1InputWindowHandle) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetInputWindowInfo(this.mNativeObject, param1SurfaceControl.mNativeObject, param1InputWindowHandle);
      return this;
    }
    
    public Transaction syncInputWindows() {
      SurfaceControl.nativeSyncInputWindows(this.mNativeObject);
      return this;
    }
    
    public Transaction setGeometry(SurfaceControl param1SurfaceControl, Rect param1Rect1, Rect param1Rect2, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetGeometry(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Rect1, param1Rect2, param1Int);
      return this;
    }
    
    public Transaction setMatrix(SurfaceControl param1SurfaceControl, float param1Float1, float param1Float2, float param1Float3, float param1Float4) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetMatrix(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Float1, param1Float2, param1Float3, param1Float4);
      return this;
    }
    
    public Transaction setMatrix(SurfaceControl param1SurfaceControl, Matrix param1Matrix, float[] param1ArrayOffloat) {
      param1Matrix.getValues(param1ArrayOffloat);
      setMatrix(param1SurfaceControl, param1ArrayOffloat[0], param1ArrayOffloat[3], param1ArrayOffloat[1], param1ArrayOffloat[4]);
      setPosition(param1SurfaceControl, param1ArrayOffloat[2], param1ArrayOffloat[5]);
      return this;
    }
    
    public Transaction setColorTransform(SurfaceControl param1SurfaceControl, float[] param1ArrayOffloat1, float[] param1ArrayOffloat2) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetColorTransform(this.mNativeObject, param1SurfaceControl.mNativeObject, param1ArrayOffloat1, param1ArrayOffloat2);
      return this;
    }
    
    public Transaction setColorSpaceAgnostic(SurfaceControl param1SurfaceControl, boolean param1Boolean) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetColorSpaceAgnostic(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Boolean);
      return this;
    }
    
    public Transaction setWindowCrop(SurfaceControl param1SurfaceControl, Rect param1Rect) {
      if (SurfaceControl.DEBUG_ALL) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SurfaceControl setWindowCrop:  sc ");
        stringBuilder.append(param1SurfaceControl);
        stringBuilder.append(" crop=");
        stringBuilder.append(param1Rect);
        stringBuilder.append(" caller=");
        stringBuilder.append(Debug.getCallers(SurfaceControl.DEBUG_DEPTH));
        String str = stringBuilder.toString();
        Log.v("SurfaceControl", str);
      } 
      checkPreconditions(param1SurfaceControl);
      if (param1Rect != null) {
        SurfaceControl.nativeSetWindowCrop(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Rect.left, param1Rect.top, param1Rect.right, param1Rect.bottom);
      } else {
        SurfaceControl.nativeSetWindowCrop(this.mNativeObject, param1SurfaceControl.mNativeObject, 0, 0, 0, 0);
      } 
      return this;
    }
    
    public Transaction setWindowCrop(SurfaceControl param1SurfaceControl, int param1Int1, int param1Int2) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetWindowCrop(this.mNativeObject, param1SurfaceControl.mNativeObject, 0, 0, param1Int1, param1Int2);
      return this;
    }
    
    public Transaction setCornerRadius(SurfaceControl param1SurfaceControl, float param1Float) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetCornerRadius(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Float);
      return this;
    }
    
    public Transaction setBackgroundBlurRadius(SurfaceControl param1SurfaceControl, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetBackgroundBlurRadius(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int);
      return this;
    }
    
    public Transaction setBackgroundBlurRadiusForOplus(SurfaceControl param1SurfaceControl, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetBackgroundBlurRadiusForOplus(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int);
      return this;
    }
    
    public Transaction setLayerStack(SurfaceControl param1SurfaceControl, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetLayerStack(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int);
      return this;
    }
    
    public Transaction deferTransactionUntil(SurfaceControl param1SurfaceControl1, SurfaceControl param1SurfaceControl2, long param1Long) {
      if (param1Long < 0L)
        return this; 
      checkPreconditions(param1SurfaceControl1);
      SurfaceControl.nativeDeferTransactionUntil(this.mNativeObject, param1SurfaceControl1.mNativeObject, param1SurfaceControl2.mNativeObject, param1Long);
      return this;
    }
    
    @Deprecated
    public Transaction deferTransactionUntilSurface(SurfaceControl param1SurfaceControl, Surface param1Surface, long param1Long) {
      if (param1Long < 0L)
        return this; 
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeDeferTransactionUntilSurface(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Surface.mNativeObject, param1Long);
      return this;
    }
    
    public Transaction reparentChildren(SurfaceControl param1SurfaceControl1, SurfaceControl param1SurfaceControl2) {
      checkPreconditions(param1SurfaceControl1);
      SurfaceControl.nativeReparentChildren(this.mNativeObject, param1SurfaceControl1.mNativeObject, param1SurfaceControl2.mNativeObject);
      return this;
    }
    
    public Transaction reparent(SurfaceControl param1SurfaceControl1, SurfaceControl param1SurfaceControl2) {
      checkPreconditions(param1SurfaceControl1);
      long l = 0L;
      if (param1SurfaceControl2 != null) {
        param1SurfaceControl2.checkNotReleased();
        l = param1SurfaceControl2.mNativeObject;
      } 
      if (SurfaceControl.DEBUG_SFC || SurfaceControl.DEBUG_ALL) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Transaction reparent sc ");
        stringBuilder.append(param1SurfaceControl1);
        stringBuilder.append(",parent=");
        stringBuilder.append(param1SurfaceControl2);
        stringBuilder.append(" caller=");
        stringBuilder.append(Debug.getCallers(SurfaceControl.DEBUG_DEPTH));
        String str = stringBuilder.toString();
        Log.v("SurfaceControl", str);
      } 
      SurfaceControl.nativeReparent(this.mNativeObject, param1SurfaceControl1.mNativeObject, l);
      this.mReparentedSurfaces.put(param1SurfaceControl1, param1SurfaceControl2);
      return this;
    }
    
    public Transaction detachChildren(SurfaceControl param1SurfaceControl) {
      if (SurfaceControl.DEBUG_SFC || SurfaceControl.DEBUG_ALL) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SurfaceControl detachChildren: sc ");
        stringBuilder.append(param1SurfaceControl);
        stringBuilder.append(" caller=");
        stringBuilder.append(Debug.getCallers(SurfaceControl.DEBUG_DEPTH));
        String str = stringBuilder.toString();
        Log.v("SurfaceControl", str);
      } 
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSeverChildren(this.mNativeObject, param1SurfaceControl.mNativeObject);
      return this;
    }
    
    public Transaction setOverrideScalingMode(SurfaceControl param1SurfaceControl, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetOverrideScalingMode(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int);
      return this;
    }
    
    public Transaction setColor(SurfaceControl param1SurfaceControl, float[] param1ArrayOffloat) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetColor(this.mNativeObject, param1SurfaceControl.mNativeObject, param1ArrayOffloat);
      return this;
    }
    
    public Transaction unsetColor(SurfaceControl param1SurfaceControl) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetColor(this.mNativeObject, param1SurfaceControl.mNativeObject, INVALID_COLOR);
      return this;
    }
    
    public Transaction setSecure(SurfaceControl param1SurfaceControl, boolean param1Boolean) {
      checkPreconditions(param1SurfaceControl);
      if (param1Boolean) {
        SurfaceControl.nativeSetFlags(this.mNativeObject, param1SurfaceControl.mNativeObject, 128, 128);
      } else {
        SurfaceControl.nativeSetFlags(this.mNativeObject, param1SurfaceControl.mNativeObject, 0, 128);
      } 
      return this;
    }
    
    public Transaction setOpaque(SurfaceControl param1SurfaceControl, boolean param1Boolean) {
      checkPreconditions(param1SurfaceControl);
      if (param1Boolean) {
        SurfaceControl.nativeSetFlags(this.mNativeObject, param1SurfaceControl.mNativeObject, 2, 2);
      } else {
        SurfaceControl.nativeSetFlags(this.mNativeObject, param1SurfaceControl.mNativeObject, 0, 2);
      } 
      return this;
    }
    
    public Transaction setDisplaySurface(IBinder param1IBinder, Surface param1Surface) {
      if (param1IBinder != null) {
        if (param1Surface != null) {
          synchronized (param1Surface.mLock) {
            SurfaceControl.nativeSetDisplaySurface(this.mNativeObject, param1IBinder, param1Surface.mNativeObject);
          } 
        } else {
          SurfaceControl.nativeSetDisplaySurface(this.mNativeObject, param1IBinder, 0L);
        } 
        return this;
      } 
      throw new IllegalArgumentException("displayToken must not be null");
    }
    
    public Transaction setDisplayLayerStack(IBinder param1IBinder, int param1Int) {
      if (param1IBinder != null) {
        SurfaceControl.nativeSetDisplayLayerStack(this.mNativeObject, param1IBinder, param1Int);
        return this;
      } 
      throw new IllegalArgumentException("displayToken must not be null");
    }
    
    public Transaction setDisplayProjection(IBinder param1IBinder, int param1Int, Rect param1Rect1, Rect param1Rect2) {
      if (param1IBinder != null) {
        if (param1Rect1 != null) {
          if (param1Rect2 != null) {
            SurfaceControl.nativeSetDisplayProjection(this.mNativeObject, param1IBinder, param1Int, param1Rect1.left, param1Rect1.top, param1Rect1.right, param1Rect1.bottom, param1Rect2.left, param1Rect2.top, param1Rect2.right, param1Rect2.bottom);
            return this;
          } 
          throw new IllegalArgumentException("displayRect must not be null");
        } 
        throw new IllegalArgumentException("layerStackRect must not be null");
      } 
      throw new IllegalArgumentException("displayToken must not be null");
    }
    
    public Transaction setDisplaySize(IBinder param1IBinder, int param1Int1, int param1Int2) {
      if (param1IBinder != null) {
        if (param1Int1 > 0 && param1Int2 > 0) {
          SurfaceControl.nativeSetDisplaySize(this.mNativeObject, param1IBinder, param1Int1, param1Int2);
          return this;
        } 
        throw new IllegalArgumentException("width and height must be positive");
      } 
      throw new IllegalArgumentException("displayToken must not be null");
    }
    
    public Transaction setAnimationTransaction() {
      SurfaceControl.nativeSetAnimationTransaction(this.mNativeObject);
      return this;
    }
    
    @Deprecated
    public Transaction setEarlyWakeup() {
      SurfaceControl.nativeSetEarlyWakeup(this.mNativeObject);
      return this;
    }
    
    public Transaction setEarlyWakeupStart() {
      SurfaceControl.nativeSetEarlyWakeupStart(this.mNativeObject);
      return this;
    }
    
    public Transaction setEarlyWakeupEnd() {
      SurfaceControl.nativeSetEarlyWakeupEnd(this.mNativeObject);
      return this;
    }
    
    public Transaction setMetadata(SurfaceControl param1SurfaceControl, int param1Int1, int param1Int2) {
      Parcel parcel = Parcel.obtain();
      parcel.writeInt(param1Int2);
      try {
        setMetadata(param1SurfaceControl, param1Int1, parcel);
        return this;
      } finally {
        parcel.recycle();
      } 
    }
    
    public Transaction setMetadata(SurfaceControl param1SurfaceControl, int param1Int, Parcel param1Parcel) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetMetadata(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Int, param1Parcel);
      return this;
    }
    
    public Transaction setShadowRadius(SurfaceControl param1SurfaceControl, float param1Float) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetShadowRadius(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Float);
      return this;
    }
    
    public Transaction setFrameRate(SurfaceControl param1SurfaceControl, float param1Float, int param1Int) {
      checkPreconditions(param1SurfaceControl);
      SurfaceControl.nativeSetFrameRate(this.mNativeObject, param1SurfaceControl.mNativeObject, param1Float, param1Int);
      return this;
    }
    
    public Transaction merge(Transaction param1Transaction) {
      if (this == param1Transaction)
        return this; 
      this.mResizedSurfaces.putAll(param1Transaction.mResizedSurfaces);
      param1Transaction.mResizedSurfaces.clear();
      this.mReparentedSurfaces.putAll(param1Transaction.mReparentedSurfaces);
      param1Transaction.mReparentedSurfaces.clear();
      SurfaceControl.nativeMergeTransaction(this.mNativeObject, param1Transaction.mNativeObject);
      return this;
    }
    
    public Transaction remove(SurfaceControl param1SurfaceControl) {
      if (SurfaceControl.DEBUG_SFC || SurfaceControl.DEBUG_ALL) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SurfaceControl remove in transaction: sc ");
        stringBuilder.append(param1SurfaceControl);
        stringBuilder.append(" caller=");
        stringBuilder.append(Debug.getCallers(SurfaceControl.DEBUG_DEPTH));
        String str = stringBuilder.toString();
        Log.v("SurfaceControl", str);
      } 
      reparent(param1SurfaceControl, null);
      param1SurfaceControl.release();
      return this;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      if (this.mNativeObject == 0L) {
        param1Parcel.writeInt(0);
      } else {
        param1Parcel.writeInt(1);
      } 
      SurfaceControl.nativeWriteTransactionToParcel(this.mNativeObject, param1Parcel);
    }
    
    private void readFromParcel(Parcel param1Parcel) {
      this.mNativeObject = 0L;
      if (param1Parcel.readInt() != 0) {
        long l = SurfaceControl.nativeReadTransactionFromParcel(param1Parcel);
        this.mFreeNativeResources = sRegistry.registerNativeAllocation(this, l);
      } 
    }
    
    public int describeContents() {
      return 0;
    }
  }
  
  class null implements Parcelable.Creator<Transaction> {
    public SurfaceControl.Transaction createFromParcel(Parcel param1Parcel) {
      return new SurfaceControl.Transaction(param1Parcel);
    }
    
    public SurfaceControl.Transaction[] newArray(int param1Int) {
      return new SurfaceControl.Transaction[param1Int];
    }
  }
  
  class LockDebuggingTransaction extends Transaction {
    Object mMonitor;
    
    public LockDebuggingTransaction(SurfaceControl this$0) {
      this.mMonitor = this$0;
    }
    
    protected void checkPreconditions(SurfaceControl param1SurfaceControl) {
      super.checkPreconditions(param1SurfaceControl);
      if (Thread.holdsLock(this.mMonitor))
        return; 
      throw new RuntimeException("Unlocked access to synchronized SurfaceControl.Transaction");
    }
  }
  
  public static long acquireFrameRateFlexibilityToken() {
    return nativeAcquireFrameRateFlexibilityToken();
  }
  
  public static void releaseFrameRateFlexibilityToken(long paramLong) {
    nativeReleaseFrameRateFlexibilityToken(paramLong);
  }
  
  public SurfaceControl() {}
  
  private static native long nativeAcquireFrameRateFlexibilityToken();
  
  private static native void nativeApplyTransaction(long paramLong, boolean paramBoolean);
  
  private static native ScreenshotGraphicBuffer nativeCaptureLayers(IBinder paramIBinder, long paramLong, Rect paramRect, float paramFloat, long[] paramArrayOflong, int paramInt);
  
  private static native boolean nativeClearAnimationFrameStats();
  
  private static native boolean nativeClearContentFrameStats(long paramLong);
  
  private static native long nativeCopyFromSurfaceControl(long paramLong);
  
  private static native long nativeCreate(SurfaceSession paramSurfaceSession, String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong, Parcel paramParcel) throws Surface.OutOfResourcesException;
  
  private static native IBinder nativeCreateDisplay(String paramString, boolean paramBoolean);
  
  private static native long nativeCreateTransaction();
  
  private static native void nativeDeferTransactionUntil(long paramLong1, long paramLong2, long paramLong3, long paramLong4);
  
  private static native void nativeDeferTransactionUntilSurface(long paramLong1, long paramLong2, long paramLong3, long paramLong4);
  
  private static native void nativeDestroyDisplay(IBinder paramIBinder);
  
  private static native void nativeDisconnect(long paramLong);
  
  private static native int nativeGetActiveColorMode(IBinder paramIBinder);
  
  private static native int nativeGetActiveConfig(IBinder paramIBinder);
  
  private static native boolean nativeGetAnimationFrameStats(WindowAnimationFrameStats paramWindowAnimationFrameStats);
  
  private static native boolean nativeGetAutoLowLatencyModeSupport(IBinder paramIBinder);
  
  private static native int[] nativeGetCompositionDataspaces();
  
  private static native boolean nativeGetContentFrameStats(long paramLong, WindowContentFrameStats paramWindowContentFrameStats);
  
  private static native DesiredDisplayConfigSpecs nativeGetDesiredDisplayConfigSpecs(IBinder paramIBinder);
  
  private static native boolean nativeGetDisplayBrightnessSupport(IBinder paramIBinder);
  
  private static native int[] nativeGetDisplayColorModes(IBinder paramIBinder);
  
  private static native DisplayConfig[] nativeGetDisplayConfigs(IBinder paramIBinder);
  
  private static native DisplayInfo nativeGetDisplayInfo(IBinder paramIBinder);
  
  private static native DisplayPrimaries nativeGetDisplayNativePrimaries(IBinder paramIBinder);
  
  private static native DisplayedContentSample nativeGetDisplayedContentSample(IBinder paramIBinder, long paramLong1, long paramLong2);
  
  private static native DisplayedContentSamplingAttributes nativeGetDisplayedContentSamplingAttributes(IBinder paramIBinder);
  
  private static native boolean nativeGetGameContentTypeSupport(IBinder paramIBinder);
  
  private static native long nativeGetHandle(long paramLong);
  
  private static native Display.HdrCapabilities nativeGetHdrCapabilities(IBinder paramIBinder);
  
  private static native long nativeGetNativeTransactionFinalizer();
  
  private static native long[] nativeGetPhysicalDisplayIds();
  
  private static native IBinder nativeGetPhysicalDisplayToken(long paramLong);
  
  private static native boolean nativeGetProtectedContentSupport();
  
  private static native void nativeMergeTransaction(long paramLong1, long paramLong2);
  
  private static native long nativeMirrorSurface(long paramLong);
  
  private static native long nativeReadFromParcel(Parcel paramParcel);
  
  private static native long nativeReadTransactionFromParcel(Parcel paramParcel);
  
  private static native void nativeRelease(long paramLong);
  
  private static native void nativeReleaseFrameRateFlexibilityToken(long paramLong);
  
  private static native void nativeReparent(long paramLong1, long paramLong2, long paramLong3);
  
  private static native void nativeReparentChildren(long paramLong1, long paramLong2, long paramLong3);
  
  private static native ScreenshotGraphicBuffer nativeScreenshot(IBinder paramIBinder, Rect paramRect, int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2);
  
  private static native boolean nativeSetActiveColorMode(IBinder paramIBinder, int paramInt);
  
  private static native void nativeSetAlpha(long paramLong1, long paramLong2, float paramFloat);
  
  private static native void nativeSetAnimationTransaction(long paramLong);
  
  private static native void nativeSetAutoLowLatencyMode(IBinder paramIBinder, boolean paramBoolean);
  
  private static native void nativeSetBackgroundBlurRadius(long paramLong1, long paramLong2, int paramInt);
  
  private static native void nativeSetBackgroundBlurRadiusForOplus(long paramLong1, long paramLong2, int paramInt);
  
  private static native void nativeSetColor(long paramLong1, long paramLong2, float[] paramArrayOffloat);
  
  private static native void nativeSetColorSpaceAgnostic(long paramLong1, long paramLong2, boolean paramBoolean);
  
  private static native void nativeSetColorTransform(long paramLong1, long paramLong2, float[] paramArrayOffloat1, float[] paramArrayOffloat2);
  
  private static native void nativeSetCornerRadius(long paramLong1, long paramLong2, float paramFloat);
  
  private static native boolean nativeSetDesiredDisplayConfigSpecs(IBinder paramIBinder, DesiredDisplayConfigSpecs paramDesiredDisplayConfigSpecs);
  
  private static native boolean nativeSetDisplayBrightness(IBinder paramIBinder, float paramFloat);
  
  private static native void nativeSetDisplayLayerStack(long paramLong, IBinder paramIBinder, int paramInt);
  
  private static native void nativeSetDisplayPowerMode(IBinder paramIBinder, int paramInt);
  
  private static native void nativeSetDisplayProjection(long paramLong, IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9);
  
  private static native void nativeSetDisplaySize(long paramLong, IBinder paramIBinder, int paramInt1, int paramInt2);
  
  private static native void nativeSetDisplaySurface(long paramLong1, IBinder paramIBinder, long paramLong2);
  
  private static native boolean nativeSetDisplayedContentSamplingEnabled(IBinder paramIBinder, boolean paramBoolean, int paramInt1, int paramInt2);
  
  private static native void nativeSetEarlyWakeup(long paramLong);
  
  private static native void nativeSetEarlyWakeupEnd(long paramLong);
  
  private static native void nativeSetEarlyWakeupStart(long paramLong);
  
  private static native void nativeSetFixedTransformHint(long paramLong1, long paramLong2, int paramInt);
  
  private static native void nativeSetFlags(long paramLong1, long paramLong2, int paramInt1, int paramInt2);
  
  private static native void nativeSetFrameRate(long paramLong1, long paramLong2, float paramFloat, int paramInt);
  
  private static native void nativeSetFrameRateSelectionPriority(long paramLong1, long paramLong2, int paramInt);
  
  private static native void nativeSetGameContentType(IBinder paramIBinder, boolean paramBoolean);
  
  private static native void nativeSetGeometry(long paramLong1, long paramLong2, Rect paramRect1, Rect paramRect2, long paramLong3);
  
  private static native void nativeSetGlobalShadowSettings(float[] paramArrayOffloat1, float[] paramArrayOffloat2, float paramFloat1, float paramFloat2, float paramFloat3);
  
  private static native void nativeSetInputWindowInfo(long paramLong1, long paramLong2, InputWindowHandle paramInputWindowHandle);
  
  private static native void nativeSetLayer(long paramLong1, long paramLong2, int paramInt);
  
  private static native void nativeSetLayerStack(long paramLong1, long paramLong2, int paramInt);
  
  private static native void nativeSetMatrix(long paramLong1, long paramLong2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);
  
  private static native void nativeSetMetadata(long paramLong1, long paramLong2, int paramInt, Parcel paramParcel);
  
  private static native void nativeSetOverrideScalingMode(long paramLong1, long paramLong2, int paramInt);
  
  private static native void nativeSetPosition(long paramLong1, long paramLong2, float paramFloat1, float paramFloat2);
  
  private static native void nativeSetRelativeLayer(long paramLong1, long paramLong2, long paramLong3, int paramInt);
  
  private static native void nativeSetShadowRadius(long paramLong1, long paramLong2, float paramFloat);
  
  private static native void nativeSetSize(long paramLong1, long paramLong2, int paramInt1, int paramInt2);
  
  private static native void nativeSetTransparentRegionHint(long paramLong1, long paramLong2, Region paramRegion);
  
  private static native void nativeSetWindowCrop(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  private static native void nativeSeverChildren(long paramLong1, long paramLong2);
  
  private static native void nativeSyncInputWindows(long paramLong);
  
  private static native void nativeWriteToParcel(long paramLong, Parcel paramParcel);
  
  private static native void nativeWriteTransactionToParcel(long paramLong, Parcel paramParcel);
  
  class OnReparentListener {
    public abstract void onReparent(SurfaceControl.Transaction param1Transaction, SurfaceControl param1SurfaceControl);
  }
}
