package android.view;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;
import com.oplus.screenshot.OplusLongshotComponentName;
import com.oplus.screenshot.OplusLongshotDump;
import com.oplus.screenshot.OplusLongshotUtils;
import com.oplus.screenshot.OplusLongshotViewBase;
import com.oplus.screenshot.OplusLongshotViewInfo;
import com.oplus.screenshot.OplusScreenshotManager;
import com.oplus.util.OplusLog;
import com.oplus.util.OplusTypeCastingHelper;
import com.oplus.view.analysis.OplusWindowNode;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OplusLongshotViewDump {
  private static final boolean DBG = OplusLongshotDump.DBG;
  
  static {
    CONTENT_COMPARATOR = new ContentComparator();
  }
  
  private final OplusLongshotViewInfo mViewInfo = new OplusLongshotViewInfo();
  
  private final Rect mTempRect1 = new Rect();
  
  private final Rect mTempRect2 = new Rect();
  
  private final List<ViewNode> mScrollNodes = new ArrayList<>();
  
  private final List<View> mSmallViews = new ArrayList<>();
  
  private final List<Rect> mFloatRects = new ArrayList<>();
  
  private final List<Rect> mSideRects = new ArrayList<>();
  
  private ViewNode mScrollNode = null;
  
  private int mDumpCount = 0;
  
  private int mMinListHeight = 0;
  
  private int mCoverHeight = 0;
  
  private int mScreenHeight = 0;
  
  private int mScreenWidght = 0;
  
  private int mMinScrollHeight = 0;
  
  private int mMinScrollDistance = 0;
  
  private static final ContentComparator CONTENT_COMPARATOR;
  
  private static final boolean FEATURE_ADJUST_WINDOW = false;
  
  private static final boolean FEATURE_DUMP_MIN_SIZE = false;
  
  private static final String JSON_CHILD_HASH = "child_hash";
  
  private static final String JSON_CHILD_LIST = "child_list";
  
  private static final String JSON_CHILD_RECT_CLIP = "child_rect_clip";
  
  private static final String JSON_CHILD_RECT_FULL = "child_rect_full";
  
  private static final String JSON_CHILD_SCROLLY = "child_scrollY";
  
  private static final String JSON_FLOAT_LIST = "float_list";
  
  private static final String JSON_FLOAT_RECT = "float_rect";
  
  private static final String JSON_PARENT_HASH = "parent_hash";
  
  private static final String JSON_PARENT_RECT_CLIP = "parent_rect_clip";
  
  private static final String JSON_PARENT_RECT_FULL = "parent_rect_full";
  
  private static final String JSON_SCROLL_CHILD = "scroll_child";
  
  private static final String JSON_SCROLL_LIST = "scroll_list";
  
  private static final String JSON_SCROLL_RECT = "scroll_rect";
  
  private static final String JSON_SIDE_LIST = "side_list";
  
  private static final String JSON_SIDE_RECT = "side_rect";
  
  private static final String JSON_VIEW_UNSUPPORTED = "view_unsupported";
  
  private static final String JSON_WINDOW_LAYER = "window_layer";
  
  private static final String JSON_WINDOW_LIST = "window_list";
  
  private static final String JSON_WINDOW_NAVIBAR = "window_navibar";
  
  private static final String JSON_WINDOW_RECT_DECOR = "window_rect_decor";
  
  private static final String JSON_WINDOW_RECT_VISIBLE = "window_rect_visible";
  
  private static final String JSON_WINDOW_STATBAR = "window_statbar";
  
  private static final String TAG = "LongshotDump";
  
  public OplusWindowNode collectWindow(View paramView, boolean paramBoolean1, boolean paramBoolean2) {
    return new OplusWindowNode(paramView, paramBoolean1, paramBoolean2);
  }
  
  public void dumpViewRoot(ViewRootImpl paramViewRootImpl, ParcelFileDescriptor paramParcelFileDescriptor, List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2, boolean paramBoolean) {
    // Byte code:
    //   0: invokestatic allowThreadDiskWrites : ()Landroid/os/StrictMode$ThreadPolicy;
    //   3: astore #6
    //   5: iload #5
    //   7: ifeq -> 18
    //   10: ldc_w 'dumpLongshot'
    //   13: astore #7
    //   15: goto -> 23
    //   18: ldc_w 'dumpScreenshot'
    //   21: astore #7
    //   23: getstatic android/view/OplusLongshotViewDump.DBG : Z
    //   26: istore #8
    //   28: new java/lang/StringBuilder
    //   31: astore #9
    //   33: aload #9
    //   35: invokespecial <init> : ()V
    //   38: aload #9
    //   40: aload #7
    //   42: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload #9
    //   48: ldc_w ' : viewAncestor.mView='
    //   51: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: aload #9
    //   57: aload_1
    //   58: getfield mView : Landroid/view/View;
    //   61: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: iload #8
    //   67: ldc 'LongshotDump'
    //   69: aload #9
    //   71: invokevirtual toString : ()Ljava/lang/String;
    //   74: invokestatic d : (ZLjava/lang/String;Ljava/lang/String;)V
    //   77: new com/android/internal/util/FastPrintWriter
    //   80: astore #9
    //   82: new java/io/FileOutputStream
    //   85: astore #10
    //   87: aload #10
    //   89: aload_2
    //   90: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   93: invokespecial <init> : (Ljava/io/FileDescriptor;)V
    //   96: aload #9
    //   98: aload #10
    //   100: invokespecial <init> : (Ljava/io/OutputStream;)V
    //   103: aload_1
    //   104: getfield mContext : Landroid/content/Context;
    //   107: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   110: astore #10
    //   112: aload_0
    //   113: aload #10
    //   115: ldc_w 201654492
    //   118: invokevirtual getDimensionPixelOffset : (I)I
    //   121: putfield mMinScrollHeight : I
    //   124: aload_0
    //   125: aload #10
    //   127: ldc_w 201654491
    //   130: invokevirtual getDimensionPixelOffset : (I)I
    //   133: putfield mMinScrollDistance : I
    //   136: aload_0
    //   137: aload #10
    //   139: ldc_w 201654490
    //   142: invokevirtual getDimensionPixelOffset : (I)I
    //   145: putfield mMinListHeight : I
    //   148: aload_0
    //   149: aload #10
    //   151: ldc_w 201654489
    //   154: invokevirtual getDimensionPixelOffset : (I)I
    //   157: putfield mCoverHeight : I
    //   160: aload_0
    //   161: aload #10
    //   163: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   166: getfield heightPixels : I
    //   169: putfield mScreenHeight : I
    //   172: aload_0
    //   173: aload #10
    //   175: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   178: getfield widthPixels : I
    //   181: putfield mScreenWidght : I
    //   184: iload #5
    //   186: ifeq -> 205
    //   189: aload_0
    //   190: aload_1
    //   191: aload #9
    //   193: aload #7
    //   195: aconst_null
    //   196: aload_3
    //   197: aload #4
    //   199: invokespecial dumpLongshot : (Landroid/view/ViewRootImpl;Ljava/io/PrintWriter;Ljava/lang/String;Lcom/oplus/screenshot/OplusLongshotDump;Ljava/util/List;Ljava/util/List;)V
    //   202: goto -> 214
    //   205: aload_0
    //   206: aload_1
    //   207: aload #9
    //   209: aload #7
    //   211: invokespecial dumpScreenshot : (Landroid/view/ViewRootImpl;Ljava/io/PrintWriter;Ljava/lang/String;)V
    //   214: aload_0
    //   215: aconst_null
    //   216: aload #9
    //   218: aload_3
    //   219: aload #4
    //   221: invokespecial packJsonNode : (Lcom/oplus/screenshot/OplusLongshotDump;Ljava/io/PrintWriter;Ljava/util/List;Ljava/util/List;)Ljava/lang/String;
    //   224: astore_3
    //   225: aload_3
    //   226: ifnull -> 235
    //   229: aload #9
    //   231: aload_3
    //   232: invokevirtual println : (Ljava/lang/String;)V
    //   235: aload #9
    //   237: invokevirtual flush : ()V
    //   240: goto -> 306
    //   243: astore_3
    //   244: goto -> 252
    //   247: astore_3
    //   248: goto -> 330
    //   251: astore_3
    //   252: getstatic android/view/OplusLongshotViewDump.DBG : Z
    //   255: istore #5
    //   257: new java/lang/StringBuilder
    //   260: astore #4
    //   262: aload #4
    //   264: invokespecial <init> : ()V
    //   267: aload #4
    //   269: aload #7
    //   271: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   274: pop
    //   275: aload #4
    //   277: ldc_w ' ERROR : '
    //   280: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   283: pop
    //   284: aload #4
    //   286: aload_3
    //   287: invokestatic getStackTraceString : (Ljava/lang/Throwable;)Ljava/lang/String;
    //   290: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   293: pop
    //   294: iload #5
    //   296: ldc 'LongshotDump'
    //   298: aload #4
    //   300: invokevirtual toString : ()Ljava/lang/String;
    //   303: invokestatic e : (ZLjava/lang/String;Ljava/lang/String;)V
    //   306: aload_0
    //   307: aload_1
    //   308: getfield mContext : Landroid/content/Context;
    //   311: aconst_null
    //   312: invokespecial reportDumpResult : (Landroid/content/Context;Lcom/oplus/screenshot/OplusLongshotDump;)V
    //   315: aload_0
    //   316: invokespecial clearList : ()V
    //   319: aload_2
    //   320: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   323: aload #6
    //   325: invokestatic setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   328: return
    //   329: astore_3
    //   330: aload_0
    //   331: aload_1
    //   332: getfield mContext : Landroid/content/Context;
    //   335: aconst_null
    //   336: invokespecial reportDumpResult : (Landroid/content/Context;Lcom/oplus/screenshot/OplusLongshotDump;)V
    //   339: aload_0
    //   340: invokespecial clearList : ()V
    //   343: aload_2
    //   344: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   347: aload #6
    //   349: invokestatic setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   352: aload_3
    //   353: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #134	-> 0
    //   #135	-> 5
    //   #136	-> 5
    //   #138	-> 23
    //   #139	-> 77
    //   #140	-> 103
    //   #141	-> 112
    //   #142	-> 124
    //   #143	-> 136
    //   #144	-> 148
    //   #145	-> 160
    //   #146	-> 172
    //   #147	-> 184
    //   #150	-> 189
    //   #152	-> 205
    //   #154	-> 214
    //   #155	-> 225
    //   #156	-> 229
    //   #158	-> 235
    //   #159	-> 243
    //   #162	-> 247
    //   #159	-> 251
    //   #160	-> 252
    //   #162	-> 306
    //   #163	-> 315
    //   #164	-> 319
    //   #165	-> 323
    //   #166	-> 328
    //   #167	-> 328
    //   #162	-> 329
    //   #163	-> 339
    //   #164	-> 343
    //   #165	-> 347
    //   #166	-> 352
    // Exception table:
    //   from	to	target	type
    //   23	77	251	java/lang/Exception
    //   23	77	247	finally
    //   77	103	251	java/lang/Exception
    //   77	103	247	finally
    //   103	112	251	java/lang/Exception
    //   103	112	247	finally
    //   112	124	251	java/lang/Exception
    //   112	124	247	finally
    //   124	136	251	java/lang/Exception
    //   124	136	247	finally
    //   136	148	251	java/lang/Exception
    //   136	148	247	finally
    //   148	160	251	java/lang/Exception
    //   148	160	247	finally
    //   160	172	251	java/lang/Exception
    //   160	172	247	finally
    //   172	184	251	java/lang/Exception
    //   172	184	247	finally
    //   189	202	251	java/lang/Exception
    //   189	202	247	finally
    //   205	214	251	java/lang/Exception
    //   205	214	247	finally
    //   214	225	243	java/lang/Exception
    //   214	225	329	finally
    //   229	235	243	java/lang/Exception
    //   229	235	329	finally
    //   235	240	243	java/lang/Exception
    //   235	240	329	finally
    //   252	306	329	finally
  }
  
  public void injectInputBegin() {
    ViewNode viewNode = this.mScrollNode;
    if (viewNode != null)
      viewNode.disableOverScroll(); 
  }
  
  public void injectInputEnd() {
    ViewNode viewNode = this.mScrollNode;
    if (viewNode != null)
      viewNode.resetOverScroll(); 
  }
  
  public void reset() {
    OplusLog.d(DBG, "LongshotDump", "reset ViewDump");
    injectInputEnd();
    this.mScrollNode = null;
    clearList();
  }
  
  private long getTimeSpend(long paramLong) {
    return SystemClock.uptimeMillis() - paramLong;
  }
  
  private void printMsg(String paramString) {
    OplusLog.d(DBG, "LongshotDump", paramString);
  }
  
  private void printSpend(String paramString, long paramLong) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" : spend=");
    stringBuilder.append(paramLong);
    paramString = stringBuilder.toString();
    printMsg(paramString);
  }
  
  private void printTag(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("========== ");
    stringBuilder.append(paramString);
    printMsg(stringBuilder.toString());
  }
  
  private void printScrollNodes(String paramString, List<ViewNode> paramList) {
    if (!DBG)
      return; 
    for (ViewNode viewNode : paramList) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(viewNode);
      printMsg(stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("    ");
      printScrollNodes(stringBuilder.toString(), viewNode.getChildList());
    } 
  }
  
  private boolean canScrollVertically(View paramView) {
    if (paramView.canScrollVertically(1))
      return true; 
    if (paramView.canScrollVertically(-1))
      return true; 
    return false;
  }
  
  private boolean isScrollableView(View paramView) {
    CharSequence charSequence = OplusViewCompat.getAccessibilityClassName(paramView);
    if (charSequence != null) {
      if (charSequence.equals("android.widget.ListView"))
        return true; 
      if (charSequence.equals("android.widget.ScrollView"))
        return true; 
    } 
    return false;
  }
  
  private boolean isScrollable(View paramView) {
    if (canScrollVertically(paramView))
      return true; 
    if (isScrollableView(paramView))
      return true; 
    return false;
  }
  
  private boolean isInvalidScrollDistance(View paramView, int paramInt) {
    boolean bool;
    int i = paramView.computeVerticalScrollExtent();
    int j = paramView.computeVerticalScrollRange();
    if ((j - i) * paramInt < this.mMinScrollDistance * i) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isValidScrollNode(View paramView, boolean paramBoolean) {
    StringBuilder stringBuilder;
    int i = paramView.getWidth();
    int j = this.mScreenWidght;
    if (j > 0 && i < j / 2) {
      paramBoolean = DBG;
      stringBuilder = new StringBuilder();
      stringBuilder.append("    ! isValidScrollNode 1 : mScreenWidght=");
      stringBuilder.append(this.mScreenWidght);
      stringBuilder.append(", width=");
      stringBuilder.append(i);
      OplusLog.d(paramBoolean, "LongshotDump", stringBuilder.toString());
      return false;
    } 
    i = stringBuilder.getHeight();
    if (stringBuilder instanceof android.widget.ScrollView) {
      if (i < this.mMinScrollHeight) {
        if (paramBoolean) {
          boolean bool = DBG;
          stringBuilder = new StringBuilder();
          stringBuilder.append("    ! isValidScrollNode 2 : height=");
          stringBuilder.append(i);
          stringBuilder.append(", mMinScrollHeight=");
          stringBuilder.append(this.mMinScrollHeight);
          stringBuilder.append(", isChildScrollNode=");
          stringBuilder.append(paramBoolean);
          OplusLog.d(bool, "LongshotDump", stringBuilder.toString());
          return false;
        } 
        if (isInvalidScrollDistance((View)stringBuilder, i)) {
          paramBoolean = DBG;
          stringBuilder = new StringBuilder();
          stringBuilder.append("    ! isValidScrollNode 3 : height=");
          stringBuilder.append(i);
          stringBuilder.append(", mMinScrollHeight=");
          stringBuilder.append(this.mMinScrollHeight);
          stringBuilder.append(", mMinScrollDistance=");
          stringBuilder.append(this.mMinScrollDistance);
          OplusLog.d(paramBoolean, "LongshotDump", stringBuilder.toString());
          return false;
        } 
      } 
    } else if (i < this.mMinListHeight) {
      Rect rect = new Rect();
      stringBuilder.getBoundsOnScreen(rect, true);
      int k = rect.bottom;
      j = this.mScreenHeight;
      if (k < j / 2) {
        paramBoolean = DBG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("    ! isValidScrollNode 4 : mScreenHeight=");
        stringBuilder.append(this.mScreenHeight);
        stringBuilder.append(", mMinListHeight=");
        stringBuilder.append(this.mMinListHeight);
        stringBuilder.append(", rect=");
        stringBuilder.append(rect);
        stringBuilder.append(", height=");
        stringBuilder.append(i);
        OplusLog.d(paramBoolean, "LongshotDump", stringBuilder.toString());
        return false;
      } 
      if (j - rect.bottom > this.mMinListHeight) {
        paramBoolean = DBG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("    ! isValidScrollNode 5 : mScreenHeight=");
        stringBuilder.append(this.mScreenHeight);
        stringBuilder.append(", mMinListHeight=");
        stringBuilder.append(this.mMinListHeight);
        stringBuilder.append(", rect=");
        stringBuilder.append(rect);
        stringBuilder.append(", height=");
        stringBuilder.append(i);
        OplusLog.d(paramBoolean, "LongshotDump", stringBuilder.toString());
        return false;
      } 
    } 
    return true;
  }
  
  private boolean isGalleryRoot(View paramView) {
    String str = paramView.getContext().getPackageName();
    if (!OplusLongshotUtils.isGallery(str))
      return false; 
    if (!(paramView instanceof android.opengl.GLSurfaceView))
      return false; 
    return true;
  }
  
  private void appendLongshotInfo(View paramView, OplusLongshotViewInfo paramOplusLongshotViewInfo) {}
  
  private void dumpViewNodes(View paramView, OplusLongshotViewInfo paramOplusLongshotViewInfo) {
    ArrayList<View> arrayList;
    if (paramView == null)
      return; 
    if (!paramView.isVisibleToUser())
      return; 
    appendLongshotInfo(paramView, paramOplusLongshotViewInfo);
    OplusBaseView oplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, paramView);
    if (paramView instanceof ViewGroup) {
      boolean bool;
      oplusBaseView.findViewsLongshotInfo(paramOplusLongshotViewInfo);
      ViewGroup viewGroup = (ViewGroup)paramView;
      int i = viewGroup.getChildCount();
      arrayList = viewGroup.buildOrderedChildList();
      byte b1 = 0;
      if (arrayList == null) {
        bool = true;
      } else {
        bool = false;
      } 
      byte b2 = b1;
      if (bool) {
        b2 = b1;
        if (viewGroup.isChildrenDrawingOrderEnabled())
          b2 = 1; 
      } 
      for (b1 = 0; b1 < i; b1++) {
        byte b;
        if (b2 != 0) {
          b = viewGroup.getChildDrawingOrder(i, b1);
        } else {
          b = b1;
        } 
        if (bool) {
          paramView = viewGroup.getChildAt(b);
        } else {
          paramView = arrayList.get(b);
        } 
        dumpViewNodes(paramView, paramOplusLongshotViewInfo);
      } 
      if (arrayList != null)
        arrayList.clear(); 
    } else {
      arrayList.findViewsLongshotInfo(paramOplusLongshotViewInfo);
    } 
  }
  
  private void dumpScrollNodes(ViewNode paramViewNode, View paramView, Point paramPoint, List<View> paramList, int paramInt, OplusLongshotViewInfo paramOplusLongshotViewInfo) {
    ViewNode viewNode;
    if (paramView == null)
      return; 
    if (!paramView.isVisibleToUser())
      return; 
    OplusBaseView oplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, paramView);
    if (paramView instanceof ViewGroup) {
      int i;
      boolean bool1, bool2;
      oplusBaseView.findViewsLongshotInfo(paramOplusLongshotViewInfo);
      paramView.getBoundsOnScreen(this.mTempRect1, true);
      if (paramPoint != null) {
        if (this.mTempRect1.width() < paramPoint.x)
          return; 
        if (this.mTempRect1.height() < paramPoint.y) {
          if (paramList != null)
            paramList.add(paramView); 
          return;
        } 
      } 
      this.mDumpCount++;
      boolean bool = isScrollable(paramView);
      if (bool) {
        long l = SystemClock.uptimeMillis();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("    ");
        AccessibilityNodeInfo accessibilityNodeInfo = paramView.createAccessibilityNodeInfo();
        if (accessibilityNodeInfo != null && accessibilityNodeInfo.isScrollable()) {
          boolean bool3;
          if (paramViewNode != null) {
            bool3 = true;
          } else {
            bool3 = false;
          } 
          if (isValidScrollNode(paramView, bool3)) {
            paramView.getBoundsOnScreen(this.mTempRect2, false);
            ViewNode viewNode1 = new ViewNode(paramView, accessibilityNodeInfo.getClassName(), this.mTempRect1, this.mTempRect2);
            if (bool3) {
              paramViewNode.addChild(viewNode1);
              stringBuilder.append("isChildScrollNode : ");
            } else {
              this.mScrollNodes.add(viewNode1);
              stringBuilder.append("isScrollNode : ");
            } 
            paramViewNode = viewNode1;
            l = getTimeSpend(l);
            viewNode1.setSpend(l);
            stringBuilder.append(viewNode1);
            paramInt++;
          } else {
            stringBuilder.append("----rmScrollNode : ");
          } 
        } else {
          l = getTimeSpend(l);
          stringBuilder.append("---noScrollNode : ");
          stringBuilder.append(paramView);
          stringBuilder.append(":spend=");
          stringBuilder.append(l);
        } 
        OplusLog.d(DBG, "LongshotDump", stringBuilder.toString());
        viewNode = paramViewNode;
        i = paramInt;
      } else {
        i = paramInt;
        viewNode = paramViewNode;
      } 
      if (i > 1)
        return; 
      ViewGroup viewGroup = (ViewGroup)paramView;
      int j = viewGroup.getChildCount();
      ArrayList<View> arrayList2 = viewGroup.buildOrderedChildList();
      if (arrayList2 == null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (bool1 && viewGroup.isChildrenDrawingOrderEnabled()) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      List<View> list;
      Point point;
      ArrayList<View> arrayList1;
      for (point = paramPoint, list = paramList, paramInt = 0, arrayList1 = arrayList2; paramInt < j; paramInt++) {
        View view;
        int k;
        if (bool2) {
          k = viewGroup.getChildDrawingOrder(j, paramInt);
        } else {
          k = paramInt;
        } 
        if (bool1) {
          view = viewGroup.getChildAt(k);
        } else {
          view = arrayList1.get(k);
        } 
        arrayList2 = null;
        if (bool)
          point = null; 
        if (bool)
          list = arrayList2; 
        dumpScrollNodes(viewNode, view, point, list, i, paramOplusLongshotViewInfo);
      } 
      if (arrayList1 != null)
        arrayList1.clear(); 
    } else {
      viewNode.findViewsLongshotInfo(paramOplusLongshotViewInfo);
    } 
  }
  
  private void dumpHierarchyLongshot(OplusLongshotDump paramOplusLongshotDump, View paramView) {
    long l = SystemClock.uptimeMillis();
    printTag("dumpHierarchyLongshot");
    this.mViewInfo.reset();
    dumpScrollNodes(null, paramView, null, null, 0, this.mViewInfo);
    if (this.mScrollNodes.isEmpty())
      for (View view : this.mSmallViews)
        dumpScrollNodes(null, view, null, null, 0, this.mViewInfo);  
    this.mSmallViews.clear();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("dumpHierarchyLongshot");
    stringBuilder.append(" : ");
    printScrollNodes(stringBuilder.toString(), this.mScrollNodes);
    l = getTimeSpend(l);
    printSpend("dumpHierarchyLongshot", l);
    if (paramOplusLongshotDump != null) {
      paramOplusLongshotDump.setScrollCount(this.mScrollNodes.size());
      paramOplusLongshotDump.setDumpCount(this.mDumpCount);
      paramOplusLongshotDump.setSpendDump(l);
    } 
  }
  
  private void dumpHierarchyScreenshot(View paramView) {
    long l = SystemClock.uptimeMillis();
    printTag("dumpHierarchyScreenshot");
    this.mViewInfo.reset();
    dumpViewNodes(paramView, this.mViewInfo);
    l = getTimeSpend(l);
    printSpend("dumpHierarchyScreenshot", l);
  }
  
  private String getAccessibilityName(ViewNode paramViewNode) {
    CharSequence charSequence = paramViewNode.getAccessibilityName();
    if (charSequence != null)
      return charSequence.toString(); 
    return null;
  }
  
  private void selectScrollNodes(List<ViewNode> paramList) {
    if (paramList.isEmpty())
      return; 
    while (paramList.size() > 1)
      paramList.remove(0); 
    ViewNode viewNode = paramList.get(0);
    selectScrollNodes(viewNode.getChildList());
  }
  
  private boolean updateCoverRect(OplusLongshotViewUtils paramOplusLongshotViewUtils, Rect paramRect1, Rect paramRect2, Rect paramRect3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("    updateCoverRect : dstRect= ");
    stringBuilder.append(paramRect1);
    stringBuilder.append(", srcRect= ");
    stringBuilder.append(paramRect2);
    stringBuilder.append(", coverRect= ");
    stringBuilder.append(paramRect3);
    printMsg(stringBuilder.toString());
    if (!Rect.intersects(paramRect3, paramRect2))
      return false; 
    Rect rect = new Rect(paramRect2);
    int i = paramRect3.top - rect.top;
    int j = rect.bottom - paramRect3.bottom;
    if (i == 0 && j == 0) {
      printMsg("    updateCoverRect : diffT = diffB = 0");
      return false;
    } 
    if (j > i) {
      rect.top = paramRect3.bottom;
    } else {
      rect.bottom = paramRect3.top;
    } 
    boolean bool1 = isLargeHeight(rect, paramRect2);
    boolean bool2 = bool1;
    if (!bool1) {
      i = Math.max(paramRect2.top, paramRect3.top);
      j = Math.min(paramRect2.bottom, paramRect3.bottom);
      if (j > i && j - i <= this.mCoverHeight) {
        bool2 = true;
      } else {
        bool2 = bool1;
        if (paramOplusLongshotViewUtils.isBottomBarRect(paramRect3, paramRect2))
          bool2 = true; 
      } 
    } 
    if (bool2) {
      this.mFloatRects.add(new Rect(paramRect3));
      j = Math.max(paramRect1.top, rect.top);
      i = Math.min(paramRect1.bottom, rect.bottom);
      if (i <= j) {
        paramRect1.setEmpty();
      } else {
        paramRect1.top = j;
        paramRect1.bottom = i;
      } 
    } 
    return bool2;
  }
  
  private boolean isSmallWidth(Rect paramRect1, Rect paramRect2) {
    boolean bool;
    if (paramRect1.width() < paramRect2.width() / 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isLargeHeight(Rect paramRect1, Rect paramRect2) {
    boolean bool;
    if (paramRect1.height() > paramRect2.height() * 2 / 5) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isVerticalBar(Rect paramRect1, Rect paramRect2) {
    boolean bool;
    if (isLargeHeight(paramRect1, paramRect2) && isSmallWidth(paramRect1, paramRect2)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isInvalidIntersect(Rect paramRect1, Rect paramRect2) {
    if (!paramRect1.intersect(paramRect2))
      return true; 
    if (isVerticalBar(paramRect1, paramRect2))
      return true; 
    return false;
  }
  
  private void printContentView(OplusLongshotViewContent paramOplusLongshotViewContent, String paramString, Rect paramRect) {
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("    ");
      stringBuilder.append(paramString);
      stringBuilder.append(" : {");
      stringBuilder.append(paramOplusLongshotViewContent);
      if (paramRect != null) {
        stringBuilder.append("} => ");
        stringBuilder.append(paramRect);
      } 
      printMsg(stringBuilder.toString());
    } 
  }
  
  private void checkCoverContents(OplusLongshotViewUtils paramOplusLongshotViewUtils, List<OplusLongshotViewContent> paramList, Rect paramRect) {
    ArrayList<OplusLongshotViewContent> arrayList = new ArrayList();
    HashMap<Object, Object> hashMap = new HashMap<>();
    for (OplusLongshotViewContent oplusLongshotViewContent1 : paramList) {
      List<OplusLongshotViewContent> list;
      OplusLongshotViewContent oplusLongshotViewContent2 = oplusLongshotViewContent1.getParent();
      if (oplusLongshotViewContent2 == null)
        continue; 
      if (paramOplusLongshotViewUtils.isLargeCoverRect(oplusLongshotViewContent2.getRect(), paramRect, false)) {
        arrayList.add(oplusLongshotViewContent1);
        continue;
      } 
      if (hashMap.containsKey(oplusLongshotViewContent2)) {
        list = (List)hashMap.get(oplusLongshotViewContent2);
        list.add(oplusLongshotViewContent1);
        continue;
      } 
      ArrayList<OplusLongshotViewContent> arrayList1 = new ArrayList();
      arrayList1.add(oplusLongshotViewContent1);
      hashMap.put(list, arrayList1);
    } 
    paramList.clear();
    paramList.addAll(arrayList);
    for (Map.Entry<Object, Object> entry : hashMap.entrySet()) {
      List list = (List)entry.getValue();
      if (list.isEmpty())
        continue; 
      this.mTempRect1.setEmpty();
      for (OplusLongshotViewContent oplusLongshotViewContent1 : list)
        this.mTempRect1.union(oplusLongshotViewContent1.getRect()); 
      OplusLongshotViewContent oplusLongshotViewContent = (OplusLongshotViewContent)entry.getKey();
      oplusLongshotViewContent = new OplusLongshotViewContent(oplusLongshotViewContent.getView(), this.mTempRect1, null);
      paramList.add(oplusLongshotViewContent);
    } 
  }
  
  private void calcScrollRectForViews(OplusLongshotViewUtils paramOplusLongshotViewUtils, Rect paramRect1, Rect paramRect2, View paramView) {
    ArrayList<?> arrayList = new ArrayList();
    ArrayList arrayList1 = new ArrayList();
    ViewParent viewParent = paramView.getParent();
    while (viewParent instanceof ViewGroup) {
      ViewGroup viewGroup2 = (ViewGroup)viewParent;
      if (viewGroup2.getChildCount() > 1) {
        paramOplusLongshotViewUtils.findCoverRect(1, viewGroup2, paramView, arrayList, arrayList1, paramRect2, null, false);
      } else {
        viewGroup2.getBoundsOnScreen(this.mTempRect1, true);
        boolean bool = DBG;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("    nofindCoverRect : rootRect=");
        stringBuilder1.append(paramRect2);
        stringBuilder1.append(", srcRect=");
        stringBuilder1.append(this.mTempRect1);
        stringBuilder1.append(", group=");
        stringBuilder1.append(viewGroup2);
        OplusLog.d(bool, "LongshotDump", stringBuilder1.toString());
      } 
      ViewGroup viewGroup1 = viewGroup2;
      ViewParent viewParent1 = viewGroup1.getParent();
    } 
    Collections.sort(arrayList, CONTENT_COMPARATOR);
    checkCoverContents(paramOplusLongshotViewUtils, (List)arrayList, paramRect2);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("-------------------------calcScrollRectForViews : coverContents=");
    stringBuilder.append(arrayList.size());
    stringBuilder.append(", sideContents=");
    stringBuilder.append(arrayList1.size());
    String str = stringBuilder.toString();
    printMsg(str);
    for (OplusLongshotViewContent oplusLongshotViewContent : arrayList) {
      boolean bool = updateCoverRect(paramOplusLongshotViewUtils, paramRect1, paramRect2, oplusLongshotViewContent.getRect());
      if (bool) {
        str = "update";
      } else {
        str = "skip  ";
      } 
      printContentView(oplusLongshotViewContent, str, paramRect1);
    } 
    for (OplusLongshotViewContent oplusLongshotViewContent : arrayList1) {
      this.mSideRects.add(new Rect(oplusLongshotViewContent.getRect()));
      printContentView(oplusLongshotViewContent, "sidebar", null);
    } 
  }
  
  private void calcScrollRectForWindow(OplusLongshotViewUtils paramOplusLongshotViewUtils, Rect paramRect1, Rect paramRect2, OplusWindowNode paramOplusWindowNode) {
    this.mTempRect1.set(paramOplusWindowNode.getCoverRect());
    if (isInvalidIntersect(this.mTempRect1, paramRect2))
      return; 
    boolean bool = updateCoverRect(paramOplusLongshotViewUtils, paramRect1, paramRect2, this.mTempRect1);
    if (DBG) {
      String str;
      if (bool) {
        str = "update";
      } else {
        str = "skip  ";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("    ");
      stringBuilder.append(str);
      stringBuilder.append(" : {");
      stringBuilder.append(paramOplusWindowNode);
      stringBuilder.append("} => ");
      stringBuilder.append(paramRect1);
      printMsg(stringBuilder.toString());
    } 
  }
  
  private void calcScrollRectForWindows(OplusLongshotViewUtils paramOplusLongshotViewUtils, Rect paramRect1, Rect paramRect2, List<OplusWindowNode> paramList) {
    if (paramList != null)
      for (OplusWindowNode oplusWindowNode : paramList)
        calcScrollRectForWindow(paramOplusLongshotViewUtils, paramRect1, paramRect2, oplusWindowNode);  
  }
  
  private void calcScrollRect(OplusLongshotDump paramOplusLongshotDump, OplusLongshotViewUtils paramOplusLongshotViewUtils, ViewNode paramViewNode, List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) {
    OplusLog.d(DBG, "LongshotDump", "==========calcScrollRect====: ");
    if (paramViewNode == null) {
      OplusLog.d(DBG, "LongshotDump", "  calcScrollRect, scrollNode=null");
      return;
    } 
    long l = SystemClock.uptimeMillis();
    printTag("calcScrollRect");
    View view = paramViewNode.getView();
    Rect rect2 = new Rect();
    view.getBoundsOnScreen(rect2, true);
    Rect rect1 = new Rect(rect2);
    calcScrollRectForViews(paramOplusLongshotViewUtils, rect1, rect2, view);
    paramViewNode.setScrollRect(rect1);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("calcScrollRect");
    stringBuilder.append(" : scrollRect=");
    stringBuilder.append(rect1);
    printMsg(stringBuilder.toString());
    l = getTimeSpend(l);
    printSpend("calcScrollRect", l);
    if (paramOplusLongshotDump != null) {
      paramOplusLongshotDump.setScrollRect(rect1);
      paramOplusLongshotDump.setSpendCalc(l);
    } 
  }
  
  private void calcScrollRects(OplusLongshotDump paramOplusLongshotDump, OplusLongshotViewUtils paramOplusLongshotViewUtils, List<ViewNode> paramList, List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) {
    for (ViewNode viewNode : paramList) {
      if (paramOplusLongshotDump != null) {
        OplusBaseView oplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, viewNode.getView());
        String str = getAccessibilityName(viewNode);
        paramOplusLongshotDump.setScrollComponent(OplusLongshotComponentName.create((OplusLongshotViewBase)oplusBaseView, str));
      } 
      calcScrollRect(paramOplusLongshotDump, paramOplusLongshotViewUtils, viewNode, paramList1, paramList2);
      calcScrollRects(paramOplusLongshotDump, paramOplusLongshotViewUtils, viewNode.getChildList(), paramList1, paramList2);
    } 
  }
  
  private void reportDumpResult(Context paramContext, OplusLongshotDump paramOplusLongshotDump) {
    if (paramOplusLongshotDump != null) {
      OplusScreenshotManager oplusScreenshotManager = OplusLongshotUtils.getScreenshotManager(paramContext);
      if (oplusScreenshotManager != null)
        oplusScreenshotManager.reportLongshotDumpResult(paramOplusLongshotDump); 
    } 
  }
  
  private void scrollNodesToJson(JSONArray paramJSONArray, List<ViewNode> paramList, boolean paramBoolean) {
    // Byte code:
    //   0: aload_2
    //   1: invokeinterface iterator : ()Ljava/util/Iterator;
    //   6: astore #4
    //   8: aload #4
    //   10: invokeinterface hasNext : ()Z
    //   15: ifeq -> 1119
    //   18: aload #4
    //   20: invokeinterface next : ()Ljava/lang/Object;
    //   25: checkcast android/view/OplusLongshotViewDump$ViewNode
    //   28: astore #5
    //   30: aload #5
    //   32: invokevirtual getView : ()Landroid/view/View;
    //   35: astore #6
    //   37: aload #6
    //   39: instanceof android/view/ViewGroup
    //   42: ifeq -> 1105
    //   45: aconst_null
    //   46: astore #7
    //   48: aconst_null
    //   49: astore #8
    //   51: aconst_null
    //   52: astore #9
    //   54: aconst_null
    //   55: astore #10
    //   57: aconst_null
    //   58: astore #11
    //   60: aconst_null
    //   61: astore_2
    //   62: aload #6
    //   64: checkcast android/view/ViewGroup
    //   67: astore #12
    //   69: new org/json/JSONObject
    //   72: astore #13
    //   74: aload #13
    //   76: invokespecial <init> : ()V
    //   79: aload #13
    //   81: ldc 'parent_hash'
    //   83: aload #12
    //   85: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   88: invokevirtual put : (Ljava/lang/String;I)Lorg/json/JSONObject;
    //   91: pop
    //   92: aload #12
    //   94: aload_0
    //   95: getfield mTempRect1 : Landroid/graphics/Rect;
    //   98: iconst_0
    //   99: invokevirtual getBoundsOnScreen : (Landroid/graphics/Rect;Z)V
    //   102: aload #13
    //   104: ldc 'parent_rect_full'
    //   106: aload_0
    //   107: getfield mTempRect1 : Landroid/graphics/Rect;
    //   110: invokevirtual flattenToString : ()Ljava/lang/String;
    //   113: invokevirtual put : (Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   116: pop
    //   117: aload #12
    //   119: aload_0
    //   120: getfield mTempRect1 : Landroid/graphics/Rect;
    //   123: iconst_1
    //   124: invokevirtual getBoundsOnScreen : (Landroid/graphics/Rect;Z)V
    //   127: aload #13
    //   129: ldc 'parent_rect_clip'
    //   131: aload_0
    //   132: getfield mTempRect1 : Landroid/graphics/Rect;
    //   135: invokevirtual flattenToString : ()Ljava/lang/String;
    //   138: invokevirtual put : (Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   141: pop
    //   142: aload #5
    //   144: invokevirtual getScrollRect : ()Landroid/graphics/Rect;
    //   147: astore #14
    //   149: aload #13
    //   151: ldc 'scroll_rect'
    //   153: aload #14
    //   155: invokevirtual flattenToString : ()Ljava/lang/String;
    //   158: invokevirtual put : (Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   161: pop
    //   162: aload #6
    //   164: astore #15
    //   166: aload_2
    //   167: astore #9
    //   169: aload #4
    //   171: astore #16
    //   173: aload #6
    //   175: astore #17
    //   177: aload #7
    //   179: astore #10
    //   181: aload #4
    //   183: astore #18
    //   185: aload #6
    //   187: astore #19
    //   189: aload #8
    //   191: astore #11
    //   193: aload #13
    //   195: ldc 'scroll_child'
    //   197: iload_3
    //   198: invokevirtual put : (Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   201: pop
    //   202: aload #6
    //   204: astore #15
    //   206: aload_2
    //   207: astore #9
    //   209: aload #4
    //   211: astore #16
    //   213: aload #6
    //   215: astore #17
    //   217: aload #7
    //   219: astore #10
    //   221: aload #4
    //   223: astore #18
    //   225: aload #6
    //   227: astore #19
    //   229: aload #8
    //   231: astore #11
    //   233: new org/json/JSONArray
    //   236: astore #20
    //   238: aload #6
    //   240: astore #15
    //   242: aload_2
    //   243: astore #9
    //   245: aload #4
    //   247: astore #16
    //   249: aload #6
    //   251: astore #17
    //   253: aload #7
    //   255: astore #10
    //   257: aload #4
    //   259: astore #18
    //   261: aload #6
    //   263: astore #19
    //   265: aload #8
    //   267: astore #11
    //   269: aload #20
    //   271: invokespecial <init> : ()V
    //   274: aload #6
    //   276: astore #15
    //   278: aload_2
    //   279: astore #9
    //   281: aload #4
    //   283: astore #16
    //   285: aload #6
    //   287: astore #17
    //   289: aload #7
    //   291: astore #10
    //   293: aload #4
    //   295: astore #18
    //   297: aload #6
    //   299: astore #19
    //   301: aload #8
    //   303: astore #11
    //   305: aload #12
    //   307: invokevirtual getChildCount : ()I
    //   310: istore #21
    //   312: aload #6
    //   314: astore #15
    //   316: aload_2
    //   317: astore #9
    //   319: aload #4
    //   321: astore #16
    //   323: aload #6
    //   325: astore #17
    //   327: aload #7
    //   329: astore #10
    //   331: aload #4
    //   333: astore #18
    //   335: aload #6
    //   337: astore #19
    //   339: aload #8
    //   341: astore #11
    //   343: aload #12
    //   345: invokevirtual buildOrderedChildList : ()Ljava/util/ArrayList;
    //   348: astore_2
    //   349: aload_2
    //   350: ifnonnull -> 359
    //   353: iconst_1
    //   354: istore #22
    //   356: goto -> 362
    //   359: iconst_0
    //   360: istore #22
    //   362: iload #22
    //   364: ifeq -> 427
    //   367: aload #6
    //   369: astore #11
    //   371: aload #4
    //   373: astore #11
    //   375: aload #6
    //   377: astore #9
    //   379: aload #4
    //   381: astore #9
    //   383: aload #6
    //   385: astore #15
    //   387: aload #12
    //   389: invokevirtual isChildrenDrawingOrderEnabled : ()Z
    //   392: ifeq -> 427
    //   395: iconst_1
    //   396: istore #23
    //   398: goto -> 430
    //   401: astore_1
    //   402: goto -> 1095
    //   405: astore #4
    //   407: aload_2
    //   408: astore #6
    //   410: aload #11
    //   412: astore_2
    //   413: goto -> 892
    //   416: astore #4
    //   418: aload_2
    //   419: astore #6
    //   421: aload #9
    //   423: astore_2
    //   424: goto -> 997
    //   427: iconst_0
    //   428: istore #23
    //   430: iconst_0
    //   431: istore #24
    //   433: iload #24
    //   435: iload #21
    //   437: if_icmpge -> 764
    //   440: iload #23
    //   442: ifeq -> 479
    //   445: aload #6
    //   447: astore #11
    //   449: aload #4
    //   451: astore #11
    //   453: aload #6
    //   455: astore #9
    //   457: aload #4
    //   459: astore #9
    //   461: aload #6
    //   463: astore #15
    //   465: aload #12
    //   467: iload #21
    //   469: iload #24
    //   471: invokevirtual getChildDrawingOrder : (II)I
    //   474: istore #25
    //   476: goto -> 483
    //   479: iload #24
    //   481: istore #25
    //   483: iload #22
    //   485: ifeq -> 520
    //   488: aload #6
    //   490: astore #11
    //   492: aload #4
    //   494: astore #11
    //   496: aload #6
    //   498: astore #9
    //   500: aload #4
    //   502: astore #9
    //   504: aload #6
    //   506: astore #15
    //   508: aload #12
    //   510: iload #25
    //   512: invokevirtual getChildAt : (I)Landroid/view/View;
    //   515: astore #10
    //   517: goto -> 564
    //   520: aload #6
    //   522: astore #15
    //   524: aload_2
    //   525: astore #9
    //   527: aload #4
    //   529: astore #16
    //   531: aload #6
    //   533: astore #17
    //   535: aload_2
    //   536: astore #10
    //   538: aload #4
    //   540: astore #18
    //   542: aload #6
    //   544: astore #19
    //   546: aload_2
    //   547: astore #11
    //   549: aload_2
    //   550: iload #25
    //   552: invokevirtual get : (I)Ljava/lang/Object;
    //   555: checkcast android/view/View
    //   558: astore #8
    //   560: aload #8
    //   562: astore #10
    //   564: aload #4
    //   566: astore #11
    //   568: aload #10
    //   570: ifnonnull -> 576
    //   573: goto -> 734
    //   576: aload #6
    //   578: astore #15
    //   580: aload_2
    //   581: astore #9
    //   583: aload #10
    //   585: invokevirtual isVisibleToUser : ()Z
    //   588: ifne -> 594
    //   591: goto -> 734
    //   594: aload #6
    //   596: astore #15
    //   598: aload_2
    //   599: astore #9
    //   601: new org/json/JSONObject
    //   604: astore #17
    //   606: aload #6
    //   608: astore #15
    //   610: aload_2
    //   611: astore #9
    //   613: aload #17
    //   615: invokespecial <init> : ()V
    //   618: aload_2
    //   619: astore #9
    //   621: aload #17
    //   623: ldc 'child_hash'
    //   625: aload #10
    //   627: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   630: invokevirtual put : (Ljava/lang/String;I)Lorg/json/JSONObject;
    //   633: pop
    //   634: aload_2
    //   635: astore #9
    //   637: aload #10
    //   639: aload_0
    //   640: getfield mTempRect1 : Landroid/graphics/Rect;
    //   643: iconst_0
    //   644: invokevirtual getBoundsOnScreen : (Landroid/graphics/Rect;Z)V
    //   647: aload_2
    //   648: astore #9
    //   650: aload #17
    //   652: ldc 'child_rect_full'
    //   654: aload_0
    //   655: getfield mTempRect1 : Landroid/graphics/Rect;
    //   658: invokevirtual flattenToString : ()Ljava/lang/String;
    //   661: invokevirtual put : (Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   664: pop
    //   665: aload_2
    //   666: astore #9
    //   668: aload #10
    //   670: aload_0
    //   671: getfield mTempRect1 : Landroid/graphics/Rect;
    //   674: iconst_1
    //   675: invokevirtual getBoundsOnScreen : (Landroid/graphics/Rect;Z)V
    //   678: aload_2
    //   679: astore #9
    //   681: aload #17
    //   683: ldc 'child_rect_clip'
    //   685: aload_0
    //   686: getfield mTempRect1 : Landroid/graphics/Rect;
    //   689: invokevirtual flattenToString : ()Ljava/lang/String;
    //   692: invokevirtual put : (Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   695: pop
    //   696: aload_2
    //   697: astore #9
    //   699: aload #12
    //   701: instanceof android/widget/ScrollView
    //   704: ifeq -> 723
    //   707: aload_2
    //   708: astore #9
    //   710: aload #17
    //   712: ldc 'child_scrollY'
    //   714: aload #10
    //   716: invokevirtual getScrollY : ()I
    //   719: invokevirtual put : (Ljava/lang/String;I)Lorg/json/JSONObject;
    //   722: pop
    //   723: aload_2
    //   724: astore #9
    //   726: aload #20
    //   728: aload #17
    //   730: invokevirtual put : (Ljava/lang/Object;)Lorg/json/JSONArray;
    //   733: pop
    //   734: iinc #24, 1
    //   737: aload #11
    //   739: astore #4
    //   741: goto -> 433
    //   744: astore #4
    //   746: aload_2
    //   747: astore #10
    //   749: goto -> 885
    //   752: astore #4
    //   754: aload_2
    //   755: astore #9
    //   757: aload #11
    //   759: astore #15
    //   761: goto -> 990
    //   764: aload #4
    //   766: astore #6
    //   768: aload_2
    //   769: astore #9
    //   771: aload #13
    //   773: ldc 'child_list'
    //   775: aload #20
    //   777: invokevirtual put : (Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   780: pop
    //   781: aload_2
    //   782: astore #9
    //   784: aload_1
    //   785: aload #13
    //   787: invokevirtual put : (Ljava/lang/Object;)Lorg/json/JSONArray;
    //   790: pop
    //   791: aload #6
    //   793: astore #4
    //   795: aload_2
    //   796: ifnull -> 1105
    //   799: aload_2
    //   800: astore #4
    //   802: aload #6
    //   804: astore_2
    //   805: goto -> 1080
    //   808: aload #4
    //   810: astore #11
    //   812: astore #4
    //   814: aload_2
    //   815: astore #6
    //   817: aload #11
    //   819: astore_2
    //   820: goto -> 892
    //   823: aload #4
    //   825: astore #11
    //   827: astore #4
    //   829: aload_2
    //   830: astore #6
    //   832: aload #11
    //   834: astore_2
    //   835: goto -> 997
    //   838: astore_1
    //   839: aload #15
    //   841: astore #6
    //   843: goto -> 871
    //   846: astore_2
    //   847: aload #16
    //   849: astore #4
    //   851: aload #17
    //   853: astore #6
    //   855: goto -> 878
    //   858: astore_2
    //   859: aload #18
    //   861: astore #4
    //   863: aload #19
    //   865: astore #6
    //   867: goto -> 979
    //   870: astore_1
    //   871: aload #9
    //   873: astore_2
    //   874: goto -> 1095
    //   877: astore_2
    //   878: aload #4
    //   880: astore #11
    //   882: aload_2
    //   883: astore #4
    //   885: aload #11
    //   887: astore_2
    //   888: aload #10
    //   890: astore #6
    //   892: aload #6
    //   894: astore #9
    //   896: getstatic android/view/OplusLongshotViewDump.DBG : Z
    //   899: istore #26
    //   901: aload #6
    //   903: astore #9
    //   905: new java/lang/StringBuilder
    //   908: astore #11
    //   910: aload #6
    //   912: astore #9
    //   914: aload #11
    //   916: invokespecial <init> : ()V
    //   919: aload #6
    //   921: astore #9
    //   923: aload #11
    //   925: ldc_w 'scrollNodesToJson:'
    //   928: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   931: pop
    //   932: aload #6
    //   934: astore #9
    //   936: aload #11
    //   938: aload #4
    //   940: invokestatic getStackTraceString : (Ljava/lang/Throwable;)Ljava/lang/String;
    //   943: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   946: pop
    //   947: aload #6
    //   949: astore #9
    //   951: iload #26
    //   953: ldc 'LongshotDump'
    //   955: aload #11
    //   957: invokevirtual toString : ()Ljava/lang/String;
    //   960: invokestatic e : (ZLjava/lang/String;Ljava/lang/String;)V
    //   963: aload_2
    //   964: astore #4
    //   966: aload #6
    //   968: ifnull -> 1105
    //   971: aload #6
    //   973: astore #4
    //   975: goto -> 1080
    //   978: astore_2
    //   979: aload #4
    //   981: astore #15
    //   983: aload #11
    //   985: astore #9
    //   987: aload_2
    //   988: astore #4
    //   990: aload #15
    //   992: astore_2
    //   993: aload #9
    //   995: astore #6
    //   997: aload #6
    //   999: astore #9
    //   1001: getstatic android/view/OplusLongshotViewDump.DBG : Z
    //   1004: istore #26
    //   1006: aload #6
    //   1008: astore #9
    //   1010: new java/lang/StringBuilder
    //   1013: astore #11
    //   1015: aload #6
    //   1017: astore #9
    //   1019: aload #11
    //   1021: invokespecial <init> : ()V
    //   1024: aload #6
    //   1026: astore #9
    //   1028: aload #11
    //   1030: ldc_w 'scrollNodesToJson:'
    //   1033: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1036: pop
    //   1037: aload #6
    //   1039: astore #9
    //   1041: aload #11
    //   1043: aload #4
    //   1045: invokestatic getStackTraceString : (Ljava/lang/Throwable;)Ljava/lang/String;
    //   1048: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1051: pop
    //   1052: aload #6
    //   1054: astore #9
    //   1056: iload #26
    //   1058: ldc 'LongshotDump'
    //   1060: aload #11
    //   1062: invokevirtual toString : ()Ljava/lang/String;
    //   1065: invokestatic e : (ZLjava/lang/String;Ljava/lang/String;)V
    //   1068: aload_2
    //   1069: astore #4
    //   1071: aload #6
    //   1073: ifnull -> 1105
    //   1076: aload #6
    //   1078: astore #4
    //   1080: aload #4
    //   1082: invokevirtual clear : ()V
    //   1085: aload_2
    //   1086: astore #4
    //   1088: goto -> 1105
    //   1091: astore_1
    //   1092: aload #9
    //   1094: astore_2
    //   1095: aload_2
    //   1096: ifnull -> 1103
    //   1099: aload_2
    //   1100: invokevirtual clear : ()V
    //   1103: aload_1
    //   1104: athrow
    //   1105: aload_0
    //   1106: aload_1
    //   1107: aload #5
    //   1109: invokevirtual getChildList : ()Ljava/util/List;
    //   1112: iconst_1
    //   1113: invokespecial scrollNodesToJson : (Lorg/json/JSONArray;Ljava/util/List;Z)V
    //   1116: goto -> 8
    //   1119: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #711	-> 0
    //   #712	-> 30
    //   #713	-> 37
    //   #714	-> 45
    //   #716	-> 62
    //   #717	-> 69
    //   #718	-> 79
    //   #719	-> 92
    //   #720	-> 102
    //   #721	-> 117
    //   #722	-> 127
    //   #723	-> 142
    //   #724	-> 149
    //   #725	-> 162
    //   #726	-> 202
    //   #727	-> 274
    //   #728	-> 312
    //   #729	-> 349
    //   #730	-> 362
    //   #758	-> 401
    //   #755	-> 405
    //   #753	-> 416
    //   #730	-> 427
    //   #731	-> 430
    //   #732	-> 440
    //   #733	-> 483
    //   #734	-> 564
    //   #735	-> 573
    //   #737	-> 576
    //   #738	-> 591
    //   #740	-> 594
    //   #741	-> 618
    //   #742	-> 634
    //   #743	-> 647
    //   #744	-> 665
    //   #745	-> 678
    //   #746	-> 696
    //   #747	-> 707
    //   #749	-> 723
    //   #731	-> 734
    //   #755	-> 744
    //   #753	-> 752
    //   #731	-> 764
    //   #751	-> 768
    //   #752	-> 781
    //   #758	-> 791
    //   #759	-> 799
    //   #755	-> 808
    //   #753	-> 823
    //   #758	-> 838
    //   #755	-> 846
    //   #753	-> 858
    //   #758	-> 870
    //   #755	-> 877
    //   #756	-> 892
    //   #758	-> 963
    //   #759	-> 971
    //   #753	-> 978
    //   #754	-> 997
    //   #758	-> 1068
    //   #759	-> 1080
    //   #758	-> 1091
    //   #759	-> 1099
    //   #761	-> 1103
    //   #713	-> 1105
    //   #763	-> 1105
    //   #764	-> 1116
    //   #765	-> 1119
    // Exception table:
    //   from	to	target	type
    //   62	69	978	org/json/JSONException
    //   62	69	877	java/lang/Exception
    //   62	69	870	finally
    //   69	79	978	org/json/JSONException
    //   69	79	877	java/lang/Exception
    //   69	79	870	finally
    //   79	92	978	org/json/JSONException
    //   79	92	877	java/lang/Exception
    //   79	92	870	finally
    //   92	102	978	org/json/JSONException
    //   92	102	877	java/lang/Exception
    //   92	102	870	finally
    //   102	117	978	org/json/JSONException
    //   102	117	877	java/lang/Exception
    //   102	117	870	finally
    //   117	127	978	org/json/JSONException
    //   117	127	877	java/lang/Exception
    //   117	127	870	finally
    //   127	142	978	org/json/JSONException
    //   127	142	877	java/lang/Exception
    //   127	142	870	finally
    //   142	149	978	org/json/JSONException
    //   142	149	877	java/lang/Exception
    //   142	149	870	finally
    //   149	162	978	org/json/JSONException
    //   149	162	877	java/lang/Exception
    //   149	162	870	finally
    //   193	202	858	org/json/JSONException
    //   193	202	846	java/lang/Exception
    //   193	202	838	finally
    //   233	238	858	org/json/JSONException
    //   233	238	846	java/lang/Exception
    //   233	238	838	finally
    //   269	274	858	org/json/JSONException
    //   269	274	846	java/lang/Exception
    //   269	274	838	finally
    //   305	312	858	org/json/JSONException
    //   305	312	846	java/lang/Exception
    //   305	312	838	finally
    //   343	349	858	org/json/JSONException
    //   343	349	846	java/lang/Exception
    //   343	349	838	finally
    //   387	395	416	org/json/JSONException
    //   387	395	405	java/lang/Exception
    //   387	395	401	finally
    //   465	476	416	org/json/JSONException
    //   465	476	405	java/lang/Exception
    //   465	476	401	finally
    //   508	517	416	org/json/JSONException
    //   508	517	405	java/lang/Exception
    //   508	517	401	finally
    //   549	560	858	org/json/JSONException
    //   549	560	846	java/lang/Exception
    //   549	560	838	finally
    //   583	591	752	org/json/JSONException
    //   583	591	744	java/lang/Exception
    //   583	591	838	finally
    //   601	606	752	org/json/JSONException
    //   601	606	744	java/lang/Exception
    //   601	606	838	finally
    //   613	618	752	org/json/JSONException
    //   613	618	744	java/lang/Exception
    //   613	618	838	finally
    //   621	634	823	org/json/JSONException
    //   621	634	808	java/lang/Exception
    //   621	634	1091	finally
    //   637	647	823	org/json/JSONException
    //   637	647	808	java/lang/Exception
    //   637	647	1091	finally
    //   650	665	823	org/json/JSONException
    //   650	665	808	java/lang/Exception
    //   650	665	1091	finally
    //   668	678	823	org/json/JSONException
    //   668	678	808	java/lang/Exception
    //   668	678	1091	finally
    //   681	696	823	org/json/JSONException
    //   681	696	808	java/lang/Exception
    //   681	696	1091	finally
    //   699	707	823	org/json/JSONException
    //   699	707	808	java/lang/Exception
    //   699	707	1091	finally
    //   710	723	823	org/json/JSONException
    //   710	723	808	java/lang/Exception
    //   710	723	1091	finally
    //   726	734	823	org/json/JSONException
    //   726	734	808	java/lang/Exception
    //   726	734	1091	finally
    //   771	781	823	org/json/JSONException
    //   771	781	808	java/lang/Exception
    //   771	781	1091	finally
    //   784	791	823	org/json/JSONException
    //   784	791	808	java/lang/Exception
    //   784	791	1091	finally
    //   896	901	1091	finally
    //   905	910	1091	finally
    //   914	919	1091	finally
    //   923	932	1091	finally
    //   936	947	1091	finally
    //   951	963	1091	finally
    //   1001	1006	1091	finally
    //   1010	1015	1091	finally
    //   1019	1024	1091	finally
    //   1028	1037	1091	finally
    //   1041	1052	1091	finally
    //   1056	1068	1091	finally
  }
  
  private List<OplusWindowNode> mergeWindowList(List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) {
    ArrayList<OplusWindowNode> arrayList = new ArrayList();
    if (paramList1 != null)
      arrayList.addAll(paramList1); 
    if (paramList2 != null)
      arrayList.addAll(paramList2); 
    return arrayList;
  }
  
  private JSONArray windowNodesToJson(List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) {
    StringBuilder stringBuilder;
    List list = null;
    List<OplusWindowNode> list1 = mergeWindowList(paramList1, paramList2);
    paramList1 = list;
    if (!list1.isEmpty()) {
      JSONArray jSONArray = new JSONArray();
      Iterator<OplusWindowNode> iterator = list1.iterator();
      while (true) {
        JSONArray jSONArray1 = jSONArray;
        if (iterator.hasNext()) {
          OplusWindowNode oplusWindowNode = iterator.next();
          try {
            JSONObject jSONObject = new JSONObject();
            this();
            jSONObject.put("window_statbar", oplusWindowNode.isStatusBar());
            jSONObject.put("window_navibar", oplusWindowNode.isNavigationBar());
            jSONObject.put("window_layer", oplusWindowNode.getSurfaceLayer());
            jSONObject.put("window_rect_decor", oplusWindowNode.getDecorRect().flattenToString());
            jSONObject.put("window_rect_visible", oplusWindowNode.getCoverRect().flattenToString());
            jSONArray.put(jSONObject);
          } catch (JSONException jSONException) {
            boolean bool = DBG;
            stringBuilder = new StringBuilder();
            stringBuilder.append("windowNodesToJson:");
            stringBuilder.append(Log.getStackTraceString((Throwable)jSONException));
            OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
          } catch (Exception exception) {
            boolean bool = DBG;
            stringBuilder = new StringBuilder();
            stringBuilder.append("windowNodesToJson:");
            stringBuilder.append(Log.getStackTraceString(exception));
            OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
          } 
          continue;
        } 
        break;
      } 
    } 
    return (JSONArray)stringBuilder;
  }
  
  private JSONArray sideRectsToJson(List<Rect> paramList) {
    JSONArray jSONArray = null;
    if (!paramList.isEmpty()) {
      JSONArray jSONArray1 = new JSONArray();
      Iterator<Rect> iterator = paramList.iterator();
      while (true) {
        jSONArray = jSONArray1;
        if (iterator.hasNext()) {
          Rect rect = iterator.next();
          try {
            JSONObject jSONObject = new JSONObject();
            this();
            jSONObject.put("side_rect", rect.flattenToString());
            jSONArray1.put(jSONObject);
          } catch (JSONException jSONException) {
            boolean bool = DBG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("sideRectsToJson:");
            stringBuilder.append(Log.getStackTraceString((Throwable)jSONException));
            OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
          } catch (Exception exception) {
            boolean bool = DBG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("sideRectsToJson:");
            stringBuilder.append(Log.getStackTraceString(exception));
            OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
          } 
          continue;
        } 
        break;
      } 
    } 
    return (JSONArray)exception;
  }
  
  private JSONArray floatRectsToJson(List<Rect> paramList) {
    JSONArray jSONArray = null;
    if (!paramList.isEmpty()) {
      JSONArray jSONArray1 = new JSONArray();
      Iterator<Rect> iterator = paramList.iterator();
      while (true) {
        jSONArray = jSONArray1;
        if (iterator.hasNext()) {
          Rect rect = iterator.next();
          try {
            JSONObject jSONObject = new JSONObject();
            this();
            jSONObject.put("float_rect", rect.flattenToString());
            jSONArray1.put(jSONObject);
          } catch (JSONException jSONException) {
            boolean bool = DBG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("floatRectsToJson:");
            stringBuilder.append(Log.getStackTraceString((Throwable)jSONException));
            OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
          } catch (Exception exception) {
            boolean bool = DBG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("floatRectsToJson:");
            stringBuilder.append(Log.getStackTraceString(exception));
            OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
          } 
          continue;
        } 
        break;
      } 
    } 
    return (JSONArray)exception;
  }
  
  private JSONObject getJSONObject(JSONObject paramJSONObject) {
    JSONObject jSONObject = paramJSONObject;
    if (paramJSONObject == null)
      jSONObject = new JSONObject(); 
    return jSONObject;
  }
  
  private String packJsonNode(OplusLongshotDump paramOplusLongshotDump, PrintWriter paramPrintWriter, List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) {
    long l = SystemClock.uptimeMillis();
    printTag("packJsonNode");
    PrintWriter printWriter = null;
    JSONObject jSONObject2 = null, jSONObject3 = null;
    paramPrintWriter = printWriter;
    JSONObject jSONObject4 = jSONObject2;
    try {
      JSONObject jSONObject;
      if (this.mViewInfo.isUnsupported()) {
        paramPrintWriter = printWriter;
        jSONObject4 = jSONObject2;
        jSONObject = new JSONObject();
        paramPrintWriter = printWriter;
        jSONObject4 = jSONObject2;
        this();
        JSONObject jSONObject5 = jSONObject;
        jSONObject4 = jSONObject;
        jSONObject.put("view_unsupported", true);
        jSONObject4 = jSONObject;
      } else {
        paramPrintWriter = printWriter;
        jSONObject4 = jSONObject2;
        if (!this.mScrollNodes.isEmpty()) {
          paramPrintWriter = printWriter;
          jSONObject4 = jSONObject2;
          JSONArray jSONArray = new JSONArray();
          paramPrintWriter = printWriter;
          jSONObject4 = jSONObject2;
          this();
          paramPrintWriter = printWriter;
          jSONObject4 = jSONObject2;
          scrollNodesToJson(jSONArray, this.mScrollNodes, false);
          paramPrintWriter = printWriter;
          jSONObject4 = jSONObject2;
          jSONObject3 = getJSONObject(null);
          JSONObject jSONObject7 = jSONObject3;
          jSONObject4 = jSONObject3;
          jSONObject3.put("scroll_list", jSONArray);
        } 
        JSONObject jSONObject5 = jSONObject3;
        jSONObject4 = jSONObject3;
        JSONArray jSONArray2 = sideRectsToJson(this.mSideRects);
        JSONObject jSONObject6 = jSONObject3;
        if (jSONArray2 != null) {
          jSONObject5 = jSONObject3;
          jSONObject4 = jSONObject3;
          jSONObject6 = getJSONObject(jSONObject3);
          jSONObject5 = jSONObject6;
          jSONObject4 = jSONObject6;
          jSONObject6.put("side_list", jSONArray2);
        } 
        jSONObject5 = jSONObject6;
        jSONObject4 = jSONObject6;
        jSONArray2 = floatRectsToJson(this.mFloatRects);
        jSONObject3 = jSONObject6;
        if (jSONArray2 != null) {
          jSONObject5 = jSONObject6;
          jSONObject4 = jSONObject6;
          jSONObject3 = getJSONObject(jSONObject6);
          jSONObject5 = jSONObject3;
          jSONObject4 = jSONObject3;
          jSONObject3.put("float_list", jSONArray2);
        } 
        jSONObject5 = jSONObject3;
        jSONObject4 = jSONObject3;
        JSONArray jSONArray1 = windowNodesToJson((List<OplusWindowNode>)jSONObject, paramList2);
        jSONObject4 = jSONObject3;
        if (jSONArray1 != null) {
          jSONObject5 = jSONObject3;
          jSONObject4 = jSONObject3;
          jSONObject = getJSONObject(jSONObject3);
          jSONObject5 = jSONObject;
          jSONObject4 = jSONObject;
          jSONObject.put("window_list", jSONArray1);
          jSONObject4 = jSONObject;
        } 
      } 
    } catch (JSONException jSONException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("packJsonNode:");
      stringBuilder.append(Log.getStackTraceString((Throwable)jSONException));
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("packJsonNode:");
      stringBuilder.append(Log.getStackTraceString(exception));
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } 
    JSONObject jSONObject1 = jSONObject4;
    if (jSONObject1 != null) {
      String str = jSONObject1.toString();
    } else {
      jSONObject1 = null;
    } 
    l = getTimeSpend(l);
    printSpend("packJsonNode", l);
    if (paramOplusLongshotDump != null)
      paramOplusLongshotDump.setSpendPack(l); 
    return (String)jSONObject1;
  }
  
  private void dumpLongshot(ViewRootImpl paramViewRootImpl, PrintWriter paramPrintWriter, String paramString, OplusLongshotDump paramOplusLongshotDump, List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) {
    this.mScrollNode = null;
    this.mDumpCount = 0;
    dumpHierarchyLongshot(paramOplusLongshotDump, paramViewRootImpl.mView);
    selectScrollNodes(this.mScrollNodes);
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" : mScrollNode=");
    stringBuilder.append(this.mScrollNode);
    OplusLog.d(bool, "LongshotDump", stringBuilder.toString());
    OplusLongshotViewUtils oplusLongshotViewUtils = new OplusLongshotViewUtils(paramViewRootImpl.mContext);
    calcScrollRects(paramOplusLongshotDump, oplusLongshotViewUtils, this.mScrollNodes, paramList1, paramList2);
  }
  
  private void dumpScreenshot(ViewRootImpl paramViewRootImpl, PrintWriter paramPrintWriter, String paramString) {
    dumpHierarchyScreenshot(paramViewRootImpl.mView);
  }
  
  private void clearList() {
    this.mScrollNodes.clear();
    this.mSmallViews.clear();
    this.mFloatRects.clear();
    this.mSideRects.clear();
  }
  
  private static final class ViewNode {
    private final Rect mClipRect = new Rect();
    
    private final Rect mFullRect = new Rect();
    
    private final Rect mScrollRect = new Rect();
    
    private final List<ViewNode> mChildList = new ArrayList<>();
    
    private long mSpend = 0L;
    
    private int mOverScrollMode = -1;
    
    private final CharSequence mAccessibilityName;
    
    private final CharSequence mClassName;
    
    private final View mView;
    
    public ViewNode(View param1View, CharSequence param1CharSequence, Rect param1Rect1, Rect param1Rect2) {
      this.mView = param1View;
      this.mAccessibilityName = param1CharSequence;
      this.mClassName = param1View.getClass().getName();
      this.mClipRect.set(param1Rect1);
      this.mFullRect.set(param1Rect2);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("View[");
      if (this.mAccessibilityName != null) {
        stringBuilder.append("accessibility=");
        stringBuilder.append(this.mAccessibilityName.toString());
        stringBuilder.append(":");
      } 
      if (this.mClassName != null) {
        stringBuilder.append("class=");
        stringBuilder.append(this.mClassName.toString());
        stringBuilder.append(":");
      } 
      stringBuilder.append("clip=");
      stringBuilder.append(this.mClipRect);
      stringBuilder.append(":full=");
      stringBuilder.append(this.mFullRect);
      stringBuilder.append(":spend=");
      stringBuilder.append(this.mSpend);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public View getView() {
      return this.mView;
    }
    
    public void addChild(ViewNode param1ViewNode) {
      this.mChildList.add(param1ViewNode);
    }
    
    public List<ViewNode> getChildList() {
      return this.mChildList;
    }
    
    public CharSequence getAccessibilityName() {
      return this.mAccessibilityName;
    }
    
    public CharSequence getClassName() {
      return this.mClassName;
    }
    
    public Rect getClipRect() {
      return this.mClipRect;
    }
    
    public Rect getFullRect() {
      return this.mFullRect;
    }
    
    public void setSpend(long param1Long) {
      this.mSpend = param1Long;
    }
    
    public void setScrollRect(Rect param1Rect) {
      this.mScrollRect.set(param1Rect);
    }
    
    public Rect getScrollRect() {
      return this.mScrollRect;
    }
    
    public void disableOverScroll() {
      ViewRootImpl viewRootImpl = this.mView.getViewRootImpl();
      if (viewRootImpl != null) {
        IOplusBaseViewRoot iOplusBaseViewRoot = (IOplusBaseViewRoot)OplusTypeCastingHelper.typeCasting(IOplusBaseViewRoot.class, viewRootImpl);
        if (iOplusBaseViewRoot != null && iOplusBaseViewRoot.getOplusViewRootImplHooks() != null)
          iOplusBaseViewRoot.getOplusViewRootImplHooks().getLongshotViewRoot(); 
        this.mOverScrollMode = this.mView.getOverScrollMode();
        boolean bool = OplusLongshotViewDump.DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("disableOverScroll : ");
        stringBuilder.append(this.mOverScrollMode);
        OplusLog.d(bool, "LongshotDump", stringBuilder.toString());
        this.mView.setOverScrollMode(2);
      } 
    }
    
    public void resetOverScroll() {
      ViewRootImpl viewRootImpl = this.mView.getViewRootImpl();
      if (viewRootImpl != null) {
        IOplusBaseViewRoot iOplusBaseViewRoot = (IOplusBaseViewRoot)OplusTypeCastingHelper.typeCasting(IOplusBaseViewRoot.class, viewRootImpl);
        if (iOplusBaseViewRoot != null && iOplusBaseViewRoot.getOplusViewRootImplHooks() != null)
          iOplusBaseViewRoot.getOplusViewRootImplHooks().getLongshotViewRoot(); 
        if (this.mOverScrollMode >= 0) {
          boolean bool = OplusLongshotViewDump.DBG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("resetOverScroll : ");
          stringBuilder.append(this.mOverScrollMode);
          OplusLog.d(bool, "LongshotDump", stringBuilder.toString());
          this.mView.setOverScrollMode(this.mOverScrollMode);
          this.mOverScrollMode = -1;
        } 
      } 
    }
  }
  
  private static class ContentComparator implements Comparator<OplusLongshotViewContent> {
    private ContentComparator() {}
    
    public int compare(OplusLongshotViewContent param1OplusLongshotViewContent1, OplusLongshotViewContent param1OplusLongshotViewContent2) {
      OplusLongshotViewContent oplusLongshotViewContent1 = param1OplusLongshotViewContent1.getParent();
      OplusLongshotViewContent oplusLongshotViewContent2 = param1OplusLongshotViewContent2.getParent();
      int i = 0;
      int j = i;
      if (oplusLongshotViewContent1 != null) {
        j = i;
        if (oplusLongshotViewContent2 != null) {
          i = rectCompare(oplusLongshotViewContent1.getRect(), oplusLongshotViewContent2.getRect());
          j = i;
          if (i == 0) {
            i = System.identityHashCode(oplusLongshotViewContent1.getView());
            j = System.identityHashCode(oplusLongshotViewContent2.getView());
            j -= i;
          } 
        } 
      } 
      i = j;
      if (j == 0)
        i = rectCompare(param1OplusLongshotViewContent1.getRect(), param1OplusLongshotViewContent2.getRect()); 
      return i;
    }
    
    private int rectCompare(Rect param1Rect1, Rect param1Rect2) {
      if (param1Rect2.top > param1Rect1.top)
        return 1; 
      if (param1Rect2.top < param1Rect1.top)
        return -1; 
      if (param1Rect2.bottom < param1Rect1.bottom)
        return 1; 
      if (param1Rect2.bottom > param1Rect1.bottom)
        return -1; 
      if (param1Rect2.left > param1Rect1.left)
        return 1; 
      if (param1Rect2.left < param1Rect1.left)
        return -1; 
      if (param1Rect2.right < param1Rect1.right)
        return 1; 
      if (param1Rect2.right > param1Rect1.right)
        return -1; 
      return 0;
    }
  }
}
