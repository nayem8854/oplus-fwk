package android.view;

import android.content.res.CompatibilityInfo;
import android.graphics.Canvas;
import android.graphics.ColorSpace;
import android.graphics.GraphicBuffer;
import android.graphics.HardwareRenderer;
import android.graphics.Matrix;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RenderNode;
import android.graphics.SurfaceTexture;
import android.os.Parcel;
import android.os.Parcelable;
import android.system.OsConstants;
import android.util.Log;
import dalvik.system.CloseGuard;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Surface implements Parcelable {
  public static final Parcelable.Creator<Surface> CREATOR = new Parcelable.Creator<Surface>() {
      public Surface createFromParcel(Parcel param1Parcel) {
        try {
          Surface surface = new Surface();
          this();
          surface.readFromParcel(param1Parcel);
          return surface;
        } catch (Exception exception) {
          Log.e("Surface", "Exception creating surface from parcel", exception);
          return null;
        } 
      }
      
      public Surface[] newArray(int param1Int) {
        return new Surface[param1Int];
      }
    };
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  final Object mLock = new Object();
  
  private final Canvas mCanvas = new CompatibleCanvas();
  
  public static final int FRAME_RATE_COMPATIBILITY_DEFAULT = 0;
  
  public static final int FRAME_RATE_COMPATIBILITY_FIXED_SOURCE = 1;
  
  public static final int ROTATION_0 = 0;
  
  public static final int ROTATION_180 = 2;
  
  public static final int ROTATION_270 = 3;
  
  public static final int ROTATION_90 = 1;
  
  public static final int SCALING_MODE_FREEZE = 0;
  
  public static final int SCALING_MODE_NO_SCALE_CROP = 3;
  
  public static final int SCALING_MODE_SCALE_CROP = 2;
  
  public static final int SCALING_MODE_SCALE_TO_WINDOW = 1;
  
  private static final String TAG = "Surface";
  
  private Matrix mCompatibleMatrix;
  
  private int mGenerationId;
  
  private HwuiContext mHwuiContext;
  
  private boolean mIsAutoRefreshEnabled;
  
  private boolean mIsSharedBufferModeEnabled;
  
  private boolean mIsSingleBuffered;
  
  private long mLockedObject;
  
  private String mName;
  
  long mNativeObject;
  
  public Surface(SurfaceControl paramSurfaceControl) {
    copyFrom(paramSurfaceControl);
  }
  
  public Surface(SurfaceTexture paramSurfaceTexture) {
    if (paramSurfaceTexture != null) {
      this.mIsSingleBuffered = paramSurfaceTexture.isSingleBuffered();
      synchronized (this.mLock) {
        this.mName = paramSurfaceTexture.toString();
        setNativeObjectLocked(nativeCreateFromSurfaceTexture(paramSurfaceTexture));
        return;
      } 
    } 
    throw new IllegalArgumentException("surfaceTexture must not be null");
  }
  
  private Surface(long paramLong) {
    synchronized (this.mLock) {
      setNativeObjectLocked(paramLong);
      return;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      release();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void release() {
    synchronized (this.mLock) {
      if (this.mHwuiContext != null) {
        this.mHwuiContext.destroy();
        this.mHwuiContext = null;
      } 
      if (this.mNativeObject != 0L) {
        nativeRelease(this.mNativeObject);
        setNativeObjectLocked(0L);
      } 
      return;
    } 
  }
  
  public void destroy() {
    release();
  }
  
  public void hwuiDestroy() {
    HwuiContext hwuiContext = this.mHwuiContext;
    if (hwuiContext != null) {
      hwuiContext.destroy();
      this.mHwuiContext = null;
    } 
  }
  
  public boolean isValid() {
    synchronized (this.mLock) {
      if (this.mNativeObject == 0L)
        return false; 
      return nativeIsValid(this.mNativeObject);
    } 
  }
  
  public int getGenerationId() {
    synchronized (this.mLock) {
      return this.mGenerationId;
    } 
  }
  
  public long getNextFrameNumber() {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      return nativeGetNextFrameNumber(this.mNativeObject);
    } 
  }
  
  public boolean isConsumerRunningBehind() {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      return nativeIsConsumerRunningBehind(this.mNativeObject);
    } 
  }
  
  public Canvas lockCanvas(Rect paramRect) throws OutOfResourcesException, IllegalArgumentException {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      if (this.mLockedObject == 0L) {
        this.mLockedObject = nativeLockCanvas(this.mNativeObject, this.mCanvas, paramRect);
        return this.mCanvas;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("Surface was already locked");
      throw illegalArgumentException;
    } 
  }
  
  public void unlockCanvasAndPost(Canvas paramCanvas) {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      if (this.mHwuiContext != null) {
        this.mHwuiContext.unlockAndPost(paramCanvas);
      } else {
        unlockSwCanvasAndPost(paramCanvas);
      } 
      return;
    } 
  }
  
  private void unlockSwCanvasAndPost(Canvas paramCanvas) {
    if (paramCanvas == this.mCanvas) {
      if (this.mNativeObject != this.mLockedObject) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("WARNING: Surface's mNativeObject (0x");
        long l1 = this.mNativeObject;
        stringBuilder.append(Long.toHexString(l1));
        stringBuilder.append(") != mLockedObject (0x");
        l1 = this.mLockedObject;
        stringBuilder.append(Long.toHexString(l1));
        stringBuilder.append(")");
        String str = stringBuilder.toString();
        Log.w("Surface", str);
      } 
      long l = this.mLockedObject;
      if (l != 0L)
        try {
          nativeUnlockCanvasAndPost(l, paramCanvas);
          return;
        } finally {
          nativeRelease(this.mLockedObject);
          this.mLockedObject = 0L;
        }  
      throw new IllegalStateException("Surface was not locked");
    } 
    throw new IllegalArgumentException("canvas object must be the same instance that was previously returned by lockCanvas");
  }
  
  public Canvas lockHardwareCanvas() {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      if (this.mHwuiContext == null) {
        HwuiContext hwuiContext1 = new HwuiContext();
        this(this, false);
        this.mHwuiContext = hwuiContext1;
      } 
      HwuiContext hwuiContext = this.mHwuiContext;
      long l = this.mNativeObject;
      int i = nativeGetWidth(l);
      l = this.mNativeObject;
      int j = nativeGetHeight(l);
      return hwuiContext.lockCanvas(i, j);
    } 
  }
  
  public Canvas lockHardwareWideColorGamutCanvas() {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      if (this.mHwuiContext != null && !this.mHwuiContext.isWideColorGamut()) {
        this.mHwuiContext.destroy();
        this.mHwuiContext = null;
      } 
      if (this.mHwuiContext == null) {
        HwuiContext hwuiContext1 = new HwuiContext();
        this(this, true);
        this.mHwuiContext = hwuiContext1;
      } 
      HwuiContext hwuiContext = this.mHwuiContext;
      long l = this.mNativeObject;
      int i = nativeGetWidth(l);
      l = this.mNativeObject;
      int j = nativeGetHeight(l);
      return hwuiContext.lockCanvas(i, j);
    } 
  }
  
  @Deprecated
  public void unlockCanvas(Canvas paramCanvas) {
    throw new UnsupportedOperationException();
  }
  
  void setCompatibilityTranslator(CompatibilityInfo.Translator paramTranslator) {
    if (paramTranslator != null) {
      float f = paramTranslator.applicationScale;
      Matrix matrix = new Matrix();
      matrix.setScale(f, f);
    } 
  }
  
  public void copyFrom(SurfaceControl paramSurfaceControl) {
    if (paramSurfaceControl != null) {
      long l = paramSurfaceControl.mNativeObject;
      if (l != 0L) {
        l = nativeGetFromSurfaceControl(this.mNativeObject, l);
        synchronized (this.mLock) {
          if (l == this.mNativeObject)
            return; 
          if (this.mNativeObject != 0L)
            nativeRelease(this.mNativeObject); 
          setNativeObjectLocked(l);
          return;
        } 
      } 
      throw new NullPointerException("null SurfaceControl native object. Are you using a released SurfaceControl?");
    } 
    throw new IllegalArgumentException("other must not be null");
  }
  
  public void createFrom(SurfaceControl paramSurfaceControl) {
    if (paramSurfaceControl != null) {
      long l = paramSurfaceControl.mNativeObject;
      if (l != 0L) {
        l = nativeCreateFromSurfaceControl(l);
        synchronized (this.mLock) {
          if (this.mNativeObject != 0L)
            nativeRelease(this.mNativeObject); 
          setNativeObjectLocked(l);
          return;
        } 
      } 
      throw new NullPointerException("null SurfaceControl native object. Are you using a released SurfaceControl?");
    } 
    throw new IllegalArgumentException("other must not be null");
  }
  
  @Deprecated
  public void transferFrom(Surface paramSurface) {
    if (paramSurface != null) {
      if (paramSurface != this)
        synchronized (paramSurface.mLock) {
          long l = paramSurface.mNativeObject;
          paramSurface.setNativeObjectLocked(0L);
          synchronized (this.mLock) {
            if (this.mNativeObject != 0L)
              nativeRelease(this.mNativeObject); 
            setNativeObjectLocked(l);
          } 
        }  
      return;
    } 
    throw new IllegalArgumentException("other must not be null");
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void readFromParcel(Parcel paramParcel) {
    if (paramParcel != null)
      synchronized (this.mLock) {
        boolean bool;
        this.mName = paramParcel.readString();
        if (paramParcel.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mIsSingleBuffered = bool;
        setNativeObjectLocked(nativeReadFromParcel(this.mNativeObject, paramParcel));
        return;
      }  
    throw new IllegalArgumentException("source must not be null");
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (paramParcel != null)
      synchronized (this.mLock) {
        boolean bool;
        paramParcel.writeString(this.mName);
        if (this.mIsSingleBuffered) {
          bool = true;
        } else {
          bool = false;
        } 
        paramParcel.writeInt(bool);
        nativeWriteToParcel(this.mNativeObject, paramParcel);
        if ((paramInt & 0x1) != 0)
          release(); 
        return;
      }  
    throw new IllegalArgumentException("dest must not be null");
  }
  
  public String toString() {
    synchronized (this.mLock) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Surface(name=");
      stringBuilder.append(this.mName);
      stringBuilder.append(")/@0x");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      return stringBuilder.toString();
    } 
  }
  
  private void setNativeObjectLocked(long paramLong) {
    long l = this.mNativeObject;
    if (l != paramLong) {
      if (l == 0L && paramLong != 0L) {
        this.mCloseGuard.open("release");
      } else if (this.mNativeObject != 0L && paramLong == 0L) {
        this.mCloseGuard.close();
      } 
      this.mNativeObject = paramLong;
      this.mGenerationId++;
      HwuiContext hwuiContext = this.mHwuiContext;
      if (hwuiContext != null)
        hwuiContext.updateSurface(); 
    } 
  }
  
  private void checkNotReleasedLocked() {
    if (this.mNativeObject != 0L)
      return; 
    throw new IllegalStateException("Surface has already been released.");
  }
  
  public void allocateBuffers() {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      nativeAllocateBuffers(this.mNativeObject);
      return;
    } 
  }
  
  public boolean isBufferAccumulated() {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      return nativeIsBufferAccumulated(this.mNativeObject);
    } 
  }
  
  public void setPresentTimeMode(int paramInt) {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      nativeSetPresentTimeMode(this.mNativeObject, paramInt);
      return;
    } 
  }
  
  void setScalingMode(int paramInt) {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      int i = nativeSetScalingMode(this.mNativeObject, paramInt);
      if (i == 0)
        return; 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Invalid scaling mode: ");
      stringBuilder.append(paramInt);
      this(stringBuilder.toString());
      throw illegalArgumentException;
    } 
  }
  
  void forceScopedDisconnect() {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      int i = nativeForceScopedDisconnect(this.mNativeObject);
      if (i == 0)
        return; 
      RuntimeException runtimeException = new RuntimeException();
      this("Failed to disconnect Surface instance (bad object?)");
      throw runtimeException;
    } 
  }
  
  public void attachAndQueueBufferWithColorSpace(GraphicBuffer paramGraphicBuffer, ColorSpace paramColorSpace) {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      ColorSpace colorSpace = paramColorSpace;
      if (paramColorSpace == null)
        colorSpace = ColorSpace.get(ColorSpace.Named.SRGB); 
      long l = this.mNativeObject;
      int i = colorSpace.getId();
      i = nativeAttachAndQueueBufferWithColorSpace(l, paramGraphicBuffer, i);
      if (i == 0)
        return; 
      RuntimeException runtimeException = new RuntimeException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to attach and queue buffer to Surface (bad object?), native error: ");
      stringBuilder.append(i);
      this(stringBuilder.toString());
      throw runtimeException;
    } 
  }
  
  public void attachAndQueueBuffer(GraphicBuffer paramGraphicBuffer) {
    attachAndQueueBufferWithColorSpace(paramGraphicBuffer, ColorSpace.get(ColorSpace.Named.SRGB));
  }
  
  public boolean isSingleBuffered() {
    return this.mIsSingleBuffered;
  }
  
  public void setSharedBufferModeEnabled(boolean paramBoolean) {
    if (this.mIsSharedBufferModeEnabled != paramBoolean) {
      int i = nativeSetSharedBufferModeEnabled(this.mNativeObject, paramBoolean);
      if (i == 0) {
        this.mIsSharedBufferModeEnabled = paramBoolean;
      } else {
        throw new RuntimeException("Failed to set shared buffer mode on Surface (bad object?)");
      } 
    } 
  }
  
  public boolean isSharedBufferModeEnabled() {
    return this.mIsSharedBufferModeEnabled;
  }
  
  public void setAutoRefreshEnabled(boolean paramBoolean) {
    if (this.mIsAutoRefreshEnabled != paramBoolean) {
      int i = nativeSetAutoRefreshEnabled(this.mNativeObject, paramBoolean);
      if (i == 0) {
        this.mIsAutoRefreshEnabled = paramBoolean;
      } else {
        throw new RuntimeException("Failed to set auto refresh on Surface (bad object?)");
      } 
    } 
  }
  
  public boolean isAutoRefreshEnabled() {
    return this.mIsAutoRefreshEnabled;
  }
  
  public void setFrameRate(float paramFloat, int paramInt) {
    synchronized (this.mLock) {
      checkNotReleasedLocked();
      paramInt = nativeSetFrameRate(this.mNativeObject, paramFloat, paramInt);
      if (paramInt != -OsConstants.EINVAL) {
        if (paramInt == 0)
          return; 
        RuntimeException runtimeException = new RuntimeException();
        this("Failed to set frame rate on Surface");
        throw runtimeException;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("Invalid argument to Surface.setFrameRate()");
      throw illegalArgumentException;
    } 
  }
  
  class OutOfResourcesException extends RuntimeException {
    public OutOfResourcesException() {}
    
    public OutOfResourcesException(Surface this$0) {
      super((String)this$0);
    }
  }
  
  public static String rotationToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return Integer.toString(paramInt); 
          return "ROTATION_270";
        } 
        return "ROTATION_180";
      } 
      return "ROTATION_90";
    } 
    return "ROTATION_0";
  }
  
  public Surface() {}
  
  private static native void nativeAllocateBuffers(long paramLong);
  
  private static native int nativeAttachAndQueueBufferWithColorSpace(long paramLong, GraphicBuffer paramGraphicBuffer, int paramInt);
  
  private static native long nativeCreateFromSurfaceControl(long paramLong);
  
  private static native long nativeCreateFromSurfaceTexture(SurfaceTexture paramSurfaceTexture) throws OutOfResourcesException;
  
  private static native int nativeForceScopedDisconnect(long paramLong);
  
  private static native long nativeGetFromSurfaceControl(long paramLong1, long paramLong2);
  
  private static native int nativeGetHeight(long paramLong);
  
  private static native long nativeGetNextFrameNumber(long paramLong);
  
  private static native int nativeGetWidth(long paramLong);
  
  private static native boolean nativeIsBufferAccumulated(long paramLong);
  
  private static native boolean nativeIsConsumerRunningBehind(long paramLong);
  
  private static native boolean nativeIsValid(long paramLong);
  
  private static native long nativeLockCanvas(long paramLong, Canvas paramCanvas, Rect paramRect) throws OutOfResourcesException;
  
  private static native long nativeReadFromParcel(long paramLong, Parcel paramParcel);
  
  private static native void nativeRelease(long paramLong);
  
  private static native int nativeSetAutoRefreshEnabled(long paramLong, boolean paramBoolean);
  
  private static native int nativeSetFrameRate(long paramLong, float paramFloat, int paramInt);
  
  private static native void nativeSetPresentTimeMode(long paramLong, int paramInt);
  
  private static native int nativeSetScalingMode(long paramLong, int paramInt);
  
  private static native int nativeSetSharedBufferModeEnabled(long paramLong, boolean paramBoolean);
  
  private static native void nativeUnlockCanvasAndPost(long paramLong, Canvas paramCanvas);
  
  private static native void nativeWriteToParcel(long paramLong, Parcel paramParcel);
  
  class CompatibleCanvas extends Canvas {
    private Matrix mOrigMatrix;
    
    final Surface this$0;
    
    private CompatibleCanvas() {
      this.mOrigMatrix = null;
    }
    
    public void setMatrix(Matrix param1Matrix) {
      if (Surface.this.mCompatibleMatrix != null) {
        Matrix matrix = this.mOrigMatrix;
        if (matrix == null || matrix.equals(param1Matrix)) {
          super.setMatrix(param1Matrix);
          return;
        } 
        matrix = new Matrix(Surface.this.mCompatibleMatrix);
        matrix.preConcat(param1Matrix);
        super.setMatrix(matrix);
        return;
      } 
      super.setMatrix(param1Matrix);
    }
    
    public void getMatrix(Matrix param1Matrix) {
      super.getMatrix(param1Matrix);
      if (this.mOrigMatrix == null)
        this.mOrigMatrix = new Matrix(); 
      this.mOrigMatrix.set(param1Matrix);
    }
  }
  
  class HwuiContext {
    private RecordingCanvas mCanvas;
    
    private HardwareRenderer mHardwareRenderer;
    
    private final boolean mIsWideColorGamut;
    
    private final RenderNode mRenderNode;
    
    final Surface this$0;
    
    HwuiContext(boolean param1Boolean) {
      RenderNode renderNode = RenderNode.create("HwuiCanvas", null);
      renderNode.setClipToBounds(false);
      this.mRenderNode.setForceDarkAllowed(false);
      this.mIsWideColorGamut = param1Boolean;
      HardwareRenderer hardwareRenderer = new HardwareRenderer();
      hardwareRenderer.setContentRoot(this.mRenderNode);
      this.mHardwareRenderer.setSurface(Surface.this, true);
      this.mHardwareRenderer.setWideGamut(param1Boolean);
      this.mHardwareRenderer.setLightSourceAlpha(0.0F, 0.0F);
      this.mHardwareRenderer.setLightSourceGeometry(0.0F, 0.0F, 0.0F, 0.0F);
    }
    
    Canvas lockCanvas(int param1Int1, int param1Int2) {
      if (this.mCanvas == null) {
        RecordingCanvas recordingCanvas = this.mRenderNode.beginRecording(param1Int1, param1Int2);
        return (Canvas)recordingCanvas;
      } 
      throw new IllegalStateException("Surface was already locked!");
    }
    
    void unlockAndPost(Canvas param1Canvas) {
      if (param1Canvas == this.mCanvas) {
        this.mRenderNode.endRecording();
        this.mCanvas = null;
        HardwareRenderer.FrameRenderRequest frameRenderRequest = this.mHardwareRenderer.createRenderRequest();
        frameRenderRequest = frameRenderRequest.setVsyncTime(System.nanoTime());
        frameRenderRequest.syncAndDraw();
        return;
      } 
      throw new IllegalArgumentException("canvas object must be the same instance that was previously returned by lockCanvas");
    }
    
    void updateSurface() {
      this.mHardwareRenderer.setSurface(Surface.this, true);
    }
    
    void destroy() {
      this.mHardwareRenderer.destroy();
    }
    
    boolean isWideColorGamut() {
      return this.mIsWideColorGamut;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class FrameRateCompatibility implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Rotation implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ScalingMode implements Annotation {}
}
