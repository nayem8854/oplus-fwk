package android.view;

import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.text.TextUtils;
import android.util.Log;
import android.util.PathParser;
import java.util.Objects;

public class CutoutSpecification {
  private static final String BIND_LEFT_CUTOUT_MARKER = "@bind_left_cutout";
  
  private static final String BIND_RIGHT_CUTOUT_MARKER = "@bind_right_cutout";
  
  private static final String BOTTOM_MARKER = "@bottom";
  
  private static final String CENTER_VERTICAL_MARKER = "@center_vertical";
  
  private static final String CUTOUT_MARKER = "@cutout";
  
  private static final boolean DEBUG = false;
  
  private static final String DP_MARKER = "@dp";
  
  private static final String LEFT_MARKER = "@left";
  
  private static final char MARKER_START_CHAR = '@';
  
  private static final int MINIMAL_ACCEPTABLE_PATH_LENGTH = "H1V1Z".length();
  
  private static final String RIGHT_MARKER = "@right";
  
  private static final String TAG = "CutoutSpecification";
  
  private final Rect mBottomBound;
  
  private final Insets mInsets;
  
  private final Rect mLeftBound;
  
  private final Path mPath;
  
  private final Rect mRightBound;
  
  private final Rect mTopBound;
  
  private CutoutSpecification(Parser paramParser) {
    this.mPath = paramParser.mPath;
    this.mLeftBound = paramParser.mLeftBound;
    this.mTopBound = paramParser.mTopBound;
    this.mRightBound = paramParser.mRightBound;
    this.mBottomBound = paramParser.mBottomBound;
    this.mInsets = paramParser.mInsets;
  }
  
  public Path getPath() {
    return this.mPath;
  }
  
  public Rect getLeftBound() {
    return this.mLeftBound;
  }
  
  public Rect getTopBound() {
    return this.mTopBound;
  }
  
  public Rect getRightBound() {
    return this.mRightBound;
  }
  
  public Rect getBottomBound() {
    return this.mBottomBound;
  }
  
  public Rect getSafeInset() {
    return this.mInsets.toRect();
  }
  
  private static int decideWhichEdge(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    byte b = 3;
    if (paramBoolean1) {
      if (paramBoolean2) {
        if (paramBoolean3) {
          b = 48;
        } else {
          b = 80;
        } 
      } else if (!paramBoolean3) {
        b = 5;
      } 
    } else if (paramBoolean2) {
      if (!paramBoolean3)
        b = 5; 
    } else if (paramBoolean3) {
      b = 48;
    } else {
      b = 80;
    } 
    return b;
  }
  
  public static class Parser {
    private boolean mBindBottomCutout;
    
    private boolean mBindLeftCutout;
    
    private boolean mBindRightCutout;
    
    private Rect mBottomBound;
    
    private final float mDensity;
    
    private final int mDisplayHeight;
    
    private final int mDisplayWidth;
    
    private boolean mInDp;
    
    private Insets mInsets;
    
    private boolean mIsCloserToStartSide;
    
    private final boolean mIsShortEdgeOnTop;
    
    private boolean mIsTouchShortEdgeEnd;
    
    private boolean mIsTouchShortEdgeStart;
    
    private Rect mLeftBound;
    
    private final Matrix mMatrix;
    
    private Path mPath;
    
    private boolean mPositionFromBottom;
    
    private boolean mPositionFromCenterVertical;
    
    private boolean mPositionFromLeft;
    
    private boolean mPositionFromRight;
    
    private Rect mRightBound;
    
    private int mSafeInsetBottom;
    
    private int mSafeInsetLeft;
    
    private int mSafeInsetRight;
    
    private int mSafeInsetTop;
    
    private final Rect mTmpRect = new Rect();
    
    private final RectF mTmpRectF = new RectF();
    
    private Rect mTopBound;
    
    public Parser(float param1Float, int param1Int1, int param1Int2) {
      boolean bool = false;
      this.mPositionFromLeft = false;
      this.mPositionFromRight = false;
      this.mPositionFromBottom = false;
      this.mPositionFromCenterVertical = false;
      this.mBindLeftCutout = false;
      this.mBindRightCutout = false;
      this.mBindBottomCutout = false;
      this.mDensity = param1Float;
      this.mDisplayWidth = param1Int1;
      this.mDisplayHeight = param1Int2;
      this.mMatrix = new Matrix();
      if (this.mDisplayWidth < this.mDisplayHeight)
        bool = true; 
      this.mIsShortEdgeOnTop = bool;
    }
    
    private void computeBoundsRectAndAddToRegion(Path param1Path, Region param1Region, Rect param1Rect) {
      this.mTmpRectF.setEmpty();
      param1Path.computeBounds(this.mTmpRectF, false);
      this.mTmpRectF.round(param1Rect);
      param1Region.op(param1Rect, Region.Op.UNION);
    }
    
    private void resetStatus(StringBuilder param1StringBuilder) {
      param1StringBuilder.setLength(0);
      this.mPositionFromBottom = false;
      this.mPositionFromLeft = false;
      this.mPositionFromRight = false;
      this.mPositionFromCenterVertical = false;
      this.mBindLeftCutout = false;
      this.mBindRightCutout = false;
      this.mBindBottomCutout = false;
    }
    
    private void translateMatrix() {
      float f1, f2;
      if (this.mPositionFromRight) {
        f1 = this.mDisplayWidth;
      } else if (this.mPositionFromLeft) {
        f1 = 0.0F;
      } else {
        f1 = this.mDisplayWidth / 2.0F;
      } 
      if (this.mPositionFromBottom) {
        f2 = this.mDisplayHeight;
      } else if (this.mPositionFromCenterVertical) {
        f2 = this.mDisplayHeight / 2.0F;
      } else {
        f2 = 0.0F;
      } 
      this.mMatrix.reset();
      if (this.mInDp) {
        Matrix matrix = this.mMatrix;
        float f = this.mDensity;
        matrix.postScale(f, f);
      } 
      this.mMatrix.postTranslate(f1, f2);
    }
    
    private int computeSafeInsets(int param1Int, Rect param1Rect) {
      if (param1Int == 3 && param1Rect.right > 0 && param1Rect.right < this.mDisplayWidth)
        return param1Rect.right; 
      if (param1Int == 48 && param1Rect.bottom > 0 && param1Rect.bottom < this.mDisplayHeight)
        return param1Rect.bottom; 
      if (param1Int == 5 && param1Rect.left > 0) {
        int i = param1Rect.left, j = this.mDisplayWidth;
        if (i < j)
          return j - param1Rect.left; 
      } 
      if (param1Int == 80 && param1Rect.top > 0) {
        int i = param1Rect.top;
        param1Int = this.mDisplayHeight;
        if (i < param1Int)
          return param1Int - param1Rect.top; 
      } 
      return 0;
    }
    
    private void setSafeInset(int param1Int1, int param1Int2) {
      if (param1Int1 == 3) {
        this.mSafeInsetLeft = param1Int2;
      } else if (param1Int1 == 48) {
        this.mSafeInsetTop = param1Int2;
      } else if (param1Int1 == 5) {
        this.mSafeInsetRight = param1Int2;
      } else if (param1Int1 == 80) {
        this.mSafeInsetBottom = param1Int2;
      } 
    }
    
    private int getSafeInset(int param1Int) {
      if (param1Int == 3)
        return this.mSafeInsetLeft; 
      if (param1Int == 48)
        return this.mSafeInsetTop; 
      if (param1Int == 5)
        return this.mSafeInsetRight; 
      if (param1Int == 80)
        return this.mSafeInsetBottom; 
      return 0;
    }
    
    private Rect onSetEdgeCutout(boolean param1Boolean1, boolean param1Boolean2, Rect param1Rect) {
      int i;
      if (param1Boolean2) {
        i = CutoutSpecification.decideWhichEdge(this.mIsShortEdgeOnTop, true, param1Boolean1);
      } else if (this.mIsTouchShortEdgeStart && this.mIsTouchShortEdgeEnd) {
        i = CutoutSpecification.decideWhichEdge(this.mIsShortEdgeOnTop, false, param1Boolean1);
      } else if (this.mIsTouchShortEdgeStart || this.mIsTouchShortEdgeEnd) {
        i = CutoutSpecification.decideWhichEdge(this.mIsShortEdgeOnTop, true, this.mIsCloserToStartSide);
      } else {
        i = CutoutSpecification.decideWhichEdge(this.mIsShortEdgeOnTop, param1Boolean2, param1Boolean1);
      } 
      int j = getSafeInset(i);
      int k = computeSafeInsets(i, param1Rect);
      if (j < k)
        setSafeInset(i, k); 
      return new Rect(param1Rect);
    }
    
    private void setEdgeCutout(Path param1Path) {
      if (this.mBindRightCutout && this.mRightBound == null) {
        this.mRightBound = onSetEdgeCutout(false, this.mIsShortEdgeOnTop ^ true, this.mTmpRect);
      } else if (this.mBindLeftCutout && this.mLeftBound == null) {
        this.mLeftBound = onSetEdgeCutout(true, this.mIsShortEdgeOnTop ^ true, this.mTmpRect);
      } else if (this.mBindBottomCutout && this.mBottomBound == null) {
        this.mBottomBound = onSetEdgeCutout(false, this.mIsShortEdgeOnTop, this.mTmpRect);
      } else if (!this.mBindBottomCutout && !this.mBindLeftCutout && !this.mBindRightCutout && this.mTopBound == null) {
        this.mTopBound = onSetEdgeCutout(true, this.mIsShortEdgeOnTop, this.mTmpRect);
      } else {
        return;
      } 
      Path path = this.mPath;
      if (path != null) {
        path.addPath(param1Path);
      } else {
        this.mPath = param1Path;
      } 
    }
    
    private void parseSvgPathSpec(Region param1Region, String param1String) {
      if (TextUtils.length(param1String) < CutoutSpecification.MINIMAL_ACCEPTABLE_PATH_LENGTH) {
        Log.e("CutoutSpecification", "According to SVG definition, it shouldn't happen");
        return;
      } 
      param1String.trim();
      translateMatrix();
      Path path = PathParser.createPathFromPathData(param1String);
      path.transform(this.mMatrix);
      computeBoundsRectAndAddToRegion(path, param1Region, this.mTmpRect);
      if (this.mTmpRect.isEmpty())
        return; 
      boolean bool = this.mIsShortEdgeOnTop;
      boolean bool1 = true, bool2 = true;
      if (bool) {
        if (this.mTmpRect.top <= 0) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mIsTouchShortEdgeStart = bool;
        if (this.mTmpRect.bottom >= this.mDisplayHeight) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mIsTouchShortEdgeEnd = bool;
        if (this.mTmpRect.centerY() < this.mDisplayHeight / 2) {
          bool = bool2;
        } else {
          bool = false;
        } 
        this.mIsCloserToStartSide = bool;
      } else {
        if (this.mTmpRect.left <= 0) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mIsTouchShortEdgeStart = bool;
        if (this.mTmpRect.right >= this.mDisplayWidth) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mIsTouchShortEdgeEnd = bool;
        if (this.mTmpRect.centerX() < this.mDisplayWidth / 2) {
          bool = bool1;
        } else {
          bool = false;
        } 
        this.mIsCloserToStartSide = bool;
      } 
      setEdgeCutout(path);
    }
    
    private void parseSpecWithoutDp(String param1String) {
      Region region = Region.obtain();
      StringBuilder stringBuilder = null;
      int i = 0;
      while (true) {
        int j = param1String.indexOf('@', i);
        if (j != -1) {
          StringBuilder stringBuilder1 = stringBuilder;
          if (stringBuilder == null)
            stringBuilder1 = new StringBuilder(param1String.length()); 
          stringBuilder1.append(param1String, i, j);
          if (param1String.startsWith("@left", j)) {
            if (!this.mPositionFromRight)
              this.mPositionFromLeft = true; 
            i = j + "@left".length();
          } else if (param1String.startsWith("@right", j)) {
            if (!this.mPositionFromLeft)
              this.mPositionFromRight = true; 
            i = j + "@right".length();
          } else if (param1String.startsWith("@bottom", j)) {
            parseSvgPathSpec(region, stringBuilder1.toString());
            i = j + "@bottom".length();
            resetStatus(stringBuilder1);
            this.mBindBottomCutout = true;
            this.mPositionFromBottom = true;
          } else if (param1String.startsWith("@center_vertical", j)) {
            parseSvgPathSpec(region, stringBuilder1.toString());
            i = j + "@center_vertical".length();
            resetStatus(stringBuilder1);
            this.mPositionFromCenterVertical = true;
          } else if (param1String.startsWith("@cutout", j)) {
            parseSvgPathSpec(region, stringBuilder1.toString());
            i = j + "@cutout".length();
            resetStatus(stringBuilder1);
          } else if (param1String.startsWith("@bind_left_cutout", j)) {
            this.mBindBottomCutout = false;
            this.mBindRightCutout = false;
            this.mBindLeftCutout = true;
            i = j + "@bind_left_cutout".length();
          } else if (param1String.startsWith("@bind_right_cutout", j)) {
            this.mBindBottomCutout = false;
            this.mBindLeftCutout = false;
            this.mBindRightCutout = true;
            i = j + "@bind_right_cutout".length();
          } else {
            i = j + 1;
          } 
          stringBuilder = stringBuilder1;
          continue;
        } 
        break;
      } 
      if (stringBuilder == null) {
        parseSvgPathSpec(region, param1String);
      } else {
        stringBuilder.append(param1String, i, param1String.length());
        parseSvgPathSpec(region, stringBuilder.toString());
      } 
      region.recycle();
    }
    
    public CutoutSpecification parse(String param1String) {
      boolean bool;
      Objects.requireNonNull(param1String);
      int i = param1String.lastIndexOf("@dp");
      if (i != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mInDp = bool;
      if (i != -1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(param1String.substring(0, i));
        stringBuilder.append(param1String.substring("@dp".length() + i));
        param1String = stringBuilder.toString();
      } 
      parseSpecWithoutDp(param1String);
      this.mInsets = Insets.of(this.mSafeInsetLeft, this.mSafeInsetTop, this.mSafeInsetRight, this.mSafeInsetBottom);
      return new CutoutSpecification(this);
    }
  }
}
