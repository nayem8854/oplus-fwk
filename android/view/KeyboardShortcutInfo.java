package android.view;

import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

public final class KeyboardShortcutInfo implements Parcelable {
  public KeyboardShortcutInfo(CharSequence paramCharSequence, Icon paramIcon, int paramInt1, int paramInt2) {
    this.mLabel = paramCharSequence;
    this.mIcon = paramIcon;
    boolean bool1 = false;
    this.mBaseCharacter = Character.MIN_VALUE;
    boolean bool2 = bool1;
    if (paramInt1 >= 0) {
      bool2 = bool1;
      if (paramInt1 <= KeyEvent.getMaxKeyCode())
        bool2 = true; 
    } 
    Preconditions.checkArgument(bool2);
    this.mKeycode = paramInt1;
    this.mModifiers = paramInt2;
  }
  
  public KeyboardShortcutInfo(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    this(paramCharSequence, null, paramInt1, paramInt2);
  }
  
  public KeyboardShortcutInfo(CharSequence paramCharSequence, char paramChar, int paramInt) {
    boolean bool;
    this.mLabel = paramCharSequence;
    if (paramChar != '\000') {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool);
    this.mBaseCharacter = paramChar;
    this.mKeycode = 0;
    this.mModifiers = paramInt;
    this.mIcon = null;
  }
  
  private KeyboardShortcutInfo(Parcel paramParcel) {
    this.mLabel = paramParcel.readCharSequence();
    this.mIcon = (Icon)paramParcel.readParcelable(null);
    this.mBaseCharacter = (char)paramParcel.readInt();
    this.mKeycode = paramParcel.readInt();
    this.mModifiers = paramParcel.readInt();
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public Icon getIcon() {
    return this.mIcon;
  }
  
  public int getKeycode() {
    return this.mKeycode;
  }
  
  public char getBaseCharacter() {
    return this.mBaseCharacter;
  }
  
  public int getModifiers() {
    return this.mModifiers;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mLabel);
    paramParcel.writeParcelable((Parcelable)this.mIcon, 0);
    paramParcel.writeInt(this.mBaseCharacter);
    paramParcel.writeInt(this.mKeycode);
    paramParcel.writeInt(this.mModifiers);
  }
  
  public static final Parcelable.Creator<KeyboardShortcutInfo> CREATOR = new Parcelable.Creator<KeyboardShortcutInfo>() {
      public KeyboardShortcutInfo createFromParcel(Parcel param1Parcel) {
        return new KeyboardShortcutInfo(param1Parcel);
      }
      
      public KeyboardShortcutInfo[] newArray(int param1Int) {
        return new KeyboardShortcutInfo[param1Int];
      }
    };
  
  private final char mBaseCharacter;
  
  private final Icon mIcon;
  
  private final int mKeycode;
  
  private final CharSequence mLabel;
  
  private final int mModifiers;
}
