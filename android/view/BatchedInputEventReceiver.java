package android.view;

import android.os.Looper;

public class BatchedInputEventReceiver extends InputEventReceiver {
  private final BatchedInputRunnable mBatchedInputRunnable;
  
  private boolean mBatchedInputScheduled;
  
  Choreographer mChoreographer;
  
  public BatchedInputEventReceiver(InputChannel paramInputChannel, Looper paramLooper, Choreographer paramChoreographer) {
    super(paramInputChannel, paramLooper);
    this.mBatchedInputRunnable = new BatchedInputRunnable();
    this.mChoreographer = paramChoreographer;
  }
  
  public void onBatchedInputEventPending(int paramInt) {
    scheduleBatchedInput();
  }
  
  public void dispose() {
    unscheduleBatchedInput();
    super.dispose();
  }
  
  void doConsumeBatchedInput(long paramLong) {
    if (this.mBatchedInputScheduled) {
      this.mBatchedInputScheduled = false;
      if (consumeBatchedInputEvents(paramLong) && paramLong != -1L)
        scheduleBatchedInput(); 
    } 
  }
  
  private void scheduleBatchedInput() {
    if (!this.mBatchedInputScheduled) {
      this.mBatchedInputScheduled = true;
      this.mChoreographer.postCallback(0, this.mBatchedInputRunnable, null);
    } 
  }
  
  private void unscheduleBatchedInput() {
    if (this.mBatchedInputScheduled) {
      this.mBatchedInputScheduled = false;
      this.mChoreographer.removeCallbacks(0, this.mBatchedInputRunnable, null);
    } 
  }
  
  class BatchedInputRunnable implements Runnable {
    final BatchedInputEventReceiver this$0;
    
    private BatchedInputRunnable() {}
    
    public void run() {
      BatchedInputEventReceiver batchedInputEventReceiver = BatchedInputEventReceiver.this;
      batchedInputEventReceiver.doConsumeBatchedInput(batchedInputEventReceiver.mChoreographer.getFrameTimeNanos());
    }
  }
}
