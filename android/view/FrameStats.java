package android.view;

public abstract class FrameStats {
  public static final long UNDEFINED_TIME_NANO = -1L;
  
  protected long[] mFramesPresentedTimeNano;
  
  protected long mRefreshPeriodNano;
  
  public final long getRefreshPeriodNano() {
    return this.mRefreshPeriodNano;
  }
  
  public final int getFrameCount() {
    boolean bool;
    long[] arrayOfLong = this.mFramesPresentedTimeNano;
    if (arrayOfLong != null) {
      bool = arrayOfLong.length;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final long getStartTimeNano() {
    if (getFrameCount() <= 0)
      return -1L; 
    return this.mFramesPresentedTimeNano[0];
  }
  
  public final long getEndTimeNano() {
    if (getFrameCount() <= 0)
      return -1L; 
    long[] arrayOfLong = this.mFramesPresentedTimeNano;
    return arrayOfLong[arrayOfLong.length - 1];
  }
  
  public final long getFramePresentedTimeNano(int paramInt) {
    long[] arrayOfLong = this.mFramesPresentedTimeNano;
    if (arrayOfLong != null)
      return arrayOfLong[paramInt]; 
    throw new IndexOutOfBoundsException();
  }
}
