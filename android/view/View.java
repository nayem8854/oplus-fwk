package android.view;

import android.animation.StateListAnimator;
import android.app.Activity;
import android.common.ColorFrameworkFactory;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.AutofillOptions;
import android.content.ClipData;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Insets;
import android.graphics.Interpolator;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.OplusBaseRenderNode;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.RenderNode;
import android.graphics.Shader;
import android.graphics.Xfermode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.hardware.display.DisplayManagerGlobal;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Trace;
import android.sysprop.DisplayProperties;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LongSparseLongArray;
import android.util.Pair;
import android.util.Pools;
import android.util.Property;
import android.util.SeempLog;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.StateSet;
import android.util.SuperNotCalledException;
import android.util.TypedValue;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityEventSource;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeIdManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillManager;
import android.view.autofill.AutofillValue;
import android.view.contentcapture.ContentCaptureManager;
import android.view.contentcapture.ContentCaptureSession;
import android.view.contentcapture.MainContentCaptureSession;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.IOplusTextViewRTLUtilForUG;
import android.widget.ScrollBarDrawable;
import com.android.internal.R;
import com.android.internal.util.FrameworkStatsLog;
import com.android.internal.view.ScrollCaptureInternal;
import com.android.internal.view.TooltipPopup;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.widget.ScrollBarUtils;
import com.google.android.collect.Lists;
import com.google.android.collect.Maps;
import com.oplus.darkmode.IOplusDarkModeManager;
import com.oplus.orms.OplusResourceManager;
import com.oplus.util.OplusReflectDataUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class View extends OplusBaseView implements Drawable.Callback, KeyEvent.Callback, AccessibilityEventSource {
  public static final int ACCESSIBILITY_CURSOR_POSITION_UNDEFINED = -1;
  
  public static final int ACCESSIBILITY_LIVE_REGION_ASSERTIVE = 2;
  
  static final int ACCESSIBILITY_LIVE_REGION_DEFAULT = 0;
  
  public static final int ACCESSIBILITY_LIVE_REGION_NONE = 0;
  
  public static final int ACCESSIBILITY_LIVE_REGION_POLITE = 1;
  
  static final int ALL_RTL_PROPERTIES_RESOLVED = 1610678816;
  
  public static final Property<View, Float> ALPHA;
  
  public static final int AUTOFILL_FLAG_INCLUDE_NOT_IMPORTANT_VIEWS = 1;
  
  private static final int[] AUTOFILL_HIGHLIGHT_ATTR;
  
  public static final String AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DATE = "creditCardExpirationDate";
  
  public static final String AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DAY = "creditCardExpirationDay";
  
  public static final String AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_MONTH = "creditCardExpirationMonth";
  
  public static final String AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_YEAR = "creditCardExpirationYear";
  
  public static final String AUTOFILL_HINT_CREDIT_CARD_NUMBER = "creditCardNumber";
  
  public static final String AUTOFILL_HINT_CREDIT_CARD_SECURITY_CODE = "creditCardSecurityCode";
  
  public static final String AUTOFILL_HINT_EMAIL_ADDRESS = "emailAddress";
  
  public static final String AUTOFILL_HINT_NAME = "name";
  
  public static final String AUTOFILL_HINT_PASSWORD = "password";
  
  public static final String AUTOFILL_HINT_PHONE = "phone";
  
  public static final String AUTOFILL_HINT_POSTAL_ADDRESS = "postalAddress";
  
  public static final String AUTOFILL_HINT_POSTAL_CODE = "postalCode";
  
  public static final String AUTOFILL_HINT_USERNAME = "username";
  
  private static final String AUTOFILL_LOG_TAG = "View.Autofill";
  
  public static final int AUTOFILL_TYPE_DATE = 4;
  
  public static final int AUTOFILL_TYPE_LIST = 3;
  
  public static final int AUTOFILL_TYPE_NONE = 0;
  
  public static final int AUTOFILL_TYPE_TEXT = 1;
  
  public static final int AUTOFILL_TYPE_TOGGLE = 2;
  
  private static final String BOOST_VIEW_BILIBILI = "com.bilibili.lib.homepage.widget.TabHost";
  
  private static final String BOOST_VIEW_CLASS = "ConversationOverscrollListView";
  
  private static final String BOOST_VIEW_IQIYI = "org.qiyi.basecore.widget";
  
  private static final String BOOST_VIEW_REDPACKET = "com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyNotHookReceiveUI";
  
  private static final String CLASS_NAME_SPECIAL_VIEW_01 = "com.tencent.mm.ui.base.MultiTouchImageView";
  
  static final int CLICKABLE = 16384;
  
  private static final String CONTENT_CAPTURE_LOG_TAG = "View.ContentCapture";
  
  static final int CONTEXT_CLICKABLE = 8388608;
  
  private static final boolean DBG = false;
  
  private static final boolean DEBUG_CONTENT_CAPTURE = false;
  
  static final int DEBUG_CORNERS_COLOR;
  
  static final int DEBUG_CORNERS_SIZE_DIP = 8;
  
  public static boolean DEBUG_DRAW = false;
  
  static final int DISABLED = 32;
  
  public static final int DRAG_FLAG_GLOBAL = 256;
  
  public static final int DRAG_FLAG_GLOBAL_PERSISTABLE_URI_PERMISSION = 64;
  
  public static final int DRAG_FLAG_GLOBAL_PREFIX_URI_PERMISSION = 128;
  
  public static final int DRAG_FLAG_GLOBAL_URI_READ = 1;
  
  public static final int DRAG_FLAG_GLOBAL_URI_WRITE = 2;
  
  public static final int DRAG_FLAG_OPAQUE = 512;
  
  static final int DRAG_MASK = 3;
  
  static final int DRAWING_CACHE_ENABLED = 32768;
  
  @Deprecated
  public static final int DRAWING_CACHE_QUALITY_AUTO = 0;
  
  private static final int[] DRAWING_CACHE_QUALITY_FLAGS;
  
  @Deprecated
  public static final int DRAWING_CACHE_QUALITY_HIGH = 1048576;
  
  @Deprecated
  public static final int DRAWING_CACHE_QUALITY_LOW = 524288;
  
  static final int DRAWING_CACHE_QUALITY_MASK = 1572864;
  
  static final int DRAW_MASK = 128;
  
  static final int DUPLICATE_PARENT_STATE = 4194304;
  
  protected static final int[] EMPTY_STATE_SET;
  
  static final int ENABLED = 0;
  
  protected static final int[] ENABLED_FOCUSED_SELECTED_STATE_SET;
  
  protected static final int[] ENABLED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] ENABLED_FOCUSED_STATE_SET;
  
  protected static final int[] ENABLED_FOCUSED_WINDOW_FOCUSED_STATE_SET;
  
  static final int ENABLED_MASK = 32;
  
  protected static final int[] ENABLED_SELECTED_STATE_SET;
  
  protected static final int[] ENABLED_SELECTED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] ENABLED_STATE_SET;
  
  protected static final int[] ENABLED_WINDOW_FOCUSED_STATE_SET;
  
  static final int FADING_EDGE_HORIZONTAL = 4096;
  
  static final int FADING_EDGE_MASK = 12288;
  
  static final int FADING_EDGE_NONE = 0;
  
  static final int FADING_EDGE_VERTICAL = 8192;
  
  static final int FILTER_TOUCHES_WHEN_OBSCURED = 1024;
  
  public static final int FIND_VIEWS_WITH_ACCESSIBILITY_NODE_PROVIDERS = 4;
  
  public static final int FIND_VIEWS_WITH_CONTENT_DESCRIPTION = 2;
  
  public static final int FIND_VIEWS_WITH_TEXT = 1;
  
  private static final int FITS_SYSTEM_WINDOWS = 2;
  
  public static final int FOCUSABLE = 1;
  
  public static final int FOCUSABLES_ALL = 0;
  
  public static final int FOCUSABLES_TOUCH_MODE = 1;
  
  public static final int FOCUSABLE_AUTO = 16;
  
  static final int FOCUSABLE_IN_TOUCH_MODE = 262144;
  
  private static final int FOCUSABLE_MASK = 17;
  
  protected static final int[] FOCUSED_SELECTED_STATE_SET;
  
  protected static final int[] FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] FOCUSED_STATE_SET;
  
  protected static final int[] FOCUSED_WINDOW_FOCUSED_STATE_SET;
  
  public static final int FOCUS_BACKWARD = 1;
  
  public static final int FOCUS_DOWN = 130;
  
  public static final int FOCUS_FORWARD = 2;
  
  public static final int FOCUS_LEFT = 17;
  
  public static final int FOCUS_RIGHT = 66;
  
  public static final int FOCUS_UP = 33;
  
  private static final String[] FORCE_HARDWARE_LIST;
  
  public static final int GONE = 8;
  
  public static final int HAPTIC_FEEDBACK_ENABLED = 268435456;
  
  public static final int IMPORTANT_FOR_ACCESSIBILITY_AUTO = 0;
  
  static final int IMPORTANT_FOR_ACCESSIBILITY_DEFAULT = 0;
  
  public static final int IMPORTANT_FOR_ACCESSIBILITY_NO = 2;
  
  public static final int IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS = 4;
  
  public static final int IMPORTANT_FOR_ACCESSIBILITY_YES = 1;
  
  public static final int IMPORTANT_FOR_AUTOFILL_AUTO = 0;
  
  public static final int IMPORTANT_FOR_AUTOFILL_NO = 2;
  
  public static final int IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS = 8;
  
  public static final int IMPORTANT_FOR_AUTOFILL_YES = 1;
  
  public static final int IMPORTANT_FOR_AUTOFILL_YES_EXCLUDE_DESCENDANTS = 4;
  
  public static final int IMPORTANT_FOR_CONTENT_CAPTURE_AUTO = 0;
  
  public static final int IMPORTANT_FOR_CONTENT_CAPTURE_NO = 2;
  
  public static final int IMPORTANT_FOR_CONTENT_CAPTURE_NO_EXCLUDE_DESCENDANTS = 8;
  
  public static final int IMPORTANT_FOR_CONTENT_CAPTURE_YES = 1;
  
  public static final int IMPORTANT_FOR_CONTENT_CAPTURE_YES_EXCLUDE_DESCENDANTS = 4;
  
  public static final int INVISIBLE = 4;
  
  public static final int KEEP_SCREEN_ON = 67108864;
  
  public static final int LAST_APP_AUTOFILL_ID = 1073741823;
  
  public static final int LAYER_TYPE_HARDWARE = 2;
  
  public static final int LAYER_TYPE_NONE = 0;
  
  public static final int LAYER_TYPE_SOFTWARE = 1;
  
  private static final int LAYOUT_DIRECTION_DEFAULT = 2;
  
  private static final int[] LAYOUT_DIRECTION_FLAGS;
  
  public static final int LAYOUT_DIRECTION_INHERIT = 2;
  
  public static final int LAYOUT_DIRECTION_LOCALE = 3;
  
  public static final int LAYOUT_DIRECTION_LTR = 0;
  
  static final int LAYOUT_DIRECTION_RESOLVED_DEFAULT = 0;
  
  public static final int LAYOUT_DIRECTION_RTL = 1;
  
  public static final int LAYOUT_DIRECTION_UNDEFINED = -1;
  
  static final int LONG_CLICKABLE = 2097152;
  
  public static final int MEASURED_HEIGHT_STATE_SHIFT = 16;
  
  public static final int MEASURED_SIZE_MASK = 16777215;
  
  public static final int MEASURED_STATE_MASK = -16777216;
  
  public static final int MEASURED_STATE_TOO_SMALL = 16777216;
  
  public static final int NAVIGATION_BAR_TRANSIENT = 134217728;
  
  public static final int NAVIGATION_BAR_TRANSLUCENT = -2147483648;
  
  public static final int NAVIGATION_BAR_TRANSPARENT = 32768;
  
  public static final int NAVIGATION_BAR_UNHIDE = 536870912;
  
  public static final int NOT_FOCUSABLE = 0;
  
  public static final int NO_ID = -1;
  
  static final int OPTIONAL_FITS_SYSTEM_WINDOWS = 2048;
  
  public static final int OVER_SCROLL_ALWAYS = 0;
  
  public static final int OVER_SCROLL_IF_CONTENT_SCROLLS = 1;
  
  public static final int OVER_SCROLL_NEVER = 2;
  
  static final int PARENT_SAVE_DISABLED = 536870912;
  
  static final int PARENT_SAVE_DISABLED_MASK = 536870912;
  
  static final int PFLAG2_ACCESSIBILITY_FOCUSED = 67108864;
  
  static final int PFLAG2_ACCESSIBILITY_LIVE_REGION_MASK = 25165824;
  
  static final int PFLAG2_ACCESSIBILITY_LIVE_REGION_SHIFT = 23;
  
  static final int PFLAG2_DRAG_CAN_ACCEPT = 1;
  
  static final int PFLAG2_DRAG_HOVERED = 2;
  
  static final int PFLAG2_DRAWABLE_RESOLVED = 1073741824;
  
  static final int PFLAG2_HAS_TRANSIENT_STATE = -2147483648;
  
  static final int PFLAG2_IMPORTANT_FOR_ACCESSIBILITY_MASK = 7340032;
  
  static final int PFLAG2_IMPORTANT_FOR_ACCESSIBILITY_SHIFT = 20;
  
  static final int PFLAG2_LAYOUT_DIRECTION_MASK = 12;
  
  static final int PFLAG2_LAYOUT_DIRECTION_MASK_SHIFT = 2;
  
  static final int PFLAG2_LAYOUT_DIRECTION_RESOLVED = 32;
  
  static final int PFLAG2_LAYOUT_DIRECTION_RESOLVED_MASK = 48;
  
  static final int PFLAG2_LAYOUT_DIRECTION_RESOLVED_RTL = 16;
  
  static final int PFLAG2_PADDING_RESOLVED = 536870912;
  
  static final int PFLAG2_SUBTREE_ACCESSIBILITY_STATE_CHANGED = 134217728;
  
  private static final int[] PFLAG2_TEXT_ALIGNMENT_FLAGS;
  
  static final int PFLAG2_TEXT_ALIGNMENT_MASK = 57344;
  
  static final int PFLAG2_TEXT_ALIGNMENT_MASK_SHIFT = 13;
  
  static final int PFLAG2_TEXT_ALIGNMENT_RESOLVED = 65536;
  
  private static final int PFLAG2_TEXT_ALIGNMENT_RESOLVED_DEFAULT = 131072;
  
  static final int PFLAG2_TEXT_ALIGNMENT_RESOLVED_MASK = 917504;
  
  static final int PFLAG2_TEXT_ALIGNMENT_RESOLVED_MASK_SHIFT = 17;
  
  private static final int[] PFLAG2_TEXT_DIRECTION_FLAGS;
  
  static final int PFLAG2_TEXT_DIRECTION_MASK = 448;
  
  static final int PFLAG2_TEXT_DIRECTION_MASK_SHIFT = 6;
  
  static final int PFLAG2_TEXT_DIRECTION_RESOLVED = 512;
  
  static final int PFLAG2_TEXT_DIRECTION_RESOLVED_DEFAULT = 1024;
  
  static final int PFLAG2_TEXT_DIRECTION_RESOLVED_MASK = 7168;
  
  static final int PFLAG2_TEXT_DIRECTION_RESOLVED_MASK_SHIFT = 10;
  
  static final int PFLAG2_VIEW_QUICK_REJECTED = 268435456;
  
  private static final int PFLAG3_ACCESSIBILITY_HEADING = -2147483648;
  
  private static final int PFLAG3_AGGREGATED_VISIBLE = 536870912;
  
  static final int PFLAG3_APPLYING_INSETS = 32;
  
  static final int PFLAG3_ASSIST_BLOCKED = 16384;
  
  private static final int PFLAG3_AUTOFILLID_EXPLICITLY_SET = 1073741824;
  
  static final int PFLAG3_CALLED_SUPER = 16;
  
  private static final int PFLAG3_CLUSTER = 32768;
  
  private static final int PFLAG3_FINGER_DOWN = 131072;
  
  static final int PFLAG3_FITTING_SYSTEM_WINDOWS = 64;
  
  private static final int PFLAG3_FOCUSED_BY_DEFAULT = 262144;
  
  private static final int PFLAG3_HAS_OVERLAPPING_RENDERING_FORCED = 16777216;
  
  static final int PFLAG3_IMPORTANT_FOR_AUTOFILL_MASK = 7864320;
  
  static final int PFLAG3_IMPORTANT_FOR_AUTOFILL_SHIFT = 19;
  
  private static final int PFLAG3_IS_AUTOFILLED = 65536;
  
  static final int PFLAG3_IS_LAID_OUT = 4;
  
  static final int PFLAG3_MEASURE_NEEDED_BEFORE_LAYOUT = 8;
  
  static final int PFLAG3_NESTED_SCROLLING_ENABLED = 128;
  
  static final int PFLAG3_NOTIFY_AUTOFILL_ENTER_ON_LAYOUT = 134217728;
  
  private static final int PFLAG3_NO_REVEAL_ON_FOCUS = 67108864;
  
  private static final int PFLAG3_OVERLAPPING_RENDERING_FORCED_VALUE = 8388608;
  
  private static final int PFLAG3_SCREEN_READER_FOCUSABLE = 268435456;
  
  static final int PFLAG3_SCROLL_INDICATOR_BOTTOM = 512;
  
  static final int PFLAG3_SCROLL_INDICATOR_END = 8192;
  
  static final int PFLAG3_SCROLL_INDICATOR_LEFT = 1024;
  
  static final int PFLAG3_SCROLL_INDICATOR_RIGHT = 2048;
  
  static final int PFLAG3_SCROLL_INDICATOR_START = 4096;
  
  static final int PFLAG3_SCROLL_INDICATOR_TOP = 256;
  
  static final int PFLAG3_TEMPORARY_DETACH = 33554432;
  
  static final int PFLAG3_VIEW_IS_ANIMATING_ALPHA = 2;
  
  static final int PFLAG3_VIEW_IS_ANIMATING_TRANSFORM = 1;
  
  private static final int PFLAG4_AUTOFILL_HIDE_HIGHLIGHT = 512;
  
  private static final int PFLAG4_CONTENT_CAPTURE_IMPORTANCE_CACHED_VALUE = 128;
  
  private static final int PFLAG4_CONTENT_CAPTURE_IMPORTANCE_IS_CACHED = 64;
  
  private static final int PFLAG4_CONTENT_CAPTURE_IMPORTANCE_MASK = 192;
  
  static final int PFLAG4_FRAMEWORK_OPTIONAL_FITS_SYSTEM_WINDOWS = 256;
  
  private static final int PFLAG4_IMPORTANT_FOR_CONTENT_CAPTURE_MASK = 15;
  
  private static final int PFLAG4_NOTIFIED_CONTENT_CAPTURE_APPEARED = 16;
  
  private static final int PFLAG4_NOTIFIED_CONTENT_CAPTURE_DISAPPEARED = 32;
  
  static final int PFLAG4_SCROLL_CAPTURE_HINT_MASK = 7168;
  
  static final int PFLAG4_SCROLL_CAPTURE_HINT_SHIFT = 10;
  
  static final int PFLAG_ACTIVATED = 1073741824;
  
  static final int PFLAG_ALPHA_SET = 262144;
  
  static final int PFLAG_ANIMATION_STARTED = 65536;
  
  private static final int PFLAG_AWAKEN_SCROLL_BARS_ON_ATTACH = 134217728;
  
  static final int PFLAG_CANCEL_NEXT_UP_EVENT = 67108864;
  
  static final int PFLAG_DIRTY = 2097152;
  
  static final int PFLAG_DIRTY_MASK = 2097152;
  
  static final int PFLAG_DRAWABLE_STATE_DIRTY = 1024;
  
  static final int PFLAG_DRAWING_CACHE_VALID = 32768;
  
  static final int PFLAG_DRAWN = 32;
  
  static final int PFLAG_DRAW_ANIMATION = 64;
  
  static final int PFLAG_FOCUSED = 2;
  
  static final int PFLAG_FORCE_LAYOUT = 4096;
  
  static final int PFLAG_HAS_BOUNDS = 16;
  
  private static final int PFLAG_HOVERED = 268435456;
  
  static final int PFLAG_INVALIDATED = -2147483648;
  
  static final int PFLAG_IS_ROOT_NAMESPACE = 8;
  
  static final int PFLAG_LAYOUT_REQUIRED = 8192;
  
  static final int PFLAG_MEASURED_DIMENSION_SET = 2048;
  
  private static final int PFLAG_NOTIFY_AUTOFILL_MANAGER_ON_CLICK = 536870912;
  
  static final int PFLAG_OPAQUE_BACKGROUND = 8388608;
  
  static final int PFLAG_OPAQUE_MASK = 25165824;
  
  static final int PFLAG_OPAQUE_SCROLLBARS = 16777216;
  
  private static final int PFLAG_PREPRESSED = 33554432;
  
  private static final int PFLAG_PRESSED = 16384;
  
  static final int PFLAG_REQUEST_TRANSPARENT_REGIONS = 512;
  
  private static final int PFLAG_SAVE_STATE_CALLED = 131072;
  
  static final int PFLAG_SCROLL_CONTAINER = 524288;
  
  static final int PFLAG_SCROLL_CONTAINER_ADDED = 1048576;
  
  static final int PFLAG_SELECTED = 4;
  
  static final int PFLAG_SKIP_DRAW = 128;
  
  static final int PFLAG_WANTS_FOCUS = 1;
  
  private static final int POPULATING_ACCESSIBILITY_EVENT_TYPES = 172479;
  
  protected static final int[] PRESSED_ENABLED_FOCUSED_SELECTED_STATE_SET;
  
  protected static final int[] PRESSED_ENABLED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_ENABLED_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_ENABLED_FOCUSED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_ENABLED_SELECTED_STATE_SET;
  
  protected static final int[] PRESSED_ENABLED_SELECTED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_ENABLED_STATE_SET;
  
  protected static final int[] PRESSED_ENABLED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_FOCUSED_SELECTED_STATE_SET;
  
  protected static final int[] PRESSED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_FOCUSED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_SELECTED_STATE_SET;
  
  protected static final int[] PRESSED_SELECTED_WINDOW_FOCUSED_STATE_SET;
  
  protected static final int[] PRESSED_STATE_SET;
  
  protected static final int[] PRESSED_WINDOW_FOCUSED_STATE_SET;
  
  private static final int PROVIDER_BACKGROUND = 0;
  
  private static final int PROVIDER_BOUNDS = 2;
  
  private static final int PROVIDER_NONE = 1;
  
  private static final int PROVIDER_PADDED_BOUNDS = 3;
  
  public static final int PUBLIC_STATUS_BAR_VISIBILITY_MASK = 16375;
  
  public static final Property<View, Float> ROTATION;
  
  public static final Property<View, Float> ROTATION_X;
  
  public static final Property<View, Float> ROTATION_Y;
  
  static final int SAVE_DISABLED = 65536;
  
  static final int SAVE_DISABLED_MASK = 65536;
  
  public static final Property<View, Float> SCALE_X;
  
  public static final Property<View, Float> SCALE_Y;
  
  public static final int SCREEN_STATE_OFF = 0;
  
  public static final int SCREEN_STATE_ON = 1;
  
  static final int SCROLLBARS_HORIZONTAL = 256;
  
  static final int SCROLLBARS_INSET_MASK = 16777216;
  
  public static final int SCROLLBARS_INSIDE_INSET = 16777216;
  
  public static final int SCROLLBARS_INSIDE_OVERLAY = 0;
  
  static final int SCROLLBARS_MASK = 768;
  
  static final int SCROLLBARS_NONE = 0;
  
  public static final int SCROLLBARS_OUTSIDE_INSET = 50331648;
  
  static final int SCROLLBARS_OUTSIDE_MASK = 33554432;
  
  public static final int SCROLLBARS_OUTSIDE_OVERLAY = 33554432;
  
  static final int SCROLLBARS_STYLE_MASK = 50331648;
  
  static final int SCROLLBARS_VERTICAL = 512;
  
  public static final int SCROLLBAR_POSITION_DEFAULT = 0;
  
  public static final int SCROLLBAR_POSITION_LEFT = 1;
  
  public static final int SCROLLBAR_POSITION_RIGHT = 2;
  
  public static final int SCROLL_AXIS_HORIZONTAL = 1;
  
  public static final int SCROLL_AXIS_NONE = 0;
  
  public static final int SCROLL_AXIS_VERTICAL = 2;
  
  public static final int SCROLL_CAPTURE_HINT_AUTO = 0;
  
  public static final int SCROLL_CAPTURE_HINT_EXCLUDE = 1;
  
  public static final int SCROLL_CAPTURE_HINT_EXCLUDE_DESCENDANTS = 4;
  
  public static final int SCROLL_CAPTURE_HINT_INCLUDE = 2;
  
  static final int SCROLL_INDICATORS_NONE = 0;
  
  static final int SCROLL_INDICATORS_PFLAG3_MASK = 16128;
  
  static final int SCROLL_INDICATORS_TO_PFLAGS3_LSHIFT = 8;
  
  public static final int SCROLL_INDICATOR_BOTTOM = 2;
  
  public static final int SCROLL_INDICATOR_END = 32;
  
  public static final int SCROLL_INDICATOR_LEFT = 4;
  
  public static final int SCROLL_INDICATOR_RIGHT = 8;
  
  public static final int SCROLL_INDICATOR_START = 16;
  
  public static final int SCROLL_INDICATOR_TOP = 1;
  
  protected static final int[] SELECTED_STATE_SET;
  
  protected static final int[] SELECTED_WINDOW_FOCUSED_STATE_SET;
  
  public static final int SOUND_EFFECTS_ENABLED = 134217728;
  
  public static final int STATUS_BAR_DISABLE_BACK = 4194304;
  
  public static final int STATUS_BAR_DISABLE_CLOCK = 8388608;
  
  public static final int STATUS_BAR_DISABLE_EXPAND = 65536;
  
  public static final int STATUS_BAR_DISABLE_HOME = 2097152;
  
  public static final int STATUS_BAR_DISABLE_NOTIFICATION_ALERTS = 262144;
  
  public static final int STATUS_BAR_DISABLE_NOTIFICATION_ICONS = 131072;
  
  public static final int STATUS_BAR_DISABLE_NOTIFICATION_TICKER = 524288;
  
  public static final int STATUS_BAR_DISABLE_RECENT = 16777216;
  
  public static final int STATUS_BAR_DISABLE_SEARCH = 33554432;
  
  public static final int STATUS_BAR_DISABLE_SYSTEM_INFO = 1048576;
  
  @Deprecated
  public static final int STATUS_BAR_HIDDEN = 1;
  
  public static final int STATUS_BAR_TRANSIENT = 67108864;
  
  public static final int STATUS_BAR_TRANSLUCENT = 1073741824;
  
  public static final int STATUS_BAR_TRANSPARENT = 8;
  
  public static final int STATUS_BAR_UNHIDE = 268435456;
  
  @Deprecated
  public static final int STATUS_BAR_VISIBLE = 0;
  
  public static final int SYSTEM_UI_CLEARABLE_FLAGS = 7;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_FULLSCREEN = 4;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_HIDE_NAVIGATION = 2;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_IMMERSIVE = 2048;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_IMMERSIVE_STICKY = 4096;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN = 1024;
  
  public static final int SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION = 512;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_LAYOUT_STABLE = 256;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR = 16;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_LIGHT_STATUS_BAR = 8192;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_LOW_PROFILE = 1;
  
  @Deprecated
  public static final int SYSTEM_UI_FLAG_VISIBLE = 0;
  
  @Deprecated
  public static final int SYSTEM_UI_LAYOUT_FLAGS = 1536;
  
  private static final int SYSTEM_UI_RESERVED_LEGACY1 = 16384;
  
  private static final int SYSTEM_UI_RESERVED_LEGACY2 = 65536;
  
  public static final int SYSTEM_UI_TRANSPARENT = 32776;
  
  public static final int TEXT_ALIGNMENT_CENTER = 4;
  
  private static final int TEXT_ALIGNMENT_DEFAULT = 1;
  
  public static final int TEXT_ALIGNMENT_GRAVITY = 1;
  
  public static final int TEXT_ALIGNMENT_INHERIT = 0;
  
  static final int TEXT_ALIGNMENT_RESOLVED_DEFAULT = 1;
  
  public static final int TEXT_ALIGNMENT_TEXT_END = 3;
  
  public static final int TEXT_ALIGNMENT_TEXT_START = 2;
  
  public static final int TEXT_ALIGNMENT_VIEW_END = 6;
  
  public static final int TEXT_ALIGNMENT_VIEW_START = 5;
  
  public static final int TEXT_DIRECTION_ANY_RTL = 2;
  
  private static final int TEXT_DIRECTION_DEFAULT = 0;
  
  public static final int TEXT_DIRECTION_FIRST_STRONG = 1;
  
  public static final int TEXT_DIRECTION_FIRST_STRONG_LTR = 6;
  
  public static final int TEXT_DIRECTION_FIRST_STRONG_RTL = 7;
  
  public static final int TEXT_DIRECTION_INHERIT = 0;
  
  public static final int TEXT_DIRECTION_LOCALE = 5;
  
  public static final int TEXT_DIRECTION_LTR = 3;
  
  static final int TEXT_DIRECTION_RESOLVED_DEFAULT = 1;
  
  public static final int TEXT_DIRECTION_RTL = 4;
  
  static final int TOOLTIP = 1073741824;
  
  public static final Property<View, Float> TRANSLATION_X;
  
  public static final Property<View, Float> TRANSLATION_Y;
  
  public static final Property<View, Float> TRANSLATION_Z;
  
  private static final int UNDEFINED_PADDING = -2147483648;
  
  protected static final String VIEW_LOG_TAG = "View";
  
  protected static final int VIEW_STRUCTURE_FOR_ASSIST = 0;
  
  protected static final int VIEW_STRUCTURE_FOR_AUTOFILL = 1;
  
  protected static final int VIEW_STRUCTURE_FOR_CONTENT_CAPTURE = 2;
  
  private static final int[] VISIBILITY_FLAGS;
  
  static final int VISIBILITY_MASK = 12;
  
  public static final int VISIBLE = 0;
  
  static final int WILL_NOT_CACHE_DRAWING = 131072;
  
  static final int WILL_NOT_DRAW = 128;
  
  protected static final int[] WINDOW_FOCUSED_STATE_SET;
  
  public static final Property<View, Float> X;
  
  public static final Property<View, Float> Y;
  
  public static final Property<View, Float> Z;
  
  private static SparseArray<String> mAttributeMap;
  
  private static int mBoostTime;
  
  private static int mDebugVersion;
  
  private static final boolean mDebugWebView;
  
  private static OplusResourceManager mOrmsManager;
  
  private static boolean sAcceptZeroSizeDragShadow;
  
  private static boolean sAlwaysAssignFocus;
  
  private static boolean sAlwaysRemeasureExactly;
  
  private static boolean sAutoFocusableOffUIThreadWontNotifyParents;
  
  static boolean sBrokenInsetsDispatch;
  
  protected static boolean sBrokenWindowBackground;
  
  private static boolean sCanFocusZeroSized;
  
  static boolean sCascadedDragDrop;
  
  private static boolean sCompatibilityDone;
  
  private static Paint sDebugPaint;
  
  public static boolean sDebugViewAttributes = false;
  
  public static String sDebugViewAttributesApplicationPackage;
  
  static boolean sForceLayoutWhenInsetsChanged;
  
  static boolean sHasFocusableExcludeAutoFocusable;
  
  private static boolean sIgnoreMeasureCache;
  
  private static int sNextAccessibilityViewId;
  
  private static final AtomicInteger sNextGeneratedId;
  
  protected static boolean sPreserveMarginParamsInLayoutParamConversion;
  
  private static boolean sShouldCheckTouchBoost;
  
  static boolean sTextureViewIgnoresDrawableSetters;
  
  static final ThreadLocal<Rect> sThreadLocal;
  
  private static boolean sThrowOnInvalidFloatProperties;
  
  private static boolean sUseBrokenMakeMeasureSpec;
  
  private static boolean sUseDefaultFocusHighlight;
  
  static boolean sUseZeroUnspecifiedMeasureSpec;
  
  private int mAccessibilityCursorPosition;
  
  AccessibilityDelegate mAccessibilityDelegate;
  
  private CharSequence mAccessibilityPaneTitle;
  
  private int mAccessibilityTraversalAfterId;
  
  private int mAccessibilityTraversalBeforeId;
  
  private int mAccessibilityViewId;
  
  private float mAmbiguousGestureMultiplier;
  
  private ViewPropertyAnimator mAnimator;
  
  private String mAssignNullStack;
  
  AttachInfo mAttachInfo;
  
  private SparseArray<int[]> mAttributeResolutionStacks;
  
  private SparseIntArray mAttributeSourceResId;
  
  @ExportedProperty(category = "attributes", hasAdjacentMapping = true)
  public String[] mAttributes;
  
  private String[] mAutofillHints;
  
  private AutofillId mAutofillId;
  
  private int mAutofillViewId;
  
  @ExportedProperty(deepExport = true, prefix = "bg_")
  private Drawable mBackground;
  
  RenderNode mBackgroundRenderNode;
  
  private int mBackgroundResource;
  
  private boolean mBackgroundSizeChanged;
  
  private TintInfo mBackgroundTint;
  
  @ExportedProperty(category = "layout")
  protected int mBottom;
  
  public boolean mCachingFailed;
  
  @ExportedProperty(category = "drawing")
  Rect mClipBounds;
  
  private ContentCaptureSession mContentCaptureSession;
  
  private boolean mContentCaptureSessionCached;
  
  private CharSequence mContentDescription;
  
  @ExportedProperty(deepExport = true)
  protected Context mContext;
  
  protected Animation mCurrentAnimation;
  
  private Drawable mDefaultFocusHighlight;
  
  private Drawable mDefaultFocusHighlightCache;
  
  boolean mDefaultFocusHighlightEnabled;
  
  private boolean mDefaultFocusHighlightSizeChanged;
  
  private int[] mDrawableState;
  
  private Bitmap mDrawingCache;
  
  private int mDrawingCacheBackgroundColor;
  
  private int mExplicitStyle;
  
  private Field mFieldText;
  
  private ViewTreeObserver mFloatingTreeObserver;
  
  int mForceHWSize;
  
  @ExportedProperty(deepExport = true, prefix = "fg_")
  private ForegroundInfo mForegroundInfo;
  
  private ArrayList<FrameMetricsObserver> mFrameMetricsObservers;
  
  GhostView mGhostView;
  
  private boolean mHasInit;
  
  private boolean mHasPerformedLongPress;
  
  private boolean mHoveringTouchDelegate;
  
  @ExportedProperty(resolveId = true)
  int mID;
  
  private boolean mIgnoreNextUpEvent;
  
  public boolean mIgnoreOnDescendantInvalidated;
  
  private boolean mInContextButtonPress;
  
  protected final InputEventConsistencyVerifier mInputEventConsistencyVerifier;
  
  private boolean mIsPreScrollConsumed;
  
  private SparseArray<Object> mKeyedTags;
  
  private int mLabelForId;
  
  private boolean mLastIsOpaque;
  
  Paint mLayerPaint;
  
  @ExportedProperty(category = "drawing", mapping = {@IntToString(from = 0, to = "NONE"), @IntToString(from = 1, to = "SOFTWARE"), @IntToString(from = 2, to = "HARDWARE")})
  int mLayerType;
  
  private Insets mLayoutInsets;
  
  protected ViewGroup.LayoutParams mLayoutParams;
  
  @ExportedProperty(category = "layout")
  protected int mLeft;
  
  private boolean mLeftPaddingDefined;
  
  ListenerInfo mListenerInfo;
  
  private float mLongClickX;
  
  private float mLongClickY;
  
  private MatchIdPredicate mMatchIdPredicate;
  
  private MatchLabelForPredicate mMatchLabelForPredicate;
  
  private LongSparseLongArray mMeasureCache;
  
  @ExportedProperty(category = "measurement")
  int mMeasuredHeight;
  
  @ExportedProperty(category = "measurement")
  int mMeasuredWidth;
  
  @ExportedProperty(category = "measurement")
  private int mMinHeight;
  
  @ExportedProperty(category = "measurement")
  private int mMinWidth;
  
  private ViewParent mNestedScrollingParent;
  
  int mNextClusterForwardId;
  
  private int mNextFocusDownId;
  
  int mNextFocusForwardId;
  
  private int mNextFocusLeftId;
  
  private int mNextFocusRightId;
  
  private int mNextFocusUpId;
  
  int mOldHeightMeasureSpec;
  
  int mOldWidthMeasureSpec;
  
  ViewOutlineProvider mOutlineProvider;
  
  private int mOverScrollMode;
  
  ViewOverlay mOverlay;
  
  @ExportedProperty(category = "padding")
  protected int mPaddingBottom;
  
  @ExportedProperty(category = "padding")
  protected int mPaddingLeft;
  
  @ExportedProperty(category = "padding")
  protected int mPaddingRight;
  
  @ExportedProperty(category = "padding")
  protected int mPaddingTop;
  
  protected ViewParent mParent;
  
  private CheckForLongPress mPendingCheckForLongPress;
  
  private CheckForTap mPendingCheckForTap;
  
  private int mPendingLayerType;
  
  private PerformClick mPerformClick;
  
  private PointerIcon mPointerIcon;
  
  @ExportedProperty(flagMapping = {@FlagToString(equals = 4096, mask = 4096, name = "FORCE_LAYOUT"), @FlagToString(equals = 8192, mask = 8192, name = "LAYOUT_REQUIRED"), @FlagToString(equals = 32768, mask = 32768, name = "DRAWING_CACHE_INVALID", outputIf = false), @FlagToString(equals = 32, mask = 32, name = "DRAWN", outputIf = true), @FlagToString(equals = 32, mask = 32, name = "NOT_DRAWN", outputIf = false), @FlagToString(equals = 2097152, mask = 2097152, name = "DIRTY")}, formatToHexString = true)
  public int mPrivateFlags;
  
  int mPrivateFlags2;
  
  int mPrivateFlags3;
  
  private int mPrivateFlags4;
  
  boolean mRecreateDisplayList;
  
  final RenderNode mRenderNode;
  
  private final Resources mResources;
  
  @ExportedProperty(category = "layout")
  protected int mRight;
  
  private boolean mRightPaddingDefined;
  
  private RoundScrollbarRenderer mRoundScrollbarRenderer;
  
  private HandlerActionQueue mRunQueue;
  
  private ScrollabilityCache mScrollCache;
  
  private Drawable mScrollIndicatorDrawable;
  
  @ExportedProperty(category = "scrolling")
  protected int mScrollX;
  
  @ExportedProperty(category = "scrolling")
  protected int mScrollY;
  
  private SendAccessibilityEventThrottle mSendStateChangedAccessibilityEvent;
  
  private SendViewScrolledAccessibilityEvent mSendViewScrolledAccessibilityEvent;
  
  private boolean mSendingHoverAccessibilityEvents;
  
  private int mSourceLayoutId;
  
  String mStartActivityRequestWho;
  
  private CharSequence mStateDescription;
  
  private StateListAnimator mStateListAnimator;
  
  @ExportedProperty(flagMapping = {@FlagToString(equals = 1, mask = 1, name = "LOW_PROFILE"), @FlagToString(equals = 2, mask = 2, name = "HIDE_NAVIGATION"), @FlagToString(equals = 4, mask = 4, name = "FULLSCREEN"), @FlagToString(equals = 256, mask = 256, name = "LAYOUT_STABLE"), @FlagToString(equals = 512, mask = 512, name = "LAYOUT_HIDE_NAVIGATION"), @FlagToString(equals = 1024, mask = 1024, name = "LAYOUT_FULLSCREEN"), @FlagToString(equals = 2048, mask = 2048, name = "IMMERSIVE"), @FlagToString(equals = 4096, mask = 4096, name = "IMMERSIVE_STICKY"), @FlagToString(equals = 8192, mask = 8192, name = "LIGHT_STATUS_BAR"), @FlagToString(equals = 16, mask = 16, name = "LIGHT_NAVIGATION_BAR"), @FlagToString(equals = 65536, mask = 65536, name = "STATUS_BAR_DISABLE_EXPAND"), @FlagToString(equals = 131072, mask = 131072, name = "STATUS_BAR_DISABLE_NOTIFICATION_ICONS"), @FlagToString(equals = 262144, mask = 262144, name = "STATUS_BAR_DISABLE_NOTIFICATION_ALERTS"), @FlagToString(equals = 524288, mask = 524288, name = "STATUS_BAR_DISABLE_NOTIFICATION_TICKER"), @FlagToString(equals = 1048576, mask = 1048576, name = "STATUS_BAR_DISABLE_SYSTEM_INFO"), @FlagToString(equals = 2097152, mask = 2097152, name = "STATUS_BAR_DISABLE_HOME"), @FlagToString(equals = 4194304, mask = 4194304, name = "STATUS_BAR_DISABLE_BACK"), @FlagToString(equals = 8388608, mask = 8388608, name = "STATUS_BAR_DISABLE_CLOCK"), @FlagToString(equals = 16777216, mask = 16777216, name = "STATUS_BAR_DISABLE_RECENT"), @FlagToString(equals = 33554432, mask = 33554432, name = "STATUS_BAR_DISABLE_SEARCH"), @FlagToString(equals = 67108864, mask = 67108864, name = "STATUS_BAR_TRANSIENT"), @FlagToString(equals = 134217728, mask = 134217728, name = "NAVIGATION_BAR_TRANSIENT"), @FlagToString(equals = 268435456, mask = 268435456, name = "STATUS_BAR_UNHIDE"), @FlagToString(equals = 536870912, mask = 536870912, name = "NAVIGATION_BAR_UNHIDE"), @FlagToString(equals = 1073741824, mask = 1073741824, name = "STATUS_BAR_TRANSLUCENT"), @FlagToString(equals = -2147483648, mask = -2147483648, name = "NAVIGATION_BAR_TRANSLUCENT"), @FlagToString(equals = 32768, mask = 32768, name = "NAVIGATION_BAR_TRANSPARENT"), @FlagToString(equals = 8, mask = 8, name = "STATUS_BAR_TRANSPARENT")}, formatToHexString = true)
  int mSystemUiVisibility;
  
  protected Object mTag;
  
  private int[] mTempNestedScrollConsumed;
  
  TooltipInfo mTooltipInfo;
  
  @ExportedProperty(category = "layout")
  protected int mTop;
  
  private TouchDelegate mTouchDelegate;
  
  private int mTouchSlop;
  
  public TransformationInfo mTransformationInfo;
  
  int mTransientStateCount;
  
  private String mTransitionName;
  
  int mUnbufferedInputSource;
  
  private Bitmap mUnscaledDrawingCache;
  
  private UnsetPressedState mUnsetPressedState;
  
  @ExportedProperty(category = "padding")
  protected int mUserPaddingBottom;
  
  @ExportedProperty(category = "padding")
  int mUserPaddingEnd;
  
  @ExportedProperty(category = "padding")
  protected int mUserPaddingLeft;
  
  int mUserPaddingLeftInitial;
  
  @ExportedProperty(category = "padding")
  protected int mUserPaddingRight;
  
  int mUserPaddingRightInitial;
  
  @ExportedProperty(category = "padding")
  int mUserPaddingStart;
  
  private float mVerticalScrollFactor;
  
  private int mVerticalScrollbarPosition;
  
  @ExportedProperty(formatToHexString = true)
  int mViewFlags;
  
  private Handler mVisibilityChangeForAutofillHandler;
  
  int mWindowAttachCount;
  
  static {
    mBoostTime = SystemProperties.getInt("persist.sys.weixin.launchuiboost", 300);
    AUTOFILL_HIGHLIGHT_ATTR = new int[] { 16844136 };
    mDebugWebView = SystemProperties.getBoolean("persist.sys.view.debug_webview", false);
    sCompatibilityDone = false;
    sUseBrokenMakeMeasureSpec = false;
    sUseZeroUnspecifiedMeasureSpec = false;
    sIgnoreMeasureCache = false;
    sAlwaysRemeasureExactly = false;
    sTextureViewIgnoresDrawableSetters = false;
    VISIBILITY_FLAGS = new int[] { 0, 4, 8 };
    DRAWING_CACHE_QUALITY_FLAGS = new int[] { 0, 524288, 1048576 };
    EMPTY_STATE_SET = StateSet.get(0);
    WINDOW_FOCUSED_STATE_SET = StateSet.get(1);
    SELECTED_STATE_SET = StateSet.get(2);
    SELECTED_WINDOW_FOCUSED_STATE_SET = StateSet.get(3);
    FOCUSED_STATE_SET = StateSet.get(4);
    FOCUSED_WINDOW_FOCUSED_STATE_SET = StateSet.get(5);
    FOCUSED_SELECTED_STATE_SET = StateSet.get(6);
    FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET = StateSet.get(7);
    ENABLED_STATE_SET = StateSet.get(8);
    ENABLED_WINDOW_FOCUSED_STATE_SET = StateSet.get(9);
    ENABLED_SELECTED_STATE_SET = StateSet.get(10);
    ENABLED_SELECTED_WINDOW_FOCUSED_STATE_SET = StateSet.get(11);
    ENABLED_FOCUSED_STATE_SET = StateSet.get(12);
    ENABLED_FOCUSED_WINDOW_FOCUSED_STATE_SET = StateSet.get(13);
    ENABLED_FOCUSED_SELECTED_STATE_SET = StateSet.get(14);
    ENABLED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET = StateSet.get(15);
    PRESSED_STATE_SET = StateSet.get(16);
    PRESSED_WINDOW_FOCUSED_STATE_SET = StateSet.get(17);
    PRESSED_SELECTED_STATE_SET = StateSet.get(18);
    PRESSED_SELECTED_WINDOW_FOCUSED_STATE_SET = StateSet.get(19);
    PRESSED_FOCUSED_STATE_SET = StateSet.get(20);
    PRESSED_FOCUSED_WINDOW_FOCUSED_STATE_SET = StateSet.get(21);
    PRESSED_FOCUSED_SELECTED_STATE_SET = StateSet.get(22);
    PRESSED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET = StateSet.get(23);
    PRESSED_ENABLED_STATE_SET = StateSet.get(24);
    PRESSED_ENABLED_WINDOW_FOCUSED_STATE_SET = StateSet.get(25);
    PRESSED_ENABLED_SELECTED_STATE_SET = StateSet.get(26);
    PRESSED_ENABLED_SELECTED_WINDOW_FOCUSED_STATE_SET = StateSet.get(27);
    PRESSED_ENABLED_FOCUSED_STATE_SET = StateSet.get(28);
    PRESSED_ENABLED_FOCUSED_WINDOW_FOCUSED_STATE_SET = StateSet.get(29);
    PRESSED_ENABLED_FOCUSED_SELECTED_STATE_SET = StateSet.get(30);
    PRESSED_ENABLED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET = StateSet.get(31);
    DEBUG_CORNERS_COLOR = Color.rgb(63, 127, 255);
    sThreadLocal = ThreadLocal.withInitial((Supplier<? extends Rect>)_$$Lambda$Y3lG3v_J32_xL0IjMGgNorZjESw.INSTANCE);
    LAYOUT_DIRECTION_FLAGS = new int[] { 0, 1, 2, 3 };
    PFLAG2_TEXT_DIRECTION_FLAGS = new int[] { 0, 64, 128, 192, 256, 320, 384, 448 };
    PFLAG2_TEXT_ALIGNMENT_FLAGS = new int[] { 0, 8192, 16384, 24576, 32768, 40960, 49152 };
    sNextGeneratedId = new AtomicInteger(1);
    mOrmsManager = null;
    sShouldCheckTouchBoost = false;
    mDebugVersion = -1;
    ALPHA = (Property<View, Float>)new Object("alpha");
    TRANSLATION_X = (Property<View, Float>)new Object("translationX");
    TRANSLATION_Y = (Property<View, Float>)new Object("translationY");
    TRANSLATION_Z = (Property<View, Float>)new Object("translationZ");
    X = (Property<View, Float>)new Object("x");
    Y = (Property<View, Float>)new Object("y");
    Z = (Property<View, Float>)new Object("z");
    ROTATION = (Property<View, Float>)new Object("rotation");
    ROTATION_X = (Property<View, Float>)new Object("rotationX");
    ROTATION_Y = (Property<View, Float>)new Object("rotationY");
    SCALE_X = (Property<View, Float>)new Object("scaleX");
    SCALE_Y = (Property<View, Float>)new Object("scaleY");
    FORCE_HARDWARE_LIST = new String[] { "com.tencent.mm/com.tencent.mm.ui.chatting.gallery.ImageGalleryUI", "com.tencent.mm/com.tencent.mm.plugin.sns.ui.SnsBrowseUI", "com.tencent.mm/com.tencent.mm.plugin.sns.ui.SnsUploadBrowseUI" };
  }
  
  class TransformationInfo {
    private final Matrix mMatrix = new Matrix();
    
    @ExportedProperty
    private float mAlpha = 1.0F;
    
    float mTransitionAlpha = 1.0F;
    
    private Matrix mInverseMatrix;
  }
  
  class TintInfo {
    BlendMode mBlendMode;
    
    boolean mHasTintList;
    
    boolean mHasTintMode;
    
    ColorStateList mTintList;
  }
  
  class ForegroundInfo {
    private ForegroundInfo() {}
    
    private int mGravity = 119;
    
    private boolean mInsidePadding = true;
    
    private boolean mBoundsChanged = true;
    
    private final Rect mSelfBounds = new Rect();
    
    private final Rect mOverlayBounds = new Rect();
    
    private Drawable mDrawable;
    
    private View.TintInfo mTintInfo;
  }
  
  class ListenerInfo {
    View.OnApplyWindowInsetsListener mOnApplyWindowInsetsListener;
    
    private CopyOnWriteArrayList<View.OnAttachStateChangeListener> mOnAttachStateChangeListeners;
    
    View.OnCapturedPointerListener mOnCapturedPointerListener;
    
    public View.OnClickListener mOnClickListener;
    
    protected View.OnContextClickListener mOnContextClickListener;
    
    protected View.OnCreateContextMenuListener mOnCreateContextMenuListener;
    
    private View.OnDragListener mOnDragListener;
    
    protected View.OnFocusChangeListener mOnFocusChangeListener;
    
    private View.OnGenericMotionListener mOnGenericMotionListener;
    
    private View.OnHoverListener mOnHoverListener;
    
    private View.OnKeyListener mOnKeyListener;
    
    private ArrayList<View.OnLayoutChangeListener> mOnLayoutChangeListeners;
    
    protected View.OnLongClickListener mOnLongClickListener;
    
    protected View.OnScrollChangeListener mOnScrollChangeListener;
    
    private View.OnSystemUiVisibilityChangeListener mOnSystemUiVisibilityChangeListener;
    
    private View.OnTouchListener mOnTouchListener;
    
    public RenderNode.PositionUpdateListener mPositionUpdateListener;
    
    ScrollCaptureCallback mScrollCaptureCallback;
    
    private List<Rect> mSystemGestureExclusionRects;
    
    private ArrayList<View.OnUnhandledKeyEventListener> mUnhandledKeyListeners;
    
    WindowInsetsAnimation.Callback mWindowInsetsAnimationCallback;
  }
  
  class TooltipInfo {
    int mAnchorX;
    
    int mAnchorY;
    
    Runnable mHideTooltipRunnable;
    
    int mHoverSlop;
    
    Runnable mShowTooltipRunnable;
    
    boolean mTooltipFromLongClick;
    
    TooltipPopup mTooltipPopup;
    
    CharSequence mTooltipText;
    
    private TooltipInfo() {}
    
    private boolean updateAnchorPos(MotionEvent param1MotionEvent) {
      int i = (int)param1MotionEvent.getX();
      int j = (int)param1MotionEvent.getY();
      if (Math.abs(i - this.mAnchorX) <= this.mHoverSlop) {
        int k = this.mAnchorY;
        if (Math.abs(j - k) <= this.mHoverSlop)
          return false; 
      } 
      this.mAnchorX = i;
      this.mAnchorY = j;
      return true;
    }
    
    private void clearAnchorPos() {
      this.mAnchorX = Integer.MAX_VALUE;
      this.mAnchorY = Integer.MAX_VALUE;
    }
  }
  
  public View(Context paramContext) {
    Resources resources;
    boolean bool = false;
    this.mIgnoreOnDescendantInvalidated = false;
    InputEventConsistencyVerifier inputEventConsistencyVerifier1 = null;
    this.mCurrentAnimation = null;
    this.mRecreateDisplayList = false;
    this.mID = -1;
    this.mAutofillViewId = -1;
    this.mAccessibilityViewId = -1;
    this.mAccessibilityCursorPosition = -1;
    this.mTag = null;
    this.mTransientStateCount = 0;
    this.mClipBounds = null;
    this.mPaddingLeft = 0;
    this.mPaddingRight = 0;
    this.mLabelForId = -1;
    this.mAccessibilityTraversalBeforeId = -1;
    this.mAccessibilityTraversalAfterId = -1;
    this.mLeftPaddingDefined = false;
    this.mRightPaddingDefined = false;
    this.mOldWidthMeasureSpec = Integer.MIN_VALUE;
    this.mOldHeightMeasureSpec = Integer.MIN_VALUE;
    this.mLongClickX = Float.NaN;
    this.mLongClickY = Float.NaN;
    this.mDrawableState = null;
    this.mOutlineProvider = ViewOutlineProvider.BACKGROUND;
    this.mNextFocusLeftId = -1;
    this.mNextFocusRightId = -1;
    this.mNextFocusUpId = -1;
    this.mNextFocusDownId = -1;
    this.mNextFocusForwardId = -1;
    this.mNextClusterForwardId = -1;
    this.mDefaultFocusHighlightEnabled = true;
    this.mPendingCheckForTap = null;
    this.mTouchDelegate = null;
    this.mHoveringTouchDelegate = false;
    this.mDrawingCacheBackgroundColor = 0;
    this.mAnimator = null;
    this.mLayerType = 0;
    if (InputEventConsistencyVerifier.isInstrumentationEnabled()) {
      inputEventConsistencyVerifier2 = new InputEventConsistencyVerifier(this, 0);
    } else {
      inputEventConsistencyVerifier2 = null;
    } 
    this.mInputEventConsistencyVerifier = inputEventConsistencyVerifier2;
    this.mSourceLayoutId = 0;
    this.mIsPreScrollConsumed = false;
    this.mUnbufferedInputSource = 0;
    this.mHasInit = false;
    this.mPendingLayerType = 0;
    this.mForceHWSize = SystemProperties.getInt("debug.imageview.forcehw.size", 16777216);
    this.mContext = paramContext;
    InputEventConsistencyVerifier inputEventConsistencyVerifier2 = inputEventConsistencyVerifier1;
    if (paramContext != null)
      resources = paramContext.getResources(); 
    this.mResources = resources;
    this.mViewHooks = (IOplusViewHooks)ColorFrameworkFactory.getInstance().getFeature(IOplusViewHooks.DEFAULT, new Object[] { this, this.mResources });
    this.mViewFlags = 402653200;
    this.mPrivateFlags2 = 140296;
    ViewConfiguration viewConfiguration = ViewConfiguration.get(paramContext);
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    this.mAmbiguousGestureMultiplier = viewConfiguration.getScaledAmbiguousGestureMultiplier();
    setOverScrollMode(1);
    this.mUserPaddingStart = Integer.MIN_VALUE;
    this.mUserPaddingEnd = Integer.MIN_VALUE;
    this.mRenderNode = RenderNode.create(getClass().getName(), new ViewAnimationHostBridge(this));
    if (!sCompatibilityDone && paramContext != null) {
      int i = (paramContext.getApplicationInfo()).targetSdkVersion;
      if (i <= 17) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sUseBrokenMakeMeasureSpec = bool1;
      if (i < 19) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sIgnoreMeasureCache = bool1;
      if (i < 23) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      Canvas.sCompatibilityRestore = bool1;
      if (i < 26) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      Canvas.sCompatibilitySetBitmap = bool1;
      Canvas.setCompatibilityVersion(i);
      if (i < 23) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sUseZeroUnspecifiedMeasureSpec = bool1;
      if (i <= 23) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sAlwaysRemeasureExactly = bool1;
      if (i <= 23) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sTextureViewIgnoresDrawableSetters = bool1;
      if (i >= 24) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sPreserveMarginParamsInLayoutParamConversion = bool1;
      if (i < 24) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sCascadedDragDrop = bool1;
      if (i < 26) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sHasFocusableExcludeAutoFocusable = bool1;
      if (i < 26) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sAutoFocusableOffUIThreadWontNotifyParents = bool1;
      sUseDefaultFocusHighlight = paramContext.getResources().getBoolean(17891571);
      if (i >= 28) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sThrowOnInvalidFloatProperties = bool1;
      if (i < 28) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sCanFocusZeroSized = bool1;
      if (i < 28) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sAlwaysAssignFocus = bool1;
      if (i < 28) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sAcceptZeroSizeDragShadow = bool1;
      if (ViewRootImpl.sNewInsetsMode != 2 || i < 30) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sBrokenInsetsDispatch = bool1;
      if (i < 29) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      sBrokenWindowBackground = bool1;
      if (i >= 29) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      GradientDrawable.sWrapNegativeAngleMeasurements = bool1;
      boolean bool1 = bool;
      if (i < 30)
        bool1 = true; 
      sForceLayoutWhenInsetsChanged = bool1;
      sCompatibilityDone = true;
    } 
    if (this.mContext.getPackageName() != null && this.mContext.getPackageName().equals("com.tencent.mm"))
      sShouldCheckTouchBoost = true; 
    String str = getClass().getName();
    if ("com.tencent.mm.ui.base.MultiTouchImageView".equals(str))
      this.mIgnoreOnDescendantInvalidated = true; 
  }
  
  public View(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public View(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public View(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial <init> : (Landroid/content/Context;)V
    //   5: aload_0
    //   6: aload_2
    //   7: invokestatic getAttributeSetSourceResId : (Landroid/util/AttributeSet;)I
    //   10: putfield mSourceLayoutId : I
    //   13: aload_1
    //   14: aload_2
    //   15: getstatic com/android/internal/R$styleable.View : [I
    //   18: iload_3
    //   19: iload #4
    //   21: invokevirtual obtainStyledAttributes : (Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    //   24: astore #5
    //   26: aload_0
    //   27: aload_1
    //   28: invokevirtual getTheme : ()Landroid/content/res/Resources$Theme;
    //   31: aload_2
    //   32: invokespecial retrieveExplicitStyle : (Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;)V
    //   35: aload_0
    //   36: aload_1
    //   37: getstatic com/android/internal/R$styleable.View : [I
    //   40: aload_2
    //   41: aload #5
    //   43: iload_3
    //   44: iload #4
    //   46: invokevirtual saveAttributeDataForStyleable : (Landroid/content/Context;[ILandroid/util/AttributeSet;Landroid/content/res/TypedArray;II)V
    //   49: getstatic android/view/View.sDebugViewAttributes : Z
    //   52: ifeq -> 62
    //   55: aload_0
    //   56: aload_2
    //   57: aload #5
    //   59: invokespecial saveAttributeData : (Landroid/util/AttributeSet;Landroid/content/res/TypedArray;)V
    //   62: iconst_0
    //   63: istore #6
    //   65: iconst_0
    //   66: istore #7
    //   68: aload_0
    //   69: getfield mOverScrollMode : I
    //   72: istore #8
    //   74: iconst_0
    //   75: istore #9
    //   77: iconst_0
    //   78: istore #10
    //   80: iconst_0
    //   81: istore #11
    //   83: iconst_0
    //   84: istore #12
    //   86: aload_1
    //   87: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   90: astore_2
    //   91: iconst_m1
    //   92: istore #13
    //   94: aload_2
    //   95: getfield targetSdkVersion : I
    //   98: istore #14
    //   100: iconst_m1
    //   101: istore #15
    //   103: aload #5
    //   105: invokevirtual getIndexCount : ()I
    //   108: istore #16
    //   110: iconst_0
    //   111: bipush #16
    //   113: ior
    //   114: istore #17
    //   116: fconst_0
    //   117: fstore #18
    //   119: fconst_0
    //   120: fstore #19
    //   122: fconst_0
    //   123: fstore #20
    //   125: fconst_0
    //   126: fstore #21
    //   128: fconst_0
    //   129: fstore #22
    //   131: fconst_0
    //   132: fstore #23
    //   134: fconst_0
    //   135: fstore #24
    //   137: fconst_1
    //   138: fstore #25
    //   140: fconst_1
    //   141: fstore #26
    //   143: iconst_0
    //   144: istore #27
    //   146: iconst_0
    //   147: istore #28
    //   149: iconst_m1
    //   150: istore #29
    //   152: iconst_m1
    //   153: istore #30
    //   155: iconst_0
    //   156: istore #31
    //   158: iconst_0
    //   159: istore #32
    //   161: ldc_w -2147483648
    //   164: istore #33
    //   166: iconst_m1
    //   167: istore #34
    //   169: iconst_0
    //   170: istore #35
    //   172: iconst_0
    //   173: bipush #16
    //   175: ior
    //   176: istore #36
    //   178: iconst_m1
    //   179: istore #37
    //   181: ldc_w -2147483648
    //   184: istore #38
    //   186: iconst_m1
    //   187: istore_3
    //   188: aconst_null
    //   189: astore #39
    //   191: iconst_0
    //   192: istore #40
    //   194: iload #40
    //   196: iload #16
    //   198: if_icmpge -> 3556
    //   201: aload #5
    //   203: iload #40
    //   205: invokevirtual getIndex : (I)I
    //   208: istore #4
    //   210: iload #4
    //   212: tableswitch default -> 640, 8 -> 3458, 9 -> 3443, 10 -> 3429, 11 -> 3401, 12 -> 3373, 13 -> 3350, 14 -> 3307, 15 -> 3270, 16 -> 3242, 17 -> 3205, 18 -> 3177, 19 -> 3124, 20 -> 3084, 21 -> 3041, 22 -> 3008, 23 -> 2965, 24 -> 2909, 25 -> 640, 26 -> 2894, 27 -> 2879, 28 -> 2864, 29 -> 2849, 30 -> 2812, 31 -> 2775, 32 -> 2738, 33 -> 2695, 34 -> 2660, 35 -> 2632, 36 -> 2617, 37 -> 2602, 38 -> 2573, 39 -> 2536, 40 -> 2499, 41 -> 2462, 42 -> 2425, 43 -> 2379, 44 -> 2365, 45 -> 640, 46 -> 640, 47 -> 640, 48 -> 2337, 49 -> 2300, 50 -> 2285, 51 -> 2270, 52 -> 2255, 53 -> 2228, 54 -> 2201, 55 -> 2174, 56 -> 2147, 57 -> 2120, 58 -> 2093, 59 -> 2066, 60 -> 2051, 61 -> 2036, 62 -> 2020, 63 -> 2017, 64 -> 2002, 65 -> 1956, 66 -> 1916, 67 -> 1859, 68 -> 1808, 69 -> 1757, 70 -> 1742, 71 -> 1727, 72 -> 1700, 73 -> 1686, 74 -> 1671, 75 -> 1644, 76 -> 1621, 77 -> 1578, 78 -> 1530, 79 -> 1502, 80 -> 1467, 81 -> 1452, 82 -> 1437, 83 -> 1422, 84 -> 1368, 85 -> 1333, 86 -> 1271, 87 -> 1246, 88 -> 1232, 89 -> 1186, 90 -> 1158, 91 -> 1133, 92 -> 1118, 93 -> 1093, 94 -> 932, 95 -> 904, 96 -> 876, 97 -> 848, 98 -> 821, 99 -> 806, 100 -> 789, 101 -> 772, 102 -> 753, 103 -> 728, 104 -> 640, 105 -> 640, 106 -> 640, 107 -> 640, 108 -> 640, 109 -> 658, 110 -> 643
    //   640: goto -> 3520
    //   643: aload_0
    //   644: aload #5
    //   646: iload #4
    //   648: iconst_0
    //   649: invokevirtual getInt : (II)I
    //   652: invokevirtual setScrollCaptureHint : (I)V
    //   655: goto -> 3520
    //   658: iload #14
    //   660: bipush #23
    //   662: if_icmpge -> 678
    //   665: aload_0
    //   666: instanceof android/widget/FrameLayout
    //   669: ifeq -> 675
    //   672: goto -> 678
    //   675: goto -> 3520
    //   678: aload_0
    //   679: getfield mForegroundInfo : Landroid/view/View$ForegroundInfo;
    //   682: ifnonnull -> 700
    //   685: aload_0
    //   686: new android/view/View$ForegroundInfo
    //   689: dup
    //   690: aconst_null
    //   691: invokespecial <init> : (Landroid/view/View$1;)V
    //   694: putfield mForegroundInfo : Landroid/view/View$ForegroundInfo;
    //   697: goto -> 700
    //   700: aload_0
    //   701: getfield mForegroundInfo : Landroid/view/View$ForegroundInfo;
    //   704: astore_2
    //   705: aload_2
    //   706: invokestatic access$100 : (Landroid/view/View$ForegroundInfo;)Z
    //   709: istore #41
    //   711: aload_2
    //   712: aload #5
    //   714: iload #4
    //   716: iload #41
    //   718: invokevirtual getBoolean : (IZ)Z
    //   721: invokestatic access$102 : (Landroid/view/View$ForegroundInfo;Z)Z
    //   724: pop
    //   725: goto -> 3520
    //   728: aload #5
    //   730: iload #4
    //   732: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   735: ifnull -> 876
    //   738: aload_0
    //   739: aload #5
    //   741: iload #4
    //   743: iconst_0
    //   744: invokevirtual getInt : (II)I
    //   747: invokevirtual setImportantForContentCapture : (I)V
    //   750: goto -> 876
    //   753: aload_0
    //   754: getfield mRenderNode : Landroid/graphics/RenderNode;
    //   757: aload #5
    //   759: iload #4
    //   761: iconst_1
    //   762: invokevirtual getBoolean : (IZ)Z
    //   765: invokevirtual setForceDarkAllowed : (Z)Z
    //   768: pop
    //   769: goto -> 3520
    //   772: aload_0
    //   773: aload #5
    //   775: iload #4
    //   777: ldc_w -16777216
    //   780: invokevirtual getColor : (II)I
    //   783: invokevirtual setOutlineAmbientShadowColor : (I)V
    //   786: goto -> 3520
    //   789: aload_0
    //   790: aload #5
    //   792: iload #4
    //   794: ldc_w -16777216
    //   797: invokevirtual getColor : (II)I
    //   800: invokevirtual setOutlineSpotShadowColor : (I)V
    //   803: goto -> 3520
    //   806: aload_0
    //   807: aload #5
    //   809: iload #4
    //   811: iconst_0
    //   812: invokevirtual getBoolean : (IZ)Z
    //   815: invokevirtual setAccessibilityHeading : (Z)V
    //   818: goto -> 3520
    //   821: aload #5
    //   823: iload #4
    //   825: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   828: ifnull -> 845
    //   831: aload_0
    //   832: aload #5
    //   834: iload #4
    //   836: invokevirtual getString : (I)Ljava/lang/String;
    //   839: invokevirtual setAccessibilityPaneTitle : (Ljava/lang/CharSequence;)V
    //   842: goto -> 3520
    //   845: goto -> 3520
    //   848: aload #5
    //   850: iload #4
    //   852: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   855: ifnull -> 873
    //   858: aload_0
    //   859: aload #5
    //   861: iload #4
    //   863: iconst_0
    //   864: invokevirtual getBoolean : (IZ)Z
    //   867: invokevirtual setScreenReaderFocusable : (Z)V
    //   870: goto -> 3520
    //   873: goto -> 3520
    //   876: aload #5
    //   878: iload #4
    //   880: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   883: ifnull -> 901
    //   886: aload_0
    //   887: aload #5
    //   889: iload #4
    //   891: iconst_1
    //   892: invokevirtual getBoolean : (IZ)Z
    //   895: invokevirtual setDefaultFocusHighlightEnabled : (Z)V
    //   898: goto -> 3520
    //   901: goto -> 3520
    //   904: aload #5
    //   906: iload #4
    //   908: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   911: ifnull -> 929
    //   914: aload_0
    //   915: aload #5
    //   917: iload #4
    //   919: iconst_0
    //   920: invokevirtual getInt : (II)I
    //   923: invokevirtual setImportantForAutofill : (I)V
    //   926: goto -> 3520
    //   929: goto -> 3520
    //   932: aload #5
    //   934: iload #4
    //   936: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   939: ifnull -> 1090
    //   942: aconst_null
    //   943: astore #42
    //   945: aload #5
    //   947: iload #4
    //   949: invokevirtual getType : (I)I
    //   952: iconst_1
    //   953: if_icmpne -> 994
    //   956: aload #5
    //   958: iload #4
    //   960: iconst_0
    //   961: invokevirtual getResourceId : (II)I
    //   964: istore #43
    //   966: aload #5
    //   968: iload #4
    //   970: invokevirtual getTextArray : (I)[Ljava/lang/CharSequence;
    //   973: astore_2
    //   974: goto -> 991
    //   977: astore_2
    //   978: aload_0
    //   979: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   982: iload #43
    //   984: invokevirtual getString : (I)Ljava/lang/String;
    //   987: astore #42
    //   989: aconst_null
    //   990: astore_2
    //   991: goto -> 1005
    //   994: aload #5
    //   996: iload #4
    //   998: invokevirtual getString : (I)Ljava/lang/String;
    //   1001: astore #42
    //   1003: aconst_null
    //   1004: astore_2
    //   1005: aload_2
    //   1006: ifnonnull -> 1037
    //   1009: aload #42
    //   1011: ifnull -> 1026
    //   1014: aload #42
    //   1016: ldc_w ','
    //   1019: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1022: astore_2
    //   1023: goto -> 1037
    //   1026: new java/lang/IllegalArgumentException
    //   1029: dup
    //   1030: ldc_w 'Could not resolve autofillHints'
    //   1033: invokespecial <init> : (Ljava/lang/String;)V
    //   1036: athrow
    //   1037: aload_2
    //   1038: arraylength
    //   1039: anewarray java/lang/String
    //   1042: astore #42
    //   1044: aload_2
    //   1045: arraylength
    //   1046: istore #43
    //   1048: iconst_0
    //   1049: istore #4
    //   1051: iload #4
    //   1053: iload #43
    //   1055: if_icmpge -> 1081
    //   1058: aload #42
    //   1060: iload #4
    //   1062: aload_2
    //   1063: iload #4
    //   1065: aaload
    //   1066: invokeinterface toString : ()Ljava/lang/String;
    //   1071: invokevirtual trim : ()Ljava/lang/String;
    //   1074: aastore
    //   1075: iinc #4, 1
    //   1078: goto -> 1051
    //   1081: aload_0
    //   1082: aload #42
    //   1084: invokevirtual setAutofillHints : ([Ljava/lang/String;)V
    //   1087: goto -> 3520
    //   1090: goto -> 3520
    //   1093: aload #5
    //   1095: iload #4
    //   1097: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   1100: ifnull -> 3520
    //   1103: aload_0
    //   1104: aload #5
    //   1106: iload #4
    //   1108: iconst_1
    //   1109: invokevirtual getBoolean : (IZ)Z
    //   1112: invokevirtual setFocusedByDefault : (Z)V
    //   1115: goto -> 3520
    //   1118: aload_0
    //   1119: aload #5
    //   1121: iload #4
    //   1123: iconst_m1
    //   1124: invokevirtual getResourceId : (II)I
    //   1127: putfield mNextClusterForwardId : I
    //   1130: goto -> 3520
    //   1133: aload #5
    //   1135: iload #4
    //   1137: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   1140: ifnull -> 3520
    //   1143: aload_0
    //   1144: aload #5
    //   1146: iload #4
    //   1148: iconst_1
    //   1149: invokevirtual getBoolean : (IZ)Z
    //   1152: invokevirtual setKeyboardNavigationCluster : (Z)V
    //   1155: goto -> 3520
    //   1158: aload #5
    //   1160: iload #4
    //   1162: iconst_m1
    //   1163: invokevirtual getDimensionPixelSize : (II)I
    //   1166: istore #34
    //   1168: aload #39
    //   1170: astore_2
    //   1171: iload #36
    //   1173: istore #4
    //   1175: iload #17
    //   1177: istore #43
    //   1179: iload #6
    //   1181: istore #44
    //   1183: goto -> 3535
    //   1186: aload #5
    //   1188: iload #4
    //   1190: iconst_m1
    //   1191: invokevirtual getDimensionPixelSize : (II)I
    //   1194: istore #30
    //   1196: aload_0
    //   1197: iload #30
    //   1199: putfield mUserPaddingLeftInitial : I
    //   1202: aload_0
    //   1203: iload #30
    //   1205: putfield mUserPaddingRightInitial : I
    //   1208: iconst_1
    //   1209: istore #28
    //   1211: iconst_1
    //   1212: istore #27
    //   1214: aload #39
    //   1216: astore_2
    //   1217: iload #36
    //   1219: istore #4
    //   1221: iload #17
    //   1223: istore #43
    //   1225: iload #6
    //   1227: istore #44
    //   1229: goto -> 3535
    //   1232: aload_0
    //   1233: aload #5
    //   1235: iload #4
    //   1237: invokevirtual getText : (I)Ljava/lang/CharSequence;
    //   1240: invokevirtual setTooltipText : (Ljava/lang/CharSequence;)V
    //   1243: goto -> 3520
    //   1246: aload #5
    //   1248: iload #4
    //   1250: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   1253: ifnull -> 3520
    //   1256: aload_0
    //   1257: aload #5
    //   1259: iload #4
    //   1261: iconst_1
    //   1262: invokevirtual getBoolean : (IZ)Z
    //   1265: invokevirtual forceHasOverlappingRendering : (Z)V
    //   1268: goto -> 3520
    //   1271: aload #5
    //   1273: iload #4
    //   1275: iconst_0
    //   1276: invokevirtual getResourceId : (II)I
    //   1279: istore #43
    //   1281: iload #43
    //   1283: ifeq -> 1304
    //   1286: aload_1
    //   1287: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   1290: astore_2
    //   1291: aload_0
    //   1292: aload_2
    //   1293: iload #43
    //   1295: invokestatic load : (Landroid/content/res/Resources;I)Landroid/view/PointerIcon;
    //   1298: invokevirtual setPointerIcon : (Landroid/view/PointerIcon;)V
    //   1301: goto -> 3520
    //   1304: aload #5
    //   1306: iload #4
    //   1308: iconst_1
    //   1309: invokevirtual getInt : (II)I
    //   1312: istore #4
    //   1314: iload #4
    //   1316: iconst_1
    //   1317: if_icmpeq -> 1330
    //   1320: aload_0
    //   1321: aload_1
    //   1322: iload #4
    //   1324: invokestatic getSystemIcon : (Landroid/content/Context;I)Landroid/view/PointerIcon;
    //   1327: invokevirtual setPointerIcon : (Landroid/view/PointerIcon;)V
    //   1330: goto -> 3520
    //   1333: aload #5
    //   1335: iload #4
    //   1337: iconst_0
    //   1338: invokevirtual getBoolean : (IZ)Z
    //   1341: ifeq -> 3520
    //   1344: iload #36
    //   1346: ldc 8388608
    //   1348: ior
    //   1349: istore #4
    //   1351: ldc 8388608
    //   1353: iload #17
    //   1355: ior
    //   1356: istore #43
    //   1358: aload #39
    //   1360: astore_2
    //   1361: iload #6
    //   1363: istore #44
    //   1365: goto -> 3535
    //   1368: aload #5
    //   1370: iload #4
    //   1372: iconst_0
    //   1373: invokevirtual getInt : (II)I
    //   1376: bipush #8
    //   1378: ishl
    //   1379: sipush #16128
    //   1382: iand
    //   1383: istore #4
    //   1385: iload #4
    //   1387: ifeq -> 3520
    //   1390: aload_0
    //   1391: aload_0
    //   1392: getfield mPrivateFlags3 : I
    //   1395: iload #4
    //   1397: ior
    //   1398: putfield mPrivateFlags3 : I
    //   1401: iconst_1
    //   1402: istore #10
    //   1404: aload #39
    //   1406: astore_2
    //   1407: iload #36
    //   1409: istore #4
    //   1411: iload #17
    //   1413: istore #43
    //   1415: iload #6
    //   1417: istore #44
    //   1419: goto -> 3535
    //   1422: aload_0
    //   1423: aload #5
    //   1425: iload #4
    //   1427: iconst_m1
    //   1428: invokevirtual getResourceId : (II)I
    //   1431: invokevirtual setAccessibilityTraversalAfter : (I)V
    //   1434: goto -> 3520
    //   1437: aload_0
    //   1438: aload #5
    //   1440: iload #4
    //   1442: iconst_m1
    //   1443: invokevirtual getResourceId : (II)I
    //   1446: invokevirtual setAccessibilityTraversalBefore : (I)V
    //   1449: goto -> 3520
    //   1452: aload_0
    //   1453: aload #5
    //   1455: bipush #81
    //   1457: iconst_0
    //   1458: invokevirtual getInt : (II)I
    //   1461: invokespecial setOutlineProviderFromAttribute : (I)V
    //   1464: goto -> 3520
    //   1467: iload #14
    //   1469: bipush #23
    //   1471: if_icmpge -> 1481
    //   1474: aload_0
    //   1475: instanceof android/widget/FrameLayout
    //   1478: ifeq -> 3520
    //   1481: aload #5
    //   1483: iload #4
    //   1485: iconst_m1
    //   1486: invokevirtual getInt : (II)I
    //   1489: aconst_null
    //   1490: invokestatic parseBlendMode : (ILandroid/graphics/BlendMode;)Landroid/graphics/BlendMode;
    //   1493: astore_2
    //   1494: aload_0
    //   1495: aload_2
    //   1496: invokevirtual setForegroundTintBlendMode : (Landroid/graphics/BlendMode;)V
    //   1499: goto -> 3520
    //   1502: iload #14
    //   1504: bipush #23
    //   1506: if_icmpge -> 1516
    //   1509: aload_0
    //   1510: instanceof android/widget/FrameLayout
    //   1513: ifeq -> 3520
    //   1516: aload_0
    //   1517: aload #5
    //   1519: iload #4
    //   1521: invokevirtual getColorStateList : (I)Landroid/content/res/ColorStateList;
    //   1524: invokevirtual setForegroundTintList : (Landroid/content/res/ColorStateList;)V
    //   1527: goto -> 3520
    //   1530: aload_0
    //   1531: getfield mBackgroundTint : Landroid/view/View$TintInfo;
    //   1534: ifnonnull -> 1548
    //   1537: aload_0
    //   1538: new android/view/View$TintInfo
    //   1541: dup
    //   1542: invokespecial <init> : ()V
    //   1545: putfield mBackgroundTint : Landroid/view/View$TintInfo;
    //   1548: aload_0
    //   1549: getfield mBackgroundTint : Landroid/view/View$TintInfo;
    //   1552: aload #5
    //   1554: bipush #78
    //   1556: iconst_m1
    //   1557: invokevirtual getInt : (II)I
    //   1560: aconst_null
    //   1561: invokestatic parseBlendMode : (ILandroid/graphics/BlendMode;)Landroid/graphics/BlendMode;
    //   1564: putfield mBlendMode : Landroid/graphics/BlendMode;
    //   1567: aload_0
    //   1568: getfield mBackgroundTint : Landroid/view/View$TintInfo;
    //   1571: iconst_1
    //   1572: putfield mHasTintMode : Z
    //   1575: goto -> 3520
    //   1578: aload_0
    //   1579: getfield mBackgroundTint : Landroid/view/View$TintInfo;
    //   1582: ifnonnull -> 1596
    //   1585: aload_0
    //   1586: new android/view/View$TintInfo
    //   1589: dup
    //   1590: invokespecial <init> : ()V
    //   1593: putfield mBackgroundTint : Landroid/view/View$TintInfo;
    //   1596: aload_0
    //   1597: getfield mBackgroundTint : Landroid/view/View$TintInfo;
    //   1600: aload #5
    //   1602: bipush #77
    //   1604: invokevirtual getColorStateList : (I)Landroid/content/res/ColorStateList;
    //   1607: putfield mTintList : Landroid/content/res/ColorStateList;
    //   1610: aload_0
    //   1611: getfield mBackgroundTint : Landroid/view/View$TintInfo;
    //   1614: iconst_1
    //   1615: putfield mHasTintList : Z
    //   1618: goto -> 3520
    //   1621: aload #5
    //   1623: iload #4
    //   1625: iconst_0
    //   1626: invokevirtual getResourceId : (II)I
    //   1629: istore #4
    //   1631: aload_0
    //   1632: aload_1
    //   1633: iload #4
    //   1635: invokestatic loadStateListAnimator : (Landroid/content/Context;I)Landroid/animation/StateListAnimator;
    //   1638: invokevirtual setStateListAnimator : (Landroid/animation/StateListAnimator;)V
    //   1641: goto -> 3520
    //   1644: aload #5
    //   1646: iload #4
    //   1648: fconst_0
    //   1649: invokevirtual getDimension : (IF)F
    //   1652: fstore #21
    //   1654: iconst_1
    //   1655: istore #44
    //   1657: aload #39
    //   1659: astore_2
    //   1660: iload #36
    //   1662: istore #4
    //   1664: iload #17
    //   1666: istore #43
    //   1668: goto -> 3535
    //   1671: aload_0
    //   1672: aload #5
    //   1674: iload #4
    //   1676: iconst_0
    //   1677: invokevirtual getBoolean : (IZ)Z
    //   1680: invokevirtual setNestedScrollingEnabled : (Z)V
    //   1683: goto -> 3520
    //   1686: aload_0
    //   1687: aload #5
    //   1689: iload #4
    //   1691: invokevirtual getString : (I)Ljava/lang/String;
    //   1694: invokevirtual setTransitionName : (Ljava/lang/String;)V
    //   1697: goto -> 3520
    //   1700: aload #5
    //   1702: iload #4
    //   1704: fconst_0
    //   1705: invokevirtual getDimension : (IF)F
    //   1708: fstore #20
    //   1710: iconst_1
    //   1711: istore #44
    //   1713: aload #39
    //   1715: astore_2
    //   1716: iload #36
    //   1718: istore #4
    //   1720: iload #17
    //   1722: istore #43
    //   1724: goto -> 3535
    //   1727: aload_0
    //   1728: aload #5
    //   1730: iload #4
    //   1732: iconst_0
    //   1733: invokevirtual getInt : (II)I
    //   1736: invokevirtual setAccessibilityLiveRegion : (I)V
    //   1739: goto -> 3520
    //   1742: aload_0
    //   1743: aload #5
    //   1745: iload #4
    //   1747: iconst_m1
    //   1748: invokevirtual getResourceId : (II)I
    //   1751: invokevirtual setLabelFor : (I)V
    //   1754: goto -> 3520
    //   1757: aload #5
    //   1759: iload #4
    //   1761: ldc_w -2147483648
    //   1764: invokevirtual getDimensionPixelSize : (II)I
    //   1767: istore #38
    //   1769: iload #38
    //   1771: ldc_w -2147483648
    //   1774: if_icmpeq -> 1783
    //   1777: iconst_1
    //   1778: istore #4
    //   1780: goto -> 1786
    //   1783: iconst_0
    //   1784: istore #4
    //   1786: iload #4
    //   1788: istore #12
    //   1790: aload #39
    //   1792: astore_2
    //   1793: iload #36
    //   1795: istore #4
    //   1797: iload #17
    //   1799: istore #43
    //   1801: iload #6
    //   1803: istore #44
    //   1805: goto -> 3535
    //   1808: aload #5
    //   1810: iload #4
    //   1812: ldc_w -2147483648
    //   1815: invokevirtual getDimensionPixelSize : (II)I
    //   1818: istore #33
    //   1820: iload #33
    //   1822: ldc_w -2147483648
    //   1825: if_icmpeq -> 1834
    //   1828: iconst_1
    //   1829: istore #4
    //   1831: goto -> 1837
    //   1834: iconst_0
    //   1835: istore #4
    //   1837: iload #4
    //   1839: istore #11
    //   1841: aload #39
    //   1843: astore_2
    //   1844: iload #36
    //   1846: istore #4
    //   1848: iload #17
    //   1850: istore #43
    //   1852: iload #6
    //   1854: istore #44
    //   1856: goto -> 3535
    //   1859: aload_0
    //   1860: aload_0
    //   1861: getfield mPrivateFlags2 : I
    //   1864: bipush #-61
    //   1866: iand
    //   1867: putfield mPrivateFlags2 : I
    //   1870: aload #5
    //   1872: iload #4
    //   1874: iconst_m1
    //   1875: invokevirtual getInt : (II)I
    //   1878: istore #4
    //   1880: iload #4
    //   1882: iconst_m1
    //   1883: if_icmpeq -> 1897
    //   1886: getstatic android/view/View.LAYOUT_DIRECTION_FLAGS : [I
    //   1889: iload #4
    //   1891: iaload
    //   1892: istore #4
    //   1894: goto -> 1900
    //   1897: iconst_2
    //   1898: istore #4
    //   1900: aload_0
    //   1901: aload_0
    //   1902: getfield mPrivateFlags2 : I
    //   1905: iload #4
    //   1907: iconst_2
    //   1908: ishl
    //   1909: ior
    //   1910: putfield mPrivateFlags2 : I
    //   1913: goto -> 3520
    //   1916: aload_0
    //   1917: aload_0
    //   1918: getfield mPrivateFlags2 : I
    //   1921: ldc_w -57345
    //   1924: iand
    //   1925: putfield mPrivateFlags2 : I
    //   1928: aload #5
    //   1930: iload #4
    //   1932: iconst_1
    //   1933: invokevirtual getInt : (II)I
    //   1936: istore #4
    //   1938: aload_0
    //   1939: aload_0
    //   1940: getfield mPrivateFlags2 : I
    //   1943: getstatic android/view/View.PFLAG2_TEXT_ALIGNMENT_FLAGS : [I
    //   1946: iload #4
    //   1948: iaload
    //   1949: ior
    //   1950: putfield mPrivateFlags2 : I
    //   1953: goto -> 3520
    //   1956: aload_0
    //   1957: aload_0
    //   1958: getfield mPrivateFlags2 : I
    //   1961: sipush #-449
    //   1964: iand
    //   1965: putfield mPrivateFlags2 : I
    //   1968: aload #5
    //   1970: iload #4
    //   1972: iconst_m1
    //   1973: invokevirtual getInt : (II)I
    //   1976: istore #4
    //   1978: iload #4
    //   1980: iconst_m1
    //   1981: if_icmpeq -> 3520
    //   1984: aload_0
    //   1985: aload_0
    //   1986: getfield mPrivateFlags2 : I
    //   1989: getstatic android/view/View.PFLAG2_TEXT_DIRECTION_FLAGS : [I
    //   1992: iload #4
    //   1994: iaload
    //   1995: ior
    //   1996: putfield mPrivateFlags2 : I
    //   1999: goto -> 3520
    //   2002: aload_0
    //   2003: aload #5
    //   2005: iload #4
    //   2007: iconst_0
    //   2008: invokevirtual getInt : (II)I
    //   2011: invokevirtual setImportantForAccessibility : (I)V
    //   2014: goto -> 3520
    //   2017: goto -> 2919
    //   2020: aload_0
    //   2021: aload #5
    //   2023: iload #4
    //   2025: iconst_0
    //   2026: invokevirtual getInt : (II)I
    //   2029: aconst_null
    //   2030: invokevirtual setLayerType : (ILandroid/graphics/Paint;)V
    //   2033: goto -> 3520
    //   2036: aload_0
    //   2037: aload #5
    //   2039: iload #4
    //   2041: iconst_m1
    //   2042: invokevirtual getResourceId : (II)I
    //   2045: putfield mNextFocusForwardId : I
    //   2048: goto -> 3520
    //   2051: aload_0
    //   2052: aload #5
    //   2054: iload #4
    //   2056: iconst_0
    //   2057: invokevirtual getInt : (II)I
    //   2060: putfield mVerticalScrollbarPosition : I
    //   2063: goto -> 3520
    //   2066: aload #5
    //   2068: iload #4
    //   2070: fconst_0
    //   2071: invokevirtual getFloat : (IF)F
    //   2074: fstore #24
    //   2076: iconst_1
    //   2077: istore #44
    //   2079: aload #39
    //   2081: astore_2
    //   2082: iload #36
    //   2084: istore #4
    //   2086: iload #17
    //   2088: istore #43
    //   2090: goto -> 3535
    //   2093: aload #5
    //   2095: iload #4
    //   2097: fconst_0
    //   2098: invokevirtual getFloat : (IF)F
    //   2101: fstore #23
    //   2103: iconst_1
    //   2104: istore #44
    //   2106: aload #39
    //   2108: astore_2
    //   2109: iload #36
    //   2111: istore #4
    //   2113: iload #17
    //   2115: istore #43
    //   2117: goto -> 3535
    //   2120: aload #5
    //   2122: iload #4
    //   2124: fconst_0
    //   2125: invokevirtual getFloat : (IF)F
    //   2128: fstore #22
    //   2130: iconst_1
    //   2131: istore #44
    //   2133: aload #39
    //   2135: astore_2
    //   2136: iload #36
    //   2138: istore #4
    //   2140: iload #17
    //   2142: istore #43
    //   2144: goto -> 3535
    //   2147: aload #5
    //   2149: iload #4
    //   2151: fconst_1
    //   2152: invokevirtual getFloat : (IF)F
    //   2155: fstore #26
    //   2157: iconst_1
    //   2158: istore #44
    //   2160: aload #39
    //   2162: astore_2
    //   2163: iload #36
    //   2165: istore #4
    //   2167: iload #17
    //   2169: istore #43
    //   2171: goto -> 3535
    //   2174: aload #5
    //   2176: iload #4
    //   2178: fconst_1
    //   2179: invokevirtual getFloat : (IF)F
    //   2182: fstore #25
    //   2184: iconst_1
    //   2185: istore #44
    //   2187: aload #39
    //   2189: astore_2
    //   2190: iload #36
    //   2192: istore #4
    //   2194: iload #17
    //   2196: istore #43
    //   2198: goto -> 3535
    //   2201: aload #5
    //   2203: iload #4
    //   2205: fconst_0
    //   2206: invokevirtual getDimension : (IF)F
    //   2209: fstore #19
    //   2211: iconst_1
    //   2212: istore #44
    //   2214: aload #39
    //   2216: astore_2
    //   2217: iload #36
    //   2219: istore #4
    //   2221: iload #17
    //   2223: istore #43
    //   2225: goto -> 3535
    //   2228: aload #5
    //   2230: iload #4
    //   2232: fconst_0
    //   2233: invokevirtual getDimension : (IF)F
    //   2236: fstore #18
    //   2238: iconst_1
    //   2239: istore #44
    //   2241: aload #39
    //   2243: astore_2
    //   2244: iload #36
    //   2246: istore #4
    //   2248: iload #17
    //   2250: istore #43
    //   2252: goto -> 3535
    //   2255: aload_0
    //   2256: aload #5
    //   2258: iload #4
    //   2260: fconst_0
    //   2261: invokevirtual getDimension : (IF)F
    //   2264: invokevirtual setPivotY : (F)V
    //   2267: goto -> 3520
    //   2270: aload_0
    //   2271: aload #5
    //   2273: iload #4
    //   2275: fconst_0
    //   2276: invokevirtual getDimension : (IF)F
    //   2279: invokevirtual setPivotX : (F)V
    //   2282: goto -> 3520
    //   2285: aload_0
    //   2286: aload #5
    //   2288: iload #4
    //   2290: fconst_1
    //   2291: invokevirtual getFloat : (IF)F
    //   2294: invokevirtual setAlpha : (F)V
    //   2297: goto -> 3520
    //   2300: aload #5
    //   2302: iload #4
    //   2304: iconst_0
    //   2305: invokevirtual getBoolean : (IZ)Z
    //   2308: ifeq -> 3520
    //   2311: iload #17
    //   2313: sipush #1024
    //   2316: ior
    //   2317: istore #43
    //   2319: iload #36
    //   2321: sipush #1024
    //   2324: ior
    //   2325: istore #4
    //   2327: aload #39
    //   2329: astore_2
    //   2330: iload #6
    //   2332: istore #44
    //   2334: goto -> 3535
    //   2337: aload #5
    //   2339: iload #4
    //   2341: iconst_1
    //   2342: invokevirtual getInt : (II)I
    //   2345: istore #8
    //   2347: aload #39
    //   2349: astore_2
    //   2350: iload #36
    //   2352: istore #4
    //   2354: iload #17
    //   2356: istore #43
    //   2358: iload #6
    //   2360: istore #44
    //   2362: goto -> 3535
    //   2365: aload_0
    //   2366: aload #5
    //   2368: iload #4
    //   2370: invokevirtual getString : (I)Ljava/lang/String;
    //   2373: invokevirtual setContentDescription : (Ljava/lang/CharSequence;)V
    //   2376: goto -> 3520
    //   2379: aload_1
    //   2380: invokevirtual isRestricted : ()Z
    //   2383: ifne -> 2414
    //   2386: aload #5
    //   2388: iload #4
    //   2390: invokevirtual getString : (I)Ljava/lang/String;
    //   2393: astore_2
    //   2394: aload_2
    //   2395: ifnull -> 3520
    //   2398: aload_0
    //   2399: new android/view/View$DeclaredOnClickListener
    //   2402: dup
    //   2403: aload_0
    //   2404: aload_2
    //   2405: invokespecial <init> : (Landroid/view/View;Ljava/lang/String;)V
    //   2408: invokevirtual setOnClickListener : (Landroid/view/View$OnClickListener;)V
    //   2411: goto -> 3520
    //   2414: new java/lang/IllegalStateException
    //   2417: dup
    //   2418: ldc_w 'The android:onClick attribute cannot be used within a restricted context'
    //   2421: invokespecial <init> : (Ljava/lang/String;)V
    //   2424: athrow
    //   2425: aload #5
    //   2427: iload #4
    //   2429: iconst_1
    //   2430: invokevirtual getBoolean : (IZ)Z
    //   2433: ifne -> 3520
    //   2436: ldc_w 268435456
    //   2439: iload #17
    //   2441: ior
    //   2442: istore #43
    //   2444: ldc_w -268435457
    //   2447: iload #36
    //   2449: iand
    //   2450: istore #4
    //   2452: aload #39
    //   2454: astore_2
    //   2455: iload #6
    //   2457: istore #44
    //   2459: goto -> 3535
    //   2462: aload #5
    //   2464: iload #4
    //   2466: iconst_0
    //   2467: invokevirtual getBoolean : (IZ)Z
    //   2470: ifeq -> 2478
    //   2473: aload_0
    //   2474: iconst_1
    //   2475: invokevirtual setScrollContainer : (Z)V
    //   2478: iconst_1
    //   2479: istore #35
    //   2481: aload #39
    //   2483: astore_2
    //   2484: iload #36
    //   2486: istore #4
    //   2488: iload #17
    //   2490: istore #43
    //   2492: iload #6
    //   2494: istore #44
    //   2496: goto -> 3535
    //   2499: aload #5
    //   2501: iload #4
    //   2503: iconst_0
    //   2504: invokevirtual getBoolean : (IZ)Z
    //   2507: ifeq -> 3520
    //   2510: iload #36
    //   2512: ldc_w 67108864
    //   2515: ior
    //   2516: istore #4
    //   2518: ldc_w 67108864
    //   2521: iload #17
    //   2523: ior
    //   2524: istore #43
    //   2526: aload #39
    //   2528: astore_2
    //   2529: iload #6
    //   2531: istore #44
    //   2533: goto -> 3535
    //   2536: aload #5
    //   2538: iload #4
    //   2540: iconst_1
    //   2541: invokevirtual getBoolean : (IZ)Z
    //   2544: ifne -> 3520
    //   2547: ldc_w 134217728
    //   2550: iload #17
    //   2552: ior
    //   2553: istore #43
    //   2555: ldc_w -134217729
    //   2558: iload #36
    //   2560: iand
    //   2561: istore #4
    //   2563: aload #39
    //   2565: astore_2
    //   2566: iload #6
    //   2568: istore #44
    //   2570: goto -> 3535
    //   2573: iload #14
    //   2575: bipush #23
    //   2577: if_icmpge -> 2587
    //   2580: aload_0
    //   2581: instanceof android/widget/FrameLayout
    //   2584: ifeq -> 3520
    //   2587: aload_0
    //   2588: aload #5
    //   2590: iload #4
    //   2592: iconst_0
    //   2593: invokevirtual getInt : (II)I
    //   2596: invokevirtual setForegroundGravity : (I)V
    //   2599: goto -> 3520
    //   2602: aload_0
    //   2603: aload #5
    //   2605: iload #4
    //   2607: iconst_0
    //   2608: invokevirtual getDimensionPixelSize : (II)I
    //   2611: putfield mMinHeight : I
    //   2614: goto -> 3520
    //   2617: aload_0
    //   2618: aload #5
    //   2620: iload #4
    //   2622: iconst_0
    //   2623: invokevirtual getDimensionPixelSize : (II)I
    //   2626: putfield mMinWidth : I
    //   2629: goto -> 3520
    //   2632: iload #14
    //   2634: bipush #23
    //   2636: if_icmpge -> 2646
    //   2639: aload_0
    //   2640: instanceof android/widget/FrameLayout
    //   2643: ifeq -> 3520
    //   2646: aload_0
    //   2647: aload #5
    //   2649: iload #4
    //   2651: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   2654: invokevirtual setForeground : (Landroid/graphics/drawable/Drawable;)V
    //   2657: goto -> 3520
    //   2660: aload #5
    //   2662: iload #4
    //   2664: iconst_0
    //   2665: invokevirtual getBoolean : (IZ)Z
    //   2668: ifeq -> 3520
    //   2671: iload #36
    //   2673: ldc 4194304
    //   2675: ior
    //   2676: istore #4
    //   2678: ldc 4194304
    //   2680: iload #17
    //   2682: ior
    //   2683: istore #43
    //   2685: aload #39
    //   2687: astore_2
    //   2688: iload #6
    //   2690: istore #44
    //   2692: goto -> 3535
    //   2695: aload #5
    //   2697: iload #4
    //   2699: iconst_0
    //   2700: invokevirtual getInt : (II)I
    //   2703: istore #4
    //   2705: iload #4
    //   2707: ifeq -> 3520
    //   2710: iload #36
    //   2712: getstatic android/view/View.DRAWING_CACHE_QUALITY_FLAGS : [I
    //   2715: iload #4
    //   2717: iaload
    //   2718: ior
    //   2719: istore #4
    //   2721: ldc 1572864
    //   2723: iload #17
    //   2725: ior
    //   2726: istore #43
    //   2728: aload #39
    //   2730: astore_2
    //   2731: iload #6
    //   2733: istore #44
    //   2735: goto -> 3535
    //   2738: aload #5
    //   2740: iload #4
    //   2742: iconst_1
    //   2743: invokevirtual getBoolean : (IZ)Z
    //   2746: ifne -> 3520
    //   2749: iload #36
    //   2751: ldc_w 65536
    //   2754: ior
    //   2755: istore #4
    //   2757: ldc_w 65536
    //   2760: iload #17
    //   2762: ior
    //   2763: istore #43
    //   2765: aload #39
    //   2767: astore_2
    //   2768: iload #6
    //   2770: istore #44
    //   2772: goto -> 3535
    //   2775: aload #5
    //   2777: iload #4
    //   2779: iconst_0
    //   2780: invokevirtual getBoolean : (IZ)Z
    //   2783: ifeq -> 3520
    //   2786: iload #36
    //   2788: ldc_w 2097152
    //   2791: ior
    //   2792: istore #4
    //   2794: ldc_w 2097152
    //   2797: iload #17
    //   2799: ior
    //   2800: istore #43
    //   2802: aload #39
    //   2804: astore_2
    //   2805: iload #6
    //   2807: istore #44
    //   2809: goto -> 3535
    //   2812: aload #5
    //   2814: iload #4
    //   2816: iconst_0
    //   2817: invokevirtual getBoolean : (IZ)Z
    //   2820: ifeq -> 3520
    //   2823: iload #17
    //   2825: sipush #16384
    //   2828: ior
    //   2829: istore #43
    //   2831: iload #36
    //   2833: sipush #16384
    //   2836: ior
    //   2837: istore #4
    //   2839: aload #39
    //   2841: astore_2
    //   2842: iload #6
    //   2844: istore #44
    //   2846: goto -> 3535
    //   2849: aload_0
    //   2850: aload #5
    //   2852: iload #4
    //   2854: iconst_m1
    //   2855: invokevirtual getResourceId : (II)I
    //   2858: putfield mNextFocusDownId : I
    //   2861: goto -> 3520
    //   2864: aload_0
    //   2865: aload #5
    //   2867: iload #4
    //   2869: iconst_m1
    //   2870: invokevirtual getResourceId : (II)I
    //   2873: putfield mNextFocusUpId : I
    //   2876: goto -> 3520
    //   2879: aload_0
    //   2880: aload #5
    //   2882: iload #4
    //   2884: iconst_m1
    //   2885: invokevirtual getResourceId : (II)I
    //   2888: putfield mNextFocusRightId : I
    //   2891: goto -> 3520
    //   2894: aload_0
    //   2895: aload #5
    //   2897: iload #4
    //   2899: iconst_m1
    //   2900: invokevirtual getResourceId : (II)I
    //   2903: putfield mNextFocusLeftId : I
    //   2906: goto -> 3520
    //   2909: iload #14
    //   2911: bipush #14
    //   2913: if_icmplt -> 2919
    //   2916: goto -> 3520
    //   2919: aload #5
    //   2921: iload #4
    //   2923: iconst_0
    //   2924: invokevirtual getInt : (II)I
    //   2927: istore #4
    //   2929: iload #4
    //   2931: ifeq -> 3520
    //   2934: iload #36
    //   2936: iload #4
    //   2938: ior
    //   2939: istore #4
    //   2941: aload_0
    //   2942: aload #5
    //   2944: invokevirtual initializeFadingEdgeInternal : (Landroid/content/res/TypedArray;)V
    //   2947: iload #17
    //   2949: sipush #12288
    //   2952: ior
    //   2953: istore #43
    //   2955: aload #39
    //   2957: astore_2
    //   2958: iload #6
    //   2960: istore #44
    //   2962: goto -> 3535
    //   2965: aload #5
    //   2967: iload #4
    //   2969: iconst_0
    //   2970: invokevirtual getInt : (II)I
    //   2973: istore #4
    //   2975: iload #4
    //   2977: ifeq -> 3520
    //   2980: iload #36
    //   2982: iload #4
    //   2984: ior
    //   2985: istore #4
    //   2987: iconst_1
    //   2988: istore #9
    //   2990: iload #17
    //   2992: sipush #768
    //   2995: ior
    //   2996: istore #43
    //   2998: aload #39
    //   3000: astore_2
    //   3001: iload #6
    //   3003: istore #44
    //   3005: goto -> 3535
    //   3008: aload #5
    //   3010: iload #4
    //   3012: iconst_0
    //   3013: invokevirtual getBoolean : (IZ)Z
    //   3016: ifeq -> 3520
    //   3019: iload #17
    //   3021: iconst_2
    //   3022: ior
    //   3023: istore #43
    //   3025: iload #36
    //   3027: iconst_2
    //   3028: ior
    //   3029: istore #4
    //   3031: aload #39
    //   3033: astore_2
    //   3034: iload #6
    //   3036: istore #44
    //   3038: goto -> 3535
    //   3041: aload #5
    //   3043: iload #4
    //   3045: iconst_0
    //   3046: invokevirtual getInt : (II)I
    //   3049: istore #4
    //   3051: iload #4
    //   3053: ifeq -> 3520
    //   3056: iload #36
    //   3058: getstatic android/view/View.VISIBILITY_FLAGS : [I
    //   3061: iload #4
    //   3063: iaload
    //   3064: ior
    //   3065: istore #4
    //   3067: iload #17
    //   3069: bipush #12
    //   3071: ior
    //   3072: istore #43
    //   3074: aload #39
    //   3076: astore_2
    //   3077: iload #6
    //   3079: istore #44
    //   3081: goto -> 3535
    //   3084: aload #5
    //   3086: iload #4
    //   3088: iconst_0
    //   3089: invokevirtual getBoolean : (IZ)Z
    //   3092: ifeq -> 3520
    //   3095: ldc_w 262161
    //   3098: iload #17
    //   3100: ior
    //   3101: istore #43
    //   3103: iload #36
    //   3105: bipush #-17
    //   3107: iand
    //   3108: ldc_w 262145
    //   3111: ior
    //   3112: istore #4
    //   3114: aload #39
    //   3116: astore_2
    //   3117: iload #6
    //   3119: istore #44
    //   3121: goto -> 3535
    //   3124: iload #36
    //   3126: bipush #-18
    //   3128: iand
    //   3129: aload_0
    //   3130: aload #5
    //   3132: invokespecial getFocusableAttribute : (Landroid/content/res/TypedArray;)I
    //   3135: ior
    //   3136: istore #4
    //   3138: iload #4
    //   3140: bipush #16
    //   3142: iand
    //   3143: ifne -> 3163
    //   3146: iload #17
    //   3148: bipush #17
    //   3150: ior
    //   3151: istore #43
    //   3153: aload #39
    //   3155: astore_2
    //   3156: iload #6
    //   3158: istore #44
    //   3160: goto -> 3535
    //   3163: aload #39
    //   3165: astore_2
    //   3166: iload #17
    //   3168: istore #43
    //   3170: iload #6
    //   3172: istore #44
    //   3174: goto -> 3535
    //   3177: aload #5
    //   3179: iload #4
    //   3181: iconst_m1
    //   3182: invokevirtual getDimensionPixelSize : (II)I
    //   3185: istore #37
    //   3187: aload #39
    //   3189: astore_2
    //   3190: iload #36
    //   3192: istore #4
    //   3194: iload #17
    //   3196: istore #43
    //   3198: iload #6
    //   3200: istore #44
    //   3202: goto -> 3535
    //   3205: aload #5
    //   3207: iload #4
    //   3209: iconst_m1
    //   3210: invokevirtual getDimensionPixelSize : (II)I
    //   3213: istore #29
    //   3215: aload_0
    //   3216: iload #29
    //   3218: putfield mUserPaddingRightInitial : I
    //   3221: iconst_1
    //   3222: istore #28
    //   3224: aload #39
    //   3226: astore_2
    //   3227: iload #36
    //   3229: istore #4
    //   3231: iload #17
    //   3233: istore #43
    //   3235: iload #6
    //   3237: istore #44
    //   3239: goto -> 3535
    //   3242: aload #5
    //   3244: iload #4
    //   3246: iconst_m1
    //   3247: invokevirtual getDimensionPixelSize : (II)I
    //   3250: istore #15
    //   3252: aload #39
    //   3254: astore_2
    //   3255: iload #36
    //   3257: istore #4
    //   3259: iload #17
    //   3261: istore #43
    //   3263: iload #6
    //   3265: istore #44
    //   3267: goto -> 3535
    //   3270: aload #5
    //   3272: iload #4
    //   3274: iconst_m1
    //   3275: invokevirtual getDimensionPixelSize : (II)I
    //   3278: istore #13
    //   3280: aload_0
    //   3281: iload #13
    //   3283: putfield mUserPaddingLeftInitial : I
    //   3286: iconst_1
    //   3287: istore #27
    //   3289: aload #39
    //   3291: astore_2
    //   3292: iload #36
    //   3294: istore #4
    //   3296: iload #17
    //   3298: istore #43
    //   3300: iload #6
    //   3302: istore #44
    //   3304: goto -> 3535
    //   3307: aload #5
    //   3309: iload #4
    //   3311: iconst_m1
    //   3312: invokevirtual getDimensionPixelSize : (II)I
    //   3315: istore_3
    //   3316: aload_0
    //   3317: iload_3
    //   3318: putfield mUserPaddingLeftInitial : I
    //   3321: aload_0
    //   3322: iload_3
    //   3323: putfield mUserPaddingRightInitial : I
    //   3326: iconst_1
    //   3327: istore #28
    //   3329: iconst_1
    //   3330: istore #27
    //   3332: aload #39
    //   3334: astore_2
    //   3335: iload #36
    //   3337: istore #4
    //   3339: iload #17
    //   3341: istore #43
    //   3343: iload #6
    //   3345: istore #44
    //   3347: goto -> 3535
    //   3350: aload #5
    //   3352: iload #4
    //   3354: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   3357: astore_2
    //   3358: iload #36
    //   3360: istore #4
    //   3362: iload #17
    //   3364: istore #43
    //   3366: iload #6
    //   3368: istore #44
    //   3370: goto -> 3535
    //   3373: aload #5
    //   3375: iload #4
    //   3377: iconst_0
    //   3378: invokevirtual getDimensionPixelOffset : (II)I
    //   3381: istore #32
    //   3383: aload #39
    //   3385: astore_2
    //   3386: iload #36
    //   3388: istore #4
    //   3390: iload #17
    //   3392: istore #43
    //   3394: iload #6
    //   3396: istore #44
    //   3398: goto -> 3535
    //   3401: aload #5
    //   3403: iload #4
    //   3405: iconst_0
    //   3406: invokevirtual getDimensionPixelOffset : (II)I
    //   3409: istore #31
    //   3411: aload #39
    //   3413: astore_2
    //   3414: iload #36
    //   3416: istore #4
    //   3418: iload #17
    //   3420: istore #43
    //   3422: iload #6
    //   3424: istore #44
    //   3426: goto -> 3535
    //   3429: aload_0
    //   3430: aload #5
    //   3432: iload #4
    //   3434: invokevirtual getText : (I)Ljava/lang/CharSequence;
    //   3437: putfield mTag : Ljava/lang/Object;
    //   3440: goto -> 3520
    //   3443: aload_0
    //   3444: aload #5
    //   3446: iload #4
    //   3448: iconst_m1
    //   3449: invokevirtual getResourceId : (II)I
    //   3452: putfield mID : I
    //   3455: goto -> 3520
    //   3458: aload #5
    //   3460: iload #4
    //   3462: iconst_0
    //   3463: invokevirtual getInt : (II)I
    //   3466: istore #7
    //   3468: iload #7
    //   3470: ifeq -> 3502
    //   3473: iload #36
    //   3475: iload #7
    //   3477: ldc_w 50331648
    //   3480: iand
    //   3481: ior
    //   3482: istore #4
    //   3484: ldc_w 50331648
    //   3487: iload #17
    //   3489: ior
    //   3490: istore #43
    //   3492: aload #39
    //   3494: astore_2
    //   3495: iload #6
    //   3497: istore #44
    //   3499: goto -> 3535
    //   3502: aload #39
    //   3504: astore_2
    //   3505: iload #36
    //   3507: istore #4
    //   3509: iload #17
    //   3511: istore #43
    //   3513: iload #6
    //   3515: istore #44
    //   3517: goto -> 3535
    //   3520: iload #6
    //   3522: istore #44
    //   3524: iload #17
    //   3526: istore #43
    //   3528: iload #36
    //   3530: istore #4
    //   3532: aload #39
    //   3534: astore_2
    //   3535: iinc #40, 1
    //   3538: aload_2
    //   3539: astore #39
    //   3541: iload #4
    //   3543: istore #36
    //   3545: iload #43
    //   3547: istore #17
    //   3549: iload #44
    //   3551: istore #6
    //   3553: goto -> 194
    //   3556: iconst_0
    //   3557: istore #40
    //   3559: aload_0
    //   3560: iload #8
    //   3562: invokevirtual setOverScrollMode : (I)V
    //   3565: aload_0
    //   3566: iload #33
    //   3568: putfield mUserPaddingStart : I
    //   3571: aload_0
    //   3572: iload #38
    //   3574: putfield mUserPaddingEnd : I
    //   3577: aload #39
    //   3579: ifnull -> 3588
    //   3582: aload_0
    //   3583: aload #39
    //   3585: invokevirtual setBackground : (Landroid/graphics/drawable/Drawable;)V
    //   3588: aload_0
    //   3589: iload #27
    //   3591: putfield mLeftPaddingDefined : Z
    //   3594: aload_0
    //   3595: iload #28
    //   3597: putfield mRightPaddingDefined : Z
    //   3600: iload_3
    //   3601: iflt -> 3632
    //   3604: iload_3
    //   3605: istore #4
    //   3607: aload_0
    //   3608: iload_3
    //   3609: putfield mUserPaddingLeftInitial : I
    //   3612: aload_0
    //   3613: iload_3
    //   3614: putfield mUserPaddingRightInitial : I
    //   3617: iload_3
    //   3618: istore #34
    //   3620: iload_3
    //   3621: istore #43
    //   3623: iload_3
    //   3624: istore #44
    //   3626: iload #34
    //   3628: istore_3
    //   3629: goto -> 3706
    //   3632: iload #30
    //   3634: iflt -> 3659
    //   3637: iload #30
    //   3639: istore_3
    //   3640: iload #30
    //   3642: istore #4
    //   3644: aload_0
    //   3645: iload #30
    //   3647: putfield mUserPaddingLeftInitial : I
    //   3650: aload_0
    //   3651: iload #30
    //   3653: putfield mUserPaddingRightInitial : I
    //   3656: goto -> 3666
    //   3659: iload #29
    //   3661: istore #4
    //   3663: iload #13
    //   3665: istore_3
    //   3666: iload #34
    //   3668: iflt -> 3692
    //   3671: iload #34
    //   3673: istore #30
    //   3675: iload_3
    //   3676: istore #44
    //   3678: iload #4
    //   3680: istore #43
    //   3682: iload #30
    //   3684: istore #4
    //   3686: iload #34
    //   3688: istore_3
    //   3689: goto -> 3706
    //   3692: iload #4
    //   3694: istore #43
    //   3696: iload_3
    //   3697: istore #44
    //   3699: iload #37
    //   3701: istore_3
    //   3702: iload #15
    //   3704: istore #4
    //   3706: aload_0
    //   3707: invokespecial isRtlCompatibilityMode : ()Z
    //   3710: ifeq -> 3808
    //   3713: iload #44
    //   3715: istore #34
    //   3717: aload_0
    //   3718: getfield mLeftPaddingDefined : Z
    //   3721: ifne -> 3737
    //   3724: iload #44
    //   3726: istore #34
    //   3728: iload #11
    //   3730: ifeq -> 3737
    //   3733: iload #33
    //   3735: istore #34
    //   3737: iload #34
    //   3739: iflt -> 3745
    //   3742: goto -> 3751
    //   3745: aload_0
    //   3746: getfield mUserPaddingLeftInitial : I
    //   3749: istore #34
    //   3751: aload_0
    //   3752: iload #34
    //   3754: putfield mUserPaddingLeftInitial : I
    //   3757: iload #43
    //   3759: istore #44
    //   3761: aload_0
    //   3762: getfield mRightPaddingDefined : Z
    //   3765: ifne -> 3781
    //   3768: iload #43
    //   3770: istore #44
    //   3772: iload #12
    //   3774: ifeq -> 3781
    //   3777: iload #38
    //   3779: istore #44
    //   3781: iload #44
    //   3783: iflt -> 3793
    //   3786: iload #44
    //   3788: istore #43
    //   3790: goto -> 3799
    //   3793: aload_0
    //   3794: getfield mUserPaddingRightInitial : I
    //   3797: istore #43
    //   3799: aload_0
    //   3800: iload #43
    //   3802: putfield mUserPaddingRightInitial : I
    //   3805: goto -> 3861
    //   3808: iload #11
    //   3810: ifne -> 3822
    //   3813: iload #40
    //   3815: istore #34
    //   3817: iload #12
    //   3819: ifeq -> 3825
    //   3822: iconst_1
    //   3823: istore #34
    //   3825: aload_0
    //   3826: getfield mLeftPaddingDefined : Z
    //   3829: ifeq -> 3843
    //   3832: iload #34
    //   3834: ifne -> 3843
    //   3837: aload_0
    //   3838: iload #44
    //   3840: putfield mUserPaddingLeftInitial : I
    //   3843: aload_0
    //   3844: getfield mRightPaddingDefined : Z
    //   3847: ifeq -> 3861
    //   3850: iload #34
    //   3852: ifne -> 3861
    //   3855: aload_0
    //   3856: iload #43
    //   3858: putfield mUserPaddingRightInitial : I
    //   3861: aload_0
    //   3862: getfield mUserPaddingLeftInitial : I
    //   3865: istore #43
    //   3867: iload #4
    //   3869: iflt -> 3875
    //   3872: goto -> 3881
    //   3875: aload_0
    //   3876: getfield mPaddingTop : I
    //   3879: istore #4
    //   3881: aload_0
    //   3882: getfield mUserPaddingRightInitial : I
    //   3885: istore #44
    //   3887: iload_3
    //   3888: iflt -> 3894
    //   3891: goto -> 3899
    //   3894: aload_0
    //   3895: getfield mPaddingBottom : I
    //   3898: istore_3
    //   3899: aload_0
    //   3900: iload #43
    //   3902: iload #4
    //   3904: iload #44
    //   3906: iload_3
    //   3907: invokevirtual internalSetPadding : (IIII)V
    //   3910: iload #17
    //   3912: ifeq -> 3923
    //   3915: aload_0
    //   3916: iload #36
    //   3918: iload #17
    //   3920: invokevirtual setFlags : (II)V
    //   3923: iload #9
    //   3925: ifeq -> 3934
    //   3928: aload_0
    //   3929: aload #5
    //   3931: invokevirtual initializeScrollbarsInternal : (Landroid/content/res/TypedArray;)V
    //   3934: iload #10
    //   3936: ifeq -> 3943
    //   3939: aload_0
    //   3940: invokespecial initializeScrollIndicatorsInternal : ()V
    //   3943: aload #5
    //   3945: invokevirtual recycle : ()V
    //   3948: iload #7
    //   3950: ifeq -> 3957
    //   3953: aload_0
    //   3954: invokevirtual recomputePadding : ()V
    //   3957: iload #31
    //   3959: ifne -> 3973
    //   3962: iload #32
    //   3964: ifeq -> 3970
    //   3967: goto -> 3973
    //   3970: goto -> 3981
    //   3973: aload_0
    //   3974: iload #31
    //   3976: iload #32
    //   3978: invokevirtual scrollTo : (II)V
    //   3981: iload #6
    //   3983: ifeq -> 4043
    //   3986: aload_0
    //   3987: fload #18
    //   3989: invokevirtual setTranslationX : (F)V
    //   3992: aload_0
    //   3993: fload #19
    //   3995: invokevirtual setTranslationY : (F)V
    //   3998: aload_0
    //   3999: fload #20
    //   4001: invokevirtual setTranslationZ : (F)V
    //   4004: aload_0
    //   4005: fload #21
    //   4007: invokevirtual setElevation : (F)V
    //   4010: aload_0
    //   4011: fload #22
    //   4013: invokevirtual setRotation : (F)V
    //   4016: aload_0
    //   4017: fload #23
    //   4019: invokevirtual setRotationX : (F)V
    //   4022: aload_0
    //   4023: fload #24
    //   4025: invokevirtual setRotationY : (F)V
    //   4028: aload_0
    //   4029: fload #25
    //   4031: invokevirtual setScaleX : (F)V
    //   4034: aload_0
    //   4035: fload #26
    //   4037: invokevirtual setScaleY : (F)V
    //   4040: goto -> 4043
    //   4043: iload #35
    //   4045: ifne -> 4062
    //   4048: iload #36
    //   4050: sipush #512
    //   4053: iand
    //   4054: ifeq -> 4062
    //   4057: aload_0
    //   4058: iconst_1
    //   4059: invokevirtual setScrollContainer : (Z)V
    //   4062: aload_0
    //   4063: invokevirtual computeOpaqueFlags : ()V
    //   4066: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5541	-> 0
    //   #5543	-> 5
    //   #5545	-> 13
    //   #5548	-> 26
    //   #5549	-> 35
    //   #5552	-> 49
    //   #5553	-> 55
    //   #5556	-> 62
    //   #5558	-> 62
    //   #5559	-> 62
    //   #5560	-> 62
    //   #5561	-> 62
    //   #5562	-> 62
    //   #5563	-> 62
    //   #5565	-> 62
    //   #5566	-> 62
    //   #5567	-> 62
    //   #5569	-> 62
    //   #5570	-> 62
    //   #5572	-> 62
    //   #5574	-> 62
    //   #5575	-> 62
    //   #5577	-> 62
    //   #5578	-> 62
    //   #5579	-> 62
    //   #5580	-> 62
    //   #5581	-> 62
    //   #5582	-> 62
    //   #5583	-> 62
    //   #5584	-> 62
    //   #5585	-> 62
    //   #5586	-> 62
    //   #5588	-> 65
    //   #5589	-> 68
    //   #5590	-> 74
    //   #5591	-> 77
    //   #5593	-> 80
    //   #5594	-> 83
    //   #5595	-> 86
    //   #5596	-> 86
    //   #5598	-> 86
    //   #5601	-> 100
    //   #5602	-> 100
    //   #5604	-> 100
    //   #5605	-> 110
    //   #5606	-> 201
    //   #5607	-> 210
    //   #6097	-> 643
    //   #5977	-> 658
    //   #5978	-> 678
    //   #5979	-> 685
    //   #5978	-> 700
    //   #5981	-> 700
    //   #5982	-> 705
    //   #5981	-> 711
    //   #6065	-> 728
    //   #6066	-> 738
    //   #6094	-> 753
    //   #6095	-> 769
    //   #6088	-> 772
    //   #6089	-> 786
    //   #6085	-> 789
    //   #6086	-> 803
    //   #6091	-> 806
    //   #6092	-> 818
    //   #6080	-> 821
    //   #6081	-> 831
    //   #6080	-> 845
    //   #6075	-> 848
    //   #6076	-> 858
    //   #6075	-> 873
    //   #5607	-> 876
    //   #6070	-> 876
    //   #6071	-> 886
    //   #6070	-> 901
    //   #6060	-> 904
    //   #6061	-> 914
    //   #6060	-> 929
    //   #6025	-> 932
    //   #6026	-> 942
    //   #6027	-> 942
    //   #6029	-> 945
    //   #6030	-> 956
    //   #6033	-> 966
    //   #6036	-> 974
    //   #6034	-> 977
    //   #6035	-> 978
    //   #6037	-> 991
    //   #6038	-> 994
    //   #6041	-> 1005
    //   #6042	-> 1009
    //   #6046	-> 1014
    //   #6043	-> 1026
    //   #6041	-> 1037
    //   #6050	-> 1037
    //   #6052	-> 1044
    //   #6053	-> 1048
    //   #6054	-> 1058
    //   #6053	-> 1075
    //   #6056	-> 1081
    //   #6057	-> 1087
    //   #6025	-> 1090
    //   #6020	-> 1093
    //   #6021	-> 1103
    //   #5871	-> 1118
    //   #5872	-> 1130
    //   #6015	-> 1133
    //   #6016	-> 1143
    //   #5626	-> 1158
    //   #5627	-> 1168
    //   #5619	-> 1186
    //   #5620	-> 1196
    //   #5621	-> 1202
    //   #5622	-> 1208
    //   #5623	-> 1208
    //   #5624	-> 1211
    //   #6012	-> 1232
    //   #6013	-> 1243
    //   #6007	-> 1246
    //   #6008	-> 1256
    //   #5995	-> 1271
    //   #5996	-> 1281
    //   #5997	-> 1286
    //   #5998	-> 1286
    //   #5997	-> 1291
    //   #6000	-> 1304
    //   #6001	-> 1314
    //   #6002	-> 1320
    //   #6005	-> 1330
    //   #5742	-> 1333
    //   #5743	-> 1344
    //   #5744	-> 1351
    //   #5986	-> 1368
    //   #5987	-> 1368
    //   #5989	-> 1385
    //   #5990	-> 1390
    //   #5991	-> 1401
    //   #5790	-> 1422
    //   #5791	-> 1434
    //   #5787	-> 1437
    //   #5788	-> 1449
    //   #5951	-> 1452
    //   #5953	-> 1464
    //   #5965	-> 1467
    //   #5966	-> 1481
    //   #5967	-> 1481
    //   #5966	-> 1494
    //   #5972	-> 1502
    //   #5973	-> 1516
    //   #5943	-> 1530
    //   #5944	-> 1537
    //   #5946	-> 1548
    //   #5948	-> 1567
    //   #5949	-> 1575
    //   #5934	-> 1578
    //   #5935	-> 1585
    //   #5937	-> 1596
    //   #5939	-> 1610
    //   #5940	-> 1618
    //   #5929	-> 1621
    //   #5930	-> 1621
    //   #5929	-> 1631
    //   #5931	-> 1641
    //   #5680	-> 1644
    //   #5681	-> 1654
    //   #5682	-> 1654
    //   #5926	-> 1671
    //   #5927	-> 1683
    //   #5923	-> 1686
    //   #5924	-> 1697
    //   #5676	-> 1700
    //   #5677	-> 1710
    //   #5678	-> 1710
    //   #5920	-> 1727
    //   #5921	-> 1739
    //   #5793	-> 1742
    //   #5794	-> 1754
    //   #5649	-> 1757
    //   #5650	-> 1769
    //   #5651	-> 1786
    //   #5645	-> 1808
    //   #5646	-> 1820
    //   #5647	-> 1837
    //   #5768	-> 1859
    //   #5771	-> 1870
    //   #5772	-> 1880
    //   #5773	-> 1886
    //   #5774	-> 1900
    //   #5775	-> 1913
    //   #5910	-> 1916
    //   #5912	-> 1928
    //   #5913	-> 1938
    //   #5914	-> 1953
    //   #5901	-> 1956
    //   #5903	-> 1968
    //   #5904	-> 1978
    //   #5905	-> 1984
    //   #5916	-> 2002
    //   #5918	-> 2014
    //   #5607	-> 2017
    //   #5897	-> 2020
    //   #5898	-> 2033
    //   #5868	-> 2036
    //   #5869	-> 2048
    //   #5894	-> 2051
    //   #5895	-> 2063
    //   #5692	-> 2066
    //   #5693	-> 2076
    //   #5694	-> 2076
    //   #5688	-> 2093
    //   #5689	-> 2103
    //   #5690	-> 2103
    //   #5684	-> 2120
    //   #5685	-> 2130
    //   #5686	-> 2130
    //   #5700	-> 2147
    //   #5701	-> 2157
    //   #5702	-> 2157
    //   #5696	-> 2174
    //   #5697	-> 2184
    //   #5698	-> 2184
    //   #5672	-> 2201
    //   #5673	-> 2211
    //   #5674	-> 2211
    //   #5668	-> 2228
    //   #5669	-> 2238
    //   #5670	-> 2238
    //   #5665	-> 2255
    //   #5666	-> 2267
    //   #5662	-> 2270
    //   #5663	-> 2282
    //   #5659	-> 2285
    //   #5660	-> 2297
    //   #5850	-> 2300
    //   #5851	-> 2311
    //   #5852	-> 2311
    //   #5891	-> 2337
    //   #5892	-> 2347
    //   #5784	-> 2365
    //   #5785	-> 2376
    //   #5880	-> 2379
    //   #5885	-> 2386
    //   #5886	-> 2394
    //   #5887	-> 2398
    //   #5881	-> 2414
    //   #5802	-> 2425
    //   #5803	-> 2436
    //   #5804	-> 2436
    //   #5838	-> 2462
    //   #5839	-> 2462
    //   #5840	-> 2473
    //   #5605	-> 2478
    //   #5844	-> 2499
    //   #5845	-> 2510
    //   #5846	-> 2518
    //   #5796	-> 2536
    //   #5797	-> 2547
    //   #5798	-> 2547
    //   #5960	-> 2573
    //   #5961	-> 2587
    //   #5877	-> 2602
    //   #5878	-> 2614
    //   #5874	-> 2617
    //   #5875	-> 2629
    //   #5955	-> 2632
    //   #5956	-> 2646
    //   #5754	-> 2660
    //   #5755	-> 2671
    //   #5756	-> 2678
    //   #5777	-> 2695
    //   #5778	-> 2705
    //   #5779	-> 2710
    //   #5780	-> 2721
    //   #5748	-> 2738
    //   #5749	-> 2749
    //   #5750	-> 2757
    //   #5736	-> 2775
    //   #5737	-> 2786
    //   #5738	-> 2794
    //   #5730	-> 2812
    //   #5731	-> 2823
    //   #5732	-> 2823
    //   #5865	-> 2849
    //   #5866	-> 2861
    //   #5862	-> 2864
    //   #5863	-> 2876
    //   #5859	-> 2879
    //   #5860	-> 2891
    //   #5856	-> 2894
    //   #5857	-> 2906
    //   #5817	-> 2909
    //   #5819	-> 2916
    //   #5823	-> 2919
    //   #5824	-> 2929
    //   #5825	-> 2934
    //   #5826	-> 2941
    //   #5827	-> 2941
    //   #5808	-> 2965
    //   #5809	-> 2975
    //   #5810	-> 2980
    //   #5811	-> 2987
    //   #5812	-> 2987
    //   #5710	-> 3008
    //   #5711	-> 3019
    //   #5712	-> 3019
    //   #5760	-> 3041
    //   #5761	-> 3051
    //   #5762	-> 3056
    //   #5763	-> 3067
    //   #5722	-> 3084
    //   #5724	-> 3095
    //   #5725	-> 3095
    //   #5726	-> 3095
    //   #5716	-> 3124
    //   #5717	-> 3138
    //   #5718	-> 3146
    //   #5717	-> 3163
    //   #5642	-> 3177
    //   #5643	-> 3187
    //   #5637	-> 3205
    //   #5638	-> 3215
    //   #5639	-> 3221
    //   #5640	-> 3221
    //   #5634	-> 3242
    //   #5635	-> 3252
    //   #5629	-> 3270
    //   #5630	-> 3280
    //   #5631	-> 3286
    //   #5632	-> 3286
    //   #5612	-> 3307
    //   #5613	-> 3316
    //   #5614	-> 3321
    //   #5615	-> 3326
    //   #5616	-> 3326
    //   #5617	-> 3329
    //   #5609	-> 3350
    //   #5610	-> 3358
    //   #5656	-> 3373
    //   #5657	-> 3383
    //   #5653	-> 3401
    //   #5654	-> 3411
    //   #5707	-> 3429
    //   #5708	-> 3440
    //   #5704	-> 3443
    //   #5705	-> 3455
    //   #5831	-> 3458
    //   #5832	-> 3468
    //   #5833	-> 3473
    //   #5834	-> 3484
    //   #5832	-> 3502
    //   #5605	-> 3520
    //   #6102	-> 3559
    //   #6107	-> 3565
    //   #6108	-> 3571
    //   #6110	-> 3577
    //   #6111	-> 3582
    //   #6116	-> 3588
    //   #6117	-> 3594
    //   #6121	-> 3600
    //   #6122	-> 3604
    //   #6123	-> 3604
    //   #6124	-> 3607
    //   #6125	-> 3607
    //   #6126	-> 3607
    //   #6127	-> 3612
    //   #6129	-> 3632
    //   #6130	-> 3637
    //   #6131	-> 3640
    //   #6132	-> 3644
    //   #6133	-> 3650
    //   #6129	-> 3659
    //   #6135	-> 3666
    //   #6136	-> 3671
    //   #6137	-> 3675
    //   #6135	-> 3692
    //   #6141	-> 3706
    //   #6149	-> 3713
    //   #6150	-> 3733
    //   #6152	-> 3737
    //   #6153	-> 3757
    //   #6154	-> 3777
    //   #6156	-> 3781
    //   #6163	-> 3808
    //   #6165	-> 3825
    //   #6166	-> 3837
    //   #6168	-> 3843
    //   #6169	-> 3855
    //   #6175	-> 3861
    //   #6177	-> 3867
    //   #6179	-> 3887
    //   #6175	-> 3899
    //   #6181	-> 3910
    //   #6182	-> 3915
    //   #6185	-> 3923
    //   #6186	-> 3928
    //   #6189	-> 3934
    //   #6190	-> 3939
    //   #6193	-> 3943
    //   #6196	-> 3948
    //   #6197	-> 3953
    //   #6200	-> 3957
    //   #6201	-> 3973
    //   #6204	-> 3981
    //   #6205	-> 3986
    //   #6206	-> 3992
    //   #6207	-> 3998
    //   #6208	-> 4004
    //   #6209	-> 4010
    //   #6210	-> 4016
    //   #6211	-> 4022
    //   #6212	-> 4028
    //   #6213	-> 4034
    //   #6204	-> 4043
    //   #6216	-> 4043
    //   #6217	-> 4057
    //   #6220	-> 4062
    //   #6222	-> 4066
    // Exception table:
    //   from	to	target	type
    //   966	974	977	android/content/res/Resources$NotFoundException
  }
  
  public int[] getAttributeResolutionStack(int paramInt) {
    if (sDebugViewAttributes) {
      SparseArray<int[]> sparseArray = this.mAttributeResolutionStacks;
      if (sparseArray != null)
        if (sparseArray.get(paramInt) != null) {
          int[] arrayOfInt1 = this.mAttributeResolutionStacks.get(paramInt);
          int i = arrayOfInt1.length;
          paramInt = i;
          if (this.mSourceLayoutId != 0)
            paramInt = i + 1; 
          i = 0;
          int[] arrayOfInt2 = new int[paramInt];
          int j = this.mSourceLayoutId;
          paramInt = i;
          if (j != 0) {
            arrayOfInt2[0] = j;
            paramInt = 0 + 1;
          } 
          for (i = 0; i < arrayOfInt1.length; i++) {
            arrayOfInt2[paramInt] = arrayOfInt1[i];
            paramInt++;
          } 
          return arrayOfInt2;
        }  
    } 
    return new int[0];
  }
  
  public Map<Integer, Integer> getAttributeSourceResourceMap() {
    HashMap<Object, Object> hashMap = new HashMap<>();
    if (!sDebugViewAttributes || this.mAttributeSourceResId == null)
      return (Map)hashMap; 
    for (byte b = 0; b < this.mAttributeSourceResId.size(); b++)
      hashMap.put(Integer.valueOf(this.mAttributeSourceResId.keyAt(b)), Integer.valueOf(this.mAttributeSourceResId.valueAt(b))); 
    return (Map)hashMap;
  }
  
  public int getExplicitStyle() {
    if (!sDebugViewAttributes)
      return 0; 
    return this.mExplicitStyle;
  }
  
  class DeclaredOnClickListener implements OnClickListener {
    private final View mHostView;
    
    private final String mMethodName;
    
    private Context mResolvedContext;
    
    private Method mResolvedMethod;
    
    public DeclaredOnClickListener(View this$0, String param1String) {
      this.mHostView = this$0;
      this.mMethodName = param1String;
    }
    
    public void onClick(View param1View) {
      if (this.mResolvedMethod == null)
        resolveMethod(this.mHostView.getContext(), this.mMethodName); 
      try {
        this.mResolvedMethod.invoke(this.mResolvedContext, new Object[] { param1View });
        return;
      } catch (IllegalAccessException illegalAccessException) {
        throw new IllegalStateException("Could not execute non-public method for android:onClick", illegalAccessException);
      } catch (InvocationTargetException invocationTargetException) {
        throw new IllegalStateException("Could not execute method for android:onClick", invocationTargetException);
      } 
    }
    
    private void resolveMethod(Context param1Context, String param1String) {
      String str;
      while (param1Context != null) {
        try {
          if (!param1Context.isRestricted()) {
            Method method = param1Context.getClass().getMethod(this.mMethodName, new Class[] { View.class });
            if (method != null) {
              this.mResolvedMethod = method;
              this.mResolvedContext = param1Context;
              return;
            } 
          } 
        } catch (NoSuchMethodException noSuchMethodException) {}
        if (param1Context instanceof ContextWrapper) {
          param1Context = ((ContextWrapper)param1Context).getBaseContext();
          continue;
        } 
        param1Context = null;
      } 
      int i = this.mHostView.getId();
      if (i == -1) {
        str = "";
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(" with id '");
        stringBuilder1.append(this.mHostView.getContext().getResources().getResourceEntryName(i));
        stringBuilder1.append("'");
        str = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not find method ");
      stringBuilder.append(this.mMethodName);
      stringBuilder.append("(View) in a parent or ancestor Context for android:onClick attribute defined on view ");
      View view = this.mHostView;
      stringBuilder.append(view.getClass());
      stringBuilder.append(str);
      throw new IllegalStateException(stringBuilder.toString());
    }
  }
  
  View() {
    InputEventConsistencyVerifier inputEventConsistencyVerifier;
    this.mIgnoreOnDescendantInvalidated = false;
    this.mCurrentAnimation = null;
    this.mRecreateDisplayList = false;
    this.mID = -1;
    this.mAutofillViewId = -1;
    this.mAccessibilityViewId = -1;
    this.mAccessibilityCursorPosition = -1;
    this.mTag = null;
    this.mTransientStateCount = 0;
    this.mClipBounds = null;
    this.mPaddingLeft = 0;
    this.mPaddingRight = 0;
    this.mLabelForId = -1;
    this.mAccessibilityTraversalBeforeId = -1;
    this.mAccessibilityTraversalAfterId = -1;
    this.mLeftPaddingDefined = false;
    this.mRightPaddingDefined = false;
    this.mOldWidthMeasureSpec = Integer.MIN_VALUE;
    this.mOldHeightMeasureSpec = Integer.MIN_VALUE;
    this.mLongClickX = Float.NaN;
    this.mLongClickY = Float.NaN;
    this.mDrawableState = null;
    this.mOutlineProvider = ViewOutlineProvider.BACKGROUND;
    this.mNextFocusLeftId = -1;
    this.mNextFocusRightId = -1;
    this.mNextFocusUpId = -1;
    this.mNextFocusDownId = -1;
    this.mNextFocusForwardId = -1;
    this.mNextClusterForwardId = -1;
    this.mDefaultFocusHighlightEnabled = true;
    this.mPendingCheckForTap = null;
    this.mTouchDelegate = null;
    this.mHoveringTouchDelegate = false;
    this.mDrawingCacheBackgroundColor = 0;
    this.mAnimator = null;
    this.mLayerType = 0;
    if (InputEventConsistencyVerifier.isInstrumentationEnabled()) {
      inputEventConsistencyVerifier = new InputEventConsistencyVerifier(this, 0);
    } else {
      inputEventConsistencyVerifier = null;
    } 
    this.mInputEventConsistencyVerifier = inputEventConsistencyVerifier;
    this.mSourceLayoutId = 0;
    this.mIsPreScrollConsumed = false;
    this.mUnbufferedInputSource = 0;
    this.mHasInit = false;
    this.mPendingLayerType = 0;
    this.mForceHWSize = SystemProperties.getInt("debug.imageview.forcehw.size", 16777216);
    this.mResources = null;
    this.mViewHooks = (IOplusViewHooks)ColorFrameworkFactory.getInstance().getFeature(IOplusViewHooks.DEFAULT, new Object[] { this, this.mResources });
    this.mRenderNode = RenderNode.create(getClass().getName(), new ViewAnimationHostBridge(this));
  }
  
  final boolean debugWebViewDraw() {
    if (this instanceof android.webkit.WebView)
      return mDebugWebView; 
    return false;
  }
  
  public final boolean isShowingLayoutBounds() {
    if (!DEBUG_DRAW) {
      AttachInfo attachInfo = this.mAttachInfo;
      return ((attachInfo != null && attachInfo.mDebugLayout) || debugWebViewDraw());
    } 
    return true;
  }
  
  public final void setShowingLayoutBounds(boolean paramBoolean) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      attachInfo.mDebugLayout = paramBoolean; 
  }
  
  private static SparseArray<String> getAttributeMap() {
    if (mAttributeMap == null)
      mAttributeMap = new SparseArray<>(); 
    return mAttributeMap;
  }
  
  private void retrieveExplicitStyle(Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    if (!sDebugViewAttributes)
      return; 
    this.mExplicitStyle = paramTheme.getExplicitStyle(paramAttributeSet);
  }
  
  public final void saveAttributeDataForStyleable(Context paramContext, int[] paramArrayOfint, AttributeSet paramAttributeSet, TypedArray paramTypedArray, int paramInt1, int paramInt2) {
    if (!sDebugViewAttributes)
      return; 
    int[] arrayOfInt = paramContext.getTheme().getAttributeResolutionStack(paramInt1, paramInt2, this.mExplicitStyle);
    if (this.mAttributeResolutionStacks == null)
      this.mAttributeResolutionStacks = (SparseArray)new SparseArray<>(); 
    if (this.mAttributeSourceResId == null)
      this.mAttributeSourceResId = new SparseIntArray(); 
    paramInt2 = paramTypedArray.getIndexCount();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      int i = paramTypedArray.getIndex(paramInt1);
      this.mAttributeSourceResId.append(paramArrayOfint[i], paramTypedArray.getSourceResourceId(i, 0));
      this.mAttributeResolutionStacks.append(paramArrayOfint[i], arrayOfInt);
    } 
  }
  
  private void saveAttributeData(AttributeSet paramAttributeSet, TypedArray paramTypedArray) {
    if (paramAttributeSet == null) {
      i = 0;
    } else {
      i = paramAttributeSet.getAttributeCount();
    } 
    int j = paramTypedArray.getIndexCount();
    String[] arrayOfString2 = new String[(i + j) * 2];
    int k = 0;
    int m;
    for (m = 0; m < i; m++) {
      arrayOfString2[k] = paramAttributeSet.getAttributeName(m);
      arrayOfString2[k + 1] = paramAttributeSet.getAttributeValue(m);
      k += 2;
    } 
    Resources resources = paramTypedArray.getResources();
    SparseArray<String> sparseArray = getAttributeMap();
    int i;
    for (m = 0, i = k, k = m; k < j; k++) {
      m = paramTypedArray.getIndex(k);
      if (paramTypedArray.hasValueOrEmpty(m)) {
        int n = paramTypedArray.getResourceId(m, 0);
        if (n != 0) {
          String str2 = sparseArray.get(n);
          String str1 = str2;
          if (str2 == null) {
            try {
              str1 = resources.getResourceName(n);
            } catch (android.content.res.Resources.NotFoundException notFoundException) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("0x");
              stringBuilder.append(Integer.toHexString(n));
              str1 = stringBuilder.toString();
            } 
            sparseArray.put(n, str1);
          } 
          arrayOfString2[i] = str1;
          arrayOfString2[i + 1] = paramTypedArray.getString(m);
          i += 2;
        } 
      } 
    } 
    String[] arrayOfString1 = new String[i];
    System.arraycopy(arrayOfString2, 0, arrayOfString1, 0, i);
    this.mAttributes = arrayOfString1;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append(getClass().getName());
    stringBuilder.append('{');
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(' ');
    int i = this.mViewFlags & 0xC;
    byte b1 = 73, b2 = 86, b3 = 46;
    if (i != 0) {
      if (i != 4) {
        if (i != 8) {
          stringBuilder.append('.');
        } else {
          stringBuilder.append('G');
        } 
      } else {
        stringBuilder.append('I');
      } 
    } else {
      stringBuilder.append('V');
    } 
    i = this.mViewFlags;
    byte b4 = 70;
    if ((i & 0x1) == 1) {
      b5 = 70;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mViewFlags & 0x20) == 0) {
      b5 = 69;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mViewFlags & 0x80) == 128) {
      b5 = 46;
    } else {
      b5 = 68;
    } 
    stringBuilder.append(b5);
    i = this.mViewFlags;
    byte b6 = 72;
    if ((i & 0x100) != 0) {
      b5 = 72;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mViewFlags & 0x200) != 0) {
      b5 = b2;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mViewFlags & 0x4000) != 0) {
      b5 = 67;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mViewFlags & 0x200000) != 0) {
      b5 = 76;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mViewFlags & 0x800000) != 0) {
      b5 = 88;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    stringBuilder.append(' ');
    if ((this.mPrivateFlags & 0x8) != 0) {
      b5 = 82;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mPrivateFlags & 0x2) != 0) {
      b5 = b4;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mPrivateFlags & 0x4) != 0) {
      b5 = 83;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    i = this.mPrivateFlags;
    if ((0x2000000 & i) != 0) {
      stringBuilder.append('p');
    } else {
      if ((i & 0x4000) != 0) {
        b5 = 80;
      } else {
        b5 = 46;
      } 
      stringBuilder.append(b5);
    } 
    if ((this.mPrivateFlags & 0x10000000) != 0) {
      b5 = b6;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mPrivateFlags & 0x40000000) != 0) {
      b5 = 65;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    if ((this.mPrivateFlags & Integer.MIN_VALUE) != 0) {
      b5 = b1;
    } else {
      b5 = 46;
    } 
    stringBuilder.append(b5);
    byte b5 = b3;
    if ((this.mPrivateFlags & 0x200000) != 0)
      b5 = 68; 
    stringBuilder.append(b5);
    stringBuilder.append(' ');
    stringBuilder.append(this.mLeft);
    stringBuilder.append(',');
    stringBuilder.append(this.mTop);
    stringBuilder.append('-');
    stringBuilder.append(this.mRight);
    stringBuilder.append(',');
    stringBuilder.append(this.mBottom);
    int j = getId();
    if (j != -1) {
      stringBuilder.append(" #");
      stringBuilder.append(Integer.toHexString(j));
      Resources resources = this.mResources;
      if (j > 0 && Resources.resourceHasPackage(j) && resources != null) {
        i = 0xFF000000 & j;
        if (i != 16777216) {
          if (i != 2130706432) {
            try {
              String str1 = resources.getResourcePackageName(j);
              String str2 = resources.getResourceTypeName(j);
            } catch (android.content.res.Resources.NotFoundException notFoundException) {}
          } else {
            String str1 = "app";
            String str2 = resources.getResourceTypeName(j);
          } 
        } else {
          String str1 = "android";
          String str2 = resources.getResourceTypeName(j);
        } 
      } 
    } 
    if (this.mAutofillId != null) {
      stringBuilder.append(" aid=");
      stringBuilder.append(this.mAutofillId);
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  protected void initializeFadingEdge(TypedArray paramTypedArray) {
    paramTypedArray = this.mContext.obtainStyledAttributes(R.styleable.View);
    initializeFadingEdgeInternal(paramTypedArray);
    paramTypedArray.recycle();
  }
  
  protected void initializeFadingEdgeInternal(TypedArray paramTypedArray) {
    initScrollCache();
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    Context context = this.mContext;
    int i = ViewConfiguration.get(context).getScaledFadingEdgeLength();
    scrollabilityCache.fadingEdgeLength = paramTypedArray.getDimensionPixelSize(25, i);
  }
  
  public int getVerticalFadingEdgeLength() {
    if (isVerticalFadingEdgeEnabled()) {
      ScrollabilityCache scrollabilityCache = this.mScrollCache;
      if (scrollabilityCache != null)
        return scrollabilityCache.fadingEdgeLength; 
    } 
    return 0;
  }
  
  public void setFadingEdgeLength(int paramInt) {
    initScrollCache();
    this.mScrollCache.fadingEdgeLength = paramInt;
  }
  
  public int getHorizontalFadingEdgeLength() {
    if (isHorizontalFadingEdgeEnabled()) {
      ScrollabilityCache scrollabilityCache = this.mScrollCache;
      if (scrollabilityCache != null)
        return scrollabilityCache.fadingEdgeLength; 
    } 
    return 0;
  }
  
  public int getVerticalScrollbarWidth() {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null) {
      ScrollBarDrawable scrollBarDrawable = scrollabilityCache.scrollBar;
      if (scrollBarDrawable != null) {
        int i = scrollBarDrawable.getSize(true);
        int j = i;
        if (i <= 0)
          j = scrollabilityCache.scrollBarSize; 
        return j;
      } 
      return 0;
    } 
    return 0;
  }
  
  protected int getHorizontalScrollbarHeight() {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null) {
      ScrollBarDrawable scrollBarDrawable = scrollabilityCache.scrollBar;
      if (scrollBarDrawable != null) {
        int i = scrollBarDrawable.getSize(false);
        int j = i;
        if (i <= 0)
          j = scrollabilityCache.scrollBarSize; 
        return j;
      } 
      return 0;
    } 
    return 0;
  }
  
  protected void initializeScrollbars(TypedArray paramTypedArray) {
    paramTypedArray = this.mContext.obtainStyledAttributes(R.styleable.View);
    initializeScrollbarsInternal(paramTypedArray);
    paramTypedArray.recycle();
  }
  
  private void initializeScrollBarDrawable() {
    initScrollCache();
    if (this.mScrollCache.scrollBar == null) {
      this.mScrollCache.scrollBar = new ScrollBarDrawable();
      this.mScrollCache.scrollBar.setState(getDrawableState());
      this.mScrollCache.scrollBar.setCallback(this);
    } 
  }
  
  protected void initializeScrollbarsInternal(TypedArray paramTypedArray) {
    initScrollCache();
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache.scrollBar == null) {
      scrollabilityCache.scrollBar = new ScrollBarDrawable(this.mViewHooks.getScrollBarEffect());
      scrollabilityCache.scrollBar.setState(getDrawableState());
      scrollabilityCache.scrollBar.setCallback(this);
    } 
    boolean bool = paramTypedArray.getBoolean(47, true);
    if (!bool)
      scrollabilityCache.state = 1; 
    scrollabilityCache.fadeScrollBars = bool;
    int i = ViewConfiguration.getScrollBarFadeDuration();
    scrollabilityCache.scrollBarFadeDuration = paramTypedArray.getInt(45, i);
    i = ViewConfiguration.getScrollDefaultDelay();
    scrollabilityCache.scrollBarDefaultDelayBeforeFade = paramTypedArray.getInt(46, i);
    Context context = this.mContext;
    i = ViewConfiguration.get(context).getScaledScrollBarSize();
    scrollabilityCache.scrollBarSize = paramTypedArray.getDimensionPixelSize(1, i);
    Drawable drawable1 = paramTypedArray.getDrawable(4);
    scrollabilityCache.scrollBar.setHorizontalTrackDrawable(drawable1);
    drawable1 = paramTypedArray.getDrawable(2);
    if (drawable1 != null)
      scrollabilityCache.scrollBar.setHorizontalThumbDrawable(drawable1); 
    bool = paramTypedArray.getBoolean(6, false);
    if (bool)
      scrollabilityCache.scrollBar.setAlwaysDrawHorizontalTrack(true); 
    drawable1 = paramTypedArray.getDrawable(5);
    scrollabilityCache.scrollBar.setVerticalTrackDrawable(drawable1);
    Drawable drawable2 = paramTypedArray.getDrawable(3);
    if (drawable2 != null)
      scrollabilityCache.scrollBar.setVerticalThumbDrawable(drawable2); 
    bool = paramTypedArray.getBoolean(7, false);
    if (bool)
      scrollabilityCache.scrollBar.setAlwaysDrawVerticalTrack(true); 
    i = getLayoutDirection();
    if (drawable1 != null)
      drawable1.setLayoutDirection(i); 
    if (drawable2 != null)
      drawable2.setLayoutDirection(i); 
    resolvePadding();
  }
  
  public void setVerticalScrollbarThumbDrawable(Drawable paramDrawable) {
    initializeScrollBarDrawable();
    this.mScrollCache.scrollBar.setVerticalThumbDrawable(paramDrawable);
  }
  
  public void setVerticalScrollbarTrackDrawable(Drawable paramDrawable) {
    initializeScrollBarDrawable();
    this.mScrollCache.scrollBar.setVerticalTrackDrawable(paramDrawable);
  }
  
  public void setHorizontalScrollbarThumbDrawable(Drawable paramDrawable) {
    initializeScrollBarDrawable();
    this.mScrollCache.scrollBar.setHorizontalThumbDrawable(paramDrawable);
  }
  
  public void setHorizontalScrollbarTrackDrawable(Drawable paramDrawable) {
    initializeScrollBarDrawable();
    this.mScrollCache.scrollBar.setHorizontalTrackDrawable(paramDrawable);
  }
  
  public Drawable getVerticalScrollbarThumbDrawable() {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null) {
      Drawable drawable = scrollabilityCache.scrollBar.getVerticalThumbDrawable();
    } else {
      scrollabilityCache = null;
    } 
    return (Drawable)scrollabilityCache;
  }
  
  public Drawable getVerticalScrollbarTrackDrawable() {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null) {
      Drawable drawable = scrollabilityCache.scrollBar.getVerticalTrackDrawable();
    } else {
      scrollabilityCache = null;
    } 
    return (Drawable)scrollabilityCache;
  }
  
  public Drawable getHorizontalScrollbarThumbDrawable() {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null) {
      Drawable drawable = scrollabilityCache.scrollBar.getHorizontalThumbDrawable();
    } else {
      scrollabilityCache = null;
    } 
    return (Drawable)scrollabilityCache;
  }
  
  public Drawable getHorizontalScrollbarTrackDrawable() {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null) {
      Drawable drawable = scrollabilityCache.scrollBar.getHorizontalTrackDrawable();
    } else {
      scrollabilityCache = null;
    } 
    return (Drawable)scrollabilityCache;
  }
  
  private void initializeScrollIndicatorsInternal() {
    if (this.mScrollIndicatorDrawable == null)
      this.mScrollIndicatorDrawable = this.mContext.getDrawable(17303434); 
  }
  
  private void initScrollCache() {
    if (this.mScrollCache == null)
      this.mScrollCache = new ScrollabilityCache(ViewConfiguration.get(this.mContext), this); 
  }
  
  private ScrollabilityCache getScrollCache() {
    initScrollCache();
    return this.mScrollCache;
  }
  
  public void setVerticalScrollbarPosition(int paramInt) {
    if (this.mVerticalScrollbarPosition != paramInt) {
      this.mVerticalScrollbarPosition = paramInt;
      computeOpaqueFlags();
      resolvePadding();
    } 
  }
  
  public int getVerticalScrollbarPosition() {
    return this.mVerticalScrollbarPosition;
  }
  
  boolean isOnScrollbar(float paramFloat1, float paramFloat2) {
    boolean bool;
    if (this.mScrollCache == null)
      return false; 
    paramFloat1 += getScrollX();
    paramFloat2 += getScrollY();
    if (computeVerticalScrollRange() > computeVerticalScrollExtent()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (isVerticalScrollBarEnabled() && !isVerticalScrollBarHidden() && bool) {
      Rect rect = this.mScrollCache.mScrollBarTouchBounds;
      getVerticalScrollBarBounds((Rect)null, rect);
      if (rect.contains((int)paramFloat1, (int)paramFloat2))
        return true; 
    } 
    if (computeHorizontalScrollRange() > computeHorizontalScrollExtent()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (isHorizontalScrollBarEnabled() && bool) {
      Rect rect = this.mScrollCache.mScrollBarTouchBounds;
      getHorizontalScrollBarBounds((Rect)null, rect);
      if (rect.contains((int)paramFloat1, (int)paramFloat2))
        return true; 
    } 
    return false;
  }
  
  boolean isOnScrollbarThumb(float paramFloat1, float paramFloat2) {
    return (isOnVerticalScrollbarThumb(paramFloat1, paramFloat2) || isOnHorizontalScrollbarThumb(paramFloat1, paramFloat2));
  }
  
  private boolean isOnVerticalScrollbarThumb(float paramFloat1, float paramFloat2) {
    if (this.mScrollCache == null || !isVerticalScrollBarEnabled() || isVerticalScrollBarHidden())
      return false; 
    int i = computeVerticalScrollRange();
    int j = computeVerticalScrollExtent();
    if (i > j) {
      paramFloat1 += getScrollX();
      paramFloat2 += getScrollY();
      Rect rect1 = this.mScrollCache.mScrollBarBounds;
      Rect rect2 = this.mScrollCache.mScrollBarTouchBounds;
      getVerticalScrollBarBounds(rect1, rect2);
      int k = computeVerticalScrollOffset();
      int m = ScrollBarUtils.getThumbLength(rect1.height(), rect1.width(), j, i);
      k = ScrollBarUtils.getThumbOffset(rect1.height(), m, j, i, k);
      i = rect1.top + k;
      k = Math.max(this.mScrollCache.scrollBarMinTouchTarget - m, 0) / 2;
      if (paramFloat1 >= rect2.left && paramFloat1 <= rect2.right && paramFloat2 >= (i - k) && paramFloat2 <= (i + m + k))
        return true; 
    } 
    return false;
  }
  
  private boolean isOnHorizontalScrollbarThumb(float paramFloat1, float paramFloat2) {
    if (this.mScrollCache == null || !isHorizontalScrollBarEnabled())
      return false; 
    int i = computeHorizontalScrollRange();
    int j = computeHorizontalScrollExtent();
    if (i > j) {
      paramFloat1 += getScrollX();
      paramFloat2 += getScrollY();
      Rect rect1 = this.mScrollCache.mScrollBarBounds;
      Rect rect2 = this.mScrollCache.mScrollBarTouchBounds;
      getHorizontalScrollBarBounds(rect1, rect2);
      int k = computeHorizontalScrollOffset();
      int m = ScrollBarUtils.getThumbLength(rect1.width(), rect1.height(), j, i);
      i = ScrollBarUtils.getThumbOffset(rect1.width(), m, j, i, k);
      i = rect1.left + i;
      j = Math.max(this.mScrollCache.scrollBarMinTouchTarget - m, 0) / 2;
      if (paramFloat1 >= (i - j) && paramFloat1 <= (i + m + j) && paramFloat2 >= rect2.top && paramFloat2 <= rect2.bottom)
        return true; 
    } 
    return false;
  }
  
  boolean isDraggingScrollBar() {
    boolean bool;
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null && scrollabilityCache.mScrollBarDraggingState != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setScrollIndicators(int paramInt) {
    setScrollIndicators(paramInt, 63);
  }
  
  public void setScrollIndicators(int paramInt1, int paramInt2) {
    int i = paramInt2 << 8 & 0x3F00;
    paramInt1 = paramInt1 << 8 & i;
    paramInt2 = this.mPrivateFlags3;
    i = (i ^ 0xFFFFFFFF) & paramInt2 | paramInt1;
    if (paramInt2 != i) {
      this.mPrivateFlags3 = i;
      if (paramInt1 != 0)
        initializeScrollIndicatorsInternal(); 
      invalidate();
    } 
  }
  
  public int getScrollIndicators() {
    return (this.mPrivateFlags3 & 0x3F00) >>> 8;
  }
  
  ListenerInfo getListenerInfo() {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null)
      return listenerInfo; 
    this.mListenerInfo = listenerInfo = new ListenerInfo();
    return listenerInfo;
  }
  
  public void setOnScrollChangeListener(OnScrollChangeListener paramOnScrollChangeListener) {
    (getListenerInfo()).mOnScrollChangeListener = paramOnScrollChangeListener;
  }
  
  public void setOnFocusChangeListener(OnFocusChangeListener paramOnFocusChangeListener) {
    (getListenerInfo()).mOnFocusChangeListener = paramOnFocusChangeListener;
  }
  
  public void addOnLayoutChangeListener(OnLayoutChangeListener paramOnLayoutChangeListener) {
    ListenerInfo listenerInfo = getListenerInfo();
    if (listenerInfo.mOnLayoutChangeListeners == null)
      ListenerInfo.access$202(listenerInfo, new ArrayList()); 
    if (!listenerInfo.mOnLayoutChangeListeners.contains(paramOnLayoutChangeListener))
      listenerInfo.mOnLayoutChangeListeners.add(paramOnLayoutChangeListener); 
  }
  
  public void removeOnLayoutChangeListener(OnLayoutChangeListener paramOnLayoutChangeListener) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo == null || listenerInfo.mOnLayoutChangeListeners == null)
      return; 
    listenerInfo.mOnLayoutChangeListeners.remove(paramOnLayoutChangeListener);
  }
  
  public void addOnAttachStateChangeListener(OnAttachStateChangeListener paramOnAttachStateChangeListener) {
    ListenerInfo listenerInfo = getListenerInfo();
    if (listenerInfo.mOnAttachStateChangeListeners == null)
      ListenerInfo.access$302(listenerInfo, new CopyOnWriteArrayList()); 
    listenerInfo.mOnAttachStateChangeListeners.add(paramOnAttachStateChangeListener);
  }
  
  public void removeOnAttachStateChangeListener(OnAttachStateChangeListener paramOnAttachStateChangeListener) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo == null || listenerInfo.mOnAttachStateChangeListeners == null)
      return; 
    listenerInfo.mOnAttachStateChangeListeners.remove(paramOnAttachStateChangeListener);
  }
  
  public OnFocusChangeListener getOnFocusChangeListener() {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null) {
      OnFocusChangeListener onFocusChangeListener = listenerInfo.mOnFocusChangeListener;
    } else {
      listenerInfo = null;
    } 
    return (OnFocusChangeListener)listenerInfo;
  }
  
  public void setOnClickListener(OnClickListener paramOnClickListener) {
    if (!isClickable())
      setClickable(true); 
    (getListenerInfo()).mOnClickListener = paramOnClickListener;
  }
  
  public boolean hasOnClickListeners() {
    boolean bool;
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnClickListener != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setOnLongClickListener(OnLongClickListener paramOnLongClickListener) {
    if (!isLongClickable())
      setLongClickable(true); 
    (getListenerInfo()).mOnLongClickListener = paramOnLongClickListener;
  }
  
  public boolean hasOnLongClickListeners() {
    boolean bool;
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnLongClickListener != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public OnLongClickListener getOnLongClickListener() {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null) {
      OnLongClickListener onLongClickListener = listenerInfo.mOnLongClickListener;
    } else {
      listenerInfo = null;
    } 
    return (OnLongClickListener)listenerInfo;
  }
  
  public void setOnContextClickListener(OnContextClickListener paramOnContextClickListener) {
    if (!isContextClickable())
      setContextClickable(true); 
    (getListenerInfo()).mOnContextClickListener = paramOnContextClickListener;
  }
  
  public void setOnCreateContextMenuListener(OnCreateContextMenuListener paramOnCreateContextMenuListener) {
    if (!isLongClickable())
      setLongClickable(true); 
    (getListenerInfo()).mOnCreateContextMenuListener = paramOnCreateContextMenuListener;
  }
  
  public void addFrameMetricsListener(Window paramWindow, Window.OnFrameMetricsAvailableListener paramOnFrameMetricsAvailableListener, Handler paramHandler) {
    FrameMetricsObserver frameMetricsObserver;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      if (attachInfo.mThreadedRenderer != null) {
        if (this.mFrameMetricsObservers == null)
          this.mFrameMetricsObservers = new ArrayList<>(); 
        frameMetricsObserver = new FrameMetricsObserver(paramWindow, paramHandler, paramOnFrameMetricsAvailableListener);
        this.mFrameMetricsObservers.add(frameMetricsObserver);
        this.mAttachInfo.mThreadedRenderer.addObserver(frameMetricsObserver.getRendererObserver());
      } else {
        Log.w("View", "View not hardware-accelerated. Unable to observe frame stats");
      } 
    } else {
      if (this.mFrameMetricsObservers == null)
        this.mFrameMetricsObservers = new ArrayList<>(); 
      frameMetricsObserver = new FrameMetricsObserver((Window)frameMetricsObserver, paramHandler, paramOnFrameMetricsAvailableListener);
      this.mFrameMetricsObservers.add(frameMetricsObserver);
    } 
  }
  
  public void removeFrameMetricsListener(Window.OnFrameMetricsAvailableListener paramOnFrameMetricsAvailableListener) {
    ThreadedRenderer threadedRenderer = getThreadedRenderer();
    FrameMetricsObserver frameMetricsObserver = findFrameMetricsObserver(paramOnFrameMetricsAvailableListener);
    if (frameMetricsObserver != null) {
      ArrayList<FrameMetricsObserver> arrayList = this.mFrameMetricsObservers;
      if (arrayList != null) {
        arrayList.remove(frameMetricsObserver);
        if (threadedRenderer != null)
          threadedRenderer.removeObserver(frameMetricsObserver.getRendererObserver()); 
      } 
      return;
    } 
    throw new IllegalArgumentException("attempt to remove OnFrameMetricsAvailableListener that was never added");
  }
  
  private void registerPendingFrameMetricsObservers() {
    if (this.mFrameMetricsObservers != null) {
      ThreadedRenderer threadedRenderer = getThreadedRenderer();
      if (threadedRenderer != null) {
        for (FrameMetricsObserver frameMetricsObserver : this.mFrameMetricsObservers)
          threadedRenderer.addObserver(frameMetricsObserver.getRendererObserver()); 
      } else {
        Log.w("View", "View not hardware-accelerated. Unable to observe frame stats");
      } 
    } 
  }
  
  private FrameMetricsObserver findFrameMetricsObserver(Window.OnFrameMetricsAvailableListener paramOnFrameMetricsAvailableListener) {
    if (this.mFrameMetricsObservers != null)
      for (byte b = 0; b < this.mFrameMetricsObservers.size(); b++) {
        FrameMetricsObserver frameMetricsObserver = this.mFrameMetricsObservers.get(b);
        if (frameMetricsObserver.mListener == paramOnFrameMetricsAvailableListener)
          return frameMetricsObserver; 
      }  
    return null;
  }
  
  public void setNotifyAutofillManagerOnClick(boolean paramBoolean) {
    if (paramBoolean) {
      this.mPrivateFlags |= 0x20000000;
    } else {
      this.mPrivateFlags &= 0xDFFFFFFF;
    } 
  }
  
  private void notifyAutofillManagerOnClick() {
    if ((this.mPrivateFlags & 0x20000000) != 0)
      try {
        getAutofillManager().notifyViewClicked(this);
      } finally {
        this.mPrivateFlags = 0xDFFFFFFF & this.mPrivateFlags;
      }  
  }
  
  private boolean performClickInternal() {
    notifyAutofillManagerOnClick();
    return performClick();
  }
  
  public boolean performClick() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial notifyAutofillManagerOnClick : ()V
    //   4: aload_0
    //   5: getfield mListenerInfo : Landroid/view/View$ListenerInfo;
    //   8: astore_1
    //   9: aload_1
    //   10: ifnull -> 299
    //   13: aload_1
    //   14: getfield mOnClickListener : Landroid/view/View$OnClickListener;
    //   17: ifnull -> 299
    //   20: aload_0
    //   21: invokevirtual getContext : ()Landroid/content/Context;
    //   24: invokevirtual getPackageName : ()Ljava/lang/String;
    //   27: astore_2
    //   28: aload_2
    //   29: ifnull -> 279
    //   32: aload_2
    //   33: invokevirtual isEmpty : ()Z
    //   36: ifne -> 279
    //   39: aload_2
    //   40: ldc_w 'com.qiyi.video'
    //   43: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   46: ifne -> 69
    //   49: aload_2
    //   50: ldc_w 'tv.danmaku.bili'
    //   53: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   56: ifne -> 69
    //   59: aload_2
    //   60: ldc_w 'com.tencent.mm'
    //   63: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   66: ifeq -> 279
    //   69: aload_1
    //   70: getfield mOnClickListener : Landroid/view/View$OnClickListener;
    //   73: invokevirtual toString : ()Ljava/lang/String;
    //   76: astore_2
    //   77: aload_2
    //   78: ifnull -> 279
    //   81: aload_2
    //   82: invokevirtual isEmpty : ()Z
    //   85: ifne -> 279
    //   88: aload_2
    //   89: ldc 'org.qiyi.basecore.widget'
    //   91: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   94: ifne -> 200
    //   97: aload_2
    //   98: ldc 'com.bilibili.lib.homepage.widget.TabHost'
    //   100: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   103: ifeq -> 109
    //   106: goto -> 200
    //   109: aload_2
    //   110: ldc 'com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyNotHookReceiveUI'
    //   112: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   115: ifeq -> 279
    //   118: new java/lang/StringBuilder
    //   121: dup
    //   122: invokespecial <init> : ()V
    //   125: astore_2
    //   126: aload_2
    //   127: ldc_w 'Boost view '
    //   130: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: pop
    //   134: aload_2
    //   135: aload_1
    //   136: getfield mOnClickListener : Landroid/view/View$OnClickListener;
    //   139: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   142: pop
    //   143: ldc_w 'RPManager'
    //   146: aload_2
    //   147: invokevirtual toString : ()Ljava/lang/String;
    //   150: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   153: pop
    //   154: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   157: ifnonnull -> 174
    //   160: ldc android/view/View
    //   162: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   165: astore_2
    //   166: aload_2
    //   167: putstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   170: aload_2
    //   171: ifnull -> 279
    //   174: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   177: new com/oplus/orms/info/OrmsSaParam
    //   180: dup
    //   181: ldc_w ''
    //   184: ldc_w 'ORMS_ACTION_LUCKY_MONEY'
    //   187: sipush #1000
    //   190: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   193: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   196: pop2
    //   197: goto -> 279
    //   200: new java/lang/StringBuilder
    //   203: dup
    //   204: invokespecial <init> : ()V
    //   207: astore_2
    //   208: aload_2
    //   209: ldc_w 'Boost view '
    //   212: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   215: pop
    //   216: aload_2
    //   217: aload_1
    //   218: getfield mOnClickListener : Landroid/view/View$OnClickListener;
    //   221: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   224: pop
    //   225: ldc_w 'VIEW'
    //   228: aload_2
    //   229: invokevirtual toString : ()Ljava/lang/String;
    //   232: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   235: pop
    //   236: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   239: ifnonnull -> 256
    //   242: ldc android/view/View
    //   244: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   247: astore_2
    //   248: aload_2
    //   249: putstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   252: aload_2
    //   253: ifnull -> 279
    //   256: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   259: new com/oplus/orms/info/OrmsSaParam
    //   262: dup
    //   263: ldc_w ''
    //   266: ldc_w 'ORMS_ACTION_ACTIVITY_START'
    //   269: getstatic android/view/View.mBoostTime : I
    //   272: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   275: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   278: pop2
    //   279: aload_0
    //   280: iconst_0
    //   281: invokevirtual playSoundEffect : (I)V
    //   284: aload_1
    //   285: getfield mOnClickListener : Landroid/view/View$OnClickListener;
    //   288: aload_0
    //   289: invokeinterface onClick : (Landroid/view/View;)V
    //   294: iconst_1
    //   295: istore_3
    //   296: goto -> 301
    //   299: iconst_0
    //   300: istore_3
    //   301: aload_0
    //   302: iconst_1
    //   303: invokevirtual sendAccessibilityEvent : (I)V
    //   306: aload_0
    //   307: iconst_1
    //   308: invokevirtual notifyEnterOrExitForAutoFillIfNeeded : (Z)V
    //   311: iload_3
    //   312: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #7542	-> 0
    //   #7545	-> 4
    //   #7546	-> 9
    //   #7549	-> 20
    //   #7550	-> 28
    //   #7551	-> 39
    //   #7552	-> 59
    //   #7553	-> 69
    //   #7554	-> 77
    //   #7555	-> 88
    //   #7560	-> 109
    //   #7561	-> 118
    //   #7562	-> 154
    //   #7563	-> 174
    //   #7556	-> 200
    //   #7557	-> 236
    //   #7558	-> 256
    //   #7569	-> 279
    //   #7570	-> 284
    //   #7571	-> 294
    //   #7572	-> 296
    //   #7573	-> 299
    //   #7576	-> 301
    //   #7578	-> 306
    //   #7580	-> 311
  }
  
  public boolean callOnClick() {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnClickListener != null) {
      listenerInfo.mOnClickListener.onClick(this);
      return true;
    } 
    return false;
  }
  
  public boolean performLongClick() {
    return performLongClickInternal(this.mLongClickX, this.mLongClickY);
  }
  
  public boolean performLongClick(float paramFloat1, float paramFloat2) {
    this.mLongClickX = paramFloat1;
    this.mLongClickY = paramFloat2;
    boolean bool = performLongClick();
    this.mLongClickX = Float.NaN;
    this.mLongClickY = Float.NaN;
    return bool;
  }
  
  private boolean performLongClickInternal(float paramFloat1, float paramFloat2) {
    sendAccessibilityEvent(2);
    boolean bool1 = false;
    ListenerInfo listenerInfo = this.mListenerInfo;
    boolean bool2 = bool1;
    if (listenerInfo != null) {
      bool2 = bool1;
      if (listenerInfo.mOnLongClickListener != null)
        bool2 = listenerInfo.mOnLongClickListener.onLongClick(this); 
    } 
    bool1 = bool2;
    if (!bool2) {
      boolean bool;
      if (!Float.isNaN(paramFloat1) && !Float.isNaN(paramFloat2)) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        bool2 = showContextMenu(paramFloat1, paramFloat2);
      } else {
        bool2 = showContextMenu();
      } 
      bool1 = bool2;
    } 
    bool2 = bool1;
    if ((this.mViewFlags & 0x40000000) == 1073741824) {
      bool2 = bool1;
      if (!bool1)
        bool2 = showLongClickTooltip((int)paramFloat1, (int)paramFloat2); 
    } 
    if (bool2)
      performHapticFeedback(0); 
    return bool2;
  }
  
  public boolean performContextClick(float paramFloat1, float paramFloat2) {
    return performContextClick();
  }
  
  public boolean performContextClick() {
    sendAccessibilityEvent(8388608);
    boolean bool1 = false;
    ListenerInfo listenerInfo = this.mListenerInfo;
    boolean bool2 = bool1;
    if (listenerInfo != null) {
      bool2 = bool1;
      if (listenerInfo.mOnContextClickListener != null)
        bool2 = listenerInfo.mOnContextClickListener.onContextClick(this); 
    } 
    if (bool2)
      performHapticFeedback(6); 
    return bool2;
  }
  
  protected boolean performButtonActionOnTouchDown(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.isFromSource(8194) && (paramMotionEvent.getButtonState() & 0x2) != 0) {
      showContextMenu(paramMotionEvent.getX(), paramMotionEvent.getY());
      this.mPrivateFlags |= 0x4000000;
      return true;
    } 
    return false;
  }
  
  public boolean showContextMenu() {
    return getParent().showContextMenuForChild(this);
  }
  
  public boolean showContextMenu(float paramFloat1, float paramFloat2) {
    return getParent().showContextMenuForChild(this, paramFloat1, paramFloat2);
  }
  
  public ActionMode startActionMode(ActionMode.Callback paramCallback) {
    return startActionMode(paramCallback, 0);
  }
  
  public ActionMode startActionMode(ActionMode.Callback paramCallback, int paramInt) {
    ViewParent viewParent = getParent();
    if (viewParent == null)
      return null; 
    try {
      return viewParent.startActionModeForChild(this, paramCallback, paramInt);
    } catch (AbstractMethodError abstractMethodError) {
      return viewParent.startActionModeForChild(this, paramCallback);
    } 
  }
  
  public void startActivityForResult(Intent paramIntent, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("@android:view:");
    stringBuilder.append(System.identityHashCode(this));
    this.mStartActivityRequestWho = stringBuilder.toString();
    getContext().startActivityForResult(this.mStartActivityRequestWho, paramIntent, paramInt, null);
  }
  
  public boolean dispatchActivityResult(String paramString, int paramInt1, int paramInt2, Intent paramIntent) {
    String str = this.mStartActivityRequestWho;
    if (str != null && str.equals(paramString)) {
      onActivityResult(paramInt1, paramInt2, paramIntent);
      this.mStartActivityRequestWho = null;
      return true;
    } 
    return false;
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {}
  
  public void setOnKeyListener(OnKeyListener paramOnKeyListener) {
    ListenerInfo.access$402(getListenerInfo(), paramOnKeyListener);
  }
  
  public void setOnTouchListener(OnTouchListener paramOnTouchListener) {
    ListenerInfo.access$502(getListenerInfo(), paramOnTouchListener);
  }
  
  public void setOnGenericMotionListener(OnGenericMotionListener paramOnGenericMotionListener) {
    ListenerInfo.access$602(getListenerInfo(), paramOnGenericMotionListener);
  }
  
  public void setOnHoverListener(OnHoverListener paramOnHoverListener) {
    ListenerInfo.access$702(getListenerInfo(), paramOnHoverListener);
  }
  
  public void setOnDragListener(OnDragListener paramOnDragListener) {
    ListenerInfo.access$802(getListenerInfo(), paramOnDragListener);
  }
  
  void handleFocusGainInternal(int paramInt, Rect paramRect) {
    int i = this.mPrivateFlags;
    if ((i & 0x2) == 0) {
      View view;
      this.mPrivateFlags = i | 0x2;
      if (this.mAttachInfo != null) {
        view = getRootView().findFocus();
      } else {
        view = null;
      } 
      ViewParent viewParent = this.mParent;
      if (viewParent != null) {
        viewParent.requestChildFocus(this, this);
        updateFocusedInCluster(view, paramInt);
      } 
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null)
        attachInfo.mTreeObserver.dispatchOnGlobalFocusChange(view, this); 
      onFocusChanged(true, paramInt, paramRect);
      refreshDrawableState();
    } 
  }
  
  public final void setRevealOnFocusHint(boolean paramBoolean) {
    if (paramBoolean) {
      this.mPrivateFlags3 &= 0xFBFFFFFF;
    } else {
      this.mPrivateFlags3 |= 0x4000000;
    } 
  }
  
  public final boolean getRevealOnFocusHint() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x4000000) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void getHotspotBounds(Rect paramRect) {
    Drawable drawable = getBackground();
    if (drawable != null) {
      drawable.getHotspotBounds(paramRect);
    } else {
      getBoundsOnScreen(paramRect);
    } 
  }
  
  public boolean requestRectangleOnScreen(Rect paramRect) {
    return requestRectangleOnScreen(paramRect, false);
  }
  
  public boolean requestRectangleOnScreen(Rect paramRect, boolean paramBoolean) {
    RectF rectF;
    boolean bool2;
    if (this.mParent == null)
      return false; 
    View view = this;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      rectF = attachInfo.mTmpTransformRect;
    } else {
      rectF = new RectF();
    } 
    rectF.set(paramRect);
    ViewParent viewParent = this.mParent;
    boolean bool1 = false;
    while (true) {
      bool2 = bool1;
      if (viewParent != null) {
        paramRect.set((int)rectF.left, (int)rectF.top, (int)rectF.right, (int)rectF.bottom);
        bool1 |= viewParent.requestChildRectangleOnScreen(view, paramRect, paramBoolean);
        if (!(viewParent instanceof View)) {
          bool2 = bool1;
          break;
        } 
        rectF.offset((view.mLeft - view.getScrollX()), (view.mTop - view.getScrollY()));
        view = (View)viewParent;
        viewParent = view.getParent();
        continue;
      } 
      break;
    } 
    return bool2;
  }
  
  public void clearFocus() {
    boolean bool;
    if (sAlwaysAssignFocus || !isInTouchMode()) {
      bool = true;
    } else {
      bool = false;
    } 
    clearFocusInternal((View)null, true, bool);
  }
  
  void clearFocusInternal(View paramView, boolean paramBoolean1, boolean paramBoolean2) {
    int i = this.mPrivateFlags;
    if ((i & 0x2) != 0) {
      this.mPrivateFlags = i & 0xFFFFFFFD;
      clearParentsWantFocus();
      if (paramBoolean1) {
        ViewParent viewParent = this.mParent;
        if (viewParent != null)
          viewParent.clearChildFocus(this); 
      } 
      onFocusChanged(false, 0, (Rect)null);
      refreshDrawableState();
      if (paramBoolean1 && (!paramBoolean2 || !rootViewRequestFocus()))
        notifyGlobalFocusCleared(this); 
    } 
  }
  
  void notifyGlobalFocusCleared(View paramView) {
    if (paramView != null) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null)
        attachInfo.mTreeObserver.dispatchOnGlobalFocusChange(paramView, null); 
    } 
  }
  
  boolean rootViewRequestFocus() {
    boolean bool;
    View view = getRootView();
    if (view != null && view.requestFocus()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void unFocus(View paramView) {
    clearFocusInternal(paramView, false, false);
  }
  
  @ExportedProperty(category = "focus")
  public boolean hasFocus() {
    boolean bool;
    if ((this.mPrivateFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasFocusable() {
    return hasFocusable(sHasFocusableExcludeAutoFocusable ^ true, false);
  }
  
  public boolean hasExplicitFocusable() {
    return hasFocusable(false, true);
  }
  
  boolean hasFocusable(boolean paramBoolean1, boolean paramBoolean2) {
    if (!isFocusableInTouchMode())
      for (ViewParent viewParent = this.mParent; viewParent instanceof ViewGroup; viewParent = viewParent.getParent()) {
        ViewGroup viewGroup = (ViewGroup)viewParent;
        if (viewGroup.shouldBlockFocusForTouchscreen())
          return false; 
      }  
    int i = this.mViewFlags;
    if ((i & 0xC) != 0 || (i & 0x20) != 0)
      return false; 
    if ((paramBoolean1 || getFocusable() != 16) && isFocusable())
      return true; 
    return false;
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    if (paramBoolean) {
      sendAccessibilityEvent(8);
    } else {
      notifyViewAccessibilityStateChangedIfNeeded(0);
    } 
    switchDefaultFocusHighlight();
    if (!paramBoolean) {
      if (isPressed())
        setPressed(false); 
      if (hasWindowFocus())
        notifyFocusChangeToImeFocusController(false); 
      onFocusLost();
    } else if (hasWindowFocus()) {
      notifyFocusChangeToImeFocusController(true);
    } 
    invalidate(true);
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnFocusChangeListener != null)
      listenerInfo.mOnFocusChangeListener.onFocusChange(this, paramBoolean); 
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      attachInfo.mKeyDispatchState.reset(this); 
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      viewParent.onDescendantUnbufferedRequested(); 
    notifyEnterOrExitForAutoFillIfNeeded(paramBoolean);
  }
  
  private void notifyFocusChangeToImeFocusController(boolean paramBoolean) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return; 
    attachInfo.mViewRootImpl.getImeFocusController().onViewFocusChanged(this, paramBoolean);
  }
  
  public void notifyEnterOrExitForAutoFillIfNeeded(boolean paramBoolean) {
    if (canNotifyAutofillEnterExitEvent()) {
      AutofillManager autofillManager = getAutofillManager();
      if (autofillManager != null)
        if (paramBoolean && isFocused()) {
          if (!isLaidOut()) {
            this.mPrivateFlags3 |= 0x8000000;
          } else if (isVisibleToUser()) {
            autofillManager.notifyViewEntered(this);
          } 
        } else if (!paramBoolean && !isFocused()) {
          autofillManager.notifyViewExited(this);
        }  
    } 
  }
  
  public void setAccessibilityPaneTitle(CharSequence paramCharSequence) {
    if (!TextUtils.equals(paramCharSequence, this.mAccessibilityPaneTitle)) {
      this.mAccessibilityPaneTitle = paramCharSequence;
      notifyViewAccessibilityStateChangedIfNeeded(8);
    } 
  }
  
  public CharSequence getAccessibilityPaneTitle() {
    return this.mAccessibilityPaneTitle;
  }
  
  private boolean isAccessibilityPane() {
    boolean bool;
    if (this.mAccessibilityPaneTitle != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void sendAccessibilityEvent(int paramInt) {
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null) {
      accessibilityDelegate.sendAccessibilityEvent(this, paramInt);
    } else {
      sendAccessibilityEventInternal(paramInt);
    } 
  }
  
  public void announceForAccessibility(CharSequence paramCharSequence) {
    if (AccessibilityManager.getInstance(this.mContext).isEnabled() && this.mParent != null) {
      AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(16384);
      onInitializeAccessibilityEvent(accessibilityEvent);
      accessibilityEvent.getText().add(paramCharSequence);
      accessibilityEvent.setContentDescription(null);
      this.mParent.requestSendAccessibilityEvent(this, accessibilityEvent);
    } 
  }
  
  public void sendAccessibilityEventInternal(int paramInt) {
    if (AccessibilityManager.getInstance(this.mContext).isEnabled())
      sendAccessibilityEventUnchecked(AccessibilityEvent.obtain(paramInt)); 
  }
  
  public void sendAccessibilityEventUnchecked(AccessibilityEvent paramAccessibilityEvent) {
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null) {
      accessibilityDelegate.sendAccessibilityEventUnchecked(this, paramAccessibilityEvent);
    } else {
      sendAccessibilityEventUncheckedInternal(paramAccessibilityEvent);
    } 
  }
  
  public void sendAccessibilityEventUncheckedInternal(AccessibilityEvent paramAccessibilityEvent) {
    int i = paramAccessibilityEvent.getEventType();
    boolean bool = true;
    if (i == 32) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i != 0 && (0x20 & paramAccessibilityEvent.getContentChangeTypes()) != 0) {
      i = bool;
    } else {
      i = 0;
    } 
    if (!isShown() && i == 0)
      return; 
    onInitializeAccessibilityEvent(paramAccessibilityEvent);
    if ((paramAccessibilityEvent.getEventType() & 0x2A1BF) != 0)
      dispatchPopulateAccessibilityEvent(paramAccessibilityEvent); 
    SendAccessibilityEventThrottle sendAccessibilityEventThrottle = getThrottleForAccessibilityEvent(paramAccessibilityEvent);
    if (sendAccessibilityEventThrottle != null) {
      sendAccessibilityEventThrottle.post(paramAccessibilityEvent);
    } else {
      requestParentSendAccessibilityEvent(paramAccessibilityEvent);
    } 
  }
  
  private void requestParentSendAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    ViewParent viewParent = getParent();
    if (viewParent != null)
      getParent().requestSendAccessibilityEvent(this, paramAccessibilityEvent); 
  }
  
  private SendAccessibilityEventThrottle getThrottleForAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    boolean bool;
    if (paramAccessibilityEvent.getEventType() == 4096) {
      if (this.mSendViewScrolledAccessibilityEvent == null)
        this.mSendViewScrolledAccessibilityEvent = new SendViewScrolledAccessibilityEvent(); 
      return this.mSendViewScrolledAccessibilityEvent;
    } 
    if ((paramAccessibilityEvent.getContentChangeTypes() & 0x40) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramAccessibilityEvent.getEventType() == 2048 && bool) {
      if (this.mSendStateChangedAccessibilityEvent == null)
        this.mSendStateChangedAccessibilityEvent = new SendAccessibilityEventThrottle(); 
      return this.mSendStateChangedAccessibilityEvent;
    } 
    return null;
  }
  
  private void clearAccessibilityThrottles() {
    cancel(this.mSendViewScrolledAccessibilityEvent);
    cancel(this.mSendStateChangedAccessibilityEvent);
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null)
      return accessibilityDelegate.dispatchPopulateAccessibilityEvent(this, paramAccessibilityEvent); 
    return dispatchPopulateAccessibilityEventInternal(paramAccessibilityEvent);
  }
  
  public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    onPopulateAccessibilityEvent(paramAccessibilityEvent);
    return false;
  }
  
  public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null) {
      accessibilityDelegate.onPopulateAccessibilityEvent(this, paramAccessibilityEvent);
    } else {
      onPopulateAccessibilityEventInternal(paramAccessibilityEvent);
    } 
  }
  
  public void onPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    if (paramAccessibilityEvent.getEventType() == 32 && isAccessibilityPane())
      paramAccessibilityEvent.getText().add(getAccessibilityPaneTitle()); 
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null) {
      accessibilityDelegate.onInitializeAccessibilityEvent(this, paramAccessibilityEvent);
    } else {
      onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    } 
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    paramAccessibilityEvent.setSource(this);
    paramAccessibilityEvent.setClassName(getAccessibilityClassName());
    paramAccessibilityEvent.setPackageName(getContext().getPackageName());
    paramAccessibilityEvent.setEnabled(isEnabled());
    paramAccessibilityEvent.setContentDescription(this.mContentDescription);
    paramAccessibilityEvent.setScrollX(getScrollX());
    paramAccessibilityEvent.setScrollY(getScrollY());
    int i = paramAccessibilityEvent.getEventType();
    if (i != 8) {
      if (i == 8192) {
        CharSequence charSequence = getIterableTextForAccessibility();
        if (charSequence != null && charSequence.length() > 0) {
          paramAccessibilityEvent.setFromIndex(getAccessibilitySelectionStart());
          paramAccessibilityEvent.setToIndex(getAccessibilitySelectionEnd());
          paramAccessibilityEvent.setItemCount(charSequence.length());
        } 
      } 
    } else {
      ArrayList<View> arrayList;
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null) {
        arrayList = attachInfo.mTempArrayList;
      } else {
        arrayList = new ArrayList();
      } 
      getRootView().addFocusables(arrayList, 2, 0);
      paramAccessibilityEvent.setItemCount(arrayList.size());
      paramAccessibilityEvent.setCurrentItemIndex(arrayList.indexOf(this));
      if (this.mAttachInfo != null)
        arrayList.clear(); 
    } 
  }
  
  public AccessibilityNodeInfo createAccessibilityNodeInfo() {
    AccessibilityNodeInfo accessibilityNodeInfo;
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null) {
      accessibilityNodeInfo = accessibilityDelegate.createAccessibilityNodeInfo(this);
    } else {
      accessibilityNodeInfo = createAccessibilityNodeInfoInternal();
    } 
    if (accessibilityNodeInfo != null && AccessibilityManager.getInstance(this.mContext).isEnabled()) {
      Context context = this.mContext;
      if (AccessibilityManager.getInstance(context).isColorDirectEnabled()) {
        accessibilityNodeInfo.setRealClassName(getClass().getName());
        if (TextUtils.isEmpty(accessibilityNodeInfo.getText()) && TextUtils.isEmpty(accessibilityNodeInfo.getContentDescription()))
          try {
            if (!this.mHasInit) {
              this.mFieldText = OplusReflectDataUtils.getInstance().getTextField(this.mContext, getClass());
              this.mHasInit = true;
            } 
            if (this.mFieldText != null)
              accessibilityNodeInfo.setContentDescription((CharSequence)this.mFieldText.get(this)); 
          } catch (Exception exception) {
            exception.printStackTrace();
          }  
      } 
    } 
    return accessibilityNodeInfo;
  }
  
  public AccessibilityNodeInfo createAccessibilityNodeInfoInternal() {
    AccessibilityNodeProvider accessibilityNodeProvider = getAccessibilityNodeProvider();
    if (accessibilityNodeProvider != null)
      return accessibilityNodeProvider.createAccessibilityNodeInfo(-1); 
    AccessibilityNodeInfo accessibilityNodeInfo = AccessibilityNodeInfo.obtain(this);
    onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
    return accessibilityNodeInfo;
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null) {
      accessibilityDelegate.onInitializeAccessibilityNodeInfo(this, paramAccessibilityNodeInfo);
    } else {
      onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    } 
  }
  
  public void getBoundsOnScreen(Rect paramRect) {
    getBoundsOnScreen(paramRect, false);
  }
  
  public void getBoundsOnScreen(Rect paramRect, boolean paramBoolean) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return; 
    RectF rectF = attachInfo.mTmpTransformRect;
    rectF.set(0.0F, 0.0F, (this.mRight - this.mLeft), (this.mBottom - this.mTop));
    mapRectFromViewToScreenCoords(rectF, paramBoolean);
    int i = Math.round(rectF.left), j = Math.round(rectF.top);
    float f = rectF.right;
    int k = Math.round(f), m = Math.round(rectF.bottom);
    paramRect.set(i, j, k, m);
  }
  
  public void mapRectFromViewToScreenCoords(RectF paramRectF, boolean paramBoolean) {
    if (!hasIdentityMatrix())
      getMatrix().mapRect(paramRectF); 
    paramRectF.offset(this.mLeft, this.mTop);
    ViewParent viewParent = this.mParent;
    while (viewParent instanceof View) {
      View view = (View)viewParent;
      paramRectF.offset(-view.mScrollX, -view.mScrollY);
      if (paramBoolean) {
        paramRectF.left = Math.max(paramRectF.left, 0.0F);
        paramRectF.top = Math.max(paramRectF.top, 0.0F);
        paramRectF.right = Math.min(paramRectF.right, view.getWidth());
        paramRectF.bottom = Math.min(paramRectF.bottom, view.getHeight());
      } 
      if (!view.hasIdentityMatrix())
        view.getMatrix().mapRect(paramRectF); 
      paramRectF.offset(view.mLeft, view.mTop);
      viewParent = view.mParent;
    } 
    if (viewParent instanceof ViewRootImpl) {
      viewParent = viewParent;
      paramRectF.offset(0.0F, -((ViewRootImpl)viewParent).mCurScrollY);
    } 
    paramRectF.offset(this.mAttachInfo.mWindowLeft, this.mAttachInfo.mWindowTop);
  }
  
  public CharSequence getAccessibilityClassName() {
    return View.class.getName();
  }
  
  public void onProvideStructure(ViewStructure paramViewStructure) {
    onProvideStructure(paramViewStructure, 0, 0);
  }
  
  public void onProvideAutofillStructure(ViewStructure paramViewStructure, int paramInt) {
    onProvideStructure(paramViewStructure, 1, paramInt);
  }
  
  public void onProvideContentCaptureStructure(ViewStructure paramViewStructure, int paramInt) {
    onProvideStructure(paramViewStructure, 2, paramInt);
  }
  
  protected void onProvideStructure(ViewStructure paramViewStructure, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mID : I
    //   4: istore #4
    //   6: iload #4
    //   8: iconst_m1
    //   9: if_icmpeq -> 82
    //   12: iload #4
    //   14: invokestatic isViewIdGenerated : (I)Z
    //   17: ifne -> 82
    //   20: aload_0
    //   21: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   24: astore #5
    //   26: aload #5
    //   28: iload #4
    //   30: invokevirtual getResourceEntryName : (I)Ljava/lang/String;
    //   33: astore #6
    //   35: aload #5
    //   37: iload #4
    //   39: invokevirtual getResourceTypeName : (I)Ljava/lang/String;
    //   42: astore #7
    //   44: aload #5
    //   46: iload #4
    //   48: invokevirtual getResourcePackageName : (I)Ljava/lang/String;
    //   51: astore #5
    //   53: goto -> 67
    //   56: astore #5
    //   58: aconst_null
    //   59: astore #6
    //   61: aconst_null
    //   62: astore #7
    //   64: aconst_null
    //   65: astore #5
    //   67: aload_1
    //   68: iload #4
    //   70: aload #5
    //   72: aload #7
    //   74: aload #6
    //   76: invokevirtual setId : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   79: goto -> 91
    //   82: aload_1
    //   83: iload #4
    //   85: aconst_null
    //   86: aconst_null
    //   87: aconst_null
    //   88: invokevirtual setId : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   91: iload_2
    //   92: iconst_1
    //   93: if_icmpeq -> 101
    //   96: iload_2
    //   97: iconst_2
    //   98: if_icmpne -> 142
    //   101: aload_0
    //   102: invokevirtual getAutofillType : ()I
    //   105: istore #4
    //   107: iload #4
    //   109: ifeq -> 134
    //   112: aload_1
    //   113: iload #4
    //   115: invokevirtual setAutofillType : (I)V
    //   118: aload_1
    //   119: aload_0
    //   120: invokevirtual getAutofillHints : ()[Ljava/lang/String;
    //   123: invokevirtual setAutofillHints : ([Ljava/lang/String;)V
    //   126: aload_1
    //   127: aload_0
    //   128: invokevirtual getAutofillValue : ()Landroid/view/autofill/AutofillValue;
    //   131: invokevirtual setAutofillValue : (Landroid/view/autofill/AutofillValue;)V
    //   134: aload_1
    //   135: aload_0
    //   136: invokevirtual getImportantForAutofill : ()I
    //   139: invokevirtual setImportantForAutofill : (I)V
    //   142: iconst_0
    //   143: istore #8
    //   145: iconst_0
    //   146: istore #9
    //   148: iconst_0
    //   149: istore #10
    //   151: iconst_0
    //   152: istore #11
    //   154: iload #8
    //   156: istore #12
    //   158: iload #10
    //   160: istore #4
    //   162: iload_2
    //   163: iconst_1
    //   164: if_icmpne -> 296
    //   167: iload #8
    //   169: istore #12
    //   171: iload #10
    //   173: istore #4
    //   175: iload_3
    //   176: iconst_1
    //   177: iand
    //   178: ifne -> 296
    //   181: aconst_null
    //   182: astore #5
    //   184: aload_0
    //   185: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   188: astore #7
    //   190: iload #9
    //   192: istore #10
    //   194: iload #11
    //   196: istore_3
    //   197: aload #7
    //   199: instanceof android/view/View
    //   202: ifeq -> 219
    //   205: aload #7
    //   207: checkcast android/view/View
    //   210: astore #5
    //   212: iload #11
    //   214: istore_3
    //   215: iload #9
    //   217: istore #10
    //   219: iload #10
    //   221: istore #12
    //   223: iload_3
    //   224: istore #4
    //   226: aload #5
    //   228: ifnull -> 296
    //   231: iload #10
    //   233: istore #12
    //   235: iload_3
    //   236: istore #4
    //   238: aload #5
    //   240: invokevirtual isImportantForAutofill : ()Z
    //   243: ifne -> 296
    //   246: iload #10
    //   248: aload #5
    //   250: getfield mLeft : I
    //   253: iadd
    //   254: istore #10
    //   256: iload_3
    //   257: aload #5
    //   259: getfield mTop : I
    //   262: iadd
    //   263: istore_3
    //   264: aload #5
    //   266: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   269: astore #5
    //   271: aload #5
    //   273: instanceof android/view/View
    //   276: ifeq -> 289
    //   279: aload #5
    //   281: checkcast android/view/View
    //   284: astore #5
    //   286: goto -> 219
    //   289: iload #10
    //   291: istore #4
    //   293: goto -> 303
    //   296: iload #4
    //   298: istore_3
    //   299: iload #12
    //   301: istore #4
    //   303: aload_0
    //   304: getfield mLeft : I
    //   307: istore #12
    //   309: aload_0
    //   310: getfield mTop : I
    //   313: istore #10
    //   315: aload_1
    //   316: iload #4
    //   318: iload #12
    //   320: iadd
    //   321: iload_3
    //   322: iload #10
    //   324: iadd
    //   325: aload_0
    //   326: getfield mScrollX : I
    //   329: aload_0
    //   330: getfield mScrollY : I
    //   333: aload_0
    //   334: getfield mRight : I
    //   337: iload #12
    //   339: isub
    //   340: aload_0
    //   341: getfield mBottom : I
    //   344: iload #10
    //   346: isub
    //   347: invokevirtual setDimens : (IIIIII)V
    //   350: iload_2
    //   351: ifne -> 377
    //   354: aload_0
    //   355: invokevirtual hasIdentityMatrix : ()Z
    //   358: ifne -> 369
    //   361: aload_1
    //   362: aload_0
    //   363: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   366: invokevirtual setTransformation : (Landroid/graphics/Matrix;)V
    //   369: aload_1
    //   370: aload_0
    //   371: invokevirtual getZ : ()F
    //   374: invokevirtual setElevation : (F)V
    //   377: aload_1
    //   378: aload_0
    //   379: invokevirtual getVisibility : ()I
    //   382: invokevirtual setVisibility : (I)V
    //   385: aload_1
    //   386: aload_0
    //   387: invokevirtual isEnabled : ()Z
    //   390: invokevirtual setEnabled : (Z)V
    //   393: aload_0
    //   394: invokevirtual isClickable : ()Z
    //   397: ifeq -> 405
    //   400: aload_1
    //   401: iconst_1
    //   402: invokevirtual setClickable : (Z)V
    //   405: aload_0
    //   406: invokevirtual isFocusable : ()Z
    //   409: ifeq -> 417
    //   412: aload_1
    //   413: iconst_1
    //   414: invokevirtual setFocusable : (Z)V
    //   417: aload_0
    //   418: invokevirtual isFocused : ()Z
    //   421: ifeq -> 429
    //   424: aload_1
    //   425: iconst_1
    //   426: invokevirtual setFocused : (Z)V
    //   429: aload_0
    //   430: invokevirtual isAccessibilityFocused : ()Z
    //   433: ifeq -> 441
    //   436: aload_1
    //   437: iconst_1
    //   438: invokevirtual setAccessibilityFocused : (Z)V
    //   441: aload_0
    //   442: invokevirtual isSelected : ()Z
    //   445: ifeq -> 453
    //   448: aload_1
    //   449: iconst_1
    //   450: invokevirtual setSelected : (Z)V
    //   453: aload_0
    //   454: invokevirtual isActivated : ()Z
    //   457: ifeq -> 465
    //   460: aload_1
    //   461: iconst_1
    //   462: invokevirtual setActivated : (Z)V
    //   465: aload_0
    //   466: invokevirtual isLongClickable : ()Z
    //   469: ifeq -> 477
    //   472: aload_1
    //   473: iconst_1
    //   474: invokevirtual setLongClickable : (Z)V
    //   477: aload_0
    //   478: instanceof android/widget/Checkable
    //   481: ifeq -> 506
    //   484: aload_1
    //   485: iconst_1
    //   486: invokevirtual setCheckable : (Z)V
    //   489: aload_0
    //   490: checkcast android/widget/Checkable
    //   493: invokeinterface isChecked : ()Z
    //   498: ifeq -> 506
    //   501: aload_1
    //   502: iconst_1
    //   503: invokevirtual setChecked : (Z)V
    //   506: aload_0
    //   507: invokevirtual isOpaque : ()Z
    //   510: ifeq -> 518
    //   513: aload_1
    //   514: iconst_1
    //   515: invokevirtual setOpaque : (Z)V
    //   518: aload_0
    //   519: invokevirtual isContextClickable : ()Z
    //   522: ifeq -> 530
    //   525: aload_1
    //   526: iconst_1
    //   527: invokevirtual setContextClickable : (Z)V
    //   530: aload_1
    //   531: aload_0
    //   532: invokevirtual getAccessibilityClassName : ()Ljava/lang/CharSequence;
    //   535: invokeinterface toString : ()Ljava/lang/String;
    //   540: invokevirtual setClassName : (Ljava/lang/String;)V
    //   543: aload_1
    //   544: aload_0
    //   545: invokevirtual getContentDescription : ()Ljava/lang/CharSequence;
    //   548: invokevirtual setContentDescription : (Ljava/lang/CharSequence;)V
    //   551: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #9003	-> 0
    //   #9004	-> 6
    //   #9007	-> 20
    //   #9008	-> 26
    //   #9009	-> 35
    //   #9010	-> 44
    //   #9013	-> 53
    //   #9011	-> 56
    //   #9012	-> 58
    //   #9014	-> 67
    //   #9015	-> 79
    //   #9016	-> 82
    //   #9019	-> 91
    //   #9021	-> 101
    //   #9024	-> 107
    //   #9025	-> 112
    //   #9026	-> 118
    //   #9027	-> 126
    //   #9029	-> 134
    //   #9032	-> 142
    //   #9033	-> 148
    //   #9034	-> 154
    //   #9036	-> 181
    //   #9038	-> 184
    //   #9039	-> 190
    //   #9040	-> 205
    //   #9043	-> 219
    //   #9044	-> 246
    //   #9045	-> 256
    //   #9047	-> 264
    //   #9048	-> 271
    //   #9049	-> 279
    //   #9048	-> 289
    //   #9056	-> 296
    //   #9058	-> 350
    //   #9059	-> 354
    //   #9060	-> 361
    //   #9062	-> 369
    //   #9064	-> 377
    //   #9065	-> 385
    //   #9066	-> 393
    //   #9067	-> 400
    //   #9069	-> 405
    //   #9070	-> 412
    //   #9072	-> 417
    //   #9073	-> 424
    //   #9075	-> 429
    //   #9076	-> 436
    //   #9078	-> 441
    //   #9079	-> 448
    //   #9081	-> 453
    //   #9082	-> 460
    //   #9084	-> 465
    //   #9085	-> 472
    //   #9087	-> 477
    //   #9088	-> 484
    //   #9089	-> 489
    //   #9090	-> 501
    //   #9093	-> 506
    //   #9094	-> 513
    //   #9096	-> 518
    //   #9097	-> 525
    //   #9099	-> 530
    //   #9100	-> 543
    //   #9101	-> 551
    // Exception table:
    //   from	to	target	type
    //   20	26	56	android/content/res/Resources$NotFoundException
    //   26	35	56	android/content/res/Resources$NotFoundException
    //   35	44	56	android/content/res/Resources$NotFoundException
    //   44	53	56	android/content/res/Resources$NotFoundException
  }
  
  public void onProvideVirtualStructure(ViewStructure paramViewStructure) {
    onProvideVirtualStructureCompat(paramViewStructure, false);
  }
  
  private void onProvideVirtualStructureCompat(ViewStructure paramViewStructure, boolean paramBoolean) {
    AccessibilityNodeProvider accessibilityNodeProvider = getAccessibilityNodeProvider();
    if (accessibilityNodeProvider != null) {
      if (paramBoolean && Log.isLoggable("View.Autofill", 2)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onProvideVirtualStructureCompat() for ");
        stringBuilder.append(this);
        Log.v("View.Autofill", stringBuilder.toString());
      } 
      AccessibilityNodeInfo accessibilityNodeInfo = createAccessibilityNodeInfo();
      paramViewStructure.setChildCount(1);
      paramViewStructure = paramViewStructure.newChild(0);
      populateVirtualStructure(paramViewStructure, accessibilityNodeProvider, accessibilityNodeInfo, paramBoolean);
      accessibilityNodeInfo.recycle();
    } 
  }
  
  public void onProvideAutofillVirtualStructure(ViewStructure paramViewStructure, int paramInt) {
    if (this.mContext.isAutofillCompatibilityEnabled())
      onProvideVirtualStructureCompat(paramViewStructure, true); 
  }
  
  public void autofill(AutofillValue paramAutofillValue) {}
  
  public void autofill(SparseArray<AutofillValue> paramSparseArray) {
    if (!this.mContext.isAutofillCompatibilityEnabled())
      return; 
    AccessibilityNodeProvider accessibilityNodeProvider = getAccessibilityNodeProvider();
    if (accessibilityNodeProvider == null)
      return; 
    int i = paramSparseArray.size();
    for (byte b = 0; b < i; b++) {
      AutofillValue autofillValue = paramSparseArray.valueAt(b);
      if (autofillValue.isText()) {
        int j = paramSparseArray.keyAt(b);
        CharSequence charSequence = autofillValue.getTextValue();
        Bundle bundle = new Bundle();
        bundle.putCharSequence("ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE", charSequence);
        accessibilityNodeProvider.performAction(j, 2097152, bundle);
      } 
    } 
  }
  
  public final AutofillId getAutofillId() {
    if (this.mAutofillId == null)
      this.mAutofillId = new AutofillId(getAutofillViewId()); 
    return this.mAutofillId;
  }
  
  public void setAutofillId(AutofillId paramAutofillId) {
    if (Log.isLoggable("View.Autofill", 2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAutofill(): from ");
      stringBuilder.append(this.mAutofillId);
      stringBuilder.append(" to ");
      stringBuilder.append(paramAutofillId);
      Log.v("View.Autofill", stringBuilder.toString());
    } 
    if (!isAttachedToWindow()) {
      if (paramAutofillId == null || paramAutofillId.isNonVirtual()) {
        if (paramAutofillId == null && (this.mPrivateFlags3 & 0x40000000) == 0)
          return; 
        this.mAutofillId = paramAutofillId;
        if (paramAutofillId != null) {
          this.mAutofillViewId = paramAutofillId.getViewId();
          this.mPrivateFlags3 = 0x40000000 | this.mPrivateFlags3;
        } else {
          this.mAutofillViewId = -1;
          this.mPrivateFlags3 &= 0xBFFFFFFF;
        } 
        return;
      } 
      throw new IllegalStateException("Cannot set autofill id assigned to virtual views");
    } 
    throw new IllegalStateException("Cannot set autofill id when view is attached");
  }
  
  public int getAutofillType() {
    return 0;
  }
  
  @ExportedProperty
  public String[] getAutofillHints() {
    return this.mAutofillHints;
  }
  
  public boolean isAutofilled() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x10000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hideAutofillHighlight() {
    boolean bool;
    if ((this.mPrivateFlags4 & 0x200) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public AutofillValue getAutofillValue() {
    return null;
  }
  
  @ExportedProperty(mapping = {@IntToString(from = 0, to = "auto"), @IntToString(from = 1, to = "yes"), @IntToString(from = 2, to = "no"), @IntToString(from = 4, to = "yesExcludeDescendants"), @IntToString(from = 8, to = "noExcludeDescendants")})
  public int getImportantForAutofill() {
    return (this.mPrivateFlags3 & 0x780000) >> 19;
  }
  
  public void setImportantForAutofill(int paramInt) {
    int i = this.mPrivateFlags3 & 0xFF87FFFF;
    this.mPrivateFlags3 = i | paramInt << 19 & 0x780000;
  }
  
  public final boolean isImportantForAutofill() {
    ViewParent viewParent = this.mParent;
    while (viewParent instanceof View) {
      int j = ((View)viewParent).getImportantForAutofill();
      if (j == 8 || j == 4) {
        if (Log.isLoggable("View.Autofill", 2)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("View (");
          stringBuilder.append(this);
          stringBuilder.append(") is not important for autofill because parent ");
          stringBuilder.append(viewParent);
          stringBuilder.append("'s importance is ");
          stringBuilder.append(j);
          Log.v("View.Autofill", stringBuilder.toString());
        } 
        return false;
      } 
      viewParent = viewParent.getParent();
    } 
    int i = getImportantForAutofill();
    if (i == 4 || i == 1)
      return true; 
    if (i == 8 || i == 2) {
      if (Log.isLoggable("View.Autofill", 2)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("View (");
        stringBuilder.append(this);
        stringBuilder.append(") is not important for autofill because its importance is ");
        stringBuilder.append(i);
        Log.v("View.Autofill", stringBuilder.toString());
      } 
      return false;
    } 
    if (i != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid autofill importance (");
      stringBuilder.append(i);
      stringBuilder.append(" on view ");
      stringBuilder.append(this);
      Log.w("View.Autofill", stringBuilder.toString());
      return false;
    } 
    i = this.mID;
    if (i != -1 && !isViewIdGenerated(i)) {
      String str1, str2;
      Resources resources = getResources();
      viewParent = null;
      String str3 = null;
      try {
        str2 = resources.getResourceEntryName(i);
        str1 = str2;
        String str = resources.getResourcePackageName(i);
        str3 = str1;
      } catch (android.content.res.Resources.NotFoundException notFoundException) {
        str2 = str1;
      } 
      if (str2 != null && str3 != null && str3.equals(this.mContext.getPackageName()))
        return true; 
    } 
    if (getAutofillHints() != null)
      return true; 
    return false;
  }
  
  @ExportedProperty(mapping = {@IntToString(from = 0, to = "auto"), @IntToString(from = 1, to = "yes"), @IntToString(from = 2, to = "no"), @IntToString(from = 4, to = "yesExcludeDescendants"), @IntToString(from = 8, to = "noExcludeDescendants")})
  public int getImportantForContentCapture() {
    return this.mPrivateFlags4 & 0xF;
  }
  
  public void setImportantForContentCapture(int paramInt) {
    int i = this.mPrivateFlags4 & 0xFFFFFFF0;
    this.mPrivateFlags4 = i | paramInt & 0xF;
  }
  
  public final boolean isImportantForContentCapture() {
    int i = this.mPrivateFlags4;
    if ((i & 0x40) != 0) {
      boolean bool1;
      if ((i & 0x80) != 0) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      return bool1;
    } 
    boolean bool = calculateIsImportantForContentCapture();
    this.mPrivateFlags4 = i = this.mPrivateFlags4 & 0xFFFFFF7F;
    if (bool)
      this.mPrivateFlags4 = i | 0x80; 
    this.mPrivateFlags4 |= 0x40;
    return bool;
  }
  
  private boolean calculateIsImportantForContentCapture() {
    ViewParent viewParent = this.mParent;
    while (viewParent instanceof View) {
      int j = ((View)viewParent).getImportantForContentCapture();
      if (j == 8 || j == 4) {
        if (Log.isLoggable("View.ContentCapture", 2)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("View (");
          stringBuilder.append(this);
          stringBuilder.append(") is not important for content capture because parent ");
          stringBuilder.append(viewParent);
          stringBuilder.append("'s importance is ");
          stringBuilder.append(j);
          Log.v("View.ContentCapture", stringBuilder.toString());
        } 
        return false;
      } 
      viewParent = viewParent.getParent();
    } 
    int i = getImportantForContentCapture();
    if (i == 4 || i == 1)
      return true; 
    if (i == 8 || i == 2) {
      if (Log.isLoggable("View.ContentCapture", 2)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("View (");
        stringBuilder.append(this);
        stringBuilder.append(") is not important for content capture because its importance is ");
        stringBuilder.append(i);
        Log.v("View.ContentCapture", stringBuilder.toString());
      } 
      return false;
    } 
    if (i != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid content capture importance (");
      stringBuilder.append(i);
      stringBuilder.append(" on view ");
      stringBuilder.append(this);
      Log.w("View.ContentCapture", stringBuilder.toString());
      return false;
    } 
    if (this instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)this;
      for (i = 0; i < viewGroup.getChildCount(); i++) {
        View view = viewGroup.getChildAt(i);
        if (view.isImportantForContentCapture())
          return true; 
      } 
    } 
    if (getAutofillHints() != null)
      return true; 
    return false;
  }
  
  private void notifyAppearedOrDisappearedForContentCaptureIfNeeded(boolean paramBoolean) {
    null = this.mAttachInfo;
    if (null != null && !null.mReadyForContentCaptureUpdates)
      return; 
    if (Trace.isTagEnabled(8L)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifyContentCapture(");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(") for ");
      stringBuilder.append(getClass().getSimpleName());
      String str = stringBuilder.toString();
      Trace.traceBegin(8L, str);
    } 
    try {
      notifyAppearedOrDisappearedForContentCaptureIfNeededNoTrace(paramBoolean);
      return;
    } finally {
      Trace.traceEnd(8L);
    } 
  }
  
  private void notifyAppearedOrDisappearedForContentCaptureIfNeededNoTrace(boolean paramBoolean) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (this.mContext.getContentCaptureOptions() == null)
      return; 
    if (paramBoolean) {
      if (!isLaidOut() || getVisibility() != 0 || (this.mPrivateFlags4 & 0x10) != 0)
        return; 
    } else {
      int i = this.mPrivateFlags4;
      if ((i & 0x10) == 0 || (i & 0x20) != 0)
        return; 
    } 
    ContentCaptureSession contentCaptureSession = getContentCaptureSession();
    if (contentCaptureSession == null)
      return; 
    if (!isImportantForContentCapture())
      return; 
    if (paramBoolean) {
      setNotifiedContentCaptureAppeared();
      if (attachInfo != null)
        attachInfo.delayNotifyContentCaptureEvent(contentCaptureSession, this, paramBoolean); 
    } else {
      int i = this.mPrivateFlags4 | 0x20;
      this.mPrivateFlags4 = i & 0xFFFFFFEF;
      if (attachInfo != null)
        attachInfo.delayNotifyContentCaptureEvent(contentCaptureSession, this, paramBoolean); 
    } 
  }
  
  private void setNotifiedContentCaptureAppeared() {
    int i = this.mPrivateFlags4 | 0x10;
    this.mPrivateFlags4 = i & 0xFFFFFFDF;
  }
  
  protected boolean getNotifiedContentCaptureAppeared() {
    boolean bool;
    if ((this.mPrivateFlags4 & 0x10) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setContentCaptureSession(ContentCaptureSession paramContentCaptureSession) {
    this.mContentCaptureSession = paramContentCaptureSession;
  }
  
  public final ContentCaptureSession getContentCaptureSession() {
    if (this.mContentCaptureSessionCached)
      return this.mContentCaptureSession; 
    ContentCaptureSession contentCaptureSession = getAndCacheContentCaptureSession();
    this.mContentCaptureSessionCached = true;
    return contentCaptureSession;
  }
  
  private ContentCaptureSession getAndCacheContentCaptureSession() {
    ContentCaptureSession contentCaptureSession = this.mContentCaptureSession;
    if (contentCaptureSession != null)
      return contentCaptureSession; 
    contentCaptureSession = null;
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof View)
      contentCaptureSession = ((View)viewParent).getContentCaptureSession(); 
    if (contentCaptureSession == null) {
      Context context = this.mContext;
      ContentCaptureManager contentCaptureManager = (ContentCaptureManager)context.getSystemService(ContentCaptureManager.class);
      if (contentCaptureManager == null) {
        contentCaptureManager = null;
      } else {
        contentCaptureSession = contentCaptureManager.getMainContentCaptureSession();
      } 
      return contentCaptureSession;
    } 
    return contentCaptureSession;
  }
  
  private AutofillManager getAutofillManager() {
    return (AutofillManager)this.mContext.getSystemService(AutofillManager.class);
  }
  
  private boolean isAutofillable() {
    int i = getAutofillType();
    boolean bool = false;
    if (i == 0)
      return false; 
    if (!isImportantForAutofill()) {
      AutofillOptions autofillOptions = this.mContext.getAutofillOptions();
      if (autofillOptions == null || !autofillOptions.isAugmentedAutofillEnabled(this.mContext))
        return false; 
      AutofillManager autofillManager = getAutofillManager();
      if (autofillManager == null)
        return false; 
      autofillManager.notifyViewEnteredForAugmentedAutofill(this);
    } 
    if (getAutofillViewId() > 1073741823)
      bool = true; 
    return bool;
  }
  
  public boolean canNotifyAutofillEnterExitEvent() {
    boolean bool;
    if (isAutofillable() && isAttachedToWindow()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void populateVirtualStructure(ViewStructure paramViewStructure, AccessibilityNodeProvider paramAccessibilityNodeProvider, AccessibilityNodeInfo paramAccessibilityNodeInfo, boolean paramBoolean) {
    int i = AccessibilityNodeInfo.getVirtualDescendantId(paramAccessibilityNodeInfo.getSourceNodeId());
    String str1 = paramAccessibilityNodeInfo.getViewIdResourceName();
    String str2 = null;
    paramViewStructure.setId(i, null, null, str1);
    Rect rect = paramViewStructure.getTempRect();
    paramAccessibilityNodeInfo.getBoundsInParent(rect);
    paramViewStructure.setDimens(rect.left, rect.top, 0, 0, rect.width(), rect.height());
    paramViewStructure.setVisibility(0);
    paramViewStructure.setEnabled(paramAccessibilityNodeInfo.isEnabled());
    if (paramAccessibilityNodeInfo.isClickable())
      paramViewStructure.setClickable(true); 
    if (paramAccessibilityNodeInfo.isFocusable())
      paramViewStructure.setFocusable(true); 
    if (paramAccessibilityNodeInfo.isFocused())
      paramViewStructure.setFocused(true); 
    if (paramAccessibilityNodeInfo.isAccessibilityFocused())
      paramViewStructure.setAccessibilityFocused(true); 
    if (paramAccessibilityNodeInfo.isSelected())
      paramViewStructure.setSelected(true); 
    if (paramAccessibilityNodeInfo.isLongClickable())
      paramViewStructure.setLongClickable(true); 
    if (paramAccessibilityNodeInfo.isCheckable()) {
      paramViewStructure.setCheckable(true);
      if (paramAccessibilityNodeInfo.isChecked())
        paramViewStructure.setChecked(true); 
    } 
    if (paramAccessibilityNodeInfo.isContextClickable())
      paramViewStructure.setContextClickable(true); 
    if (paramBoolean) {
      AutofillId autofillId = getAutofillId();
      autofillId = new AutofillId(autofillId, AccessibilityNodeInfo.getVirtualDescendantId(paramAccessibilityNodeInfo.getSourceNodeId()));
      paramViewStructure.setAutofillId(autofillId);
    } 
    CharSequence charSequence1 = paramAccessibilityNodeInfo.getClassName();
    if (charSequence1 != null)
      str2 = charSequence1.toString(); 
    paramViewStructure.setClassName(str2);
    paramViewStructure.setContentDescription(paramAccessibilityNodeInfo.getContentDescription());
    if (paramBoolean) {
      i = paramAccessibilityNodeInfo.getMaxTextLength();
      if (i != -1)
        paramViewStructure.setMaxTextLength(i); 
      paramViewStructure.setHint(paramAccessibilityNodeInfo.getHintText());
    } 
    CharSequence charSequence2 = paramAccessibilityNodeInfo.getText();
    if (charSequence2 != null || paramAccessibilityNodeInfo.getError() != null) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i != 0)
      paramViewStructure.setText(charSequence2, paramAccessibilityNodeInfo.getTextSelectionStart(), paramAccessibilityNodeInfo.getTextSelectionEnd()); 
    if (paramBoolean)
      if (paramAccessibilityNodeInfo.isEditable()) {
        paramViewStructure.setDataIsSensitive(true);
        if (i != 0) {
          paramViewStructure.setAutofillType(1);
          paramViewStructure.setAutofillValue(AutofillValue.forText(charSequence2));
        } 
        int k = paramAccessibilityNodeInfo.getInputType();
        i = k;
        if (k == 0) {
          i = k;
          if (paramAccessibilityNodeInfo.isPassword())
            i = 129; 
        } 
        paramViewStructure.setInputType(i);
      } else {
        paramViewStructure.setDataIsSensitive(false);
      }  
    int j = paramAccessibilityNodeInfo.getChildCount();
    if (j > 0) {
      paramViewStructure.setChildCount(j);
      for (i = 0; i < j; i++) {
        if (AccessibilityNodeInfo.getVirtualDescendantId(paramAccessibilityNodeInfo.getChildNodeIds().get(i)) == -1) {
          Log.e("View", "Virtual view pointing to its host. Ignoring");
        } else {
          int k = AccessibilityNodeInfo.getVirtualDescendantId(paramAccessibilityNodeInfo.getChildId(i));
          AccessibilityNodeInfo accessibilityNodeInfo = paramAccessibilityNodeProvider.createAccessibilityNodeInfo(k);
          ViewStructure viewStructure = paramViewStructure.newChild(i);
          populateVirtualStructure(viewStructure, paramAccessibilityNodeProvider, accessibilityNodeInfo, paramBoolean);
          accessibilityNodeInfo.recycle();
        } 
      } 
    } 
  }
  
  public void dispatchProvideStructure(ViewStructure paramViewStructure) {
    dispatchProvideStructure(paramViewStructure, 0, 0);
  }
  
  public void dispatchProvideAutofillStructure(ViewStructure paramViewStructure, int paramInt) {
    dispatchProvideStructure(paramViewStructure, 1, paramInt);
  }
  
  private void dispatchProvideStructure(ViewStructure paramViewStructure, int paramInt1, int paramInt2) {
    if (paramInt1 == 1) {
      paramViewStructure.setAutofillId(getAutofillId());
      onProvideAutofillStructure(paramViewStructure, paramInt2);
      onProvideAutofillVirtualStructure(paramViewStructure, paramInt2);
    } else if (!isAssistBlocked()) {
      onProvideStructure(paramViewStructure);
      onProvideVirtualStructure(paramViewStructure);
    } else {
      paramViewStructure.setClassName(getAccessibilityClassName().toString());
      paramViewStructure.setAssistBlocked(true);
    } 
  }
  
  public void dispatchInitialProvideContentCaptureStructure() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null) {
      null = new StringBuilder();
      null.append("dispatchProvideContentCaptureStructure(): no AttachInfo for ");
      null.append(this);
      Log.w("View.ContentCapture", null.toString());
      return;
    } 
    ContentCaptureManager contentCaptureManager = ((AttachInfo)null).mContentCaptureManager;
    if (contentCaptureManager == null) {
      null = new StringBuilder();
      null.append("dispatchProvideContentCaptureStructure(): no ContentCaptureManager for ");
      null.append(this);
      Log.w("View.ContentCapture", null.toString());
      return;
    } 
    ((AttachInfo)null).mReadyForContentCaptureUpdates = true;
    if (!isImportantForContentCapture()) {
      if (Log.isLoggable("View.ContentCapture", 3))
        Log.d("View.ContentCapture", "dispatchProvideContentCaptureStructure(): decorView is not important"); 
      return;
    } 
    ((AttachInfo)null).mContentCaptureManager = contentCaptureManager;
    ContentCaptureSession contentCaptureSession = getContentCaptureSession();
    if (contentCaptureSession == null) {
      if (Log.isLoggable("View.ContentCapture", 3)) {
        null = new StringBuilder();
        null.append("dispatchProvideContentCaptureStructure(): no session for ");
        null.append(this);
        Log.d("View.ContentCapture", null.toString());
      } 
      return;
    } 
    contentCaptureSession.internalNotifyViewTreeEvent(true);
    try {
      dispatchProvideContentCaptureStructure();
      return;
    } finally {
      contentCaptureSession.internalNotifyViewTreeEvent(false);
    } 
  }
  
  void dispatchProvideContentCaptureStructure() {
    ContentCaptureSession contentCaptureSession = getContentCaptureSession();
    if (contentCaptureSession != null) {
      ViewStructure viewStructure = contentCaptureSession.newViewStructure(this);
      onProvideContentCaptureStructure(viewStructure, 0);
      setNotifiedContentCaptureAppeared();
      contentCaptureSession.notifyViewAppeared(viewStructure);
    } 
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return; 
    Rect rect = attachInfo.mTmpInvalRect;
    getDrawingRect(rect);
    paramAccessibilityNodeInfo.setBoundsInParent(rect);
    getBoundsOnScreen(rect, true);
    paramAccessibilityNodeInfo.setBoundsInScreen(rect);
    ViewParent viewParent = getParentForAccessibility();
    if (viewParent instanceof View)
      paramAccessibilityNodeInfo.setParent((View)viewParent); 
    if (this.mID != -1) {
      View view2 = getRootView();
      View view1 = view2;
      if (view2 == null)
        view1 = this; 
      view1 = view1.findLabelForView(this, this.mID);
      if (view1 != null)
        paramAccessibilityNodeInfo.setLabeledBy(view1); 
      if ((this.mAttachInfo.mAccessibilityFetchFlags & 0x10) != 0) {
        int i = this.mID;
        if (Resources.resourceHasPackage(i))
          try {
            String str = getResources().getResourceName(this.mID);
            paramAccessibilityNodeInfo.setViewIdResourceName(str);
          } catch (android.content.res.Resources.NotFoundException notFoundException) {} 
      } 
    } 
    if (this.mLabelForId != -1) {
      View view2 = getRootView();
      View view1 = view2;
      if (view2 == null)
        view1 = this; 
      view1 = view1.findViewInsideOutShouldExist(this, this.mLabelForId);
      if (view1 != null)
        paramAccessibilityNodeInfo.setLabelFor(view1); 
    } 
    if (this.mAccessibilityTraversalBeforeId != -1) {
      View view2 = getRootView();
      View view1 = view2;
      if (view2 == null)
        view1 = this; 
      view1 = view1.findViewInsideOutShouldExist(this, this.mAccessibilityTraversalBeforeId);
      if (view1 != null && view1.includeForAccessibility())
        paramAccessibilityNodeInfo.setTraversalBefore(view1); 
    } 
    if (this.mAccessibilityTraversalAfterId != -1) {
      View view2 = getRootView();
      View view1 = view2;
      if (view2 == null)
        view1 = this; 
      view1 = view1.findViewInsideOutShouldExist(this, this.mAccessibilityTraversalAfterId);
      if (view1 != null && view1.includeForAccessibility())
        paramAccessibilityNodeInfo.setTraversalAfter(view1); 
    } 
    paramAccessibilityNodeInfo.setVisibleToUser(isVisibleToUser());
    paramAccessibilityNodeInfo.setImportantForAccessibility(isImportantForAccessibility());
    paramAccessibilityNodeInfo.setPackageName(this.mContext.getPackageName());
    paramAccessibilityNodeInfo.setClassName(getAccessibilityClassName());
    paramAccessibilityNodeInfo.setStateDescription(getStateDescription());
    paramAccessibilityNodeInfo.setContentDescription(getContentDescription());
    paramAccessibilityNodeInfo.setEnabled(isEnabled());
    paramAccessibilityNodeInfo.setClickable(isClickable());
    paramAccessibilityNodeInfo.setFocusable(isFocusable());
    paramAccessibilityNodeInfo.setScreenReaderFocusable(isScreenReaderFocusable());
    paramAccessibilityNodeInfo.setFocused(isFocused());
    paramAccessibilityNodeInfo.setAccessibilityFocused(isAccessibilityFocused());
    paramAccessibilityNodeInfo.setSelected(isSelected());
    paramAccessibilityNodeInfo.setLongClickable(isLongClickable());
    paramAccessibilityNodeInfo.setContextClickable(isContextClickable());
    paramAccessibilityNodeInfo.setLiveRegion(getAccessibilityLiveRegion());
    TooltipInfo tooltipInfo = this.mTooltipInfo;
    if (tooltipInfo != null && tooltipInfo.mTooltipText != null) {
      AccessibilityNodeInfo.AccessibilityAction accessibilityAction;
      paramAccessibilityNodeInfo.setTooltipText(this.mTooltipInfo.mTooltipText);
      if (this.mTooltipInfo.mTooltipPopup == null) {
        accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP;
      } else {
        accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP;
      } 
      paramAccessibilityNodeInfo.addAction(accessibilityAction);
    } 
    paramAccessibilityNodeInfo.addAction(4);
    paramAccessibilityNodeInfo.addAction(8);
    if (isFocusable())
      if (isFocused()) {
        paramAccessibilityNodeInfo.addAction(2);
      } else {
        paramAccessibilityNodeInfo.addAction(1);
      }  
    if (!isAccessibilityFocused()) {
      paramAccessibilityNodeInfo.addAction(64);
    } else {
      paramAccessibilityNodeInfo.addAction(128);
    } 
    if (isClickable() && isEnabled())
      paramAccessibilityNodeInfo.addAction(16); 
    if (isLongClickable() && isEnabled())
      paramAccessibilityNodeInfo.addAction(32); 
    if (isContextClickable() && isEnabled())
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK); 
    CharSequence charSequence = getIterableTextForAccessibility();
    if (charSequence != null && charSequence.length() > 0) {
      paramAccessibilityNodeInfo.setTextSelection(getAccessibilitySelectionStart(), getAccessibilitySelectionEnd());
      paramAccessibilityNodeInfo.addAction(131072);
      paramAccessibilityNodeInfo.addAction(256);
      paramAccessibilityNodeInfo.addAction(512);
      paramAccessibilityNodeInfo.setMovementGranularities(11);
    } 
    paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN);
    populateAccessibilityNodeInfoDrawingOrderInParent(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setPaneTitle(this.mAccessibilityPaneTitle);
    paramAccessibilityNodeInfo.setHeading(isAccessibilityHeading());
    TouchDelegate touchDelegate = this.mTouchDelegate;
    if (touchDelegate != null)
      paramAccessibilityNodeInfo.setTouchDelegateInfo(touchDelegate.getTouchDelegateInfo()); 
  }
  
  public void addExtraDataToAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo, String paramString, Bundle paramBundle) {}
  
  private void populateAccessibilityNodeInfoDrawingOrderInParent(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    int j;
    if ((this.mPrivateFlags & 0x10) == 0) {
      paramAccessibilityNodeInfo.setDrawingOrder(0);
      return;
    } 
    int i = 1;
    View view = this;
    ViewParent viewParent = getParentForAccessibility();
    while (true) {
      j = i;
      if (view != viewParent) {
        ViewParent viewParent1 = view.getParent();
        if (!(viewParent1 instanceof ViewGroup)) {
          j = 0;
          break;
        } 
        ViewGroup viewGroup = (ViewGroup)viewParent1;
        int k = viewGroup.getChildCount();
        int m = i;
        if (k > 1) {
          ArrayList<View> arrayList = viewGroup.buildOrderedChildList();
          if (arrayList != null) {
            int n = arrayList.indexOf(view);
            for (j = 0; j < n; j++)
              i += numViewsForAccessibility(arrayList.get(j)); 
            m = i;
          } else {
            int n;
            j = viewGroup.indexOfChild(view);
            boolean bool = viewGroup.isChildrenDrawingOrderEnabled();
            if (j >= 0 && bool)
              j = viewGroup.getChildDrawingOrder(k, j); 
            if (bool) {
              n = k;
            } else {
              n = j;
            } 
            m = i;
            if (j != 0) {
              byte b = 0;
              while (true) {
                m = i;
                if (b < n) {
                  byte b1;
                  if (bool) {
                    b1 = viewGroup.getChildDrawingOrder(k, b);
                  } else {
                    b1 = b;
                  } 
                  m = i;
                  if (b1 < j)
                    m = i + numViewsForAccessibility(viewGroup.getChildAt(b)); 
                  b++;
                  i = m;
                  continue;
                } 
                break;
              } 
            } 
          } 
        } 
        view = (View)viewParent1;
        i = m;
        continue;
      } 
      break;
    } 
    paramAccessibilityNodeInfo.setDrawingOrder(j);
  }
  
  private static int numViewsForAccessibility(View paramView) {
    if (paramView != null) {
      if (paramView.includeForAccessibility())
        return 1; 
      if (paramView instanceof ViewGroup)
        return ((ViewGroup)paramView).getNumChildrenForAccessibility(); 
    } 
    return 0;
  }
  
  private View findLabelForView(View paramView, int paramInt) {
    if (this.mMatchLabelForPredicate == null)
      this.mMatchLabelForPredicate = new MatchLabelForPredicate(); 
    MatchLabelForPredicate.access$1302(this.mMatchLabelForPredicate, paramInt);
    return findViewByPredicateInsideOut(paramView, this.mMatchLabelForPredicate);
  }
  
  public boolean isVisibleToUserForAutofill(int paramInt) {
    if (this.mContext.isAutofillCompatibilityEnabled()) {
      AccessibilityNodeProvider accessibilityNodeProvider = getAccessibilityNodeProvider();
      if (accessibilityNodeProvider != null) {
        AccessibilityNodeInfo accessibilityNodeInfo = accessibilityNodeProvider.createAccessibilityNodeInfo(paramInt);
        if (accessibilityNodeInfo != null)
          return accessibilityNodeInfo.isVisibleToUser(); 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isVisibleToUserForAutofill(");
        stringBuilder.append(paramInt);
        stringBuilder.append("): no provider");
        Log.w("View", stringBuilder.toString());
      } 
      return false;
    } 
    return true;
  }
  
  public boolean isVisibleToUser() {
    return isVisibleToUser((Rect)null);
  }
  
  protected boolean isVisibleToUser(Rect paramRect) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      if (attachInfo.mWindowVisibility != 0)
        return false; 
      View view = this;
      while (view instanceof View) {
        view = view;
        if (view.getAlpha() <= 0.0F || view.getTransitionAlpha() <= 0.0F || view.getVisibility() != 0)
          return false; 
        ViewParent viewParent = view.mParent;
      } 
      Rect rect = this.mAttachInfo.mTmpInvalRect;
      Point point = this.mAttachInfo.mPoint;
      if (!getGlobalVisibleRect(rect, point))
        return false; 
      if (paramRect != null) {
        rect.offset(-point.x, -point.y);
        return paramRect.intersect(rect);
      } 
      return true;
    } 
    return false;
  }
  
  public AccessibilityDelegate getAccessibilityDelegate() {
    return this.mAccessibilityDelegate;
  }
  
  public void setAccessibilityDelegate(AccessibilityDelegate paramAccessibilityDelegate) {
    this.mAccessibilityDelegate = paramAccessibilityDelegate;
  }
  
  public AccessibilityNodeProvider getAccessibilityNodeProvider() {
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null)
      return accessibilityDelegate.getAccessibilityNodeProvider(this); 
    return null;
  }
  
  public int getAccessibilityViewId() {
    if (this.mAccessibilityViewId == -1) {
      int i = sNextAccessibilityViewId;
      sNextAccessibilityViewId = i + 1;
      this.mAccessibilityViewId = i;
    } 
    return this.mAccessibilityViewId;
  }
  
  public int getAutofillViewId() {
    if (this.mAutofillViewId == -1)
      this.mAutofillViewId = this.mContext.getNextAutofillId(); 
    return this.mAutofillViewId;
  }
  
  public int getAccessibilityWindowId() {
    byte b;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      b = attachInfo.mAccessibilityWindowId;
    } else {
      b = -1;
    } 
    return b;
  }
  
  @ExportedProperty(category = "accessibility")
  public final CharSequence getStateDescription() {
    return this.mStateDescription;
  }
  
  @ExportedProperty(category = "accessibility")
  public CharSequence getContentDescription() {
    return this.mContentDescription;
  }
  
  @RemotableViewMethod
  public void setStateDescription(CharSequence paramCharSequence) {
    CharSequence charSequence = this.mStateDescription;
    if (charSequence == null) {
      if (paramCharSequence == null)
        return; 
    } else if (charSequence.equals(paramCharSequence)) {
      return;
    } 
    this.mStateDescription = paramCharSequence;
    if (!TextUtils.isEmpty(paramCharSequence) && getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
    if (AccessibilityManager.getInstance(this.mContext).isEnabled()) {
      AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain();
      accessibilityEvent.setEventType(2048);
      accessibilityEvent.setContentChangeTypes(64);
      sendAccessibilityEventUnchecked(accessibilityEvent);
    } 
  }
  
  @RemotableViewMethod
  public void setContentDescription(CharSequence paramCharSequence) {
    boolean bool;
    CharSequence charSequence = this.mContentDescription;
    if (charSequence == null) {
      if (paramCharSequence == null)
        return; 
    } else if (charSequence.equals(paramCharSequence)) {
      return;
    } 
    this.mContentDescription = paramCharSequence;
    if (paramCharSequence != null && paramCharSequence.length() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool && getImportantForAccessibility() == 0) {
      setImportantForAccessibility(1);
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } else {
      notifyViewAccessibilityStateChangedIfNeeded(4);
    } 
  }
  
  @RemotableViewMethod
  public void setAccessibilityTraversalBefore(int paramInt) {
    if (this.mAccessibilityTraversalBeforeId == paramInt)
      return; 
    this.mAccessibilityTraversalBeforeId = paramInt;
    notifyViewAccessibilityStateChangedIfNeeded(0);
  }
  
  public int getAccessibilityTraversalBefore() {
    return this.mAccessibilityTraversalBeforeId;
  }
  
  @RemotableViewMethod
  public void setAccessibilityTraversalAfter(int paramInt) {
    if (this.mAccessibilityTraversalAfterId == paramInt)
      return; 
    this.mAccessibilityTraversalAfterId = paramInt;
    notifyViewAccessibilityStateChangedIfNeeded(0);
  }
  
  public int getAccessibilityTraversalAfter() {
    return this.mAccessibilityTraversalAfterId;
  }
  
  @ExportedProperty(category = "accessibility")
  public int getLabelFor() {
    return this.mLabelForId;
  }
  
  @RemotableViewMethod
  public void setLabelFor(int paramInt) {
    if (this.mLabelForId == paramInt)
      return; 
    this.mLabelForId = paramInt;
    if (paramInt != -1 && this.mID == -1)
      this.mID = generateViewId(); 
    notifyViewAccessibilityStateChangedIfNeeded(0);
  }
  
  protected void onFocusLost() {
    resetPressedState();
  }
  
  private void resetPressedState() {
    if ((this.mViewFlags & 0x20) == 32)
      return; 
    if (isPressed()) {
      setPressed(false);
      if (!this.mHasPerformedLongPress)
        removeLongPressCallback(); 
    } 
  }
  
  @ExportedProperty(category = "focus")
  public boolean isFocused() {
    boolean bool;
    if ((this.mPrivateFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public View findFocus() {
    View view;
    if ((this.mPrivateFlags & 0x2) != 0) {
      view = this;
    } else {
      view = null;
    } 
    return view;
  }
  
  public boolean isScrollContainer() {
    boolean bool;
    if ((this.mPrivateFlags & 0x100000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setScrollContainer(boolean paramBoolean) {
    if (paramBoolean) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null && (this.mPrivateFlags & 0x100000) == 0) {
        attachInfo.mScrollContainers.add(this);
        this.mPrivateFlags = 0x100000 | this.mPrivateFlags;
      } 
      this.mPrivateFlags |= 0x80000;
    } else {
      if ((0x100000 & this.mPrivateFlags) != 0)
        this.mAttachInfo.mScrollContainers.remove(this); 
      this.mPrivateFlags &= 0xFFE7FFFF;
    } 
  }
  
  @Deprecated
  public int getDrawingCacheQuality() {
    return this.mViewFlags & 0x180000;
  }
  
  @Deprecated
  public void setDrawingCacheQuality(int paramInt) {
    setFlags(paramInt, 1572864);
  }
  
  public boolean getKeepScreenOn() {
    boolean bool;
    if ((this.mViewFlags & 0x4000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setKeepScreenOn(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 67108864);
  }
  
  public int getNextFocusLeftId() {
    return this.mNextFocusLeftId;
  }
  
  public void setNextFocusLeftId(int paramInt) {
    this.mNextFocusLeftId = paramInt;
  }
  
  public int getNextFocusRightId() {
    return this.mNextFocusRightId;
  }
  
  public void setNextFocusRightId(int paramInt) {
    this.mNextFocusRightId = paramInt;
  }
  
  public int getNextFocusUpId() {
    return this.mNextFocusUpId;
  }
  
  public void setNextFocusUpId(int paramInt) {
    this.mNextFocusUpId = paramInt;
  }
  
  public int getNextFocusDownId() {
    return this.mNextFocusDownId;
  }
  
  public void setNextFocusDownId(int paramInt) {
    this.mNextFocusDownId = paramInt;
  }
  
  public int getNextFocusForwardId() {
    return this.mNextFocusForwardId;
  }
  
  public void setNextFocusForwardId(int paramInt) {
    this.mNextFocusForwardId = paramInt;
  }
  
  public int getNextClusterForwardId() {
    return this.mNextClusterForwardId;
  }
  
  public void setNextClusterForwardId(int paramInt) {
    this.mNextClusterForwardId = paramInt;
  }
  
  public boolean isShown() {
    View view = this;
    do {
      if ((view.mViewFlags & 0xC) != 0)
        return false; 
      ViewParent viewParent = view.mParent;
      if (viewParent == null)
        return false; 
      if (!(viewParent instanceof View))
        return true; 
      view = (View)viewParent;
    } while (view != null);
    return false;
  }
  
  @Deprecated
  protected boolean fitSystemWindows(Rect paramRect) {
    int i = this.mPrivateFlags3;
    if ((i & 0x20) == 0) {
      if (paramRect == null)
        return false; 
      try {
        this.mPrivateFlags3 = i | 0x40;
        WindowInsets windowInsets = new WindowInsets();
        this(paramRect);
        return dispatchApplyWindowInsets(windowInsets).isConsumed();
      } finally {
        this.mPrivateFlags3 &= 0xFFFFFFBF;
      } 
    } 
    return fitSystemWindowsInt(paramRect);
  }
  
  private boolean fitSystemWindowsInt(Rect paramRect) {
    if ((this.mViewFlags & 0x2) == 2) {
      Rect rect = sThreadLocal.get();
      boolean bool = computeFitSystemWindows(paramRect, rect);
      applyInsets(rect);
      return bool;
    } 
    return false;
  }
  
  private void applyInsets(Rect paramRect) {
    this.mUserPaddingStart = Integer.MIN_VALUE;
    this.mUserPaddingEnd = Integer.MIN_VALUE;
    this.mUserPaddingLeftInitial = paramRect.left;
    this.mUserPaddingRightInitial = paramRect.right;
    internalSetPadding(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public WindowInsets onApplyWindowInsets(WindowInsets paramWindowInsets) {
    if ((this.mPrivateFlags4 & 0x100) != 0 && (this.mViewFlags & 0x2) != 0)
      return onApplyFrameworkOptionalFitSystemWindows(paramWindowInsets); 
    if ((this.mPrivateFlags3 & 0x40) == 0) {
      if (fitSystemWindows(paramWindowInsets.getSystemWindowInsetsAsRect()))
        return paramWindowInsets.consumeSystemWindowInsets(); 
    } else if (fitSystemWindowsInt(paramWindowInsets.getSystemWindowInsetsAsRect())) {
      return paramWindowInsets.consumeSystemWindowInsets();
    } 
    return paramWindowInsets;
  }
  
  private WindowInsets onApplyFrameworkOptionalFitSystemWindows(WindowInsets paramWindowInsets) {
    Rect rect = sThreadLocal.get();
    paramWindowInsets = computeSystemWindowInsets(paramWindowInsets, rect);
    applyInsets(rect);
    return paramWindowInsets;
  }
  
  public void setOnApplyWindowInsetsListener(OnApplyWindowInsetsListener paramOnApplyWindowInsetsListener) {
    (getListenerInfo()).mOnApplyWindowInsetsListener = paramOnApplyWindowInsetsListener;
  }
  
  public WindowInsets dispatchApplyWindowInsets(WindowInsets paramWindowInsets) {
    try {
      this.mPrivateFlags3 |= 0x20;
      if (this.mListenerInfo != null && this.mListenerInfo.mOnApplyWindowInsetsListener != null) {
        paramWindowInsets = this.mListenerInfo.mOnApplyWindowInsetsListener.onApplyWindowInsets(this, paramWindowInsets);
        return paramWindowInsets;
      } 
      paramWindowInsets = onApplyWindowInsets(paramWindowInsets);
      return paramWindowInsets;
    } finally {
      this.mPrivateFlags3 &= 0xFFFFFFDF;
    } 
  }
  
  public void setWindowInsetsAnimationCallback(WindowInsetsAnimation.Callback paramCallback) {
    (getListenerInfo()).mWindowInsetsAnimationCallback = paramCallback;
  }
  
  public boolean hasWindowInsetsAnimationCallback() {
    boolean bool;
    if ((getListenerInfo()).mWindowInsetsAnimationCallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void dispatchWindowInsetsAnimationPrepare(WindowInsetsAnimation paramWindowInsetsAnimation) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mWindowInsetsAnimationCallback != null)
      this.mListenerInfo.mWindowInsetsAnimationCallback.onPrepare(paramWindowInsetsAnimation); 
  }
  
  public WindowInsetsAnimation.Bounds dispatchWindowInsetsAnimationStart(WindowInsetsAnimation paramWindowInsetsAnimation, WindowInsetsAnimation.Bounds paramBounds) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mWindowInsetsAnimationCallback != null)
      return this.mListenerInfo.mWindowInsetsAnimationCallback.onStart(paramWindowInsetsAnimation, paramBounds); 
    return paramBounds;
  }
  
  public WindowInsets dispatchWindowInsetsAnimationProgress(WindowInsets paramWindowInsets, List<WindowInsetsAnimation> paramList) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mWindowInsetsAnimationCallback != null)
      return this.mListenerInfo.mWindowInsetsAnimationCallback.onProgress(paramWindowInsets, paramList); 
    return paramWindowInsets;
  }
  
  public void dispatchWindowInsetsAnimationEnd(WindowInsetsAnimation paramWindowInsetsAnimation) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mWindowInsetsAnimationCallback != null)
      this.mListenerInfo.mWindowInsetsAnimationCallback.onEnd(paramWindowInsetsAnimation); 
  }
  
  public void setSystemGestureExclusionRects(List<Rect> paramList) {
    if (paramList.isEmpty() && this.mListenerInfo == null)
      return; 
    ListenerInfo listenerInfo = getListenerInfo();
    if (paramList.isEmpty()) {
      ListenerInfo.access$1402(listenerInfo, null);
      if (listenerInfo.mPositionUpdateListener != null)
        this.mRenderNode.removePositionUpdateListener(listenerInfo.mPositionUpdateListener); 
    } else {
      ListenerInfo.access$1402(listenerInfo, paramList);
      if (listenerInfo.mPositionUpdateListener == null) {
        listenerInfo.mPositionUpdateListener = (RenderNode.PositionUpdateListener)new Object(this);
        this.mRenderNode.addPositionUpdateListener(listenerInfo.mPositionUpdateListener);
      } 
    } 
    postUpdateSystemGestureExclusionRects();
  }
  
  void postUpdateSystemGestureExclusionRects() {
    Handler handler = getHandler();
    if (handler != null)
      handler.postAtFrontOfQueue(new _$$Lambda$WlJa6OPA72p3gYtA3nVKC7Z1tGY(this)); 
  }
  
  void updateSystemGestureExclusionRects() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      attachInfo.mViewRootImpl.updateSystemGestureExclusionRectsForView(this); 
  }
  
  public List<Rect> getSystemGestureExclusionRects() {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null) {
      List<Rect> list = listenerInfo.mSystemGestureExclusionRects;
      if (list != null)
        return list; 
    } 
    return Collections.emptyList();
  }
  
  public void getLocationInSurface(int[] paramArrayOfint) {
    getLocationInWindow(paramArrayOfint);
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null && attachInfo.mViewRootImpl != null) {
      paramArrayOfint[0] = paramArrayOfint[0] + this.mAttachInfo.mViewRootImpl.mWindowAttributes.surfaceInsets.left;
      paramArrayOfint[1] = paramArrayOfint[1] + this.mAttachInfo.mViewRootImpl.mWindowAttributes.surfaceInsets.top;
    } 
  }
  
  public WindowInsets getRootWindowInsets() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      return attachInfo.mViewRootImpl.getWindowInsets(false); 
    return null;
  }
  
  public WindowInsetsController getWindowInsetsController() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      return attachInfo.mViewRootImpl.getInsetsController(); 
    ViewParent viewParent = getParent();
    if (viewParent instanceof View)
      return ((View)viewParent).getWindowInsetsController(); 
    if (viewParent instanceof ViewRootImpl)
      return ((ViewRootImpl)viewParent).getInsetsController(); 
    return null;
  }
  
  @Deprecated
  protected boolean computeFitSystemWindows(Rect paramRect1, Rect paramRect2) {
    WindowInsets windowInsets = computeSystemWindowInsets(new WindowInsets(paramRect1), paramRect2);
    paramRect1.set(windowInsets.getSystemWindowInsetsAsRect());
    return windowInsets.isSystemWindowInsetsConsumed();
  }
  
  public WindowInsets computeSystemWindowInsets(WindowInsets paramWindowInsets, Rect paramRect) {
    Pair<Insets, WindowInsets> pair;
    boolean bool;
    if ((this.mViewFlags & 0x800) != 0 || (this.mPrivateFlags4 & 0x100) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null) {
        Window.OnContentApplyWindowInsetsListener onContentApplyWindowInsetsListener = attachInfo.mContentOnApplyWindowInsetsListener;
        if (onContentApplyWindowInsetsListener == null) {
          paramRect.setEmpty();
          return paramWindowInsets;
        } 
        pair = onContentApplyWindowInsetsListener.onContentApplyWindowInsets(this, paramWindowInsets);
        paramRect.set(((Insets)pair.first).toRect());
        return (WindowInsets)pair.second;
      } 
    } 
    paramRect.set(pair.getSystemWindowInsetsAsRect());
    return pair.consumeSystemWindowInsets().inset(paramRect);
  }
  
  public void setFitsSystemWindows(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 2);
  }
  
  @ExportedProperty
  public boolean getFitsSystemWindows() {
    boolean bool;
    if ((this.mViewFlags & 0x2) == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean fitsSystemWindows() {
    return getFitsSystemWindows();
  }
  
  @Deprecated
  public void requestFitSystemWindows() {
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      viewParent.requestFitSystemWindows(); 
  }
  
  public void requestApplyInsets() {
    requestFitSystemWindows();
  }
  
  public void makeOptionalFitsSystemWindows() {
    setFlags(2048, 2048);
  }
  
  public void makeFrameworkOptionalFitsSystemWindows() {
    this.mPrivateFlags4 |= 0x100;
  }
  
  public boolean isFrameworkOptionalFitsSystemWindows() {
    boolean bool;
    if ((this.mPrivateFlags4 & 0x100) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @ExportedProperty(mapping = {@IntToString(from = 0, to = "VISIBLE"), @IntToString(from = 4, to = "INVISIBLE"), @IntToString(from = 8, to = "GONE")})
  public int getVisibility() {
    return this.mViewFlags & 0xC;
  }
  
  @RemotableViewMethod
  public void setVisibility(int paramInt) {
    setFlags(paramInt, 12);
  }
  
  @ExportedProperty
  public boolean isEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x20) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @RemotableViewMethod
  public void setEnabled(boolean paramBoolean) {
    byte b;
    if (paramBoolean == isEnabled())
      return; 
    if (paramBoolean) {
      b = 0;
    } else {
      b = 32;
    } 
    setFlags(b, 32);
    refreshDrawableState();
    invalidate(true);
    if (!paramBoolean)
      cancelPendingInputEvents(); 
  }
  
  public void setFocusable(boolean paramBoolean) {
    setFocusable(paramBoolean);
  }
  
  public void setFocusable(int paramInt) {
    if ((paramInt & 0x11) == 0)
      setFlags(0, 262144); 
    setFlags(paramInt, 17);
  }
  
  public void setFocusableInTouchMode(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 262144);
    if (paramBoolean)
      setFlags(1, 17); 
  }
  
  public void setAutofillHints(String... paramVarArgs) {
    if (paramVarArgs == null || paramVarArgs.length == 0) {
      this.mAutofillHints = null;
      return;
    } 
    this.mAutofillHints = paramVarArgs;
  }
  
  public void setAutofilled(boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool;
    if (paramBoolean1 != isAutofilled()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      if (paramBoolean1) {
        this.mPrivateFlags3 |= 0x10000;
      } else {
        this.mPrivateFlags3 &= 0xFFFEFFFF;
      } 
      if (paramBoolean2) {
        this.mPrivateFlags4 |= 0x200;
      } else {
        this.mPrivateFlags4 &= 0xFFFFFDFF;
      } 
      invalidate();
    } 
  }
  
  public void setSoundEffectsEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 134217728);
  }
  
  @ExportedProperty
  public boolean isSoundEffectsEnabled() {
    boolean bool;
    if (134217728 == (this.mViewFlags & 0x8000000)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setHapticFeedbackEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 268435456);
  }
  
  @ExportedProperty
  public boolean isHapticFeedbackEnabled() {
    boolean bool;
    if (268435456 == (this.mViewFlags & 0x10000000)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @ExportedProperty(category = "layout", mapping = {@IntToString(from = 0, to = "LTR"), @IntToString(from = 1, to = "RTL"), @IntToString(from = 2, to = "INHERIT"), @IntToString(from = 3, to = "LOCALE")})
  public int getRawLayoutDirection() {
    return (this.mPrivateFlags2 & 0xC) >> 2;
  }
  
  @RemotableViewMethod
  public void setLayoutDirection(int paramInt) {
    if (getRawLayoutDirection() != paramInt) {
      this.mPrivateFlags2 &= 0xFFFFFFF3;
      resetRtlProperties();
      this.mPrivateFlags2 |= paramInt << 2 & 0xC;
      resolveRtlPropertiesIfNeeded();
      requestLayout();
      invalidate(true);
    } 
  }
  
  @ExportedProperty(category = "layout", mapping = {@IntToString(from = 0, to = "RESOLVED_DIRECTION_LTR"), @IntToString(from = 1, to = "RESOLVED_DIRECTION_RTL")})
  public int getLayoutDirection() {
    int i = (getContext().getApplicationInfo()).targetSdkVersion;
    boolean bool = false;
    if (i < 17) {
      this.mPrivateFlags2 |= 0x20;
      return 0;
    } 
    if ((this.mPrivateFlags2 & 0x10) == 16)
      bool = true; 
    return bool;
  }
  
  @ExportedProperty(category = "layout")
  public boolean isLayoutRtl() {
    int i = getLayoutDirection();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  @ExportedProperty(category = "layout")
  public boolean hasTransientState() {
    boolean bool;
    if ((this.mPrivateFlags2 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setHasTransientState(boolean paramBoolean) {
    int i;
    boolean bool = hasTransientState();
    if (paramBoolean) {
      i = this.mTransientStateCount + 1;
    } else {
      i = this.mTransientStateCount - 1;
    } 
    this.mTransientStateCount = i;
    boolean bool1 = false;
    if (i < 0) {
      this.mTransientStateCount = 0;
      Log.e("View", "hasTransientState decremented below 0: unmatched pair of setHasTransientState calls");
    } else if ((paramBoolean && i == 1) || (!paramBoolean && this.mTransientStateCount == 0)) {
      int j = this.mPrivateFlags2;
      i = bool1;
      if (paramBoolean)
        i = Integer.MIN_VALUE; 
      this.mPrivateFlags2 = j & Integer.MAX_VALUE | i;
      paramBoolean = hasTransientState();
      ViewParent viewParent = this.mParent;
      if (viewParent != null && paramBoolean != bool)
        try {
          viewParent.childHasTransientStateChanged(this, paramBoolean);
        } catch (AbstractMethodError abstractMethodError) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mParent.getClass().getSimpleName());
          stringBuilder.append(" does not fully implement ViewParent");
          Log.e("View", stringBuilder.toString(), abstractMethodError);
        }  
    } 
  }
  
  public boolean isAttachedToWindow() {
    boolean bool;
    if (this.mAttachInfo != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isLaidOut() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x4) == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean isLayoutValid() {
    boolean bool;
    if (isLaidOut() && (this.mPrivateFlags & 0x1000) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setWillNotDraw(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 128);
  }
  
  @ExportedProperty(category = "drawing")
  public boolean willNotDraw() {
    boolean bool;
    if ((this.mViewFlags & 0x80) == 128) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public void setWillNotCacheDrawing(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 131072);
  }
  
  @ExportedProperty(category = "drawing")
  @Deprecated
  public boolean willNotCacheDrawing() {
    boolean bool;
    if ((this.mViewFlags & 0x20000) == 131072) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @ExportedProperty
  public boolean isClickable() {
    boolean bool;
    if ((this.mViewFlags & 0x4000) == 16384) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setClickable(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 16384);
  }
  
  public boolean isLongClickable() {
    boolean bool;
    if ((this.mViewFlags & 0x200000) == 2097152) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setLongClickable(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 2097152);
  }
  
  public boolean isContextClickable() {
    boolean bool;
    if ((this.mViewFlags & 0x800000) == 8388608) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setContextClickable(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 8388608);
  }
  
  private void setPressed(boolean paramBoolean, float paramFloat1, float paramFloat2) {
    if (paramBoolean)
      drawableHotspotChanged(paramFloat1, paramFloat2); 
    setPressed(paramBoolean);
  }
  
  public void setPressed(boolean paramBoolean) {
    boolean bool1;
    int i = this.mPrivateFlags;
    boolean bool = true;
    if ((i & 0x4000) == 16384) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (paramBoolean == bool1)
      bool = false; 
    if (paramBoolean) {
      this.mPrivateFlags = 0x4000 | this.mPrivateFlags;
    } else {
      this.mPrivateFlags &= 0xFFFFBFFF;
    } 
    if (bool)
      refreshDrawableState(); 
    dispatchSetPressed(paramBoolean);
  }
  
  protected void dispatchSetPressed(boolean paramBoolean) {}
  
  @ExportedProperty
  public boolean isPressed() {
    boolean bool;
    if ((this.mPrivateFlags & 0x4000) == 16384) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isAssistBlocked() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x4000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setAssistBlocked(boolean paramBoolean) {
    if (paramBoolean) {
      this.mPrivateFlags3 |= 0x4000;
    } else {
      this.mPrivateFlags3 &= 0xFFFFBFFF;
    } 
  }
  
  public boolean isSaveEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x10000) != 65536) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setSaveEnabled(boolean paramBoolean) {
    int i;
    if (paramBoolean) {
      i = 0;
    } else {
      i = 65536;
    } 
    setFlags(i, 65536);
  }
  
  @ExportedProperty
  public boolean getFilterTouchesWhenObscured() {
    boolean bool;
    if ((this.mViewFlags & 0x400) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setFilterTouchesWhenObscured(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 1024);
  }
  
  public boolean isSaveFromParentEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x20000000) != 536870912) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setSaveFromParentEnabled(boolean paramBoolean) {
    int i;
    if (paramBoolean) {
      i = 0;
    } else {
      i = 536870912;
    } 
    setFlags(i, 536870912);
  }
  
  @ExportedProperty(category = "focus")
  public final boolean isFocusable() {
    int i = this.mViewFlags;
    boolean bool = true;
    if (1 != (i & 0x1))
      bool = false; 
    return bool;
  }
  
  @ExportedProperty(category = "focus", mapping = {@IntToString(from = 0, to = "NOT_FOCUSABLE"), @IntToString(from = 1, to = "FOCUSABLE"), @IntToString(from = 16, to = "FOCUSABLE_AUTO")})
  public int getFocusable() {
    int i = this.mViewFlags;
    if ((i & 0x10) > 0) {
      i = 16;
    } else {
      i &= 0x1;
    } 
    return i;
  }
  
  @ExportedProperty(category = "focus")
  public final boolean isFocusableInTouchMode() {
    boolean bool;
    if (262144 == (this.mViewFlags & 0x40000)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isScreenReaderFocusable() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x10000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setScreenReaderFocusable(boolean paramBoolean) {
    updatePflags3AndNotifyA11yIfChanged(268435456, paramBoolean);
  }
  
  public boolean isAccessibilityHeading() {
    boolean bool;
    if ((this.mPrivateFlags3 & Integer.MIN_VALUE) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setAccessibilityHeading(boolean paramBoolean) {
    updatePflags3AndNotifyA11yIfChanged(-2147483648, paramBoolean);
  }
  
  private void updatePflags3AndNotifyA11yIfChanged(int paramInt, boolean paramBoolean) {
    int i = this.mPrivateFlags3;
    if (paramBoolean) {
      paramInt = i | paramInt;
    } else {
      paramInt = i & (paramInt ^ 0xFFFFFFFF);
    } 
    if (paramInt != this.mPrivateFlags3) {
      this.mPrivateFlags3 = paramInt;
      notifyViewAccessibilityStateChangedIfNeeded(0);
    } 
  }
  
  public View focusSearch(int paramInt) {
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      return viewParent.focusSearch(this, paramInt); 
    return null;
  }
  
  @ExportedProperty(category = "focus")
  public final boolean isKeyboardNavigationCluster() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x8000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  View findKeyboardNavigationCluster() {
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof View) {
      View view = ((View)viewParent).findKeyboardNavigationCluster();
      if (view != null)
        return view; 
      if (isKeyboardNavigationCluster())
        return this; 
    } 
    return null;
  }
  
  public void setKeyboardNavigationCluster(boolean paramBoolean) {
    if (paramBoolean) {
      this.mPrivateFlags3 |= 0x8000;
    } else {
      this.mPrivateFlags3 &= 0xFFFF7FFF;
    } 
  }
  
  public final void setFocusedInCluster() {
    setFocusedInCluster(findKeyboardNavigationCluster());
  }
  
  private void setFocusedInCluster(View paramView) {
    if (this instanceof ViewGroup)
      ((ViewGroup)this).mFocusedInCluster = null; 
    if (paramView == this)
      return; 
    ViewParent viewParent = this.mParent;
    View view = this;
    while (viewParent instanceof ViewGroup) {
      ((ViewGroup)viewParent).mFocusedInCluster = view;
      if (viewParent == paramView)
        break; 
      view = (View)viewParent;
      viewParent = viewParent.getParent();
    } 
  }
  
  private void updateFocusedInCluster(View paramView, int paramInt) {
    if (paramView != null) {
      View view1 = paramView.findKeyboardNavigationCluster();
      View view2 = findKeyboardNavigationCluster();
      if (view1 != view2) {
        paramView.setFocusedInCluster(view1);
        if (!(paramView.mParent instanceof ViewGroup))
          return; 
        if (paramInt == 2 || paramInt == 1) {
          ((ViewGroup)paramView.mParent).clearFocusedInCluster(paramView);
          return;
        } 
        if (paramView instanceof ViewGroup) {
          view1 = paramView;
          if (view1.getDescendantFocusability() == 262144)
            if (ViewRootImpl.isViewDescendantOf(this, paramView))
              ((ViewGroup)paramView.mParent).clearFocusedInCluster(paramView);  
        } 
      } 
    } 
  }
  
  @ExportedProperty(category = "focus")
  public final boolean isFocusedByDefault() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x40000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setFocusedByDefault(boolean paramBoolean) {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x40000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean == bool)
      return; 
    if (paramBoolean) {
      this.mPrivateFlags3 |= 0x40000;
    } else {
      this.mPrivateFlags3 &= 0xFFFBFFFF;
    } 
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof ViewGroup)
      if (paramBoolean) {
        ((ViewGroup)viewParent).setDefaultFocus(this);
      } else {
        ((ViewGroup)viewParent).clearDefaultFocus(this);
      }  
  }
  
  boolean hasDefaultFocus() {
    return isFocusedByDefault();
  }
  
  public View keyboardNavigationClusterSearch(View paramView, int paramInt) {
    if (isKeyboardNavigationCluster())
      paramView = this; 
    if (isRootNamespace())
      return FocusFinder.getInstance().findNextKeyboardNavigationCluster(this, paramView, paramInt); 
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      return viewParent.keyboardNavigationClusterSearch(paramView, paramInt); 
    return null;
  }
  
  public boolean dispatchUnhandledMove(View paramView, int paramInt) {
    return false;
  }
  
  public void setDefaultFocusHighlightEnabled(boolean paramBoolean) {
    this.mDefaultFocusHighlightEnabled = paramBoolean;
  }
  
  @ExportedProperty(category = "focus")
  public final boolean getDefaultFocusHighlightEnabled() {
    return this.mDefaultFocusHighlightEnabled;
  }
  
  View findUserSetNextFocus(View paramView, int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 17) {
          if (paramInt != 33) {
            if (paramInt != 66) {
              if (paramInt != 130)
                return null; 
              paramInt = this.mNextFocusDownId;
              if (paramInt == -1)
                return null; 
              return findViewInsideOutShouldExist(paramView, paramInt);
            } 
            paramInt = this.mNextFocusRightId;
            if (paramInt == -1)
              return null; 
            return findViewInsideOutShouldExist(paramView, paramInt);
          } 
          paramInt = this.mNextFocusUpId;
          if (paramInt == -1)
            return null; 
          return findViewInsideOutShouldExist(paramView, paramInt);
        } 
        paramInt = this.mNextFocusLeftId;
        if (paramInt == -1)
          return null; 
        return findViewInsideOutShouldExist(paramView, paramInt);
      } 
      paramInt = this.mNextFocusForwardId;
      if (paramInt == -1)
        return null; 
      return findViewInsideOutShouldExist(paramView, paramInt);
    } 
    if (this.mID == -1)
      return null; 
    return paramView.findViewByPredicateInsideOut(this, new _$$Lambda$View$bhR1vB5ZYp3dv_Kth4jtLSS0KEs(this, paramView, this));
  }
  
  View findUserSetNextKeyboardNavigationCluster(View paramView, int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2)
        return null; 
      paramInt = this.mNextClusterForwardId;
      if (paramInt == -1)
        return null; 
      return findViewInsideOutShouldExist(paramView, paramInt);
    } 
    if (this.mID == -1)
      return null; 
    paramInt = this.mID;
    return paramView.findViewByPredicateInsideOut(this, new _$$Lambda$View$1Ho5ifN6XtJ0tTt_C9rQGIcLJ3E(paramInt));
  }
  
  private View findViewInsideOutShouldExist(View paramView, int paramInt) {
    return findViewInsideOutShouldExist(paramView, this, paramInt);
  }
  
  private View findViewInsideOutShouldExist(View paramView1, View paramView2, int paramInt) {
    if (this.mMatchIdPredicate == null)
      this.mMatchIdPredicate = new MatchIdPredicate(); 
    this.mMatchIdPredicate.mId = paramInt;
    paramView1 = paramView1.findViewByPredicateInsideOut(paramView2, this.mMatchIdPredicate);
    if (paramView1 == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("couldn't find view with id ");
      stringBuilder.append(paramInt);
      Log.w("View", stringBuilder.toString());
    } 
    return paramView1;
  }
  
  public ArrayList<View> getFocusables(int paramInt) {
    ArrayList<View> arrayList = new ArrayList(24);
    addFocusables(arrayList, paramInt);
    return arrayList;
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt) {
    addFocusables(paramArrayList, paramInt, isInTouchMode());
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2) {
    if (paramArrayList == null)
      return; 
    if (!canTakeFocus())
      return; 
    if ((paramInt2 & 0x1) == 1 && !isFocusableInTouchMode())
      return; 
    paramArrayList.add(this);
  }
  
  public void addKeyboardNavigationClusters(Collection<View> paramCollection, int paramInt) {
    if (!isKeyboardNavigationCluster())
      return; 
    if (!hasFocusable())
      return; 
    paramCollection.add(this);
  }
  
  public void findViewsWithText(ArrayList<View> paramArrayList, CharSequence paramCharSequence, int paramInt) {
    if (getAccessibilityNodeProvider() != null) {
      if ((paramInt & 0x4) != 0)
        paramArrayList.add(this); 
    } else if ((paramInt & 0x2) != 0 && paramCharSequence != null && paramCharSequence.length() > 0) {
      CharSequence charSequence = this.mContentDescription;
      if (charSequence != null && charSequence.length() > 0) {
        paramCharSequence = paramCharSequence.toString().toLowerCase();
        charSequence = this.mContentDescription.toString().toLowerCase();
        if (charSequence.contains(paramCharSequence))
          paramArrayList.add(this); 
      } 
    } 
  }
  
  public ArrayList<View> getTouchables() {
    ArrayList<View> arrayList = new ArrayList();
    addTouchables(arrayList);
    return arrayList;
  }
  
  public void addTouchables(ArrayList<View> paramArrayList) {
    int i = this.mViewFlags;
    if (((i & 0x4000) == 16384 || (i & 0x200000) == 2097152 || (i & 0x800000) == 8388608) && (i & 0x20) == 0)
      paramArrayList.add(this); 
  }
  
  public boolean isAccessibilityFocused() {
    boolean bool;
    if ((this.mPrivateFlags2 & 0x4000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean requestAccessibilityFocus() {
    AccessibilityManager accessibilityManager = AccessibilityManager.getInstance(this.mContext);
    if (!accessibilityManager.isEnabled() || !accessibilityManager.isTouchExplorationEnabled())
      return false; 
    if ((this.mViewFlags & 0xC) != 0)
      return false; 
    int i = this.mPrivateFlags2;
    if ((i & 0x4000000) == 0) {
      this.mPrivateFlags2 = i | 0x4000000;
      ViewRootImpl viewRootImpl = getViewRootImpl();
      if (viewRootImpl != null)
        viewRootImpl.setAccessibilityFocus(this, null); 
      invalidate();
      sendAccessibilityEvent(32768);
      return true;
    } 
    return false;
  }
  
  public void clearAccessibilityFocus() {
    clearAccessibilityFocusNoCallbacks(0);
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl != null) {
      View view = viewRootImpl.getAccessibilityFocusedHost();
      if (view != null && ViewRootImpl.isViewDescendantOf(view, this))
        viewRootImpl.setAccessibilityFocus(null, null); 
    } 
  }
  
  private void sendAccessibilityHoverEvent(int paramInt) {
    View view = this;
    while (true) {
      if (view.includeForAccessibility()) {
        view.sendAccessibilityEvent(paramInt);
        return;
      } 
      ViewParent viewParent = view.getParent();
      if (viewParent instanceof View) {
        View view1 = (View)viewParent;
        continue;
      } 
      break;
    } 
  }
  
  void clearAccessibilityFocusNoCallbacks(int paramInt) {
    int i = this.mPrivateFlags2;
    if ((0x4000000 & i) != 0) {
      this.mPrivateFlags2 = i & 0xFBFFFFFF;
      invalidate();
      if (AccessibilityManager.getInstance(this.mContext).isEnabled()) {
        AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(65536);
        accessibilityEvent.setAction(paramInt);
        AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
        if (accessibilityDelegate != null) {
          accessibilityDelegate.sendAccessibilityEventUnchecked(this, accessibilityEvent);
        } else {
          sendAccessibilityEventUnchecked(accessibilityEvent);
        } 
      } 
    } 
  }
  
  public final boolean requestFocus() {
    return requestFocus(130);
  }
  
  public boolean restoreFocusInCluster(int paramInt) {
    if (restoreDefaultFocus())
      return true; 
    return requestFocus(paramInt);
  }
  
  public boolean restoreFocusNotInCluster() {
    return requestFocus(130);
  }
  
  public boolean restoreDefaultFocus() {
    return requestFocus(130);
  }
  
  public final boolean requestFocus(int paramInt) {
    return requestFocus(paramInt, (Rect)null);
  }
  
  public boolean requestFocus(int paramInt, Rect paramRect) {
    return requestFocusNoSearch(paramInt, paramRect);
  }
  
  private boolean requestFocusNoSearch(int paramInt, Rect paramRect) {
    if (!canTakeFocus())
      return false; 
    if (isInTouchMode() && 262144 != (this.mViewFlags & 0x40000))
      return false; 
    if (hasAncestorThatBlocksDescendantFocus())
      return false; 
    if (!isLayoutValid()) {
      this.mPrivateFlags |= 0x1;
    } else {
      clearParentsWantFocus();
    } 
    handleFocusGainInternal(paramInt, paramRect);
    return true;
  }
  
  void clearParentsWantFocus() {
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof View) {
      View view = (View)viewParent;
      view.mPrivateFlags &= 0xFFFFFFFE;
      ((View)viewParent).clearParentsWantFocus();
    } 
  }
  
  public final boolean requestFocusFromTouch() {
    if (isInTouchMode()) {
      ViewRootImpl viewRootImpl = getViewRootImpl();
      if (viewRootImpl != null)
        viewRootImpl.ensureTouchMode(false); 
    } 
    return requestFocus(130);
  }
  
  private boolean hasAncestorThatBlocksDescendantFocus() {
    boolean bool = isFocusableInTouchMode();
    ViewParent viewParent = this.mParent;
    while (viewParent instanceof ViewGroup) {
      viewParent = viewParent;
      if (viewParent.getDescendantFocusability() == 393216 || (!bool && viewParent.shouldBlockFocusForTouchscreen()))
        return true; 
      viewParent = viewParent.getParent();
    } 
    return false;
  }
  
  @ExportedProperty(category = "accessibility", mapping = {@IntToString(from = 0, to = "auto"), @IntToString(from = 1, to = "yes"), @IntToString(from = 2, to = "no"), @IntToString(from = 4, to = "noHideDescendants")})
  public int getImportantForAccessibility() {
    return (this.mPrivateFlags2 & 0x700000) >> 20;
  }
  
  public void setAccessibilityLiveRegion(int paramInt) {
    if (paramInt != getAccessibilityLiveRegion()) {
      int i = this.mPrivateFlags2 & 0xFE7FFFFF;
      this.mPrivateFlags2 = i | paramInt << 23 & 0x1800000;
      notifyViewAccessibilityStateChangedIfNeeded(0);
    } 
  }
  
  public int getAccessibilityLiveRegion() {
    return (this.mPrivateFlags2 & 0x1800000) >> 23;
  }
  
  public void setImportantForAccessibility(int paramInt) {
    int i = getImportantForAccessibility();
    if (paramInt != i) {
      boolean bool2, bool1 = true;
      if (paramInt == 4) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (paramInt == 2 || bool2) {
        View view = findAccessibilityFocusHost(bool2);
        if (view != null)
          view.clearAccessibilityFocus(); 
      } 
      if (i == 0 || paramInt == 0) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i != 0 && includeForAccessibility()) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      int j = this.mPrivateFlags2 & 0xFF8FFFFF;
      this.mPrivateFlags2 = j | paramInt << 20 & 0x700000;
      if (i == 0 || bool2 != includeForAccessibility()) {
        notifySubtreeAccessibilityStateChangedIfNeeded();
        return;
      } 
      notifyViewAccessibilityStateChangedIfNeeded(0);
    } 
  }
  
  private View findAccessibilityFocusHost(boolean paramBoolean) {
    if (isAccessibilityFocusedViewOrHost())
      return this; 
    if (paramBoolean) {
      ViewRootImpl viewRootImpl = getViewRootImpl();
      if (viewRootImpl != null) {
        View view = viewRootImpl.getAccessibilityFocusedHost();
        if (view != null && ViewRootImpl.isViewDescendantOf(view, this))
          return view; 
      } 
    } 
    return null;
  }
  
  public boolean isImportantForAccessibility() {
    int i = getImportantForAccessibility();
    null = false;
    if (i == 2 || i == 4)
      return false; 
    ViewParent viewParent = this.mParent;
    while (viewParent instanceof View) {
      if (((View)viewParent).getImportantForAccessibility() == 4)
        return false; 
      viewParent = viewParent.getParent();
    } 
    if (i != 1 && !isActionableForAccessibility() && !hasListenersForAccessibility() && getAccessibilityNodeProvider() == null && getAccessibilityLiveRegion() == 0 && !isAccessibilityPane()) {
      Context context = this.mContext;
      return AccessibilityManager.getInstance(context).isColorDirectEnabled() ? true : null;
    } 
    return true;
  }
  
  public ViewParent getParentForAccessibility() {
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof View) {
      View view = (View)viewParent;
      if (view.includeForAccessibility())
        return this.mParent; 
      return this.mParent.getParentForAccessibility();
    } 
    return null;
  }
  
  View getSelfOrParentImportantForA11y() {
    if (isImportantForAccessibility())
      return this; 
    ViewParent viewParent = getParentForAccessibility();
    if (viewParent instanceof View)
      return (View)viewParent; 
    return null;
  }
  
  public void addChildrenForAccessibility(ArrayList<View> paramArrayList) {}
  
  public boolean includeForAccessibility() {
    AttachInfo attachInfo = this.mAttachInfo;
    null = false;
    if (attachInfo != null) {
      if ((attachInfo.mAccessibilityFetchFlags & 0x8) == 0)
        return isImportantForAccessibility() ? true : null; 
    } else {
      return false;
    } 
    return true;
  }
  
  public boolean isActionableForAccessibility() {
    return (isClickable() || isLongClickable() || isFocusable());
  }
  
  private boolean hasListenersForAccessibility() {
    ListenerInfo listenerInfo = getListenerInfo();
    return (this.mTouchDelegate != null || listenerInfo.mOnKeyListener != null || listenerInfo.mOnTouchListener != null || listenerInfo.mOnGenericMotionListener != null || listenerInfo.mOnHoverListener != null || listenerInfo.mOnDragListener != null);
  }
  
  public void notifyViewAccessibilityStateChangedIfNeeded(int paramInt) {
    if (!AccessibilityManager.getInstance(this.mContext).isEnabled() || this.mAttachInfo == null)
      return; 
    if (paramInt != 1 && isAccessibilityPane())
      if (getVisibility() == 0 || paramInt == 32) {
        AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain();
        onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setEventType(32);
        accessibilityEvent.setContentChangeTypes(paramInt);
        accessibilityEvent.setSource(this);
        onPopulateAccessibilityEvent(accessibilityEvent);
        ViewParent viewParent = this.mParent;
        if (viewParent != null)
          try {
            viewParent.requestSendAccessibilityEvent(this, accessibilityEvent);
          } catch (AbstractMethodError abstractMethodError) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(this.mParent.getClass().getSimpleName());
            stringBuilder.append(" does not fully implement ViewParent");
            Log.e("View", stringBuilder.toString(), abstractMethodError);
          }  
        return;
      }  
    if (getAccessibilityLiveRegion() != 0) {
      AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain();
      accessibilityEvent.setEventType(2048);
      accessibilityEvent.setContentChangeTypes(paramInt);
      sendAccessibilityEventUnchecked(accessibilityEvent);
    } else {
      ViewParent viewParent = this.mParent;
      if (viewParent != null)
        try {
          viewParent.notifySubtreeAccessibilityStateChanged(this, this, paramInt);
        } catch (AbstractMethodError abstractMethodError) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mParent.getClass().getSimpleName());
          stringBuilder.append(" does not fully implement ViewParent");
          Log.e("View", stringBuilder.toString(), abstractMethodError);
        }  
    } 
  }
  
  public void notifySubtreeAccessibilityStateChangedIfNeeded() {
    if (!AccessibilityManager.getInstance(this.mContext).isEnabled() || this.mAttachInfo == null)
      return; 
    int i = this.mPrivateFlags2;
    if ((i & 0x8000000) == 0) {
      this.mPrivateFlags2 = i | 0x8000000;
      ViewParent viewParent = this.mParent;
      if (viewParent != null)
        try {
          viewParent.notifySubtreeAccessibilityStateChanged(this, this, 1);
        } catch (AbstractMethodError abstractMethodError) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mParent.getClass().getSimpleName());
          stringBuilder.append(" does not fully implement ViewParent");
          Log.e("View", stringBuilder.toString(), abstractMethodError);
        }  
    } 
  }
  
  public void setTransitionVisibility(int paramInt) {
    this.mViewFlags = this.mViewFlags & 0xFFFFFFF3 | paramInt;
  }
  
  void resetSubtreeAccessibilityStateChanged() {
    this.mPrivateFlags2 &= 0xF7FFFFFF;
  }
  
  public boolean dispatchNestedPrePerformAccessibilityAction(int paramInt, Bundle paramBundle) {
    for (ViewParent viewParent = getParent(); viewParent != null; viewParent = viewParent.getParent()) {
      if (viewParent.onNestedPrePerformAccessibilityAction(this, paramInt, paramBundle))
        return true; 
    } 
    return false;
  }
  
  public boolean performAccessibilityAction(int paramInt, Bundle paramBundle) {
    AccessibilityDelegate accessibilityDelegate = this.mAccessibilityDelegate;
    if (accessibilityDelegate != null)
      return accessibilityDelegate.performAccessibilityAction(this, paramInt, paramBundle); 
    return performAccessibilityActionInternal(paramInt, paramBundle);
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    TooltipInfo tooltipInfo;
    AttachInfo attachInfo;
    Rect rect;
    CharSequence charSequence;
    int i;
    if (isNestedScrollingEnabled() && (paramInt == 8192 || paramInt == 4096 || paramInt == 16908344 || paramInt == 16908345 || paramInt == 16908346 || paramInt == 16908347))
      if (dispatchNestedPrePerformAccessibilityAction(paramInt, paramBundle))
        return true;  
    switch (paramInt) {
      default:
        return false;
      case 16908357:
        tooltipInfo = this.mTooltipInfo;
        if (tooltipInfo == null || tooltipInfo.mTooltipPopup == null)
          return false; 
        hideTooltip();
        return true;
      case 16908356:
        tooltipInfo = this.mTooltipInfo;
        if (tooltipInfo != null && tooltipInfo.mTooltipPopup != null)
          return false; 
        return showLongClickTooltip(0, 0);
      case 16908348:
        if (isContextClickable()) {
          performContextClick();
          return true;
        } 
      case 16908342:
        attachInfo = this.mAttachInfo;
        if (attachInfo != null) {
          rect = attachInfo.mTmpInvalRect;
          getDrawingRect(rect);
          return requestRectangleOnScreen(rect, true);
        } 
      case 131072:
        charSequence = getIterableTextForAccessibility();
        if (charSequence == null)
          return false; 
        i = -1;
        if (rect != null) {
          paramInt = rect.getInt("ACTION_ARGUMENT_SELECTION_START_INT", -1);
        } else {
          paramInt = -1;
        } 
        if (rect != null)
          i = rect.getInt("ACTION_ARGUMENT_SELECTION_END_INT", -1); 
        if ((getAccessibilitySelectionStart() != paramInt || getAccessibilitySelectionEnd() != i) && paramInt == i) {
          setAccessibilitySelection(paramInt, i);
          notifyViewAccessibilityStateChangedIfNeeded(0);
          return true;
        } 
      case 512:
        if (rect != null) {
          paramInt = rect.getInt("ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT");
          boolean bool = rect.getBoolean("ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN");
          return traverseAtGranularity(paramInt, false, bool);
        } 
      case 256:
        if (rect != null) {
          paramInt = rect.getInt("ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT");
          boolean bool = rect.getBoolean("ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN");
          return traverseAtGranularity(paramInt, true, bool);
        } 
      case 128:
        if (isAccessibilityFocused()) {
          clearAccessibilityFocus();
          return true;
        } 
      case 64:
        if (!isAccessibilityFocused())
          return requestAccessibilityFocus(); 
      case 32:
        if (isLongClickable()) {
          performLongClick();
          return true;
        } 
      case 16:
        if (isClickable()) {
          performClickInternal();
          return true;
        } 
      case 8:
        if (isSelected()) {
          setSelected(false);
          return isSelected() ^ true;
        } 
      case 4:
        if (!isSelected()) {
          setSelected(true);
          return isSelected();
        } 
      case 2:
        if (hasFocus()) {
          clearFocus();
          return isFocused() ^ true;
        } 
      case 1:
        break;
    } 
    if (!hasFocus()) {
      getViewRootImpl().ensureTouchMode(false);
      return requestFocus();
    } 
  }
  
  private boolean traverseAtGranularity(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    int arrayOfInt[], k, m;
    CharSequence charSequence = getIterableTextForAccessibility();
    if (charSequence == null || charSequence.length() == 0)
      return false; 
    AccessibilityIterators.TextSegmentIterator textSegmentIterator = getIteratorForGranularity(paramInt);
    if (textSegmentIterator == null)
      return false; 
    int i = getAccessibilitySelectionEnd();
    int j = i;
    if (i == -1)
      if (paramBoolean1) {
        j = 0;
      } else {
        j = charSequence.length();
      }  
    if (paramBoolean1) {
      arrayOfInt = textSegmentIterator.following(j);
    } else {
      arrayOfInt = arrayOfInt.preceding(j);
    } 
    if (arrayOfInt == null)
      return false; 
    i = arrayOfInt[0];
    j = arrayOfInt[1];
    if (paramBoolean2 && isAccessibilitySelectionExtendable()) {
      k = getAccessibilitySelectionStart();
      m = k;
      if (k == -1)
        if (paramBoolean1) {
          m = i;
        } else {
          m = j;
        }  
      if (paramBoolean1) {
        k = j;
      } else {
        k = i;
      } 
    } else {
      if (paramBoolean1) {
        m = j;
      } else {
        m = i;
      } 
      k = m;
    } 
    setAccessibilitySelection(m, k);
    if (paramBoolean1) {
      m = 256;
    } else {
      m = 512;
    } 
    sendViewTextTraversedAtGranularityEvent(m, paramInt, i, j);
    return true;
  }
  
  public CharSequence getIterableTextForAccessibility() {
    return getContentDescription();
  }
  
  public boolean isAccessibilitySelectionExtendable() {
    return false;
  }
  
  public int getAccessibilitySelectionStart() {
    return this.mAccessibilityCursorPosition;
  }
  
  public int getAccessibilitySelectionEnd() {
    return getAccessibilitySelectionStart();
  }
  
  public void setAccessibilitySelection(int paramInt1, int paramInt2) {
    if (paramInt1 == paramInt2 && paramInt2 == this.mAccessibilityCursorPosition)
      return; 
    if (paramInt1 >= 0 && paramInt1 == paramInt2 && paramInt2 <= getIterableTextForAccessibility().length()) {
      this.mAccessibilityCursorPosition = paramInt1;
    } else {
      this.mAccessibilityCursorPosition = -1;
    } 
    sendAccessibilityEvent(8192);
  }
  
  private void sendViewTextTraversedAtGranularityEvent(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mParent == null)
      return; 
    AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(131072);
    onInitializeAccessibilityEvent(accessibilityEvent);
    onPopulateAccessibilityEvent(accessibilityEvent);
    accessibilityEvent.setFromIndex(paramInt3);
    accessibilityEvent.setToIndex(paramInt4);
    accessibilityEvent.setAction(paramInt1);
    accessibilityEvent.setMovementGranularity(paramInt2);
    this.mParent.requestSendAccessibilityEvent(this, accessibilityEvent);
  }
  
  public AccessibilityIterators.TextSegmentIterator getIteratorForGranularity(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt == 8) {
          CharSequence charSequence = getIterableTextForAccessibility();
          if (charSequence != null && charSequence.length() > 0) {
            AccessibilityIterators.ParagraphTextSegmentIterator paragraphTextSegmentIterator = AccessibilityIterators.ParagraphTextSegmentIterator.getInstance();
            paragraphTextSegmentIterator.initialize(charSequence.toString());
            return paragraphTextSegmentIterator;
          } 
        } 
      } else {
        CharSequence charSequence = getIterableTextForAccessibility();
        if (charSequence != null && charSequence.length() > 0) {
          Context context = this.mContext;
          Locale locale = (context.getResources().getConfiguration()).locale;
          AccessibilityIterators.WordTextSegmentIterator wordTextSegmentIterator = AccessibilityIterators.WordTextSegmentIterator.getInstance(locale);
          wordTextSegmentIterator.initialize(charSequence.toString());
          return wordTextSegmentIterator;
        } 
      } 
    } else {
      CharSequence charSequence = getIterableTextForAccessibility();
      if (charSequence != null && charSequence.length() > 0) {
        Context context = this.mContext;
        Locale locale = (context.getResources().getConfiguration()).locale;
        AccessibilityIterators.CharacterTextSegmentIterator characterTextSegmentIterator = AccessibilityIterators.CharacterTextSegmentIterator.getInstance(locale);
        characterTextSegmentIterator.initialize(charSequence.toString());
        return characterTextSegmentIterator;
      } 
    } 
    return null;
  }
  
  public final boolean isTemporarilyDetached() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x2000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void dispatchStartTemporaryDetach() {
    this.mPrivateFlags3 |= 0x2000000;
    notifyEnterOrExitForAutoFillIfNeeded(false);
    notifyAppearedOrDisappearedForContentCaptureIfNeeded(false);
    onStartTemporaryDetach();
  }
  
  public void onStartTemporaryDetach() {
    removeUnsetPressCallback();
    this.mPrivateFlags |= 0x4000000;
  }
  
  public void dispatchFinishTemporaryDetach() {
    this.mPrivateFlags3 &= 0xFDFFFFFF;
    onFinishTemporaryDetach();
    if (hasWindowFocus() && hasFocus())
      notifyFocusChangeToImeFocusController(true); 
    notifyEnterOrExitForAutoFillIfNeeded(true);
    notifyAppearedOrDisappearedForContentCaptureIfNeeded(true);
  }
  
  public void onFinishTemporaryDetach() {}
  
  public KeyEvent.DispatcherState getKeyDispatcherState() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      KeyEvent.DispatcherState dispatcherState = attachInfo.mKeyDispatchState;
    } else {
      attachInfo = null;
    } 
    return (KeyEvent.DispatcherState)attachInfo;
  }
  
  public boolean dispatchKeyEventPreIme(KeyEvent paramKeyEvent) {
    return onKeyPreIme(paramKeyEvent.getKeyCode(), paramKeyEvent);
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    InputEventConsistencyVerifier inputEventConsistencyVerifier2 = this.mInputEventConsistencyVerifier;
    if (inputEventConsistencyVerifier2 != null)
      inputEventConsistencyVerifier2.onKeyEvent(paramKeyEvent, 0); 
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnKeyListener != null && (this.mViewFlags & 0x20) == 0 && listenerInfo.mOnKeyListener.onKey(this, paramKeyEvent.getKeyCode(), paramKeyEvent))
      return true; 
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      KeyEvent.DispatcherState dispatcherState = attachInfo.mKeyDispatchState;
    } else {
      attachInfo = null;
    } 
    if (paramKeyEvent.dispatch(this, (KeyEvent.DispatcherState)attachInfo, this))
      return true; 
    InputEventConsistencyVerifier inputEventConsistencyVerifier1 = this.mInputEventConsistencyVerifier;
    if (inputEventConsistencyVerifier1 != null)
      inputEventConsistencyVerifier1.onUnhandledEvent(paramKeyEvent, 0); 
    return false;
  }
  
  public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent) {
    return onKeyShortcut(paramKeyEvent.getKeyCode(), paramKeyEvent);
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.isTargetAccessibilityFocus()) {
      if (!isAccessibilityFocusedViewOrHost())
        return false; 
      paramMotionEvent.setTargetAccessibilityFocus(false);
    } 
    boolean bool1 = false, bool2 = false;
    InputEventConsistencyVerifier inputEventConsistencyVerifier = this.mInputEventConsistencyVerifier;
    if (inputEventConsistencyVerifier != null)
      inputEventConsistencyVerifier.onTouchEvent(paramMotionEvent, 0); 
    int i = paramMotionEvent.getActionMasked();
    if (i == 0) {
      SeempLog.record(3);
      stopNestedScroll();
    } 
    if (onFilterTouchEventForSecurity(paramMotionEvent)) {
      bool1 = bool2;
      if ((this.mViewFlags & 0x20) == 0) {
        bool1 = bool2;
        if (handleScrollBarDragging(paramMotionEvent))
          bool1 = true; 
      } 
      ListenerInfo listenerInfo = this.mListenerInfo;
      bool2 = bool1;
      if (listenerInfo != null) {
        bool2 = bool1;
        if (listenerInfo.mOnTouchListener != null) {
          bool2 = bool1;
          if ((this.mViewFlags & 0x20) == 0) {
            bool2 = bool1;
            if (listenerInfo.mOnTouchListener.onTouch(this, paramMotionEvent))
              bool2 = true; 
          } 
        } 
      } 
      bool1 = bool2;
      if (!bool2) {
        bool1 = bool2;
        if (onTouchEvent(paramMotionEvent))
          bool1 = true; 
      } 
    } 
    if (!bool1) {
      inputEventConsistencyVerifier = this.mInputEventConsistencyVerifier;
      if (inputEventConsistencyVerifier != null)
        inputEventConsistencyVerifier.onUnhandledEvent(paramMotionEvent, 0); 
    } 
    if (i == 1 || i == 3 || (i == 0 && !bool1))
      stopNestedScroll(); 
    return bool1;
  }
  
  boolean isAccessibilityFocusedViewOrHost() {
    if (!isAccessibilityFocused()) {
      if (getViewRootImpl() != null) {
        ViewRootImpl viewRootImpl = getViewRootImpl();
        if (viewRootImpl.getAccessibilityFocusedHost() == this)
          return true; 
      } 
      return false;
    } 
    return true;
  }
  
  protected boolean canReceivePointerEvents() {
    return ((this.mViewFlags & 0xC) == 0 || getAnimation() != null);
  }
  
  public boolean onFilterTouchEventForSecurity(MotionEvent paramMotionEvent) {
    if ((this.mViewFlags & 0x400) != 0 && (paramMotionEvent.getFlags() & 0x1) != 0)
      return false; 
    return true;
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent) {
    InputEventConsistencyVerifier inputEventConsistencyVerifier = this.mInputEventConsistencyVerifier;
    if (inputEventConsistencyVerifier != null)
      inputEventConsistencyVerifier.onTrackballEvent(paramMotionEvent, 0); 
    return onTrackballEvent(paramMotionEvent);
  }
  
  public boolean dispatchCapturedPointerEvent(MotionEvent paramMotionEvent) {
    if (!hasPointerCapture())
      return false; 
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnCapturedPointerListener != null) {
      OnCapturedPointerListener onCapturedPointerListener = listenerInfo.mOnCapturedPointerListener;
      if (onCapturedPointerListener.onCapturedPointer(this, paramMotionEvent))
        return true; 
    } 
    return onCapturedPointerEvent(paramMotionEvent);
  }
  
  public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent) {
    InputEventConsistencyVerifier inputEventConsistencyVerifier = this.mInputEventConsistencyVerifier;
    if (inputEventConsistencyVerifier != null)
      inputEventConsistencyVerifier.onGenericMotionEvent(paramMotionEvent, 0); 
    int i = paramMotionEvent.getSource();
    if ((i & 0x2) != 0) {
      i = paramMotionEvent.getAction();
      if (i == 9 || i == 7 || i == 10) {
        if (dispatchHoverEvent(paramMotionEvent))
          return true; 
      } else if (dispatchGenericPointerEvent(paramMotionEvent)) {
        return true;
      } 
    } else if (dispatchGenericFocusedEvent(paramMotionEvent)) {
      return true;
    } 
    if (dispatchGenericMotionEventInternal(paramMotionEvent))
      return true; 
    inputEventConsistencyVerifier = this.mInputEventConsistencyVerifier;
    if (inputEventConsistencyVerifier != null)
      inputEventConsistencyVerifier.onUnhandledEvent(paramMotionEvent, 0); 
    return false;
  }
  
  private boolean dispatchGenericMotionEventInternal(MotionEvent paramMotionEvent) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnGenericMotionListener != null && (this.mViewFlags & 0x20) == 0)
      if (listenerInfo.mOnGenericMotionListener.onGenericMotion(this, paramMotionEvent))
        return true;  
    if (onGenericMotionEvent(paramMotionEvent))
      return true; 
    int i = paramMotionEvent.getActionButton();
    int j = paramMotionEvent.getActionMasked();
    if (j != 11) {
      if (j == 12)
        if (this.mInContextButtonPress && (i == 32 || i == 2)) {
          this.mInContextButtonPress = false;
          this.mIgnoreNextUpEvent = true;
        }  
    } else if (isContextClickable() && !this.mInContextButtonPress && !this.mHasPerformedLongPress && (i == 32 || i == 2)) {
      if (performContextClick(paramMotionEvent.getX(), paramMotionEvent.getY())) {
        this.mInContextButtonPress = true;
        setPressed(true, paramMotionEvent.getX(), paramMotionEvent.getY());
        removeTapCallback();
        removeLongPressCallback();
        return true;
      } 
    } 
    InputEventConsistencyVerifier inputEventConsistencyVerifier = this.mInputEventConsistencyVerifier;
    if (inputEventConsistencyVerifier != null)
      inputEventConsistencyVerifier.onUnhandledEvent(paramMotionEvent, 0); 
    return false;
  }
  
  protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnHoverListener != null && (this.mViewFlags & 0x20) == 0)
      if (listenerInfo.mOnHoverListener.onHover(this, paramMotionEvent))
        return true;  
    return onHoverEvent(paramMotionEvent);
  }
  
  protected boolean hasHoveredChild() {
    return false;
  }
  
  protected boolean pointInHoveredChild(MotionEvent paramMotionEvent) {
    return false;
  }
  
  protected boolean dispatchGenericPointerEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  protected boolean dispatchGenericFocusedEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public final boolean dispatchPointerEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.isTouchEvent())
      return dispatchTouchEvent(paramMotionEvent); 
    return dispatchGenericMotionEvent(paramMotionEvent);
  }
  
  public void dispatchWindowFocusChanged(boolean paramBoolean) {
    onWindowFocusChanged(paramBoolean);
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    if (!paramBoolean) {
      if (isPressed())
        setPressed(false); 
      this.mPrivateFlags3 &= 0xFFFDFFFF;
      if ((this.mPrivateFlags & 0x2) != 0)
        notifyFocusChangeToImeFocusController(false); 
      removeLongPressCallback();
      removeTapCallback();
      onFocusLost();
    } else if ((this.mPrivateFlags & 0x2) != 0) {
      notifyFocusChangeToImeFocusController(true);
    } 
    refreshDrawableState();
  }
  
  public boolean hasWindowFocus() {
    boolean bool;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null && attachInfo.mHasWindowFocus) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasImeFocus() {
    boolean bool;
    if (getViewRootImpl() != null && getViewRootImpl().getImeFocusController().hasImeFocus()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void dispatchVisibilityChanged(View paramView, int paramInt) {
    onVisibilityChanged(paramView, paramInt);
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt) {}
  
  public void dispatchDisplayHint(int paramInt) {
    onDisplayHint(paramInt);
  }
  
  protected void onDisplayHint(int paramInt) {}
  
  public void dispatchWindowVisibilityChanged(int paramInt) {
    onWindowVisibilityChanged(paramInt);
  }
  
  protected void onWindowVisibilityChanged(int paramInt) {
    if (paramInt == 0)
      initialAwakenScrollBars(); 
  }
  
  boolean isAggregatedVisible() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x20000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean dispatchVisibilityAggregated(boolean paramBoolean) {
    int i = getVisibility();
    boolean bool = true;
    if (i == 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i != 0 || !paramBoolean)
      onVisibilityAggregated(paramBoolean); 
    if (i != 0 && paramBoolean) {
      paramBoolean = bool;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  public void onVisibilityAggregated(boolean paramBoolean) {
    int i;
    boolean bool = isAggregatedVisible();
    if (paramBoolean) {
      i = this.mPrivateFlags3 | 0x20000000;
    } else {
      i = this.mPrivateFlags3 & 0xDFFFFFFF;
    } 
    this.mPrivateFlags3 = i;
    if (paramBoolean && this.mAttachInfo != null)
      initialAwakenScrollBars(); 
    Drawable drawable = this.mBackground;
    if (drawable != null && paramBoolean != drawable.isVisible())
      drawable.setVisible(paramBoolean, false); 
    drawable = this.mDefaultFocusHighlight;
    if (drawable != null && paramBoolean != drawable.isVisible())
      drawable.setVisible(paramBoolean, false); 
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null) {
      Drawable drawable1 = foregroundInfo.mDrawable;
    } else {
      foregroundInfo = null;
    } 
    if (foregroundInfo != null && paramBoolean != foregroundInfo.isVisible())
      foregroundInfo.setVisible(paramBoolean, false); 
    if (isAutofillable()) {
      AutofillManager autofillManager = getAutofillManager();
      if (autofillManager != null && getAutofillViewId() > 1073741823) {
        Handler handler = this.mVisibilityChangeForAutofillHandler;
        if (handler != null)
          handler.removeMessages(0); 
        if (paramBoolean) {
          autofillManager.notifyViewVisibilityChanged(this, true);
        } else {
          if (this.mVisibilityChangeForAutofillHandler == null)
            this.mVisibilityChangeForAutofillHandler = new VisibilityChangeForAutofillHandler(this); 
          this.mVisibilityChangeForAutofillHandler.obtainMessage(0, this).sendToTarget();
        } 
      } 
    } 
    if (paramBoolean != bool) {
      if (isAccessibilityPane()) {
        if (paramBoolean) {
          i = 16;
        } else {
          i = 32;
        } 
        notifyViewAccessibilityStateChangedIfNeeded(i);
      } 
      notifyAppearedOrDisappearedForContentCaptureIfNeeded(paramBoolean);
      if (!getSystemGestureExclusionRects().isEmpty())
        postUpdateSystemGestureExclusionRects(); 
    } 
  }
  
  public int getWindowVisibility() {
    byte b;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      b = attachInfo.mWindowVisibility;
    } else {
      b = 8;
    } 
    return b;
  }
  
  public void getWindowVisibleDisplayFrame(Rect paramRect) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      try {
        attachInfo.mSession.getDisplayFrame(this.mAttachInfo.mWindow, paramRect);
        if (this.mContext.getPackageName() != null && this.mContext.getPackageName().equals("com.tencent.mm") && this.mAttachInfo != null && this.mAttachInfo.mViewRootImpl != null)
          paramRect.bottom = Math.max(paramRect.bottom, this.mAttachInfo.mViewRootImpl.mWinFrame.bottom); 
        Rect rect = this.mAttachInfo.mVisibleInsets;
        paramRect.left += rect.left;
        paramRect.top += rect.top;
        paramRect.right -= rect.right;
        paramRect.bottom -= rect.bottom;
        return;
      } catch (RemoteException remoteException) {
        return;
      }  
    Display display = DisplayManagerGlobal.getInstance().getRealDisplay(0);
    display.getRectSize((Rect)remoteException);
  }
  
  public void getWindowDisplayFrame(Rect paramRect) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      try {
        attachInfo.mSession.getDisplayFrame(this.mAttachInfo.mWindow, paramRect);
        return;
      } catch (RemoteException remoteException) {
        return;
      }  
    Display display = DisplayManagerGlobal.getInstance().getRealDisplay(0);
    display.getRectSize((Rect)remoteException);
  }
  
  public void dispatchConfigurationChanged(Configuration paramConfiguration) {
    onConfigurationChanged(paramConfiguration);
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {}
  
  void dispatchCollectViewAttributes(AttachInfo paramAttachInfo, int paramInt) {
    performCollectViewAttributes(paramAttachInfo, paramInt);
  }
  
  void performCollectViewAttributes(AttachInfo paramAttachInfo, int paramInt) {
    if ((paramInt & 0xC) == 0) {
      if ((this.mViewFlags & 0x4000000) == 67108864)
        paramAttachInfo.mKeepScreenOn = true; 
      paramAttachInfo.mSystemUiVisibility |= this.mSystemUiVisibility;
      ListenerInfo listenerInfo = this.mListenerInfo;
      if (listenerInfo != null && listenerInfo.mOnSystemUiVisibilityChangeListener != null)
        paramAttachInfo.mHasSystemUiListeners = true; 
    } 
  }
  
  void needGlobalAttributesUpdate(boolean paramBoolean) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null && !attachInfo.mRecomputeGlobalAttributes && (paramBoolean || attachInfo.mKeepScreenOn || attachInfo.mSystemUiVisibility != 0 || attachInfo.mHasSystemUiListeners))
      attachInfo.mRecomputeGlobalAttributes = true; 
  }
  
  @ExportedProperty
  public boolean isInTouchMode() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      return attachInfo.mInTouchMode; 
    return ViewRootImpl.isInTouchMode();
  }
  
  @CapturedViewProperty
  public final Context getContext() {
    return this.mContext;
  }
  
  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    SeempLog.record(4);
    if (KeyEvent.isConfirmKey(paramInt)) {
      if ((this.mViewFlags & 0x20) == 32)
        return true; 
      if (paramKeyEvent.getRepeatCount() == 0) {
        paramInt = this.mViewFlags;
        if ((paramInt & 0x4000) == 16384 || (paramInt & 0x200000) == 2097152) {
          paramInt = 1;
        } else {
          paramInt = 0;
        } 
        if (paramInt != 0 || (this.mViewFlags & 0x40000000) == 1073741824) {
          float f1 = getWidth() / 2.0F;
          float f2 = getHeight() / 2.0F;
          if (paramInt != 0)
            setPressed(true, f1, f2); 
          long l = ViewConfiguration.getLongPressTimeout();
          checkForLongClick(l, f1, f2, 0);
          return true;
        } 
      } 
    } 
    return false;
  }
  
  public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    SeempLog.record(5);
    if (KeyEvent.isConfirmKey(paramInt)) {
      paramInt = this.mViewFlags;
      if ((paramInt & 0x20) == 32)
        return true; 
      if ((paramInt & 0x4000) == 16384 && isPressed()) {
        setPressed(false);
        if (!this.mHasPerformedLongPress) {
          removeLongPressCallback();
          if (!paramKeyEvent.isCanceled())
            return performClickInternal(); 
        } 
      } 
    } 
    return false;
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyShortcut(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onCheckIsTextEditor() {
    return false;
  }
  
  public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo) {
    return null;
  }
  
  public boolean checkInputConnectionProxy(View paramView) {
    return false;
  }
  
  public void createContextMenu(ContextMenu paramContextMenu) {
    ContextMenu.ContextMenuInfo contextMenuInfo = getContextMenuInfo();
    ((MenuBuilder)paramContextMenu).setCurrentMenuInfo(contextMenuInfo);
    onCreateContextMenu(paramContextMenu);
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnCreateContextMenuListener != null)
      listenerInfo.mOnCreateContextMenuListener.onCreateContextMenu(paramContextMenu, this, contextMenuInfo); 
    ((MenuBuilder)paramContextMenu).setCurrentMenuInfo(null);
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      viewParent.createContextMenu(paramContextMenu); 
  }
  
  protected ContextMenu.ContextMenuInfo getContextMenuInfo() {
    return null;
  }
  
  protected void onCreateContextMenu(ContextMenu paramContextMenu) {}
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  private boolean dispatchTouchExplorationHoverEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mContext : Landroid/content/Context;
    //   4: invokestatic getInstance : (Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;
    //   7: astore_2
    //   8: aload_2
    //   9: invokevirtual isEnabled : ()Z
    //   12: ifeq -> 426
    //   15: aload_2
    //   16: invokevirtual isTouchExplorationEnabled : ()Z
    //   19: ifne -> 25
    //   22: goto -> 426
    //   25: aload_0
    //   26: getfield mHoveringTouchDelegate : Z
    //   29: istore_3
    //   30: aload_1
    //   31: invokevirtual getActionMasked : ()I
    //   34: istore #4
    //   36: iconst_0
    //   37: istore #5
    //   39: iconst_0
    //   40: istore #6
    //   42: iconst_0
    //   43: istore #7
    //   45: aload_0
    //   46: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   49: invokevirtual getTouchDelegateInfo : ()Landroid/view/accessibility/AccessibilityNodeInfo$TouchDelegateInfo;
    //   52: astore #8
    //   54: iconst_0
    //   55: istore #9
    //   57: iload #9
    //   59: aload #8
    //   61: invokevirtual getRegionCount : ()I
    //   64: if_icmpge -> 101
    //   67: aload #8
    //   69: iload #9
    //   71: invokevirtual getRegionAt : (I)Landroid/graphics/Region;
    //   74: astore_2
    //   75: aload_2
    //   76: aload_1
    //   77: invokevirtual getX : ()F
    //   80: f2i
    //   81: aload_1
    //   82: invokevirtual getY : ()F
    //   85: f2i
    //   86: invokevirtual contains : (II)Z
    //   89: ifeq -> 95
    //   92: iconst_1
    //   93: istore #5
    //   95: iinc #9, 1
    //   98: goto -> 57
    //   101: iload_3
    //   102: ifne -> 140
    //   105: iload #4
    //   107: bipush #9
    //   109: if_icmpeq -> 119
    //   112: iload #4
    //   114: bipush #7
    //   116: if_icmpne -> 172
    //   119: aload_0
    //   120: aload_1
    //   121: invokevirtual pointInHoveredChild : (Landroid/view/MotionEvent;)Z
    //   124: ifne -> 172
    //   127: iload #5
    //   129: ifeq -> 172
    //   132: aload_0
    //   133: iconst_1
    //   134: putfield mHoveringTouchDelegate : Z
    //   137: goto -> 172
    //   140: iload #4
    //   142: bipush #10
    //   144: if_icmpeq -> 167
    //   147: iload #4
    //   149: bipush #7
    //   151: if_icmpne -> 172
    //   154: aload_0
    //   155: aload_1
    //   156: invokevirtual pointInHoveredChild : (Landroid/view/MotionEvent;)Z
    //   159: ifne -> 167
    //   162: iload #5
    //   164: ifne -> 172
    //   167: aload_0
    //   168: iconst_0
    //   169: putfield mHoveringTouchDelegate : Z
    //   172: iload #4
    //   174: bipush #7
    //   176: if_icmpeq -> 256
    //   179: iload #4
    //   181: bipush #9
    //   183: if_icmpeq -> 224
    //   186: iload #4
    //   188: bipush #10
    //   190: if_icmpeq -> 200
    //   193: iload #6
    //   195: istore #10
    //   197: goto -> 423
    //   200: iload #6
    //   202: istore #10
    //   204: iload_3
    //   205: ifeq -> 423
    //   208: aload_0
    //   209: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   212: aload_1
    //   213: invokevirtual onTouchExplorationHoverEvent : (Landroid/view/MotionEvent;)Z
    //   216: pop
    //   217: iload #6
    //   219: istore #10
    //   221: goto -> 423
    //   224: iload #6
    //   226: istore #10
    //   228: iload_3
    //   229: ifne -> 423
    //   232: iload #6
    //   234: istore #10
    //   236: aload_0
    //   237: getfield mHoveringTouchDelegate : Z
    //   240: ifeq -> 423
    //   243: aload_0
    //   244: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   247: aload_1
    //   248: invokevirtual onTouchExplorationHoverEvent : (Landroid/view/MotionEvent;)Z
    //   251: istore #10
    //   253: goto -> 423
    //   256: iload_3
    //   257: ifeq -> 280
    //   260: aload_0
    //   261: getfield mHoveringTouchDelegate : Z
    //   264: ifeq -> 280
    //   267: aload_0
    //   268: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   271: aload_1
    //   272: invokevirtual onTouchExplorationHoverEvent : (Landroid/view/MotionEvent;)Z
    //   275: istore #10
    //   277: goto -> 423
    //   280: iload_3
    //   281: ifne -> 344
    //   284: aload_0
    //   285: getfield mHoveringTouchDelegate : Z
    //   288: ifeq -> 344
    //   291: aload_1
    //   292: invokevirtual getHistorySize : ()I
    //   295: ifne -> 301
    //   298: goto -> 306
    //   301: aload_1
    //   302: invokestatic obtainNoHistory : (Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    //   305: astore_1
    //   306: aload_1
    //   307: bipush #9
    //   309: invokevirtual setAction : (I)V
    //   312: aload_0
    //   313: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   316: aload_1
    //   317: invokevirtual onTouchExplorationHoverEvent : (Landroid/view/MotionEvent;)Z
    //   320: istore #10
    //   322: aload_1
    //   323: iload #4
    //   325: invokevirtual setAction : (I)V
    //   328: iload #10
    //   330: aload_0
    //   331: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   334: aload_1
    //   335: invokevirtual onTouchExplorationHoverEvent : (Landroid/view/MotionEvent;)Z
    //   338: ior
    //   339: istore #10
    //   341: goto -> 423
    //   344: iload #7
    //   346: istore #10
    //   348: iload_3
    //   349: ifeq -> 341
    //   352: iload #7
    //   354: istore #10
    //   356: aload_0
    //   357: getfield mHoveringTouchDelegate : Z
    //   360: ifne -> 341
    //   363: aload_1
    //   364: invokevirtual isHoverExitPending : ()Z
    //   367: istore #10
    //   369: aload_1
    //   370: iconst_1
    //   371: invokevirtual setHoverExitPending : (Z)V
    //   374: aload_0
    //   375: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   378: aload_1
    //   379: invokevirtual onTouchExplorationHoverEvent : (Landroid/view/MotionEvent;)Z
    //   382: pop
    //   383: aload_1
    //   384: invokevirtual getHistorySize : ()I
    //   387: ifne -> 393
    //   390: goto -> 398
    //   393: aload_1
    //   394: invokestatic obtainNoHistory : (Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    //   397: astore_1
    //   398: aload_1
    //   399: iload #10
    //   401: invokevirtual setHoverExitPending : (Z)V
    //   404: aload_1
    //   405: bipush #10
    //   407: invokevirtual setAction : (I)V
    //   410: aload_0
    //   411: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   414: aload_1
    //   415: invokevirtual onTouchExplorationHoverEvent : (Landroid/view/MotionEvent;)Z
    //   418: pop
    //   419: iload #6
    //   421: istore #10
    //   423: iload #10
    //   425: ireturn
    //   426: iconst_0
    //   427: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #15467	-> 0
    //   #15468	-> 8
    //   #15472	-> 25
    //   #15473	-> 30
    //   #15474	-> 36
    //   #15475	-> 39
    //   #15477	-> 45
    //   #15478	-> 54
    //   #15479	-> 67
    //   #15480	-> 75
    //   #15481	-> 92
    //   #15478	-> 95
    //   #15489	-> 101
    //   #15490	-> 105
    //   #15492	-> 119
    //   #15494	-> 132
    //   #15497	-> 140
    //   #15499	-> 154
    //   #15500	-> 167
    //   #15503	-> 172
    //   #15534	-> 200
    //   #15535	-> 208
    //   #15529	-> 224
    //   #15530	-> 243
    //   #15505	-> 256
    //   #15507	-> 267
    //   #15508	-> 280
    //   #15510	-> 291
    //   #15511	-> 298
    //   #15512	-> 306
    //   #15513	-> 312
    //   #15514	-> 322
    //   #15515	-> 328
    //   #15516	-> 341
    //   #15518	-> 363
    //   #15519	-> 369
    //   #15520	-> 374
    //   #15521	-> 383
    //   #15522	-> 390
    //   #15523	-> 398
    //   #15524	-> 404
    //   #15525	-> 410
    //   #15526	-> 419
    //   #15539	-> 423
    //   #15469	-> 426
  }
  
  public boolean onHoverEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   4: ifnull -> 17
    //   7: aload_0
    //   8: aload_1
    //   9: invokespecial dispatchTouchExplorationHoverEvent : (Landroid/view/MotionEvent;)Z
    //   12: ifeq -> 17
    //   15: iconst_1
    //   16: ireturn
    //   17: aload_1
    //   18: invokevirtual getActionMasked : ()I
    //   21: istore_2
    //   22: aload_0
    //   23: getfield mSendingHoverAccessibilityEvents : Z
    //   26: ifne -> 78
    //   29: iload_2
    //   30: bipush #9
    //   32: if_icmpeq -> 41
    //   35: iload_2
    //   36: bipush #7
    //   38: if_icmpne -> 117
    //   41: aload_0
    //   42: invokevirtual hasHoveredChild : ()Z
    //   45: ifne -> 117
    //   48: aload_0
    //   49: aload_1
    //   50: invokevirtual getX : ()F
    //   53: aload_1
    //   54: invokevirtual getY : ()F
    //   57: invokevirtual pointInView : (FF)Z
    //   60: ifeq -> 117
    //   63: aload_0
    //   64: sipush #128
    //   67: invokespecial sendAccessibilityHoverEvent : (I)V
    //   70: aload_0
    //   71: iconst_1
    //   72: putfield mSendingHoverAccessibilityEvents : Z
    //   75: goto -> 117
    //   78: iload_2
    //   79: bipush #10
    //   81: if_icmpeq -> 105
    //   84: iload_2
    //   85: bipush #7
    //   87: if_icmpne -> 117
    //   90: aload_0
    //   91: aload_1
    //   92: invokevirtual getX : ()F
    //   95: aload_1
    //   96: invokevirtual getY : ()F
    //   99: invokevirtual pointInView : (FF)Z
    //   102: ifne -> 117
    //   105: aload_0
    //   106: iconst_0
    //   107: putfield mSendingHoverAccessibilityEvents : Z
    //   110: aload_0
    //   111: sipush #256
    //   114: invokespecial sendAccessibilityHoverEvent : (I)V
    //   117: iload_2
    //   118: bipush #9
    //   120: if_icmpeq -> 129
    //   123: iload_2
    //   124: bipush #7
    //   126: if_icmpne -> 159
    //   129: aload_1
    //   130: sipush #8194
    //   133: invokevirtual isFromSource : (I)Z
    //   136: ifeq -> 159
    //   139: aload_0
    //   140: aload_1
    //   141: invokevirtual getX : ()F
    //   144: aload_1
    //   145: invokevirtual getY : ()F
    //   148: invokevirtual isOnScrollbar : (FF)Z
    //   151: ifeq -> 159
    //   154: aload_0
    //   155: invokevirtual awakenScrollBars : ()Z
    //   158: pop
    //   159: aload_0
    //   160: invokespecial isHoverable : ()Z
    //   163: ifne -> 178
    //   166: aload_0
    //   167: invokevirtual isHovered : ()Z
    //   170: ifeq -> 176
    //   173: goto -> 178
    //   176: iconst_0
    //   177: ireturn
    //   178: iload_2
    //   179: bipush #9
    //   181: if_icmpeq -> 201
    //   184: iload_2
    //   185: bipush #10
    //   187: if_icmpeq -> 193
    //   190: goto -> 206
    //   193: aload_0
    //   194: iconst_0
    //   195: invokevirtual setHovered : (Z)V
    //   198: goto -> 206
    //   201: aload_0
    //   202: iconst_1
    //   203: invokevirtual setHovered : (Z)V
    //   206: aload_0
    //   207: aload_1
    //   208: invokespecial dispatchGenericMotionEventInternal : (Landroid/view/MotionEvent;)Z
    //   211: pop
    //   212: iconst_1
    //   213: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #15579	-> 0
    //   #15580	-> 15
    //   #15586	-> 17
    //   #15587	-> 22
    //   #15588	-> 29
    //   #15590	-> 41
    //   #15591	-> 48
    //   #15592	-> 63
    //   #15593	-> 70
    //   #15596	-> 78
    //   #15598	-> 90
    //   #15599	-> 105
    //   #15600	-> 110
    //   #15604	-> 117
    //   #15605	-> 129
    //   #15606	-> 139
    //   #15607	-> 154
    //   #15612	-> 159
    //   #15635	-> 176
    //   #15613	-> 178
    //   #15618	-> 193
    //   #15615	-> 201
    //   #15616	-> 206
    //   #15629	-> 206
    //   #15632	-> 212
  }
  
  private boolean isHoverable() {
    int i = this.mViewFlags;
    boolean bool = false;
    if ((i & 0x20) == 32)
      return false; 
    if ((i & 0x4000) == 16384 || (i & 0x200000) == 2097152 || (i & 0x800000) == 8388608)
      bool = true; 
    return bool;
  }
  
  @ExportedProperty
  public boolean isHovered() {
    boolean bool;
    if ((this.mPrivateFlags & 0x10000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setHovered(boolean paramBoolean) {
    if (paramBoolean) {
      int i = this.mPrivateFlags;
      if ((i & 0x10000000) == 0) {
        this.mPrivateFlags = 0x10000000 | i;
        refreshDrawableState();
        onHoverChanged(true);
      } 
    } else {
      int i = this.mPrivateFlags;
      if ((0x10000000 & i) != 0) {
        this.mPrivateFlags = 0xEFFFFFFF & i;
        refreshDrawableState();
        onHoverChanged(false);
      } 
    } 
  }
  
  public void onHoverChanged(boolean paramBoolean) {}
  
  protected boolean handleScrollBarDragging(MotionEvent paramMotionEvent) {
    if (this.mScrollCache == null)
      return false; 
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    int i = paramMotionEvent.getAction();
    if (this.mScrollCache.mScrollBarDraggingState != 0 || i == 0)
      if (paramMotionEvent.isFromSource(8194) && paramMotionEvent.isButtonPressed(1)) {
        if (i != 0) {
          if (i != 2) {
            this.mScrollCache.mScrollBarDraggingState = 0;
            return false;
          } 
          if (this.mScrollCache.mScrollBarDraggingState == 0)
            return false; 
          if (this.mScrollCache.mScrollBarDraggingState == 1) {
            Rect rect = this.mScrollCache.mScrollBarBounds;
            getVerticalScrollBarBounds(rect, (Rect)null);
            i = computeVerticalScrollRange();
            int j = computeVerticalScrollOffset();
            int k = computeVerticalScrollExtent();
            int m = rect.height(), n = rect.width();
            n = ScrollBarUtils.getThumbLength(m, n, k, i);
            m = rect.height();
            j = ScrollBarUtils.getThumbOffset(m, n, k, i, j);
            float f3 = this.mScrollCache.mScrollBarDraggingPos;
            f1 = (rect.height() - n);
            float f4 = j;
            f4 = Math.min(Math.max(f4 + f2 - f3, 0.0F), f1);
            n = getHeight();
            if (Math.round(f4) != j && f1 > 0.0F && n > 0 && k > 0) {
              i = Math.round((i - k) / k / n * f4 / f1);
              if (i != getScrollY()) {
                this.mScrollCache.mScrollBarDraggingPos = f2;
                setScrollY(i);
              } 
            } 
            return true;
          } 
          if (this.mScrollCache.mScrollBarDraggingState == 2) {
            Rect rect = this.mScrollCache.mScrollBarBounds;
            getHorizontalScrollBarBounds(rect, (Rect)null);
            i = computeHorizontalScrollRange();
            int j = computeHorizontalScrollOffset();
            int k = computeHorizontalScrollExtent();
            int m = rect.width(), n = rect.height();
            n = ScrollBarUtils.getThumbLength(m, n, k, i);
            m = rect.width();
            j = ScrollBarUtils.getThumbOffset(m, n, k, i, j);
            float f3 = this.mScrollCache.mScrollBarDraggingPos;
            f2 = (rect.width() - n);
            float f4 = j;
            f4 = Math.min(Math.max(f4 + f1 - f3, 0.0F), f2);
            n = getWidth();
            if (Math.round(f4) != j && f2 > 0.0F && n > 0 && k > 0) {
              i = Math.round((i - k) / k / n * f4 / f2);
              if (i != getScrollX()) {
                this.mScrollCache.mScrollBarDraggingPos = f1;
                setScrollX(i);
              } 
            } 
            return true;
          } 
        } 
        if (this.mScrollCache.state == 0)
          return false; 
        if (isOnVerticalScrollbarThumb(f1, f2)) {
          this.mScrollCache.mScrollBarDraggingState = 1;
          this.mScrollCache.mScrollBarDraggingPos = f2;
          return true;
        } 
        if (isOnHorizontalScrollbarThumb(f1, f2)) {
          this.mScrollCache.mScrollBarDraggingState = 2;
          this.mScrollCache.mScrollBarDraggingPos = f1;
          return true;
        } 
        this.mScrollCache.mScrollBarDraggingState = 0;
        return false;
      }  
    this.mScrollCache.mScrollBarDraggingState = 0;
    return false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: iconst_3
    //   1: invokestatic record : (I)I
    //   4: pop
    //   5: aload_1
    //   6: invokevirtual getX : ()F
    //   9: fstore_2
    //   10: aload_1
    //   11: invokevirtual getY : ()F
    //   14: fstore_3
    //   15: aload_0
    //   16: getfield mViewFlags : I
    //   19: istore #4
    //   21: aload_1
    //   22: invokevirtual getAction : ()I
    //   25: istore #5
    //   27: getstatic android/view/View.sShouldCheckTouchBoost : Z
    //   30: ifeq -> 119
    //   33: iload #5
    //   35: ifne -> 119
    //   38: aload_0
    //   39: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   42: ifnull -> 119
    //   45: aload_0
    //   46: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   49: invokevirtual getClass : ()Ljava/lang/Class;
    //   52: invokevirtual toString : ()Ljava/lang/String;
    //   55: ldc 'ConversationOverscrollListView'
    //   57: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   60: ifeq -> 119
    //   63: ldc_w 'View'
    //   66: ldc_w 'Boost from press enter conversation.'
    //   69: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   72: pop
    //   73: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   76: ifnonnull -> 96
    //   79: ldc android/view/View
    //   81: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   84: astore #6
    //   86: aload #6
    //   88: putstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   91: aload #6
    //   93: ifnull -> 119
    //   96: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   99: new com/oplus/orms/info/OrmsSaParam
    //   102: dup
    //   103: ldc_w ''
    //   106: ldc_w 'ORMS_ACTION_TOUCH_BOOST'
    //   109: sipush #500
    //   112: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   115: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   118: pop2
    //   119: iconst_0
    //   120: istore #7
    //   122: iload #4
    //   124: sipush #16384
    //   127: iand
    //   128: sipush #16384
    //   131: if_icmpeq -> 165
    //   134: iload #4
    //   136: ldc_w 2097152
    //   139: iand
    //   140: ldc_w 2097152
    //   143: if_icmpeq -> 165
    //   146: iload #4
    //   148: ldc 8388608
    //   150: iand
    //   151: ldc 8388608
    //   153: if_icmpne -> 159
    //   156: goto -> 165
    //   159: iconst_0
    //   160: istore #8
    //   162: goto -> 168
    //   165: iconst_1
    //   166: istore #8
    //   168: iload #4
    //   170: bipush #32
    //   172: iand
    //   173: bipush #32
    //   175: if_icmpne -> 215
    //   178: iload #5
    //   180: iconst_1
    //   181: if_icmpne -> 200
    //   184: aload_0
    //   185: getfield mPrivateFlags : I
    //   188: sipush #16384
    //   191: iand
    //   192: ifeq -> 200
    //   195: aload_0
    //   196: iconst_0
    //   197: invokevirtual setPressed : (Z)V
    //   200: aload_0
    //   201: aload_0
    //   202: getfield mPrivateFlags3 : I
    //   205: ldc_w -131073
    //   208: iand
    //   209: putfield mPrivateFlags3 : I
    //   212: iload #8
    //   214: ireturn
    //   215: aload_0
    //   216: getfield mTouchDelegate : Landroid/view/TouchDelegate;
    //   219: astore #6
    //   221: aload #6
    //   223: ifnull -> 237
    //   226: aload #6
    //   228: aload_1
    //   229: invokevirtual onTouchEvent : (Landroid/view/MotionEvent;)Z
    //   232: ifeq -> 237
    //   235: iconst_1
    //   236: ireturn
    //   237: iload #8
    //   239: ifne -> 259
    //   242: iload #4
    //   244: ldc_w 1073741824
    //   247: iand
    //   248: ldc_w 1073741824
    //   251: if_icmpne -> 257
    //   254: goto -> 259
    //   257: iconst_0
    //   258: ireturn
    //   259: iload #5
    //   261: ifeq -> 836
    //   264: iload #5
    //   266: iconst_1
    //   267: if_icmpeq -> 547
    //   270: iload #5
    //   272: iconst_2
    //   273: if_icmpeq -> 333
    //   276: iload #5
    //   278: iconst_3
    //   279: if_icmpeq -> 285
    //   282: goto -> 1000
    //   285: iload #8
    //   287: ifeq -> 295
    //   290: aload_0
    //   291: iconst_0
    //   292: invokevirtual setPressed : (Z)V
    //   295: aload_0
    //   296: invokespecial removeTapCallback : ()V
    //   299: aload_0
    //   300: invokespecial removeLongPressCallback : ()V
    //   303: aload_0
    //   304: iconst_0
    //   305: putfield mInContextButtonPress : Z
    //   308: aload_0
    //   309: iconst_0
    //   310: putfield mHasPerformedLongPress : Z
    //   313: aload_0
    //   314: iconst_0
    //   315: putfield mIgnoreNextUpEvent : Z
    //   318: aload_0
    //   319: aload_0
    //   320: getfield mPrivateFlags3 : I
    //   323: ldc_w -131073
    //   326: iand
    //   327: putfield mPrivateFlags3 : I
    //   330: goto -> 1000
    //   333: iload #8
    //   335: ifeq -> 344
    //   338: aload_0
    //   339: fload_2
    //   340: fload_3
    //   341: invokevirtual drawableHotspotChanged : (FF)V
    //   344: aload_1
    //   345: invokevirtual getClassification : ()I
    //   348: istore #9
    //   350: iload #9
    //   352: iconst_1
    //   353: if_icmpne -> 362
    //   356: iconst_1
    //   357: istore #4
    //   359: goto -> 365
    //   362: iconst_0
    //   363: istore #4
    //   365: aload_0
    //   366: getfield mTouchSlop : I
    //   369: istore #5
    //   371: iload #4
    //   373: ifeq -> 455
    //   376: aload_0
    //   377: invokespecial hasPendingLongPressCallback : ()Z
    //   380: ifeq -> 455
    //   383: aload_0
    //   384: fload_2
    //   385: fload_3
    //   386: iload #5
    //   388: i2f
    //   389: invokevirtual pointInView : (FFF)Z
    //   392: ifne -> 441
    //   395: aload_0
    //   396: invokespecial removeLongPressCallback : ()V
    //   399: invokestatic getLongPressTimeout : ()I
    //   402: i2f
    //   403: aload_0
    //   404: getfield mAmbiguousGestureMultiplier : F
    //   407: fmul
    //   408: f2l
    //   409: lstore #10
    //   411: aload_1
    //   412: invokevirtual getEventTime : ()J
    //   415: lstore #12
    //   417: aload_1
    //   418: invokevirtual getDownTime : ()J
    //   421: lstore #14
    //   423: aload_0
    //   424: lload #10
    //   426: lload #12
    //   428: lload #14
    //   430: lsub
    //   431: lsub
    //   432: fload_2
    //   433: fload_3
    //   434: iconst_3
    //   435: invokespecial checkForLongClick : (JFFI)V
    //   438: goto -> 441
    //   441: iload #5
    //   443: i2f
    //   444: aload_0
    //   445: getfield mAmbiguousGestureMultiplier : F
    //   448: fmul
    //   449: f2i
    //   450: istore #4
    //   452: goto -> 459
    //   455: iload #5
    //   457: istore #4
    //   459: aload_0
    //   460: fload_2
    //   461: fload_3
    //   462: iload #4
    //   464: i2f
    //   465: invokevirtual pointInView : (FFF)Z
    //   468: ifne -> 507
    //   471: aload_0
    //   472: invokespecial removeTapCallback : ()V
    //   475: aload_0
    //   476: invokespecial removeLongPressCallback : ()V
    //   479: aload_0
    //   480: getfield mPrivateFlags : I
    //   483: sipush #16384
    //   486: iand
    //   487: ifeq -> 495
    //   490: aload_0
    //   491: iconst_0
    //   492: invokevirtual setPressed : (Z)V
    //   495: aload_0
    //   496: aload_0
    //   497: getfield mPrivateFlags3 : I
    //   500: ldc_w -131073
    //   503: iand
    //   504: putfield mPrivateFlags3 : I
    //   507: iload #7
    //   509: istore #4
    //   511: iload #9
    //   513: iconst_2
    //   514: if_icmpne -> 520
    //   517: iconst_1
    //   518: istore #4
    //   520: iload #4
    //   522: ifeq -> 1000
    //   525: aload_0
    //   526: invokespecial hasPendingLongPressCallback : ()Z
    //   529: ifeq -> 1000
    //   532: aload_0
    //   533: invokespecial removeLongPressCallback : ()V
    //   536: aload_0
    //   537: lconst_0
    //   538: fload_2
    //   539: fload_3
    //   540: iconst_4
    //   541: invokespecial checkForLongClick : (JFFI)V
    //   544: goto -> 1000
    //   547: aload_0
    //   548: aload_0
    //   549: getfield mPrivateFlags3 : I
    //   552: ldc_w -131073
    //   555: iand
    //   556: putfield mPrivateFlags3 : I
    //   559: iload #4
    //   561: ldc_w 1073741824
    //   564: iand
    //   565: ldc_w 1073741824
    //   568: if_icmpne -> 575
    //   571: aload_0
    //   572: invokespecial handleTooltipUp : ()V
    //   575: iload #8
    //   577: ifne -> 606
    //   580: aload_0
    //   581: invokespecial removeTapCallback : ()V
    //   584: aload_0
    //   585: invokespecial removeLongPressCallback : ()V
    //   588: aload_0
    //   589: iconst_0
    //   590: putfield mInContextButtonPress : Z
    //   593: aload_0
    //   594: iconst_0
    //   595: putfield mHasPerformedLongPress : Z
    //   598: aload_0
    //   599: iconst_0
    //   600: putfield mIgnoreNextUpEvent : Z
    //   603: goto -> 1000
    //   606: aload_0
    //   607: getfield mPrivateFlags : I
    //   610: ldc_w 33554432
    //   613: iand
    //   614: ifeq -> 623
    //   617: iconst_1
    //   618: istore #4
    //   620: goto -> 626
    //   623: iconst_0
    //   624: istore #4
    //   626: aload_0
    //   627: getfield mPrivateFlags : I
    //   630: sipush #16384
    //   633: iand
    //   634: ifne -> 642
    //   637: iload #4
    //   639: ifeq -> 828
    //   642: iconst_0
    //   643: istore #16
    //   645: iload #16
    //   647: istore #8
    //   649: aload_0
    //   650: invokevirtual isFocusable : ()Z
    //   653: ifeq -> 684
    //   656: iload #16
    //   658: istore #8
    //   660: aload_0
    //   661: invokevirtual isFocusableInTouchMode : ()Z
    //   664: ifeq -> 684
    //   667: iload #16
    //   669: istore #8
    //   671: aload_0
    //   672: invokevirtual isFocused : ()Z
    //   675: ifne -> 684
    //   678: aload_0
    //   679: invokevirtual requestFocus : ()Z
    //   682: istore #8
    //   684: iload #4
    //   686: ifeq -> 696
    //   689: aload_0
    //   690: iconst_1
    //   691: fload_2
    //   692: fload_3
    //   693: invokespecial setPressed : (ZFF)V
    //   696: aload_0
    //   697: getfield mHasPerformedLongPress : Z
    //   700: ifne -> 759
    //   703: aload_0
    //   704: getfield mIgnoreNextUpEvent : Z
    //   707: ifne -> 759
    //   710: aload_0
    //   711: invokespecial removeLongPressCallback : ()V
    //   714: iload #8
    //   716: ifne -> 759
    //   719: aload_0
    //   720: getfield mPerformClick : Landroid/view/View$PerformClick;
    //   723: ifnonnull -> 739
    //   726: aload_0
    //   727: new android/view/View$PerformClick
    //   730: dup
    //   731: aload_0
    //   732: aconst_null
    //   733: invokespecial <init> : (Landroid/view/View;Landroid/view/View$1;)V
    //   736: putfield mPerformClick : Landroid/view/View$PerformClick;
    //   739: aload_0
    //   740: aload_0
    //   741: getfield mPerformClick : Landroid/view/View$PerformClick;
    //   744: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   747: ifne -> 755
    //   750: aload_0
    //   751: invokespecial performClickInternal : ()Z
    //   754: pop
    //   755: aload_0
    //   756: invokevirtual hookPerformClick : ()V
    //   759: aload_0
    //   760: getfield mUnsetPressedState : Landroid/view/View$UnsetPressedState;
    //   763: ifnonnull -> 779
    //   766: aload_0
    //   767: new android/view/View$UnsetPressedState
    //   770: dup
    //   771: aload_0
    //   772: aconst_null
    //   773: invokespecial <init> : (Landroid/view/View;Landroid/view/View$1;)V
    //   776: putfield mUnsetPressedState : Landroid/view/View$UnsetPressedState;
    //   779: iload #4
    //   781: ifeq -> 806
    //   784: aload_0
    //   785: getfield mUnsetPressedState : Landroid/view/View$UnsetPressedState;
    //   788: astore_1
    //   789: invokestatic getPressedStateDuration : ()I
    //   792: i2l
    //   793: lstore #12
    //   795: aload_0
    //   796: aload_1
    //   797: lload #12
    //   799: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   802: pop
    //   803: goto -> 824
    //   806: aload_0
    //   807: aload_0
    //   808: getfield mUnsetPressedState : Landroid/view/View$UnsetPressedState;
    //   811: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   814: ifne -> 824
    //   817: aload_0
    //   818: getfield mUnsetPressedState : Landroid/view/View$UnsetPressedState;
    //   821: invokevirtual run : ()V
    //   824: aload_0
    //   825: invokespecial removeTapCallback : ()V
    //   828: aload_0
    //   829: iconst_0
    //   830: putfield mIgnoreNextUpEvent : Z
    //   833: goto -> 1000
    //   836: aload_1
    //   837: invokevirtual getSource : ()I
    //   840: sipush #4098
    //   843: if_icmpne -> 858
    //   846: aload_0
    //   847: aload_0
    //   848: getfield mPrivateFlags3 : I
    //   851: ldc_w 131072
    //   854: ior
    //   855: putfield mPrivateFlags3 : I
    //   858: aload_0
    //   859: iconst_0
    //   860: putfield mHasPerformedLongPress : Z
    //   863: iload #8
    //   865: ifne -> 886
    //   868: invokestatic getLongPressTimeout : ()I
    //   871: i2l
    //   872: lstore #12
    //   874: aload_0
    //   875: lload #12
    //   877: fload_2
    //   878: fload_3
    //   879: iconst_3
    //   880: invokespecial checkForLongClick : (JFFI)V
    //   883: goto -> 1000
    //   886: aload_0
    //   887: aload_1
    //   888: invokevirtual performButtonActionOnTouchDown : (Landroid/view/MotionEvent;)Z
    //   891: ifeq -> 897
    //   894: goto -> 1000
    //   897: aload_0
    //   898: invokevirtual isInScrollingContainer : ()Z
    //   901: istore #8
    //   903: iload #8
    //   905: ifeq -> 978
    //   908: aload_0
    //   909: aload_0
    //   910: getfield mPrivateFlags : I
    //   913: ldc_w 33554432
    //   916: ior
    //   917: putfield mPrivateFlags : I
    //   920: aload_0
    //   921: getfield mPendingCheckForTap : Landroid/view/View$CheckForTap;
    //   924: ifnonnull -> 940
    //   927: aload_0
    //   928: new android/view/View$CheckForTap
    //   931: dup
    //   932: aload_0
    //   933: aconst_null
    //   934: invokespecial <init> : (Landroid/view/View;Landroid/view/View$1;)V
    //   937: putfield mPendingCheckForTap : Landroid/view/View$CheckForTap;
    //   940: aload_0
    //   941: getfield mPendingCheckForTap : Landroid/view/View$CheckForTap;
    //   944: aload_1
    //   945: invokevirtual getX : ()F
    //   948: putfield x : F
    //   951: aload_0
    //   952: getfield mPendingCheckForTap : Landroid/view/View$CheckForTap;
    //   955: aload_1
    //   956: invokevirtual getY : ()F
    //   959: putfield y : F
    //   962: aload_0
    //   963: aload_0
    //   964: getfield mPendingCheckForTap : Landroid/view/View$CheckForTap;
    //   967: invokestatic getTapTimeout : ()I
    //   970: i2l
    //   971: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   974: pop
    //   975: goto -> 1000
    //   978: aload_0
    //   979: iconst_1
    //   980: fload_2
    //   981: fload_3
    //   982: invokespecial setPressed : (ZFF)V
    //   985: invokestatic getLongPressTimeout : ()I
    //   988: i2l
    //   989: lstore #12
    //   991: aload_0
    //   992: lload #12
    //   994: fload_2
    //   995: fload_3
    //   996: iconst_3
    //   997: invokespecial checkForLongClick : (JFFI)V
    //   1000: iconst_1
    //   1001: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #15839	-> 0
    //   #15840	-> 5
    //   #15841	-> 10
    //   #15842	-> 15
    //   #15843	-> 21
    //   #15847	-> 27
    //   #15848	-> 38
    //   #15849	-> 63
    //   #15850	-> 73
    //   #15851	-> 96
    //   #15856	-> 119
    //   #15860	-> 168
    //   #15861	-> 178
    //   #15862	-> 195
    //   #15864	-> 200
    //   #15867	-> 212
    //   #15869	-> 215
    //   #15870	-> 226
    //   #15871	-> 235
    //   #15869	-> 237
    //   #15875	-> 237
    //   #16059	-> 257
    //   #15876	-> 259
    //   #15990	-> 285
    //   #15991	-> 290
    //   #15993	-> 295
    //   #15994	-> 299
    //   #15995	-> 303
    //   #15996	-> 308
    //   #15997	-> 313
    //   #15998	-> 318
    //   #15999	-> 330
    //   #16002	-> 333
    //   #16003	-> 338
    //   #16006	-> 344
    //   #16007	-> 350
    //   #16009	-> 365
    //   #16010	-> 371
    //   #16011	-> 383
    //   #16015	-> 395
    //   #16016	-> 399
    //   #16019	-> 411
    //   #16020	-> 423
    //   #16011	-> 441
    //   #16026	-> 441
    //   #16010	-> 455
    //   #16030	-> 459
    //   #16033	-> 471
    //   #16034	-> 475
    //   #16035	-> 479
    //   #16036	-> 490
    //   #16038	-> 495
    //   #16041	-> 507
    //   #16043	-> 520
    //   #16045	-> 532
    //   #16046	-> 536
    //   #15878	-> 547
    //   #15879	-> 559
    //   #15880	-> 571
    //   #15882	-> 575
    //   #15883	-> 580
    //   #15884	-> 584
    //   #15885	-> 588
    //   #15886	-> 593
    //   #15887	-> 598
    //   #15888	-> 603
    //   #15890	-> 606
    //   #15891	-> 626
    //   #15894	-> 642
    //   #15895	-> 645
    //   #15896	-> 678
    //   #15899	-> 684
    //   #15904	-> 689
    //   #15907	-> 696
    //   #15909	-> 710
    //   #15912	-> 714
    //   #15916	-> 719
    //   #15917	-> 726
    //   #15919	-> 739
    //   #15920	-> 750
    //   #15924	-> 755
    //   #15929	-> 759
    //   #15930	-> 766
    //   #15933	-> 779
    //   #15934	-> 784
    //   #15935	-> 789
    //   #15934	-> 795
    //   #15936	-> 806
    //   #15938	-> 817
    //   #15941	-> 824
    //   #15943	-> 828
    //   #15944	-> 833
    //   #15947	-> 836
    //   #15948	-> 846
    //   #15950	-> 858
    //   #15952	-> 863
    //   #15953	-> 868
    //   #15954	-> 868
    //   #15953	-> 874
    //   #15958	-> 883
    //   #15961	-> 886
    //   #15962	-> 894
    //   #15966	-> 897
    //   #15970	-> 903
    //   #15971	-> 908
    //   #15972	-> 920
    //   #15973	-> 927
    //   #15975	-> 940
    //   #15976	-> 951
    //   #15977	-> 962
    //   #15980	-> 978
    //   #15981	-> 985
    //   #15982	-> 985
    //   #15981	-> 991
    //   #15987	-> 1000
    //   #16056	-> 1000
  }
  
  public boolean isInScrollingContainer() {
    ViewParent viewParent = getParent();
    while (viewParent != null && viewParent instanceof ViewGroup) {
      if (((ViewGroup)viewParent).shouldDelayChildPressedState())
        return true; 
      viewParent = viewParent.getParent();
    } 
    return false;
  }
  
  private void removeLongPressCallback() {
    CheckForLongPress checkForLongPress = this.mPendingCheckForLongPress;
    if (checkForLongPress != null)
      removeCallbacks(checkForLongPress); 
  }
  
  private boolean hasPendingLongPressCallback() {
    if (this.mPendingCheckForLongPress == null)
      return false; 
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return false; 
    return attachInfo.mHandler.hasCallbacks(this.mPendingCheckForLongPress);
  }
  
  private void removePerformClickCallback() {
    PerformClick performClick = this.mPerformClick;
    if (performClick != null)
      removeCallbacks(performClick); 
  }
  
  private void removeUnsetPressCallback() {
    if ((this.mPrivateFlags & 0x4000) != 0 && this.mUnsetPressedState != null) {
      setPressed(false);
      removeCallbacks(this.mUnsetPressedState);
    } 
  }
  
  private void removeTapCallback() {
    CheckForTap checkForTap = this.mPendingCheckForTap;
    if (checkForTap != null) {
      this.mPrivateFlags &= 0xFDFFFFFF;
      removeCallbacks(checkForTap);
    } 
  }
  
  public void cancelLongPress() {
    removeLongPressCallback();
    removeTapCallback();
  }
  
  public void setTouchDelegate(TouchDelegate paramTouchDelegate) {
    this.mTouchDelegate = paramTouchDelegate;
  }
  
  public TouchDelegate getTouchDelegate() {
    return this.mTouchDelegate;
  }
  
  public final void requestUnbufferedDispatch(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    if (this.mAttachInfo != null && (i == 0 || i == 2))
      if (paramMotionEvent.isTouchEvent()) {
        this.mAttachInfo.mUnbufferedDispatchRequested = true;
        return;
      }  
  }
  
  public final void requestUnbufferedDispatch(int paramInt) {
    if (this.mUnbufferedInputSource == paramInt)
      return; 
    this.mUnbufferedInputSource = paramInt;
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      viewParent.onDescendantUnbufferedRequested(); 
  }
  
  private boolean hasSize() {
    boolean bool;
    if (this.mBottom > this.mTop && this.mRight > this.mLeft) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean canTakeFocus() {
    int i = this.mViewFlags;
    null = true;
    if ((i & 0xC) == 0 && (i & 0x1) == 1 && (i & 0x20) == 0)
      if (!sCanFocusZeroSized) {
        if (!isLayoutValid() || hasSize())
          return null; 
      } else {
        return null;
      }  
    return false;
  }
  
  void setFlags(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mContext : Landroid/content/Context;
    //   4: astore_3
    //   5: aload_3
    //   6: invokestatic getInstance : (Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;
    //   9: invokevirtual isEnabled : ()Z
    //   12: istore #4
    //   14: iload #4
    //   16: ifeq -> 32
    //   19: aload_0
    //   20: invokevirtual includeForAccessibility : ()Z
    //   23: ifeq -> 32
    //   26: iconst_1
    //   27: istore #5
    //   29: goto -> 35
    //   32: iconst_0
    //   33: istore #5
    //   35: aload_0
    //   36: getfield mViewFlags : I
    //   39: istore #6
    //   41: aload_0
    //   42: getfield mViewFlags : I
    //   45: iload_2
    //   46: iconst_m1
    //   47: ixor
    //   48: iand
    //   49: iload_1
    //   50: iload_2
    //   51: iand
    //   52: ior
    //   53: istore #7
    //   55: aload_0
    //   56: iload #7
    //   58: putfield mViewFlags : I
    //   61: iload #7
    //   63: iload #6
    //   65: ixor
    //   66: istore #8
    //   68: iload #8
    //   70: ifne -> 74
    //   73: return
    //   74: aload_0
    //   75: getfield mPrivateFlags : I
    //   78: istore #9
    //   80: iconst_0
    //   81: istore #10
    //   83: iconst_0
    //   84: istore #11
    //   86: iload #8
    //   88: istore_2
    //   89: iload #11
    //   91: istore #12
    //   93: iload #7
    //   95: bipush #16
    //   97: iand
    //   98: ifeq -> 165
    //   101: iload #8
    //   103: istore_2
    //   104: iload #11
    //   106: istore #12
    //   108: iload #8
    //   110: sipush #16401
    //   113: iand
    //   114: ifeq -> 165
    //   117: iload #7
    //   119: sipush #16384
    //   122: iand
    //   123: ifeq -> 131
    //   126: iconst_1
    //   127: istore_2
    //   128: goto -> 133
    //   131: iconst_0
    //   132: istore_2
    //   133: aload_0
    //   134: aload_0
    //   135: getfield mViewFlags : I
    //   138: bipush #-2
    //   140: iand
    //   141: iload_2
    //   142: ior
    //   143: putfield mViewFlags : I
    //   146: iload #6
    //   148: iconst_1
    //   149: iand
    //   150: iload_2
    //   151: iconst_1
    //   152: iand
    //   153: ixor
    //   154: istore #12
    //   156: iload #8
    //   158: bipush #-2
    //   160: iand
    //   161: iload #12
    //   163: ior
    //   164: istore_2
    //   165: iload #10
    //   167: istore #13
    //   169: iload_2
    //   170: iconst_1
    //   171: iand
    //   172: ifeq -> 311
    //   175: iload #10
    //   177: istore #13
    //   179: iload #9
    //   181: bipush #16
    //   183: iand
    //   184: ifeq -> 311
    //   187: iload #6
    //   189: iconst_1
    //   190: iand
    //   191: iconst_1
    //   192: if_icmpne -> 236
    //   195: iload #9
    //   197: iconst_2
    //   198: iand
    //   199: ifeq -> 236
    //   202: aload_0
    //   203: invokevirtual clearFocus : ()V
    //   206: aload_0
    //   207: getfield mParent : Landroid/view/ViewParent;
    //   210: astore_3
    //   211: iload #10
    //   213: istore #13
    //   215: aload_3
    //   216: instanceof android/view/ViewGroup
    //   219: ifeq -> 311
    //   222: aload_3
    //   223: checkcast android/view/ViewGroup
    //   226: invokevirtual clearFocusedInCluster : ()V
    //   229: iload #10
    //   231: istore #13
    //   233: goto -> 311
    //   236: iload #10
    //   238: istore #13
    //   240: iload #6
    //   242: iconst_1
    //   243: iand
    //   244: ifne -> 311
    //   247: iload #10
    //   249: istore #13
    //   251: iload #9
    //   253: iconst_2
    //   254: iand
    //   255: ifne -> 311
    //   258: iload #10
    //   260: istore #13
    //   262: aload_0
    //   263: getfield mParent : Landroid/view/ViewParent;
    //   266: ifnull -> 311
    //   269: aload_0
    //   270: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   273: astore_3
    //   274: getstatic android/view/View.sAutoFocusableOffUIThreadWontNotifyParents : Z
    //   277: ifeq -> 305
    //   280: iload #12
    //   282: ifeq -> 305
    //   285: aload_3
    //   286: ifnull -> 305
    //   289: aload_3
    //   290: getfield mThread : Ljava/lang/Thread;
    //   293: astore_3
    //   294: iload #10
    //   296: istore #13
    //   298: aload_3
    //   299: invokestatic currentThread : ()Ljava/lang/Thread;
    //   302: if_acmpne -> 311
    //   305: aload_0
    //   306: invokespecial canTakeFocus : ()Z
    //   309: istore #13
    //   311: iload_1
    //   312: bipush #12
    //   314: iand
    //   315: istore_1
    //   316: iload #13
    //   318: istore #10
    //   320: iload_1
    //   321: ifne -> 362
    //   324: iload #13
    //   326: istore #10
    //   328: iload_2
    //   329: bipush #12
    //   331: iand
    //   332: ifeq -> 362
    //   335: aload_0
    //   336: aload_0
    //   337: getfield mPrivateFlags : I
    //   340: bipush #32
    //   342: ior
    //   343: putfield mPrivateFlags : I
    //   346: aload_0
    //   347: iconst_1
    //   348: invokevirtual invalidate : (Z)V
    //   351: aload_0
    //   352: iconst_1
    //   353: invokevirtual needGlobalAttributesUpdate : (Z)V
    //   356: aload_0
    //   357: invokespecial hasSize : ()Z
    //   360: istore #10
    //   362: iload #10
    //   364: istore #13
    //   366: iload_2
    //   367: bipush #32
    //   369: iand
    //   370: ifeq -> 411
    //   373: aload_0
    //   374: getfield mViewFlags : I
    //   377: bipush #32
    //   379: iand
    //   380: ifne -> 392
    //   383: aload_0
    //   384: invokespecial canTakeFocus : ()Z
    //   387: istore #13
    //   389: goto -> 411
    //   392: iload #10
    //   394: istore #13
    //   396: aload_0
    //   397: invokevirtual isFocused : ()Z
    //   400: ifeq -> 411
    //   403: aload_0
    //   404: invokevirtual clearFocus : ()V
    //   407: iload #10
    //   409: istore #13
    //   411: iload #13
    //   413: ifeq -> 432
    //   416: aload_0
    //   417: getfield mParent : Landroid/view/ViewParent;
    //   420: astore_3
    //   421: aload_3
    //   422: ifnull -> 432
    //   425: aload_3
    //   426: aload_0
    //   427: invokeinterface focusableViewAvailable : (Landroid/view/View;)V
    //   432: iload_2
    //   433: bipush #8
    //   435: iand
    //   436: ifeq -> 543
    //   439: aload_0
    //   440: iconst_0
    //   441: invokevirtual needGlobalAttributesUpdate : (Z)V
    //   444: aload_0
    //   445: invokevirtual requestLayout : ()V
    //   448: aload_0
    //   449: getfield mViewFlags : I
    //   452: bipush #12
    //   454: iand
    //   455: bipush #8
    //   457: if_icmpne -> 529
    //   460: aload_0
    //   461: invokevirtual hasFocus : ()Z
    //   464: ifeq -> 490
    //   467: aload_0
    //   468: invokevirtual clearFocus : ()V
    //   471: aload_0
    //   472: getfield mParent : Landroid/view/ViewParent;
    //   475: astore_3
    //   476: aload_3
    //   477: instanceof android/view/ViewGroup
    //   480: ifeq -> 490
    //   483: aload_3
    //   484: checkcast android/view/ViewGroup
    //   487: invokevirtual clearFocusedInCluster : ()V
    //   490: aload_0
    //   491: invokevirtual clearAccessibilityFocus : ()V
    //   494: aload_0
    //   495: invokevirtual destroyDrawingCache : ()V
    //   498: aload_0
    //   499: getfield mParent : Landroid/view/ViewParent;
    //   502: astore_3
    //   503: aload_3
    //   504: instanceof android/view/View
    //   507: ifeq -> 518
    //   510: aload_3
    //   511: checkcast android/view/View
    //   514: iconst_1
    //   515: invokevirtual invalidate : (Z)V
    //   518: aload_0
    //   519: aload_0
    //   520: getfield mPrivateFlags : I
    //   523: bipush #32
    //   525: ior
    //   526: putfield mPrivateFlags : I
    //   529: aload_0
    //   530: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   533: astore_3
    //   534: aload_3
    //   535: ifnull -> 543
    //   538: aload_3
    //   539: iconst_1
    //   540: putfield mViewVisibilityChanged : Z
    //   543: iload_2
    //   544: iconst_4
    //   545: iand
    //   546: ifeq -> 632
    //   549: aload_0
    //   550: iconst_0
    //   551: invokevirtual needGlobalAttributesUpdate : (Z)V
    //   554: aload_0
    //   555: aload_0
    //   556: getfield mPrivateFlags : I
    //   559: bipush #32
    //   561: ior
    //   562: putfield mPrivateFlags : I
    //   565: aload_0
    //   566: getfield mViewFlags : I
    //   569: bipush #12
    //   571: iand
    //   572: iconst_4
    //   573: if_icmpne -> 618
    //   576: aload_0
    //   577: invokevirtual getRootView : ()Landroid/view/View;
    //   580: aload_0
    //   581: if_acmpeq -> 618
    //   584: aload_0
    //   585: invokevirtual hasFocus : ()Z
    //   588: ifeq -> 614
    //   591: aload_0
    //   592: invokevirtual clearFocus : ()V
    //   595: aload_0
    //   596: getfield mParent : Landroid/view/ViewParent;
    //   599: astore_3
    //   600: aload_3
    //   601: instanceof android/view/ViewGroup
    //   604: ifeq -> 614
    //   607: aload_3
    //   608: checkcast android/view/ViewGroup
    //   611: invokevirtual clearFocusedInCluster : ()V
    //   614: aload_0
    //   615: invokevirtual clearAccessibilityFocus : ()V
    //   618: aload_0
    //   619: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   622: astore_3
    //   623: aload_3
    //   624: ifnull -> 632
    //   627: aload_3
    //   628: iconst_1
    //   629: putfield mViewVisibilityChanged : Z
    //   632: iload_2
    //   633: bipush #12
    //   635: iand
    //   636: ifeq -> 779
    //   639: iload_1
    //   640: ifeq -> 654
    //   643: aload_0
    //   644: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   647: ifnull -> 654
    //   650: aload_0
    //   651: invokespecial cleanupDraw : ()V
    //   654: aload_0
    //   655: getfield mParent : Landroid/view/ViewParent;
    //   658: astore_3
    //   659: aload_3
    //   660: instanceof android/view/ViewGroup
    //   663: ifeq -> 689
    //   666: aload_3
    //   667: checkcast android/view/ViewGroup
    //   670: astore_3
    //   671: aload_3
    //   672: aload_0
    //   673: iload_2
    //   674: bipush #12
    //   676: iand
    //   677: iload_1
    //   678: invokevirtual onChildVisibilityChanged : (Landroid/view/View;II)V
    //   681: aload_3
    //   682: iconst_1
    //   683: invokevirtual invalidate : (Z)V
    //   686: goto -> 704
    //   689: aload_3
    //   690: ifnull -> 704
    //   693: aload_3
    //   694: aload_0
    //   695: aconst_null
    //   696: invokeinterface invalidateChild : (Landroid/view/View;Landroid/graphics/Rect;)V
    //   701: goto -> 704
    //   704: aload_0
    //   705: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   708: ifnull -> 779
    //   711: aload_0
    //   712: aload_0
    //   713: iload_1
    //   714: invokevirtual dispatchVisibilityChanged : (Landroid/view/View;I)V
    //   717: aload_0
    //   718: getfield mParent : Landroid/view/ViewParent;
    //   721: ifnull -> 775
    //   724: aload_0
    //   725: invokevirtual getWindowVisibility : ()I
    //   728: ifne -> 775
    //   731: aload_0
    //   732: getfield mParent : Landroid/view/ViewParent;
    //   735: astore_3
    //   736: aload_3
    //   737: instanceof android/view/ViewGroup
    //   740: ifeq -> 755
    //   743: aload_3
    //   744: checkcast android/view/ViewGroup
    //   747: astore_3
    //   748: aload_3
    //   749: invokevirtual isShown : ()Z
    //   752: ifeq -> 775
    //   755: iload_1
    //   756: ifne -> 765
    //   759: iconst_1
    //   760: istore #13
    //   762: goto -> 768
    //   765: iconst_0
    //   766: istore #13
    //   768: aload_0
    //   769: iload #13
    //   771: invokevirtual dispatchVisibilityAggregated : (Z)Z
    //   774: pop
    //   775: aload_0
    //   776: invokevirtual notifySubtreeAccessibilityStateChangedIfNeeded : ()V
    //   779: ldc_w 131072
    //   782: iload_2
    //   783: iand
    //   784: ifeq -> 791
    //   787: aload_0
    //   788: invokevirtual destroyDrawingCache : ()V
    //   791: ldc 32768
    //   793: iload_2
    //   794: iand
    //   795: ifeq -> 818
    //   798: aload_0
    //   799: invokevirtual destroyDrawingCache : ()V
    //   802: aload_0
    //   803: aload_0
    //   804: getfield mPrivateFlags : I
    //   807: ldc_w -32769
    //   810: iand
    //   811: putfield mPrivateFlags : I
    //   814: aload_0
    //   815: invokevirtual invalidateParentCaches : ()V
    //   818: ldc 1572864
    //   820: iload_2
    //   821: iand
    //   822: ifeq -> 841
    //   825: aload_0
    //   826: invokevirtual destroyDrawingCache : ()V
    //   829: aload_0
    //   830: aload_0
    //   831: getfield mPrivateFlags : I
    //   834: ldc_w -32769
    //   837: iand
    //   838: putfield mPrivateFlags : I
    //   841: iload_2
    //   842: sipush #128
    //   845: iand
    //   846: ifeq -> 944
    //   849: aload_0
    //   850: getfield mViewFlags : I
    //   853: sipush #128
    //   856: iand
    //   857: ifeq -> 923
    //   860: aload_0
    //   861: getfield mBackground : Landroid/graphics/drawable/Drawable;
    //   864: ifnonnull -> 908
    //   867: aload_0
    //   868: getfield mDefaultFocusHighlight : Landroid/graphics/drawable/Drawable;
    //   871: ifnonnull -> 908
    //   874: aload_0
    //   875: getfield mForegroundInfo : Landroid/view/View$ForegroundInfo;
    //   878: astore_3
    //   879: aload_3
    //   880: ifnull -> 893
    //   883: aload_3
    //   884: invokestatic access$1600 : (Landroid/view/View$ForegroundInfo;)Landroid/graphics/drawable/Drawable;
    //   887: ifnull -> 893
    //   890: goto -> 908
    //   893: aload_0
    //   894: aload_0
    //   895: getfield mPrivateFlags : I
    //   898: sipush #128
    //   901: ior
    //   902: putfield mPrivateFlags : I
    //   905: goto -> 935
    //   908: aload_0
    //   909: aload_0
    //   910: getfield mPrivateFlags : I
    //   913: sipush #-129
    //   916: iand
    //   917: putfield mPrivateFlags : I
    //   920: goto -> 935
    //   923: aload_0
    //   924: aload_0
    //   925: getfield mPrivateFlags : I
    //   928: sipush #-129
    //   931: iand
    //   932: putfield mPrivateFlags : I
    //   935: aload_0
    //   936: invokevirtual requestLayout : ()V
    //   939: aload_0
    //   940: iconst_1
    //   941: invokevirtual invalidate : (Z)V
    //   944: ldc_w 67108864
    //   947: iload_2
    //   948: iand
    //   949: ifeq -> 985
    //   952: aload_0
    //   953: getfield mParent : Landroid/view/ViewParent;
    //   956: ifnull -> 985
    //   959: aload_0
    //   960: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   963: astore_3
    //   964: aload_3
    //   965: ifnull -> 985
    //   968: aload_3
    //   969: getfield mRecomputeGlobalAttributes : Z
    //   972: ifne -> 985
    //   975: aload_0
    //   976: getfield mParent : Landroid/view/ViewParent;
    //   979: aload_0
    //   980: invokeinterface recomputeViewAttributes : (Landroid/view/View;)V
    //   985: iload #4
    //   987: ifeq -> 1079
    //   990: iload_2
    //   991: istore_1
    //   992: aload_0
    //   993: invokespecial isAccessibilityPane : ()Z
    //   996: ifeq -> 1004
    //   999: iload_2
    //   1000: bipush #-13
    //   1002: iand
    //   1003: istore_1
    //   1004: iload_1
    //   1005: iconst_1
    //   1006: iand
    //   1007: ifne -> 1058
    //   1010: iload_1
    //   1011: bipush #12
    //   1013: iand
    //   1014: ifne -> 1058
    //   1017: iload_1
    //   1018: sipush #16384
    //   1021: iand
    //   1022: ifne -> 1058
    //   1025: ldc_w 2097152
    //   1028: iload_1
    //   1029: iand
    //   1030: ifne -> 1058
    //   1033: ldc 8388608
    //   1035: iload_1
    //   1036: iand
    //   1037: ifeq -> 1043
    //   1040: goto -> 1058
    //   1043: iload_1
    //   1044: bipush #32
    //   1046: iand
    //   1047: ifeq -> 1079
    //   1050: aload_0
    //   1051: iconst_0
    //   1052: invokevirtual notifyViewAccessibilityStateChangedIfNeeded : (I)V
    //   1055: goto -> 1079
    //   1058: iload #5
    //   1060: aload_0
    //   1061: invokevirtual includeForAccessibility : ()Z
    //   1064: if_icmpeq -> 1074
    //   1067: aload_0
    //   1068: invokevirtual notifySubtreeAccessibilityStateChangedIfNeeded : ()V
    //   1071: goto -> 1079
    //   1074: aload_0
    //   1075: iconst_0
    //   1076: invokevirtual notifyViewAccessibilityStateChangedIfNeeded : (I)V
    //   1079: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #16231	-> 0
    //   #16232	-> 5
    //   #16233	-> 14
    //   #16235	-> 35
    //   #16236	-> 41
    //   #16238	-> 61
    //   #16239	-> 68
    //   #16240	-> 73
    //   #16242	-> 74
    //   #16243	-> 80
    //   #16246	-> 83
    //   #16247	-> 86
    //   #16251	-> 117
    //   #16252	-> 126
    //   #16254	-> 131
    //   #16256	-> 133
    //   #16257	-> 146
    //   #16258	-> 156
    //   #16262	-> 165
    //   #16263	-> 187
    //   #16266	-> 202
    //   #16267	-> 206
    //   #16268	-> 222
    //   #16270	-> 236
    //   #16276	-> 258
    //   #16277	-> 269
    //   #16278	-> 274
    //   #16281	-> 294
    //   #16282	-> 305
    //   #16288	-> 311
    //   #16289	-> 316
    //   #16290	-> 324
    //   #16296	-> 335
    //   #16297	-> 346
    //   #16299	-> 351
    //   #16304	-> 356
    //   #16308	-> 362
    //   #16309	-> 373
    //   #16313	-> 383
    //   #16315	-> 392
    //   #16319	-> 411
    //   #16320	-> 425
    //   #16324	-> 432
    //   #16325	-> 439
    //   #16326	-> 444
    //   #16328	-> 448
    //   #16329	-> 460
    //   #16330	-> 467
    //   #16331	-> 471
    //   #16332	-> 483
    //   #16335	-> 490
    //   #16336	-> 494
    //   #16337	-> 498
    //   #16339	-> 510
    //   #16343	-> 518
    //   #16345	-> 529
    //   #16346	-> 538
    //   #16351	-> 543
    //   #16352	-> 549
    //   #16357	-> 554
    //   #16359	-> 565
    //   #16361	-> 576
    //   #16362	-> 584
    //   #16363	-> 591
    //   #16364	-> 595
    //   #16365	-> 607
    //   #16368	-> 614
    //   #16371	-> 618
    //   #16372	-> 627
    //   #16376	-> 632
    //   #16378	-> 639
    //   #16379	-> 650
    //   #16382	-> 654
    //   #16383	-> 666
    //   #16384	-> 671
    //   #16386	-> 681
    //   #16387	-> 689
    //   #16388	-> 693
    //   #16387	-> 704
    //   #16391	-> 704
    //   #16392	-> 711
    //   #16399	-> 717
    //   #16400	-> 748
    //   #16401	-> 755
    //   #16403	-> 775
    //   #16407	-> 779
    //   #16408	-> 787
    //   #16411	-> 791
    //   #16412	-> 798
    //   #16413	-> 802
    //   #16414	-> 814
    //   #16417	-> 818
    //   #16418	-> 825
    //   #16419	-> 829
    //   #16422	-> 841
    //   #16423	-> 849
    //   #16424	-> 860
    //   #16426	-> 883
    //   #16429	-> 893
    //   #16427	-> 908
    //   #16432	-> 923
    //   #16434	-> 935
    //   #16435	-> 939
    //   #16438	-> 944
    //   #16439	-> 952
    //   #16440	-> 975
    //   #16444	-> 985
    //   #16447	-> 990
    //   #16448	-> 999
    //   #16450	-> 1004
    //   #16459	-> 1043
    //   #16460	-> 1050
    //   #16453	-> 1058
    //   #16454	-> 1067
    //   #16456	-> 1074
    //   #16464	-> 1079
  }
  
  public void bringToFront() {
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      viewParent.bringChildToFront(this); 
  }
  
  protected void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    notifySubtreeAccessibilityStateChangedIfNeeded();
    postSendViewScrolledAccessibilityEventCallback(paramInt1 - paramInt3, paramInt2 - paramInt4);
    this.mBackgroundSizeChanged = true;
    this.mDefaultFocusHighlightSizeChanged = true;
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null)
      ForegroundInfo.access$2202(foregroundInfo, true); 
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      attachInfo.mViewScrollChanged = true; 
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnScrollChangeListener != null)
      this.mListenerInfo.mOnScrollChangeListener.onScrollChange(this, paramInt1, paramInt2, paramInt3, paramInt4); 
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {}
  
  protected void dispatchDraw(Canvas paramCanvas) {}
  
  public final ViewParent getParent() {
    return this.mParent;
  }
  
  public void setScrollX(int paramInt) {
    scrollTo(paramInt, this.mScrollY);
  }
  
  public void setScrollY(int paramInt) {
    scrollTo(this.mScrollX, paramInt);
  }
  
  public final int getScrollX() {
    return this.mScrollX;
  }
  
  public final int getScrollY() {
    return this.mScrollY;
  }
  
  @ExportedProperty(category = "layout")
  public final int getWidth() {
    return this.mRight - this.mLeft;
  }
  
  @ExportedProperty(category = "layout")
  public final int getHeight() {
    return this.mBottom - this.mTop;
  }
  
  public void getDrawingRect(Rect paramRect) {
    paramRect.left = this.mScrollX;
    paramRect.top = this.mScrollY;
    paramRect.right = this.mScrollX + this.mRight - this.mLeft;
    paramRect.bottom = this.mScrollY + this.mBottom - this.mTop;
  }
  
  public final int getMeasuredWidth() {
    return this.mMeasuredWidth & 0xFFFFFF;
  }
  
  @ExportedProperty(category = "measurement", flagMapping = {@FlagToString(equals = 16777216, mask = -16777216, name = "MEASURED_STATE_TOO_SMALL")})
  public final int getMeasuredWidthAndState() {
    return this.mMeasuredWidth;
  }
  
  public final int getMeasuredHeight() {
    return this.mMeasuredHeight & 0xFFFFFF;
  }
  
  @ExportedProperty(category = "measurement", flagMapping = {@FlagToString(equals = 16777216, mask = -16777216, name = "MEASURED_STATE_TOO_SMALL")})
  public final int getMeasuredHeightAndState() {
    return this.mMeasuredHeight;
  }
  
  public final int getMeasuredState() {
    return this.mMeasuredWidth & 0xFF000000 | this.mMeasuredHeight >> 16 & 0xFFFFFF00;
  }
  
  public Matrix getMatrix() {
    ensureTransformationInfo();
    Matrix matrix = this.mTransformationInfo.mMatrix;
    this.mRenderNode.getMatrix(matrix);
    return matrix;
  }
  
  public final boolean hasIdentityMatrix() {
    return this.mRenderNode.hasIdentityMatrix();
  }
  
  void ensureTransformationInfo() {
    if (this.mTransformationInfo == null)
      this.mTransformationInfo = new TransformationInfo(); 
  }
  
  public final Matrix getInverseMatrix() {
    ensureTransformationInfo();
    if (this.mTransformationInfo.mInverseMatrix == null)
      TransformationInfo.access$2402(this.mTransformationInfo, new Matrix()); 
    Matrix matrix = this.mTransformationInfo.mInverseMatrix;
    this.mRenderNode.getInverseMatrix(matrix);
    return matrix;
  }
  
  public float getCameraDistance() {
    float f = (this.mResources.getDisplayMetrics()).densityDpi;
    return this.mRenderNode.getCameraDistance() * f;
  }
  
  public void setCameraDistance(float paramFloat) {
    float f = (this.mResources.getDisplayMetrics()).densityDpi;
    invalidateViewProperty(true, false);
    this.mRenderNode.setCameraDistance(Math.abs(paramFloat) / f);
    invalidateViewProperty(false, false);
    invalidateParentIfNeededAndWasQuickRejected();
  }
  
  @ExportedProperty(category = "drawing")
  public float getRotation() {
    return this.mRenderNode.getRotationZ();
  }
  
  public void setRotation(float paramFloat) {
    if (paramFloat != getRotation()) {
      invalidateViewProperty(true, false);
      this.mRenderNode.setRotationZ(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getRotationY() {
    return this.mRenderNode.getRotationY();
  }
  
  public void setRotationY(float paramFloat) {
    if (paramFloat != getRotationY()) {
      invalidateViewProperty(true, false);
      this.mRenderNode.setRotationY(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getRotationX() {
    return this.mRenderNode.getRotationX();
  }
  
  public void setRotationX(float paramFloat) {
    if (paramFloat != getRotationX()) {
      invalidateViewProperty(true, false);
      this.mRenderNode.setRotationX(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getScaleX() {
    return this.mRenderNode.getScaleX();
  }
  
  public void setScaleX(float paramFloat) {
    if (paramFloat != getScaleX()) {
      paramFloat = sanitizeFloatPropertyValue(paramFloat, "scaleX");
      invalidateViewProperty(true, false);
      this.mRenderNode.setScaleX(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getScaleY() {
    return this.mRenderNode.getScaleY();
  }
  
  public void setScaleY(float paramFloat) {
    if (paramFloat != getScaleY()) {
      paramFloat = sanitizeFloatPropertyValue(paramFloat, "scaleY");
      invalidateViewProperty(true, false);
      this.mRenderNode.setScaleY(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getPivotX() {
    return this.mRenderNode.getPivotX();
  }
  
  public void setPivotX(float paramFloat) {
    if (!this.mRenderNode.isPivotExplicitlySet() || paramFloat != getPivotX()) {
      invalidateViewProperty(true, false);
      this.mRenderNode.setPivotX(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getPivotY() {
    return this.mRenderNode.getPivotY();
  }
  
  public void setPivotY(float paramFloat) {
    if (!this.mRenderNode.isPivotExplicitlySet() || paramFloat != getPivotY()) {
      invalidateViewProperty(true, false);
      this.mRenderNode.setPivotY(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
    } 
  }
  
  public boolean isPivotSet() {
    return this.mRenderNode.isPivotExplicitlySet();
  }
  
  public void resetPivot() {
    if (this.mRenderNode.resetPivot())
      invalidateViewProperty(false, false); 
  }
  
  @ExportedProperty(category = "drawing")
  public float getAlpha() {
    float f;
    TransformationInfo transformationInfo = this.mTransformationInfo;
    if (transformationInfo != null) {
      f = transformationInfo.mAlpha;
    } else {
      f = 1.0F;
    } 
    return f;
  }
  
  public void forceHasOverlappingRendering(boolean paramBoolean) {
    int i = this.mPrivateFlags3 | 0x1000000;
    if (paramBoolean) {
      this.mPrivateFlags3 = i | 0x800000;
    } else {
      this.mPrivateFlags3 = i & 0xFF7FFFFF;
    } 
  }
  
  public final boolean getHasOverlappingRendering() {
    boolean bool;
    int i = this.mPrivateFlags3;
    if ((0x1000000 & i) != 0) {
      if ((i & 0x800000) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
    } else {
      bool = hasOverlappingRendering();
    } 
    return bool;
  }
  
  @ExportedProperty(category = "drawing")
  public boolean hasOverlappingRendering() {
    return true;
  }
  
  public void setAlpha(float paramFloat) {
    ensureTransformationInfo();
    if (this.mTransformationInfo.mAlpha != paramFloat) {
      setAlphaInternal(paramFloat);
      if (onSetAlpha((int)(255.0F * paramFloat))) {
        this.mPrivateFlags |= 0x40000;
        invalidateParentCaches();
        invalidate(true);
      } else {
        this.mPrivateFlags &= 0xFFFBFFFF;
        invalidateViewProperty(true, false);
        this.mRenderNode.setAlpha(getFinalAlpha());
      } 
    } 
  }
  
  boolean setAlphaNoInvalidation(float paramFloat) {
    ensureTransformationInfo();
    if (this.mTransformationInfo.mAlpha != paramFloat) {
      setAlphaInternal(paramFloat);
      boolean bool = onSetAlpha((int)(255.0F * paramFloat));
      if (bool) {
        this.mPrivateFlags |= 0x40000;
        return true;
      } 
      this.mPrivateFlags &= 0xFFFBFFFF;
      this.mRenderNode.setAlpha(getFinalAlpha());
    } 
    return false;
  }
  
  void setAlphaInternal(float paramFloat) {
    boolean bool2;
    float f = this.mTransformationInfo.mAlpha;
    TransformationInfo.access$2502(this.mTransformationInfo, paramFloat);
    boolean bool1 = true;
    if (paramFloat == 0.0F) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (f != 0.0F)
      bool1 = false; 
    if ((bool2 ^ bool1) != 0)
      notifySubtreeAccessibilityStateChangedIfNeeded(); 
  }
  
  public void setTransitionAlpha(float paramFloat) {
    ensureTransformationInfo();
    if (this.mTransformationInfo.mTransitionAlpha != paramFloat) {
      this.mTransformationInfo.mTransitionAlpha = paramFloat;
      this.mPrivateFlags &= 0xFFFBFFFF;
      invalidateViewProperty(true, false);
      this.mRenderNode.setAlpha(getFinalAlpha());
    } 
  }
  
  private float getFinalAlpha() {
    TransformationInfo transformationInfo = this.mTransformationInfo;
    if (transformationInfo != null)
      return transformationInfo.mAlpha * this.mTransformationInfo.mTransitionAlpha; 
    return 1.0F;
  }
  
  @ExportedProperty(category = "drawing")
  public float getTransitionAlpha() {
    float f;
    TransformationInfo transformationInfo = this.mTransformationInfo;
    if (transformationInfo != null) {
      f = transformationInfo.mTransitionAlpha;
    } else {
      f = 1.0F;
    } 
    return f;
  }
  
  public void setForceDarkAllowed(boolean paramBoolean) {
    if (this.mRenderNode.setForceDarkAllowed(paramBoolean))
      invalidate(); 
  }
  
  @ExportedProperty(category = "drawing")
  public boolean isForceDarkAllowed() {
    return this.mRenderNode.isForceDarkAllowed();
  }
  
  @CapturedViewProperty
  public final int getTop() {
    return this.mTop;
  }
  
  public final void setTop(int paramInt) {
    if (paramInt != this.mTop) {
      boolean bool = hasIdentityMatrix();
      if (bool) {
        if (this.mAttachInfo != null) {
          int n, m = this.mTop;
          if (paramInt < m) {
            n = paramInt;
            m = paramInt - m;
          } else {
            n = this.mTop;
            m = 0;
          } 
          invalidate(0, m, this.mRight - this.mLeft, this.mBottom - n);
        } 
      } else {
        invalidate(true);
      } 
      int k = this.mRight - this.mLeft;
      int i = this.mBottom, j = this.mTop;
      this.mTop = paramInt;
      this.mRenderNode.setTop(paramInt);
      sizeChange(k, this.mBottom - this.mTop, k, i - j);
      if (!bool) {
        this.mPrivateFlags |= 0x20;
        invalidate(true);
      } 
      this.mBackgroundSizeChanged = true;
      this.mDefaultFocusHighlightSizeChanged = true;
      ForegroundInfo foregroundInfo = this.mForegroundInfo;
      if (foregroundInfo != null)
        ForegroundInfo.access$2202(foregroundInfo, true); 
      invalidateParentIfNeeded();
    } 
  }
  
  @CapturedViewProperty
  public final int getBottom() {
    return this.mBottom;
  }
  
  public boolean isDirty() {
    boolean bool;
    if ((this.mPrivateFlags & 0x200000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void setBottom(int paramInt) {
    if (paramInt != this.mBottom) {
      boolean bool = hasIdentityMatrix();
      if (bool) {
        if (this.mAttachInfo != null) {
          int m;
          if (paramInt < this.mBottom) {
            m = this.mBottom;
          } else {
            m = paramInt;
          } 
          invalidate(0, 0, this.mRight - this.mLeft, m - this.mTop);
        } 
      } else {
        invalidate(true);
      } 
      int j = this.mRight - this.mLeft;
      int i = this.mBottom, k = this.mTop;
      this.mBottom = paramInt;
      this.mRenderNode.setBottom(paramInt);
      sizeChange(j, this.mBottom - this.mTop, j, i - k);
      if (!bool) {
        this.mPrivateFlags |= 0x20;
        invalidate(true);
      } 
      this.mBackgroundSizeChanged = true;
      this.mDefaultFocusHighlightSizeChanged = true;
      ForegroundInfo foregroundInfo = this.mForegroundInfo;
      if (foregroundInfo != null)
        ForegroundInfo.access$2202(foregroundInfo, true); 
      invalidateParentIfNeeded();
    } 
  }
  
  @CapturedViewProperty
  public final int getLeft() {
    return this.mLeft;
  }
  
  public final void setLeft(int paramInt) {
    if (paramInt != this.mLeft) {
      boolean bool = hasIdentityMatrix();
      if (bool) {
        if (this.mAttachInfo != null) {
          int n, m = this.mLeft;
          if (paramInt < m) {
            n = paramInt;
            m = paramInt - m;
          } else {
            n = this.mLeft;
            m = 0;
          } 
          invalidate(m, 0, this.mRight - n, this.mBottom - this.mTop);
        } 
      } else {
        invalidate(true);
      } 
      int j = this.mRight, i = this.mLeft;
      int k = this.mBottom - this.mTop;
      this.mLeft = paramInt;
      this.mRenderNode.setLeft(paramInt);
      sizeChange(this.mRight - this.mLeft, k, j - i, k);
      if (!bool) {
        this.mPrivateFlags |= 0x20;
        invalidate(true);
      } 
      this.mBackgroundSizeChanged = true;
      this.mDefaultFocusHighlightSizeChanged = true;
      ForegroundInfo foregroundInfo = this.mForegroundInfo;
      if (foregroundInfo != null)
        ForegroundInfo.access$2202(foregroundInfo, true); 
      invalidateParentIfNeeded();
    } 
  }
  
  @CapturedViewProperty
  public final int getRight() {
    return this.mRight;
  }
  
  public final void setRight(int paramInt) {
    if (paramInt != this.mRight) {
      boolean bool = hasIdentityMatrix();
      if (bool) {
        if (this.mAttachInfo != null) {
          int m;
          if (paramInt < this.mRight) {
            m = this.mRight;
          } else {
            m = paramInt;
          } 
          invalidate(0, 0, m - this.mLeft, this.mBottom - this.mTop);
        } 
      } else {
        invalidate(true);
      } 
      int j = this.mRight, k = this.mLeft;
      int i = this.mBottom - this.mTop;
      this.mRight = paramInt;
      this.mRenderNode.setRight(paramInt);
      sizeChange(this.mRight - this.mLeft, i, j - k, i);
      if (!bool) {
        this.mPrivateFlags |= 0x20;
        invalidate(true);
      } 
      this.mBackgroundSizeChanged = true;
      this.mDefaultFocusHighlightSizeChanged = true;
      ForegroundInfo foregroundInfo = this.mForegroundInfo;
      if (foregroundInfo != null)
        ForegroundInfo.access$2202(foregroundInfo, true); 
      invalidateParentIfNeeded();
    } 
  }
  
  private static float sanitizeFloatPropertyValue(float paramFloat, String paramString) {
    return sanitizeFloatPropertyValue(paramFloat, paramString, -3.4028235E38F, Float.MAX_VALUE);
  }
  
  private static float sanitizeFloatPropertyValue(float paramFloat1, String paramString, float paramFloat2, float paramFloat3) {
    if (paramFloat1 >= paramFloat2 && paramFloat1 <= paramFloat3)
      return paramFloat1; 
    if (paramFloat1 < paramFloat2 || paramFloat1 == Float.NEGATIVE_INFINITY) {
      if (!sThrowOnInvalidFloatProperties)
        return paramFloat2; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Cannot set '");
      stringBuilder1.append(paramString);
      stringBuilder1.append("' to ");
      stringBuilder1.append(paramFloat1);
      stringBuilder1.append(", the value must be >= ");
      stringBuilder1.append(paramFloat2);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    if (paramFloat1 > paramFloat3 || paramFloat1 == Float.POSITIVE_INFINITY) {
      if (!sThrowOnInvalidFloatProperties)
        return paramFloat3; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Cannot set '");
      stringBuilder1.append(paramString);
      stringBuilder1.append("' to ");
      stringBuilder1.append(paramFloat1);
      stringBuilder1.append(", the value must be <= ");
      stringBuilder1.append(paramFloat3);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    if (Float.isNaN(paramFloat1)) {
      if (!sThrowOnInvalidFloatProperties)
        return 0.0F; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Cannot set '");
      stringBuilder1.append(paramString);
      stringBuilder1.append("' to Float.NaN");
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("How do you get here?? ");
    stringBuilder.append(paramFloat1);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  @ExportedProperty(category = "drawing")
  public float getX() {
    return this.mLeft + getTranslationX();
  }
  
  public void setX(float paramFloat) {
    setTranslationX(paramFloat - this.mLeft);
  }
  
  @ExportedProperty(category = "drawing")
  public float getY() {
    return this.mTop + getTranslationY();
  }
  
  public void setY(float paramFloat) {
    setTranslationY(paramFloat - this.mTop);
  }
  
  @ExportedProperty(category = "drawing")
  public float getZ() {
    return getElevation() + getTranslationZ();
  }
  
  public void setZ(float paramFloat) {
    setTranslationZ(paramFloat - getElevation());
  }
  
  @ExportedProperty(category = "drawing")
  public float getElevation() {
    return this.mRenderNode.getElevation();
  }
  
  public void setElevation(float paramFloat) {
    if (paramFloat != getElevation()) {
      paramFloat = sanitizeFloatPropertyValue(paramFloat, "elevation");
      invalidateViewProperty(true, false);
      this.mRenderNode.setElevation(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getTranslationX() {
    return this.mRenderNode.getTranslationX();
  }
  
  public void setTranslationX(float paramFloat) {
    if (paramFloat != getTranslationX()) {
      invalidateViewProperty(true, false);
      this.mRenderNode.setTranslationX(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getTranslationY() {
    return this.mRenderNode.getTranslationY();
  }
  
  public void setTranslationY(float paramFloat) {
    if (paramFloat != getTranslationY()) {
      invalidateViewProperty(true, false);
      this.mRenderNode.setTranslationY(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public float getTranslationZ() {
    return this.mRenderNode.getTranslationZ();
  }
  
  public void setTranslationZ(float paramFloat) {
    if (paramFloat != getTranslationZ()) {
      paramFloat = sanitizeFloatPropertyValue(paramFloat, "translationZ");
      invalidateViewProperty(true, false);
      this.mRenderNode.setTranslationZ(paramFloat);
      invalidateViewProperty(false, true);
      invalidateParentIfNeededAndWasQuickRejected();
    } 
  }
  
  public void setAnimationMatrix(Matrix paramMatrix) {
    invalidateViewProperty(true, false);
    this.mRenderNode.setAnimationMatrix(paramMatrix);
    invalidateViewProperty(false, true);
    invalidateParentIfNeededAndWasQuickRejected();
  }
  
  public Matrix getAnimationMatrix() {
    return this.mRenderNode.getAnimationMatrix();
  }
  
  public StateListAnimator getStateListAnimator() {
    return this.mStateListAnimator;
  }
  
  public void setStateListAnimator(StateListAnimator paramStateListAnimator) {
    StateListAnimator stateListAnimator = this.mStateListAnimator;
    if (stateListAnimator == paramStateListAnimator)
      return; 
    if (stateListAnimator != null)
      stateListAnimator.setTarget(null); 
    this.mStateListAnimator = paramStateListAnimator;
    if (paramStateListAnimator != null) {
      paramStateListAnimator.setTarget(this);
      if (isAttachedToWindow())
        paramStateListAnimator.setState(getDrawableState()); 
    } 
  }
  
  public final boolean getClipToOutline() {
    return this.mRenderNode.getClipToOutline();
  }
  
  public void setClipToOutline(boolean paramBoolean) {
    damageInParent();
    if (getClipToOutline() != paramBoolean)
      this.mRenderNode.setClipToOutline(paramBoolean); 
  }
  
  private void setOutlineProviderFromAttribute(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            setOutlineProvider(ViewOutlineProvider.PADDED_BOUNDS); 
        } else {
          setOutlineProvider(ViewOutlineProvider.BOUNDS);
        } 
      } else {
        setOutlineProvider((ViewOutlineProvider)null);
      } 
    } else {
      setOutlineProvider(ViewOutlineProvider.BACKGROUND);
    } 
  }
  
  public void setOutlineProvider(ViewOutlineProvider paramViewOutlineProvider) {
    this.mOutlineProvider = paramViewOutlineProvider;
    invalidateOutline();
  }
  
  public ViewOutlineProvider getOutlineProvider() {
    return this.mOutlineProvider;
  }
  
  public void invalidateOutline() {
    rebuildOutline();
    notifySubtreeAccessibilityStateChangedIfNeeded();
    invalidateViewProperty(false, false);
  }
  
  private void rebuildOutline() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return; 
    if (this.mOutlineProvider == null) {
      this.mRenderNode.setOutline(null);
    } else {
      Outline outline = attachInfo.mTmpOutline;
      outline.setEmpty();
      outline.setAlpha(1.0F);
      this.mOutlineProvider.getOutline(this, outline);
      this.mRenderNode.setOutline(outline);
    } 
  }
  
  @ExportedProperty(category = "drawing")
  public boolean hasShadow() {
    return this.mRenderNode.hasShadow();
  }
  
  public void setOutlineSpotShadowColor(int paramInt) {
    if (this.mRenderNode.setSpotShadowColor(paramInt))
      invalidateViewProperty(true, true); 
  }
  
  public int getOutlineSpotShadowColor() {
    return this.mRenderNode.getSpotShadowColor();
  }
  
  public void setOutlineAmbientShadowColor(int paramInt) {
    if (this.mRenderNode.setAmbientShadowColor(paramInt))
      invalidateViewProperty(true, true); 
  }
  
  public int getOutlineAmbientShadowColor() {
    return this.mRenderNode.getAmbientShadowColor();
  }
  
  public void setRevealClip(boolean paramBoolean, float paramFloat1, float paramFloat2, float paramFloat3) {
    this.mRenderNode.setRevealClip(paramBoolean, paramFloat1, paramFloat2, paramFloat3);
    invalidateViewProperty(false, false);
  }
  
  public void getHitRect(Rect paramRect) {
    if (!hasIdentityMatrix()) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo == null) {
        paramRect.set(this.mLeft, this.mTop, this.mRight, this.mBottom);
        return;
      } 
      RectF rectF = attachInfo.mTmpTransformRect;
      rectF.set(0.0F, 0.0F, getWidth(), getHeight());
      getMatrix().mapRect(rectF);
      paramRect.set((int)rectF.left + this.mLeft, (int)rectF.top + this.mTop, (int)rectF.right + this.mLeft, (int)rectF.bottom + this.mTop);
      return;
    } 
    paramRect.set(this.mLeft, this.mTop, this.mRight, this.mBottom);
  }
  
  final boolean pointInView(float paramFloat1, float paramFloat2) {
    return pointInView(paramFloat1, paramFloat2, 0.0F);
  }
  
  public boolean pointInView(float paramFloat1, float paramFloat2, float paramFloat3) {
    boolean bool;
    if (paramFloat1 >= -paramFloat3 && paramFloat2 >= -paramFloat3 && paramFloat1 < (this.mRight - this.mLeft) + paramFloat3 && paramFloat2 < (this.mBottom - this.mTop) + paramFloat3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void getFocusedRect(Rect paramRect) {
    getDrawingRect(paramRect);
  }
  
  public boolean getGlobalVisibleRect(Rect paramRect, Point paramPoint) {
    int i = this.mRight - this.mLeft;
    int j = this.mBottom - this.mTop;
    boolean bool = false;
    if (i > 0 && j > 0) {
      paramRect.set(0, 0, i, j);
      if (paramPoint != null)
        paramPoint.set(-this.mScrollX, -this.mScrollY); 
      ViewParent viewParent = this.mParent;
      if (viewParent == null || viewParent.getChildVisibleRect(this, paramRect, paramPoint))
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public final boolean getGlobalVisibleRect(Rect paramRect) {
    return getGlobalVisibleRect(paramRect, (Point)null);
  }
  
  public final boolean getLocalVisibleRect(Rect paramRect) {
    Point point;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      point = attachInfo.mPoint;
    } else {
      point = new Point();
    } 
    if (getGlobalVisibleRect(paramRect, point)) {
      paramRect.offset(-point.x, -point.y);
      return true;
    } 
    return false;
  }
  
  public void offsetTopAndBottom(int paramInt) {
    if (paramInt != 0) {
      boolean bool = hasIdentityMatrix();
      if (bool) {
        if (isHardwareAccelerated()) {
          invalidateViewProperty(false, false);
        } else {
          ViewParent viewParent = this.mParent;
          if (viewParent != null) {
            AttachInfo attachInfo = this.mAttachInfo;
            if (attachInfo != null) {
              int i, j;
              boolean bool1;
              Rect rect = attachInfo.mTmpInvalRect;
              if (paramInt < 0) {
                i = this.mTop + paramInt;
                j = this.mBottom;
                bool1 = paramInt;
              } else {
                i = this.mTop;
                j = this.mBottom + paramInt;
                bool1 = false;
              } 
              rect.set(0, bool1, this.mRight - this.mLeft, j - i);
              viewParent.invalidateChild(this, rect);
            } 
          } 
        } 
      } else {
        invalidateViewProperty(false, false);
      } 
      this.mTop += paramInt;
      this.mBottom += paramInt;
      this.mRenderNode.offsetTopAndBottom(paramInt);
      if (isHardwareAccelerated()) {
        invalidateViewProperty(false, false);
        invalidateParentIfNeededAndWasQuickRejected();
      } else {
        if (!bool)
          invalidateViewProperty(false, true); 
        invalidateParentIfNeeded();
      } 
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  public void offsetLeftAndRight(int paramInt) {
    if (paramInt != 0) {
      boolean bool = hasIdentityMatrix();
      if (bool) {
        if (isHardwareAccelerated()) {
          invalidateViewProperty(false, false);
        } else {
          ViewParent viewParent = this.mParent;
          if (viewParent != null) {
            AttachInfo attachInfo = this.mAttachInfo;
            if (attachInfo != null) {
              int i, j;
              Rect rect = attachInfo.mTmpInvalRect;
              if (paramInt < 0) {
                i = this.mLeft + paramInt;
                j = this.mRight;
              } else {
                i = this.mLeft;
                j = this.mRight + paramInt;
              } 
              rect.set(0, 0, j - i, this.mBottom - this.mTop);
              viewParent.invalidateChild(this, rect);
            } 
          } 
        } 
      } else {
        invalidateViewProperty(false, false);
      } 
      this.mLeft += paramInt;
      this.mRight += paramInt;
      this.mRenderNode.offsetLeftAndRight(paramInt);
      if (isHardwareAccelerated()) {
        invalidateViewProperty(false, false);
        invalidateParentIfNeededAndWasQuickRejected();
      } else {
        if (!bool)
          invalidateViewProperty(false, true); 
        invalidateParentIfNeeded();
      } 
      notifySubtreeAccessibilityStateChangedIfNeeded();
    } 
  }
  
  @ExportedProperty(deepExport = true, prefix = "layout_")
  public ViewGroup.LayoutParams getLayoutParams() {
    return this.mLayoutParams;
  }
  
  public void setLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    if (paramLayoutParams != null) {
      this.mLayoutParams = paramLayoutParams;
      resolveLayoutParams();
      ViewParent viewParent = this.mParent;
      if (viewParent instanceof ViewGroup)
        ((ViewGroup)viewParent).onSetLayoutParams(this, paramLayoutParams); 
      requestLayout();
      return;
    } 
    throw new NullPointerException("Layout parameters cannot be null");
  }
  
  public void resolveLayoutParams() {
    ViewGroup.LayoutParams layoutParams = this.mLayoutParams;
    if (layoutParams != null)
      layoutParams.resolveLayoutDirection(getLayoutDirection()); 
  }
  
  public void scrollTo(int paramInt1, int paramInt2) {
    if (this.mScrollX != paramInt1 || this.mScrollY != paramInt2) {
      int i = this.mScrollX;
      int j = this.mScrollY;
      this.mScrollX = paramInt1;
      this.mScrollY = paramInt2;
      invalidateParentCaches();
      onScrollChanged(this.mScrollX, this.mScrollY, i, j);
      if (!awakenScrollBars())
        postInvalidateOnAnimation(); 
    } 
  }
  
  public void scrollBy(int paramInt1, int paramInt2) {
    scrollTo(this.mScrollX + paramInt1, this.mScrollY + paramInt2);
  }
  
  protected boolean awakenScrollBars() {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    null = true;
    if (scrollabilityCache != null) {
      int i = scrollabilityCache.scrollBarDefaultDelayBeforeFade;
      if (awakenScrollBars(i, true))
        return null; 
    } 
    return false;
  }
  
  private boolean initialAwakenScrollBars() {
    boolean bool = this.mViewHooks.needHook();
    boolean bool1 = false;
    if (bool)
      return false; 
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null) {
      int i = scrollabilityCache.scrollBarDefaultDelayBeforeFade;
      if (awakenScrollBars(i * 4, true))
        bool1 = true; 
    } 
    return bool1;
  }
  
  protected boolean awakenScrollBars(int paramInt) {
    return awakenScrollBars(paramInt, true);
  }
  
  protected boolean awakenScrollBars(int paramInt, boolean paramBoolean) {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache == null || !scrollabilityCache.fadeScrollBars)
      return false; 
    if (scrollabilityCache.scrollBar == null) {
      scrollabilityCache.scrollBar = new ScrollBarDrawable(this.mViewHooks.getScrollBarEffect());
      scrollabilityCache.scrollBar.setState(getDrawableState());
      scrollabilityCache.scrollBar.setCallback(this);
    } 
    if (isHorizontalScrollBarEnabled() || isVerticalScrollBarEnabled()) {
      if (paramBoolean)
        postInvalidateOnAnimation(); 
      int i = paramInt;
      if (scrollabilityCache.state == 0)
        i = Math.max(750, paramInt); 
      long l = AnimationUtils.currentAnimationTimeMillis() + i;
      scrollabilityCache.fadeStartTime = l;
      scrollabilityCache.state = 1;
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null) {
        attachInfo.mHandler.removeCallbacks(scrollabilityCache);
        if (!this.mViewHooks.getScrollBarEffect().isTouchPressed())
          this.mAttachInfo.mHandler.postAtTime(scrollabilityCache, l); 
      } 
      return true;
    } 
    return false;
  }
  
  private boolean skipInvalidate() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mViewFlags : I
    //   4: bipush #12
    //   6: iand
    //   7: ifeq -> 47
    //   10: aload_0
    //   11: getfield mCurrentAnimation : Landroid/view/animation/Animation;
    //   14: ifnonnull -> 47
    //   17: aload_0
    //   18: getfield mParent : Landroid/view/ViewParent;
    //   21: astore_1
    //   22: aload_1
    //   23: instanceof android/view/ViewGroup
    //   26: ifeq -> 42
    //   29: aload_1
    //   30: checkcast android/view/ViewGroup
    //   33: astore_1
    //   34: aload_1
    //   35: aload_0
    //   36: invokevirtual isViewTransitioning : (Landroid/view/View;)Z
    //   39: ifne -> 47
    //   42: iconst_1
    //   43: istore_2
    //   44: goto -> 49
    //   47: iconst_0
    //   48: istore_2
    //   49: iload_2
    //   50: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #18624	-> 0
    //   #18626	-> 34
    //   #18624	-> 49
  }
  
  @Deprecated
  public void invalidate(Rect paramRect) {
    int i = this.mScrollX;
    int j = this.mScrollY;
    invalidateInternal(paramRect.left - i, paramRect.top - j, paramRect.right - i, paramRect.bottom - j, true, false);
  }
  
  @Deprecated
  public void invalidate(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = this.mScrollX;
    int j = this.mScrollY;
    invalidateInternal(paramInt1 - i, paramInt2 - j, paramInt3 - i, paramInt4 - j, true, false);
  }
  
  public void invalidate() {
    invalidate(true);
  }
  
  public void invalidate(boolean paramBoolean) {
    invalidateInternal(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop, paramBoolean, true);
  }
  
  void invalidateInternal(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mGhostView : Landroid/view/GhostView;
    //   4: astore #7
    //   6: aload #7
    //   8: ifnull -> 18
    //   11: aload #7
    //   13: iconst_1
    //   14: invokevirtual invalidate : (Z)V
    //   17: return
    //   18: aload_0
    //   19: invokespecial skipInvalidate : ()Z
    //   22: ifeq -> 26
    //   25: return
    //   26: aload_0
    //   27: aload_0
    //   28: getfield mPrivateFlags4 : I
    //   31: sipush #-193
    //   34: iand
    //   35: putfield mPrivateFlags4 : I
    //   38: aload_0
    //   39: iconst_0
    //   40: putfield mContentCaptureSessionCached : Z
    //   43: aload_0
    //   44: getfield mPrivateFlags : I
    //   47: istore #8
    //   49: iload #8
    //   51: bipush #48
    //   53: iand
    //   54: bipush #48
    //   56: if_icmpeq -> 104
    //   59: iload #5
    //   61: ifeq -> 74
    //   64: iload #8
    //   66: ldc 32768
    //   68: iand
    //   69: ldc 32768
    //   71: if_icmpeq -> 104
    //   74: aload_0
    //   75: getfield mPrivateFlags : I
    //   78: ldc_w -2147483648
    //   81: iand
    //   82: ldc_w -2147483648
    //   85: if_icmpne -> 104
    //   88: iload #6
    //   90: ifeq -> 301
    //   93: aload_0
    //   94: invokevirtual isOpaque : ()Z
    //   97: aload_0
    //   98: getfield mLastIsOpaque : Z
    //   101: if_icmpeq -> 301
    //   104: iload #6
    //   106: ifeq -> 128
    //   109: aload_0
    //   110: aload_0
    //   111: invokevirtual isOpaque : ()Z
    //   114: putfield mLastIsOpaque : Z
    //   117: aload_0
    //   118: aload_0
    //   119: getfield mPrivateFlags : I
    //   122: bipush #-33
    //   124: iand
    //   125: putfield mPrivateFlags : I
    //   128: aload_0
    //   129: getfield mPrivateFlags : I
    //   132: ldc_w 2097152
    //   135: ior
    //   136: istore #8
    //   138: aload_0
    //   139: iload #8
    //   141: putfield mPrivateFlags : I
    //   144: iload #5
    //   146: ifeq -> 173
    //   149: iload #8
    //   151: ldc_w -2147483648
    //   154: ior
    //   155: istore #8
    //   157: aload_0
    //   158: iload #8
    //   160: putfield mPrivateFlags : I
    //   163: aload_0
    //   164: iload #8
    //   166: ldc_w -32769
    //   169: iand
    //   170: putfield mPrivateFlags : I
    //   173: aload_0
    //   174: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   177: astore #9
    //   179: aload_0
    //   180: getfield mParent : Landroid/view/ViewParent;
    //   183: astore #7
    //   185: aload #7
    //   187: ifnull -> 266
    //   190: aload #9
    //   192: ifnull -> 266
    //   195: iload_1
    //   196: iload_3
    //   197: if_icmpge -> 266
    //   200: iload_2
    //   201: iload #4
    //   203: if_icmpge -> 266
    //   206: aload #9
    //   208: getfield mTmpInvalRect : Landroid/graphics/Rect;
    //   211: astore #10
    //   213: aload #10
    //   215: iload_1
    //   216: iload_2
    //   217: iload_3
    //   218: iload #4
    //   220: invokevirtual set : (IIII)V
    //   223: aload_0
    //   224: getfield mIgnoreOnDescendantInvalidated : Z
    //   227: ifeq -> 256
    //   230: aload #7
    //   232: instanceof android/view/ViewGroup
    //   235: ifeq -> 256
    //   238: aload #7
    //   240: checkcast android/view/ViewGroup
    //   243: astore #9
    //   245: aload #9
    //   247: ifnull -> 256
    //   250: aload #9
    //   252: iconst_1
    //   253: putfield mIgnoreOnDescendantInvalidated : Z
    //   256: aload #7
    //   258: aload_0
    //   259: aload #10
    //   261: invokeinterface invalidateChild : (Landroid/view/View;Landroid/graphics/Rect;)V
    //   266: aload_0
    //   267: getfield mBackground : Landroid/graphics/drawable/Drawable;
    //   270: astore #7
    //   272: aload #7
    //   274: ifnull -> 301
    //   277: aload #7
    //   279: invokevirtual isProjected : ()Z
    //   282: ifeq -> 301
    //   285: aload_0
    //   286: invokespecial getProjectionReceiver : ()Landroid/view/View;
    //   289: astore #7
    //   291: aload #7
    //   293: ifnull -> 301
    //   296: aload #7
    //   298: invokevirtual damageInParent : ()V
    //   301: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #18713	-> 0
    //   #18714	-> 11
    //   #18715	-> 17
    //   #18718	-> 18
    //   #18719	-> 25
    //   #18723	-> 26
    //   #18724	-> 38
    //   #18726	-> 43
    //   #18729	-> 93
    //   #18730	-> 104
    //   #18731	-> 109
    //   #18732	-> 117
    //   #18735	-> 128
    //   #18737	-> 144
    //   #18738	-> 149
    //   #18739	-> 163
    //   #18743	-> 173
    //   #18744	-> 179
    //   #18745	-> 185
    //   #18746	-> 206
    //   #18747	-> 213
    //   #18750	-> 223
    //   #18751	-> 230
    //   #18752	-> 238
    //   #18753	-> 245
    //   #18754	-> 250
    //   #18759	-> 256
    //   #18763	-> 266
    //   #18764	-> 285
    //   #18765	-> 291
    //   #18766	-> 296
    //   #18770	-> 301
  }
  
  private View getProjectionReceiver() {
    ViewParent viewParent = getParent();
    while (viewParent != null && viewParent instanceof View) {
      View view = (View)viewParent;
      if (view.isProjectionReceiver())
        return view; 
      viewParent = viewParent.getParent();
    } 
    return null;
  }
  
  private boolean isProjectionReceiver() {
    boolean bool;
    if (this.mBackground != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void invalidateViewProperty(boolean paramBoolean1, boolean paramBoolean2) {
    if (isHardwareAccelerated()) {
      RenderNode renderNode = this.mRenderNode;
      if (!renderNode.hasDisplayList() || (this.mPrivateFlags & 0x40) != 0) {
        if (paramBoolean1)
          invalidateParentCaches(); 
        if (paramBoolean2)
          this.mPrivateFlags |= 0x20; 
        invalidate(false);
        return;
      } 
      damageInParent();
      return;
    } 
    if (paramBoolean1)
      invalidateParentCaches(); 
    if (paramBoolean2)
      this.mPrivateFlags |= 0x20; 
    invalidate(false);
  }
  
  protected void damageInParent() {
    ViewParent viewParent = this.mParent;
    if (viewParent != null && this.mAttachInfo != null)
      viewParent.onDescendantInvalidated(this, this); 
  }
  
  protected void invalidateParentCaches() {
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof View) {
      View view = (View)viewParent;
      view.mPrivateFlags |= Integer.MIN_VALUE;
    } 
  }
  
  protected void invalidateParentIfNeeded() {
    if (isHardwareAccelerated()) {
      ViewParent viewParent = this.mParent;
      if (viewParent instanceof View)
        ((View)viewParent).invalidate(true); 
    } 
  }
  
  protected void invalidateParentIfNeededAndWasQuickRejected() {
    if ((this.mPrivateFlags2 & 0x10000000) != 0)
      invalidateParentIfNeeded(); 
  }
  
  @ExportedProperty(category = "drawing")
  public boolean isOpaque() {
    boolean bool;
    if ((this.mPrivateFlags & 0x1800000) == 25165824 && getFinalAlpha() >= 1.0F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void computeOpaqueFlags() {
    Drawable drawable = this.mBackground;
    if (drawable != null && drawable.getOpacity() == -1) {
      this.mPrivateFlags |= 0x800000;
    } else {
      this.mPrivateFlags &= 0xFF7FFFFF;
    } 
    int i = this.mViewFlags;
    if (((i & 0x200) == 0 && (i & 0x100) == 0) || (i & 0x3000000) == 0 || (0x3000000 & i) == 33554432) {
      this.mPrivateFlags |= 0x1000000;
      return;
    } 
    this.mPrivateFlags &= 0xFEFFFFFF;
  }
  
  protected boolean hasOpaqueScrollbars() {
    boolean bool;
    if ((this.mPrivateFlags & 0x1000000) == 16777216) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Handler getHandler() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      return attachInfo.mHandler; 
    return null;
  }
  
  private HandlerActionQueue getRunQueue() {
    if (this.mRunQueue == null)
      this.mRunQueue = new HandlerActionQueue(); 
    return this.mRunQueue;
  }
  
  public ViewRootImpl getViewRootImpl() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      return attachInfo.mViewRootImpl; 
    return null;
  }
  
  public ThreadedRenderer getThreadedRenderer() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      ThreadedRenderer threadedRenderer = attachInfo.mThreadedRenderer;
    } else {
      attachInfo = null;
    } 
    return (ThreadedRenderer)attachInfo;
  }
  
  public boolean post(Runnable paramRunnable) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      return attachInfo.mHandler.post(paramRunnable); 
    getRunQueue().post(paramRunnable);
    return true;
  }
  
  public boolean postDelayed(Runnable paramRunnable, long paramLong) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      return attachInfo.mHandler.postDelayed(paramRunnable, paramLong); 
    getRunQueue().postDelayed(paramRunnable, paramLong);
    return true;
  }
  
  public void postOnAnimation(Runnable paramRunnable) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      attachInfo.mViewRootImpl.mChoreographer.postCallback(1, paramRunnable, null);
    } else {
      getRunQueue().post(paramRunnable);
    } 
  }
  
  public void postOnAnimationDelayed(Runnable paramRunnable, long paramLong) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      attachInfo.mViewRootImpl.mChoreographer.postCallbackDelayed(1, paramRunnable, null, paramLong);
    } else {
      getRunQueue().postDelayed(paramRunnable, paramLong);
    } 
  }
  
  public boolean removeCallbacks(Runnable paramRunnable) {
    if (paramRunnable != null) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null) {
        attachInfo.mHandler.removeCallbacks(paramRunnable);
        attachInfo.mViewRootImpl.mChoreographer.removeCallbacks(1, paramRunnable, null);
      } 
      getRunQueue().removeCallbacks(paramRunnable);
    } 
    return true;
  }
  
  public void postInvalidate() {
    postInvalidateDelayed(0L);
  }
  
  public void postInvalidate(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    postInvalidateDelayed(0L, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void postInvalidateDelayed(long paramLong) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      attachInfo.mViewRootImpl.dispatchInvalidateDelayed(this, paramLong); 
  }
  
  public void postInvalidateDelayed(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      AttachInfo.InvalidateInfo invalidateInfo = AttachInfo.InvalidateInfo.obtain();
      invalidateInfo.target = this;
      invalidateInfo.left = paramInt1;
      invalidateInfo.top = paramInt2;
      invalidateInfo.right = paramInt3;
      invalidateInfo.bottom = paramInt4;
      attachInfo.mViewRootImpl.dispatchInvalidateRectDelayed(invalidateInfo, paramLong);
    } 
  }
  
  public void postInvalidateOnAnimation() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      attachInfo.mViewRootImpl.dispatchInvalidateOnAnimation(this); 
  }
  
  public void postInvalidateOnAnimation(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      AttachInfo.InvalidateInfo invalidateInfo = AttachInfo.InvalidateInfo.obtain();
      invalidateInfo.target = this;
      invalidateInfo.left = paramInt1;
      invalidateInfo.top = paramInt2;
      invalidateInfo.right = paramInt3;
      invalidateInfo.bottom = paramInt4;
      attachInfo.mViewRootImpl.dispatchInvalidateRectOnAnimation(invalidateInfo);
    } 
  }
  
  private void postSendViewScrolledAccessibilityEventCallback(int paramInt1, int paramInt2) {
    if (AccessibilityManager.getInstance(this.mContext).isEnabled()) {
      AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(4096);
      accessibilityEvent.setScrollDeltaX(paramInt1);
      accessibilityEvent.setScrollDeltaY(paramInt2);
      sendAccessibilityEventUnchecked(accessibilityEvent);
    } 
  }
  
  public void computeScroll() {}
  
  public boolean isHorizontalFadingEdgeEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x1000) == 4096) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setHorizontalFadingEdgeEnabled(boolean paramBoolean) {
    if (isHorizontalFadingEdgeEnabled() != paramBoolean) {
      if (paramBoolean)
        initScrollCache(); 
      this.mViewFlags ^= 0x1000;
    } 
  }
  
  public boolean isVerticalFadingEdgeEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x2000) == 8192) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setVerticalFadingEdgeEnabled(boolean paramBoolean) {
    if (isVerticalFadingEdgeEnabled() != paramBoolean) {
      if (paramBoolean)
        initScrollCache(); 
      this.mViewFlags ^= 0x2000;
    } 
  }
  
  public int getFadingEdge() {
    return this.mViewFlags & 0x3000;
  }
  
  public int getFadingEdgeLength() {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null && (this.mViewFlags & 0x3000) != 0)
      return scrollabilityCache.fadingEdgeLength; 
    return 0;
  }
  
  protected float getTopFadingEdgeStrength() {
    float f;
    if (computeVerticalScrollOffset() > 0) {
      f = 1.0F;
    } else {
      f = 0.0F;
    } 
    return f;
  }
  
  protected float getBottomFadingEdgeStrength() {
    float f;
    int i = computeVerticalScrollOffset(), j = computeVerticalScrollExtent();
    if (i + j < computeVerticalScrollRange()) {
      f = 1.0F;
    } else {
      f = 0.0F;
    } 
    return f;
  }
  
  protected float getLeftFadingEdgeStrength() {
    float f;
    if (computeHorizontalScrollOffset() > 0) {
      f = 1.0F;
    } else {
      f = 0.0F;
    } 
    return f;
  }
  
  protected float getRightFadingEdgeStrength() {
    float f;
    int i = computeHorizontalScrollOffset(), j = computeHorizontalScrollExtent();
    if (i + j < computeHorizontalScrollRange()) {
      f = 1.0F;
    } else {
      f = 0.0F;
    } 
    return f;
  }
  
  public boolean isHorizontalScrollBarEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x100) == 256) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setHorizontalScrollBarEnabled(boolean paramBoolean) {
    if (isHorizontalScrollBarEnabled() != paramBoolean) {
      this.mViewFlags ^= 0x100;
      computeOpaqueFlags();
      resolvePadding();
    } 
  }
  
  public boolean isVerticalScrollBarEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x200) == 512) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setVerticalScrollBarEnabled(boolean paramBoolean) {
    if (isVerticalScrollBarEnabled() != paramBoolean) {
      this.mViewFlags ^= 0x200;
      computeOpaqueFlags();
      resolvePadding();
    } 
  }
  
  protected void recomputePadding() {
    internalSetPadding(this.mUserPaddingLeft, this.mPaddingTop, this.mUserPaddingRight, this.mUserPaddingBottom);
  }
  
  public void setScrollbarFadingEnabled(boolean paramBoolean) {
    initScrollCache();
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    scrollabilityCache.fadeScrollBars = paramBoolean;
    if (paramBoolean) {
      scrollabilityCache.state = 0;
    } else {
      scrollabilityCache.state = 1;
    } 
  }
  
  public boolean isScrollbarFadingEnabled() {
    boolean bool;
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null && scrollabilityCache.fadeScrollBars) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getScrollBarDefaultDelayBeforeFade() {
    int i;
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache == null) {
      i = ViewConfiguration.getScrollDefaultDelay();
    } else {
      i = scrollabilityCache.scrollBarDefaultDelayBeforeFade;
    } 
    return i;
  }
  
  public void setScrollBarDefaultDelayBeforeFade(int paramInt) {
    (getScrollCache()).scrollBarDefaultDelayBeforeFade = paramInt;
  }
  
  public int getScrollBarFadeDuration() {
    int i;
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache == null) {
      i = ViewConfiguration.getScrollBarFadeDuration();
    } else {
      i = scrollabilityCache.scrollBarFadeDuration;
    } 
    return i;
  }
  
  public void setScrollBarFadeDuration(int paramInt) {
    (getScrollCache()).scrollBarFadeDuration = paramInt;
  }
  
  public int getScrollBarSize() {
    int i;
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache == null) {
      i = ViewConfiguration.get(this.mContext).getScaledScrollBarSize();
    } else {
      i = scrollabilityCache.scrollBarSize;
    } 
    return i;
  }
  
  public void setScrollBarSize(int paramInt) {
    (getScrollCache()).scrollBarSize = paramInt;
  }
  
  public void setScrollBarStyle(int paramInt) {
    int i = this.mViewFlags;
    if (paramInt != (i & 0x3000000)) {
      this.mViewFlags = i & 0xFCFFFFFF | 0x3000000 & paramInt;
      computeOpaqueFlags();
      resolvePadding();
    } 
  }
  
  @ExportedProperty(mapping = {@IntToString(from = 0, to = "INSIDE_OVERLAY"), @IntToString(from = 16777216, to = "INSIDE_INSET"), @IntToString(from = 33554432, to = "OUTSIDE_OVERLAY"), @IntToString(from = 50331648, to = "OUTSIDE_INSET")})
  public int getScrollBarStyle() {
    return this.mViewFlags & 0x3000000;
  }
  
  protected int computeHorizontalScrollRange() {
    return getWidth();
  }
  
  protected int computeHorizontalScrollOffset() {
    return this.mScrollX;
  }
  
  protected int computeHorizontalScrollExtent() {
    return getWidth();
  }
  
  protected int computeVerticalScrollRange() {
    return getHeight();
  }
  
  protected int computeVerticalScrollOffset() {
    return this.mScrollY;
  }
  
  protected int computeVerticalScrollExtent() {
    return getHeight();
  }
  
  public boolean canScrollHorizontally(int paramInt) {
    int i = computeHorizontalScrollOffset();
    int j = computeHorizontalScrollRange() - computeHorizontalScrollExtent();
    boolean bool1 = false, bool2 = false;
    if (j == 0)
      return false; 
    if (paramInt < 0) {
      bool1 = bool2;
      if (i > 0)
        bool1 = true; 
      return bool1;
    } 
    if (i < j - 1)
      bool1 = true; 
    return bool1;
  }
  
  public boolean canScrollVertically(int paramInt) {
    int i = computeVerticalScrollOffset();
    int j = computeVerticalScrollRange() - computeVerticalScrollExtent();
    boolean bool1 = false, bool2 = false;
    if (j == 0)
      return false; 
    if (paramInt < 0) {
      bool1 = bool2;
      if (i > 0)
        bool1 = true; 
      return bool1;
    } 
    if (i < j - 1)
      bool1 = true; 
    return bool1;
  }
  
  void getScrollIndicatorBounds(Rect paramRect) {
    paramRect.left = this.mScrollX;
    paramRect.right = this.mScrollX + this.mRight - this.mLeft;
    paramRect.top = this.mScrollY;
    paramRect.bottom = this.mScrollY + this.mBottom - this.mTop;
  }
  
  private void onDrawScrollIndicators(Canvas paramCanvas) {
    char c;
    if ((this.mPrivateFlags3 & 0x3F00) == 0)
      return; 
    Drawable drawable = this.mScrollIndicatorDrawable;
    if (drawable == null)
      return; 
    int i = drawable.getIntrinsicHeight();
    int j = drawable.getIntrinsicWidth();
    Rect rect = this.mAttachInfo.mTmpInvalRect;
    getScrollIndicatorBounds(rect);
    if ((this.mPrivateFlags3 & 0x100) != 0) {
      boolean bool = canScrollVertically(-1);
      if (bool) {
        drawable.setBounds(rect.left, rect.top, rect.right, rect.top + i);
        drawable.draw(paramCanvas);
      } 
    } 
    if ((this.mPrivateFlags3 & 0x200) != 0) {
      boolean bool = canScrollVertically(1);
      if (bool) {
        drawable.setBounds(rect.left, rect.bottom - i, rect.right, rect.bottom);
        drawable.draw(paramCanvas);
      } 
    } 
    if (getLayoutDirection() == 1) {
      i = 8192;
      c = 'က';
    } else {
      i = 4096;
      c = ' ';
    } 
    if ((this.mPrivateFlags3 & (i | 0x400)) != 0) {
      boolean bool = canScrollHorizontally(-1);
      if (bool) {
        drawable.setBounds(rect.left, rect.top, rect.left + j, rect.bottom);
        drawable.draw(paramCanvas);
      } 
    } 
    if ((this.mPrivateFlags3 & (c | 0x800)) != 0) {
      boolean bool = canScrollHorizontally(1);
      if (bool) {
        drawable.setBounds(rect.right - j, rect.top, rect.right, rect.bottom);
        drawable.draw(paramCanvas);
      } 
    } 
  }
  
  private void getHorizontalScrollBarBounds(Rect paramRect1, Rect paramRect2) {
    int k;
    if (paramRect1 == null)
      paramRect1 = paramRect2; 
    if (paramRect1 == null)
      return; 
    int i = this.mViewFlags, j = 0;
    if ((i & 0x2000000) == 0) {
      i = -1;
    } else {
      i = 0;
    } 
    if (isVerticalScrollBarEnabled() && !isVerticalScrollBarHidden()) {
      k = 1;
    } else {
      k = 0;
    } 
    int m = getHorizontalScrollbarHeight();
    if (k) {
      k = getVerticalScrollbarWidth();
    } else {
      k = j;
    } 
    int n = this.mRight, i1 = this.mLeft;
    j = this.mBottom - this.mTop;
    paramRect1.top = this.mScrollY + j - m - (this.mUserPaddingBottom & i);
    paramRect1.left = this.mScrollX + (this.mPaddingLeft & i);
    paramRect1.right = this.mScrollX + n - i1 - (this.mUserPaddingRight & i) - k;
    paramRect1.bottom = paramRect1.top + m;
    if (paramRect2 == null)
      return; 
    if (paramRect2 != paramRect1)
      paramRect2.set(paramRect1); 
    i = this.mScrollCache.scrollBarMinTouchTarget;
    if (paramRect2.height() < i) {
      k = (i - paramRect2.height()) / 2;
      paramRect2.bottom = Math.min(paramRect2.bottom + k, this.mScrollY + j);
      paramRect2.top = paramRect2.bottom - i;
    } 
    if (paramRect2.width() < i) {
      k = (i - paramRect2.width()) / 2;
      paramRect2.left -= k;
      paramRect2.right = paramRect2.left + i;
    } 
  }
  
  private void getVerticalScrollBarBounds(Rect paramRect1, Rect paramRect2) {
    if (this.mRoundScrollbarRenderer == null) {
      getStraightVerticalScrollBarBounds(paramRect1, paramRect2);
    } else {
      if (paramRect1 == null)
        paramRect1 = paramRect2; 
      getRoundVerticalScrollBarBounds(paramRect1);
    } 
  }
  
  private void getRoundVerticalScrollBarBounds(Rect paramRect) {
    int i = this.mRight, j = this.mLeft;
    int k = this.mBottom, m = this.mTop;
    paramRect.left = this.mScrollX;
    paramRect.top = this.mScrollY;
    paramRect.right = paramRect.left + i - j;
    paramRect.bottom = this.mScrollY + k - m;
  }
  
  private void getStraightVerticalScrollBarBounds(Rect paramRect1, Rect paramRect2) {
    if (paramRect1 == null)
      paramRect1 = paramRect2; 
    if (paramRect1 == null)
      return; 
    if ((this.mViewFlags & 0x2000000) == 0) {
      i = -1;
    } else {
      i = 0;
    } 
    int j = getVerticalScrollbarWidth();
    int k = this.mVerticalScrollbarPosition;
    int m = k;
    if (k == 0)
      if (isLayoutRtl()) {
        m = 1;
      } else {
        m = 2;
      }  
    k = this.mRight - this.mLeft;
    int n = this.mBottom, i1 = this.mTop;
    if (m != 1) {
      paramRect1.left = this.mScrollX + k - j - (this.mUserPaddingRight & i);
    } else {
      paramRect1.left = this.mScrollX + (this.mUserPaddingLeft & i);
    } 
    paramRect1.top = this.mScrollY + (this.mPaddingTop & i);
    paramRect1.right = paramRect1.left + j;
    paramRect1.bottom = this.mScrollY + n - i1 - (this.mUserPaddingBottom & i);
    if (paramRect2 == null)
      return; 
    if (paramRect2 != paramRect1)
      paramRect2.set(paramRect1); 
    int i = this.mScrollCache.scrollBarMinTouchTarget;
    if (paramRect2.width() < i) {
      j = (i - paramRect2.width()) / 2;
      if (m == 2) {
        paramRect2.right = Math.min(paramRect2.right + j, this.mScrollX + k);
        paramRect2.left = paramRect2.right - i;
      } else {
        paramRect2.left = Math.max(paramRect2.left + j, this.mScrollX);
        paramRect2.right = paramRect2.left + i;
      } 
    } 
    if (paramRect2.height() < i) {
      m = (i - paramRect2.height()) / 2;
      paramRect2.top -= m;
      paramRect2.bottom = paramRect2.top + i;
    } 
  }
  
  protected final void onDrawScrollBars(Canvas paramCanvas) {
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    if (scrollabilityCache != null) {
      ScrollBarDrawable scrollBarDrawable;
      int j, i = scrollabilityCache.state;
      if (i == 0)
        return; 
      if (i == 2) {
        if (scrollabilityCache.interpolatorValues == null)
          scrollabilityCache.interpolatorValues = new float[1]; 
        float[] arrayOfFloat = scrollabilityCache.interpolatorValues;
        if (scrollabilityCache.scrollBarInterpolator.timeToValues(arrayOfFloat) == Interpolator.Result.FREEZE_END) {
          scrollabilityCache.state = 0;
        } else {
          scrollabilityCache.scrollBar.mutate().setAlpha(Math.round(arrayOfFloat[0]));
        } 
        i = 1;
      } else {
        scrollabilityCache.scrollBar.mutate().setAlpha(255);
        i = 0;
      } 
      boolean bool = isHorizontalScrollBarEnabled();
      if (isVerticalScrollBarEnabled() && !isVerticalScrollBarHidden()) {
        j = 1;
      } else {
        j = 0;
      } 
      if (this.mRoundScrollbarRenderer != null) {
        if (j) {
          Rect rect = scrollabilityCache.mScrollBarBounds;
          getVerticalScrollBarBounds(rect, (Rect)null);
          RoundScrollbarRenderer roundScrollbarRenderer = this.mRoundScrollbarRenderer;
          scrollBarDrawable = scrollabilityCache.scrollBar;
          float f = scrollBarDrawable.getAlpha() / 255.0F;
          roundScrollbarRenderer.drawRoundScrollbars(paramCanvas, f, rect);
          if (i != 0)
            invalidate(); 
        } 
      } else if (j || bool) {
        ScrollBarDrawable scrollBarDrawable1 = ((ScrollabilityCache)scrollBarDrawable).scrollBar;
        if (bool) {
          int k = computeHorizontalScrollRange();
          int m = computeHorizontalScrollOffset();
          int n = computeHorizontalScrollExtent();
          scrollBarDrawable1.setParameters(k, m, n, false);
          Rect rect = ((ScrollabilityCache)scrollBarDrawable).mScrollBarBounds;
          getHorizontalScrollBarBounds(rect, (Rect)null);
          onDrawHorizontalScrollBar(paramCanvas, scrollBarDrawable1, rect.left, rect.top, rect.right, rect.bottom);
          if (i != 0)
            invalidate(rect); 
        } 
        if (j) {
          j = computeVerticalScrollRange();
          int m = computeVerticalScrollOffset();
          int k = computeVerticalScrollExtent();
          scrollBarDrawable1.setParameters(j, m, k, true);
          Rect rect = ((ScrollabilityCache)scrollBarDrawable).mScrollBarBounds;
          getVerticalScrollBarBounds(rect, (Rect)null);
          onDrawVerticalScrollBar(paramCanvas, scrollBarDrawable1, rect.left, rect.top, rect.right, rect.bottom);
          if (i != 0)
            invalidate(rect); 
        } 
      } 
    } 
  }
  
  protected boolean isVerticalScrollBarHidden() {
    return false;
  }
  
  protected void onDrawHorizontalScrollBar(Canvas paramCanvas, Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramDrawable.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
    paramDrawable.draw(paramCanvas);
  }
  
  protected void onDrawVerticalScrollBar(Canvas paramCanvas, Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramDrawable.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
    paramDrawable.draw(paramCanvas);
  }
  
  protected void onDraw(Canvas paramCanvas) {}
  
  void assignParent(ViewParent paramViewParent) {
    if (paramViewParent == null && isDebugVersion()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mParent:");
      stringBuilder.append(this.mParent);
      stringBuilder.append(" parent:");
      stringBuilder.append(paramViewParent);
      stringBuilder.append(" ");
      stringBuilder.append(Debug.getCallers(8));
      this.mAssignNullStack = stringBuilder.toString();
    } 
    if (this.mParent == null) {
      this.mParent = paramViewParent;
    } else {
      if (paramViewParent == null) {
        this.mParent = null;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("view ");
      stringBuilder.append(this);
      stringBuilder.append(" being added, but it already has a parent");
      throw new RuntimeException(stringBuilder.toString());
    } 
  }
  
  boolean isDebugVersion() {
    if (mDebugVersion == -1)
      mDebugVersion = "1".equals(SystemProperties.get("SPECIAL_OPPO_CONFIG")); 
    int i = mDebugVersion;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  protected void onAttachedToWindow() {
    if ((this.mPrivateFlags & 0x200) != 0)
      this.mParent.requestTransparentRegion(this); 
    this.mPrivateFlags3 &= 0xFFFFFFFB;
    jumpDrawablesToCurrentState();
    AccessibilityNodeIdManager.getInstance().registerViewWithId(this, getAccessibilityViewId());
    resetSubtreeAccessibilityStateChanged();
    rebuildOutline();
    if (isFocused())
      notifyFocusChangeToImeFocusController(true); 
  }
  
  public boolean resolveRtlPropertiesIfNeeded() {
    if (!needRtlPropertiesResolution())
      return false; 
    if (!isLayoutDirectionResolved()) {
      resolveLayoutDirection();
      resolveLayoutParams();
    } 
    if (!isTextDirectionResolved())
      resolveTextDirection(); 
    if (!isTextAlignmentResolved())
      resolveTextAlignment(); 
    if (!areDrawablesResolved())
      resolveDrawables(); 
    if (!isPaddingResolved())
      resolvePadding(); 
    onRtlPropertiesChanged(getLayoutDirection());
    return true;
  }
  
  public void resetRtlProperties() {
    resetResolvedLayoutDirection();
    resetResolvedTextDirection();
    resetResolvedTextAlignment();
    resetResolvedPadding();
    resetResolvedDrawables();
  }
  
  void dispatchScreenStateChanged(int paramInt) {
    onScreenStateChanged(paramInt);
  }
  
  public void onScreenStateChanged(int paramInt) {}
  
  void dispatchMovedToDisplay(Display paramDisplay, Configuration paramConfiguration) {
    this.mAttachInfo.mDisplay = paramDisplay;
    this.mAttachInfo.mDisplayState = paramDisplay.getState();
    onMovedToDisplay(paramDisplay.getDisplayId(), paramConfiguration);
  }
  
  public void onMovedToDisplay(int paramInt, Configuration paramConfiguration) {}
  
  private boolean hasRtlSupport() {
    return ((IOplusTextViewRTLUtilForUG)OplusFeatureCache.getOrCreate(IOplusTextViewRTLUtilForUG.DEFAULT, new Object[0])).hasRtlSupportForView(this.mContext);
  }
  
  private boolean isRtlCompatibilityMode() {
    int i = (getContext().getApplicationInfo()).targetSdkVersion;
    return (i < 17 || !hasRtlSupport());
  }
  
  private boolean needRtlPropertiesResolution() {
    boolean bool;
    if ((this.mPrivateFlags2 & 0x60010220) != 1610678816) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onRtlPropertiesChanged(int paramInt) {}
  
  public boolean resolveLayoutDirection() {
    this.mPrivateFlags2 &= 0xFFFFFFCF;
    if (hasRtlSupport()) {
      int i = this.mPrivateFlags2, j = (i & 0xC) >> 2;
      if (j != 1) {
        if (j != 2) {
          if (j == 3)
            if (1 == TextUtils.getLayoutDirectionFromLocale(Locale.getDefault()))
              this.mPrivateFlags2 |= 0x10;  
        } else {
          if (!canResolveLayoutDirection())
            return false; 
          try {
            if (!this.mParent.isLayoutDirectionResolved())
              return false; 
            if (this.mParent.getLayoutDirection() == 1)
              this.mPrivateFlags2 |= 0x10; 
          } catch (AbstractMethodError abstractMethodError) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(this.mParent.getClass().getSimpleName());
            stringBuilder.append(" does not fully implement ViewParent");
            Log.e("View", stringBuilder.toString(), abstractMethodError);
          } 
        } 
      } else {
        this.mPrivateFlags2 = i | 0x10;
      } 
    } 
    this.mPrivateFlags2 |= 0x20;
    return true;
  }
  
  public boolean canResolveLayoutDirection() {
    if (getRawLayoutDirection() != 2)
      return true; 
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      try {
        return viewParent.canResolveLayoutDirection();
      } catch (AbstractMethodError abstractMethodError) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mParent.getClass().getSimpleName());
        stringBuilder.append(" does not fully implement ViewParent");
        Log.e("View", stringBuilder.toString(), abstractMethodError);
      }  
    return false;
  }
  
  public void resetResolvedLayoutDirection() {
    this.mPrivateFlags2 &= 0xFFFFFFCF;
  }
  
  public boolean isLayoutDirectionInherited() {
    boolean bool;
    if (getRawLayoutDirection() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isLayoutDirectionResolved() {
    boolean bool;
    if ((this.mPrivateFlags2 & 0x20) == 32) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean isPaddingResolved() {
    boolean bool;
    if ((this.mPrivateFlags2 & 0x20000000) == 536870912) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void resolvePadding() {
    int i = getLayoutDirection();
    if (!isRtlCompatibilityMode()) {
      if (this.mBackground != null && (!this.mLeftPaddingDefined || !this.mRightPaddingDefined)) {
        Rect rect1 = sThreadLocal.get();
        Rect rect2 = rect1;
        if (rect1 == null) {
          rect2 = new Rect();
          sThreadLocal.set(rect2);
        } 
        this.mBackground.getPadding(rect2);
        if (!this.mLeftPaddingDefined)
          this.mUserPaddingLeftInitial = rect2.left; 
        if (!this.mRightPaddingDefined)
          this.mUserPaddingRightInitial = rect2.right; 
      } 
      if (i != 1) {
        int k = this.mUserPaddingStart;
        if (k != Integer.MIN_VALUE) {
          this.mUserPaddingLeft = k;
        } else {
          this.mUserPaddingLeft = this.mUserPaddingLeftInitial;
        } 
        k = this.mUserPaddingEnd;
        if (k != Integer.MIN_VALUE) {
          this.mUserPaddingRight = k;
        } else {
          this.mUserPaddingRight = this.mUserPaddingRightInitial;
        } 
      } else {
        int k = this.mUserPaddingStart;
        if (k != Integer.MIN_VALUE) {
          this.mUserPaddingRight = k;
        } else {
          this.mUserPaddingRight = this.mUserPaddingRightInitial;
        } 
        k = this.mUserPaddingEnd;
        if (k != Integer.MIN_VALUE) {
          this.mUserPaddingLeft = k;
        } else {
          this.mUserPaddingLeft = this.mUserPaddingLeftInitial;
        } 
      } 
      int j = this.mUserPaddingBottom;
      if (j < 0)
        j = this.mPaddingBottom; 
      this.mUserPaddingBottom = j;
    } 
    internalSetPadding(this.mUserPaddingLeft, this.mPaddingTop, this.mUserPaddingRight, this.mUserPaddingBottom);
    onRtlPropertiesChanged(i);
    this.mPrivateFlags2 |= 0x20000000;
  }
  
  public void resetResolvedPadding() {
    resetResolvedPaddingInternal();
  }
  
  void resetResolvedPaddingInternal() {
    this.mPrivateFlags2 &= 0xDFFFFFFF;
  }
  
  protected void onDetachedFromWindow() {}
  
  protected void onDetachedFromWindowInternal() {
    this.mPrivateFlags &= 0xFBFFFFFF;
    int i = this.mPrivateFlags3 & 0xFFFFFFFB;
    this.mPrivateFlags3 = i & 0xFDFFFFFF;
    removeUnsetPressCallback();
    removeLongPressCallback();
    removePerformClickCallback();
    clearAccessibilityThrottles();
    stopNestedScroll();
    jumpDrawablesToCurrentState();
    destroyDrawingCache();
    cleanupDraw();
    this.mCurrentAnimation = null;
    if ((this.mViewFlags & 0x40000000) == 1073741824)
      hideTooltip(); 
    AccessibilityNodeIdManager.getInstance().unregisterViewWithId(getAccessibilityViewId());
  }
  
  private void cleanupDraw() {
    resetDisplayList();
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      attachInfo.mViewRootImpl.cancelInvalidate(this); 
  }
  
  void invalidateInheritedLayoutMode(int paramInt) {}
  
  protected int getWindowAttachCount() {
    return this.mWindowAttachCount;
  }
  
  public IBinder getWindowToken() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      IBinder iBinder = attachInfo.mWindowToken;
    } else {
      attachInfo = null;
    } 
    return (IBinder)attachInfo;
  }
  
  public WindowId getWindowId() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return null; 
    if (attachInfo.mWindowId == null)
      try {
        attachInfo.mIWindowId = attachInfo.mSession.getWindowId(attachInfo.mWindowToken);
        if (attachInfo.mIWindowId != null) {
          WindowId windowId = new WindowId();
          this(attachInfo.mIWindowId);
          attachInfo.mWindowId = windowId;
        } 
      } catch (RemoteException remoteException) {} 
    return attachInfo.mWindowId;
  }
  
  public IBinder getApplicationWindowToken() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      IBinder iBinder1 = attachInfo.mPanelParentWindowToken;
      IBinder iBinder2 = iBinder1;
      if (iBinder1 == null)
        iBinder2 = attachInfo.mWindowToken; 
      return iBinder2;
    } 
    return null;
  }
  
  public Display getDisplay() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      Display display = attachInfo.mDisplay;
    } else {
      attachInfo = null;
    } 
    return (Display)attachInfo;
  }
  
  IWindowSession getWindowSession() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      IWindowSession iWindowSession = attachInfo.mSession;
    } else {
      attachInfo = null;
    } 
    return (IWindowSession)attachInfo;
  }
  
  protected IWindow getWindow() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      IWindow iWindow = attachInfo.mWindow;
    } else {
      attachInfo = null;
    } 
    return (IWindow)attachInfo;
  }
  
  int combineVisibility(int paramInt1, int paramInt2) {
    return Math.max(paramInt1, paramInt2);
  }
  
  void dispatchAttachedToWindow(AttachInfo paramAttachInfo, int paramInt) {
    CopyOnWriteArrayList copyOnWriteArrayList;
    this.mAttachInfo = paramAttachInfo;
    ViewOverlay viewOverlay = this.mOverlay;
    if (viewOverlay != null)
      viewOverlay.getOverlayView().dispatchAttachedToWindow(paramAttachInfo, paramInt); 
    this.mWindowAttachCount++;
    this.mPrivateFlags |= 0x400;
    ViewTreeObserver viewTreeObserver = this.mFloatingTreeObserver;
    viewOverlay = null;
    if (viewTreeObserver != null) {
      paramAttachInfo.mTreeObserver.merge(this.mFloatingTreeObserver);
      this.mFloatingTreeObserver = null;
    } 
    registerPendingFrameMetricsObservers();
    if ((this.mPrivateFlags & 0x80000) != 0) {
      this.mAttachInfo.mScrollContainers.add(this);
      this.mPrivateFlags |= 0x100000;
    } 
    HandlerActionQueue handlerActionQueue = this.mRunQueue;
    if (handlerActionQueue != null) {
      handlerActionQueue.executeActions(paramAttachInfo.mHandler);
      this.mRunQueue = null;
    } 
    performCollectViewAttributes(this.mAttachInfo, paramInt);
    onAttachedToWindow();
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null)
      copyOnWriteArrayList = listenerInfo.mOnAttachStateChangeListeners; 
    if (copyOnWriteArrayList != null && copyOnWriteArrayList.size() > 0)
      for (OnAttachStateChangeListener onAttachStateChangeListener : copyOnWriteArrayList)
        onAttachStateChangeListener.onViewAttachedToWindow(this);  
    int i = paramAttachInfo.mWindowVisibility;
    if (i != 8) {
      onWindowVisibilityChanged(i);
      if (isShown()) {
        boolean bool;
        if (i == 0) {
          bool = true;
        } else {
          bool = false;
        } 
        onVisibilityAggregated(bool);
      } 
    } 
    onVisibilityChanged(this, paramInt);
    if ((this.mPrivateFlags & 0x400) != 0)
      refreshDrawableState(); 
    needGlobalAttributesUpdate(false);
    notifyEnterOrExitForAutoFillIfNeeded(true);
    notifyAppearedOrDisappearedForContentCaptureIfNeeded(true);
  }
  
  void dispatchDetachedFromWindow() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      int i = attachInfo.mWindowVisibility;
      if (i != 8) {
        onWindowVisibilityChanged(8);
        if (isShown())
          onVisibilityAggregated(false); 
      } 
    } 
    onDetachedFromWindow();
    onDetachedFromWindowInternal();
    if (attachInfo != null)
      attachInfo.mViewRootImpl.getImeFocusController().onViewDetachedFromWindow(this); 
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null) {
      CopyOnWriteArrayList copyOnWriteArrayList = listenerInfo.mOnAttachStateChangeListeners;
    } else {
      listenerInfo = null;
    } 
    if (listenerInfo != null && listenerInfo.size() > 0)
      for (OnAttachStateChangeListener onAttachStateChangeListener : listenerInfo)
        onAttachStateChangeListener.onViewDetachedFromWindow(this);  
    if ((this.mPrivateFlags & 0x100000) != 0) {
      this.mAttachInfo.mScrollContainers.remove(this);
      this.mPrivateFlags &= 0xFFEFFFFF;
    } 
    this.mAttachInfo = null;
    ViewOverlay viewOverlay = this.mOverlay;
    if (viewOverlay != null)
      viewOverlay.getOverlayView().dispatchDetachedFromWindow(); 
    notifyEnterOrExitForAutoFillIfNeeded(false);
    notifyAppearedOrDisappearedForContentCaptureIfNeeded(false);
  }
  
  public final void cancelPendingInputEvents() {
    dispatchCancelPendingInputEvents();
  }
  
  void dispatchCancelPendingInputEvents() {
    this.mPrivateFlags3 &= 0xFFFFFFEF;
    onCancelPendingInputEvents();
    if ((this.mPrivateFlags3 & 0x10) == 16)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("View ");
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append(" did not call through to super.onCancelPendingInputEvents()");
    throw new SuperNotCalledException(stringBuilder.toString());
  }
  
  public void onCancelPendingInputEvents() {
    removePerformClickCallback();
    cancelLongPress();
    this.mPrivateFlags3 |= 0x10;
  }
  
  public void saveHierarchyState(SparseArray<Parcelable> paramSparseArray) {
    dispatchSaveInstanceState(paramSparseArray);
  }
  
  protected void dispatchSaveInstanceState(SparseArray<Parcelable> paramSparseArray) {
    if (this.mID != -1 && (this.mViewFlags & 0x10000) == 0) {
      this.mPrivateFlags &= 0xFFFDFFFF;
      Parcelable parcelable = onSaveInstanceState();
      if ((this.mPrivateFlags & 0x20000) != 0) {
        if (parcelable != null)
          paramSparseArray.put(this.mID, parcelable); 
      } else {
        throw new IllegalStateException("Derived class did not call super.onSaveInstanceState()");
      } 
    } 
  }
  
  protected Parcelable onSaveInstanceState() {
    this.mPrivateFlags |= 0x20000;
    if (this.mStartActivityRequestWho != null || isAutofilled() || this.mAutofillViewId > 1073741823) {
      BaseSavedState baseSavedState = new BaseSavedState();
      if (this.mStartActivityRequestWho != null)
        baseSavedState.mSavedData |= 0x1; 
      if (isAutofilled())
        baseSavedState.mSavedData |= 0x2; 
      if (this.mAutofillViewId > 1073741823)
        baseSavedState.mSavedData |= 0x4; 
      baseSavedState.mStartActivityRequestWhoSaved = this.mStartActivityRequestWho;
      baseSavedState.mIsAutofilled = isAutofilled();
      baseSavedState.mHideHighlight = hideAutofillHighlight();
      baseSavedState.mAutofillViewId = this.mAutofillViewId;
      return baseSavedState;
    } 
    return BaseSavedState.EMPTY_STATE;
  }
  
  public void restoreHierarchyState(SparseArray<Parcelable> paramSparseArray) {
    dispatchRestoreInstanceState(paramSparseArray);
  }
  
  protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray) {
    int i = this.mID;
    if (i != -1) {
      Parcelable parcelable = paramSparseArray.get(i);
      if (parcelable != null) {
        this.mPrivateFlags &= 0xFFFDFFFF;
        onRestoreInstanceState(parcelable);
        if ((this.mPrivateFlags & 0x20000) == 0)
          throw new IllegalStateException("Derived class did not call super.onRestoreInstanceState()"); 
      } 
    } 
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    StringBuilder stringBuilder1;
    this.mPrivateFlags |= 0x20000;
    if (paramParcelable == null || paramParcelable instanceof AbsSavedState) {
      if (paramParcelable != null && paramParcelable instanceof BaseSavedState) {
        BaseSavedState baseSavedState = (BaseSavedState)paramParcelable;
        if ((baseSavedState.mSavedData & 0x1) != 0)
          this.mStartActivityRequestWho = baseSavedState.mStartActivityRequestWhoSaved; 
        if ((baseSavedState.mSavedData & 0x2) != 0)
          setAutofilled(baseSavedState.mIsAutofilled, baseSavedState.mHideHighlight); 
        if ((baseSavedState.mSavedData & 0x4) != 0) {
          paramParcelable = paramParcelable;
          ((BaseSavedState)paramParcelable).mSavedData &= 0xFFFFFFFB;
          if ((this.mPrivateFlags3 & 0x40000000) != 0) {
            if (Log.isLoggable("View.Autofill", 3)) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("onRestoreInstanceState(): not setting autofillId to ");
              stringBuilder1.append(baseSavedState.mAutofillViewId);
              stringBuilder1.append(" because view explicitly set it to ");
              stringBuilder1.append(this.mAutofillId);
              Log.d("View.Autofill", stringBuilder1.toString());
            } 
          } else {
            this.mAutofillViewId = baseSavedState.mAutofillViewId;
            this.mAutofillId = null;
          } 
        } 
      } 
      return;
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Wrong state class, expecting View State but received ");
    stringBuilder2.append(stringBuilder1.getClass().toString());
    stringBuilder2.append(" instead. This usually happens when two views of different type have the same id in the same hierarchy. This view's id is ");
    Context context = this.mContext;
    stringBuilder2.append(ViewDebug.resolveId(context, getId()));
    stringBuilder2.append(". Make sure other views do not use the same id.");
    throw new IllegalArgumentException(stringBuilder2.toString());
  }
  
  public long getDrawingTime() {
    long l;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      l = attachInfo.mDrawingTime;
    } else {
      l = 0L;
    } 
    return l;
  }
  
  public void setDuplicateParentStateEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setFlags(bool, 4194304);
  }
  
  public boolean isDuplicateParentStateEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x400000) == 4194304) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setLayerType(int paramInt, Paint paramPaint) {
    if (paramInt >= 0 && paramInt <= 2) {
      if (sShouldCheckTouchBoost && paramInt == 1) {
        Context context = this.mContext;
        if (context instanceof Activity) {
          Window window = ((Activity)context).getWindow();
          if (window != null && shouldForceLayerHardware(window.getAttributes().getTitle()))
            this.mPendingLayerType = 2; 
        } 
      } 
      boolean bool = this.mRenderNode.setLayerType(paramInt);
      if (!bool) {
        setLayerPaint(paramPaint);
        return;
      } 
      if (paramInt != 1)
        destroyDrawingCache(); 
      this.mLayerType = paramInt;
      if (paramInt == 0)
        paramPaint = null; 
      this.mLayerPaint = paramPaint;
      this.mRenderNode.setLayerPaint(paramPaint);
      invalidateParentCaches();
      invalidate(true);
      return;
    } 
    throw new IllegalArgumentException("Layer type can only be one of: LAYER_TYPE_NONE, LAYER_TYPE_SOFTWARE or LAYER_TYPE_HARDWARE");
  }
  
  public void setLayerPaint(Paint paramPaint) {
    int i = getLayerType();
    if (i != 0) {
      this.mLayerPaint = paramPaint;
      if (i == 2) {
        if (this.mRenderNode.setLayerPaint(paramPaint))
          invalidateViewProperty(false, false); 
      } else {
        invalidate();
      } 
    } 
  }
  
  public int getLayerType() {
    return this.mLayerType;
  }
  
  public void buildLayer() {
    if (this.mLayerType == 0)
      return; 
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      if (getWidth() == 0 || getHeight() == 0)
        return; 
      int i = this.mLayerType;
      if (i != 1) {
        if (i == 2) {
          updateDisplayListIfDirty();
          if (attachInfo.mThreadedRenderer != null && this.mRenderNode.hasDisplayList())
            attachInfo.mThreadedRenderer.buildLayer(this.mRenderNode); 
        } 
      } else {
        buildDrawingCache(true);
      } 
      return;
    } 
    throw new IllegalStateException("This view must be attached to a window first");
  }
  
  protected void destroyHardwareResources() {
    ViewOverlay viewOverlay = this.mOverlay;
    if (viewOverlay != null)
      viewOverlay.getOverlayView().destroyHardwareResources(); 
    GhostView ghostView = this.mGhostView;
    if (ghostView != null)
      ghostView.destroyHardwareResources(); 
  }
  
  @Deprecated
  public void setDrawingCacheEnabled(boolean paramBoolean) {
    char c = Character.MIN_VALUE;
    this.mCachingFailed = false;
    if (paramBoolean)
      c = '耀'; 
    setFlags(c, 32768);
  }
  
  @ExportedProperty(category = "drawing")
  @Deprecated
  public boolean isDrawingCacheEnabled() {
    boolean bool;
    if ((this.mViewFlags & 0x8000) == 32768) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void outputDirtyFlags(String paramString, boolean paramBoolean, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(this);
    stringBuilder.append("             DIRTY(");
    stringBuilder.append(this.mPrivateFlags & 0x200000);
    stringBuilder.append(") DRAWN(");
    stringBuilder.append(this.mPrivateFlags & 0x20);
    stringBuilder.append(") CACHE_VALID(");
    stringBuilder.append(this.mPrivateFlags & 0x8000);
    stringBuilder.append(") INVALIDATED(");
    stringBuilder.append(this.mPrivateFlags & Integer.MIN_VALUE);
    stringBuilder.append(")");
    Log.d("View", stringBuilder.toString());
    if (paramBoolean)
      this.mPrivateFlags &= paramInt; 
    if (this instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)this;
      int i = viewGroup.getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = viewGroup.getChildAt(b);
        stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        stringBuilder.append("  ");
        view.outputDirtyFlags(stringBuilder.toString(), paramBoolean, paramInt);
      } 
    } 
  }
  
  protected void dispatchGetDisplayList() {}
  
  public boolean canHaveDisplayList() {
    boolean bool;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null && attachInfo.mThreadedRenderer != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public RenderNode updateDisplayListIfDirty() {
    RenderNode renderNode = this.mRenderNode;
    if (!canHaveDisplayList())
      return renderNode; 
    if ((this.mPrivateFlags & 0x8000) == 0 || !renderNode.hasDisplayList() || this.mRecreateDisplayList) {
      if (renderNode.hasDisplayList() && !this.mRecreateDisplayList) {
        int i1 = this.mPrivateFlags | 0x8020;
        this.mPrivateFlags = i1 & 0xFFDFFFFF;
        dispatchGetDisplayList();
        return renderNode;
      } 
      this.mRecreateDisplayList = true;
      int i = this.mRight, j = this.mLeft;
      int k = this.mBottom, m = this.mTop;
      int n = getLayerType();
      RecordingCanvas recordingCanvas = renderNode.beginRecording(i - j, k - m);
      if (n == 1)
        try {
          buildDrawingCache(true);
          Bitmap bitmap = getDrawingCache(true);
          if (bitmap != null)
            recordingCanvas.drawBitmap(bitmap, 0.0F, 0.0F, this.mLayerPaint); 
          return renderNode;
        } finally {
          renderNode.endRecording();
          setDisplayListProperties(renderNode);
        }  
      computeScroll();
      recordingCanvas.translate(-this.mScrollX, -this.mScrollY);
      this.mPrivateFlags = i = this.mPrivateFlags | 0x8020;
      this.mPrivateFlags = i &= 0xFFDFFFFF;
      if ((i & 0x80) == 128) {
        dispatchDraw((Canvas)recordingCanvas);
        drawAutofilledHighlight((Canvas)recordingCanvas);
        if (this.mOverlay != null && !this.mOverlay.isEmpty())
          this.mOverlay.getOverlayView().draw((Canvas)recordingCanvas); 
        if (isShowingLayoutBounds())
          debugDrawFocus((Canvas)recordingCanvas); 
      } else {
        draw((Canvas)recordingCanvas);
      } 
    } else {
      int i = this.mPrivateFlags | 0x8020;
      this.mPrivateFlags = i & 0xFFDFFFFF;
      return renderNode;
    } 
    renderNode.endRecording();
    setDisplayListProperties(renderNode);
    return renderNode;
  }
  
  private void resetDisplayList() {
    this.mRenderNode.discardDisplayList();
    RenderNode renderNode = this.mBackgroundRenderNode;
    if (renderNode != null)
      renderNode.discardDisplayList(); 
  }
  
  @Deprecated
  public Bitmap getDrawingCache() {
    return getDrawingCache(false);
  }
  
  @Deprecated
  public Bitmap getDrawingCache(boolean paramBoolean) {
    Bitmap bitmap;
    int i = this.mViewFlags;
    if ((i & 0x20000) == 131072)
      return null; 
    if ((i & 0x8000) == 32768)
      buildDrawingCache(paramBoolean); 
    if (paramBoolean) {
      bitmap = this.mDrawingCache;
    } else {
      bitmap = this.mUnscaledDrawingCache;
    } 
    return bitmap;
  }
  
  @Deprecated
  public void destroyDrawingCache() {
    Bitmap bitmap = this.mDrawingCache;
    if (bitmap != null) {
      bitmap.recycle();
      this.mDrawingCache = null;
    } 
    bitmap = this.mUnscaledDrawingCache;
    if (bitmap != null) {
      bitmap.recycle();
      this.mUnscaledDrawingCache = null;
    } 
  }
  
  @Deprecated
  public void setDrawingCacheBackgroundColor(int paramInt) {
    if (paramInt != this.mDrawingCacheBackgroundColor) {
      this.mDrawingCacheBackgroundColor = paramInt;
      this.mPrivateFlags &= 0xFFFF7FFF;
    } 
  }
  
  @Deprecated
  public int getDrawingCacheBackgroundColor() {
    return this.mDrawingCacheBackgroundColor;
  }
  
  @Deprecated
  public void buildDrawingCache() {
    buildDrawingCache(false);
  }
  
  @Deprecated
  public void buildDrawingCache(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPrivateFlags : I
    //   4: ldc 32768
    //   6: iand
    //   7: ifeq -> 31
    //   10: iload_1
    //   11: ifeq -> 24
    //   14: aload_0
    //   15: getfield mDrawingCache : Landroid/graphics/Bitmap;
    //   18: ifnonnull -> 226
    //   21: goto -> 31
    //   24: aload_0
    //   25: getfield mUnscaledDrawingCache : Landroid/graphics/Bitmap;
    //   28: ifnonnull -> 226
    //   31: ldc2_w 8
    //   34: invokestatic isTagEnabled : (J)Z
    //   37: ifeq -> 80
    //   40: new java/lang/StringBuilder
    //   43: dup
    //   44: invokespecial <init> : ()V
    //   47: astore_2
    //   48: aload_2
    //   49: ldc_w 'buildDrawingCache/SW Layer for '
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload_2
    //   57: aload_0
    //   58: invokevirtual getClass : ()Ljava/lang/Class;
    //   61: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   64: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: pop
    //   68: aload_2
    //   69: invokevirtual toString : ()Ljava/lang/String;
    //   72: astore_2
    //   73: ldc2_w 8
    //   76: aload_2
    //   77: invokestatic traceBegin : (JLjava/lang/String;)V
    //   80: ldc_w 'SnsCommentShowAbLayout'
    //   83: aload_0
    //   84: invokevirtual getClass : ()Ljava/lang/Class;
    //   87: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   90: invokevirtual equals : (Ljava/lang/Object;)Z
    //   93: istore_3
    //   94: iload_3
    //   95: ifeq -> 150
    //   98: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   101: ifnonnull -> 118
    //   104: ldc android/view/View
    //   106: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   109: astore_2
    //   110: aload_2
    //   111: putstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   114: aload_2
    //   115: ifnull -> 215
    //   118: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   121: astore_2
    //   122: new com/oplus/orms/info/OrmsSaParam
    //   125: astore #4
    //   127: aload #4
    //   129: ldc_w ''
    //   132: ldc_w 'ORMS_ACTION_ANIMATION'
    //   135: bipush #100
    //   137: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   140: aload_2
    //   141: aload #4
    //   143: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   146: pop2
    //   147: goto -> 215
    //   150: ldc_w 'MultiTouchImageView'
    //   153: aload_0
    //   154: invokevirtual getClass : ()Ljava/lang/Class;
    //   157: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   160: invokevirtual equals : (Ljava/lang/Object;)Z
    //   163: ifeq -> 215
    //   166: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   169: ifnonnull -> 186
    //   172: ldc android/view/View
    //   174: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   177: astore_2
    //   178: aload_2
    //   179: putstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   182: aload_2
    //   183: ifnull -> 215
    //   186: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   189: astore_2
    //   190: new com/oplus/orms/info/OrmsSaParam
    //   193: astore #4
    //   195: aload #4
    //   197: ldc_w ''
    //   200: ldc_w 'ORMS_ACTION_ANIMATION'
    //   203: bipush #30
    //   205: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   208: aload_2
    //   209: aload #4
    //   211: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   214: pop2
    //   215: aload_0
    //   216: iload_1
    //   217: invokespecial buildDrawingCacheImpl : (Z)V
    //   220: ldc2_w 8
    //   223: invokestatic traceEnd : (J)V
    //   226: return
    //   227: astore_2
    //   228: ldc2_w 8
    //   231: invokestatic traceEnd : (J)V
    //   234: aload_2
    //   235: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #21742	-> 0
    //   #21744	-> 31
    //   #21745	-> 40
    //   #21746	-> 56
    //   #21745	-> 73
    //   #21751	-> 80
    //   #21752	-> 98
    //   #21753	-> 118
    //   #21755	-> 150
    //   #21756	-> 166
    //   #21757	-> 186
    //   #21761	-> 215
    //   #21763	-> 220
    //   #21764	-> 226
    //   #21766	-> 226
    //   #21763	-> 227
    //   #21764	-> 234
    // Exception table:
    //   from	to	target	type
    //   80	94	227	finally
    //   98	114	227	finally
    //   118	147	227	finally
    //   150	166	227	finally
    //   166	182	227	finally
    //   186	215	227	finally
    //   215	220	227	finally
  }
  
  private void buildDrawingCacheImpl(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: iconst_0
    //   2: putfield mCachingFailed : Z
    //   5: aload_0
    //   6: getfield mRight : I
    //   9: aload_0
    //   10: getfield mLeft : I
    //   13: isub
    //   14: istore_2
    //   15: aload_0
    //   16: getfield mBottom : I
    //   19: aload_0
    //   20: getfield mTop : I
    //   23: isub
    //   24: istore_3
    //   25: aload_0
    //   26: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   29: astore #4
    //   31: aload #4
    //   33: ifnull -> 50
    //   36: aload #4
    //   38: getfield mScalingRequired : Z
    //   41: ifeq -> 50
    //   44: iconst_1
    //   45: istore #5
    //   47: goto -> 53
    //   50: iconst_0
    //   51: istore #5
    //   53: iload_2
    //   54: istore #6
    //   56: iload_3
    //   57: istore #7
    //   59: iload_1
    //   60: ifeq -> 104
    //   63: iload_2
    //   64: istore #6
    //   66: iload_3
    //   67: istore #7
    //   69: iload #5
    //   71: ifeq -> 104
    //   74: iload_2
    //   75: i2f
    //   76: aload #4
    //   78: getfield mApplicationScale : F
    //   81: fmul
    //   82: ldc_w 0.5
    //   85: fadd
    //   86: f2i
    //   87: istore #6
    //   89: iload_3
    //   90: i2f
    //   91: aload #4
    //   93: getfield mApplicationScale : F
    //   96: fmul
    //   97: ldc_w 0.5
    //   100: fadd
    //   101: f2i
    //   102: istore #7
    //   104: aload_0
    //   105: getfield mDrawingCacheBackgroundColor : I
    //   108: istore #8
    //   110: iload #8
    //   112: ifne -> 130
    //   115: aload_0
    //   116: invokevirtual isOpaque : ()Z
    //   119: ifeq -> 125
    //   122: goto -> 130
    //   125: iconst_0
    //   126: istore_3
    //   127: goto -> 132
    //   130: iconst_1
    //   131: istore_3
    //   132: aload #4
    //   134: ifnull -> 150
    //   137: aload #4
    //   139: getfield mUse32BitDrawingCache : Z
    //   142: ifeq -> 150
    //   145: iconst_1
    //   146: istore_2
    //   147: goto -> 152
    //   150: iconst_0
    //   151: istore_2
    //   152: iload_3
    //   153: ifeq -> 166
    //   156: iload_2
    //   157: ifne -> 166
    //   160: iconst_2
    //   161: istore #9
    //   163: goto -> 169
    //   166: iconst_4
    //   167: istore #9
    //   169: iload #6
    //   171: iload #7
    //   173: imul
    //   174: iload #9
    //   176: imul
    //   177: i2l
    //   178: lstore #10
    //   180: aload_0
    //   181: getfield mContext : Landroid/content/Context;
    //   184: astore #12
    //   186: aload #12
    //   188: invokestatic get : (Landroid/content/Context;)Landroid/view/ViewConfiguration;
    //   191: invokevirtual getScaledMaximumDrawingCacheSize : ()I
    //   194: i2l
    //   195: lstore #13
    //   197: iload #6
    //   199: ifle -> 701
    //   202: iload #7
    //   204: ifle -> 701
    //   207: lload #10
    //   209: lload #13
    //   211: lcmp
    //   212: ifle -> 218
    //   215: goto -> 701
    //   218: iconst_1
    //   219: istore #9
    //   221: iload_1
    //   222: ifeq -> 234
    //   225: aload_0
    //   226: getfield mDrawingCache : Landroid/graphics/Bitmap;
    //   229: astore #12
    //   231: goto -> 240
    //   234: aload_0
    //   235: getfield mUnscaledDrawingCache : Landroid/graphics/Bitmap;
    //   238: astore #12
    //   240: aload #12
    //   242: ifnull -> 269
    //   245: aload #12
    //   247: invokevirtual getWidth : ()I
    //   250: iload #6
    //   252: if_icmpne -> 269
    //   255: aload #12
    //   257: astore #15
    //   259: aload #12
    //   261: invokevirtual getHeight : ()I
    //   264: iload #7
    //   266: if_icmpeq -> 397
    //   269: iload_3
    //   270: ifne -> 281
    //   273: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   276: astore #15
    //   278: goto -> 298
    //   281: iload_2
    //   282: ifeq -> 293
    //   285: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   288: astore #15
    //   290: goto -> 298
    //   293: getstatic android/graphics/Bitmap$Config.RGB_565 : Landroid/graphics/Bitmap$Config;
    //   296: astore #15
    //   298: aload #12
    //   300: ifnull -> 308
    //   303: aload #12
    //   305: invokevirtual recycle : ()V
    //   308: aload_0
    //   309: getfield mResources : Landroid/content/res/Resources;
    //   312: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   315: iload #6
    //   317: iload #7
    //   319: aload #15
    //   321: invokestatic createBitmap : (Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   324: astore #15
    //   326: aload #15
    //   328: aload_0
    //   329: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   332: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   335: getfield densityDpi : I
    //   338: invokevirtual setDensity : (I)V
    //   341: iload_1
    //   342: ifeq -> 359
    //   345: aload_0
    //   346: aload #15
    //   348: putfield mDrawingCache : Landroid/graphics/Bitmap;
    //   351: goto -> 365
    //   354: astore #12
    //   356: goto -> 678
    //   359: aload_0
    //   360: aload #15
    //   362: putfield mUnscaledDrawingCache : Landroid/graphics/Bitmap;
    //   365: iload_3
    //   366: ifeq -> 382
    //   369: iload_2
    //   370: ifeq -> 382
    //   373: aload #15
    //   375: iconst_0
    //   376: invokevirtual setHasAlpha : (Z)V
    //   379: goto -> 382
    //   382: iconst_0
    //   383: istore #7
    //   385: iload #8
    //   387: ifeq -> 393
    //   390: iconst_1
    //   391: istore #7
    //   393: iload #7
    //   395: istore #9
    //   397: aload #4
    //   399: ifnull -> 443
    //   402: aload #4
    //   404: getfield mCanvas : Landroid/graphics/Canvas;
    //   407: astore #16
    //   409: aload #16
    //   411: astore #12
    //   413: aload #16
    //   415: ifnonnull -> 427
    //   418: new android/graphics/Canvas
    //   421: dup
    //   422: invokespecial <init> : ()V
    //   425: astore #12
    //   427: aload #12
    //   429: aload #15
    //   431: invokevirtual setBitmap : (Landroid/graphics/Bitmap;)V
    //   434: aload #4
    //   436: aconst_null
    //   437: putfield mCanvas : Landroid/graphics/Canvas;
    //   440: goto -> 454
    //   443: new android/graphics/Canvas
    //   446: dup
    //   447: aload #15
    //   449: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   452: astore #12
    //   454: iload #9
    //   456: ifeq -> 466
    //   459: aload #15
    //   461: iload #8
    //   463: invokevirtual eraseColor : (I)V
    //   466: aload_0
    //   467: invokevirtual computeScroll : ()V
    //   470: aload #12
    //   472: invokevirtual save : ()I
    //   475: istore #7
    //   477: iload_1
    //   478: ifeq -> 505
    //   481: iload #5
    //   483: ifeq -> 505
    //   486: aload #4
    //   488: getfield mApplicationScale : F
    //   491: fstore #17
    //   493: aload #12
    //   495: fload #17
    //   497: fload #17
    //   499: invokevirtual scale : (FF)V
    //   502: goto -> 505
    //   505: aload #12
    //   507: aload_0
    //   508: getfield mScrollX : I
    //   511: ineg
    //   512: i2f
    //   513: aload_0
    //   514: getfield mScrollY : I
    //   517: ineg
    //   518: i2f
    //   519: invokevirtual translate : (FF)V
    //   522: aload_0
    //   523: aload_0
    //   524: getfield mPrivateFlags : I
    //   527: bipush #32
    //   529: ior
    //   530: putfield mPrivateFlags : I
    //   533: aload_0
    //   534: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   537: astore #15
    //   539: aload #15
    //   541: ifnull -> 559
    //   544: aload #15
    //   546: getfield mHardwareAccelerated : Z
    //   549: ifeq -> 559
    //   552: aload_0
    //   553: getfield mLayerType : I
    //   556: ifeq -> 570
    //   559: aload_0
    //   560: aload_0
    //   561: getfield mPrivateFlags : I
    //   564: ldc 32768
    //   566: ior
    //   567: putfield mPrivateFlags : I
    //   570: aload_0
    //   571: getfield mPrivateFlags : I
    //   574: istore #5
    //   576: iload #5
    //   578: sipush #128
    //   581: iand
    //   582: sipush #128
    //   585: if_icmpne -> 644
    //   588: aload_0
    //   589: iload #5
    //   591: ldc_w -2097153
    //   594: iand
    //   595: putfield mPrivateFlags : I
    //   598: aload_0
    //   599: aload #12
    //   601: invokevirtual dispatchDraw : (Landroid/graphics/Canvas;)V
    //   604: aload_0
    //   605: aload #12
    //   607: invokespecial drawAutofilledHighlight : (Landroid/graphics/Canvas;)V
    //   610: aload_0
    //   611: getfield mOverlay : Landroid/view/ViewOverlay;
    //   614: astore #15
    //   616: aload #15
    //   618: ifnull -> 650
    //   621: aload #15
    //   623: invokevirtual isEmpty : ()Z
    //   626: ifne -> 650
    //   629: aload_0
    //   630: getfield mOverlay : Landroid/view/ViewOverlay;
    //   633: invokevirtual getOverlayView : ()Landroid/view/ViewGroup;
    //   636: aload #12
    //   638: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   641: goto -> 650
    //   644: aload_0
    //   645: aload #12
    //   647: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   650: aload #12
    //   652: iload #7
    //   654: invokevirtual restoreToCount : (I)V
    //   657: aload #12
    //   659: aconst_null
    //   660: invokevirtual setBitmap : (Landroid/graphics/Bitmap;)V
    //   663: aload #4
    //   665: ifnull -> 675
    //   668: aload #4
    //   670: aload #12
    //   672: putfield mCanvas : Landroid/graphics/Canvas;
    //   675: return
    //   676: astore #12
    //   678: iload_1
    //   679: ifeq -> 690
    //   682: aload_0
    //   683: aconst_null
    //   684: putfield mDrawingCache : Landroid/graphics/Bitmap;
    //   687: goto -> 695
    //   690: aload_0
    //   691: aconst_null
    //   692: putfield mUnscaledDrawingCache : Landroid/graphics/Bitmap;
    //   695: aload_0
    //   696: iconst_1
    //   697: putfield mCachingFailed : Z
    //   700: return
    //   701: iload #6
    //   703: ifle -> 788
    //   706: iload #7
    //   708: ifle -> 788
    //   711: new java/lang/StringBuilder
    //   714: dup
    //   715: invokespecial <init> : ()V
    //   718: astore #12
    //   720: aload #12
    //   722: aload_0
    //   723: invokevirtual getClass : ()Ljava/lang/Class;
    //   726: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   729: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   732: pop
    //   733: aload #12
    //   735: ldc_w ' not displayed because it is too large to fit into a software layer (or drawing cache), needs '
    //   738: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   741: pop
    //   742: aload #12
    //   744: lload #10
    //   746: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   749: pop
    //   750: aload #12
    //   752: ldc_w ' bytes, only '
    //   755: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   758: pop
    //   759: aload #12
    //   761: lload #13
    //   763: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   766: pop
    //   767: aload #12
    //   769: ldc_w ' available'
    //   772: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   775: pop
    //   776: ldc_w 'View'
    //   779: aload #12
    //   781: invokevirtual toString : ()Ljava/lang/String;
    //   784: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   787: pop
    //   788: aload_0
    //   789: invokevirtual destroyDrawingCache : ()V
    //   792: aload_0
    //   793: iconst_1
    //   794: putfield mCachingFailed : Z
    //   797: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #21772	-> 0
    //   #21774	-> 5
    //   #21775	-> 15
    //   #21777	-> 25
    //   #21778	-> 31
    //   #21780	-> 53
    //   #21781	-> 74
    //   #21782	-> 89
    //   #21785	-> 104
    //   #21786	-> 110
    //   #21787	-> 132
    //   #21789	-> 152
    //   #21790	-> 180
    //   #21791	-> 186
    //   #21792	-> 197
    //   #21804	-> 218
    //   #21805	-> 221
    //   #21807	-> 240
    //   #21809	-> 269
    //   #21812	-> 273
    //   #21817	-> 273
    //   #21818	-> 278
    //   #21823	-> 281
    //   #21827	-> 298
    //   #21830	-> 308
    //   #21832	-> 326
    //   #21833	-> 341
    //   #21834	-> 345
    //   #21839	-> 354
    //   #21836	-> 359
    //   #21838	-> 365
    //   #21850	-> 382
    //   #21852	-> 385
    //   #21856	-> 397
    //   #21857	-> 402
    //   #21858	-> 409
    //   #21859	-> 418
    //   #21861	-> 427
    //   #21866	-> 434
    //   #21869	-> 443
    //   #21872	-> 454
    //   #21873	-> 459
    //   #21876	-> 466
    //   #21877	-> 470
    //   #21879	-> 477
    //   #21880	-> 486
    //   #21881	-> 493
    //   #21879	-> 505
    //   #21884	-> 505
    //   #21886	-> 522
    //   #21887	-> 533
    //   #21889	-> 559
    //   #21893	-> 570
    //   #21894	-> 588
    //   #21895	-> 598
    //   #21896	-> 604
    //   #21897	-> 610
    //   #21898	-> 629
    //   #21901	-> 644
    //   #21904	-> 650
    //   #21905	-> 657
    //   #21907	-> 663
    //   #21909	-> 668
    //   #21911	-> 675
    //   #21839	-> 676
    //   #21843	-> 678
    //   #21844	-> 682
    //   #21846	-> 690
    //   #21848	-> 695
    //   #21849	-> 700
    //   #21792	-> 701
    //   #21793	-> 701
    //   #21794	-> 711
    //   #21799	-> 788
    //   #21800	-> 792
    //   #21801	-> 797
    // Exception table:
    //   from	to	target	type
    //   308	326	676	java/lang/OutOfMemoryError
    //   326	341	676	java/lang/OutOfMemoryError
    //   345	351	354	java/lang/OutOfMemoryError
    //   359	365	676	java/lang/OutOfMemoryError
    //   373	379	354	java/lang/OutOfMemoryError
  }
  
  public Bitmap createSnapshot(ViewDebug.CanvasProvider paramCanvasProvider, boolean paramBoolean) {
    float f;
    int i = this.mRight, j = this.mLeft;
    int k = this.mBottom, m = this.mTop;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      f = attachInfo.mApplicationScale;
    } else {
      f = 1.0F;
    } 
    i = (int)((i - j) * f + 0.5F);
    k = (int)((k - m) * f + 0.5F);
    Canvas canvas1 = null, canvas2 = null;
    m = 1;
    if (i <= 0)
      i = 1; 
    if (k > 0)
      m = k; 
    Canvas canvas3 = canvas1;
    try {
      Canvas canvas = paramCanvasProvider.getCanvas(this, i, m);
      if (attachInfo != null) {
        canvas3 = canvas1;
        canvas2 = attachInfo.mCanvas;
        canvas3 = canvas2;
        attachInfo.mCanvas = null;
      } 
      canvas3 = canvas2;
      computeScroll();
      canvas3 = canvas2;
      i = canvas.save();
      canvas3 = canvas2;
      canvas.scale(f, f);
      canvas3 = canvas2;
      canvas.translate(-this.mScrollX, -this.mScrollY);
      canvas3 = canvas2;
      k = this.mPrivateFlags;
      canvas3 = canvas2;
      m = this.mPrivateFlags & 0xFFDFFFFF;
      canvas3 = canvas2;
      this.mPrivateFlags = m;
      if ((m & 0x80) == 128) {
        canvas3 = canvas2;
        dispatchDraw(canvas);
        canvas3 = canvas2;
        drawAutofilledHighlight(canvas);
        canvas3 = canvas2;
        if (this.mOverlay != null) {
          canvas3 = canvas2;
          if (!this.mOverlay.isEmpty()) {
            canvas3 = canvas2;
            this.mOverlay.getOverlayView().draw(canvas);
          } 
        } 
      } else {
        canvas3 = canvas2;
        draw(canvas);
      } 
      canvas3 = canvas2;
      this.mPrivateFlags = k;
      canvas3 = canvas2;
      canvas.restoreToCount(i);
      canvas3 = canvas2;
      return paramCanvasProvider.createBitmap();
    } finally {
      if (canvas3 != null)
        attachInfo.mCanvas = canvas3; 
    } 
  }
  
  public boolean isInEditMode() {
    return false;
  }
  
  protected boolean isPaddingOffsetRequired() {
    return false;
  }
  
  protected int getLeftPaddingOffset() {
    return 0;
  }
  
  protected int getRightPaddingOffset() {
    return 0;
  }
  
  protected int getTopPaddingOffset() {
    return 0;
  }
  
  protected int getBottomPaddingOffset() {
    return 0;
  }
  
  protected int getFadeTop(boolean paramBoolean) {
    int i = this.mPaddingTop;
    int j = i;
    if (paramBoolean)
      j = i + getTopPaddingOffset(); 
    return j;
  }
  
  protected int getFadeHeight(boolean paramBoolean) {
    int i = this.mPaddingTop;
    int j = i;
    if (paramBoolean)
      j = i + getTopPaddingOffset(); 
    return this.mBottom - this.mTop - this.mPaddingBottom - j;
  }
  
  @ExportedProperty(category = "drawing")
  public boolean isHardwareAccelerated() {
    boolean bool;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null && attachInfo.mHardwareAccelerated) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setClipBounds(Rect paramRect) {
    Rect rect = this.mClipBounds;
    if (paramRect == rect || (paramRect != null && paramRect.equals(rect)))
      return; 
    if (paramRect != null) {
      rect = this.mClipBounds;
      if (rect == null) {
        this.mClipBounds = new Rect(paramRect);
      } else {
        rect.set(paramRect);
      } 
    } else {
      this.mClipBounds = null;
    } 
    this.mRenderNode.setClipRect(this.mClipBounds);
    invalidateViewProperty(false, false);
  }
  
  public Rect getClipBounds() {
    Rect rect;
    if (this.mClipBounds != null) {
      rect = new Rect(this.mClipBounds);
    } else {
      rect = null;
    } 
    return rect;
  }
  
  public boolean getClipBounds(Rect paramRect) {
    Rect rect = this.mClipBounds;
    if (rect != null) {
      paramRect.set(rect);
      return true;
    } 
    return false;
  }
  
  private boolean applyLegacyAnimation(ViewGroup paramViewGroup, long paramLong, Animation paramAnimation, boolean paramBoolean) {
    int i = paramViewGroup.mGroupFlags;
    boolean bool = paramAnimation.isInitialized();
    if (!bool) {
      paramAnimation.initialize(this.mRight - this.mLeft, this.mBottom - this.mTop, paramViewGroup.getWidth(), paramViewGroup.getHeight());
      paramAnimation.initializeInvalidateRegion(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop);
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null)
        paramAnimation.setListenerHandler(attachInfo.mHandler); 
      onAnimationStart();
    } 
    Transformation transformation = paramViewGroup.getChildTransformation();
    bool = paramAnimation.getTransformation(paramLong, transformation, 1.0F);
    if (paramBoolean && this.mAttachInfo.mApplicationScale != 1.0F) {
      if (paramViewGroup.mInvalidationTransformation == null)
        paramViewGroup.mInvalidationTransformation = new Transformation(); 
      transformation = paramViewGroup.mInvalidationTransformation;
      paramAnimation.getTransformation(paramLong, transformation, 1.0F);
    } 
    if (bool)
      if (!paramAnimation.willChangeBounds()) {
        if ((i & 0x90) == 128) {
          paramViewGroup.mGroupFlags |= 0x4;
        } else if ((i & 0x4) == 0) {
          paramViewGroup.mPrivateFlags |= 0x40;
          paramViewGroup.invalidate(this.mLeft, this.mTop, this.mRight, this.mBottom);
        } 
      } else {
        if (paramViewGroup.mInvalidateRegion == null)
          paramViewGroup.mInvalidateRegion = new RectF(); 
        RectF rectF = paramViewGroup.mInvalidateRegion;
        paramAnimation.getInvalidateRegion(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop, rectF, transformation);
        paramViewGroup.mPrivateFlags |= 0x40;
        int j = this.mLeft + (int)rectF.left;
        i = this.mTop + (int)rectF.top;
        int k = (int)(rectF.width() + 0.5F);
        int m = (int)(rectF.height() + 0.5F);
        paramViewGroup.invalidate(j, i, k + j, m + i);
      }  
    return bool;
  }
  
  void setDisplayListProperties(RenderNode paramRenderNode) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 244
    //   4: aload_1
    //   5: aload_0
    //   6: invokevirtual getHasOverlappingRendering : ()Z
    //   9: invokevirtual setHasOverlappingRendering : (Z)Z
    //   12: pop
    //   13: aload_0
    //   14: getfield mParent : Landroid/view/ViewParent;
    //   17: astore_2
    //   18: aload_2
    //   19: instanceof android/view/ViewGroup
    //   22: ifeq -> 42
    //   25: aload_2
    //   26: checkcast android/view/ViewGroup
    //   29: astore_2
    //   30: aload_2
    //   31: invokevirtual getClipChildren : ()Z
    //   34: ifeq -> 42
    //   37: iconst_1
    //   38: istore_3
    //   39: goto -> 44
    //   42: iconst_0
    //   43: istore_3
    //   44: aload_1
    //   45: iload_3
    //   46: invokevirtual setClipToBounds : (Z)Z
    //   49: pop
    //   50: fconst_1
    //   51: fstore #4
    //   53: aload_0
    //   54: getfield mParent : Landroid/view/ViewParent;
    //   57: astore_2
    //   58: fload #4
    //   60: fstore #5
    //   62: aload_2
    //   63: instanceof android/view/ViewGroup
    //   66: ifeq -> 168
    //   69: fload #4
    //   71: fstore #5
    //   73: aload_2
    //   74: checkcast android/view/ViewGroup
    //   77: getfield mGroupFlags : I
    //   80: sipush #2048
    //   83: iand
    //   84: ifeq -> 168
    //   87: aload_0
    //   88: getfield mParent : Landroid/view/ViewParent;
    //   91: checkcast android/view/ViewGroup
    //   94: astore #6
    //   96: aload #6
    //   98: invokevirtual getChildTransformation : ()Landroid/view/animation/Transformation;
    //   101: astore_2
    //   102: fload #4
    //   104: fstore #5
    //   106: aload #6
    //   108: aload_0
    //   109: aload_2
    //   110: invokevirtual getChildStaticTransformation : (Landroid/view/View;Landroid/view/animation/Transformation;)Z
    //   113: ifeq -> 168
    //   116: aload_2
    //   117: invokevirtual getTransformationType : ()I
    //   120: istore #7
    //   122: fload #4
    //   124: fstore #5
    //   126: iload #7
    //   128: ifeq -> 168
    //   131: iload #7
    //   133: iconst_1
    //   134: iand
    //   135: ifeq -> 144
    //   138: aload_2
    //   139: invokevirtual getAlpha : ()F
    //   142: fstore #4
    //   144: fload #4
    //   146: fstore #5
    //   148: iload #7
    //   150: iconst_2
    //   151: iand
    //   152: ifeq -> 168
    //   155: aload_1
    //   156: aload_2
    //   157: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   160: invokevirtual setStaticMatrix : (Landroid/graphics/Matrix;)Z
    //   163: pop
    //   164: fload #4
    //   166: fstore #5
    //   168: aload_0
    //   169: getfield mTransformationInfo : Landroid/view/View$TransformationInfo;
    //   172: ifnull -> 230
    //   175: fload #5
    //   177: aload_0
    //   178: invokespecial getFinalAlpha : ()F
    //   181: fmul
    //   182: fstore #5
    //   184: fload #5
    //   186: fstore #4
    //   188: fload #5
    //   190: fconst_1
    //   191: fcmpg
    //   192: ifge -> 220
    //   195: ldc_w 255.0
    //   198: fload #5
    //   200: fmul
    //   201: f2i
    //   202: istore #7
    //   204: fload #5
    //   206: fstore #4
    //   208: aload_0
    //   209: iload #7
    //   211: invokevirtual onSetAlpha : (I)Z
    //   214: ifeq -> 220
    //   217: fconst_1
    //   218: fstore #4
    //   220: aload_1
    //   221: fload #4
    //   223: invokevirtual setAlpha : (F)Z
    //   226: pop
    //   227: goto -> 244
    //   230: fload #5
    //   232: fconst_1
    //   233: fcmpg
    //   234: ifge -> 244
    //   237: aload_1
    //   238: fload #5
    //   240: invokevirtual setAlpha : (F)Z
    //   243: pop
    //   244: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #22229	-> 0
    //   #22230	-> 4
    //   #22231	-> 13
    //   #22232	-> 30
    //   #22231	-> 44
    //   #22234	-> 50
    //   #22235	-> 53
    //   #22237	-> 87
    //   #22238	-> 96
    //   #22239	-> 102
    //   #22240	-> 116
    //   #22241	-> 122
    //   #22242	-> 131
    //   #22243	-> 138
    //   #22245	-> 144
    //   #22246	-> 155
    //   #22251	-> 168
    //   #22252	-> 175
    //   #22253	-> 184
    //   #22254	-> 195
    //   #22255	-> 204
    //   #22256	-> 217
    //   #22259	-> 220
    //   #22260	-> 230
    //   #22261	-> 237
    //   #22264	-> 244
  }
  
  boolean draw(Canvas paramCanvas, ViewGroup paramViewGroup, long paramLong) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual isHardwareAccelerated : ()Z
    //   4: istore #5
    //   6: aload_0
    //   7: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   10: astore #6
    //   12: aload #6
    //   14: ifnull -> 36
    //   17: aload #6
    //   19: getfield mHardwareAccelerated : Z
    //   22: ifeq -> 36
    //   25: iload #5
    //   27: ifeq -> 36
    //   30: iconst_1
    //   31: istore #7
    //   33: goto -> 39
    //   36: iconst_0
    //   37: istore #7
    //   39: iconst_0
    //   40: istore #8
    //   42: aload_0
    //   43: invokevirtual hasIdentityMatrix : ()Z
    //   46: istore #9
    //   48: aload_2
    //   49: getfield mGroupFlags : I
    //   52: istore #10
    //   54: iload #10
    //   56: sipush #256
    //   59: iand
    //   60: ifeq -> 82
    //   63: aload_2
    //   64: invokevirtual getChildTransformation : ()Landroid/view/animation/Transformation;
    //   67: invokevirtual clear : ()V
    //   70: aload_2
    //   71: aload_2
    //   72: getfield mGroupFlags : I
    //   75: sipush #-257
    //   78: iand
    //   79: putfield mGroupFlags : I
    //   82: aconst_null
    //   83: astore #11
    //   85: iconst_0
    //   86: istore #12
    //   88: aload_0
    //   89: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   92: astore #6
    //   94: aload #6
    //   96: ifnull -> 113
    //   99: aload #6
    //   101: getfield mScalingRequired : Z
    //   104: ifeq -> 113
    //   107: iconst_1
    //   108: istore #13
    //   110: goto -> 116
    //   113: iconst_0
    //   114: istore #13
    //   116: aload_0
    //   117: invokevirtual getAnimation : ()Landroid/view/animation/Animation;
    //   120: astore #14
    //   122: aload #14
    //   124: ifnull -> 170
    //   127: aload_0
    //   128: aload_2
    //   129: lload_3
    //   130: aload #14
    //   132: iload #13
    //   134: invokespecial applyLegacyAnimation : (Landroid/view/ViewGroup;JLandroid/view/animation/Animation;Z)Z
    //   137: istore #15
    //   139: aload #14
    //   141: invokevirtual willChangeTransformationMatrix : ()Z
    //   144: istore #16
    //   146: iload #16
    //   148: ifeq -> 161
    //   151: aload_0
    //   152: aload_0
    //   153: getfield mPrivateFlags3 : I
    //   156: iconst_1
    //   157: ior
    //   158: putfield mPrivateFlags3 : I
    //   161: aload_2
    //   162: invokevirtual getChildTransformation : ()Landroid/view/animation/Transformation;
    //   165: astore #6
    //   167: goto -> 316
    //   170: aload_0
    //   171: getfield mPrivateFlags3 : I
    //   174: istore #17
    //   176: aconst_null
    //   177: astore #18
    //   179: iload #17
    //   181: iconst_1
    //   182: iand
    //   183: ifeq -> 206
    //   186: aload_0
    //   187: getfield mRenderNode : Landroid/graphics/RenderNode;
    //   190: aconst_null
    //   191: invokevirtual setAnimationMatrix : (Landroid/graphics/Matrix;)Z
    //   194: pop
    //   195: aload_0
    //   196: aload_0
    //   197: getfield mPrivateFlags3 : I
    //   200: bipush #-2
    //   202: iand
    //   203: putfield mPrivateFlags3 : I
    //   206: iload #8
    //   208: istore #15
    //   210: aload #11
    //   212: astore #6
    //   214: iload #12
    //   216: istore #16
    //   218: iload #7
    //   220: ifne -> 316
    //   223: iload #8
    //   225: istore #15
    //   227: aload #11
    //   229: astore #6
    //   231: iload #12
    //   233: istore #16
    //   235: iload #10
    //   237: sipush #2048
    //   240: iand
    //   241: ifeq -> 316
    //   244: aload_2
    //   245: invokevirtual getChildTransformation : ()Landroid/view/animation/Transformation;
    //   248: astore #19
    //   250: aload_2
    //   251: aload_0
    //   252: aload #19
    //   254: invokevirtual getChildStaticTransformation : (Landroid/view/View;Landroid/view/animation/Transformation;)Z
    //   257: istore #20
    //   259: iload #8
    //   261: istore #15
    //   263: aload #11
    //   265: astore #6
    //   267: iload #12
    //   269: istore #16
    //   271: iload #20
    //   273: ifeq -> 316
    //   276: aload #19
    //   278: invokevirtual getTransformationType : ()I
    //   281: istore #17
    //   283: aload #18
    //   285: astore #6
    //   287: iload #17
    //   289: ifeq -> 296
    //   292: aload #19
    //   294: astore #6
    //   296: iload #17
    //   298: iconst_2
    //   299: iand
    //   300: ifeq -> 309
    //   303: iconst_1
    //   304: istore #16
    //   306: goto -> 312
    //   309: iconst_0
    //   310: istore #16
    //   312: iload #8
    //   314: istore #15
    //   316: iload #16
    //   318: iload #9
    //   320: iconst_1
    //   321: ixor
    //   322: ior
    //   323: istore #21
    //   325: aload_0
    //   326: aload_0
    //   327: getfield mPrivateFlags : I
    //   330: bipush #32
    //   332: ior
    //   333: putfield mPrivateFlags : I
    //   336: iload #21
    //   338: ifne -> 419
    //   341: iload #10
    //   343: sipush #2049
    //   346: iand
    //   347: iconst_1
    //   348: if_icmpne -> 419
    //   351: aload_0
    //   352: getfield mLeft : I
    //   355: i2f
    //   356: fstore #22
    //   358: aload_0
    //   359: getfield mTop : I
    //   362: i2f
    //   363: fstore #23
    //   365: aload_0
    //   366: getfield mRight : I
    //   369: i2f
    //   370: fstore #24
    //   372: aload_0
    //   373: getfield mBottom : I
    //   376: i2f
    //   377: fstore #25
    //   379: aload_1
    //   380: fload #22
    //   382: fload #23
    //   384: fload #24
    //   386: fload #25
    //   388: invokevirtual quickReject : (FFFF)Z
    //   391: ifeq -> 419
    //   394: aload_0
    //   395: getfield mPrivateFlags : I
    //   398: bipush #64
    //   400: iand
    //   401: ifne -> 419
    //   404: aload_0
    //   405: aload_0
    //   406: getfield mPrivateFlags2 : I
    //   409: ldc_w 268435456
    //   412: ior
    //   413: putfield mPrivateFlags2 : I
    //   416: iload #15
    //   418: ireturn
    //   419: aload_0
    //   420: aload_0
    //   421: getfield mPrivateFlags2 : I
    //   424: ldc_w -268435457
    //   427: iand
    //   428: putfield mPrivateFlags2 : I
    //   431: iload #5
    //   433: ifeq -> 474
    //   436: aload_0
    //   437: getfield mPrivateFlags : I
    //   440: ldc_w -2147483648
    //   443: iand
    //   444: ifeq -> 453
    //   447: iconst_1
    //   448: istore #16
    //   450: goto -> 456
    //   453: iconst_0
    //   454: istore #16
    //   456: aload_0
    //   457: iload #16
    //   459: putfield mRecreateDisplayList : Z
    //   462: aload_0
    //   463: aload_0
    //   464: getfield mPrivateFlags : I
    //   467: ldc_w 2147483647
    //   470: iand
    //   471: putfield mPrivateFlags : I
    //   474: aload_0
    //   475: invokevirtual getLayerType : ()I
    //   478: istore #26
    //   480: iload #26
    //   482: iconst_1
    //   483: if_icmpeq -> 500
    //   486: iload #7
    //   488: ifne -> 494
    //   491: goto -> 500
    //   494: aconst_null
    //   495: astore #19
    //   497: goto -> 528
    //   500: iload #26
    //   502: istore #17
    //   504: iload #26
    //   506: ifeq -> 517
    //   509: iconst_1
    //   510: istore #17
    //   512: aload_0
    //   513: iconst_1
    //   514: invokevirtual buildDrawingCache : (Z)V
    //   517: aload_0
    //   518: iconst_1
    //   519: invokevirtual getDrawingCache : (Z)Landroid/graphics/Bitmap;
    //   522: astore #19
    //   524: iload #17
    //   526: istore #26
    //   528: iload #7
    //   530: ifeq -> 563
    //   533: aload_0
    //   534: invokevirtual updateDisplayListIfDirty : ()Landroid/graphics/RenderNode;
    //   537: astore #11
    //   539: aload #11
    //   541: invokevirtual hasDisplayList : ()Z
    //   544: ifne -> 556
    //   547: iconst_0
    //   548: istore #17
    //   550: aconst_null
    //   551: astore #11
    //   553: goto -> 570
    //   556: iload #7
    //   558: istore #17
    //   560: goto -> 570
    //   563: aconst_null
    //   564: astore #11
    //   566: iload #7
    //   568: istore #17
    //   570: iload #17
    //   572: ifne -> 594
    //   575: aload_0
    //   576: invokevirtual computeScroll : ()V
    //   579: aload_0
    //   580: getfield mScrollX : I
    //   583: istore #27
    //   585: aload_0
    //   586: getfield mScrollY : I
    //   589: istore #28
    //   591: goto -> 600
    //   594: iconst_0
    //   595: istore #27
    //   597: iconst_0
    //   598: istore #28
    //   600: aload #19
    //   602: ifnull -> 616
    //   605: iload #17
    //   607: ifne -> 616
    //   610: iconst_1
    //   611: istore #29
    //   613: goto -> 619
    //   616: iconst_0
    //   617: istore #29
    //   619: aload #19
    //   621: ifnonnull -> 635
    //   624: iload #17
    //   626: ifne -> 635
    //   629: iconst_1
    //   630: istore #30
    //   632: goto -> 638
    //   635: iconst_0
    //   636: istore #30
    //   638: iconst_m1
    //   639: istore #7
    //   641: iload #17
    //   643: ifeq -> 651
    //   646: aload #6
    //   648: ifnull -> 657
    //   651: aload_1
    //   652: invokevirtual save : ()I
    //   655: istore #7
    //   657: iload #30
    //   659: ifeq -> 685
    //   662: aload_1
    //   663: aload_0
    //   664: getfield mLeft : I
    //   667: iload #27
    //   669: isub
    //   670: i2f
    //   671: aload_0
    //   672: getfield mTop : I
    //   675: iload #28
    //   677: isub
    //   678: i2f
    //   679: invokevirtual translate : (FF)V
    //   682: goto -> 742
    //   685: iload #17
    //   687: ifne -> 704
    //   690: aload_1
    //   691: aload_0
    //   692: getfield mLeft : I
    //   695: i2f
    //   696: aload_0
    //   697: getfield mTop : I
    //   700: i2f
    //   701: invokevirtual translate : (FF)V
    //   704: iload #13
    //   706: ifeq -> 742
    //   709: iload #17
    //   711: ifeq -> 720
    //   714: aload_1
    //   715: invokevirtual save : ()I
    //   718: istore #7
    //   720: fconst_1
    //   721: aload_0
    //   722: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   725: getfield mApplicationScale : F
    //   728: fdiv
    //   729: fstore #23
    //   731: aload_1
    //   732: fload #23
    //   734: fload #23
    //   736: invokevirtual scale : (FF)V
    //   739: goto -> 742
    //   742: iload #17
    //   744: ifeq -> 753
    //   747: fconst_1
    //   748: fstore #23
    //   750: goto -> 764
    //   753: aload_0
    //   754: invokevirtual getAlpha : ()F
    //   757: aload_0
    //   758: invokevirtual getTransitionAlpha : ()F
    //   761: fmul
    //   762: fstore #23
    //   764: aload #6
    //   766: ifnonnull -> 836
    //   769: fload #23
    //   771: fconst_1
    //   772: fcmpg
    //   773: iflt -> 836
    //   776: aload_0
    //   777: invokevirtual hasIdentityMatrix : ()Z
    //   780: ifeq -> 836
    //   783: aload_0
    //   784: getfield mPrivateFlags3 : I
    //   787: iconst_2
    //   788: iand
    //   789: ifeq -> 795
    //   792: goto -> 836
    //   795: aload_0
    //   796: getfield mPrivateFlags : I
    //   799: ldc_w 262144
    //   802: iand
    //   803: ldc_w 262144
    //   806: if_icmpne -> 829
    //   809: aload_0
    //   810: sipush #255
    //   813: invokevirtual onSetAlpha : (I)Z
    //   816: pop
    //   817: aload_0
    //   818: aload_0
    //   819: getfield mPrivateFlags : I
    //   822: ldc_w -262145
    //   825: iand
    //   826: putfield mPrivateFlags : I
    //   829: iload #7
    //   831: istore #31
    //   833: goto -> 1212
    //   836: aload #6
    //   838: ifnonnull -> 852
    //   841: iload #9
    //   843: ifne -> 849
    //   846: goto -> 852
    //   849: goto -> 1039
    //   852: iconst_0
    //   853: istore #31
    //   855: iload #30
    //   857: ifeq -> 873
    //   860: iload #27
    //   862: ineg
    //   863: istore #31
    //   865: iload #28
    //   867: ineg
    //   868: istore #32
    //   870: goto -> 876
    //   873: iconst_0
    //   874: istore #32
    //   876: aload #6
    //   878: ifnull -> 991
    //   881: iload #21
    //   883: ifeq -> 951
    //   886: iload #17
    //   888: ifeq -> 905
    //   891: aload #11
    //   893: aload #6
    //   895: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   898: invokevirtual setAnimationMatrix : (Landroid/graphics/Matrix;)Z
    //   901: pop
    //   902: goto -> 936
    //   905: aload_1
    //   906: iload #31
    //   908: ineg
    //   909: i2f
    //   910: iload #32
    //   912: ineg
    //   913: i2f
    //   914: invokevirtual translate : (FF)V
    //   917: aload_1
    //   918: aload #6
    //   920: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   923: invokevirtual concat : (Landroid/graphics/Matrix;)V
    //   926: aload_1
    //   927: iload #31
    //   929: i2f
    //   930: iload #32
    //   932: i2f
    //   933: invokevirtual translate : (FF)V
    //   936: aload_2
    //   937: aload_2
    //   938: getfield mGroupFlags : I
    //   941: sipush #256
    //   944: ior
    //   945: putfield mGroupFlags : I
    //   948: goto -> 951
    //   951: aload #6
    //   953: invokevirtual getAlpha : ()F
    //   956: fstore #22
    //   958: fload #23
    //   960: fstore #24
    //   962: fload #22
    //   964: fconst_1
    //   965: fcmpg
    //   966: ifge -> 995
    //   969: fload #23
    //   971: fload #22
    //   973: fmul
    //   974: fstore #24
    //   976: aload_2
    //   977: aload_2
    //   978: getfield mGroupFlags : I
    //   981: sipush #256
    //   984: ior
    //   985: putfield mGroupFlags : I
    //   988: goto -> 995
    //   991: fload #23
    //   993: fstore #24
    //   995: iload #9
    //   997: ifne -> 1035
    //   1000: iload #17
    //   1002: ifne -> 1035
    //   1005: aload_1
    //   1006: iload #31
    //   1008: ineg
    //   1009: i2f
    //   1010: iload #32
    //   1012: ineg
    //   1013: i2f
    //   1014: invokevirtual translate : (FF)V
    //   1017: aload_1
    //   1018: aload_0
    //   1019: invokevirtual getMatrix : ()Landroid/graphics/Matrix;
    //   1022: invokevirtual concat : (Landroid/graphics/Matrix;)V
    //   1025: aload_1
    //   1026: iload #31
    //   1028: i2f
    //   1029: iload #32
    //   1031: i2f
    //   1032: invokevirtual translate : (FF)V
    //   1035: fload #24
    //   1037: fstore #23
    //   1039: fload #23
    //   1041: fconst_1
    //   1042: fcmpg
    //   1043: iflt -> 1061
    //   1046: aload_0
    //   1047: getfield mPrivateFlags3 : I
    //   1050: iconst_2
    //   1051: iand
    //   1052: ifeq -> 1058
    //   1055: goto -> 1061
    //   1058: goto -> 1208
    //   1061: fload #23
    //   1063: fconst_1
    //   1064: fcmpg
    //   1065: ifge -> 1081
    //   1068: aload_0
    //   1069: aload_0
    //   1070: getfield mPrivateFlags3 : I
    //   1073: iconst_2
    //   1074: ior
    //   1075: putfield mPrivateFlags3 : I
    //   1078: goto -> 1092
    //   1081: aload_0
    //   1082: aload_0
    //   1083: getfield mPrivateFlags3 : I
    //   1086: bipush #-3
    //   1088: iand
    //   1089: putfield mPrivateFlags3 : I
    //   1092: aload_2
    //   1093: aload_2
    //   1094: getfield mGroupFlags : I
    //   1097: sipush #256
    //   1100: ior
    //   1101: putfield mGroupFlags : I
    //   1104: iload #29
    //   1106: ifne -> 1208
    //   1109: fload #23
    //   1111: ldc_w 255.0
    //   1114: fmul
    //   1115: f2i
    //   1116: istore #31
    //   1118: aload_0
    //   1119: iload #31
    //   1121: invokevirtual onSetAlpha : (I)Z
    //   1124: ifne -> 1193
    //   1127: iload #17
    //   1129: ifeq -> 1153
    //   1132: aload #11
    //   1134: aload_0
    //   1135: invokevirtual getAlpha : ()F
    //   1138: fload #23
    //   1140: fmul
    //   1141: aload_0
    //   1142: invokevirtual getTransitionAlpha : ()F
    //   1145: fmul
    //   1146: invokevirtual setAlpha : (F)Z
    //   1149: pop
    //   1150: goto -> 1205
    //   1153: iload #26
    //   1155: ifne -> 1190
    //   1158: aload_1
    //   1159: iload #27
    //   1161: i2f
    //   1162: iload #28
    //   1164: i2f
    //   1165: aload_0
    //   1166: invokevirtual getWidth : ()I
    //   1169: iload #27
    //   1171: iadd
    //   1172: i2f
    //   1173: iload #28
    //   1175: aload_0
    //   1176: invokevirtual getHeight : ()I
    //   1179: iadd
    //   1180: i2f
    //   1181: iload #31
    //   1183: invokevirtual saveLayerAlpha : (FFFFI)I
    //   1186: pop
    //   1187: goto -> 1205
    //   1190: goto -> 1205
    //   1193: aload_0
    //   1194: aload_0
    //   1195: getfield mPrivateFlags : I
    //   1198: ldc_w 262144
    //   1201: ior
    //   1202: putfield mPrivateFlags : I
    //   1205: goto -> 1208
    //   1208: iload #7
    //   1210: istore #31
    //   1212: iload #17
    //   1214: ifne -> 1326
    //   1217: iload #10
    //   1219: iconst_1
    //   1220: iand
    //   1221: ifeq -> 1308
    //   1224: aload #19
    //   1226: ifnonnull -> 1308
    //   1229: iload #30
    //   1231: ifeq -> 1260
    //   1234: aload_1
    //   1235: iload #27
    //   1237: iload #28
    //   1239: iload #27
    //   1241: aload_0
    //   1242: invokevirtual getWidth : ()I
    //   1245: iadd
    //   1246: iload #28
    //   1248: aload_0
    //   1249: invokevirtual getHeight : ()I
    //   1252: iadd
    //   1253: invokevirtual clipRect : (IIII)Z
    //   1256: pop
    //   1257: goto -> 1308
    //   1260: iload #13
    //   1262: ifeq -> 1293
    //   1265: aload #19
    //   1267: ifnonnull -> 1273
    //   1270: goto -> 1293
    //   1273: aload_1
    //   1274: iconst_0
    //   1275: iconst_0
    //   1276: aload #19
    //   1278: invokevirtual getWidth : ()I
    //   1281: aload #19
    //   1283: invokevirtual getHeight : ()I
    //   1286: invokevirtual clipRect : (IIII)Z
    //   1289: pop
    //   1290: goto -> 1308
    //   1293: aload_1
    //   1294: iconst_0
    //   1295: iconst_0
    //   1296: aload_0
    //   1297: invokevirtual getWidth : ()I
    //   1300: aload_0
    //   1301: invokevirtual getHeight : ()I
    //   1304: invokevirtual clipRect : (IIII)Z
    //   1307: pop
    //   1308: aload_0
    //   1309: getfield mClipBounds : Landroid/graphics/Rect;
    //   1312: astore #6
    //   1314: aload #6
    //   1316: ifnull -> 1326
    //   1319: aload_1
    //   1320: aload #6
    //   1322: invokevirtual clipRect : (Landroid/graphics/Rect;)Z
    //   1325: pop
    //   1326: iload #29
    //   1328: ifne -> 1404
    //   1331: iload #17
    //   1333: ifeq -> 1360
    //   1336: aload_0
    //   1337: ldc_w -2097153
    //   1340: aload_0
    //   1341: getfield mPrivateFlags : I
    //   1344: iand
    //   1345: putfield mPrivateFlags : I
    //   1348: aload_1
    //   1349: checkcast android/graphics/RecordingCanvas
    //   1352: aload #11
    //   1354: invokevirtual drawRenderNode : (Landroid/graphics/RenderNode;)V
    //   1357: goto -> 1557
    //   1360: aload_0
    //   1361: getfield mPrivateFlags : I
    //   1364: istore #17
    //   1366: iload #17
    //   1368: sipush #128
    //   1371: iand
    //   1372: sipush #128
    //   1375: if_icmpne -> 1396
    //   1378: aload_0
    //   1379: ldc_w -2097153
    //   1382: iload #17
    //   1384: iand
    //   1385: putfield mPrivateFlags : I
    //   1388: aload_0
    //   1389: aload_1
    //   1390: invokevirtual dispatchDraw : (Landroid/graphics/Canvas;)V
    //   1393: goto -> 1557
    //   1396: aload_0
    //   1397: aload_1
    //   1398: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   1401: goto -> 1557
    //   1404: aload #19
    //   1406: ifnull -> 1557
    //   1409: aload_0
    //   1410: ldc_w -2097153
    //   1413: aload_0
    //   1414: getfield mPrivateFlags : I
    //   1417: iand
    //   1418: putfield mPrivateFlags : I
    //   1421: iload #26
    //   1423: ifeq -> 1499
    //   1426: aload_0
    //   1427: getfield mLayerPaint : Landroid/graphics/Paint;
    //   1430: astore #6
    //   1432: aload #6
    //   1434: ifnonnull -> 1440
    //   1437: goto -> 1499
    //   1440: aload #6
    //   1442: invokevirtual getAlpha : ()I
    //   1445: istore #17
    //   1447: fload #23
    //   1449: fconst_1
    //   1450: fcmpg
    //   1451: ifge -> 1468
    //   1454: aload_0
    //   1455: getfield mLayerPaint : Landroid/graphics/Paint;
    //   1458: iload #17
    //   1460: i2f
    //   1461: fload #23
    //   1463: fmul
    //   1464: f2i
    //   1465: invokevirtual setAlpha : (I)V
    //   1468: aload_1
    //   1469: aload #19
    //   1471: fconst_0
    //   1472: fconst_0
    //   1473: aload_0
    //   1474: getfield mLayerPaint : Landroid/graphics/Paint;
    //   1477: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    //   1480: fload #23
    //   1482: fconst_1
    //   1483: fcmpg
    //   1484: ifge -> 1557
    //   1487: aload_0
    //   1488: getfield mLayerPaint : Landroid/graphics/Paint;
    //   1491: iload #17
    //   1493: invokevirtual setAlpha : (I)V
    //   1496: goto -> 1557
    //   1499: aload_2
    //   1500: getfield mCachePaint : Landroid/graphics/Paint;
    //   1503: astore #11
    //   1505: aload #11
    //   1507: astore #6
    //   1509: aload #11
    //   1511: ifnonnull -> 1535
    //   1514: new android/graphics/Paint
    //   1517: dup
    //   1518: invokespecial <init> : ()V
    //   1521: astore #6
    //   1523: aload #6
    //   1525: iconst_0
    //   1526: invokevirtual setDither : (Z)V
    //   1529: aload_2
    //   1530: aload #6
    //   1532: putfield mCachePaint : Landroid/graphics/Paint;
    //   1535: aload #6
    //   1537: fload #23
    //   1539: ldc_w 255.0
    //   1542: fmul
    //   1543: f2i
    //   1544: invokevirtual setAlpha : (I)V
    //   1547: aload_1
    //   1548: aload #19
    //   1550: fconst_0
    //   1551: fconst_0
    //   1552: aload #6
    //   1554: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    //   1557: iload #7
    //   1559: iflt -> 1568
    //   1562: aload_1
    //   1563: iload #7
    //   1565: invokevirtual restoreToCount : (I)V
    //   1568: aload #14
    //   1570: ifnull -> 1606
    //   1573: iload #15
    //   1575: ifne -> 1606
    //   1578: iload #5
    //   1580: ifne -> 1599
    //   1583: aload #14
    //   1585: invokevirtual getFillAfter : ()Z
    //   1588: ifne -> 1599
    //   1591: aload_0
    //   1592: sipush #255
    //   1595: invokevirtual onSetAlpha : (I)Z
    //   1598: pop
    //   1599: aload_2
    //   1600: aload_0
    //   1601: aload #14
    //   1603: invokevirtual finishAnimatingView : (Landroid/view/View;Landroid/view/animation/Animation;)V
    //   1606: iload #15
    //   1608: ifeq -> 1643
    //   1611: iload #5
    //   1613: ifeq -> 1643
    //   1616: aload #14
    //   1618: invokevirtual hasAlpha : ()Z
    //   1621: ifeq -> 1643
    //   1624: aload_0
    //   1625: getfield mPrivateFlags : I
    //   1628: ldc_w 262144
    //   1631: iand
    //   1632: ldc_w 262144
    //   1635: if_icmpne -> 1643
    //   1638: aload_0
    //   1639: iconst_1
    //   1640: invokevirtual invalidate : (Z)V
    //   1643: aload_0
    //   1644: iconst_0
    //   1645: putfield mRecreateDisplayList : Z
    //   1648: iload #15
    //   1650: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #22273	-> 0
    //   #22279	-> 6
    //   #22283	-> 39
    //   #22284	-> 42
    //   #22285	-> 48
    //   #22287	-> 54
    //   #22288	-> 63
    //   #22289	-> 70
    //   #22292	-> 82
    //   #22293	-> 85
    //   #22294	-> 88
    //   #22295	-> 116
    //   #22296	-> 122
    //   #22297	-> 127
    //   #22298	-> 139
    //   #22299	-> 146
    //   #22300	-> 151
    //   #22302	-> 161
    //   #22304	-> 170
    //   #22306	-> 186
    //   #22307	-> 195
    //   #22309	-> 206
    //   #22311	-> 244
    //   #22312	-> 250
    //   #22313	-> 259
    //   #22314	-> 276
    //   #22315	-> 283
    //   #22316	-> 296
    //   #22321	-> 316
    //   #22325	-> 325
    //   #22327	-> 336
    //   #22330	-> 379
    //   #22332	-> 404
    //   #22333	-> 416
    //   #22335	-> 419
    //   #22337	-> 431
    //   #22340	-> 436
    //   #22341	-> 462
    //   #22344	-> 474
    //   #22345	-> 474
    //   #22346	-> 474
    //   #22347	-> 480
    //   #22348	-> 500
    //   #22350	-> 509
    //   #22351	-> 512
    //   #22353	-> 517
    //   #22356	-> 528
    //   #22359	-> 533
    //   #22360	-> 539
    //   #22364	-> 547
    //   #22365	-> 547
    //   #22360	-> 556
    //   #22356	-> 563
    //   #22369	-> 570
    //   #22370	-> 570
    //   #22371	-> 570
    //   #22372	-> 575
    //   #22373	-> 579
    //   #22374	-> 585
    //   #22371	-> 594
    //   #22377	-> 600
    //   #22378	-> 619
    //   #22380	-> 638
    //   #22381	-> 641
    //   #22382	-> 651
    //   #22384	-> 657
    //   #22385	-> 662
    //   #22387	-> 685
    //   #22388	-> 690
    //   #22390	-> 704
    //   #22391	-> 709
    //   #22393	-> 714
    //   #22396	-> 720
    //   #22397	-> 731
    //   #22401	-> 742
    //   #22402	-> 764
    //   #22404	-> 776
    //   #22466	-> 795
    //   #22467	-> 809
    //   #22468	-> 817
    //   #22471	-> 829
    //   #22406	-> 836
    //   #22407	-> 852
    //   #22408	-> 855
    //   #22410	-> 855
    //   #22411	-> 860
    //   #22412	-> 865
    //   #22410	-> 873
    //   #22415	-> 876
    //   #22416	-> 881
    //   #22417	-> 886
    //   #22418	-> 891
    //   #22422	-> 905
    //   #22423	-> 917
    //   #22424	-> 926
    //   #22426	-> 936
    //   #22416	-> 951
    //   #22429	-> 951
    //   #22430	-> 958
    //   #22431	-> 969
    //   #22432	-> 976
    //   #22415	-> 991
    //   #22436	-> 995
    //   #22437	-> 1005
    //   #22438	-> 1017
    //   #22439	-> 1025
    //   #22444	-> 1035
    //   #22445	-> 1061
    //   #22446	-> 1068
    //   #22448	-> 1081
    //   #22450	-> 1092
    //   #22451	-> 1104
    //   #22452	-> 1109
    //   #22453	-> 1118
    //   #22454	-> 1127
    //   #22455	-> 1132
    //   #22456	-> 1153
    //   #22457	-> 1158
    //   #22456	-> 1190
    //   #22462	-> 1193
    //   #22464	-> 1205
    //   #22451	-> 1208
    //   #22471	-> 1208
    //   #22473	-> 1217
    //   #22474	-> 1229
    //   #22475	-> 1234
    //   #22477	-> 1260
    //   #22480	-> 1273
    //   #22477	-> 1293
    //   #22478	-> 1293
    //   #22485	-> 1308
    //   #22487	-> 1319
    //   #22491	-> 1326
    //   #22492	-> 1331
    //   #22493	-> 1336
    //   #22494	-> 1348
    //   #22497	-> 1360
    //   #22498	-> 1378
    //   #22499	-> 1388
    //   #22501	-> 1396
    //   #22504	-> 1404
    //   #22505	-> 1409
    //   #22506	-> 1421
    //   #22518	-> 1440
    //   #22519	-> 1447
    //   #22520	-> 1454
    //   #22522	-> 1468
    //   #22523	-> 1480
    //   #22524	-> 1487
    //   #22508	-> 1499
    //   #22509	-> 1505
    //   #22510	-> 1514
    //   #22511	-> 1523
    //   #22512	-> 1529
    //   #22514	-> 1535
    //   #22515	-> 1547
    //   #22529	-> 1557
    //   #22530	-> 1562
    //   #22533	-> 1568
    //   #22534	-> 1578
    //   #22535	-> 1591
    //   #22537	-> 1599
    //   #22540	-> 1606
    //   #22541	-> 1616
    //   #22543	-> 1638
    //   #22547	-> 1643
    //   #22549	-> 1648
  }
  
  static Paint getDebugPaint() {
    if (sDebugPaint == null) {
      Paint paint = new Paint();
      paint.setAntiAlias(false);
    } 
    return sDebugPaint;
  }
  
  final int dipsToPixels(int paramInt) {
    float f = (getContext().getResources().getDisplayMetrics()).density;
    return (int)(paramInt * f + 0.5F);
  }
  
  private final void debugDrawFocus(Canvas paramCanvas) {
    if (isFocused() || debugWebViewDraw()) {
      int i = dipsToPixels(8);
      int j = this.mScrollX;
      int k = this.mRight + j - this.mLeft;
      int m = this.mScrollY;
      int n = this.mBottom + m - this.mTop;
      Paint paint = getDebugPaint();
      paint.setColor(DEBUG_CORNERS_COLOR);
      paint.setStyle(Paint.Style.FILL);
      paramCanvas.drawRect(j, m, (j + i), (m + i), paint);
      paramCanvas.drawRect((k - i), m, k, (m + i), paint);
      paramCanvas.drawRect(j, (n - i), (j + i), n, paint);
      paramCanvas.drawRect((k - i), (n - i), k, n, paint);
      paint.setStyle(Paint.Style.STROKE);
      paramCanvas.drawLine(j, m, k, n, paint);
      paramCanvas.drawLine(j, n, k, m, paint);
    } 
  }
  
  public void draw(Canvas paramCanvas) {
    int i8, i = this.mPrivateFlags;
    this.mPrivateFlags = 0xFFDFFFFF & i | 0x20;
    drawBackground(paramCanvas);
    ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).markOnDraw(this, paramCanvas);
    i = this.mViewFlags;
    if ((i & 0x1000) != 0) {
      j = 1;
    } else {
      j = 0;
    } 
    if ((i & 0x2000) != 0) {
      k = 1;
    } else {
      k = 0;
    } 
    if (!k && !j) {
      onDraw(paramCanvas);
      dispatchDraw(paramCanvas);
      drawAutofilledHighlight(paramCanvas);
      ViewOverlay viewOverlay1 = this.mOverlay;
      if (viewOverlay1 != null && !viewOverlay1.isEmpty())
        this.mOverlay.getOverlayView().dispatchDraw(paramCanvas); 
      onDrawForeground(paramCanvas);
      drawDefaultFocusHighlight(paramCanvas);
      if (isShowingLayoutBounds())
        debugDrawFocus(paramCanvas); 
      return;
    } 
    float f1 = 0.0F;
    float f2 = 0.0F;
    float f3 = 0.0F;
    i = this.mPaddingLeft;
    boolean bool = isPaddingOffsetRequired();
    if (bool)
      i += getLeftPaddingOffset(); 
    int m = this.mScrollX + i;
    int n = 0, i1 = this.mRight, i2 = 0, i3 = i1 + m - this.mLeft - this.mPaddingRight - i;
    int i4 = this.mScrollY + getFadeTop(bool);
    int i5 = i4 + getFadeHeight(bool);
    if (bool) {
      i3 += getRightPaddingOffset();
      i5 += getBottomPaddingOffset();
    } 
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    int i6 = 0;
    float f4 = scrollabilityCache.fadingEdgeLength;
    int i7 = 0;
    i1 = (int)f4;
    if (k) {
      i = i1;
      if (i4 + i1 > i5 - i1)
        i = (i5 - i4) / 2; 
    } else {
      i = i1;
    } 
    float f5 = 0.0F;
    if (j && m + i > i3 - i) {
      i8 = (i3 - m) / 2;
    } else {
      i8 = i;
    } 
    if (k) {
      f5 = Math.max(0.0F, Math.min(1.0F, getTopFadingEdgeStrength()));
      if (f5 * f4 > 1.0F) {
        i = 1;
      } else {
        i = 0;
      } 
      n = i;
      f1 = Math.max(0.0F, Math.min(1.0F, getBottomFadingEdgeStrength()));
      if (f1 * f4 > 1.0F) {
        i = 1;
      } else {
        i = 0;
      } 
      i2 = i;
    } 
    if (j) {
      f2 = Math.max(0.0F, Math.min(1.0F, getLeftFadingEdgeStrength()));
      if (f2 * f4 > 1.0F) {
        i = 1;
      } else {
        i = 0;
      } 
      f3 = Math.max(0.0F, Math.min(1.0F, getRightFadingEdgeStrength()));
      if (f3 * f4 > 1.0F) {
        j = 1;
      } else {
        j = 0;
      } 
      i6 = i;
      i7 = j;
    } 
    int i9 = paramCanvas.getSaveCount();
    i = -1;
    int k = -1;
    int j = -1;
    int i10 = getSolidColor();
    if (i10 == 0) {
      if (n != 0)
        i = paramCanvas.saveUnclippedLayer(m, i4, i3, i4 + i8); 
      if (i2 != 0)
        k = paramCanvas.saveUnclippedLayer(m, i5 - i8, i3, i5); 
      if (i6 != 0)
        j = paramCanvas.saveUnclippedLayer(m, i4, m + i8, i5); 
      if (i7 != 0) {
        i1 = paramCanvas.saveUnclippedLayer(i3 - i8, i4, i3, i5);
        int i11 = k;
        k = i1;
        i1 = i;
        i = i11;
      } else {
        byte b = -1;
        i1 = i;
        i = k;
        k = b;
      } 
    } else {
      scrollabilityCache.setFadeColor(i10);
      i = -1;
      k = -1;
      i1 = -1;
      j = -1;
    } 
    onDraw(paramCanvas);
    dispatchDraw(paramCanvas);
    ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).markDrawFadingEdge(this, paramCanvas);
    Paint paint = scrollabilityCache.paint;
    Matrix matrix = scrollabilityCache.matrix;
    Shader shader = scrollabilityCache.shader;
    if (i7 != 0) {
      matrix.setScale(1.0F, f4 * f3);
      matrix.postRotate(90.0F);
      matrix.postTranslate(i3, i4);
      shader.setLocalMatrix(matrix);
      paint.setShader(shader);
      if (i10 == 0) {
        paramCanvas.restoreUnclippedLayer(k, paint);
      } else {
        paramCanvas.drawRect((i3 - i8), i4, i3, i5, paint);
      } 
    } 
    if (i6 != 0) {
      matrix.setScale(1.0F, f4 * f2);
      matrix.postRotate(-90.0F);
      matrix.postTranslate(m, i4);
      shader.setLocalMatrix(matrix);
      paint.setShader(shader);
      if (i10 == 0) {
        paramCanvas.restoreUnclippedLayer(j, paint);
      } else {
        paramCanvas.drawRect(m, i4, (m + i8), i5, paint);
      } 
    } 
    if (i2 != 0) {
      matrix.setScale(1.0F, f4 * f1);
      matrix.postRotate(180.0F);
      matrix.postTranslate(m, i5);
      shader.setLocalMatrix(matrix);
      paint.setShader(shader);
      if (i10 == 0) {
        paramCanvas.restoreUnclippedLayer(i, paint);
      } else {
        paramCanvas.drawRect(m, (i5 - i8), i3, i5, paint);
      } 
    } 
    if (n != 0) {
      matrix.setScale(1.0F, f4 * f5);
      matrix.postTranslate(m, i4);
      shader.setLocalMatrix(matrix);
      paint.setShader(shader);
      if (i10 == 0) {
        paramCanvas.restoreUnclippedLayer(i1, paint);
      } else {
        paramCanvas.drawRect(m, i4, i3, (i4 + i8), paint);
      } 
    } 
    paramCanvas.restoreToCount(i9);
    ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).markForeground(this, paramCanvas);
    drawAutofilledHighlight(paramCanvas);
    ViewOverlay viewOverlay = this.mOverlay;
    if (viewOverlay != null && !viewOverlay.isEmpty())
      this.mOverlay.getOverlayView().dispatchDraw(paramCanvas); 
    onDrawForeground(paramCanvas);
    drawDefaultFocusHighlight(paramCanvas);
    if (isShowingLayoutBounds())
      debugDrawFocus(paramCanvas); 
  }
  
  private void drawBackground(Canvas paramCanvas) {
    Drawable drawable = this.mBackground;
    if (drawable == null)
      return; 
    setBackgroundBounds();
    if (paramCanvas.isHardwareAccelerated()) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null && attachInfo.mThreadedRenderer != null) {
        this.mBackgroundRenderNode = getDrawableRenderNode(drawable, this.mBackgroundRenderNode);
        RenderNode renderNode = this.mBackgroundRenderNode;
        if (renderNode != null && renderNode.hasDisplayList()) {
          setBackgroundRenderNodeProperties(renderNode);
          ((RecordingCanvas)paramCanvas).drawRenderNode(renderNode);
          return;
        } 
      } 
    } 
    ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).markBackground(this, paramCanvas);
    int i = this.mScrollX;
    int j = this.mScrollY;
    if ((i | j) == 0) {
      drawable.draw(paramCanvas);
    } else {
      paramCanvas.translate(i, j);
      drawable.draw(paramCanvas);
      paramCanvas.translate(-i, -j);
    } 
  }
  
  void setBackgroundBounds() {
    if (this.mBackgroundSizeChanged) {
      Drawable drawable = this.mBackground;
      if (drawable != null) {
        drawable.setBounds(0, 0, this.mRight - this.mLeft, this.mBottom - this.mTop);
        this.mBackgroundSizeChanged = false;
        rebuildOutline();
      } 
    } 
  }
  
  private void setBackgroundRenderNodeProperties(RenderNode paramRenderNode) {
    paramRenderNode.setTranslationX(this.mScrollX);
    paramRenderNode.setTranslationY(this.mScrollY);
  }
  
  private RenderNode getDrawableRenderNode(Drawable paramDrawable, RenderNode paramRenderNode) {
    RenderNode renderNode = paramRenderNode;
    if (paramRenderNode == null) {
      renderNode = RenderNode.create(paramDrawable.getClass().getName(), new ViewAnimationHostBridge(this));
      renderNode.setUsageHint(1);
    } 
    Rect rect = paramDrawable.getBounds();
    int i = rect.width();
    int j = rect.height();
    RecordingCanvas recordingCanvas = renderNode.beginRecording(i, j);
    ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).markBackground(this, (Canvas)recordingCanvas);
    recordingCanvas.translate(-rect.left, -rect.top);
    try {
      paramDrawable.draw((Canvas)recordingCanvas);
      renderNode.endRecording();
      renderNode.setLeftTopRightBottom(rect.left, rect.top, rect.right, rect.bottom);
      renderNode.setProjectBackwards(paramDrawable.isProjected());
      renderNode.setProjectionReceiver(true);
      return renderNode;
    } finally {
      renderNode.endRecording();
    } 
  }
  
  public ViewOverlay getOverlay() {
    if (this.mOverlay == null)
      this.mOverlay = new ViewOverlay(this.mContext, this); 
    return this.mOverlay;
  }
  
  @ExportedProperty(category = "drawing")
  public int getSolidColor() {
    return 0;
  }
  
  private static String printFlags(int paramInt) {
    String str = "";
    int i = 0;
    if ((paramInt & 0x1) == 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("");
      stringBuilder.append("TAKES_FOCUS");
      str = stringBuilder.toString();
      i = 0 + 1;
    } 
    paramInt &= 0xC;
    if (paramInt != 4) {
      if (paramInt == 8) {
        String str1 = str;
        if (i > 0) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str);
          stringBuilder1.append(" ");
          str1 = stringBuilder1.toString();
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str1);
        stringBuilder.append("GONE");
        str = stringBuilder.toString();
      } 
    } else {
      String str1 = str;
      if (i > 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str);
        stringBuilder1.append(" ");
        str1 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str1);
      stringBuilder.append("INVISIBLE");
      str = stringBuilder.toString();
    } 
    return str;
  }
  
  private static String printPrivateFlags(int paramInt) {
    String str1 = "";
    int i = 0;
    if ((paramInt & 0x1) == 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("");
      stringBuilder.append("WANTS_FOCUS");
      str1 = stringBuilder.toString();
      i = 0 + 1;
    } 
    String str2 = str1;
    int j = i;
    if ((paramInt & 0x2) == 2) {
      str2 = str1;
      if (i > 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str1);
        stringBuilder1.append(" ");
        str2 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append("FOCUSED");
      str2 = stringBuilder.toString();
      j = i + 1;
    } 
    str1 = str2;
    i = j;
    if ((paramInt & 0x4) == 4) {
      str1 = str2;
      if (j > 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str2);
        stringBuilder1.append(" ");
        str1 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str1);
      stringBuilder.append("SELECTED");
      str1 = stringBuilder.toString();
      i = j + 1;
    } 
    str2 = str1;
    j = i;
    if ((paramInt & 0x8) == 8) {
      str2 = str1;
      if (i > 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str1);
        stringBuilder1.append(" ");
        str2 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append("IS_ROOT_NAMESPACE");
      str2 = stringBuilder.toString();
      j = i + 1;
    } 
    str1 = str2;
    i = j;
    if ((paramInt & 0x10) == 16) {
      str1 = str2;
      if (j > 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str2);
        stringBuilder1.append(" ");
        str1 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str1);
      stringBuilder.append("HAS_BOUNDS");
      str1 = stringBuilder.toString();
      i = j + 1;
    } 
    str2 = str1;
    if ((paramInt & 0x20) == 32) {
      str2 = str1;
      if (i > 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str1);
        stringBuilder1.append(" ");
        str2 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append("DRAWN");
      str2 = stringBuilder.toString();
    } 
    return str2;
  }
  
  public boolean isLayoutRequested() {
    boolean bool;
    if ((this.mPrivateFlags & 0x1000) == 4096) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isLayoutModeOptical(Object paramObject) {
    boolean bool;
    if (paramObject instanceof ViewGroup && ((ViewGroup)paramObject).isLayoutModeOptical()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean setOpticalFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    Insets insets1;
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof View) {
      insets1 = ((View)viewParent).getOpticalInsets();
    } else {
      insets1 = Insets.NONE;
    } 
    Insets insets2 = getOpticalInsets();
    return setFrame(insets1.left + paramInt1 - insets2.left, insets1.top + paramInt2 - insets2.top, insets1.left + paramInt3 + insets2.right, insets1.top + paramInt4 + insets2.bottom);
  }
  
  public void layout(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if ((this.mPrivateFlags3 & 0x8) != 0) {
      onMeasure(this.mOldWidthMeasureSpec, this.mOldHeightMeasureSpec);
      this.mPrivateFlags3 &= 0xFFFFFFF7;
    } 
    int i = this.mLeft;
    int j = this.mTop;
    int k = this.mBottom;
    int m = this.mRight;
    if (isLayoutModeOptical(this.mParent)) {
      bool = setOpticalFrame(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      bool = setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
    } 
    View view = null;
    if (bool || (this.mPrivateFlags & 0x2000) == 8192) {
      onLayout(bool, paramInt1, paramInt2, paramInt3, paramInt4);
      if (shouldDrawRoundScrollbar()) {
        if (this.mRoundScrollbarRenderer == null)
          this.mRoundScrollbarRenderer = new RoundScrollbarRenderer(this); 
      } else {
        this.mRoundScrollbarRenderer = null;
      } 
      this.mPrivateFlags &= 0xFFFFDFFF;
      ListenerInfo listenerInfo = this.mListenerInfo;
      if (listenerInfo != null && listenerInfo.mOnLayoutChangeListeners != null) {
        ArrayList<OnLayoutChangeListener> arrayList = (ArrayList)listenerInfo.mOnLayoutChangeListeners.clone();
        int n = arrayList.size();
        for (byte b = 0; b < n; b++)
          ((OnLayoutChangeListener)arrayList.get(b)).onLayoutChange(this, paramInt1, paramInt2, paramInt3, paramInt4, i, j, m, k); 
      } else {
        view = null;
      } 
    } else {
      view = null;
    } 
    boolean bool = isLayoutValid();
    this.mPrivateFlags &= 0xFFFFEFFF;
    this.mPrivateFlags3 |= 0x4;
    if (!bool && isFocused()) {
      this.mPrivateFlags &= 0xFFFFFFFE;
      if (canTakeFocus()) {
        clearParentsWantFocus();
      } else if (getViewRootImpl() == null || !getViewRootImpl().isInLayout()) {
        clearFocusInternal(view, true, false);
        clearParentsWantFocus();
      } else if (!hasParentWantsFocus()) {
        clearFocusInternal(view, true, false);
      } 
    } else {
      paramInt1 = this.mPrivateFlags;
      if ((paramInt1 & 0x1) != 0) {
        this.mPrivateFlags = paramInt1 & 0xFFFFFFFE;
        View view1 = findFocus();
        if (view1 != null)
          if (!restoreDefaultFocus() && !hasParentWantsFocus())
            view1.clearFocusInternal(view, true, false);  
      } 
    } 
    paramInt1 = this.mPrivateFlags3;
    if ((0x8000000 & paramInt1) != 0) {
      this.mPrivateFlags3 = paramInt1 & 0xF7FFFFFF;
      notifyEnterOrExitForAutoFillIfNeeded(true);
    } 
    notifyAppearedOrDisappearedForContentCaptureIfNeeded(true);
  }
  
  private boolean hasParentWantsFocus() {
    ViewParent viewParent = this.mParent;
    while (viewParent instanceof ViewGroup) {
      viewParent = viewParent;
      if ((((ViewGroup)viewParent).mPrivateFlags & 0x1) != 0)
        return true; 
      viewParent = ((ViewGroup)viewParent).mParent;
    } 
    return false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {}
  
  protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool = false;
    if (this.mLeft != paramInt1 || this.mRight != paramInt3 || this.mTop != paramInt2 || this.mBottom != paramInt4) {
      boolean bool1 = true;
      int i = this.mPrivateFlags;
      int j = this.mRight - this.mLeft;
      int k = this.mBottom - this.mTop;
      int m = paramInt3 - paramInt1;
      int n = paramInt4 - paramInt2;
      if (m != j || n != k) {
        bool = true;
      } else {
        bool = false;
      } 
      invalidate(bool);
      this.mLeft = paramInt1;
      this.mTop = paramInt2;
      this.mRight = paramInt3;
      this.mBottom = paramInt4;
      this.mRenderNode.setLeftTopRightBottom(paramInt1, paramInt2, paramInt3, paramInt4);
      this.mPrivateFlags |= 0x10;
      if (bool)
        sizeChange(m, n, j, k); 
      if ((this.mViewFlags & 0xC) == 0 || this.mGhostView != null) {
        this.mPrivateFlags |= 0x20;
        invalidate(bool);
        invalidateParentCaches();
      } 
      this.mPrivateFlags |= i & 0x20;
      this.mBackgroundSizeChanged = true;
      this.mDefaultFocusHighlightSizeChanged = true;
      ForegroundInfo foregroundInfo = this.mForegroundInfo;
      if (foregroundInfo != null)
        ForegroundInfo.access$2202(foregroundInfo, true); 
      notifySubtreeAccessibilityStateChangedIfNeeded();
      bool = bool1;
    } 
    return bool;
  }
  
  public final void setLeftTopRightBottom(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  private void sizeChange(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: iload_2
    //   3: iload_3
    //   4: iload #4
    //   6: invokevirtual onSizeChanged : (IIII)V
    //   9: aload_0
    //   10: getfield mOverlay : Landroid/view/ViewOverlay;
    //   13: astore #5
    //   15: aload #5
    //   17: ifnull -> 40
    //   20: aload #5
    //   22: invokevirtual getOverlayView : ()Landroid/view/ViewGroup;
    //   25: iload_1
    //   26: invokevirtual setRight : (I)V
    //   29: aload_0
    //   30: getfield mOverlay : Landroid/view/ViewOverlay;
    //   33: invokevirtual getOverlayView : ()Landroid/view/ViewGroup;
    //   36: iload_2
    //   37: invokevirtual setBottom : (I)V
    //   40: getstatic android/view/View.sCanFocusZeroSized : Z
    //   43: ifne -> 166
    //   46: aload_0
    //   47: invokevirtual isLayoutValid : ()Z
    //   50: ifeq -> 166
    //   53: aload_0
    //   54: getfield mParent : Landroid/view/ViewParent;
    //   57: astore #5
    //   59: aload #5
    //   61: instanceof android/view/ViewGroup
    //   64: ifeq -> 82
    //   67: aload #5
    //   69: checkcast android/view/ViewGroup
    //   72: astore #5
    //   74: aload #5
    //   76: invokevirtual isLayoutSuppressed : ()Z
    //   79: ifne -> 166
    //   82: iload_1
    //   83: ifle -> 129
    //   86: iload_2
    //   87: ifgt -> 93
    //   90: goto -> 129
    //   93: iload_3
    //   94: ifle -> 102
    //   97: iload #4
    //   99: ifgt -> 166
    //   102: aload_0
    //   103: getfield mParent : Landroid/view/ViewParent;
    //   106: ifnull -> 166
    //   109: aload_0
    //   110: invokespecial canTakeFocus : ()Z
    //   113: ifeq -> 166
    //   116: aload_0
    //   117: getfield mParent : Landroid/view/ViewParent;
    //   120: aload_0
    //   121: invokeinterface focusableViewAvailable : (Landroid/view/View;)V
    //   126: goto -> 166
    //   129: aload_0
    //   130: invokevirtual hasFocus : ()Z
    //   133: ifeq -> 162
    //   136: aload_0
    //   137: invokevirtual clearFocus : ()V
    //   140: aload_0
    //   141: getfield mParent : Landroid/view/ViewParent;
    //   144: astore #5
    //   146: aload #5
    //   148: instanceof android/view/ViewGroup
    //   151: ifeq -> 162
    //   154: aload #5
    //   156: checkcast android/view/ViewGroup
    //   159: invokevirtual clearFocusedInCluster : ()V
    //   162: aload_0
    //   163: invokevirtual clearAccessibilityFocus : ()V
    //   166: aload_0
    //   167: invokespecial rebuildOutline : ()V
    //   170: getstatic com/oplus/darkmode/IOplusDarkModeManager.DEFAULT : Lcom/oplus/darkmode/IOplusDarkModeManager;
    //   173: iconst_0
    //   174: anewarray java/lang/Object
    //   177: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   180: checkcast com/oplus/darkmode/IOplusDarkModeManager
    //   183: aload_0
    //   184: invokeinterface markViewTypeBySize : (Landroid/view/View;)V
    //   189: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #23337	-> 0
    //   #23338	-> 9
    //   #23339	-> 20
    //   #23340	-> 29
    //   #23344	-> 40
    //   #23346	-> 74
    //   #23347	-> 82
    //   #23355	-> 93
    //   #23356	-> 102
    //   #23357	-> 116
    //   #23348	-> 129
    //   #23349	-> 136
    //   #23350	-> 140
    //   #23351	-> 154
    //   #23354	-> 162
    //   #23361	-> 166
    //   #23364	-> 170
    //   #23366	-> 189
  }
  
  protected void onFinishInflate() {}
  
  public Resources getResources() {
    return this.mResources;
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    if (verifyDrawable(paramDrawable)) {
      Rect rect = paramDrawable.getDirtyBounds();
      int i = this.mScrollX;
      int j = this.mScrollY;
      invalidate(rect.left + i, rect.top + j, rect.right + i, rect.bottom + j);
      rebuildOutline();
    } 
  }
  
  public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong) {
    if (verifyDrawable(paramDrawable) && paramRunnable != null) {
      paramLong -= SystemClock.uptimeMillis();
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null) {
        Choreographer choreographer = attachInfo.mViewRootImpl.mChoreographer;
        paramLong = Choreographer.subtractFrameDelay(paramLong);
        choreographer.postCallbackDelayed(1, paramRunnable, paramDrawable, paramLong);
      } else {
        getRunQueue().postDelayed(paramRunnable, paramLong);
      } 
    } 
  }
  
  public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable) {
    if (verifyDrawable(paramDrawable) && paramRunnable != null) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null)
        attachInfo.mViewRootImpl.mChoreographer.removeCallbacks(1, paramRunnable, paramDrawable); 
      getRunQueue().removeCallbacks(paramRunnable);
    } 
  }
  
  public void unscheduleDrawable(Drawable paramDrawable) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null && paramDrawable != null)
      attachInfo.mViewRootImpl.mChoreographer.removeCallbacks(1, null, paramDrawable); 
  }
  
  protected void resolveDrawables() {
    int i;
    if (!isLayoutDirectionResolved() && getRawLayoutDirection() == 2)
      return; 
    if (isLayoutDirectionResolved()) {
      i = getLayoutDirection();
    } else {
      i = getRawLayoutDirection();
    } 
    Drawable drawable2 = this.mBackground;
    if (drawable2 != null)
      drawable2.setLayoutDirection(i); 
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null && foregroundInfo.mDrawable != null)
      this.mForegroundInfo.mDrawable.setLayoutDirection(i); 
    Drawable drawable1 = this.mDefaultFocusHighlight;
    if (drawable1 != null)
      drawable1.setLayoutDirection(i); 
    this.mPrivateFlags2 |= 0x40000000;
    onResolveDrawables(i);
  }
  
  boolean areDrawablesResolved() {
    boolean bool;
    if ((this.mPrivateFlags2 & 0x40000000) == 1073741824) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onResolveDrawables(int paramInt) {}
  
  protected void resetResolvedDrawables() {
    resetResolvedDrawablesInternal();
  }
  
  void resetResolvedDrawablesInternal() {
    this.mPrivateFlags2 &= 0xBFFFFFFF;
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    if (paramDrawable != this.mBackground) {
      ForegroundInfo foregroundInfo = this.mForegroundInfo;
      return ((foregroundInfo != null && foregroundInfo.mDrawable == paramDrawable) || this.mDefaultFocusHighlight == paramDrawable);
    } 
    return true;
  }
  
  protected void drawableStateChanged() {
    int[] arrayOfInt = getDrawableState();
    int i = 0;
    Drawable drawable = this.mBackground;
    int j = i;
    if (drawable != null) {
      j = i;
      if (drawable.isStateful())
        j = false | drawable.setState(arrayOfInt); 
    } 
    drawable = this.mDefaultFocusHighlight;
    i = j;
    if (drawable != null) {
      i = j;
      if (drawable.isStateful())
        bool1 = j | drawable.setState(arrayOfInt); 
    } 
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null) {
      Drawable drawable1 = foregroundInfo.mDrawable;
    } else {
      foregroundInfo = null;
    } 
    boolean bool2 = bool1;
    if (foregroundInfo != null) {
      bool2 = bool1;
      if (foregroundInfo.isStateful())
        bool2 = bool1 | foregroundInfo.setState(arrayOfInt); 
    } 
    ScrollabilityCache scrollabilityCache = this.mScrollCache;
    boolean bool1 = bool2;
    if (scrollabilityCache != null) {
      ScrollBarDrawable scrollBarDrawable = scrollabilityCache.scrollBar;
      bool1 = bool2;
      if (scrollBarDrawable != null) {
        bool1 = bool2;
        if (scrollBarDrawable.isStateful()) {
          if (scrollBarDrawable.setState(arrayOfInt) && this.mScrollCache.state != 0) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          bool1 = bool2 | bool1;
        } 
      } 
    } 
    StateListAnimator stateListAnimator = this.mStateListAnimator;
    if (stateListAnimator != null)
      stateListAnimator.setState(arrayOfInt); 
    if (bool1)
      invalidate(); 
  }
  
  public void drawableHotspotChanged(float paramFloat1, float paramFloat2) {
    Drawable drawable = this.mBackground;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
    drawable = this.mDefaultFocusHighlight;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null && foregroundInfo.mDrawable != null)
      this.mForegroundInfo.mDrawable.setHotspot(paramFloat1, paramFloat2); 
    dispatchDrawableHotspotChanged(paramFloat1, paramFloat2);
  }
  
  public void dispatchDrawableHotspotChanged(float paramFloat1, float paramFloat2) {}
  
  public void refreshDrawableState() {
    this.mPrivateFlags |= 0x400;
    drawableStateChanged();
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      viewParent.childDrawableStateChanged(this); 
  }
  
  private Drawable getDefaultFocusHighlightDrawable() {
    if (this.mDefaultFocusHighlightCache == null) {
      Context context = this.mContext;
      if (context != null) {
        TypedArray typedArray = context.obtainStyledAttributes(new int[] { 16843534 });
        this.mDefaultFocusHighlightCache = typedArray.getDrawable(0);
        typedArray.recycle();
      } 
    } 
    return this.mDefaultFocusHighlightCache;
  }
  
  private void setDefaultFocusHighlight(Drawable paramDrawable) {
    this.mDefaultFocusHighlight = paramDrawable;
    boolean bool = true;
    this.mDefaultFocusHighlightSizeChanged = true;
    if (paramDrawable != null) {
      int i = this.mPrivateFlags;
      if ((i & 0x80) != 0)
        this.mPrivateFlags = i & 0xFFFFFF7F; 
      paramDrawable.setLayoutDirection(getLayoutDirection());
      if (paramDrawable.isStateful())
        paramDrawable.setState(getDrawableState()); 
      if (isAttachedToWindow()) {
        if (getWindowVisibility() != 0 || !isShown())
          bool = false; 
        paramDrawable.setVisible(bool, false);
      } 
      paramDrawable.setCallback(this);
    } else if ((this.mViewFlags & 0x80) != 0 && this.mBackground == null) {
      ForegroundInfo foregroundInfo = this.mForegroundInfo;
      if (foregroundInfo == null || foregroundInfo.mDrawable == null)
        this.mPrivateFlags |= 0x80; 
    } 
    invalidate();
  }
  
  public boolean isDefaultFocusHighlightNeeded(Drawable paramDrawable1, Drawable paramDrawable2) {
    boolean bool2, bool1 = false;
    if ((paramDrawable1 == null || !paramDrawable1.isStateful() || !paramDrawable1.hasFocusStateSpecified()) && (paramDrawable2 == null || !paramDrawable2.isStateful() || !paramDrawable2.hasFocusStateSpecified())) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (!isInTouchMode() && getDefaultFocusHighlightEnabled() && bool2 && isAttachedToWindow() && sUseDefaultFocusHighlight)
      bool1 = true; 
    return bool1;
  }
  
  private void switchDefaultFocusHighlight() {
    if (isFocused()) {
      Drawable drawable2;
      boolean bool1;
      Drawable drawable1 = this.mBackground;
      ForegroundInfo foregroundInfo = this.mForegroundInfo;
      if (foregroundInfo == null) {
        foregroundInfo = null;
      } else {
        drawable2 = foregroundInfo.mDrawable;
      } 
      boolean bool = isDefaultFocusHighlightNeeded(drawable1, drawable2);
      if (this.mDefaultFocusHighlight != null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (bool && !bool1) {
        setDefaultFocusHighlight(getDefaultFocusHighlightDrawable());
      } else if (!bool && bool1) {
        setDefaultFocusHighlight((Drawable)null);
      } 
    } 
  }
  
  private void drawDefaultFocusHighlight(Canvas paramCanvas) {
    if (this.mDefaultFocusHighlight != null && isFocused()) {
      if (this.mDefaultFocusHighlightSizeChanged) {
        this.mDefaultFocusHighlightSizeChanged = false;
        int i = this.mScrollX;
        int j = this.mRight, k = this.mLeft;
        int m = this.mScrollY;
        int n = this.mBottom, i1 = this.mTop;
        this.mDefaultFocusHighlight.setBounds(i, m, j + i - k, n + m - i1);
      } 
      this.mDefaultFocusHighlight.draw(paramCanvas);
    } 
  }
  
  public final int[] getDrawableState() {
    int[] arrayOfInt = this.mDrawableState;
    if (arrayOfInt != null && (this.mPrivateFlags & 0x400) == 0)
      return arrayOfInt; 
    this.mDrawableState = arrayOfInt = onCreateDrawableState(0);
    this.mPrivateFlags &= 0xFFFFFBFF;
    return arrayOfInt;
  }
  
  protected int[] onCreateDrawableState(int paramInt) {
    int[] arrayOfInt1;
    if ((this.mViewFlags & 0x400000) == 4194304) {
      ViewParent viewParent = this.mParent;
      if (viewParent instanceof View)
        return ((View)viewParent).onCreateDrawableState(paramInt); 
    } 
    int i = this.mPrivateFlags;
    int j = 0;
    if ((i & 0x4000) != 0)
      j = 0x0 | 0x10; 
    int k = j;
    if ((this.mViewFlags & 0x20) == 0)
      k = j | 0x8; 
    j = k;
    if (isFocused())
      j = k | 0x4; 
    k = j;
    if ((i & 0x4) != 0)
      k = j | 0x2; 
    int m = k;
    if (hasWindowFocus())
      m = k | 0x1; 
    j = m;
    if ((0x40000000 & i) != 0)
      j = m | 0x20; 
    AttachInfo attachInfo = this.mAttachInfo;
    k = j;
    if (attachInfo != null) {
      k = j;
      if (attachInfo.mHardwareAccelerationRequested) {
        k = j;
        if (ThreadedRenderer.isAvailable())
          k = j | 0x40; 
      } 
    } 
    j = k;
    if ((0x10000000 & i) != 0)
      j = k | 0x80; 
    m = this.mPrivateFlags2;
    k = j;
    if ((m & 0x1) != 0)
      k = j | 0x100; 
    j = k;
    if ((m & 0x2) != 0)
      j = k | 0x200; 
    int[] arrayOfInt2 = StateSet.get(j);
    if (paramInt == 0)
      return arrayOfInt2; 
    if (arrayOfInt2 != null) {
      arrayOfInt1 = new int[arrayOfInt2.length + paramInt];
      System.arraycopy(arrayOfInt2, 0, arrayOfInt1, 0, arrayOfInt2.length);
    } else {
      arrayOfInt1 = new int[paramInt];
    } 
    return arrayOfInt1;
  }
  
  protected static int[] mergeDrawableStates(int[] paramArrayOfint1, int[] paramArrayOfint2) {
    int i = paramArrayOfint1.length;
    i--;
    while (i >= 0 && paramArrayOfint1[i] == 0)
      i--; 
    System.arraycopy(paramArrayOfint2, 0, paramArrayOfint1, i + 1, paramArrayOfint2.length);
    return paramArrayOfint1;
  }
  
  public void jumpDrawablesToCurrentState() {
    Drawable drawable2 = this.mBackground;
    if (drawable2 != null)
      drawable2.jumpToCurrentState(); 
    StateListAnimator stateListAnimator = this.mStateListAnimator;
    if (stateListAnimator != null)
      stateListAnimator.jumpToCurrentState(); 
    Drawable drawable1 = this.mDefaultFocusHighlight;
    if (drawable1 != null)
      drawable1.jumpToCurrentState(); 
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null && foregroundInfo.mDrawable != null)
      this.mForegroundInfo.mDrawable.jumpToCurrentState(); 
  }
  
  @RemotableViewMethod
  public void setBackgroundColor(int paramInt) {
    Drawable drawable = this.mBackground;
    if (drawable instanceof ColorDrawable) {
      ((ColorDrawable)drawable.mutate()).setColor(paramInt);
      computeOpaqueFlags();
      this.mBackgroundResource = 0;
    } else {
      setBackground((Drawable)new ColorDrawable(paramInt));
    } 
  }
  
  @RemotableViewMethod
  public void setBackgroundResource(int paramInt) {
    if (paramInt != 0 && paramInt == this.mBackgroundResource)
      return; 
    Drawable drawable = null;
    if (paramInt != 0)
      drawable = this.mContext.getDrawable(paramInt); 
    setBackground(drawable);
    this.mBackgroundResource = paramInt;
  }
  
  public void setBackground(Drawable paramDrawable) {
    setBackgroundDrawable(paramDrawable);
  }
  
  @Deprecated
  public void setBackgroundDrawable(Drawable paramDrawable) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual computeOpaqueFlags : ()V
    //   4: aload_0
    //   5: getfield mBackground : Landroid/graphics/drawable/Drawable;
    //   8: astore_2
    //   9: aload_1
    //   10: aload_2
    //   11: if_acmpne -> 15
    //   14: return
    //   15: iconst_0
    //   16: istore_3
    //   17: aload_0
    //   18: iconst_0
    //   19: putfield mBackgroundResource : I
    //   22: aload_2
    //   23: ifnull -> 59
    //   26: aload_0
    //   27: invokevirtual isAttachedToWindow : ()Z
    //   30: ifeq -> 43
    //   33: aload_0
    //   34: getfield mBackground : Landroid/graphics/drawable/Drawable;
    //   37: iconst_0
    //   38: iconst_0
    //   39: invokevirtual setVisible : (ZZ)Z
    //   42: pop
    //   43: aload_0
    //   44: getfield mBackground : Landroid/graphics/drawable/Drawable;
    //   47: aconst_null
    //   48: invokevirtual setCallback : (Landroid/graphics/drawable/Drawable$Callback;)V
    //   51: aload_0
    //   52: aload_0
    //   53: getfield mBackground : Landroid/graphics/drawable/Drawable;
    //   56: invokevirtual unscheduleDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   59: aload_1
    //   60: ifnull -> 351
    //   63: getstatic android/view/View.sThreadLocal : Ljava/lang/ThreadLocal;
    //   66: invokevirtual get : ()Ljava/lang/Object;
    //   69: checkcast android/graphics/Rect
    //   72: astore #4
    //   74: aload #4
    //   76: astore_2
    //   77: aload #4
    //   79: ifnonnull -> 97
    //   82: new android/graphics/Rect
    //   85: dup
    //   86: invokespecial <init> : ()V
    //   89: astore_2
    //   90: getstatic android/view/View.sThreadLocal : Ljava/lang/ThreadLocal;
    //   93: aload_2
    //   94: invokevirtual set : (Ljava/lang/Object;)V
    //   97: aload_0
    //   98: invokevirtual resetResolvedDrawablesInternal : ()V
    //   101: aload_1
    //   102: aload_0
    //   103: invokevirtual getLayoutDirection : ()I
    //   106: invokevirtual setLayoutDirection : (I)Z
    //   109: pop
    //   110: aload_1
    //   111: aload_2
    //   112: invokevirtual getPadding : (Landroid/graphics/Rect;)Z
    //   115: ifeq -> 215
    //   118: aload_0
    //   119: invokevirtual resetResolvedPaddingInternal : ()V
    //   122: aload_1
    //   123: invokevirtual getLayoutDirection : ()I
    //   126: iconst_1
    //   127: if_icmpeq -> 169
    //   130: aload_0
    //   131: aload_2
    //   132: getfield left : I
    //   135: putfield mUserPaddingLeftInitial : I
    //   138: aload_0
    //   139: aload_2
    //   140: getfield right : I
    //   143: putfield mUserPaddingRightInitial : I
    //   146: aload_0
    //   147: aload_2
    //   148: getfield left : I
    //   151: aload_2
    //   152: getfield top : I
    //   155: aload_2
    //   156: getfield right : I
    //   159: aload_2
    //   160: getfield bottom : I
    //   163: invokevirtual internalSetPadding : (IIII)V
    //   166: goto -> 205
    //   169: aload_0
    //   170: aload_2
    //   171: getfield right : I
    //   174: putfield mUserPaddingLeftInitial : I
    //   177: aload_0
    //   178: aload_2
    //   179: getfield left : I
    //   182: putfield mUserPaddingRightInitial : I
    //   185: aload_0
    //   186: aload_2
    //   187: getfield right : I
    //   190: aload_2
    //   191: getfield top : I
    //   194: aload_2
    //   195: getfield left : I
    //   198: aload_2
    //   199: getfield bottom : I
    //   202: invokevirtual internalSetPadding : (IIII)V
    //   205: aload_0
    //   206: iconst_0
    //   207: putfield mLeftPaddingDefined : Z
    //   210: aload_0
    //   211: iconst_0
    //   212: putfield mRightPaddingDefined : Z
    //   215: aload_0
    //   216: getfield mBackground : Landroid/graphics/drawable/Drawable;
    //   219: astore_2
    //   220: aload_2
    //   221: ifnull -> 251
    //   224: aload_2
    //   225: invokevirtual getMinimumHeight : ()I
    //   228: aload_1
    //   229: invokevirtual getMinimumHeight : ()I
    //   232: if_icmpne -> 251
    //   235: aload_0
    //   236: getfield mBackground : Landroid/graphics/drawable/Drawable;
    //   239: astore_2
    //   240: aload_2
    //   241: invokevirtual getMinimumWidth : ()I
    //   244: aload_1
    //   245: invokevirtual getMinimumWidth : ()I
    //   248: if_icmpeq -> 253
    //   251: iconst_1
    //   252: istore_3
    //   253: aload_0
    //   254: aload_1
    //   255: putfield mBackground : Landroid/graphics/drawable/Drawable;
    //   258: aload_1
    //   259: invokevirtual isStateful : ()Z
    //   262: ifeq -> 274
    //   265: aload_1
    //   266: aload_0
    //   267: invokevirtual getDrawableState : ()[I
    //   270: invokevirtual setState : ([I)Z
    //   273: pop
    //   274: aload_0
    //   275: invokevirtual isAttachedToWindow : ()Z
    //   278: ifeq -> 312
    //   281: aload_0
    //   282: invokevirtual getWindowVisibility : ()I
    //   285: ifne -> 301
    //   288: aload_0
    //   289: invokevirtual isShown : ()Z
    //   292: ifeq -> 301
    //   295: iconst_1
    //   296: istore #5
    //   298: goto -> 304
    //   301: iconst_0
    //   302: istore #5
    //   304: aload_1
    //   305: iload #5
    //   307: iconst_0
    //   308: invokevirtual setVisible : (ZZ)Z
    //   311: pop
    //   312: aload_0
    //   313: invokespecial applyBackgroundTint : ()V
    //   316: aload_1
    //   317: aload_0
    //   318: invokevirtual setCallback : (Landroid/graphics/drawable/Drawable$Callback;)V
    //   321: aload_0
    //   322: getfield mPrivateFlags : I
    //   325: istore #6
    //   327: iload #6
    //   329: sipush #128
    //   332: iand
    //   333: ifeq -> 348
    //   336: aload_0
    //   337: iload #6
    //   339: sipush #-129
    //   342: iand
    //   343: putfield mPrivateFlags : I
    //   346: iconst_1
    //   347: istore_3
    //   348: goto -> 404
    //   351: aload_0
    //   352: aconst_null
    //   353: putfield mBackground : Landroid/graphics/drawable/Drawable;
    //   356: aload_0
    //   357: getfield mViewFlags : I
    //   360: sipush #128
    //   363: iand
    //   364: ifeq -> 402
    //   367: aload_0
    //   368: getfield mDefaultFocusHighlight : Landroid/graphics/drawable/Drawable;
    //   371: ifnonnull -> 402
    //   374: aload_0
    //   375: getfield mForegroundInfo : Landroid/view/View$ForegroundInfo;
    //   378: astore_1
    //   379: aload_1
    //   380: ifnull -> 390
    //   383: aload_1
    //   384: invokestatic access$1600 : (Landroid/view/View$ForegroundInfo;)Landroid/graphics/drawable/Drawable;
    //   387: ifnonnull -> 402
    //   390: aload_0
    //   391: aload_0
    //   392: getfield mPrivateFlags : I
    //   395: sipush #128
    //   398: ior
    //   399: putfield mPrivateFlags : I
    //   402: iconst_1
    //   403: istore_3
    //   404: aload_0
    //   405: invokevirtual computeOpaqueFlags : ()V
    //   408: iload_3
    //   409: ifeq -> 416
    //   412: aload_0
    //   413: invokevirtual requestLayout : ()V
    //   416: aload_0
    //   417: iconst_1
    //   418: putfield mBackgroundSizeChanged : Z
    //   421: aload_0
    //   422: iconst_1
    //   423: invokevirtual invalidate : (Z)V
    //   426: aload_0
    //   427: invokevirtual invalidateOutline : ()V
    //   430: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #23972	-> 0
    //   #23974	-> 4
    //   #23975	-> 14
    //   #23978	-> 15
    //   #23980	-> 17
    //   #23986	-> 22
    //   #23987	-> 26
    //   #23988	-> 33
    //   #23990	-> 43
    //   #23991	-> 51
    //   #23994	-> 59
    //   #23995	-> 63
    //   #23996	-> 74
    //   #23997	-> 82
    //   #23998	-> 90
    //   #24000	-> 97
    //   #24001	-> 101
    //   #24002	-> 110
    //   #24003	-> 118
    //   #24004	-> 122
    //   #24012	-> 130
    //   #24013	-> 138
    //   #24014	-> 146
    //   #24006	-> 169
    //   #24007	-> 177
    //   #24008	-> 185
    //   #24009	-> 205
    //   #24016	-> 205
    //   #24017	-> 210
    //   #24022	-> 215
    //   #24023	-> 224
    //   #24024	-> 240
    //   #24025	-> 251
    //   #24033	-> 253
    //   #24034	-> 258
    //   #24035	-> 265
    //   #24037	-> 274
    //   #24038	-> 281
    //   #24041	-> 312
    //   #24044	-> 316
    //   #24046	-> 321
    //   #24047	-> 336
    //   #24048	-> 346
    //   #24050	-> 348
    //   #24052	-> 351
    //   #24053	-> 356
    //   #24055	-> 383
    //   #24056	-> 390
    //   #24068	-> 402
    //   #24071	-> 404
    //   #24073	-> 408
    //   #24074	-> 412
    //   #24077	-> 416
    //   #24078	-> 421
    //   #24079	-> 426
    //   #24080	-> 430
  }
  
  public Drawable getBackground() {
    return this.mBackground;
  }
  
  public void setBackgroundTintList(ColorStateList paramColorStateList) {
    if (this.mBackgroundTint == null)
      this.mBackgroundTint = new TintInfo(); 
    this.mBackgroundTint.mTintList = paramColorStateList;
    this.mBackgroundTint.mHasTintList = true;
    applyBackgroundTint();
  }
  
  public ColorStateList getBackgroundTintList() {
    TintInfo tintInfo = this.mBackgroundTint;
    if (tintInfo != null) {
      ColorStateList colorStateList = tintInfo.mTintList;
    } else {
      tintInfo = null;
    } 
    return (ColorStateList)tintInfo;
  }
  
  public void setBackgroundTintMode(PorterDuff.Mode paramMode) {
    BlendMode blendMode = null;
    if (paramMode != null)
      blendMode = BlendMode.fromValue(paramMode.nativeInt); 
    setBackgroundTintBlendMode(blendMode);
  }
  
  public void setBackgroundTintBlendMode(BlendMode paramBlendMode) {
    if (this.mBackgroundTint == null)
      this.mBackgroundTint = new TintInfo(); 
    this.mBackgroundTint.mBlendMode = paramBlendMode;
    this.mBackgroundTint.mHasTintMode = true;
    applyBackgroundTint();
  }
  
  public PorterDuff.Mode getBackgroundTintMode() {
    TintInfo tintInfo = this.mBackgroundTint;
    if (tintInfo != null && tintInfo.mBlendMode != null) {
      PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(this.mBackgroundTint.mBlendMode);
    } else {
      tintInfo = null;
    } 
    return (PorterDuff.Mode)tintInfo;
  }
  
  public BlendMode getBackgroundTintBlendMode() {
    TintInfo tintInfo = this.mBackgroundTint;
    if (tintInfo != null) {
      BlendMode blendMode = tintInfo.mBlendMode;
    } else {
      tintInfo = null;
    } 
    return (BlendMode)tintInfo;
  }
  
  private void applyBackgroundTint() {
    if (this.mBackground != null && this.mBackgroundTint != null) {
      TintInfo tintInfo = this.mBackgroundTint;
      if (tintInfo.mHasTintList || tintInfo.mHasTintMode) {
        this.mBackground = this.mBackground.mutate();
        if (tintInfo.mHasTintList)
          this.mBackground.setTintList(tintInfo.mTintList); 
        if (tintInfo.mHasTintMode)
          this.mBackground.setTintBlendMode(tintInfo.mBlendMode); 
        if (this.mBackground.isStateful())
          this.mBackground.setState(getDrawableState()); 
      } 
    } 
  }
  
  public Drawable getForeground() {
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null) {
      Drawable drawable = foregroundInfo.mDrawable;
    } else {
      foregroundInfo = null;
    } 
    return (Drawable)foregroundInfo;
  }
  
  public void setForeground(Drawable paramDrawable) {
    if (this.mForegroundInfo == null) {
      if (paramDrawable == null)
        return; 
      this.mForegroundInfo = new ForegroundInfo();
    } 
    if (paramDrawable == this.mForegroundInfo.mDrawable)
      return; 
    if (this.mForegroundInfo.mDrawable != null) {
      if (isAttachedToWindow())
        this.mForegroundInfo.mDrawable.setVisible(false, false); 
      this.mForegroundInfo.mDrawable.setCallback(null);
      unscheduleDrawable(this.mForegroundInfo.mDrawable);
    } 
    ForegroundInfo.access$1602(this.mForegroundInfo, paramDrawable);
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    boolean bool = true;
    ForegroundInfo.access$2202(foregroundInfo, true);
    if (paramDrawable != null) {
      int i = this.mPrivateFlags;
      if ((i & 0x80) != 0)
        this.mPrivateFlags = i & 0xFFFFFF7F; 
      paramDrawable.setLayoutDirection(getLayoutDirection());
      if (paramDrawable.isStateful())
        paramDrawable.setState(getDrawableState()); 
      applyForegroundTint();
      if (isAttachedToWindow()) {
        if (getWindowVisibility() != 0 || !isShown())
          bool = false; 
        paramDrawable.setVisible(bool, false);
      } 
      paramDrawable.setCallback(this);
    } else if ((this.mViewFlags & 0x80) != 0 && this.mBackground == null && this.mDefaultFocusHighlight == null) {
      this.mPrivateFlags |= 0x80;
    } 
    requestLayout();
    invalidate();
  }
  
  public boolean isForegroundInsidePadding() {
    boolean bool;
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null) {
      bool = foregroundInfo.mInsidePadding;
    } else {
      bool = true;
    } 
    return bool;
  }
  
  public int getForegroundGravity() {
    int i;
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null) {
      i = foregroundInfo.mGravity;
    } else {
      i = 8388659;
    } 
    return i;
  }
  
  public void setForegroundGravity(int paramInt) {
    if (this.mForegroundInfo == null)
      this.mForegroundInfo = new ForegroundInfo(); 
    if (this.mForegroundInfo.mGravity != paramInt) {
      int i = paramInt;
      if ((0x800007 & paramInt) == 0)
        i = paramInt | 0x800003; 
      paramInt = i;
      if ((i & 0x70) == 0)
        paramInt = i | 0x30; 
      ForegroundInfo.access$2602(this.mForegroundInfo, paramInt);
      requestLayout();
    } 
  }
  
  public void setForegroundTintList(ColorStateList paramColorStateList) {
    if (this.mForegroundInfo == null)
      this.mForegroundInfo = new ForegroundInfo(); 
    if (this.mForegroundInfo.mTintInfo == null)
      ForegroundInfo.access$2702(this.mForegroundInfo, new TintInfo()); 
    this.mForegroundInfo.mTintInfo.mTintList = paramColorStateList;
    this.mForegroundInfo.mTintInfo.mHasTintList = true;
    applyForegroundTint();
  }
  
  public ColorStateList getForegroundTintList() {
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null && foregroundInfo.mTintInfo != null) {
      ColorStateList colorStateList = this.mForegroundInfo.mTintInfo.mTintList;
    } else {
      foregroundInfo = null;
    } 
    return (ColorStateList)foregroundInfo;
  }
  
  public void setForegroundTintMode(PorterDuff.Mode paramMode) {
    BlendMode blendMode = null;
    if (paramMode != null)
      blendMode = BlendMode.fromValue(paramMode.nativeInt); 
    setForegroundTintBlendMode(blendMode);
  }
  
  public void setForegroundTintBlendMode(BlendMode paramBlendMode) {
    if (this.mForegroundInfo == null)
      this.mForegroundInfo = new ForegroundInfo(); 
    if (this.mForegroundInfo.mTintInfo == null)
      ForegroundInfo.access$2702(this.mForegroundInfo, new TintInfo()); 
    this.mForegroundInfo.mTintInfo.mBlendMode = paramBlendMode;
    this.mForegroundInfo.mTintInfo.mHasTintMode = true;
    applyForegroundTint();
  }
  
  public PorterDuff.Mode getForegroundTintMode() {
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null && foregroundInfo.mTintInfo != null) {
      BlendMode blendMode = this.mForegroundInfo.mTintInfo.mBlendMode;
    } else {
      foregroundInfo = null;
    } 
    if (foregroundInfo != null)
      return BlendMode.blendModeToPorterDuffMode((BlendMode)foregroundInfo); 
    return null;
  }
  
  public BlendMode getForegroundTintBlendMode() {
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null && foregroundInfo.mTintInfo != null) {
      BlendMode blendMode = this.mForegroundInfo.mTintInfo.mBlendMode;
    } else {
      foregroundInfo = null;
    } 
    return (BlendMode)foregroundInfo;
  }
  
  private void applyForegroundTint() {
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null && foregroundInfo.mDrawable != null) {
      foregroundInfo = this.mForegroundInfo;
      if (foregroundInfo.mTintInfo != null) {
        TintInfo tintInfo = this.mForegroundInfo.mTintInfo;
        if (tintInfo.mHasTintList || tintInfo.mHasTintMode) {
          ForegroundInfo foregroundInfo1 = this.mForegroundInfo;
          ForegroundInfo.access$1602(foregroundInfo1, foregroundInfo1.mDrawable.mutate());
          if (tintInfo.mHasTintList)
            this.mForegroundInfo.mDrawable.setTintList(tintInfo.mTintList); 
          if (tintInfo.mHasTintMode)
            this.mForegroundInfo.mDrawable.setTintBlendMode(tintInfo.mBlendMode); 
          if (this.mForegroundInfo.mDrawable.isStateful())
            this.mForegroundInfo.mDrawable.setState(getDrawableState()); 
        } 
      } 
    } 
  }
  
  private Drawable getAutofilledDrawable() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return null; 
    if (attachInfo.mAutofilledDrawable == null) {
      Context context = getRootView().getContext();
      TypedArray typedArray = context.getTheme().obtainStyledAttributes(AUTOFILL_HIGHLIGHT_ATTR);
      int i = typedArray.getResourceId(0, 0);
      this.mAttachInfo.mAutofilledDrawable = context.getDrawable(i);
      typedArray.recycle();
    } 
    return this.mAttachInfo.mAutofilledDrawable;
  }
  
  private void drawAutofilledHighlight(Canvas paramCanvas) {
    if (isAutofilled() && !hideAutofillHighlight()) {
      Drawable drawable = getAutofilledDrawable();
      if (drawable != null) {
        drawable.setBounds(0, 0, getWidth(), getHeight());
        drawable.draw(paramCanvas);
      } 
    } 
  }
  
  public void onDrawForeground(Canvas paramCanvas) {
    onDrawScrollIndicators(paramCanvas);
    onDrawScrollBars(paramCanvas);
    ForegroundInfo foregroundInfo = this.mForegroundInfo;
    if (foregroundInfo != null) {
      Drawable drawable = foregroundInfo.mDrawable;
    } else {
      foregroundInfo = null;
    } 
    if (foregroundInfo != null) {
      if (this.mForegroundInfo.mBoundsChanged) {
        ForegroundInfo.access$2202(this.mForegroundInfo, false);
        Rect rect1 = this.mForegroundInfo.mSelfBounds;
        Rect rect2 = this.mForegroundInfo.mOverlayBounds;
        if (this.mForegroundInfo.mInsidePadding) {
          rect1.set(0, 0, getWidth(), getHeight());
        } else {
          int n = getPaddingLeft(), i1 = getPaddingTop();
          int i2 = getWidth(), i3 = getPaddingRight(), i4 = getHeight(), i5 = getPaddingBottom();
          rect1.set(n, i1, i2 - i3, i4 - i5);
        } 
        int i = getLayoutDirection();
        int k = this.mForegroundInfo.mGravity, m = foregroundInfo.getIntrinsicWidth();
        int j = foregroundInfo.getIntrinsicHeight();
        Gravity.apply(k, m, j, rect1, rect2, i);
        foregroundInfo.setBounds(rect2);
      } 
      foregroundInfo.draw(paramCanvas);
    } 
  }
  
  public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    resetResolvedPaddingInternal();
    this.mUserPaddingStart = Integer.MIN_VALUE;
    this.mUserPaddingEnd = Integer.MIN_VALUE;
    this.mUserPaddingLeftInitial = paramInt1;
    this.mUserPaddingRightInitial = paramInt3;
    this.mLeftPaddingDefined = true;
    this.mRightPaddingDefined = true;
    internalSetPadding(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  protected void internalSetPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mUserPaddingLeft = paramInt1;
    this.mUserPaddingRight = paramInt3;
    this.mUserPaddingBottom = paramInt4;
    int i = this.mViewFlags;
    boolean bool = false;
    int j = paramInt1, k = paramInt3, m = paramInt4;
    if ((i & 0x300) != 0) {
      boolean bool1 = false;
      int n = paramInt1, i1 = paramInt3;
      if ((i & 0x200) != 0) {
        if ((i & 0x1000000) == 0) {
          n = 0;
        } else {
          n = getVerticalScrollbarWidth();
        } 
        i1 = this.mVerticalScrollbarPosition;
        if (i1 != 0) {
          if (i1 != 1) {
            if (i1 != 2) {
              n = paramInt1;
              i1 = paramInt3;
            } else {
              i1 = paramInt3 + n;
              n = paramInt1;
            } 
          } else {
            n = paramInt1 + n;
            i1 = paramInt3;
          } 
        } else if (isLayoutRtl()) {
          n = paramInt1 + n;
          i1 = paramInt3;
        } else {
          i1 = paramInt3 + n;
          n = paramInt1;
        } 
      } 
      j = n;
      k = i1;
      m = paramInt4;
      if ((i & 0x100) != 0) {
        if ((i & 0x1000000) == 0) {
          paramInt1 = bool1;
        } else {
          paramInt1 = getHorizontalScrollbarHeight();
        } 
        m = paramInt4 + paramInt1;
        k = i1;
        j = n;
      } 
    } 
    paramInt1 = bool;
    if (this.mPaddingLeft != j) {
      paramInt1 = 1;
      this.mPaddingLeft = j;
    } 
    if (this.mPaddingTop != paramInt2) {
      paramInt1 = 1;
      this.mPaddingTop = paramInt2;
    } 
    if (this.mPaddingRight != k) {
      paramInt1 = 1;
      this.mPaddingRight = k;
    } 
    if (this.mPaddingBottom != m) {
      paramInt1 = 1;
      this.mPaddingBottom = m;
    } 
    if (paramInt1 != 0) {
      requestLayout();
      invalidateOutline();
    } 
  }
  
  public void setPaddingRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    resetResolvedPaddingInternal();
    this.mUserPaddingStart = paramInt1;
    this.mUserPaddingEnd = paramInt3;
    this.mLeftPaddingDefined = true;
    this.mRightPaddingDefined = true;
    if (getLayoutDirection() != 1) {
      this.mUserPaddingLeftInitial = paramInt1;
      this.mUserPaddingRightInitial = paramInt3;
      internalSetPadding(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      this.mUserPaddingLeftInitial = paramInt3;
      this.mUserPaddingRightInitial = paramInt1;
      internalSetPadding(paramInt3, paramInt2, paramInt1, paramInt4);
    } 
  }
  
  public int getSourceLayoutResId() {
    return this.mSourceLayoutId;
  }
  
  public int getPaddingTop() {
    return this.mPaddingTop;
  }
  
  public int getPaddingBottom() {
    return this.mPaddingBottom;
  }
  
  public int getPaddingLeft() {
    if (!isPaddingResolved())
      resolvePadding(); 
    return this.mPaddingLeft;
  }
  
  public int getPaddingStart() {
    int i;
    if (!isPaddingResolved())
      resolvePadding(); 
    if (getLayoutDirection() == 1) {
      i = this.mPaddingRight;
    } else {
      i = this.mPaddingLeft;
    } 
    return i;
  }
  
  public int getPaddingRight() {
    if (!isPaddingResolved())
      resolvePadding(); 
    return this.mPaddingRight;
  }
  
  public int getPaddingEnd() {
    int i;
    if (!isPaddingResolved())
      resolvePadding(); 
    if (getLayoutDirection() == 1) {
      i = this.mPaddingLeft;
    } else {
      i = this.mPaddingRight;
    } 
    return i;
  }
  
  public boolean isPaddingRelative() {
    return (this.mUserPaddingStart != Integer.MIN_VALUE || this.mUserPaddingEnd != Integer.MIN_VALUE);
  }
  
  Insets computeOpticalInsets() {
    Insets insets;
    Drawable drawable = this.mBackground;
    if (drawable == null) {
      insets = Insets.NONE;
    } else {
      insets = insets.getOpticalInsets();
    } 
    return insets;
  }
  
  public void resetPaddingToInitialValues() {
    if (isRtlCompatibilityMode()) {
      this.mPaddingLeft = this.mUserPaddingLeftInitial;
      this.mPaddingRight = this.mUserPaddingRightInitial;
      return;
    } 
    if (isLayoutRtl()) {
      int i = this.mUserPaddingEnd;
      if (i < 0)
        i = this.mUserPaddingLeftInitial; 
      this.mPaddingLeft = i;
      i = this.mUserPaddingStart;
      if (i < 0)
        i = this.mUserPaddingRightInitial; 
      this.mPaddingRight = i;
    } else {
      int i = this.mUserPaddingStart;
      if (i < 0)
        i = this.mUserPaddingLeftInitial; 
      this.mPaddingLeft = i;
      i = this.mUserPaddingEnd;
      if (i < 0)
        i = this.mUserPaddingRightInitial; 
      this.mPaddingRight = i;
    } 
  }
  
  public Insets getOpticalInsets() {
    if (this.mLayoutInsets == null)
      this.mLayoutInsets = computeOpticalInsets(); 
    return this.mLayoutInsets;
  }
  
  public void setOpticalInsets(Insets paramInsets) {
    this.mLayoutInsets = paramInsets;
  }
  
  public void setSelected(boolean paramBoolean) {
    boolean bool;
    if ((this.mPrivateFlags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool != paramBoolean) {
      boolean bool1;
      int i = this.mPrivateFlags;
      if (paramBoolean) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      this.mPrivateFlags = i & 0xFFFFFFFB | bool1;
      if (!paramBoolean)
        resetPressedState(); 
      invalidate(true);
      refreshDrawableState();
      dispatchSetSelected(paramBoolean);
      if (paramBoolean) {
        sendAccessibilityEvent(4);
      } else {
        notifyViewAccessibilityStateChangedIfNeeded(0);
      } 
    } 
  }
  
  protected void dispatchSetSelected(boolean paramBoolean) {}
  
  @ExportedProperty
  public boolean isSelected() {
    boolean bool;
    if ((this.mPrivateFlags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setActivated(boolean paramBoolean) {
    boolean bool;
    int i = this.mPrivateFlags, j = 1073741824;
    if ((i & 0x40000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool != paramBoolean) {
      i = this.mPrivateFlags;
      if (!paramBoolean)
        j = 0; 
      this.mPrivateFlags = i & 0xBFFFFFFF | j;
      invalidate(true);
      refreshDrawableState();
      dispatchSetActivated(paramBoolean);
    } 
  }
  
  protected void dispatchSetActivated(boolean paramBoolean) {}
  
  @ExportedProperty
  public boolean isActivated() {
    boolean bool;
    if ((this.mPrivateFlags & 0x40000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public ViewTreeObserver getViewTreeObserver() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      return attachInfo.mTreeObserver; 
    if (this.mFloatingTreeObserver == null)
      this.mFloatingTreeObserver = new ViewTreeObserver(this.mContext); 
    return this.mFloatingTreeObserver;
  }
  
  public View getRootView() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      View view1 = attachInfo.mRootView;
      if (view1 != null)
        return view1; 
    } 
    View view = this;
    while (true) {
      ViewParent viewParent = view.mParent;
      if (viewParent != null && viewParent instanceof View) {
        view = (View)viewParent;
        continue;
      } 
      break;
    } 
    return view;
  }
  
  public boolean toGlobalMotionEvent(MotionEvent paramMotionEvent) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return false; 
    Matrix matrix = attachInfo.mTmpMatrix;
    matrix.set(Matrix.IDENTITY_MATRIX);
    transformMatrixToGlobal(matrix);
    paramMotionEvent.transform(matrix);
    return true;
  }
  
  public boolean toLocalMotionEvent(MotionEvent paramMotionEvent) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return false; 
    Matrix matrix = attachInfo.mTmpMatrix;
    matrix.set(Matrix.IDENTITY_MATRIX);
    transformMatrixToLocal(matrix);
    paramMotionEvent.transform(matrix);
    return true;
  }
  
  public void transformMatrixToGlobal(Matrix paramMatrix) {
    View view;
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof View) {
      view = (View)viewParent;
      view.transformMatrixToGlobal(paramMatrix);
      paramMatrix.preTranslate(-view.mScrollX, -view.mScrollY);
    } else if (view instanceof ViewRootImpl) {
      ViewRootImpl viewRootImpl = (ViewRootImpl)view;
      viewRootImpl.transformMatrixToGlobal(paramMatrix);
      paramMatrix.preTranslate(0.0F, -viewRootImpl.mCurScrollY);
    } 
    paramMatrix.preTranslate(this.mLeft, this.mTop);
    if (!hasIdentityMatrix())
      paramMatrix.preConcat(getMatrix()); 
  }
  
  public void transformMatrixToLocal(Matrix paramMatrix) {
    View view;
    ViewParent viewParent = this.mParent;
    if (viewParent instanceof View) {
      view = (View)viewParent;
      view.transformMatrixToLocal(paramMatrix);
      paramMatrix.postTranslate(view.mScrollX, view.mScrollY);
    } else if (view instanceof ViewRootImpl) {
      ViewRootImpl viewRootImpl = (ViewRootImpl)view;
      viewRootImpl.transformMatrixToLocal(paramMatrix);
      paramMatrix.postTranslate(0.0F, viewRootImpl.mCurScrollY);
    } 
    paramMatrix.postTranslate(-this.mLeft, -this.mTop);
    if (!hasIdentityMatrix())
      paramMatrix.postConcat(getInverseMatrix()); 
  }
  
  @ExportedProperty(category = "layout", indexMapping = {@IntToString(from = 0, to = "x"), @IntToString(from = 1, to = "y")})
  public int[] getLocationOnScreen() {
    int[] arrayOfInt = new int[2];
    getLocationOnScreen(arrayOfInt);
    return arrayOfInt;
  }
  
  public void getLocationOnScreen(int[] paramArrayOfint) {
    getLocationInWindow(paramArrayOfint);
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      paramArrayOfint[0] = paramArrayOfint[0] + attachInfo.mWindowLeft;
      paramArrayOfint[1] = paramArrayOfint[1] + attachInfo.mWindowTop;
    } 
  }
  
  public void getLocationInWindow(int[] paramArrayOfint) {
    if (paramArrayOfint != null && paramArrayOfint.length >= 2) {
      paramArrayOfint[0] = 0;
      paramArrayOfint[1] = 0;
      transformFromViewToWindowSpace(paramArrayOfint);
      return;
    } 
    throw new IllegalArgumentException("outLocation must be an array of two integers");
  }
  
  public void transformFromViewToWindowSpace(int[] paramArrayOfint) {
    if (paramArrayOfint != null && paramArrayOfint.length >= 2) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo == null) {
        paramArrayOfint[1] = 0;
        paramArrayOfint[0] = 0;
        return;
      } 
      float[] arrayOfFloat = attachInfo.mTmpTransformLocation;
      arrayOfFloat[0] = paramArrayOfint[0];
      arrayOfFloat[1] = paramArrayOfint[1];
      if (!hasIdentityMatrix())
        getMatrix().mapPoints(arrayOfFloat); 
      arrayOfFloat[0] = arrayOfFloat[0] + this.mLeft;
      arrayOfFloat[1] = arrayOfFloat[1] + this.mTop;
      ViewParent viewParent = this.mParent;
      while (viewParent instanceof View) {
        View view = (View)viewParent;
        arrayOfFloat[0] = arrayOfFloat[0] - view.mScrollX;
        arrayOfFloat[1] = arrayOfFloat[1] - view.mScrollY;
        if (!view.hasIdentityMatrix())
          view.getMatrix().mapPoints(arrayOfFloat); 
        arrayOfFloat[0] = arrayOfFloat[0] + view.mLeft;
        arrayOfFloat[1] = arrayOfFloat[1] + view.mTop;
        viewParent = view.mParent;
      } 
      if (viewParent instanceof ViewRootImpl) {
        viewParent = viewParent;
        arrayOfFloat[1] = arrayOfFloat[1] - ((ViewRootImpl)viewParent).mCurScrollY;
      } 
      paramArrayOfint[0] = Math.round(arrayOfFloat[0]);
      paramArrayOfint[1] = Math.round(arrayOfFloat[1]);
      return;
    } 
    throw new IllegalArgumentException("inOutLocation must be an array of two integers");
  }
  
  protected <T extends View> T findViewTraversal(int paramInt) {
    if (paramInt == this.mID)
      return (T)this; 
    return null;
  }
  
  protected <T extends View> T findViewWithTagTraversal(Object paramObject) {
    if (paramObject != null && paramObject.equals(this.mTag))
      return (T)this; 
    return null;
  }
  
  protected <T extends View> T findViewByPredicateTraversal(Predicate<View> paramPredicate, View paramView) {
    if (paramPredicate.test(this))
      return (T)this; 
    return null;
  }
  
  public final <T extends View> T findViewById(int paramInt) {
    if (paramInt == -1)
      return null; 
    return findViewTraversal(paramInt);
  }
  
  public final <T extends View> T requireViewById(int paramInt) {
    T t = (T)findViewById(paramInt);
    if (t != null)
      return t; 
    throw new IllegalArgumentException("ID does not reference a View inside this View");
  }
  
  public <T extends View> T findViewByAccessibilityIdTraversal(int paramInt) {
    if (getAccessibilityViewId() == paramInt)
      return (T)this; 
    return null;
  }
  
  public <T extends View> T findViewByAutofillIdTraversal(int paramInt) {
    if (getAutofillViewId() == paramInt)
      return (T)this; 
    return null;
  }
  
  public final <T extends View> T findViewWithTag(Object paramObject) {
    if (paramObject == null)
      return null; 
    return findViewWithTagTraversal(paramObject);
  }
  
  public final <T extends View> T findViewByPredicate(Predicate<View> paramPredicate) {
    return findViewByPredicateTraversal(paramPredicate, (View)null);
  }
  
  public final <T extends View> T findViewByPredicateInsideOut(View paramView, Predicate<View> paramPredicate) {
    View view = null;
    while (true) {
      view = paramView.findViewByPredicateTraversal(paramPredicate, view);
      if (view != null || paramView == this)
        break; 
      ViewParent viewParent = paramView.getParent();
      if (viewParent == null || !(viewParent instanceof View))
        return null; 
      View view1 = (View)viewParent;
      view = paramView;
      paramView = view1;
    } 
    return (T)view;
  }
  
  public void setId(int paramInt) {
    this.mID = paramInt;
    if (paramInt == -1 && this.mLabelForId != -1)
      this.mID = generateViewId(); 
  }
  
  public void setIsRootNamespace(boolean paramBoolean) {
    if (paramBoolean) {
      this.mPrivateFlags |= 0x8;
    } else {
      this.mPrivateFlags &= 0xFFFFFFF7;
    } 
  }
  
  public boolean isRootNamespace() {
    boolean bool;
    if ((this.mPrivateFlags & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @CapturedViewProperty
  public int getId() {
    return this.mID;
  }
  
  public long getUniqueDrawingId() {
    return this.mRenderNode.getUniqueId();
  }
  
  @ExportedProperty
  public Object getTag() {
    return this.mTag;
  }
  
  public void setTag(Object paramObject) {
    this.mTag = paramObject;
  }
  
  public Object getTag(int paramInt) {
    SparseArray<Object> sparseArray = this.mKeyedTags;
    if (sparseArray != null)
      return sparseArray.get(paramInt); 
    return null;
  }
  
  public void setTag(int paramInt, Object paramObject) {
    if (paramInt >>> 24 >= 2) {
      setKeyedTag(paramInt, paramObject);
      return;
    } 
    throw new IllegalArgumentException("The key must be an application-specific resource id.");
  }
  
  public void setTagInternal(int paramInt, Object paramObject) {
    if (paramInt >>> 24 == 1) {
      setKeyedTag(paramInt, paramObject);
      return;
    } 
    throw new IllegalArgumentException("The key must be a framework-specific resource id.");
  }
  
  private void setKeyedTag(int paramInt, Object paramObject) {
    if (this.mKeyedTags == null)
      this.mKeyedTags = new SparseArray(2); 
    this.mKeyedTags.put(paramInt, paramObject);
  }
  
  public void debug() {
    debug(0);
  }
  
  protected void debug(int paramInt) {
    String str6 = debugIndent(paramInt - 1);
    StringBuilder stringBuilder8 = new StringBuilder();
    stringBuilder8.append(str6);
    stringBuilder8.append("+ ");
    stringBuilder8.append(this);
    String str9 = stringBuilder8.toString();
    int i = getId();
    str6 = str9;
    if (i != -1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str9);
      stringBuilder.append(" (id=");
      stringBuilder.append(i);
      stringBuilder.append(")");
      str6 = stringBuilder.toString();
    } 
    Object object = getTag();
    str9 = str6;
    if (object != null) {
      stringBuilder7 = new StringBuilder();
      stringBuilder7.append(str6);
      stringBuilder7.append(" (tag=");
      stringBuilder7.append(object);
      stringBuilder7.append(")");
      str9 = stringBuilder7.toString();
    } 
    Log.d("View", str9);
    if ((this.mPrivateFlags & 0x2) != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(debugIndent(paramInt));
      stringBuilder.append(" FOCUSED");
      String str = stringBuilder.toString();
      Log.d("View", str);
    } 
    str9 = debugIndent(paramInt);
    StringBuilder stringBuilder5 = new StringBuilder();
    stringBuilder5.append(str9);
    stringBuilder5.append("frame={");
    stringBuilder5.append(this.mLeft);
    stringBuilder5.append(", ");
    stringBuilder5.append(this.mTop);
    stringBuilder5.append(", ");
    stringBuilder5.append(this.mRight);
    stringBuilder5.append(", ");
    stringBuilder5.append(this.mBottom);
    stringBuilder5.append("} scroll={");
    stringBuilder5.append(this.mScrollX);
    stringBuilder5.append(", ");
    stringBuilder5.append(this.mScrollY);
    stringBuilder5.append("} ");
    String str5 = stringBuilder5.toString();
    Log.d("View", str5);
    if (this.mPaddingLeft != 0 || this.mPaddingTop != 0 || this.mPaddingRight != 0 || this.mPaddingBottom != 0) {
      str9 = debugIndent(paramInt);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str9);
      stringBuilder.append("padding={");
      stringBuilder.append(this.mPaddingLeft);
      stringBuilder.append(", ");
      stringBuilder.append(this.mPaddingTop);
      stringBuilder.append(", ");
      stringBuilder.append(this.mPaddingRight);
      stringBuilder.append(", ");
      stringBuilder.append(this.mPaddingBottom);
      stringBuilder.append("}");
      String str = stringBuilder.toString();
      Log.d("View", str);
    } 
    str9 = debugIndent(paramInt);
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append(str9);
    stringBuilder4.append("mMeasureWidth=");
    stringBuilder4.append(this.mMeasuredWidth);
    stringBuilder4.append(" mMeasureHeight=");
    stringBuilder4.append(this.mMeasuredHeight);
    String str4 = stringBuilder4.toString();
    Log.d("View", str4);
    str4 = debugIndent(paramInt);
    ViewGroup.LayoutParams layoutParams = this.mLayoutParams;
    if (layoutParams == null) {
      stringBuilder7 = new StringBuilder();
      stringBuilder7.append(str4);
      stringBuilder7.append("BAD! no layout params");
      str4 = stringBuilder7.toString();
    } else {
      str4 = stringBuilder7.debug(str4);
    } 
    Log.d("View", str4);
    str4 = debugIndent(paramInt);
    StringBuilder stringBuilder7 = new StringBuilder();
    stringBuilder7.append(str4);
    stringBuilder7.append("flags={");
    String str8 = stringBuilder7.toString();
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(str8);
    stringBuilder3.append(printFlags(this.mViewFlags));
    str8 = stringBuilder3.toString();
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(str8);
    stringBuilder3.append("}");
    String str3 = stringBuilder3.toString();
    Log.d("View", str3);
    str8 = debugIndent(paramInt);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(str8);
    stringBuilder2.append("privateFlags={");
    String str2 = stringBuilder2.toString();
    StringBuilder stringBuilder6 = new StringBuilder();
    stringBuilder6.append(str2);
    stringBuilder6.append(printPrivateFlags(this.mPrivateFlags));
    String str7 = stringBuilder6.toString();
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(str7);
    stringBuilder1.append("}");
    String str1 = stringBuilder1.toString();
    Log.d("View", str1);
  }
  
  protected static String debugIndent(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder((paramInt * 2 + 3) * 2);
    for (byte b = 0; b < paramInt * 2 + 3; b++) {
      stringBuilder.append(' ');
      stringBuilder.append(' ');
    } 
    return stringBuilder.toString();
  }
  
  @ExportedProperty(category = "layout")
  public int getBaseline() {
    return -1;
  }
  
  public boolean isInLayout() {
    boolean bool;
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl != null && viewRootImpl.isInLayout()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void requestLayout() {
    LongSparseLongArray longSparseLongArray = this.mMeasureCache;
    if (longSparseLongArray != null)
      longSparseLongArray.clear(); 
    AttachInfo attachInfo2 = this.mAttachInfo;
    if (attachInfo2 != null && attachInfo2.mViewRequestingLayout == null) {
      ViewRootImpl viewRootImpl = getViewRootImpl();
      if (viewRootImpl != null && viewRootImpl.isInLayout() && !viewRootImpl.requestLayoutDuringLayout(this))
        return; 
      this.mAttachInfo.mViewRequestingLayout = this;
    } 
    int i = this.mPrivateFlags | 0x1000;
    this.mPrivateFlags = i | Integer.MIN_VALUE;
    ViewParent viewParent = this.mParent;
    if (viewParent != null && !viewParent.isLayoutRequested())
      try {
        this.mParent.requestLayout();
      } catch (NullPointerException nullPointerException) {
        if (isDebugVersion()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("mAssignNullStack:");
          stringBuilder.append(this.mAssignNullStack);
          Log.e("View", stringBuilder.toString());
        } 
        throw nullPointerException;
      }  
    AttachInfo attachInfo1 = this.mAttachInfo;
    if (attachInfo1 != null && attachInfo1.mViewRequestingLayout == this)
      this.mAttachInfo.mViewRequestingLayout = null; 
  }
  
  public void forceLayout() {
    LongSparseLongArray longSparseLongArray = this.mMeasureCache;
    if (longSparseLongArray != null)
      longSparseLongArray.clear(); 
    int i = this.mPrivateFlags | 0x1000;
    this.mPrivateFlags = i | Integer.MIN_VALUE;
  }
  
  public final void measure(int paramInt1, int paramInt2) {
    boolean bool1, bool3, bool4;
    boolean bool = isLayoutModeOptical(this);
    if (bool != isLayoutModeOptical(this.mParent)) {
      Insets insets = getOpticalInsets();
      int j = insets.left + insets.right;
      bool1 = insets.top + insets.bottom;
      if (bool)
        j = -j; 
      j = MeasureSpec.adjust(paramInt1, j);
      if (bool) {
        paramInt1 = -bool1;
      } else {
        paramInt1 = bool1;
      } 
      paramInt2 = MeasureSpec.adjust(paramInt2, paramInt1);
      paramInt1 = j;
    } 
    long l1 = paramInt1 << 32L | paramInt2 & 0xFFFFFFFFL;
    if (this.mMeasureCache == null)
      this.mMeasureCache = new LongSparseLongArray(2); 
    int i = this.mPrivateFlags;
    boolean bool2 = true;
    if ((i & 0x1000) == 4096) {
      i = 1;
    } else {
      i = 0;
    } 
    if (paramInt1 != this.mOldWidthMeasureSpec || paramInt2 != this.mOldHeightMeasureSpec) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (MeasureSpec.getMode(paramInt1) == 1073741824 && MeasureSpec.getMode(paramInt2) == 1073741824) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if (getMeasuredWidth() == MeasureSpec.getSize(paramInt1) && getMeasuredHeight() == MeasureSpec.getSize(paramInt2)) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    if (bool1 && (sAlwaysRemeasureExactly || !bool3 || !bool4)) {
      bool1 = bool2;
    } else {
      bool1 = false;
    } 
    if (i != 0 || bool1) {
      this.mPrivateFlags &= 0xFFFFF7FF;
      resolveRtlPropertiesIfNeeded();
      if (i != 0) {
        i = -1;
      } else {
        i = this.mMeasureCache.indexOfKey(l1);
      } 
      if (i < 0 || sIgnoreMeasureCache) {
        onMeasure(paramInt1, paramInt2);
        this.mPrivateFlags3 &= 0xFFFFFFF7;
      } else {
        long l = this.mMeasureCache.valueAt(i);
        setMeasuredDimensionRaw((int)(l >> 32L), (int)l);
        this.mPrivateFlags3 |= 0x8;
      } 
      i = this.mPrivateFlags;
      if ((i & 0x800) == 2048) {
        this.mPrivateFlags = i | 0x2000;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("View with id ");
        stringBuilder.append(getId());
        stringBuilder.append(": ");
        stringBuilder.append(getClass().getName());
        stringBuilder.append("#onMeasure() did not set the measured dimension by calling setMeasuredDimension()");
        throw new IllegalStateException(stringBuilder.toString());
      } 
    } 
    this.mOldWidthMeasureSpec = paramInt1;
    this.mOldHeightMeasureSpec = paramInt2;
    LongSparseLongArray longSparseLongArray = this.mMeasureCache;
    long l2 = this.mMeasuredWidth;
    longSparseLongArray.put(l1, this.mMeasuredHeight & 0xFFFFFFFFL | l2 << 32L);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    paramInt1 = getDefaultSize(getSuggestedMinimumWidth(), paramInt1);
    paramInt2 = getDefaultSize(getSuggestedMinimumHeight(), paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  protected final void setMeasuredDimension(int paramInt1, int paramInt2) {
    boolean bool = isLayoutModeOptical(this);
    int i = paramInt1, j = paramInt2;
    if (bool != isLayoutModeOptical(this.mParent)) {
      Insets insets = getOpticalInsets();
      j = insets.left + insets.right;
      i = insets.top + insets.bottom;
      if (!bool)
        j = -j; 
      int k = paramInt1 + j;
      if (bool) {
        paramInt1 = i;
      } else {
        paramInt1 = -i;
      } 
      j = paramInt2 + paramInt1;
      i = k;
    } 
    setMeasuredDimensionRaw(i, j);
  }
  
  private void setMeasuredDimensionRaw(int paramInt1, int paramInt2) {
    this.mMeasuredWidth = paramInt1;
    this.mMeasuredHeight = paramInt2;
    this.mPrivateFlags |= 0x800;
  }
  
  public static int combineMeasuredStates(int paramInt1, int paramInt2) {
    return paramInt1 | paramInt2;
  }
  
  public static int resolveSize(int paramInt1, int paramInt2) {
    return resolveSizeAndState(paramInt1, paramInt2, 0) & 0xFFFFFF;
  }
  
  public static int resolveSizeAndState(int paramInt1, int paramInt2, int paramInt3) {
    int i = MeasureSpec.getMode(paramInt2);
    paramInt2 = MeasureSpec.getSize(paramInt2);
    if (i != Integer.MIN_VALUE) {
      if (i == 1073741824)
        paramInt1 = paramInt2; 
    } else if (paramInt2 < paramInt1) {
      paramInt1 = 0x1000000 | paramInt2;
    } 
    return 0xFF000000 & paramInt3 | paramInt1;
  }
  
  public static int getDefaultSize(int paramInt1, int paramInt2) {
    int i = paramInt1;
    int j = MeasureSpec.getMode(paramInt2);
    paramInt2 = MeasureSpec.getSize(paramInt2);
    if (j != Integer.MIN_VALUE)
      if (j != 0) {
        if (j != 1073741824) {
          paramInt1 = i;
          return paramInt1;
        } 
      } else {
        return paramInt1;
      }  
    paramInt1 = paramInt2;
    return paramInt1;
  }
  
  protected int getSuggestedMinimumHeight() {
    int i;
    Drawable drawable = this.mBackground;
    if (drawable == null) {
      i = this.mMinHeight;
    } else {
      i = Math.max(this.mMinHeight, drawable.getMinimumHeight());
    } 
    return i;
  }
  
  protected int getSuggestedMinimumWidth() {
    int i;
    Drawable drawable = this.mBackground;
    if (drawable == null) {
      i = this.mMinWidth;
    } else {
      i = Math.max(this.mMinWidth, drawable.getMinimumWidth());
    } 
    return i;
  }
  
  public int getMinimumHeight() {
    return this.mMinHeight;
  }
  
  @RemotableViewMethod
  public void setMinimumHeight(int paramInt) {
    this.mMinHeight = paramInt;
    requestLayout();
  }
  
  public int getMinimumWidth() {
    return this.mMinWidth;
  }
  
  public void setMinimumWidth(int paramInt) {
    this.mMinWidth = paramInt;
    requestLayout();
  }
  
  public Animation getAnimation() {
    return this.mCurrentAnimation;
  }
  
  public void startAnimation(Animation paramAnimation) {
    // Byte code:
    //   0: aload_1
    //   1: ldc2_w -1
    //   4: invokevirtual setStartTime : (J)V
    //   7: aload_0
    //   8: invokespecial shouldBoostAnimation : ()Z
    //   11: ifeq -> 87
    //   14: aload_1
    //   15: invokevirtual getDuration : ()J
    //   18: lstore_2
    //   19: lload_2
    //   20: ldc2_w 1000
    //   23: lcmp
    //   24: ifle -> 35
    //   27: sipush #1000
    //   30: istore #4
    //   32: goto -> 39
    //   35: lload_2
    //   36: l2i
    //   37: istore #4
    //   39: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   42: ifnonnull -> 62
    //   45: ldc android/view/View
    //   47: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   50: astore #5
    //   52: aload #5
    //   54: putstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   57: aload #5
    //   59: ifnull -> 84
    //   62: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   65: new com/oplus/orms/info/OrmsSaParam
    //   68: dup
    //   69: ldc_w ''
    //   72: ldc_w 'ORMS_ACTION_ANIMATION'
    //   75: iload #4
    //   77: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   80: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   83: pop2
    //   84: goto -> 169
    //   87: aload_0
    //   88: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   91: ifnull -> 169
    //   94: aload_0
    //   95: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   98: getfield mIsWeixinLauncherUI : Z
    //   101: ifeq -> 169
    //   104: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   107: ifnonnull -> 127
    //   110: ldc android/view/View
    //   112: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   115: astore #5
    //   117: aload #5
    //   119: putstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   122: aload #5
    //   124: ifnull -> 150
    //   127: getstatic android/view/View.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   130: new com/oplus/orms/info/OrmsSaParam
    //   133: dup
    //   134: ldc_w ''
    //   137: ldc_w 'ORMS_ACTION_ANIMATION'
    //   140: getstatic android/view/View.mBoostTime : I
    //   143: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   146: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   149: pop2
    //   150: aload_1
    //   151: getfield mSpeeduped : Z
    //   154: ifne -> 169
    //   157: aload_1
    //   158: ldc_w 0.8
    //   161: invokevirtual scaleCurrentDuration : (F)V
    //   164: aload_1
    //   165: iconst_1
    //   166: putfield mSpeeduped : Z
    //   169: aload_0
    //   170: aload_1
    //   171: invokevirtual setAnimation : (Landroid/view/animation/Animation;)V
    //   174: aload_0
    //   175: invokevirtual invalidateParentCaches : ()V
    //   178: aload_0
    //   179: iconst_1
    //   180: invokevirtual invalidate : (Z)V
    //   183: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #26103	-> 0
    //   #26106	-> 7
    //   #26107	-> 14
    //   #26108	-> 19
    //   #26109	-> 39
    //   #26110	-> 62
    //   #26112	-> 84
    //   #26113	-> 87
    //   #26114	-> 104
    //   #26115	-> 127
    //   #26118	-> 150
    //   #26119	-> 157
    //   #26120	-> 164
    //   #26124	-> 169
    //   #26125	-> 174
    //   #26126	-> 178
    //   #26127	-> 183
  }
  
  public void clearAnimation() {
    Animation animation = this.mCurrentAnimation;
    if (animation != null)
      animation.detach(); 
    this.mCurrentAnimation = null;
    invalidateParentIfNeeded();
  }
  
  public void setAnimation(Animation paramAnimation) {
    this.mCurrentAnimation = paramAnimation;
    if (paramAnimation != null) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null && attachInfo.mDisplayState == 1 && paramAnimation.getStartTime() == -1L)
        paramAnimation.setStartTime(AnimationUtils.currentAnimationTimeMillis()); 
      paramAnimation.reset();
    } 
  }
  
  protected void onAnimationStart() {
    this.mPrivateFlags |= 0x10000;
  }
  
  protected void onAnimationEnd() {
    this.mPrivateFlags &= 0xFFFEFFFF;
  }
  
  protected boolean onSetAlpha(int paramInt) {
    return false;
  }
  
  public boolean gatherTransparentRegion(Region paramRegion) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (paramRegion != null && attachInfo != null) {
      int i = this.mPrivateFlags;
      if ((i & 0x80) == 0) {
        int[] arrayOfInt = attachInfo.mTransparentLocation;
        getLocationInWindow(arrayOfInt);
        if (getZ() > 0.0F) {
          i = (int)getZ();
        } else {
          i = 0;
        } 
        paramRegion.op(arrayOfInt[0] - i, arrayOfInt[1] - i, arrayOfInt[0] + this.mRight - this.mLeft + i, arrayOfInt[1] + this.mBottom - this.mTop + i * 3, Region.Op.DIFFERENCE);
      } else {
        Drawable drawable2 = this.mBackground;
        if (drawable2 != null && drawable2.getOpacity() != -2)
          applyDrawableToTransparentRegion(this.mBackground, paramRegion); 
        ForegroundInfo foregroundInfo = this.mForegroundInfo;
        if (foregroundInfo != null && foregroundInfo.mDrawable != null) {
          foregroundInfo = this.mForegroundInfo;
          if (foregroundInfo.mDrawable.getOpacity() != -2)
            applyDrawableToTransparentRegion(this.mForegroundInfo.mDrawable, paramRegion); 
        } 
        Drawable drawable1 = this.mDefaultFocusHighlight;
        if (drawable1 != null && drawable1.getOpacity() != -2)
          applyDrawableToTransparentRegion(this.mDefaultFocusHighlight, paramRegion); 
      } 
    } 
    return true;
  }
  
  public void playSoundEffect(int paramInt) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null || attachInfo.mRootCallbacks == null || !isSoundEffectsEnabled())
      return; 
    this.mAttachInfo.mRootCallbacks.playSoundEffect(paramInt);
  }
  
  public boolean performHapticFeedback(int paramInt) {
    return performHapticFeedback(paramInt, 0);
  }
  
  public boolean performHapticFeedback(int paramInt1, int paramInt2) {
    AttachInfo attachInfo = this.mAttachInfo;
    boolean bool = false;
    if (attachInfo == null)
      return false; 
    if ((paramInt2 & 0x1) == 0 && !isHapticFeedbackEnabled())
      return false; 
    AttachInfo.Callbacks callbacks = this.mAttachInfo.mRootCallbacks;
    if ((paramInt2 & 0x2) != 0)
      bool = true; 
    return callbacks.performHapticFeedback(paramInt1, bool);
  }
  
  @Deprecated
  public void setSystemUiVisibility(int paramInt) {
    if (paramInt != this.mSystemUiVisibility) {
      this.mSystemUiVisibility = paramInt;
      if (this.mParent != null) {
        AttachInfo attachInfo = this.mAttachInfo;
        if (attachInfo != null && !attachInfo.mRecomputeGlobalAttributes)
          this.mParent.recomputeViewAttributes(this); 
      } 
    } 
  }
  
  @Deprecated
  public int getSystemUiVisibility() {
    return this.mSystemUiVisibility;
  }
  
  @Deprecated
  public int getWindowSystemUiVisibility() {
    boolean bool;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      bool = attachInfo.mSystemUiVisibility;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public void onWindowSystemUiVisibilityChanged(int paramInt) {}
  
  @Deprecated
  public void dispatchWindowSystemUiVisiblityChanged(int paramInt) {
    onWindowSystemUiVisibilityChanged(paramInt);
  }
  
  @Deprecated
  public void setOnSystemUiVisibilityChangeListener(OnSystemUiVisibilityChangeListener paramOnSystemUiVisibilityChangeListener) {
    ListenerInfo.access$1802(getListenerInfo(), paramOnSystemUiVisibilityChangeListener);
    if (this.mParent != null) {
      AttachInfo attachInfo = this.mAttachInfo;
      if (attachInfo != null && !attachInfo.mRecomputeGlobalAttributes)
        this.mParent.recomputeViewAttributes(this); 
    } 
  }
  
  @Deprecated
  public void dispatchSystemUiVisibilityChanged(int paramInt) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnSystemUiVisibilityChangeListener != null)
      listenerInfo.mOnSystemUiVisibilityChangeListener.onSystemUiVisibilityChange(paramInt & 0x3FF7); 
  }
  
  boolean updateLocalSystemUiVisibility(int paramInt1, int paramInt2) {
    int i = this.mSystemUiVisibility;
    paramInt1 = (paramInt2 ^ 0xFFFFFFFF) & i | paramInt1 & paramInt2;
    if (paramInt1 != i) {
      setSystemUiVisibility(paramInt1);
      return true;
    } 
    return false;
  }
  
  public void setDisabledSystemUiVisibility(int paramInt) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null && attachInfo.mDisabledSystemUiVisibility != paramInt) {
      this.mAttachInfo.mDisabledSystemUiVisibility = paramInt;
      ViewParent viewParent = this.mParent;
      if (viewParent != null)
        viewParent.recomputeViewAttributes(this); 
    } 
  }
  
  class DragShadowBuilder {
    private final WeakReference<View> mView;
    
    public DragShadowBuilder(View this$0) {
      this.mView = new WeakReference<>(this$0);
    }
    
    public DragShadowBuilder() {
      this.mView = new WeakReference<>(null);
    }
    
    public final View getView() {
      return this.mView.get();
    }
    
    public void onProvideShadowMetrics(Point param1Point1, Point param1Point2) {
      View view = this.mView.get();
      if (view != null) {
        param1Point1.set(view.getWidth(), view.getHeight());
        param1Point2.set(param1Point1.x / 2, param1Point1.y / 2);
      } else {
        Log.e("View", "Asked for drag thumb metrics but no view");
      } 
    }
    
    public void onDrawShadow(Canvas param1Canvas) {
      View view = this.mView.get();
      if (view != null) {
        view.draw(param1Canvas);
      } else {
        Log.e("View", "Asked to draw drag shadow but no view");
      } 
    }
  }
  
  @Deprecated
  public final boolean startDrag(ClipData paramClipData, DragShadowBuilder paramDragShadowBuilder, Object paramObject, int paramInt) {
    return startDragAndDrop(paramClipData, paramDragShadowBuilder, paramObject, paramInt);
  }
  
  public final boolean startDragAndDrop(ClipData paramClipData, DragShadowBuilder paramDragShadowBuilder, Object paramObject, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   4: astore #5
    //   6: aload #5
    //   8: ifnonnull -> 23
    //   11: ldc_w 'View'
    //   14: ldc_w 'startDragAndDrop called on a detached view.'
    //   17: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   20: pop
    //   21: iconst_0
    //   22: ireturn
    //   23: aload #5
    //   25: getfield mViewRootImpl : Landroid/view/ViewRootImpl;
    //   28: getfield mSurface : Landroid/view/Surface;
    //   31: invokevirtual isValid : ()Z
    //   34: ifne -> 49
    //   37: ldc_w 'View'
    //   40: ldc_w 'startDragAndDrop called with an invalid surface.'
    //   43: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   46: pop
    //   47: iconst_0
    //   48: ireturn
    //   49: aload_1
    //   50: ifnull -> 80
    //   53: iload #4
    //   55: sipush #256
    //   58: iand
    //   59: ifeq -> 68
    //   62: iconst_1
    //   63: istore #6
    //   65: goto -> 71
    //   68: iconst_0
    //   69: istore #6
    //   71: aload_1
    //   72: iload #6
    //   74: invokevirtual prepareToLeaveProcess : (Z)V
    //   77: goto -> 80
    //   80: new android/graphics/Point
    //   83: dup
    //   84: invokespecial <init> : ()V
    //   87: astore #7
    //   89: new android/graphics/Point
    //   92: dup
    //   93: invokespecial <init> : ()V
    //   96: astore #8
    //   98: aload_2
    //   99: aload #7
    //   101: aload #8
    //   103: invokevirtual onProvideShadowMetrics : (Landroid/graphics/Point;Landroid/graphics/Point;)V
    //   106: aload #7
    //   108: getfield x : I
    //   111: iflt -> 623
    //   114: aload #7
    //   116: getfield y : I
    //   119: iflt -> 623
    //   122: aload #8
    //   124: getfield x : I
    //   127: iflt -> 623
    //   130: aload #8
    //   132: getfield y : I
    //   135: iflt -> 623
    //   138: aload #7
    //   140: getfield x : I
    //   143: ifeq -> 154
    //   146: aload #7
    //   148: getfield y : I
    //   151: ifne -> 172
    //   154: getstatic android/view/View.sAcceptZeroSizeDragShadow : Z
    //   157: ifeq -> 612
    //   160: aload #7
    //   162: iconst_1
    //   163: putfield x : I
    //   166: aload #7
    //   168: iconst_1
    //   169: putfield y : I
    //   172: aload_0
    //   173: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   176: getfield mViewRootImpl : Landroid/view/ViewRootImpl;
    //   179: astore #9
    //   181: new android/view/SurfaceSession
    //   184: dup
    //   185: invokespecial <init> : ()V
    //   188: astore #10
    //   190: new android/view/SurfaceControl$Builder
    //   193: dup
    //   194: aload #10
    //   196: invokespecial <init> : (Landroid/view/SurfaceSession;)V
    //   199: astore #5
    //   201: aload #5
    //   203: ldc_w 'drag surface'
    //   206: invokevirtual setName : (Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
    //   209: astore #5
    //   211: aload #5
    //   213: aload #9
    //   215: invokevirtual getSurfaceControl : ()Landroid/view/SurfaceControl;
    //   218: invokevirtual setParent : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;
    //   221: astore #5
    //   223: aload #7
    //   225: getfield x : I
    //   228: istore #11
    //   230: aload #7
    //   232: getfield y : I
    //   235: istore #12
    //   237: aload #5
    //   239: iload #11
    //   241: iload #12
    //   243: invokevirtual setBufferSize : (II)Landroid/view/SurfaceControl$Builder;
    //   246: astore #5
    //   248: aload #5
    //   250: bipush #-3
    //   252: invokevirtual setFormat : (I)Landroid/view/SurfaceControl$Builder;
    //   255: astore #5
    //   257: aload #5
    //   259: ldc_w 'View.startDragAndDrop'
    //   262: invokevirtual setCallsite : (Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
    //   265: astore #5
    //   267: aload #5
    //   269: invokevirtual build : ()Landroid/view/SurfaceControl;
    //   272: astore #13
    //   274: new android/view/Surface
    //   277: dup
    //   278: invokespecial <init> : ()V
    //   281: astore #14
    //   283: aload #14
    //   285: aload #13
    //   287: invokevirtual copyFrom : (Landroid/view/SurfaceControl;)V
    //   290: aconst_null
    //   291: astore #15
    //   293: aconst_null
    //   294: astore #16
    //   296: aconst_null
    //   297: astore #5
    //   299: aload #14
    //   301: aconst_null
    //   302: invokevirtual lockCanvas : (Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    //   305: astore #17
    //   307: aload #17
    //   309: iconst_0
    //   310: getstatic android/graphics/PorterDuff$Mode.CLEAR : Landroid/graphics/PorterDuff$Mode;
    //   313: invokevirtual drawColor : (ILandroid/graphics/PorterDuff$Mode;)V
    //   316: aload_2
    //   317: aload #17
    //   319: invokevirtual onDrawShadow : (Landroid/graphics/Canvas;)V
    //   322: aload #14
    //   324: aload #17
    //   326: invokevirtual unlockCanvasAndPost : (Landroid/graphics/Canvas;)V
    //   329: aload #9
    //   331: aload #7
    //   333: invokevirtual getLastTouchPoint : (Landroid/graphics/Point;)V
    //   336: aload_0
    //   337: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   340: getfield mSession : Landroid/view/IWindowSession;
    //   343: astore #15
    //   345: aload_0
    //   346: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   349: getfield mWindow : Landroid/view/IWindow;
    //   352: astore_2
    //   353: aload #9
    //   355: invokevirtual getLastTouchSource : ()I
    //   358: istore #12
    //   360: aload #7
    //   362: getfield x : I
    //   365: i2f
    //   366: fstore #18
    //   368: aload #7
    //   370: getfield y : I
    //   373: i2f
    //   374: fstore #19
    //   376: aload #8
    //   378: getfield x : I
    //   381: i2f
    //   382: fstore #20
    //   384: aload #8
    //   386: getfield y : I
    //   389: istore #11
    //   391: iload #11
    //   393: i2f
    //   394: fstore #21
    //   396: aload #15
    //   398: aload_2
    //   399: iload #4
    //   401: aload #13
    //   403: iload #12
    //   405: fload #18
    //   407: fload #19
    //   409: fload #20
    //   411: fload #21
    //   413: aload_1
    //   414: invokeinterface performDrag : (Landroid/view/IWindow;ILandroid/view/SurfaceControl;IFFFFLandroid/content/ClipData;)Landroid/os/IBinder;
    //   419: astore_2
    //   420: aload_2
    //   421: ifnull -> 488
    //   424: aload_0
    //   425: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   428: getfield mDragSurface : Landroid/view/Surface;
    //   431: ifnull -> 444
    //   434: aload_0
    //   435: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   438: getfield mDragSurface : Landroid/view/Surface;
    //   441: invokevirtual release : ()V
    //   444: aload_0
    //   445: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   448: aload #14
    //   450: putfield mDragSurface : Landroid/view/Surface;
    //   453: aload_0
    //   454: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   457: aload_2
    //   458: putfield mDragToken : Landroid/os/IBinder;
    //   461: aload #9
    //   463: aload_3
    //   464: invokevirtual setLocalDragState : (Ljava/lang/Object;)V
    //   467: goto -> 488
    //   470: astore_1
    //   471: goto -> 479
    //   474: astore_1
    //   475: goto -> 483
    //   478: astore_1
    //   479: goto -> 596
    //   482: astore_1
    //   483: aload_2
    //   484: astore_3
    //   485: goto -> 566
    //   488: aload_2
    //   489: ifnull -> 498
    //   492: iconst_1
    //   493: istore #6
    //   495: goto -> 501
    //   498: iconst_0
    //   499: istore #6
    //   501: aload_2
    //   502: ifnonnull -> 510
    //   505: aload #14
    //   507: invokevirtual destroy : ()V
    //   510: aload #10
    //   512: invokevirtual kill : ()V
    //   515: iload #6
    //   517: ireturn
    //   518: astore_1
    //   519: aload #16
    //   521: astore_2
    //   522: goto -> 596
    //   525: astore_1
    //   526: aload #5
    //   528: astore_3
    //   529: goto -> 566
    //   532: astore_1
    //   533: aload #15
    //   535: astore_2
    //   536: aload #14
    //   538: aload #17
    //   540: invokevirtual unlockCanvasAndPost : (Landroid/graphics/Canvas;)V
    //   543: aload #15
    //   545: astore_2
    //   546: aload_1
    //   547: athrow
    //   548: astore_1
    //   549: aload #5
    //   551: astore_3
    //   552: goto -> 566
    //   555: astore_1
    //   556: aload #16
    //   558: astore_2
    //   559: goto -> 596
    //   562: astore_1
    //   563: aload #5
    //   565: astore_3
    //   566: aload_3
    //   567: astore_2
    //   568: ldc_w 'View'
    //   571: ldc_w 'Unable to initiate drag'
    //   574: aload_1
    //   575: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   578: pop
    //   579: aload_3
    //   580: ifnonnull -> 588
    //   583: aload #14
    //   585: invokevirtual destroy : ()V
    //   588: aload #10
    //   590: invokevirtual kill : ()V
    //   593: iconst_0
    //   594: ireturn
    //   595: astore_1
    //   596: aload_2
    //   597: ifnonnull -> 605
    //   600: aload #14
    //   602: invokevirtual destroy : ()V
    //   605: aload #10
    //   607: invokevirtual kill : ()V
    //   610: aload_1
    //   611: athrow
    //   612: new java/lang/IllegalStateException
    //   615: dup
    //   616: ldc_w 'Drag shadow dimensions must be positive'
    //   619: invokespecial <init> : (Ljava/lang/String;)V
    //   622: athrow
    //   623: new java/lang/IllegalStateException
    //   626: dup
    //   627: ldc_w 'Drag shadow dimensions must not be negative'
    //   630: invokespecial <init> : (Ljava/lang/String;)V
    //   633: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #26672	-> 0
    //   #26673	-> 11
    //   #26674	-> 21
    //   #26676	-> 23
    //   #26677	-> 37
    //   #26678	-> 47
    //   #26681	-> 49
    //   #26682	-> 53
    //   #26681	-> 80
    //   #26685	-> 80
    //   #26686	-> 89
    //   #26687	-> 98
    //   #26689	-> 106
    //   #26696	-> 138
    //   #26697	-> 154
    //   #26700	-> 160
    //   #26701	-> 166
    //   #26709	-> 172
    //   #26710	-> 181
    //   #26711	-> 190
    //   #26712	-> 201
    //   #26713	-> 211
    //   #26714	-> 237
    //   #26715	-> 248
    //   #26716	-> 257
    //   #26717	-> 267
    //   #26718	-> 274
    //   #26719	-> 283
    //   #26720	-> 290
    //   #26722	-> 299
    //   #26724	-> 307
    //   #26725	-> 316
    //   #26727	-> 322
    //   #26728	-> 329
    //   #26731	-> 329
    //   #26733	-> 336
    //   #26734	-> 353
    //   #26733	-> 396
    //   #26739	-> 420
    //   #26740	-> 424
    //   #26741	-> 434
    //   #26743	-> 444
    //   #26744	-> 453
    //   #26746	-> 461
    //   #26753	-> 470
    //   #26749	-> 474
    //   #26753	-> 478
    //   #26749	-> 482
    //   #26739	-> 488
    //   #26748	-> 488
    //   #26753	-> 501
    //   #26754	-> 505
    //   #26756	-> 510
    //   #26748	-> 515
    //   #26753	-> 518
    //   #26749	-> 525
    //   #26727	-> 532
    //   #26728	-> 543
    //   #26749	-> 548
    //   #26753	-> 555
    //   #26749	-> 562
    //   #26750	-> 566
    //   #26751	-> 579
    //   #26753	-> 579
    //   #26754	-> 583
    //   #26756	-> 588
    //   #26751	-> 593
    //   #26753	-> 595
    //   #26754	-> 600
    //   #26756	-> 605
    //   #26757	-> 610
    //   #26698	-> 612
    //   #26689	-> 623
    //   #26691	-> 623
    // Exception table:
    //   from	to	target	type
    //   299	307	562	java/lang/Exception
    //   299	307	555	finally
    //   307	316	532	finally
    //   316	322	532	finally
    //   322	329	562	java/lang/Exception
    //   322	329	555	finally
    //   329	336	562	java/lang/Exception
    //   329	336	555	finally
    //   336	353	562	java/lang/Exception
    //   336	353	555	finally
    //   353	391	562	java/lang/Exception
    //   353	391	555	finally
    //   396	420	525	java/lang/Exception
    //   396	420	518	finally
    //   424	434	482	java/lang/Exception
    //   424	434	478	finally
    //   434	444	482	java/lang/Exception
    //   434	444	478	finally
    //   444	453	482	java/lang/Exception
    //   444	453	478	finally
    //   453	461	482	java/lang/Exception
    //   453	461	478	finally
    //   461	467	474	java/lang/Exception
    //   461	467	470	finally
    //   536	543	548	java/lang/Exception
    //   536	543	595	finally
    //   546	548	548	java/lang/Exception
    //   546	548	595	finally
    //   568	579	595	finally
  }
  
  public final void cancelDragAndDrop() {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null) {
      Log.w("View", "cancelDragAndDrop called on a detached view.");
      return;
    } 
    if (attachInfo.mDragToken != null) {
      try {
        this.mAttachInfo.mSession.cancelDragAndDrop(this.mAttachInfo.mDragToken, false);
      } catch (Exception exception) {
        Log.e("View", "Unable to cancel drag", exception);
      } 
      this.mAttachInfo.mDragToken = null;
    } else {
      Log.e("View", "No active drag to cancel");
    } 
  }
  
  public final void updateDragShadow(DragShadowBuilder paramDragShadowBuilder) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null) {
      Log.w("View", "updateDragShadow called on a detached view.");
      return;
    } 
    if (attachInfo.mDragToken != null) {
      try {
        Canvas canvas = this.mAttachInfo.mDragSurface.lockCanvas(null);
        try {
          canvas.drawColor(0, PorterDuff.Mode.CLEAR);
          paramDragShadowBuilder.onDrawShadow(canvas);
        } finally {
          this.mAttachInfo.mDragSurface.unlockCanvasAndPost(canvas);
        } 
      } catch (Exception exception) {
        Log.e("View", "Unable to update drag shadow", exception);
      } 
    } else {
      Log.e("View", "No active drag");
    } 
  }
  
  public final boolean startMovingTask(float paramFloat1, float paramFloat2) {
    try {
      return this.mAttachInfo.mSession.startMovingTask(this.mAttachInfo.mWindow, paramFloat1, paramFloat2);
    } catch (RemoteException remoteException) {
      Log.e("View", "Unable to start moving", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void finishMovingTask() {
    try {
      this.mAttachInfo.mSession.finishMovingTask(this.mAttachInfo.mWindow);
    } catch (RemoteException remoteException) {
      Log.e("View", "Unable to finish moving", (Throwable)remoteException);
    } 
  }
  
  public boolean onDragEvent(DragEvent paramDragEvent) {
    return false;
  }
  
  boolean dispatchDragEnterExitInPreN(DragEvent paramDragEvent) {
    return callDragEventHandler(paramDragEvent);
  }
  
  public boolean dispatchDragEvent(DragEvent paramDragEvent) {
    paramDragEvent.mEventHandlerWasCalled = true;
    if (paramDragEvent.mAction == 2 || paramDragEvent.mAction == 3)
      getViewRootImpl().setDragFocus(this, paramDragEvent); 
    return callDragEventHandler(paramDragEvent);
  }
  
  final boolean callDragEventHandler(DragEvent paramDragEvent) {
    boolean bool;
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mOnDragListener != null && (this.mViewFlags & 0x20) == 0 && listenerInfo.mOnDragListener.onDrag(this, paramDragEvent)) {
      bool = true;
    } else {
      bool = onDragEvent(paramDragEvent);
    } 
    int i = paramDragEvent.mAction;
    if (i != 4) {
      if (i != 5) {
        if (i == 6) {
          this.mPrivateFlags2 &= 0xFFFFFFFD;
          refreshDrawableState();
        } 
      } else {
        this.mPrivateFlags2 |= 0x2;
        refreshDrawableState();
      } 
    } else {
      this.mPrivateFlags2 &= 0xFFFFFFFC;
      refreshDrawableState();
    } 
    return bool;
  }
  
  boolean canAcceptDrag() {
    int i = this.mPrivateFlags2;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public void onCloseSystemDialogs(String paramString) {}
  
  public void applyDrawableToTransparentRegion(Drawable paramDrawable, Region paramRegion) {
    int[] arrayOfInt;
    Region region = paramDrawable.getTransparentRegion();
    Rect rect = paramDrawable.getBounds();
    AttachInfo attachInfo = this.mAttachInfo;
    if (region != null && attachInfo != null) {
      int i = getRight() - getLeft();
      int j = getBottom() - getTop();
      if (rect.left > 0)
        region.op(0, 0, rect.left, j, Region.Op.UNION); 
      if (rect.right < i)
        region.op(rect.right, 0, i, j, Region.Op.UNION); 
      if (rect.top > 0)
        region.op(0, 0, i, rect.top, Region.Op.UNION); 
      if (rect.bottom < j)
        region.op(0, rect.bottom, i, j, Region.Op.UNION); 
      arrayOfInt = attachInfo.mTransparentLocation;
      getLocationInWindow(arrayOfInt);
      region.translate(arrayOfInt[0], arrayOfInt[1]);
      paramRegion.op(region, Region.Op.INTERSECT);
    } else {
      paramRegion.op((Rect)arrayOfInt, Region.Op.DIFFERENCE);
    } 
  }
  
  private void checkForLongClick(long paramLong, float paramFloat1, float paramFloat2, int paramInt) {
    int i = this.mViewFlags;
    if ((i & 0x200000) == 2097152 || (i & 0x40000000) == 1073741824) {
      this.mHasPerformedLongPress = false;
      if (this.mPendingCheckForLongPress == null)
        this.mPendingCheckForLongPress = new CheckForLongPress(); 
      this.mPendingCheckForLongPress.setAnchor(paramFloat1, paramFloat2);
      this.mPendingCheckForLongPress.rememberWindowAttachCount();
      this.mPendingCheckForLongPress.rememberPressedState();
      this.mPendingCheckForLongPress.setClassification(paramInt);
      postDelayed(this.mPendingCheckForLongPress, paramLong);
    } 
  }
  
  public static View inflate(Context paramContext, int paramInt, ViewGroup paramViewGroup) {
    LayoutInflater layoutInflater = LayoutInflater.from(paramContext);
    return layoutInflater.inflate(paramInt, paramViewGroup);
  }
  
  protected boolean overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean) {
    boolean bool;
    int i = this.mOverScrollMode;
    if (computeHorizontalScrollRange() > computeHorizontalScrollExtent()) {
      j = 1;
    } else {
      j = 0;
    } 
    if (computeVerticalScrollRange() > computeVerticalScrollExtent()) {
      k = 1;
    } else {
      k = 0;
    } 
    if (i == 0 || (i == 1 && j)) {
      j = 1;
    } else {
      j = 0;
    } 
    if (i == 0 || (i == 1 && k)) {
      k = 1;
    } else {
      k = 0;
    } 
    i = paramInt3 + paramInt1;
    if (!j)
      paramInt7 = 0; 
    int j = paramInt4 + paramInt2;
    if (!k)
      paramInt8 = 0; 
    int m = -paramInt7;
    paramInt7 += paramInt5;
    int k = -paramInt8;
    paramInt8 += paramInt6;
    if (i > paramInt7) {
      paramBoolean = true;
    } else if (i < m) {
      paramInt7 = m;
      paramBoolean = true;
    } else {
      paramBoolean = false;
      paramInt7 = i;
    } 
    if (j > paramInt8) {
      bool = true;
    } else if (j < k) {
      paramInt8 = k;
      bool = true;
    } else {
      bool = false;
      paramInt8 = j;
    } 
    onOverScrolled(paramInt7, paramInt8, paramBoolean, bool);
    this.mViewHooks.getScrollBarEffect().onOverScrolled(paramInt3 + paramInt1, paramInt4 + paramInt2, paramInt5, paramInt6);
    return (paramBoolean || bool);
  }
  
  protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {}
  
  public int getOverScrollMode() {
    return this.mOverScrollMode;
  }
  
  public void setOverScrollMode(int paramInt) {
    if (paramInt == 0 || paramInt == 1 || paramInt == 2) {
      this.mOverScrollMode = paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid overscroll mode ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setNestedScrollingEnabled(boolean paramBoolean) {
    if (paramBoolean) {
      this.mPrivateFlags3 |= 0x80;
    } else {
      stopNestedScroll();
      this.mPrivateFlags3 &= 0xFFFFFF7F;
    } 
  }
  
  public boolean isNestedScrollingEnabled() {
    boolean bool;
    if ((this.mPrivateFlags3 & 0x80) == 128) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean startNestedScroll(int paramInt) {
    if (hasNestedScrollingParent())
      return true; 
    if (isNestedScrollingEnabled()) {
      ViewParent viewParent = getParent();
      View view = this;
      while (viewParent != null) {
        try {
          if (viewParent.onStartNestedScroll(view, this, paramInt)) {
            this.mNestedScrollingParent = viewParent;
            viewParent.onNestedScrollAccepted(view, this, paramInt);
            return true;
          } 
        } catch (AbstractMethodError abstractMethodError) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("ViewParent ");
          stringBuilder.append(viewParent);
          stringBuilder.append(" does not implement interface method onStartNestedScroll");
          Log.e("View", stringBuilder.toString(), abstractMethodError);
        } 
        if (viewParent instanceof View)
          view = (View)viewParent; 
        viewParent = viewParent.getParent();
      } 
    } 
    return false;
  }
  
  public void stopNestedScroll() {
    ViewParent viewParent = this.mNestedScrollingParent;
    if (viewParent != null) {
      viewParent.onStopNestedScroll(this);
      this.mNestedScrollingParent = null;
    } 
  }
  
  public boolean hasNestedScrollingParent() {
    boolean bool;
    if (this.mNestedScrollingParent != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean dispatchNestedScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfint) {
    if (isNestedScrollingEnabled() && this.mNestedScrollingParent != null) {
      if (paramInt1 != 0 || paramInt2 != 0 || paramInt3 != 0 || paramInt4 != 0) {
        int i = 0;
        int j = 0;
        if (paramArrayOfint != null) {
          getLocationInWindow(paramArrayOfint);
          i = paramArrayOfint[0];
          j = paramArrayOfint[1];
        } 
        this.mNestedScrollingParent.onNestedScroll(this, paramInt1, paramInt2, paramInt3, paramInt4);
        if (paramArrayOfint != null) {
          getLocationInWindow(paramArrayOfint);
          paramArrayOfint[0] = paramArrayOfint[0] - i;
          paramArrayOfint[1] = paramArrayOfint[1] - j;
        } 
        if (isOplusOSStyle())
          return this.mIsPreScrollConsumed; 
        return true;
      } 
      if (paramArrayOfint != null) {
        paramArrayOfint[0] = 0;
        paramArrayOfint[1] = 0;
      } 
    } 
    return false;
  }
  
  public boolean dispatchNestedPreScroll(int paramInt1, int paramInt2, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    boolean bool = isNestedScrollingEnabled();
    boolean bool1 = false;
    if (bool && this.mNestedScrollingParent != null) {
      if (paramInt1 != 0 || paramInt2 != 0) {
        int i = 0;
        int j = 0;
        if (paramArrayOfint2 != null) {
          getLocationInWindow(paramArrayOfint2);
          i = paramArrayOfint2[0];
          j = paramArrayOfint2[1];
        } 
        int[] arrayOfInt = paramArrayOfint1;
        if (paramArrayOfint1 == null) {
          if (this.mTempNestedScrollConsumed == null)
            this.mTempNestedScrollConsumed = new int[2]; 
          arrayOfInt = this.mTempNestedScrollConsumed;
        } 
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        this.mNestedScrollingParent.onNestedPreScroll(this, paramInt1, paramInt2, arrayOfInt);
        if (paramArrayOfint2 != null) {
          getLocationInWindow(paramArrayOfint2);
          paramArrayOfint2[0] = paramArrayOfint2[0] - i;
          paramArrayOfint2[1] = paramArrayOfint2[1] - j;
        } 
        if (arrayOfInt[0] != 0 || arrayOfInt[1] != 0)
          bool1 = true; 
        return bool1;
      } 
      if (paramArrayOfint2 != null) {
        paramArrayOfint2[0] = 0;
        paramArrayOfint2[1] = 0;
      } 
    } 
    return false;
  }
  
  public boolean dispatchNestedFling(float paramFloat1, float paramFloat2, boolean paramBoolean) {
    if (isNestedScrollingEnabled()) {
      ViewParent viewParent = this.mNestedScrollingParent;
      if (viewParent != null)
        return viewParent.onNestedFling(this, paramFloat1, paramFloat2, paramBoolean); 
    } 
    return false;
  }
  
  public boolean dispatchNestedPreFling(float paramFloat1, float paramFloat2) {
    if (isNestedScrollingEnabled()) {
      ViewParent viewParent = this.mNestedScrollingParent;
      if (viewParent != null)
        return viewParent.onNestedPreFling(this, paramFloat1, paramFloat2); 
    } 
    return false;
  }
  
  protected float getVerticalScrollFactor() {
    if (this.mVerticalScrollFactor == 0.0F) {
      TypedValue typedValue = new TypedValue();
      if (this.mContext.getTheme().resolveAttribute(16842829, typedValue, true)) {
        Context context = this.mContext;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.mVerticalScrollFactor = typedValue.getDimension(displayMetrics);
      } else {
        throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
      } 
    } 
    return this.mVerticalScrollFactor;
  }
  
  protected float getHorizontalScrollFactor() {
    return getVerticalScrollFactor();
  }
  
  @ExportedProperty(category = "text", mapping = {@IntToString(from = 0, to = "INHERIT"), @IntToString(from = 1, to = "FIRST_STRONG"), @IntToString(from = 2, to = "ANY_RTL"), @IntToString(from = 3, to = "LTR"), @IntToString(from = 4, to = "RTL"), @IntToString(from = 5, to = "LOCALE"), @IntToString(from = 6, to = "FIRST_STRONG_LTR"), @IntToString(from = 7, to = "FIRST_STRONG_RTL")})
  public int getRawTextDirection() {
    return (this.mPrivateFlags2 & 0x1C0) >> 6;
  }
  
  public void setTextDirection(int paramInt) {
    if (getRawTextDirection() != paramInt) {
      this.mPrivateFlags2 &= 0xFFFFFE3F;
      resetResolvedTextDirection();
      this.mPrivateFlags2 |= paramInt << 6 & 0x1C0;
      resolveTextDirection();
      onRtlPropertiesChanged(getLayoutDirection());
      requestLayout();
      invalidate(true);
    } 
  }
  
  @ExportedProperty(category = "text", mapping = {@IntToString(from = 0, to = "INHERIT"), @IntToString(from = 1, to = "FIRST_STRONG"), @IntToString(from = 2, to = "ANY_RTL"), @IntToString(from = 3, to = "LTR"), @IntToString(from = 4, to = "RTL"), @IntToString(from = 5, to = "LOCALE"), @IntToString(from = 6, to = "FIRST_STRONG_LTR"), @IntToString(from = 7, to = "FIRST_STRONG_RTL")})
  public int getTextDirection() {
    return (this.mPrivateFlags2 & 0x1C00) >> 10;
  }
  
  public boolean resolveTextDirection() {
    this.mPrivateFlags2 &= 0xFFFFE1FF;
    if (hasRtlSupport()) {
      int i = getRawTextDirection();
      switch (i) {
        default:
          this.mPrivateFlags2 |= 0x400;
          this.mPrivateFlags2 |= 0x200;
          return true;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
          this.mPrivateFlags2 |= i << 10;
          this.mPrivateFlags2 |= 0x200;
          return true;
        case 0:
          break;
      } 
      if (!canResolveTextDirection()) {
        this.mPrivateFlags2 |= 0x400;
        return false;
      } 
      try {
        if (this.mParent != null && !this.mParent.isTextDirectionResolved()) {
          this.mPrivateFlags2 |= 0x400;
          return false;
        } 
        try {
          i = this.mParent.getTextDirection();
        } catch (AbstractMethodError abstractMethodError) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mParent.getClass().getSimpleName());
          stringBuilder.append(" does not fully implement ViewParent");
          Log.e("View", stringBuilder.toString(), abstractMethodError);
          i = 3;
        } 
        switch (i) {
          default:
            this.mPrivateFlags2 |= 0x400;
            this.mPrivateFlags2 |= 0x200;
            return true;
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
          case 7:
            break;
        } 
        this.mPrivateFlags2 |= i << 10;
      } catch (AbstractMethodError abstractMethodError) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mParent.getClass().getSimpleName());
        stringBuilder.append(" does not fully implement ViewParent");
        Log.e("View", stringBuilder.toString(), abstractMethodError);
        this.mPrivateFlags2 |= 0x600;
        return true;
      } 
    } else {
      this.mPrivateFlags2 |= 0x400;
    } 
    this.mPrivateFlags2 |= 0x200;
    return true;
  }
  
  public boolean canResolveTextDirection() {
    if (getRawTextDirection() != 0)
      return true; 
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      try {
        return viewParent.canResolveTextDirection();
      } catch (AbstractMethodError abstractMethodError) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mParent.getClass().getSimpleName());
        stringBuilder.append(" does not fully implement ViewParent");
        Log.e("View", stringBuilder.toString(), abstractMethodError);
      }  
    return false;
  }
  
  public void resetResolvedTextDirection() {
    int i = this.mPrivateFlags2 & 0xFFFFE1FF;
    this.mPrivateFlags2 = i | 0x400;
  }
  
  public boolean isTextDirectionInherited() {
    boolean bool;
    if (getRawTextDirection() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTextDirectionResolved() {
    boolean bool;
    if ((this.mPrivateFlags2 & 0x200) == 512) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @ExportedProperty(category = "text", mapping = {@IntToString(from = 0, to = "INHERIT"), @IntToString(from = 1, to = "GRAVITY"), @IntToString(from = 2, to = "TEXT_START"), @IntToString(from = 3, to = "TEXT_END"), @IntToString(from = 4, to = "CENTER"), @IntToString(from = 5, to = "VIEW_START"), @IntToString(from = 6, to = "VIEW_END")})
  public int getRawTextAlignment() {
    return (this.mPrivateFlags2 & 0xE000) >> 13;
  }
  
  public void setTextAlignment(int paramInt) {
    if (paramInt != getRawTextAlignment()) {
      this.mPrivateFlags2 &= 0xFFFF1FFF;
      resetResolvedTextAlignment();
      this.mPrivateFlags2 |= paramInt << 13 & 0xE000;
      resolveTextAlignment();
      onRtlPropertiesChanged(getLayoutDirection());
      requestLayout();
      invalidate(true);
    } 
  }
  
  @ExportedProperty(category = "text", mapping = {@IntToString(from = 0, to = "INHERIT"), @IntToString(from = 1, to = "GRAVITY"), @IntToString(from = 2, to = "TEXT_START"), @IntToString(from = 3, to = "TEXT_END"), @IntToString(from = 4, to = "CENTER"), @IntToString(from = 5, to = "VIEW_START"), @IntToString(from = 6, to = "VIEW_END")})
  public int getTextAlignment() {
    return (this.mPrivateFlags2 & 0xE0000) >> 17;
  }
  
  public boolean resolveTextAlignment() {
    this.mPrivateFlags2 &= 0xFFF0FFFF;
    if (hasRtlSupport()) {
      int i = getRawTextAlignment();
      switch (i) {
        default:
          this.mPrivateFlags2 |= 0x20000;
          this.mPrivateFlags2 |= 0x10000;
          return true;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
          this.mPrivateFlags2 |= i << 17;
          this.mPrivateFlags2 |= 0x10000;
          return true;
        case 0:
          break;
      } 
      if (!canResolveTextAlignment()) {
        this.mPrivateFlags2 |= 0x20000;
        return false;
      } 
      try {
        if (!this.mParent.isTextAlignmentResolved()) {
          this.mPrivateFlags2 = 0x20000 | this.mPrivateFlags2;
          return false;
        } 
        try {
          i = this.mParent.getTextAlignment();
        } catch (AbstractMethodError abstractMethodError) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mParent.getClass().getSimpleName());
          stringBuilder.append(" does not fully implement ViewParent");
          Log.e("View", stringBuilder.toString(), abstractMethodError);
          i = 1;
        } 
        switch (i) {
          default:
            this.mPrivateFlags2 |= 0x20000;
            this.mPrivateFlags2 |= 0x10000;
            return true;
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
            break;
        } 
        this.mPrivateFlags2 |= i << 17;
      } catch (AbstractMethodError abstractMethodError) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mParent.getClass().getSimpleName());
        stringBuilder.append(" does not fully implement ViewParent");
        Log.e("View", stringBuilder.toString(), abstractMethodError);
        this.mPrivateFlags2 |= 0x30000;
        return true;
      } 
    } else {
      this.mPrivateFlags2 |= 0x20000;
    } 
    this.mPrivateFlags2 |= 0x10000;
    return true;
  }
  
  public boolean canResolveTextAlignment() {
    if (getRawTextAlignment() != 0)
      return true; 
    ViewParent viewParent = this.mParent;
    if (viewParent != null)
      try {
        return viewParent.canResolveTextAlignment();
      } catch (AbstractMethodError abstractMethodError) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mParent.getClass().getSimpleName());
        stringBuilder.append(" does not fully implement ViewParent");
        Log.e("View", stringBuilder.toString(), abstractMethodError);
      }  
    return false;
  }
  
  public void resetResolvedTextAlignment() {
    int i = this.mPrivateFlags2 & 0xFFF0FFFF;
    this.mPrivateFlags2 = i | 0x20000;
  }
  
  public boolean isTextAlignmentInherited() {
    boolean bool;
    if (getRawTextAlignment() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTextAlignmentResolved() {
    boolean bool;
    if ((this.mPrivateFlags2 & 0x10000) == 65536) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int generateViewId() {
    while (true) {
      int i = sNextGeneratedId.get();
      int j = i + 1;
      int k = j;
      if (j > 16777215)
        k = 1; 
      if (sNextGeneratedId.compareAndSet(i, k))
        return i; 
    } 
  }
  
  private static boolean isViewIdGenerated(int paramInt) {
    boolean bool;
    if ((0xFF000000 & paramInt) == 0 && (0xFFFFFF & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void captureTransitioningViews(List<View> paramList) {
    if (getVisibility() == 0)
      paramList.add(this); 
  }
  
  public void findNamedViews(Map<String, View> paramMap) {
    if (getVisibility() == 0 || this.mGhostView != null) {
      String str = getTransitionName();
      if (str != null)
        paramMap.put(str, this); 
    } 
  }
  
  public PointerIcon onResolvePointerIcon(MotionEvent paramMotionEvent, int paramInt) {
    float f1 = paramMotionEvent.getX(paramInt);
    float f2 = paramMotionEvent.getY(paramInt);
    if (isDraggingScrollBar() || isOnScrollbarThumb(f1, f2))
      return PointerIcon.getSystemIcon(this.mContext, 1000); 
    return this.mPointerIcon;
  }
  
  public void setPointerIcon(PointerIcon paramPointerIcon) {
    this.mPointerIcon = paramPointerIcon;
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null || attachInfo.mHandlingPointerEvent)
      return; 
    try {
      this.mAttachInfo.mSession.updatePointerIcon(this.mAttachInfo.mWindow);
    } catch (RemoteException remoteException) {}
  }
  
  public PointerIcon getPointerIcon() {
    return this.mPointerIcon;
  }
  
  public boolean hasPointerCapture() {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl == null)
      return false; 
    return viewRootImpl.hasPointerCapture();
  }
  
  public void requestPointerCapture() {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl != null)
      viewRootImpl.requestPointerCapture(true); 
  }
  
  public void releasePointerCapture() {
    ViewRootImpl viewRootImpl = getViewRootImpl();
    if (viewRootImpl != null)
      viewRootImpl.requestPointerCapture(false); 
  }
  
  public void onPointerCaptureChange(boolean paramBoolean) {}
  
  public void dispatchPointerCaptureChanged(boolean paramBoolean) {
    onPointerCaptureChange(paramBoolean);
  }
  
  public boolean onCapturedPointerEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public void setOnCapturedPointerListener(OnCapturedPointerListener paramOnCapturedPointerListener) {
    (getListenerInfo()).mOnCapturedPointerListener = paramOnCapturedPointerListener;
  }
  
  class MeasureSpec {
    public static final int AT_MOST = -2147483648;
    
    public static final int EXACTLY = 1073741824;
    
    private static final int MODE_MASK = -1073741824;
    
    private static final int MODE_SHIFT = 30;
    
    public static final int UNSPECIFIED = 0;
    
    public static int makeMeasureSpec(int param1Int1, int param1Int2) {
      if (View.sUseBrokenMakeMeasureSpec)
        return param1Int1 + param1Int2; 
      return 0x3FFFFFFF & param1Int1 | 0xC0000000 & param1Int2;
    }
    
    public static int makeSafeMeasureSpec(int param1Int1, int param1Int2) {
      if (View.sUseZeroUnspecifiedMeasureSpec && param1Int2 == 0)
        return 0; 
      return makeMeasureSpec(param1Int1, param1Int2);
    }
    
    public static int getMode(int param1Int) {
      return 0xC0000000 & param1Int;
    }
    
    public static int getSize(int param1Int) {
      return 0x3FFFFFFF & param1Int;
    }
    
    static int adjust(int param1Int1, int param1Int2) {
      int i = getMode(param1Int1);
      int j = getSize(param1Int1);
      if (i == 0)
        return makeMeasureSpec(j, 0); 
      int k = j + param1Int2;
      j = k;
      if (k < 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("MeasureSpec.adjust: new size would be negative! (");
        stringBuilder.append(k);
        stringBuilder.append(") spec: ");
        stringBuilder.append(toString(param1Int1));
        stringBuilder.append(" delta: ");
        stringBuilder.append(param1Int2);
        String str = stringBuilder.toString();
        Log.e("View", str);
        j = 0;
      } 
      return makeMeasureSpec(j, i);
    }
    
    public static String toString(int param1Int) {
      int i = getMode(param1Int);
      param1Int = getSize(param1Int);
      StringBuilder stringBuilder = new StringBuilder("MeasureSpec: ");
      if (i == 0) {
        stringBuilder.append("UNSPECIFIED ");
      } else if (i == 1073741824) {
        stringBuilder.append("EXACTLY ");
      } else if (i == Integer.MIN_VALUE) {
        stringBuilder.append("AT_MOST ");
      } else {
        stringBuilder.append(i);
        stringBuilder.append(" ");
      } 
      stringBuilder.append(param1Int);
      return stringBuilder.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface MeasureSpecMode {}
  }
  
  class CheckForLongPress implements Runnable {
    private int mClassification;
    
    private boolean mOriginalPressedState;
    
    private int mOriginalWindowAttachCount;
    
    private float mX;
    
    private float mY;
    
    final View this$0;
    
    private CheckForLongPress() {}
    
    public void run() {
      if (this.mOriginalPressedState == View.this.isPressed() && View.this.mParent != null && this.mOriginalWindowAttachCount == View.this.mWindowAttachCount) {
        View.this.recordGestureClassification(this.mClassification);
        if (!SystemProperties.getBoolean("sys.oppo.justshot", false)) {
          if (View.this.performLongClick(this.mX, this.mY))
            View.access$3302(View.this, true); 
        } else {
          View.this.resetPressedState();
          View.this.setPressed(false);
          View.this.removeTapCallback();
          View.this.removeLongPressCallback();
        } 
      } 
    }
    
    public void setAnchor(float param1Float1, float param1Float2) {
      this.mX = param1Float1;
      this.mY = param1Float2;
    }
    
    public void rememberWindowAttachCount() {
      this.mOriginalWindowAttachCount = View.this.mWindowAttachCount;
    }
    
    public void rememberPressedState() {
      this.mOriginalPressedState = View.this.isPressed();
    }
    
    public void setClassification(int param1Int) {
      this.mClassification = param1Int;
    }
  }
  
  class CheckForTap implements Runnable {
    final View this$0;
    
    public float x;
    
    public float y;
    
    private CheckForTap() {}
    
    public void run() {
      View view = View.this;
      view.mPrivateFlags &= 0xFDFFFFFF;
      View.this.setPressed(true, this.x, this.y);
      long l = (ViewConfiguration.getLongPressTimeout() - ViewConfiguration.getTapTimeout());
      View.this.checkForLongClick(l, this.x, this.y, 3);
    }
  }
  
  class PerformClick implements Runnable {
    final View this$0;
    
    private PerformClick() {}
    
    public void run() {
      View.this.recordGestureClassification(1);
      View.this.performClickInternal();
    }
  }
  
  private void recordGestureClassification(int paramInt) {
    if (paramInt == 0)
      return; 
    FrameworkStatsLog.write(177, getClass().getName(), paramInt);
  }
  
  public ViewPropertyAnimator animate() {
    if (this.mAnimator == null)
      if (shouldBoostAnimation()) {
        this.mAnimator = (ViewPropertyAnimator)new Object(this, this);
      } else {
        this.mAnimator = new ViewPropertyAnimator(this);
      }  
    return this.mAnimator;
  }
  
  public final void setTransitionName(String paramString) {
    this.mTransitionName = paramString;
  }
  
  @ExportedProperty
  public String getTransitionName() {
    return this.mTransitionName;
  }
  
  public void requestKeyboardShortcuts(List<KeyboardShortcutGroup> paramList, int paramInt) {}
  
  class UnsetPressedState implements Runnable {
    final View this$0;
    
    private UnsetPressedState() {}
    
    public void run() {
      View.this.setPressed(false);
    }
  }
  
  class VisibilityChangeForAutofillHandler extends Handler {
    private final AutofillManager mAfm;
    
    private final View mView;
    
    private VisibilityChangeForAutofillHandler(View this$0, View param1View) {
      this.mAfm = (AutofillManager)this$0;
      this.mView = param1View;
    }
    
    public void handleMessage(Message param1Message) {
      AutofillManager autofillManager = this.mAfm;
      View view = this.mView;
      autofillManager.notifyViewVisibilityChanged(view, view.isShown());
    }
  }
  
  class BaseSavedState extends AbsSavedState {
    static final int AUTOFILL_ID = 4;
    
    public BaseSavedState() {
      this((Parcel)this$0, null);
    }
    
    public BaseSavedState(ClassLoader param1ClassLoader) {
      super((Parcel)this$0, param1ClassLoader);
      this.mSavedData = this$0.readInt();
      this.mStartActivityRequestWhoSaved = this$0.readString();
      this.mIsAutofilled = this$0.readBoolean();
      this.mHideHighlight = this$0.readBoolean();
      this.mAutofillViewId = this$0.readInt();
    }
    
    public BaseSavedState() {
      super((Parcelable)this$0);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.mSavedData);
      param1Parcel.writeString(this.mStartActivityRequestWhoSaved);
      param1Parcel.writeBoolean(this.mIsAutofilled);
      param1Parcel.writeBoolean(this.mHideHighlight);
      param1Parcel.writeInt(this.mAutofillViewId);
    }
    
    public static final Parcelable.Creator<BaseSavedState> CREATOR = (Parcelable.Creator<BaseSavedState>)new Parcelable.ClassLoaderCreator<BaseSavedState>() {
        public View.BaseSavedState createFromParcel(Parcel param2Parcel) {
          return new View.BaseSavedState();
        }
        
        public View.BaseSavedState createFromParcel(Parcel param2Parcel, ClassLoader param2ClassLoader) {
          return new View.BaseSavedState(param2ClassLoader);
        }
        
        public View.BaseSavedState[] newArray(int param2Int) {
          return new View.BaseSavedState[param2Int];
        }
      };
    
    static final int IS_AUTOFILLED = 2;
    
    static final int START_ACTIVITY_REQUESTED_WHO_SAVED = 1;
    
    int mAutofillViewId;
    
    boolean mHideHighlight;
    
    boolean mIsAutofilled;
    
    int mSavedData;
    
    String mStartActivityRequestWhoSaved;
  }
  
  class AttachInfo {
    static interface Callbacks {
      boolean performHapticFeedback(int param2Int, boolean param2Boolean);
      
      void playSoundEffect(int param2Int);
    }
    
    static class InvalidateInfo {
      private static final int POOL_LIMIT = 10;
      
      private static final Pools.SynchronizedPool<InvalidateInfo> sPool = new Pools.SynchronizedPool<>(10);
      
      int bottom;
      
      int left;
      
      int right;
      
      View target;
      
      int top;
      
      public static InvalidateInfo obtain() {
        InvalidateInfo invalidateInfo = sPool.acquire();
        if (invalidateInfo == null)
          invalidateInfo = new InvalidateInfo(); 
        return invalidateInfo;
      }
      
      public void recycle() {
        this.target = null;
        sPool.release(this);
      }
    }
    
    int mDisplayState = 0;
    
    final Rect mContentInsets = new Rect();
    
    final Rect mVisibleInsets = new Rect();
    
    final Rect mStableInsets = new Rect();
    
    final Rect mCaptionInsets = new Rect();
    
    final DisplayCutout.ParcelableWrapper mDisplayCutout = new DisplayCutout.ParcelableWrapper(DisplayCutout.NO_CUTOUT);
    
    final ViewTreeObserver.InternalInsetsInfo mGivenInternalInsets = new ViewTreeObserver.InternalInsetsInfo();
    
    final ArrayList<View> mScrollContainers = new ArrayList<>();
    
    final KeyEvent.DispatcherState mKeyDispatchState = new KeyEvent.DispatcherState();
    
    final Point mLocationInParentDisplay = new Point();
    
    final int[] mTransparentLocation = new int[2];
    
    final int[] mInvalidateChildLocation = new int[2];
    
    final int[] mTmpLocation = new int[2];
    
    final float[] mTmpTransformLocation = new float[2];
    
    final Rect mTmpInvalRect = new Rect();
    
    final RectF mTmpTransformRect = new RectF();
    
    final RectF mTmpTransformRect1 = new RectF();
    
    final List<RectF> mTmpRectList = new ArrayList<>();
    
    final Matrix mTmpMatrix = new Matrix();
    
    final Transformation mTmpTransformation = new Transformation();
    
    final Outline mTmpOutline = new Outline();
    
    final ArrayList<View> mTempArrayList = new ArrayList<>(24);
    
    int mAccessibilityWindowId = -1;
    
    boolean mDebugLayout = ((Boolean)DisplayProperties.debug_layout().orElse(Boolean.valueOf(false))).booleanValue();
    
    final Point mPoint = new Point();
    
    int mAccessibilityFetchFlags;
    
    Drawable mAccessibilityFocusDrawable;
    
    boolean mAlwaysConsumeSystemBars;
    
    float mApplicationScale;
    
    Drawable mAutofilledDrawable;
    
    Canvas mCanvas;
    
    SparseArray<ArrayList<Object>> mContentCaptureEvents;
    
    ContentCaptureManager mContentCaptureManager;
    
    Window.OnContentApplyWindowInsetsListener mContentOnApplyWindowInsetsListener;
    
    int mDisabledSystemUiVisibility;
    
    Display mDisplay;
    
    public Surface mDragSurface;
    
    IBinder mDragToken;
    
    long mDrawingTime;
    
    boolean mForceReportNewAttributes;
    
    final Handler mHandler;
    
    boolean mHandlingPointerEvent;
    
    boolean mHardwareAccelerated;
    
    boolean mHardwareAccelerationRequested;
    
    boolean mHasNonEmptyGivenInternalInsets;
    
    boolean mHasSystemUiListeners;
    
    boolean mHasWindowFocus;
    
    IWindowId mIWindowId;
    
    boolean mInTouchMode;
    
    boolean mKeepScreenOn;
    
    int mLeashedParentAccessibilityViewId;
    
    IBinder mLeashedParentToken;
    
    boolean mNeedsUpdateLightCenter;
    
    IBinder mPanelParentWindowToken;
    
    List<RenderNode> mPendingAnimatingRenderNodes;
    
    boolean mReadyForContentCaptureUpdates;
    
    boolean mRecomputeGlobalAttributes;
    
    final Callbacks mRootCallbacks;
    
    View mRootView;
    
    boolean mScalingRequired;
    
    Matrix mScreenMatrixInEmbeddedHierarchy;
    
    ScrollCaptureInternal mScrollCaptureInternal;
    
    final IWindowSession mSession;
    
    int mSystemUiVisibility;
    
    ThreadedRenderer mThreadedRenderer;
    
    View mTooltipHost;
    
    final ViewTreeObserver mTreeObserver;
    
    boolean mUnbufferedDispatchRequested;
    
    boolean mUse32BitDrawingCache;
    
    View mViewRequestingLayout;
    
    final ViewRootImpl mViewRootImpl;
    
    boolean mViewScrollChanged;
    
    boolean mViewVisibilityChanged;
    
    final IWindow mWindow;
    
    WindowId mWindowId;
    
    int mWindowLeft;
    
    final IBinder mWindowToken;
    
    int mWindowTop;
    
    int mWindowVisibility;
    
    AttachInfo(View this$0, IWindow param1IWindow, Display param1Display, ViewRootImpl param1ViewRootImpl, Handler param1Handler, Callbacks param1Callbacks, Context param1Context) {
      this.mSession = (IWindowSession)this$0;
      this.mWindow = param1IWindow;
      this.mWindowToken = param1IWindow.asBinder();
      this.mDisplay = param1Display;
      this.mViewRootImpl = param1ViewRootImpl;
      this.mHandler = param1Handler;
      this.mRootCallbacks = param1Callbacks;
      this.mTreeObserver = new ViewTreeObserver(param1Context);
    }
    
    ContentCaptureManager getContentCaptureManager(Context param1Context) {
      ContentCaptureManager contentCaptureManager2 = this.mContentCaptureManager;
      if (contentCaptureManager2 != null)
        return contentCaptureManager2; 
      ContentCaptureManager contentCaptureManager1 = (ContentCaptureManager)param1Context.getSystemService(ContentCaptureManager.class);
      return contentCaptureManager1;
    }
    
    void delayNotifyContentCaptureInsetsEvent(Insets param1Insets) {
      ContentCaptureManager contentCaptureManager = this.mContentCaptureManager;
      if (contentCaptureManager == null)
        return; 
      MainContentCaptureSession mainContentCaptureSession = contentCaptureManager.getMainContentCaptureSession();
      ArrayList<Object> arrayList = ensureEvents(mainContentCaptureSession);
      arrayList.add(param1Insets);
    }
    
    private void delayNotifyContentCaptureEvent(ContentCaptureSession param1ContentCaptureSession, View param1View, boolean param1Boolean) {
      AutofillId autofillId;
      ArrayList<Object> arrayList = ensureEvents(param1ContentCaptureSession);
      if (!param1Boolean)
        autofillId = param1View.getAutofillId(); 
      arrayList.add(autofillId);
    }
    
    private ArrayList<Object> ensureEvents(ContentCaptureSession param1ContentCaptureSession) {
      if (this.mContentCaptureEvents == null)
        this.mContentCaptureEvents = new SparseArray<>(1); 
      int i = param1ContentCaptureSession.getId();
      ArrayList<Object> arrayList2 = this.mContentCaptureEvents.get(i);
      ArrayList<Object> arrayList1 = arrayList2;
      if (arrayList2 == null) {
        arrayList1 = new ArrayList();
        this.mContentCaptureEvents.put(i, arrayList1);
      } 
      return arrayList1;
    }
    
    ScrollCaptureInternal getScrollCaptureInternal() {
      if (this.mScrollCaptureInternal != null)
        this.mScrollCaptureInternal = new ScrollCaptureInternal(); 
      return this.mScrollCaptureInternal;
    }
  }
  
  class ScrollabilityCache implements Runnable {
    public final Interpolator scrollBarInterpolator = new Interpolator(1, 2);
    
    private static final float[] OPAQUE = new float[] { 255.0F };
    
    private static final float[] TRANSPARENT = new float[] { 0.0F };
    
    public int state = 0;
    
    public final Rect mScrollBarBounds = new Rect();
    
    public final Rect mScrollBarTouchBounds = new Rect();
    
    public int mScrollBarDraggingState = 0;
    
    public float mScrollBarDraggingPos = 0.0F;
    
    public static final int DRAGGING_HORIZONTAL_SCROLL_BAR = 2;
    
    public static final int DRAGGING_VERTICAL_SCROLL_BAR = 1;
    
    public static final int FADING = 2;
    
    public static final int NOT_DRAGGING = 0;
    
    public static final int OFF = 0;
    
    public static final int ON = 1;
    
    public boolean fadeScrollBars;
    
    public long fadeStartTime;
    
    public int fadingEdgeLength;
    
    public View host;
    
    public float[] interpolatorValues;
    
    private int mLastColor;
    
    public final Matrix matrix;
    
    public final Paint paint;
    
    public ScrollBarDrawable scrollBar;
    
    public int scrollBarDefaultDelayBeforeFade;
    
    public int scrollBarFadeDuration;
    
    public int scrollBarMinTouchTarget;
    
    public int scrollBarSize;
    
    public Shader shader;
    
    public ScrollabilityCache(View this$0, View param1View) {
      this.fadingEdgeLength = this$0.getScaledFadingEdgeLength();
      this.scrollBarSize = this$0.getScaledScrollBarSize();
      this.scrollBarMinTouchTarget = this$0.getScaledMinScrollbarTouchTarget();
      this.scrollBarDefaultDelayBeforeFade = ViewConfiguration.getScrollDefaultDelay();
      this.scrollBarFadeDuration = ViewConfiguration.getScrollBarFadeDuration();
      this.paint = new Paint();
      this.matrix = new Matrix();
      LinearGradient linearGradient = new LinearGradient(0.0F, 0.0F, 0.0F, 1.0F, -16777216, 0, Shader.TileMode.CLAMP);
      this.paint.setShader((Shader)linearGradient);
      this.paint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
      this.host = param1View;
    }
    
    public void setFadeColor(int param1Int) {
      if (param1Int != this.mLastColor) {
        this.mLastColor = param1Int;
        if (param1Int != 0) {
          LinearGradient linearGradient = new LinearGradient(0.0F, 0.0F, 0.0F, 1.0F, param1Int | 0xFF000000, param1Int & 0xFFFFFF, Shader.TileMode.CLAMP);
          this.paint.setShader((Shader)linearGradient);
          this.paint.setXfermode(null);
        } else {
          LinearGradient linearGradient = new LinearGradient(0.0F, 0.0F, 0.0F, 1.0F, -16777216, 0, Shader.TileMode.CLAMP);
          this.paint.setShader((Shader)linearGradient);
          this.paint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        } 
      } 
    }
    
    public void run() {
      long l = AnimationUtils.currentAnimationTimeMillis();
      if (l >= this.fadeStartTime) {
        int i = (int)l;
        Interpolator interpolator = this.scrollBarInterpolator;
        interpolator.setKeyFrame(0, i, OPAQUE);
        int j = this.scrollBarFadeDuration;
        interpolator.setKeyFrame(0 + 1, i + j, TRANSPARENT);
        this.state = 2;
        this.host.invalidate(true);
      } 
    }
  }
  
  class SendAccessibilityEventThrottle implements Runnable {
    private AccessibilityEvent mAccessibilityEvent;
    
    public volatile boolean mIsPending;
    
    final View this$0;
    
    private SendAccessibilityEventThrottle() {}
    
    public void post(AccessibilityEvent param1AccessibilityEvent) {
      updateWithAccessibilityEvent(param1AccessibilityEvent);
      if (!this.mIsPending) {
        this.mIsPending = true;
        View view = View.this;
        long l = ViewConfiguration.getSendRecurringAccessibilityEventsInterval();
        view.postDelayed(this, l);
      } 
    }
    
    public void run() {
      if (AccessibilityManager.getInstance(View.this.mContext).isEnabled())
        View.this.requestParentSendAccessibilityEvent(this.mAccessibilityEvent); 
      reset();
    }
    
    public void updateWithAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent) {
      this.mAccessibilityEvent = param1AccessibilityEvent;
    }
    
    public void reset() {
      this.mIsPending = false;
      this.mAccessibilityEvent = null;
    }
  }
  
  class SendViewScrolledAccessibilityEvent extends SendAccessibilityEventThrottle {
    public int mDeltaX;
    
    public int mDeltaY;
    
    final View this$0;
    
    private SendViewScrolledAccessibilityEvent() {}
    
    public void updateWithAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent) {
      super.updateWithAccessibilityEvent(param1AccessibilityEvent);
      this.mDeltaX += param1AccessibilityEvent.getScrollDeltaX();
      this.mDeltaY += param1AccessibilityEvent.getScrollDeltaY();
      param1AccessibilityEvent.setScrollDeltaX(this.mDeltaX);
      param1AccessibilityEvent.setScrollDeltaY(this.mDeltaY);
    }
    
    public void reset() {
      super.reset();
      this.mDeltaX = 0;
      this.mDeltaY = 0;
    }
  }
  
  private void cancel(SendAccessibilityEventThrottle paramSendAccessibilityEventThrottle) {
    if (paramSendAccessibilityEventThrottle == null || !paramSendAccessibilityEventThrottle.mIsPending)
      return; 
    removeCallbacks(paramSendAccessibilityEventThrottle);
    paramSendAccessibilityEventThrottle.reset();
  }
  
  class AccessibilityDelegate {
    public void sendAccessibilityEvent(View param1View, int param1Int) {
      param1View.sendAccessibilityEventInternal(param1Int);
    }
    
    public boolean performAccessibilityAction(View param1View, int param1Int, Bundle param1Bundle) {
      return param1View.performAccessibilityActionInternal(param1Int, param1Bundle);
    }
    
    public void sendAccessibilityEventUnchecked(View param1View, AccessibilityEvent param1AccessibilityEvent) {
      param1View.sendAccessibilityEventUncheckedInternal(param1AccessibilityEvent);
    }
    
    public boolean dispatchPopulateAccessibilityEvent(View param1View, AccessibilityEvent param1AccessibilityEvent) {
      return param1View.dispatchPopulateAccessibilityEventInternal(param1AccessibilityEvent);
    }
    
    public void onPopulateAccessibilityEvent(View param1View, AccessibilityEvent param1AccessibilityEvent) {
      param1View.onPopulateAccessibilityEventInternal(param1AccessibilityEvent);
    }
    
    public void onInitializeAccessibilityEvent(View param1View, AccessibilityEvent param1AccessibilityEvent) {
      param1View.onInitializeAccessibilityEventInternal(param1AccessibilityEvent);
    }
    
    public void onInitializeAccessibilityNodeInfo(View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      param1View.onInitializeAccessibilityNodeInfoInternal(param1AccessibilityNodeInfo);
    }
    
    public void addExtraDataToAccessibilityNodeInfo(View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo, String param1String, Bundle param1Bundle) {
      param1View.addExtraDataToAccessibilityNodeInfo(param1AccessibilityNodeInfo, param1String, param1Bundle);
    }
    
    public boolean onRequestSendAccessibilityEvent(ViewGroup param1ViewGroup, View param1View, AccessibilityEvent param1AccessibilityEvent) {
      return param1ViewGroup.onRequestSendAccessibilityEventInternal(param1View, param1AccessibilityEvent);
    }
    
    public AccessibilityNodeProvider getAccessibilityNodeProvider(View param1View) {
      return null;
    }
    
    public AccessibilityNodeInfo createAccessibilityNodeInfo(View param1View) {
      return param1View.createAccessibilityNodeInfoInternal();
    }
  }
  
  class MatchIdPredicate implements Predicate<View> {
    public int mId;
    
    private MatchIdPredicate() {}
    
    public boolean test(View param1View) {
      boolean bool;
      if (param1View.mID == this.mId) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  class MatchLabelForPredicate implements Predicate<View> {
    private int mLabeledId;
    
    private MatchLabelForPredicate() {}
    
    public boolean test(View param1View) {
      boolean bool;
      if (param1View.mLabelForId == this.mLabeledId) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  public int getScrollCaptureHint() {
    return (this.mPrivateFlags4 & 0x1C00) >> 10;
  }
  
  public void setScrollCaptureHint(int paramInt) {
    int i = this.mPrivateFlags4 & 0xFFFFE3FF;
    this.mPrivateFlags4 = i | paramInt << 10 & 0x1C00;
  }
  
  public void setScrollCaptureCallback(ScrollCaptureCallback paramScrollCaptureCallback) {
    (getListenerInfo()).mScrollCaptureCallback = paramScrollCaptureCallback;
  }
  
  public ScrollCaptureCallback createScrollCaptureCallbackInternal(Rect paramRect, Point paramPoint) {
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo == null)
      return null; 
    if (attachInfo.mScrollCaptureInternal == null)
      this.mAttachInfo.mScrollCaptureInternal = new ScrollCaptureInternal(); 
    return this.mAttachInfo.mScrollCaptureInternal.requestCallback(this, paramRect, paramPoint);
  }
  
  public void dispatchScrollCaptureSearch(Rect paramRect, Point paramPoint, Queue<ScrollCaptureTarget> paramQueue) {
    ScrollCaptureCallback scrollCaptureCallback1;
    int i = getScrollCaptureHint();
    if ((i & 0x1) != 0)
      return; 
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo == null) {
      listenerInfo = null;
    } else {
      scrollCaptureCallback1 = listenerInfo.mScrollCaptureCallback;
    } 
    ScrollCaptureCallback scrollCaptureCallback2 = scrollCaptureCallback1;
    if (scrollCaptureCallback1 == null)
      scrollCaptureCallback2 = createScrollCaptureCallbackInternal(paramRect, paramPoint); 
    if (scrollCaptureCallback2 != null) {
      paramPoint = new Point(paramPoint.x, paramPoint.y);
      paramRect = new Rect(paramRect);
      paramQueue.add(new ScrollCaptureTarget(this, paramRect, paramPoint, scrollCaptureCallback2));
    } 
  }
  
  private static void dumpFlags() {
    HashMap<String, String> hashMap = Maps.newHashMap();
    try {
      for (Field field : View.class.getDeclaredFields()) {
        int i = field.getModifiers();
        if (Modifier.isStatic(i) && Modifier.isFinal(i))
          if (field.getType().equals(int.class)) {
            i = field.getInt(null);
            dumpFlag(hashMap, field.getName(), i);
          } else if (field.getType().equals(int[].class)) {
            int[] arrayOfInt = (int[])field.get(null);
            for (i = 0; i < arrayOfInt.length; i++) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append(field.getName());
              stringBuilder.append("[");
              stringBuilder.append(i);
              stringBuilder.append("]");
              dumpFlag(hashMap, stringBuilder.toString(), arrayOfInt[i]);
            } 
          }  
      } 
      ArrayList<Comparable> arrayList = Lists.newArrayList();
      arrayList.addAll(hashMap.keySet());
      Collections.sort(arrayList);
      for (String str : arrayList)
        Log.d("View", hashMap.get(str)); 
      return;
    } catch (IllegalAccessException illegalAccessException) {
      throw new RuntimeException(illegalAccessException);
    } 
  }
  
  private static void dumpFlag(HashMap<String, String> paramHashMap, String paramString, int paramInt) {
    String str1 = String.format("%32s", new Object[] { Integer.toBinaryString(paramInt) }).replace('0', ' ');
    paramInt = paramString.indexOf('_');
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt > 0) {
      str2 = paramString.substring(0, paramInt);
    } else {
      str2 = paramString;
    } 
    stringBuilder.append(str2);
    stringBuilder.append(str1);
    stringBuilder.append(paramString);
    String str2 = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append(str1);
    stringBuilder.append(" ");
    stringBuilder.append(paramString);
    paramString = stringBuilder.toString();
    paramHashMap.put(str2, paramString);
  }
  
  public void encode(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    paramViewHierarchyEncoder.beginObject(this);
    encodeProperties(paramViewHierarchyEncoder);
    paramViewHierarchyEncoder.endObject();
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    float f;
    byte b1;
    Object object = ViewDebug.resolveId(getContext(), this.mID);
    if (object instanceof String) {
      paramViewHierarchyEncoder.addProperty("id", (String)object);
    } else {
      paramViewHierarchyEncoder.addProperty("id", this.mID);
    } 
    object = this.mTransformationInfo;
    if (object != null) {
      f = ((TransformationInfo)object).mAlpha;
    } else {
      f = 0.0F;
    } 
    paramViewHierarchyEncoder.addProperty("misc:transformation.alpha", f);
    paramViewHierarchyEncoder.addProperty("misc:transitionName", getTransitionName());
    paramViewHierarchyEncoder.addProperty("layout:left", this.mLeft);
    paramViewHierarchyEncoder.addProperty("layout:right", this.mRight);
    paramViewHierarchyEncoder.addProperty("layout:top", this.mTop);
    paramViewHierarchyEncoder.addProperty("layout:bottom", this.mBottom);
    paramViewHierarchyEncoder.addProperty("layout:width", getWidth());
    paramViewHierarchyEncoder.addProperty("layout:height", getHeight());
    paramViewHierarchyEncoder.addProperty("layout:layoutDirection", getLayoutDirection());
    paramViewHierarchyEncoder.addProperty("layout:layoutRtl", isLayoutRtl());
    paramViewHierarchyEncoder.addProperty("layout:hasTransientState", hasTransientState());
    paramViewHierarchyEncoder.addProperty("layout:baseline", getBaseline());
    object = getLayoutParams();
    if (object != null) {
      paramViewHierarchyEncoder.addPropertyKey("layoutParams");
      object.encode(paramViewHierarchyEncoder);
    } 
    paramViewHierarchyEncoder.addProperty("scrolling:scrollX", this.mScrollX);
    paramViewHierarchyEncoder.addProperty("scrolling:scrollY", this.mScrollY);
    paramViewHierarchyEncoder.addProperty("padding:paddingLeft", this.mPaddingLeft);
    paramViewHierarchyEncoder.addProperty("padding:paddingRight", this.mPaddingRight);
    paramViewHierarchyEncoder.addProperty("padding:paddingTop", this.mPaddingTop);
    paramViewHierarchyEncoder.addProperty("padding:paddingBottom", this.mPaddingBottom);
    paramViewHierarchyEncoder.addProperty("padding:userPaddingRight", this.mUserPaddingRight);
    paramViewHierarchyEncoder.addProperty("padding:userPaddingLeft", this.mUserPaddingLeft);
    paramViewHierarchyEncoder.addProperty("padding:userPaddingBottom", this.mUserPaddingBottom);
    paramViewHierarchyEncoder.addProperty("padding:userPaddingStart", this.mUserPaddingStart);
    paramViewHierarchyEncoder.addProperty("padding:userPaddingEnd", this.mUserPaddingEnd);
    paramViewHierarchyEncoder.addProperty("measurement:minHeight", this.mMinHeight);
    paramViewHierarchyEncoder.addProperty("measurement:minWidth", this.mMinWidth);
    paramViewHierarchyEncoder.addProperty("measurement:measuredWidth", this.mMeasuredWidth);
    paramViewHierarchyEncoder.addProperty("measurement:measuredHeight", this.mMeasuredHeight);
    paramViewHierarchyEncoder.addProperty("drawing:elevation", getElevation());
    paramViewHierarchyEncoder.addProperty("drawing:translationX", getTranslationX());
    paramViewHierarchyEncoder.addProperty("drawing:translationY", getTranslationY());
    paramViewHierarchyEncoder.addProperty("drawing:translationZ", getTranslationZ());
    paramViewHierarchyEncoder.addProperty("drawing:rotation", getRotation());
    paramViewHierarchyEncoder.addProperty("drawing:rotationX", getRotationX());
    paramViewHierarchyEncoder.addProperty("drawing:rotationY", getRotationY());
    paramViewHierarchyEncoder.addProperty("drawing:scaleX", getScaleX());
    paramViewHierarchyEncoder.addProperty("drawing:scaleY", getScaleY());
    paramViewHierarchyEncoder.addProperty("drawing:pivotX", getPivotX());
    paramViewHierarchyEncoder.addProperty("drawing:pivotY", getPivotY());
    object = this.mClipBounds;
    if (object == null) {
      object = null;
    } else {
      object = object.toString();
    } 
    paramViewHierarchyEncoder.addProperty("drawing:clipBounds", (String)object);
    paramViewHierarchyEncoder.addProperty("drawing:opaque", isOpaque());
    paramViewHierarchyEncoder.addProperty("drawing:alpha", getAlpha());
    paramViewHierarchyEncoder.addProperty("drawing:transitionAlpha", getTransitionAlpha());
    paramViewHierarchyEncoder.addProperty("drawing:shadow", hasShadow());
    paramViewHierarchyEncoder.addProperty("drawing:solidColor", getSolidColor());
    paramViewHierarchyEncoder.addProperty("drawing:layerType", this.mLayerType);
    paramViewHierarchyEncoder.addProperty("drawing:willNotDraw", willNotDraw());
    paramViewHierarchyEncoder.addProperty("drawing:hardwareAccelerated", isHardwareAccelerated());
    paramViewHierarchyEncoder.addProperty("drawing:willNotCacheDrawing", willNotCacheDrawing());
    paramViewHierarchyEncoder.addProperty("drawing:drawingCacheEnabled", isDrawingCacheEnabled());
    paramViewHierarchyEncoder.addProperty("drawing:overlappingRendering", hasOverlappingRendering());
    paramViewHierarchyEncoder.addProperty("drawing:outlineAmbientShadowColor", getOutlineAmbientShadowColor());
    paramViewHierarchyEncoder.addProperty("drawing:outlineSpotShadowColor", getOutlineSpotShadowColor());
    paramViewHierarchyEncoder.addProperty("focus:hasFocus", hasFocus());
    paramViewHierarchyEncoder.addProperty("focus:isFocused", isFocused());
    paramViewHierarchyEncoder.addProperty("focus:focusable", getFocusable());
    paramViewHierarchyEncoder.addProperty("focus:isFocusable", isFocusable());
    paramViewHierarchyEncoder.addProperty("focus:isFocusableInTouchMode", isFocusableInTouchMode());
    paramViewHierarchyEncoder.addProperty("misc:clickable", isClickable());
    paramViewHierarchyEncoder.addProperty("misc:pressed", isPressed());
    paramViewHierarchyEncoder.addProperty("misc:selected", isSelected());
    paramViewHierarchyEncoder.addProperty("misc:touchMode", isInTouchMode());
    paramViewHierarchyEncoder.addProperty("misc:hovered", isHovered());
    paramViewHierarchyEncoder.addProperty("misc:activated", isActivated());
    paramViewHierarchyEncoder.addProperty("misc:visibility", getVisibility());
    paramViewHierarchyEncoder.addProperty("misc:fitsSystemWindows", getFitsSystemWindows());
    paramViewHierarchyEncoder.addProperty("misc:filterTouchesWhenObscured", getFilterTouchesWhenObscured());
    paramViewHierarchyEncoder.addProperty("misc:enabled", isEnabled());
    paramViewHierarchyEncoder.addProperty("misc:soundEffectsEnabled", isSoundEffectsEnabled());
    paramViewHierarchyEncoder.addProperty("misc:hapticFeedbackEnabled", isHapticFeedbackEnabled());
    object = getContext().getTheme();
    if (object != null) {
      paramViewHierarchyEncoder.addPropertyKey("theme");
      object.encode(paramViewHierarchyEncoder);
    } 
    object = this.mAttributes;
    if (object != null) {
      b1 = object.length;
    } else {
      b1 = 0;
    } 
    paramViewHierarchyEncoder.addProperty("meta:__attrCount__", b1 / 2);
    for (byte b2 = 0; b2 < b1; b2 += 2) {
      object = new StringBuilder();
      object.append("meta:__attr__");
      object.append(this.mAttributes[b2]);
      paramViewHierarchyEncoder.addProperty(object.toString(), this.mAttributes[b2 + 1]);
    } 
    paramViewHierarchyEncoder.addProperty("misc:scrollBarStyle", getScrollBarStyle());
    paramViewHierarchyEncoder.addProperty("text:textDirection", getTextDirection());
    paramViewHierarchyEncoder.addProperty("text:textAlignment", getTextAlignment());
    object = getContentDescription();
    if (object == null) {
      object = "";
    } else {
      object = object.toString();
    } 
    paramViewHierarchyEncoder.addUserProperty("accessibility:contentDescription", (String)object);
    paramViewHierarchyEncoder.addProperty("accessibility:labelFor", getLabelFor());
    paramViewHierarchyEncoder.addProperty("accessibility:importantForAccessibility", getImportantForAccessibility());
  }
  
  boolean shouldDrawRoundScrollbar() {
    boolean bool = this.mResources.getConfiguration().isScreenRound();
    boolean bool1 = false;
    if (!bool || this.mAttachInfo == null)
      return false; 
    View view = getRootView();
    WindowInsets windowInsets = getRootWindowInsets();
    int i = getHeight();
    int j = getWidth();
    int k = view.getHeight();
    int m = view.getWidth();
    if (i != k || j != m)
      return false; 
    getLocationInWindow(this.mAttachInfo.mTmpLocation);
    if (this.mAttachInfo.mTmpLocation[0] == windowInsets.getStableInsetLeft()) {
      j = this.mAttachInfo.mTmpLocation[1];
      if (j == windowInsets.getStableInsetTop())
        bool1 = true; 
    } 
    return bool1;
  }
  
  public void setTooltipText(CharSequence paramCharSequence) {
    if (TextUtils.isEmpty(paramCharSequence)) {
      setFlags(0, 1073741824);
      hideTooltip();
      this.mTooltipInfo = null;
    } else {
      setFlags(1073741824, 1073741824);
      if (this.mTooltipInfo == null) {
        TooltipInfo tooltipInfo = new TooltipInfo();
        tooltipInfo.mShowTooltipRunnable = new _$$Lambda$View$llq76MkPXP4bNcb9oJt_msw0fnQ(this);
        this.mTooltipInfo.mHideTooltipRunnable = new _$$Lambda$QI1s392qW8l6mC24bcy9050SkuY(this);
        this.mTooltipInfo.mHoverSlop = ViewConfiguration.get(this.mContext).getScaledHoverSlop();
        this.mTooltipInfo.clearAnchorPos();
      } 
      this.mTooltipInfo.mTooltipText = paramCharSequence;
    } 
  }
  
  public void setTooltip(CharSequence paramCharSequence) {
    setTooltipText(paramCharSequence);
  }
  
  public CharSequence getTooltipText() {
    TooltipInfo tooltipInfo = this.mTooltipInfo;
    if (tooltipInfo != null) {
      CharSequence charSequence = tooltipInfo.mTooltipText;
    } else {
      tooltipInfo = null;
    } 
    return (CharSequence)tooltipInfo;
  }
  
  public CharSequence getTooltip() {
    return getTooltipText();
  }
  
  private boolean showTooltip(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (this.mAttachInfo == null || this.mTooltipInfo == null)
      return false; 
    if (paramBoolean && (this.mViewFlags & 0x20) != 0)
      return false; 
    if (TextUtils.isEmpty(this.mTooltipInfo.mTooltipText))
      return false; 
    hideTooltip();
    this.mTooltipInfo.mTooltipFromLongClick = paramBoolean;
    this.mTooltipInfo.mTooltipPopup = new TooltipPopup(getContext());
    if ((this.mPrivateFlags3 & 0x20000) == 131072) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    this.mTooltipInfo.mTooltipPopup.show(this, paramInt1, paramInt2, paramBoolean, this.mTooltipInfo.mTooltipText);
    this.mAttachInfo.mTooltipHost = this;
    notifyViewAccessibilityStateChangedIfNeeded(0);
    return true;
  }
  
  void hideTooltip() {
    TooltipInfo tooltipInfo = this.mTooltipInfo;
    if (tooltipInfo == null)
      return; 
    removeCallbacks(tooltipInfo.mShowTooltipRunnable);
    if (this.mTooltipInfo.mTooltipPopup == null)
      return; 
    this.mTooltipInfo.mTooltipPopup.hide();
    this.mTooltipInfo.mTooltipPopup = null;
    this.mTooltipInfo.mTooltipFromLongClick = false;
    this.mTooltipInfo.clearAnchorPos();
    AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null)
      attachInfo.mTooltipHost = null; 
    notifyViewAccessibilityStateChangedIfNeeded(0);
  }
  
  private boolean showLongClickTooltip(int paramInt1, int paramInt2) {
    removeCallbacks(this.mTooltipInfo.mShowTooltipRunnable);
    removeCallbacks(this.mTooltipInfo.mHideTooltipRunnable);
    return showTooltip(paramInt1, paramInt2, true);
  }
  
  private boolean showHoverTooltip() {
    return showTooltip(this.mTooltipInfo.mAnchorX, this.mTooltipInfo.mAnchorY, false);
  }
  
  boolean dispatchTooltipHoverEvent(MotionEvent paramMotionEvent) {
    if (this.mTooltipInfo == null)
      return false; 
    int i = paramMotionEvent.getAction();
    if (i != 7) {
      if (i == 10) {
        this.mTooltipInfo.clearAnchorPos();
        if (!this.mTooltipInfo.mTooltipFromLongClick)
          hideTooltip(); 
      } 
    } else if ((this.mViewFlags & 0x40000000) == 1073741824) {
      if (!this.mTooltipInfo.mTooltipFromLongClick && this.mTooltipInfo.updateAnchorPos(paramMotionEvent)) {
        if (this.mTooltipInfo.mTooltipPopup == null) {
          removeCallbacks(this.mTooltipInfo.mShowTooltipRunnable);
          Runnable runnable = this.mTooltipInfo.mShowTooltipRunnable;
          long l = ViewConfiguration.getHoverTooltipShowTimeout();
          postDelayed(runnable, l);
        } 
        if ((getWindowSystemUiVisibility() & 0x1) == 1) {
          i = ViewConfiguration.getHoverTooltipHideShortTimeout();
        } else {
          i = ViewConfiguration.getHoverTooltipHideTimeout();
        } 
        removeCallbacks(this.mTooltipInfo.mHideTooltipRunnable);
        postDelayed(this.mTooltipInfo.mHideTooltipRunnable, i);
      } 
      return true;
    } 
    return false;
  }
  
  void handleTooltipKey(KeyEvent paramKeyEvent) {
    int i = paramKeyEvent.getAction();
    if (i != 0) {
      if (i == 1)
        handleTooltipUp(); 
    } else if (paramKeyEvent.getRepeatCount() == 0) {
      hideTooltip();
    } 
  }
  
  private void handleTooltipUp() {
    TooltipInfo tooltipInfo = this.mTooltipInfo;
    if (tooltipInfo == null || tooltipInfo.mTooltipPopup == null)
      return; 
    removeCallbacks(this.mTooltipInfo.mHideTooltipRunnable);
    Runnable runnable = this.mTooltipInfo.mHideTooltipRunnable;
    long l = ViewConfiguration.getLongPressTooltipHideTimeout();
    postDelayed(runnable, l);
  }
  
  private int getFocusableAttribute(TypedArray paramTypedArray) {
    TypedValue typedValue = new TypedValue();
    if (paramTypedArray.getValue(19, typedValue)) {
      if (typedValue.type == 18) {
        boolean bool;
        if (typedValue.data == 0) {
          bool = false;
        } else {
          bool = true;
        } 
        return bool;
      } 
      return typedValue.data;
    } 
    return 16;
  }
  
  public View getTooltipView() {
    TooltipInfo tooltipInfo = this.mTooltipInfo;
    if (tooltipInfo == null || tooltipInfo.mTooltipPopup == null)
      return null; 
    return this.mTooltipInfo.mTooltipPopup.getContentView();
  }
  
  private boolean shouldForceLayerHardware(CharSequence paramCharSequence) {
    for (String str : FORCE_HARDWARE_LIST) {
      if (str.equals(paramCharSequence))
        return true; 
    } 
    return false;
  }
  
  public void adjustLayerType(int paramInt1, int paramInt2) {
    if (this.mPendingLayerType == 2) {
      int i = this.mLayerType;
      if (i == 1) {
        if (paramInt1 * paramInt2 <= this.mForceHWSize) {
          setLayerType(2, this.mLayerPaint);
          Log.i("hw", "force hw draw true");
        } else {
          this.mPendingLayerType = 0;
        } 
      } else if (i == 2) {
        if (paramInt1 * paramInt2 > this.mForceHWSize) {
          setLayerType(1, this.mLayerPaint);
          Context context = this.mContext;
          if (context instanceof Activity)
            ((Activity)context).setBoostAnimation(true); 
          this.mPendingLayerType = 0;
        } 
      } 
    } 
  }
  
  private boolean shouldBoostAnimation() {
    Context context = this.mContext;
    if (context instanceof Activity)
      return ((Activity)context).shouldBoostAnimation(); 
    return false;
  }
  
  private void hypnusSetAction(int paramInt1, int paramInt2) {}
  
  public static boolean isDefaultFocusHighlightEnabled() {
    return sUseDefaultFocusHighlight;
  }
  
  View dispatchUnhandledKeyEvent(KeyEvent paramKeyEvent) {
    if (onUnhandledKeyEvent(paramKeyEvent))
      return this; 
    return null;
  }
  
  boolean onUnhandledKeyEvent(KeyEvent paramKeyEvent) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mUnhandledKeyListeners != null)
      for (int i = this.mListenerInfo.mUnhandledKeyListeners.size() - 1; i >= 0; i--) {
        if (((OnUnhandledKeyEventListener)this.mListenerInfo.mUnhandledKeyListeners.get(i)).onUnhandledKeyEvent(this, paramKeyEvent))
          return true; 
      }  
    return false;
  }
  
  boolean hasUnhandledKeyListener() {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && listenerInfo.mUnhandledKeyListeners != null) {
      listenerInfo = this.mListenerInfo;
      if (!listenerInfo.mUnhandledKeyListeners.isEmpty())
        return true; 
    } 
    return false;
  }
  
  public void addOnUnhandledKeyEventListener(OnUnhandledKeyEventListener paramOnUnhandledKeyEventListener) {
    ArrayList<OnUnhandledKeyEventListener> arrayList1 = (getListenerInfo()).mUnhandledKeyListeners;
    ArrayList<OnUnhandledKeyEventListener> arrayList2 = arrayList1;
    if (arrayList1 == null) {
      arrayList2 = new ArrayList();
      ListenerInfo.access$4602(getListenerInfo(), arrayList2);
    } 
    arrayList2.add(paramOnUnhandledKeyEventListener);
    if (arrayList2.size() == 1) {
      ViewParent viewParent = this.mParent;
      if (viewParent instanceof ViewGroup)
        ((ViewGroup)viewParent).incrementChildUnhandledKeyListeners(); 
    } 
  }
  
  public void removeOnUnhandledKeyEventListener(OnUnhandledKeyEventListener paramOnUnhandledKeyEventListener) {
    ListenerInfo listenerInfo = this.mListenerInfo;
    if (listenerInfo != null && 
      listenerInfo.mUnhandledKeyListeners != null) {
      listenerInfo = this.mListenerInfo;
      if (!listenerInfo.mUnhandledKeyListeners.isEmpty()) {
        this.mListenerInfo.mUnhandledKeyListeners.remove(paramOnUnhandledKeyEventListener);
        if (this.mListenerInfo.mUnhandledKeyListeners.isEmpty()) {
          ListenerInfo.access$4602(this.mListenerInfo, null);
          ViewParent viewParent = this.mParent;
          if (viewParent instanceof ViewGroup)
            ((ViewGroup)viewParent).decrementChildUnhandledKeyListeners(); 
        } 
      } 
    } 
  }
  
  protected void setValueScrollX(int paramInt) {
    this.mScrollX = paramInt;
  }
  
  protected void setValueScrollY(int paramInt) {
    this.mScrollY = paramInt;
  }
  
  protected RenderNode getRenderNode() {
    return this.mRenderNode;
  }
  
  public Bitmap getColorCustomDrawingCache(Rect paramRect, int paramInt) {
    return this.mViewHooks.getOplusCustomDrawingCache(paramRect, paramInt, this.mPrivateFlags);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AutofillFlags implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class AutofillImportance implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class AutofillType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ContentCaptureImportance implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class DrawingCacheQuality implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class FindViewFlags implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class FocusDirection implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class FocusRealDirection implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Focusable implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class FocusableMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class LayerType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class LayoutDir implements Annotation {}
  
  class OnApplyWindowInsetsListener {
    public abstract WindowInsets onApplyWindowInsets(View param1View, WindowInsets param1WindowInsets);
  }
  
  class OnAttachStateChangeListener {
    public abstract void onViewAttachedToWindow(View param1View);
    
    public abstract void onViewDetachedFromWindow(View param1View);
  }
  
  class OnCapturedPointerListener {
    public abstract boolean onCapturedPointer(View param1View, MotionEvent param1MotionEvent);
  }
  
  class OnClickListener {
    public abstract void onClick(View param1View);
  }
  
  class OnContextClickListener {
    public abstract boolean onContextClick(View param1View);
  }
  
  class OnCreateContextMenuListener {
    public abstract void onCreateContextMenu(ContextMenu param1ContextMenu, View param1View, ContextMenu.ContextMenuInfo param1ContextMenuInfo);
  }
  
  class OnDragListener {
    public abstract boolean onDrag(View param1View, DragEvent param1DragEvent);
  }
  
  class OnFocusChangeListener {
    public abstract void onFocusChange(View param1View, boolean param1Boolean);
  }
  
  class OnGenericMotionListener {
    public abstract boolean onGenericMotion(View param1View, MotionEvent param1MotionEvent);
  }
  
  class OnHoverListener {
    public abstract boolean onHover(View param1View, MotionEvent param1MotionEvent);
  }
  
  class OnKeyListener {
    public abstract boolean onKey(View param1View, int param1Int, KeyEvent param1KeyEvent);
  }
  
  class OnLayoutChangeListener {
    public abstract void onLayoutChange(View param1View, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, int param1Int8);
  }
  
  class OnLongClickListener {
    public abstract boolean onLongClick(View param1View);
  }
  
  class OnScrollChangeListener {
    public abstract void onScrollChange(View param1View, int param1Int1, int param1Int2, int param1Int3, int param1Int4);
  }
  
  @Deprecated
  class OnSystemUiVisibilityChangeListener {
    public abstract void onSystemUiVisibilityChange(int param1Int);
  }
  
  class OnTouchListener {
    public abstract boolean onTouch(View param1View, MotionEvent param1MotionEvent);
  }
  
  class OnUnhandledKeyEventListener {
    public abstract boolean onUnhandledKeyEvent(View param1View, KeyEvent param1KeyEvent);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ResolvedLayoutDir implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ScrollBarStyle implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ScrollCaptureHint implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ScrollIndicators implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TextAlignment implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ViewStructureType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Visibility implements Annotation {}
}
