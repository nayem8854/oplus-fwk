package android.view;

import android.content.res.CompatibilityInfo;
import android.os.Debug;
import android.os.SystemProperties;
import android.util.DisplayMetrics;
import android.util.Log;

public class OplusScreenCompatViewInjector {
  public static final boolean DEBUG;
  
  private static final String TAG = "OplusScreenCompatViewInjector";
  
  public static int sCompatDensity;
  
  public static boolean sIsDisplayCompatApp = false;
  
  static {
    DEBUG = SystemProperties.getBoolean("persist.sys.compat.debug", true);
    sCompatDensity = 420;
  }
  
  public static void overrideDisplayMetricsIfNeed(DisplayMetrics paramDisplayMetrics) {
    int i = paramDisplayMetrics.densityDpi, j = sCompatDensity;
    if (i != j) {
      float f = j * 0.00625F;
      paramDisplayMetrics.scaledDensity = f;
      paramDisplayMetrics.densityDpi = sCompatDensity;
      paramDisplayMetrics.xdpi = paramDisplayMetrics.noncompatXdpi * 0.75F;
      paramDisplayMetrics.ydpi = paramDisplayMetrics.noncompatYdpi * 0.75F;
      paramDisplayMetrics.widthPixels = (int)(paramDisplayMetrics.noncompatWidthPixels * 0.75F);
      paramDisplayMetrics.heightPixels = (int)(paramDisplayMetrics.noncompatHeightPixels * 0.75F);
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DisplayCompat: applyToDisplayMetrics0, inoutDm=");
        stringBuilder.append(paramDisplayMetrics);
        stringBuilder.append(" noncompatDensityDpi=");
        stringBuilder.append(paramDisplayMetrics.noncompatDensityDpi);
        stringBuilder.append(" caller:");
        stringBuilder.append(Debug.getCallers(10));
        String str = stringBuilder.toString();
        Log.d("OplusScreenCompatViewInjector", str);
      } 
    } 
  }
  
  public static void applyCompatInfo(CompatibilityInfo paramCompatibilityInfo, DisplayMetrics paramDisplayMetrics) {
    if (sIsDisplayCompatApp && paramDisplayMetrics.densityDpi != sCompatDensity) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DisplayCompat: applyCompatInfo, change out=");
        stringBuilder.append(paramDisplayMetrics);
        stringBuilder.append(" to ");
        stringBuilder.append(sCompatDensity);
        stringBuilder.append(" caller:");
        stringBuilder.append(Debug.getCallers(10));
        String str = stringBuilder.toString();
        Log.d("OplusScreenCompatViewInjector", str);
      } 
      paramCompatibilityInfo.applyToDisplayMetrics(paramDisplayMetrics);
    } 
  }
  
  public static void updateCompatDensityIfNeed(int paramInt) {
    if (!sIsDisplayCompatApp)
      return; 
    int i = (int)(paramInt * 0.75D);
    if (sCompatDensity != i) {
      sCompatDensity = i;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DisplayCompat: updateCompatDensityIfNeed from ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" to density=");
      stringBuilder.append(i);
      stringBuilder.append(" callers=");
      stringBuilder.append(Debug.getCallers(5));
      String str = stringBuilder.toString();
      Log.i("OplusScreenCompatViewInjector", str);
    } 
  }
}
