package android.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.os.RemoteException;
import android.util.CloseGuard;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class ScrollCaptureClient extends IScrollCaptureClient.Stub {
  private static final int DEFAULT_TIMEOUT = 1000;
  
  private static final String TAG = "ScrollCaptureClient";
  
  private final CloseGuard mCloseGuard;
  
  private IScrollCaptureController mController;
  
  private final Handler mHandler;
  
  private final Point mPositionInWindow;
  
  private final Rect mScrollBounds;
  
  private ScrollCaptureTarget mSelectedTarget;
  
  private ScrollCaptureSession mSession;
  
  protected Surface mSurface;
  
  private DelayedAction mTimeoutAction;
  
  private int mTimeoutMillis = 1000;
  
  public ScrollCaptureClient(ScrollCaptureTarget paramScrollCaptureTarget, IScrollCaptureController paramIScrollCaptureController) {
    Objects.requireNonNull(paramScrollCaptureTarget, "<selectedTarget> must non-null");
    Objects.requireNonNull(paramIScrollCaptureController, "<controller> must non-null");
    Rect rect = paramScrollCaptureTarget.getScrollBounds();
    Objects.requireNonNull(rect, "target.getScrollBounds() must be non-null to construct a client");
    rect = rect;
    this.mSelectedTarget = paramScrollCaptureTarget;
    this.mHandler = paramScrollCaptureTarget.getContainingView().getHandler();
    this.mScrollBounds = new Rect(rect);
    this.mPositionInWindow = new Point(paramScrollCaptureTarget.getPositionInWindow());
    this.mController = paramIScrollCaptureController;
    CloseGuard closeGuard = new CloseGuard();
    closeGuard.open("close");
    paramScrollCaptureTarget.getContainingView().addOnAttachStateChangeListener((View.OnAttachStateChangeListener)new Object(this, paramScrollCaptureTarget));
  }
  
  public void setTimeoutMillis(int paramInt) {
    this.mTimeoutMillis = paramInt;
  }
  
  public DelayedAction getTimeoutAction() {
    return this.mTimeoutAction;
  }
  
  private void checkConnected() {
    if (this.mSelectedTarget != null && this.mController != null)
      return; 
    throw new IllegalStateException("This client has been disconnected.");
  }
  
  private void checkStarted() {
    if (this.mSession != null)
      return; 
    throw new IllegalStateException("Capture session has not been started!");
  }
  
  public void startCapture(Surface paramSurface) throws RemoteException {
    checkConnected();
    this.mSurface = paramSurface;
    scheduleTimeout(this.mTimeoutMillis, new _$$Lambda$ScrollCaptureClient$Epfw54HmxmekAnuTbczAITVw7Dg(this));
    this.mSession = new ScrollCaptureSession(this.mSurface, this.mScrollBounds, this.mPositionInWindow, this);
    this.mHandler.post(new _$$Lambda$ScrollCaptureClient$Wx8q2o_h2xkFAIBvHcSi3uj_Sm0(this));
  }
  
  private void onStartCaptureCompleted() {
    if (cancelTimeout())
      this.mHandler.post(new _$$Lambda$ScrollCaptureClient$9ZUY_FVuYBZWGLMdk_bSi5wu7zg(this)); 
  }
  
  private void onStartCaptureTimeout() {
    endCapture();
  }
  
  public void requestImage(Rect paramRect) {
    checkConnected();
    checkStarted();
    scheduleTimeout(this.mTimeoutMillis, new _$$Lambda$ScrollCaptureClient$cg6Nc5a_WzAiBUwgRjGwyrOr1MQ(this));
    this.mHandler.post(new _$$Lambda$ScrollCaptureClient$rf3BkY5f_J7V42O_dMJLA1rnVkM(this, paramRect));
  }
  
  void onRequestImageCompleted(long paramLong, Rect paramRect) {
    paramRect = new Rect(paramRect);
    if (cancelTimeout())
      this.mHandler.post(new _$$Lambda$ScrollCaptureClient$jB_gZzHC6CVXS20ua5gOXB2VBpE(this, paramLong, paramRect)); 
  }
  
  private void onRequestImageTimeout() {
    endCapture();
  }
  
  public void endCapture() {
    if (isStarted()) {
      scheduleTimeout(this.mTimeoutMillis, new _$$Lambda$ScrollCaptureClient$ViSKCOfqcLht_jEVL0NThLc_K9A(this));
      this.mHandler.post(new _$$Lambda$ScrollCaptureClient$OoPpYradNVuARe3t3TP2xK3X9iI(this));
    } else {
      disconnect();
    } 
  }
  
  private boolean isStarted() {
    boolean bool;
    if (this.mController != null && this.mSelectedTarget != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void onEndCaptureCompleted() {
    if (cancelTimeout())
      doShutdown(); 
  }
  
  private void onEndCaptureTimeout() {
    doShutdown();
  }
  
  private void doShutdown() {
    try {
      if (this.mController != null)
        this.mController.onConnectionClosed(); 
    } catch (RemoteException remoteException) {
    
    } finally {
      disconnect();
    } 
  }
  
  public void disconnect() {
    ScrollCaptureSession scrollCaptureSession = this.mSession;
    if (scrollCaptureSession != null) {
      scrollCaptureSession.disconnect();
      this.mSession = null;
    } 
    this.mSelectedTarget = null;
    this.mController = null;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ScrollCaptureClient{, session=");
    stringBuilder.append(this.mSession);
    stringBuilder.append(", selectedTarget=");
    stringBuilder.append(this.mSelectedTarget);
    stringBuilder.append(", clientCallbacks=");
    stringBuilder.append(this.mController);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private boolean cancelTimeout() {
    DelayedAction delayedAction = this.mTimeoutAction;
    if (delayedAction != null)
      return delayedAction.cancel(); 
    return false;
  }
  
  private void scheduleTimeout(long paramLong, Runnable paramRunnable) {
    DelayedAction delayedAction = this.mTimeoutAction;
    if (delayedAction != null)
      delayedAction.cancel(); 
    this.mTimeoutAction = new DelayedAction(this.mHandler, paramLong, paramRunnable);
  }
  
  class DelayedAction {
    private final Runnable mAction;
    
    private final AtomicBoolean mCompleted = new AtomicBoolean();
    
    private final Handler mHandler;
    
    private final Object mToken = new Object();
    
    public DelayedAction(ScrollCaptureClient this$0, long param1Long, Runnable param1Runnable) {
      this.mHandler = (Handler)this$0;
      this.mAction = param1Runnable;
      this$0.postDelayed(new _$$Lambda$ScrollCaptureClient$DelayedAction$dG_6ZyjvsGGEg_j2UjRHC5kFNv8(this), this.mToken, param1Long);
    }
    
    private boolean onTimeout() {
      if (this.mCompleted.compareAndSet(false, true)) {
        this.mAction.run();
        return true;
      } 
      return false;
    }
    
    public boolean timeoutNow() {
      return onTimeout();
    }
    
    public boolean cancel() {
      if (!this.mCompleted.compareAndSet(false, true))
        return false; 
      this.mHandler.removeCallbacksAndMessages(this.mToken);
      return true;
    }
  }
}
