package android.view;

import android.animation.AnimationHandler;
import android.animation.Animator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Insets;
import android.graphics.Rect;
import android.os.CancellationSignal;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Trace;
import android.util.ArraySet;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.PathInterpolator;
import android.view.inputmethod.InputMethodManager;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class InsetsController implements WindowInsetsController, InsetsAnimationControlCallbacks {
  public static final Interpolator SYSTEM_BARS_INTERPOLATOR = new PathInterpolator(0.4F, 0.0F, 0.2F, 1.0F);
  
  static {
    SYNC_IME_INTERPOLATOR = new PathInterpolator(0.2F, 0.0F, 0.0F, 1.0F);
    LINEAR_OUT_SLOW_IN_INTERPOLATOR = new PathInterpolator(0.0F, 0.0F, 0.2F, 1.0F);
    FAST_OUT_LINEAR_IN_INTERPOLATOR = new PathInterpolator(0.4F, 0.0F, 1.0F, 1.0F);
    DEBUG = false;
    sEvaluator = (TypeEvaluator<Insets>)_$$Lambda$InsetsController$Cj7UJrCkdHvJAZ_cYKrXuTMsjz8.INSTANCE;
  }
  
  public static class InternalAnimationControlListener implements WindowInsetsAnimationControlListener {
    protected static final int FLOATING_IME_BOTTOM_INSET = -80;
    
    private ValueAnimator mAnimator;
    
    private WindowInsetsAnimationController mController;
    
    private final boolean mDisable;
    
    private final long mDurationMs;
    
    private final int mFloatingImeBottomInset;
    
    private final boolean mHasAnimationCallbacks;
    
    private final int mRequestedTypes;
    
    private ThreadLocal<AnimationHandler> mSfAnimationHandlerThreadLocal = (ThreadLocal<AnimationHandler>)new Object(this);
    
    private final boolean mShow;
    
    public InternalAnimationControlListener(boolean param1Boolean1, boolean param1Boolean2, int param1Int1, boolean param1Boolean3, int param1Int2) {
      this.mShow = param1Boolean1;
      this.mHasAnimationCallbacks = param1Boolean2;
      this.mRequestedTypes = param1Int1;
      this.mDurationMs = calculateDurationMs();
      this.mDisable = param1Boolean3;
      this.mFloatingImeBottomInset = param1Int2;
    }
    
    public void onReady(WindowInsetsAnimationController param1WindowInsetsAnimationController, int param1Int) {
      Insets insets2;
      this.mController = param1WindowInsetsAnimationController;
      if (InsetsController.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("default animation onReady types: ");
        stringBuilder.append(param1Int);
        Log.d("InsetsController", stringBuilder.toString());
      } 
      if (this.mDisable) {
        onAnimationFinish();
        return;
      } 
      ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
      valueAnimator.setDuration(this.mDurationMs);
      this.mAnimator.setInterpolator(new LinearInterpolator());
      Insets insets1 = param1WindowInsetsAnimationController.getHiddenStateInsets();
      if (param1WindowInsetsAnimationController.hasZeroInsetsIme())
        insets1 = Insets.of(insets1.left, insets1.top, insets1.right, this.mFloatingImeBottomInset); 
      if (this.mShow) {
        insets2 = insets1;
      } else {
        insets2 = param1WindowInsetsAnimationController.getShownStateInsets();
      } 
      if (this.mShow)
        insets1 = param1WindowInsetsAnimationController.getShownStateInsets(); 
      Interpolator interpolator1 = getInterpolator();
      Interpolator interpolator2 = getAlphaInterpolator();
      this.mAnimator.addUpdateListener(new _$$Lambda$InsetsController$InternalAnimationControlListener$SInf91MjJKDQFXwrp7C_HBi0xaQ(this, interpolator1, param1WindowInsetsAnimationController, insets2, insets1, interpolator2));
      this.mAnimator.addListener((Animator.AnimatorListener)new Object(this));
      if (!this.mHasAnimationCallbacks)
        this.mAnimator.setAnimationHandler(this.mSfAnimationHandlerThreadLocal.get()); 
      this.mAnimator.start();
    }
    
    public void onFinished(WindowInsetsAnimationController param1WindowInsetsAnimationController) {
      if (InsetsController.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("InternalAnimationControlListener onFinished types:");
        int i = this.mRequestedTypes;
        stringBuilder.append(WindowInsets.Type.toString(i));
        String str = stringBuilder.toString();
        Log.d("InsetsController", str);
      } 
    }
    
    public void onCancelled(WindowInsetsAnimationController param1WindowInsetsAnimationController) {
      ValueAnimator valueAnimator = this.mAnimator;
      if (valueAnimator != null)
        valueAnimator.cancel(); 
      try {
        if (param1WindowInsetsAnimationController instanceof InsetsAnimationControlImpl) {
          InsetsAnimationControlImpl insetsAnimationControlImpl = (InsetsAnimationControlImpl)param1WindowInsetsAnimationController;
          boolean bool = this.mShow;
          InsetsState insetsState = new InsetsState();
          this();
          insetsAnimationControlImpl.applyInsetsAndAlphaNoCheck(bool, 1.0F, insetsState);
        } 
      } catch (Exception exception) {}
      if (InsetsController.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("InternalAnimationControlListener onCancelled types:");
        stringBuilder.append(this.mRequestedTypes);
        Log.d("InsetsController", stringBuilder.toString());
      } 
    }
    
    Interpolator getInterpolator() {
      if ((this.mRequestedTypes & WindowInsets.Type.ime()) != 0) {
        if (this.mHasAnimationCallbacks)
          return InsetsController.SYNC_IME_INTERPOLATOR; 
        if (this.mShow)
          return InsetsController.LINEAR_OUT_SLOW_IN_INTERPOLATOR; 
        return InsetsController.FAST_OUT_LINEAR_IN_INTERPOLATOR;
      } 
      return InsetsController.SYSTEM_BARS_INTERPOLATOR;
    }
    
    Interpolator getAlphaInterpolator() {
      if ((this.mRequestedTypes & WindowInsets.Type.ime()) != 0) {
        if (this.mHasAnimationCallbacks)
          return (Interpolator)_$$Lambda$InsetsController$InternalAnimationControlListener$JCR0O3j9NxyNcNRXO181IWLmsto.INSTANCE; 
        if (this.mShow)
          return (Interpolator)_$$Lambda$InsetsController$InternalAnimationControlListener$0SeZK03YbYwxm_nBE39jPZ1sdMM.INSTANCE; 
        return InsetsController.FAST_OUT_LINEAR_IN_INTERPOLATOR;
      } 
      return (Interpolator)_$$Lambda$InsetsController$InternalAnimationControlListener$hxk87AxkClLRhRgGak0NUvJOACM.INSTANCE;
    }
    
    protected void onAnimationFinish() {
      this.mController.finish(this.mShow);
      if (InsetsController.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onAnimationFinish showOnFinish: ");
        stringBuilder.append(this.mShow);
        Log.d("InsetsController", stringBuilder.toString());
      } 
    }
    
    public long getDurationMs() {
      return this.mDurationMs;
    }
    
    private long calculateDurationMs() {
      long l;
      if ((this.mRequestedTypes & WindowInsets.Type.ime()) != 0) {
        if (this.mHasAnimationCallbacks)
          return 285L; 
        return 200L;
      } 
      if (this.mShow) {
        l = 275L;
      } else {
        l = 340L;
      } 
      return l;
    }
  }
  
  class RunningAnimation {
    final InsetsAnimationControlRunner runner;
    
    boolean startDispatched;
    
    final int type;
    
    RunningAnimation(InsetsController this$0, int param1Int) {
      this.runner = (InsetsAnimationControlRunner)this$0;
      this.type = param1Int;
    }
  }
  
  class PendingControlRequest {
    final int animationType;
    
    final CancellationSignal cancellationSignal;
    
    final long durationMs;
    
    final Interpolator interpolator;
    
    final int layoutInsetsDuringAnimation;
    
    final WindowInsetsAnimationControlListener listener;
    
    final int types;
    
    final boolean useInsetsAnimationThread;
    
    PendingControlRequest(InsetsController this$0, WindowInsetsAnimationControlListener param1WindowInsetsAnimationControlListener, long param1Long, Interpolator param1Interpolator, int param1Int1, int param1Int2, CancellationSignal param1CancellationSignal, boolean param1Boolean) {
      this.types = this$0;
      this.listener = param1WindowInsetsAnimationControlListener;
      this.durationMs = param1Long;
      this.interpolator = param1Interpolator;
      this.animationType = param1Int1;
      this.layoutInsetsDuringAnimation = param1Int2;
      this.cancellationSignal = param1CancellationSignal;
      this.useInsetsAnimationThread = param1Boolean;
    }
  }
  
  private final InsetsState mState = new InsetsState();
  
  private final InsetsState mLastDispatchedState = new InsetsState();
  
  private final InsetsState mRequestedState = new InsetsState();
  
  private final Rect mFrame = new Rect();
  
  private final SparseArray<InsetsSourceConsumer> mSourceConsumers = new SparseArray<>();
  
  private final SparseArray<InsetsSourceControl> mTmpControlArray = new SparseArray<>();
  
  private final ArrayList<RunningAnimation> mRunningAnimations = new ArrayList<>();
  
  private static final int ANIMATION_DURATION_HIDE_MS = 340;
  
  private static final int ANIMATION_DURATION_SHOW_MS = 275;
  
  private static final int ANIMATION_DURATION_SYNC_IME_MS = 285;
  
  private static final int ANIMATION_DURATION_UNSYNC_IME_MS = 200;
  
  public static final int ANIMATION_TYPE_HIDE = 1;
  
  public static final int ANIMATION_TYPE_NONE = -1;
  
  public static final int ANIMATION_TYPE_SHOW = 0;
  
  public static final int ANIMATION_TYPE_USER = 2;
  
  public static boolean DEBUG = false;
  
  private static final Interpolator FAST_OUT_LINEAR_IN_INTERPOLATOR;
  
  public static final int LAYOUT_INSETS_DURING_ANIMATION_HIDDEN = 1;
  
  public static final int LAYOUT_INSETS_DURING_ANIMATION_SHOWN = 0;
  
  private static final Interpolator LINEAR_OUT_SLOW_IN_INTERPOLATOR;
  
  private static final int PENDING_CONTROL_TIMEOUT_MS = 2000;
  
  private static final Interpolator SYNC_IME_INTERPOLATOR;
  
  private static final String TAG = "InsetsController";
  
  static final boolean WARN = false;
  
  private static TypeEvaluator<Insets> sEvaluator;
  
  private final Runnable mAnimCallback;
  
  private boolean mAnimCallbackScheduled;
  
  private boolean mAnimationsDisabled;
  
  private int mCaptionInsetsHeight;
  
  private final BiFunction<InsetsController, Integer, InsetsSourceConsumer> mConsumerCreator;
  
  private final ArrayList<WindowInsetsController.OnControllableInsetsChangedListener> mControllableInsetsChangedListeners;
  
  private int mDisabledUserAnimationInsetsTypes;
  
  private final Handler mHandler;
  
  private final Host mHost;
  
  private Runnable mInvokeControllableInsetsChangedListeners;
  
  private DisplayCutout mLastDisplayCutout;
  
  private WindowInsets mLastInsets;
  
  private int mLastLegacySoftInputMode;
  
  private int mLastLegacySystemUiFlags;
  
  private int mLastLegacyWindowFlags;
  
  private int mLastStartedAnimTypes;
  
  private Runnable mPendingControlTimeout;
  
  private PendingControlRequest mPendingImeControlRequest;
  
  private boolean mStartingAnimation;
  
  private final ArrayList<InsetsAnimationControlImpl> mTmpFinishedControls;
  
  private final ArrayList<WindowInsetsAnimation> mTmpRunningAnims;
  
  private int mTypesBeingCancelled;
  
  private final List<WindowInsetsAnimation> mUnmodifiableTmpRunningAnims;
  
  public InsetsController(Host paramHost, BiFunction<InsetsController, Integer, InsetsSourceConsumer> paramBiFunction, Handler paramHandler) {
    ArrayList<WindowInsetsAnimation> arrayList = new ArrayList();
    this.mUnmodifiableTmpRunningAnims = Collections.unmodifiableList(arrayList);
    this.mTmpFinishedControls = new ArrayList<>();
    this.mCaptionInsetsHeight = 0;
    this.mPendingControlTimeout = new _$$Lambda$InsetsController$6uoSHBPvxV1C0JOZKhH1AyuNXmo(this);
    this.mControllableInsetsChangedListeners = new ArrayList<>();
    this.mInvokeControllableInsetsChangedListeners = new _$$Lambda$InsetsController$QTmHZEUVF9HpffXawWFTRAOvEno(this);
    this.mHost = paramHost;
    this.mConsumerCreator = paramBiFunction;
    this.mHandler = paramHandler;
    this.mAnimCallback = new _$$Lambda$InsetsController$zpmOxHfTFV_3me2u3C8YaXSUauQ(this);
  }
  
  public InsetsController(Host paramHost) {
    this(paramHost, (BiFunction<InsetsController, Integer, InsetsSourceConsumer>)rZT3QkL9zMFTeHtZbfcaHIzvlsc, handler);
  }
  
  public void onFrameChanged(Rect paramRect) {
    if (this.mFrame.equals(paramRect))
      return; 
    this.mHost.notifyInsetsChanged();
    this.mFrame.set(paramRect);
  }
  
  public InsetsState getState() {
    return this.mState;
  }
  
  public boolean isRequestedVisible(int paramInt) {
    return getSourceConsumer(paramInt).isRequestedVisible();
  }
  
  public InsetsState getLastDispatchedState() {
    return this.mLastDispatchedState;
  }
  
  public boolean onStateChanged(InsetsState paramInsetsState) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mState : Landroid/view/InsetsState;
    //   4: aload_1
    //   5: iconst_1
    //   6: iconst_0
    //   7: invokevirtual equals : (Ljava/lang/Object;ZZ)Z
    //   10: ifeq -> 28
    //   13: aload_0
    //   14: invokespecial captionInsetsUnchanged : ()Z
    //   17: ifne -> 23
    //   20: goto -> 28
    //   23: iconst_0
    //   24: istore_2
    //   25: goto -> 30
    //   28: iconst_1
    //   29: istore_2
    //   30: iload_2
    //   31: ifne -> 47
    //   34: aload_0
    //   35: getfield mLastDispatchedState : Landroid/view/InsetsState;
    //   38: aload_1
    //   39: invokevirtual equals : (Ljava/lang/Object;)Z
    //   42: ifeq -> 47
    //   45: iconst_0
    //   46: ireturn
    //   47: getstatic android/view/InsetsController.DEBUG : Z
    //   50: ifeq -> 85
    //   53: new java/lang/StringBuilder
    //   56: dup
    //   57: invokespecial <init> : ()V
    //   60: astore_3
    //   61: aload_3
    //   62: ldc_w 'onStateChanged: '
    //   65: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload_3
    //   70: aload_1
    //   71: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   74: pop
    //   75: ldc 'InsetsController'
    //   77: aload_3
    //   78: invokevirtual toString : ()Ljava/lang/String;
    //   81: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   84: pop
    //   85: aload_0
    //   86: getfield mLastDispatchedState : Landroid/view/InsetsState;
    //   89: aload_1
    //   90: iconst_1
    //   91: invokevirtual set : (Landroid/view/InsetsState;Z)V
    //   94: new android/view/InsetsState
    //   97: dup
    //   98: aload_0
    //   99: getfield mState : Landroid/view/InsetsState;
    //   102: iconst_1
    //   103: invokespecial <init> : (Landroid/view/InsetsState;Z)V
    //   106: astore_3
    //   107: aload_0
    //   108: aload_1
    //   109: invokespecial updateState : (Landroid/view/InsetsState;)V
    //   112: aload_0
    //   113: invokespecial applyLocalVisibilityOverride : ()V
    //   116: aload_0
    //   117: getfield mState : Landroid/view/InsetsState;
    //   120: aload_3
    //   121: iconst_1
    //   122: iconst_1
    //   123: invokevirtual equals : (Ljava/lang/Object;ZZ)Z
    //   126: ifne -> 153
    //   129: getstatic android/view/InsetsController.DEBUG : Z
    //   132: ifeq -> 144
    //   135: ldc 'InsetsController'
    //   137: ldc_w 'onStateChanged, notifyInsetsChanged'
    //   140: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   143: pop
    //   144: aload_0
    //   145: getfield mHost : Landroid/view/InsetsController$Host;
    //   148: invokeinterface notifyInsetsChanged : ()V
    //   153: aload_0
    //   154: getfield mState : Landroid/view/InsetsState;
    //   157: aload_0
    //   158: getfield mLastDispatchedState : Landroid/view/InsetsState;
    //   161: iconst_1
    //   162: iconst_1
    //   163: invokevirtual equals : (Ljava/lang/Object;ZZ)Z
    //   166: ifne -> 214
    //   169: getstatic android/view/InsetsController.DEBUG : Z
    //   172: ifeq -> 210
    //   175: new java/lang/StringBuilder
    //   178: dup
    //   179: invokespecial <init> : ()V
    //   182: astore_1
    //   183: aload_1
    //   184: ldc_w 'onStateChanged, send state to WM: '
    //   187: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   190: pop
    //   191: aload_1
    //   192: aload_0
    //   193: getfield mState : Landroid/view/InsetsState;
    //   196: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   199: pop
    //   200: ldc 'InsetsController'
    //   202: aload_1
    //   203: invokevirtual toString : ()Ljava/lang/String;
    //   206: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   209: pop
    //   210: aload_0
    //   211: invokespecial updateRequestedState : ()V
    //   214: iconst_1
    //   215: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #632	-> 0
    //   #634	-> 13
    //   #635	-> 30
    //   #636	-> 45
    //   #638	-> 47
    //   #639	-> 85
    //   #640	-> 94
    //   #641	-> 107
    //   #642	-> 112
    //   #644	-> 116
    //   #646	-> 129
    //   #647	-> 144
    //   #649	-> 153
    //   #651	-> 169
    //   #652	-> 210
    //   #654	-> 214
  }
  
  private void updateState(InsetsState paramInsetsState) {
    this.mState.setDisplayFrame(paramInsetsState.getDisplayFrame());
    int i = 0;
    int[] arrayOfInt = new int[1];
    arrayOfInt[0] = 0;
    for (byte b1 = 0; b1 < 16; b1++) {
      InsetsSource insetsSource = paramInsetsState.peekSource(b1);
      if (insetsSource != null) {
        int j = getAnimationType(b1);
        int k = i, m = j;
        if (!insetsSource.isUserControllable()) {
          int n = InsetsState.toPublicType(b1);
          i |= n;
          k = i;
          m = j;
          if (j == 2) {
            m = -1;
            arrayOfInt[0] = arrayOfInt[0] | n;
            k = i;
          } 
        } 
        getSourceConsumer(b1).updateSource(insetsSource, m);
        i = k;
      } 
    } 
    for (byte b2 = 0; b2 < 16; b2++) {
      InsetsSource insetsSource = this.mState.peekSource(b2);
      if (insetsSource != null && 
        paramInsetsState.peekSource(b2) == null)
        this.mState.removeSource(b2); 
    } 
    if (this.mCaptionInsetsHeight != 0)
      this.mState.getSource(2).setFrame(new Rect(this.mFrame.left, this.mFrame.top, this.mFrame.right, this.mFrame.top + this.mCaptionInsetsHeight)); 
    updateDisabledUserAnimationTypes(i);
    if (arrayOfInt[0] != 0)
      this.mHandler.post(new _$$Lambda$InsetsController$Q7zlA88P3JZ91fuDPXoGY_WBbiw(this, arrayOfInt)); 
  }
  
  private void updateDisabledUserAnimationTypes(int paramInt) {
    int i = this.mDisabledUserAnimationInsetsTypes ^ paramInt;
    if (i != 0) {
      for (int j = this.mSourceConsumers.size() - 1; j >= 0; j--) {
        InsetsSourceConsumer insetsSourceConsumer = this.mSourceConsumers.valueAt(j);
        if (insetsSourceConsumer.getControl() != null && (
          InsetsState.toPublicType(insetsSourceConsumer.getType()) & i) != 0) {
          this.mHandler.removeCallbacks(this.mInvokeControllableInsetsChangedListeners);
          this.mHandler.post(this.mInvokeControllableInsetsChangedListeners);
          break;
        } 
      } 
      this.mDisabledUserAnimationInsetsTypes = paramInt;
    } 
  }
  
  private boolean captionInsetsUnchanged() {
    if (this.mState.peekSource(2) == null && this.mCaptionInsetsHeight == 0)
      return true; 
    if (this.mState.peekSource(2) != null) {
      int i = this.mCaptionInsetsHeight;
      InsetsState insetsState = this.mState;
      if (i == insetsState.peekSource(2).getFrame().height())
        return true; 
    } 
    return false;
  }
  
  public WindowInsets calculateInsets(boolean paramBoolean1, boolean paramBoolean2, DisplayCutout paramDisplayCutout, int paramInt1, int paramInt2, int paramInt3) {
    this.mLastLegacySoftInputMode = paramInt1;
    this.mLastLegacyWindowFlags = paramInt2;
    this.mLastLegacySystemUiFlags = paramInt3;
    this.mLastDisplayCutout = paramDisplayCutout;
    WindowInsets windowInsets = this.mState.calculateInsets(this.mFrame, null, paramBoolean1, paramBoolean2, paramDisplayCutout, paramInt1, paramInt2, paramInt3, null);
    return windowInsets;
  }
  
  public Rect calculateVisibleInsets(int paramInt) {
    return this.mState.calculateVisibleInsets(this.mFrame, paramInt);
  }
  
  public void onControlsChanged(InsetsSourceControl[] paramArrayOfInsetsSourceControl) {
    if (paramArrayOfInsetsSourceControl != null) {
      int m;
      byte b;
      for (m = paramArrayOfInsetsSourceControl.length, b = 0; b < m; ) {
        InsetsSourceControl insetsSourceControl = paramArrayOfInsetsSourceControl[b];
        if (insetsSourceControl != null)
          this.mTmpControlArray.put(insetsSourceControl.getType(), insetsSourceControl); 
        b++;
      } 
    } 
    int k = 0;
    int[] arrayOfInt2 = new int[1];
    int[] arrayOfInt1 = new int[1];
    int j;
    for (j = this.mSourceConsumers.size() - 1; j >= 0; j--) {
      InsetsSourceConsumer insetsSourceConsumer = this.mSourceConsumers.valueAt(j);
      InsetsSourceControl insetsSourceControl = this.mTmpControlArray.get(insetsSourceConsumer.getType());
      insetsSourceConsumer.setControl(insetsSourceControl, arrayOfInt2, arrayOfInt1);
    } 
    int i;
    for (i = this.mTmpControlArray.size() - 1, j = k; i >= 0; i--, j = k) {
      InsetsSourceControl insetsSourceControl = this.mTmpControlArray.valueAt(i);
      int m = insetsSourceControl.getType();
      InsetsSourceConsumer insetsSourceConsumer = getSourceConsumer(m);
      insetsSourceConsumer.setControl(insetsSourceControl, arrayOfInt2, arrayOfInt1);
      k = j;
      if (j == 0) {
        boolean bool = insetsSourceConsumer.isRequestedVisible();
        InsetsState insetsState = this.mRequestedState;
        if (bool != insetsState.getSourceOrDefaultVisibility(m)) {
          j = 1;
        } else {
          j = 0;
        } 
        if (m == 13 && bool) {
          k = 1;
        } else {
          k = 0;
        } 
        if (j != 0 || k != 0) {
          j = 1;
        } else {
          j = 0;
        } 
        k = j;
      } 
    } 
    this.mTmpControlArray.clear();
    i = invokeControllableInsetsChangedListeners();
    arrayOfInt2[0] = arrayOfInt2[0] & (i ^ 0xFFFFFFFF);
    arrayOfInt1[0] = arrayOfInt1[0] & (i ^ 0xFFFFFFFF);
    if (arrayOfInt2[0] != 0)
      applyAnimation(arrayOfInt2[0], true, false); 
    if (arrayOfInt1[0] != 0)
      applyAnimation(arrayOfInt1[0], false, false); 
    if (j != 0)
      updateRequestedState(); 
  }
  
  public void show(int paramInt) {
    show(paramInt, false);
  }
  
  public void show(int paramInt, boolean paramBoolean) {
    if (paramBoolean && this.mPendingImeControlRequest != null) {
      PendingControlRequest pendingControlRequest = this.mPendingImeControlRequest;
      this.mPendingImeControlRequest = null;
      this.mHandler.removeCallbacks(this.mPendingControlTimeout);
      controlAnimationUnchecked(pendingControlRequest.types, pendingControlRequest.cancellationSignal, pendingControlRequest.listener, this.mFrame, true, pendingControlRequest.durationMs, pendingControlRequest.interpolator, pendingControlRequest.animationType, pendingControlRequest.layoutInsetsDuringAnimation, pendingControlRequest.useInsetsAnimationThread);
      return;
    } 
    int i = 0;
    ArraySet<Integer> arraySet = InsetsState.toInternalType(paramInt);
    for (paramInt = arraySet.size() - 1; paramInt >= 0; paramInt--, i = j) {
      int j = ((Integer)arraySet.valueAt(paramInt)).intValue();
      int k = getAnimationType(j);
      InsetsSourceConsumer insetsSourceConsumer = getSourceConsumer(j);
      if ((insetsSourceConsumer.isRequestedVisible() && k == -1) || k == 0) {
        j = i;
        if (DEBUG) {
          j = insetsSourceConsumer.getType();
          boolean bool = insetsSourceConsumer.isRequestedVisible();
          Log.d("InsetsController", String.format("show ignored for type: %d animType: %d requestedVisible: %s", new Object[] { Integer.valueOf(j), Integer.valueOf(k), Boolean.valueOf(bool) }));
          j = i;
        } 
      } else if (paramBoolean && k == 2) {
        j = i;
      } else {
        j = i | InsetsState.toPublicType(insetsSourceConsumer.getType());
      } 
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("show typesReady: ");
      stringBuilder.append(i);
      Log.d("InsetsController", stringBuilder.toString());
    } 
    applyAnimation(i, true, paramBoolean);
  }
  
  public void hide(int paramInt) {
    hide(paramInt, false);
  }
  
  void hide(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: iload_1
    //   3: invokestatic toInternalType : (I)Landroid/util/ArraySet;
    //   6: astore #4
    //   8: aload #4
    //   10: invokevirtual size : ()I
    //   13: iconst_1
    //   14: isub
    //   15: istore_1
    //   16: iload_1
    //   17: iflt -> 100
    //   20: aload #4
    //   22: iload_1
    //   23: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   26: checkcast java/lang/Integer
    //   29: invokevirtual intValue : ()I
    //   32: istore #5
    //   34: aload_0
    //   35: iload #5
    //   37: invokevirtual getAnimationType : (I)I
    //   40: istore #6
    //   42: aload_0
    //   43: iload #5
    //   45: invokevirtual getSourceConsumer : (I)Landroid/view/InsetsSourceConsumer;
    //   48: astore #7
    //   50: aload #7
    //   52: invokevirtual isRequestedVisible : ()Z
    //   55: ifne -> 67
    //   58: iload_3
    //   59: istore #5
    //   61: iload #6
    //   63: iconst_m1
    //   64: if_icmpeq -> 91
    //   67: iload #6
    //   69: iconst_1
    //   70: if_icmpne -> 79
    //   73: iload_3
    //   74: istore #5
    //   76: goto -> 91
    //   79: iload_3
    //   80: aload #7
    //   82: invokevirtual getType : ()I
    //   85: invokestatic toPublicType : (I)I
    //   88: ior
    //   89: istore #5
    //   91: iinc #1, -1
    //   94: iload #5
    //   96: istore_3
    //   97: goto -> 16
    //   100: aload_0
    //   101: iload_3
    //   102: iconst_0
    //   103: iload_2
    //   104: invokevirtual applyAnimation : (IZZ)V
    //   107: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #875	-> 0
    //   #876	-> 2
    //   #877	-> 8
    //   #878	-> 20
    //   #879	-> 34
    //   #880	-> 42
    //   #881	-> 50
    //   #884	-> 73
    //   #886	-> 79
    //   #877	-> 91
    //   #888	-> 100
    //   #889	-> 107
  }
  
  public void controlWindowInsetsAnimation(int paramInt, long paramLong, Interpolator paramInterpolator, CancellationSignal paramCancellationSignal, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener) {
    controlWindowInsetsAnimation(paramInt, paramCancellationSignal, paramWindowInsetsAnimationControlListener, false, paramLong, paramInterpolator, 2);
  }
  
  private void controlWindowInsetsAnimation(int paramInt1, CancellationSignal paramCancellationSignal, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener, boolean paramBoolean, long paramLong, Interpolator paramInterpolator, int paramInt2) {
    if ((this.mState.calculateUncontrollableInsetsFromFrame(this.mFrame) & paramInt1) != 0) {
      paramWindowInsetsAnimationControlListener.onCancelled(null);
      return;
    } 
    Rect rect = this.mFrame;
    int i = getLayoutInsetsDuringAnimationMode(paramInt1);
    controlAnimationUnchecked(paramInt1, paramCancellationSignal, paramWindowInsetsAnimationControlListener, rect, paramBoolean, paramLong, paramInterpolator, paramInt2, i, false);
  }
  
  private void controlAnimationUnchecked(int paramInt1, CancellationSignal paramCancellationSignal, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener, Rect paramRect, boolean paramBoolean1, long paramLong, Interpolator paramInterpolator, int paramInt2, int paramInt3, boolean paramBoolean2) {
    if ((paramInt1 & this.mTypesBeingCancelled) == 0) {
      PendingControlRequest pendingControlRequest;
      InsetsAnimationThreadControlRunner insetsAnimationThreadControlRunner;
      InsetsAnimationControlImpl insetsAnimationControlImpl;
      if (paramInt2 == 2) {
        int j = paramInt1 & this.mDisabledUserAnimationInsetsTypes;
        if (DEBUG) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("user animation disabled types: ");
          stringBuilder1.append(j);
          Log.d("InsetsController", stringBuilder1.toString());
        } 
        int k = this.mDisabledUserAnimationInsetsTypes;
        if (paramBoolean1 && (WindowInsets.Type.ime() & j) != 0) {
          InsetsState insetsState = this.mState;
          if (!insetsState.getSource(13).isVisible())
            getSourceConsumer(13).hide(true, paramInt2); 
        } 
        paramInt1 &= k ^ 0xFFFFFFFF;
      } 
      if (paramInt1 == 0) {
        paramWindowInsetsAnimationControlListener.onCancelled(null);
        if (DEBUG)
          Log.d("InsetsController", "no types to animate in controlAnimationUnchecked"); 
        return;
      } 
      cancelExistingControllers(paramInt1);
      if (DEBUG) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("controlAnimation types: ");
        stringBuilder1.append(paramInt1);
        stringBuilder1.append(", caller:");
        stringBuilder1.append(Debug.getCallers(5));
        Log.d("InsetsController", stringBuilder1.toString());
      } 
      this.mLastStartedAnimTypes |= paramInt1;
      ArraySet<Integer> arraySet = InsetsState.toInternalType(paramInt1);
      SparseArray<InsetsSourceControl> sparseArray = new SparseArray();
      Pair<Integer, Boolean> pair = collectSourceControls(paramBoolean1, arraySet, sparseArray, paramInt2);
      int i = ((Integer)pair.first).intValue();
      paramBoolean1 = ((Boolean)pair.second).booleanValue();
      if (DEBUG)
        Log.d("InsetsController", String.format("controlAnimationUnchecked, typesReady: %s imeReady: %s", new Object[] { Integer.valueOf(i), Boolean.valueOf(paramBoolean1) })); 
      if (!paramBoolean1) {
        abortPendingImeControlRequest();
        pendingControlRequest = new PendingControlRequest(paramInt1, paramWindowInsetsAnimationControlListener, paramLong, paramInterpolator, paramInt2, paramInt3, paramCancellationSignal, paramBoolean2);
        this.mPendingImeControlRequest = pendingControlRequest;
        this.mHandler.postDelayed(this.mPendingControlTimeout, 2000L);
        if (DEBUG)
          Log.d("InsetsController", "Ime not ready. Create pending request"); 
        if (paramCancellationSignal != null)
          paramCancellationSignal.setOnCancelListener(new _$$Lambda$InsetsController$2eTQCcgXs2h2fCMtnCUpVNcB9ps(this, pendingControlRequest)); 
        return;
      } 
      if (i == 0) {
        if (DEBUG)
          Log.d("InsetsController", "No types ready. onCancelled()"); 
        pendingControlRequest.onCancelled(null);
        return;
      } 
      if (paramBoolean2) {
        InsetsState insetsState = this.mState;
        Host host = this.mHost;
        insetsAnimationThreadControlRunner = new InsetsAnimationThreadControlRunner(sparseArray, paramRect, insetsState, (WindowInsetsAnimationControlListener)pendingControlRequest, i, this, paramLong, paramInterpolator, paramInt2, host.getHandler());
      } else {
        insetsAnimationControlImpl = new InsetsAnimationControlImpl(sparseArray, paramRect, this.mState, (WindowInsetsAnimationControlListener)insetsAnimationThreadControlRunner, i, this, paramLong, paramInterpolator, paramInt2);
      } 
      this.mRunningAnimations.add(new RunningAnimation(insetsAnimationControlImpl, paramInt2));
      if (DEBUG) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Animation added to runner. useInsetsAnimationThread: ");
        stringBuilder1.append(paramBoolean2);
        Log.d("InsetsController", stringBuilder1.toString());
      } 
      if (paramCancellationSignal != null)
        paramCancellationSignal.setOnCancelListener(new _$$Lambda$InsetsController$fj4b0iMlmzNGo8WMz8tFflJUfrs(this, insetsAnimationControlImpl)); 
      if (paramInt3 == 0) {
        showDirectly(paramInt1);
      } else {
        hideDirectly(paramInt1, false, paramInt2);
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cannot start a new insets animation of ");
    stringBuilder.append(WindowInsets.Type.toString(paramInt1));
    stringBuilder.append(" while an existing ");
    paramInt1 = this.mTypesBeingCancelled;
    stringBuilder.append(WindowInsets.Type.toString(paramInt1));
    stringBuilder.append(" is being cancelled.");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private Pair<Integer, Boolean> collectSourceControls(boolean paramBoolean, ArraySet<Integer> paramArraySet, SparseArray<InsetsSourceControl> paramSparseArray, int paramInt) {
    int i = 0;
    boolean bool = true;
    for (int j = paramArraySet.size() - 1; j >= 0; j--, i = k) {
      int k;
      InsetsSourceConsumer insetsSourceConsumer = getSourceConsumer(((Integer)paramArraySet.valueAt(j)).intValue());
      if (paramInt == 0 || paramInt == 2) {
        k = 1;
      } else {
        k = 0;
      } 
      boolean bool1 = false;
      if (k) {
        k = insetsSourceConsumer.requestShow(paramBoolean);
        if (k != 0) {
          if (k != 1) {
            k = bool1;
          } else {
            boolean bool2 = false;
            bool = bool2;
            k = bool1;
            if (DEBUG) {
              Log.d("InsetsController", "requestShow IME_SHOW_DELAYED");
              bool = bool2;
              k = bool1;
            } 
          } 
        } else {
          k = 1;
        } 
      } else {
        if (!paramBoolean)
          insetsSourceConsumer.notifyHidden(); 
        k = 1;
      } 
      if (k == 0) {
        k = i;
      } else {
        InsetsSourceControl insetsSourceControl = insetsSourceConsumer.getControl();
        if (insetsSourceControl != null) {
          paramSparseArray.put(insetsSourceConsumer.getType(), new InsetsSourceControl(insetsSourceControl));
          k = i | InsetsState.toPublicType(insetsSourceConsumer.getType());
        } else if (paramInt == 0) {
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("collectSourceControls no control for show(). fromIme: ");
            stringBuilder.append(paramBoolean);
            Log.d("InsetsController", stringBuilder.toString());
          } 
          insetsSourceConsumer.show(paramBoolean);
          k = i;
        } else {
          k = i;
          if (paramInt == 1) {
            insetsSourceConsumer.hide();
            k = i;
          } 
        } 
      } 
    } 
    return new Pair<>(Integer.valueOf(i), Boolean.valueOf(bool));
  }
  
  private int getLayoutInsetsDuringAnimationMode(int paramInt) {
    ArraySet<Integer> arraySet = InsetsState.toInternalType(paramInt);
    for (paramInt = arraySet.size() - 1; paramInt >= 0; paramInt--) {
      InsetsSourceConsumer insetsSourceConsumer = this.mSourceConsumers.get(((Integer)arraySet.valueAt(paramInt)).intValue());
      if (insetsSourceConsumer != null)
        if (!insetsSourceConsumer.isRequestedVisible())
          return 0;  
    } 
    return 1;
  }
  
  private void cancelExistingControllers(int paramInt) {
    int i = this.mTypesBeingCancelled;
    this.mTypesBeingCancelled |= paramInt;
    try {
      for (int j = this.mRunningAnimations.size() - 1; j >= 0; j--) {
        InsetsAnimationControlRunner insetsAnimationControlRunner = ((RunningAnimation)this.mRunningAnimations.get(j)).runner;
        if ((insetsAnimationControlRunner.getTypes() & paramInt) != 0)
          cancelAnimation(insetsAnimationControlRunner, true); 
      } 
      if ((WindowInsets.Type.ime() & paramInt) != 0)
        abortPendingImeControlRequest(); 
      return;
    } finally {
      this.mTypesBeingCancelled = i;
    } 
  }
  
  private void abortPendingImeControlRequest() {
    PendingControlRequest pendingControlRequest = this.mPendingImeControlRequest;
    if (pendingControlRequest != null) {
      pendingControlRequest.listener.onCancelled(null);
      this.mPendingImeControlRequest = null;
      this.mHandler.removeCallbacks(this.mPendingControlTimeout);
      if (DEBUG)
        Log.d("InsetsController", "abortPendingImeControlRequest"); 
    } 
  }
  
  public void notifyFinished(InsetsAnimationControlRunner paramInsetsAnimationControlRunner, boolean paramBoolean) {
    cancelAnimation(paramInsetsAnimationControlRunner, false);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifyFinished. shown: ");
      stringBuilder.append(paramBoolean);
      Log.d("InsetsController", stringBuilder.toString());
    } 
    if (paramBoolean) {
      showDirectly(paramInsetsAnimationControlRunner.getTypes());
    } else {
      int i = paramInsetsAnimationControlRunner.getTypes();
      int j = paramInsetsAnimationControlRunner.getAnimationType();
      hideDirectly(i, true, j);
    } 
  }
  
  public void applySurfaceParams(SyncRtSurfaceTransactionApplier.SurfaceParams... paramVarArgs) {
    this.mHost.applySurfaceParams(paramVarArgs);
  }
  
  void notifyControlRevoked(InsetsSourceConsumer paramInsetsSourceConsumer) {
    for (int i = this.mRunningAnimations.size() - 1; i >= 0; i--) {
      InsetsAnimationControlRunner insetsAnimationControlRunner = ((RunningAnimation)this.mRunningAnimations.get(i)).runner;
      if ((insetsAnimationControlRunner.getTypes() & InsetsState.toPublicType(paramInsetsSourceConsumer.getType())) != 0)
        cancelAnimation(insetsAnimationControlRunner, true); 
    } 
    if (paramInsetsSourceConsumer.getType() == 13)
      abortPendingImeControlRequest(); 
  }
  
  private void cancelAnimation(InsetsAnimationControlRunner paramInsetsAnimationControlRunner, boolean paramBoolean) {
    boolean bool;
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      int j = paramInsetsAnimationControlRunner.getTypes(), k = paramInsetsAnimationControlRunner.getAnimationType();
      stringBuilder.append(String.format("cancelAnimation of types: %d, animType: %d", new Object[] { Integer.valueOf(j), Integer.valueOf(k) }));
      stringBuilder.append(", ");
      stringBuilder.append(Debug.getCallers(5));
      String str = stringBuilder.toString();
      Log.d("InsetsController", str);
    } 
    if (paramBoolean)
      paramInsetsAnimationControlRunner.cancel(); 
    byte b = 0;
    boolean bool1 = false;
    int i = this.mRunningAnimations.size() - 1;
    while (true) {
      int j = b;
      if (i >= 0) {
        boolean bool2;
        RunningAnimation runningAnimation = this.mRunningAnimations.get(i);
        if (runningAnimation.runner == paramInsetsAnimationControlRunner) {
          this.mRunningAnimations.remove(i);
          ArraySet<Integer> arraySet = InsetsState.toInternalType(paramInsetsAnimationControlRunner.getTypes());
          for (j = arraySet.size() - 1, i = bool1; j >= 0; j--)
            bool2 = i | getSourceConsumer(((Integer)arraySet.valueAt(j)).intValue()).notifyAnimationFinished(); 
          bool = bool2;
          if (paramBoolean) {
            bool = bool2;
            if (runningAnimation.startDispatched) {
              dispatchAnimationEnd(runningAnimation.runner.getAnimation());
              bool = bool2;
            } 
          } 
          break;
        } 
        bool2--;
        continue;
      } 
      break;
    } 
    if (bool) {
      this.mHost.notifyInsetsChanged();
      updateRequestedState();
    } 
  }
  
  private void applyLocalVisibilityOverride() {
    for (int i = this.mSourceConsumers.size() - 1; i >= 0; i--) {
      InsetsSourceConsumer insetsSourceConsumer = this.mSourceConsumers.valueAt(i);
      insetsSourceConsumer.applyLocalVisibilityOverride();
    } 
  }
  
  public InsetsSourceConsumer getSourceConsumer(int paramInt) {
    InsetsSourceConsumer insetsSourceConsumer = this.mSourceConsumers.get(paramInt);
    if (insetsSourceConsumer != null)
      return insetsSourceConsumer; 
    insetsSourceConsumer = this.mConsumerCreator.apply(this, Integer.valueOf(paramInt));
    this.mSourceConsumers.put(paramInt, insetsSourceConsumer);
    return insetsSourceConsumer;
  }
  
  public void notifyVisibilityChanged() {
    this.mHost.notifyInsetsChanged();
    updateRequestedState();
  }
  
  public void updateCompatSysUiVisibility(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    this.mHost.updateCompatSysUiVisibility(paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void onWindowFocusGained() {
    getSourceConsumer(13).onWindowFocusGained();
  }
  
  public void onWindowFocusLost() {
    getSourceConsumer(13).onWindowFocusLost();
  }
  
  public void applyImeVisibility(boolean paramBoolean) {
    if (paramBoolean) {
      show(8, true);
    } else {
      hide(8);
    } 
  }
  
  public int getAnimationType(int paramInt) {
    for (int i = this.mRunningAnimations.size() - 1; i >= 0; i--) {
      InsetsAnimationControlRunner insetsAnimationControlRunner = ((RunningAnimation)this.mRunningAnimations.get(i)).runner;
      if (insetsAnimationControlRunner.controlsInternalType(paramInt))
        return ((RunningAnimation)this.mRunningAnimations.get(i)).type; 
    } 
    return -1;
  }
  
  private void updateRequestedState() {
    boolean bool = false;
    for (int i = this.mSourceConsumers.size() - 1; i >= 0; i--, bool = bool1) {
      boolean bool1;
      InsetsSourceConsumer insetsSourceConsumer = this.mSourceConsumers.valueAt(i);
      int j = insetsSourceConsumer.getType();
      if (j == 2) {
        bool1 = bool;
      } else {
        bool1 = bool;
        if (insetsSourceConsumer.getControl() != null) {
          InsetsSource insetsSource = this.mState.getSource(j);
          bool1 = bool;
          if (!insetsSource.equals(this.mRequestedState.peekSource(j))) {
            this.mRequestedState.addSource(new InsetsSource(insetsSource));
            bool1 = true;
          } 
          if (!insetsSource.equals(this.mLastDispatchedState.peekSource(j)))
            bool1 = true; 
        } 
      } 
    } 
    if (!bool)
      return; 
    this.mHost.onInsetsModified(this.mRequestedState);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateRequestedState ");
      stringBuilder.append(this.mRequestedState);
      stringBuilder.append(", caller:");
      stringBuilder.append(Debug.getCallers(5));
      Log.d("InsetsController", stringBuilder.toString());
    } 
  }
  
  public void applyAnimation(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramInt == 0) {
      if (DEBUG)
        Log.d("InsetsController", "applyAnimation, nothing to animate"); 
      return;
    } 
    boolean bool1 = this.mHost.hasAnimationCallbacks();
    boolean bool2 = this.mAnimationsDisabled;
    Host host = this.mHost;
    InternalAnimationControlListener internalAnimationControlListener = new InternalAnimationControlListener(paramBoolean1, bool1, paramInt, bool2, host.dipToPx(-80));
    InsetsState insetsState = this.mState;
    Rect rect = insetsState.getDisplayFrame();
    long l = internalAnimationControlListener.getDurationMs();
    Interpolator interpolator = internalAnimationControlListener.getInterpolator();
    controlAnimationUnchecked(paramInt, null, internalAnimationControlListener, rect, paramBoolean2, l, interpolator, paramBoolean1 ^ true, paramBoolean1 ^ true, bool1 ^ true);
  }
  
  private void hideDirectly(int paramInt1, boolean paramBoolean, int paramInt2) {
    ArraySet<Integer> arraySet = InsetsState.toInternalType(paramInt1);
    for (paramInt1 = arraySet.size() - 1; paramInt1 >= 0; paramInt1--)
      getSourceConsumer(((Integer)arraySet.valueAt(paramInt1)).intValue()).hide(paramBoolean, paramInt2); 
  }
  
  private void showDirectly(int paramInt) {
    ArraySet<Integer> arraySet = InsetsState.toInternalType(paramInt);
    for (paramInt = arraySet.size() - 1; paramInt >= 0; paramInt--)
      getSourceConsumer(((Integer)arraySet.valueAt(paramInt)).intValue()).show(false); 
  }
  
  public void cancelExistingAnimations() {
    cancelExistingControllers(WindowInsets.Type.all());
  }
  
  void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.println(paramString);
    paramPrintWriter.println("InsetsController:");
    InsetsState insetsState = this.mState;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("  ");
    insetsState.dump(stringBuilder.toString(), paramPrintWriter);
  }
  
  public void startAnimation(InsetsAnimationControlImpl paramInsetsAnimationControlImpl, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener, int paramInt, WindowInsetsAnimation paramWindowInsetsAnimation, WindowInsetsAnimation.Bounds paramBounds) {
    this.mHost.dispatchWindowInsetsAnimationPrepare(paramWindowInsetsAnimation);
    this.mHost.addOnPreDrawRunnable(new _$$Lambda$InsetsController$VzAUS17blucBK1u6_37NlEpdYqI(this, paramInsetsAnimationControlImpl, paramInt, paramWindowInsetsAnimation, paramBounds, paramWindowInsetsAnimationControlListener));
  }
  
  public void dispatchAnimationEnd(WindowInsetsAnimation paramWindowInsetsAnimation) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InsetsAnimation: ");
    stringBuilder.append(WindowInsets.Type.toString(paramWindowInsetsAnimation.getTypeMask()));
    String str = stringBuilder.toString();
    int i = paramWindowInsetsAnimation.getTypeMask();
    Trace.asyncTraceEnd(8L, str, i);
    this.mHost.dispatchWindowInsetsAnimationEnd(paramWindowInsetsAnimation);
  }
  
  public void scheduleApplyChangeInsets(InsetsAnimationControlRunner paramInsetsAnimationControlRunner) {
    if (this.mStartingAnimation || paramInsetsAnimationControlRunner.getAnimationType() == 2) {
      this.mAnimCallback.run();
      this.mAnimCallbackScheduled = false;
      return;
    } 
    if (!this.mAnimCallbackScheduled) {
      this.mHost.postInsetsAnimationCallback(this.mAnimCallback);
      this.mAnimCallbackScheduled = true;
    } 
  }
  
  public void setSystemBarsAppearance(int paramInt1, int paramInt2) {
    this.mHost.setSystemBarsAppearance(paramInt1, paramInt2);
  }
  
  public int getSystemBarsAppearance() {
    return this.mHost.getSystemBarsAppearance();
  }
  
  public void setCaptionInsetsHeight(int paramInt) {
    this.mCaptionInsetsHeight = paramInt;
  }
  
  public void setSystemBarsBehavior(int paramInt) {
    this.mHost.setSystemBarsBehavior(paramInt);
  }
  
  public int getSystemBarsBehavior() {
    return this.mHost.getSystemBarsBehavior();
  }
  
  public void setAnimationsDisabled(boolean paramBoolean) {
    this.mAnimationsDisabled = paramBoolean;
  }
  
  private int calculateControllableTypes() {
    int i = 0;
    for (int j = this.mSourceConsumers.size() - 1; j >= 0; j--, i = k) {
      InsetsSourceConsumer insetsSourceConsumer = this.mSourceConsumers.valueAt(j);
      InsetsSource insetsSource = this.mState.peekSource(insetsSourceConsumer.mType);
      int k = i;
      if (insetsSourceConsumer.getControl() != null) {
        k = i;
        if (insetsSource != null) {
          k = i;
          if (insetsSource.isUserControllable())
            k = i | InsetsState.toPublicType(insetsSourceConsumer.mType); 
        } 
      } 
    } 
    return (this.mState.calculateUncontrollableInsetsFromFrame(this.mFrame) ^ 0xFFFFFFFF) & i;
  }
  
  private int invokeControllableInsetsChangedListeners() {
    this.mHandler.removeCallbacks(this.mInvokeControllableInsetsChangedListeners);
    this.mLastStartedAnimTypes = 0;
    int i = calculateControllableTypes();
    int j = this.mControllableInsetsChangedListeners.size();
    for (byte b = 0; b < j; b++)
      ((WindowInsetsController.OnControllableInsetsChangedListener)this.mControllableInsetsChangedListeners.get(b)).onControllableInsetsChanged(this, i); 
    return this.mLastStartedAnimTypes;
  }
  
  public void addOnControllableInsetsChangedListener(WindowInsetsController.OnControllableInsetsChangedListener paramOnControllableInsetsChangedListener) {
    Objects.requireNonNull(paramOnControllableInsetsChangedListener);
    this.mControllableInsetsChangedListeners.add(paramOnControllableInsetsChangedListener);
    paramOnControllableInsetsChangedListener.onControllableInsetsChanged(this, calculateControllableTypes());
  }
  
  public void removeOnControllableInsetsChangedListener(WindowInsetsController.OnControllableInsetsChangedListener paramOnControllableInsetsChangedListener) {
    Objects.requireNonNull(paramOnControllableInsetsChangedListener);
    this.mControllableInsetsChangedListeners.remove(paramOnControllableInsetsChangedListener);
  }
  
  public void releaseSurfaceControlFromRt(SurfaceControl paramSurfaceControl) {
    this.mHost.releaseSurfaceControlFromRt(paramSurfaceControl);
  }
  
  public void reportPerceptible(int paramInt, boolean paramBoolean) {
    ArraySet<Integer> arraySet = InsetsState.toInternalType(paramInt);
    int i = this.mSourceConsumers.size();
    for (paramInt = 0; paramInt < i; paramInt++) {
      InsetsSourceConsumer insetsSourceConsumer = this.mSourceConsumers.valueAt(paramInt);
      if (arraySet.contains(Integer.valueOf(insetsSourceConsumer.getType())))
        insetsSourceConsumer.onPerceptible(paramBoolean); 
    } 
  }
  
  Host getHost() {
    return this.mHost;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AnimationType implements Annotation {}
  
  class Host {
    public abstract void addOnPreDrawRunnable(Runnable param1Runnable);
    
    public abstract void applySurfaceParams(SyncRtSurfaceTransactionApplier.SurfaceParams... param1VarArgs);
    
    public abstract int dipToPx(int param1Int);
    
    public abstract void dispatchWindowInsetsAnimationEnd(WindowInsetsAnimation param1WindowInsetsAnimation);
    
    public abstract void dispatchWindowInsetsAnimationPrepare(WindowInsetsAnimation param1WindowInsetsAnimation);
    
    public abstract WindowInsets dispatchWindowInsetsAnimationProgress(WindowInsets param1WindowInsets, List<WindowInsetsAnimation> param1List);
    
    public abstract WindowInsetsAnimation.Bounds dispatchWindowInsetsAnimationStart(WindowInsetsAnimation param1WindowInsetsAnimation, WindowInsetsAnimation.Bounds param1Bounds);
    
    public abstract Handler getHandler();
    
    public abstract InputMethodManager getInputMethodManager();
    
    public abstract String getRootViewTitle();
    
    public abstract int getSystemBarsAppearance();
    
    public abstract int getSystemBarsBehavior();
    
    public abstract IBinder getWindowToken();
    
    public abstract boolean hasAnimationCallbacks();
    
    public abstract void notifyInsetsChanged();
    
    public abstract void onInsetsModified(InsetsState param1InsetsState);
    
    public abstract void postInsetsAnimationCallback(Runnable param1Runnable);
    
    public abstract void releaseSurfaceControlFromRt(SurfaceControl param1SurfaceControl);
    
    public abstract void setSystemBarsAppearance(int param1Int1, int param1Int2);
    
    public abstract void setSystemBarsBehavior(int param1Int);
    
    public abstract void updateCompatSysUiVisibility(int param1Int, boolean param1Boolean1, boolean param1Boolean2);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class LayoutInsetsDuringAnimation implements Annotation {}
}
