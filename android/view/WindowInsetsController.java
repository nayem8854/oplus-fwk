package android.view;

import android.os.CancellationSignal;
import android.view.animation.Interpolator;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface WindowInsetsController {
  public static final int APPEARANCE_LIGHT_NAVIGATION_BARS = 16;
  
  public static final int APPEARANCE_LIGHT_STATUS_BARS = 8;
  
  public static final int APPEARANCE_LOW_PROFILE_BARS = 4;
  
  public static final int APPEARANCE_OPAQUE_NAVIGATION_BARS = 2;
  
  public static final int APPEARANCE_OPAQUE_STATUS_BARS = 1;
  
  public static final int BEHAVIOR_SHOW_BARS_BY_SWIPE = 1;
  
  public static final int BEHAVIOR_SHOW_BARS_BY_TOUCH = 0;
  
  public static final int BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE = 2;
  
  void addOnControllableInsetsChangedListener(OnControllableInsetsChangedListener paramOnControllableInsetsChangedListener);
  
  void controlWindowInsetsAnimation(int paramInt, long paramLong, Interpolator paramInterpolator, CancellationSignal paramCancellationSignal, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener);
  
  InsetsState getState();
  
  int getSystemBarsAppearance();
  
  int getSystemBarsBehavior();
  
  void hide(int paramInt);
  
  boolean isRequestedVisible(int paramInt);
  
  void removeOnControllableInsetsChangedListener(OnControllableInsetsChangedListener paramOnControllableInsetsChangedListener);
  
  void setAnimationsDisabled(boolean paramBoolean);
  
  void setCaptionInsetsHeight(int paramInt);
  
  void setSystemBarsAppearance(int paramInt1, int paramInt2);
  
  void setSystemBarsBehavior(int paramInt);
  
  void show(int paramInt);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Appearance {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Behavior {}
  
  public static interface OnControllableInsetsChangedListener {
    void onControllableInsetsChanged(WindowInsetsController param1WindowInsetsController, int param1Int);
  }
}
