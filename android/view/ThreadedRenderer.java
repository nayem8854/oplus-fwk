package android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.HardwareRenderer;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RenderNode;
import android.os.Trace;
import android.util.Log;
import android.view.animation.AnimationUtils;
import com.android.internal.R;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public final class ThreadedRenderer extends HardwareRenderer {
  public static final String DEBUG_DIRTY_REGIONS_PROPERTY = "debug.hwui.show_dirty_regions";
  
  public static final String DEBUG_FORCE_DARK = "debug.hwui.force_dark";
  
  public static final String DEBUG_FPS_DIVISOR = "debug.hwui.fps_divisor";
  
  public static final String DEBUG_OVERDRAW_PROPERTY = "debug.hwui.overdraw";
  
  public static final String DEBUG_SHOW_LAYERS_UPDATES_PROPERTY = "debug.hwui.show_layers_updates";
  
  public static final String DEBUG_SHOW_NON_RECTANGULAR_CLIP_PROPERTY = "debug.hwui.show_non_rect_clip";
  
  public static int EGL_CONTEXT_PRIORITY_HIGH_IMG = 12545;
  
  public static int EGL_CONTEXT_PRIORITY_LOW_IMG = 0;
  
  public static int EGL_CONTEXT_PRIORITY_MEDIUM_IMG = 12546;
  
  public static final String OVERDRAW_PROPERTY_SHOW = "show";
  
  static final String PRINT_CONFIG_PROPERTY = "debug.hwui.print_config";
  
  static final String PROFILE_MAXFRAMES_PROPERTY = "debug.hwui.profile.maxframes";
  
  public static final String PROFILE_PROPERTY = "debug.hwui.profile";
  
  public static final String PROFILE_PROPERTY_VISUALIZE_BARS = "visual_bars";
  
  private static final String[] VISUALIZERS;
  
  public static boolean sRendererDisabled;
  
  public static boolean sSystemRendererDisabled;
  
  public static boolean sTrimForeground;
  
  private boolean mEnabled;
  
  private int mHeight;
  
  static {
    EGL_CONTEXT_PRIORITY_LOW_IMG = 12547;
    isAvailable();
    sRendererDisabled = false;
    sSystemRendererDisabled = false;
    sTrimForeground = false;
    VISUALIZERS = new String[] { "visual_bars" };
  }
  
  public static void disable(boolean paramBoolean) {
    sRendererDisabled = true;
    if (paramBoolean)
      sSystemRendererDisabled = true; 
  }
  
  public static void enableForegroundTrimming() {
    sTrimForeground = true;
  }
  
  public static boolean isAvailable() {
    return true;
  }
  
  public static ThreadedRenderer create(Context paramContext, boolean paramBoolean, String paramString) {
    ThreadedRenderer threadedRenderer = null;
    if (isAvailable())
      threadedRenderer = new ThreadedRenderer(paramContext, paramBoolean, paramString); 
    return threadedRenderer;
  }
  
  private boolean mInitialized = false;
  
  private int mInsetLeft;
  
  private int mInsetTop;
  
  private final float mLightRadius;
  
  private final float mLightY;
  
  private final float mLightZ;
  
  private ArrayList<HardwareRenderer.FrameDrawingCallback> mNextRtFrameCallbacks;
  
  private boolean mRequested = true;
  
  private boolean mRootNodeNeedsUpdate;
  
  private int mSurfaceHeight;
  
  private int mSurfaceWidth;
  
  private int mWidth;
  
  ThreadedRenderer(Context paramContext, boolean paramBoolean, String paramString) {
    setName(paramString);
    setOpaque(paramBoolean ^ true);
    TypedArray typedArray = paramContext.obtainStyledAttributes(null, R.styleable.Lighting, 0, 0);
    this.mLightY = typedArray.getDimension(3, 0.0F);
    this.mLightZ = typedArray.getDimension(4, 0.0F);
    this.mLightRadius = typedArray.getDimension(2, 0.0F);
    float f1 = typedArray.getFloat(0, 0.0F);
    float f2 = typedArray.getFloat(1, 0.0F);
    typedArray.recycle();
    setLightSourceAlpha(f1, f2);
  }
  
  public void destroy() {
    this.mInitialized = false;
    updateEnabledState((Surface)null);
    super.destroy();
  }
  
  boolean isEnabled() {
    return this.mEnabled;
  }
  
  void setEnabled(boolean paramBoolean) {
    this.mEnabled = paramBoolean;
  }
  
  boolean isRequested() {
    return this.mRequested;
  }
  
  void setRequested(boolean paramBoolean) {
    this.mRequested = paramBoolean;
  }
  
  private void updateEnabledState(Surface paramSurface) {
    if (paramSurface == null || !paramSurface.isValid()) {
      setEnabled(false);
      return;
    } 
    setEnabled(this.mInitialized);
  }
  
  boolean initialize(Surface paramSurface) throws Surface.OutOfResourcesException {
    boolean bool = this.mInitialized;
    this.mInitialized = true;
    updateEnabledState(paramSurface);
    setSurface(paramSurface);
    return bool ^ true;
  }
  
  boolean initializeIfNeeded(int paramInt1, int paramInt2, View.AttachInfo paramAttachInfo, Surface paramSurface, Rect paramRect) throws Surface.OutOfResourcesException {
    if (isRequested())
      if (!isEnabled() && 
        initialize(paramSurface)) {
        setup(paramInt1, paramInt2, paramAttachInfo, paramRect);
        return true;
      }  
    return false;
  }
  
  void updateSurface(Surface paramSurface) throws Surface.OutOfResourcesException {
    updateEnabledState(paramSurface);
    setSurface(paramSurface);
  }
  
  public void setSurface(Surface paramSurface) {
    if (paramSurface != null && paramSurface.isValid()) {
      super.setSurface(paramSurface);
    } else {
      super.setSurface(null);
    } 
  }
  
  void registerRtFrameCallback(HardwareRenderer.FrameDrawingCallback paramFrameDrawingCallback) {
    if (this.mNextRtFrameCallbacks == null)
      this.mNextRtFrameCallbacks = new ArrayList<>(); 
    this.mNextRtFrameCallbacks.add(paramFrameDrawingCallback);
  }
  
  void destroyHardwareResources(View paramView) {
    destroyResources(paramView);
    clearContent();
  }
  
  private static void destroyResources(View paramView) {
    paramView.destroyHardwareResources();
  }
  
  void setup(int paramInt1, int paramInt2, View.AttachInfo paramAttachInfo, Rect paramRect) {
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    if (paramRect != null && (paramRect.left != 0 || paramRect.right != 0 || paramRect.top != 0 || paramRect.bottom != 0)) {
      this.mInsetLeft = paramRect.left;
      this.mInsetTop = paramRect.top;
      this.mSurfaceWidth = this.mInsetLeft + paramInt1 + paramRect.right;
      this.mSurfaceHeight = this.mInsetTop + paramInt2 + paramRect.bottom;
      setOpaque(false);
    } else {
      this.mInsetLeft = 0;
      this.mInsetTop = 0;
      this.mSurfaceWidth = paramInt1;
      this.mSurfaceHeight = paramInt2;
    } 
    this.mRootNode.setLeftTopRightBottom(-this.mInsetLeft, -this.mInsetTop, this.mSurfaceWidth, this.mSurfaceHeight);
    setLightCenter(paramAttachInfo);
  }
  
  void setLightCenter(View.AttachInfo paramAttachInfo) {
    Point point = paramAttachInfo.mPoint;
    paramAttachInfo.mDisplay.getRealSize(point);
    float f1 = point.x / 2.0F, f2 = paramAttachInfo.mWindowLeft;
    float f3 = this.mLightY, f4 = paramAttachInfo.mWindowTop;
    setLightSourceGeometry(f1 - f2, f3 - f4, this.mLightZ, this.mLightRadius);
  }
  
  int getWidth() {
    return this.mWidth;
  }
  
  int getHeight() {
    return this.mHeight;
  }
  
  void dumpGfxInfo(PrintWriter paramPrintWriter, FileDescriptor paramFileDescriptor, String[] paramArrayOfString) {
    int i;
    paramPrintWriter.flush();
    if (paramArrayOfString == null || paramArrayOfString.length == 0) {
      i = 1;
    } else {
      i = 0;
    } 
    int j;
    for (byte b = 0; b < paramArrayOfString.length; b++, j = i) {
      String str = paramArrayOfString[b];
      i = -1;
      int k = str.hashCode();
      if (k != -252053678) {
        if (k != 1492) {
          if (k == 108404047 && str.equals("reset"))
            i = 1; 
        } else if (str.equals("-a")) {
          i = 2;
        } 
      } else if (str.equals("framestats")) {
        i = 0;
      } 
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            i = j;
          } else {
            i = 1;
          } 
        } else {
          i = j | 0x2;
        } 
      } else {
        i = j | 0x1;
      } 
    } 
    dumpProfileInfo(paramFileDescriptor, j);
  }
  
  Picture captureRenderingCommands() {
    return null;
  }
  
  public boolean loadSystemProperties() {
    boolean bool = super.loadSystemProperties();
    if (bool)
      invalidateRoot(); 
    return bool;
  }
  
  private void updateViewTreeDisplayList(View paramView) {
    boolean bool;
    paramView.mPrivateFlags |= 0x20;
    if ((paramView.mPrivateFlags & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
      bool = true;
    } else {
      bool = false;
    } 
    paramView.mRecreateDisplayList = bool;
    paramView.mPrivateFlags &= Integer.MAX_VALUE;
    paramView.updateDisplayListIfDirty();
    paramView.mRecreateDisplayList = false;
  }
  
  private void updateRootDisplayList(View paramView, DrawCallbacks paramDrawCallbacks) {
    Trace.traceBegin(8L, "Record View#draw()");
    updateViewTreeDisplayList(paramView);
    if (this.mNextRtFrameCallbacks != null) {
      ArrayList<HardwareRenderer.FrameDrawingCallback> arrayList = this.mNextRtFrameCallbacks;
      this.mNextRtFrameCallbacks = null;
      setFrameCallback(new _$$Lambda$ThreadedRenderer$ydBD_R1iP5u_97XYakm_jKvC1b4(arrayList));
    } 
    if (this.mRootNodeNeedsUpdate || !this.mRootNode.hasDisplayList()) {
      RecordingCanvas recordingCanvas = this.mRootNode.beginRecording(this.mSurfaceWidth, this.mSurfaceHeight);
      try {
        int i = recordingCanvas.save();
        recordingCanvas.translate(this.mInsetLeft, this.mInsetTop);
        paramDrawCallbacks.onPreDraw(recordingCanvas);
        recordingCanvas.enableZ();
        recordingCanvas.drawRenderNode(paramView.updateDisplayListIfDirty());
        recordingCanvas.disableZ();
        paramDrawCallbacks.onPostDraw(recordingCanvas);
        recordingCanvas.restoreToCount(i);
        this.mRootNodeNeedsUpdate = false;
        this.mRootNode.endRecording();
        return;
      } finally {
        this.mRootNode.endRecording();
      } 
    } 
    Trace.traceEnd(8L);
  }
  
  void invalidateRoot() {
    this.mRootNodeNeedsUpdate = true;
  }
  
  void draw(View paramView, View.AttachInfo paramAttachInfo, DrawCallbacks paramDrawCallbacks) {
    Choreographer choreographer = paramAttachInfo.mViewRootImpl.mChoreographer;
    choreographer.mFrameInfo.markDrawStart();
    updateRootDisplayList(paramView, paramDrawCallbacks);
    if (paramAttachInfo.mPendingAnimatingRenderNodes != null) {
      int j = paramAttachInfo.mPendingAnimatingRenderNodes.size();
      for (byte b = 0; b < j; b++) {
        List<RenderNode> list = paramAttachInfo.mPendingAnimatingRenderNodes;
        RenderNode renderNode = list.get(b);
        registerAnimatingRenderNode(renderNode);
      } 
      paramAttachInfo.mPendingAnimatingRenderNodes.clear();
      paramAttachInfo.mPendingAnimatingRenderNodes = null;
    } 
    int i = syncAndDrawFrame(choreographer.mFrameInfo);
    if ((i & 0x2) != 0) {
      Log.w("OpenGLRenderer", "Surface lost, forcing relayout");
      paramAttachInfo.mViewRootImpl.mForceNextWindowRelayout = true;
      paramAttachInfo.mViewRootImpl.requestLayout();
    } 
    if ((i & 0x1) != 0)
      paramAttachInfo.mViewRootImpl.invalidate(); 
  }
  
  public RenderNode getRootNode() {
    return this.mRootNode;
  }
  
  class DrawCallbacks {
    public abstract void onPostDraw(RecordingCanvas param1RecordingCanvas);
    
    public abstract void onPreDraw(RecordingCanvas param1RecordingCanvas);
  }
  
  public static class SimpleRenderer extends HardwareRenderer {
    private final float mLightRadius;
    
    private final float mLightY;
    
    private final float mLightZ;
    
    public SimpleRenderer(Context param1Context, String param1String, Surface param1Surface) {
      setName(param1String);
      setOpaque(false);
      setSurface(param1Surface);
      TypedArray typedArray = param1Context.obtainStyledAttributes(null, R.styleable.Lighting, 0, 0);
      this.mLightY = typedArray.getDimension(3, 0.0F);
      this.mLightZ = typedArray.getDimension(4, 0.0F);
      this.mLightRadius = typedArray.getDimension(2, 0.0F);
      float f1 = typedArray.getFloat(0, 0.0F);
      float f2 = typedArray.getFloat(1, 0.0F);
      typedArray.recycle();
      setLightSourceAlpha(f1, f2);
    }
    
    public void setLightCenter(Display param1Display, int param1Int1, int param1Int2) {
      Point point = new Point();
      param1Display.getRealSize(point);
      float f1 = point.x / 2.0F, f2 = param1Int1;
      float f3 = this.mLightY, f4 = param1Int2;
      setLightSourceGeometry(f1 - f2, f3 - f4, this.mLightZ, this.mLightRadius);
    }
    
    public RenderNode getRootNode() {
      return this.mRootNode;
    }
    
    public void draw(HardwareRenderer.FrameDrawingCallback param1FrameDrawingCallback) {
      long l = AnimationUtils.currentAnimationTimeMillis();
      if (param1FrameDrawingCallback != null)
        setFrameCallback(param1FrameDrawingCallback); 
      HardwareRenderer.FrameRenderRequest frameRenderRequest = createRenderRequest();
      frameRenderRequest = frameRenderRequest.setVsyncTime(l * 1000000L);
      frameRenderRequest.syncAndDraw();
    }
  }
}
