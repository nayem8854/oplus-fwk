package android.view;

import android.os.Parcel;

public interface IOplusLongshotViewHelper extends IOplusLongshotWindow {
  boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2);
}
