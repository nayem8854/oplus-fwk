package android.view;

import android.graphics.Insets;
import android.view.animation.Interpolator;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public final class WindowInsetsAnimation {
  private float mAlpha;
  
  private final long mDurationMillis;
  
  private float mFraction;
  
  private final Interpolator mInterpolator;
  
  private final int mTypeMask;
  
  public WindowInsetsAnimation(int paramInt, Interpolator paramInterpolator, long paramLong) {
    this.mTypeMask = paramInt;
    this.mInterpolator = paramInterpolator;
    this.mDurationMillis = paramLong;
  }
  
  public int getTypeMask() {
    return this.mTypeMask;
  }
  
  public float getFraction() {
    return this.mFraction;
  }
  
  public float getInterpolatedFraction() {
    Interpolator interpolator = this.mInterpolator;
    if (interpolator != null)
      return interpolator.getInterpolation(this.mFraction); 
    return this.mFraction;
  }
  
  public Interpolator getInterpolator() {
    return this.mInterpolator;
  }
  
  public long getDurationMillis() {
    return this.mDurationMillis;
  }
  
  public void setFraction(float paramFloat) {
    this.mFraction = paramFloat;
  }
  
  public float getAlpha() {
    return this.mAlpha;
  }
  
  public void setAlpha(float paramFloat) {
    this.mAlpha = paramFloat;
  }
  
  public static final class Bounds {
    private final Insets mLowerBound;
    
    private final Insets mUpperBound;
    
    public Bounds(Insets param1Insets1, Insets param1Insets2) {
      this.mLowerBound = param1Insets1;
      this.mUpperBound = param1Insets2;
    }
    
    public Insets getLowerBound() {
      return this.mLowerBound;
    }
    
    public Insets getUpperBound() {
      return this.mUpperBound;
    }
    
    public Bounds inset(Insets param1Insets) {
      Insets insets1 = this.mLowerBound;
      int i = param1Insets.left, j = param1Insets.top, k = param1Insets.right, m = param1Insets.bottom;
      Insets insets2 = WindowInsets.insetInsets(insets1, i, j, k, m);
      insets1 = this.mUpperBound;
      j = param1Insets.left;
      i = param1Insets.top;
      k = param1Insets.right;
      m = param1Insets.bottom;
      return new Bounds(insets2, WindowInsets.insetInsets(insets1, j, i, k, m));
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Bounds{lower=");
      stringBuilder.append(this.mLowerBound);
      stringBuilder.append(" upper=");
      stringBuilder.append(this.mUpperBound);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  public static abstract class Callback {
    public static final int DISPATCH_MODE_CONTINUE_ON_SUBTREE = 1;
    
    public static final int DISPATCH_MODE_STOP = 0;
    
    private final int mDispatchMode;
    
    public Callback(int param1Int) {
      this.mDispatchMode = param1Int;
    }
    
    public final int getDispatchMode() {
      return this.mDispatchMode;
    }
    
    public void onPrepare(WindowInsetsAnimation param1WindowInsetsAnimation) {}
    
    public WindowInsetsAnimation.Bounds onStart(WindowInsetsAnimation param1WindowInsetsAnimation, WindowInsetsAnimation.Bounds param1Bounds) {
      return param1Bounds;
    }
    
    public void onEnd(WindowInsetsAnimation param1WindowInsetsAnimation) {}
    
    public abstract WindowInsets onProgress(WindowInsets param1WindowInsets, List<WindowInsetsAnimation> param1List);
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface DispatchMode {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DispatchMode {}
}
