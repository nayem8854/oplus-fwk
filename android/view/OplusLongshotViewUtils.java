package android.view;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.oplus.screenshot.OplusLongshotDump;
import com.oplus.util.OplusLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OplusLongshotViewUtils {
  private static final boolean DBG = OplusLongshotDump.DBG;
  
  private static final RectComparator RECT_COMPARATOR = new RectComparator();
  
  private static final String TAG = "LongshotDump";
  
  private final Rect mTempRect1 = new Rect();
  
  private final Rect mTempRect2 = new Rect();
  
  private final Rect mTempRect3 = new Rect();
  
  private boolean needUpdateParent(Rect paramRect1, Rect paramRect2, OplusLongshotViewContent paramOplusLongshotViewContent, boolean paramBoolean) {
    if (isLargeCoverRect(paramRect1, paramRect2, paramBoolean))
      return false; 
    return isLargeCoverRect(paramOplusLongshotViewContent.getRect(), paramRect2, paramBoolean);
  }
  
  public void findCoverRect(int paramInt, ViewGroup paramViewGroup, View paramView, List<OplusLongshotViewContent> paramList1, List<OplusLongshotViewContent> paramList2, Rect paramRect, OplusLongshotViewContent paramOplusLongshotViewContent, boolean paramBoolean) {
    Rect rect2;
    boolean bool1, bool2;
    Rect rect1 = new Rect();
    paramViewGroup.getBoundsOnScreen(rect1, true);
    if (paramRect == null) {
      rect2 = rect1;
    } else {
      rect2 = paramRect;
    } 
    int i = 0;
    if (paramOplusLongshotViewContent == null) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    String str4 = getPrefix(paramInt);
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str4);
    stringBuilder.append("findCoverRect : rootRect=");
    stringBuilder.append(rect2);
    stringBuilder.append(", srcRect=");
    stringBuilder.append(rect1);
    stringBuilder.append(", group=");
    stringBuilder.append(paramViewGroup);
    stringBuilder.append(", keepLargeRect=");
    stringBuilder.append(paramBoolean);
    String str1 = stringBuilder.toString(), str5 = "LongshotDump";
    OplusLog.d(bool, "LongshotDump", str1);
    int j = paramViewGroup.getChildCount();
    ArrayList<View> arrayList2 = paramViewGroup.buildOrderedChildList();
    if (arrayList2 == null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    int k = i;
    if (bool2) {
      k = i;
      if (paramViewGroup.isChildrenDrawingOrderEnabled())
        k = 1; 
    } 
    OplusLongshotViewContent oplusLongshotViewContent = paramOplusLongshotViewContent;
    i = j - 1;
    rect1 = rect2;
    String str2 = str4, str3 = str5;
    ArrayList<View> arrayList1 = arrayList2;
    while (true) {
      if (i >= 0) {
        View view;
        int m;
        if (k != 0) {
          m = paramViewGroup.getChildDrawingOrder(j, i);
        } else {
          m = i;
        } 
        if (bool2) {
          view = paramViewGroup.getChildAt(m);
        } else {
          view = arrayList1.get(m);
        } 
        if (view == null) {
          m = i;
        } else {
          if (view == paramView)
            break; 
          if (!view.isVisibleToUser()) {
            m = i;
          } else {
            view.getBoundsOnScreen(this.mTempRect3, true);
            if (!Rect.intersects(this.mTempRect3, rect1)) {
              m = i;
            } else {
              ViewGroup viewGroup;
              if (isTransparentGroup(view)) {
                StringBuilder stringBuilder1;
                if (bool1)
                  oplusLongshotViewContent = new OplusLongshotViewContent(view, this.mTempRect3, null); 
                if (isWaterMarkGroup(rect1, (ViewGroup)view)) {
                  OplusLongshotViewContent oplusLongshotViewContent1 = new OplusLongshotViewContent(view, this.mTempRect3, oplusLongshotViewContent);
                  bool = DBG;
                  stringBuilder1 = new StringBuilder();
                  stringBuilder1.append(str2);
                  stringBuilder1.append("  skipCoverRect : isWaterMarkGroup=");
                  stringBuilder1.append(oplusLongshotViewContent1);
                  OplusLog.d(bool, str3, stringBuilder1.toString());
                } else if (isSideBarGroup(rect1, (ViewGroup)stringBuilder1, paramList2)) {
                  OplusLongshotViewContent oplusLongshotViewContent1 = new OplusLongshotViewContent((View)stringBuilder1, this.mTempRect3, oplusLongshotViewContent);
                  bool = DBG;
                  stringBuilder1 = new StringBuilder();
                  stringBuilder1.append(str2);
                  stringBuilder1.append("  skipCoverRect : isSideBarGroup=");
                  stringBuilder1.append(oplusLongshotViewContent1);
                  OplusLog.d(bool, str3, stringBuilder1.toString());
                  paramList2.add(oplusLongshotViewContent1);
                } else {
                  if (needUpdateParent(this.mTempRect3, rect1, oplusLongshotViewContent, paramBoolean))
                    oplusLongshotViewContent = new OplusLongshotViewContent((View)stringBuilder1, this.mTempRect3, null); 
                  arrayList2 = new ArrayList<>();
                  viewGroup = (ViewGroup)stringBuilder1;
                  m = i;
                  findCoverRect(paramInt + 1, viewGroup, null, (List)arrayList2, paramList2, rect1, oplusLongshotViewContent, paramBoolean);
                  paramList1.addAll(arrayList2);
                  i--;
                } 
                m = i;
              } 
              m = i;
              if (hasVisibleContent(str2, viewGroup, rect1, paramBoolean, "noCoverContent")) {
                if (bool1)
                  oplusLongshotViewContent = new OplusLongshotViewContent(viewGroup, this.mTempRect3, null); 
                OplusLongshotViewContent oplusLongshotViewContent1 = new OplusLongshotViewContent(viewGroup, this.mTempRect3, oplusLongshotViewContent);
                if (isSideBarRect(this.mTempRect3, rect1)) {
                  bool = DBG;
                  StringBuilder stringBuilder1 = new StringBuilder();
                  stringBuilder1.append(str2);
                  stringBuilder1.append("  skipCoverRect : isSideBarView=");
                  stringBuilder1.append(oplusLongshotViewContent1);
                  OplusLog.d(bool, str3, stringBuilder1.toString());
                  if (paramList2 != null)
                    paramList2.add(oplusLongshotViewContent1); 
                } else {
                  bool = DBG;
                  StringBuilder stringBuilder1 = new StringBuilder();
                  stringBuilder1.append(str2);
                  stringBuilder1.append("  addCoverRect : ");
                  stringBuilder1.append(oplusLongshotViewContent1);
                  OplusLog.d(bool, str3, stringBuilder1.toString());
                  paramList1.add(oplusLongshotViewContent1);
                } 
              } 
            } 
          } 
        } 
      } else {
        break;
      } 
      i--;
    } 
    if (arrayList1 != null)
      arrayList1.clear(); 
  }
  
  public boolean isBottomBarRect(Rect paramRect1, Rect paramRect2) {
    if (paramRect1.width() != paramRect2.width())
      return false; 
    if (paramRect1.bottom != paramRect2.bottom)
      return false; 
    return true;
  }
  
  public boolean isLargeCoverRect(Rect paramRect1, Rect paramRect2, boolean paramBoolean) {
    if (!paramBoolean) {
      if (paramRect1.contains(paramRect2))
        return true; 
      Rect rect = new Rect();
      if (rect.setIntersect(paramRect1, paramRect2)) {
        if (rect.width() < getMinSize(paramRect2.width()))
          return false; 
        if (rect.height() < getMinSize(paramRect2.height()))
          return false; 
        return true;
      } 
    } 
    return false;
  }
  
  private int getMinSize(int paramInt) {
    return paramInt * 3 / 4;
  }
  
  private String getPrefix(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramInt; b++)
      stringBuilder.append("    "); 
    return stringBuilder.toString();
  }
  
  private boolean isSmallCoverRect(Rect paramRect1, Rect paramRect2) {
    if (paramRect1.width() > 1)
      return false; 
    if (paramRect1.height() > 1)
      return false; 
    return true;
  }
  
  private boolean isCenterCoverRect(Rect paramRect1, Rect paramRect2) {
    Rect rect = new Rect();
    rect.set(paramRect2);
    int i = paramRect2.width() / 3;
    int j = paramRect2.height() / 3;
    rect.inset(i, j);
    return rect.contains(paramRect1);
  }
  
  private boolean isTransparentDrawable(Drawable paramDrawable) {
    boolean bool = true;
    if (paramDrawable != null) {
      if (-2 != paramDrawable.getOpacity())
        bool = false; 
      return bool;
    } 
    return true;
  }
  
  private boolean isTransparentGroup(View paramView) {
    if (paramView instanceof android.widget.GridView)
      return false; 
    if (paramView instanceof ViewGroup)
      return isTransparentDrawable(paramView.getBackground()); 
    return false;
  }
  
  private void initCenterRect(Rect paramRect1, Rect paramRect2) {
    paramRect1.set(paramRect2);
    int i = paramRect2.width() / 4;
    int j = paramRect2.height() / 4;
    paramRect1.inset(i, j);
  }
  
  private void printNoContentLog(String paramString1, String paramString2, String paramString3, Rect paramRect, View paramView) {
    if (paramString1 != null && paramString2 != null) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("  ");
      stringBuilder.append(paramString2);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString3);
      stringBuilder.append("=");
      stringBuilder.append(paramRect);
      stringBuilder.append(":");
      stringBuilder.append(paramView);
      OplusLog.d(bool, "LongshotDump", stringBuilder.toString());
    } 
  }
  
  private boolean hasVisibleContent(String paramString1, View paramView, Rect paramRect, boolean paramBoolean, String paramString2) {
    ImageView imageView;
    paramView.getBoundsOnScreen(this.mTempRect1, true);
    if (isCenterCoverRect(this.mTempRect1, paramRect)) {
      printNoContentLog(paramString1, paramString2, "CenterCover", this.mTempRect1, paramView);
      return false;
    } 
    if (!isTransparentDrawable(paramView.getBackground()))
      return true; 
    if (paramView instanceof TextView) {
      TextView textView = (TextView)paramView;
      for (Drawable drawable : textView.getCompoundDrawables()) {
        if (!isTransparentDrawable(drawable))
          return true; 
      } 
      if (!TextUtils.isEmpty(textView.getHint()))
        return true; 
      if (!TextUtils.isEmpty(textView.getText()))
        return true; 
      printNoContentLog(paramString1, paramString2, "TextView", this.mTempRect1, paramView);
      return false;
    } 
    if (paramView instanceof ImageView) {
      imageView = (ImageView)paramView;
      if (!isTransparentDrawable(imageView.getDrawable()))
        return true; 
      printNoContentLog(paramString1, paramString2, "ImageView", this.mTempRect1, paramView);
      return false;
    } 
    if (isLargeCoverRect(this.mTempRect1, (Rect)imageView, paramBoolean)) {
      printNoContentLog(paramString1, paramString2, "LargeCover", this.mTempRect1, paramView);
      return false;
    } 
    if (isSmallCoverRect(this.mTempRect1, (Rect)imageView)) {
      printNoContentLog(paramString1, paramString2, "SmallCover", this.mTempRect1, paramView);
      return false;
    } 
    return true;
  }
  
  private boolean isNeighboringRect(Rect paramRect1, Rect paramRect2) {
    boolean bool;
    if (paramRect1.top == paramRect2.bottom) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isSameLineRect(Rect paramRect1, Rect paramRect2) {
    boolean bool;
    if (paramRect1.top == paramRect2.top && paramRect1.bottom == paramRect2.bottom) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isSideBarRect(Rect paramRect1, Rect paramRect2) {
    if (paramRect1.width() > paramRect2.width() / 3)
      return false; 
    if (paramRect1.height() < paramRect2.height() * 2 / 5)
      return false; 
    return true;
  }
  
  private boolean findSideBarContent(View paramView, Rect paramRect1, Rect paramRect2) {
    boolean bool = true;
    if (paramRect2 != null) {
      paramView.getBoundsOnScreen(this.mTempRect1, true);
      if (paramRect2.isEmpty())
        paramRect2.set(this.mTempRect1); 
    } 
    if (isTransparentGroup(paramView)) {
      boolean bool2, bool1 = false;
      ViewGroup viewGroup = (ViewGroup)paramView;
      int i = viewGroup.getChildCount();
      ArrayList<View> arrayList = viewGroup.buildOrderedChildList();
      if (arrayList == null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (!bool2 || !viewGroup.isChildrenDrawingOrderEnabled())
        bool = false; 
      for (int j = i - 1; j >= 0; j--) {
        int k;
        if (bool) {
          k = viewGroup.getChildDrawingOrder(i, j);
        } else {
          k = j;
        } 
        if (bool2) {
          paramView = viewGroup.getChildAt(k);
        } else {
          paramView = arrayList.get(k);
        } 
        if (paramView != null)
          if (paramView.isVisibleToUser())
            if (findSideBarContent(paramView, paramRect1, null)) {
              bool1 = true;
              break;
            }   
      } 
      if (arrayList != null)
        arrayList.clear(); 
      return bool1;
    } 
    if (hasVisibleContent(null, paramView, paramRect1, false, null))
      return true; 
    return false;
  }
  
  private boolean isWaterMarkGroup(Rect paramRect, ViewGroup paramViewGroup) {
    this.mTempRect2.setEmpty();
    boolean bool1 = true;
    int i = paramViewGroup.getChildCount();
    boolean bool2 = true;
    int j = i - 1;
    while (true) {
      i = bool1;
      if (j >= 0) {
        View view = paramViewGroup.getChildAt(j);
        if (view instanceof TextView) {
          view.getBoundsOnScreen(this.mTempRect1, true);
          this.mTempRect2.union(this.mTempRect1);
          j--;
        } 
        i = 0;
      } 
      break;
    } 
    initCenterRect(this.mTempRect1, paramRect);
    if (i == 0 || !Rect.intersects(this.mTempRect2, this.mTempRect1))
      bool2 = false; 
    return bool2;
  }
  
  private boolean isLargeWidth(View paramView, Rect paramRect) {
    boolean bool;
    if (paramView.getWidth() > paramRect.width() * 2 / 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isSideBarGroup(Rect paramRect, ViewGroup paramViewGroup, List<OplusLongshotViewContent> paramList) {
    boolean bool;
    int i = 0;
    if (paramList == null)
      return false; 
    if (isLargeWidth(paramViewGroup, paramRect))
      return false; 
    Rect rect2 = new Rect();
    ArrayList<Rect> arrayList = new ArrayList();
    int j = paramViewGroup.getChildCount();
    ArrayList<View> arrayList1 = paramViewGroup.buildOrderedChildList();
    if (arrayList1 == null) {
      bool = true;
    } else {
      bool = false;
    } 
    int k = i;
    if (bool) {
      k = i;
      if (paramViewGroup.isChildrenDrawingOrderEnabled())
        k = 1; 
    } 
    for (i = j - 1; i >= 0; i--) {
      View view;
      int m;
      if (k != 0) {
        m = paramViewGroup.getChildDrawingOrder(j, i);
      } else {
        m = i;
      } 
      if (bool) {
        view = paramViewGroup.getChildAt(m);
      } else {
        view = arrayList1.get(m);
      } 
      if (view != null)
        if (view.isVisibleToUser())
          if (!isLargeWidth(view, paramRect))
            if (findSideBarContent(view, paramRect, rect2)) {
              view.getBoundsOnScreen(this.mTempRect1, true);
              arrayList.add(new Rect(this.mTempRect1));
            }    
    } 
    if (arrayList1 != null)
      arrayList1.clear(); 
    Collections.sort(arrayList, RECT_COMPARATOR);
    this.mTempRect1.setEmpty();
    Rect rect1 = new Rect();
    for (Rect rect : arrayList) {
      if (rect1.isEmpty() || isNeighboringRect(rect1, rect))
        this.mTempRect1.union(rect); 
      rect1.set(rect);
    } 
    return isSideBarRect(this.mTempRect1, paramRect);
  }
  
  public OplusLongshotViewUtils(Context paramContext) {}
  
  private static class RectComparator implements Comparator<Rect> {
    private RectComparator() {}
    
    public int compare(Rect param1Rect1, Rect param1Rect2) {
      int i = param1Rect2.top - param1Rect1.top;
      int j = i;
      if (i != 0)
        j = param1Rect2.left - param1Rect1.left; 
      return j;
    }
  }
}
