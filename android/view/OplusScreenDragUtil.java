package android.view;

import android.graphics.Rect;
import android.os.SystemProperties;

public class OplusScreenDragUtil {
  public static int DRAG_STATE_HOLD = 0;
  
  public static int DRAG_STATE_NORMAL = 0;
  
  public static int DRAG_STATE_OFFSET = 0;
  
  private static final int FINISH_HANDLED = 1;
  
  private static final int FORWARD = 0;
  
  private static final String PERSIST_KEY = "persist.oppo.drag.screendrag";
  
  private static final String PERSIST_KEY_METRICS = "persist.oppo.drag.displaymetrics";
  
  private static final String PERSIST_KEY_STATE = "persist.oppo.drag.dragstate";
  
  private static String SPLIT_PROP;
  
  static {
    DRAG_STATE_HOLD = 1;
    DRAG_STATE_OFFSET = 2;
    SPLIT_PROP = ",";
  }
  
  public static void setScreenDragState(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(String.valueOf(paramInt));
    stringBuilder.append(",0,0,0");
    SystemProperties.set("persist.oppo.drag.screendrag", stringBuilder.toString());
  }
  
  public static int getScreenDragState() {
    String str = SystemProperties.get("persist.oppo.drag.screendrag", "0,0,0,0");
    String[] arrayOfString = str.split(SPLIT_PROP);
    return Integer.parseInt(arrayOfString[0]);
  }
  
  public static int getOffsetX() {
    String str = SystemProperties.get("persist.oppo.drag.screendrag", "0,0,0,0");
    String[] arrayOfString = str.split(SPLIT_PROP);
    return Integer.parseInt(arrayOfString[1]);
  }
  
  public static int getOffsetY() {
    String str = SystemProperties.get("persist.oppo.drag.screendrag", "0,0,0,0");
    String[] arrayOfString = str.split(SPLIT_PROP);
    return Integer.parseInt(arrayOfString[2]);
  }
  
  public static float getScale() {
    String str = SystemProperties.get("persist.oppo.drag.screendrag", "0,0,0,0");
    String[] arrayOfString = str.split(SPLIT_PROP);
    return Float.parseFloat(arrayOfString[3]);
  }
  
  public static int getWidth() {
    String str = SystemProperties.get("persist.oppo.drag.displaymetrics", "0,0");
    String[] arrayOfString = str.split(SPLIT_PROP);
    return Integer.parseInt(arrayOfString[0]);
  }
  
  public static int getHeight() {
    String str = SystemProperties.get("persist.oppo.drag.displaymetrics", "0,0");
    String[] arrayOfString = str.split(SPLIT_PROP);
    return Integer.parseInt(arrayOfString[1]);
  }
  
  public static boolean isNormalState() {
    boolean bool;
    if (DRAG_STATE_NORMAL == getScreenDragState()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isDragState() {
    int i = getScreenDragState();
    return (DRAG_STATE_HOLD == i || DRAG_STATE_OFFSET == i);
  }
  
  public static boolean isHoldState() {
    boolean bool;
    if (DRAG_STATE_HOLD == getScreenDragState()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isOffsetState() {
    boolean bool;
    if (DRAG_STATE_OFFSET == getScreenDragState()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void resetState() {
    setScreenDragState(DRAG_STATE_NORMAL);
    SystemProperties.set("persist.oppo.drag.dragstate", "0");
    SystemProperties.set("persist.oppo.drag.displaymetrics", "0,0");
  }
  
  public static float getOffsetPosX(float paramFloat) {
    return (paramFloat - getOffsetX()) / getScale();
  }
  
  public static float getOffsetPosY(float paramFloat) {
    return (paramFloat - getOffsetY()) / getScale();
  }
  
  public static float getOffsetPosXScale(float paramFloat1, float paramFloat2) {
    if (getOffsetX() == 0) {
      paramFloat1 /= getScale();
    } else {
      paramFloat1 = (paramFloat1 - getWidth() / paramFloat2 * (1.0F - getScale())) / getScale();
    } 
    return paramFloat1;
  }
  
  public static float getOffsetPosYScale(float paramFloat1, float paramFloat2) {
    paramFloat1 = (paramFloat1 - getHeight() / paramFloat2 * (1.0F - getScale())) / getScale();
    return paramFloat1;
  }
  
  public static void setEventLocationScale(MotionEvent paramMotionEvent, float paramFloat) {
    float f1 = getOffsetPosXScale(paramMotionEvent.getX() - getXOffset(paramMotionEvent), paramFloat), f2 = getXOffset(paramMotionEvent);
    paramFloat = getOffsetPosYScale(paramMotionEvent.getY() - getYOffset(paramMotionEvent), paramFloat);
    float f3 = getYOffset(paramMotionEvent);
    paramMotionEvent.setLocation(f1 + f2, paramFloat + f3);
  }
  
  public static void setEventLocation(MotionEvent paramMotionEvent) {
    float f1 = getOffsetPosX(paramMotionEvent.getX() - getXOffset(paramMotionEvent)), f2 = getXOffset(paramMotionEvent);
    float f3 = getOffsetPosY(paramMotionEvent.getY() - getYOffset(paramMotionEvent)), f4 = getYOffset(paramMotionEvent);
    paramMotionEvent.setLocation(f1 + f2, f3 + f4);
  }
  
  public static int screenOffsetDeliverPointer(MotionEvent paramMotionEvent, View paramView) {
    float f1 = getOffsetPosX(paramMotionEvent.getX() - getXOffset(paramMotionEvent)), f2 = getXOffset(paramMotionEvent);
    float f3 = getOffsetPosY(paramMotionEvent.getY() - getYOffset(paramMotionEvent)), f4 = getYOffset(paramMotionEvent);
    paramMotionEvent.setLocation(f1 + f2, f3 + f4);
    if (paramView.dispatchPointerEvent(paramMotionEvent))
      return 1; 
    return 0;
  }
  
  public static void scaleScreenshotIfNeeded(Rect paramRect) {
    if (isOffsetState()) {
      float f = getScale();
      int i = getWidth();
      int j = getHeight();
      int k = paramRect.width();
      int m = paramRect.height();
      if (getOffsetX() > 0) {
        paramRect.left = (int)((1.0F - f) * i + paramRect.left * f);
        paramRect.top = (int)((1.0F - f) * j + paramRect.top * f);
        paramRect.right = paramRect.left + (int)(k * f);
        paramRect.bottom = paramRect.top + (int)(m * f);
      } else {
        paramRect.left = (int)(paramRect.left * f);
        paramRect.top = (int)((1.0F - f) * j + paramRect.top * f);
        paramRect.right = paramRect.left + (int)(k * f);
        paramRect.bottom = paramRect.top + (int)(m * f);
      } 
    } 
  }
  
  private static float getXOffset(MotionEvent paramMotionEvent) {
    long l = OplusMirrirMotionEvent.mNativePtr.get(paramMotionEvent);
    return ((Float)OplusMirrirMotionEvent.nativeGetXOffset.call(new Object[] { Long.valueOf(l) })).floatValue();
  }
  
  private static float getYOffset(MotionEvent paramMotionEvent) {
    return ((Float)OplusMirrirMotionEvent.nativeGetYOffset.call(new Object[] { Long.valueOf(OplusMirrirMotionEvent.mNativePtr.get(paramMotionEvent)) })).floatValue();
  }
}
