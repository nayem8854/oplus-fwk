package android.view;

import android.os.Parcel;
import android.os.Parcelable;

public class RemoteAnimationAdapter implements Parcelable {
  public RemoteAnimationAdapter(IRemoteAnimationRunner paramIRemoteAnimationRunner, long paramLong1, long paramLong2, boolean paramBoolean) {
    this.mRunner = paramIRemoteAnimationRunner;
    this.mDuration = paramLong1;
    this.mChangeNeedsSnapshot = paramBoolean;
    this.mStatusBarTransitionDelay = paramLong2;
  }
  
  public RemoteAnimationAdapter(IRemoteAnimationRunner paramIRemoteAnimationRunner, long paramLong1, long paramLong2) {
    this(paramIRemoteAnimationRunner, paramLong1, paramLong2, false);
  }
  
  public RemoteAnimationAdapter(Parcel paramParcel) {
    this.mRunner = IRemoteAnimationRunner.Stub.asInterface(paramParcel.readStrongBinder());
    this.mDuration = paramParcel.readLong();
    this.mStatusBarTransitionDelay = paramParcel.readLong();
    this.mChangeNeedsSnapshot = paramParcel.readBoolean();
  }
  
  public IRemoteAnimationRunner getRunner() {
    return this.mRunner;
  }
  
  public long getDuration() {
    return this.mDuration;
  }
  
  public long getStatusBarTransitionDelay() {
    return this.mStatusBarTransitionDelay;
  }
  
  public boolean getChangeNeedsSnapshot() {
    return this.mChangeNeedsSnapshot;
  }
  
  public void setCallingPidUid(int paramInt1, int paramInt2) {
    this.mCallingPid = paramInt1;
    this.mCallingUid = paramInt2;
  }
  
  public int getCallingPid() {
    return this.mCallingPid;
  }
  
  public int getCallingUid() {
    return this.mCallingUid;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongInterface(this.mRunner);
    paramParcel.writeLong(this.mDuration);
    paramParcel.writeLong(this.mStatusBarTransitionDelay);
    paramParcel.writeBoolean(this.mChangeNeedsSnapshot);
  }
  
  public static final Parcelable.Creator<RemoteAnimationAdapter> CREATOR = new Parcelable.Creator<RemoteAnimationAdapter>() {
      public RemoteAnimationAdapter createFromParcel(Parcel param1Parcel) {
        return new RemoteAnimationAdapter(param1Parcel);
      }
      
      public RemoteAnimationAdapter[] newArray(int param1Int) {
        return new RemoteAnimationAdapter[param1Int];
      }
    };
  
  private int mCallingPid;
  
  private int mCallingUid;
  
  private final boolean mChangeNeedsSnapshot;
  
  private final long mDuration;
  
  private final IRemoteAnimationRunner mRunner;
  
  private final long mStatusBarTransitionDelay;
}
