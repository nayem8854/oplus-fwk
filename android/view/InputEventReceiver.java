package android.view;

import android.os.IBinder;
import android.os.Looper;
import android.os.MessageQueue;
import android.util.Log;
import android.util.SparseIntArray;
import dalvik.system.CloseGuard;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

public abstract class InputEventReceiver {
  private static final String TAG = "InputEventReceiver";
  
  Choreographer mChoreographer;
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private InputChannel mInputChannel;
  
  private MessageQueue mMessageQueue;
  
  private long mReceiverPtr;
  
  private final SparseIntArray mSeqMap = new SparseIntArray();
  
  public InputEventReceiver(InputChannel paramInputChannel, Looper paramLooper) {
    if (paramInputChannel != null) {
      if (paramLooper != null) {
        this.mInputChannel = paramInputChannel;
        this.mMessageQueue = paramLooper.getQueue();
        this.mReceiverPtr = nativeInit(new WeakReference<>(this), paramInputChannel, this.mMessageQueue);
        this.mCloseGuard.open("dispose");
        return;
      } 
      throw new IllegalArgumentException("looper must not be null");
    } 
    throw new IllegalArgumentException("inputChannel must not be null");
  }
  
  protected void finalize() throws Throwable {
    try {
      dispose(true);
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void dispose() {
    dispose(false);
  }
  
  private void dispose(boolean paramBoolean) {
    CloseGuard closeGuard = this.mCloseGuard;
    if (closeGuard != null) {
      if (paramBoolean)
        closeGuard.warnIfOpen(); 
      this.mCloseGuard.close();
    } 
    long l = this.mReceiverPtr;
    if (l != 0L) {
      nativeDispose(l);
      this.mReceiverPtr = 0L;
    } 
    InputChannel inputChannel = this.mInputChannel;
    if (inputChannel != null) {
      inputChannel.dispose();
      this.mInputChannel = null;
    } 
    this.mMessageQueue = null;
    Reference.reachabilityFence(this);
  }
  
  public void onInputEvent(InputEvent paramInputEvent) {
    finishInputEvent(paramInputEvent, false);
  }
  
  public void onFocusEvent(boolean paramBoolean1, boolean paramBoolean2) {}
  
  public void onBatchedInputEventPending(int paramInt) {
    consumeBatchedInputEvents(-1L);
  }
  
  public final void finishInputEvent(InputEvent paramInputEvent, boolean paramBoolean) {
    if (paramInputEvent != null) {
      if (this.mReceiverPtr == 0L) {
        Log.w("InputEventReceiver", "Attempted to finish an input event but the input event receiver has already been disposed.");
      } else {
        int i = this.mSeqMap.indexOfKey(paramInputEvent.getSequenceNumber());
        if (i < 0) {
          Log.w("InputEventReceiver", "Attempted to finish an input event that is not in progress.");
        } else {
          int j = this.mSeqMap.valueAt(i);
          this.mSeqMap.removeAt(i);
          nativeFinishInputEvent(this.mReceiverPtr, j, paramBoolean);
        } 
      } 
      paramInputEvent.recycleIfNeededAfterDispatch();
      return;
    } 
    throw new IllegalArgumentException("event must not be null");
  }
  
  public final boolean consumeBatchedInputEvents(long paramLong) {
    long l = this.mReceiverPtr;
    if (l == 0L) {
      Log.w("InputEventReceiver", "Attempted to consume batched input events but the input event receiver has already been disposed.");
      return false;
    } 
    return nativeConsumeBatchedInputEvents(l, paramLong);
  }
  
  public IBinder getToken() {
    InputChannel inputChannel = this.mInputChannel;
    if (inputChannel == null)
      return null; 
    return inputChannel.getToken();
  }
  
  private void dispatchInputEvent(int paramInt, InputEvent paramInputEvent) {
    this.mSeqMap.put(paramInputEvent.getSequenceNumber(), paramInt);
    onInputEvent(paramInputEvent);
  }
  
  private void dispatchMotionEventInfo(int paramInt1, int paramInt2) {
    try {
      if (this.mChoreographer == null)
        this.mChoreographer = Choreographer.getInstance(); 
      if (this.mChoreographer != null)
        this.mChoreographer.setMotionEventInfo(paramInt1, paramInt2); 
    } catch (Exception exception) {
      Log.e("InputEventReceiver", "cannot invoke setMotionEventInfo.");
    } 
  }
  
  private static native boolean nativeConsumeBatchedInputEvents(long paramLong1, long paramLong2);
  
  private static native void nativeDispose(long paramLong);
  
  private static native void nativeFinishInputEvent(long paramLong, int paramInt, boolean paramBoolean);
  
  private static native long nativeInit(WeakReference<InputEventReceiver> paramWeakReference, InputChannel paramInputChannel, MessageQueue paramMessageQueue);
  
  public static interface Factory {
    InputEventReceiver createInputEventReceiver(InputChannel param1InputChannel, Looper param1Looper);
  }
}
