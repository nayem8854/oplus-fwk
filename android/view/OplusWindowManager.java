package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import com.oplus.darkmode.IOplusDarkModeListener;
import com.oplus.direct.OplusDirectFindCmd;
import com.oplus.screenshot.IOplusScreenShotEuclidManager;
import java.util.List;

public class OplusWindowManager extends OplusBaseWindowManager implements IOplusWindowManager, IOplusLongshotWindowManager, IOplusDirectWindowManager {
  private final IOplusLongshotWindowManager mColorLongshot = ((IOplusScreenShotEuclidManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusScreenShotEuclidManager.DEFAULT, new Object[0])).getIOplusLongshotWindowManager();
  
  private final OplusDirectWindowManager mColorDirect = new OplusDirectWindowManager();
  
  private static final String TAG = "OplusWindowManager";
  
  private static final String SCALE = "scale";
  
  private static final String OFFERTY = "offertY";
  
  private static final String OFFERTX = "offertX";
  
  public boolean isLockWndShow() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void keyguardSetApkLockScreenShowing(boolean paramBoolean) throws RemoteException {
    null = new StringBuilder();
    null.append("keyguardSetApkLockScreenShowing showing = ");
    null.append(paramBoolean);
    Log.d("OplusWindowManager", null.toString());
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      boolean bool;
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      parcel1.writeInt(bool);
      this.mRemote.transact(10003, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public IBinder getApkUnlockWindow() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10004, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readStrongBinder();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void keyguardShowSecureApkLock(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      boolean bool;
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      parcel1.writeInt(bool);
      this.mRemote.transact(10005, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isLockOnShow() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10006, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isSIMUnlockRunning() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10007, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isInputShow() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10010, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isFullScreen() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10011, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isStatusBarVisible() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10012, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isRotatingLw() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10013, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean checkIsFloatWindowForbidden(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10014, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void setMagnification(Bundle paramBundle) throws RemoteException {
    MagnificationSpec magnificationSpec = MagnificationSpec.obtain();
    magnificationSpec.scale = paramBundle.getFloat("scale", 1.0F);
    magnificationSpec.offsetX = paramBundle.getFloat("offertX", 0.0F);
    magnificationSpec.offsetY = paramBundle.getFloat("offertY", 0.0F);
    try {
      setMagnificationSpecEx(magnificationSpec);
    } catch (RemoteException remoteException) {}
  }
  
  public void setMagnificationSpecEx(MagnificationSpec paramMagnificationSpec) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramMagnificationSpec != null) {
        parcel1.writeInt(1);
        paramMagnificationSpec.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10015, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void requestDismissKeyguard() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10017, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void requestKeyguard(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10019, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isWindowShownForUid(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10018, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void removeWindowShownOnKeyguard() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10022, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public String getCurrentFocus() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10025, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readString();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public Rect getFloatWindowRect(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      Rect rect;
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10030, parcel1, parcel2, 0);
      parcel2.readException();
      if (parcel2.readInt() != 0) {
        rect = (Rect)Rect.CREATOR.createFromParcel(parcel2);
      } else {
        rect = null;
      } 
      return rect;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void setSplitTimeout(int paramInt) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindowManager");
      parcel.writeInt(paramInt);
      this.mRemote.transact(10036, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void setGestureFollowAnimation(boolean paramBoolean) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      boolean bool;
      parcel.writeInterfaceToken("android.view.IWindowManager");
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      parcel.writeInt(bool);
      this.mRemote.transact(10042, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void startOplusDragWindow(String paramString, int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      if (paramBundle != null) {
        parcel1.writeInt(1);
        paramBundle.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10046, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void registerOplusWindowStateObserver(IOplusWindowStateObserver paramIOplusWindowStateObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramIOplusWindowStateObserver != null) {
        IBinder iBinder = paramIOplusWindowStateObserver.asBinder();
      } else {
        paramIOplusWindowStateObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusWindowStateObserver);
      this.mRemote.transact(10047, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void unregisterOplusWindowStateObserver(IOplusWindowStateObserver paramIOplusWindowStateObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramIOplusWindowStateObserver != null) {
        IBinder iBinder = paramIOplusWindowStateObserver.asBinder();
      } else {
        paramIOplusWindowStateObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusWindowStateObserver);
      this.mRemote.transact(10048, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isInFreeformMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10049, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void getFreeformStackBounds(Rect paramRect) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10050, parcel1, parcel2, 0);
      parcel2.readException();
      if (parcel2.readInt() != 0)
        paramRect.readFromParcel(parcel2); 
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isActivityNeedPalette(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10051, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getNavBarOplusFromAdaptation(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10052, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getStatusBarOplusFromAdaptation(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10053, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getImeBgOplusFromAdaptation(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10054, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getTypedWindowLayer(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10055, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getFocusedWindowIgnoreHomeMenuKey() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10056, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void registerOnUiModeConfigurationChangeFinishListener(IOplusDarkModeListener paramIOplusDarkModeListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramIOplusDarkModeListener != null) {
        IBinder iBinder = paramIOplusDarkModeListener.asBinder();
      } else {
        paramIOplusDarkModeListener = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusDarkModeListener);
      this.mRemote.transact(10057, parcel1, parcel2, 1);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void unregisterOnUiModeConfigurationChangeFinishListener(IOplusDarkModeListener paramIOplusDarkModeListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramIOplusDarkModeListener != null) {
        IBinder iBinder = paramIOplusDarkModeListener.asBinder();
      } else {
        paramIOplusDarkModeListener = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusDarkModeListener);
      this.mRemote.transact(10058, parcel1, parcel2, 1);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void setBootAnimationRotationLock(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      boolean bool;
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      parcel1.writeInt(bool);
      this.mRemote.transact(10059, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean updateInvalidRegion(String paramString, List<RectF> paramList, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeString(paramString);
      parcel1.writeList(paramList);
      parcel1.writeString(String.valueOf(paramBoolean1));
      parcel1.writeString(String.valueOf(paramBoolean2));
      parcel1.writeBundle(paramBundle);
      this.mRemote.transact(10060, parcel1, parcel2, 0);
      parcel2.readException();
      paramBoolean1 = Boolean.valueOf(parcel2.readString()).booleanValue();
      return paramBoolean1;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean setJoyStickConfig(int paramInt, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      this.mRemote.transact(10061, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean setJoyStickStatus(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10062, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean setJoyStickSwitch(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10063, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void getFocusedWindowFrame(Rect paramRect) throws RemoteException {
    this.mColorLongshot.getFocusedWindowFrame(paramRect);
  }
  
  public int getLongshotSurfaceLayer() throws RemoteException {
    return this.mColorLongshot.getLongshotSurfaceLayer();
  }
  
  public int getLongshotSurfaceLayerByType(int paramInt) throws RemoteException {
    return this.mColorLongshot.getLongshotSurfaceLayerByType(paramInt);
  }
  
  public void longshotNotifyConnected(boolean paramBoolean) throws RemoteException {
    this.mColorLongshot.longshotNotifyConnected(paramBoolean);
  }
  
  public boolean isNavigationBarVisible() throws RemoteException {
    return this.mColorLongshot.isNavigationBarVisible();
  }
  
  public boolean isShortcutsPanelShow() throws RemoteException {
    return this.mColorLongshot.isShortcutsPanelShow();
  }
  
  public void longshotInjectInput(InputEvent paramInputEvent, int paramInt) throws RemoteException {
    this.mColorLongshot.longshotInjectInput(paramInputEvent, paramInt);
  }
  
  public boolean isKeyguardShowingAndNotOccluded() throws RemoteException {
    return this.mColorLongshot.isKeyguardShowingAndNotOccluded();
  }
  
  public void longshotInjectInputBegin() throws RemoteException {
    this.mColorLongshot.longshotInjectInputBegin();
  }
  
  public void longshotInjectInputEnd() throws RemoteException {
    this.mColorLongshot.longshotInjectInputEnd();
  }
  
  public IBinder getLongshotWindowByType(int paramInt) throws RemoteException {
    return this.mColorLongshot.getLongshotWindowByType(paramInt);
  }
  
  public SurfaceControl getLongshotWindowByTypeForR(int paramInt) throws RemoteException {
    return this.mColorLongshot.getLongshotWindowByTypeForR(paramInt);
  }
  
  public boolean isVolumeShow() throws RemoteException {
    return this.mColorLongshot.isVolumeShow();
  }
  
  public boolean isFloatAssistExpand() throws RemoteException {
    return this.mColorLongshot.isFloatAssistExpand();
  }
  
  public boolean isEdgePanelExpand() throws RemoteException {
    return this.mColorLongshot.isEdgePanelExpand();
  }
  
  public void directFindCmd(OplusDirectFindCmd paramOplusDirectFindCmd) throws RemoteException {
    this.mColorDirect.directFindCmd(paramOplusDirectFindCmd);
  }
}
