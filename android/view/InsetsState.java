package android.view;

import android.graphics.Insets;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArraySet;
import android.util.SparseIntArray;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;

public class InsetsState implements Parcelable {
  public static final InsetsState EMPTY = new InsetsState();
  
  private InsetsSource[] mSources = new InsetsSource[16];
  
  private final Rect mDisplayFrame = new Rect();
  
  private boolean mIsFormInsetsAnimation = false;
  
  public static final Parcelable.Creator<InsetsState> CREATOR;
  
  static final int FIRST_TYPE = 0;
  
  static final int ISIDE_BOTTOM = 3;
  
  static final int ISIDE_FLOATING = 4;
  
  static final int ISIDE_LEFT = 0;
  
  static final int ISIDE_RIGHT = 2;
  
  static final int ISIDE_TOP = 1;
  
  static final int ISIDE_UNKNOWN = 5;
  
  public static final int ITYPE_BOTTOM_DISPLAY_CUTOUT = 12;
  
  public static final int ITYPE_BOTTOM_GESTURES = 4;
  
  public static final int ITYPE_BOTTOM_TAPPABLE_ELEMENT = 8;
  
  public static final int ITYPE_CAPTION_BAR = 2;
  
  public static final int ITYPE_CLIMATE_BAR = 14;
  
  public static final int ITYPE_EXTRA_NAVIGATION_BAR = 15;
  
  public static final int ITYPE_IME = 13;
  
  public static final int ITYPE_INVALID = -1;
  
  public static final int ITYPE_LEFT_DISPLAY_CUTOUT = 9;
  
  public static final int ITYPE_LEFT_GESTURES = 5;
  
  public static final int ITYPE_NAVIGATION_BAR = 1;
  
  public static final int ITYPE_RIGHT_DISPLAY_CUTOUT = 11;
  
  public static final int ITYPE_RIGHT_GESTURES = 6;
  
  public static final int ITYPE_SHELF = 1;
  
  public static final int ITYPE_STATUS_BAR = 0;
  
  public static final int ITYPE_TOP_DISPLAY_CUTOUT = 10;
  
  public static final int ITYPE_TOP_GESTURES = 3;
  
  public static final int ITYPE_TOP_TAPPABLE_ELEMENT = 7;
  
  static final int LAST_TYPE = 15;
  
  public static final int SIZE = 16;
  
  public InsetsState(InsetsState paramInsetsState) {
    set(paramInsetsState);
  }
  
  public InsetsState(InsetsState paramInsetsState, boolean paramBoolean) {
    set(paramInsetsState, paramBoolean);
  }
  
  public InsetsState(InsetsState paramInsetsState, boolean paramBoolean1, boolean paramBoolean2) {
    this.mIsFormInsetsAnimation = paramBoolean2;
    set(paramInsetsState, paramBoolean1);
  }
  
  public WindowInsets calculateInsets(Rect paramRect, InsetsState paramInsetsState, boolean paramBoolean1, boolean paramBoolean2, DisplayCutout paramDisplayCutout, int paramInt1, int paramInt2, int paramInt3, SparseIntArray paramSparseIntArray) {
    // Byte code:
    //   0: bipush #9
    //   2: anewarray android/graphics/Insets
    //   5: astore #10
    //   7: bipush #9
    //   9: anewarray android/graphics/Insets
    //   12: astore #11
    //   14: bipush #16
    //   16: newarray boolean
    //   18: astore #12
    //   20: new android/graphics/Rect
    //   23: dup
    //   24: aload_1
    //   25: invokespecial <init> : (Landroid/graphics/Rect;)V
    //   28: astore #13
    //   30: new android/graphics/Rect
    //   33: dup
    //   34: aload_1
    //   35: invokespecial <init> : (Landroid/graphics/Rect;)V
    //   38: astore #14
    //   40: iconst_0
    //   41: istore #15
    //   43: iconst_0
    //   44: istore #16
    //   46: iload #15
    //   48: bipush #15
    //   50: if_icmpgt -> 279
    //   53: aload_0
    //   54: getfield mSources : [Landroid/view/InsetsSource;
    //   57: iload #15
    //   59: aaload
    //   60: astore_1
    //   61: aload_1
    //   62: ifnonnull -> 94
    //   65: iload #15
    //   67: invokestatic toPublicType : (I)I
    //   70: invokestatic indexOf : (I)I
    //   73: istore #17
    //   75: aload #10
    //   77: iload #17
    //   79: aaload
    //   80: ifnonnull -> 273
    //   83: aload #10
    //   85: iload #17
    //   87: getstatic android/graphics/Insets.NONE : Landroid/graphics/Insets;
    //   90: aastore
    //   91: goto -> 273
    //   94: getstatic android/view/ViewRootImpl.sNewInsetsMode : I
    //   97: iconst_1
    //   98: if_icmpne -> 116
    //   101: aload_1
    //   102: invokevirtual getType : ()I
    //   105: bipush #13
    //   107: if_icmpeq -> 116
    //   110: iconst_1
    //   111: istore #17
    //   113: goto -> 119
    //   116: iconst_0
    //   117: istore #17
    //   119: getstatic android/view/ViewRootImpl.sNewInsetsMode : I
    //   122: iconst_2
    //   123: if_icmpeq -> 143
    //   126: iload #15
    //   128: ifeq -> 137
    //   131: iload #15
    //   133: iconst_1
    //   134: if_icmpne -> 143
    //   137: iconst_1
    //   138: istore #18
    //   140: goto -> 146
    //   143: iconst_0
    //   144: istore #18
    //   146: iload #16
    //   148: istore #19
    //   150: getstatic android/view/ViewRootImpl.sNewInsetsMode : I
    //   153: ifne -> 181
    //   156: iload #15
    //   158: ifeq -> 178
    //   161: iload #15
    //   163: iconst_1
    //   164: if_icmpeq -> 178
    //   167: iload #16
    //   169: istore #19
    //   171: iload #15
    //   173: bipush #13
    //   175: if_icmpne -> 181
    //   178: iconst_1
    //   179: istore #19
    //   181: iload #18
    //   183: ifne -> 258
    //   186: iload #19
    //   188: ifne -> 258
    //   191: iload #17
    //   193: ifeq -> 199
    //   196: goto -> 258
    //   199: aload_0
    //   200: aload_1
    //   201: aload #13
    //   203: iconst_0
    //   204: aload #10
    //   206: aload #9
    //   208: aload #12
    //   210: invokespecial processSource : (Landroid/view/InsetsSource;Landroid/graphics/Rect;Z[Landroid/graphics/Insets;Landroid/util/SparseIntArray;[Z)V
    //   213: aload_1
    //   214: invokevirtual getType : ()I
    //   217: bipush #13
    //   219: if_icmpeq -> 273
    //   222: aload_2
    //   223: ifnull -> 236
    //   226: aload_2
    //   227: iload #15
    //   229: invokevirtual getSource : (I)Landroid/view/InsetsSource;
    //   232: astore_1
    //   233: goto -> 236
    //   236: aload_1
    //   237: ifnonnull -> 243
    //   240: goto -> 273
    //   243: aload_0
    //   244: aload_1
    //   245: aload #14
    //   247: iconst_1
    //   248: aload #11
    //   250: aconst_null
    //   251: aconst_null
    //   252: invokespecial processSource : (Landroid/view/InsetsSource;Landroid/graphics/Rect;Z[Landroid/graphics/Insets;Landroid/util/SparseIntArray;[Z)V
    //   255: goto -> 273
    //   258: aload #12
    //   260: iload #15
    //   262: invokestatic toPublicType : (I)I
    //   265: invokestatic indexOf : (I)I
    //   268: aload_1
    //   269: invokevirtual isVisible : ()Z
    //   272: bastore
    //   273: iinc #15, 1
    //   276: goto -> 43
    //   279: invokestatic systemBars : ()I
    //   282: invokestatic displayCutout : ()I
    //   285: ior
    //   286: istore #17
    //   288: iload #17
    //   290: istore #15
    //   292: iload #6
    //   294: sipush #240
    //   297: iand
    //   298: bipush #16
    //   300: if_icmpne -> 311
    //   303: iload #17
    //   305: invokestatic ime : ()I
    //   308: ior
    //   309: istore #15
    //   311: iload #7
    //   313: sipush #1024
    //   316: iand
    //   317: ifeq -> 333
    //   320: iload #15
    //   322: invokestatic statusBars : ()I
    //   325: iconst_m1
    //   326: ixor
    //   327: iand
    //   328: istore #15
    //   330: goto -> 333
    //   333: getstatic android/view/ViewRootImpl.sNewInsetsMode : I
    //   336: iconst_2
    //   337: if_icmpne -> 355
    //   340: iload #8
    //   342: sipush #256
    //   345: iand
    //   346: ifeq -> 355
    //   349: iconst_1
    //   350: istore #20
    //   352: goto -> 358
    //   355: iconst_0
    //   356: istore #20
    //   358: new android/view/WindowInsets
    //   361: dup
    //   362: aload #10
    //   364: aload #11
    //   366: aload #12
    //   368: iload_3
    //   369: iload #4
    //   371: aload #5
    //   373: iload #15
    //   375: iload #20
    //   377: invokespecial <init> : ([Landroid/graphics/Insets;[Landroid/graphics/Insets;[ZZZLandroid/view/DisplayCutout;IZ)V
    //   380: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #191	-> 0
    //   #192	-> 7
    //   #193	-> 14
    //   #194	-> 20
    //   #195	-> 30
    //   #196	-> 40
    //   #197	-> 53
    //   #198	-> 61
    //   #199	-> 65
    //   #200	-> 75
    //   #201	-> 83
    //   #206	-> 94
    //   #207	-> 101
    //   #208	-> 119
    //   #210	-> 146
    //   #213	-> 181
    //   #218	-> 199
    //   #223	-> 213
    //   #224	-> 222
    //   #225	-> 226
    //   #226	-> 236
    //   #227	-> 236
    //   #228	-> 240
    //   #230	-> 243
    //   #214	-> 258
    //   #215	-> 273
    //   #196	-> 273
    //   #235	-> 279
    //   #237	-> 279
    //   #238	-> 288
    //   #239	-> 303
    //   #241	-> 311
    //   #242	-> 320
    //   #241	-> 333
    //   #245	-> 333
  }
  
  public Rect calculateVisibleInsets(Rect paramRect, int paramInt) {
    Insets insets = Insets.NONE;
    for (byte b = 0; b <= 15; b++) {
      InsetsSource insetsSource = this.mSources[b];
      if (insetsSource != null)
        if (ViewRootImpl.sNewInsetsMode == 2 || b == 13) {
          int i = toPublicType(b);
          if (WindowInsets.Type.isVisibleInsetsType(i, paramInt))
            insets = Insets.max(insetsSource.calculateVisibleInsets(paramRect), insets); 
        }  
    } 
    return insets.toRect();
  }
  
  public int calculateUncontrollableInsetsFromFrame(Rect paramRect) {
    int i = 0;
    for (byte b = 0; b <= 15; b++, i = j) {
      int j;
      InsetsSource insetsSource = this.mSources[b];
      if (insetsSource == null) {
        j = i;
      } else {
        Insets insets = insetsSource.calculateInsets(paramRect, true);
        j = i;
        if (!canControlSide(paramRect, getInsetSide(insets)))
          j = i | toPublicType(b); 
      } 
    } 
    return i;
  }
  
  private boolean canControlSide(Rect paramRect, int paramInt) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: iconst_0
    //   3: istore #4
    //   5: iload_2
    //   6: ifeq -> 75
    //   9: iload_2
    //   10: iconst_1
    //   11: if_icmpeq -> 33
    //   14: iload_2
    //   15: iconst_2
    //   16: if_icmpeq -> 75
    //   19: iload_2
    //   20: iconst_3
    //   21: if_icmpeq -> 33
    //   24: iload_2
    //   25: iconst_4
    //   26: if_icmpeq -> 31
    //   29: iconst_0
    //   30: ireturn
    //   31: iconst_1
    //   32: ireturn
    //   33: iload #4
    //   35: istore #5
    //   37: aload_1
    //   38: getfield top : I
    //   41: aload_0
    //   42: getfield mDisplayFrame : Landroid/graphics/Rect;
    //   45: getfield top : I
    //   48: if_icmpne -> 72
    //   51: iload #4
    //   53: istore #5
    //   55: aload_1
    //   56: getfield bottom : I
    //   59: aload_0
    //   60: getfield mDisplayFrame : Landroid/graphics/Rect;
    //   63: getfield bottom : I
    //   66: if_icmpne -> 72
    //   69: iconst_1
    //   70: istore #5
    //   72: iload #5
    //   74: ireturn
    //   75: iload_3
    //   76: istore #5
    //   78: aload_1
    //   79: getfield left : I
    //   82: aload_0
    //   83: getfield mDisplayFrame : Landroid/graphics/Rect;
    //   86: getfield left : I
    //   89: if_icmpne -> 112
    //   92: iload_3
    //   93: istore #5
    //   95: aload_1
    //   96: getfield right : I
    //   99: aload_0
    //   100: getfield mDisplayFrame : Landroid/graphics/Rect;
    //   103: getfield right : I
    //   106: if_icmpne -> 112
    //   109: iconst_1
    //   110: istore #5
    //   112: iload #5
    //   114: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #296	-> 0
    //   #306	-> 29
    //   #304	-> 31
    //   #302	-> 33
    //   #299	-> 75
  }
  
  private void processSource(InsetsSource paramInsetsSource, Rect paramRect, boolean paramBoolean, Insets[] paramArrayOfInsets, SparseIntArray paramSparseIntArray, boolean[] paramArrayOfboolean) {
    Insets insets = paramInsetsSource.calculateInsets(paramRect, paramBoolean);
    int i = toPublicType(paramInsetsSource.getType());
    processSourceAsPublicType(paramInsetsSource, paramArrayOfInsets, paramSparseIntArray, paramArrayOfboolean, insets, i);
    if (i == 32)
      processSourceAsPublicType(paramInsetsSource, paramArrayOfInsets, paramSparseIntArray, paramArrayOfboolean, insets, 16); 
  }
  
  private void processSourceAsPublicType(InsetsSource paramInsetsSource, Insets[] paramArrayOfInsets, SparseIntArray paramSparseIntArray, boolean[] paramArrayOfboolean, Insets paramInsets, int paramInt) {
    paramInt = WindowInsets.Type.indexOf(paramInt);
    Insets insets = paramArrayOfInsets[paramInt];
    if (insets == null) {
      paramArrayOfInsets[paramInt] = paramInsets;
    } else {
      paramArrayOfInsets[paramInt] = Insets.max(insets, paramInsets);
    } 
    if (paramArrayOfboolean != null)
      paramArrayOfboolean[paramInt] = paramInsetsSource.isVisible(); 
    if (paramSparseIntArray != null) {
      paramInt = getInsetSide(paramInsets);
      if (paramInt != 5)
        paramSparseIntArray.put(paramInsetsSource.getType(), paramInt); 
    } 
  }
  
  private int getInsetSide(Insets paramInsets) {
    if (Insets.NONE.equals(paramInsets))
      return 4; 
    if (paramInsets.left != 0)
      return 0; 
    if (paramInsets.top != 0)
      return 1; 
    if (paramInsets.right != 0)
      return 2; 
    if (paramInsets.bottom != 0)
      return 3; 
    return 5;
  }
  
  public InsetsSource getSource(int paramInt) {
    InsetsSource insetsSource = this.mSources[paramInt];
    if (insetsSource != null)
      return insetsSource; 
    insetsSource = new InsetsSource(paramInt);
    this.mSources[paramInt] = insetsSource;
    return insetsSource;
  }
  
  public InsetsSource peekSource(int paramInt) {
    return this.mSources[paramInt];
  }
  
  public boolean hasSources() {
    for (byte b = 0; b < 16; b++) {
      if (this.mSources[b] != null)
        return true; 
    } 
    return false;
  }
  
  public boolean getSourceOrDefaultVisibility(int paramInt) {
    boolean bool;
    InsetsSource insetsSource = this.mSources[paramInt];
    if (insetsSource != null) {
      bool = insetsSource.isVisible();
    } else {
      bool = getDefaultVisibility(paramInt);
    } 
    return bool;
  }
  
  public void setDisplayFrame(Rect paramRect) {
    this.mDisplayFrame.set(paramRect);
  }
  
  public Rect getDisplayFrame() {
    return this.mDisplayFrame;
  }
  
  public void removeSource(int paramInt) {
    this.mSources[paramInt] = null;
  }
  
  public void setSourceVisible(int paramInt, boolean paramBoolean) {
    InsetsSource insetsSource = this.mSources[paramInt];
    if (insetsSource != null)
      insetsSource.setVisible(paramBoolean); 
  }
  
  public void set(InsetsState paramInsetsState) {
    set(paramInsetsState, false);
  }
  
  public void set(InsetsState paramInsetsState, boolean paramBoolean) {
    this.mDisplayFrame.set(paramInsetsState.mDisplayFrame);
    if (paramBoolean) {
      for (byte b = 0; b < 16; b++) {
        InsetsSource insetsSource = paramInsetsState.mSources[b];
        InsetsSource[] arrayOfInsetsSource = this.mSources;
        if (insetsSource != null) {
          insetsSource = new InsetsSource(insetsSource, this.mIsFormInsetsAnimation);
        } else {
          insetsSource = null;
        } 
        arrayOfInsetsSource[b] = insetsSource;
      } 
    } else {
      for (byte b = 0; b < 16; b++)
        this.mSources[b] = paramInsetsState.mSources[b]; 
    } 
  }
  
  public void addSource(InsetsSource paramInsetsSource) {
    this.mSources[paramInsetsSource.getType()] = paramInsetsSource;
  }
  
  public static ArraySet<Integer> toInternalType(int paramInt) {
    ArraySet<Integer> arraySet = new ArraySet();
    if ((paramInt & 0x1) != 0)
      arraySet.add(Integer.valueOf(0)); 
    if ((paramInt & 0x2) != 0)
      arraySet.add(Integer.valueOf(1)); 
    if ((paramInt & 0x4) != 0)
      arraySet.add(Integer.valueOf(2)); 
    if ((paramInt & 0x80) != 0) {
      arraySet.add(Integer.valueOf(9));
      arraySet.add(Integer.valueOf(10));
      arraySet.add(Integer.valueOf(11));
      arraySet.add(Integer.valueOf(12));
    } 
    if ((paramInt & 0x8) != 0)
      arraySet.add(Integer.valueOf(13)); 
    return arraySet;
  }
  
  public static int toPublicType(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown type: ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      case 13:
        return 8;
      case 9:
      case 10:
      case 11:
      case 12:
        return 128;
      case 7:
      case 8:
        return 64;
      case 5:
      case 6:
        return 16;
      case 3:
      case 4:
        return 32;
      case 2:
        return 4;
      case 1:
      case 15:
        return 2;
      case 0:
      case 14:
        break;
    } 
    return 1;
  }
  
  public static boolean getDefaultVisibility(int paramInt) {
    boolean bool;
    if (paramInt != 13) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean containsType(int[] paramArrayOfint, int paramInt) {
    if (paramArrayOfint == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOfint.length, b = 0; b < i; ) {
      int j = paramArrayOfint[b];
      if (j == paramInt)
        return true; 
      b++;
    } 
    return false;
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("InsetsState");
    paramPrintWriter.println(stringBuilder.toString());
    for (byte b = 0; b < 16; b++) {
      InsetsSource insetsSource = this.mSources[b];
      if (insetsSource != null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        stringBuilder.append("  ");
        insetsSource.dump(stringBuilder.toString(), paramPrintWriter);
      } 
    } 
  }
  
  public static String typeToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("ITYPE_UNKNOWN_");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 15:
        return "ITYPE_EXTRA_NAVIGATION_BAR";
      case 14:
        return "ITYPE_CLIMATE_BAR";
      case 13:
        return "ITYPE_IME";
      case 12:
        return "ITYPE_BOTTOM_DISPLAY_CUTOUT";
      case 11:
        return "ITYPE_RIGHT_DISPLAY_CUTOUT";
      case 10:
        return "ITYPE_TOP_DISPLAY_CUTOUT";
      case 9:
        return "ITYPE_LEFT_DISPLAY_CUTOUT";
      case 8:
        return "ITYPE_BOTTOM_TAPPABLE_ELEMENT";
      case 7:
        return "ITYPE_TOP_TAPPABLE_ELEMENT";
      case 6:
        return "ITYPE_RIGHT_GESTURES";
      case 5:
        return "ITYPE_LEFT_GESTURES";
      case 4:
        return "ITYPE_BOTTOM_GESTURES";
      case 3:
        return "ITYPE_TOP_GESTURES";
      case 2:
        return "ITYPE_CAPTION_BAR";
      case 1:
        return "ITYPE_NAVIGATION_BAR";
      case 0:
        break;
    } 
    return "ITYPE_STATUS_BAR";
  }
  
  public boolean equals(Object paramObject) {
    return equals(paramObject, false, false);
  }
  
  public boolean equals(Object paramObject, boolean paramBoolean1, boolean paramBoolean2) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!this.mDisplayFrame.equals(((InsetsState)paramObject).mDisplayFrame))
      return false; 
    for (byte b = 0; b < 16; b++) {
      if (!paramBoolean1 || 
        b != 2) {
        InsetsSource insetsSource1 = this.mSources[b];
        InsetsSource insetsSource2 = ((InsetsState)paramObject).mSources[b];
        if (insetsSource1 != null || insetsSource2 != null) {
          if ((insetsSource1 != null && insetsSource2 == null) || (insetsSource1 == null && insetsSource2 != null))
            return false; 
          if (!insetsSource2.equals(insetsSource1, paramBoolean2))
            return false; 
        } 
      } 
    } 
    return true;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mDisplayFrame, Integer.valueOf(Arrays.hashCode((Object[])this.mSources)) });
  }
  
  public InsetsState(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mDisplayFrame, paramInt);
    paramParcel.writeParcelableArray((Parcelable[])this.mSources, 0);
  }
  
  static {
    CREATOR = new Parcelable.Creator<InsetsState>() {
        public InsetsState createFromParcel(Parcel param1Parcel) {
          return new InsetsState(param1Parcel);
        }
        
        public InsetsState[] newArray(int param1Int) {
          return new InsetsState[param1Int];
        }
      };
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mDisplayFrame.set((Rect)paramParcel.readParcelable(null));
    this.mSources = (InsetsSource[])paramParcel.readParcelableArray(null, InsetsSource.class);
  }
  
  public String toString() {
    StringJoiner stringJoiner = new StringJoiner(", ");
    for (byte b = 0; b < 16; b++) {
      InsetsSource insetsSource = this.mSources[b];
      if (insetsSource != null)
        stringJoiner.add(insetsSource.toString()); 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InsetsState: {mDisplayFrame=");
    stringBuilder.append(this.mDisplayFrame);
    stringBuilder.append(", mSources= { ");
    stringBuilder.append(stringJoiner);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public void scaleInsets(float paramFloat) {
    this.mDisplayFrame.scale(paramFloat);
    for (byte b = 0; b <= 15; b++) {
      InsetsSource insetsSource = this.mSources[b];
      if (insetsSource != null)
        insetsSource.scaleFrame(paramFloat); 
    } 
  }
  
  public InsetsState() {}
  
  @Retention(RetentionPolicy.SOURCE)
  class InternalInsetsSide implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class InternalInsetsType implements Annotation {}
}
