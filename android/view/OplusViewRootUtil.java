package android.view;

import android.content.Context;
import android.content.res.CompatibilityInfo;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Slog;
import java.lang.ref.WeakReference;

public class OplusViewRootUtil implements IOplusViewRootUtil {
  private static OplusViewRootUtil sInstance = null;
  
  private float mCompactScale = 1.0F;
  
  static {
    mHeteromorphism = false;
    mHeteromorphismHeight = -1;
  }
  
  private boolean mFullScreen = true;
  
  private DisplayInfo mDisplayInfo = new DisplayInfo();
  
  private int mNavBarMode = 0;
  
  private static final float EVENT_270 = 1.5707964F;
  
  private static final float EVENT_90 = -1.5707964F;
  
  private static final float EVENT_ORI = 0.0F;
  
  private static final float EVENT_OTHER = -3.1415927F;
  
  private static final float GESTURE_BOTTOM_AREA_PROP = 0.05F;
  
  private static final float GLOABL_SCALE_COMPAT_APP = 1.333333F;
  
  private static final String HATEEROMORPHISM = "oppo.systemui.disable.edgepanel";
  
  public static final String KEY_NAVIGATIONBAR_MODE = "hide_navigationbar_enable";
  
  private static final boolean LOCAL_LOGV = true;
  
  private static final int MODE_NAVIGATIONBAR = 0;
  
  private static final int MODE_NAVIGATIONBAR_GESTURE = 2;
  
  private static final int MODE_NAVIGATIONBAR_GESTURE_SIDE = 3;
  
  private static final int MODE_NAVIGATIONBAR_WITH_HIDE = 1;
  
  private static final String TAG = "ColorViewRootUtil";
  
  private static boolean mHeteromorphism;
  
  private static int mHeteromorphismHeight;
  
  private int mHideNavigationbarArea;
  
  private boolean mIgnoring;
  
  private boolean mIsDisplayCompatApp;
  
  private int mScreenHeight;
  
  private int mScreenWidth;
  
  private int mSideGestureAreaWidth;
  
  private int mStatusBarHeight;
  
  public static OplusViewRootUtil getInstance() {
    // Byte code:
    //   0: ldc android/view/OplusViewRootUtil
    //   2: monitorenter
    //   3: getstatic android/view/OplusViewRootUtil.sInstance : Landroid/view/OplusViewRootUtil;
    //   6: ifnonnull -> 21
    //   9: new android/view/OplusViewRootUtil
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/view/OplusViewRootUtil.sInstance : Landroid/view/OplusViewRootUtil;
    //   21: getstatic android/view/OplusViewRootUtil.sInstance : Landroid/view/OplusViewRootUtil;
    //   24: astore_0
    //   25: ldc android/view/OplusViewRootUtil
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/view/OplusViewRootUtil
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #77	-> 0
    //   #78	-> 3
    //   #79	-> 9
    //   #81	-> 21
    //   #82	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	28	30	finally
    //   31	34	30	finally
  }
  
  public void initSwipState(Display paramDisplay, Context paramContext) {
    Slog.d("ColorViewRootUtil", "initSwipState");
    initSwipState(paramDisplay, paramContext, false);
  }
  
  public void initSwipState(Display paramDisplay, Context paramContext, boolean paramBoolean) {
    StringBuilder stringBuilder2;
    int k;
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("initSwipState, isDisplayCompatApp ");
    stringBuilder3.append(paramBoolean);
    Slog.d("ColorViewRootUtil", stringBuilder3.toString());
    this.mIsDisplayCompatApp = paramBoolean;
    if (paramDisplay != null && paramDisplay.getDisplayId() != -1 && 
      paramDisplay.getDisplayId() != 0) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("don't initSwipState because display ");
      stringBuilder2.append(paramDisplay.getDisplayId());
      stringBuilder2.append(" is not default display");
      Slog.d("ColorViewRootUtil", stringBuilder2.toString());
      return;
    } 
    paramDisplay.getDisplayInfo(this.mDisplayInfo);
    DisplayMetrics displayMetrics2 = new DisplayMetrics();
    paramDisplay.getRealMetrics(displayMetrics2);
    int i = displayMetrics2.heightPixels;
    int j = displayMetrics2.widthPixels;
    if (i > j) {
      k = i;
    } else {
      k = j;
    } 
    this.mScreenHeight = k;
    if (i <= j)
      j = i; 
    this.mScreenWidth = j;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("mScreenHeight ");
    stringBuilder1.append(this.mScreenHeight);
    stringBuilder1.append(", mScreenWidth ");
    stringBuilder1.append(this.mScreenWidth);
    Slog.d("ColorViewRootUtil", stringBuilder1.toString());
    j = this.mDisplayInfo.logicalHeight;
    i = this.mDisplayInfo.logicalWidth;
    if (j <= i)
      j = i; 
    this.mHideNavigationbarArea = stringBuilder2.getResources().getDimensionPixelSize(17105326) + j - 21;
    this.mStatusBarHeight = stringBuilder2.getResources().getDimensionPixelSize(201654286);
    int m = stringBuilder2.getPackageManager().hasSystemFeature("oppo.systemui.disable.edgepanel") ^ true;
    if (m != 0)
      mHeteromorphismHeight = stringBuilder2.getResources().getInteger(202178562); 
    DisplayMetrics displayMetrics1 = new DisplayMetrics();
    this.mDisplayInfo.getAppMetrics(displayMetrics1);
    this.mCompactScale = CompatibilityInfo.computeCompatibleScaling(displayMetrics1, null);
    this.mNavBarMode = Settings.Secure.getInt(stringBuilder2.getContentResolver(), "hide_navigationbar_enable", 0);
    this.mSideGestureAreaWidth = stringBuilder2.getResources().getInteger(202178573);
    if (paramBoolean) {
      Slog.d("ColorViewRootUtil", "sIsDisplayCompatApp");
      this.mScreenHeight = (int)(this.mScreenHeight * 1.333333F);
      this.mScreenWidth = (int)(this.mScreenWidth * 1.333333F);
    } 
    this.mStatusBarHeight = (int)(this.mScreenWidth * 0.05F);
  }
  
  public boolean needScale(int paramInt1, int paramInt2, Display paramDisplay) {
    CompatibilityInfo compatibilityInfo1 = null;
    CompatibilityInfo compatibilityInfo2 = compatibilityInfo1;
    if (paramDisplay != null) {
      compatibilityInfo2 = compatibilityInfo1;
      if (paramDisplay.getDisplayAdjustments() != null)
        compatibilityInfo2 = paramDisplay.getDisplayAdjustments().getCompatibilityInfo(); 
    } 
    if (compatibilityInfo2 != null && compatibilityInfo2.isScalingRequired() && 
      !compatibilityInfo2.supportsScreen() && paramInt1 != paramInt2)
      return true; 
    return false;
  }
  
  public float getCompactScale() {
    return this.mCompactScale;
  }
  
  public int getScreenHeight() {
    return this.mScreenHeight;
  }
  
  public int getScreenWidth() {
    return this.mScreenWidth;
  }
  
  public boolean swipeFromBottom(MotionEvent paramMotionEvent, int paramInt1, int paramInt2, Display paramDisplay) {
    StringBuilder stringBuilder1, stringBuilder2;
    Slog.d("ColorViewRootUtil", "swipeFromBottom!");
    if (paramDisplay.getDisplayId() != 0) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("don't intercept event because display ");
      stringBuilder1.append(paramDisplay.getDisplayId());
      stringBuilder1.append(" is not default display");
      Slog.e("ColorViewRootUtil", stringBuilder1.toString());
      return false;
    } 
    if ((stringBuilder1.getSource() & 0x2) == 0) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("don't intercept event because event source is ");
      stringBuilder2.append(stringBuilder1.getSource());
      Slog.d("ColorViewRootUtil", stringBuilder2.toString());
      return false;
    } 
    int i = stringBuilder1.getAction();
    if (i != 0) {
      if (i == 1)
        if (this.mIgnoring) {
          this.mIgnoring = false;
          return true;
        }  
    } else {
      int j = (int)stringBuilder1.getRawY();
      int k = (int)stringBuilder1.getRawX();
      float f = this.mCompactScale;
      int m = j;
      i = k;
      if (f != 1.0F) {
        m = j;
        i = k;
        if (f != -1.0F) {
          m = j;
          i = k;
          if (needScale(paramInt1, paramInt2, (Display)stringBuilder2)) {
            f = this.mCompactScale;
            m = (int)(j * f + 0.5F);
            i = (int)(f * k + 0.5F);
          } 
        } 
      } 
      this.mIgnoring = shouldIgnore(i, m, (MotionEvent)stringBuilder1, (Display)stringBuilder2);
    } 
    return this.mIgnoring;
  }
  
  public void checkGestureConfig(Context paramContext) {
    this.mNavBarMode = Settings.Secure.getInt(paramContext.getContentResolver(), "hide_navigationbar_enable", 0);
  }
  
  public DisplayInfo getDisplayInfo() {
    return this.mDisplayInfo;
  }
  
  public IOplusLongshotViewHelper getOplusLongshotViewHelper(WeakReference<ViewRootImpl> paramWeakReference) {
    return new OplusLongshotViewHelper(paramWeakReference);
  }
  
  private boolean shouldIgnore(int paramInt1, int paramInt2, MotionEvent paramMotionEvent, Display paramDisplay) {
    // Byte code:
    //   0: aload_3
    //   1: invokevirtual getDownTime : ()J
    //   4: aload_3
    //   5: invokevirtual getEventTime : ()J
    //   8: lcmp
    //   9: ifeq -> 49
    //   12: new java/lang/StringBuilder
    //   15: dup
    //   16: invokespecial <init> : ()V
    //   19: astore #4
    //   21: aload #4
    //   23: ldc 'do not ignore inject event MotionEvent:'
    //   25: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   28: pop
    //   29: aload #4
    //   31: aload_3
    //   32: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: ldc 'ColorViewRootUtil'
    //   38: aload #4
    //   40: invokevirtual toString : ()Ljava/lang/String;
    //   43: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   46: pop
    //   47: iconst_0
    //   48: ireturn
    //   49: iconst_0
    //   50: istore #5
    //   52: aload #4
    //   54: ifnull -> 64
    //   57: aload #4
    //   59: invokevirtual getRotation : ()I
    //   62: istore #5
    //   64: aload_0
    //   65: getfield mIsDisplayCompatApp : Z
    //   68: ifeq -> 102
    //   71: ldc 'ColorViewRootUtil'
    //   73: ldc 'shouldIgnore, sIsDisplayCompatApp'
    //   75: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   78: pop
    //   79: iload_1
    //   80: i2f
    //   81: ldc 1.333333
    //   83: fmul
    //   84: f2i
    //   85: istore_1
    //   86: iload_2
    //   87: i2f
    //   88: ldc 1.333333
    //   90: fmul
    //   91: f2i
    //   92: istore #6
    //   94: iload_1
    //   95: istore_2
    //   96: iload #6
    //   98: istore_1
    //   99: goto -> 110
    //   102: iload_1
    //   103: istore #6
    //   105: iload_2
    //   106: istore_1
    //   107: iload #6
    //   109: istore_2
    //   110: aload_0
    //   111: getfield mNavBarMode : I
    //   114: istore #6
    //   116: iload #6
    //   118: ifeq -> 443
    //   121: iload #6
    //   123: iconst_1
    //   124: if_icmpne -> 130
    //   127: goto -> 443
    //   130: iload #5
    //   132: ifeq -> 215
    //   135: iload #5
    //   137: iconst_1
    //   138: if_icmpeq -> 159
    //   141: iload #5
    //   143: iconst_2
    //   144: if_icmpeq -> 215
    //   147: iload #5
    //   149: iconst_3
    //   150: if_icmpeq -> 159
    //   153: iconst_0
    //   154: istore #7
    //   156: goto -> 268
    //   159: iload_1
    //   160: aload_0
    //   161: getfield mScreenWidth : I
    //   164: aload_0
    //   165: getfield mStatusBarHeight : I
    //   168: isub
    //   169: if_icmpge -> 209
    //   172: aload_0
    //   173: getfield mSideGestureAreaWidth : I
    //   176: istore #6
    //   178: iload_2
    //   179: iload #6
    //   181: if_icmplt -> 195
    //   184: iload_2
    //   185: aload_0
    //   186: getfield mScreenHeight : I
    //   189: iload #6
    //   191: isub
    //   192: if_icmple -> 203
    //   195: aload_0
    //   196: getfield mNavBarMode : I
    //   199: iconst_3
    //   200: if_icmpeq -> 209
    //   203: iconst_0
    //   204: istore #7
    //   206: goto -> 268
    //   209: iconst_1
    //   210: istore #7
    //   212: goto -> 268
    //   215: iload_1
    //   216: aload_0
    //   217: getfield mScreenHeight : I
    //   220: aload_0
    //   221: getfield mStatusBarHeight : I
    //   224: isub
    //   225: if_icmpge -> 265
    //   228: aload_0
    //   229: getfield mSideGestureAreaWidth : I
    //   232: istore #6
    //   234: iload_2
    //   235: iload #6
    //   237: if_icmplt -> 251
    //   240: iload_2
    //   241: aload_0
    //   242: getfield mScreenWidth : I
    //   245: iload #6
    //   247: isub
    //   248: if_icmple -> 259
    //   251: aload_0
    //   252: getfield mNavBarMode : I
    //   255: iconst_3
    //   256: if_icmpeq -> 265
    //   259: iconst_0
    //   260: istore #7
    //   262: goto -> 268
    //   265: iconst_1
    //   266: istore #7
    //   268: new java/lang/StringBuilder
    //   271: dup
    //   272: invokespecial <init> : ()V
    //   275: astore #4
    //   277: aload #4
    //   279: ldc 'nav gesture mode swipeFromBottom ignore '
    //   281: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   284: pop
    //   285: aload #4
    //   287: iload #7
    //   289: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   292: pop
    //   293: aload #4
    //   295: ldc ' downY '
    //   297: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   300: pop
    //   301: aload #4
    //   303: iload_1
    //   304: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   307: pop
    //   308: aload #4
    //   310: ldc ' mScreenHeight '
    //   312: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   315: pop
    //   316: aload #4
    //   318: aload_0
    //   319: getfield mScreenHeight : I
    //   322: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   325: pop
    //   326: aload #4
    //   328: ldc ' mScreenWidth '
    //   330: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   333: pop
    //   334: aload #4
    //   336: aload_0
    //   337: getfield mScreenWidth : I
    //   340: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   343: pop
    //   344: aload #4
    //   346: ldc ' mStatusBarHeight '
    //   348: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   351: pop
    //   352: aload #4
    //   354: aload_0
    //   355: getfield mStatusBarHeight : I
    //   358: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   361: pop
    //   362: aload #4
    //   364: ldc ' globalScale '
    //   366: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   369: pop
    //   370: aload #4
    //   372: aload_0
    //   373: getfield mCompactScale : F
    //   376: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   379: pop
    //   380: aload #4
    //   382: ldc ' nav mode '
    //   384: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   387: pop
    //   388: aload #4
    //   390: aload_0
    //   391: getfield mNavBarMode : I
    //   394: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   397: pop
    //   398: aload #4
    //   400: ldc ' event '
    //   402: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   405: pop
    //   406: aload #4
    //   408: aload_3
    //   409: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   412: pop
    //   413: aload #4
    //   415: ldc ' rotation '
    //   417: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   420: pop
    //   421: aload #4
    //   423: iload #5
    //   425: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   428: pop
    //   429: ldc 'ColorViewRootUtil'
    //   431: aload #4
    //   433: invokevirtual toString : ()Ljava/lang/String;
    //   436: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   439: pop
    //   440: goto -> 729
    //   443: iload #5
    //   445: ifeq -> 492
    //   448: iload #5
    //   450: iconst_1
    //   451: if_icmpeq -> 495
    //   454: iload #5
    //   456: iconst_2
    //   457: if_icmpeq -> 492
    //   460: iload #5
    //   462: iconst_3
    //   463: if_icmpeq -> 472
    //   466: iconst_0
    //   467: istore #7
    //   469: goto -> 542
    //   472: iload_2
    //   473: aload_0
    //   474: getfield mStatusBarHeight : I
    //   477: if_icmple -> 486
    //   480: iconst_0
    //   481: istore #7
    //   483: goto -> 542
    //   486: iconst_1
    //   487: istore #7
    //   489: goto -> 542
    //   492: goto -> 520
    //   495: iload_2
    //   496: aload_0
    //   497: getfield mScreenHeight : I
    //   500: aload_0
    //   501: getfield mStatusBarHeight : I
    //   504: isub
    //   505: if_icmpge -> 514
    //   508: iconst_0
    //   509: istore #7
    //   511: goto -> 542
    //   514: iconst_1
    //   515: istore #7
    //   517: goto -> 542
    //   520: iload_1
    //   521: aload_0
    //   522: getfield mScreenHeight : I
    //   525: aload_0
    //   526: getfield mStatusBarHeight : I
    //   529: isub
    //   530: if_icmpge -> 539
    //   533: iconst_0
    //   534: istore #7
    //   536: goto -> 542
    //   539: iconst_1
    //   540: istore #7
    //   542: new java/lang/StringBuilder
    //   545: dup
    //   546: invokespecial <init> : ()V
    //   549: astore #4
    //   551: aload #4
    //   553: ldc 'nav bar mode ignore '
    //   555: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   558: pop
    //   559: aload #4
    //   561: iload #7
    //   563: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   566: pop
    //   567: aload #4
    //   569: ldc ' downX '
    //   571: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   574: pop
    //   575: aload #4
    //   577: iload_2
    //   578: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   581: pop
    //   582: aload #4
    //   584: ldc ' downY '
    //   586: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   589: pop
    //   590: aload #4
    //   592: iload_1
    //   593: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   596: pop
    //   597: aload #4
    //   599: ldc ' mScreenHeight '
    //   601: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   604: pop
    //   605: aload #4
    //   607: aload_0
    //   608: getfield mScreenHeight : I
    //   611: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   614: pop
    //   615: aload #4
    //   617: ldc ' mScreenWidth '
    //   619: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   622: pop
    //   623: aload #4
    //   625: aload_0
    //   626: getfield mScreenWidth : I
    //   629: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   632: pop
    //   633: aload #4
    //   635: ldc ' mStatusBarHeight '
    //   637: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   640: pop
    //   641: aload #4
    //   643: aload_0
    //   644: getfield mStatusBarHeight : I
    //   647: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   650: pop
    //   651: aload #4
    //   653: ldc ' globalScale '
    //   655: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   658: pop
    //   659: aload #4
    //   661: aload_0
    //   662: getfield mCompactScale : F
    //   665: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   668: pop
    //   669: aload #4
    //   671: ldc ' nav mode '
    //   673: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   676: pop
    //   677: aload #4
    //   679: aload_0
    //   680: getfield mNavBarMode : I
    //   683: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   686: pop
    //   687: aload #4
    //   689: ldc ' rotation '
    //   691: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   694: pop
    //   695: aload #4
    //   697: iload #5
    //   699: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   702: pop
    //   703: aload #4
    //   705: ldc ' event '
    //   707: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   710: pop
    //   711: aload #4
    //   713: aload_3
    //   714: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   717: pop
    //   718: ldc 'ColorViewRootUtil'
    //   720: aload #4
    //   722: invokevirtual toString : ()Ljava/lang/String;
    //   725: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   728: pop
    //   729: iload #7
    //   731: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #234	-> 0
    //   #235	-> 12
    //   #236	-> 47
    //   #238	-> 49
    //   #239	-> 49
    //   #240	-> 52
    //   #241	-> 57
    //   #243	-> 64
    //   #244	-> 71
    //   #245	-> 79
    //   #246	-> 86
    //   #243	-> 102
    //   #248	-> 110
    //   #277	-> 130
    //   #291	-> 159
    //   #295	-> 203
    //   #297	-> 209
    //   #280	-> 215
    //   #284	-> 259
    //   #286	-> 265
    //   #288	-> 268
    //   #301	-> 268
    //   #248	-> 443
    //   #249	-> 443
    //   #266	-> 472
    //   #267	-> 480
    //   #269	-> 486
    //   #249	-> 492
    //   #259	-> 495
    //   #260	-> 508
    //   #262	-> 514
    //   #264	-> 517
    //   #252	-> 520
    //   #253	-> 533
    //   #255	-> 539
    //   #257	-> 542
    //   #273	-> 542
    //   #305	-> 729
  }
}
